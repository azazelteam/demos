<?php
include_once( dirname( __FILE__ ) . "/../../objects/classes.php" );

$Title = "Zone chaude, Ouh coquine ...";
$js = array();
$js[] = "jquery/jquery.qtip.min.js";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
?>
<script type="text/javascript" src="<?=$GLOBAL_START_URL?>/js/jquery/jquery.qtip.min.js"></script>

	<script type="text/javascript">
	<!--
	    $(document).ready(function(){
	        
	        $("#helpClickEstimate").qtip({
	            adjust: {
	                mouse: true,
	                screen: true
	            },
	            content: 'Visiteur(s) ayant cliqué sur<br />le bouton "Ajouter à mon panier devis".',
	            position: {
	                corner: {
	                    target: "rightMiddle",
	                    tooltip: "leftMiddle"
	                }
	            },
	            style: {
	                background: "#FFFFFF",
	                border: {
	                    color: "#EB6A0A",
	                    radius: 5
	                },
	                fontFamily: "Arial,Helvetica,sans-serif",
	                fontSize: "13px",
	                tip: true
	            }
	        });

	    });
	-->
	</script>

<style type="text/css">
			<!--
			div.mainContent div.content ul.menu {
				width:510px;
				vertical-align:bottom;
				float:right;
				margin-top:-32px;
				*margin-top:-21px;
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item {
				background-color:#EB6a0a;
				width:95px;
				color:#FFFFFF;
				height:20px;
				float:left;
				margin-left:7px;
				cursor:pointer;
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item a {
				color:#FFFFFF;
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.tabs-selected a {
				color:#44474E;
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item div.centered {
				margin-left:8px;
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item div.right,
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item div.left {
				width:5px;
				height:5px;
				background-repeat:no-repeat;
				font-size:1px; /* correction d'un bug IE */
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item div.left {
				background-image:url(../../../images/back_office/layout/menu_corner_left.gif);
			}
			
			div#global div#globalMainContent div.mainContent div.content ul.menu li.item div.right {
				background-image:url(../../../images/back_office/layout/menu_corner_right.gif);
				float:right;
			}
			
			textarea{
				 font-size:10px;
			}
			
			.center{
				text-align:center;
			}
	
			.titleRss{
				text-align:center;
				font-weight:bold;
			}
			
			.info{
				text-align: left;
				padding-top: 15px;
				padding-bottom: 15px;
				margin-left: 15px;
				margin-right: 15px;
			}
			
			.spacer{
				height: 25px;
				border: 0px;
			}
			-->
</style>

<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">
                	Exploration des zonnes chaudes
                	<a name="topPage"></a>
                </p>
            </div>
            <div class="subContent" style="margin-top: 20px;">
            <!-- -->
            <?php
/**
 * ClickHeat : Fichier de résultats / Results file
 * 
 * @author Yvan Taviaud - LabsMedia - www.labsmedia.com
 * @since 27/10/2006
**/

/** Direct call forbidden */

if (!defined('CLICKHEAT_LANGUAGE'))
{
	exit;
}

/** List of available groups */
$groups = array();
$d = dir($clickheatConf['logPath']);
while (($dir = $d->read()) !== false)
{
	if ($dir === '.' || $dir === '..' || !is_dir($d->path.$dir)) continue;
	$pos = strpos($dir, ',');
	if ($pos !== false)
	{
		$site = substr($dir, 0, $pos);
	}
	else
	{
		$site = '';
	}
	if (IS_PIWIK_MODULE === true)
	{
		if ($site !== PHPMV_SELECTED_SITE)
		{
			continue;
		}
		$site = '';
	}
	if (!isset($groups[$site]))
	{
		$groups[$site] = array();
	}
	$groups[$site][] = '<option value="'.$dir.'">'.($pos === false ? $dir : substr($dir, $pos + 1)).'</option>';
}
$d->close();
/** Sort groups in alphabetical order */
ksort($groups);
$__selectGroups = '';
foreach ($groups as $key => $options)
{
	sort($options);
	if ($key !== '')
	{
		$__selectGroups .= '<optgroup label="'.$key.'">';
	}
	$__selectGroups .= implode("\n", $options);
	if ($key !== '')
	{
		$__selectGroups .= '</optgroup>';
	}
}

asort($__screenSizes);
/** Screen sizes */
$__selectScreens = '';
for ($i = 0; $i < count($__screenSizes); $i++)
{
	$__selectScreens .= '<option value="'.$__screenSizes[$i].'">'.($__screenSizes[$i] === 0 ? LANG_ALL : $__screenSizes[$i].'px').'</option>';
}

/** Browsers */
$__selectBrowsers = '';
foreach ($__browsersList as $label => $name)
{
	$__selectBrowsers .= '<option value="'.$label.'">'.($label === 'all' ? LANG_ALL : ($label === 'unknown' ? LANG_UNKNOWN : $name)).'</option>';
}

/** Date */
$date = isset($_GET['date']) ? strtotime($_GET['date']) : ($clickheatConf['yesterday'] === true ? mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')) : false);
if ($date === false)
{
	$date = time();
}
$__day = (int) date('d', $date);
$__month = (int) date('m', $date);
$__year = (int) date('Y', $date);
?>
<div id="adminPanel"><span class="float-right">
<a href="http://www.labsmedia.<?php echo CLICKHEAT_LANGUAGE === 'fr' ? 'fr' : 'com' ?>/clickheat/index.html"><img src="<?=$GLOBAL_START_URL?>/clickheat/images/logo170.png" width="170" height="35" alt="ClickHeat" /></a><br />
<a href="#" onclick="adminCookie(); return false;"><?php echo LANG_LOG_MY_CLICKS ?></a>
<?php if (CLICKHEAT_ADMIN === true) echo '<a href="', CLICKHEAT_INDEX_PATH, 'action=config">', LANG_CONFIG, '</a> <a href="#" onclick="showJsCode(); return false;">Javascript</a> <a href="#" onclick="showLatestVersion(); return false;">', LANG_LATEST_CHECK, '</a> '; ?>
<a href="<?php echo CLICKHEAT_INDEX_PATH ?>action=logout"><?php echo LANG_LOGOUT ?></a><br />
<?php
foreach ($__languages as $lang) 
{
	echo '<a href="', CLICKHEAT_INDEX_PATH, 'language=', $lang, '"><img src="', $GLOBAL_START_URL, '/clickheat/images/flags/', $lang, '.png" width="18" height="12" alt="', $lang, '" /></a> ';
}
?><br />
<span id="cleaner">&nbsp;</span></span>
<form action="<?php echo CLICKHEAT_INDEX_PATH ?>" method="get" onsubmit="return false;" id="clickForm">
<table cellpadding="0" cellspacing="1" border="0" id="clickTable">
<tr>
	<th><?php echo LANG_SITE ?> &amp; <?php echo LANG_GROUP ?></th><td><select name="group" id="formGroup" onchange="hideGroupLayout(); loadIframe();"><?php echo $__selectGroups ?></select><?php if (CLICKHEAT_ADMIN === true) echo ' <a href="#" onclick="showGroupLayout(); return false;"><img src="', $GLOBAL_START_URL, '/clickheat/images/layout.png" width="16" height="16" align="absmiddle" alt="Layout" /></a>'; ?> <a href="#" onclick="updateHeatmap(); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/clickheat/images/reload.png" width="16" height="16" align="absmiddle" alt="Refresh" /></a></td>
	<td rowspan="4">
<?php
$__calendar = '<table cellpadding="0" cellspacing="0" border="0" class="clickheat-calendar"><tr>';
$days = explode(',', LANG_DAYS);
for ($d = 0; $d < 7; $d++)
{
	$D = $d + ($clickheatConf['start'] === 'm' ? 0 : 6);
	if ($D > 6)
	{
		$D -= 7;
	}
	$__calendar .= '<th>'.$days[$D].'</th>';
}
$__calendar .= '</tr><tr>';
$before = date('w', mktime(0, 0, 0, $__month, 1, $__year)) - ($clickheatConf['start'] === 'm' ? 1 : 0);
if ($before < 0)
{
	$before += 7;
}
$__lastDayOfMonth = date('t', mktime(0, 0, 0, $__month - 1, 1, $__year));
for ($d = 1; $d <= $before; $d++)
{
	$__calendar .= '<td id="clickheat-calendar-10'.$d.'">'.($__lastDayOfMonth - $before + $d).'</td>';
}
$cols = $before - 1;
$__js = 'var weekDays = [';
for ($d = 1, $days = date('t', $date); $d <= $days; $d++)
{
	$D = mktime(0, 0, 0, $__month, $d, $__year);
	if (++$cols === 7)
	{
		$__calendar .= '</tr><tr>';
		$cols = 0;
	}
	$__calendar .= '<td id="clickheat-calendar-'.$d.'"><a href="#" onclick="updateCalendar('.$d.'); return false;">'.$d.'</a></td>';
	$__js .= ','.(date('W', $D) + (date('w', $D) == 0 && $clickheatConf['start'] === 's' ? 1 : 0));
}
$__js .= '];';
for ($d = 1; $cols < 6; $cols++, $d++)
{
	$__calendar .= '<td id="clickheat-calendar-11'.$d.'">'.$d.'</td>';
}
$__calendar .= '</tr></table>';
$ranges = explode(',', LANG_RANGE);
$months = explode(',', LANG_MONTHS);
echo $__calendar;
?>
	</td>
	<td rowspan="4">
		<table cellpadding="1" cellspacing="0" border="0" class="clickheat-calendar">
			<tr>
				<th><a href="#" onclick="url = window.location.href.replace(/&?date=\d+-\d+-\d+/,''); if (url.search(/\?/) == -1) url += '?'; url += '&date=<?php echo ($__month == '1' ? $__year - 1 : $__year), '-', ($__month == '1' ? '12' : sprintf('%02d', $__month - 1)) ?>-01'; window.location.href = url; return false;"><img src="<?php echo $GLOBAL_START_URL ?>/clickheat/images/previous.png" width="16" height="16" align="absmiddle" alt="Previous" /></a><?php echo $months[$__month] ?><a href="#" onclick="url = window.location.href.replace(/&?date=\d+-\d+-\d+/,''); if (url.search(/\?/) == -1) url += '?'; url += '&date=<?php echo ($__month == '12' ? $__year + 1 : $__year), '-', ($__month == '12' ? '01' : sprintf('%02d', $__month + 1)) ?>-01'; window.location.href = url; return false;"><img src="<?php echo $GLOBAL_START_URL ?>/clickheat/images/next.png" width="16" height="16" align="absmiddle" alt="Next" /></a></th>
			</tr>
			<tr>
				<td id="clickheat-calendar-d"><a href="#" onclick="currentRange = 'd'; this.blur(); updateCalendar(); return false;"><?php echo $ranges[0] ?></a></td>
			</tr>
			<tr>
				<td id="clickheat-calendar-w"><a href="#" onclick="currentRange = 'w'; this.blur(); updateCalendar(); return false;"><?php echo $ranges[1] ?></a></td>
			</tr>
			<tr>
				<td id="clickheat-calendar-m"><a href="#" onclick="currentRange = 'm'; this.blur(); updateCalendar(); return false;"><?php echo $ranges[2] ?></a></td>
			</tr>
		</table>
	</td>
</tr>
<tr>
	<th><?php echo LANG_BROWSER ?></th><td><select name="browser" id="formBrowser" onchange="updateHeatmap();"><?php echo $__selectBrowsers ?></select></td>
</tr>
<tr>
	<th><?php echo LANG_HEATMAP ?></th><td><input type="checkbox" id="formHeatmap" name="heatmap" onclick="updateHeatmap();"<?php if ($clickheatConf['heatmap'] === true) echo ' checked="checked"'; ?> /><span id="alphaSelector"></span></td>
</tr>
<tr>
	<th><?php echo LANG_SCREENSIZE ?></th><td><select name="screen" id="formScreen" onchange="resizeDiv(); updateHeatmap();"><?php echo $__selectScreens ?></select></td>
</tr>
</table>
</form>
</div>
<div id="divPanel" onmouseover="showPanel();" onclick="hidePanel();"><img src="<?php echo $GLOBAL_START_URL?>/clickheat/images/arrow-up.png" width="11" height="6" alt="" /></div>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/clickheat/js/admin.js"></script>
<div id="overflowDiv">
	<div id="layoutDiv"></div>
	<div id="pngDiv"></div>
	<p><iframe src="<?php echo $GLOBAL_START_URL ?>/clickheat/clickempty.html" id="webPageFrame" onload="cleanIframe();" frameborder="0" scrolling="no" width="50" height="0"></iframe></p>
</div>

<!-- -->

            </div>
				 
        </div>  
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos</p>
            </div>
            <div class="content" style="padding-top:5px;padding-left:10px; padding-right:10px; text-align:center;">
	            <p style="text-align:left; margin-top:0px;; margin-bottom:0px;">
	            	Séléctionner la page souhaité, ainsi qu'une date afin d'accéder au zone chaude ...<br />Par défaut votre adresse est exclue des zonnes chaudes.
	            </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        
    </div>
   	<div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Résultat de la recherche </p>
				<div class="rightContainer"></div>
            </div>
            <div class="subContent">
				<div class="tableHeaderLeft"></div>
				<div class="tableHeaderRight"></div>
            	
                <div class="resultTableContainer clear">
				<iframe src ="<?=$GLOBAL_START_URL?>/clickheat/index.php?action=view" width="100%" height="1024px" title="ClickHeat">
				
				</iframe>
               </div>
                
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>  <!--mainContent fullWidthContent -->

</div><!-- globalMainContent -->

<?php
include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>