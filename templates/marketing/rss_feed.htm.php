<?php

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );
?>
			
			<style type="text/css">
			/* <![CDATA[ */
				
				.ui-tabs .ui-tabs-nav{
					width:auto;
					float:right;
					margin-top:-32px;
					*margin-top:-21px;
				}
				
				.ui-tabs .ui-tabs-nav li{
					width: auto;
				}
				
			/* ]]> */
			</style>
			
			<script type="text/javascript">
			<!--
				//onglets
				$(function() {
					$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
				});
				
				function doActivate(){
					
					var activate = document.getElementById('activate').value ;
					
					var location='<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;
					var data='action=activate&activate=' + activate ;
					var divToComplete = document.getElementById('messZone');
					var message='';
					var callback = 'refreshDoActivate( '+ activate +' )' ;
					var sync = false ;
					machineAjax( location, divToComplete, data, message, true, callback, sync );
	
				}
				
				function refreshDoActivate(activate){
					var zoneActi = document.getElementById('zoneActivate') ;
					if(activate == 1){
						zoneActi.innerHTML = '<form id="useRss" method="post" action="<?php echo $GLOBAL_START_URL ?>/marketing/rss/feed_rss.php"><input type="hidden" value="0" name="activate" id="activate"/><input type="submit" class="blueButton" value="Désactivé les flux RSS" onclick="javascript: doActivate(); return true ;"/></form>';
					}else{
						zoneActi.innerHTML = '<form id="useRss" method="post" action="<?php echo $GLOBAL_START_URL ?>/marketing/rss/feed_rss.php"><input type="hidden" value="1" name="activate" id="activate"/><input type="submit" class="blueButton" value="Activé les flux RSS" onclick="javascript: doActivate();"/></form>';
					}
					
				}
				
				function editRssItem( id ){
					
					var location='<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;
					var data='action=editItem&idItem=' + id ;
					var divToComplete = document.getElementById('loadZone');
					var message='Chargement du flux en cours ... ' ;
					var callback = null ;
					var sync = true ;
					machineAjax( location, divToComplete, data, message, true, callback, sync );
					
					return false ;
				}
				
				function createItem(){
					
					var titre = document.getElementById('tittle_rss').value;
					var pubDate = document.getElementById('pubDate_rss').value;
					var desc = document.getElementById('desc_rss').value;
					var link = document.getElementById('link_rss').value;
					var enclosure = document.getElementById('enclosure').value;
					var enclosure_type = document.getElementById('enclosure_type').options[document.getElementById('enclosure_type').selectedIndex].value;

					if(document.getElementById('active_rss').checked == false){
						var actif = 0 ;
						
					}else{
						var actif = 1 ;
						
					}
					
					var location='<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;
					var data='action=createItem&title_rss=' + titre + '&pubDate_rss=' + pubDate + '&desc_rss=' + desc + '&link_rss=' + link + '&actif=' + actif+'&enclosure='+enclosure+'&enclosure_type='+enclosure_type ;
					var divToComplete = document.getElementById('loadZone');
					var message='Ajout du flux en cours ... ';
										
					var callback = 'updateList()' ;
					var sync = true ;
					machineAjax( location, divToComplete, data, message, true, callback, sync );
					
				}
				
				function updateList(list){

						// Mise a jour de la liste actifs
						var location='<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;
						var data='action=reloadListActifs';
						var divToComplete = document.getElementById('actifs');
						var message='Mise à jour de la liste ... ';
						var callback = null ;
						var sync = true ;
						machineAjax( location, divToComplete, data, message, true, callback, sync );
					
						// Mise a jour de la liste inactifs
						var location1='<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;
						var data1='action=reloadListUnactifs';
						var divToComplete1 = document.getElementById('inactifs');
						var message1='Mise à jour de la liste ... ';
						var callback1 = null ;
						var sync1 = true ;
						machineAjax( location1, divToComplete1, data1, message1, true, callback1, sync1 );
					
				}
				
				function saveItem( idItem ){
					
					var titre = document.getElementById('tittle_rss').value;
					var pubDate = document.getElementById('pubDate_rss').value;
					var desc = document.getElementById('desc_rss').value;
					var link = document.getElementById('link_rss').value;
					var enclosure = document.getElementById('enclosure').value;
					var enclosure_type = document.getElementById('enclosure_type').options[document.getElementById('enclosure_type').selectedIndex].value;
					
					if(document.getElementById('active_rss').checked == false){
						var actif = 0 ;
					}else{
						var actif = 1 ;
					}
					
					var location='<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;
					var data='action=saveItem&idItem=' + idItem + '&title_rss=' + titre + '&pubDate_rss=' + pubDate + '&desc_rss=' + desc + '&link_rss=' + link + '&actif=' + actif +'&enclosure='+enclosure+'&enclosure_type='+enclosure_type;
					var divToComplete = document.getElementById('loadZone');
					var message='Sauvegarde du flux en cours ... ' ;
					var callback = 'updateList()' ;
					var sync = true ;
					machineAjax( location, divToComplete, data, message, true, callback, sync );
				}
				
				
				function machineAjax( location, divToComplete, data, message, display, callback, sync ){

					divToComplete.innerHTML = '<div style="margin:10px 0px; margin-left:-5px;"></div><div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /><br />' + message + '</div>';
					
					var xhr_object = null;
					if(window.XMLHttpRequest) // Firefox
					   xhr_object = new XMLHttpRequest();
					else if(window.ActiveXObject) // Internet Explorer
					   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
					else { // XMLHttpRequest non supporté par le navigateur
					   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
					   return;
					}
					// CONTROLE DE L'ETAT DE LA REQUETE
					// CHAQUE CHANGEMENT D'ETAT AFFICHE UNE LIGNE DANS NOTRE DIV
					xhr_object.onreadystatechange = function()
					{
						if(xhr_object.readyState == 4){
							 // ON CONTROLE LE STATUS (ERREUR 404, ETC)
							if(xhr_object.status == 200){
								//alert("Réponse Ok");
								if(display == true){
									divToComplete.innerHTML = xhr_object.responseText ;
								}
								if(callback != null){
									eval(callback);
								}
							}else{
									divToComplete.innerHTML = '<div style="text-align:center;padding:5px;">Impossible de trouver une correspondance !</div>' ;
							}
								
						}
					}; 
					xhr_object.open("POST", location, sync);
					// ne pas oublier ça pour le post
					xhr_object.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
					xhr_object.send( data );
				}	
			-->
			</script>
<div id="globalMainContent">
	<div class="mainContent">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">
                	Gestion des flux RSS
                	<a name="topPage"></a>
                </p>
            </div>
            <form name="commitRss" method="post" id="commitRss" action="<?php echo $GLOBAL_START_URL?>/marketing/rss/feed_rss.php">
            <div id="container">
					<ul class="menu">
						<li class="item">
							<div class="right"></div><div class="left"></div>
							<div class="centered"><a href="#actifs">Flux actifs</a></div>
						</li>
						<li class="item">
							<div class="right"></div><div class="left"></div>
							<div class="centered"><a href="#inactifs">Flux inactifs</a></div>
						</li>
					</ul>
            
            
            <!-- FLUX RSS ACTIFS -->
            
            <div class="subContent" id="actifs">
           		<div class="tableContainer">
           			<table class="dataTable dateChoiceTable">
		           			<thead>
		           			<tr>
		           				<td colspan="6" style="font-weight:bold; font-size:14px; text-align:center;">Liste des flux RSS Actifs</td>
		           			</tr>
		           			<tr>
		           				<td class="titleRss" style="width:30px;">
		           					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="Delete Image"/>
		           				</td>
		           				<td>Editer</td> 
		           				<td class="titleRss" style="width:400px;">Titre</td>
		           				<td class="titleRss">Liens</td>
		           				<td class="titleRss" style="width:115px;">Date de publication</td>
		           				<td class="titleRss">Désactiver</td>
		           			</tr>
		           			</thead>
		           			<tbody>
		           				<?php
		           				
		           				if( $lstFluxActifs != false ){
		           					$comptActifs = 0;
		           					while( !$lstFluxActifs->EOF() ){
			           					?>
				           				<tr>
						           			<td class="lefterCol center"><input type="checkbox" id="deleteRSS<?=$comptActifs?>" name="deleteRSS<?=$comptActifs?>" value="<?php echo $lstFluxActifs->fields( "id_rss" ) ?>" onclick="javascript: if( document.getElementById('unactiveRSSItem<?=$comptActifs?>').checked==true ){ document.getElementById('unactiveRSSItem<?=$comptActifs?>').checked = false ; }" /></td>
						           			<td><img src="<?=$GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" style="cursor:pointer;" onclick="javascript : editRssItem(<?php echo $lstFluxActifs->fields( "id_rss" ) ?>);return false;"/></td>
						           			<td><?php echo $lstFluxActifs->fields( "title_rss" ) ?></td>
						           			<td><a href="<?php echo $lstFluxActifs->fields( "link_rss" ) ?>" onclick="javascript: window.open(this.href);return false;"><img src="<?=$GLOBAL_START_URL ?>/images/back_office/content/search.png"/></a></td>
						           			<td><?php echo $lstFluxActifs->fields( "pubDate_rss" ) ?></td>
						           			<td class="righterCol center">
						           				<input type="checkbox" id="unactiveRSSItem<?=$comptActifs?>" name="unactiveRSSItem<?=$comptActifs?>" value="<?php echo $lstFluxActifs->fields( "id_rss" ) ?>" onclick="javascript: if( document.getElementById('deleteRSS<?=$comptActifs?>').checked==true ){ document.getElementById('deleteRSS<?=$comptActifs?>').checked = false ; }" />
						           			</td>
						           		</tr>
				           				<?php
				           				$comptActifs++;
				           				$lstFluxActifs->MoveNext();
			           				}
		           				}
		           				else{
		           					?>
		           					<tr><td colspan="6" style="font-weight:bold; text-align:center; color: red; border:none; font-size:14px;">Aucun flux RSS actifs</td></tr>
		           					<?php
		           				}
		           				?>
		           			</tbody>
	           			</table>
           		</div>
            	<div class="submitButtonContainer">
                    	<input type="submit" class="blueButton" value="Valider" onclick="" />
                </div>
            </div>
            
            <!-- FLUX RSS INACTIFS -->
            <div class="subContent" id="inactifs">
           		<div class="tableContainer">
           			<table class="dataTable dateChoiceTable">
		           			<thead>
		           			<tr>
		           				<td colspan="6" style="font-weight:bold; font-size:14px; text-align:center;">Liste des flux RSS Inactifs</td>
		           			</tr>
		           			<tr>
		           				<td class="titleRss" style="width:30px;">
		           					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="Delete Image"/>
		           				</td>
		           				<td>Editer</td> 
		           				<td class="titleRss" style="width:400px;">Titre</td>
		           				<td class="titleRss">Liens</td>
		           				<td class="titleRss" style="width:115px;">Date de publication</td>
		           				<td class="titleRss">Activer</td>
		           			</tr>
		           			</thead>
		           			<tbody>
		           				<?php
		           				
		           				if( $lstFluxInactifs != false ){
		           					$comptIna = 0;
		           					while( !$lstFluxInactifs->EOF() ){
			           					?>
				           				<tr>
						           			<td class="lefterCol center"><input type="checkbox" id="deleteUnacRSS<?=$comptIna?>" name="deleteUnacRSS<?=$comptIna?>" value="<?php echo $lstFluxInactifs->fields( "id_rss" ) ?>" onclick="javascript: if( document.getElementById('activeRSSItem<?=$comptIna?>').checked==true ){ document.getElementById('activeRSSItem<?=$comptIna?>').checked = false ; }"/></td>
						           			<td><img src="<?=$GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" style="cursor:pointer;" onclick="javascript: editRssItem(<?php echo $lstFluxInactifs->fields( "id_rss" ) ?>); return false;"/></td>
						           			<td><?php echo $lstFluxInactifs->fields( "title_rss" ) ?></td>
						           			<td><a href="<?php echo $lstFluxInactifs->fields( "link_rss" ) ?>" onclick="javascript: window.open(this.href);return false;"><img src="<?=$GLOBAL_START_URL ?>/images/back_office/content/search.png"/></a></td>
						           			<td><?php echo $lstFluxInactifs->fields( "pubDate_rss" ) ?></td>
						           			<td class="righterCol center">
						           				<input type="checkbox" id="activeRSSItem<?=$comptIna?>" name="activeRSSItem<?=$comptIna?>" value="<?php echo $lstFluxInactifs->fields( "id_rss" ) ?>" onclick="javascript: if( document.getElementById('deleteUnacRSS<?=$comptIna?>').checked==true ){ document.getElementById('deleteUnacRSS<?=$comptIna?>').checked = false ; }" />
						           			</td>
						           		</tr>
				           				<?php
				           				$comptIna++;
				           				$lstFluxInactifs->MoveNext();
			           				}
		           				}
		           				else{
		           					?>
		           					<tr><td colspan="6" style="font-weight:bold; text-align:center; color: red; border:none; font-size:14px;">Aucun flux RSS inactifs</td></tr>
		           					<?php
		           				}
		           				?>
		           			</tbody>
	           			</table>
           		</div>
            	<div class="submitButtonContainer">
            			<input type="hidden" name="nbrRssIna" id="nbrRssIna" value="<?=$comptIna?>" />
            			<input type="hidden" name="nbrRssAct" id="nbrRssAct" value="<?=$comptActifs?>" />
                    	<input type="submit" class="blueButton" value="Valider" onclick=""/>
                </div>
            </div>
         
        </div> <!--Contener -->
        </form>
       
        
        </div>  
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content" style="padding-top:5px;padding-bottom:5px;padding-left:10px; padding-right:10px;">
	            <p style="padding:10px 0px 0px 0px;text-align:left;" id="zoneActivate">
	            	<form id="useRss" method="post" action="<?php echo $GLOBAL_START_URL ?>/marketing/rss/feed_rss.php">
	            		<?php
	            		if ($isActivated == true){
	            			?>
		            			<input type="hidden" value="0" name="activate" id="activate"/>
		            			<input type="submit" class="blueButton" value="Désactivé les flux RSS" onclick="javascript: doActivate(); return true ;"/>
	            			<?php
	            		}
	            		else{
	            			?>
		            			<input type="hidden" value="1" name="activate" id="activate"/>
		            			<input type="submit" class="blueButton" value="Activé les flux RSS" onclick="javascript: doActivate(); return true ;"/>
	            			<?php
	            		}
	            		?>
	            	</form>
	            </p>
	            <p style="padding:10px 0px 0px 0px;text-align:left;"><input type="button" class="blueButton" value="Créer un nouveau flux" onclick="javascript: document.location = '<?=$GLOBAL_START_URL?>/marketing/rss/feed_rss.php' ;"/></p>
	            <div id="messZone" style="margin-top:10px; width:90%; margin-left:auto;margin-right:auto;">
	            	<?php
	            	if($mess != ""){
	            		echo $mess ;
	            	}
	            	?>
	            </div>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>
   	
   	<div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        
        <div class="content" id="loadZone">
        	<div class="headTitle">
                <p class="title">Ajouter un fux RSS</p>
				<div class="rightContainer"></div>
            </div>
            <div class="subContent">
				<div class="tableHeaderLeft">
					<a href="#bottomPage" class="goUpOrDown">
						Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a>
				</div>
				<div class="tableHeaderRight">

				</div>
            	<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">

                	<table class="dataTable resultTable">
	                	<tbody>
	                		<tr>
	                			<th class="lefterCol">Titre du flux</th>
	                			<td>
	                				<input type="text" class="textInput" name="tittle_rss" id="tittle_rss" value="" />
	                			</td>
	                			<th>Date de publication</th>
	                			<td class="righterCol">
	                				<input class="calendarInput" type="text" name="pubDate_rss" id="pubDate_rss" />
	                				<?php echo DHTMLCalendar::calendar( "pubDate_rss" ) ?>
	                			</td>
	                		</tr>
	                		<tr>
	                			<th>Lien Html</th>
	                			<td class="righterCol">
	                				<input type="text" class="textInput" name="link_rss" id="link_rss" value="<?php echo $GLOBAL_START_URL ?>" />
	                			</td>
	                			<th>Actif</th>
	                			<td class="righterCol">
	                				<input type="checkbox" id="active_rss" name="active_rss" />
	                			</td>
	                		</tr>
	                		<tr>
	                			<th>Média</th>
	                			<td class="righterCol">
	                				<input type="text" class="textInput" name="enclosure" id="enclosure" value="" />
	                			</td>
	                			<th>Type de média</th>
	                			<td class="righterCol">
	                				<select name="enclosure_type" id="enclosure_type">
										<option value="">-</option>
										<option value="audio/mpeg">Audio/Mpeg</option>
										<option value="image/jpeg">Image/Jpeg</option>
										<option value="text">Text</option>
										<option value="video/wmw">Video/Wmw</option>
										<option value="video/mpeg">Video/Mpeg</option>
									</select>
	                			</td>
	                		</tr>
	                		<tr>
	                			<td colspan="4">&nbsp;</td>
	                		</tr>
	                		<tr>
	                			<th>Description</th>
	                			<td colspan="3" class="righterCol">
	                				<textarea name="desc_rss" id="desc_rss" rows="10" style="width:100%;"></textarea>
	                			</td>
	                		</tr>
	                	</tbody>
                	</table>
                	<div class="submitButtonContainer">
                		<input style="margin-top:10px;" type="button" class="blueButton" value="Créer" onclick="javascript: createItem();" />
               		</div>
               		
                	
                </div>
                <a href="#topPage" class="goUpOrDown">
                	Aller en haut de page
                    <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                    <a name="bottomPage"></a>
                </a>
           	</div>
   		</div><!-- content -->
   		
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
</div><!-- globalMainContent -->
<?php
include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>