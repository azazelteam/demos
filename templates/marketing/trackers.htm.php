<?php
include_once( dirname( __FILE__ ) . "/../../config/init.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

$Title = "Suivie des process commerciaux";
//$js = array();
//$js[] = "jquery/jquery.qtip.min.js";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
//include_once( "$GLOBAL_START_PATH/js/jquery/jquery.qtip.min.js" );
?>

<script type="text/javascript" src="<?=$GLOBAL_START_URL?>/js/jquery/jquery.qtip.min.js"></script>

	<script type="text/javascript">
	<!--
	    $(document).ready(function(){
	        
	        /* Slide des infos */
	        $('#devis_button').css('cursor','pointer').click(function(){
		       $('#infosPlusDevis').slideToggle('slow');
		    });
			
			$('#basket_button').css('cursor','pointer').click(function(){
		       $('#infosPlusCommandes').slideToggle('slow');
		    });
		    
		    $('#divers_button').css('cursor','pointer').click(function(){
		       $('#infosPlusPDF').slideToggle('slow');
		    });
			
			
	        $("#helpPopupEstimate").qtip({
	            adjust: {
	                mouse: true,
	                screen: true
	            },
	            content: 'Survol de la zone<br />Mes devis.',
	            position: {
	                corner: {
	                    target: "rightMiddle",
	                    tooltip: "leftMiddle"
	                }
	            },
	            style: {
	                background: "#FFFFFF",
	                border: {
	                    color: "#EB6A0A",
	                    radius: 5
	                },
	                fontFamily: "Arial,Helvetica,sans-serif",
	                fontSize: "13px",
	                tip: true
	            }
	        });
	    });
	    

			    
	-->
	</script>

<style type="text/css">

</style>

<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">
                	Suivi du process commercial
                	<a name="topPage"></a>
                </p>
            </div>
            <div class="subContent" style="margin-top: 20px;">
            	<form method="post" action="<?php echo $GLOBAL_START_URL ?>/marketing/trackers/trackers.php" id="SearchForm">
            	<table class="dataTable dateChoiceTable">
           			<thead>
           			<tr>
           				
           				<!--<th>Pour le / Entre le</th>-->
                        <th>Pour le</th>
           				<td><input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php if(isset($_POST["sp_bt_start"]) && $_POST["sp_bt_start"] != ""){ echo $_POST["sp_bt_start"] ;}else{echo date('d-m-Y');} ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_start" ) ?></td>
           				 
           				<!--<th>Et le</th>
           				<td><input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php if(isset($_POST["sp_bt_end"]) && $_POST["sp_bt_end"] != ""){ echo $_POST["sp_bt_end"] ;}else{echo date('d-m-Y');} ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_end" ) ?></td>
           				-->
           			</tr>
           			<tr>
           				<td colspan="4" style="border:none;"><input type="submit" value="Valider" class="blueButton" style="float:right; margin-right:15px; margin-top:10px;"/></td>
           			</tr>
           			</thead>
       			</table>
       			</form>
            </div>
				 
        </div>  
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content" style="padding-top:5px;padding-left:10px; padding-right:10px; text-align:center;">
	            <p style="text-align:left; margin-top:0px;; margin-bottom:0px;">
	            	Les statistiques sont mise à jour toutes les heures. Cependant vous pouvez lancer une mise à jour manuelle en cliquant sur le bouton ci-dessous.
	            	<form id="updateTrackers" method="post" action="<?php echo $GLOBAL_START_URL ?>/marketing/trackers/trackers.php">
							<input type="hidden" value="1" name="updateTrackers"/>
							<input type="submit" value="Rafraichir" class="blueButton"/>
					</form>
	            </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        
    </div>
   	
  	<?php
    if(isset($search) && ($search == 1 || $search == 2)){
    ?>
<div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Résultat de la recherche <?php if($search==1){ echo 'pour la journée du '.$_POST["sp_bt_start"] ; }else{ echo 'pour la période du '.$_POST["sp_bt_start"].' au '.$_POST["sp_bt_end"] ;} ?></p>
				<div class="rightContainer"></div>
            </div>
            <div class="subContent">
				<div class="tableHeaderLeft"></div>
				<div class="tableHeaderRight"></div>
            	
            	<div class="resultTableContainer clear">
				<!-- Devis -->
				<div class="headTitle" style="margin-top:30px;">
	                <p class="title">Produits ajoutés au panier depuis une fiche client</p>
					<div class="rightContainer"></div>
            	</div>
            	
            	<!-- NEW PART -->
            	
            	<div style="width:100%; margin-top:10px;">
            		<strong><?php echo $sumFull ?></strong> Cliques "Demander un devis" / "Ajouter à ma commande" soit <strong><?php echo $sumUnique ?></strong> cliques uniques.<br />
            		<strong><?php echo $nbrClickAddOrder ?></strong> Cliques "Ajouter à ma commande" soit <strong><?php echo $nbrUniqueClickAddOrder ?></strong> cliques uniques.<br />
            		<strong><?php echo $nbrClickAddEstimate ?></strong> Cliques "Demander un devis" soit <strong><?php echo $nbrUniqueClickAddEstimate ?></strong> cliques uniques.
	           	</div>
           		<?php 
            		GetGlobalTrackersToolTipBox( $idPannierGlobal, null, null );
            	?>

            	<!-- END NEW PART -->

       					
       			<!-- Divers / PDF -->
       			<div class="headTitle" style="margin-top:30px;">
	                <p class="title">Suivi de process divers liés à la fiche produit</p>
					<div class="rightContainer"></div>
            	</div>
       			
       			<table class="dataTable dateChoiceTable" style="margin-top:30px;">
           			<thead>
           			<tr>
           				<td style="border:none; text-align:center;width:25%;"></td>
           				<th style="text-align:center;width:25%;">Imp. PDF</th>
           				<th style="text-align:center;width:25%;">Ajout list perso.</th>
           				<th style="text-align:center;width:25%;"><div id="helpPopupCompa">Comparateur</div></th>
           				<td style="border:none;">&nbsp;</td>
           			</tr>
           			</thead>
           			<tbody>
           				<tr>   
          					<td class="lefterCol" style="font-weight:bold;"><div id="helpAccounts">Nombre</div></td>
           					<td style="text-align:center;"><?php if( $nbrPDF != false ){ echo $nbrPDF ; }else{echo "N/A" ;} ; ?></td>
           					<td style="text-align:center;"><?php if( $nbrAddLstPerso != false ){ echo $nbrAddLstPerso ; }else{echo "N/A" ;} ; ?></td>
           					<td style="text-align:center;" class="righterCol"><?php if( $nbrComparateur != false ){ echo $nbrComparateur ; }else{ echo "N/A" ;} ; ?></td>
           					<td>
       							<a href="#pdf" id="divers_button">
       							<img class="helpicon" src="<?=$GLOBAL_START_URL?>/images/back_office/content/help.png" alt="" border="0" /> 
           					</td>
           				</tr>
           			</tbody>
       			</table>
       			<a name="pdf"></a>
       			<!-- tableau de detail pour les PDF -->
       			<div id="infosPlusPDF" style="display:none;">
       			<br /><br />
       			<?php 
            		GetPDFTrackersToolTipBox( $idPDF, null, null );
            	?>
       			</div>
       			<!-- Mon Compte -->
       			<div class="headTitle" style="margin-top:30px;">
	                <p class="title">Suivi du process "Mon Compte"</p>
					<div class="rightContainer"></div>
            	</div>
      			
      			<table class="dataTable resultTable" style="margin-top:30px;">
           			<thead>
           			<tr>
           				<td style="border:none;">&nbsp;</td>
           				<th style="text-align:center;">Mon compte</th>
           				<th style="text-align:center;">Suivi devis</th>
           				<th style="text-align:center;">Suivi commandes</th>
           				<th style="text-align:center;">Modification "Mes Informations"</th>
           				<th style="text-align:center;">Modification M.D.P.</th>
           			</tr>
           			</thead>
           			<tbody>
           				<tr>   
          					<td class="lefterCol" style="font-weight:bold;">Nombre</td>
           					<td style="text-align:center;"><?php if( $click_bt_account != false ){ echo $click_bt_account ; }else{echo "N/A" ;} ; ?></td>
           					<td style="text-align:center;"><?php if( $click_follow_estimate != false ){ echo $click_follow_estimate ; }else{echo "N/A" ;} ; ?></td>
           					<td style="text-align:center;"><?php if( $click_follow_order != false ){ echo $click_follow_order ; }else{echo "N/A" ;} ; ?></td>
           					<td style="text-align:center;"><?php if( $click_change_infos != false ){ echo $click_change_infos ; }else{echo "N/A" ;} ; ?></td>
           					<td style="text-align:center;" class="righterCol"><?php if( $click_change_pwd != false ){ echo $click_change_pwd ; }else{echo "N/A" ;} ; ?></td>
           				</tr>
           			</tbody>
       			</table>
       			
       			
               </div>
        	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>  <!--mainContent fullWidthContent -->
    <?php
    }
    ?>
</div><!-- globalMainContent -->

<?php
include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>