<?php

// Récupére la session pour uploadify
if( isset( $_REQUEST['sid_uploadify'] ) )
{
    @session_id(  $_REQUEST['sid_uploadify']   );
    @session_start();
}

include_once( dirname( __FILE__ ) . "/../../config/init.php" );
include_once( dirname( __FILE__ ) . "/../../objects/Salesman.php" );

if( !User::getInstance()->getId() )
	exit( "Session expirée" );

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload photo */

if( isset( $_FILES[ "photo" ] ) ){
	
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . "/images/users/";
	$ext = strtolower( substr( $_FILES[ "photo" ][ "name" ] , strrpos( $_FILES[ "photo" ][ "name" ] , "." ) + 1 ) );
	$basename = intval( $_REQUEST[ "iduser" ] );

	$files = glob( $targetPath . $basename . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	foreach( $files as $file )
		unlink( $file );

	include_once( dirname( __FILE__ ) . "/../../objects/ImageUploader.php" );
	
	$uploader = new ImageUploader( "photo", str_replace( "//", "/", $targetPath ), $basename . "." . $ext );
	$uploader->setMaxWidth( 120 );
	$uploader->setMaxHeight( 120 );
	$uploader->setResizeImage();
	$uploader->upload();

	exit( "/images/users/$basename.$ext?" . md5_file( str_replace( "//", "/", $targetPath ) . $basename . "." . $ext ) );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload signature */

if( isset( $_FILES[ "signature" ] ) ){
	
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . "/images/users/signatures/";
	$ext = strtolower( substr( $_FILES[ "signature" ][ "name" ] , strrpos( $_FILES[ "signature" ][ "name" ] , "." ) + 1 ) );
	$basename = intval( $_REQUEST[ "iduser" ] );

	$files = glob( $targetPath . $basename . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	foreach( $files as $file )
		unlink( $file );

	include_once( dirname( __FILE__ ) . "/../../objects/ImageUploader.php" );
	
	$uploader = new ImageUploader( "signature", str_replace( "//", "/", $targetPath ), $basename . "." . $ext );
	$uploader->setMaxWidth( 120 );
	$uploader->setMaxHeight( 85 );
	$uploader->setResizeImage();
	$uploader->upload();

	exit( "/images/users/signatures/$basename.$ext?" . md5_file( str_replace( "//", "/", $targetPath ) . $basename . "." . $ext ) );

}

/* ------------------------------------------------------------------------------------------------------------- */


	
/* ------------------------------------------------------------------------------------------------------------- */
/* éditer un utilisateur existant */

if( isset( $_REQUEST[ "edit" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	editUser( new Salesman( intval( $_REQUEST[ "iduser" ] ) ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/* disponibilité du login */

if( isset( $_REQUEST[ "check_login" ] ) ){
	
	if( DBUtil::query( "SELECT 1 FROM `user` WHERE `login` LIKE " . DBUtil::quote( $_REQUEST[ "login" ] ) . " LIMIT 1" )->RecordCount() )
		exit( "0" );

	exit( "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/* congés d'une période données */

if( isset( $_REQUEST[ "get_holidays" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayHolidays( new Salesman( intval( $_REQUEST[ "iduser" ] ) ), intval( $_REQUEST[ "year" ] ), $_REQUEST[ "month" ] );
	exit();
		
}

/* ------------------------------------------------------------------------------------------------------------- */
/* modification des congés */

if( isset( $_REQUEST[ "update_holidays" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	updateHolidays( new Salesman( intval( $_REQUEST[ "iduser" ] ) ), intval( $_REQUEST[ "year" ] ), $_REQUEST[ "month" ], explode( ",", $_REQUEST[ "holidays" ] ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/* salaires et primes d'une période donnée */

if( isset( $_REQUEST[ "salary" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displaySalary( new Salesman( intval( $_REQUEST[ "iduser" ] ) ), intval( $_REQUEST[ "year" ] ), $_REQUEST[ "month" ] );
	exit();
		
}

/* ------------------------------------------------------------------------------------------------------------- */

?>
<script type="text/javascript">
/* <![CDATA[ */
 
 	/* ---------------------------------------------------------------------------------------------------- */
 	
 	function filterUserList(){

 	 	var searchString = $( "#search" ).val().toLowerCase();

 	 	
	 	 $( "#CharA li" ).each( function( i ){
	 		
 	 		if( !searchString.length || $( this ).text().toLowerCase().indexOf( searchString ) != -1 )
 	 	 			$( this ).css( "display", "block" );
 	 		else	$( this ).css( "display", "none" );
 	 	 		
 	 	});
 	 	
	}

 	/* ---------------------------------------------------------------------------------------------------- */
 	
	function editUser( iduser ){

		$.ajax({
		 	
			url: "/templates/secure/users.php?edit",
			async: true,
			type: "GET",
			data: "iduser=" + iduser,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouvel utilisateur" ); },
		 	success: function( responseText ){

				$( "#User" ).html( responseText );
				
			}

		});

	}

	/* ---------------------------------------------------------------------------------------------------- */
 	
	function createUser(){

		var login = prompt( "Veuillez saisir l'identifiant du nouvel utilisateur", "identifiant");

		if( login == null || !login.length )
			return;

		var reg = new RegExp( "^[0-9a-zA-Z_@]+$", "g" );

		if( !reg.test( login ) ){

			alert( "L'identifiant ne doit contenir que des caractères alphanumériques. Les caractères '@' et '_' sont également autorisés." );
			createUser();
			
			return;
			
		}
		
		$.ajax({
		 	
			url: "/templates/secure/users.php?check_login",
			async: true,
			type: "GET",
			data: "login=" + escape( login ),
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouvel utilisateur" ); },
		 	success: function( responseText ){

				if( responseText == "1" )
					document.location = "/secure/users.php?create&login=" + escape( login );
				else{

					alert( "Cet identifiant n'est pas disponible, merci de bien vouloir en choisir un autre" );
					createUser();

				}
				
			}

		});

	}
	
	/* ---------------------------------------------------------------------------------------------------- */
 	
 /* ]]> */
</script>
<div class="centerMax">
	<div class="blocLeftSupplier">
		<input type="text" name="search" id="search" onkeyup="filterUserList();" /> <!-- A DEV : Tri des users -->
		<div class="spacer"></div>
		
		<ul class="addSupplier">
			<li><a href="#" onclick="createUser(); return false;">Créer un employé</a></li> <!-- A DEV : Création d'un user -->
		</ul>
			
		<ul id="CharA" class="listSupplier">
			<li class="letter">Employés actuels</li>
			<?php
				$listUsers = getUsers(0);
				foreach( $listUsers as $user ){
					
					$label = strlen( $user[ "iduser" ] . $user["firstname"] . $user["lastname"] ) ? $user[ "iduser" ] ." - ".$user["firstname"]." ".$user["lastname"] : $user[ "login" ];
					echo "<li onclick=\"editUser(". $user[ "iduser" ] . ");\">$label</li>";
					
				}
			?> <!-- A DEV : Modifier un user -->
			
			<li class="letter">Anciens employés</li>
			<?php
				$listUsers = getUsers(1);
				foreach( $listUsers as $user ){
					$label = strlen( $user["firstname"] . $user["lastname"] ) ? $user["firstname"]." ".$user["lastname"] : $user[ "login" ];
					echo "<li onclick=\"editUser(". $user[ "iduser" ] . ");\">$label</li>";
				}
			?> <!-- A DEV : Modifier un user -->
		</ul>
	</div>
	
	 <!-- A DEV : Infos user -->
	<div class="blocRightSupplier">
		<div id="User" class="contentResult">
		<?php
		
			if( !isset( $_REQUEST[ "iduser" ] ) )
					listUsers( $listUsers ) && exit();
			else 	editUser( new Salesman( intval( $_REQUEST[ "iduser" ] ) ) );
			
		?>	
		</div>
	</div>
</div>
<?php

/* ------------------------------------------------------------------------------------------------------------- */

function listUsers( $users ){
	
	?>
	<!-- Titre -->
	<h1 class="titleSearch">
		<span class="textTitle">Gestion des employés</span>
		<div class="spacer"></div>
	</h1>
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<!-- Content -->
	<div class="blocResult"><div class="blocMarge">
		<ul id="listUsers">
		<?php
		
			$listUsers = getUsers(0);
			foreach( $listUsers as $iduser => $user ){
				
				?>
				<li onclick="editUser(<?php echo $iduser; ?>);">
					<img src="<?php echo $user[ "photo" ]; ?>" alt="" title="" width="120" style="border:1px solid #eee;" />
					<br />
					<strong><?php echo htmlentities( $user[ "iduser" ] ); ?> - <?php echo htmlentities( $user[ "firstname" ]." ".$user[ "lastname" ] ); ?></strong><br/>
					<em><?php echo htmlentities( $user[ "job" ] ); ?></em>
				</li>
				<?php
				
			}
			
		?><!-- A DEV : Modifier un user -->
		</ul>
		<div class="spacer"></div>
	</div></div>	
	<div class="spacer"></div>
	<?php
			
}

/* ------------------------------------------------------------------------------------------------------------- */

function editUser( Salesman $salesman ){
	
	include_once( dirname( __FILE__ ) . "/../../objects/Util.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ----------------------------------------------------------------------------------- */
		
		$( document ).ready( function(){

			$( "#UserForm" ).ajaxForm({ 

				beforeSubmit: preSubmitCallback, 
				success: postSubmitCallback 

			});

		});

		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
		}

		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 

			if( responseText != "1" ){

				alert( "Impossible de sauvegarder les modifications : " + responseText );
				return;

			}

			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
			$.blockUI.defaults.css.fontSize = '12px';
	    	$.growlUI( '', "Modifications sauvegardées" );
	    	
		}
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	
	<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>
	
	<form action="/secure/users.php?update&amp;iduser=<?php echo $salesman->getId(); ?>" method="post" id="UserForm" enctype="multipart/form-data">
		<!-- Titre -->
		<h1 class="titleSearch">
			<span class="textTitle"> 
			
				<?php echo htmlentities( $salesman->get( "iduser" ) . " - " .$salesman->get( "lastname" ) . " " . $salesman->get( "firstname" ) ); ?>
				(<em><?php echo $salesman->get( "hasbeen" ) ? "Ancien employé" : "Employé en activité"; ?></em>)
			</span>
			<div class="spacer"></div>
		</h1>
		
		<!-- Content -->
		<div class="blocResult">
			<div class="blocMarge">
				<div class="blocMultiple blocMLeft" style="width:160px;">
					<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
					<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
					<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
					<script type="text/javascript">
					/* <![CDATA[ */
					
						$( document ).ready( function(){ 
				
							$( "#photo" ).uploadify({
								
								'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
								'script'		: '/templates/secure/users.php',
								'folder'		: '',
								'scriptData'	: { 'iduser': '<?php echo $salesman->getId(); ?>' , 'sid_uploadify' : '<?php echo session_id();?>' },
								'fileDataName'	: 'photo',
								'buttonText' 	: "",
								'buttonImg' 	: "<?php 
								
									if( $photo = $salesman->getPhotoURI() )
										echo $photo;
									else echo $salesman->get( "gender" ) < 2 ? "/images/back_office/content/no-pict-m.jpg" : "/images/back_office/content/no-pict-g.jpg"; 
									
								?>", 
								'width'			: "120",
								'height'		: "120",
								'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
								'fileDesc'		: "Photo",
								'auto'           : true,
								'multi'          : false,
								'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
								'onComplete' 	: function( event, queueID, fileObj, response, data ){
				
				 					$( "#photo" ).uploadifySettings( "buttonImg", response );
									
								}
				
							});

							$( "#signature" ).uploadify({
								
								'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
								'script'		: '/templates/secure/users.php',
								'folder'		: '',
								'scriptData'	: { 'iduser': '<?php echo $salesman->getId(); ?>' , 'sid_uploadify' : '<?php echo session_id();?>' },
								'fileDataName'	: 'signature',
								'buttonText' 	: "",
								'buttonImg' 	: "<?php 
								
									if( $signature = $salesman->getSignatureURI() )
										echo $signature;
									else echo "/images/back_office/content/no-signature.jpg";
									
								?>", 
								'width'			: "120",
								'height'		: "85",
								'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
								'fileDesc'		: "Signature",
								'auto'           : true,
								'multi'          : false,
								'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
								'onComplete' 	: function( event, queueID, fileObj, response, data ){
				
				 					$( "#signature" ).uploadifySettings( "buttonImg", response );
									
								}
				
							});

						});
				
					/* ]]> */
					</script>
					<p class="alignCenter">
						<input type="file" id="photo" value="" />
					</p>			
					<p class="alignCenter">
						<input type="file" id="signature" value="" />
					</p>
				</div>
	
				<div class="blocMultiple blocMLeft" style="width:310px;">
					<label><span>Titre <strong class="asterix">*</strong></span></label>
					<div class="simpleSelect">
						<select name="gender">
							<option value="0"<?php if( $salesman->get( "gender" ) == 0 ) echo " selected=\"selected\""; ?>></option>	
							<option value="2"<?php if( $salesman->get( "gender" ) == 2 ) echo " selected=\"selected\""; ?>>Mme</option>
							<option value="1"<?php if( $salesman->get( "gender" ) == 1 ) echo " selected=\"selected\""; ?>>M.</option>
							<option value="3"<?php if( $salesman->get( "gender" ) == 3 ) echo " selected=\"selected\""; ?>>Mlle</option>
						</select>
					</div>
					<div class="spacer"></div>
				
					<label><span>Nom <strong class="asterix">*</strong></span></label>
					<input class="textInput inputSupplier" name="lastname" id="lastname" value="<?php echo htmlentities( $salesman->get( "lastname" ) ); ?>" type="text" /> 
					<div class="spacer"></div>
					
					<label><span>Prénom <strong class="asterix">*</strong></span></label>
					<input class="textInput inputSupplier" name="firstname" id="firstname" value="<?php echo htmlentities( $salesman->get( "firstname" ) ); ?>" type="text" /> 
					<div class="spacer"></div>
	
					<div class="marginTop">
						<label><span>Identifiant <strong class="asterix">*</strong></span></label>
						<input class="textInput inputSupplier" name="login" id="login" value="<?php echo htmlentities( $salesman->get( "login" ) ); ?>" type="text" /> 
						<div class="spacer"></div>
						
						<label><span>Mot de passe</span></label>
						<input class="textInput inputSupplier" name="password" id="password" value="" type="password" /> 
						<div class="spacer"></div>
						
						<label><span>Accès admin</label>
						<div class="selectSupplier">
							<p>
								<span><input type="radio" name="admin" value="1"<?php if(  $salesman->get( "admin" ) ) echo " checked=\"checked\""; ?> /> oui</span>
								<span><input type="radio" name="admin" value="0"<?php if( !$salesman->get( "admin" ) ) echo " checked=\"checked\""; ?> /> non</span>
							</p>
						</div>
						<?php if($akilae_comext ==1){ ?>
						<div class="spacer"></div>
						<label><span>Commercial ext.</label>
						<div class="selectSupplier">
							<p>
								<span><input type="radio" name="salesman_user" value="1"<?php if(  $salesman->get( "salesman_user" ) ) echo " checked=\"checked\""; ?> /> oui</span>
								<span><input type="radio" name="salesman_user" value="0"<?php if( !$salesman->get( "salesman_user" ) ) echo " checked=\"checked\""; ?> /> non</span>
							</p>
						</div>
						<?php } ?>
						<div class="spacer"></div>
						
					</div>
					<div class="spacer"></div> 	
				
				</div><!-- blocMultiple -->
				
				<div class="blocMultiple blocMRight">
					<label><span>Email <strong class="asterix">*</strong></span></label>							
					<input class="textInput" name="email" id="email" value="<?php echo htmlentities( $salesman->get( "email" ) ); ?>" type="text" />
					<div class="spacer"></div>
					
					<label><span>Téléphone</span></label>
					<input class="textidInput" name="phonenumber" id="phonenumber" value="<?php echo Util::phonenumberFormat( $salesman->get( "phonenumber" ) ); ?>" type="text" /> 
					
					<label class="smallLabel"><span>Fax</span></label>
					<input class="textidInput" name="faxnumber" id="faxnumber" value="<?php echo Util::phonenumberFormat( $salesman->get( "faxnumber" ) ); ?>" type="text" /> 
					<div class="spacer"></div>
					
					<label><span>Adresse <strong class="asterix">*</strong></span></label>							
					<input class="textInput" name="address" id="address" value="<?php echo htmlentities( $salesman->get( "address" ) ); ?>" type="text" /> 
					<div class="spacer"></div>
					
					<label><span>Adresse complémentaire</span></label>							
					<textarea class="textInput" name="address_2" id="address_2"><?php echo htmlentities( $salesman->get( "address_2" ) ); ?></textarea>
					<div class="spacer"></div> 
					
					<label><span>Code postal <strong class="asterix">*</strong></span></label>
					<input class="numberInput" name="zipcode" id="zipcode" value="<?php echo $salesman->get( "zipcode" ); ?>" type="text" /> 
					
					<label class="smallLabel"><span>Ville <strong class="asterix">*</strong></span></label>
					<input class="mediumInput" name="city" id="city" value="<?php echo htmlentities( $salesman->get( "city" ) ); ?>" type="text" />
					<div class="spacer"></div>  
					
					<label><span>Pays</span></label>
					<?php
					
						include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
						XHTMLFactory::createSelectElement( "state", "idstate", "name_1", "name_1", $salesman->get( "idstate" ), 0, "", "name=\"idstate\"");
						
					?>
					<div class="spacer"></div> 
				</div>
				<div class="spacer"></div>		
	
				<script type="text/javascript">
				/* <![CDATA[ */
					$(function() {
						$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
					});
					
				/* ]]> */
				</script>
			
			<div id="container">
				<!-- Onglet -->
				<ul class="menu">
					<li class="item"><a href="#infos-perso"><span>Infos personnelles</span></a></li>	
					<li class="item"><a href="#revenus"><span>Contrat &amp; Revenus</span></a></li>	
					<li class="item"><a href="#conges"><span>Congés</span></a></li>	
				</ul>
				<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px -5px 0pt; width: 101%;">&nbsp;</div>
	       			
	       		<!-- *********************** -->
				<!--    Infos personnelles 	 -->
				<!-- *********************** -->
				<div id="infos-perso">
					<div class="blocMultiple blocMLeft">
						<h2 style="margin-top:0;">Identité</h2>
						
						<label><span>Sexe</span></label>
						<div class="simpleSelect">
							<select name="">
								<option value=""></option>
								<option value="1">M</option>
								<option value="2">F</option>
							</select>
						</div>
						<div class="spacer"></div>
						
						<label><span>Date naissance</span></label>
						<input class="textWithSign" name="birthday" id="birthday" value="<?php echo Util::dateFormat( $salesman->get( "birthday" ), "d/m/Y" ); ?>" type="text" /> 
						<?php DHTMLCalendar::calendar( "birthday" ); ?>
						
						<label class="smallLabel"><span>Lieu</label>
						<input class="textidInput" name="place_of_birth" id="" value="<?php echo htmlentities( $salesman->get( "place_of_birth" ) ); ?>" type="text" /> 
						<div class="spacer"></div>
						
						<label><span>N° Sécurité sociale</span></label>							
						<input class="textInput" name="nir" id="" value="<?php echo htmlentities( $salesman->get( "nir" ) ); ?>" type="text" />
						<div class="spacer"></div>
				
						<label><span>Etat civil</span></label>
						<div class="simpleSelect">
						<select name="marital_status">
							<option value="">-</option>
							<option value="single"<?php if( $salesman->get( "marital_status" ) == "single" ) echo " selected=\"selected\""; ?>>Célibataire</option>
							<option value="married"<?php if( $salesman->get( "marital_status" ) == "married" ) echo " selected=\"selected\""; ?>>Marié</option>	
							<option value="divorced"<?php if( $salesman->get( "marital_status" ) == "divorced" ) echo " selected=\"selected\""; ?>>Divorcé</option>	
							<option value="widowed"<?php if( $salesman->get( "marital_status" ) == "widowed" ) echo " selected=\"selected\""; ?>>Veuf</option>
						</select>
						</div>
						
						<label class="smallLabel"><span>Enfant(s)</span></label>
						<div class="simpleSelect">
							<input class="textidInput" name="childcount" id="" value="<?php if( $salesman->get( "childcount" ) ) echo $salesman->get( "childcount" ); ?>" type="text" />
						</div>
						<div class="spacer"></div><br/>
						
						<h2>Moyen de locomotion</h2>
	
						<label><span>Puissance fiscale</span></label>
						<input class="textWithSign" name="taxable_horsepower" id="" value="<?php if( $salesman->get( "taxable_horsepower" ) ) echo $salesman->get( "taxable_horsepower" ); ?>" type="text" /> 
						<span class="textSigne">&nbsp;CV</span>
						
						<label class="smallLabel"><span>Nb kms</span></label>
						<input class="textWithSign" name="distance" id="" value="<?php if( $salesman->get( "distance" ) ) echo $salesman->get( "distance" ); ?>" type="text" /> 
						<span class="textSigne">&nbsp;kms</span>
						
						<div class="spacer"></div> 
					
					</div>
					<div class="blocMultiple blocMRight">
	
						<h2 style="margin-top:0;">Adresse personnelle	</h2>
						
						<label><span>Email</span></label>							
						<input disabled="disabled" class="textInput" name="" id="" value="" type="text" />
						<div class="spacer"></div>
						
						<label><span>Téléphone</span></label>
						<input disabled="disabled" class="textidInput" name="" id="" value="" type="text" /> 
						
						<label class="smallLabel"><span>Fax</span></label>
						<input disabled="disabled" class="textidInput" name="" id="" value="" type="text" /> 
						<div class="spacer"></div>
						
						
						<label><span>Adresse</span></label>							
						<input disabled="disabled" class="textInput" name="" id="" value="" type="text" /> 
						<div class="spacer"></div>
						
						<label><span>Adresse complémentaire</span></label>							
						<textarea disabled="disabled" class="textInput" name="" id=""></textarea>
						<div class="spacer"></div> 
						
						<label><span>Code postal</span></label>
						<input disabled="disabled" class="numberInput" name="" id="" value="" type="text" /> 
						
						<label class="smallLabel"><span>Ville</span></label>
						<input disabled="disabled" class="mediumInput" name="" id="" value="" type="text" />
						<div class="spacer"></div>  
						
						<label><span>Pays</span></label>
						<select disabled="disabled" name="idstate">
							<option value="">France</option>	
						</select>
						<div class="spacer"></div> 
						
						
					</div>
					<div class="spacer"></div>
					
				</div>
	       			
	       			
	       			<!-- *********** -->
				<!--   Revenus 	 -->
				<!-- *********** -->
				<div id="revenus">
					
					<div id="Salary" class="blocMultiple blocMRight">
						<?php displaySalary( $salesman, date( "Y" ), date( "m" ) ); ?>
					</div>
					<div class="blocMultiple blocMLeft">
						<h2 style="margin-top:0;">Responsabilité</h2>
						
						<label><span>Titre du poste</span></label>							
						<input class="textInput" name="job" id="" value="<?php echo htmlentities( $salesman->get( "job" ) ); ?>" type="text" />
						<div class="spacer"></div>
						
						<label><span>Type contrat</span></label>
						<div class="simpleSelect">
						<select name="contract">
							<option value="">-</option>
							<option value="cdi"<?php if( $salesman->get( "contract" ) == "cdi" ) echo " selected=\"selected\""; ?>>CDI</option>	
							<option value="cdd"<?php if( $salesman->get( "contract" ) == "cdd" ) echo " selected=\"selected\""; ?>>CDD</option>
							<option value="cie"<?php if( $salesman->get( "contract" ) == "cie" ) echo " selected=\"selected\""; ?>>CIE</option>
							<option value="ctt"<?php if( $salesman->get( "contract" ) == "ctt" ) echo " selected=\"selected\""; ?>>CTT</option>
							<option value="indenture"<?php if( $salesman->get( "contract" ) == "indenture" ) echo " selected=\"selected\""; ?>>Contrat Jeune</option>
						</select>
						</div>
						
						<label class="smallLabel"><span>Statut</span></label>
						<div class="simpleSelect">
						<select name="executive">
							<option value="1"<?php if(  $salesman->get( "executive" ) ) echo " selected=\"selected\""; ?>>Cadre</option>	
							<option value="0"<?php if( !$salesman->get( "executive" ) ) echo " selected=\"selected\""; ?>>Non cadre</option>	
						</select>
						</div>
						<div class="spacer"></div>	
						
						<label><span>Mutuelle</span></label>
						<div class="simpleSelect">
						<select name="idstate">
							<option value="1"<?php if(  $salesman->get( "private_complementary_health_insurance" ) ) echo " checked=\"checked\""; ?>>Individuelle</option>	
							<option value="0"<?php if( !$salesman->get( "private_complementary_health_insurance" ) ) echo " checked=\"checked\""; ?>>Professionnelle</option>	
						</select>
						</div>
						<div class="spacer"></div>	
						
						<label><span>Objectif mensuel</span></label>
						<input class="textWithSign" name="objective_month" id="objective_month" value="<?php if( $salesman->get( "objective_month" ) > 0.0 ) echo Util::numberFormat( $salesman->get( "objective_month" ) ); ?>" type="text" /> 
						<span class="textSigne">&nbsp;¤</span>
						
						<label class="smallLabel"><span>Objectif %</span></label>
						<input class="textWithSign" name="objective_percent" id="objective_percent" value="<?php if( $salesman->get( "objective_percent" ) > 0.0 ) echo Util::numberFormat( $salesman->get( "objective_percent" ) ); ?>" type="text" /> 
						<span class="textSigne">&nbsp;%</span>
						<div class="spacer"></div>	
						
						<label><span>Date d'embauche</span></label>
						<input class="textWithSign" name="hire_date" id="hire_date" value="<?php echo Util::dateFormat( $salesman->get( "hire_date" ), "d/m/Y" ); ?>" type="text" /> 
						<?php DHTMLCalendar::calendar( "hire_date" ); ?>
						
						<label class="smallLabel"><span>Date de fin</span></label>
						<input class="textWithSign" name="termination_of_business_date" id="termination_of_business_date" value="<?php echo Util::dateFormat( $salesman->get( "termination_of_business_date" ), "d/m/Y" ); ?>" type="text" /> 
						<?php DHTMLCalendar::calendar( "termination_of_business_date" ); ?>
						<div class="spacer"></div>	
		
					</div>
					<div class="spacer"></div> 
				</div>
				
				
				<!-- *********** -->
				<!--   Congés 	 -->
				<!-- *********** -->
				<div id="conges">
				<?php displayHolidays( $salesman, date( "Y" ), date( "m" ) ); ?>
				</div><!-- congés -->
				
			</div>
			<div class="spacer"></div>	
			
			<input type="submit" value="Sauvegarder" class="inputSave" name="Modify" />
			<div class="spacer"></div> 
	
		</div></div>	
		<div class="spacer"></div>
	</form>
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function displayHolidays( Salesman $salesman, $year, $month ){
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function getHolidays(){

			$( "#HolidaySelectedMonth" ).html( $("#HolidayMonth :selected" ).html() );
			$( "#HolidaySelectedYear" ).html( $("#HolidayYear :selected" ).html() );

			$.ajax({
			 	
				url: "/templates/secure/users.php?get_holidays",
				async: true,
				type: "GET",
				data: "iduser=<?php echo $salesman->getId(); ?>&year=" + $("#HolidayYear" ).val() + "&month=" + $("#HolidayMonth" ).val(),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le calendrier" ); },
			 	success: function( responseText ){

					$( "#conges" ).html( responseText );
					
				}

			});
			
		}
		
	/* ]]> */
	</script>
	<div class="blocMultiple blocMLeft">
		<label><span>Période</span></label>
		<select id="HolidayMonth" style="width:120px !important;" onchange="getHolidays();">
		<?php
	
			$i = 1;
			while( $i < 13 ){
				
				?>
				<option value="<?php echo sprintf( "%02d", $i ); ?>"<?php if( $i == intval( $month ) ) echo " selected=\"selected\""; ?>><?php echo strftime("%B", mktime( 0, 0, 0, $i, 1, 1 ) ); ?></option>
				<?php
				
				$i++;
				
			}
			
		?>
		</select>
		<select id="HolidayYear" style="width:80px !important; margin-left:5px;" onchange="updateCalendar();">
		<?php
		
			$rs = DBUtil::query( "SELECT MIN( SUBSTR( start, 1, 4 ) ) AS previousYear FROM user_holiday" );
			$previousYear = $rs->fields( "previousYear" ) ? intval( $rs->fields( "previousYear" ) ) : date( "Y" );
			
			while( $previousYear <= date( "Y" ) ){
				
				?>
				<option value="<?php echo $previousYear; ?>"<?php if( $previousYear == $year ) echo " selected=\"selected\""; ?>><?php echo $previousYear; ?></option>
				<?php
				
				$previousYear++;
					
			}
			
		?>
		</select>
	</div> 
	<div class="spacer"></div> 

	<div class="blocMultiple blocMLeft">
		<h2 style="margin-top:0;">Congés - <span id="HolidaySelectedMonth"><?php echo strftime("%B", strtotime( "$year-$month" ) ); ?></span> <span id="HolidaySelectedYear"><?php echo strftime("%Y", strtotime( "$year-$month" ) ); ?></span></h2>
		<p>
			<span class="floatleft"><input type="radio" name="type" id="" checked="checked" value="paid_leave" /> Congés payés</span>
			<span style="margin-left:15px;" class="floatleft"><input type="radio" name="type" id="" value="sick_leave" /> Arrêts de maladie</span>
			<span style="margin-left:15px;" class="floatleft"><input type="radio" name="type" id="" value="telecommuting" /> Télétravail</span>	
			<span style="margin-left:15px;" class="floatleft"><input type="radio" name="type" id="" value="undefined" /> Autres</span>	
		</p>
		<?php getCalendar( $salesman, $year, $month ); ?>
	</div>
	<div class="blocMultiple blocMRight">
		<h2 style="margin-top:0;">Liste des évènements - <?php echo strftime("%B %Y", strtotime( "$year-$month" ) ); ?></h2>
		<?php
		
			$rs = DBUtil::query( "
			SELECT start, end, IF( LENGTH( type ), type, 'undefined' ) AS type
			FROM user_holiday 
			WHERE iduser = '" . $salesman->getId() . "'
			AND STRCMP( SUBSTR( end, 1, 7 ), '$year-$month" . "' ) >= 0
			AND STRCMP( SUBSTR( start, 1, 7 ), '$year-$month" . "' ) <= 0
			ORDER BY start ASC, end DESC" );
	
			$userHoliday = array();
			while( !$rs->EOF() ){

				if( !isset( $userHoliday[ $rs->fields( "type" ) ] ) )
					$userHoliday[ $rs->fields( "type" ) ] = array();
					
				array_push( $userHoliday[ $rs->fields( "type" ) ], array(
				
					"start" => $rs->fields( "start" ),
					"end" 	=> $rs->fields( "end" )
				
				));

				$rs->MoveNext();
				
			}

			foreach( $userHoliday as $type => $holidays ){

				?>
				<p>
					<strong>
					<?php
					
						switch( $type ){

							case "paid_leave" : 	echo "Congés payés"; break;
							case "sick_leave" : 	echo "Arrêts Maladie"; break;
							case "telecommuting" : 	echo "Télétravail"; break;
							case "undefined" : 
							default : 				echo "Autres congés";
						}
						
					?>
					</strong>
					<?php
					
						foreach( $holidays as $holiday ){
							
							echo "<br />&#155;&nbsp;";
							
							if( substr( $holiday[ "start" ], 0, 10 ) == substr( $holiday[ "end" ], 0, 10 ) )
								echo "Le " . strftime("%A %d %B %Y", strtotime( $holiday[ "start" ] ) );
							else{
								
								echo "Du ". strftime( "%A", strtotime( $holiday[ "start" ] ) );
								echo strftime( "%d %B %Y", strtotime( $holiday[ "start" ] ) );
								echo " au " . strftime("%A %d %B %Y", strtotime( $holiday[ "end" ] ) );
								
							}
							
						}
						
				?>
				</p>
				<?php
			
			}
			
			/* jours oeuvrés, tickets restaurant, etc... */
			
			include_once( dirname( __FILE__ ) . "/../../objects/DateUtil.php" );
			
			$dayCount = 31;
			while( !checkdate( $month, $dayCount, $year ) )
				$dayCount--;
		
			$leaveCount 		= 0.0;
			$labouringDayCount 	= $dayCount;
			$mealVoucherCount 	= $dayCount;
			
			$day = 1;
			while( $day <= $dayCount ){
				
				$isHoliday = DateUtil::isHoliday( "$year-$month-" . sprintf( "%02d", $day ) );
				$isWeekEnd = in_array( intval( strftime( "%w", strtotime( "$year-$month-" . sprintf( "%02d", $day ) ) ) ), array( 0, 6 ) );
				
				if( $isWeekEnd || $isHoliday ){
					
					$labouringDayCount--;
					$mealVoucherCount--;
					
				}
				
				$query = "
				SELECT IF( LENGTH( type ), type, 'undefined' ) AS type,
				IF( SUBSTR( start, 12, 2 ) = '12' AND SUBSTR( start, 1, 10 ) = '$year-$month-" . sprintf( "%02d", $day ) . "', TRUE, FALSE ) AS pm,
				IF( SUBSTR( end, 12, 2 ) = '12' AND SUBSTR( end, 1, 10 ) = '$year-$month-" . sprintf( "%02d", $day ) . "', TRUE, FALSE ) AS am
				FROM user_holiday 
				WHERE iduser = '" . $salesman->getId() . "'
				AND STRCMP( SUBSTR( end, 1, 10 ), '$year-$month-" . sprintf( "%02d", $day ) . "' ) >= 0
				AND STRCMP( SUBSTR( start, 1, 10 ), '$year-$month-" . sprintf( "%02d", $day ) . "' ) <= 0
				LIMIT 1";
				
				$rs = DBUtil::query( $query );
				
				if( $rs->RecordCount() && !$isWeekEnd && !$isHoliday ){
					
					
					$mealVoucherCount 	-= $rs->fields( "am" ) || $rs->fields( "pm" ) ? 0.5 : 1.0;
					$leaveCount			+= $rs->fields( "am" ) || $rs->fields( "pm" ) ? 0.5 : 1.0;
					
					if( $rs->fields( "type" ) != "telecommuting" )
						$labouringDayCount 	-= $rs->fields( "am" ) || $rs->fields( "pm" ) ? 0.5 : 1.0;
					
				}

				$day++;
				
			}
			
		?>
		<table cellspacing="0" cellpadding="0" border="0" class="recapConge">
		<tbody>
		<tr>
			<th>Nombre de jours oeuvrés</th>
			<td><?php echo Util::numberFormat( $labouringDayCount, 1 ); ?></td>
		</tr>
		<tr>
			<th>Nombre d'absence</th>
			<td><?php echo Util::numberFormat( $leaveCount, 1 ); ?></td>
		</tr>
		<tr>
			<th>Nombre de tickets restaurant</th>
			<td><?php echo Util::numberFormat( $mealVoucherCount, 1 ); ?></td>
		</tr>
		</tbody>
		</table>
		<div class="spacer"></div> 
		
	</div>
	<div class="spacer"></div>
	<?php
				
}

/* ------------------------------------------------------------------------------------------------------------- */

function getCalendar( Salesman $salesman, $year, $month ){

	/* nombre de jours dans le mois */
	
	$dayCount = 31;
	while( !checkdate( $month, $dayCount, $year ) )
		$dayCount--;

	?>
	<style type="text/css">
		
		#HolidayCalendar.edited{ border-width:2px; border-style:groove; }
		
		/* demi-journées */
		
		#HolidayCalendar td div{
			width:100%;
			height:100%;
			margin:0px;
			padding:0px;
			position:relative;
			vertical-align:middle;
		}
		#HolidayCalendar td div span.day{
			width:100%;
			height:100%;
			margin:0px;
			padding:0px;
			position:absolute;
			top:0xp;
			left:0px;
			/*line-height:100%;
			text-align:center;
			vertical-align:middle;*/
			z-index:1;
		}
		#HolidayCalendar td div div.am{
			width:50%;
			height:100%;
			margin:0px;
			padding:0px;
			float:left;
			left:0px;
			top:0px;
			position:absolute;
			z-index:2;
		}
		#HolidayCalendar td div div.pm{
			width:50%;
			height:100%;
			margin:0px;
			padding:0px;
			position:absolute;
			right:0px;
			top:0px;
			z-index:2;
		}
		
		/* jours fériés */
		
		#HolidayCalendar td{ cursor: pointer; }
		#HolidayCalendar td.publicHoliday,
		#HolidayCalendar td.padding{ 
			background: transparent url(../../images/back_office/content/calpad.jpg) repeat scroll 0 0 !important; 
			cursor:not-allowed;
		}
		
		/* congés */
		
		#HolidayCalendar td div div.paid_leave		{ background: transparent url(../../images/back_office/content/user_holidays.png) repeat scroll 136px 0 !important; }
		#HolidayCalendar td div div.sick_leave		{ background: transparent url(../../images/back_office/content/user_holidays.png) repeat scroll 0 0 !important; }
		#HolidayCalendar td div div.telecommuting	{ background: transparent url(../../images/back_office/content/user_holidays.png) repeat scroll 204px 0 !important; }
		#HolidayCalendar td div div.undefined		{ background: transparent url(../../images/back_office/content/user_holidays.png) repeat scroll 68px 0 !important; }

	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* -------------------------------------------------------------------------------------------- */
		
		var $start 	= null;		//début de sélection cliquée
		var $end 	= null;		//fin de sélection survolée
		var createMode = true; 	//création ou suppression

		/* current record */
		
		var userHolidays = new Array( '<?php
		
			/* tableau contenant toutes les demi-journées */
	
			$i = 0;
			while( $i <= $dayCount * 2 ){
		
				if( $i > 0 )
					echo "','";

				echo $i % 2 ? "pm" : "am";
				
				$day = sprintf( "%02d", ceil( ( $i + 1 ) / 2 ) );
				$start_time = $i % 2 ? "12:00:00" : "00:00:00";
				$end_time 	= $i % 2 ? "12:00:01" : "00:00:00";
				
				$query = "
				SELECT IF( LENGTH( type ), type, 'undefined' ) AS type, start, end
				FROM user_holiday 
				WHERE iduser = '" . $salesman->getId() . "'
				AND STRCMP( end, '$year-$month-$day $end_time' ) >= 0
				AND STRCMP(start, '$year-$month-$day $start_time' ) <= 0
				LIMIT 1";

				$rs = DBUtil::query( $query );
				
				if( $rs->RecordCount() )
					echo " " . $rs->fields( "type" );
				
				$i++;
				
			}
		
		?>' );

		/* -------------------------------------------------------------------------------------------- */
		
		$( document ).ready( function(){

			displayHolidays();
			setCalendarEvents();
			
		});

		/* -------------------------------------------------------------------------------------------- */
		/* display holidays */
		
		function displayHolidays(){

			/* calendar */
			
			var day = 1;
			while( day <= <?php echo $dayCount; ?> ){

				$( "#HolidayCalendar div[day=" + day + "] div.am" ).attr( "class", userHolidays[ ( day - 1 ) * 2 ] );
				$( "#HolidayCalendar div[day=" + day + "] div.pm" ).attr( "class", userHolidays[ ( day - 1 ) * 2 + 1 ] );
				
				day++;

			}	
			  
		}

		/* -------------------------------------------------------------------------------------------- */
		/* set onclick and oumouseover events */
		
		function setCalendarEvents(){

			/* onclick */
			
			$( "#HolidayCalendar div.am, #HolidayCalendar div.pm" ).click( function( event ){

				/* jour férié */
				
				if( $( this ).parent().parent().hasClass( "publicHoliday" ) )
					return;
				
				/* fin de sélection */
				
				if( $start ){

					$end = $( this );
					validateSelection();
					clearSelection();
					
					$( "#HolidayCalendar" ).removeClass( "edited" );
							
					return;
	
				}

				/* début de sélection */
				
				$start 		= $( this );
				$end 		= $( this );
				createMode 	= $( this ).attr( "class" ) == "am" || $( this ).attr( "class" ) == "pm";

				$( "#HolidayCalendar" ).addClass( "edited" );
				displaySelection();
		
			});

			/* onmouseover */

			$( "#HolidayCalendar div.am, #HolidayCalendar div.pm" ).hover( function( event ){

				if( !$start ) //pas de sélection active
					return;

				if( parseInt( $( this ).parent().attr( "day" ) ) >= parseInt( $start.parent().attr( "day" ) ) ){ //sélection unidirectionnelle
					
					$end = $( this );
					displaySelection();

				}
				
			});
			
		}
		
		/* -------------------------------------------------------------------------------------------- */
		/* display selection */
		
		function displaySelection(){

			/* selected range */
			
			var currentType = $( "input[name='type']:checked" ).val();
			var start 		= ( parseInt( $start.parent().attr( "day" ) ) - 1 ) * 2;
			var end 		= ( parseInt( $end.parent().attr( "day" ) ) - 1 ) * 2;
			
			if( $start.hasClass( "pm" ) )
				start++;
			
			if( $end.hasClass( "pm" ) )
				end++;

			/* selected cells */

			var i = start;
			while( i <= end ){

				var day = Math.floor( i / 2 ) + 1;

				if( i % 2 )
						$( "#PM" + day ).attr( "class", createMode ? "pm " + currentType : "pm" );
				else 	$( "#AM" + day ).attr( "class", createMode ? "am " + currentType : "am" );

				i++;

			}

			/* unselected cells */
			
			var j = userHolidays.length;
			while( j >= i ){

				var day = Math.floor( j / 2 ) + 1;
				
				if( j % 2 )
						$( "#PM" + day ).attr( "class", userHolidays[ j ] );
				else	$( "#AM" + day ).attr( "class", userHolidays[ j ] );
				
				j--;

			}
	
		}
		
		/* -------------------------------------------------------------------------------------------- */
		/* validate selection */
		
		function validateSelection(){

			var day = 1;
			while( day <= <?php echo $dayCount; ?> ){

				userHolidays[ ( day - 1 ) * 2 ] 		= $( "#AM" + day ).attr( "class" );
				userHolidays[ ( day - 1 ) * 2 + 1 ] 	= $( "#PM" + day ).attr( "class" );
				
				day++;

			}

			updateHolidays();
			getHolidays();
			
		}

		/* -------------------------------------------------------------------------------------------- */
		/* cancel selection */
		
		function clearSelection(){

			$start 		= null;
			$end 		= null;
			createMode 	= true;

		}
		
		/* -------------------------------------------------------------------------------------------- */
		/* save */
		
		function updateHolidays(){

			$.ajax({
			 	
				url: "/templates/secure/users.php?update_holidays",
				async: true,
				type: "POST",
				data: "iduser=<?php echo $salesman->getId(); ?>&year=" + $( "#HolidayYear" ).val() + "&month=" + $( "#HolidayMonth" ).val() + "&holidays=" + serializeHolidays(),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouvel utilisateur" ); },
			 	success: function( responseText ){
if( responseText != "" )
	alert( responseText );
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', "Modifications sauvegardées" );					
					
				}

			});
			
		}

		/* -------------------------------------------------------------------------------------------- */
		
		function serializeHolidays(){

			var holidays = new Array();
			var i = 0;
			while( i < userHolidays.length ){

				holidays.push( userHolidays[ i ].replace( "am", "" ).replace( "pm", "" ).replace( " ", "" ) );
				
				i++;
				
			}

			return holidays.join( "," );
			
		}

		/* -------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<table cellspacing="0" cellpadding="0" class="calendarUsers" id="HolidayCalendar">
		<thead>
			<tr>
				<th>Lun</th><th>Mar</th><th>Mer</th><th>Jeu</th><th>Ven</th><th>Sam</th><th>Dim</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<?php
			
				include_once( dirname( __FILE__ ) . "/../../objects/DateUtil.php" );
		
				$firstDayIndex = date( "N", strtotime( "$year-$month-01" ) );

				$i = 1;
				while( $i != $firstDayIndex ){
					
					?>
					<td class="padding"></td>
					<?php
					
					$i++;
					
				}
				
				$weekCount 	= 1;
				$day 		= 1;
				while( $day <= $dayCount ){
			
					?>
					<td<?php if( DateUtil::isHoliday( "$year-$month-" . sprintf( "%02d", $day ) ) ) echo " class=\"publicHoliday\""; ?>>
						<div day="<?php echo $day; ?>">
							<span class="day"><?php echo $day; ?></span>
							<div id="AM<?php echo $day; ?>" class="am"></div>
							<div id="PM<?php echo $day; ?>" class="pm"></div>
						</div>
					</td>
					<?php
					
					if( !( $i % 7 ) && $day < $dayCount ){
						
						?>
						</tr>
						<tr>
						<?php
						
						$weekCount++;
						
					}
					
					$day++;
					$i++;
					
				}
				
				while( ( $i -1 ) % 7 ){
					
					?>
					<td class="padding"></td>
					<?php
					
					$i++;
						
				}
				
			?>
			</tr>
			<?php
			
				if( $weekCount < 5 ){
					
					?>
					<tr>
						<td class="padding"></td>
						<td class="padding"></td>
						<td class="padding"></td>
						<td class="padding"></td>
						<td class="padding"></td>
						<td class="padding"></td>
						<td class="padding"></td>
					</tr>
					<?php
					
				}
				
		?>
		</tbody>
	</table>
	<?php
					
}

/* ------------------------------------------------------------------------------------------------------------- */

function displaySalary( Salesman $salesman, $year, $month ){
	
	$rs= DBUtil::query( 
	
		"SELECT * 
		FROM user_salary 
		WHERE start_date <= '$year-$month-01' 
		AND iduser = '" . $salesman->getId() . "'
		ORDER BY start_date DESC 
		LIMIT 1" 
	
	);
	
	$salary = $rs->RecordCount() ? ( object )$rs->fields : NULL;
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function updateSalary(){

			$.ajax({
			 	
				url: "/templates/secure/users.php?salary",
				async: true,
				type: "GET",
				data: "iduser=<?php echo $salesman->getId(); ?>&year=" + $("#SalaryYear" ).val() + "&month=" + $( "#SalaryMonth" ).val(),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le calendrier" ); },
			 	success: function( responseText ){

					$( "#Salary" ).html( responseText );
					
				}

			});
			
		}
		
	/* ]]> */
	</script>
	<h2 style="margin-top:0;">Salaire & prime</h2>
	<label><span>Période</span></label>
	<select id="SalaryMonth" name="salary_month" style="width:80px !important;" onchange="updateSalary();">
	<?php
	
		$i = 1;
		while( $i < 13 ){
			
			?>
			<option value="<?php echo sprintf( "%02d", $i ); ?>"<?php if( $i == intval( $month ) ) echo " selected=\"selected\""; ?>><?php echo strftime("%B", mktime( 0, 0, 0, $i, 1, 1 ) ); ?></option>
			<?php
			
			$i++;
			
		}
		
	?>
	</select>
	<select id="SalaryYear" name="salary_year" style="width:80px !important; margin-left:5px;" onchange="updateSalary();">
	<?php
	
		$previousYear = intval( substr( $salesman->get( "hire_date" ), 0, 4 ) );
		
		if( !$previousYear )
			$previousYear = date( "Y" );
			
		while( $previousYear <= date( "Y" ) ){
			
			?>
			<option value="<?php echo $previousYear; ?>"<?php if( $previousYear == $year ) echo " selected=\"selected\""; ?>><?php echo $previousYear; ?></option>
			<?php
			
			$previousYear++;
				
		}
		
	?>
	</select>
	
	<label style="clear:left;"><span>Salaire brut</span></label>
	<input class="textWithSign" name="gross_income" id="" value="<?php if( $salary ) echo Util::numberFormat( $salary->gross_income ); ?>" type="text" /> 
	<span class="textSigne">&nbsp;¤</span>
	
	<label class="smallLabel"><span>Tps travail</label>
	<input class="textWithSign" name="working_time" id="" value="<?php if( $salary ) echo Util::numberFormat( $salary->working_time, 1 ); ?>" type="text" /> 
	<span class="textSigne">&nbsp;h</span>
	<div class="spacer"></div> 
	
	<label><span>Primes brute</span></label>
	<input class="textWithSign" name="bonus" id="" value="<?php if( $salary ) echo Util::numberFormat( $salary->bonus, 2 ); ?>" type="text" /> 
	<span class="textSigne">&nbsp;¤</span>
	
	<label class="smallLabel"><span>Trajet</span></label>
	<input class="textWithSign" name="car_allowance" id="" value="<?php if( $salary ) echo Util::numberFormat( $salary->car_allowance, 2 ); ?>" type="text" /> 
	<span class="textSigne">&nbsp;¤</span>
	<div class="spacer"></div> 
	<?php
					
}

/* ------------------------------------------------------------------------------------------------------------- */
/* sauvegarde */

function updateHolidays( Salesman $salesman, $year, $month, $data ){
	
	$lastDay = 31;
	while( !checkdate( $month, $lastDay, $year ) )
		$lastDay--;
		
	DBUtil::query( 
		"DELETE FROM user_holiday 
		WHERE iduser = '" . $salesman->getId() . "' 
		AND start BETWEEN '$year-$month-01 00:00:00' AND '$year-$month-$lastDay 23:59:59'
		AND end BETWEEN '$year-$month-01 00:00:00' AND '$year-$month-$lastDay 23:59:59'"
	);
		
	/* récupérer tous les congés */
	
	$i 			= 0;
	$start 		= false;
	$type 		= false;
	$holidays 	= array();
	
	while( $i < count( $data ) ){
			
		if( $data[ $i ] ){
	
			$type 	= $data[ $i ];
			$day 	= sprintf( "%02d", ceil( ( $i + 1 ) / 2 ) );
			$time 	= $i % 2 ? "12:00:00" : "00:00:00";
			$start 	= "$year-$month-$day $time";

			while( $i + 1 < count( $data ) && $data[ $i + 1 ] == $type )
				$i++;
			
			if( $start ){
	
				$day 	= sprintf( "%02d", ceil( ( $i + 1 ) / 2 ) );
				$time 	= $i % 2 ? "23:59:59" : "12:00:00";
				$end 	= "$year-$month-$day $time";
				
				array_push( $holidays, array(
				
					"start" => $start,
					"end" 	=> $end,
					"type"	=> $type
				
				) );
			
				$type 	= false;
				$start 	= false;
				
			}
			
		}

		$i++;
		
	}
	
	/* insertions */
	
	foreach( $holidays as $holiday ){

		//recherche d'intersections
		
		$query = "
		SELECT * FROM user_holiday
		WHERE iduser = '" . $salesman->getId() . "' 
		AND (
			( start > '" . $holiday[ "start" ] . "' AND start < '" . $holiday[ "end" ] . "' )
			OR ( end > '" . $holiday[ "start" ] . "' AND end < '" . $holiday[ "end" ] . "' )
			OR ( start < '" . $holiday[ "start" ] . "' AND end > '" . $holiday[ "start" ] . "' )
		)
		ORDER BY start ASC, end DESC";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() ){
			
			while( !$rs->EOF() ){
	
				setHolidayIssue( $salesman->getId(), $rs->fields( "start" ), $rs->fields( "end" ), $rs->fields( "type" ), $holiday[ "start" ], $holiday[ "end" ], $holiday[ "type" ] );
				$rs->MoveNext();
				
			}
			
		}
		else DBUtil::query(
			"INSERT INTO user_holiday( iduser, start, end, type, creation_date )
			VALUES( '" . $salesman->getId() . "', '" . $holiday[ "start" ] . "', '" . $holiday[ "end" ] . "', '" . $holiday[ "type" ] . "', NOW() )"
		);	
	
		
	}
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function setHolidayIssue( $iduser, $start1, $end1, $type1, $start2, $end2, $type2 ){
	
	// ----------[---|---]--------|-----------
	
	if( strcmp( $start1, $start2 ) < 0 && strcmp( $end1, $end2 ) < 0 ){

		if( $type1 == $type2 )
			DBUtil::query( "UPDATE user_holiday SET end = '$end2' WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
		else if( substr( $start2, 11, 2 ) == "12" )
			DBUtil::query( "UPDATE user_holiday SET end = '$start2' WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
		else{
			
			$end = date( "Y-m-d H:i:s", mktime( 23, 59, 59, intval( substr( $start2, 5, 2 ) ), intval( substr( $start2, 8, 2 ) ) - 1, intval( substr( $start2, 0, 4 ) ) ) );
			DBUtil::query( "UPDATE user_holiday SET end = '$end' WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start2', '$end2', '$type2', NOW() )" );

		}	
		
		return;
		
	}
	
	// --------------|--[------]--|-----------
	
	if( strcmp( $start1, $start2 ) > 0 && strcmp( $end1, $end2 ) < 0 ){
	
		DBUtil::query( "DELETE FROM user_holiday WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
		DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start2', '$end2', '$type2', NOW() )" );
		
		return;
		
	}
	
	// --------------|--------[---|---]-------
	
	if( strcmp( $start1,$start2 ) > 0 && strcmp( $end1, $end2 ) > 0 ){
		
		if( $type1 == $type2 )
			DBUtil::query( "UPDATE user_holiday SET start = '$start2' WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
		else if( substr( $start2, 11, 2 ) == "12" ){
			
			DBUtil::query( "UPDATE user_holiday SET end = '$start2' WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start2', '$end2', '$type2', NOW() )" );
			
		}
		else{
			
			$start = date( "Y-m-d H:i:s", mktime( 0, 0, 0, intval( substr( $end2, 5, 2 ) ), intval( substr( $end2, 8, 2 ) ) + 1, intval( substr( $end2, 0, 4 ) ) ) );
			DBUtil::query( "UPDATE user_holiday SET start = '$start' WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start2', '$end2', '$type2', NOW() )" );

		}	
		
		return;
		
	}
	
	// ----------[---|------------|---]-------
	//             a        b       c
	
	if( strcmp( $start1, $start2 ) < 0 && strcmp( $end1, $end2 ) > 0 ){
	
		if( $type1 == $type2 )
			return;
			
		//b
		
		DBUtil::query( "DELETE FROM user_holiday WHERE iduser = '$iduser' AND start = '$start1' AND end = '$end1' LIMIT 1" );
		DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start2', '$end2', '$type2', NOW() )" );
		
		//a
		
		if( substr( $start2, 11, 2 ) == "12" )
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', $start1 , $start2, $type1, NOW() )" );
		else{

			$end = date( "Y-m-d H:i:s", mktime( 23, 59, 59, intval( substr( $start2, 5, 2 ) ), intval( substr( $start2, 8, 2 ) ) - 1, intval( substr( $start2, 0, 4 ) ) ) );
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start1', '$end', '$type1', NOW() )" );
			
		}

		//c
		
		if( substr( $end2, 11, 2 ) == "12" )
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$end2', '$end1', '$type1', NOW() )" );
		else{

			$start = date( "Y-m-d H:i:s", mktime( 0, 0, 0, intval( substr( $end2, 5, 2 ) ), intval( substr( $end2, 8, 2 ) ) + 1, intval( substr( $end2, 0, 4 ) ) ) );
			DBUtil::query( "INSERT INTO user_holiday( iduser, start, end, type, creation_date ) VALUES( '$iduser', '$start', '$end1', '$type1', NOW() )" );
			
		}
		
	}
	
}

/* ------------------------------------------------------------------------------------------------------------- */

?>
