<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

?>
<div id="EstimateSearchContainer">
<a name="topPage"></a>
            <div id="globalMainContent">
            	<script type="text/javascript">
				<!--
	
					function SortResult(sby,ord){
						
						document.adm_estim.action="<?php echo $ScriptName ?>?search=1&sortby="+sby+"&sens="+ord;
						document.adm_estim.submit();
						
					}
					
					function goToPage( pageNumber ){
						
						document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
						document.forms.adm_estim.submit();
						
					}
					
					function selectRelance(){
						
						document.getElementById( "interval_select_relance" ).checked=true;
						
					}
					
					function selectMonth(){
						
						document.getElementById( "interval_select_month" ).checked=true;
						
					}
					
					function selectBetween( field ){
						
						document.getElementById( "interval_select_between" ).checked=true;
						
						return true;
						
					}
					
					function confirmDeleteEstimate( idestimate ){
						
						var ret = confirm( '<?php echo Dictionnary::translate("gest_com_confirm_delete_estimate") ?> ' + idestimate + '?' );
						
						if( ret == true )
							document.forms.adm_estim.elements[ 'EstimateToDelete' ].value = idestimate;
						
						return ret;
						
					}
					
					function UpdateUseEstimate( idestimate){
						
						document.forms.adm_estim.elements[ 'use_estimate' ].value = idestimate;
						document.forms.adm_estim.submit();
						
					}
					
					function showHideAdvanced(){
						
						var advanceddiv = document.getElementById( 'advanced_search' );
						advanceddiv.style.display = advanceddiv.style.display == 'block' ? 'none' : 'block';
						
					}
					
				// -->
				</script>
				<form name="adm_estim" id="adm_estim" method="post" action="<?php echo $ScriptName ?>">
            	<div class="mainContent">
                <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
                    <div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
                    	<input type="hidden" name="search" value="1" />
						<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
						<input type="hidden" name="EstimateToDelete" id="EstimateToDelete" value="0" />
						<input type="hidden" name="use_estimate" id="use_estimate" value="0" />
                    	<div class="headTitle">
                            <p class="title">Rechercher un devis</p>
                            <div class="rightContainer">
                            	<input type="radio" name="date_type" id="type_creation" class="VCenteredWithText" value="creation_date"<?php if( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" || !isset( $_POST[ "date_type" ] ) ) echo " checked=\"checked\""; ?> /> Date de création
                                <input type="radio" name="date_type" id="type_sent" class="VCenteredWithText"  value="sent_date"<?php if( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "sent_date" ) echo " checked=\"checked\""; ?> /> Date d'envoi
                            </div>
                        </div>
                        <div class="subContent">
                    		<!-- tableau choix de date -->
                            <div class="tableContainer">
                                <table class="dataTable">
                                    <tr>	
                                        <td style="width:20%"><input type="radio" name="interval_select" id="interval_select_relance" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1 || !isset( $_POST[ "interval_select" ] ) ) echo " checked=\"checked\""; ?> /> Depuis</td>
                                        <td style="width:30%">
                                        	<select name="minus_date_select" onchange="selectRelance()" class="fullWidth">
												<option value="0"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == 0 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_today") ?></option>
												<option value="-1"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_yesterday") ?></option>
												<option value="-2"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -2 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2days") ?></option>
												<option value="-7"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -7 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1week") ?></option>
												<option value="-14"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -14 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2weeks") ?></option>
												<option value="-30"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -30 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1month") ?></option>
												<option value="-60"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -60 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2months") ?></option>
												<option value="-90"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -90 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_3months") ?></option>
												<option value="-180"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -180 || !isset( $_POST[ "minus_date_select" ] ) ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_6months") ?></option>
												<option value="-365"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -365 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1year") ?></option>
												<option value="-730"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -730 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2year") ?></option>
												<option value="-1825"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1825 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_5year") ?></option>
											</select>
                                        </td>
                                        <td style="width:20%"><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> Pour le mois de</td>
                                        <td style="width:30%">
                                        	<?php FStatisticsGenerateDateHTMLSelect( "selectMonth()" ); ?>
                                        </td>
                                    </tr>
                                    <tr>	
                                        <td><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> Entre le</td>
                                        <td><?php $date = isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : ""; ?><input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_start", true, "selectBetween" ) ?></td>
                                        <td> et le</td>
                                        <td><?php $date = isset( $_POST[ "sp_bt_end" ] ) ? $_POST[ "sp_bt_end" ] : ""; ?><input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_end", true, "selectBetween" ) ?></td>
                                    </tr>
                                </table>
                            </div>
                        	<!-- tableau de recherche -->
                            <div class="tableContainer">
                                <table class="dataTable">
                                    <tr class="filledRow">
                                        <th style="width:20%">N° client </th>
                                        <td style="width:30%">
                                        	<input type="text" name="sp_idbuyer" id="sp_idbuyer" class="textInput" value="<?php echo isset( $_POST[ "sp_idbuyer" ] ) ? $_POST[ "sp_idbuyer" ] : "" ?>" />
                                        	<?php
                                        	
                                        		include_once( dirname( __FILE__ ) . "/../../objects/AutoCompletor.php" );
            									AutoCompletor::completeFromDBPattern( "sp_idbuyer", "buyer", "idbuyer", "{company}", 15 );

            							?>
                                        </td>
                                        <th style="width:20%">Nom du contact</th>
                                        <td style="width:30%"><input type="text" name="sp_lastname" class="textInput" value="<?php echo isset( $_POST[ "sp_lastname" ] ) ? $_POST[ "sp_lastname" ] : "" ?>" /></td>
                                    </tr>
                                    <tr>
                                        <th>Nom / Raison sociale</th>
                                        <td><input type="text" name="company" class="textInput" value="<?php echo isset( $_POST[ "company" ] ) ? $_POST[ "company" ] : "" ?>" /></td>
                                        <th>Fax</th>
                                        <td><input type="text" name="faxnumber" class="textInput" value="<?php echo isset( $_POST[ "faxnumber" ] ) ? $_POST[ "faxnumber" ] : "" ?>" /></td>
                                    </tr>
                                    <tr>
                                        <th>Code postal</th>
                                        <td><input type="text" name="zipcode" class="textInput" value="<?php echo isset( $_POST[ "zipcode" ] ) ? $_POST[ "zipcode" ] : "" ?>" /></td>
                                        <th>Ville</th>
                                        <td><input type="text" name="city" class="textInput" value="<?php echo isset( $_POST[ "city" ] ) ? $_POST[ "city" ] : "" ?>" /></td>
                                    </tr>
                                    <tr class="filledRow">
                                        <th>Provenance du client</th>
                                        <td>
<?php
											
												$lang = User::getInstance()->getLang();
											
												$reqsource="SELECT idsource, source_code, source$lang AS source FROM source ORDER BY source$lang ASC";
												$rssource = DBUtil::query( $reqsource );
											
?>
											<select name="idsource[]" class="fullWidth">
												<option value="">Toutes</option>
<?php
												
													for( $s = 0 ; $s < $rssource->RecordCount() ; $s++ ){
												
?>
												<option value="<?php echo $rssource->Fields( "idsource" ) ?>"<?php if( isset( $_POST[ "idsource" ] ) && $_POST[ "idsource" ][ 0 ] == $rssource->Fields( "idsource" ) ){ echo " selected=\"selected\""; } ?>><?php echo $rssource->Fields( "source" ) ?></option>
<?php
												
														$rssource->MoveNext();
												
													}
?>
                                            </select>
                                        </td>
                                        <th>Commercial</th>
                                        <td>
<?php
														
												if( $hasAdminPrivileges ){
													
													include_once( dirname( __FILE__ ) . "/../../script/user_list.php" );
													userList( true, isset( $_POST[ "iduser" ] ) ? intval( $_POST[ "iduser" ] ) : 0 );
					
													//XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "lastname", isset( $_POST[ "iduser" ] ) ? intval( $_POST[ "iduser" ] ) : false, $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
													
												}
												else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
													
?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Taux scoring</th>
                                        <td>
<?php
											
												if( isset( $_POST[ "scoring_tx" ] ) )
													$scoring_tx = $_POST[ "scoring_tx" ];
												else
													$scoring_tx = "X";
											
												if( $hasAdminPrivileges )
													DrawLift_scoring( $scoring_tx );
												else
													echo "<b>" . $scoring_tx . "</b>";
											
?>
                                        </td>
                                        <th>Délais du scoring</th>
                                        <td><input type="text" name="scoring_date" id="scoring_date" class="calendarInput" value="" /> <?php echo DHTMLCalendar::calendar( "scoring_date" ) ?></td>
                                    </tr>
                                    <tr>
                                    	<th>Groupement</th>
                                        <td colspan="3"><?php XHTMLFactory::createSelectElement( "grouping", "idgrouping", "grouping", "grouping", isset( $_POST[ "idgrouping" ] ) ? $_POST[ "idgrouping" ] : false, "", "-", "name=\"idgrouping\"" ); ?></td>
                                    </tr>
                                    <tr><td colspan="4" class="tableSeparator"></td></tr>
                                    <tr class="filledRow">
                                        <th>Devis n°</th>
                                        <td><input type="text" name="sp_idorder" class="textInput" value="<?php echo isset( $_POST[ "sp_idorder" ] ) ? $_POST[ "sp_idorder" ] : "" ?>" /></td>
                                        <th>Avancement</th>
                                        <td>
                                        	<select name="sp_status" class="fullWidth">
												<option value="">Tous</option>
												<option value="ToDo"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "ToDo" ){ echo " selected=\"selected\""; } ?>>A traîter</option>
												<option value="InProgress"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "InProgress" ){ echo " selected=\"selected\""; } ?>>En cours</option>
												<option value="Send"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "Send" ){ echo " selected=\"selected\""; } ?>>Envoyé</option>
												<option value="Ordered"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "Ordered" ){echo " selected=\"selected\""; } ?>>Confirmé</option>
												<option value="Refused"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "Refused" ){echo " selected=\"selected\""; } ?>>Refusé</option>
											</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Relance entre le</th>
                                        <td><?php $date = isset( $_POST[ "rel_start" ] ) ? $_POST[ "rel_start" ] : ""; ?><input type="text" name="rel_start" id="rel_start" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "rel_start" ) ?></td>
                                        <th>et le</th>
                                        <td><?php $date = isset( $_POST[ "rel_end" ] ) ? $_POST[ "rel_end" ] : ""; ?><input type="text" name="rel_end" id="rel_end" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "rel_end" ) ?></td>
                                	</tr>
                                    <tr>
                                        <th>Montant TTC</th>
                                        <td><input type="text" name="total_amount" class="textInput" value="<?php echo isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "" ?>" /></td>
                                        <td colspan="2"></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="submitButtonContainer">
                            	<input type="submit" class="blueButton" value="Rechercher" />
                            </div>
                        </div>
                    </div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
                </div>
                <div id="tools" class="rightTools">
                	<div class="toolBox">
                    	<div class="header">
                        	<div class="topRight"></div><div class="topLeft"></div>
                            <p class="title">Outils</p>
                        </div>
                        <div class="content"></div>
                        <div class="footer">
                        	<div class="bottomLeft"></div>
                            <div class="bottomMiddle"></div>
                            <div class="bottomRight"></div>
                        </div>
                    </div>
                    <div class="toolBox">
                    	<div class="header">
                        	<div class="topRight"></div><div class="topLeft"></div>
                            <p class="title">Infos Plus</p>
                        </div>
                        <div class="content">
                        </div>
                        <div class="footer">
                        	<div class="bottomLeft"></div>
                            <div class="bottomMiddle"></div>
                            <div class="bottomRight"></div>
                        </div>
                    </div>
                </div>
<?php if ( $search != "" ){ ?>
<?php if( $resultCount > $maxSearchResults ){ ?>
                <div class="mainContent fullWidthContent">
					<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Plus de <?php echo $maxSearchResults ?> devis ont été trouvés</p>
						<p class="msg" style="text-align:center;">Merci de bien vouloir affiner votre recherche</p>
					</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
				</div>
<?php }else{ ?>
<?php if( $rs->RecordCount() <= 0 ){?>
                <div class="mainContent fullWidthContent">
					<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Aucun résultat ne correspond à votre recherche</p>
					</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
				</div>
<?php }else{ ?>
				<script type="text/javascript">
				/* <![CDATA[ */
				
					/* --------------------------------------------------------------------------------------------- */
					
					function createEstimateSlideshow(){
					
						if( $( "#Slideshow" ).length )
							return;
						
						var data = "estimates=<?php

							$estimates = array();
							
							$rs->MoveFirst();
							while( !$rs->EOF() ){
								
								array_push( $estimates, $rs->fields( "idestimate" ) );
								
								$rs->MoveNext();
									
							}
							
							echo Util::base64url_encode( serialize( $estimates ) );
							
							$estimates = NULL;
							
							$rs->MoveFirst();
							
						?>";
						
						$.ajax({
				
							url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/slideshow/estimate_slideshow.php?ondestroy=destroyEstimateSlideshow",
							async: true,
							cache: false,
							type: "POST",
							data: data,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger le diaporama" ); },
						 	success: function( responseText ){
				
								$( "#tools" ).hide();
								$( "#EstimateSearchContainer" ).hide();
								$( responseText ).insertBefore( $( "#EstimateSearchContainer" ) );
								
							}
				
						});
						
					}
					
					/* ------------------------------------------------------------------------------------------------ */
					
					function destroyEstimateSlideshow(){
					
						$( "#Slideshow" ).remove();
						$( "#EstimateSearchContainer" ).show();
						$( "#tools" ).show();
		
					}
				
					/* ------------------------------------------------------------------------------------------------ */
					
				/* ]]> */
				</script>
                <div class="mainContent fullWidthContent">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
                    	<div class="headTitle">
                            <p class="title">Résultats de la recherche : <?php echo $rs->RecordCount() ?> devis - Montant total : <?php echo Util::priceFormat( $totalAmount ); ?>
                            </p>
							<div class="rightContainer">
								<a href="#bottom" class="goUpOrDown" style="text-decoration:none;font-size:10px; color:#000000;">
									Aller en bas de page
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
								</a> &nbsp;&nbsp;
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/export_csv.php?type=estimate&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
								<a class="blueLink" title="Afficher le diaporama" href="#" onclick="createEstimateSlideshow(); return false;">
									diaporama
								</a>
							</div>
                        </div>
                        <div class="subContent">
							<div class="tableHeaderLeft" style="float:right;">
								<!--<a href="#bottom" class="goUpOrDown">
									Aller en bas de page
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
								</a>-->
							</div>
                        	<!-- tableau résultats recherche -->
                            <div class="resultTableContainer clear">
                            	<table class="dataTable resultTable">
                                	<thead>
                                        <tr>
                                            <th>Com.</th>
                                            <?php 
                                            if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1){
                                            ?>
                                            	<th>Conf.<br />récep.</th>
                                            <?php 
                                            }
                                            if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1){
                                            ?>
                                        		<th>Conf.<br />lect.</th>
                                            <?php 
                                            }
                                            ?>
                                            <th>Supprimer<br />Dupliquer</th>
                                            <th>Devis n°</th>
                                            <th>Client n°</th>
                                            <th>Type</th>
                                            <th>Code postal</th>
                                            <th>Raison sociale</th>
                                            <th>Contact (nom)</th>
                                            <th>Provenance</th>
                                            <th>Statut</th>
                                            <th><?php echo isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" ? "Date de création" : "Date d'envoi" ?></th>
                                            <th>Total HT</th>
                                            <th>Marge brute (%)</th>
                                            <th>Marge nette (%)</th>
                                            <!-- Quand une des cases est décochée, le devis n'est pas pris en compte dans les tableaux récapitulatifs -->
                                            <!--<th><img src="<?php echo $GLOBAL_START_URL ?>/www/icones/calculer.gif" alt="Utilisation du devis dans les calculs" /></th>-->
                                        </tr>
                                        <!-- petites flèches de tri -->
                                        <tr>
                                        	
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('u.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('u.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                            <?php 
                                            if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1){
                                            ?>
                                            	<th class="noTopBorder"></th>
                                            <?php 
                                            }
                                            if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1){
                                            ?>
                                        		<th class="noTopBorder"></th>
                                            <?php 
                                            }
                                            ?>
                                            <th class="noTopBorder"></th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('op.idestimate','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('op.idestimate','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('op.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('op.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                             <th class="noTopBorder">
                                            	<a href="javascript:SortResult('bu.contact','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('bu.contact','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('bu.zipcode','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('bu.zipcode','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('bu.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('bu.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('cont.lastname','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('cont.lastname','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('so.source<?php echo $lang ?>','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('so.source<?php echo $lang ?>','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('op.status','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('op.status','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('op.<?php echo isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" ? "DateHeure" : "sent_date" ?>','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('op.<?php echo isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" ? "DateHeure" : "sent_date" ?>','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                        	<th class="noTopBorder">
                                            	<a href="javascript:SortResult('op.total_amount_ht','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                                <a href="javascript:SortResult('op.total_amount_ht','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                            </th>
                                            <th class="noTopBorder"></th>
                                            <th class="noTopBorder"></th>
                                    	</tr>
                                    </thead>
                                    <tbody>
<?php
											
											//Parcours de chacune des lignes de résultat
											
											$rs->MoveFirst();
											
											$netMarginAmountSum		= 0.0;
											$roughMarginAmountSum 	= 0.0;
											$total_amount_htSum 	= 0.0;
											
											for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
												
												$Int_IdBuyer		= $rs->Fields("idbuyer");
												$Int_Iderp			= $rs->Fields("iderp");
												$company			= $rs->Fields("company");
												$Int_IdEstimate		= $rs->Fields("idestimate");
												$Int_IdDelivery		= $rs->Fields("iddelivery");
												$Int_IdPayment		= $rs->Fields("idpayment");
												$Str_Status			= $rs->Fields("status");
												$valid_until		= usDate2eu( $rs->Fields("valid_until") );
												$Str_Name			= getTitle($rs->Fields("title")).' '.$rs->Fields("lastname");				
												$relaunch			= usDate2eu( $rs->Fields("relaunch") );
												$totalht			= $rs->Fields("total_amount_ht");
												$totalcharge		= $rs->Fields("total_charge");
												$billing			= $rs->Fields("billing_amount");
												$idsource			= $rs->fields( "idsource" );
												$source				= getSourceText( $idsource );
												$initials			= $rs->fields( "initial" );
												$zipcode			= $rs->fields( "zipcode" );
												$style 				= "";
												$use_estimate 		= $rs->fields( "use_estimate" );
												$isContact			= $rs->fields( "contact" ) ;
												$statusReceipt 		= $rs->fields("status_return_receipt");
												$statusNotification = $rs->fields("status_delivery_notification");
												
												
												if( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" )
													$Str_Date = usDate2eu( substr( $rs->fields( "DateHeure" ), 0, 10 ) );
												else
													$Str_Date = usDate2eu( substr( $rs->fields( "sent_date" ), 0, 10 ) );
												
												// Calcul des marges
												$idestimates = array();
												$idestimates[] = $Int_IdEstimate;
												
												$roughMargin = 0;
												$roughMarginRate = 0;
																						
												$roughMarginRate 		= $rs->fields( "gross_margin_rate" );
												$netMarginRate 			= $rs->fields( "net_margin_rate" );
												$roughMarginAmountSum 	+= $rs->fields( "gross_margin_amount" );
												$netMarginAmountSum 	+= $rs->fields( "net_margin_amount" );
												$discount_price 		= $rs->fields( "discount_price" ) === null ? 0.0 : $rs->fields( "discount_price" );
												$total_amount_htSum 	+= ( $discount_price - $rs->fields( "total_discount_amount" ) - $rs->fields( "billing_amount" ) );
							
												/*
												if( $Str_Status == "Ordered" ){
													$rsdatecde = DBUtil::query( "SELECT DateHeure FROM `order` WHERE idestimate=$Int_IdEstimate" );
													$datecde = usDate2eu( substr( $rsdatecde->fields( "DateHeure" ), 0, 10 ) );
												}else{
													$datecde = "-";
												}
												
												$style2 = "";
												
												if( $initials == "SC" && $Str_Status == "ToDo" && ( $idsource == 2 || $idsource == 1 ) )
													$style2 = "style=\"font-weight:normal;color:#FF0000;\"";
												
												if( $Str_Status == "InProgress" )
													$style2 = "style=\"font-weight:normal;color:#007F0E;\"";
												*/
												
												$ck = $use_estimate == 1 ? " checked=\"checked\"" : "";
												
												if( $Str_Status == "InProgress" )
													$class = "blueText";
												elseif( $Str_Status == "ToDo" )
													$class = "orangeText";
												else
													$class = "blackText";
	                                    		
?>
                                    	<tr class="blackText">
                                        	
                                        	<td style="width:3%;" class="lefterCol"><?php echo $initials ?></td>
                                            <?php 
                                            if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1){
                                            ?>
                                            	<td>
                                            	<?php 
                                            	if($statusReceipt == 1){
                                            	?>
                                            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailbox-active.png" alt="" title="Une confirmation de reception a été reçue pour le devis"/>
                                            	<?php 
                                            	}
                                            	else{
                                            	?>
                                            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailbox-inactive.png" alt="" title="Aucune confirmation de réception n'a été reçue pour le devis"/>
                                            	<?php 
                                            	}
												?>
                                            	</td>
                                            <?php 
                                            }
                                            if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1){
                                            ?>
                                        		<td>
                                        		<?php 
                                            	if($statusNotification == 1){
                                            	?>
                                            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailopen-active.png" alt="" title="Une confirmation de lecture a été reçue pour le devis"/>
                                            	<?php 
                                            	}
                                            	else{
                                            	?>
                                            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailopen-inactive.png" alt="" title="Aucune confirmation de lecture n'a été reçue pour le devis"/>
                                            	<?php 
                                            	}
												?>
                                        		</td>
                                            <?php 
                                            }
                                            ?>
                                            <td style="width:4%;">
                                            	<?php if( $Str_Status == "InProgress" || $Str_Status == "ToDo" ){ ?><input type="image" name="DeleteEstimate" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="<?php echo Dictionnary::translate("delete_estimate") ?>" onclick="return confirmDeleteEstimate( <?php echo $Int_IdEstimate ?>);" /><?php } ?>
                                            	<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $Int_IdEstimate ?>&amp;editcopy=1" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/editcopy.png" alt="<?php echo Dictionnary::translate("duplicate") ?>" /></a>
                                            </td>
                                            <td style="width:6%;" valign="middle" style="white-space: nowrap;">
                                            	<span ><?php echo GenerateHTMLForToolTipBox( $Int_IdEstimate ) ?></span>
                                            	<a class="grasBack" href="com_admin_devis.php?IdEstimate=<?php echo $Int_IdEstimate ?>" onclick="window.open(this.href); return false;"><?php echo $Int_IdEstimate ?></a>
                                            </td>
                                            
                                            <td style="width:6%;">
                                            	 <a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Int_IdBuyer ?>">
                                            		<?php 
                                            		if(isset($Int_Iderp) && $Int_Iderp != "" && $Int_Iderp != 0 ){
                                            			echo $Int_Iderp . "/" . $Int_IdBuyer ; 
                                            		}else{
                                            			echo $Int_IdBuyer;
                                            		}
                                            		?>
                                            	 </a>
                                            </td>
                                            <!-- Prospect, client ? -->
                                            <td>
                                            	<?php
	                                            	switch( $isContact ){
	                                            		case "0":
	                                            			echo "Client";
	                                            			break;
	                                            		case "1":
	                                            			echo "Prospect";
	                                            			break;
	                                            	}
                                            	?>
                                            </td>
                                            <td style="width:6%;"><?php echo $zipcode ?></td>
                                            <td style="width:14%;"><a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Int_IdBuyer ?>"><?php if(!empty($company) && $company != "" && $company != " ") { echo $company ; } else { echo $Str_Name; } ?></a></td>
                                            <td style="width:14%;"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Int_IdBuyer ?>"><?php echo $Str_Name ?></a></td>
                                            <td style="width:11%;"><?php echo $source ?></td>
                                            <td style="width:7%;" class="<?php echo $class ?>"><b><?php echo Dictionnary::translate( $Str_Status ) ?></b></td>
                                            <td style="width:7%;"><?php echo $Str_Date ?></td>
                                            <td style="text-align:right; width:7%;"><?php echo Util::priceFormat( $totalht ) ?></td>
                                            <td style="width:6%;"><?php echo Util::rateFormat( $roughMarginRate ) ?></td>
                                            <td style="width:6%;" class="righterCol"><?php echo Util::rateFormat( $netMarginRate ) ?></td>
                                            <!--<td style="width:3%" class="righterCol"><input type="checkbox" name="use_estimate_<?php echo $Int_IdEstimate ?>" onclick="UpdateUseEstimate(<?php echo $Int_IdEstimate ?>)"<?php echo $ck ?> /></td>-->
                                        </tr>
<?php
										
												$rs->MoveNext();
					    						
											}
										
?>
                                    </tbody>
                                </table>
                            </div>
                       	</div>
               		</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
                </div>
<?php } ?>
<?php } ?>
<?php if( $resultCount > 0 && $resultCount <= $maxSearchResults ){ ?>
				<div class="mainContent fullWidthContent">
					<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
						<div class="headTitle">
                            <p class="title">Récapitulatif</p>
							<div class="rightContainer">
							<a href="#topPage" class="goUpOrDown" style="font-size:10px; color:#000000;">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
							
							</div>
                        </div>
                        <div class="subContent">
                            <!--
                            <a href="#topPage" class="goUpOrDown">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
                            -->
						    <table class="dataTable resultTable">
						    	<thead>
									<tr>
										<th rowspan="2" style="background-color:#FFFFFF; border-width:0px;"></th>
										<th colspan="7">Total devis (HT)</th>	
										<th rowspan="2">Marge brute</th>
										<th rowspan="2">Marge nette après port achat et vente</th>
									</tr>
									<tr>
										<th>Période</th>
										<th>Envoyé<br />relancé</th>
										<th>Envoyé<br />non relancé</th>
										<th>En cours</th>
										<th>A traîter</th>
										<th>Réfusé</th>
										<th>Commandé</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th style="width:5%;">Volume</th>
										<td style="width:10%;"><?php echo $rs->RecordCount() ?></td>
										<td style="width:10%;"><?php echo $nbenvoyeRel ?></td>
										<td style="width:10%;"><?php echo $nbenvoyeNonRel ?></td>
										<td style="width:10%;"><?php echo $nbencours ?></td>
										<td style="width:10%;"><?php echo $nbatraiter ?></td>
										<td style="width:10%;"><?php echo $nbrefuse ?></td>
										<td style="width:10%;"><?php echo $nbconfirme ?></td>
										<td style="width:10%;"> - </td>
										<td class="righterCol" style="width:10%;"> - </td>
									</tr>
									<?
									// calcul des pourcentages
									$resultCountRate = 100;
									$nbenvoyeRelRate = ($resultCountRate * $nbenvoyeRel)/$rs->RecordCount();
									$nbenvoyeNonRelRate = ($resultCountRate * $nbenvoyeNonRel)/$rs->RecordCount();
									$nbencoursRate = ($resultCountRate * $nbencours)/$rs->RecordCount();
									$nbatraiterRate = ($resultCountRate * $nbatraiter)/$rs->RecordCount();
									$nbrefuseRate = ($resultCountRate * $nbrefuse)/$rs->RecordCount();
									$nbconfirmeRate = ($resultCountRate * $nbconfirme)/$rs->RecordCount();
									
									?>
									<tr>
										<th style="width:5%; text-align:center;" >%</th>
										<td style="width:10%;"><?php echo Util::rateFormat($resultCountRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbenvoyeRelRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbenvoyeNonRelRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbencoursRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbatraiterRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbrefuseRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbconfirmeRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat( $roughMarginAmountSum / $total_amount_htSum * 100 , 2 ) ?></td>
										<td class="righterCol" style="width:10%;"><?php echo Util::rateFormat( $netMarginAmountSum / $total_amount_htSum * 100, 2 ) ?></td>
									</tr>
									<!-- Ligne de séparation -->
									<tr>
										<td colspan="10">&nbsp;</th>
									</tr>
									<!-- Fin ligne de séparation-->
									<tr>
										<th>Montant</th>
										<td><?php echo Util::priceFormat($totalAmount) ?></td>
										<td><?php echo Util::priceFormat($totalenvoyeRel) ?></td>
										<td><?php echo Util::priceFormat($totalenvoyeNonRel) ?></td>
										<td><?php echo Util::priceFormat($totalencours) ?></td>
										<td><?php echo Util::priceFormat($totalatraiter) ?></td>
										<td><?php echo Util::priceFormat($totalrefuse) ?></td>
										<td><?php echo Util::priceFormat($totalconfirme) ?></td>
										<td><?php echo Util::priceFormat( $roughMarginAmountSum ) ?></td>
										<td class="righterCol"><?php echo Util::priceFormat( $netMarginAmountSum ) ?></td>
									</tr>
									<?
									// calcul des pourcentages sur montants
									$totalAmountRate = 100;
									$totalenvoyeRelRate = ($totalAmountRate * $totalenvoyeRel)/$totalAmount;
									$totalenvoyeNonRelRate = ($totalAmountRate * $totalenvoyeNonRel)/$totalAmount;
									$totalencoursRate = ($totalAmountRate * $totalencours)/$totalAmount;
									$totalatraiterRate = ($totalAmountRate * $totalatraiter)/$totalAmount;
									$totalrefuseRate = ($totalAmountRate * $totalrefuse)/$totalAmount;
									$totalconfirmeRate = ($totalAmountRate * $totalconfirme)/$totalAmount;
									
									?>
									
									<tr>
										<th style="text-align:center;">%</th>
										<td><?php echo Util::rateFormat($totalAmountRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalenvoyeRelRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalenvoyeNonRelRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalencoursRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalatraiterRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalrefuseRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalconfirmeRate , 2) ?></td>
										<td> - </td>
										<td class="righterCol"> - </td>
									</tr>
									
								</tbody>
							</table>
						</div>
					</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
				</div>
                <div class="mainContent fullWidthContent">
					<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
					    <div class="headTitle">
                            <p class="title">Provenance</p>
							<div class="rightContainer">
							<a href="#topPage" class="goUpOrDown" style="font-size:10px; color:#000000;">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
							
							</div>
                        </div>
                        <div class="subContent">
                            <!--
                            <a href="#topPage" class="goUpOrDown">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
                            -->
						    <table class="dataTable resultTable">
						    	<thead>
									<tr>
										<th style="background-color:#FFFFFF; border-width:0px;"></th>
										<th><?php echo Dictionnary::translate("estimate_total") ?></th>
<?php
											
											//@todo: problème car on n'affiche qu'un certain nombre de sources et qu'il n'y a aucun criète sur le choix des sources
											//Attention à l'ordre d'affichage. Si on change l'ordre d'affichage ici (tableau $sources), il faut aussi le changer dans le contrôleur
											
											$sources = array( 1, 9, 5, 3, 6, 13, 2, 10 );
											
											for( $i = 0 ; $i < count( $sources ) ; $i++ ){
												
												$query = "SELECT source$lang FROM source WHERE idsource = " . $sources[ $i ];
												$rssources = DBUtil::query( $query );
												
												echo "				<th style=\"vertical-align:top;\">" . $rssources->fields( "source$lang" ) . "</th>\n";
												
											}
											
											//Calcul de la largeur en pourcentage de chaque colonne afin d'avoir toutes les colonnes à la même taille
											$width = floor( 92 / ( count( $sources ) + 2 ) );
											
?>
										<th>Autres</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th style="width:<?php echo ( 100 - ( count( $sources ) + 2 ) * $width ) ?>%;"><?php echo Dictionnary::translate( "number" ) ?></th>
<?php
									
											//Nombre de devis
											
											for( $i = 0 ; $i < count( $number ) ; $i++ ){
												
												if( $i == count( $number ) - 1 )
													$class = " class=\"righterCol\"";
												else
													$class = "";
													
													echo "				<td$class style=\"width:" . $width . "%;\">" . $number[ $i ] . "</th>\n";
												
											}
												
?>
									</tr>
									<tr>
										<th><?php echo Dictionnary::translate( "rate_number" ) ?></th>
<?php
											
											//Nombre de devis en %
											
											for( $i = 0 ; $i < count( $rate_number ) ; $i++ ){
												
												if( $i == count( $rate_number ) - 1 )
													$style = " class=\"righterCol\"";
												else
													$style = "";
												
												echo "				<td$style>" . Util::rateFormat( $rate_number[ $i ], 0 ) . "</th>\n";
												
											}
											
?>
									</tr>
									<tr>
										<th><?php echo Dictionnary::translate( "amount" ) ?></td>
<?php
											
											//Montant des devis
											
											for( $i = 0 ; $i < count( $amount ) ; $i++ ){
												
												if( $i == count( $amount ) - 1 )
													$style = " class=\"righterCol\"";
												else
													$style = "";
												
												echo "				<td$style>" . Util::priceFormat( $amount[ $i ] ) . "</th>\n";
												
											}
											
?>
									</tr>
									<tr>
										<th><?php echo Dictionnary::translate( "average_estimate" ) ?></td>
<?php
											
											//Devis moyen
											
											for( $i = 0 ; $i < count( $average_estimate ) ; $i++ ){
												
												if( $i == count( $average_estimate ) - 1 )
													$style = " class=\"righterCol\"";
												else
													$style = "";
												
												if( $i == 2 ){
													$style2 = " style=\"color:red;\"" ; 
													echo "<td$style $style2>" . Util::priceFormat( $average_estimate[ $i ], 0 ) . "</th>\n";
												}else{
													echo "<td$style>" . Util::priceFormat( $average_estimate[ $i ], 0 ) . "</th>\n";
												}
											}
											
?>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
				</div>
<?php } ?>
<?php } ?>
               	</form>
            </div>
<a name="bottom"></a>
</div><!-- EstimateSearchContainer -->