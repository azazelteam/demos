<!-- ++++++++++++++++++++++++++++++++++++++++++ VIRTUAL BUYER FRAME ++++++++++++++++++++++++++++++++++++++++++ -->
<?php 

anchor( "UpdateTodoMessage" );
	
?>
<script type="text/javascript">
<!--

	function show_refus() {
	
		var refusal_value=document.getElementById( "chstat" ).elements['cstatus'].options[document.getElementById( "chstat" ).elements['cstatus'].selectedIndex].value; 
	
		
		if(refusal_value=='Refused'){ 
			
			document.getElementById( "chstat" ).elements['refusal'].style.display = 'block';
		}else{
			document.getElementById( "chstat" ).elements['refusal'].style.display = 'none';
		}
		
	}
	
	function testrefus() {
	
		var refusal_value=document.getElementById( "chstat" ).elements['cstatus'].options[document.getElementById( "chstat" ).elements['cstatus'].selectedIndex].value;
		
		if(refusal_value=='Refused'){ 
			var motif=document.getElementById( "chstat" ).elements['refusal'].options[document.getElementById( "chstat" ).elements['refusal'].selectedIndex].value;
		
			if(motif==0){
				alert("Vous ne pouvez malheureusement pas modifier le statut car vous avez omis de sélectionner un motif de refus. Il vous prie de bien vouloir en sélectionner un. Merci de votre compréhension. Pour plus d'informations contactez Akilaé.");
				return false ;
			}else{
				return true ;
			}
		}
		
	}
	
	function showTodoInfos(){
		
		var tododiv = document.getElementById( 'TodoDiv' );
		tododiv.style.display = tododiv.style.display == 'block' ? 'none' : 'block';
	
	}
	
	function showFilesInfos(){
		
		var filesdiv = document.getElementById( 'FilesDiv' );
		filesdiv.style.display = filesdiv.style.display == 'block' ? 'none' : 'block';
	
	}

	function visibility(thingId, txtAff, txtMasque) {
		var targetElement; 
		var targetElementLink;
		targetElement = document.getElementById(thingId);
		targetElementLink = document.getElementById(thingId+'Link');
		if (targetElement.style.display == "none") {
			targetElement.style.display = "";
			targetElementLink.innerHTML = txtMasque;
		} else {
			targetElement.style.display = "none";
			targetElementLink.innerHTML = txtAff;
		}
	}
//-->
</script>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	 <div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
	    <div class="content">
	    	<div class="subContent">
		    	<div class="headTitle"><?
					if($IdEstimate!=0){
						
						?>
						<p class="title">Devis n° <?php echo $IdEstimate ?></p>
						<?php 
						
					}
					else{
						
						?>
						<p class="title">Création de devis</p>
						<?php 
						
					}
					
					?>
		            <div class="rightContainer date"><?php echo humanReadableDatetime( $Order->getValue( "DateHeure" ) ); ?></div>
					<div class="clear"></div>
				</div>			
		        <div class="subHeadTitle">
		            <p class="subHeadTitleElementLeft"><b><?php  echo Dictionnary::translate( "gest_com_virtual_data" ) ; ?></b></p>
		        </div>
		        <div class="clear"></div>
		        <div class="subContent">
					<p style="margin-top:15px; text-align:left;">Ce devis express va être créé sur votre compte personnel. Vous pouvez attribuer ce devis à un prospect ou à un client enregistré ou créer sa fiche d'identité en cliquant sur le bouton <b>Créer un nouveau prospect</b></p>
					<form method="post" action="com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>" enctype="multipart/form-data">
						<input type="hidden" name="ReassignBuyer" value="1" />
						<p style="margin-top:20px; text-align:left;">
							<b>Attribuer le devis par :</b>
						</p>
						<ul style="margin-left:25px; list-style-type:none; text-align:left; width:400px;">
							<li style="height:25px;">
								Cr&eacute;ation d'un nouveau prospect
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=contact&amp;VirtualEstimate=<?php echo $IdEstimate ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Créer un prospect" style="margin-left:15px; vertical-align:middle;" /></a>
							</li>
							<li style="height:25px;">
								Recherche du client ou Prospect 
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_select.php?buyer&amp;VirtualEstimate=<?php echo $IdEstimate ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Rechercher" style="margin-left:10px; vertical-align:middle;" /></a>
							</li>
							<li style="height:25px;">
								N° de client ou prospect 
								<input type="text" name="RealBuyer" value="" class="textInput" />
								<input type="submit" class="blueButton" value="Attribuer">
							</li>
						</ul>
					</form>
					<!-- todo -->
					<div class="clear" style="margin-top:10px;"></div>
					<form id="TodoInfosForm" action="com_admin_devis.php#lastaction" method="post" enctype="multipart/form-data">
			            <input type="hidden" name="IdEstimate" value="<?php echo $Order->getValue( "idestimate" ) ?>" />
			            <p class="orangeText">
			            	Commentaires / Relances
			            	<a href="#" id="relaunchCommentsDivLink" onclick="visibility('relaunchCommentsDiv','[Afficher]','[Masquer]'); return false;" class="blueLink"><?php echo strlen( $Order->getValue( "todo" ) ) ? "[Masquer]" : "[Afficher]" ?></a>		            	
			            </p>
			            <div id="relaunchCommentsDiv" style="display:<?php echo strlen( $Order->getValue( "todo" ) ) ? "block" : "none" ?>;">
			                <textarea name="todo" cols="" rows="" style="height:30px; width:95%; float:left;"><?php echo htmlentities( $Order->getValue( "todo" ) ) ?></textarea>
			                <input type="image" name="UpdateTodoMessage" id="UpdateTodoMessage" src="../../images/back_office/content/rightArrow.png" style="float:right;" />
			                <div class="clear"></div>
			            </div>
					</form>
					<!-- files -->
					<?php
		
						$PJ = $Order->getValue( "estimate_doc" );
						$offer_signed = $Order->getValue( "offer_signed" );
						$supplier_offer = $Order->getValue( "supplier_offer" );
						$supplier_doc = $Order->getValue( "supplier_doc" );
			
						$hasAttachments = !empty( $PJ ) || !empty( $offer_signed ) || !empty( $supplier_offer ) || !empty( $supplier_doc );
						
					?>
					<div class="clear" style="margin-top:10px;"></div>
					<form name="FilesForm" id="FilesForm" action="com_admin_devis.php#lastaction" method="post" enctype="multipart/form-data">
				        <p class="orangeText">
				          	Pièces jointes
				           	<a href="#" id="linkJointDivLink" onclick="visibility('linkJointDiv','[Afficher]','[Masquer]'); return false;" class="blueLink"><?php echo $hasAttachments ? "[Masquer]" : "[Afficher]" ?></a>
				        </p>
						<div id="linkJointDiv" style="display:<?php echo $hasAttachments ? "block" : "none" ?>;">
							<table cellspacing="0" cellpadding="4" class="dataTable" style="width:95%;float:left;">
								<tr>
									<th>Pièces jointes</th>
									<td nowrap><input<?php echo $disabled ?> type="file" name="estimate_doc">
										<?php  $PJ = $Order->getValue( "estimate_doc" ); 
										if(!empty($PJ)){?>
										&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $PJ ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>&nbsp;<input<?php echo $disabled ?> type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_estimate_doc" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
										<?php } ?>
									</td>
									<th><?php  echo Dictionnary::translate("gest_com_estimate_signed") ; ?></th>
									<td nowrap><input type="file" name="offer_signed">
										<?php  $offer_signed = $Order->getValue( "offer_signed" ); 
										if(!empty($offer_signed)){?>
										&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $offer_signed ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>&nbsp;<input<?php echo $disabled ?> type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_offer_signed" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
										<?php } ?>
									</td>				
								</tr>
								<tr>
									<th><?php  echo Dictionnary::translate("gest_com_supplier_offer") ; ?></th>
									<td nowrap><input type="file" name="supplier_offer">
										<?php  $supplier_offer = $Order->getValue( "supplier_offer" ); 
										if(!empty($supplier_offer)){?>
										&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $supplier_offer ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>&nbsp;<input<?php echo $disabled ?> type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_supplier_offer" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
									<?php } ?>
									</td>
									<th><?php  echo Dictionnary::translate("gest_com_doc_supplier") ; ?></th>
									<td nowrap><input type="file" name="supplier_doc">
										<?php  $supplier_doc = $Order->getValue( "supplier_doc" ); 
										if(!empty($supplier_doc)){?>
										&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $supplier_doc ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>&nbsp;<input<?php echo $disabled ?> type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_supplier_doc" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
										<?php } ?>
									</td>
								</tr>
							</table>
							<input type="hidden" name="IdEstimate" value="<?php echo $Order->getValue( "idestimate" ) ?>" />
		                	<input type="image" name="UpdateFiles" id="UpdateFiles" src="../../images/back_office/content/rightArrow.png" style="float:right;" />
		                	<div class="clear"></div>
						</div>
					</form>
				</div>
        </div>
    </div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div>
<?php

//---------------------------------------------------------------------------------------------------

function getCommercialName( $iduser ){
	
	global $Session;
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT title, firstname, lastname FROM user WHERE iduser = '$iduser' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le nom du commercial" );
		
	return $rs->fields( "title" ) . ". " . $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
		
}

//---------------------------------------------------------------------------------------------------

?>