<?php

include_once( dirname( __FILE__ ) . "/../../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once ("$GLOBAL_START_PATH/objects/AutoCompletor.php");

if( $type == "contact" && isset( $key ) )
	$submit = "Modifier les informations du prospect";
elseif( $type == "buyer" && isset( $key ) )
	$submit = "Modifier les informations du client";
elseif( $type == "contact" && !isset( $key ) )
	$submit = "Cr�er le prospect";
elseif( $type == "buyer" && !isset( $key ) )
	$submit = "Cr�er le client";
else
	$submit = "Modifier";

//---------------------------------------------------------------------------------------------

//champs obligatoires dynamiques, en fonction du profil client � parametrer ici
//Note : le num�ro de SIRET, le code NAF et le num�ro de TVA Intracommunautaire ne doivent pas �tre g�r�s ici
/**
 * @todo
 * Actuellement le champ `buyer`.catalog_right est utilis� � la fois et � tort pour :
 * 		-> le niveau de privil�ge pour la consultation des produits sur le front office
 * 		-> le profil commercial ( particulier, revendeur, ... )
 * Il faut dissocier les 2 et associer � chaque profil commercial un niveau de privil�ge
 * car certains niveau de privil�ges ne sont pas des profils commerciaux ( ex : visiteur, administrateur ).
 */
$requiredElements = array(

	/*prospects*/
	
	$contact = 1 => array(
	
		$catalog_right = 0 /*aucun*/ 			=> array( "catalog_right" ),
		$catalog_right = 1 /*particulier*/ 		=> array( "Adress", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 2 /*acheteur*/ 		=> array( "Company", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 3 /*association*/ 		=> array( "Company", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 4 /*administration*/ 	=> array( "Company", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 5 /*grand compte*/ 	=> array( "Company", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 6 /*revendeur*/ 		=> array( "Company", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 7 /*administrateur*/ 	=> array()
		
	),
	
	/*clients*/
	
	$contact = 0 => array( 
	
		$catalog_right = 0 /*aucun*/ 			=> array( "catalog_right" ),
		$catalog_right = 1 /*particulier*/ 		=> array( "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 2 /*acheteur*/ 		=> array( "Company", "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 3 /*association*/ 		=> array( "Company", "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 4 /*administration*/ 	=> array( "Company", "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 5 /*grand compte*/ 	=> array( "Company", "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 6 /*revendeur*/ 		=> array( "Company", "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "Phonenumber", "idsource" ),
		$catalog_right = 7 /*administrateur*/ 	=> array()
		
	)
	
);

//---------------------------------------------------------------------------------------------

?>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
			<script type="text/javascript">
			<!--
				
				//---------------------------------------------------------------------------------------------
			
				//champs obligatoires dynamiques, en fonction du profil client
				
				var requiredElements = new Array();
				
				requiredElements[ 0 ] = new Array();
				requiredElements[ 1 ] = new Array();
<?php
					
					foreach( $requiredElements as $contact => $elements ){
						
						foreach( $elements as $catalog_right => $elementNames ){
							
?>
				requiredElements[ <?php echo $contact ?> ][ <?php echo $catalog_right ?> ] = new Array( <?php
							
							$i = 0;
							while( $i < count( $elementNames ) ){
								
								if( $i )
									echo ",";
								
								echo "'" . $elementNames[ $i ] . "'";
								
								$i++;
								
							}
							
							?> );
<?php
							
						}
						
					}
					
?>
				
				//couleur de fond des champs obligatoires
				
				var disabledBackgroundColor = '#E7E7E7';
				
				var defaultBackgroundColor = '#FFFFFF';
				var defaultColor = '#000000';
				
				var requiredBackgroundColor = '#CFC3E9';
				var requiredColor = '#000000';
				
				var errorBackgroundColor = '#FF8484';
				var errorColor = '#000000';
				
				//---------------------------------------------------------------------------------------------
				
				function resetForm(){
					
					var elements = document.forms.frm.elements;
					var i = 0;
					while( i < elements.length ){
						
						setRequiredElement( elements[ i ], false );
						
						i++;
						
					}
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//met en �vidence les champs obligatoires du formulaire
				
				function initRequiredElements(){
					
					resetForm();
					
					var form = document.forms.frm;
					var catalog_right = form.elements[ 'catalog_right'�][ form.elements[ 'catalog_right'�].selectedIndex ].value;
					var contact = <?php echo $type == "contact" ? "1" : "0" ?>;
					
					var i = 0;
					while( i < form.elements.length ){
						
						if( form.elements[ i ].getAttribute( 'name' ) != null && isRequired( form.elements[ i ].getAttribute( 'name' ), contact, catalog_right ) && ( !form.elements[ i ].type || form.elements[ i ].type != 'hidden' ) )
							setRequiredElement( form.elements[ i ], true );
						
						i++;
						
					}
					
					//TVA Intracommunautaire
					
					if( isVATRequired() )
						addRequiredElement( document.forms.frm.elements[ 'Tva' ] );
					else
						removeRequiredElement( document.forms.frm.elements[ 'Tva' ] );
					
					//SIRET et code NAF
					
					if( isSiretAndAPERequired() ){
						
						addRequiredElement( document.forms.frm.elements[ 'siret' ] );
						addRequiredElement( document.forms.frm.elements[ 'Naf' ] );
						
					}else{
						
						removeRequiredElement( document.forms.frm.elements[ 'siret' ] );
						removeRequiredElement( document.forms.frm.elements[ 'Naf' ] );
						
					}
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//rend un champ donn� obligatoire ou non
				
				function setRequiredElement( element, required ){
					
					if( !element.type || element.type != 'button' && element.type != 'submit' && element.type != 'image' ){
						
						element.style.backgroundColor = required ? requiredBackgroundColor : defaultBackgroundColor;
						//element.style.color = required ? requiredColor : defaultColor;
						
					}
					
					var nextSibling = element.nextSibling;
					
//					if( !required && nextSibling && nextSibling.title && nextSibling.title == 'Required Symbol' )
//						element.parentNode.removeChild( nextSibling );
//					
//					if( required ){
//						
//						var mul = document.createElement( 'span' );
//						
//						mul.setAttribute( 'title', 'Required Symbol' );
//						mul.style.color = '#FF0000';
//						mul.style.fontWeight = 'bold';
//						mul.style.fontSize = '14px';
//						mul.style.padding = '3px';
//						mul.innerHTML = '*';
//						
//						if( nextSibling )
//							element.parentNode.insertBefore( mul, nextSibling );
//						else
//							element.parentNode.appendChild( mul );
//						
//					}
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//Return true si un nom de champ donn� est obligatoire un type de client donn� et une valeur donn�e de catalog_right
				
				function isRequired( elementName, contact, catalog_right ){
					
					if( elementName == null || elementName.length == 0 )
						return false;
					
					var j = 0;
					while( j < requiredElements[ contact ][ catalog_right ].length ){
						
						if( requiredElements[ contact ][ catalog_right ][ j ].toLowerCase() == elementName.toLowerCase() )
							return true;
						
						j++;
						
					}
					
					return false;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si tous les champs obligatoires ont �t� renseign�s pour un type de client donn�
				
				function checkRequiredElements( contact ){
					
					if( document.getElementById( "GenerateLogin" ) && document.getElementById( "GenerateLogin" ).checked && document.getElementById( "Email" ).value == "" ){
						
						alert( "Vous devez saisir une adresse email pour cr�er un compte pour le Front Office." );
						return false;
						
					}
					
					var form = document.forms.frm;
					var catalog_right = form.elements[ 'catalog_right'�][ form.elements[ 'catalog_right'�].selectedIndex ].value;
					
					//champs dynamiques
					
					var hasError = false;
					var i = 0;
					while( i < form.elements.length ){
						
						var element = form.elements[ i ];
						
						if( element.getAttribute( 'name' ) != null && isRequired( element.getAttribute( 'name' ), contact, catalog_right ) ){
							
							if( isEmpty( element ) ){ //le champ obligatoire n'est pas renseign�
								
								element.style.backgroundColor = errorBackgroundColor;
								element.style.color = errorColor;
								
								hasError = true;
								
							}else{ //le champ obligatoire est bien renseign�
								
								element.style.backgroundColor = requiredBackgroundColor;
								element.style.color = requiredColor;
								
							}
							
						}
						
						i++;
						
					}
					
					if( hasError )
						alert( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "missing_required_fields" ) ) ?>' );
					
					return !hasError;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function addRequiredElement( element ){
					
					var catalog_right 	= document.forms.frm.elements[ 'catalog_right'�][ document.forms.frm.elements[ 'catalog_right'�].selectedIndex ].value;
					var contact = <?php echo $type == "contact" ? "1" : "0" ?>;
					
					if( isRequired( element.name, contact, catalog_right ) )
						return;
					
					var requiredElementCount = requiredElements[ contact ][ catalog_right ].length;
					requiredElements[ contact ][ catalog_right ][ requiredElementCount ] = element.name;
					
					setRequiredElement( element, true );
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function removeRequiredElement( element ){
					
					var catalog_right 	= document.forms.frm.elements[ 'catalog_right'�][ document.forms.frm.elements[ 'catalog_right'�].selectedIndex ].value;
					var contact = <?php echo $type == "contact" ? "1" : "0" ?>;
					
					if( !isRequired( element.name, contact, catalog_right ) )
						return;
					
					var index = 0;
					var done = false;
					while( index < requiredElements[ contact ][ catalog_right ].length && !done ){
						
						if( requiredElements[ contact ][ catalog_right ][ index ] == element.name )
							done = true;
						else
							index++;
						
					}
					
					requiredElements[ contact ][ catalog_right ].splice( index, 1 );
					
					setRequiredElement( element, false );
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si le num�ro SIRET et le code NAF sont obligatoires
				
				function isSiretAndAPERequired(){
					
<?php
					
					//contacts : Siret non requis
					
					if( $type == "contact" ){
						
?>
					return false;
<?php
						
					}else{ //clients
						
?>
					var selectedState 	= document.forms.frm.elements[ 'idstate' ].options[ document.forms.frm.elements[ 'idstate' ].selectedIndex ].value;
					var catalog_right 	= document.forms.frm.elements[ 'catalog_right'�][ document.forms.frm.elements[ 'catalog_right'�].selectedIndex ].value;
					
					//v�rifier si le pays s�lectionn� utilise le Siret et le code NAF
					
					if( selectedState != 1 && selectedState != 44 && selectedState != 48 )
						return false;
					
					switch( catalog_right ){
						
						case '2' :
						case '6' :
						case '5' : return true;
						
						default : return false;
					
					}
<?php
						
					}
					
?>
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si la TVA Intracommunautaire est obligatoire, sinon false
				//TVA Intracommunautaire obligatoire que pour le client et pour les profils 'Acheteur', 'Revendeur' et 'Grand Compte'
				
				function isVATRequired(){
					
<?php 
					
					//contacts : TVA intracommunautaire non requise
					
					if( $type == "contact" ){
						
?>
					return false;
<?php
						
					}else{ //clients
						
						//@todo corriger bidouille 'France H.T.' dans la table `state`
						
?>
					var euStates = new Array( 1, 44, 48, <?php
						
						$rs = DBUtil::query( "SELECT idstate FROM state WHERE code_eu LIKE 'INTRA'" );
						
						$i = 0;
						while( !$rs->EOF() ){
							
							if( $i )
								echo ",";
							
							echo $rs->fields( "idstate" );
							
							$rs->MoveNext();
							$i++;
							
						}
						
						?>);
					
					var selectedState 	= document.forms.frm.elements[ 'idstate' ].options[ document.forms.frm.elements[ 'idstate' ].selectedIndex ].value;
					var catalog_right 	= document.forms.frm.elements[ 'catalog_right'�][ document.forms.frm.elements[ 'catalog_right'�].selectedIndex ].value;
					
					//v�rifier si le pays s�lectionn� est intracommunautaire
					
					var i = 0;
					var euStateSelected = false;
					while( !euStateSelected && i < euStates.length ){
						
						if( selectedState == euStates[ i ] ) //TVA intracommunautaire non requise pour l
							euStateSelected = true;
						
						i++;
						
					}
					
					if( !euStateSelected )
						return false;
					
					switch( catalog_right ){
						
						case '2' :
						case '6' :
						case '5' : return true;
						
						default : return false;
						
					}
<?php
						
					}
					
?>
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function selectCity( zipcode ){
				
					var xhr = getXMLHttpRequest();
					
					xhr.onreadystatechange = function(){
			
						if( xhr.readyState == 4 && xhr.status == 200 ){
						
							if( !xhr.responseXML )
								return;
	
							createCitySelection( xhr.responseXML );
							
						}
						
					}
				
					var url = '<?php echo $GLOBAL_START_URL ?>/sales_force/commune.php?zipcode=' + zipcode;
		
					xhr.open( "GET", url, true );
					xhr.send( null );
					
				}
				
				//------------------------------------------------------------------------------------------------
				
				function createCitySelection( xmlDocument ){
					
					var target = document.getElementById( 'City' );
					var communes = xmlDocument.getElementsByTagName( 'commune' );
			
					if( !communes.length )
						return;
						
					if( communes.length == 1 ){
					
						target.value = communes[ 0 ].getAttribute( 'value' );
						return;
						
					}
					
					var selection = document.getElementById( 'CitySelection' );
	
					var innerHTML 	='<p style="background-color:#EAEAEA; padding:7px; font-weight:bold;">Communes propos�es</p>';
					innerHTML 		+= '<div style="overflow:auto; height:120px;">';
					innerHTML 		+= '<ul style="list-style-type:none;">';
					
					var i = 0;
					while( i < communes.length ){
	
						var commune = communes[ i ].getAttribute( 'value' );
						
						innerHTML += '<li style="text-align:left;">';
						innerHTML += '<input type="radio" onclick="document.getElementById( \'City\' ).value = \'' + commune + '\'; setTimeout( $.unblockUI, 10 );" />&nbsp;';
						innerHTML += '<a style="font-size:11px; text-decoration:none;" href="#" class="blueLink" onclick="document.getElementById( \'City\' ).value = \'' + commune + '\'; setTimeout( $.unblockUI, 10 ); return false;">';
						innerHTML += commune;
						innerHTML += '</a>';
						innerHTML += '</li>';
						
						i++;
						
					}
					
					innerHTML += '</ul>';
					innerHTML += '</div>';
					innerHTML += '<p style="text-align:right;">';
					innerHTML += '<input type="button" value="Annuler" class="blueButton" onclick="setTimeout( $.unblockUI, 10 );" />';
					innerHTML += '</p>';
					
					selection.innerHTML = innerHTML;
					
					$.blockUI({ 
	
						message: $('#CitySelection'),
						css: { padding: '15px', cursor: 'pointer' }
						
					});
						
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si au moins un des 2 champs 'email' ou 'fax' est bien renseign�
				
				function checkEmailOrFax(){
					
					var email = document.forms.frm.elements[ 'Email' ];
					var fax = document.forms.frm.elements[ 'Faxnumber' ];
					
					if( isEmpty( email ) && isEmpty( fax ) ){ //le champ obligatoire n'est pas renseign�
						
						email.style.backgroundColor = errorBackgroundColor;
						email.style.color = errorColor;
						fax.style.backgroundColor = errorBackgroundColor;
						fax.style.color = errorColor;
						
						alert( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "gest_com_email_or_faxnumber_required" ) ) ?>' );
						
						return false;
						
					}
					
					email.style.backgroundColor = requiredBackgroundColor;
					email.style.color = requiredColor;
					fax.style.backgroundColor = requiredBackgroundColor;
					fax.style.color = requiredColor;
					
					return true;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function checkMail(){
					
					var email = document.forms.frm.elements[ 'EmailToCheck' ].value;
					
					if( email == '' )
						return;
					
					var options ='left=300, top=300, directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, toolbar=no, width=350, height=150';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/checkmail.php?type=<?php echo $type ?>&email=' + email;
					
					window.open( location, '_blank', options );
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function checkFax(){
					
					var fax = document.forms.frm.elements[ 'FaxToCheck' ].value;
					
					if( fax == '' )
						return;
					
					var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, toolbar=no, width=350, height=150';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/checkfax.php?type=<?php echo $type ?>&fax=' + fax;
					
					window.open( location, '_blank', options ) ;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//Pour le t�l�chargement du mod�le
				function popupletter(key) {
					
					postop = (self.screen.height-300)/2;
					posleft = (self.screen.width-250)/2;
					var letter=document.getElementById('letter').value;
					var contact=document.getElementById('idContact').value;
					window.open('contact_letter.php?key='+key+'&letter='+letter+'&contact='+contact, '_blank', 'top=' + postop + ',left=' + posleft + ',width=250,height=300,resizable,scrollbars=yes,status=0');
					
				}
			
				//Pour cr�er un nouveau mod�le
				function popupNewModelLetter(key) {
					
					postop = (self.screen.height-300)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_letter.php?key='+key+'&letter=new&contact=new', '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=300,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour supprimer un mod�le
				function popupDelModelLetter(){
					
					var select=document.getElementById('letter');
					var letter=document.getElementById('letter').value;
					
					var index=letter.indexOf(".", 0);
					strletter=letter.substring(0, index);
					
					var cherche = /balise/i;
					var resultat = cherche.test(strletter);
					
					if(!resultat){
						if(confirm("<?php echo Dictionnary::translate("gest_com_confirm_delete")." ";?>"+strletter)){
							request("",select,escape(letter));
						}
					}else{
						alert("<?php echo Dictionnary::translate("contact_ModelDellerror");?>"+strletter);
					}
				
				}
				
				//Pour supprimer un mod�le sur l'affichage
				function popupDelDisplay(select){
					
					n=select.selectedIndex;
				
					for (i = n; i < Number(select.options.length-1); i++){
						 select.options[i] = new Option((select.options[ Number(i+1) ].text ),(select.options[ Number(i+1) ].value) );
					}
					
					select.options.length--; 
					
				}
				
				
				//Pour l'insertion de la lettre envoy�e
				function popupletter2(key) {
					
					postop = (self.screen.height-400)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_letter2.php?key='+key, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=400,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour l'envoie de mail
				function popupmail(key) {
					
					postop = (self.screen.height-650)/2;
					posleft = (self.screen.width-900)/2;
					var documentation=document.getElementById('documentation').value;
					window.open('contact_mailer.php?doc='+documentation+'&key='+key, '_blank', 'top=' + postop + ',left=' + posleft + ',width=900,height=650,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour la reception de mail ainsi que de lettre
				function popupreceive(key,type){
					
					postop = (self.screen.height-550)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_receive.php?key='+key+'&type='+type, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=550,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour le suivi client <-> commercial.
				function popupFollow(key){
					
					postop = (self.screen.height-800)/2;
					posleft = (self.screen.width-820)/2;
					window.open('contact_follow.php?key='+key+'&resp=0', '_blank', 'top=' + postop + ',left=' + posleft + ',width=820,height=800,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour l'envoie r�ception des fax
				function popupfax(key,type){
					
					postop = (self.screen.height-550)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_fax.php?key='+key+'&type='+type, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=550,resizable,scrollbars=yes,status=0');
					
				}
				
				////------------------------------Partie Ajax maj------------------------------
					
				function createXHR(){
					
					var req = null;  
			 		
					if(window.XMLHttpRequest){
			 			req = new XMLHttpRequest();
			
					}else if(window.ActiveXObject){
						
						try {
							req = new ActiveXObject("Msxml2.XMLHTTP");
						} catch (e)
						{
							try {
								req = new ActiveXObject("Microsoft.XMLHTTP");
							} catch (e) {}
						}
			        }
			        
			    	return req;
			    	
				}
				
				function request(type,select,name){
					
					var req = createXHR();
					req.onreadystatechange = function(){
						
						if(req.readyState == 4){
							
							if(req.status == 200){						
								
								req=req.responseText;
								
								if (req=="1")
									popupDelDisplay(select); //supprime le mod�le � l'affichage
								else
									alert("<?php  echo Dictionnary::translate("gest_com_impossible_delete_file")?>");
								
							}else{
								
								alert("<?php  echo Dictionnary::translate("gest_com_impossible_delete_file")?>");
								
							}
							
						}
						
					}; 
				 	
					var data="filename="+name;
					req.open("POST", "contact_ajax.php", true);
					req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					req.send(data);
					
				}
				
				function show_factor_infos() {
					
					var status = document.forms.frm.elements['factor_status'].options[document.forms.frm.elements['factor_status'].selectedIndex].value; 
					var factmttitlediv = document.getElementById( 'fact_mt_title' );
					var factmtdiv = document.getElementById( 'fact_mt' );
					var FactorAmountCreationDateDiv = document.getElementById( 'FactorAmountCreationDateDiv' );
					var FactorAmountCreationDateTitleDiv = document.getElementById( 'FactorAmountCreationDateTitleDiv' );
					var factdatetitlediv = document.getElementById( 'fact_date_title' );
					var factdatediv = document.getElementById( 'fact_date' );
					
					if(status=='Accepted'){ 
						
						factmttitlediv.style.display = 'block';
						factmtdiv.style.display = 'block';
						FactorAmountCreationDateDiv.style.display = 'block';
						FactorAmountCreationDateTitleDiv.style.display = 'block';
						
					}else{
						
						factmttitlediv.style.display = 'none';
						factmtdiv.style.display = 'none';
						FactorAmountCreationDateDiv.style.display = 'none';
						FactorAmountCreationDateTitleDiv.style.display = 'none';
						
					}
					
					if(status=='Refused'){ 
						
						factdatetitlediv.style.display = 'block';
						factdatediv.style.display = 'block';
						
					}else{
						
						factdatetitlediv.style.display = 'none';
						factdatediv.style.display = 'none';
						
					}
					
				}
				
				//---------------------------------------------------------------------------------------------
<?php
				
				if( isset( $key ) ){
					
?>
				// PopUp pour voir l'historique d'un client
				function seeBuyerHisto(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-750)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $key; ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
					
				}
				
				// PopUp pour l'ajout d'un contact
				function AddContact(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-1000)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $key; ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1000,height=600,resizable,scrollbars=yes,status=0');
					
				}
				
				// PopUp pour la modification d'un contact
				function UpdContact(id,idb){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-1000)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?c='+id+'&b='+idb, '_blank', 'top=' + postop + ',left=' + posleft + ',width=1000,height=600,resizable,scrollbars=yes,status=0');
					
				}
				
				// PopUp pour voir la gestion des contacts
				function seeContacts(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-1000)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $key ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1000,height=600,resizable,scrollbars=yes,status=0');
					
					return false;
					
				}
				
				// PopUp pour voir les relances du client
				function seeRelaunch(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-750)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/accounting/buyer_relaunch.php?b=<?php echo $key ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
					
					return false;
					
				}
<?php
					
				}
				
?>
			// -->
			</script>
			<script type="text/javascript">
			<!--
			
				//onglets
				
				$(function() {
				
					$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
				
				});
				
			-->
			</script>
			<div id="globalMainContent">
				<div id="CitySelection" style="display:none;"></div>
				<form action="contact_buyer.php?type=<?php echo $type ?>" method="post" name="frm" id="frm" onsubmit="return checkRequiredElements( <?php echo $type == "contact" ? "1" : "0" ?> ) && checkEmailOrFax();" enctype="multipart/form-data">
					<?php if( isset( $_REQUEST[ "IdEstimate" ] ) ){ ?><input type="hidden" name="IdEstimate" value="<?php echo intval( $_REQUEST[ "IdEstimate" ] ) ?>" /><?php } ?>
					<?php if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ ?><input type="hidden" name="VirtualEstimate" value="<?php echo intval( $_REQUEST[ "VirtualEstimate" ] ) ?>" /><?php } ?>
					<?php if( !isset( $key ) ){ ?><input type="hidden" name="newkey" value="<?php echo User::getInstance()->getId() ?>" /><?php } ?>
					<?php if( $type == "contact" ){ ?><input type="hidden" name="contact" value="1" />
					<?php }elseif( isset( $_GET[ "act" ] ) && $_GET[ "act" ] == "ptob" ){ ?><input type="hidden" name="contact" value="0" /><?php } ?>
					<?php if( isset( $key ) ){ ?><input type="hidden" name="key" value="<?php echo $key ?>" /><?php } ?>
					<input type="hidden" name="user_changed" value="0" />
					<div class="mainContent">
						<div class="topRight"></div><div class="topLeft"></div>
						<div class="content">
							<div class="headTitle">
								<p class="title">
								
<?php			
								
								if( isset( $key ) ){
									
?>
									Fiche <?php echo $type == "contact" ? "prospect" : "client" ?> n�
<?php
									
									$iderp = $rsbuy->fields("iderp");
									
									if( isset ( $iderp ) )
										echo $rsbuy->fields( "iderp" ) . "/" . $key;
									else
										echo $key;
									
								}else{
								
?>
									Cr�ation <?php echo $type == "contact" ? "prospect" : "client" ?>
<?php
										
								}
										
?>
								</p>
							</div>
							<div id="container">
								<ul class="menu">
									<li class="item">
										<a href="#name-address">Coordonn�es</a>
									</li>
									<!--<li class="item">
										<a href="#factor">Factor</a>
									</li>-->
									<?php
									if( isset($_GET[ "key" ]) ){
										?>
										<li class="item">
											<a href="#pricing">Tarification</a>
										</li>
										<?php
									}
									?>
									<li class="item">
										<a href="#rib">RIB</a>
									</li>
<?php
									
									if( isset( $key ) ){
										
?>
									<li class="item">
										<a href="#history">Historique</a>
									</li>
<?php
										
									}
									
?>
								</ul>
<?php
								
								/**********************************************************
								 *                       Coordonn�es                      *
								 **********************************************************/
								
?>
								<div class="subContent" id="name-address">
									<div style="margin:10px 0px; margin-left:-5px;">
										<?php
									
										if( $key && DBUtil::getDBValue( "contact", "buyer", "idbuyer", $key ) ){
											
											?>
											<div style="height:100%;  float:right;">
												<a class="blueLink" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $key ?>&amp;act=ptob">
													Passer en client
													<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="" style="border-style:none; vertical-align:middle;" />
												</a>
											</div>
											<?php
											
										}
										
										?>
										<?php if( isset( $key ) ){ ?><div style="margin-left:3px; margin-bottom:3px;">Date de cr�ation : <?php echo humanReadableDatetime( $rsbuy->Fields( "create_date" ) ) ?></div><?php } ?>
										<table>
<?php
											
											if( !isset( $key ) ){
												
?>
											<tr>
												<th>V�rifier l'email</th> 	
												<td>
													<input type="hidden" name="newkey" value="<?php echo User::getInstance()->getId() ?>" >
													<input type="text" name="EmailToCheck" class="textInput" />
												</td>
												<td><input type="image" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" value="ok" name="CheckEmail" onclick="checkMail(); return false;" style="vertical-align:bottom;" /></td>
											</tr>
											<tr>
												<th>V�rifier le num�ro de t�l.</th> 	
												<td><input type="text" name="FaxToCheck" class="textInput" /></td>
												<td><input type="image" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" value="ok" name="CheckFax" onclick="checkFax(); return false;" style="vertical-align:bottom;" /></td>
											</tr>
<?php
											
											}
											
?>
											<tr>
												<th><b>Commercial :</b></th>
												<td colspan="2">
<?php
													
													DrawLift_commercial( "user", isset( $key ) ? $rsbuy->fields( "iduser" ) : User::getInstance()->getId(), "", !$hasAdminPrivileges );
													
?>
												</td>
											</tr>
											<!--<tr>
												<th><b>Profil :</b></th>
												<td colspan="2">
<?php
													
													$selectedValue = isset( $right ) ? $right : false;
													
													XHTMLFactory::createSelectElement( "catalog_right", "idcatalog_right", "catalog_right" . User::getInstance()->getLang(), "idcatalog_right", $selectedValue, "0", "- S�lectionner -", "name=\"catalog_right\" onchange=\"initRequiredElements();\" onkeypress=\"initRequiredElements();\" class=\"fullWidth\"" );
													
?>
												</td>
											</tr>-->
										</table>
									</div>
									<div class="halfContent halfLeft">
										<div class="subTitleContainer">
											<p class="subTitle">Client / Contact principal</p>
											<div class="rightContainer">
												<input type="button" value="Gestion contacts" class="blueButton smallSizedText" onclick="seeContacts(); return false;" />								
											</div>
										</div>
										<div class="tableContainer clear">
											<table class="dataTable clientTable">
												<tr>
													<th>Titre</th>
													<td>
<?php  
														
														if( isset( $key ) )
															DrawLift_4( "title", $rscp->Fields( "title" ) );	
														else
															DrawLift_4( "title", "Pas de titre" );
														
?>
													</td>
													<th>Fonction</th>
													<td>
<?php
														if( isset( $key ) )
															DrawLift_1( "function", $rscp->Fields( "idfunction" ) );
														else
															DrawLift_1( "function" );
														
?>
													</td>
												</tr>
												<tr>
													<th style="width:16%;">Nom</th>
													<td style="width:34%;"><input style="text-transform:uppercase;" type="text" name="Lastname" value="<?php if( isset( $key ) ){ echo $nom; } ?>" class="textInput" /></td>
													<th style="width:16%;">Pr�nom</th>
													<td style="width:34%;"><input style="text-transform:capitalize;" type="text" name="Firstname" value="<?php if( isset( $key ) ){ echo $pnom; } ?>" class="textInput" /></td>
												</tr>
												<tr>
													<th>T�l.</th>
													<td><input type="text" name="Phonenumber" value="<?php if( isset( $key ) ){ echo $tel; } ?>" class="textInput" /></td>
													<th>Fax</th>
													<td><input type="text" name="Faxnumber" value="<?php if( isset( $key ) ){ echo $fax; } ?>" class="textInput" /></td>
												</tr>
												<tr>
													<th>E-mail</th>
													<td>
														<?php if( $usemail == 1 ){ ?><input type="text" name="Email" value="<?php echo $emailtocheck ?>" class="textInput" />
														<?php }else{ ?><input type="text" name="Email" value="<?php if( isset( $key ) ){ echo $email; } ?>" class="textInput" /><?php } ?>
													</td>
													<th>Portable</th>
													<td><input type="text" name="gsm" value="<?php if( isset( $key ) ){ echo $gsm; } ?>" class="textInput" /></td>
												</tr>
												<tr>
													<td colspan="4" class="tableSeparator"></td>
												</tr>
												<?php if( isset( $login ) && !empty( $login ) ){ ?>
												<tr>
													<th colspan="2">Identifiant</th>
													<td colspan="2"><?php echo $login ?></td>
												</tr>
												<tr>
													<th colspan="2">Mot de passe</th>
													<td colspan="2">**********</td>
												</tr>
												<?php } ?>
											</table>
											<?php if( !isset( $key ) ){ ?>Le client recevra automatiquement un mail d'enregistrement lui donnant son identifiant et son mot de passe pour acc�der � son compte sur le Front Office.<?php } ?>
											<?php if( isset( $login ) && !empty( $login ) ){ ?>
											<input type="checkbox" name="GeneratePassword" style="vertical-align:middle;" />
											G�n�rer un nouveau mot de passe
											<?php } ?>
											<?php if( isset( $key ) && ( !isset( $login ) && empty( $login ) ) ){ ?>
											<input type="checkbox" name="GenerateLogin" style="vertical-align:middle;" />
											Cr�er un compte client pour le Front Office
											<?php } ?>
										</div>
									</div>
									<div class="halfContent halfRight">
										<div class="subTitleContainer">
											<p class="subTitle">Informations g�n�rales</p>
										</div>
										<div class="tableContainer">
											<table class="dataTable clientTable">
												<!--<tr>
													<th style="width:20%;">Nom</th>
													<td colspan="3"><input type="text" name="Company" value="<?php if( isset( $key ) ){ echo $raison; } ?>" onkeyup="this.value=this.value.toUpperCase();" class="textInput" /></td>
												</tr>-->
												<tr>
													<th>Adresse</th>
													<td colspan="3"><input type="text" name="Adress" value="<?php if( isset( $key ) ){ echo $adresse; } ?>" class="textInput" /></td>
												</tr>
												<tr>
													<th>Compl�ment adresse</th>
													<td colspan="3"><textarea name="Adress_2" style="border:1px solid #AAAAAA; color:#44474e; height:30px; width:100%;"><?php if( isset( $key ) ){ echo $adresse2; } ?></textarea></td>
												</tr>
												<tr>
													<th>Code postal</th>
													<td><input type="text" name="Zipcode" value="<?php if( isset( $key ) ){ echo $cp; } ?>" onkeyup="if( this.value.length == 5 ) selectCity( this.value );" class="textInput" /></td>
													<th>Ville</th>
													<td>
														<input type="text" style="text-transform:uppercase;" name="City" id="City" value="<?php if( isset( $key ) ){ echo htmlentities( $ville ); } ?>" class="textInput" />
														<?php AutoCompletor::completeFromDB( "City", "communes", "commune" ); ?>
													</td>
												</tr>
												<tr>
													<!--<th>R�gion</th>
													<td>
<?php
													
													/*if( isset( $key ) )
														DrawLift_buyerfamily( "buyer_family", $rsbuy->Fields( "idbuyer_family" ) );
													else
														DrawLift_buyerfamily( "buyer_family" );
													*/
?>
													
													</td>-->
													<th>Pays</th>
													<td colspan="3">
<?php 
														
														$selectedValue = isset( $key ) ? $pays : DBUtil::getParameter( "default_idstate" );
														$lang = User::getInstance()->getLang();
														XHTMLFactory::createSelectElement( "state", "idstate", "name$lang", "name$lang", $selectedValue, false, false, "name=\"idstate\" id=\"idstate\"" ); 
														
?>
														<script type="text/javascript">
														<!--
															
															//mettre � jour les champs obligatoires qui d�pendent du pays s�lectionn�
															
															document.forms.frm.elements[ 'idstate' ].onkeypress = function(){ initRequiredElements(); };
															document.forms.frm.elements[ 'idstate' ].onchange = function(){ initRequiredElements(); };
															
														// -->
														</script>
													</td>
												</tr>
												<tr>
													<td colspan="4" class="tableSeparator"></td>
												</tr>
												<!--<tr>
													<th>APE/NAF</th>
													<td><input type="text" name="Naf" value="<?php if( isset( $key ) ){ echo $ape; } ?>" class="textInput" /></td>
													<th>Siret</th>
													<td><input type="text" name="siret" value="<?php if( isset( $key ) ){ echo $siret; } ?>" maxlength="14" onkeypress="return forceUnsignedIntegerValue( event );" class="textInput" /></td>
												</tr>
												<tr>
													<th colspan="2">Tva intracommunautaire</th>
													<td colspan="2"><input type="text" name="Tva" value="<?php if( isset( $key ) ){ echo $tva; } ?>" class="textInput" /></td>
												</tr>-->
											</table>
										</div>
									</div>
									<!-- <div class="halfContent halfLeft clear">	  
										<div class="subTitleContainer">
											<p class="subTitle">Informations compl�mentaires</p>
										</div>
										<div class="tableContainer">
											<table class="dataTable clientTable">
												<tr>
													<th style="width:20%;">T�l.</th>
													<td style="width:35%;"><input type="text" name="phone_standard" value="<?php if( isset( $key ) ){ echo $telstandard; } ?>" class="textInput" /></td>
													<th style="width:20%;">Fax</th>
													<td style="width:25%;"><input type="text" name="fax_number" value="<?php if( isset( $key ) ){ echo $fax; } ?>" class="textInput" /></td>
												</tr>
												<tr>
													<th>E-mail</th>
													<td colspan="3"><input type="text" name="company_mail" value="<?php if( isset( $key ) ){ echo $company_mail; } ?>" class="textInput" /></td>
												</tr>
												<tr>
													<th>Site web</th>
													<td colspan="3">
														<?php if( isset( $key ) ){ ?><a href="<?php echo $www ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" style="float:right;" /></a><?php } ?>
														<input type="text" name="www" value="<?php if( isset( $key ) ){ echo $www; } ?>" class="textInput" style="width:90%;" />
													</td>
												</tr>
												<tr>
													<td colspan="4" class="tableSeparator"></td>
												</tr>
												<tr>
													<th>Domaine d'activit�</th>
													<td>
<?php 
													
													/*if( !isset( $key ) )
														DrawLift_3( "activity" );
													else
														DrawLift_3( "activity", $rsbuy->Fields( "idactivity" ) );
													*/
?>

													</td>
													<th>Effectif</th>
													<td>
<?php
													/*
													if( isset( $key ) )
														DrawLift_5( $rsbuy->Fields( "idmanpower" ) );
													else
														DrawLift_5();
													*/
?>

													</td>
												</tr>
												<tr>
													<th>Newsletter</th>
													<td<?php if( $type != "buyer" ){ ?> colspan="3"<?php } ?>>
														<?php if( isset( $key ) && $rsbuy->fields( "newsletter" ) > 0 ) echo "Oui"; else echo "Non"; ?>
													</td>
<?php
												//if( $type == "buyer" ){
													
?>
													<th>R�f�rencement client</th>
													<td>
<?php	
													
													/*if( isset( $key ) )
														DrawLift_referenced( $buyer_ref_us );
													else
														DrawLift_referenced();*/
													
?>

													</td>
<?php
													
												//}
												
?>
												</tr>
											</table>
										</div>
								  	</div> -->
									<!--
									<div class="halfContent halfRight">
										<div class="subTitleContainer">
											<p class="subTitle orangeText">Commentaire Contact</p>
										</div>
										<div class="tableContainer">
											<textarea name="commentcp" style="border:1px solid #AAAAAA; color:#44474e; height:70px; width:100%;"><?php if( isset( $key ) ){ echo $commentairecp; } ?></textarea>
										</div>
									</div>
									-->
									<div class="clear">
									<!--<div class="halfContent halfRight">	  --> 
									
									
										<div class="subTitleContainer">
											<p class="subTitle orangeText">Commentaire Soci�t�</p>
										</div>
										<div class="tableContainer">
											<textarea name="comment" style="border:1px solid #AAAAAA; color:#44474e; height:70px; width:100%;"><?php if( isset( $key ) ){ echo $commentaire; } ?></textarea>
										</div>
									
									
									
									<!--</div>-->
										<!--
										<div class="subTitleContainer">
											<p class="subTitle">Logo client</p>
										</div>
										<div style="margin:5px;"><input type="file" name="logo" value="" /></div>
									<?php
										
										if( isset( $key ) && !empty( $logo ) && file_exists( $GLOBAL_START_PATH . $logo ) ){
											
									?>
											<div style="margin:5px;"><img src="<?php echo $GLOBAL_START_URL . $logo ?>" alt="" /></div>
									<?php
											
										}
										
									?>
									-->
									</div>
									<div class="clear"></div>
									<div class="submitButtonContainer">
										<input type="submit" name="Modify" class="blueButton" value="<?php echo $submit ?>" />
									</div>
								</div>
								<?php
								
								/**********************************************************
								 *                         Factor                         *
								 **********************************************************/
								
								/*$factor_url = DBUtil::getParameterAdmin( "ad_factor_url" );
								
								if( isset( $key ) ){
									
									if( $factor_status == "Accepted" ){
										
										$display_amount = "block";
										$display_factor_amount = true;
										
									}else{
										
										$display_amount = "none";
										$display_factor_amount = true;
										
									}
									
									if( $factor_status == "Refused" )
										$display_refusal = "block";
									else
										$display_refusal = "none";
									
								}else{
									
									$display_refusal = "none";
									$display_amount = "none";
									$display_factor_amount = false;
									
								}*/
								
								?>
								<!--<div class="subContent" id="factor">
									<div style="margin:10px 0px; margin-left:-5px;"></div>
									<?php if( !empty( $factor_url ) ){ ?><div><a href="<?php echo $factor_url ?>" onclick="window.open(this.href); return false;" class="normalLink blueText">Site Web du factor</a></div><?php } ?>
									<div class="tableContainer">
										<table class="dataTable clientTable">
											<tr>
												<th style="width:25%;">Factor n�</th>
												<td style="width:25%;"><input type="text" name="factor" maxlength="20" value="<?php if( isset( $key ) ){ echo $factor; } ?>" class="textInput" /></td>
												<th style="width:25%;">
													<div id="fact_mt_title" style="display:<?php echo $display_amount ?>;">
														Encours accord� (TTC)
													</div>
												</th>
												<td style="width:25%;">
													<div id="fact_mt" style="display:<?php echo $display_amount ?>;">
														<input type="text" name="factor_amount" maxlength="10" value="<?php if( isset( $key ) ){ echo $factor_amount; } ?>" class="textInput" style="width:60px;" /> &euro;
													</div>
												</td>
											</tr>
											<tr>
												<th>Statut</th>
												<td>
<?php
													
													/*if( isset( $key ) )
														Drawlift_factor_status( $factor_status );	
													else
														Drawlift_factor_status();*/
													
?>
												</td>
												<th>
													<div id="FactorAmountCreationDateTitleDiv" style="display:<?php echo $display_amount ?>;">
														Date d'accord de l'encours
													</div>
													<div id="fact_date_title" style="display:<?php echo $display_refusal ?>;">
														Date de refus
													</div>
												</th>
												<td>
													<div id="FactorAmountCreationDateDiv" style="display:<?php echo $display_amount ?>;">
<?php
														
														/*if( !empty( $factor_amount_creation_date ) && $factor_amount_creation_date!= "0000-00-00" ){
															 
															$value = usDate2eu( $factor_amount_creation_date );
															$startdate = date( $factor_amount_creation_date );
															
														}else{
															
															$value = "";
															$startdate = date( "Y-m-d" );
															
														}*/
														
?>
														<input type="text" name="factor_amount_creation_date" id="factor_amount_creation_date" value="<?php echo $value ?>" readonly="readonly" class="calendarInput" />
														<?php DHTMLCalendar::calendar( "factor_amount_creation_date" ); ?>
													</div>
													<div id="fact_date" style="display:<?php echo $display_refusal ?>;">
<?php
														
														if( !empty( $factor_refusal_date ) && $factor_refusal_date != "0000-00-00" ){
															
															$value = usDate2eu( $factor_refusal_date );
															$startdate = $factor_refusal_date;
															
														}else{
															
															$value = "";
															$startdate = date( "Y-m-d" );
															
														}
														
?>
														<input type="text" name="factor_refusal_date" id="factor_refusal_date" value="<?php echo $value ?>" class="calendarInput" readonly="readonly" />
														<?php DHTMLCalendar::calendar( "factor_refusal_date" ); ?>
													</div>
												</td>
											</tr>
<?php
											
											if( isset( $key ) && $display_factor_amount ){
												
?>
											<tr>
												<th>Solde de l'encours (TTC)</th>
												<td colspan="3"><?php echo Util::priceFormat( getBuyerFactorBalance( $key, $factor_amount ) ) ?></td>
											</tr>
<?php
												
											}
											
?>
										</table>
									</div>
									<div class="submitButtonContainer">
										<input type="submit" name="Modifiy" class="blueButton" value="<?php echo $submit ?>" />
									</div>
								</div>-->
<?php
								
								/**********************************************************
								 *                      Tarification                      *
								 **********************************************************/
								
?>
								<div class="subContent" id="pricing">
									<div style="margin:10px 0px; margin-left:-5px;"></div>
<?php
								
								if( isset( $key ) ){
									
?>
									<div class="subTitleContainer">
										<p class="subTitle">R�glement</p>
									</div>
									<div class="tableContainer">
										<table class="dataTable clientTable">
											<tr>
												<th style="width:25%;">D�lais de paiement</th>
												<td>
<?php
													
													if( isset( $key ) )
														DrawLift_buyerdelay( "payment_delay", $delivpayment );
													else
														DrawLift_buyerdelay( "payment_delay", 4 );
													
?>
												</td>
											</tr>
											<tr>
												<th style="width:25%;">Moyen de paiement</th>
												<td>
<?php
													
													if( isset( $key ) )
														DrawLift_buyercond( "payment", $condpayment );
													else
														DrawLift_buyercond( "payment", 4 );
													
?>
												</td>
											</tr>
											<tr>
												<th style="width:25%;">Famille d'acheteur</th>
												<td>
<?php
													
													if( isset( $key ) )
														DrawLift_buyerfamily( "buyer_family", $rsbuy->Fields( "idbuyer_family" ) );
													else
														DrawLift_buyerfamily( "buyer_family" );
													
?>
												</td>
											</tr>
										</table>
									</div>
									<div class="submitButtonContainer">
										<input type="submit" name="Modify" class="blueButton" value="<?php echo $submit ?>" />
									</div>
<?php
									
								}
								
?>
								</div>
<?php
								
								/**********************************************************
								 *                           RIB                          *
								 **********************************************************/
								
?>
								<div class="subContent" id="rib">
									<div style="margin:10px 0px; margin-left:-5px;"></div>
									<div class="tableContainer">
										<table class="dataTable clientTable">
											<tr>
												<th>Code �tablissement</th>
												<td><input type="text" name="code_etabl" maxlength="5" value="<?php if( isset( $key ) ){ echo $code_etabl; } ?>" class="textInput" style="width:35px;" /></td>
												<th>Code guichet</th>
												<td><input type="text" name="code_guichet" maxlength="5" value="<?php if( isset( $key ) ){ echo $code_guichet; } ?>" class="textInput" style="width:35px;" /></td>
											</tr>
											<tr>
												<th>N� Compte</th>
												<td><input type="text" name="n_compte" maxlength="11" value="<?php if( isset( $key ) ){ echo $n_compte; } ?>" class="textInput" style="width:70px;" /></td>
												<th>Cl� RIB</th>
												<td><input type="text" name="cle_rib" maxlength="2" value="<?php if( isset( $key ) ){ echo $cle_rib; } ?>" class="textInput" style="width:15px;" /></td>
											</tr>
											<tr>
												<th>Domiciliation</th>
												<td colspan="3"><input type="text" name="domiciliation" maxlength="40" value="<?php if( isset( $key ) ){ echo $domiciliation; } ?>" class="textInput" style="width:245px;" /></td>
											</tr>
										</table>
									</div>
									<div class="submitButtonContainer">
										<input type="submit" name="Modify" class="blueButton" value="<?php echo $submit ?>" />
									</div>
								</div>
<?php
								
								/**********************************************************
								 *                       Historique                       *
								 **********************************************************/
								
								if( isset( $key ) ){
									
?>
								<div class="subContent" id="history">
									<div style="margin:10px 0px; margin-left:-5px;"></div>
<?php
									
									$included_history = true;
									// On est en mode B2C
									include( "$GLOBAL_START_PATH/sales_force/B2C/buyer_history.php" );
									
?>
								</div>
<?php
									
								}
								
?>
							</div><!-- container -->
						</div>
						 <!-- on coupe le flux -->
						<div class="clear"></div>
						<div class="bottomRight"></div><div class="bottomLeft"></div>
					</div>
<?php
					
					/**********************************************************
					 *                     Boite � outils                     *
					 **********************************************************/
					
?>
					<div id="tools" class="rightTools">
						<div class="toolBox">
							<div class="header">
								<div class="topRight"></div><div class="topLeft"></div>
								<p class="title">Outils</p>
							</div>
							<div class="content" style="line-height:15px; padding:5px;">
<?php
								
								if( $type == "contact" && isset( $key ) ){
									
?>
								<a href="#" onclick="if( !checkRequiredElements( 0 ) || !checkEmailOrFax() ) return false; else document.forms.frm.onsubmit = function(){ document.forms.frm.action = document.forms.frm.action + '&amp;act=ptob'; return true; };" class="normalLink blueText">Passer le prospect en client</a><br />
<?php 
									
								}
								
								if( isset( $key ) ){
									
									//provenance  = devis express
									
									/*if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){
										
?>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo intval( $_REQUEST[ "VirtualEstimate" ] ) ?>" class="normalLink blueText">Retour au devis</a><br />
<?php	
										
									}
									
									if( isset( $_REQUEST[ "IdEstimate" ] ) ){
										
?>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo intval( $_REQUEST[ "IdEstimate" ] ) ?>" class="normalLink blueText">Retour au devis</a><br />
<?php	
										
									}*/
									
?>
								<!--<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $key ?>&idcontact=0" class="normalLink blueText">Cr�er un devis</a><br />-->
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $key ?>&idcontact=0" class="normalLink blueText">Cr�er une commande</a><br />
<?php
									
									if( $relaunched ){
										
?>
								<a href="#" style="color:#FF0000; font-weight:bold;" onClick="seeRelaunch(); return false;">Client relanc�</a>
<?php
										
									}
									
?>
<?php
            						
            					}
            					
?>
							</div>
							<div class="footer">
								<div class="bottomLeft"></div>
								<div class="bottomMiddle"></div>
								<div class="bottomRight"></div>
							</div>
						</div>
						<div class="toolBox">
							<div class="header">
								<div class="topRight"></div><div class="topLeft"></div>
								<p class="title">Infos Plus</p>
							</div>
							<div class="content">
								<!--<div class="subTitleContainer" style="margin-top:0px;">
									<p class="subTitle">Scoring</p>
								</div>
								<table class="infoPlus">
									<tr>
										<td>Scoring</td>
										<td>
<?php 
										
										/*if( isset( $key ) )
											DrawLift_scoring_buyer( $idscoring_buyer );
										else
											DrawLift_scoring_buyer();
										*/
?>
										</td>
									</tr>
								</table>-->
								<div class="subTitleContainer" style="margin-top:0px;">
									<p class="subTitle">Provenance</p>
								</div>
								<div style="margin:0px 5px 0px 5px;">
								
<?php
								if( isset( $key ) )
									echo getStrSource( $rsbuy->fields( "idsource" ) );
								else
									
									DrawLift_6( "source", 0, "ORDER BY display_order ASC" );
								
?>
								</div>
							</div>
							<div class="footer">
								<div class="bottomLeft"></div>
								<div class="bottomMiddle"></div>
								<div class="bottomRight"></div>
							</div>
						</div>
					</div>
					<script type="text/javascript" language="javascript">
					<!--
					
						initRequiredElements();
					
					// -->
					</script>
				</form>
			</div>
<?php

/**************************************************************
*                                                             *
* Code de Thomas pour le suivi de courrier du client supprim� *
*                                                             *
***************************************************************

if(isset($_GET["key"])){
		
	//--------------------------------------reception
	?>
	<div id="titleAndLetterManagement" style="width:140px; float:left; margin-left:10px;">
		<div><?php echo Dictionnary::translate( "doc_management_send" );?></div><br />
		<div class="div_letter_mail_Management">
			<b><?php echo Dictionnary::translate( "doc_management_ByMail" );?></b><hr/>
			<label for="documentation">Mod�le :</label><br />
			<?php displayMail($key);?>
			----- <a href="#" onclick="popupmail(<?php echo $key;?>);return false;" ><?php echo Dictionnary::translate( "to_send" );?></a>  
			<br />
			<br />
			<b><?php echo Dictionnary::translate( "doc_management_ByFax" );?></b><hr/>
				<a href="#" onclick="popupfax(<?php echo $key;?>,'send');return false;" ><?php echo Dictionnary::translate( "doc_management_save" );?></a>  
			<br />
			<br />
			<b><?php echo Dictionnary::translate( "doc_management_ByLetter" );?></b><hr/>
			<label for="idContact">Destinataire :</label><br />
			<?php contact($key);?><br /><br />
			<?php displayletter($key);?><br /><br />
			<a href="#" onclick="popupletter(<?php echo $key;?>);return false;" ><?php echo Dictionnary::translate( "Download" );?></a> 
			<a href="#" onclick="popupNewModelLetter(<?php echo $key;?>);return false;" ><?php echo Dictionnary::translate( "doc_management_newModel" );?></a>
			<a href="#" onclick="popupDelModelLetter();return false;" ><?php echo Dictionnary::translate( "delete" );?></a>
			<br />
			<br />
			<b><?php echo Dictionnary::translate( "doc_management_SaveTheLetter" );?></b><hr/>
			<a href="#" onclick="popupletter2(<?php echo $key;?>);return false;" ><?php echo Dictionnary::translate( "doc_management_save" );?></a>
		</div>
		<br />
		<br />
		<?php
		//--------------------------------------Envoie
		?>
		<div><?php echo Dictionnary::translate( "doc_management_receive" );?></div><br />
		<div class="div_letter_mail_Management">
			<b><?php echo Dictionnary::translate( "doc_management_mail" );?></b><hr/>
			<a href="#" onclick="popupreceive(<?php echo $key.',\'mail\'';?>);return false;" ><?php echo Dictionnary::translate( "doc_management_save" );?></a>
			<br />
			<br />
			<b><?php echo Dictionnary::translate( "fax" );?></b><hr/>
			<a href="#" onclick="popupfax(<?php echo $key;?>,'receive');return false;" ><?php echo Dictionnary::translate( "doc_management_save" );?></a>
			<br />
			<br />
			<b><?php echo Dictionnary::translate( "doc_management_letter" );?></b><hr/>
			<A href="#"  onclick="popupreceive(<?php echo $key.',\'letter\'';?>);return false;" ><?php echo Dictionnary::translate( "doc_management_save" );?></a>
		</div>
		<br />
		<br />
		<?php
		//--------------------------------------Suivi
		?>	
		<div><?php echo Dictionnary::translate( "doc_management_monitoring" );?></div><br />
		<div class="div_letter_mail_Management">
			<a href="#"  onclick="popupFollow(<?php echo $key;?>);return false;" ><?php echo Dictionnary::translate( "doc_management_see" );?></a>
		</div>
	</div>
	<?php	
						
}
*/

?>