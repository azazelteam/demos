<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "Updqte_", true );
anchor( "UpdateRefDiscount_", true ); 
anchor( "UpdateOrderPriceWD_", true ); 
anchor( "Supprimer" );

//----------------------------------------------------------------------------------------------------------------

/* n'autoriser la suppression de lignes article que pour les commandes de réapprovisionnement */

if( !$Order->get( "idorder" ) ){
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function confsupp( idrow, reference ){
		
			var ret = confirm( '<?php echo Dictionnary::translate("gest_com_confirm_delete_order_suplier"); ?> : ' + reference );
			
			if( ret )
				document.forms.frm.elements[ 'Delete_' + idrow ].value = 1;
			
			return ret;
			
		}
	
	/* ]]> */
	</script>
	<?php
	
}

?>
<a name="references"></a>
<div class="contentResult" style="margin-bottom: 10px;">

	<h1 class="titleEstimate"><span class="textTitle">Descriptif de la commande</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>       
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">
	
		<?php if( $Order->get( "internal_supply" ) && !strlen( $disabled ) ){ /* n'autoriser l'ajout de lignes article que pour les commandes de réapprovisionnement */ ?>
			
			<script type="text/javascript">
			<!--
				//onglets
				$(function() {
					$('#containerEstimate').tabs({ fxFade: true, fxSpeed: 'fast' });
				});
			-->
			</script>
			<div id="containerEstimate">
			
			<ul class="menu">
	            <li class="item"><a href="#search-product"><span>Rechercher par critères</span></a></li>
	            <li class="item"><a href="#create-product"><span>Créer un produit</span></a></li>
	            <!--<li class="item"><a href="#search-cata" onclick="$('#CatalogFrame').attr('src','/');"><span>Rechercher sur le catalogue</span></a></li>-->
			</ul>
			<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0 8px -5px; width: 101%;"> </div>
			<div class="spacer"></div>
			
			<!-- ********************* -->
			<!-- Rechercher un produit -->
			<!-- ********************* -->
			<div id="search-product">
			
				<script type="text/javascript"> 
				/* <![CDATA[ */
					function searchProductOrderSup() {
						
						var data = $('#searchEstimateP').formSerialize(); 
						$.ajax({
							url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/cat_select.php?searchOrderSupplier&IdOrder=<?php echo $IdOrder; ?>",
							async: true,
							type: "POST",
							data: data,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de rechercher un produit" ); },
						 	success: function( responseText ){
						 		$("#responseSearch").html(responseText); 		
							}
						});
					}
				/* ]]> */
				</script>	
				<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/cat_select.js"></script>
						
				<?php $lang = User::getInstance()->getLang(); ?>
				<form name="searchEstimateP" id="searchEstimateP" action="#" method="post">
				<input type="hidden" name="search" value="1" />
				<input type="hidden" name="idorder" value="<?php echo $IdOrder ?>">
				
				<div class="blocMultiple blocMLeft">
	        		<h2 style="margin-top:5px;">&#0155; Par informations sur le produit :</h2>
	        		<label><span><?php echo Dictionnary::translate( "gest_com_product_type" ) ?></span></label>
	        		<select name="type">
	        			<option value=""<?php if( !isset( $_REQUEST[ "type" ] ) || $_REQUEST[ "type" ] == "" ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_all" ) ?></option>
	        			<option value="catalog"<?php if( isset( $_REQUEST[ "type" ] ) && $_REQUEST[ "type" ] == "catalog" ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_catalog" ) ?></option>
	        			<option value="specific"<?php if( isset( $_REQUEST[ "type" ] ) && $_REQUEST[ "type" ] == "specific" ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_specific" ) ?></option>
	        			<option value="alternativ"<?php if( isset( $_REQUEST[ "type" ] ) && $_REQUEST[ "type" ] == "alternativ" ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_alternative" ) ?></option>
	        			<option value="destocking"<?php if( isset( $_REQUEST[ "type" ] ) && $_REQUEST[ "type" ] == "destocking" ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_destocking" ) ?></option>
	        		</select>
	        		
	        		<div class="spacer"></div>
	        		
	        		<label><span><?php echo Dictionnary::translate("product_name") ; ?></span></label>
	        		<input type="text" name="name<?php echo $lang ?>" value="<?php if( isset( $_POST[ "name$lang" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "name$lang" ] ); ?>" class="textidInput" />
		        		
	        		<label class="smallLabel"><span><?php echo Dictionnary::translate("reference") ; ?></span></label>
	        		<input type="text" name="reference" value="<?php if( isset( $_POST[ "reference" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "reference" ] ); ?>" class="textidInput" />
	        		<div class="spacer"></div>
	        	</div>
	        	
	        	<div class="blocMultiple blocMRight">
	        		<h2 style="margin-top:5px;">&#0155; Par informations sur le fournisseur :</h2>
	        		
	        		<label><span><?php echo Dictionnary::translate("ref_supplier") ; ?></span></label>
	        		<input type="text" name="ref_supplier" value="<?php if( isset( $_POST[ "ref_supplier" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "ref_supplier" ] ); ?>" class="textInput" />
					<div class="spacer"></div>
					
					
					<label><span><?php echo Dictionnary::translate("supplier") ; ?></span></label>
					<input type="hidden" name="idsupplier" value="<?php echo $Order->getSupplier()->getId() ?>" />
					<?php echo htmlentities( $Order->getSupplier()->get( "name" ) ) ?>
					
					<div class="spacer"></div>				
	        	</div>
	        	<div class="spacer"></div>
	        	
	        	<div class="blocMultiple blocMLeft">
	        		<h2>&#0155; Par caractéristiques produits :</h2>
	        		
	        		<label><span><?php echo Dictionnary::translate("designation") ; ?></span></label>
	        		<input type="text" name="designation<?php echo $lang ?>" value="<?php if( isset( $_POST[ "designation$lang" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "designation$lang" ] ); ?>" class="textInput" />
	        		<div class="spacer"></div>
	        		
	        		<label><span><?php echo Dictionnary::translate("keyword") ; ?></span></label>
					<input type="text" name="key_word<?php echo $lang ?>" value="<?php if( isset( $_POST[ "key_word$lang" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "key_word$lang" ] ); ?>" class="textidInput" />
					
					<label class="smallLabel"><span><?php echo Dictionnary::translate("gest_com_amount") ; ?></span></label>
					<div class="simpleSelect">
					<select name="sellingcost">
						<option value="">-</option>
						<option value="0/100">de 0 à 100</option>
						<option value="100/200">de 100 à 200</option>
						<option value="200/500">de 200 à 500</option>
						<option value="500/1000">de 500 à 1000</option>
						<option value="1000/2000">de 1000 à 2000</option>
						<option value="2000/5000">de 2000 à 5000</option>
					</select>
					</div>
					<div class="spacer"></div>
					
					<!-- <label><span><?php echo Dictionnary::translate( "category" ) ?></span></label>
					<input type="hidden" name="idcategory" value="<?php if( isset( $_POST[ "idcategory" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "idcategory" ] ); ?>" >
					<input type="text" name="categoryname" value="<?php if( isset( $_POST[ "categoryname" ] ) && isset( $_POST[ "search" ] ) ) echo stripslashes( $_POST[ "categoryname" ] ); ?>" readonly="readonly" class="textInput" style="width:450px;" /> 
					<a href="#" Onclick="window.open('sse_cat_select.php','ssse_cat_select','width=260,height=300,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');">Sélectionner la catégorie</a> 
					<a href="#" onclick="resetCategory(); return false;">Effacer</a>
					<div class="spacer"></div> -->
				</div>
				
				<input type="button" onclick="return searchProductOrderSup();" name="SSESearch" class="blueButton floatright" value="Rechercher" />
				</form>
				<div class="spacer"></div>
				
				<!-- Resultat de la recherche -->
				<div id="responseSearch"></div>
				<div class="spacer"></div>
				
				
			</div> <!-- #search-product -->
			
			
			<!-- ********************* -->
			<!--   Créer un produit    -->
			<!-- ********************* -->
			<div id="create-product" style="display:none;">
				<script type="text/javascript"> 
				/* <![CDATA[ */
					function addProduct() {
						
						var data = $('#frmCreate').formSerialize(); 
						$.ajax({
							url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/new_article.php?IdOrder=<?php echo $IdOrder; ?>",
							async: true,
							type: "POST",
							data: data,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de rechercher un produit" ); },
						 	success: function( responseText ){
						 		$("#responseAddProduct").html(responseText); 		
							}
						});
					}
				/* ]]> */
				</script>	
				
				<div class="blocMultiple blocMLeft">
				<form id="frmCreate" action="#" method="post" enctype="multipart/form-data">
					<input type="hidden" name="IdEstimate" value="<?php echo $IdEstimate ?>" />
					<input type="hidden" name="AddReference" value="On" />
				
					<label><span><?php echo Dictionnary::translate('type') ?></span></label>
					<select name="type">
						<option value="alternativ"><?php echo Dictionnary::translate('alternative') ?></option>
						<option value="specific"><?php echo Dictionnary::translate('specific') ?></option>
					</select>
					<div class="spacer"></div>
					
					
					<input type="button" onclick="return addProduct();" style="margin:5px 0;" class="blueButton floatright" value="Sélectionner" name="AddReference" />
				</form></div>	
				<div class="spacer"></div>
				<div id="responseAddProduct"></div>
				<div class="spacer"></div>
				
			</div>
			<div class="spacer"></div>
			
			<!-- ********************* -->
			<!-- Rechercher  catalogue -->
			<!-- ********************* -->
			<div id="search-cata" style="display:none;">
				<iframe src="" width="98%" height="400" name="iframe" id="CatalogFrame" style="border:1px solid #CCCCCC;"></iframe>
				<div class="spacer"></div>		
			</div><!-- #search-cata -->
			<div class="spacer"></div>	
			
			</div> <!-- containerEstimate -->
			<div class="spacer"></div><br/>
			
		<?php }  ?>
		
		
		
		<form action="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $Order->getId(); ?>" method="post" name="frm" id="frm" onsubmit="if(this.elements[ 'idpayment_delay' ].options[this.elements[ 'idpayment_delay' ].selectedIndex].value == 0){alert('Veuillez sélectionner un délai de paiement !'); return false;};"enctype="multipart/form-data">
		<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $IdOrder ?>" />
		<input type="hidden" name="ModifyOrder" value="1" />
		
		
		<?php
			/*lignes article*/
			$n = $Order->getItemCount();
			for ($i=0;$i<$n;$i++) {
				displayItem( $Order, $Order->getItemAt( $i ), $i, $disabled );
			}
		?>
	</div></div>
</div><!-- mainContent -->
<?php

//----------------------------------------------------------------------------------------------------------------

function displayItem( SupplierOrder $order, &$item, $i, $disabled ){

	global $GLOBAL_START_URL;
	
	if( !$order->get( "idorder" ) ){ /* n'autoriser la suppression de lignes article que pour les commandes de réapprovisionnement */
		
		?>
		<input type="hidden" name="Delete_<?php echo $i ?>" value="0" />
		<?php
		
	}
	
	?>
	<input type="hidden" name="useimg_<?=$i?>" value="<?php echo $item->get( "useimg") ?>" />
	<div class="tableContainer" style="margin-bottom:5px;margin-top:15px;">
		<table class="dataTable devisTable">
			<tr>
				<th style="width:75px; height:40px;">Photo</th>
				<th>Réf. & Désignation courte</th>
				<th>
			    	<?php if($item->get('quantity_per_linear_meter') > 0 || $item->get('quantity_per_truck') > 0 || $item->get('quantity_per_pallet') > 0) { ?>
	            	<style type="text/css">
	            		.tooltipfrshover2 { background-color:#FFFFFF; border:1px solid lightgray; text-align:left; padding:0 5px;
							color:#8B9298; display:block; left:70px; position:absolute; top:-7px; width:170px; z-index:99;
						}
	            	</style>
	            	<span onmouseover="document.getElementById('popfrs<?php echo $item->get( "idrow" ) ?>').className='tooltipfrshover2';" onmouseout="document.getElementById('popfrs<?php echo $item->get( "idrow" ) ?>').className='tooltipfrs';" style=" position:relative; cursor:pointer; ">
	                	<?php echo $item->get( "lot" ) > 1 ? "Qté / lot" : "Quantité" ?> <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" onmouse style="border-style: none; vertical-align: text-bottom;" alt="" />
	                	
	                	<span id='popfrs<?php echo $item->get( "idrow" ) ?>' class='tooltipfrs' style="font-size:11px;">
			    		<?php if($item->get('quantity_per_linear_meter') > 0) { ?>
	                		Quantité par ML : <?php echo $item->get('quantity_per_linear_meter') ?><br />
	                	<?php } ?>
	                	<?php if($item->get('quantity_per_truck') > 0) { ?>
	                		Quantité par camion : <?php echo $item->get('quantity_per_truck') ?><br />
	                	<?php } ?>
	                	<?php if($item->get('quantity_per_pallet') > 0) { ?>
	                		Quantité par palette : <?php echo $item->get('quantity_per_pallet') ?><br /> 
	                	<?php } ?></span>
	                </span>
	            	<?php } else { ?>
	            		<?php echo $item->get( "lot" ) > 1 ? "Qté / lot" : "Quantité" ?>                            	
	            	<?php } ?>    									
				</th>
				<th>Px Tarif<br/>fournisseur</th>
				<th>Remise<br/>fournisseur</th>
				<th>Prix tarif <br />HT</th>
				<th>Remise<br />supplémentaire</th>
				<th>Prix d'achat<br />unitaire net</th>
				<th>Total prix<br />d'achat HT</th>
			</tr>
			<tr>
				<td rowspan="3"><?php displayReferenceThumb( $item ); ?></td>
				<td style="text-align:left;"><?php displayReferenceInfos( $item ); ?></td>
				<td rowspan="3"> <?php echo $item->get( "min_cde_supplier" ); ?>
				<?php
				
					if( $order->get( "idorder" ) )
						echo $item->get( "lot" ) > 1 ? $item->get( "lot" ) . " / " . $item->get( "unit" ) : $item->get( "quantity" );
					else{
						
						?>
						<input  <?php echo $disabled ?>  type="text" name="quantity_<?php echo $i; ?>" class="textInput" style="width:70px;" value="<?php echo $item->get( "quantity" ); ?>" onkeypress="return forceUnsignedIntegerValue( event );" />
						<?php
						
					}
					
				?>
				</td>
				<td><?php echo Util::priceFormat( $item->get( "suppliercost" ) ) ?></td>
				<td><?
					$supplier_discount_rate = $item->get( "suppliercost" ) > 0.0 ? ( $item->get( "suppliercost" ) - $item->get( "buyingcost" ) ) * 100.0 / $item->get( "suppliercost" ) : 0.0;
					echo Util::rateFormat( $supplier_discount_rate );
				?></td>
				<td>
					<input  <?php echo $disabled ?> type="text" class="textInput" style="width:60px;" name="unit_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $item->get( "unit_price" )) ?>" onkeypress="return forceUnsignedFloatValue( event );" />&nbsp;&euro;
					<input type="hidden" name="old_unit_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $item->get( "unit_price" ))?>" />
				</td>
				<td>
					<input  <?php echo $disabled ?> type="text" class="textInput alignCenter" style="width:60px;" name="ref_discount_<?php echo $i ?>" value="<?php echo Util::numberFormat( $item->get( "ref_discount" ) ) ?>" onkeypress="return forceUnsignedFloatValue( event );" />&nbsp;%
					<input type="hidden" name="old_ref_discount_<?php echo $i ?>" value="<?php echo Util::numberFormat( $item->get( "ref_discount" ) ) ?>" />
				</td>
				<td>
					<input  <?php echo $disabled ?> type="text" class="textInput" style="width:60px;" name="os_discount_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $item->get( "discount_price" ) ) ?>" onkeypress="return forceUnsignedFloatValue( event );" />&nbsp;&euro;
					<input type="hidden" name="old_os_discount_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $item->get( "discount_price" ) ) ?>" />
				</td>
				<td><?php echo Util::priceFormat( $item->get( "discount_price" ) * $item->get( "quantity" ) ) ?></td>				
			</tr>
			<tr>
				<td rowspan="2" style="width:210px; height:110px;">
					<textarea <?php echo $disabled ?> name="designation_<?php echo $i ?>" style="width:210px; height:110px; border-style:none;"><?php echo stripDesignationTags( $item ) ?></textarea>
				</td>
				<th>Longueur<br/>hors-tout</th>
				<th>Largeur<br/>hors-tout</th>
				<th style="height:40px;">Surface<br/>au sol</th>
				<th>Poids unitaire</th>
				<th>Délai d'expédition</th>
				<th>Départ usine</th>
			</tr>
			<tr>
				<td><input  <?php echo $disabled ?> type="text" class="textInput alignCenter" style="width:60px;" name="width_<?php echo $i ?>" value="<?php echo $item->get( "width" ) ?>" onkeypress="return forceUnsignedFloatValue( event );" /></td>
				<td><input  <?php echo $disabled ?> type="text" class="textInput alignCenter" style="width:60px;" name="length_<?php echo $i ?>" value="<?php echo $item->get( "length" ) ?>" onkeypress="return forceUnsignedFloatValue( event );" /></td>
				<td><input  <?php echo $disabled ?> type="text" class="textInput alignCenter" style="width:60px;" name="surface_<?php echo $i ?>" value="<?php echo $item->get( "surface" ) ?>" onkeypress="return forceUnsignedFloatValue( event );" /></td>
				<td><?php echo $item->get( "weight" ) * $item->get( "quantity" ) ?> kg</td>
				<?php $enDeliv = $disabled ? false : true;	?>
				<td><?php listDelivDelay( $i, $item->get( "delivdelay" ), $enDeliv); ?></td>
				<td><?php displayAvailability( $order, $item, $i ); ?></td>
			</tr>
			<tr>
				<th colspan="9" style="text-align:left;">Commentaires&nbsp;
				<?php
						
					$displayComment = strlen( $item->get( "comment" ) ) ? " display:block; " : " display:none; ";
					$AffMask = strlen( $item->get( "comment" ) ) ? "[Masquer]" : "[Afficher]";
					
					?>
					<a href="#" id="CommentDiv_<?=$i?>Link" onclick="visibility('CommentDiv_<?=$i?>','[Afficher]','[Masquer]'); return false;"><?php echo $AffMask ?></a>
				</th>
			</tr>
			<tr>
				<td colspan="9">
					<div id="CommentDiv_<?=$i?>" style="<?php echo $displayComment; ?>">
						<textarea  <?php echo $disabled ?> id="comment_<?php echo $i ?>" name="comment_<?php echo $i ?>" style="width:100%; height:50px; border:none;"><?php echo $item->get( "comment" ) ?></textarea>
					</div>
				</td>
			</tr>
			
		</table>
	</div><!-- tableContainer -->
	<?php
	
		if( !$order->get( "idorder" ) ){ /* n'autoriser la suppression de lignes article que pour les commandes de réapprovisionnement */
			
			?>
			<div class="leftContainer">
		        <input  <?php echo $disabled ?> type="submit" class="blueButton" value="Supprimer" onclick="return confsupp(<?php echo $i ?>,'<?php echo $item->get( "reference" )?>');" />
		    </div>
			<?php
			
		}
	
	?>
    <div class="rightContainer">
        <input type="button" onclick="window.open('<?php echo "$GLOBAL_START_URL/statistics/statistics.php?cat=product&reference=" . urlencode( $item->get( "reference" ) ) . "&amp;period&amp;StatStartDate=" . urlencode( date( "d/m/Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ) ) . "&amp;StatEndDate=" . urlencode( date( "d/m/Y" ) ) ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px; margin-right:5px;"/>
        <input  <?php echo $disabled ?> type="submit" class="blueButton" name="" value="Recalculer" />
    </div>
    <div class="clear"></div>
	<?php
	
	DHTMLCalendar::calendar( "availability_date_$i", false );
				
}

//----------------------------------------------------------------------------------------------------------------

function getAvailabilityDate( $delivdelay ){
	
	global 	$GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT delaydatetime FROM delay WHERE delay$lang LIKE '$delivdelay' LIMIT 1";
	
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le délais de paiement" );
	
	if( !$rs->RecordCount() )
		return false;
		
	$delaydatetime = $rs->fields( "delaydatetime" );
	
	$tomorrow = date( "Y-m-d", time() + 1 * 3600 * 24 );

	return DateUtil::getNextOpenDay( $tomorrow, $delaydatetime );
	
}

//----------------------------------------------------------------------------------------------------------------

function listDelivDelay( $idrow, $selection, $enabled = true ){
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT iddelay, delay$lang, delaydatetime FROM delay ORDER BY delaydatetime ASC";
	
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des délais de paiement" );
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	
	?>
	<input type="hidden" name="DelivDelayChanged_<?php echo $idrow ?>" value="0" />
	<select id="delivdelay_<?php echo $idrow ?>" name="delivdelay_<?php echo $idrow ?>"<?php echo $disabled ?> onchange="document.forms.frm.elements[ 'DelivDelayChanged_<?php echo $idrow ?>' ].value = 1;">
	<?php
	
		while( !$rs->EOF() ){
		
			$delivdelay = $rs->fields( "delay$lang" );
			$iddelay = $rs->fields("iddelay");
			$selected = $iddelay == $selection ? " selected=\"selected\"" : "";
			
			?>
			<option value="<?php echo $iddelay ?>"<?php echo $selected ?>><?php echo htmlentities( $delivdelay ) ?></option>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</select>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( SupplierOrderItem &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" onclick="window.open(this.href); return false;">
		<?php 
		
	}
	
	?>
	<img src="<?php echo URLFactory::getReferenceImageURI( $item->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){

	$designation = $item->get( "designation" );
	$designation = str_replace("\n",'', $designation);
	//$designation = str_replace("\r",'', $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = strip_tags($designation);
	
	return $designation;
		
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$item ){
		
	global $GLOBAL_START_URL;

	/*référence catalogue*/
	
	?>
	<p>Référence catalogue : <?php echo $item->get( "reference" ) ?></p>
	 <?php
	 
 	if( $item->get( "idcolor" ) )
 		displayColor( $item );
	 
	 /*référence et infos fournisseur*/

	?>
	<p>
		<span class="lightGrayText">Réf. fournisseur : <?php echo $item->get( "ref_supplier" ) ?></span>
		&nbsp;
		<a href="#" onclick="showSupplierInfos(); return false;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
		</a>
	</p>
	<?php 

	/*lien fiche PDF*/
	
	$idproduct = DBUtil::getDBValue("idproduct","detail","idarticle",$item->get( "idarticle" ));	

	$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $item->get( "idarticle" ) );
	$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : URLFactory::getPDFURL( $idproduct );
	
	?>
	<p>
		<a class="blueLink" href="<?php echo $href ?>" onclick="window.open(this.href); return false;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
		</a>
	</p>
	<?php
							 	
}

//----------------------------------------------------------------------------------------------------------------

function displayAvailability( SupplierOrder $order, &$item, $i ){
	global $disabled;
	
	$availability_date 	= DBUtil::getDBValue( "availability_date", "bl_delivery", "idorder_supplier", $order->get( "idorder_supplier" ) );
	$delivdelay 		= $item->get( "delivdelay" );

	$delivdelayChanged 	= isset( $_POST[ "DelivDelayChanged_$i" ] ) && $_POST[ "DelivDelayChanged_$i" ] == 1;
	
	if( $delivdelayChanged || $availability_date == "0000-00-00" )
		$availability_date = getAvailabilityDate( $delivdelay );
	
	if( $availability_date == "0000-00-00" )
		$availability_date = "";
	else $availability_date = usDate2eu( $availability_date );
			
	?>
	<input <?php echo $disabled ?> type="text" class="calendarInput" name="availability_date_<?php echo $i ?>" id="availability_date_<?php echo $i ?>" value="<?php echo $availability_date ?>" readonly="readonly" />
	<?php 

}

//-----------------------------------------------------------------------------------------------------

function displayColor( SupplierOrderItem $item ){
	
	if( !$item->get( "idcolor" ) )
		return;
		
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( !$idproduct )
		return;

	include_once( dirname( __FILE__ ) . "/../../../objects/Color.php" );
	
	$color = new Color( $idproduct, $item->get( "idcolor" ) );
	
	?>
	<div>
		<img src="<?php echo substr( $color->getImageURI(), 2 ); ?>" alt="<?php echo htmlentities( $color->getName() ); ?>" style="border:1px solid #B2B2B2; width:15px; height:15px;" />
		<span style="margin-right:15px;"><?php echo htmlentities( $color->getName() . " - " . $color->getCode() ); ?></span>
	</div>
	<?php
	
}

//----------------------------------------------------------------------------------------------------------------

?>