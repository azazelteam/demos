<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE PAIEMENT ++++++++++++++++++++++++++++++++++++++++++ -->
<?php  

anchor( "UpdateIdPayment" );
anchor( "UpdateIdPaymentDelay" );
anchor( "UpdateBL" );

//paiement comptant
$cash_payment = $Order->get( "cash_payment" );
$cash_enabled = $cash_payment ? "" : " disabled=\"disabled\"";

$no_tax_export = $Order->get( "no_tax_export" );

//montant règlé comptant
$balance_date = $Order->get( "balance_date" );
$startdate = $balance_date;

//Date de règlement
$balance_date = $Order->get( "balance_date" );

if( $balance_date == "0000-00-00" ){
	$balance_date = "";
	$startdate = date( "Y-m-d" );
}else{
	$startdate = $balance_date;
}

?>
<div class="contentResult" style="margin-bottom: 10px;">

	<h1 class="titleEstimate"><span class="textTitle">Paiement</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>       
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">
		
		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
			<label><span>Mode de règlement</span></label>	
			<?php 
				$enbl = true;
				if( $disabled ) { $enbl = false; }
				DrawLift_modepayment( $Order->get( "idpayment" ) , $enbl ); 
			?>
			<input type="hidden" name="UpdateIdPayment" value="ok" />
			<div class="spacer"></div>  
			
			<label><span>Délai de paiement</span></label>		
			<?php DrawPaymentDelayList( $Order->get( "idpayment_delay" ) , $enbl ); ?>
			<input type="hidden" name="UpdateIdPaymentDelay" value="ok" />
			<div class="spacer"></div>  
		</div></div>	
		
		<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
			<label><span>N° cde fournisseur</span></label>
			<input <?=$disabled?> name="n_order_supplier" type="text" class="textidInputMin" style="width:14.3%;" value="<?php echo $Order->get( "n_order_supplier" ) ?>" />
			<div class="spacer"></div>	
		</div></div>
		<div class="spacer"></div>
		
		
		<script type="text/javascript">
		/* <![CDATA[ */
		
			function enableCashPayment( enable ){
			
				document.getElementById( "frm" ).elements[ 'balance_date' ].disabled = enable ? '' : 'disabled';
				
				if( enable )
					$( '#balance_date' ).datepicker( "enable" );
				else $( '#balance_date' ).datepicker( "disable" );

				document.getElementById( "frm" ).elements[ 'balance_date' ].value = enable ? '<?php 
				
					list( $year, $month, $day ) = explode( "-", $Order->get( "balance_date" ) );
					echo $Order->get( "balance_date" ) != "0000-00-00" ? "$day-$month-$year" : date( "d-m-Y" ); 
					
				?>' : '';
				document.getElementById( 'balance_date_calendar' ).style.display = enable ? 'block' : 'none';
				
			}
			
		/* ]]> */
		</script>

		<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
			<h2><input type="checkbox" name="cash_payment" value="1" onclick="enableCashPayment( this.checked );"<?php if( $Order->get( "cash_payment" ) ) echo " checked=\"checked\" disabled=\"disabled\""; ?> style="vertical-align:middle;" />
			Paiement comptant</h2>
			
			<label><span>Date de règlement</span></label>
			<input type="text" name="balance_date" id="balance_date" value="<?php echo usDate2eu( $balance_date ) ?>" readonly="readonly"<?php echo $cash_enabled ?> class="calendarInput" />
			<?php DHTMLCalendar::calendar( "balance_date" ); ?>
			
			<label class="smallLabel" style="width:22%;"><span>Mode règlement</span></label>
			<div class="simpleSelect">
				<?php DrawLift("payment",$Order->get("payment_idpayment"),"",true,false,"payment_idpayment"); ?>
			</div>
			<div class="spacer"></div>
			
			<label><span>Commentaire<br/>&nbsp;</span></label>		
			<textarea name="payment_comment" class="textInput"><?=$Order->get("payment_comment")?></textarea>
			<div class="spacer"></div>	

		</div></div>
		
		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
			<h2>Acompte</h2>
			
			<label><span>Montant</span></label>
			<input<?php echo $disabled ?> type="text" class="calendarInput percentage" name="down_payment_rate" value="<?php echo Util::numberFormat( $down_payment_rate ) ?>" />
			<span class="textidSimp" style="width:5.4%;">%</span>
				
			<input<?php echo $disabled ?> type="text" class="calendarInput price" name="down_payment_amount" value="<?php echo Util::numberFormat( $down_payment_amount ) ?>" />
			<span class="textidSimp" style="width:5.4%;">&euro;</span>	
			<div class="spacer"></div>	
			
			<label><span>Date de règlement</span></label>
			<input<?php echo $disabled ?> type="text" name="down_payment_date" id="down_payment_date" value="<?php echo usDate2eu( $Order->get("down_payment_date") ) ?>" readonly="readonly" class="calendarInput" />
			<?php DHTMLCalendar::calendar( "down_payment_date" ); ?>
			
			<label class="smallLabel" style="width:22%;"><span>Mode règlement</span></label>
			<div class="simpleSelect">
				<?php DrawLift("payment",$Order->get("down_payment_idpayment"),"", !strlen( $disabled ),false,"down_payment_idpayment");?>
			</div>
			<div class="spacer"></div>
			
			<label><span>Commentaire<br/>&nbsp;</span></label>		
			<textarea<?php echo $disabled ?> name="down_payment_comment" class="textInput"><?=stripslashes($Order->get("down_payment_comment"))?></textarea>
			<div class="spacer"></div>	
		</div></div>
		<div class="spacer"></div>	

		<input <?=$disabled?> type="submit" style="float:right;margin-top:5px;" class="blueButton" value="Recalculer" name="updateOrder" />
   		
		<div class="spacer"></div>

			
	</div></div>
</div><!-- mainContent -->			
