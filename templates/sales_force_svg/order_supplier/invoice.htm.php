<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

//++++++++++++++++++++++++++++++++++++++++++ CADRE PAIEMENT ++++++++++++++++++++++++++++++++++++++++++

anchor( "UpdateIdPayment" );
anchor( "UpdateIdPaymentDelay" );
anchor( "UpdateBL" );

$bls = $Order->getBLs ($Order->get("idorder_supplier"));

$idbl_delivery = $bls[0];

$Query = "SELECT billing_supplier FROM billing_supplier_row WHERE idbl_delivery = '$idbl_delivery' LIMIT 1"; //@todo plusieurs factures possibles
$rsbil = & DBUtil::query( $Query );

$billing_supplier = $rsbil->fields("billing_supplier");


if($billing_supplier!==0){

	$supplierinvoice = new SupplierInvoice($billing_supplier, $Order->get("idsupplier"));
	
	$Date = $supplierinvoice->get("Date");
	
	if($Date == '0000-00-00'){
		$Date = '';
	}else{
		$Date = usDate2Eu($Date);
	}
	
}else{

	$billing_supplier = '';
	$Date = '';
		
}	
?>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="mainContent fullWidthContent">
	<div class="topRight"></div><div class="topLeft"></div>
	<div class="content">
		<div class="subContent">
	    	<div class="subTitleContainer">
	            <p class="subTitle">Facture fournisseur</p>
	        </div>
		    <div class="tableContainer">
				<table>
					<tr>
						<th>Facture n°</th>
						<td style="padding-right:10px; white-space:nowrap;"><input type="text" name="billing_supplier" value="<?php echo $billing_supplier ?>" class="textInput" style="width:60px;" /></td>
						<th>Date de facturation</th>
						<td style="white-space:nowrap;">
							<input type="text" name="Date" id="Date" value="<?php echo $Date ?>" class="calendarInput" />
							<?php DHTMLCalendar::calendar( "Date" ); ?>
						</td>
					</tr>
				</table>
			</div>
			<input type="submit" name="UpdInvoice" value="Modifier" class="blueButton" style="float:left;" />
			<div class="clear"></div>
		</div>
	</div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div>
