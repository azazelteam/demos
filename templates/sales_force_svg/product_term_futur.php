<?php

ini_set("memory_limit", "32M");

// TODO : à modifier pour utiliser admin_search.inc.php comme dans le FrontOffice
//include_once($GLOBAL_START_PATH.'/catalog/admin_search.inc.php');

//--------------------------------------------------------------------------------------------------

if( isset( $_REQUEST[ "qtip" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../objects/Estimate.php" );
	
	if( !User::getInstance()->getId() )
		exit( "Session expirée" );
		
	estimateQTip( new Estimate(  $_REQUEST[ "idestimate" ] ) );

	exit();	
	
}
//-------------------------------------------------------------------------------------------------


/*if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "month_term" ){

	$month_term = $_POST[ "month_term" ] ;
	$reference = $_POST[ "reference" ] ;
	include_once( "../config/init.php" );
	$dates = date('Y-m-d');

	$query = "
	UPDATE detail 
	SET month_term = '" . Util::html_escape( stripslashes( $month_term )) . "' 
	 
	WHERE reference = '" . $reference . "' 
	 
	";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}*/
//--------------------------------------------------------------------------------------------------

include_once (dirname(__FILE__) . "/../objects/classes.php");
include_once ("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
// Relance Majax et pas Majax
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

	
$Title = "Liste des références non définies pour les échéances";

include ("$GLOBAL_START_PATH/templates/back_office/head.php");


//--------------------------------------------------------------------------------------------------

include_once ($GLOBAL_START_PATH . '/catalog/admin_func.inc.php');
include_once ($GLOBAL_START_PATH . '/catalog/drawlift.php');

$hasAdminPrivileges = User::getInstance()->get("admin");
$iduser = User::getInstance()->getId();


//---------------------------------------------------------------------------------------------

?>

	<script type="text/javascript">
	/* <![CDATA[ */
	
		function SortResult(sby, ord) {
			document.adm_estim.action="<?php echo $ScriptName ?>?search=1&sortby="+sby+"&sens="+ord;
			document.adm_estim.submit();
		}
		
		function confirmDeleteEstimate(idestimate) {
			var ret = confirm( 'Confirmer la suppression du devis n°' + idestimate + '?' );
			if( ret == true )
				document.forms.adm_estim.elements[ 'EstimateToDelete' ].value = idestimate;
			
			return ret;
		}
		
		function UpdateUseEstimate(idestimate) {
			document.forms.adm_estim.elements[ 'use_estimate' ].value = idestimate;
			document.forms.adm_estim.submit();
		}
		
		function SetRelaunchList(idestimate) {
			var list = document.forms.adm_estim.elements[ 'relaunch_list' ].value;
			document.forms.adm_estim.elements[ 'relaunch_list' ].value = list+','+idestimate;
		}
		
		function checkAll(check) {
			var checkboxes = document.forms.adm_estim.elements["relaunch_estimate"];
			var i=0;
			while(i<checkboxes.length){
				checkboxes[i].checked=check;
				i++;
			}
		}
		
		function openDialog( idestimate ){
								
    		$( "#estimateDialog" ).html( '' );
    		
			$.ajax({
				 	
				url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/relaunch_estimate.php?estimate=" + idestimate,
				async: true,
			 	success: function( responseText ){
	
					$('#estimateDialog').append( responseText );
					
				}
	
			});
			
			$( "#estimateDialog" ).dialog({
				width: 1000,
				height: 'auto',
				position: 'top',
				modal: true,
				resizable:false,
				close:function(event, ui) {
					tinyMCE.execCommand('mceFocus', false, 'message_container');
					tinyMCE.execCommand('mceRemoveControl', false, 'message_container'); }
			});

		}

		/* ----------------------------------------------------------------------------------------------------- */
		/* Qtip lignes articles */
		
		$(document).ready( function(){

			$( "img[id^=estimateQTip]").each( function( i ){

				var idestimate = $( this ).attr( "id" ).substr( 12 );
			
		        $( this ).qtip({
	
		       		adjust: { mouse: true, screen: true },
					content: {
					      url: '/sales_force/relaunch_search.php',
					      data: { idestimate: idestimate, qtip: 1 },
					      method: 'get'
					  },
		            position: { corner: { target: "rightMiddle", tooltip: "leftMiddle" } },
		            style: { 
			            background: "#FFFFFF", border: { color: "#EB6A0A", radius: 5 },
		                fontFamily: "Arial,Helvetica,sans-serif",
		                fontSize: "13px",
		                tip: true,
		                width: 'auto'
		            }
		            
		        });

			});
			
	    });

		/* ----------------------------------------------------------------------------------------------------- */
		

		function save(reference, idbuyer, idorder, dates, month_term, quantity){
				
			
				
				var divToComplete = document.getElementById('echeancesaved') ;
						//var data = ''; 
					//	var message = 'Supression du contact en cours ...';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/saveecheance.php?reference=' + reference + '&idbuyer=' + idbuyer + '&idorder=' + idorder + '&dates=' + dates+ '&month_term=' + month_term + '&quantity=' + quantity;
						//machineAjax( location, divToComplete, data, message );
						
			
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
	
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
		{
				divToComplete.innerHTML = xhr_object.responseText ;
			window.location.reload();
			}else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);
						
						
						
				//	}
				}
			
		function changemonth(month_term,reference){
		
		//var month_term = document.getElementById('month_term'+i).value ;
			//alert(month_term);		
				var data = "month_term=" + month_term + "&reference=" + reference;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_futur.php?update=month_term",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
				
	/* ]]> */
	</script>
	
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

	<style type="text/css">

		.ui-dialog-titlebar{
			display:none;
		}
		
		.ui-widget-content {
			background:transparent;
			border:none;
		}
		
		.ui-dialog .ui-dialog-content {
			background:transparent;
		}
		
		.ui-datepicker{
			z-index:1200;
			background:url("images/ui-bg_highlight-soft_100_eeeeee_1x100.png") repeat-x scroll 50% top #EEEEEE;
			border:1px solid #DDDDDD;
			color:#333333;
		}
		
		.ui-datepicker-trigger{
			float:left;
		}
	</style>
	
	<form action="product_term_futur.php#" method="post" name="adm_estim" id="adm_estim">
	<input type="hidden" name="EstimateToDelete" id="EstimateToDelete" value="0" />
	<input type="hidden" name="use_estimate" id="use_estimate" value="0" />
	<input type="hidden" name="relaunch_list" id="relaunch_list" />
	<input type="hidden" name="search" value="1" />
	<?php
		//------------------------------------------------------------------------------------------------------------------------------------------
				$SQL_Querys = " 
				SELECT  d.reference,
			 d.month_term,
				orw.quantity, 
				orw.idorder, 
				orw.summary, 
				orw.designation,
				orw.updated,
				o.idbuyer,
				b.company,
				c.firstname
					FROM 
					detail d,
					contact c,  
						buyer b, 
						`order_row` orw,
						`order` o
					
						
					WHERE 
					 d.month_term > 0
					  AND orw.reference =  d.reference
				
					AND o.idorder = orw.idorder
					AND o.idbuyer = b.idbuyer
					AND o.idbuyer = c.idbuyer
					AND c.idcontact = 0
			
					
				";
				
			$rsq = DBUtil::query($SQL_Querys);
		$total =0;
						for ($i = 0; $i < $rsq->RecordCount(); $i++) {
						 $references =  $rsq->Fields("reference");
						  $idorders =  $rsq->Fields("idorder");
		  $exist = 0;
		      $qr = " 
				SELECT count(ot.`idproduct_term`) as exist
				
			
				
					FROM 
					
					product_term ot
					where 
					ot.`idorder` = '$idorders'
					AND ot.`reference` = '$references'
					 
					
				";
				
			$rss = DBUtil::query($qr);
		
									if($rss->Fields("exist") == 0)
									
			$total=$total+1; 
		  
				$rsq->MoveNext();
			} 
		
		//---------------------------------------------------------------------------------------------------------------------------------------------
			$lang = User::getInstance()->getLang();
			// $SQL_Query = "SELECT $Str_FieldList FROM estimate op, buyer bu, commercial com $SQL_Condition";
			$SQL_Query = " 
				SELECT  orw.`reference`,
			 d.month_term,
				orw.quantity, 
				orw.idorder, 
				orw.summary, 
				orw.designation,
				orw.updated,
				o.idbuyer,
				b.company,
				c.firstname
					FROM 
					detail d,
					contact c,  
						buyer b, 
						`order_row` orw,
						`order` o
					
						
					WHERE 
					 d.month_term > 0
					  AND orw.`reference` =  d.`reference`
				
					AND o.idorder = orw.idorder
					AND o.idbuyer = b.idbuyer
					AND o.idbuyer = c.idbuyer
					AND c.idcontact = 0
			
					
				";
				
			$rs = DBUtil::query($SQL_Query);
			//print $rs->RecordCount();exit;
			global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
		
			include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");
		
			$encryptedQuery = URLFactory::base64url_encode(Encryptor::encrypt($SQL_Query, $GLOBAL_DB_PASS));

		
			if ($rs->RecordCount() > 0) { // At least one row result
		
		?>
	<div class="centerMax">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div id="tools" class="rightTools">
			<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
			Accès rapide</h2>
			<div class="spacer"></div>
			
			<div class="blocSearch">
				&nbsp;
			</div>
			
			<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
			Quelques chiffres...</h2>
			<div class="spacer"></div>
			
			<div class="blocSearch">
				&nbsp;
			</div>
			
		</div>	
		<div class="contentDyn">
			<!-- Titre -->
			<h1 class="titleSearch">
            
			<span class="textTitle">Création des échéances des références commandées :<?php echo $total ?> </span>
            <span class="selectDate">
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/export_csv.php?type=estimate&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				</span>
			<div class="spacer"></div>
			</h1>
					 <div id="echeancesaved" style="width:inherit; height:auto">
                          
                               </div><br><br>		
							
			<div class="blocSearch"><div style="margin:5px;">
				
			<table class="dataTable resultTable">
					<thead>
						<tr>
							<th>N° commande</th>
							<th>N° client</th>
							<th>Société / Nom</th>
							<th>Référence</th>
							<th>Designation</th>
                            <th>Qté</th>
                            <th>Mois échéance</th>
                             <th></th>
							
							
						</tr>
						<!-- petites flèches de tri -->
						
					</thead>
					<tbody>
					<?php
	$total =0;
	
							for ($i = 0; $i < $rs->RecordCount(); $i++) {
						 $reference =  $rs->Fields("reference");
						  $month_term =  $rs->Fields("month_term");
						  $date =  $rs->Fields("updated");
						//  $dates =  substr( $rs->Fields("updated"), 0, 10);
						//  $month_term = 4;
					//$dates =  date("Y-m-d H:i:s" , strtotime("+$month_term month", strtotime($date."-01" )));
					//$dates = date("Y-m-d", strtotime($date));	
				//	$dates = urlencode ($dates);
						//print $dates;
						
				$quantity =  $rs->Fields("quantity");
				$idorder = $rs->Fields("idorder");
				$designation = strip_tags($rs->Fields("designation"),'<br>');
				
				$summary = $rs->Fields("summary");
				$company = $rs->Fields("company");
				$firstname = $rs->Fields("firstname");
			$idbuyer = $rs->Fields("idbuyer");
							
							$contact_Name = $rs->Fields("company") . "  " . $rs->Fields("firstname") ;
					
			
					 
						    $qr = " 
				SELECT count(ot.`idproduct_term`) as exist
				
			
				
					FROM 
					
					product_term ot
					where 
					ot.`idorder` = '$idorder'
					AND ot.`reference` = '$reference'
					 
					
				";
				
			$rss = DBUtil::query($qr);
			//var_dump($rss->Fields("exist"));
			//var_dump($rss);exit;
					/*	while( !$rss->EOF()){
						$idorder_ot =  $rss->Fields("idorder");
						$reference_ot =  $rss->Fields("reference");
										if(($idorder_ot == $idorder) && ($reference_ot == $reference ))
										$exist = 1;
										$rss->MoveNext();
									}*/
									if($rss->Fields("exist") == 0) {
									
			$total=$total+1; 
					?>
						  <input type="hidden" name="reference" id="reference<?php echo $i ?>" value="<?php echo $reference ?>" />
							<input type="hidden" name="idbuyer" id="idbuyer<?php echo $i ?>" value="<?php echo  $idbuyer ?>" />		
                             <input type="hidden" name="idorder" id="idorder<?php echo $i ?>" value="<?php echo  $idorder ?>" />
									
                           
							<input type="hidden" name="quantity" id="quantity<?php echo $i ?>" value="<?php echo $quantity ?>" />	
                            <input type="hidden" name="dates" id="dates<?php echo $i ?>" value="<?php echo $date ?>" />			        
			        <tr class="blackText">
                     <td style="width:3%;" class="lefterCol"><?php echo $idorder ?></td>
                       <td style="width:3%;" class="lefterCol"><?php echo $rs->fields( "idbuyer" ); ?></td>
					 <td style="width:10%;">
						 
				        	<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>" onclick="window.open(this.href); return false;">
				        		<?php if($rs->fields("company")){ echo htmlentities( $rs->fields( "company" ) ); } else { ?> - <?php  }?>
				        	</a> / <?php echo $firstname ?>
				        </td>
                      <td  valign="left" style="text-align: left;width:10%;"><?php echo $reference ?>  
                    </td>
                      <td style="text-align: left;width:40%;"><?php echo $designation ?></td>
                      <td style="width:5%;"><?php echo $quantity ?> </td>
                        <td style="width:5%;">  <input  name="month_term" id="month_term<?php echo $i ?>" value="<?php echo $month_term ?>"   size="1" maxlength="2"/> </td>
                  
                    
				        
                     <td style="width:3%;" class="lefterCol"><a href="#" onClick=" save(document.getElementById('reference<?php echo $i ?>').value,document.getElementById('idbuyer<?php echo $i ?>').value, document.getElementById('idorder<?php echo $i ?>').value , document.getElementById('dates<?php echo $i ?>').value, document.getElementById('month_term<?php echo $i ?>').value,document.getElementById('quantity<?php echo $i ?>').value )">valider</a></td>
                    
			        </tr>

			<?php
		}
			
				$rs->MoveNext();
			} ?>
					</tbody>
					</table>
				
		        <div class="spacer"></div>   
			</div></div>
		</div>       		      	
		<div class="spacer"></div>     
		
		
		
	



			
			        
<?php } else {  ?>
	
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<p class="msg" style="text-align:center;">Aucun résultat </p>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
<?php }
 ?>


</div>
</form>



<?php

//--------------------------------------------------------------------------------------------------

include ("$GLOBAL_START_PATH/templates/back_office/foot.php");

//--------------------------------------------------------------------------------------------------
/*function Addecheance($reference, $idbuyer, $idorder, $designation, $month_term, $quantity  ){
	
	

	
	$query = "INSERT INTO product_term (
			
			idbuyer,
			idorder,
			reference,
			designation,
			term_date,
			
			lastupdate
			
		)VALUES(
			'$idbuyer',
			'$idorder',
			'$reference',
			'$designation',
			'0',
			'now()'
			
		)";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false ){
		
		trigger_error( "Impossible d'enregistrer l'échéance commande", E_USER_ERROR );
		return;

	}
	


	return true;
			
}*/

//--------------------------------------------------------------------------------------------------

?>