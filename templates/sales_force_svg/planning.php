
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/jquery-ui-1.7.1.custom.min.js"></script>
<script>
  
$(document).ready(function() {
    $('#btnPrev').click(function(){
    
      $('#navigation').val('-1');
      $('#form-week').submit();
    });
    
    $('#btnNext').click(function(){
      
      $('#navigation').val('1');
      $('#form-week').submit();
    });
    
    
     $('#btnWeek').click(function(){
      
      var dtNow = '<?php echo date('Y-m-d')?>';
    
      $('#navigation').val('0');
      $('#dtStart').val(dtNow);
      $('#form-week').submit();
    });
    
    
  
});

function  checkingBuyerProcess(){
    
    
     $('#idbuyer').change(function(){
                        var explorerURL = "<?php echo $GLOBAL_START_URL ?>/templates/sales_force/planning_form.php?checking=1"
                  
                    $.ajax({

            			url: explorerURL,            			
            			type: "POST",
                        data:{
                            
                            idbuyer:$('#idbuyer').val(), 
                            
                        },
                        dataType:'json',
                        success: function( response ){
                            if( response.result == 'OK'){
                               
                           	    $('#idcontact').html('');
                                $.each(response.contacts, function(index, item){
                                 $('<option>').
                                  attr('value', item.idcontact).
                                  text(item.title_1 + ' '+item.firstname+ ' '+ item.lastname).
                                  appendTo($('#idcontact'));
                               });
        
                            }
                            else alert(response.result);
                        }
                 });
                 
               });
}

function savingProcess(){
    
     $('#btnSave').click(function(){
                    
                   
                      	var saveURL = "<?php echo $GLOBAL_START_URL ?>/templates/sales_force/planning_form.php?save=1";

                     	$.ajax({

                			url: saveURL,
                			async: false,
                			cache: false,
                			type: "POST",
                            data:{
                                date_event:$('#date_event').val(), 
                                idbuyer:$('#idbuyer').val(), 
                                idtype_follow:$('#idtype_follow').val(),
                                starttime_event:$('#starttime_event').val(),
                                endtime_event:$('#endtime_event').val(),
                                idcontact : $('#idcontact').val(),
                                idcontact_cause : $('#idcontact_cause').val(),
                                comment : $('#comment').val(),
                                idcontact_follow:$('#idcontact_follow').val()
                                
                            },
                            dataType:'json',
                            success: function( response ){
                                if( response == 'OK'){
                               	    $("#plannigDialog").dialog( 'destroy' ); 
        				            $("#plannigDialog").remove();
									location.reload();
                                }
                                else alert( response);
                            }
                       });
                    
                });
}

function editFollow( idcontact_follow ){
	
		var explorerURL = "<?php echo $GLOBAL_START_URL ?>/templates/sales_force/planning_form.php?edit=1";

	
		$.ajax({

			url: explorerURL,
			async: false,
			cache: false,
			type: "POST",
            data:{ idcontact_follow:idcontact_follow},			
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger le formulaire" );
            console.log(errorThrown)},
		 	success: function( responseText ){
	
				$( "#div-container" ).append( '<div id="plannigDialog"></div>' );
				
				$("#plannigDialog").html( responseText );
        		$("#plannigDialog").dialog({
        		
        			title: "Suivi n° "+idcontact_follow,
        			width:  500,
        			height: 350,
        			close: function(event, ui) { 
        			
        				$("#plannigDialog").dialog( 'destroy' ); 
        				$("#plannigDialog").remove();
        				
        			}
        		
        		});
                
                $('#btnCancel').click(function(){
                    
                    $("#plannigDialog").dialog( 'destroy' ); 
                    $("#plannigDialog").remove();
                    
                });
                
                checkingBuyerProcess();
                savingProcess();
               
        		
			}

		});
	
	}

function showForm( date ){
	
		var explorerURL = "<?php echo $GLOBAL_START_URL ?>/templates/sales_force/planning_form.php?new=1&dt="+date;

	
		$.ajax({

			url: explorerURL,
			async: false,
			cache: false,
			type: "GET",			
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger le formulaire" );
            console.log(errorThrown)},
		 	success: function( responseText ){
	
				$( "#div-container" ).append( '<div id="plannigDialog"></div>' );
				
				$("#plannigDialog").html( responseText );
        		$("#plannigDialog").dialog({
        		
        			title: "Suivi contact",
        			width:  500,
        			height: 350,
        			close: function(event, ui) { 
        			
        				$("#plannigDialog").dialog( 'destroy' ); 
        				$("#plannigDialog").remove();
        				
        			}
        		
        		});
                
                $('#btnCancel').click(function(){
                    
                    $("#plannigDialog").dialog( 'destroy' ); 
                    $("#plannigDialog").remove();
                    
                });
                
                checkingBuyerProcess();
                savingProcess();
               
        		
			}

		});
	
	}

</script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
<style>
ul#horizontal-list {
	width: 500px;
	list-style: none;
	padding-top: 20px;
	}
	ul#horizontal-list li {
		display: inline;
	}
    </style>

<div class="centerMax" id="div-container">

    
       <div class="spacer"></div>
          <div style="margin: auto; width: 500px!important; text-align: center;">  
          
          
            
          
          
          
        </div>
         <?php setlocale(LC_TIME,'fr_FR.utf8');?>
    <div id="calendar" style="width: 950px!important;">
    
        <h1 class="titleSearch" style="width: 870px; text-align: center; font-size: 12px!important;">
        	<!--<span class="textTitle">Votre suivi au quotidien </span>-->
            <span >Semaine : <?=strftime ( '%d %b %Y',$currentWeek['startDate']->getTimestamp())?> - <?=strftime ( '%d %b %Y',$currentWeek['endDate']->getTimestamp())?></strong></span>
        	<div class="spacer"></div>
        </h1>
        
       
            <form  id="form-week" method="post" action="" style="width: 870px;">
              <input type="hidden" name="dtStart" id="dtStart" value="<?=$currentWeek['startDate']->format('Y-m-d')?>" />
              <input type="hidden" name="navigation" id="navigation" value="0"/>
              
            </form>
          
      
        <div class="spacer"></div>
       
         <div style="clear: right; margin:auto; width:600px!important;" >
           
              <div style="float: left;width:175px; ">
              <input type="button" value="<< Semaine précédente" id="btnPrev" class="blueButton" />
              </div>
               <div style="float: left; margin-left:50px; width: 150px!important;  vertical-align: bottom; text-align: center*; ">
              <input type="button"  value="Sémaine actuelle" id="btnWeek"  class="blueButton"/>         
              </div>
            
             <div style="float: right;width: 150px; ">
              
               <input type="button" value="Semaine suivante >>"  id="btnNext" class="blueButton"/>
               </div>
                
           </div>
            <div class="spacer"></div>
            <?php    
               $heigth_morning = '245px';
               $heigth_afternoon = '500px';
               $schedule = DBUtil::getParameterAdmin("schedule");
               //$schedule = 'NORMAL';
             
               if(strtoupper($schedule)=='NORMAL') {              
                $heigth_afternoon = '375px';
              }
            ?>
		<ul class="heures">
			<li style="height:35px;background: #ffffff;"></li>
			<li class="matin" style="height:<?=$heigth_morning?>!important;"></li>
            
			<li class="apresmidi" style="height:<?=$heigth_afternoon?>!important;"></li>
		</ul>
        
		<?php
            
            /*$times = array('08:00:00','08:30:00','09:00:00','09:30:00','10:00:00','10:30:00','11:00:00','11:30:00','12:00:00','12:30:00',
                           '13:00:00','13:30:00','14:00:00','14:30:00','15:00:00','15:30:00','16:00:00','16:30:00','17:00:00','17:30:00','18:00:00');*/
            $times = list_times();
       ?>
             <ul style="width:25px!important;">
              <li style="height:35px!important;"></li>
                <?php for( $i=1; $i<= count($times) ; $i++):
                 $time =$times[$i-1];
                  ?>
                <li style="height:40px; text-align: left;width:25px!important;"><?=format_time($time)?></li>
                <?php endfor ?>
             </ul>               
            <?php 
               foreach($days as  $day):?>
                <ul>
                 <li style="height:35px!important;"> <strong><?=$day['label']?> 
                     <?=$datetime->createFromFormat('Y-m-d', $day['value'])->format('d/m/y')?></strong>
                     <br /><a href="#" onclick="showForm('<?=$day['value']?>')" ><img src="/images/back_office/content/button_create.png" height="15" alt="" /> Ajouter un événement</a>
                 </li>
              <?php $data = list_events($day['value']);
                    $events = array();
                     
                     foreach($data as $row){
                            $starttime  = $row['starttime_event'];
                            if(! array_key_exists($starttime,$events)){
                               $events[$starttime] = $row;
                            }
                     }
                    for( $i=1; $i<= count($times) ; $i++):
                        
                         $time = $times[$i-1];  
                         
                         if($i==9){
                             
                            display_separator_cell();
                            
                         }                       
                        
                         if( array_key_exists($time,$events)){
                             $event = $events[$time];                                
                             $unit = intval($event['duration'])/ 30;
                             $unit = $unit == 0 ? 1: $unit;
                            
                            $bgcolor= $event['idtype_follow']==1 ? '#FFE4E1': '#FAEBD7';
                            if( $unit == 1 ){
                               display_event($event, $unit,$bgcolor);
                               // = $i +1;
                            }
                            else {
                              
                               display_event($event, $unit,$bgcolor);
                               for( $ii= 1; $ii< $unit; $ii++){
                                display_empty_cell($bgcolor);
                               }
                               $i= $i+$unit-1 ;  
                            }
                         }
                         else{
                             if($i==count($times)){                         
                              display_separator_cell();                            
                            } 
                            else 
                               display_empty_cell();
                           //$i= $i+1;
                         }
                    endfor;
                       
                       
                ?>
                </ul>
            <?php endforeach ?>
        
       
	</div>
</div>
                <div class="spacer"></div>
            </div>
        </div>
        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>
<?php

function  display_event($event, $unit,$bgColor ){
    
    $height = $unit*40;
    $height = 40;
   ?>               
    <li class="vide" style="height: <?=$height?>px; text-align: left;padding-left: 5px!important; background-color:<?=$bgColor?>!important;"><?=$event['company']?> - <?=$event['title_1']?> <?=$event['lastname']?> 
    <br /><a href="#" onclick="editFollow('<?=$event['idcontact_follow']?>')" ><img src="/images/back_office/content/edit.png" alt="" /> Modifier</a>
                
<?php 

}

function  display_empty_cell($bgcolor=''){ 
    
    if($bgcolor!=''):?>
     <li class="vide" style="height:40px; text-align: left; background-color:<?=$bgcolor?>"></li>
     <?php else:?>
      <li class="vide" style="height:40px; text-align: left "></li>
     <?php endif ;

}
?>
<?php 
function  display_separator_cell(){ ?>
    
     <li class="vide" style="height:1px;background-color: #c1c2c3!important;text-align: left;"></li>
                  
<?php 

}
 function  format_time($strTime){
    
    $items = explode(':', $strTime);
    if( count($items)>=3)
    return $items[0].':'.$items[1];
    else return '';
 }
 
 function list_times(){
    
     $morning = array('09:00:00','09:30:00','10:00:00','10:30:00','11:00:00','11:30:00','12:00:00');
     $timeoff = array('12:30:00','13:00:00','13:30:00');
     $afternoon = array('14:00:00','14:30:00','15:00:00','15:30:00','16:00:00','16:30:00','17:00:00','17:30:00','18:00:00');
   	 
     $schedule = DBUtil::getParameterAdmin("schedule");
     //$schedule = 'NORMAL';
     
     $times = array();
     if(strtoupper($schedule)=='NORMAL') {        
        $times = array_merge($morning,$afternoon);
     }
     else{
      
        $times = array_merge($morning,$timeoff,$afternoon);
     }     
     return $times;
 }
?>