<!-- ++++++++++++++++++++++++++++++++++++++++++ VIRTUAL BUYER FRAME ++++++++++++++++++++++++++++++++++++++++++ -->
<?php 

anchor( "UpdateTodoMessage" );

include_once ("$GLOBAL_START_PATH/objects/AutoCompletor.php");
	
?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery.example.min.js"></script>
<script type="text/javascript">
<!--

	function show_refus() {
	
		var refusal_value=document.getElementById( "chstat" ).elements['cstatus'].options[document.getElementById( "chstat" ).elements['cstatus'].selectedIndex].value; 
	
		
		if(refusal_value=='Refused'){ 
			
			document.getElementById( "chstat" ).elements['refusal'].style.display = 'block';
		}else{
			document.getElementById( "chstat" ).elements['refusal'].style.display = 'none';
		}
		
	}
	
	function testrefus() {
	
		var refusal_value=document.getElementById( "chstat" ).elements['cstatus'].options[document.getElementById( "chstat" ).elements['cstatus'].selectedIndex].value;
		
		if(refusal_value=='Refused'){ 
			var motif=document.getElementById( "chstat" ).elements['refusal'].options[document.getElementById( "chstat" ).elements['refusal'].selectedIndex].value;
		
			if(motif==0){
				alert("Vous ne pouvez malheureusement pas modifier le statut car vous avez omis de sélectionner un motif de refus. Il vous prie de bien vouloir en sélectionner un. Merci de votre compréhension. Pour plus d'informations contactez Akilaé.");
				return false ;
			}else{
				return true ;
			}
		}
		
	}
	
	function showTodoInfos(){
		
		var tododiv = document.getElementById( 'TodoDiv' );
		tododiv.style.display = tododiv.style.display == 'block' ? 'none' : 'block';
	
	}
	
	function showFilesInfos(){
		
		var filesdiv = document.getElementById( 'FilesDiv' );
		filesdiv.style.display = filesdiv.style.display == 'block' ? 'none' : 'block';
	
	}

	function visibility(thingId, txtAff, txtMasque) {
		var targetElement; 
		var targetElementLink;
		targetElement = document.getElementById(thingId);
		targetElementLink = document.getElementById(thingId+'Link');
		if (targetElement.style.display == "none") {
			targetElement.style.display = "";
			targetElementLink.innerHTML = txtMasque;
		} else {
			targetElement.style.display = "none";
			targetElementLink.innerHTML = txtAff;
		}
	}
	
	/* Affecte le client à un devis n°idestimete */
	function affectBuyer(idbuyer, idestimate){
		checkBuyer($('#idbuyerAjax').fieldValue(), <?php echo $IdEstimate ?>, '');
	}
	
	function checkBuyer(idbuyer, idestimate, idcontact){

		var postData = "ReassignBuyer=1&RealBuyer=" + idbuyer + "&IdContact=" + idcontact + "&IdEstimate=" + idestimate;

		$.ajax({

			url: "<?=$GLOBAL_START_URL?>/sales_force/com_admin_devis.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
		 	success: function( responseText ){

    			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
    			$.blockUI.defaults.growlCSS.color = '#FFF';
				$.blockUI.defaults.growlCSS.backgroundColor = '#000';
				
    			if( responseText == ''){
			    	window.location.href = "<?=$GLOBAL_START_URL?>/sales_force/com_admin_devis.php?IdEstimate="+idestimate;
        		} else {
		    		$.growlUI( '', responseText );
		    	}
			}
		});

	}
	
	/* Affiche la liste des utilisateurs recherchés */
	function viewListBuyer(){
		
		var companyValue = ($('#CompanyAjax').fieldValue() != $('#CompanyAjax').attr('title')) ? $('#CompanyAjax').fieldValue() : ""; 
		var emailValue = ($('#EmailAjax').fieldValue() != $('#EmailAjax').attr('title')) ? $('#EmailAjax').fieldValue() : ""; 
		var idbuyerValue  = ($('#idbuyerAjax').fieldValue() != $('#idbuyerAjax').attr('title')) ? $('#idbuyerAjax').fieldValue() : ""; 
		var sirenValue = ($('#sirenAjax').fieldValue() != $('#sirenAjax').attr('title')) ? $('#sirenAjax').fieldValue() : ""; 
		var zipcodeValue = ($('#ZipcodeAjax').fieldValue() != $('#ZipcodeAjax').attr('title')) ? $('#ZipcodeAjax').fieldValue() : ""; 
		
		var postData = "emailValue=" + emailValue + "&zipcodeValue=" + zipcodeValue + "&companyValue=" + companyValue + "&idbuyerValue=" + idbuyerValue + "&sirenValue=" + sirenValue + "&idEstimate=" + <?php echo $IdEstimate ?> ;
		$.ajax({

			url: "<?=$GLOBAL_START_URL?>/templates/sales_force/estimate/searchBuyer.php",
			async: true,
			cache: false,
			type: "GET",
			beforeSend : function(){ $('#resultBuyerBDD').html( "<h2 class='search'>R&eacute;sultats de la base client</h2><p class='alignCenter'><img src='<?=$GLOBAL_START_URL?>/catalog/search_engine/ajax-loader.gif' /> Recherche en cours...</p>" ); },
			data: postData,
		 	success: function( responseText ){
				
				$('#resultBuyerBDD').html( responseText ); 
		
			}
		});
		
		var postDataManageo = "companyName=" + companyValue + "&sirenManageo=" + sirenValue + "&dept=" + zipcodeValue + "&idEstimate=<?php echo $IdEstimate ?>";
		$.ajax({

			url: "<?=$GLOBAL_START_URL?>/templates/sales_force/estimate/searchManageo.php",
			async: true,
			cache: false,
			type: "POST",
			beforeSend : function(){ $('#resultBuyerManageo').html( "<br/><h2 class='search'>R&eacute;sultats de la base manageo</h2><p class='alignCenter'><img src='<?=$GLOBAL_START_URL?>/catalog/search_engine/ajax-loader.gif' /> Recherche en cours...</p>" ); },
			data: postDataManageo,
		 	success: function( responseTextMangeo ){
				
				$('#resultBuyerManageo').html( responseTextMangeo );
				   		
			}
		});
		
		$("#resultBuyer").slideDown(400);

		
	}
	
	//---------------------------------------------------------------------------------------------
				
	function selectCity( zipcode ){
	
		var xhr = getXMLHttpRequest();
		
		xhr.onreadystatechange = function(){

			if( xhr.readyState == 4 && xhr.status == 200 ){
			
				if( !xhr.responseXML )
					return;

				createCitySelection( xhr.responseXML );
				
			}
			
		}
	
		var url = '<?php echo $GLOBAL_START_URL ?>/sales_force/commune.php?zipcode=' + zipcode;

		xhr.open( "GET", url, true );
		xhr.send( null );
		
	}
	
	//------------------------------------------------------------------------------------------------
	
	function createCitySelection( xmlDocument ){
		
		var target = document.getElementById( 'City' );
		var communes = xmlDocument.getElementsByTagName( 'commune' );

		if( !communes.length )
			return;
			
		if( communes.length == 1 ){
		
			target.value = communes[ 0 ].getAttribute( 'value' );
			$('#City').removeClass('customInput not_example');
			return;
			
		}
		
		var selection = document.getElementById( 'CitySelection' );

		var innerHTML 	='<p style="background-color:#EAEAEA; padding:7px; font-weight:bold;">Communes proposées</p>';
		innerHTML 		+= '<div style="overflow:auto; height:120px;">';
		innerHTML 		+= '<ul style="list-style-type:none;">';
		
		var i = 0;
		while( i < communes.length ){

			var commune = communes[ i ].getAttribute( 'value' );
			
			innerHTML += '<li style="text-align:left;">';
			innerHTML += '<input type="radio" onclick="$(\'#City\').val(\'' + commune + '\'); $(\'#City\').removeClass(\'customInput not_example\'); setTimeout( $.unblockUI, 10 );" />&nbsp;';
			innerHTML += '<a style="font-size:11px; text-decoration:none;" href="#" class="blueLink" onclick="$(\'#City\').val(\'' + commune + '\'); $(\'#City\').removeClass(\'customInput not_example\'); setTimeout( $.unblockUI, 10 ); return false;">';
			innerHTML += commune;
			innerHTML += '</a>';
			innerHTML += '</li>';
			
			i++;
			
		}
		
		innerHTML += '</ul>';
		innerHTML += '</div>';
		innerHTML += '<p style="text-align:right;">';
		innerHTML += '<input type="button" value="Annuler" class="blueButton" onclick="setTimeout( $.unblockUI, 10 );" />';
		innerHTML += '</p>';
		
		selection.innerHTML = innerHTML;
		
		$.blockUI({ 

			message: $('#CitySelection'),
			css: { padding: '15px', cursor: 'pointer' }
			
		});
			
	}
	
	//---------------------------------------------------------------------------------------------
				
	function copySiretToSiren( siret ){
		
		if( siret.length < 10 )
			document.getElementById( "sirenAjax" ).value = siret;
		else
			document.getElementById( "sirenAjax" ).value = siret.substring( 0, 9 );
		
		return true;
		
	}
	
	//---------------------------------------------------------------------------------------------
	/* Affecte le client Manageo */
	
	function validateManageo(formData, jqForm, options) { 
	    var form = jqForm[0]; 

	    var Company = form.Company.value; 
	    var Zipcode = form.Zipcode.value; 
	    var City = form.City.value; 
	    var url = form.url.value; 
	    var idestimate = form.idest.value; 
	    
	    var companyValue = ($('#CompanyAjax').fieldValue() != $('#CompanyAjax').attr('title')) ? $('#CompanyAjax').fieldValue() : ""; 
		var emailValue = ($('#EmailAjax').fieldValue() != $('#EmailAjax').attr('title')) ? $('#EmailAjax').fieldValue() : ""; 
		var sirenValue = ($('#sirenAjax').fieldValue() != $('#sirenAjax').attr('title')) ? $('#sirenAjax').fieldValue() : ""; 
		var siretValue = ($('#siret').fieldValue() != $('#siret').attr('title')) ? $('#siret').fieldValue() : ""; 
		var zipcodeValue = ($('#ZipcodeAjax').fieldValue() != $('#ZipcodeAjax').attr('title')) ? $('#ZipcodeAjax').fieldValue() : ""; 
	    var FirstnameValue = ($('#buyerFirstname').fieldValue() != $('#buyerFirstname').attr('title')) ? $('#buyerFirstname').fieldValue() : ""; 
	    var LastnameValue = ($('#buyerLastname').fieldValue() != $('#buyerLastname').attr('title')) ? $('#buyerLastname').fieldValue() : ""; 
	    var AdressValue = ($('#Adress').fieldValue() != $('#Adress').attr('title')) ? $('#Adress').fieldValue() : ""; 
	    var Adress2Value = ($('#Adress_2').fieldValue() != $('#Adress_2').attr('title')) ? $('#Adress_2').fieldValue() : ""; 
	    var PhonenumberValue = ($('#Phonenumber').fieldValue() != $('#Phonenumber').attr('title')) ? $('#Phonenumber').fieldValue() : ""; 
	    var gsmValue = ($('#gsm').fieldValue() != $('#gsm').attr('title')) ? $('#gsm').fieldValue() : ""; 
	    var faxValue = ($('#fax').fieldValue() != $('#fax').attr('title')) ? $('#fax').fieldValue() : ""; 
	    var cityUser = ($('#City').fieldValue() != $('#City').attr('title')) ? $('#City').fieldValue() : ""; 
	    var profilUser = ( !$("input[name=idcustomer_profile_radio]:checked").val() ) ? $('#idcustomer_profile').val() : $("input[name=idcustomer_profile_radio]:checked").val(); 
	    var provenanceUser = ( !$("input[name=idsource_radio]:checked").val() ) ? $('#idsource').val() : $("input@name=idsource_radio]:checked").val(); 
	    
	    var postDataManageo = "Company=" + Company + "&Zipcode=" + Zipcode + "&City=" + City + "&url=" + url + "&createBuyer=1&idest=<?php echo $IdEstimate ?>&CompanyUser="+companyValue+"&LastnameUser="+LastnameValue+"&FirstnameUser="+FirstnameValue+"&EmailUser="+emailValue+"&AdressUser="+AdressValue+"&Adress_2User="+Adress2Value+"&ZipcodeUser="+zipcodeValue+"&cityUser="+cityUser+"&sirenUser="+sirenValue+"&siretUser="+siretValue+"&PhonenumberUser="+PhonenumberValue+"&gsmUser="+gsmValue+"&faxUser="+faxValue+"&profilUser="+profilUser+"&provenanceUser="+provenanceUser;
		
		alert(postDataManageo);
		
		$.ajax({
			url: "<?=$GLOBAL_START_URL?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>",
			async: false,
			cache: false,
			type: "POST",
			data: postDataManageo,
			success: function( responseText ){
				
				window.location.href = "<?=$GLOBAL_START_URL?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>";
		
			}
		});
		
		
	}
		  

	//---------------------------------------------------------------------------------------------
	/* Champs pré remplis */
	
	$(function() {
		$('.customInput').example(function() {
			
			//alert("ici" + $(this).attr('title'));
			return $(this).attr('title');
			
		}, {className: 'not_example'});
		
		$('#closeZone').click(function() {
			$("#resultBuyer").slideUp(400);
		});

    });
    
    //---------------------------------------------------------------------------------------------
	/* Controle du champ civilité */
    
    $(document).ready(function(){ 
    	 $('#formCreateBuyer2').ajaxForm( { beforeSubmit: validate,  success: showResponse } ); 
    }); 
    
    function validate(formData, jqForm, options) { 
	
	    var titleValue = $('select[name=title]').fieldValue(); 
	
	    if (!titleValue[0] || titleValue[0] == 0) { 
	        alert('Merci de renseigner la civilité.'); 
	        
	        $('.customInput').example(function() {
				return $(this).attr('title');
			}, {className: 'not_example'});
			
	        return false; 
	    }
	}
	function showResponse(formData, jqForm, options) { 
		window.location.href = "<?=$GLOBAL_START_URL?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>";
	}
	
	function modifProfile() {
		if($('#idcustomer_profile').val() >= 0) {
			$('#idcustomer_profile_1').attr('checked', false);
			$('#idcustomer_profile_2').attr('checked', false);
		}
		
	}
	
	function modifSource() {
		if($('#idsource').val() >= 0) {
			$('#idsource_1').attr('checked', false);
		}
		
	}
	
	

	
//-->
</script>

<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>

<!-- Bloc formulaire -->
<div class="contentResult" style="margin-bottom:0;">	
	<h1 class="titleEstimate">
	<span class="textTitle">
	<? if($IdEstimate!=0) { ?>
		Devis n° <?php echo $IdEstimate ?>
	<?php } else { ?>
		Création de devis
	<?php } ?>
	</span>
	<span class="selectDate">
		<form method="post" action="com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>" enctype="multipart/form-data" id="formCreateBuyer" onsubmit="return affectBuyer();">
		<input type="submit" name="affecterBuyer" class="blueButton" value="&raquo;" style="float:right; margin:1px 0 0 5px;" />	
		<input type="text" title="N° client" name="idbuyerAjax" id="idbuyerAjax" value="" class="InputTextZipcode customInput InputAlert" style="float:right;" />
		</form> 
	</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>

	<form method="post" action="com_admin_devis.php" enctype="multipart/form-data" id="formCreateBuyer2">
	<input type="hidden" name="idest" id="idest" value="<?php echo $IdEstimate ?>" />
	<div class="blocEstimateResult"><div class="blocMarge" style="margin-top:0;">
		<div class="spacer"></div>
		
		<div class="blocSite">
			<h2>Profil du client</h2>
			<p><span>
				<input type="radio" name="idcustomer_profile_radio" id="idcustomer_profile_2" value="2" onclick="javascript:$('#idcustomer_profile').val('-1');" /> Entreprise&nbsp;&nbsp;&nbsp;
				<input type="radio" name="idcustomer_profile_radio" id="idcustomer_profile_1" value="1" onclick="javascript:$('#idcustomer_profile').val('-1');" /> Particulier&nbsp;&nbsp;
			</span>
			<?php XHTMLFactory::createSelectElement( "customer_profile", "idcustomer_profile", "name", "name", -1, "-1", "- Autres -", " id=\"idcustomer_profile\" name=\"idcustomer_profile\" class=\"selectBuyer\" onchange=\"javascript:modifProfile();\"" ); ?>
			</p>
			<div class="spacer"></div>
		</div>	
		<div class="blocSite">
			<h2>Provenance du client</h2>
			<p><span>
				<input type="radio" name="idsource_radio" id="idsource_1" value="9" onclick="javascript:$('#idsource').val('-1');" /> Web Téléphone&nbsp;&nbsp;&nbsp;
			</span>
			<?php XHTMLFactory::createSelectElement( "source", "idsource", "source_1", "source_1", -1, "-1", "- Autres -", " id=\"idsource\" name=\"idsource\" class=\"selectBuyer\" onchange=\"javascript:modifSource();\"" ); ?>			
			</p>
			<div class="spacer"></div>
		</div>	
		<div class="blocSite" style="margin-right:0;">	
			<h2>Informations société</h2>
			<input onchange="javascript:viewListBuyer();" type="text" title="Siren" name="siren" id="sirenAjax" value="" class="InputTextMedium customInput InputAlert" style="margin-right:0; width:137px;" onkeyup="this.value = this.value.replace( new RegExp( '[^0-9]*', 'g' ), '' );" onblur="this.onkeyup(event);" maxlength="11" />
			<img onclick="javascript:viewListBuyer();" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-btn-search.jpg" class="floatleft" style="margin:6px 10px 0 0; cursor:pointer;" />
			<input type="text" title="Siret" value="" id="siret" name="siret" class="InputTextMediumNone customInput" onkeyup="this.value = this.value.replace( new RegExp( '[^0-9]*', 'g' ), '' ); if( $( this ).val().length ) $('#siren').val( $( this ).val().substring( 0, 9 ) );" maxlength="14" />
			<div class="spacer"></div>		
		</div>	
		<div class="spacer"></div>			
		
		<div class="blocSite">
		
			<h2 class="marginTop">Informations client</h2>
			<input onchange="javascript:viewListBuyer();" type="text" title="Raison sociale" name="Company" id="CompanyAjax" class="InputText customInput InputAlert" style="text-transform: uppercase; width:316px;" />
			<img onclick="javascript:viewListBuyer();" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-btn-search.jpg" class="floatleft" style="margin:6px 0 0 0; cursor:pointer;" />

			<div class="spacer"></div>
			
			<div class="SelectTextTrio "><?php  
				if( isset( $key ) ) { DrawLift_4( "title", $rscp->Fields( "title" ) ); }
				else { DrawLift_4( "title", "Pas de titre" ); }
			?></div>
			<input type="text" title="Nom" id="buyerLastname" name="Lastname" value="" class="InputTextTrio upperCase customInput" />
			<input type="text" title="Prénom" id="buyerFirstname" name="Firstname" value="" class="InputTextTrioNone upperCase customInput" />
			<div class="spacer"></div>	
			
			<input onchange="javascript:viewListBuyer();" type="text" title="Email" name="Email" id="EmailAjax" value="" class="InputText customInput InputAlert" style="width:316px;" />
			<img onclick="javascript:viewListBuyer();" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-btn-search.jpg" class="floatleft" style="margin:6px 0 0 0; cursor:pointer;" />
			<div class="spacer"></div>	

		</div>	
		<div class="blocSite">

			<h2 class="marginTop">Adresse postale</h2>
			<input type="text" title="Adresse" name="Adress" id="Adress" value="" class="InputText customInput" />
			<div class="spacer"></div>
			
			<input type="text" title="Adresse complémentaire" name="Adress_2" id="Adress_2" class="InputText customInput" />
			<div class="spacer"></div>
			
			<input onchange="javascript:viewListBuyer();" type="text" title="Code postal" name="Zipcode" id="ZipcodeAjax" value="" class="InputTextZipcode customInput InputAlert" onkeyup="if( this.value.length == 5 ) selectCity( this.value );" />				
			<input type="text" title="Ville" name="City" id="City" value="" class="InputTextCity upperCase customInput" />
			<?php AutoCompletor::completeFromDB( "City", "communes", "commune" ); ?>
			<div id="CitySelection" class="displayNone"></div>
			<div class="spacer"></div>	

		</div>				
		<div class="blocSite" style="margin-right:0;">	
				
			<h2 class="marginTop">Coordonnées</h2>	
			<input type="text" title="Téléphone" name="Phonenumber" id="Phonenumber" value="" class="InputTextMedium customInput" />
			<input type="text" title="Portable" name="gsm" id="gsm" value="" class="InputTextMediumNone customInput" />
			<div class="spacer"></div>	
			
			<input type="text" title="Fax" name="fax" id="fax" value="" class="InputTextMedium customInput" />
			<div class="spacer"></div>
			
			<a class="floatleft" href="http://www.euridile.com" target="_blank" style="margin:5px 50px 5px 0;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-euridile_g.jpg" alt="" width="48" height="25" /></a>
			<a class="floatleft" href="http://www.google.fr/" target="_blank" style="margin:5px 50px 5px 0;;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-google_g.jpg" alt="" height="25" width="48" /></a>
			<a class="floatleft" href="http://www.societe.com/" target="_blank" style="margin:5px 50px 5px 0;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
			<a class="floatleft" href="http://www.pagesjaunes.fr/" target="_blank" style="margin:5px 0;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-pages-jaunes_g.jpg" alt="" width="48" height="25" /></a>
			
			<input type="submit" name="createBuyer" value="Créer le client" class="inputCreateBuyer marginTopSmall" />

			<!-- <img src="layout/icons-manageo.jpg" class="iconsVerif" />
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-axa.jpg" class="iconsVerif" /> -->
		</div>	
		<div class="spacer"></div>	
	</div></div>
	<div class="spacer"></div>	
	</form>
			
	<div class="spacer"></div>	
	
	<div id="resultBuyer" style="display:none;">
		<h1 class="titleEstimate">
			<span class="textTitle">Suggestion de contacts</span>
			<span class="selectDate"><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/icons-close.jpg" class="floatright" id="closeZone" style="cursor:pointer; margin:2px 3px 0 0;" /></span>
			<div class="spacer"></div>
		</h1>
		
		<div class="blocEstimateResult"><div class="blocMargeSearch">
			<!-- Suggestion -->
			<div id="resultBuyerBDD"></div>
			<div id="resultBuyerManageo"></div>
		</div></div>
		<div class="spacer"></div>	
	</div>
	<div class="spacer"></div>	

		
</div>
<div class="spacer"></div>



<?php

//---------------------------------------------------------------------------------------------------

function getCommercialName( $iduser ){
	
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT title, firstname, lastname FROM user WHERE iduser = '$iduser' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le nom du commercial" );
		
	return $rs->fields( "title" ) . ". " . $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
		
}

//---------------------------------------------------------------------------------------------------

?>