<!-- ++++++++++++++++++++++++++++++++++++++++++ PAIEMENT ET ECHEANCE ++++++++++++++++++++++++++++++++++++++++++ -->
<?php 

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "UpdateIdPayment" );
anchor( "UpdatePaymentDelay" );
anchor( "period_date_sel" );
anchor( "UpdateFactor" );
anchor( "UpdateRelaunch" );

//Scoring client
if( $Order->getCustomer()->getId() != DBUtil::getParameterAdmin( "internal_customer" ) ){
	
	$db = &DBUtil::getConnection();
		// ------- Mise à jour de l'id gestion des Index  #1161
	$reqidscoringb = "SELECT s.txtscoring_buyer AS scoring, s.idscoring_buyer AS idscoring FROM scoring_buyer s, buyer b WHERE b.idscoring_buyer = s.idscoring_buyer AND b.idbuyer = '" . $Order->getCustomer()->get( "idbuyer" )."'";
	$rsidscoringb = $db->Execute( $reqidscoringb );
	
	$scoring_buyer = $rsidscoringb->fields( "scoring" );
	$idscoring_buyer = $rsidscoringb->fields( "idscoring" );
	
}else{
	
	$idscoring_buyer = 0;
	
}

//-----------------------------------------------------------------------------

// Mode de règlement et delai de paiement par defaut lors de la création d'un devis
if( count( $rows ) == 0 || $Str_status == 'ToDo'){
	$defaultPaymentMode = DBUtil::getParameterAdmin( "default_estimate_payment_mode" );
	$defaultPaymentDelay = DBUtil::getParameterAdmin( "default_estimate_payment_delay" );
}
	

//date de validité

$valid_until = $Order->get( "valid_until" );
$reset = false;
	
if( $valid_until == "0000-00-00" ) //aucune date saisie
	$reset = true;
else{
		
	$values = explode( "-", $valid_until );
	
	if( count( $values ) != 3 )
		$reset = true;
	else{
		
		list( $year, $month, $day ) = $values;
		if( !checkdate( $month, $day, $year ) )
			$reset = true;
	}
	
}

if( $reset ){
	
	$curdate = date( "Y-m-d" );
	$estimate_validity = DBUtil::getParameterAdmin( "estimate_validity" );
	
	$valid_until = date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) + $estimate_validity,  date( "Y" ) ) );
	
}

//---------------------------------------------------------------------------------

//date de validité
$relaunch = $Order->get( "relaunch" );

if( $relaunch == "0000-00-00" ){ //aucune date saisie
	
	$relaunch = "";
	$startdate = date( "Y-m-d" );
	
}
else $startdate = $relaunch;

//----------------------------------------------------------------------------------

//paiement comptant
$cash_payment = $Order->get( "cash_payment" );
$no_tax_export = $Order->get( "no_tax_export" );

//montant réglé comptant
$balance_date = $Order->get( "balance_date" );
$startdate = $balance_date;

//Date de règlement
$balance_date = $Order->get( "balance_date" );

if( $balance_date == "0000-00-00" ){
	
	$balance_date = "";
	$startdate = date( "Y-m-d" );
	
}
else $startdate = $balance_date;

//-----------------------------------------------------------------------------------

$statusest = $Order->get("status");

include_once( dirname( __FILE__ ) . "/../../../objects/RegulationsBuyer.php" );
include_once( dirname( __FILE__ ) . "/../../../objects/Payment.php" );
$regulations = RegulationsBuyer::getEstimateRegulations( $IdEstimate );

if( count( $regulations ) ){
	?>
<!--
		<h1 class="titleEstimate">
			<span class="textTitle">Règlements reçus</span>
			<div class="spacer"></div>
		</h1>
		
		<div class="blocEstimateResult"><div style="margin:5px;">
		
	        <table class="dataTable devisTable" style="margin-top:5px; width:100%;">
	            <tr>
	                <th>Com.</th>
					<th>Acompte</th>
					<th>Date</th>
					<th>Mode de règlement</th>
					<th>Banque</th>
					<th>Pièce</th>
					<th>Commentaire</th>
					<th>Montant</th>
	            </tr>
			<?php
			for( $i = 0 ; $i < count( $regulations ) ; $i++ ){					
				?>
				<tr>
					<td><?php echo $regulations[ $i ][ "initial" ]; ?></td>
					<td><?php if( $regulations[$i]["from_down_payment"] == 1 ){ echo "Oui"; }else{ echo "Non"; } ?></td>
					<td><?php echo usDate2eu($regulations[$i]["payment_date"]); ?></td>
					<td><?php echo getPaymentTxt($regulations[$i]["idpayment"]); ?></td>
					<td><?php echo $regulations[$i]["bank"]; ?></td>
					<td><?php echo stripslashes($regulations[$i]["piece"]) ?></td>
					<td><?php echo stripslashes($regulations[$i]["comment"]) ?></td>
					<td><?php echo Util::priceFormat($regulations[$i]["amount"]); ?></td>
				</tr>
				<?php	
			}			
			?>
			</table>

		</div></div>

	<div class="spacer"></div>
	-->
<?
}
?>


	<h1 class="titleEstimate"><span class="textTitle">Paiement</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>
	
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">

		
	<div class="leftContainer" style="width:89%;">
		<script type="text/javascript">
    		function choufForCashPayment(idPaymentDelay) {
    			if(idPaymentDelay == 1){
    				document.getElementById('down_payment').checked=false;
    				document.getElementById('cash_payment').checked=true;
    				document.getElementById('down_payment').disabled = 'disabled';
    				enableCashPayment( true , true );
					dischpufleButtons();
					
					var maValue = document.forms.frm.elements['idpayment'].options[document.forms.frm.elements['idpayment'].selectedIndex].value;
					var myListe = document.forms.frm.elements['payment_idpayment'].options;
					var maSelected = 0;
					
					for(i = 0 ; i < myListe.length ; i++){
						if( myListe[i].value == maValue){
							maSelected = i;
						}
					}
					document.forms.frm.elements['payment_idpayment'].selectedIndex = maSelected;
													
    			} else {
    				enableCashPayment( false , true );
    				document.getElementById('down_payment').disabled = '';
    				document.getElementById('cash_payment').checked=false;
    				dischpufleButtons();
    				enableDP();
    			}
    		}

    		function checkCB(){
				cbID = <?php echo Payment::$PAYMENT_MOD_DEBIT_CARD; ?>;
        		selectedVal = $('#idpayment :selected').val();

				if( selectedVal == cbID ){
					idPaymentDelay = 1;
					$( '#idpayment_delay' ).val( 1 );
				}else{
					idPaymentDelay = $( '#idpayment_delay :selected' ).val( );
				}
				
        	}

			function confirmPayment(){
				
				return confirm( 'Générer la commande et passer au paiement ?' );
				
			}
        	
    	</script>
		
		
		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
			<label><span>Mode de règlement</span></label>
			<?php 
			
			//DrawLift_modepayment( $Order->get( "idpayment" ), empty( $disabled ), true ); --> Drawlift BURK CACA c'est sale! <--
			XHTMLFactory::createSelectElement( 'payment', 'idpayment', 'name_1', 'name_1', $Order->get( "idpayment" ), false, "", "name='idpayment' id='idpayment' $disabled onchange=\"$('#UpdateIdPayment').val( 1 );$('#UpdatePaymentDelay').val( 1 );checkCB(); \" " );
	
			?>
		 	<input type="hidden" id="UpdateBillingRate" name="UpdateBillingRate" value ="" />
	    	<input type="hidden" id="UpdateBillingAmount" name="UpdateBillingAmount" value ="" />
	    	<input type="hidden" id="UpdateInstallmentRate" name="UpdateInstallmentRate" value="" />
	    	<input type="hidden" id="UpdateInstallmentAmount" name="UpdateInstallmentAmount" value="" />
		 	<input type="hidden" name="UpdateIdPayment" id="UpdateIdPayment" value="" />
			<div class="spacer"></div>

			<div class="spacer"></div>

			<?php
				if( $Order->get( 'idpayment' ) == 1 && $Order->get( "cash_payment_idregulation" ) == 0 ){  ?>
					<label style="height:47px;"><span>Accès au <br/>paiement sécurisé</span></label>
					
					<p style="text-align:center; padding:5px 0 0 0; height:45px; width:70%;" class="textidSimp">
					<a href="<?php echo $GLOBAL_START_URL ?>/accounting/pay_secure.php?IdEstimate=<?php echo $IdEstimate ?>" onclick="return confirmPayment();">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/front_office/global/icons-payment.jpg" width="193" height="45" alt="Lien vers le paiement sécurisé" /></a>
					</p>
					
					<div class="spacer"></div>
			<?php } 
				if( $Order->get( 'idpayment' ) == 16 && $Order->get( "cash_payment_idregulation" ) == 0 ){  ?>
					<label style="height:47px;"><span>Accès au <br/>paiement sécurisé</span></label>
					
					<p style="text-align:center; padding:5px 0 0 0; height:45px; width:70%;" class="textidSimp">
					<a href="<?php echo $GLOBAL_START_URL ?>/accounting/pay_secure.php?fractionne&IdEstimate=<?php echo $IdEstimate ?>" onclick="return confirmPayment();">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/front_office/global/icons-payment.jpg" width="193" height="45" alt="Lien vers le paiement sécurisé" /></a>
					</p>
					
					<div class="spacer"></div>
			<?php } 
			?>

			
			<label><span>Délai de paiement</span></label>
	    	<?php
		    	if(isset($defaultPaymentDelay)){
		    		$idpayment_delay = $defaultPaymentDelay ; 
		    	}else{
		    		$idpayment_delay = $Order->get( "idpayment_delay" )?$Order->get( "idpayment_delay" ):0;
		    	}
		    	
				$lang = "_1";
				$specialchar=" onchange='$(\"#UpdateIdPayment\").val( 1 );$(\"#UpdatePaymentDelay\").val( 1 ); choufForCashPayment(this.options[this.selectedIndex].value);'";
				
				echo "<select style=\"border: 1px solid #AAAAAA;\" id=\"idpayment_delay\" name=\"idpayment_delay\" $disabled $specialchar>\n";
				
				$rsd = DBUtil::query("SELECT idpayment_delay, payment_delay$lang FROM payment_delay ORDER BY idpayment_delay ASC");
				if( $rsd->RecordCount() > 0 ){
					for( $i=0 ; $i < $rsd->RecordCount() ; $i++ ){
						if( $idpayment_delay == $rsd->Fields( "idpayment_delay" ) )
							$selected = " selected=\"selected\"";
						else $selected = "";
						
						echo "<option value=\"" . $rsd->Fields( "idpayment_delay" ) . "\"$selected>" . $rsd->Fields( "payment_delay$lang" ) . "</option>";
						$rsd->MoveNext();
			        }  
				}
			    echo "</select>";
			?>
			<input type="hidden" name="UpdatePaymentDelay" id="UpdatePaymentDelay" value="" />
			<div class="spacer"></div>
 	
		</div></div>		 	
		
		<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">				 	
			
			<label><span>Assurance crédit</span></label>
			<span class="textSimple"><?php displayInsurance( $Order, strlen( $disabled ) > 0 ); ?></span>
			<div class="spacer"></div>	 
			
			<?php
				if( $Order->getTotalATI() == 0.0 )
            		$rebate_rate = 0.0;
            	else 
            		$rebate_rate = $Order->get( "rebate_type" ) == "rate" 	? $Order->get( "rebate_value" ) : $Order->get( "rebate_value" ) * 100.0 / $Order->getTotalATI();
            	
            	$rebate_amount 	= $Order->get( "rebate_type" ) == "amount" ? $Order->get( "rebate_value" ) : $Order->getTotalATI() * $Order->get( "rebate_value" ) / 100.0;
			?>
			
			<label><span>Export HT</span></label>
			<?php if( $disabled ){ ?>
            	<?php if( $no_tax_export ) echo "Oui"; else echo "Non"; ?>
            <?php }else{ ?>
				<div class="simpleSelect">
				<select name="no_tax_export" id="no_tax_export"<?php echo $disabled ?>>
					<option value="0"<?php if( !$no_tax_export ) echo " selected=\"selected\""; ?>>Non</option>
					<option value="1"<?php if( $no_tax_export ) echo " selected=\"selected\""; ?>>Oui</option>
				</select>
				</div>
			<?php } ?>
			
			<label class="smallLabelMin"><span>Référence client</span></label>
			<input <?php echo $disabled ?> type="text" name="n_order" value="<?php echo htmlentities( $Order->get( "n_order" ) ); ?>" class="textidInputMin" style="width:14.3%;" />
			<div class="spacer"></div>	

            <!-- <label><span>Date de création interne</span></label> -->
            <?php  	
			if( $Order->get( "remote_creation_date" ) != "0000-00-00" )
				$remote_creation_date = usDate2eu( $Order->get( "remote_creation_date" ) );
			else $remote_creation_date = "";
			$startdate = empty( $remote_creation_date ) ? date( "Y-m-d" ) : $Order->get( "remote_creation_date" );
			?>
			<input <?php echo $disabled ?> class="calendarInput" type="hidden" name="remote_creation_date" id="remote_creation_date" value="<?php echo $remote_creation_date ?>" />
			<?php //DHTMLCalendar::calendar( "remote_creation_date" ); ?>  
			
			
			
		</div></div>
		<div class="spacer"></div>	
		
		<p style="color:#FF0000; font-weight:bold; margin-top:10px;">
			<?php
				if($idscoring_buyer)
					echo Dictionnary::translate("gest_com_scoring_buyer")." ".$idscoring_buyer." - ".Dictionnary::translate($scoring_buyer);
			?>
		</p>
	</div>
	
	<div class="rightContainer" style="width:10%;"><div class="tableContainer" style="margin-top:0px;">
        <table class="dataTable devisTable summaryTable" style="margin-bottom:0px;">
        	<tr>
        		<th colspan="2">Escompte</th>
        	</tr>
        	<tr>
        		<td><span id="epv"><?php echo Util::rateFormat( $rebate_rate ); ?></span></td>
        		<td><span id="ev"><?php echo Util::priceFormat( $rebate_amount ); ?></span></td>
        	</tr>
        	<tr>
        		<th colspan="2">Net à payer sur facture</th>
        	</tr>
        	<tr>
        		<td colspan="2"><span id="netp">
        		<?php 
        			/* ne pas utiliser Invoice::getBalanceOutstanding() ici, même si la facture existe car certaines données ont pû être modifiées dans la commande et ces modifications ne sont pas reportées dans le devis ( escompte, acompte, ... ) */
        			echo Util::priceFormat( $Order->getNetBill() ); 
        		?>
        		</span></td>
        	</tr>
      	</table>
	</div></div>
	
				
	<div class="leftContainer" style="width:89%; display:none;">
		<script type="text/javascript">
		
			function enableRebatePayment (enable) {				
				document.getElementById( "bloc_escompte" ).style.display = enable ? 'none' : 'block';	
				
				if( !enable ){					
					document.getElementById( "bloc_escompte" ).style.display = 'block';
				} else {
					document.getElementById( "bloc_escompte" ).style.display = 'none';
				}
				
				if( enable ){
					document.getElementById( "frm" ).elements[ 'rebate_rate' ].value = '';
					document.getElementById( "frm" ).elements[ 'rebate_amount' ].value = '';
				}
				
			}
		

			function enableCashPayment( enable , duclick){
				
				if(!document.getElementById('cash_payment').checked && !document.getElementById('down_payment').checked){
					disableAll();
					return;	
				}
				
				document.getElementById( "frm" ).elements[ 'payment_idpayment' ].disabled = enable ? '' : 'disabled';
				document.getElementById( "frm" ).elements[ 'cash_payment_piece' ].disabled = enable ? '' : 'disabled';
				document.getElementById( "frm" ).elements[ 'cash_payment_idbank' ].disabled = enable ? '' : 'disabled';
				document.getElementById( "frm" ).elements[ 'payment_comment' ].disabled = enable ? '' : 'disabled';
				document.getElementById( "frm" ).elements[ 'balance_date' ].disabled = enable ? '' : 'disabled';		
				
				if(enable)
					$( '#balance_date' ).datepicker( "enable" ) ;
				else $( '#balance_date' ).datepicker( "disable" );
				
				if(enable)
					$( '#down_payment_date' ).datepicker( "disable" );
				else $( '#down_payment_date' ).datepicker( "enable" );
				
				document.getElementById( "frm" ).elements[ 'down_payment_date' ].disabled = enable ? 'disabled' : '';
				
				document.getElementById( "frm" ).elements[ 'down_payment_date' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "frm" ).elements[ 'down_payment_rate' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "frm" ).elements[ 'down_payment_amount' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "frm" ).elements[ 'down_payment_piece' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "frm" ).elements[ 'down_payment_comment' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].disabled = enable ? 'disabled' : '';
				document.getElementById( "bloc_acompte" ).style.display = enable ? 'none' : 'block';
				
				
				if( !enable && duclick ){
					document.getElementById( "frm" ).elements[ 'down_payment_date' ].value = '<?=usDate2eu($Order->get( "down_payment_date" ))?>';
					document.getElementById( "frm" ).elements[ 'down_payment_rate' ].value = '<?=Util::numberFormat( $down_payment_rate )?>';
					document.getElementById( "frm" ).elements[ 'down_payment_amount' ].value = '<?=Util::numberFormat( $down_payment_amount )?>';
					
					var mesoptionsA = document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].options;
					var monoptionA = 0;
					for(i=0;i<mesoptionsA.length;i++){
						if( mesoptionsA[i].value == <?=$Order->get( "down_payment_idbank" )?$Order->get( "down_payment_idbank" ):0?> ){
							monoptionA = i ;
						}
					}
					
					document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].selectedIndex = monoptionA ;
					
					document.getElementById( "frm" ).elements[ 'down_payment_piece' ].value = '<?=$Order->get( "down_payment_piece" )?>';
					document.getElementById( "frm" ).elements[ 'down_payment_comment' ].value = '<?=$Order->get( "down_payment_comment" )?>';
					
					var mesoptions = document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].options;
					var monoption = 0;
					for(i=0;i<mesoptions.length;i++){
						if( mesoptions[i].value == <?=$Order->get( "down_payment_idpayment" )?> ){
							monoption = i ;
						}
					}
					
					document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].selectedIndex = monoption ;
					
					document.getElementById( "bloc_acompte" ).style.display = 'block';
					
				}else{
					if( enable && duclick){
						document.getElementById( "frm" ).elements[ 'down_payment_date' ].value = '';
						document.getElementById( "frm" ).elements[ 'down_payment_rate' ].value = '';
						document.getElementById( "frm" ).elements[ 'down_payment_amount' ].value = '';
						document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].selectedIndex = 0;
						document.getElementById( "frm" ).elements[ 'down_payment_piece' ].value = '';
						document.getElementById( "frm" ).elements[ 'down_payment_comment' ].value = '';
						document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].selectedIndex = 0;
						document.getElementById( "bloc_acompte" ).style.display = 'none';
					}
				}
				
				document.getElementById( "frm" ).elements[ 'balance_date' ].value = enable ? '<?php 
				
					list( $year, $month, $day ) = explode( "-", $Order->get( "balance_date" ) );
					echo $Order->get( "balance_date" ) != "0000-00-00" ? "$day-$month-$year" : date( "d-m-Y" ); 
					
				?>' : '';
				document.getElementById( 'balance_date_calendar' ).style.display = enable ? 'block' : 'none';

				var factor = document.getElementById( 'factor' );
				
				if( enable && factor ){
				
					switch( factor.tagName.toLowerCase() ){
					
						case "select" :
							
							var i = 0;
							while( i < factor.options.length ){
							
								if( factor.options[ i ].value == 0 )
									factor.selectedIndex = i;
									
								i++;
								
							}
							
							break;
							
						case "input" : factor.value = 0; break;
					
					}
					
				}
				
			}
			
			function dischpufleButtons(){
				if((document.getElementById('cash_payment').checked && document.getElementById('CPIdRegulation').value == 0 && document.getElementById( "frm" ).elements[ 'idpayment' ].options[document.getElementById( "frm" ).elements[ 'idpayment' ].selectedIndex].value != 1) || (document.getElementById('down_payment').checked && document.getElementById('DPIdRegulation').value == 0)){	
						
					if(document.getElementById( "Estimate2Orderbtn" )){
						document.getElementById( "Estimate2Orderbtn" ).onclick = function( ){
			       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
							$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
							$.blockUI.defaults.growlCSS.color = '#FFFFFF';
						    $.growlUI( '', " Cette action est impossible du fait qu'un acompte ou un paiement comptant est en cours de validation " );
						}								
					}	
				}else{
						
					if(document.getElementById( "Estimate2Orderbtn" )){
						document.getElementById( "Estimate2Orderbtn" ).disabled = '';
						document.getElementById( "Estimate2Orderbtn" ).onclick = function( ){
							<?php if ($vide==0){ ?>
								<?php if(!empty($todomessage)){ ?>
									PopupComments('<?=$IdEstimate?>');
								<?php }else{ ?>
									confirmSendOrder();
								<?php } ?>
							<?php } ?>
							
							<?php if($vide==1){ ?>
								confirmOrder();
							<?php } ?>
						}
					}
				}
			}
			
			$(document).ready(function() {
								
				<?if($Order->get( "cash_payment" ) || $Order->get( "down_payment_value" )>0 || $Order->get( "idpayment_delay" )==1){?>
					choufForCashPayment(<?php echo $Order->get( "idpayment_delay" ) ?>);
				<?php } else{?>
					disableAll();
				<?php } ?>
				
				<?php if( ($Order->get( "cash_payment" ) && $Order->get( "cash_payment_idregulation" )==0 ) || ( $Order->get( "down_payment_value" ) > 0 && $Order->get( "down_payment_idregulation" )==0 ) ){ ?>
					dischpufleButtons();
				<?php } ?>
				
				<?php if( $rebate_amount == 0 ) { ?> 
					disableEscompte(); 
				<?php } ?>
				
			});
			
			function disableEscompte(){
				document.getElementById( "bloc_escompte" ).style.display = 'none';
			}
			
			function disableAll(){
				
				$( '#balance_date' ).datepicker( "disable" );
				$( '#down_payment_date' ).datepicker( "disable" );
				
				document.getElementById( "frm" ).elements[ 'payment_idpayment' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'cash_payment_piece' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'cash_payment_idbank' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'payment_comment' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'balance_date' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'balance_date' ].value = '';
				
				document.getElementById( "frm" ).elements[ 'down_payment_rate' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'down_payment_amount' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'down_payment_piece' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'down_payment_comment' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].disabled = 'disabled';
				document.getElementById( "frm" ).elements[ 'down_payment_date' ].disabled = 'disabled';
				document.getElementById( "bloc_acompte" ).style.display = 'none';
			}

			function enableDP(){
				document.getElementById( "frm" ).elements[ 'down_payment_rate' ].disabled = '';
				document.getElementById( "frm" ).elements[ 'down_payment_amount' ].disabled = '';
				document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].disabled = '';
				document.getElementById( "frm" ).elements[ 'down_payment_piece' ].disabled = '';
				document.getElementById( "frm" ).elements[ 'down_payment_comment' ].disabled = '';
				document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].disabled = '';
				document.getElementById( "frm" ).elements[ 'down_payment_date' ].disabled = '';
				document.getElementById( "bloc_acompte" ).style.display = 'block';
			}
		</script>
		<input type="hidden" id="CPIdRegulation" value="<?=$Order->get('cash_payment_idregulation')?>">
		<input type="hidden" id="DPIdRegulation" value="<?=$Order->get('down_payment_idregulation')?>">
        <table class="dataTable devisTable" style="margin-top:10px; width:100%;">
            <tr>
                <th style="width:70px;">Paiement comptant</th>
                <th style="width:15%;">Date de règlement</th>
                <th style="width:15%;">Mode de règlement</th>
                <th style="width:15%;">Num. pièce</th>
                <th style="width:15%;">Banque émettrice</th>
                <th>Commentaire</th>
            </tr>
            <tr>
            	<td style="text-align:center;">
            		<input <?php echo $disabled ?> type="checkbox" id="cash_payment" name="cash_payment" value="1" onclick="enableCashPayment( this.checked , true );this.checked?document.getElementById('down_payment').checked=false:'';dischpufleButtons();"<?php if( $Order->get( "cash_payment" ) ) echo " checked=\"checked\" "; ?> style="vertical-align:bottom;" />
            	</td>
                <td style="white-space:nowrap;">
					<span>
						<input <?php echo $disabled ?> type="text" name="balance_date" id="balance_date" value="<?php echo usDate2eu( $balance_date ) ?>" readonly="readonly"<?php echo $cash_enabled ?> class="calendarInput" />
					</span>
					<span id="balance_date_calendar" style="margin-left:3px; margin-top:2px;<?php if( $cash_enabled ) echo" display:none;"; ?>">
						<?php DHTMLCalendar::calendar( "balance_date" ); ?>
					</span>
				</td>
				<td style="white-space:nowrap;">
					<?php DrawLift("payment",$Order->get("payment_idpayment"),"", !strlen( $disabled ),false,"payment_idpayment"); ?>
				</td>
				<td style="white-space:nowrap;">
					<input <?php echo $disabled ?> type="text" name="cash_payment_piece" class="textInput" style="width:100px;" value="<?=htmlentities(stripslashes($Order->get("cash_payment_piece")))?>" />
				</td>
				<td style="white-space:nowrap;">
					<select <?php echo $disabled ?> name="cash_payment_idbank" id="cash_payment_idbank">
						<?
						$selectedIDBcp = $Order->get("cash_payment_idbank");
						?>
						<option value="0">Sélection</option>
						<?
						$banks = DBUtil::query("SELECT * FROM bank");
						while(!$banks->EOF()){
							$idbcp = $banks->fields('idbank');
							$bname = $banks->fields('bank_name');
							if($selectedIDBcp==$idbcp)
								echo "<option value='$idbcp' selected>$bname</option>";									
							else
								echo "<option value='$idbcp'>$bname</option>";
							
							$banks->moveNext();
						}
						?>
					</select>
				</td>
				<td style="white-space:nowrap;">
					<input <?php echo $disabled ?> type="text" name="payment_comment" value="<?=htmlentities(stripslashes($Order->get("payment_comment")))?>" class="textInput" size="35" />
				</td>
			</tr>
		</table> 
	</div>

	<div class="leftContainer" style="width:89%;">
		
		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
			<h2>
			<input <?php echo $disabled ?> type="checkbox" onchange="return recalEstimate();" id="down_payment" name="down_payment" value="1" onclick="enableCashPayment( !this.checked , true ); this.checked?document.getElementById('cash_payment').checked=false:''; dischpufleButtons();"<?php if( $Order->get( "down_payment_value" )>0.00 ) echo " checked=\"checked\" "; ?> /> 
			Acompte</h2>
			
			<div id="bloc_acompte">
				<label><span>Montant</span></label>
				<input<?php echo $disabled ?> id="ap" onfocus="this.select()" onkeyup="return recalEstimate();" type="text" class="calendarInput percentage" id="down_payment_rate" name="down_payment_rate" value="<?php echo Util::numberFormat( $down_payment_rate ) ?>" />
				<input id="apold" type="hidden" name="old_down_payment_rate" value="<?php echo Util::numberFormat( $down_payment_rate ); ?>" /> 
				<span class="textidSimp" style="width:5.4%;">%</span>
					
				<input<?php echo $disabled ?> id="a" onfocus="this.select()" onkeyup="return recalEstimate();" type="text" class="calendarInput price" id="down_payment_amount" name="down_payment_amount" value="<?php echo Util::numberFormat( $down_payment_amount ) ?>" />
				<input id="aold" type="hidden" name="old_down_payment_amount" value="<?php echo Util::numberFormat( $down_payment_amount ); ?>" /> 
				<span class="textidSimp" style="width:5.4%;">&euro;</span>	
				<div class="spacer"></div>	
				
				<label><span>Date de règlement</span></label>
				<input<?php echo $disabled ?> type="text" name="down_payment_date" id="down_payment_date" value="<?php echo usDate2eu( $Order->get("down_payment_date") ) ?>" readonly="readonly" class="calendarInput" />
				<?php DHTMLCalendar::calendar( "down_payment_date" ); ?>
				
				<label class="smallLabel" style="width:22%;"><span>Mode règlement</span></label>
				<div class="simpleSelect">
					<?php DrawLift("payment", $Order->get("down_payment_idpayment"),"", !strlen( $disabled ), false, "down_payment_idpayment");?>
				</div>
				<div class="spacer"></div>
						
				<label><span>Num. pièce</span></label>
				<input <?php echo $disabled ?> type="text" name="down_payment_piece" class="calendarInput" value="<?=htmlentities(stripslashes($Order->get("down_payment_piece")))?>" />
				<span class="textidSimp" style="width:3.4%;">&nbsp;</span>
				
				<label class="smallLabel" style="width:22%;"><span>Banque émettrice</span></label>		
				<div class="simpleSelect"><select <?php echo $disabled ?> name="down_payment_idbank" id="down_payment_idbank">
					<?php $selectedIDB = $Order->get("down_payment_idbank"); ?>
					<option value="0">Sélection</option>
					<?php $banks->moveFirst();
					while(!$banks->EOF()){
						$idb = $banks->fields('idbank');
						$bname = $banks->fields('bank_name');
						if($selectedIDB==$idb)
							echo "<option value='$idb' selected>$bname</option>";									
						else
							echo "<option value='$idb'>$bname</option>";
						
						$banks->moveNext();
					}
					?>
				</select></div>	
				<div class="spacer"></div>	
				
				<label><span>Commentaire<br/>&nbsp;</span></label>		
				<textarea<?php echo $disabled ?> name="down_payment_comment" class="textInput"><?=htmlentities(stripslashes($Order->get("down_payment_comment")))?></textarea>
				<div class="spacer"></div>
			</div> <!-- #bloc_acompte -->
								
		</div></div>
		<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
			<h2>
			<input type="checkbox" onchange="return recalEstimate();" id="rebate_payment" name="rebate_payment" value="1" onclick="enableRebatePayment( !this.checked , true );"<?php if( $rebate_amount > 0.00 ) echo " checked=\"checked\" "; ?> /> 
			Escompte</h2>
			
			<div id="bloc_escompte" style="<?php if( $rebate_amount > 0.00 ) echo "display:block;"; ?>">
				<label><span>Escompte</span></label>
				<input<?php echo $disabled ?> id="ep" onfocus="this.select()" onkeyup="return recalEstimate();" type="text" name="rebate_rate" class="calendarInput percentage" value="<?php echo Util::numberFormat( $rebate_rate ); ?>" /> 
				<input id="epold" type="hidden" name="old_rebate_rate" value="<?php echo Util::numberFormat( $rebate_rate ); ?>" /> 
				<span class="textidSimp" style="width:5.4%;">%</span>	
				
				<input<?php echo $disabled ?> id="e" onfocus="this.select()" onkeyup="return recalEstimate();" type="text" name="rebate_amount" class="calendarInput price" value="<?php echo Util::numberFormat( $rebate_amount ); ?>" /> 
				<input id="eold" type="hidden" name="old_rebate_amount" value="<?php echo Util::numberFormat( $rebate_amount ); ?>" /> 
				<span class="textidSimp" style="width:5.4%;">&euro;</span>	
				<div class="spacer"></div>	
			</div>
			
		</div></div>
		<div class="spacer"></div>	
	</div>
	<div class="spacer"></div>	
        
	<!-- <div class="leftContainer" style="width:89%;">
    	<input <?php echo $disabled ?> type="submit" style="float:right; margin-top:5px;" class="blueButton" value="Enregistrer" name="ModifyEstimate" />
    </div> -->
    <div class="spacer"></div>
        
	</div></div>
	<div class="spacer"></div>	
</div>

<?php

/* ------------------------------------------------------------------------------------------------------------------------- */
/* assurance crédit */

function displayInsurance( Estimate &$estimate, $disabled = false ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	//assurance non éditable
	
	if( in_array( $estimate->get( "status" ), array(
			
		Estimate::$STATUS_REFUSED,
		Estimate::$STATUS_ORDERED

	) ) ){
		
		echo $estimate->get( "insurance" ) ? "oui" : "non";
		
		?>
		<input type="hidden" id="insurance" name="insurance" value="<?php echo $estimate->get( "insurance" ); ?>" />
		<?php
				
		return;

	}
	
	/* pas d'assurance crédit pour les paiements comptant */
						
	if( $estimate->get( "cash_payment" ) ){
		
		?>
		non
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
	
	//assurance crédit désactivée
	
	$idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) );
	
	if( !$idcredit_insurance ){
			
		?>
		non disponible
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php	
		
		return;
		
	}

	/* assurance crédit activée */
	
	$creditInsurance			= ( object )DBUtil::query( "SELECT * FROM credit_insurance WHERE idcredit_insurance = '$idcredit_insurance' LIMIT 1" )->fields;
	$customer 			=& $estimate->getCustomer();
	$insurance 			= $estimate->get( "insurance" );
	$minimum 			= floatval( $creditInsurance->minimum );
	$maximum 			= floatval( $creditInsurance->maximum );
	$netBillET 			= round( $estimate->getNetBill() / ( 1.0 + $estimate->getVATRate() / 100.0 ), 2 );
	$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();
	
	/* client réfusé */
	
	if( $customer->get( "insurance_status" ) == "Refused" ){
		
		?>
		<b style="color:#FF0000;">Client refusé</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
	
	if( $customer->get( "insurance_status" ) == "Deleted" ){
		
		?>
		<b style="color:#FF0000;">Client supprimé</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}

	if( $customer->get( "insurance_status" ) == "Terminated" ){
		
		?>
		<b style="color:#FF0000;">Client résilié</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}	
	
	/* client non assuré */
	
	if( !$customer->get( "insurance" ) ){
		
		?>
		<b style="color:#FF0000;">Demande à faire</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
	
	/* montant non couvert */
	
	if( $netBillET < $minimum ){
		
		?>
		En dessous du seuil
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
	
	}

	if( $netBillET > $maximum ){
		
		?>
		Au dessus du seuil
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;

	}
	
	/* encours maximum atteint */
	
	if( $outstandingCredits + $netBillET > $customer->get( "insurance_amount" ) ){
		
		?>
		<b style="color:#FF0000;">Encours maximum atteint</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
	
	/* assurance éditable */
	
	if( !$estimate->get( "insurance_auto" ) ){
		?>
		<input<?php if( $estimate->get( "insurance" ) ) echo " checked=\"checked\""; if( $disabled ) echo " disabled=\"disabled\""; ?> type="radio" name="insurance" value="1" onclick="updateEstimate( 'insurance', this.value );" /> oui
		<input<?php if( !$estimate->get( "insurance" ) ) echo " checked=\"checked\""; if( $disabled ) echo " disabled=\"disabled\""; ?> type="radio" name="insurance" value="0" onclick="updateEstimate( 'insurance', this.value );" /> non
		<?php
	}else{
		?>
		<input checked="checked" <?php if( $disabled ) echo " disabled=\"disabled\""; ?> type="radio" name="insurance" value="1" onclick="updateEstimate( 'insurance', this.value );" /> oui
		<input<?php if( $disabled ) echo " disabled=\"disabled\""; ?> type="radio" name="insurance" value="0" onclick="updateEstimate( 'insurance', this.value );" /> non
		<?php		
	}
						
}

/* ------------------------------------------------------------------------------------------------------------------------- */

?>