<?php

include_once( dirname( __FILE__ ) . "/../../config/init.php" );

if( isset( $key ) ){
	
	include_once( "$GLOBAL_START_PATH/objects/Customer.php" );
	$customer = new Customer( $key );
	
}

if( $type == "contact" && isset( $key ) )
	$submit = "Enregistrer les modifs.";
elseif( $type == "buyer" && isset( $key ) )
	$submit = "Enregistrer les modifs.";
elseif( $type == "contact" && !isset( $key ) )
	$submit = "Enregistrer prospect";
elseif( $type == "buyer" && !isset( $key ) )
	$submit = "Enregistrer client";
else
	$submit = "Modifier";

//---------------------------------------------------------------------------------------------

include_once ("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
include_once ("$GLOBAL_START_PATH/objects/AutoCompletor.php");

$phonenumber_default	= "";
$gsm_default			= "";
$faxnumber_default		= "";
$email_default			= "";

//---------------------------------------------------------------------------------------------

//champs obligatoires dynamiques, en fonction du profil client à parametrer ici
//Note le numéro de SIRET, le code NAF et le numéro de TVA Intracommunautaire ne doivent pas être gérés ici
$requiredElements = array(

	/*prospects*/
	
	$contact = 1 => array(
		
		$idcustomer_profile = -1 /*aucune sélection*/		=> array( "idcustomer_profile" ),
		$idcustomer_profile = 0 /*aucun*/ 					=> array( "idcustomer_profile" ),
		$idcustomer_profile = 1 /*particulier*/ 			=> array( "idstate", "title", "Lastname", "idsource" ), 
		$idcustomer_profile = 2 /*acheteur*/ 				=> array( "Company", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 3 /*association*/ 			=> array( "Company", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 4 /*administration*/ 			=> array( "Company", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 5 /*grand compte*/ 			=> array( "Company", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 6 /*revendeur*/ 				=> array( "Company", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 7 /*administrateur*/ 			=> array( "Company", "idstate", "title", "Lastname", "idsource" )
		
	),
	
	/*clients*/
	
	$contact = 0 => array( 
		
		$idcustomer_profile = -1 /*aucune sélection*/ 	=> array( "idcustomer_profile" ),
		$idcustomer_profile = 0 /*aucun*/ 				=> array( "idcustomer_profile" ),
		$idcustomer_profile = 1 /*particulier*/ 				=> array( "Adress", "City", "Zipcode", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 2 /*acheteur*/ 				=> array( "Company", "City", "Zipcode", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 3 /*association*/ 				=> array( "Company", "City", "Zipcode", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 4 /*administration*/ 			=> array( "Company", "City", "Zipcode", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 5 /*grand compte*/ 			=> array( "Company", "City", "Zipcode", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 6 /*revendeur*/ 				=> array( "Company", "City", "Zipcode", "idstate", "title", "Lastname", "idsource" ),
		$idcustomer_profile = 7 /*administrateur*/ 			=> array()
		
	)
	
);

//---------------------------------------------------------------------------------------------

if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "month_term" ){

	$month_term = $_POST[ "month_term" ] ;
	$reference = $_POST[ "reference" ] ;
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE detail 
	SET month_term = '" . Util::html_escape( stripslashes( $month_term )) . "' 
	WHERE reference = '" . $reference . "' 
	 
	";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
?>
<script type="text/javascript">
    function changeNAFText( element ){ document.getElementById( "Naf" ).value = element.info; }
        function loadBillingcontt(id, idb){
    		var divToComplete = document.getElementById('loadcomments') ;
    		if(id != -1){
    		document.getElementById('loadZones').style.visibility="visible";
    		document.getElementById('msge').style.visibility = "hidden";
    		}
    		
    document.frm.idcontact.value=id ;
    document.getElementById('table').style.visibility = "hidden";
    //document.getElementById('idcontact').value=id ;
    		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/loadBillingcont.php?idBillingc=' + id + '&idBillingb=' + idb;
    		//window.location.href="page2.htm?param1="+param1+"&param2="+param2; 
    		divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /> Chargement en cours ...</div>';	
    		var xhr_object = null;
    		if(window.XMLHttpRequest) // Firefox
    		   xhr_object = new XMLHttpRequest();
    		else if(window.ActiveXObject) // Internet Explorer
    		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
    		else { // XMLHttpRequest non supporté par le navigateur
    		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
    		   return;
    		}
    
    		xhr_object.onreadystatechange = function()
    		{
    		if(xhr_object.readyState == 4)
    		{
    		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
    			if(xhr_object.status == 200)
    			
    			
    				divToComplete.innerHTML = xhr_object.responseText ;
    			else
    				return ;
    			}
    		}; 
    		
    		xhr_object.open("GET", location, true);
    		xhr_object.send(null);
    
    }
    function show_factor_infos() {
					
					var status = document.forms.frm.elements['factor_status'].options[document.forms.frm.elements['factor_status'].selectedIndex].value; 
					var factmttitlediv = document.getElementById( 'fact_mt_title' );
					var factmtdiv = document.getElementById( 'fact_mt' );
					var FactorAmountCreationDateDiv = document.getElementById( 'FactorAmountCreationDateDiv' );
					var FactorAmountCreationDateTitleDiv = document.getElementById( 'FactorAmountCreationDateTitleDiv' );
					var factdatetitlediv = document.getElementById( 'fact_date_title' );
					var factdatediv = document.getElementById( 'fact_date' );
					
					if(status=='Accepted'){ 
						
						factmttitlediv.style.display = 'block';
						factmtdiv.style.display = 'block';
						FactorAmountCreationDateDiv.style.display = 'block';
						FactorAmountCreationDateTitleDiv.style.display = 'block';
						
					}else{
						
						factmttitlediv.style.display = 'none';
						factmtdiv.style.display = 'none';
						FactorAmountCreationDateDiv.style.display = 'none';
						FactorAmountCreationDateTitleDiv.style.display = 'none';
						
					}
					
					if(status=='Refused'){ 
						
						factdatetitlediv.style.display = 'block';
						factdatediv.style.display = 'block';
						
					}else{
						
						factdatetitlediv.style.display = 'none';
						factdatediv.style.display = 'none';
						
					}
					
				}
				

</script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<!-- Google Maps  -->
<!-- Prod 	<script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=ABQIAAAAak7GSOsDGAIL-xxfeapIOxTZ42yXlyKOzICi2Y7x0YjVNiUDFRQUmmX5PrZeee0AOG4sFeV9qyAgBQ"></script> -->
<?php if( isset( $key ) && isset( $google_map_api_key ) && $google_map_api_key != "" ){ ?>
	<script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;sensor=false&amp;key=<?php echo $google_map_api_key ?>"></script>
<?php } ?>
<!-- Fin Google Maps -->

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<?php
// Les scrpts JS étant immenses ils ont été déplacés dans ce fichier, merci
include_once($GLOBAL_START_PATH.'/templates/sales_force/contact_buyer_js.php');/*if (isset ($_POST['Modifiy']) ) var_dump($_POST['Modifiy']);*/
?>





<!--<form action="contact_buyer.php?type=<?php echo $type ?>#topform" method="post" name="frm" id="frm" onsubmit="return checkRequiredElements( <?php echo $type == "contact" ? "1" : "0" ?> ) && checkEmailOrFax() && checkPhoneOrMobile();" enctype="multipart/form-data">-->

<form action="contact_buyer.php?type=<?php echo $type ?>#suivi" method="post" name="frm" id="frm" onsubmit="return checkRequiredElements( <?php echo $type == "contact" ? "1" : "0" ?> )" enctype="multipart/form-data">
<?php if( isset( $_REQUEST[ "IdEstimate" ] ) ){ ?><input type="hidden" name="IdEstimate" value="<?php echo  $_REQUEST[ "IdEstimate" ]  ?>" /><?php } ?>
<?php if( isset( $_REQUEST[ "IdOrder" ] ) ){ ?><input type="hidden" name="IdOrder" value="<?php echo  $_REQUEST[ "IdOrder" ]  ?>" /><?php } ?>
<?php if( isset( $_REQUEST[ "idbilling_buyer" ] ) ){ ?><input type="hidden" name="idbilling_buyer" value="<?php echo  $_REQUEST[ "idbilling_buyer" ]  ?>" /><?php } ?>
<?php if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ ?><input type="hidden" name="VirtualEstimate" value="<?php echo  $_REQUEST[ "VirtualEstimate" ]  ?>" /><?php } ?>
<?php if( !isset( $key ) ){ ?><input type="hidden" name="newkey" value="<?php echo User::getInstance()->getId() ?>" /><?php } ?>
<?php if( $type == "contact" ){ ?><input type="hidden" name="contact" value="1" />
<?php }elseif( isset( $_GET[ "act" ] ) && $_GET[ "act" ] == "ptob" ){ ?><input type="hidden" name="contact" value="0" /><?php } ?>
<?php if( isset( $key ) ){ ?><input type="hidden" name="key" value="<?php echo $key ?>" /><?php } ?>

<input type="hidden" name="user_changed" value="0" />	

<div class="centerMax">
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<!-- Bloc de droite -->
	<div id="tools" class="rightTools">

		<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-tool.png" alt="star" />&nbsp;
		Outils</h2>
		<div class="spacer"></div>

		<div class="blocSearch">
			<?php if( isset( $key ) ) {
				// ------- Mise à jour de l'id gestion des Index  #1161
				$hasLitigationInvoices = DBUtil::query("SELECT COUNT(idbilling_buyer) as cpt FROM billing_buyer WHERE litigious > 0 AND idbuyer = '$key' ")->fields("cpt");
				if($hasLitigationInvoices){?>
					<div class="content" style="line-height:15px;padding:5px;">
						<span class="msgError">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" class="verticalMiddle" />
							<?=$hasLitigationInvoices?> facture<?php echo $hasLitigationInvoices == 1 ? "" : "s" ?> litigieuse<?php echo $hasLitigationInvoices == 1 ? "" : "s" ?> impayable<?php echo $hasLitigationInvoices == 1 ? "" : "s" ?>
						</span>
					</div>
				<?php }
			} ?>
			
			<?php if( $type == "contact" && isset( $key ) ){ ?>
				<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $key ?>&amp;act=ptob<?php echo ( isset( $_REQUEST[ "IdEstimate" ] ) && !empty( $_REQUEST[ "IdEstimate" ] ) ) ? "&amp;IdEstimate=" . $_REQUEST[ "IdEstimate" ] : "" ?>" class="normalLink blueText">&#0155; Passer le prospect en client</a>
			<?php } ?>
			
			<?php if( isset( $key ) ){ ?>
				<?php if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ //provenance  = devis express ?>
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo  $_REQUEST[ "VirtualEstimate" ]  ?>" class="normalLink blueText">&#0155; Retour au devis</a>
				<?php } ?>
				
				<?php if( isset( $_REQUEST[ "IdEstimate" ] ) ){ ?>
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo  $_REQUEST[ "IdEstimate" ]  ?>" class="normalLink blueText">&#0155; Retour au devis</a>
				<?php } ?>
				
				<?php if( isset( $_REQUEST[ "IdOrder" ] ) ){ ?>
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo  $_REQUEST[ "IdOrder" ]  ?>" class="normalLink blueText">&#0155; Retour à la commande</a>
				<?php } ?>
				
				<?php if( isset( $_REQUEST[ "idbilling_buyer" ] ) ){ ?>
					<a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo  $_REQUEST[ "idbilling_buyer" ]  ?>" class="normalLink blueText">&#0155; Retour à la facture</a>
				<?php } ?>
				
				<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $key ?>&amp;idcontact=0" class="normalLink blueText">&#0155; Créer un devis</a>
				<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $key ?>&amp;idcontact=0" class="normalLink blueText">&#0155; Créer une commande</a>
				
				<?php if( $relaunched ){ ?>
					<a href="#" onClick="seeRelaunch(); return false;">&#0155; Client relancé</a>
				<?php } ?>
				
				<?php if( DBUtil::getDBValue( "insurance_status", "buyer", "idbuyer", $key ) == "Refused" ){ ?>
					<p class="msgError floatleft" style="margin:5px 0 0 5px;">Client refusé à l'assurance</p>
				<?php } ?>
				
				<?php // credit dispo
				$credit_amount = DBUtil::getDBValue( "credit_balance", "buyer", "idbuyer", $key );
				if( $credit_amount > 0 ){ ?>
					<p class="msgError floatleft" style="margin:5px 0 0 5px;">Crédit disponible <?=Util::priceFormat($credit_amount)?></p>
				<?php } ?>
				<div class="spacer"></div> 
				
				
				
					
			<?php } ?>					

			
		</div>
		<div class="spacer"></div>
						

		<?			
			$nbdevis = 0; $nbcdes = 0; $totDevis = 0; $totCdes = 0;
			$lastDevis = "-"; $lastCde = "-";
			if( isset( $key ) ){
				$nbdevis 	= DBUtil::query("SELECT count(idestimate) as cpte FROM estimate WHERE idbuyer='$key'")->fields('cpte');
				$nbcdes		= DBUtil::query("SELECT count(idorder) as cpte FROM `order` WHERE idbuyer='$key'")->fields('cpte');
				$lastDevis	= DBUtil::query("SELECT MAX(DateHeure) as maDate FROM estimate WHERE idbuyer='$key'")->fields('maDate');
				$lastCde	= DBUtil::query("SELECT MAX(DateHeure) as maDate FROM `order` WHERE idbuyer='$key'")->fields('maDate');
				$totDevis	= DBUtil::query("SELECT SUM(total_amount_ht) as sumo FROM estimate WHERE idbuyer='$key'")->fields('sumo');
				$totCdes	= DBUtil::query("SELECT SUM(total_amount_ht) as sumo FROM `order`  WHERE idbuyer='$key'")->fields('sumo');
			}
		?>

		<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
		Cotation client</h2>
		<div class="spacer"></div>

		<div class="blocSearch">
			<a href="#" onClick="javascript: $('#container').tabs( 'select' ,4 );" class="normalLink blueText">&#0155; Nombre cdes. 
			<?=$nbcdes?></a>
			<a href="#" onClick="javascript: $('#container').tabs( 'select' ,4 );" class="normalLink blueText">&#0155; Dernière cde. 
			<?php if($lastCde!="" && $lastCde!= '-'){ echo Util::DateFormat($lastCde,false); } else { echo '-'; } ?></a>
			<a href="#" onClick="javascript: $('#container').tabs( 'select' ,4 );" class="normalLink blueText">&#0155; Total cdes. HT 
			<?=Util::priceFormat($totCdes)?></a><br/>
			
			<a href="#" onClick="javascript: $('#container').tabs( 'select' ,4 );" class="normalLink blueText">&#0155; Nombre devis 
			<?=$nbdevis?></a>
			<a href="#" onClick="javascript: $('#container').tabs( 'select' ,4 );" class="normalLink blueText">&#0155; Dernier devis 
			<?php if($lastDevis!="" && $lastDevis!= '-'){echo Util::DateFormat($lastDevis,false);}else{ echo '-';} ?></a>
			<a href="#" onClick="javascript: $('#container').tabs( 'select' ,4 );" class="normalLink blueText">&#0155; Total devis HT 
			<?=Util::priceFormat($totDevis)?></a>
			<div class="spacer"></div>
		</div>

		<div class="spacer"></div>

		<div id="contener_page_blanche" class="displayNone">
			<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-pageb.png" alt="star" />&nbsp;
			Pages Blanches</h2>
			<div class="spacer"></div>
	
			<div class="blocSearch">
	
			 	<label><span>Nom</span></label>
			 	<input class="textInput" type="text" name="pb_search_nom" id ="pb_search_nom" value=""/>
			 	<div class="spacer"></div>
			 	
				<label><span>Prénom</span></label>
				<input class="textInput" type="text" name="pb_search_prenom" id="pb_search_prenom" />
				<div class="spacer"></div>
				
				<label><span>Code postal / Ville</span></label>
				<input type="text" class="textInput" name="pb_search_cp" id="pb_search_cp" />
				<div class="spacer"></div>
												
				<input type="button" id="ButtonSearchPb" value="Ok" class="inputOk" onClick="javascript: getInfosFromPb();"/>
				<div class="spacer"></div>
				
				<div id="responsePb"></div>
				<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
		</div>
		
		<?php $groupings = DBUtil::query("SELECT * FROM grouping ORDER BY grouping ASC"); ?>
	
		<script type="text/javascript">
		
			var groupings = new Array();
			<?php $i=0;
			while(!$groupings->EOF()){ ?>
				groupings[<?=$i?>] = new Array();
				
				<?
				echo 'groupings['.$i.']["id"] = "'.$groupings->fields('idgrouping').'" ; ';
				if($groupings->fields('grouping_logo')!=""){
					echo 'groupings['.$i.']["logo"] = "'.$GLOBAL_START_URL.'/www/'.$groupings->fields('grouping_logo').'" ; ';
				}else{
					echo 'groupings['.$i.']["logo"] = ""; ';
				}
				$i++;					
				$groupings->MoveNext();	
			}
			?>
			
		</script>
	
		<script type="text/javascript">
			function modifyGrouping(newGroupingId){
										
				var postData = "key=<?=$key?>&updateGrouping&newGroupingId="+newGroupingId;
				
				$.ajax({

					url: "<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php",
					async: true,
					cache: false,
					type: "GET",
					data: postData,
					error: function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la fiche client" ); },
				 	success: function( responseText ){

	        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
	        			$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
	        			$.blockUI.defaults.growlCSS.color = '#FFFFFF';

	        			if( responseText == ''){
	        				$.blockUI.defaults.growlCSS.backgroundColor = '#000';
		        			$.growlUI( '', 'Modifications enregistrées' );
					    	window.location.reload();
		        		}else{
				    		$.growlUI( '', responseText );
				    	}
					}
				});
			}
			
			
			function updateGroupingLogo(newGroupingId){
				id = 'x';
				
				for( i=0 ; i<groupings.length ; i++ ){
					if( newGroupingId == groupings[i]['id']){
						id = i;
					}
				}
				
				if( id!='x' && groupings[id]['logo']!='' ){ 
					document.getElementById('groupingLogo').innerHTML = '';									
					$('#groupingLogo').append('<img src="'+groupings[id]['logo']+'" />');
				}else{
					document.getElementById('groupingLogo').innerHTML = '';
				}
				
				document.getElementById('updateGrouping').value=1;
				
			}
		</script>
  
		<?php
		if( isset( $key ) ){
			$customerGrouping = DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $key );
			$groupingName = "" ;
			$groupingLogo = "" ;
			$groupingId = "";
			if( $customerGrouping ) {
				$groupingId	  = DBUtil::getDBValue( "idgrouping", "grouping", "idgrouping", $customerGrouping );												
				$groupingName = DBUtil::getDBValue( "grouping", "grouping", "idgrouping", $customerGrouping );
				$groupingLogo = DBUtil::getDBValue( "grouping_logo", "grouping", "idgrouping", $customerGrouping );
			}
		} else {
			$groupingName = "" ;
			$groupingLogo = "" ;
			$groupingId = "";
		}	
		?>

		<?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping"); ?>
		<?php if($akilae_grouping==1){ ?>

		<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-group.png" alt="star" />&nbsp;
		Groupement</h2>
		<div class="spacer"></div>

		<div class="blocSearch">
			<input type="hidden" name="updateGrouping" id="updateGrouping" value="0">
			<select name="select_grouping" id="select_grouping" onChange="updateGroupingLogo( this.options[this.selectedIndex].value);" >
				<option value=0>Aucun</option>
				<?
				if($groupings->RecordCount()){
					$groupings->MoveFirst();
					while(!$groupings->EOF()){
						
						$actualIdGrouping = $groupings->fields("idgrouping");
						$selected = "";
						if($actualIdGrouping == $groupingId ){
							$selected = ' selected="selected"';
						}
						echo '<option value="'.$actualIdGrouping.'"'.$selected.'>'.$groupings->fields('grouping')."</option>";
						
						$groupings->MoveNext();	
					}
				}
				?>
		   </select>
		   <div class="spacer"></div>
		
		   
		   	<?php if( isset( $key ) ){ ?>
		   		<input type="button" class="inputOk" value="Ok" onClick="modifyGrouping( document.getElementById( 'select_grouping' ).options[document.getElementById( 'select_grouping' ).selectedIndex].value);">
				<div class="spacer"></div>
			<?php }?>
			
			<?php if($groupingLogo != ""){ ?>
				<div class="alignCenter" id="groupingLogo">
					<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $groupingLogo ?>" class="marginTop" />
				</div>
				<div class="spacer"></div>
			<?php } ?>
			
			
		</div>
		  
		<div class="spacer"></div>
		
	 <?php } ?>
	</div>	

	
	<!-- Bloc formulaire -->
	<div class="contentDyn">
		
		<div id="CitySelection" class="displayNone"></div>
				
			<!-- Titre -->
			<h1 class="titleSearch">
			<span class="textTitle" style="width:72%;">
			<?php if( isset( $key ) ){ ?>
					
					<?php echo $type == "contact" ? "Prospect" : "Client" ?>
				
					 N° <?php				
					$iderp = $rsbuy->fields("iderp");
					if( isset ( $iderp ) ) { echo $rsbuy->fields( "iderp" ) . "/" . $key; }
					else { echo $key; } ?> - 
					<?php echo $raison; ?> - 
					<?php
						if($rscp->Fields( "title" ) == 2) {
							echo "Madame ";
						}
						else if($rscp->Fields( "title" ) == 1) {
							echo "Monsieur ";
						}
						else if($rscp->Fields( "title" ) == 3) {
							echo "Mademoiselle ";
						}
						echo $nom; 
					?>
					
				<?php
				} else { ?>
					Création <?php echo $type == "contact" ? "prospect" : "client" ?>
			<?php } ?>			
			</span>
			<span class="selectDate" style="width:26%;">
			<?php if( isset( $key ) ){ ?>
							<strong>Date création :</strong>
							<?php echo ucfirst( humanReadableDate( substr( $rsbuy->Fields( "create_date" ), 0, 10 ) ) ) ?> 
							<?php echo substr( $rsbuy->Fields( "create_date" ), 0, 4 ) ?></b> à 
							<?php echo substr( $rsbuy->Fields( "create_date" ), 11, 2 ) ?>h<?php echo substr( $rsbuy->Fields( "create_date" ), 14, 2 ) ?>
						<?php } ?>&nbsp;
			</span>
			<div class="spacer"></div>
			</h1>
			
			<div id="container">
				<!-- Onglet -->
				<ul class="menu">
					<li class="item"><a href="#name-address"><span>Coordonnées</span></a></li>
					<?php if(isset($key)){ ?>
						<li class="item"><a href="#gestContacts"><span>Contacts</span></a></li>
					<?php }	?>
					<!--<li class="item"><a href="#insurance"><span>Assurance</span></a></li>-->
					<?php if(isset($key)){ ?>
						<li class="item"><a href="#history"><span>Historique</span></a></li>
					<?php }	?>
					<li class="item"><a href="#rib"><span>RIB</a></li>
					<li class="item"><a href="#pricing"><span>Règlements</span></a></li>
					<!--<li class="item"><a href="#factor"><span>Factor</span></a></li>-->
					<li class="item"><a href="#address"><span>Adresses</span></a></li>
					<li class="item" id="menuInfoSociete"><a href="#infosDivers"><span>Infos société</span></a></li>
					<li class="item"><a href="#infosComp"><span>Infos complémentaires</span></a></li>
<li class="item"><a href="#suivi"><span>Suivi <?php if(isset($idbuyer)){	 $qs = DBUtil::query("SELECT * FROM contact_follow WHERE idbuyer = '$idbuyer'"); 
	$count = $qs->RecordCount();if($count > 0 ) { echo '* ( '.$count.' )'; } ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png"><?php }?></span></a> </li>
    
    <?php
	 if(isset($idbuyer)){	// ------- Mise à jour de l'id gestion des Index  #1161
   	$SQL_Querys = " 
				SELECT  d.reference,
			 d.month_term,
				orw.quantity, 
				orw.idorder, 
				orw.summary, 
				orw.designation,
				orw.updated,
				o.idbuyer,
				b.company,
				c.firstname
					FROM 
					detail d,
					contact c,  
						buyer b, 
						`order_row` orw,
						`order` o
					
						
					WHERE 
					 d.month_term > 0
					  AND orw.reference =  d.reference
					AND o.idbuyer = '$idbuyer'
					AND o.idorder = orw.idorder
					AND o.idbuyer = b.idbuyer
					AND o.idbuyer = c.idbuyer
					AND c.idcontact = 0
			
					
				";
				
			$rsq = DBUtil::query($SQL_Querys);
		$total =0;
						for ($i = 0; $i < $rsq->RecordCount(); $i++) {
						 $references =  $rsq->Fields("reference");
						  $idorders =  $rsq->Fields("idorder");
		  $exist = 0;
						    $qrs = " 
				SELECT ot.idorder,
				ot.reference
			
				
					FROM 
					
					product_term ot
					
					 
					
				";
				
			$rsss = DBUtil::query($qrs);
						while( !$rsss->EOF()){
						$idorder_ots =  $rsss->Fields("idorder");
						$reference_ots =  $rsss->Fields("reference");
										if(($idorder_ots == $idorders) && ($reference_ots == $references ))
										$exist = 1;
										$rsss->MoveNext();
									}
									if($exist == 0) {
			$total=$total+1; 
		
		}
			
				$rsq->MoveNext();
			} 
			}
			 ?>
	
	
	
	
	
	  <?php if(isset($idbuyer)){ if($total > 0 ) {  ?><li class="item"><a href="#echeances"><span>Echéances à venir<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png"></span></a> </li><?php } }?>
	 <?php if(isset($idbuyer)) { $qs = DBUtil::query("SELECT * FROM product_term WHERE idbuyer = '$idbuyer'"); 
	$count = $qs->RecordCount();if($count > 0 ) {    ?><li class="item"><a href="#echeancier"><span>Echéancier<?php echo '('.$count.')'; ?></span></a> </li><?php } }?>
  	</ul>
			
						<a name="topform"></a>
				<!-- Formulaire -->
				<div class="blocSearch withTab"><div class="blocMarge">
                <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>

<!-- *********** -->
					<!-- echeancier -->
					<!-- *********** -->
					
					
					<!-- *********** -->
					<!-- Coordonnées -->
					<!-- *********** -->
					<div id="name-address">
					
					<?php if( !isset( $key ) ){ ?>
						<p><strong>Pour créer un <?php echo $type == "contact" ? "prospect" : "client" ?>, vous devez impérativement définir son profil et sa provenance.</strong></p><br/>
					<?php } ?>
					<div class="blocMultiple blocMRight">
						
						<label><strong><span>Suivie par</span></strong></label>
						<?php 
                          
                          //62. Gestion commercial unique pour la version open source
                         $contact_commercial_file = dirname( __FILE__ ) . "/../../akilae_pro/commercial.php";
                          
                         //Si fichier présent -> gestion multi-commerciaux, 
                         // si fichier absent -> commercial = contact_commercial 
                        if( file_exists($contact_commercial_file)){ 
                           
							include_once( dirname( __FILE__ ) . "/../../script/user_list.php" );
							userList( true, isset( $key ) ? $rsbuy->fields( "iduser" ) : User::getInstance()->getId() );
                        }
                        else{
                            
                            $iduser_commercial = DBUtil::getParameter( "contact_commercial" );
                            $selected_commercial   = isset( $key ) ? $rsbuy->fields( "iduser" ): $iduser_commercial;
                            
                            $query = " SELECT iduser, CONCAT( firstname, ' ', lastname ) AS innerHTML, hasbeen FROM user WHERE `available` IS TRUE AND iduser =$iduser_commercial ";
                            $rs_com =& DBUtil::query( $query );
                            ?>
                            	<select id="iduser" name="iduser">
                                	<option value="0">-</option>
                                    <?php if( $rs_com->RecordCount()> 0 ){
                                    		$selected_attrib = $rs_com->fields( "iduser" ) == $selected_commercial ? " selected=\"selected\"" : "";
                                    ?>
                                            <option<?php echo $selected_attrib; ?> value="<?php echo $rs_com->fields( "iduser" ); ?>"><?php echo htmlentities( $rs_com->fields( "innerHTML" ) ); ?></option>
                                    <?php }?>
                                </select>
                        <?php 
                        }
						?>
						<div class="spacer"></div>	
						
						<?php if( isset( $key ) ){ ?>
							<label><span>Vérifier l'email</span></label> 	
							<input type="text" name="EmailToCheck" class="mediumInput" />
							<input type="image" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/background-btn-verif.jpg" value="ok" name="CheckEmail" onClick="checkMail(); return false;" class="floatleft marginVerif" />
							<div class="spacer"></div>
						<?php } ?>

						<?php if( isset( $key ) ){ ?>
							<label><span>Vérifier le fax</span></label> 	
							<input type="text" name="FaxToCheck" class="mediumInput" />
							<input type="image" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/background-btn-verif.jpg" value="ok" name="CheckFax" onClick="checkFax(); return false;" class="floatleft marginVerif" />
						<?php } ?>
						
					
					</div> <!-- Fin blocMultiple -->
					
					
					<div class="blocMultiple blocMLeft">
						<label><span><strong>Profil</strong><strong class="asterix">*</strong></span></label>
						<?php XHTMLFactory::createSelectElement( "customer_profile", "idcustomer_profile", "name", "name", isset( $key ) ? $customer->get( "idcustomer_profile" ) : 0, "-1", "- Sélectionner -", " id=\"idcustomer_profile\" name=\"idcustomer_profile\" onchange=\"initRequiredElements();\" onkeypress=\"initRequiredElements();\" class=\"fullWidth\"" ); ?>
						<input type="hidden" name="catalog_right" value="<?php echo $key ? $customer->get( "catalog_right" ) : 0; ?>" />
						<div class="spacer"></div>
							
						<label><span><strong>Provenance</strong><strong class="asterix">*</strong></span></label>
						<?php DrawLift_6( "source", isset( $key ) ? $rsbuy->fields( "idsource" ) : "", "ORDER BY display_order ASC" ); ?>
						<div class="spacer"></div>
						
						<label><span><strong>Scoring</strong></span></label>
						<?php 
							if( isset( $key ) ){ DrawLift_scoring_buyer( $idscoring_buyer ); }
							else { DrawLift_scoring_buyer(); }
						?>
						<div class="spacer"></div>
					
					</div> <!-- Fin blocMultiple -->
					<div class="spacer"></div>

					<!--<input type="button" value="<?php echo $submit ?>" class="inputSave" onclick="if( checkRequiredElements( <?php echo $type == "contact" ? "1" : "0" ?> ) && checkEmailOrFax() ) document.getElementById('frm').submit();" />-->
						<input type="button" value="<?php echo $submit ?>" class="inputSave" onClick="if( checkRequiredElements( <?php echo $type == "contact" ? "1" : "0" ?> ) ) document.getElementById('frm').submit();" />
					
					<div class="spacer"></div>				
					
					
					<div class="blocMultiple blocMLeft">
						<h2>Client / Contact principal</h2>
						
						<div id="notPartTable1">
							<label><span><strong id="lbl_raison_soc">Raison sociale</strong><strong class="asterix">*</strong></span></label>
							<input type="text" name="Company" style="text-transform: uppercase;" id="Company" value="<?php if( isset( $key ) ){ echo Util::upperCase( $raison ); } ?>" onBlur="javascript:  if(document.getElementById('idcustomer_profile').value == 7){ getInfosCommunes(); } else { autoSearchOnManageo(); }" class="textInput fontBold" />
							<div class="spacer"></div>
							
							<div id="blocNC">
							<label><span><strong>Nom commercial</strong></span></label>
							<input type="text" name="comName" style="text-transform: uppercase;" id="comName" value="<?php if( isset( $key ) ){ echo Util::upperCase( $comName ); } ?>" class="textInput" />
							</div>
							<div class="spacer"></div>
						</div>
						<div class="spacer"></div>
						
						<div class="marginTop">
							<div id="function" style="display:inline;">
								<label><span>Fonction</span></label>								
								<?php 
									if( isset( $key ) ) { DrawLift_1( "function", $rscp->Fields( "idfunction" ) ); }
									else { DrawLift_1( "function" ); }									
								?>
								<div class="spacer"></div> 
							</div>
							<label><span>Titre<strong class="asterix">*</strong></span></label>
							<div class="simpleSelect"><?php  
								if( isset( $key ) ) { DrawLift_4( "title", $rscp->Fields( "title" ) ); }
								else { DrawLift_4( "title", "Pas de titre" ); }
							?></div>
							<div class="spacer"></div> 	
							
							
							<label><span>Nom<strong class="asterix">*</strong></span></label>
							<input type="text" id="buyerLastname" name="Lastname" value="<?php if( isset( $key ) ){ echo $nom ; } ?>" class="textidInput upperCase" />
							
							<label class="smallLabel"><span>Prénom</span></label>
							<input type="text" id="buyerFirstname" name="Firstname" value="<?php if( isset( $key ) ){ echo $pnom; } ?>" class="textidInput upperCase" />
							<div class="spacer"></div>
						</div>
						<div class="spacer"></div>
						
						<div class="marginTop">
							<label><span>Tél. direct<strong class="asterix">*</strong></span></label>
							<input type="text" name="Phonenumber" id="Phonenumber" value="<?php if( isset( $key ) ){ echo $tel; } ?>" class="textmoitieInput" onFocus="if( this.value == '<?php echo $phonenumber_default ?>' ){ this.value = ''; }" onBlur="if( this.value == '' && ( document.getElementById('gsm').value == '' || document.getElementById('gsm').value == '<?php echo $gsm_default ?>' ) ){ this.value = '<?php echo $phonenumber_default ?>'; document.getElementById('gsm').value = '<?php echo $gsm_default ?>'; } initRequiredElements();" />
							
							<label class="smallLabelMoitie"><span><em>ou</em> portable<strong class="asterix">*</strong></span></label>
							<input type="text" name="gsm" id="gsm" value="<?php if( isset( $key ) ){ echo $gsm; } ?>" class="textmoitieInput" onFocus="if( this.value == '<?php echo $gsm_default ?>' ){ this.value = '';}" onBlur="if( this.value == '' && ( document.getElementById('Phonenumber').value == '' || document.getElementById('Phonenumber').value == '<?php echo $phonenumber_default ?>' ) ){ this.value = '<?php echo $gsm_default ?>'; document.getElementById('Phonenumber').value = '<?php echo $phonenumber_default ?>'; } initRequiredElements();" />
							<div class="spacer"></div>	
							
							<label><span>E-mail<strong class="asterix">*</strong></span></label>
							<?php if( $usemail == 1 ){ ?><input type="text" name="Email" value="<?php echo $emailtocheck ?>" class="textInput" />
							<?php }else{ ?><input type="text" name="Email" id="Email" value="<?php if( isset( $key ) ){ echo $email; } ?>" class="textInput" onFocus="if( this.value == '<?php echo $email_default ?>' ){ this.value = '';}" onBlur="if( this.value == '' && ( document.getElementById('Faxnumber').value == '' || document.getElementById('Faxnumber').value == '<?php echo $faxnumber_default ?>' ) ){ this.value = '<?php echo $email_default ?>'; document.getElementById('Faxnumber').value = '<?php echo $faxnumber_default ?>'; } initRequiredElements();" /><?php } ?>
							<div class="spacer"></div>
							
							<label><span><em>ou</em> fax&nbsp;direct<strong class="asterix">*</strong></span></label>
							<input type="text" name="Faxnumber" id="Faxnumber" value="<?php if( isset( $key ) ){ echo $fax; } ?>" class="textmoitieInput" onFocus="if( this.value == '<?php echo $faxnumber_default ?>' ){ this.value = '';}" onBlur="if( this.value == '' && ( document.getElementById('Email').value == '' || document.getElementById('Email').value == '<?php echo $email_default ?>' ) ){ this.value = '<?php echo $faxnumber_default ?>'; document.getElementById('Email').value = '<?php echo $email_default ?>'; } initRequiredElements();" />
							<div class="spacer"></div>					
													
							
							
							
						</div>
						<div class="spacer"></div>
								
						<div class="marginTop">
							<label><span>Adresse<strong class="asterix">*</strong></span></label>
							<input type="text" name="Adress" id="Adress" value="<?php if( isset( $key ) ){ echo $adresse; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Complément adresse</span></label>
							<textarea name="Adress_2" id="Adress_2" class="textInput"><?php if( isset( $key ) ){ echo $adresse2; } ?></textarea>
							<div class="spacer"></div>
							
							<label><span>Code postal<strong class="asterix">*</strong></span></label>
							<input type="text" name="Zipcode" id="Zipcode" value="<?php if( isset( $key ) ){ echo $cp; } ?>" onKeyUp="if( this.value.length == 5 ) selectCity( this.value );" onBlur="javascript: autoSearchOnManageo();" class="numberInput" /></td>
							
							<label class="smallLabel"><span>Ville<strong class="asterix">*</strong></span></label>							
							<input type="text" name="City" id="City" value="<?php if( isset( $key ) ){ echo htmlentities( $ville ); } ?>" class="mediumInput upperCase" />
							<?php AutoCompletor::completeFromDB( "City", "communes", "commune" ); ?>
							<div class="spacer"></div>
							
							<label><span>Pays</span></label>
							<?php 
								$selectedValue = isset( $key ) ? $pays : DBUtil::getParameter( "default_idstate" );
								$lang = User::getInstance()->getLang();
								XHTMLFactory::createSelectElement( "state", "idstate", "name$lang", "name$lang", $selectedValue, false, false, "name=\"idstate\" id=\"idstate\" onchange=\"initRequiredElements();\" onkeyup=\"initRequiredElements();\" " ); 
							?>
                            
                            <label><span>Langue du client</span></label>
                            
                            <?php
							$qr = DBUtil::query("SELECT * FROM language");
							if (isset($idbuyer))
							$idlanguage = DBUtil::getDBValue( "id_lang", "buyer", "idbuyer", $idbuyer);
							?>
                            
                            <select name="language_buyer" id="language_buyer">
                            <?php
							while(!$qr->EOF())
							{
							?>
                            <?php 
							if ($idlanguage == $qr->fields("id_lang"))
							{						
							?>
                            
							<option value="<?php echo $qr->fields("id_lang"); ?>" selected ><?php echo $qr->fields("name"); ?></option>
							<?php
							}else{
							?>
							<option value="<?php echo $qr->fields("id_lang"); ?>" ><?php echo $qr->fields("name"); ?></option>
							
							<?php
							}
							$qr->MoveNext();
							}
							
							?>
                            
                            </select>                            
                            
							
							
							<div class="spacer"></div>
						</div>
						<div class="spacer"></div>
						
					</div> <!-- Fin blocMultiple -->
					
					<div class="blocMultiple blocMRight" id="notPartTable5">
						<h2>Informations générales</h2>
						<div id="notMairie">
							<label><span><strong>APE/NAF</strong></span></label>
							<input type="text" name="NafText" id="NafText" value="<?php if( isset( $key ) ){ echo DBUtil::getDBValue( "naf_comment", "naf", "idnaf", str_replace(" ", "",$ape) ); } ?>" class="textInput" />
							<?php echo AutoCompletor::completeFromFile( "NafText", "$GLOBAL_START_URL/sales_force/naf_autocomplete.php", 10, "changeNAFText" ) ?>
							<div class="spacer"></div>
							
							<label><span>NAF n°<strong class="asterix">*</strong></span></label>
							<input type="text" name="Naf" id="Naf" value="<?php if( isset( $key ) ){ echo $ape; } ?>" class="textidInput" onKeyPress="changeNAFNumber( this );" />
							
							<label class="smallLabel"><span>TVA intr.</span></label>
							<input type="text" name="Tva" value="<?php if( isset( $key ) ){ echo $tva; } ?>" class="textidInput" />
							<div class="spacer"></div>
						</div>
						<label><span>Siren<strong class="asterix">*</strong></span></label>
						<input type="text" name="siren" id="siren" value="<?php if( isset( $key ) ) echo $siren; ?>" class="textidInput" onKeyUp="this.value = this.value.replace( new RegExp( '[^0-9]*', 'g' ), '' ); $('#sirenOfCompany').val( $(this).val() );" onBlur="this.onkeyup(event); autoSearchOnManageo();" />
						
						<label class="smallLabel"><span>Siret</span></label>
						<input type="text" name="siret" id="siret" value="<?php if( isset( $key ) ) echo $siret; ?>" onKeyUp="this.value = this.value.replace( new RegExp( '[^0-9]*', 'g' ), '' ); if( $( this ).val().length ) $('#siren').val( $( this ).val().substring( 0, 9 ) );" onBlur="this.onkeyup(event);" class="textidInput" />
						<div class="spacer"></div>		
						
											
						<div class="marginTop" id="infosFacultativesMairie">
							<label><span>Capital</span></label>
							<input type="text" name="companyCapital" id="companyCapital" value="<?php if( isset( $key ) ){ echo $capital; } ?>" class="textWithSign" />
							<span class="textSigne">&nbsp;&euro;</span>
							
							<label class="smallLabel"><span>Dernier exercice</span></label>
							<input type="text" name="last_exo" id="last_exo" value="<?php if( isset( $key ) ){ echo $last_exo; } ?>" class="textidInput"/>
							<div class="spacer"></div>	
							
						
							<label><span>Date de création</span></label>
							<input type="text" name="creationDate" id="creationDate" value="<?php if( isset( $key ) ){ echo $creation_date; } ?>" class="textidInput" />
							
							<label class="smallLabel"><span>CA</span></label>
							<input type="text" name="ca_from_web" id="ca_from_web" value="<?php if( isset( $key ) ){ echo Util::numberFormat( $ca_company, 0 ); } ?>" class="textWithSign"  /> 
							<span class="textSigne">&nbsp;&euro;</span>
							<div class="spacer"></div>	
							

							
							<label><span>Rentabilité financière</span></label>
							<input type="text" name="renta_finan" id="renta_finan" value="<?php if( isset( $key ) ){ echo $renta_finan; } ?>" class="textWithSign" /> 
							<span class="textSigne">&nbsp;%</span>
							
							<label class="smallLabel"><span>Résultat</span></label>
							<input type="text" name="companyResult" id="companyResult" value="<?php if( isset( $key ) ){ echo $resultat; } ?>" class="textWithSign" />
							<span class="textSigne">&nbsp;&euro;</span>
							<div class="spacer"></div>	
							
							
							<label><span>Nb&nbsp;d'établissement</span></label>
							<input type="text" name="nbr_etab" id="nbr_etab" maxlength="3" value="<?php if( isset( $key ) ){ echo $nbr_etab; } ?>" class="textidInput" />
								
							<label class="smallLabel"><span>Effectif</span></label>
							<div class="simpleSelect">
								<?php if( isset( $key ) ) { DrawLift_5( $rsbuy->Fields( "idmanpower" ) ); }
								else { DrawLift_5(); } ?>
							</div>
							<div class="spacer"></div>
							<!-- <label><span>Domaine activité</span></label>
							<?php if( !isset( $key ) ) { DrawLift_3( "activity", "", "", " style=\"\"" ); }
							else { DrawLift_3( "activity", $rsbuy->fields( "idactivity" ), "", " style=\"\"" ); } ?>
							<div class="spacer"></div> -->	
							
							
							
						</div>
						<!-- infosPlusMairie : Affiché uniquement dans le cas d'une MAIRIE -->					
						<div class="marginTop" id="infosPlusMairie" style="display:none;">
							<label><span>Nom du maire</span></label>
							<input type="text" name="nom_Maire" id="nom_Maire" value="<?php if( isset( $key ) && $customer->get( "idcustomer_profile" ) == 7 ){ echo $nomMaire; } ?>" class="textidInput" />
							
							<label class="smallLabel"><span>Prénom</span></label>
							<input type="text" name="prenom_Maire" id="prenom_Maire" value="<?php if( isset( $key ) && $customer->get( "idcustomer_profile" ) == 7 ){ echo $prenomMaire; } ?>" class="textidInput" />
							
							<div class="spacer"></div>
							
							<label><span>Région</span></label>
							<input type="text" name="mairie_region" id="mairie_region" value="<?php if( isset( $key ) && $customer->get( "idcustomer_profile" ) == 7 ){ echo $regionMairie; } ?>" class="textidInput" />
							
							<label class="smallLabel"><span>Dpt.</span></label>
							<input type="text" name="mairie_dpt" id="mairie_dpt" value="<?php if( isset( $key ) && $customer->get( "idcustomer_profile" ) == 7 ){ echo $deptMairie; } ?>" class="textidInput" />
							<div class="spacer"></div>
							
							<label><span>Nbr habitants</span></label>
							<input type="text" name="nbr_habitant" id="nbr_habitant" value="<?php if( isset( $key ) && $customer->get( "idcustomer_profile" ) == 7 ){ echo $nbrHabitant; } ?>" class="textidInput" />
							
							<label class="smallLabel"><span>n° Insee</span></label>
							<input type="text" name="insee_code" id="insee_code" value="<?php if( isset( $key ) && $customer->get( "idcustomer_profile" ) == 7 ){ echo $inseeMairie; } ?>" class="textidInput" />
							<div class="spacer"></div>
						</div>
						
						
						<div class="spacer"></div>
						
						<div class="marginTop">
							<label><span>Tél.</span></label>
							<input type="text" name="phone_standard" id="phone_standard" value="<?php if( isset( $key ) ){ echo $telstandard; } ?>" class="textidInput" />
							
							<label class="smallLabel"><span>Fax</span></label>
							<input type="text" name="fax_standard" id="fax_standard" value="<?php if( isset( $key ) ){ echo $faxstandard; } ?>" class="textidInput" />
							<div class="spacer"></div>
							
							<label><span>E-mail</span></label>
							<input type="text" name="company_mail" value="<?php if( isset( $key ) ){ echo $company_mail; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Site web</span></label>
							<?php if( isset( $key ) && !empty( $www ) && $www != "http://" ){ ?>
							<a href="<?php echo $www ?>" onClick="window.open(this.href); return false;">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" class="floatright" />
							</a><?php } ?>
							<input type="text" name="www" id="www" value="<?php if( isset( $key ) ){ echo $www; }else{ echo "http://"; } ?>" class="textInput" />
							<div class="spacer"></div>
						</div>
						<div class="spacer"></div>	
						
						<h2>Commentaire Société</h2>
						<label><span>Commentaire<br/>&nbsp;</span></label>
						<textarea name="comment" style="height:60px;" class="textInput"><?php if( isset( $key ) ){ echo $commentaire; } ?></textarea>
						<div class="spacer"></div>	
					
					</div> <!-- Fin blocMultiple -->
				<div class="blocMultiple blocMLeft" style="width:100%; margin:0;">	
					<h2>Spécifications</h2>
					<?php if (DBUtil::getParameterAdmin('display_buyer_info1')) { ?>
					<div class="blocMultiple blocMLeft">
					 	<label id="txtSearchCompany"><span><?php echo Dictionnary::translate("buyer_info1");?></span></label>					 	
					 	<input type="text" name="Buyer_info1" id="Buyer_info1" value="<?php if( isset( $key ) ){ echo htmlentities( $buyer_info1 ); } ?>" class="mediumInput upperCase" />
						<div class="spacer"></div>				
					</div>
					<?php } ?>
					<?php if (DBUtil::getParameterAdmin('display_buyer_info2')) { ?>
					<div class="blocMultiple blocMRight">
					 	<label id="txtSearchCompany"><span><?php echo Dictionnary::translate("buyer_info2");?></span></label>					 	
					 	<input type="text" name="Buyer_info2" id="Buyer_info2" value="<?php if( isset( $key ) ){ echo htmlentities( $buyer_info2 ); } ?>" class="mediumInput upperCase" />
						<div class="spacer"></div>	
					</div>	
					<?php } ?>
					<div class="spacer"></div>		
					<?php if (DBUtil::getParameterAdmin('display_buyer_info3')) { ?>
					<div class="blocMultiple blocMLeft">
					 	<label id="txtSearchCompany"><span><?php echo Dictionnary::translate("buyer_info3");?></span></label>					 	
					 	<input type="text" name="Buyer_info3" id="Buyer_info3" value="<?php if( isset( $key ) ){ echo htmlentities( $buyer_info3 ); } ?>" class="mediumInput upperCase" />
						<div class="spacer"></div>				
					</div>
					<?php } ?>
					<?php if (DBUtil::getParameterAdmin('display_buyer_info4')) { ?>
					<div class="blocMultiple blocMRight">
					 	<label id="txtSearchCompany"><span><?php echo Dictionnary::translate("buyer_info4");?></span></label>					 	
					 	<input type="text" name="Buyer_info4" id="Buyer_info4" value="<?php if( isset( $key ) ){ echo htmlentities( $buyer_info4 ); } ?>" class="mediumInput upperCase" />
						<div class="spacer"></div>
					</div>
					<?php } ?>
					<div class="spacer"></div>		
					<?php if (DBUtil::getParameterAdmin('display_buyer_info5')) { ?>
					<div class="blocMultiple blocMLeft">
										 	<label id="txtSearchCompany"><span><?php echo Dictionnary::translate("buyer_info5");?></span></label>					 	
					 	<input type="text" name="Buyer_info5" id="Buyer_info5" value="<?php if( isset( $key ) ){ echo htmlentities( $buyer_info5 ); } ?>" class="mediumInput upperCase" />
						<div class="spacer"></div>				
					</div>
					<?php } ?>
					<?php if (DBUtil::getParameterAdmin('display_buyer_info6')) { ?>
					<div class="blocMultiple blocMRight">
					 	<label id="txtSearchCompany"><span><?php echo Dictionnary::translate("buyer_info6");?></span></label>					 	
					 	<input type="text" name="Buyer_info6" id="Buyer_info6" value="<?php if( isset( $key ) ){ echo htmlentities( $buyer_info6 ); } ?>" class="mediumInput upperCase" />
						<div class="spacer"></div>	
								
					</div>
					<?php } ?>
					</div>
					</div>

					<div id="notPartTable7"></div> 
					<div id="notPartTable6"></div> 		
					<div id="notPartTable4"></div> 	
					<div id="notPartTable2"></div> 
						
					<div class="spacer" id="spacer1"></div>
					<div class="spacer" id="spacer2"></div>
					
					<div class="blocMultiple blocMLeft">
						<div class="spacer"></div>	
						<div class="spacer"></div>							
					</div> <!-- Fin blocMultiple -->
					<div class="spacer"></div>	
	

					<input type="submit" name="Modify" class="inputSave" value="<?php echo $submit ?>" />
					<div class="spacer"></div>	
					
					</div> <!-- Fin #name-address -->
					
					<!-- *********** -->
					<!--  address  -->
					<!-- *********** -->
					<div id="address" class="displayNone">
                    
                    <div class="blocMultiple blocMLeft" id="livraisonBloc">		
							<h2>Adresse de livraison</h2>	
							
							<?php if( isset($billingDel) && $billingDel == true ){ ?>
								<label><span>Charger une adresse</span></label>
								<select name="loadBillingDel" onChange="javascript: loadBillingDell( this.value ); ">
									<option value="">-</option>
									<?php
									while( !$rsBillingDel->EOF()){
										$idBillingDell = $rsBillingDel->fields('iddelivery') ;
										$option = $rsBillingDel->fields('lastname');													
									?>
										<option value="<?php echo $idBillingDell ?>" <?php if ($rsDelDisplay->fields('lastname')==$rsBillingDel->fields('lastname')) {echo " selected=\"selected\"";} ?>   ><?php echo $option ?></option>
									<?php
										$rsBillingDel->MoveNext();
									}
									?>
								</select>
							<?php }else{ ?>
								<div class="textInfo">Aucune adresse de livraison spécifique n'a été definie.</div>
							<?php } ?>
							<div class="spacer"></div>
							
							<?php
                            if( isset($DelDisplay) && $DelDisplay == true )
                            {
                            ?>
                            
                            <div id="loadZone1">
							<label><span>Raison sociale</span></label>
							<input type="text" name="companyDel" id="companyDel" value="<?php echo $rsDelDisplay->fields('company'); ?>" onKeyUp="this.value=this.value.toUpperCase();" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Titre</span></label>
							<select name="titleDel" id="titleDel">
								<option value="1" <?php if ($rsDelDisplay->fields('title')==1) {echo " selected=\"selected\"";} ?> >Mr</option>
								<option value="2" <?php if ($rsDelDisplay->fields('title')==2) {echo " selected=\"selected\"";} ?>>Mme</option>
								<option value="3" <?php if ($rsDelDisplay->fields('title')==3) {echo " selected=\"selected\"";} ?>>Mlle</option>
							</select>
							<div class="spacer"></div>
							
							<label><span>Nom</span></label>
							<input type="text" name="nomDel" id="nomDel" value="<?php echo $rsDelDisplay->fields('lastname'); ?>" class="textidInput upperCase" />
							
							<label class="smallLabel"><span>Prénom</span></label>
							<input type="text" name="prenomDel" id="prenomDel" value="<?php echo $rsDelDisplay->fields('firstname'); ?>" class="textidInput upperCase" />
							<div class="spacer"></div>
							
							<label><span>Tél.</span></label>
							<input type="text" name="telDel" id="telDel" value="<?php echo $rsDelDisplay->fields('phonenumber'); ?>" class="textidInput" />
							
							<label class="smallLabel"><span>Fax</span></label>
							<input type="text" name="faxDel" id="faxDel" value="<?php echo $rsDelDisplay->fields('faxnumber'); ?>" class="textidInput" />
							<div class="spacer"></div>
							
							<label><span>Adresse</span></label>
							<input type="text" name="adrDel" id="adrDel" value="<?php echo $rsDelDisplay->fields('adress'); ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Complément adresse</span></label>
							<textarea name="moreAdrDel" id="moreAdrDel" class="textInput"><?php echo $rsDelDisplay->fields('adress_2'); ?></textarea>
							<div class="spacer"></div>
							
							<label><span>Code postal</span></label>
							<input type="text" name="cpDel" id="cpDel" value="<?php echo $rsDelDisplay->fields('zipcode'); ?>" class="numberInput" maxlenght="5" />
							
							<label class="smallLabel"><span>Ville</span></label>
							<input type="text" name="cityDel" id="cityDel" value="<?php echo $rsDelDisplay->fields('city'); ?>" onKeyUp="javascript: this.value=this.value.toUpperCase();" class="mediumInput"/>
							<div class="spacer"></div>
							</div>	
                            
                           <?php 
                            }else{
                           ?> 
                            <div id="loadZone1">
							<label><span>Raison sociale</span></label>
							<input type="text" name="companyDel" id="companyDel" value="<?php if( isset( $key ) ){ echo ""; } ?>" onKeyUp="this.value=this.value.toUpperCase();" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Titre</span></label>
							<select name="titleDel" id="titleDel">
								<option value="1" >Mr</option>
								<option value="2">Mme</option>
								<option value="3">Mlle</option>
							</select>
							<div class="spacer"></div>
							
							<label><span>Nom</span></label>
							<input type="text" name="nomDel" id="nomDel" value="" class="textidInput upperCase" />
							
							<label class="smallLabel"><span>Prénom</span></label>
							<input type="text" name="prenomDel" id="prenomDel" value="" class="textidInput upperCase" />
							<div class="spacer"></div>
							
							<label><span>Tél.</span></label>
							<input type="text" name="telDel" id="telDel" value="" class="textidInput" />
							
							<label class="smallLabel"><span>Fax</span></label>
							<input type="text" name="faxDel" id="faxDel" value="" class="textidInput" />
							<div class="spacer"></div>
							
							<label><span>Adresse</span></label>
							<input type="text" name="adrDel" id="adrDel" value="<?php if( isset( $key ) ){ echo ""; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Complément adresse</span></label>
							<textarea name="moreAdrDel" id="moreAdrDel" class="textInput"><?php if( isset( $key ) ){ echo ""; } ?></textarea>
							<div class="spacer"></div>
							
							<label><span>Code postal</span></label>
							<input type="text" name="cpDel" id="cpDel" value="<?php if( isset( $key ) ){ echo ""; } ?>" class="numberInput" maxlenght="5" />
							
							<label class="smallLabel"><span>Ville</span></label>
							<input type="text" name="cityDel" id="cityDel" value="<?php if( isset( $key ) ){ echo ""; } ?>" onKeyUp="javascript: this.value=this.value.toUpperCase();" class="mediumInput"/>
							<div class="spacer"></div>
							</div>	
                            <?php } ?>

						<div class="spacer"></div>	
                            
                            		
						</div>
                    
                        
                        <div class="blocMultiple blocMLeft" id="facturationBloc">		
							<h2>Adresse de facturation</h2>	
							
							<?php if( isset($billingAdr) && $billingAdr == true ){ ?>
								<label><span>Charger une adresse</span></label>
								<select name="loadBillingAdr" onChange="javascript: loadBillingAdrr( this.value ); ">
									<option value="">-</option>
									<?php
									while( !$rsBillingAdr->EOF()){
										$idBillingAddr = $rsBillingAdr->fields('idbilling_adress') ;
										$option = $rsBillingAdr->fields('firstname').' '.$rsBillingAdr->fields('lastname').' - '.$rsBillingAdr->fields('zipcode').' '.$rsBillingAdr->fields('city') ;													
									?>
										<option value="<?php echo $idBillingAddr ?>"><?php echo $option ?></option>
									<?php
										$rsBillingAdr->MoveNext();
									}
									?>
								</select>
							<?php }else{ ?>
								<div class="textInfo">Aucune adresse de facturation spécifique n'a été definie.</div>
							<?php } ?>
							<div class="spacer"></div>
							
							<div id="loadZone">
							<label><span>Raison sociale</span></label>
							<input type="text" name="companyBilling" id="companyBilling" value="<?php if( isset( $key ) ){ echo ""; } ?>" onKeyUp="this.value=this.value.toUpperCase();" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Titre</span></label>
							<select name="titleBilling" id="titleBilling">
								<option value="1" >Mr</option>
								<option value="2">Mme</option>
								<option value="3">Mlle</option>
							</select>
							<div class="spacer"></div>
							
							<label><span>Nom</span></label>
							<input type="text" name="nomBilling" id="nomBilling" value="" onBlur="javascript: document.getElementById('accountant').value = this.value ;" class="textidInput upperCase" />
							
							<label class="smallLabel"><span>Prénom</span></label>
							<input type="text" name="prenomBilling" id="prenomBilling" value="" onBlur="javascript: document.getElementById('accountantLastName').value = this.value ;" class="textidInput upperCase" />
							<div class="spacer"></div>
							
							<label><span>Tél.</span></label>
							<input type="text" name="telBilling" id="telBilling" value="" class="textidInput" />
							
							<label class="smallLabel"><span>Fax</span></label>
							<input type="text" name="faxBilling" id="faxBilling" value="" class="textidInput" />
							<div class="spacer"></div>
							
							<label><span>Adresse</span></label>
							<input type="text" name="adrBilling" id="adrBilling" value="<?php if( isset( $key ) ){ echo ""; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Complément adresse</span></label>
							<textarea name="moreAdrBilling" id="moreAdrBilling" class="textInput"><?php if( isset( $key ) ){ echo ""; } ?></textarea>
							<div class="spacer"></div>
							
							<label><span>Code postal</span></label>
							<input type="text" name="cpBilling" id="cpBilling" value="<?php if( isset( $key ) ){ echo ""; } ?>" class="numberInput" maxlenght="5" />
							
							<label class="smallLabel"><span>Ville</span></label>
							<input type="text" name="cityBilling" id="cityBilling" value="<?php if( isset( $key ) ){ echo ""; } ?>" onKeyUp="javascript: this.value=this.value.toUpperCase();" class="mediumInput"/>
							<div class="spacer"></div>
							</div>	
                            

						<div class="spacer"></div>	
                            
                            		
						</div> <!-- Fin blocMultiple -->			
		
						<div class="spacer"></div><br/>
			                            <input type="submit" name="Modifiy" class="inputSave" value="<?php echo $submit ?>" />	

					
					</div> <!-- Fin #address -->
					<!-- *********** -->
					<!--    suivi    -->
					<!-- *********** --> 
					<?php
					 include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
						
	
			 
							 if( isset($billingc) && $billingc == true ){ 
							   $idcontact =   $rsBillingcont->fields('idcontact'); 
$idbuyer =   $rsBillingcont->fields('idbuyer'); 
$iduser =   $rsuser->fields('iduser');
}

?>



<?php if( isset( $idbuyer ) ){ ?><input type="hidden" name="idbuyer" value="<?php echo $idbuyer  ?>" /><?php } ?>
				
                                <div id="suivi" class="displayNone">
					
						<div class="blocMultiple blocMLeft" id="facturationBloc" style="width:99%">		
							<h2>CRM</h2>
                           
							<?php if( isset($billingc) && $billingc == true ){ ?>
								<label><span>Charger un contact</span></label> 
                                <input type="hidden" name="idbuyer" id="idbuyer" value="<?php echo $idbuyer  ?>" />
                               
								<select name="loadBillingcont" id="loadBillingcont" onChange="javascript: loadBillingcontt( this.value, document.getElementById('idbuyer').value ) ">
                             
									<option value="-1" >Tous les contacts</option>
									
									<?php
									 
									while( !$rsBillingcont->EOF()){
										$idBillingcontact = $rsBillingcont->fields('idcontact') ;
										$option = $rsBillingcont->fields('title_1').' '.$rsBillingcont->fields('firstname').' '.$rsBillingcont->fields('lastname');													
									?>
										<option value="<?php echo $idBillingcontact ?>"><?php echo $option ?></option>
									<?php
										$rsBillingcont->MoveNext();
									}
									?>
								</select>
                                <body onLoad="javascript: if(document.getElementById('loadBillingcont').value == -1)loadBillingcontt( document.getElementById('loadBillingcont').value, document.getElementById('idbuyer').value )">
							<?php }else{ ?>
								<div class="textInfo">Aucun contact n'a été definie.</div>
								
							<?php } 
							
							?>
                            <div id="msge" style="visibility:hidden;color:#CC0000" class="textInfo" >Vous devez sélectionné un contact</div>
							<div class="spacer"></div>
                           
                           <div id="loadZones" style="visibility:hidden">
                           <div class="blocMultiple blocMLeft" style="width:50%">
                           <input type="hidden" name="idcontact" id="idcontact" value="" />
                            <input type="hidden" name="idcommercial" id="idcommercial" value="" />
                             <input type="hidden" name="username" id="username" value="" />
                              <input type="hidden" name="idcontact_cause" id="idcontact_cause" value="" />
                                <input type="hidden" name="idsupplier" id="idsupplier" value="" />
                              <?php    if(isset ($_REQUEST[ "idcommercial" ])  ){ ?>
                             <script> 
							/* var sel = document.getElementById('commercial');
    var val = document.getElementById('idcommercial').value;
    for(var i = 0, j = sel.options.length; i < j; ++i) {
        if(sel.options[i].innerHTML === val) {
           sel.selectedIndex = i;
           break;
        }
    }*/

							
						//	 document.getElementById("commercial").value = document.getElementById("idcommercial").value </script>
                             <?php } 
							
							?>
							<input type="hidden" name="idfollow" id="idfollow" value="" />
                           <input type="hidden" name="fin" id="fin" value="" />
                           	<?php if( isset($userc) && $userc == true ){ ?>
                           <label><span>Commercial</span></label>
							<select name="commercial" id="commercial" >
                             
									<option value="-1" >Commercial</option>
									
									<?php
									 
									while( !$rscommercial->EOF()){
										$idusers = $rscommercial->fields('iduser') ;
										$options = $rscommercial->fields('title_1').' '.$rscommercial->fields('firstname').' '.$rscommercial->fields('lastname');													
									?>
										<option value="<?php echo $idusers ?>" <?php echo $idusers == $_request[ "idcommercial" ] ? 'select="selected"' : ''; ?>><?php echo $options ?></option>
									<?php
										$rscommercial->MoveNext();
									}
									
									?>
								</select>
                                	<?php } 
							
							?>
                              
                            <?php if( isset($usercc) && $usercc == true ){ ?>
                           <label><span>Sujet</span></label>
							<select name="cause" id="cause" >
                             
									<option value="-1" >Sujet</option>
									
									<?php
									 
									while( !$rscause->EOF()){
										$idcontact_cause = $rscause->fields('idcontact_cause') ;
										$cause = $rscause->fields('cause_1');													
									?>
										<option value="<?php echo $idcontact_cause ?>" <?php if($idcontact_cause == $_request[ "idcontact_cause" ])  echo 'select="selected"'; ?>><?php echo $cause ?></option>
									<?php
										$rscause->MoveNext();
									}
									
									?>
								</select>
                                	<?php } 
							
							?>
                           <div class="spacer"></div>
                          
								<?php if( isset($users) && $users == true ){ ?>
                           <label><span>Fournisseur</span></label>
							<select name="supplier" id="supplier" >
                             
									<option value="-1" >Fournisseur</option>
									
									<?php
									 
									while( !$rssupplier->EOF()){
										$idsupplier = $rssupplier->fields('idsupplier') ;
										$name = $rssupplier->fields('name');													
									?>
										<option value="<?php echo $idsupplier ?>" if($idsupplier == $_request[ "idsupplier" ])select="selected"><?php echo $name ?></option>
									<?php
										$rssupplier->MoveNext();
									}
									
									?>
								</select>
                                	<?php } 
							
							?>
							</div>
							
							<div class="blocMultiple blocMRight">
                           
                            
                            <label><span>Type</span></label>
							<select name="idtype_follow" id="idtype_follow" >
                             
									<option value="-1" ></option>
									
									<?php
									 
									while( !$rstype_follow->EOF()){
										$idtype_follow	 = $rstype_follow->fields('idtype_follow') ;
										$type_follow_1	 = $rstype_follow->fields('type_follow_1');													
									?>
										<option value="<?php echo $idtype_follow ?>" <?php if($idtype_follow == $_request[ "idtype_follow" ])  echo 'select="selected"'; ?>><?php echo $type_follow_1	 ?></option>
									<?php
										$rstype_follow->MoveNext();
									}
									
									?>
								</select>
                            <div class="spacer"></div>
                            
							<label><span>Date prochaine relance</span></label>
							<?php $date = isset( $_POST[ "rel_end" ] ) ? $_POST[ "rel_end" ] :  date("d-m-Y"); ?><input type="text" name="rel_end" id="rel_end" class="calendarInput" value="" /> <?php echo DHTMLCalendar::calendar( "rel_end" ) ?>
							<div class="spacer"></div>
							
							<label><span>Date envoi catalogue</span></label>
							<?php $dates = isset( $_POST[ "rel_send" ] ) ? $_POST[ "rel_send" ] : date("d-m-Y"); ?><input type="text" name="rel_send" id="rel_send" class="calendarInput" value="" /> <?php echo DHTMLCalendar::calendar( "rel_send" ) ?>
							<div class="spacer"></div>
                            
                            												
							</div>
							<div class="spacer"></div>
							
						
							<label><span>Commentaire</span></label>
							<textarea name="commentaire" id="commentaire" class="textInput" style="height:75px;width:600px"></textarea>
<script>
//Vider la valeur du textarea
document.getElementById('commentaire').value='' ;
</script>
	
		
							
							
                        <input type="submit" name="Modify-suivi" class="inputSave" value="Enregistrer" />
                        <br><br>
                         <input type="button" value="Nouveau" name="ajouter" class="inputSave" onClick="javascript: resetsuivi(document.getElementById('loadBillingcont').value)"/>
                           <br><br>
                              <input type="hidden" value="Réaliser" name="realiser" id="realiser" class="inputSave" onClick="javascript: realisercomment(document.getElementById('idfollow').value)"/>
                               <br><br>
							</div>
                             <div id="realisercomments" style="width:inherit; height:auto">
                          
                               </div><br><br>	
                             <div id="deletecomments" style="width:inherit; height:auto">
                          
                               </div><br><br>
                            
                            	   <?php    if(isset ($_REQUEST[ "idcontact" ]) && ($idbuyer) ){ ?>
                             <script> document.getElementById('loadZones').style.visibility = "visible"; </script>
                               <?php  } ?>
                             <div id="loadcomments" style="width:inherit; height:auto">
                          
                               </div><br><br>
                          
               <div id="table" style="visibility:visible">              
       <?php                     
                            
                            
                           if(isset ($_REQUEST[ "idcontact" ]) && ( $_POST[ "idcontact" ]!="" ) && $idbuyer ){ 
						   
                           
$key = $idbuyer;
$contact = $_REQUEST[ "idcontact" ];
	// ------- Mise à jour de l'id gestion des Index  #1161
$q = "SELECT cf.*, u.initial
FROM contact_follow cf
	LEFT JOIN user u  ON cf.iduser = u.iduser  
    LEFT JOIN type_follow t  ON cf.idtype_follow = t.idtype_follow     
		
 WHERE cf.idbuyer = '$key' and cf.idcontact = $contact 
 ORDER BY cf.date_relaunch ASC"; 
	//$q = "SELECT lastname,firstname,title_1 from contact,title WHERE contact.idbuyer = 10012 AND contact.idcontact = $keys AND contact.title = title.idtitle "; 
	$db	= DBUtil::getConnection();
	$rsq = $db->Execute($q);
	//$rs = $db->Execute($q);
	 include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	 ?>

fsdfsdfsdf
	<table class="dataTable resultTable">
					<thead>
						<tr>
                        <th>Initial</th>
                         <th>Fournisseur</th>
                          <th>Nom utilisateur</th>
                           <th>Service</th>
							<th>Commentaire</th>
				<th>Date création</th>
				<th>Date prochaine relance</th>
				<th>Date envoi catalogue</th>
                <th>Type</th>
                <th>Réalisé</th>
							<th>Editer</th>
                            <th>Supprimer</th>
						</tr>
                        </thead>
						
                       
                        	<?php
	 while( !$rsq->EOF()){
	
 $comment = nl2br($rsq->fields('comment'));
 
$date_creation =  substr($rsq->Fields("date_creation"), 0, 10);
  if($date_creation == '0000-00-00'){
	$date_creation 	 = '00-00-0000';
	}
	else
	{
	$date_creation =  date("d-m-Y", strtotime($date_creation));
	}

 $date_relance =  substr($rsq->Fields("date_relaunch"), 0, 10);
   if($date_relance == '0000-00-00'){
	$date_relance 	 = '00-00-0000';
	}
	else
	{
	$date_relance =  date("d-m-Y", strtotime($date_relance));
	}
	
  $date_envoi =  substr($rsq->Fields("date_send"), 0, 10);
    if($date_envoi == '0000-00-00'){
	$date_envoi 	 = '00-00-0000';
	}
	else
	{
	$date_envoi =  date("d-m-Y", strtotime($date_envoi));
	}
	
  $date_new =  substr($rsq->Fields("date_new"), 0, 10);
    if($date_new == '0000-00-00'){
	$date_new 	 = '00-00-0000';
	}
	else
	{
	$date_new =  date("d-m-Y", strtotime($date_new));
	}
  $realiser = $rsq->Fields("realized") ;
  $rating = $rsq->Fields("rating_priority") ;
  $initial = $rsq->Fields("initial") ;
   $idcontact_cause = $rsq->Fields("idcontact_cause") ;
    $idsupplier = $rsq->Fields("idsupplier") ;
	// $cause_1 = $rsq->Fields("cause_1") ;
  ////  $name = $rsq->Fields("name") ;
  	$q = "SELECT cause_1
			FROM   contact_cause
			WHERE idcontact_cause = $idcontact_cause
			
		
			";
	$rsu=& DBUtil::query( $q );
	
				
				$cause_1 = Util::doNothing($rsu->fields("cause_1"));
				$cause_1 =  htmlentities($cause_1, ENT_QUOTES, "UTF-8");
					$q = "SELECT name
			FROM   supplier
			WHERE idsupplier = $idsupplier
			
		
			";
	$rssu=& DBUtil::query( $q );
	
				
				$name = Util::doNothing($rssu->fields("name"));
				$name =  htmlentities($name, ENT_QUOTES, "UTF-8");
	 ?>
 <tbody>
  <tr class="blackText">
  

                 <td ><?php echo $initial  ?></td>
                  <td ><?php echo $name ?></td>
                  <td ><?php echo $rsq->fields('username') ?></td>
                  
                 <td ><?php echo $cause_1 ?></td>
                 <td valign="left" style="text-align: left;"><?php echo $comment  ?></td>
				 <td><?php echo  $date_creation ?><?php echo DHTMLCalendar::calendar( "rel_start" ) ?></td>
                 
                 
                                 <td>
                 
                 
                 
                 <table width="100%">
				
				<tr>
				<td style="border: 0px">
				
				
				
				
				
				<?php
				
				for ($i=0; $i < 3; $i++)
				{
				if ($i <= ($rating - 1 ) )
				{
				?>
				<a href="#" onClick="javascript: rating_function(<?php echo $rsq->fields('idcontact_follow') ?>,<?php echo ($i + 1) ?>)">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rating_on.gif" ></a>
				<?php
				}
				else
				{
				?>
				<a href="#" onClick="javascript: rating_function(<?php echo $rsq->fields('idcontact_follow') ?>,<?php echo ($i + 1) ?>)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rating_off.gif" onMouseOver="this.src='<?=$GLOBAL_START_URL ?>/images/back_office/content/rating_on.gif'" onMouseOut="this.src='<?=$GLOBAL_START_URL ?>/images/back_office/content/rating_off.gif'" /></a>
				
				<?php
				}
				?>
				<?php
				}
				
				?>
				
				
				
				
	
				
				</td>
				</tr>
				
				
				
				<tr>
				<td>
				<?php echo  $date_relance ?><?php echo DHTMLCalendar::calendar( "rel_end" ) ?>
				</td>
				</tr>
				</table>
                 
                 
                 </td>
				
			     <td><?php echo $date_envoi ?> <?php echo DHTMLCalendar::calendar( "rel_send" ) ?></td>
              <td ><?php echo $rsq->fields('type_follow_1') ?></td>
                <td ><a href="#" onClick="javascript: realisercomment(<?php echo $rsq->fields('idcontact_follow') ?>)"><?php if ($realiser == 1 ) {?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/ok.png" alt="réaliser" width="20px" height="20px" /><?php }else{  ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/non.png" alt="Non réaliser" width="20px" height="20px" /><?php } ?></a></td>
                  <td><a href="#" onClick="javascript: loadBillingconttcoment(<?php echo $rsq->fields('idcontact_follow') ?>)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Voir" /></a></td>
		    <!--<td><a href="#" onClick="javascript: deletecomment(<?php echo $rsq->fields('idcontact_follow') ?>)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/drop.png" alt="Voir" /></a></td>-->
			</tr></tbody>
            <?php
										$rsq->MoveNext();
									}
									?>
			
					</table>
		<?php
										
									}
									?>   
     </div>
						</div> <!-- Fin blocMultiple -->			
		
						<div class="spacer"></div><br/>
					
                    
                    
						
						<div class="spacer"></div>	
					</body>
					</div> <!-- Fin #suivi -->
				
					<!-- *********** -->
					<!--  Assurance  -->
					<!-- *********** -->
					<div id="insurance" class="displayNone">

					<?php 
					if( $idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) ) ) {
						include_once( "$GLOBAL_START_PATH/objects/DBObject.php" );
						class CreditInsurance extends DBObject { 
							public function __construct( $idcredit_insurance ) { 
								parent::__construct( "credit_insurance", false, "idcredit_insurance", $idcredit_insurance ); 
								} 
						}
						$creditInsurance = new CreditInsurance( $idcredit_insurance );
					?>
					
					<div class="blocMultiple blocMultipleBorder" style="min-height:150px;">
						<h2>Assurance du client</h2>
						
						<script type="text/javascript">
						/* <![CDATA[ */
							function updateInsuranceForm(){

								switch( $( "#insurance_status :selected" ).val() ){
									case "Accepted":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "block" );
										$( "#InsuranceAmountDiv" ).css( "display", "block" );
										$( "#OutstandingCreditsDiv" ).css( "display", "block" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
																				
										if( $( "#insurance_amount_creation_date" ).val() == "" )
											$( "#insurance_amount_creation_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										//alert($( "#insurance_amount" ).val());
										
										if( $( "#insurance_amount" ).val() == "" || $( "#insurance_amount" ).val() == "0.00" )
											$( "#insurance_amount" ).val( "5000.00" ); 
											
										$( "#insurance_refusal_date" ).val( "" );										
										
										break;

									case "Refused":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "block" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
																				
										if( $( "#insurance_refusal_date" ).val() == "" )
											$( "#insurance_refusal_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										$( "#insurance_amount_creation_date" ).val( "" );		
										$( "#insurance_amount" ).val( "" );
																				
										break;

									case "Terminated":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "block" );
										
										if( $( "#insurance_refusal_date" ).val() == "" )
											$( "#insurance_refusal_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										$( "#insurance_amount_creation_date" ).val( "" );		
										$( "#insurance_amount" ).val( "" );										
										break;

									case "Deleted":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "block" );
										
										if( $( "#insurance_refusal_date" ).val() == "" )
											$( "#insurance_refusal_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										$( "#insurance_amount_creation_date" ).val( "" );		
										$( "#insurance_amount" ).val( "" );										
										break;

									default:
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "none" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
										
										$( "#insurance_refusal_date" ).val( "" );
										$( "#insurance_amount_creation_date" ).val( "" );
										$( "#insurance_amount" ).val( "" );										
										break;
								}
							}
						/* ]]> */
						</script>
						
						<label><span>Statut</span></label>
						<select name="insurance_status" id="insurance_status" onChange="updateInsuranceForm();" style="width:160px !important;">
							<option value="">-</option>
							<option<?php if( isset( $key ) && $insurance_status == "Accepted" ) echo " selected=\"selected\""; ?> value="Accepted">Accepté</option>
							<option<?php if( isset( $key ) && $insurance_status == "Refused" ) echo " selected=\"selected\""; ?> value="Refused">Refusé</option>
							<option<?php if( isset( $key ) && $insurance_status == "Terminated" ) echo " selected=\"selected\""; ?> value="Terminated">Résilé</option>
							<option<?php if( isset( $key ) && $insurance_status == "Deleted" ) echo " selected=\"selected\""; ?> value="Deleted">Supprimé</option>
						</select>
						<div class="spacer"></div>	
						
						
						<div id="InsuranceAttachmentDiv"<?php if( !isset( $key ) || $insurance_status == "" ) echo " style=\"display:none;\""; ?>>
							<label><span>Pièce jointe</span></label>
							<input class="textSimple" style="height:21px; width:159px;" type="file" size="10" name="InsuranceAttachment" id="InsuranceAttachment" value="<?php echo Dictionnary::translate( "add" ) ?>" />
							<?php if(isset($key))listInsuranceAttachments( $key ); ?>
						</div>
						<div class="spacer"></div>	
						
						<div id="InsuranceAmountCreationDateDiv"<?php if( !isset( $key )  || $insurance_status != "Accepted" ) echo " style=\"display:none;\""; ?>>
							<label><span>Date d'accord de l'encours</span></label>
							<input type="text" name="insurance_amount_creation_date" id="insurance_amount_creation_date" value="<?php echo isset( $key ) && $insurance_amount_creation_date != "0000-00-00" ? $insurance_amount_creation_date : "" ?>" readonly class="textidInput" />
							<?php DHTMLCalendar::calendar( "insurance_amount_creation_date" ); ?>
						</div>
						<div class="spacer"></div>	
						
						<div id="InsuranceRefusalDateDiv"<?php if( !isset( $key ) || $insurance_status != "Refused" ) echo " style=\"display:none;\""; ?>>
							<label><span>Date refus</span></label>
							<input type="text" name="insurance_refusal_date" id="insurance_refusal_date" value="<?php echo $insurance_refusal_date != "0000-00-00" ? $insurance_refusal_date : "" ?>" class="textidInput" readonly />
							<?php DHTMLCalendar::calendar( "insurance_refusal_date" ); ?>
						</div>
						<div id="InsuranceTerminationDateDiv"<?php if( !isset( $key ) || $insurance_status != "Terminated" ) echo " style=\"display:none;\""; ?>>
							<label><span>Date résiliation</span></label>
							<input type="text" name="insurance_termination_date" id="insurance_termination_date" value="<?php echo $insurance_termination_date != "0000-00-00" ? $insurance_termination_date : "" ?>" class="textidInput" readonly />
							<?php DHTMLCalendar::calendar( "insurance_termination_date" ); ?>
						</div>
						<div id="InsuranceDeletionDateDiv"<?php if( !isset( $key ) || $insurance_status != "Deleted" ) echo " style=\"display:none;\""; ?>>
							<label><span>Date suppression</span></label>
							<input type="text" name="insurance_deletion_date" id="insurance_deletion_date" value="<?php echo $insurance_deletion_date != "0000-00-00" ? $insurance_deletion_date : "" ?>" class="textidInput" readonly />
							<?php DHTMLCalendar::calendar( "insurance_deletion_date" ); ?>
						</div>												
						<div class="spacer"></div>	
						
						<div id="InsuranceAmountDiv"<?php if( !isset( $key ) || $insurance_status != "Accepted" ) echo " style=\"display:none;\""; ?>>
							<label><span>Encours accordé (HT)</span></label>
							<input style="width:16.3%;" type="text" id="insurance_amount" name="insurance_amount" maxlength="10" value="<?php if( isset( $key ) ) echo $insurance_amount; ?>" class="textidInputMin" style="<?php if( $insurance_status != "Accepted" ) echo " background-color:#E7E7E7;"; ?>" /> 
							<span class="textSigne">&euro;</span>
						</div>
						<div class="spacer"></div>	
						
						<div id="OutstandingCreditsDiv"<?php if( !isset( $key ) || $insurance_status != "Accepted" ) echo " style=\"display:none;\""; ?>>
							<label><span>Encours assurance (HT)</span></label>
							<span class="textidSimpleMin"><?php echo isset( $key ) ? Util::priceFormat( $customer->getInsuranceOutstandingCreditsET() ) : Util::priceFormat( 0.0 ); ?></span>
						</div>
						<div class="spacer"></div>		
						
					</div> <!-- Fin blocMultiple -->
	

					<div class="blocMultiple blocMultipleBorder" style="min-height:150px;">
						<h2>Montant assurance</h2>
						<div class="spacer"></div>	
						
						<p>Montant min. pris en charge TTC : <?php echo Util::priceFormat( $creditInsurance->get( "minimum" ) ); ?></p>
						<p>Montant max. assuré / client : <?php echo Util::priceFormat( $creditInsurance->get( "maximum" ) ); ?></p>
						<p>Délai min. de prise en charge : 
						<?php
							$months = floor( $creditInsurance->get( "start_delay" ) / 30 );
							$days 	= $months ? ceil( $creditInsurance->get( "start_delay" ) % 30 ) : $creditInsurance->get( "start_delay" );
							if( $months && $days ) { echo "$months mois et $days jours"; }
							else { echo $months ? "$months mois" : "$days jours"; }
						?></p>
						<p>Délai max. pour prise en charge : 
						<?php
							$months = floor( $creditInsurance->get( "end_delay" ) / 30 );
							$days 	= $months ? ceil( $creditInsurance->get( "end_delay" ) % 30 ) : $creditInsurance->get( "end_delay" );
							if( $months && $days ) { echo "$months mois et $days jours"; }
							else { echo $months ? "$months mois" : "$days jours"; }
						?></p>
					</div> <!-- Fin blocMultiple -->
					
					<div class="blocMultiple blocMultipleBorder" style="min-height:150px;">
						<h2><?php echo htmlentities( $creditInsurance->get( "name" ) ); ?> - <span class="upperCase">Coordonnées</span></h2>
						<div class="spacer"></div>		
						
						<p><strong><?php echo DBUtil::getDBValue( "title$lang", "title", "idtitle", $creditInsurance->get( "idtitle" ) ) . ". " . htmlentities( $creditInsurance->get( "firstname" ) ) . " " . htmlentities( $creditInsurance->get( "lastname" ) ); ?></strong></p>
						<p><?php echo htmlentities( $creditInsurance->get( "address" ) ); ?></p>
						<p><?php echo htmlentities( $creditInsurance->get( "address2" ) ); ?></p>
						<p class="floatleft"><strong><?php echo htmlentities( $creditInsurance->get( "zipcode" ) ); ?> <?php echo htmlentities( $creditInsurance->get( "city" ) ); ?> - <?php echo DBUtil::getDBValue( "name$lang", "state", "idstate", $creditInsurance->get( "idstate" ) ); ?></strong></p>
						<div class="spacer"></div>
						
						<p class="marginTop floatleft">Tél. : <?php echo Util::phoneNumberFormat( $creditInsurance->get( "phonenumber" ) ); ?> - Fax : <?php echo Util::phoneNumberFormat( $creditInsurance->get( "faxnumber" ) ); ?></p>
						<div class="spacer"></div>
						
						<p>Email : <a href="#" class="blueLink" onClick="mailto:'<?php echo $creditInsurance->get( "email" ); ?>'; return false;"><?php echo htmlentities( $creditInsurance->get( "email" ) ); ?></a></p>
						<p>Site internet : <a href="<?php echo $creditInsurance->get( "website" ); ?>" class="blueLink" onClick="window.open( this.href ); return false;"><?php echo htmlentities( $creditInsurance->get( "website" ) ); ?></a></p>
						<div class="spacer"></div>						
						
					</div> <!-- Fin blocMultiple -->
					
					<div class="spacer"></div><br/>
					
					<input type="submit" name="Modifiy" class="inputSave" value="<?php echo $submit ?>" />
					<div class="spacer"></div>				
					
					<?php } ?>
					
					</div> <!-- Fin #insurance -->
					
<?php
/**********************************************************
								 *                         Factor                         *
								 **********************************************************/
								
								$factor_url = DBUtil::getParameterAdmin( "ad_factor_url" );
								
								if( isset( $key ) ){
									
									if( $factor_status == "Accepted" ){
										
										$display_amount = "block";
										$display_factor_amount = true;
										
									}else{
										
										$display_amount = "none";
										$display_factor_amount = true;
										
									}
									
									if( $factor_status == "Refused" )
										$display_refusal = "block";
									else
										$display_refusal = "none";
									
								}else{
									
									$display_refusal = "none";
									$display_amount = "none";
									$display_factor_amount = false;
									
								}
								
								?>
							<!--	<div class="subContent" id="factor">
									<div style="margin:10px 0px; margin-left:-5px;"></div>
									<?php if( !empty( $factor_url ) ){ ?><div><a href="<?php echo $factor_url ?>" onClick="window.open(this.href); return false;" class="normalLink blueText">Site Web du factor</a></div><?php } ?>
									<div class="tableContainer">
										<table class="dataTable clientTable">
											<tr>
												<th style="width:25%;">Factor n°</th>
												<td style="width:25%;"><!--<input type="text" name="factor" maxlength="20" value="<?php if( isset( $key ) ){ echo $factor; } ?>" class="textInput" />
								
													<select name="factor" maxlength="20">
														<?php if(( isset( $key )&& $factor=='1')) {  ?>
														<option value="1" selected >Oui</option>
														<option value="0">Non </option>
														<?php } else {?>
														<option value="1" >Oui</option>
														<option value="0" selected >Non</option>
															<?php }?>
															
									
													</select>
												</td>
												<th style="width:25%;">
													<div id="fact_mt_title" style="display:<?php echo $display_amount ?>;">
														Encours accordé (TTC)
													</div>
												</th>
												<td style="width:25%;">
													<div id="fact_mt" style="display:<?php echo $display_amount ?>;">
														<input type="text" name="factor_amount" maxlength="10" value="<?php if( isset( $key ) ){ echo $factor_amount; } ?>" class="textInput" style="width:60px;" /> &euro;
													</div>
												</td>
											</tr>
											<tr>
												<th>Statut</th>
												<td>
<?php
													
													/*if( isset( $key ) )
														Drawlift_factor_status( $factor_status );	
													else
														Drawlift_factor_status();*/
													
?>
												</td>
												<th>
													<div id="FactorAmountCreationDateTitleDiv" style="display:<?php echo $display_amount ?>;">
														Date d'accord de l'encours
													</div>
													<div id="fact_date_title" style="display:<?php echo $display_refusal ?>;">
														Date de refus
													</div>
												</th>
												<td>
													<div id="FactorAmountCreationDateDiv" style="display:<?php echo $display_amount ?>;">
<?php
														
														/*if( !empty( $factor_amount_creation_date ) && $factor_amount_creation_date!= "0000-00-00" ){
															 
															$value = usDate2eu( $factor_amount_creation_date );
															$startdate = date( $factor_amount_creation_date );
															
														}else{
															
															$value = "";
															$startdate = date( "Y-m-d" );
															
														}*/
														
?>
														<input type="text" name="factor_amount_creation_date" id="factor_amount_creation_date" value="<?php echo $value ?>" readonly class="calendarInput" />
														<?php DHTMLCalendar::calendar( "factor_amount_creation_date" ); ?>
													</div>
													<div id="fact_date" style="display:<?php echo $display_refusal ?>;">
<?php
														
														if( !empty( $factor_refusal_date ) && $factor_refusal_date != "0000-00-00" ){
															
															$value = usDate2eu( $factor_refusal_date );
															$startdate = $factor_refusal_date;
															
														}else{
															
															$value = "";
															$startdate = date( "Y-m-d" );
															
														}
														
?>
														<input type="text" name="factor_refusal_date" id="factor_refusal_date" value="<?php echo $value ?>" class="calendarInput" readonly />
														<?php DHTMLCalendar::calendar( "factor_refusal_date" ); ?>
													</div>
												</td>
											</tr>
<?php
											
											if( isset( $key ) && $display_factor_amount ){
												
?>
											<tr>
												<th>Solde de l'encours (TTC)</th>
												<td colspan="3"><?php echo Util::priceFormat( getBuyerFactorBalance( $key, $factor_amount ) ) ?></td>
											</tr>
<?php
												
											}
											
?>
										</table>
									</div>
									<div class="submitButtonContainer">
										<input type="submit" name="Modifiy" class="blueButton" value="<?php echo $submit ?>" />
									</div>
								</div>-->
					<!-- ************ -->
					<!-- Tarification -->
					<!-- ************ -->
					<div id="pricing" class="displayNone">
					
						<?php if( isset( $key ) ){ ?>
						<div class="blocMultiple blocMLeft">
							<h2>Règlement</h2>
							
							<label><span>Délais de paiement</span></label>
							<?php if( isset( $key ) ){ DrawLift_buyerdelay( "payment_delay", $delivpayment ); }
							else { DrawLift_buyerdelay( "payment_delay", 1 );} ?>
							<div class="spacer"></div>
							
							<label><span>Moyen de paiement</span></label>
							<?php if( isset( $key ) ){ DrawLift_buyercond( "payment", $condpayment ); }
							else { DrawLift_buyercond( "payment", 4 );} ?>
							<div class="spacer"></div>
							
							<label><span>Famille d'acheteur</span></label>
							<?php if( isset( $key ) ){ $rsbuy->Fields( "idbuyer_family"); }
							else { DrawLift_buyerfamily( "buyer_family");} ?>
							<?php DrawLift_buyerfamily( "buyer_family"); ?>
							<div class="spacer"></div>
							
						</div> <!-- Fin blocMultiple -->
						<?php }	?>

						<div class="spacer"></div>
						
						<div class="blocMultiple blocMLeft">
							<h2>Escompte automatique</h2>
							
							<label><span>Escompte</span></label>
							<input type="text" class="calendarInput" name="rebate_rate" value="<?php echo isset( $key ) ? DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $key ) : floatval( DBUtil::getParameterAdmin( "rebate_rate" ) ); ?>" />
							<span class="textidSimp">&nbsp;%</span>
							 
							<div class="spacer"></div>
	
						</div> <!-- Fin blocMultiple -->	
						<div class="spacer"></div>
						
						<input type="submit" name="Modify" class="inputSave" value="Sauvegarder" />
						<div class="spacer"></div>
											
					</div> <!-- Fin #pricing -->
					
					
					<!-- ************ -->
					<!--     RIB      -->
					<!-- ************ -->
					<div id="rib" class="displayNone">
					
						<div class="blocMultiple blocMLeft">
							<h2>RIB</h2>
							
							<label class="marginTopRib"><span>RIB</span></label>
							<table class="tabRib" cellspacing="0" cellpadding="0" border="0">
							<tr>
								<th align="left">Code banque</th>
								<th align="left">Code guichet</th>
								<th align="left">N° de compte</th>
								<th align="left">Clé RIB</th>
							</tr>
							<tr>
								<td><input type="text" class="textInput codeBanque" name="code_etabl" maxlength="5" value="<?php if( isset( $key ) ){ echo $code_etabl; } ?>" /></td>
								<td><input type="text" class="textInput codeBanque" name="code_guichet" maxlength="5" value="<?php if( isset( $key ) ){ echo $code_guichet; } ?>" /></td>
								<td><input type="text" class="textInput nCompte" name="n_compte" maxlength="11" value="<?php if( isset( $key ) ){ echo $n_compte; } ?>"  /></td>
								<td><input type="text" class="textInput cleRib" name="cle_rib" maxlength="2" value="<?php if( isset( $key ) ){ echo $cle_rib; } ?>"  /></td>
							</tr>
							</table>
							<div class="spacer"></div>
							
							
							<label><span>Domiciliation<br/>&nbsp;</span></label>
							<textarea class="textInput" name="domiciliation"><?php if( isset( $key ) ){ echo $domiciliation; } ?></textarea>
							<div class="spacer"></div>
							
							<label><span>Pièce jointe</span></label>
							<input type="file" name="RIBAttachment" class="textSimple" style="height:21px;" />
							<?php if( isset( $key ) ){ showRIBAttachment( $key ); } ?>
							
							
						</div> <!-- Fin blocMultiple -->	
						<div class="spacer"></div>	
						
						<input type="submit" name="Modify" class="inputSave" value="<?php echo $submit ?>" />
						<div class="spacer"></div>	
					
					</div> <!-- Fin #rib -->
					
					
					<!-- ************ -->
					<!--  Historique  -->
					<!-- ************ -->
					<?php if( isset( $key ) ){ ?>
					<div id="history" class="displayNone">
						<?php	
							//$included_history = true;
							include_once( "$GLOBAL_START_PATH/sales_force/buyer_history.php" );	
						?>
						<?php if ( $_SESSION['salesman_user'] == 0 ) {?>	
						<?php echo listEstimates( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo listOrders( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo listInvoices( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo listCredits( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo PaidRegulationsBuyer( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo PendingRegulationsBuyer( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo BacklogRegulationsBuyer( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo listBL( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo displaySearchResultsByMonth( $key ) ?>
						<?php } ?>
						
						<?php if ( $_SESSION['salesman_user'] == 1 ) {?>
						<?php echo listEstimates( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo listOrders( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo listInvoices( $key ) ?>
						<div class="spacer"></div><br/>
						<?php echo PaidRegulationsBuyer( $key ) ?>
						<?php } ?>
						
						
						
						
						<!--<p class="subTitle">Historique du client n°<?php echo $idbuyer ?><p>
						<div class="spacer"></div>	-->					
						<?php	
						//	include_once( "$GLOBAL_START_PATH/sales_force/buyer_history_table.php" );	
						?>	
	
					</div> <!-- Fin #history -->
					<?php }	?>
					
					
					
					<!-- ************ -->
					<!-- Infos Divers -->
					<!-- ************ -->
					<div id="infosDivers" class="displayNone">
					
						<div class="blocMultiple blocMLeft">
							<h2>Informations diverses sur la société</h2>	
							
							<label><span>Forme juridique</span></label>
							<input type="text" name="status_company" id="status_company" value="<?php if( isset( $key ) ){ echo $form; } ?>" class="textInput" />
							<div class="spacer"></div>
												
						</div> <!-- Fin blocMultiple -->	
					
						<div class="blocMultiple blocMRight">
							<h2>Informations financières sur la société</h2>	
							
							
							
							<label><span>Durée de <br/>l'exercice</span></label>
							<input type="text" name="lenght_exo" id="lenght_exo" value="<?php if( isset( $key ) ){ echo $lenght_exo; } ?>" class="calendarInput" />
							<div class="spacer"></div>
							
							<label><span>Clôture de l'exercice</span></label>
							<input type="text" name="cloture_exo" id="cloture_exo" value="<?php if( isset( $key ) ){ echo $clo_exo; } ?>" class="calendarInput" />
							<span style="float:left; margin:9px 0 0 2px; width:20px;">&nbsp;</span>
							
							<label><span>Valeur ajoutée</span></label>
							<input type="text" name="vala_web" id="vala_web" value="<?php if( isset( $key ) ){ echo Util::numberFormat( $va_company, 0 ); } ?>" class="calendarInput" /> 
							<span style="float:left; margin:9px 0 0 2px; width:20px;">&euro;</span>	
							
							<div class="spacer"></div>	
							
							<label><span>Rentabilité commerciale</span></label>
							<input type="text" name="renta_com" id="renta_com" value="<?php if( isset( $key ) ){ echo $renta_com; } ?>" class="calendarInput" /> 
							<span style="float:left; margin:9px 0 0 2px; width:20px;">%</span>
							
							<label><span>Exédent brut d'exploitation</span></label>
							<input type="text" name="exed_exploit" id="exed_exploit" value="<?php if( isset( $key ) ){ echo Util::numberFormat( $exed_exploit, 0 ); } ?>" class="calendarInput" /> 
							<span style="float:left; margin:9px 0 0 2px; width:20px;">&euro;</span>
							<div class="spacer"></div>
				
						</div> <!-- Fin blocMultiple -->
					
					
						<div class="blocMultiple blocMLeft">
							<h2>Informations diverses sur le dirigeant de la société</h2>
							
							<label><span>Nom</span></label>
							<input type="text" name="nom_boss" id="nom_boss" value="<?php if( isset( $key ) ){ echo $boss_name; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Fonction</span></label>
							<input type="text" name="fonction_boss" id="fonction_boss" value="<?php if( isset( $key ) ){ echo $boss_fonction; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Date de naissance</span></label>
							<input type="text" name="birthday_boss" id="birthday_boss" value="<?php if( isset( $key ) ){ echo $boss_birthday; } ?>" class="textInput" />
							<div class="spacer"></div>
							
							<label><span>Lieu de naissance</span></label>
							<input type="text" name="birthday_boss_city" id="birthday_boss_city" value="<?php if( isset( $key ) ){ echo $boss_birthday_city; } ?>" class="textInput" />
							<div class="spacer"></div>
		
						</div> <!-- Fin blocMultiple -->
						<div class="spacer"></div>
						<?php if( isset( $key ) ) listSubsidiaryCompanies( $key ); ?>
							
						<input type="submit" name="Modify" class="inputSave" value="<?php echo $submit ?>" />
						<div class="spacer"></div>	
					
					</div> <!-- Fin #infosDivers -->	
					
					

					<!-- *********** -->
					<!--  Infos com  -->
					<!-- *********** -->
					<div id="infosComp" class="displayNone">
						<div class="blocMultiple blocMRight">
							<?php if( isset( $key ) && !empty( $logo ) && file_exists( $GLOBAL_START_PATH . $logo ) ){ ?>
								<img src="<?php echo $GLOBAL_START_URL . $logo ?>" alt="" style="max-width:200px; *width:<?php echo $maxwidth ?>;" />
								<div class="spacer"></div>	
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $key ?>&amp;DeleteLogo=<?php echo $logo ?>#infosComp" onClick="return confirm( 'Voulez-vous vraiment supprimer ce document ?' );">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" style="border-style:none;" /></a>
							
							<?php }	?>
							<div class="spacer"></div>	
						</div>
						
						<div class="blocMultiple blocMLeft" id="notPartTable3">
							<h2>Informations complémentaires</h2>	
							
							<?php if( isset( $login ) && !empty( $login ) ){ ?>
								<label><span>Identifiant</span></label>
								<span class="textSimple" style="height:36px;">&nbsp;<?php echo $login ?><br/>
								
								&nbsp;<input type="checkbox" name="GeneratePassword" class="verticalMiddle" />
								Générer un nouveau mot de passe
								
								</span>
							<?php } ?>
							<div class="spacer"></div>
							
							<?php if( !isset( $key ) ){ ?><p class="textInfo">Le client recevra automatiquement un mail d'enregistrement lui donnant son identifiant et son mot de passe pour accéder à son compte sur le Front Office.</p><?php } ?>
							
							<label><span>Logo</span></label>
							<input type="file" name="logo" value="" class="textSimple" style="height:21px" />
							<div class="spacer"></div>									
														
							<label><span>Newsletter</span></label>
							<span class="textidSimple">&nbsp;<?php if( isset( $key ) && $rsbuy->fields( "newsletter" ) > 0 ){ echo "Oui";}
							else{ echo "Non"; } ?></span>
							
							<?php if( $type == "buyer" ){ ?>
							<label class="smallLabel"><span>Réf/client</span></label>
							<div class="simpleSelect">
								<?php if( isset( $key ) ) { DrawLift_referenced( $buyer_ref_us ); }
								else { DrawLift_referenced(); } ?>
							</div>
							<?php }	?>						
							<div class="spacer"></div>	
																			
							<?php if( isset( $key ) && ( !isset( $login ) && empty( $login ) ) ){ ?>
								<label><span>Compte client</span></label>
								<span class="textSimple"><input type="checkbox" name="GenerateLogin" class="verticalMiddle" />
								Créer un compte client pour le Front Office</span><div class="spacer"></div>
							<?php } ?>
							<div class="spacer"></div>	
											
						</div> <!-- Fin blocMultiple -->
						<div class="spacer"></div>	
						
						<div class="blocMultiple blocMLeft" id="notPartTable3">
							<h2>Conditions générales de vente</h2>	
							<label><span>Fichier</span></label>
							<input type="file" name="gts" size="10" value="" class="textSimple" style="height:21px" />
							<?php
							
								if( isset( $key ) && file_exists( "$GLOBAL_START_PATH/data/gts/$key/" . $customer->get( "gts" ) ) ){
									
									?>
									<script type="text/javascript">
									/* <![[ CDATA */
										function deleteGTS(){

											$.ajax({
											 	
												url: "<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?delete=gts&idbuyer=<?php echo $key; ?>",
												async: true,
												type: "POST",
												data: "",
												error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( onErrorMessage ); },
											 	success: function( responseText ){
									
													if( responseText != "1" ){

														alert( "Une erreur inattendue est survenue : " + responseText );
														return;
														
													}

													$( "#GTSView" ).remove();
													$( "#GTSDelete" ).remove();
												}
									
											});
											
										}

									/* ]]> */
									</script>
									<a id="GTSView" title="Télécharger le fichier" href="/sales_force/contact_buyer.php?read=gts&idbuyer=<?php echo $key; ?>" onClick="window.open( this.href ); return false;">
										<img src="/images/back_office/content/oeil.gif" alt="" />
									</a>
									<a id="GTSDelete" title="Supprimer le fichier" href="#" onClick="if( confirm('Etes-vous certains de vouloir supprimer les conditions générales de vente?') ) deleteGTS(); return false;">
										<img src="/images/back_office/content/corbeille.jpg" alt="" />
									</a>
									<?php
									
								}
								
							?>
							<div class="spacer"></div>									
						</div> <!-- Fin blocMultiple -->
						<div class="spacer"></div>	
						
						<?php if( isset( $key ) && isset( $google_map_api_key ) && $google_map_api_key != "" ){ ?>										
							<div class="blocMultiple blocMLeft">
								<h2>Visuel Google Maps</h2>
								<div id="mapG2" style="width:725px; height:350px; margin:10px 0;"></div>
								<?php
									if( isset( $key )){
										// Les scrpts JS étant immenses ils ont été déplacés dans ce fichier, merci
										include_once($GLOBAL_START_PATH.'/templates/sales_force/googleMaps.js.php');
									}
								?>
							</div>
							<div class="spacer"></div>
							
							<div class="blocMultiple blocMLeft">
								<h2>Visuel Street View</h2>
								<div id="pano" style="width:725px; height:350px; display:none;  margin:10px 0;"></div>
							</div>
							<div class="spacer"></div>	
						<?php } ?>
						
						<div class="spacer"></div><br/>
					
						<input type="submit" name="Modifiy" class="inputSave" value="<?php echo $submit ?>" />
						<div class="spacer"></div>	
					
					</div> <!-- Fin #name-address -->
										
					<!-- ************ -->
					<!-- gestContacts -->
					<!-- ************ -->
					<?php if(isset($key)){ ?>
					<div id="gestContacts" class="displayNone">
						<div id="gestContactsContent" style="margin:10px 0px; margin-left:-5px;"></div>
			
						<div class="spacer"></div>
										
					</div> <!-- Fin #gestContacts -->
					
					
					<?php }	?>
					  <!-- *********** -->
					<!-- echeances -->
					<!-- *********** -->
					<div id="echeances" class="displayNone">
                      <?php


//--------------------------------------------------------------------------------------------------

include_once (dirname(__FILE__) . "/../objects/classes.php");
include_once ("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
// Relance Majax et pas Majax
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

	
$Title = "Liste des références non définies pour les échéances";


//--------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------

?>

	<script type="text/javascript">
	/* <![CDATA[ */
	
		

		function save(reference, idbuyer, idorder, dates, month_term, quantity,designation){
				
			
				
				var divToComplete = document.getElementById('echeancesaved') ;
						//var data = ''; 
					//	var message = 'Supression du contact en cours ...';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/saveecheance.php?reference=' + reference + '&idbuyer=' + idbuyer + '&idorder=' + idorder + '&dates=' + dates+ '&month_term=' + month_term + '&quantity=' + quantity + '&designation=' + designation;
						//machineAjax( location, divToComplete, data, message );
						
			
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
	
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
		{
				divToComplete.innerHTML = xhr_object.responseText ;
			window.location.reload();
			}else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);
						
						
						
				//	}
				}
		function changemonth(month_term,reference){
		
		//var month_term = document.getElementById('month_term'+i).value ;
			//alert(month_term);		
				var data = "month_term=" + month_term + "&reference=" + reference;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_futur.php?update=month_term",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
				
	/* ]]> */
	</script>
	


	<?php
		
		
		
		
			$lang = User::getInstance()->getLang();
			// $SQL_Query = "SELECT $Str_FieldList FROM estimate op, buyer bu, commercial com $SQL_Condition";
			$SQL_Query = " 
				SELECT  d.reference,
			 d.month_term,
				orw.quantity, 
				orw.idorder, 
				orw.summary, 
				orw.designation,
				orw.updated,
				o.idbuyer,
				b.company,
				c.firstname
					FROM 
					detail d,
					contact c,  
						buyer b, 
						`order_row` orw,
						`order` o
					
						
					WHERE 
					 d.month_term > 0
					  AND orw.reference =  d.reference
				
					AND o.idorder = orw.idorder
					AND o.idbuyer = b.idbuyer
					AND o.idbuyer = c.idbuyer
					AND c.idcontact = 0
			AND o.idbuyer = '$key'
					
				";
				
			$rs = DBUtil::query($SQL_Query);
			
			global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
		
			include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");
		
			$encryptedQuery = URLFactory::base64url_encode(Encryptor::encrypt($SQL_Query, $GLOBAL_DB_PASS));
		
			if ($rs->RecordCount() > 0) { // At least one row result
		
		?>
	
		<div class="">
			<!-- Titre -->
			<h1 class="titleSearch">
            
			<span class="textTitle">Liste des références non définies : <?php echo $rs->RecordCount() ?> </span>
           
			<div class="spacer"></div>
			</h1>
					 <div id="echeancesaved" style="width:inherit; height:auto">
                          
                               </div><br><br>		
							
			
				
			<table class="dataTable resultTable">
					<thead>
						<tr>
							<th>N° commande</th>
							<th>N° client</th>
							<th>Company/Nom</th>
							<th>Référence</th>
							<th>Designation</th>
                            <th>Qté</th>
                            <th>Mois échéance</th>
                             <th></th>
							
							
						</tr>
						<!-- petites flèches de tri -->
						
					</thead>
					<tbody>
					<?php
	
						for ($i = 0; $i < $rs->RecordCount(); $i++) {
						 $reference =  $rs->Fields("reference");
						  $month_term =  $rs->Fields("month_term");
						  $date =  $rs->Fields("updated");
					
						
				$quantity =  $rs->Fields("quantity");
				$idorder = $rs->Fields("idorder");
				$designation = nl2br($rs->Fields("designation"));
				
				$summary = $rs->Fields("summary");
				$company = $rs->Fields("company");
				$firstname = $rs->Fields("firstname");
			$idbuyer = $rs->Fields("idbuyer");
							
							$contact_Name = $rs->Fields("company") . "  " . $rs->Fields("firstname") ;
					
			
					 
						  $exist = 0;
						    $qr = " 
				SELECT ot.idorder,
				ot.reference
			
				
					FROM 
					
					product_term ot
					
					 
					
				";
				
			$rss = DBUtil::query($qr);
						while( !$rss->EOF()){
						$idorder_ot =  $rss->Fields("idorder");
						$reference_ot =  $rss->Fields("reference");
										if(($idorder_ot == $idorder) && ($reference_ot == $reference ))
										$exist = 1;
										$rss->MoveNext();
									}
									if($exist == 0) {
			
					?>
						  <input type="hidden" name="reference" id="reference<?php echo $i ?>" value="<?php echo $reference ?>" />
							<input type="hidden" name="idbuyer" id="idbuyer<?php echo $i ?>" value="<?php echo  $idbuyer ?>" />		
                             <input type="hidden" name="idorder" id="idorder<?php echo $i ?>" value="<?php echo  $idorder ?>" />
							<input type="hidden" name="designation" id="designation<?php echo $i ?>" value="<?php echo  htmlspecialchars($designation) ?>" />		
                           
							<input type="hidden" name="quantity" id="quantity<?php echo $i ?>" value="<?php echo $quantity ?>" />	
                            <input type="hidden" name="dates" id="dates<?php echo $i ?>" value="<?php echo $date ?>" />			        
			        <tr class="blackText">
                     <td style="width:3%;" class="lefterCol"><?php echo $idorder ?></td>
                       <td style="width:3%;" class="lefterCol"><?php echo $rs->fields( "idbuyer" ); ?></td>
					 <td style="width:10%;">
						 
				        	<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>" onClick="window.open(this.href); return false;">
				        		<?php if($rs->fields("company")){ echo htmlentities( $rs->fields( "company" ) ); } else { ?> - <?php  }?>
				        	</a> / <?php echo $firstname ?>
				        </td>
                      <td  valign="left" style="text-align: left;width:5%;"><?php echo $reference ?>  
                    </td>
                      <td style="width:34%;text-align: left;line-height:8px;" ><?php echo $designation ?></td>
                      <td style="width:9%;"><?php echo $quantity ?> </td>
                 
                      <td style="width:5%;">  <input  onkeyup="javascript:changemonth(document.getElementById('month_term<?php echo $i ?>').value,document.getElementById('reference<?php echo $i ?>').value)" name="month_term" id="month_term<?php echo $i ?>" value="<?php echo $month_term ?>"   size="1" maxlength="2"/> </td>
				        
                     <td style="width:3%;" class="lefterCol"><a href="#" onClick=" save(document.getElementById('reference<?php echo $i ?>').value,document.getElementById('idbuyer<?php echo $i ?>').value, document.getElementById('idorder<?php echo $i ?>').value , document.getElementById('dates<?php echo $i ?>').value, document.getElementById('month_term<?php echo $i ?>').value,document.getElementById('quantity<?php echo $i ?>').value,document.getElementById('designation<?php echo $i ?>').value )">Valider</a></td>
                    
			        </tr>

			<?php
		}
			
				$rs->MoveNext();
			} ?>
					</tbody>
					</table>
				
		        <div class="spacer"></div>   
			
		</div>       		      	
		<div class="spacer"></div>     
			        
<?php } else {  ?>
	
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<p class="msg" style="text-align:center;">Aucun résultat </p>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
<?php }
 ?>


</div>



                  
                    <div class="spacer"></div>
						</div>
                          <!-- *********** -->
					<!-- echeances -->
					<!-- *********** -->
					<div id="echeancier" class="displayNone">
           
                  <?php

if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "n_service" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_name = '" . Util::html_escape( stripslashes( $_POST[ "n_service" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "n_serial" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET serial_number = '" . Util::html_escape( stripslashes( $_POST[ "n_serial" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "service_extern" ){

	if ($_POST[ "service_extern" ] == 0)$service_ext = 1;
	else $service_ext = 0;
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_extern = '" . Util::html_escape( stripslashes( $service_ext )) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "updatemails" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
		$idproduct_term = intval($_GET[ "idproduct_term" ]);
	error_reporting( 0 );
	 SendRelaunch();
	
	exit();
	
}
//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

$Title = "Récapitulatif fiche client administration";

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "update" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
		
	error_reporting( 0 );
	ModifyDate();
	
	exit();
	
}



//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/Supplier.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$con = DBUtil::getConnection();

$use_help = DBUtil::getParameterAdmin( "gest_com_use_help" );

$resultPerPage = 1000;
$maxSearchResults = 1000;

if( isset( $_REQUEST[ "idbuyer" ] ) )
	$idbuyer = $_REQUEST[ "idbuyer" ];


$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

$TableName = "product_term";

//---------------------------------------------------------------------------------------------

//suppression d'une commande

if( isset( $_POST[ "DeleteOrder" ] ) || isset( $_POST[ "DeleteOrder_x" ] ) )
	SupplierOrder::delete(  $_POST[ "OrderToDelete" ]  );

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<script type="text/javascript" language="javascript">
<!--
	
	function SortResult( sby, ord ){
		
		document.adm_estim.action="product_term_list.php?search=1&sortby="+sby+"&sens="+ord;
		document.adm_estim.submit();
		
	}
	
	function goToPage( pageNumber ){
		
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function SetDate( element ){

		var idorders = element.id.substring( element.id.lastIndexOf( '_', element.id.length ) + 1, element.id.length );
		
		var availibility = $( "#availability_date_" + idorders ).attr( "value" );
	//	var dispatch = $( "#dispatch_date_" + idorders ).attr( "value" );
		
		var postData = "action=update&idos=" + idorders + "&av=" + availibility 
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la date" ); },
		 	success: function( responseText ){

				if( responseText == 'wrong av date format' )
					alert( "Format de date echeance incorrect !" );
					else if( responseText == 'wrong sql' )
					alert( "Impossible de mettre à jour la date !" );
				
        		else if( responseText.length ){
        		
        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Modifications enregistrées' );

        			$( "#cellstatus_" + idorders ).html( responseText );
        			$( "#cellstatus_" + idorders ).attr( "class", "" );
        			$( "#celldelete_" + idorders ).html( "" );
        			
        		}	
					
			}

		});
		
		
	}

	function confirmDeleteOrder( idorder ){

		
		var ret = confirm( '<?php echo Dictionnary::translate( "gest_com_confirm_delete_order_supplier" ) ?>' + idorder + '?' );
		
		if( ret == true )
			document.forms.adm_estim.elements[ 'OrderToDelete' ].value = idorder;
		
		return ret;
		
	}
	
	function selectRelance(){
		
		document.getElementById( "interval_select_relance" ).checked = true;
		
	}
	
	function selectMonth(){
		
		document.getElementById( "interval_select_month" ).checked = true;
		
	}
	
	function selectDelivery( dateField ){
		
		document.getElementById( "interval_select_delivery" ).checked = true;
		
	}
	
	function selectBetween( dateField ){
		
		document.getElementById( "interval_select_between" ).checked = true;
		
		return true;
		
	}
	
// -->
</script>
<?php

//	$idbuyer = 11955;
	$from = "product_term";
	$now = getdate();
	
		
	
	if( !empty( $idbuyer ) ){
			// ------- Mise à jour de l'id gestion des Index  #1161
		//$idbuyer = FProcessString( $idbuyer );
		$SQL_Condition = " product_term.idbuyer = '$idbuyer' ";
	
	}
	
	
	
	$lang = User::getInstance()->getLang();
	

	
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sortby" ] ) )
		$orderby = " ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
	else
		$orderby = " ORDER BY idproduct_term ASC";
	
	//Nombre de pages
	$SQL_Query = "
	SELECT COUNT(*) AS resultCount 
	FROM $from
	 
		WHERE $SQL_Condition 
	";	
//	echo $SQL_Query;
	$rs = $con->Execute( $SQL_Query );
//	var_dump($rs);
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );
	
	if( $resultCount < $maxSearchResults ){
		
		$pageCount = ceil( $resultCount / $resultPerPage );
		
		//Recherche
		$start = ( $page - 1 ) * $resultPerPage;
		
		$SQL_Query = "
		SELECT 
		product_term.idproduct_term,
		product_term.idorder,
		product_term.reference,
		product_term.designation,
		product_term.service_extern,
		product_term.service_name,
		product_term.serial_number,
		product_term.idbuyer,
		product_term.relaunch_date,
		product_term.doc_certificat,
		product_term.term_date,
		buyer.company,
				contact.firstname
			
		FROM $from
		INNER JOIN buyer ON buyer.idbuyer = product_term.idbuyer 
		INNER JOIN contact ON product_term.idbuyer = contact.idbuyer AND contact.idcontact = 0
		WHERE $SQL_Condition 
		
		$orderby
		LIMIT $start, $resultPerPage";
//echo $SQL_Query;
		$rs = $con->Execute( $SQL_Query );
//	var_dump($rs);
		global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
			
		include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
			
		$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $SQL_Query, $GLOBAL_DB_PASS ) );
		
		if( $rs->RecordCount() > 0 ){
			
			?>
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function updateservice( idproduct_term, n_service ){
				
					var data = "idproduct_term=" + idproduct_term + "&n_service=" + n_service;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=n_service",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        					
						}

					});
		
				}


function updateserial( idproduct_term, n_serial ){
				
					var data = "idproduct_term=" + idproduct_term + "&n_serial=" + n_serial;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=n_serial",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        					
						}

					});
		
				}
				
				function updatedoc( idproduct_term,idorder, documents ){
				
					var data = "idproduct_term=" + idproduct_term + "&idorder=" + idorder + "&documents=" + documents;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=documents",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        					
						}

					});
		
				}
				

				function deliveryNoteDialog( idorder_supplier ){

					$( "#DeliveryNoteDialog" ).html( '<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/small_black_spinner.gif" alt="" /> chargement en cours...</p>' );

					$("#DeliveryNoteDialog").dialog({
			        
						modal: true,	
						title: "Commande Fournisseur N° " + idorder_supplier,
						position: "top",
						width: 800,
						zIndex:998,
						close: function(event, ui) { 
						
							$("#DeliveryNoteDialog").dialog( 'destroy' );
							
						}
					
					});

					$.ajax({
					 	
						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?dialog&idorder_supplier=" + idorder_supplier,
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible d'afficher la boîte de dialogue" ); 
							
						},
					 	success: function( responseText ){
					
							$( "#DeliveryNoteDialog" ).html( responseText );
										
						}
					
					});
					
				}
				//Handler of tooltip By behzad
	        $(document).ready(function() {
					$(".fournisseurHelp").each( function(){
					$(this).qtip({
						adjust: {
							mouse: true,
							screen: true
						},
						content: { url: '<?php echo HTTP_HOST; ?>/sales_force/product_term_list.php?showTooltip&idorder_supplier_parent=' + $(this).attr( 'rel' ) },
						position: {
							corner: {
								target: "rightMiddle",
								tooltip: "leftMiddle"
							}
						},
						style: {
							background: "#FFFFFF",
							border: {
								color: "#EB6A0A",
								radius: 5
							},
							fontFamily: "Arial,Helvetica,sans-serif",
							fontSize: "13px",
							tip: true,
							width: 600
						}
					});
					
				});
			});	
			function realiserservice(idproduct_term,service_extern){
					
				var data = "idproduct_term=" + idproduct_term + "&service_extern=" + service_extern;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=service_extern",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
				
					function SendEmail( idproduct_term, idbuyer ){

	
			var data = "idproduct_term=" + idproduct_term + "&idbuyer=" + idbuyer;
					var mails = 1;
					var idbuyer = idbuyer;
					
		var postData = "action=updatemails&idproduct_term=" + idproduct_term 
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?key="+idbuyer,
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'envoyer l'email" ); },
		 	success: function( responseText ){

				if( responseText == 'impossible relancer' )
					alert( "Impossible de relancer !" );
					else if( responseText == 'aucun mail' )
					alert( "Aucun mail trouvé !" );
				
        		else if( responseText.length ){
        		
        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Mails envoyés' );

        			$( "#cellstatus_" + idorders ).html( responseText );
        			$( "#cellstatus_" + idorders ).attr( "class", "" );
        			$( "#celldelete_" + idorders ).html( "" );
        			window.location.reload();
        		}	
					
			}

		});
		
		
	}

			//end of Tooltip
			/* ]]> */
			</script>
			<div id="DeliveryNoteDialog" style="display:none;"></div>
			<a name="topPage"></a>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />			

<div class="">
<form action="<?php echo $ScriptName ?>" method="post" name="adm_estim" name="adm_estim" enctype="multipart/form-data">
	<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
	<input type="hidden" name="search" value="1" />
				
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Récapitulatif fiche client administration : <?php echo $rs->RecordCount() ?> fiche(s)</span>
	<span class="selectDate">
                <a href="#bottom" class="goUpOrDown" style="text-decoration:none;font-size:10px; color:#000000;">
						Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a> &nbsp;&nbsp;
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/export_csv.php?type=order_supplier&amp;req=<?php echo $encryptedQuery ?>">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				</span>
	<div class="spacer"></div>
	</h1>		
				
	
							<table class="dataTable resultTable">
								<thead>
									<tr>
										<th style="width:8%;">N° cde</th>
										<th style="width:8%;">Référence</th>
										<th style="width:8%;">Service externe</th>
										<th style="width:21%;">Désignation</th>
										<th style="width:15%;">Gestion</th>
										
										<th style="width:15%;">N° de série</th>
										<th style="width:15%;">Certificat</th>
										<th style="width:10%;">Date échéance</th>
										<th style="width:8%;">Statut</th>
                                        <th style="width:8%;">Envoi de mails</th>
									</tr>
									<!-- petites flèches de tri -->
									<tr>
										<th class="noTopBorder">
											<a href="javascript:SortResult('user.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onMouseOver="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onMouseOut="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('user.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onMouseOver="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onMouseOut="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
									
										<th class="noTopBorder">
											<a href="javascript:SortResult('order_supplier.idorder_supplier','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onMouseOver="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onMouseOut="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('order_supplier.idorder_supplier','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onMouseOver="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onMouseOut="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
                                        	<th class="noTopBorder"></th>
										<th class="noTopBorder">
											
										</th>
											<th class="noTopBorder"></th>
											<th class="noTopBorder"></th>
											<th class="noTopBorder"></th>
											
										<th class="noTopBorder">
											<a href="javascript:SortResult('order_supplier.total_amount_ht','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onMouseOver="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onMouseOut="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('order_supplier.total_amount_ht','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onMouseOver="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onMouseOut="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
									</tr>
								</thead>
								<tbody>

<?php
			
			//Récupérer les traductions des status
			$strstatus = array();
			$db = &DBUtil::getConnection();
			
			$query = "SELECT order_supplier_status, order_supplier_status$lang FROM order_supplier_status";
			$rs2 = $db->Execute( $query );
			
			if( $rs2 === false )
				die( Dictionnary::translate( "gest_com_impossible_recover_status_list" ) );
			
			while( !$rs2->EOF() ){
				
				$strstatus[ $rs2->fields( "order_supplier_status" ) ] = $rs2->fields( "order_supplier_status$lang" );
				
				$rs2->MoveNext();
				
			}
			
			for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
					$idproduct_term			= $rs->Fields( "idproduct_term" );
				$idorder			= $rs->Fields( "idorder" );
				$idbuyer	= $rs->Fields( "idbuyer" );
				$company	= $rs->Fields( "company" );
				$firstname			= $rs->Fields( "firstname" );
				$reference				= $rs->Fields( "reference" );
				$service_name	= $rs->Fields( "service_name" );
				$serial_number		= $rs->fields( "serial_number" );
				$doc_certificat			= $rs->fields( "doc_certificat" );
				$term_date			= $rs->fields( "term_date" );
			$service_extern	= $rs->Fields( "service_extern" );
				$designation	= $rs->Fields( "designation" );
				$relaunch_date	= $rs->Fields( "relaunch_date" );
				
					
					
$dates =  date("Y-m-d");
$Nombres_jours =  NbJours($dates,$term_date);

					
					if( $term_date == "0000-00-00" )
					$term_date = "";
				
				
?>
											<tr class="blackText">
												<td class="lefterCol"><?php echo $idorder ?></td>    
												
												
												
                                                <td class="lefterCol"><?php echo $reference ?></td>  
                                                    <td class="lefterCol">
												   <a href="#" onClick="realiserservice(<?php echo $idproduct_term; ?>,<?php echo $service_extern; ?> )"><?php if ($service_extern == 1 ) {?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/ok.png" alt="oui" width="20px" height="20px" /><?php }else{  ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/non.png" alt="Non" width="20px" height="20px" /><?php } ?></a>
												   
												</td>   
                                                    <td class="lefterCol"><?php echo $designation ?></td>
											<td style="white-space:nowrap;">
												
														<input id="service_<?php echo $idproduct_term; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $service_name ); ?>" onKeyUp="" />
														<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onClick="updateservice(<?php echo $idproduct_term; ?>, $('#service_<?php echo $idproduct_term; ?>').val() );" />
														
												</td>
												<td style="white-space:nowrap;">
												
														<input id="serial_<?php echo $idproduct_term; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $serial_number ); ?>" onKeyUp="" />
														<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onClick="updateserial(<?php echo $idproduct_term; ?>, $('#serial_<?php echo $idproduct_term; ?>').val() );" />
														
												</td>
												 <td class="lefterCol">
                                                  <form action="<?php echo $GLOBAL_START_URL ?>/sales_force/product_term_list.php" method="post" name="adm_estim" name="adm_estim" enctype="multipart/form-data" ><input type="hidden" name="uploader" id="uploader" value="1" />
                                                 <input type="file" class="" name="documents" id="documents" value="" />
                                                <input type="submit" class="blueButton" value="Rechercher" />  </form>
                                                <br>
                                                 <br>
                                           <a href="/data/certificat/<?php echo  $idproduct_term ?>/<?php echo  $doc_certificat ?>">    <?php echo  $doc_certificat ?></a>
                                                 </td>
												<td style="white-space:nowrap;">
															<input size="10" onKeyUp="if( !this.value.length || this.value.length == 10 ) SetDate( this );" type="text" name="availability_date_<?php echo $idproduct_term ?>" id="availability_date_<?php echo $idproduct_term ?>" value="<?php echo usDate2eu( $term_date ) ?>" class="calendarInput" /><?php echo DHTMLCalendar::calendar( "availability_date_$idproduct_term", true, "SetDate" ) ?>
														</td>
														 <td class="lefterCol">
                                                           <?php if ($service_extern == 1 ) {?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton gris.png" alt="gris" width="20px" height="20px" /><?php }else{  if (($Nombres_jours < 30) && ($Nombres_jours > 2) ){ ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton jaune.png" alt="jaune" width="20px" height="20px" /><?php } 
														 if ($Nombres_jours == 1 ){ ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton vert.png" alt="vert" width="20px" height="20px" />
                                                      <?php    }
														   if ($Nombres_jours < 0 ){  ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton rouge.jpg" alt="rouge" width="20px" height="20px" />
														   <?php
														   }
														   
														   } ?>
                                                         </td>
                                                          <td> <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailbox-inactive.png" alt="oui" width="20px" height="20px" onClick="SendEmail(<?php echo $idproduct_term; ?>,<?php echo $idbuyer; ?> )"/> <br><?php echo  $relaunch_date ?></td>
                                             <!--   <td id="celldelete_<?php echo $idproduct_term ?>"><input type="image" name="DeleteOrder" id="DeleteOrder" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="Supprimer" onclick="return confirmDeleteOrder( <?php echo $idproduct_term ?>);" /></td>-->
											</tr>

				
<?php
				
				$rs->MoveNext();
				
			}
			
?>
</tbody>
                    	</table>
                        <div class="spacer"></div>
               		
	<div style="clear:both;"></div>
	<?php
			
		}else{ //pas de résultat
			
?>
				<div class="blocResult mainContent">
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Aucun résultat ne correspond à votre recherche</p>
					</div>
				</div>
<?php
		
		}
		
		paginate();

		//-------------------------------------------------------------------------------------------
		
		$lang = User::getInstance()->getLang();
		
		$nbtotal = 0;
		$totalorder = 0.0;
		
		//Commandes en attente

		$nbstandby=0;
		$totalstandby=0;
		
		//Commandes confirmées

		$nbconfirmed=0;
		$totalconfirmed=0;
		
		//Commandes envoyées

		$nbsent=0;
		$totalsent=0;
		
		//Commandes expédiées par le fournisseur

		$nbdispatch=0;
		$totaldispatch=0;
		
		//Commandes réceptionnées

		$nbdelivered=0;
		$totaldelivered=0;
		
		// Factures receptionnées
		$nbBillingRecept=0;
		$totalBillingRecept=0;
		
		//commandes annulées
		$cancelled = 0;
		
		$rs->MoveFirst();
		while( !$rs->EOF() ){
		
			switch( $rs->fields( "order_supplier_status" ) ){
			
				case "standby" : //Commandes en attente
				
					$nbstandby++;
					$totalstandby += $rs->Fields("total_amount_ht");
					
					break;
				
				case "confirmed" : //Commandes confirmées
				
					$nbconfirmed++;
					$totalconfirmed += $rs->Fields("total_amount_ht");
					
					break;

				case "SendByMail" : //Commandes envoyées
				case "SendByFax" :
				
					$nbsent++;
					$totalsent += $rs->Fields("total_amount_ht");
					
					break;
			
				case "dispatch" ://Commandes expédiées
	
					$nbdispatch++;
					$totaldispatch += $rs->Fields("total_amount_ht");
					
					break;
					
				case "received" ://Commandes réceptionnées
	
					$nbdelivered++;
					$totaldelivered += $rs->Fields("total_amount_ht");
					
					break;
				
				case "invoiced" ://Commandes facturée
					$nbdelivered++;
					$nbBillingRecept++;
					$totaldelivered += $rs->Fields("total_amount_ht");
					
					break;
				
				case "cancelled" : $cancelled++; break; //commande annulée
					
			}
			
			$nbtotal++;
			
			$rs->MoveNext();
			
		}

		$totalorder = $totalstandby + $totalconfirmed + $totalsent + $totaldispatch + $totaldelivered + $totalBillingRecept ;
		
		if( $nbtotal > 0 && $totalorder > 0 ){
			
			//Ration commandé/total
			$ratio_nb = ( $nbstandby + $nbconfirmed + $nbsent + $nbdelivered ) * 100 / $nbtotal;
			$ratio_order = ( $totalstandby + $totalconfirmed + $totalsent + $totaldelivered ) * 100 / $totalorder;
			
			//Commande moyenne
			$avg = $totalorder / $nbtotal;
			
		}else{
			
			$ratio_nb = 0;
			$ratio_order = 0;
			$avg = 0;
			
		}
		
		//-------------------------------------------------------------------------------------------
		
	
				
}	
		
	
	

	
?>
               	</form>
            </div>
<a name="bottom"></a>
</div>
<div class="spacer"></div><br/>
</div>       		      	
<div class="spacer"></div>   
		


			
		
<?php


//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/sales_force" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onClick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onClick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/sales_force" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

function ModifyDate(){
	global $GLOBAL_START_PATH;
	
	
	$idorders = $_GET[ "idos" ];
	$availability_date = $_GET[ "av" ];
		
	//$dispatch_date = $_GET[ "disp" ];
	
	//$Order = new SupplierOrder( $idorders );
	
	$regs = array();
	$pattern = "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/";
	
	if( preg_match( $pattern, $availability_date, $regs ) !== false ){
		
		$day = $regs[ 1 ];
		$month = $regs[ 2 ];
		$year = $regs[ 3 ];
		
		$availability_date = $year."-".$month."-".$day;
		
	}elseif( !empty( $availability_date ) ){
		
		echo "wrong av date format";
		return;
		
	}
	
	
	

			
	$qSuivi = "
		UPDATE product_term 
		SET term_date = '$availability_date' 
		
		WHERE idproduct_term = '$idorders'
		LIMIT 1"
	;
	
		$rsSuivi =& DBUtil::query( $qSuivi );
	
	
		if($rsSuivi === false){
			trigger_error( "Impossible de mettre à jour la date!", E_USER_ERROR );
			echo "wrong sql";
		return;
		}
	
	
 	echo "Confirmée";
	
}
 	if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "documents" ){
	$name_file = $_POST[ "documents" ];
		include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name_file . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			$idorder =  $_POST[ "idorder" ] ;
			$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./product_term_list.php\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term;
	
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	
	//exit( $ret === false ? "" : "1" );	
	}
		
}

function NbJours($debut, $fin) {

  $tDeb = explode("-", $debut);
  $tFin = explode("-", $fin);

  $diff = mktime(0, 0, 0, $tFin[1], $tFin[2], $tFin[0]) - 
          mktime(0, 0, 0, $tDeb[1], $tDeb[2], $tDeb[0]);
  
  return(($diff / 86400)+1);

}
	if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "documents" ){
	global $GLOBAL_START_PATH;
	$name_file = $_POST[ "documents" ];
		include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name_file . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			$idorder =  $_POST[ "idorder" ] ;
			$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./product_term_list.php\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term;
	
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	
	//exit( $ret === false ? "" : "1" );	
	}
		
}

//--------------------------------------------------------------------------------------------------

?>
           
                    <div class="spacer"></div>
						</div>
					
				
				</div></div>
				<div class="spacer"></div>
								
				<!--<div class="blocSearch"><div class="blocMarge">
					<!--<div class="blocMultiple blocMLeft" style="width:100%; margin:0;">
						<h2 style="padding:0 0 10px; margin-top:0;">
							<span class="floatleft" style="margin:9px 0 0 0;">Vérification en ligne</span>
							<a href="http://www.pagesjaunes.fr/" target="_blank" class="floatright marginLeftBig">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-pages-jaunes_g.jpg" alt="" width="48" height="25" /></a>
							<a href="http://www.ctqui.com" target="_blank" class="floatright marginLeftBig">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-ctqui_g.jpg" alt="" width="48" height="25" /></a>
							<a href="http://www.societe.com/" target="_blank" class="floatright marginLeftBig">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
							<a href="http://www.manageo.fr/" target="_blank" class="floatright marginLeftBig">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
							<a href="http://www.euridile.com/" target="_blank" class="floatright marginLeftBig">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-euridile_g.jpg" alt="" width="48" height="25" /></a>
							<?php if(isset($key) && !empty($raison)){
								$searchGoogle = str_replace(" ","+", $raison); ?>
									<a href="http://www.google.fr/search?hl=fr&q=<?php echo $searchGoogle ?>" onClick="window.open(this.href); return false;" class="floatright marginLeftBig">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-google_g.jpg" alt="" width="48" height="25" /></a>
							<?php } ?>
							<div class="spacer"></div>	
						</h2>
						
					</div>-->
					
					<div class="spacer"></div>	
					
					<div class="blocMultiple blocMLeft" id="contenerSearchManageo1">
					 	<!--<label id="txtSearchCompany"><span>Société & départ.</span></label>	-->				 	
					 	<input class="textInput" style="width:50%;" type="hidden" name="nameOfCompany" id ="nameOfCompany" value=""/>
						<input class="textInput" style="width:19%; margin:3px 0 0 10px;" type="hidden" maxlength="2" size="1" name="departmentCompany" id="departmentCompany" /><br />
						<div class="spacer"></div>				
					</div>
					<div class="blocMultiple blocMRight" id="contenerSearchManageo2">	
						<!--<label id="txtSearchCompany"><span>Ou siren société</span></label>	-->					
						<input class="textidInput" type="hidden" name="sirenOfCompany" id ="sirenOfCompany" value="" style="width:50%;"/>
						
					<!--	<input type="button" id="ButtonSearch" value="Rechercher" class="blueButton" onClick="javascript: getInfos();" style="margin:4px 0 0 2px;" />-->
						<div class="spacer"></div>	
					</div>
					
					<!-- Annuaire Mairie -->
					
					<div class="blocMultiple blocMLeft" id="contenerSearchMairie">
					 <!--	<label id="txtSearchMairie"><span>Nom de la commune</span></label>	-->				 	
					 	<input class="textInput" style="width:50%;" type="hidden" name="nameOfCommune" id ="nameOfCommune" value=""/>
					<!--	<input type="button" id="ButtonSearchCommune" value="Rechercher" class="blueButton" onClick="javascript: getInfosCommunes();" style="margin:4px 0 0 2px;" />-->
						<div class="spacer"></div>				
					</div>

										
					<!-- Fin annuaire Mairie -->
					<div class="spacer"></div>	
					
					<div id="contener_manageo" class="displayNone">
						<script type="text/javascript" src="/js/array_prototype_extended.js"></script>
						<script type="text/javascript">
						/* <![CDATA[ */

							function manageoSortResults( key, asc ){ //key = name, zipcode, city ou type

								var results = new Array();

								$( "div[id^='ManageoResult']" ).each( function( i ){

									results[ $( this ).attr( "id" ) ] = $( this ).find( "#" + $( this ).attr( "id" ) + "_" + key ).eq( 0 ).html();
									
								});

								if( asc )
										results.sortStringAsc();
								else 	results.sortStringDesc();
								
								var html = "";
								for( key in results )
									html += $( "#" + key ).html();
						
								$( "#infosDeOufs" ).html( html );
								
							}
							
						/* ]]> */
						</script>
											
					</div>
					
					<!--
					
					<div class="spacer"></div>
					<!-- Fin masquage  -->
				<!--</div>-->
                <div style="display:none !important" id="contener_communes" class="displayNone">
						<div style="width:100%" id="infosDeOufsCommune"></div>								
					</div>
                <div style="display:none !important ;width:100%" id="infosDeOufs"></div>	
                </div>
				
				<div class="spacer"></div>
				
			</div> <!-- Fin #container -->
				
	</div> <!-- Fin Bloc formulaire -->
</div> <!-- Fin  centerMax -->



<script type="text/javascript" language="javascript">
function loadGestContact( idbuyer ){
	
	var divToComplete = document.getElementById('gestContactsContent') ;

	var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php' ;
	var data = 'buyer=' + idbuyer ;
	var message = 'Chargement en cours ...';
	machineAjax( location, divToComplete, data, message );
}
function machineAjax( location, divToComplete, data, message ){
	
	divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /><br />' + message + '</div>';
	
	var xhr_object = null;
	if(window.XMLHttpRequest){ // Firefox
	   xhr_object = new XMLHttpRequest();
		if (xhr_object.overrideMimeType) {
			xhr_object.overrideMimeType('application/x-www-form-urlencoded; charset=utf-8');
		}
	}else if(window.ActiveXObject) // Internet Explorer
	   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
	else { // XMLHttpRequest non supporté par le navigateur
	   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
	   return;
	}
	xhr_object.onreadystatechange = function()
	{
	if(xhr_object.readyState == 4)
	{
		 // ON CONTROLE LE STATUS (ERREUR 404, ETC)
		if(xhr_object.status == 200)
			divToComplete.innerHTML = '<div style="padding:5px;">' + xhr_object.responseText + '</div>' ;
		else
			divToComplete.innerHTML = '<div style="text-align:center;padding:5px;">Erreur : ' + xhr_object.status +' </div>' ;
		}
	}; 
	
	xhr_object.open("POST", location, true);
	// ne pas oublier ça pour le post
	xhr_object.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
	xhr_object.send( data );
}
function autoSearchOnManageo(){
		
		var divToComplete = document.getElementById('infosDeOufs') ;
		var companyName = document.getElementById('Company').value ;
		document.getElementById('nameOfCompany').value = companyName;
		var deptCompany = document.getElementById('Zipcode').value.substring(0,2) ;
		document.getElementById('departmentCompany').value = deptCompany;

		var sirenToManageo = document.getElementById('siren').value.replace( new RegExp( "[^0-9]*", "g" ), "" );
		document.getElementById('sirenOfCompany').value = sirenToManageo;
		
		if( companyName == '' && sirenToManageo == ''){
			return;
		}
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php';
		if( sirenToManageo != ''){
			var data = 'sirenManageo=' + sirenToManageo ;
		}else{
			if(deptCompany != ""){
				var data = 'companyName=' + companyName + '&dept='+ deptCompany ;
			}
			else{
				var data = 'companyName=' + companyName ;
			}
			
		}
		message = '';
		
		machineAjax( location, divToComplete, data, message );
}
<!--
//	initRequiredElements();
	<?php if(isset($key)){ ?>
        
        autoSearchOnManageo();
		loadGestContact('<?php echo $key ?>');
	<?php }	?>
// -->
</script>
</form>


<?php

	//restaurer le formulaire dans son état précédent
	
	if( isset( $_POST ) && count( $_POST ) ){

		?>
		<script type="text/javascript">
		/* <![CDATA[ */
		<?php
		
			foreach( $_POST as $key => $value ){
				
				if( !is_array( $value ) ){
					
					$value = stripslashes( $value );
					$value = str_replace( "\r\n", "\\n", $value );
					$value = str_replace( "\n", "\\n", $value );
					$value = str_replace( "'", "\'", $value );
					
					echo "\nresetFormElementValue( 'frm', '$key', '$value' );";
					
				}
				else{

					array_map( "stripslashes", $value );
					
					$i = 0;
					while( $i < count( $value ) ){
						 
						$value[ $i ] = str_replace( "\r\n", "\\n", $value[ $i ] );
						$value[ $i ] = str_replace( "\n", "\\n", $value[ $i ] );
						$value[ $i ] = str_replace( "'", "\'", $value[ $i ] );
					
						$i++;
						
					}
					
					echo "\nresetFormCollectionValue( 'frm', '{$key}[]', new Array('" . implode( "','", $value ) . "') );";
					
				}
		
			}
		
		?>
		/* ]]> */
		</script>
		<?php
	
	}

/* ---------------------------------------------------------------------------------------------------- */
	
function listSubsidiaryCompanies( $idbuyer ){
	
	$query = "
	SELECT b2.*, c.*, u.initial
	FROM buyer b1, buyer b2, contact c, user u
	WHERE b1.idbuyer = '$idbuyer'
	-- AND ( 
	-- 	( b2.siren REGEXP('^[0-9]{9}\$') AND b2.siren = b1.siren )
	-- 	OR ( b2.siren REGEXP('^[0-9]{9}\$') AND b2.siren = SUBSTR( b1.siret, 1, 9 ) )
	-- 	OR ( b2.siret REGEXP('^[0-9]{14}\$') AND SUBSTR( b2.siret, 1, 9 ) = b1.siren )
	-- 	OR ( b2.siret REGEXP('^[0-9]{14}\$') AND SUBSTR( b2.siret, 1, 9 ) = SUBSTR( b1.siret, 1, 9 ) )
	-- )
	AND b2.siret REGEXP('^[0-9]{14}\$') AND SUBSTR( b2.siret, 1, 9 ) = SUBSTR( b1.siret, 1, 9 )
	AND b2.idbuyer <> b1.idbuyer
	AND c.idbuyer = b2.idbuyer
	AND c.idcontact = 0
	AND u.iduser = b2.iduser
	ORDER BY b2.idbuyer DESC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return;
		
	?>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	       
		$( document ).ready( function(){ 
				
            $( "#SubsidiaryCompanies" ).tablesorter({
		      	
		      	headers: { 
		
					0: {  sorter:'text' },
					1: {  sorter:'digit' },
					2: {  sorter:'digit' },
					3: {  sorter:'text' },
					4: {  sorter:'text' },
					5: {  sorter:'digit' },
					6: {  sorter:'digit' },
					7: {  sorter:'currency' }
		      		
				} 
					        
			});

		});
		
	/* ]]> */
	</script>
	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    border-right: 1px solid #dad9c7; 
		    padding-bottom:15px;
		
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 
		
	</style>
	<div class="blocMultiple blocMLeft" style="width:100%;">
		<h2>Etablissements connus</h2>
		<table id="SubsidiaryCompanies" class="dataTable resultTable sortable">
			<thead>
				<tr>
					<th>Com</th>
					<th>Client n°</th>
					<th>Dép</th>
					<th>Raison sociale</th>
					<th>Type</th>
					<th>Nombre de devis</th>
					<th>Nombre de commandes</th>
					<th>CA HT</th>
				</tr>
			</thead>
			<tbody>	
			<?php
			
				$estimateCountSum 	= 0;
				$orderCountSum 		= 0;
				$total_amount_ht 	= 0.0;
				
				while( !$rs->EOF() ){
					
					$rsOrder 			= DBUtil::query( "SELECT COUNT(idorder) AS `count`, SUM(total_amount_ht) AS total_amount_ht FROM `order` WHERE idbuyer = '" . $rs->fields( "idbuyer" ) . "'" );
					$orderCount 		= $rsOrder->fields( "count" );
					$total_amount_ht 	= $rsOrder->fields( "total_amount_ht" );
					$estimateCount 		= DBUtil::query( "SELECT COUNT(*) AS `count` FROM estimate WHERE idbuyer = '" . $rs->fields( "idbuyer" ) . "'" )->fields( "count" );
				
				
					?>
					<tr style="cursor:pointer;" onClick="window.open('/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>');">
						<td class="lefterCol"><?php echo $rs->fields( "initial" ); ?></td>
						<td><?php echo $rs->fields( "idbuyer" ); ?></td>
						<td><?php echo $rs->fields( "zipcode" ); ?></td>
						<td><?php echo htmlentities( $rs->fields( "company" ) ); ?></td>
						<td><?php echo $orderCount ? "Client" : "Prospect"; ?></td>
						<td style="text-align:right;"><?php echo $estimateCount; ?></td>
						<td style="text-align:right;"><?php echo $orderCount; ?></td>
						<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_htSum ); ?></td>
					</tr>
					<?php
					
					$estimateCountSum 	+= $estimateCount;
					$orderCountSum 		+= $orderCount;
					$total_amount_htSum += $total_amont_ht;
				
					$rs->MoveNext();
						
				}
				
			?>
			</tbody>
			<tr style="font-weight:bold;">
				<td class="lefterCol" colspan="5"></td>
				<td style="text-align:right;"><?php echo $estimateCount; ?></td>
				<td style="text-align:right;"><?php echo $orderCount; ?></td>
				<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_htSum ); ?></td>
			</tr>
		</table>
	</div> <!-- Fin blocMultiple -->
	<br style="clear:both;" />	
	<?php
					
}

/* ---------------------------------------------------------------------------------------------------- */

	
function SendRelaunch(  ){

	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php" );
	$mail = true;
	 $date = date("Y-m-d" ,mktime(date("H"),date("i"),date("s"),date("m")+1,date("d"),date("Y")));
	  $idproduct_term = $_GET[ "idproduct_term" ];
	   $date = date("Y-m-d");
	  // print $date;exit;
	//  $idproduct_term = $_GET[ "idproduct_term" ];
	/*$qr = "
		UPDATE product_term SET
		relaunch_date = '$date'
		
		WHERE 
		idproduct_term = $idproduct_term
		
		";
	
	$rq =& DBUtil::query( $qr );
	*/
	$q1 = "
			SELECT *
			FROM product_term
			WHERE 
			idproduct_term = $idproduct_term
			";
	$rs1 =& DBUtil::query( $q1 );
	
	$idord = $rs1->fields( "idorder" );
	
	$query = "
		SELECT pt.idorder,
		pt.idbuyer,
		pt.reference,
		pt.term_date,
		pt.idproduct_term,
		ord.iduser,
		orw.idsupplier
		FROM 
		product_term pt ,`order` ord, order_row orw
		WHERE 
		pt.idproduct_term = $idproduct_term
		AND pt.service_extern = 0
		AND pt.idorder = $idord
		AND ord.idorder = pt.idorder
		AND orw.idorder = pt.idorder
		AND pt.reference = orw.reference
		
		
		";
	
	$rs =& DBUtil::query( $query );
	$resultCount = $rs->RecordCount();
	if( $rs === false ){
	echo "impossible relancer";
		return;
	}
	$idbuyer			= $rs->fields( "idbuyer" );
	$idorder			= $rs->fields( "idorder" );
	$reference			= $rs->fields( "reference" );
	$term_date			= $rs->fields( "term_date" );
	$idproduct_term		= $rs->fields( "idproduct_term" );
	
	$iduser		= $rs->fields( "iduser" );
	$idsupplier		= $rs->fields( "idsupplier" );
	
 $idcontact = 0;
	$idbilling = 10003;
	//include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/MailTemplateEditor.php" );
	
	
	
	
	
	
	$editor = new MailTemplateEditor();
	
	
	//----------------------------------------------------------Choix du modèle de mail d'échéance à envoyer---------------------------------------------------//	
	$ref = $rs1->fields( "reference" );  
	
	$qr2 = "
			SELECT d.reference,d.idproduct,p.idcategory
			FROM `detail` d INNER JOIN `product` p ON  d.idproduct = p.idproduct
			WHERE 
			d.reference = '$ref'
			";
	$res2 =& DBUtil::query( $qr2 );
	
	$idcat = $res2->fields( "idcategory" );
	
	$qr3 = "
			SELECT idcategoryparent,idcategorychild
			FROM `category_link`
			WHERE 
			idcategorychild = '$idcat'
			";
	$res3 =& DBUtil::query( $qr3 );
	
	$idcatparent = $res3->fields( "idcategoryparent" );
	$idcatchild = $res3->fields( "idcategorychild" );
	
	$tab = array('1004','1001','10508','1701');

	while ( !in_array ($idcatchild, $tab) || $idcatparent != 0 )
	{
	
	$qr4 = "
			SELECT idcategoryparent,idcategorychild
			FROM `category_link`
			WHERE 
			idcategorychild = '$idcatparent'
			";
	$res4 =& DBUtil::query( $qr4 );
	
	$idcatparent = $res4->fields( "idcategoryparent" );
	$idcatchild = $res4->fields( "idcategorychild" );
	
	}
	
	$idcategory = $idcatchild;
	
	$template = 'ECHEANCIER';
	$temp = $template.$idcategory;
	$editor->setLanguage( substr( User::getInstance()->getLang(), 1 ) ); //@todo : imposer la langue du destinataire
	
	$qr5 = "
			SELECT identifier
			FROM `mail_template`
			WHERE 
			identifier = '$temp'
			";
	$res5 =& DBUtil::query( $qr5 );
	
	$resultCount = $res5->RecordCount();
	
	if ( $resultCount > 0 )
		{
		$editor->setTemplate( $temp );
		}
	else
		{
		$editor->setTemplate( $template );
		}
	
	//----------------------------------------------------------Fin choix du modèle de mail d'échéance à envoyer---------------------------------------------------//
	
		
		
	/*$editor->setUseOrderTags( $idorder );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();*/
	
	
	$editor->setUseAdminTags(); 
$editor->setUseCommercialTags($iduser);
$editor->setUseUtilTags(); 	
$editor->setUseBuyerTags($idbuyer,$idcontact); 
$editor->setUseecheanceTags($idproduct_term	); 
$editor->setUseSupplierTags($idsupplier); 
	//////////////////
	
	$Subject = $editor->unTagSubject();	
	
			
		
		$Message = $editor->unTagBody();
	
	$AdminName = DBUtil::getParameterAdmin( "ad_name" );
	
	$mailer = new htmlMimeMail();
	
	/*if( $mail == false ){
		
		$smtp_server	= DBUtil::getParameterAdmin( "fax_smtp_server" );
		$smtp_sender	= DBUtil::getParameterAdmin( "fax_smtp_sender" );
		$smtp_auth		= DBUtil::getParameterAdmin( "fax_smtp_auth" );
		$smtp_user		= DBUtil::getParameterAdmin( "fax_smtp_user" );
		$smtp_password	= DBUtil::getParameterAdmin( "fax_smtp_password" );
		$smtp_recipient	= DBUtil::getParameterAdmin( "fax_smtp_recipient" );
		$AdminName		= DBUtil::getParameterAdmin( "ad_name" );
		
		$mailer->setSMTPParams( $smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password );
		
	}*/
	
//	if( $mail == true )
		
	
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );




	/*$queryb = "SELECT orw.idorder,ord.idbuyer
			FROM   order_row orw, `order` ord 
			WHERE orw.reference = '$reference'
			AND orw.idorder = ord.idorder
		
			";
	$rsb =& DBUtil::query( $queryb );
	while( !$rsb->EOF() ){
				
				$idorders = $rsb->fields("idorder");
				$idbuyer = $rsb->fields("idbuyer");
				
			*/
				// ------- Mise à jour de l'id gestion des Index  #1161
	$queryc = "SELECT sc.mail, sc.firstname, sc.lastname
			FROM contact sc
			WHERE sc.idbuyer = '$idbuyer'
			
			";
	$rsc =& DBUtil::query( $queryc );
	
	if($rsc===false)
		{
	echo "aucun mail";
		return;
	}
	//while( !$rsc->EOF() ){
				
	$email_business = $rsc->fields("mail");
	//$firstname_business = $rsc->fields("firstname");
	//$lastname_business = $rsc->fields("lastname");
	
			$receipt = array( $email );
			
	
		$mailer->setFrom('"'.$AdminName.'"');
$mailer->setBcc( $email_business );
	$mailer->setBcc( $email_business );
	
	
	// $mailer->send(  );
	
	
		 $mailer->send( $receipt );
	

	/*	$rsb->MoveNext();
				
			} 
	$rsc->MoveNext();
				
			}*/
			
	echo "confirmé";
		return;
}

//--------------------------------------------------------------------------------------------------


?>
