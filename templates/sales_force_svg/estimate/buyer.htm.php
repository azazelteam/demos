<?php

/* ----------------------------------------------------------------------------------------------------------- */
/* assurance - liste des pièces jointes */

if( isset( $_REQUEST[ "list" ] ) && $_REQUEST[ "list" ] == "insurance_attachments" ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	listInsuranceAttachments( intval( $_REQUEST[ "idbuyer" ] ) );
	
	exit();
	
}

/* ----------------------------------------------------------------------------------------------------------- */

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/sales_force/buyer_history.php" );

$customer =& $Order->getCustomer();

?>
<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE BUYER ++++++++++++++++++++++++++++++++++++++++++ -->
<script type="text/javascript">
<!--
function show_refus() {
	
	var refusal_value = document.getElementById( "cstatus" ).value;
	
	if(refusal_value=='Refused'){ 
		
		document.getElementById('refusaldiv').style.display = 'inline';
		document.getElementById( "chstat" ).elements['refusal'].style.display = 'inline';
		
	}else{

		document.getElementById('refusaldiv').style.display = 'none';
		document.getElementById( "chstat" ).elements['refusal'].style.display = 'none';
		
	}
}

function testrefus() {
	var refusal_value=document.getElementById( "chstat" ).elements['cstatus'].options[document.getElementById( "chstat" ).elements['cstatus'].selectedIndex].value;
	
	if(refusal_value=='Refused'){ 
		var motif=document.getElementById( "chstat" ).elements['refusal'].options[document.getElementById( "chstat" ).elements['refusal'].selectedIndex].value;
	
		if(motif==0){
			alert("Vous ne pouvez malheureusement pas modifier le statut car vous avez omis de sélectionner un motif de refus. Il vous prie de bien vouloir en sélectionner un. Merci de votre compréhension. Pour plus d'informations contactez Akilaé.");
			return false ;
		}else{
			return true ;
		}
	}
}

function showTodoInfos(){
	
	var tododiv = document.getElementById( 'TodoDiv' );
	var relaunchdiv = document.getElementById( 'RelaunchDiv' );
	tododiv.style.display = tododiv.style.display == 'block' ? 'none' : 'block';
	relaunchdiv.style.display = relaunchdiv.style.display == 'block' ? 'none' : 'block';

}

function showFilesInfos(){
	
	var filesdiv = document.getElementById( 'FilesDiv' );
	filesdiv.style.display = filesdiv.style.display == 'block' ? 'none' : 'block';

}

// PopUp pour voir les relances du client
function seeRelaunch(){
	postop = (self.screen.height-600)/2;
	posleft = (self.screen.width-750)/2;
	window.open('<?php echo $GLOBAL_START_URL ?>/accounting/buyer_relaunch.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
}

function visibility(thingId, txtAff, txtMasque) {
	var targetElement; 
	var targetElementLink;
	targetElement = document.getElementById(thingId);
	targetElementLink = document.getElementById(thingId+'Link');
	if (targetElement.style.display == "none") {
		targetElement.style.display = "";
		targetElementLink.innerHTML = txtMasque;
	} else {
		targetElement.style.display = "none";
		targetElementLink.innerHTML = txtAff;
	}
}

// PopUp pour voir l'historique d'un client
function seeBuyerHisto(){
	
	postop = (self.screen.height-600)/2;
	posleft = (self.screen.width-1010)/2;
	window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0');
	
}

// -->
</script>
<?php

anchor( "StatusChanged" );
anchor( "ComChanged" );
anchor( "StatusChanged" );
anchor( "UpdateTodoMessage" );
	
?>


<!-- Bloc formulaire -->
<div class="contentResult" style="margin-bottom: 10px;">


	<h1 class="titleEstimate">
	<span class="textTitle">
		<a title="Créer un devis" href="<?php echo URLFactory::getHostURL(); ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>&idcontact=0" onclick="window.open(this.href); return false;">
			<img src="/images/back_office/content/icons-create.gif" alt="Créer un devis" />
		</a>
		<a title="Dupliquer le devis" href="<?php echo URLFactory::getHostURL(); ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate; ?>&amp;editcopy=ok" onclick="window.open(this.href); return false;">
			<img src="/images/back_office/content/editcopy.png" alt="Dupliquer le devis" />
		</a>
		Devis n°<?php echo $IdEstimate ?> 
		- <?php echo $Order->getCustomer()->get( "contact" ) ? "Prospect" : "Client"; ?> <?php echo $Order->getCustomer()->get('company'); ?>
		- <?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" )) ;  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ); ?> <?php echo $Order->getCustomer()->getContact()->get( "firstname" ); ?>
		<?php
		switch( $customer->get( "insurance_status" ) ){
			case "Refused":
				echo "<span class='msgError'> (Refusé à l'assurance crédit)</span>";
				break;
			
			case "Terminated":
				echo "<span class='msgError'> (Résilé à l'assurance crédit)</span>";
				break;
				
			case "Deleted":
				echo "<span class='msgError'> (Supprimé à l'assurance crédit)</span>";
				break;
				
		}
				
		?>
	</span>
	<span class="selectDate">
		<!-- Statut du devis -->
		<script type="text/javascript">
		/* <![CDATA[ */
		$(document).ready(function() {
			$('.editable').editable('<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>&StatusChanged=On', {
		 		data   : " {'ToDo':'<?php echo Dictionnary::translate("ToDo"); ?>', 'InProgress':'<?php echo Dictionnary::translate("InProgress"); ?>', 'Send':'<?php echo Dictionnary::translate("Send"); ?>', 'Refused':'<?php echo Dictionnary::translate("Refused"); ?>'}",
			    type   : 'select',
			    submit : 'OK',
			    name   : 'cstatus_name',
			    id     : 'cstatus',
			    style  : 'inherit',
			    callback : function(value, settings) {
			    	document.location.href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>"
			    }
			        
			});
		});		
		/* ]]> */
		</script>	
		
		<?php if( $Str_status != "Ordered" ){ ?>
			<p style="font-size:14px;"><strong>Statut : </strong>
			<?php 
				if($Str_status == 'ToDo') {
					$color = "#D81D21"; //Rouge
				} elseif($Str_status == 'Refused') {
					$color = "#F58D00"; //Orange
				} elseif($Str_status == 'Send') {
					$color = "#73C200"; //Vert
				} else {
					$color = "#1E9CD1"; //Bleu
				}
			?>
			<span class="editable" style="color:<?php echo $color; ?>">
			<?php 
				
				if( $Str_status == Estimate::$STATUS_REFUSED
					&& DBUtil::getDBValue( "idestimate", "logismarket_categories", "idestimate", $IdEstimate ) )
					echo "Rejeté";
				else echo Dictionnary::translate($Str_status); 
				
			?>
			</span></p> 
		<?php }else{ ?>
			<?php $associatedOrder = DBUtil::getDBValue( "idorder", "order", "idestimate", $IdEstimate ); ?>
			<p style="font-size:14px;"><span style="color:#73C200;">Commandé</span>, (<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $associatedOrder ?>">Accéder à la commande n° <?php echo $associatedOrder ?></a>)</p>
		<?php } ?>
		<div class="spacer"></div>
	</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>
	
	
	
	<div class="blocEstimateResult" style="margin:0;"><div style="margin:0 5px 5px 5px;">
		<div class="blocMLeft" style="margin-bottom:0px;">
			
			<p><strong>Crée le</strong> <?php echo humanReadableDatetime( $Order->get( "DateHeure" ) ); ?></p>
			<div class="spacer"></div>

			
			<div class="spacer"></div>
			<span style="color:red">iduser :<?php echo $iduser; ?></span>
			<!-- Suivi par -->
			<?php

			$rs = &DBUtil::query("select iduser, CONCAT( firstname, ' ', lastname ) AS label from user WHERE available IS TRUE AND hasbeen IS FALSE ORDER BY firstname ASC, lastname ASC"); 
			if($rs->RecordCount()>0)
			{
				for($i=0 ; $i < $rs->RecordCount() ; $i++)
				{
					$value = $rs->Fields('iduser');
					$view = $rs->Fields('label');
					$array[$value] = $view;
					$rs->MoveNext();
		        }
			}
			
			$principal = DBUtil::getDBValue( "admin_principal", "user", "iduser", User::getInstance()->getId() );
			
			?>
			<script type="text/javascript">
			/* <![CDATA[ */
			$(document).ready(function() {
				$('.editCom').editable('<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>&ComChanged=On', {
			 		data   : '<?php print json_encode($array); ?>',
				    type   : 'select',
				    submit : 'OK',
				    name   : 'iduser_name',
				    id     : 'iduser',
				    style  : 'inherit'     
				});
			});		
			/* ]]> */
			</script>	

			<p><strong>Devis suivi par </strong><span <?php if($principal == 1){?> class="editCom" <?php } ?>> <?php echo getCommercialFullName( $Order->get( "iduser" ) ) ?></span>
			<span style="font-size:9px;">(<strong>Client suivi par</strong> : <?php echo getCommercialFullName( $Order->getCustomer()->get( "iduser" ) ); ?>)</span>
			</p>
			<div class="spacer"></div>	
			
			
			
		</div>
		<div class="floatright">
			<div id="motifRefus" style="<?php if($Str_status == 'Refused'){ ?>display:block;<?php } else { ?>display:none;<?php } ?>">

				<?php
				
				$lang = User::getInstance()->getLang();	
				$rs = &DBUtil::query("SELECT idrefusal, refusal$lang FROM refusal ORDER BY refusal$lang ASC");
				if($rs->RecordCount()>0)
				{
					for($i=0 ; $i < $rs->RecordCount() ; $i++)
					{
						$value = $rs->Fields( "idrefusal" );
						$view = $rs->Fields( "refusal$lang" );
						$arrayv[$value] = $view;
						$rs->MoveNext();
			        }
				}
				?>		 		
				
			
				<script type="text/javascript">
				/* <![CDATA[ */
				$(document).ready(function() {
					$('.editRefuse').editable('<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>&RefusChanged=On', {
				 		data   : '<?php print json_encode($arrayv); ?>',
					    type   : 'select',
					    submit : 'OK',
					    name   : 'refusal_name',
					    id     : 'idrefusal',
					    style  : 'inherit'     
					});
				});		
				/* ]]> */
				</script>	
				
				<p style="text-align:right;"><strong>Motif de refus : </strong><span class="editRefuse"><?php echo GetRefuse($Order->get( "refusal" )); ?></span></p>

			</div>
						

			<div class="spacer"></div>	
		</div>
		<div class="spacer"></div>		

		
		<div class="spacer"></div>
		<?php if($relaunched){?><b><a href="#" class="blueLink" onclick="seeRelaunch(); return false;">!! Attention Mauvais Payeur !!</a></b><?php } ?>
        
        
        <script type="text/javascript">
		<!--
			//onglets
			$(function() {
				$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
			});
		-->
		</script>
         
        <div id="container">
         
	        <ul class="menu">
				<li class="item"><a href="#infos-client"><span><?php echo $Order->getCustomer()->get( "contact" ) ? "Prospect n°" : "Client n°"; ?><?php echo $Order->getCustomer()->get( "idbuyer" );  ?></span></a></li>
				<li class="item"><a href="#contact"><span>Autres contacts</span></a></li>
				
				<li class="item"><a href="#relances-client">
					<span>Relances</span></a>
					<?php if($Order->get( "todo" )) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
				<?php
					// ------- Mise à jour de l'id gestion des Index  #1161
					$rs =& DBUtil::query( "SELECT iddelivery, idbilling_adress FROM `estimate` WHERE idestimate = '$IdEstimate'"  );
	
					$deliveryAddress = $rs->fields( "iddelivery" ); 
					$billingAddress = $rs->fields( "idbilling_adress" );
				?>
				<li class="item"><a href="#adresses">
					<span>Adresses complémentaires</span></a>
					<?php if($deliveryAddress || $billingAddress) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
				<li class="item"><a href="#historique-client"><span>Historique</span></a></li>
				<li class="item"><a href="#visualiser-imprimer"><span>Visualiser et imprimer</span></a></li>
				
				<li class="item"><a href="#bloc-note">
					<span>Bloc-notes</span></a>
					<?php if($Order->get( "note" )) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
				<?php
					$supplier_offer = $Order->get( "supplier_offer" );
					$supplier_doc = $Order->get( "supplier_doc" );
					$offer_signed = $Order->get( "offer_signed" );
                    $buyer_doc = $Order->get( "buyer_doc" );
					$hasAttachments = !empty( $offer_signed) || !empty( $supplier_offer) || !empty( $supplier_doc) || !empty( $buyer_doc);
				?>    
				<li class="item"><a href="#pa-client">
					<span>Pièces archivées</span></a>
					<?php if($hasAttachments) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
				<!--<li class="item"><a href="#insurance">
					<span>Assurance du client</span></a>-->
				</li>
			</ul>
			<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0 8px -5px; width: 100%;"> </div>
			
			<?php $google_map_api_key = DBUtil::getParameterAdmin("google_map_api_key"); ?>
			<?php
				//Liste des fournisseurs				
				$i = 0; $idsuppliers = array();
				while( $i < $Order->getItemCount() ){
					$idsupplier = $Order->getItemAt($i)->get( "idsupplier" );
					if( !in_array($idsupplier, $idsuppliers) ) { $idsuppliers[] = $idsupplier; }
					$i++;
				}
				
				function getDistance($adresse1,$adresse2){	
					$adresse1 = str_replace(" ", "-", $adresse1);
					$adresse2 = str_replace(" ", "-", $adresse2);
					
					$url = 'http://maps.google.com/maps/api/directions/xml?language=fr&origin='.$adresse1.'&destination='.$adresse2.'&sensor=false';
					$xml = file_get_contents($url);
					$root = simplexml_load_string($xml);
					
					$distance=$root->route->leg->distance->text;
					$duree=$root->route->leg->duration->value;
					$etapes=$root->route->leg->step;
					
					return array(
						'url'=>$url,
						'distanceEnMetres'=>$distance,
						'dureeEnSecondes'=>$duree,
						'etapes'=>$etapes,
						'adresseDepart'=>$root->route->leg->start_address,
						'adresseArrivee'=>$root->route->leg->end_address
					);
					
					
				}
			?>
			
			<script type="text/javascript" src="http://maps.google.com/maps?file=api&amp;v=2&amp;key=<?php echo $google_map_api_key ?>"></script>
			<!--<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/mapiconmaker.js"></script>-->
			<script type="text/javascript">
			/* <![CDATA[ */
			
				/* Affiche la pop up d'info sur le fournisseur */
				function showMap() {
				
										
					$.blockUI({
						message: $('#geolocalisation'),
						fadeIn: 700, 
		        		fadeOut: 700,
						css: {
							width: '600px',	top: '0px', left: '50%', 'margin-left': '-300px', 'margin-top': '50px',
							padding: '5px', '-webkit-border-radius': '10px',  '-moz-border-radius': '10px',
			                'background-color': '#ECEAE5' 
						 }
					}); 
					
					initialize();	
										
					$('.blockOverlay').attr('title','Cliquez ici pour fermer la fenêtre').click(function() {
						$.unblockUI();
						return false;
					});						
	
				}

				
				function initialize() {
					if (GBrowserIsCompatible()) {
						
						<?php if( $Order->getCustomer()->get('zipcode') != "" ) { ?>
						var geoXml = new GGeoXml("<?php echo $GLOBAL_START_URL ?>/images/map/<?php echo substr($Order->getCustomer()->get('zipcode'), 0, 2) ?>.kml");
						<?php } ?>
						
						var maCarte = new GMap2(document.getElementById("EmplacementDeMaCarte"));
						maCarte.setCenter(new GLatLng(45, 3.450079), 5);
						maCarte.addControl(new GLargeMapControl());
						
						
						/* Affichage le fournisseur */
						<?php 
						$i = 0;
						while ($i < count($idsuppliers)) { 
							$zipcode_supplier =  DBUtil::getDBValue( "zipcode", "supplier", "idsupplier", $idsuppliers[$i] );
							$city_supplier =  DBUtil::getDBValue( "city", "supplier", "idsupplier", $idsuppliers[$i] );
							$adress_supplier =  DBUtil::getDBValue( "adress", "supplier", "idsupplier", $idsuppliers[$i] );
							$city_supplier =  DBUtil::getDBValue( "city", "supplier", "idsupplier", $idsuppliers[$i] );
						?>
							var adresse = "<?php echo str_replace(",", "", $adress_supplier) ?>, <?php echo $zipcode_supplier ?> <?php echo $city_supplier ?>";
						    var geocoder = new google.maps.ClientGeocoder();
						    var myIconRed = MapIconMaker.createMarkerIcon({width: 36, height: 32, primaryColor: 'da211c'});
						    geocoder.getLatLng(adresse, function (coord) {					
								var marker = new GMarker(coord, {icon: myIconRed});
								maCarte.addOverlay(marker);
							});
						<?php $i++; } ?>
						
						/* Flèche sur la ville du client */
						<?php if( $Order->getCustomer()->get('city') != "" ) { ?>
						var adresse = "<?php echo str_replace(",", "", $Order->getCustomer()->get('adress')) ?>, <?php echo $Order->getCustomer()->get('zipcode') ?> <?php echo $Order->getCustomer()->get('city') ?>";
					    var geocoder = new google.maps.ClientGeocoder();
					    var myIconGreen = MapIconMaker.createMarkerIcon({width: 36, height: 32, primaryColor: 'b0da1c'});
					   
					    geocoder.getLatLng(adresse, function (coord) {					
							var marker = new GMarker(coord, {icon: myIconGreen});
							maCarte.addOverlay(marker);
						});
						<?php } ?>
						
						/* Affichage de la région du client */
						<?php if( $Order->getCustomer()->get('zipcode') != "" && $Order->getCustomer()->get( "idstate" ) == "1"  ) { ?>
						maCarte.addOverlay(geoXml);	
						<?php } ?>				
					}
				}	
				
						
			/* ]]> */
			</script>
			
			<!-- *************** -->
			<!-- Geolocalisation -->
			<!-- *************** -->
	<!--	<style type="text/css">
				.forecast { width:135px; float:left; }
			</style>
			
			<div id="geolocalisation" style="display:none;">
				<div class="contentResult" style="margin-bottom: 10px; margin-left:10px;"><div class="blocResult">
				
				<div id="EmplacementDeMaCarte" style="width:560px; height:400px; border:1px solid #eee; margin:5px 10px; display:block; " class="blocMultiple blocMLeft">
					Carte de la france
				</div>
				<div class="spacer"></div>
				
				<div class="blocMultiple blocMLeft" style="margin:5px 10px; width:560px; ">
					<h2 style="text-align:left;"><img src="http://www.resto-asiat.com/upload/3126_picto_itineraire.jpg" /> Itinéraire Fournisseur &#0155; Client</h2>
					-->
					<?php
				/*								
					//Parcours des fournisseurs
					$i = 0;
					
					if( $Order->getCustomer()->get('city') != "" ) {
						$city_client = $Order->getCustomer()->get('city');	
					} else {
						$city_client = "";
					}
					?>
					
					<?php
					while ($i < count($idsuppliers)) {
					
						$name_supplier =  DBUtil::getDBValue( "name", "supplier", "idsupplier", $idsuppliers[$i] );
						$zipcode_supplier =  DBUtil::getDBValue( "zipcode", "supplier", "idsupplier", $idsuppliers[$i] );
						$city_supplier =  DBUtil::getDBValue( "city", "supplier", "idsupplier", $idsuppliers[$i] );
						
						$info_trajet = getDistance( $city_supplier, $Order->getCustomer()->get('city'));
						
						$distance_km = $info_trajet['distanceEnMetres'];
						$total = $info_trajet['dureeEnSecondes'];
						$heure = intval(abs($total / 3600));
						$total = $total - ($heure * 3600);
						$minute = intval(abs($total / 60));
						$total = $total - ($minute * 60);
						$duree =  $heure."heures et ".$minute." minutes"; ?>
					
						<p><strong>Fournisseur :</strong> <?php echo $name_supplier ?> - <?php echo $zipcode_supplier ?> <?php echo $city_supplier ?></p>
						<p><strong>Client :</strong> <?php echo $Order->getCustomer()->get('company'); ?> - <?php echo $Order->getCustomer()->get('zipcode'); ?> <?php echo $city_client ?></p>
						<p><strong>Distance à parcourir :</strong> <?php echo $distance_km ?></p>
						
					<?php $i++;	} ?>
					
					
					<?php
						require($GLOBAL_START_PATH.'/objects/GoogleWeather.php');
						if( $city_client != "" ) {
							
							try{
								
								$gweather = new GoogleWeatherAPI( str_replace("CEDEX", "", $city_client),'fr' );
								if($gweather->isFound()) {
								
									?>
									<h2 style="text-align:left;">Météo à <?php echo htmlentities( $gweather->getCity() ); ?></h2>
									<?php 
								
									foreach( $gweather->getForecast() as $temp) {
										echo '<div class="forecast">';
										echo '<p style="text-align:center;"><strong>'.$temp['day_of_week'].'</strong><br/>';
										echo '<img src="'.$temp['icon'].'"/><br/>';
										echo $temp['low'].' °C | '.$temp['high'].' °C</p>';
										echo '</div>';
									}
									
								} 
								else{ 
								
									?>
									<h2 style="text-align:left;">Météo à <?php echo $city_client ?></h2>
									<p>Météo non disponible.</p>
									<?php 
								
								}
							
							}
							catch( Exception $e ){
								
								?>
								<h2 style="text-align:left;">Météo à <?php echo $city_client ?></h2>
								<p>Météo non disponible.</p>
								<?php 
									
							}
							
						}
						*/
				?>
			<!--	</div>
				<div class="spacer"></div>
				</div></div>				
			</div>-->
			

			
			<!-- *********** -->
			<!--    Client   -->
			<!-- *********** -->
			<div id="infos-client">
			
				<div class="blocMultiple blocMultiple4">
			    	<div style="min-height:100px;">
			            <p><a href="#" onclick="showMap(); return false; ">
			            <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-map-earth.jpg" width="16" alt="Carte du monde" /></a> 
			            <strong><?php echo $Order->getCustomer()->get('company'); ?>
						<?php
							// ------- Mise à jour de l'id gestion des Index  #1161
							$hasLitigGraves = DBUtil::query("SELECT count(idbilling_buyer) as compte FROM billing_buyer WHERE litigious = 1 AND idbuyer = '$idbuyer'")->fields('compte');
							if($hasLitigGraves){
							    		echo "<img src=\"$GLOBAL_START_URL/images/back_office/content/litigation.png\" alt=\"Litige\" style=\"vertical-align:middle;\" /> <span style=\"color:red;\">ATTENTION CE CLIENT A UNE OU PLUSIEURS FACTURES IMPAYABLES</span>";
							}
						?>		
						(n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>)</strong>
						</p>
			            <p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ); ?> <?php echo $Order->getCustomer()->getContact()->get( "firstname" ); ?></p>
			            <p><?php echo $Order->getCustomer()->get('adress'); ?>
			            <?php $adr2 = "<p>".str_replace("&Atilde;&uml;", "è", $Order->getCustomer()->get('adress_2'))."</p>"; if(!empty($adr2)){ echo $adr2; } ?><?php
						//var_dump(str_replace("&Atilde;&uml;", "è", $Order->getCustomer()->get('adress_2')));
						?>
			            <p><strong><?php echo $Order->getCustomer()->get('zipcode'); ?> <?php echo $Order->getCustomer()->get('city'); ?></strong> - <?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ); ?></p>
			    	</div>
			        <br/>
		
						
		           
					<p><strong>Profil client: </strong><span><?php echo $Order->getCustomer()->get( "idcustomer_profile" ) ? DBUtil::getDBValue( "name", "customer_profile", "idcustomer_profile", $Order->getCustomer()->get( "idcustomer_profile" ) ) : "N/C"; ?></span></p>
		            <div class="spacer"></div>
									
		            
		            <!--<p><strong>Statut assurance</strong> : 
		            <?php		
						switch( $customer->get( "insurance_status" ) ){
							case "Accepted":
								echo "accepté";
								break;
								
							case "Refused":
								echo "refusé";
								break;

							case "Deleted":
								echo "supprimé";
								break;
								
							case "Terminated":
								echo "résilié";
								break;

							default:
								echo "non assuré";
								break;
						}				
					?></p>-->
			 <p><strong>Provenance client : </strong><?php echo DBUtil::getDBValue( "source_1", "source", "idsource", $Order->getCustomer()->get( "idsource" ) ); ?></p>
		            <div class="spacer"></div>
		            <script type="text/javascript">
						/* <![CDATA[ */
						$(document).ready(function() {
							$('.editUserProfil').editable('/sales_force/com_admin_devis.php?iduser=<?php echo $Order->getCustomer()->getId();  ?>&IdEstimate=<?php echo $Order->getId() ?>&editUserProfil=On', {
						 		data   : '<?php 
						 		
							 			$rs =& DBUtil::query( "SELECT * FROM customer_profile" );
							 			
							 			$profiles = array();
										while( !$rs->EOF() ){
											
											$profiles[ $rs->fields( "idcustomer_profile" ) ] = $rs->fields( "name" );
											$rs->MoveNext();
											
										}
										
							 			print json_encode( $profiles ); 
							 			
						 			?>',
							    type   : 'select',
							    submit : 'OK',
							    name   : 'idcustomer_profile',
							    id     : '',
							    style  : 'inherit'     
							});
						});		
						/* ]]> */
					</script>				            
			<p><strong>Provenance devis : </strong> 
			<select onchange="updateEstimate('idsource', this.options[this.selectedIndex].value);" onkeyup="this.onchange(event);" style="width: 50px;">
			<?php 

				$rs = DBUtil::query( "SELECT idsource, source_1 AS `source` FROM source WHERE available IS TRUE ORDER BY source_1 ASC" );
				while( !$rs->EOF() ){

					$selected = $Order->get( "idsource" ) == $rs->fields( "idsource" ) ? " selected=\"selected\"" : "";
					
					?>
					<option<?php echo $selected; ?> value="<?php echo $rs->fields( "idsource" ); ?>"><?php echo htmlentities( $rs->fields( "source" ) ); ?></option>
					<?php
					
					$rs->MoveNext();
					
				}
		
			?>
			</select>
			</p>
				</div>
				<div class="blocMultiple blocMultiple4">
					
					<div style="min-height:100px;">
						<table cellpadding="0" cellspacing="0" border="0">
				            <?php if(HumanReadablePhone($Order->getCustomer()->getContact()->get( "phonenumber" ))) { ?>
				            <tr>
				            	<td><p><strong>Tél. : </strong></p></td>
				            	<td><p><?php echo HumanReadablePhone($Order->getCustomer()->getContact()->get( "phonenumber" )); ?></p></td>
				            </tr>
				            <?php } ?>
				            <?php if(HumanReadablePhone($Order->getCustomer()->getContact()->get( "faxnumber" ))) { ?>
				            <tr>
				            	<td><p><strong>Fax. : </strong></p></td>
				            	<td><p><?php echo HumanReadablePhone($Order->getCustomer()->getContact()->get( "faxnumber" )); ?></p></td>
				            </tr>
				            <?php } ?>
				            <?php if(HumanReadablePhone($Order->getCustomer()->getContact()->get( "gsm" ))) { ?>
				            <tr>
				            	<td><p><strong>GSM : </strong></p></td>	
				            	<td><p><?php echo HumanReadablePhone($Order->getCustomer()->getContact()->get( "gsm" )); ?></p></td>
				           	</tr>
				            <?php } ?>
				            <tr>
				            	<td width="40"><p><strong>Email : </strong></p></td>	
				           		<td><p><a class="blueLink" href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?></a></p></td>
				        	</tr>
				        </table>   	
			           	<p><a class="viewContact" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>&IdEstimate=<?php echo $IdEstimate ?>">Voir la fiche client n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?></a></p>
					
					</div> 
		            <br/>
		            <!--
                    <p><strong>Encours garanti HT</strong> : <?php echo $Order->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->get( "insurance_amount" ) ) : "-"; ?></p>
					<p><strong>Encours assurance crédit HT</strong> : <?php echo $Order->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->getInsuranceOutstandingCreditsET() ) : "-"; ?></p>
					-->
                    <p><strong><?php
						switch( $customer->get( "insurance_status" ) ){
							case "Accepted":
								echo "Date d'assurance";
								break;
								
							case "Refused":
								echo "Date de refus";
								break;

							case "Deleted":
								echo "Date de suppression";
								break;
								
							case "Terminated":
								echo "Date de résiliation";
								break;

							default:
								echo "";
								break;
						}					
	
					?></strong> 
					<?php
						switch( $customer->get( "insurance_status" ) ){
							case "Accepted":
								echo Util::dateFormatEu( $customer->get( "insurance_amount_creation_date" ) );
								break;
								
							case "Refused":
								echo Util::dateFormatEu( $customer->get( "insurance_refusal_date" ) );
								break;

							case "Deleted":
								echo Util::dateFormatEu( $customer->get( "insurance_deletion_date" ) );
								break;
								
							case "Terminated":
								echo Util::dateFormatEu( $customer->get( "insurance_termination_date" ) );
								break;

							default:
								echo "";
								break;
						}					
	
					?></p>	
			        			
        <?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping");
			if($akilae_grouping==1){
		?>
			        <div class="blocMultiple blocMRight" style="margin-bottom: 0px; width:100%;"><div style="margin:5px;">	
			        <?php
						$customerGrouping = DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $Order->getCustomer()->get( "idbuyer" ) );
						if( $customerGrouping ){
							$grouping = new GroupingCatalog();
							
							//@todo: Bidouille qui consiste à changer l'idgrouping du groupement à la volée pour charger les infos
							// Le problème vient du fait qu'on ne peut instancier un groupement que depuis le Front-Office
							$grouping->getGroupingInfos( $customerGrouping );
							$groupingName = $grouping->getGrouping( "grouping" );
							$groupingLogo = $grouping->getGrouping( "grouping_logo" );							
					?>
	
							<p><strong>Groupement : </strong><?php echo $groupingName ?></p>
							<div style="margin:0 0 5px 0; text-align:center;">
								<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $groupingLogo ?>" alt="<?php echo $groupingName ?>" />
							</div>
							<div class="spacer"></div>
					
					<?php } ?>
			        </div></div>
			        <div class="spacer"></div>
				<?php } ?>
				</div>

		       	<div class="blocMultiple notMarginBottom">
		       		
		       		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px; width:100%;"><div style="margin:5px 5px 0 5px;">	
		       		<?php if($Order->getCustomer()->get('naf')) { ?>
							<p><strong>APE/NAF</strong> : <?php echo DBUtil::getDBValue("naf_comment", "naf", "idnaf", str_replace(" ", "",$Order->getCustomer()->get('naf')) ); ?></p>
					<?php } ?>
		       		</div></div>
					<div class="spacer"></div>	
					
		       		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
						<?php

							$rs =& DBUtil::query("SELECT qty FROM manpower WHERE idmanpower='".$Order->getCustomer()->get('idmanpower')."'");
							$qtty = $rs->Fields( "qty" );	
						?>	
						
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="50%"><p><strong>Chiffre d'affaire</strong></p></td>
							<td width="50%"><p><?php echo ($Order->getCustomer()->get('ca')) ? $Order->getCustomer()->get('ca')."&euro;" : "-"; ?></p></td>
						</tr>	
						<tr>
							<td><p><strong>Effectif</strong></p></td>
							<td><p><?php echo $qtty ? $qtty : "-"; ?></p></td>
						</tr>		
						<tr>
							<td><p><strong>Siren</strong></p></td>
							<td><p><?php echo ($Order->getCustomer()->get('siren')) ? $Order->getCustomer()->get('siren') : "-"; ?></p></td>
						</tr>								
						</table>

				        <div class="spacer"></div>	
					</div></div>
					
					<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
					
						<table border="0" cellspacing="0" cellpadding="0" width="100%">
						<tr>
							<td width="50%"><p><strong>Résultat</strong></p></td>
							<td width="50%"><p><?php echo $Order->getCustomer()->get('resultat') ? $Order->getCustomer()->get('resultat')."&euro;" : "-"; ?></p></td>
						</tr>	
						<tr>
							<td><p><strong>Rentabilité financière</strong></p></td>
							<td><p><?php echo $Order->getCustomer()->get('renta_finan') ? $Order->getCustomer()->get('renta_finan')."%" : "-"; ?></p></td>
						</tr>		
						<tr>
							<td><p><strong>Siret</strong></p></td>
							<td><p><?php echo $Order->getCustomer()->get('siret') ? $Order->getCustomer()->get('siret') : ""; ?></p></td>
						</tr>		
						</table>
				        <div class="spacer"></div>	
					</div></div>
					<div class="spacer"></div>	
					
					<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0pt 8px 0; width: 99%;"> </div>
					
					
					
					

					<?
					$siren = $Order->getCustomer()->get('siren');
					$cp = $Order->getCustomer()->get('zipcode');
					$raison = $Order->getCustomer()->get('company');
					
					$axaLogin = DBUtil::getParameterAdmin( 'axa_login' );
					$axaPass = DBUtil::getParameterAdmin( 'axa_pass' );
					
					if( $Order->getCustomer()->get('idbuyer') && $raison != '' && $axaLogin && $axaPass ){ ?>
						<iframe style="display:none;" id="monAxa" src="https://cofanet.coface.com/portalViewWeb/portal/validateAuthentication.do?userCodeOp=<?php echo $axaLogin; ?>&userPassword=<?php echo $axaPass; ?>"></iframe>
						<script type="text/javascript">
						/* <![CDATA[ */
							<?php if($siren) { ?>
								var monUrlAxa ="https://cofanet.coface.com/subscriberView/pageflow/CompanySearch/searchByLegalIdentifier.do?legalIdCountry=FRA&legalIdentifierType=SIREN&legalIdentifierValue=<?php echo $siren ?>";
							<?php } else {
								if($cp){ ?>								
									var monUrlAxa = "https://cofanet.coface.com/subscriberView/pageflow/CompanySearch/searchByCompanyName.do?wlw-select_key%3A%7BactionForm.companyNameCountry%7DOldValue=true&wlw-select_key%3A%7BactionForm.companyNameCountry%7D=FRA&%7BactionForm.companyName%7D=" + escape('<?php echo Util::upperCase( $raison ); ?>') + "&%7BactionForm.town%7D=&%7BactionForm.postalCode%7D=<?php echo $cp; ?>&%7BactionForm.streetName%7D=&wlw-select_key%3A%7BactionForm.departmentCode%7DOldValue=true&wlw-select_key%3A%7BactionForm.departmentCode%7D=netui_null&wlw-checkbox_key%3A%7BactionForm.mainEstablishmentOnly%7DOldValue=false&wlw-checkbox_key%3A%7BactionForm.mainEstablishmentOnly%7D=on&wlw-checkbox_key%3A%7BactionForm.phoneticValue%7DOldValue=false";
								<?php } else { ?>
									var monUrlAxa = "https://cofanet.coface.com/subscriberView/pageflow/CompanySearch/begin.do";
								<?php }
							} ?>
							$(document).ready(function(){
				
							    $('#monAxa').load(function(){
							        $('#axalink').show();
							    });
							});
						/* ]]> */
						</script>		
									
						<a class="floatleft" href="#" style="margin:0 30px 0 5px;" onclick="window.open(monUrlAxa); return false;" style="display:none;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-assur-credit-axa.jpg" alt="" width="65" height="25" /></a>

					<?php } ?>

					
					<?php if(isset($siren) && strlen($siren) > 3){ ?>
						<a class="floatleft" href="http://www.manageo.fr/entreprise/rc/preliste.jsp?action=2&siren=<?=$siren?>&x=42&y=2" target="_blank" style="margin:0 40px 0 5px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
						<a class="floatleft" href="http://www.societe.com/cgi-bin/recherche?rncs=<?=$siren?>&imageField2.x=60&imageField2.y=1" target="_blank" style="margin-right:40px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
					<?php }else{ ?>
						<a class="floatleft" href="http://www.manageo.fr/" target="_blank" style="margin:0 40px 0 5px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
						<a class="floatleft" href="http://www.societe.com/" target="_blank" style="margin-right:40px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
					<?php } ?>
			
			        <a class="floatleft" href="http://www.pagesjaunes.fr/" target="_blank" style="margin-right:40px;">
			        <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-pages-jaunes_g.jpg" alt="" width="48" height="25" /></a>
					<a class="floatleft" href="http://www.ctqui.com" target="_blank" style="margin-right:40px;">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-ctqui_g.jpg" alt="" width="48" height="25" /></a>
					<a class="floatleft" href="http://www.euridile.com" target="_blank" style="margin-right:40px;">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-euridile_g.jpg" alt="" width="48" height="25" /></a>
					<div class="spacer"></div>
                     
			
            <?php
			
			$query = "
					SELECT title_offer 
					FROM estimate
					WHERE idestimate = '" . $IdEstimate . "'
					";

					$rs =& DBUtil::query( $query );
			
			$title_offer = $rs->fields("title_offer");
			
			?>
            
            <form id="linkJoint" action="com_admin_devis.php?IdEstimate=<?php echo $Order->getId(); ?>" method="post" enctype="multipart/form-data">
            <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
                    <p><strong>Titre du devis</strong><br>
					<input type="text" name="title_offer" value="<?php echo $title_offer; ?>"</p>
                           <input type="submit" class="blueButton" value="ok" />
                    
                    </div>
		       	</div>
	       	</form>
	       		
                </div>
                <div class="spacer"></div>
			</div>
			
                
           <!-- *************** -->
			<!-- Autres contacts -->
			<!-- *************** -->
			<div id="contact" class="displayNone">
				<h2 class="subTitle">Sélectionner le contact pour ce devis</h2>
				<div class="spacer"></div>
				
				<!-- Liste des contacts -->
				<?php
				
					$query = "
					SELECT c.idcontact, CONCAT( t.title_1, ' ', c.lastname,' ', c.firstname ) AS nomPrenom, c.faxnumber, c.phonenumber, c.mail, c.gsm, f.function_1, c.idfunction 
					FROM title t, contact c
					LEFT JOIN function f ON f.idfunction = c.idfunction
					WHERE idbuyer = '" . $customer->getId() . "' AND t.idtitle = c.title 
					ORDER BY idcontact ASC";

					$rs =& DBUtil::query( $query );
					$i=0;
					while( !$rs->EOF() ){
						$selected = $Order->get( "idcontact" ) == $rs->fields( "idcontact" ) ? " checked='checked'" : "";	
						$bgcolor = ($i++ & 1) ? '#FFFFFF' : '#EEEEEE';
					
				?>
	            		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px; width:25%;"><div style="margin:5px; padding:5px; height:115px; background:<?php echo $bgcolor ?>; border:1px solid #EEEEEE;">
			            	<input style="float:right;" type="radio" name="contactSelect"<?php echo $selected; ?> onchange="document.location='<?php echo URLFactory::getHostURL(); ?>/sales_force/com_admin_devis.php?update_contact&idcontact=<?php echo $rs->fields( "idcontact" ); ?>&IdEstimate=<?php echo $Order->getId(); ?>'" />
			            	<p><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/icons-user.png" alt="user" />&nbsp;<strong><?php echo htmlentities( $rs->fields( "nomPrenom" ) ); ?></strong></p>
			            	<p><strong>Fonction</strong> : <?php if($rs->fields( "idfunction" ) ) { ?><?php echo $rs->fields( "function_1" ); ?><?php } else { ?>Non renseigné<?php } ?></p>
			            	<p><strong>Email : </strong><a class="blueLink" href="mailto:<?php echo $rs->fields( "mail" ); ?>"><?php echo $rs->fields( "mail" ); ?></a></p>
			            	<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
			            	<td width="50%" style="background:none;"><p><?php if($rs->fields( "phonenumber" )) { ?>Tél. : <?php echo $rs->fields( "phonenumber" ); ?><?php } ?>&nbsp;</p></td>
				            <td width="50%" style="background:none;"><p><?php if($rs->fields( "faxnumber" )) { ?>Fax : <?php echo $rs->fields( "faxnumber" ); ?><?php } ?>&nbsp;</p></td>
				            </tr></table>
				            <div class="spacer"></div>
				            <p><?php if($rs->fields( "gsm" ))  { ?>GSM : <?php echo $rs->fields( "gsm" ); ?><?php } ?>&nbsp;</p>
			            	<div class="spacer"></div>
			            	<p><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>&IdEstimate=<?php echo $IdEstimate ?>#gestContacts">
			            	<img class="floatright" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-edit.png" /></a></p>
			            </div></div>	
			            
			            				
				<?php

						$rs->MoveNext();
					}
				?>
				<div class="spacer"></div>
				
			</div>
			
			
			<!-- *********** -->
			<!-- Relance -->
			<!-- *********** -->
			<div id="relances-client" class="displayNone">
			<?php
			
				//date de relance avant la fin de validité du devis
				
				$rs =& DBUtil::query( "SELECT * FROM estimate_reminder WHERE idestimate = '" . $Order->getId() . "' LIMIT 1" );
				
				if( $rs->RecordCount() ){
					
					?>
					<p>
						<b>Date de relance avant fin de validité :</b> <?php echo Util::dateFormatEu( substr( $rs->fields( "datetime" ), 0, 10 ) ); ?>
					</p>
					<?php
				
				}
				
				?>
				<div class="blocMultiple blocMultiple4">
		            <p><strong><?php echo $Order->getCustomer()->get('company'); ?></strong>
					<?php
						// ------- Mise à jour de l'id gestion des Index  #1161
						$hasLitigGraves = DBUtil::query("SELECT count(idbilling_buyer) as compte FROM billing_buyer WHERE litigious = 1 AND idbuyer = '$idbuyer'")->fields('compte');
						if($hasLitigGraves){
							echo "<img src=\"$GLOBAL_START_URL/images/back_office/content/litigation.png\" alt=\"Litige\" style=\"vertical-align:middle;\" /> <span style=\"color:red;\">ATTENTION CE CLIENT A UNE OU PLUSIEURS FACTURES QU'IL NE PEUT PAYER</span>";
						}
					?>				
					</p>
		            <p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ); ?> <?php echo $Order->getCustomer()->getContact()->get( "firstname" ); ?></p>
		            <p><?php echo $Order->getCustomer()->get('adress'); ?>
		            <?php $adr2 = "<p>".$Order->getCustomer()->get('adress_2')."</p>"; if(!empty($adr2)){ echo $adr2; } ?>
		            <p><strong><?php echo $Order->getCustomer()->get('zipcode'); ?> <?php echo $Order->getCustomer()->get('city'); ?></strong> - <?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ); ?></p>
					
					<form class="floatleft" id="TodoInfosForm" action="com_admin_devis.php?IdEstimate=<?php echo $Order->getId(); ?>#relances-client" method="post" enctype="multipart/form-data">
						<input type="submit" class="blueButton" name="RelaunchButton" value="Relancer" />
			        </form>
			        <div class="spacer"></div>
				</div>
		            
		        <div class="blocMultiple blocMultiple4">
			            <p><strong>Tél. : </strong><?php echo HumanReadablePhone($Order->getCustomer()->getContact()->get( "phonenumber" )); ?></p>
			            <p><strong>Fax : </strong><?php echo HumanReadablePhone($Order->getCustomer()->getContact()->get( "faxnumber" )); ?></p>
			            <p><strong>GSM : </strong><?php echo HumanReadablePhone($Order->getCustomer()->getContact()->get( "gsm" )); ?></p>
			            <p><strong>Email : </strong><a class="blueLink" href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?></a></p>
			            <br/>
				</div>
			
				<div class="blocMultiple blocMRight BlocDevis" style="margin-bottom: 0px;">
					<form action="com_admin_devis.php?IdEstimate=<?php echo $Order->getId(); ?>#lastaction" method="post" enctype="multipart/form-data">	
			        <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px 5px 5px 0;">
			        	<label style="background:none; width:23%;"><span>Scoring</span></label>

			       		<?php  //activer ou pas le buton de mise a jour. Bisou
							if( $Order->getCustomer()->getId() == DBUtil::getParameterAdmin( "internal_customer" ) ){
								$ds = " disabled=\"disabled\"";
							}else{
								$ds="";
							}
						?>
						<?php
							$scoring_tx = $Order->get( "scoring_tx" );								
							XHTMLFactory::createSelectElement( 'scoring_estimate' , 'scoring_tx' , 'scoring_name' , 'scoring_tx' , $scoring_tx , '' , '' , ' name="scoring_tx" style="width:180px !important;" ' );

						?>
						<div class="spacer"></div>
						
						
			        </div></div>
			        
			        <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px 0 5px 0;">	
		       			
			            <label style="background:none; width:23%;"><span>Date</span></label>
			            
			            <?php $array_scoring_date=explode("-",$Order->get("scoring_date" )); ?>
						<input type="text" name="scoring_date" id="scoring_date" class="calendarInput" value="<?php echo  usDate2eu($Order->get("scoring_date" )); ?>" readonly="readonly" style="float:left; margin-left:0;" />
						<span class="floatleft"><?php DHTMLCalendar::calendar( "scoring_date" ); ?></span>
			            
					    <input type="image" name="UpdateScoring" id="UpdateScoring" value="Valider" class="blueButton floatright" style="margin:5px 0 0 0;" />
						<div class="spacer"></div>
			            
						<input type="hidden" name="ModifyEstimate" value="true" />
						<input type="hidden" name="IdEstimate" value="<?php echo $Order->get( "idestimate" ); ?>" />
				        
				        <div class="spacer"></div>
		       		</div></div>
		       		</form>
		       		<div class="spacer"></div>	
				</div>
				<div class="spacer"></div>
				<div class="blocMultiple" style="margin-bottom: 0px;">

	            	<label style="width:100%; background:none;"><span>Relances</span></label>
		            <div class="spacer"></div>	
		            
					<?php
						$todo = $Order->get( "todo" );
						$relaunch_send_date = $Order->get( "relaunch_send_date" );
							// ------- Mise à jour de l'id gestion des Index  #1161
						$relaunches = DBUtil::query( "SELECT u.initial , er.* 
														FROM estimate_relaunch er
														LEFT JOIN user u ON u.iduser = er.iduser 
														WHERE idestimate = '" . $Order->get( "idestimate" ) ." '	
														ORDER BY idrelaunch ASC" );
						
						if( $relaunches->RecordCount() ){
							?>
							<table class="dataTable" >
								<tr>
									<th>Initiales</th>
									<th>Date relance</th>
									<th>Type</th>
									<th>Commentaire</th>
									<th>Etat</th>
									<th>Mail</th>
								</tr>
							<?
							while( !$relaunches->EOF() ){
								$link = '';
								$file = '/data/history/estimate_relaunch/' . $Order->get( "idestimate" ) . '/' . $relaunches->fields( 'idrelaunch' ) . '_mail_' . Util::dateFormat( $relaunches->fields( 'DateHeure' ) , 'd-m-Y' ) . '.txt';

								if( file_exists( dirname( __FILE__ ) . '/../../..' . $file ) )
									$link = "<a href='#' onclick=\"openMail( '$file' ); return false;\" class='blueLink'>Voir</a>";
								?>
								<tr>
									<td><?php echo $relaunches->fields( 'initial' ) ?></td>
									<td><?php echo Util::dateFormatEu( $relaunches->fields( 'DateHeure' ) ) ?></td>
									<td><?php echo $relaunches->fields( 'type' ) ?></td>
									<td><?php echo stripslashes( $relaunches->fields( 'comment' ) ) ?></td>
									<td><?php echo stripslashes( $relaunches->fields( 'statement' ) ) ?></td>
									<td><?php echo $link ?></td>
								</tr>
								<?
								$relaunches->MoveNext();
							}
							?>
							</table>
							<?
						}
						
					?>
					<script type="text/javascript">
					
						function openMail( adress ){
							
							$.ajax({
								 	
								url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_devis.php?IdEstimate=" + <?php echo $Order->get( "idestimate" ); ?> + "&getHistoricFileContent=" + adress,
								async: true,
							 	success: function( responseText ){
					
									$('#mailViewer').html( responseText );
									
								}
					
							});
							
							$( "#mailViewer" ).dialog({
								width: 700,
								height: 'auto',
								position: 'top',
								modal: true,
								resizable:false,
								title: 'Visualisation de mail',
								close:function(event, ui) { $('#mailViewer').dialog( 'destroy' ); }
							});
						}
						
					</script>
					<div id="mailViewer" style="display:none;"></div>
	            	<label style="width:100%; background:none;"><span>Commentaires</span></label>
		            <div class="spacer"></div>	
	            	<div id="relancesCommentsDiv">
	            		<form id="TodoInfosForm" action="com_admin_devis.php?IdEstimate=<?php echo $Order->getId(); ?>#relances-client" method="post" enctype="multipart/form-data">
		    	    	    <input type="hidden" name="IdEstimate" value="<?php echo $Order->get( "idestimate" ) ?>" />
							<input type="hidden" name="ModifyEstimate" value="true">
	                		<textarea<?php echo $disabled ?> name="todo" style="width: 100%; height:100px;" class="textInput"><?php echo html_entity_decode( $Order->get( "todo" ) ) ?></textarea>
	               			<div class="spacer"></div>	
		                
		               		 <input type="submit" value="Enregistrer" name="UpdateTodoMessage" id="UpdateTodoMessage" style="float: right; margin-top: 3px;" class="blueButton" />
			       		</form>
					</div>
			        
				</div>
				<div class="spacer"></div>			    
			    
			    
			</div>
			
			

			<!-- ********************** -->
			<!-- Visualiser et imprimer -->
			<!-- ********************** -->
			<div id="visualiser-imprimer" class="displayNone">
				<?php if( !strlen( $Order->getCustomer()->getContact()->get( "mail" ) ) ) { ?>
						<p class="subTitleWarning">
						<?php  echo Dictionnary::translate("gest_com_buyer_without_mail_order") ; ?></p>
				<?php } ?>
				<div class="spacer"></div>	
					
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<p style="margin-bottom:5px;"><a class="normalLink" style="display:block;" href="/catalog/pdf_estimate.php?idestimate=<?=$IdEstimate;?>" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
						&nbsp;<?php echo Dictionnary::translate( "view_pdf_estimate" ); ?><?=$IdEstimate;?></a></p>
                        
                        <p style="margin-bottom:5px;"><a class="normalLink" style="display:block;" href="/catalog/odt_estimate.php?idestimate=<?=$IdEstimate;?>" onclick="window.open(this.href); return false;">
                        	<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/odt.jpg" width="17px" height="17px"/>
						&nbsp;Devis ODT n°<?=$IdEstimate;?></a></p>
                        
						
						<?php if( $Order->getCustomer()->getId() != DBUtil::getParameterAdmin( "internal_customer" ) ){ ?>
							<p><a class="normalLink" style="display:block;" href="/catalog/pdf_proforma_invoice.php?idestimate=<?php echo $IdEstimate ?>" >
							<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
							&nbsp;Facture Pro Forma PDF n°<?php echo $IdEstimate ?></a></p>
						<?php } ?>
					</div></div>
					
					
					<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<?php if( strlen( $Order->getCustomer()->getContact()->get( "mail" ) ) ){ ?>
							<p style="margin-bottom:5px;"><a class="normalLink" style="display:block;" href="transmit.php?IdEstimate=<?php echo $IdEstimate ?>" onclick="window.open(this.href); return false;">
							<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
							&nbsp;<?php echo Dictionnary::translate( "view_email_estimate" ); ?></a></p>
							
							<?php if( $Order->getCustomer()->getId() != DBUtil::getParameterAdmin( "internal_customer" ) ){ ?>
								<p><a class="normalLink" style="display:block;" href="transmit.php?IdEstimate=<?php echo $IdEstimate ?>&amp;type=pro">
								<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
								&nbsp;Mail d'accompagnement Pro Forma</a></p>
							<?php } ?>
						<?php } ?>
					</div></div>
					<div class="spacer"></div>
				</div>
				
				<div class="blocMultiple blocMRight" style="margin-bottom: 0px;">
					<!--<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<p style="margin-bottom:5px;"><a class="normalLink" style="display:block;" href="/catalog/pdf_estimate.php?idestimate=<?=$IdEstimate;?>" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-fax.png" />
						&nbsp;<?php echo Dictionnary::translate( "view_fax_estimate" ); ?><?=$IdEstimate;?></a></p>

						<p><a href="generateEstimate.php?IdEstimate=<?=$IdEstimate?>" class="normalLink" style="display:block;" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-word.png" />
						&nbsp;<?php echo Dictionnary::translate( "view_word_estimate" ); ?><?=$IdEstimate?></a></p>
					</div></div>-->
			
					<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin: 5px;">

						<?php
							
				        $rs =& DBUtil::query( "SELECT ec.*, u.login, u.firstname, u.lastname, u.email FROM estimate_comments ec, user u WHERE ec.idestimate = '" . $Order->get( "idestimate" ) . "' AND ec.iduser = u.iduser ORDER BY date_time ASC" );
				        if( $rs->RecordCount() ){
				        	
				        	?>
							<script type="text/javascript">
							/* <![CDATA[ */
							
								function estimateCommentsDialog(){
								
									$( "#EstimateCommentsDialog" ).dialog({
										title:"Commentaires",
										bgiframe: true,
										modal: true,
										width:700,
										overlay: {
											backgroundColor: '#000000',
											opacity: 0.5
										},
										close: function(event, ui) { $( "#EstimateCommentsDialog" ).dialog( "destroy" ); }
										
									});
								
								}
							
							/* ]]> */
							</script>
							<a style="display:block; margin-left:5px;" href="#" onclick="estimateCommentsDialog();">Voir les <?php echo $rs->RecordCount(); ?> commentaire(s)</a>
				        	<div id="EstimateCommentsDialog" style="display:none; padding-bottom:20px;">
				        	<?php
				        	
				        		while( !$rs->EOF() ){
				        			
					        		?>
									<p style="margin-bottom:0px;">
										<b><?php echo htmlentities( $rs->fields( "login" ) ); ?></b> &lt;<a href="#" onclick="mailto:<?php echo $rs->fields( "email" ); ?>; return false;"><?php echo htmlentities( $rs->fields( "email" ) ); ?></a>&gt;
										a dit le <?php echo Util::dateFormatEu( $rs->fields( "date_time" ) ); ?> :
									</p>
									<div style="background-color:#FFFFFF; border:1px solid #D2D2D2; padding:10px; color:#7A8EA3">
										<?php echo nl2br( htmlentities( $rs->fields( "comment" ) ) ); ?>
									</div>
									<?php
									
									$rs->MoveNext();
								
				        		}
				        		
				        		?>
				        		<p style="text-align:right;">
				        			<input type="button" class="blueButton" value="Fermer" onclick="$('#EstimateCommentsDialog').dialog('destroy');" />
				        		</p>
				        	</div>
				        	<?php	
				        	
				        }
				        
				        // Recupération du siret
				        
				        $querySiret = 'SELECT siret FROM buyer WHERE idbuyer = "'.$Order->getCustomer()->get( "idbuyer" ).'"';
				        $bdSiret = DBUtil::getConnection();
				        $rsSiret = $bdSiret->Execute($querySiret);
				        
				        if($rsSiret === false){
				        	
				        }else{
				        	$siren = substr($rsSiret->fields("siret"), 0, 9);
				        }
					    ?>		

					</div></div>
					<div class="spacer"></div>
				</div>
				<div class="spacer"></div>
			
			</div>
			
			
			<!-- ********** -->
			<!-- Bloc notes -->
			<!-- ********** -->
			<div id="bloc-note" class="displayNone">

	    		<textarea id="EstimateNotes" style="border:1px solid #D2D2D2; font-size:12px; width:100%; height:80px;"><?php echo htmlentities( $Order->get( "note" ) ); ?></textarea>
	    		<div class="spacer"></div>
	    		
	    		<input type="button" class="blueButton floatright" style="margin-top:8px;" value="Sauvegarder" onclick="updateEstimate( 'note', $('#EstimateNotes').val() );" />
				<div class="spacer"></div>
			
			</div>
			
			
			<!-- **************** -->
			<!-- Pièces archivées -->
			<!-- **************** -->
			<div id="pa-client" class="displayNone">
				
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
					<form id="linkJoint" action="com_admin_devis.php?IdEstimate=<?php echo $Order->getId(); ?>#lastaction" method="post" enctype="multipart/form-data">
					    
					<label style="width:50%;"><span><?php  echo Dictionnary::translate("gest_com_estimate_signed") ; ?></span></label>
					<input type="file" name="offer_signed" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php   
					if(!empty($offer_signed)){?>
						&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $offer_signed ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_offer_signed" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>
					
					<label style="width:50%;"><span><?php  echo Dictionnary::translate("gest_com_supplier_offer") ; ?></span></label>
					<input type="file" name="supplier_offer" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<div class="spacer"></div>
					<?php  
					if(!empty($supplier_offer)){?>
						&nbsp;<a class="floatleft" href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $supplier_offer ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_supplier_offer" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>
					
					<label style="width:50%;"><span><?php  echo Dictionnary::translate("gest_com_doc_supplier") ; ?></span></label>
					<input type="file" name="supplier_doc" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php   
					if(!empty($supplier_doc)){?>
						&nbsp;<a class="floatleft" href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $supplier_doc ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_supplier_doc" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
                    <div class="spacer"></div>
					
					<label style="width:50%;"><span><?php  echo Dictionnary::translate("gest_com_doc_customer") ; ?></span></label>
					<input type="file" name="buyer_doc" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php   
					if(!empty($buyer_doc)){?>
						&nbsp;<a class="floatleft" href="<?php echo $GLOBAL_START_URL ?>/data/estimate/<?php echo $buyer_doc ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_buyer_doc" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>
		
					<input type="hidden" name="IdEstimate" value="<?php echo $Order->get( "idestimate" ); ?>" />
			        <input type="image" value="Enregistrer" name="UpdateFiles" id="UpdateFiles" style="margin: 5px 5px 0pt 0pt;" class="blueButton floatright" />
			        <div class="spacer"></div>
			
			    	</form>
			    	<div class="spacer"></div>
		    	</div>
		    	<div class="spacer"></div>
			
			</div>
			
			<!-- *********** -->
			<!--  Adresses   -->
			<!-- *********** -->
			<div id="adresses" class="displayNone">
			<?php
				if( $Order->getCustomer()->getId() != DBUtil::getParameterAdmin( "internal_customer" ) ) {
					include( "$GLOBAL_START_PATH/templates/sales_force/estimate/addresses.htm.php" );
				}
			?>
			</div>	
			
			
			<!-- *********** -->
			<!--  Assurance   -->
			<!-- *********** -->
			<div id="insurance" class="displayNone">
			<?php 
			
					if( $idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) ) ) {
												
						$insurance						= $Order->getCustomer()->get( "insurance" );
						$insurance_status				= $Order->getCustomer()->get( "insurance_status" );
						$insurance_amount				= $Order->getCustomer()->get( "insurance_amount" );
						$insurance_amount_creation_date	= $Order->getCustomer()->get( "insurance_amount_creation_date" );
						$insurance_refusal_date			= $Order->getCustomer()->get( "insurance_refusal_date" );
						$insurance_deletion_date		= $Order->getCustomer()->get( "insurance_deletion_date" );
						$insurance_termination_date		= $Order->getCustomer()->get( "insurance_termination_date" );
						
					?>
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
					
						<script type="text/javascript">
						/* <![CDATA[ */
						
							/* ---------------------------------------------------------------------------------------- */
							
							$( document ).ready( function(){

								$( "#InsuranceForm" ).ajaxForm( { success: insurancePostSubmitCallback } );
								
							});

							/* ---------------------------------------------------------------------------------------- */
							
							function insurancePostSubmitCallback( responseText, statusText ){

								if( responseText != "1" ){

									alert( "Impossible d'enregistrer les modifications : " + responseText );
									return;

								}
								
								$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
								$.blockUI.defaults.css.fontSize = '12px';
						    	$.growlUI( '', 'Modifications enregistrées' );

						    	listInsuranceAttachments();
						    	
							}

							/* ---------------------------------------------------------------------------------------- */
							
							function updateInsuranceForm(){
								switch( $( "#insurance_status :selected" ).val() ){
									case "Accepted":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "block" );
										$( "#InsuranceAmountDiv" ).css( "display", "block" );
										$( "#OutstandingCreditsDiv" ).css( "display", "block" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
																				
										if( $( "#insurance_amount_creation_date" ).val() == "" )
											$( "#insurance_amount_creation_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										//alert($( "#insurance_amount" ).val());
										
										if( $( "#insurance_amount" ).val() == "" || $( "#insurance_amount" ).val() == "0.00" )
											$( "#insurance_amount" ).val( "5000.00" ); 
											
										$( "#insurance_refusal_date" ).val( "" );										
										
										break;
	
									case "Refused":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "block" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
																				
										if( $( "#insurance_refusal_date" ).val() == "" )
											$( "#insurance_refusal_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										$( "#insurance_amount_creation_date" ).val( "" );		
										$( "#insurance_amount" ).val( "" );
																				
										break;
	
									case "Terminated":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "block" );
										
										if( $( "#insurance_refusal_date" ).val() == "" )
											$( "#insurance_refusal_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										$( "#insurance_amount_creation_date" ).val( "" );		
										$( "#insurance_amount" ).val( "" );										
										break;
	
									case "Deleted":
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "block" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "block" );
										
										if( $( "#insurance_refusal_date" ).val() == "" )
											$( "#insurance_refusal_date" ).val( "<?php echo date( "d-m-Y" ); ?>" );
										
										$( "#insurance_amount_creation_date" ).val( "" );		
										$( "#insurance_amount" ).val( "" );										
										break;
	
									default:
										$( "#InsuranceAmountCreationDateDiv" ).css( "display", "none" );
										$( "#InsuranceAmountDiv" ).css( "display", "none" );
										$( "#OutstandingCreditsDiv" ).css( "display", "none" );
										$( "#InsuranceRefusalDateDiv" ).css( "display", "none" );
										$( "#InsuranceAttachmentDiv" ).css( "display", "none" );
										$( "#InsuranceDeletionDateDiv" ).css( "display", "none" );
										$( "#InsuranceTerminationDateDiv" ).css( "display", "none" );
										
										$( "#insurance_refusal_date" ).val( "" );
										$( "#insurance_amount_creation_date" ).val( "" );
										$( "#insurance_amount" ).val( "" );										
										break;
								}
								

							}
							
							/* ---------------------------------------------------------------------------------------- */
							/* assurance - suppression pièce jointe */
							
							function deleteInsuranceAttachment( filename ){

								$.ajax({

									url: "/sales_force/estimate_parseform.php?delete=insurance_attachment",
									async: true,
									type: "POST",
									data: "idbuyer=<?php echo $Order->getCustomer()->getId(); ?>&filename=" + escape( filename ),
									error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
								 	success: function( responseText ){

										if( responseText == "1" ){
										
											$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							        		$.blockUI.defaults.css.fontSize = '12px';
							        		$.growlUI( '', 'Modifications enregistrées' );

								        	listInsuranceAttachments();
								        	
						        		}
						        		else alert( "Impossible de supprimer la pièce jointe : " + responseText );
						        		
									}

								});
								
							}

							/* ---------------------------------------------------------------------------------------- */
							/* assurance - actualiser la liste des pièces jointes */
							
							function listInsuranceAttachments(){

								$.ajax({

									url: "/templates/sales_force/estimate/buyer.htm.php?list=insurance_attachments",
									async: true,
									type: "GET",
									data: "idbuyer=<?php echo $Order->getCustomer()->getId(); ?>",
									error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
								 	success: function( responseText ){

										$( "#InsuranceAttachments" ).html( responseText );
						        		
									}

								});
								
							}
							
							/* ---------------------------------------------------------------------------------------- */
							
						/* ]]> */
						</script>
						<form id="InsuranceForm" action="/sales_force/estimate_parseform.php?update=insurance&amp;idbuyer=<?php echo $Order->getCustomer()->getId(); ?>" enctype="multipart/form-data" method="post">
							<label style="width: 40%;"><span>Statut</span></label>
							<select name="insurance_status" id="insurance_status" onchange="updateInsuranceForm();" style="width:160px !important;">
								<option value="">-</option>
								<option<?php if( isset( $key ) && $insurance_status == "Accepted" ) echo " selected=\"selected\""; ?> value="Accepted">Accepté</option>
								<option<?php if( isset( $key ) && $insurance_status == "Refused" ) echo " selected=\"selected\""; ?> value="Refused">Refusé</option>
								<option<?php if( isset( $key ) && $insurance_status == "Terminated" ) echo " selected=\"selected\""; ?> value="Terminated">Résilé</option>
								<option<?php if( isset( $key ) && $insurance_status == "Deleted" ) echo " selected=\"selected\""; ?> value="Deleted">Supprimé</option>
							</select>
							<div class="spacer"></div>	
							
							
							<div id="InsuranceAttachmentDiv"<?php if( $insurance_status == "" ) echo " style=\"display:none;\""; ?>>
								<label style="width: 40%;"><span>Pièce jointe</span></label>
								<input class="textSimple" style="height:21px; width:159px;" type="file" size="10" name="InsuranceAttachment" id="InsuranceAttachment" value="<?php echo Dictionnary::translate( "add" ) ?>" />
								<span id="InsuranceAttachments">
									<?php listInsuranceAttachments( $customer->getId() ); ?>
								</span>
							</div>
							<div class="spacer"></div>	
							
							<div id="InsuranceAmountCreationDateDiv"<?php if( $insurance_status != "Accepted" ) echo " style=\"display:none;\""; ?>>
								<label style="width: 40%;"><span>Date d'accord de l'encours</span></label>
								<input type="text" name="insurance_amount_creation_date" id="insurance_amount_creation_date" value="<?php echo $insurance_amount_creation_date ?>" readonly="readonly" class="textidInput" />
								<?php DHTMLCalendar::calendar( "insurance_amount_creation_date" ); ?>
							</div>
							<div class="spacer"></div>	
							
							<div id="InsuranceRefusalDateDiv"<?php if( $insurance_status != "Refused" ) echo " style=\"display:none;\""; ?>>
								<label style="width: 40%;"><span>Date refus</span></label>
								<input type="text" name="insurance_refusal_date" id="insurance_refusal_date" value="<?php echo $insurance_refusal_date ?>" class="textidInput" readonly="readonly" />
								<?php DHTMLCalendar::calendar( "insurance_refusal_date" ); ?>
							</div>
							<div id="InsuranceTerminationDateDiv"<?php if( !isset( $key ) || $insurance_status != "Terminated" ) echo " style=\"display:none;\""; ?>>
								<label><span>Date résiliation</span></label>
								<input type="text" name="insurance_termination_date" id="insurance_termination_date" value="<?php echo $insurance_termination_date != "0000-00-00" ? $insurance_termination_date : "" ?>" class="textidInput" readonly="readonly" />
								<?php DHTMLCalendar::calendar( "insurance_termination_date" ); ?>
							</div>
							<div id="InsuranceDeletionDateDiv"<?php if( !isset( $key ) || $insurance_status != "Deleted" ) echo " style=\"display:none;\""; ?>>
								<label><span>Date suppression</span></label>
								<input type="text" name="insurance_deletion_date" id="insurance_deletion_date" value="<?php echo $insurance_deletion_date != "0000-00-00" ? $insurance_deletion_date : "" ?>" class="textidInput" readonly="readonly" />
								<?php DHTMLCalendar::calendar( "insurance_deletion_date" ); ?>
							</div>
							<div class="spacer"></div>	
							
							<div id="InsuranceAmountDiv"<?php if( $insurance_status != "Accepted" ) echo " style=\"display:none;\""; ?>>
								<label style="width: 40%;"><span>Encours accordé (HT)</span></label>
								<input style="width:16.3%;" type="text" id="insurance_amount" name="insurance_amount" maxlength="10" value="<?php echo $insurance_amount; ?>" class="textidInputMin" style="<?php if( $insurance_status != "Accepted" ) echo " background-color:#E7E7E7;"; ?>" /> 
								<span class="textSigne">&euro;</span>
							</div>
							<div class="spacer"></div>	
							
							<div id="OutstandingCreditsDiv"<?php if( $insurance_status != "Accepted" ) echo " style=\"display:none;\""; ?>>
								<label style="width: 40%;"><span>Encours assurance (HT)</span></label>
								<span class="textidSimpleMin"><?php echo Util::priceFormat( $customer->getInsuranceOutstandingCreditsET() ); ?></span>
							</div>
							<div class="spacer"></div>	
							
							<input type="image" class="blueButton floatright" style="margin: 5px 5px 0pt 0pt;" id="UpdateBuer" name="UpdateBuer" value="Enregistrer">	
						</form>
					</div> <!-- Fin blocMultiple -->
					<div class="spacer"></div>
					<?php 
					
				} 
				
			?>
			</div>	
			
			
			<!-- *********** -->
			<!-- Historique -->
			<!-- *********** -->
			<?php $nbdevis = 0; $nbcdes = 0; $totDevis = 0; $totCdes = 0;
			$lastDevis = "-"; $lastCde = "-";

			$nbdevis 	= DBUtil::query("SELECT count(idestimate) as cpte FROM estimate WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('cpte');
			$nbcdes		= DBUtil::query("SELECT count(idorder) as cpte FROM `order` WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('cpte');
			$totDevis	= DBUtil::query("SELECT SUM(total_amount_ht) as sumo FROM estimate WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('sumo');
			$totCdes	= DBUtil::query("SELECT SUM(total_amount_ht) as sumo FROM `order`  WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('sumo'); ?>
						
			<div id="historique-client" class="displayNone">

				<div class="blocMultiple blocMLeft BlocDevis" style="margin-bottom: 0px;">
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px 5px 5px 0;">	
		            	<p><strong>Nombre devis</strong> : <?php echo $nbdevis ?></p>
		            </div></div>		            
		            <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px;">	
			           	<p><strong>Total devis HT</strong> : <?=Util::priceFormat($totDevis)?></p>
			        </div></div>
			        <div class="spacer"></div> 
				</div>
				
				<div class="blocMultiple blocMRight BlocDevis" style="margin-bottom: 0px;">
		            <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px;">	
		            	<p><strong>Nombre cdes.</strong> : <?=$nbcdes?></p>
		            </div></div>	            
		            <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px;">	
						<p><strong>Total cdes. HT</strong> : <?=Util::priceFormat($totCdes)?></p>
		            </div></div>
		            <div class="spacer"></div>  
				</div>
				<div class="spacer"></div>
				
				<?php	
					$idbuyer = $Order->getCustomer()->get( "idbuyer" );
					include_once( "$GLOBAL_START_PATH/sales_force/buyer_history_table.php" );	
				?>
			

			</div><!-- #historique-client -->
			
        </div> <!-- Fin container onglet -->
            	
	</div></div>
	<div class="spacer"></div>
</div>

<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>

<?php
//----------------------------------------------------------------------------
function listInsuranceAttachments( $key ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$path = "$GLOBAL_START_PATH/data/insurance/$key";
	
	if( !file_exists( $path ) || !is_dir( $path ) )
		return;
		
	$dir = opendir( $path );
	
	$files = array();
	while( $file = readdir( $dir ) ){
		
		if( $file != "." && $file != ".." )
			$files[] = $file;
			
	}
	
	if( !count( $files ) ){
	
		echo "Aucun fichier joint";
		return;
		
	}

	?>
	<ul style="list-style-type:none;">
	<?php
	
	foreach( $files as $file ){
		
		?>
		<li>
			<a href="#" onclick="if( confirm( 'Voulez-vous vraiment supprimer ce document ?' ) ) deleteInsuranceAttachment( '<?php echo str_replace( "'", "\'", $file ); ?>' ); return false;"><img src="/images/back_office/content/corbeille.jpg" style="border-style:none;" /></a>
			<a href="/data/insurance/<?php echo $key ?>/<?php echo str_replace( "+", "%20", urlencode( $file ) ) ?>" onclick="window.open(this.href); return false;"><?php echo htmlentities( $file ) ?></a>
		</li>
		<?php
		
	}
	
	?>
	</ul>
	<?php
	
}

?>