<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE MONTANTS ++++++++++++++++++++++++++++++++++++++++++ -->
<?php

anchor( "UpdateTotalDiscountAmount" );
anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" );
anchor( "UpdateInstallmentRate" );
anchor( "UpdateInstallmentAmount" );
anchor( "UpdateTotalChargeHT" );
anchor( "UpdateChargeSupplierRate" );
anchor( "UpdateChargeSupplierAmount" );
anchor( "ApplySupplierCharges" );

//total ttc
$vatrate = $Order->getVATRate();
$vat_amount = ($total_estimate_ht * $vatrate) / 100;
$ttc1 = $total_estimate_ht + $vat_amount;

?>
<!-- ************************************* MONTANTS ET TARIFICATIONS *********************************** -->
<?php $total_charge_auto = $Order->get( "total_charge_auto" ); ?>
<script type="text/javascript">
/* <![CDATA[ */

	function setExFactory( element ){
	
		//document.forms.frm.elements[ 'ex_factory' ].value = this.checked ? '1' : '0';
		
	}

/* ]]> */
</script>

<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>

	<div class="contentResult" style="margin-bottom: 10px;">	
			
	<h1 class="titleEstimate"><span class="textTitle">Montant et tarification du devis</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>
	
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">
	
	<div class="leftContainer" style="width:89%;">
		<table class="dataTable devisTable">
        	<tr>
				<?php if( B2B_STRATEGY ){?>
            	<th>Prix vente<br />HT brut </th>
				<?php }else{ ?>
				<th>Prix vente<br />TTC brut </th>
				<?php } ?>
				
				
            	
                <th colspan="2">Remise supplémentaire<br />sur facture</th>
				<th>Total HT net</th>
				<th colspan="2">Port vente refacturé</th>
				<th>
				<?php if( B2B_STRATEGY ){?>
            		Total HT
				<?php }else{ ?>
					Total TTC
				<?php } ?>
				
			<br />avec port vente</th>
				<th colspan="2">Remise pour<br />paiement comptant</th>
				<th>Total HT</th>
				<th>TVA  </th>
				<th>Montant TVA</th>
				<th>Total TTC</th>
            </tr>
            <tr>
            <?php
            
            	$articlesET = 0.0;
				$it = $Order->getItems()->iterator();
				while( $it->hasNext() )
					$articlesET += $it->next()->getTotalET();
					
					/*
				$it = $Order->getItems()->iterator();
				while( $it->hasNext() ){
					$articlesVAT += ($it->next()->getTotalET())*($Order->getVATRate())/100;
				
				}*/
				
				$i = 0;
				
				$articlesVAT = 0.0;
				while( $i < $Order->getItemCount() ){
		
		
				$articlesVAT += ($Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ))*($Order->getItemAt($i)->getVATRate())/100;
				
				$i++;
		
	}
				
				
				
				
				?>
				
				 <?php
            
            	$articlesATI = 0.0;
				$it = $Order->getItems()->iterator();
				while( $it->hasNext() )
					$articlesATI += $it->next()->getTotalATI();
				
				?>
				<?php if( B2B_STRATEGY ){?>
            	<td rowspan="3"><span id="prvhtb"><?php echo Util::priceFormat( $articlesET ); ?></span> </td>
				<?php }else{ ?>
				<td rowspan="3"><span id="prvhtb"><?php echo Util::priceFormat( $articlesATI ); ?></span> </td>
				<?php } ?>
				
                <td rowspan="3">
                	<input<?php echo $disabled; ?> id="rsfp" type="text" name="total_discount_rate" class="textInput percentage" value="<?php echo Util::numberFormat($total_discount_rate) ?>" onfocus="this.select()" onkeyup="document.getElementById('UpdateTotalDiscountRate').value='ok'; document.getElementById('UpdateTotalDiscountAmount').value=''; recalEstimate(); " /> %
                	<input type="hidden" name="UpdateTotalDiscountRate" id="UpdateTotalDiscountRate" value="" />
                </td>
                <td rowspan="3">
                	<input<?php echo $disabled; ?> id="rsf" type="text" name="total_discount_amount" class="textInput price" value="<?php echo Util::numberFormat($total_discount_amount) ?>" onfocus="this.select()" onkeyup="document.getElementById('UpdateTotalDiscountAmount').value='ok'; document.getElementById('UpdateTotalDiscountRate').value=''; recalEstimate(); " /> &euro;
                	<input type="hidden" name="UpdateTotalDiscountAmount" id="UpdateTotalDiscountAmount" value="" />
                </td>
				
				<?php if( B2B_STRATEGY ){?>
            	<td rowspan="3"><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) ?></span></td>
				<?php }else{ ?>
				<td rowspan="3"> </td>
				<?php } ?>
				
                                
				<td colspan="2">
					<select<?php echo $disabled; ?> name="total_charge_auto" onchange="updateCharges( this, '<?php echo Util::numberFormat( $Order->getChargesStrategy()->getChargesET() ) ?>' ); recalEstimate();">
                	<?php
                	
                		/* frais de port non calculables pour la Corse, les DOM-TOMs et l'étranger */
                	
                	if( $Order->get( "total_charge_auto" ) 
						//&& $this->get( "total_charge_ht" ) == 0.0
						&& ( $Order->getForwardingAddress()->getIdState() != 1
						|| ( strlen( $Order->getForwardingAddress()->getZipcode() ) > 1 && substr( $Order->getForwardingAddress()->getZipcode(), 0, 2 ) == "20" ) ) )
							$automaticChargesAvailable = false;
					else 	$automaticChargesAvailable = true;
		
                		if( $automaticChargesAvailable ){
                			
                			?>
	                		<option value="1"<?php if( $total_charge_auto ) echo " selected=\"selected\""; ?>>Coût paramétré</option>
	                		<?php
	                		
                		}
                		
                		?>
                		<option value="0"<?php if( !$Order->get( "ex_factory" ) && !$total_charge_auto ) echo " selected=\"selected\""; ?>>Coût calculé</option>
                		<option value="1"<?php if( !$automaticChargesAvailable || $Order->get( "ex_factory" ) ) echo " selected=\"selected\""; ?>>Départ usine</option>
                		<option value="0"<?php if( $Order->get( "charge_free" ) ) echo " selected=\"selected\""; ?>>Offert</option>
                	</select>
                	<br />
                	<input type="hidden" name="ex_factory" value="<?php echo $Order->get( "ex_factory" ) ? "1" : "0"; ?>" />
                	<input type="hidden" name="charge_free" value="<?php echo $Order->get( "charge_free" ) ? "1" : "0"; ?>" />
				</td>
				
				<?php if( B2B_STRATEGY ){?>
            	<td rowspan="3"><b><span id="thtpv"><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET() ) ?></span></b></td>
				<?php }else{ ?>
				<td rowspan="3"> </td>
				<?php } ?>
					
				
				<td rowspan="3">
					<input<?php echo $disabled ?> type="text" id="billing_rate" name="billing_rate" class="textInput percentage" value="<?php echo Util::numberFormat( $billing_rate ); ?>" onfocus="this.select()" onkeyup="document.getElementById('UpdateBillingRate').value='ok'; document.getElementById('UpdateBillingAmount').value=''; recalEstimate(); " /> %
				</td>
                <td rowspan="3">
                	<input<?php echo $disabled ?> type="text" id="billing_amount" name="billing_amount" class="textInput price" value="<?php echo Util::numberFormat( $billing_amount ); ?>" onfocus="this.select()" onkeyup="document.getElementById('UpdateBillingRate').value=''; document.getElementById('UpdateBillingAmount').value='ok'; recalEstimate(); " /> &euro;
                </td>
				
                <?php 
				
				if( B2B_STRATEGY ){
				
				$ht = $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET();        
				
				//$ttc = (($Order->getTotalATI()- ( $Order->getChargesET() + $Order->getChargesET()*$Order->getVATRate()/100 )   ) - ( (($Order->getTotalATI()- ( $Order->getChargesET() + $Order->getChargesET()*$Order->getVATRate()/100 )   )*$total_discount_rate)/100 )) + ( $Order->getChargesET() + $Order->getChargesET()*$Order->getVATRate()/100                     );
				$vat = $articlesVAT - $articlesVAT * $total_discount_rate/100 + $Order->getChargesET()*$Order->getVATRate()/100;
				
				$ttc = $articlesATI - ($articlesATI*$total_discount_rate/100) + $Order->getChargesATI();  
				?>
                
                <!-- HT -->
                <td rowspan="3">
					<b><span id="tht">
				<?php echo Util::priceFormat( $ht ); ?>
					</span></b>
				</td>
                
                <!-- TVA (%) -->
				<td rowspan="3">
					<span id="ttva"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></span>
				</td>
                
                <!-- TVA (€) -->
				<td rowspan="3">
					<span id="mtva"> <?php echo Util::priceFormat( $vat ); ?>					
					</span>
				</td>
                				
                <!-- TTC -->
                <td rowspan="3">
					<b><span id="tttc"><?php echo Util::priceFormat($ht + $vat); ?>
					
					</span></b>
				</td>
                
                <?php }else{
                
                 $ht = $articlesET - ( ($articlesET*$total_discount_rate)/100 )+  ( $Order->getChargesET() );
                
                $ttc = $articlesATI - ($articlesATI*$total_discount_rate/100) +( $Order->getChargesATI() );
                 ?>
                <!-- HT -->
                <td rowspan="3">
					<b><span id="tht">
					<?php echo Util::priceFormat( $ht  ); ?>
			
					</span></b>
				</td>
                
                <!-- TVA (%) -->
				<td rowspan="3">
					<span id="ttva"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></span>
				</td>
                
                 <!-- TVA (€) -->
				<td rowspan="3">
					<span id="mtva"><?php echo Util::priceFormat($ttc - $ht ); ?>
					</span>
				</td>
                
                <!-- TTC -->
                <td rowspan="3">
					<b><span id="tttc"><?php echo Util::priceFormat($ttc ); ?></span></b>
				</td>
                <?php
				} 
				?>
                
                
            </tr>
            <tr>
            	<th>Poids</th>
                 <?php 
				
				if( B2B_STRATEGY ){
				?>
            	<th>Port HT</th>
                <?php } else { ?>
                <th>Port TTC</th>
                <?php
				} ?>
                
                
            </tr>
            <tr>
            	<td>
            		<?php
            			
            			$Order->getWeight();
            			/*$Order->getChargesATI();*/
            			echo Util::numberFormat( $Order->getWeight() )
            			
            		?>
            		&nbsp;kg
            	</td>
            	<td>
            		<input<?php echo $disabled ?> name="total_charge_ht" id="total_charge_ht" value="<?php
            		
            			if( $Order->get( "ex_factory" ) )
            				echo "Départ usine";
            			else if( $Order->get( "charge_free" ) )
            				echo "Offert";
            			else if( $Order->get( "total_charge_auto" ) )
            				
							if( B2B_STRATEGY ){
							
	            				echo $Order->getChargesET() > 0.0 ?  Util::numberFormat( $Order->getChargesET()  ) : "Franco"; 
	            				}else{
								echo $Order->getChargesATI() > 0.0 ?  Util::numberFormat( $Order->getChargesATI()  ) : "Franco";
								}
            			
						else{
						
								if( B2B_STRATEGY )
									{
									 echo Util::numberFormat( $Order->get( "total_charge_ht" )  );
            						}
									else
									{
									 echo Util::numberFormat( $Order->get( "total_charge" )  );
            						}
							}
									
										
            		?>" <?php if( $Order->get( "total_charge_auto" ) || $Order->get( "ex_factory" ) || $Order->get( "charge_free" ) ) echo " disabled=\"disabled\""; ?> class="textInput price" onfocus="this.select()" onkeyup="recalEstimate();" />&nbsp;&euro;
            		<input type="hidden" name="UpdateTotalChargeHT" value="ok" />
            		<input type="hidden" name="svg_total_charge_ht" value="<?php echo $Order->get( "total_charge_ht" ) ?>" />
            	</td>
            </tr>
        </table>
        <div class="spacer"></div>
        
        <!-- <div class="floatright" style="margin-top:5px;">
        	<input type="submit" class="blueButton" name="ModifyEstimate" value="Enregistrer" />
        </div> -->
	</div>
    <div class="rightContainer" style="width:10%;">
        <table class="dataTable devisTable summaryTable">
            <tr>
                <th colspan="2">Marge nette après ports<br /> </th><!-- Port Achat et Vente -->
            </tr>
            <tr>
                <td colspan="1">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
                	<span id="mnf">
					                	<?php 
					$netMarginAmount = $Order->get( "net_margin_amount" );
                	echo Util::priceFormat( $netMarginAmount ); 
                	 ?>
					 </span>
										<?php } ?>
			

				<?php if($akilae_comext ==1){ ?>

					<span style="color:red !important;"> / 
					<?php 
					$netMarginAmount_com = $Order->get( "net_margin_amount" ) / (1+ $_SESSION['rate']/100) ;
                	echo Util::priceFormat( $netMarginAmount_com ); 
					?>						
					</span>	
				<?php } ?>
                </td>
            </tr>
                    <tr>
                        <th>%</th>
                     </tr>
            <tr>
                <td style="width:50%">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
                	<span id="mnfp">
                	<?=Util::rateFormat( $Order->get( "net_margin_rate" ) ) ?></span> <?php } ?>
					<?php if($akilae_comext ==1){ ?>
					<span style="color:red !important;"> / 
					<?=Util::rateFormat( $Order->get( "net_margin_rate" ) - $_SESSION['rate'] ) ?></span>
					<?php } ?>
					
                </td>
            </tr>
        </table>
	</div><!-- rightContainer -->
	<div class="spacer"></div>

	</div></div>
	<div class="spacer"></div>	

		