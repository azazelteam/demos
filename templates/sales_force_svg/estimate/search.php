<?php

/* ----------------------------------------------------------------------------------- */
/* suppression d'un devis */

if( isset( $_REQUEST[ "delete" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/Estimate.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
	
	exit( Estimate::delete( intval( $_REQUEST[ "idestimate" ] ) ) ? "1" : "0" );
	
}

/* ----------------------------------------------------------------------------------- */

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

?>

<script type="text/javascript">
<!--
	
	function goToPage( pageNumber ){
		
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function selectRelance(){
		
		document.getElementById( "interval_select_relance" ).checked=true;
		
	}
	
	function selectMonth(){
		
		document.getElementById( "interval_select_month" ).checked=true;
		
	}
	
	function selectBetween( field ){
		
		document.getElementById( "interval_select_between" ).checked=true;
		
		return true;
		
	}
	
	function deleteEstimate( idestimate ){
		
		if( !confirm( 'Confirmer la suppression du devis n° ' + idestimate + '?' ) )
			return false;
		
		$.ajax({
			
			url: "/templates/sales_force/estimate/search.php?delete",
			async: true,
			cache: false,
			type: "POST",
			data: "idestimate=" + idestimate,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le devis" ); },
		 	success: function( responseText ){

				if( responseText == "1" )
					$( "#EstimateRow" + idestimate ).fadeOut(700, function(){ $( "#EstimateRow" + idestimate ).remove(); } );
				else alert( "Impossible de supprimer le devis : " + responseText );
				
			}

		});
		
	}
	
	function UpdateUseEstimate( idestimate){
		
		document.forms.adm_estim.elements[ 'use_estimate' ].value = idestimate;
		document.forms.adm_estim.submit();
		
	}
	
	function showHideAdvanced(){
		
		var advanceddiv = document.getElementById( 'advanced_search' );
		advanceddiv.style.display = advanceddiv.style.display == 'block' ? 'none' : 'block';
		
	}
	
// -->
</script>

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
		

<div class="centerMax">
<div id="tools" class="rightTools">
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
	Accès rapide</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
	Quelques chiffres...</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
</div>	
<div class="contentDyn">
<form name="adm_estim" id="adm_estim" method="post" action="/sales_force/com_admin_search_estimate.php">
	<input type="hidden" name="search" value="1" />
	<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	<input type="hidden" name="EstimateToDelete" id="EstimateToDelete" value="0" />
	<input type="hidden" name="use_estimate" id="use_estimate" value="0" />
	
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Rechercher un devis</span>
	<span class="selectDate">
		<input type="radio" name="date_type" id="type_creation" class="VCenteredWithText" value="creation_date"<?php if( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" || !isset( $_POST[ "date_type" ] ) ) echo " checked=\"checked\""; ?> /> Date de création
	    <input type="radio" name="date_type" id="type_sent" class="VCenteredWithText"  value="sent_date"<?php if( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "sent_date" ) echo " checked=\"checked\""; ?> /> Date d'envoi
	</span>
	<div class="spacer"></div>
	</h1>
	
	
	<div class="blocSearch"><div style="margin:5px;">
	
		<!-- Choix par date -->
		<div class="blocMultiple blocMLeft">
			<h2>&#0155; Par date :</h2>
			
			<label><span><input style="float:left;" type="radio" name="interval_select" id="interval_select_relance" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1 || !isset( $_POST[ "interval_select" ] ) ) echo " checked=\"checked\""; ?> /> 
			&nbsp;Depuis</span></label>
			<select id="my-select" name="minus_date_select" onchange="selectRelance()" class="fullWidth">
				<option value="0"<?php if( !isset( $_POST[ "minus_date_select" ] ) || $_POST[ "minus_date_select" ] == 0 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_today") ?></option>
				<option value="-1"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_yesterday") ?></option>
				<option value="-2"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -2 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2days") ?></option>
				<option value="-7"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -7 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1week") ?></option>
				<option value="-14"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -14 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2weeks") ?></option>
				<option value="-30"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -30 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1month") ?></option>
				<option value="-60"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -60 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2months") ?></option>
				<option value="-90"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -90 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_3months") ?></option>
				<option value="-180"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -180 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_6months") ?></option>
				<option value="-365"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -365 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1year") ?></option>
				<option value="-730"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -730 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2year") ?></option>
				<option value="-1825"<?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1825 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_5year") ?></option>
			</select>
			<div class="spacer"></div>
			
			<label><span><input style="float:left;" type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> 
			&nbsp;Pour le mois de</span></label>
	        <?php FStatisticsGenerateDateHTMLSelect( "selectMonth()" ); ?>
	        <div class="spacer"></div>
	        
	        <label><span><input style="float:left;" type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> 
	        &nbsp;Entre le</span></label>
	        <?php $date = isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : ""; ?>
	        <input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php echo $date ?>" />
	        <?php echo DHTMLCalendar::calendar( "sp_bt_start", true, "selectBetween" ) ?>
	        
	        <label class="smallLabel"><span>et le</span></label>
	        <?php $date = isset( $_POST[ "sp_bt_end" ] ) ? $_POST[ "sp_bt_end" ] : ""; ?>
	        <input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php echo $date ?>" />
	        <?php echo DHTMLCalendar::calendar( "sp_bt_end", true, "selectBetween" ) ?>
        </div>   
        
        <!-- Choix par informations sur le devis -->
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Par informations sur le devis :</h2>
        
        	<label><span>Devis n°</span></label>
            <input type="text" name="sp_idorder" class="textInput" value="<?php echo isset( $_POST[ "sp_idorder" ] ) ? $_POST[ "sp_idorder" ] : "" ?>" />
            <div class="spacer"></div>
            
                      
            <label><span>Avancement</span></label>
            <div class="simpleSelectAvancement">
	            <select name="sp_status">
					<option value="">Tous</option>
					<option value="ToDo"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "ToDo" ){ echo " selected=\"selected\""; } ?>>A traîter</option>
					<option value="InProgress"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "InProgress" ){ echo " selected=\"selected\""; } ?>>En cours</option>
					<option value="Send"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "Send" ){ echo " selected=\"selected\""; } ?>>Envoyé</option>
					<option value="Ordered"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "Ordered" ){echo " selected=\"selected\""; } ?>>Confirmé</option>
					<option value="Refused"<?php if( isset( $_POST[ "sp_status" ] ) && $_POST[ "sp_status" ] == "Refused" ){echo " selected=\"selected\""; } ?>>Refusé</option>
				</select>
			</div>
			<label class="smallLabelFournisseur"><span>Fournisseur</span></label>
			<div class="simpleSelect">
				<select name="idsupplier">
					<option value="">Tous</option>
					<?php
					$suppliers = DBUtil::query("SELECT idsupplier,name FROM supplier WHERE idsupplier<>0 AND name<>'' ORDER BY name ASC");
					if($suppliers !== false && $suppliers->RecordCount() > 0){
						$seleted = isset($_POST['idsupplier']) && $_POST['idsupplier'] != '' ? $_POST['idsupplier'] : false; 
						while(!$suppliers->EOF()){
							$selSelect = "";
							if ( $seleted == $suppliers->fields('idsupplier') )
								$selSelect = " selected='selected' ";
								
							?><option value="<?php echo $suppliers->fields('idsupplier')?>" <?php echo $selSelect?>><php echo $suppliers->fields('name')?></option><?php
							$suppliers->MoveNext();
						}
					}
					?>
				</select>
			</div>
			<div class="spacer"></div>
            
            <label><span>Relance entre le</span></label>
            <?php $date = isset( $_POST[ "rel_start" ] ) ? $_POST[ "rel_start" ] : ""; ?>
            <input type="text" name="rel_start" id="rel_start" class="calendarInput" value="<?php echo $date ?>" />
            <?php echo DHTMLCalendar::calendar( "rel_start" ) ?>
            
            <label class="smallLabel"><span>et le</span></label>
            <?php $date = isset( $_POST[ "rel_end" ] ) ? $_POST[ "rel_end" ] : ""; ?>
            <input type="text" name="rel_end" id="rel_end" class="calendarInput" value="<?php echo $date ?>" />
            <?php echo DHTMLCalendar::calendar( "rel_end" ) ?>
            
            
            <label><span>Afficher les devis uniques</span></label>
            <label style="width:168px; height:38px;"><span><input style="float:left;" type="radio" name="unique_estimate" id="unique_estimate" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "unique_estimate" ] ) && $_POST[ "unique_estimate" ] == "1" ) echo " checked=\"checked\""; ?> /> 
			Oui</span></label>
            <label style="width:168px;  height:38px;"><span><input style="float:left;" type="radio" name="unique_estimate" id="unique_estimate" class="VCenteredWithText" value="0"<?php if( isset( $_POST[ "unique_estimate" ] ) && $_POST[ "unique_estimate" ] == "0" ) echo " checked=\"checked\""; ?> /> 
			Non</span></label>
           
           
            
            <br style="clear:both;" />
            <input type="checkbox" value="1" name="auto"<?php if( isset( $_POST[ "auto" ] ) ) echo " checked=\"checked\""; ?> />
            <span style="font-size:12px;"> Afficher uniquement les devis envoyé automatiquement au client</span>
            
            <div class="spacer"></div>
            
            <!-- <label>Montant TTC</label>
            <input type="text" name="total_amount" class="textInput" value="<?php echo isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "" ?>" />
            <div class="spacer"></div> -->
        
        </div>
        <div class="spacer"></div>
        
        <!-- Choix par client -->
        <div class="blocMultiple blocMLeft">
        	<h2>&#0155; Par client :</h2>
        	
        	<label><span><strong>Raison sociale</strong></span></label>
        	<input type="text" name="company" id="company" class="textInput" value="<?php echo isset( $_POST[ "company" ] ) ? $_POST[ "company" ] : "" ?>" />
        	 <?php
            
            	include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "company", "buyer", "company", "Client n°{idbuyer}", 15 ); 
            
            ?>
        	<div class="spacer"></div>
        	
        	<label><span><strong>Nom du client</strong></span></label>
            <input type="text" name="sp_lastname" id="sp_lastname" class="textInput" value="<?php echo isset( $_POST[ "sp_lastname" ] ) ? $_POST[ "sp_lastname" ] : "" ?>" />
            <?php
            
            	include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "sp_lastname", "contact", "lastname", "Client n°{idbuyer}", 15 ); 
            
            ?>
        	<div class="spacer"></div>
        	
        	<label><span><strong>N° du client</strong></span></label>
            <input type="text" name="sp_idbuyer" id="sp_idbuyer" class="textInput" value="<?php echo isset( $_POST[ "sp_idbuyer" ] ) ? $_POST[ "sp_idbuyer" ] : "" ?>" />
            <?php 
            
            	include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "sp_idbuyer", "buyer", "idbuyer", "{company}", 15 ); 
            	
            ?>
            <div class="spacer"></div>
            
            

        	<!-- <label>Fax</label>
        	<input type="text" name="faxnumber" class="textInput" value="<?php echo isset( $_POST[ "faxnumber" ] ) ? $_POST[ "faxnumber" ] : "" ?>" />
        	-->
        	
        	<label><span>Code postal</span></label>
        	<input type="text" name="zipcode" class="numberInput" value="<?php echo isset( $_POST[ "zipcode" ] ) ? $_POST[ "zipcode" ] : "" ?>" />
            
            <label class="smallLabel"><span>Ville</span></label>
            <input type="text" name="city" class="mediumInput" value="<?php echo isset( $_POST[ "city" ] ) ? $_POST[ "city" ] : "" ?>" />
            <div class="spacer"></div>                        
        </div>
        
        <!-- ??? -->
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Autres critères :</h2>

			<label><span><strong>Commercial</strong></span></label>
			<?php									
				if( $hasAdminPrivileges ){
					
					include_once( dirname( __FILE__ ) . "/../../../script/user_list.php" );
					userList( true, isset( $_POST[ "iduser" ] ) ? intval( $_POST[ "iduser" ] ) : 0 );
						
				}
				else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";												
			?>
			<div class="spacer"></div>
			
			<label><span>Taux scoring</span></label>
			<?php
											
				if( isset( $_POST[ "scoring_tx" ] ) )
					$scoring_tx = $_POST[ "scoring_tx" ];
				else
					$scoring_tx = "X";
			
				if( $hasAdminPrivileges )
					DrawLift_scoring( $scoring_tx );
				else
					echo "<b>" . $scoring_tx . "</b>";									
			?>
			<div class="spacer"></div>
			
			<label><span>Délais du scoring</span></label>
            <input type="text" name="scoring_date" id="scoring_date" class="calendarInput" value="" /> 
            <?php echo DHTMLCalendar::calendar( "scoring_date" ) ?>
			<div class="spacer"></div>
			 
			 <label><span>Provenance</span></label>
			<?php XHTMLFactory::createSelectElement( "source", "idsource", "source_1", "source_1", isset( $_POST[ "idsource" ] ) ? $_POST[ "idsource" ] : false, "", "-", "name=\"idsource\"" ); ?>
            <!--<select name="idsource" id="idsource">
            	<option value="0"<?php if( !isset( $_POST[ "idsource" ] ) || !$_POST[ "idsource" ] ) echo " selected=\"selected\""; ?>>Toutes</options>
            	<option value="6"<?php if( isset( $_POST[ "idsource" ] ) && $_POST[ "idsource" ] == "6" ) echo " selected=\"selected\""; ?>>EDGB2B</options>
            	<option value="5"<?php if( isset( $_POST[ "idsource" ] ) && $_POST[ "idsource" ] == "5" ) echo " selected=\"selected\""; ?>>Logismarket Axess</options>
            	<option value="1"<?php if( isset( $_POST[ "idsource" ] ) && $_POST[ "idsource" ] == "1" ) echo " selected=\"selected\""; ?>>Web direct</options>
            	<option value="15"<?php if( isset( $_POST[ "idsource" ] ) && $_POST[ "idsource" ] == "15" ) echo " selected=\"selected\""; ?>>Zoneindustrie</options>
            </select>-->
            <div class="spacer"></div>
            
			<?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping"); ?> 
			<?php if($akilae_grouping==1){ ?>

            <label><span>Groupement</span></label>
            <div class="simpleSelect">
            <?php XHTMLFactory::createSelectElement( "grouping", "idgrouping", "grouping", "grouping", isset( $_POST[ "idgrouping" ] ) ? $_POST[ "idgrouping" ] : false, "", "-", "name=\"idgrouping\" " ); ?>
            </div>
            <div class="spacer"></div>    
			<?php } ?>

        	
        </div>
        <div class="spacer"></div>
        
        <input type="submit" class="inputSearch" value="Rechercher" />    
        <div class="spacer"></div>      
	                        
	</div></div>
	<div style="clear:both;"></div>
	
</div>       		      	
<div class="spacer"></div>           	
            	
            	
<div id="EstimateSearchContainer">
<a name="topPage"></a>
            <div class="contentResult">         	
            	
            	
            	
                
<?php if ( $search != "" || isset($viewTodo) ){ ?>
<?php if( $resultCount > $maxSearchResults ){ ?>
                <div class="blocResult mainContent">
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Plus de <?php echo $maxSearchResults ?> devis ont été trouvés</p>
						<p class="msg" style="text-align:center;">Merci de bien vouloir affiner votre recherche</p>
					</div>
				</div>
<?php }else{ ?>
<?php if( $rs->RecordCount() <= 0 ){?>
                <div class="blocResult mainContent">
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Aucun résultat ne correspond à votre recherche</p>
					</div>
				</div>
<?php }else{ ?>
				<script type="text/javascript">
				/* <![CDATA[ */
				
					/* --------------------------------------------------------------------------------------------- */
					
					function createEstimateSlideshow(){
					
						if( $( "#Slideshow" ).length )
							return;
						
						var data = "estimates=<?php

							$estimates = array();
							
							$rs->MoveFirst();
							while( !$rs->EOF() ){
								
								array_push( $estimates, $rs->fields( "idestimate" ) );
								
								$rs->MoveNext();
									
							}
							
							echo URLFactory::base64url_encode( serialize( $estimates ) );
							//echo Util::base64url_encode( serialize( $estimates ) );
							
							$estimates = NULL;
							
							$rs->MoveFirst();
							
						?>";
						
						$.ajax({
				
							url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/slideshow/estimate_slideshow.php?ondestroy=destroyEstimateSlideshow",
							async: true,
							cache: false,
							type: "POST",
							data: data,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger le diaporama" ); },
						 	success: function( responseText ){
				
								$( ".titleSearch" ).hide();
								$( ".blocSearch" ).hide();
								$( ".blocResult" ).hide();
								$( "#tools" ).hide();
								
								$( responseText ).insertBefore( $( "#EstimateSearchContainer" ) );
								
							}
				
						});
						
					}
					
					/* ------------------------------------------------------------------------------------------------ */
					
					function destroyEstimateSlideshow(){
						$( "#Slideshow" ).remove();
						$( ".titleSearch" ).show();
						$( ".blocSearch" ).show();
						$( ".blocResult" ).show();
						$( "#tools" ).show();
		
					}
				
					/* ------------------------------------------------------------------------------------------------ */
					
				/* ]]> */
				</script>
				
				<style type="text/css">
					table.sortable th.header{ background-image: url(<?php echo $GLOBAL_START_URL ?>/images/small_sort.gif); cursor: pointer; font-weight: bold; background-repeat: no-repeat; background-position: 50% 85%; border-right: 1px solid #dad9c7; padding-bottom:18px; }
					table.sortable th.headerSortUp{ background-image: url(<?php echo $GLOBAL_START_URL ?>/images/small_asc.gif); }
					table.sortable th.headerSortDown{ background-image: url(<?php echo $GLOBAL_START_URL ?>/images/small_desc.gif); }
				</style>
				
				<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.min.js"></script>
				<script type="text/javascript">
				/* <![CDATA[ */
					$( document ).ready( function(){
						$( "#ResultEstimate" ).tablesorter({ 	
				      		sortList: [[0,1]],  
		                    widgets: ['zebra'],  
		                    dateFormat: 'uk', 
				      		headers: { 
								0: {  sorter:'text' }, 1: {  sorter:'text' }, 2: {  sorter:'text' }, 3: {  sorter:'text' },
				      			4: {  sorter:'text' }, 5: {  sorter:'digit' }, 6: {  sorter:'text' },
				      			7: {  sorter:'text' }, 8: {  sorter:'text' }, 9: {  sorter:'date' },
				      			10: {  sorter:'currency' }, 11: {  sorter:false }, 12: {  sorter:false },
				      			13: {  sorter:false }, 14: {  sorter:false }, 15: {  sorter:false },
				            } 
			      		});
			      		
			      		$( "#ResultEstimateToDo" ).tablesorter({ 	
				      		sortList: [[0,1]],  
		                    widgets: ['zebra'],  
		                    dateFormat: 'uk', 
				      		headers: { 
								0: {  sorter:'text' }, 1: {  sorter:'text' }, 2: {  sorter:'text' }, 3: {  sorter:'text' },
				      			4: {  sorter:'text' }, 5: {  sorter:'digit' }, 6: {  sorter:'text' },
				      			7: {  sorter:'text' }, 8: {  sorter:'text' }, 9: {  sorter:'date' },
				      			10: {  sorter:'currency' }, 11: {  sorter:false }, 12: {  sorter:false },
				      			13: {  sorter:false }, 14: {  sorter:false }, 15: {  sorter:false },
				            } 
			      		});
			      		

			      		$( "#ResultEstimateInProgress" ).tablesorter({ 	
				      		sortList: [[0,1]],  
		                    widgets: ['zebra'],  
		                    dateFormat: 'uk', 
				      		headers: { 
								0: {  sorter:'text' }, 1: {  sorter:'text' }, 2: {  sorter:'text' }, 3: {  sorter:'text' },
				      			4: {  sorter:'text' }, 5: {  sorter:'digit' }, 6: {  sorter:'text' },
				      			7: {  sorter:'text' }, 8: {  sorter:'text' }, 9: {  sorter:'shortDate' },
				      			10: {  sorter:'currency' }, 11: {  sorter:false }, 12: {  sorter:false },
				      			13: {  sorter:false }, 14: {  sorter:false }, 15: {  sorter:false },
				            } 
			      		});
			      		
			      		$( "#ResultEstimateEstimateAuto" ).tablesorter({ 	
				      		sortList: [[0,1]],  
		                    widgets: ['zebra'],  
		                    dateFormat: 'uk', 
				      		headers: { 
								0: {  sorter:'text' }, 1: {  sorter:'text' }, 2: {  sorter:'text' }, 3: {  sorter:'text' },
				      			4: {  sorter:'text' }, 5: {  sorter:'digit' }, 6: {  sorter:'text' },
				      			7: {  sorter:'text' }, 8: {  sorter:'text' }, 9: {  sorter:'shortDate' },
				      			10: {  sorter:'currency' }, 11: {  sorter:false }, 12: {  sorter:false },
				      			13: {  sorter:false }, 14: {  sorter:false }, 15: {  sorter:false },
				            } 
			      		});
			      		
			      		//Onglets
			      		$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
					});

					
				/* ]]> */
			   </script>
				
               <script type="text/javascript">
               /* <![CDATA[ */
            		function changeUser( idestimate , user , infos , newInit ){

						//var n = infos.match( "\[[0-9]*\]" );
            			//idUser = n[0].replace('[','').replace(']','') ;
            			
						var data = "IdEstimate="+idestimate+"&NewUser="+user;
				
						$.ajax({
				
							url: "<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_search_estimate.php",
							async: true,
							type: "POST",
							data: data,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
						 	success: function( responseText ){
				
								if( responseText == "1" ){
								
									$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					        		$.blockUI.defaults.css.fontSize = '12px';
					        		$.growlUI( '', 'Modifications enregistrées' );
					        		
					        		$( '#' + infos + idestimate ).hide();
					        		$( '#' + infos + 'a_' + idestimate ).html( newInit );
				        	
				        		}
				        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
				        		
							}
				
						});
            			
            		}
            	/* ]]> */
            	</script>
            	<style type="text/css">
				/* <![CDATA[ */
					.autosuggest ul{ width:150px; }
					.autosuggest .as_header, .autosuggest .as_footer{ width:138px; }
				/* ]]> */
				</style>
				
							
				<!-- Titre -->
				<h1 class="titleSearch">
				<span class="textTitle">Résultats de la recherche : <?php echo $rs->RecordCount() ?> devis - Montant total : <?php echo Util::priceFormat( $totalAmount ); ?></span>
				<span class="selectDate">
					<a href="#bottom" class="goUpOrDown" style="text-decoration:none;font-size:10px; color:#000000;">
						Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a> &nbsp;&nbsp;
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/export_csv.php?type=estimate&amp;req=<?php echo $encryptedQuery ?>">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" />
					</a>
					<a class="blueLink" title="Afficher le diaporama" href="#" onclick="createEstimateSlideshow(); return false;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/diapo_icon.gif" alt="diaporama" />
					</a>
				</span>
				<div class="spacer"></div>
				</h1>
				
				<?php if ( $search == "") { ?>
					<div id="container">
						<!-- Onglet -->
						<!--<ul class="menu">
							<li class="item"><a href="#ToDo"><span><?php echo getNbEstimate($rs, "ToDo"); ?> devis à traiter</span></a>
							<?php if( getNbEstimate($rs, "ToDo") > 0) { ?>
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
							<?php } ?>
							</li>
							<li class="item"><a href="#InProgress"><span><?php echo getNbEstimate($rs, "InProgress"); ?> devis en cours</span></a></li>
							<li class="item"><a href="#EstimateAuto"><span>Devis envoyés automatiquement</span></a></li>
						</ul>	-->							
				<?php }  ?>
				
		                <div class="blocResult mainContent <?php if ( $search == "") { ?>withTab<?php }  ?>" >
		                    <?php if ( $search == "") { ?>
								<div class="content" id="ToDo" style="display:none;">
									<?php if( getNbEstimate($rs, "ToDo") > 0) { ?>
										<?php getEstimate($rs, "ToDo"); ?>
									<?php } ?>
									<div class="spacer"></div>
								</div>
								
								<div class="content" id="InProgress" style="display:none;">
			                        <?php if( getNbEstimate($rs, "InProgress") > 0) { ?>
			                       		<?php getEstimate($rs, "InProgress"); ?>
			                       	<?php } ?>
			                    	<div class="spacer"></div>
			               		</div>
			               		
			               		<div class="content" id="EstimateAuto" style="display:none;">
			                        <?php getEstimate($rs, "EstimateAuto"); ?>
			                    	<div class="spacer"></div>
			               		</div>
			               		
		                    <?php } else { ?>

			                    <div class="content">
			                        <?php getEstimate($rs); ?>
			                    	<div class="spacer"></div>
			               		</div>
		               		<?php } ?>
		                </div>
                
                <?php if ( $search == "") { ?></div><?php } ?>
                
                
                
                <div class="spacer"></div>
<?php } ?>
<?php } ?>
<?php if( $resultCount > 0 && $resultCount <= $maxSearchResults ){ ?>
				<h1 class="titleSearch">
				<span class="textTitle">Récapitulatif</span>
				<span class="selectDate">
					<a href="#topPage" class="goUpOrDown" style="font-size:10px; color:#000000;">
                    	Aller en haut de page
                        <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                    </a>
				</span>
				<div class="spacer"></div>
				</h1>				
				
				<div class="blocResult mainContent">
                    <div class="content">
                        <div class="subContent">
						    <table class="dataTable resultTable">
						    	<thead>
										<tr>
										<th rowspan="2" style="background-color:#FFFFFF; border-width:0px;"></th>
										<th colspan="9">Total devis (HT)</th>	
									</tr>
									<tr>
										<th>Période</th>
										<th>A traîter</th>
										<th>En cours</th>
										<th>Envoyé<br />non relancé</th>
										<th>Envoyé<br />relancé</th>
										<th>Réfusé</th>
										<th>Commandé</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<th style="width:5%;">Volume</th>
										<td style="width:10%;"><?php echo $rs->RecordCount() ?></td>
										<td style="width:10%;"><?php echo $nbatraiter ?></td>
										<td style="width:10%;"><?php echo $nbencours ?></td>
										<td style="width:10%;"><?php echo $nbenvoyeNonRel ?></td>
										<td style="width:10%;"><?php echo $nbenvoyeRel ?></td>
										<td style="width:10%;"><?php echo $nbrefuse ?></td>
										<td style="width:10%;"><?php echo $nbconfirme ?></td>
										
									</tr>
									<?php
									// calcul des pourcentages
									$resultCountRate = 100;
									$nbenvoyeRelRate = ($resultCountRate * $nbenvoyeRel)/$rs->RecordCount();
									$nbenvoyeNonRelRate = ($resultCountRate * $nbenvoyeNonRel)/$rs->RecordCount();
									$nbencoursRate = ($resultCountRate * $nbencours)/$rs->RecordCount();
									$nbatraiterRate = ($resultCountRate * $nbatraiter)/$rs->RecordCount();
									$nbrefuseRate = ($resultCountRate * $nbrefuse)/$rs->RecordCount();
									$nbconfirmeRate = ($resultCountRate * $nbconfirme)/$rs->RecordCount();
									
									?>
									<tr>
										<th style="width:5%; text-align:center;" >%</th>
										<td style="width:10%;"><?php echo Util::rateFormat($resultCountRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbatraiterRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbencoursRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbenvoyeNonRelRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbenvoyeRelRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbrefuseRate , 2) ?></td>
										<td style="width:10%;"><?php echo Util::rateFormat($nbconfirmeRate , 2) ?></td>
										
									</tr>
									<!-- Ligne de séparation -->
									<tr>
										<td colspan="10">&nbsp;</th>
									</tr>
									<!-- Fin ligne de séparation-->
									<tr>
										<th>Montant</th>
										<td><?php echo Util::priceFormat($totalAmount) ?></td>
										<td><?php echo Util::priceFormat($totalatraiter) ?></td>
										<td><?php echo Util::priceFormat($totalencours) ?></td>
										<td><?php echo Util::priceFormat($totalenvoyeNonRel) ?></td>
										<td><?php echo Util::priceFormat($totalenvoyeRel) ?></td>
										<td><?php echo Util::priceFormat($totalrefuse) ?></td>
										<td><?php echo Util::priceFormat($totalconfirme) ?></td>
																		</tr>
									<?php
									// calcul des pourcentages sur montants
									$totalAmountRate = 100;
									$totalenvoyeRelRate = $totalAmount > 0.0 ? ($totalAmountRate * $totalenvoyeRel)/$totalAmount : 0.0;
									$totalenvoyeNonRelRate = $totalAmount > 0.0 ? ($totalAmountRate * $totalenvoyeNonRel)/$totalAmount : 0.0;
									$totalencoursRate = $totalAmount > 0.0 ? ($totalAmountRate * $totalencours)/$totalAmount : 0.0;
									$totalatraiterRate = $totalAmount > 0.0 ? ($totalAmountRate * $totalatraiter)/$totalAmount : 0.0;
									$totalrefuseRate = $totalAmount > 0.0 ? ($totalAmountRate * $totalrefuse)/$totalAmount : 0.0;
									$totalconfirmeRate = $totalAmount > 0.0 ? ($totalAmountRate * $totalconfirme)/$totalAmount : 0.0;
									
									?>
									
									<tr>
										<th style="text-align:center;">%</th>
										<td><?php echo Util::rateFormat($totalAmountRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalatraiterRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalencoursRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalenvoyeNonRelRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalenvoyeRelRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalrefuseRate , 2) ?></td>
										<td><?php echo Util::rateFormat($totalconfirmeRate , 2) ?></td>
										
									</tr>
								</tbody>
							</table>
							<div class="spacer"></div>
						</div>
					</div>
				</div>
				<div class="spacer"></div>
                
                
                <h1 class="titleSearch">
				<span class="textTitle">Provenance</span>
				<span class="selectDate">
					<a href="#topPage" class="goUpOrDown" style="font-size:10px; color:#000000;">
                    	Aller en haut de page
                        <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                    </a>
				</span>
				<div class="spacer"></div>
				</h1>	
                
                
                <div class="blocResult mainContent">
                    <div class="content">
                        <div class="subContent">
                            <!--
                            <a href="#topPage" class="goUpOrDown">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
                            -->
						    <table class="dataTable resultTable">
						    	<thead>
									<tr>
										<th style="background-color:#FFFFFF; border-width:0px;"></th>
										<th><?php echo Dictionnary::translate("estimate_total") ?></th>
<?php
											
											//@todo: problème car on n'affiche qu'un certain nombre de sources et qu'il n'y a aucun criète sur le choix des sources
											//Attention à l'ordre d'affichage. Si on change l'ordre d'affichage ici (tableau $sources), il faut aussi le changer dans le contrôleur
											
                                            /*$sources = array( 1, 9, 5, 3, 6, 13, 2, 10 );
											
											for( $i = 0 ; $i < count( $sources ) ; $i++ ){
												
												$query = "SELECT source$lang FROM source WHERE idsource = " . $sources[ $i ];
												$rssources = DBUtil::query( $query );
												
												echo "				<th style=\"vertical-align:top;\">" . Util::doNothing($rssources->fields( "source$lang" )) . "</th>\n";
												
											}*/
                                            //Rendre la provenance en dynamique
                                            $query = "SELECT * FROM source";
                                            $rs =& DBUtil::query( $query );
                                            while( !$rs->EOF() ){
                                                $sources[$rs->fields( "idsource" )]	= $rs->fields( "source_1" );
                                        		$rs->MoveNext();
                                        	}
                                            foreach($sources as $key=>$value){
                                                echo "<th style=\"vertical-align:top;\">" . $value . "</th>\n";
                                            }
											
											//Calcul de la largeur en pourcentage de chaque colonne afin d'avoir toutes les colonnes à la même taille
											$width = floor( 92 / ( count( $sources ) + 2 ) );
											
?>
										
                                        <!--Rendre la provenance en dynamique <th>Autres</th>-->
									</tr>
								</thead>
								<tbody>
									<tr>
										<th style="width:<?php echo ( 100 - ( count( $sources ) + 2 ) * $width ) ?>%;"><?php echo Dictionnary::translate( "number" ) ?></th>
<?php
									
											//Nombre de devis
											
											/*for( $i = 0 ; $i < count( $number ) ; $i++ ){
												
												if( $i == count( $number ) - 1 )
													$class = " class=\"righterCol\"";
												else
													$class = "";
													
													echo "				<td$class style=\"width:" . $width . "%;\">" . $number[ $i ] . "</th>\n";
												
											}*/
											//Rendre la provenance en dynamique
                                            $totalNb = 0;
                                            foreach($provenance as $key=>$value){
                                                $totalNb += $value["nb"];
                                            }
                                            echo "				<td$class style=\"width:" . $width . "%;\">" . $totalNb . "</th>\n";	
                                            foreach($sources as $key=>$value){
                                                $nb = $provenance[$key]["nb"];
                                                echo "				<td$class style=\"width:" . $width . "%;\">" . $nb . "</th>\n";
                                            }	
?>
									</tr>
									<tr>
										<th><?php echo Dictionnary::translate( "rate_number" ) ?></th>
<?php
											
											//Nombre de devis en %
											
											/*for( $i = 0 ; $i < count( $rate_number ) ; $i++ ){
												
												if( $i == count( $rate_number ) - 1 )
													$style = " class=\"righterCol\"";
												else
													$style = "";
												
												echo "				<td$style>" . Util::rateFormat( $rate_number[ $i ], 0 ) . "</th>\n";
												
											}*/
                                            //Rendre la provenance en dynamique
											echo "				<td$style>" . Util::rateFormat( 100 * $totalNb/ $totalNb, 2 ) . "</th>\n";
                                            foreach($sources as $key=>$value){
                                                $nb = $provenance[$key]["nb"];
                                                echo "				<td$style>" . Util::rateFormat( 100 * $nb/ $totalNb, 2 ) . "</th>\n";
                                            }
?>
									</tr>
									<tr>
										<th><?php echo Dictionnary::translate( "amount" ) ?></td>
<?php
											
											//Montant des devis
											
											/*for( $i = 0 ; $i < count( $amount ) ; $i++ ){
												
												if( $i == count( $amount ) - 1 )
													$style = " class=\"righterCol\"";
												else
													$style = "";
												
												echo "				<td$style>" . Util::priceFormat( $amount[ $i ] ) . "</th>\n";
												
											}*/
											//Rendre la provenance en dynamique
                                            $totalMt = 0;
                                            foreach($provenance as $key=>$value){
                                                $totalMt += $value["supAmount"];
                                            }
                                            echo "				<td$style>" . Util::priceFormat( $totalMt ) . "</th>\n";
                                            foreach($sources as $key=>$value){
                                                $supAmount = $provenance[$key]["supAmount"];
                                                echo "				<td$style>" . Util::priceFormat( $supAmount ) . "</th>\n";
                                            }	
?>
									</tr>
									<tr>
										<th><?php echo Dictionnary::translate( "average_estimate" ) ?></td>
<?php
											
											//Devis moyen
											
											/*for( $i = 0 ; $i < count( $average_estimate ) ; $i++ ){
												
												if( $i == count( $average_estimate ) - 1 )
													$style = " class=\"righterCol\"";
												else
													$style = "";
												
												if( $i == 2 ){
													$style2 = " style=\"color:red;\"" ; 
													echo "<td$style $style2>" . Util::priceFormat( $average_estimate[ $i ], 0 ) . "</th>\n";
												}else{
													echo "<td$style>" . Util::priceFormat( $average_estimate[ $i ], 0 ) . "</th>\n";
												}
											}*/
											//Rendre la provenance en dynamique
                                            echo "<td$style>" . Util::priceFormat( $totalMt / $totalNb, 0 ) . "</th>\n";
                                            foreach($sources as $key=>$value){
                                                $supAmount = $provenance[$key]["supAmount"];
                                                $nb = $provenance[$key]["nb"];
                                                if($nb > 0)echo "<td$style>" . Util::priceFormat( $supAmount / $nb, 0 ) . "</th>\n";
                                                else echo "<td$style>" . Util::priceFormat(0, 0 ) . "</th>\n";
                                            }	
?>
									</tr>
								</tbody>
							</table>
							
						</div>
						<div class="spacer"></div>
					</div>
				</div>
				<div class="spacer"></div><br/>
<?php } ?>
<?php } ?>
               	</form>
            </div>
<a name="bottom"></a>
</div>
<div class="spacer"></div><br/>
</div><!-- centerMax -->


<?php

/********************************* Tableau résultat **********************************/
function getEstimate ( $rs, $statutView ) {
?>

<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>

<table class="dataTable resultTable sortable" id="ResultEstimate<?php echo $statutView?>">
	<thead>
        <tr>
            <th>Com.</th>
            <th>N° devis</th>
            
            <th>N° client</th>
            <th>Raison sociale</th>
            <th>Contact (nom)</th>
            <th>Code postal</th>
            <th>Type</th>                                    
            <th>Provenance</th>
            <th>Statut</th>
            <th><?php echo (isset($statutView) || isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date") ? "Date de création" : "Date d'envoi" ?></th>
            <th>Total HT</th>
			<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<th>Marge brute (%)</th>
			<?php } ?>
				<th>Marge nette (%)</th>
         
            
		
            <?php 
            if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1){
            ?>
            	<th>Conf.<br />récep.</th>
            <?php 
            }
            if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1){
            ?>
        		<th>Conf.<br />lect.</th>
            <?php 
            }
            ?>
            
            <th>Supp.</th>
            
            <!-- Quand une des cases est décochée, le devis n'est pas pris en compte dans les tableaux récapitulatifs -->
            <!--<th><img src="<?php echo $GLOBAL_START_URL ?>/www/icones/calculer.gif" alt="Utilisation du devis dans les calculs" /></th>-->
        </tr>
    </thead>
   <tbody>

	<?php
			
	//Parcours de chacune des lignes de résultat
	
	$rs->MoveFirst();
	
	$netMarginAmountSum		= 0.0;
	$roughMarginAmountSum 	= 0.0;
	$total_amount_htSum 	= 0.0;
	
	$userList = DBUtil::query( "SELECT iduser, firstname, lastname, initial FROM `user` WHERE hasbeen = 0 ORDER BY firstname ASC" );											
	

	
	
	for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
		
		$Int_IdBuyer		= $rs->Fields("idbuyer");
		$Int_Iderp			= $rs->Fields("iderp");
		$company			= $rs->Fields("company");
		$Int_IdEstimate		= $rs->Fields("idestimate");
		$Int_IdDelivery		= $rs->Fields("iddelivery");
		$Int_IdPayment		= $rs->Fields("idpayment");
		$Str_Status			= $rs->Fields("status");
		$valid_until		= usDate2eu( $rs->Fields("valid_until") );
		$Str_Lastname		= $rs->Fields("contact_lastname");
		$Str_Name			= getTitle($rs->Fields("title")).' '.$rs->Fields("contact_lastname");				
		$relaunch			= usDate2eu( $rs->Fields("relaunch") );
		$totalht			= $rs->Fields("total_amount_ht");
		$totalcharge		= $rs->Fields("total_charge");
		$billing			= $rs->Fields("billing_amount");
		$idsource			= $rs->fields( "idsource" );
		$initials			= $rs->fields( "initial" );
		$zipcode			= $rs->fields( "zipcode" );
		$style 				= "";
		$use_estimate 		= $rs->fields( "use_estimate" );
		$isContact			= $rs->fields( "contact" ) ;
		$statusReceipt 		= $rs->fields("status_return_receipt");
		$statusNotification = $rs->fields("status_delivery_notification");
		
		
		if( isset($statutView) || ( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == "creation_date" ) )
			$Str_Date = usDate2eu( substr( $rs->fields( "DateHeure" ), 0, 10 ) );
		else
			$Str_Date = usDate2eu( substr( $rs->fields( "sent_date" ), 0, 10 ) );
		
		// Calcul des marges
		$idestimates = array();
		$idestimates[] = $Int_IdEstimate;
		
		$roughMargin = 0;
		$roughMarginRate = 0;

		$roughMarginRate 		= $rs->fields( "gross_margin_rate" );
		$netMarginRate 			= $rs->fields( "net_margin_rate" );
		$roughMarginAmountSum 	+= $rs->fields( "gross_margin_amount" );;
		$netMarginAmountSum 	+= $rs->fields( "net_margin_amount" );;
		$discount_price 		= $rs->fields( "discount_price" ) === null ? 0.0 : $rs->fields( "discount_price" );
		$total_amount_htSum 	+= ( $discount_price - $rs->fields( "total_discount_amount" ) - $rs->fields( "billing_amount" ) );
								
		$ck = $use_estimate == 1 ? " checked=\"checked\"" : "";
		
		if( $Str_Status == "InProgress" )
			$class = "blueText";
		elseif( $Str_Status == "ToDo" )
			$class = "orangeText";
		else
			$class = "blackText";

        $userlogged = User::getInstance()->get( 'login' );
        $css="";
        
        if( ( $userlogged == 'sile') && ( $rs->fields('naf') == "" || $rs->fields('siret') =="" ) ){
        	$css="<span style='color:#ff0000;font-size:20px;font-weight:bold;'>*</span>";
        } ?>
        
       
        
        <?php
        
        if( !isset($statutView) || 
        	(isset($statutView) && $statutView == $Str_Status) || 
        	(isset($statutView) && $statutView == "EstimateAuto" && $rs->fields( "auto" ) == "1" && $Str_Status == "Send")  
        ) {
        ?>	
        
        
        
    	<tr id="EstimateRow<?php echo $rs->fields( "idestimate" ); ?>" class="blackText<?php if( $rs->fields( "isInternalCustomer" ) ) echo " firstDraft"; ?>">
        	<td style="width:3%;" class="lefterCol">

            	<a title="<?php echo $initials ? $initials : '?' ?>" href="#" id="commercialEst_a_<?php echo $Int_IdEstimate ?>" onclick="$('#commercialEst_<?php echo $Int_IdEstimate ?>').show();return false;"><?php echo $initials ? $initials : '?' ?></a>
            	<select name="commercialEst_<?php echo $Int_IdEstimate ?>" id="commercialEst_<?php echo $Int_IdEstimate ?>" style="width:200px;margin-left:-20px;z-index:999;position:absolute;display:none;" onblur="$(this).hide()" onchange="changeUser( <?php echo $Int_IdEstimate ?> , $('#commercialEst_<?php echo $Int_IdEstimate ?> :selected').val() , 'commercialEst_' , $('#commercialEst_<?php echo $Int_IdEstimate ?> :selected').attr('id') );">
            		<?php
            		
            		$userList->MoveFirst();
            		while( !$userList->EOF() ){
            			echo "<option value='".$userList->fields('iduser')."' id='".$userList->fields('initial')."'>".$userList->fields('firstname')." ".$userList->fields('lastname')."</option>";
            		
            			$userList->MoveNext();
            		}
            		?>
            	</select>
            	<?php
            	include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "commercialEstimate_$Int_IdEstimate", "user", "initial", "[{iduser}] {firstname} {lastname}", false , "function(obj){ changeUser( $Int_IdEstimate , $('#commercialEstimate_$Int_IdEstimate').val() , obj.info );}" );
            	?>
        	</td>
            <td style="width:7%;" valign="middle" style="white-space: nowrap;">
            	<a class="grasBack" href="com_admin_devis.php?IdEstimate=<?php echo $Int_IdEstimate ?>" onclick="window.open(this.href); return false;"><?php echo $Int_IdEstimate ?></a>
            	<span><?php echo GenerateHTMLForToolTipBox( $Int_IdEstimate ) ?></span>
            </td>
            <td style="width:5%;">
            	<?php if( !$rs->fields( "isInternalCustomer" ) ){ ?>
            	 <a href=" <?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Int_IdBuyer ?>" target="_blank">
            		<?php 
            		if(isset($Int_Iderp) && $Int_Iderp != "" && $Int_Iderp != 0 ){
            			echo $Int_Iderp . "/" . $Int_IdBuyer ; 
            		}else{
            			echo $Int_IdBuyer;
            		}
            		?>
            		<?php echo $css?>
            	 </a>
            	<?php } ?>
            </td>
            <td style="width:14%;">
            	<a class="grasBack" target="_blank" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Int_IdBuyer ?>">
            		<?php if(!empty($company) && $company != "" && $company != " ") { echo $company ; } else { echo $Str_Name; } ?>
            	</a>
            </td>
            <td style="width:14%;">
            <?php if( !$rs->fields( "isInternalCustomer" ) ){ ?>
            	<a title="<?php echo $Str_Lastname ?>" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Int_IdBuyer ?>" target="_blank"><?php echo $Str_Name ?></a>
            <?php } ?>
            </td>
            <td style="width:6%;"><?php if( !$rs->fields( "isInternalCustomer" ) ) echo $zipcode; ?></td>
             <td>
            	<?php
                	switch( $isContact ){
                		case "0":
                			echo "Client";
                			break;
                		case "1":
                			echo "Prospect";
                			break;
                	}
            	?>
            </td>
            <td style="width:11%;"><?php echo htmlentities( $rs->fields( "source_1" ) ); ?></td>
            <td style="width:7%;" class="<?php echo $class ?>">
            	<b><?php 
            	
            	if( $Str_Status == Estimate::$STATUS_REFUSED && DBUtil::getDBValue( "idestimate", "logismarket_categories", "idestimate", $Int_IdEstimate ) )
            		echo "Rejeté";
            	else echo Dictionnary::translate( $Str_Status ); 
            
            	?></b>
            </td>
            <td style="width:7%;"><?php echo $Str_Date ?></td>
            <td style="text-align:right; width:7%;"><?php echo Util::priceFormat( $totalht ) ?></td>
			<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<td style="width:6%;"><?php echo Util::rateFormat( $roughMarginRate ) ?></td>
			<?php } ?>
				<td style="width:6%;" class="righterCol">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<?php echo Util::rateFormat( $netMarginRate ) ?>
					 <?php } ?>
				<?php if($akilae_comext ==1){ ?> / 	 
				<span style="color:red !important;"><?php echo Util::rateFormat( $netMarginRate - $_SESSION['rate'] ) ?></font></td>
				<?php } ?>
            

       		<?php  if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1){ ?>
            <td>
            	<?php  if($statusReceipt == 1){ ?>
            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailbox-active.png" alt="" title="Une confirmation de reception a été reçue pour le devis"/>
            	<?php } else{ ?>
            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailbox-inactive.png" alt="" title="Aucune confirmation de réception n'a été reçue pour le devis"/>
            	<?php } ?>
            	</td>
            <?php  } if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1){ ?>
        		<td>
        		<?php if($statusNotification == 1){ ?>
            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailopen-active.png" alt="" title="Une confirmation de lecture a été reçue pour le devis"/>
            	<?php } else { ?>
            		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailopen-inactive.png" alt="" title="Aucune confirmation de lecture n'a été reçue pour le devis"/>
            	<?php } ?>
        		</td>
            <?php } ?>
            <td style="width:4%;">
            	<?php if( $Str_Status == "InProgress" || $Str_Status == "ToDo" ){ ?><img src="/images/back_office/content/corbeille.jpg" alt="Supprimer le devis" onclick="return deleteEstimate(<?php echo $rs->fields( "idestimate" ); ?>);" style="cursor:pointer;" /><?php } ?>
            </td>
        </tr>
        <?php } ?>
        
		<?php $rs->MoveNext();
				
	} ?>
    </tbody>
</table>
<?php
}

function getNbEstimate ( $rs, $statutView ) {

			
	//Parcours de chacune des lignes de résultat
	$rs->MoveFirst();
	$return = 0;

	for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
		

		$Str_Status = $rs->Fields("status");
		        
        if( $statutView == $Str_Status ) {
        	$return++;
        }

        $rs->MoveNext();
				
	}
	
	return $return;	
}
?>



