<?php 
include_once( dirname( __FILE__ ) . "/../../../objects/ProductDocument.php" );
?>
<script type="text/javascript"> 
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------- */
		
	function updateOrder( property, value ){
		
		var data = "property=" + escape( property ) + "&value=" + escape( value );
		
		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_order.php?update&idorder=<?php echo $Order->getId(); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sauvegarder les modifications" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Impossible de sauvegarder les modifications : " + responseText ); 
					return;
					
				}
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );
    				
			}

		});
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
		
	function showSupplierInfos( idsupplier){
					
		if( idsupplier == '0' )
			return;

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
            		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
			}

		});
		
	}
	
	function popupStock(ref)
	{
		postop = (self.screen.height-400)/2;
		posleft = (self.screen.width-400)/2;
	
		window.open('popup_stock.htm.php?r='+ref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=700,height=500,resizable,scrollbars=yes,status=0');
		
	}
	
	function changeSupplierCharges( idsupplier , amount ){
		
		var elementID = 'SupplierChargesAuto_' + idsupplier;
		var element = document.getElementById( elementID );
		
		if( element.value == 1 )
			setSupplierChargesAuto( idsupplier, amount );
		else
			restoreSupplierCharges( idsupplier );
		
	}
	
	function setSupplierChargesAuto( idsupplier, amount ){
		
		var elementID = 'supplier_charges_' + idsupplier;
		var element = document.forms.frm.elements[ elementID ];
		
		element.backup = element.value;
		element.value = amount;
		element.disabled = 'disabled';
		
	}
	
	function restoreSupplierCharges( idsupplier ){
		
		var elementID = 'supplier_charges_' + idsupplier;
		var element = document.forms.frm.elements[ elementID ];
		
		if( element.backup != null )
			element.value = element.backup;
		
		element.disabled = '';
		
	}
	
	function changeInternalSupplierCharges( idsupplier , amount ){
		
		var elementID = 'InternalSupplierChargesAuto_' + idsupplier;
		var element = document.getElementById( elementID );
		
		if( element.value == 1 )
			setInternalSupplierChargesAuto( idsupplier, amount );
		else
			restoreInternalSupplierCharges( idsupplier );
		
	}
	
	function setInternalSupplierChargesAuto( idsupplier, amount ){
		
		var elementID = 'internal_supplier_charges_' + idsupplier;
		var element = document.forms.frm.elements[ elementID ];
		
		element.backup = element.value;
		element.value = amount;
		element.disabled = 'disabled';
		
	}
	
	function restoreInternalSupplierCharges( idsupplier ){
		
		var elementID = 'internal_supplier_charges_' + idsupplier;
		var element = document.forms.frm.elements[ elementID ];
		
		if( element.backup != null )
			element.value = element.backup;
		
		element.disabled = '';
		
	}
	
	function setChargesAuto( amount ){
		
		var elementID = 'total_charge_ht';
		var element = document.forms.frm.elements[ elementID ];
		
		element.backup = element.value;
		
		element.value = amount != "Franco" && amount > 0.0 ? amount : "Franco";
		element.disabled = 'disabled';
		
		$( "#charge_free" ).val( "0" );
		
	}
	
	function restoreCharges(){
	
		var elementID = 'total_charge_ht';
		var element = document.forms.frm.elements[ elementID ];
		
		if( element.backup != null )
			element.value = element.backup == "Offert" ? "0,0" : element.backup;
		
		element.disabled = '';

		$( "#charge_free" ).val( "0" );
		
	}

	function chargeFree(){

		document.forms.frm.elements[ "total_charge_ht" ].value = "Offert";
		document.forms.frm.elements[ "total_charge_ht" ].disabled = "disabled";
		$( "#charge_free" ).val( "1" );
		
	}
	
	function substitute( idrow ){
	
		postop = ( self.screen.height - 400 ) / 2;
		posleft = (self.screen.width - 400 ) / 2;

		var options ='top=' + postop + ',left=' + posleft + 'directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=800, height=500';
		var location = '<?php $GLOBAL_START_URL ?>/sales_force/substitute.php?idorder=<?php echo $IdOrder ?>&idrow=' + idrow;
		
		window.open( location, '_blank', options ) ;
		
	}

	/* ------------------------------------------------------------------------------------------------- */
	
	var colorIndex = 0;
	
	/* ------------------------------------------------------------------------------------------------- */
			
	function colorDialog( itemIndex, idproduct ){

		colorIndex = itemIndex;
		
		$.ajax({
		 	
			url: "/product_management/catalog/colors.php?callback=addColor&idproduct=" + idproduct,
			async: true,
			type: "GET",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les couleurs" ); },
		 	success: function( responseText ){

				$( "#ColorDialog" ).html( responseText );
				$( "#ColorDialog" ).dialog({
			        
					modal: true,	
					title: "Couleurs disponibles",
					close: function(event, ui) { 
					
						$("#ColorDialog").dialog( "destroy" );
						
					},
					width: 600,
					height: 600
					
				});
				
			}

		});

	}

	/* ------------------------------------------------------------------------------------------------- */
	
	function addColor( idcolor ){
		
		$( "#idcolor" + colorIndex ).val( idcolor );
		$( "#frm" ).submit();
		
	}

	/* ------------------------------------------------------------------------------------------------- */
	
	function addDeleteDocument( add , iddocument , idestimate ){

		todoaction = 'delete_document';
		if( add ){
			todoaction = 'add_document';
		}

		var postDatas = "idorder=" + idestimate + "&iddocument=" + iddocument;

		$.ajax({
		 	
			url: "/sales_force/order_parseform.php?" + todoaction,
			async: true,
			type: "POST",
			data: postDatas,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter le document" ); },
		 	success: function( responseText ){
			 	if(responseText=='1'){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
		        	$.growlUI( '', 'Modifications enregistrées' );
			 	}else{
					alert( responseText );
			 	}
			}

		});		
		
	}
	
/* ]]> */
</script>
<script type="text/javascript">
<!--
	//onglets
	$(function() {
		$('#containerEstimate').tabs({ fxFade: true, fxSpeed: 'fast' });
	});
-->
</script>
<div class="spacer"></div>
	
<div class="contentResult" style="margin-bottom: 10px;">

	<h1 class="titleEstimate"><span class="textTitle">Descriptif de la commande</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>       
	<a name="references"></a>
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">
		<div id="containerEstimate">
			<ul class="menu" style="display:<?php echo strlen( $disabled ) ? "none" : "block"; ?>;">
				<li class="item"><a href="#add-ref"><span>Ajouter une référénce</span></a></li>
				<li class="item"><a href="#search-product"><span>Rechercher par critères</span></a></li>
            	<li class="item"><a href="#create-product"><span>Créer un produit</span></a></li>
            	<li class="item"><a href="#search-cata" onclick="window.open('<?php echo URLFactory::getHostURL(); ?>'); return false;"><span>Rechercher sur le catalogue</span></a></li>
			</ul>
			<div class="spacer"></div>
			<div style="border-top: 1px solid rgb(208, 208, 208); margin: 0 0 8px -5px; width: 101%; display:<?php echo strlen( $disabled ) ? "none" : "block"; ?>;"> </div>
			<div class="spacer"></div>
			
			
			<!-- ********************* -->
			<!-- Ajouter une référénce -->
			<!-- ********************* -->
			<div id="add-ref">
				<?php if( empty( $disabled ) ){ ?>
			 	<div class="blocMultiple blocMLeft" style="width:85%">
		    		<form action="com_admin_order.php?IdOrder=<?php echo $Order->getId(); ?>#lastaction" method="post" name='searchfrm' id='searchfrm' enctype="multipart/form-data">
		    		
		    		<label style="width:23%"><span>Ajout d'une référence à la commande</span></label> 
		    		<span class="textSimple" style="margin:0;">
						<input type="text" name="directref" id="directref" class="textInput" style="width:100px; margin:0;" />
						<style type="text/css">

							#as_directref ul{ width:250px;}
							#as_directref .as_header,
							#as_directref .as_footer{ width:238px;}
							
						</style>
						<?php
						
							include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            				AutoCompletor::completeFromDBPattern( "directref", "detail", "reference", "{summary_1}", 15 );
            
            			?>
						<span class="floatleft" style="margin:5px 3px 0 3px; "> x </span>
						<input type="text" name="directqte" name="directqte" value="1" class="textInput" style="width:20px; margin:0;" onkeypress="return forceUnsignedIntegerValue( event );" />
						<input type="submit" class="blueButton" name="AddDirectRef" value="Ajouter" style="margin:1px 3px 0 3px; "  />
					</span>
					
					</form>
				</div>
		        <div class="spacer"></div>      
				<?php } ?>	
			</div> <!-- #add-ref -->
			
			
			<!-- ********************* -->
			<!--   Créer un produit    -->
			<!-- ********************* -->
			<div id="create-product" style="display:none;">
				<script type="text/javascript"> 
				/* <![CDATA[ */
					function addProduct() {
						
						var data = $('#frmCreate').formSerialize(); 
						$.ajax({
							url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/new_article.php?IdOrder=<?php echo $IdOrder; ?>",
							async: true,
							type: "POST",
							data: data,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de rechercher un produit" ); },
						 	success: function( responseText ){
						 		
						 		$("#responseAddProduct").html(responseText); 		
							}
						});
					}
				/* ]]> */
				</script>	
				
				<div class="blocMultiple blocMLeft">
				<form id="frmCreate" action="#" method="post" enctype="multipart/form-data">
					<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>" />
					<input type="hidden" name="AddReference" value="On" />
				
					<label><span><?php echo Dictionnary::translate('type') ?></span></label>
					<select name="type">
						<option value="alternativ"><?php echo Dictionnary::translate('alternative') ?></option>
						<option value="specific"><?php echo Dictionnary::translate('specific') ?></option>
					</select>
					<div class="spacer"></div>
					
					
					<input type="button" onclick="return addProduct();" style="margin:5px 0;" class="blueButton floatright" value="Sélectionner" name="AddReference" />
				</form></div>	
				<div class="spacer"></div>
				<div id="responseAddProduct"></div>
				<div class="spacer"></div>
				
			</div> <!-- #create-product -->
			
			<!-- ********************* -->
			<!-- Rechercher  catalogue -->
			<!-- ********************* -->
			<div id="search-cata" style="display:none;"></div><!-- #search-cata -->
			
			
			<!-- ********************* -->
			<!-- Rechercher un produit -->
			<!-- ********************* -->
			<div id="search-product" style="display:none;">
				<script type="text/javascript">
				/* <![CDATA[ */
				
					function addReference( idarticle ){
	
						document.location = "/sales_force/com_admin_order.php?IdOrder=<?php echo $IdOrder; ?>&IdProduct=" + idarticle;
						
					}
					
				/* ]]> */
				</script>
				<?php 
			
				include( dirname( __FILE__ ) . "/../../../sales_force/reference_search.php" ); 
				displaySearchForm( "addReference" );
				
			?>
			</div> <!-- #search-product -->
			<div class="spacer"></div>

		</div>
		<div class="spacer"></div>


<form action="com_admin_order.php?IdOrder=<?php echo $Order->getId(); ?>#lastaction" method="post" name='frm' id='frm' enctype="multipart/form-data">
	<input type="hidden" name="iduser" id="iduser" value="<?php echo $Order->get('iduser') ?>" />
	<input type="hidden" name="sendmail" id="sendmail" value="0" />
	<input type="hidden" name="sendmailcancel" id="sendmailcancel" value="0" />
	<input type="hidden" name="sendmailpaiement" id="sendmailpaiement" value="0" />
	<input type="hidden" name="ModifyOrder" id="ModifyOrder" value="1" />
	<input type="hidden" name="IdOrder" value="<?php echo $Order->getId() ?>" />
	<input type="hidden" name="terminate" value="0" />
	<input type="hidden" name="DdeProForma" value="0" />
	<input type="hidden" name="ConfCom" value="0" />
		
		
		<?php if( strlen($Order->get( "comment" )) > 5 ) { ?>
    	<div class="blocMultiple blocMRight" style="width:100%;">	
	    	<label style="width:100%; background:none;"><span style="margin-left:0;">Commentaires du client</span></label>
            <div class="spacer"></div>	
             
            <div id="clientsCommentsDiv">
                <textarea disabled="disabled" name="comment" class="textInput" style="width: 100%; height:80px;"><?php 
					echo html_entity_decode( $Order->get( "comment" ) );
				?></textarea>
                <div class="spacer"></div>	
            </div>
            <div class="spacer"></div>
	    </div>     
	    <?php } ?> 	
	        
	
		<?php
			anchor( "Delete_", true );
			anchor( "Updqte_", true );
			anchor( "UpdateRefDiscountRate_", true );
			anchor( "UpdateOrderPriceWD_", true );
			anchor( "UpdateUnitCostRate_", true );
			anchor( "UpdateUnitCostAmount_", true );
			anchor( "UpdateDesignation_", true );
			anchor( "UpdateSupplierCharges_", true );
			anchor( "UpdateInternalSupplierCharges_", true ); 
			anchor( "UpdRow_", true ); 

			$rows = getIdRowsSortedBySupplier();
			
			$currentSupplier = 0;
			for ( $k = 0; $k < count( $rows ); $k++ ) {
				
				$i = $rows[ $k ]; //rows triées par fournisseur @todo tri dans l'objet!!!

				?>
				<div class="floatleft" style="width:89%; margin-bottom:10px;">
					<?php displayItem( $Order, $Order->getItemAt( $i ), $disabled ); ?>
				</div>
				<div class="floatright" style="width:10%;">
					<?php displayItemMargins( $Order->getItemAt( $i ) ); ?>
				</div>
				<div class="clear"></div>
				<?php
				
				/*frais de port fournisseur*/
				if( $k == $Order->getItemCount() - 1 || $Order->getItemAt( $i)->get( "idsupplier" ) != $Order->getItemAt( $rows[ $k + 1 ])->get( "idsupplier" ) ){
					displaySupplierCharges( $Order->getItemAt( $i)->get( "idsupplier" ) );	
				}
			}
			
		?>
		<div class="spacer"></div>
	</div></div>
</div>
<div class="spacer"></div>

<?php

//-------------------------------------------------------------------------------------------------------
/**
 * Afficher une ligne article donnée
 * @param OrderItem $item la ligne article à afficher
 * @param $disabled l'état d'activation des champs de saisie
 */					
function displayItem( Order $order, &$item, $disabled ){
	
	global $GLOBAL_START_URL;
		
	/*@todo coefficients de vente à placer ( aussi dans le devis mon canard )
	
	$rough_sale_coeff = $item->get( "unit_cost" ) > 0.0 ? $item->get("unit_price" ) / $item->get( "unit_cost" ) : 0.0;
	$sale_coeff = $item->get( "unit_cost_amount" ) > 0.0 ? $item->get( "discount_price" ) / $item->get( "unit_cost_amount" ) : 0.0;
	*/
	
	/*vérification du stock*/
	
	if( in_array( $order->get( "status" ), array( Order::$STATUS_TODO, Order::$STATUS_IN_PROGRESS, Order::$STATUS_DEMAND_IN_PROGRESS ) ) )
		checkStockLevel( $item ); 
		
	?>
	<!--
	<div class="floatleft" style="margin-bottom:5px; width:100%;">
		<table class="dataTable devisTable">
		    <tr>
				<th>Référence</th>
				<th>Désignation</th>
				<th>Quantité</th>
				<th>Prix vente<br />tarif unitaire <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
		    	<th>Remise<br />client %</th>
		    	<th>Prix vente<br />net unitaire <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
		    	<th>Prix vente<br />total <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
				<th></th>
		    </tr>
				<td>
					<?php $refText = $item->get( "reference" ); ?>
					<p><?php echo $refText ?></p></td>
				<td style="text-align:left;">
				<textarea name="designation[<?php echo $item->get( "idrow" ) ?>]" style="width:210px; height:25px; border-style:none;"><?php echo (stripDesignationTags( $item )) ?></textarea></td>
				<td><?php displayQuantity( $item, $disabled ); ?></td>
				<td><input<?php echo $disabled ?> type="text" class="textInput price" name="unit_price[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo  Util::numberFormat( $item->get( "unit_price" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;&euro;</td>
				<td><?php /*remise client %*/ displayCommercialDiscount( $item, $disabled ); ?></td>
				<td><?php /*prix unitaire vente net*/	 displayDiscountPrice( $item, $disabled ); ?></td>
				<td><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "discount_price" ) ) ?></td>		
				<td width="150">
		<div class="floatleft">
		<input type="hidden" name="DeleteItem[<?php echo $item->get( "idrow" ) ?>]" value="0" />
        <input type="submit" name="" class="blueButton" value="Supprimer" onclick="if( !confirm( 'Voulez-vous vraiment supprimer la référence \'<?php echo $item->get( "reference" ) ?>\' ?' ) ) return false; document.forms.frm.elements[ 'DeleteItem[<?php echo $item->get( "idrow" ) ?>]' ].value = 1;" />
        <input type="submit" class="blueButton" name="" value="Recalculer" />
    </td>
	</tr>
	</table>
	
    <div class="clear"></div>
	-->
	
	<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>
	
	<div class="floatleft" style="margin-bottom:5px; width:100%;">
		<table class="dataTable devisTable">
		    <tr>
		    	<th style="width:75px;">Photo</th>
		    	<th>Réf. & désignation courte</th>
		    	<th>		    	
			    	<?php if($item->get('quantity_per_linear_meter') > 0 || $item->get('quantity_per_truck') > 0 || $item->get('quantity_per_pallet') > 0) { ?>
	            	<style type="text/css">
	            		.tooltipfrshover2 { background-color:#FFFFFF; border:1px solid lightgray; text-align:left; padding:0 5px;
							color:#8B9298; display:block; left:70px; position:absolute; top:-7px; width:170px; z-index:99;
						}
	            	</style>
	            	<span onmouseover="document.getElementById('popfrs<?php echo $item->get( "idrow" ) ?>').className='tooltipfrshover2';" onmouseout="document.getElementById('popfrs<?php echo $item->get( "idrow" ) ?>').className='tooltipfrs';" style=" position:relative; cursor:pointer; ">
	                	Quantité <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" onmouse style="border-style: none; vertical-align: text-bottom;" alt="" />
	                	
	                	<span id='popfrs<?php echo $item->get( "idrow" ) ?>' class='tooltipfrs' style="font-size:11px;">
			    		<?php if($item->get('quantity_per_linear_meter') > 0) { ?>
	                		Quantité par ML : <?php echo $item->get('quantity_per_linear_meter') ?><br />
	                	<?php } ?>
	                	<?php if($item->get('quantity_per_truck') > 0) { ?>
	                		Quantité par camion : <?php echo $item->get('quantity_per_truck') ?><br />
	                	<?php } ?>
	                	<?php if($item->get('quantity_per_pallet') > 0) { ?>
	                		Quantité par palette : <?php echo $item->get('quantity_per_pallet') ?><br /> 
	                	<?php } ?></span>
	                </span>
	            	<?php } else { ?>
	            		Quantité                            	
	            	<?php } ?>    	
		    	</th>
		    	<th>Prix vente<br />tarif unitaire <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
		    	<th>Remise<br />client %</th>
		    	<th>Prix vente<br />net unitaire <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
		    	<th>Prix vente<br />total <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
		    </tr>
		    <tr>
		    	<td rowspan="4" style="vertical-align:top;">
				<?php 
				
					/*options */ 			displayOptionLink( $item ); 
					/*produits similaires*/	displaySimilarLink( $item );
					/*image*/				displayReferenceThumb( $item );
					/*promotion*/			displayPromoLogo( $item );
					/*destockage*/			displayDestockingLogo( $item );
					/*prix au volume*/		displayVolPriceLink( $order, $item );
					
				?>
                <div class="spacer"></div><br/>
				Ordre PDF
                <input style=" width:50px; " class="textInput price" type="text" name="idrow_line[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo  $item->get("idrow_line") ?>" onkeypress="return forceFloatValue( event );" />
                
		    	</td>
		    	<td style="text-align:left;"><?php displayReferenceInfos( $item, strlen( $disabled ) == 0 ); ?></td>
				<td rowspan="3">
				<?php 
				
					/*popup stock*/ 	displayStockPopup( $item );
					/*minimum de cde*/	checkMinOrder( $item );
					/*quantité*/		displayQuantity( $item, $disabled );
				
				?>
				</td>
				<td><input<?php echo $disabled ?> type="text" class="textInput price" name="unit_price[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo  Util::numberFormat( $item->get( "unit_price" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;&euro;</td>
				<td><?php /*remise client %*/ displayCommercialDiscount( $item, $disabled ); ?></td>
				<td><?php /*prix unitaire vente net*/	 displayDiscountPrice( $item, $disabled ); ?></td>
				<td><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "discount_price" ) ) ?></td>		
			</tr>
			<tr>
		    	<td rowspan="3" style="width:210px; height:110px;">
					<textarea<?php echo $disabled ?> name="designation[<?php echo $item->get( "idrow" ) ?>]" style="width:210px; height:110px; border-style:none;"><?php echo (stripDesignationTags( $item )) ?></textarea>
				</td>
				<th>Prix vente fabricant HT</th>
		    	<th>Remise<br />fournisseur %</th>
		    	<th>Prix achat<br />net unitaire HT</th>
		    	<th>Prix achat<br />total HT</th>
		    </tr>
			<tr>
				<td><?php echo Util::priceFormat( $item->get( "unit_cost" ) ) ?></td>
				<td>
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				
				<input<?php echo $disabled ?> type="text" class="textInput percentage" name="unit_cost_rate[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo Util::numberFormat( $item->get( "unit_cost_rate" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;%
				<?php } ?>
				<?php if($akilae_comext ==1){ ?>
				<br><br>
				<span style="color:red !important;">Vide</span></td>
				<?php } ?>
				<td>
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<input<?php echo $disabled ?> type="text" class="textInput price" name="unit_cost_amount[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo Util::numberFormat( $item->get( "unit_cost_amount" )) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;&euro;
				<?php } ?>
				<?php if($akilae_comext ==1){ ?>
				<br><br>
				<span style="color:red !important;">Vide</span></td>
				<?php } ?>
				<td>
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "unit_cost_amount" ) ) ?>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?>
				<br><br>
				<span style="color:red !important;"><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "unit_cost_amount" ) * (1 + $_SESSION['rate'] / 100)) ?></span>
				<?php } ?>
				
				</td>	
			</tr>
			<tr>
				<th>Stock<br />interne</th>
				<th>Stock<br />externe</th>
				<th>Expédition</th>
				<th>Poids unitaire</th>
				<th>Poids total</th>
			</tr>
			<tr>
				<td colspan="2">
					<div class="leftContainer" >
						<input<?php echo $disabled ?> type="checkbox" class="valignCenter" name="useimg[<?php echo $item->get( "idrow" ) ?>]" value="1"<?php if( $item->get( "useimg" ) ) echo " checked=\"checked\""; ?> />
		            </div>
		            <div class="leftContainer blueText" style="text-align:left;margin-left:5px;">
		                Utiliser photo
		            </div>
		            <div class="leftContainer" style="margin-left:15px;">
		            	<input<?php echo $disabled ?> type="checkbox" name="usepdf[<?php echo $item->get( "idrow" ) ?>]" value="1"<?php if( $item->get( "usepdf" ) ) echo " checked=\"checked\""; ?> />
		            </div>
		            <div class="leftContainer blueText" style="text-align:left;margin-left:5px;">
		                Joindre fiche produit
		            </div><?php 
					$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
					$documentsProd = ProductDocument::getDocumentsProduct( $idproduct );                 
                            
					if( $documentsProd ){
                        ?>
                        <div class="spacer"></div>
                        <?php 
						foreach( $documentsProd as $documentProd ){ 
                            
                            $checkid = DBUtil::query( "SELECT count(*) as checkid 
                            				FROM order_row_product_document 
                            				WHERE unique_id = " . $documentProd->getDocumentUniqueId() . " 
                            				AND idorder = '" . $item->get( "idorder" ) . "'  LIMIT 1" )->fields('checkid');
                            
                            ?>
                            <div class="leftContainer" >
                                <input <?php echo $checkid ? "checked='checked'" : "" ?> onclick="addDeleteDocument( $(this).attr( 'checked' ) , $(this).val() , <?php echo $order->getId() ?> )" type="checkbox" name="product_document" class="valignCenter" value="<?php echo $documentProd->getDocumentUniqueId() ?>" />
                            </div>
                            <div class="leftContainer blueText" style="text-align:left;margin-left:5px;">
                                Joindre : <?php echo $documentProd->getDocumentName(); ?>
                            </div>
                            <div class="spacer"></div>	                            		
                            <?php
                            
						}
                            	
					}
					?>
                    
                    
                              
				</td>
				<td><?php echo intval( $item->get( "quantity" ) - $item->get( "external_quantity" ) ) ?></td>
				<td>
					<input<?php if( $item->get( "idsupplier" ) == DBUtil::getParameterAdmin( "internal_supplier" ) ){ echo " disabled=\"disabled\""; }else echo $disabled ?> type="text" class="textInput quantity" name="external_quantity[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo $item->get( "external_quantity" ) ?>" onkeypress="return forceUnsignedIntegerValue( event );" />
				</td>
				<td><?php if( !DBUtil::getParameterAdmin("view_delay_delivery") ) DrawLift_Delay( "delivdelay[" . $item->get( "idrow" ) . "]", $item->get( "delivdelay" ), empty( $disabled ), "" ); ?></td>
				<td><?php echo Util::numberFormat( $item->get( "weight" ) ) . " kg"; ?></td>
				<td><?php echo Util::numberFormat( $item->get( "quantity" ) * $item->get( "weight" ) ) . " kg"; ?></td>
			</tr>
		</table>
	</div><!--  float:none; -->
	<div class="spacer"></div>
	
	<div class="floatleft">
		<input type="hidden" name="DeleteItem[<?php echo $item->get( "idrow" ) ?>]" value="0" />
        <input type="submit" name="" class="blueButton" value="Supprimer" onclick="if( !confirm( 'Voulez-vous vraiment supprimer la référence \'<?php echo $item->get( "reference" ) ?>\' ?' ) ) return false; document.forms.frm.elements[ 'DeleteItem[<?php echo $item->get( "idrow" ) ?>]' ].value = 1;" />
        <!--<input type="button" class="blueButton" value="Substituer" style="margin-left:5px;" onclick="substitute( <?php echo $item->get( "idrow" ) ?> ); return false;" />-->
    </div>
    <div class="floatright">
        <input type="button" onclick="window.open('<?php echo "$GLOBAL_START_URL/statistics/statistics.php?cat=product&reference=" . urlencode( $item->get( "reference" ) ) . "&amp;period&amp;StatStartDate=" . urlencode( date( "d/m/Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ) ) . "&amp;StatEndDate=" . urlencode( date( "d/m/Y" ) ) ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px; margin-right:5px;"/>
        <input type="button" value="Ouvrir un litige" class="blueButton blueButtonActif" onclick="document.location = '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idorder=<?php echo $item->get( "idorder" ) ?>&idrow=<?php echo $item->get( "idrow" ) ?>';" />
        <input type="submit" class="blueButton" name="" value="Recalculer" />
    </div>
    <div class="clear"></div>
    <?php
        
}

//-----------------------------------------------------------------------------------------------------

/**
 * Trie les lignes de commande par idsupplier et idrow
 */
function getIdRowsSortedBySupplier(){
	
	global $Order;
	
	//récupérer les identifiant fournisseur
	
	$rows = array();
	$idsuppliers = array();
	
	$i = 0;
	while( $i < $Order->getItemCount() ){
		
		$rows[] = $i;
		$idsuppliers[] = $Order->getItemAt( $i)->get( "idsupplier" );
		
		$i++;
		
	}

	//tri par fournisseur

    $sorted = false;
 	while( !$sorted ){
 		
 		$sorted = true;
 		$i = 0;
 		while( $i < count( $idsuppliers ) - 1){
 			
 			if( $idsuppliers[ $i ] > $idsuppliers[ $i + 1 ] 
 				|| ( $idsuppliers[ $i ] == $idsuppliers[ $i + 1 ] && $rows[ $i ] > $rows[ $i + 1 ] ) ){

 				$tmp = $idsuppliers[ $i ];
 				$idsuppliers[ $i ] = $idsuppliers[ $i + 1 ];
 				$idsuppliers[ $i + 1 ] = $tmp;
 				
 				$tmp = $rows[ $i ];
 				$rows[ $i ] = $rows[ $i + 1 ];
 				$rows[ $i + 1 ] = $tmp;
 				
 				$sorted = false;
 				
 			}
    			
 			$i++;
 			
 		}
 		
 	}
 	
	return $rows;
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * vérification du stock
 */
function checkStockLevel( &$item ){
	
	global $warning; //@todo :o/
	
	if( $item->get( "reference_base" ) != $item->get( "reference" ) || !DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) )
		return;
	
	$rs =& DBUtil::query( "SELECT stock_level FROM detail WHERE reference = '" . $item->get( "reference" ) . "' LIMIT 1" );

	if( !$rs->RecordCount() || $rs->fields( "stock_level" ) == -1 )
		return;
	
	$internalQuantity = $item->get( "quantity" ) - $item->get( "external_quantity" );
	
	if( $internalQuantity > $rs->fields( "stock_level" ) ){
		
		?>
		<p>
			Stock insuffisant pour la référence <?php echo $item->get( "reference" ) ?>
			Impossible de poursuivre la commande. Il reste <?php echo $rs->fields( "stock_level" ) ?> pièces.
		</p>
		<?php

		$warning = 1;
		
	}
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * options
 */
function displayOptionLink( &$item ){
			
	if( DBUtil::query( "SELECT COUNT( * ) AS `hasOption` FROM associat_product WHERE idproduct='" . DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) . "'" )->fields( "hasOption" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="optionssel(<?php echo $item->get( "idarticle" ) ?>); return false;" class="orangeText"><b>Options</b></a>
		</p>
		<?php 
		
	}
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * produits similaires
 */
function displaySimilarLink( &$item ){
	
	//produits similaires

	if( DBUtil::query( "SELECT COUNT( * ) AS `hasSimilar` FROM product WHERE idproduct='" . DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) . "' AND ( idproduct_similar_1 > 0 OR idproduct_similar_2 > 0 OR idproduct_similar_3 > 0 )" )->fields( "hasSimilar" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="similarsel(<?php echo $item->get( "idarticle" ) ?>); return false;" class="orangeText"><b>Similaires</b></a>
		</p>
		<?php 
		
	}
    
}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( OrderItem &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( $idproduct ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" onclick="window.open( this.href ); return false;">
		<?php 
		
	}
	
	?>
	<img src="<?php echo URLFactory::getReferenceImageURI( $item->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * logo promo
 */
function displayPromoLogo( &$item ){
	
	global $GLOBAL_START_URL;
	
	if( $item->get( "hasPromo" ) ){
		
		?>
		<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/promo.gif" alt="En promotion" /></p>
		<?php
		
	}
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * destockage
 */
function displayDestockingLogo( &$item ){
	
	global $GLOBAL_START_URL;
	
	if( $item->get( "isDestocking" ) ){
		
		?>
		<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/destocking.gif" alt="Destockage" /></p>
		<?php
		
	}
			
}

//-----------------------------------------------------------------------------------------------------
/**
 * prix au volumne
 */
function displayVolPriceLink( Order $order, &$item ){
	
	global $GLOBAL_START_URL;
	
	$idbuyer_family = DBUtil::getDBValue( "idbuyer_family", "buyer", "idbuyer", $order->get( "idbuyer" ) );
	
	$query = "
	SELECT COUNT( rate ) AS `count`
	FROM multiprice_family 
	WHERE idbuyer_family = '$idbuyer_family' 
	AND reference = '" . $item->get( "reference" ) . "' 
	AND begin_date <= NOW() 
	AND end_date >= NOW()";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->fields( "count" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="PopupVolume( '<?php echo $item->get( "reference" ) ?>','<?php echo $idbuyer_family ?>');return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/volume.gif" alt="Prix au volume" style="border-style:none;" />
			</a>
		</p>
		<?php
		
	}
		
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$item, $editable = true ){
		
	global $GLOBAL_START_URL;
	
	/*référence avant substitution*/
						
	if( strlen( $item->get( "ref_replace" ) ) ){
			
		?>
		<p>Référence Substitut : <?php echo $item->get( "ref_replace" ) ?></p>
		<?php
		
	}
	
	/*référence catalogue*/
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) ){ 
		
		$refText = "<a href=\"" . URLFactory::getProductURL( $idproduct ) . "\" onclick=\"window.open( this.href ); return false;\">" . $item->get( "reference" ) . "</a>";
		
	}else{
		
		$refText = $item->get( "reference" );
		
	}
	
	?>
	<p>Référence catalogue : <?php echo $refText ?></p>
	 <?php
	 
	 /* couleurs */
	 
	 displayColor( $item, $editable );
	 
	 /*référence et infos fournisseur*/
	 
	 if( $item->get( "idsupplier" ) ){
		
		?>
		<p>
			<span class="lightGrayText">Réf. fournisseur : <?php echo $item->get( "ref_supplier" ) ?></span>
			&nbsp;
			<a href="#" onclick="showSupplierInfos( <?php echo $item->get( "idsupplier" ) ?> ); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
			</a>
		</p>
		<?php 

	 }
	
	/*référence alternative*/
	
	if( strlen( $item->get( "alternative" ) ) ){
		
		?>
		<p><span class="msg">Attention référence modifiée</span></p>
		<p>Complément référence : <?php echo $item->get( "alternative" ) ?></p>
		<?php 
		
	}
	
	/*lien fiche PDF*/
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) ){
		
		$idproduct = DBUtil::getDBValue("idproduct","detail","idarticle",$item->get( "idarticle" ));		
		
		$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $item->get( "idarticle" ) );
		$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : URLFactory::getPDFURL( $idproduct );
		
		?>
		<p>
			<a class="blueLink" href="<?php echo $href ?>" onclick="window.open(this.href); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
			</a>
		</p>
		<?php
			
	}
								 	
}

//-----------------------------------------------------------------------------------------------------

function displayStockPopup( &$item ){
	
	global $GLOBAL_START_URL;
	
	if( DBUtil::getDBValue( "stock_level", "detail", "idarticle", $item->get( "idarticle" ) ) == -1 )
		return;
			
	?>
	<p style="text-align:center; margin:5px;">
		<a href="#" onclick="popupStock( '<?php echo $item->get( "reference" ) ?>' );return false;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/stock.jpg" alt="Informations sur le stock" style="border-style:none;" />
		</a>
	</p>
	<?php
			
}

//-----------------------------------------------------------------------------------------------------

function checkMinOrder( &$item ){
	
	if( $item->get( "min_cde" ) < 2 )
		return;
				
	?>
	<p style="text-align:center; margin:5px;">
		<span class="Marge">Qté min : <?php echo $item->get( "min_cde" ) ?></span>
	</p>
	<?php
		
}

//-----------------------------------------------------------------------------------------------------

function displayQuantity( &$item, $disabled ){
	
	?>
	
	<input<?php echo $disabled ?> type="text" class="textInput quantity" name="quantity[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo $item->get( "quantity" ) ?>" onkeypress="return forceUnsignedIntegerValue( event );" />
	<?php
	
	/*quantité par lot*/
	
	if( $item->get( "lot" ) > 1 ){
			
		?>
		<p><?php echo $item->get("lot" ) . " / " . htmlentities( DBUtil::getDBValue( "unit" . "_1", "unit", "idunit", $item->get( "unit" ) ) ); ?></p>
		<?php
	}
	
}

function displayCommercialDiscount( &$item, $disabled ){
					
	if( DBUtil::getParameterAdmin( "view_ref_discount_add") == 0 ){

		?>
		<input<?php echo $disabled ?> type="text" class="textInput percentage" name="ref_discount[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo Util::numberFormat( $item->get( "ref_discount" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;%
		<?php 

	}
	else{

		?>
		<p><b>Base</b></p>
		<p><input<?php echo $disabled ?> type="text" class="textInput percentage" name="ref_discount_base[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo Util::numberFormat( $item->get( "ref_discount_base" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;%</p>
		<p><b>Complémentaire</b></p>
		<p><input<?php echo $disabled ?> type="text" class="textInput percentage" name="ref_discount_add[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo Util::numberFormat( $item->get( "ref_discount_add" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;%</p>
		<?php 
		
	}
					
}	
		
//-----------------------------------------------------------------------------------------------------

function displayDiscountPrice( &$item, $disabled ){
		
	if( DBUtil::getParameterAdmin( "view_ref_discount_add") == 0 ){
		
		?>
		<input<?php echo $disabled ?> type="text" class="textInput price" name="discount_price[<?php echo $item->get( "idrow" ) ?>]" value="<?php echo Util::numberFormat( $item->get( "discount_price" ) ) ?>" onkeypress="return forceFloatValue( event );" />&nbsp;&euro;
		<?php 
		
	}
	else echo "<b>" . Util::priceFormat( $item->get( "discount_price" ) ) . "</b>";
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){
	    
    $des_edit=stripslashes($item->get('designation'));
    $des_edit = str_replace("\n",'', $des_edit);
	$des_edit = str_replace("\r",'', $des_edit);
	$des_edit = str_replace('<br />',"\n", $des_edit);
	$des_edit = str_replace('<br />',"\n", $des_edit);
	$des_edit = strip_tags($des_edit);
	return $des_edit;
  
}

//-----------------------------------------------------------------------------------------------------

function displayItemMargins( &$item ){
	
	?>
		<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>
	
    <div class="tableContainer">
        <table class="dataTable devisTable summaryTable">
            <tr>
                <th colspan="1">Marge achat / PV</th>
            </tr>
            <tr>
                <td colspan="1">
			   <?php 
			   
                                 $query = "SELECT tva FROM `tva`
                                  ";

                                 $rs =& DBUtil::query( $query );
                               ?>  
                                <?php 
                                $idord=$item->get("idorder");
                                 $q1 = "SELECT status FROM `order` where idorder= '".$idord."'
                                  ";
                                    $rs1 =& DBUtil::query( $q1 );
                              $status= $rs1->fields('status');
                                 
                               ?> 
                             
                               
                               
                               
                               
                               
                               
                               
						 Prix HT remisé : 
						 <?php if( B2B_STRATEGY ){
						 $prix_ht =  $item->get( "quantity" ) * ( $item->get( "discount_price" )); 
						 } 
				else { 
						 $prix_ht =  $item->get( "quantity" ) * (( $item->get( "discount_price" ) / (( $item->getVATRate() / 100.0 ) + 1))); 
						 } 
						 echo Util::priceFormat( $prix_ht );						 
						 ?>
						 
						 <br /><br/>   
				         <input type="hidden" name="<?php echo $item->get( 'idarticle') ;?>"value="<?php echo $item->get( 'idarticle') ;?>">
				 
				    TVA :
				         <?php 
						 //$vatrate = $item->getVATRate();
						 $vatrate = $item->get( "vat_rate" );
						 if($vatrate == 19.60)
						  $vatrate = 20.00;
						 if(($status=="Ordered")||($status=="Cancelled"))
				                 {
				         ?>
				                 
						<label>
					<?php
						echo " ".$vatrate. " %";
						?>
						</label>


				        <?php
				                }
				                else 
				                { 
				         ?>
				                         <select name="<?php echo $item->get( 'idrow') ;?>" id="tva"  onchange="submit(this.form)"> 
				          
				        <?php
				           
				          
				          
				          
				          
	                                while( !$rs->EOF() )
	                                        {

	                                        
	                                        if($rs->fields('tva')==$vatrate){
                                        ?>
                                                <option value="<?php echo $rs->fields('tva');?>" selected><?php echo $rs->fields('tva');?> </option>


                                                <?php
                                                }
                                                else 
                                                {
                                                ?>
                                                <option name="vatRate"value="<?php echo $rs->fields('tva');?>"><?php echo $rs->fields('tva');?> </option>
                                                <?php
                                                }

                                                
                                                $rs->MoveNext();
                                                }

                                                ?>
                                                </select>
						<?php
						}
						?>
                                                <br /><br />
						
						 Marge  = 
				<?php if( B2B_STRATEGY ){
					$mb =  $item->get( "quantity" ) * ( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) );
					$mb_com =  $item->get( "quantity" ) * ( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) * (1 + ($_SESSION['rate'] / 100)) );
			  } 
				else { 
					$mb =  $item->get( "quantity" ) * (( $item->get( "discount_price" ) / (( $item->getVATRate() / 100.0 ) + 1)) - $item->get( "unit_cost_amount" ) );
			  } 
			 ?>
			<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
			<?php echo Util::priceFormat( $mb ); ?>
			  <?php } ?>
			  <?php if($akilae_comext ==1){ ?> / 
			  <span style="color:red !important;"><?php echo Util::priceFormat( $mb_com ); ?></span>
			  <?php } ?> 
				</td>
            </tr>
            <tr>
                <th>MB %</th>
            </tr>
			                   
            <tr>
                <td style="width:50%">
					<?php if ( $_SESSION['salesman_user'] == 0 ) {?>		
                <?php
                
                	/*marge finale*/
					$mbf = ( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) ) / $item->get( "discount_price" ) * 100 ;
					echo Util::rateFormat( $mbf );
				?>
				<?php } ?>
				 <?php if($akilae_comext ==1){ ?> / 
				<span style="color:red !important;">
				<?php
                
                	/*marge finale commercial*/
					$mbf_com = (( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) ) / $item->get( "discount_price" ) * 100) - $_SESSION['rate'] ;
					echo Util::rateFormat( $mbf_com );
														
				?>
				</span>
				 <?php } ?> 
                </td>
				
            </tr>
        </table>
        
		
		
        <br />
		  <?php
		  	if ($item->get( "ecotaxe_code" ))
			{
		  	$ecotaxe_code = $item->get( "ecotaxe_code" );
			$idrow = $item->get( "idrow" );
		  	$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
			$ecotaxe_amount = $rs1->fields( "amount" ) * $item->get( "quantity" );
		
		  	$rs2 =& DBUtil::query( "UPDATE order_row SET ecotaxe_amount = $ecotaxe_amount  WHERE ecotaxe_code = '$ecotaxe_code' and idrow = $idrow " );
		  ?>
          
           <table class="dataTable devisTable summaryTable">
           <tr>
           <th>Code ecotaxe</th>
          <th>Montant</th>
          </tr>
           <td style="width:50%" align="center"><?php if ($item->get( "ecotaxe_code" ) ) echo $item->get( "ecotaxe_code" ); else echo "-"; ?></td>
           <td style="width:50%" align="center"><?php echo $ecotaxe_amount; ?></td> 
           </table>
           
        <?php
		}
		?>
        
	</div>
	<?php
	       
}

//-----------------------------------------------------------------------------------------------------

function displayColor( OrderItem $item, $editable = true ){
	
	?>
	<input type="hidden" id="idcolor<?php echo $item->get( "idrow" ) - 1; ?>" name="idcolor[<?php echo $item->get( "idrow" ); ?>]" value="<?php echo $item->get( "idcolor" ); ?>" />
	<?php
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( !$idproduct )
		return;

	/* couleurs disponibles */
		
	$rs = DBUtil::query( "SELECT idcolor FROM color_product WHERE idproduct = '$idproduct' ORDER BY display ASC" );
	
	if( !$rs->RecordCount() )
		return;

	include_once( dirname( __FILE__ ) . "/../../../objects/Color.php" );
	
	$colors = array();
	
	while( !$rs->EOF() ){
		
		$colors[ $rs->fields( "idcolor" ) ] = new Color( $idproduct, $rs->fields( "idcolor" ) );
		
		$rs->MoveNext();
		
	}
		
	?>
	<div>
	<?php
	
		if( $item->get( "idcolor" ) ){
			
			$color = $colors[ $item->get( "idcolor" ) ];
			
			if( $editable ){
				
				?>
				<img src="/images/back_office/content/corbeille.jpg" alt="Supprimer la couleur" style="cursor:pointer;" onclick="addColor( 0 );" />
				<?php
				
			}
			
			?>
			<img src="<?php echo substr( $color->getImageURI(), 2 ); ?>" alt="<?php echo htmlentities( $color->getName() ); ?>" style="border:1px solid #B2B2B2; width:15px; height:15px;" />
			<span style="margin-right:15px;"><?php echo htmlentities( $color->getName() . " - " . $color->getCode() ); ?></span>
			<?php
			
		}
		
		if( $editable ){
			
			?>
			<a href="#" onclick="colorDialog(<?php $index = $item->get( "idrow" ) - 1; echo $index . "," . $idproduct; ?>); return false;" class="blueLink">couleurs</a>
			<?php
			
		}
		
	?>
	</div>
	<div id="ColorDialog" style="display:none;">
		<ul id="ColorList">
		<?php
		
			foreach( $colors as $idcolor => $color ){
				
				?>
				<li>
					<div style="position:relative;">
						<a href="#" onclick="" style="cursor:pointer;">
							<img src="<?php echo substr( $color->getImageURI(), 2 ); ?>" alt="<?php echo htmlentities( $color->getName() ); ?>" />
						</a>
						<div class="label">
							<?php echo htmlentities( $color->getCode() ); ?>
						</div>
					</div>
				</li>
				<?php
				
			}
			
		?>
		</ul>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>
