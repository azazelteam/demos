<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "UpdateIdPayment" );
anchor( "UpdatePaymentDel" );
anchor( "UpdateFactor" );
anchor( "UpdateNOrder" );
anchor( "UpdateBalanceAmount" );
anchor( "UpdateBalanceDate" );
anchor( "UpdateBillingComment" );


//----------------------------------------------------------------------------
//partie éditable si la commande n'a pas été facturée, payée ou annulée

if( $Order->get( "status" ) == Order::$STATUS_INVOICED
	|| 	$Order->get( "status" ) == Order::$STATUS_CANCELLED
	|| 	$Order->get( "status" ) == Order::$STATUS_PAID )
		$payment_disabled = " disabled=\"disabled\"";
else 	$payment_disabled = "";

//----------------------------------------------------------------------------

$paid = $Order->get( 'paid' );

$idpayment_delay = $Order->get( "idpayment_delay" );

if( empty( $idpayment_delay ) )
	$idpayment_delay = 4;

//----------------------------------------------------------------------------

//paiement comptant
$cash_payment = $Order->get( "cash_payment" );
$cash_enabled = $cash_payment ? "" : " disabled=\"disabled\"";

$no_tax_export = $Order->get( "no_tax_export" );

//montant règlé comptant
$balance_date = $Order->get( "balance_date" );
$startdate = $balance_date;

//Date de règlement
$balance_date = $Order->get( "balance_date" );

if( $balance_date == "0000-00-00" ){
	$balance_date = "";
	$startdate = date( "Y-m-d" );
}else{
	$startdate = $balance_date;
}

$n_order = $Order->get( "n_order" );

$total_amount = $Order->get( "total_amount" );

?>

<script type="text/javascript">
	function choufForCashPayment(idPaymentDelay){
		if(idPaymentDelay == 1){
			document.getElementById('down_payment').checked = false;
			document.getElementById('cash_payment').checked = true;
			document.getElementById('down_payment').disabled = 'disabled';
			enableCashPayment( true , true );
			dischpufleButtons();
			
			var maValue = document.forms.frm.elements['idpayment'].options[document.forms.frm.elements['idpayment'].selectedIndex].value;
			
			var myListe = document.forms.frm.elements['payment_idpayment'].options;
			
			var maSelected = 0;
			
			for(i = 0 ; i < myListe.length ; i++){
				if( myListe[i].value == maValue){
					maSelected = i;
				}
			}
			
			document.forms.frm.elements['payment_idpayment'].selectedIndex = maSelected;
											
		} else {
			enableCashPayment( false , true );
			document.getElementById('down_payment').disabled = '';
			document.getElementById('cash_payment').checked = false;
			if(document.getElementById('down_payment_amount').value!='' && parseFloat(document.getElementById('down_payment_amount').value) > 0){
				document.getElementById('down_payment').checked = true;
			}
			dischpufleButtons();
			enableDP();
		}
	}
</script>
<?php

include_once( dirname( __FILE__ ) . "/../../../../objects/RegulationsBuyer.php" );

$regulations = RegulationsBuyer::getOrderRegulations( $IdOrder );

if( count( $regulations ) ){
	?>
	<div class="contentResult" style="margin-bottom:10px;">
		<h1 class="titleEstimate">
			<span class="textTitle">Règlements reçus</span>
			<div class="spacer"></div>
		</h1>
		
		<div class="blocEstimateResult"><div style="margin:5px;">
		
	        <table class="dataTable devisTable" style="margin-top:5px; width:100%;">
	            <tr>
	                <th>Com.</th>
					<th>Acompte</th>
					<th>Date</th>
					<th>Mode de règlement</th>
					<th>Banque</th>
					<th>Pièce</th>
					<th>Commentaire</th>
					<th>Montant</th>
	            </tr>
			<?php
			for( $i = 0 ; $i < count( $regulations ) ; $i++ ){					
				?>
				<tr>
					<td><?php echo $regulations[ $i ][ "initial" ]; ?></td>
					<td><?php if( $regulations[$i]["from_down_payment"] == 1 ){ echo "Oui"; }else{ echo "Non"; } ?></td>
					<td><?php echo usDate2eu($regulations[$i]["payment_date"]); ?></td>
					<td><?php echo getPaymentTxt($regulations[$i]["idpayment"]); ?></td>
					<td><?php echo $regulations[$i]["bank"]; ?></td>
					<td><?php echo stripslashes($regulations[$i]["piece"]) ?></td>
					<td><?php echo stripslashes($regulations[$i]["comment"]) ?></td>
					<td><?php echo Util::priceFormat($regulations[$i]["amount"]); ?></td>
				</tr>
				<?php	
			}			
			?>
			</table>

		</div></div>
		
	</div>
	<div class="spacer"></div>
<?
}
?>

<div class="contentResult" style="margin-bottom:10px;">

	<h1 class="titleEstimate">
	<span class="textTitle">Paiement et échéance</span>
	<div class="spacer"></div>
	</h1>
	<div class="spacer"></div>
	
	<div class="blocEstimateResult"><div style="margin:5px;">
		
		<div class="floatleft" style="width:89%;display:none;">
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function enableCashPayment( enable , duclick ){
					
					if(!document.getElementById('cash_payment').checked && !document.getElementById('down_payment').checked){
						disableAll();
						return;	
					}
					
					document.getElementById( "frm" ).elements[ 'payment_idpayment' ].disabled = enable ? '' : 'disabled';
					document.getElementById( "frm" ).elements[ 'cash_payment_piece' ].disabled = enable ? '' : 'disabled';
					document.getElementById( "frm" ).elements[ 'cash_payment_idbank' ].disabled = enable ? '' : 'disabled';
					document.getElementById( "frm" ).elements[ 'payment_comment' ].disabled = enable ? '' : 'disabled';
					document.getElementById( "frm" ).elements[ 'balance_date' ].disabled = enable ? '' : 'disabled';
					
					document.getElementById( "frm" ).elements[ 'down_payment_rate' ].disabled = enable ? 'disabled' : '';
					document.getElementById( "frm" ).elements[ 'down_payment_amount' ].disabled = enable ? 'disabled' : '';
					document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].disabled = enable ? 'disabled' : '';
					document.getElementById( "frm" ).elements[ 'down_payment_piece' ].disabled = enable ? 'disabled' : '';
					document.getElementById( "frm" ).elements[ 'down_payment_comment' ].disabled = enable ? 'disabled' : '';
					document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].disabled = enable ? 'disabled' : '';
					
					if( enable )
						$( '#balance_date' ).datepicker( "enable" );
					else $( '#balance_date' ).datepicker( "disable" );
	
					if( enable )
						$( '#down_payment_date' ).datepicker( "disable" );
					else $( '#down_payment_date' ).datepicker( "enable" );
	
					if( !enable && duclick ){
						document.getElementById( "frm" ).elements[ 'down_payment_date' ].value = '<?=usDate2eu($Order->get( "down_payment_date" ))?>';
						document.getElementById( "frm" ).elements[ 'down_payment_rate' ].value = '<?=Util::numberFormat( $down_payment_rate )?>';
						document.getElementById( "frm" ).elements[ 'down_payment_amount' ].value = '<?=Util::numberFormat( $down_payment_amount )?>';
						
						var mesoptionsA = document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].options;
						var monoptionA = 0;
						for(i=0;i<mesoptionsA.length;i++){
							if( mesoptionsA[i].value == <?=$Order->get( "down_payment_idbank" )?$Order->get( "down_payment_idbank" ):0?> ){
								monoptionA = i ;
							}
						}
						
						document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].selectedIndex = monoptionA ;
						
						document.getElementById( "frm" ).elements[ 'down_payment_piece' ].value = '<?=$Order->get( "down_payment_piece" )?>';
						document.getElementById( "frm" ).elements[ 'down_payment_comment' ].value = '<?=$Order->get( "down_payment_comment" )?>';
						
						var mesoptions = document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].options;
						var monoption = 0;
						for(i=0;i<mesoptions.length;i++){
							if( mesoptions[i].value == <?=$Order->get( "down_payment_idpayment" )?> ){
								monoption = i ;
							}
						}
						
						document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].selectedIndex = monoption ;
					}else{
						if( enable && duclick){
							document.getElementById( "frm" ).elements[ 'down_payment_date' ].value = '';
							document.getElementById( "frm" ).elements[ 'down_payment_rate' ].value = '';
							document.getElementById( "frm" ).elements[ 'down_payment_amount' ].value = '';
							document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].selectedIndex = 0;
							document.getElementById( "frm" ).elements[ 'down_payment_piece' ].value = '';
							document.getElementById( "frm" ).elements[ 'down_payment_comment' ].value = '';
							document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].selectedIndex = 0;
						}
					}
	
					document.getElementById( "frm" ).elements[ 'balance_date' ].value = enable ? '<?php 
					
						list( $year, $month, $day ) = explode( "-", $Order->get( "balance_date" ) );
						echo $Order->get( "balance_date" ) != "0000-00-00" ? "$day-$month-$year" : date( "d-m-Y" ); 
						
					?>' : '';
					document.getElementById( 'balance_date_calendar' ).style.display = enable ? 'block' : 'none';
	
					var factor = document.getElementById( 'factor' );
					
					if( enable && factor ){
					
						switch( factor.tagName.toLowerCase() ){
						
							case "select" :
								
								var i = 0;
								while( i < factor.options.length ){
								
									if( factor.options[ i ].value == 0 )
										factor.selectedIndex = i;
										
									i++;
									
								}
								
								break;
								
							case "input" : factor.value = 0; break;
						
						}
						
					}
					
				}
				
				function dischpufleButtons(){
				
					if(document.getElementById( "confirmOder" )){
						if((document.getElementById('cash_payment').checked && document.getElementById('CPIdRegulation').value == 0) || (document.getElementById('down_payment').checked && document.getElementById('DPIdRegulation').value == 0)){	
							document.getElementById( "confirmOder" ).onclick = function( ){
				       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
								$.blockUI.defaults.css.fontSize = '12px';
								$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
								$.blockUI.defaults.growlCSS.color = '#FFFFFF';
							    $.growlUI( '', " Cette action est impossible du fait qu'un acompte ou un paiement comptant est en cours de validation " );
							}								
						}else{
							document.getElementById( "confirmOder" ).disabled = '';
							document.getElementById( "confirmOder" ).onclick = function( ){
								confirmOrder( document.getElementById( "confirmOder" ), '' );
							}
						}
					}
				}
				
				$(document).ready(function() {
				
					<?if($Order->get( "cash_payment" ) || $Order->get( "down_payment_value" )>0  || $Order->get( "idpayment_delay" )==1){?>
						choufForCashPayment(<?php echo $Order->get( "idpayment_delay" ) ?>);
					<?php } else{?>
						disableAll();
					<?php } ?>
					
					<?php if( ($Order->get( "cash_payment" ) && $Order->get( "cash_payment_idregulation" )==0 ) || ( $Order->get( "down_payment_value" ) > 0 && $Order->get( "down_payment_idregulation" )==0 ) ){ ?>
						dischpufleButtons();
					<?php } ?>
					
											
				});
				
				function disableAll(){
					$( '#balance_date' ).datepicker( "disable" );
					$( '#down_payment_date' ).datepicker( "disable" );
					
					document.getElementById( "frm" ).elements[ 'payment_idpayment' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'cash_payment_piece' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'cash_payment_idbank' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'payment_comment' ].disabled = 'disabled';
					
					document.getElementById( "frm" ).elements[ 'down_payment_rate' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'down_payment_amount' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'down_payment_piece' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'down_payment_comment' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].disabled = 'disabled';
					document.getElementById( "frm" ).elements[ 'down_payment_date' ].disabled = 'disabled';
				}
				
				function enableDP(){
					document.getElementById( "frm" ).elements[ 'down_payment_rate' ].disabled = '';
					document.getElementById( "frm" ).elements[ 'down_payment_amount' ].disabled = '';
					document.getElementById( "frm" ).elements[ 'down_payment_idbank' ].disabled = '';
					document.getElementById( "frm" ).elements[ 'down_payment_piece' ].disabled = '';
					document.getElementById( "frm" ).elements[ 'down_payment_comment' ].disabled = '';
					document.getElementById( "frm" ).elements[ 'down_payment_idpayment' ].disabled = '';
					document.getElementById( "frm" ).elements[ 'down_payment_date' ].disabled = '';
				}
	
			/* ]]> */
			</script>
			<input type="hidden" id="CPIdRegulation" value="<?=$Order->get('cash_payment_idregulation')?>">
			<input type="hidden" id="DPIdRegulation" value="<?=$Order->get('down_payment_idregulation')?>">
	        <table class="dataTable devisTable" style="margin-top:10px; width:100%;">
	            <tr>
	                <th style="width:70px;">Paiement comptant</th>
	                <th style="width:15%;">Date de règlement</th>
	                <th style="width:15%;">Mode de règlement</th>
	                <th style="width:15%;">Num. pièce</th>
	                <th style="width:15%;">Banque émettrice</th>
	                <th>Commentaire</th>
	            </tr>
	            <tr>
	            	<td style="text-align:center;">
	            		<input <?php echo $disabled ?> type="checkbox" id="cash_payment" name="cash_payment" value="1" onclick="enableCashPayment( this.checked , true );this.checked?document.getElementById('down_payment').checked=false:'';dischpufleButtons();"<?php if( $Order->get( "cash_payment" ) ) echo " checked=\"checked\" disabled=\"disabled\""; ?> style="vertical-align:bottom;" />
	            	</td>
	                <td style="white-space:nowrap;">
						<span>
							<input <?php echo $disabled ?> type="text" name="balance_date" id="balance_date" value="<?php echo usDate2eu( $balance_date ) ?>" readonly="readonly"<?php echo $cash_enabled ?> class="calendarInput" />
						</span>
						<span id="balance_date_calendar" style="margin-left:3px; margin-top:2px;<?php if( $cash_enabled ) echo" display:none;"; ?>">
							<?php DHTMLCalendar::calendar( "balance_date" ); ?>
						</span>
					</td>
					<td style="white-space:nowrap;">
						<?php DrawLift("payment",$Order->get("payment_idpayment"),"",!strlen( $disabled ),false,"payment_idpayment"); ?>
					</td>
					<td style="white-space:nowrap;">
						<input <?php echo $disabled ?> type="text" name="cash_payment_piece" class="textInput" style="width:100px;" value="<?=htmlentities(stripslashes($Order->get("cash_payment_piece")))?>" />
					</td>
					<td style="white-space:nowrap;">
						<select <?php echo $disabled ?> name="cash_payment_idbank" id="cash_payment_idbank">
							<?
							$selectedIDBcp = $Order->get("down_payment_idbank");
							?>
							<option value="0">Sélection</option>
							<?
							$banks = DBUtil::query("SELECT * FROM bank");
							while(!$banks->EOF()){
								$idbcp = $banks->fields('idbank');
								$bname = $banks->fields('bank_name');
								if($selectedIDBcp==$idbcp)
									echo "<option value='$idbcp' selected>$bname</option>";									
								else
									echo "<option value='$idbcp'>$bname</option>";
								
								$banks->moveNext();
							}
							?>
						</select>
					</td>
					<td style="white-space:nowrap;">
						<input <?php echo $disabled ?> type="text" name="payment_comment" value="<?=htmlentities(stripslashes($Order->get("payment_comment")))?>" class="textInput" size="35" />
					</td>
				</tr>
			</table> 
		</div>
	
		<div class="floatleft" style="width:89%;">
			<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
				<label><span>Mode de règlement</span></label>
				<?php 		
					DrawLift_modepayment( $Order->get( 'idpayment' ), !strlen( $payment_disabled ) );
				?>
				<div class="spacer"></div>
				
				<label style="height:47px;"><span>Accès au <br/>paiement sécurisé</span></label>
				<?php
					if( $Order->get( 'idpayment' ) == 1 && $Order->get( "cash_payment_idregulation" ) == 0 ){  ?>
						<p style="text-align:center; padding:5px 0 0 0; height:45px; width:70%;" class="textidSimp">
						<a href="<?php echo $GLOBAL_START_URL ?>/accounting/pay_secure.php?IdOrder=<?php echo $IdOrder ?>">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/front_office/global/icons-payment.jpg" width="193" height="45" alt="Lien vers le paiement sécurisé" /></a>
						</p>
				<?php } 
					if( $Order->get( 'idpayment' ) == 16 && $Order->get( "cash_payment_idregulation" ) == 0 ){  ?>
						<p style="text-align:center; padding:5px 0 0 0; height:45px; width:70%;" class="textidSimp">
						<a href="<?php echo $GLOBAL_START_URL ?>/accounting/pay_secure.php?fractionne&IdOrder=<?php echo $IdOrder ?>">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/front_office/global/icons-payment.jpg" width="193" height="45" alt="Lien vers le paiement sécurisé" /></a>
						</p>
				<?php } 
				?>
				
				<input type="hidden" id="UpdateIdPayment" name="UpdateIdPayment" value="ok" />
				
				<div class="spacer"></div>
				
				<label><span>Délai de paiement</span></label>
				<?php 
					$idpayment_delay = $Order->get( "idpayment_delay" )?$Order->get( "idpayment_delay" ):DBUtil::getParameter( "default_idpayment_delay" );
					$lang = "_1";
					$specialchar=" onchange='choufForCashPayment(this.options[this.selectedIndex].value);'";
					
					echo "<select name=\"idpayment_delay\" $specialchar>\n";
					$rsd = DBUtil::query("SELECT idpayment_delay, payment_delay$lang FROM payment_delay ORDER BY idpayment_delay ASC");
					
					if( $rsd->RecordCount() > 0 ){
						for( $i=0 ; $i < $rsd->RecordCount() ; $i++ ){
							if( $idpayment_delay == $rsd->Fields( "idpayment_delay" ) )
								$selected = " selected=\"selected\"";
							else $selected = "";
								echo "<option value=\"" . $rsd->Fields( "idpayment_delay" ) . "\"$selected>" . $rsd->Fields( "payment_delay$lang" ) . "</option>";
								$rsd->MoveNext();
						}
					}
					
					echo "</select>";
				?>
				<div class="spacer"></div>
				
				<label><span>Echéance règlement</span></label>
				<?php 		
					if( $Str_status == "Invoiced" ) {
						$echeance = $Order->get( "deliv_payment" );
					
						if( $echeance == "0000-00-00" ) { 
							$echeance = "";
							$startdate = date( "Y-m-d" );
						}
						$startdate = $echeance; ?>
						<input type="text" name="deliv_payment" id="deliv_payment" value="<?php echo usDate2eu( $echeance ); ?>" readonly="readonly"<?php echo $payment_disabled ?> class="calendarInput" />
						<?php if( empty( $payment_disabled ) ) DHTMLCalendar::calendar( "deliv_payment" ); ?>
						<input type="hidden" id="UpdateEcheance" name="UpdateEcheance" value="ok" />
						<?php 
					
					} 
					else { echo '<span class="textidSimp">-</span>'; }
				?>
					 	
			</div></div>		 	
			
			<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">				
				<label><span>Assurance crédit</span></label>
				<span class="textSimple" style="border:none;"><b style="color:red;">&nbsp;<?php displayInsurance( $Order, strlen( $payment_disabled ) > 0 ); ?></b></span>
				<div class="spacer"></div>
				
				<label><span>Date de création</span></label>
	        	<?php 
					if( $Order->get( "remote_creation_date" ) != "0000-00-00" )
						$remote_creation_date = usDate2eu( $Order->get( "remote_creation_date" ) );
					else $remote_creation_date = "";
					$startdate = empty( $remote_creation_date ) ? date( "Y-m-d" ) : $Order->get( "remote_creation_date" );
				?>
				<input <?php echo $disabled ?> class="calendarInput" type="text" name="remote_creation_date" id="remote_creation_date" value="<?php echo $remote_creation_date ?>" />
				<?php DHTMLCalendar::calendar( "remote_creation_date" ); ?>
				
				<label class="smallLabelMin" ><span>Référence client</span></label>
				<input type="text" name="n_order" style="width: 14.3%;"<?php if( !empty( $n_order ) ) echo " value=\"$n_order\""; ?> class="textidInputMin" />
				<input type="hidden" name="UpdateNOrder" value="OK" />
				<div class="spacer"></div>
				
				<label><span>Facturation HT</span></label>
				<span class="textSimple" style="border:none;">&nbsp;<?php echo $Order->get( "no_tax_export" ) ? "Oui" : "Non"; ?></span>
				<div class="spacer"></div>
				
			</div></div>	
			<div class="spacer"></div>
			
			<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
				<h2><input <?php echo $disabled ?> type="checkbox" id="down_payment" name="down_payment" value="1" onclick="enableCashPayment( !this.checked , true );this.checked?document.getElementById('cash_payment').checked=false:'';dischpufleButtons();"<?php if( $Order->get( "down_payment_value" )>0.00 ) echo " checked=\"checked\" disabled=\"disabled\""; ?> style="vertical-align:middle;" />
				Acompte</h2>
				
				<label><span>Montant</span></label>
				<input<?php echo $disabled ?> type="text" class="calendarInput percentage" id="down_payment_rate" name="down_payment_rate" value="<?php echo Util::numberFormat( $down_payment_rate ) ?>" />
				<span class="textidSimp" style="width:5.4%;">%</span>
				
				<input<?php echo $disabled ?> type="text" class="calendarInput price" id="down_payment_amount" name="down_payment_amount" value="<?php echo Util::numberFormat( $down_payment_amount ) ?>" />
				<span class="textidSimp" style="width:5.4%;">&euro;</span>	
				<div class="spacer"></div>	
				
				<label><span>Date de règlement</span></label>
				<input <?php echo $disabled ?> type="text" name="down_payment_date" id="down_payment_date" value="<?php echo usDate2eu( $Order->get("down_payment_date") ) ?>" readonly="readonly" class="calendarInput" />
				<?php DHTMLCalendar::calendar( "down_payment_date" ); ?>
				
				<label class="smallLabel" style="width:22%;"><span>Mode règlement</span></label>
				<div class="simpleSelect">
					<?php DrawLift("payment",$Order->get("down_payment_idpayment"),"", !strlen( $disabled ),false,"down_payment_idpayment");?>
				</div>
				<div class="spacer"></div>
				
				<label><span>Num. pièce</span></label>
				<input <?php echo $disabled ?> type="text" name="down_payment_piece" class="calendarInput" value="<?=htmlentities(stripslashes($Order->get("down_payment_piece")))?>" />
				<span class="textidSimp" style="width:3.4%;">&nbsp;</span>
				
				<label class="smallLabel" style="width:22%;"><span>Banque émettrice</span></label>	
				<div class="simpleSelect">
				<select <?php echo $disabled ?> name="down_payment_idbank" id="down_payment_idbank">
					<?
					$selectedIDB = $Order->get("down_payment_idbank");
					?>
					<option value="0">Sélection</option>
					<?
					$banks->moveFirst();
					while(!$banks->EOF()){
						$idb = $banks->fields('idbank');
						$bname = $banks->fields('bank_name');
						if($selectedIDB==$idb)
							echo "<option value='$idb' selected>$bname</option>";									
						else
							echo "<option value='$idb'>$bname</option>";
						
						$banks->moveNext();
					}
					?>
				</select></div> 
				<div class="spacer"></div>	
				
				<label><span>Commentaire<br/>&nbsp;</span></label>	
				<textarea<?php echo $disabled ?> name="down_payment_comment" class="textInput"><?=htmlentities(stripslashes($Order->get("down_payment_comment")))?></textarea>
				<div class="spacer"></div>

			</div></div>
			
			
			<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
				<h2>Escompte</h2>
				
				<?php		
					if( $Order->getTotalATI() == 0.0 )
	            		$rebate_rate = 0.0;
	            	else $rebate_rate 	= $Order->get( "rebate_type" ) == "rate" 	? $Order->get( "rebate_value" ) : $Order->get( "rebate_value" ) * 100.0 / $Order->getTotalATI();
	            	$rebate_amount 	= $Order->get( "rebate_type" ) == "amount" ? $Order->get( "rebate_value" ) : $Order->getTotalATI() * $Order->get( "rebate_value" ) / 100.0;
				?>
				<label><span>Escompte</span></label>
				<input<?php echo $disabled ?> type="text" name="rebate_rate" value="<?php echo Util::numberFormat( $rebate_rate ); ?>" class="calendarInput percentage" />
				<span class="textidSimp" style="width:5.4%;">%</span>	
				
				<input<?php echo $disabled ?> type="text" name="rebate_amount" class="calendarInput price" value="<?php echo Util::numberFormat( $rebate_amount ); ?>" />
				<span class="textidSimp" style="width:5.4%;">&euro;</span>	
				<div class="spacer"></div>
			</div></div>
			<div class="spacer"></div>

		</div>	

		<div class="floatright" style="width:10%;">				
			<table class="dataTable devisTable summaryTable" style="margin-bottom:0px;">
            	<tr>
            		<th colspan="2">Escompte</th>
            	</tr>
            	<tr>
            		<td><?php echo Util::rateFormat( $rebate_rate ); ?></td>
            		<td><?php echo Util::priceFormat( $rebate_amount ); ?></td>
            	</tr>
            	<tr>
            		<th colspan="2">Net à payer sur facture</th>
            	</tr>
            	<tr>
            		<td colspan="2"><?php echo Util::priceFormat( $Order->getBalanceOutstanding() ); ?></td>
            	</tr>
          	</table>	
	
		</div>
		<div class="spacer"></div>
		
	</div></div>
</div>
<?php

/* ------------------------------------------------------------------------------------------------------------------------- */
/* assurance crédit */

function displayInsurance( Order &$order, $disabled = false ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	//assurance non éditable
	
	if( !in_array( $order->get( "status" ), array(
			
		Order::$STATUS_DEMAND_IN_PROGRESS,
		Order::$STATUS_IN_PROGRESS, 
		Order::$STATUS_TO_PAY,
		Order::$STATUS_TODO

	) ) ){
		
		echo $order->get( "insurance" ) ? "oui" : "non";
		
		?>
		<input type="hidden" name="insurance" id="insurance" value="<?php echo $order->get( "insurance" ); ?>" />
		<?php
		
		return;

	}
	
	//pas d'assurance crédit pour les paiements comptant
						
	if( $order->get( "cash_payment" ) ){
		
		?>
		non ( paiement comptant )
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
	
	//assurance crédit désactivée
	
	$idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) );
	
	if( !$idcredit_insurance ){
			
		?>
		non disponible
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php	
		
		return;
		
	}

	/* assurance crédit activée */
	
	$creditInsurance			= ( object )DBUtil::query( "SELECT * FROM credit_insurance WHERE idcredit_insurance = '$idcredit_insurance' LIMIT 1" )->fields;
	$customer 			= new Customer( $order->get( "idbuyer" ), $order->get( "idcontact" ) );
	$insurance 			= $order->get( "insurance" );
	$minimum 			= floatval( $creditInsurance->minimum );
	$maximum 			= floatval( $creditInsurance->maximum );
	$netBillET 			= round( $order->getNetBill() / ( 1.0 + $order->getVATRate() / 100.0 ), 2 );
	$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();
	
	/* client réfusé */
	
	if( $customer->get( "insurance_status" ) == "Refused" ){
		
		?>
		<b style="color:#FF0000;">Client refusé</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}

	if( $customer->get( "insurance_status" ) == "Deleted" ){
		
		?>
		<b style="color:#FF0000;">Client supprimé</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}

	if( $customer->get( "insurance_status" ) == "Terminated" ){
		
		?>
		<b style="color:#FF0000;">Client résilié</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}		
	
	/* client non assuré */

	if( !intval( $customer->get( "insurance" ) ) ){
		
		?>
		<b style="color:#FF0000;">Demande à faire</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
	
	/* montant non couvert */
	
	if( $netBillET < $minimum ){
		
		?>
		En dessous du seuil
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
	
	}

	if( $netBillET > $maximum ){
		
		?>
		Au dessus du seuil
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;

	}
	
	if( $outstandingCredits + $netBillET > $customer->get( "insurance_amount" ) ){
		
		?>
		<b style="color:#FF0000;">Encours maximum atteint</b>
		<input type="hidden" id="insurance" name="insurance" value="0" />
		<?php
		
		return;
		
	}
		
	?>
	<input<?php if( $order->get( "insurance" ) ) echo " checked=\"checked\""; if( $disabled ) echo " disabled=\"disabled\""; ?> type="radio" name="insurance" value="1" onclick="updateOrder( 'insurance', this.value );" /> oui
	<input<?php if( !$order->get( "insurance" ) ) echo " checked=\"checked\""; if( $disabled ) echo " disabled=\"disabled\""; ?> type="radio" name="insurance" value="0" onclick="updateOrder( 'insurance', this.value );" /> non
	<?php
						
}

/* ------------------------------------------------------------------------------------------------------------------------- */
function getPaymentTxt( $idpayment ){
	
	if( empty( $idpayment ) )
		return "";
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs =& DBUtil::query( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}
?>