<?php

//-----------------------------------------------------------------------------------------------------

function displaySupplierCharges( $idsupplier ){

	global	$Order,
			$disabled;

	if( empty( $idsupplier ) ){

		?>

		<p><?php  echo Dictionnary::translate("gest_com_supplier_unknown") ; ?></p>
		<?php

		return;

	}

	$select_charge = DBUtil::getDBValue( "select_charge", "supplier", "idsupplier", $idsupplier );
	$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
	$isInternalSupplier = $idsupplier == $internal_supplier;
	$idstate = $Order->getSupplierIdState( $idsupplier );
	$transit = $Order->getSupplierTransit( $idsupplier );
	$name = GetSupName($idsupplier);

	if( $isInternalSupplier )
		$title = "Dépôt -&gt; Client";
	else if( $transit )
		$title = "Fournisseur -&gt; Dépôt&nbsp;&nbsp;&nbsp;&nbsp;Dépôt -&gt; Client";
	else $title = "Livraison directe par le fournisseur $name";

	$weight = $Order->getSupplierWeight( $idsupplier );
	$buyingAmount = $Order->getSupplierBuyingAmount( $idsupplier );
	$orderAmount = $Order->getSupplierOrderAmount( $idsupplier );
	$franco = $Order->getSupplierFranco( $idsupplier );
	$charges = $Order->getSupplierCharges( $idsupplier );

	//marges

	$discount_price = 0.0;
	$unit_cost_amount = 0.0;
	$unit_price = 0.0;
	$quantity = 0;
	$i = 0;
	$hasExternalStock = false;
	$total_discount_price_ref=0;
	while( $i < $Order->getItemCount() ){

		$ref_supplier = $Order->getItemAt( $i)->get( "idsupplier" );
		$ref_idstate = $Order->getSupplierIdState( $ref_supplier );

		if( $ref_supplier == $idsupplier ){

			$ref_unit_cost_amout = $Order->getItemAt( $i)->get( "unit_cost_amount" );


			if( B2B_STRATEGY ){
			$ref_discount_price = $Order->getItemAt( $i )->get( "discount_price" );
			  }
				else{
			$ref_discount_price = $Order->getItemAt( $i )->get( "discount_price" ) / ( 1.0 + $Order->getItemAt( $i )->getVATRate() / 100.0 );
	         }



			$ref_unit_price = $Order->getItemAt( $i)->get( "unit_price" );

			//if( $idsupplier == $internal_supplier )
				$ref_quantity = $Order->getItemAt( $i)->get( "quantity" );
			//else $ref_quantity = $Order->getItemAt( $i)->get( "external_quantity" );

			$hasExternalStock |= ( $idsupplier != $internal_supplier ) && $ref_quantity > 0;

			$quantity += $ref_quantity;

			$unit_cost_amount += 	$ref_quantity * $ref_unit_cost_amout;
			$unit_price += 			$ref_quantity * $ref_unit_price;
			$discount_price += 		$ref_quantity * $ref_discount_price;

		}

		$i++;

	}

	//ne pas afficher la ligne si tous les articles sont en stock interne et que le fournisseur n'est pas le fournisseur interne

	/*if( !$hasExternalStock && $idsupplier != $internal_supplier )
		return;*/

	//marge brute
	$mb0_rate = $unit_price ? ( 1 - ( $unit_cost_amount / $unit_price ) ) * 100 : "-";
	//$mb0_amount =  $unit_price - $unit_cost_amount;
	$mb0_amount =  $discount_price - $unit_cost_amount;

	$mb1_rate = $orderAmount ? ( 1 - ( $buyingAmount / $orderAmount ) ) * 100 : "-";
	$mb1_amount = $orderAmount - $buyingAmount;

	//frais de port vente (approximation : répartition égale par fournisseur )
	$supplierCount = $Order->getSupplierCount();

	//frais de port achat
	$supplierCharges = $Order->getSupplierCharges( $idsupplier );
	$supplierChargesRate = $buyingAmount == 0 ? 0 : ( $supplierCharges / $buyingAmount ) * 100;

	//marge avec port achat
	$mb2_rate = $orderAmount ? ( 1 - ( ( $buyingAmount + $supplierCharges ) / $orderAmount ) ) * 100 : "-";
	$mb2_amount = $orderAmount - $buyingAmount - $supplierCharges;

	//marge avec port achat et vente
	$mb3_rate = $discount_price ? ( 1 - ( ( $unit_cost_amount + $supplierCharges ) / $discount_price ) ) * 100 : "-";
	$mb3_amount = $discount_price  - ( $unit_cost_amount + $supplierCharges );

	$idstate = $Order->getSupplierIdState( $idsupplier );
	$transit = $Order->getSupplierTransit( $idsupplier );

	//total achat avec port
	$totalAchatEtPort = $buyingAmount + $supplierCharges;

	$charges_auto = $Order->hasAutomaticSupplierCharge( $idsupplier );

	$internalSupplierCharges = $Order->getInternalSupplierCharges( $idsupplier );
	$internalSupplierChargesRate = $orderAmount == 0 ? 0 : ( $internalSupplierCharges / $orderAmount ) * 100;
	$internal_charges_auto = $Order->hasAutomaticInternalSupplierCharge( $idsupplier );


	?>
	<a name="#SupplierCharges<?php echo $idsupplier; ?>"></a>
	<p style="margin-bottom:10px; margin-top:5px;">
		<strong><?php echo $title ?></strong>
	</p>
	<?php

		if( $hasExternalStock && $idsupplier != $internal_supplier ){

			?>
			<input type="hidden" name="SupplierChargesAuto_<?php echo $idsupplier ?>" value="0" />
			<input type="hidden" name="supplier_charges_<?php echo $idsupplier ?>" value="0.0" />
			<input type="hidden" name="internal_supplier_charges_percent_<?php echo  $idsupplier ?>" value="0.0" />
			<?php

		}

	?>

	<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>

    <div class="leftContainer" style="margin-top:0px;width:89%;">
        <table class="dataTable devisTable">
            <tr>
                <th>Conditions de port</th>
                <?php

               		if( $hasExternalStock && $idsupplier != $internal_supplier ){

               			?>
		                <th colspan="2">Coût port facturé<br />Fournisseur</th>
		                <?php

               		}

                /*
                 * Facturation désactivée en attendant que "les calculs" soient refaits. En attendant, on garde l'ancienne facturation globale du port vente
                 */
                /*<th colspan="2">Coût port vente<br />% Total ventes</th>*/
                /*<th colspan="2">Facturation<br />port vente<br />% Total ventes</th>*/
                ?>
                <th colspan="2">Coût port facturé<br />Transporteur</th>
                <th>Prix de revient<br />PA + coûts port</th>
                <th colspan="2">Marge sur port vente</th>
                <th colspan="2">Marge brute / PV<br />sans port</th>
                <!--<th colspan="2">Différentiel/Port</th>-->
                <!--<th colspan="2">Marge nette<br />après Port</th>-->
            </tr>
            <tr>
            	<td rowspan="2">
            	<?php

            		if( $select_charge == OnCharterSale::$EX_FACTORY )
            			echo "Prix départ usine";
            		else if( $franco == -1 || $select_charge == OnCharterSale::$ALWAYS_FRANCO )
						echo "Franco permanent";
					else echo $franco > 0.0 ? "Franco à partir de " . Util::priceFormat( $franco ) : "-";

            	?>
            	</td>
            	<?php

            		if( $hasExternalStock && $idsupplier != $internal_supplier ){

            			?>
		                <td colspan="2">
		                	<select name="SupplierChargesAuto_<?php echo  $idsupplier ?>" id="SupplierChargesAuto_<?php echo  $idsupplier ?>" onchange="recalEstimate(); changeSupplierCharges('<?php echo  $idsupplier ?>', '<?php

		                		$supplierCharges = $Order->getSupplierCharges( $idsupplier, true );

		                		if( $supplierCharges == 0.0 && ( $select_charge == OnCharterSale::$ALWAYS_FRANCO || ( $franco > 0.0 && $franco <= $Order->getSupplierBuyingAmount( $idsupplier ) ) ) )
		                			echo "Franco";
		                		else echo Util::numberFormat( $supplierCharges );

		                		?>');">
		                    	<option value="1"<?php if( $charges_auto ){ echo " selected='selected'"; } ?>>Coût paramétré</option>
		                    	<option value="0"<?php if( !$charges_auto ){ echo " selected='selected'"; } ?>>Coût calculé</option>
		                    </select>
		                </td>
                		<?php

            		}

            	?>
                <td colspan="2">
                	<select name="InternalSupplierChargesAuto_<?php echo  $idsupplier ?>" id="InternalSupplierChargesAuto_<?php echo  $idsupplier ?>" onchange="recalEstimate(); changeInternalSupplierCharges(<?php echo  $idsupplier ?>, '<?php

                		$internalSupplierCharges = $Order->getInternalSupplierCharges( $idsupplier, true );

                		if( $internalSupplierCharges == 0.0 && ( $select_charge == OnCharterSale::$ALWAYS_FRANCO || ( $franco > 0.0 && $franco <= $Order->getSupplierBuyingAmount( $idsupplier ) ) ) )
                			echo "Franco";
                		else echo Util::numberFormat( $internalSupplierCharges );

                		?>');">
                    	<option value="1" <?php if ($internal_charges_auto){echo "selected='selected'";}?>>Coût paramétré</option>
                    	<option value="0" <?php if (!$internal_charges_auto){echo "selected='selected'";}?>>Coût calculé</option>
                    </select>
                </td>
               	 <?php /*
                <td colspan="2">
                	<select name="total_charge_auto">
                	<?php $total_charge_auto = 1; ?>
                    	<option value="0"<?php if( $total_charge_auto ){ echo " selected='selected'"; }?>>Coût paramétré</option>
                    	<option value="1"<?php if( !$total_charge_auto ){ echo " selected='selected'"; }?>>Coût calculé</option>
                    </select>
                </td>
*/
                	//cout port vente facturé a refaire en base
	                $cpvf = 0;

	                //marge cout port vente
	                $mpv = $cpvf-$Order->getInternalSupplierCharges( $idsupplier );

	                if( $cpvf )
	                	$mpv_rate = ( $mpv / $cpvf ) * 100;
	                else
	                	$mpv_rate = 0;

                	//total vente avec port facturé
					$totalVenteEtPortFacture = $discount_price + $cpvf;

					//Prix de vente - coûts port
					$prixRevient = $unit_cost_amount + $Order->getSupplierCharges( $idsupplier ) + $Order->getInternalSupplierCharges( $idsupplier );
					$prixRevient_com = $prixRevient * (1+ $_SESSION['rate']/100);

					//marge brute sur PV sans port
					$mbPVp = $totalVenteEtPortFacture - $buyingAmount;
					$mbPVp_rate = $totalVenteEtPortFacture == 0 ? 0 : ( $mbPVp / $totalVenteEtPortFacture ) * 100;
					$mbPVp_com = $mbPVp / (1 + ($_SESSION['rate'] / 100));
					$mbPVp_com_rate = $mbPVp_rate - $_SESSION['rate'];

					//marge nette après port achat et vente
					$mnpapv = $totalVenteEtPortFacture - $prixRevient;
					$mnpapv_rate = $totalVenteEtPortFacture == 0 ? 0 : ( $mnpapv / $totalVenteEtPortFacture ) * 100;
					$mnpapv_coef = $mnpapv == 0 || $prixRevient == 0.0 ? 0 : ( $totalVenteEtPortFacture / $prixRevient );
					$mnpapv_com = $mnpapv / (1 + ($_SESSION['rate'] / 100)) ;
					$mnpapv_com_rate = $mnpapv_rate - $_SESSION['rate'];


                ?>
                <td rowspan="2">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<span id="pr_<?php echo  $idsupplier ?>"><?php echo  Util::priceFormat($prixRevient) ?></span>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> /
				<span style="color:red !important;"><?php echo  Util::priceFormat($prixRevient_com) ?></span>
				<?php } ?>
				</td>
                <td rowspan="2"><?php echo  Util::priceFormat( $mpv ) ?></td>
                <td rowspan="2"><?php echo  Util::numberFormat( $mpv_rate ) ?>%</td>
                <td rowspan="2">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<span id="mbpvp_<?php echo  $idsupplier ?>"><?php echo  Util::priceFormat($mbPVp) ?></span>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> /
				<span style="color:red !important;"><?php echo  Util::priceFormat($mbPVp_com) ?></span>
				<?php } ?>
				</td>
                <td rowspan="2">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<span id="mbpvpp_<?php echo  $idsupplier ?>"><?php echo  Util::numberFormat( $mbPVp_rate ) ?> % <span>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> /
				<span style="color:red !important;"><?php echo  Util::numberFormat( $mbPVp_com_rate ) ?>% </span>
				<?php } ?>
				</td>

            </tr>
            <tr>
            	<td>
            		<!-- Champs cachés utilisés en attendant de mettre en place la fonction de coût port vente -->
            		<input type="hidden" name="supplier_charges_percent_<?php echo  $idsupplier ?>" value="<?php echo  Util::numberFormat( $supplierChargesRate ) ?>" />
            		<span id="cpfp_<?php echo  $idsupplier ?>"><?php echo  Util::numberFormat( $supplierChargesRate ) ?></span> %
            	</td>
            	<?php

            		if( $hasExternalStock && $idsupplier != $internal_supplier ){

            			?>
		                <td>
		                	<input type="text" id="supplier_charges_<?php echo  $idsupplier ?>" onfocus="this.select()" onkeyup="return recalEstimate();" class="textInput price" name="supplier_charges_<?php echo  $idsupplier ?>" value="<?php

		                		$supplierCharges = $Order->getSupplierCharges( $idsupplier, false );

		                		if( $supplierCharges == 0.0 && ( $select_charge == OnCharterSale::$ALWAYS_FRANCO || ( $franco > 0.0 && $franco <= $Order->getSupplierBuyingAmount( $idsupplier ) ) ) )
		                			echo "Franco";
		                		else echo Util::numberFormat( $supplierCharges );

		                	?>" <?php if( $charges_auto ) echo " disabled=\"disabled\""; ?> /> &euro;

		                	<input type="hidden" name="svg_supplier_charges_<?php echo  $idsupplier ?>" value="<?php

		                		$supplierCharges = $Order->getSupplierCharges( $idsupplier, false );

		                		if( $supplierCharges == 0.0 && ( $select_charge == OnCharterSale::$ALWAYS_FRANCO || ( $franco > 0.0 && $franco <= $Order->getSupplierBuyingAmount( $idsupplier ) ) ) )
		                			echo "Franco";
		                		else echo Util::numberFormat( $supplierCharges );

		                	?>" />

		                </td>
		                 <td>
		                	<input type="hidden" name="internal_supplier_charges_percent_<?php echo  $idsupplier ?>" value="<?php echo  Util::numberFormat( $internalSupplierChargesRate ) ?>" />
		                	<span id="cptp_<?php echo  $idsupplier ?>"><?php echo  Util::numberFormat( $internalSupplierChargesRate ) ?></span> %
		                </td>
		                <?php

            		}

            	?>
                <td>
	                <input type="text" id="cpt_<?php echo  $idsupplier ?>" onfocus="this.select()" onkeyup="return recalEstimate();" class="textInput price" name="internal_supplier_charges_<?php echo  $idsupplier ?>" value="<?php

	        			$internalSupplierCharges = $Order->getInternalSupplierCharges( $idsupplier, false );

		        		if( $internalSupplierCharges == 0.0 && ( $select_charge == OnCharterSale::$ALWAYS_FRANCO || ( $franco > 0.0 && $franco <= $Order->getSupplierBuyingAmount( $idsupplier ) ) ) )
		        			echo "Franco";
		        		else echo Util::numberFormat( $internalSupplierCharges );

	                ?>"<?php if( $internal_charges_auto ) echo " disabled=\"disabled\""; ?> /> &euro;

	                <input type="hidden" name="svg_internal_supplier_charges_<?php echo  $idsupplier ?>" value="<?php

	        			$internalSupplierCharges = $Order->getInternalSupplierCharges( $idsupplier, false );

		        		if( $internalSupplierCharges == 0.0 && ( $select_charge == OnCharterSale::$ALWAYS_FRANCO || ( $franco > 0.0 && $franco <= $Order->getSupplierBuyingAmount( $idsupplier ) ) ) )
		        			echo "Franco";
		        		else echo Util::numberFormat( $internalSupplierCharges );

	                ?>" />

                </td>
            </tr>
            <tr>
            	<td style="border-bottom:none; border-left:none;" colspan="10">
            		<!--<a class="blueButton" href="/sales_force/carrier_calls_for_tender.php?idestimate=<?php echo $Order->getId(); ?>&amp;idsupplier=<?php echo $idsupplier; ?>" onclick="window.open(this.href; return false;">
				    	Appel d'offre transporteur
				    </a>-->
            	</td>
            	<!--<th colspan="2" style="text-align:right;">Transporteur</th>
            	<td colspan="2">
	            	<select name="idcarrier_<?php echo $idsupplier; ?>">
			    		<option value="0"><?php echo htmlentities( $name ); ?></option>
			    		<?php

			    			$idcarrier = $Order->getSupplierCarrier( $idsupplier );
			    			$rs =& DBUtil::query( "SELECT c.idcarrier, p.name FROM carrier c, provider p WHERE p.idprovider = c.idprovider ORDER BY p.name ASC" );

			    			while( !$rs->EOF() ){

			    				$selected = $idcarrier == $rs->fields( "idcarrier" ) ? " selected=\"selected\"" : "";

			    				?>
			    				<option<?php echo $selected; ?> value="<?php echo $rs->fields( "idcarrier" ); ?>"><?php echo htmlentities( $rs->fields( "name" ) ); ?></option>
			    				<?php

			    				$rs->MoveNext();

			    			}

			    	?>
			    	</select>
		    	</td>
            	<td colspan="5" style="border-bottom:none; border-right:none;">&nbsp;</td>-->
			</tr>


        </table>
	 <div class="floatright" style="margin-top:5px;">
        	<input type="submit" class="blueButton" name="ModifyEstimate" value="Recalculer" />
        </div>
    </div>
    <div class="rightContainer" style="width:10%; margin-bottom:35px;">
        <table class="dataTable devisTable summaryTable">
            <tr>
                <th colspan="1">Marge nette <br />après coûts port </th><!-- Port Achat et Vente -->
            </tr>
            <tr>
                <td colspan="1">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<span id="mnap_<?php echo  $idsupplier ?>"><?php echo  Util::priceFormat($mnpapv); ?></span>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> /
				<span style="color:red !important;"><?php echo  Util::priceFormat($mnpapv_com); ?></span>
				<?php } ?>
				</td>
            </tr>
                    <tr>
                        <th>%</th>
                        </tr>
            <tr>
                <td style="width:50%">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<span id="mnapp_<?php echo  $idsupplier ?>"><?php echo  Util::numberFormat($mnpapv_rate); ?>%</span>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> /
				<span style="color:red !important;"><?php echo  Util::numberFormat($mnpapv_com_rate); ?> % </span>
				<?php } ?>
				</td>

        </table>
    </div>
    <div class="spacer"></div>
	<?php
	/*
	<table cellspacing="0" cellpadding="2" border="1" style="width:100%; margin:10px;">
		<tr>
			<?php

			if( !$isInternalSupplier ){

				?>
				<th><?php echo Dictionnary::translate("gest_com_buying_amount") ; ?></th>
				<th><?php echo Dictionnary::translate("gest_com_selling_amount") ; ?></th>
				<?php

			}


			?>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_strock") ; ?></th>
			<?php
			if( !$isInternalSupplier && !$transit ){

				?>
				<th><?php  echo Dictionnary::translate("gest_com_supplier_franco") ; ?></th>
				<?php

			}
			?>
			<th<?php  if( $transit ) echo " colspan=\"2\""; ?>><?php  echo Dictionnary::translate("gest_com_charge_buy") ; ?></th>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buy") ; ?><?php if( $transit ) echo " " . Dictionnary::translate("import"); ?></th>
			<?php

			if( !$transit ){

				?>
				<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buyand_sell") ; ?></th>
				<?php

			}

			?>
		</tr>
		<tr>
		<?php

			if( !$isInternalSupplier ){

				?>
				<td><?php echo  Util::priceFormat( $buyingAmount ) ?></td>
				<td><?php echo  Util::priceFormat( $orderAmount ) ?></td>
				<?php

			}


			?>
			<td><?php echo  Util::priceFormat( $mb1_amount ) ?></td>
			<td><?php echo  Util::numberFormat( $mb1_rate ) ?> %</td>
			<?php
			if( !$isInternalSupplier && !$transit ){

				?>
				<td>
				<?php

					if( $franco == -1 )
						echo Dictionnary::translate( "gest_com_always_franco" );
					else echo $franco > 0.0 ? Util::priceFormat( $franco ) : "-"

				?>
				</td>
				<?php

			}

			$charges_auto = $Order->hasAutomaticSupplierCharge( $idsupplier );
			$buying_amount = $Order->getSupplierBuyingAmount( $idsupplier );

			if( $transit ){

				if( $charges_auto )
					$charge_rate = $Order->getSupplierChargeRate( $idsupplier );
				else $charge_rate = $supplierCharges / $buying_amount * 100.0;

				?>
				<td><?php echo  Util::numberFormat( $charge_rate ) ?> %</td>
				<?php

			}

			$auto_color = 	"#7243B7";
			$manual_color = "#0036FF";
			$input_color = 	$charges_auto ? $auto_color : $manual_color;

			?>
			<td>
				<input type="radio" name="SupplierChargesAuto_<?php echo  $idsupplier ?>" value="1" id="SupplierChargesAuto_<?php echo  $idsupplier ?>[]" onclick="setSupplierChargesAuto( <?php echo  $idsupplier ?>, '<?php echo  Util::priceFormat( $Order->getSupplierCharges( $idsupplier, true ) ) ?>', '<?php echo  $auto_color ?>' );"<?php if( $charges_auto ) echo " checked=\"checked\""; ?><?php echo  $disabled ?> />
				<span style="font-weight:bold; color:<?php echo  $auto_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_fixed") ; ?></span>
				&nbsp;<input type="radio" name="SupplierChargesAuto_<?php echo  $idsupplier ?>" value="0" id="SupplierChargesAuto_<?php echo  $idsupplier ?>[]" onclick="restoreSupplierCharges( <?php echo  $idsupplier ?>, '<?php echo  $manual_color ?>' );"<?php if( !$charges_auto ) echo " checked=\"checked\""; ?><?php echo  $disabled ?> />
				<span style="font-weight:bold; color:<?php echo  $manual_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_calculate") ; ?></span>
				<br />
				<input type="text" name="supplier_charges_<?php echo  $idsupplier ?>" value="<?php echo  Util::priceFormat( $supplierCharges ) ?>" size="8"<?php if( $charges_auto ) echo " disabled=\"disabled\""; ?> style="font-weight:bold; color:<?php echo  $input_color ?>" />
				<input type="submit" name="UpdateSupplierCharges_<?php echo  $idsupplier ?>" value="ok" />
			</td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo GenerateHTMLHelp('1','Calculée uniquement sur la quantité externe'); ?><?php echo  Util::priceFormat( $mb2_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::numberFormat( $mb2_rate ) ?> %</td>
			<?php

			if( !$transit ){

				?>
				<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::priceFormat( $mb3_amount ) ?></td>
				<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::numberFormat( $mb3_rate ) ?> %</td>
				<?php

			}

			?>
		</tr>
	</table>
	*/

	$error = $Order->getSupplierError( $idsupplier );
	if( !empty( $error ) )
		echo "<p style=\"text-align:center;font-weight:normal;width:89%;\" class=\"msg\">$error</p>";

}

//-----------------------------------------------------------------------------------------------------
/*
function displayInternalSupplierCharges( $idsupplier ){

	global	$Order,
			$disabled,
			$mb2_amount, $mb2_rate;

	$db = &DBUtil::getConnection();

	$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
	$isInternalSupplier = true;
	$idstate = $Order->getSupplierIdState( $idsupplier );
	$transit = $Order->getSupplierTransit( $idsupplier );

	$title = "Dépôt -&gt; Client";

	$weight = $Order->getInternalSupplierWeight( $idsupplier );
	$buyingAmount = $Order->getInternalSupplierBuyingAmount( $idsupplier );
	$orderAmount = $Order->getInternalSupplierOrderAmount( $idsupplier );

	//note : pas de franco pour le fournisseur interne

	$internalSupplierCharges = $Order->getInternalSupplierCharges( $idsupplier );
	$supplierCharges = $Order->getSupplierCharges( $idsupplier );

	//marges

	$unit_cost_amount = 0.0;
	$unit_price = 0.0;
	$quantity = 0;
	$discount_price = 0.0;
	$i = 0;
	while( $i < $Order->getItemCount() ){

		$ref_supplier = $Order->getItemAt( $i)->get( "idsupplier" );
		$ref_idstate = $Order->getSupplierIdState( $ref_supplier );

		if( $ref_supplier == $idsupplier ){

			$ref_unit_cost_amount = $Order->getItemAt( $i)->get( "unit_cost_amount" );
			$ref_unit_price = $Order->getItemAt( $i)->get( "unit_price" );
			$ref_discount_price = $Order->getItemAt( $i )->get( "discount_price" );

			$ref_quantity = $Order->getItemAt( $i)->get( "quantity" );
			$quantity += $ref_quantity;

			$unit_cost_amount += 	$ref_quantity * $ref_unit_cost_amount;
			$unit_price += 			$ref_quantity * $ref_unit_price;
			$discount_price += 		$ref_quantity * $ref_discount_price;

		}

		$i++;

	}

	//marge brute

	$mb0_rate = $unit_price ? ( 1 - ( $unit_cost_amount / $unit_price ) ) * 100 : "-";
	$mb0_amount =  $unit_price - $unit_cost_amount;

	$mb1_rate = $orderAmount ? ( 1 - ( $buyingAmount / $orderAmount ) ) * 100 : "-";
	$mb1_amount = $orderAmount - $buyingAmount;

	//frais de port vente (approximation : répartition égale par fournisseur )

	$supplierCount = $Order->getSupplierCount();

	//marge avec port achat

	$mb2_rate = $orderAmount ? ( 1 - ( ( $buyingAmount + $supplierCharges ) / $orderAmount ) ) * 100 : "-";
	$mb2_amount = $orderAmount - $buyingAmount - $supplierCharges;

	//marge avec port achat et vente

	$mb3_rate = $discount_price ? ( 1 - ( ( $unit_cost_amount + $supplierCharges + $internalSupplierCharges ) / $discount_price ) ) * 100 : "-";
	$mb3_amount = $discount_price - ( $unit_cost_amount + $supplierCharges + $internalSupplierCharges );

	$idstate = $Order->getSupplierIdState( $idsupplier );

	$weight = $Order->getInternalSupplierWeight( $idsupplier );
	$buyingAmount = $Order->getInternalSupplierBuyingAmount( $idsupplier );
	$orderAmount = $Order->getInternalSupplierOrderAmount( $idsupplier );

	$charges_auto = $Order->hasAutomaticInternalSupplierCharge( $idsupplier );

	?>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<p style="text-align:left; font-weight:bold; margin-left:10px; color:#000000; font-size:14px;"><?php echo  $title ?></p>
	<table cellspacing="0" cellpadding="2" border="1" style="width:100%; margin:10px;">
			<!--
			<td style="background-color:#E0E0E0;">Poids</td>
			<td style="background-color:#E0E0E0;">Montant Achats</td>
			<td style="background-color:#E0E0E0;">Montant Cde</td>
			-->
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_strock") ; ?></th>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buy") ; ?><?php if( $idstate ) echo " Import"; ?></th>
			<th><?php  echo Dictionnary::translate("gest_com_charge_resailer") ; ?></th>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buyand_sell") ; ?></th>
		</tr>
		<tr>
			<!--
			<td style="background-color:#E0E0E0;"><?php echo  Util::numberFormat( $weight ) ?></td>
			<td style="background-color:#E0E0E0;"><?php echo  Util::priceFormat( $buyingAmount ) ?></td>
			<td style="background-color:#E0E0E0;"><?php echo  Util::priceFormat( $orderAmount ) ?></td>
			-->
			<?php

			$auto_color = 	"#7243B7";
			$manual_color = "#0036FF";
			$input_color = 	$charges_auto ? $auto_color : $manual_color;

			?>
			<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::priceFormat( $mb1_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::numberFormat( $mb1_rate ) ?> %</td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo GenerateHTMLHelp('2','Marge sur quantité externe + Marge sur quantité interne'); ?><?php echo  Util::priceFormat( $mb2_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::numberFormat( $mb2_rate ) ?> %</td>
			<td>
				<input type="radio" name="InternalSupplierChargesAuto_<?php echo  $idsupplier ?>" value="1" id="InternalSupplierChargesAuto_<?php echo  $idsupplier ?>[]" onclick="setInternalSupplierChargesAuto( <?php echo  $idsupplier ?>, '<?php echo  Util::priceFormat( $Order->getInternalSupplierCharges( $idsupplier, true ) ) ?>', '<?php echo  $auto_color ?>' );"<?php if( $charges_auto ) echo " checked=\"checked\""; ?> />
				<span style="font-weight:bold; color:<?php echo  $auto_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_contractual") ; ?></span>
				<input type="radio" name="InternalSupplierChargesAuto_<?php echo  $idsupplier ?>" value="0" id="InternalSupplierChargesAuto_<?php echo  $idsupplier ?>[]" onclick="restoreInternalSupplierCharges( <?php echo  $idsupplier ?>, '<?php echo  $manual_color ?>' );"<?php if( !$charges_auto ) echo " checked=\"checked\""; ?> />
				<span style="font-weight:bold; color:<?php echo  $manual_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_calculate") ; ?></span>
				<br />
				<input type="text" class="textInput price" name="internal_supplier_charges_<?php echo  $idsupplier ?>" value="<?php echo  Util::priceFormat( $internalSupplierCharges ) ?>" <?php if( $charges_auto ) echo " disabled=\"disabled\""; ?> style="font-weight:bold; color:<?php echo  $input_color ?>" />
				<input type="submit" class="blueButton" name="UpdateInternalSupplierCharges_<?php echo  $idsupplier ?>" value="ok" />
			</td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::priceFormat( $mb3_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo  Util::numberFormat( $mb3_rate ) ?> %</td>
		</tr>
	</table>
	<?php

}
*/
//-----------------------------------------------------------------------------------------------------

function GenerateHTMLHelp( $id,$help ){

	global $GLOBAL_START_URL;

	$html="<div onMouseOver=\"document.getElementById('pop$id').className='tooltiphover';\" onMouseOut=\"document.getElementById('pop$id').className='tooltip';\" class='tooltipa'><img class=\"helpicon\" src=\"$GLOBAL_START_URL/images/back_office/content/icon_help.gif\" alt=\"\" />";

	$html.="<div id='pop$id' class='tooltip' style='width:150px;'>".$help."</div></div>";

	return $html;

}

?>
