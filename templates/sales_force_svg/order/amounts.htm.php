<?php 
	anchor( "UpdateInstallmentAmount" ); 
	anchor( "UpdateTotalChargeHT" ); 
	anchor( "UpdateChargeSupplierRate" );
	anchor( "UpdateChargeSupplierAmount" );
	anchor( "UpdatePaymentDelay" );
	anchor( "ApplySupplierCharges" );
?>
<script type="text/javascript">
/* <![CDATA[ */
	function showAmountsInfos() {
		var tabrem = document.getElementById( 'TabRem' );
		tabrem.style.display = tabrem.style.display == 'block' ? 'none' : 'block';
		
		var tabesc = document.getElementById( 'TabEsc' );
		tabesc.style.display = tabesc.style.display == 'block' ? 'none' : 'block';
		
		var tabmt = document.getElementById( 'TabMt' );
		tabmt.style.display = tabmt.style.display == 'block' ? 'none' : 'block';
		
		var tabfrs = document.getElementById( 'TabFrs' );
		tabfrs.style.display = tabfrs.style.display == 'block' ? 'none' : 'block';
		
		var tabttctitle = document.getElementById( 'TabTTCTitle' );
		tabttctitle.style.display = tabttctitle.style.display == 'block' ? 'none' : 'block';
		
		var tabttccont = document.getElementById( 'TabTTCCont' );
		tabttccont.style.display = tabttccont.style.display == 'block' ? 'none' : 'block';
	}
/* ]]> */
</script>    

<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>

<div class="contentResult" style="margin-bottom:10px;">

	<h1 class="titleEstimate">
	<span class="textTitle">Montants et tarifications de la commande</span>
	<div class="spacer"></div>
	</h1>
	<div class="spacer"></div>
	
	<div class="blocEstimateResult" style="margin:0;"><div style="margin:5px;">
	
	
		<div class="floatleft" style="width:89%;">
			<table class="dataTable devisTable">
	            <tr>
	            	<?php if(!B2B_STRATEGY){
	?><th>Prix vente<br />TTC brut </th><?php
}
else{
	?><th>Prix vente<br />HT brut</th><?php
	
} ?>
	                <th colspan="2">Remise supplémentaire<br />sur facture</th>
					<th>Total HT net</th>
					<th colspan="2">Port vente refacturé</th>
					<th>
					<?php if( B2B_STRATEGY ){?>
            		Total HT
				<?php }else{ ?>
					Total TTC
				<?php } ?>
									
					<br />avec port vente</th>
					<th colspan="2">Remise pour<br />paiement comptant</th>
					<th>Total HT</th>
					<th>Taux TVA</th>
					<th>Montant TVA</th>
					<th>Total TTC</th>
				</tr>
	            <tr>
	            <?php
	            
	            	$articlesET = 0.0;
					$it = $Order->getItems()->iterator();
					while( $it->hasNext() )
						$articlesET += $it->next()->getTotalET();
					
	            	?>
					 <?php
            
            	$articlesATI = 0.0;
				$it = $Order->getItems()->iterator();
				while( $it->hasNext() )
					$articlesATI += $it->next()->getTotalATI();
					
				$i = 0;
				
				$articlesVAT = 0.0;
				while( $i < $Order->getItemCount() ){
		
		
				$articlesVAT += ($Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ))*($Order->getItemAt($i)->getVATRate())/100;
				
				$i++;
		
	}
					
				
				?>
				<?php if(B2B_STRATEGY){
	?><td rowspan="3"><?php echo Util::priceFormat( $articlesET ); ?></td><?php
}
else{
	?><td rowspan="3"><?php echo Util::priceFormat( $articlesATI ); ?></td><?php
	
} ?>

					
					<td rowspan="3" style="white-space:nowrap;"><input<?php echo $disabled ?> class="textInput percentage" name="total_discount_rate" value="<?php echo Util::numberFormat($total_discount_rate) ?>" /> %</td>
					<td rowspan="3" style="white-space:nowrap;"><input<?php echo $disabled ?> class="textInput price" name="total_discount_amount" value="<?php echo Util::numberFormat( $total_discount_amount ) ?>" /> &euro;</td>
					
					<?php if( B2B_STRATEGY ){?>
            	<td rowspan="3"><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) ?></td>
				<?php }else{ ?>
				<td rowspan="3"> </td>
				<?php } ?>

					<td colspan="2">
						<input type="hidden" name="UpdateTotalChargeHT" id="UpdateTotalChargeHT" value="">
						<select<?php echo $disabled; ?> name="total_charge_auto" id="total_charge_auto" onchange="document.getElementById('UpdateTotalChargeHT').value='ok';">
							<option value="1" onclick="setChargesAuto( '<?php echo Util::numberFormat( $Order->getChargesStrategy()->getChargesET() ) ?>' );" <?php if( $Order->get( "total_charge_auto" ) ) echo " selected=\"selected\""; ?><?php echo $disabled ?>>Coût paramétré</option>
							<option value="0" onclick="restoreCharges();"<?php if( !$Order->get( "total_charge_auto" ) && !$Order->get( "charge_free" ) ) echo " selected=\"selected\""; ?><?php echo $disabled ?>>Coût calculé</option>
							<option value="0" onclick="chargeFree();"<?php if( $Order->get( "charge_free" ) ) echo " selected=\"selected\""; ?>>Offert</option>
						</select>
						<input type="hidden" name="charge_free" id="charge_free" value="<?php echo $Order->get( "charge_free" ); ?>" />
					</td>
					
					<?php if( B2B_STRATEGY ){?>
            	<td rowspan="3"><b><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET() ) ?></b></td>
				<?php }else{ ?>
				<td rowspan="3"> </td>
				<?php } ?>
				
					<td rowspan="3" style="white-space:nowrap;">
						<input<?php echo $disabled ?> type="text" class="textInput percentage" name="billing_rate" value="<?php echo Util::numberFormat( $billing_rate ) ?>" /> %
					</td>
					<td rowspan="3" style="white-space:nowrap;">
						<input<?php echo $disabled ?> type="text" class="textInput price" name="billing_amount" value="<?php echo Util::numberFormat( $billing_amount ) ?>" /> &euro;
					</td>
					
					
 <?php 
				
				if( B2B_STRATEGY ){
								
				$ht = $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET();
				
				$vat = $articlesVAT - $articlesVAT * $total_discount_rate/100 + $Order->getChargesET()*$Order->getVATRate()/100;     
				
				$ttc = $articlesATI - ($articlesATI*$total_discount_rate/100) + $Order->getChargesATI();

				?>
                
                <!-- HT -->
                <td rowspan="3">
					<b><span id="tht">
				<?php echo Util::priceFormat( $ht ); ?>
					</span></b>
				</td>
                
                <!-- TVA (%) -->
				<td rowspan="3">
					<span id="ttva"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></span>
				</td>
                
                <!-- TVA (€) -->
				<td rowspan="3">
					<span id="mtva"> <?php echo Util::priceFormat( $vat  ); ?>					
					</span>
				</td>
                				
                <!-- TTC -->
                <td rowspan="3">
					<b><span id="tttc"><?php echo Util::priceFormat($ht + $vat); ?>
					
					</span></b>
				</td>
                
                <?php }else{
                
                $ht = $articlesET - ( ($articlesET*$total_discount_rate)/100 )+  ( $Order->getChargesET() );
                
                $ttc = $articlesATI - ($articlesATI*$total_discount_rate/100) + $Order->getChargesATI();
                 ?>
                <!-- HT -->
                <td rowspan="3">
					<b><span id="tht">
					<?php echo Util::priceFormat( $ht  ); ?>
			
					</span></b>
				</td>
                
                <!-- TVA (%) -->
				<td rowspan="3">
					<span id="ttva"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></span>
				</td>
                
                 <!-- TVA (€) -->
				<td rowspan="3">
					<span id="mtva"><?php echo Util::priceFormat($ttc - $ht ); ?>
					</span>
				</td>
                
                <!-- TTC -->
                <td rowspan="3">
					<b><span id="tttc"><?php echo Util::priceFormat($ttc ); ?></span></b>
				</td>
                <?php
				} 
				?>
                    
                    
                
                </tr>
				<tr>
					<th>Poids</th>
					 <?php 
				
				if( B2B_STRATEGY ){
				?>
            	<th>Port HT</th>
                <?php } else { ?>
                <th>Port TTC</th>
                <?php
				} ?>
				</tr>
				<tr>
					<td><?php echo Util::numberFormat( $Order->getWeight() ) ; ?>&nbsp;kg</td>
					<td style="white-space:nowrap;">
						<input<?php if( $Order->get( "charge_free" ) || $Order->get( "total_charge_auto" ) ){ echo " disabled=\"disabled\""; } else echo $disabled ?> class="textInput" style="width:35px;" name="total_charge_ht" value="<?php 
						
							if( $Order->get( "charge_free" ) )
								echo "Offert";
							else if( $Order->get( "total_charge_auto" ) )
							
								if( B2B_STRATEGY ){
							
	            				echo $Order->getChargesET() > 0.0 ?  Util::numberFormat( $Order->getChargesET()  ) : "Franco"; 
	            				}else{
								echo $Order->getChargesATI() > 0.0 ?  Util::numberFormat( $Order->getChargesATI()  ) : "Franco";
								}
							
							
							else{
						
								if( B2B_STRATEGY )
									{
									 echo Util::numberFormat( $Order->get( "total_charge_ht" )  );
            						}
									else
									{
									 echo Util::numberFormat( $Order->get( "total_charge" )  );
            						}
							}
							
						?>" /> &euro;
					</td>
				<tr>
	    	</table>
	    	<div class="spacer"></div>
		
			<div class="floatright" style="margin-top:5px;">
		        <input type="submit" class="blueButton" name="SaveOrder" value="Recalculer" />
		    </div>
		    <div class="spacer"></div>
	    	
	    	<?php 
				$downPaymentRequired = false;
				$it = $Order->getItems()->iterator();
				while( !$downPaymentRequired && $it->hasNext() )
					$downPaymentRequired |= DBUtil::getDBValue( "type", "detail", "idarticle", $it->next()->get( "idarticle" ) ) != "catalog";

					if( $downPaymentRequired ) {	
			?>
					<p class="msg" style="text-align:center;">!!! Cette commande contient un ou plusieurs produits spécifiques / alternatifs, pensez à demander un acompte au client !!!</p>
			<?php			
					} 
			?>
	    	
		</div><!-- leftContainer -->
		<div class="floatright" style="width:10%;">
	        <table class="dataTable devisTable summaryTable">
	            <tr>
	                <th colspan="2">Marge nette après <br /> Port Achat et Vente</th>
	            </tr>
				
				<tr>
                <td colspan="1">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
                	<span id="mnf">
                	<?php 
					$netMarginAmount = $Order->get( "net_margin_amount" );
                	echo Util::priceFormat( $netMarginAmount ); 
                	 ?></span>
					<?php } ?>
					<?php if($akilae_comext ==1){ ?>
					<span style="color:red !important;"> / 
					<?php 
					$netMarginAmount_com = $Order->get( "net_margin_amount" ) / (1+ $_SESSION['rate']/100) ;
                	echo Util::priceFormat( $netMarginAmount_com ); 
					?>						
					</span>	
					<?php } ?>			
                </td>
            </tr>
                    <tr>
                        <th>%</th>
                     </tr>
            <tr>
                <td style="width:50%">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
                	<span id="mnfp">
                	<?php echo Util::rateFormat( $Order->get( "net_margin_rate" ) ) ?></span>
					<?php } ?>
					<?php if($akilae_comext ==1){ ?>
					<span style="color:red !important;"> / 
					<?php echo Util::rateFormat( $Order->get( "net_margin_rate" ) - $_SESSION['rate'] ) ?></span>
					<?php } ?>
                </td>
            </tr>
				
	        </table>
		</div><!-- rightContainer -->
		<div class="spacer"></div>
	</div></div>
</div>

       