<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE MONTANTS ++++++++++++++++++++++++++++++++++++++++++ -->
<?php 

anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateTotalDiscountAmount" );
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" ); 
anchor( "UpdateInstallmentRate" ); 
anchor( "UpdateInstallmentAmount" ); 
anchor( "UpdateTotalChargeHT" ); 
anchor( "UpdateChargeSupplierRate" );
anchor( "UpdateChargeSupplierAmount" );
anchor( "UpdatePaymentDelay" );
anchor( "ApplySupplierCharges" );


?>
<script type="text/javascript" language="javascript">
<!--
function showAmountsInfos(){
	
	var tabrem = document.getElementById( 'TabRem' );
	tabrem.style.display = tabrem.style.display == 'block' ? 'none' : 'block';
	
	var tabesc = document.getElementById( 'TabEsc' );
	tabesc.style.display = tabesc.style.display == 'block' ? 'none' : 'block';
	
	var tabmt = document.getElementById( 'TabMt' );
	tabmt.style.display = tabmt.style.display == 'block' ? 'none' : 'block';
	
	var tabfrs = document.getElementById( 'TabFrs' );
	tabfrs.style.display = tabfrs.style.display == 'block' ? 'none' : 'block';
	
	var tabttctitle = document.getElementById( 'TabTTCTitle' );
	tabttctitle.style.display = tabttctitle.style.display == 'block' ? 'none' : 'block';
	
	var tabttccont = document.getElementById( 'TabTTCCont' );
	tabttccont.style.display = tabttccont.style.display == 'block' ? 'none' : 'block';
}

function changeCharge(){
	var chargeTTC =  document.getElementById("total_charge_ht").value ;
	//alert(chargeTTC);
	var chargeHT = chargeTTC / 1.196 ;
	//alert(chargeHT);
	document.getElementById("total_charge_ht").value = chargeHT ;
}

// -->
</script>

<div class="mainContent fullWidthContent">
	<div class="topRight"></div><div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Montants et tarifications de la commande</p>
				<div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
            </div>
            <div class="tableContainer">

				<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
				<br />
				<?php 
					
					$downPaymentRequired = false;
					$it =& $Order->iterator();
					while( !$downPaymentRequired && $it->hasNext() ){
						
						$item =& $it->next();
						
						$downPaymentRequired |= DBUtil::query( "SELECT type FROM detail WHERE idarticle = '" . $item->getValue( "idarticle" ) . "' AND type NOT LIKE 'catalog' LIMIT 1" )->RecordCount() > 0;
						
					}
					
					if( $downPaymentRequired ){
						
						?>
						<div class="msg" style="text-align:center;">!!! Cette commande contient un ou plusieurs produits spécifiques / alternatifs, pensez à demander un acompte au client !!!</div>
						<?php
							
					} 
				
				?>
				<!--<b>
					<a href="#" onclick="showAmountsInfos(); return false;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/show.gif" alt="Afficher/Masquer les informations" style="border-style:none;" align="middle" />&nbsp;&nbsp;Voir toutes les informations
					</a>
				</b>-->
				<?php $total_charge_auto = $Order->getValue( "total_charge_auto" ); ?>
				<table class="dataTable devisTable">
					<tr>
						<th colspan="2" style="width:100px;">Remise sur facture</th>
						<th style="width:100px;">
							Montant Net HT
						</th>
						<th style="width:100px;">
							Montant Net TTC
						</th>
						<th>Montant TVA</th>
						<th>Poids Total</th>
						<!--<th colspan="2" style="width:100px;">Coût port vente</th>-->
						
						<th>Frais de port <br /> <!-- Coût port vente TTC refacturé -->
						<input type="hidden" name="UpdateTotalChargeHT" id="UpdateTotalChargeHT" value="">
										<select name="total_charge_auto" id="total_charge_auto" onchange="document.getElementById('UpdateTotalChargeHT').value='ok';">
											<option value="1" onclick="setChargesAuto( '<?php echo Util::numberFormat( $Order->WotCharge( true ) ) ?>' );" <?php if( $total_charge_auto ) echo " selected=\"selected\""; ?><?php echo $disabled ?>>Coût calculé</option>
											<option value="0" onclick="restoreCharges();"<?php if( !$total_charge_auto ) echo " selected=\"selected\""; ?><?php echo $disabled ?>>Coût paramètré</option>
										</select>
						</th>
						<!--
						<th colspan="2" style="width:100px;">Remise pour paiement comptant</th>
						<th style="width:50px;">Total HT après remise pour paiement comptant</th>
						<th colspan="2">Acompte</th>
						-->
						<!--<th style="width:50px;">TTC à payer</th>-->
						<th colspan="2" style="width:100px;">Marge nette après ports</th>
						<th style="width:50px;">TTC à payer</th>
					</tr>
					<tr>
						<td>
							<input<?php echo $disabled ?> class="textInput" style="width:35px;" name="total_discount_rate" size="5" value="<?php echo Util::numberFormat($total_discount_rate) ?>" onchange="document.getElementById('UpdateTotalDiscountRate').value='ok'; document.getElementById('UpdateTotalDiscountAmount').value='';"/>%
							<input type="hidden" name="UpdateTotalDiscountRate" id="UpdateTotalDiscountRate" value="">
						</td>
						<td>
							<input disabled class="textInput" style="width:35px;" name="total_discount_amount" size="5" value="<?php echo Util::numberFormat( $total_discount_amount ) ?>" onchange="document.getElementById('UpdateTotalDiscountAmount').value='ok'; document.getElementById('UpdateTotalDiscountRate').value='';"/>&euro;
							<input type="hidden" name="UpdateTotalDiscountAmount" id="UpdateTotalDiscountAmount" value="">
						</td>
						
						<td>
							<?php echo Util::priceFormat( $ht ) ?>
						</td>
						<td>
							<?php echo Util::priceFormat( $ttc ) ?>
						</td>
						<td>
							<?php
							$tva = $ttc - $ht ;
							echo Util::priceFormat( $tva ) ;
							?>
						</td>
						<!--<td>
							<?php echo Util::numberFormat( $Order->getValue( "charge_supplier_rate" ) ) ?> %
						</td>
						<td>
							<?php echo Util::priceFormat( $Order->getValue( "charge_supplier" ) ) ?>
						</td>-->
						<td>
						<?php
						$Order->GetTotal_weight();
						echo Util::numberFormat($Order->getValue('total_weight')) ?>&nbsp;kg
						
						</td>
						<td>
							<!--
							<table style="border:none;width:100%;" cellpadding=0 cellspacing=0>
								<tr>
									<td colspan="2" style="border:none;">-->
										
										<!--
										<input type="radio" name="total_charge_auto" value="1" id="total_charge_auto" onclick="setChargesAuto( '<?php echo Util::priceFormat( $Order->WotCharge( true ) ) ?>' );" <?php if( $total_charge_auto ) echo " checked=\"checked\""; ?><?php echo $disabled ?> />
										<span>Coût calculé</span><br />
										<input type="radio" name="total_charge_auto" value="0" id="total_charge_auto" onclick="restoreCharges();"<?php if( !$total_charge_auto ) echo " checked=\"checked\""; ?><?php echo $disabled ?> />
										<span>Coût paramètré</span>
										-->
										
										<!--<input type="hidden" name="UpdateTotalChargeHT" id="UpdateTotalChargeHT" value="">
										<select name="total_charge_auto" id="total_charge_auto" onchange="document.getElementById('UpdateTotalChargeHT').value='ok';">
											<option value="1" onclick="setChargesAuto( '<?php echo Util::numberFormat( $Order->WotCharge( true ) ) ?>' );" <?php if( $total_charge_auto ) echo " selected=\"selected\""; ?><?php echo $disabled ?>>Coût calculé</option>
											<option value="0" onclick="restoreCharges();"<?php if( !$total_charge_auto ) echo " selected=\"selected\""; ?><?php echo $disabled ?>>Coût paramètré</option>
										</select>
									</td>
									
								</tr>
								<tr>
									<td style="border:none;"><?
										$Order->GetTotal_weight();
										echo Util::numberFormat($Order->getValue('total_weight')) ?>&nbsp;kg
									</td>
									<td style="border:none;">
										
									</td>
								<tr>
							</table>-->
						<input<?php echo $disabled ?> id="total_charge_ht" class="textInput" style="width:35px;" name="total_charge_ht" value="<?php if( $total_charge_auto ) { if($Order->getValue('total_charge')>0){ echo Util::numberFormat($Order->getValue('total_charge'));}else{echo "Franco";}}else{echo Util::numberFormat($Order->getValue('total_charge'));} ?>" <?php if( $total_charge_auto ) echo " disabled=\"disabled\""; ?>/>&euro;
						</td>
						
						<!--<td>
							A<input<?php echo $disabled ?> type="text" class="textInput" style="width:35px;" name="billing_rate" size="5" value="<?php echo Util::numberFormat( $billing_rate ) ?>" onchange="document.getElementById('UpdateBillingRate').value='ok'; document.getElementById('UpdateBillingAmount').value='';">%
							<input type="hidden" name="UpdateBillingRate" id="UpdateBillingRate" value="">
						</td>
						<td>
							B<input<?php echo $disabled ?> type="text" class="textInput" style="width:35px;" name="billing_amount" size="5" value="<?php echo Util::numberFormat( $billing_amount ) ?>" onchange="document.getElementById('UpdateBillingAmount').value='ok'; document.getElementById('UpdateBillingRate').value='';"/>&euro;
							<input type="hidden" name="UpdateBillingAmount" id="UpdateBillingAmount" value="">
						</td>
						
						<td>
							C<?php echo Util::priceFormat( $Order->GetTotal_amount_ht_wb() ) ?>
						</td>
						
						<td>
							D<input type="text" class="textInput percentage" name="down_payment_rate" value="<?php echo Util::numberFormat( $down_payment_rate ) ?>" onchange="document.getElementById('UpdateInstallmentRate').value='ok'; document.getElementById('UpdateInstallmentAmount').value='';"/>%
							<input type="hidden" name="UpdateInstallmentRate" id="UpdateInstallmentRate" value="">
						</td>
						<td>
							E<input type="text" class="textInput price" name="down_payment_amount" value="<?php echo Util::numberFormat( $down_payment_amount ) ?>" onchange="document.getElementById('UpdateInstallmentAmount').value='ok'; document.getElementById('UpdateInstallmentRate').value='';"/>&euro;
							<input type="hidden" name="UpdateInstallmentAmount" id="UpdateInstallmentAmount" value="">
						</td>-->
						
						
						
						<td>
							<?php echo Util::numberFormat( Order::getNetMarginRate( $Order->getId() ) ) ?> %
						</td>
						<td>
							<?php echo Util::priceFormat( Order::getNetMarginAmount( $Order->getId() ) ) ?>
						</td>
						<td>
							<?php echo Util::priceFormat( $Order->GetTotal_amount_ttc_wbi() ) ?>
						</td>
					<tr>
					<tr>
						<td colspan="10" style="border:none;"><input type="submit" style="float:right;margin-top:5px;margin-right:10px;" class="blueButton" value="Modifier" name="SaveOrder" onclick=""changeCharge();"/></td>
					</tr>
				</table>
				
			</div>
		</div>
	</div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div>