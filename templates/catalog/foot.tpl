	
	<!--====================================== Baseline ======================================-->

        <div class="clear"></div>
		
		
        <!--====================================== Cat&eacute;gories ======================================-->
  <b><b>
        <div id="footer">
    	{if:iduser}<span style="color:red;font-weight: bold;">/foot.tpl</span>{end:}
		
			
    </div>
	<flexy:include src="google_analytics.tpl" async></flexy:include>
	
	<script src="{start_url}/js/85528.js" type="text/javascript" async></script>
<script type="text/javascript">
	/*$('select#search-marque ~ .sbHolder').find('.sbOptions')
	.attr('style', 'width: 118px !important')
	.hide();*/
	$('#mylogin').click(function(e) {
	e.stopPropagation();
	$('#loginpanel').animate({top:0}, {queue:false, easing:'easeOutCirc', duration:200});
	if ($('#header').hasClass('opened')) {
			$('#header + #menu #mainFrontMenu').fadeOut();
			$('#header #SearchForm').fadeOut();
		}
	});
	$("body").click(function(e) {
	if (e.target.id == "loginpanel" || $(e.target).parents("#loginpanel").size()) e.stopPropagation();
	else hidelogin();
	if (e.target.id == "menu" || $(e.target).parents("#menu").size()) e.stopPropagation();
	else menureset();
	});
	$("form#login #email, form#login #password").textPlaceholder();
	$("form#search #search-text").textPlaceholder();
	$("form#newsletter #emailnews").textPlaceholder(); 
	
	
	
	function hidelogin () {
		$('#loginpanel').animate({top:-250}, {queue:false, easing:'easeOutCirc', duration:300 });
		if ($('#header').hasClass('opened')) {
			$('#header + #menu #mainFrontMenu').fadeIn();
			$('#header #SearchForm').fadeIn();
		}
	} 
	
	$('#icons_compare').mouseenter(function(e) {
	$(this).find('div').stop().toggle('fast');
	})
	$('#icons_compare').mouseleave(function(e) {
	$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseenter(function(e) {
	$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseleave(function(e) {
	$(this).find('div').stop().toggle('fast');
	}) 
</script>
<script type="text/javascript">
function verifLogin (){
	var login = document.forms.RegisterForm.elements[ 'Login' ].value;
	var password = document.forms.RegisterForm.elements[ 'Password' ].value;
	if(login == ''){
		alert('Le champ Identifiant est vide !!!');
		return false;
	}
	if(password == ''){
		alert('Le champ Mot de passe est vide !!!');
		return false;
	}
	return true ;
}
function verifCreateAccount() {
	var data = $('#CreateAccount').formSerialize();
	$.ajax({
		url: "/catalog/account.php?createLoginAjax",
		async: true,
		type: "POST",
		data: data,
		error : function( XMLHttpRequest, textStatus, errorThrown ){ },
		success: function( responseText ){
			if(responseText != '') {
				$("#returnCreateAccount").html(responseText);
			} else {
				document.getElementById("CreateAccount").submit();
			}
		}
	});
}
//Mot de passe oublié
function forgotPassword() {
	$( "#forgotPassword" ).dialog({
		title:"Mot de passe oublié ?",
		bgiframe: true,
		modal: true,
		width:550,
		minHeight:10,
		closeText: "Fermer",
		/*overlay: {
		backgroundColor: '#000000',
		opacity: 0.5
		},*/
		close: function(event, ui) { $( "#forgotPassword" ).dialog( "destroy" ); }
	});
}
//Contrôle de l'email saisie pour le mot de passe oublié
function sendPassword( Email, divResponse ) {
	data = "&Email="+Email;
	$.ajax({
		url: "/catalog/forgetmail.php",
		async: true,
		type: "POST",
		data: data,
		error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer l'email" ); },
		success: function( responseText ) {
			$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>" + responseText + "</p>");
		}
	});
}
// Fonction de sélection des informations newsletter
function newsletter () {
	test = window.open(start_url + '/catalog/newsletter.php','hw','width=380,height=450,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1')
}
function menureset () {
	$('#menu > ul > li > a')
	.stop()
	.animate({ color: '#fff', backgroundColor: 'transparent' }, 100, 'easeOutCirc' )
	.removeClass('nosub');
	$('#menu').find('.submenu')
	.css('marginTop','-5px')
	.css('display','none');
} 

(function () {

		var mobile = document.createElement('div');
		mobile.className = 'nav-mobile';
		document.querySelector('#menu').appendChild(mobile);

		function hasClass(elem, className) {
			return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
		}

		function toggleClass(elem, className) {
			var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
			if (hasClass(elem, className)) {
				while (newClass.indexOf(' ' + className + ' ') >= 0) {
					newClass = newClass.replace(' ' + className + ' ', ' ');
				}
				elem.className = newClass.replace(/^\s+|\s+$/g, '');
			} else {
				elem.className += ' ' + className;
			}
		}

		var mobileNav = document.querySelector('.nav-mobile');
		var header = document.querySelector('#header');
		var largeur_fenetre = $(window).width();
		if(largeur_fenetre <= 999){
			$('#mainFrontMenu > li').each(function(index) {
				$(this).click(function(e){
					if (this.className == 'clicked') {
						this.className = '';
					} else {
						$('#mainFrontMenu > li').each(function(index) {
							this.className = '';
						});
						this.className = 'clicked';
						e.preventDefault();
					}
				});
			});
		}
				
		mobileNav.onclick = function (e) {
			toggleClass(this, 'nav-mobile-open');
			toggleClass(header, 'opened showMe');
			$('#menu > #mainFrontMenu').addClass('showMe');
		};
		$('div.category_description p').filter(function () { 
			return this.innerHTML
				.replace("&nbsp;"," ")
				.trim() 
				== "" 
		}).remove();

	})();
</script>
<!-- Code Google de la balise de remarketing -->
{if:pagename}
	{scriptRemarketing:h}
{else:}
	{if:head}
		{head.getScriptRemarketing():h}
	{end:}
{end:}

<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = ......;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="{start_url}/js/conversion.js" >
</script>
<noscript>
<div style="display:inline;">
<img height="1" alt="google ads" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/........../?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

</body>
</html> 

