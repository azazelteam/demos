 
<!-- Début Header fiche produit -->
	<flexy:include src="/product/product_detail_header.tpl"></flexy:include>
<!-- Fin Header fiche produit -->

<script type="text/javascript">
<!--

	function checkOnglet(actif, none) {
		//Rendre inactif
		document.getElementById('bloc_similaire').style.display='none';
		document.getElementById('bloc_complementaire').style.display='none';
		document.getElementById('bloc_accessoires').style.display='none';
		document.getElementById('onglet_similaire').className ='';
		document.getElementById('onglet_complementaire').className ='';
		document.getElementById('onglet_accessoires').className ='';
		
		//Bloc produits
		document.getElementById('bloc_'+actif).style.display='block';
		
		//Onglet
		document.getElementById('onglet_'+actif).className ='actif';
	}
-->
</script>


<!-- Produits similaires -->
<div class="basic" style="float:left; margin:0 0 0 4px;" id="list1a">
	{if:iduser}<span style="color:red;font-weight: bold;">/product/product_detail_8.tpl</span>{end:}
	{if:hasSimilars}
	<a class="item" style="-khtml-border-radius:3px; -moz-border-radius-topleft:3px; -moz-border-radius-topright:3px; ">Voir les produits similaires</a>
	<!-- Produits similaires -->	
	<div class="article_similaire">
		{foreach:similars,similar}
			<div class="similaire" onclick="document.location='{similar[productURL]}'" style="cursor:pointer; font-size:11px;">
				<img src="{similar[icone]}" alt="{similar[name]:h}" width="70" />
				{if:similar[labelImage]}
					<img src="{similar[labelImage]}" class="starProduct" alt="similar[labelName]" width="20" />
				{end:}
				<div class="spacer"></div>
				<span class="titleSim">{similar[name]:h}</span>
			</div>
		{end:}
	</div>
	{end:}
	
	{if:hasComplements}
	<a class="item">Voir les produits complémentaires</a>
	<!-- Produits complementaires -->
	<div class="article_similaire">
		{foreach:complements,complement}
			<div class="similaire" onclick="document.location='{complement[productURL]}'" style="cursor:pointer; font-size:11px;">
				<img src="{complement[icone]}" alt="{complement[name]:h}" width="70" />
				{if:complement[labelImage]}
					<img src="{complement[labelImage]}" class="starProduct" alt="complement[labelName]" width="20" />
				{end:}
				<div class="spacer"></div>
				<span class="titleSim">{complement[name]:h}</span>
			</div>
		{end:}
	</div>
	{end:}
	
	{if:hasassociates}
	<a class="item">Découvrir les accessoires</a>
	<!-- Accessoires -->
	<div class="article_similaire2">
		<form id="formAccessoires" action="{start_url}/catalog/basket.php" method="post">
		<div class="scrollProduct">
			<input type="hidden" name="min_cde" value="" />
			<input type="hidden" name="idcolor" value="" />
			<input type="hidden" name="idtissus" value="" />
			<input type="hidden" name="checked" id="checked" value="0" />
			
			<h1 class="titleAccessoires">Sélectionner vos accessoires</h1>
			<div class="addAccessoires">
				<table cellspacing="1" border="0" cellpadding="0">
					{foreach:associates,associate}
					<tr>
						<td class="bgSelectAcces">
						{if:associate[WotPrice]}
							<input type="checkbox" name="checkboxAdd" id="checkbox_{associate[idarticle]}" onclick="checkedArticle('{associate[idarticle]}', 'checkbox_{associate[idarticle]}');" />
							<input type="hidden" name="none[]" value="{associate[idarticle]}"  id="idassoc_{associate[idarticle]}" />
							<input type="hidden" name="quantity_{associate[idarticle]}" value="1" />
						{else:}
							<strong>x</strong>
						{end:}
						</td>
						<td><img src="{associate[icone]}" width="60" alt="{associate[name]}" /></td>
						<td>{associate[name]}<br/>
						{if:associate[designation]}
							{associate[designation]:h}<br/>
						{end:}
						<span class="greyAcces">Réf.: {associate[ref]}</span><br/>
						{if:associate[WotPrice]}
							<strong class="priceAcces">{associate[WotPrice]:h}&euro;</strong>
						{else:}
							<strong class="priceAcces" href="mailto:{associate[mail]}">Contactez-nous</a>
						{end:}</td>
					</tr>
					{end:}
				</table>
			</div>
		</div>
		<div class="btnaddAccess">	
			<input type="submit" class="btnAssociateDev" onclick="document.getElementById('formAccessoires').action='{start_url}/catalog/estimate_basket.php'; return validAssociate();" name='AddDevAcs' id='AddDevAcs' value="Demander un devis" />
			<input type="submit" class="btnAssociateCom" onclick="return validAssociate();" name="AddCdeAcc" value="Panier commande" />
		</div>
		</form>
	</div>
	{end:}	
</div>

<div class="product_similaire">

	<div class="spacer"></div>
	<a href="{hrefcat}" class="voir-categorie">Voir l'ensemble de la gamme</a>
	<div class="spacer"></div>
	
	<a href="{start_url}/catalog/promotions.php" class="voir-categorie2">Voir l'ensemble<br/> des promotions</a>
	<a href="{start_url}/catalog/destock.php" class="voir-categorie3">Voir l'ensemble<br/> du destockage</a>
	<div class="spacer"></div>
		
</div>
<div class="spacer"></div>

<!-- Specifications technique -->	
<div class="floatleft">
	<img src="{start_url}/images/front_office/global/top_gamme_star.jpg" width="972" height="3" class="top_star floatleft" alt="" />
	<div class="spacer"></div>
	<h1 class="title_specification_big"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="&#0155;" /> 
	Caractéristiques générales : {productName:h}</h1>
	<div class="specification_big">
		<flexy:include src="/product/detail_{IdMaskDetail}.tpl"></flexy:include>
	</div>
	<div class="spacer"></div>
	<img src="{start_url}/images/front_office/global/footer_gamme_star.jpg" width="972" height="2" class="footer_star floatleft" alt="" />
	<div class="spacer"></div>
</div>
<div class="spacer"></div>
<ul class="options">
<li>
	<!-- PDF -->			
	<a onclick="" href="#" rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_pdf.png" style="margin:0;border:none;"></a>
</li>

<li>
	<!-- Imprimer -->			
	<a  onclick="window.open(this.href); return false;" href="{product.getPDFURL()}" rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_imprimer.png" style="margin:0;border:none;"></a>
</li>
<li>		
	<!-- Envoyer par email -->					
	<a  style="cursor:pointer;" href="{baseURL}/catalog/send_friend.php?IdProduct={product.getId()}" onclick="window.open(this.href,'mailto','width=555,height=250,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=1'); return false;" rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_sendemail.png" style="margin:0;border:none;"></a>
</li>
{if:authentified}
	<li>	
		<!-- liste personnelle -->			
		<a  href="{baseURL}/catalog/buyer_func.php?IdProduct?{product.getId()}" onclick="javascript:alert('Le produit a été ajouté à votre catalogue personnel.'); " rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_addtolist.png" style="margin:0;border:none;"></a>			
	</li>
{end:}
</ul>
<!-- Description complète -->
<a name="millieu_page"></a>
<img src="{start_url}/images/front_office/global/top_gamme_star.jpg" width="972" height="3" class="top_star floatleft" alt="" />
<div class="spacer"></div>
<h1 class="title_star"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="›" /> 
Description complète : {productName:h}</h1>
<div class="star">
	<div class="description">{description:h}</div>
</div>
<div class="spacer"></div>
<img src="{start_url}/images/front_office/global/footer_gamme_star.jpg" width="972" height="2" class="footer_star floatleft" alt="" />
<div class="spacer"></div>

<!-- Options, Accessoires -->
<a name="accessoires"></a>
{if:hasassociates}
	<img src="{start_url}/images/front_office/global/top_gamme_star.jpg" width="972" height="3" class="top_star floatleft" alt="" />
	<div class="spacer"></div>
	<h1 class="title_star"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="›" /> 
	Options, Accessoires : {productName:h}</h1>
	<div class="star">
		<flexy:include src="/product/associat_product.tpl"></flexy:include>
	</div>
	<div class="spacer"></div>
	<img src="{start_url}/images/front_office/global/footer_gamme_star.jpg" width="972" height="2" class="footer_star floatleft" alt="" />
	<div class="spacer"></div>
{end:}

<!-- Plan technique -->
{if:imageDocURL}
	<img src="{start_url}/images/front_office/global/top_gamme_star.jpg" width="972" height="3" class="top_star floatleft" alt="" />
	<div class="spacer"></div>
	<h1 class="title_star"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="›" /> 
	Plan technique : {productName:h}</h1>
	<div class="star">
		<img src="{imageDocURL}" alt="plan" />
	</div>
	<div class="spacer"></div>
	<img src="{start_url}/images/front_office/global/footer_gamme_star.jpg" width="972" height="2" class="footer_star floatleft" alt="" />
	<div class="spacer"></div>
{end:}

<!-- Module de commentaire -->
<img src="{start_url}/images/front_office/global/top_gamme_star.jpg" width="972" height="3" class="top_star floatleft" alt=""/>
<div class="spacer"></div>
<h1 class="title_star"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="›" /> 
Avis consommateur : {productName:h}</h1>
<div class="star" id="comentaires">
	{if:commentAdded}
		<h2 style="font-size:12px;">&#0155; Merci de nous avoir donné votre avis.</h2>
	{end:}
	{if:!commentAvailable}
		{if:authentified}
			<h2 style="font-size:12px;">&#0155; Vous avez déjà donné votre avis sur ce produit.</h2>
		{else:}
			<h2 style="font-size:12px;">&#0155; Veuillez vous identifier afin de poster un commentaire.</h2>
		{end:}
	{end:}
	
	{if:hascomments} 
		<FLEXY:TOJAVASCRIPT avgNote="commentsAvg">
		
		<p>
			<strong>&#0155; Moyenne des notes attribuées à ce produit : </strong>
			<span id='myAvg'>
					<select name="avgGrade" disabled="disabled">
						<option value="0">0 étoile</option>
						<option value="1">1 étoile</option>
						<option value="2">2 étoiles</option>
						<option value="3">3 étoiles</option>
						<option value="4">4 étoiles</option>
					</select>
			</span>
		</p>
		<br />
		
		{foreach:comments,key,comment}
			
			<p>
				<strong>{comment[firstname]} {comment[lastname]}, le {comment[date]} </strong>
				<span id='commentNote_{key}_{comment[note_buyer]}' class="noters">

					<select name="buyerNote_{key}" disabled="disabled">
						<option value="0">0 étoile</option>
						<option value="1">1 étoile</option>
						<option value="2">2 étoiles</option>
						<option value="3">3 étoiles</option>
						<option value="4">4 étoiles</option>
					</select>
					
				</span>
				<br/>
				{comment[product_comment]}
			</p>

		{end:}
		
		<script type="text/javascript">		
		<!--		
			$('span').filter('.noters').each(function(){
				$("#"+$(this).attr("id")).stars({ 
				     inputType: "select",
				     cancelShow: false
				});
				
				noteTab = $(this).attr("id").split('_');
				note = noteTab[noteTab.length-1];
				
				$("#"+$(this).attr("id")).stars("select", Math.round(note));
			});
		//-->
		</script>
		
	{else:}	
		<p>Il n'y a pas encore de commentaire sur ce produit</p>
	{end:} 
	<div class="spacer"></div>
	

	{if:commentAvailable}
		<p><strong>&#0155; Ajouter un commentaire :</strong></p>
		<form name ="commentsForm" action="{actionCommentsForm}" method="post"  onSubmit="javascript: return validform_comments();">
			<textarea name="myComment"></textarea>
			<p style="width:45px; float:left; margin-top:7px;">	
				Note : <span id='myGrade' style="float:left;"></span>
	
				<div id="mySelects" style="float:left; margin-top:5px;">
					<select name="myGrade">
						<option value="0">0 étoile</option>
						<option value="1">1 étoile</option>
						<option value="2">2 étoiles</option>
						<option value="3">3 étoiles</option>
						<option value="4">4 étoiles</option>
					</select>
				</div>	
			</p>
			
			<input type="submit" value="Poster mon message" class="validComments" />
		</form>

		<script type="text/javascript">
			$("#mySelects").stars({	inputType: "select" , cancelShow: false	});
			$("#mySelects").stars("select", 2);
					
			$("#myAvg").stars({ 
				 inputType: "select",
				 cancelShow: false
			});
			$("#myAvg").stars("select", Math.round(avgNote));
			
			$("#commentNote[]").stars({ 
				 inputType: "select",
				 cancelShow: false
			});
				
		</script>
		
	{end:}
	<div class="spacer"></div>

</div>

<div class="spacer"></div>
<img src="{start_url}/images/front_office/global/footer_gamme_star.jpg" width="972" height="2" class="footer_star floatleft" alt="" />
<div class="spacer"></div> 

<!-- Derniere Consultations
<flexy:include src="lastConsultationProd.tpl"></flexy:include> -->




