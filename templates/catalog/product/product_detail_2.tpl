	<!-- détail produit pour les vêtements -->
	<link type="text/css" rel="stylesheet" href="{baseURL}/templates/catalog/css/products.css" />
	<!--<link type="text/css" rel="stylesheet" href="{baseURL}/templates/catalog/css/product4-new.css" />-->
	
	<tr>
		<td valign="top">
		{if:iduser}<span style="color:red;font-weight: bold;">/product/product_detail_2.tpl</span>{end:}
			<!-- Début Header fiche produit -->
			<flexy:include src="/product/product_detail_header.tpl"></flexy:include>
			<!-- Fin Header fiche produit -->
			<br />
		</td>
		
		<td valign="top">
			<!--<div id="contenerSpe" style="width: 745px;">-->
			<!-- On créer 3 div, l'un pour les Spé Géné, Option et acessoires, Plan Tech 
			Selon l'onglet sur lequel on clique, on masque ou affiche une DIV -->		
			<div class="detailcolD">
		    	<div class="prod-title">
		        	<h4>{product.get(#name_1#):h}</h4>
			        <small>{product.getReferenceCount()} référence(s) disponible(s)</small>
			        <a href="#AnchorTabRef">voir</a>
		        </div>	
	            <div class="infos_prix">
                    {if:product.getLowerPriceET()}
                        <span class="prix">
                            à partir de
                            {if:cheapestArticle.getDiscountRate()}
                                <del>{cheapestArticle.getLowerPriceET():h}</del>
                            {end:}
                             <big>{product.getLowerPriceET():h} HT</big>
                        </span><br/> 		
                    {end:}
				</div>
                <br style="clear:both;">
                
                <div style="width: 100%; margin: 10px 0 20px 0; text-align: right;">
                    <!-- pictos certifications -->
                    <img src="{picto.image}" alt="{picto.name}" title="{picto.name}" class="pictos primary" flexy:foreach="product.getPrimaryPictogram(),picto" />
                </div>
		
				<div id="descprod 2">
					{if:product.getTruncatedDescription(#350#)}
						{product.getTruncatedDescription(#350#):h}
					{else:}
						{product.get(#description_1#):h}
					{end:}
				</div>
                                
				<div id="detail8" style="float:left;">
					<flexy:include src="/product/detail_8.tpl"></flexy:include>
				</div>
				<br style="clear:both;">
			</div>		
		</td>
	</tr>
</table>

<br/>
<br/>
<br/>

<div id="ProductOneDetailDiv" style="clear: both;">	

  	<flexy:include src="/product/detail_2.tpl"></flexy:include>
	
	<div id="optionsAccessoires" >
		<div id="cornerLeft"></div>
		<center>		
		</center>
	</div>	
	<div id="planTechniques" >
		<div id="cornerLeft"></div>
		{if:product.getDataSheetURI()}
			<img src="{product.getDataSheetURI()}" alt="plan" />
		{end:}
	</div>
	<div id="bot"></div>
	<br /> <br />
	
</div>