﻿<flexy:include src="header.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->
	<div id="center" class="page home">  
	
<b><b>{if:iduser}<span style="color:red;font-weight: bold;">/index.tpl</span>{end:}
	
       
<script type="text/javascript">
            $('#slider').layerSlider({
				skin : 'lsskin',
				skinsPath : 'templates/catalog/images/home/',
				twoWaySlideshow : false,
				thumbnailNavigation : 'disabled',
                navPrevNext : true,
                navStartStop : false,
                navButtons : true,
				responsiveUnder : 960
            });
			
</script>
						<br>
         <div class="products">
		 Espace produits en promotions
                    	<ul class="list_product">
                         {if:!getPromotions(1)}
                            <!--pas de promotion-->
                            <li itemtype="http://schema.org/Product" itemscope="" class="homelist" flexy:foreach="getFocusProducts(6,1),focusProduct">
                            <a itemprop="url" style="left: -29px;" href="{focusProduct.getURL()}" class="btn-det" title="Ajouter au panier"></a>
                                <a itemprop="url" href="{focusProduct.getURL()}">
                                        <span class="ruban ccoeur"></span>
                                        <img itemprop="image" width="150" height="150" alt="{focusProduct.get(#name_1#):h}" src="{focusProduct.getImageURI(#150#)}">
                                        <span itemprop="name" class="h2">{focusProduct.get(#name_1#):h}</span>
                                        <span class="price">
                                            <small>&agrave; partir de</small><br>
                                            {if:focusProduct.getLowerPriceET()}
                                                <span class="strong">{focusProduct.getLowerPriceET():h} <small>HT</small></span>
                                            {end:}
                                        </span>
                                    </a>
                            </li> 
                        {else:}
                            <li class="homelist" flexy:foreach="getPromotions(3),promoArticle" itemtype="http://schema.org/Product" itemscope="">
                            <a itemprop="url" class="btn-det" style="left: -29px;" href="{promoArticle.getURL()}" title="Ajouter au panier">
                            	Ajout au panier
                            </a>
                            <a itemprop="url" href="{promoArticle.getURL()}">
                            	<span class="ruban promo"><span>-{promoArticle.getDiscountRate():h}</span></span>
                                <img src="{promoArticle.getImageURI(#150#)}" width="150" height="150"  alt="{promoArticle.get(#summary_1#):h}" title="{promoArticle.get(#summary_1#)}" itemprop="image">
                                <span itemprop="name" class="h2">{promoArticle.get(#summary_1#):h}</span>
                                <span class="price">
                                            <small>&agrave; partir de</small><br>
                                            {if:promoArticle.getDiscountPriceET()}
                                                {if:promoArticle.getDiscountRate()}
                                                    <del>{promoArticle.getCeilingPriceET():h} <small>HT</small></del><br>
                                                 {end:}
                                                    <span class="strong">{promoArticle.getDiscountPriceET():h}<small> HT</small></span>
                                            {end:}
                                        </span>
                                
                                
                            </a>                               
                            </li>
                            <li itemtype="http://schema.org/Product" itemscope="" class="homelist" flexy:foreach="getFocusProducts(3,1),focusProduct">
                            	<a itemprop="url" style="left: -29px;" href="{focusProduct.getURL()}" class="btn-det" title="Ajouter au panier"><img width="60" alt="Ajouter au panier" height="60" src="/templates/catalog/images/list-btn-detail.png"></a>
                                <a itemprop="url" href="{focusProduct.getURL()}">
	                                    <span class="ruban ccoeur"></span>
                                        <img itemprop="image" width="150" height="150" alt="{focusProduct.get(#name_1#):h}" src="{focusProduct.getImageURI(#150#)}">
                                        <span itemprop="name" class="h2">{focusProduct.get(#name_1#):h}</span>
                                        <span class="price">
                                            <small>&agrave; partir de</small><br>
                                            {if:focusProduct.getLowerPriceET()}
                                                <span class="strong">{focusProduct.getLowerPriceET():h} <small>HT</small></span>
                                        	{end:}
                                        </span>
                                    </a>
                            </li>
                        {end:} 
               			 </ul>
                <div class="clear"></div>
            <script type="text/javascript">
				$(document).ready(function(){
					$(".rate").each(function(index) {
						var discountFloat = $(this).text().trim();
						var discountInt = discountFloat.replace(/,[0-9]*\s%/g, "%");
						$(this).html(discountInt);
					});	
				});
							$('ul.listprods li').mouseover(function(e) {
								$(this).find('a.btn-det').stop().animate({left:43}, 200, "easeInOutCirc");
							})
							$('ul.listprods li').mouseout(function(e) {
								$(this).find('a.btn-det').stop().animate({left:-29}, 200, "easeInOutCirc");
							})
			</script>
        </div>
        

                       
        <div id="arbo">
               
                {getListCateg2():h}
      </div>
           </div>
        </div><!-- Fin main -->
        </div><!-- Fin content -->
		
	<!--====================================== Fin du contenu ======================================-->
	
	{if:iduser}<span style="color:red;font-weight: bold;">/foot.tpl</span>{end:}<br>
<flexy:include src="foot.tpl"></flexy:include> 