<!DOCTYPE HTML>
<html lang="fr">
<head>
	<meta charset=utf-8" />
<title>V&ecirc;tements de travail, chaussures de s&eacute;curit&eacute;, &eacute;quipement de protection individuelle - Abisco</title>
<meta content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1" name="viewport" />
	<meta name="{name}" content="{content:h}" flexy:foreach="head.getMetaTags(),name,content" />
	<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/general.css" />
    <style type="text/css">body{scrollbar-face-color:#efefef;scrollbar-shadow-color:#efefef;scrollbar-highlight-color:#efefef;scrollbar-3dlight-color:#c7c7c7;scrollbar-darkshadow-color:#c7c7c7;scrollbar-track-color:#c7c7c7;scrollbar-arrow-color:#7b7b7b;}
div#main div.module{border:1px solid #ccc;}div#contents div#main div#frameG,div#contents div#main div#frameD{display:none;}div#main div.module h4{text-indent:0;font-size:1.2em;color:#f39903;}div#header ul#quickmenu,div#header ul#assist,div#header ul#flags,div#header ul#institmenu,div#header h2{position:absolute;margin:30px 0 0 50px;color:#f39903;font-size:1.5em;display:block;text-indent:0;background:none;}div.page .data,div.page .compare{width:auto;overflow:visible;}
.tdBlanc{background-color:#FFFFFF;width:5;}.tdAntracite{background-color:#8299aa;width:1;}.text{color:#999999;font-face:Arial, Helvetica, sans-serif;font-weight:bold;size:2;}</style>
     <link rel="stylesheet" type="text/css" href="{start_url}/templates/catalog/css/slider.css" />
     <link rel="stylesheet" type="text/css" href="{start_url}/templates/catalog/css/carousel/carousel.css" />
	<script type="text/javascript" src="{start_url}/js/jquery/jquery-1.6.2.min.js" ></script>
	<script type="text/javascript" src="{start_url}/js/js_search_filter.js" async></script>
	{if:head}
	<script type="text/javascript" src="{script}" flexy:foreach="head.getScripts(),script" async></script>
	<link type="text/css" rel="stylesheet" href="{css}" flexy:foreach="head.getCSS(),css" />
    {end:}
<link rel="stylesheet" type="text/css" media="screen" href="{start_url}/templates/catalog/css/superfish.css"  />
<link rel="stylesheet" type="text/css" media="screen" href="{start_url}/templates/catalog/css/superfish-navbar.css" />
	<!--[if lt IE 7.]>
	<script defer type="text/javascript" src="{start_url}/js/pngfix.js"></script>
	<link href="{start_url}/templates/catalog/css/ltie7.css" rel="stylesheet" type="text/css" />
	<![endif]-->
    <flexy:toJavascript baseURL="baseURL"></flexy:toJavascript>
	
	<FLEXY:TOJAVASCRIPT globalstarturl="start_url">
    
	<script type="text/javascript" src="{start_url}/templates/catalog/js/newfile/jquery-1.js"></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/newfile/jquery-ui-1.js" ></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/newfile/jquery_003.js" ></script>
	<script type="text/javascript" src="{start_url}/templates/catalog/js/newfile/jquery_002.js"></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/newfile/greensock.js"defer></script>   
    
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.easing.min.js" defer></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-transit-modified.js"defer ></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-ui-1.10.3.custom.pack.js" defer></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/layerslider.transitions.js" ></script>
	<script type="text/javascript" src="{start_url}/templates/catalog/js/layerslider.kreaturamedia.jquery.js" ></script>
    <script type="text/javascript">
		function forgetPassword(){
			$('#bg-ui-clone').show();
			$('#forgetPassword').show();
		}
		function sendPassword(Email,divResponse){data="&Email="+Email;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(XMLHttpRequest,textStatus,errorThrown){alert("Impossible de récupérer l'email");},success:function(responseText){$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+responseText+"</p>");}});}
		function closeBTN(){
			$('#bg-ui-clone').hide();
			$('#forgetPassword').hide();
		}
	</script>

<script type="text/javascript" src="{start_url}/js/superfish_menu/hoverIntent.js" defer></script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/superfish.js" defer></script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/supersubs.js" defer></script>	
 <!-- Fin Script Code tracking -->
<script type="text/javascript">
(function() 
	{
		var s = document.createElement('script');
		s.type = 'text/javascript'; s.async = true;
		s.src = ('https:' == document.location.protocol ? 'https://' : 'http://')+'bp-1c51.kxcdn.com/prj/AS-2314166.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	}
)();
</script> 


</head>
<body>

<div style="width: auto; min-height: 0px; height: auto; top: 30%; left: 35%; position: fixed; z-index: 1002; background: none repeat scroll 0px 0px rgb(255, 255, 255); display: none; padding: 5px; border-radius: 5px;" id="forgetPassword" class="">
<p class="txt-oubl" style="float:left">Mot de passe oublié?</p>
<p class="txt-clos" style="float:right; cursor: pointer;"><a onclick="closeBTN();">close</a></p>
			<div style="clear:both" id="EmailResponse">
				<p class="blocTextNormal">Saisissez votre adresse e-mail pour récupérer vos identifiants</p>
				<input type="text" style="width:285px;" class="loginFieldMedium input_email" value="" name="Email" id="Email" />				
				<input type="submit" onclick="sendPassword( $('#Email').val(), '#EmailResponse' );" style="float:left;" class="submitField" value="Envoyer" name="Envoyer" />			
			</div>
		</div>
	<div id="mainContainer" class="{pagename}">
	{if:iduser}<span style="color:red;font-weight: bold;">/head/header_accueil.tpl</span>{end:}
		<div id="contents">
		
		<b><b>
		
			<div id="header" style="">
	        LOGO
			<div id="menutop">
            	<div id="loginpanel">
                	<p>Mon <strong>compte</strong></p>
                    {if:customer}
                    <style type="text/css">
						#header #loginpanel h3 {
							margin: 0;
							text-align: center;
							width: 100%;
						}
						#loginpanel > div {
							clear: both;
							float: left;
							margin: 10px 0;
							text-align: center;
							width: 100%;
						}
						#header #loginpanel ul {
							clear: both;
							float: left;
							text-align: center;
							width: 100%;
							margin: 0;
						}
					</style>   
                 <div>
                 	<span class="welcome" style="color: #fff; font-weight: bold;">Bienvenue</span>					
                	<span class="userlogin" style="color: #fff;">{customer.get(#firstname#)} {customer.get(#lastname#)}</span>  
                 </div>
                <ul>					
                    <li class="left"><a href="{start_url}/catalog/account.php"  rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"><img src="{baseURL}/templates/catalog/images/profil.jpg" /> Mon compte</a></li>					
                    <li class="left"><a href="{start_url}/www/logoff.php"  rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"><img src="{baseURL}/templates/catalog/images/deco.jpg" /> [Déconnexion]</a></li>
                </ul>
            {else:}
                    <form id="login" action="/catalog/account.php" target="_top" name="Form_logon" method="post">
                        <label for="emaillogin">Adresse email</label>
                        <input type="text" placeholder="adresse email" class="floatL" name="Login" id="emaillogin" style="height:30px" data-validate="rlogin" />
                        <label for="passwlogin">Mot de passe</label>
                        <input type="password" placeholder="mot de passe" class="floatL" name="Password" id="passwlogin" style="height:30px" />
                        <input type="submit" value="OK" />
                    </form>
                    <ul>
                    	<li><a href="#" onclick="forgetPassword(); return false;">Mot de passe oubli&eacute; ?</a></li>
                    	<li><a href="/catalog/register.php">Cr&eacute;er son compte client</a></li>
					</ul>
              {end:}
                    <a onclick="hidelogin();" title="Fermer" class="close" href="javascript:;"></a>
                </div>
                <ul>
                    <li><a href="/catalog/contact.php">Contact</a></li>
                    <li><a id="mylogin" href="javascript:;">Mon compte</a></li>
                    <li><a href="{start_url}/catalog/basket.php">Mon panier <small class="BasketItemCount">
					{if:basket.getItemCount()}
                                        ({basket.getItemCount()})
                    {else:}
                    	0
                    {end:}
					 </small></a></li>
                </ul>
              
			</div>
            <form id="SearchForm" action="/catalog/search.php" method="post">
                <label for="search-text">Rechercher...</label>
                <input type="text" placeholder="Rechercher..." value="{search_value}" name="search_product" class="floatL" id="search-text" />
		 {head.GetBrand():h}
		{head.GetJobs():h}		
                <input type="hidden" value="" name="idcategoryname" />
                <input type="hidden" value="AND" name="type" />
                <input type="hidden" value="1" name="page" />
                <input type="hidden" value="20" name="resultPerPage" />
                <input type="submit" title="Rechercher" value="Rechercher" />
            </form>
            <span class="clear"></span>
		</div>
			 <flexy:include src="menu/menu_alt.tpl"></flexy:include>
			<div id="main">        
