<flexy:include src="/head/header.tpl"></flexy:include>
<flexy:include src="menu/menu.tpl"></flexy:include>
<!-- Google Code for Commande directe Conversion Page -->

<script type="text/javascript">

/* <![CDATA[ */
var google_conversion_id = 1038552525;
var google_conversion_language = "fr";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "GvTHCJHBjgEQzZuc7wM";
var google_conversion_value = 0;
if (1.0) {
   google_conversion_value = 1.0;
}
/* ]]> */
</script>

<script type="text/javascript"  
src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="http://www.googleadservices.com/pagead/conversion/1038552525/?value=1.0&amp;label=GvTHCJHBjgEQzZuc7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript><!--

<flexy:include src="b_left.tpl"></flexy:include>-->
<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/basket.css" />


<!--====================================== Contenu ======================================-->
<div id="center" class="page finish">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/finish.tpl</span>{end:}
	<ul class="basket_fil">
    <li class="un"><a href="/catalog/basket.php"><span>Panier commande</span></a></li>
    <li class="deux"><span>Authentification</span></li>
    <li class="trois"><a href="/catalog/order.php"><span>Livraison et facturation</span></a></li>
    <li class="quatre actif"><span>Confirmation</span></li>
	</ul>
    <h1 class="title_commande" style="width:96%">Votre commande a bien &eacute;t&eacute; enregistr&eacute;e</h1>
    <p>Merci pour votre commande dont le numéro est <span class="ndevis_confirm"><b>{idorder}</b></span> et de la confiance que vous accordez à notre soci&eacute;t&eacute;.
    <br /><br />
    Le service commercial d'Abisco est actuellement en train de traiter votre commande et vous enverra un e-mail de confirmation d&eacute;finitive sous peu. </p>
    <div class="spacer"></div><br /> 
      
    <h1 class="title_commande" style="width:96%">A partir de votre compte, vous avez acc&egrave;s :</h1>
    <p style="clear:both">- Au suivi et &agrave; l'historique de vos commandes et devis
    <br /><br />- A la commande express
    <br /><br />- A toutes les offres exclusivement r&eacute;serv&eacute;es aux membres de Abisco
    </p>
    <div class="spacer"></div><br />  	 
    
    <h1 class="title_commande" style="width:96%">R&eacute;capitulatif de votre commande :</h1>
    <flexy:include src="articles_read.tpl"></flexy:include>		 			
        
    <flexy:include src="basket_amounts.tpl"></flexy:include>  
    <div class="spacer"></div><br />  	   	
            
    <!-- <flexy:include src="addresses_read.tpl"></flexy:include> -->
    <p>Mode de paiement utilis&eacute; : <b>{mode_payment_used:h}</b></p>	
    <!--<textarea>{mode_payment_used}</textarea>-->			
    
    <p>Pour toutes informations compl&eacute;mentaires, </span><span class="infocomp_orange">n'h&eacute;sitez pas &agrave; nous contacter</span></p>
	<div class="spacer"></div>
    <p>
		<a href="{start_url}/" class="btn_continue">Retour vers le site</a>
		<a href="{start_url}/catalog/account.php" class="btn_continue">Mon compte</a>
	</p>
    <!-- <a href="javascript:window.print();"><img src="{start_url}/templates/catalog/images/btn_imprimer.gif" border="0"/></a>
    <a href="{start_url}/index.php"><img src="{start_url}/templates/catalog/images/btn_poursuivre_droit.gif" border="0" /></a> -->
</div>
<!--<flexy:include src="b_right.tpl"></flexy:include>-->
</div>
</div>

<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>       


