<flexy:include src="/head/header.tpl"></flexy:include>

<!--<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/basket.css" />-->
<!--<link rel="stylesheet" href="{start_url}/templates/catalog/css/bootstrap.min.css" />-->

<!--====================================== Contenu ======================================-->

<div id="center" class="page">
	<script type='text/javascript'>
		jQuery(document).ready(function () {
			// validate the comment form when it is submitted
			jQuery("#loginpanel form").validate({
				onSubmit: true,
				eachInvalidField: function () {
					alert('Merci de renseigner une adresse email valide');
				}
			});
			jQuery('#RegisterForm').validate({
				onChange: true,
				onKeyup: true,
				onBlur: true,
				onSubmit: true,
				eachValidField: function () {
					if (jQuery(this).attr('name') != 'newsletter' && jQuery(this).attr('name') != 'title') {
						jQuery(this).removeClass('not_valid');
						jQuery(this).addClass('valid');
						if (jQuery(this).siblings('span.check').length == 0)
							jQuery(this).after('<span class="check"></span>');
						if (jQuery(this).siblings('span.not_valid').length != 0)
							jQuery(this).siblings('span.not_valid').fadeOut('slow').remove();
					}
				},
				eachInvalidField: function () {
					if (jQuery(this).attr('name') == 'profile') {
						if (jQuery(this).find('option:selected').val() == '-1') {
							jQuery('#registerblock').html(
								'<h1>Nous ne pouvons traiter votre demande d\'inscription </h1><p>&nbsp;</p><p>Madame,Monsieur,</p><p>&nbsp;</p><p>Nos catalogues et nos offres internet s\'adressent exclusivement aux professionnels conform&eacute;ment &agrave; nos <a href="/cgv.php">Conditions G&eacute;n&eacute;rales de ventes</a>.</p><p>&nbsp;</p><p>&nbsp;</p><p>Cordialement</p><p>Le service Client Abisco.</p><br /><br /><a href="/">&#x3C; Retour &agrave; la page d\'accueil</a>'
							);
						} else
							jQuery(this).removeClass('valid').addClass('not_valid');
					} else {
						if (jQuery(this).attr('name') == 'email') {
							if (jQuery(this).siblings('span.not_valid').length == 0)
								jQuery(this).after('<span class="not_valid">Merci de renseigner une adresse mail valide</span>');
						}
						if (jQuery(this).attr('name') == 'pwd') {
							if (jQuery(this).siblings('span.not_valid').length == 0)
								jQuery(this).after(
									'<span class="not_valid">Le champ Mot de passe doit comporter au minimum 6 caract&egrave;res</span>');
						}
						jQuery(this).removeClass('valid').addClass('not_valid');
						jQuery(this).siblings('span.valid').remove();
					}
					if (jQuery(this).siblings('span.check').length == 0)
						jQuery(this).after('<span class="check"></span>');
				}
			});
			jQuery.validateExtend({
				email: {
					required: true,
					pattern: /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/,
					conditional: function (value) {
						var verif =
							/^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
						return (verif.test(value));
					}
				},
				pwd: {
					required: true,
					conditional: function (value) {
						return value.length >= 6;
					}
				},
				profile: {
					required: true,
					conditional: function (value) {
						return value == '1' || value == 2 || value == 3;
					}
				},
				number: {
					required: true,
					pattern: /^[0-9]+$/,
					conditional: function (value) {
						return value.length < 21;
					}
				}
			});
			jQuery('input[name="pwd"]').focusout(function () {
				jQuery('input[name="pwd_confirm"]').val(jQuery(this).val());
			});
			jQuery('#loginblock form').validate({
				onKeyup: true,
				onBlur: true,
				onSubmit: true,
				eachValidField: function () {
					jQuery(this).removeClass('not_valid').addClass('valid');
					if (jQuery(this).siblings('span.not_valid').length != 0)
						jQuery(this).siblings('span.not_valid').fadeOut('slow').remove();
				},
				eachInvalidField: function () {
					if (jQuery(this).attr('name') == 'Login') {
						if (jQuery(this).siblings('span.not_valid').length == 0)
							jQuery(this).after('<span class="not_valid">Merci de renseigner une adresse mail valide</span>');
					}
					jQuery(this).removeClass('valid').addClass('not_valid');
				},
			});
			jQuery.validateExtend({
				login: {
					required: true,
					pattern: /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/,
					conditional: function (value) {
						var verif =
							/^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
						return (verif.test(value));
					}
				}
			});
		});
	</script>
	{if:customer}
	<flexy:include src="registerok.tpl"></flexy:include>
	{else:}

	<script type='text/javascript'>
		<!--

		// Test des champs du formulaire d'enregistrement
		/*function verifForm (){
         
        var emailAddr = RegisterForm.elements[ 'email' ].value;
        
        if(emailAddr == ''){
            alert('Le champ Email est vide !!!');
            return false;
        }else{
            if(!isEmail(emailAddr)){
                alert('Le champ Email n\'est pas valide !!!');
                return false;
            }
        }		
            
      
        if( RegisterForm.elements[ 'pwd' ].value == "") {
            
            alert('Le champ Mot de passe est vide !!!');
            return false;
            
        }else if( RegisterForm.elements[ 'pwd' ].value.length < 6) {
                
                alert('Le champ Mot de passe doit comporter au minimum 6 caract\350res !!!');
                return false;
      
        }
    
        if( RegisterForm.elements[ 'pwd_confirm' ].value == "") {
        
            alert('Le champ confirmation mot de passe est vide !!!');
            return false;
            
        }else if( RegisterForm.elements[ 'pwd_confirm' ].value != RegisterForm.pwd.value) {
        
            alert('Le champ confirmation de mot de passe doit \352tre identique au champ Mot de passe !!!');
            return false;
                
        }
        if(this.RegisterForm.elements['title'].options[this.RegisterForm.elements['title'].selectedIndex].value == ''){
            
            alert('Le champ Titre est vide !!!');
            return false;
        
        } 
        
        if( RegisterForm.elements[ 'lastname' ].value == '') {
        
            alert('Le champ Nom ?> est vide !!!');
            return false;
            
        }
          
        
              
        if( RegisterForm.elements[ 'phonenumber' ].value == '') {
        
            alert('Le champ T\351l\351phone est vide !!!');
            return false;
            
        }
          
                 
        if( RegisterForm.elements[ 'adress' ].value == '') {
          
            alert('Le champ Adresse est vide !!!');
            return false;
            
        }
        
        if( RegisterForm.elements[ 'zipcode' ].value == "") {
          
            alert('Le champ Code Postal est vide !!!');
            return false;
            
        }
        
        if( RegisterForm.elements[ 'city' ].value == "") {
          
            alert('Le champ Ville est vide !!!');
            return false;
            
        }
        
                  
        return true;
         
	}*/

		function isEmail(strSaisie) {
			var verif =
				/^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
			return (verif.test(strSaisie));
		}

		//-->
	</script>
	{if:orderInProgress}
	{if:iduser}<span style="color:red;font-weight: bold;">/basket/register.tpl</span>{end:}
	<ul class="basket_fil">
		<li class="un"><a href="/catalog/basket.php"><span>Panier commande</span></a></li>
		<li class="deux actif"><span>Authentification</span></li>
		<li class="trois"><span>Livraison et facturation</span></li>
		<li class="quatre"><span>Confirmation</span></li>
	</ul>
	<div class="spacer"></div>
	{end:}
	{if:loginfail}
	{if:iduser}<span style="color:red;font-weight: bold;">/basket/register.tpl</span>{end:}
	<div id="loginfail" style="text-align:center;color:#f00;margin-bottom:15px">Votre adresse email ou mot de passe est
		incorrect.</div>{end:}
	{if:errorInscription}<div id="loginfail" style="text-align:center;color:#f00;margin-bottom:15px">{errorInscription:h}</div>{end:}

	<div class="register-container">
		<div class="register_bloc no-border-r">
			<input type="radio" onclick="$('#registerblock').slideDown('slow'); $('#loginblock').slideUp('fast');" value="0"
			 name="action_account"> <strong>Nouveau client ?</strong>&nbsp;Cr&eacute;ez votre compte
		</div>
		<div class="register_bloc">
			<input type="radio" checked="checked" onclick="$('#registerblock').slideUp('fast'); $('#loginblock').slideDown('slow');"
			 value="1" name="action_account"> <strong>D&eacute;j&agrave; client ?</strong>&nbsp;Connectez-vous
		</div>
	</div>

	<div id="loginblock" class="loginblock">
		<form method="post" name="Form_logon" target="_top" action="/catalog/account.php" id="logininregister">
			<label for="emailrlogin">Adresse email <span class="ast_orange">*</span></label>
			<input type="text" style="height:30px" id="emailrlogin" name="Login" class="floatL " placeholder="adresse email"
			 data-required="true" data-validate="login" />
			<label for="passrwlogin">Mot de passe <span class="ast_orange">*</span></label>
			<input type="password" style="height:30px" id="passrwlogin" name="Password" class="floatL" placeholder="mot de passe"
			 data-required="true" />
			<input class="btn-med" type="submit" value="S'identifier" />
			<a href="#" onclick="forgetPassword(); return false;" style="display: inline-block; font-size: 13px;">Mot de passe
				oublié ?</a>
			{if:orderInProgress}
			<input type="hidden" name="redirectTo" id="rredirection" value="order.php" flexy:ignore />
			{else:}
			<input type="hidden" name="redirectTo" id="rredirection" value="account.php" flexy:ignore />
			{end:}
		</form>
	</div>
	<div class="spacer"></div>
	<div id="registerblock" style="display: none;">
		<form action="{start_url}/catalog/register.php" id="RegisterForm" method="post">
			<!--<input type="hidden" name="FromScript" value="{ScriptName}" />-->
			{if:orderInProgress}
			<input type="hidden" name="redirectTo" id="redirection" value="order.php" flexy:ignore />
			{else:}
			<input type="hidden" name="redirectTo" id="redirection" value="account.php" flexy:ignore />
			{end:}
			<div class="spacer"></div>

			{if:errorMsgNotEmpty}
			<p class="errorMsg">{errorMsg:h}</p>
			{end:}

			<div class="row">
				
				<div class="form_left col-md-6">
	
					<h3>Mes informations de connexion</h3>
					<div>
						<label>E-mail <span class="ast_orange">*</span></label>
						<input type="text" name="email" value="{userInfoEmail:h}" class="AuthInput" data-required="true" size="40"
						 data-validate="email" />
					</div>
					<div class="spacer"></div>
	
	
					<div>
						<label>Mot de passe <span class="ast_orange">*</span></label>
						<input type="password" name="pwd" value="{userInfoPwd:h}" class="AuthInput" maxlength="20" size="40" data-required="true"
						 data-validate="pwd" />
					</div>
					<div class="spacer"></div>
	
					<!--<label>Confirmation mot de passe <span class="ast_orange">*</span></label>-->
					<input type="password" name="pwd_confirm" value="{userInfoPwdConfirm:h}" class="AuthInput" maxlength="20" size="40"
					 style="display:none" />
					<!--<div class="spacer"></div>-->
	
					<div class="form-footer">
						<p class="txt_oblg">Tous les champs marqu&eacute;s d'un <span class="ast_orange">*</span> sont &agrave; remplir
							obligatoirement</p>
					</div>
				</div>
	
				<div class="form_right col-md-6">
					<h3>Mes coordonn&eacute;es</h3>
	
					<div class="form_line"><label>Profil <span class="ast_orange">*</span></label>
						<select type="text" name="profile" data-required="true" data-validate="profile">
							<option value="0"></option>
							<option value="1">Professionnel</option>
							<option value="2">Association</option>
							<option value="3">Administration</option>
							<option value="-1">Particulier</option>
						</select>
					</div>
					<div class="spacer"></div>
		
					<div class="spacer"></div>
					<label>Civilit&eacute; <span class="ast_orange">*</span></label>
					<input type="radio" style="height:auto" name="title" class="input_radio" value="1" checked="checked" /> M.
					<input type="radio" style="height:auto" name="title" class="input_radio" value="2" /> Mme.
					<input type="radio" style="height:auto" name="title" class="input_radio" value="3" /> Mlle.
					<div class="spacer"></div>
					
					<div class="form_line"><label>Raison sociale  <span class="ast_orange">*</span></label>
        <input type="text" name="company" value="{userInfoCompany:h}" size="40"  maxlength="50" data-required="true" />
        </div>
               
        
        <div class="form_line"><label>SIRET </label>
        <input type="text" name="siret" value="{userInfoSiret:h}" size="40"  maxlength="14"/>
        </div>
        
        <!--<div class="form_line"><label>NAF </label>
        <input type="text" name="naf" value="{userInfoNaf:h}" size="40"  maxlength="5"/>
        </div>-->
		
	
					<div class="form_line"><label>Nom <span class="ast_orange">*</span></label>
						<input type="text" name="lastname" value="{userInfoLastname:h}" size="40" maxlength="20" data-required="true" />
					</div>
	
					<div class="form_line"><label>Pr&eacute;nom </label>
						<input type="text" name="firstname" value="{userInfoFirstname:h}" size="40" maxlength="20" />
					</div>
	
					<div class="form_line"><label>T&eacute;l. <span class="ast_orange">*</span></label>
						<input type="text" name="phonenumber" value="{userInfoPhone:h}" size="40" maxlength="20" data-required="true" 
						 data-validate="number" style="width: 160px" />
					</div>
	
					<!--<div class="form_line"><label>T&eacute;l. portable </label>
			<input type="text" name="gsm" value="{userInfoGsm:h}" size="40" maxlength="20" />
			</div>-->
	
					
					<div class="form_line"><label>Adresse <span class="ast_orange">*</span></label>
						<input type="text" name="adress" value="{userInfoAdress:h}" maxlength="100" data-required="true" style="width: 265px" />
					</div>
	
					<!--<div class="form_line"><label>Adresse 2 </label>
			<input type="text" name="adress_2" value="{userInfoAdress_2:h}" size="40"  maxlength="100"/>
			</div>-->
	
					<div class="form_line"><label>Code postal <span class="ast_orange">*</span></label>
						<input type="text" name="zipcode" value="{userInfoZipcode:h}" size="40" maxlength="10" data-required="true" style="width: 80px" />
					</div>
	
					<div class="form_line"><label>Commune <span class="ast_orange">*</span></label>
						<input type="text" name="city" value="{userInfoCity:h}" size="40" maxlength="50" data-required="true" />
					</div>
	
					<div class="form_line"><label>Pays <span class="ast_orange">*</span></label>
						<select id="idstate" name="idstate">
							<option value="1" selected="selected">France</option>
							<option value="3">Alg&eacute;rie</option>
							<option value="2">Andorre</option>
							<option value="4">Allemagne</option>
							<option value="5">Arabie Saoudite</option>
							<option value="6">Autriche</option>
							<option value="7">Belgique</option>
							<option value="8">Bulgarie</option>
							<option value="9">Chypre</option>
							<option value="10">Danemark</option>
							<option value="11">Espagne</option>
							<option value="12">Estonie</option>
							<option value="13">Grande Bretagne</option>
							<option value="14">Gr&egrave;ce</option>
							<option value="15">Hongrie</option>
							<option value="16">Irlande</option>
							<option value="17">Islande</option>
							<option value="18">Israel</option>
							<option value="19">Italie</option>
							<option value="20">Koweit</option>
							<option value="21">Lettonie</option>
							<option value="22">Liban</option>
							<option value="23">Liechtenstein</option>
							<option value="24">Lithuanie</option>
							<option value="25">Luxembourg</option>
							<option value="26">Maroc</option>
							<option value="27">Monaco</option>
							<option value="28">Norv&egrave;ge</option>
							<option value="29">Pays-bas</option>
							<option value="30">Pologne</option>
							<option value="31">Portugal</option>
							<option value="32">R&eacute;publique Tch&egrave;que</option>
							<option value="33">Roumanie</option>
							<option value="34">Slovaquie</option>
							<option value="35">Slov&eacute;nie</option>
							<option value="36">Su&egrave;de</option>
							<option value="37">Suisse</option>
							<option value="38">Tunisie</option>
							<option value="39">Turquie</option>
							<option value="40">Ukraine</option>
							<option value="41">Emirats Arabes Unis</option>
							<option value="42">S&eacute;n&eacute;gal</option>
							<option value="43">Saint-Vincent-et-les-Grenadines</option>
							<option value="44">France (DOM-TOM)</option>
							<option value="45">Burkina Faso</option>
							<option value="46">Canada</option>
							<option value="47">Congo</option>
							<option value="48">France H.T.</option>
							<option value="49">Madagascar</option>
							<option value="50">R&eacute;publique de Maurice</option>
							<option value="51">R&eacute;publique du Congo</option>
							<option value="52">C&ocirc;te d'Ivoire</option>
						</select>
					</div>
					<div class="form_line"><label>Optin</label>
            <input type="checkbox" name="optin">
					</div>
					<div class="form_line"><label>Date de naissance: </label>
            <input type="text" name="birthday" value="{dateNaissance:h}" size="40" maxlength="20" class="datepicker" />
					</div>
	
					<div class="spacer"></div>
	
					<div class="form-footer">
						<p class="txt_oblg">Tous les champs marqu&eacute;s d'un <span class="ast_orange">*</span> sont &agrave; remplir
							obligatoirement</p>
						<div class="spacer"></div><br />
						<div class="spacer"></div>
	
						<p class="txt_oblg">Selon la loi "Informatique et libert&eacute;s" du 05/01/76, vous disposez d'un droit
							d'acc&egrave;s, de rectification et de suppression des donn&eacute;es qui vous
							concernent.</p>
						<p class="txt_oblg"><input type="checkbox" style="width: auto; border: medium none ! important; height: auto;"
							 name="newsletter" value="1" />&nbsp;<strong>Abonnement &agrave; la newsletter du site.</strong> Tenez-moi
							inform&eacute; de
							l'actualit&eacute;,
							des promotions et des op&eacute;rations de d&eacute;stockage qui vous concernent.</p>
	
					</div>
				</div>

			</div>

			<div class="spacer"></div>

			<div class="submit">
				<input type="submit" name="Register" value="{registerSubmitValue:h}" class="btn_send_register" />
			</div>
			<input type="hidden" name="idsource" value="{param_admin_default_buyer_source}" />
		</form>

	</div>
	{end:}
</div>
<!--
<flexy:include src="b_right.tpl"></flexy:include>-->
</div>

<script type="text/javascript">
	function hidelogin() {
		$('#loginpanel').animate({
			top: -120
		}, {
			queue: false,
			easing: 'easeOutCirc',
			duration: 300
		});
	}

	$('#mylogin').click(function (e) {
		e.stopPropagation();
		$('#loginpanel').animate({
			top: 0
		}, {
			queue: false,
			easing: 'easeOutCirc',
			duration: 200
		});
	});
	$("body").click(function (e) {
		if (e.target.id == "loginpanel" || $(e.target).parents("#loginpanel").size()) e.stopPropagation();
		else hidelogin();
		if (e.target.id == "menu" || $(e.target).parents("#menu").size()) e.stopPropagation();
		else menureset();
	});
	$("form#login #email, form#login #password").textPlaceholder();
	$("form#search #search-text").textPlaceholder();
	$("form#newsletter #emailnews").textPlaceholder();

	$('#icons_compare').mouseenter(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_compare').mouseleave(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseenter(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseleave(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
</script>

<!--====================================== Fin du contenu ======================================-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="{start_url}/templates/catalog/js/jquery-validate.js" type="text/javascript"></script>
<script type="text/javascript">
	jQuery(document).ready(function () {
		// validate the comment form when it is submitted
		jQuery("#login").validate({
			onSubmit: true,
			eachInvalidField: function () {
				//jQuery(this).css('border-color: #f00');
				alert('Merci de renseigner une adresse email valide');
			},
			eachValidField: function () {
				jQuery(this).css('border-color: #e0e0e0');
			}
		});
		jQuery.validateExtend({
			rlogin: {
				required: true,
				pattern: /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
			}
		});
	});
	
$(function(){
    $.noConflict()
    //$.getScript("https://code.jquery.com/jquery-1.12.4.js");
    //$.getScript("https://code.jquery.com/ui/1.12.1/jquery-ui.js");
    $( ".datepicker" ).datepicker();
});

</script>
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>
<!-- {if:orderInProgress}
{else:}
<flexy:include src="foot.tpl"></flexy:include>
{end:} -->

</div>