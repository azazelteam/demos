<html>
<head>
	<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/popup_basket.css" />
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	
</head>
<body>
	<div class="borderConfirmDevis">
	{if:iduser}<span style="color:red;font-weight: bold;">/basket/popup_estimate_basket.tpl</span>{end:}
		<img src="{start_url}/images/front_office/global/topPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />
		<div class="bgContentDevis">
		<div class="contentConfirmDevis">
			<div class="spacer"></div><br/>
			
			<div class="panierConfirmDevis">
				<h1>Vous venez d'ajouter &agrave; votre panier :</h1>
				<div class="spacer"></div>
				
				<table class="tableConfirmDevis" cellspacing="0" cellpadding="0" border="0">
				 <tr>
				 <td class="img"><img src="{imageURL}" onclick="self.parent.tb_remove();" width="70" /></td>
				 <td class="txt">{productName}</td>
				 </table>
				 
			
				<a href="#" onclick="self.parent.tb_remove();" class="continue">Poursuivre ma visite</a>
				<a href="#" onclick="self.parent.tb_remove('redirectToEstimateBasket();');" class="devis_acheter">Finaliser mon devis</a>
				<div class="spacer"></div><br/>
			</div>
		</div>
		</div>
		<img src="{start_url}/images/front_office/global/bottomPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />
		<div class="spacer"></div>
	</div>
</body>
</html>
