<flexy:include src="/head/header.tpl"></flexy:include>
	<!-- Catégorie -->
	<div id="content" >	
	{if:iduser}<span style="color:red;font-weight: bold;">/custom_page.tpl</span>{end:}
    {breadcrumbs:h}
		<div id="categorybloc">
			<div class="intro">
				{Content:h}
				<div style="clear:both;margin-bottom:20px;"></div>
				{if:idpage_prod}
					{if:categ_group}
					<div flexy:foreach="product_categories,product_categorie" class="cat-marque">
						<h3>{product_categorie[cat_name]:e}</h3>
						<div class="products">
							<ul class="list_product">
								<li itemscope itemtype="http://schema.org/Product" flexy:foreach="product_categorie[products],key,product">
									<flexy:include src="/product/product_detail_13.tpl"></flexy:include>
								</li>
							</ul>
						</div>
					</div>
					{else:}
					<div class="cat-marque">
						<div class="products">
							<ul class="list_product">
								{foreach:product_categories,product_categorie}
									<li itemscope itemtype="http://schema.org/Product" flexy:foreach="product_categorie[products],key,product">
										<flexy:include src="/product/product_detail_13.tpl"></flexy:include>
									</li>
								{end:}
							</ul>
						</div>
					</div>
					{end:}
				{end:}
				<div style="clear:both"></div>
				{Content_plus:h}
			</div>
			<div style="clear:both"></div>
			<div class="spacer"></div>
			</div></div></div></div>
<flexy:include src="foot.tpl"></flexy:include>