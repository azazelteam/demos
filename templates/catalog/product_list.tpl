{head.addCSS(#/templates/catalog/css/products.css#)}
<flexy:include src="header.tpl"></flexy:include>
<flexy:include src="leftBanner.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->


<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/product_list.tpl</span>{end:}
<h3>{category_name}</h3>
<br/>
<p>{category_text:h}</p>
<br style="clear:both;"/>

<table cellpadding="10" cellspacing="0" border="0" align="center">
	<tr flexy:foreach="category.getChildren(),child">
		<td style="vertical-align:top; padding-bottom:15px;" height="300">
			<!--<h3>
			<span class="catlink_arrow">&gt;&gt; </span><a class="CategoryName" href="{child.getURL():h}" class="catlink">{child.get(#name_1#):h}</a>
			</h3>-->
			<h3 class="CategoryName" ><span class="catlink_arrow">&gt;&gt; </span>{child.get(#name_1#):h}</H3>

			<br/>
			<br/>
			<ul class="listcats">
				<li flexy:foreach="child.getProducts(),product">
					<a href="{product.getURL():h}">
						<img src="{product.getImageURI(#70#)}" alt="{product.get(#name_1#):h}" />
					</a><br/>	
						<a href="{product.getURL():h}" style="color: #f39903;">{product.get(#name_1#):h}</a>
					<br />
					{if:product.getLowerPriceET()}
						<br/><span style="color: #f39903; font-size: 0.8em; font-weight: bold;">à partir de {product.getLowerPriceET():h}</span><br/>								
					{else:}
						&gt;<a onclick="window.open('{baseURL}/catalog/mail_product.php?idproduct={product.getID()}','mailto','width=630,height=750,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');"> Nous contacter</a>
						<br/>					
					{end:}
					
				</li>
			</ul>
			<br style="clear:both;" />
		</td>
	</tr>
</table>

<br style="clear:both;">

</div>

<!--====================================== Fin du contenu ======================================-->
<!-- Colonne de droite de la page -->
<flexy:include src="b_right.tpl"></flexy:include>            
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
