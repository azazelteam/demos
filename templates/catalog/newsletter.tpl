<flexy:include src="/head/head.tpl"></flexy:include>
<flexy:include src="menu/menu_alt.tpl"></flexy:include>
<!-- <flexy:include src="b_left_cpt.tpl"></flexy:include>-->
<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/newsletter.css" />

<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/newsletter.tpl</span>{end:}
	<!-- début div centre -->
    <p class="textNews" style="text-align:center; font-weight:bold; color:#C50106;" flexy:if="errorString">{errorString}</p>
    {if:display_form}
        <form action="{action}" method="post" enctype="multipart/form-data">
            <div class="spacer"></div>
            
            {if:subscription}
                <p class="textNews">
                    <strong>En vous abonnant à notre newsletter restez connecté à l'actualité de notre site {adSitename}.</strong>
                </p>
            {else:}
                <p class="textNews">
                    <strong>En vous désincrivant de notre newsletter vous ne recevrez plus de courriel.</strong>
                </p>
            {end:}
            
                <label>Email *</label>
                <input size="10" type="text" name="email" class="input_text" value="" />
                <div class="spacer"></div>
                
            {if:subscription}
                <label>Civilité</label>
                <select name="gender"></select>
                <div class="spacer"></div>
                
                <label>Nom</label>
                <input size="10" type="text" name="lastname" class="input_text" value="" />
                <div class="spacer"></div>
                
                <label>Prénom</label>
                <input size="10" type="text" name="firstname" class="input_text" value="" />
                <div class="spacer"></div>
                
                <label>Société</label>
                <input size="10" type="text" name="company" class="input_text" value="" />
                <div class="spacer"></div>

                <label>Adresse</label>
                <input size="10" type="text" name="address" class="input_text" value="" />
                <div class="spacer"></div>
                
                <label>Complément d'adresse</label>
                <input size="10" type="text" name="address2" class="input_text" value="" />
                <div class="spacer"></div>
                
                <label>Code postal</label>
                <input size="10" type="text" name="zipcode" class="input_text" value="" />
                <div class="spacer"></div>
                
                <label>Ville</label>
                <input size="10" type="text" name="city" class="input_text" value="" />
                <div class="spacer"></div>
                
                <label>N° tel</label>
                <input size="10" type="text" name="phonenumber" class="input_text" value="" />
                <div class="spacer"></div>
                <input type="submit" name="valider" value="M'abonner!" class="btn_send" />
                <div class="spacer"></div>
                        
            {else:}

                <div class="spacer"></div><br/>
                <input type="submit" value="Me désinscrire!" class="btn_send" />
    
            {end:}
            
        </form>
        
        <div class="spacer"></div>	
    {else:}
        {if:subscription}
        <p class="textNews" style="text-align:center; font-weight:bold; color:#C50106;">Félicitations! Vous êtes désormais abonné à la newsletter.</p>
        <p><a href="javascript:history.go(-1)" style="width: 120px;text-align: center;padding: 0.4em 1em 0.3em; border-radius: 14px 14px 14px 14px; background: none repeat scroll 0px 0px rgb(239, 137, 6); color: rgb(255, 255, 255); text-decoration: none; display: block; float: left; height: 22px; line-height: 20px; box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);">Retour</a></p>
        {else:}
        <p class="textNews" style="text-align:center; font-weight:bold; color:#C50106;">Désormais, vous ne recevrez plus de courriel de notre part</p>
        <p><a href="javascript:history.go(-1)" style="width: 120px;text-align: center;padding: 0.4em 1em 0.3em; border-radius: 14px 14px 14px 14px; background: none repeat scroll 0px 0px rgb(239, 137, 6); color: rgb(255, 255, 255); text-decoration: none; display: block; float: left; height: 22px; line-height: 20px; box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);">Retour</a></p>
        {end:}
    {end:}	
	<!-- fin div centre -->
</div>
</div>
</div>

<!--<flexy:include src="b_right.tpl"></flexy:include>   -->
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 