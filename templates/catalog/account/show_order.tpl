	<flexy:include src="head/header.tpl"></flexy:include>
    
    <script type="text/javascript" language="javascript">
			<!--
			function CheckAll()
			{
				var checkboxes = document.forms.mainform.elements["IdArticles"];
				var atleastcheckbox = 0;
				
				for (i = 0; i < checkboxes.length; i++){
					
					if(checkboxes[i].checked){
						var valeur = checkboxes[i].value;
						var qtt = "quantity_"+valeur;
						if(document.forms.mainform.elements["quantity_"+valeur].value==""){
							alert ("Vous devez saisir une quantité pour chaque référence que vous voulez commander !");
							return false;
						} else {
							if(isNaN(document.forms.mainform.elements["quantity_"+valeur].value)){
								alert ("La quantité pour chaque référence doit être un nombre !");
								return false;
							} else {
								if(document.forms.mainform.elements["quantity_"+valeur].value <= 0){
									alert ("La quantité pour chaque référence doit être un nombre supérieur à 0 !");
									return false;
								}
							}
						}
						
						atleastcheckbox++;
					}
					
				}
				
				if(atleastcheckbox == 0){
					alert("Veuillez cocher au moins une case avant de commander !");
					return false;
				}else{
					return true;
				}	
			}
			
			// Auto check the box ! checkFlex Busta Flex
			function checktheBox(id){
				var myCheckboxes = document.forms.mainform.elements["IdArticles"];
				var myInputQuantity = document.forms.mainform.elements["quantity_"+id];
				
				for (i = 0; i < myCheckboxes.length; i++){
					if(myCheckboxes[i].value == id){
						if((myInputQuantity.value != "") && (myInputQuantity.value != 0) ){
							myCheckboxes[i].checked = true ;
						}
						else {
							myCheckboxes[i].checked = false ;
						}
					}
				}
			}
			
			// -->
		</script>
    
    
    
    
<!--</head>
<body>-->
<!-- Header -->
<!--<flexy:include src="head/banner.tpl"></flexy:include>-->

<div id="content" class="account">
{if:iduser}<span style="color:red;font-weight: bold;">/account/show_order.tpl</span>{end:}

	<div id="leftbloc">
			<flexy:include src="menu/menu_compte.tpl"></flexy:include>
	</div>
	
	<div id="categorybloc" class="account">
		<div class="intro">

			<h2>Commande n° {idorder}</h2>
			
			<div id="text">
			<p>Votre commande a été bien enregistrée le {order_date:e}.</p>

			<!-- ************  statut de la commande ************-->
			{if:Str_Status_ToDo}
				<p class="blocTextNormal"><strong>Votre commande est actuellement en cours de traitement.</strong></p>	
			{end:} 
			{if:Str_Status_InProgress}
				<p class="blocTextNormal"><strong>Votre commande est actuellement en cours de traitement.</strong></p>	
			{end:} 
			{if:Str_Status_Send}
				<p class="blocTextNormal"><strong>Votre commande définitive vous a été transmise.</strong></p>	
			{end:}
			{if:Str_Status_Ordered}
				<p class="blocTextNormal"><strong>Votre commande est confirmée.</strong></p>	
			{end:}
			{if:Str_Status_Invoiced}
				<p class="blocTextNormal"><strong>Votre commande est confirmée et facturée.</strong></p>	
			{end:}
			{if:Str_Status_Paid}
				<p class="blocTextNormal"><strong>Votre commande est facturée et payée.</strong></p>	
			{end:}


			<form name="mainform" id="mainform" action="{start_url}/catalog/show_order.php?idorder={idorder}" method="POST" >
            <input type="hidden" name="validated" value="1">
            
			<div class="spacer"></div>


			<!-- *************** Articles de la commande *************** -->
			<table class="accountTable" cellspacing="0" cellpadding="0" border="0" style="font-size:11px;">
				<tr>
					<th>Icone</th>
					<th>Référence</th>
					<th width="180">Désignation</th>
					<th>Délai de livraison</th>
					<th>Prix TTC</th>
					<th>Remise</th>	
					<th>Prix remisé TTC</th>
					<th>Qté</th>
					{if:hasLot}
						<th>Qté / lot</th>
					{end:}
					<th>Prix total TTC</th>
                    <th>Qté à cder</th>
                    <th>Cder</th>
					<!--
					<th class="detail">{traduction(#priceTTC#)}</th>
					-->
					<!--<th class="detail">{traduction(#status#)}</th>-->
					<!--
					<th class="detail">{traduction(#total_priceWT#)}</th>
					-->
				</tr>
				
				<!-- ************ Construction du tableau des articles ************ -->
				{foreach:order_articles,line}
					<tr>
						<td style="text-align:center;">
							{if:line[Icon]}
								<img src="{line[Icon]:h}" width="50" alt="Icone">
							{else:}
								Pas d'icone
							{end:}
						</td>
						<td style="text-align:center;">{line[reference]:h}</td>
						<td style="text-align:left;">{line[summary]:h}</td>
						<td style="text-align:center;">{line[delivdelay]:h}</td>
						<td style="text-align:center;">{line[unit_price]:h}</td>
						<td style="text-align:center;">{line[ref_discount]:h}</td>
						<td style="text-align:center;">{line[discount_price]:h}</td>
                        <td style="text-align:center;">{line[quantity]:h}</td>       
						
					        
						{if:hasLot}
							<td style="text-align:center;">{line[lot]:h}</td>
						{end:}
						<td style="text-align:center;" class="tdPrice">{line[RefTotalAmountHT]:h}</td>
                        <td class="inProgress" style="text-align:center;"><input type="text" id="quantity_{line[i]}" name="quantity_{line[i]}" size="2" onChange="checktheBox({line[idarticle]});" class="input_quantity" value="1"  /></td>
                        <td class="inProgress" style="text-align:center;"><input type="checkbox" id="check_{line[i]}" name="check_{line[i]}" value="{line[i]}" style="margin: auto; float: none; display: block; border: medium none; height: 15px; width: 15px;"/></td>
                       
                        
                        <input type="hidden" name="ref_{line[i]}" value="{line[reference]:h}" >
					</tr>
				{end:}
			</table>
			
            <div class="spacer"></div>
			</p>
			<input type="submit" name="commander" value="Commander" class="input_commander_medium floatleft" style="float:right;margin-right:2%;"/>
			</form>	
            
            
            {if:refs_not_found}
<br><br>
<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
<th>Qté</th>
 <th>Référence</td>
</tr>
{foreach:list_articles_not_found,article}

<tr>
<td>{article[quantity]}</td>
<td>{article[reference]}</td>
</tr>
{end:}
</table>

</br></br>

<font color=black size=-1 face=arial><b>
Les articles ci-dessus n'ont pas été trouvées ou la quantité est fausse!
</br>
Merci de vérifier la référence et la quantité.

</b></font>
<br><br>
{end:}

{if:hasrefwoprice}

<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
 <th>Référence</td>
</tr>
{foreach:list_refs_hide_prices,ref}
<tr>
<td>{ref}</td>
</tr>
{end:}
</table>
		
<br><br>
		<p style="color:#000000; font-weight:bold">
		Le prix des références ci-dessus n'est pas disponible pour le moment
		</p>
		<p>
		Pour plus d'informations concernant ces références, <a href="mailto:{ad_mail}" style="text-decoration:underline;">contactez-nous par email</a> ou au <font style="font-weight:bold; color:#FF0000;">{ad_tel}</font>
		</p>
{end:}
	<div class="spacer"></div>
            
            
            
            
            
            
            
            
            
			
			<!-- Total du devis -->
		<table class="accountTable" border="0" cellpadding="0" cellspacing="0">	
		{if:remise}
			<tr>
				<th align="center">Montant HT</th>
				<th align="center">Taux remise</th>
				<th align="center">Total HT avec remise</th>
				<th align="center">Remise</th>
			{end:}
		
		{if:is_valid}
			
				<th align="center">Montant HT</th>
				<th align="center">Taux de remise commerciale</th>
				<th align="center">Remise commerciale</th>
			
		{end:}
				<th align="center">Frais de port et emballage</th>
				<th align="center">Total HT</th>
			    <th align="center">TVA</th>
			
		</tr>
		
		{if:remise}
			<tr>
				<td align="right">{total_amount_ht_avant_remise:h} &nbsp;</td>
				<td align="right">{remise_rate:h}&nbsp;</td>
				<td align="right">{total_amount_ht:h}&nbsp;</td>
				<td align="right">{discount:h}&nbsp;</td>
			
		{end:}
		
		{if:is_valid}
				<td align="right">{total_amount_ht_avant_remise:h} &nbsp;</td>
				<td align="right">{total_discount:h}&nbsp;</td>
				<td align="right">{total_discount_amount:h}&nbsp;</td>
		
		{end:}
		
		    	<td align="right">{total_charge_ht:h}</td>
				<td align="right">{total_amount_ht_wb:h}</td>
				<td align="right">{getVATAmount:h}</td>
			
			
		</tr>
		
		<tr>
		{if:remise}
			    <th class="tdPrice" align="left" colspan="4"></th>
			
		{end:}
		
		{if:is_valid}
				<th class="tdPrice" align="left" colspan="3"></th>
		
		{end:}
		
			
			<th class="tdPrice" align="left">Total TTC</th>
			<td class="tdPrice" align="right">{total_amount_ttc_wbi:h}</td>
		</tr>
		</table>
			
			
					
			<!-- ******************* Pied de commande ****************** -->
			<!--<flexy:include src="basket_amounts.tpl"></flexy:include>-->

			<p class="blocTextNormal" style="float:left; width:200px;"><strong>Vos coordonnées :</strong><br />
				{buyer_company}<br />
				{buyer_title} {buyer_firstname} {buyer_lastname}<br />
				{buyer_adress} {buyer_adress_2}<br />
				{buyer_zipcode} {buyer_city}
			<p>
			
			{if:iddelivery}
			<p class="blocTextNormal" style="float:left; width:200px;"><strong>Adresse de Livraison :</strong><br />
				{d_buyer_company}<br />
				{d_buyer_title} {d_buyer_firstname} {d_buyer_lastname}<br />
				{d_buyer_adress} {d_buyer_adress_2}<br />
				{d_buyer_zipcode} {d_buyer_city}
			<p>
			{end:}
			
			{if:idbilling}
			<p class="blocTextNormal" style="float:left; width:200px;"><strong>Adresse de facturation :</strong><br />
				{b_buyer_company}</strong><br />
				{b_buyer_title} {b_buyer_firstname} {b_buyer_lastname}<br />
				{b_buyer_adress} {b_buyer_adress_2}<br />
				{b_buyer_zipcode} {b_buyer_city}
			<p>	
			{end:}

			<p class="blocTextNormal" style="float:left; width:200px;"><strong>Moyens de paiement :</strong><br />
				&#0155; {PaymentDelay:h}<br />
				&#0155; {PayMod:h}
			</p>
			<div class="spacer"></div><br/>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
	</div>
	<div class="spacer"></div>
	
</div>
</div>
</div>
<flexy:include src="foot.tpl"></flexy:include>