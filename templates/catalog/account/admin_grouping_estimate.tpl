{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
        <link rel="stylesheet" type="text/css" href="{baseURL}/css/catalog/admin_grouping.css" />
        <link rel="stylesheet" type="text/css" href="{baseURL}/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
	</head>
	<body>
	<!-- Header -->

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/admin_grouping_estimate.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
			 {if:logoGroup}
		
             <a href="/catalog/grouping_catalog.php"> <img src="{logoGroup}" border="0" alt="Logo" width="180px" height="100px"/> </a>
             {end:}
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
				<h2>Ensemble des devis du groupement <strong>{buyerGroup}</strong></h2>
                
                
                <br>
		
				{form:h}
		
				<br><br><br>
									
{if:NoResults}
			<p class="blocTextNormal">Aucun résultat</p>
		{else:}
				
			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th>n°</th>
				<th>Date</th>
				<th>Montant TTC</th>
				<th>Délai de validité</th>
				<th>Statut</th>
				<th>n° client</th>
				<th>Client</th>
				<th>PDF</th>
				<th style="background:#F9F9F9;">Détail</th>
			</tr>
			{foreach:array_estimate,line}
				{if:line[color]}
					<tr>
						<td class="inProgress">{line[idestimate]}</td>
						<td class="inProgress">{line[DateHeure]:h}</td>
						<td class="inProgress">{line[total_amount]:h}</td>
						<td class="inProgress">{line[valid_until]:h}</td>
						<td class="inProgress">{line[status]:h}</td>
						<td class="inProgress">{line[idbuyer]:h}</td>
						<td class="inProgress">{line[company]:h}</td>
						<td class="inProgress">-</td>
						<td class="inProgress">-</td>
					</tr>
				{else:}
				<tr>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[idestimate]}</a></td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[DateHeure]:h}</a></td>
						<td class="tdPrice"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[total_amount_ht]:h}</a></td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[valid_until]:h}</a></td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[status]:h}</a></td>
						<td>{line[idbuyer]:h}</td>
						<td>{line[company]:h}</td>
						<td>
							<a href="{baseURL}/catalog/pdf_estimate.php?idestimate={line[idestimate]}" target="_blank">
							<img src="http://www.astelos.com/templates/catalog/images/pdf_icon.gif" /></a>
						</td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">
						<img src="http://www.astelos.com/templates/catalog/images/iconsNextBack.png" style="vertical-align:middle;" /></a></td>
					</tr>
					
				{end:}				
			{end:}		
			</table>
		{end:}

				<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
            
		</div>
		<div class="spacer"></div>
		
	</div>
	</div>
	</div>
	<flexy:include src="foot.tpl"></flexy:include>

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}

