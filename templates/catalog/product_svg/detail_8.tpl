<span style="display:none;">
	<input type="hidden" id="idproduct" value="{product.getId()}" />
    <input type="hidden" id="idarticle{index}"  value="{article.getId()}" flexy:foreach="product.getArticles(),index,article" />
	<input type="hidden" id="prixtotal{article.getId()}" value="{article.getCeilingPriceET():h}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="prixtotalremise{article.getId()}" value="{article.getPriceET():h}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="discountRate{article.getId()}" value="{article.getDiscountRate()}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="deliveryTime{article.getId()}" value="{article.getDeliveryTime()}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="hidecost{article.getId()}" value="{article.get(#hidecost#)}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="designation{article.getId()}" value="{article.get(#designation_1#):h}" flexy:foreach="product.getArticles(),article" />
    <input type="hidden" id="stockLevel{article.getId()}" value="{article.get(#stock_level#)}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="colorCode{color.id}" value="{color.code}" flexy:foreach="product.getColors(),color" />
	<input type="hidden" id="colorLabel{color.id}" value="{color.label:h}" flexy:foreach="product.getColors(),color" />
</span>
<script type="text/javascript" src="{baseURL}/js/cart.js" async></script>
{if:iduser}<span style="color:red;font-weight: bold;">/product/detail_8.tpl</span>{end:}
<div id="CartAddConfirmation" style="display: none;">
	<div class="borderConfirmDevis">
		<div class="bgContentDevis">
			<div class="contentConfirmCommande">
				<div class="spacer"></div>
				<br>
				<div class="panierConfirmDevis">
					<span class="h3">Vous venez d'ajouter &agrave; votre panier :</span>
				  <div class="spacer"></div>
					<table>
						 <tr>
							<td class="img"><img src="{product.getImageURI():h}" onClick="$('#CartAddConfirmation').dialog('destroy');" width="70" alt="{product.get(#name_1#)}"></td>
							<td class="txt">{product.get(#name_1#):h}</td>
						</tr>
					</table>
					<a href="#" onClick="$( '#CartAddConfirmation' ).dialog( 'destroy' ); return false;" class="btn-med grey">Poursuivre ma visite</a>
					<a href="{start_url}/catalog/basket.php" class="btn-med grey" >Acc&eacute;der &agrave; mon panier</a>
					<div class="spacer"></div>
					<br>
				</div>
			</div>
		</div>
		
		<div class="spacer"></div>
	</div>
</div>
<!-- début de modification -->
<div id="panel" class="panel_product">
<!-- fin de modification -->
	<script type="text/javascript">
	   
		function getSelectedArticle( tableId ){		
			/* utilisation de la r&eacute;f&eacute;rence */			
			if( !$( "select[id^='HeadingSelector']" ).size() )
				return $( "#HeadingValues_" + tableId ).val();				
			/* utilisation des intitul&eacute;s */			
			var selection = false;			
			$( "#ArticleList" ).find( "li" ).each( function( i ){			
				var identifier = $( this ).attr( "id" ).split( "_" );
				var idarticle = identifier[ 1 ];
				var selected = true;				
				$( "select[id^='HeadingSelector'][id$='_" + tableId + "']" ).each( function( i ){			
					
					var headingIndex = $( this ).attr( "id" ).substr( 16, 1 );		
					if( $( "#HeadingValue_" + idarticle + "_" + headingIndex ).val() != $( this ).val() )
						selected = false;					
				});				
				if( selected ){
					selection = idarticle;
				}
				
			});			
			return selection;		
		}

        
	</script>
	<div flexy:foreach="product.getArticleTables(),tableId,Articles">
		<div style="color:#fe7b00;; font-size: 12px; font-weight: bold;">
<span flexy:if="product.get(#table_name_1#)"> {product.get(#table_name_1#)}</span>
		</div>
		{Math.counter.reset(#-1#)}
        <ul id="ArticleList" style="display:none;">
            <li id="idarticle_{article.getId()}" style="{Math.counter.inc()}" flexy:foreach="product.getArticles(),article"></li>
        </ul>
		<div class="selection">        
	        <div style="float: left;">
                <div id="selectionHeader" class="selectionHeader">
                {if:Math.counter.value}
                    <!-- Module de selection -->
                    <!-- début de modification -->
                    {if:product.getSelectableHeadings(2)}	
                    <!-- fin de modification -->			
                        <script type="text/javascript">
                            function selectHeading( headingIndex, headingValue, tableId, headingValue ){    
                                /* listes suivantes */
                                var i = headingIndex + 1;
								var $nextHeadingSelectedValue = $( "#HeadingSelector_" + i + "_" + tableId ).val();/**/
                                while( i < 3 ){                        
                                    $( "#HeadingSelectorContainer_" + i + "_" + tableId ).hide();
                                    /*$( "#HeadingSelector_" + i + "_" + tableId ).val( "" );*/
                                    i++;                                    
                                }
                                /* prochaine liste */                
                                var nextHeadingIndex = headingIndex + 1;    
                                if( $( "#HeadingSelector_" + headingIndex + "_" + tableId ).val() != "" )
                                        $( "#HeadingSelectorContainer_" + nextHeadingIndex + "_" + tableId ).fadeIn( 700 );
                        
                                if( !$( "#HeadingSelector_" + nextHeadingIndex + "_" + tableId ).size() ){
                                    var selection = getSelectedArticle( tableId );
                                    if( selection )
                                        selectArticle( selection, tableId );            
                                    return;                                    
                                }                        
                                var $nextHeadingSelector = $( "#HeadingSelector_" + nextHeadingIndex + "_" + tableId );                                
                                /* filtrer les valeurs de la prochaine liste */
								var $nextHeadingValueChanged = true;/**/
                                $nextHeadingSelector.children().each( function( i ){
                                    if( $( this ).val() )
                                        $( this ).remove();
                                });                                
                                $( "#ArticleList" ).find( "li" ).each( function( i ){
									var identifier = $( this ).attr( "id" ).split( "_" );
									var idarticle = identifier[ 1 ];                                    
                                    if( $( "#HeadingValue_" + idarticle + "_" + headingIndex ).val() == headingValue )
									{
										var doublon = false;
										$nextHeadingSelector.children().each( function( i ){
											if ($(this).text() == $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val())
												doublon = true;
												return;
										});
										if (!doublon){
											if ($( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() == $nextHeadingSelectedValue)
											{
												$nextHeadingSelector.append( '<option value="' + $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() + '" selected="selected">' + $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() + '</option>' );
												showprice(getSelectedArticle(tableId));
												$nextHeadingValueChanged = false;
												selectHeading( nextHeadingIndex, $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val(), tableId, $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() );
											}	
											else
												$nextHeadingSelector.append( '<option value="' + $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() + '">' + $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() + '</option>' );
										}
									}/**/
                                });
                                if ($nextHeadingValueChanged)
									$nextHeadingSelector.val( "" );/**/                                
                            }							
							function showprice(sel){
								if (!sel)
									return false;
								$(".htttc").hide();
								$(".htttc-"+sel).show();
							}
                        </script>
                        <!-- début de modification -->
                        <span flexy:foreach="product.getSelectableHeadings(2),headingIndex,heading">
                        <!-- fin de modification -->
                        {if:!headingIndex}                            
                             <span id="HeadingSelectorContainer_{headingIndex}_{tableId}" style="display:block;">
                                <input type="hidden" id="HeadingValue_{article.getId()}_{headingIndex}" value="{article.getHeadingValue(heading.id):h}" flexy:foreach="product.getArticles(),article" />
                                <label for="HeadingSelector_{headingIndex}_{tableId}" class="label_product">{heading.name:h} :</label>                                
                                <select id="HeadingSelector_{headingIndex}_{tableId}" onChange="selectHeading( {headingIndex}, this.options[ this.selectedIndex ].innerHTML, {tableId}, this.options[ this.selectedIndex ].value );showprice(getSelectedArticle({tableId}));" class="select_product">
                                    <option value="">S&eacute;lection</option>
                                </select>
                            </span>
                        {else:}
                            <span id="HeadingSelectorContainer_{headingIndex}_{tableId}" class="orderOptions" style="display:none;">
                                <input type="hidden" id="HeadingValue_{article.getId()}_{headingIndex}" value="{article.getHeadingValue(heading.id):h}" flexy:foreach="product.getArticles(),article" />
                                <label for="HeadingSelector_{headingIndex}_{tableId}" class="label_product">{heading.name:h} :</label>
                                <select id="HeadingSelector_{headingIndex}_{tableId}" onChange="selectHeading( {headingIndex}, this.options[ this.selectedIndex ].innerHTML, {tableId}, this.options[ this.selectedIndex ].value );showprice(getSelectedArticle({tableId}));" class="select_product">
                                    <option value="">S&eacute;lection</option>
                                    <option value="{value:e}" flexy:foreach="heading.values,value">{value:e}</option>
                                </select>
                            </span>
                        {end:}
                        </span>
                    {else:}
                        

                        <div class="orderOptions">
                            <label for="HeadingValues_{tableId}" class="label_product">
                            R&eacute;f&eacute;rence :</label>
                            <select id="HeadingValues_{tableId}" onChange="selectArticle( this.options[ this.selectedIndex ].value, {tableId} );" class="select_product">
                                <option value="">S&eacute;lection</option>
                                <option value="{article.getId()}" flexy:foreach="Articles,article">{article.get(#reference#):h}</option>			
                            </select>
                        </div>
                    {end:}
                {else:}
                    <!-- r&eacute;f&eacute;rence unique -->
                    <input type="hidden" id="HeadingValues_{tableId}" value="{article.getId()}" flexy:foreach="product.getArticles(),article" />

                    <script type="text/javascript">
                        $('.htttc').show();
                    </script>
                    <flexy:toJavascript articleCount="Math.counter.value"></flexy:toJavascript>
                {end:}
                <div class="spacer"></div>
                    <!-- Generation de la quantit&eacute; minimum -->
                    <FLEXY:TOJAVASCRIPT tableId="tableId">
                    <script type="text/javascript">
                    <!--                                            
                        function change_quantity(reference) {
                            var quantityMin = document.getElementById("value_"+reference).value;
                            document.getElementById("HeadingQuantity_"+tableId).value = quantityMin;
                        }                    
                        function fct_plus( tableId ) {                        
                            var idarticle = getSelectedArticle(tableId);                
                            if( idarticle ){
                                var q = document.getElementById("HeadingQuantity_"+tableId).value;
                                var plus = parseInt(q) + 1;
                                document.getElementById("HeadingQuantity_"+tableId).value = plus;
                            } else {
                                alert( "Vous devez d\'abord s\351lectionner une r\351f\351rence" );
                            }    
                            document.getElementById("infoQuantityMin").style.display = "none" ;    
                        }                      
                        function fct_moins( tableId ) {                        
                            var idarticle = getSelectedArticle(tableId);                
                            if( idarticle ){                            
                                var q = document.getElementById("HeadingQuantity_"+tableId).value;
                                var stock_min =  document.getElementById("value_"+idarticle).value;
                                //Si quantit&eacute; == 1 ou stock minimum == quantit&eacute;
                                if(q == 1 || stock_min == q) {
                                    var moins = parseInt(q);
                                }
                                else {
                                    var moins = parseInt(q) - 1;
                                }                                
                                //Affichage message?
                                if(stock_min == q) {
                                    document.getElementById("infoQuantityMin").style.display = "block" ;
                                }                                
                                document.getElementById("HeadingQuantity_"+tableId).value = moins;
                            } else {
                                alert( "Vous devez d\'abord s\351lectionner une r\351f\351rence" );
                            }
                        }                        
                        function chiffres(event) {
                            // Compatibilit&eacute; IE / Firefox
                            if(!event&&window.event) {
                                event=window.event;
                            }
                            // IE
                            if(event.keyCode < 48 || event.keyCode > 57) {
                                event.returnValue = false;
                                event.cancelBubble = true;
                            }	
                            // DOM
                            if(event.which < 48 || event.which > 57) {
                                event.preventDefault();
                                event.stopPropagation();
                            }	
                        }
                    // -->				
                    </script>
                    <div id="select_qty" class="orderOptions quantite">
                        <label for="HeadingQuantity_{tableId}" class="label_product floatleft">Quantit&eacute; : </label>
                        <input type="text" id="HeadingQuantity_{tableId}" name="quantity_{tableId}" value="1" class="select_product qty floatleft" style="width: 54px; height: 54px; text-align: center; background: none repeat scroll 0px 0px rgb(255, 255, 255); border: 2px solid rgb(204, 204, 204); margin-bottom: 8px;" onKeyPress="chiffres(event);" />
                        <div class="up-down" style="float: left; background: none repeat scroll 0% 0% rgb(114, 112, 108); padding: 3px;">
                            <a onClick="fct_plus({tableId});" style="float: left; clear: both; margin-top: -1px;" class="up"><img src="{baseURL}/templates/catalog/images/btn_plus.png" alt="plus"></a>
                            <a onClick="fct_moins({tableId});" style="float: left; clear: both; margin-top: -1px;" class="down"><img src="{baseURL}/templates/catalog/images/btn_moins.png" alt="moins"></a>
                        </div>
                    </div>

                    <p class="pButton">
                        <!-- début de modification -->
                        <input type="button" name="AddCde" id="AddCde" class="btn-med" value="Commander" onClick="addToCart( getSelectedArticle({tableId}), $('#HeadingQuantity_{tableId}').val(), 'order' );" />
                        <!-- fin de modification -->
                        <!--{if:customer}-->
                        <!-- <br>
                        <input type="button" name="AddSD" id="AddSD" value="Demander un devis" class="btn-med" onClick="addToCart( getSelectedArticle({tableId}), $('#HeadingQuantity_{tableId}').val(), 'estimate' );" /> -->
                        <!--{else:}
                        <input type="button" id="AddSD" value="Demander un devis" class="btn-med" onClick="alert('Vous n\'\350tes pas conn\351ct\351s.\nConnectez-vous pour pouvoir demander un devis.');" />	
                        {end:}-->
                    </p>

                    <div class="spacer"></div>
					{if:product.getColors()}
						 <div id="Colors{tableId}">
                            <label class="label_couleur">Couleur :</label>
							<form name="ColorForm" id="ColorForm" action="{baseURL}/catalog/product_one.php" method="get" flexy:ignore="yes">
							<div flexy:foreach="product.getColors(),color"><div class="prod_img">
                            <input type="radio" class="ProductColorRadioButton" name="Colors{tableId}[]" id="Colors{tableId}_{color.id}" value="{color.id}" flexy:ignore="yes" />
                            <a class="thumbnail" href="#thumb" onClick="$('#Colors{tableId}_{color.id}').attr('checked', 'checked'); ">
                            <img alt="Color" class="ProductColorImage" src="{color.image}" height="60"></a></div>
                        </div>
                        <input type="hidden" flexy:ignore="name='displayedDetailTable'" value="{tableId}" />
                                <input type="hidden" flexy:ignore="name='displayedDetailTable'" value="{tableId}" />
                            </form>
                            <div class="spacer"></div>
                    	</div>
					{end:}
                    <span flexy:foreach="product.getArticles(),article" class="sr-only">
                        {if:article.get(#min_cde#)}
                            <input id="value_{article.get(#idarticle#):h}" name="value[]" value="{article.get(#min_cde#):h}" type="hidden" flexy:ignore="yes" />
                        {else:}
                            <input id="value_{article.get(#idarticle#):h}" name="value[]" value="1" type="hidden" flexy:ignore="yes" />
                        {end:}
                    </span>                    
                </div>				
				<p id="infoQuantityMin" style="display:none;">
                    <img src="{baseURL}/templates/catalog/images/icons_warning_qty.jpg" alt="" width="20" height="18">
                    Vous avez atteint la quantit&eacute; minimum pour cette r&eacute;f&eacute;rence.</p>
                    
			</div>

            <p id="infoStockMin" style="display:none;">
				<img src="{baseURL}/templates/catalog/images/icons_warning_qty.jpg" alt="" width="20" height="18">
                Article epuis&eacute;.
            </p>	
            			
            <div class="spacer"></div>

			<div id="contentPrice" >
				<div id="selectionPrice">	
					<p class="description" id="myCaract" style="float:left; width:250px;"><strong></strong></p>
					<p class="description" id="myDelai" style="float:right; text-align:right; width:190px; font-style:italic;"><strong></strong></p>
                </div>
			</div>
		</div>
		<div class="secure_payment">
		 
		</div>
	</div>	
</div>

<script type="text/javascript">
	var idarticle0 = $("#idarticle0").val();
	var stockLevel0 = $("#stockLevel" + idarticle0).val();
	if (typeof articleCount != 'undefined' && articleCount == 0 && stockLevel0 == 0) {
		$("#orderOptions").hide();
		$("#infoStockMin").css("display", "block");
	}
    /* <![CDATA[ */     
        /* ----------------------------------------------------------------------- */
(function() {
   // your page initialization code here
   // the DOM will be available here
   var firstHeadings = [];
var headInput =$('#HeadingSelectorContainer_0_0 input');
$.each(headInput,function(i,val){
    
                var ind = firstHeadings.indexOf($(this).val())
                ;
                if(ind == -1)
                {
                    firstHeadings[i] = $(this).val();
                    $('#HeadingSelector_0_0').append( '<option value="' + $(this).val() + '">' + $(this).val() + '</option>' );
                }
            });/**/
})();
        $(document).ready(function(){
            jQuery(".htttc").each(function(indexe){
                if ($(this).data('price') == $('#lowerprice').val()){
                    $(this).css("display", "block");
                    return false;
                }
            }); 

            // $(".htttc:first").css("display", "block");
            
            
        });
            
    /* ]]> */
	$(function() {
		var options = {  
			zoomType: 'standard',  
			lens:true,  
			preloadImages: true,  
			preloadText: 'Chargement...',
			alwaysOn:false,
			zoomWidth: 250,
			zoomHeight: 250,
			title: false
		};  
		$('.colorZoom').jqzoom(options);		
		$('.zoomPad').css('z-index','auto');
		});

</script>
