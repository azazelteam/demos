<script type="text/javascript" src="{start_url}/js/starbox.js"></script>
<script type="text/javascript" src="{start_url}/js/scriptaculous.js?load=effects"></script>
<script type="text/javascript" src="{start_url}/js/starbox.js"></script>
<script src="{start_url}/js/PopBox.js"  type="text/javascript"></script>


<!-- Image & action sur l'article -->
<div class="left_product">
	{if:iduser}<span style="color:red;font-weight: bold;">/product/chartep_popup.tpl</span>{end:}
	<div id="gallery">
		<!-- Affichage image produit -->
		<img id="ProductOneImg" width="200" src="{imageURL}" alt="{productName:h}"  />		
	</div>

	
	<!-- Picto -->
	{foreach:pubs,pub}
		<img src="{pub}" style="border:none; margin:2px 0 2px 4px; float:left;" width="115" />
	{end:}
	<div class="spacer"></div>
	
</div>

<!-- Description & module de selection -->
<div class="desc_product">
	<img src="{start_url}/images/front_office/global/top_products.jpg" width="484" height="3" alt="" class="floatleft" />
	
	<!-- Titre & description -->
	<h1><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="" /> 
	{productName:h}</h1>
	<div class="spacer"></div>
	
	<!-- Prix --> 
	{if:hasprice}
		<p class="price2"><span class="min">A partir de </span>{pxmin:h} <!-- HT --></p>
	{else:}
		<p class="price2">&nbsp;</p>
	{end:}
	
	<!-- Label --> 
	{if:labelImage}
		<img src="{labelImage:h}" alt="{labelName:h}" class="floatlabel" /> 
	 {end:}
	 
	 <!-- Si promotions --> 
	{if:hasPromo} 
		<div class="promos">
			<img src="{start_url}/images/front_office/icones/promo_product.jpg" width="120" height="20" class="starProduct" alt="Promotions" />
			<br/>Jusqu'au {jourFin}.{moisFin}.{anneeFin}
		</div>
	{end:}
	
	<div class="spacer"></div>

	<!-- Description -->
	{if:hasDesc}
		<div class="description">
		{if:truncateDescription}
			{shortDesc:h} ...
		{else:}
            {description:h}
    	{end:}
		</div>
		<!-- Plus d'informations -->
		<p>
		{if:truncateDescription}
			<a href="#" class="moreInfos desc_complete" href="#millieu_page" onclick="changeViewDiv('descriptionFull', 'optionsAccessoires', 'planTechniques', 'specificationsGene')" title="Description compl&egrave;te">
			&#0155; Description compl&egrave;te</a>
		{end:}		
		<a href="{hrefcat}" class="desc_complete2">&#0155; Voir tous les autres produits</a>
		{if:brand}
			<a href="{brandLink}" class="desc_complete">&#0155; Voir tous les produits de {brand}</a>&nbsp;&nbsp;&nbsp;&nbsp;
		{end:}

		</p>
		<div class="spacer"></div>
		<div class="floatleft">
		{foreach:pictos,path}
			<img src="{path[img]}" alt="Livraison - Garantie" class="pictos" />
		{end:}
		</div>
		
		<div class="certification">
		<!-- pictos certifications -->
		{foreach:certifications,certification}
			<img src="{certification:h}" alt="Certifications" height="30" />	
		{end:}
		</div>
		<div class="spacer"></div><br/>	 	
	{end:}
	
	<!-- Module de selection & Action -->
	<flexy:include src="/product/detail_popup.tpl"></flexy:include>
	
</div> 


<!-- autoselect de la première référence dans la liste des ref detail_8 -->
<script type="text/javascript">
  var myList = document.getElementById('HeadingValues_0');
  
  if( myList.options.length <= 2 ){
  	
  myList.options[1].selected=true;
  var idarticleEnd = myList.options[1].value ;
      	
  selectArticle( idarticleEnd, 0 );
  myList.options[1].selected=true ;
  
  document.getElementById('selectionHeader').style.display = "none" ;
  }
  
</script>


