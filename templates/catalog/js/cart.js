/* --------------------------------------------------------------------------------------------- */
/* ajout d'une r\351f\351rence au panier */

$('.btn-det').click(function(e){
	e.preventDefault();e.stopPropagation();
});

function addToCart( idarticle, quantity, cartType ){
	console.log('addToCArt');
	$.getScript("/templates/catalog/js/jquery/jquery-ui-1.8.4.custom.min.js");
	window.scrollTo(0, 0);
	if( !idarticle ){
	
		alert( "Vous devez d\'abord s\351lectionner une r\351f\351rence" );
		return;
		
	}
	/* ajout back office */
	
	if( window.opener && window.opener.document && window.opener.document.getElementById( 'adminScript' ) ){

		window.opener.location = window.opener.document.getElementById( 'adminScript' ).value + '&IdProduct=' + idarticle;
		
		if( !confirm( "Votre s\351lection a \351t\351 valid\351e. Souhaitez-vous poursuivre votre visite?" ) )
			window.close();
			
		return;
		
	}

	var data = "idarticle=" + idarticle + "&quantity=" + quantity;
	
	var done = false;

	$( document ).find( ".ProductColorRadioButton" ).each( function( i ){
	
		if( $( this ).attr( "checked" ) &&  $( this ).val() && !done ){
		
			data += "&idcolor=" + $( this ).val();
			done = true;
			
		}
			
	});
	
	var url = new URL(window.location);
	var utm_source = url.searchParams.get("utm_source");
	if(utm_source) data+="&utm_source=" + utm_source;

	$('#waiting-block').fadeIn();

	$.ajax({
			
		url: "/catalog/basket.php?exit&" + cartType,
		async: true,
		type: "GET",
        contentType: "application/x-www-form-urlencoded;charset=utf-8",
		data: data,
		error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter la s\351lection au panier" ); },
		success: function( responseText ){
			$('#waiting-block').fadeOut();
			/* retourne le nombre de produit dans le panier */
			if( !responseText ){ 
				alert( "Impossible d'ajouter la s\351lection au panier" + responseText );
				return;
			}
			var jsonResponse = $.parseJSON(responseText);
			/* onglet panier *//*mise a jour panier*/
			$( "small.BasketItemCount" ).html( "(" + jsonResponse.ItemCount + ")" );
			$( "span.BasketItemCount" ).html( jsonResponse.ItemCount );
			$( "span.total" ).find("span").html( jsonResponse.TotalET + ' \u20AC' );
			$( "#commandeI" ).attr( "class", "commandeI" );

			/* confirmation de l'ajout - si devis redirection vers panier devis */
			if(cartType == "estimate" ) {
			
				// window.location = "/catalog/estimate_basket.php";
				var cartObjectSelector = "#CartAddConfirmation_" + idarticle;

				var popo = $(cartObjectSelector).dialog({ autoOpen: false });
				
				if( $(cartObjectSelector).length == 0 )
					cartObjectSelector = "#CartAddConfirmation";
				
				$( cartObjectSelector ).find( "#CartRedirectionButton" ).attr( "class", cartType == "estimate" ? "devis_acheter" : "commande_acheter" );
				$( cartObjectSelector ).find( "#CartRedirectionButton" ).attr( "href", "/catalog/estimate_basket.php" );
				$( cartObjectSelector ).find( "#CartRedirectionButton" ).text( "visualiser ma demande" );
				$( "#CartAddConfirmation .panierConfirmDevis span.h3" ).text( "Vous venez d'ajouter à votre panier devis :" );
		
				$( cartObjectSelector ).dialog({
					bgiframe: true,
					modal: true,
					resizable: false,
					dialogClass: 'noTitleBar',
					width:499,
					/*height:260,*/
					/*overlay: {
						backgroundColor: '#000000',
						opacity: 0.5
					},*/
					close: function(event, ui) { popo.dialog( "destroy" ); }

				});
				$('#ui-id-6').html("Vous venez d'ajouter à votre panier devis :");	
			
			} else {
				var cartObjectSelector = "#CartAddConfirmation_" + idarticle;

				var popo = $(cartObjectSelector).dialog({ autoOpen: false });
				
				if( $(cartObjectSelector).length == 0 )
					cartObjectSelector = "#CartAddConfirmation";
				
				$( cartObjectSelector ).find( "#CartRedirectionButton" ).attr( "class", cartType == "estimate" ? "devis_acheter" : "commande_acheter" );
				$( cartObjectSelector ).find( "#CartRedirectionButton" ).text( "Accéder à mon panier" );
				$( "#CartAddConfirmation .panierConfirmDevis span.h3" ).text( "Vous venez d'ajouter à votre panier :" );
		
				$( cartObjectSelector ).dialog({
					bgiframe: true,
					modal: true,
					resizable: false,
					dialogClass: 'noTitleBar',
					width:499,
					/*height:260,*/
					/*overlay: {
						backgroundColor: '#000000',
						opacity: 0.5
					},*/
					close: function(event, ui) { popo.dialog( "destroy" ); }		
				});
				console.log('cartObjectSelector',cartObjectSelector);
				$( cartObjectSelector ).dialog("open");
				$('#ui-id-6').html("Vous venez d'ajouter à votre panier:");
			}
			/* fin de si */





		}

	});

}

/*************** custom  ****************/
function customDialog(blockSelected,title){
    $(blockSelected).dialog({
        title:title,
        bgiframe: true,
        modal: true,
        width:550,
        minHeight:10,
        closeText: "Fermer",
        /*overlay: {
        backgroundColor: '#000000',
        opacity: 0.5
        },*/
        close: function(event, ui) { $(blockSelected).dialog( "destroy" ); }
    });

}
/***************************************/
/* --------------------------------------------------------------------------------------------- */

function updateCartPanel(){

}

/* --------------------------------------------------------------------------------------------- */

var articles = new Array();
var formParameters = new Array();

function isNumber( str ){
	
	var validChars = "0123456789.";
	var isNumber = true;
	var char;
	
	for( i = 0; i < str.length && isNumber; i++ ){ 
		   
		char = str.charAt( i ); 
		if( validChars.indexOf( char ) == -1 ) 
		isNumber = false;
	
	}
		   
	return isNumber;
   
}

function selectArticle( idarticle, tableId ){

	if( !idarticle )
		return;
		
		
	$( "#myCaract" ).html( $( "designation" + idarticle).val() );
	$( "#contentPrice" ).css( "display", "block" );

	if ( $("#stockLevel" + idarticle).val() == 0 ) {
		$(".orderOptions").hide();
		$("#infoStockMin").css("display", "block");
	}
	else {
		$(".orderOptions").show();
		$("#infoStockMin").css("display", "none");
	}

	if( $( "#discountRate" + idarticle ).length > 0 ){
		
		$("div.htttc").css('display','none !important').removeClass('infos_prix2');
		$(".htttc-" + idarticle ).show().addClass('infos_prix2');
		$( "#pxTarrif" ).html( $( "#prixtotal" + idarticle ).val() );
		$( "#htPrice" ).html( $( "#prixtotalremise" + idarticle ).val() );
		//$( "#promoRate" ).html( $( "#discountRate" + idarticle ).val() );
		//$( "#myDelai" ).html( "Disponible sous " + $( "#deliveryTime" + idarticle ).val() );
		
		//$( "#promoRate" ).css( "display", "block" );
		//$( "#txtPromo" ).css( "display", "block" );
		$( "#txtPTarrif" ).css( "display", "block" );
		$( "#pxTarrif" ).css( "display", "block" );
		$( "#pxTarrif" ).css( "text-decoration", "line-through" );
		$( "#promoRate" ).css( "display", "block" );
		$( "#AddCde" ).css( "display", "block" );

	}
	else if( $( "#hidecost" + idarticle ).val() == 0 && $( "#prixtotal" + idarticle ).val() != "" ){
				
		$( "#min" ).css( "display", "block" );
		$( "#htPrice" ).html( $( "#prixtotal" + idarticle ).val() );
		//$( "#myDelai" ).html( "Disponible sous " + $( "#deliveryTime" + idarticle ).val() );
		$( "#AddCde" ).css( "display", "block" );
		
	}
	else{
		$("div.htttc").removeAttr('style').addClass('infos_prix2');
		$( "#htPrice" ).html( '<a href="#" class="contactUs" >Contactez-nous</a> ' );
		//$( "#myDelai" ).html( "Disponible sous " + $( "#deliveryTime" + idarticle ).val() );
		$( "#AddCde" ).css( "display", "block" );
		$( "#AddLD" ).css( "display", "block" );
		
	}
	
}

/* --------------------------------------------------------------------------------------------- */

function selectColor( idcolor, tableId ){

	if( !idcolor )
		return;

	/*todo : pr\351voir d'afficher le suppl\351ment */
	
}

/* --------------------------------------------------------------------------------------------- */

function getCartBlock( productId, productTitle, productImageURI ){
	var str = 	'<div id="CartAddConfirmation_' + productId  + '" style="display: none;">' +
				'	<div class="borderConfirmDevis">' +
				'		<div class="bgContentDevis">' +
				'			<div class="contentConfirmCommande">' +
				'				<div class="spacer"></div>' +
				'				<br />' +
				'				<div class="panierConfirmDevis">' +
				'					<h1>Vous venez d\'ajouter &agrave; votre panier :</h1>' +
				'					<div class="spacer"></div>' +
				'					<table class="tableConfirmDevis" cellspacing="0" cellpadding="0" border="0">' +
				'						 <tr>' +
				'							<td class="img"><img src="' + productImageURI + '" onclick="$(\'#CartAddConfirmation_' + productId + '\').dialog(\'destroy\');" width="70" /></td>' +
				'							<td class="txt">' + productTitle + '</td>' +
				'						</tr>' +
				'					</table>' +
				'					<a href="#" onclick="$( \'#CartAddConfirmation_' + productId + '\' ).dialog( \'destroy\' ); return false;" class="btn_continue">Poursuivre ma visite</a>' +
				'					<a id="CartRedirectionButton" href="' + baseURL + '/catalog/basket.php" class="btn_send" >Acc&eacute;der &agrave; mon panier</a>' +
				'					<div class="spacer"></div>' +
				'					<br />' +
				'				</div>' +
				'			</div>' +
				'		</div>' +
				'		<img src="{start_url}/templates/catalog/images/bottomPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />' +
				'		<div class="spacer"></div>' +
				'	</div>' +
				'</div>';

	/*
	var head = document.getElementsByTagName("head")[0];
	var script = document.createElement("script");
	script.innerHTML = str;
	// Handle Script loading
	head.appendChild(script);
	*/

	return str;
}



$('input.form-comment__valid').click(function(e){
	e.stopPropagation();
	e.preventDefault();
	checkIfConnected(function(user){
		console.log("user",user.iduser);
		if(user.iduser > 0){
			addCommentaire($('select.select_product').val(), $('.form-comment__wrapper > textarea:nth-child(3)').val(),user);
		}else{
			alert("connectez vous s'il vous plaît!");
		}
	});
	
});
function addCommentaire(nbStar, commentaire, user){

var url = location.href;
	$.ajax({
		url: url,
		type: "POST",
		data: { 
			addCommentaire: true,
			rating: nbStar, 
			comment: commentaire,
			name:  user.firstname + " " + user.lastname,
			sur_name: "",
			user_mail: "",
			user_city: "",
			  },
		success: function(response){
			console.log("response",response);
			$('#comentaires').prepend(response);
			$('#commentEmpty').remove();
		}
	});
}

function checkIfConnected(callback){

	var url = location.href;
	$.ajax({
		url: url,
		type: "POST",
		data: {check_user_connected : true},
		success: function (response){
			console.log('response',response);
			var res = JSON.parse(response);
			if(res && res.iduser > 0){
				callback(res);
			}else{
				alert("connectez vous s'il vous plaît!");
			}

		}
	});

}

//$.noConflict();
$(window).load(function() {
       $.getScript('js/social.js', function() {});
       $.getScript("/templates/catalog/js/jquery/jquery-ui-1.8.4.custom.min.js",function(){

       	$('input.datepicker').datepicker({
       		altFormat: "dd/mm/yyyy"
       	});
       });

$("input[name='phonenumber'],.number").keypress(function(event){
       if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
           event.preventDefault(); //stop character from entering input
       }

   });

    });

$( "#tabs" ).tabs();

$('.nav-link').click(function(e){
	openTab(e, $(this).attr('href'));
});


function openTab(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tab-content__item");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("nav-link");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  console.log("cityName",cityName);
  //document.getElementById(cityName).style.display = "block";
  $(cityName).css("display","block");
  evt.currentTarget.className += " active";
} 


/* --------------------------------------------------------------------------------------------- */