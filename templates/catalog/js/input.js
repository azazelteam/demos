
$(function () {
	$(".input_identifiant").watermark("E-mail");
	$(".input_email").watermark("Adresse email");
	$(".input_password").watermark("Mot de passe");
	$(".input_conf_password").watermark("Confirmer mot de passe");
	$(".input_old_password").watermark("Ancien mot de passe");
	$(".input_tva_intra").watermark("TVA Intracomm.");
	

	$(".input_title").watermark("Titre");
	$(".input_nom").watermark("Nom");
	$(".input_prenom").watermark("Prénom");
	$(".input_fax").watermark("Fax");
	$(".input_phone").watermark("Téléphone");
	$(".input_gsm").watermark("GSM");
	
	$(".input_company").watermark("Entreprise");
	$(".input_comments").watermark("Commentaire");
	$(".input_question").watermark("Précisez votre demande");
	
	$(".input_adress").watermark("Adresse");
	$(".input_adresscomp").watermark("Adresse complémentaire");
	$(".input_cp").watermark("Code postal");
	$(".input_city").watermark("Ville");
	
	$(".input_siret").watermark("Siret");
	$(".input_naf").watermark("NAF");
	$(".input_subject").watermark("Sujet");
	
	//bug $(".input_voucher").watermark("Vous bénéficiez d'un code avantage ?");
	
	// simpletooltip renvoie l'objet précédemment sélectionné, selon la convention jQuery.
	$("a.tooltip").simpletooltip();
	
	
});
