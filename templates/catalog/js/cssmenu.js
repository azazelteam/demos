function sfHover(){

	var sfEls = document.getElementById("cssmenu").getElementsByTagName("li");
	for (var i=0; i<sfEls.length; i++) {
		sfEls[i].onmouseover=function() {
			this.className+=" sfhover";
		}
		sfEls[i].onmouseout=function() {
			this.className=this.className.replace(new RegExp(" sfhover\\b"), "");
		}
	}
	
}

function cssMenu( website ){

	var cssMenuXHR = null;
   
    if( window.XMLHttpRequest )
        cssMenuXHR = new XMLHttpRequest();
    else if( window.ActiveXObject ){ // Internet Explorer
        
        try{
        
            cssMenuXHR = new ActiveXObject( 'Msxml2.XMLHTTP' );
       
        }
        catch( e ){
        
            try{
            
                cssMenuXHR = new ActiveXObject( 'Microsoft.XMLHTTP' );
            }
            catch( e1 ){
            
                cssMenuXHR = null;
                
            }
            
        }
        
    }
    else{ // XMLHttpRequest non support� par le navigateur
    
        return;
    
    }
    			
	if( cssMenuXHR == null )
		return;

	var url = website + '/www/ajax_cssmenu.php';

	cssMenuXHR.onreadystatechange = function(){
	
		if ( cssMenuXHR.readyState == 4 && cssMenuXHR.status == 200 ){
		
			if( !cssMenuXHR.responseText )
				return;
			
			var cssMenuDiv = document.getElementById( 'CSSMenuDiv' );
			if( cssMenuDiv == null )
				return;
				
			cssMenuDiv.innerHTML = cssMenuXHR.responseText;
			sfHover();
			
		}
		
	}
	
	cssMenuXHR.open( 'GET', url, true );
	cssMenuXHR.send( null );
	
}