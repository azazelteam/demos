<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Oubli mot de passe</title>
	<link rel="stylesheet" href="{baseURL}/css/catalog/defaultsite.css" />
	
	<script src="{baseURL}/js/jquery/jquery-1.3.2.js" type="text/javascript"></script>
	
	<script src="{baseURL}/js/jquery.watermark.js" type="text/javascript"></script>
	<script src="{baseURL}/js/input.js" type="text/javascript"></script>
	<script src="{baseURL}/js/jquery.simpletooltip.js" type="text/javascript"></script>
	<script src="{baseURL}/js/jquery/jquery.form.js" type="text/javascript"></script>
	
</head>
<body>

<div style="padding:5px;">
{if:iduser}<span style="color:red;font-weight: bold;">/forget_mail.tpl</span>{end:}
{if:send}
	<p class="alert">{message:h}<br/>
	<a href="javascript:window.close();" class="lien">Fermer</a></p>
	<div class="spacer"></div>
{else:}
	<form action="{baseURL}/catalog/{ScriptName}" method="post" name="Form_logon" enctype="multipart/form-data">
		<p style="font-size:12px;">Vous avez oublié votre login ou mot de passe ?<br/>
		Saisissez votre adresse e-mail :</p>
		<input type="text" name="Email" value="{email}" class="loginFieldMedium input_email" />
		<input type="image" name="Envoyer" src="{start_url}/www/icones/ok.gif" />
	</form>
{end:}
</div>
<flexy:include src="google_analytics.tpl"></flexy:include>
</body>
</html>

</html>