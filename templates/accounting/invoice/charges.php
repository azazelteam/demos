<?php

//-----------------------------------------------------------------------------------------------------

function displaySupplierCharges( $idsupplier ){
	
	global 	$Session,
			$Order,
			$disabled;
	
	$db = &DBUtil::getConnection();

	if( empty( $idsupplier ) ){
	
		?>
		<p class="msg" style="text-align:center;"><?php  echo Dictionnary::translate("gest_com_supplier_unknown") ; ?></p>
		<?php
		
		return;	
		
	}
	
	$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
	$isInternalSupplier = $idsupplier == $internal_supplier;
	$idstate = $Order->getSupplierIdState( $idsupplier );
	$transit = $Order->getSupplierTransit( $idsupplier );
	$name = GetSupName($idsupplier);
	
	if( $isInternalSupplier )
		$title = "Dépôt -&gt; Client";
	else if( $transit )
		$title = "Fournisseur -&gt; Dépôt";
	else $title = "Livraison directe par le fournisseur $name";
	
	$weight = $Order->getSupplierWeight( $idsupplier );
	$buyingAmount = $Order->getSupplierBuyingAmount( $idsupplier );
	$orderAmount = $Order->getSupplierOrderAmount( $idsupplier );
	$franco = $Order->getSupplierFranco( $idsupplier );
	$charges = $Order->getSupplierCharges( $idsupplier );
	
	//marges
	
	$discount_price = 0.0;
	$unit_cost_amount = 0.0;
	$unit_price = 0.0;
	$quantity = 0;
	$i = 0;
	$hasExternalStock = false;
	while( $i < $Order->getItemCount() ){
		
		$ref_supplier = $Order->getArticleValue( $i, "idsupplier" );
		$ref_idstate = $Order->getSupplierIdState( $ref_supplier );
		
		if( $ref_supplier == $idsupplier ){
			
			$ref_unit_cost_amout = $Order->getArticleValue( $i, "unit_cost_amount" );
			$ref_discount_price = $Order->getDiscountPrice( $i );
			$ref_unit_price = $Order->getArticleValue( $i, "unit_price" );
			
			if( $idsupplier == $internal_supplier )
				$ref_quantity = $Order->getArticleValue( $i, "quantity" );
			else $ref_quantity = $Order->getArticleValue( $i, "external_quantity" );
			
			$hasExternalStock |= ( $idsupplier != $internal_supplier ) && $ref_quantity > 0;
			
			$quantity += $ref_quantity;
			
			$unit_cost_amount += 	$ref_quantity * $ref_unit_cost_amout;
			$unit_price += 			$ref_quantity * $ref_unit_price;
			$discount_price += 		$ref_quantity * $ref_discount_price;

		}

		$i++;
			
	}
	
	//ne pas afficher la ligne si tous les articles sont en stock interne et que le fournisseur n'est pas le fournisseur interne
	
	if( !$hasExternalStock && !$isInternalSupplier)
		return;
		
	//marge brute
	
	$mb0_rate = $unit_price ? ( 1 - ( $unit_cost_amount / $unit_price ) ) * 100 : "-";
	$mb0_amount =  $unit_price - $unit_cost_amount;
	
	$mb1_rate = $orderAmount ? ( 1 - ( $buyingAmount / $orderAmount ) ) * 100 : "-";
	$mb1_amount = $orderAmount - $buyingAmount;
	
	//frais de port vente (approximation : répartition égale par fournisseur )
	
	$supplierCount = $Order->getSupplierCount();

	//frais de port achat
	
	$supplierCharges = $Order->getSupplierCharges( $idsupplier );

	//marge avec port achat
	
	$mb2_rate = $orderAmount ? ( 1 - ( ( $buyingAmount + $supplierCharges ) / $orderAmount ) ) * 100 : "-";
	$mb2_amount = $orderAmount - $buyingAmount - $supplierCharges;
	
	//marge avec port achat et vente
	
	$mb3_rate = $discount_price ? ( 1 - ( ( $unit_cost_amount + $supplierCharges ) / $discount_price ) ) * 100 : "-"; 
	$mb3_amount = $discount_price  - ( $unit_cost_amount + $supplierCharges );
	
	$idstate = $Order->getSupplierIdState( $idsupplier );
	$transit = $Order->getSupplierTransit( $idsupplier );
			
	?>
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<p style="text-align:left; font-weight:bold; margin-left:10px; color:#000000; font-size:14px;"><?php echo $title ?></p>
	<table class="Infos_Table" cellspacing="0" cellpadding="2" border="1" style="width:100%; margin:10px;">
			<?php
			
			if( !$isInternalSupplier ){
				
				?>
				<th><?php echo Dictionnary::translate("gest_com_buying_amount") ; ?></th>
				<th><?php echo Dictionnary::translate("gest_com_selling_amount") ; ?></th>
				<?php
				
			}
			
			
			?>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_strock") ; ?></th>
			<?php 
			if( !$isInternalSupplier && !$transit ){
				
				?>
				<th><?php  echo Dictionnary::translate("gest_com_supplier_franco") ; ?></th>
				<?php
				
			}
			?>
			<th<?php  if( $transit ) echo " colspan=\"2\""; ?>><?php  echo Dictionnary::translate("gest_com_charge_buy") ; ?></th>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buy") ; ?><?php if( $transit ) echo " " . Dictionnary::translate("import"); ?></th>
			<?php
			
			if( !$transit ){
				
				?>
				<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buyand_sell") ; ?></th>
				<?php
				
			}
			
			?>
		</tr>
		<tr>
		<?php
		
			if( !$isInternalSupplier ){
				
				?>
				<td><?php echo Util::priceFormat( $buyingAmount ) ?></td>
				<td><?php echo Util::priceFormat( $orderAmount ) ?></td>
				<?php
				
			}
		
			
			?>
			<td><?php echo Util::priceFormat( $mb1_amount ) ?></td>
			<td><?php echo Util::numberFormat( $mb1_rate ) ?> %</td>
			<?php
			if( !$isInternalSupplier && !$transit ){
				
				?>
				<td>
				<?php 
				
				if( $franco == -1 )
					echo Dictionnary::translate( "gest_com_always_franco" );
				else echo $franco > 0.0 ? Util::priceFormat( $franco ) : "-" 
				
				?>
				</td>
				<?php
				
			}
			
			$charges_auto = $Order->hasAutomaticSupplierCharge( $idsupplier );
			$buying_amount = $Order->getSupplierBuyingAmount( $idsupplier );
			
			if( $transit ){
				
				if( $charges_auto )
					$charge_rate = $Order->getSupplierChargeRate( $idsupplier );
				else $charge_rate = $supplierCharges / $buying_amount * 100.0;

				?>
				<td><?php echo Util::numberFormat( $charge_rate ) ?> %</td>
				<?php
				
			}
			
			$auto_color = 	"#7243B7";
			$manual_color = "#0036FF";
			$input_color = 	$charges_auto ? $auto_color : $manual_color;
			
			?>
			<td>
				<input type="radio" name="SupplierChargesAuto_<?php echo $idsupplier ?>" value="1" id="SupplierChargesAuto_<?php echo $idsupplier ?>[]" onclick="setSupplierChargesAuto( <?php echo $idsupplier ?>, '<?php echo Util::priceFormat( $Order->getSupplierCharges( $idsupplier, true ) ) ?>', '<?php echo $auto_color ?>' );"<?php if( $charges_auto ) echo " checked=\"checked\""; ?> />
				<span style="font-weight:bold; color:<?php echo $auto_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_fixed") ; ?></span>
				&nbsp;<input type="radio" name="SupplierChargesAuto_<?php echo $idsupplier ?>" value="0" id="SupplierChargesAuto_<?php echo $idsupplier ?>[]" onclick="restoreSupplierCharges( <?php echo $idsupplier ?>, '<?php echo $manual_color ?>' );"<?php if( !$charges_auto ) echo " checked=\"checked\""; ?> />
				<span style="font-weight:bold; color:<?php echo $manual_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_calculate") ; ?></span>
				<br />
				<input type="text" name="supplier_charges_<?php echo $idsupplier ?>" value="<?php echo Util::priceFormat( $supplierCharges ) ?>" size="8"<?php if( $charges_auto ) echo " disabled=\"disabled\""; ?> style="font-weight:bold; color:<?php echo $input_color ?>" />
				<input type="submit" name="UpdateSupplierCharges_<?php echo $idsupplier ?>" value="ok" />
			</td>
			<td style="font-weight:bold; color:#FF0000;"><?=GenerateHTMLHelp('1','Calculée uniquement sur la quantité externe'); ?><?php echo Util::priceFormat( $mb2_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo Util::numberFormat( $mb2_rate ) ?> %</td>
			<?php
			
			if( !$transit ){
				
				?>
				<td style="font-weight:bold; color:#FF0000;"><?php echo Util::priceFormat( $mb3_amount ) ?></td>
				<td style="font-weight:bold; color:#FF0000;"><?php echo Util::numberFormat( $mb3_rate ) ?> %</td>
				<?php
				
			}
			
			?>
		</tr>
	</table>
	<?php
	
	$error = $Order->getSupplierError( $idsupplier );
	if( !empty( $error ) )
		echo "<p style=\"text-align:center;font-weight:normal;\" class=\"msg\">$error</p>";
		
}

//-----------------------------------------------------------------------------------------------------

function displayInternalSupplierCharges( $idsupplier ){
	
	global 	$Session,
			$Order,
			$disabled;
	
	$db = &DBUtil::getConnection();

	$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
	$isInternalSupplier = true;
	$idstate = $Order->getSupplierIdState( $idsupplier );
	
	$title = "Dépôt -&gt; Client";
	
	$weight = $Order->getInternalSupplierWeight( $idsupplier );
	$buyingAmount = $Order->getInternalSupplierBuyingAmount( $idsupplier );
	$orderAmount = $Order->getInternalSupplierOrderAmount( $idsupplier );
	
	//note : pas de franco pour le fournisseur interne
	
	$internalSupplierCharges = $Order->getInternalSupplierCharges( $idsupplier );
	$supplierCharges = $Order->getSupplierCharges( $idsupplier );
	
	//marges
	
	$unit_cost_amount = 0.0;
	$unit_price = 0.0;
	$quantity = 0;
	$discount_price = 0.0;
	$i = 0;
	while( $i < $Order->getItemCount() ){
		
		$ref_supplier = $Order->getArticleValue( $i, "idsupplier" );
		$ref_idstate = $Order->getSupplierIdState( $ref_supplier );
		
		if( $ref_supplier == $idsupplier ){

			$ref_unit_cost_amount = $Order->getArticleValue( $i, "unit_cost_amount" );
			$ref_unit_price = $Order->getArticleValue( $i, "unit_price" );
			$ref_discount_price = $Order->getDiscountPrice( $i );
		
			$ref_quantity = $Order->getArticleValue( $i, "quantity" );
			$quantity += $ref_quantity;
			
			$unit_cost_amount += 	$ref_quantity * $ref_unit_cost_amount;
			$unit_price += 			$ref_quantity * $ref_unit_price;
			$discount_price += 		$ref_quantity * $ref_discount_price;
		
		}
		
		$i++;
			
	}
	
	//marge brute
	
	$mb0_rate = $unit_price ? ( 1 - ( $unit_cost_amount / $unit_price ) ) * 100 : "-";
	$mb0_amount =  $unit_price - $unit_cost_amount;
	
	$mb1_rate = $orderAmount ? ( 1 - ( $buyingAmount / $orderAmount ) ) * 100 : "-";
	$mb1_amount = $orderAmount - $buyingAmount;
	
	//frais de port vente (approximation : répartition égale par fournisseur )
	
	$supplierCount = $Order->getSupplierCount();
	
	//marge avec port achat
	
	$mb2_rate = $orderAmount ? ( 1 - ( ( $buyingAmount + $supplierCharges ) / $orderAmount ) ) * 100 : "-";
	$mb2_amount = $orderAmount - $buyingAmount - $supplierCharges;
	
	//marge avec port achat et vente
	
	$mb3_rate = $discount_price ? ( 1 - ( ( $unit_cost_amount + $supplierCharges + $internalSupplierCharges ) / $discount_price ) ) * 100 : "-"; 
	$mb3_amount = $discount_price - ( $unit_cost_amount + $supplierCharges + $internalSupplierCharges );
	
	$idstate = $Order->getSupplierIdState( $idsupplier );
	
	$weight = $Order->getInternalSupplierWeight( $idsupplier );
	$buyingAmount = $Order->getInternalSupplierBuyingAmount( $idsupplier );
	$orderAmount = $Order->getInternalSupplierOrderAmount( $idsupplier );
	
	$charges_auto = $Order->hasAutomaticInternalSupplierCharge( $idsupplier );
		
	?>
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<p style="text-align:left; font-weight:bold; margin-left:10px; color:#000000; font-size:14px;"><?php echo $title ?></p>
	<table class="Infos_Table" cellspacing="0" cellpadding="2" border="1" style="width:100%; margin:10px;">
			<!-- 
			<td style="background-color:#E0E0E0;">Poids</td>
			<td style="background-color:#E0E0E0;">Montant Achats</td>
			<td style="background-color:#E0E0E0;">Montant Cde</td>
			-->
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_strock") ; ?></th>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buy") ; ?><?php if( $idstate ) echo " Import"; ?></th>
			<th><?php  echo Dictionnary::translate("gest_com_charge_resailer") ; ?></th>
			<th colspan="2"><?php  echo Dictionnary::translate("gest_com_rough_after_charge_buyand_sell") ; ?></th>
		</tr>
		<tr>
			<!--
			<td style="background-color:#E0E0E0;"><?php echo Util::numberFormat( $weight ) ?></td>
			<td style="background-color:#E0E0E0;"><?php echo Util::priceFormat( $buyingAmount ) ?></td>
			<td style="background-color:#E0E0E0;"><?php echo Util::priceFormat( $orderAmount ) ?></td>
			-->
			<?php

			$auto_color = 	"#7243B7";
			$manual_color = "#0036FF";
			$input_color = 	$charges_auto ? $auto_color : $manual_color;
			
			?>
			<td style="font-weight:bold; color:#FF0000;"><?php echo Util::priceFormat( $mb1_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo Util::numberFormat( $mb1_rate ) ?> %</td>
			<td style="font-weight:bold; color:#FF0000;"><?=GenerateHTMLHelp('2','Marge sur quantité externe + Marge sur quantité interne'); ?><?php echo Util::priceFormat( $mb2_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo Util::numberFormat( $mb2_rate ) ?> %</td>
			<td>
				<input type="radio" name="InternalSupplierChargesAuto_<?php echo $idsupplier ?>" value="1" id="InternalSupplierChargesAuto_<?php echo $idsupplier ?>[]" onclick="setInternalSupplierChargesAuto( <?php echo $idsupplier ?>, '<?php echo Util::priceFormat( $Order->getInternalSupplierCharges( $idsupplier, true ) ) ?>', '<?php echo $auto_color ?>' );"<?php if( $charges_auto ) echo " checked=\"checked\""; ?> />
				<span style="font-weight:bold; color:<?php echo $auto_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_contractual") ; ?></span>
				<input type="radio" name="InternalSupplierChargesAuto_<?php echo $idsupplier ?>" value="0" id="InternalSupplierChargesAuto_<?php echo $idsupplier ?>[]" onclick="restoreInternalSupplierCharges( <?php echo $idsupplier ?>, '<?php echo $manual_color ?>' );"<?php if( !$charges_auto ) echo " checked=\"checked\""; ?> />
				<span style="font-weight:bold; color:<?php echo $manual_color ?>"><?php  echo Dictionnary::translate("gest_com_buy_calculate") ; ?></span>
				<br />
				<input type="text" name="internal_supplier_charges_<?php echo $idsupplier ?>" value="<?php echo Util::priceFormat( $internalSupplierCharges ) ?>" size="8"<?php if( $charges_auto ) echo " disabled=\"disabled\""; ?> style="font-weight:bold; color:<?php echo $input_color ?>" />
				<input type="submit" name="UpdateInternalSupplierCharges_<?php echo $idsupplier ?>" value="ok" />
			</td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo Util::priceFormat( $mb3_amount ) ?></td>
			<td style="font-weight:bold; color:#FF0000;"><?php echo Util::numberFormat( $mb3_rate ) ?> %</td>
		</tr>
	</table>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------
function GenerateHTMLHelp( $id,$help ){
	
	global $GLOBAL_START_URL;
	
	$html="<div onMouseOver=\"document.getElementById('pop$id').className='tooltiphover';\" onMouseOut=\"document.getElementById('pop$id').className='tooltip';\" class='tooltipa'><img class=\"helpicon\" src=\"$GLOBAL_START_URL/images/back_office/content/icon_help.gif\" alt=\"\" border=\"0\" />";
	
	$html.="<div id='pop$id' class='tooltip' style='width:150px;'>".$help."</div></div>";
	
	return $html;
	
}
?>