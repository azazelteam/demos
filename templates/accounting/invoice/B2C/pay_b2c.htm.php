<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "UpdateIdPayment" );
anchor( "UpdatePaymentDel" );
anchor( "UpdateFactor" );
anchor( "UpdateNOrder" );
anchor( "UpdateBalanceAmount" );
anchor( "UpdateBalanceDate" );
anchor( "UpdateBillingComment" );

//----------------------------------------------------------------------------
//partie éditable si la commande n'a pas été facturée, payée ou annulée

if( $Order->get( "status" ) == Invoice::$STATUS_PAID )
		$payment_disabled = " disabled=\"disabled\"";
else 	$payment_disabled = "";

//----------------------------------------------------------------------------

?>
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div><div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Paiement et échéance</p>
                <div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
			</div>
			 <div class="tableContainer">
				<table class="dataTable devisTable">
                    <tr>
                        <th rowspan="2">Mode</th>
                        <th rowspan="2">Délai de<br />livraison</th>
                        <th rowspan="2" style="width:100px;">Echéance de<br />règlement</th>
                        <th rowspan="2" style="width:75px;">Vos réf. de<br/>commande</th>
                        <th rowspan="2">Date de créa.<br />interne</th>
                        <th colspan="2">Avoir</th>
                    </tr>
        			<tr>
                    	<th>Dispo.</th>
                    	<th>Utilisé</th>
                    </tr>
        			<tr>
        				<td>
        				<?php 
        				
        					/*mode de paiement*/
        					
							$idpayment=$Order->get( 'idpayment' );

							if( empty( $idpayment ) )
								$idpayment = 4;
						
							DrawLift_modepayment( $idpayment ); 
							if( $idpayment==1){
								?>
								<br />
								<a href="<?=$GLOBAL_START_URL?>/accounting/pay_secure.php?IdOrder=<?=$IdOrder;?>" onclick="window.open(this.href); return false;">Lien vers le paiement sécurisé</a>
								<?php 
							}
							
						?>
        				</td>
        				<td>
        				<?php 
        				
        					/*délai de livraison*/
        					
							$idpayment_delay = $Order->get( "idpayment_delay" );
							if( empty( $idpayment_delay ) )
								$idpayment_delay = DBUtil::getParameter( "default_idpayment_delay" );
								
							DrawPaymentDelayList( $idpayment_delay );
						
						?>
        				</td>
        				<td>
        				<?php 
						
							/*Echéance de paiement*/
							
							$echeance = $Order->get( "deliv_payment" );
				
							if( $echeance == "0000-00-00" ){ //aucune date saisie
							
								$echeance = "";
								$startdate = date( "Y-m-d" );
								
							}
							else $startdate = $echeance;
							
							?>
							<input class="calendarInput" type="text" name="deliv_payment" id="deliv_payment" value="<?php echo usDate2eu( $echeance ); ?>" readonly="readonly" />
							<?php DHTMLCalendar::calendar( "deliv_payment" ) ?>
        				</td>
        				<td>
        					<!-- Vos ref de commande (2 colonnes) -->
        					<input class="textInput" type="text" name="n_order" value="<?php echo $Order->get( "n_order" ) ?>" />
        				</td>
        				<td>
        				<?php 
        				
							if( $Order->get( "remote_creation_date" ) != "0000-00-00" )
								$remote_creation_date = usDate2eu( $Order->get( "remote_creation_date" ) );
							else $remote_creation_date = "";
							$startdate = empty( $remote_creation_date ) ? date( "Y-m-d" ) : $Order->get( "remote_creation_date" );
							
							?>
							<input class="calendarInput" type="text" name="remote_creation_date" id="remote_creation_date" value="<?php echo $remote_creation_date ?>" />
							<?php DHTMLCalendar::calendar( "remote_creation_date" ); ?>
        				</td>
        				<td>
        				<?php 
        				
        					/*avoir */
        					
        					$credit_balance = $Order->getCustomer()->get("credit_balance");
        					echo Util::priceFormat($credit_balance);
        					?>
        				</td>
        				<td>
        					<input type="text" class="textInput" style="width:30px;" name="credit_amount" value="<?=Util::numberFormat($Order->get("credit_amount"));?>" />
        				</td>
        			</tr>
            	</table>
				<input type="submit" style="float:left;margin-top:5px;" class="blueButton" value="Modifier" name="SaveOrder" />
       		</div>
			<div class="clear"></div>
		</div>
	</div>
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div>