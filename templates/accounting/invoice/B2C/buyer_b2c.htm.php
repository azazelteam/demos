<script type="text/javascript">
/* <![CDATA[ */
	
	/ PopUp pour voir l'historique d'un client
	fnction seeBuyerHisto()
	{
	potop = (self.screen.height-600)/2;
		poseft = (self.screen.width-1010)/2;
		windw.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0');
	}
	
/* ]]> */
</script>
<?php anchor( "Order2Billing", true );?>
<!-- entete -->
<div class="mainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    <div class="topRight"></div>
    <div class="topLeft"></div>
    <div class="content">
    	<div class="headTitle">
            <p class="title">
            <?php 
            
	            echo $Str_status == "Ordered" ? Dictionnary::translate("gest_com_n_order_confirm") : Dictionnary::translate("gest_com_idbilling_buyer");
	            echo " " . $IdOrder;
            
            ?>
            </p>
            <div class="rightContainer date"><?php echo humanReadableDatetime( $Order->get( "DateHeure" ) ) ?></div>
      		<div class="clear"></div>
        </div><!-- headTitle -->
        <div class="subHeadTitle">
        	<div class="subHeadTitleElementRight">
        		<form name="commercial" action="<?=$GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $IdOrder ?>" method="POST">
	                <b><?php echo Dictionnary::translate( "salesman"); ?></b>
	            	<?php 
	            	
	            		if( User::getInstance()->get("admin") ){
	            			
		            		include_once( dirname( __FILE__ ) . "/../../../../script/user_list.php" );
							userList( true, $Order->get( "iduser" ) );
		            		
		            		?>
		            		<input type="image" name="ComChanged" id="ComChanged" alt="Modifier" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" style="vertical-align:middle; margin-left:5px;" />
		            		<?php
		            		
	            		}
		            	else echo htmlentities( Util::getDBValue( "firstname", "user", "iduser", $Order->get( "iduser" ) ) . " " . Util::getDBValue( "lastname", "user", "iduser", $Order->get( "iduser" ) ) );
		            	
		            ?>
            	</form>
            </div>
            <p class="subHeadTitleElementLeft" style="margin-top:5px;"><b><?php echo Dictionnary::translate( "salesman_buyer"); ?> : </b><?php echo DBUtil::getDBValue( "firstname", "user", "iduser", $Order->getCustomer()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $Order->getCustomer()->get( "iduser" ) ) ?></p>
    		<div class="clear" style="margin-bottom:30px; margin-top:5px;">
                <div style="float:left;">STATUT DE LA FACTURE : <?php echo Dictionnary::translate($Str_status); ?><?=$status_plus?></div>
			</div>
    	</div><!-- subHeadTitle -->
       <div class="subContent">
       		<!-- Coordonnées société -->
        	<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate("gest_com_company_info") ; ?></p>
			</div><!-- subTitleContainer -->
    	  	<div class="tableContainer">
                <table class="dataTable">
                    <tr class="filledRow">
                        <th><?php echo Dictionnary::translate("idbuyer") ?></th>
                        <td>
                        <?php
                        
                        	echo $Order->getCustomer()->get( "idbuyer" );
					 
							if($relaunched){
								
								?>
								<b><a href="#" class="blueLink" onclick=""seeRelaunch(); return false;">!! Attention client relancé !!</a>
								<?php 
							
							} 
							
						?>
                        </td>
                        <th><?php echo str_replace("<br />","",Dictionnary::translate("source")) ?></th>
                        <td><?php echo DBUtil::getDBValue( "source_1", "source", "idsource", $Order->getCustomer()->get( "idsource" ) ) ?></td>
                    </tr>
                    <tr><td colspan="4" class="tableSeparator"></td></tr>
                    <tr class="filledRow">
                        <th><?php echo Dictionnary::translate( "lastname" ) ?></th>
                        <td><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) )  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ) ?></td>
                        <th><?php echo Dictionnary::translate( "firstname" ) ?></th>
                        <td>
							<?php
								$buyerFirstname = $Order->getCustomer()->getContact()->get( "firstname" ) ;
								if( !empty($buyerFirstname)){
									echo $buyerFirstname;
								}
								else{
									echo "-";
								}
							?>
						</td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("adress") ?></th>
                        <td colspan="3"><?php echo $Order->getCustomer()->get( "adress" ) ?></td>
                        
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("adress_2") ?></th>
                        <td colspan="3">
                           <?php $adr2=$Order->getCustomer()->get( "adress_2" );if(empty($adr2)){echo "-";}else{ echo $adr2;} ?> 
                        </td>
                        
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("zipcode") ?></th>
                        <td>
                            <?php echo $Order->getCustomer()->get( "zipcode" ) ?>
                        </td>
                        <th><?php echo Dictionnary::translate("city") ?>&nbsp;/&nbsp;<?php echo Dictionnary::translate("state") ?></th>
                        <td>
                            <?php echo $Order->getCustomer()->get( "city" ) ?>&nbsp;/&nbsp;<?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ) ?>
                        </td>
                    </tr>
                    <tr><td colspan="4" class="tableSeparator"></td></tr>
                    <tr class="filledRow">
                        <th><?php echo Dictionnary::translate("phone") ?></th>
                        <td><?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "phonenumber" )) ?></td>
                        <th><?php echo Dictionnary::translate("gsm") ?></th>
                        <td>
                            <?php
								$buyerGsm = $Order->getCustomer()->getContact()->get( "gsm" );
								if(!empty($buyerGsm)){
									echo $buyerGsm;
								} 
								else{
									echo "-";
								}
							?>
                        </td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("email") ?></th>
                        <td colspan="3">
                            <a href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ) ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ) ?></a>
                        </td>
                	</tr>
                </table>
            </div><!-- tableContainer -->
		</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight">
	</div><div class="bottomLeft"></div>
</div><!-- mainContent -->
<div id="tools" class="rightTools">
<?php
    
    $rsLitigation =& DBUtil::query( "SELECT lb.idlitigation, l.editable FROM litigations_billing_buyer lb , litigations l WHERE lb.idlitigation = l.idlitigation AND lb.idbilling_buyer = '" . $Order->get("idbilling_buyer" ) . "'" );
		    	
	if( $rsLitigation->RecordCount() ){
		
		?>
		<div class="toolBox">
	    	<div class="header">
	        	<div class="topRight"></div><div class="topLeft"></div>
	            <p class="title">Litiges ouverts</p>
	        </div>
	        <div class="content">
	            <p>
	            	<ul style="margin-left:10px;">
		            <?php
		            
		            	while( !$rsLitigation->EOF() ){
		
							?>
							<li>
								<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open( this.href ); return false;">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="vertical-align:middle;" />
									Litige N° <?php echo $rsLitigation->fields( "idlitigation" ) ?> ( <?php echo $rsLitigation->fields( "editable" ) ? "en cours" : "résolu"; ?> )
								</a>
							</li>
							<?php
						
							$rsLitigation->MoveNext();
							
		            	}
		            	
		            ?>
		            </ul>
	            </p>
	        </div>
	        <div class="footer">
	        	<div class="bottomLeft"></div>
	            <div class="bottomMiddle"></div>
	            <div class="bottomRight"></div>
	        </div>
	    </div><!-- toolBox -->
		<?php

	}
	
	/* avoirs client*/
	
	?>
    <div class="toolBox">
    	<div class="header">
        	<div class="topRight"></div><div class="topLeft"></div>
            <p class="title">Avoirs</p>
        </div>
        <div class="content">
			<p><a class="normalLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idbilling_buyer=<?php echo $Order->getId() ?>" taget="_blank">Créer un avoir client</a></p>
        	<?php
        	
        		$rsCredits =& DBUtil::query( "SELECT idcredit FROM `credits` WHERE idbilling_buyer='" . $Order->get("idbilling_buyer") ."'" );
        	
        		if( $rsCredits->RecordCount() ){
        			
        			?>
		        	<div class="subTitleContainer" style="margin-top:0;">
		            	<p class="subTitle">Liste des avoirs client créés</p>
		            </div>
		        	<p>
		        	<ul style="margin-left:10px;">
		        		<?php
		        		
		        			while( !$rsCredits->EOF() ){
		        				
		        				?>
		        				<li>
		        					<a class="normalLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $rsCredits->fields( "idcredit" ) ?>">Avoir Client n°<?php echo $rsCredits->fields( "idcredit" ) ?></a>
		        				</li>
		        				<?php
		        				
		        				$rsCredits->MoveNext();
		        				
		        			}
		        			
		        		?>
		        		</ul>
		        	</p>
		        	<?php
		        	
        		}
        		
        	?>
        </div><!-- content -->
        <div class="footer">
        	<div class="bottomLeft"></div>
            <div class="bottomMiddle"></div>
            <div class="bottomRight"></div>
        </div>
    </div><!-- toolBox -->
    <div class="toolBox">
    	<div class="header">
        	<div class="topRight"></div>
        	<div class="topLeft"></div>
            <p class="title">Visualiser et imprimer</p>
        </div>
        <div class="content" style="padding:5px;">
		<?php
		
			if( !strlen( $Order->getCustomer()->getContact()->get( "mail" ) ) ){
						
				?>
				<div class="subTitleContainer" style="margin-bottom:5px; margin-top:0px;">
					<p class="subTitleWarning"><?php  echo Dictionnary::translate("gest_com_buyer_without_mail_order") ; ?></p>
				</div>
				<?php
				
			}
			else{

				?>
				<p><a class="normalLink" style="display:block;" href="transmit_invoice.php?IdInvoice=<?php echo $Order->getId() ?>" onclick="window.open(this.href); return false;">Mail d'accompagnement</a></p>
				<?php

			}	
		
			?>
			<p><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $Order->getId() ?>" class="normalLink">Facture PDF n°<?php echo $Order->getId() ?></a></p>
        </div><!-- content -->
        <div class="footer">
        	<div class="bottomLeft"></div>
            <div class="bottomMiddle"></div>
            <div class="bottomRight"></div>
        </div>
    </div><!-- toolBox -->
</div><!-- tools -->