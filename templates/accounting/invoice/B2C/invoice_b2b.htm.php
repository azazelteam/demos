<script type="text/javascript">
/* <![CDATA[ */

	function showSupplierInfos( idsupplier){
					
		if( idsupplier == '0' )
			return;

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
            		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
			}

		});
		
	}
	
/* ]]> */
</script>
<a name="references"></a>
<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" />
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="mainContent fullWidthContent">
	<div class="topRight"></div>
	<div class="topLeft"></div>
	<div class="content">
		<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Descriptifs de la facture</p>
			</div><!-- subTitleContainer -->
			<?php

				anchor( "UpdateRefDiscountRate_", true );
				anchor( "UpdateOrderPriceWD_", true );
				anchor( "UpdateUnitCostRate_", true );
				anchor( "UpdateUnitCostAmount_", true );
				anchor( "UpdateDesignation_", true );
				anchor( "UpdateSupplierCharges_", true );
				anchor( "UpdateInternalSupplierCharges_", true ); 
				anchor( "UpdRow_", true ); 
				
				/*lignes articles*/ 
				
				$it = $Order->getItems()->iterator();
				
				while( $it->hasNext() ){
				
					$item =& $it->next();
					
					?>
					<div class="leftContainer" style="width:845px; margin-bottom:10px;">
					<?php
						
					displayItem( $Order, $item );
				
					?>
					</div>
					<div class="rightContainer" style="width:110px;">
						<div class="tableContainer" style="margin-bottom:0px;">
	       					<table class="dataTable devisTable summaryTable">
								<tr>
									<th class="totauxTabHeader">Prix de vente total HT</th>
									<th class="totauxTabHeader">Prix de vente total TTC</th>
								</tr>
								<tr>
									<td><?php echo Util::priceFormat( $item->get( "discount_price" ) * $item->get( "quantity" ) ) ?></td>
									<td><?php echo Util::priceFormat( $item->get( "discount_price" ) * $item->get( "quantity" ) * ( 1.0 + $item->get( "vat_rate" ) / 100.0 ) ) ?></td>
								</tr>
							</table>
						</div>
						<?php displayItemMargins( $item ); ?>
					</div>
					<div class="clear"></div>
					<?php
					
				}
				
			?>
			</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php

//-------------------------------------------------------------------------------------------------------------------

function displayItem( Invoice &$invoice, InvoiceItem &$item ){
	
	global $GLOBAL_START_URL;
	
	?>
	<div class="tableContainer" style="margin-bottom:5px;">
		<table class="dataTable devisTable">
		    <tr>				
				<th style="width:75px;">Photo</th>
				<th>Réf. & Désignation courte</th>
				<th>Quantité</th>
				<th>Prix d'achat Net Unitaire</th>
				<th>Prix de vente Unitaire Net HT</th>
				<th>Prix de vente TTC Unitaire Net</th>
				<th>Remise Client</th>			
				<th>Prix de vente final Net Unitaire</th>
		    </tr>
		    <tr>
				<td rowspan="3"><?php /*image*/ displayReferenceThumb( $item ); ?></td>
				<td style="text-align:left;"><?php displayReferenceInfos( $item ); ?></td>
				<td rowspan="3"><?php echo $item->get( "quantity") ?></td>
				<td><?php echo Util::priceFormat( $item->get( "unit_cost_amount" ) ) ?></td>
				<td><?php echo Util::priceFormat( $item->get( "discount_price" ) ) ?></td>
				<td><?php echo Util::priceFormat( $item->get( "discount_price" ) * $item->get( "quantity" ) * ( 1.0 + $item->get( "vat_rate" ) / 100.0 ) ); ?></td>
				<td>
				<?php
				
					if( DBUtil::getParameterAdmin( "view_ref_discount_add" ) == 0 )
						echo Util::rateFormat( $item->get( "ref_discount" ) );
					else{
						
						?>
						<p><b>Base</b></p>
						<p><?php echo Util::rateFormat( $item->get( "ref_discount_base" ) ) ?></p>
						<p><b>Complémentaire</b></p>
						<p><?php echo Util::rateFormat( $item->get( "ref_discount_add" ) ) ?></p>
						<?php 
						
					}
					
				?>
				</td>
				<td><?php echo Util::priceFormat( $item->get( "discount_price" ) * $item->get( "quantity" ) ) ?></td>
			</tr>
			<tr>
				<td rowspan="2" style="width:210px; height:110px;">
					<div style="width:210px; height:110px; border-style:none; text-align:left;"><?php echo stripDesignationTags( $item ) ?></div>
				</td>
				<th colspan="3">Délai d'exped.</th>
				<th>Poids unitaire</th>
				<th>Poids total</th>
			</tr>
			<tr>
				<td colspan="3">
				<?php 
				
					/*délai de livraison @todo : n'existe pas*/
				
					$lang = User::getInstance()->getLang();
					$deliv_delay = DBUtil::query( "SELECT d.delay$lang AS delay FROM delay d, `order_row` orow WHERE orow.idorder = '" . $invoice->get( "idorder" ) . "' AND orow.idrow = '" . $item->get( "idorder_row" ) . "' AND orow.delivdelay = d.iddelay LIMIT 1" )->fields( "delay" );
					echo htmlentities( $deliv_delay );
					
				?>
				</td>
				<td><?php echo Util::numberFormat( $item->get( "weight" ) ); ?>&nbsp;kg</td>
				<td><?php echo Util::numberFormat( $item->get( "quantity" ) *  $item->get( "weight" ) ) . "&nbsp;kg"; ?></td>
			</tr>
		</table>
	</div><!-- tableContainer -->
	<div class="leftContainer">
    </div>
    <div class="rightContainer">
        <input type="button" onclick="window.open('<?php echo "$GLOBAL_START_URL/statistics/statistics.php?cat=product&reference=" . urlencode( $item->get( "reference" ) ) . "&amp;period&amp;StatStartDate=" . urlencode( date( "d/m/Y", mktime( 0, 0, 0, date( "m" ) - 6, date( "d" ), date( "Y" ) ) ) ) . "&amp;StatEndDate=" . urlencode( date( "d/m/Y" ) ) ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px; margin-right:5px;"/>
   		<input type="button" value="Ouvrir un litige" class="orangeButton" onclick="document.location = '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idbilling_buyer=<?php echo $invoice->get( "idbilling_buyer" ) ?>&idrow=<?php echo $item->get( "idrow" ) ?>';" />
    </div>
    <div class="clear"></div>
    <?php

}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( $idproduct ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" onclick="window.open(this.href); return false;">
		<?php 
		
	}
	
	?>
	<img src="<?php echo URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$item ){
		
	global $GLOBAL_START_URL;
	
	/*référence avant substitution @todo : n'existe pas*/
	
	/*référence catalogue*/
	
	?>
	<p>Référence catalogue : <?php echo $item->get( "reference" ) ?></p>
	 <?php
	 
	 /*référence et infos fournisseur*/
	 
	 if( $item->get( "idsupplier" ) ){
		
		?>
		<p>
			<span class="lightGrayText">Réf. fournisseur : <?php echo DBUtil::getDBValue( "ref_supplier", "detail", "idarticle", $item->get( "idarticle" ) ); ?></span>
			&nbsp;
			<a href="#" onclick="showSupplierInfos( <?php echo $item->get( "idsupplier" ) ?> ); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
			</a>
		</p>
		<?php 

	 }
	
	/*référence alternative @todo : n'existe pas*/
	
	/*lien fiche PDF*/
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) ){
		
		$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $item->get( "idarticle" ) );
		$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : "/catalog/pdf_product.php?idproduct=$idproduct";
		
		?>
		<p>
			<a class="blueLink" href="<?php echo $href ?>" onclick="window.open(this.href); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
			</a>
		</p>
		<?php
			
	}
								 	
}

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){

	$designation = $item->get( "designation" );
	$designation = str_replace("\n",'', $designation);
	$designation = str_replace("\r",'', $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = strip_tags($designation);
	
	return $designation;
		
}

//-----------------------------------------------------------------------------------------------------

function displayItemMargins( &$item ){

	?>
    <div class="tableContainer">
        <table class="dataTable devisTable summaryTable">
            <tr>
				<th>Marge Brute HT</th>
				<th>Marge Brute %</th>
				<th>Coeff.</th>
			</tr>
			<tr>
				<td>
				<?php
			
					/*marge brute HT*/
					
					$mb =  $item->get( "quantity" ) * ( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) );
					echo Util::priceFormat( $mb );
					
				?>
				</td>
				<td>
				<?php
                
                	/*marge finale*/
				
					$mbf = $item->get( "discount_price" ) > 0.0 ? ( 1 - $item->get( "unit_cost_amount" ) / $item->get( "discount_price" ) ) * 100.0 : 0.0;
					echo Util::rateFormat( $mbf );
				
				?>
				</td>
				<td>
				<?php 
			
					/*coefficient de vente brut*/
					
					$rough_sale_coeff = $item->get( "unit_cost" ) > 0.0 ? $item->get( "unit_price" ) / $item->get( "unit_cost" ) : 0.0; 
					echo Util::numberFormat(  $rough_sale_coeff );
					
				?>
				</td>
			</tr>
        </table>
	</div><!-- tableContainer -->
	<?php
	       
}

//-----------------------------------------------------------------------------------------------------

?>