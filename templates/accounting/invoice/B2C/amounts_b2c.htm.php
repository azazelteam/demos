<?php 

anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateTotalDiscountAmount" );
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" ); 
anchor( "UpdateInstallmentRate" ); 
anchor( "UpdateInstallmentAmount" ); 
anchor( "UpdateTotalChargeHT" ); 
anchor( "UpdateChargeSupplierRate" );
anchor( "UpdateChargeSupplierAmount" );
anchor( "UpdatePaymentDelay" );
anchor( "ApplySupplierCharges" );
?>
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Montants et tarifications de la facture</p>
				<div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
            </div>
            <div class="tableContainer">
				<table class="dataTable devisTable">
                    <tr>
	    				<th colspan="2">Remise sur<br/>facture</th>
	    				<th colspan="2">Montant</th>
	    				<th colspan="2">Port</th>
	    				<th colspan="2">Acompte</th>
	    				<th colspan="2">Marge nette après<br />port achat vente</th>
	    				<th rowspan="2">Montant TTC à payer</th>
    				</tr>
    				<tr>
                    	<th>%</th>
                    	<th>&euro;</th>
                    	<th>Net H.T.</th>
                    	<th>T.T.C.</th>
                    	<th>Poids</th>
                    	<th>Port H.T.</th>
                    	<th>%</th>
                    	<th>&euro;</th>
                    	<th>%</th>
                    	<th>&euro;</th>
                    </tr>
					<tr>
						<td><?php echo Util::numberFormat($total_discount_rate) ?></td><td><?php echo Util::priceFormat( $total_discount_amount ) ?></td>
						<td><?php echo Util::priceFormat( $ht ) ?></td>
						<td><?php echo Util::priceFormat( $ttc ) ?></td>
						<td><?php  echo $Order->getWeight(); ?>&nbsp;kg</td>
						<td><?php if($Order->get('total_charge_ht')>0){ echo Util::priceFormat($Order->get('total_charge_ht')); }else echo "Franco"; ?></td>
						<td><?php echo Util::numberFormat( $down_payment_rate ) ?></td><td><?php echo Util::priceFormat( $down_payment_amount ) ?></td>
						<td><?=Util::numberFormat( $Order->get( "net_margin_rate" ));?></td>
						<td><?php echo Util::priceFormat( $Order->get( "net_margin_amount" ) ); ?></td>
						<td><?php echo Util::priceFormat( $Order->getNetBill() ) ?></td>
					</tr>
			</table>
 			</div><!-- tableContainer -->
       	</div><!-- subContent -->
	</div><!-- content -->
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->