<?php 

anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateTotalDiscountAmount" );
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" ); 
anchor( "UpdateInstallmentRate" ); 
anchor( "UpdateInstallmentAmount" ); 
anchor( "UpdateTotalChargeHT" ); 
anchor( "UpdateChargeSupplierRate" );
anchor( "UpdateChargeSupplierAmount" );
anchor( "UpdatePaymentDelay" );
anchor( "ApplySupplierCharges" );

?>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="contentResult" style="margin-bottom: 0;">

	<h1 class="titleEstimate"><span class="textTitle">Montants et tarifications de la facture</span>
	<div class="spacer"></div></h1>

	<div class="blocEstimateResult"><div style="margin:5px;">

         <div class="floatleft" style="margin-bottom:10px; width:89%;">
			<table class="dataTable devisTable">
                <tr>
                	<th>Prix vente<br />	<?php if( B2B_STRATEGY ){?> HT	<?php }else{ ?>
				TTC	<?php } ?></th>
                    <th colspan="2">Remise supplémentaire<br />sur facture</th>
					<th>Total HT</th>
					<th colspan="2">Port vente refacturé</th>
					<th>Total  HT<br />avec port vente</th>
					<th colspan="2">Remise pour<br />paiement comptant</th>
					<th>Total HT</th>
					<th>Taux TVA</th>
					<th>Montant TVA</th>
					<th>Total TTC</th>
				</tr>
                <tr>
 <?php
	            
	            	$articlesET = 0.0;
					$it = $Order->getItems()->iterator();
					while( $it->hasNext() )
						$articlesET += $it->next()->getTotalET();
					
	            	?>
					 <?php
            
            	$articlesATI = 0.0;
				$it = $Order->getItems()->iterator();
				while( $it->hasNext() )
				$articlesATI += $it->next()->getTotalATI();
					
				$i = 0;
				
				$articlesVAT = 0.0;
				while( $i < $Order->getItemCount() ){
				$articlesVAT += ($Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ))*($Order->getItemAt($i)->getVATRate())/100;
				$i++;
				}
				
				?>
				<?php if(B2B_STRATEGY){
	?><td rowspan="3"><?php echo Util::priceFormat( $articlesET ); ?></td><?
}
else{
	?><td rowspan="3"><?php echo Util::priceFormat( $articlesATI ); ?></td><?
	
} ?>

					<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat(  $Order->get( "total_discount" ) ) ?> %</td>
					<td rowspan="3" style="white-space:nowrap;">
					<?php if( B2B_STRATEGY ){?> <?php echo Util::numberFormat( $Order->get( "total_discount_amount" ) ) ?> &euro;	<?php }else{ ?>
					<?php } ?>
					</td>
					<td rowspan="3"><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) ?></td>
					<td colspan="2"><?php echo $Order->get( "total_charge_auto" ) ? "Coût paramétré" : "Coût calculé"; ?></td>
					<td rowspan="3">
					<?php if( B2B_STRATEGY ){?> <b><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET() ) ?></b>	<?php }else{ ?>
				<?php } ?></td>
					<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat(  $Order->get( "billing_rate" ) ) ?> %</td>
					<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat( $Order->get( "billing_amount" ) ) ?> &euro;</td>
					
                    <?php if( B2B_STRATEGY ){
								
				$ht = $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET();        
				
				$vat = $articlesVAT - $articlesVAT * $total_discount_rate/100 + $Order->getChargesET()*$Order->getVATRate()/100;
				
				$ttc = $articlesATI - ($articlesATI*$total_discount_rate/100) + $Order->getChargesATI();  
				
				?>
                
                <!-- HT -->
                <td rowspan="3">
					<b><span id="tht">
				<?php echo Util::priceFormat( $ht ); ?>
					</span></b>
				</td>
                
                <!-- TVA (%) -->
				<td rowspan="3">
					<span id="ttva"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></span>
				</td>
                
                <!-- TVA (€) -->
				<td rowspan="3">
					<span id="mtva"> <?php echo Util::priceFormat( $vat ); ?>
					</span>
				</td>
                				
                <!-- TTC -->
                <td rowspan="3">
					<b><span id="tttc"><?php echo Util::priceFormat($ht + $vat); ?>
					
					</span></b>
				</td>
                
                <?php }else{
                
                 $ht = $articlesET - ( ($articlesET*$total_discount_rate)/100 )+  ( $Order->getChargesATI() / ( 1 + $Order->getVATRate()/100 ) );
                
                $ttc = $articlesATI - ($articlesATI*$total_discount_rate/100) + $Order->getChargesATI();
                 ?>
                <!-- HT -->
                <td rowspan="3">
					<b><span id="tht">
					<?php echo Util::priceFormat( $ht  ); ?>
			
					</span></b>
				</td>
                
                <!-- TVA (%) -->
				<td rowspan="3">
					<span id="ttva"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></span>
				</td>
                
                 <!-- TVA (€) -->
				<td rowspan="3">
					<span id="mtva"><?php echo Util::priceFormat($ttc - $ht ); ?>
					</span>
				</td>
                
                <!-- TTC -->
                <td rowspan="3">
					<b><span id="tttc"><?php echo Util::priceFormat($ttc ); ?></span></b>
				</td>
                <?php
				} 
				?>
                
                
                </tr>
				<tr>
					<th>Poids</th>
                    <?php
					if( B2B_STRATEGY ){
				?>
            	<th>Port HT</th>
                <?php } else { ?>
                <th>Port TTC</th>
                <?php
				} ?>
				</tr>
				<tr>
					<td><?php echo Util::numberFormat( $Order->getWeight() ) ; ?>&nbsp;kg</td>
					<td style="white-space:nowrap;"><?php if( B2B_STRATEGY ){
							
	            				echo $Order->getChargesET() > 0.0 ?  Util::numberFormat( $Order->getChargesET()  ) : "Franco"; 
	            				}else{
								echo $Order->getChargesATI() > 0.0 ?  Util::numberFormat( $Order->getChargesATI()  ) : "Franco";
								} ?></td>
				<tr>
        	</table>
        	<div class="spacer"></div>
		
			<!--<div class="floatright" style="margin-top:5px;">
	        	<input type="submit" class="blueButton" name="SaveOrder" value="Recalculer" />
	        </div>-->
		</div><!-- leftContainer -->
		<div class="floatright" style="width:10%; margin-bottom:10px;">
            <table class="dataTable devisTable summaryTable">
                <tr>
                    <th colspan="2">Marge nette après <br /> Port Achat et Vente</th>
                </tr>
                <tr>
                    <td colspan="2"><?php echo Util::priceFormat( $Order->get( "net_margin_amount" ) ); ?></td>
                </tr>
                <tr>
                    <th>%</th>
                    <th>Coeff.</th>
                </tr>
                <tr>
                    <td style="width:50%"><?php echo Util::rateFormat( $Order->get( "net_margin_rate" ) ) ?></td>
					<td style="width:50%"><?php echo Util::priceFormat( $Order->get( "net_margin_amount" ) ) ?></td>
                </tr>
            </table>
		</div><!-- rightContainer -->
		
        <div class="spacer"></div>
	        
	</div></div><!-- blocEstimateResult -->
	<div class="spacer"></div>
	
</div>