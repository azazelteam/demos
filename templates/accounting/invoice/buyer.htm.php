<?php
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	$customer = new Customer( $Order->get( "idbuyer" ), $Order->get( "idcontact" ) );
?>
<script type="text/javascript">
/* <![CDATA[ */

	// PopUp pour voir l'historique d'un client
	function seeBuyerHisto(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-1010)/2;
		window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0');
		
	}

	// PopUp pour voir les relances du client
	function seeRelaunch(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-750)/2;
		window.open('<?php echo $GLOBAL_START_URL ?>/accounting/buyer_relaunch.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
		
	}

/* ]]> */
</script>
<?php anchor( "Order2Billing", true );?>



<div class="contentResult" style="margin-bottom: 0px;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<h1 class="titleEstimate">
	<span class="textTitle">
		<?php 
            
        	/* litige */
	    	$rsLitigation =& DBUtil::query( "SELECT COUNT(*) AS hasLitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '" . $Order->get("idbilling_buyer" ) . "'" );
	    	if( $rsLitigation->fields( "hasLitigation" ) ){
	    		
	    		?>
	    		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="vertical-align:middle;" />
	    		<span style="color:#EB6A0A;">Facture litigieuse n°<?php echo $Order->getId(); ?></span>
	    		<?php
	    	
	    	}
	    	else{
	    		
	            echo $Str_status == "Ordered" ? "Confirmation de commande client n°" : "Facture n°";
	            echo " " . $IdOrder;
        
	    	}
	    	
	    	$isImpayable = $Order->get( "litigious" );
	    	if($isImpayable){
	    		echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src=\"$GLOBAL_START_URL/images/back_office/content/litigation.png\" alt=\"Litige\" style=\"vertical-align:middle;\" /> <span style=\"color:red;\">Facture impayable</span>";
	    	}
        ?>
	</span>
	<span class="selectDate">
		<p style="font-size:14px;">
			<strong>Statut de la facture : </strong>
			<span style="font-weight:bold; color:#ff0000;">
				<?php echo Dictionnary::translate( $Str_status ) ?>
				<?php echo $status_plus ?>
			</span>
		</p>
	</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>
	
	
	
	<div class="blocEstimateResult" style="margin:0;"><div style="margin:0 5px 5px 5px;">
		<div class="blocMLeft" style="margin-bottom:0px;">
			<p><strong>Crée le</strong> <?php echo humanReadableDatetime( $Order->get( "DateHeure" ) ) ?></p>
			<div class="spacer"></div>
						
			<p><strong><?php echo Dictionnary::translate( "salesman"); ?> : </strong> 
			<?php 
            	/*commercial affaire*/
        		if( User::getInstance()->get("admin") ){
        			
        			include_once( dirname( __FILE__ ) . "/../../../script/user_list.php" );
					userList( true, $Order->get( "iduser" ) );
					
            		//DrawLift_commercial("user",$Order->get( "iduser" ) );
            		
            		?>
            		<input type="image" name="ComChanged" id="ComChanged" alt="Modifier" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" style="vertical-align:middle; margin-left:5px;" />
            		<?php
            		
        		}
            	else echo htmlentities( Util::getDBValue( "firstname", "user", "iduser", $Order->get( "iduser" ) ) . " " . Util::getDBValue( "lastname", "user", "iduser", $Order->get( "iduser" ) ) );
	        ?></p>
			<div class="spacer"></div>
			
			<p><strong><?php echo Dictionnary::translate( "salesman_buyer"); ?> : </strong> 
			<?php echo DBUtil::getDBValue( "firstname", "user", "iduser", $Order->getCustomer()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $Order->getCustomer()->get( "iduser" ) ) ?></p>
			<div class="spacer"></div>
		</div>
		<div class="floatright">
			<p style="text-align: right;"><strong>Commande(s) client : </strong>
			<?php
			// ------- Mise à jour de l'id gestion des Index  #1161
				$cdes = DBUtil::query( "SELECT bl.idorder FROM bl_delivery bl WHERE idbilling_buyer = '$IdOrder' GROUP BY bl.idorder" );
				if( $cdes->RecordCount() ){
					$i = 0;
					while( !$cdes->EOF() ){
						if( $i > 0)
							echo ' - ';
							
						echo "<a style='color:#ed8210;' href='$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=". $cdes->fields( 'idorder' ) ."' onclick='window.open(this.href); return false;'>" . $cdes->fields( 'idorder' ) . '</a>';
												
						$cdes->MoveNext();
					}
				}else{
					echo '<span style="color:#ed8210;">Facture seule</span>';
				}
				// ------- Mise à jour de l'id gestion des Index  #1161
				$cdesFs = DBUtil::query( "SELECT bl.idorder_supplier FROM bl_delivery bl WHERE idbilling_buyer = '$IdOrder' GROUP BY bl.idorder_supplier" );
				if( $cdesFs->RecordCount() ){
				?>
				<br /><strong>Commande(s) fournisseur : </strong>
					<?
					$i = 0;
					while( !$cdesFs->EOF() ){
						if( $i > 0)
							echo ' - ';
							
						echo "<a style='color:#ed8210;' href='$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=". $cdesFs->fields( 'idorder_supplier' ) ."' onclick='window.open(this.href); return false;'>" . $cdesFs->fields( 'idorder_supplier' ) . '</a>';
						echo ' - ';
						$cdesFs->MoveNext();
					}
				} ?>
			</p>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
		
		
		
		
		
		<script type="text/javascript">
		<!--
			//onglets
			$(function() {
				$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
			});
		-->
		</script>
         
        <div id="container">
         
	        <ul class="menu">
				<li class="item"><a href="#infos-client"><span>Client n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?></span></a></li>
				<li class="item"><a href="#visualiser-imprimer"><span>Visualiser et imprimer</span></a></li>
				<li class="item"><a href="#addresses"><span>Adresse de facturation</span></a></li>
				<li class="item"><a href="#commentaires">
					<span>Commentaires</span></a>
					<?php if($Order->get( "billing_comment" )) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				<li class="item"><a href="#contentieux-facture">
					<span>Contentieux sur facture</span></a>
					<?php if($isImpayable) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
			</ul>
			<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0 8px -5px; width: 100%;"> </div>
		
			<?php
				if($isImpayable){
					$dateLit = Util::DateFormat( $Order->get("litigation_date") );
					$commentLit = $Order->get("litigation_comment");
					
					$avai = " disabled='disabled' ";
				}else{
					$dateLit = date("d-m-Y");
					$commentLit = "";
					
					$avai="";
				}
			?>
			<div id="contentieux-facture">
				<div class="blocMultiple blocMLeft">
				<form action="com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" name="litigious_creation" enctype="multipart/form-data" method="post">
					<input type="hidden" name="update_litigious" value="1" />
					
					<label><span>Date de constat :</span></label>
					<input type="text" <?=$avai?> name="litigation_date" id="litigation_date" readonly="readonly" class="calendarInput" value="<?=$dateLit?>" />
					<?php DHTMLCalendar::calendar( "litigation_date" ) ?>
					<div class="spacer"></div> 
					
					<label><span>Commentaire</span></label>
					<textarea <?=$avai?> name="litigation_comment" class="textInput" style="height:60px;"><?=stripslashes($commentLit)?></textarea>
					<div class="spacer"></div> 					
					
					<div style="float:right; margin:5px 0 0 0;">
						<input <?=$avai?> type="submit" class="blueButton" value="Enregistrer" />
					</div>
				</form>
				</div>
			</div>		
		
			<div id="addresses">		
				<?php include( "$GLOBAL_START_PATH/templates/accounting/invoice/addresses.htm.php" ); ?>
			</div>
			
			<div id="commentaires">		
				<?php include( "$GLOBAL_START_PATH/templates/accounting/invoice/comments.htm.php" ); ?>
			</div>		
			
			<div id="infos-client">
		
				<div class="blocMultiple blocMultiple4">
					<div style="min-height:100px;">
						<p><strong><?php echo $Order->getCustomer()->get('company'); ?>
						(n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>)</strong></p>
						<p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ); ?> <?php echo $Order->getCustomer()->getContact()->get( "firstname" ); ?></p>
						<p><?php echo $Order->getCustomer()->get('adress'); ?><br/>
						<?php $adr2= $Order->getCustomer()->get('adress_2');if(empty($adr2)){echo "&nbsp;";}else{ echo $adr2;} ?></p>
						<p><strong><?php echo $Order->getCustomer()->get('zipcode'); ?> <?php echo $Order->getCustomer()->get('city'); ?></strong> - <?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ); ?></p>
					</div><br/>
					
					<p><strong><?php echo str_replace("<br />","",Dictionnary::translate( $Order->getCustomer()->get( "contact" ) ? "contact_source" : "buyer_source" ) ) ?> :</strong> 
		            <?php echo DBUtil::getDBValue( "source_1", "source", "idsource", $Order->getCustomer()->get( "idsource" ) ); ?></p>
		           <!-- <p><strong>Statut demande assurance :</strong> 
					<?php
						switch( $customer->get( "insurance_status" ) ){
							case "Accepted":
								echo "accepté";
								break;
								
							case "Refused":
								echo "refusé";
								break;

							case "Deleted":
								echo "supprimé";
								break;
								
							case "Terminated":
								echo "résilié";
								break;

							default:
								echo "non assuré";
								break;
						}					

					?></p>-->
					
					<!--<p><strong><?php
						switch( $customer->get( "insurance_status" ) ){
							case "Accepted":
								echo "Date d'assurance : ";
								break;
								
							case "Refused":
								echo "Date de refus : ";
								break;

							case "Deleted":
								echo "Date de suppression : ";
								break;
								
							case "Terminated":
								echo "Date de résiliation : ";
								break;

							default:
								echo "";
								break;
						}					
	
					?></strong> -->
					<!--<?php
						switch( $customer->get( "insurance_status" ) ){
							case "Accepted":
								echo Util::dateFormatEu( $customer->get( "insurance_amount_creation_date" ) );
								break;
								
							case "Refused":
								echo Util::dateFormatEu( $customer->get( "insurance_refusal_date" ) );
								break;

							case "Deleted":
								echo Util::dateFormatEu( $customer->get( "insurance_deletion_date" ) );
								break;
								
							case "Terminated":
								echo Util::dateFormatEu( $customer->get( "insurance_termination_date" ) );
								break;

							default:
								echo "";
								break;
						}					
	
					?></p>-->
				</div>
				
				<div class="blocMultiple blocMultiple4">
					<div style="min-height:100px;">
						<table cellpadding="0" cellspacing="0" border="0">
				            <?php if(Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "phonenumber" ))) { ?>
				            <tr>
				            	<td><p><strong>Tél. : </strong></p></td>
				            	<td><p><?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "phonenumber" )); ?></p></td>
				            </tr>
				            <?php } ?>
				            <?php if(Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "faxnumber" ))) { ?>
				            <tr>
				            	<td><p><strong>Fax. : </strong></p></td>
				            	<td><p><?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "faxnumber" )); ?></p></td>
				            </tr>
				            <?php } ?>
				            <?php if(Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "gsm" ))) { ?>
				            <tr>
				            	<td><p><strong>GSM : </strong></p></td>	
				            	<td><p><?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "gsm" )); ?></p></td>
				           	</tr>
				            <?php } ?>
				            <tr>
				            	<td width="40"><p><strong>Email : </strong></p></td>	
				           		<td><p><a class="blueLink" href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?></a></p></td>	
				        	</tr>
				        </table> 
				        <p><a class="viewContact" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?type=<?php echo $Order->getCustomer()->get( "contact" ) ? "contact" : "buyer" ?>&amp;key=<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>&amp;idbilling_buyer=<?php echo $IdOrder ?>" onclick="window.open(this.href); return false;" >
				        Voir la fiche client n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?></a></p>
		
							 
				    </div><br/>
					<!--
					<p><strong>Encours garanti HT : </strong><?php echo $Order->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->get( "insurance_amount" ) ) : "-"; ?></p>
					<p><strong>Encours assurance HT : </strong><?php echo $Order->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->getInsuranceOutstandingCreditsET() ) : "-"; ?></p>
			        <?php if($relaunched){?>
		            	<p><a href="#" class="blueLink" onclick="seeRelaunch(); return false;">!! Attention client relancé !!</a></p>
		            <?php } ?>
			        
			        <div class="spacer"></div> -->
				</div>
				
				<div class="blocMultiple blocMultiple4">
					<?php 
				        // Recupération du siret
			        
				        $querySiret = 'SELECT siret FROM buyer WHERE idbuyer = "'.$Order->getCustomer()->get( "idbuyer" ).'"';
				        $bdSiret = DBUtil::getConnection();
				        $rsSiret = $bdSiret->Execute($querySiret);
		        
				        if($rsSiret === true){
				        	$siren = substr($rsSiret->fields("siret"), 0, 9);
				        }
		    		?>
		    		
		    		<p><strong>Vérification en ligne</strong></p>
	        							
					<?php if(isset($siren) && strlen($siren) > 3){ ?>
						<a class="floatleft" href="http://www.manageo.fr/entreprise/rc/preliste.jsp?action=2&siren=<?=$siren?>&x=42&y=2" target="_blank" style="margin:0 10px 0 5px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
						<a class="floatleft" href="http://www.societe.com/cgi-bin/recherche?rncs=<?=$siren?>&imageField2.x=60&imageField2.y=1" target="_blank" style="margin-right:10px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
					<?php }else{ ?>
						<a class="floatleft" href="http://www.manageo.fr/" target="_blank" style="margin:0 10px 0 5px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
						<a class="floatleft" href="http://www.societe.com/" target="_blank" style="margin-right:10px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
					<?php } ?>
			
			        <a class="floatleft" href="http://www.pagesjaunes.fr/" target="_blank" style="margin-right:10px;">
			        <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-pages-jaunes_g.jpg" alt="" width="48" height="25" /></a>
					<a class="floatleft" href="http://www.ctqui.com" target="_blank" style="margin-right:10px;">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-ctqui_g.jpg" alt="" width="48" height="25" /></a>
					<div class="spacer"></div> 
					
                    
                    <?php
			
			$query = "
					SELECT title_offer 
					FROM  `billing_buyer`
					WHERE idbilling_buyer = '" . $_GET["IdInvoice"] . "'
					";

					$rs =& DBUtil::query( $query );
			
			$title_offer = $rs->fields("title_offer");
			
			?>
            
           <form action="com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" name="litigious_creation" enctype="multipart/form-data" method="post">
            <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
                    <p><strong>Titre de la facture</strong><br />
					
					<input type="text" name="title_offer" value="<?php echo $title_offer; ?>"><input type="submit" class="blueButton" value="ok" /></p>
						
                    </div>
		       	</div>
	       	
	       		<div class="spacer"></div>
			
			</form>
	                 
                    
                     <?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping");
			if($akilae_grouping==1){
					?>
                    
					<?php
	
						$customerGrouping = DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $Order->getCustomer()->get( "idbuyer" ) );
						
						if( $customerGrouping ){
							
							$grouping = new GroupingCatalog();
							
							//@todo: Bidouille qui consiste à changer l'idgrouping du groupement à la volée pour charger les infos
							// Le problème vient du fait qu'on ne peut instancier un groupement que depuis le Front-Office
							$grouping->getGroupingInfos( $customerGrouping );
							
							$groupingName = $grouping->getGrouping( "grouping" );
							$groupingLogo = $grouping->getGrouping( "grouping_logo" );
												
					?>	
							<p><strong>Groupement : </strong><?php echo $groupingName ?></p>
							<div style="margin:0 0 5px 0; text-align:center;">
								<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $groupingLogo ?>" alt="<?php echo $groupingName ?>" />
							</div>
							<div class="spacer"></div>
					
					<?php } ?>
						<?php } ?>
				</div>
				
				<div class="blocMultiple blocMultiple4">
					<?php
					/* Litiges */
				    $rsLitigation =& DBUtil::query( "SELECT lb.idlitigation, l.editable FROM litigations_billing_buyer lb , litigations l WHERE lb.idlitigation = l.idlitigation AND  idbilling_buyer = '" . $Order->get("idbilling_buyer" ) . "'" );    	
					if( $rsLitigation->RecordCount() ){
					?>
						<p><strong>&#0155; Litiges ouverts</strong></p>
						<?php while( !$rsLitigation->EOF() ){ ?>
							<p><a class="blueLink" href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open( this.href ); return false;">
							<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="vertical-align:middle;" />
							Litige N° <?php echo $rsLitigation->fields( "idlitigation" ) ?> ( <?php echo $rsLitigation->fields( "editable" ) ? "en cours" : "résolu"; ?> )
							</a></p>
						<?php
							$rsLitigation->MoveNext();
						} ?>
				
						<?php
				
					} 
					
					/* avoirs client*/ ?>
					<p><strong>&#0155; Avoirs</strong></p>
					<p><a class="blueLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idbilling_buyer=<?php echo $Order->getId() ?>" taget="_blank">
					Créer un avoir client</a></p>
				    
				    <?php
				    $rsCredits =& DBUtil::query( "SELECT idcredit FROM `credits` WHERE idbilling_buyer='" . $Order->get("idbilling_buyer") ."'" );
				    if( $rsCredits->RecordCount() ){
				    ?>
						<p><strong>Liste des avoirs client créés</strong></p>
						<?php while( !$rsCredits->EOF() ){ ?>
							<p><a class="blueLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $rsCredits->fields( "idcredit" ) ?>">
							Avoir Client n°<?php echo $rsCredits->fields( "idcredit" ) ?></a></p>
						<?php
						    $rsCredits->MoveNext();
						} ?>
						
						<?php
					} ?>
					<div class="spacer"></div> 
				</div>
				

			
			</div> <!-- #infos-client -->
			
			
			<div id="visualiser-imprimer">
				<div class="blocMultiple blocMultiple4">
					<?php if( !strlen( $Order->getCustomer()->getContact()->get( "mail" ) ) ){ ?>
						<p style="margin-bottom:5px;" class="subTitleWarning"><?php  echo Dictionnary::translate("gest_com_buyer_without_mail_order") ; ?></p>				
					<?php } else { ?>
						<p style="margin-bottom:5px;"><a style="display:block;" class="normalLink" href="transmit_invoice.php?IdInvoice=<?php echo $Order->getId() ?>" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
						&nbsp;Mail d'accompagnement</a></p>
					<?php }	?>
				</div>
				
				<div class="blocMultiple blocMultiple4">
					<p style="margin-bottom:5px;"><a style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $Order->getId() ?>" class="normalLink">
					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
					&nbsp;Facture PDF n°<?php echo $Order->getId() ?></a></p>
	       		
                    <p style="margin-bottom:5px;"><a class="normalLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/catalog/odt_invoice.php?idinvoice=<?php echo $Order->getId()?>" >
                        	<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/odt.jpg" width="17px" height="17px"/>
						&nbsp;Facture ODT n°<?php echo $Order->getId() ?></a></p>
                
                
                
	       			<?php if( file_exists( "$GLOBAL_START_PATH/www/cgv.pdf" ) ){ ?>
						<p style="margin-bottom:5px;"><a class="normalLink" style="display:block;" href="<?php echo $GLOBAL_START_URL; ?>/www/cgv.pdf" onclick="window.open( this.href ); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
						&nbsp;Conditions Générales de Vente</a></p>
					<?php } ?>
				</div>
			
			</div> <!-- #visualiser-imprimer -->
			
		</div> <!-- #container -->	
		<div class="spacer"></div>
	
	</div></div>

</div>


