<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "UpdateIdPayment" );
anchor( "UpdatePaymentDel" );
anchor( "UpdateFactor" );
anchor( "UpdateNOrder" );
anchor( "UpdateBalanceAmount" );
anchor( "UpdateBalanceDate" );
anchor( "UpdateBillingComment" );

//----------------------------------------------------------------------------
//partie éditable si la commande n'a pas été facturée, payée ou annulée

if( $Order->get( "status" ) == Invoice::$STATUS_PAID )
		$payment_disabled = " disabled=\"disabled\"";
else 	$payment_disabled = "";

$no_tax_export = $Order->get( "no_tax_export" );

//----------------------------------------------------------------------------

?>
<div class="contentResult" style="margin-bottom: 0;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>

	<h1 class="titleEstimate"><span class="textTitle">Paiement et échéance</span>
	<div class="spacer"></div></h1>

	<div class="blocEstimateResult"><div style="margin:5px;">

			<div class="floatleft" style="margin-top:10px; width:89%;">
			
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
					<label><span>Mode de règlement</span></label>
					<?php DrawLift_modepayment( $Order->get( "idpayment" ) ? $Order->get( "idpayment" ) : 4, $Order->get( "status" ) != Invoice::$STATUS_PAID ); ?>
					<input type="hidden" id="UpdateIdPayment" name="UpdateIdPayment" value="ok" />
					<div class="spacer"></div>
					
					<?php if( $Order->get( "idpayment" ) == 1 && $Order->get( "status" ) != Invoice::$STATUS_PAID ){ ?>
						<label style="height:47px;"><span>Accès au <br/>paiement sécurisé</span></label>
						<p style="text-align:center; padding:5px 0 0 0; height:45px; width:70%;" class="textidSimp">
						<a href="<?php echo $GLOBAL_START_URL ?>/accounting/pay_secure.php?IdOrder=<?php echo $IdOrder ?>" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-payment.jpg" width="193" height="45" alt="Lien vers le paiement sécurisé" /></a>
						</p>
						<div class="spacer"></div>
					<?php } ?>
					
					<label><span>Délai de paiement</span></label>
					<?php 
						$idpayment_delay = $Order->get( "idpayment_delay" );
						if( empty( $idpayment_delay ) )
							$idpayment_delay = DBUtil::getParameter( "default_idpayment_delay" );
	
						DrawPaymentDelayList( $idpayment_delay );
					?>
					<div class="spacer"></div>
					
					<label><span>Echéance règlement</span></label>
					 <?php 	
						//Echeance
						$echeance = $Order->get( "deliv_payment" );
						if( $echeance == "0000-00-00" ){ //aucune date saisie
							$echeance = "";
							$startdate = date( "Y-m-d" );
						}
						else $startdate = $echeance;
					?>
					<input class="calendarInput" type="text" name="deliv_payment" id="deliv_payment" value="<?php echo usDate2eu( $echeance ); ?>" readonly="readonly" />
					<?php DHTMLCalendar::calendar( "deliv_payment" ) ?>
				</div></div>		 	
		
				<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">				 	
					<!--<label><span>Assurance</span></label>
					<span class="textSimple"><?php echo $Order->get( "insurance" ) ? "Oui" : "Non"; ?></span>	
					<div class="spacer"></div>-->

					<label><span>Facturation HT</span></label>
					<span class="textSimple" style="width:20%;"><?php echo $Order->get( "no_tax_export" ) ? "Oui" : "Non"; ?></span>	
					
					<label><span>Réf. commande</span></label>
					<span class="textSimple" style="width:20%;">
					<?php
						$query = "
						SELECT o.n_order, o.remote_creation_date
						FROM bl_delivery bl, `order` o
						WHERE bl.idbilling_buyer = '" . $Order->getId() . "'
						AND bl.idorder = o.idorder
						LIMIT 1";
						$rs =& DBUtil::query( $query );
						while( !$rs->EOF() ){
							echo htmlentities( $rs->fields( "n_order" ) );
							$rs->MoveNext();
						}
					?>
					</span>	
					<div class="spacer"></div>
					
					<label><span>Date création interne</span></label>
					<span class="textSimple">
					<?php
						$rs->MoveFirst();
						while( !$rs->EOF() ){
							echo htmlentities( Util::dateFormatEu( $rs->fields( "remote_creation_date" ) ) );
							$rs->MoveNext();
						}
					?>
					</span>	
					<div class="spacer"></div>
				</div></div>			
				<div class="spacer"></div>
				
				<?php	
					if( $Order->getTotalATI() == 0.0 )
                		$rebate_rate = 0.0;
                	else $rebate_rate 	= $Order->get( "rebate_type" ) == "rate" 	? $Order->get( "rebate_value" ) : $Order->get( "rebate_value" ) * 100.0 / $Order->getTotalATI();
                	$rebate_amount 	= $Order->get( "rebate_type" ) == "amount" ? $Order->get( "rebate_value" ) : $Order->getTotalATI() * $Order->get( "rebate_value" ) / 100.0;
				?>
				
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
					<h2>Escompte</h2>
					
					<label><span>Escompte</span></label>
					<input<?php if( $Order->get( "status" ) == Invoice::$STATUS_PAID ) echo " disabled=\"disabled\""; ?> type="text" name="rebate_rate" class="calendarInput percentage" value="<?php echo Util::numberFormat( $rebate_rate ); ?>" />
					<span class="textidSimp" style="width:5.4%;">%</span>	
					
					<input<?php if( $Order->get( "status" ) == Invoice::$STATUS_PAID ) echo " disabled=\"disabled\""; ?> type="text" name="rebate_amount" class="calendarInput price" value="<?php echo Util::numberFormat( $rebate_amount ); ?>" />
					<span class="textidSimp" style="width:5.4%;">&euro;</span>	
					<div class="spacer"></div>
					
					<!--<h2>Crédit disponible</h2>
					<label><span>Crédit disponible</span></label>
                    <input type="text" class="calendarInput" name="credit_amount" value="<?=Util::numberFormat($Order->get("credit_amount"));?>" />  
                    <span class="textidSimp" style="width:5.4%;">&euro;</span>	
                    
                    <span class="textidSimp" style="width:25.4%;"><?php
						$credit_balance = $Order->getCustomer()->get("credit_balance");
                       	echo "dispo. : " . Util::priceFormat( $credit_balance );
                    ?></span>-->

				</div></div>
					

				<?php
					$query = "
					SELECT o.down_payment_comment, o.down_payment_date, o.down_payment_piece, p.name_1 AS paymentName, b.bank_name
					FROM bl_delivery bl, `order` o
					LEFT JOIN payment p ON p.idpayment = o.down_payment_idpayment
					LEFT JOIN bank b ON b.idbank = o.down_payment_idbank
					WHERE bl.idbilling_buyer = '" . $Order->getId() . "'
					AND bl.idorder = o.idorder
					LIMIT 1";
					
					$rs =& DBUtil::query( $query );
					
					$downPaymentAmount = $Order->getDownPaymentAmount();
					
				?>
				<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">
					<h2>Acompte</h2>
					
					<label><span>Acompte</span></label>
					<span class="textSimple" style="width:20%;">
						<?php echo $downPaymentAmount > 0.0 ? Util::rateFormat( $Order->getDownPaymentRate() ) : "" ?>
						<?php echo $downPaymentAmount > 0.0 ? Util::priceFormat( $downPaymentAmount = $Order->getDownPaymentAmount() ) : "" ?>
					</span>
					
					<label><span>Date règlement</span></label>
					<span class="textSimple" style="width:20%;"><?php echo $downPaymentAmount > 0.0 ? Util::dateFormatEu( $rs->fields( "down_payment_date" ) ) : "-"; ?></span>
					
					<label><span>Mode règlement</span></label>
					<span class="textSimple" style="width:20%;"><?php echo $downPaymentAmount > 0.0 ? htmlentities( $rs->fields( "paymentName" ) ) : "-"; ?></span>
					
					<label><span>Num. pièce</span></label>
					<span class="textSimple" style="width:20%;"><?php echo $downPaymentAmount > 0.0 ? htmlentities( $rs->fields( "down_payment_piece" ) ) : "-"; ?></span>
					
					<label><span>Banque émettrice</span></label>
					<span class="textSimple" style="width:20%;"><?php echo $downPaymentAmount > 0.0 ? htmlentities( $rs->fields( "bank_name" ) ) : "-"; ?></span>
					
					<label><span>Commentaires</span></label>
					<span class="textSimple" style="width:20%;"><?php echo $downPaymentAmount > 0.0 ? htmlentities( $rs->fields( "down_payment_comment" ) ) : "-"; ?></span>
					
				</div></div>
				<div class="spacer"></div>

				<div class="floatright" style="margin-top:5px;">
					<input type="submit" class="blueButton" value="Recalculer" name="SaveOrder" />
	       		</div>
			</div>
			
			<div class="floatright" style="width:10%; margin-top:10px;">
		    	<div class="tableContainer" style="margin-top:0px;">
		    	<?php
		    	
		    		if( $Order->getTotalATI() == 0.0 )
                		$rebate_rate = 0.0;
                	else $rebate_rate 	= $Order->get( "rebate_type" ) == "rate" 	? $Order->get( "rebate_value" ) : $Order->get( "rebate_value" ) * 100.0 / $Order->getTotalATI();
                	$rebate_amount 	= $Order->get( "rebate_type" ) == "amount" ? $Order->get( "rebate_value" ) : $Order->getTotalATI() * $Order->get( "rebate_value" ) / 100.0;
  	
                    ?>
		            <table class="dataTable devisTable summaryTable">
		            	<tr>
		            		<th colspan="2">Escompte</th>
		            	</tr>
		            	<tr>
		            		<td><?php echo Util::rateFormat( $rebate_rate ); ?></td>
		            		<td><?php echo Util::priceFormat( $rebate_amount ); ?></td>
		            	</tr>
		            	<tr>
		            		<th colspan="2">Net à payer</th>
		            	</tr>
		            	<tr>
		            		<td colspan="2"><?php echo Util::priceFormat( $Order->getNetBill() ); ?></td>
		            	</tr>
		            	<tr>
		            		<th colspan="2">Solde à payer</th>
		            	</tr>
		            	<tr>
		            		<td colspan="2"><?php echo Util::priceFormat( $Order->getBalanceOutstanding() ); ?></td>
		            	</tr>
		          	</table>
				</div>
		    </div>
		    
        <div class="spacer"></div>
	        
	</div></div><!-- blocEstimateResult -->
	<div class="spacer"></div>
	
</div>