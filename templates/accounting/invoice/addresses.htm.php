<script type="text/javascript">
/* <![CDATA[ */

	//------------------------------------------------------------------------------------------------
	
	/*affectation auto des adresses à la facture après modification des données*/
		
	function selectInvoiceAddress( addressType, selectedAddress ){
	
		var primaryKey 	= getPrimaryKeyName( addressType );
		var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
		var checked 	= document.getElementById( addressType + '_checkbox' ).checked;
		
		var selectedAddress = 0;
		
		if( checked )
			selectedAddress = addresses.options[ addresses.selectedIndex ].value;
		
		$.ajax({
	 	
			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?select&type=" + addressType + "&id=" + selectedAddress + "&tablename=billing_buyer&primarykey=<?php echo $Order->getId() ?>",
			async: false,
		 	success: function( response ){
		 		
				if( response == '0' )
					xhrAlert( 'Impossible de mettre à jour la facture' );
				
			}
	
		});
		
	}
	
	//------------------------------------------------------------------------------------------------
	
/*	]]> */
</script>
<?php 

/**
 * Affichage des adresses de livraison/facturation si différentes
 */	

$iddelivery = DBUtil::getDBValue( "iddelivery", "order", "idorder", $Order->get( "idorder" ) );

$forwardingAddress 	= $Order->getForwardingAddress() instanceof ForwardingAddress ? $Order->getForwardingAddress() : false;
$invoiceAddress 	= $Order->getInvoiceAddress() instanceof InvoiceAddress ? $Order->getInvoiceAddress() : false;

?>
<input type="hidden" name="updateAddresses" value="1" />
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    <div class="content">
    	<?php anchor( "UpdateAdresses" ); ?>
    	<div class="subContent">
            <?php
            
            	include_once( "$GLOBAL_START_PATH/objects/AddressEditor.php" );
            	
            	//@todo en attendant une gestion cohérente des adresses, on édite pas l'adresse du client par défaut

            	new AddressEditor( $Order->get( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_FORWARDING, $forwardingAddress, true, "float:left; width:50%; margin-top:15px;" );
				new AddressEditor( $Order->get( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_INVOICE, $invoiceAddress, false, "float:right; width:50%; margin-top:15px;", "selectInvoiceAddress" );
	
			?>
			<br style="clear:both;" />
		</div><!-- subContent -->
   	</div><!-- content -->
</div><!-- mainContent -->