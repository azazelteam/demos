<!-- ************************************* ( o ) ( o ) résumé des paiements ( o ) ( o ) *********************************** -->

<div class="contentResult" style="margin-bottom: 0;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<h1 class="titleEstimate"><span class="textTitle">Paiements enregistrés pour cette facture</span>
	<div class="spacer"></div></h1>

	<div class="blocEstimateResult"><div style="margin:5px;">

        <?
        
        include_once( dirname( __FILE__ ) . "/../../../objects/RegulationsBuyer.php" );
        
        $regulations = RegulationsBuyer::getRegulations($Order->get("idbilling_buyer"));
        
        if(count($regulations)){
        	
            ?>
			<div class="floatleft" style="margin-top:3px; width:100%;">
				<table class="dataTable devisTable">
					<tr>
						<th style="width:7%;">Com</th>
						<th style="width:13%;">Num reglement</th>
						<th style="width:13%;">Date de paiement</th>
						<th style="width:13%;">Montant règlé</th>
						<th style="width:13%;">Total du reglement</th>
						<th style="width:13%;">Mode de règlement</th>
						<th style="width:13%;">Acompte</th>
						<th>Commentaire</th>
					</tr>
					<?php
					
						for( $i=0 ; $i<count($regulations) ; $i++){ 
							
							?>
							<tr>
								<td><?php echo $regulations[ $i ][ "initial" ]; ?></td>
								<td><?php echo $regulations[$i]["idregulations_buyer"] ?></td>
								<td><?php echo usDate2eu($regulations[$i]["payment_date"]) ?></td>
								<td><?php echo Util::priceFormat($regulations[$i]["detailledAmount"]) ?></td>
								<td><?php echo Util::priceFormat($regulations[$i]["amount"]) ?></td>
								<td><?php echo getPaymentTxt($regulations[$i]["idpayment"]) ?></td>
								<td><?php if($regulations[$i]["from_down_payment"]==1){ echo "Oui"; }else{ echo "Non"; } ?></td>
								<td><?php echo stripslashes($regulations[$i]["comment"]) ?></td>
							</tr>
							<?php
							
						} 
						
				?>
				</table>
			</div>
			<?php
			
		}
		else echo "Aucun paiement enregistré pour cette facture";
		
		?>
        <div class="spacer"></div>
	        
	</div></div><!-- blocEstimateResult -->
	<div class="spacer"></div>
	
</div>