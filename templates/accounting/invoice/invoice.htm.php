<script type="text/javascript">
/* <![CDATA[ */

	function showSupplierInfos( idsupplier){
					
		if( idsupplier == '0' )
			return;

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
            		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
			}

		});
		
	}
	
/* ]]> */
</script>
<a name="references"></a>
<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" />

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/accounting/invoice/invoice.htm.php</span>
<?php } ?>

<div class="contentResult" style="margin-bottom: 10px;">

	<h1 class="titleEstimate"><span class="textTitle">Descriptif de la facture</span>
	<div class="spacer"></div></h1>


	<div class="blocEstimateResult"><div style="margin:5px;">
	
		<div class="content">
			<div class="subContent">
				<?php
				
					anchor( "UpdateRefDiscountRate_", true );
					anchor( "UpdateOrderPriceWD_", true );
					anchor( "UpdateUnitCostRate_", true );
					anchor( "UpdateUnitCostAmount_", true );
					anchor( "UpdateDesignation_", true );
					anchor( "UpdateSupplierCharges_", true );
					anchor( "UpdateInternalSupplierCharges_", true ); 
					anchor( "UpdRow_", true );
				
					/*lignes articles*/ 
					
					$it = $Order->getItems()->iterator();
					
					while( $it->hasNext() ){
					
						$item =& $it->next();
						
						?>
						<?php
							
						displayItem( $Order, $item );
					
						?>
						<div class="floatright" style="width: 10%; margin-bottom: 10px;">
							<?php displayItemMargins( $item ); ?>
						</div>
						<?php
						
					}
					
				?>
				</div><!-- subContent -->
		</div><!-- content -->
		<div class="spacer"></div>
		
	</div></div><!-- blocEstimateResult -->
	<div class="spacer"></div>

</div>
<?php

//-------------------------------------------------------------------------------------------------------------------

function displayItem( Invoice &$invoice, InvoiceItem &$item ){
	
	global $GLOBAL_START_URL;
	
	?>
	<div style="width: 89%; margin-bottom: 10px;" class="floatleft">
		<table class="dataTable devisTable">
		    <tr>
		    	<th style="width:75px;">Photo</th>
		    	<th>Réf. & Désignation courte</th>
		    	<th>Quantité</th>
		    	<th>Prix unitaire
				
				<?php if( B2B_STRATEGY ){?> HT	<?php }else{ ?>
				TTC	<?php } ?>
				
				</th>
		    	<th>Remise<br />Client %</th>
		    	<th>Prix unitaire<br />Vente Net
					<?php if( B2B_STRATEGY ){?> HT	<?php }else{ ?>
				TTC	<?php } ?>
				</th>
		    	<th>Prix vente<br />Total<?php if( B2B_STRATEGY ){?> HT	<?php }else{ ?>
				TTC	<?php } ?></th>
		    </tr>
			<tr>
		    	<td rowspan="5" style="vertical-align:top;">
				<?php 
				
					/*options */ 			displayOptionLink( $item ); 
					/*produits similaires*/	displaySimilarLink( $item );
					/*image*/				displayReferenceThumb( $item );
					/*promotion @todo : n'existe pas*/
					/*destockage @todo : n'existe pas*/
					/*prix au volume*/		displayVolPriceLink( $invoice, $item );
					
				?>
		    	</td>
		    	<td rowspan="2" style="text-align:left;"><?php displayReferenceInfos( $item ); ?></td>
				<td rowspan="3">
				<?php 
				
					/*popup stock*/ 	displayStockPopup( $item );
					/*minimum de cde*/	checkMinOrder( $item );
					/*quantité*/		echo $item->get( "quantity" );
					/*quantité par lot*/ 
					
					if( $item->get( "lot" ) > 1 ){
							
						?>
						<p><?php echo $item->get("lot" ) . " / " . htmlentities( DBUtil::getDBValue( "unit" . "_1", "unit", "idunit", $item->get( "unit" ) ) ); ?></p>
						<?php
					}
					
				?>
				</td>
				<td><?php  /*Prix unitaire*/ echo Util::priceFormat( $item->get( "unit_price" ) ); ?></td>
				<td>
				<?php 
				
					/*remise client %*/ 
				
					if( DBUtil::getParameterAdmin( "view_ref_discount_add" ) == 0 )
						echo Util::rateFormat( $item->get( "ref_discount" ) );
					else{
				
						?>
						<p><b>Base</b></p>
						<p><?php echo Util::rateFormat( $item->get( "ref_discount_base" ) ) ?></p>
						<p><b>Complémentaire</b></p>
						<p><?php echo Util::rateFormat( $item->get( "ref_discount_add" ) ) ?></p>
						<?php 
						
					}
				
				?>
				</td>
				<td><?php /*prix unitaire vente net*/	echo Util::priceFormat( $item->get( "discount_price" ) ); ?></td>
				<td><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "discount_price" ) ) ?></td>		
			</tr>
			<tr>
				<th>Prix unitaire<br />Tarif fournisseur</th>
		    	<th>Remise<br />Fournisseur %</th>
		    	<th>Prix unitaire<br />Achat net</th>
		    	<th>Prix achat<br />Total</th>
		    </tr>
			<tr>
				<td rowspan="3" style="width:210px; height:110px;">
					<div style="width:210px; height:110px; border-style:none; text-align:left;"><?php echo stripDesignationTags( $item ) ?></div>
				</td>
				<td><?php echo Util::priceFormat( $item->get( "unit_cost" ) ) ?></td>
				<td><?php echo Util::rateFormat( $item->get( "unit_cost_rate" ) ) ?></td>
				<td><?php echo Util::priceFormat( $item->get( "unit_cost_amount" )) ?></td>
				<td><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "unit_cost_amount" ) ) ?></td>	
			</tr>
			<tr>
				<th>Stock<br />interne</th>
				<th>Stock<br />externe</th>
				<th>Délai<br />d'éxpédition</th>
				<th>Poids unitaire</th>
				<th>Poids total</th>
			</tr>
			<tr>
				<td><?php
				
					$external_quantity = DBUtil::query( "SELECT external_quantity FROM `order_row` WHERE idorder = '" . $invoice->get( "idorder" ) . "' AND idrow = '" . $item->get( "idorder_row" ) . "' LIMIT 1" )->fields( "external_quantity" );
					echo intval( $item->get( "quantity" ) - $external_quantity );
					
				?></td>
				<td><?php /*quantité externe @todo : n'existe pas*/ echo $external_quantity ?></td>
				<td>
				<?php 
				
					/*délai de livraison @todo : n'existe pas*/
				
					$lang = User::getInstance()->getLang();
					$deliv_delay = DBUtil::query( "SELECT d.delay$lang AS delay FROM delay d, `order_row` orow WHERE orow.idorder = '" . $invoice->get( "idorder" ) . "' AND orow.idrow = '" . $item->get( "idorder_row" ) . "' AND orow.delivdelay = d.iddelay LIMIT 1" )->fields( "delay" );
					echo htmlentities( $deliv_delay );
					
				?></td>
				<td><?php echo Util::numberFormat( $item->get( "weight" ) ) . " kg"; ?></td>
				<td><?php echo Util::numberFormat( $item->get( "quantity" ) * $item->get( "weight" ) ) . " kg"; ?></td>
			</tr>
		</table>
		<div class="spacer"></div>
		
		<div class="floatright">
	        <input type="button" onclick="window.open('<?php echo "$GLOBAL_START_URL/statistics/statistics.php?cat=product&reference=" . urlencode( $item->get( "reference" ) ) . "&amp;period&amp;StatStartDate=" . urlencode( date( "d/m/Y", mktime( 0, 0, 0, date( "m" ) - 6, date( "d" ), date( "Y" ) ) ) ) . "&amp;StatEndDate=" . urlencode( date( "d/m/Y" ) ) ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px; margin-right:5px;"/>
	    	<input type="button" value="Ouvrir un litige" class="blueButton blueButtonActif" onclick="document.location = '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idbilling_buyer=<?php echo $invoice->get( "idbilling_buyer" ) ?>&idrow=<?php echo $item->get( "idrow" ) ?>';" />
	    </div>
		
	</div><!-- tableContainer -->
    
    
    <?php

}

//-----------------------------------------------------------------------------------------------------
/**
 * options
 */
function displayOptionLink( &$item ){
			
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( DBUtil::query( "SELECT COUNT( * ) AS `hasOption` FROM associat_product WHERE idproduct='$idproduct'" )->fields( "hasOption" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="optionssel(<?php echo $item->get( "idarticle" ) ?>); return false;" class="orangeText"><b>Options</b></a>
		</p>
		<?php 
		
	}
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * produits similaires
 */
function displaySimilarLink( &$item ){
	
	//produits similaires

	if( DBUtil::query( "SELECT COUNT( * ) AS `hasSimilar` FROM product WHERE idproduct='" . DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) . "' AND ( idproduct_similar_1 > 0 OR idproduct_similar_2 > 0 OR idproduct_similar_3 > 0 )" )->fields( "hasSimilar" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="similarsel(<?php echo $item->get( "idarticle" ) ?>); return false;" class="orangeText"><b>Similaires</b></a>
		</p>
		<?php 
		
	}
    
}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( $idproduct ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" onclick="window.open(this.href); return false;">
		<?php 
		
	}
	
	?>
	<img src="<?php echo URLFactory::getReferenceImageURI( $item->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * prix au volumne
 */
function displayVolPriceLink( &$invoice, &$item ){
	
	global $GLOBAL_START_URL;
	
	$idbuyer_family = DBUtil::getDBValue( "idcustomer_profile", "buyer", "idbuyer", $invoice->get( "idbuyer" ) );
	
	$query = "
	SELECT COUNT( rate ) AS `count`
	FROM multiprice_family 
	WHERE idcustomer_profile = '$idcustomer_profile' 
	AND reference = '" . $item->get( "reference" ) . "' 
	AND begin_date <= NOW() 
	AND end_date >= NOW()";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->fields( "count" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="PopupVolume( '<?php echo $item->get( "reference" ) ?>','<?php echo $idbuyer_family ?>');return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/www/img/volume.gif" alt="Prix au volume" style="border-style:none;" />
			</a>
		</p>
		<?php
		
	}
		
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$item ){
		
	global $GLOBAL_START_URL;
	
	/*référence avant substitution @todo : n'existe pas*/
	
	/*référence catalogue*/
	
	?>
	<p>Référence catalogue : <?php echo $item->get( "reference" ) ?></p>
	 <?php
	 
	 /*référence et infos fournisseur*/
	 
	 if( $item->get( "idsupplier" ) ){
		
	 	$ref_supplier = DBUtil::getDBValue( "ref_supplier", "detail", "idarticle", $item->get( "idarticle" ) );
	 	
		?>
		<p>
			<span class="lightGrayText">Réf. fournisseur : <?php echo $ref_supplier ?></span>
			&nbsp;
			<a href="#" onclick="showSupplierInfos( <?php echo $item->get( "idsupplier" ) ?> ); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
			</a>
		</p>
		<?php 

	 }
	
	/*référence alternative @todo : n'existe pas*/
	
	/*lien fiche PDF*/
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) ){
		
		$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $item->get( "idarticle" ) );
		$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : "/catalog/pdf_product.php?idproduct=$idproduct";
		
		?>
		<p>
			<a class="blueLink" href="<?php echo $href ?>" onclick="window.open(this.href); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
			</a>
		</p>
		<?php
			
	}
								 	
}

//-----------------------------------------------------------------------------------------------------

function displayStockPopup( &$item ){
	
	global $GLOBAL_START_URL;
	
	if( DBUtil::getDBValue( "stock_level", "detail", "idarticle", $item->get( "idarticle" ) ) == -1 )
		return;
			
	?>
	<p style="text-align:center; margin:5px;">
		<a href="#" onclick="popupStock( '<?php echo $item->get( "reference" ) ?>' );return false;">
			<img src="<?php echo $GLOBAL_START_URL ?>/www/img/stock.jpg" alt="Informations sur le stock" style="border-style:none;" />
		</a>
	</p>
	<?php
			
}

//-----------------------------------------------------------------------------------------------------

function checkMinOrder( &$item ){
	
	$min_cde = DBUtil::getDBValue( "min_cde", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( $min_cde < 2 )
		return;
				
	?>
	<p style="text-align:center; margin:5px;">
		<span class="Marge">Qté min : <?php echo $min_cde ?></span>
	</p>
	<?php
		
}	

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){

	$designation = $item->get( "designation" );
	$designation = str_replace("\n",'', $designation);
	$designation = str_replace("\r",'', $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = strip_tags($designation);
	
	return $designation;
		
}

//-----------------------------------------------------------------------------------------------------

function displayItemMargins( &$item ){
	
	?>
    <div class="tableContainer">
        <table class="dataTable devisTable summaryTable">
            <tr>
                <th colspan="2">Marge Brute Article  /  PV<br/>Hors Port</th>
            </tr>
            <tr>
                <td colspan="2">
				<?php
			
					/*marge brute HT*/
					
					$mb =  $item->get( "quantity" ) * ( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) );
					echo Util::priceFormat( $mb );
					
				?>
				</td>
            </tr>
            <tr>
                <th>MB %</th>
                <th>Coeff.</th>
            </tr>
            <tr>
                <td style="width:50%">
                <?php
                
                	/*marge finale*/
				
					$mbf = $item->get( "discount_price" ) > 0.0 ? ( 1 - $item->get( "unit_cost_amount" ) / $item->get( "discount_price" ) ) * 100.0 : 0.0;
					echo Util::rateFormat( $mbf );
				
				?>
                </td>
				<td style="width:50%">
				<?php 
			
					/*coefficient de vente brut*/
					
					$rough_sale_coeff = $item->get( "unit_cost" ) > 0.0 ? $item->get( "unit_price" ) / $item->get( "unit_cost" ) : 0.0; 
					echo Util::numberFormat(  $rough_sale_coeff );
					
				?>
				</td>
            </tr>
        </table>
        
         <br />
		  <?php
		  if ($item->get( "ecotaxe_code" ))
		  {
		  	$ecotaxe_code = $item->get( "ecotaxe_code" );
			$idrow = $item->get( "idrow" );
		  	$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
			$ecotaxe_amount = $rs1->fields( "amount" ) * $item->get( "quantity" );
		
		  	$rs2 =& DBUtil::query( "UPDATE order_row SET ecotaxe_amount = $ecotaxe_amount  WHERE ecotaxe_code = '$ecotaxe_code' and idrow = $idrow " );
		  ?>
          
           <table class="dataTable devisTable summaryTable">
           
           	<tr>
           	<th>Code ecotaxe</th>
          	<th>Montant</th>
          	</tr>
           
           <td style="width:50%" align="center"><?php if ($item->get( "ecotaxe_code" ) ) echo $item->get( "ecotaxe_code" ); else echo "-"; ?></td>
           <td style="width:50%" align="center"><?php echo $ecotaxe_amount; ?></td> 
           
           </table>
        
        <?
		}
		?>
        
        
	</div>
	<?php
	       
}

//-----------------------------------------------------------------------------------------------------

?>