<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"> 
/* <![CDATA[ */

	function showSupplierInfos( idsupplier){
					
		if( idsupplier == '0' )
			return;

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
            		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
			}

		});
		
	}
	
	function deleteReference( idrow, reference ){
	
		if( confirm( 'Confirmez la suppression de : ' + reference ) ){
		
			document.getElementById( 'delete_' + idrow ).value = 1;
			document.forms.frm.submit();
		
		}
		
		return false;
		
	}
	
	function selectReference(){
	
		var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=750, height=650';
		var location = 'invoice_references.php?idcredit=' + <?php echo $credit->get( "idcredit" ) ?> + '&ispopup=true';
		
		window.open( location, '_blank', options ) ;
		
	}
	
/* ]]> */
</script>
<?php

	anchor( "UpdateRefDiscountRate_", true );
	anchor( "UpdateDesignation_", true );
	anchor( "UpdRow_", true ); 

?>
<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $credit->getId() ?>" />
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Descriptifs de l'avoir</p>
                <?php
                
                /*onglets ajout références*/
				
				if( $credit->isPartial() || ( $credit->get( "credited_charges" ) == 0.0 && $credit->getInvoice()->get( "total_charge_ht" ) > 0.0 ) ){
				
					?>
					<div class="menu menuDevis">
					<?php
					
						if( $credit->isPartial() ){
							
							?>
							<div class="item" onclick="selectReference(); return false;">
								<div class="right"></div><div class="left"></div>
								<div class="centered"><?php  echo Dictionnary::translate("gest_com_by_reference") ; ?></div>
							</div>
							<?php
							
						}
						
						if( $credit->get( "credited_charges" ) == 0.0 && $credit->getInvoice()->get( "total_charge_ht" ) > 0.0 ){
							
							?>
							<div class="item" onclick="document.forms.frm.elements[ 'credited_charges' ].value = '<?php echo Util::numberFormat( $credit->getInvoice()->get( "total_charge_ht" ), 2 ) ?>'; document.forms.frm.submit(); return false;">
								<div class="right"></div><div class="left"></div>
								<div class="centered">CREDITER PORT</div>
							</div>
							<?php
							
						}
						
					?>
					</div>
					<?php
					
				}
				
			?>
        	</div><!-- subTitleContainer -->
			<a name="references"></a>
			<?php 
			
				/*lignes articles*/ 
				
				$it = $credit->getItems()->iterator();
				
				while( $it->hasNext() ){
					
					$creditItem =& $it->next();
					displayItem( $creditItem, $invoice->getItemAt( $creditItem->get( "idbilling_buyer_row" ) - 1 ), $disabled );
		
				}
				
				displayCreditedCharges( $credit );
				
		?>
		</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php

//-------------------------------------------------------------------------------------------------

function displayItem( &$creditItem, &$invoiceItem, $disabled ){
	
	global $GLOBAL_START_URL;
	
	//lien vers la fiche produit
	
	$lang 			= "_1";	
	$idproduct 		= DBUtil::getDBValue( "idproduct", "detail", "idarticle", $invoiceItem->get( "idarticle" ) );
	$productURL 	= URLFactory::getProductURL( $idproduct );

	?>
	<input type="hidden" id="reference_<?php echo $creditItem->get( "idrow" ) ?>" value="<?php echo $invoiceItem->get( "reference" ) ?>" />
	<input type="hidden" id="delete_<?php echo $creditItem->get( "idrow" ) ?>" name="delete_<?php echo $creditItem->get( "idrow" ) ?>" value="0" />
	<div class="tableContainer" style="margin-bottom:5px;">
		<table class="dataTable devisTable">
			<tr>
				<th style="width:75px;">Photo</th>
				<th>Réf. & Désignation courte</th>
				<th>Quantité facturée</th>
				<th>Prix de vente facture</th>
				<th>Remise sur vente facture</th>		
				<th>Total HT facturé</th>
		    </tr>
			<tr>
				<td rowspan="7"><?php /*image*/	displayReferenceThumb( $creditItem ); ?></td>
				<td rowspan="3" style="padding:10px; text-align:left;">
					<?php displayReferenceInfos( $creditItem, $invoiceItem ); ?>
				</td>
				<td rowspan="3"><?php echo $creditItem->get( "quantity" ) ?></td>
				<td rowspan="3"><?php echo Util::priceFormat( $invoiceItem->get( "unit_price" ) ) ?></td>
				<td><?php echo Util::rateFormat( $invoiceItem->get( "unit_price" ) > 0.0 ? round( ( 1 - $invoiceItem->get( "discount_price" ) / $invoiceItem->get( "unit_price" ) ) * 100.0 , 2 ) : 0.0 ); ?></td>
				<td rowspan="3"><?php echo Util::priceFormat( $invoiceItem->get( "quantity" ) * round( $invoiceItem->get( "discount_price" ), 2 ) ); ?></td>
			</tr>
			<tr>
				<th>Prix de vente net facturé</th>
			</tr>
			<tr>
				<td><?php echo Util::priceFormat( $invoiceItem->get( "discount_price" ) ) ?></td>
			</tr>
			<tr>
				<td rowspan="4" style="width:210px; height:110px;">
					<textarea<?php echo $disabled ?> name="designation_<?php echo $creditItem->get( "idrow" ) ?>" style="width:210px; height:110px; border-style:none;"><?php echo stripDesignationTags( $creditItem ) ?></textarea>
				</td>
				<th>Quantité</th>
				<th>Prix Unitaire Net</th>
				<th>Remise %</th>		
				<th>Total HT</th>
			</tr>
			<tr>
				<td rowspan="3">
					<input<?php echo $disabled ?> type="text" class="textInput quantity" name="quantity_<?php echo $creditItem->get( "idrow" ) ?>" value="<?php echo $creditItem->get( "quantity" ) ?>" onkeypress="return forceUnsignedIntegerValue( event );" style="width:40px;" />
				</td>
				<td rowspan="3">
					<input<?php echo $disabled ?> type="text" class="textInput price" name="unit_price_<?php echo $creditItem->get( "idrow" ) ?>" value="<?php echo Util::numberFormat( $creditItem->get( "unit_price" ) ) ?>" onkeypress="return forceUnsignedFloatValue( event );" style="width:40px;" />&nbsp;&euro;
				</td>
				<td>
				<?php
				
					$discount_price = $creditItem->get( "discount_price" );
					$unit_price 	= $creditItem->get( "unit_price" );
					$discount_rate 	= $unit_price > 0.0 ? ( 1 - $discount_price / $unit_price ) * 100.0: 0.0;
					
					?>
					<input type="text" class="textInput rate"<?php echo $disabled ?> name="discount_rate_<?php echo $creditItem->get( "idrow" ) ?>" value="<?php echo Util::numberFormat( $discount_rate ) ?>" onkeypress="return forceFloatValue( event );" size="5"  style="width:40px;" />&nbsp;%
					<input type="submit"<?php echo $disabled ?> name="UpdateDiscountRate_<?php echo $creditItem->get( "idrow" ) ?>" value="Ok" class="blueButton" style="text-align:center;cursor:pointer;"/>
				</td>
				<td rowspan="3">
				<?php 
				
					$total_discount_price = $creditItem->get( "quantity" ) * round( $creditItem->get( "discount_price" ), 2 );
					echo Util::priceFormat( $total_discount_price ); 
				
				?>
				</td>
			</tr>
			<tr>
				<th>Prix remisé</th>
			</tr>
			<tr>
				<td>
					<input type="text"<?php echo $disabled ?> class="textInput price" name="discount_price_<?php echo $creditItem->get( "idrow" ) ?>" value="<?php echo Util::numberFormat( $creditItem->get( "discount_price" ) ) ?>" onkeypress="return forceUnsignedFloatValue( event );" style="width:40px;" />&nbsp;&euro;
					<input type="submit"<?php echo $disabled ?> name="UpdateDiscountPrice_<?php echo $creditItem->get( "idrow" ) ?>" value="Ok" class="blueButton" style="text-align:center;cursor:pointer;" />
				</td>
			</tr>
		</table>
	</div><!-- tableContainer -->
	<div class="leftContainer">
        <input type="submit" name="DeleteItem_<?php echo $creditItem->get( "idrow" ) ?>" class="blueButton" value="Supprimer" onclick="deleteReference( <?php echo $creditItem->get( "idrow" ) ?>, '<?php echo $invoiceItem->get( "reference" ) ?>' ); return false;" />
    </div>
    <div class="rightContainer">
        <input type="button" onclick="window.open('<?php echo "$GLOBAL_START_URL/statistics/statistics.php?cat=product&reference=" . urlencode( $invoiceItem->get( "reference" ) ) . "&amp;period&amp;StatStartDate=" . urlencode( date( "d/m/Y", mktime( 0, 0, 0, date( "m" ) - 6, date( "d" ), date( "Y" ) ) ) ) . "&amp;StatEndDate=" . urlencode( date( "d/m/Y" ) ) ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px; margin-right:5px;" />
        <input type="submit" class="blueButton" name="" value="Recalculer" />
    </div>
    <div class="clear"></div>
	<?php 
	
}


//-------------------------------------------------------------------------------------------------

function displayCreditedCharges( &$credit ){

	global $GLOBAL_START_URL;
	
	//créditer les frais de port facturés
	
	if( $credit->get( "credited_charges" ) > 0.0 ){
		
		?>
		<div class="tableContainer" style="margin-bottom:5px;">
			<table class="dataTable devisTable">
				<tr>
					<th style="width:75px;">Photo</th>
					<th>Réf. & Désignation courte</th>
					<th>Quantité facturée</th>
					<th>Montant H.T. Facturé</th>
					<th>Montant H.T.</th>			
			    </tr>
				<tr>
					<td><img src="<?php echo $GLOBAL_START_URL ?>/www/img_prod/photo_missing_icone.jpg" alt="" /></td>
					<td>Frais de port</td>
					<td>1</td>
					<td><?php echo Util::priceFormat( $credit->getInvoice()->get( "total_charge_ht" ) ) ?></td>
					<td><input type="text" class="textInput price" name="credited_charges" value="<?php echo Util::numberFormat( $credit->get( "credited_charges" ) ) ?>" onkeypress="return forceUnsignedFloatValue( event );" /> &euro;</td>
			    </tr>
	    	</table>
	    </div><!-- tableContainer -->
	    <div class="leftContainer">
	        <input type="button" name="" class="blueButton" value="Supprimer" onclick="document.forms.frm.elements[ 'credited_charges' ].value = '0.0'; document.forms.frm.submit();" />
	    </div>
	    <div class="clear"></div>
		<?php
		
	}
	else{
		
		?>
		<input type="hidden" name="credited_charges" value="0.0" />
		<?php
			
	}

}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	if( $idproduct ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" onclick="window.open(this.href); return false;">
		<?php 
		
	}
	
	?>
	<img src="<?php echo URLFactory::getReferenceImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$creditItem, &$invoiceItem ){
	
	global $GLOBAL_START_URL;

	/*référence catalogue*/
	
	?>
	<p>Référence catalogue : <?php echo $invoiceItem->get( "reference" ) ?></p>
	 <?php
	 
	 /*référence et infos fournisseur*/
	 
	 if( $invoiceItem->get( "idsupplier" ) ){
		
		?>
		<p>
			<span class="lightGrayText">Réf. fournisseur : <?php echo DBUtil::getDBValue( "ref_supplier", "detail", "idarticle", $invoiceItem->get( "idarticle" ) ) ?></span>
			&nbsp;
			<a href="#" onclick="showSupplierInfos( <?php echo $invoiceItem->get( "idsupplier" ) ?> ); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
			</a>
		</p>
		<?php 

	 }

	/*lien fiche PDF*/
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $invoiceItem->get( "idarticle" ) ) ){
		
		$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $invoiceItem->get( "idarticle" ) );
		
		$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : "/catalog/pdf_product.php?idproduct=$idproduct";
		
		?>
		<p>
			<a class="blueLink" href="<?php echo $href ?>" onclick="window.open(this.href); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
			</a>
		</p>
		<?php
			
	}
								 	
}

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){

	$designation = html_entity_decode(html_entity_decode($item->get( "designation")));
	$designation = str_replace("\n",'', $designation);
	$designation = str_replace("\r",'', $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = strip_tags($designation);
	
	
	return $designation;
		
}

//-----------------------------------------------------------------------------------------------------

?>