<?php anchor( "UpdateComment" ); ?>
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Commentaires</p>
				<div class="rightContainer"></div><!-- correction d'un bug IE -->
			</div>
	        <div class="clear" style="margin-top:10px;">
	        	<textarea<?php echo $disabled ?> class="txtArea" style="width:100%; height:100px;" name="comment"><?php echo htmlentities( $credit->get( "comment" ) ) ?></textarea>
	        </div> 
	        <div class="leftContainer">
		        <input type="submit" name="" class="blueButton" value="Modifier" style="margin-top:5px;" />
		    </div> 
		    <div class="clear"></div> 
		</div><!-- subContent -->
	</div><!-- content -->
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div><!-- mainContent -->