<?php $customer = new Customer( $credit->get( "idbuyer" ), $credit->get( "idcontact" ) ); ?>
<script type="text/javascript">
/* <![CDATA[ */

	/*PopUp pour voir l'historique d'un client*/
	function seeBuyerHisto(){
	
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-1010)/2;
		window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $credit->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0');
	}
	
/* ]]> */
</script>
<div class="mainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    <div class="topRight"></div>
    <div class="topLeft"></div>
    <div class="content">
    	<div class="headTitle">
            <p class="title">
            	Avoir n° <?php echo $credit->getId() ?>
				créé à partir de la facture n° <a href="<?php echo "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=" . $credit->get( "idbilling_buyer" ) ?>" target="blank"><?php echo $credit->get( "idbilling_buyer" ) ?></a>
            </p>
            <div class="rightContainer date"><?php echo humanReadableDatetime( $credit->get( "DateHeure" ) ) ?></div>
            <div class="clear"></div>
        </div><!-- headTitle -->
        <div class="subHeadTitle">
			<div class="subHeadTitleElementRight">
                <b>Commercial affaire</b>
                <?php 
	            	
	            	/*commercial affaire*/
	            	
            		if( User::getInstance()->get("admin") ){
            			
            			include_once( dirname( __FILE__ ) . "/../../../script/user_list.php" );
						userList( true, $credit->get( "iduser" ) );
	
	            		//DrawLift_commercial("user", $credit->get( "iduser" ) );
	            		
	            		?>
	            		<input type="image" name="ComChanged" id="ComChanged" alt="Modifier" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" style="vertical-align:middle; margin-left:5px;" />
	            		<?php
	            		
            		}
	            	else echo htmlentities( Util::getDBValue( "firstname", "user", "iduser", $credit->get( "iduser" ) ) . " " . Util::getDBValue( "lastname", "user", "iduser", $credit->get( "iduser" ) ) );
	            	
			?>
            </div>
            <p class="subHeadTitleElementLeft" style="margin-top:5px;"><b>Commercial client : </b><?php echo DBUtil::getDBValue( "firstname", "user", "iduser", $credit->getCustomer()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $credit->getCustomer()->get( "iduser" ) ) ?></p>
    		<div class="clear" style="margin-bottom:30px; margin-top:5px;">
                <div style="float:left;">STATUT DE L'AVOIR : <?php echo $credit->get( "editable" ) ? "En cours" : "Validé" ?></div>
			</div>
    	</div><!-- subHeadTitle -->
   		<div class="subContent">
			<!-- Coordonnées société -->
        	<div class="subTitleContainer">
                <p class="subTitle">Coordonnées société</p>
			</div>
	        <div class="tableContainer">
            	<table class="dataTable">
                    <tr class="filledRow">
                        <th><?php echo Dictionnary::translate( $credit->getCustomer()->get( "contact" ) ? "idcontact" : "idbuyer" ); ?></th>
                        <td colspan="2"><?php echo $credit->getCustomer()->get( "idbuyer" );  ?> <a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?type=<?php echo $credit->getCustomer()->get( "contact" ) ? "contact" : "buyer" ?>&amp;key=<?php echo $credit->getCustomer()->get( "idbuyer" );  ?>" onclick="window.open(this.href); return false;" ><?php echo Dictionnary::translate( $credit->getCustomer()->get( "contact" ) ? "gest_com_edit_contact" : "gest_com_edit_buyer" ) ; ?></a>&nbsp;&nbsp;&nbsp;</td>
                        <td colspan="4"><a href="#" class="blueLink" onclick=""seeBuyerHisto(); return false;"><?php echo Dictionnary::translate( $credit->getCustomer()->get( "contact" ) ? "gest_com_view_contact_story" : "gest_com_view_buyer_story" ) ; ?></a></td>
                    </tr>
                    <tr>
                        <th>Nom / Raison sociale</th>
                        <td colspan="2"><?php echo $credit->getCustomer()->get( "company" ); ?></td>
                        <th colspan="2"><?php echo str_replace("<br />","",Dictionnary::translate( $credit->getCustomer()->get( "contact" ) ? "contact_source" : "buyer_source" ) ) ?></th>
                        <td colspan="2"><?php echo DBUtil::getDBValue( "source_1", "source", "idsource", $credit->getCustomer()->get( "idsource" ) ); ?></td>
                    </tr>
                     <tr>
                        <th>Adresse</th>
                        <td colspan="6"><?php echo $credit->getCustomer()->get( "adress" ); ?></td>
                    </tr>
                    <tr>
                        <th>Adresse complémentaire</th>
              			<td colspan="6"><?php $adr2= $credit->getCustomer()->get( "adress_2" );if(empty($adr2)){echo "&nbsp;";}else{ echo $adr2;} ?></td>
                    </tr>
                    <tr>
                        <th>Code postal</th>
                        <td><?php echo $credit->getCustomer()->get( "zipcode" ); ?></td>
                        <th>Ville</th>
                        <td colspan="2"><?php echo $credit->getCustomer()->get( "city" ); ?></td>
                        <th>Pays</th>
                        <td><?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $credit->getCustomer()->get( "idstate" ) ); ?></td>
                    </tr>
                    <tr class="filledRow">
                        <th>Nom</th>
                        <td colspan="2"><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $credit->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $credit->getCustomer()->getContact()->get( "lastname" ); ?></td>
                        <th colspan="2">Prébom</th>
                        <td colspan="2"><?php echo $credit->getCustomer()->getContact()->get( "firstname" ); ?></td>
                    </tr>
                    <tr>
                        <th>Téléphone</th>
                        <td colspan="2"><?php echo Util::phonenumberFormat($credit->getCustomer()->getContact()->get( "phonenumber" )); ?></td>
                        <th colspan="2">Fax</th>
                        <td colspan="2"><?php echo Util::phonenumberFormat($credit->getCustomer()->getContact()->get( "faxnumber" )); ?></td>
                    </tr>
                    <tr>
                        <th>E-mail</th>
                        <td colspan="2"><a class="blueLink" href="mailto:<?php echo $credit->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $credit->getCustomer()->getContact()->get( "mail" ); ?></a></td>
                        <th colspan="2">GSM</th>
                        <td colspan="2"><?php echo Util::phonenumberFormat( $credit->getCustomer()->getContact()->get( "gsm" ) ) ?></td>
                    </tr>
                    <tr>
                        <th>Statut demande assurance</th>
                        <td colspan="2">
						<?php
							switch( $customer->get( "insurance_status" ) ){
								case "Accepted":
									echo "accepté";
									break;
									
								case "Refused":
									echo "refusé";
									break;

								case "Deleted":
									echo "supprimé";
									break;
									
								case "Terminated":
									echo "résilié";
									break;

								default:
									echo "non assuré";
									break;
							}
							
						?>
						</td>
						<th colspan="2">
						<?php
							switch( $customer->get( "insurance_status" ) ){
								case "Accepted":
									echo "Date d'assurance";
									break;
									
								case "Refused":
									echo "Date de refus";
									break;

								case "Deleted":
									echo "Date de suppression";
									break;
									
								case "Terminated":
									echo "Date de résiliation";
									break;

								default:
									echo "";
									break;
							}						
									
						?>
						</th>
						<td colspan="2">
						<?php
							switch( $customer->get( "insurance_status" ) ){
								case "Accepted":
									echo Util::dateFormatEu( $customer->get( "insurance_amount_creation_date" ) );
									break;
									
								case "Refused":
									echo Util::dateFormatEu( $customer->get( "insurance_refusal_date" ) );
									break;

								case "Deleted":
									echo Util::dateFormatEu( $customer->get( "insurance_deletion_date" ) );
									break;
									
								case "Terminated":
									echo Util::dateFormatEu( $customer->get( "insurance_deletion_date" ) );
									break;									
							}
									
						?>
						</td>
					</tr>
					<tr>
						<th>Encours garanti HT</td>
						<td colspan="2"><?php echo $credit->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->get( "insurance_amount" ) ) : "-"; ?></td>
						<th colspan="2">Encours assurance HT</td>
						<td colspan="2"><?php echo $credit->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->getInsuranceOutstandingCreditsET() ) : "-"; ?></td>
					</tr>
               </table>
			</div><!-- tableContainer -->
		</div><!-- subContent -->
		<div class="subContent">
			<!-- Coordonnées société -->
        	<div class="subTitleContainer">
                <p class="subTitle">Objet de l'avoir</p>
			</div><!-- subTitleContainer -->
            <div class="clear" style="margin-top:10px;">
	            <textarea name="object" class="txtArea" style="width:100%;" <?php echo $disabled ?>><?php echo htmlentities( $credit->get( "object" ) ); ?></textarea>
            </div>	
       	</div><!-- subContent -->
	</div><!-- content -->
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div><!-- mainContent fullWidthContent -->
<div id="tools" class="rightTools">
	<div class="toolBox">
		<div class="header">
	    	<div class="topRight"></div><div class="topLeft"></div>
	        <p class="title">Outils</p>
	    </div>
	    <div class="content" style="text-align:center; padding-top:5px; padding-bottom:5px;">
		</div><!-- content -->
	    <div class="footer">
	    	<div class="bottomLeft"></div>
	        <div class="bottomMiddle"></div>
	        <div class="bottomRight"></div>
	    </div>
	</div><!-- toolBox -->
	<div class="toolBox">
    	<div class="header">
        	<div class="topRight"></div>
        	<div class="topLeft"></div>
            <p class="title">Visualiser et imprimer</p>
        </div>
	    <div class="content" style="padding:5px;">
	    <?php
	    
	    	if( !strlen( $credit->getCustomer()->getContact()->get( "mail" ) ) ){
						
				?>
				<div class="subTitleContainer">
					<p class="subTitleWarning"><?php  echo Dictionnary::translate("gest_com_buyer_without_mail_order") ; ?></p>
				</div>
				<?php
				
			}
			
			?>
			<div style="float:left;">
				<a class="normalLink" style="display:block;" href="/catalog/pdf_credit.php?idcredit=<?php echo $credit->getId() ?>" onclick="window.open(this.href); return false;">Avoir PDF n°<?php echo $credit->getId() ?></a>
				<a class="normalLink" style="display:block;" href="/catalog/pdf_invoice.php?idinvoice=<?php echo $credit->get( "idbilling_buyer" ) ?>" onclick="window.open(this.href); return false;">Facture PDF n°<?php echo $credit->get( "idbilling_buyer" ) ?></a>
				<?php
				
				if( strlen( $credit->getCustomer()->getContact()->get( "mail" ) ) ){
					
					?>
					<a class="normalLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/sales_force/transmit_credit.php?idcredit=<?php echo $credit->getId() ?>" onclick="window.open(this.href); return false;">Mail d'accompagnement</a>
					<?php
					
				}
				
			?>
			</div>
			<div style="float:right; margin-left:20px;"></div>
            <div style="clear:both;"></div>
		</div><!-- content -->
	    <div class="footer">
	    	<div class="bottomLeft"></div>
	        <div class="bottomMiddle"></div>
	        <div class="bottomRight"></div>
	    </div>
	</div><!-- toolBox -->
</div><!-- tools -->
<br style="clear:both;" />