﻿<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div><div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Montants et tarifications de l'avoir</p>
				<div class="rightContainer"></div><!-- correction d'un bug IE -->
        	</div>
			<div class="tableContainer">
				<table class="dataTable devisTable">
					<tr>
						<th colspan="2">Port refacturé</th>
						<th rowspan="2">Montant Net HT</th>
						<th colspan="2">Remise sur facture</th>
						<th rowspan="2">Montant TVA</th>
						<th colspan="2">Montant Total</th>
						<!--<th rowspan="2">Montant TTC</th>-->
					</tr>
					<tr>
						<th>Port Net HT</th>
						<th>Port TTC</th>
						<th>%</th>
						<th>&euro;</th>
						<th>Net HT</th>
						<th>TTC</th>
					</tr>
					<tr>
						<td style="text-align:left; width:100px;">
							<input <?php echo $disabled ?> style="width:30px;" class="textInput price" type="text" id="total_charge_ht" name="total_charge_ht" value="<?php echo Util::numberFormat( $credit->get( "total_charge_ht" ) ) ?>" onkeypress="return forceUnsignedFloatValue( event );" />&nbsp;&euro;
							<input <?php echo $disabled ?> type="submit" value="Ok" class="blueButton" />
						</td>
						<td>
						<?php
						
							$total_charge_ET 	= $credit->get( "total_charge_ht" );
							$vat_rate 			= $credit->get( "vat_rate" );
							$total_charge_ATI 	= $total_charge_ET * ( 1.0 + $vat_rate / 100.0 );
		
							echo Util::priceFormat( $total_charge_ATI );
							
						?>
						</td>
						<td><?php echo Util::priceFormat( $credit->getTotalET() ) ?></td>
						<td style="text-align:left; width:100px;">
						<?php
							
							//vérifier s'il y a une remise globale
							
							$hasGlobalDiscount = true;
							$item_discount_rate = 0.0;
							
							if( $credit->getItemCount() ){
								
								$item_discount_rate = $credit->getItemAt( 0)->get( "discount_rate" );
								$i = 0;
								while( $i < $credit->getItemCount() ){
				
									$hasGlobalDiscount = $item_discount_rate == $credit->getItemAt( $i)->get( "discount_rate" );
									
									$i++;
									
								}
							
							}
							
							$discount_rate 		= $hasGlobalDiscount ? $item_discount_rate : 0.0;
							$discount_amount 	= $hasGlobalDiscount ? $credit->getTotalET() * $discount_rate / 100.0 : 0.0;
								
							?>
		                	<input <?php echo $disabled ?> style="width:30px;" class="textInput rate" type="text" name="discount_rate" value="<?php echo Util::numberFormat( $discount_rate ) ?>" onkeypress="return forceFloatValue( event );" style="width:55px;" />% &nbsp;
							<input <?php echo $disabled ?> class="blueButton" type="submit" name="UpdateDiscountRate" value="Ok" />
						</td>
						<td><?php echo Util::priceFormat( $discount_amount ) ?></td>
						<td><?php echo Util::priceFormat( $credit->getTotalATI() - $credit->getTotalET() ); ?>
						</td>
						<td>
                        <?php
												 $total_amount_ht=$credit->get( "total_amount_ht" )?$credit->get( "total_amount_ht" ):$credit->getTotalET();
												  $total_amount=$credit->get( "total_amount" )?$credit->get( "total_amount" ):$credit->getTotalATI();
						?>
                        <input style="width:70px;" class="textInput price" type="text" id="total_amount_ht" name="total_amount_ht" value="<?php echo Util::numberFormat( $total_amount_ht)  ?>" />&nbsp;&euro;</td>
                        
                        
						<td> <input style="width:70px;" class="textInput price" type="text" id="total_amount" name="total_amount" value="<?php echo Util::numberFormat($total_amount ) ?>"/>&nbsp;&euro;</td>
					<!--	<td><?php echo Util::priceFormat( $credit->getTotalATI() ) ?></td> -->
					</tr>
				</table>
			</div><!-- tableContainer -->                
		</div><!-- subContent -->
    </div><!-- content -->
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div><!-- mainContent -->