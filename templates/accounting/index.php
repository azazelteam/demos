<?php

include( dirname( __FILE__ ) . "/../objects/classes.php" );

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
header( "Content-Type: text/html; charset=utf-8" );
?>

<style type="text/css">
	html, body {
		background-color:#84A5C6;
	}
	div#global div#header div#menu {
		border-bottom:1px solid #FFFFFF;
	}
</style>

<div style="margin-left:auto; margin-right:auto; ">

<div class="mainContentDecision">	
	<h1>Comptabilité</h1>
	<div style="clear:both;"></div>
	
	<div class="leftColumn">
		<p class="type">Facturation clients</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/com_admin_search_bl.php">Cdes à facturer</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/invoice_search.php">Rechercher une facture</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/provisionnal_next_invoices.php">En attente de facturation</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/litigation_search.php">Rechercher un litige</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/credit_search.php">Rechercher un avoir</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/factor_invoices.php#import">Factures à céder au factor</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/factor_refusal.php">Définancement factor</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/insurance.php">Assurance Crédit</a></li>
		</ul>
	</div>
	
	<div class="leftColumn">
		<p class="type">Facturation fournisseurs</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/received_supplier_invoices.php">Enregistrement factures</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices_to_pay.php">Factures a payer</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/search_supplier_invoices.php">Recherche factures</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/next_supplier_invoices.php">Futures factures</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/register_supplier_proforma.php">Enregistrement facture proforma</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/search_supplier_proforma.php">Recherche facture proforma</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_credits_requests.php">Enregistrement avoir</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_credits_search.php">Rechercher un avoir</a></li>			
		</ul>
	</div>
	
	<div class="leftColumn">
		<p class="type">Règlements clients</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/invoices_follow-up.php">Factures en retard de règlement</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/payment_registration.php">Enregistrement des règlements</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/com_admin_list_notpayed.php">Factures non échues</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/regulations_buyer_search.php">Rechercher un règlement client</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/regulations_on_bad_account.php">Erreurs affectation Banque / Factor</a></li>
		</ul>
	</div>
	<div style="clear:both;"></div><br/>
	
	<div class="leftColumn">
		<p class="type">Décisionnel</p>	
		<ul>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/statistics/statistics.php?cat=marge_billing">Facturation clients</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/statistics/statistics.php?cat=supplier_invoice_evol">Evol. facturations / fournis.</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/statistics/statistics.php?cat=comp_cust_and_sup_invoice">Différentiel règlement clients/fournis.</a></li>
			
		</ul>
	</div>
	
	<div class="leftColumn">
		<p class="type">Règlements fournisseurs</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/regulations_supplier_search.php">Rechercher un règlement fournisseur</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_due_date_file.php">Echeancier fournisseur</a></li>
		</ul>
	</div>
	
	<div class="leftColumn">
		<p class="type">Remboursements client</p>
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/repayments.php">Rechercher un règlement client</a></li>
		</ul>	
	</div>
	
	
	<div style="clear:both;"></div><br/>
	
		
	<div class="leftColumn">
		<p class="type">Comptable</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/account_plan.php">Ecritures comptables</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/account_plan.php">Interrogation des tiers</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/balance.php">Balance des tiers</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/bank_reconciliation.php">Rapprochement bancaire</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/accounts_listing_bank.php">Relevés Crédit Mutuel</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/accounts_listing_factor.php">Relevés Factor Règlements</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/bookkeeping_entries.php">Relevés Factor CCV</a></li>
		</ul>
	</div>
	
	<div class="leftColumn">
		<p class="type">Interface SAGE</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/sage_buyer_export.php">Export client</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/sage_factor_export.php">Export factor</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/sage_sales_export.php">Export des ventes</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/sage_factor_refusal_export.php">Export définancement factor</a></li>
		</ul>
	</div>
	
	<div class="leftColumn">
		<p class="type">Trésorerie</p>	
		<ul>		
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/situation.php">Situation clients / fournisseurs</a></li>
			<li><a href="<?php echo $GLOBAL_START_URL; ?>/accounting/situation_bb.php">Situation fournisseurs / clients</a></li>
		</ul>
	</div>
	
	
	
	
	

</div>



<?php

//include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

?>

</div>
</body>
</html>