<style type="text/css">

	div, p, table td, table th{
	
		font-family:Verdana, Arial; 
		font-size:12px; 
		text-align:left;
		
	}
	
	table th{ background-color:#E7E7E7; }
	
</style>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div style="border: medium none; margin: 0pt; padding: 0pt; height: 100%; width: 100%;">
	<div style="padding: 0pt 10px; background: #ffffff none repeat scroll 0% 0%; margin-left: auto; margin-right: auto; width: auto; height: 100%;">
		<table style="font-family:Verdana, Arial; color:#505050;" border="0" cellspacing="0" cellpadding="0" width="auto">
			<tbody>
				<tr>
					<td>
						<a href="http://www.akilae-saas.fr"><img src="../images/front_office/mail/logo.jpg" border="0" alt="Axess industries" width="170" /></a>
					</td>
					<td>
						<p>Strasbourg, le {date:h}</p>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top:20px;">
						<p>Vous trouvez ci-dessous la liste des factures qui nous ont été réglé par nos client :</p>
						<table style="border-collapse:collapse; border:1px solid #505050" border="1" cellspacing="0" cellpadding="3" width="auto">
							<tr>
								<th>Facture n°</th>
								<th>Client n°</th>
								<th>Code postal</th>
								<th>Raison sociale</th>
								<th>Date de création</th>
								<th>Total HT</th>
								<th>Total TTC</th>
							</tr>
							{foreach:invoices,invoice}
								<tr>
									<td>{invoice[idinvoice]}</td>
									<td>{invoice[idcustomer]}</td>
									<td>{invoice[zipcode]}</td>
									<td>{invoice[company]:h}</td>
									<td>{invoice[creation_date]:h}</td>
									<td style="text-align:right;">{invoice[totalET]:h}</td>
									<td style="text-align:right;">{invoice[totalATI]:h}</td>
								</tr>
							{end:}
						</table>
						<p>Nous vous souhaitons une excellente journ&eacute;e.</p>
						<p>Cordialement,</p>
						<p>
							{salesman[name]} {salesman[lastname]}
							<br /> T&eacute;l. : {salesman[phonenumber]} - Fax :  {salesman[faxnumber]}
							<br /> Mail : <a href="mailto:{salesman[email]}">{salesman[email]}</a>
						</p>
						<p style="font-size:12px; letter-spacing:14px; text-align:center; color:#808080; margin-top:30px;"><strong>www.akilae-saas.fr</strong></p>
						<p style="font-size:10px; line-height:14px; text-align:center; color:#989797;">
						
							<br /> Tel. : - Fax : + - Mail : 
							<br /> 
						</p>
						
						
					</td>
				</tr>
				
			</tbody>
		</table>
	</div>
</div>