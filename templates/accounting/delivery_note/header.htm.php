<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE BUYER ++++++++++++++++++++++++++++++++++++++++++ -->
<script type="text/javascript" language="javascript">
<!--
// -->
</script>
<div id="ContentDiv" style="margin-top:10px;">
	<table border="0" cellspacing="0" cellpadding="3" style="width:100%; background-color:#FFFFFF; margin-top:10px;">
		<tr>
			<td class="TopLeftCorner"></td>
			<td class="FrameBorderTop"></td>
			<td class="TopRightCorner"></td>
			<td class="Empty" colspan="2"></td>
		</tr>
		<tr>
			<td class="FrameBorderLeft"></td>
			<td id="TitleCell">
			<span class="Order_num"><?php echo Dictionnary::translate( "gest_com_order_supplier" ) ?>&nbsp;<?php echo $idorderS ?></span>
			<br />
			<span class="Estimate_date">Gestion des bons de livraisons multiples</span>
			</td>
			<td class="FrameBorderRight"></td>
			<td class="TableTabRight" style="width:200px;" bgcolor="#F7F7F7"><a href="#" onclick="history.go(-1);" class="retour"><img src="<?php echo $GLOBAL_START_URL ?>/images/retour.jpg" border="0" align="top">&nbsp;<?php  echo Dictionnary::translate("back") ; ?></a></td>
			<td class="TableTabRight" style="width:10px;"></td>
		</tr>
		<tr>
			<td class="FrameBorderLeft"></td>
			<td colspan="3" class="TableContent">
				<br />
				<table cellspacing="0" cellpadding="4" width="100%">
					<tr>
						<td class="Estimate_statut" width="20%" align="left"></td>
						<td width="30%" align="left"></td>
						<td align="right">
						<table>
						<tr>
						<td>
						<?php
	
	if( strlen( $orders->getBuyer( "email" ) ) ){
		
		?>
		<b><?php  echo Dictionnary::translate("gest_com_send_by_mail") ; ?></b><br />
		<a href="#" onclick="alert( 'Tes chaussettes sont sales!' ); return false;" class="fichier"><?php  echo Dictionnary::translate("gest_com_invoice_confirmation") ; ?></a><br />
		<?php 
		
	} 
	?>
	</td>
	<td width="30">&nbsp;</td>
	<td>
	<?php 
	echo "<b>".Dictionnary::translate('gest_com_view_pdf')."</b><br />";
	?> 
	<a href="getcredit.php?idcredit=<?php echo $orders->getId() ?>" class="Lien" target="_blank"><?php  echo Dictionnary::translate("gest_com_credit_PDF") ; ?>_<?php echo $orders->getId() ?>.pdf</a>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	</table>
	<table cellpadding="3" cellspacing="0" border="0">
	<tr>
		<td class="Grd_Titre" style="text-align:left;">Nombre de BL à créer</td>
	</tr>
	<tr>
		<td>
			<table cellspacing="0" cellpadding="4" class="Buyer_Table" align="center" style="margin-top:10px;">
				<tr>
					<th width="200">Nombre de BL</th>
					<td width="200" colspan="3"><input type="text" name="nb">&nbsp;&nbsp;<input type="submit" name="Generate" value="Générer"></td>	
				</tr>
				<?if(isset($nb)){
					
					$dispo = getArticlesQuantities($idorderS);
					?>
					<tr>
						<td colspan="2">&nbsp;</td>	
					</tr>
					<tr>
						<th width="200">Quantités disponibles</th>
						<td width="200" colspan="3">
						<?
						foreach($dispo as $ref){
							
							$reference = $ref[0];
							$quantity = $ref[1];
							
							echo "<b>".$quantity."</b>&nbsp;&nbsp;&nbsp;x&nbsp;&nbsp;&nbsp;".$reference."<br />";
						}
						?>
						</td>	
					</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	</table>
<?php

GraphicFactory::table_end();

//-------------------------------------------------------
function HumanReadablePhone($tel){
	//On regarde la longeueur de la cheine pour parcourir
	$lg=strlen($tel);

	$telwcs='';
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($tel,$i,1);
	
		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if($i==0){
			if(($tocheck=='+')OR(is_numeric($tocheck))){
				$telwcs.=$tocheck;
			}
		}else{
			//On ne garde que les chiffres
			if(is_numeric($tocheck)){
				$telwcs.=$tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg=strlen($telwcs);

	$telok ='';

	
	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck=substr($telwcs,0,1);

	if($tocheck=='+'){
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,3);
		$telok.=$tocheck." ";
		$cpt=3;
	}else{
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,2);
		$telok.=$tocheck." ";
		$cpt=2;
	}

	//On traite le reste de la chaine
	for($i=$cpt;$i<$lg;$i+=2){
		if($i==$cpt){
			$tocheck=substr($telwcs,$i,2);
		}else{
			$tocheck=substr($telwcs,$i,2);
		}
		$telok.=$tocheck." ";
	}	

	//On enlève l'espace de fin
	$telok=trim($telok);

	return $telok;
}
?>