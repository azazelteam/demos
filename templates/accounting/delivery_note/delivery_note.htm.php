<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<?php

anchor( "UpdateRefDiscountRate_", true );
anchor( "UpdateDesignation_", true );
anchor( "UpdRow_", true ); 

?>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" language="javascript">
<!--

	function deleteReference( idrow, reference ){
	
		if( confirm( 'Confirmez la suppression de : ' + reference ) ){
		
			document.getElementById( 'delete_' + idrow ).value = 1;
			document.forms.frm.submit();
		
		}
		
		return false;
		
	}

	function showSupplierInfos( idsupplier){

		if( idsupplier == '0' )
			return;
			
		var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=650, height=650';
		var location = 'supplier_infos.php?idsupplier=' + idsupplier + '&ispopup=true';
		
		window.open( location, '_blank', options ) ;
		
	}
	
	function initWYSIWGEditors() {
	
		tinyMCE.init({
				mode : "textareas",
				editor_selector : "WYSIWYGEditor",
				theme : "simple",
				plugins : "preview,contextmenu,paste,directionality,noneditable",
				theme_advanced_buttons1 : "newdocument,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,cleanup,|,preview",
				theme_advanced_buttons2 : "bold,italic,underline,strikethrough,|,bullist",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : false,
		});
			
	}
	
	function html2wysiwyg( HTMLDiv ){
				
		var html = HTMLDiv.innerHTML;
		var elementID = HTMLDiv.getAttribute( 'id' );
		
		var parentID = elementID + '_parent';
		var parentNode = document.getElementById( parentID );
		
		var hiddenID = elementID + '_hidden';
		var hiddenElement = document.getElementById( hiddenID );
		
		parentNode.removeChild( HTMLDiv );
		parentNode.removeChild( hiddenElement );
		
		var useragent = navigator.userAgent;

		if( useragent.toLowerCase().indexOf( 'msie' ) == -1 ){

			var textarea = document.createElement( 'textarea' );

			textarea.setAttribute( 'id', elementID ); 
			textarea.setAttribute( 'name', elementID );
			textarea.setAttribute( 'class', 'WYSIWYGEditor' );
			
			textarea.value = html;
			textarea.style.width = '100%';
			textarea.style.height = '120px';

			parentNode.style.width = '100%';
			parentNode.style.height = '120px';
			
			parentNode.appendChild( textarea );

			initWYSIWGEditors();
			
			textarea.setAttribute( 'class', '' );
			
		}
		else{
		
			parentNode.style.width = '100%';
			parentNode.style.height = '120px';
			
			var innerHTML = '<textarea id="' + elementID + '" name="' + elementID + '" class="WYSIWYGEditor" style="height:120px; width:100%;">' + html + '</textarea>';
			
			parentNode.innerHTML = innerHTML;

			initWYSIWGEditors();

			document.getElementById( elementID ).className = '';
			
		}

	}
		
	function selectReference(){
	
		var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=750, height=650';
		var location = 'invoice_references.php?idcredit=' + <?php echo $idorderS ?> + '&ispopup=true';
		
		window.open( location, '_blank', options ) ;
		
	}
		
// -->
</script>
<a name="references"></a>
<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/multi_delivery_note.php?idorderS=<?php echo $idorderS ?>" />
<div class="Onglets_rouge" style="width:180px;"><a href="#" onclick="selectReference(); return false;"><?php  echo Dictionnary::translate("gest_com_by_reference") ; ?></a></div>
<table cellspacing="0" cellpadding="3" border="0" style="width:950px;">
	<tr>
		<td class="Infos_contour" align="center" bgcolor="#CDCDDO" colspan="9">	
			<table width="910" cellspacing="0" style="margin:10px;" bgcolor="#FFFFFF">
				<tr>
					<td>
						<table border="1" class="Infos_Table" width="900" cellspacing="0" style="margin:10px;">
						    <tr style="height:35px;">
						    	<th><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="" /></th>
								<th><?php echo Dictionnary::translate( "photo" ) ; ?></th>
								<th><?php echo Dictionnary::translate( "ref" ) ; ?> & <?php echo Dictionnary::translate( "designation" ) ?></th>
								<th><?php echo Dictionnary::translate( "gest_com_ref_quantity" ) ?></th>
						    </tr>
							<?php

	$it =& $orders->iterator();
	
	$i=0;
	while( $it->hasNext() ){
		
		//$creditItem 	=& $it->next();
		//$invoiceItem 	=& $invoice->getItemAt( $creditItem->getValue( "idbilling_buyer_row" ) - 1 );
		
		$idarticle = $orders->getArticleValue( $i,"idarticle" );
		$reference = $orders->getArticleValue($i,"reference");
		
		//lien vers la fiche produit
		
		$lang 			= Session::getInstance()->getLang();	
		$idproduct 		= DBUtil::getDBValue( "idproduct", "detail", "idarticle", $idarticle );
		$productName 	= DBUtil::getDBValue( "name$lang", "product", "idproduct", $idproduct );
		$productURL 	= getProductURL( $idproduct, $productName );

		?>
		<tr style="height:35px;">
			<td>
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="" onclick="deleteReference( <?php echo $orders->getArticleValue( $i, "idorder_row" ) ?>, '<?php echo $orders->getArticleValue( $i,"reference" ) ?>' ); return false;" style="border-style:none; cursor:pointer;" />
				<input type="hidden" id="delete_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>" name="delete_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>" value="0" />
			</td>
			<td>
				<input type="hidden" id="reference_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>" value="<?php echo $orders->getArticleValue( $i,"reference" ) ?>" />
				<a href="<?php echo $productURL ?>" target="_blank">
					<img src="<?php echo $GLOBAL_START_URL ?>/www<?php echo $orders->getArticleValue( $i,"imagedirectory" ) ?>/<?php echo $orders->getArticleValue( $i,"iconname" ) ?>" alt="icone" border="0" />
				</a>
			</td>
			<td>
				<p style="font-weight:bold;">
					<?php  echo Dictionnary::translate("catalog_reference") ; ?> : <a href="<?php echo $productURL ?>" target="_blank" class="CatalogReference"><?php echo htmlentities( $orders->getArticleValue( $i,"reference" ) ) ?></a>
				</p>
				<div id="designation_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>_parent" style="position:relative; text-align:left; height:120px; width:100%; padding:0px; margin-top:10px;">
					<div id="designation_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>" style="cursor:pointer; position:relative; margin:0px; top:0px; left:0px; width:100%; height:auto;" onclick="html2wysiwyg( this );">
						<?php echo $orders->getArticleValue( $i,"designation" ) ?>
					</div>
					<input type="hidden" name="designation_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>" id="designation_<?php echo $orders->getArticleValue( $i,"idorder_row" ) ?>_hidden" value="<?php echo htmlentities( $orders->getArticleValue( $i,"designation" ) ) ?>" />
				</div>
			</td>
			<td><input type="text" name="qtt_$i<?php echo $orders->getArticleValue( $i,"quantity" ) ?></td>
		</tr>
		<?php 
		$i++;
	} //while( $it->hasNext() )
	
						?>
						</table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php

//-----------------------------------------------------------------------------------------------------

function getSupplierPopup( $idsupplier, $index ){

	global 	$Session,
			$GLOBAL_START_URL;
	
	$db =& DBUtil::getConnection();
	$lang = $Session->GetLang();
	
	$query = "
	SELECT * FROM supplier WHERE supplier.idsupplier = '$idsupplier' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les informations du fournisseur '$idsupplier'" );
		
	$supplierInfos = $rs->fields;
	
	?>
	<div id="SupplierInfosDiv_<?php echo $index ?>" style="cursor:help; display:none; width:650px; position:absolute; left:-10px; top:-10px; z-index:99; padding:15px; background-color:#FFFFFF; border:1px solid #B7B7B7;">
	<table class="Supplier_Table" cellspacing="0" cellpadding="2" align="center" style="width:650px;">
		<tr>
			<th><?php echo Dictionnary::translate("company_social") ?></th>
			<td width="150"><?php echo $supplierInfos[ "name" ] ?></td>
			<th><?php echo Dictionnary::translate("name") ?></th>
			<td width="150"><?php echo getManagerTitle( $supplierInfos[ "managertitle" ]  ).' '. $supplierInfos[ "managerfirstname" ] .' '. $supplierInfos[ "managerlastname" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("adress") ?></th>
			<td width="150"><?php echo $supplierInfos[ "adress" ] ?></td>
			<th><?php  echo Dictionnary::translate("adress_2") ?></th>
			<td width="150"><?php echo $supplierInfos[ "adress_2" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("zipcode") ?></th>
			<td width="150"><?php echo $supplierInfos[ "zipcode" ] ?></td>
			<th><?php  echo Dictionnary::translate("city") ?></th>
			<td width="150"><?php echo $supplierInfos[ "city" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("state") ?></th>
			<td width="150">
				<?php echo getDBValue( "name" . Session::getInstance()->getLang(), "state", "idstate", $supplierInfos[ "idstate" ] ) ?>
			</td>
			<th><?php  echo Dictionnary::translate("email") ?></th>
			<td width="150"><a href="mailto:<?php echo $supplierInfos[ "email" ] ?>" class="mail"><?php echo $supplierInfos[ "email" ] ?></a></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("phone") ?></th>
			<td width="150"><?php echo $supplierInfos[ "phonenumber" ] ?></td>
			<th><?php  echo Dictionnary::translate("fax") ?></th>
			<td width="150"><?php echo $supplierInfos[ "faxnumber" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("payment") ?></th>
			<td width="150"><?php echo getPaymentTxt($supplierInfos[ "idpaiement" ]) ?></td>
			<th><?php  echo Dictionnary::translate("payment_delay") ?></th>
			<td width="150"><?php echo getPaymentDelayTxt($supplierInfos[ "pay_delay" ]) ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("select_charge") ?></th>
			<td width="150"><?php echo getSelectCharge($supplierInfos[ "select_charge" ]) ?></td>
			<th><?php  echo Dictionnary::translate("charge_rate") ?></th>
			<td width="150"><?php echo $supplierInfos[ "charge_rate" ] ?></td>
		</tr>
	</table>
	<?php
	
	if( !empty( $supplierInfos[ "comment" ] ) ){
		
		?>
		<table class="Supplier_Table" cellspacing="0" cellpadding="2" align="center" style="width:650px; margin-top:25px;">
			<tr>
				<th colspan="4" style="font-weight:bold;">
					<?php echo Dictionnary::translate( "comment" ) ?> :
				</th>
			</tr>
			<tr>
				<td colspan="4" style="white-space:normal;">
					<?php echo nl2br( $supplierInfos[ "comment" ] ) ?>
				</td>
			</tr>
		</table>
		<?php
		
	}
		
	?>
	<p style="text-align:center; margin-top:20px;">
		<a href="#" onclick="showSupplierInfos( <?php echo $idsupplier ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/btn_modify.jpg" style="border-style:none;" /></a>
	</p>
	</div>
	<?php
		
}

//-----------------------------------------------------------------------------------------------------

function getSelectCharge( $idcharge ){
	
	global 	$Session;
	
	if( empty( $idcharge ) )
		return "";
		
	$lang 	= $Session->GetLang();
	$db 	=& DBUtil::getConnection();
	
	$query = "SELECT charge_select$lang AS name FROM charge_select WHERE idcharge_select = $idcharge LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getManagerTitle( $idtitle ){
	
	global 	$Session;
	
	if( empty( $idtitle ) )
		return "";
		
	$lang 	= $Session->GetLang();
	$db 	=& DBUtil::getConnection();
	
	$query = "SELECT title$lang AS title FROM title WHERE idtitle = $idtitle LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "title" );
	
}


//-----------------------------------------------------------------------------------------------------

function getPaymentTxt( $idpayment ){
	
	global $Session;
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = $Session->GetLang();
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getPaymentDelayTxt( $idpayment ){
	
	global $Session;
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = $Session->GetLang();
	$db = &DBUtil::getConnection();
	
	$query = "SELECT payment_delay$lang AS name FROM payment_delay WHERE idpayment_delay = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

?>