<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE ADRESSES ++++++++++++++++++++++++++++++++++++++++++ -->
<?php

anchor( "UpdateAdresses" );

GraphicFactory::table_start_infos('Gestion des adresses différentes');
		
$default = "-";	

/**
 * Sélection des adresses de livraison/facturation
 */
	 	
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
$idbuyer = $Order->getValue( "idbuyer" );
$where = "WHERE `idbuyer`='$idbuyer'";

//-----------------------------------------------------------------------------------------

//$deliveryAddresses = &GetBuyerDeliveryAddresses( $idbuyer );
$billingAddresses = GetBuyerBillingAddresses( $idbuyer );

//$iddelivery = $Order->getValue( "iddelivery" );
$idbilling = $Order->getValue( "idbilling_adress" );
	 		
//$deliveryCheckStatus = $iddelivery ? "checked=\"checked\"" : "";
$billingCheckStatus = $idbilling ? "checked=\"checked\"" : "";

/*$deliveryCurrentSet = array(

	"iddelivery" 	=> empty( $deliveryCheckStatus ) ? "" : $Order->getValue( "iddelivery" ),
	"title" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "title" ),
	"firstname" 	=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "firstname" ),
	"lastname" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "lastname" ),
	"adress" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "adress" ),
	"adress_2" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "adress_2" ),
	"zipcode" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "zipcode" ),
	"city" 			=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "city" ),
	"idstate" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "idstate" ),
	"company" 		=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "company" ),
	"phonenumber" 	=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "phonenumber" ),
	"faxnumber" 	=> empty( $deliveryCheckStatus ) ? "" : $Order->getForwardingAddress()->getValue( "faxnumber" ),

);*/

$billingCurrentSet = array(

	"idbilling_adress" 	=> empty( $billingCheckStatus ) ? "" : $Order->getValue( "idbilling_adress" ),
	"title" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "title" ),
	"firstname" 		=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "firstname" ),
	"lastname" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "lastname" ),
	"adress" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "adress" ),
	"adress_2" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "adress_2" ),
	"zipcode" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "zipcode" ),
	"city" 				=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "city" ),
	"idstate" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "idstate" ),
	"company" 			=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "company" ),
	"phonenumber" 		=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "phonenumber" ),
	"faxnumber" 		=> empty( $billingCheckStatus ) ? "" : $Order->getInvoiceAddress()->getValue( "faxnumber" ),

);

//Champs obligatoires

//Adresse de livraison
/*$d_company_required = DBUtil::getParameterAdmin("d_company_required");
$d_company_required = $d_company_required ? "<span class=\"asterix\">*</span>" : "";

$d_title_required = DBUtil::getParameterAdmin("d_title_required");
$d_title_required = $d_title_required ? "<span class=\"asterix\">*</span>" : "";

$d_lastname_required = DBUtil::getParameterAdmin("d_lastname_required");
$d_lastname_required = $d_lastname_required ? "<span class=\"asterix\">*</span>" : "";

$d_firstname_required = DBUtil::getParameterAdmin("d_firstname_required");
$d_firstname_required = $d_firstname_required ? "<span class=\"asterix\">*</span>" : "";

$d_address_required = DBUtil::getParameterAdmin("d_address_required");
$d_address_required = $d_address_required ? "<span class=\"asterix\">*</span>" : "";

$d_address2_required = DBUtil::getParameterAdmin("d_address2_required");
$d_address2_required = $d_address2_required ? "<span class=\"asterix\">*</span>" : "";

$d_zipcode_required = DBUtil::getParameterAdmin("d_zipcode_required");
$d_zipcode_required = $d_zipcode_required ? "<span class=\"asterix\">*</span>" : "";

$d_city_required = DBUtil::getParameterAdmin("d_city_required");
$d_city_required = $d_city_required ? "<span class=\"asterix\">*</span>" : "";

$d_state_required = DBUtil::getParameterAdmin("d_state_required");
$d_state_required = $d_state_required ? "<span class=\"asterix\">*</span>" : "";

$d_phonenumber_required = DBUtil::getParameterAdmin("d_phonenumber_required");
$d_phonenumber_required = $d_phonenumber_required ? "<span class=\"asterix\">*</span>" : "";

$d_faxnumber_required = DBUtil::getParameterAdmin("d_faxnumber_required");
$d_faxnumber_required = $d_faxnumber_required ? "<span class=\"asterix\">*</span>" : "";
*/

//Adresse de facturation
$b_company_required = DBUtil::getParameterAdmin("b_company_required");
$b_company_required = $b_company_required ? "<span class=\"asterix\">*</span>" : "";

$b_title_required = DBUtil::getParameterAdmin("b_title_required");
$b_title_required = $b_title_required ? "<span class=\"asterix\">*</span>" : "";

$b_lastname_required = DBUtil::getParameterAdmin("b_lastname_required");
$b_lastname_required = $b_lastname_required ? "<span class=\"asterix\">*</span>" : "";

$b_firstname_required = DBUtil::getParameterAdmin("b_firstname_required");
$b_firstname_required = $b_firstname_required ? "<span class=\"asterix\">*</span>" : "";

$b_address_required = DBUtil::getParameterAdmin("b_address_required");
$b_address_required = $b_address_required ? "<span class=\"asterix\">*</span>" : "";

$b_address2_required = DBUtil::getParameterAdmin("b_address2_required");
$b_address2_required = $b_address2_required ? "<span class=\"asterix\">*</span>" : "";

$b_zipcode_required = DBUtil::getParameterAdmin("b_zipcode_required");
$b_zipcode_required = $b_zipcode_required ? "<span class=\"asterix\">*</span>" : "";

$b_city_required = DBUtil::getParameterAdmin("b_city_required");
$b_city_required = $b_city_required ? "<span class=\"asterix\">*</span>" : "";

$b_state_required = DBUtil::getParameterAdmin("b_state_required");
$b_state_required = $b_state_required ? "<span class=\"asterix\">*</span>" : "";

$b_phonenumber_required = DBUtil::getParameterAdmin("b_phonenumber_required");
$b_phonenumber_required = $b_phonenumber_required ? "<span class=\"asterix\">*</span>" : "";

$b_faxnumber_required = DBUtil::getParameterAdmin("b_faxnumber_required");
$b_faxnumber_required = $b_faxnumber_required ? "<span class=\"asterix\">*</span>" : "";

//-----------------------------------------------------------------------------------------



?>
<script type="text/javascript" language="javascript">
<!--
function displayForm(id) {
	//alert(id);
	tableForm = document.getElementById(id);
	//alert(tableForm);
	if(tableForm) {
		//alert(tableForm.style.display);
		if(!tableForm.style.display || tableForm.style.display == 'none') {
			tableForm.style.display = 'block';
		}
		else {
			tableForm.style.display = 'none';
		}
	}
}
// -->
</script>
<script type="text/javascript" language="javascript">
<!--

	var deliveryAddresses = new Array();
	var billingAddresses = new Array();
	
	<?php
	
		$addressFields = array( 
			"adress", 
			"adress_2", 
			"zipcode", 
			"city", 
			"idstate", 
			"lastname",
			"firstname",
			"company",
			"title",
			"phonenumber",
			"faxnumber"
		);
		
		echo "
		var addressFields = new Array(";
		
		$i = 0;
		while( $i < count( $addressFields ) ){
			
			if( $i )
				echo ", ";
				
			echo "'" . $addressFields[ $i ] . "'";
			
			$i++;
			
		}

		echo ");";

		
		//list billing addresses
		
		$i = 0;
		while( $i < count( $billingAddresses ) ){
			
			$billing = $billingAddresses[ $i ];
			$idBilling = $billing[ "id" ];
			
			echo "
			billingAddresses[ $idBilling ] = new Array();";

			$k = 0;
			while( $k < count( $addressFields ) ){
				
				$field = $addressFields[ $k ];
				
				echo "
				billingAddresses[ $idBilling ][ '$field' ] = '" . addslashes( $billing[ $field ] ) . "';";
				
				$k++;
				
			}
			
			$i++;
			
		}
		
	?>
	
	
	function selectBillingAddress( idbilling ){
		
		var i = 0;
		while( i < addressFields.length ){
		
			var field = addressFields[ i ];
			var idelement = 'billing[' + field + ']';
			var element = document.getElementById( idelement );
			
			element.value = billingAddresses[ idbilling ][ field ];
			
			i++;
			
		}
		
	}
	
	
	function clearB(){
		document.getElementById("idbilling").value="0";
		document.getElementById("billing[company]").value="";
		document.getElementById("billing[title]").value="";
		document.getElementById("billing[lastname]").value="";
		document.getElementById("billing[firstname]").value="";
		document.getElementById("billing[adress]").value="";
		document.getElementById("billing[adress_2]").value="";
		document.getElementById("billing[zipcode]").value="";
		document.getElementById("billing[city]").value="";
		document.getElementById("billing[adress_2]").value="";
		document.getElementById("billing[idstate]").value="1";
		document.getElementById("billing[phonenumber]").value="";
		document.getElementById("billing[faxnumber]").value="";
	}
	
// -->
</script>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<table border="0" width="100%">
	<tr>
		<td valign="top">
			<input type="hidden" name="AddressEditorDisplayed" value="1" />
		</td>
		<td width="20">
		</td>
		<td valign="top">
			<!--Affichage du Formulaire Adresse de facturation-->
			<span class="Adresses_Titre">
				<input id="billing_checkbox" type="checkbox" name="billing_checkbox" value="1" onclick="javascript: displayForm('billing_different');"<?php echo $billingCheckStatus ?> />
				&nbsp;<?php echo Dictionnary::translate("billing_different"); ?>
			</span>
			<br />
			<br />
			<table id="billing_different" width="340" cellspacing="0" class="Adresses_Table" style="display:<?php echo empty( $billingCheckStatus ) ? "none" : "block"; ?>">
			    <tr>
			      	<th width="140">Adresse existante</th>
					<td width="200"><?php  DrawBillingLift( $billingCurrentSet[ "idbilling_adress" ],$where); ?></td>
			  	</tr>
			  	<tr>
			        <th><?php echo Dictionnary::translate("company"); ?>&nbsp;<?=$b_company_required?></th>
			        <td>
			            <input type="text" size="33" name="billing[Company]" id="billing[company]" value="<?php echo $billingCurrentSet[ "company" ] ?>">
					</td>
				</tr>
				<tr>
					<th width="140"><?php echo Dictionnary::translate("title"); ?>&nbsp;<?=$b_title_required?></th>
					<td width="200">
						<select name="billing[Title]" id="billing[title]">
						<?php $title = $billingCurrentSet[ "title" ]; ?>
							<option value="" <?php  if( $title==''){echo "selected=\"selected\"";} ?>></option>
							<option value="1" <?php  if($title=='Mr'){echo "selected=\"selected\"";} ?>>Mr</option>
							<option value="2" <?php  if($title=='Mme'){echo "selected=\"selected\"";} ?>>Mme</option>
							<option value="3" <?php  if($title=='Mlle'){echo "selected=\"selected\"";} ?>>Mlle</option>
						</select>
					</td>
				</tr>
			       <tr>       
			        <th><?php echo Dictionnary::translate("lastname"); ?>&nbsp;<?=$b_lastname_required?></th>
			        <td>
			            <input type="text" size="33" name="billing[Lastname]" id="billing[lastname]" value="<?php echo $billingCurrentSet[ "lastname" ] ?>">
					</td>
			   </tr>
			   <tr>
			        <th><?php echo Dictionnary::translate("firstname"); ?>&nbsp;<?=$b_firstname_required?></th>
			        <td>
			            <input type="text" size="33" name="billing[Firstname]" id="billing[firstname]" value="<?php echo $billingCurrentSet[ "firstname" ]; ?>">
					</td>
				</tr>
			   <tr>
			        <th><?php echo Dictionnary::translate("adress"); ?>&nbsp;<?=$b_address_required?></th>
			        <td colspan="3">
			           <input size="33" type="text" name="billing[Adress]" id="billing[adress]" value="<?php echo $billingCurrentSet[ "adress" ] ?>">
					</td>
				</tr><tr>
			       <th><?php echo Dictionnary::translate("adress_2"); ?>&nbsp;<?=$b_address2_required?></th>
			       <td colspan="3">
			          <input size="33" type="text" name="billing[Adress_2]" id="billing[adress_2]" value="<?php echo $billingCurrentSet[ "adress_2" ] ?>">
				   </td>
			    </tr><tr>
			        <th><?php echo Dictionnary::translate("zipcode"); ?>&nbsp;<?=$b_zipcode_required?></th>
			        <td>
			         	<input size="33" type="text" name="billing[Zipcode]" id="billing[zipcode]" value="<?php echo $billingCurrentSet[ "zipcode" ] ?>">
					</td>
				</tr><tr>	
			        <th><?php echo Dictionnary::translate("city"); ?>&nbsp;<?=$b_city_required?></th>
			        <td>
			         	<input type="text" size="33" name="billing[City]" id="billing[city]" value="<?php echo $billingCurrentSet[ "city" ] ?>">
					</td>
				</tr><tr>
			        <th><?php echo Dictionnary::translate("state"); ?>&nbsp;<?=$b_state_required?></th>
			        <td><?php echo DrawLift_id("state",$billingCurrentSet[ "idstate" ],'','billing[idstate]'); ?>
			        </td>
				</tr>
				<tr>
					<th><?php echo Dictionnary::translate("phone"); ?>&nbsp;<?=$b_phonenumber_required?></th>
			        <td ><input size="33" type="text" name="billing[Phonenumber]" id="billing[phonenumber]" value="<?php echo $billingCurrentSet[ "phonenumber" ] ?>" maxlength="20"></td>     
			    </tr>
				<tr>
					<th><?php echo Dictionnary::translate("fax"); ?>&nbsp;<?=$b_faxnumber_required?></th>
			        <td ><input size="33" type="text" name="billing[Faxnumber]" id="billing[faxnumber]" value="<?php echo $billingCurrentSet[ "faxnumber" ] ?>" maxlength="20"></td> 
			    </tr>
			    <tr>
			    <td colspan="2" style="border:none;"><span class="asterix">*</span>&nbsp;<span class="Adresses_Oblg">Champs obligatoires</span></td>
				</tr>
				<tr>
			    <td colspan="2" style="border:none;"><input type="button" name="NewBAdresse" value="Créer une adresse" onclick="javascript:clearB()" /><input type="submit" name="UpdateAdresses" value="Modifier" /></td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<?php

GraphicFactory::table_end();

//-----------------------------------------------------------------------------------------

function GetBuyerBillingAddresses( $idbuyer ){
	
	global $Session;
	
	$billingAddresses = array();
	
	$query = "SELECT idbilling_adress AS id, adress, adress_2, zipcode, city, idstate, firstname, lastname, company, title, phonenumber, faxnumber FROM billing_adress WHERE idbuyer = $idbuyer";
	$db = & DBUtil::getConnection();
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les adresses de livraison du client" );
		
	$i = 0;
	while( !$rs->EOF() ){
		
		foreach( $rs->fields as $key => $value )
			$billingAddresses[ $i ][ $key ] = $value;
		
		$rs->MoveNext();
		$i++;
		
	}
	
	return $billingAddresses;
	
}

//-----------------------------------------------------------------------------------------

?>