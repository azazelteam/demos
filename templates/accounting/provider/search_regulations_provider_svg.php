<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche règlements services
 */
 
//------------------------------------------------------------------------------------------------

include_once( "../../config/init.php" );
include_once( "../../objects/DHTMLCalendar.php" );
include_once( "../../catalog/admin_func.inc.php" );
include_once( "../../script/global.fct.php" );

$Title = "Recherche de factures de service";

//------------------------------------------------------------------------------------------------
/* suppression - ajax */

if( isset( $_REQUEST[ "delete" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit( "Votre session a expiré" );
	
	$ret = 	DBUtil::query( "DELETE FROM regulations_provider WHERE idregulations_provider = '" . intval( $_REQUEST[ "delete" ] ) . "'" ) ;
	$ret &= DBUtil::query( "DELETE FROM billing_regulations_provider WHERE idregulations_provider = '" . intval( $_REQUEST[ "delete" ] ) . "'" ) ;
	
	exit( $ret === false ? "0" : "1" );
	
}

//------------------------------------------------------------------------------------------------
/* recherche - ajax */

if( isset( $_POST[ "search" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit();
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------------

include( "../../templates/back_office/head.php" );

?>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<?php displayForm(); ?>
</div><!-- globalMainContent -->
<?php

include( "../../templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){

	?>
	<script type="text/javascript" src="/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="/js/form.lib.js"></script>
	<script type="text/javascript" src="/js/ajax.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */

		/* ----------------------------------------------------------------------------------- */

		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			$( "#SearchForm" ).ajaxForm( options );

		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );

		}

		/* ----------------------------------------------------------------------------------- */
			
	/* ]]> */
	</script>
    <div class="mainContent">
		<div class="topRight"></div>
		<div class="topLeft"></div>
		<div class="content">
            <form action="/accounting/provider/search_regulations_provider.php" method="post" id="SearchForm">
  			<input type="hidden" name="search" value="1" />
            <div class="headTitle">
                    <p class="title">Rechercher un règlement</p>
                    <div class="rightContainer">
                    <input type="radio" name="date_type" value="payment_date" checked="checked" class="VCenteredWithText" /> Date de règlement
                    <input type="radio" name="date_type" value="DateHeure" class="VCenteredWithText" /> Date de saisie
                        <input type="radio" name="date_type" value="deliv_payment" class="VCenteredWithText" /> Date d'échéance
                    </div>
                </div>
                <div class="subContent">
                	<!-- tableau choix de date -->
					<div class="tableContainer">
                    	<table class="dataTable dateChoiceTable">
							<tr>	
                            	<th class="filledCell">
                            		<input type="radio" name="interval_select" id="interval_select_since" value="1" checked="checked" class="VCenteredWithText"/> Depuis
                            	</th>
                                <td>
									<select name="minus_date_select" onchange="$( '#interval_select_since' ).attr('checked','checked');" class="fullWidth">
						            	<option value="0">Aujourd'hui</option>
						              	<option value="-1"><?echo= Dictionnary::translate("order_yesterday")  ?></option>
					              		<option value="-2"><?echo= Dictionnary::translate("order_2days")  ?></option>
						              	<option value="-7"><?echo= Dictionnary::translate("order_1week") ?></option>
						              	<option value="-14"><?echo= Dictionnary::translate("order_2weeks") ?></option>
						              	<option value="-30"><?echo= Dictionnary::translate("order_1month") ?></option>
						              	<option value="-60"><?echo= Dictionnary::translate("order_2months") ?></option>
						              	<option value="-90"><?echo= Dictionnary::translate("order_3months") ?></option>
						              	<option value="-180"><?echo= Dictionnary::translate("order_6months") ?></option>
						              	<option value="-365" selected="selected"><?echo= Dictionnary::translate("order_1year") ?></option>
						              	<option value="-730">Moins de 2 ans</option>
						              	<option value="-1095">Moins de 3 ans</option>
						              	<option value="all">Toutes</option>
				            		</select>
								</td>
								<th class="filledCell">
									<input type="radio" name="interval_select" id="interval_select_month" value="2" class="VCenteredWithText" /> Pour le mois de
								</th>
								<td><?php FStatisticsGenerateDateHTMLSelect( "$( '#interval_select_month' ).attr('checked','checked');" ); ?></td>
							</tr>
							<tr>	
								<th class="filledCell">
								    <input type="radio" name="interval_select" id="interval_select_between" value="3" class="VCenteredWithText" /> Entre le
								</th>
								<td>
					 				<input type="text" name="bt_start" id="bt_start" value="<?echo= date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ) ?>" class="calendarInput" />
					 				<?php DHTMLCalendar::calendar( "bt_start", true, "$( '#interval_select_between' ).attr('checked','checked');" ); ?>
								</td>
								<th class="filledCell">et le</th>
								<td>
					 				<input type="text" name="bt_stop" id="bt_stop" value="<?echo= date( "d-m-Y" ) ?>" class="calendarInput" />
					 				<?php DHTMLCalendar::calendar( "bt_stop", true, "$( '#interval_select_between' ).attr('checked','checked');" ); ?>
								</td>
							</tr>
                            <tr>
                                <td colspan="4" class="tableSeparator"></td>
                           	</tr>
                            <tr>
                            	<th class="filledCell">Facture n°</th>
                                <td><input type="text" size="10" name="idprovider_invoice"  value="" class="textInput" /></td>
                                <th class="filledCell">Prestataire</th>
                                <td>
                                <?php
                                    
                                    include_once( "../../objects/XHTMLFactory.php" );
                                    XHTMLFactory::createSelectElement( "provider", "idprovider", "name", "name", false, 0, "Tous", "name=\"idprovider\"" );
                                    
                                ?>
                               	</td>
                            </tr>
                            <tr>
								<th class="filledCell">Total facturé HT</th>
                                <td><input type="text" size="10" name="totalET" value="" class="textInput" /></td>
                                <th class="filledCell">Total facturé TTC</th>
                                <td><input type="text" size="10" name="totalATI"  value="" class="textInput" /></td>
                            </tr>
                            <tr>
								<th class="filledCell">Mode de règlement</th>
                                <td colspan="3">
								<?php
								
									include_once( "../../objects/XHTMLFactory.php" );
                                    XHTMLFactory::createSelectElement( "payment", "idpayment", "name_1", "name_1", false, 0, "Tous", "name=\"idpayment\"" );
                                    
                                ?>
								</td>
                            </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton" style="text-align: center;" value="Rechercher" />
                    </div>
                </div>
                </form>
            </div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
	<div class="clear"></div>
	<div id="SearchResults"></div>
	<?php 

}

//------------------------------------------------------------------------------------------------

function search(){

	$now = getdate();

	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "invoice_date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
			
	switch ($interval_select){
			    
		case 1:
			//min
			if( $minus_date_select == "all" )
					$Min_Time = "0000-00-00";
			else 	$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$where  = "`regulations_provider`.$date_type >='$Min_Time' AND `regulations_provider`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$where  = "`regulations_provider`.$date_type >='$Min_Time' AND `regulations_provider`.$date_type <'$Max_Time' ";
	     	
			break;
			     
		case 3:
		default :
				
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$where  = "`regulations_provider`.$date_type >='$Min_Time' AND `regulations_provider`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !User::getInstance()->get( "admin" ) )
		$where .= " AND `provider_invoice`.iduser = '" . User::getInstance()->getId() . "'";
	else if( isset( $_POST[ "iduser" ] ) && intval( $_POST[ "iduser" ] ) )
		$where .= " AND provider_invoice.iduser = '" . intval( $_POST[ "iduser" ] ) . "'";
							
	if( isset( $_POST[ "idprovider" ] ) && intval( $_POST[ "idprovider" ] ) )
		$where .= " AND provider_invoice.idprovider = '" . intval( $_POST[ "idprovider" ] ) . "'";

	if( isset( $_POST[ "idpayment" ] ) && intval( $_POST[ "idpayment" ] ) )
		$where .= " AND regulations_provider.idpayment = '" . intval( $_POST[ "idpayment" ] ) . "'";
		
	if ( isset( $_POST[ "idprovider_invoice" ] ) && strlen( $_POST[ "idprovider_invoice" ] ) )
		$where .= " AND provider_invoice.idprovider_invoice LIKE '%" . $_POST[ "idprovider_invoice" ] . "%'";

	$query = "
	SELECT `user`.initial,
		regulations_provider.idregulations_provider,
		GROUP_CONCAT( provider_invoice.idprovider_invoice ) AS idprovider_invoice,
		GROUP_CONCAT( provider_invoice.idprovider ) AS idprovider,
		GROUP_CONCAT( provider_invoice.invoice_date ) AS invoice_date,
		payment.name_1 AS payment,
		billing_regulations_provider.rebate_amount,
		regulations_provider.amount,
		regulations_provider.payment_date,
		provider.name AS provider,
		SUM( pii.price * pii.quantity ) AS totalET,
		SUM( pii.price * pii.quantity ) + provider_invoice.vat_amount AS totalATI,
		provider_invoice.vat_amount
	FROM `user`, 
		provider, 
		provider_invoice,
		provider_invoice_item pii, 
		regulations_provider,
		billing_regulations_provider,
		payment
	WHERE $where
	AND `user`.iduser = regulations_provider.iduser
	AND payment.idpayment = regulations_provider.idpayment
	AND pii.idprovider_invoice = provider_invoice.idprovider_invoice
	AND pii.idprovider = provider_invoice.idprovider
	AND provider_invoice.idprovider = billing_regulations_provider.idprovider
	AND provider_invoice.idprovider_invoice = billing_regulations_provider.idprovider_invoice
	AND billing_regulations_provider.idregulations_provider = regulations_provider.idregulations_provider
	AND provider.idprovider = provider_invoice.idprovider
	GROUP BY regulations_provider.idregulations_provider";
	
	if( isset( $_POST[ "totalET" ] ) && strlen( $_POST[ "totalET" ] ) 
		|| isset( $_POST[ "totalATI" ] ) && strlen( $_POST[ "totalATI" ] ) )
		$query .= " HAVING 1";
		
	if( isset( $_POST[ "totalET" ] ) && strlen( $_POST[ "totalET" ] ) )
		$query .= " AND SUM( pii.price * pii.quantity ) = '" . Util::text2num( $_POST[ "totalET" ] ) . "'";

	if( isset( $_POST[ "totalATI" ] ) && strlen( $_POST[ "totalATI" ] ) )
		$query .= " AND SUM( pii.price * pii.quantity ) + provider_invoice.vat_amount = '" . Util::text2num( $_POST[ "totalATI" ] ) . "'";
	
		
	$query .= "
	ORDER BY provider.name ASC, regulations_provider.payment_date DESC, provider_invoice.idprovider_invoice ASC";

	$rs = DBUtil::query( $query );

	if( !$rs->RecordCount() ){
		
		?>
		<div class="mainContent fullWidthContent">
        	<div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<div class="headTitle">
                    <p class="title">
                    	Aucun règlement n'a été trouvé !
                    </p>
					<div class="rightContainer">
					
					</div>
                </div>
       		</div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div> <!-- mainContent fullWidthContent -->
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* -------------------------------------------------------------------------------------------------- */
		
		$( document ).ready( function(){ 
			
			$( ".sortable" ).tablesorter({
		      	
		      	headers: { 

					0: {  sorter:false },
					1: {  sorter:'text' },
					2: {  sorter:'text' },
					3: {  sorter:'date' },
					4: {  sorter:'currency' },
					5: {  sorter:'currency' },
					6: {  sorter:'text' },
					7: {  sorter:'currency' },
					8: {  sorter:'currency' },
					9: {  sorter:'date' },
					10: {  sorter:false }
		      		
				} 
					        
			});

		});

		/* -------------------------------------------------------------------------------------------------- */
		
		function deletePayment( idregulations_provider ){

			$.ajax({
			 	
				url: "/accounting/provider/search_regulations_provider.php?delete=" + idregulations_provider,
				async: true,
				type: "POST",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( onErrorMessage ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){

						alert( "Impossible de supprimer le règlement : " + responseText );
						return;

					}

					$( "#Payment" + idregulations_provider ).remove();
					
				}
	
			});

		}

		/* -------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    padding-left: 20px; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
		    padding-bottom:15px;
		
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 
		
	</style>
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div>
		<div class="topLeft"></div>
        <?php
        
        	while( !$rs->EOF() )
        		displayProviderPayments( $rs->fields( "idprovider" ), $rs );
        		
        ?>
        <div class="bottomRight"></div>
        <div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
	<?php	

}

//--------------------------------------------------------------------------------

function displayProviderPayments( $idprovider, $rs ){
	
	?>
	<div class="content">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
        <div class="headTitle">
        	<p class="title"><?php echo htmlentities( $rs->fields( "provider" ) ); ?></p>
			<div class="rightContainer"></div>
		</div>
		<div class="subContent">
			<div class="tableHeaderLeft">
				<a href="#" class="goUpOrDown">
					Aller en bas de page
					<img src="/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
				</a>
			</div>
			<div class="tableHeaderRight"></div>
			<!-- tableau résultats recherche -->
			<div class="resultTableContainer clear">
				<table class="dataTable resultTable sortable" id="ResultTable">
					<thead>
				       	<tr>
				       		<th></th>
				       		<th>Com.</th>
				       		<th>Facture n°</th>
				       		<th>Date de facture</th>
				       		<th>Total HT facturé</th>
				       		<th>Total TTC facturé</th>
				       		<th>Mode de paiement</th>
				       		<th>Escompte</th>
					      	<th>Total réglé</th>
					      	<th>Date de paiement</th>
				       	</tr>
			       	</thead>
			       	<tbody>
					<?php

						$totalATISum 	= 0.0;
						$totalETSum 	= 0.0;
						$amountSum 		= 0.0;
						
						while( !$rs->EOF() && $idprovider == $rs->fields( "idprovider" ) ){	
														
							?>
					        <tr class="blackText" id="Payment<?php echo $rs->fields( "idregulations_provider" ); ?>">
					        	<td class="lefterCol">
					        		<a href="#" onclick="if( confirm('Etes-vous certains de vouloir supprimer ce règlement?') ) deletePayment(<?php echo $rs->fields( "idregulations_provider" ); ?>); return false;">
					        			<img src="/images/back_office/content/corbeille.jpg" alt="" />
					        		</a>
					        	</td>
					        	<td><?php echo $rs->fields( "initial" ); ?></td> 
					        	<td>
					        	<?php
					        	
					        		$invoices = explode( ",", $rs->fields( "idprovider_invoice" ) );
					        		$providers = explode( ",", $rs->fields( "idprovider" ) );
					        		
					        		$i = 0;
					        		while( $i < count( $invoices ) ){
					        			
					        			?>
					        			<a class="grasBack" href="/accounting/provider/provider_invoice.php?idprovider_invoice=<?php echo urlencode( $invoices[ $i ] ); ?>&idprovider=<?php echo $providers[ $i ]; ?>" onclick="window.open(this.href); return false;" style="display:block;">
											<?php echo htmlentities( $invoices[ $i ] ); ?>
										</a>
										<?php
									
					        			$i++;
					        			
					        		}
					        		
					        	?>
								</td>
								<td>
								<?php 
								
									foreach( explode( ",", $rs->fields( "invoice_date" ) ) as $invoice_date )
										echo "<span style=\"display:block;\">" . Util::dateFormatEu( $invoice_date ) . "</span>"; 
								
								?>
								</td>
								<td style="text-align:right; white-space:nowrap;">
								<?php 
								
									foreach( explode( ",", $rs->fields( "totalET" ) ) as $totalET )
										echo "<span style=\"display:block;\">" . Util::priceFormat( $totalET ) . "</span>"; 
										
								?>
								</td>
								<td style="text-align:right; white-space:nowrap;">
								<?php 
								
									foreach( explode( ",", $rs->fields( "totalATI" ) ) as $totalATI )
										echo "<span style=\"display:block;\">" . Util::priceFormat( $totalATI ) . "</span>"; 
										
								?>
								</td>
								<td><?php echo htmlentities( $rs->fields( "payment" ) ); ?></td> 
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "rebate_amount" ) > 0.0 ? Util::priceFormat( $rs->fields( "rebate_amount" ) ) : "-"; ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "amount" ) ); ?></td>
								<td class="righterCol"><?php echo Util::dateFormatEu( substr( $rs->fields( "payment_date" ), 0, 10 ), "d/m/Y" ); ?></td>
							</tr>
							<?php

							foreach( explode( ",", $rs->fields( "totalATI" ) ) as $totalATI )
								$totalATISum += $totalATI;

							foreach( explode( ",", $rs->fields( "totalET" ) ) as $totalET )
								$totalETSum += $totalET;
								
							$amountSum += $rs->fields( "amount" );
							
							$rs->MoveNext();			
						
						}
						
					?>
					</tbody>
					<tr>
			       		<td colspan="4" class="lefterCol"></td>
			       		<td style="text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $totalETSum ); ?></td>
			       		<td style="text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $totalATISum ); ?></td>
			       		<td colspan="2"></td>
				      	<td style="text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $amountSum ); ?></td>
				      	<td class="righterCol"></td>
			       	</tr>
            	</table>
                </div><!-- resultTableContainer -->
                <a href="#" class="goUpOrDown">
                	Aller en haut de page
                    <img src="/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>
           </div><!-- subContent -->
   	</div><!-- content -->
   	<?php
   	
}

//--------------------------------------------------------------------------------

?>