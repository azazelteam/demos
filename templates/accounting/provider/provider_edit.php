<?php

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* transporteur */

if( isset( $_REQUEST[ "carrier" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	if( intval( $_REQUEST[ "carrier" ] ) )
			$ret = DBUtil::query( "INSERT IGNORE INTO carrier( idprovider ) VALUES( '" . intval( $_REQUEST[ "idprovider" ] ) . "' )" );
	else 	$ret = DBUtil::query( "DELETE FROM carrier WHERE idprovider = '" . intval( $_REQUEST[ "idprovider" ] ) . "' LIMIT 1" );
	
	exit( $ret === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload logo */

if( isset( $_FILES[ "logo" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . $_REQUEST[ "folder" ] . "/";
	$ext = strtolower( substr( $_FILES[ "logo" ][ "name" ] , strrpos( $_FILES[ "logo" ][ "name" ] , "." ) + 1 ) );
	$targetFile =  str_replace( "//", "/", $targetPath ) . $_REQUEST[ "idprovider" ] . ".$ext";

	$files = glob( "$targetPath/" . $_REQUEST[ "idprovider" ] . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	
	foreach( $files as $file )
		unlink( $file );
		
	move_uploaded_file( $_FILES[ "logo" ][ "tmp_name" ], $targetFile );
	chmod( $targetFile, 0755 );
	
	exit( URLFactory::getProviderImageURI( $_REQUEST[ "idprovider" ], URLFactory::$IMAGE_CUSTOM_SIZE, 150, 150 ) );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* modifier le nom d'un compte */

if( isset( $_REQUEST[ "update_account_name" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$rs = DBUtil::query( "SELECT 1 FROM accounting_plan WHERE idaccounting_plan = " . DBUtil::quote( $_REQUEST[ "idaccounting_plan" ] ) . " LIMIT 1" );
	
	if( !$rs->RecordCount() )
		exit( "Ce N° de compte n'existe pas dans votre plan comptable" );
		
	$rs = DBUtil::query(
	
		"UPDATE service 
		SET idaccounting_plan = " . DBUtil::quote( $_REQUEST[ "idaccounting_plan" ] ) . "
		WHERE idservice = '" . intval( $_REQUEST[ "idservice" ] ) . "'
		LIMIT 1"
	
	);
		
	exit( $rs === false ? "0" : "1" );
	 
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* intitulé d'un code NAF - ajax */

if( isset( $_REQUEST[ "naf" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	exit( DBUtil::getDBValue( "naf_comment", "naf", "idnaf", stripslashes( $_REQUEST[ "naf" ] ) ) );
		
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* création d'un service - ajax */

if( isset( $_REQUEST[ "create_service" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$query = "
	INSERT INTO service( idprovider, label, idaccounting_plan, price, vat_rate )
	VALUES( '" . intval( $_REQUEST[ "idprovider" ] ) . "', 
		" . DBUtil::quote( $_REQUEST[ "label" ] ) . ", 
		'" . intval( $_REQUEST[ "idaccounting_plan" ] ) . "',
		'" . Util::text2num( $_REQUEST[ "price" ] ) . "',
		'" . Util::text2num( $_REQUEST[ "vat_rate" ] ) . "'
	)";
	
	exit( DBUtil::query( $query ) === false ? "Impossible de créer le service" :getProviderServices( intval( $_REQUEST[ "idprovider" ] ) ) );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression d'un service - ajax */

if( isset( $_REQUEST[ "delete_service" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	if( DBUtil::query( "SELECT idservice FROM provider_invoice_item WHERE idservice = '" . intval( $_REQUEST[ "idservice" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( "0" );

	exit( DBUtil::query( "DELETE FROM service WHERE idservice = '" . intval( $_REQUEST[ "idservice" ] ) . "' LIMIT 1" ) === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* création d'un compte - ajax */

if( isset( $_REQUEST[ "create_account" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	if( DBUtil::query( "SELECT 1 FROM accounting_plan WHERE idaccounting_plan = " . DBUtil::quote( $_REQUEST[ "idaccounting_plan" ] ) . " LIMIT 1" )->RecordCount() )
		exit( "0" );
		
	$query = "
	INSERT INTO accounting_plan( idaccounting_plan, accounting_plan_1, accounting_plan_parent )
	VALUES( " . DBUtil::quote( $_REQUEST[ "idaccounting_plan" ] ) . ", " . DBUtil::quote( $_REQUEST[ "label" ] ) . ", " . DBUtil::quote( $_REQUEST[ "idparent" ] ) . " )";
	
	exit( DBUtil::query( $query ) === false ? "Impossible de créer le compte" :getProviderServices( intval( $_REQUEST[ "idprovider" ] ) ) );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* définir le contact principal - ajax */

if( isset( $_REQUEST[ "main_contact" ] ) && isset( $_REQUEST[ "idprovider" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/Provider.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/ProviderContact.php" );
	include( dirname( __FILE__ ) . "/../../../lib/contact_editor.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	$provider->set( "main_contact", intval( $_GET[ "main_contact" ] ) );
	$provider->save();
	
	$mainContact = false;
	$it = $provider->getContacts()->iterator();
	
	while( $mainContact === false && $it->hasNext() ){

		$contact =& $it->next();
		
		if( $contact->isMainContact() )
			$mainContact = $contact;
		
	}
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $provider->getContacts(), $mainContact->getId(), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression d'un contact - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "contact" && isset( $_REQUEST[ "idprovider" ] ) && isset( $_REQUEST[ "idcontact" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/Provider.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/ProviderContact.php" );
	include( dirname( __FILE__ ) . "/../../../lib/contact_editor.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	ProviderContact::delete( intval( $_REQUEST[ "idprovider" ] ), intval( $_REQUEST[ "idcontact" ] ) );
	$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $provider->getContacts(), $provider->get( "main_contact" ), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* duplication d'un contact - ajax */

if( isset( $_REQUEST[ "duplicate" ] ) && $_REQUEST[ "duplicate" ] == "contact" && isset( $_REQUEST[ "idprovider" ] ) && isset( $_REQUEST[ "idcontact" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/Provider.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/ProviderContact.php" );
	include( dirname( __FILE__ ) . "/../../../lib/contact_editor.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	
	$done = false;
	$it = $provider->getContacts()->iterator();
	while( $it->hasNext() ){
		
		$contact =& $it->next();
		
		if( $contact->getId() == intval( $_REQUEST[ "idcontact" ] ) ){

			$clone = $contact->duplicate();
			$done = true;
			
		}
		
	}

	$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $provider->getContacts(), $done ? $clone->getId() : $provider->get( "main_contact" ), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* création d'un nouveau contact - ajax */

if( isset( $_REQUEST[ "create" ] ) && $_REQUEST[ "create" ] == "contact" && isset( $_REQUEST[ "idprovider" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/Provider.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/ProviderContact.php" );
	include( dirname( __FILE__ ) . "/../../../lib/contact_editor.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$contact = ProviderContact::create( intval( $_REQUEST[ "idprovider" ] ) );
	$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $provider->getContacts(), $contact->getId(), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* historique factures - ajax */

if( isset( $_REQUEST[ "history" ] ) && isset( $_REQUEST[ "idprovider" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	getProviderHistory( intval( $_REQUEST[ "idprovider" ] ) );

	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des services - ajax */

if( isset( $_REQUEST[ "services" ] ) && isset( $_REQUEST[ "idprovider" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	getProviderServices( intval( $_REQUEST[ "idprovider" ] ) );

	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* chiffres clés manageo - ajax */

if( isset( $_REQUEST[ "last_period" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../lib/manageo.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	header( "Content-Type: text/xml;charset=utf-8" );
	$document = manageo_stdclass2DomDocument( manageo_SIRET2stdclass( $_REQUEST[ "siret" ] ) );

	echo $document->saveXML();
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

include_once( dirname( __FILE__ ) . "/../../../objects/XHTMLFactory.php" );

header( "Content-Type: text/html; charset=utf-8" );

?>
<div id="ProviderEdit">*
	<link rel="stylesheet" type="text/css" href="/css/back_office/form_default.css" />
	<style type="text/css">
		/*div#global div#header { position:fixed; top:0; width:100%; z-index:100; }*/
		div#footer { display:none !important; }
		#sliderPanel { z-index:100; }
	</style>
	
	<link rel="stylesheet" type="text/css" href="/js/jquery/ui/treeview/jquery.treeview.css" />
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ---------------------------------------------------------------------------------------------------------------- */
	
		$( document ).ready( function() {
			
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
	
	    	$('#ProviderForm').ajaxForm( {
			 	
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback      
	  
		    } );
	    	
		});
	
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
		}
	
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
	
			if( responseText != "1" ){
	
				alert( "Impossible de sauvegarder les informations : " + responseText );
				return;
				
			}

			/* confirmation */
			
			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
			$.blockUI.defaults.css.fontSize = '12px';
	    	$.growlUI( "", "Modifications enregistrées" );

	    	getServices();
	    	
		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function getHistory(){

			if ( typeof this.init == 'undefined' ) this.init = false;

			if( this.init )
				return;

			this.init = true;
			
			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&history",
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer l'historique du prestataire de services" ); },
			 	success: function( responseText ){
	
					$( "#ProviderHistory" ).html( responseText );
				
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function getServices(){

			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&services",
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les services du prestataire" ); },
			 	success: function( responseText ){
	
					$( "#ProviderServices" ).html( responseText );
				
				}
	
			});

		}
		
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function manageoDialog(){

			if( !$( "#name" ).val().length )
				return;

			var parameters = "cancelCallback=manageoCloseDialog&selectCallback=manageoSelectCompany&name=" + escape( $( "#name" ).val() );
			
			if( $( "#zipcode" ).val().length )
				parameters += "&zipcode=" + escape( $( "#zipcode" ).val() );
			
			$.ajax({
			 	
				url: "/sales_force/manageo.php?" + parameters,
				async: true,
				type: "GET",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'effectuer la recherche" ); },
			 	success: function( responseText ){
					
					$( "#ManageoDialog").html( responseText );
					
					$( "#ManageoDialog" ).dialog({
				        
						modal: true,	
						title: "Recherche Manageo",
						width:850,
						close: function(event, ui) { 

							$("#ManageoDialog").dialog( "destroy" );
							
						}
					
					});
					
					
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------------------------- */

		function manageoCloseDialog(){ $("#ManageoDialog").dialog( "destroy" ); }

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function manageoSelectCompany( $company ){ 

			$( "#name" ).val( $company.find( "name" ).text() );
			$( "#legal_form" ).val( $company.find( "legal_form" ).text() );
			$( "#siret" ).val( $company.find( "siret" ).text() );
			$( "#naf" ).val( $company.find( "naf" ).text() );
			updateNAFDescription();
			$( "#share_capital" ).val( $company.find( "share_capital" ).text() );
			$( "#address" ).val( $company.find( "address" ).text() );
			$( "#address2" ).val( $company.find( "address2" ).text() );
			$( "#zipcode" ).val( $company.find( "zipcode" ).text() );
			$( "#city" ).val( $company.find( "city" ).text() );
			$( "#idstate" ).val( $company.find( "idstate" ).text() );
			$( "#faxnumber" ).val( $company.find( "faxnumber" ).text() );
			$( "#phonenumber" ).val( $company.find( "phonenumber" ).text() );
			$( "#manager_lastname" ).val( $company.find( "manager_lastname" ).text() );
			$( "#manager_firstname" ).val( $company.find( "manager_firstname" ).text() );
			$( "#manager_place_of_birth" ).val( $company.find( "manager_place_of_birth" ).text() );
			$( "#institution_count" ).val( $company.find( "institution_count" ).text() );
			$( "#staff" ).val( $company.find( "last_period" ).find( "staff" ).text() );

			var birthday = $company.find( "manager_birthday" ).text().split( "-" );
			$( "#manager_birthday" ).val( birthday[ 2 ] + "-" + birthday[ 1 ] + "-" + birthday[ 0 ] );

			var creation_date = $company.find( "creation_date" ).text().split( "-" );
			$( "#creation_date" ).val( creation_date[ 2 ] + "-" + creation_date[ 1 ] + "-" + creation_date[ 0 ] );

			$("#ManageoDialog").dialog( "destroy" ); 

		}
		
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function updateNAFDescription(){

			if( !$( "#naf" ).val().length ){
				
				$( "#NAFDescription").val( "" );
				return;

			}
			
			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?naf=" + escape( $( "#naf" ).val() ),
				async: true,
				type: "GET",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer l'intitulé du code NAF" ); },
			 	success: function( responseText ){
					
					$( "#NAFDescription").val( responseText );

				}
	
			});
			
		}

		/* ---------------------------------------------------------------------------------------------------- */
	 	
	 	function setCarrier( carrier ){

			var success = false;
			var value = carrier ? "1" : "0";
			
			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&carrier=" + value,
				async: false,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de modifier le transporteur" ); },
			 	success: function( responseText ){
	
					success = responseText == "1";

					if( success ){

						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
				    	$.growlUI( "", "Modifications enregistrées" );

					}
					
				}
	
			});

			return success;
			
		}

	 	/* ---------------------------------------------------------------------------------------------------- */
	 	
	/* ]]> */
	</script>
    
	<div class="blocRightSupplier">
		<div id="ManageoDialog" style="display:none;"></div>
		<form action="/accounting/provider/provider.php?update&amp;idprovider=<?php echo $provider->getId(); ?>" method="post" id="ProviderForm" enctype="multipart/form-data">
			<input type="hidden" name="update" value="1" />
			<div class="contentResult">
				<!-- Titre -->
				<h1 class="titleSearch">
					<span class="selectDate">
						<input type="checkbox" value="1"<?php 
						
							if( $idcarrier = DBUtil::getDBValue( "idcarrier", "carrier", "idprovider", $provider->getId() ) ){
								
								echo " checked=\"checked\""; 
								
								if( DBUtil::getDBValue( "idcarriage", "carriage", "idcarrier", $idcarrier ) )
										echo " readonly=\"readonly\" onclick=\"return false;\"";
								else 	echo " onclick=\"return setCarrier( this.checked );\"";
							}
							else echo " onclick=\"return setCarrier( this.checked );\"";
							
						?> /> Ce prestataire est un transporteur
					</span>
					<span class="textTitle">Société n°<?php echo $provider->getId(); ?> - <span id="ProviderName"><?php echo $provider->get("name"); ?></span></span>
					<div class="spacer"></div>
				</h1>
				<div class="blocResult" style="margin:0;"><div style="margin:5px;">	
					
					<div class="blocMultiple blocMLeft" style="width:160px;">
						<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
						<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
						<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
						<script type="text/javascript">
						/* <![CDATA[ */
						
							$( document ).ready( function(){

								$( "#logo" ).uploadify({
								
									'uploader'		: '/js/jquery/ui/uploadify-2.1.0/uploadify.swf',
									'script'		: '/templates/accounting/provider/provider_edit.php',
									'folder'		: '/images/providers',
									'scriptData'	: { 'idprovider': '<?php echo $provider->getId(); ?>'},
									'fileDataName'	: 'logo',
									'buttonImg'		: '<?php echo URLFactory::getProviderImageURI( $provider->getId(), URLFactory::$IMAGE_CUSTOM_SIZE, 150, 150 ); ?>',
									'width' 		: 150,
									'height'		: 150,
									'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
									'fileDesc'		: "Images",
									'auto'           : true,
									'multi'          : false,
									'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
									'onComplete' 	: function( event, queueID, fileObj, response, data ){
		
										$( "#logo" ).uploadifySettings( "buttonImg", response );
											
									}
	
								});

							});
							
						/* ]]> */
						</script>
						<input type="file" id="logo" name="" value="" />
						<div class="spacer"></div>
					</div>
				
					<div class="blocMultiple blocMLeft" style="width:33%;">

						<label><span><strong>Raison sociale</strong></span></label>
						<a href="#" onclick="manageoDialog(); return false;">
							<img id="ManageoDialogButton" src="/images/back_office/content/manageo.gif" alt="" style="vertical-align:bottom; float:right; margin-top:5px; opacity:<?php echo strlen( $provider->get("name") ) ? "1.0" : "0.6"; ?>" />
						</a>
						<input type="text" name="name" id="name" value="<?php echo $provider->get("name"); ?>" onkeyup="this.value=this.value.toUpperCase(); $('#ManageoDialogButton').css('opacity',this.value.length ? '1.0' : '0.6'); $('#ProviderName').html($(this).val());" class="textInput" style="font-weight:bold; width:160px;" />

						<div class="spacer"></div>

						<div style="margin-top:15px;">
							<label><span>Adresse</span></label>
							<input type="text" name="address" id="address" value="<?php echo $provider->get("address"); ?>" class="textInput inputSupplier" />
							<div class="spacer"></div>
							
							<label><span>Complément adresse</span></label>
							<input type="text" name="address2" id="address2" value="<?php echo $provider->get("address2"); ?>" class="textInput inputSupplier" />
							<div class="spacer"></div>
							
							<label><span>Code postal</span></label>
							<input type="text" name="zipcode" id="zipcode" value="<?php echo $provider->get("zipcode"); ?>" class="numberInput" maxlenght="5" />
							<div class="spacer"></div>
							
							<label><span>Ville</span></label>
							<input type="text" name="city" id="city" value="<?php echo $provider->get("city"); ?>" onkeyup="javascript:this.value=this.value.toUpperCase();" class="textInput inputSupplier" />
							<div class="spacer"></div>
							
							<label><span>Pays</span></label>
							<div class="selectSupplier">
								<?php XHTMLFactory::createSelectElement( "state", "idstate", "name_1", "name_1", $provider->get("idstate") ? $provider->get("idstate") : 1, false, false, "name=\"idstate\" id=\"idstate\"" ); ?>
							</div>
							<div class="spacer"></div>
							
							
							
						</div>
						<div class="spacer"></div>

					</div>
					<div class="blocMultiple blocMRight">
						<label><span>Site Internet</span></label>
						<input style="width:64%;"  type="text" id="website" name="website" value="<?php echo $provider->get( "website" ); ?>" class="textInput" />
						<a href="#" onclick="window.open(document.getElementById('website').value); return false;">
						<img class="floatleft" style="margin-top:5px;" src="/images/back_office/layout/icons-web.png"  /></a>
						<div class="spacer"></div>
						
						<label><span>Tél.</span></label>
						<span class="textSimple" style="width:25%;"><?php echo Util::phonenumberFormat( $provider->get("phonenumber" ) );  ?></span>
						<label><span>Fax</span></label>
						<span class="textSimple" style="width:25%;"><?php echo Util::phonenumberFormat( $provider->get("faxnumber" ) );  ?></span>
						
						<div class="spacer"></div>
						
						<div style="margin-top:15px;">
							
							
							<label style="height:116px;"><span><strong>Commentaire<br/>&nbsp;</strong></span></label>
							<textarea name="comment" id="comment" class="textInput" style="height:116px; padding:0px;"><?php echo html_entity_decode( strip_tags( $provider->get( "comment" ) ) ); ?></textarea>
							<div class="spacer"></div>
								
						</div>
								
					</div>
					<div class="spacer"></div>		

				
					<div id="container">
						<!-- Onglet -->
						<ul class="menu">
							<li class="item"><a href="#contacts-provider"><span>Contacts</span></a></li>
							<li class="item"><a href="#infos-provider"><span>Infos société</span></a></li>
							<li class="item"><a href="#condition-provider"><span>Facturation</span></a></li>
							<li class="item"><a href="#services-provider" onclick="getServices(); return false;"><span>Liste des services</span></a></li>
							<li class="item"><a href="#pieces-provider"><span>Pièces jointes</span></a></li>
							
							<li class="item"><a href="#history-provider" onclick="getHistory(); return false;"><span>Historique</span></a></li>
							
						</ul>
						<div style="width:101%; border-top:1px solid #D0D0D0; margin:3px -5px 0 -5px;">&nbsp;</div>
							
		
						<!-- *********** -->
						<!-- Contacts 	 -->
						<!-- *********** -->
						<div id="contacts-provider">
							<script type="text/javascript">
							/* <![CDATA[ */

							    /* -------------------------------------------------------------------------- */
								
								function createContact(){

									$.ajax({
									 	
										url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&create=contact",
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le contact" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});
									
								}

								/* -------------------------------------------------------------------------- */
								
								function deleteContact( idcontact ){

									$.ajax({
									 	
										url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&delete=contact&idcontact=" + idcontact,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le contact" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});

								}

								/* -------------------------------------------------------------------------- */
								
								function duplicateContact( idcontact ){

									$.ajax({
									 	
										url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&duplicate=contact&idcontact=" + idcontact,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de dupliquer le contact" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});

								}

								/* -------------------------------------------------------------------------- */
								
								function setMainContact( idcontact ){

									$.ajax({
									 	
										url: "/templates/accounting/provider/provider_edit.php?idprovider=<?php echo $provider->getId(); ?>&main_contact=" + idcontact,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de définir le contact principal" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});

								}
								
								/* -------------------------------------------------------------------------- */
								
							/* ]]> */
							</script>
							<div id="ContactEditorContainer"> 
							<?php
							
								include( dirname( __FILE__ ) . "/../../../lib/contact_editor.php" );
								displayContactEditor( $provider->getContacts(), $provider->get( "main_contact" ), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
							
							?>
							</div><!-- blocMLeft -->
							
							
							
							<div class="spacer"></div>
							
							
						</div> <!-- #contacts-provider -->	
						
						<!-- *********** -->
						<!--  Condition  -->
						<!-- *********** -->
						<div id="infos-provider">	
						
							<div class="blocMultiple blocMLeft">
		
								<h2>Informations juridiques</h2>
								<label><span><strong><em>Code NAF</em></strong></span></label>
								<input type="text" name="naf" id="naf" value="<?php echo $provider->get("naf"); ?>" class="textInput" onblur="updateNAFDescription();" />
								<div class="spacer"></div>
								
								<label><span><strong><em>Activité APE/NAF</em></strong></span></label>
								<input type="text" id="NAFDescription" value="<?php 
								
									$label = DBUtil::getDBValue( "naf_comment", "naf", "idnaf", $provider->get( "naf" ) );
									
									if( $label ) 
										echo htmlentities( $label ); 
									
								?>" class="textInput" readonly="readonly" />
								<div class="spacer"></div>

								<div style="margin-top:15px;">
									<label><span><strong><em>Capital social</em></strong></span></label>
									<input type="text" name="share_capital" id="share_capital" value="<?php echo htmlentities( $provider->get( "share_capital" ) ); ?>" class="textidInput" />
										
									<label class="smallLabel"><span><strong><em>Création</em></strong></span></label>
									<input type="text" name="creation_date" id="creation_date" value="<?php echo Util::dateFormat( $provider->get( "creation_date" ), "d/m/Y" ); ?>" class="textidInput" />
									<div class="spacer"></div>
									
									<label><span><strong><em>Forme juridique</em></strong></span></label>
									<input type="text" name="legal_form" id="legal_form" value="<?php echo htmlentities( $provider->get( "legal_form" ) ); ?>" class="textInput" />
									<div class="spacer"></div>
																		
									
									
									<label><span><strong><em>Siren/Siret</em></strong></span></label>
									<input type="text" name="siret" id="siret" value="<?php echo htmlentities( $provider->get( "siret" ) ); ?>" class="textidInput" />
									<div class="spacer"></div>
									
									<label><span><strong><em>Nombre d'établissement</em></strong></span></label>
									<input type="text" name="institution_count" id="institution_count" value="<?php echo htmlentities( $provider->get( "institution_count" ) ); ?>" class="textidInput" />
									
									<div class="spacer"></div>
									
								</div>
								<div class="spacer"></div>
								
								<h2>Dirigeant de la société</h2>
								
								<label><span><strong><em>Nom</em></strong></span></label>
								<input type="text" name="manager_lastname" id="manager_lastname" value="<?php echo htmlentities( $provider->get( "manager_lastname" ) ); ?>" class="textInput" />
								
								<label><span><strong><em>Prénom</em></strong></span></label>
								<input type="text" name="manager_firstname" id="manager_firstname" value="<?php echo htmlentities( $provider->get( "manager_firstname" ) ); ?>" class="textInput" />
								
								<div class="spacer"></div>

								<label><span><strong><em>Date de naissance</em></strong></span></label>
								<input type="text" name="manager_birthday" id="manager_birthday" value="<?php echo Util::dateFormat( $provider->get( "manager_birthday" ), "d/m/Y" ); ?>" class="textidInput" />
								
								<label class="smallLabel"><span><strong><em>Lieu</em></strong></span></label>
								<input type="text" name="manager_place_of_birth" id="manager_place_of_birth" value="<?php echo htmlentities( $provider->get( "manager_place_of_birth" ) ); ?>" class="textidInput" />							
								<div class="spacer"></div>
								
								<div style="margin-top:15px;">
									<label><span><strong><em>Tél.</em></strong></span></label>
									<input type="text" name="manager_phonenumber" id="manager_phonenumber" value="<?php echo Util::phonenumberFormat( $provider->get( "manager_phonenumber" ) ); ?>" class="textidInput" />
									
									<label class="smallLabel"><span><strong><em>Portable</em></strong></span></label>
									<input type="text" name="manager_gsm" id="manager_gsm" value="<?php echo Util::phonenumberFormat( $provider->get( "manager_gsm" ) ); ?>" class="textidInput" />							
									<div class="spacer"></div>
									
									<label><span><strong><em>Email</em></strong></span></label>
									<input type="text" name="manager_email" id="manager_email" value="<?php echo htmlentities( $provider->get( "manager_email" ) ); ?>" class="textInput" />
									<div class="spacer"></div>
								</div>
		
							</div><!-- blocMLeft -->
							
							<script type="text/javascript">
							/* <![CDATA[ */
							
								function getLastPeriodInfos(){

									if( !$( "#siret" ).val().length ){

										alert( "Vous devez d'abord renseigner un N° SIRET/SIREN" );
										return;
										
									}

									$.blockUI({
										
										message: "Récupération des données...",
										css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
										fadeIn: 0, 
										fadeOut: 700
											
									}); 

									$( "#LastPeriod" ).find( "input[type='text']" ).val( "" );
									$('.blockOverlay').attr('title','Click to unblock').click( $.unblockUI );
									
									$.ajax({
									 	
										url: "/templates/accounting/provider/provider_edit.php?last_period",
										async: true,
										type: "POST",
										data: "siret=" + $( "#siret" ).val(),
										error : function( XMLHttpRequest, textStatus, errorThrown ){ $.unblockUI(); alert( "Impossible de récupérer les chiffres clés" ); },
									 	success: function( responseXML ){

											var elements = new Array( "fiscal_year","duration","closing_date","turnover","net_income","staff","added_value","gross_operating_profit","sale_efficiency","financial_efficiency" );
											var $xml = $( responseXML );

											var i = 0;
											while( i < elements.length ){
												
												$( "#" + elements[ i ] ).val( $xml.find( elements[ i ] ).text() );
												i++;

											}
											
											$.unblockUI();
											$( "#LastPeriod" ).fadeIn( 700 );
											
										}
							
									});
									
								}
								
							/* ]]> */
							</script>
							<div class="blocMultiple blocMRight">
								
								<h2>Chiffres clés</h2>
								<p style="text-align:right;">
									<a href="#" class="blueLink" onclick="getLastPeriodInfos(); return false;">Récupérer les chiffres du dernier exercice</a>
								</p>
								<div id="LastPeriod" style="display:none;">
								
									<label><span><em>Dernier exercice publié</em></span></label>
									<input id="fiscal_year" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Durée de l'exercice</em></span></label>
									<input id="duration" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Clôture de l'exercice</em></span></label>
									<input id="closing_date" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Chiffre d'affaires</em></span></label>
									<input id="turnover" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Résultat Net</em></span></label>
									<input id="net_income" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Effectif</em></span></label>
									<input id="staff" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Valeur ajoutée</em></span></label>
									<input id="added_value" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Excédent brut d'exploitation</em></span></label>
									<input id="gross_operating_profit" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Rentabilité commerciale</em></span></label>
									<input id="sale_efficiency" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Rentabilité financière</em></span></label>
									<input id="financial_efficiency" value="" class="calendarInput" type="text" />
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
								</div>
								
							</div><!-- blocMRight -->
		
						</div> <!-- #infos-provider -->	
										
						<!-- *********** -->
						<!--  Condition  -->
						<!-- *********** -->
						<div id="condition-provider">	
						
							<div class="blocMultiple blocMLeft">
								<h2>Conditions de règlement</h2>
								<label><span>Mode de règlement</span></label>
								<?php XHTMLFactory::createSelectElement( "payment", "idpayment", "name_1", "name_1", $provider->get("idpayment"), false, false, "name=\"idpayment\" id=\"idpayment\"" ); ?>
								<div class="spacer"></div>
								
								<label><span>Délais de règlement</span></label>
								<?php XHTMLFactory::createSelectElement( "payment_delay", "idpayment_delay", "payment_delay_1", "payment_delay_1", $provider->get( "idpayment_delay" ), false, false, "name=\"idpayment_delay\" id=\"idpayment_delay\"" ); ?>
								<div class="spacer"></div>

								<label><span><em>Banque</em></span></label>
								<input name="bic_bank_name" id="bic_bank_name" value="<?php echo htmlentities( $provider->get( "bic_bank_name" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
								
								<label><span>N° compte SAGE</span></label>
								<input type="text" id="erp_name" name="erp_name" value="<?php echo $provider->get("erp_name");  ?>" class="textidInput" />
								<div class="spacer"></div>
								
								<h2>Comptable</h2>
								
								<label><span><em>Titre</em></span></label>
								<div class="simpleSelect">
								<?php 
									$selectedValueTitle = $provider->getContact()->get("idtitle");
									XHTMLFactory::createSelectElement( "title", "idtitle", "title_1", "title_1", $selectedValueTitle, false, false, "name=\"???\" id=\"???\"" ); 
								?>
								</div>
								<div class="spacer"></div>
								
								<label><span><em>Nom</em></span></label>
								<input name="accountant_lastname" id="accountant_lastname" value="<?php echo htmlentities( $provider->get( "accountant_lastname" ) ); ?>" class="textidInput" type="text" />
								
								<label class="smallLabel"><span><em>Prénom</em></span></label>
								<input name="accountant_firstname" id="accountant_firstname" value="<?php echo htmlentities( $provider->get( "accountant_firstname" ) ); ?>" class="textidInput" type="text" />
								<div class="spacer"></div>
									
								<label><span><strong><em>Tél.</em></strong></span></label>
								<input type="text" name="accountant_phonenumber" id="accountant_phonenumber" value="<?php echo Util::phonenumberFormat( $provider->get( "accountant_phonenumber" ) ); ?>" class="textidInput" />
								
								<label class="smallLabel"><span><strong><em>Fax</em></strong></span></label>
								<input type="text" name="accountant_faxnumber" id="accountant_faxnumber" value="<?php echo Util::phonenumberFormat( $provider->get( "accountant_faxnumber" ) ); ?>" class="textidInput" />							
								<div class="spacer"></div>
								<div class="spacer"></div>
																
							</div><!-- blocMLeft -->
							
							<div class="blocMultiple blocMRight">
								<h2>RIB</h2>
								
								
								
								<label><span>Code établissement</span></label>
								<input id="bic_bank_code" name="bic_bank_code" maxlength="5" value="<?php echo htmlentities( $provider->get( "bic_bank_code" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
								
								<label><span>Code guichet</span></label>
								<input id="bic_branch_code" name="bic_branch_code" maxlength="5" value="<?php echo htmlentities( $provider->get( "bic_branch_code" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
								
								<label><span>Numéro de compte</span></label>
								<input id="bic_account_number" name="bic_account_number" maxlength="11" value="<?php echo htmlentities( $provider->get( "bic_account_number" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
								
								<label><span>Clé RIB</span></label>
								<input id="bic_check_digits" name="bic_check_digits" maxlength="2" value="<?php echo htmlentities( $provider->get( "bic_check_digits" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
								
								<label><span>Domiciliation</span></label>
								<input id="bic_banking_domiciliation" name="bic_banking_domiciliation" maxlength="40" value="<?php echo htmlentities( $provider->get( "bic_banking_domiciliation" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
										
								<label><span>IBAN</span></label>
								<input id="IBAN" name="IBAN" value="<?php echo htmlentities( $provider->get( "IBAN" ) ); ?>" class="textInput" type="text" />
								<div class="spacer"></div>
								
							</div><!-- blocMRight -->
							<div class="spacer"></div>
						
						</div> <!-- #condition-provider -->	

						<div class="spacer"></div>	
	
						<!-- **********	-->
						<!-- Historique  -->
						<!-- **********	-->
						<div id="history-provider">	
							<div id="ProviderHistory">
								<p style="text-align:center;">
									<img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...
								</p>
							</div>
						</div><!-- history-provider -->
						
						<!-- **********	-->
						<!-- Services   -->
						<!-- **********	-->
						<div id="services-provider">	
							<div id="ProviderServices">
								<p style="text-align:center;">
									<img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...
								</p>
							</div>
						</div><!-- services-provider -->

						<!-- **********	-->
						<!-- pieces jointes   -->
						<!-- **********	-->
						<div id="pieces-supplier">	
							<?php 
								define('CONFIG_SYS_DEFAULT_PATH', '../../images/'); //accept relative path only
								define('CONFIG_SYS_ROOT_PATH', '../../images/');	//accept relative path only
								//include("../../js/ajaxfilemanager/ajaxfilemanager.php"); 
							?>
						</div><!-- pieces-supplier -->
						
						<input type="submit" name="Modify" class="inputSave" value="Sauvegarder" />
						<?php
						
							if( $provider->allowDelete() ){
								
								?>
								<input type="button" name="" class="inputSave" value="Supprimer" onclick="if( confirm('Etes-vous certains de vouloir supprimer ce prestataire?') ) document.location = '<?php URLFactory::getHostURL(); ?>/accounting/provider/provider.php?delete&idprovider=<?php echo $provider->getId(); ?>';" style="margin-right:10px;" />
								<?php
								
							}
							
						?>
						<div class="spacer"></div>	
		
			
					</div> <!--  #container -->
					<div class="spacer"></div>
				
				</div></div><!-- blocResult -->
				<div class="spacer"></div>
									
		   </div><!-- contentResult -->
		   <div class="spacer"></div>
		
		</form>
	</div> <!-- Fin blocRightSupplier -->
	<div class="spacer"></div><br/><br/><br/>
</div>
<?php

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getProviderHistory( $idprovider ){
	
	?>
	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    padding-left: 20px; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
		    padding-bottom:15px;
		
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 
	
	</style>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<div class="blocMultiple blocMLeft">
		<h2>Factures de service</h2>
		<?php listProviderInvoices( $idprovider ); ?>
	</div>
	<?php
					
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listProviderInvoices( $idprovider ){

	$query = "
	SELECT pi.idprovider_invoice, 
		COALESCE( SUM( pii.price * pii.quantity ) , 0.0 ) AS totalET,
		COALESCE( SUM( pii.price * pii.quantity ) + pi.vat_amount, 0.0 ) AS totalATI, 
		pi.creation_date,
		u.initial
	FROM provider_invoice pi
	LEFT JOIN provider_invoice_item pii 
	ON ( pii.idprovider = pi.idprovider AND pii.idprovider_invoice = pi.idprovider_invoice ), 
	user u
	WHERE pi.idprovider = '$idprovider'
	AND u.iduser = pi.iduser
	GROUP BY CONCAT( pi.idprovider_invoice, '_', pi.idprovider )
	ORDER BY pi.creation_date DESC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune facture trouvée</p>
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ $( "#ProviderInvoiceTable" ).tablesorter(); });

	/* ]]> */
	</script>
	<div class="tableContainer clear">
		<table id="ProviderInvoiceTable" class="dataTable resultTable sortable">
			<thead>
				<tr>
					<th>Com</th>
					<th>Facture n°</th>
					<th>Total HT</th>
					<th>Total TTC</th>
					<th>Date</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$totalETSum 	= 0.0;
				$totalATISum 	= 0.0;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td class="lefterCol"><?php echo strtoupper( $rs->fields( "initial" ) ); ?></td>
						<td><?php echo htmlentities( $rs->fields( "idprovider_invoice" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "totalET" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "totalATI" ) ); ?></td>
						<td class="righterCol"><?php echo Util::dateFormat( $rs->fields( "creation_date" ), "d-m-Y" ); ?></td>
					</tr>
					<?php
					
					$totalETSum 	+= $rs->fields( "totalET" );
					$totalATISum 	+= $rs->fields( "totalATI" );
					
					$rs->MoveNext();
					
				}
				
			?>
			</tbody>
			<tr>
				<td colspan="2" class="lefterCol"></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $totalETSum ); ?></b></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $totalATISum ); ?></b></td>
				<td class="righterCol">
			</tr>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getProviderServices( $idprovider ){

	?>
	<style type="text/css">
	
		#ProviderServiceList li{
		
			width:80%;
			height:35px;
			border:1px solid #E7E7E7;
			
		}
			
	</style>
	<script type="text/javascript" src="/js/jquery/ui/treeview/jquery.treeview.min.js"></script>
	<script type="text/javascript" src="/js/jquery/jquery.cookie.js"></script>
	<script>
	/* <![CDATA[ */

		/* ---------------------------------------------------------------------------------------------- */
		
		var $selectedTreeNode = null;
		
		$(document).ready( function(){ $("#AccountTree").treeview({
			
				animated: "fast",
				collapsed: true,
				unique: false
				
			}); 

			$("#AccountTree li span").click( function(){
				
				$("#AccountTree li span").css( "font-weight", "normal" );
				$( this ).css( "font-weight", "bold" );
				$selectedTreeNode = $( this ).parent();
				
			});
		
		});

		/* ---------------------------------------------------------------------------------------------- */
		
		function accountDialog(){

			var idaccounting_plan = $selectedTreeNode == null ? 0 : $selectedTreeNode.find( "input[type=hidden][name=idaccounting_plan]" ).val();

			$( "#AccountDialog #AccountNumber" ).val( idaccounting_plan ? idaccounting_plan : "" );
			$( "#AccountDialog #AccountLabel" ).val( "" );
			
			$( "#AccountDialog" ).dialog({
		        
				modal: true,	
				title: "Créer un nouveau compte",
				close: function(event, ui) { 
				
					$("#AccountDialog").dialog( "destroy" );
					
				}
			
			});
			
		}

		/* ---------------------------------------------------------------------------------------------- */
		
		function serviceDialog(){

			var idaccounting_plan = $selectedTreeNode == null ? 0 : $selectedTreeNode.find( "input[type=hidden][name=idaccounting_plan]" ).val();

			if( idaccounting_plan ){
				
				$( "#ServiceAccountNumber" ).val( idaccounting_plan );
				$( "#ServiceAccountLabel" ).val( $selectedTreeNode.find( "span" ).eq( 0 ).text().split( ":" )[ 1 ] );

			}
			
			$( "#ServiceDialog" ).dialog({
		        
				modal: true,	
				title: "Créer un nouveau service",
				width: 350,
				close: function(event, ui) { 
				
					$("#ServiceDialog").dialog( "destroy" );
					
				}
			
			});
			
		}
		
		/* ---------------------------------------------------------------------------------------------- */
		
		function createService(){

			var idaccounting_plan 	= $( "#ServiceAccountNumber" ).val();
			var price 				= parseFloat( $( "#ServicePrice" ).val().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
			var label 				= $( "#ServiceLabel" ).val();
			var vat_rate 			= $( "#ServiceVATRate" ).val();
			
			if( !idaccounting_plan.length ){

				alert( "Merci de saisir un N° de compte valide" );
				return;
				
			}

			if( price <= 0.0 ){

				alert( "Merci de saisir un montant valide" );
				return;
				
			}

			if( !label.length ){

				alert( "Merci de saisir un intitulé valide" );
				return;
				
			}
			
			var data = "idaccounting_plan=" + idaccounting_plan;
			data 	+= "&label=" + escape( label );
			data 	+= "&price=" + escape( price );
			data 	+= "&vat_rate=" + escape( vat_rate );
			data 	+= "&idprovider=<?php echo $idprovider; ?>";
			
			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?create_service",
				async: true,
				type: "GET",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouveau compte" ); },
			 	success: function( responseText ){

					$( '#ServiceDialog' ).dialog( 'destroy' );
					$( "#ServiceForm" ).html( responseText );

				}
	
			});

		}
		
		/* ---------------------------------------------------------------------------------------------- */
		
		function createAccount(){

			var idparent = $selectedTreeNode == null ? 0 : $selectedTreeNode.find( "input[type=hidden][name=idaccounting_plan]" ).val();

			if( !$( "#AccountLabel" ).val().length || !$( "#AccountNumber" ).val().length ){

				alert( "Vous devez saisir un numéro et un nom de compte valide" );	
				return;

			}
			
			var data = "&idaccounting_plan=" + $( "#AccountNumber" ).val();
			data	+= "&label=" + escape( $( "#AccountLabel" ).val() );
			data	+= "&idparent=" + escape( idparent );
			data	+= "&idprovider=<?php echo $idprovider; ?>";

			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?create_account",
				async: true,
				type: "GET",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouveau compte" ); },
			 	success: function( responseText ){

					if( responseText == "0" ){
						
						alert( "Ce numéro de compte existe déjà" );
						return;

					}

					$( '#AccountDialog' ).dialog( 'destroy' );
					$( "#ServiceForm" ).html( responseText );
					
				}
	
			});
			
		}

		/* ---------------------------------------------------------------------------------------------- */
		
		
	/* ]]> */	
	</script>
	<div id="ServiceForm">
		<div class="blocMultiple blocMLeft">
			<h2>Créer un service</h2>			
			<span style="float:right; margin-top:7px;">
				<a href="#" class="blueLink" onclick="serviceDialog(); return false;">
					<img src="/images/back_office/content/flexigrid_button_create.png" alt="" />
					Créer un service
				</a>
			</span>
			<span style="float:right; margin-top:7px; margin-right:10px;">
				<a href="#" class="blueLink" onclick="accountDialog(); return false;">
					<img src="/images/back_office/content/flexigrid_button_create.png" alt="" />
					Créer un compte
				</a>
			</span>
			<br style="clear:both" />
			<ul id="AccountTree" class="treeview" style="margin:20px;">
				<?php getAccounts( 0 ); ?>
			</ul>
		</div>
		<div class="blocMultiple blocMRight">
			<h2>Services facturés par le fournisseur</h2>
			<ul id="ProviderServiceList">
				<?php listProviderServices( $idprovider ); ?>
			</ul>
		</div>
	</div>
	<br style="clear:both;" />
	<?php

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getAccounts( $idparent ){
	
	$query = "
	SELECT ap1.idaccounting_plan, ap1.accounting_plan_1, GROUP_CONCAT( ap2.idaccounting_plan ) AS children
	FROM accounting_plan ap1 
	LEFT JOIN accounting_plan ap2
	ON ap2.accounting_plan_parent = ap1.idaccounting_plan
	WHERE ap1.accounting_plan_parent = '$idparent' AND ap1.idaccounting_plan <> 0
	GROUP BY ap1.idaccounting_plan
	ORDER BY ap1.idaccounting_plan ASC";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		?>
		<li>
			<span>
				<input type="hidden" name="idaccounting_plan" value="<?php echo htmlentities( $rs->fields( "idaccounting_plan" ) ); ?>" />
				<span class='accountText' style="display:none;"><?php echo $rs->fields( "accounting_plan_1" ); ?></span>
				<?php echo htmlentities( $rs->fields( "idaccounting_plan" ) . " : " . $rs->fields( "accounting_plan_1" ) ); ?>
			</span>
			<?php
			
				if( $rs->fields( "children" ) != NULL ){
					
					echo "<ul>"; 
					getAccounts( $rs->fields( "idaccounting_plan" ) ); 
					echo "</ul>";
					
				}

		?>
		</li>
		<?php
		
		$rs->MoveNext();
		
	}

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listProviderServices( $idprovider ){
	
	$query = "
	SELECT s.*, ap.accounting_plan_1 AS accountLabel, pii.idservice IS NULL AS allowDelete
	FROM service s LEFT JOIN provider_invoice_item pii ON pii.idservice = s.idservice, accounting_plan ap
	WHERE s.idprovider = '" . intval( $idprovider ) . "' 
	AND ap.idaccounting_plan = s.idaccounting_plan
	GROUP BY s.idservice
	ORDER BY accountLabel ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">
			Aucun service trouvé
		</p>
		<?php	
		
		return;
		
	}
	
	?>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.min.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ---------------------------------------------------------------------------------------------- */
		
		$( document ).ready( function(){ $( "#Services" ).tablesorter(); });

		/* ---------------------------------------------------------------------------------------------- */
		
		function deleteService( idservice ){

			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?delete_service",
				async: true,
				type: "GET",
				data: "idservice=" + idservice,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le service" ); },
			 	success: function( responseText ){

					if( responseText != "1" ){

						alert( "Impossible de supprimer le service : " + responseText );
						return;
						
					}
					
					$( "#ServiceRow" + idservice ).remove();

				}
	
			});

		}

	/* ---------------------------------------------------------------------------------------------- */
		
		function updateAccountName( $anchor, idservice ){

			var idaccounting_plan = prompt( "Intitulé du compte", $anchor.html() );

			if( idaccounting_plan == null || !idaccounting_plan.length || idaccounting_plan == $anchor.html() )
				return;
			
			$.ajax({
			 	
				url: "/templates/accounting/provider/provider_edit.php?update_account_name",
				async: true,
				type: "GET",
				data: "idservice=" + idservice + "&idaccounting_plan=" + idaccounting_plan,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le service" ); },
			 	success: function( responseText ){

					if( responseText != "1" ){

						alert( "Impossible de modifier le nom du compte : " + responseText );
						return;
						
					}
					
					$anchor.html( idaccounting_plan );

				}
	
			});

		}
		
		/* ---------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<style type="text/css">

		#Services{ margin-top:5px; }
		
		#Services th.header,
		#Services th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    padding-left: 20px; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
		    padding-bottom:15px;
		
		}
		
		#Services th.headerSortUp,
		#Services th.headerSortUp{  background-image: url(/images/small_asc.gif); } 
	
		#Services th.headerSortDown,
		#Services th.headerSortDown{ background-image: url(/images/small_desc.gif); } 
	
	</style>
	<table id="Services" class="dataTable resultTable">
		<thead>
			<tr>
				<th></th>
				<th>N° compte</th>
				<th>Intitulé du compte</th>
				<th>Service facturé</th>
			</tr>
		</thead>
		<tbody>
		<?php
		
			while( !$rs->EOF() ){
				
				?>
				<tr id="ServiceRow<?php echo $rs->fields( "idservice" ); ?>">
					<td class="lefterCol">
					<?php
						
						if( $rs->fields( "allowDelete" ) ){
							
							?>
							<a href="#" title="Supprimer le service" onclick="if( confirm('Etes-vous certains de vouloir supprimer ce service?') )deleteService(<?php echo $rs->fields( "idservice" ); ?>); return false;">
								<img src="/images/back_office/content/corbeille.jpg" alt="" />
							</a>
							<?php
								
						}
						
					?>
					</td>
					<td>
						<a title="Modifier l'intitulé du compte" href="#" onclick="updateAccountName($(this), '<?php echo $rs->fields( "idservice" ); ?>'); return false;"><?php echo htmlentities( $rs->fields( "idaccounting_plan" ) ); ?></a>
					</td>
					<td><?php echo htmlentities( $rs->fields( "accountLabel" ) ); ?></td>
					<td class="righterCol"><?php echo htmlentities( $rs->fields( "label" ) ); ?></td>
				</tr>
				<?php
				
				$rs->MoveNext();
				
			}
			
		?>
		</tbody>
	</table>
	<?php

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

?>