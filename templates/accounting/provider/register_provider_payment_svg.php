<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement règlements factures services
 */

/* ------------------------------------------------------------------------------------------------------------ */

include_once( dirname( __FILE__ ) . "/../../config/init.php" );

include_once( dirname( __FILE__ ) . "/../../objects/ProviderInvoice.php" );
include_once( dirname( __FILE__ ) . "/../../objects/Provider.php" );
include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );
include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
include_once( dirname( __FILE__ ) . "/../../objects/Payment.php" );

header( "Content-Type: text/html; charset=utf-8" );

if( !User::getInstance()->getId() )
	exit( "Votre session a expiré" );

/* ------------------------------------------------------------------------------------------------------------ */
/* enregistrement - ajax */

if( isset( $_POST[ "idprovider_invoice" ] ) && isset( $_POST[ "idprovider" ] ) ){

	$_POST = Util::arrayMapRecursive( "Util::doNothing", Util::arrayMapRecursive( "stripslashes", $_POST ) );

	/* montant du règlement */
	
	$amountSum = 0.0;
	foreach( $_POST[ "amount" ] as $amount )
		$amountSum += Util::text2num( $amount );

	if( $amountSum <= 0.0 )
		exit( "0" );

	/* enregistrement du règlement */
		
	$idregulations_provider = DBUtil::getUniqueKeyValue( "regulations_provider", "idregulations_provider" );
	$bank_date = isset( $_POST[ "bank_date" ] ) ? DBUtil::quote( Util::dateFormat( $_POST[ "bank_date" ], "Y-m-d" ) ) : "";
	
	$query = "
	INSERT INTO regulations_provider(
		idregulations_provider,
		idprovider,
		iduser,
		amount,
		idpayment,
		payment_date,
		deliv_payment,
		DateHeure,
		bank_date,
		lastupdate,
		comment,
		username,
		piece_number
	) VALUES (
		'$idregulations_provider',
		'" . intval( $_POST[ "idprovider" ] ) . "',
		'" . User::getInstance()->getId() . "',
		'" . Util::text2num( $amountSum ) . "',
		'" . intval( $_POST[ "idpayment" ] ) . "',
		" . DBUtil::quote( Util::dateFormat( $_POST[ "payment_date" ], "Y-m-d" ) ) . ",
		" . DBUtil::quote( Util::dateFormat( $_POST[ "deliv_payment" ], "Y-m-d" ) ) . ",
		NOW(),
		'$bank_date',
		NOW(),
		" . DBUtil::quote( $_POST[ "comment" ] ) . ",
		" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
		" . DBUtil::quote( $_POST[ "piece_number" ] ) . "
	)";

	if( DBUtil::query( $query ) === false ){
		
		trigger_error( "Impossible d'enregistrer le règlement", E_USER_ERROR );
		exit( "0" );
		
	}

	/* enregistrement du détail par facture */
	
	$i = 0;
	while( $i < count( $_POST[ "idprovider_invoice" ] ) ){
		
		if( Util::text2num( $_POST[ "amount" ][ $i ] > 0.0 ) ){
			
			$query = "
			INSERT INTO billing_regulations_provider(
				idprovider_invoice,
				idregulations_provider,
				idprovider,
				amount,
				lastupdate,
				username
			) VALUES (
				" . DBUtil::quote( $_POST[ "idprovider_invoice" ][ $i ] ) . ",
				'$idregulations_provider',
				'" . intval( $_POST[ "idprovider" ] ) . "',
				'" . Util::text2num( $_POST[ "amount" ][ $i ] ) . "',
				NOW(),
				" . DBUtil::quote( User::getInstance()->get( "login" ) ) . "
			)";
			
			if( DBUtil::query( $query ) === false ){
				
				trigger_error( "Impossible d'enregistrer le règlement", E_USER_ERROR );
				exit( "0" );
			
			}
		
		}
		
		$i++;
			
	}
	
	exit( "1" );

}

/* ------------------------------------------------------------------------------------------------------------ */
/* suppression règlement - ajax */

if( isset( $_REQUEST[ "drop" ] ) && $_REQUEST[ "drop" ] == "payment" ){

	$rs =& DBUtil::query( "SELECT amount FROM billing_regulations_provider WHERE idregulations_provider = '" . intval( $_REQUEST[ "idregulations_provider" ] ) . "'" );
	
	DBUtil::query( "DELETE FROM billing_regulations_provider WHERE idregulations_provider = '" . intval( $_REQUEST[ "idregulations_provider" ] ) . "'" );

	if( $rs->RecordCount() == 1 ){

		DBUtil::query( "DELETE FROM regulations_provider WHERE idregulations_provider = '" . intval( $_REQUEST[ "idregulations_provider" ] ) . "' LIMIT 1" );
		exit( "1" );
			
	}

	DBUtil::query( "UPDATE regulations_provider SET amount = GREATEST( 0.0, amount - " . $rs->fields( "amount" ) . " ) WHERE idregulations_provider = '" . intval( $_REQUEST[ "idregulations_provider" ] ) . "' LIMIT 1" );	
	
	exit( "1" );
		
}

/* ------------------------------------------------------------------------------------------------------------ */

if( !isset( $_REQUEST[ "idprovider" ] ) || !isset( $_REQUEST[ "invoices" ] ) )
	exit( "paramètre absent" );

$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );

$invoices = explode( ",", $_REQUEST[ "invoices" ] );
$i = 0;
while( $i < count( $invoices ) ){
	
	$invoices[ $i ] = new ProviderInvoice( $invoices[ $i ], $provider->getId() );
	$i++;
	
}

/* ------------------------------------------------------------------------------------------------------------ */

header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );
header( "Content-Type: text/html; charset=utf-8" );
	
?>
<script type="text/javascript" src="/js/form.lib.js"></script>
<script type="text/javascript">
/* <![CDATA[ */

    /* ------------------------------------------------------------------------------------------------ */
    /* désélection facture */
    
	function dropInvoice( idprovider_invoice ){

		$( "input:checked[type=checkbox][id^='<?php echo intval( $_REQUEST[ "idprovider" ] ); ?>_'][value='" + idprovider_invoice + "']" ).attr( "checked", false );

		$('#PaymentDialog').dialog('destroy');

		paymentDialog( <?php echo intval( $_REQUEST[ "idprovider" ] ); ?> );
		
	}

	/* ------------------------------------------------------------------------------------------------ */
    /* suppression règlement */
    
    function dropPayment( idregulations_provider ){

		if( !confirm( "Etes-vous certains de vouloir supprimer ce règlement?" ) )
			return;
		
		$.ajax({
		 	
			url: "/accounting/provider/register_provider_payment.php?drop=payment",
			async: true,
			type: "GET",
			data: "idregulations_provider=" + idregulations_provider,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le dialogue" ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert( "Impossible de supprimer le règlement : " + responseText );
				else{
					
					$('#PaymentDialog').dialog('destroy');
					paymentDialog( <?php echo intval( $_REQUEST[ "idprovider" ] ); ?> );

				}
				
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------ */
	/* recalcul des totaux */
	
	function recalculate(){

		var amountSum = 0.0;

		$( "input[type='text'][name='amount[]']" ).each( function( i ){
			
			amount = parseFloat( $( this ).val().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );

			if( amount )
				amountSum += amount;
			
		} );

		$( "#AmountSum" ).html( amountSum.toFixed( 2 ) + " ¤" );

		var balanceOutstandingSum = parseFloat( $( "#BalanceOutstandingSum" ).html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );

		$( "#AmountSum" ).css( "color", amountSum > balanceOutstandingSum ? "#FF0000" : "#333333" );

	}

	/* ------------------------------------------------------------------------------------------------ */
	/* enregistrer le règlement */
	
	function savePayment(){

		$( "#PaymentForm" ).ajaxSubmit({
			
			success: function( responseText, statusText ){

				if( responseText != "1" ){
					
					alert( "Un problème est survenu lors de l'enregistrement du règlement : " + responseText );
					return;

				}

				$('#PaymentDialog').dialog('destroy');
				paymentDialog( <?php echo intval( $_REQUEST[ "idprovider" ] ); ?> );
				$( "input:checked[type=checkbox][id^='<?php echo intval( $_REQUEST[ "idprovider" ] ); ?>_']" ).parent().parent().css( "opacity", "0.5" );
				$( "input:checked[type=checkbox][id^='<?php echo intval( $_REQUEST[ "idprovider" ] ); ?>_']" ).remove();
				
			}
		
		}); 
		
	}
	
	/* ------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<div id="globalMainContent" style="margin-top:-25px; width:100%;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent fullWidthContent" style="width:100%;">
		<div class="topRight"></div>
		<div class="topLeft"></div>
		<div class="content">
			<form id="PaymentForm" action="/accounting/provider/register_provider_payment.php" method="post">
				<input type="hidden" name="idprovider" value="<?php echo intval( $_REQUEST[ "idprovider" ] ); ?>" />
				<div class="subContent" style="margin-top:40px;">
				<?php
				
					/* règlements déjà saisis pour les factures sélectionnées */
				
					listRegulations( $invoices ); 
		
					/* formulaire pour nouveau règlement */
					
					$balanceOutstangingSum = 0.0;
					foreach( $invoices as $invoice )
						$balanceOutstangingSum += $invoice->getBalanceOutStanding();
				
					if( $balanceOutstangingSum > 0.0 )
						displayPaymentForm( $invoices );
					
					?>
				</div>
				<div class="clear"></div>
				<div class="subContent" style="margin-top:10px;">
					<div class="submitButtonContainer" style="float:right; padding-bottom:15px;">
					<?php
					
						if( $balanceOutstangingSum > 0.0 ){
							
							?>
							<input type="button" name="" class="blueButton" value="Annuler" onclick="$('#PaymentDialog').dialog('destroy');" />
							<input type="button" name="" class="blueButton" value="Enregistrer" onclick="savePayment();" />
							<?php
							
						}
						else{
							
							?>
							<input type="button" name="" class="blueButton" value="Terminer" onclick="$('#PaymentDialog').dialog('destroy');" />
							<?php
							
						}
						
					?>
					</div>
				</div>
			</form>
			<div class="clear"></div>
		</div>
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div>
</div>
<?php

/* ------------------------------------------------------------------------------------------------------------ */
/**
 * Formulaire de saisie
 * @param array<ProviderInvoice> $invoices
 * @return void
 */
function displayPaymentForm( array $invoices ){
	
	?>
	<style type="text/css">
		.ui-datepicker{ z-index: 2999; }
	</style>
	<div class="subTitleContainer">
		<p class="subTitle">Nouveau règlement</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<thead>
				<tr>
					<th class="filledCell" style="text-align:center; width:25%;">Date de paiement</th>
					<th class="filledCell" style="text-align:center; width:25%;">Date d'échéance</th>
					<th class="filledCell" style="text-align:center; width:25%;">Mode de paiement</th>
					<th class="filledCell" style="text-align:center; width:25%;">Pièce n°</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td style="text-align:center;">
						<input type="text" name="payment_date" id="payment_date" value="<?php echo date( "d/m/Y" ); ?>" class="calendarInput paymentDate" />
						<?php DHTMLCalendar::calendar( "payment_date", true ); ?>
					</td>
					<td style="text-align:center;">
						<input type="text" name="deliv_payment" id="deliv_payment" value="<?php $invoice = $invoices[ 0 ]; echo Util::dateFormat( $invoice->get( "deliv_payment" ), "d/m/Y" ); ?>" class="calendarInput paymentDate" />
						<?php DHTMLCalendar::calendar( "deliv_payment", 	true ); ?>
					</td>
					<td style="text-align:center;">
						<?php XHTMLFactory::createSelectElement( "payment", "idpayment", "name_1", "name_1", false, false, "", "name=\"idpayment\"" ); ?>	
					</td>
					<td style="text-align:center;">
						<input type="text" name="piece_number" value="" class="textInput" style="width:220px;" />
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="subTitleContainer">
		<p class="subTitle">Montants</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<thead>
				<tr>
					<th class="filledCell"></th>
					<th class="filledCell" style="text-align:center; white-space:nowrap;">Facture n°</th>
					<th class="filledCell" style="text-align:center; width:90px;">TTC facture</th>
					<th class="filledCell" style="text-align:center; width:90px;">Acompte</th>
					<th class="filledCell" style="text-align:center; width:90px;">Escompte</th>
					<th class="filledCell" style="text-align:center; width:90px;">Net à payer</th>
					<th class="filledCell" style="text-align:center; width:90px;">Réglé</th>
					<th class="filledCell" style="text-align:center; width:90px;">Solde TTC</th>
					<th class="filledCell" style="text-align:center; width:90px;">Montant</th>
				</tr>
			</thead>
			<tbody>
			<?php
	
				$totalATISum 			= 0.0;
				$downPaymentSum 		= 0.0;
				$rebateSum 				= 0.0;
				$netBillSum 			= 0.0;
				$paidSum 				= 0.0;
				$balanceOutstandingSum 	= 0.0;
				
				foreach( $invoices as $invoice ){
	
					?>
					<tr>
						<td style="text-align:center; width:19px; padding:0px;">
							<a href="#" onclick="dropInvoice( '<?php echo str_replace( "'", "\'", $invoice->getId() ); ?>' ); return false;" title="Ne pas règler cette facture">
								<img src="/images/back_office/content/collapse.png" alt="" />
							</a>
						</td>
						<td style="text-align:center;">
							<?php echo htmlentities( $invoice->getId() ); ?>
							<input type="hidden" name="idprovider_invoice[]" value="<?php echo htmlentities( $invoice->getId() ); ?>" />
						</td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $invoice->getTotalATI() ); ?></td>
						<td style="text-align:right;"><?php echo $invoice->get( "down_payment_amount" ) > 0.0 ? Util::priceFormat( $invoice->get( "down_payment_amount" ) ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo $invoice->get( "rebate_amount" ) > 0.0 ? Util::priceFormat( $invoice->get( "rebate_amount" ) ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $invoice->getNetBill() ); ?></td>
						<td style="text-align:right;"><?php echo $invoice->getNetBill() - $invoice->getBalanceOutstanding() > 0.0 ? Util::priceFormat( $invoice->getNetBill() - $invoice->getBalanceOutstanding() ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $invoice->getBalanceOutstanding() ); ?></td>
						<td style="text-align:right;">
							<input type="<?php echo $invoice->getBalanceOutstanding() > 0.0 ? "text" : "hidden"; ?>" name="amount[]" onkeypress="return forceUnsignedFloatValue( event );" onkeyup="recalculate();" onchange="this.onkeyup( event );" value="<?php echo Util::priceFormat( $invoice->getBalanceOutstanding() ); ?>" class="textInput" style="text-align:right; width:70px;" />
						</td>
					</tr>
					<?php 
				
					$totalATISum 			+= $invoice->getTotalATI();
					$downPaymentSum 		+= $invoice->get( "down_payment_amount" );
					$rebateSum				+= $invoice->get( "rebate_amount" );
					$netBillSum				+= $invoice->getNetBill();
					$paidSum 				+= $invoice->getNetBill() - $invoice->getBalanceOutstanding();
					$balanceOutstandingSum 	+= $invoice->getBalanceOutstanding();
					
				}
			
			?>
			</tbody>
			<tfoot>
				<tr style="font-weight:bold;">
					<td colspan="2" style="border-style:none;"></td>
					<td style="text-align:right;"><?php echo Util::priceFormat( $totalATISum ); ?></td>
					<td style="text-align:right;"><?php echo $downPaymentSum > 0.0 ? Util::priceFormat( $downPaymentSum ) : "-"; ?></td>
					<td style="text-align:right;"><?php echo $rebateSum > 0.0 ? Util::priceFormat( $rebateSum ) : "-"; ?></td>
					<td style="text-align:right;"><?php echo $netBillSum > 0.0 ? Util::priceFormat( $netBillSum ) : "-"; ?></td>
					<td style="text-align:right;"><?php echo $paidSum > 0.0 ? Util::priceFormat( $paidSum ) : "-"; ?></td>
					<td id="BalanceOutstandingSum" style="text-align:right;"><?php echo Util::priceFormat( $balanceOutstandingSum ); ?></td>
					<td id="AmountSum" style="text-align:right;"><?php echo Util::priceFormat( $balanceOutstandingSum ); ?></td>
				</tr>
			</tfoot>
		</table>
	</div>
	<div class="subTitleContainer">
		<p class="subTitle">Commentaire</p>
	</div>
	<textarea name="comment" style="margin-top:10px; width:100%; height:70px;"></textarea>
	<?php

}

/* ------------------------------------------------------------------------------------------------------------ */
/**
 * Liste des règlements déjà saisis
 * @param array<ProviderInvoice> $invoices
 * @return float la somme des règlements déjà saisis
 */
function listRegulations( array $invoices ){
	
	$idprovider = $invoices[ 0 ]->getProvider()->getId();
	$ids = array();
	
	$i = 0;
	while( $i < count( $invoices ) ){
		
		$invoice = $invoices[ $i ];
		$ids[] = DBUtil::quote( $invoice->getId() );	
	
		$i++;
		
	}
	
	$query = "
	SELECT rp.payment_date, 
		rp.deliv_payment, 
		rp.piece_number,
		rp.comment,
		rp.idregulations_provider,
		SUM( brp.amount ) AS amount,
		GROUP_CONCAT( brp.idprovider_invoice ) AS idprovider_invoice,
		p.name_1 AS payment
	FROM billing_regulations_provider brp, regulations_provider rp, payment p
	WHERE brp.idprovider = '$idprovider'
	AND brp.idprovider_invoice IN (" . implode( ",", $ids ) . ")
	AND rp.idregulations_provider = brp.idregulations_provider
	AND p.idpayment = rp.idpayment
	GROUP BY rp.idregulations_provider
	ORDER BY rp.payment_date";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return;
		
	?>
	<div class="subTitleContainer">
		<p class="subTitle">Règlements déjà saisis pour les factures sélectionnées</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<thead>
				<tr>
					<th class="filledCell" style="text-align:center; width:24px;"></th>
					<th class="filledCell" style="text-align:center; width:120px;">Date de paiement</th>
					<th class="filledCell" style="text-align:center; width:120px;">Date d'échéance</th>
					<th class="filledCell" style="text-align:center; width:120px;">Mode de paiement</th>
					<th class="filledCell" style="text-align:center; width:120px;">Pièce n°</th>
					<th class="filledCell" style="text-align:center;">Commentaire</th>
					<th class="filledCell" style="text-align:center; width:auto; white-space:nowrap;">Facture(s) n°</th>
					<th class="filledCell" style="text-align:center; width:90px;">Montant</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$amountSum = 0.0;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td style="text-align:center;">
							<a href="#" onclick="dropPayment(<?php echo $rs->fields( "idregulations_provider" ); ?>); return false;" title="Supprimer ce règlement">
								<img src="/images/back_office/content/corbeille.jpg" alt="" />
							</a>
						</td>
						<td style="text-align:center;"><?php echo Util::dateFormat( $rs->fields( "payment_date" ), "d/m/Y" ); ?></td>
						<td style="text-align:center;"><?php echo Util::dateFormat( $rs->fields( "deliv_payment" ), "d/m/Y" ); ?></td>
						<td style="text-align:center;"><?php echo htmlentities( $rs->fields( "payment" ) ); ?></td>
						<td style="text-align:center;"><?php echo htmlentities( $rs->fields( "piece_number" ) ); ?></td>
						<td style="text-align:center;"><?php echo htmlentities( $rs->fields( "comment" ) ); ?></td>
						<td style="text-align:center;"><?php echo htmlentities( str_replace( ",", ", ", $rs->fields( "idprovider_invoice" ) ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "amount" ) ); ?></td>
					</tr>
					<?php
					
					$amountSum += $rs->fields( "amount" );
					
					$rs->MoveNext();
					
				}
	
				?>
			</tbody>
			<tr>
				<td colspan="7" style="border-style:none;"></td>
				<td style="text-align:right; font-weight:bold;" id="PaymentSum"><?php echo Util::priceFormat( $amountSum ); ?></td>
			</tr>
		</table>
	</div>
	<?php
	
	return $amountSum;
	
}

/* ------------------------------------------------------------------------------------------------------------ */

?>