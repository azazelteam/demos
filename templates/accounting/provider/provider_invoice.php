<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Factures services
 */


include( dirname( __FILE__ ) . "/../../config/init.php" );

include_once( dirname( __FILE__ ) . "/../../objects/ProviderInvoice.php" );
include_once( dirname( __FILE__ ) . "/../../objects/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );
include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
header( "Content-Type: text/html; charset=utf-8" );

/* --------------------------------------------------------------------------------------------- */
/* enregistrement facture - ajax */

if( isset( $_POST[ "save" ] ) ){

	if( !User::getInstance()->getId() )
		exit();

	save();

	exit();

}

/* --------------------------------------------------------------------------------------------- */
/* suppression facture - ajax */
if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "invoice" ){

	if( !User::getInstance()->getId() )
		exit();

	ProviderInvoice::delete( $_REQUEST[ "idprovider_invoice" ], intval( $_REQUEST[ "idprovider" ] ) );
		
	header( "Location: /accounting/provider/provider_invoice.php" );
	
	exit();
	
}

/* --------------------------------------------------------------------------------------------- */
/* création nouveau service - ajax */

if( isset( $_REQUEST[ "create" ] ) && $_REQUEST[ "create" ] == "service" ){

	if( !User::getInstance()->getId() )
		exit();

	$query = "
	INSERT IGNORE INTO service( idprovider, label, idaccounting_plan, price, vat_rate )
	VALUES( 
		'" . intval( $_REQUEST[ "idprovider" ] ) . "',
		" . DBUtil::quote( stripslashes( Util::doNothing( $_REQUEST[ "label" ] ) ) ) . ",
		'" . intval( $_REQUEST[ "idaccounting_plan" ] ) . "',
		'" . Util::text2num( $_REQUEST[ "price" ] ) . "',
		'" . Util::text2num( $_REQUEST[ "vat_rate" ] ) . "' 
	)";
	
	DBUtil::query( $query );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	echo '<option value="0">-</option>';
	
	listProviderServices( intval( $_REQUEST[ "idprovider" ] ) );
	
	exit();
	
}

/* --------------------------------------------------------------------------------------------- */
/**
 * facturation d'un service - ajax
 */
if( isset( $_REQUEST[ "add" ] ) && $_REQUEST[ "add" ] == "item" ){

	if( !User::getInstance()->getId() )
		exit();
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$service = DBUtil::stdclassFromDB( "service", "idservice", intval( $_REQUEST[ "idservice" ] ) );
	
	?>
	<tr>
		<td>
			<input type="hidden" name="idservice[]" value="<?php echo $service->idservice; ?>" />
			<?php echo htmlentities( $service->label ); ?>
			<a href="#" onclick="$( this ).parents( 'tr' ).remove(); recalculate(); return false;" style="float:left;">
				<img src="/images/back_office/content/corbeille.jpg" alt="" />
			</a>
		</td>
		<td><input type="text" name="label[]" class="textInput" value="<?php echo htmlentities( $service->label ); ?>" /></td>
		<td style="text-align:right;"><input type="text" name="price[]" class="textInput price" value="<?php echo Util::priceFormat( $service->price ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
		 <td style="text-align:right;"><input type="text" name="ttcprice[]" class="textInput price" value="<?php echo Util::priceFormat( $service->ttcprice ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
		<td style="text-align:right;"><input type="text" name="quantity[]" class="textInput price" value="1" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
         <td style="text-align:right;"><input type="text" name="vat_rate[]" class="textInput price" value="<?php echo Util::rateFormat( $service->vat_rate ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
		<td style="text-align:right;"><?php echo Util::priceFormat( $service->price ); ?></td>
     
	</tr>
	<?php
	
	exit();
	
}
			
/* --------------------------------------------------------------------------------------------- */
/**
 * Liste des services facturables pour un prestataire donné - ajax
 */
if( isset( $_REQUEST[ "provider_services" ] ) && isset( $_REQUEST[ "idprovider" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit();
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	listProviderServices( intval( $_REQUEST[ "idprovider" ] ) );
	
	exit();
	
}

/* --------------------------------------------------------------------------------------------- */
/**
 * Coordonnées du prestataire - ajax
 */
if( isset( $_GET[ "provider_info" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit();
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	getProviderInfo( intval( $_GET[ "idprovider" ] ) ? new Provider( intval( $_GET[ "idprovider" ] ) ) : NULL );
	
	exit();
	
}
/*
	@author:Behzad
	make payment
*/
if( isset( $_GET[ "payment_mode" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if(isset($_GET['idprovider']) && !empty($_GET['idprovider'])) {
		$idprovider = $_GET['idprovider'];
		if(isset($_GET['idprovider_invoice']) && !empty($_GET['idprovider_invoice'])) {
			$idprovider_invoice = $_GET['idprovider_invoice'];
			$rs_payment = DBUtil::query( "SELECT `idpayment` FROM `provider_invoice` WHERE `idprovider_invoice` = $idprovider_invoice" );
			$idpayment =  $rs_payment->fields( "idpayment" );
			//echo("idpayment:".$idpayment);
		}
		else {	
			$rs_payment = DBUtil::query( "SELECT `idpayment` FROM `provider` WHERE `idprovider` = $idprovider" );
			$idpayment =  $rs_payment->fields( "idpayment" );
		}
		DrawLift_modepayment($idpayment);
	}
	exit();
	
	
}


 
/* --------------------------------------------------------------------------------------------- */

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$providerInvoice = NULL;

if( isset( $_REQUEST[ "idprovider_invoice" ] ) && isset( $_REQUEST[ "idprovider" ] ) )
	$providerInvoice = new ProviderInvoice( $_REQUEST[ "idprovider_invoice" ], intval( $_REQUEST[ "idprovider" ] ) );

?>
<link rel="stylesheet" type="text/css" href="/css/back_office/form_default.css" />
<link rel="stylesheet" type="text/css" href="/js/jquery/ui/treeview/jquery.treeview.css" />
<script type="text/javascript" src="/js/jquery/ui/treeview/jquery.treeview.min.js"></script>
<script type="text/javascript" src="/js/jquery/jquery.cookie.js"></script>
<script type="text/javascript">
/* <![CDATA[ */

    /* ------------------------------------------------------------------------------------------------------ */
	<?php
	
		/* facture comptabilisée */
	
		if( $providerInvoice && DBUtil::query( 
			"SELECT 1 FROM account_plan 
			WHERE action = 'provider_invoices' 
			AND idrelation = '" . $providerInvoice->getProvider()->getId() . "'
			AND relation_type = 'provider'
			AND idaccount_record = " . DBUtil::quote( $providerInvoice->getId() ) . "
			LIMIT 1" )->RecordCount() ){
		
			?>
			$( document ).ready( function(){

				$( "input, select, textarea" ).attr( "disabled", "disabled" );
				$( "#CreationDate" ).append( '<span style="color:#FF0000; display:block;"><b>Comptabilisée</b></span>' );
				
			});
			
			<?php
		
		}
	
	?>
	/* ------------------------------------------------------------------------------------------------------ */

	$( document ).ready( function(){
	
		$('#ProviderInvoiceForm').ajaxForm( { success: postSubmitCallback } );

		$("#AccountTree").treeview({
			
			animated: "fast",
			collapsed: true,
			unique: true
			
		});

		$("#AccountTree li span").click( function(){
			
			$("#AccountTree li span").css( "font-weight", "normal" );
			$( this ).css( "font-weight", "bold" );

			$( "#AccountNumber" ).val( $( this ).parent().find( "input[type=hidden][name=idaccounting_plan]" ).val() );
			$( "#AccountName" ).val( $( this ).parent().find( "input[type=hidden][name=accounting_plan_1]" ).val() );
					
		});
		
		selectPaymentMode($( "#idprovider" ).val(), $( "#idprovider_invoice" ).val());
		
	});

	/* ------------------------------------------------------------------------------------------------------ */
	
	function postSubmitCallback( responseText, statusText ){

		if( statusText != "success" || responseText != "1" ){
			
			alert( "Impossible de sauvegarder la facture :\n" + responseText );
			return;
			
		}

		$( "#idprovider" ).replaceWith( '<input type="hidden" name="idprovider" id="idprovider" value="' + $( "#idprovider" ).val() + '" /><input type="text" readonly="readonly" value="' + $( "#idprovider option:selected").eq( 0 ).html() + '" class="textInput">' );
		$( "#idprovider_invoice[type=text]" ).replaceWith( '<input type="hidden" name="idprovider_invoice" id="idprovider_invoice" value="' + $( "#idprovider_invoice" ).val() + '" /> ' + $( "#idprovider_invoice" ).val() );
		$( "#DeleteButton" ).css( "display", "inline" );
		
		$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
		$.blockUI.defaults.css.fontSize = '12px';
    	$.growlUI( '', "Modifications sauvegardées" );
    	
	}
	
	/* ------------------------------------------------------------------------------------------------------ */

	function createServiceDialog(){

		$( "#CreateServiceDialog" ).dialog({
	        
			modal: true,	
			title: "Créer un nouveau service",
			close: function(event, ui) { 
			
				$("#CreateServiceDialog").dialog( "destroy" );
				
			},
			width: 800
			
		});
		
	}

	/* ------------------------------------------------------------------------------------------------------ */

	function createService(){

		if( !$( "#AccountNumber" ).val().replace( new RegExp( "[^0-9]+", "g" ), "" ).length ){

			alert( "Vous devez sélectionner un n° de compte" );
			return false;
			
		}

		if( !$( "#label" ).val().length ){

			alert( "Vous devez saisir une désignation" );
			return false;
			
		}

		var data = "idprovider=" + $( "#idprovider" ).val();
		data 	+= "&idaccounting_plan=" + $( "#AccountNumber" ).val().replace( new RegExp( "[^0-9]+", "g" ) );
		data 	+= "&label=" + $( "#label" ).val();
		data 	+= "&price=" + $( "#price" ).val();
			data 	+= "&vat_rate=" + $( "#vat_rate" ).val();
		$.ajax({
		 	
			url: "/accounting/provider/provider_invoice.php?create=service",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le service" ); },
		 	success: function( responseText ){

				$( "#ProviderServices" ).html( responseText );
				
			}

		});
		
		return true;
		 
	}
	
	/* ------------------------------------------------------------------------------------------------------ */

	function deleteInvoice(){

		document.location = "/accounting/provider/provider_invoice.php?delete=invoice&idprovider_invoice=" + escape( $( "#idprovider_invoice" ).val() ) + "&idprovider=" + $( "#idprovider" ) .val();
		
	}

	/* ------------------------------------------------------------------------------------------------------ */
	
	function getAccountNumber( obj ){

		$( "#AccountNumber" ).val( obj.info.split( ":" )[ 0 ].replace( new RegExp( " ", "g" ), "" ) );
		
	}
	
	/* ------------------------------------------------------------------------------------------------------ */

	function selectPaymentMode( idprovider, idprovider_invoice ){
		
		if ( idprovider_invoice === undefined )
			idprovider_invoice = '';

		$.ajax({
			
			url: "<?php echo URLFactory::getHostURL(); ?>/accounting/provider/provider_invoice.php?payment_mode&idprovider=" + idprovider + "&idprovider_invoice=" + idprovider_invoice,
			async: true,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer le mode de règlement du prestatire" ); },
			success: function( responseText ){

				$( "#payment_mode" ).html( responseText );
				
			}

		});
	
	}

	/* ------------------------------------------------------------------------------------------------------ */

/* ]]> */
</script>
<div id="CreateServiceDialog" style="display:none;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>

<?php
	
	include_once( dirname( __FILE__ ) . "/../../objects/AutoCompletor.php" );
	AutoCompletor::completeFromDBPattern( "AccountName", "accounting_plan", "accounting_plan_1", "{idaccounting_plan} : {accounting_plan_1}", 15, "getAccountNumber" );
		
	?>
	<div class="blocMultiple blocMLeft" style="border-right:2px dotted #E7E7E7;">
		<label><span><strong>N° de compte :</strong></span></label>
		<br style="clear:both;" />
		<ul id="AccountTree" class="treeview" style="margin:20px;">
			<?php listAccounts( 0 ); ?>
		</ul>
		<div class="spacer"></div>
	</div>
	<div class="blocMultiple blocMRight">
		<label><span><strong>N° de compte :</strong></span></label>
		<input type="text" id="AccountNumber" value="" class="textInput" />
		<label><span><strong>Nom du compte :</strong></span></label>
		<input type="text" id="AccountName" value="" class="textInput" />
		<label><span><strong>Référence :</strong></span></label>
		<input type="text" id="label" value="" class="textInput" />
		<!--<label><span><strong>Montant HT ( optionnel ):</strong></span></label>
		<input type="text" id="price" value="0.0" class="textInput" />-->
	</div>
	<p style="text-align:right; margin:15px 0px;">
		<input type="button" class="blueButton" value="Annuler" onclick="$('#CreateServiceDialog').dialog( 'destroy' );" />
		<input type="button" class="blueButton" value="Créer" onclick="if( createService() ) $( '#CreateServiceDialog' ).dialog( 'destroy' );" />
	</p>
</div>
<form id="ProviderInvoiceForm" method="post" action="/accounting/provider/provider_invoice.php<?php if( $providerInvoice ) echo "?idprovider_invoice=" . urlencode( $providerInvoice->getId() ) . "&amp;idprovider=" . $providerInvoice->getProvider()->getId(); ?>">
	<input type="hidden" name="save" value="1" />
	<div class="centerMax">
		<div class="rightTools"> <!-- Colonne de droite -->
			<h2>&nbsp;Outils</h2>
			<div class="spacer"></div>
			
		</div> <!-- Fin .rightTools -->
		<div class="contentDyn"> <!-- Bloc de recherche -->
			<h1 class="titleSearch">
				<span class="textTitle">
					Facture de service n° 
					<?php
					
						if( $providerInvoice ){
							
							?>
							<input type="hidden" id="idprovider_invoice" name="idprovider_invoice" value="<?php echo htmlentities( $providerInvoice->get( "idprovider_invoice" ) ); ?>" />
							<?php
							
							echo htmlentities( $providerInvoice->get( "idprovider_invoice" ) );
							
						}
						else{
							
							?>
							<input class="textInput" type="text" id="idprovider_invoice" name="idprovider_invoice" value="" />
							<?php
					
						}
						
					?>
					du <input type="text" class="calendarInput" id="InvoiceDate" name="invoice_date" value="<?php echo $providerInvoice ? usDate2eu( $providerInvoice->get( "invoice_date" ) ) : date( "d-m-Y" ); ?>" />
					<?php DHTMLCalendar::calendar( "InvoiceDate", false ); ?>
				</span>
				<span class="selectDate" id="CreationDate">créée le <?php echo strftime( "%d %B %Y", $providerInvoice ? strtotime( $providerInvoice->get( "creation_date" ) ) : time() ); ?></span>
				<div class="spacer"></div>
			</h1>
			<div class="spacer"></div>
			<div class="blocSearch">
				<div id="ProviderInfo" class="marginLeft">
					<?php getProviderInfo( $providerInvoice ? $providerInvoice->getProvider() : NULL, $providerInvoice == NULL ); ?>
					<div class="spacer"></div>
				</div><!-- Fin .marginLeft -->
				<div class="spacer"></div>
			</div><!-- Fin .blocSearch -->
			<div class="spacer"></div>
		</div><!-- Fin .contentDyn -->
		<div class="spacer"></div>	
		<div class="contentDyn">
			<h1 class="titleSearch">
				<span class="textTitle" style="width:300px;">Services facturés</span>
				<span id="ServiceControls" style="display:<?php echo $providerInvoice ? "block" : "none"; ?>; width:600px;" class="selectDate">
					Facturer un service
					<select id="ProviderServices" onchange="addItem( $( this ) );">
						<option value="0">-</option>
						<?php if( $providerInvoice ) listProviderServices( $providerInvoice->getProvider()->getId() ); ?>
					</select>
					<a href="#" onclick="createServiceDialog(); return false;" style="margin-left:15px; white-space:nowrap;">
						<img src="/images/back_office/content/flexigrid_button_create.png" alt="" />
						Créer un service
					</a>
				</span>
				<div class="spacer"></div>
			</h1>
			<div class="spacer"></div>
			<div class="blocResult mainContent">
				<div id="Services" class="content">
					<?php getServices( $providerInvoice ); ?>
				</div><!-- Fin .content -->
			</div>
		</div><!-- Fin .contentDyn -->
		<div class="spacer"></div>
		<div class="contentDyn">
			<h1 class="titleSearch">
				<span class="textTitle">Paiement et échéance</span>
				<div class="spacer"></div>
			</h1>
			<div class="spacer"></div>
			<div class="blocResult mainContent">
				<div class="content">
					<table class="dataTable devisTable">
						<thead>
							<tr class="filledRow">
								<th style="width:20%;">Mode de paiement</th>
								<th style="width:20%;">Délai de paiement</th>
								<th style="width:20%;">Date d'échéance</th>
								<th style="width:20%;">Escompte</th>
								<th style="width:20%;">NET A PAYER</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td id="payment_mode">
								<?echo=DrawLift_modepayment();?>
									
								</td>
								<td><?php XHTMLFactory::createSelectElement( "payment_delay", "idpayment_delay", "payment_delay_1", "payment_delay_1", $providerInvoice ? $providerInvoice->get( "idpayment_delay" ) : 0, false, "", "name=\"idpayment_delay\"" ); ?></td>
								<td>
									<input type="text" class="calendarInput" id="DelivPayment" name="deliv_payment" value="<?php echo $providerInvoice ? usDate2eu( $providerInvoice->get( "deliv_payment" ) ) : date( "d-m-Y" ); ?>" />
									<?php DHTMLCalendar::calendar( "DelivPayment", false ); ?>
								</td>
								<td id="Rebate" style="font-weight:bold;">
									<input type="text" name="rebate_amount" class="textInput price" value="<?php echo number_format( $providerInvoice ? $providerInvoice->get( "rebate_amount" ) : 0.0, 2, ",", " " ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /> ¤
								</td>
								<td id="NetBill" style="font-weight:bold;">
									<?php echo Util::priceFormat( $providerInvoice ? $providerInvoice->getNetBill() : 0.0 ); ?>
								</td>
							</tr>
						</tbody>
					</table>
				</div><!-- Fin .content -->
			</div><!-- Fin .blocResult -->
		</div><!-- Fin .contentDyn -->
		<div class="spacer"></div>	
		<div class="contentDyn">
			<h1 class="titleSearch">
				<span class="textTitle">Commentaires</span>
				<div class="spacer"></div>
			</h1>
			<div class="spacer"></div>
			<div class="blocResult mainContent">
				<div class="content">
					<textarea name="comment" style="width:100%; height:120px;"><?php if( $providerInvoice )echo htmlentities( $providerInvoice->get( "comment" ) ); ?></textarea>
				</div><!-- Fin .content -->
			</div>
			<div class="floatRight">
				<input id="DeleteButton" class="buttonValidRight buttonDelete blueButton" type="button" name="" value="Supprimer" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette facture?' ) ) deleteInvoice();" style="display:<?php echo $providerInvoice ? "inline" : "none"; ?>" />
				<input type="submit" name="SaveEstimate" value="Enregistrer" class="buttonValidRight buttonSave">
			</div>
		</div><!-- Fin .contentDyn -->
		<div class="spacer"></div>
	</div><!-- Fin .centerMax -->			
</form>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

/* --------------------------------------------------------------------------------------------- */
/* coordonnées/sélection prestataire */

function getProviderInfo( Provider $provider = NULL, $editable = true, ProviderInvoice $providerInvoice = NULL ){
	$providerInvoice = NULL;

if( isset( $_REQUEST[ "idprovider_invoice" ] ) && isset( $_REQUEST[ "idprovider" ] ) )
	$providerInvoice = new ProviderInvoice( $_REQUEST[ "idprovider_invoice" ], intval( $_REQUEST[ "idprovider" ] ) );

	?>
	<script type="text/javascript">

		/* --------------------------------------------------------------------------------------------- */
	
		var currentProvider = <?php echo $provider ? $provider->getId() : "null"; ?>;
		
		function selectProvider( idprovider ){

			if( idprovider == "0" ){

				$( "#ServiceControls" ).css( "display", "none" );
				return;

			}
			
			if( idprovider != currentProvider  )
				$( "#ProviderInvoiceItems tbody tr" ).remove();
			
			$.ajax({
			 	
				url: "<?php echo URLFactory::getHostURL(); ?>/accounting/provider/provider_invoice.php?provider_info&idprovider=" + idprovider,
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sélectionner le prestataire" ); },
			 	success: function( responseText ){
	
					$( "#ProviderInfo" ).html( responseText );
					
				}
	
			});

			$.ajax({
			 	
				url: "<?php echo URLFactory::getHostURL(); ?>/accounting/provider/provider_invoice.php?provider_services&idprovider=" + idprovider,
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer la liste des services du prestatire" ); },
			 	success: function( responseText ){
	
					$( "#ProviderServices" ).html( '<option value="0">-</option>' + responseText );
					$( "#ServiceControls" ).css( "display", "block" );
					
				}
	
			});

			selectPaymentMode(idprovider)

			currentProvider = idprovider;
			recalculate();
			
		}

		/* --------------------------------------------------------------------------------------------- */
		
	</script>
	<div class="blocMultiple blocMLeft">
		<h2>Coordonnées société</h2>
		<label><span>Raison sociale</span></label>
		<?php 
		
			if( !$editable && $provider ){
				
				?>
				<input type="text" class="textInput" value="<?php echo htmlentities( $provider->get( "name" ) ) ?>" readonly="readonly" />
				<input type="hidden" name="idprovider" id="idprovider" value="<?php echo $provider->getId(); ?>" />
				<?php
				
			}
			else XHTMLFactory::createSelectElement( "provider", "idprovider", "name", "name", $provider ? $provider->getId() : false, 0, "Sélectionnez un prestataire", "name=\"idprovider\" id=\"idprovider\" onchange=\"selectProvider(this.options[this.selectedIndex].value);\" style=\"width:auto;\"" ); 
			
		?>
		<div class="spacer"></div>
		
		<label><span>Adresse</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo htmlentities( $provider->get( "address" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>   
		
		<label><span>Adresse comp.</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo htmlentities( $provider->get( "address2" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>
		
		<label><span>Code postal</span></label>
		<input type="text" class="textInput" value="<?php if( $provider && $provider->get( "zipcode" ) ) echo htmlentities( $provider->get( "zipcode" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>
		
		<label><span>Ville</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo htmlentities( $provider->get( "city" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>
		
		<label><span>Pays</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo htmlentities( DBUtil::getDBValue( "name_1", "state", "idstate", $provider->get( "idstate" ) ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>
			
		
							
		
				
	</div>
	<div class="blocMultiple blocMRight">
		
		<h2>
		<?php
			
			if( !$editable && $provider ){
	                        
				?>
				<a style="margin-left:35px;" class="blueLink" href="/accounting/provider/provider.php?idprovider=<?php echo $provider->getId(); ?>" onclick="window.open(this.href); return false;">Accès fiche</a>
				<a style="margin-left:35px;" href="/accounting/provider/provider.php?idprovider=<?php echo $provider->getId(); ?>#services-provider" class="blueLink" onclick="window.open(this.href); return false;">Voir l'historique</a>
				<?php
	                        
			}
	
		?>&nbsp;
		</h2>
		<div class="spacer"></div>
		
		<label><span>Nom</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo htmlentities( DBUtil::getDBValue( "title_1", "title", "idtitle", $provider->getContact()->get( "idtitle" ) ) . " " . $provider->getContact()->get( "lastname" ) );  ?>" readonly="readonly" />
		<div class="spacer"></div>
		
		<label><span>Prénom</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo htmlentities( $provider->getContact()->get( "firstname" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>
				
		<label><span>Téléphone</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo Util::phonenumberFormat( $provider->getContact()->get( "phonenumber" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>

		<label><span>Fax</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo Util::phonenumberFormat( $provider->getContact()->get( "faxnumber" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>

		<label><span>Email</span></label>
		<a class="blueLink" href="mailto:<?php if( $provider ) echo $provider->getContact()->get( "email" ); ?>">
			<?php if( $provider ) echo $provider->getContact()->get( "email" ); ?>
		</a>
		<div class="spacer"></div>

		<label><span>GSM</span></label>
		<input type="text" class="textInput" value="<?php if( $provider ) echo Util::phonenumberFormat( $provider->getContact()->get( "gsm" ) ); ?>" readonly="readonly" />
		<div class="spacer"></div>

	</div>
	
	
	<hr />
	<font color="#FF6600" size="2">Nom de la société (si service Divers ...)</font>
		<input type="text" class="textInput" name="provider_specific" value="<?php if( $providerInvoice )echo htmlentities( $providerInvoice->get( "provider_specific" ) ); ?>"  />
		<div class="spacer"></div>
	
	
	<?php
			
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getServices( ProviderInvoice $providerInvoice = NULL ){

	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ---------------------------------------------------------------------------------------------------------- */
		
		function addItem( $select ){

			if( !$select.val() )
				return;

			$.ajax({
			 	
				url: "<?php echo URLFactory::getHostURL(); ?>/accounting/provider/provider_invoice.php?add=item",
				async: true,
				type: "GET",
				data: "idprovider=" + $( "#idprovider" ).val() + "&idservice=" + $select.val(),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter le service" ); },
			 	success: function( responseText ){
	
					$( "#ProviderInvoiceItems tbody" ).append( responseText );
					recalculate();
					
				}
	
			});

			$select.val( 0 );
			
		}

		/* ---------------------------------------------------------------------------------------------------------- */
		
		function recalculate(){

			var totalET = 0.0;
var totalATI = 0.0;
$( "input[name='ttcprice[]']" ).each( function ( i ){

				var ttcprice		= parseFloat( $( this ).val().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
				var quantity 	= parseInt( $( "input[name='quantity[]']" ).eq( i ).val() );
				var priceATI 	= ttcprice * quantity;
				
				$( this ).parent().siblings( ":last" ).html( priceATI.toFixed( 2 ) + " ¤" );

				totalATI += priceATI;
					
			} );
 $( "input[name='price[]']" ).each( function( i ){

				var price 		= parseFloat( $( this ).val().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
				var quantity 	= parseInt( $( "input[name='quantity[]']" ).eq( i ).val() );
				var priceET 	= price * quantity;
				
				$( this ).parent().siblings( ":last" ).html( priceET.toFixed( 2 ) + " ¤" );

				totalET += priceET;
					
			} );


			
			$( "input[name='TotalET']" ).val( totalET.toFixed( 2 ) );
			$( "input[name='TotalATI']" ).val( totalATI.toFixed( 2 ) );
			
			//$( "#TotalET" ).html( totalET.toFixed( 2 )  + " ¤" );
//$( "#TotalATI" ).html( totalATI.toFixed( 2 )  + " ¤" );
			var vat = 0.0;
			
			
if( totalATI > 0.0){
				vat = totalATI - totalET;
			//	$( "input[name='vat_amount']" ).val( vat.toFixed( 2 ) );
	$( "#vat_amount" ).html( vat.toFixed( 2 )  + " ¤" );
	}
	else {
	vat = 0.00;
	$( "#vat_amount" ).html( vat.toFixed( 2 )  + " ¤" );
	}
			
			//else vat = parseFloat( $( "input[name='vat_amount']" ).val().replace( new RegExp( " ", "g" ), "" ).replace( new RegExp( ",", "g" ), "." ) );
			
		//	var totalATI = totalET + vat;
			
		//	$( "#TotalATI" ).html( totalATI.toFixed( 2 ) + " ¤" );

			//var downPaymentAmount = parseFloat( $( "input[name='down_payment_amount']" ).val().replace( new RegExp( " ", "g" ), "" ).replace( new RegExp( ",", "g" ), "." ) );
			var rebateAmount = parseFloat( $( "input[name='rebate_amount']" ).val().replace( new RegExp( " ", "g" ), "" ).replace( new RegExp( ",", "g" ), "." ) );

			//var netBill = totalATI - downPaymentAmount - rebateAmount;
			var netBill = totalATI - rebateAmount;
			
			$( "#NetBill" ).html( netBill.toFixed( 2 ) + " ¤" );
			
		}
	function recalculatetotal(){

			var totalET = $( "input[name='TotalET']" ).val( );
var totalATI = $( "input[name='TotalATI']" ).val( );



			
		//	$( "input[name='TotalET']" ).val( totalET.toFixed( 2 ) );
		//	$( "input[name='TotalATI']" ).val( totalATI.toFixed( 2 ) );
			
			//$( "#TotalET" ).html( totalET.toFixed( 2 )  + " ¤" );
//$( "#TotalATI" ).html( totalATI.toFixed( 2 )  + " ¤" );
			var vat = 0.0;
			
			
if( totalATI > 0.0){
				vat = totalATI - totalET;
			//	$( "input[name='vat_amount']" ).val( vat.toFixed( 2 ) );
	$( "#vat_amount" ).html( vat.toFixed( 2 )  + " ¤" );
	}
	else {
	vat = 0.00;
	$( "#vat_amount" ).html( vat.toFixed( 2 )  + " ¤" );
	}
			
			//else vat = parseFloat( $( "input[name='vat_amount']" ).val().replace( new RegExp( " ", "g" ), "" ).replace( new RegExp( ",", "g" ), "." ) );
			
		//	var totalATI = totalET + vat;
			
		//	$( "#TotalATI" ).html( totalATI.toFixed( 2 ) + " ¤" );

			//var downPaymentAmount = parseFloat( $( "input[name='down_payment_amount']" ).val().replace( new RegExp( " ", "g" ), "" ).replace( new RegExp( ",", "g" ), "." ) );
			var rebateAmount = parseFloat( $( "input[name='rebate_amount']" ).val().replace( new RegExp( " ", "g" ), "" ).replace( new RegExp( ",", "g" ), "." ) );

			//var netBill = totalATI - downPaymentAmount - rebateAmount;
			var netBill = totalATI - rebateAmount;
			
			$( "#NetBill" ).html( netBill.toFixed( 2 ) + " ¤" );
			
		}
		/* ---------------------------------------------------------------------------------------------------------- */
		
	/* ]]> */	
	</script>
	<div class="tableContainer">
		<table id="ProviderInvoiceItems" class="dataTable devisTable">
			<thead>
			<tr class="filledRow">
					<th style="width:20%;">Référence</th>
					<th style="width:20%;">Désignation</th>
					<th style="width:10%;">P.U. H.T. Net</th>
                    <th style="width:10%;">P.U. T.T.C. Net</th>
					<th style="width:20%;">Quantité</th>
                     <th style="width:10%;">Taux TVA (%)</th>
					<th style="width:10%;">Montant HT</th>
                    
				</tr>
			</thead>
			<tbody>
			<?php
			
				if( $providerInvoice ){
			
					$it = $providerInvoice->getItems()->iterator();
					while( $it->hasNext() ){
			
						$item =& $it->next();
						
						?>
						<tr>
							<td>
								<input type="hidden" name="idservice[]" value="<?php echo $item->get( "idservice" ); ?>" />
								<?php echo htmlentities( DBUtil::getDBValue( "label", "service", "idservice", "vat_rate", $item->get( "idservice" ) ) ); ?>
								<a href="#" onclick="$( this ).parents( 'tr' ).remove(); recalculate(); return false;" style="float:left;">
									<img src="/images/back_office/content/corbeille.jpg" alt="" />
								</a>
							</td>
						<td><input type="text" name="label[]" class="textInput" value="<?php echo htmlentities( $item->get( "label" ) ); ?>" style="width:100%;" /></td>
							<td style="text-align:right;"><input type="text" name="price[]" class="textInput price" value="<?php echo Util::priceFormat( $item->get( "price" ) ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
                            <td style="text-align:right;"><input type="text" name="ttcprice[]" class="textInput price" value="<?php echo Util::priceFormat( $item->get( "ttcprice" ) ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
							<td style="text-align:right;"><input type="text" name="quantity[]" class="textInput price" value="<?php echo $item->get( "quantity" ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
                             <td style="text-align:right;"><input type="text" name="vat_rate[]" class="textInput price" value="<?php echo $item->get( "vat_rate" ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( round( $item->get( "quantity" ) * $item->get( "price" ), 2 ) ); ?></td>
                       
						</tr>
						<?php						
							
					}
					
				}
	
			?>
			</tbody>
		</table>
	</div>
	<div class="tableContainer">
		<table class="dataTable devisTable" style="margin-top:15px; width:40%; float:right;">
			<tbody>
				<tr>
					<th>Total HT</th>
					<td style="text-align:right; font-weight:bold;" >
                  <input type="text" id="TotalET" name="TotalET" class="textInput price" value="<?php echo Util::priceFormat( $providerInvoice ? $providerInvoice->getTotalET() : 0.0 ); ?>" onchange="recalculatetotal();" onkeyup="this.onchange(event);" /> 
					
					</td>
				</tr>
				<tr>
					<th>
						TVA
						
					</th>
					<td style="text-align:right; font-weight:bold;" id="vat_amount">
                    
						<?php echo number_format( $providerInvoice ? $providerInvoice->getTotalATI() - $providerInvoice->getTotalET() : 0.0, 2, ",", " " ); ?> ¤
					</td>
				</tr>
				<tr>
					<th>Total TTC</th>
					<td  style="text-align:right; font-weight:bold;">
                      <input type="text" id="TotalATI" name="TotalATI" class="textInput price" value="<?php echo Util::priceFormat( $providerInvoice ? $providerInvoice->getTotalATI() : 0.0 ); ?>" onchange="recalculatetotal();" onkeyup="this.onchange(event);" />
						
					</td>
				</tr>
				<tr>
					<th>Facture document</th>
					<td>
						<input type="file" class="textInput" name="provider_invoice_doc" />
						<?php 
						if( $providerInvoice && $providerInvoice->get( 'provider_invoice_doc' ) ){
							if( !file_exists( DOCUMENT_ROOT . "/data/provider_invoice/" . $providerInvoice->get( 'provider_invoice_doc' ) ) )
								echo "<br /><span style='font-weight:bold;color:#ff0000;'>Fichier inexistant</span>";
							else
								echo "<br /><span style='font-weight:bold;color:#000;'><a href='" . HTTP_HOST . "/data/provider_invoice/" . $providerInvoice->get( 'provider_invoice_doc' ) . "' style='font-weight:bold;color:#000;' onclick='window.open(this.href);return false;'>Lien vers le fichier</a></span>";
						}
						?>
					</td>
				</tr>				
			</tbody>
		</table>
	</div>
	<br style="clear:both;" />
	<?php

}

/* --------------------------------------------------------------------------------------------- */

function listProviderServices( $idprovider ){

	$rs =& DBUtil::query( "SELECT idservice, label FROM service WHERE idprovider = '" . intval( $idprovider ) . "' ORDER BY label ASC" );

	while( !$rs->EOF() ){

		?>
		<option value="<?php echo $rs->fields( "idservice" ); ?>"><?php echo htmlentities( $rs->fields( "label" ) ); ?></option>
		<?php

		$rs->MoveNext();

	}

}

/* --------------------------------------------------------------------------------------------- */

function save(){
	
	ob_start();
	
	$_POST =& Util::arrayMapRecursive( "Util::doNothing", Util::arrayMapRecursive( "stripslashes", $_POST ) );

	if( !strlen( $_POST[ "idprovider_invoice" ] ) ){
		
		echo "Vous devez renseigner un numéro de facture";
		return;
			
	}
	
	if( !intval( $_POST[ "idprovider" ] ) ){
		
		echo "Vous devez sélectionner un prestataire de service";
		return;
			
	}
	$vat_amount =  $_POST[ "TotalATI" ] - $_POST[ "TotalET" ] ;
	if( DBUtil::query( "SELECT 1 FROM provider_invoice WHERE idprovider_invoice = " . DBUtil::quote( $_POST[ "idprovider_invoice" ] ) . " AND idprovider = '" . intval( $_POST[ "idprovider" ] ) . "'" )->RecordCount() )
			$providerInvoice = new ProviderInvoice( $_POST[ "idprovider_invoice" ], $_POST[ "idprovider" ] );
	else 	$providerInvoice = ProviderInvoice::create( $_POST[ "idprovider_invoice" ], intval( $_POST[ "idprovider" ] ),$_POST[ "TotalET" ],$_POST[ "TotalATI" ],$vat_amount );
	
	$providerInvoice->set( "invoice_date", 		Util::dateFormatUs( $_POST[ "invoice_date" ] ) );
	$providerInvoice->set( "deliv_payment", 	Util::dateFormatUs( $_POST[ "deliv_payment" ] ) );
	$providerInvoice->set( "vat_amount", 		$vat_amount );
	$providerInvoice->set( "idpayment", 		Util::text2num( $_POST[ "idpayment" ] ) );
	$providerInvoice->set( "idpayment_delay", 	Util::text2num( $_POST[ "idpayment_delay" ] ) );
	$providerInvoice->set( "rebate_amount", 	Util::text2num( $_POST[ "rebate_amount" ] ) );
	$providerInvoice->set( "provider_specific",  htmlentities($_POST[ "provider_specific" ] ));
	$providerInvoice->set( "comment", 			$_POST[ "comment" ] );
	$providerInvoice->set( "total_amount_ht", 			$_POST[ "TotalET" ] );
	$providerInvoice->set( "total_amount", 			$_POST[ "TotalATI" ] );
	
	/* services facturés */
	
	$providerInvoice->removeItems();
	
	$i = 0;
	if( isset( $_POST[ "idservice" ] ) )
		while( $i < count( $_POST[ "idservice" ] ) ){
	
			$item = $providerInvoice->addService( intval( $_POST[ "idservice" ][ $i ] ) );
			$item->set( "vat_rate",  Util::text2num($_POST[ "vat_rate" ][ $i ])  );
			$item->set( "label", 	$_POST[ "label" ][ $i ] );
			$item->set( "price", 	Util::text2num( $_POST[ "price" ][ $i ] ) );
			$item->set( "quantity", intval( $_POST[ "quantity" ][ $i ] ) );
	

	$query = "
	update service SET  vat_rate = ' ". Util::text2num($_POST[ "vat_rate" ][ $i ]) ."'
	WHERE 
	idservice =  '" .  intval( $_POST[ "idservice" ][ $i ] ) . "'
		
	";
	
	DBUtil::query( $query );
			$i++;
			
		}
	/* services taux tva enregistrer */
	
	
	//PJs
	if( isset( $_FILES[ "provider_invoice_doc" ] ) && is_uploaded_file( $_FILES[ "provider_invoice_doc" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "provider_invoice_doc";
		$idsupplier = $_POST[ "idprovider" ];
		
		$target_dir = DOCUMENT_ROOT . "/data/provider_invoice/$YearFile/$idsupplier/";
		$prefixe = "Fact";
		$_FILES[$fieldname]['name'] = stripslashes( $_FILES[$fieldname]['name'] );
		$_FILES[$fieldname]['name'] = strtolower($_FILES[$fieldname]['name']); // on passe la chaine de caractère en minuscule
$_FILES[$fieldname]['name'] = strtr($_FILES[$fieldname]['name'], "àäåâôöîïûüéè", "aaaaooiiuuee"); // on remplace les accents
$_FILES[$fieldname]['name'] = str_replace(' ', '-', $_FILES[$fieldname]['name']); // on remplace les espaces par des tirets
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if( ( $extension != '.doc' ) && ( $extension != '.jpg' ) && ( $extension != '.xls' ) && ( $extension != '.pdf' ) && ( $extension != '.DOC' ) && ( $extension != '.JPG' ) && ( $extension != '.XLS' ) && ( $extension != '.PDF' ) ){
   			exit( "Format de fichier invalide" );
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir . $prefixe . "_" . $DateFile . "_" . $name_file;
		$newdoc = $YearFile . "/" . $idsupplier . "/" . $prefixe . "_" . $DateFile . "_" . $name_file;
	
		//billing_supplier existe
		//rep année	
		
		if( !file_exists( DOCUMENT_ROOT . '/data/provider_invoice/' . $YearFile ) )
			if( mkdir( DOCUMENT_ROOT . '/data/provider_invoice/' . $YearFile ) === false )
				exit( "Impossible de créer le répertoire '/provider_invoice/$YearFile'" );
			
		if( chmod( DOCUMENT_ROOT . '/data/provider_invoice/' . $YearFile , 0755 ) === false )
			exit( "Impossible de modifier les droits du répertoire '/provider_invoice/$YearFile'" );	
			
		if( !file_exists( DOCUMENT_ROOT . '/data/provider_invoice/' . $YearFile . '/' . $idsupplier ) )
			if( mkdir( DOCUMENT_ROOT . '/data/provider_invoice/' . $YearFile . '/' . $idsupplier ) === false )
				exit( "Impossible de créer le répertoire '/provider_invoice/$YearFile/$idsupplier'" );
			
		if( chmod( DOCUMENT_ROOT . '/data/provider_invoice/' . $YearFile . '/' . $idsupplier , 0755 ) === false )
			exit( "Impossible de modifier les droits du répertoire '/provider_invoice/$YearFile/$idsupplier'" );	
    	
    	if( !move_uploaded_file( $tmp_file , $dest ) )
        	exit( "Impossible de déplacer le fichier uploadé vers sa destination $dest" );
	
		if( chmod( $dest , 0755 ) === false )
			exit( "Impossible de modifier les droits du fichier $dest" );	

		$providerInvoice->set( $fieldname , $newdoc );
	}		
		
	$providerInvoice->save();
	
	$data = ob_get_contents();
	
	ob_end_clean();
	
	exit( empty( $data ) ? "1" : $data );
	
}

/* --------------------------------------------------------------------------------------------- */

function listProviderInvoiceItems( ProviderInvoice $providerInvoice ){
	
	$it = $providerInvoice->getItems()->iterator();
	
	while( $it->hasNext() ){

		$item =& $it->next();
		
		?>
		<tr>
			<td>
				<input type="hidden" name="idservice[]" value="<?php echo $item->get( "idservice" ); ?>" />
				<?php echo htmlentities( DBUtil::getDBValue( "label", "service", "idservice", $item->get( "idservice" ) ) ); ?>
				<a href="#" onclick="$( this ).parents( 'tr' ).remove(); recalculate(); return false;" style="float:left;">
					<img src="/images/back_office/content/corbeille.jpg" alt="" />
				</a>
			</td>
			<td><input type="text" name="label[]" class="textInput" value="<?php echo htmlentities( $item->get( "label" ) ); ?>" /></td>
			<td style="text-align:right;"><input type="text" name="price[]" class="textInput price" value="<?php echo Util::priceFormat( $item->get( "price" ) ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
             <td style="text-align:right;"><input type="text" name="pricettc[]" class="textInput price" value="<?php echo Util::priceFormat( $item->get( "pricettc" ) ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
			<td style="text-align:right;"><input type="text" name="quantity[]" class="textInput price" value="<?php echo $item->get( "quantity" ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
             <td style="text-align:right;"><input type="text" name="vat_rate[]" class="textInput price" value="<?php echo $item->get( "vat_rate" ); ?>" onchange="recalculate();" onkeyup="this.onchange(event);" /></td>
			<td style="text-align:right;"><?php echo Util::priceFormat( round( $item->get( "quantity" ) * $item->get( "price" ), 2 ) ); ?></td>
		</tr>
		<?php						
			
	}
						
}

/* --------------------------------------------------------------------------------------------- */

function listAccounts( $idparent ){
	
	$query = "
	SELECT ap1.idaccounting_plan, ap1.accounting_plan_1, GROUP_CONCAT( ap2.idaccounting_plan ) AS children
	FROM accounting_plan ap1 
	LEFT JOIN accounting_plan ap2
	ON ap2.accounting_plan_parent = ap1.idaccounting_plan
	WHERE ap1.accounting_plan_parent = '$idparent' AND ap2.accounting_plan_parent <> '0'
	GROUP BY ap1.idaccounting_plan
	ORDER BY ap1.idaccounting_plan ASC";

	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		?>
		<li>
			<span>
				<input type="hidden" name="idaccounting_plan" value="<?php echo htmlentities( $rs->fields( "idaccounting_plan" ) ); ?>" />
				<input type="hidden" name="accounting_plan_1" value="<?php echo htmlentities( $rs->fields( "accounting_plan_1" ) ); ?>" />
				<?php echo htmlentities( $rs->fields( "idaccounting_plan" ) . " : " . $rs->fields( "accounting_plan_1" ) ); ?>
			</span>
			<?php
			
				if( $rs->fields( "children" ) != NULL ){
					
					echo "<ul>"; 
					listAccounts( $rs->fields( "idaccounting_plan" ) ); 
					echo "</ul>";
					
				}

		?>
		</li>
		<?php
		
		$rs->MoveNext();
		
	}

}

/* --------------------------------------------------------------------------------------------- */

?>