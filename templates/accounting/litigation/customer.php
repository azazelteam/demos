<?php 

include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" ); 

?>
<script type="text/javascript">
/* <![CDATA[ */

	function seeBuyerHisto(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-1010)/2;
		window.open( '<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $litigation->getCustomer()->get( "idbuyer" ) ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0' );

	}

/* ]]> */
</script>
<div class="mainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    <div class="topRight"></div>
    <div class="topLeft"></div>
	<div class="content" style="margin-bottom:25px;">
		<div class="headTitle">
			<?php 
				$onWhat = "";
				switch ( $litigation->getType() ){
					case Litigation::$TYPE_BILLING_BUYER :
						$onWhat = "facture";
						break;
					
					case Litigation::$TYPE_ORDER :
						$onWhat = "commande";
						break;
					default:
						$onWhat = "rien du tout";
						break;
				}
			?>
			<p class="title">N° Litige sur <?php echo $onWhat; ?> : <?php echo $litigation->getIdLitigation(); ?></p>
			<div class="rightContainer date"><?php echo humanReadableDatetime( $litigation->getCreationDate( "creation_date" ) ); ?></div>
  			<div class="clear"></div>      	
		</div><!-- headTitle -->
        <div class="subHeadTitle">
            <div class="subHeadTitleElementRight">
                <b>Commercial Affaire</b>
            	<?php 
            	
	            	/*commercial affaire*/
	            	
            		if( $litigation->isEditable() && User::getInstance()->get("admin") ){
            			
						include_once( dirname( __FILE__ ) . "/../../../script/user_list.php" );
						userList( false, $litigation->getSalesman()->get( "iduser" ) );
	
	            		?>
	            		<input type="image" name="" id="" alt="Modifier" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" style="vertical-align:middle; margin-left:5px;" />
	            		<?php
	            		
            		}
	            	else echo htmlentities( DBUtil::getDBValue( "firstname", "user", "iduser", $litigation->getSalesman()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $litigation->getCustomer()->get( "iduser" ) ) );
	            
	        ?>
            </div><!-- subHeadTitleElementRight -->
            <p class="subHeadTitleElementLeft" style="margin-top:5px;">
            	<b>Commercial client : </b><?php echo DBUtil::getDBValue( "firstname", "user", "iduser", $litigation->getCustomer()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $litigation->getCustomer()->get( "iduser" ) ); ?>
            </p>
            <div class="clear" style="margin-top:5px;">
            	STATUT DU LITIGE : <?php echo $litigation->isEditable() ? "En cours" : "Résolu"; ?>
			</div>
			<div class="clear" style="margin-top:5px; margin-bottom:30px; margin-bottom:30px;">
				<?php
				switch ( $litigation->getType() ){
					case Litigation::$TYPE_BILLING_BUYER :
						?>
						N° FACTURE LITIGIEUSE : <a class="blueLink" href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $litigation->getIdInvoice() ?>" onclick="window.open( this.href ); return false;">
							<?php echo $litigation->getIdInvoice() ?>
						</a>
						<?php 
						break;
					
					case Litigation::$TYPE_ORDER :
						?>
						N° COMMANDE LITIGIEUSE : <a class="blueLink" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $litigation->getIdOrder() ?>" onclick="window.open( this.href ); return false;">
							<?php echo $litigation->getIdOrder() ?>
						</a>
						<?php 
						break;
					default:
						echo "rien du tout";
						break;
				}
				?>			
				
			</div>
    	</div><!-- subHeadTitle -->
        <div class="subContent">
       		<!-- Coordonnées société -->
        	<div class="subTitleContainer">
                <p class="subTitle">Coordonnées société</p>
			</div>
			<div class="tableContainer">
		    	<table class="dataTable" style="margin-top:10px;">
		            <tr class="filledRow">
		                <th>N° client</th>
		                <td><?php echo $litigation->getCustomer()->get( "idbuyer" );  ?></td>
		                <td>
		                	<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?type=buyer&amp;key=<?php echo $litigation->getCustomer()->get( "idbuyer" );  ?>" target="_blank" >Accès fiche client</a>
		                </td>
		                <td>
		                	<a href="#" class="blueLink" onclick="seeBuyerHisto(); return false;">Voir l'historique du client</a>
		                </td>
		            </tr>
		            <tr>
		                <th>Nom / Raison sociale</th>
		                <td><?php echo htmlentities( $litigation->getCustomer()->get( "company" ) ) ?></td>
		                <th>Provenance</th>
		                <td><?php echo htmlentities( DBUtil::getDBValue( "source" . User::getInstance()->getLang(), "source", "idsource", $litigation->getCustomer()->get( "idsource" ) ) ) ?></td>
		            </tr>
		             <tr>
		                <th>Adresse</th>
		                <td colspan="3"><?php echo htmlentities( $litigation->getCustomer()->get( "adress" ) ) ?></td>
		            </tr>
		            <tr>
		                <th>Adresse complémentaire</th>
		      			<td colspan="3"><?php echo htmlentities( $litigation->getCustomer()->get( "adress_2" ) ) ?></td>
		            </tr>
		            <tr>
		                <th>Code postal</th>
		                <td><?php echo htmlentities( $litigation->getCustomer()->get( "zipcode" ) ) ?></td>
		                <th>Ville</th>
		                <td><?php echo htmlentities( $litigation->getCustomer()->get( "city" ) ) ?></td>
		            </tr>
		            <tr>
		            	<th>Pays</th>
		                <td><?php echo htmlentities( DBUtil::getDBValue( "name" . User::getInstance()->getLang(), "state", "idstate", $litigation->getCustomer()->get( "idstate" ) ) ) ?></td>
		                <td colspan="2"></td>
		            </tr>
		            <tr class="filledRow">
		                <th>Nom</th>
		                <td><?php echo DBUtil::getDBValue( "title" . User::getInstance()->getLang(), "title", "idtitle", $litigation->getCustomer()->getContact()->get( "title" ) ) . " " . htmlentities( $litigation->getCustomer()->getContact()->get( "lastname" ) ) ?></td>
		                <th>Prénom</th>
		                <td><?php echo htmlentities( $litigation->getCustomer()->getContact()->get( "firstname" ) ) ?></td>
		            </tr>
		            <tr>
		                <th>Téléphone</th>
		                <td><?php echo htmlentities( Util::phonenumberFormat( $litigation->getCustomer()->getContact()->get( "phonenumber" ) ) ) ?></td>
		                <th>Fax</th>
		                <td><?php echo htmlentities( Util::phonenumberFormat( $litigation->getCustomer()->getContact()->get( "faxnumber" ) ) ) ?></td>
		            </tr>
		            <tr>
		                <th>Email</th>
		                <td>
		                	<a class="blueLink" href="#" onclick="mailto:<?php echo htmlentities( $litigation->getCustomer()->getContact()->get( "mail" ) ) ?>; return false;">
		                		<?php echo htmlentities( $litigation->getCustomer()->getContact()->get( "mail" ) ) ?>
		                	</a>
		                </td>
		                <th>GSM</th>
		                <td><?=htmlentities( $litigation->getCustomer()->getContact()->get( "gsm" ) ) ?></td>
		            </tr>
		           <!-- <tr>
		                <th>N° Factor</th>
		                <td><?php echo htmlentities( $litigation->getCustomer()->get( "factor" ) ) ?></td>
		                <th>Couverture</th>
		                <td><?php echo Util::priceFormat( getEncours( $litigation->getCustomer()->get( "idbuyer" ) ) ); ?></td>
		            </tr>
		            <tr>
			            <?php
			            
				             switch( $litigation->getCustomer()->get( "factor_status" ) ){
				             	
				             	case "Accepted" :
				             	
									$th = Dictionnary::translate( "gest_com_factor_guarantee" );
									$td = Util::priceFormat( $litigation->getCustomer()->get( "factor_amount" ) );
									
									break;
							
								case "Refused" :
								
									$th = "Date de refus";
									$td = usDate2eu( $litigation->getCustomer()->get( "factor_refusal_date" ) );

									break;
									
								default :
								
									$th = "&nbsp;";
									$td = "&nbsp;";
							
							}
						
						?>
						<th><?php echo $th ?></th>
		                <td><?php echo $td ?></td>
		                <td colspan="2"></td>
		            </tr>-->
		        </table>
		    </div><!-- tableContainer -->
		</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php
/**
 * @todo règlements + avoirs + escomptes + etc...
 * @param int $idbuyer
 */
function getEncours( $IdBuyer ){
	
	include_once( dirname( __FILE__ ) . "/../../../objects/Order.php" );
	// ------- Mise à jour de l'id gestion des Index  #1161
	$query = "
	SELECT total_amount, down_payment_type, down_payment_value 
	FROM `order` 
	WHERE idbuyer = '$IdBuyer'
	AND ( status='" . Order::$STATUS_ORDERED . "' OR status='" . Order::$STATUS_INVOICED . "' )";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer l'encours du client" );
	
	$encours = 0;
	
	for( $i = 0 ; $i < $rs->Recordcount() ; $i++ ){
		
		if( $rs->fields( "down_payment_type" ) == "rate" )
			$down_payment_amount = round( $rs->fields( "total_amount" ) * $rs->fields( "down_payment_value" ) / 100, 2 );
		else
			$down_payment_amount = $rs->fields( "down_payment_value" );
		
		$encours = $encours + $rs->fields( "total_amount" ) - $down_payment_amount;
		
		$rs->MoveNext();
		
	}
	
	return $encours;
			
}

?>