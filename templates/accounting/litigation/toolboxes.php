<?php

/**
 * Boîtes à outils
 */
 
/* mode ajax */

if( !headers_sent() && isset( $_GET[ "idlitigation" ] ) && isset( $_GET[ "ajax" ] ) ){
	
	include_once( "../../../config/init.php" );
	include_once( "$GLOBAL_START_PATH/objects/Litigation.php" );
	
	header( "Content-Type: text/html; charset=utf-8" );
	$litigation = new Litigation(  $_GET[ "idlitigation" ] );
	
}

?>
<div id="tools" class="rightTools">
    <div class="toolBox">
    	<div class="header">
        	<div class="topRight"></div>
        	<div class="topLeft"></div>
            <p class="title">Visualiser et imprimer</p>
        </div>
        <div class="content" style="padding:5px;">
		<?php
		
			listEmailDocuments( $litigation );
			listPDFDocuments( $litigation );
	
		?>
        </div>
        <div class="footer">
        	<div class="bottomLeft"></div>
            <div class="bottomMiddle"></div>
            <div class="bottomRight"></div>
        </div>
    </div><!-- toolBox -->
	<?php
		
		$customerGrouping = DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $litigation->getCustomer()->get( "idbuyer" ) );
		
		if( $customerGrouping ){
			
			include_once( "$GLOBAL_START_PATH/objects/GroupingCatalog.php" );
			
			$grouping = new GroupingCatalog();
			
			//@todo: Bidouille qui consiste à changer l'idgrouping du groupement à la volée pour charger les infos
			// Le problème vient du fait qu'on ne peut instancier un groupement que depuis le Front-Office
			$grouping->getGroupingInfos( $customerGrouping );
			
			$groupingName = $grouping->getGrouping( "grouping" );
			$groupingLogo = $grouping->getGrouping( "grouping_logo" );
								
			?>
			<div class="toolBox">
				<div class="header">
					<div class="topRight"></div><div class="topLeft"></div>
					<p class="title">Groupement</p>
				</div>
				<div class="content">
					<div class="subTitleContainer" style="margin-top:0px;">
						<p class="subTitle"><?php echo $groupingName ?></p>
					</div>
					<div style="margin-top:10px; text-align:center;">
						<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $groupingLogo ?>" />
					</div>
				</div>
				<div class="footer">
					<div class="bottomLeft"></div>
					<div class="bottomMiddle"></div>
					<div class="bottomRight"></div>
				</div>
			</div><!-- toolBox -->
			<?php 
			
		} 
		
?>
</div><!-- tools -->
<?php

//---------------------------------------------------------------------------------------

function listEmailDocuments( &$litigation ){

	global $GLOBAL_START_URL;
	
	/* client sans adresse email */
	
	?>
	<p style="margin:0px; padding:0px;"><b>PAR MAIL</b></p>
	<?php
	
	if( !strlen( $litigation->getCustomer()->getContact()->get( "mail" ) ) ){
						
		?>
		<div class="subTitleContainer">
			<p class="subTitleWarning">
				Attention ce client ne possède pas d'adresse email, les fonctions d'envoi de commande par email sont par conséquent désactivées
			</p>
		</div>
		<?php
		
		return;
		
	}

	/* ouverture */
	
	?>
	<p style="margin:5px 0px 0px 0px; padding:0px;">
		<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation_mail.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&amp;mail=creation&amp;typeLitigation=<?php echo $litigation->getType()?>">
			Courriel de prise en charge
		</a>
	</p>
	<?php
		
	/* accord de retour */

	$location = $litigation->getGoodsDeliveryLocation();
	
	if( $location != Litigation::$GOODS_DELIVERY_LOCATION_NONE ){
		
		?>
		<p style="margin:5px 0px 0px 0px; padding:0px;">
			<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation_mail.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&amp;mail=return_to_sender&amp;typeLitigation=<?php echo $litigation->getType()?>">
				Accord de retour
			</a>
		</p>
		<?php
		
	}
	
	/* accusé de réception du retour de marchandise */

	if( $litigation->getRepaymentCount()  ){
		
		$lang = User::getInstance()->getLang();
	
		$query = "
		SELECT rp.idrepayment,
			rp.idpayment,
			rp.amount,
			p.name$lang AS `mode`,
			SUBSTR( payment_date, 1, 10 ) AS payment_date
		FROM credits c, repayments rp
		LEFT JOIN payment p
		ON rp.idpayment = p.idpayment
		WHERE c.idlitigation = '" . $litigation->getIdLitigation() . "'
		AND c.idcredit = rp.idcredit
		AND rp.payment_date <> '0000-00-00 00:00:00'
		ORDER BY rp.creation_date DESC";
	
		$rs =& DBUtil::query( $query );
	
		while( !$rs->EOF() ){
			
			?>
			<p style="margin:5px 0px 0px 0px; padding:0px;">
				<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation_mail.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&amp;mail=repayment&amp;idrepayment=<?php echo $rs->fields( "idrepayment" ); ?>&amp;typeLitigation=<?php echo $litigation->getType()?>">
					Remboursement du <?php echo Util::dateFormatEu( $rs->fields( "payment_date" ) ); ?>
					<br />( <?php echo htmlentities( $rs->fields( "mode" ) ) . ", " . Util::priceFormat( $rs->fields( "amount" ) ); ?> )
				</a>
			</p>
			<?php
			
			$rs->MoveNext();
			
		}
		
	}
	
	/* remboursement */

	if( $location != Litigation::$GOODS_DELIVERY_LOCATION_NONE && $litigation->getGoodsHasBeenDelivered() ){
		
		?>
		<p style="margin:5px 0px 0px 0px; padding:0px;">
			<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation_mail.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&amp;mail=return_to_sender_confirmation&amp;typeLitigation=<?php echo $litigation->getType()?>">
				Accusé de réception du retour de marchandise
			</a>
		</p>
		<?php
		
	}
	
	/* facture des frais de dossier */
	
	if( $litigation->getManagementFeesIdInvoice() ){
		
		?>
		<p style="margin:5px 0px 0px 0px; padding:0px;">
			<a href="<?php echo $GLOBAL_START_URL ?>/accounting/transmit_invoice.php?IdInvoice=<?php echo $litigation->getManagementFeesIdInvoice() ?>">
				Facture des frais de dossier
			</a>
		</p>
		<?php
		
	}
	
}

//---------------------------------------------------------------------------------------

function listPDFDocuments( &$litigation ){

	global $GLOBAL_START_URL;
	
	?>
	<p style="margin:10px 0px 0px 0px; padding:0px;"><b>VISUALISER PDF</b></p>
	<p style="margin:5px 0px 0px 0px; padding:0px;">
		<?php
		switch ( $litigation->getType() ){
			case Litigation::$TYPE_BILLING_BUYER :
				?>
				<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $litigation->getIdInvoice() ?>" onclick="window.open( this.href ); return false;">Facture litigieuse N° <?php echo $litigation->getIdInvoice() ?></a>
				<?php 
				break;
			
			case Litigation::$TYPE_ORDER :
				?>
				<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $litigation->getIdOrder() ?>" onclick="window.open( this.href ); return false;">Commande litigieuse N° <?php echo $litigation->getIdOrder() ?></a>
				<?php 
				break;
			default:
				echo "rien du tout";
				break;
		}
		?>		
	</p>
	<?php
	
	/* facture des frais de dossier */
	
	if( $litigation->getManagementFeesIdInvoice() ){
		
		?>
		<p style="margin:5px 0px 0px 0px; padding:0px;">
			<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $litigation->getManagementFeesIdInvoice() ?>" onclick="window.open( this.href ); return false;">Facture frais de dossier N° <?php echo $litigation->getManagementFeesIdInvoice() ?></a>
		</p>
		<?php
	
	}
	
	/* avoirs */
	
	$query = "
	SELECT c.idcredit,
		c.idbilling_buyer,
		c.editable,
		ex.idexchange
	FROM credits c
	LEFT JOIN exchanges ex
	ON c.idcredit = ex.idcredit
	WHERE c.idlitigation = '" . $litigation->getIdLitigation() . "'
	ORDER BY c.idcredit ASC";

	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){
		
		?>
		<p style="margin:5px 0px 5px 0px; padding:0px;"><u>Avoirs</u></p>
		<?php
		
	}
		
	while( !$rs->EOF() ){
		
		?>
		<div style="margin-left:5px;"><a href="/catalog/pdf_credit.php?idcredit=<?php echo $rs->fields( "idcredit" ) ?>" onclick="window.open( this.href ); return false;">Avoir N° <?php echo $rs->fields( "idcredit" ) ?></a></div>
		<?php
		
		$rs->MoveNext();	
		
	}
	
	/* commandes d'échange */
	
	$query = "
	SELECT o.idorder
	FROM exchanges ex, credits c, `order` o
	WHERE ex.idcredit = c.idcredit
	AND c.idlitigation = '" . $litigation->getIdLitigation() . "' 
	AND ex.idorder = o.idorder
	ORDER BY o.idorder ASC";

	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){
		
		?>
		<p style="margin:5px 0px 5px 0px; padding:0px;"><u>Commandes d'échange</u></p>
		<?php
		
	}
		
	while( !$rs->EOF() ){
		
		?>
		<div style="margin-left:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $rs->fields( "idorder" ) ?>" onclick="window.open( this.href ); return false;">Commande Client N° <?php echo $rs->fields( "idorder" ) ?></a></div>
		<?php
		
		$rs->MoveNext();	
		
	}
	
	/* BL de retour */

	$location = $litigation->getGoodsDeliveryLocation();
	
	if( $location != Litigation::$GOODS_DELIVERY_LOCATION_NONE ){
		
		?>
		<p style="margin:5px 0px 5px 0px; padding:0px;"><u>BL de retour</u></p>
		<?php
		
	}
	
	switch( $location ){
		
		case Litigation::$GOODS_DELIVERY_LOCATION_LOCAL :
		
			?>
			<div style="margin-left:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?output_delivery_note&amp;idorder_supplier=<?php echo $litigation->getReturnToSender()->get( "idorder_supplier" ) ?>&amp;idrow= <?php echo $litigation->getReturnToSender()->get( "idrow" ) ?>&location=<?php echo Litigation::$GOODS_DELIVERY_LOCATION_LOCAL ?>" onclick="window.open( this.href ); return false;">BL de retour en interne</a></div>
			<?php
		
			break;

		case Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER :
		
			?>
			<div style="margin-left:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?output_delivery_note&amp;idorder_supplier=<?php echo $litigation->getReturnToSender()->get( "idorder_supplier" ) ?>&amp;idrow= <?php echo $litigation->getReturnToSender()->get( "idrow" ) ?>&location=<?php echo Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER ?>" onclick="window.open( this.href ); return false;">BL de retour chez le fournisseur</a></div>
			<?php
			
			break;
		
		case Litigation::$GOODS_DELIVERY_LOCATION_BOTH :
		
			?>
			<div style="margin-left:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?output_delivery_note&amp;idorder_supplier=<?php echo $litigation->getReturnToSender()->get( "idorder_supplier" ) ?>&amp;idrow= <?php echo $litigation->getReturnToSender()->get( "idrow" ) ?>&location=<?php echo Litigation::$GOODS_DELIVERY_LOCATION_LOCAL ?>" onclick="window.open( this.href ); return false;">BL de retour en interne</a></div>
			<div style="margin-left:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?output_delivery_note&amp;idorder_supplier=<?php echo $litigation->getReturnToSender()->get( "idorder_supplier" ) ?>&amp;idrow= <?php echo $litigation->getReturnToSender()->get( "idrow" ) ?>&location=<?php echo Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER ?>" onclick="window.open( this.href ); return false;">BL de retour chez le fournisseur</a></div>
			<?php
		
			break;	
		
	}

}

//---------------------------------------------------------------------------------------

?>