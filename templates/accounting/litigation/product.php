<?php 

$editable =  $litigation->isEditable();
$editable &= $litigation->getGoodsDeliveryLocation() == Litigation::$GOODS_DELIVERY_LOCATION_NONE || !$litigation->getGoodsHasBeenDelivered();
$editable &= !$litigation->getGoodsHasBeenDelivered();

$disabled = $editable ? "" : " disabled=\"disabled\""; 

createRepaymentEditor( $litigation ); 

?>
<script type="text/javascript"> 
/* <![CDATA[ */
	
	/*-------------------------------------------------------------------------*/

	function showSupplierInfos( idsupplier){
					
		if( idsupplier == '0' )
			return;

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
            		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
			}

		});
		
	}

	/*-------------------------------------------------------------------------*/

/* ]]> */
</script>
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Descriptifs du produit facturé</p>
        	</div><!-- subTitleContainer -->
			<a name="references"></a>
			<div class="tableContainer" style="margin-bottom:5px;">
				<table class="dataTable devisTable">
				    <tr>
				    	<th style="width:75px;">Photo</th>
				    	<th>Réf. & Désignation courte</th>
				    	<th>Quantité facturée</th>
				    	<th>Prix unitaire</th>
				    	<th>Remise<br />Client %</th>
				    	<th>Prix unitaire<br />Vente Net</th>
				    	<th>Prix vente<br />Total</th>
				    </tr>
				    <tr>
				    	<td rowspan="5" style="vertical-align:top;">
						<?php 
						
							/*image*/				displayReferenceThumb( $litigation->getItem() );
							/*promotion*/			/*@todo : n'existe pas dans les lignes article de la facture*/
							/*destockage*/			/*@todo : n'existe pas dans les lignes article de la facture*/
							/*prix au volume*/		displayVolPriceLink( $litigation->getItem() );
							
						?>
				    	</td>
				    	<td style="text-align:left;"><?php displayReferenceInfos( $litigation->getItem() ); ?></td>
						<td><?php echo $litigation->getItem()->get( "quantity" ); ?></td>
						<td><?php echo Util::priceFormat( $litigation->getItem()->get( "unit_price" ) ); ?></td>
						<td>
						<?php 
						
							/*remise client %*/
							
							if( DBUtil::getParameterAdmin( "view_ref_discount_add") == 0 )
								echo Util::rateFormat( $litigation->getItem()->get( "ref_discount" ) );
							else{
						
								?>
								<p><b>Base</b></p>
								<p><?php echo Util::rateFormat( $litigation->getItem()->get( "ref_discount_base" ) ) ?></p>
								<p><b>Complémentaire</b></p>
								<p><?php echo Util::rateFormat( $litigation->getItem()->get( "ref_discount_add" ) ) ?></p>
								<?php 
								
							}
							
						?>
						</td>
						<td><?php /*prix unitaire vente net*/ echo Util::priceFormat( $litigation->getItem()->get( "discount_price" ) ); ?></td>
						<td><?php echo Util::priceFormat( $litigation->getItem()->get( "quantity" ) * $litigation->getItem()->get( "discount_price" ) ); ?></td>		
					</tr>
					<tr>
				    	<td rowspan="4" style="width:210px; height:110px;">
							<textarea readonly="readonly" style="width:210px; height:110px; border-style:none;"><?php echo stripDesignationTags( $litigation->getItem() ); ?></textarea>
						</td>
						<th>Quantité</th>
						<th>Prix unitaire<br />Tarif fournisseur</th>
				    	<th>Remise<br />Fournisseur %</th>
				    	<th>Prix unitaire<br />Achat net</th>
				    	<th>Prix achat<br />Total</th>
				    </tr>
					<tr>
						<td>
						<?php
						
							/*popup stock*/ displayStockPopup( $litigation->getItem() );
							/*quantité*/
						
							?>
							<input<?php echo $disabled ?> type="text" class="textInput quantity" id="quantity" name="quantity" value="<?php echo $litigation->getItem()->get( "quantity" ) ?>" onkeypress="return forceUnsignedIntegerValue( event );" />
						</td>
						<td><?php echo Util::priceFormat( $litigation->getItem()->get( "unit_cost" ) ); ?></td>
						<td><?php echo Util::rateFormat( $litigation->getItem()->get( "unit_cost_rate" ) ); ?></td>
						<td><?php echo Util::priceFormat( $litigation->getItem()->get( "unit_cost_amount" )); ?></td>
						<td><?php echo Util::priceFormat( $litigation->getItem()->get( "quantity" ) * $litigation->getItem()->get( "unit_cost_amount" ) ); ?></td>	
					</tr>
					<tr>
						<th>Stock<br />interne</th>
						<th>Stock<br />externe</th>
						<th>Délai<br />d'éxpédition</th>
						<th>Poids unitaire</th>
						<th>Poids total</th>
					</tr>
					<tr>
						<td><?php echo $litigation->getItem()->get( "quantity" ); ?></td>
						<td><?php echo $litigation->getItem()->get( "quantity" ); ?></td>
						<td>
						<?php
						
							/*@todo delai de livraison absent dans la facture*/
							
							$query = "
							SELECT d.delay" . User::getInstance()->getLang() . " AS delay
							FROM order_row orow, delay d
							WHERE orow.idorder = '" . $litigation->getSale()->get( "idorder" ) . "'
							AND orow.idrow = '" . $litigation->getItem()->get( "idorder_row" ) . "'
							AND d.iddelay = orow.delivdelay
							LIMIT 1";
							
							echo htmlentities( DBUtil::query( $query )->fields( "delay" ) );
							
						?>
						</td>
						<td><?php echo Util::numberFormat( $litigation->getItem()->get( "weight" ) ) . " kg"; ?></td>
						<td><?php echo Util::numberFormat( $litigation->getItem()->get( "quantity" ) * $litigation->getItem()->get( "weight" ) ) . " kg"; ?></td>
					</tr>
				</table>
			</div><!-- tableContainer -->
			<?php
			
				if( $litigation->isEditable() ){
					
					?>
					<div class="leftContainer">
						<input type="submit" name="" value="Enregistrer" class="blueButton" />
					</div>
					<?php
					
				}
				
			?>
		    <div class="rightContainer" id="WayoutControls">
		    
		    </div><!-- rightContainer WayoutControls -->
			<div class="clear"></div>
		</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php

//-----------------------------------------------------------------------------------------------------

/**
 * boîte de dialogue pour la création d'un nouveau remboursement
 */
function createRepaymentEditor( $litigation ){

	?>
	<div id="RepaymentEditor" style="display:none; width:auto; height:auto;">
		<fieldset style="border:2px solid #E7E7E7; width:auto; color:#44474E; font-size:10px; font-family:Arial, Helvetica, sans-serif;">
			<legend style="font-weight:bold; text-transform:uppercase;">Enregistrer un nouveau remboursement</legend>
			<input type="hidden" id="CreateRepayment" name="CreateRepayment" value="0" />
            <input type="hidden" id="idbuyer" name="idbuyer" value="<?php echo $litigation->getCustomer()->get( "idbuyer" );  ?>" />
			<p style="margin:10px; text-align:left;">
				<label style="width:50%; text-align:right; float:left; margin-right:10px;">Mode de paiement</label>
				<?php 
				
					$lang = User::getInstance()->getLang();
					XHTMLFactory::createSelectElement( "payment", "idpayment", "name$lang", "name$lang", false, false, "", " id=\"idpayment\" name=\"idpayment\"" );
					
			?>
			</p>
			<p style="margin-right:10px; text-align:left;">
				<label style="width:50%; text-align:right; float:left; margin-right:10px;">Montant</label>
				<input type="text" style="border: 1px solid #AAAAAA; font-size: 11px; color:#44474E" size="10" name="repaymentAmount" id="repaymentAmount" value="<?php echo Util::numberFormat( $litigation->getItem()->get( "discount_price" ) * $litigation->getItem()->get( "quantity" ) ); ?>" onkeypress="return forceUnsignedFloatValue( event );" />&nbsp;&euro;
			</p>
			<p style="text-align:center; margin:10px;">
				<input type="button" value="Annuler" onclick="$.unblockUI(); resetRepaymentEditor();" style="border-style:none; background-color:#7A8EA3; color:#FFFFFF; padding:2px 13px 2px 13px; text-align:center;" />
				<input type="button" id="CreateRepaymentButton" value="Enregistrer" onclick="createRepayment();" style="border-style:none; background-color:#7A8EA3; color:#FFFFFF; padding:2px 13px 2px 13px; text-align:center;" />
			</p>
		</fieldset>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$item ){
		
	global $GLOBAL_START_URL;

	/*référence avant substitution @todo : n'existe pas dans la facture*/
	
	/*référence catalogue*/
	
	?>
	<p>Référence catalogue : <?php echo $item->get( "reference" ) ?></p>
	 <?php
	 
	 /*référence et infos fournisseur*/
	 
	 if( $item->get( "idsupplier" ) ){
		
		?>
		<p>
			<span class="lightGrayText">Réf. fournisseur : <?php echo DBUtil::getDBValue( "ref_supplier", "detail", "idarticle", $item->get( "idarticle" ) ) ?></span>
			&nbsp;
			<a href="#" onclick="showSupplierInfos( <?php echo $item->get( "idsupplier" ) ?> ); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
			</a>
		</p>
		<?php 

	 }
	
	/*référence alternative @todo : n'existe pas dans la facture*/
	
	/*lien fiche PDF*/
	
	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ) ){
		
		$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $item->get( "idarticle" ) );
		$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : "/catalog/pdf_product.php?idproduct=$idproduct";
		
		?>
		<p>
			<a class="blueLink" href="<?php echo $href ?>" target="_blank">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
			</a>
		</p>
		<?php
			
	}
								 	
}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( $idproduct ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" target="_blank">
		<?php 
		
	}
	
	?>
	<img src="<?php 
	
		$regs = array();
		if( preg_match( "/(.*)_[0-9]+/", $item->get('reference'), $regs ) ){
			
			$idarticle = DBUtil::getDBValue( "idarticle", "detail", "reference", $regs[ 1 ] );
			echo URLFactory::getReferenceImageURI( $idarticle, URLFactory::$IMAGE_ICON_SIZE );
	
		}
		else echo URLFactory::getReferenceImageURI( $item->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE ); 
		
	?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------

function displayStockPopup( &$item ){
	
	global $GLOBAL_START_URL;
	
	if( DBUtil::getDBValue( "stock_level", "detail", "idarticle", $item->get( "idarticle" ) ) == -1 )
		return;
			
	?>
	<p style="text-align:center; margin:5px;">
		<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/popup_stock.htm.php?r=<?php echo $item->get( "reference" ) ?>" onclick="window.open( this.href ); return false;">
			<img src="<?php echo $GLOBAL_START_URL ?>/www/img/stock.jpg" alt="Informations sur le stock" style="border-style:none;" />
		</a>
	</p>
	<?php
			
}

//-----------------------------------------------------------------------------------------------------
/**
 * destockage
 */
function displayDestockingLogo( &$item ){
	
	global $GLOBAL_START_URL;
	
	if( $item->get( "isDestocking" ) ){
		
		?>
		<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL ?>/www/img/destocking.gif" alt="Destockage" /></p>
		<?php
		
	}
			
}

//-----------------------------------------------------------------------------------------------------
/**
 * prix au volumne
 */
function displayVolPriceLink( &$item ){
	
	global $GLOBAL_START_URL;
	
	$idbuyer_family = DBUtil::getDBValue( "idbuyer_family", "buyer", "idbuyer", $item->get( "idbuyer" ) );
	
	$query = "
	SELECT COUNT( rate ) AS `count`
	FROM multiprice_family 
	WHERE idbuyer_family = '$idbuyer_family' 
	AND reference = '" . $item->get( "reference" ) . "' 
	AND begin_date <= NOW() 
	AND end_date >= NOW()";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->fields( "count" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="PopupVolume( '<?php echo $item->get( "reference" ) ?>','<?php echo $idbuyer_family ?>');return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/www/img/volume.gif" alt="Prix au volume" style="border-style:none;" />
			</a>
		</p>
		<?php
		
	}
		
}

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){

	$designation = $item->get( "designation" );
	$designation = str_replace("\n",'', $designation);
	$designation = str_replace("\r",'', $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = strip_tags($designation);
	
	return $designation;
		
}

//-----------------------------------------------------------------------------------------------------

?>