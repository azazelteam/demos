<?php 

	/* mode ajax */

	if( !headers_sent() && isset( $_GET[ "idlitigation" ] ) && isset( $_GET[ "ajax" ] ) ){
		
		include_once( "../../../config/init.php" );
		include_once( "$GLOBAL_START_PATH/objects/Litigation.php" );
		
		header( "Content-Type: text/html; charset=utf-8" );
		$litigation = new Litigation(  $_GET[ "idlitigation" ] );
		
	}

	$editable =  $litigation->isEditable();
	$editable &= $litigation->getGoodsDeliveryLocation() == Litigation::$GOODS_DELIVERY_LOCATION_NONE || !$litigation->getGoodsHasBeenDelivered();
	$editable &= !$litigation->getGoodsHasBeenDelivered();
	$disabled = $editable ? "" : " disabled=\"disabled\""; 
	
?>
<script type="text/javascript">
/* <![CDATA[ */

	function setAllowGoodsDelivery( allow ){
		
		document.getElementById( 'RequireGoodsDeliveryItem' ).style.display = allow ? 'block' : 'none';
		
		if( !allow ){
			
			var wayout_require_goods_delivery = document.forms.LitigationForm.elements[ 'wayout_require_goods_delivery' ];
			var i = 0;
			while( i < wayout_require_goods_delivery.length ){
				
				wayout_require_goods_delivery[ i ].checked = wayout_require_goods_delivery[ i ].value == 0;
				
				i++;
				
			}
			
		}
		
	}
	
/* ]]> */
</script>
<div class="mainContent fullWidthContent">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/accounting/litigation/litigation.htm.php</span>
<?php } ?>

	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Raisons</p>
	        </div>
	        <div class="tableContainer" style="margin-bottom:5px;">
				<table class="dataTable devisTable">
				    <tr>
				    	<th>Motif</th>
				    	<th>Retour de la marchandise</th>
				    	<th>Issue du litige</th>
				    	<th>Montant des frais de dossier HT</th>
				    </tr>
				    <tr>
				    	<td>
				    	<?php 
	        	
	        				include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" ); 
	        				include_once( "$GLOBAL_START_PATH/objects/ui/DXComboBox.php" ); 
	        				
			        		$lang = User::getInstance()->getLang();
			            	XHTMLFactory::createSelectElement( $table = "litigation_motives", $columnAsValue = "idlitigation_motive", $columnAsInnerHTML = "motive", $orderByColumn = "motive", $selectedValue = $litigation->getIdLitigationMotive(), $nullValue = false, $nullInnerHTML = "", $attributes = " name=\"idlitigation_motive\" id=\"idlitigation_motive\" style=\"width:auto;\"$disabled" );
			        		DXComboBox::create( "idlitigation_motive" );
			     
			        		?>
			        		<div id="LitigationMotiveHelp" style="display:none;">
			        			<p>
			        				<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/help.png" alt="aide" />
				        			Vous pouvez ajouter un nouveau motif de litige en cliquant sur le menu déroulant de gauche et en saisissant votre nouveau motif directement au clavier
			        			</p>
			        			<p style="text-align:center; margin:0px;">
			        				<input type="button" name="" value="Fermer" class="blueButton" onclick="$.unblockUI();" />
			        			</p>
			        		</div>
			        		<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/help.png" alt="aide" style="cursor:pointer; vertical-align:middle;" onclick="$.blockUI({message: $('#LitigationMotiveHelp' ),css: { padding: '15px', cursor: 'pointer', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }}); $('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);" />
	        			</td>
	        			<td>
	        				<ul style="list-style-type:none;">
	        					<li style="text-align:left;"><input<?php echo $disabled ?> type="radio" name="goods_delivery_location" value="<?php echo Litigation::$GOODS_DELIVERY_LOCATION_NONE ?>"<?php if( $litigation->getGoodsDeliveryLocation() == Litigation::$GOODS_DELIVERY_LOCATION_NONE ) echo " checked=\"checked\""; ?> onclick="setAllowGoodsDelivery( false );" /> non prévu</li>
	        					<li style="text-align:left;"><input<?php echo $disabled ?> type="radio" name="goods_delivery_location" value="<?php echo Litigation::$GOODS_DELIVERY_LOCATION_LOCAL ?>"<?php if( $litigation->getGoodsDeliveryLocation() == Litigation::$GOODS_DELIVERY_LOCATION_LOCAL ) echo " checked=\"checked\""; ?> onclick="setAllowGoodsDelivery( true );" /> en interne</li>
	        					<li style="text-align:left;"><input<?php echo $disabled ?> type="radio" name="goods_delivery_location" value="<?php echo Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER ?>"<?php if( $litigation->getGoodsDeliveryLocation() == Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER ) echo " checked=\"checked\""; ?> onclick="setAllowGoodsDelivery( true );" /> chez le fournisseur</li>
	            				<li style="text-align:left;"><input<?php echo $disabled ?> type="radio" name="goods_delivery_location" value="<?php echo Litigation::$GOODS_DELIVERY_LOCATION_BOTH ?>"<?php if( $litigation->getGoodsDeliveryLocation() == Litigation::$GOODS_DELIVERY_LOCATION_BOTH ) echo " checked=\"checked\""; ?> onclick="setAllowGoodsDelivery( true );" /> chez le fournisseur avec transit par le dépôt interne</li>
	            			</ul>
	        			</td>
            			<td>
	        				<ul style="list-style-type:none;">
	            				<li style="text-align:left;">
	            					<input<?php echo $disabled ?> type="radio" name="wayout_require_goods_delivery" value="0"<?php if( !$litigation->getWayoutRequireGoodsDelivery() ) echo " checked=\"checked\""; ?> /> immédiate
	            				</li>
	            				<li style="text-align:left; display: <?php echo $litigation->getGoodsDeliveryLocation() != Litigation::$GOODS_DELIVERY_LOCATION_NONE ? "block" : "none"; ?>;" id="RequireGoodsDeliveryItem">
	            					<input<?php echo $disabled ?> type="radio" name="wayout_require_goods_delivery" value="1"<?php if( $litigation->getWayoutRequireGoodsDelivery() ) echo " checked=\"checked\""; ?> /> sur retour marchandise
	            				</li>
	            			</ul>
            			</td>
            			<td>
            				<input<?php if( !$litigation->isEditable() ) echo " disabled=\"disabled\""; ?>  type="text" class="textInput price" id="management_fees" name="management_fees" value="<?php echo Util::numberFormat( $litigation->getManagementFees() ); ?>" onkeypress="return forceUnsignedFloatValue( event );" />&euro;
            			</td>
            		</tr>
				</table>
			</div>
			<?php
			
				if( $litigation->isEditable() ){
					
					?>
		        	<div class="leftContainer">
				    	<input class="blueButton" type="submit" name="" value="Enregistrer" />
				    </div>
				    <?php
				    
				}
				
			?>
		    <div class="clear"></div>
        </div><!-- subContent -->
        <?php
        
        	if( $litigation->getGoodsDeliveryLocation() != Litigation::$GOODS_DELIVERY_LOCATION_NONE )
        		displayGoodsDeliveryStatus( $litigation );
        		
        ?>
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php

//-------------------------------------------------------------------------------------------

function displayGoodsDeliveryStatus( &$litigation ){

	global $GLOBAL_START_PATH;
	
	/*$query = "
	SELECT rts.*
	FROM litigations l, 
		billing_buyer_row bbr,
		bl_delivery bl,
		bl_delivery_row blr, 
		order_supplier_row osr,
		return_to_sender rts
	WHERE l.idlitigation = '" . $litigation->getIdLitigation() . "'
	AND bbr.idbilling_buyer = '" . $litigation->getInvoice()->get( "idbilling_buyer" ) . "'
	AND bbr.idrow = '" . $litigation->getInvoiceItem()->get( "idrow" ) . "'
	AND bl.idbilling_buyer = bbr.idbilling_buyer
	AND bl.idbl_delivery = blr.idbl_delivery
	AND blr.idorder_row = bbr.idorder_row
	AND osr.idorder_supplier = bl.idorder_supplier
	AND osr.idorder_row = blr.idorder_row
	AND rts.idorder_supplier = osr.idorder_supplier
	AND rts.idrow = osr.idrow";
	
	$rs =& DBUtil::query( $query );*/
							
	?>
	<div class="subContent" style="margin-top:20px;">
    	<div class="subTitleContainer">
            <p class="subTitle">Suivi du retour de la marchandise</p>
        </div>
        <div class="tableContainer" style="margin-bottom:5px;">
			<table class="dataTable devisTable">
			    <tr>
			    	<th>Date de réception</th>
			    	<th>Marchandise à réceptionner</th>
			    	<th>Commentaires du magazinier</th>
			    </tr>
			    <tr>
			    	<td>
			    	<?php 
			    	
			    		if( $litigation->getReturnToSender() == null || $litigation->getReturnToSender()->get( "return_date" ) == "0000-00-00 00:00:00" )
			    			echo "-";
			    		else{
			    		
			    			include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
			    				
			    			echo usDatetime2eu( $litigation->getReturnToSender()->get( "return_date" ) );
			    		
			    		}
			    		
			    	?>
			    	</td>
			    	<td>
			    	<?php
			    	
			    		if( $litigation->getReturnToSender() == null )
			    			echo "-";
			    		else if( $litigation->getReturnToSender()->get( "validated" ) )
			    			echo "<span class=\"blueText\"><b>Validée</b></span>";
			    		else if( $litigation->getReturnToSender()->get( "return_date" ) == "0000-00-00 00:00:00" )
			    			echo "en attente";
			    		else echo "<span class=\"orangeText\"><b>Non validée</b></span>";
			    		
			    	?>
			    	</td>
			    	<td>
			    	<?php 
			    	
			    		if( $litigation->getReturnToSender() != null )
				    		echo strlen( $litigation->getReturnToSender()->get( "comment" ) ) ? htmlentities( $litigation->getReturnToSender()->get( "comment" ) ) : "aucun commentaire"; 
			    		else echo "-";
			    		
			    	?>
			    	</td>
			    </tr>
			</table>
		</div>
	    <div class="clear"></div>
    </div><!-- subContent -->
    <?php
        	
}

//-------------------------------------------------------------------------------------------

?>