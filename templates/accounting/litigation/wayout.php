<?php

/**
 * liste des issues au litige
 */

/* mode ajax */

if( !headers_sent() && isset( $_GET[ "idlitigation" ] ) && isset( $_GET[ "ajax" ] ) ){
	
	include_once( "../../../config/init.php" );
	include_once( "$GLOBAL_START_PATH/objects/Litigation.php" );
	
	header( "Content-Type: text/html; charset=utf-8" );
	$litigation = new Litigation(  $_GET[ "idlitigation" ] );
	
}

$editable =  $litigation->isEditable();
$editable &= $litigation->getWayoutRequireGoodsDelivery() == false || ( $litigation->getGoodsHasBeenDelivered() && $litigation->getGoodsDeliveryHasBeenValidated() );

?>
<div class="mainContent fullWidthContent" style="margin-top:25px;">
    <div class="topRight"></div>
    <div class="topLeft"></div>
	<div class="content">
		<div class="headTitle">
			<p class="title">Issues en cours</p>
  			<div class="clear"></div>      	
		</div><!-- headTitle -->
		<?php
					
			$wayoutCount = $litigation->getCreditCount() + $litigation->getExchangeCount() + $litigation->getRepaymentCount() ;
			
		    if( $editable ){

				?>
				<p style="text-align:right; margin-top:10px;">
					<span style="float:left;"><?php echo $wayoutCount ? "$wayoutCount issue(s) en cours" : "Auncune issue créée"; ?></span>
			    	<input class="blueButton" type="button" name="" value="Créer un avoir" onclick="createCredit();" />
			    	<input class="blueButton" type="button" name="" value="Procéder à un échange" style="margin-left:5px;" onclick="createExchange();" />
			    	<input class="blueButton" type="button" name="" value="Effectuer un remboursement" style="margin-left:5px;" onclick="createRepaymentEditor();" />
				</p>
				<?php

			}
			
			if( $litigation->getWayoutRequireGoodsDelivery() && !$litigation->getGoodsHasBeenDelivered() ){
		
				?>
				<div class="subContent" style="margin-top:25px;">
					<p class="msg" style="text-align:center; margin-bottom:20px;">Le retour de la marchandise n'a pas encore été validé</p>
				</div><!-- subContent -->
				<?php
				
				listLitigationCredits( $litigation );
				listLitigationExchanges( $litigation );
				listLitigationRepayments( $litigation );
				
			}
			else if( $litigation->getWayoutRequireGoodsDelivery() && !$litigation->getGoodsDeliveryHasBeenValidated() ){
		
				?>
				<div class="subContent" style="margin-top:25px;">
					<p class="msg" style="text-align:center;">Le retour de la marchandise n'a pas été validé.</p>
					<p style="text-align:center; margin:10px 0px 20px 0px;"><input type="button" class="orangeButton" value="Ignorer" onclick="ignoreDeliveryFailure();" /></p>
				</div><!-- subContent -->
				<?php
				
			}
			else if( $wayoutCount ){
				
				listLitigationCredits( $litigation );
				listLitigationExchanges( $litigation );
				listLitigationRepayments( $litigation );
				
			}
			
		?>
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php

//-----------------------------------------------------------------------------------

/**
 * liste des avoirs en cours/éffectués
 */
function listLitigationCredits( &$litigation ){
	
	global $GLOBAL_START_URL;
	
	if( !$litigation->getCreditCount() )
		return;
	
	$query = "
	SELECT c.idcredit,
		c.idbilling_buyer,
		c.idbuyer,
		c.editable,
		SUBSTRING( c.DateHeure, 1, 10 ) AS DateHeure,
		c.total_amount_ht,
		c.total_amount,
		c.total_charge_ht,
		c.credited_charges,
		c.vat_rate,
		ROUND( SUM( cr.discount_price * cr.quantity ) + c.credited_charges, 2 ) AS totalETWoCharge,
		ROUND( c.credited_charges, 2 ) AS creditedChargesET,
		ROUND( c.total_charge_ht, 2 ) AS invoicedChargesET,
		u.initial,
		ex.idexchange,
		r.idrepayment
	FROM `user` u, credits c
	LEFT JOIN exchanges ex ON c.idcredit = ex.idcredit
	LEFT JOIN repayments r ON c.idcredit = r.idcredit
	LEFT JOIN credit_rows cr ON cr.idcredit = c.idcredit
	WHERE c.idlitigation = '" . $litigation->getIdLitigation() . "'
	AND c.iduser = u.iduser
	GROUP BY c.idcredit
	ORDER BY c.DateHeure DESC";

	$rs =& DBUtil::query( $query );
	
	?>
	<div class="subContent" style="margin-top:25px;">
		<div class="subTitleContainer">
			<a name="credits"></a>
            <p class="subTitle">Avoirs créés</p>
		</div><!-- subTitleContainer -->
		<div class="tableHeaderLeft"></div>
		<div class="tableHeaderRight"></div>
		<div class="resultTableContainer clear">
	    	<table class="dataTable resultTable" style="margin-top:10px; margin-bottom:10px;">
	    		<thead>
		    		<tr>
		    			<td style="border-style:none;"></td>
						<th class="filledCell">Com</th>
						<th class="filledCell">Avoir n°</th>
						<th class="filledCell">Facture n°</th>
						<th class="filledCell">Client n°</th>
						<th class="filledCell">Statut</th>
						<th class="filledCell">Date de l'avoir</th>
						<th class="filledCell">Editer l'avoir</th>
						<th class="filledCell">Montant HT</th>
						<th class="filledCell">Ports refacturés</th>
						<th class="filledCell">Total HT</th>
						<th class="filledCell">Total TTC</th>
					</tr>
				</thead>
				<tbody>
		    	<?php
		    	
		    		$totalETWoChargeSum 	= 0.0;
		    		$invoicedChargesETSum 	= 0.0;
		    		$creditedChargesETSum 	= 0.0;
		    		
		    		$totalETSum 			= 0.0;
		    		$totalATISum 			= 0.0;
		    		
					while( !$rs->EOF() ){
		
						$totalETWoChargeSum 	+= $rs->fields( "totalETWoCharge" );
						$invoicedChargesETSum 	+= $rs->fields( "invoicedChargesET" );
						$creditedChargesETSum 	+= $rs->fields( "creditedChargesET" );
						
						//$totalET 				= $rs->fields( "totalETWoCharge" ); + $rs->fields( "invoicedChargesET" ) - $rs->fields( "creditedChargesET" );
						//$totalATI 				= $totalET * ( 1.0 + $rs->fields( "vat_rate" ) / 100.0 );
						
						$totalET 				= $rs->fields( "total_amount_ht" );
						$totalATI 				= $rs->fields( "total_amount" );
						
						$totalETSum 			+= $totalET;
						$totalATISum 			+= $totalATI;
						
						?>
						<tr class="blackText">
							<td style="border-style:none; text-align:right; padding:2px; width:19px;">
							<?php
							/*
								if( $litigation->isEditable() && $rs->fields( "editable" ) && $rs->fields( "idexchange" ) == null && $rs->fields( "idrepayment" ) == null ){
									
									?>
									<img style="cursor:pointer;" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="supprimer" onclick="if( !confirm( 'Voulez-vous vraiment supprimer cet avoir?' ) ) return false; dropCredit( '<?php echo $rs->fields( "idcredit" ) ?> ');" />
									<?php
									
								}
*/
							?>
							</td>
							<td class="lefterCol"><?php echo $rs->fields( "initial" ) ?></td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $rs->fields( "idcredit" ) ?>" target="_blank">
									<?php echo $rs->fields( "idcredit" ) ?>
								</a>
							</td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $rs->fields( "idbilling_buyer" ) ?>" target="_blank">
									<?php echo $rs->fields( "idbilling_buyer" ) ?>
								</a>
							</td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=buyer&key=<?php echo $rs->fields( "idbuyer" ) ?>" target="_blank">
									<?php echo $rs->fields( "idbuyer" ) ?>
								</a>
							</td>
							<td class="<?php echo $rs->fields( "editable" ) ? "orangeText" : "blueText" ?>"><b><?php echo $rs->fields( "editable" ) ? "En cours" : "Validé" ?></b></td>
							<td><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ) ) ?></td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $rs->fields( "idcredit" ) ?>" target="_blank">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="" />
								</a>
							</td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "totalETWoCharge" ) ) ?></td>
							<td style="text-align:right;">-<?php echo Util::priceFormat( $rs->fields( "invoicedChargesET" ) ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $totalET ) ?></td>
							<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $totalATI ) ?></td>
						</tr>
						<?php
						
						$rs->MoveNext();
						
					}
		    		
			    	?>
			    	<tr>
			    		<td colspan="8" style="border-style:none;"></td>
						<td style="text-align:right; border-style:none; font-weight:bold;"><?php echo Util::priceFormat( $totalETWoChargeSum ) ?></td>
						<td style="text-align:right; border-style:none; font-weight:bold;">-<?php echo Util::priceFormat( $invoicedChargesETSum ) ?></td>
						<td style="text-align:right; border-style:none; font-weight:bold;"><?php echo Util::priceFormat( $totalETSum ) ?></td>
						<td style="text-align:right; border-style:none; font-weight:bold;"><?php echo Util::priceFormat( $totalATISum ) ?></td>
			    	</tr>
		    	</tbody>
	    	</table>
	    </div>
	</div><!-- subContent -->
	<?php

}

//-----------------------------------------------------------------------------------

/**
 * liste des échanges en cours/éffectués
 */
function listLitigationExchanges( &$litigation ){
	
	global 	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
	
	if( !$litigation->getExchangeCount() )
		return;
	
	$query = "
	SELECT ex.idexchange,
		ex.idorder,
		o.idorder,
		o.idbuyer,
		o.`status`,
		SUBSTRING( o.DateHeure, 1, 10 ) AS DateHeure,
		o.total_amount_ht,
		o.total_amount,
		u.initial,
		c.editable = 1 AND o.status IN ( 'ToDo', 'InProgress' ) AS allowDelete
	FROM exchanges ex, credits c, `order` o, `user` u
	WHERE ex.idcredit = c.idcredit
	AND c.idlitigation = '" . $litigation->getIdLitigation() . "' 
	AND ex.idorder = o.idorder
	AND o.iduser = u.iduser
	ORDER BY o.DateHeure DESC";

	$rs =& DBUtil::query( $query );
	
	?>
	<div class="subContent">
		<div class="subTitleContainer">
			<a name="exchanges"></a>
            <p class="subTitle">Echanges éffectués</p>
		</div>
		<div class="tableHeaderLeft"></div>
		<div class="tableHeaderRight"></div>
		<div class="resultTableContainer clear">
	    	<table class="dataTable resultTable" style="margin-top:10px; margin-bottom:10px;">
	    		<thead>
		    		<tr>
		    			<td style="border-style:none;"></td>
						<th class="filledCell">Com</th>
						<th class="filledCell">Commande n°</th>
						<th class="filledCell">Client n°</th>
						<th class="filledCell">Statut</th>
						<th class="filledCell">Date de commande</th>
						<th class="filledCell">Editer la commande</th>
						<th class="filledCell">Commandes fournisseur</th>
						<th class="filledCell">Factures</th>
						<th class="filledCell">Total HT</th>
						<th class="filledCell">Total TTC</th>
					</tr>
				</thead>
				<tbody>
				<?php
		    	
		    		$total_amountSum 	= 0.0;
		    		$total_amount_htSum = 0.0;
		    		
					while( !$rs->EOF() ){
		
						$total_amountSum 	+= $rs->fields( "total_amount" );
						$total_amount_htSum += $rs->fields( "total_amount_ht" );
						
						?>
						<tr class="blackText">
							<td style="border-style:none; text-align:right; padding:2px; width:19px;">
							<?php
							
								if( $litigation->isEditable() && $rs->fields( "allowDelete" ) ){
									
									?>
									<img style="cursor:pointer;" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="supprimer" onclick="if( !confirm( 'Voulez-vous vraiment supprimer cet échange?' ) ) return false; dropExchange( <?php echo $rs->fields( "idexchange" ) ?> );" />
									<?php
									
								}
							
							?>
							</td>
							<td class="lefterCol"><?php echo $rs->fields( "initial" ) ?></td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $rs->fields( "idorder" ) ?>" target="_blank">
									<?php echo $rs->fields( "idorder" ) ?>
								</a>
							</td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=buyer&key=<?php echo $rs->fields( "idbuyer" ) ?>" target="_blank">
									<?php echo $rs->fields( "idbuyer" ) ?>
								</a>
							</td>
							<td class="<?php echo in_array( $rs->fields( "status" ), array( "ToDo", "DemandInProgress", "InProgress" ) ) ? "orangeText" : "blueText" ?>"><b><?php echo Dictionnary::translate( $rs->fields( "status" ) ) ?></a></td>
							<td><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ) ) ?></td>
							<td>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $rs->fields( "idorder" ) ?>" target="_blank">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="" />
								</a>
							</td>
							<td style="text-align:right;">
								<ul style="list-style-type:none;">
								<?php
								
									$rs2 =& DBUtil::query( "SELECT idorder_supplier FROM order_supplier WHERE idorder = '" . $rs->fields( "idorder" ) . "'" );
									while( !$rs2->EOF() ){
										
										?>
										<li>
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $rs2->fields( "idorder_supplier" ) ?>" target="_blank"><?php echo $rs2->fields( "idorder_supplier" ) ?></a>
										</li>
										<?php
										
										$rs2->MoveNext();
										
									}
									
								?>
								</ul>
							</td>
							<td style="text-align:right;">
								<ul style="list-style-type:none;">
								<?php
								
									$rs2 =& DBUtil::query( "SELECT idbilling_buyer FROM billing_buyer WHERE idorder = '" . $rs->fields( "idorder" ) . "'" );
									while( !$rs2->EOF() ){
										
										?>
										<li>
											<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $rs2->fields( "idbilling_buyer" ) ?>" target="_blank"><?php echo $rs2->fields( "idbilling_buyer" ) ?></a>
										</li>
										<?php
										
										$rs2->MoveNext();
										
									}
									
								?>
								</ul>
							</td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
							<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
						</tr>
						<?php
						
						$rs->MoveNext();
						
					}
		    		
			    	?>
			    	<tr>
			    		<td colspan="9" style="border-style:none;"></td>
			    		<td style="text-align:right; font-weight:bold; border-style:none;"><?php echo Util::priceFormat( $total_amount_htSum ) ?></td>
			    		<td style="text-align:right; font-weight:bold; border-style:none;"><?php echo Util::priceFormat( $total_amountSum ) ?></td>
			    	</tr>
		    	</tbody>
	    	</table>
	    </div>
	</div><!-- subContent -->
	<?php
				
}

//-----------------------------------------------------------------------------------

/**
 * liste des remboursements en cours/éffectués
 */
function listLitigationRepayments( &$litigation ){
	
	global $GLOBAL_START_URL;
	
	if( !$litigation->getRepaymentCount() )
		return;
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT rp.idrepayment,
		rp.idpayment,
		rp.amount,
		SUBSTRING( rp.creation_date, 1, 10 ) AS creation_date,
		SUBSTRING( rp.payment_date, 1, 10 ) AS payment_date, 
		rp.payment_date <> '0000-00-00 00:00:00' AS paid,
		p.name$lang AS paymentName
	FROM credits c, repayments rp
	LEFT JOIN payment p
	ON rp.idpayment = p.idpayment
	WHERE c.idlitigation = '" . $litigation->getIdLitigation() . "'
	AND rp.idcredit = c.idcredit
	ORDER BY rp.creation_date DESC";

	$rs =& DBUtil::query( $query );
	
	?>
	<div class="subContent">
		<a name="repayments"></a>
		<div class="subTitleContainer">
            <p class="subTitle">Remboursements éffectués</p>
		</div>
		<div class="tableHeaderLeft"></div>
		<div class="tableHeaderRight"></div>
		<div class="resultTableContainer clear">
	    	<table class="dataTable resultTable" style="margin-top:10px; margin-bottom:10px;">
	    		<thead>
		    		<tr>
		    			<td style="border-style:none;"></td>
		    			<th class="filledCell">Mode de paiement</th>
		    			<th class="filledCell">Date de création</th>
		    			<th class="filledCell">Date de paiement</th>
						<th class="filledCell">Statut</th>
						<th class="filledCell">Montant</th>
					</tr>
				</thead>
				<tbody>
				<?php
				
					$amountSum = 0.0;
					
					while( !$rs->EOF() ){
						
						$amountSum += $rs->fields( "amount" );
						
						?>
						<tr>
							<td style="border-style:none; text-align:right; padding:2px; width:19px;">
							<?php
							
								if( $litigation->isEditable() && !$rs->fields( "paid" ) ){
									
									?>
									<img style="cursor:pointer;" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="supprimer" onclick="if( !confirm( 'Voulez-vous vraiment supprimer ce remboursement?' ) ) return false; dropRepayment( <?php echo $rs->fields( "idrepayment" ) ?> );" />
									<?php
									
								}
							
							?>
							</td>
							<td class="lefterCol"><?php echo htmlentities( $rs->fields( "paymentName" ) ) ?></td>
							<td><?php echo Util::dateFormatEu( $rs->fields( "creation_date" ) ) ?></td>
							<td><?php echo $rs->fields( "paid" ) ? Util::dateFormatEu( $rs->fields( "payment_date" ) ) : "-"	 ?></td>
							<td class="<?php echo $rs->fields( "paid" ) ? "blueText" : "orangeText" ?>"><b><?php echo $rs->fields( "paid" ) ? "Effectué" : "A éffectuer" ?></b></td>
							<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "amount" ) ) ?></td>
						</tr>
						<?php
						
						$rs->MoveNext();
							
					}
					
					?>
					<tr>
						<td colspan="5" style="border-style:none;"></td>
						<td style="border-style:none; text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $amountSum ) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php
				
}

//-----------------------------------------------------------------------------------

?>