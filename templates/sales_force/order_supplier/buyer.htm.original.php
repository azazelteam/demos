<?php

/* -------------------------------------------------------------------------------------------------------------- */
/* sélection adresse de livraison */

if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] = "iddelivery" ){

	include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/Customer.php" );
	include_once( dirname( __FILE__ ) . "/../../../objects/ForwardingAddress.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
	
	$rs =  DBUtil::query( "SELECT idbuyer, idcontact FROM order_supplier WHERE idorder_supplier = '" . intval( $_REQUEST[ "idorder_supplier" ] ) . "'" );
	
	$customer 			= new Customer( $rs->fields( "idbuyer" ), $rs->fields( "idcontact" ) );
	$forwardingAddress 	= intval( $_REQUEST[ "iddelivery" ] ) ? new ForwardingAddress( intval( $_REQUEST[ "iddelivery" ] ) ) : $customer->getAddress();
	//$forwardingAddress 	= intval( $_REQUEST[ "iddelivery" ] ) ? new ForwardingAddress( intval( $_REQUEST[ "iddelivery" ] ) ) : new ForwardingAddress( intval( DBUtil::getParameterAdmin( "internal_delivery_default" ) ) );
	
	listForwardingAddresses( $customer, $forwardingAddress, true );
	
	exit();
	
}

/* -------------------------------------------------------------------------------------------------------------- */

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "UpdateStatus" );
anchor( "UpdateMiscInfos" );

$bls = SupplierOrder::getBLs( $IdOrder );
$blCount = count( $bls );
						
?>
<script type="text/javascript"> 
/* <![CDATA[ */
	function showFilesInfos(){
		var filesdiv = document.getElementById( 'FilesDiv' );
		filesdiv.style.display = filesdiv.style.display == 'block' ? 'none' : 'block';
	}
	
	function visibility(thingId, txtAff, txtMasque) {
		var targetElement; 
		var targetElementLink;
		targetElement = document.getElementById(thingId);
		targetElementLink = document.getElementById(thingId+'Link');
		if (targetElement.style.display == "none") {
			targetElement.style.display = "";
			targetElementLink.innerHTML = txtMasque;
		} else {
			targetElement.style.display = "none";
			targetElementLink.innerHTML = txtAff;
		}
	}
	
	function changeStatus(){
		var status = document.getElementById( 'orderSupplierStatus' ).value;
		document.location = "<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $IdOrder ?>&changestatus&status=" + status;
	}


	function checkAndChangeIdOrderBuyer(IdO,IdOrderBuyer){
		var postData = "IdOrder="+IdO+"&ChangeIdOrder&IdOrderBuyer="+IdOrderBuyer;
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la commande fournisseur" ); },
		 	success: function( responseText ){

    			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
    			$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
    			$.blockUI.defaults.growlCSS.color = '#FFFFFF';

    			if( responseText == "success"){

    				$.blockUI.defaults.growlCSS.backgroundColor = '#000';
        			$.growlUI( '', 'Modifications enregistrées' );
        			
			    	window.location.href="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder="+IdO;
			    	
        		}else{
					
		    		$.growlUI( '', responseText );
		    		
		    	}
					
			}

		});
	}

	function changeMyForm(IdO,optionValue){
						
		var postData = "IdOrder="+IdO+"&changestatus&status="+optionValue+"&blCount=<?=$blCount?>";
	
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la commande fournisseur" ); },
		 	success: function( responseText ){

    			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
				$.blockUI.defaults.growlCSS.backgroundColor = '#000';
    			
    			if( responseText.length ){

        			$( "#tableOperationCouille" ).html( responseText );
        			
        		}
					
			}

		});
		
		
	}
	
	function updateMyStatus(IdO,optionValue){
	
		var postData = "IdOrder="+IdO+"&updStatus&status="+optionValue;
		
		var divElements = document.getElementById("tableOperationCouille").getElementsByTagName("input");
		var stringDeFou = "";
		
		for (i=0 ; i< divElements.length ; i++){
			stringDeFou += "&"+divElements[i].id+"="+divElements[i].value;
		}
		
		postData += stringDeFou;
							
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la commande fournisseur" ); },
		 	success: function( responseText ){
		
    			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
    			$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
    			$.blockUI.defaults.growlCSS.color = '#FFFFFF';

    			if( responseText == "success"){

    				$.blockUI.defaults.growlCSS.backgroundColor = '#000';
        			$.growlUI( '', 'Modifications enregistrées' );
        			
			    	document.getElementById('frm_buyer').submit();
			    	
        		}else{
					
		    		$.growlUI( '', responseText );
		    		
		    	}
		    	
			}

		});
		
	}
	
	function confirmAnnulation(){
		ret = confirm('Confirmez vous l\'annulation ?');
		
		if(ret){
			updateMyStatus(<?php echo $IdOrder ?>,'annuler');
		}
	}

	function confirmDeliveryMail(){
		ret = confirm('Confirmer l\'envoi du mail d\'expédition ?');
		
		if(ret){
			document.getElementById("mail_manu").value = "1";
			document.getElementById('frm_buyer').submit();

		}
	}
	
	function confirmDeliverySecondMail(){
		ret = confirm('Confirmer l\'envoi du mail d\'expédition ?');
		
		if(ret){
			document.getElementById('sendmail').value = "1";
			document.getElementById('frm').submit();
		}
	}

	/* ------------------------------------------------------------------------------------------------- */
	
	function updateSupplierOrder( property, value ){
		
		var data = "property=" + escape( property ) + "&value=" + escape( value );
		
		$.ajax({
		 	
			url: "<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?update&idorder_supplier=<?php echo $Order->getId(); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sauvegarder les modifications" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Impossible de sauvegarder les modifications : " + responseText ); 
					return;
					
				}
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );
    				
			}

		});
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
/* ]]> */
</script>



<!-- Bloc formulaire -->
<div class="contentResult" style="margin-bottom: 10px;">


	<h1 class="titleEstimate">
	<span class="textTitle">
		<div class="floatleft">Commande fournisseur n°<?php echo $IdOrder ?></div>
	</span>
	<span class="selectDate">
		<?php
			$iduser = User::getInstance()->getId();
			$IsAdmin = User::getInstance()->get("admin");
		?>
		
		<p style="font-size:14px;"><strong>Statut : </strong>
		<?php
					 
			if( $IsAdmin == 1 ){

				$lang = "_1";
				
				$selectedValue = $Order->get( 'status' );
				$selectedValueDisplayOrder = 0;
				
				$andlinette = "";					
				if( $selectedValue == 'received' || $selectedValue == 'invoiced' || $selectedValue == 'confirmed' || $selectedValue == 'dispatch'){
					$selectedValueDisplayOrder = DBUtil::getDBValue( "display_order", "order_supplier_status", "order_supplier_status", $selectedValue );
				}
				if($selectedValue!='invoiced'){
					$andlinette = " AND order_supplier_status<>'invoiced' ";						
				}	
							
				echo '<select onChange=\'changeMyForm('.$IdOrder.',this.options[this.selectedIndex].value);\' name=\'status\' id=\'orderSupplierStatus\' style=\'border: 1px solid #AAAAAA;\''.$disabled.'>';					
				$rs = DBUtil::query('SELECT order_supplier_status AS status, order_supplier_status'.$lang.' FROM order_supplier_status WHERE display_order >= '.$selectedValueDisplayOrder.' '.$andlinette.' ORDER BY display_order ASC');
				if($rs->RecordCount()>0){
					while( !$rs->EOF() ){										
						if( $selectedValue == $rs->Fields( 'status' ) )
							$selected = ' selected=\'selected\'';
						else $selected = '';
						echo '<option value=\'' . $rs->Fields( 'status' ) . '\'' . $selected . '>' . $rs->Fields( 'order_supplier_status'.$lang ) . '</option>';
						$rs->MoveNext();
			        }  
				}
			    echo '</select>';
				
				if($selectedValue=='invoiced' || $selectedValue=='dispatch'){
					echo '<a href="#" onclick="confirmAnnulation();return false;" class="blueLink" style="margin-left:5px;color:red;text-transform:none;">Annuler</a>';						
				}
								
			}
			else echo htmlentities( Dictionnary::translate( $Order->get( "status" ) ) );
						
		?></p>
		

	</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>

	<div class="blocEstimateResult" style="margin:0;"><div style="margin:0 5px 5px 5px;">
		
		<div class="floatleft" style="margin-bottom:0px;">
			
			<p><strong>Crée le</strong> <?php echo humanReadableDatetime( $Order->get( "DateHeure" ) ); ?></p>
			<?php if( $Order->get("send_date") != "0000-00-00 00:00:00" ){ ?>
				<p><strong>Envoyée le</strong> <?php echo humanReadableDatetime( $Order->get("send_date") );?></p>
			<?php } if( $Order->get( "confirmation_date" ) != "0000-00-00" ){ ?>
				<p><strong>Confirmée le</strong> <?php echo humanReadableDate( $Order->get( "confirmation_date" ) );?></p>
			<?php } ?> 
			<p><a class="blueLink" href="./com_admin_order.php?IdOrder=<?php echo $Order->get('idorder')?>">Afficher la commande client n°<?php echo $Order->get('idorder')?></a></p>
			<div class="spacer"></div>
			
			<div class="blocMultiple blocMLeft" style="width:100%;"><div style="margin:5px 0;" id="tableOperationCouille">
				
				
				<?php 
			
				$confirmation_date = 	usDate2eu( $Order->get( "confirmation_date" ) );
				if( $confirmation_date == "00-00-0000" ){
					$confirmation_date = "";
				}
				
				$dispatch_date 		= DBUtil::getDBValue( "dispatch_date", "bl_delivery", "idorder_supplier", $Order->get( "idorder_supplier" ) );
				$availability_date 	= DBUtil::getDBValue( "availability_date", "bl_delivery", "idorder_supplier", $Order->get( "idorder_supplier" ) );
				
				if( $availability_date == "00-00-0000" ){
					$availability_date = "";
				}else{
					$availability_date = usDate2eu( $availability_date );
				}
				
				if( $dispatch_date == "00-00-0000" ){
					$dispatch_date = "";
				}else{
					$dispatch_date = usDate2eu( $dispatch_date );
				}
				
				$bls = SupplierOrder::getBLs( $Order->get( "idorder_supplier" ) );
				$blCount = count( $bls );
				?>
				<div class="spacer"></div>				
					<label style='width:100px;'><span>Date réception</span></label>
					<input style='width:70px;' class="calendarInput" type="text" name="confirmation_date" id="confirmation_date" value="<?php echo $confirmation_date ?>" />
					<?php DHTMLCalendar::calendar( "confirmation_date" ) ?>
					
					<label style='width:200px; margin-left:8px;'><span>Date confirmée par fournisseur</span></label>
					<?php 
						if( $blCount > 1 ){	?>
							<span class=\"textidSimpleMin\" style='width:200px; border:none;'><?php echo Dictionnary::translate( "gest_com_partial_deliveries" ) ?></span><?
						}else{
							?><input style='width:70px;' class="calendarInput" type="text" name="availability_date" id="availability_date"  value="<?php echo $availability_date ?>" />	<?
							DHTMLCalendar::calendar( "availability_date" );
						}
			
				
				if($Str_status=='confirmed'){
					if($dispatch_date!='00-00-0000'){?>
						<label style='width:100px; margin-left:8px;'><span>Date d'expédition</span></label>
						<?php
							if( $blCount > 1 ){	?>
								<span class=\"textidSimpleMin\" style='width:200px; border:none;'><?php echo Dictionnary::translate( "gest_com_partial_deliveries" ) ?></span><?
							}else{
								echo $dispatch_date;
							}?>
					<?
					}
				}
				
				if($Str_status=='dispatch' OR $Str_status=='received' OR $Str_status=='invoiced' ){?>
					<label style='width:140px; margin-left:8px;'><span>Date expédition réelle</span></label>
					<?php
						if( $blCount > 1 ){	?>
							<span class=\"textidSimpleMin\" style='width:200px; border:none;'><?php echo Dictionnary::translate( "gest_com_partial_deliveries" ) ?></span><?
						}else{?>
							<input style='width:70px;' class="calendarInput" type="text" name="dispatch_date" id="dispatch_date" value="<?php echo $dispatch_date ?>"  /><?
							DHTMLCalendar::calendar( "dispatch_date" );
						}?>
					<?php
				}
				
				?>
				<input type="button" class="blueButton floatright" value="Modifier" name="UpdateMiscInfos" onclick="updateMyStatus(<?=$IdOrder?>,document.getElementById('orderSupplierStatus').options[document.getElementById('orderSupplierStatus').selectedIndex].value);" style="margin-top:5px; margin-left:8px;" />
			</div></div>
			<div class="spacer"></div>	
			
			
		</div>	
		
		<div class="floatright">
			
			<?php if( !$Order->get( "internal_supply" ) ){ //il ne s'agit pas d'une commande de réapprovisionnement
			
				$commande=$Order->get('idorder');
				$bdd=&DBUtil::getConnection();
				$query="SELECT status FROM `order` WHERE idorder=$commande";
				$rso=$bdd->Execute($query);
			?>
				<!-- <p><strong>&#0155; Informations sur la commande client</strong> <a class="blueLink" href="./com_admin_order.php?IdOrder=<?php echo $Order->get('idorder')?>">n°<?php echo $Order->get('idorder')?></a></p>
				<p><?php echo Dictionnary::translate("status") ?> : <?php echo Dictionnary::translate($rso->Fields('status')) ?></p>
				<p><a class="blueLink" href="/catalog/pdf_order.php?idorder=<?=$Order->get('idorder')?>" onclick="window.open(this.href); return false;" class="Edition_pdf"><?php  echo Dictionnary::translate("gest_com_conf_order_pdf") ; ?></a></p>
				<div class="spacer"></div> -->

			
				<!-- <p><strong>&#0155; Informations sur la livraison</strong></p> -->
				<p style="text-align:right;"><a class="blueLink" href="#" onClick="confirmDeliveryMail();">Prévenir le client que la marchandise a été expédiée		<input type="hidden" name="mail_manu" id="mail_manu" value="" />
				<div class="spacer"></div> 


			<?php } else{ ?>

		        <p><strong>&#0155; <?php echo Dictionnary::translate("gest_com_order_information") ; ?></strong></p>				
				<p>Affecter une commande client&nbsp;:&nbsp;</p>
				<input type="text" class="textInput" value="<?php echo $Order->get('idorder') ?>" style="width:70px;float:left;margin-top:1px;margin-right:5px;" name="IdOrderBuyer" id="IdOrderBuyer" <?php echo $disabled ?>>
			    <input <?php echo $disabled ?> type="button" class="blueButton" value="Modifier" name="ChangeIdOrder" id="ChangeIdOrder" style="float:left;margin-top:1px;" onClick="return confirm('Confirmez l\'affectation de la commande fournisseur à la commande client : '+document.getElementById('IdOrderBuyer').value+' ? ' ) && checkAndChangeIdOrderBuyer(<?=$IdOrder?>,document.getElementById('IdOrderBuyer').value);"/>
				<div class="spacer"></div>

			<?php } ?>
			<div class="spacer"></div>

			<p><strong>&#0155; Infos sur le BL 
			<?
				$i=0;
				foreach( $bls as $idbl_delivery ) {
					$deliveryNote = New DeliveryNote($idbl_delivery);
					
					if($i) { echo " - "; }
					if($deliveryNote->get( "date_delivery") <> '0000-00-00'){
						$date_or_text = "(".$deliveryNote->get( "date_delivery").")";
					}
					else{
						$date_or_text = '(En attente)';
					}
					echo "<a class=\"blueLink\" href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\">n°$idbl_delivery</a> ".$date_or_text;
					$i++;	
				}
				
				$query = "
				SELECT idorder_supplier
				FROM bl_delivery
				WHERE idorder_supplier = '" . $Order->getId() . "'
				AND idbilling_buyer = '0'
				AND date_send LIKE '0000-00-00'
				AND date_delivery LIKE '0000-00-00'";
				
				if( DBUtil::query( $query )->RecordCount() ){ ?>
					(<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/delivery_notes.php?idorder_supplier=<?php echo $Order->getId(); ?>">Modifier</a>)
				<?php } 
			?>
			</strong></p>
			<?php
			
				/* régulations */
			
				if( $Order->getSupplier()->getId() != DBUtil::getParameterAdmin( "internal_supplier" ) 
					&& !in_array( $Order->get( "status" ), array( SupplierOrder::$STATUS_CANCELLED, SupplierOrder::$STATUS_STANDBY ) ) ){
					
					?>
					<p>
						<strong>&#0155;</strong>
						<a class="blueLink" href="#" onclick="window.open('/accounting/register_supplier_invoices.php?supplier=<?php echo $Order->getSupplier()->getId(); ?>&bl=<?php 
						
							$deliveryNotes = $Order->getDeliveryNotes();
							$i = 0;
							while( $i < $deliveryNotes->size() ){
								
								if( $i ) 
									echo ",";
								
								echo $deliveryNotes->get( $i )->getId();
								
								$i++;
									
							}
							
						?>'); return false;"> Régulation
						</a>
					</p>
					<?php
					
				}
				
			?>
		</div>
		
		<div class="spacer"></div>
		
		<?php
			$useBuyerDelivery = $Order->get( "idorder" ) && DBUtil::getDBValue( "iddelivery", "order", "idorder", $Order->get( "idorder" ) ) == $Order->get( "iddelivery" );
			$bls = SupplierOrder::getBLs( $Order->get( "idorder_supplier" ) );
			$blCount = count( $bls );
			
			if( $blCount > 1 && $useBuyerDelivery) {
				$messageBl = Dictionnary::translate( "gest_com_partial_deliveries" );
			} else {
				$messageBl = $Order->getForwardingAddress()->getCompany(); 
			}
			
		?>
		
		<script type="text/javascript">
		<!--
			$(function() {
				$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
			});
		-->
		</script>
		<div id="container">
		
			<!-- Onglets à l'échalotte pour les infos client fournisseur et livraison -->
			<ul class="menu">
			    <li class="item"><a href="#DeliveryInfosDiv"><span>Infos commandes</span></a></li>
			    <li class="item"><a href="#visualiserImprimer"><span>Visualiser et imprimer</span></a></li>
			    <?php
					$PJ = $Order->get( "order_confirmed" );
					$PJOS = $Order->get( "order_supplier_doc" );
					$PJOSBL = $Order->get( "order_supplier_bl" );
					$PJOSBE = $Order->get( "order_supplier_be" );
					$PJOSDivers = $Order->get( "order_supplier_divers" );
				?>
			    <li class="item"><a href="#paClient">
			    	<span>Pièces archivées</span></a>
			    	<?php if($PJ || $PJOS || $PJOSBL || $PJOSBE || $PJOSDivers) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
			    </li>
			    
			    <?php 
				    $commentCdeFs = $Order->get('comment');
					$commentBl = $Order->get('commentBL'); 
				?>
			    <li class="item"><a href="#commentaire">
			    	<span>Commentaires</span></a>
			    	<?php if($commentCdeFs || $commentBl) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
			    </li>
			    <?php
			    
			    	if( $Order->get( "idsupplier" ) != DBUtil::getParameterAdmin( "internal_supplier" ) 
			    		&& ( $Order->getSupplier()->get( "remind_dispatch" ) || $Order->getSupplier()->get( "remind_confirmation" ) ) ){
			    		
			    		?>
					    <li class="item">
					    	<a href="#reminders"><span>Alertes</span></a>
					    	<?php 
					    	
					    		if( $Order->get( "remind_confirmation" ) 
					    			|| $Order->get( "remind_dispatch" )
					    			|| DBUtil::query( "SELECT idorder_supplier FROM supplier_confirmation_reminder WHERE idorder_supplier = '" . $Order->getId() . "'" )->RecordCount()
					    			|| DBUtil::query( "SELECT idorder_supplier FROM supplier_dispatch_reminder WHERE idorder_supplier = '" . $Order->getId() . "'" )->RecordCount() ){
					    				
				    				?>
									<img src="/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
									<?php 
		
					    		} 
						    		
					    ?>
					    </li>
					    <?php
					    
			    	}
			  ?>
			</ul>
			<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0 8px -5px; width: 100%;"> </div>
			<div class="spacer"></div>
			
			<!-- *********** -->
			<!--  Livraison  -->
			<!-- *********** -->
			<div id="DeliveryInfosDiv">
				
				
				
				<div class="blocMultiple3" style="background:#EEEEEE; border:1px solid #EEEEEE; width:30%; padding:0 1% 1% 1%;">
					<h2>Fournisseur : <?php echo $Order->getSupplier()->get( "name" ) ?></h2>
					<?php displaySupplierInfos(); ?>
				</div>
				
				<div class="blocMultiple3" style="border:1px solid #EEEEEE; width:30%; padding:0 1% 1% 1%;">
					<h2>Client : <?php echo $Order->getForwardingAddress()->getCompany() ?></h2>
					<?php displayBuyerInfos(); ?>
				</div>
				<script type="text/javascript">
				/* <![CDATA[ */
				
					function setSupplierOrderForwardingAddress( iddelivery ){
			
						$.ajax({
						 	
							url: "/templates/sales_force/order_supplier/buyer.htm.php?update=iddelivery",
							async: true,
							type: "POST",
							data: "idorder_supplier=<?php echo $Order->getId(); ?>&iddelivery=" + iddelivery,
							error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier l'adresse de livraison" ); },
						 	success: function( responseText ){
			
								$( "#ForwardingAddress" ).html( responseText );
								
							}
			
						});
						
					}
					
				/* ]]> */
				</script>
				<div class="blocMultiple3" style="background:#EEEEEE; border:1px solid #EEEEEE; width:30%; padding:0 1% 1% 1%;">
					<form action="/sales_force/supplier_order.php?IdOrder=<?php echo $Order->getId(); ?>" method="post" enctype="multipart/form-data">
						<div id="ForwardingAddress">
							<?php listForwardingAddresses( $Order->getCustomer(), $Order->getForwardingAddress() ); ?>
						</div>
						<p style="text-align:right;">
							<input type="submit" value="Sauvegarder" name="" class="blueButton" />
						</p>
					</form>
				</div>
				<div class="spacer"></div>
			</div>
						
			<!-- *********** -->
			<!--  Visu/Print -->
			<!-- *********** -->
			<div id="visualiserImprimer" class="displayNone">

				<?php if( !strlen( $Order->getSupplier()->getContact()->get( "email" ) ) ){ ?>
					<p class="subTitleWarning"><?php  echo Dictionnary::translate( "gest_com_supplier_without_mail_order" ) ; ?></p>
				<?php } ?>
				
				<?php if( $priority = getSupplierPriority( $Order->get( "idsupplier" ) ) ){ ?>
					<p><em><?php echo Dictionnary::translate( "gest_com_supplier_priority" ) ?> : <?php echo htmlentities( $priority ) ?></em></p>
				<?php } ?>
				<div class="spacer"></div>
			
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
					
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<p style="margin-bottom:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_supplier_order.php?idorder=<?=$IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
						&nbsp;PDF Commande fournisseur</a></p>
						
						<?php 				
							if(DBUtil::getParameterAdmin('internal_delivery')==$Order->get('iddelivery')){
								$blCount = 0;
								foreach( $bls as $idbl_delivery ){
									echo '<p style="margin-bottom:5px;">';
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\" class=\"normalLink\">";
									echo '<img src="'.$GLOBAL_START_URL.'/images/back_office/content/pdf_icon.gif" />';
									echo "&nbsp; PDF Bulletin de livraison n°$idbl_delivery</a></p>";
									
									echo '<p style="margin-bottom:5px;">';
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\" class=\"normalLink\">";
									echo '<img src="'.$GLOBAL_START_URL.'/images/back_office/content/pdf_icon.gif" />';
									echo "&nbsp; BL dépôt</a></p>";
									
									$blCount++;		
								}
							} else {
								$blCount = 0;
								foreach( $bls as $idbl_delivery ){
									echo '<p style="margin-bottom:5px;">';
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\" class=\"normalLink\">";	
									echo '<img src="'.$GLOBAL_START_URL.'/images/back_office/content/pdf_icon.gif" />';
									echo "&nbsp; PDF Bulletin de livraison n°$idbl_delivery</a></p>";
									$blCount++;	
								}
							}
							
							foreach( $bls as $idbl_delivery ){

								if( $idcarriage = DBUtil::getDBValue( "idcarriage", "carriage", "idcarriage", $idbl_delivery ) ){
									
									?>
									<p style="margin-bottom:5px;">
										<a href="/sales_force/pdf_delivery_order.php?idcarriage=<?php echo $idcarriage; ?>" class="normalLink" onclick="window.open(this.href); return false;">	
											<img src="/images/back_office/content/pdf_icon.gif" />
											&nbsp; PDF Ordre d'enlèvement n°<?php echo $idbl_delivery; ?>
										</a>
									</p>
									<?php

								}
								
							}
							
						?>
						
					</div></div>
					
					<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<p style="margin-bottom:5px;"><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_supplier_order.php?idorder=<?=$IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-fax.png" />
						&nbsp;FAX PDF Commande fournisseur</a></p>
						
						<?php 
							if(DBUtil::getParameterAdmin('internal_delivery')==$Order->get('iddelivery')){
								
								$blCount = 0;
								foreach( $bls as $idbl_delivery ){
									echo '<p style="margin-bottom:5px;">';
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\" class=\"normalLink\">";		
									echo '<img src="'.$GLOBAL_START_URL.'/images/back_office/layout/icons-menu-fax.png" />';
									echo "&nbsp; FAX PDF Bulletin de livraison n°$idbl_delivery</a></p>";
									
									echo '<p style="margin-bottom:5px;">';
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\" class=\"normalLink\">";		
									echo '<img src="'.$GLOBAL_START_URL.'/images/back_office/layout/icons-menu-fax.png" />';
									echo "&nbsp; FAX BL dépôt</a></p>";
									
									$blCount++;	
								}	
							} else {
								
								$blCount = 0;
								foreach( $bls as $idbl_delivery ){
									echo '<p style="margin-bottom:5px;">';
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\" class=\"normalLink\">";	
									echo '<img src="'.$GLOBAL_START_URL.'/images/back_office/layout/icons-menu-fax.png" />';
									echo "&nbsp; FAX PDF Bulletin de livraison n°$idbl_delivery</a></p>";
									
									$blCount++;	
								}
							}
						?>
					</div></div>

				</div>
				
				<?php if( strlen( $Order->getSupplier()->getContact()->get( "email" ) ) ){ ?>
				<div class="blocMultiple blocMRight" style="margin-bottom: 0px;">
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<p style="margin-bottom:5px;"><a href="transmit_supplier.php?IdOrder=<?php echo $IdOrder ?>" class="normalLink">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
						&nbsp; Envoyer par E-mail</a></p>
						
						<p><a href="transmit_supplier.php?IdOrder=<?php echo $IdOrder ?>" class="normalLink">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
						&nbsp; Envoyer PDF cde fournisseur + PDF BL</a></p>
					</div></div>
				</div>
				<?php } ?>
				
				
				<div class="spacer"></div>

			</div> <!-- # visualiserImprimer -->
			<div class="spacer"></div>
			
			
			<!-- ************** -->
			<!-- Pièces jointes -->
			<!-- ************** -->
			<div id="paClient" class="displayNone">
				
	
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
				
					<label style="width:50%;"><span><?php  echo Dictionnary::translate("gest_com_confirm_order") ; ?></span></label>
					<input type="file" name="order_confirmed" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php if(!empty($PJ)){?>
						<a class="floatleft" style="margin:4px 0 0 0;" href="<?php echo $GLOBAL_START_URL ?>/data/order_supplier/<?php echo $PJ ?>" onclick="window.open(this.href); return false;"><img src="<?=$GLOBAL_START_URL?>/images/back_office/content/pdf.gif" border="0" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;cursor:pointer" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_order_confirmed" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>
					
					<label style="width:50%;"><span>BL fournisseur :</span></label>
					<input type="file" name="order_supplier_bl" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php if(!empty($PJOSBL)){?>
						<a class="floatleft" style="margin:4px 0 0 0;" href="<?php echo $GLOBAL_START_URL ?>/data/order_supplier/<?php echo $PJOSBL ?>" onclick="window.open(this.href); return false;"><img src="<?=$GLOBAL_START_URL?>/images/back_office/content/pdf.gif" border="0" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;cursor:pointer" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_order_supplier_bl" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>

					<label style="width:50%;"><span>Piece jointe fournisseur :</span></label>
					<input type="file" name="order_supplier_doc" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php if(!empty($PJOS)){?>
						<a class="floatleft" style="margin:4px 0 0 0;" href="<?php echo $GLOBAL_START_URL ?>/data/order_supplier/<?php echo $PJOS ?>" onclick="window.open(this.href); return false;"><img src="<?=$GLOBAL_START_URL?>/images/back_office/content/pdf.gif" border="0" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;cursor:pointer" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_order_supplier_doc" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>				
					
					<label style="width:50%;"><span>Bon d'émargement :</span></label>
					<input type="file" name="order_supplier_be" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php if(!empty($PJOSBE)){?>
						<a class="floatleft" style="margin:4px 0 0 0;" href="<?php echo $GLOBAL_START_URL ?>/data/order_supplier/<?php echo $PJOSBE ?>" onclick="window.open(this.href); return false;"><img src="<?=$GLOBAL_START_URL?>/images/back_office/content/pdf.gif" border="0" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;cursor:pointer" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_order_supplier_be" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>

					<label style="width:50%;"><span>Divers :</span></label>
					<input type="file" name="order_supplier_divers" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
					<?php if(!empty($PJOSDivers)){?>
						<a class="floatleft" style="margin:4px 0 0 0;" href="<?php echo $GLOBAL_START_URL ?>/data/order_supplier/<?php echo $PJOSDivers ?>" onclick="window.open(this.href); return false;"><img src="<?=$GLOBAL_START_URL?>/images/back_office/content/pdf.gif" border="0" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
						<input class="floatleft" style="margin:3px 0 0 2px;cursor:pointer" type="image" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_order_supplier_divers" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
					<?php } ?>
					<div class="spacer"></div>
					
					<input type="submit" value="Enregistrer" name="UpdateFiles" id="UpdateFiles" style="float: right; margin:3px 0 0 0;" class="blueButton" />
				</div>
					
			</div> <!-- # paClient -->
			<div class="spacer"></div>
			
			
			<!-- *********** -->
			<!-- Commentaire -->
			<!-- *********** -->
			<div id="commentaire" class="displayNone">
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
					
					<label style="width:100%; background:none;"><span>Ajouter un commentaire sur la commande fournisseur</span></label>
			        <div class="spacer"></div>	
					<textarea <?php echo $disabled ?> name="comment" style="width: 100%; height:100px; margin:0; -moz-border-radius:5px; -webkit-border-radius:5px;" class="textInput"><?php echo html_entity_decode($commentCdeFs) ?></textarea>
					<div class="spacer"></div>
				    <input type="submit" value="Enregistrer" name="UpdComment" id="updateOrder" style="float: right; margin:3px 0 0 0;" class="blueButton" />
			    </div>
				<div class="blocMultiple blocMRight" style="margin-bottom: 0px;">
					
					<label style="width:100%; background:none;"><span>Ajouter un commentaire sur le BL</span></label>
					<div class="spacer"></div>
					<textarea <?php echo $disabled ?> name="commentBL" style="width: 100%; height:100px; margin:0; -moz-border-radius:5px; -webkit-border-radius:5px;" class="textInput"><?php echo html_entity_decode($commentBl) ?></textarea>
					<div class="spacer"></div>
					<input type="submit" value="Enregistrer" name="UpdComment" id="updateOrder" style="float: right; margin:3px 0 0 0;" class="blueButton" />
			    </div>				
				
			</div> <!-- # commentaire -->
			<div class="spacer"></div>
		
			<!-- *********** -->
			<!-- Reminders 	-->
			<!-- *********** -->
			<div id="reminders" class="displayNone">
				
				
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">				
					<?php
				
					$rs =& DBUtil::query( "SELECT * FROM supplier_confirmation_reminder WHERE idorder_supplier = '" . $Order->getId() . "'" );
					
					if( $rs->recordCount() )
						echo "<p>Rappel de demande de confirmation de Cde envoyé le " . Util::dateFormatEu( $rs->fields( "datetime" ) ) . "</p>";
					else{
						
						?>
						<p><input type="checkbox" value="1"<?php if( $Order->get( "remind_confirmation" ) ) echo " checked=\"checked\""; ?> onclick="updateSupplierOrder('remind_confirmation', this.checked ? 1 : 0 );" /> Activer le rappel de demande de confirmation de Cde automatique</p>
						<?php
						
					}
					
					?>
					<?php
					
					$rs =& DBUtil::query( "SELECT * FROM supplier_dispatch_reminder WHERE idorder_supplier = '" . $Order->getId() . "'" );
					
					if( $rs->recordCount() )
						echo "<p>Rappel d'expédition de la marchandise envoyé le " . Util::dateFormatEu( $rs->fields( "datetime" ) ) . "</p>";
					else{
						
						?>
						<p><input type="checkbox" value="1"<?php if( $Order->get( "remind_dispatch" ) ) echo " checked=\"checked\""; ?> onclick="updateSupplierOrder('remind_dispatch', this.checked ? 1 : 0 );" /> Activer le rappel d'expédition de la marchandise</p>
						<?php
						
					}
					
					?>
				</div></div>
				
			</div>
			
        </div> <!-- Fin container onglet -->

    <div class="spacer"></div>  
    </div></div>    

</div>

		



<?php 

//-------------------------------------------------------------------------------------------------------

function getSupplierPriority( $idsupplier ){
	
	
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT send_to FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer les préférences du fournisseur" );
		
	$priority = $rs->fields( "send_to" );
	
	if( empty( $priority ) )
		return false;
		
	return $rs->fields( "send_to" );
	
}


//--------------------------------------------------------------------------------------------------------------

function displayBuyerInfos(){
	
	global	$Order, $disabled;
		
	?>

	<p><strong><?php echo $Order->getCustomer()->get('company') ?></strong> (n°<?php echo $Order->getCustomer()->get('idbuyer') ?>)</p>
	<p><?php echo Util::getTitle(DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) )).' '.$Order->getCustomer()->getContact()->get( "firstname" ).' '.$Order->getCustomer()->getContact()->get( "lastname" ) ?></p>
	<p><?php echo $Order->getCustomer()->get('adress') ?>
	<?php if($Order->getCustomer()->get('adress_2')){ ?>
		- <?php echo $Order->getCustomer()->get('adress_2') ?>
	<?php } ?></p>
	<p><strong><?php echo $Order->getCustomer()->get('zipcode') ?> <?php echo $Order->getCustomer()->get('city') ?></strong> -
	<?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ) ?></p>
   
	<p style="margin-top:10px;"><?php if($Order->getCustomer()->getContact()->get( "phonenumber" )) { ?>
    	Tél. : <?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "phonenumber" )); ?>
    <?php } ?>
    <?php if($Order->getCustomer()->getContact()->get( "faxnumber" )) { ?>
    	- <?=Dictionnary::translate("fax") ?> : <?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "faxnumber" )); ?>
    <?php } ?></p>
    
    <p><?=Dictionnary::translate("email") ?> : <a class="blueLink" href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ) ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ) ?></a></p>

	<?php
	
}

//--------------------------------------------------------------------------------------------------------------
		
function displaySupplierInfos(){
	
	global	$Order,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function showSupplierInfos(){
						
			
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=<?php echo $Order->get( "idsupplier" ) ?>",
				async: false,
			 	success: function(msg){
			 		
					$.blockUI({
						
						message: msg,
						fadeIn: 700, 
	            		fadeOut: 700,
						css: {
							width: '700px',
							top: '0px',
							left: '50%',
							'margin-left': '-350px',
							'margin-top': '50px',
							padding: '5px', 
							cursor: 'help',
							'-webkit-border-radius': '10px', 
			                '-moz-border-radius': '10px',
			                'background-color': '#FFFFFF',
			                'font-size': '11px',
			                'font-family': 'Arial, Helvetica, sans-serif',
			                'color': '#44474E'
						 }
						 
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
	
			});
			
		}
		
	/* ]]> */
	</script>

	<p><strong><a href="#" class="blueLink" onclick="showSupplierInfos(); return false;"><?php echo $Order->getSupplier()->get('name'); ?></a></strong></p>
	<p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getSupplier()->getContact()->get( "idtitle" )  ).' '.$Order->getSupplier()->getContact()->get('firstname').' '.$Order->getSupplier()->getContact()->get('lastname') ?></p>
	<p><?php echo $Order->getSupplier()->get('adress'); ?>
	<?php if($Order->getSupplier()->get('adress_2')){ ?>
		- <?php echo $Order->getSupplier()->get('adress_2'); ?>
	<?php } ?></p>
	<p><strong><?php echo $Order->getSupplier()->get('zipcode') ?> <?php echo $Order->getSupplier()->get('city') ?></strong> -
	<?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getSupplier()->get('idstate') ); ?></p>
   
	<p style="margin-top:10px;"><?php if($Order->getSupplier()->getContact()->get('phonenumber')) { ?>
    	Tél. : <?php echo $Order->getSupplier()->getContact()->get('phonenumber'); ?>
    <?php } ?>
    <?php if($Order->getSupplier()->getContact()->get('faxnumber')) { ?>
    	- <?=Dictionnary::translate("fax") ?> : <?php echo $Order->getSupplier()->getContact()->get('faxnumber'); ?>
    <?php } ?></p>
    
    <p><?=Dictionnary::translate("email") ?> : <a class="blueLink" href="mailto:<?php echo $Order->getSupplier()->getContact()->get('email') ?>"><?php echo $Order->getSupplier()->getContact()->get('email') ?></a>
    <a href="#" onclick="showSupplierInfos(); return false;" class="viewContact" style="float:right;">Fiche fournisseur</a>
    </p>

	<?php
	
}

//--------------------------------------------------------------------------------------------------------------
	/*	
function displayDeliveryInfos(){
	
	global	$Order,$disabled;
		
	
	$useBuyerDelivery = $useBuyerDelivery = $Order->get( "idorder" ) && DBUtil::getDBValue( "iddelivery", "order", "idorder", $Order->get( "idorder" ) ) == $Order->get( "iddelivery" );
	$bls = SupplierOrder::getBLs( $Order->get( "idorder_supplier" ) );	
	?>

	
	
	<?php if( $useBuyerDelivery && count( $bls ) > 1 ){ ?>
		<p class="msg" style="text-align:center; margin-top:15px;"><?php echo Dictionnary::translate( "gest_com_partial_deliveries" ) ?></p>
	<?php return; } ?>
					
	<p><strong><?php echo $Order->getForwardingAddress()->getCompany() ?></strong></p>
	<p><?php echo Util::getTitle($Order->getForwardingAddress()->getGender()).' '.$Order->getForwardingAddress()->getFirstName().' '.$Order->getForwardingAddress()->getLastName() ?></p>
	<p><?php echo $Order->getForwardingAddress()->getAddress() ?>
	<?php if($Order->getForwardingAddress()->getAddress2()){ ?>
		- <?php echo $Order->getForwardingAddress()->getAddress2() ?>
	<?php } ?></p>
	<p><strong><?php echo $Order->getForwardingAddress()->getZipcode() ?> <?php echo $Order->getForwardingAddress()->getCity() ?></strong> -
	<?php echo Util::getState($Order->getForwardingAddress()->getIdState()); ?></p>
   
	<p style="margin-top:10px;"><?php if($Order->getForwardingAddress()->getPhonenumber()) { ?>
    	Tél. : <?php echo $Order->getForwardingAddress()->getPhonenumber(); ?>
    <?php } ?>
    <?php if($Order->getForwardingAddress()->getFaxnumber()) { ?>
    	- Fax : <?php echo $Order->getForwardingAddress()->getFaxnumber(); ?>
    <?php } ?></p>
    <div class="spacer"></div>
    <?php
    
    	$internal_delivery = DBUtil::getParameterAdmin( "internal_delivery" );
    	if( $Order->get( "idbuyer" ) == DBUtil::getParameterAdmin( "internal_customer" ) ){
    		
    		?>
    		<input type="hidden" name="delivery" value="<?php echo SupplierOrder::$INTERNAL_FORWARDING_ADDRESS; ?>" />
    		<?php
    		
    	}
    	else{
    		
	    	?>
		    <select name="delivery">
				<option value="<?php echo SupplierOrder::$INTERNAL_FORWARDING_ADDRESS ?>"<?php if( $Order->get( "iddelivery" ) == $internal_delivery ) echo " selected=\"selected\""; ?>>Dépôt interne</option>
				<option value="<?php echo SupplierOrder::$BUYER_FORWARDING_ADDRESS ?>"<?php if( $Order->get( "iddelivery" ) != $internal_delivery ) echo " selected=\"selected\""; ?>>Client</option>
			</select>
			<input <?=$disabled?> type="submit" class="blueButton" name="UpdateDelivery" value="<?php echo Dictionnary::translate("register"); ?>" />
			<?php
		
    	}
    	
    ?>
	<div class="spacer"></div>

	<?php
	
}
*/
//-------------------------------------------------------------------------------------------------------
/**
 * @param Customer $customer le client de l'affaire
 * @param Address $selectedAddress l'adresse de livraison actuelle
 * @return void
 */
function listForwardingAddresses( Customer $customer, Address $selectedAddress, $userInteraction = false ){
	global $Order;

	$internal_delivery 			= DBUtil::getParameterAdmin( "internal_delivery" );
	$internal_customer 			= DBUtil::getParameterAdmin( "internal_customer" );
	$internal_customer_company = DBUtil::getDBValue( "company", "buyer", "idbuyer", $internal_customer );

	if (!$userInteraction && $customer->getId() == $internal_customer) {
		$selectedAddress = new ForwardingAddress( intval( $internal_delivery ) );
	}
	
	?>
<h2>Adresse de livraison :</h2>
	<select name="iddelivery" onchange="setSupplierOrderForwardingAddress($(this).val());" onkeyup="this.onchange(event);">
		<optgroup label="Adresses de la société">
		<?php

			$rs = DBUtil::query( "SELECT iddelivery, company FROM delivery WHERE idbuyer = '$internal_customer' ORDER BY iddelivery ASC" );

			while( !$rs->EOF() ){
				
				?>
				<option<?php if( $selectedAddress instanceof ForwardingAddress && $selectedAddress->getId() == $rs->fields( "iddelivery" ) ) echo " selected=\"selected\""; ?> label="<?php echo htmlentities( $rs->fields( "company" ) ); ?>" value="<?php echo $rs->fields( "iddelivery" ); ?>">
					<?php echo htmlentities( $rs->fields( "company" ) ); ?>
				</option>
				<?php
				
				$rs->MoveNext();

			}

		?>
		</optgroup>
		<?php
		
			/* adresses clients ( sauf interne... ) @todo*/
		
			if( $customer->getId() != $internal_customer ){
				
				?>
				<optgroup label="Adresses client">
					<?php $label = strlen( $customer->get( "company" ) ) ? $customer->get( "company" ) : DBUtil::getDBValue( "title_1", "title", "idtitle", $customer->getContact()->getGender() ) . " " . $customer->getContact()->getFirstName() . " " . $customer->getContact()->getLastName(); ?>
					<option<?php if( $selectedAddress instanceof CustomerAddress ) echo " selected=\"selected\""; ?> label="<?php echo htmlentities( $label ); ?>" value="<?php echo $rs->fields( "iddelivery" ); ?>"><?php echo htmlentities( $label ); ?></option>
					<?php
					$query = "
							SELECT
								o.iddelivery, company, title, firstname, lastname
							FROM `order` o
							LEFT JOIN `delivery` d
								ON d.iddelivery = o.iddelivery
							WHERE o.idorder = " . $Order->getId();
					$rs = DBUtil::query( $query );
					if ($rs->RecordCount()) {
						$label = strlen( $rs->fields( "company" ) ) ? $rs->fields( "company" ) : DBUtil::getDBValue( "title_1", "title", "idtitle", $rs->fields( "title" ) ) . " " .$rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
					?>   
						<option label="<?php echo htmlentities( $label ); ?>" value="<?php echo $rs->fields( "iddelivery" ); ?>"><?php echo htmlentities( $label ); ?></option>
                    <?php
					}
                    ?>
					<?php
					
						$rs = DBUtil::query( 
						
							"SELECT d.iddelivery, d.company, d.firstname, d.lastname, COALESCE( t.title_1, '' ) AS gender 
							FROM delivery d
							LEFT JOIN title t ON t.idtitle = d.title
							WHERE idbuyer = '" . $customer->getId() . "' 
							ORDER BY iddelivery ASC" 
						
						);
						
						while( !$rs->EOF() ){
							
							$label = strlen( $rs->fields( "company" ) ) ? $rs->fields( "company" ) : $rs->fields( "gender" ) . " " . $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
							
							?>
							<option<?php if( $selectedAddress instanceof ForwardingAddress && $selectedAddress->getId() == $rs->fields( "iddelivery" ) ) echo " selected=\"selected\""; ?> label="<?php echo htmlentities( $customer->get( "company" ) ); ?>" value="<?php echo $rs->fields( "iddelivery" ); ?>"><?php echo htmlentities( $rs->fields( "company" ) ); ?></option>
							<?php
							
							$rs->MoveNext();
							
						}
						
				?>
				</optgroup>
				<?php
				
			}
			
	?>
	</select>
	<p><strong><?php echo htmlentities( $selectedAddress->getCompany() ); ?></strong></p>
	<p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $selectedAddress->getGender() ) . ' ' . $selectedAddress->getFirstName() . ' ' . $selectedAddress->getLastName(); ?></p>
	<p><?php echo $selectedAddress->getAddress(); ?>
	<?php 
	
		if( strlen( $selectedAddress->getAddress2() ) )
			echo " - " . $selectedAddress->getAddress2();
	?>
	</p>
	<p>
		<strong><?php echo $selectedAddress->getZipcode(); ?> <?php echo $selectedAddress->getCity(); ?></strong> - 
		<?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $selectedAddress->getIdState() ); ?>
	</p>
	<p style="margin-top:10px;">
	<?php 
	
	if( strlen( $selectedAddress->getPhonenumber() ) )
    	echo "Tél. : " . Util::phonenumberFormat( $selectedAddress->getPhonenumber() ); 
    	
   	if( strlen( $selectedAddress->getFaxnumber() ) ){
   		
   		if( strlen( $selectedAddress->getPhonenumber() ) )
   			echo " - ";
   			
    	echo "Fax : " . Util::phonenumberFormat( $selectedAddress->getFaxnumber() ); 
    	
   	}

   	if( strlen( $selectedAddress->getGSM() ) || strlen( $selectedAddress->getEmail() ) )
   		echo "<br />";
   		
	if( strlen( $selectedAddress->getGSM() ) )
    	echo "GSM : " . Util::phonenumberFormat( $selectedAddress->getGSM() ); 
    	
   	if( strlen( $selectedAddress->getEmail() ) ){
   		
   		if( strlen( $selectedAddress->getGSM() ) )
   			echo " - ";
   			
    	echo "Email : <a class=\"blueLink\" href=\"mailto:" . htmlentities( $selectedAddress->getEmail() ) . "\">" . htmlentities( $selectedAddress->getEmail() ) . "</a>"; 
    	
   	}
   	
	?>
	</p>
	<?php
	
}

//-------------------------------------------------------------------------------------------------------

?>