<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE COMMENTAIRES ++++++++++++++++++++++++++++++++++++++++++ -->
<?php GraphicFactory::table_start_infos('Commentaires');?>
		<br />
		<!-- Commentaires -->
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order_supplier/comments.htm.php</span>
<?php } ?>
	
		<table class="Comments_Table" width="100%">
		<tr>
			<td width="70%">
				<b>Commande fournisseur</b><br />
				<?php if ($Str_status=="standby" AND strlen($Order->getValue('comment'))==0){?>
<textarea name="comment" cols="100" rows="5"><?php  echo Dictionnary::translate("gest_com_comment_msg1") ; ?> :
- <?php  echo Dictionnary::translate("gest_com_comment_msg2") ; ?> <?=$Order->getSupplier('name')?>,
- <?php  echo Dictionnary::translate("gest_com_comment_msg3") ; ?>

- <?php  echo Dictionnary::translate("gest_com_comment_msg4") ; ?>
</textarea>  
				<?php }else{?>	
					<textarea name="comment" cols="100" rows="5"><?=$Order->getValue('comment'); ?></textarea>
				<?php } ?>
			</td>
			<td rowspan="2" valign="middle" align="center"><input type="submit" name="UpdComment" value="Modifier"></td>
		</tr>
		<tr>
			<td>
				<b>Bon de livraison</b><br />
				<textarea name="commentBL" cols="100" rows="5"><?=$Order->getValue('commentBL'); ?></textarea>
			</td>
		</tr>
		</table>	
<?php GraphicFactory::table_end()?>