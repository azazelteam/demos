<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE MONTANTS ++++++++++++++++++++++++++++++++++++++++++ -->
<?php 

anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateTotalDiscountAmount" ); 
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" ); 
anchor( "UpdateInstallmentRate" );
anchor( "UpdateInstallmentAmount" );
anchor( "UpdateTotalChargeHT" );

?>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order_supplier/amounts.htm.php</span>
<?php } ?>

<div class="contentResult" style="margin-bottom: 10px;">

	<h1 class="titleEstimate"><span class="textTitle">Montants et tarifications de la commande</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>       
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">			
			
		<div class="tableContainer">
			<table class="dataTable devisTable">
				<tr>
					<th rowspan="2" colspan="2">Remise sur facture</th>
					<th colspan="2">Coût Port</th>
					<!--<th rowspan="2">Port</th>-->
					<th colspan="2">Montant</th>
					<th rowspan="2" colspan="2">Remise pour<br/>paiement comptant</th>
					<th rowspan="2">Total HT</th>
					<th rowspan="2">Cde Fournisseur<br />assujettie<br />à la TVA</th>
					<th rowspan="2">Net à payer</th>
			
				</tr>
				<tr>
					<th>Vente<br />Fournisseur</th>
					<th>Achat<br />Transporteur</th>
					<th>Net HT</th>
					<th>HT + port</th>
				</tr>
				<tr>
					<td>
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" name="total_discount_rate" value="<?php echo Util::numberFormat( $total_discount_rate ) ?>" />&nbsp;%
						<input type="hidden" name="old_total_discount_rate" value="<?php echo Util::numberFormat( $total_discount_rate ) ?>" />
						<input type="hidden" name="UpdateTotalDiscountRate" value="ok" />
					</td>
					<td>
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" name="total_discount_amount" value="<?php echo Util::numberFormat( $total_discount_amount ) ?>" />&nbsp;&euro;
						<input type="hidden" name="old_total_discount_amount" value="<?php echo Util::numberFormat( $total_discount_amount ) ?>" />
						<input type="hidden" name="UpdateTotalDiscountAmount" value="ok" />
					</td>
					<td>
					<?php
					
						if( $Order->get( "internal_supply" ) ) /* réapprovisionnement */
							$charges = $Order->get( "total_charge_ht" );
						else{
							
							$rs =& DBUtil::query( "SELECT charges FROM order_charges WHERE idorder = '" . $Order->get( "idorder" ) . "' AND idsupplier = '" . $Order->get( "idsupplier" ) . "' LIMIT 1" );
							$charges = $rs->RecordCount() ? $rs->fields( "charges" ) : 0.0;
						
						}
						
						?>
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" name="charges" value="<?php echo Util::numberFormat( $charges ) ?>" />&nbsp;&euro;
					</td>
					<td>
					<?php
					
						/*if( $Order->get( "idsupplier" ) == DBUtil::getParameterAdmin( "internal_supplier" ) )
							echo "-";
						else */if( $Order->get( "internal_supply" ) ){ /* réapprovisionnement */
							
							?>
							<input class="textInput" style="width:40px;" name="internal_supplier_charges" value="<?php echo Util::numberFormat( $Order->get( "charge_carrier_sell" ) ) ?>" />&nbsp;&euro;
							<?php
							
						}
						else{

							$rs =& DBUtil::query( "SELECT internal_supplier_charges FROM order_charges WHERE idorder = '" . $Order->get( "idorder" ) . "' AND idsupplier = '" . $Order->get( "idsupplier" ) . "' LIMIT 1" );
							$internal_supplier_charges = $rs->RecordCount() ? $rs->fields( "internal_supplier_charges" ) : 0.0;
							
							?>
							<input class="textInput" style="width:40px;" name="internal_supplier_charges" value="<?php echo Util::numberFormat( $internal_supplier_charges ) ?>" />&nbsp;&euro;
							<?php
							
						}
						
					?>
					</td>
					<?php
					
						$articlesET = 0.0;
						$it = $Order->getItems()->iterator();
						while( $it->hasNext() )
							$articlesET += $it->next()->getTotalET();
						
					?>
					<td><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) ?></td>
					<td><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET() ) ?></td>
					<td>
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" type="text" name="billing_rate" value="<?php echo Util::numberFormat( $Order->get( "billing_rate" ) ) ?>">&nbsp;%
						<input type="hidden" name="old_billing_rate" value="<?php echo Util::numberFormat( $Order->get( "billing_rate" ) ) ?>">					
						<input type="hidden" name="UpdateBillingRate" value="ok" />
					</td>
					<td>
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" type="text" name="billing_amount" value="<?php echo Util::numberFormat( $Order->get( "billing_amount" ) ) ?>" />&nbsp;&euro;
						<input type="hidden" name="old_billing_amount" value="<?php echo Util::numberFormat( $Order->get( "billing_amount" ) ) ?>" />
						<input type="hidden" name="UpdateBillingAmount" value="ok" />
					</td>
					<td><strong style="color:red;"><?php echo Util::priceFormat( $total_ht_wb ) ?></strong></td>				
					<td style="text-align:center;">
					<?
					$yesOUno = $Order->get("no_tax_export");
					
					if($yesOUno ==  1 || $Order->get("charge_vat_rate") == 0){
					?>
						<input type="radio" <?php echo $disabled ?> name="useVAT" value="2" /> OUI
						<input type="radio" <?php echo $disabled ?> name="useVAT" checked="checked" value="1" /> NON
					<?
					}else{
					?>	
						<input type="radio" <?php echo $disabled ?> name="useVAT" checked="checked" value="2" /> OUI
						<input type="radio" <?php echo $disabled ?> name="useVAT" value="1" /> NON
					<?php } ?>
					</td>

					<?
					$rebate_value	= $Order->get( "rebate_value" );
					$order_ttcWB	= $Order->getTotalATI();
					
					if( $order_ttcWB > 0.0 )
						$rebate_rate	= $Order->get( "rebate_type" ) == 'rate' 	? $rebate_value : round( $rebate_value / $order_ttcWB * 100 , 2 ) ;
					else $rebate_rate = 0.0;
					
					$rebate_amount	= $Order->get( "rebate_type" ) == 'amount'	? $rebate_value : round( $order_ttcWB * $rebate_value / 100 , 2 ) ;
					?>

					<td><strong style="color:red;"><?php echo Util::priceFormat( $Order->getNetBill() ) ?></strong></td>
				</tr>
				<tr>
					<th colspan="5">Frais divers HT</th>
					<th colspan="6">Taxes diverses HT</th>				
				</tr>
				<tr>
					<td colspan="5">
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" name="other_charges" value="<?php echo Util::numberFormat( $Order->get( "other_charges" ) ) ?>" />&nbsp;&euro;
					</td>
					<td colspan="6">
						<input <?php echo $disabled ?> class="textInput" style="width:40px;" name="other_taxes" value="<?php echo Util::numberFormat( $Order->get( "other_taxes" ) ) ?>" />&nbsp;&euro;
					</td>
				</tr>
			</table>
			<input type="submit" style="float:right; margin-top:5px;" class="blueButton" name="updateOrder" value="Modifier" />
		</div>
		<div class="spacer"></div>
			
	</div></div>
	
        <!-- Infos sur les factures -->
		<?php
		$query = "
		SELECT DISTINCT bs.*
		FROM billing_supplier bs, billing_supplier_row bsr, bl_delivery bl
		WHERE bl.idorder_supplier = '" . $Order->get( "idorder_supplier" ) . "'
		AND bl.idbl_delivery = bsr.idbl_delivery
		AND bl.idsupplier = bsr.idsupplier
		AND bsr.billing_supplier = bs.billing_supplier
		AND bsr.idsupplier = bs.idsupplier
		GROUP BY bsr.billing_supplier, bsr.idsupplier
		ORDER BY proforma DESC";
		$rs =& DBUtil::query( $query );
		if( $rs->RecordCount() ){ ?>
			
			<h1 class="titleEstimate">
				<span class="textTitle">Informations sur les factures</span>
				<div class="spacer"></div>  
			</h1>
			<div class="blocEstimateResult" style="margin:0;"><div style="margin:0 5px 5px 5px;">
			<script type="text/javascript">
			<!--
		      
			    $(document).ready(function(){
			        $(".qtipa").each( function(){
			        	$(this).qtip({
				            adjust: {
				                mouse: true,
				                screen: true
				            },
							content: $(this).parent('td').find('.qtip').html(),
				            position: {
				                corner: {
				                    target: "rightMiddle",
				                    tooltip: "leftMiddle"
				                }
				            },
				            style: {
				                background: "#FFFFFF",
				                border: {
				                    color: "#EB6A0A",
				                    radius: 5
				                },
				                fontFamily: "Arial,Helvetica,sans-serif",
				                fontSize: "13px",
				                tip: true,
				                width: 'auto'
				            }
			        	});
			    	});
				});
			// -->
			</script>
			
			<table class="dataTable devisTable">
				<tr>
		       		<th colspan="2">Facture / Proforma n°</th>
		       		<th>Statut</th>
		       		<th>Date de facture</th>
		       		<th>Date enregistrement</th>
		       		<th>Date d'échéance</th>
			      	<th>Total TTC</th>
				</tr>
				<?php 
				$supplierInvoices = array();
				while( !$rs->EOF() ){ ?>
					<tr>
						<td><?php echo $rs->fields( "proforma" ) ? 'Proforma' : 'Facture' ?></td>
						<td><?php
							if( !$rs->fields( "proforma" ) ){
								//on cherche si la facture découle d'une proforma
								$proformasFacture = DBUtil::query( ' SELECT proforma_id FROM billing_proforma WHERE billing_supplier = "' . $rs->fields( "billing_supplier" ) .'"' );
								
								if( $proformasFacture->RecordCount() ){
									?>
									<a href="#" class="qtipa" style="font-weight:bold;color:#1E9CD1;" onclick="return false;"><?php echo htmlentities( $rs->fields( "billing_supplier" ) ) ?></a>
									<div class="qtip" style="display:none;">
										<span style="font-weight:bold">Facture issue <?php echo $proformasFacture->RecordCount() > 1 ? 'des' : 'de la' ?> proforma :</span>
										<ul style="list-style:none;">
										<?
										while( !$proformasFacture->EOF() ){
											?>
											<li><?php echo $proformasFacture->fields( 'proforma_id' ) ?></li>
											<?
											$proformasFacture->MoveNext();
										}
										?>
										</ul>
									</div>
									<?
								}else
									echo htmlentities( $rs->fields( "billing_supplier" ) );
							}else
								echo htmlentities( $rs->fields( "billing_supplier" ) );
							
						?></td>
						<td><?php echo Dictionnary::translate( $rs->fields( "status" ) ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "Date" ), false, "-" ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "creation_date" ), false, "-" ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "deliv_payment" ), false, "-" ) ?></td>
						<td><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
					</tr>
					<?php 
					$supplierInvoices[] = $rs->fields( "billing_supplier" );
					$rs->MoveNext(); 
				}
					
					/* autres commandes fournisseur concernées par ces factures */					
					$query = "
					SELECT bsr.idsupplier, GROUP_CONCAT( bl.idorder_supplier ) AS supplierOrders
					FROM billing_supplier_row bsr, bl_delivery bl
					WHERE bsr.idsupplier = '" . $Order->get( "idsupplier" ) . "'
					AND bsr.billing_supplier IN( '" . implode( "','", $supplierInvoices ) . "' )
					AND bsr.idbl_delivery = bl.idbl_delivery
					AND bl.idorder_supplier <> '" . $Order->getId() . "'
					GROUP BY bsr.idsupplier";
					
					$invoicedSupplierOrdersRS =& DBUtil::query( $query );
					if( $invoicedSupplierOrdersRS->RecordCount() ){ ?>
						
						<tr>
							<td colspan="6" style="color:red;">Attention, ces factures concernent également les commandes fournisseur suivantes : 
							<?php
							
								$invoicedSupplierOrderCount = 0;
								$invoicedSupplierOrders = explode( ",", $invoicedSupplierOrdersRS->fields( "supplierOrders" ) );
								
								foreach( $invoicedSupplierOrders as $invoicedSupplierOrder ){
									
									if( $invoicedSupplierOrderCount )
										echo ",&nbsp;";
										
									?>
									<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/supplier_order.php?IdOrder=<?php echo $invoicedSupplierOrder; ?>" onclick="window.open( this.href ); return false;"><?php echo $invoicedSupplierOrder; ?></a>
									<?php
									
									$invoicedSupplierOrderCount++;
										
								}
								
							?>
							</td>
						</tr>
						<?php
						
					}
					
			?>
			</table>
			</div></div>
			<div class="spacer"></div>	

		<?php } ?>
	
</div><!-- mainContent -->