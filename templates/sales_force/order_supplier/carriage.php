<?php

/* ---------------------------------------------------------------------------------- */

include_once( dirname( __FILE__ ) . "/../../../config/init.php" );
include_once( dirname( __FILE__ ) . "/../../../objects/Carriage.php" );
include_once( dirname( __FILE__ ) . "/../../../objects/Carrier.php" );
include_once( dirname( __FILE__ ) . "/../../../objects/ForwardingAddress.php" );
include_once( dirname( __FILE__ ) . "/../../../objects/SupplierFactoryAddress.php" );
	
/* ---------------------------------------------------------------------------------- */
/* mise à jour du transporteur */

if( isset( $_REQUEST[ "update_carrier" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$carriage = new Carriage(  $_REQUEST[ "idcarriage" ]  );
	$carriage->setDeliverer( $_REQUEST[ "idcarrier" ] ? new Carrier( $_REQUEST[ "idcarrier" ] ) : $carriage->getDeliveryNote()->getSupplier() );
	$carriage->save();
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	editCarriage( $carriage );
	exit();
	
}

/* ---------------------------------------------------------------------------------- */
/* mise à jour de l'adresse d'enlèvement */

if( isset( $_REQUEST[ "update_supplier_factory" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$carriage = new Carriage(  $_REQUEST[ "idcarriage" ]  );
	$carriage->setProvenance(  $_REQUEST[ "idsupplier_factory" ] ? new SupplierFactoryAddress(  $_REQUEST[ "idsupplier_factory" ]  ) : $carriage->getDeliveryNote()->getSupplier()->getAddress() );
	$carriage->save();
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	editCarriage( $carriage );
	exit();
	
}

/* ---------------------------------------------------------------------------------- */
/* mise à jour de l'adresse d'enlèvement */

if( isset( $_REQUEST[ "update_forwarding_address" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$carriage = new Carriage(  $_REQUEST[ "idcarriage" ]  );
	$carriage->setDestination(  $_REQUEST[ "iddelivery" ] ? new ForwardingAddress(  $_REQUEST[ "iddelivery" ]  ) : $carriage->getDeliveryNote()->getCustomer()->getAddress() );
	$carriage->save();
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	editCarriage( $carriage );
	exit();
	
}

/* ---------------------------------------------------------------------------------- */
/* mise à jour des commentaires */

if( isset( $_REQUEST[ "update_comment" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$carriage = new Carriage(  $_REQUEST[ "idcarriage" ] );
	$carriage->set( "comment", stripslashes( /*Util::doNothing( */$_REQUEST[ "comment" ]/* )*/ ) );
	$carriage->save();
	
	exit( "1" );
	
}

/* ---------------------------------------------------------------------------------- */

?>
<script type="text/javascript">
/* <![CDATA[ */

	/* ---------------------------------------------------------------------------------- */
	
	$( document ).ready( function(){ $( "#CarriageListContainer" ).tabs( { fxFade: true, fxSpeed: 'fast' } ); });

	/* ---------------------------------------------------------------------------------- */
	/* mise à jour du transporteur */
	
	function setCarrier( idcarriage, idcarrier ){

		var data = "idcarriage=" + idcarriage;
		data += "&idcarrier=" + idcarrier;
			
		$.ajax({
		 	
			url: "/templates/sales_force/order_supplier/carriage.php?update_carrier",
			async: true,
			data: data,
			success: function( responseText ){ $( "#Carriage" + idcarriage ).html( $( responseText ).html() ); }
	
		});
		
	}

	/* ---------------------------------------------------------------------------------- */
	/* mise à jour l'adresse d'enlèvement */
	
	function setDelivererAddress( idcarriage, idsupplier_factory ){

		var data = "idcarriage=" + idcarriage;
		data += "&idsupplier_factory=" + idsupplier_factory;
			
		$.ajax({
		 	
			url: "/templates/sales_force/order_supplier/carriage.php?update_supplier_factory",
			async: true,
			data: data,
			success: function( responseText ){ $( "#Carriage" + idcarriage ).html( $( responseText ).html() ); }
	
		});
		
	}
	
	/* ---------------------------------------------------------------------------------- */
	/* mise à jour de l'adresse de livraison */
	
	function setForwardingAddress( idcarriage, iddelivery ){

		var data = "idcarriage=" + idcarriage;
		data += "&iddelivery=" + iddelivery;
			
		$.ajax({
		 	
			url: "/templates/sales_force/order_supplier/carriage.php?update_forwarding_address",
			async: true,
			data: data,
			success: function( responseText ){ $( "#Carriage" + idcarriage ).html( $( responseText ).html() ); }
	
		});
		
	}

	/* ---------------------------------------------------------------------------------- */
	
	function deliveryOrder( idcarriage ){

		var data = "idcarriage=" + idcarriage;
		data += "&comment=" + $( "#carriage_comment" + idcarriage ).val();
		
		$.ajax({
		 	
			url: "/templates/sales_force/order_supplier/carriage.php?update_comment",
			async: true,
			data: data,
			success: function( responseText ){

				if( responseText != "1" ){

					alert( "Impossible de sauvegarder les commentaires" );
					return;

				}

				document.location='/sales_force/mail_delivery_order.php?idcarriage=' + idcarriage;

			}
	
		});

	}
	
/* ]]> */
</script>
<a name="carriages"></a>
<div class="contentResult" style="margin-bottom: 10px;">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order_supplier/carriage.php</span>
<?php } ?>

	<h1 class="titleEstimate">
		<span class="textTitle">Transport</span>
		<div class="spacer"></div>
	</h1>
	<div class="spacer"></div>
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;">
		<div id="CarriageListContainer" style="margin:5px;">
			<ul class="menu">
			<?php
			
				$query = "
				SELECT c.idcarriage, bl.idbl_delivery
				FROM carriage c, bl_delivery bl
				WHERE bl.idorder_supplier = '" . $Order->getId() . "'
				AND c.idcarriage = bl.idbl_delivery
				ORDER BY bl.idbl_delivery ASC";
				
				$rs =& DBUtil::query( $query );
				
				while( !$rs->EOF() ){
				
					?>
					<li class="item"><a href="#Carriage<?php echo $rs->fields( "idcarriage" ); ?>"><span>OE n° <?php echo $rs->fields( "idcarriage" ); ?></span></a></li>
					<?php
					
					$rs->MoveNext();
					
				}
				
				?>
			</ul>
			<?php
			
				$rs->Move( 0 );
								
				while( !$rs->EOF() ){
						//editCarriage( new Carriage( $rs->fields( "idcarriage" ) ) );
					$rs->MoveNext();
					
				}
				
			?>
			<div class="spacer"></div>
		</div>
	</div><!-- blocEstimateResult -->
</div><!-- contentResult -->
<?php

/* ------------------------------------------------------------------------------------------ */

function editCarriage( Carriage $carriage ){

	$internal_delivery = DBUtil::getParameterAdmin( "internal_delivery" );

	?>
	<div id="Carriage<?php echo $carriage->getId(); ?>" style="border:2px dotted #E7E7E7; padding:10px;">
		<div class="spacer"></div>
		<p>
			<label><span><b>Transport assuré par :</b></span></label>
			<select onchange="setCarrier(<?php echo $carriage->getId(); ?>,$(this).val());">
				<option value="0"><?php echo htmlentities( $carriage->getDeliveryNote()->getSupplier()->get( "name" ) ); ?></option>
				<?php
				
				$rs =& DBUtil::query( "SELECT c.idcarrier, p.name FROM carrier c, provider p WHERE p.idprovider = c.idprovider ORDER BY p.name ASC" );
    			
    			while( !$rs->EOF() ){
    				
    				$selected = $carriage->getDeliverer() instanceof Carrier && $carriage->getDeliverer()->getId() == $rs->fields( "idcarrier" ) ? " selected=\"selected\"" : "";
    				
    				?>
    				<option<?php echo $selected; ?> value="<?php echo $rs->fields( "idcarrier" ); ?>"><?php echo htmlentities( $rs->fields( "name" ) ); ?></option>
    				<?php
    				
    				$rs->MoveNext();
    					
    			}
					
				?>
			</select>
		</p>
		<div class="blocMultiple3" style="background:#EEEEEE; border:1px solid #EEEEEE; width:30%; padding:0 1% 1% 1%; margin-top:10px; height:150px;">
			<h2>Adresse d'enlèvement :</h2>
			<select onchange="setDelivererAddress(<?php echo $carriage->getId(); ?>, $(this).val());">
				<option value="0">Adresse principale Fournisseur</option>
				<?php
				
					$it = $carriage->getDeliveryNote()->getSupplier()->getFactories()->iterator();
					while( $it->hasNext() ){

						$factory = $it->next();
						$selected = $carriage->getProvenance() instanceof SupplierFactoryAddress && $carriage->getProvenance()->getId() == $factory->getId() ? " selected=\"selected\"" : "";
						
						?>
						<option<?php echo $selected; ?> value="<?php echo $factory->getId(); ?>"><?php echo htmlentities( $factory->getCompany() ); ?></option>
						<?php
						
					}

			?>
			</select>
			<?php displayAddress( $carriage->getProvenance() ); ?>
		</div><!-- blocMultiple3 -->
		<div class="blocMultiple3" style="background:#EEEEEE; border:1px solid #EEEEEE; width:30%; padding:0 1% 1% 1%; margin-top:10px; height:150px;">
			<h2>Adresse de livraison :</h2>
			<select onchange="setForwardingAddress(<?php echo $carriage->getId(); ?>,$(this).val());">
				<option value="0"<?php if( $carriage->getDestination() instanceof CustomerAddress ) echo " selected=\"selected\""; ?>>Adresse principale Client</option>
				<?php
				
					$rs = DBUtil::query( "SELECT iddelivery, company FROM delivery WHERE idbuyer = '" . $carriage->getDeliveryNote()->getCustomer()->getId() . "' ORDER BY company ASC" );
				
					while( !$rs->EOF() ){

						$selected = $carriage->getDestination() instanceof ForwardingAddress && $carriage->getDestination()->getId() == $rs->fields( "iddelivery" ) ? " selected=\"selected\"" : "";
						
						?>
						<option<?php echo $selected; ?> value="<?php echo $rs->fields( "iddelivery" ); ?>"><?php echo htmlentities( $rs->fields( "company" ) ); ?></option>
						<?php
						
						$rs->MoveNext();
						
					}
					
				?>
				<option<?php if( $carriage->getDestination() instanceof ForwardingAddress && $carriage->getDestination()->getId() == $internal_delivery ) echo " selected=\"selected\""; ?> value="<?php echo $internal_delivery; ?>">Dépôt interne</option>
			</select>
			<?php displayAddress( $carriage->getDestination() ); ?>
		</div><!-- blocMultiple3 -->
		<div class="blocMultiple3" style="background:#EEEEEE; border:1px solid #EEEEEE; width:30%; padding:0 1% 1% 1%; margin-top:10px; height:150px;">
			<h2>Colisage :</h2>
			<label><span>todo</span></label>
		</div><!-- blocMultiple3 -->
		<div class="spacer"></div>
		<p style="vertical-align:top; margin:10px 0px;">
			<b>Commentaires :</b>
			<br />
			<textarea name="carriage_comment<?php echo $carriage->getId(); ?>" id="carriage_comment<?php echo $carriage->getId(); ?>" style="width:100%; height:120px; border:1px solid #E7E7E7;"><?php echo htmlentities( $carriage->get( "comment" ) ); ?></textarea>
		</p>
		<p style="float:right;">
			<input type="button" onclick="deliveryOrder(<?php echo $carriage->getId(); ?>);" name="" value="Ordre d'enlèvement" class="buttonValid buttonValidMail"<?php if( $carriage->getDeliverer() instanceof Supplier ) echo " disabled=\"disabled\""; ?> />
			<?php
			
				include_once( dirname( __FILE__ ) . "/../../../objects/Util.php" );
			
				if( $carriage->get( "delivery_order_last_send_date" ) != "0000-00-00 00:00:00" )
					echo "<span style=\"font-weight:bold; display:block;\">Dernière date d'envoi : le " . Util::dateFormat( $carriage->get( "delivery_order_last_send_date" ), "d/m/Y" ) . "</span>";
					
		?>
		</p>
		<br style="clear:both;" />
	</div><!-- Carriage -->
	<?php
	
}

/* ------------------------------------------------------------------------------------------ */

function displayAddress( Address $address ){

	?>
	<p><span>
	<?php 
			
		echo DBUtil::getDBValue( "title_1", "title", "idtitle", $address->getGender() ) .
		" " . htmlentities( $address->getFirstName() ) .
		" " . htmlentities( $address->getLastName() );
			
	?>
	</span></p>
	<p><span><?php echo htmlentities( $address->getAddress() ); ?></span></p>
	<p><span><?php echo htmlentities( $address->getAddress2() ); ?></span></p>
	<p><span><b>
	<?php 
		
		echo htmlentities( $address->getZipcode() ) .
		" " . htmlentities( $address->getCity() ) .
		" - " . htmlentities( DBUtil::getDBValue( "name_1", "state", "idstate", $address->getIdState() ) );
			
	?>
	</b></span></p>
	<?php
	
}

/* ------------------------------------------------------------------------------------------ */

?>