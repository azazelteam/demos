<?php
/** 
 * Package gestion commerciale
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Commandes fournisseurs
 */	

include_once( "../config/init.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );
/* identification requise */

if( !User::getInstance()->getId() ){
	
	include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	exit();
	
}

// initialize variables
$internal_delivery = DBUtil::getParameterAdmin( "internal_delivery" );

//------------------------------------------------------------------------------------------
//Added By ParsMedia
		// ------- Mise à jour de l'id gestion des Index  #1161
if(isset($_POST['SaveOrder'])){
	$IdOrder = $_REQUEST['IdOrder'];
	$crossdocking = $_REQUEST['crossdocking'];
	$delivery_address_id = $_REQUEST['delivery_address_id'];
	if($delivery_address_id <> ''){
		//echo 'case1';
		$query = "
			UPDATE `order_supplier`
			SET `iddelivery` = '$delivery_address_id' 
			WHERE `idorder_supplier` = '$IdOrder';";
		$rs =& DBUtil::query( $query );
		// echo $query;
		$query = "
			UPDATE `bl_delivery`
			SET `iddelivery` = '$delivery_address_id' 
			WHERE `idorder_supplier` = '$IdOrder';";
		//echo $query;
		$rs =& DBUtil::query( $query );
	}	
	if($crossdocking == '1'){
		//echo 'case2';
		$query = "
			UPDATE `order_supplier`
			SET `iddelivery` = '$internal_delivery' 
			WHERE `idorder_supplier` = '$IdOrder';";
		$rs =& DBUtil::query( $query );
		//echo $query;
		$query = "
			UPDATE `bl_delivery`
			SET `iddelivery` = '$internal_delivery' 
			WHERE `idorder_supplier` = '$IdOrder';";
		$rs =& DBUtil::query( $query );
		// echo $query;
	}
	$query = "
		UPDATE `order_supplier`
		SET `cross-docking` = '$crossdocking' 
		WHERE `idorder_supplier` = '$IdOrder'";
	$rs =& DBUtil::query( $query );

}

//------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------
//Added By ParsMedia
/* 
* Tooltip Content show 
*/
if( isset( $_GET[ 'showTooltip' ] ) && $_GET[ 'idorder_supplier' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( showTooltipSupplier( $_GET[ 'idorder_supplier' ] ) );

}

//------------------------------------------------------------------------------------------


if( isset( $_REQUEST[ "idbl_delivery" ] ) ){
	
	header( "Location: /sales_force/supplier_order.php?IdOrder=" .  DBUtil::getDBValue( "idorder_supplier", "bl_delivery", "idbl_delivery", $_REQUEST[ "idbl_delivery" ] ) ) );
	exit();
	
}

//------------------------------------------------------------------------------------------
/* Traitement Ajax du changement de statut 1ere phase*/

if( isset( $_GET[ "changestatus" ] ) ){
	
	header('Content-Type: text/html; charset=UTF-8'); 
	// ------- Mise à jour de l'id gestion des Index  #1161
	if( !isset( $_GET[ "IdOrder" ] ) )
		exit( "False important parameter 'idorder'" );
	
	if( !isset( $_GET[ "status" ] ) )
		exit( "Missing important parameter 'status'" );
	
		
	include_once( "../objects/DHTMLCalendar.php" );
	include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
		
	$status = $_GET[ "status" ];
	
	$blCoucount = $_GET[ "blCount" ];
	
	$resTxt = "";
	
	$IdOrder = $_REQUEST[ "IdOrder" ];	
	$Order = new SupplierOrder( $IdOrder );
		
	$conf_date = Util::dateFormatEu( $Order->get( "confirmation_date" ) );
	if( $conf_date == "00-00-0000" ){
		$conf_date = "";
	}
	
	$disp_date 		= DBUtil::getDBValue( "dispatch_date", "bl_delivery", "idorder_supplier", $Order->get( "idorder_supplier" ) );
	$avail_date 	= DBUtil::getDBValue( "availability_date", "bl_delivery", "idorder_supplier", $Order->get( "idorder_supplier" ) );
	
	if( $avail_date == "00-00-0000" ){
		$avail_date = "";
	}else{
		$avail_date = Util::dateFormatEu( $avail_date );
	}
	
	if( $disp_date == "00-00-0000" ){
		$disp_date = "";
	}else{
		$disp_date = Util::dateFormatEu( $disp_date );
	}
	
	if($status=='confirmed' OR $status=='dispatch' OR $status=='received' OR $status=='invoiced' ){
		$resTxt = "<label style='width:100px;'><span>Date réception</span></label>
		<input class=\"calendarInput\" style='width:70px;' type=\"text\" name=\"confirmation_date\" id=\"confirmation_date\" value=\"".$conf_date."\" />".
		DHTMLCalendar::calendar( "confirmation_date" )."
		<label style='width:200px; margin-left:8px;'><span>Date confirmée par fournisseur</span></label>";		
		
		if( $blCoucount > 1 ){
			$resTxt .= "<span class=\"textidSimpleMin\" style=\"border:none;\">".Dictionnary::translate( "gest_com_partial_deliveries" )."</span>";
		}else{
			$resTxt .= "<input style='width:70px;' class=\"calendarInput\" type=\"text\" name=\"availability_date\" id=\"availability_date\"  value=\"".$avail_date."\" />
				".DHTMLCalendar::calendar( "availability_date" );
		}
	}
	
	if($status=='confirmed'){
		if($dispatch_date!='00-00-0000'){
			$resTxt = "<label style='width:100px; margin-left:8px;'><span>Date d'expédition</span></label>";
			if( $blCount > 1 ){
				$resTxt .= "<span class=\"textidSimpleMin\" style=\"border:none; width:200px;\">".Dictionnary::translate( "gest_com_partial_deliveries" ) ."</span>";
			}else{
				$resTxt .= $dispatch_date;
			}
		}
	}
	
	if($status=='dispatch' OR $status=='received' OR $status=='invoiced'){
		$resTxt .= "<label style='width:140px; margin-left:8px;'><span>Date expédition réelle</span></label>";			
		if( $blCoucount > 1 ){
			$resTxt .= "<span class=\"textidSimpleMin\" style=\"border:none; width:200px;\">".Dictionnary::translate( "gest_com_partial_deliveries" )."</span>";
		}else{
			$resTxt .= "<input style='width:70px;' class=\"calendarInput\" type=\"text\" name=\"dispatch_date\" id=\"dispatch_date\" value=\"".$disp_date."\"  />
				".DHTMLCalendar::calendar( "dispatch_date" );
		}
	}
	// ------- Mise à jour de l'id gestion des Index  #1161
	if( $status=='confirmed' OR $status=='dispatch' OR $status=='received' OR $status=='received' OR $status=='invoiced'){
		$resTxt .= "<input type=\"button\" onClick=\"updateMyStatus('".$_GET[ "IdOrder" ]."',document.getElementById('orderSupplierStatus').options[document.getElementById('orderSupplierStatus').selectedIndex].value);\" class=\"blueButton floatright\" value=\"Modifier\" name=\"UpdateMiscInfos\" style=\"margin-top:5px; margin-left:8px;\"/>";
	}else{
		$resTxt .= "<input type=\"button\" onClick=\"updateMyStatus('".$_GET[ "IdOrder" ]."',document.getElementById('orderSupplierStatus').options[document.getElementById('orderSupplierStatus').selectedIndex].value);\" class=\"blueButton floatright\" value=\"Modifier\" name=\"UpdateMiscInfos\" style=\"margin-top:5px; margin-left:8px;\"/>";
	}

	echo $resTxt ;
	exit;
	
}


//------------------------------------------------------------------------------------------
/* Traitement Ajax du changement de statut 2eme phase l'enregistrement*/
if( isset( $_GET[ "updStatus" ] ) ){
	
	header('Content-Type: text/html; charset=UTF-8'); 
	// ------- Mise à jour de l'id gestion des Index  #1161
	if( !isset( $_GET[ "IdOrder" ] ) )
		exit( "False important parameter 'idorder'" );
	
	if( !isset( $_GET[ "status" ] ) )
		exit( "Missing important parameter 'status'" );
	
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );
	include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
	include_once( "$GLOBAL_START_PATH/objects/TradeFactory.php" );
	include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
	$status = $_GET[ "status" ];
	
	$verifStatus = DBUtil::getDBValue( "order_supplier_status", "order_supplier_status", "order_supplier_status", $status );

	if(!$verifStatus && $status != 'annuler')
		exit("Statut inconnu");
		
	$IdOrder = $_REQUEST[ "IdOrder" ];	
	$Order = new SupplierOrder( $IdOrder );

	if($status == 'annuler'){
		$Order->set( "status" , 'received' );
		$Order->set( "availability_date", "" );
		$Order->set( "dispatch_date", "" );
	
		$query = "UPDATE `bl_delivery` 
			   	  SET dispatch_date='0000-00-00' 
				  WHERE idorder_supplier = '" . $Order->get( "idorder_supplier" ) . "'";
					
		$rs =& DBUtil::query( $query );
		
		$query = "UPDATE `bl_delivery` 
				  SET availability_date = '0000-00-00'
				  WHERE idorder_supplier = '" . $Order->get( "idorder_supplier" ) . "'";
			
		$rs =& DBUtil::query( $query );		
						
		$Order->save();
		
		exit("success");
	}

	if( $status != 'received' && $status != 'confirmed' && $status != 'invoiced' && $status != 'dispatch' ){
		$Order->set( "status" , $status );	
		exit("success");
	}
	
	if( $status == 'confirmed'  && (!isset( $_GET[ "confirmation_date" ] ) || empty( $_GET[ "confirmation_date" ] ) ) )
		exit("Veuillez entrer une date de confirmation");
		
	if( $status == 'dispatch'  && (!isset( $_GET[ "dispatch_date" ] ) || empty( $_GET[ "dispatch_date" ] ) || $_GET[ "dispatch_date" ] == "00-00-0000" ) )
		exit("Veuillez entrer une date d'expédition");
	
	
	
	
	if( $status == 'confirmed' || $status == 'invoiced'){
		if( !empty( $_GET[ "confirmation_date" ] ) ){
			$confirmation_date = explode( "-", $_GET[ "confirmation_date" ] );
			
			if( count( $confirmation_date ) == 3 ){
			
				list( $day, $month, $year ) = $confirmation_date;
				
				if( checkdate( $month, $day, $year ) )
					$Order->set( "confirmation_date" , "$year-$month-$day" );
				else
					exit("Veuillez entrer une date de confirmation correcte !");
			}else{
				exit("Veuillez entrer une date de confirmation correcte !");
			}
		}else{
			$Order->set( "confirmation_date" , "0000-00-00" );
		}
	}
	
	
	$dispatch_date = false ;
	if( $status == 'dispatch' || $status == 'invoiced' ){
		
		$tmp = explode( "-", $_GET[ "dispatch_date" ] );
		
		if( count( $tmp ) == 3 ){
		
			list( $day, $month, $year ) = $tmp;
			
			if( checkdate( $month, $day, $year ) ){
				
					$dispatch_date =  "$year-$month-$day";
					
					$Order->set( "dispatch_date", $dispatch_date );
					
					$query = "
					UPDATE `bl_delivery` 
					SET dispatch_date='$dispatch_date' 
					WHERE idorder_supplier = '" . $Order->get( "idorder_supplier" ) . "'";
					
					$rs =& DBUtil::query( $query );
		
			}
			else exit("Veuillez entrer une date d'expédition !");
			
		}else{
			exit("Veuillez entrer une date d'expédition !");
		}
	}

	$availability_date = false;
	if(isset( $_GET[ "availability_date" ]) && $_GET[ "availability_date" ] && $_GET[ "availability_date" ]!= "00-00-0000"){
	
		$tmp = explode( "-", $_GET[ "availability_date" ] );
		
		if( count( $tmp ) == 3 ){
		
			list( $day, $month, $year ) = $tmp;
			
			if( checkdate( $month, $day, $year ) )
					$availability_date =  "$year-$month-$day";
			else 	$availability_date = false;
			
		}
		
		if( $availability_date == false && $dispatch_date != false )
			$availability_date = $dispatch_date;
		
		if( $availability_date != false ){
		
			$Order->set( "availability_date", $availability_date );
			
			$query = "
			UPDATE `bl_delivery` 
			SET availability_date = '$availability_date'
			WHERE idorder_supplier = '" . $Order->get( "idorder_supplier" ) . "'";
			
			$rs =& DBUtil::query( $query );
			
		}else{
			exit("Veuillez entrer une date de disponibilité correcte !");
		}
		
	}else{
		if($dispatch_date){
			$availability_date = $dispatch_date;
			if( $availability_date != false ){
			
				$Order->set( "availability_date", $availability_date );
				
				$query = "
				UPDATE `bl_delivery` 
				SET availability_date = '$availability_date'
				WHERE idorder_supplier = '" . $Order->get( "idorder_supplier" ) . "'";
				
				$rs =& DBUtil::query( $query );
			}
		}
	}


	$Order->set( "status" , $status );	
	
	$Order->save();
	
	exit("success");

}

//------------------------------------------------------------------------------------------
// Traitement Ajax de la commande client
if( isset( $_GET[ "ChangeIdOrder" ] ) && isset( $_GET[ "IdOrderBuyer" ] ) ){
	
	header('Content-Type: text/html; charset=UTF-8'); 

	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );
	include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
	include_once( "$GLOBAL_START_PATH/objects/TradeFactory.php" );
	include_once( "$GLOBAL_START_PATH/sales_force/order_supplier_parseform.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
	if( !isset( $_GET[ "IdOrder" ] ) )
		exit( "False important parameter 'idorder'" );
				
	$IdOrder = $_GET[ "IdOrder" ];	
	$Order = new SupplierOrder( $IdOrder );

	$verifIdOrderBuyer = DBUtil::getDBValue( "idorder", "order", "idorder", $_GET[ "IdOrderBuyer" ] );
	if($verifIdOrderBuyer && $_GET[ "IdOrderBuyer" ] > 0){
		
		$Order->set( "idorder" , $_GET[ "IdOrderBuyer" ] );
		ModifyOrder( $Order );

		$query = "
		UPDATE `bl_delivery` 
		SET idorder='".$_GET[ "IdOrderBuyer" ]."' 
		WHERE idorder_supplier = '" . $IdOrder . "'";
		
		$rs =& DBUtil::query( $query );
							
		$message = 'success';
	}else{
		$message = 'La commande client '.$_GET[ "IdOrderBuyer" ] .' n\'existe pas';
	}
	
	exit($message);
}

//------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/objects/TradeFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/sales_force/order_supplier_parseform.php" );

//---------------------------------------------------------------------------------
//verification si c'est possible d'afficher quelque chose

if( isset( $_REQUEST['IdOrder'])){

	//compte pour voir si ya une cde de ce num
	$idordera=$_REQUEST['IdOrder'] ;
	$reqverif="SELECT COUNT(idorder_supplier) as comptage FROM `order_supplier` WHERE idorder_supplier=$idordera";
	
	$baseverif = DBUtil::getConnection();
	$resverif = $baseverif->Execute( $reqverif );

	$Title = 'Commande n°'.$idordera;
	
	if($resverif->fields("comptage")==0){
		include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
		$html="<div style='	text-align:center; color:#FF0000; font-size:18px; font-weight:bold;'>".Dictionnary::translate("this_number_dont_exist")."</div>";
		die( $html );
	}
}

//---------------------------------------------------------------------------------------------
/* mise à jour de la commande - ajax */

if( isset( $_REQUEST[ "update" ] ) 
	&& isset( $_REQUEST[ "idorder_supplier" ] ) 
	&& isset( $_REQUEST[ "property" ] )
	&& isset( $_REQUEST[ "value" ] ) ){
	
	$supplierOrder = new SupplierOrder(  $_REQUEST[ "idorder_supplier" ]  );
	
	$supplierOrder->set( $_REQUEST[ "property" ], stripslashes( $_REQUEST[ "value" ] ) );
	
	$supplierOrder->save();

	exit( "1" );
		
}

//---------------------------------------------------------------------------------
//chargement/création de la commande

if( isset( $_REQUEST[ "IdOrder" ] ) ){
	
	$IdOrder = $_REQUEST[ "IdOrder" ];	
	$Order = new SupplierOrder( $IdOrder );

}
else if( isset( $_GET[ "idsupplier" ] ) ){

	//réapprovisionnement => créer une commande fournisseur vierge et un BL associé
	
	$idsupplier =  $_GET[ "idsupplier" ] ;
	$idbuyer	=  DBUtil::getParameterAdmin( "internal_customer" ) ;
	$idcontact 	= isset( $_GET[ "idcontact" ] ) ? intval( $_GET[ "idcontact" ] ) : 0;
	
	if( !$idbuyer || !DBUtil::getDBValue( "idbuyer", "buyer", "idbuyer", $idbuyer ) )
		trigger_error( "Impossible de récupérer les données du client interne", E_USER_ERROR );

	$Order 		= TradeFactory::createSupplierOrder( $idsupplier, $idbuyer, $idcontact );
	$IdOrder 	= $Order->get( "idorder_supplier" );
	
	$deliveryNote = TradeFactory::createDeliveryNoteFromSupplierOrder( $Order );
	$idbl_delivery = $deliveryNote->get( "idbl_delivery" );
	
	$Order->set( "bl", $deliveryNote->get( "idbl_delivery" ) ); //la putain de jointure inversée qui sert à rien @todo ALTER TABLE `order_supplier` DROP `bl`
	$Order->set( "internal_supply", 1 );
	
	$Order->save();
	
	DBUtil::query( "UPDATE `order_supplier` SET bl = '$idbl_delivery' WHERE idorder_supplier = '$IdOrder' LIMIT 1" ); //la putain de jointure inversée qui sert à rien @todo ALTER TABLE `order_supplier` DROP `bl`
	
	// Mise à jour de iddelivery
	$query = "
		UPDATE `order_supplier`
		SET `iddelivery` = '$internal_delivery' 
		WHERE `idorder_supplier` = '$IdOrder';";
	$rs =& DBUtil::query( $query );
	$query = "
		UPDATE `bl_delivery`
		SET `iddelivery` = '$internal_delivery' 
		WHERE `idorder_supplier` = '$IdOrder';";
	$rs =& DBUtil::query( $query );
	
}
else die( "Missing parameter" );

//---------------------------------------------------------------------------------
//droits de consultation

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

if( !$hasAdminPrivileges && $iduser ){
	
	$idbuyer = $Order->get( "idbuyer" );
	
	$userCommande = DBUtil::getDBValue('iduser','order_supplier','idorder_supplier',$_REQUEST['IdOrder']);
	$buyerCommande = DBUtil::getDBValue('idbuyer','order_supplier','idorder_supplier',$_REQUEST['IdOrder']);
	$buyerCommercial = DBUtil::getDBValue('iduser','buyer','idbuyer',$buyerCommande);
				
	if( $userCommande != $iduser && $iduser != $buyerCommercial ) 
		die( Dictionnary::translate( "gest_com_no_right_to_view_order" ) );
		
}

//------------------------------------------------------------------------------------------
/* annulation de commande */

if( isset( $_GET[ "cancel" ] ) ){
	
	$Order->cancel();
	
	if( isset( $_GET[ "send" ] ) )
		if( !sendSupplierOrderCancellation( $Order ) )
			trigger_error( "impossible d'envoyer la demande d'annulation de commande fournisseur n° " . $Order->getId(), E_USER_ERROR );
	
}

//---------------------------------------------------------------------------------
//Gestion de l'envoi du mail au client pour la livraison

if( isset( $_POST[ "mail_manu" ] ) && $_POST["mail_manu"]==1 ) {

	if(  strcmp($Order->get("dispatch_date"), date("Y-m-d") ) < 0 ||  strcmp($Order->get("dispatch_date"), date("Y-m-d") ) == 0 ){
		
		//echo "test : ".strcmp($Order->get("dispatch_date"), date("Y-m-d") );
		
		include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
		//-----------------------------------------------------------------------------------------
		$iduser = User::getInstance()->getId();
	
		$AdminName= DBUtil::getParameterAdmin( "ad_name" );
	
		$CommercialName= User::getInstance()->get('firstname').' '.User::getInstance()->get('lastname');
		$CommercialEmail = User::getInstance()->get('email');
	
		$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );
	
		//-----------------------------------------------------------------------------------------

		//charger le modèle de mail

		include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
		$editor = new MailTemplateEditor();
	
		$editor->setLanguage( $Order->getCustomer()->get( "id_lang" ) ); //@todo : imposer la langue du destinataire
		$editor->setTemplate( "DELIVERY_IMMINENT" );
	
		$editor->setUseOrderTags( $Order->get( "idorder" ) );
		$editor->setUseSupplierOrderTags( $Order->get( "idorder_supplier") );
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( $Order->getCustomer()->get( "idbuyer" ), $Order->getCustomer()->getContact()->getId() );
		$editor->setUseCommercialTags( $iduser );
	
		$Message = $editor->unTagBody();
		$Subject = $editor->unTagSubject();
	
		//-----------------------------------------------------------------------------------------
		$mailer = new htmlMimeMail();

		$mailer->setFrom( '"'.$AdminName.' - '.$CommercialName.'" <'.$CommercialEmail.'>');
	
		$mailer->setSubject( $Subject );
		$mailer->setHtml( $Message );
	
		$result_b = $mailer->send( array( $BuyerEmail ),"smtp" );
	
		if($result_b){
		
			$Order->set("delivery_mail_date", date("Y-m-d"));
			$Order->save();
		
		}
		//----------------------------------------------------------------------------------------------

		//envoie d'une copie au commercial
		
		if( DBUtil::getParameterAdmin( "cc_salesman" ) ){
			
			$mailer->setSubject( "(" . Dictionnary::translate( "copy" ) . ") $Subject" );
			$result_c = $mailer->send( array( $CommercialEmail ),"smtp" );
			
		}
		
	}
	
}





//---------------------------------------------------------------------------------

if( isset( $_POST[ "DeleteOrder"] ) || isset( $_POST[ "DeleteOrder_x"] ) ){
	
	$query = "
	SELECT COUNT(bb.*) AS disallowDeletion
	FROM billing_buyer bb, supplier_order so
	WHERE so.idorder = bb.idorder
	AND so.idorder_supplier = '" .  $_REQUEST[ "IdOrder" ]  . "'";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->fields( "disallowDeletion" ) ){
		
		SupplierOrder::delete( $_REQUEST[ "IdOrder" ]  );
		header( "Location: com_admin_search_order_supplier.php" );
	
	}
	else trigger_error( "ouch! Invoiced order deletion allowed", E_USER_ERROR );
	
}

if( isset( $_POST[ "ModifyOrder" ] ) )
	ModifyOrder( $Order );
	
//---------------------------------------------------------------------------------------------

/**
 * Ajout de produits
 */

if( isset($_GET['IdProduct']) ){ //@todo : idarticle et pas IdProduct!{
	
	/*
	 * Note : l'ajout d'une référence d'un fournisseur différent ne fonctionnera
	 * pas lors du réapprovisionnement. ( cf. SupplierOrder::addArticle( $idarticle ) )
	 */
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );

	$IdOrder = $Order->get( "idorder" );
	$Order->addArticle( new CArticle( $_GET['IdProduct'] ) );
	$Order->save();
	
	//réapprovisionnement : synchronisation du BL associé à la commande fournisseur
		
	if( !$Order->get( "idorder" ) )
		TradeFactory::synchronizeDeliveryNoteFromSupplierOrder( $Order );
			
}

/**
 * By Behzad
 * Add grouping Order :addGroupingOrder
 */

if( isset($_GET['addGroupingOrder']) ){ 
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	
	$IdOrder = $Order->getId();
	$IdOrderChild = $_GET['IdOrderChild'];
	$childOrder = new SupplierOrder( $IdOrderChild );

	$iterator = $childOrder->getItems()->iterator();
	while( $iterator->hasNext() ) {
		$supplierOrderItem =& $iterator->next();
		$Order->addArticle(
			new CArticle( $supplierOrderItem->get( "idarticle" ) ),
			$supplierOrderItem->get( "quantity" ),
			$IdOrderChild
		);
		$Order->save();		
	}
	$childOrder->set( "idorder_supplier_parent",  $IdOrder );
	$childOrder->save();
		
	/*
	$query = "
		UPDATE
			`order_supplier_row`
		SET
			quantity = 0
		WHERE
			`idorder_supplier` = $IdOrder
			AND (idorder_supplier_original IS NULL OR idorder_supplier_original = 0)";
	DBUtil::query( $query );
	*/
	
	/*
	$query = "	SELECT idarticle FROM order_supplier_row
				WHERE idorder_supplier = $IdOrderChild";
	$rs = DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		$Order->addArticle( new CArticle( $rs->fields( "idarticle" ) ));
		$Order->save();
		
		$rs->MoveNext();
	}
	
	$query = "	UPDATE `order_supplier` 
				SET `idorder_supplier_parent` = $IdOrder 
				WHERE `idorder_supplier` = $IdOrderChild";	
	DBUtil::query( $query );

	*/
	
	if( !$IdOrder )
		TradeFactory::synchronizeDeliveryNoteFromSupplierOrder( $Order );
		
}
	
//---------------------------------------------------------------------------------------------

//ajout d'articles depuis le panier de la commande ( front office )

if( isset($_GET['getbuyerbasket']) ) {
	
	

	$it = Basket::getInstance()->getItems()->iterator();
	
	while( $it->hasNext() ){
		
		$item =& $it->next();
		
		$Order->addArticle( $item->getArticle(), $item->getQuantity() );
			
	}
		
	$Order->save();
	
	//réapprovisionnement : synchronisation du BL associé à la commande fournisseur
		
	if( !$Order->get( "idorder" ) )
		TradeFactory::synchronizeDeliveryNoteFromSupplierOrder( $Order );
			
}

//---------------------------------------------------------------------------------------------
	
//Envoyer un mail, @todo : ne pas venir sur cette page si c'est pour aller sur une autre

 if( isset( $_POST[ "sendmail" ] ) && $_POST[ "sendmail" ] == 1){
 	
 	$IdOrder = $Order->getId();

 	if(isset($_POST["ConfirmSendByMail"])) {
 		header( "Location: $GLOBAL_START_URL/sales_force/mail_order_supplier.php?IdOrder=$IdOrder" );	
 	}
		
		
 }
 
//---------------------------------------------------------------------------------------------

$IdOrder = $Order->getId();

//---------------------------------------------------------------------------------------------

$Str_status = $Order->get( "status" );

//état d'activation des champs du formulaire en fonction du status de la cde

if( $Str_status == SupplierOrder::$STATUS_INVOICED || $Str_status == SupplierOrder::$STATUS_CANCELLED )
	$disabled = " disabled=\"disabled\"";
else $disabled = "";

//---------------------------------------------------------------------------------------------

//Test pour navision

$modif = 0;
$msgcontact1 = "";
$msgcontact2 = "";
$msgdeliv = "";
$msgbill = "";

//---------------------------------------------------------------------------------------------

/**
 * Envoyer un fax
 * @todo : ne pas venir sur cette page si c'est pour aller sur une autre
 */
 
if( isset( $_POST[ "ConfirmSendByFax" ] ) || isset( $_POST[ "ConfirmSendByFax" ] ) ){
	
	header( "Location: $GLOBAL_START_URL/sales_force/transmit_fax_supplier.php?IdOrder=$IdOrder" );

}

 
//---------------------------------------------------------------------------------------------

//Gestion de la présence de l'email
$vide=0;
$b_email=$Order->getSupplier()->getContact()->get('email');
if(empty($b_email)){
	$vide=1;
}
?>
<?php 

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
?>	
<script type="text/javascript">
<!--

function confirmSendMail(){

	if (confirmCarrierDelivery()) {
		if( confirm( '<?php echo Dictionnary::translate( "gest_com_send_order_suplier" ) ?> ?' ) ){
			document.getElementById( 'sendmail' ).value=  '1';
			return true;
		}else{
			return false;
		}
	}else{
		return false;
	}
}

function confirmCarrierDelivery(){
	
	var var_text_tmp = "Vous devez sélectionner pour la commande fournisseur l'adresse de livraison du transporteur. Etes-vous sûr de vouloir envoyer la commande founisseur par mail ?";
	if(document.getElementById( 'carrier_delivery_check' ).value ==  '1'){
		if(confirm(var_text_tmp)){
			return true;
		}else{
			return false;
		}
	}else{
		return true;
	}
	
}

function confirmSendFax(){

	if( confirm( '<?php echo Dictionnary::translate( "gest_com_send_order_supplier_by_fax" ) ?> ?' ) ){
		return true;
	}else{
		return false;
	}
	
}

function confirmDispatch(){

	if( confirm( '<?php echo addslashes( Dictionnary::translate( "gest_com_dispath_order_supplier" ) ) ?> ?' ) )
				
	return true;
	
}

function confirmOrder(f)
{

	if( !confirm( '<?php echo Dictionnary::translate( "gest_com_confirm_order_suplier" ) ?> ?' ) )
		return false;
		
	f.form.terminate.value=1;

	return true;
	
}

function getIdOrder(){
	
	return '<?php echo $IdOrder ?>';
	
}

function confsuppPJ(ff)
{
	var ret = confirm("<?php echo addslashes( Dictionnary::translate("gest_com_confirm_delete_link") ) ?> ?");
	if (ret == true)
	{
		ff.form.submit();
	}
	
	return ret;
}

// PopUp pour sélectionner un article existant dans la BD
function catsel()
{
	postop = (self.screen.height-600)/2;
	posleft = (self.screen.width-900)/2;
	window.open('cat_select.php?fromscript=supplier_order.php&force_supplier=<?php echo $Order->get( "idsupplier" ) ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=850,height=600,resizable,scrollbars=yes,status=0');
	
}

// PopUp pour sélectionner un article existant dans la BD
function catsel_test()
{
	h = (self.screen.height-100);
	w = (self.screen.width-100);
	postop = (h-100);
	posleft = (w-100);
	window.open('../catalog/product.php?fromscript=supplier_order.php', '_blank', 'top=' + postop + ',left=' + posleft + ',width=600,height=600,resizable,scrollbars=yes,status=0');
	
}

function selectReference(){
	
	postop = (self.screen.height-600)/2;
	posleft = (self.screen.width-600)/2;
	window.open('ref_select.php?fromscript=supplier_order.php&idorder_supplier=<?php echo $Order->get( "idorder_supplier" ) ?>&force_supplier=<?php echo $Order->get( "idsupplier" ) ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=800,height=600,resizable,scrollbars=yes,status=0');
	
}

// PopUp pour ajouter un nouvel article
function newArt()
{
	postop = (self.screen.height-600)/2;
	posleft = (self.screen.width-700)/2;
	window.open('new_article.php?fromscript=supplier_order.php<?php echo urlencode( "?IdOrderSupplier=".$Order->get( "idorder_supplier" )) ?>&IdOrderSupplier=<?php echo $Order->get( "idorder_supplier" ) ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=650,height=600,resizable,scrollbars=yes,status=0');
	
}
	
function addSelectedReferences( selection ){

	alert( selection.length );
	var serializedIds = '';
	var i = 0;
	while( i < selection.length ){
	
		if( i )
			selection += ',';
			
		serializedIds += selection[ i ];
		
		i++;
		
	}
	
	document.location = 'sales_force?references=' + serializedIds;
	
}

// -->
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<div class="centerMax">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order_supplier/supplier_order.php</span>
<?php } ?>


	<form action="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $IdOrder ?>" id="frm_buyer" method="post" enctype="multipart/form-data">
		<input type="hidden" name="ModifyOrder" value="1" />
		<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>" />
		<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $_REQUEST[ "IdOrder" ] ?>" />
		

		<?php 
			include( "$GLOBAL_START_PATH/templates/sales_force/order_supplier/buyer.htm.php");

			$hasLot = false;
			$i = 0;
			while( $i < $Order->getItemCount() ){
				if( $Order->getItemAt( $i)->get( "lot" ) > 1 )
					$hasLot = true;	
				$i++;
			}
		?>	
	</form>		
		<?php 
		include( "$GLOBAL_START_PATH/templates/sales_force/order_supplier/order.htm.php" );
		?>
		
		<?php
		
			$articlesET = 0.0;
			$it = $Order->getItems()->iterator();
			while( $it->hasNext() )
				$articlesET += $it->next()->getTotalET();
			
			$ttc = $Order->getTotalATI() - $Order->getChargesATI() - $Order->get( "billing_amount" );
			$ht = $articlesET; //HT sans frais de port, avant remise	
			$ht_wd = $articlesET - $Order->get( "total_discount_amount" ); //HT avec remise
		
			$total_discount_rate = $Order->get( "total_discount" );
			$total_discount_amount = $Order->get( "total_discount_amount" );
			
			$total_charge_ht = 0.0 + $Order->get( "total_charge_ht" );
			//$charge_vat = 0.0 + $total_charge_ht * $Order->get('charge_vat_rate')/100;
			$charge_vat = 0.0 + $Order->get( "charge_vat" );
			$total_charge_ttc = 0.0 + $Order->get( "total_charge" );
			
			$total_order_ttc = 0.0 + $ttc + $total_charge_ttc;
			$total_order_ht = 0.0 + $ht_wd + $total_charge_ht;
			
			$billing_rate = $Order->get( "billing_rate" );
			$billing_amount = $Order->get( "billing_amount" );
			
			$down_payment_rate = $Order->getDownPaymentRate();
			$down_payment_amount = $Order->getDownPaymentAmount();
			
			$total_ht_wb = $Order->getTotalET();
			$total_ht_wbi = $total_ht_wb - $down_payment_amount;
			
			$total_ttc_wb = $Order->getTotalATI();
			$total_ttc_wbi = $Order->getNetBill();
			
			//$Factor = $Order->get( "factor" );
			
			$franco=$Order->getSupplier()->get( "franco_supplier" );
		include( "$GLOBAL_START_PATH/templates/sales_force/order_supplier/amounts.htm.php" );
		include( "$GLOBAL_START_PATH/templates/sales_force/order_supplier/pay.htm.php" );
		include( dirname( __FILE__ ) . "/../templates/sales_force/order_supplier/carriage.php" );
		$disabled = $Str_status != SupplierOrder::$STATUS_STANDBY ? " disabled=\"disabled\"" : ""; 
		
		?>
		<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>" />
		<input type="hidden" name="terminate" value="0" />
		
		
		
		<div class="spacer"></div>
		<div class="contentResult">
			
			<h1 class="titleEstimate"><span class="textTitle">Finaliser la commande fournisseur</span>
			<div class="spacer"></div></h1>
			<div class="spacer"></div>
		
			
			<div class="blocEstimateResult"><div style="margin:5px;">

			<div class="floatleft">
			
			
			<?php
				
				if( $Str_status==SupplierOrder::$STATUS_STANDBY ){
					if( strlen( $Order->getSupplier()->getContact()->get( "email" ) ) ){ 
						
						$carrier_delivery =  DBUtil::getParameterAdmin( "carrier_delivery" );
						
						$query = "
							SELECT EXISTS (
								SELECT 1 
								FROM `order_row`
								WHERE `idorder` = '" . $Order->get( "idorder" ) . "'
							) is_carrier_delivery";
						$rs =& DBUtil::query( $query );
						$carrier_delivery_check = $rs->fields( "is_carrier_delivery" );
					
					?>
						<!--Envoyer par mail-->
						<input type="hidden" name="sendmail" id="sendmail" value="" />
						<input type="hidden" name="carrier_delivery_check" id="carrier_delivery_check" value="<?php echo $carrier_delivery_check; ?>" />
						<input type="submit" class="buttonValid buttonValidMail<?php echo $Order->getSupplier()->get( "send_to" ) == "Mail" ? "actif" : "" ?>" value="Envoyer par 
email" name="ConfirmSendByMail" onclick="javascript:return confirmSendMail();" />
					
					<?php } ?>
					
					<!--Envoyer par fax-->
					<input type="submit" class="buttonValid buttonValidFax<?php echo $Order->getSupplier()->get( "send_to" ) == "Fax" ? "actif" : "" ?>" value="Envoyer par 
fax" name="ConfirmSendByFax" />
				<?php 
				}	
				if( ($Str_status==SupplierOrder::$STATUS_SENT_BY_MAIL ) OR ($Str_status==SupplierOrder::$STATUS_SENT_BY_FAX ) ){
				?>	
					<input type="submit" class="buttonValid buttonModPros" value="Confirmée par 
le fournisseur" name="ConfirmOrder" onclick="javascript:return confirmOrder( this );" />
					<?php
				}
				if( $Str_status==SupplierOrder::$STATUS_CONFIRMED ) {
				?>	
					<input type="submit" class="buttonValid buttonModPros" value="Expédié par 
le fournisseur" name="ConfirmDispatch" onclick="javascript:return confirmDispatch( this );" />
					<?php
				}
				if( $modif==0 ){
				} else { 
					
					echo '<div class="alert" align="center">'.$msgcontact1.'</div>';
					echo '<div class="alert" align="center">'.$msgcontact2.'</div>';
					echo '<div class="alert" align="center">'.$msgdeliv.'</div>';
					echo '<div class="alert" align="center">'.$msgbill.'</div>';
				 } 

			?>
			</div>
			<div class="floatright">
			<?php
			
				//annulation de cde
				
				if( $Order->get( "status" ) != SupplierOrder::$STATUS_CANCELLED ){
					
					?>
					<script type="text/javascript">
					/* <![CDATA[ */
					
						function cancelOrder(){

							var location = "<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?IdOrder=<?php echo $Order->getId(); ?>&cancel";

							<?php
							
								$supplier = DBUtil::stdclassFromDB( "supplier", "idsupplier", $Order->get( "idsupplier" ) );
								
								if( $Order->get( "status" ) != SupplierOrder::$STATUS_STANDBY
									&& ($supplier->send_to == "Mail" && strlen( $supplier->email ) || $supplier->send_to == "Fax" && strlen( $supplier->faxnumber ) ) ){
										
										?>
										if( confirm( "Souhaitez-vous faire la demande d'annulation par courriel ou par fax?" ) )
											location += "&send";
										<?php
										
								}
								else if( $Order->get( "status" ) != SupplierOrder::$STATUS_STANDBY ){
									
									?>
									if( !confirm( "La demande d'annulation ne pourra pas être envoyée au fournisseur car celui-ci ne possède pas d'adresse électronique ou de numéro de fax. Souhaitez-vous poursuivre l'annulation de la commande?" ) )
										return;
									<?php
									
								}
									
							?>
							document.location = location;
							
						}

					/* ]]> */
					</script>
					<input type="button" onclick="if( confirm( 'Etes-vous certains de vouloir annuler cette commande?' ) ) cancelOrder();" value="Annuler la commande" class="buttonValidRight buttonDelete" />
					<?php
					
				}
				
				?>
				<input type="submit" class="buttonValidRight buttonSave" value="Enregistrer" name="SaveOrder" />
				<input type="hidden" id="delivery_address_id" value="" name="delivery_address_id" />
				<input type="hidden" id="crossdocking" value="" name="crossdocking" />
			</div>
			<div class="spacer"></div><br/>
			
		</div></div>
		<div class="spacer"></div>
	</div>

	</form>
</div>

<?php 

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>	
</body>
</html>
<?php

//------------------------------------------------------------------------------------------------

function anchor( $name, $hasIndex = false ){

	if( $hasIndex ){
		
		foreach( $_POST as $key => $value ){
		
			$pos = strpos( $key, $name );	
			if( $pos !== false && !$pos ){
				
				echo "<a name=\"lastaction\"></a>";
				return;	
				
			}
			
		}
		
	}
	else if( isset( $_POST[ $name ] ) || isset( $_POST[ $name . "_x" ] ) )	
		echo "<a name=\"lastaction\"></a>";
	
}

//------------------------------------------------------------------------------------------------
function uploadPJ($id){
	
	global	$GLOBAL_START_PATH,
			$Order,$IdOrder;
	
	$frs = $Order->get("idsupplier");
		
	$YearFile = date("Y");
	$DateFile = date("d-m");
	
	$newdoc='vide';
	$filestoupload=array();
	
	//On remplit le tableau avec les fichiers uploadés
	if( isset( $_FILES[ "order_confirmed" ] ) && is_uploaded_file( $_FILES[ "order_confirmed" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_confirmed","target"=>"$GLOBAL_START_PATH/data/order_supplier/$YearFile/$frs/","prefixe"=>"OC");
	}
	
	if( isset( $_FILES[ "order_supplier_doc" ] ) && is_uploaded_file( $_FILES[ "order_supplier_doc" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_supplier_doc","target"=>"$GLOBAL_START_PATH/data/order_supplier/$YearFile/$frs/","prefixe"=>"PJ");
	}

	if( isset( $_FILES[ "order_supplier_bl" ] ) && is_uploaded_file( $_FILES[ "order_supplier_bl" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_supplier_bl","target"=>"$GLOBAL_START_PATH/data/order_supplier/$YearFile/$frs/","prefixe"=>"BL");
	}
	
	if( isset( $_FILES[ "order_supplier_be" ] ) && is_uploaded_file( $_FILES[ "order_supplier_be" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_supplier_be","target"=>"$GLOBAL_START_PATH/data/order_supplier/$YearFile/$frs/","prefixe"=>"BE");
	}
		
	if( isset( $_FILES[ "order_supplier_divers" ] ) && is_uploaded_file( $_FILES[ "order_supplier_divers" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_supplier_divers","target"=>"$GLOBAL_START_PATH/data/order_supplier/$YearFile/$frs/","prefixe"=>"Divers");
	}
	
	//On parcours le tableau
	foreach($filestoupload as $upload){

		$fieldname = $upload["fieldname"];
	
		$target_dir = $upload["target"];
		$prefixe = $upload["prefixe"];
		
		$name_file = $_FILES[$fieldname]['name'];
		$tmp_file = $_FILES[$fieldname]['tmp_name'];
		
		$extension = substr($name_file, strrpos($name_file,"."));
	
		if(($extension!='.doc')&&($extension!='.jpg')&&($extension!='.xls')&&($extension!='.pdf')&&($extension!='.DOC')&&($extension!='.JPG')&&($extension!='.XLS')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./supplier_order.php?IdOrder=".$IdOrder."\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
	
		// on copie le fichier dans le dossier de destination
    	$dest=$target_dir.$prefixe."_".$DateFile."_".$id.$extension;
		$newdoc= $YearFile."/".$frs."/".$prefixe."_".$DateFile."_".$id.$extension;
	
		//order_supplier existe
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/order_supplier/'.$YearFile ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/order_supplier/'.$YearFile ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		if( chmod( $GLOBAL_START_PATH.'/data/order_supplier/'.$YearFile, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		if( !file_exists( $GLOBAL_START_PATH.'/data/order_supplier/'.$YearFile.'/'.$frs ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/order_supplier/'.$YearFile.'/'.$frs ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		if( chmod( $GLOBAL_START_PATH.'/data/order_supplier/'.$YearFile.'/'.$frs , 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
    	
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		if( chmod( $dest, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
			
		$Order->set( $fieldname , $newdoc );
		$Order->save();	
	
	}
	
}

//------------------------------------------------------------------------------------------------
/**
 * Envoi de la demande d'annulation de commande au fournisseur
 * @param SupplierOrder $supplierOrder
 * @return void
 */
function sendSupplierOrderCancellation( SupplierOrder $supplierOrder ){
	
	include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();

	$editor->setLanguage( $supplierOrder->getSupplier()->get( "language" ) );
	$editor->setTemplate( MailTemplateEditor::$SUPPLIER_ORDER_CANCELLATION  );

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseCommercialTags( $supplierOrder->get( "iduser" ) );
	$editor->setUseSupplierTags( $supplierOrder->get( "idsupplier" ) );
	$editor->setUseSupplierOrderTags( $supplierOrder->getId() );
	$editor->setUseBuyerTags( $supplierOrder->get( "idbuyer" ) );
	
	include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );
	
	$mailer = new htmlMimeMail();
	$mailer->setHtml( $editor->unTagBody() );
	$idorder_supplier = $supplierOrder->getId();
	
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFSupplierOrder.php" );
	$odf = new ODFSupplierOrder( $idorder_supplier );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment( $pdfData, "Commande_Fournisseur_$idorder_supplier.pdf", "application/pdf" );
	
	/* envoi par courriel ou par fax */
	
	switch( $supplierOrder->getSupplier()->get( "send_to" ) ){

		case "Fax" : 
			
			if( strlen( $supplierOrder->getSupplier()->getContact()->get( "faxnumber" ) )
				&& !sendSupplierOrderCancellationEmail( $editor, $mailer, $supplierOrder->getSupplier()->getContact()->get( "faxnumber" ) ) )
				return false;
				
			break;
			
		case "Mail" :
		default :
			
			if( strlen( $supplierOrder->getSupplier()->getContact()->get( "email" ) )
				&& !sendSupplierOrderCancellationEmail( $editor, $mailer, $supplierOrder->getSupplier()->getContact()->get( "email" ) ) )
				return false;
				
			break;
		
		default : return false;
		
	}
	
	/* copie au commercial */

	if( DBUtil::getParameterAdmin( "cc_salesman" ) ){
		
		$mailer = new htmlMimeMail();
		$mailer->setHtml( $editor->unTagBody() );
		$mailer->setSubject( "Copie : " . $editor->unTagSubject() );
		
		return $mailer->send( array( User::getInstance()->get( "email" ) ) );
	
	}
	
	return true; 
	
}

//------------------------------------------------------------------------------------------------

function sendSupplierOrderCancellationEmail( MailTemplateEditor &$editor, htmlMimeMail &$mailer, $recipient ){

	$mailer->setSubject( $editor->unTagSubject() );
	return $mailer->send( array( $recipient ) );
	
}

//------------------------------------------------------------------------------------------------

function sendSupplierOrderCancellationFax( MailTemplateEditor &$editor, htmlMimeMail &$mailer, $faxnumber ){
	
	$smtp_server 	= DBUtil::getParameterAdmin('fax_smtp_server');
	$smtp_sender 	= DBUtil::getParameterAdmin('fax_smtp_sender');
	$smtp_auth 		= DBUtil::getParameterAdmin('fax_smtp_auth');
	$smtp_user 		= DBUtil::getParameterAdmin('fax_smtp_user');
	$smtp_password 	= DBUtil::getParameterAdmin('fax_smtp_password');
	$smtp_recipient = DBUtil::getParameterAdmin('fax_smtp_recipient');

	$mailer->setSMTPParams( $smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password );
	$mailer->setSubject( $faxnumber );

	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . "\" <$smtp_user>" );
	
	return $mailer->send( array( $smtp_recipient ), "smtp" );
	
}

//------------------------------------------------------------------------------------------------
		// ------- Mise à jour de l'id gestion des Index  #1161
//--------------------------------------------------------------------------------------------------
function showTooltipSupplier($idorder_supplier){
	$query = "	SELECT * FROM order_supplier_row osr
				JOIN delay d ON
					d.iddelay = osr.delivdelay
				WHERE osr.idorder_supplier = '$idorder_supplier'";
	
	$rs = DBUtil::query( $query );
	$rows ='';
	while( !$rs->EOF() ){
		
		$rows .= "<tr>
			<td>".$rs->fields("reference")."</td>
			<td>".$rs->fields("summary")."</td>
			<td>".$rs->fields("quantity")."</td>
			<td>".$rs->fields("ref_supplier")."</td>
			<td>".$rs->fields("delay_1")."</td>
			</tr>";
		$rs->MoveNext();
		
	}
	
	
	$tooltip_content = "
	<strong>Commande fournisseur  n°$idorder_supplier</strong>
	<div class='content' style='padding:5px;'>
		<div class='subContent'>
			<div class='resultTableContainer'>
				<table class='dataTable resultTable' >
					<thead>
						<tr>
							<th>Référence</th>
							<th width='50%'>Désignation</th>
							<th>Qté</th>
							<th>Réf. Fournisseur</th>
							<th >Délai</th>
						</tr>
					</thead>
					<tbody>
							$rows
					</tbody>
				</table>
			</div>
		</div>
	</div>
	";
	
	return $tooltip_content;
}
?>