<?php

//------------------------------------------------------------------------------------------------
/* mise à jour des BLs depuis la boîte de dialogue - Livraisons partielles - ajax */

//------------------------------------------------------------------------------------------
/* 
* Tooltip Content show 
* By behzad
*/
if( isset( $_GET[ 'showTooltip' ] ) && $_GET[ 'idorder_supplier_parent' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( showTooltip_idorder_supplier_parent( $_GET[ 'idorder_supplier_parent' ] ) );

}
////////////////////////////////////////////////////
if( isset( $_POST[ "updateDeliveryNotes" ] ) ){

	include_once( dirname( __FILE__ ) . "/../config/init.php" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	
	foreach( $_POST as $key => $value ){
		
		if( substr( $key, 0, strlen( "availability_date_" ) ) == "availability_date_" ){
			
			$regs = array();
			
			if( !preg_match( "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/", $value, $regs ) )
				exit( "Le format de la date de mise à disposition est incorrect : $value" );
				
			$idbl_delivery 		= intval( substr( $key, strlen( "availability_date_" )  ) );
			$availability_date	= $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
			//echo "\nUPDATE bl_delivery SET availability_date = " . DBUtil::quote( $availability_date ) . " WHERE idbl_delivery = '$idbl_delivery' LIMIT 1";
			DBUtil::query( "UPDATE bl_delivery SET availability_date = " . DBUtil::quote( $availability_date ) . " WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
			
		}
		else if( substr( $key, 0, strlen( "dispatch_date_" ) ) == "dispatch_date_" ){
			
			if( !preg_match( "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/", $value, $regs ) )
				exit( "Le format de la date d'expédition est incorrect : $value" );
				
			$idbl_delivery 	= intval( substr( $key, strlen( "dispatch_date_" ) ) );
			$dispatch_date	= $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
			//echo "\nUPDATE bl_delivery SET dispatch_date = " . DBUtil::quote( $dispatch_date ) . " WHERE dispatch_date = '$idbl_delivery' LIMIT 1";
			DBUtil::query( "UPDATE bl_delivery SET dispatch_date = " . DBUtil::quote( $dispatch_date ) . " WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
			
		}
		
	}
	
	exit( "1" );
	
}

//------------------------------------------------------------------------------------------------
/* boîte de dialogue - Livraisons partielles */

if( isset( $_REQUEST[ "dialog" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../objects/DHTMLCalendar.php" );
	include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	
	$idorder_supplier = intval( $_REQUEST[ "idorder_supplier" ] );

	$query = "
	SELECT bl.idbl_delivery, bl.availability_date, bl.dispatch_date,
		GROUP_CONCAT( blr.reference ORDER BY blr.idrow ASC ) AS reference,
		GROUP_CONCAT( blr.ref_supplier ORDER BY blr.idrow ASC ) AS ref_supplier,
		GROUP_CONCAT( blr.quantity ORDER BY blr.idrow ASC ) AS quantity
	FROM bl_delivery bl
	LEFT JOIN bl_delivery_row blr
	ON blr.idbl_delivery = bl.idbl_delivery
	WHERE bl.idorder_supplier = '$idorder_supplier'
	GROUP BY bl.idbl_delivery
	ORDER BY bl.idbl_delivery ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<style type="text/css">
		.ui-datepicker{ z-index: 2999; }
	</style>
	<script type="text/javascript">

		function updateDeliveryNotes(){

			var data = "updateDeliveryNotes=1";

			$( "#DelyveryNotes<?php echo $idorder_supplier; ?>" ).find( "input[id^='availability_date']" ).each( function( i ){

				data += "&" + $( this ).attr( "id" )  + "=" + escape( $( this ).val() );
		
			});

			$( "#DelyveryNotes<?php echo $idorder_supplier; ?>" ).find( "input[id^='dispatch_date']" ).each( function( i ){

				data += "&" + $( this ).attr( "id" )  + "=" + escape( $( this ).val() );
		
			});

			$.ajax({

				url: "<?php echo $GLOBAL_START_URL ?>/sales_force/product_term_list.php",
				async: true,
				cache: false,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ 

					alert( "Impossible de sauvegarder les modifications" ); 

				},
				success: function( responseText ){
					
					if( responseText != "1" )
						alert( "Impossible de sauvegarder les modifications : " + responseText );
	        		else{
	        		
	        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
				    	$.growlUI( '', 'Modifications enregistrées' );
				        
						$( "#DeliveryNoteDialog" ).dialog( "destroy" );
	
	        		}	
									
				}
			
			});

		}

	</script>
	<div class="mainContent">
    
		<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/product_term_buyer.php</span>
<?php } ?>
	
		<div class="topRight"></div>
		<div class="topLeft"></div>
		<div class="content">			
			<div class="subContent">
				<div class="tableContainer">
					<table class="dataTable" id="DelyveryNotes<?php echo $idorder_supplier; ?>">
						<tr>
							<th style="text-align:center;">BL N°</th>
							<th style="text-align:center;">Réf. Catalogue</th>
							<th style="text-align:center;">Réf. Fournisseur</th>
							<th style="text-align:center;">Quantité</th>
							<th style="text-align:center;">Date de mise à disposition</th>
							<th style="text-align:center;">Date d'expédition</th>
						</tr>
						<?php
						
							while( !$rs->EOF() ){
								
								?>
								<tr>
									<td style="text-align:center;"><?php echo $rs->fields( "idbl_delivery" ); ?></td>
									<td style="text-align:center;"><?php echo implode( "<br />", explode( ",", $rs->fields( "reference" ) ) ); ?></td>
									<td style="text-align:center;"><?php echo implode( "<br />", explode( ",", $rs->fields( "ref_supplier" ) ) ); ?></td>
									<td style="text-align:center;"><?php echo implode( "<br />", explode( ",", $rs->fields( "quantity" ) ) ); ?></td>
									<td style="text-align:center;">
										<input type="text" class="calendarInput" value="<?php echo usDate2eu( $rs->fields( "availability_date" ) ); ?>" id="availability_date_<?php echo $rs->fields( "idbl_delivery" ); ?>" />
										<?php DHTMLCalendar::calendar( "availability_date_" . $rs->fields( "idbl_delivery" ), true ); ?>
									</td>
									<td style="text-align:center;">
										<input type="text" class="calendarInput" value="<?php echo usDate2eu( $rs->fields( "dispatch_date" ) ); ?>" id="dispatch_date_<?php echo $rs->fields( "idbl_delivery" ); ?>" />
										<?php DHTMLCalendar::calendar( "dispatch_date_" . $rs->fields( "idbl_delivery" ), true ); ?>
									</td>
								</tr>
								<?php
								
								$rs->MoveNext();
								
							}
							
					?>
					</table>
				</div>
				<p style="text-align:right;">
					<input type="button" class="blueButton" onclick="$('#DeliveryNoteDialog').dialog('destroy');" value="Annuler" />
					<input type="button" class="blueButton" onclick="updateDeliveryNotes();" value="Sauvegarder" />
				</p>
			</div>
		</div>
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>		
	</div>			
	<?php
	
	exit();
		
}

//------------------------------------------------------------------------------------------------
/* mise à jour n° de commande interne */
/* mise à jour n° de commande interne */

if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "n_service" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_name = '" . Util::html_escape( stripslashes( $_POST[ "n_service" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "n_serial" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET serial_number = '" . Util::html_escape( stripslashes( $_POST[ "n_serial" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "service_extern" ){

	if ($_POST[ "service_extern" ] == 0)$service_ext = 1;
	else $service_ext = 0;
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_extern = '" . Util::html_escape( stripslashes( $service_ext )) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

$Title = "Récapitulatif fiche client administration";

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "update" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
		
	error_reporting( 0 );
	ModifyDate();
	
	exit();
	
}

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/Supplier.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$con = DBUtil::getConnection();

$use_help = DBUtil::getParameterAdmin( "gest_com_use_help" );

$resultPerPage = 1000;
$maxSearchResults = 1000;

if( isset( $_REQUEST[ "idbuyer" ] ) )
	$idbuyer = $_REQUEST[ "idbuyer" ];


$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

$TableName = "product_term";
/*$search = "";

if( isset( $_POST[ "search" ] ) )
	$search = $_POST[ "search" ];

if( isset( $_GET[ "search" ] ) )
	$search = $_GET[ "search" ];

if( isset( $_POST[ "idorder" ] ) )				$idorder = $_POST[ "idorder" ];						else $idorder = "";
/*if( isset( $_POST[ "n_order_supplier" ] ) )		$n_order_supplier = $_POST[ "n_order_supplier" ];	else $n_order_supplier = "";
if( isset( $_POST[ "idbl_delivery" ] ) )		$idbl_delivery = $_POST[ "idbl_delivery" ];			else $idbl_delivery = "";
if( isset( $_POST[ "idsupplier" ] ) )			$idsupplier = $_POST[ "idsupplier" ];				else $idsupplier = "";
if( isset( $_POST[ "idbuyer" ] ) )				$idbuyer = $_POST[ "idbuyer" ];						else $idbuyer = "";
if( isset( $_POST[ "idorder_supplier" ] ) )		$idorder_supplier = $_POST[ "idorder_supplier" ];	else $idorder_supplier = "";
if( isset( $_POST[ "lastname" ] ) )				$lastname = $_POST[ "lastname" ];					else $lastname = "";
if( isset( $_POST[ "status" ] ) )				$status = $_POST[ "status" ];						else $status = "";*/
//if( isset( $_POST[ "minus_date_select" ] ) )	$minus_date_select = $_POST[ "minus_date_select" ];	else $minus_date_select = "-365";
/*if( isset( $_POST[ "company" ] ) )				$company = $_POST[ "company" ];						else $company = "";
if( isset( $_POST[ "zipcode" ] ) )				$zipcode = $_POST[ "zipcode" ];						else $zipcode = "";
if( isset( $_POST[ "city" ] ) )					$city = $_POST[ "city" ];							else $city = "";*/

//---------------------------------------------------------------------------------------------

//suppression d'une commande

if( isset( $_POST[ "DeleteOrder" ] ) || isset( $_POST[ "DeleteOrder_x" ] ) )
	SupplierOrder::delete( intval( $_POST[ "OrderToDelete" ] ) );

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<script type="text/javascript" language="javascript">
<!--
	
	function SortResult( sby, ord ){
		
		document.adm_estim.action="product_term_list.php?search=1&sortby="+sby+"&sens="+ord;
		document.adm_estim.submit();
		
	}
	
	function goToPage( pageNumber ){
		
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function SetDate( element ){

		var idorders = element.id.substring( element.id.lastIndexOf( '_', element.id.length ) + 1, element.id.length );
		
		var availibility = $( "#availability_date_" + idorders ).attr( "value" );
	//	var dispatch = $( "#dispatch_date_" + idorders ).attr( "value" );
		
		var postData = "action=update&idos=" + idorders + "&av=" + availibility 
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la date" ); },
		 	success: function( responseText ){

				if( responseText == 'wrong av date format' )
					alert( "Format de date echeance incorrect !" );
					else if( responseText == 'wrong sql' )
					alert( "Impossible de mettre à jour la date !" );
				
        		else if( responseText.length ){
        		
        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Modifications enregistrées' );

        			$( "#cellstatus_" + idorders ).html( responseText );
        			$( "#cellstatus_" + idorders ).attr( "class", "" );
        			$( "#celldelete_" + idorders ).html( "" );
        			
        		}	
					
			}

		});
		
		
	}

	function confirmDeleteOrder( idorder ){

		
		var ret = confirm( '<?php echo Dictionnary::translate( "gest_com_confirm_delete_order_supplier" ) ?>' + idorder + '?' );
		
		if( ret == true )
			document.forms.adm_estim.elements[ 'OrderToDelete' ].value = idorder;
		
		return ret;
		
	}
	
	function selectRelance(){
		
		document.getElementById( "interval_select_relance" ).checked = true;
		
	}
	
	function selectMonth(){
		
		document.getElementById( "interval_select_month" ).checked = true;
		
	}
	
	function selectDelivery( dateField ){
		
		document.getElementById( "interval_select_delivery" ).checked = true;
		
	}
	
	function selectBetween( dateField ){
		
		document.getElementById( "interval_select_between" ).checked = true;
		
		return true;
		
	}
	
// -->
</script>
<?php

//	$idbuyer = 11955;
	$from = "product_term,buyer,contact";
	$now = getdate();
	
		
	
	if( !empty( $idbuyer ) ){
		
		$idbuyer = FProcessString( $idbuyer );
		$SQL_Condition .= " product_term.idbuyer = $idbuyer ";
	
	}
	
	
	
	$lang = User::getInstance()->getLang();
	
	$SQL_Condition	.= "
	
	AND product_term.idbuyer = buyer.idbuyer
	AND product_term.idbuyer = contact.idbuyer
	AND contact.idcontact = 0
";
	
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sortby" ] ) )
		$orderby = " ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
	else
		$orderby = " ORDER BY idproduct_term ASC";
	
	//Nombre de pages
	$SQL_Query = "
	SELECT COUNT(*) AS resultCount 
	FROM $from
	 
		WHERE $SQL_Condition 
	";	
//	echo $SQL_Query;
	$rs = $con->Execute( $SQL_Query );
//	var_dump($rs);
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );
	
	if( $resultCount < $maxSearchResults ){
		
		$pageCount = ceil( $resultCount / $resultPerPage );
		
		//Recherche
		$start = ( $page - 1 ) * $resultPerPage;
		
		$SQL_Query = "
		SELECT 
		product_term.idproduct_term,
		product_term.idorder,
		product_term.reference,
		product_term.designation,
		product_term.service_extern,
		product_term.service_name,
		product_term.serial_number,
		product_term.idbuyer,
		product_term.doc_certificat,
		product_term.term_date,
		buyer.company,
				contact.firstname
			
		FROM $from
		
		WHERE $SQL_Condition 
		
		$orderby
		LIMIT $start, $resultPerPage";
//echo $SQL_Query;
		$rs = $con->Execute( $SQL_Query );
//	var_dump($rs);
		global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
			
		include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
			
		$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $SQL_Query, $GLOBAL_DB_PASS ) );
		
		if( $rs->RecordCount() > 0 ){
			
			?>
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function updateservice( idproduct_term, n_service ){
				
					var data = "idproduct_term=" + idproduct_term + "&n_service=" + n_service;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=n_service",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        					
						}

					});
		
				}


function updateserial( idproduct_term, n_serial ){
				
					var data = "idproduct_term=" + idproduct_term + "&n_serial=" + n_serial;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=n_serial",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        					
						}

					});
		
				}
				
				function updatedoc( idproduct_term,idorder, documents ){
				
					var data = "idproduct_term=" + idproduct_term + "&idorder=" + idorder + "&documents=" + documents;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=documents",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        					
						}

					});
		
				}
				

				function deliveryNoteDialog( idorder_supplier ){

					$( "#DeliveryNoteDialog" ).html( '<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/small_black_spinner.gif" alt="" /> chargement en cours...</p>' );

					$("#DeliveryNoteDialog").dialog({
			        
						modal: true,	
						title: "Commande Fournisseur N° " + idorder_supplier,
						position: "top",
						width: 800,
						zIndex:998,
						close: function(event, ui) { 
						
							$("#DeliveryNoteDialog").dialog( 'destroy' );
							
						}
					
					});

					$.ajax({
					 	
						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?dialog&idorder_supplier=" + idorder_supplier,
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible d'afficher la boîte de dialogue" ); 
							
						},
					 	success: function( responseText ){
					
							$( "#DeliveryNoteDialog" ).html( responseText );
										
						}
					
					});
					
				}
				//Handler of tooltip By behzad
	        $(document).ready(function() {
					$(".fournisseurHelp").each( function(){
					$(this).qtip({
						adjust: {
							mouse: true,
							screen: true
						},
						content: { url: '<?php echo HTTP_HOST; ?>/sales_force/product_term_list.php?showTooltip&idorder_supplier_parent=' + $(this).attr( 'rel' ) },
						position: {
							corner: {
								target: "rightMiddle",
								tooltip: "leftMiddle"
							}
						},
						style: {
							background: "#FFFFFF",
							border: {
								color: "#EB6A0A",
								radius: 5
							},
							fontFamily: "Arial,Helvetica,sans-serif",
							fontSize: "13px",
							tip: true,
							width: 600
						}
					});
					
				});
			});	
			function realiserservice(idproduct_term,service_extern){
					
				var data = "idproduct_term=" + idproduct_term + "&service_extern=" + service_extern;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=service_extern",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
			//end of Tooltip
			/* ]]> */
			</script>
			<div id="DeliveryNoteDialog" style="display:none;"></div>
			<a name="topPage"></a>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
				
<div class="centerMax">
<div id="tools" class="rightTools">
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
	Accès rapide</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
	Quelques chiffres...</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
</div>	
<div class="contentDyn">
<form action="<?php echo $ScriptName ?>" method="post" name="adm_estim" name="adm_estim" enctype="multipart/form-data">
	<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
	<input type="hidden" name="search" value="1" />
				
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Récapitulatif fiche client administration : <?php echo $rs->RecordCount() ?> fiche(s)</span>
	<span class="selectDate">
                <a href="#bottom" class="goUpOrDown" style="text-decoration:none;font-size:10px; color:#000000;">
						Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a> &nbsp;&nbsp;
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/export_csv.php?type=order_supplier&amp;req=<?php echo $encryptedQuery ?>">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				</span>
	<div class="spacer"></div>
	</h1>		
				
	<div class="blocSearch"><div style="margin:5px;">
		<div class="subContent content">
							<table class="dataTable resultTable">
								<thead>
									<tr>
										<th style="width:8%;">N° cde</th>
										<th style="width:8%;">Référence</th>
										<th style="width:8%;">Service externe</th>
										<th style="width:21%;">Désignation</th>
										<th style="width:15%;">Gestion</th>
										
										<th style="width:15%;">N° de série</th>
										<th style="width:15%;">Certificat</th>
										<th style="width:10%;">Date échéance</th>
										<th style="width:8%;">Statut</th>
									</tr>
									<!-- petites flèches de tri -->
									<tr>
										<th class="noTopBorder">
											<a href="javascript:SortResult('user.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('user.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
									
										<th class="noTopBorder">
											<a href="javascript:SortResult('order_supplier.idorder_supplier','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('order_supplier.idorder_supplier','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
                                        	<th class="noTopBorder"></th>
										<th class="noTopBorder">
											
										</th>
											<th class="noTopBorder"></th>
											<th class="noTopBorder"></th>
											<th class="noTopBorder"></th>
											
										<th class="noTopBorder">
											<a href="javascript:SortResult('order_supplier.total_amount_ht','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('order_supplier.total_amount_ht','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										
										<th class="noTopBorder"></th>
									</tr>
								</thead>
								<tbody>

<?php
			
			//Récupérer les traductions des status
			$strstatus = array();
			$db = &DBUtil::getConnection();
			
			$query = "SELECT order_supplier_status, order_supplier_status$lang FROM order_supplier_status";
			$rs2 = $db->Execute( $query );
			
			if( $rs2 === false )
				die( Dictionnary::translate( "gest_com_impossible_recover_status_list" ) );
			
			while( !$rs2->EOF() ){
				
				$strstatus[ $rs2->fields( "order_supplier_status" ) ] = $rs2->fields( "order_supplier_status$lang" );
				
				$rs2->MoveNext();
				
			}
			
			for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
					$idproduct_term			= $rs->Fields( "idproduct_term" );
				$idorder			= $rs->Fields( "idorder" );
				$idbuyer	= $rs->Fields( "idbuyer" );
				$company	= $rs->Fields( "company" );
				$firstname			= $rs->Fields( "firstname" );
				$reference				= $rs->Fields( "reference" );
				$service_name	= $rs->Fields( "service_name" );
				$serial_number		= $rs->fields( "serial_number" );
				$doc_certificat			= $rs->fields( "doc_certificat" );
				$term_date			= $rs->fields( "term_date" );
			$service_extern	= $rs->Fields( "service_extern" );
				$designation	= $rs->Fields( "designation" );
				
				if( $term_date == "0000-00-00" )
					$term_date = "";
					
					
$dates =  date("Y-m-d");
$Nombres_jours =  NbJours($dates,$term_date);
// Affiche 2
//echo $Nombres_jours;
					//$datetime1 = new DateTime('2009-10-11');
//$datetime2 = new DateTime('2009-10-13');
					//$result =  temps_ecoule($term_date,"date");
					
				//	$dates =  date("Y-m-d");
		

				//	echo $dates;
					
					
				
				
?>
											<tr class="blackText<?php if( $oss == "cancelled" ) echo " cancelled"; ?>">
												<td class="lefterCol"><?php echo $idorder ?></td>
												
												
												
                                                <td class="lefterCol"><?php echo $reference ?></td>
                                                    <td class="lefterCol">
												   <a href="#" onClick="realiserservice(<?php echo $idproduct_term; ?>,<?php echo $service_extern; ?> )"><?php if ($service_extern == 1 ) {?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/ok.png" alt="oui" width="20px" height="20px" /><?php }else{  ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/non.png" alt="Non" width="20px" height="20px" /><?php } ?></a>
												   
												</td>   
                                                    <td class="lefterCol"><?php echo $designation ?></td>
											<td style="white-space:nowrap;">
												<?php
												
													if( $oss != "cancelled" ){
														
														?>
														<input id="service_<?php echo $idproduct_term; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $service_name ); ?>" onkeyup="" />
														<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onclick="updateservice(<?php echo $idproduct_term; ?>, $('#service_<?php echo $idproduct_term; ?>').val() );" />
														<?php
														
													}
													
												?>
												</td>
												<td style="white-space:nowrap;">
												<?php
												
													if( $oss != "cancelled" ){
														
														?>
														<input id="serial_<?php echo $idproduct_term; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $serial_number ); ?>" onkeyup="" />
														<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onclick="updateserial(<?php echo $idproduct_term; ?>, $('#serial_<?php echo $idproduct_term; ?>').val() );" />
														<?php
														
													}
													
												?>
												</td>
												 <td class="lefterCol">
                                                  <form action="<?php echo $GLOBAL_START_URL ?>/sales_force/product_term_list.php" method="post" name="adm_estim" name="adm_estim" enctype="multipart/form-data" ><input type="hidden" name="uploader" id="uploader" value="1" />
                                                 <input type="file" class="" name="documents" id="documents" value="" />
                                                <input type="submit" class="blueButton" value="Rechercher" />  </form>
                                                <br>
                                                 <br>
                                           <a href="/data/certificat/<?php echo  $idproduct_term ?>/<?php echo  $doc_certificat ?>">    <?php echo  $doc_certificat ?></a>
                                                 </td>
												<td style="white-space:nowrap;">
															<input size="10" onkeyup="if( !this.value.length || this.value.length == 10 ) SetDate( this );" type="text" name="availability_date_<?php echo $idproduct_term ?>" id="availability_date_<?php echo $idproduct_term ?>" value="<?php echo usDate2eu( $term_date ) ?>" class="calendarInput" /><?php echo DHTMLCalendar::calendar( "availability_date_$idproduct_term", true, "SetDate" ) ?>
														</td>
														 <td class="lefterCol">
                                                           <?php if ($service_extern == 1 ) {?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton gris.png" alt="gris" width="20px" height="20px" /><?php }else{  if (($Nombres_jours < 30) && ($Nombres_jours > 2) ){ ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton jaune.png" alt="jaune" width="20px" height="20px" /><?php }
														 if ($Nombres_jours == 1 ){ ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton vert.png" alt="vert" width="20px" height="20px" />
                                                      <?php    }
														   if ($Nombres_jours < 0 ){  ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bouton rouge.jpg" alt="rouge" width="20px" height="20px" />
														   <?php
														   }
														   
														   } ?>
                                                         </td>
                                             <!--   <td id="celldelete_<?php echo $idproduct_term ?>"><input type="image" name="DeleteOrder" id="DeleteOrder" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="Supprimer" onclick="return confirmDeleteOrder( <?php echo $idproduct_term ?>);" /></td>-->
											</tr>

				
<?php
				
				$rs->MoveNext();
				
			}
			
?>
</tbody>
                    	</table>
                        <div class="spacer"></div>
               		</div>
	
	</div></div>
	<div style="clear:both;"></div>
	<?php
			
		}else{ //pas de résultat
			
?>
				<div class="blocResult mainContent">
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Aucun résultat ne correspond à votre recherche</p>
					</div>
				</div>
<?php
		
		}
		
		paginate();

		//-------------------------------------------------------------------------------------------
		
		$lang = User::getInstance()->getLang();
		
		$nbtotal = 0;
		$totalorder = 0.0;
		
		//Commandes en attente

		$nbstandby=0;
		$totalstandby=0;
		
		//Commandes confirmées

		$nbconfirmed=0;
		$totalconfirmed=0;
		
		//Commandes envoyées

		$nbsent=0;
		$totalsent=0;
		
		//Commandes expédiées par le fournisseur

		$nbdispatch=0;
		$totaldispatch=0;
		
		//Commandes réceptionnées

		$nbdelivered=0;
		$totaldelivered=0;
		
		// Factures receptionnées
		$nbBillingRecept=0;
		$totalBillingRecept=0;
		
		//commandes annulées
		$cancelled = 0;
		
		$rs->MoveFirst();
		while( !$rs->EOF() ){
		
			switch( $rs->fields( "order_supplier_status" ) ){
			
				case "standby" : //Commandes en attente
				
					$nbstandby++;
					$totalstandby += $rs->Fields("total_amount_ht");
					
					break;
				
				case "confirmed" : //Commandes confirmées
				
					$nbconfirmed++;
					$totalconfirmed += $rs->Fields("total_amount_ht");
					
					break;

				case "SendByMail" : //Commandes envoyées
				case "SendByFax" :
				
					$nbsent++;
					$totalsent += $rs->Fields("total_amount_ht");
					
					break;
			
				case "dispatch" ://Commandes expédiées
	
					$nbdispatch++;
					$totaldispatch += $rs->Fields("total_amount_ht");
					
					break;
					
				case "received" ://Commandes réceptionnées
	
					$nbdelivered++;
					$totaldelivered += $rs->Fields("total_amount_ht");
					
					break;
				
				case "invoiced" ://Commandes facturée
					$nbdelivered++;
					$nbBillingRecept++;
					$totaldelivered += $rs->Fields("total_amount_ht");
					
					break;
				
				case "cancelled" : $cancelled++; break; //commande annulée
					
			}
			
			$nbtotal++;
			
			$rs->MoveNext();
			
		}

		$totalorder = $totalstandby + $totalconfirmed + $totalsent + $totaldispatch + $totaldelivered + $totalBillingRecept ;
		
		if( $nbtotal > 0 && $totalorder > 0 ){
			
			//Ration commandé/total
			$ratio_nb = ( $nbstandby + $nbconfirmed + $nbsent + $nbdelivered ) * 100 / $nbtotal;
			$ratio_order = ( $totalstandby + $totalconfirmed + $totalsent + $totaldelivered ) * 100 / $totalorder;
			
			//Commande moyenne
			$avg = $totalorder / $nbtotal;
			
		}else{
			
			$ratio_nb = 0;
			$ratio_order = 0;
			$avg = 0;
			
		}
		
		//-------------------------------------------------------------------------------------------
		
	
				
}	
		
	
	

	
?>
               	</form>
            </div>
<a name="bottom"></a>
</div>
<div class="spacer"></div><br/>
</div>       		      	
<div class="spacer"></div>   
		


			
		
<?php

//------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/sales_force" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/sales_force" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function GenerateHTMLForToolTipBox( $idorder_supplier, $suppliername ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT b.idbuyer, b.company 
	FROM buyer b, order_supplier os 
	WHERE os.idbuyer = b.idbuyer 
	AND os.idorder_supplier = '$idorder_supplier' 
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom de la société pour la commande '$idorder_supplier'" );
		
	$company = $rs->fields( "company" );
	$idbuyer = $rs->fields( "idbuyer" );
	
	//TODO : jointure entre order_supplier_row et order_row approximative
	
	$query = "
	SELECT osr.idrow, osr.reference, osr.ref_supplier, osr.quantity, osr.discount_price, osr.summary, 
	COALESCE( orow.discount_price, 0.0 ) AS pv, 
	COALESCE( orow.quantity, 0 ) AS qte 
	FROM order_supplier os
	LEFT JOIN order_supplier_row osr ON( osr.idorder_supplier = os.idorder_supplier )
	LEFT JOIN `order` o ON o.idorder = os.idorder
	LEFT JOIN order_row orow ON orow.idorder = o.idorder 
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND ( o.idorder IS NULL OR orow.idrow IS NULL OR orow.idrow = osr.idorder_row )";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les infos des lignes de commande" );
	
	$html = "<span onmouseover=\"document.getElementById('pop$idorder_supplier').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idorder_supplier').className='tooltip';\" style=\"position:relative;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/help.png\" alt=\"voir le détail\" class=\"tooltipa VCenteredWithText\" />";
	
	$html .= "<span id='pop$idorder_supplier' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
	<div class=\"headTitle\">
		<p class=\"title\">Client n° $idbuyer - $company</p>
		<div class=\"rightContainer\"></div>
	</div>
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	if( $rs->RecordCount() ){
		
		$html .= "<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
			<thead>
			<tr>
				<th>".Dictionnary::translate('reference')."</th>
				<th>".Dictionnary::translate('gest_com_ref_supplier')."</th>
				<th>".Dictionnary::translate('product')."</th>
				<th>".Dictionnary::translate('supplier')."</td>
				<th>".Dictionnary::translate('gest_com_quantity')."</th>
				<th>".Dictionnary::translate('gest_com_PHAUHT')."</th>
				<th>".Dictionnary::translate('gest_com_buying_MTHAHT')."</th>
				<th>".Dictionnary::translate('gest_com_MTVTHT')."</th>
				<th>".Dictionnary::translate('gest_com_rough_strock')."</th>
				<th>".Dictionnary::translate('gest_com_rough_strock')." %</th>
			</tr>
			</thead>
			<tbody>\n";
		
		while( !$rs->EOF() ){
			
			$TitrePdt = $rs->fields('summary');
			$MTHA = $rs->fields('discount_price')*$rs->fields('quantity');
			$MTVT = $rs->fields('pv')*$rs->fields('qte');
			$MB = $MTVT-$MTHA;
			if($MTHA>0 AND $MTVT>0){
				$M=(1-($MTHA/$MTVT))*100;
			}else{
				$M=0;
			}
		
			$html.= "
			<tr>
				<td class=\"lefterCol\">" . $rs->fields('reference') . "</td>
				<td>" . $rs->fields('ref_supplier') . "</td>
				<td style=\"white-space:nowrap;\">$TitrePdt</td>
				<td style=\"white-space:nowrap;\">$suppliername</td>
				<td>" . $rs->fields('quantity') . "</td>
				<td>" . Util::numberFormat($rs->fields('discount_price')) . "</td>
				<td>" . Util::numberFormat($MTHA) . "</td>
				<td>" . Util::numberFormat($MTVT) . "</td>
				<td>" . Util::numberFormat($MB) . "</td>
				<td class=\"righterCol\">" . Util::numberFormat($M) . "</td>
			</tr>";
			
			$rs->MoveNext();
			
		}
		
		$html.= "	</tbody>
	</table>\n";
		
	}else{
		
		$html .= "<div style=\"font-size: 12px; white-space:nowrap;\">Cette commande ne comporte aucun article.</div>";
		
	}
	
	$html .= "</div>
</div>
</div>
</span>
</span>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function DrawToolTipBox( $idorder_supplier, $suppliername ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT b.idbuyer, b.company 
	FROM buyer b, order_supplier os 
	WHERE os.idbuyer = b.idbuyer 
	AND os.idorder_supplier = '$idorder_supplier' 
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom de la société pour la commande '$idorder_supplier'" );
		
	$company = $rs->fields( "company" );
	$idbuyer = $rs->fields( "idbuyer" );
	
	//TODO : jointure entre order_supplier_row et order_row approximative
	
	$query = "
	SELECT DISTINCT( osr.idrow ), osr.reference, osr.ref_supplier, osr.quantity, osr.discount_price, osr.summary, orow.discount_price AS pv, orow.quantity AS qte 
	FROM order_supplier_row osr, order_row orow, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND osr.idorder_supplier = os.idorder_supplier
	AND orow.idorder = os.idorder
	AND orow.reference = osr.reference";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les infos des lignes de commande" );
	
	$html="
	<div id='pop$idorder_supplier' class='tooltip'>
	<table border='0' cellspacing='0' align='center'>
			<tr>
			<td colspan=\"10\" style=\"text-align:center;\"><b>" . Dictionnary::translate( "gest_com_search_idbuyer" ) ." ".$idbuyer."</b> - ".$company."</td>
			</tr>
			<tr>
			<td colspan=\"10\">&nbsp;</td>
			</tr>
			<tr>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('reference')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_ref_supplier')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('product')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('supplier')."</td>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_quantity')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_PHAUHT')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_buying_MTHAHT')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_MTVTHT')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_rough_strock')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_rough_strock')." %</th>
			</tr>";
	
	while( !$rs->EOF() ){
		
		$TitrePdt = $rs->fields('summary');
		$MTHA = $rs->fields('discount_price')*$rs->fields('quantity');
		$MTVT = $rs->fields('pv')*$rs->fields('qte');
		$MB = $MTVT-$MTHA;
		if($MTHA>0 AND $MTVT>0){
			$M=(1-($MTHA/$MTVT))*100;
		}else{
			$M=0;
		}
	
		$html.= "
		<tr>
			<td style=\"text-align:center;\">" . $rs->fields('reference') . "</td>
			<td style=\"text-align:center;\">" . $rs->fields('ref_supplier') . "</td>
			<td  style=\"text-align:center;\"  style=\"white-space:nowrap;\">$TitrePdt</td>
			<td  style=\"text-align:center;\"  style=\"white-space:nowrap;\">$suppliername</td>
			<td style=\"text-align:center;\">" . $rs->fields('quantity') . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($rs->fields('discount_price')) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($MTHA) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($MTVT) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($MB) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($M) . "</td>
		</tr>";
		
		$rs->MoveNext();
		
	}
	
	$html.= "</table>
	</div>";
	
	echo $html;
	
}

//--------------------------------------------------------------------------------------------------

function GenerateHTMLForToolTipBoxTel( $idorder_supplier ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT sc.phonenumber 
	FROM supplier s, supplier_contact sc, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";

	$rs = $db->Execute( $query );
	/*
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le numéro de téléphone du fournisseur" );
		
	$tel = $rs->fields( "phonenumber" );
	*/
	
	$html = "<span onmouseover=\"document.getElementById('poptel$idorder_supplier').className='tooltiphover';\" onmouseout=\"document.getElementById('poptel$idorder_supplier').className='tooltip';\" style=\"position:relative;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/help.png\" alt=\"voir le détail\" class=\"tooltipa\" />";
	
	$html .= "<span id='poptel$idorder_supplier' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	
	$html .= "	<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
		<thead>
		<tr>
			<th>".Dictionnary::translate('phone')."</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class=\"lefterCol righterCol\">$tel</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	</span>
	</span>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function DrawToolTipBoxTel( $idorder_supplier ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT sc.phonenumber 
	FROM supplier s, supplier_contact sc, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le numéro de téléphone du fournisseur" );
		
	$tel = $rs->fields( "phonenumber" );
	
	$html="
	<div id='poptel$idorder_supplier' class='tooltip'>
	<table border='0' cellspacing='0' align='center'>
		<tr>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('phone')."</th>
		</tr>
		<tr>
			<td style=\"text-align:center;\"  style=\"white-space:nowrap;\">" . $tel . "</td>
		</tr>
	</table>
	</div>
	";
	
	echo $html;
	
}
//--------------------------------------------------------------------------------------------------

function GenerateHTMLForOrderToolTipBox( $idorders ){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT o.status, c.idbuyer, c.lastname, c.firstname, c.phonenumber " .
			"FROM contact c, buyer b,`order` o, order_supplier os " .
			"WHERE os.idorder_supplier=$idorders " .
			"AND o.idorder = os.idorder " .
			"AND o.idbuyer = c.idbuyer " .
			"AND o.idcontact = c.idcontact";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_recover_information_order") );				
	
	$html="<div onMouseOver=\"document.getElementById('popord$idorders').className='tooltipordhover';\" onMouseOut=\"document.getElementById('popord$idorders').className='tooltipord';\" class='tooltiporda'><img class=\"helpicon\" src=\"$GLOBAL_START_URL/images/back_office/content/icon_help.gif\" alt=\"\" border=\"0\" />";
	
	$html.="<div id='popord$idorders' class='tooltipord'><table border='0' cellspacing='0' align='center'><tr><th>&nbsp;".Dictionnary::translate('status')."&nbsp;</th><th>&nbsp;".Dictionnary::translate('idbuyer')."&nbsp;</th><th>&nbsp;".Dictionnary::translate('lastname')."&nbsp;</th><th>&nbsp;".Dictionnary::translate('firstname')."&nbsp;</td><th>&nbsp;".Dictionnary::translate('phonenumber')."&nbsp;</th></tr>";
	
	$idbuyer = $rs->fields('idbuyer');
	$lastname = $rs->fields('lastname');	
	$firstname = $rs->fields('firstname');
	$phonenumber = $rs->fields('phonenumber');
	$status = Dictionnary::translate($rs->fields('status'));
	
	$html.= "<tr><td>";
	$html.= $status;
	$html.= "</td><td>";
	$html.= $idbuyer;
	$html.= "</td><td style=\"white-space:nowrap;\">";
	$html.= $lastname;
	$html.= "</td><td style=\"white-space:nowrap;\">";
	$html.= $firstname;
	$html.= "</td><td>";
	$html.= $phonenumber;
	$html.= "</td></tr>";
	
	
	$html.="</table></div></div>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function DrawOrderToolTipBox( $idorders ){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT o.status,c.idbuyer, c.lastname, c.firstname, c.phonenumber " .
			"FROM contact c, buyer b,`order` o, order_supplier os " .
			"WHERE os.idorder_supplier=$idorders " .
			"AND o.idorder = os.idorder " .
			"AND o.idbuyer = c.idbuyer " .
			"AND o.idcontact = c.idcontact";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_recover_information_order") );				
	
	$idbuyer = $rs->fields('idbuyer');
	$lastname = $rs->fields('lastname');	
	$firstname = $rs->fields('firstname');
	$phonenumber = $rs->fields('phonenumber');
	$status = Dictionnary::translate($rs->fields('status'));
	
	$html = "<span onmouseover=\"document.getElementById('popord$idorders').className='tooltiphover';\" onmouseout=\"document.getElementById('popord$idorders').className='tooltip';\" style=\"position:relative;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/help.png\" alt=\"voir le détail\" class=\"tooltipa\" />";
	
	$html .= "<span id='popord$idorders' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	
	$html .= "	<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
		<thead>
		<tr>
			<th>Statut</th>
			<th>Client n°</th>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Téléphone</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class=\"lefterCol\">$status</td>
			<td>$idbuyer</td>
			<td>$lastname</td>
			<td>$firstname</td>
			<td class=\"righterCol\">$phonenumber</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	</span>
	</span>";
	
	return $html;
	
}
//--------------------------------------------------------------------------------------------------

function getOrderStatus( $idorder ){
	
	
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT status FROM `order` WHERE idorder = '$idorder' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le status de la commande '$idorder'" );
	
	return $rs->fields( "status" );
	
}

//--------------------------------------------------------------------------------------------------

function getOrderSupplierTotalWeight( $idorder_supplier ){

	
	
	$db =& DBUtil::getConnection();
	
	$query = "
	SELECT SUM( weight ) AS weight
	FROM order_supplier_row
	WHERE idorder_supplier = '$idorder_supplier'";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le poids total pour la commande fournisseur '$idorder_supplier'<br />$query" );

	if( !$rs->RecordCount() || !$rs->fields( "weight" ) )
		return 0.0;
		
	return $rs->fields( "weight" );
	
}

//--------------------------------------------------------------------------------------------------

function ModifyDate(){
	global $GLOBAL_START_PATH;
	
	
	$idorders = $_GET[ "idos" ];
	$availability_date = $_GET[ "av" ];
		
	//$dispatch_date = $_GET[ "disp" ];
	
	//$Order = new SupplierOrder( $idorders );
	
	$regs = array();
	$pattern = "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/";
	
	if( preg_match( $pattern, $availability_date, $regs ) !== false ){
		
		$day = $regs[ 1 ];
		$month = $regs[ 2 ];
		$year = $regs[ 3 ];
		
		$availability_date = $year."-".$month."-".$day;
		
	}elseif( !empty( $availability_date ) ){
		
		echo "wrong av date format";
		return;
		
	}
	
	
	

			
	$qSuivi = "
		UPDATE product_term 
		SET term_date = '$availability_date' 
		
		WHERE idproduct_term = '$idorders'
		LIMIT 1"
	;
	
		$rsSuivi =& DBUtil::query( $qSuivi );
	
	
		if($rsSuivi === false){
			trigger_error( "Impossible de mettre à jour la date!", E_USER_ERROR );
			echo "wrong sql";
		return;
		}
	
	
 	echo "Confirmée";
	
}
 	if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "documents" ){
	$name_file = $_POST[ "documents" ];
		include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name_file . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			$idorder =  $_POST[ "idorder" ] ;
			$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./product_term_list.php\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term;
	
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		if( chmod( $dest, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	
	//exit( $ret === false ? "" : "1" );	
	}
		
}

function NbJours($debut, $fin) {

  $tDeb = explode("-", $debut);
  $tFin = explode("-", $fin);

  $diff = mktime(0, 0, 0, $tFin[1], $tFin[2], $tFin[0]) - 
          mktime(0, 0, 0, $tDeb[1], $tDeb[2], $tDeb[0]);
  
  return(($diff / 86400)+1);

}
	if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "documents" ){
	global $GLOBAL_START_PATH;
	$name_file = $_POST[ "documents" ];
		include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name_file . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			$idorder =  $_POST[ "idorder" ] ;
			$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./product_term_list.php\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term;
	
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		if( chmod( $dest, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	
	//exit( $ret === false ? "" : "1" );	
	}
		
}

//--------------------------------------------------------------------------------------------------

?>