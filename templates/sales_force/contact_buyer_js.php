<?php

if( isset( $_GET[ "check" ] ) && $_GET[ "check" ] == "email" ){
	
	include_once( "Validate.php" ); //classe PEAR
	
	if( !strlen( $_GET[ "email" ] ) || !Validate::email( $_GET[ "email" ] ) )
		exit( "Le format de l'adresse email est incorrect" );

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	//if( $idbuyer = DBUtil::getDBValue( "idbuyer", "contact", "mail", $_GET[ "email" ] ) )
		//exit( "Cette adresse email est déjà utilisée par le client n° $idbuyer" );
		
	exit( "1" );
	
}

?>
<script type="text/javascript" src="/js/jquery/json.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
				
				//---------------------------------------------------------------------------------------------
			
				//champs obligatoires dynamiques, en fonction du profil client
				
				var requiredElements = new Array();
				
				requiredElements[ 0 ] = new Array();
				requiredElements[ 1 ] = new Array();
<?php
					
					foreach( $requiredElements as $contact => $elements ){
						
						foreach( $elements as $idcustomer_profile => $elementNames ){
							
?>
				requiredElements[ <?php echo $contact ?> ][ <?php echo $idcustomer_profile ?> ] = new Array( <?php
							
							$i = 0;
							while( $i < count( $elementNames ) ){
								
								if( $i )
									echo ",";
								
								echo "'" . $elementNames[ $i ] . "'";
								
								$i++;
								
							}
							
							?> );
<?php
							
						}
						
					}
					
?>
				
				//couleur de fond des champs obligatoires
				var disabledBackgroundColor = '#E7E7E7';
				
				var defaultBackgroundColor = '#FFFFFF';
				var defaultColor = '#505050';
				
				var requiredBackgroundColor = '#D12524';
				var requiredColor = '#505050';
				
				var errorBackgroundColor = '#FFFFFF';
				var errorColor = '#505050';
				
				//---------------------------------------------------------------------------------------------
				
				function resetForm(){
					
					var form = document.forms.frm;
					var elements = form.elements;
					
					var i = 0;
					while( i < elements.length ){
						
						setRequiredElement( elements[ i ], false );
						
						i++;
						
					}
					
					form.elements[ 'Phonenumber' ].style.border = "1px solid "+requiredBackgroundColor;
					form.elements[ 'Faxnumber' ].style.border = "1px solid "+requiredBackgroundColor;
					form.elements[ 'Email' ].style.border = "1px solid "+requiredBackgroundColor;
					form.elements[ 'gsm' ].style.border = "1px solid "+requiredBackgroundColor;
					
					form.elements[ 'Phonenumber' ].style.color = requiredColor;
					form.elements[ 'Faxnumber' ].style.color = requiredColor;
					form.elements[ 'Email' ].style.color = requiredColor;
					form.elements[ 'gsm' ].style.color = requiredColor;
					
					
					if( form.elements[ 'Phonenumber' ].value != '<?php echo $phonenumber_default ?>' && form.elements[ 'Phonenumber' ].value != '' ) {
						//form.elements[ 'gsm' ].style.backgroundColor = defaultBackgroundColor;
						form.elements[ 'gsm' ].style.color = defaultColor;
					}
					
					if( form.elements[ 'gsm' ].value != '<?php echo $gsm_default ?>' && form.elements[ 'gsm' ].value != '' ) {
						//form.elements[ 'Phonenumber' ].style.backgroundColor = defaultBackgroundColor;
						form.elements[ 'Phonenumber' ].style.color = defaultColor;
					}
					
					if( form.elements[ 'Faxnumber' ].value != '<?php echo $faxnumber_default ?>' && form.elements[ 'Faxnumber' ].value != '' ) {
						//form.elements[ 'Email' ].style.backgroundColor = defaultBackgroundColor;
						form.elements[ 'Email' ].style.color = defaultColor;
					}
						
					if( form.elements[ 'Email' ].value != '<?php echo $email_default ?>' && form.elements[ 'Email' ].value != '' ) {
						//form.elements[ 'Faxnumber' ].style.backgroundColor = defaultBackgroundColor;
						form.elements[ 'Faxnumber' ].style.color = defaultColor;
					}
						
						
					//Code pour désactiver les champs quand aucun profil n'est sélectionné
					
					
					var idcustomer_profile = form.elements[ 'idcustomer_profile' ][ form.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					
					if( idcustomer_profile == "-1" ){
						
						var elements = document.forms.frm.elements;
						var i = 0;
						var element = false;
						var passed = false;
						
						while( i < elements.length ){
							
							element = elements[ i ];
							
							if( !element.type || element.type != 'button' && element.type != 'submit' && element.type != 'image' && passed ){
								
								element.disabled = true;
								//element.style.background = disabledBackgroundColor;
								
							}
							
							if( element == form.elements[ 'idcustomer_profile' ] )
								passed = true;
							
							i++;
							
						}
						
					}else{
						
						var elements = document.forms.frm.elements;
						var i = 0;
						var element = false;
						var passed = false;
						
						while( i < elements.length ){
							
							element = elements[ i ];
							
							if( !element.type || element.type != 'button' && element.type != 'submit' && element.type != 'image' && passed )
								element.disabled = false;
							
							if( element == form.elements[ 'idcustomer_profile' ] )
								passed = true;
							
							i++;
							
						}
						
					}
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				
				//met en évidence les champs obligatoires du formulaire
				
				function initRequiredElements(){
					
					resetForm();
					
					var form = document.forms.frm;
					var idcustomer_profile = form.elements[ 'idcustomer_profile' ][ form.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					var contact = <?php echo $type == "contact" ? "1" : "0" ?>;
					
					var i = 0;
					while( i < form.elements.length ){
						
						if( form.elements[ i ].getAttribute( 'name' ) != null && isRequired( form.elements[ i ].getAttribute( 'name' ), contact, idcustomer_profile ) && ( !form.elements[ i ].type || form.elements[ i ].type != 'hidden' ) )
							setRequiredElement( form.elements[ i ], true );
						
						i++;
						
					}
					
					//Fixe ou mobile
					if( document.getElementById( "Phonenumber" ).value == "" && document.getElementById( "gsm" ).value == "" ){
						
						document.getElementById( "Phonenumber" ).value = "<?php echo $phonenumber_default ?>";
						document.getElementById( "gsm" ).value = "<?php echo $gsm_default ?>";
						
					}
					
					//Fax ou email
					if( document.getElementById( "Faxnumber" ).value == "" && document.getElementById( "Email" ).value == "" ){
						
						document.getElementById( "Faxnumber" ).value = "<?php echo $faxnumber_default ?>";
						document.getElementById( "Email" ).value = "<?php echo $email_default ?>";
						
					}
					
					//TVA Intracommunautaire
					
					/*
					if( isVATRequired() )
						addRequiredElement( document.forms.frm.elements[ 'Tva' ] );
					else
						removeRequiredElement( document.forms.frm.elements[ 'Tva' ] );
					*/
					//SIRET et code NAF
					/*
					if( isSirenAndAPERequired() ){
						
						addRequiredElement( document.forms.frm.elements[ 'siren' ] );

						addRequiredElement( document.forms.frm.elements[ 'Naf' ] );
						
					}else{
						
						removeRequiredElement( document.forms.frm.elements[ 'siren' ] );
						removeRequiredElement( document.forms.frm.elements[ 'Naf' ] );
						
					}
					*/
					
					// masquage des tableaux et div non nécessaires au particulier
					if(idcustomer_profile == 1){
						document.getElementById('notPartTable1').style.display = 'none';
						document.getElementById('notPartTable2').style.display = 'none';
						document.getElementById('notPartTable3').style.display = 'none';
						document.getElementById('notPartTable4').style.display = 'none';
						document.getElementById('notPartTable5').style.display = 'none';
						document.getElementById('notPartTable6').style.display = 'none';
						document.getElementById('notPartTable7').style.display = 'none';
						document.getElementById('spacer1').style.display = 'none';
						document.getElementById('spacer2').style.display = 'none';
						document.getElementById('facturationBloc').className = 'blocMultiple blocMRight';
						document.getElementById('function').style.display = 'none';
						// Masquage de l'onglet Infos Société
						document.getElementById('menuInfoSociete').style.display = 'none';
					}else{
						document.getElementById('notPartTable1').style.display = 'block';
						document.getElementById('notPartTable2').style.display = 'block';
						document.getElementById('notPartTable3').style.display = 'block';
						document.getElementById('notPartTable4').style.display = 'block';
						document.getElementById('notPartTable5').style.display = 'block';
						document.getElementById('notPartTable6').style.display = 'block';
						document.getElementById('notPartTable7').style.display = 'block';
						document.getElementById('spacer1').style.display = 'block';
						document.getElementById('spacer2').style.display = 'block';
						document.getElementById('facturationBloc').className = 'blocMultiple blocMLeft';
						document.getElementById('function').style.display = 'block';
					}
					
					
					// En fonction du type de client on affiche le module manageo ou bien le module page blanche
					if(idcustomer_profile > 1 ){
						document.getElementById('contener_manageo').style.display = 'block' ; 
						document.getElementById('contener_page_blanche').style.display = 'none' ;
						document.getElementById('menuInfoSociete').style.display = 'block';
						document.getElementById('lbl_raison_soc').innerHTML = 'Raison sociale';
						document.getElementById('blocNC').style.display = 'block';
						//document.getElementById('infosDeOufs').style.display = 'block';
						document.getElementById('contenerSearchManageo1').style.display = 'block';
						document.getElementById('contenerSearchManageo2').style.display = 'block';
						document.getElementById('notMairie').style.display = 'block';
					}
				
					if(idcustomer_profile == 1 ){
						document.getElementById('contener_page_blanche').style.display = 'block' ; 
						document.getElementById('contener_manageo').style.display = 'none' ; 
					}
					
					if(idcustomer_profile == 0 ){
						document.getElementById('contener_page_blanche').style.display = 'block' ; 
						document.getElementById('contener_manageo').style.display = 'block' ; 
					}

					if( idcustomer_profile == 4 ){
						document.getElementById('menuInfoSociete').style.display = 'none';
					}

					
					// Cas d'une mairie idcustomer_profile = 7
					if(idcustomer_profile == 7){
						// Affichage du module Annuaire Mairie
					//	document.getElementById('contener_communes').style.display = 'block';
						document.getElementById('contenerSearchManageo1').style.display = 'none';
						document.getElementById('contenerSearchManageo2').style.display = 'none';
						
						// Masquage du champs Raison Sociale et Nom Commerciale
						document.getElementById('blocNC').style.display = 'none';
						document.getElementById('lbl_raison_soc').innerHTML = 'Mairie'; 

						document.getElementById('menuInfoSociete').style.display = 'none';
						document.getElementById('infosFacultativesMairie').style.display = 'none';
						document.getElementById('infosPlusMairie').style.display = 'block';
						document.getElementById('infosDeOufs').style.display = 'none';
						document.getElementById('notMairie').style.display = 'none';
					}
					
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//rend un champ donné obligatoire ou non
				
				function setRequiredElement( element, required ){
					
					if( !required && (!element.type || element.type != 'button' && element.type != 'submit' && element.type != 'image')) {
						element.style.background = "#FFFFFF";
						element.style.color = defaultColor;
					}
			
					else if( required && (!element.type || element.type != 'button' && element.type != 'submit' && element.type != 'image')) {
						element.style.border = " 1px solid "+requiredBackgroundColor;
						element.style.color = requiredColor;
					}
					
					var nextSibling = element.nextSibling;
					
//					if( !required && nextSibling && nextSibling.title && nextSibling.title == 'Required Symbol' )
//						element.parentNode.removeChild( nextSibling );
//					
//					if( required ){
//						
//						var mul = document.createElement( 'span' );
//						
//						mul.setAttribute( 'title', 'Required Symbol' );
//						mul.style.color = '#FF0000';
//						mul.style.fontWeight = 'bold';
//						mul.style.fontSize = '14px';
//						mul.style.padding = '3px';
//						mul.innerHTML = '*';
//						
//						if( nextSibling )
//							element.parentNode.insertBefore( mul, nextSibling );
//						else
//							element.parentNode.appendChild( mul );
//						
//					}
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//Return true si un nom de champ donné est obligatoire un type de client donné et une valeur donnée de idcustomer_profile
				
				function isRequired( elementName, contact, idcustomer_profile ){
					
					if( elementName == null || elementName.length == 0 )
						return false;
					
					var j = 0;
					while( j < requiredElements[ contact ][ idcustomer_profile ].length ){
						
						if( requiredElements[ contact ][ idcustomer_profile ][ j ].toLowerCase() == elementName.toLowerCase() )
							return true;
						
						j++;
						
					}
					
					return false;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si tous les champs obligatoires ont été renseignés pour un type de client donné
				
				function checkRequiredElements( contact ){
					
					if( document.getElementById( "GenerateLogin" ) && document.getElementById( "GenerateLogin" ).checked && document.getElementById( "Email" ).value == "" ){
						
						alert( "Vous devez saisir une adresse email pour créer un compte pour le Front Office." );
						return false;
						
					}
					
					var form = document.forms.frm;
					var idcustomer_profile = form.elements[ 'idcustomer_profile' ][ form.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					
					//champs dynamiques
					
					var hasError = false;
					var i = 0;
					while( i < form.elements.length ){
						
						var element = form.elements[ i ];
						
						if( element.getAttribute( 'name' ) != null && isRequired( element.getAttribute( 'name' ), contact, idcustomer_profile ) ){
							
							if( !isFilled( element ) ){ //le champ obligatoire n'est pas renseigné
								
								//element.style.backgroundColor = errorBackgroundColor;
								element.style.color = errorColor;
								
								hasError = true;
								
							}else{ //le champ obligatoire est bien renseigné
								
								element.style.border = "1px solid "+requiredBackgroundColor;
								element.style.color = requiredColor;
								
							}
							
							if( element.getAttribute( 'name' ) == "siret" && !checkSiren( form.elements[ 'siren' ].value ) ){
								
								alert( "Le numéro de Siren que vous avez renseigné n'est pas valide." );
								
								//element.style.backgroundColor = errorBackgroundColor;
								element.style.color = errorColor;
								
								hasError = true;
								
							}
							
						}
						
						if( element.getAttribute( 'name' ) != null && element.getAttribute( 'name' ) == 'Phonenumber' && element.value == '<?php echo $phonenumber_default ?>' && form.elements[ 'gsm' ].value == '<?php echo $gsm_default ?>' ){
							
							//element.style.backgroundColor = errorBackgroundColor;
							element.style.color = errorColor;
							
						}
						
						if( element.getAttribute( 'name' ) != null && element.getAttribute( 'name' ) == 'gsm' && element.value == '<?php echo $gsm_default ?>' && form.elements[ 'Phonenumber' ].value == '<?php echo $phonenumber_default ?>' ){
							
							//element.style.backgroundColor = errorBackgroundColor;
							element.style.color = errorColor;
							
						}
						
						if( element.getAttribute( 'name' ) != null && element.getAttribute( 'name' ) == 'Faxnumber' && element.value == '<?php echo $faxnumber_default ?>' && form.elements[ 'Email' ].value == '<?php echo $email_default ?>' ){
							
							//element.style.backgroundColor = errorBackgroundColor;
							element.style.color = errorColor;
							
						}
						
						if( element.getAttribute( 'name' ) != null && element.getAttribute( 'name' ) == 'Email' && element.value == '<?php echo $email_default ?>' && form.elements[ 'Faxnumber' ].value == '<?php echo $faxnumber_default ?>' ){
							
							//element.style.backgroundColor = errorBackgroundColor;
							element.style.color = errorColor;
							
						}
						
						if( element.getAttribute( 'name' ) != null && element.getAttribute( 'name' ) == 'title' && element.value == '' && form.elements[ 'title' ].value == '' ){
							
							//element.style.backgroundColor = errorBackgroundColor;
							element.style.color = errorColor;
							
						}
						
						i++;
						
					}
					
					if( hasError )
						alert( '<?php echo str_replace( "'", "\'", "Certains champs obligatoires dans l'onglet Coordonnées n'ont pas été renseignés" ) ?>' );
					
					return !hasError;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function addRequiredElement( element ){
					
					var idcustomer_profile 	= document.forms.frm.elements[ 'idcustomer_profile' ][ document.forms.frm.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					var contact = <?php echo $type == "contact" ? "1" : "0" ?>;
					
					if( isRequired( element.name, contact, idcustomer_profile ) )
						return;
					
					var requiredElementCount = requiredElements[ contact ][ idcustomer_profile ].length;
					requiredElements[ contact ][ idcustomer_profile ][ requiredElementCount ] = element.name;
					
					setRequiredElement( element, true );
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function removeRequiredElement( element ){
					
					var idcustomer_profile 	= document.forms.frm.elements[ 'idcustomer_profile' ][ document.forms.frm.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					var contact = <?php echo $type == "contact" ? "1" : "0" ?>;
					
					if( !isRequired( element.name, contact, idcustomer_profile ) )
						return;
					
					var index = 0;
					var done = false;
					while( index < requiredElements[ contact ][ idcustomer_profile ].length && !done ){
						
						if( requiredElements[ contact ][ idcustomer_profile ][ index ] == element.name )
							done = true;
						else
							index++;
						
					}
					
					requiredElements[ contact ][ idcustomer_profile ].splice( index, 1 );
					
					setRequiredElement( element, false );
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si le numéro SIRET et le code NAF sont obligatoires
				
				function isSirenAndAPERequired(){
					
<?php
					
					//contacts : Siret non requis
					
					if( $type == "contact" ){
						
?>
					return false;
<?php
						
					}else{ //clients
						
?>
					var selectedState 	= document.forms.frm.elements[ 'idstate' ].options[ document.forms.frm.elements[ 'idstate' ].selectedIndex ].value;
					var idcustomer_profile 	= document.forms.frm.elements[ 'idcustomer_profile' ][ document.forms.frm.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					
					//vérifier si le pays sélectionné utilise le Siren et le code NAF
					
					if( selectedState != 1 && selectedState != 44 && selectedState != 48 )
						return false;
					
					switch( idcustomer_profile ){
						
						case '2' :
						case '6' :
						case '5' : return true;
						
						default : return false;
					
					}
<?php
						
					}
					
?>
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si la TVA Intracommunautaire est obligatoire, sinon false
				//TVA Intracommunautaire obligatoire que pour le client et pour les profils 'Acheteur', 'Revendeur' et 'Grand Compte'
				
				function isVATRequired(){
					
<?php 
					
					//contacts : TVA intracommunautaire non requise
					
					if( $type == "contact" ){
						
?>
					return false;
<?php
						
					}else{ //clients
						
						//@todo corriger bidouille 'France H.T.' dans la table `state`
						
?>
					var euStates = new Array( <?php
						
						$rs = DBUtil::query( "SELECT idstate FROM state WHERE code_eu LIKE 'INTRA'" );
						
						$i = 0;
						while( !$rs->EOF() ){
							
							if( $i )
								echo ",";
							
							echo $rs->fields( "idstate" );
							
							$rs->MoveNext();
							$i++;
							
						}
						
						?>);
					
					var selectedState 	= document.forms.frm.elements[ 'idstate' ].options[ document.forms.frm.elements[ 'idstate' ].selectedIndex ].value;
					var idcustomer_profile 	= document.forms.frm.elements[ 'idcustomer_profile' ][ document.forms.frm.elements[ 'idcustomer_profile' ].selectedIndex ].value;
					
					//vérifier si le pays sélectionné est intracommunautaire
					
					var i = 0;
					var euStateSelected = false;
					while( !euStateSelected && i < euStates.length ){
						
						if( selectedState == euStates[ i ] ) //TVA intracommunautaire non requise pour l
							euStateSelected = true;
						
						i++;
						
					}
					
					if( !euStateSelected )
						return false;
					
					switch( idcustomer_profile ){
						
						case '2' :
						case '6' :
						case '5' : return true;
						
						default : return false;
						
					}
<?php
						
					}
					
?>
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function selectCity( zipcode ){
				
					var xhr = getXMLHttpRequest();
					
					xhr.onreadystatechange = function(){
			
						if( xhr.readyState == 4 && xhr.status == 200 ){
						
							if( !xhr.responseXML )
								return;
	
							createCitySelection( xhr.responseXML );
							
						}
						
					}
				
					var url = '<?php echo $GLOBAL_START_URL ?>/sales_force/commune.php?zipcode=' + zipcode;
		
					xhr.open( "GET", url, true );
					xhr.send( null );
					
				}
				
				//------------------------------------------------------------------------------------------------
				
				function createCitySelection( xmlDocument ){
					
					var target = document.getElementById( 'City' );
					var communes = xmlDocument.getElementsByTagName( 'commune' );
			
					if( !communes.length )
						return;
						
					if( communes.length == 1 ){
					
						target.value = communes[ 0 ].getAttribute( 'value' );
						return;
						
					}
					
					var selection = document.getElementById( 'CitySelection' );
	
					var innerHTML 	='<p style="background-color:#EAEAEA; padding:7px; font-weight:bold;">Communes proposées</p>';
					innerHTML 		+= '<div style="overflow:auto; height:120px;">';
					innerHTML 		+= '<ul style="list-style-type:none;">';
					
					var i = 0;
					while( i < communes.length ){
	
						var commune = communes[ i ].getAttribute( 'value' );
						
						innerHTML += '<li style="text-align:left;">';
						innerHTML += '<input type="radio" onclick="document.getElementById( \'City\' ).value = \'' + commune + '\'; setTimeout( $.unblockUI, 10 );" />&nbsp;';
						innerHTML += '<a style="font-size:11px; text-decoration:none;" href="#" class="blueLink" onclick="document.getElementById( \'City\' ).value = \'' + commune + '\'; setTimeout( $.unblockUI, 10 ); return false;">';
						innerHTML += commune;
						innerHTML += '</a>';
						innerHTML += '</li>';
						
						i++;
						
					}
					
					innerHTML += '</ul>';
					innerHTML += '</div>';
					innerHTML += '<p style="text-align:right;">';
					innerHTML += '<input type="button" value="Annuler" class="blueButton" onclick="setTimeout( $.unblockUI, 10 );" />';
					innerHTML += '</p>';
					
					selection.innerHTML = innerHTML;
					
					$.blockUI({ 
	
						message: $('#CitySelection'),
						css: { padding: '15px', cursor: 'pointer' }
						
					});
						
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si au moins un des 2 champs 'email' ou 'fax' est bien renseigné
				
				function checkEmailOrFax(){
					
					var email = document.forms.frm.elements[ 'Email' ];
					var fax = document.forms.frm.elements[ 'Faxnumber' ];
					
					if( !isFilled( email ) && !isFilled( fax ) || fax.value == '<?php echo $faxnumber_default ?>' && email.value == '<?php echo $email_default ?>' ){ //le champ obligatoire n'est pas renseigné
						
						//email.style.backgroundColor = errorBackgroundColor;
						email.style.color = errorColor;
						//fax.style.backgroundColor = errorBackgroundColor;
						fax.style.color = errorColor;
						
						alert( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "gest_com_email_or_faxnumber_required" ) ) ?>' );
						
						return false;
						
					}
					
					//email.style.backgroundColor = requiredBackgroundColor;
					email.style.color = requiredColor;
					//fax.style.backgroundColor = requiredBackgroundColor;
					fax.style.color = requiredColor;
					
					return true;
					
				}

				//---------------------------------------------------------------------------------------------
				
				function checkSiren( siren ){

					siren = siren.replace( new RegExp( '[^0-9]*', 'g' ), '' );
					
					var checksum = 0;
					var split = 0;
					
					var i = 0;
					while( i < siren.length ){
						
						split = parseInt( siren.substring( i, i + 1 ) );
						
						if( i % 2 == 0 ){
							
							checksum += split;
							
						}else{
							
							split = split * 2;
							
							checksum += parseInt( String( split ).substring( 0, 1 ) );
							
							if( split > 10 )
								checksum += parseInt( String( split ).substring( 1, 2 ) );
						}
						
						i++;
						
					}
					
					if( checksum % 10 == 0 )
						return true;
					else
						return false;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//return true si au moins un des 2 champs 'téléphone' ou 'mobile' est bien renseigné
				
				function checkPhoneOrMobile(){
					
					var phone = document.forms.frm.elements[ 'Phonenumber' ];
					var gsm = document.forms.frm.elements[ 'gsm' ];
					
					if( !isFilled( phone ) && !isFilled( gsm ) || phone.value == '<?php echo $phonenumber_default ?>' && gsm.value == '<?php echo $gsm_default ?>' ){ //le champ obligatoire n'est pas renseigné
						
						//phone.style.backgroundColor = errorBackgroundColor;
						phone.style.color = errorColor;
						//gsm.style.backgroundColor = errorBackgroundColor;
						gsm.style.color = errorColor;
						
						alert( 'Merci de bien vouloir renseigner au moins un de ces 2 champs : numéro de téléphone OU de mobile' );
						
						return false;
						
					}
					
					//phone.style.backgroundColor = requiredBackgroundColor;
					phone.style.color = requiredColor;
					//gsm.style.backgroundColor = requiredBackgroundColor;
					gsm.style.color = requiredColor;
					
					return true;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function checkMail(){
					
					var email = document.forms.frm.elements[ 'EmailToCheck' ].value;
					
					if( email == '' )
						return;
					
					var options ='left=300, top=300, directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, toolbar=no, width=350, height=150';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/checkmail.php?type=<?php echo $type ?>&email=' + email;
					
					window.open( location, '_blank', options );
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				function checkFax(){
					
					var fax = document.forms.frm.elements[ 'FaxToCheck' ].value;
					
					if( fax == '' )
						return;
					
					var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=no, status=no, toolbar=no, width=350, height=150';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/checkfax.php?type=<?php echo $type ?>&fax=' + fax;
					
					window.open( location, '_blank', options ) ;
					
				}
				
				//---------------------------------------------------------------------------------------------
				
				//Pour le téléchargement du modèle
				function popupletter(key) {
					
					postop = (self.screen.height-300)/2;
					posleft = (self.screen.width-250)/2;
					var letter=document.getElementById('letter').value;
					var contact=document.getElementById('idContact').value;
					window.open('contact_letter.php?key='+key+'&letter='+letter+'&contact='+contact, '_blank', 'top=' + postop + ',left=' + posleft + ',width=250,height=300,resizable,scrollbars=yes,status=0');
					
				}
			
				//Pour créer un nouveau modèle
				function popupNewModelLetter(key) {
					
					postop = (self.screen.height-300)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_letter.php?key='+key+'&letter=new&contact=new', '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=300,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour supprimer un modèle
				function popupDelModelLetter(){
					
					var select=document.getElementById('letter');
					var letter=document.getElementById('letter').value;
					
					var index=letter.indexOf( ".", 0 );
					strletter=letter.substring( 0, index );
					
					var cherche = /balise/i;
					var resultat = cherche.test(strletter);
					
					if(!resultat){
						if(confirm("<?php echo Dictionnary::translate("gest_com_confirm_delete")." ";?>"+strletter)){
							request("",select,escape(letter));
						}
					}else{
						alert("<?php echo Dictionnary::translate("contact_ModelDellerror");?>"+strletter);
					}
				
				}
				
				//Pour supprimer un modèle sur l'affichage
				function popupDelDisplay(select){
					
					n=select.selectedIndex;
				
					for (i = n; i < Number(select.options.length-1); i++){
						 select.options[i] = new Option((select.options[ Number(i+1) ].text ),(select.options[ Number(i+1) ].value) );
					}
					
					select.options.length--; 
					
				}
				
				
				//Pour l'insertion de la lettre envoyée
				function popupletter2(key) {
					
					postop = (self.screen.height-400)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_letter2.php?key='+key, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=400,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour l'envoie de mail
				function popupmail(key) {
					
					postop = (self.screen.height-650)/2;
					posleft = (self.screen.width-900)/2;
					var documentation=document.getElementById('documentation').value;
					window.open('contact_mailer.php?doc='+documentation+'&key='+key, '_blank', 'top=' + postop + ',left=' + posleft + ',width=900,height=650,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour la reception de mail ainsi que de lettre
				function popupreceive(key,type){
					
					postop = (self.screen.height-550)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_receive.php?key='+key+'&type='+type, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=550,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour le suivi client <-> commercial.
				function popupFollow(key){
					
					postop = (self.screen.height-800)/2;
					posleft = (self.screen.width-820)/2;
					window.open('contact_follow.php?key='+key+'&resp=0', '_blank', 'top=' + postop + ',left=' + posleft + ',width=820,height=800,resizable,scrollbars=yes,status=0');
					
				}
				
				//Pour l'envoie réception des fax
				function popupfax(key,type){
					
					postop = (self.screen.height-550)/2;
					posleft = (self.screen.width-500)/2;
					window.open('contact_fax.php?key='+key+'&type='+type, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=550,resizable,scrollbars=yes,status=0');
					
				}
				
				////------------------------------Partie Ajax maj------------------------------
					
				function createXHR(){
					
					var req = null;  
			 		
					if(window.XMLHttpRequest){
			 			req = new XMLHttpRequest();
			
					}else if(window.ActiveXObject){
						
						try {
							req = new ActiveXObject("Msxml2.XMLHTTP");
						} catch (e)
						{
							try {
								req = new ActiveXObject("Microsoft.XMLHTTP");
							} catch (e) {}
						}
			        }
			        
			    	return req;
			    	
				}
				
				function request(type,select,name){
					
					var req = createXHR();
					req.onreadystatechange = function(){
						
						if(req.readyState == 4){
							
							if(req.status == 200){						
								
								req=req.responseText;
								
								if (req=="1")
									popupDelDisplay(select); //supprime le modèle à l'affichage
								else
									alert("<?php  echo Dictionnary::translate("gest_com_impossible_delete_file")?>");
								
							}else{
								
								alert("<?php  echo Dictionnary::translate("gest_com_impossible_delete_file")?>");
								
							}
							
						}
						
					}; 
				 	
					var data="filename="+name;
					req.open("POST", "contact_ajax.php", true);
					req.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
					req.send(data);
					
				}

		//---------------------------------------------------------------------------------------------
		// POMPAGE MANAGEO
		
		
	function clearManageo(){
		var divToComplete = document.getElementById('infosDeOufs') ;
		divToComplete.innerHTML = '';
	}
				
	function getInfos(){
					
		var divToComplete = document.getElementById('infosDeOufs') ;
		var companyName = document.getElementById('nameOfCompany').value ;
		var deptCompany = document.getElementById('departmentCompany').value ;
		var sirenToManageo = document.getElementById('sirenOfCompany').value.replace( new RegExp( "[^0-9]*", "g" ), "" ) ;
		
		// On lock les champs
		companyName.disabled = true ;
		deptCompany.disabled = true ;
	
		if( companyName == '' && sirenToManageo == '' ){
			alert("Vous devez renseigner une société ou bien le siren de celle-ci !");
			return;
		}
		
		var message = 'Recherche en cours ...' ;
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php';
	
		if( sirenToManageo != ''){
			var data = 'sirenManageo=' + sirenToManageo;
		}else{
			if( deptCompany != ""){
				var data = 'companyName=' + companyName + '&dept='+ deptCompany;
			}
			else{
				var data = 'companyName=' + companyName ;
			}
		}

		machineAjax( location, divToComplete, data, message );
		
		companyName.disabled = false ;
		deptCompany.disabled = false ;
	}

	/* ----------------------------------------------------------------------------------------------------------- */
	
	// POMPAGE MAIRIE
	function getInfosCommunes(){
					
		var divToComplete = document.getElementById('infosDeOufsCommune') ;

		if(document.getElementById('Company').value != ''){
			var nameOfCommune = document.getElementById('Company').value ;
		}
		else{
			var nameOfCommune = document.getElementById('nameOfCommune').value ;
		}

		// On lock les champs
		nameOfCommune.disabled = true ;
			
		if( nameOfCommune == ''){
			alert("Vous devez renseigner le nom de la commune !");
			return;
		}
		
		var message = 'Recherche en cours ...' ;
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos_mairie.php';
		var data = 'nameOfCommune=' + nameOfCommune ;
		
		machineAjax( location, divToComplete, data, message );
		
		nameOfCommune.disabled = false ;
	}
	
	// FIN POMPAGE MAIRIE

	/* ----------------------------------------------------------------------------------------------------------- */
	
	function getInfosNext(){
	
		var myUrl = document.getElementById("urlToRecup").value ;
		var divToComplete = document.getElementById('infosDeOufs') ;
			
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php';
		var data = 'selectUrl=' + myUrl;
		var message = 'Recherche en cours ...';
		
		machineAjax( location, divToComplete, data, message );
		
	}

	function manageo_str2num( string ){

		return string.replace( new RegExp( "[^0-9\.,-]", "g" ), "" );
		
	}
	
	function autoCompleteFromManageo(){ 

		var reg = new RegExp("(&nbsp;)", "g");

		// Onglet Infos société
		document.getElementById("status_company").value = document.getElementById("forme_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("nbr_etab").value = document.getElementById("etablissement_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("last_exo").value = document.getElementById("exercice_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("lenght_exo").value = document.getElementById("dure_exercice_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("cloture_exo").value = document.getElementById("cloture_ewercice_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("ca_from_web").value = format( manageo_str2num( document.getElementById("ca_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''), 0 , " " );
		document.getElementById("vala_web").value = format( manageo_str2num( document.getElementById("va_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''), 0, " " ) ; 
		document.getElementById("exed_exploit").value = format( manageo_str2num( document.getElementById("exed_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''), 0, " " ) ; 
		document.getElementById("renta_com").value = manageo_str2num( document.getElementById("rentabilite_com_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("renta_finan").value = manageo_str2num( document.getElementById("rentatbilite_finan_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("nom_boss").value = document.getElementById("nom_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("fonction_boss").value = document.getElementById("fonction_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("birthday_boss").value = document.getElementById("naissance_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("birthday_boss_city").value = document.getElementById("lieu_naissance_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// Fiche client
		if(document.getElementById("Adress").value.length == 0){
			document.getElementById("Adress").value = document.getElementById("adress_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace("<br>", "") ; 
		}
		if(document.getElementById("Zipcode").value.length == 0){
			document.getElementById("Zipcode").value = document.getElementById("adress_web_cp").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		}
		if(document.getElementById("City").value.length == 0){
			document.getElementById("City").value = document.getElementById("adress_web_city").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		}
		document.getElementById("NafText").value = document.getElementById("activite_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("Naf").value = document.getElementById("naf_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 

		if(document.getElementById("phone_standard").value.length == 0){
			document.getElementById("phone_standard").value = document.getElementById("tel_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		}

		if(document.getElementById("fax_standard").value.length == 0){
			document.getElementById("fax_standard").value = document.getElementById("fax_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		}
		
		document.getElementById("siret").value = document.getElementById("siret_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,""); 
		document.getElementById("siren").value = document.getElementById("siren_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,""); 
		document.getElementById("Company").value = document.getElementById("denomination").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("creationDate").value = document.getElementById("creation_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("companyResult").value = format( manageo_str2num( document.getElementById("resultat_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''), 0, " " ) ; 
		document.getElementById("companyCapital").value = format( manageo_str2num( document.getElementById("capital_web").innerHTML ).replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''), 0, " " ) ; 

		// On modifie le champs indiquant que manageo est passé
		//document.getElementById("manageoLoaded").value = 1;
		
		var effectif = document.getElementById("idmanpower") ;
		
		var effectifFromManageo = document.getElementById("effectif_web") ;
		var valueEffectif = effectifFromManageo.innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
				
		if( valueEffectif != "" ){
				
			if( valueEffectif == 0){
				//effectif.selectedIndex = 1 ; 
			}else if( (valueEffectif == 1) || (valueEffectif == 2) ){
				effectif.selectedIndex = 1 ; 
			}else if( (valueEffectif >= 3) && (valueEffectif <= 5) ){
				effectif.selectedIndex = 2; 
			}else if( (valueEffectif >= 6) && (valueEffectif <= 9) ){
				effectif.selectedIndex = 3; 
			}
			else if( (valueEffectif >= 10) && (valueEffectif <= 19) ){
				effectif.selectedIndex = 4; 
			}
			else if( (valueEffectif >= 20) && (valueEffectif <= 49) ){
				effectif.selectedIndex = 5; 
			}else if( (valueEffectif >= 50) && (valueEffectif <= 99) ){
				effectif.selectedIndex = 6; 
			}
			else if( (valueEffectif >= 100) && (valueEffectif <= 199) ){
				effectif.selectedIndex = 7; 
			}
			else if( (valueEffectif >= 200) && (valueEffectif <= 249) ){
				effectif.selectedIndex = 8; 
			}
			else if( (valueEffectif >= 250) && (valueEffectif <= 499) ){
				effectif.selectedIndex = 9; 
			}
			else if( (valueEffectif >= 500) && (valueEffectif <= 999) ){
				effectif.selectedIndex = 10; 
			}
			else if( valueEffectif >= 1000 ){
				effectif.selectedIndex = 11; 
			}
		}

	}
	

	function autoSearchOnManageo(){
		
		var divToComplete = document.getElementById('infosDeOufs') ;
		var companyName = document.getElementById('Company').value ;
		document.getElementById('nameOfCompany').value = companyName;
		var deptCompany = document.getElementById('Zipcode').value.substring(0,2) ;
		document.getElementById('departmentCompany').value = deptCompany;

		var sirenToManageo = document.getElementById('siren').value.replace( new RegExp( "[^0-9]*", "g" ), "" );
		document.getElementById('sirenOfCompany').value = sirenToManageo;
		
		if( companyName == '' && sirenToManageo == ''){
			return;
		}
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php';
		if( sirenToManageo != ''){
			var data = 'sirenManageo=' + sirenToManageo ;
		}else{
			if(deptCompany != ""){
				var data = 'companyName=' + companyName + '&dept='+ deptCompany ;
			}
			else{
				var data = 'companyName=' + companyName ;
			}
			
		}
		message = '';
		
		machineAjax( location, divToComplete, data, message );
	}
	
	function format(valeur,decimal,separateur) {
		// formate un chiffre avec 'decimal' chiffres après la virgule et un separateur
			var deci=Math.round( Math.pow(10,decimal)*(Math.abs(valeur)-Math.floor(Math.abs(valeur)))) ; 
			var val=Math.floor(Math.abs(valeur));
			if ((decimal==0)||(deci==Math.pow(10,decimal))) {val=Math.floor(Math.abs(valeur)); deci=0;}
			var val_format=val+"";
			var nb=val_format.length;
			for (var i=1;i<4;i++) {
				if (val>=Math.pow(10,(3*i))) {
					val_format=val_format.substring(0,nb-(3*i))+separateur+val_format.substring(nb-(3*i));
				}
			}
			if (decimal>0) {
				var decim=""; 
				for (var j=0;j<(decimal-deci.toString().length);j++) {decim+="0";}
				deci=decim+deci.toString();
				val_format=val_format+"."+deci;
			}
			if (parseFloat(valeur)<0) {val_format="-"+val_format;}
			return val_format;
	}
	
	
	//--------------------------------- FIN POMPAGE MANAGEO ------------------------------------------------------------
	//--------------------------------- POMPAGE PAGES BLANCHES ---------------------------------------------------------
	function getInfosFromPb(){
		
		var divToComplete = document.getElementById('responsePb') ;
		var nom = document.getElementById('pb_search_nom').value ;
		var prenom = document.getElementById('pb_search_prenom').value ;
		var cpVille = document.getElementById('pb_search_cp').value ;

		if( (nom == '' || cpVille == '') ){
			alert("Vous devez renseigner un nom ainsi qu'une ville ou bien un code postal !");
			return;
		}

		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/pages_blanches.php';
		var data = 'pb_search_nom=' + nom + '&pb_search_prenom='+ prenom + '&pb_search_cp='+ cpVille ;
		var message = 'Recherche en cours ...';
		
		machineAjax( location, divToComplete, data, message );
	
	}
	
	function completeFromPb(){
	
		var occurence = document.getElementById("selectedOccurence").value ;
		var reg = new RegExp("(&nbsp;)", "g");
		//alert(occurence) ;
		document.getElementById("buyerLastname").value = document.getElementById("pb_nom_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
		document.getElementById("buyerFirstname").value = document.getElementById("pb_prenom_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
		document.getElementById("Adress").value = document.getElementById("pb_rue_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
		document.getElementById("Zipcode").value = document.getElementById("pb_cp_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
		document.getElementById("City").value = document.getElementById("pb_ville_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
		document.getElementById("Phonenumber").value = document.getElementById("pb_tel_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
		document.getElementById("Faxnumber").value = document.getElementById("pb_fax_"+occurence).innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ;
	
	}
	
	//--------------------------------- FIN POMPAGE PAGES BLANCHES -----------------------------------------------------
	
	function loadBillingAdrr( id ){
		var divToComplete = document.getElementById('loadZone') ;

		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/loadBillingAdr.php?idBillingAdr=' + id ;
		divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /> Chargement en cours ...</div>';
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}

		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
				divToComplete.innerHTML = xhr_object.responseText ;
			else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);

}


function loadBillingDell( id ){
		var divToComplete = document.getElementById('loadZone1') ;

		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/loadBillingDel.php?idBillingDel=' + id ;
		divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /> Chargement en cours ...</div>';
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}

		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
				divToComplete.innerHTML = xhr_object.responseText ;
			else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);

}















	function loadBillingcontt(id, idb){
		var divToComplete = document.getElementById('loadcomments') ;
		if(id != -1){
		document.getElementById('loadZones').style.visibility="visible";
		document.getElementById('msge').style.visibility = "hidden";
		}
		
document.frm.idcontact.value=id ;
document.getElementById('table').style.visibility = "hidden";
//document.getElementById('idcontact').value=id ;
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/loadBillingcont.php?idBillingc=' + id + '&idBillingb=' + idb;
		//window.location.href="page2.htm?param1="+param1+"&param2="+param2; 
		divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /> Chargement en cours ...</div>';
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}

		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
			
				divToComplete.innerHTML = xhr_object.responseText ;
			else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);

}

//Rating stars function
function rating_function(idcontact_follow, rating)
			{
			
						//	if (confirm('Etes vous sur de vouloir réaliser le commentaire ?' )){
					
						var divToComplete = document.getElementById('realisercomments') ;
						//var data = ''; 
					//	var message = 'Supression du contact en cours ...';
						var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/rating_stars.php?idcontact_follow=' + idcontact_follow +' &idrating=' + rating;
						//machineAjax( location, divToComplete, data, message );
					
			
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
	
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
		{
				divToComplete.innerHTML = xhr_object.responseText ;
			 loadBillingcontt( document.getElementById('loadBillingcont').value, document.getElementById('idbuyer').value );
			 document.getElementById('loadBillingcont').value = document.getElementById('idbuyer').value;
			  document.getElementById('commercial').value =  document.getElementById('idcommercial').value;
			   document.getElementById('supplier').value =  document.getElementById('idsupplier').value;
			    document.getElementById('cause').value =  document.getElementById('idcontact_cause').value;
			}else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);
						
						
						
				//	}
			
			
			
			}
//--------------------------------Add commercial---------------------------------





//------------------------------------------------------------------------
function realisercomment(idf){
					
			//	if (confirm('Etes vous sur de vouloir réaliser le commentaire ?' )){
					
						var divToComplete = document.getElementById('realisercomments') ;
						//var data = ''; 
					//	var message = 'Supression du contact en cours ...';
						var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/realisercomment.php?idfollow=' + idf;
						//machineAjax( location, divToComplete, data, message );
					
			
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
	
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
		{
				divToComplete.innerHTML = xhr_object.responseText ;
			 loadBillingcontt( document.getElementById('loadBillingcont').value, document.getElementById('idbuyer').value );
			 document.getElementById('loadBillingcont').value = document.getElementById('idbuyer').value;
			  document.getElementById('commercial').value =  document.getElementById('idcommercial').value;
			   document.getElementById('supplier').value =  document.getElementById('idsupplier').value;
			    document.getElementById('cause').value =  document.getElementById('idcontact_cause').value;
			}else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);
						
						
						
				//	}
				}
	//-------------------------------------delete comment----------------------------------------------
			
			function deletecomment( idf ){
	
				if (confirm('Etes vous sur de vouloir supprimer le commentaire ?' )){
					
						var divToComplete = document.getElementById('deletecomments') ;
						//var data = ''; 
					//	var message = 'Supression du contact en cours ...';
						var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/deletecomment.php?idfollow=' + idf;
						//machineAjax( location, divToComplete, data, message );
					
					//	divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /> Chargement en cours ...</div>';
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
	
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
		{
				divToComplete.innerHTML = xhr_object.responseText ;
				alert ('Commentaire supprimer avec succés');
			 loadBillingcontt( document.getElementById('loadBillingcont').value, document.getElementById('idbuyer').value );
			  document.getElementById('loadBillingcont').value = document.getElementById('idbuyer').value;
			   document.getElementById('commercial').value =  document.getElementById('idcommercial').value;
			      document.getElementById('supplier').value =  document.getElementById('idsupplier').value;
			    document.getElementById('cause').value =  document.getElementById('idcontact_cause').value;
			}else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);
						
						
						
					}
				}
			
			
			//------------------------------------------------------------------------------------------------
			
			
				function resetsuivi(idc){
				
					document.frm.idfollow.value='' ;
				
				
				document.frm.rel_end.value= '';
//document.frm.date_new.value= '';
				document.frm.rel_send.value ='';

				document.frm.commentaire.value='' ;
					if(idc == -1){
					document.getElementById('loadZones').style.visibility = "hidden";
						document.getElementById('msge').style.visibility = "visible";
					}
					else {
					document.getElementById('msge').style.visibility = "hidden";
					}
					if(idc == -1){ 
					if(document.getElementById('loadZones').style.visibility = "hidden"){
		document.getElementById('msge').style.visibility = "visible";
		}
		
			}	else {
					document.getElementById('msge').style.visibility = "hidden";
					}
			
				}
					
function loadBillingconttcoment(idf){
		
document.getElementById('loadZones').style.visibility = "visible";
document.getElementById('realiser').type = "button";
//document.getElementById('idcontact').value=id ;
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/loadBillingcontcomment.php?idfollow=' + idf;
		//window.location.href="page2.htm?param1="+param1+"&param2="+param2; 
	
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}

		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			{
			document.getElementById('loadZones').style.visibility = "visible";
			
			document.getElementById('msge').style.visibility = "hidden";
		     	var chaine = xhr_object.responseText ;
                var tableau = chaine.split('#$#'); 
				//divToComplete.innerHTML = xhr_object.responseText ;
				//alert(tableau);
				document.frm.idfollow.value=tableau[0] ;
				document.frm.idcontact.value=tableau[1] ;
				
				document.frm.rel_end.value=tableau[2] ;
				document.frm.rel_send.value=tableau[3] ;
				document.frm.commentaire.value=tableau[4] ;
				
				
				// document.frm.date_new.value=tableau[5] ;
				 
				 
				 var idbuyer = tableau[6] ;
				
				    
					  
				  var idcontact = tableau[1] ;
				 
				 
				 
				 
				 if(tableau[7]== 0)
				 var idcommercial = -1 ;
				  else
				  var idcommercial = tableau[7] ;
				 
				 
				   
				    if(tableau[9]== 0)
				 var idsupplier = -1 ;
				  else
				  var idsupplier = tableau[9] ;
					
					
					 if(tableau[10]== 0)
				   var idcontact_cause = -1 ;				
				     else
				  var idcontact_cause = tableau[10] ;
                  var idtype_follow = tableau[12] ;
				  
				   document.frm.username.value=tableau[8] ;
				   document.frm.idcommercial.value=idcommercial ;
				     document.frm.idsupplier.value=idsupplier ;
					 document.frm.idcontact_cause.value=idcontact_cause ;
					 
					 document.frm.fin.value=tableau[11] ;
				 
	
				 loadBillingcontt( idcontact, idbuyer );
				
				 document.getElementById("commercial").value = idcommercial;
document.getElementById("supplier").value = idsupplier;
document.getElementById("cause").value = idcontact_cause;
	document.getElementById("loadBillingcont").value = idcontact;
        if(!idtype_follow)idtype_follow = 0;
    	document.getElementById("idtype_follow").value = parseInt(idtype_follow);
//document.getElementById("commercial" ).options[selectedIndex] = idcommercial;
     //  document.frm.commercial.option.value == idcommercial;


				//document.frm.commercial.options.value= idcommercial;
		//	document.getElementById('commercial').selectedIndex=idcommercial;
			
					
				//if(document.getElementById('loadBillingcont').value =)
			//	document.frm.commentaire.value=tableau[4] ;
				
				
				
				
				}
			else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);

}	



<?php
				
				if( isset( $key ) ){
					
?>
				
				// PopUp pour voir l'historique d'un client
				function seeBuyerHisto(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-750)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $key; ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
					
				}
				
				// PopUp pour l'ajout d'un contact
				function AddContact(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-1000)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $key; ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1000,height=600,resizable,scrollbars=yes,status=0');
					
				}
				
				// PopUp pour la modification d'un contact
				function UpdContact(id,idb){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-1000)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?c='+id+'&b='+idb, '_blank', 'top=' + postop + ',left=' + posleft + ',width=1000,height=600,resizable,scrollbars=yes,status=0');
					
				}
				
				// PopUp pour voir la gestion des contacts
				function seeContacts(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-800)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $key ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
					
					return false;
					
				}
				
				// PopUp pour voir les relances du client
				function seeRelaunch(){
					
					postop = (self.screen.height-600)/2;
					posleft = (self.screen.width-750)/2;
					window.open('<?php echo $GLOBAL_START_URL ?>/accounting/buyer_relaunch.php?b=<?php echo $key ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
					
					return false;
					
				}
				
				/* ----------------------------------------------------------------------------------- */
<?php
					
				}
				
?>
				
				function changeNAFText( element ){ document.getElementById( "Naf" ).value = element.info; }
				
				/* ----------------------------------------------------------------------------------- */
				
				function changeNAFNumber( element ){
					
					$.ajax({
						type: "GET",
						url: "<?php echo $GLOBAL_START_URL ?>/sales_force/naf_autocomplete.php?number=" + element.value,
						dataType: "text",
						success: function( responseText ){
							document.getElementById( "NafText" ).value = responseText;
						}
					});
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				// ---------------------------- Gestion contacts --------------------------------------
				
				function loadGestContact( idbuyer ){
	
					var divToComplete = document.getElementById('gestContactsContent') ;
				
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php' ;
					var data = 'buyer=' + idbuyer ;
					var message = 'Chargement en cours ...';
					machineAjax( location, divToComplete, data, message );
				}
				
				function loadContactToModify( idbuyer, idcontact, firstname, lastname ){
						
					var divToComplete = document.getElementById('gestContactsContent') ;
					
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php' ;
					var data = 'buyer=' + idbuyer + '&contact=' + idcontact ;
					var message = 'Chargement du contact ' + firstname + ' ' + lastname + ' ...';
					machineAjax( location, divToComplete, data, message );
					
				}
				
				function modifyContact( idcontact, idbuyer ){
					
					var divToComplete = document.getElementById('gestContactsContent') ;
					var lastname = document.getElementById('gestlastname').value ;
					var firstname = document.getElementById('gestfirstname').value ;
					var title = document.getElementById('gestTitle').value ;
					var contact_group = document.getElementById('gestcontact_group').value ;
					var idprincipal = document.getElementById('gestidprincipal').value ;
					var idobsolete = document.getElementById('gestidobsolete').value ;
					//var iddepartment = document.getElementById('iddepartment').options[document.getElementById('iddepartment').selectedIndex].value ;
					var idfunction = document.getElementById('gestIdFunction').options[document.getElementById('gestIdFunction').selectedIndex].value ;
					var faxnumber = document.getElementById('gestFaxnumber').value ;
					var phonenumber = document.getElementById('gestPhonenumber').value ;
					var mail = document.getElementById('gestMail').value ;
					var gsm = document.getElementById('gestGsm').value ;
					var comment = document.getElementById('gestComment').value ;
					//var contact_info1 = document.getElementById('').value ;
					
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php';
					var data = 'Modify=1&buyer=' + idbuyer + '&contact=' + idcontact + '&lastname=' + lastname + '&comment=' + comment + '&firstname=' + firstname + '&title=' + title + '&contact_group=' +contact_group + '&idprincipal=' + idprincipal + '&idobsolete=' + idobsolete + '&idfunction=' + idfunction + '&faxnumber=' + faxnumber + '&phonenumber='+ phonenumber +'&mail=' + mail + '&gsm=' + gsm ;

					if( document.getElementById( 'GeneratePassword' ) && document.getElementById( 'GeneratePassword' ).checked == true ){
						data = data + '&GeneratePassword=1' ; 
					}

					var message = 'Enregistrement du contact ' + lastname + ' ' + firstname + ' ...';
					machineAjax( location, divToComplete, data, message );
					
				}
				
				function showFormAddContact( idbuyer ){
	
					var divToComplete = document.getElementById('gestContactsContent') ;
					
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php';
					data = 'add=1&buyer=' + idbuyer ;
					var message = 'Chargement en cours ...';
					machineAjax( location, divToComplete, data, message );
					
				}

				function createContact(){

				if( !$( "#gestFaxnumber" ).val().maxlength ){
						
						addUser();
						return;

					}
			/*	$.ajax({
					 	
						url: "/templates/sales_force/contact_buyer_js.php?check=email",
						async: true,
						type: "GET",
						data: "email=" + $( "#gestMail" ).val(),
						error :  function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de vérifier le format de l'adresse email" ); },
					 	success: function( responseText ){

							if( responseText == "1" )
								addUser();
							else alert( responseText );
							
						}

					});*/

					
				}
				
				function addUser(){

					var divToComplete = document.getElementById('gestContactsContent') ;
					
					var lastname = document.getElementById('gestlastname').value ;
					var firstname = document.getElementById('gestfirstname').value ;
					var title = document.getElementById('gestTitle').value ;
					//var iddepartment = document.getElementById('iddepartment').options[document.getElementById('iddepartment').selectedIndex].value ;
					var idfunction = document.getElementById('gestIdFunction').options[document.getElementById('gestIdFunction').selectedIndex].value ;
					var faxnumber = document.getElementById('gestFaxnumber').value ;
					var phonenumber = document.getElementById('gestPhonenumber').value ;
					var mail = document.getElementById('gestMail').value ;
					var gsm = document.getElementById('gestGsm').value ;
					var comment = document.getElementById('gestComment').innerHTML ;
					
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php';
					var data = 'addUser=1&buyer=<?php echo  $key ; ?>&comment=' + comment +  '&lastname=' + lastname + '&firstname=' + firstname + '&title=' + title + '&idfunction=' + idfunction + '&faxnumber=' + faxnumber + '&phonenumber='+ phonenumber +'&mail=' + mail + '&gsm=' + gsm;
					var message = 'Enregistrement du contact ' + lastname + ' ' + firstname + ' en cours ...';
					machineAjax( location, divToComplete, data, message );
					
				}
				
				function deleteUser( idcontact, idbuyer, lastname, firstname ){
	
					if (confirm('Etes vous sur de vouloir supprimer le contact ' + lastname + ' ' + firstname + ' ?' )){
					
						var divToComplete = document.getElementById('gestContacts') ;
						var data = 'Delete=1&contact=' + idcontact + '&buyer=' + idbuyer + '&lastname=' + lastname + '&firstname=' + firstname ; 
						var message = 'Supression du contact en cours ...';
						var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php';
						machineAjax( location, divToComplete, data, message );
						
					}
				}
				
				function updateContactOrder( idbuyer ){
					
					if( document.getElementById('changeOrderContact').value == ''){
						alert('Vous devez selectionner un contact !');
						return ;
					}

					var idcontact = document.getElementById('changeOrderContact').value ;
					var divToComplete = document.getElementById('gestContactsContent') ;
					var data = 'changeOrder=1&idcontact=' + idcontact + '&idbuyer=' + idbuyer ; 
					var message = 'Modification de l\'ordre des contacts en cours ...';
					var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_manage.php';
					
					machineAjax( location, divToComplete, data, message );

				}
				
				
				function autoCompleteMairie(){

					
					var mairePrincipalContact = document.getElementById('mairePrincipale');
					
					var nomMaire 			= document.getElementById('nomMaire').innerHTML;
					var prenomMaire 		= document.getElementById('prenomMaire').innerHTML;
					var streetCommune 		= document.getElementById('streetCommune').innerHTML;
					var cpCommune 			= document.getElementById('cpCommune').innerHTML;
					var cityCommmune 		= document.getElementById('cityCommmune').innerHTML;
					var telCommune 			= document.getElementById('telCommune').innerHTML;
					var faxCommune 			= document.getElementById('faxCommune').innerHTML;
					var regionCommune 		= document.getElementById('regionCommune').innerHTML;
					var deptCommune 		= document.getElementById('deptCommune').innerHTML;
					var inseeCommune 		= document.getElementById('inseeCommune').innerHTML;
					var sirenCommune 		= document.getElementById('sirenCommune').innerHTML;
					var webCommune 			= document.getElementById('webCommune').innerHTML;
					var habitantsCommune 	= document.getElementById('habitantsCommune').innerHTML;

					document.getElementById('nom_Maire').value = nomMaire ;
					document.getElementById('prenom_Maire').value = prenomMaire ;
					document.getElementById('Adress').value = streetCommune ;
					document.getElementById('Zipcode').value = cpCommune;
					document.getElementById('City').value = cityCommmune;
					document.getElementById('phone_standard').value = telCommune ;
					document.getElementById('fax_standard').value = faxCommune ;
					document.getElementById('mairie_region').value = regionCommune;
					document.getElementById('mairie_dpt').value = deptCommune ;
					document.getElementById('insee_code').value = inseeCommune;
					document.getElementById('siren').value = sirenCommune;
					document.getElementById('www').value = webCommune ;
					document.getElementById('nbr_habitant').value = habitantsCommune;

					document.getElementById('Company').value = cityCommmune;
					
					if(mairePrincipalContact.checked == true){
						document.getElementById('buyerLastname').value = nomMaire ; 
						document.getElementById('buyerFirstname').value = prenomMaire ; 
					}
					
				}
				
				function machineAjax( location, divToComplete, data, message ){
	
					divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /><br />' + message + '</div>';
					
					var xhr_object = null;
					if(window.XMLHttpRequest){ // Firefox
					   xhr_object = new XMLHttpRequest();
						if (xhr_object.overrideMimeType) {
							xhr_object.overrideMimeType('application/x-www-form-urlencoded; charset=utf-8');
						}
					}else if(window.ActiveXObject) // Internet Explorer
					   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
					else { // XMLHttpRequest non supporté par le navigateur
					   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
					   return;
					}
					xhr_object.onreadystatechange = function()
					{
					if(xhr_object.readyState == 4)
					{
						 // ON CONTROLE LE STATUS (ERREUR 404, ETC)
						if(xhr_object.status == 200)
							divToComplete.innerHTML = '<div style="padding:5px;">' + xhr_object.responseText + '</div>' ;
						else
							divToComplete.innerHTML = '<div style="text-align:center;padding:5px;">Erreur : ' + xhr_object.status +' </div>' ;
						}
					}; 
					
					xhr_object.open("POST", location, true);
					// ne pas oublier ça pour le post
					xhr_object.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
					xhr_object.send( data );
				}
				
			
				//onglets
				
				$(function() {
				
					$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
				
				});
				
				
/* ]]> */
</script>


<style type="text/css">
/* <![CDATA[ */
	
	.ui-tabs .ui-tabs-nav{
		width:auto;
		float:right;
		margin-top:-32px;
		*margin-top:-21px;
	}
	
	.ui-tabs .ui-tabs-nav li{
		width: auto;
	}
	
/* ]]> */
</style>
