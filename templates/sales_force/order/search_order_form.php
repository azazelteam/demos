<?
/**
 * Package commercial
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Moteur de recherche commande client B2B
 */
?>

<div class="contentDyn">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order/search_order_form.php</span>
<?php } ?>


	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Rechercher une commande</span>
	<div class="spacer"></div>
	</h1>
	
	
	<div class="blocSearch"><div style="margin:5px;">

		<!-- Choix par date -->
		<div class="blocMultiple blocMLeft">
			<h2>&#0155; Par date :</h2>
			
			<label><span><input type="radio" name="interval_select" id="interval_select_relance" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1 || !isset( $_POST[ "interval_select" ] ) ) echo " checked=\"checked\""; ?> /> 
			&nbsp;Depuis</span></label>
			<select name="minus_date_select" onchange="selectRelance();" class="fullWidth">
				<option value="0" <?php if( !isset( $_POST[ "minus_date_select" ] ) || ( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == 0 ) ) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_today") ?></option>
				<option value="-1" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1 ) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_yesterday") ?></option>
				<option value="-2" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -2) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2days") ?></option>
				<option value="-7" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -7) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1week") ?></option>
				<option value="-14" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -14) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2weeks") ?></option>
				<option value="-30" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -30) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1month") ?></option>
				<option value="-60" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -60) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2months") ?></option>
				<option value="-90" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -90) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_3months") ?></option>
				<option value="-180" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -180) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_6months") ?></option>
				<option value="-365" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -365 ) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1year") ?></option>
				<option value="-730" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -730) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2year") ?></option>
				<option value="-1825" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1825) echo "selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_5year") ?></option>
			</select>
			<div class="spacer"></div>
			
			<label><span><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> 
			&nbsp;Pour le mois de</span></label>
			<?php FStatisticsGenerateDateHTMLSelect( "selectMonth()" ) ?>
			<div class="spacer"></div>
			
			<label><span><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> 
			&nbsp;Entre le</span></label>
			<?php $date = isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : ""; ?>
			<input type="text" name="bt_start" id="bt_start" class="calendarInput" value="<?php echo $date ?>" />
			<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
			
			<label class="smallLabel"><span>et le</span></label>
			<?php $date = isset( $_POST[ "bt_end" ] ) ? $_POST[ "bt_end" ] : ""; ?>
			<input type="text" name="bt_end" id="bt_end" class="calendarInput" value="<?php echo $date ?>" />
			<?php echo DHTMLCalendar::calendar( "bt_end", true, "selectBetween" ) ?>
			
		</div>
        
        <!-- Choix par informations sur le devis -->
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Par informations sur la commande :</h2>
        	
			<label><span>Commande n°</span></label>
			<input type="text" name="idorder" class="textInput" value="<?php echo isset( $_POST[ "idorder" ] ) ? stripslashes( $_POST[ "idorder" ] ) : "" ?>" />
			<div class="spacer"></div>
			
			<label><span>Avancement</span></label>
			<select name="status" id="status" class="fullWidth">
				<option value=""<?php if( $status == "none" ){ echo " selected=\"selected\""; } ?>>Tous</option>
				<option value="ToPay"<?php if( $status == "ToPay" ){ echo " selected=\"selected\""; } ?>>A Payer</option>
				<option value="ToDo"<?php if( $status == "ToDo" ){ echo " selected=\"selected\""; } ?>>A traîter</option>
				<option value="Cancelled"<?php if( $status == "Cancelled" ){ echo " selected=\"selected\""; } ?>>Annulée</option>
				<option value="Ordered"<?php if( $status == "Ordered" ){ echo " selected=\"selected\""; } ?>>Confirmé</option>
				<option value="DemandInProgress"<?php if( $status == "DemandInProgress" ){ echo " selected=\"selected\""; } ?>>Demande de confirmation en cours</option>
				<option value="InProgress"<?php if( $status == "InProgress" ){ echo " selected=\"selected\""; } ?>>En cours</option>
				<option value="Send"<?php if( $status == "Send" ){ echo " selected=\"selected\""; } ?>>Envoyé</option>
				<option value="Refused"<?php if( $status == "Refused" ){ echo " selected=\"selected\""; } ?>>Refusé</option>
			</select>
						
            <div class="spacer"></div>
			
			<label><span>Devis n°</span></label>	
			<input type="text" name="idestimate" class="numberInput" value="<?php echo isset( $_POST[ "idestimate" ] ) ? stripslashes( $_POST[ "idestimate" ] ) : "" ?>" />
			
			
        	<label class="smallLabelMin"><span>Facture n°</span></label>
			<input type="text" name="idbilling_buyer" class="numberInput" value="<?php echo isset( $_POST[ "idbilling_buyer" ] ) ? stripslashes( $_POST[ "idbilling_buyer" ] ) : "" ?>" />
			<div class="spacer"></div>
			
			<label><span>Total devis HT</span></label>
			<input type="text" name="estimate_total_amount_ht" class="numberInput" value="<?php echo isset( $_POST[ "estimate_total_amount_ht" ] ) ? stripslashes( $_POST[ "estimate_total_amount_ht" ] ) : "" ?>" />
			
        	<label class="smallLabelMin"><span>Total commande HT</span></label>
			<input type="text" name="order_total_amount_ht" class="numberInput" value="<?php echo isset( $_POST[ "order_total_amount_ht" ] ) ? stripslashes( $_POST[ "order_total_amount_ht" ] ) : "" ?>" />
			<div class="spacer"></div>
			
			<label><span>Total devis TTC</span></label>
			<input type="text" name="estimate_total_amount" class="numberInput" value="<?php echo isset( $_POST[ "estimate_total_amount" ] ) ? stripslashes( $_POST[ "estimate_total_amount" ] ) : "" ?>" />

			<label class="smallLabelMin"><span>Total commande TTC</span></label>
			<input type="text" name="order_total_amount" class="numberInput" value="<?php echo isset( $_POST[ "order_total_amount" ] ) ? stripslashes( $_POST[ "order_total_amount" ] ) : "" ?>" />
			<div class="spacer"></div>
		</div>	
        <div class="spacer"></div>
        
        <!-- Choix par client -->
        <div class="blocMultiple blocMLeft">
        	<h2>&#0155; Par client :</h2>
        	
        	<label><span><strong>Raison sociale</strong></span></label>
        	<input type="text" name="company" id="company" class="textInput" value="<?php echo isset( $_POST[ "company" ] ) ? stripslashes( $_POST[ "company" ] ) : "" ?>" />
        	<?php
                                        	
				include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "company", "buyer", "company", "Client n°{idbuyer}", 15 );

            ?>
        	<div class="spacer"></div>
        	
        	<label><span><strong>Nom du client</strong></span></label>
        	<input type="text" name="lastname" id="lastname" class="textInput" value="<?php echo isset( $_POST[ "lastname" ] ) ? stripslashes( $_POST[ "lastname" ] ) : "" ?>" />
        	<?php
                                        	
				include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "lastname", "contact", "lastname", "Client n°{idbuyer}", 15 );

            ?>
        	<div class="spacer"></div>
        	
        	<label><span><strong>N° du client</strong></span></label>
            <input type="text" name="idbuyer" id="idbuyer" class="textInput" value="<?php echo isset( $_POST[ "idbuyer" ] ) ? stripslashes( $_POST[ "idbuyer" ] ) : "" ?>" />
           	<?php
                                        	
				include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "idbuyer", "buyer", "idbuyer", "{company}", 15 );

            ?>
            <div class="spacer"></div>

        	<!-- <label><span>Fax</span></label>
        	<input type="text" name="faxnumber" class="textInput" value="<?php echo isset( $_POST[ "faxnumber" ] ) ? stripslashes( $_POST[ "faxnumber" ] ) : "" ?>" />
        	<div class="spacer"></div> -->
        	
        	<label><span>Code postal</span></label>
        	<input type="text" name="zipcode" class="numberInput" value="<?php echo isset( $_POST[ "zipcode" ] ) ? stripslashes( $_POST[ "zipcode" ] ) : "" ?>" />
        	
        	<label class="smallLabel"><span>Ville</span></label>
        	<input type="text" name="city" class="mediumInput" value="<?php echo isset( $_POST[ "city" ] ) ? stripslashes( $_POST[ "city" ] ) : "" ?>" />
        	<div class="spacer"></div>
        </div>
        
        <!-- Choix par provenance -->
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Autres critères :</h2>
			<label><span>Commercial</span></label>	
			<?php						
				if( $hasAdminPrivileges ){
					
					include_once( dirname( __FILE__ ) . "/../../../script/user_list.php" );
					userList( true, $iduser );
					
					//XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "firstname", $iduser, $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
					
				}
				else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";					
			?> 
			<div class="spacer"></div>
			
			<?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping"); ?> 
			<?php if($akilae_grouping==1){ ?>
			<label><span>Groupement</span></label>	
			<?php XHTMLFactory::createSelectElement( "grouping", "idgrouping", "grouping", "grouping", isset( $_POST[ "idgrouping" ] ) ? $_POST[ "idgrouping" ] : false, "", "-", "name=\"idgrouping\"" ); ?>
			<?php } ?>
			
			<?php $akilae_source = DBUtil::getParameterAdmin("akilae_source");
			if($akilae_source==1){	?>
			<label><span>Provenance</span></label>	
			<?php XHTMLFactory::createSelectElement( "source", "idsource", "source_1", "source_1", isset( $_POST[ "idsource" ] ) ? $_POST[ "idsource" ] : false, "", "-", "name=\"idsource\"" ); ?>
			<?php } ?>
			
			<!--<label><span>Livraison</span></label>	
			<input title="Recherche dans l'adresse, le code postal, la ville et l'entreprise de livraison" type="text" name="forwarding" class="textInput" value="<?php if( isset( $_POST[ "forwarding" ] ) ) echo stripslashes( $_POST[ "forwarding" ] ); ?>" />
			<div class="spacer"></div>-->
		
        </div>	
        <div class="spacer"></div>		

        <input type="submit" class="inputSearch" value="Rechercher" />    
        <div class="spacer"></div>  
        
        
	</div></div>
	<div style="clear:both;"></div>
	
</div>       		      	
<div class="spacer"></div>    