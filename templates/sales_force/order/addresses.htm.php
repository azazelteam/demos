
<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order/addresses.htm.php</span>
<?php } ?>

<script type="text/javascript">
/* <![CDATA[ */

	//------------------------------------------------------------------------------------------------
	
	/*affectation auto des adresses à la commande après modification des données*/
		
	function selectOrderAddress( addressType, selectedAddress ){
	
		var primaryKey 	= getPrimaryKeyName( addressType );
		var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
		var checked 	= document.getElementById( addressType + '_checkbox' ).checked;
		
		var selectedAddress = 0;
		
		if( checked )
			selectedAddress = addresses.options[ addresses.selectedIndex ].value;
		
		$.ajax({
	 	
			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?select&type=" + addressType + "&id=" + selectedAddress + "&tablename=order&primarykey=<?php echo $Order->getId() ?>",
			async: false,
		 	success: function( response ){
		 		
				if( response == '0' )
					xhrAlert( 'Impossible de mettre à jour la commande' );
				
			}
	
		});
		
	}
	
	//------------------------------------------------------------------------------------------------
	
/*	]]> */
</script>
<?php 

/**
 * Affichage des adresses de livraison/facturation si différentes
 */	

$forwardingAddress 	= $Order->getForwardingAddress() instanceof ForwardingAddress ? $Order->getForwardingAddress() : false;
$invoiceAddress 	= $Order->getInvoiceAddress() instanceof InvoiceAddress ? $Order->getInvoiceAddress() : false;

?>
<input type="hidden" name="updateAddresses" value="1" />

<?php anchor( "UpdateAdresses" ); ?>
<?php if( $msgdeliv != "" || $msgbill != "" ){ ?>
	
	<p class="alert" style="text-align:center;">
	<?php echo "<br />$msgdeliv<br />".$msgbill; ?>
	</p>
	
<?php } 
include_once( "$GLOBAL_START_PATH/objects/AddressEditor.php" );

//@todo en attendant une gestion cohérente des adresses, on édite pas l'adresse du client par défaut

new AddressEditor( $Order->get( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_FORWARDING, $forwardingAddress, false, "float:left; width:50%; margin-top:15px;", $callback = "selectOrderAddress" );
new AddressEditor( $Order->get( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_INVOICE, $invoiceAddress, false, "float:right; width:50%; margin-top:15px;", $callback = "selectOrderAddress" );

?>
<div class="spacer"></div>
