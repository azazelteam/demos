<script type="text/javascript" language="javascript">
<!--

	function showSupplierInfos( idsupplier){

		if( idsupplier == '0' )
			return;
			
		var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=650, height=650';
		var location = '/product_management/enterprise/supplier.php?idsupplier=' + idsupplier + '&ispopup=true';
		
		window.open( location, '_blank', options ) ;
		
	}
	
// -->
</script>
<?php

anchor( "UpdateRefDiscountRate_", true );
anchor( "UpdateOrderPriceWD_", true );
anchor( "UpdateUnitCostRate_", true );
anchor( "UpdateUnitCostAmount_", true );
anchor( "UpdateDesignation_", true );
anchor( "UpdateSupplierCharges_", true );
anchor( "UpdateInternalSupplierCharges_", true ); 
anchor( "UpdRow_", true ); 

?>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<a name="references"></a>

<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" />

<div class="mainContent fullWidthContent">
	<div class="topRight"></div><div class="topLeft"></div>
	<div class="content">
		<div class="headTitle">
			<p class="title">Article(s) de la commande</p>
			<div class="rightContainer"></div>
		</div>
		<div class="subContent">
			<div class="tableHeaderLeft">
				<a href="#bottomPage" class="goUpOrDown">
					Aller en bas de page
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
				</a>
			</div>
			<div class="tableHeaderRight">
				<!--
				<a href="#"><img src="img/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
				<a href="#"><img src="img/content/xls_icon.png" alt="Exporter les donnéés au format XLS" /></a>
				-->
			</div>
			<!-- tableau résultats recherche -->
			<div class="resultTableContainer clear">
				<table class="dataTable resultTable">
				<thead>	
    <tr>
		<th class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("photo") ; ?></th>
		<th class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("ref") ; ?> & <?php echo Dictionnary::translate("designation") ; ?></th>
		<th class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("quantity") ; ?></th>
		<!--
		<th class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("gest_com_discount_buy") ; ?></th>
		<th class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate( "gest_com_rough_sale_coeff" ) ?></th>
		-->
		<th class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("gest_com_discount_sell") ; ?></th>
		<?php
			if( $hasLot ){
				?>
				<th class="lefterCol righterCol" style="font-weight:bold;"><?php  echo Dictionnary::translate("qty/lot") ; ?></th>
				<?php
			}
		?>
    	<th style="border:none; background-color:#ffffff;">&nbsp</th>
    	<th style="border:none; background-color:#ffffff;">&nbsp</th>
    </tr>
    </thead>
    <tbody>
	<?php
	
	$down_payment_msg = 0;
	$phatot=0;

	$n = $Order->getItemCount();
	$rows = getIdRowsSortedBySupplier();
	$currentSupplier = 0;
	$warning=0;
	
	//Gestion de la remise supplémentaire
	$view_ref_discount_add = DBUtil::getParameterAdmin( "view_ref_discount_add");
	
	for ( $k = 0; $k < count( $rows ); $k++ ) {
		$msgsto = '';
		
		$i = $rows[ $k ]; //rows triées par fournisseur
		
		$idarticle = $Order->getItemAt($i)->get('idarticle');
		
		$options = GetOptions($idarticle);
		$similar = GetSimilar($idarticle);
		
		$href_pdf = '';
		$reference = $Order->getItemAt($i)->get('reference');
		$phatot=$phatot+$Order->getItemAt($i)->get('unit_cost');
		$buy = $Order->getCustomer()->get('idbuyer_family');
		$VolPrice = IsVolumeDiscounted($reference);
		$useref=1;
		
		$madb=DBUtil::getConnection();
		
		//On récupère l'id product et le name pour faire le lien vers la fiche produit
		
		$lang = "_1";
		
		$qlink = "SELECT p.idproduct, p.name$lang FROM product p, detail d WHERE d.idarticle='$idarticle' AND d.idproduct = p.idproduct LIMIT 1";
		$rlink =& DBUtil::query( $qlink );
			
		$idp 	= $rlink->RecordCount() ? $rlink->fields( "idproduct" ) : "";
		$name 	= $rlink->RecordCount() ? $rlink->fields( "name$lang" ) : "";
		$link 	= $rlink->RecordCount() ? URLFactory::getProductURL( $idp, $name ) : "";

		//lien fiche PDF
		
		$query = "SELECT type, doc FROM detail WHERE reference LIKE '" . Util::html_escape( $reference ) . "' LIMIT 1";
		$rs =& DBUtil::query( $query );
		
		$doc 	= $rs->RecordCount() ? $rs->fields( "doc" ) : "";
		$type 	= $rs->RecordCount() ? $rs->fields( "type" ) : "";
		
		if( $type == "catalog" && empty( $doc ) )
			$href_pdf = "/catalog/pdf_product.php?idproduct=" . DBUtil::getDBValue( "idproduct", "detail", "idarticle", $Order->getItemAt( $i)->get("idarticle" ) );
		else if( !empty( $doc ) )
			$href_pdf = "$GLOBAL_START_URL/$doc";
		else $href_pdf = "";

		//désignation
		
		$designation_edit= stripslashes( $Order->getItemAt($i)->get('designation') );
		
		$designation_edit = str_replace("\n",'', $designation_edit);
		$designation_edit = str_replace("\r",'', $designation_edit);
		$designation_edit = str_replace('<br />',"\n", $designation_edit);
		$designation_edit = str_replace('<br />',"\n", $designation_edit);
		$designation_edit = strip_tags($designation_edit);			
		
		//marges
		
		//Marge initiale
	
		if(($Order->getItemAt($i)->get('unit_cost_amount')>0)AND($Order->getItemAt( $i )->get( "discount_price" )>0)){
			
			//$mbi = ( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $quantity + $supplier_charges ) / ( $Order->getItemAt( $i)->get( 'unit_price' ) * $quantity ) ) ) * 100;
			$mbi = ( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) ) / ( $Order->getItemAt( $i)->get( 'unit_price' ) ) ) ) * 100;
			$mbi = Util::numberFormat( $mbi );
			
		}else{
			
			$mbi='0';
			
		}
		
		//Marge finale
		
		if(($Order->getItemAt($i)->get('unit_cost_amount')>0)AND($Order->getItemAt( $i )->get( "discount_price" )>0)){
			
			//$mbf = ( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $quantity + $supplier_charges ) / ( $Order->getItemAt( $i )->get( "discount_price" ) * $quantity ) ) ) * 100;
			$mbf = ( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) ) / ( $Order->getItemAt( $i )->get( "discount_price" ) ) ) ) * 100;
			$mbf = Util::numberFormat( $mbf );
			
		}else{
			
			$mbf='0';
		
		}
		
		//Marge numérique
		
		if(($Order->getItemAt($i)->get('unit_cost_amount')>0)AND($Order->getItemAt( $i )->get( "discount_price" )>0)){
			
			//$mbn = ( $Order->getItemAt( $i )->get( "discount_price" ) * $Order->getItemAt( $i)->get('quantity' ) ) - ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $Order->getItemAt( $i)->get( 'quantity' ) + $supplier_charges );
			$mbn = ( $Order->getItemAt( $i )->get( "discount_price" ) * $Order->getItemAt( $i)->get('quantity' ) ) - ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $Order->getItemAt( $i)->get( 'quantity' ) );
			$mbn = Util::numberFormat( $mbn );
			
		}else{
			
			$mbn='0';
		
		}
		
	?>
	<tr>
		<td rowspan="5" class="lefterCol righterCol">
			<input type="hidden" id="reference_<?php echo $i ?>" value="<?php echo $Order->getItemAt( $i)->get( "reference" ) ?>" />
			<a href="<?=$link;?>" onclick="window.open(this.href); return false;"><?php echo $icone ?></a>
		</td>
		<td rowspan="5" class="lefterCol righterCol"  style="font-weight:bold;">
			<p>
				<?php echo Dictionnary::translate("catalog_reference") ; ?> : <a href="<?=$link;?>" onclick="window.open(this.href); return false;"><?php echo $reference ?></a>
			</p>
			<div style="margin-top:15px;">
				<?=$designation_edit?>
			</div>
		</td>
		<td class="lefterCol righterCol"><?php echo $Order->getItemAt($i)->get('quantity') ?></td>
		<!--
		<td class="lefterCol righterCol"><?php echo Util::numberFormat( $Order->getItemAt( $i)->get('unit_cost_rate' ) ) ?></td>
		-->
		<?php 
		
		$rough_sale_coeff = $Order->getItemAt( $i)->get( "unit_cost" ) > 0.0 ? $Order->getItemAt( $i)->get("unit_price" ) / $Order->getItemAt( $i)->get( "unit_cost" ) : 0.0;
			
		?>
		<!--<td<?php if( $rough_sale_coeff <= 0.0 ) echo " style=\"color:#FF0000; font-weight:bold;\""; ?> class="lefterCol righterCol">
			A<?php echo Util::numberFormat( round( $rough_sale_coeff, 2 ) ) ?>
		</td>-->
		<td class="lefterCol righterCol">
		<?php
		
			if($view_ref_discount_add==0)
				echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount' ) ) . "&nbsp;%";
			else{
				
				?>
				<b><?=Dictionnary::translate("gest_com_ref_base");?></b>
				<br />
				<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount_base' ) ) ?>&nbsp;%
				<br />
				<br />
				<b><?=Dictionnary::translate("gest_com_ref_add");?></b>
				<br />
				<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount_add' ) ) ?>&nbsp;%
				<?php 
				
			}
			
		?>
		</td>
		<?php
		
			if( $hasLot ){
				?>
				<td rowspan="5" class="lefterCol righterCol"><b><?php echo $Order->getItemAt( $i)->get("lot" ) . " / " . $Order->getItemAt( $i)->get( "unit" ); ?></b> </td>
				<?php
			}
			
			$total_price_wd = $Order->getItemAt( $i )->get( "discount_price" ) * $Order->getItemAt($i)->get('quantity');
			
			$sale_coeff = $Order->getItemAt( $i)->get( "unit_cost_amount" ) > 0.0 ? $Order->getItemAt( $i )->get( "discount_price" ) / $Order->getItemAt( $i)->get( "unit_cost_amount" ) : 0.0;
			
			?>
			<!--
			<td<?php if( $sale_coeff <= 0.0 ) echo " style=\"color:#FF0000; font-weight:bold;\""; ?> class="lefterCol righterCol">
			-->
			<?php
			
			echo Util::numberFormat( round( $sale_coeff, 2 ) ) 
		
		?>
		</td>
		<td rowspan="5" style="border-right:none;border-top:none;border-bottom:none;">&nbsp;&nbsp;&nbsp;</td>
		<td rowspan="5" style="border-right:none;border-top:none;border-bottom:none;">
			<table cellpadding=0 cellspacing=0 border=0>
				<tr>
					<td colspan="2" style="background-color:#eb6a0a; color:#ffffff;border: 1px solid #eb6a0a;"> Marge Brute <br /> Article / PV</td>
				</tr>
				<tr>
					<td colspan="2" style="border: 1px solid #eb6a0a;"><?=$mbn?>&nbsp;&euro;</td>
				</tr>
				<tr>
					<td style="background-color:#eb6a0a; color:#ffffff;border: 1px solid #eb6a0a;">MB %</td>
					<td style="background-color:#eb6a0a; color:#ffffff;border: 1px solid #eb6a0a;">Coeff.</td>
				</tr>
				<tr>
					<td style="border: 1px solid #eb6a0a;"><?=$mbf?>&nbsp;%</td>
					
					<td style="border: 1px solid #eb6a0a;"><?php echo Util::numberFormat( round( $rough_sale_coeff, 2 ) ) ?></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("buyingcost") ; ?></td>
		<td class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate( "sellingcost" ) ; ?></td>
	</tr>
	<tr>
		<td class="lefterCol righterCol">
			<b><?php echo Util::priceFormat($Order->getItemAt($i)->get('unit_cost')) ?></b>
		</td>
		<td class="lefterCol righterCol">
			<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get('unit_price' )) ?>&nbsp;&euro;
		</td>
	</tr>
	<tr>
		<td  class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("gest_com_total_buy_ht") ; ?></td>
		<td  class="lefterCol righterCol" style="font-weight:bold;"><?php echo Dictionnary::translate("price_t_ht") ; ?></td>
	</tr>
	<tr>
		<td  class="lefterCol righterCol"><b><?=Util::priceFormat($Order->getItemAt( $i)->get( "unit_cost_amount" )*$Order->getItemAt( $i)->get( "quantity" ))?></b></td>
		<td  class="lefterCol righterCol">
			<b><?php echo Util::priceFormat($total_price_wd) ?></b>
		</td>
	</tr>
	
<?php } ?>
</tbody>
				</table>
			</div>
			<a href="#topPage" class="goUpOrDown">
				Aller en haut de page
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
			</a>
		</div>
	</div>
	<div class="bottomRight"></div><div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->

<?php

//-----------------------------------------------------------------------------------------------------

/**
 * Trie les lignes de commande par idsupplier et idrow
 */
function getIdRowsSortedBySupplier(){
	
	global $Order;
	
	//récupérer les identifiant fournisseur
	
	$rows = array();
	$idsuppliers = array();
	
	$i = 0;
	while( $i < $Order->getItemCount() ){
		
		$rows[] = $i;
		$idsuppliers[] = $Order->getItemAt( $i)->get( "idsupplier" );
		
		$i++;
		
	}

	//tri par fournisseur

    $sorted = false;
 	while( !$sorted ){
 		
 		$sorted = true;
 		$i = 0;
 		while( $i < count( $idsuppliers ) - 1){
 			
 			if( $idsuppliers[ $i ] > $idsuppliers[ $i + 1 ] 
 				|| ( $idsuppliers[ $i ] == $idsuppliers[ $i + 1 ] && $rows[ $i ] > $rows[ $i + 1 ] ) ){

 				$tmp = $idsuppliers[ $i ];
 				$idsuppliers[ $i ] = $idsuppliers[ $i + 1 ];
 				$idsuppliers[ $i + 1 ] = $tmp;
 				
 				$tmp = $rows[ $i ];
 				$rows[ $i ] = $rows[ $i + 1 ];
 				$rows[ $i + 1 ] = $tmp;
 				
 				$sorted = false;
 				
 			}
    			
 			$i++;
 			
 		}
 		
 	}
 	
	return $rows;
	
}

//-----------------------------------------------------------------------------------------------------

function getSupplierPopup( $idsupplier, $index ){

	global 
			$GLOBAL_START_URL;
	
	$db =& DBUtil::getConnection();
	$lang = "_1";
	
	$query = "
	SELECT s.* 
	FROM supplier s, supplier_contact sc
	WHERE s.idsupplier = '$idsupplier' 
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les informations du fournisseur '$idsupplier'" );
		
	$supplierInfos = $rs->fields;
	
	?>
	<div id="SupplierInfosDiv_<?php echo $index ?>" style="cursor:help; display:none; width:650px; position:absolute; left:-10px; top:-10px; z-index:99; padding:15px; background-color:#FFFFFF; border:1px solid #B7B7B7;">
	<table cellspacing="0" cellpadding="2" align="center" style="width:650px;">
		<tr>
			<th><?php echo Dictionnary::translate("company_social") ?></th>
			<td width="150"><?php echo $supplierInfos[ "name" ] ?></td>
			<th><?php echo Dictionnary::translate("name") ?></th>
			<td width="150"><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $supplierInfos[ "idtitle" ]  ).' '. $supplierInfos[ "firstname" ] .' '. $supplierInfos[ "lastname" ] ?></td>		
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("adress") ?></th>
			<td width="150"><?php echo $supplierInfos[ "adress" ] ?></td>
			<th><?php  echo Dictionnary::translate("adress_2") ?></th>
			<td width="150"><?php echo $supplierInfos[ "adress_2" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("zipcode") ?></th>
			<td width="150"><?php echo $supplierInfos[ "zipcode" ] ?></td>
			<th><?php  echo Dictionnary::translate("city") ?></th>
			<td width="150"><?php echo $supplierInfos[ "city" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("state") ?></th>
			<td width="150">
				<?php echo getCountryName( $supplierInfos[ "idstate" ] ); ?>
			</td>
			<th><?php  echo Dictionnary::translate("email") ?></th>
			<td width="150"><a href="mailto:<?php echo $supplierInfos[ "email" ] ?>" class="mail"><?php echo $supplierInfos[ "email" ] ?></a></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("phone") ?></th>
			<td width="150"><?php echo $supplierInfos[ "phonenumber" ] ?></td>
			<th><?php  echo Dictionnary::translate("fax") ?></th>
			<td width="150"><?php echo $supplierInfos[ "faxnumber" ] ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("payment") ?></th>
			<td width="150"><?php echo getPaymentTxt($supplierInfos[ "idpaiement" ]) ?></td>
			<th><?php  echo Dictionnary::translate("payment_delay") ?></th>
			<td width="150"><?php echo getPaymentDelayTxt($supplierInfos[ "pay_delay" ]) ?></td>
		</tr>
		<tr>
			<th><?php  echo Dictionnary::translate("select_charge") ?></th>
			<td width="150"><?php echo getSelectCharge($supplierInfos[ "select_charge" ]) ?></td>
			<th><?php  echo Dictionnary::translate("charge_rate") ?></th>
			<td width="150"><?php echo $supplierInfos[ "charge_rate" ] ?></td>
		</tr>
	</table>
	<?php
	
	if( !empty( $supplierInfos[ "comment" ] ) ){
		
		?>
		<table class="Supplier_Table" cellspacing="0" cellpadding="2" align="center" style="width:650px; margin-top:25px;">
			<tr>
				<th colspan="4" style="font-weight:bold;">
					<?php echo Dictionnary::translate( "comment" ) ?> :
				</th>
			</tr>
			<tr>
				<td colspan="4" style="white-space:normal;">
					<?php echo nl2br( $supplierInfos[ "comment" ] ) ?>
				</td>
			</tr>
		</table>
		<?php
		
	}
		
	?>
	<p style="text-align:center; margin-top:20px;">
		<a href="#" onclick="showSupplierInfos( <?php echo $idsupplier ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/btn_modify.jpg" style="border-style:none;" /></a>
	</p>
	</div>
	<?php
		
}

//-----------------------------------------------------------------------------------------------------

function getSelectCharge( $idcharge ){
	
	
	
	if( empty( $idcharge ) )
		return "";
		
	$lang 	= "_1";
	$db 	=& DBUtil::getConnection();
	
	$query = "SELECT charge_select$lang AS name FROM charge_select WHERE idcharge_select = $idcharge LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getCountryName( $idstate ){

	
	
	if( !$idstate )
		return "";
		
	$db = &DBUtil::getConnection();
	$lang = "_1";
	
	$query = "SELECT name$lang AS name FROM state WHERE idstate = $idstate LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getPaymentTxt( $idpayment ){
	
	
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = "_1";
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getPaymentDelayTxt( $idpayment ){
	
	
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = "_1";
	$db = &DBUtil::getConnection();
	
	$query = "SELECT payment_delay$lang AS name FROM payment_delay WHERE idpayment_delay = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

?>