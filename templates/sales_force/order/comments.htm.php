<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE COMMENTAIRES ++++++++++++++++++++++++++++++++++++++++++ -->
<?php 

anchor( "UpdateComment" );

GraphicFactory::table_start_infos('Commentaires');

$commentaire = $Order->getValue('comment');

if(empty($commentaire) AND ($Str_status=="InProgress" || $Str_status == "DemandInProgress")){
	$commentaire = Dictionnary::translate("gest_com_return_order_signed");
}
?>
		<!-- Commentaires -->
	
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order/comments.htm.php</span>
<?php } ?>

		<table class="PJ_Table" width="100%">
		<tr>
			<th style="padding:5px;"><?php  echo Dictionnary::translate("comment") ; ?></th>
			<td style="padding:5px;width:100%;"><input<?php echo $disabled ?> type="text" style="width:100%;" maxlength="500" name="comment" value="<?=$commentaire?>"></td>
		</tr>
		</table>
		<br />	
		<div align="center">
			<?GraphicFactory::submit("btn_modify", $name = "UpdateComment", $id = "UpdateComment");?></div>
<?php GraphicFactory::table_end();?>