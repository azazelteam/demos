<?php 
	$customer = new Customer( $Order->get( "idbuyer" ), $Order->get( "idcontact" ) ); 
	include_once( "$GLOBAL_START_PATH/sales_force/buyer_history.php" );
?>

<script type="text/javascript">
/* <![CDATA[ */

	// PopUp pour voir l'historique d'un client
	function seeBuyerHisto(){
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-1010)/2;
		window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0');
	}
	
	function visibility( thingId, txtAff, txtMasque ){
		var targetElement; 
		var targetElementLink;
		targetElement = document.getElementById( thingId );
		targetElementLink = document.getElementById( thingId + 'Link' );
		
		if( targetElement.style.display == "none" ){
			targetElement.style.display = "";
			targetElementLink.innerHTML = txtMasque;
		} else{
			targetElement.style.display = "none";
			targetElementLink.innerHTML = txtAff;
		}	
	}
	
/* ]]> */
</script>


<?php anchor( "Order2Billing", true );?>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/order/buyer.htm.php</span>
<?php } ?>


<!-- Bloc formulaire -->
<div class="contentResult" style="margin-bottom: 10px;">
	
	<h1 class="titleEstimate">
		<span class="textTitle">
			<a title="Dupliquer la commande" href="<?php echo URLFactory::getHostURL(); ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $Order->getId(); ?>&amp;duplicate" onclick="window.open(this.href); return false;">
				<img src="/images/back_office/content/editcopy.png" alt="Dupliquer la commande" />
			</a>
			<?php if($Order->get( "status" ) == "Ordered"){?>
				Confirmation de commande n° <?php echo $IdOrder ?> - <?php echo $Order->getCustomer()->get('company'); ?>
			<?php }else{?>
				Commande client n° <?php echo $IdOrder ?> - <?php echo $Order->getCustomer()->get('company'); ?>
			<?php } ?>
			
            <?php		
				switch( $customer->get( 'insurance_status') ){
            		case 'Refused':
            			echo "<span class='msgError'>(Refusé à l'assurance crédit)</span>";
            			break;
            		
            		case 'Deleted':
            			echo "<span class='msgError'>(Supprimé à l'assurance crédit)</span>";
            			break;
            			
            		case 'Terminated':
            			echo "<span class='msgError'>(Résilé à l'assurance crédit)</span>";
            			break;
            	}
			?>
		</span>
		<span class="selectDate">
			<p style="font-size:14px;">Statut : 
			<?php
				
				if( $Order->get( "status" ) == Order::$STATUS_CANCELLED ) 
					echo "Annulée";
				if( $Order->get( "status" ) == Order::$STATUS_DEMAND_IN_PROGRESS ) {
					
					if( $Order->get( "pro_forma" ) == 1 )
						echo "Facture Pro Forma envoyée<br />En attente de paiement<br />";
					else echo "Commande envoyée<br />En attente de confirmation<br />";
				}
				if( $Order->get( "status" ) == Order::$STATUS_ORDERED &&  $Order->get( 'idpayment' ) == 1 ) {
					echo "Confirmée<br />";
				} 
				if( $Order->get( 'idpayment' ) != 1 ) {
					echo Dictionnary::translate( $Order->get( "status" ) ) . "<br />";
				}

				if( $Order->get( "cash_payment" ) && $Order->get( "status" ) == Order::$STATUS_ORDERED ) {
					echo "Payée comptant";
					if( $Order->get( 'idpayment' ) == 1) {
						echo " par CB";
					}
				}
			?></p>			
		</span>
		<div class="spacer"></div>
	</h1>
	<div class="spacer"></div>
	
	<div class="blocEstimateResult" style="margin:0;"><div style="margin:0 5px 5px 5px;">
		<div class="blocMLeft" style="margin-bottom:0px;">

			<!-- Suivi par -->
			<?php
			$Base = &DBUtil::getConnection();		
			$rs = &DBUtil::query("select iduser, firstname, lastname from user WHERE available IS TRUE AND hasbeen IS FALSE ORDER BY firstname ASC, lastname ASC"); 
			if($rs->RecordCount()>0)
			{
				for($i=0 ; $i < $rs->RecordCount() ; $i++)
				{
					$value = $rs->Fields('iduser');
					$view = $rs->Fields('firstname')." ".$rs->Fields('lastname');
					$array[$value] = $view;
					$rs->MoveNext();
		        }
			$principal = DBUtil::getDBValue( "admin_principal", "user", "iduser", User::getInstance()->getId() );
			}
			?>
			
			<script type="text/javascript">
			/* <![CDATA[ */
			$(document).ready(function() {
				$('.editCom').editable('<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $IdOrder ?>&ComChanged=On', { 
			 		data   : '<?php print json_encode($array); ?>',
				    type   : 'select',
				    submit : 'OK',
				    name   : 'iduser_name',
				    id     : 'iduser',
				    style  : 'inherit'     
				});
			});		
			/* ]]> */
			</script>	
			
			<table cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td><p><strong>Crée le : </strong></p></td>
				<td><p><?php echo humanReadableDatetime( $Order->get( "DateHeure" ) ); ?></p></td>
			</tr>
			<tr>
				<td><p><strong>Commercial affaire : </strong>&nbsp;</td>
				<td><p>
					<?php if( User::getInstance()->get("admin") ) { ?>
						<span <?php if($principal == 1){?> class="editCom" <?php } ?>><?php echo getCommercialFullName( $Order->get( "iduser" ) ) ?></span>
		            <?php } else { 
		            	echo htmlentities( DBUtil::getDBValue( "firstname", "user", "iduser", $Order->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $Order->get( "iduser" ) ) );
					} ?>
					<span style="font-size:9px;">(<strong><?php echo Dictionnary::translate( "salesman_buyer"); ?></strong> : <?php echo getCommercialFullName( $Order->getCustomer()->get( "iduser" ) ); ?>)</span>
				</p></td>
			</tr>
			</table>
			<div class="spacer"></div>

		</div>
		
		<div class="floatright">
			<p><strong>Origine : </strong> 
			<?php $estimateId = $Order->get( "idestimate" ); if( $estimateId != 0){ ?>
				Devis n° <a onclick="javascript: window.open(this.href) ; return false ;" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $estimateId ?>"><?php echo $estimateId ?> (Accéder au devis)</a>
			<?php }else echo htmlentities( DBUtil::getDBValue( "source_1", "source", "idsource", $Order->get( "idsource" ) ) ) ?></p>
			<div class="spacer"></div>		
		</div>
		<div class="spacer"></div>
		
		
		
		<script type="text/javascript">
		<!--
			//onglets
			$(function() {
				$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
			});
		-->
		</script>
		
		
		<div id="container" style="min-height:135px;">
			
			

		
	        <ul class="menu">
				<li class="item"><a href="#infos-client"><span><?php echo $Order->getCustomer()->get( "contact" ) ? "Prospect n°" : "Client n°"; ?><?php echo $Order->getCustomer()->get( "idbuyer" );  ?></span></a></li>
				<li class="item"><a href="#contact"><span>Autres contacts</span></a></li>
				<li class="item"><a href="#comments">
					<span>Commentaires</span></a>
					<?php if($Order->get( "todo" )) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
				<?php
					// ------- Mise à jour de l'id gestion des Index  #1161
					$rs =& DBUtil::query( "SELECT iddelivery, idbilling_adress FROM `order` WHERE idorder = '$IdOrder'"  );
	
					$deliveryAddress = $rs->fields( "iddelivery" ); 
					$billingAddress = $rs->fields( "idbilling_adress" );
				?>
				<li class="item"><a href="#adresses">
					<span>Adresses complémentaires</span></a>
					<?php if($deliveryAddress || $billingAddress) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
				<li class="item"><a href="#historique-client"><span>Historique</span></a></li>
				<li class="item"><a href="#visualiser-imprimer"><span>Visualiser et imprimer</span></a></li>
				
				<?php
					$offer_signed = $Order->get( "offer_signed" );
					$supplier_offer = $Order->get( "supplier_offer" );
					$supplier_doc = $Order->get( "supplier_doc" );
					$hasAttachments = !empty( $offer_signed) || !empty( $supplier_offer) || !empty( $supplier_doc);
				?>
				<li class="item"><a href="#piece-archive">
					<span>Pièces archivées</span></a> 
					<?php if($hasAttachments) { ?>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-warning.png" width="17" height="17" class="iconsNotif" />
					<?php } ?>
				</li>
				
			</ul>
			<div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0 8px -5px; width: 101%;"> </div>
			
			
			<!-- *********** -->
			<!--    Client   -->
			<!-- *********** -->
			<div id="infos-client">
				<?php
					if( $Order->getCustomer()->getId() == DBUtil::getParameterAdmin( "internal_customer" ) ){
						displayVirtualCustomerInfos($Order);
					} else { 	
						displayCustomerInfos($Order);
					}
				?>
				<div class="spacer"></div>			
			</div>
			
			<!-- *************** -->
			<!-- Autres contacts -->
			<!-- *************** -->
			<div id="contact" style="display:none;">
				<h2 class="subTitle">Sélectionner le contact pour cette commande</h2>
				<div class="spacer"></div>
				
				<!-- Liste des contacts -->
				<?php
					$rs =& DBUtil::query( "SELECT c.idcontact, CONCAT( t.title_1, ' ', c.lastname,' ', c.firstname ) AS nomPrenom, c.faxnumber, c.phonenumber, c.mail, c.gsm, f.function_1, c.idfunction FROM contact c, title t, function f WHERE idbuyer = '" . $customer->getId() . "' AND t.idtitle = c.title AND c.idfunction = f.idfunction ORDER BY idcontact ASC" );
					$i=0;
					while( !$rs->EOF() ){
						$selected = $Order->get( "idcontact" ) == $rs->fields( "idcontact" ) ? " checked='checked'" : "";	
						$bgcolor = ($i++ & 1) ? '#FFFFFF' : '#EEEEEE';
						if($rs->fields( "mail" ) != "") {
				?>
	            		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px; width:25%;"><div style="margin:5px; padding:5px; height:115px; background:<?php echo $bgcolor ?>; border:1px solid #EEEEEE;">	
			            	<input style="float:right;" type="radio" name="contactSelect"<?php echo $selected; ?> onchange="document.location='<?php echo URLFactory::getHostURL(); ?>/sales_force/com_admin_order.php?update_contact&idcontact=<?php echo $rs->fields( "idcontact" ); ?>&IdOrder=<?php echo $Order->getId(); ?>'" />
			            	<p><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/icons-user.png" alt="user" />&nbsp;<strong><?php echo htmlentities( $rs->fields( "nomPrenom" ) ); ?></strong></p>
			            	<p><strong>Fonction</strong> : <?php if($rs->fields( "idfunction" ) != "0") { ?><?php echo $rs->fields( "function_1" ); ?><?php } else { ?>Non renseigné<?php } ?></p>
			            	<p>Email : <a class="blueLink" href="mailto:<?php echo $rs->fields( "mail" ); ?>"><?php echo $rs->fields( "mail" ); ?></a></p>
			            	<table border="0" cellpadding="0" cellspacing="0" width="100%"><tr>
			            	<td width="50%" style="background:none;"><p><?php if($rs->fields( "phonenumber" )) { ?>Tél. : <?php echo $rs->fields( "phonenumber" ); ?><?php } ?>&nbsp;</p></td>
				            <td width="50%" style="background:none;"><p><?php if($rs->fields( "faxnumber" )) { ?>Fax : <?php echo $rs->fields( "faxnumber" ); ?><?php } ?>&nbsp;</p></td>
				            </tr></table>
				            <div class="spacer"></div>
				            <p><?php if($rs->fields( "gsm" ))  { ?>GSM : <?php echo $rs->fields( "gsm" ); ?></p><?php } ?>&nbsp;</p>		            
			            	<div class="spacer"></div>
			            </div></div>					
				<?php
						}
						$rs->MoveNext();
					}
				?>
				<div class="spacer"></div>
				
			</div>
			
			
			<!-- *********** -->
			<!--    Comments   -->
			<!-- *********** -->
			<div id="comments" style="display:none;">
				
				<div class="blocMultiple blocMLeft BlocDevis" style="margin-bottom: 0px;">
			        <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px;">	
			            <div style="min-height:100px;">
			            <p><strong><?php echo $Order->getCustomer()->get('company'); ?></strong>
						(n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>)</p>
			            <p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ); ?> <?php echo $Order->getCustomer()->getContact()->get( "firstname" ); ?></p>
			            <p><?php echo $Order->getCustomer()->get('adress'); ?>
			            <?php $adr2= $Order->getCustomer()->get('adress_2');if(empty($adr2)){echo "&nbsp;";}else{ echo $adr2;} ?></p>
			            <p><strong><?php echo $Order->getCustomer()->get('zipcode'); ?> <?php echo $Order->getCustomer()->get('city'); ?></strong> - <?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ); ?></p>
			            </div><br/>						
			        </div></div>
			        <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px;">	
			            <div style="min-height:100px;">
			            <?php if($Order->getCustomer()->getContact()->get( "phonenumber" )) { ?>
			            	<p><strong>Tél. :</strong> <?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "phonenumber" )); ?></p>
			            <?php } ?>
			            <?php if($Order->getCustomer()->getContact()->get( "faxnumber" )) { ?>
			            	<p><strong>Fax :</strong> <?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "faxnumber" )); ?></p>
			            <?php } ?>
			            <?php if($Order->getCustomer()->getContact()->get( "gsm" )) { ?>
			            	<p><strong>GSM :</strong> <?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "gsm" )); ?></p>
			            <?php } ?>   
			            <p>Email : <a class="blueLink" href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?></a></p>
			           	<p><a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?key=<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>&amp;IdOrder=<?php echo $Order->getId() ?>" onclick="window.open(this.href); return false;" >
						<?php echo Dictionnary::translate( $Order->getCustomer()->get( "contact" ) ? "gest_com_edit_contact" : "gest_com_edit_buyer" ) ; ?> n°<?php echo $Order->getCustomer()->get( "idbuyer" );  ?></a></p>
			           	</div><br/>
			        </div></div>
			        <div class="spacer"></div>        
				</div>
				
				<div class="blocMultiple blocMRight" style="margin-bottom: 0px;">

			    	<form name="TodoInfosForm" id="TodoInfosForm" action="com_admin_order.php?IdOrder=<?php echo $Order->getId() ?>#comments" method="post" enctype="multipart/form-data">
			            <input type="hidden" name="IdOrder" value="<?php echo $Order->get( "idorder" ) ?>" />
			            
			            <label style="width:100%; background:none;"><span>Commentaires</span></label>
			            <div class="spacer"></div>	
			            <div id="clientsCommentsDiv"><div style="margin:0 5px 0 0;">
			            	<textarea name="todo" class="textInput" style="width: 99%; height:50px; margin:0; -moz-border-radius:5px; -webkit-border-radius:5px;"><?php echo html_entity_decode( $Order->get( "todo" ) ) ?></textarea>
			            </div></div>
			            <div class="spacer"></div>	
			            <input type="submit" class="blueButton" style="margin: 3px 4px 0pt 0pt; float: right;" id="UpdateTodoMessage" name="UpdateTodoMessage" value="Enregistrer" />
			        </form>	
				</div>
				<div class="spacer"></div>		
			</div>			

			<!-- ************ -->
			<!--   Adresses   -->
			<!-- ************ -->
			<div id="adresses" style="display:none;">
				<?php include( "$GLOBAL_START_PATH/templates/sales_force/order/addresses.htm.php" ); ?>
				<div class="spacer"></div>
			</div>
			
			
			<!-- *********** -->
			<!-- Historique -->
			<!-- *********** -->
			<?php $nbdevis = 0; $nbcdes = 0; $totDevis = 0; $totCdes = 0;
			$lastDevis = "-"; $lastCde = "-";

			$nbdevis 	= DBUtil::query("SELECT count(idestimate) as cpte FROM estimate WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('cpte');
			$nbcdes		= DBUtil::query("SELECT count(idorder) as cpte FROM `order` WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('cpte');
			$totDevis	= DBUtil::query("SELECT SUM(total_amount_ht) as sumo FROM estimate WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('sumo');
			$totCdes	= DBUtil::query("SELECT SUM(total_amount_ht) as sumo FROM `order`  WHERE idbuyer='".$Order->getCustomer()->get( "idbuyer" )."'")->fields('sumo'); ?>
						
			<div id="historique-client" style="display:none;">

				<div class="blocMultiple blocMLeft BlocDevis" style="margin-bottom: 0px;">
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px 5px 5px 0;">	
		            	<p><strong>Nombre devis</strong> : <?php echo $nbdevis?></p>
		            </div></div>		            
		            <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px;">	
			           	<p><strong>Total devis HT</strong> : <?php echo Util::priceFormat($totDevis)?></p>
			        </div></div>
			        <div class="spacer"></div> 
				</div>
				
				<div class="blocMultiple blocMRight BlocDevis" style="margin-bottom: 0px;">
		            <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px;">	
		            	<p><strong>Nombre cdes.</strong> : <?php echo $nbcdes?></p>
		            </div></div>	            
		            <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px;">	
						<p><strong>Total cdes. HT</strong> : <?php echo Util::priceFormat($totCdes)?></p>
		            </div></div>
		            <div class="spacer"></div>  
				</div>
				<div class="spacer"></div>
				
				<?php	
					$idbuyer = $Order->getCustomer()->get( "idbuyer" );
					include_once( "$GLOBAL_START_PATH/sales_force/buyer_history_table.php" );	
				?>
						

			</div><!-- #historique-client -->	
			
			
			<!-- ************ -->
			<!--  Visualiser  -->
			<!-- ************ -->
			<div id="visualiser-imprimer" style="display:none;">
				
				<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">
					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">
						<?php
							$rs =& DBUtil::query( "SELECT idbilling_buyer FROM billing_buyer WHERE idorder = '" . $Order->getId() . "'" );
							if( $rs->RecordCount() ){
								
								foreach( $rs->fields as $key => $value){ ?> 
									<p style="margin-bottom:5px;">
									<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $value?>" class="normalLink" onclick="window.open(this.href); return false;">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
									&nbsp;PDF facture n°<?php echo $value ?></a></p>
								<?php } ?>
								
								<p style="margin-bottom:5px;">
								<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
								<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
								&nbsp;PDF commande n°<?php echo $IdOrder ?></a></p>
								
							<?php } else {
								if( $Order->get( "status" ) == "InProgress"){?>
									
									<p style="margin-bottom:5px;">
									<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
									&nbsp;PDF commande n°<?php echo $IdOrder ?></a></p>
							
									<p style="margin-bottom:5px;">
									<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
									&nbsp;Fax PDF commande n°<?php echo $IdOrder ?></a></p>
									
								<?php } else{?>
									<p style="margin-bottom:5px;">
									<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
									&nbsp;PDF commande n°<?php echo $IdOrder ?></a></p>
									
									<p style="margin-bottom:5px;">
									<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_order.php?idorder=<?php echo $IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
									&nbsp;Fax PDF commande n°<?php echo $IdOrder ?></a></p>
								<?php }
							} 
						?>
						
						<p><a style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_proforma_invoice.php?idorder=<?php echo $IdOrder?>" class="normalLink" onclick="window.open(this.href); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" />
						&nbsp;PDF Pro Forma n°<?php echo $IdOrder ?></a></p>
						
					</div></div>
					
					
					<?php if( ( $Order->get( "status" ) == "InProgress" || $Order->get( "status" ) == "DemandInProgress" || $Order->get( "status" ) == "Ordered" || $Order->get( "status" ) == "Send" || $Order->get( "status" ) == "Invoiced" || $Order->get( "status" ) == "Paid" ) ) { ?> 
						<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin: 5px;">
	
							<?php if(  strlen( $Order->getCustomer()->getContact()->get( "mail" ) ) ) {?>
						
									<p style="margin-bottom:5px;">
									<a style="display:block;" href="transmit.php?IdOrder=<?php echo $IdOrder ?>" class="normalLink">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
									&nbsp;<?php  echo Dictionnary::translate("gest_com_order_confirm") ; ?></a></p>
							
									<?php if(  $Order->get( "status" ) != "Invoiced" AND $Order->get( "status" ) != "Paid" ) { ?>
										<p style="margin-bottom:5px;">
										<a style="display:block;" href="transmit.php?IdOrder=<?php echo $IdOrder ?>&type=dde" class="normalLink">
										<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
										&nbsp;<?php  echo Dictionnary::translate("gest_com_ask_order_confirm") ; ?></a></p>
									<?php } ?>
									
									<p><a style="display:block;" href="transmit.php?IdOrder=<?php echo $IdOrder ?>&type=pro" class="normalLink">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-mail.png" />
									&nbsp;<?php  echo Dictionnary::translate("gest_com_proforma") ; ?></a></p>
					
							<?php } ?>
						</div></div>
					</div>
					<div class="blocMultiple blocMRight" style="margin-bottom: 0px;">
						
						<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin: 5px;">
							<p style="margin-bottom:5px;">
							<a href="transmit_fax.php?IdOrder=<?php echo $IdOrder ?>" class="normalLink">
							<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-fax.png" />
							&nbsp;<?php  echo Dictionnary::translate("gest_com_order_confirm") ; ?></a></p>
							
							<?php if(  $Order->get( "status" ) != "Invoiced" AND $Order->get( "status" ) != "Paid" ){?>
								<p><a style="display:block;" href="transmit_fax.php?IdOrder=<?php echo $IdOrder ?>&type=dde" class="normalLink">
								<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/icons-menu-fax.png" />
								&nbsp;<?php  echo Dictionnary::translate("gest_com_ask_order_confirm") ; ?></a></p>
							<?php } ?>
						</div></div>
						<div class="spacer"></div>
						
					</div>
					<?php } else { ?>
						<div class="spacer"></div>
					</div>
					<?php } ?>
					<div class="spacer"></div>
					
					
					
				</div> <!-- #visualiser-imprimer -->
				
				<!-- ************ -->
				<!--  Pieces arc  -->
				<!-- ************ -->
				<div id="piece-archive" style="display:none;">

					<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;">    
						<form name="linkJoint" id="linkJoint" action="com_admin_order.php?IdOrder=<?php echo $Order->getId() ?>#piece-archive" method="post" enctype="multipart/form-data">

							<label style="width:50%;"><span><?php echo Dictionnary::translate( "gest_com_estimate_signed" ) ?></span></label>
							<input type="file" name="offer_signed" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
							<?php if( !empty( $offer_signed ) ){ ?>
								&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/order/<?php echo $offer_signed ?>" style="margin:5px 0 0 0;" class="floatleft" onclick="window.open( this.href ); return false;"><img src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
								<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_offer_signed" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
							<?php } ?>
							<div class="spacer"></div>
							
							<label style="width:50%;"><span><?php echo Dictionnary::translate( "gest_com_supplier_offer" ) ?></span></label>
							<input type="file" name="supplier_offer" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
							<?php if( !empty( $supplier_offer ) ){ ?>
								&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/order/<?php echo $supplier_offer ?>" style="margin:5px 0 0 0;" class="floatleft" onclick="window.open( this.href ); return false;"><img src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
								<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_supplier_offer" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
							<?php } ?>
							<div class="spacer"></div>
							
							<label style="width:50%;"><span><?php echo Dictionnary::translate( "gest_com_doc_supplier" ) ?></span></label>
							<input type="file" name="supplier_doc" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
							<?php if( !empty( $supplier_doc ) ){ ?>
								&nbsp;<a href="<?php echo $GLOBAL_START_URL ?>/data/order/<?php echo $supplier_doc ?>" style="margin:5px 0 0 0;" class="floatleft" onclick="window.open( this.href ); return false;"><img src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
								<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="Delete_supplier_doc" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
							<?php } ?>
							<div class="spacer"></div>

							<label style="width:50%;"><span>Confirmation de Paiement</span></label>
							<input type="file" name="payment_confirmation" size="9" class="textSimple" style="background: rgb(255, 255, 255); height: 21px; color: rgb(80, 80, 80); width:35%;" />
							<?php if( strlen( $Order->get( "payment_confirmation" ) ) ){ ?>
								&nbsp;<a href="/data/order/<?php echo $Order->get( "payment_confirmation" ); ?>" style="margin:5px 0 0 0;" class="floatleft" onclick="window.open( this.href ); return false;"><img src="/images/back_office/content/oeil.gif" alt="Visualiser la pièce jointe" align="absmiddle" /></a>
								<input class="floatleft" style="margin:3px 0 0 2px;" type="image" src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg" name="delete_payment_confirmation" onclick="return confsuppPJ(this)" align="absmiddle" alt="Supprimer la pièce jointe"/>
							<?php } ?>
							<div class="spacer"></div>
							
							<input type="hidden" name="IdOrder" value="<?php echo $Order->get( "idorder" ); ?>" />			
			                <input type="image" value="Enregistrer" name="UpdateFiles" id="UpdateFiles" style="margin: 5px 5px 0pt 0pt;" class="blueButton floatright" />
			                <div class="spacer"></div>

						</form>
						<div class="spacer"></div>
					</div>
					<div class="spacer"></div>
					
					
				</div> <!-- #piece-archive -->
	
		</div> <!-- #container -->
	</div></div>
	<div class="spacer"></div>
	
	
	<?php
		$rs =& DBUtil::query( "SELECT DISTINCT( idorder_supplier ), idsupplier, `status`, iddelivery, DateHeure, total_amount FROM `order_supplier` WHERE idorder = '" . $Order->getId() . "'" );
		if( $rs->RecordCount() ){
	?>
		<h1 class="titleEstimate">
			<span class="textTitle">Informations sur la commande</span>
			<div class="spacer"></div>
		</h1>
		<div class="spacer"></div>

		<div class="blocEstimateResult" style="margin:0;"><div style="margin:0 5px 5px 5px;">
			<?php listDeliveryNotes( $Order ); ?>
		</div></div>
		<div class="spacer"></div>

	<?php
		}
	?>

		
</div> <!-- ContentDyn -->



<?php

//------------------------------------------------------------------------------------------------------
function getInvoicesForOrder($idorder){
	
	
	
	$bdd = DBUtil::getConnection();
		// ------- Mise à jour de l'id gestion des Index  #1161
	$Query = "SELECT idbilling_buyer FROM billing_buyer WHERE idorder='$idorder'";
	$rs = $bdd->Execute($Query);
	
	if($rs===false)
		die("Impossible de récupérer les factures de la commande $idorder");
	
	if($rs->RecordCount()>0){
		return $rs->fields;
	}else{
		return false;
	}
		
}

//-------------------------------------------------------

function listDeliveryNotes( &$order ){
	
	global $GLOBAL_START_URL;
	$internal_delivery = DBUtil::getParameterAdmin( "internal_delivery" );
	$rs =& DBUtil::query( "SELECT DISTINCT( idorder_supplier ), idsupplier, `status`, iddelivery, DateHeure, confirmation_date, dispatch_date, total_amount,`cross-docking` as crossDocking FROM `order_supplier` WHERE idorder = '" . $order->getId() . "'" );
		
	if( $rs->RecordCount() ){
	?>
		
		<table class="dataTable devisTable" cellpadding="4" cellspacing="0">
			<tr>
				<th>Commande fournisseur n°</th>
				<th><?php echo Dictionnary::translate("supplier") ?></th>
				<th><?php echo Dictionnary::translate("status") ?></th>
                <th>Livraison</th>
				<th>Date de commande</th>
				<th>Date confirmée</th>
				<th>Montant TTC</th>
			</tr>
			<?php
				while(!$rs->EOF()) {
					if( $internal_delivery == $rs->fields( "crossDocking" ) ) $livraison = "Stock";
                    else $livraison = "Drop-shipping";
                    ?>
					<tr>
						<td><a href="./supplier_order.php?IdOrder=<?php echo $rs->fields('idorder_supplier')?>" class="blueLink" onclick="window.open(this.href); return false;" style="font-size:13px;font-weight:bold;"><?php echo $rs->fields('idorder_supplier')?></a></td>
						<td><b><?php echo htmlentities( DBUtil::query( "SELECT name FROM supplier WHERE idsupplier = '" . $rs->fields( "idsupplier" ) . "' LIMIT 1" )->fields( "name" ) ) ?></b></td>
						<td><?php echo DBUtil::query( "SELECT order_supplier_status_1 FROM order_supplier_status WHERE order_supplier_status = '" .  Util::html_escape( $rs->fields("status") ) . "' LIMIT 1" )->fields( "order_supplier_status_1" ) ?></td>
						<td><?php echo $livraison ?></td>
                        <td><?php echo usDate2eu( substr( $rs->fields('DateHeure'), 0, 10 ) ) ?></td>
						<td><?php echo usDate2eu( substr( $rs->fields('confirmation_date'), 0, 10 ) ) ?></td>
						<td><?php echo Util::priceFormat($rs->fields('total_amount')) ?></td>
					</tr>
					<tr>
						<th colspan="7">
						<?php echo Dictionnary::translate("edition") ; ?> : <a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_supplier_order.php?idorder=<?php echo $rs->fields('idorder_supplier')?>" onclick="window.open(this.href); return false;"><?php  echo Dictionnary::translate("gest_com_order_supplier") ; ?></a>
						<?php
						
							include_once( dirname( __FILE__ ) . "/../../../objects/SupplierOrder.php" );
							
							$bls = SupplierOrder::getBLs( $rs->fields( "idorder_supplier" ) );

							if( DBUtil::getParameterAdmin( "internal_delivery" ) == $rs->fields( "iddelivery" ) ){
								$blCount = 0;
								foreach( $bls as $idbl_delivery ){
									if( $blCount )
										echo "<br />";
									echo " - <a class='blueLink' href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\">" . Dictionnary::translate( "gest_com_supplier_bl" ) . "</a> - <a class='blueLink' href=\"/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\">BL Dépôt</a>";
									$blCount++;
								}
							} else {
								$blCount = 0;
								foreach( $bls as $idbl_delivery ){
									if( $blCount )
										echo "<br />";
									echo " - <a class='blueLink' href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery\" target=\"_blank\">" . Dictionnary::translate( "gest_com_supplier_bl" ) . "</a>";	
									$blCount++;
								}
							}
							
							/* modifier la livraison */
							
							$query = "
							SELECT idorder_supplier
							FROM bl_delivery
							WHERE idorder = '" . $order->getId() . "'
							AND idbilling_buyer = '0'
							AND date_send LIKE '0000-00-00'
							AND date_delivery LIKE '0000-00-00'";
							
							if( DBUtil::query( $query )->RecordCount() ){
							?>
								- <a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/delivery_notes.php?idorder_supplier=<?php echo $rs->fields( "idorder_supplier"); ?>">Modifier la livraison</a>
							<?php } ?>
						</th>
					</tr>
					<?php if( $blCount ) { ?>
					<tr>
						<th colspan="7" style="text-align:right;">
							<?php
								foreach( $bls as $idbl_delivery ){
									$availability_date = DBUtil::getDBValue( "availability_date", "bl_delivery", "idbl_delivery", $idbl_delivery );
									if( $availability_date != "0000-00-00" )
										echo "Date de mise à disposition prévue : ".usDate2eu( $availability_date );
								}
							?> - 
							<?php
								foreach( $bls as $idbl_delivery ){
									$date_delivery = DBUtil::getDBValue( "date_delivery", "bl_delivery", "idbl_delivery", $idbl_delivery );
									if( $date_delivery != "0000-00-00" )
										echo "Date expédition : ".usDate2eu( $date_delivery );
								}
							?>
						</th>
					</tr>
					<?php }
			
					$rs->MoveNext(); 
			
				}
				
		?>
		</table>
		<?php
		
	}
	
}

//------------------------------------------------------------------------------------------------

function displayVirtualCustomerInfos( Order &$order ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	?>
	
	<h1 class="titleSearch">
		<span class="textTitle">Coordonnées du prospect ou client à créer</span>
		<div class="spacer"></div>
	</h1>
	<div class="spacer"></div>
	
	<div class="blocResult"><div style="margin:5px;">
		<p>Cette commande express va être créée sur votre compte personnel. Vous pouvez attribuer cette commande à un prospect ou à un client 
		enregistré ou créer sa fiche d'identité en cliquant sur le bouton <b>Créer un nouveau prospect</b></p>
		
		<p><b>Attribuer la commande par :</b></p>

		<ul>
			<li style="height:25px;">
				&#0155; Cr&eacute;ation d'un nouveau prospect
				<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=contact&amp;VirtualOrder=<?php echo $order->getId() ?>">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Créer un prospect" style="margin-left:15px; vertical-align:middle;" /></a>
			</li>
			<li style="height:25px;">
				&#0155; Recherche du client ou Prospect 
				<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_select.php?buyer&amp;VirtualOrder=<?php echo $order->getId() ?>">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Rechercher" style="margin-left:10px; vertical-align:middle;" /></a>
			</li>
			<li style="height:25px;">
				&#0155; N° de client ou prospect 
				<input type="text" id="RealBuyer" value="" class="textInput" />
				<input type="button" class="blueButton" value="Attribuer" onclick="document.location = '<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_order.php?ReassignBuyer&RealBuyer=' + $('#RealBuyer').val() + '&IdOrder=<?php echo $order->getId(); ?>';" />
				<?php
					include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
					//AutoCompletor::completeFromDB( "RealBuyer", "buyer", "idbuyer", 15 );
					AutoCompletor::completeFromDBPattern( "RealBuyer", "buyer", "idbuyer", "{company}", 15 );
					
				?>
			</li>
		</ul>
		<div class="spacer"></div>
	</div></div>
	
	

	<?php					
}

//------------------------------------------------------------------------------------------------

function displayCustomerInfos( Order &$order ){
	
	global $GLOBAL_START_URL, $customer;
?>

	<div class="blocMultiple blocMLeft BlocDevis" style="margin-bottom: 0px;">
        <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:5px;">	
            <div style="min-height:100px;">
            <p><strong><?php echo $order->getCustomer()->get('company'); ?></strong>
			(n°<?php echo $order->getCustomer()->get( "idbuyer" );  ?>)
			<?php global $relaunched; if($relaunched){?>
				<b><a href="#" class="blueLink" onclick="seeRelaunch(); return false;">!! Attention client relancé !!</a>
			<?php } ?>
			</p>
            <p><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $order->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $order->getCustomer()->getContact()->get( "lastname" ); ?> <?php echo $order->getCustomer()->getContact()->get( "firstname" ); ?></p>
            <p><?php echo $order->getCustomer()->get('adress'); ?>
            <?php $adr2= str_replace("&Atilde;&uml;", "è",$order->getCustomer()->get('adress_2'));if(empty($adr2)){echo "&nbsp;";}else{ echo $adr2;} ?></p>
            <p><strong><?php echo $order->getCustomer()->get('zipcode'); ?> <?php echo $order->getCustomer()->get('city'); ?></strong> - <?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $order->getCustomer()->get( "idstate" ) ); ?></p>
            </div><br/>
            
        

		<p><strong>Profil client : </strong><span class="editUserProfil editUser"><?php echo $order->getCustomer()->get( "idcustomer_profile" ) ? DBUtil::getDBValue( "name", "customer_profile", "idcustomer_profile", $order->getCustomer()->get( "idcustomer_profile" ) ) : "N/C"; ?></span></p>
            <div class="spacer"></div>
         
		    <?php $akilae_source = DBUtil::getParameterAdmin("akilae_source");
			if($akilae_source==1){	?>
		 <p><strong>Provenance client</strong> : <?php echo DBUtil::getDBValue( "source_1", "source", "idsource", $order->getCustomer()->get( "idsource" ) ); ?></p>
            <div class="spacer"></div>	
			
		
            <script type="text/javascript">
			/* <![CDATA[ */
			
				$(document).ready(function() {
					$('.editUserProfil').editable('/sales_force/com_admin_order.php?iduser=<?php echo $order->getCustomer()->getId();  ?>&IdOrder=<?php echo $order->getId() ?>&editUserProfil=On', { 
				 		data   : '<?php 
				 		
					 			$rs =& DBUtil::query( "SELECT * FROM customer_profile" );
					 			
					 			$profiles = array();
								while( !$rs->EOF() ){
									
									$profiles[ $rs->fields( "idcustomer_profile" ) ] = $rs->fields( "name" );
									$rs->MoveNext();
									
								}
								
					 			print json_encode( $profiles ); 
					 			
				 			?>',
					    type   : 'select',
					    submit : 'OK',
					    name   : 'idcustomer_profile',
					    id     : '',
					    style  : 'inherit'     
					});
				});	
					
			/* ]]> */
		</script>   
		
		 <p><strong>Provenance commande : </strong></p>     
		 <?php //echo htmlentities( DBUtil::getDBValue( "source_1", "source", "idsource", $Order->get( "idsource" ))) ?>
		<?php } ?>
        </div></div>
        
        <div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:5px;">	
            <div style="min-height:100px;">
            <?php if($order->getCustomer()->getContact()->get( "phonenumber" )) { ?>
            	<p>Tél. : <?php echo Util::phonenumberFormat($order->getCustomer()->getContact()->get( "phonenumber" )); ?></p>
            <?php } ?>
            <?php if($order->getCustomer()->getContact()->get( "faxnumber" )) { ?>
            	<p>Fax : <?php echo Util::phonenumberFormat($order->getCustomer()->getContact()->get( "faxnumber" )); ?></p>
            <?php } ?>
            <?php if($order->getCustomer()->getContact()->get( "gsm" )) { ?>
            	<p>GSM : <?php echo Util::phonenumberFormat($order->getCustomer()->getContact()->get( "gsm" )); ?></p>
            <?php } ?>   
            <p>Email : <a class="blueLink" href="mailto:<?php echo $order->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $order->getCustomer()->getContact()->get( "mail" ); ?></a></p>
           	<p><a class="viewContact"  href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?key=<?php echo $order->getCustomer()->get( "idbuyer" );  ?>&amp;IdOrder=<?php echo $order->getId() ?>" onclick="window.open(this.href); return false;" >
			Voir la fiche client n°<?php echo $order->getCustomer()->get( "idbuyer" );  ?></a></p>
           	</div><br/>
           	
           <!-- <p><strong>Encours garanti HT</strong> : <?php echo $order->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->get( "insurance_amount" ) ) : "-"; ?></p>
			<p><strong>Encours assurance crédit HT</strong> : <?php echo $order->getCustomer()->get('insurance_status')=='Accepted' ? Util::priceFormat( $customer->getInsuranceOutstandingCreditsET() ) : "-"; ?></p>
		   -->
        	<p><strong><?php
				switch( $customer->get( "insurance_status" ) ){
					case "Accepted":
						echo "Date d'assurance : ";
						break;
						
					case "Refused":
						echo "Date de refus : ";
						break;

					case "Deleted":
						echo "Date de suppression : ";
						break;
						
					case "Terminated":
						echo "Date de résiliation : ";
						break;

					default:
						echo "";
						break;
				}					

			?></strong> 
			<?php
				switch( $customer->get( "insurance_status" ) ){
					case "Accepted":
						echo Util::dateFormatEu( $customer->get( "insurance_amount_creation_date" ) );
						break;
						
					case "Refused":
						echo Util::dateFormatEu( $customer->get( "insurance_refusal_date" ) );
						break;

					case "Deleted":
						echo Util::dateFormatEu( $customer->get( "insurance_deletion_date" ) );
						break;
						
					case "Terminated":
						echo Util::dateFormatEu( $customer->get( "insurance_termination_date" ) );
						break;

					default:
						echo "";
						break;
				}					

			?></p>
        </div></div>
        <div class="spacer"></div>
        
        <div class="blocMultiple blocMRight" style="margin-bottom: 0px; width:100%;"><div style="margin:5px;">	        
        <?php
			$customerGrouping = DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $order->getCustomer()->get( "idbuyer" ) );
			if( $customerGrouping ){
			
				$grouping = new GroupingCatalog();
				
				//@todo: Bidouille qui consiste à changer l'idgrouping du groupement à la volée pour charger les infos
				// Le problème vient du fait qu'on ne peut instancier un groupement que depuis le Front-Office
				$grouping->getGroupingInfos( $customerGrouping );
				
				$groupingName = $grouping->getGrouping( "grouping" );
				$groupingLogo = $grouping->getGrouping( "grouping_logo" );				
		?>
				<p><strong>Groupement : </strong><?php echo $groupingName ?></p>
				<div style="margin:0 0 5px 0; text-align:center;">
					<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $groupingLogo ?>" />
				</div>
				<div class="spacer"></div>
		
		<?php } ?>
			
        </div></div>
        <div class="spacer"></div>       
       		        
	</div>
	
	<div class="blocMultiple blocMRight" style="margin-bottom: 0px;">
		       		
   		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px; width:100%;"><div style="margin:5px 5px 0 5px;">	
   		<?php if($order->getCustomer()->get('naf')) { ?>
				<p><strong>APE/NAF</strong> : <?php echo DBUtil::getDBValue("naf_comment", "naf", "idnaf", $order->getCustomer()->get('naf')); ?></p>
		<?php } ?>
   		</div></div>
		<div class="spacer"></div>	
		
   		<div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
			<?php
				$Base = &DBUtil::getConnection();
				$rs =& DBUtil::query("SELECT qty FROM manpower WHERE idmanpower='".$order->getCustomer()->get('idmanpower')."'");
				$qtty = $rs->Fields( "qty" );	
			?>	
			
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<?php if($order->getCustomer()->get('ca')) { ?>
			<tr>
				<td width="50%"><p><strong>Chiffre d'affaire</strong></p></td>
				<td width="50%"><p><?php echo $order->getCustomer()->get('ca'); ?>&euro;</p></td>
			</tr>	
			<?php } ?>
			<?php if($qtty) { ?>
			<tr>
				<td><p><strong>Effectif</strong></p></td>
				<td><p><?php echo $qtty; ?></p></td>
			</tr>		
			<?php } ?>	
			</table>
	        <div class="spacer"></div>	
		</div></div>
		
		<div class="blocMultiple blocMRight" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
		
			<table border="0" cellspacing="0" cellpadding="0" width="100%">
			<?php if($order->getCustomer()->get('resultat')) { ?>
			<tr>
				<td width="50%"><p><strong>Résultat</strong></p></td>
				<td width="50%"><p><?php echo $order->getCustomer()->get('resultat'); ?>&euro;</p></td>
			</tr>	
			<?php } ?>
			<?php if($order->getCustomer()->get('renta_finan')) { ?>	
			<tr>
				<td><p><strong>Rentabilité financière</strong></p></td>
				<td><p><?php echo $order->getCustomer()->get('renta_finan'); ?>%</p></td>
			</tr>		
			<?php } ?>	
			</table>
	        <div class="spacer"></div>	
		</div></div>
		<div class="spacer"></div>	
		
		<!-- <div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0pt 8px 0; width: 99%;"> </div> -->
		
		<?php
		// Recupération du siret
        $querySiret = 'SELECT siret FROM buyer WHERE idbuyer = "'.$order->getCustomer()->get( "idbuyer" ).'"';
        $bdSiret = DBUtil::getConnection();
        $rsSiret = $bdSiret->Execute($querySiret);
        
        if($rsSiret === false){
        	
        }else{
        	$siren = substr($rsSiret->fields("siret"), 0, 9);
        }
        ?>
		
		<?php if(isset($siren) && strlen($siren) > 3){ ?>
			<a href="http://www.manageo.fr/entreprise/rc/preliste.jsp?action=2&siren=<?php echo $siren?>&x=42&y=2" target="_blank" style="margin:0 60px 0 5px;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
			<a href="http://www.societe.com/cgi-bin/recherche?rncs=<?php echo $siren?>&imageField2.x=60&imageField2.y=1" target="_blank" style="margin:0 60px 0 5px;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
		<?php } else { ?>
			<a href="http://www.manageo.fr/" target="_blank" style="margin:0 60px 0 5px;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-manageo_g.jpg" alt="" width="48" height="25" /></a>
			<a href="http://www.societe.com/" target="_blank" style="margin:0 60px 0 5px;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-societe_g.jpg" alt="" width="48" height="25" /></a>
		<?php }  ?>
		
    	<a href="http://www.pagesjaunes.fr/" target="_blank" style="margin:0 60px 0 5px;">
    	<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-pages-jaunes_g.jpg" alt="" width="48" height="25" /></a>
		<a href="http://www.ctqui.com" target="_blank" style="margin:0 60px 0 5px;">
		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/logo-ctqui_g.jpg" alt="" width="48" height="25" /></a>
		
		<div class="spacer"></div>	
		<!-- 
		<p>N° Confirmation Commande Client : 7536</p>
		<p>Date confirmation commande client : 05/07/2010</p>
		
		<div class="blocMultiple blocMLeft"><div class="blocMarge">	
			<p>Commande Fournisseur : Schoeller Arca</p>
			<p>Date Commande fournisseur : 07/07/2010</p>
			<p>Date de mise à disposition : 08/07/2010 	ou Date d'expédition : 09/07/2010</p>
			<p>N° Facture Fournisseur : 12/07/2010</p>
		
		</div></div>
		
		<div class="blocMultiple blocMRight"><div class="blocMarge">	
			<p>N° Facture client : 8793 ou en attente de facturation</p>
			<p>Montant TTC : 4875,00 ¤</p>
			<p>Date de facture : 18/07/2010</p>
			<p>Echéance facture : 31/09/2010</p>
		
		</div></div>
		<div class="spacer"></div>	
	
		-->
	<?php
			
			$query = "
					SELECT title_offer 
					FROM  `order`
					WHERE idorder = '" . $_GET["IdOrder"] . "'
					";

					$rs =& DBUtil::query( $query );
			
			$title_offer = $rs->fields("title_offer");
			
			?>
            
           <form id="linkJoint" action="com_admin_order.php?IdOrder=<?php echo $_GET["IdOrder"] ?>" method="post" enctype="multipart/form-data">
            <div class="blocMultiple blocMLeft" style="margin-bottom: 0px;"><div style="margin:0 5px 5px 5px;">	
                   <p><strong>Titre de la commande</strong><br />
				   <input type="text" name="title_offer" value="<?php echo $title_offer; ?>" /> <input type="submit" class="blueButton" value="ok" /></p>
                    
                    </div>
		       	</div>
	       	
	       		<div class="spacer"></div>
			
			</form>
	</div>
	<div class="spacer"></div>	
	
	<p><?php

		global $msgcontact1, $msgcontact2;
		if( $msgcontact1 != "" || $msgcontact2 !="" ){	?>
			<p class="subTitleWarning">
		<?php   if($msgcontact1!="")
					echo $msgcontact1.' <br/><a href="contact_buyer.php?key='.$order->getCustomer()->get("idbuyer").'">'.Dictionnary::translate("gest_com_valid_data").'</a>';

				if($msgcontact2!="")
					echo $msgcontact2.' <br/><a href="contact_buyer.php?key='.$order->getCustomer()->get("idbuyer").'">'.Dictionnary::translate("gest_com_valid_data").'</a>';
		?>
			</p>
		<?php } ?>
	</p>
	<div class="spacer"></div>	
	
	<?php
			
}

//------------------------------------------------------------------------------------------------

?>