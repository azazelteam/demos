<script type="text/javascript">
/* <![CDATA[ */

	//------------------------------------------------------------------------------------------------
	
	/*affectation auto des adresses au devis après modification des données*/
		
	function selectEstimateAddress( addressType, selectedAddress ){
	
		var primaryKey 	= getPrimaryKeyName( addressType );
		var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
		var checked 	= document.getElementById( addressType + '_checkbox' ).checked;
		
		var selectedAddress = 0;
		
		if( checked )
			selectedAddress = addresses.options[ addresses.selectedIndex ].value;
		
		$.ajax({
	 	
			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?select&type=" + addressType + "&id=" + selectedAddress + "&tablename=estimate&primarykey=<?php echo $Order->getId() ?>",
			async: false,
		 	success: function( response ){
		 		
				if( response == '0' )
					xhrAlert( 'Impossible de mettre à jour le devis' );
				
			}
	
		});
		
	}
	
	//------------------------------------------------------------------------------------------------
	
/*	]]> */
</script>
<?php 

/**
 * Affichage des adresses de livraison/facturation si différentes
 */	

$forwardingAddress 	= $Order->getForwardingAddress() instanceof ForwardingAddress ? $Order->getForwardingAddress() : false;
$invoiceAddress 	= $Order->getInvoiceAddress() instanceof InvoiceAddress ? $Order->getInvoiceAddress() : false;

?>
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
		<input type="hidden" name="updateAddresses" value="1" />
    	<?php anchor( "UpdateAdresses" ); ?>
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Adresses complémentaires</p>
				<div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
            </div>
            <?php
            
            	include_once( "$GLOBAL_START_PATH/objects/AddressEditor.php" );
            	
            	//@todo en attendant une gestion cohérente des adresses, on édite pas l'adresse du client par défaut
				
            	new AddressEditor( $Order->getValue( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_FORWARDING, $forwardingAddress, false, "float:left; width:50%; margin-top:15px;", "selectEstimateAddress" );
				new AddressEditor( $Order->getValue( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_INVOICE, $invoiceAddress, false, "float:right; width:50%; margin-top:15px;", "selectEstimateAddress" );
				
			?>
			<br style="clear:both;" />
		</div>
   	</div>
   	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div>