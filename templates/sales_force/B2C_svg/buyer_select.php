<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

?>		<div id="globalMainContent">
		<script language="javascript">
		<!--
		
			function goToPage( page ){
				
				<?php
				
				if( isset( $_GET[ "fieldname" ] ) && isset( $_GET[ "pre" ] ) ){
					
					$fieldname = $_GET[ "fieldname" ];
					$pre = $_GET[ "pre" ];
					
					$location = "buyer_select.php?$ScriptObject&fieldname=$fieldname&pre=$pre";
					
					if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "ord" ] ) )
						$location .= "&sortby=" . $_GET[ "sortby" ] . "&ord=" . $_GET[ "ord" ];
					
					?>
					document.location = '<?php echo $location ?>&page=' + page + '#Results';
					<?php
					
				}else{
					
					?>
					document.forms.search_form.elements[ 'page' ].value = page;
					document.forms.search_form.submit();
					<?php
					
				}
				
				?>
				
			}
			
			function sortby( sby, ord ){
				
				<?php
				
				if( isset( $_GET[ "fieldname" ] ) && isset( $_GET[ "pre" ] ) ){
					
					$fieldname = $_GET[ "fieldname" ];
					$pre = $_GET[ "pre" ];
					$page = isset( $_GET[ "page" ] ) ? intval( $_GET[ "page" ] ) : 1;
					
?>
					document.location = 'buyer_select.php?<?php echo $ScriptObject ?>&fieldname=<?php echo $fieldname ?>&pre=<?php echo $pre ?>&sortby=' + sby + '&ord=' + ord + '&page=1#Results';
<?php
					
				}else{
					
?>
					document.forms.search_form.elements[ 'sortby' ].value = sby;
					document.forms.search_form.elements[ 'ord' ].value = ord;
					
					document.forms.search_form.submit();
<?php
					
				}
				
?>
				
			}
			
			function hideScoring(){
				
				document.getElementById('scoring_title').style.visibility='hidden';
				document.getElementById('scoring_content').style.visibility='hidden';
				
			}
			
			function showScoring(){
				
				document.getElementById('scoring_title').style.visibility='visible';
				document.getElementById('scoring_content').style.visibility='visible';
				
			}
			
		//-->
		</script>
			<form name="search_form" action="<?php echo "$ScriptName?$ScriptObject" ?>#Results" method="post">
			<?php if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ ?><input type="hidden" name="VirtualEstimate" value="<?php echo intval( $_REQUEST[ "VirtualEstimate" ] ) ?>" /><?php } ?>
			<input type="hidden" name="SSESearch" value="1" />
			<input type="hidden" name="sortby" value="<?php if( isset( $_POST[ "sortby" ] ) ) echo $_POST[ "sortby" ]; ?>" />
			<input type="hidden" name="ord" value="<?php if( isset( $_POST[ "ord" ] ) ) echo $_POST[ "ord" ]; ?>" />
        	<div class="mainContent">
                <div class="topRight"></div><div class="topLeft"></div>
                <div class="content">
                	<input type="hidden" name="search" value="1" />
					<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
					<input type="hidden" name="EstimateToDelete" id="EstimateToDelete" value="0" />
					<input type="hidden" name="use_estimate" id="use_estimate" value="0" />
                	<div class="headTitle">
                        <p class="title">(B2C) Rechercher un client</p>
                        <div class="rightContainer">
		                	<!--
		                	<input type="radio" name="contact" onchange="hideScoring();" value="1" class="VCenteredWithText"<?php if( $contact == 1 ) echo " checked=\"checked\""; ?>> Prospects
							<input type="radio" name="contact" onchange="showScoring();" value="0" class="VCenteredWithText"<?php if( $contact == 0 ) echo " checked=\"checked\""; ?>> Clients
							<input type="radio" name="contact" onchange="hideScoring();" value="2" class="VCenteredWithText" checked <?php if( $contact == 2 ) echo " checked=\"checked\""; ?>> Prospects & clients
                        	-->
                        	<input type="hidden" name="contact" value="2"/>
                        </div>
                    </div>
                    <div class="subContent">
                     	<!-- tableau de recherche -->
                        <div class="tableContainer">
                            <table class="dataTable">
                                <tr class="filledRow">
                                    <th style="width:20%">N� client </th>
                                    <td style="width:30%"><input type="text" style="width: 60px;" name="idbuyer" class="textInput" value="<?php echo $idbuyer ?>" /></td>
                                	<th>Commercial</th>
                                    <td>
                                    <?php
										$hasAdminPrivileges = User::getInstance()->get( "admin" );
										if( $hasAdminPrivileges )
											XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "lastname", $iduser, $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
										else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
									?>
                                    </td>
                                	 <!--
                                	 <th>Nom / Raison sociale</th>
                                     <td><input type="text" name="company" class="textInput" value="<?php echo $company ?>" /></td>
                                    -->
                                </tr>
                                <tr>
                                	<th style="width:20%">Nom du contact</th>
                               		<td style="width:30%"><input type="text" name="lastname" class="textInput" value="<?php echo $lastname ?>" /></td>
                                                                   
                                    <th>Pr�nom</th>
                                    <td colspan="3"><input type="text" name="firstname" class="textInput" value="<?php echo $firstname ?>" /></td>
                                </tr>
                                <tr>
                                    <th>Email</th>
                                    <td><input type="text" name="email" class="textInput" value="<?php echo $email ?>" /></td>
                                    <th>Cr�� depuis</th>
                                    <td><input type="text" name="create_date_start" id="create_date_start" class="calendarInput" value="<?php echo $create_date_start ?>" /> <?php echo DHTMLCalendar::calendar( "create_date_start" ); ?></td>
                                
                                </tr>
                                <tr>
                                    <th>T�l�phone</th>
                                    <td colspan="3"><input type="text" name="phonenumber" class="textInput" value="<?php echo $phonenumber ?>" /></td>
                                    <!--
                                    <th>Fax</th>
                                    <td><input type="text" name="faxnumber" class="textInput" value="<?php echo $faxnumber ?>" /></td>
                               		-->
                                </tr>
                                <tr>
                                    <th>Code postal</th>
                                    <td><input type="text" name="zipcode" class="textInput" value="<?php echo $zipcode ?>" /></td>
                                    <th>Provenance du client</th>
                                    <td><?php Drawlift_idsource( $idsource ) ?></td>
                                </tr>
                                <!--<tr>
                                    <th>Commercial</th>
                                    <td colspan="3"><?php
											
											$hasAdminPrivileges = User::getInstance()->get( "admin" );
											
											if( $hasAdminPrivileges )
												XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "lastname", $iduser, $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
											else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
													
										?></td>
                                    
                                    <th>Code NAF</th>
                                    <td><input type="text" name="naf" class="textInput" value="<?php echo $naf ?>" /></td>
                                	
                                </tr>
                                
                                <tr>
                                    
                                    <th>Profil</th>
                                    <td><?php
											
											if( $hasAdminPrivileges )
												DrawLift_catright("catalog_right",$catalog_right);
											else
												echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
										?>
									</td>
									
                                    <th id="scoring_title" style="visibility:<?php echo $contact == 0 ? "visible" : "hidden" ?>;">Scoring</th>
                                    <td colspan="3" id="scoring_content" style="visibility:<?php echo $contact == 0 ? "visible" : "hidden" ?>;"><?php 
											
											if( $hasAdminPrivileges )
												DrawLift_scoring_buyer( $idscoring_buyer ); 
											else
												echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
											
										?></td>
                                </tr>
                                -->
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton" value="Rechercher" />
                        </div>
                    </div>
                </div>
                <div class="bottomRight"></div><div class="bottomLeft"></div>
            </div>
            <div id="tools" class="rightTools">
            	<div class="toolBox">
                	<div class="header">
                    	<div class="topRight"></div><div class="topLeft"></div>
                        <p class="title">Outils</p>
                    </div>
                    <div class="content"></div>
                    <div class="footer">
                    	<div class="bottomLeft"></div>
                        <div class="bottomMiddle"></div>
                        <div class="bottomRight"></div>
                    </div>
                </div>
                <div class="toolBox">
                	<div class="header">
                    	<div class="topRight"></div><div class="topLeft"></div>
                        <p class="title">Infos Plus</p>
                    </div>
                    <div class="content">
                    	
					</div>
					<div class="footer">
						<div class="bottomLeft"></div>
						<div class="bottomMiddle"></div>
						<div class="bottomRight"></div>
					</div>
				</div>
			</div>
<?php if( isset( $_POST[ "SSESearch" ] ) || isset( $_POST[ "SSESearch_x" ] ) ){ ?>
			<a name="Results"></a>
			<div class="mainContent fullWidthContent">
				<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
<?php if( count( $SSEResult ) > $maxSearchResults ){ ?>
						<p class="msg" style="text-align:center;">Plus de <?php echo $maxSearchResults ?> clients / prospects ont �t� trouv�s</p>
						<p class="msg" style="text-align:center;">Merci de bien vouloir affiner votre recherche</p>
<?php }elseif( !count( $SSEResult ) ){ ?>
					<p class="msg" style="text-align:center;">Aucun r�sultat ne correspond � votre recherche</p>
<?php }else{ ?>
					<div class="headTitle">
						<p class="title">R�sultats de la recherche : <?php $customersContactsCount = getCustomersCount(); echo $customersContactsCount[ 0 ]; ?>
						 <?php
                         	
                         	/*switch( $contact ){
								
								case 0:
									echo "clients";
									break;
									
								case 1:
									echo "prospects";
									break;
								
								case 2:
									echo "clients ou prospects";
									break;
								
							}*/
							
							?> 
							Client(s)
							<!-- / $customersContactsCount[ 1 ]  contacts--></p>
						<div class="rightContainer"></div>
					</div>
					<div class="subContent">
						<div class="tableHeaderLeft">
							<a href="#bottom" class="goUpOrDown">
								Aller en bas de page
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
							</a>
						</div>
						<div class="tableHeaderRight"></div>
						
						<!-- tableau r�sultats recherche -->
						<div class="resultTableContainer clear">
							<table class="dataTable resultTable">
								<thead>
									<tr>
<?php /*Contact principal ou pas. <th>Com</th> */ ?>
										<th>Com.</th>
										<th>Client n�</th>
										<th>D�p.</th>
										<th>Contact (nom)</th>
										<!--<th>Raison sociale</th>-->
										<th>Provenance</th>
										<!--
										<th>Type</th>
										<th>Scoring</th>
										<th>R�f�renc�</th>
										<th>Statut</th>
										-->
<?php
											
										if( $ScriptObject == "buyer" ){
												
?>
										<!--
										<th>Nombre de devis</th>
										-->
										<th>Nombre de commandes</th>
										<th>CA HT</th>
										<th>CA TTC</th>
<?php
												
										}
											
?>
										
<?php
											
											if( $ScriptObject == "estimate" ){
												
?>
										<!--
										<th>Nouveau devis</th>
										-->
<?php
												
											}else if( $ScriptObject == "order" ){
												
?>
										<th>Nouvelle commande</th>
<?php
												
											}
											
											//provenance = devis express
											if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){
												
?>
										<!--<th>Nouveau devis</th>-->
<?php
												
											}
											
?>
										<th>Coordonn�es</th>
										<th>Historique</th>
									</tr>
									<!-- petites fl�ches de tri -->
									<tr>
<?php
										/*<th class="noTopBorder">
										<a href="javascript:sortby(cont.idcontact,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:sortby(cont.idcontact,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>*/
										?>
										<th class="noTopBorder">
											<a href="javascript:sortby('u.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('u.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="javascript:sortby('b.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('b.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="javascript:sortby('b.zipcode','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('b.zipcode','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										
										<th class="noTopBorder">
											<!--
											<a href="javascript:sortby('b.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('b.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
											-->
										</th>
										
										<th class="noTopBorder">
											<a href="javascript:sortby('b.idsource','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('b.idsource','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<!--
										<th class="noTopBorder">
    										<a href="javascript:sortby('b.contact','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('b.contact','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="javascript:sortby('b.idscoring_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:sortby('b.idscoring_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
										-->
<?php
											
											if( $ScriptObject == "buyer" ){
												
?>
										<!--
										<th class="noTopBorder"></th>
										-->
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
<?php
												
											}
											
?>
										<th class="noTopBorder"></th>
<?php											
											
											if( $ScriptObject == "estimate" || $ScriptObject == "order" || isset( $_REQUEST[ "VirtualEstimate" ] ) ){
												
?>
										<!--
										<th class="noTopBorder"></th>
										-->
<?php
												
											}
?>
										<th class="noTopBorder"></th>
										<!--<th class="noTopBorder"></th>-->
									</tr>
								</thead>
								<tbody>
<?php
		
		for( $i = 0 ; $i < count( $SSEResult ); $i++ ){
			
			$rs = DBUtil::query( "SELECT idcontact FROM contact WHERE idbuyer = " . $SSEResult[ $i ][ "idbuyer" ] );
			$contactsCount = $rs->RecordCount();
			
			// Recherche de l'intitul� pour source
			$lang = User::getInstance()->getLang();
			$db = &DBUtil::getConnection();
			
			if( isset( $SSEResult[ $i ][ "idsource" ] ) && $SSEResult[ $i ][ "idsource" ] ) {
				
				$ssequerysource = $SSEResult[ $i ][ "idsource" ];
				$SSESourceQuery = "SELECT source$lang AS sourcename FROM source WHERE idsource = $ssequerysource LIMIT 1";
				$rssource = $db->Execute( $SSESourceQuery );
				$SSEResult[ $i ][ "source" ] = $rssource->RecordCount() == 0 ? "-" : $rssource->Fields( "sourcename" );
				
			}else{
				
				$SSEResult[ $i ][ "source" ] = "-";
				
			}
			
			//------------------------------------
		
			if( $SSEResult[ $i ][ "contact" ] == 1 )
				$Linkb = "<a href=\"contact_buyer.php?type=contact&key=" . $SSEResult[ $i ][ "idbuyer" ] . "#name-address\"><img src=\"$GLOBAL_START_URL/images/back_office/content/rightArrow.png\" alt=\"Voir\" /></a>";
			else
				$Linkb = "<a href=\"contact_buyer.php?type=buyer&key=" . $SSEResult[ $i ][ "idbuyer" ]. "#name-address\"><img src=\"$GLOBAL_START_URL/images/back_office/content/rightArrow.png\" alt=\"Voir\" /></a>";
	
			if( $ScriptObject == "estimate" ){
				
				$Link = "<a href=\"com_admin_devis.php?idbuyer=" . $SSEResult[ $i ][ "idbuyer" ] ."&idcontact=" . $SSEResult[ $i ][ "idcontact" ] . "\">";
				$Link .= "<img src=\"$GLOBAL_START_URL/www/icones/ajout_devis.gif\" alt=\"Cr�er un devis\" /></a>";
				
			}else if( $ScriptObject == "order" ){
				
				$Link = "<a href=\"com_admin_order.php?idbuyer=" . $SSEResult[ $i ][ "idbuyer" ] ."&idcontact=" . $SSEResult[ $i ][ "idcontact" ] . "\">";
				$Link .= "<img src=\"$GLOBAL_START_URL/images/back_office/content/rightArrow.png\" alt=\"Cr�er devis / commande\"></a>";
				
			}
			
					/*
				<td>
					
					if( $SSEResult[ $i ][ "idcontact" ] > 0 )
						echo "Co";
					else
						echo "Cl";
					
				</td>
					*/

?>
									<tr class="blackText">
										<td class="lefterCol" rowspan="<?php echo $contactsCount ?>"><?php echo $SSEResult[ $i ][ "initial" ] ?></td>
										<td rowspan="<?php echo $contactsCount ?>"><span style="font-weight:bold;font-size:11px;"><?php
			
			if( isset( $SSEResult[ $i ][ "iderp" ] ) )
				echo $SSEResult[ $i ][ "iderp" ] . "/" . $SSEResult[ $i ][ "idbuyer" ];
			else
				echo $SSEResult[ $i ][ "idbuyer" ];
			
			$isContact = $SSEResult[ $i ][ "contact" ] == 1;
			
?></span></td>
										<td rowspan="<?php echo $contactsCount ?>"><?php echo substr( $SSEResult[ $i ][ "zipcode" ], 0, 2 ) ?></td>
										<td class="lefterCol righterCol"> <span style="font-size:11px;font-weight:bold;"><?php echo $SSEResult[ $i ][ "lastname" ] . " " . $SSEResult[ $i ][ "firstname" ] ?></span></td>
										<!--<td rowspan="<?php echo $contactsCount ?>"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&type=<?php echo $isContact ? "contact" : "buyer" ?>"><?php echo $SSEResult[ $i ][ "company" ] ?></a></td>-->
										<td rowspan="<?php echo $contactsCount ?>"><?php echo $SSEResult[ $i ][ "source" ] ?></td>
										<!--<td rowspan="<?php echo $contactsCount ?>"><?php echo $SSEResult[ $i ][ "contact" ] ? Dictionnary::translate( "prospect" ) : Dictionnary::translate( "buyer" ) ?></td>
										<td rowspan="<?php echo $contactsCount ?>"><?php
			
													if( $SSEResult[ $i ][ "idscoring_buyer" ] == 0 )
														echo "-";
													else
														echo $SSEResult[ $i ][ "idscoring_buyer" ];
													
										?>
										</td>
										<td rowspan="<?php echo $contactsCount ?>">
										<?php
			
													if( isset( $_POST[ "catalog_right" ] ) ){
														
														if( $SSEResult[ $i ][ "buyer_ref_us" ] == 1 )
															echo Dictionnary::translate( "yes" );
														else
															echo Dictionnary::translate( "no" );
														
													}
													
										?>
										</td>
										<td rowspan="<?php echo $contactsCount ?>"><?php
			
										if( $SSEResult[ $i ][ "catalog_right" ] >= 0 ){
											
											$idimgtoinclude = $SSEResult[ $i ][ "catalog_right" ];
											$reqett = "SELECT photo, catalog_right$lang FROM catalog_right WHERE idcatalog_right = $idimgtoinclude";
											$rsimg = $db->Execute( $reqett );
											$image = $rsimg->Fields( "photo" );
											
											if( $image == "" )
												echo $rsimg->Fields( "catalog_right$lang" );
											else
												echo "<img src=\"$GLOBAL_START_URL$image\" border=\"0\" width=\"30\">";
											
										}else{
											
											echo "-";
											
										}
										
											?>
										</td>-->
<?php
			
			if( $ScriptObject == "buyer" ){
				
?>
										<!--
										<td rowspan="<?php echo $contactsCount ?>"><?php echo getEstimateCount( $SSEResult[$i][ "idbuyer" ] ) ?></td>
										-->
										<td rowspan="<?php echo $contactsCount ?>"><?php echo getInvoiceCount( $SSEResult[$i][ "idbuyer" ] ) ?></td>
										<td rowspan="<?php echo $contactsCount ?>" style="white-space:nowrap;"><?php echo Util::priceFormat( getSalesTurnoverWT( $SSEResult[$i][ "idbuyer" ] ) ) ?></td>
										<td rowspan="<?php echo $contactsCount ?>" style="white-space:nowrap;"><?php echo Util::priceFormat( getSalesTurnoverATI( $SSEResult[$i][ "idbuyer" ] ) ) ?></td><?php
										
			}
			
?>
										 <!--<td class="lefterCol righterCol"><?php echo $SSEResult[ $i ][ "lastname" ] . " " . $SSEResult[ $i ][ "firstname" ] ?></td>-->
<?php
						
			if( $ScriptObject == "estimate" ){
				
?>										
										<!--
										<td class="lefterCol righterCol">
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&idcontact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>">
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Cr�er un devis" />
											</a>
										</td>
										-->
<?php
				
			}
			
			if( $ScriptObject == "order" ){
				
?>
										<td class="lefterCol righterCol">
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&idcontact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>">
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Cr�er une commande" />
											</a>
										</td>
<?php
				
			}
			
			//provenance = devis express
			
			if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){
				
?>
										<!--
										<td class="lefterCol righterCol">
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?ReassignBuyer&amp;RealBuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&amp;IdContact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>&amp;IdEstimate=<?php echo intval( $_REQUEST[ "VirtualEstimate" ] ) ?>">
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Cr�er un devis" />
											</a>
										</td>
										-->
										
										<?php
				
			}
			
?>
				
										<td rowspan="<?php echo $contactsCount ?>"><?php echo $Linkb ?></td>
										<td class="righterCol" rowspan="<?php echo $contactsCount ?>"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $SSEResult[ $i ][ "contact" ] == 1 ? "contact" : "buyer" ?>&amp;key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>#history" onClick="seeBuyerHisto(<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Voir" /></a></td>
									</tr>
<?php
			$rs = DBUtil::query( "SELECT firstname, lastname, idbuyer, idcontact FROM contact WHERE idbuyer = " . $SSEResult[$i][ "idbuyer" ] . " AND idcontact != 0 ORDER BY idcontact" );
			
			while( !$rs->EOF ){
				
?>
									<tr>
										<td class="lefterCol righterCol"><?php echo $rs->fields( "lastname" ) . " " . $rs->fields( "firstname" ) ?></td>
<?php
			
			if( $ScriptObject == "estimate" ){
				
?>
										<!--<td class="lefterCol righterCol">
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $rs->fields( "idbuyer" ) ?>&idcontact=<?php echo $rs->fields( "idcontact" ) ?>">
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Cr�er un devis" />
											</a>
										</td>-->
<?php
			
			}
			
			if( $ScriptObject == "order" ){
				
?>
										<td class="lefterCol righterCol">
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $rs->fields( "idbuyer" ) ?>&idcontact=<?php echo $rs->fields( "idcontact" ) ?>">
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Cr�er une commande" />
											</a>
										</td>
<?php
			
			}
			
			if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){
				
?>
										<!--
										<td class="lefterCol righterCol">
											<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?ReassignBuyer&amp;RealBuyer=<?php echo $rs->fields( "idbuyer" ) ?>&amp;IdContact=<?php echo $rs->fields( "idcontact" ) ?>&amp;IdEstimate=<?php echo intval( $_REQUEST[ "VirtualEstimate" ] ) ?>">
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Cr�er un devis" />
											</a>
										</td>
										-->
<?php
			
			}
				
?>
									</tr>
<?php
					
				$rs->MoveNext();
				
			}
			
		}
		
?>
								</tbody>
							</table>
<?php
				
		//if( isset( $_POST[ "SSESearch" ] ) || isset( $_POST[ "SSESearch_x" ] ) || isset( $_GET[ "pre" ] ) )
			//paginate();
		
?>
                            <a href="#top" class="goUpOrDown">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
						</div>
					</div>
<?php
			
		}
		
?>
				</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
<?php
				
	}
	
?>
		<a name="bottom"></a>
	</form>
</div>