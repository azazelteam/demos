<?php include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );?>		

<script language="javascript">
<!--

	function goToPage( page ){
		
		<?php if( isset( $_GET[ "fieldname" ] ) && isset( $_GET[ "pre" ] ) ){
			
			$fieldname = $_GET[ "fieldname" ];
			$pre = $_GET[ "pre" ];
			
			$location = "buyer_select.php?$ScriptObject&fieldname=$fieldname&pre=$pre";
			
			if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "ord" ] ) )
				$location .= "&sortby=" . $_GET[ "sortby" ] . "&ord=" . $_GET[ "ord" ];
			
		?>
			document.location = '<?php echo $location ?>&page=' + page + '#Results';
		<?php }else{ ?>
			document.forms.search_form.elements[ 'page' ].value = page;
			document.forms.search_form.submit();
		<?php } ?>
		
	}
	
	function sortby( sby, ord ){
		
		<?php if( isset( $_GET[ "fieldname" ] ) && isset( $_GET[ "pre" ] ) ){
			
			$fieldname = $_GET[ "fieldname" ];
			$pre = $_GET[ "pre" ];
			$page = isset( $_GET[ "page" ] ) ? intval( $_GET[ "page" ] ) : 1;
					
		?>
			document.location = 'buyer_select.php?<?php echo $ScriptObject ?>&fieldname=<?php echo $fieldname ?>&pre=<?php echo $pre ?>&sortby=' + sby + '&ord=' + ord + '&page=1#Results';
		<?php }else{ ?>
			document.forms.search_form.elements[ 'sortby' ].value = sby;
			document.forms.search_form.elements[ 'ord' ].value = ord;
			document.forms.search_form.submit();
		<?php } ?>
		
	}
	
	function hideScoring(){
		document.getElementById('scoring_title').style.visibility='hidden';
		document.getElementById('scoring_content').style.visibility='hidden';
	}
	
	function showScoring(){
		document.getElementById('scoring_title').style.visibility='visible';
		document.getElementById('scoring_content').style.visibility='visible';
	}
	
//-->
</script>
		

		<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/buyer_select.php</span>
<?php } ?>
		
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/queryLoader.css"  type="text/css" />
<script type='text/javascript' src='<?php echo $GLOBAL_START_URL ?>/js/queryLoader.js'></script>

<form name="search_form" action="/sales_force/buyer_select.php?<?php echo $ScriptObject; ?>#Results" method="post">
<?php if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ ?><input type="hidden" name="VirtualEstimate" value="<?php echo  $_REQUEST[ "VirtualEstimate" ]  ?>" /><?php } ?>
<?php if( isset( $_REQUEST[ "VirtualOrder" ] ) ){ ?><input type="hidden" name="VirtualOrder" value="<?php echo  $_REQUEST[ "VirtualOrder" ]  ?>" /><?php } ?>
<input type="hidden" name="SSESearch" value="1" />
<input type="hidden" name="sortby" value="<?php if( isset( $_POST[ "sortby" ] ) ) echo $_POST[ "sortby" ]; ?>" />
<input type="hidden" name="ord" value="<?php if( isset( $_POST[ "ord" ] ) ) echo $_POST[ "ord" ]; ?>" />

<input type="hidden" name="search" value="1" />
<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
<input type="hidden" name="EstimateToDelete" id="EstimateToDelete" value="0" />
<input type="hidden" name="use_estimate" id="use_estimate" value="0" />		

<div class="centerMax">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div id="tools" class="rightTools">
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
	Accès rapide</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
	Quelques chiffres...</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
</div>	
<div class="contentDyn">
	
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Rechercher un prospect ou un client</span>
	<span class="selectDate">
		<input type="radio" name="contact" onchange="hideScoring();" value="1" class="VCenteredWithText"<?php if( $contact == 1 ) echo " checked=\"checked\""; ?>> Prospects
		<input type="radio" name="contact" onchange="showScoring();" value="0" class="VCenteredWithText"<?php if( $contact == 0 ) echo " checked=\"checked\""; ?>> Clients
		<input type="radio" name="contact" onchange="hideScoring();" value="2" class="VCenteredWithText"<?php if( $contact == 2 ) echo " checked=\"checked\""; ?>> Prospects & clients
    </span>
	<div class="spacer"></div>
	</h1>
					
					
	<div class="blocSearch"><div style="margin:5px;">
	
		<!-- Left -->
		<div class="blocMultiple blocMLeft">
			<label><span><strong>N° client</strong></span></label>
			<input type="text" name="idbuyer" class="textInput" value="<?php echo $idbuyer ?>" />
			<div class="spacer"></div>
			
			<label><span>Nom / Raison sociale</span></label>
			<input type="text" name="company" class="textInput" value="<?php echo $company ?>" />
			<div class="spacer"></div>
			
			<label><span>Email</span></label>
			<input type="text" name="email" class="textInput" value="<?php echo $email ?>" />
			<div class="spacer"></div>
			
			<label><span>Téléphone</span></label>
			<input type="text" name="phonenumber" class="textInput" value="<?php echo $phonenumber ?>" />
			<div class="spacer"></div>
			
			<label><span>Code postal</span></label>
			<input type="text" name="zipcode" class="textInput" value="<?php echo $zipcode ?>" />
			<div class="spacer"></div>
			
			<label><span>Commercial</span></label>
			<?php 
				$hasAdminPrivileges = User::getInstance()->get( "admin" );
				if( $hasAdminPrivileges ){
					
					include_once( dirname( __FILE__ ) . "/../../script/user_list.php" );
					userList( true, $iduser );
					
					//XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "firstname", $iduser, $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
					
				}
				else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";		
			?>
			<div class="spacer"></div>
			
			<label><span>Profil</span></label>
			<?php XHTMLFactory::createSelectElement( "customer_profile", "idcustomer_profile", "name", "name", isset( $_POST[ "idcustomer_profile" ] ) ? intval( $_POST[ "idcustomer_profile" ] ) : 0, "", "Tous", "name=\"idcustomer_profile\"" ); ?>
			<div class="spacer"></div>
			
			<label><span>Siret</span></label>
			<input class="textInput" type="text" name="siret" value="<?php echo $siret ?>" size="14" />
			<div class="spacer"></div>
		</div>
		
		<!-- Right -->
		<div class="blocMultiple blocMRight">		
			<label><span><strong>Nom du contact</strong></span></label>
			<input type="text" name="lastname" class="textInput" value="<?php echo $lastname ?>" />  
			<div class="spacer"></div>
			
			<label><span>Prénom</span></label>
			<input type="text" name="firstname" class="textInput" value="<?php echo $firstname ?>" />
			<div class="spacer"></div>
			
			<label><span>Créé entre le</span></label>
			<input type="text" name="create_date_start" id="create_date_start" class="calendarInput" value="<?php echo $create_date_start ?>" /> <?php echo DHTMLCalendar::calendar( "create_date_start" ); ?>
			<label class="smallLabel"><span>et le</span></label>
			<input type="text" name="create_date_end" id="create_date_end" class="calendarInput" value="<?php echo $create_date_end ?>" /> <?php echo DHTMLCalendar::calendar( "create_date_end" ); ?>
			<div class="spacer"></div>
			
			<label><span>Fax</span></label>
			<input type="text" name="faxnumber" class="textInput" value="<?php echo $faxnumber ?>" />
			<div class="spacer"></div>
			
			<?php $akilae_source = DBUtil::getParameterAdmin("akilae_source");
			if($akilae_source==1){
			?>

			<label><span>Provenance du client</span></label>
			<?php Drawlift_idsource( $idsource ) ?>
			<div class="spacer"></div>
			
			<?php } ?>
			
			<label><span>Code NAF</span></label>
			<input type="text" name="naf" class="textInput" value="<?php echo $naf ?>" />
			<div class="spacer"></div>
			
			<?php if($contact != 0) { ?>
			<label id="scoring_title"><span>Scoring</span></label>
			<span id="scoring_content">
			<?php 
				if( $hasAdminPrivileges )
					DrawLift_scoring_buyer( $idscoring_buyer ); 
				else
					echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
			?></span>
			<?php } ?>
			<?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping"); ?>
			
			<?php if($akilae_grouping==1){ ?>
			<div class="spacer"></div>
			
			<label><span>Groupement</span></label>
			<?php XHTMLFactory::createSelectElement( "grouping", "idgrouping", "grouping", "grouping", isset( $_POST[ "idgrouping" ] ) ? $_POST[ "idgrouping" ] : false, "", "-", "name=\"idgrouping\"" ); ?>
			<?php } ?>
			
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
        
        <input type="submit" class="inputSearch" value="Rechercher" />    
        <div class="spacer"></div>     
		
	</div></div>
	<div class="spacer"></div>

</form>
	
</div>       		      	
<div class="spacer"></div>       

<?php 
	//flush(); 
?>
<div class="contentResult" id="ResultBuyer" >
	<?php if( isset( $_POST[ "SSESearch" ] ) || isset( $_POST[ "SSESearch_x" ] ) ){ ?>
		<a name="top"></a><a name="Results"></a>
				
		<?php if( count( $SSEResult ) > $maxSearchResults ){ ?>
			<p class="msg" style="text-align:center;">Plus de <?php echo $maxSearchResults ?> clients / prospects ont été trouvés</p>
			<p class="msg" style="text-align:center;">Merci de bien vouloir affiner votre recherche</p>
		<?php }elseif( !count( $SSEResult ) ){ ?>
			<p class="msg" style="text-align:center;">Aucun résultat ne correspond à votre recherche</p>
		<?php }else{ 
	
	
	
			/******************************************
			 * Résultat de la recherche
			 ******************************************/
			//ob_start();
			$customersContactsCount = getCustomersCount();
			?>
		
			<h1 class="titleSearch">
				<span class="textTitle" id="caSearch">Résultats de la recherche : <?php //$customersContactsCount = getCustomersCount(); echo $customersContactsCount[ 0 ]; ?> 
				<?php
		         	
		         	/*switch( $contact ){
						
						case 0:
							echo "clients";
							break;
							
						case 1:
							echo "prospects";
							break;
						
						case 2:
							echo "clients ou prospects";
							break;
						
					}*/
					
				?><?php echo $customersContactsCount[ 0 ] ?> contacts - CA </span>
				<span class="selectDate">
					<a href="#bottom" class="goUpOrDown" style="font-size:10px; color:#000000;">
		            	Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a>
				</span>
				<div class="spacer"></div>
			</h1>
			
			
			<div class="blocResult mainContent">
			<div class="content">
				<!-- tableau résultats recherche -->
				
				
				<table class="dataTable resultTable">
					<thead>
						<tr>
							<?php /*Contact principal ou pas. <th>Com</th> */ ?>
							<th>Com.</th>
							<th>Client n°</th>
							<th>Dép.</th>
							<th>Raison sociale</th>
							<th>Provenance</th>
							<th>Type</th>
							<th>Scoring</th>
							<th>Profil</th>
							<th>Suivi</th>
							<?php if( $ScriptObject == "buyer" ){ ?>
								<th>Nombre de devis</th>
								<th>Nombre de commandes</th>
								<th>CA HT</th>
							<?php } ?>
							<th>Contact (nom)</th>
							<?php if( $ScriptObject == "estimate" ){ ?>
							<th>
							<?php 
								if( isset( $_REQUEST[ "VirtualEstimate" ] ) )
									echo "Assigner devis";
								else if( isset( $_REQUEST[ "VirtualOrder" ] ) )
									echo "Assigner commande";
								else echo "Nouveau devis";
							?>
							</th>
							<?php } else if( $ScriptObject == "order" ){ ?>
							<th>Nouvelle commande</th>
							<?php }
								
								//provenance = devis express
								if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){
									
									?>
									<th><?php echo "Attribuer devis"; ?></th>
									<?php
									
								}
								else if( isset( $_REQUEST[ "VirtualOrder" ] ) ){
									
									?>
									<th><?php echo "Attribuer commande"; ?></th>
									<?php
									
								}
							?>
							<th>Coordonnées</th>
							<th>Historique</th>
						</tr>
						<!-- petites flèches de tri -->
						<tr>
							<th class="noTopBorder">
								<a href="javascript:sortby('u.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('u.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="javascript:sortby('b.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('b.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="javascript:sortby('b.zipcode','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('b.zipcode','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="javascript:sortby('b.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('b.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="javascript:sortby('b.idsource','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('b.idsource','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="javascript:sortby('b.contact','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('b.contact','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="javascript:sortby('b.idscoring_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="javascript:sortby('b.idscoring_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder"></th>
							<th class="noTopBorder"></th>
							<?php if( $ScriptObject == "buyer" ){ ?>
								<th class="noTopBorder"></th>
								<th class="noTopBorder"></th>
								<th class="noTopBorder"></th>
							<?php } ?>
							<th class="noTopBorder"></th>
							<?php											
								if( $ScriptObject == "estimate" 
									|| $ScriptObject == "order" 
									|| isset( $_REQUEST[ "VirtualEstimate" ] ) 
									|| isset( $_REQUEST[ "VirtualOrder" ] ) ){	
							?>
							<th class="noTopBorder"></th>
							<?php } ?>
							<th class="noTopBorder"></th>
							<th class="noTopBorder"></th>
						</tr>
					</thead>
					<tbody>
					<?php	
					
					$salesTurnoverWTSum = 0.0;
					$estimateCountSum = 0.0;
					$orderCountSum = 0.0;
					
					for( $i = 0 ; $i < count( $SSEResult ); $i++ ){
					// ------- Mise à jour de l'id gestion des Index  #1161
					$rs = DBUtil::query( "SELECT idcontact FROM contact WHERE idbuyer = '" . $SSEResult[ $i ][ "idbuyer" ]."'" );
					$contactsCount = $rs->RecordCount();
					
					// ------- Mise à jour de l'id gestion des Index  #1161
					$rss = DBUtil::query( "SELECT *  FROM contact_follow WHERE idbuyer = '" . $SSEResult[ $i ][ "idbuyer" ]."'" );
					$commentsCount = $rss->RecordCount();
					
					
					$genderRs = DBUtil::query( "SELECT title_1 FROM title WHERE idtitle = " . $SSEResult[ $i ][ "title" ] );
					$titleContact = $genderRs->Fields( "title_1" );
					
					/*
					$scoring__buyerRs = DBUtil::query( "SELECT txtscoring_buyer FROM scoring_buyer WHERE idscoring_buyer = " . $SSEResult[ $i ][ "scoring_buyer" ] );
					$titleScoring_buyer = $scoring_buyerRs->Fields( "txtscoring_buyer" );
					*/
					
					// Recherche de l'intitulé pour source
					$lang = User::getInstance()->getLang();
					$db = &DBUtil::getConnection();
					$idbuyer = $SSEResult[ $i ][ "idbuyer" ];
					if( isset( $SSEResult[ $i ][ "idsource" ] ) && $SSEResult[ $i ][ "idsource" ] ) {
						
						$ssequerysource = $SSEResult[ $i ][ "idsource" ];
						$SSESourceQuery = "SELECT source$lang AS sourcename FROM source WHERE idsource = $ssequerysource LIMIT 1";
						$rssource = $db->Execute( $SSESourceQuery );
						$SSEResult[ $i ][ "source" ] = $rssource->RecordCount() == 0 ? "-" : $rssource->Fields( "sourcename" );
						
					}else{
						
						$SSEResult[ $i ][ "source" ] = "-";
					}
					
														
					//------------------------------------
				
					if( $SSEResult[ $i ][ "contact" ] == 1 )
						$Linkb = "<a href=\"contact_buyer.php?key=" . $SSEResult[ $i ][ "idbuyer" ] . "#name-address\" target = \"_blank\"><img src=\"$GLOBAL_START_URL/images/back_office/content/rightArrow.png\" alt=\"Voir\" /></a>";
					else
						$Linkb = "<a href=\"contact_buyer.php?key=" . $SSEResult[ $i ][ "idbuyer" ]. "#name-address\" target = \"_blank\"><img src=\"$GLOBAL_START_URL/images/back_office/content/rightArrow.png\" alt=\"Voir\" /></a>";
			
					if( $ScriptObject == "estimate" ){
						
						$Link = "<a href=\"com_admin_devis.php?idbuyer=" . $SSEResult[ $i ][ "idbuyer" ] ."&amp;idcontact=" . $SSEResult[ $i ][ "idcontact" ] . "\">";
						$Link .= "<img src=\"$GLOBAL_START_URL/www/icones/ajout_devis.gif\" alt=\"Créer un devis\" /></a>";
						
					}else if( $ScriptObject == "order" ){
						
						$Link = "<a href=\"com_admin_order.php?idbuyer=" . $SSEResult[ $i ][ "idbuyer" ] ."&amp;idcontact=" . $SSEResult[ $i ][ "idcontact" ] . "\">";
						$Link .= "<img src=\"$GLOBAL_START_URL/images/back_office/content/rightArrow.png\" alt=\"Créer devis / commande\"></a>";
						
					}
								
					$isContact = $SSEResult[ $i ][ "contact" ] == 1;
		
					?>
						<tr class="blackText">
							<td class="lefterCol" rowspan="<?php echo $contactsCount ?>"><?php echo $SSEResult[ $i ][ "initial" ] ?></td>
							<td rowspan="<?php echo $contactsCount ?>"><a style="font-weight:bold; font-size:11px;" target="_blank" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>"><?php
					
					if( isset( $SSEResult[ $i ][ "iderp" ] ) )
						echo $SSEResult[ $i ][ "iderp" ] . "/" . $SSEResult[ $i ][ "idbuyer" ];
					else
						echo $SSEResult[ $i ][ "idbuyer" ];
					
					?></a></td>
						<td rowspan="<?php echo $contactsCount ?>"><?php echo substr( $SSEResult[ $i ][ "zipcode" ], 0, 2 ) ?></td>
						<td rowspan="<?php echo $contactsCount ?>"><a style="font-weight:bold; font-size:11px;" target="_blank" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>"><?php if(!empty($SSEResult[ $i ][ "company" ]) && $SSEResult[ $i ][ "company" ] != "" && $SSEResult[ $i ][ "company" ] != " "){ echo $SSEResult[ $i ][ "company" ] ; }else{ echo $titleContact.' '.$SSEResult[ $i ][ "lastname" ] ; }?></a>
							
						
						</td>
						<td rowspan="<?php echo $contactsCount ?>"><?php echo $SSEResult[ $i ][ "source" ] ?></td>
						<td rowspan="<?php echo $contactsCount ?>"><?php echo $SSEResult[ $i ][ "contact" ] ? Dictionnary::translate( "prospect" ) : Dictionnary::translate( "buyer" ) ?></td>
						<td rowspan="<?php echo $contactsCount ?>">
						<?php echo $titleScoring_buyer ?>
						
						<?php
					
					if( $SSEResult[ $i ][ "idscoring_buyer" ] == 0 )
						echo "-";
					else
						echo $SSEResult[ $i ][ "idscoring_buyer" ];
					
					?></td>
					<td rowspan="<?php echo $contactsCount ?>"><?php
					
					if( isset( $_POST[ "idcustomer_profile" ] ) ){
						
						if( $SSEResult[ $i ][ "buyer_ref_us" ] == 1 )
							echo Dictionnary::translate( "yes" );
						else
							echo Dictionnary::translate( "no" );
						
					}
					
					?></td>
					<td rowspan="<?php echo $contactsCount ?>"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $SSEResult[ $i ][ "contact" ] == 1 ? "contact" : "buyer" ?>&amp;key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>#suivi" ><?php if( $commentsCount != 0 ){ ?><?php echo $commentsCount ?></a><?php echo GenerateHTMLForToolTipBox( $idbuyer ) ?><?php }?></td>
					
					<?php if( $ScriptObject == "buyer" ){ ?>
						<td rowspan="<?php echo $contactsCount ?>"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>#history" 
						target="_blank"><?php $estimateCountSum += $estimateCount = getEstimateCount( $SSEResult[$i][ "idbuyer" ] ); echo $estimateCount; ?></a></td>
						<td rowspan="<?php echo $contactsCount ?>"><?php $orderCountSum += $orderCount = getOrderCount( $SSEResult[$i][ "idbuyer" ] ); echo $orderCount; ?></td>
						<td rowspan="<?php echo $contactsCount ?>" style="white-space:nowrap;"><?php $salesTurnoverWTSum += $salesTurnoverWT = getSalesTurnoverWT( $SSEResult[$i][ "idbuyer" ] ); echo Util::priceFormat( $salesTurnoverWT ); ?></td>
					<?php } ?>
					
						<td class="lefterCol righterCol"><?php echo strtoupper( $SSEResult[ $i ][ "lastname" ] ) . " " . $SSEResult[ $i ][ "firstname" ] ?></td>
					
					<?php if( $ScriptObject == "estimate" ){ ?>
						<td class="lefterCol righterCol">
							<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&idcontact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer un devis" />
							</a>
						</td>
					<?php } if( $ScriptObject == "order" ){ ?>
						<td class="lefterCol righterCol">
							<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&idcontact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer une commande" />
							</a>
						</td>
					<?php } if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ ?>
						<td class="lefterCol righterCol">
							<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?ReassignBuyer&amp;RealBuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&amp;IdContact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>&amp;IdEstimate=<?php echo  $_REQUEST[ "VirtualEstimate" ]  ?>">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer un devis" />
							</a>
						</td>
					<?php } else if( isset( $_REQUEST[ "VirtualOrder" ] ) ){ ?>
						<td class="lefterCol righterCol">
							<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?ReassignBuyer&amp;RealBuyer=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>&amp;IdContact=<?php echo $SSEResult[ $i ][ "idcontact" ] ?>&amp;IdOrder=<?php echo  $_REQUEST[ "VirtualOrder" ] ?>">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer une commande" />
							</a>
						</td>
					<?php } ?>
						
						<td rowspan="<?php echo $contactsCount ?>"><?php echo $Linkb ?></td>
						<td class="righterCol" rowspan="<?php echo $contactsCount ?>"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $SSEResult[ $i ][ "contact" ] == 1 ? "contact" : "buyer" ?>&amp;key=<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>#history" onClick="seeBuyerHisto(<?php echo $SSEResult[ $i ][ "idbuyer" ] ?>); return false;" target="_blank" ><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Voir" /></a></td>
					</tr>
					
					<?php
					// ------- Mise à jour de l'id gestion des Index  #1161
					$rs = DBUtil::query( "SELECT firstname, lastname, idbuyer, idcontact FROM contact WHERE idbuyer = '" . $SSEResult[$i][ "idbuyer" ] . "' AND idcontact != 0 ORDER BY idcontact" );
					while( !$rs->EOF ){ ?>
					
					<tr>
						<td class="lefterCol righterCol"><?php echo $rs->fields( "lastname" ) . " " . $rs->fields( "firstname" ) ?></td>
		
					<?php if( $ScriptObject == "estimate" ){ ?>
							<td class="lefterCol righterCol">
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $rs->fields( "idbuyer" ) ?>&idcontact=<?php echo $rs->fields( "idcontact" ) ?>">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer un devis" />
								</a>
							</td>
					<?php } if( $ScriptObject == "order" ){ ?>
							<td class="lefterCol righterCol">
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $rs->fields( "idbuyer" ) ?>&idcontact=<?php echo $rs->fields( "idcontact" ) ?>">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer une commande" />
								</a>
							</td>
					<?php } if( isset( $_REQUEST[ "VirtualEstimate" ] ) ){ ?>
							<td class="lefterCol righterCol">
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?ReassignBuyer&amp;RealBuyer=<?php echo $rs->fields( "idbuyer" ) ?>&amp;IdContact=<?php echo $rs->fields( "idcontact" ) ?>&amp;IdEstimate=<?php echo  $_REQUEST[ "VirtualEstimate" ] ?>">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer un devis" />
								</a>
							</td>
					<?php } else if( isset( $_REQUEST[ "VirtualOrder" ] ) ){ ?>
							<td class="lefterCol righterCol">
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?ReassignBuyer&amp;RealBuyer=<?php echo $rs->fields( "idbuyer" ) ?>&amp;IdContact=<?php echo $rs->fields( "idcontact" ) ?>&amp;IdOrder=<?php echo  $_REQUEST[ "VirtualOrder" ] ?>">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrowOrange.gif" style="width:20px;" alt="Créer une commande" />
								</a>
							</td>
					<?php } ?>
					
					</tr>
				
				<?php $rs->MoveNext(); }
				
					
					
				} 
				
				if( $ScriptObject == "buyer" ){
	
					?>
					<tr>
						<td class="lefterCol" colspan="9"></td>
						<td style="white-space:nowrap;"><b><?php echo $estimateCountSum; ?></b></td>
						<td style="white-space:nowrap;"><b><?php echo $orderCountSum; ?></b></td>
						<td style="white-space:nowrap;" id="totalHTSearch"><b><?php echo Util::priceFormat( $salesTurnoverWTSum ); ?></b></td>
						<td class="righterCol" colspan="3"></td>
					</tr>
					<?php
					
				}
				?>
				</tbody>
				</table>  
			</div>
			</div>                
	        <div class="spacer"></div>
	           
	        <h1 class="titleSearch">
			<span class="textTitle">&nbsp;</span>
			<span class="selectDate">
				<a href="#top" class="goUpOrDown" style="font-size:10px; color:#000000;">
	            	Aller en haut de page
	                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
	            </a>
			</span>
			<div class="spacer"></div>
			</h1>	
		
			
			
			<?php
			//ob_end_flush();
	
			/******************************************
			* Fin Résultat de la recherche
			*******************************************/
	
		} ?>
	<?php } ?>
</div>

<div class="spacer"></div><br/>
<a name="bottom"></a>	
</div><!-- centerMax -->
</form>

<script type='text/javascript'>
	//QueryLoader.init();
	
	document.getElementById('caSearch').innerHTML = document.getElementById('caSearch').innerHTML + document.getElementById('totalHTSearch').innerHTML ; 
	
	
</script>
 
 <?php
/***************************************************************************************************************************************/
function GenerateHTMLForToolTipBox( $idbuyer ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT *
	FROM contact_follow cf
	WHERE cf.idbuyer =  '$idbuyer'  
	";

	$rs = $db->Execute( $query );
	if( $rs === false )
		die( "Impossible de récupérer les commentaires pour le client '$idbuyer'" );
		
	
	$idbuyer = $rs->fields( "idbuyer" );

	//TODO : jointure entre order_supplier_row et order_row approximative
	
	$html = "<div onmouseover=\"document.getElementById('pop$idbuyer').style.display='block';\" onmouseout=\"document.getElementById('pop$idbuyer').style.display='none';\" ><img src=\"$GLOBAL_START_URL/images/back_office/content/icons-warning.png\" alt=\"voir le détail\" class=\"tooltipa VCenteredWithText\"  style=\"position:relative;  \"/>";
	
	$html .= "<span id='pop$idbuyer' class='tooltip'>
<div class=\"content\" style=\"width:auto;height:300px ;position:absolute; top:-1px; left:-1px; overflow:scroll; padding:5px; background:#fff; border:1px solid #000;margin-left:50px;margin-top:25px; \">
	<div class=\"headTitle\">
		<p class=\"title\">Client n° $idbuyer </p>
		<div class=\"rightContainer\"></div>
	</div>
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	if( $rs->RecordCount() ){
		
		$html .= "<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
			<thead>
			<tr>
				<!--<th style=\"background:#CECEF6;width:10%\">N° commentaire</th>-->
				<th style=\"background:#CECEF6;width:60%\">commentaire</th>
				<th style=\"background:#CECEF6;width:20%\">date relance</th>
				<!--<th style=\"background:#CECEF6;width:20%\">Nouvelle date</th>-->
				<th style=\"background:#CECEF6;width:10%\">réalisé</td>
				
			</tr>
			</thead>
			<tbody>\n";
		
		while( !$rs->EOF() ){
				$comment = nl2br($rs->fields( "comment" ));
			$idcontact_follow = $rs->fields('idcontact_follow');
			
			$realized = $rs->fields('realized');
			$date_relaunch = date("Y-m-d",strtotime($rs->fields('date_relaunch')));
			
			$html.= "
			<tr>
				<!--<td class=\"lefterCol\">" .  $rs->fields('idcontact_follow') . "</td>-->
				<td style=\"text-align:left\">" . $comment . "</td>
				
				<td>" . $date_relaunch . "</td>
					<!--<td>" . $rs->fields('date_new') . "</td>-->
					<td>"; 
					
					
					if ($realized == 1)  
					$html.= "<img src=\"$GLOBAL_START_URL/images/back_office/content/ok.png\" alt=\"réaliser\" width=\"14px\" height=\"14px\" />
					";
					else 
			$html.= "<img src=\"$GLOBAL_START_URL/images/back_office/content/non.png\" alt=\"Non réaliser\" width=\"14px\" height=\"14px\" /></td></tr>";
			
			$rs->MoveNext();
			
		}
		
		$html.= "	</tbody>
	</table>\n";
		
	}else{
		
		$html .= "<div style=\"font-size: 12px; white-space:nowrap;\">Ce client n'a aucun commentaire.</div>";
		
	}
	
	$html .= "</div>
</div>
</div>
</span>
</div>";
	
	return $html;
	
}
/****************************************************************************************************************************************/
?>

