<?php

include_once( "../objects/classes.php" );

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />			
				
<div class="centerMax">
		<!-- Titre -->
		<h1 class="titleSearch">
		<span class="textTitle"><?php echo htmlentities( Dictionnary::translate( "restocking" ) ) ?></span>
		<div class="spacer"></div>
		</h1>
		
		<?php
			if( isset( $_POST[ "restock" ] ) ){
				$generatedSupplierOrders = restock();
				if( count( $generatedSupplierOrders ) )
					listGeneratedSupplierOrders( $generatedSupplierOrders );	
			}
			displayForm();
		?>
</div>

<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------

function displayForm(){
	
	global 	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	
	$lang = "_1";
	$minstock_rate = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "minstock_rate" );
			
	$query = "
	SELECT d.idarticle,
		d.reference,
		d.ref_supplier,
		d.designation$lang AS designation,
		d.min_cde_supplier,
		d.stock_level,
		d.buyingcost,
		d.minstock,
		u.unit$lang AS unit,
		d.lot,
		p.idproduct,
		p.idsupplier,
		p.name$lang AS name,
		s.name AS supplierName,
		s.minbuyingamount
	FROM detail d, product p, unit u, supplier s
	WHERE d.idproduct = p.idproduct
	AND d.stock_level <> -1
	AND d.stock_level <= d.minstock
	AND d.unit = u.idunit
	AND p.idsupplier = s.idsupplier
	AND ((d.stock_level > 0 AND d.minstock / d.stock_level >= '$minstock_rate') OR d.stock_level = 0)
	ORDER BY p.idsupplier ASC, p.idproduct ASC, d.reference ASC";
	//echo $query ;
	$rs =& DBUtil::query( $query );

	if( !$rs->RecordCount() ){
	
		?>
		<p style="text-align:center;" class="msg">
			<?php echo Dictionnary::translate( "nothing_to_restock" ) ?>
		</p>
		<?php
			
		return;
		
	}
	
	?>
	<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript" language="javascript">
	<!--
	
		//-------------------------------------------------------------------------------------
		
		function isCollection( obj ){
		
   			return obj.length != null;
     
		}

		//-------------------------------------------------------------------------------------
		
		function selectReference( cell, idarticle ){
		
			var idarticles = document.forms.RestockingForm.elements[ 'idarticle[]' ];
			
			if( !isCollection( idarticles ) ) //si un seul élément
				idarticles = new Array( idarticles );
			
			var i = 0;
			var done = false;
			while( !done && i < idarticles.length ){
			
				if( idarticles[ i ].value == idarticle ){

					idarticles[ i ].checked = !idarticles[ i ].checked;
					cell.parentNode.className = idarticles[ i ].checked ? 'selectedRow' : 'defaultRow';
					
					updateReferenceAmountAt( i );
					
					done = true;
					
				}
				
				i++;
				
			}

		}
		
		//-------------------------------------------------------------------------------------

		function selectSupplierReferences( idsupplier, checked ){
		
			var suppliers 		= document.forms.RestockingForm.elements[ 'idsupplier[]' ];
			var idarticles 		= document.forms.RestockingForm.elements[ 'idarticle[]' ];

			if( !isCollection( suppliers ) ) //si un seul élément
				suppliers = new Array( suppliers );
			
			if( !isCollection( idarticles ) ) //si un seul élément
				idarticles = new Array( idarticles );
				
			var i = 0;
			while( i < suppliers.length ){
			
				if( suppliers[ i ].value == idsupplier ){

					var parentNode = idarticles[ i ].parentNode;
					var done = false;
					while( !done && parentNode ){
					
						if( parentNode.tagName.toLowerCase() == 'tr' ){
						
							idarticles[ i ].checked = checked;
							parentNode.className = checked ? 'selectedRow' : 'defaultRow';
							
							updateReferenceAmountAt( i );
							
							done = true;
						
						}
						else parentNode = parentNode.parentNode;
						
					}
					
				}
				
				i++;
				
			}
			
			document.getElementById( "selectText_" + idsupplier ).innerHTML = checked ? "Tout désélectionner" : "Tout sélectionner";
			
		}
		
		//-------------------------------------------------------------------------------------
		
		function updateSupplierAmounts( idsupplier ){

			var suppliers 		= document.forms.RestockingForm.elements[ 'idsupplier[]' ];
			var buyingcosts 	= document.forms.RestockingForm.elements[ 'buyingcost[]' ];
			var quantities 		= document.forms.RestockingForm.elements[ 'quantity[]' ];
			var articles 		= document.forms.RestockingForm.elements[ 'idarticle[]' ];
			
			if( !isCollection( suppliers ) ) //si un seul élément
				suppliers = new Array( suppliers );
				
			if( !isCollection( buyingcosts ) ) //si un seul élément
				buyingcosts = new Array( buyingcosts );
				
			if( !isCollection( quantities ) ) //si un seul élément
				quantities = new Array( quantities );
				
			if( !isCollection( articles ) ) //si un seul élément
				articles = new Array( articles );
				
			var i = 0;
			var total = 0.0;
			while( i < suppliers.length ){
			
				if( articles[ i ].checked && suppliers[ i ].value == idsupplier )
					total += parseFloat( buyingcosts[ i ].value ) * parseFloat( quantities[ i ].value );
				
				i++;
				
			}
			
			total = Math.round( total * 100.0 ) / 100.0;
			total = total.toFixed( 2 );
			
			document.getElementById( 'supplier_total_' + idsupplier ).innerHTML = total + '&nbsp;¤';
			
			//vérification du montant minimum de commande
			
			var minbuyingamount = parseFloat( document.getElementById( 'minbuyingamount_' + idsupplier ).innerHTML );

			var totalCell = document.getElementById( 'supplier_total_' + idsupplier );
			
			if( total > 0.0 && total < minbuyingamount ){
			
				document.getElementById( 'minbuyingamount_' + idsupplier ).style.color = '#FF0000';
				totalCell.style.color = '#FF0000';

				document.forms.RestockingForm.onsubmit = function(){
				
					alert( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "minbuyingamount_not_reached" ) ) ?>' );
					return false;
					
				}
				
			}
			else{
			
				totalCell.style.color = '#464D56';
				document.getElementById( 'minbuyingamount_' + idsupplier ).style.color = '#464D56';
				
				document.forms.RestockingForm.onsubmit = function(){ return checkQuantities(); }
				
			}
			
		}
		
		//-------------------------------------------------------------------------------------
		
		function updateReferenceAmountAt( index ){
		
			var idsuppliers 	= document.forms.RestockingForm.elements[ 'idsupplier[]' ];
			var buyingcosts 	= document.forms.RestockingForm.elements[ 'buyingcost[]' ];
			var quantities 		= document.forms.RestockingForm.elements[ 'quantity[]' ];
			var articles 		= document.forms.RestockingForm.elements[ 'idarticle[]' ];
			
			if( !isCollection( idsuppliers ) ) //si un seul élément
				idsuppliers = new Array( idsuppliers );
				
			if( !isCollection( buyingcosts ) ) //si un seul élément
				buyingcosts = new Array( buyingcosts );
				
			if( !isCollection( quantities ) ) //si un seul élément
				quantities = new Array( quantities );
				
			if( !isCollection( articles ) ) //si un seul élément
				articles = new Array( articles );
				
			var idsupplier 		= idsuppliers[ index ].value;
			var buyingcost 	= buyingcosts[ index ].value;
			var quantity 		= quantities[ index ].value;
			var article 		= articles[ index ];
			var total 			= 0.0;
			
			if( article.checked )
				total = parseFloat( buyingcost ) * parseFloat( quantity );
			
			total = Math.round( total * 100.0 ) / 100.0;
			total = total.toFixed( 2 );
			
			document.getElementById( 'ref_total_' + article.value ).innerHTML = total + '&nbsp;¤';
			
			updateSupplierAmounts( idsupplier );
			
		}
		
		//-------------------------------------------------------------------------------------
		
		//informations fournisseur
		
		function editSupplier( idsupplier){

			if( idsupplier == '0' )
				return;
				
			var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=650, height=650';
			var location = '/product_management/enterprise/supplier.php?idsupplier=' + idsupplier + '&ispopup=true';
			
			window.open( location, '_blank', options ) ;
			
		}
		
		//-------------------------------------------------------------------------------------

		function checkQuantities(){
		
			var quantity 			= document.forms.RestockingForm.elements[ 'quantity[]' ];
			var idarticle 			= document.forms.RestockingForm.elements[ 'idarticle[]' ];
			var min_cde_supplier 	= document.forms.RestockingForm.elements[ 'min_cde_supplier[]' ];
			
			if( !isCollection( quantity ) ) //si un seul élément
				quantity = new Array( quantity );
				
			if( !isCollection( idarticle ) ) //si un seul élément
				idarticle = new Array( idarticle );
				
			if( !isCollection( min_cde_supplier ) ) //si un seul élément
				min_cde_supplier = new Array( min_cde_supplier );
				
			var i = 0;
			var hasError = false;
			while( i < quantity.length ){
			
				if( idarticle[ i ].checked && parseInt( quantity[ i ].value ) < parseInt( min_cde_supplier[ i ].value ) ){
				
					quantity[ i ].style.backgroundColor = '#FF0000';
					quantity[ i ].style.color = '#FFFFFF';

					hasError = true;
					
				}
				else{
			
					quantity[ i ].style.backgroundColor = '#FFFFFF';
					quantity[ i ].style.color = '#464D56';
					
				}
				
				i++;
				
			}
			
			if( hasError )
				alert( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "min_cde_supplier_not_reached" ) ) ?>' );
				
			return !hasError;
			
		}
		
		//-------------------------------------------------------------------------------------
		
	// -->
	</script>
	
		<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/restock.php</span>
<?php } ?>
	
	<form action="restock.php" method="post" name="RestockingForm" enctype="multipart/form-data">
		
		<div class="blocResult mainContent">
			<div class="subContent content">
				<h2 class="subTitle" style="margin-top:3px;"><?php echo htmlentities( Dictionnary::translate( "critical_stock_levels" ) ) ?></h2>
				<p><?php echo Dictionnary::translate( "internal_customer_list" ) ?> : <?php listInternalCustomers(); ?></p>
			</div>
		</div>
		<div class="spacer"></div>
		
		
		<input type="hidden" name="restock" value="1" />
		<?php
		
		$i = 0;
		$last_idsupplier = false;
		while( !$rs->EOF() ){
			
			if( !$last_idsupplier || $rs->fields( "idsupplier" ) != $last_idsupplier ){
				
				?>
				
				<!-- Titre -->
				<h1 class="titleSearch">
					<span class="selectDate" style="position:relative; float:left; width:auto;" onmouseover="document.getElementById( 'SupplierInfosDiv_<?php echo $rs->fields( "idsupplier" ) ?>' ).style.display='block';" onmouseout="document.getElementById( 'SupplierInfosDiv_<?php echo $rs->fields( "idsupplier" ) ?>' ).style.display='none';">
						<a href="#" onclick="showSupplierInfos( <?php echo $rs->fields( "idsupplier" ) ?> ); return false;" style="font-weight:bold; color:#FF0000; font-size:12px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="Voir le détail" /></a>
						<?php getSupplierPopup( $rs->fields( "idsupplier" ) ); ?>
					</span>
					<span class="textTitle"><?php echo htmlentities( Dictionnary::translate( "supplier" ) ) ?> : <?php echo htmlentities( $rs->fields( "supplierName" ) ) ?></span>
					
					<div class="spacer"></div>
				</h1>
				<div class="blocResult mainContent">
					<div class="subContent content">
						<table class="dataTable resultTable">
							<thead>
								<tr>
									<th style="background:none; border-width:0px; width:3%;"></th>
									<th style="width:4%;">Icône</th>
									<th style="width:7%;">Produit n°</th>
									<th style="width:16%;">Désignation</th>
									<th style="width:6%;">Référence catalogue</th>
									<th style="width:6%;">Référence fournisseur</th>
									<th style="width:8%;">Quantité par lot</th>
									<th style="width:8%;">Prix d'achat</th>
									<th style="width:8%;">Stock actuel</th>
									<th style="width:2%;"> </th>
									<th style="width:8%;">Stock min.</th>
									<th style="width:8%;">Cde en cours</th>
									<th style="width:8%;">Cde minimum</th>
									<th style="width:7%;">Réapp.</th>
									<th style="width:7%;">Total HT</th>
								</tr>
							</thead>
							<tbody>
							<?php	

			}
			
			$iconSrc 			= URLFactory::getReferenceImageURI( $rs->fields( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE );
			$last_idsupplier 	= $rs->fields( "idsupplier" );
			$minbuyingamount 	= $rs->fields( "minbuyingamount" );
			
			$orderedQuantity 	= getOrderedQuantity( $rs->fields( "idsupplier" ), $rs->fields( "idarticle" ) );
			$minstock 			= $rs->fields( "minstock" );
			$min_cde_supplier 	= $rs->fields( "min_cde_supplier" );
			$stock_level 		= $rs->fields( "stock_level" );
			
			//quantité de base
			
			$quantity = 1;
			
			//minimum de commande imposé par le fournisseur
			
			if( $min_cde_supplier )
				$quantity = $min_cde_supplier;
			
			//passer en dessus du seuil qui déclenche le réapprovisionnement
			
			if( $quantity + $orderedQuantity < $minstock + 1 )
				$quantity += $minstock - $quantity - $orderedQuantity + 1;

			//$stockColor = $orderedQuantity + $stock_level > $minstock + 1 ? "#008800" : "#FF0000";
			$stockColor = $orderedQuantity + $stock_level > $minstock + 1 ? "#444444" : "#444444";
		
		
		//Added by behzad for task 4 edit 6
			
			$divide_minstock_by_stocklevel = $rs->fields( "minstock" ) / $rs->fields( "stock_level" );
			//echo $divide_minstock_by_stocklevel;
				if($rs->fields( "minstock" ) <= $rs->fields( "stock_level" ))
					$stock_proche_min_icon = "/images/back_office/content/more.png";
				elseif($divide_minstock_by_stocklevel >= $minstock_rate)
					$stock_proche_min_icon = "/images/back_office/content/less.png";
				elseif( $rs->fields( "stock_level" ) == 0)
					$stock_proche_min_icon = "/images/back_office/content/less.png";
				
			
			?>						
			<tr onmouseover="document.body.style.cursor='pointer';" onmouseout="document.body.style.cursor='default';">
				<td class="lefterCol">
					<input type="hidden" name="indexes[<?php echo $rs->fields( "idarticle" ) ?>]" value="<?php echo $i ?>" />
					<input type="hidden" name="idsupplier[]" value="<?php echo $rs->fields( "idsupplier" ) ?>" />
					<input type="hidden" name="buyingcost[]" value="<?php echo $rs->fields( "buyingcost" ) ?>" />
					<input type="hidden" name="min_cde_supplier[]" value="<?php echo $min_cde_supplier ?>" />
					<input type="checkbox" name="idarticle[]" value="<?php echo $rs->fields( "idarticle" ) ?>" onclick="this.checked = !this.checked; selectReference( this.parentNode, <?php echo $rs->fields( "idarticle" ) ?> );" />
				</td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><img src="<?php echo $iconSrc ?>" alt="" style="width:25px; height:25px;" /></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><?php echo $rs->fields( "idproduct" ) ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><?php echo $rs->fields( "name" ) ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><?php echo htmlentities( $rs->fields( "reference" ) ) ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><?php echo htmlentities( $rs->fields( "ref_supplier" ) ) ?></td>
				<!-- <td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><?php echo $rs->fields( "designation" ) ?></td> -->
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );"><?php echo $rs->fields( "lot" ) ?> / <?php echo $rs->fields( "unit" ) ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" style="text-align:center;"><?php echo Util::priceFormat( $rs->fields( "buyingcost" ) ) ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" style="color:<?php echo $stockColor ?>;"><?php echo $stock_level ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" style="color:<?php echo $stockColor ?>;"><img src='<?php echo $GLOBAL_START_URL.$stock_proche_min_icon ?>' /></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" style="color:<?php echo $stockColor ?>;"><?php echo $minstock ?></td>
				
					
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" style="color:<?php echo $stockColor ?>;"><?php echo $orderedQuantity ?></td>
				<td onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" style="text-align:center;"><?php echo $min_cde_supplier ?></td>
				<td>
					<input type="text" name="quantity[]" value="<?php echo $quantity ?>" style="text-align:right;" onkeypress="return forceUnsignedIntegerValue( event );" onkeyup="updateReferenceAmountAt( <?php echo $i ?> );" class="textInput" />
				</td>
				<td class="righterCol" onclick="selectReference( this, <?php echo $rs->fields( "idarticle" ) ?> );" id="ref_total_<?php echo $rs->fields( "idarticle" ) ?>" style="text-align:right;">0.00&nbsp;&euro;</td>
			</tr>
			
			<?php
			

			$rs->MoveNext();
			
			$i++;
			
			if( $rs->EOF() || $rs->fields( "idsupplier" ) != $last_idsupplier ){
				
								?>	
								<tr id="totalRow_<?php echo $last_idsupplier ?>">
									<td colspan="12" style="background:none; border-style:none; text-align:right;">
										Montant minimmum de commande :
										<span id="minbuyingamount_<?php echo $last_idsupplier ?>"><?php echo $minbuyingamount > 0.0 ? Util::priceFormat( $minbuyingamount ) : Dictionnary::translate( "none" ) ?></span>
									</td>
									<td id="supplier_total_<?php echo $last_idsupplier ?>" class="lefterCol righterCol" style="text-align:right;">0.00&nbsp;&euro;</td>
								</tr>
							</tbody>
						</table>
						<div class="spacer"></div>
						
						<div class="submitButtonContainer" style="margin-top:10px;">
							<div style="text-align:right; vertical-align:middle;">
								<input type="submit" name="restockBtn[]" id="restockBtn" class="inputDefault" value="Réapprovisionner" style="margin-top:10px;" />
								<a href="#" id="checker" onclick="selectSupplierReferences( <?php echo $last_idsupplier ?>, this.id == 'checker' ); this.id = this.id == 'checker' ? 'unchecked' : 'checker'; return false;" /><span id="selectText_<?php echo $last_idsupplier ?>">Tout sélectionner</span></a>
							</div>
						</div>	
						<div class="spacer"></div>
						
						
					</div>
				</div>
				
				<?php
				
			}

		}
				
	?>
	</form>
	<?php

}

//------------------------------------------------------------------------------------------

function getSupplierPopup( $idsupplier ){

	global $GLOBAL_START_URL;
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT s.*, 
		t.title$lang AS title, 
		st.name$lang AS stateName, 
		p.name$lang AS payment, 
		pd.payment_delay$lang AS payment_delay, 
		cs.charge_select$lang AS charge_select
	FROM supplier s, 
		supplier_contact sc,
		title t, 
		state st, 
		payment p, 
		payment_delay pd, 
		charge_select cs
	WHERE s.idsupplier = '$idsupplier'
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	AND s.idstate = st.idstate
	AND sc.idtitle = t.idtitle
	AND s.idpaiement = p.idpayment
	AND s.pay_delay = pd.idpayment_delay
	AND s.select_charge = cs.idcharge_select
	LIMIT 1";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		die( "Impossible de récupérer les informations du fournisseur '$idsupplier'" );
		
	$supplier = $rs->fields;
	
	?>			
	<div id="SupplierInfosDiv_<?php echo $idsupplier ?>" style="cursor:help; display:none; width:650px; position:absolute; left:-10px; top:-10px; z-index:99; padding:15px; background-color:#FFFFFF; border:1px solid #B7B7B7;">
		<div class="content">
			<div class="headTitle">
				<p class="title">Informations fournisseur : hgfhfghfghf</p>
			</div>
			<div class="subContent">
				<div class="resultTableContainer">
					<table class="dataTable">
						<tr>
							<th class="filledCell"><?php echo Dictionnary::translate("company_social") ?></th>
							<td width="150"><?php echo $supplier[ "name" ] ?></td>
							<th><?php echo Dictionnary::translate("gest_com_contact") ?></th>
							<td width="150" class="righterCol"><?php echo $supplier[ "title" ] . " " . $supplier[ "firstname" ] . " " . $supplier[ "lastname" ] ?></td>	
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("adress") ?></th>
							<td width="150"><?php echo $supplier[ "adress" ] ?></td>
							<th class="filledCell"><?php  echo Dictionnary::translate("adress_2") ?></th>
							<td width="150" class="righterCol"><?php echo $supplier[ "adress_2" ] ?></td>		
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("zipcode") ?></th>
							<td width="150"><?php echo $supplier[ "zipcode" ] ?></td>
							<th class="filledCell"><?php  echo Dictionnary::translate("city") ?></th>
							<td width="150" class="righterCol"><?php echo $supplier[ "city" ] ?></td>		
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("state") ?></th>
							<td width="150">
								<?php echo $supplier[ "stateName" ] ?>
							</td>
							<th class="filledCell"><?php  echo Dictionnary::translate("email") ?></th>
							<td width="150" class="righterCol"><a href="mailto:<?php echo $supplier[ "email" ] ?>" class="mail"><?php echo $supplier[ "email" ] ?></a></td>		
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("phone") ?></th>
							<td width="150"><?php echo $supplier[ "phonenumber" ] ?></td>
							<th class="filledCell"><?php  echo Dictionnary::translate("fax") ?></th>
							<td width="150" class="righterCol"><?php echo $supplier[ "faxnumber" ] ?></td>		
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("payment") ?></th>
							<td width="150"><?php echo $supplier[ "idpaiement" ] ?></td>
							<th class="filledCell"><?php  echo Dictionnary::translate("payment_delay") ?></th>
							<td width="150" class="righterCol"><?php echo $supplier[ "payment_delay" ] ?></td>		
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("select_charge") ?></th>
							<td width="150"><?php echo $supplier[ "charge_select" ] ?></td>
							<th class="filledCell"><?php  echo Dictionnary::translate("charge_rate") ?></th>
							<td width="150" class="righterCol"><?php echo $supplier[ "charge_rate" ] ?></td>		
						</tr>
						<tr>
							<th class="filledCell"><?php  echo Dictionnary::translate("gest_com_franco_supplier") ?></th>
							<td colspan="3" class="righterCol"><?php echo Util::priceFormat( $supplier[ "franco_supplier" ] ) ?></td>		
						</tr>
					</table>
					<?php
					
					if( !empty( $supplier[ "comment" ] ) ){
						
						?>
						<table class="dataTable" style="margin-top:15px;">
							<tr>
								<th class="filledCell">
									<?php echo Dictionnary::translate( "comment" ) ?> :
								</th>
							</tr>
							<tr>
								<td class="lefterCol righterCol" style="white-space:normal;">
									<?php echo nl2br( $supplier[ "comment" ] ) ?>
								</td>
							</tr>
						</table>
						<?php
			
					}
			
					?>
					<div class="submitButtonContainer">
						<input type="submit" value="Modifier" onclick="editSupplier( <?php echo $idsupplier ?> ); return false;" class="blueButton" />
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
		
}

//------------------------------------------------------------------------------------------

function restock(){

	if( !isset( $_POST[ "idarticle" ] ) )
		return;
		
	$indexes 			= $_POST[ "indexes" ];
	$internal_customer 	= intval( DBUtil::getParameterAdmin( "internal_customer" ) );
	$idcontact 			= intval( $_POST[ "idcontact" ] );

	//tri par fournisseur
	
	foreach( $_POST[ "idarticle" ] as $idarticle ){

		$idsupplier = $_POST[ "idsupplier" ][ $indexes[ $idarticle ] ];
		$quantity 	= $_POST[ "quantity" ][ $indexes[ $idarticle ] ];

		$supplierOrders[ $idsupplier ][ $idarticle ] = $quantity;
		
	}
	
	//création des commandes fournisseur
	
	$internal_delivery = DBUtil::getParameterAdmin( "internal_delivery" );
	$ad_name = DBUtil::getParameterAdmin( "ad_name" );
	$generatedSupplierOrders = array();
	foreach( $supplierOrders as $idsupplier => $references ){
		
		$supplierOrder = TradeFactory::createSupplierOrder( $idsupplier, $internal_customer, $idcontact );	
		$supplierOrder->set( "internal_supply", 1 );
		$supplierOrder->set( "iddelivery", $internal_delivery );
		
		//ajout des références
		
		include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
		
		foreach( $references as $idarticle => $quantity )
			$supplierOrder->addArticle( new CArticle( $idarticle ), $quantity );


		
		//création du BL associé
		
		$idorder_supplier = $supplierOrder->get( "idorder_supplier" );
		
		$generatedSupplierOrders[] = $idorder_supplier;
		
		$deliveryNote = TradeFactory::createDeliveryNoteFromSupplierOrder( $supplierOrder );
		//$deliveryNote->set( "iddelivery", $internal_delivery );
		
		$idbl_delivery = $deliveryNote->get( "idbl_delivery" );
		
			//Added by behzad since task 7.b edit6
		if($supplierOrder->get("iddelivery") == 0){
			$idbuyer = $supplierOrder->get("idbuyer");
			$query = "SELECT B.company, C.firstname, C.lastname  FROM `contact` C,buyer B WHERE 
						C.idbuyer = B.idbuyer
						AND B.`idbuyer` = '$idbuyer' ";
			$rs_buyer =& DBUtil::query( $query );
			
			$contre_marque = $rs_buyer->fields( "company" ) .', '. $rs_buyer->fields( "firstname" ).' '. $rs_buyer->fields( "lastname" );
		}else{
			$idbuyer = $deliveryNote->get("idbuyer");
			$query = "SELECT B.company, C.firstname, C.lastname  FROM `contact` C,buyer B WHERE 
						C.idbuyer = B.idbuyer
						AND B.`idbuyer` = '$idbuyer' ";
			//echo $query;
			$rs_buyer =& DBUtil::query( $query );
			$contre_marque = $rs_buyer->fields( "company" ) .', '. $rs_buyer->fields( "firstname" ).' '. $rs_buyer->fields( "lastname" );
		}
		$coments_to_save = "Contre-marque : ".$contre_marque."\n\rMagasin : ".$ad_name ;
		
		$supplierOrder->set("comment",$coments_to_save);
		$supplierOrder->set("commentBL",$coments_to_save);
		$deliveryNote->set("commentBL",$coments_to_save);
		
		///////////////////////
		$supplierOrder->save();
		$deliveryNote->save();
		//$supplierOrder->set( "bl", $deliveryNote->get( "idbl_delivery" ) ); //jointure inversée qui sert à rien @todo ALTER TABLE `order_supplier` DROP `bl`
		//die;
		DBUtil::query( "UPDATE `order_supplier` SET bl = '$idbl_delivery' WHERE idorder_supplier = '$idorder_supplier' LIMIT 1" );
	}

	return $generatedSupplierOrders;

}

//------------------------------------------------------------------------------------------

function listGeneratedSupplierOrders( $generatedSupplierOrders ){
	
	global $GLOBAL_START_URL;
	
	//liste des commandes générées
	
	?>
			<div class="blocResult mainContent">
				<div class="subContent content">
					<h2 class="subTitle" style="margin-top:3px;">
						<?php echo Dictionnary::translate( "generated_supplier_orders" ) ?>
					</h2>
					<div class="spacer"></div>
				
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th style="text-align:center;">Commande fournisseur n°</th>
								<th style="text-align:center;"><?php echo Dictionnary::translate( "supplier" ) ?></th>
								<th style="text-align:center;">BL n°</th>
								<th style="text-align:center;"><?php echo Dictionnary::translate( "internal_customer" ) ?></th>
								<th style="text-align:center;">Total HT</th>
								<th style="text-align:center;">Total TTC</th>
							</tr>
						</thead>
						<tbody>
						<?php

							foreach( $generatedSupplierOrders as $idorder_supplier ){
					
								$query = "
								SELECT os.idsupplier, 
									os.idbuyer, 
									os.idcontact, 
									os.total_amount_ht, 
									os.total_amount, 
									s.name AS supplierName, 
									bl.idbl_delivery,
									b.zipcode,
									b.company,
									c.lastname, 
									c.firstname
								FROM order_supplier os, supplier s, bl_delivery bl, buyer b, contact c
								WHERE os.idorder_supplier = '$idorder_supplier'
								AND os.idsupplier = s.idsupplier
								AND os.idorder_supplier = bl.idorder_supplier
								AND os.idbuyer = c.idbuyer
								AND os.idcontact = c.idcontact
								AND os.idbuyer = b.idbuyer
								LIMIT 1";
								
								$rs =& DBUtil::query( $query );
								
								?>											
								<tr class="blackText">
									<td class="lefterCol">
										<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier ?>" onclick="window.open(this.href); return false;"><?php echo $idorder_supplier ?></a>
									</td>
									<td>
										<a href="/product_management/enterprise/supplier.php?idsupplier=<?php echo $rs->fields( "idsupplier" ) ?>" onclick="window.open(this.href); return false;"><?php echo $rs->fields( "supplierName" ) ?></a>
									</td>
									<td><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_delivery.php?iddelivery=<?php echo $rs->fields( "idbl_delivery" ) ?>" onclick="window.open(this.href); return false;"><?php echo $rs->fields( "idbl_delivery" ) ?></a></td>
									<td>
										<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=buyer&key=<?php echo $rs->fields( "idbuyer" ) ?>" target="blank">
											<?php echo htmlentities( $rs->fields( "company" ) ) ?> 
											( <?php echo $rs->fields( "zipcode" ) ?> )
											<?php echo htmlentities( $rs->fields( "lastname" ) ) . " " . htmlentities( $rs->fields( "firstname" ) ) ?>
										</a>
									</td>
									<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
									<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
								</tr>
								<?php
					
							}
							
						?>
						</tbody>
					</table>
				</div>
			</div>
	<?php
	
}

//------------------------------------------------------------------------------------------

function listInternalCustomers(){
	
	global $GLOBAL_START_URL;
	
	$internal_customer = intval( DBUtil::getParameterAdmin( "internal_customer" ) );

	$query = "
	SELECT b.idbuyer, b.zipcode, b.company, b.contact, c.idcontact, u.initial, c.lastname, c.firstname
	FROM buyer b, contact c, user u
	WHERE b.idbuyer = '$internal_customer'
	AND b.idbuyer = c.idbuyer
	AND b.iduser = u.iduser
	ORDER BY c.lastname ASC, c.firstname ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<select name="idcontact">
	<?php

		while( !$rs->EOF() ){
	
			$idbuyer = $rs->fields( "idbuyer" );
			$idcontact = $rs->fields( "idcontact" );

			?>
			<option value="<?php echo $rs->fields( "idcontact" ) ?>">
				<?php echo htmlentities( $rs->fields( "company" ) ) ?> 
				( <?php echo $rs->fields( "zipcode" ) ?> )
				- <?php echo htmlentities( $rs->fields( "lastname" ) ) . " " . htmlentities( $rs->fields( "firstname" ) ) ?>
			</option>	
			<?php 
	
			$rs->MoveNext();
	
		}

	?>
	</select>
	<?php

}

//------------------------------------------------------------------------------------------

function getOrderedQuantity( $idsupplier, $idarticle ){
	
	$internal_customer = intval( DBUtil::getParameterAdmin( "internal_customer" ) );
	
	$query = "
	SELECT SUM( osr.quantity ) AS orderedQuantity
	FROM order_supplier_row osr, order_supplier os
	WHERE os.idsupplier = '$idsupplier'
	AND os.idorder_supplier = osr.idorder_supplier
	AND os.idbuyer = '$internal_customer'
	AND osr.idarticle = '$idarticle'
	AND os.status NOT IN ('delivered', 'dispatch', 'received', 'send', 'cancelled')
	GROUP BY osr.idarticle";
	
	$rs =& DBUtil::query( $query );

	return $rs->RecordCount() ? $rs->fields( "orderedQuantity" ) : 0;
	
}

//------------------------------------------------------------------------------------------

?>