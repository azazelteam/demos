<!-- ++++++++++++++++++++++++++++++++++++++++++ CADRE MONTANTS ++++++++++++++++++++++++++++++++++++++++++ -->
<?php

anchor( "UpdateTotalDiscountAmount" );
anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" );
anchor( "UpdateInstallmentRate" );
anchor( "UpdateInstallmentAmount" );
anchor( "UpdateTotalChargeHT" );
anchor( "UpdateChargeSupplierRate" );
anchor( "UpdateChargeSupplierAmount" );
anchor( "ApplySupplierCharges" );

//total ttc
$vatrate = $Order->getVATRate();
$vat_amount = ($total_estimate_ht * $vatrate) / 100;
$ttc1 = $total_estimate_ht + $vat_amount;

?>
<!-- ************************************* MONTANTS ET TARIFICATIONS *********************************** -->
<?php $total_charge_auto = $Order->getValue( "total_charge_auto" ); ?>
<script type="text/javascript">
/* <![CDATA[ */

	function setExFactory( element ){
	
		//document.forms.frm.elements[ 'ex_factory' ].value = this.checked ? '1' : '0';
		
	}

/* ]]> */
</script>
<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="topRight"></div><div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer" style="margin-bottom:10px;">
                <p class="subTitle">Montant et tarification du devis</p>
				<div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
            </div>
              <div class="leftContainer" style="margin-top:0px;width:89%;">
            	<table class="dataTable devisTable">
                	<tr>
                    	<th>Prix vente<br />HT brut</th>
                        <th colspan="2">Remise supplémentaire<br />sur facture</th>
						<th>Total HT net</th>
						<th colspan="2">Port vente refacturé</th>
						<th>Total HT<br />avec port vente</th>
						<th colspan="2">Remise pour<br />paiement comptant</th>
						<th>Total HT</th>
						<th>Taux TVA</th>
						<th>Montant TVA</th>
						<th>Total TTC</th>
                    </tr>
                    <tr>
                    	<td rowspan="3"><?php echo Util::priceFormat( $Order->GetTotal_amount_ht() + $Order->getValue( "total_discount_amount" ) ); ?></td>
                        <td rowspan="3">
                        	<input type="text" name="total_discount_rate" class="textInput percentage" value="<?php echo Util::numberFormat($total_discount_rate) ?>" onchange="document.getElementById('UpdateTotalDiscountRate').value='ok'; document.getElementById('UpdateTotalDiscountAmount').value='';" /> %
                        	<input type="hidden" name="UpdateTotalDiscountRate" id="UpdateTotalDiscountRate" value="" />
                        </td>
                        <td rowspan="3">
                        	<input type="text" name="total_discount_amount" class="textInput price" value="<?php echo Util::numberFormat($total_discount_amount) ?>" onchange="document.getElementById('UpdateTotalDiscountAmount').value='ok'; document.getElementById('UpdateTotalDiscountRate').value='';" /> &euro;
                        	<input type="hidden" name="UpdateTotalDiscountAmount" id="UpdateTotalDiscountAmount" value="" />
                        </td>
                        <td rowspan="3"><?php echo Util::priceFormat( $Order->GetTotal_amount_ht() ) ?></td>
                        <td colspan="2">
							<select name="total_charge_auto" onchange="updateCharges( this, '<?php echo Util::numberFormat( $Order->WotCharge( true ) ) ?>' );">
                        		<option value="1"<?php if( $total_charge_auto ) echo " selected=\"selected\""; ?>>Coût calculé</option>
                        		<option value="0"<?php if( !$Order->getValue( "ex_factory" ) && !$total_charge_auto ) echo " selected=\"selected\""; ?>>Coût paramétré</option>
                        		<option value="1"<?php if( $Order->getValue( "ex_factory" ) ) echo " selected=\"selected\""; ?>>Départ usine</option>
                        	</select>
                        	<br />
                        	<input type="hidden" name="ex_factory" value="<?php echo $Order->getValue( "ex_factory" ) ? "1" : "0"; ?>" />
						</td>
						<td rowspan="3"><b><?php echo Util::priceFormat( $Order->GetTotal_amount_ht() + $Order->GetTotal_charge_ht() ) ?></b></td>
						<td rowspan="3"><input<?php echo $disabled ?> type="text" id="billing_rate" name="billing_rate" class="textInput percentage" value="<?php echo Util::numberFormat( $billing_rate ); ?>" onchange="document.getElementById('UpdateBillingRate').value='ok'; document.getElementById('UpdateBillingAmount').value='';" /> %</td>
                        <td rowspan="3"><input<?php echo $disabled ?> type="text" id="billing_amount" name="billing_amount" class="textInput price" value="<?php echo Util::numberFormat( $billing_amount ); ?>" onchange="document.getElementById('UpdateBillingRate').value=''; document.getElementById('UpdateBillingAmount').value='ok';" /> &euro;</td>
						<td rowspan="3"><b><?php echo Util::priceFormat( $Order->GetTotal_amount_ht_wb() ) ?></b></td>
						<td rowspan="3"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></td>
						<td rowspan="3"><?php echo Util::priceFormat( ( $Order->GetTotal_amount_ht_wb() ) * $Order->getVATRate() / 100.0 ); ?></td>
						<td rowspan="3"><b><?php echo Util::priceFormat( $Order->GetTotal_amount_ttc_wb() ); ?></b></td>
                    </tr>
                    <tr>
                    	<th>Poids</th>
                    	<th>Port HT</th>
                    </tr>
                    <tr>
                    	<td>
                    		<?php
                    			
                    			$Order->GetTotal_weight();
                    			/*$Order->WTCharge();*/
                    			echo Util::numberFormat( $Order->getValue( "total_weight" ) )
                    			
                    		?>
                    		&nbsp;kg
                    	</td>
                    	<td>
                    		<input<?php echo $disabled ?> name="total_charge_ht" id="total_charge_ht" value="<?php echo $Order->getValue( "total_charge_ht" ) > 0.0 ?  Util::numberFormat( $Order->getValue( "total_charge_ht" )  ) : "Franco"; ?>" <?php if( $Order->getValue( "total_charge_auto" ) ) echo " disabled=\"disabled\""; ?> class="textInput price" />&nbsp;&euro;
                    		<input type="hidden" name="UpdateTotalChargeHT" value="ok" />
                    	</td>
                    </tr>
                </table>
            </div>
            <div class="rightContainer" style="width:95px;">
		    	<div class="tableContainer" style="margin-top:0px;">
		            <table class="dataTable devisTable summaryTable">
		                <tr>
		                    <th colspan="2">Marge nette après <br /> Port Achat et Vente</th>
		                </tr>
		                <tr>
		                    <td colspan="2"><?php $netMarginAmount = Estimate::getNetMarginAmount( $Order->getId() ); ?><?php echo Util::priceFormat( $netMarginAmount ); ?></td>
		                </tr>
		                        <tr>
		                            <th>%</th>
		                            <th>Coeff.</th>
		                        </tr>
		                <tr>
		                    <td style="width:50%"><?=Util::rateFormat( Estimate::getNetMarginRate( $Order->getId() ) ) ?></td>
							<td style="width:50%"><?php echo $netMarginAmount ? Util::numberFormat( ( $Order->GetTotal_amount_ht() + $Order->GetTotal_charge_ht() ) / $netMarginAmount ) : "0" ?></td>
		                </tr>
		            </table>
		    	</div><!-- tableContainer -->
    		</div><!-- rightContainer -->
			<div class="leftContainer" style="margin-top:5px;">
	        	<input type="submit" class="blueButton" name="ModifyEstimate" value="Recalculer" />
	        </div>
	        <div class="clear"></div>
        </div>
   	</div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div>