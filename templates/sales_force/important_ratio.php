<?
/**
 * Package commercial
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Ratios clients B2B
 */
?>
	<!-- Titre -->
	
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/important_ratio.php</span>
<?php } ?>
	<h1 class="titleSearch">
	<span class="textTitle">Ratios importants</span>
	<div class="spacer"></div>
	</h1>
	
	<div class="blocResult mainContent">
		<div class="content">
		<div class="subContent">
		    <table class="dataTable resultTable">
				<thead>
					<tr>
						<th rowspan="2" style="background-color:#FFFFFF; border-width:0px;"></th>
						<th colspan="8">Total commandes (HT)</th>
						<th rowspan="2">Marge nette après <br>port achat et vente</th>
						
					</tr>
					<tr>
						<th>A traîter</th>
						<th>En cours</th>
						<th>Demandes en cours</th>
						<th>Annulé</th>
						<th>Commandé total</th>
						<th>En préparation</th>
						<th>Expédié</th>
						<th>Moyenne commandé</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th style="width:6%;">Quantité</th>
						<td style="width:7%;"><?php echo $nbtodo ?></td>
						<td style="width:7%;"><?php echo $nbattente ?></td>
						<td style="width:7%;"><?php echo $nbdde ?></td>
						<td style="width:7%;"><?php echo $nbcancelled ?></td>
						<td style="width:7%;"><?php echo $nbtotal ?></td>
						<td style="width:7%;"><?php echo $nbpicking ?></td>
						<td style="width:7%;"><?php echo $nbdelivered ?></td>
						<td style="width:7%;"><?php echo $nbtotal ? $nbtotal : "-" ?></td>
						<td style="width:14%;" class="righterCol">
						<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
						<?php echo $nbtotal ? Util::rateFormat( $order_net_margin_rate + ($_SESSION['rate'] * $order_net_margin_rate) / 100, 2 ) : "-" ?>
						<?php } ?> 
						<?php if($akilae_comext ==1){ ?> / 
						<span style="color:red !important;">
						<?php echo $nbtotal ? Util::rateFormat( $order_net_margin_rate + ($_SESSION['rate'] * $order_net_margin_rate) / 100 - $_SESSION['rate'], 2 ) : "-" ?>
						<?php } ?>
						</span>
						</td>
					</tr>
					<tr>
						<th style="width:6%;">Montant</th>
						<td style="width:7%;"><?php echo Util::priceFormat( $totaltodo )?></td>
						<td style="width:7%;"><?php echo Util::priceFormat( $totalattente )?></td>
						<td style="width:7%;"><?php echo Util::priceFormat( $totaldde )?></td>
						<td style="width:7%;"><?php echo Util::priceFormat( $totalcancelled )?></td>
						<td style="width:7%;"><?php echo Util::priceFormat( $totalcommande + $totalpicking + $totaldelivered)?></td>
						<td style="width:7%;"><?php echo Util::priceFormat( $totalpicking )?></td>
						<td style="width:7%;"><?php echo Util::priceFormat( $totaldelivered )?></td>												
						<td style="width:7%;"><?php echo $totalorder ? Util::priceFormat( $moy ) : "-" ?></td>
						<!--<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
						<td style="width:14%;"><?php echo $totalorder ? Util::priceFormat( $order_rough_stroke_amount ) : "-" ?></td>
						<?php } ?>-->
						<td style="width:14%;" class="righterCol">
						<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
						<?php echo $totalorder ? Util::priceFormat( $order_net_margin_amount ) : "-" ?>
						<?php } ?>
						<?php if($akilae_comext ==1){ ?> / 
						<span style="color:red !important;"><?php echo $totalorder ? Util::priceFormat( $order_net_margin_amount / ((1+ $_SESSION['rate'] / 100))) : "-" ?></span>
						<?php } ?>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		</div>
	</div>
	<div class="spacer"></div>		
	            
                
    <h1 class="titleSearch">
	<span class="textTitle">Provenance</span>
	<span class="selectDate">
		<a href="#topPage" class="goUpOrDown" style="font-size:10px; color:#000000;">
        	Aller en haut de page
            <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
        </a>
	</span>
	<div class="spacer"></div>
	</h1>	
    
    
    <div class="blocResult mainContent">
        <div class="content">
            <div class="subContent">
                <!--
                <a href="#topPage" class="goUpOrDown">
                	Aller en haut de page
                    <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>
                -->
			    <table class="dataTable resultTable">
			    	<thead>
						<tr>
							<th style="background-color:#FFFFFF; border-width:0px;"></th>
							<th>Total commandes</th>
<?php
								
								//Rendre la provenance en dynamique
                                $query = "SELECT * FROM source";
                                $rs =& DBUtil::query( $query );
                                while( !$rs->EOF() ){
                                    $sources[$rs->fields( "idsource" )]	= $rs->fields( "source_1" );
                            		$rs->MoveNext();
                            	}
                                foreach($sources as $key=>$value){
                                    echo "<th style=\"vertical-align:top;\">" . $value . "</th>\n";
                                }
								
								//Calcul de la largeur en pourcentage de chaque colonne afin d'avoir toutes les colonnes à la même taille
								$width = floor( 92 / ( count( $sources ) + 2 ) );
								
?>
							
                            <!--Rendre la provenance en dynamique <th>Autres</th>-->
						</tr>
					</thead>
					<tbody>
						<tr>
							<th style="width:<?php echo ( 100 - ( count( $sources ) + 2 ) * $width ) ?>%;"><?php echo Dictionnary::translate( "number" ) ?></th>
<?php
						
								//Nombre de devis
								
								//Rendre la provenance en dynamique
                                $totalNb = 0;
                                foreach($provenance as $key=>$value){
                                    $totalNb += $value["nb"];
                                }
                                echo "				<td$class style=\"width:" . $width . "%;\">" . $totalNb . "</th>\n";	
                                foreach($sources as $key=>$value){
                                    $nb = $provenance[$key]["nb"];
                                    echo "				<td$class style=\"width:" . $width . "%;\">" . $nb . "</th>\n";
                                }	
?>
						</tr>
						<tr>
							<th><?php echo Dictionnary::translate( "rate_number" ) ?></th>
<?php
								
								//Nombre de devis en %
								
								//Rendre la provenance en dynamique
								echo "				<td$style>" . Util::rateFormat( 100 * $totalNb/ $totalNb, 2 ) . "</th>\n";
                                foreach($sources as $key=>$value){
                                    $nb = $provenance[$key]["nb"];
                                    echo "				<td$style>" . Util::rateFormat( 100 * $nb/ $totalNb, 2 ) . "</th>\n";
                                }
?>
						</tr>
						<tr>
							<th><?php echo Dictionnary::translate( "amount" ) ?></td>
<?php
								
								//Montant des devis
								
								//Rendre la provenance en dynamique
                                $totalMt = 0;
                                foreach($provenance as $key=>$value){
                                    $totalMt += $value["supAmount"];
                                }
                                echo "				<td$style>" . Util::priceFormat( $totalMt ) . "</th>\n";
                                foreach($sources as $key=>$value){
                                    $supAmount = $provenance[$key]["supAmount"];
                                    echo "				<td$style>" . Util::priceFormat( $supAmount ) . "</th>\n";
                                }	
?>
						</tr>
						<tr>
							<th><?php echo Dictionnary::translate( "average_estimate" ) ?></td>
<?php
								
								//Devis moyen
								
								//Rendre la provenance en dynamique
                                echo "<td$style>" . Util::priceFormat( $totalMt / $totalNb, 0 ) . "</th>\n";
                                foreach($sources as $key=>$value){
                                    $supAmount = $provenance[$key]["supAmount"];
                                    $nb = $provenance[$key]["nb"];
                                    if($nb > 0)echo "<td$style>" . Util::priceFormat( $supAmount / $nb, 0 ) . "</th>\n";
                                    else echo "<td$style>" . Util::priceFormat(0, 0 ) . "</th>\n";
                                }	
?>
						</tr>
					</tbody>
				</table>
				
			</div>
			<div class="spacer"></div>
		</div>
	</div>
	<div class="spacer"></div><br/>	