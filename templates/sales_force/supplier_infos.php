<?php

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

if( isset( $_REQUEST[ "ispopup" ] ) && $_REQUEST[ "ispopup" ] == "true" )
	$banner = "no";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------------

$supplierInfos = array(

	"name"				=> "",
	"managertitle"		=> "",
	"managerfirstname"	=> "",
	"managerlastname"	=> "",
	"adress"			=> "",
	"adress_2"			=> "",
	"zipcode"			=> "",
	"city"				=> "",
	"idstate"			=> 0,
	"email"				=> "",
	"phonenumber"		=> "",
	"faxnumber"			=> "",
	"comment"			=> "",
	"idpaiement"		=> "",
	"pay_delay"			=> "",
	"select_charge"		=> "",
	"charge_rate"		=> "",
	"info_1"			=> "",	
	"info_2"			=> "",
	"info_3"			=> "",	
	"info_4"			=> "",	
	"info_5"			=> "",	
	"info_6"			=> "",	
	"info_7"			=> "",	
	"info_8"			=> "",	
	"info_9"			=> "",		
	"info_10"			=> "",										
	"send_to"			=> ""
	
);

getSupplierInfos( $supplierInfos );

//--------------------------------------------------------------------------------------------------

?>
			<div id="globalMainContent">
				<script type="text/javascript" language="javascript">
				<!--
					
					function selectSupplier(){
						
						var supplierList = document.forms.SupplierForm.elements[ 'idsupplier' ];
						var selectedValue = supplierList.options[ supplierList.selectedIndex ].value;
						var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_infos.php?idsupplier=' + selectedValue;
<?php
							
							if( isset( $_REQUEST[ "ispopup" ] ) )
							echo "location += '&ispopup=true';";
							
?>
						
						document.location = location;
						
					}
					
				// -->
				</script>
				<div class="mainContent fullWidthContent">
					<div class="topRight"></div><div class="topLeft"></div>
					<div class="content">
						<div class="headTitle">
							<p class="title">Informations fournisseur</p>
						</div>
						<div class="subContent">
							<div class="tableHeaderLeft"></div>
							<div class="tableHeaderRight"></div>
<?php
	
if( isset( $_POST[ "UpdateComments" ] ) )
	UpdateComments();

displaySupplierInfos( $supplierInfos );

?>
						</div>
					</div>
					<div class="bottomRight"></div><div class="bottomLeft"></div>
				</div>
			</div>
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//--------------------------------------------------------------------------------------------------

function displaySupplierInfos( $supplierInfos ){
	
	global	$GLOBAL_START_URL;

?>
							<form id="SupplierForm" name="SupplierForm" action="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_infos.php" method="post" enctype="multipart/form-data">
<?php
	
	if( isset( $_REQUEST[ "ispopup" ] ) )
		echo "<input type=\"hidden\" name=\"ispopup\" value=\"1\" />";
	
?>                              
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/supplier_infos.php</span>
<?php } ?>


								<div style="font-weight:bold; margin:10px 0px;">Choix du fournisseur : <?php echo getSupplierList() ?></div>
								<div class="clear">
									<table class="dataTable">
										<tr>
											<th class="filledCell">Raison sociale</th>
											<td><?php echo $supplierInfos[ "name" ] ?></td>
											<th class="filledCell">Contact (nom)</th>
											<td><?php echo getManagerTitle( $supplierInfos[ "managertitle" ]  ) . " " . $supplierInfos[ "managerfirstname" ] . " " . $supplierInfos[ "managerlastname" ] ?></td>
										</tr>
										<tr>
											<th class="filledCell">Adresse</th>
											<td><?php echo $supplierInfos[ "adress" ] ?></td>
											<th class="filledCell">Complément adresse</th>
											<td><?php echo $supplierInfos[ "adress_2" ] ?></td>
										</tr>
										<tr>
											<th class="filledCell">Code postal</th>
											<td><?php echo $supplierInfos[ "zipcode" ] ?></td>
											<th class="filledCell">Ville</th>
											<td><?php echo $supplierInfos[ "city" ] ?></td>
										</tr>
										<tr>
											<th class="filledCell">Pays</th>
											<td><?php echo getCountryName( $supplierInfos[ "idstate" ] ); ?></td>
											<th class="filledCell">e-mail</th>
											<td><a href="mailto:<?php echo $supplierInfos[ "email" ] ?>"><?php echo $supplierInfos[ "email" ] ?></a></td>
										</tr>
										<tr>
											<th class="filledCell">Téléphone</th>
											<td><?php echo $supplierInfos[ "phonenumber" ] ?></td>
											<th class="filledCell">Fax</th>
											<td><?php echo $supplierInfos[ "faxnumber" ] ?></td>
										</tr>
										<tr>
											<th class="filledCell">Mode de paiement</th>
											<td><?php echo getPaymentTxt( $supplierInfos[ "idpaiement" ] ) ?></td>
											<th class="filledCell">Délais de paiement</th>
											<td><?php echo getPaymentDelayTxt( $supplierInfos[ "pay_delay" ] ) ?></td>
										</tr><tr>
											<th class="filledCell">N° client</th>
											<td><?php echo $supplierInfos[ "info_1" ] ?></td>
											<th class="filledCell">Remise commerciale</th>
											<td><?php echo $supplierInfos[ "info_2" ] ?></td>
										</tr><tr>
											<th class="filledCell">Franco</th>
											<td><?php echo $supplierInfos[ "info_3" ] ?></td>
											<th class="filledCell">Livraison directe</th>
											<td><?php echo $supplierInfos[ "info_4" ] ?></td>
										</tr><tr>
											<th class="filledCell">Transporteur</th>
											<td><?php echo $supplierInfos[ "info_5" ] ?></td>
										</tr>
										
										
										<!--<tr>
											<th class="filledCell">Type de frais de port</th>
											<td><?php echo getSelectCharge( $supplierInfos[ "select_charge" ] ) ?></td>
											<th class="filledCell">Pourcentage achat</th>
											<td><?php echo Util::rateFormat( $supplierInfos[ "charge_rate" ] ) ?></td>
										</tr>
										<tr>
											<th class="filledCell">Mode d'envoi favori</th>
											<td colspan="3"><?php echo $supplierInfos[ "send_to" ] ?></td>
										</tr>-->
									</table>
								</div>
								<div style="margin:10px 0px;">
									<div style="float:left;">Commentaires :</div>
									<textarea name="comment" style="float:right; height:300px; width:880px;"><?php echo nl2br( $supplierInfos[ "comment" ] ) ?></textarea>
								</div>
								<div class="clear"></div>
								<div class="submitButtonContainer" style="margin:10px 0px;">
									<input type="hidden" name="UpdateComments" />
									<input type="submit" value="Enregistrer" class="blueButton" />
									<input type="reset" value="Annuler" class="blueButton" />
								</div>
							</form>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function getSupplierList(){
	
	$html = "
	<select name=\"idsupplier\" onchange=\"selectSupplier();\">
		<option value=\"0\">-</option>";
		
	$Base = DBUtil::getConnection();
	$rs = $Base->Execute( "SELECT idsupplier, name FROM supplier ORDER BY name ASC" );
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			if( $_REQUEST[ "idsupplier" ] == $rs->fields( "idsupplier" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			$html .= "
			<option value=\"" . $rs->fields( "idsupplier" ) . "\"$selected>" . $rs->fields( "name" ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}

    $html .= "
	</select>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function getCountryName( $idstate ){
	
	if( !$idstate )
		return "";
		
	$db = DBUtil::getConnection();
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT name$lang AS name FROM state WHERE idstate = $idstate LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function updateComments(){
	
	if( !isset( $_REQUEST[ "idsupplier" ] ) || empty( $_REQUEST[ "idsupplier" ] ) )
		return;
		
	$db = DBUtil::getConnection();
	
	$idsupplier = intval( $_REQUEST[ "idsupplier" ] );
	
	$query = "UPDATE supplier SET comment = '" . $_POST[ "comment" ] . "' WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_register_comment") );
	
	?>
	<p style="text-align:center;" class="msg">
		<?php echo  Dictionnary::translate("gest_com_comment_register"); ?>
		
	</p>
	<?php
		
}

//--------------------------------------------------------------------------------------------------

function getSupplierInfos( &$supplierInfos ){
	
	$db = DBUtil::getConnection();
	
	//récupérer les infos du fournisseur
	
	if( isset( $_REQUEST[ "idsupplier" ] ) && !empty( $_REQUEST[ "idsupplier" ] ) ){
		
		$idsupplier = intval( $_REQUEST[ "idsupplier" ] );
		
		$query = "SELECT * FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( Dictionnary::translate("gest_com_impossible_recover_supplier_information") );
		
	
		$i = 0;
		while( $i < count( $rs->fields ) ){
		
			$field = $rs->FetchField( $i );
			$fieldname = $field->name;
			$value = $rs->fields( $fieldname );
			
			$supplierInfos[ $fieldname ] = $value;
			
			$i++;
			
		}
					
		$supplierInfos[ "comment" ] = isset( $_POST[ "comment" ] ) ? stripslashes( $_POST[ "comment" ] ) : $rs->fields( "comment" );
		
	}
	else $supplierInfos[ "comment" ] = isset( $_POST[ "comment" ] ) ? stripslashes( $_POST[ "comment" ] ) : "";
	
}

//--------------------------------------------------------------------------------------------------

function getManagerTitle( $idtitle ){
	
	if( empty( $idtitle ) )
		return "";
		
	$lang = User::getInstance()->getLang();
	$db = DBUtil::getConnection();
	
	$query = "SELECT title$lang AS title FROM title WHERE idtitle = $idtitle LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "title" );
	
}

//--------------------------------------------------------------------------------------------------

function getPaymentTxt( $idpayment ){
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = User::getInstance()->getLang();
	$db = DBUtil::getConnection();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function getPaymentDelayTxt( $idpayment ){
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = User::getInstance()->getLang();
	$db = DBUtil::getConnection();
	
	$query = "SELECT payment_delay$lang AS name FROM payment_delay WHERE idpayment_delay = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function getSelectCharge( $idcharge ){
	
	if( empty( $idcharge ) )
		return "";
		
	$lang = User::getInstance()->getLang();
	$db = DBUtil::getConnection();
	
	$query = "SELECT charge_select$lang AS name FROM charge_select WHERE idcharge_select = $idcharge LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}
//--------------------------------------------------------------------------------------------------

?>
