<?
/**
 * Package commercial
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Resultat recherche commande client B2B
 */
?>
 
<script type="text/javascript">
/* <![CDATA[ */

	/* --------------------------------------------------------------------------------------------- */
	
	function createOrderSlideshow(){
	
		if( $( "#Slideshow" ).length )
			return;
		
		var data = "orders=<?php

			$orders = array();
			
			$rs->MoveFirst();
			while( !$rs->EOF() ){
				
				array_push( $orders, $rs->fields( "idorder" ) );
				
				$rs->MoveNext();
					
			}
			
			echo URLFactory::base64url_encode( serialize( $orders ) );
			
			$orders = NULL;
			
			$rs->MoveFirst();
			
		?>";
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/slideshow/order_slideshow.php?ondestroy=destroyOrderSlideshow",
			async: true,
			cache: false,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger le diaporama" ); },
			success: function( responseText ){

				$( "#tools" ).hide();
				$( "#OrderSearchContainer" ).hide();
				$( responseText ).insertBefore( $( "#OrderSearchContainer" ) );
				
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------ */
	
	function destroyOrderSlideshow(){
	
		$( "#Slideshow" ).remove();
		$( "#OrderSearchContainer" ).show();
		$( "#tools" ).show();

	}

	/* ------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>


<?php

/********************************* Tableau résultat **********************************/
function getOrder ( $rs, $statutView, $date_type ) {
	$con = DBUtil::getConnection();
	$rs->MoveFirst();
?>

	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/search_results_order.php</span>
<?php } ?>

<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>

	<table class="dataTable resultTable sortable" id="ResultOrder<?php echo $statutView ?>">
		<thead>
			<tr>
				<th>Com.</th>
				<?php if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1) { ?>
	            	<th>Conf.<br />récep.</th>
	            <?php } ?>
	             
	            <?php if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1) { ?>
	            	<th>Conf.<br />lect.</th>
	            <?php } ?>
	
				<th>Commande n°</th>
				<th>Devis n°</th>
				<th>Client n°</th>
				<th>Nom<br />Client</th>
				<th>Nom<br />Contact</th>
				<th>Statut</th>
				<th>Date commande</th>
				<th>Total<br />HT</th>
				<th>Total<br />TTC</th>
				<th>Commandes fournisseur n°</th>
				<th>PDF commande</th>
				<th>Offre signée</th>
				<th>Facture n°</th>
				<th>Marge nette</th>
				<th>Marge nette %</th>
			</tr>
		</thead>
		<tbody>
		<?php
		
		$total_amountSum = 0.0;
		$total_amount_htSum = 0.0;
		
		for ($i = 0; $i < $rs->RecordCount(); $i++) {
		
			$Int_IdBuyer = $rs->Fields("idbuyer");
			$Int_Iderp = $rs->Fields("iderp");
			$Str_Company = $rs->Fields("company");
			$Int_IdOrder = $rs->Fields("idorder");
			$Int_IdEstimate = $rs->Fields("idestimate");
			$Int_IdDelivery = $rs->Fields("iddelivery");
			$Int_IdPayment = $rs->Fields("idpayment");
			$Str_Date = $rs->Fields("$date_type");
			$Str_Status = $rs->Fields("status");
			$Str_Name = DBUtil::getDBValue("title_1", "title", "idtitle", $rs->fields("title")) . " " . $rs->Fields("lastname");
			$totalorderht = $rs->Fields("total_amount_ht");
			$totalcharge = $rs->Fields("total_charge");
			$seen = $rs->fields("seen");
			$idsource = $rs->fields("idsource");
			$source = $rs->fields( "source" );
			$initials = $rs->fields("initial");
			$zipcode = $rs->fields("zipcode");
			$style = "";
			$isContact = $rs->fields("contact") == 1;
			$balance_date = $rs->fields("balance_date");
			$statusReceipt = $rs->fields("status_return_receipt");
			$statusNotification = $rs->fields("status_delivery_notification");
			$dpIdregulation = $rs->fields("down_payment_idregulation");
			$cpIdregulation = $rs->fields("cash_payment_idregulation");
			$paid = $rs->fields("paid");
		
			// Affichage en rouge des clients non passé sous managéo
			if( ( strlen($rs->Fields("nbr_etablissement")) > 0 && strlen($rs->Fields("capital")) > 0 ) || $rs->Fields("catalog_right") ==  1 ){
				$style='' ;
			}else{
				$style='style="color:red;"';
			}
		
			/*if ($balance_date != "0000-00-00")
				$Str_Status = "Paid";*/
			
			if ($Str_Status == "ToDo")
				$class = " orangeText";
			elseif ($Str_Status == "InProgress") $class = " blueText";
			elseif ($Str_Status == "DemandInProgress") $class = " blueText";
			else
				$class = "";
			
			$idbillings = Order::getInvoices($Int_IdOrder);
			$bls = Order::getBls($Int_IdOrder);
				
			$mb = $rs->fields( "net_margin_amount" );
			$mbpercent = $rs->fields( "net_margin_rate" ); 
			
			// On controle le statut des commandes fournisseurs
			$isOrderSupplierStandBy = false;
			if( isset($statutView) && $statutView == "CB" ) { 
			// ------- Mise à jour de l'id gestion des Index  #1161
				$query_cb = "SELECT idorder_supplier, status FROM order_supplier WHERE idorder = '$Int_IdOrder'";
				$rs_cb = $con->Execute($query_cb);
				
				if ($rs_cb->RecordCount() > 0) {
					for ($nbOrderSup = 0; $nbOrderSup < $rs_cb->RecordCount(); $nbOrderSup++) {
						if($rs_cb->Fields("status") == "standby") { $isOrderSupplierStandBy = true; }
						$rs_cb->MoveNext();
					}
				} 		
			}
			

	        if( !isset($statutView) || $statutView == "" ||
	        	( isset($statutView) && $statutView == $rs->Fields("status") ) ||
	        	( isset($statutView) && $statutView == "Today" && substr( $rs->fields("conf_order_date"), 0, 10) == date("Y-m-d") && $rs->Fields("status") == "Ordered" ) ||
	        	( isset($statutView) && $statutView == "Today" && substr( $rs->fields("DateHeure"), 0, 10) == date("Y-m-d") && $rs->Fields("status") == "Ordered" ) ||
	        	( isset($statutView) && $statutView == "CB" && $rs->Fields("paid") == "1" && $rs->Fields("payment_idpayment") == "1" && $isOrderSupplierStandBy == true )  
	        ) {
		
			?>
			
			<tr valign="top" class="blackText<?php if( $rs->fields( "isInternalCustomer" ) ) echo " firstDraft"; ?>">
				<td class="lefterCol"><?php echo $initials ?></td>
				
				<?php if(DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 1) { ?>
		        <td>
		        	<?php if($statusReceipt == 1){ ?>
		            	<img src="/images/back_office/content/icons-mailbox-active.png" alt="" title="Une confirmation de reception a été reçue pour la commande"/>
		            <?php } else { ?>
		            	<img src="/images/back_office/content/icons-mailbox-inactive.png" alt="" title="Aucune confirmation de réception n'a été reçue pour la commande"/>
		            <?php } ?>
		        </td>
		        <?php } if(DBUtil::getParameterAdmin( "use_delivery_notification" ) == 1) { ?>
		        <td>
		        	<?php if($statusNotification == 1){ ?>
		            	<img src="/images/back_office/content/icons-mailopen-active.png" alt="" title="Une confirmation de lecture a été reçue pour la commande"/>
		            <?php } else { ?>
		            	<img src="/images/back_office/content/icons-mailopen-inactive.png" alt="" title="Aucune confirmation de lecture n'a été reçue pour la commande"/>
		            <?php } ?>
		        </td>
		        <?php } ?>
				
				<td style="white-space: nowrap;">
					<div style="margin-left:auto; margin-right:auto; text-align:center; width:55px;">
						<?php echo GenerateHTMLForToolTipBox( $Int_IdOrder ) ?>
						<a class="grasBack" href="/sales_force/com_admin_order.php?IdOrder=<?php echo $Int_IdOrder ?>" onclick="window.open(this.href); return false;"><?php echo $Int_IdOrder ?></a>
					</div>
				</td>
				<td><?php echo $Int_IdEstimate == 0 ? "-" : $Int_IdEstimate ?></td>
				<td><a target="_blank" href="/sales_force/contact_buyer.php?type=<?php echo $isContact ? "contact" : "buyer" ?>&key=<?php echo $Int_IdBuyer ?>" <?php echo $style ?>><?php echo isset( $Int_Iderp ) ? $Int_Iderp . "/" . $Int_IdBuyer : $Int_IdBuyer ?></a></td>
				<td><a target="_blank" class="grasBack" href="/sales_force/contact_buyer.php?type=<?php echo $isContact ? "contact" : "buyer" ?>&key=<?php echo $Int_IdBuyer ?>"><?php if(!empty($Str_Company) && $Str_Company != "" && $Str_Company != " "){echo $Str_Company ; }else{ echo $Str_Name ;} ?></a></td>
				<td><a target="_blank" title="<?php echo $rs->Fields("lastname") ?>" href="/sales_force/contact_buyer.php?type=<?php echo $isContact ? "contact" : "buyer" ?>&key=<?php echo $Int_IdBuyer ?>"><?php echo $Str_Name ?></a></td>
				<td class="grasBack<?php echo $class ?>"><?
					echo Dictionnary::translate( $Str_Status );
					if( $paid && $cpIdregulation ){
						echo "<br />payée";
					}
				?>
				</td>
				<td><?php echo usDate2eu( substr( $Str_Date, 0, 10 ) ) ?></td>
				<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $totalorderht ) ?></td>
				<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
				<td>
				<?php
				
				
					if ($Str_Status == "Ordered" || $Str_Status == "Invoiced") {
				// ------- Mise à jour de l'id gestion des Index  #1161
						$query = "SELECT idorder_supplier FROM order_supplier WHERE idorder = '$Int_IdOrder'";
						$rs2 = $con->Execute($query);
				
						if ($rs2->RecordCount() > 0) {
				
							for ($f = 0; $f < $rs2->RecordCount(); $f++) {
				
								$idordersup = $rs2->fields("idorder_supplier");
				?>
								<div style="margin-left:auto; margin-right:auto; text-align:center; width:55px; height:20px;">
									<?php echo GenerateHTMLForOrderSupplierToolTipBox( $idordersup ) ?>
									<a href="/sales_force/supplier_order.php?IdOrder=<?php echo $idordersup ?>" onclick="window.open(this.href); return false;"><?php echo $idordersup ?></a>
								</div>
				<?php
				
				
								$rs2->MoveNext();
				
							}
				
						}
				
					}
				?>
				</td>
				<td>
				<?php if( $Str_Status != "ToDo" ){ ?>
					<a href="/catalog/pdf_order.php?idorder=<?php echo $Int_IdOrder ?>" onclick="window.open(this.href); return false;">
					<img src="/images/back_office/content/pdf_icon.gif" alt="Editer" /></a>
				<?php } ?>
				</td>
				<td>
				<?php
				
					if( strlen( $rs->fields( "offer_signed" ) ) ){
						
						?>
						<a href="<?php echo $GLOBAL_START_URL; ?>/data/order/<?php echo $rs->fields( "offer_signed" ); ?>" onclick="window.open(this.href); return false;">
							<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf_icon.gif" alt="Télécharger l'offre signée" />
						</a>
						<?php	
						
					}
					
				?>
				</td>
				<td>
				<?php
															
					if( count( $idbillings ) ){
																	
						foreach( $idbillings as $value ){
																	
							$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$value' LIMIT 1" );
						if( $rsLitigation->RecordCount() ){
																		
							?>
							<img src="/images/back_office/content/litigation.png" alt="Litige" style="cursor:pointer; vertical-align:middle;" onclick="window.open( '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer' );" />
							<?php
																			
						}
					
						?>
						<a href="/catalog/pdf_invoice.php?idinvoice=<?php echo $value ?>" onclick="window.open(this.href); return false;"><?php echo $value ?></a><br />
						<?php
					
					
						}
					
					}
					
				?>  
				</td>
				<td>
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<?php echo Util::priceFormat( $mb ) ?>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> / 	 
				<span style="color:red !important;"><?php echo Util::priceFormat( ($mbpercent - $_SESSION['rate']) * $totalorderht / 100 ) ?></span>
				<?php } ?>
				
				</td>
				<td class="righterCol">
				<?php if ( $_SESSION['salesman_user'] == 0 ) {?>
				<?php echo Util::rateFormat( $mbpercent ) ?>
				<?php } ?>
				<?php if($akilae_comext ==1){ ?> / 
				<span style="color:red !important;"><?php echo Util::rateFormat( $mbpercent - $_SESSION['rate'] ) ?></span>
				<?php } ?>
				</td>      
								
			</tr>
			<?php } //viewTodo ?>
			<?php
					
				if( $Str_Status != Order::$STATUS_CANCELLED && $Str_Status != Order::$STATUS_REFUSED){
					$total_amountSum += $rs->fields( "total_amount" );
					$total_amount_htSum += $rs->fields( "total_amount_ht" );
				}
					
				$rs->MoveNext();
			
			}
		?>
		</tbody>
		<tfoot>
			<?php  if( !isset($statutView) || $statutView == "" ) { ?>
			<tr>
				<td colspan="10" class="lefterCol"></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $total_amount_htSum ); ?></b></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $total_amountSum ); ?></b></td>
				<td colspan="6" class="righterCol"></td>
			</tr>
			<?php } ?>
		</tfoot>
	</table>
<?php
}
/********************************* Tableau résultat **********************************/
function getNbOrder ( $rs, $statutView, $date_type ) {
	$con = DBUtil::getConnection();
			
	//Parcours de chacune des lignes de résultat
	$rs->MoveFirst();
	$return = 0;

	for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
		

		$Str_Status = $rs->Fields("status");
		$Str_Date = $rs->Fields("$date_type");
		        
        if( $statutView == $Str_Status ) {
        	$return++;
        }
        
        // Commande du jour
       
	        	
        
        
        
        if(  ( $statutView == "Today" && substr( $rs->fields("conf_order_date"), 0, 10) == date("Y-m-d") && $rs->Fields("status") == "Ordered" ) ||
	    ( $statutView == "Today" && substr( $rs->fields("DateHeure"), 0, 10) == date("Y-m-d") && $rs->Fields("status") == "Ordered" ) ) {
        	$return++;
        }
        
        //Commande CB non confirmé au(x) fournisseur(s)
		$isOrderSupplierStandBy = false;
		if( $statutView == "CB" ) { 
		// ------- Mise à jour de l'id gestion des Index  #1161
			$query_cb = "SELECT idorder_supplier, status FROM order_supplier WHERE idorder = '".$rs->Fields("idorder")."'";
			$rs_cb = $con->Execute($query_cb);
			
			if ($rs_cb->RecordCount() > 0) {
				for ($nbOrderSup = 0; $nbOrderSup < $rs_cb->RecordCount(); $nbOrderSup++) {
					if($rs_cb->Fields("status") == "standby") { $isOrderSupplierStandBy = true; }
					$rs_cb->MoveNext();
				}
			} 		
		}
        
        if( $statutView == "CB" && $isOrderSupplierStandBy == true && $rs->Fields("paid") == "1" && $rs->Fields("payment_idpayment") == "1" ) {
        	$return++;	
        } 

        $rs->MoveNext();
				
	}
	
	return $return;	
}
?>