<?php

/**
 * Package planning
 * Created on 08 Aout 2017
 * @author jfCorbé
 * Ajout et modification planning
*/

include_once( dirname( __FILE__ ) . "/../../config/init.php" );

//-----------------------------------------------------------------------------------
$idcontact_follow = 0;
 if( isset($_REQUEST['save'])){
        
         $idcontact_follow = intval( $_POST['idcontact_follow']);
         $iduser = User::getInstance()->getId();
         $username  = 'admin';
         
         // Validation heure début et fin
         $datetime = new DateTime(); 
         $strDate1 = date('Y-m-d' ).$_POST['starttime_event'];
         $strDate2 = date('Y-m-d' ).$_POST['endtime_event'];
        
         $datetime1= $datetime->createFromFormat('Y-m-d H:i',$strDate1 );
         $datetime2= $datetime->createFromFormat('Y-m-d H:i',$strDate2 );
   
        $interval  = date_diff ($datetime1,$datetime2);
          // var_dump($interval) ;
        if( $interval->invert == 1){
             echo json_encode("Erreur: Les horaires sont invalides");
        }
        else{
    
         // Bloquage de juxtaposition des RDV
             
         // 1 - Heure début 
         $query = " SELECT  c2.idcontact_follow , c2.starttime_event , c2.endtime_event FROM  contact_follow c2
          WHERE c2.date_event=".DBUtil::quote($_POST['date_event']). "  AND c2.iduser=".$iduser . "  
           AND c2.starttime_event <= ".DBUtil::quote($_POST['starttime_event']) ." AND  c2.endtime_event >= ".DBUtil::quote($_POST['starttime_event']) ;
         
         if( $idcontact_follow > 0){
            
            $query .= " AND  c2.idcontact_follow <>".intval($idcontact_follow);
         }
         
         $rs = DBUtil::query($query );
      
    	 if( $rs === false )
    		die( " Erreur : V&eacute;rification juxtaposition des RDV");
         
         if( $rs->RecordCount()> 0){
            $record = $rs->fields;
            echo json_encode("Erreur: Il y a une juxtaposition des événements entre  ".format_time(trim($record['starttime_event'])). " à ".format_time($record['endtime_event']));
         }
         else{
            
            // 2 - Heure terminé       
             $query = " SELECT  c2.idcontact_follow , c2.starttime_event , c2.endtime_event FROM  contact_follow c2
              WHERE c2.date_event=".DBUtil::quote($_POST['date_event']). "  AND c2.iduser=".$iduser . "  
               AND c2.starttime_event <= ".DBUtil::quote($_POST['endtime_event']) ." AND  c2.endtime_event >= ".DBUtil::quote($_POST['endtime_event']) ;
             
              if( $idcontact_follow > 0){
            
                $query .= " AND  c2.idcontact_follow <>".intval($idcontact_follow);
              }
              
             $rs = DBUtil::query($query );
            
        	 if( $rs === false )
        		die( " Erreur : V&eacute;rification juxtaposition des RDV");
             
             if( $rs->RecordCount()> 0){
                $record = $rs->fields;
                echo json_encode("Erreur: Il y a une juxtaposition des événements entre  ".format_time(trim($record['starttime_event'])). " à ".format_time($record['endtime_event']));
             }
             else{
                
                 // 3 - Heure terminé  et début      
                 $query = " SELECT  c2.idcontact_follow , c2.starttime_event , c2.endtime_event FROM  contact_follow c2
                  WHERE c2.date_event=".DBUtil::quote($_POST['date_event']). "  AND c2.iduser=".$iduser . "  
                   AND c2.starttime_event <= ".DBUtil::quote($_POST['starttime_event']) ." AND  c2.endtime_event >= ".DBUtil::quote($_POST['endtime_event']) ;
                 
                  if( $idcontact_follow > 0){
            
                    $query .= " AND  c2.idcontact_follow <>".intval($idcontact_follow);
                  }
                  
                 $rs = DBUtil::query($query );
                
            	 if( $rs === false )
            		die( " Erreur : V&eacute;rification juxtaposition des RDV");
                 
                 if( $rs->RecordCount()> 0){
                    $record = $rs->fields;
                    echo json_encode("Erreur: Il y a une juxtaposition des événements entre  ".format_time(trim($record['starttime_event'])). " à ".format_time($record['endtime_event']));
                 }
                 else{
      
        
                    $q = " INSERT INTO `contact_follow`( `idbuyer`, `iduser`, `idcontact`,  `idcontact_cause`, `idtype_follow`, `comment`, `date_creation`,   date_event,starttime_event,endtime_event,username) 
                    VALUES (".DBUtil::quote($_POST['idbuyer']).",".$iduser.",".intval($_POST['idcontact']).",". intval($_POST['idcontact_cause']).",".intval($_POST['idtype_follow']).",". DBUtil::quote($_POST['comment']).",". DBUtil::quote(date('Y-m-d h:i:s')).",".DBUtil::quote($_POST['date_event']).",".DBUtil::quote($_POST['starttime_event']).",".DBUtil::quote($_POST['endtime_event']).",".DBUtil::quote($username).")";
                 
                     if( $idcontact_follow > 0){
                     
                        $q= " UPDATE contact_follow SET idbuyer=".DBUtil::quote($_POST['idbuyer']).", idcontact=".intval($_POST['idcontact']).", idcontact_cause=".intval($_POST['idcontact_cause']). ", idtype_follow=".intval($_POST['idtype_follow']). ", comment=".DBUtil::quote($_POST['comment']). ", starttime_event = ".DBUtil::quote($_POST['starttime_event']).", endtime_event = ".DBUtil::quote($_POST['endtime_event']). ", lastupdate=".DBUtil::quote(date('Y-m-d h:i:s'));  
                        $q .= " WHERE idcontact_follow =".intval( $_POST['idcontact_follow']);
                     }
                
                    $rs =  DBUtil::query($q);
                    echo json_encode('OK');
               }
        }
     }
   }
     
 }
//--------------------------------------------------------------------------------------
else
 if( isset($_REQUEST['checking'])){
 
    $response = array('result'=>'NONE','contacts'=>array());
   
    $query = " SELECT idbuyer FROM buyer  WHERE idbuyer=". DBUtil::quote($_POST['idbuyer']);
    $rs = DBUtil::query($query );
      
	 if( $rs === false )
		die( " Erreur : r&eacutecup&eacute buyer");
     if( $rs->RecordCount() )
       $response['result']= 'OK';
     else 
        $response['result']= 'N° Client :'.$_POST['idbuyer'] . ' est  inconnu!';
        
     if(  $response['result']== 'OK'){
        
         $query = " SELECT c.idcontact,c.idbuyer ,c.lastname,c.firstname , title_1 FROM `contact`  c
         LEFT JOIN  title ON idtitle = c.title WHERE idbuyer=". DBUtil::quote($_POST['idbuyer']). " ORDER BY  c.firstname " ;
         $rs = DBUtil::query($query );
         
         if( $rs === false )
		  die( " Erreur : r&eacutecup&eacute buyer");
          
         if( $rs->RecordCount() > 0){
            
             while( !$rs->EOF() ){
	        
    			$response['contacts'][] =  $rs->fields;	
    			$rs->MoveNext();
    		}
         }
    }
    
    echo json_encode($response);
		
 }
 //---------------------------------------------------------------------------------------------
else
if( !isset($_REQUEST['save'])){
         
   	 $LstHours = list_times();
     $LstTypeFollow = array();
     $query = " SELECT * FROM `type_follow` WHERE idtype_follow IN (1,2) order by `type_follow_1`";
     $rs = DBUtil::query($query );
      
	 if( $rs === false )
		die( " Erreur : r&eacutecup&eacute de la liste des type follow");
		
	   if( $rs->RecordCount() ){
		
		$i = 0;
		while( !$rs->EOF() ){
	        
			$LstTypeFollow[] =  $rs->fields;	
			$rs->MoveNext();
		}
    }  
    
    $LstCauseContact = array();
     $query = " SELECT * FROM `contact_cause`  ORDER BY cause_1  ";
     $rs = DBUtil::query($query );
      
	 if( $rs === false )
		die( " Erreur : r&eacutecup&eacute de la liste des causes du contact");
		
	   if( $rs->RecordCount() > 0 ){
		
		$i = 0;
		while( !$rs->EOF() ){
	        
			$LstCauseContact[] =  $rs->fields;	
			$rs->MoveNext();
		}
    }  
    
     $LstContact = array();
     
    
    
    $obj = new stdClass;
    $obj->idcontact_follow = 0;
    $obj->date_event = null;
    $obj->startime_event = null;
    $obj->endtime_event = null;
    $obj->idtype_follow = null;
    $obj->idbuyer = null;
    $obj->idcontact = null;
    $obj->comment = null;
    $obj->idcontact_cause= null;
   
    $StrDate = date('Y-m-d');
    
    if( isset($_GET['dt'])){
      $StrDate = $_GET['dt'];
    } 
    
    $datetime = new DateTime();
     
    $items =  explode('-',$StrDate);
   
    if( count($items) == 3) {
    
     $StrFormatedDate = intval($items[0]).'-'.intval($items[1]).'-'.intval($items[2]);
    }
    
    $obj->date_event= $datetime->createFromFormat('Y-m-d',$StrFormatedDate );
    
  
    if(isset($_REQUEST['edit'])){
        
               
        if( isset($_POST['idcontact_follow']))$idcontact_follow = intval($_POST['idcontact_follow']);
         
        $query = " SELECT * FROM contact_follow  WHERE  idcontact_follow = ".$idcontact_follow;
         $rs = DBUtil::query($query );
      
	   if( $rs === false )
		die( " Erreur : r&eacutecup&eacute contact_follow");
		
	   if( $rs->RecordCount() > 0  ){
	       
            $record = $rs->fields;
            $obj->date_event  = $datetime->createFromFormat('Y-m-d', $record['date_event']);
    	    $obj->startime_event = trim($record['starttime_event']);
            $obj->endtime_event = $record['endtime_event'];
            $obj->idtype_follow = $record['idtype_follow'];
            $obj->idbuyer =  $record['idbuyer'];
            $obj->idcontact = $record['idcontact'];
            $obj->comment = $record['comment'];
            $obj->idcontact_cause= $record['idcontact_cause'];
       }
       
        if( $obj->idbuyer!=null){
          $query = " SELECT c.idcontact,c.idbuyer ,c.lastname,c.firstname , title_1 FROM `contact`  c
             LEFT JOIN  title ON idtitle = c.title WHERE idbuyer=". DBUtil::quote($obj->idbuyer). " ORDER BY  c.firstname " ;
             $rs = DBUtil::query($query );
             
          if( $rs === false )
    		die( " Erreur : r&eacutecup&eacute de la liste des contacts");
    		
    	   if( $rs->RecordCount() > 0 ){
    		
    		$i = 0;
    		while( !$rs->EOF() ){
    	        
    			$LstContact[] =  $rs->fields;	
    			$rs->MoveNext();
    		}
          }
      }
    }
   
     $schedule = DBUtil::getParameterAdmin("schedule");
     //$schedule = 'NORMAL';
    ?>
    
    	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/planning_form.php</span>
<?php } ?>

    <table >
        <tr><td><h4>Date d'événement:</h4></td><td ><h4><?=$obj->date_event->format('d M y')?><input  type="hidden" value="<?=$obj->date_event->format('Y-m-d')?>" id="date_event" />
       </h4></td></tr>
       <tr><td>Heures:</td><td>
        <select id="starttime_event" style="width:75px!important;">
        <?
        
         $notDisplayed = array();
         if( strtoupper($schedule == 'NORMAL'))
           $notToDisplay = array('12:00','12:30','13:00','13:30') ;
      
         foreach($LstHours as $hour):
          
           $attrib2 = substr($obj->startime_event, 0, strlen($hour)) === $hour ? 'selected' : ''; 
        ?>
        <?php  if(!in_array($hour,$notToDisplay)):?>
       <option value="<?=$hour?>" <?=$attrib2?> ><?=$hour?></option>*
       <?php endif ?>
    <?endforeach ?>
      
     </select> à <select id="endtime_event" style="width: 75px!important;">
    <?php unset($LstHours[0]);
       $notDisplayed2 = array();
         if( strtoupper($schedule == 'NORMAL'))
           $notToDisplay2 = array('12:30','13:00','13:30') ;
      
       foreach($LstHours as $hour):
       $attrib = substr($obj->endtime_event, 0, strlen($hour)) === $hour? 'selected' : '';
        ?>
     <?php  if(!in_array($hour,$notToDisplay2)):?>
       <option value="<?=$hour?>" <?=$attrib?>><?=$hour?></option>
        <?php endif ?>
    <?endforeach ?>
      
     </select></td></tr>
        <tr><td>Type d'événement:</td><td >
        <select id="idtype_follow" style="width: 350px!important;">
      <?php 
      foreach($LstTypeFollow as $item):
       $attrib = $obj->idtype_follow == $item['idtype_follow'] ? 'selected' : ''; 
       ?>
       <option value="<?=$item['idtype_follow']?>" <?=$attrib?> ><?=$item['type_follow_1']?></option>
      <?php endforeach ?>
      </select>
      </td>
       </tr>
     
     <tr><td>Motif</td>
     <td >
     <select id="idcontact_cause" style="width: 350px!important;">
     <option value=""></option>
      <?php 
      foreach($LstCauseContact as $item):
       $attrib = $obj->idcontact_cause == $item['idcontact_cause'] ? 'selected' : ''; 
      ?>
       <option value="<?=$item['idcontact_cause']?>"  <?=$attrib?> ><?=$item['cause_1']?></option>
       <?php  endforeach ?>
      </select>
     </td></tr>
     <tr><td>
     N° Client:</td>
     <td ><input type="text" id="idbuyer" style="width: 100px;" value="<?=$obj->idbuyer?>"  />
    
     </td></tr>
     <tr><td>
      Contact:</td>
     <td ><select id="idcontact" style="width:350px!important;">
     <option value="0"> - </option>
    <?php foreach($LstContact as $item):
     $attrib = $obj->idcontact == $item['idcontact'] ? 'selected' : ''; 
    ?>    
       <option value="<?=$item['idcontact']?>" <?=$attrib?> ><?=$item['title_1']?> <?=$item['firstname']?>  <?=$item['lastname']?></option>
    <?endforeach ?>
       
     </select>
     </td></tr>
     <tr><td style="text-align: top; padding-top: 15px!important;">
      Commentaire:</td>
     <td ><?php if( $obj->comment!=null):?>
     <textarea rows="5" cols="60" id="comment"><?=$obj->comment?></textarea> 
        <?php else:?>
        <textarea rows="5" cols="60" id="comment"><?=$obj->comment?></textarea>
    <?php endif ?>
     </td></tr>
      <tr><td colspan="4" style="text-align: right; padding-top: 15px!important;"><input type="button" value=" Sauvegarder " id="btnSave" class="blueButton"/> 
      <!--<input type="button" value=" Annuler "  class="blueButton"/>--></td>
      </tr>
    </table>
      <input type="hidden" id="idcontact_follow" value="<?=$idcontact_follow?>" />
<?php } 


 function  format_time($strTime){
    
    $items = explode(':', $strTime);
    if( count($items)>=3)
    return $items[0].':'.$items[1];
    else return '';
 }
 
  function list_times(){
    
     $morning = array('08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00');
     $timeoff = array('12:30','13:00','13:30');
     $afternoon = array('14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00');
   	 
     $schedule = DBUtil::getParameterAdmin("schedule");
     //$schedule = 'NORMAL';
     
     $times = array();
     if(strtoupper($schedule)=='NORMAL') {        
        $times = array_merge($morning,$afternoon);
     }
     else{
      
        $times = array_merge($morning,$timeoff,$afternoon);
     }     
     return $times;
 }
 ?>