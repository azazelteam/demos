<?php

/* ----------------------------------------------------------------------------------------------------------- */
include_once( dirname( __FILE__ ) . "/../../../objects/ProductDocument.php" );
?>
<script type="text/javascript"> 
/* <![CDATA[ */
	
	/* Recalcule les infos du devis */
	function recalEstimate() {
		//window.setTimeout('recalEst()',3000); //3000
	}	
	
	function recalEst() {
		
		var data = $('#frm').formSerialize(); 
		
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_devis.php?recalEstimate&idestimate=<?php echo $IdEstimate; ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ },
		 	success: function( responseText ){
		 	
		 		//Affichage des elements
				var arr = responseText.split('&&');
				for (var i=0; i<arr.length; i++) {
					var key = arr[i].split('=')

					if(key[0] != "") {
						
						$("#"+key[0]).html(key[1]); 
						$("#"+key[0]).attr("value", key[1])
						
					}
				}
		 	
				//$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				//$.blockUI.defaults.css.fontSize = '12px';
	        	//$.growlUI( '', 'Mise à jour du devis:'+responseText ); //+responseText 	

			}
		});
	}
	
	
	
	/* Affiche la pop up d'info sur le fournisseur */
	function showSupplierInfos( idsupplier) {	
		if( idsupplier == '0' )
			return;
	
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
				$.blockUI({
					message: msg,
					fadeIn: 700, 
	        		fadeOut: 700,
					css: {
						width: '700px',	top: '0px', left: '50%', 'margin-left': '-350px', 'margin-top': '50px',
						padding: '5px', '-webkit-border-radius': '10px',  '-moz-border-radius': '10px',
		                'background-color': '#ECEAE5' 
					 }
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			}
		});
	}
	
	
	/* Affiche la pop up d'info sur l'article */
	
/*	function showArticleInfos( idarticle) {
		var msg = document.getElementById('popupComments').innerHTML;
		$.blockUI({
			message: msg,
			fadeIn: 700, 
    		fadeOut: 700	 
		}); 
		
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
		setTimeout('$.unblockUI;',2000);
		
		
	}*/

	function showArticleInfos( idarticle) {	
		if( idarticle == '0' )
			return;
	
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos_article.php?idarticle=" + idarticle,
		
			async: false,
		 	success: function(msg){
				$.blockUI({
					message: msg,
					fadeIn: 700, 
	        		fadeOut: 700,
					css: {
						width: '700px',	top: '0px', left: '50%', 'margin-left': '-350px', 'margin-top': '50px',
						padding: '5px', '-webkit-border-radius': '10px',  '-moz-border-radius': '10px',
		                'background-color': '#ECEAE5' 
					 }
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			}
		});
	}
	
	
	
	function changeSupplierCharges( idsupplier , amount ){
	
		var elementID = 'SupplierChargesAuto_' + idsupplier;
		var element = document.getElementById( elementID );
						
		if(element.value==1){
			setSupplierChargesAuto(idsupplier,amount);
		}else{
			restoreSupplierCharges(idsupplier);
		}
	
	}

	function changeInternalSupplierCharges( idsupplier , amount ){
	
		var elementID = 'InternalSupplierChargesAuto_' + idsupplier;
		var element = document.getElementById( elementID );
						
		if(element.value==1){
			setInternalSupplierChargesAuto(idsupplier,amount);
		}else{
			restoreInternalSupplierCharges(idsupplier);
		}
	
	}

	function setSupplierChargesAuto( idsupplier, amount ){
	
		var $element = $( "#supplier_charges_" + idsupplier );
		
		$element.attr( "backup", $element.val() );
		$element.val( amount );
		$element.attr( "disabled", "disabled" );
		
	}
	
	function updateCharges( selectMenu, amount ){
		
		var total_charge_auto = document.forms.frm.elements[ 'total_charge_auto' ];
		
		if( total_charge_auto.options[ total_charge_auto.selectedIndex ].innerHTML == 'Départ usine' ){
			
			document.forms.frm.elements[ 'total_charge_ht' ].value = 'Départ usine';
			document.forms.frm.elements[ 'total_charge_ht' ].disabled = 'disabled';
			document.forms.frm.elements[ 'ex_factory' ].value = '1';
			document.forms.frm.elements[ 'charge_free' ].value = '0';
			
			return;
		
		}

		document.forms.frm.elements[ 'ex_factory' ].value = '0';
		
		if( total_charge_auto.options[ total_charge_auto.selectedIndex ].innerHTML == 'Offert' ){
		
			document.forms.frm.elements[ 'total_charge_ht' ].value = 'Offert';
			document.forms.frm.elements[ 'total_charge_ht' ].disabled = 'disabled';
			document.forms.frm.elements[ 'charge_free' ].value = '1';
			document.forms.frm.elements[ 'ex_factory' ].value = '0';
			
			return;
		
		}

		document.forms.frm.elements[ 'charge_free' ].value = '0';
		
		if( selectMenu.value == 0 )
			restoreCharges();
		else
			setChargesAuto( amount );
		
	}
	
	function setChargesAuto( amount ){
	
		var elementID = 'total_charge_ht';
		var element = document.getElementById( "frm" ).elements[ elementID ];
		
		var total_charge_auto = document.forms.frm.elements[ 'total_charge_auto' ];
		
		if( total_charge_auto.options[ total_charge_auto.selectedIndex ].innerHTML != 'Départ usine' )
			element.backup = element.value;
			
		element.value = amount != "Franco" && amount > 0.0 ? amount : "Franco";
		element.disabled = 'disabled';
		
		
	}
	
	function restoreCharges(){
	
		var elementID = 'total_charge_ht';
		var element = document.getElementById( "frm" ).elements[ elementID ];
		
		if( element.backup != null )
			element.value = element.backup;
		
		element.disabled = '';
		
	}
	
	function setInternalSupplierChargesAuto( idsupplier, amount ){
	
		var elementID = 'internal_supplier_charges_' + idsupplier;
		var element = document.getElementById( "frm" ).elements[ elementID ];
		
		element.backup = element.value;
		element.value = amount;
		element.disabled = 'disabled';
		
	}

	function restoreSupplierCharges( idsupplier ){
	
		var $element = $( '#supplier_charges_' + idsupplier );
		
		if( $element.attr( "backup" ) != null )
			$element.val( $element.attr( "backup" ) );
		
		$element.attr( "disabled", "" );
		
	}
	
	function restoreInternalSupplierCharges( idsupplier ){
	
		var elementID = 'internal_supplier_charges_' + idsupplier;
		var element = document.getElementById( "frm" ).elements[ elementID ];
		
		if( element.backup != null )
			element.value = element.backup;
		
		element.disabled = '';
		
	}
	
	function substitute( idrow ){
	
		var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=600, height=750';
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/substitute.php?idestimate=<?php echo $IdEstimate ?>&idrow=' + idrow;
		
		window.open( location, '_blank', options ) ;
		
	}

	/* ------------------------------------------------------------------------------------------------------------------------- */
			
	function showButton(){
		
		var checked = 0;
		$( ".comparative" ).each( function( i ){
		
			if( this.checked )
				checked++;
				
		} );

		$( "#btn_comparer" ).css( "display", checked > 1 ? "inline" : "none" );

	}
	
	/* ------------------------------------------------------------------------------------------------------------------------- */
	
	function confirmSendMail(){
		
		if( checkRelaunchDate() == false )
			return false;
		
		if( confirm( 'Confirmez l\'envoi du mail ?' ) ){
			//document.getElementById( 'sendmail' ).value=  '1';
			
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/sales_force/mail_estimate.php?IdEstimate=<?=$IdEstimate?>",
				async: false,
			 	success: function(msg){
			 		
					$.blockUI({
						
						message: msg,
						fadeIn: 700, 
		        		fadeOut: 700,
						css: {
							width: '700px',
							top: '0px',
							left: '50%',
							'margin-left': '-350px',
							'margin-top': '50px',
							padding: '5px', 
							cursor: 'help',
							'-webkit-border-radius': '10px', 
			                '-moz-border-radius': '10px',
			                'background-color': '#FFFFFF',
			                'font-size': '11px',
			                'font-family': 'Arial, Helvetica, sans-serif',
			                'color': '#44474E'
						 }
						 
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);

					setTimeout('$.unblockUI; $( "#frm" ).submit();',2000);
					
				}
		
			});				
			
			return true;
		}else{ 
			//document.getElementById( 'sendmail' ).value=  '0';
			return false;
		}
		
	}
	
	
	function confirmPaiement(){
	
		if( confirm( 'Confirmer le paiement de la commande ?' ) )
			return true;
		
	}
	
	function confirmSendFax(){
	
			postop = (self.screen.height-300)/2;
			posleft = (self.screen.width-100)/2;
			window.open('fax_estimate.php?id=<?=$IdEstimate?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=300,height=100,resizable,scrollbars=yes,status=0');
		
	}
	
	function checkRelaunchDate(){
		
		if( !document.getElementById( "relaunch" ) || document.getElementById( "relaunch" ).value == "" || document.getElementById( "relaunch" ).value == "0000-00-00" ){
			
			alert( "Vous devez saisir une date de relance." );
			
			return false;
			
		}else{
			
			return true;
			
		}
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------------------- */
	
	function getInsuranceConfirmation(){
		return null;
		
		var insurance = $( "#insurance" ).val();
		
		return <?php
	
			$idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) );
			$customer 			=& $Order->getCustomer();
            $minimum 			= floatval( DBUtil::getDBValue( "minimum", "credit_insurance", "idcredit_insurance", $idcredit_insurance ) );
			$maximum 			= floatval( DBUtil::getDBValue( "maximum", "credit_insurance", "idcredit_insurance", $idcredit_insurance ) );
			$netBillET 			= round( $Order->getNetBill() / ( 1.0 + $Order->getVATRate() / 100.0 ), 2 );
			$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();

			if( $netBillET < $minimum || $netBillET > $maximum )
				echo "null";
			else if( !$customer->get( "insurance" ) || $customer->get( "insurance_status" ) != "Accepted" )
				echo "'Le client n\'a pas d\'assurance crédit. Etes-vous certains de vouloir confirmer la commande?'";
			else if( !$Order->get( "insurance" ) )
				echo "insurance != 0 && insurance != null ? null : 'Le montant de la commande ne sera pas assuré. Etes-vous certains de vouloir confirmer la commande?'";
			else if( $outstandingCredits + $netBillET > $customer->get( "insurance_amount" ) )
				echo "'L\'encours assurance du client ne permettra pas de couvrir le montant commande. Etes-vous certains de vouloir confirmer la commande?'";
			else echo "null";

		?>;
			
	}
	
	/* ------------------------------------------------------------------------------------------------------------------------- */
	
	function confirmSendOrder(){
	<?php

		if( $idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) ) ){ //assurance activée
		
			?>
			/* vérification de l'encours assurance */
			
			var insuranceConfirmation = null;
			
			if( !document.getElementById( 'frm' ).elements[ 'cash_payment' ].checked )
				insuranceConfirmation = getInsuranceConfirmation();
		
			if( insuranceConfirmation && !confirm( insuranceConfirmation ) )
				return;
			
			<?php
		
		}
		
		?>
		if( confirm( '<?php echo addslashes( Dictionnary::translate("gest_com_send_mail_order") ) ?> ?' ) ){
			document.getElementById( 'sendmail' ).value=  '1';
		}else{
			document.getElementById( 'sendmail' ).value=  '0';
		} 
	
		document.getElementById( "frm" ).elements['Estimate2Order'].value= '1';
		document.getElementById( "frm" ).submit();	
		
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------------------- */
	
	function confirmOrder(){
	<?php

		if( $Order->get( "insurance" ) && $idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) ) ){ //assurance activée
		
			?>
			/* vérification de l'encours assurance */
			
			var insuranceConfirmation = null;
			
			if( !document.getElementById( 'frm' ).elements[ 'cash_payment' ].checked )
				insuranceConfirmation = getInsuranceConfirmation();
		
			if( insuranceConfirmation && !confirm( insuranceConfirmation ) )
				return;
			
			<?php
		
		}
		
		?>

		if( confirm( '<?php echo addslashes( Dictionnary::translate("gest_com_transform_estimate_order") ) ?> ?' ) ){
			document.getElementById( "frm" ).elements['Estimate2Order'].value= '1';
		} 
	
		
		return true;
		
		
	}

	/* ------------------------------------------------------------------------------------------------------------------------- */
	
	function confirmDemanInProgress(){
	<?php

		if( $Order->get( "insurance" ) && $idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) ) ){ //assurance activée
		
			?>
			/* vérification de l'encours assurance */
			
			var insuranceConfirmation = null;
			
			if( !document.getElementById( 'frm' ).elements[ 'cash_payment' ].checked )
				insuranceConfirmation = getInsuranceConfirmation();
		
			if( insuranceConfirmation && !confirm( insuranceConfirmation ) )
				return false;
			
			<?php
		
		}
		
		?>

		if( confirm( '<?php echo addslashes( Dictionnary::translate("gest_com_transform_estimate_order") ) ?> ?' ) ){
		
			document.getElementById( "frm" ).elements['Estimate2DemandInProgress'].value= '1';
			
			if( confirm( "Envoyer la demande de confirmation par courriel?" ) )
				document.getElementById( "frm" ).elements['MailDemandInProgress'].value= '1';
				
			return true;
			
		} 
	
		
		return false;
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------------------- */
	
	function confirmProForma(){
	
		
			if( confirm( "Confirmer l'envoi de la facture Proforma ?" ) ){
				//document.getElementById( 'sendmail' ).value=  '1';
				
				$.ajax({

					url: "<?php echo $GLOBAL_START_URL ?>/sales_force/mail_proforma.php?IdEstimate=<?=$IdEstimate?>",
					async: false,
				 	success: function(msg){
				 		
						$.blockUI({
							
							message: msg,
							fadeIn: 700, 
			        		fadeOut: 700,
							css: {
								width: '700px',
								top: '0px',
								left: '50%',
								'margin-left': '-350px',
								'margin-top': '50px',
								padding: '5px', 
								cursor: 'help',
								'-webkit-border-radius': '10px', 
				                '-moz-border-radius': '10px',
				                'background-color': '#FFFFFF',
				                'font-size': '11px',
				                'font-family': 'Arial, Helvetica, sans-serif',
				                'color': '#44474E'
							 }
							 
						}); 
						
						$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
						
						window.location.href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?=$IdEstimate?>";
						
						setTimeout('$.unblockUI;',2000);
						
					}
			
				});				
			
			}else{
				document.getElementById( 'sendmail' ).value=  '0';
				
				
				
			} 
			
			//document.getElementById( "frm" ).elements['Estimate2ProForma'].value= '1';
		
			return true;	
	}
	
	function confrefuse(){
		
		postop = (self.screen.height-300)/2;
		posleft = (self.screen.width-100)/2;
		window.open('motif_refus.php?id=<?=$IdEstimate?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=300,height=100,resizable,scrollbars=yes,status=0');
		
	}
	
	function confdevis(f){
		
		f.form.terminate.value=1;
		return false;
		
	}
	
	function confdeviswmail(){
		
		if( confirm( 'Confirmer le devis ?' ) ){
			
			document.getElementById( "frm" ).terminate.value=1;
			return false;
			
		}
		
	}
	
	function confsupp(cpt,nom){
		
		var ret = confirm("<?php echo addslashes( Dictionnary::translate("gest_com_confirm_delete") ) ?> : "+ nom);
		
		if (ret == true){
			
			document.getElementById( "frm" ).elements['Delete_'+cpt].value=1;
			document.getElementById( "frm" ).submit();
			
		}
		
		return ret;
		
	}
	
	function confsuppPJ(ff){
		
		var ret = confirm("<?php echo addslashes( Dictionnary::translate("gest_com_confirm_delete_link") ) ?> ?");
		
		if (ret == true)
			ff.form.submit();
		
		return ret;
		
	}
	
	// PopUp pour sélectionner un article existant dans la BD
	function catsel(){
	
		var options ='directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=900, height=650';
		var location = 'cat_select.php?IdEstimate=<?php echo $IdEstimate ?>';

		window.open( location , '_blank', options ) ;
		//window.open( 'cat_select.php?IdEstimate=<?php echo $IdEstimate ?>', '_blank', options ) ;
	}
	
	// PopUp pour sélectionner les options de l'article existant dans la BD
	function optionssel(id){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-750)/2;
		window.open('options_select.php?IdEstimate=<?php echo $IdEstimate ?>&IdArt='+id, '_blank', 'top=' + postop + ',left=' + posleft + ',width=750,height=600,resizable,scrollbars=yes,status=0');
		
	}
	
	// PopUp pour sélectionner les produits similaires de l'article existant dans la BD
	function similarsel(id){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-950)/2;
		window.open('similar_select.php?IdEstimate=<?php echo $IdEstimate ?>&IdArt='+id, '_blank', 'top=' + postop + ',left=' + posleft + ',width=950,height=600,resizable,scrollbars=yes,status=0');
		
	}
	
	// PopUp pour sélectionner un article existant dans la BD
	function catsel_test(){
		
		h = (self.screen.height-100);
		w = (self.screen.width-100);
		postop = (h-100);
		posleft = (w-100);
		window.open('../catalog/product.php?fromscript=com_admin_devis.php', '_blank', 'top=' + postop + ',left=' + posleft + ',width=600,height=600,resizable,scrollbars=yes,status=0');
		
	}
	
	// PopUp pour ajouter un nouvel article
	function newArt(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-700)/2;
		window.open('new_article.php?fromscript=com_admin_devis.php<?php echo urlencode( "?IdEstimate=$IdEstimate" ) ?>&IdEstimate=<?php echo $IdEstimate ?>', '_blank', 'top=' + (self.screen.height-600)/2 + ',left=' + (self.screen.width-700)/2 + ',width=650,height=600,resizable,scrollbars=yes,status=0');
		
	}
	
	function displayCatalog(){
	
		postop = 40;
		posleft = 40;
		window.open('<?php echo $GLOBAL_START_URL ?>', '_blank', 'top=40 ,left=40 ,width=800,height=600,resizable,scrollbars=yes,status=0');
	
	}
	
	function editArt(idrow){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-600)/2;
		window.open('new_article.php?idrow='+idrow,'edit_article','top='+postop+',left='+posleft+',width=600,height=600,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
		window.open('new_article.php?fromscript=<?php echo urlencode( "?IdEstimate=$IdEstimate" ) ?>&IdEstimate=<?php echo $IdEstimate ?>&idrow='+ idrow, '_blank', 'top=' + postop + ',left=' + posleft + ',width=600,height=600,resizable,scrollbars=yes,status=0');
		
	}
	
	function selectReference(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-600)/2;
		window.open('ref_select.php?IdEstimate=<?php echo $IdEstimate ?>', '_blank', 'top=' + (self.screen.height-600)/2 + ',left=' + (self.screen.width-600)/2 + ',width=800,height=600,resizable,scrollbars=yes,status=0');
		
	}
	
	function addSelectedReferences( selection ){
	
		alert( selection.length );
		var serializedIds = '';
		var i = 0;
		while( i < selection.length ){
		
			if( i )
				selection += ',';
				
			serializedIds += selection[ i ];
			
			i++;
			
		}
		
		document.location = 'sales_force?references=' + serializedIds;
		
	}
	
	// PopUp pour les infos des remises au volume
	function PopupVolume(ref,buy){
		
		postop = (self.screen.height-150)/2;
		posleft = (self.screen.width-300)/2;
	
		window.open('volume_price.php?r='+ref+'&b='+buy, '_blank', 'top=' + postop + ',left=' + posleft + ',width=300,height=150,resizable,scrollbars=yes,status=0');
		
	}
	
	// PopUp pour les infos du stock
	function PopupStock(ref){
		
		postop = (self.screen.height-400)/2;
		posleft = (self.screen.width-400)/2;
	
		window.open('popup_stock.htm.php?r='+ref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=400,resizable,scrollbars=yes,status=0');
		
	}
	
	// PopUp pour les commentaires
	function PopupComments(idest){
		var msg = document.getElementById('popupComments').innerHTML;
		$.blockUI({
			message: msg,
			fadeIn: 700, 
    		fadeOut: 700	 
		}); 
		
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
		setTimeout('$.unblockUI;',2000);
		
		
	}

	function checkQuantities(){
	
		var i = 0;
		while( i < <?php echo $Order->getItemCount() ?> ){
		
			var quantity = document.getElementById( "frm" ).elements[ 'qtt_' + i ].value;
			var min_cde = document.getElementById( "frm" ).elements[ 'min_cde_' + i ].value;
			var reference = document.getElementById( "frm" ).elements[ 'reference_' + i ].value;
			
			if( !isNumeric( quantity ) ){
				
				var message = '<?php echo str_replace( "'", "\'", Dictionnary::translate( "gest_com_invalid_quantity" ) ) ?>';
				message = message.replace( '%reference', reference );
				
				alert( message );
				
				return false;
				
			}
			
			var int_min_cde = parseInt( min_cde );
			var int_quantity = parseInt( quantity );
			
			if( int_quantity < int_min_cde ){
	
				var message = '<?php echo str_replace( "'", "\'", Dictionnary::translate( "gest_com_insufficient_quantity" ) ) ?>';
				message = message.replace( '%reference', reference );
				message = message.replace( '%min_cde', min_cde );
				
				alert( message );
				
				document.getElementById( "frm" ).elements[ 'qtt_' + i ].value = int_min_cde;
				
				return false;
				
			}
			
			i++;
			
		}
		
		return true;
		
	}
	
	/* Retourne true si la chaine est un chiffre */
	function isNumeric( text ){
	
		var validChars = '0123456789';
		var char;
		
		var i = 0;
		while( i < text.length ){
	   
	   		char = text.charAt( i );
	   		
	   		if ( validChars.indexOf( char ) == -1 ) 
	   			return false;
	   			
			i++;
	   	
		}
	   	
	   	return true;
	
	}		

	
	/* ------------------------------------------------------------------------------------------------- */
	
	function updateEstimate( property, value ){
		
		var data = "property=" + escape( property ) + "&value=" + escape( value );
		
		
		
		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_devis.php?update&idestimate=<?php echo $Order->getId(); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sauvegarder les modifications" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Impossible de sauvegarder les modifications : " + responseText ); 
					return;
					
				}
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );
    				
			}

		});
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function updateEstimateItemAt( index, property, value ){
		
		var data = "index=" + index + "&property=" + escape( property ) + "&value=" + escape( value );
				
		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_devis.php?update&idestimate=<?php echo $Order->getId(); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sauvegarder les modifications" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Impossible de sauvegarder les modifications : " + responseText ); 
					return;
					
				}
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );
    				
			}

		});
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	var colorIndex = 0;
	
	/* ------------------------------------------------------------------------------------------------- */
			
	function colorDialog( itemIndex, idproduct ){

		colorIndex = itemIndex;
		
		$.ajax({
		 	
			url: "/product_management/catalog/colors.php?callback=addColor&idproduct=" + idproduct,
			async: true,
			type: "GET",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les couleurs" ); },
		 	success: function( responseText ){

				$( "#ColorDialog" ).html( responseText );
				$( "#ColorDialog" ).dialog({
			        
					modal: true,	
					title: "Couleurs disponibles",
					close: function(event, ui) { 
					
						$("#ColorDialog").dialog( "destroy" );
						
					},
					width: 600,
					height: 600
					
				});
				
			}

		});

	}

	/* ------------------------------------------------------------------------------------------------- */
	
	function addColor( idcolor ){
		
		$( "#idcolor" + colorIndex ).val( idcolor );
		$( "#frm" ).submit();
		
	}

	/* ------------------------------------------------------------------------------------------------- */
	
	function addDeleteDocument( add , iddocument , idestimate ){

		todoaction = 'delete_document';
		if( add ){
			todoaction = 'add_document';
		}

		var postDatas = "idestimate=" + idestimate + "&iddocument=" + iddocument;

		$.ajax({
		 	
			url: "/sales_force/estimate_parseform.php?" + todoaction,
			async: true,
			type: "POST",
			data: postDatas,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter le document" ); },
		 	success: function( responseText ){
			 	if(responseText=='1'){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
		        	$.growlUI( '', 'Modifications enregistrées' );
			 	}else{
					alert( responseText );
			 	}			
			}

		});		
		
	}
	
	
/* ]]> */
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<div class="centerMax">
	<?php 
	
	
	if( $Order->getCustomer()->getId() == DBUtil::getParameterAdmin( "internal_customer" ) )
		include( "$GLOBAL_START_PATH/templates/sales_force/estimate/virtual_buyer.htm.php" );
	else include( "$GLOBAL_START_PATH/templates/sales_force/estimate/buyer.htm.php" );
	
	anchor( "Delete_", true );
	anchor( "UpdateUnitCostRate_", true );
	anchor( "UpdateRefDiscountRate_", true );
	anchor( "Updqte_", true );
	anchor( "UpdateUnitCostAmount_", true );
	anchor( "UpdateEstimatePriceWD_", true );
	anchor( "Updextqte_", true ); 
	anchor( "UpdateDesignation_", true ); 
	anchor( "UpdateSupplierCharges_", true );
	anchor( "UpdateInternalSupplierCharges_", true ); 
	anchor( "UpdRow_", true ); 
	
	
	?>
	
	
	<script type="text/javascript">
	<!--
		//onglets
		$(function() {
			$('#containerEstimate').tabs({ fxFade: true, fxSpeed: 'fast' });
		});
	-->
	</script>
	<div class="spacer"></div>
	
<div class="contentResult" style="margin-bottom: 5px;">

	<h1 class="titleEstimate"><span class="textTitle">Rechercher un produit</span>
	<div class="spacer"></div></h1>
	<div class="spacer"></div>       
	<a name="references"></a>
	
	<div class="blocEstimateResult" style="margin:0; -moz-border-radius-topleft: 0px ! important;"><div style="margin:5px;">
	

	<div id="containerEstimate">
		
		<ul class="menu" style="display:<?php echo strlen( $disabled ) ? "none" : "block"; ?>;">
            <li class="item"><a href="#add-ref"><span>Ajouter une référénce</span></a></li>
            <li class="item"><a href="#search-product"><span>Rechercher par critères</span></a></li>
            <li class="item"><a href="#create-product"><span>Créer un produit</span></a></li>
            <li class="item"><a href="#search-cata" onclick="window.open('<?php echo URLFactory::getHostURL(); ?>'); return false;"><span>Rechercher sur le catalogue</span></a></li>
            <!-- <li class="item"><a href="#" onclick="window.open('ref_select.php?IdEstimate=<?php echo $IdEstimate ?>', '_blank', 'top=' + (self.screen.height-600)/2 + ',left=' + (self.screen.width-600)/2 + ',width=800,height=600,resizable,scrollbars=yes,status=0');">
            <span>Par référence</span></a></li>
            </li> -->
        </ul>
        <div style="border-top: 1px solid rgb(208, 208, 208); margin: 3px 0 8px -5px; width: 101%;"> </div>
		<div class="spacer"></div>
		
		
		<!-- ********************* -->
		<!--   Créer un produit    -->
		<!-- ********************* -->
		<div id="create-product">
			<script type="text/javascript"> 
			/* <![CDATA[ */
				function addProduct() {
					
					var data = $('#frmCreate').formSerialize(); 
					$.ajax({
						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/new_article.php?IdEstimate=<?php echo $IdEstimate; ?>",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de rechercher un produit" ); },
					 	success: function( responseText ){
					 	    $("#responseAddProduct").html(responseText); 		
						}
					});
				}
			/* ]]> */
			</script>	
			
			<div class="blocMultiple blocMLeft">
			<form id="frmCreate" action="/sales_force/com_admin_devis.php?IdEstimate=<?php echo $Order->getId() ?>" method="post" enctype="multipart/form-data">
				<input type="hidden" name="IdEstimate" value="<?php echo $IdEstimate ?>" />
				<input type="hidden" name="AddReference" value="On" />
			
				<label><span><?php echo Dictionnary::translate('type') ?></span></label>
				<div class="simpleSelectCreate"><select name="type">
					<option value="alternativ"><?php echo Dictionnary::translate('alternative') ?></option>
					<option value="specific"><?php echo Dictionnary::translate('specific') ?></option>
				</select></div>
				
				<input type="button" onclick="return addProduct();" style="margin:5px 0;" class="blueButton floatright" value="Sélectionner" name="AddReference" />
			</form></div>	
			<div class="spacer"></div>
			<div id="responseAddProduct"></div>
			<div class="spacer"></div>
			
		</div>
		<div class="spacer"></div>
		

		<!-- ********************* -->
		<!-- Ajouter une référénce -->
		<!-- ********************* -->
		<div id="add-ref" style="display:none;">
			
			<form action="/sales_force/com_admin_devis.php?IdEstimate=<?php echo $Order->getId() ?>" method="post" id="frmAdd" enctype="multipart/form-data">

				<?php if( !strlen( $disabled ) ){ ?>
		            <div class="blocMultiple blocMLeft" style="width:85%">
	            		<label style="width:23%"><span><?php echo Dictionnary::translate("add_product_estimate") ?></span></label>
	            		<span class="textSimple" style="margin:0;">
							<input<?php echo $disabled; ?> type="text" name="directref" id="directref" class="textInput" style="width:100px; margin:0;" />
							<style type="text/css">

								#as_directref ul{ width:250px;}
								#as_directref .as_header,
								#as_directref .as_footer{ width:238px;}
								
							</style>
							<?php
							
								include_once( dirname( __FILE__ ) . "/../../../objects/AutoCompletor.php" );
            					AutoCompletor::completeFromDBPattern( "directref", "detail", "reference", "{summary_1}", 15 );
            	
            				?>
							<span class="floatleft" style="margin:5px 3px 0 3px; "> x </span>
							<input<?php echo $disabled; ?> type="text" name="directqte" value="1" class="textInput" style="width:20px; margin:0;" />
		            		<input<?php echo $disabled; ?> type="submit" name="AddDirectRef" class="blueButton" value="Ajouter" style="margin: 0 0 0 3px;" />
		            		<input type="button" class="blueButton" name="btn_comparer" id="btn_comparer" value="Comparer" onclick="window.open( '<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_devis.php?compare&idestimate=<?php echo $Order->getId(); ?>' );" style="margin:5px 3px 0 3px; display:<?php
								$p = 0;
								$checked = 0;
								while( !$displayed && $p < $Order->getItemCount() ){
									if( $Order->getItemAt( $p)->get( "comparative" ) )
										$checked++;
										
									$p++;	
								}
								echo $checked > 1  ? "inline" : "none";
								
							?>;" />
						</span>
					</div>
		            <div class="spacer"></div>      
				<?php } ?>	
			</form>	
			<div class="spacer"></div>	
							
		</div><!-- #add-ref -->
		<div class="spacer"></div>
		
		
		<!-- ********************* -->
		<!-- Rechercher  catalogue -->
		<!-- ********************* -->
		<div id="search-cata" style="display:none;"></div><!-- #search-cata -->
		<div class="spacer"></div>		
		
		
		<!-- ********************* -->
		<!-- Rechercher un produit -->
		<!-- ********************* -->
		<div id="search-product" style="display:none;">
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function addReference( idarticle ){

					document.location = "/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate; ?>&IdProduct=" + idarticle;
					
				}
				
			/* ]]> */
			</script>
			<?php 
		
			include( dirname( __FILE__ ) . "/../../../sales_force/reference_search.php" ); 
			displaySearchForm( "addReference" );
			
		?>
		</div> <!-- #search-product -->
		<div class="spacer"></div>
	</div> <!-- #containerEstimate -->
	<div class="spacer"></div>
	</div></div>

</div>

<div class="contentResult" style="margin-bottom: 5px;">
	
	<h1 class="titleEstimate"><span class="textTitle">Descriptif du devis</span>
	<div class="spacer"></div></h1>
	<div class="blocEstimateResult" style="margin:0;"><div style="margin:5px;">
	
	<form action="/sales_force/com_admin_devis.php?IdEstimate=<?php echo $Order->getId() ?>#lastaction" method="post" id="frm" enctype="multipart/form-data">
	<input type="hidden" name="Estimate2ProForma" value="0" />
	<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $IdEstimate ?>" />
	<input type="hidden" name="ModifyEstimate" />
	<input type="hidden" name="sendmail" id="sendmail" value="0" />	
	<!-- <input type="hidden" name="sendmailpaiement" id="sendmailpaiement" value="0" /> --> 
	<input type="hidden" name="sendfax" id="sendfax" value="0" />	
	<input type="hidden" name="comparatif" id="comparatif" value="0" />
	<input type="hidden" name="todo" value="<?php echo $Order->get( "todo" ) ?>" />
	 
		
		
			<!-- COMMENTAIRE Logismarket / edgb2b -->
        	<?php 
        	
        	if( strlen($Order->get( "auto_comment" )) > 5 ) { 
      
        		?>
		        <div class="blocMultiple blocMLeft" style="width:65%;">	
		        	<label style="width:100%; background:none;"><span style="margin-left:0;">Commentaires <?php
		        	
		        		if( $Order->get( "idsource" ) != 1 )
		        			echo htmlentities( DBUtil::getDBValue( "source_1", "source", "idsource", $Order->get( "idsource" ) ) );
		        			
		        		?> 
	            		<a href="#" id="autoCommentsDivLink" onclick="visibility('autoCommentsDiv','<?php echo strlen( $Order->get( "auto_comment" ) ) ? "  [Lire commentaires]" : "  [Pas de commentaires]" ?>','[Masquer]'); return false;" class="blueLink">
	    					<?php echo strlen( $Order->get( "auto_comment" ) ) ? "[Masquer]" : "  [Pas de commentaires]" ?>
	    				</a>
		            </span></label>
		            <div class="spacer"></div>	
		             
		            <div id="autoCommentsDiv" style="display:<?php echo strlen( $Order->get( "auto_comment" ) ) ? "block" : "none" ?>">
			        	<textarea readonly="readonly" name="auto_comment" style="width: 100%; height:80px;" class="textInput"><?php echo html_entity_decode ( $Order->get( "auto_comment" ) ) ?></textarea>
			        </div>	
			        <div class="spacer"></div>
			    </div>	        
        		<?php 
        	
        	} 
        	
        	if( in_array( $Order->get( "idsource" ), array( 5, 6, 15, 17 ) ) ) //logimarket, edgb2b et zone industrie
				displayRemoteEstimateControls( $Order );
			        		
        	?>
        	<div class="spacer"></div>
        	
        	<?php if( strlen($Order->get( "comment" )) > 5 ) { ?>
	        	<div class="blocMultiple blocMRight" style="width:100%;">	
			    	<label style="width:100%; background:none;"><span style="margin-left:0;">Commentaires du client</span></label>
		            <div class="spacer"></div>	
		             
		            <div id="clientsCommentsDiv">
		                <textarea disabled="disabled" name="comment" style="width: 100%; height:80px;" class="textInput"><?php echo html_entity_decode ( $Order->get( "comment" ) ) ?></textarea>
		                <div class="spacer"></div>	
		            </div>
		            <div class="spacer"></div>
			    </div>     
		    <?php } ?> 	
			<div class="spacer"></div><br/>
		
			<?php

			$down_payment_msg = 0;
			$phatot=0;
			$n = $Order->getItemCount();
		
			$rows = getIdRowsSortedBySupplier();
			$currentSupplier = 0;
			
			//Gestion de la remise supplémentaire
			$view_ref_discount_add = DBUtil::getParameterAdmin( "view_ref_discount_add");
			
			for ( $k = 0; $k < count( $rows ); $k++ ) {
				
				$i = $rows[ $k ]; //rows triées par fournisseur
				
				$idarticle = $Order->getItemAt($i)->get('idarticle');
				
				$options = GetOptions($idarticle);
				$similar = GetSimilar($idarticle);
				
				$href_pdf = '';
				$icone = '<img src="'. URLFactory::getReferenceImageURI( $Order->getItemAt($i)->get('idarticle'), URLFactory::$IMAGE_ICON_SIZE ) . '" alt="icone" />';
		
				$reference = $Order->getItemAt($i)->get('reference');
				$reference_base = $Order->getItemAt($i)->get('reference_base');
				$phatot=$phatot+$Order->getItemAt($i)->get('unit_cost');
				$buy = $Order->getCustomer()->get('idbuyer_family');
				$VolPrice = IsVolumeDiscounted($reference);
				$alternative = $Order->getItemAt($i)->get('alternative');
				
				$useref=1;
				 
				$madb=DBUtil::getConnection();
				$is_service = 0;
				
				//On récupère l'id product et le name pour faire le lien vers la fiche produit
				$qlink="SELECT p.idproduct, p.name_1 , coalesce(service ,0) is_service FROM product p, detail d WHERE d.idarticle='$idarticle' AND d.idproduct = p.idproduct";
				$rlink=$madb->Execute($qlink);
				
				if($rlink===false)
					die("Impossible de générer le lien vers la fiche produit");
				
				if($rlink->RecordCount()>0){
					$idp = $rlink->fields("idproduct");
					$name = $rlink->fields("name_1");
					
					$link =  URLFactory::getProductURL($idp,$name);
					$is_service = $rlink->fields("is_service");
				}
				
				//$reqfrs="SELECT ref_supplier FROM detail WHERE reference='$reference_base'";
				//$resfrs=$madb->Execute($reqfrs);
				$reffrs= $Order->getItemAt($i)->get('ref_supplier');
				
				//On récupère le type de la référence pour gérer la fiche produit
				if(empty($alternative)){
					$typeref="SELECT type,doc FROM detail WHERE reference='$reference'";
				}else{
					$typeref="SELECT type,doc FROM detail WHERE reference='$reference_base'";	
				}
				$rstype=$madb->Execute($typeref);	
				
				if($Str_status=='Send'){
					//Stock
					if($reference_base==$reference){
						$stock="SELECT stock_level FROM detail WHERE reference='$reference_base'";
						$rsstock=$madb->Execute($stock);	
		
						$stockval = $rsstock->fields("stock_level");
						
						if( $rsstock->RecordCount()>0){
							$msgsto='';
							if($stockval!=-1){
								//L'aricle possède une gestion de stock
								//On contrôle si la quantité interne saisie ne dépasse pas le stock
								$Qte = $Order->getItemAt( $i)->get( "quantity" )-$Order->getItemAt( $i)->get( "external_quantity" );
								if($Qte>$stockval){
									$msgsto= Dictionnary::translate("gest_com_stock_not_enough_for_reference") . $reference . Dictionnary::translate("gest_com_impossible_continue") . $stockval . Dictionnary::translate("pieces");
									$warning=1;
							}
						}
					}else{
						$stockval=-1;
					}
					}else{
						$stockval=-1;
					}	
					
				}
				
				if (!empty($idarticle)) {
					
					// Cas des articles existants dans le catalogue en ligne
					$usepdf = $Order->getItemAt($i)->get('usepdf');
					$checked = '';
					if (!empty($usepdf)) {
						$checked = ' checked="checked"';
					}
					$drc = "'".Dictionnary::translate("help")."','".Dictionnary::translate("help_insert")."'";
					
					//si ce n'est pas un produit catalogue, on regarde si il y a une documentation'
					
					if($rstype->Fields('type')!='catalog'){
						$useref=0;
						$mondoc=$rstype->Fields('doc');
						$down_payment_msg = 1;
						
						if($mondoc!=''){
							$href_apdf = '<a href="..'.$mondoc.'" onclick="window.open(this.href); return false;" class="blueLink">' .
							'<img src="'.$GLOBAL_START_URL.'/images/back_office/content/pdf.gif" alt="PDF" /> Voir la fiche Pdf</a>';
							$href_pdf="<input$disabled type=\"checkbox\" class=\"valignCenter\" name=\"usepdf_$i\" value=\"1\"$checked />";
						}
					}else{
						$useref=1;
						$IdProduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) );
						$href_aodt = '<a class="blueLink" href="/catalog/odt_product.php?idestimate='.$IdProduct.'" onclick="window.open(this.href); return false;">' .
							'<img src="'.$GLOBAL_START_URL.'/images/back_office/content/pdf.gif" alt="ODT" /> Voir la fiche Odt</a>';
						
			
						$href_apdf = '<a class="blueLink" href="'. URLFactory::getPDFURL( DBUtil::getDBValue( "idproduct", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) ) ).'" onclick="window.open(this.href); return false;">' .
							'<img src="'.$GLOBAL_START_URL.'/images/back_office/content/pdf.gif" alt="PDF" /> Voir la fiche Pdf</a>';
							
						//Si il ya un num de devis on récupère le choix utilisateur
						$href_pdf="<input$disabled type=\"checkbox\" class=\"valignCenter\" name=\"usepdf_$i\" value=\"1\"$checked />"; 
					}
					
				} 

				//@todo :o(
				$des_edit=stripslashes($Order->getItemAt($i)->get('designation'));
                //$des_edit=html_entity_decode($des_edit);
				//$des_edit=html_entity_decode($des_edit);
                $des_edit = str_replace("\n",'', $des_edit);
				$des_edit = str_replace("\r",'', $des_edit);
				$des_edit = str_replace('<br />',"\n", $des_edit);
				$des_edit = str_replace('<br />',"\n", $des_edit);
				$des_edit = strip_tags($des_edit);
			
				//frais de port fournisseur
			
				$quantity = $Order->getItemAt( $i)->get( "quantity" );
				
				//Calcul des marges
				//Marge initiale
				
				if(($Order->getItemAt($i)->get('unit_cost_amount')>0)AND($Order->getItemAt( $i )->get( "discount_price" )>0)){
						
						if( B2B_STRATEGY ){ 
							 $mbi = ( 1- ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $quantity + $supplier_charges ) / ( $Order->getItemAt( $i)->get( 'unit_price' ) * $quantity ) ) ) * 100;
			 				} 
				else{ 
				$mbi = $Order->getItemAt( $i)->get( 'unit_price' ) > 0.0 ? ( 1- ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) ) / ( $Order->getItemAt( $i)->get( 'unit_price' )/ ( 1.0 + $Order->getItemAt( $i )->getVATRate() / 100.0 ) ) ) ) * 100 : 0.0;
				 } 
				$mbi = Util::numberFormat( $mbi );
				
				}else{
					
					$mbi='0';
				
				}
			
				//Marge finale
				
				if(($Order->getItemAt($i)->get('unit_cost_amount')>0)AND($Order->getItemAt( $i )->get( "discount_price" )>0)){
					
					if( B2B_STRATEGY ){ 
			 			$mbf = ( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $quantity + $supplier_charges ) / ( $Order->getItemAt( $i )->get( "discount_price" ) * $quantity ) ) ) * 100;
						
						$mbf_com = (( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $quantity + $supplier_charges ) / ( $Order->getItemAt( $i )->get( "discount_price" ) * $quantity ) ) ) * 100) - $_SESSION['rate'] ;
					} 
					  
				else{ 
						$mbf = ( 1 - ( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) ) / ( $Order->getItemAt( $i )->get( "discount_price" )/ ( 1.0 + $Order->getItemAt( $i )->getVATRate() / 100.0 ) ) ) ) * 100;
	         } 
					
					$mbf = Util::numberFormat( $mbf );
					$mbf_com = Util::numberFormat( $mbf_com );
					
					
				if( B2B_STRATEGY ){ 
			$mprice_ht = ( $Order->getItemAt( $i )->get( "discount_price" ) * $quantity ) ;
			  } 
				else{ 
			$mprice_ht = ( $Order->getItemAt( $i )->get( "discount_price" )/ ( 1.0 + $Order->getItemAt( $i )->getVATRate() / 100.0 ) ) ;
	         } 
								
					$mprice_ht = Util::numberFormat( $mprice_ht );
					$vatRate = $Order->getItemAt( $i )->getVATRate() ;

				}else{
					
					$mbf='100';
				
				}
			
				//Marge numérique
				
				if( ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) > 0 ) && ( $Order->getItemAt( $i )->get( "discount_price" ) > 0 ) ){
					
								 if( B2B_STRATEGY ){ 
									$mbn = ( $Order->getItemAt( $i )->get( "discount_price" ) * $Order->getItemAt( $i)->get('quantity' ) ) - ( $Order->getItemAt( $i)->get( 'unit_cost_amount' ) * $Order->getItemAt( $i)->get( 'quantity' ) );
																	
								} 
				$mbn = Util::numberFormat( $mbn );
			
				}else{
					
				$mbn = $Order->getItemAt( $i)->get( 'discount_price' );	
				
				}	
				
				if( $hasLot ){
					$colspan=11;
				}else{
					$colspan=10;
				}
				
				//Affichage du message de stock
				if($Str_status=='Send'){
					if( isset( $msgsto ) &&  strlen($msgsto)>0){
						?>
						<tr>
						<td colspan="<?=$colspan?>"><div class="msg" align="center"><?=$msgsto?></div></td>
						</tr>
						<?		
					}
				}
				
							
				
	// Ligne produit négoce
           if( $is_service == 0){ ?>	
		   
			<?php $akilae_comext = DBUtil::getParameterAdmin("akilae_comext"); ?>
			
            <div class="floatleft" style="width:89%;margin-bottom:10px;">

                    <table class="dataTable devisTable">
                        <tr>
                            <th style="width:75px;">Photo</th>
                            <th>Réf. &amp; Designation</th>
                            <th style="width:75px; height:36px;">
                            	
                            	<?php if($Order->getItemAt($i)->get('quantity_per_truck') > 0 || $Order->getItemAt($i)->get('quantity_per_pallet') > 0 || $Order->getItemAt($i)->get('quantity_per_linear_meter') > 0) { ?>
                            	<style type="text/css">
                            		.tooltipfrshover2 { background-color:#FFFFFF; border:1px solid lightgray; text-align:left; padding:0 5px;
										color:#8B9298; display:block; left:70px; position:absolute; top:-7px; width:170px; z-index:99;
									}
                            	</style>
                            	<span onmouseover="document.getElementById('popfrs<?php echo $k ?>').className='tooltipfrshover2';" onmouseout="document.getElementById('popfrs<?php echo $k ?>').className='tooltipfrs';" style=" position:relative; cursor:pointer; ">
	                            	Quantité <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" onmouse style="border-style: none; vertical-align: text-bottom;" alt="" />
	                            	
	                            	<span id='popfrs<?php echo $k ?>' class='tooltipfrs' style="font-size:11px;">
	                            	<?php if($Order->getItemAt($i)->get('quantity_per_linear_meter') > 0) { ?>
	                            		Quantité par ML : <?php echo $Order->getItemAt($i)->get('quantity_per_linear_meter') ?><br />
	                            	<?php } ?>
	                            	<?php if($Order->getItemAt($i)->get('quantity_per_truck') > 0) { ?>
	                            		Quantité par camion : <?php echo $Order->getItemAt($i)->get('quantity_per_truck') ?><br />
	                            	<?php } ?>
	                            	<?php if($Order->getItemAt($i)->get('quantity_per_pallet') > 0) { ?>
	                            		Quantité par palette : <?php echo $Order->getItemAt($i)->get('quantity_per_pallet') ?><br />
	                            	<?php } ?></span>
	                            </span>
                            	<?php } else { ?>
                            		Quantité                            	
                            	<?php } ?>
                            </th>
                            <th>Prix vente<br />tarif unitaire	<?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
                            <th>Remise<br />client %</th>
                            <th>Prix vente<br />net unitaire <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?> </th>
                            <?php
                            /*<th>Remise client<br />supplémentaire</th>
                            <th>Prix unitaire<br />Vente net final</th>*/
                            ?>
                            <th>Prix vente<br />total <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
                        </tr>
                        <tr>
                            <td rowspan="4" style="vertical-align:top;">
							
								<a href="<?php echo $link ?>" onclick="window.open(this.href); return false;"><?php echo $icone ?></a>
								<?php
								
								if( $Order->getItemAt( $i )->get( "hasPromo" ) ){
	
									?>
									<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/promo.gif" alt="En promotion" /></p>
									<script type="text/javascript">
								      
									    $(document).ready(function(){
									        $("#helpPromo_<?php echo $i ?>").qtip({
									            adjust: {
									                mouse: true,
									                screen: true
									            },
												content: 'Ce produit est en promotion<br />Promotion de <?php echo Util::rateFormat( $Order->getItemAt( $i )->get( "ref_discount_base" ) ) ?>',
									            position: {
									                corner: {
									                    target: "rightMiddle",
									                    tooltip: "leftMiddle"
									                }
									            },
									            style: {
									                background: "#FFFFFF",
									                border: {
									                    color: "#EB6A0A",
									                    radius: 5
									                },
									                fontFamily: "Arial,Helvetica,sans-serif",
									                fontSize: "13px",
									                tip: true
									            }
									        });
									    });
									

									</script>
									<?php
									
								}
								
								?>
								<div class="spacer"></div><br/>
								<div class="leftContainer" >
									<?
									$UseImg=$Order->getItemAt( $i)->get("useimg");
									
									$selected='';
									
									if($UseImg==1){
										$selected =  ' checked="checked"';
									}
									
									?>
									
									<input type="checkbox" class="valignCenter" name="useimg_<?php echo $i ?>" value="1"<?php echo $selected ?> />
	                            </div>
								<div class="leftContainer blueText" style="text-align:left;margin-left:5px;">Util. photo</div>
								
								<input type="hidden" id="min_cde_<?php echo $i ?>" value="<?php echo $Order->getItemAt( $i)->get( "min_cde" ) ?>" />
								<input type="hidden" id="reference_<?php echo $i ?>" value="<?php echo $Order->getItemAt( $i)->get( "reference" ) ?>" />
								<input type="hidden" name="Delete_<?php echo $i ?>" value="0" />

<div class="spacer"></div><br/>

						                           Ordre PDF
                        <input style=" width:50px; " onfocus="this.select()" onkeyup="recalEstimate();" class="textInput price" type="text" name="idrow_line_<?php echo $i ?>" value="<?php echo  $Order->getItemAt( $i)->get("idrow_line") ?>" />
                           
                           
                            </td>
                            <td style="text-align:left;">
                                Référence catalogue : <a href="<?php echo $link ?>" onclick="window.open(this.href); return false;"><?php echo $Order->getItemAt( $i)->get( 'reference' ) ?></a>
                                <br />
								<?php
								
								displayColor( $Order->getItems()->get( $i ), strlen( $disabled ) == 0 );
								
								$idsupplier = $Order->getItemAt( $i)->get( "idsupplier" );
								
								if( $idsupplier ){
									
									?>
									<span class="lightGrayText">Réf. fournisseur (<?php echo DBUtil::getDBValue( "name", "supplier", "idsupplier", $idsupplier ) ?>) : <?php echo $reffrs ?></span>
									<span class="lightGrayText" style="position:relative;">&nbsp;<a href="#" onclick="showSupplierInfos( <?php echo $idsupplier ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" /></a></span>
	                                <?php 
	    
										if( isset( $href_apdf ) ){ 
	                                	
	                                		?><span style="lightGrayText; display:block;" ><?php echo $href_apdf ?></span><?php
	                                	
	                                	} 
										if( isset( $href_aodt ) ){ 
	                                	
	                                		?><span style="lightGrayText; display:block;" ><?php echo $href_aodt ?></span><?php
	                                	
	                                	} 
								
								}
								
							?>
                            </td>
                            <!-- Qté -->
                            <td>                             	
								<input<?php echo $disabled ?> onfocus="this.select()" onkeyup="recalEstimate();" class="textInput quantity" type="text" name="qtt_<?php echo $i ?>" value="<?php echo $Order->getItemAt($i)->get('quantity') ?>" style="text-align:center; width:50px;" />
								<input type="hidden" name="old_qtt_<?php echo $i ?>" value="<?php echo $Order->getItemAt($i)->get('quantity') ?>" />
                            </td>
							
							<!-- P.U -->
                            <td>
								<input<?php echo $disabled ?> style=" width:50px; " id="pvu_<?php echo $i ?>" onfocus="this.select()" onkeyup="recalEstimate();" class="textInput price" type="text" name="unit_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get('unit_price' )) ?>" />&nbsp;&euro;
								<input type="hidden" id="old_pvu_<?php echo $i ?>" name="old_unit_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get('unit_price' ))?>" />
                            </td>
                            
                            <!-- Remise Client -->	                            
                            <td>
								<input<?php echo $disabled ?> style="text-align:center; width:50px;" id="rc_<?php echo $i ?>" onfocus="this.select()" onkeyup="recalEstimate();" class="textInput percentage" type="text" name="ref_discount_<?php echo $i ?>" id="helpPromo_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount' ) ) ?>"<?php echo $Order->getItemAt( $i )->get( "hasPromo" ) ? " style=\"background-color:#FF8484;\"" : "" ?> />&nbsp;%
								<input type="hidden" id="old_rc_<?php echo $i ?>" name="old_ref_discount_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount' ) ) ?>" />
                            </td>
                            
                            <!-- P.U Vente Net -->
                            <td>
                            	<input<?php echo $disabled ?> style=" width:50px; " id="pvnetu_<?php echo $i ?>" onfocus="this.select()" onkeyup="recalEstimate();" type="text" class="textInput price" name="estimate_priceWD_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get( "discount_price" ) ); ?>" /> &euro;
								<input type="hidden" id="old_pvnetu_<?php echo $i ?>" name="old_estimate_priceWD_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get( "discount_price" ) ); ?>" />
                            </td>
                            
                            <!-- Prix de Vente Total -->
                            <td>
                            	<span id="pvnet_<?php echo $i ?>"><?php echo Util::priceFormat( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" )) ?></span>
                            </td>
                        </tr> 
                        <tr>
                            <td rowspan="3" style="text-align:left; width:210px">
                                <textarea<?php echo $disabled ?> name="designation_<?php echo $i ?>" cols="" rows="" style="width:210px; height:110px; border-style:none;"><?php echo $des_edit ?></textarea>
                            </td>
                            
                            <!-- tx remise fs -->
							<th style="height:36px;">
								Prix vente fabricant HT
							</th>
                            <th style="width:125px;">Prix achat<br />remisé HT</th>
                            <th style="width:125px;">Remise supplémentaire<br />fournisseur %</th>
                            <th style="width:125px;">Prix achat<br />net unitaire HT</th>
                            <?php
                            /*<th>Remise fourn<br />supplémentaire</th>
                            <th>Prix unitaire<br />Achat net final</th>*/
                            ?>
                            <th style="width:125px;">Prix achat<br />total HT</th>
                        </tr>	                        
                        <tr>
                            <td style="height:33px;">
									<?php echo Util::priceFormat(DBUtil::getDBValue( "suppliercost", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) )) ?>
						
							                          
                            <?php
								
								
								$idarticle = $Order->getItemAt($i)->get('idarticle');
							//echo $idarticle;
								$q = "SELECT idarticle FROM quantity_price WHERE idarticle = '" . $idarticle."' ";
	$rs = DBUtil::query($q);
       if ( $rs->RecordCount() > 0 ) {
	
								
									
									?>
									
									<a href="#" onclick="showArticleInfos( <?php echo $idarticle ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/fleche.jpg" alt="" style="border-style:none; vertical-align:text-bottom;" /></a>
                        <?php    } ?>
                            
                            
                            </td>
                            <td>
							<?php if ( $_SESSION['salesman_user'] == 0  ) {?> 
							<?php echo Util::priceFormat($Order->getItemAt($i)->get('unit_cost')) ?>

							<?php } ?>
							<?php if($akilae_comext ==1){ ?>
							<br><br>
							<span style="color:red !important;"><?php echo Util::priceFormat($Order->getItemAt($i)->get('unit_cost') * (1 + $_SESSION['rate'] / 100)) ?></span>
							<?php } ?>
							</td>
                            <td><?php if ( $_SESSION['salesman_user'] == 0  ) {?> 
								<input<?php echo $disabled ?> style="text-align:center; width:50px;" id="rf_<?php echo $i ?>" onfocus="this.select()" onkeyup="return recalEstimate();" type="text" class="textInput percentage" name="unit_cost_rate_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('unit_cost_rate' ) ) ?>" />&nbsp;%
								<input type="hidden" id="old_rf_<?php echo $i ?>" name="old_unit_cost_rate_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('unit_cost_rate' ) ) ?>" />
								
							<?php } ?>
								<?php if($akilae_comext ==1){ ?>
								<br><br>
								<span style="color:red !important;">Vide</span>
							<?php } ?>
                            </td>
                            <td><?php if ( $_SESSION['salesman_user'] == 0 ) {?> 
								<input<?php echo $disabled ?> style=" width:50px; " id="pau_<?php echo $i ?>" onfocus="this.select()" onkeyup="return recalEstimate();" class="textInput price" type="text" name="unit_cost_amount_<?php echo $i ?>" size="6" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get( "unit_cost_amount" )) ?>" />&nbsp;&euro;
								<input type="hidden" id="old_pau_<?php echo $i ?>" name="old_unit_cost_amount_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get( "unit_cost_amount" )) ?>" />
                            	<!--<input type="text" class="textInput price" value="600,00" /> &euro;-->
								<?php } ?>
								<?php if($akilae_comext ==1){ ?>
								<br><br>
								<span style="color:red !important;">Vide</span>
								<?php } ?>
                            </td>
                            <td>
                            	<span id="pa_<?php echo $i ?>">
								<?php if ( $_SESSION['salesman_user'] == 0 ) {?> 
                            	<?php echo Util::priceFormat( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt($i)->get( "unit_cost_amount" ) ) ?></span>
								<?php } ?>
								<?php if($akilae_comext ==1){ ?>
								<br><br>                          	
								<span style="color:red !important;"><?php echo Util::priceFormat( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt($i)->get( "unit_cost_amount" ) * (1 + $_SESSION['rate'] / 100)) ?></span>
								<?php } ?>
                            </td>
                        </tr>
                        <tr>
                            <th style="height:36px;">Stock<br />interne</th>
                            <th>Stock<br />externe</th>
                            <?php
                            /*<th>Stock<br />Fournisseur</th>
                            <th>Stock<br />en commande</th>*/
                            ?>
                            <th>Expédition</th>
                            <th>Poids unitaire</th>
                            <th>Poids total</th>
                        </tr>
                    	<tr>
                        	<td colspan="2" style="height:36px;">
                            
                            
                            <?php
                        
                            if( !empty( $href_pdf ) ){
                            	
                            	?>
	                            <div class="leftContainer" >
	                            	<?php echo $href_pdf; ?>
	                                <!--<input type="checkbox" class="valignCenter" />-->
	                            </div>
	                            <div class="leftContainer blueText" style="text-align:left;margin-left:5px;">
	                                Joindre fiche produit
	                            </div>
	                            <div class="spacer"></div>
	                            <?php
                            }
                            
                            $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) );
                            
                            $documentsProd = ProductDocument::getDocumentsProduct( $idproduct );                 
                            
                            if( $documentsProd ){
                            	
                            	foreach( $documentsProd as $documentProd ){ 
                            		
                            		$checkid = DBUtil::query( "SELECT count(*) as checkid 
                            						FROM estimate_row_product_document 
                            						WHERE unique_id = " . $documentProd->getDocumentUniqueId() . " 
                            						AND idestimate = " . $Order->getItemAt( $i )->get( "idestimate" ) . " LIMIT 1" )->fields('checkid');
                            		
                            		?>
		                            <div class="leftContainer" >
		                                <input <?php echo $checkid ? "checked='checked'" : "" ?> onclick="addDeleteDocument( $(this).attr( 'checked' ) , $(this).val() , <?php echo $IdEstimate ?> )" type="checkbox" name="product_document" class="valignCenter" value="<?php echo $documentProd->getDocumentUniqueId() ?>" />
		                            </div>
		                            <div class="leftContainer blueText" style="text-align:left;margin-left:5px;">
		                                Joindre : <?php echo $documentProd->getDocumentName(); ?>
		                            </div>
		                            <div class="spacer"></div>	                            		
                            		<?php
                            		
                            	}
                            	
                            }
                           
                           	if( count( $rows ) > 1 ){ ?>
                            <div class="leftContainer" style="margin-left:15px;">
                            	<input<?php echo $disabled; ?> type="checkbox"<?php if( $Order->getItemAt( $i )->get( "comparative" ) ) echo " checked=\"checked\""; ?> class="comparative" name="comparative_<?php echo $i ?>" value="<?php $i; ?>" onclick="showButton(); updateEstimateItemAt( '<?php echo $i; ?>', 'comparative', this.checked ? '1' : '0' );" />
                            </div>
                            <div class="leftContainer blueText" style="text-align:left;margin-left:5px;">Ajouter au comparatif</div>
                            <?php } ?>
                            
                        </td>
                        <td>
                        	<!-- stock interne -->
							<?php
							if($Order->getItemAt($i)->get('external_quantity')<$Order->getItemAt($i)->get('quantity')){
								$int_qtt=($Order->getItemAt($i)->get('quantity')-$Order->getItemAt($i)->get('external_quantity'));
							}else{
								$int_qtt="0";
							}
							?>
							<?php echo $int_qtt ?>
                        </td>
                        <td>
                        	<!-- stock externe -->
							<input<?php echo $disabled ?> style="text-align:center; width:50px;" onkeyup="recalEstimate();" class="textInput quantity" type="text" name="ext_qtt_<?php echo $i ?>" size="2" value="<?php echo $Order->getItemAt($i)->get('external_quantity') ?>" />
							<input type="hidden" name="old_ext_qtt_<?php echo $i ?>" value="<?php echo $Order->getItemAt($i)->get('external_quantity') ?>" />
                        </td>
                        <?php
                        /*<td>
                        	<!-- stock fournisseur -->
                        	<input type="text" class="textInput quantity" value="---" />
                        </td>
                        <td>
                        	<!-- stock en commande -->
                        	<input type="text" class="textInput quantity" value="---" />
                        </td>*/
                        ?>
                        <td>
                        	<!-- délai d expédition -->
                            <!--<select class="comboBox">
                                <option>3 jours</option>
                                <option>5 jours</option>
                            </select>-->
							<?
							if(!$global_deliv) {
								DrawLift_Delay( "delivdelay_$i", $Order->getItemAt( $i)->get( "delivdelay" ), empty( $disabled ), "onchange='recalEstimate();'" );
							}
							?>
                        </td>
                        <td>
                        	<!-- poids unitaire -->
							<?					
								$unitweight = $Order->getItemAt( $i)->get( "weight" );
								echo Util::numberFormat( $unitweight ) . " kg"; 
							?>
                        </td>
                        <td>
                        	<!-- poids total -->
							<?
								$totalweight = $Order->getItemAt( $i)->get( "quantity" ) *  $unitweight;
								echo Util::numberFormat( $totalweight ) . " kg"; 
								$x=$i+1;
							?>
                        </td>
                    </tr>
                </table>

				<?
				//liens vers les stats de la référence ( afficher sur les 6 derniers mois )
					
				$StatStartDate 	= date( "d/m/Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) );
				$StatEndDate 	= date( "d/m/Y" );
				$reference 		= $Order->getItemAt( $i)->get("reference" );
                $idproduct = DBUtil::getDBValue( "idproduct", "detail", "reference", $reference );
                //$statURL 		= "$GLOBAL_START_URL/statistics/statistics.php?cat=product&amp;reference=" . urlencode( $reference ) . "&amp;period&amp;StatStartDate=" . urlencode( $StatStartDate ) . "&amp;StatEndDate=" . urlencode( $StatEndDate );
				$statURL 		= "$GLOBAL_START_URL/statistics/statistics.php?cat=product&amp;idproduct=" . urlencode( $idproduct ) . "&amp;period&amp;StatStartDate=" . urlencode( $StatStartDate ) . "&amp;StatEndDate=" . urlencode( $StatEndDate );
				?>
				
                <div class="leftContainer" style="margin-top:2px;">
                	<?php if($options){ ?>
					<a href="#" onclick="optionssel(<?=$idarticle?>); return false;" class="blueButton" style="text-decoration:none;"><b>Options</b></a>
					<?php } ?>
								
					<?php if($similar || DBUtil::getDBValue( "clone_id", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) ) ){ ?>
					<a href="#" onclick="similarsel(<?php echo $Order->getItemAt( $i )->get( "idarticle" ); ?>); return false;" class="blueButton" style="text-decoration:none;"><b>Similaires</b></a>
					<?php } ?>
                
                
                    <input type="button" class="blueButton" value="Supprimer" onclick="confsupp(<?=$i?>,'<?=$Order->getItemAt($i)->get('reference')?>');" />
                    <!--<input type="button" class="blueButton" value="Substituer" style="margin-left:5px;" onclick="substitute( <?php echo $i ?> ); return false;" />-->
                	<input type="button" class="blueButton" value="Visu. site" style="margin-left:5px;" onclick="window.open('<?php echo $link ?>'); return false;" />
                                
                </div>
                <div class="rightContainer" style="margin-top:2px;">
                    <a href="http://www.google.fr/search?q=<?php echo urlencode( $name ) ?>" onclick="window.open(this.href); return false;"><img src="http://images.google.com/images/isr_g.png" alt="Google" style="margin:5px 5px 0 0;" height="17" style="floatleft" /></a>
                   <!-- <input type="button" class="blueButton" value="Appel d'offre" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/tender_cat_select.php?idestimate=<?php echo $IdEstimate ?>&amp;idrow=<?php echo $x ?>&amp;reference=<?php echo $reference ?>' , '_blank', 'directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=800, height=1000' );" />-->
                    <input type="button" onclick="window.open('<?php echo $statURL ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px;"/>
                    <input type="submit" class="blueButton" name="UpdRow_<?php echo $i ?>" id="UpdRow_<?php echo $i ?>" value="Recalculer" />
                </div>
               
            </div>
            <div class="floatright" style="width:10%;">
					 <?php 
                                        $query = "SELECT tva FROM `tva`
                                        ";

                                        $rs =& DBUtil::query( $query );
                                    ?>	
			 <?php 
			 $idrow_vat=$Order->getItemAt($i)->get('idrow');
                                       // $idestimate=$item->get("idorder");
                                        
                                                $q1 = "SELECT status FROM `estimate` where idestimate= '".$IdEstimate."'";
                                                $rs1 =& DBUtil::query( $q1 );
                                                
                                             $status= $rs1->fields('status');
                                             
                                             
                                             $q3 = "SELECT * FROM `estimate_row` WHERE idestimate='".$IdEstimate."' and idarticle=$idarticle AND idrow= '$idrow_vat'";
                         
 						$rs3 =& DBUtil::query( $q3 );
                                              $vat= $rs3->fields('vat_rate');
                                 
                               ?> 
                <table class="dataTable devisTable summaryTable">
                    <tr>

                        <th colspan="1">Marge achat / PV</th><!-- Article  /  PV -->
                    </tr>
                    <tr>
                        <td colspan="1"><span id="mbhp_<?php echo $i ?>">
						Prix HT : <?php echo $mprice_ht ?><br /><br />
						
				         <input type="hidden" name="<?php echo $Order->getItemAt($i)->get('idarticle');?>" value="<?php echo $idarticle ;?>">
				  
				      TVA : 

						 <?php  
						  if($vatRate == 19.60)
						  $vatRate = 20.00;
						 if(($status=="Ordered")||($status=="Cancelled"))
				                 {
				         ?>
						
						<label>
					<?php
						echo " ".$vatRate. " %";
						?>
						</label>
				                
				          
				        <?php
				                }
				                else 
				                 { 
				         ?>
				                         <select name="<?php echo $Order->getItemAt($i)->get('idrow') ;?>" id="tva"  onchange="submit(this.form)"> 
				          
				        

                                        <?php 
	                                while( !$rs->EOF() )
	                                        {
	                                        if($rs->fields('tva')==$vat){
                                        ?>
                                                <option value="<?php echo $rs->fields('tva');?>" selected><?php echo $rs->fields('tva');?> </option>


                                                <?php
                                                }
                                                else 
                                                {
                                                ?>
                                                <option name="vatRate"value="<?php echo $rs->fields('tva');?>"><?php echo $rs->fields('tva');?> </option>
                                                <?
                                                }

                                                
                                                $rs->MoveNext();
                                                }

                                                ?>
                                                </select>
						<?php
				                 }
				          	?>
                                                <br /><br />
                        Marge :<?php if ( $_SESSION['salesman_user'] == 0 ) {?>  <?php echo $mbn ?></span> &euro; <?php } ?>
						<?php if($akilae_comext ==1){ ?> / 							
						<span style="color:red !important;"><?php echo Util::priceFormat( $mbn / (1 + $_SESSION['rate'] / 100)) ?> &euro; </span>
						<?php } ?>
						</td>
                    </tr>
                    <tr>
                        <th>MB %</th>

                    </tr>
                    <tr>
                        <td style="width:50%">
						 <?php if ( $_SESSION['salesman_user'] == 0 ) {?><span id="mbhpp_<?php echo $i ?>"><?php echo $mbf ?>% </span> <?php } ?>
						
						<?php if($akilae_comext ==1){ ?> / 
						<span style="color:red !important;"> <?php echo $mbf_com ?>%</span>
						<?php } ?>
						</td>
						<?php 
									 if( B2B_STRATEGY ){ 
			 							$rough_sale_coeff = $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ) / ( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "unit_cost_amount" ) ); 
			  } 
				else{ 
									$rough_sale_coeff = $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ) / ( 1.0 + $Order->getItemAt( $i )->getVATRate() / 100.0 ) / ( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "unit_cost_amount" ) );
	         } 
							?>
						
					
                    </tr>
                </table>
           
           <br /><br />
          
          
		  <?php
		  if ($Order->getItemAt( $i)->get( "ecotaxe_code" ))
		  {
		  	$ecotaxe_code = $Order->getItemAt( $i)->get( "ecotaxe_code" );
			$idrow = $Order->getItemAt( $i)->get( "idrow" );
		  	$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
			$ecotaxe_amount = $rs1->fields( "amount" ) * $Order->getItemAt( $i)->get( "quantity" );
		
		  	$rs2 =& DBUtil::query( "UPDATE estimate_row SET ecotaxe_amount = $ecotaxe_amount  WHERE ecotaxe_code = '$ecotaxe_code' and idrow = $idrow " );
		  ?>
          
           <table class="dataTable devisTable summaryTable">
           
           <tr>
           <th>Code ecotaxe</th>
           <th>Montant</th>
           </tr>
           
           <td style="width:50%" align="center"><?php if ($Order->getItemAt( $i)->get( "ecotaxe_code" )) echo $Order->getItemAt( $i)->get( "ecotaxe_code" );else echo "-"; ?></td>
           <td style="width:50%" align="center"><?php echo Util::priceFormat($ecotaxe_amount); ?></td>
           
           </table>
            <?
			}
			?>          
            </div>
            <div class="spacer"></div>
            <?
			//frais de port fournisseur
			$idsupplier = $Order->getItemAt( $i)->get( "idsupplier" );
			if( $k == $Order->getItemCount() - 1 || $idsupplier != $Order->getItemAt( $rows[ $k + 1 ])->get( "idsupplier" ) ){
				
				$colspan = $hasLot ? 11 : 10;

				displaySupplierCharges( $idsupplier );

			}
			}
// Ligne produit service
           if( $is_service == 1){ ?>
            
           
            <div class="floatleft" style="width:89%;margin-bottom:10px;">

                    <table class="dataTable devisTable">
                        <tr>
                            <!--<th style="width:75px;">Photo</th>-->
                            <th>Réf. &amp; Designation</th>
                            <th style="width:75px; height:26px;">
                            	
                            	<?php if($Order->getItemAt($i)->get('quantity_per_truck') > 0 || $Order->getItemAt($i)->get('quantity_per_pallet') > 0 || $Order->getItemAt($i)->get('quantity_per_linear_meter') > 0) { ?>
                            	<style type="text/css">
                            		.tooltipfrshover2 { background-color:#FFFFFF; border:1px solid lightgray; text-align:left; padding:0 5px;
										color:#8B9298; display:block; left:70px; position:absolute; top:-7px; width:170px; z-index:99;
									}
                            	</style>
                            	<span onmouseover="document.getElementById('popfrs<?php echo $k ?>').className='tooltipfrshover2';" onmouseout="document.getElementById('popfrs<?php echo $k ?>').className='tooltipfrs';" style=" position:relative; cursor:pointer; ">
	                            	Quantité <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" onmouse style="border-style: none; vertical-align: text-bottom;" alt="" />
	                            	
	                            	<span id='popfrs<?php echo $k ?>' class='tooltipfrs' style="font-size:11px;">
	                            	<?php if($Order->getItemAt($i)->get('quantity_per_linear_meter') > 0) { ?>
	                            		Quantité par ML : <?php echo $Order->getItemAt($i)->get('quantity_per_linear_meter') ?><br />
	                            	<?php } ?>
	                            	<?php if($Order->getItemAt($i)->get('quantity_per_truck') > 0) { ?>
	                            		Quantité par camion : <?php echo $Order->getItemAt($i)->get('quantity_per_truck') ?><br />
	                            	<?php } ?>
	                            	<?php if($Order->getItemAt($i)->get('quantity_per_pallet') > 0) { ?>
	                            		Quantité par palette : <?php echo $Order->getItemAt($i)->get('quantity_per_pallet') ?><br />
	                            	<?php } ?></span>
	                            </span>
                            	<?php } else { ?>
                            		Quantité                            	
                            	<?php } ?>
                            </th>
                            <th>Prix vente<br />tarif unitaire	<?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
                            <th>Remise<br />client %</th>
                            <th>Prix vente<br />net unitaire <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?> </th>
                            <?php
                            /*<th>Remise client<br />supplémentaire</th>
                            <th>Prix unitaire<br />Vente net final</th>*/
                            ?>
                            <th>Prix vente<br />total <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></th>
                        </tr>
                        <tr>
                          <!--  <td rowspan="4" style="vertical-align:top;">
							
								<a href="<?php echo $link ?>" onclick="window.open(this.href); return false;"><?php echo $icone ?></a>
								<?php
								
								if( $Order->getItemAt( $i )->get( "hasPromo" ) ){
	
									?>
									<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/promo.gif" alt="En promotion" /></p>
									<script type="text/javascript">
								      
									    $(document).ready(function(){
									        $("#helpPromo_<?php echo $i ?>").qtip({
									            adjust: {
									                mouse: true,
									                screen: true
									            },
												content: 'Ce produit est en promotion<br />Promotion de <?php echo Util::rateFormat( $Order->getItemAt( $i )->get( "ref_discount_base" ) ) ?>',
									            position: {
									                corner: {
									                    target: "rightMiddle",
									                    tooltip: "leftMiddle"
									                }
									            },
									            style: {
									                background: "#FFFFFF",
									                border: {
									                    color: "#EB6A0A",
									                    radius: 5
									                },
									                fontFamily: "Arial,Helvetica,sans-serif",
									                fontSize: "13px",
									                tip: true
									            }
									        });
									    });
									

									</script>
									<?php
									
								}
								
								?>
								<div class="spacer"></div><br/>
								<div class="leftContainer" >
									<?
									$UseImg=$Order->getItemAt( $i)->get("useimg");
									
									$selected='';
									
									if($UseImg==0){
										$selected =  ' checked="checked"';
									}
									
									?>
									
									<input type="checkbox" class="valignCenter" name="useimg_<?php echo $i ?>" value="0"<?php echo $selected ?> />
	                            </div>
								<div class="leftContainer blueText" style="text-align:left;margin-left:5px;">Util. photo</div>
								
								<input type="hidden" id="min_cde_<?php echo $i ?>" value="<?php echo $Order->getItemAt( $i)->get( "min_cde" ) ?>" />
								<input type="hidden" id="reference_<?php echo $i ?>" value="<?php echo $Order->getItemAt( $i)->get( "reference" ) ?>" />
								<input type="hidden" name="Delete_<?php echo $i ?>" value="0" />

<div class="spacer"></div><br/>

						                           Ordre PDF
                        <input style=" width:50px; " onfocus="this.select()" onkeyup="recalEstimate();" class="textInput price" type="text" name="idrow_line_<?php echo $i ?>" value="<?php echo  $Order->getItemAt( $i)->get("idrow_line") ?>" />
                           
                           
                            </td>-->
                            <td style="text-align:left;">
                                Référence catalogue : <a href="<?php echo $link ?>" onclick="window.open(this.href); return false;"><?php echo $Order->getItemAt( $i)->get( 'reference' ) ?></a>
                                <br />
								<?php
								
								displayColor( $Order->getItems()->get( $i ), strlen( $disabled ) == 0 );
								
								$idsupplier = $Order->getItemAt( $i)->get( "idsupplier" );
								
								if( $idsupplier ){
									
									?>
									<span class="lightGrayText">Réf. fournisseur (<?php echo DBUtil::getDBValue( "name", "supplier", "idsupplier", $idsupplier ) ?>) : <?php echo $reffrs ?></span>
									<span class="lightGrayText" style="position:relative;">&nbsp;<a href="#" onclick="showSupplierInfos( <?php echo $idsupplier ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" /></a></span>
	                                <?php 
	    
																	
								}
								
							?>
                            </td>
                            <!-- Qté -->
                            <td>                             	
								<input<?php echo $disabled ?> onfocus="this.select()" onkeyup="recalEstimate();" class="textInput quantity" type="text" name="qtt_<?php echo $i ?>" value="<?php echo $Order->getItemAt($i)->get('quantity') ?>" style="text-align:center; width:50px;" />
								<input type="hidden" name="old_qtt_<?php echo $i ?>" value="<?php echo $Order->getItemAt($i)->get('quantity') ?>" />
                            </td>
							
							<!-- P.U -->
                            <td>
								<input<?php echo $disabled ?> style=" width:50px; " id="pvu_<?php echo $i ?>" onfocus="this.select()" onkeyup="recalEstimate();" class="textInput price" type="text" name="unit_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get('unit_price' )) ?>" />&nbsp;&euro;
								<input type="hidden" id="old_pvu_<?php echo $i ?>" name="old_unit_price_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get('unit_price' ))?>" />
                            </td>
                            
                            <!-- Remise Client -->	                            
                            <td>
								<input<?php echo $disabled ?> style="text-align:center; width:50px;" id="rc_<?php echo $i ?>" onfocus="this.select()" onkeyup="recalEstimate();" class="textInput percentage" type="text" name="ref_discount_<?php echo $i ?>" id="helpPromo_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount' ) ) ?>"<?php echo $Order->getItemAt( $i )->get( "hasPromo" ) ? " style=\"background-color:#FF8484;\"" : "" ?> />&nbsp;%
								<input type="hidden" id="old_rc_<?php echo $i ?>" name="old_ref_discount_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('ref_discount' ) ) ?>" />
                            </td>
                            
                            <!-- P.U Vente Net -->
                            <td>
                            	<input<?php echo $disabled ?> style=" width:50px; " id="pvnetu_<?php echo $i ?>" onfocus="this.select()" onkeyup="recalEstimate();" type="text" class="textInput price" name="estimate_priceWD_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get( "discount_price" ) ); ?>" /> &euro;
								<input type="hidden" id="old_pvnetu_<?php echo $i ?>" name="old_estimate_priceWD_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get( "discount_price" ) ); ?>" />
                            </td>
                            
                            <!-- Prix de Vente Total -->
                            <td>
                            	<span id="pvnet_<?php echo $i ?>"><?php echo Util::priceFormat( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" )) ?></span>
                            </td>
                        </tr> 
                        <tr>
                            <td rowspan="2" style="text-align:left; width:210px">
                                <textarea<?php echo $disabled ?> name="designation_<?php echo $i ?>" cols="" rows="" style="width:210px; height:70; border-style:none;"><?php echo $des_edit ?></textarea>
                            </td>
                            
                            <!-- tx remise fs -->
							<th style="height:36px;">
								Tarif fournisseur HT
							</th>
                            <th style="width:125px;">Prix achat<br />remisé HT</th>
                            <th style="width:125px;">Remise supplémentaire<br />fournisseur %</th>
                            <th style="width:125px;">Prix achat<br />net unitaire HT</th>
                            <?php
                            /*<th>Remise fourn<br />supplémentaire</th>
                            <th>Prix unitaire<br />Achat net final</th>*/
                            ?>
                            <th style="width:125px;">Prix achat<br />total HT</th>
                        </tr>	                        
                        <tr>
                            <td style="height:33px;"><?php echo Util::priceFormat(DBUtil::getDBValue( "suppliercost", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) )) ?>
                          
                            <?php
								
								
								$idarticle = $Order->getItemAt($i)->get('idarticle');
							//echo $idarticle;
								$q = "SELECT idarticle FROM quantity_price WHERE idarticle = '" . $idarticle."' ";
	$rs = DBUtil::query($q);
       if ( $rs->RecordCount() > 0 ) {
	
								
									
									?>
									
									<a href="#" onclick="showArticleInfos( <?php echo $idarticle ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/fleche.jpg" alt="" style="border-style:none; vertical-align:text-bottom;" /></a>
                        <?php    } ?>
                            
                            
                            </td>
                            <td><?php echo Util::priceFormat($Order->getItemAt($i)->get('unit_cost')) ?></td>
                            <td>
								<input<?php echo $disabled ?> style="text-align:center; width:50px;" id="rf_<?php echo $i ?>" onfocus="this.select()" onkeyup="return recalEstimate();" type="text" class="textInput percentage" name="unit_cost_rate_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('unit_cost_rate' ) ) ?>" />&nbsp;%
								<input type="hidden" id="old_rf_<?php echo $i ?>" name="old_unit_cost_rate_<?php echo $i ?>" value="<?php echo Util::numberFormat( $Order->getItemAt( $i)->get('unit_cost_rate' ) ) ?>" />
                            </td>
                            <td>
								<input<?php echo $disabled ?> style=" width:50px; " id="pau_<?php echo $i ?>" onfocus="this.select()" onkeyup="return recalEstimate();" class="textInput price" type="text" name="unit_cost_amount_<?php echo $i ?>" size="6" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get( "unit_cost_amount" )) ?>" />&nbsp;&euro;
								<input type="hidden" id="old_pau_<?php echo $i ?>" name="old_unit_cost_amount_<?php echo $i ?>" value="<?php echo  Util::numberFormat( $Order->getItemAt( $i)->get( "unit_cost_amount" )) ?>" />
                            	<!--<input type="text" class="textInput price" value="600,00" /> &euro;-->
                            </td>
                            <td>
                            	<span id="pa_<?php echo $i ?>">
                            	<?php echo Util::priceFormat( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt($i)->get( "unit_cost_amount" ) ) ?>
                            	</span>
                            </td>
                        </tr>
                        
                </table>

				<?
				//liens vers les stats de la référence ( afficher sur les 6 derniers mois )
					
				$StatStartDate 	= date( "d/m/Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) );
				$StatEndDate 	= date( "d/m/Y" );
				$reference 		= $Order->getItemAt( $i)->get("reference" );
                $idproduct = DBUtil::getDBValue( "idproduct", "detail", "reference", $reference );
                //$statURL 		= "$GLOBAL_START_URL/statistics/statistics.php?cat=product&amp;reference=" . urlencode( $reference ) . "&amp;period&amp;StatStartDate=" . urlencode( $StatStartDate ) . "&amp;StatEndDate=" . urlencode( $StatEndDate );
				$statURL 		= "$GLOBAL_START_URL/statistics/statistics.php?cat=product&amp;idproduct=" . urlencode( $idproduct ) . "&amp;period&amp;StatStartDate=" . urlencode( $StatStartDate ) . "&amp;StatEndDate=" . urlencode( $StatEndDate );
				?>
				
                <div class="leftContainer" style="margin-top:2px;">
                	<?php if($options){ ?>
					<a href="#" onclick="optionssel(<?=$idarticle?>); return false;" class="blueButton" style="text-decoration:none;"><b>Options</b></a>
					<?php } ?>
								
					<?php if($similar || DBUtil::getDBValue( "clone_id", "detail", "idarticle", $Order->getItemAt( $i )->get( "idarticle" ) ) ){ ?>
					<a href="#" onclick="similarsel(<?php echo $Order->getItemAt( $i )->get( "idarticle" ); ?>); return false;" class="blueButton" style="text-decoration:none;"><b>Similaires</b></a>
					<?php } ?>
                
                
                    <input type="button" class="blueButton" value="Supprimer" onclick="confsupp(<?=$i?>,'<?=$Order->getItemAt($i)->get('reference')?>');" />
                    <!--<input type="button" class="blueButton" value="Substituer" style="margin-left:5px;" onclick="substitute( <?php echo $i ?> ); return false;" />-->
                	<input type="button" class="blueButton" value="Visu. site" style="margin-left:5px;" onclick="window.open('<?php echo $link ?>'); return false;" />
                                
                </div>
                <div class="rightContainer" style="margin-top:2px;">
                    <a href="http://www.google.fr/search?q=<?php echo urlencode( $name ) ?>" onclick="window.open(this.href); return false;"><img src="http://images.google.com/images/isr_g.png" alt="Google" style="margin:5px 5px 0 0;" height="17" style="floatleft" /></a>
                   <!-- <input type="button" class="blueButton" value="Appel d'offre" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/tender_cat_select.php?idestimate=<?php echo $IdEstimate ?>&amp;idrow=<?php echo $x ?>&amp;reference=<?php echo $reference ?>' , '_blank', 'directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=800, height=1000' );" />-->
                    <input type="button" onclick="window.open('<?php echo $statURL ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px;"/>
                    <input type="submit" class="blueButton" name="UpdRow_<?php echo $i ?>" id="UpdRow_<?php echo $i ?>" value="Recalculer" />
                </div>
               
            </div>
            <div class="floatright" style="width:10%;">
					 <?php 
                                        $query = "SELECT tva FROM `tva`
                                        ";

                                        $rs =& DBUtil::query( $query );
                                    ?>	
			 <?php 
			 $idrow_vat=$Order->getItemAt($i)->get('idrow');
                                       // $idestimate=$item->get("idorder");
                                        
                                                $q1 = "SELECT status FROM `estimate` where idestimate= '".$IdEstimate."'";
                                                $rs1 =& DBUtil::query( $q1 );
                                                
                                             $status= $rs1->fields('status');
                                             
                                             
                                             $q3 = "SELECT * FROM `estimate_row` WHERE idestimate='".$IdEstimate."' and idarticle=$idarticle AND idrow= '$idrow_vat'";
                         
 						$rs3 =& DBUtil::query( $q3 );
                                              $vat= $rs3->fields('vat_rate');
                                 
                               ?> 
                <table class="dataTable devisTable summaryTable">
                    <tr>

                        <th colspan="2">Marge achat / PV</th><!-- Article  /  PV -->
                    </tr>
                    <tr>
                        <td colspan="2"><span id="mbhp_<?php echo $i ?>">
						Prix HT : <?php echo $mprice_ht ?><br /><br />
						
				         <input type="hidden" name="<?php echo $Order->getItemAt($i)->get('idarticle');?>" value="<?php echo $idarticle ;?>">
				  
				      TVA : 

						 <?php  
						  if($vatRate == 19.60)
						  $vatRate = 20.00;
						 if(($status=="Ordered")||($status=="Cancelled"))
				                 {
				         ?>
						
						<label>
					<?php
						echo " ".$vatRate. " %";
						?>
						</label>
				                
				          
				        <?php
				                }
				                else 
				                 { 
				         ?>
				                         <select name="<?php echo $Order->getItemAt($i)->get('idrow') ;?>" id="tva"  onchange="submit(this.form)"> 
				          
				        

                                        <?php 
	                                while( !$rs->EOF() )
	                                        {
	                                        if($rs->fields('tva')==$vat){
                                        ?>
                                                <option value="<?php echo $rs->fields('tva');?>" selected><?php echo $rs->fields('tva');?> </option>


                                                <?php
                                                }
                                                else 
                                                {
                                                ?>
                                                <option name="vatRate"value="<?php echo $rs->fields('tva');?>"><?php echo $rs->fields('tva');?> </option>
                                                <?
                                                }

                                                
                                                $rs->MoveNext();
                                                }

                                                ?>
                                                </select>
						<?php
				                 }
				          	?>
                                                <br /><br />
                        Marge : <?php echo $mbn ?></span> &euro;</td>
                    </tr>
                    <tr>
                        <th>MB %</th>
                        <th>Coeff.</th>
                    </tr>
                    <tr>
                        <td style="width:50%"><span id="mbhpp_<?php echo $i ?>"><?php echo $mbf ?></span> %</td>
						<?php 
									 if( B2B_STRATEGY ){ 
			 							$rough_sale_coeff = $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ) / ( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "unit_cost_amount" ) ); 
			  } 
				else{ 
									$rough_sale_coeff = $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ) / ( 1.0 + $Order->getItemAt( $i )->getVATRate() / 100.0 ) / ( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "unit_cost_amount" ) );
	         } 
							?>
						
						
						<td style="width:50%"><span id="coeff_<?php echo $i ?>">
						<?php echo Util::numberFormat( round( $rough_sale_coeff, 2 ) ); ?></span></td>
                    </tr>
                </table>
           <br /><br />
          
          
          
		  <?php
		  if ($Order->getItemAt( $i)->get( "ecotaxe_code" ))
		  {
		  	$ecotaxe_code = $Order->getItemAt( $i)->get( "ecotaxe_code" );
			$idrow = $Order->getItemAt( $i)->get( "idrow" );
		  	$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
			$ecotaxe_amount = $rs1->fields( "amount" ) * $Order->getItemAt( $i)->get( "quantity" );
		
		  	$rs2 =& DBUtil::query( "UPDATE estimate_row SET ecotaxe_amount = $ecotaxe_amount  WHERE ecotaxe_code = '$ecotaxe_code' and idrow = $idrow " );
		  ?>
          
           <table class="dataTable devisTable summaryTable">
           
           <tr>
           <th>Code ecotaxe</th>
           <th>Montant</th>
           </tr>
           
           <td style="width:50%" align="center"><?php if ($Order->getItemAt( $i)->get( "ecotaxe_code" )) echo $Order->getItemAt( $i)->get( "ecotaxe_code" );else echo "-"; ?></td>
           <td style="width:50%" align="center"><?php echo Util::priceFormat($ecotaxe_amount); ?></td>
           
           </table>
            <?
			}
			?>          
            </div>
            <div class="spacer"></div>
            <?
			//frais de port fournisseur
			$idsupplier = $Order->getItemAt( $i)->get( "idsupplier" );
			if( $k == $Order->getItemCount() - 1 || $idsupplier != $Order->getItemAt( $rows[ $k + 1 ])->get( "idsupplier" ) ){
				
				$colspan = $hasLot ? 11 : 10;

				displaySupplierCharges( $idsupplier );

			}

          }
		}
		?>
		<div class="spacer"></div>	
		
		
	</div></div>
	<div class="spacer"></div>	
</div>	

<?php

//-----------------------------------------------------------------------------------------------------

/**
 * Trie les lignes de devis par idsupplier et idrow
 */
function getIdRowsSortedBySupplier(){
	
	global $Order;
	
	//récupérer les identifiant fournisseur
	
	$idsuppliers = array();
	$rows = array();
	$i = 0;
	while( $i < $Order->getItemCount() ){
		
		$rows[] = $i;
		$idsuppliers[] = $Order->getItemAt( $i)->get( "idsupplier" );
		
		$i++;
		
	}

	//tri par fournisseur

    $sorted = false;
 	while( !$sorted ){
 		
 		$sorted = true;
 		$i = 0;
 		while( $i < count( $idsuppliers ) - 1){
 			
 			if( $idsuppliers[ $i ] > $idsuppliers[ $i + 1 ] 
 				|| ( $idsuppliers[ $i ] == $idsuppliers[ $i + 1 ] && $rows[ $i ] > $rows[ $i + 1 ] ) ){

 				$tmp = $idsuppliers[ $i ];
 				$idsuppliers[ $i ] = $idsuppliers[ $i + 1 ];
 				$idsuppliers[ $i + 1 ] = $tmp;
 				
 				$tmp = $rows[ $i ];
 				$rows[ $i ] = $rows[ $i + 1 ];
 				$rows[ $i + 1 ] = $tmp;
 				
 				$sorted = false;
 				
 			}
    			
 			$i++;
 			
 		}
 		
 	}
 	
	return $rows;
	
}

//-----------------------------------------------------------------------------------------------------

function getSelectCharge( $idcharge ){
	
	if( empty( $idcharge ) )
		return "";
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT charge_select$lang AS name FROM charge_select WHERE idcharge_select = $idcharge LIMIT 1";
	$rs =& DBUtil::query( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getCountryName( $idstate ){
	
	if( !$idstate )
		return "";
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT name$lang AS name FROM state WHERE idstate = $idstate LIMIT 1";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getPaymentTxt( $idpayment ){
	
	if( empty( $idpayment ) )
		return "";
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs =& DBUtil::query( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function getPaymentDelayTxt( $idpayment ){
	
	
	
	if( empty( $idpayment ) )
		return "";
	
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT payment_delay$lang AS name FROM payment_delay WHERE idpayment_delay = $idpayment LIMIT 1";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}

//-----------------------------------------------------------------------------------------------------

function displayRemoteEstimateControls( Estimate $estimate ){

	?>
	<div class="blocMultiple blocMRight" style="width:35%;">
		<label style="width:100%; background:none; margin:0 0 0 30px;">
	        <span><strong>Vous ne souhaitez pas répondre par un devis ! Deux options :</strong></span>
	    </label>
	    <div class="spacer"></div>
	    
		<div style="position: relative;" onmouseout="document.getElementById('popBtn1').className='tooltip';" onmouseover="document.getElementById('popBtn1').className='tooltiphover';">
	        <input type="button" class="blueButton" value="Envoyer une invitation" onclick="document.location='/sales_force/mail_remote_estimate.php?idestimate=<?php echo $estimate->getId(); ?>';" style="float:left; margin:5px 0 5px 120px; width:175px;" />
	      	<span class="tooltip" id="popBtn1" style="left:200px; top:0;">
	      		<div style="padding: 5px; width: 200px; height: auto;" class="content">
					<div style="text-align: left; color: rgb(0, 0, 0); font-size:12px; " class="subContent">
					Invitez le client à nous donner plus d'informations en lui proposant de choisir parmi des catégories pertinentes du site.
					</div>
	      		</div>
	      		<img class="arrow" src="/images/back_office/content/box_tooltip_arrow.gif" style="left:-8px;">   		
	      	</span>
		</div>
		<div class="spacer"></div>
		
		<div style="position: relative;" onmouseout="document.getElementById('popBtn2').className='tooltip';" onmouseover="document.getElementById('popBtn2').className='tooltiphover';">
	        <input type="button" class="blueButton" value="Envoyer : A bientôt!" onclick="document.location='/sales_force/mail_remote_estimate.php?idestimate=<?php echo $estimate->getId(); ?>&auto';" style="float:left; margin:5px 0 5px 120px; width:175px;" />
	    	<span class="tooltip" id="popBtn2" style="left:200px; top:0;">
	    		<div style="padding: 5px; width: 200px; height: auto;" class="content">
	    			<div style="text-align: left; color: rgb(0, 0, 0); font-size:12px; " class="subContent">
					Expliquez au client que nous ne proposons pas le produit qu'il recherche, en lui proposant aussi de revenir pour de futurs projets.</span>
					</div>
	      		</div> 
	      		<img class="arrow" src="/images/back_office/content/box_tooltip_arrow.gif" style="left:-8px;">
	    	</span>
	    </div>
		<div class="spacer"></div>
		
	    <p style="line-height:18px; margin:5px 0 0 30px;">	
	    	<?php
	      	
	    		//produits devisés logismarket
		
				$rs =& DBUtil::query( "SELECT * FROM logismarket_categories WHERE idestimate = '" . $estimate->getId() . "'" );
		
		  		if( $rs->RecordCount() ){
		  			
		  			?>
			        Votre demande de devis concerne les produits suivants :
			        <br />
			        <?php
			        
				        while( !$rs->EOF() ){
				        	
				        	if( $rs->fields( "idcategory" ) ){
				        			
			        			?>
				        		<a title="Visualiser sur le site" class="blueLink" href="<?php echo URLFactory::getCategoryURL( $rs->fields( "idcategory" ) ); ?>" onclick="window.open(this.href); return false;">
				        			&#0155; <?php echo htmlentities( $rs->fields( "name" ) ); ?>
				        		</a>
				        		<?php
				        		
			        		}
			        		else echo "&#0155; " . htmlentities( $rs->fields( "name" ) );
	
				        	$rs->MoveNext();
				        	
				        }
	   
		  		}
	  		
		?>
	    </p>
    </div>
    <?php

}

//-----------------------------------------------------------------------------------------------------

function displayColor( EstimateItem $item, $editable = true ){
	
	?>
	<input type="hidden" id="idcolor<?php echo $item->get( "idrow" ) - 1; ?>" name="idcolor[]" value="<?php echo $item->get( "idcolor" ); ?>" />
	<?php
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( !$idproduct )
		return;

	/* couleurs disponibles */
		
	$rs = DBUtil::query( "SELECT idcolor FROM color_product WHERE idproduct = '$idproduct' ORDER BY display ASC" );
	
	if( !$rs->RecordCount() )
		return;

	include_once( dirname( __FILE__ ) . "/../../../objects/Color.php" );
	
	$colors = array();
	
	while( !$rs->EOF() ){
		
		$colors[ $rs->fields( "idcolor" ) ] = new Color( $idproduct, $rs->fields( "idcolor" ) );
		
		$rs->MoveNext();
		
	}
		
	?>
	<div>
	<?php
	
		if( $item->get( "idcolor" ) ){
			
			$color = $colors[ $item->get( "idcolor" ) ];
			
			if( $editable ){
				
				?>
				<img src="/images/back_office/content/corbeille.jpg" alt="Supprimer la couleur" style="cursor:pointer;" onclick="addColor( 0 );" />
				<?php
				
			}

			if( $color instanceof Color ){
			?>
			<img src="<?php echo substr( $color->getImageURI(), 2 ); ?>" alt="<?php echo htmlentities( $color->getName() ); ?>" style="border:1px solid #B2B2B2; width:15px; height:15px;" />
			<span style="margin-right:15px;"><?php echo htmlentities( $color->getName() . " - " . $color->getCode() ); ?></span>
			<?php
			}
			
		}
		
		if( $editable ){
			
			?>
			<a href="#" onclick="colorDialog(<?php $index = $item->get( "idrow" ) - 1; echo $index . "," . $idproduct; ?>); return false;" class="blueLink">couleurs</a>
			<?php
			
		}
		
	?>
	</div>
	<div id="ColorDialog" style="display:none;">
		<ul id="ColorList">
		<?php
		
			foreach( $colors as $idcolor => $color ){
				
				?>
				<li>
					<div style="position:relative;">
						<a href="#" onclick="" style="cursor:pointer;">
							<img src="<?php echo substr( $color->getImageURI(), 2 ); ?>" alt="<?php echo htmlentities( $color->getName() ); ?>" />
						</a>
						<div class="label">
							<?php echo htmlentities( $color->getCode() ); ?>
						</div>
					</div>
				</li>
				<?php
				
			}
			
		?>
		</ul>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>
