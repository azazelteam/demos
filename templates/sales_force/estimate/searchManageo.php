<?php

/**
 * Author : NG
 * 
 * Description : Chargé de récupérer depuis Managéo (manageo.com) les infos sur une société
 * Nomenclature des URLs Ã  former :
 * 		http://www.verif.com/comptage_siren.php?siren_recherche=415064237
 * 		http://www.societe.com/cgi-bin/liste?nom=SMILE+INDUSTRIES&dep=67 
 * 		http://www.manageo.fr/entreprise/rc/preliste.jsp?action=1&Rsociale=SMILE+INDUSTRIES&Dept=67
 * 
 * @TODO : 	
 * 		Possibilitée de choisir entre Managéo & societe.com
 */
include_once( dirname( __FILE__ ) . '/../../../config/init.php' );
include_once( dirname( __FILE__ ) . '/../../../lib/manageo.php' );


header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );
header( "Content-Type: text/html; charset=utf-8" );

/* --------------------------------------------------------------------------------------------------------------------------------------- */

// La requetes proviennent t'elles d'une recherche avec moultes résultats ?
if(isset($_POST['selectUrl'])){
	// Une recherche à  été effectuée, cependant plusieurs résultat étaient possibles
	printInfosFromManageo( $_POST['selectUrl'] );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------------------------------------------- */

global $company;

if(isset($_POST['companyName'])){
	$company = trim($_POST[ 'companyName' ]) ; 
	$company = str_replace( ' ', '+', $company );
	// Manip à  deux balles pour virer la forme juridique de la recherche
	
	if( substr_count( $company, 'SA ' ) > 0 ||  substr_count( $company, ' SA ' ) > 0 || substr_count( $company, ' SA' ) > 0 ){
		$company = str_replace( 'SA ', '',$company );
		$company = str_replace( ' SA ', '',$company );
		$company = str_replace( ' SA', '',$company );	
	}elseif( substr_count( $company, 'SAS ' ) > 0 ||  substr_count( $company, ' SAS ' ) > 0 || substr_count( $company, ' SAS' ) > 0 ){
		$company = str_replace( 'SAS ', '',$company );
		$company = str_replace( ' SAS ', '',$company );
		$company = str_replace( ' SAS', '',$company );
	}elseif( substr_count( $company, 'SARL ' ) > 0 ||  substr_count( $company, ' SARL ' ) > 0 || substr_count( $company, ' SARL' ) > 0 ){
		$company = str_replace( 'SARL ', '',$company );
		$company = str_replace( ' SARL ', '',$company );
		$company = str_replace( ' SARL', '',$company );
	}elseif( substr_count( $company, 'EURL ' ) > 0 ||  substr_count( $company, ' EURL ' ) > 0 || substr_count( $company, ' EURL' ) > 0 ){
		$company = str_replace( 'EURL ', '',$company );
		$company = str_replace( ' EURL ', '',$company );
		$company = str_replace( ' EURL', '',$company );
	}elseif( substr_count( $company, 'SNC ' ) > 0 ||  substr_count( $company, ' SNC ' ) > 0 || substr_count( $company, ' SNC' ) > 0 ){
		$company = str_replace( 'SNC ', '',$company );
		$company = str_replace( ' SNC ', '',$company );
		$company = str_replace( ' SNC', '',$company );
	}
}

//recherche

$siren 		= isset( $_POST['sirenManageo'] ) && preg_match( "/[0-9]{9}/", $_POST['sirenManageo'] ) ? $_POST['sirenManageo'] : false;
$company 	= strlen( $company ) ? $company : false;
$dept 		= isset( $_POST[ "dept" ] ) && preg_match( "/[0-9]+/", $_POST['dept'] ) ? $_POST[ "dept" ] : false;
$dept 		= substr($dept,0,2);
$idEstimate = isset( $_POST[ "idEstimate" ] ) ? $_POST[ "idEstimate" ] : ""; 

$document = manageo_search( $siren, $company, $dept );

//affichage des résultats
	
if( $document && $document->getElementsByTagName( "result" )->length > 0 ){ // Plusieurs sociétés correspindes au critères
		
	$return = "";
	
	for($i=1; $i<=$document->documentElement->getAttribute( "pages" ); $i++) {
		
		$document = manageo_search( $siren, $company, $dept, $i );
		$return .= selectOccurence( $document->getElementsByTagName( "result" ), $idEstimate ) ;
	}
	
	
		
		
	echo "<br/><h2 class='search' style='color:#cb2120;'>R&eacute;sultats de la base Manageo</h2>";
	echo '<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tabBuyer">
		<tr>
			<th width="310">Soci&eacute;t&eacute;</th>
			<th width="170">Etablissement</th>
			<th width="435">Code postal & Ville</th>
			<th>Affecter le devis au client</th> 
		</tr>';		
		
	echo $return;
		
	echo '</table><div class="spacer"></div>';

	
	
	
}


/* --------------------------------------------------------------------------------------------------------------------------------------- */

/**
 * Fonction permettant de retourner les différents resultats de la recherche dans le cas 
 * ou il y a plusieurs resultat possible
 *  
 */
function selectOccurence( DOMNodeList $results, $idEstimate ){

	$i = 0;
	$return = "";
	
		
	while($i < $results->length){
		
		$name 		= $results->item( $i )->getElementsByTagName( "name" )->item( 0 )->nodeValue;
		$url 		= $results->item( $i )->getElementsByTagName( "url" )->item( 0 )->nodeValue;
		$zipcode 	= $results->item( $i )->getElementsByTagName( "zipcode" )->item( 0 )->nodeValue;
		$city 		= $results->item( $i )->getElementsByTagName( "city" )->item( 0 )->nodeValue;
		$type 		= $results->item( $i )->getElementsByTagName( "type" )->item( 0 )->nodeValue;

		$bgcolor = ($i++ & 1) ? '#FFFFFF' : '#F5F5F5'; 
		
		$return .= '<tr>';
		$return .= '<td colspan="4" style="background:'.$bgcolor.'">';		
			
			$return .= "<script type='text/javascript'>$(document).ready(function() { $('#myFormManageo".$i."').ajaxForm( { beforeSubmit: validateManageo } ); });</script>";
			
			$return .= '<form method="post" enctype="multipart/form-data" id="myFormManageo'.$i.'">'; 
			$return .= '<input type="hidden" name="idest" id="idest" value="'.$idEstimate.'" />';	
			$return .= '<input type="hidden" name="Company" id="Company" value="'.$name.'" />';	
			$return .= '<input type="hidden" name="Zipcode" id="Zipcode" value="'.$zipcode.'" />';	
			$return .= '<input type="hidden" name="City" id="City" value="'.$city.'" />';	
			$return .= '<input type="hidden" name="url" id="url" value="'.$url.'" />';	
			$return .= '<table cellpadding="0" cellspacing="0" border="0" width="100%">';		
				$return .= '<td width="315" style="padding:0; border:none; background:none;"><strong>'.$name.'</strong></td>';	
				$return .= '<td width="175" style="padding:0; border:none; background:none;">'.$type.'</td>';
				$return .= '<td width="445" style="padding:0; border:none; background:none;">'.$zipcode.' - '.$city.'</td>';
				$return .= '<td style="padding:0; border:none; background:none;"><input type="submit" name="reloadEstimate" class="blueButton" value="&raquo;" style="float:none; margin-left:55px;" /></td>';
			$return .= '</table>';		
			$return .= '</form>';		
			
					
		$return .= '</td>';	
		$return .= '</tr>';
	
	}
	
	flush();
	
	return $return;
	
}

/* ----------------------------------------------------------------------------------------------------------- */

?>