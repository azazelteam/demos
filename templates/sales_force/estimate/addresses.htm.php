
<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/estimate/addresses.htm.php</span>
<?php } ?>

<script type="text/javascript">
/* <![CDATA[ */

	//------------------------------------------------------------------------------------------------
	
	/*affectation auto des adresses au devis après modification des données*/
		
	function selectEstimateAddress( addressType, selectedAddress ){
	
		var primaryKey 	= getPrimaryKeyName( addressType );
		var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
		var checked 	= document.getElementById( addressType + '_checkbox' ).checked;
		
		var selectedAddress = 0;
		
		if( checked )
			selectedAddress = addresses.options[ addresses.selectedIndex ].value;
		
		$.ajax({
	 	
			url: "<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?select&type=" + addressType + "&id=" + selectedAddress + "&tablename=estimate&primarykey=<?php echo $Order->getId() ?>",
			async: false,
		 	success: function( response ){
		 		
				if( response == '0' )
					xhrAlert( 'Impossible de mettre à jour le devis' );
				
			}
	
		});
		
	}
	
	//------------------------------------------------------------------------------------------------
	
/*	]]> */
</script>
<?php 
/**
 * Affichage des adresses de livraison/facturation si différentes
 */	
$forwardingAddress 	= $Order->getForwardingAddress() instanceof ForwardingAddress ? $Order->getForwardingAddress() : false;
$invoiceAddress 	= $Order->getInvoiceAddress() instanceof InvoiceAddress ? $Order->getInvoiceAddress() : false;
?>


		
<input type="hidden" name="updateAddresses" value="1" />
<?php anchor( "UpdateAdresses" ); ?>
<div class="spacer"></div>

<?php

	include_once( "$GLOBAL_START_PATH/objects/AddressEditor.php" );
	
	//@todo en attendant une gestion cohérente des adresses, on édite pas l'adresse du client par défaut
	$disabledAddr=false;
	if(strlen( $disabled ))
		$disabledAddr=true;
		
	new AddressEditor( $Order->get( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_FORWARDING, $forwardingAddress, $disabledAddr, "float:left; width:50%; margin-top:15px;", "selectEstimateAddress" );
	new AddressEditor( $Order->get( "idbuyer" ), AddressEditor::$ADDRESS_TYPE_INVOICE, $invoiceAddress, $disabledAddr, "float:left; width:50%; margin-top:15px;", "selectEstimateAddress" );
	
?>
<div class="spacer"></div>

