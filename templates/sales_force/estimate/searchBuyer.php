<?php
	include_once( "../../../config/init.php" );
?>	

<?php 
	$where = ""; 
	$where .= $_REQUEST['idbuyerValue'] != "" ? " AND b.idbuyer = '".$_REQUEST['idbuyerValue']."'" : "";
	$where .= $_REQUEST['emailValue'] != "" ? " AND c.mail LIKE '%".$_REQUEST['emailValue']."%'" : "";
	$where .= $_REQUEST['companyValue'] != "" ? " AND b.company LIKE '%".$_REQUEST['companyValue']."%'" : "";
	$where .= $_REQUEST['sirenValue'] != "" ? " AND b.siren LIKE '%".str_replace(" ", "", $_REQUEST['sirenValue'])."%'" : "";
	$where .= $_REQUEST['zipcodeValue'] != "" ? " AND b.zipcode LIKE '%".str_replace(" ", "", $_REQUEST['zipcodeValue'])."%'" : "";
	
	if($where != "") {
		$query = "SELECT c.idcontact, CONCAT( t.title_1, ' ', c.lastname,' ', c.firstname ) AS nomPrenom, b.adress, b.adress_2, c.mail, b.zipcode, b.city, b.company, b.idbuyer
		FROM title t, buyer b, contact c
		WHERE t.idtitle = c.title AND b.idbuyer = c.idbuyer ".$where." ORDER BY b.company";
		
		$rs =& DBUtil::query( $query );
		$i=0;
		$return = "";

		while( !$rs->EOF() ){
			$bgcolor = ($i++ & 1) ? '#FFFFFF' : '#F5F5F5'; 
			$return .= "<tr>";	
				$return .= '<td onclick="javascript: checkBuyer('.htmlentities( $rs->fields( "idbuyer" ) ).', '.$_REQUEST[ 'idEstimate' ].', '.htmlentities( $rs->fields( "idcontact" ) ).')" style="background:'.$bgcolor.'"><strong>'.htmlentities($rs->fields( "company" )).'</strong> (n&deg;'.htmlentities( $rs->fields( "idbuyer" ) ).')</td>';	
				$return .= '<td onclick="javascript: checkBuyer('.htmlentities( $rs->fields( "idbuyer" ) ).', '.$_REQUEST[ 'idEstimate' ].', '.htmlentities( $rs->fields( "idcontact" ) ).')" style="background:'.$bgcolor.'">'.htmlentities( $rs->fields( "nomPrenom" ) ).'</td>';
				$return .= '<td onclick="javascript: checkBuyer('.htmlentities( $rs->fields( "idbuyer" ) ).', '.$_REQUEST[ 'idEstimate' ].', '.htmlentities( $rs->fields( "idcontact" ) ).')" style="background:'.$bgcolor.'">'.$rs->fields( "zipcode" ).' - '.$rs->fields( "city" ).'</td>';
				$return .= '<td onclick="javascript: checkBuyer('.htmlentities( $rs->fields( "idbuyer" ) ).', '.$_REQUEST[ 'idEstimate' ].', '.htmlentities( $rs->fields( "idcontact" ) ).')" style="background:'.$bgcolor.'"><a class="blueLink" href="mailto:'.$rs->fields( "mail" ).'">'.$rs->fields( "mail" ).'</a></td>';
				$return .= '<td onclick="javascript: checkBuyer('.htmlentities( $rs->fields( "idbuyer" ) ).', '.$_REQUEST[ 'idEstimate' ].', '.htmlentities( $rs->fields( "idcontact" ) ).')" style="background:'.$bgcolor.'"><a href="#" onclick="javascript: checkBuyer('.htmlentities( $rs->fields( "idbuyer" ) ).', '.$_REQUEST[ 'idEstimate' ].', '.htmlentities( $rs->fields( "idcontact" ) ).')" class="blueButton" style="float:none; margin-left:55px;">&nbsp;&raquo;&nbsp;</a></td>'; 
			$return .= "</tr>";		
			$rs->MoveNext();
		}
	}
?>

<?php
	if(!isset($i)) {
		
		//echo "<h2 class='search' style='color:#91cb20;'>R&eacute;sultats de la base client</h2><p class='alignCenter fontBold'>Aucun r&eacute;sultat ne correspond &agrave; votre recherche</p>";
			
	} else {
?>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/estimate/searchBuyer.php</span>
<?php } ?>

		<h2 class='search' style='color:#91cb20;'>R&eacute;sultats de la base client (<?php echo $i; ?> r&eacute;sultats)</h2>
		<table cellpadding="0" cellspacing="0" border="0" width="100%" class="tabBuyer">
		<tr>
			<th width="310">Soci&eacute;t&eacute;</th>
			<th width="170">Contact principal</th>
			<th width="240">Code postal & Ville</th>
			<th width="190">Email</th>
			<th>Affecter le devis au client</th>
		</tr>
		<?php echo $return; ?>
		</table>
		<div class="spacer"></div>	
	
<?php } ?>
	
	
