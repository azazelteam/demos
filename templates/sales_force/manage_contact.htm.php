			<style type="text/css">
			<!--
				div#global{
					min-width:0px;
				}
				
				div#global div#globalMainContent{
					margin-top:-15px;
					padding-right:5px;
					width:750px;
				}
			-->
			</style>
			<div id="globalMainContent">
            
				<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/manage_contact.htm.php</span>
<?php } ?>
			
			
				<a href="#" onclick="window.opener.location='<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idbuyer ?>'; window.close();" style="font-weight:bold;color:red;border:1px solid red; float:right; margin-top:5px; padding:3px;">X Fermer</a>
				<h1 style="color:#44474E; font-size:16px;">Gestion des contacts</h1>
				<?php if( !empty( $msg ) ){ ?><span class="msg"><?php echo $msg ?></span><br /><?php } ?>
				<?php if( $res === "loginalreadyexist" ){ ?><span class="msg">Cet identifiant est déjà utlisé</span><?php } ?>
				<?php if( $res === "badmail" ){ ?><span class="msg">Cet email est déjà utlisé</span><?php } ?>
				<?php if( $res === "passtooshort" ){ ?><span class="msg">Le mot de passe doit comporter au minimum 6 caractères</span><?php } ?>
				<div class="mainContent fullWidthContent">
					<div class="topRight"></div><div class="topLeft"></div>
					<div class="content">
						<div class="headTitle">
							<p class="title"><?php echo $company ?></p>
						</div>
						<div class="subContent">
				<script type="text/javascript">
				<!--
					
					function CheckForm( formulaire ){
						
						if( document.getElementById( "GenerateLogin" ) && document.getElementById( "GenerateLogin" ).checked && formulaire.mail.value == "" ){
							
							alert( "Vous devez saisir une adresse email pour créer un compte sur le Front Office." );
							return false;
							
						}
						
						if( formulaire.title.value == 0 ) {
							alert('Vous devez renseigner le titre de la personne');
							return false;
						}
					
						if( formulaire.lastname.value == "") {
					  		alert('Vous devez renseigner le nom de la personne');
					  		return false;
					  	}
					  	
						if( formulaire.phonenumber.value == "") {
					  		alert('Vous devez renseigner le numéro de téléphone');
					  		return false;
					  	}
					  						  	
					  	if(formulaire.mail.value != "") {
							
							var i;
							
							isok=1;
							
							emailAddr = formulaire.mail.value;
								
							i = emailAddr.indexOf("@");
							if (i == -1) {
								isok=0;
							}
						
							var username = emailAddr.substring(0, i);
							var domain = emailAddr.substring(i + 1, emailAddr.length)
					
							
							i = 0;
							while ((username.substring(i, i + 1) == " ") && (i < username.length)) {
								i++;
							}
							if (i > 0) {
								username = username.substring(i, username.length);
							}
					
							i = domain.length - 1;
							while ((domain.substring(i, i + 1) == " ") && (i >= 0)) {
								i--;
							}
							
							if (i < (domain.length - 1)) {
								domain = domain.substring(0, i + 1);
							}
					
						
							if ((username == "") || (domain == "")) {
								isok=0;
							}
						
							
							var ch;
							for (i = 0; i < username.length; i++) {
								ch = (username.substring(i, i + 1)).toLowerCase();
								if (!(((ch >= "a") && (ch <= "z")) || 
									((ch >= "0") && (ch <= "9")) ||
									(ch == "_") || (ch == "-") || (ch == "."))) {
									isok=0;
								}
							}
						
						
							for (i = 0; i < domain.length; i++) {
								ch = (domain.substring(i, i + 1)).toLowerCase();
								if (!(((ch >= "a") && (ch <= "z")) || 
									((ch >= "0") && (ch <= "9")) ||$idbuyer
									(ch == "_") || (ch == "-") || (ch == "."))) {
										isok=0;
								}
							}
						
							if(isok==0) {
								alert('Le format de l\'adresse email n\'est pas valide');
								return false;
							}
						}else{
					  		alert('Vous devez renseigner le champ Email');
					  		return false;
						
						}
					
						return true;
						
					}
					
				// -->
				</script>
							
						
						
						
<?php

if( isset( $idcontact ) || isset( $_GET[ "add" ] )){
	
?>
			<form name="managecont" id="managecont" action="<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php" method="POST" onsubmit="return CheckForm(this);">
					<input type="hidden" name="idbuyer" value="<?php echo $idbuyer ?>" />
					<?php if( isset( $idcontact ) ){ ?><input type="hidden" name="idcontact" value="<?php echo $idcontact ?>" /><?php } ?>
					<div class="tableContainer">
						<table class="dataTable clientTable">
							<tr>
								<th>Client n°</th>
								<td style="font-weight: bold; font-size: 15px;"><?php echo $idbuyer ?></td>
							</tr>
							<tr>
								<th>Titre <span class="asterix">*</span></th>
								<td>
<?php
								
								$selectedValue = isset( $idcontact ) ? $title : false;
								
								XHTMLFactory::createSelectElement( "title", "idtitle", "title" . User::getInstance()->getLang(), "idtitle", $selectedValue, "0", "- Sélectionner -", "name=\"title\"  style=\"background-color:#CFC3E9;\" " );
								
?>
								</td>
							</tr>
							<tr>
								<th>Nom <span class="asterix">*</span></th>
								<td><input type="text" style="background-color:#CFC3E9;text-transform:uppercase;" name="lastname" maxlength="30" value="<?php if( isset( $idcontact ) ){ echo $lastname; } ?>" class="textInput" /></td>
							</tr>
							<tr>
								<th>Prénom</th>
								<td><input type="text" style="text-transform:capitaize;" name="firstname" maxlength="30" value="<?php if( isset( $idcontact ) ){ echo $firstname; } ?>" class="textInput" /></td>
							</tr>
							<tr>
								<th>Fonction</th>
								<td>
<?php
								
								if( isset( $idcontact ) )
									DrawLift_1( "function", $idfunction );
								else
									DrawLift_1( "function" );
								
?>
								</td>
							</tr>
							<!--<tr>
								<th>Service</th>
								<td>
<?php
								
								//if( isset( $idcontact ) )
								//	DrawLift_2( "department", $iddepartment );
								//else
								//	DrawLift_2( "department" );
								
?>
								</td>
							</tr>-->
							<tr>
								<th>Téléphone <span class="asterix">*</span></th>
								<td><input maxlength="20"  style="background-color:#CFC3E9;" type="text" name="phonenumber" value="<?php if( isset( $idcontact ) ){ echo $phonenumber; } ?>" class="textInput" /></td>
							</tr>
							<tr>
								<th>Portable</th>
								<td><input  maxlength="30" type="text" name="gsm" value="<?php if( isset( $idcontact ) ){ echo $gsm; } ?>" class="textInput" /></td>
							</tr>
							<tr>
								<th>Fax</th>
								<td><input maxlength="20" type="text" name="faxnumber" value="<?php if( isset( $idcontact ) ){ echo $faxnumber; } ?>" class="textInput" /></td>
							</tr>		
							<tr>
								<th>Email <span class="asterix">*</span></th>
								<td><input type="text"  style="background-color:#CFC3E9;" name="mail" value="<?php if( isset( $idcontact ) ){ echo $mail; } ?>" class="textInput" onkeyup="if( this.value != '' ) document.getElementById('accountMessage').style.display='block'; else document.getElementById('accountMessage').style.display='none';" /></td>
								<?php if( !empty( $mail ) ){ ?><input type="hidden" name="oldmail" value="<?php echo $mail ?>" /><?php } ?>
							</tr>
							<tr>
								<th>Commentaire</th>
								<td><textarea name="comment" style="height:50px; width:100%;"><?php if( isset( $idcontact ) ){ echo $comment; } ?></textarea></td>
							</tr>
							<?php if( isset( $idcontact ) && !empty( $login ) ){ ?>
							<tr>
								<th>Identifiant</th>
								<td><?php echo $login ?></td>
							</tr>
							<tr>
								<th>Mot de passe</th>
								<td>**********</td>
							</tr>
							<?php } ?>
						</table>
						<div style="margin-top:5px;">
							<?php if( !isset( $idcontact ) ){ ?><div id="accountMessage" style="display:none;">Le client recevra automatiquement un mail d'enregistrement lui donnant son identifiant et son mot de passe pour accéder à son compte sur le Front Office.</div><?php } ?>
							<?php if( isset( $login ) && !empty( $login ) ){ ?>
							<input type="checkbox" name="GeneratePassword" id="GeneratePassword" style="vertical-align:middle;" />
							Générer un nouveau mot de passe
							<?php } ?>
							<?php if( isset( $idcontact ) && ( !isset( $login ) || empty( $login ) ) ){ ?>
							<input type="checkbox" name="GenerateLogin" id="GenerateLogin" style="vertical-align:middle;" />
							Créer un compte client pour le Front Office
							<?php } ?>
						</div>
					</div>
					<div class="submitButtonContainer">
<?php
					
					if( isset( $idcontact ) ){
						
?>
						<input type="submit" class="blueButton" value="Modifier" name="Modify" />
						<!--<input type="submit" class="blueButton" value="Supprimer" name="Delete" onclick="return confirm( 'Voulez-vous vraiment supprimer ce contact ?' );" />-->
<?php
						
					}else{
					
?>
						<input type="submit" class="blueButton" value="Ajouter" name="Add" />
<?php
						
					}
					
?>
						<input type="submit" class="blueButton" value="Retour à la liste des contacts" onclick="document.location='<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $idbuyer ?>'; return false;" />
					</div>
				</form>
<?php
	
}else{
	
	$query = "SELECT * FROM contact WHERE idbuyer = $idbuyer ORDER BY idcontact";
	$rs = DBUtil::query( $query );
	
?>
							<div class="tableContainer">
								<table class="dataTable resultTable">
									<thead>
										<tr>
											<th>Nom</th>
											<th>Fonction</th>
											<th>Email</th>
											<th>Téléphone</th>
											<th>Fax</th>
											<th>Modifier</th>
											<th>Créer devis</th>
											<th>Créer commande</th>
										</tr>
									</thead>
									<tbody>
<?php
									
									for( $i = 0 ; $i < $rs->Recordcount() ; $i++ ){
										
										$id			= $rs->fields( "idcontact" );
										$title		= $rs->fields( "title" );
										$name		= $rs->fields( "lastname" );
										$firstname	= $rs->fields( "firstname" );
										$email		= $rs->fields( "mail" );
										$fax		= $rs->fields( "faxnumber" );
										$function	= getFunction( $rs->fields( "idfunction" ) );
										
										$phone		= "";
										
										if( $rs->fields( "phonenumber" ) )
											$phone .= $rs->fields( "phonenumber" );
										if( $rs->fields( "phonenumber" ) && $rs->fields( "gsm" ) )
											$phone .= "<br />";
										if( $rs->fields( "gsm" ) )
											$phone .= $rs->fields( "gsm" ) . " (GSM)";
										
										$title = getTitle( $title ); 
										//$completeName = $title.'<a href="#" onclick="UpdContact('.$cont_id.', '.$key.'); return false;" style="text-decoration:none;"> '.$cont_firstname.' '.$cont_name.'</a>';
										$fullName = $title . " " . $firstname. " " . $name;
										
?>
										<tr class="blackText">
											<td class="lefterCol"><?php echo $fullName ?></td>
											<td><?php echo $function ?></td>
											<td><a href="mailto:<?php echo $email ?>"><?php echo $email ?></a></td>
											<td><?php echo $phone ?></td>
											<td><?php echo $fax == "00 00 00 00 00" ? "-" : $fax ?></td>
											<td><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $idbuyer ?>&amp;c=<?php echo $id ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" /></a></td>
											<td><a href="#" onclick="window.opener.location='<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?idbuyer=<?php echo $idbuyer ?>&amp;idcontact=<?php echo $id ?>';  window.opener.focus(); window.close();"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" /></a></td>
											<td class="righterCol"><a href="#" onclick="window.opener.location='<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?idbuyer=<?php echo $idbuyer ?>&amp;idcontact=<?php echo $id ?>';  window.opener.focus(); window.close();"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" /></a></td>
										</tr>
<?php
										
										$rs->MoveNext();
										
									}
									
?>
									</tbody>
								</table>
							</div>
							<div class="submitButtonContainer">
								<input type="submit" class="blueButton" value="Ajouter un contact" onclick="document.location='<?php echo $GLOBAL_START_URL ?>/sales_force/manage_contact.php?b=<?php echo $idbuyer ?>&amp;add';" />
							</div>
<?php
	
}

?>
						</div>
					</div>
					<div class="bottomRight"></div><div class="bottomLeft"></div>
				</div>
			</div>