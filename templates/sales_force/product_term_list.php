<?php

ini_set( "memory_limit", "64M" ); //@todo script à optimiser

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
include_once ("$GLOBAL_START_PATH/objects/Invoice.php");



if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "n_service" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_name = '" . Util::html_escape( stripslashes( $_POST[ "n_service" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "n_serial" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET serial_number = '" . Util::html_escape( stripslashes( $_POST[ "n_serial" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}

if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "service_extern" ){

	if ($_POST[ "service_extern" ] == 0)$service_ext = 1;
	else $service_ext = 0;
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_extern = '" . Util::html_escape( stripslashes( $service_ext )) . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "updatemails" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
		$idproduct_term = intval($_GET[ "idproduct_term" ]);
	error_reporting( 0 );
	 SendRelaunch();
	
	exit();
	
}

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

$Title = "Récapitulatif général administration";

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "update" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
		
	error_reporting( 0 );
	ModifyDate();
	
	exit();
	
}

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/Supplier.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$con = DBUtil::getConnection();

$use_help = DBUtil::getParameterAdmin( "gest_com_use_help" );

$resultPerPage = 1000;
$maxSearchResults = 1000;

$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

$TableName = "product_term";
$search = "";

if( isset( $_POST[ "search" ] ) )
	$search = $_POST[ "search" ];

if( isset( $_GET[ "search" ] ) )
	$search = $_GET[ "search" ];

if( isset( $_POST[ "idorder" ] ) )				$idorder = $_POST[ "idorder" ];						else $idorder = "";
/*if( isset( $_POST[ "n_order_supplier" ] ) )		$n_order_supplier = $_POST[ "n_order_supplier" ];	else $n_order_supplier = "";
if( isset( $_POST[ "idbl_delivery" ] ) )		$idbl_delivery = $_POST[ "idbl_delivery" ];			else $idbl_delivery = "";
if( isset( $_POST[ "idsupplier" ] ) )			$idsupplier = $_POST[ "idsupplier" ];				else $idsupplier = "";
if( isset( $_POST[ "idbuyer" ] ) )				$idbuyer = $_POST[ "idbuyer" ];						else $idbuyer = "";
if( isset( $_POST[ "idorder_supplier" ] ) )		$idorder_supplier = $_POST[ "idorder_supplier" ];	else $idorder_supplier = "";
if( isset( $_POST[ "lastname" ] ) )				$lastname = $_POST[ "lastname" ];					else $lastname = "";
if( isset( $_POST[ "status" ] ) )				$status = $_POST[ "status" ];						else $status = "";*/
if( isset( $_POST[ "minus_date_select" ] ) )	$minus_date_select = $_POST[ "minus_date_select" ];	else $minus_date_select = "-365";
/*if( isset( $_POST[ "company" ] ) )				$company = $_POST[ "company" ];						else $company = "";
if( isset( $_POST[ "zipcode" ] ) )				$zipcode = $_POST[ "zipcode" ];						else $zipcode = "";
if( isset( $_POST[ "city" ] ) )					$city = $_POST[ "city" ];							else $city = "";*/

//---------------------------------------------------------------------------------------------

//suppression d'une commande

if( isset( $_POST[ "DeleteOrder" ] ) || isset( $_POST[ "DeleteOrder_x" ] ) )
	SupplierOrder::delete( intval( $_POST[ "OrderToDelete" ] ) );

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<script type="text/javascript" language="javascript">
<!--
	
	function SortResult( sby, ord ){
		
		document.adm_estim.action="product_term_list.php?search=1&sortby="+sby+"&sens="+ord;
		document.adm_estim.submit();
		
	}
	
	function goToPage( pageNumber ){
		
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function SetDate( element ){

		var idorders = element.id.substring( element.id.lastIndexOf( '_', element.id.length ) + 1, element.id.length );
		
		var availibility = $( "#availability_date_" + idorders ).attr( "value" );
	//	var dispatch = $( "#dispatch_date_" + idorders ).attr( "value" );
		
		var postData = "action=update&idos=" + idorders + "&av=" + availibility 
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la date" ); },
		 	success: function( responseText ){

				if( responseText == 'wrong av date format' )
					alert( "Format de date echeance incorrect !" );
					else if( responseText == 'wrong sql' )
					alert( "Impossible de mettre à jour la date !" );
				
        		else if( responseText.length ){
        		
        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Modifications enregistrées' );

        			$( "#cellstatus_" + idorders ).html( responseText );
        			$( "#cellstatus_" + idorders ).attr( "class", "" );
        			$( "#celldelete_" + idorders ).html( "" );
        			
        		}	
					
			}

		});
		
		
	}

	function confirmDeleteOrder( idproduct_term ){

		
		var ret = confirm( '<?php echo Dictionnary::translate( "gest_com_confirm_delete_order_supplier" ) ?>' + idproduct_term + '?' );
		
		if( ret == true )
			document.forms.adm_estim.elements[ 'OrderToDelete' ].value = idproduct_term;
		
		return ret;
		
	}
	
	function selectRelance(){
		
		document.getElementById( "interval_select_relance" ).checked = true;
		
	}
	
	function selectMonth(){
		
		document.getElementById( "interval_select_month" ).checked = true;
		
	}
	
	function selectDelivery( dateField ){
		
		document.getElementById( "interval_select_delivery" ).checked = true;
		
	}
	
	function selectBetween( dateField ){
		
		document.getElementById( "interval_select_between" ).checked = true;
		
		return true;
		
	}
	
// -->
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
				
<div class="centerMax">

	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/sales_force/product_term_list.php</span>
<?php } ?>


<div id="tools" class="rightTools">
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
	Accès rapide</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
	Quelques chiffres...</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
</div>	
<div class="contentDyn">
<form action="<?php echo $ScriptName ?>" method="post" name="adm_estim" name="adm_estim" enctype="multipart/form-data">
	<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
	<input type="hidden" name="search" value="1" />
				
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Récapitulatif général administration</span>
	
	<div class="spacer"></div>
	</h1>		
				
	<div class="blocSearch"><div style="margin:5px;">
	
		<!-- Choix par date -->
		<div class="blocMultiple blocMLeft">
			<h2>&#0155; Par date :</h2>
			
			<label><span><input type="radio" name="interval_select" id="interval_select_relance" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1 || !isset( $_POST[ "interval_select" ] ) ) echo " checked=\"checked\""; ?> /> 
			&nbsp;Depuis</span></label>
			<select name="minus_date_select" onchange="selectRelance()" class="fullWidth">
				
				<option value="-7" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ?></option>
			
				<option value="-30" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ?></option>
				
				<option value="-90" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ?></option>
				<option value="-180" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ?></option>
				<option value="-365" <?php if( !isset( $_POST[ "minus_date_select" ] ) || $_POST[ "minus_date_select" ] == -365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ?></option>
				<option value="-730" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -730) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2year") ?></option>
				
			</select>
			<div class="spacer"></div>
			
			
		</div>  
		
		<!-- Choix par informations sur le commande -->
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Par référence :</h2>
        	
        	<label><span>Référence n°</span></label>
        	<input type="text" name="idorder" id="idorder" class="numberInput" value="<?php echo isset( $_POST[ "idorder" ] ) ? $_POST[ "idorder" ] : "" ?>" />
        	<?php
                                        	
				include_once( dirname( __FILE__ ) . "/../objects/AutoCompletor.php" );
            	AutoCompletor::completeFromDBPattern( "idorder", "order", "idorder", "Client n°{idbuyer}", 15 );

            ?>
        	
        </div>
        <div class="spacer"></div>
      <!--  <div class="blocMultiple blocMLeft">
			<h2>Envoi de mails :</h2>
			  <input type="button" class="blueButton" value="Envoyer" onClick="SendRelaunch( )"/>   
			
			<div class="spacer"></div>
			
			
		</div>  -->
      
		
        
        <input type="submit" class="inputSearch" value="Rechercher" />    
        <div class="spacer"></div>      
	                        
	</div></div>
	<div style="clear:both;"></div>
	
</div>       		      	
<div class="spacer"></div>   

<?php

if( $search != "" ){
	
	$from = "product_term,buyer,contact";
	$now = getdate();
	$interval_select = $_POST[ "interval_select" ];
	$minus_date_select = $_POST[ "minus_date_select" ];
	

			$Min_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $now[ "mon" ], $now[ "mday" ] + $minus_date_select, $now[ "year" ] ) );
			$Now_Time = date( "Y-m-d 23:59:59" );
			
			$SQL_Condition = "product_term.`term_date` >= '$Min_Time' AND product_term.`term_date` <= '$Now_Time'";
		
		
	
	if( !empty( $idorder ) ){
		
		$idorder = FProcessString( $idorder );
		$SQL_Condition .= " AND product_term.reference = $idorder ";
	
	}
	
	
	
	$lang = User::getInstance()->getLang();
	
	$SQL_Condition	.= "
	
	AND product_term.idbuyer = buyer.idbuyer
	AND product_term.idbuyer = contact.idbuyer
	AND contact.idcontact = 0
";
	
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sortby" ] ) )
		$orderby = " ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
	else
		$orderby = " ORDER BY idproduct_term ASC";
	
	//Nombre de pages
	$SQL_Query = "
	SELECT COUNT(*) AS resultCount 
	FROM $from
	 
		WHERE $SQL_Condition 
	";	
//	echo $SQL_Query;
	$rs = $con->Execute( $SQL_Query );
//	var_dump($rs);
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );
	
	if( $resultCount < $maxSearchResults ){
		
		$pageCount = ceil( $resultCount / $resultPerPage );
		
		//Recherche
		$start = ( $page - 1 ) * $resultPerPage;
		
		$SQL_Query = "
		SELECT 
		product_term.idproduct_term,
		product_term.idorder,
		product_term.reference,
		product_term.designation,
		product_term.service_extern,
		product_term.service_name,
		product_term.serial_number,
		product_term.idbuyer,
		product_term.doc_certificat,
		product_term.term_date,
		buyer.company,
				contact.firstname
			
		FROM $from
		
		WHERE $SQL_Condition 
		
		$orderby
		LIMIT $start, $resultPerPage";
//echo $SQL_Query;
		$rs = $con->Execute( $SQL_Query );
//	var_dump($rs);
		global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
			
		include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
			
		$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $SQL_Query, $GLOBAL_DB_PASS ) );
		
		if( $rs->RecordCount() > 0 ){
			
			?>
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function updateservice( idproduct_term, n_service ){
				
					var data = "idproduct_term=" + idproduct_term + "&n_service=" + n_service;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=n_service",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        		window.location.reload();			
						}

					});
		
				}


function updateserial( idproduct_term, n_serial ){
				
					var data = "idproduct_term=" + idproduct_term + "&n_serial=" + n_serial;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=n_serial",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
				
				function updatedoc( idproduct_term,idorder, documents ){
				
					var data = "idproduct_term=" + idproduct_term + "&idorder=" + idorder + "&documents=" + documents;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=documents",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
				
				
				function deliveryNoteDialog( idorder_supplier ){

					$( "#DeliveryNoteDialog" ).html( '<p style="text-align:center;"><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/small_black_spinner.gif" alt="" /> chargement en cours...</p>' );

					$("#DeliveryNoteDialog").dialog({
			        
						modal: true,	
						title: "Commande Fournisseur N° " + idorder_supplier,
						position: "top",
						width: 800,
						zIndex:998,
						close: function(event, ui) { 
						
							$("#DeliveryNoteDialog").dialog( 'destroy' );
							
						}
					
					});

					$.ajax({
					 	
						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?dialog&idorder_supplier=" + idorder_supplier,
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible d'afficher la boîte de dialogue" ); 
							
						},
					 	success: function( responseText ){
					
							$( "#DeliveryNoteDialog" ).html( responseText );
										
						}
					
					});
					
				}
				//Handler of tooltip By behzad
	        $(document).ready(function() {
					$(".fournisseurHelp").each( function(){
					$(this).qtip({
						adjust: {
							mouse: true,
							screen: true
						},
						content: { url: '<?php echo HTTP_HOST; ?>/sales_force/product_term_list.php?showTooltip&idorder_supplier_parent=' + $(this).attr( 'rel' ) },
						position: {
							corner: {
								target: "rightMiddle",
								tooltip: "leftMiddle"
							}
						},
						style: {
							background: "#FFFFFF",
							border: {
								color: "#EB6A0A",
								radius: 5
							},
							fontFamily: "Arial,Helvetica,sans-serif",
							fontSize: "13px",
							tip: true,
							width: 600
						}
					});
					
				});
			});	
			
			function realiserservice(idproduct_term,service_extern){
					
				var data = "idproduct_term=" + idproduct_term + "&service_extern=" + service_extern;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=service_extern",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}
				
				
				
				
				function SendEmail( idproduct_term ){

	
			var data = "idproduct_term=" + idproduct_term;
					var mails = 1;
		var postData = "action=updatemails&idproduct_term=" + idproduct_term 
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la date" ); },
		 	success: function( responseText ){

				if( responseText == 'wrong av date format' )
					alert( "Format de date echeance incorrect !" );
					else if( responseText == 'wrong sql' )
					alert( "Impossible de mettre à jour la date !" );
				
        		else if( responseText.length ){
        		
        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Mails envoyés' );

        			$( "#cellstatus_" + idorders ).html( responseText );
        			$( "#cellstatus_" + idorders ).attr( "class", "" );
        			$( "#celldelete_" + idorders ).html( "" );
        			
        		}	
					
			}

		});
		
		
	}

				/*	function SendEmail(idproduct_term){
					
				var data = "idproduct_term=" + idproduct_term;
					var mails = 1;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/product_term_list.php?update=mails",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
			        			window.location.reload();		
						}

					});
		
				}*/
			//end of Tooltip
			/* ]]> */
			</script>
			<div id="DeliveryNoteDialog" style="display:none;"></div>
			<a name="topPage"></a>

			<div class="contentResult">  
			
				<!-- Titre -->
				<h1 class="titleSearch">
				<span class="textTitle">Résultats de la recherche : <?php echo $rs->RecordCount() ?>échéance(s) commande(s) </span>
				<span class="selectDate">
                <a href="#bottom" class="goUpOrDown" style="text-decoration:none;font-size:10px; color:#000000;">
						Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a> &nbsp;&nbsp;
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/export_csv.php?type=order_supplier&amp;req=<?php echo $encryptedQuery ?>">
					<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				</span>
				<div class="spacer"></div>
				</h1>
				
				<div class="blocResult mainContent">
					<div class="subContent content">
							<table class="dataTable resultTable">
								<thead>
									<tr>
										<th style="width:4%;">N° cde</th>
										<th style="width:4%;">N° client</th>
										<th style="width:8%;">Company + name</th>
										<th style="width:8%;">Référence</th>
										<th style="width:4%;">Service externe</th>
										<th style="width:21%;">Designation</th>
										<th style="width:14%;">Gestion</th>
										<th style="width:14%;">N° de série</th>
										<th style="width:15%;">Certificat</th>
										<th style="width:10%;">Date échéance</th>
										<th style="width:8%;">Envoi de mails</th>
									</tr>
									<!-- petites flèches de tri -->
									<tr>
										<th class="noTopBorder">
											<a href="javascript:SortResult('user.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('user.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder">
											
										</th>
										<th class="noTopBorder">
											<a href="javascript:SortResult('order_supplier.n_order_supplier','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('order_supplier.n_order_supplier','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											
										</th>
										<th class="noTopBorder">
										
										</th>
										<th class="noTopBorder">
										
										</th>
										<th class="noTopBorder">
											
										</th>
										<th class="noTopBorder">
											
										</th>
										<th class="noTopBorder">	<a href="javascript:SortResult('order_supplier.n_order_supplier','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="javascript:SortResult('order_supplier.n_order_supplier','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a></th>
									<th class="noTopBorder"></th>
									</tr>
                                    	
								</thead>
								<tbody>

<?php
			
			//Récupérer les traductions des status
			$strstatus = array();
			$db = &DBUtil::getConnection();
			
			$query = "SELECT order_supplier_status, order_supplier_status$lang FROM order_supplier_status";
			$rs2 = $db->Execute( $query );
			
			if( $rs2 === false )
				die( Dictionnary::translate( "gest_com_impossible_recover_status_list" ) );
			
			while( !$rs2->EOF() ){
				
				$strstatus[ $rs2->fields( "order_supplier_status" ) ] = $rs2->fields( "order_supplier_status$lang" );
				
				$rs2->MoveNext();
				
			}
			
			for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
					$idproduct_term			= $rs->Fields( "idproduct_term" );
				$idorder			= $rs->Fields( "idorder" );
				$idbuyer	= $rs->Fields( "idbuyer" );
				$company	= $rs->Fields( "company" );
				$firstname			= $rs->Fields( "firstname" );
				$reference				= $rs->Fields( "reference" );
				$service_name	= $rs->Fields( "service_name" );
				$serial_number		= $rs->fields( "serial_number" );
				$doc_certificat			= $rs->fields( "doc_certificat" );
				$term_date			= $rs->fields( "term_date" );
			$service_extern	= $rs->Fields( "service_extern" );
				$designation	= $rs->Fields( "designation" );
				
				if( $term_date == "0000-00-00" )
					$term_date = "";
				
				
?>
											<tr class="blackText<?php if( $oss == "cancelled" ) echo " cancelled"; ?>">
												<td class="lefterCol"><?php echo $idorder ?></td>
												
												<td valign="middle" style="white-space: nowrap;">
													<div style="margin-left:auto; margin-right:auto; text-align:center; width:55px;">
														<?php echo $idbuyer ?>
													</div>
												</td>
												<td style="white-space:nowrap;">
													<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>" onclick="window.open(this.href); return false;">
				        		<?php if($rs->fields("company")){ echo htmlentities( $rs->fields( "company" ) ); } else { ?> - <?php  }?>
				        	</a> / <?php echo $firstname ?>
												</td>
                                                <td class="lefterCol"><?php echo $reference ?></td>
                                                   <td class="lefterCol">
												   <a href="#" onClick="realiserservice(<?php echo $idproduct_term; ?>,<?php echo $service_extern; ?> )"><?php if ($service_extern == 1 ) {?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/ok.png" alt="oui" width="20px" height="20px" /><?php }else{  ?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/non.png" alt="Non" width="20px" height="20px" /><?php } ?></a>
												   
												</td>  
                                                    <td class="lefterCol"><?php echo $designation ?></td>
											<td style="white-space:nowrap;">
												<?php
												
													if( $oss != "cancelled" ){
														
														?>
														<input id="service_<?php echo $idproduct_term; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $service_name ); ?>" onkeyup="" />
														<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onclick="updateservice(<?php echo $idproduct_term; ?>, $('#service_<?php echo $idproduct_term; ?>').val() );" />
														<?php
														
													}
													
												?>
												</td>
												<td style="white-space:nowrap;">
												<?php
												
													if( $oss != "cancelled" ){
														
														?>
														<input id="serial_<?php echo $idproduct_term; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $serial_number ); ?>" onkeyup="" />
														<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onclick="updateserial(<?php echo $idproduct_term; ?>, $('#serial_<?php echo $idproduct_term; ?>').val() );" />
														<?php
														
													}
													
												?>
												</td>
												 <td class="lefterCol">
                                                 
                                                  <form action="<?php echo $GLOBAL_START_URL ?>/sales_force/product_term_list.php" method="post" name="adm_estim" name="adm_estim" enctype="multipart/form-data" ><input type="hidden" name="uploader" id="uploader" value="1" />
                                                 <input type="file" class="" name="documents" id="documents" value="" />
                                                <input type="submit" class="blueButton" value="Rechercher" />  </form>
                                                <br>
                                                 <br>
                                           <a href="/data/certificat/<?php echo  $idproduct_term ?>/<?php echo  $doc_certificat ?>">    <?php echo  $doc_certificat ?></a>
                                                 </td>
												<td style="white-space:nowrap;">
															<input size="10" onkeyup="if( !this.value.length || this.value.length == 10 ) SetDate( this );" type="text" name="availability_date_<?php echo $idproduct_term ?>" id="availability_date_<?php echo $idproduct_term ?>" value="<?php echo usDate2eu( $term_date ) ?>" class="calendarInput" /><?php echo DHTMLCalendar::calendar( "availability_date_$idproduct_term", true, "SetDate" ) ?>
														</td>
															 <td> <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-mailbox-inactive.png" alt="oui" width="20px" height="20px" onClick="SendEmail(<?php echo $idproduct_term; ?> )"/> </td>
                                             <!--   <td id="celldelete_<?php echo $idproduct_term ?>"><input type="image" name="DeleteOrder" id="DeleteOrder" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg" alt="Supprimer" onclick="return confirmDeleteOrder( <?php echo $idproduct_term ?>);" /></td>-->
											</tr>
<?php
				
				$rs->MoveNext();
				
			}
			
?>
</tbody>
                    	</table>
                        <div class="spacer"></div>
               		</div>
				</div>
                <div class="spacer"></div>
<?php
			
		}else{ //pas de résultat
			
?>
				<div class="blocResult mainContent">
                    <div class="content">
                    	<p class="msg" style="text-align:center;">Aucun résultat ne correspond à votre recherche</p>
					</div>
				</div>
<?php
		
		}
		
		paginate();

		//-------------------------------------------------------------------------------------------
		
		$lang = User::getInstance()->getLang();
		
		$nbtotal = 0;
		$totalorder = 0.0;
		
		//Commandes en attente

		$nbstandby=0;
		$totalstandby=0;
		
		//Commandes confirmées

		$nbconfirmed=0;
		$totalconfirmed=0;
		
		//Commandes envoyées

		$nbsent=0;
		$totalsent=0;
		
		//Commandes expédiées par le fournisseur

		$nbdispatch=0;
		$totaldispatch=0;
		
		//Commandes réceptionnées

		$nbdelivered=0;
		$totaldelivered=0;
		
		// Factures receptionnées
		$nbBillingRecept=0;
		$totalBillingRecept=0;
		
		//commandes annulées
		$cancelled = 0;
		
		$rs->MoveFirst();
		while( !$rs->EOF() ){
		
			switch( $rs->fields( "order_supplier_status" ) ){
			
				case "standby" : //Commandes en attente
				
					$nbstandby++;
					$totalstandby += $rs->Fields("total_amount_ht");
					
					break;
				
				case "confirmed" : //Commandes confirmées
				
					$nbconfirmed++;
					$totalconfirmed += $rs->Fields("total_amount_ht");
					
					break;

				case "SendByMail" : //Commandes envoyées
				case "SendByFax" :
				
					$nbsent++;
					$totalsent += $rs->Fields("total_amount_ht");
					
					break;
			
				case "dispatch" ://Commandes expédiées
	
					$nbdispatch++;
					$totaldispatch += $rs->Fields("total_amount_ht");
					
					break;
					
				case "received" ://Commandes réceptionnées
	
					$nbdelivered++;
					$totaldelivered += $rs->Fields("total_amount_ht");
					
					break;
				
				case "invoiced" ://Commandes facturée
					$nbdelivered++;
					$nbBillingRecept++;
					$totaldelivered += $rs->Fields("total_amount_ht");
					
					break;
				
				case "cancelled" : $cancelled++; break; //commande annulée
					
			}
			
			$nbtotal++;
			
			$rs->MoveNext();
			
		}

		$totalorder = $totalstandby + $totalconfirmed + $totalsent + $totaldispatch + $totaldelivered + $totalBillingRecept ;
		
		if( $nbtotal > 0 && $totalorder > 0 ){
			
			//Ration commandé/total
			$ratio_nb = ( $nbstandby + $nbconfirmed + $nbsent + $nbdelivered ) * 100 / $nbtotal;
			$ratio_order = ( $totalstandby + $totalconfirmed + $totalsent + $totaldelivered ) * 100 / $totalorder;
			
			//Commande moyenne
			$avg = $totalorder / $nbtotal;
			
		}else{
			
			$ratio_nb = 0;
			$ratio_order = 0;
			$avg = 0;
			
		}
		
		//-------------------------------------------------------------------------------------------
		
	
				
}	
		
	
	
}
	
?>
               	</form>
            </div>
<a name="bottom"></a>
</div>
<div class="spacer"></div><br/>
</div><!-- centerMax -->
<?php

//------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/sales_force" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/sales_force" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------


//--------------------------------------------------------------------------------------------------

function ModifyDate(){
	global $GLOBAL_START_PATH;
	
	
	$idorders = $_GET[ "idos" ];
	$availability_date = $_GET[ "av" ];
		
	//$dispatch_date = $_GET[ "disp" ];
	
	//$Order = new SupplierOrder( $idorders );
	
	$regs = array();
	$pattern = "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/";
	
	if( preg_match( $pattern, $availability_date, $regs ) !== false ){
		
		$day = $regs[ 1 ];
		$month = $regs[ 2 ];
		$year = $regs[ 3 ];
		
		$availability_date = $year."-".$month."-".$day;
		
	}elseif( !empty( $availability_date ) ){
		
		echo "wrong av date format";
		return;
		
	}
	
	
	

			
	$qSuivi = "
		UPDATE product_term 
		SET term_date = '$availability_date' 
		
		WHERE idproduct_term = '$idorders'
		LIMIT 1"
	;
	
		$rsSuivi =& DBUtil::query( $qSuivi );
	
	
		if($rsSuivi === false){
			trigger_error( "Impossible de mettre à jour la date!", E_USER_ERROR );
			echo "wrong sql";
		return;
		}
	
	
 	echo "Confirmée";
	
}
 	if( isset( $_POST[ "uploader" ] ) && $_POST[ "uploader" ] == 1 ){
	global $GLOBAL_START_PATH;
	
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
		//	$idorder =  $_POST[ "idorder" ] ;
		//	$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./product_term_list.php\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term.$extension;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term.$extension;
	//$name= $idorder."_".$idproduct_term.$extension;
		include_once( "../config/init.php" );
		$id =1;
	$name = $idorder."_".$idproduct_term.$extension;
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name . "' 
	WHERE idproduct_term = '" . $idproduct_term . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		if( chmod( $dest, 0755 ) === false )
			die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	
	//exit( $ret === false ? "" : "1" );	
	}
		
}
//-------------------------------------------------------------------------------------------------

	
function SendRelaunch(  ){

	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php" );
	$mail = true;
	 $date = date("Y-m-d" ,mktime(date("H"),date("i"),date("s"),date("m")+1,date("d"),date("Y")));
	  $idproduct_term = $_GET[ "idproduct_term" ];
	$query = "
		SELECT pt.idorder,
		pt.idbuyer,
		pt.reference,
		pt.term_date,
		pt.idproduct_term,
		ord.iduser,
		orw.idsupplier
		FROM 
		product_term pt ,`order` ord, order_row orw
		WHERE 
		pt.idproduct_term = $idproduct_term
		AND pt.term_date < '$date'
		AND pt.service_extern = 0
		AND pt.relaunch_date == NULL
		AND ord.idorder = pt.idorder
		AND orw.idorder = pt.idorder
		";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les informations" );
	
	$idbuyer			= $rs->fields( "idbuyer" );
	$idorder			= $rs->fields( "idorder" );
	$reference			= $rs->fields( "reference" );
	$term_date			= $rs->fields( "term_date" );
	$idproduct_term		= $rs->fields( "idproduct_term" );
	
	$iduser		= $rs->fields( "iduser" );
	$idsupplier		= $rs->fields( "idsupplier" );
	
 $idcontact = 0;
	$idbilling = 10003;
	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	$template = 'ECHEANCIER';
	$editor->setLanguage( substr( User::getInstance()->getLang(), 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( $template );
		
	$editor->setUseOrderTags( $idorder );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	
	
	$editor->setUseAdminTags(); 
$editor->setUseCommercialTags($iduser);
$editor->setUseUtilTags(); 	
$editor->setUseBuyerTags($idbuyer,$idcontact); 
$editor->setUseecheanceTags($idproduct_term	); 
$editor->setUseSupplierTags($idsupplier); 
	
	
	
	
	
	$Message = $editor->unTagBody();
	
	$AdminName = DBUtil::getParameterAdmin( "ad_name" );
	
	$mailer = new htmlMimeMail();
	
	if( $mail == false ){
		
		$smtp_server	= DBUtil::getParameterAdmin( "fax_smtp_server" );
		$smtp_sender	= DBUtil::getParameterAdmin( "fax_smtp_sender" );
		$smtp_auth		= DBUtil::getParameterAdmin( "fax_smtp_auth" );
		$smtp_user		= DBUtil::getParameterAdmin( "fax_smtp_user" );
		$smtp_password	= DBUtil::getParameterAdmin( "fax_smtp_password" );
		$smtp_recipient	= DBUtil::getParameterAdmin( "fax_smtp_recipient" );
		$AdminName		= DBUtil::getParameterAdmin( "ad_name" );
		
		$mailer->setSMTPParams( $smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password );
		
	}
	
	if( $mail == true )
		$Subject = $editor->unTagSubject();
	
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );
	$queryb = "SELECT idorder
			FROM   order_row 
			WHERE reference = $reference
		
			";
	$rsb =& DBUtil::query( $queryb );
	while( !$rsb->EOF() ){
				
				$idorders = $rsb->fields("idorder");
				
				
			
	$queryc = "SELECT sc.email, sc.firstname, sc.lastname
			FROM supplier_contact sc, order_supplier os
			WHERE os.idorder = $idorders
			AND os.idsupplier = sc.idsupplier
			
			";
	$rsc =& DBUtil::query( $queryc );
	
	if($rsc===false)
		die ("Impossible de récupérer le mail");
	while( !$rsc->EOF() ){
				
	$email_business = $rsc->fields("email");
	$firstname_business = $rsc->fields("firstname");
	$lastname_business = $rsc->fields("lastname");
	
			$receipt = array( $email );
			
	
		$mailer->setFrom('"'.$AdminName.'" <'.$smtp_user.'>');
$mailer->setBcc( $email_business );
	$mailer->setBcc( $email_business );
	
	if( $mail == true )
	 $mailer->send(  );
	else
		 $mailer->send( array( "$smtp_recipient" ), "smtp" );
	if( $mail == true )
		 $mailer->send( $receipt );
	else
		 $mailer->send( array( "$smtp_recipient" ), "smtp" );
		$rsc->MoveNext();
				
			} 
	$rsb->MoveNext();
				
			}
			
	echo "confirmé";
		return;
}

//--------------------------------------------------------------------------------------------------

?>