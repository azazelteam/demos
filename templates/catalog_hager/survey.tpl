	<flexy:include src="/head/head.tpl"></flexy:include>
{if:iduser}<span style="color:red;font-weight: bold;">survey.tpl</span>{end:}
<flexy:include src="b_left.tpl"></flexy:include>

<!--====================================== Contenu ======================================-->
<div id="center" class="page">

<b>survey.tpl<b>

		<p id="SurveyTitle">
			<img src="{start_url}/www/icones/smile_m.jpg" align="middle" border="0"> 
			<b>{translations[survey_title]}</b>
		</p>
		{if:register}
			{if:register_error}
				{register_error}
			{else:}
			<p class="thanksSurvey">Nous vous remercions pour votre participation</p>
			<p class="center"><a class="aAccueil" href="{start_url}/index.php">Retour &agrave; l'accueil</a></p>
			
			{end:}
		{else:}
		
		<flexy:toJavascript start_url="baseURL">
		</flexy:toJavascript>
		<script type="text/javascript" language="javascript">
		<!--

		function reply( questionID, replyID ){
 	
			resetQuestion( questionID );
 		
			var button_id = replyID + 1;
			var satisfactionLevel = replyID + 1;
			var img = document.getElementById( 'survey_btn_' + questionID + '_' + replyID );
			var replies = document.forms.SurveyForm.elements[ 'reply[]' ];
			replies[ questionID ].value = satisfactionLevel;
		
			var src = start_url + '/www/icones/survey_btn_' + button_id + '_on.gif';
			img.src = src;
		
		}
	
		function resetQuestion( questionID ){
	
			var i = 0;
			while( i < 5 ){
		
				var button_id = i + 1;
				var img = document.getElementById( 'survey_btn_' + questionID + '_' + i );
				var src = start_url + '/www/icones/survey_btn_' + button_id + '_off.gif';
				img.src = src;
			
				i++;
			
			}
		
		}
		
		function getSurveyHTML(){
	
			var surveyHMTL = document.getElementById( 'SurveyHMTL' );
			var html = document.getElementById( 'SurveyDiv' ).innerHTML;
		
			surveyHTML.value = html;		
		}
		
		// -->
		</script>
		<p id="SurveyMan">{translations[survey_man]}</p>	
				
		<form id="SurveyForm" action="survey.php?action=post" method="post" enctype="multipart/form-data" onsubmit="getSurveyHTML(); ">
		<input type="hidden" name="surveyHTML" id="surveyHTML" value="" />
		<div id="SurveyDiv">
		<table cellspacing="1" cellpadding="2" border="0" align="left" id="SurveyTable">
	 	<tr style="border:1px solid #dbdbdb; border-collapse:collapse; background-color:#fe8819">
			<td valign="top" style="color:white; border:1px solid #dbdbdb;">Quel est votre degr&eacute; de satisfaction concernant :</td>
	 		<td valign="top" style="color:white; border:1px solid #dbdbdb;">{translations[satisfaction_level_1]}</td>
	 		<td valign="top" style="color:white; border:1px solid #dbdbdb;">{translations[satisfaction_level_2]}</td>
	 		<td valign="top" style="color:white; border:1px solid #dbdbdb;">{translations[satisfaction_level_3]}</td>
	 		<td valign="top" style="color:white; border:1px solid #dbdbdb;">{translations[satisfaction_level_4]}</td>
	 		<td valign="top" style="color:white; border:1px solid #dbdbdb;">{translations[satisfaction_level_5]}</td>
			</tr>
			{foreach:survey_lines,line}
			<tr>
				<td valign="top" align="right" class="SurveyQuestion">
					{line[question]:h}
					<input type="hidden" id="reply[]" name="reply[]" value="0" />
					<input type="hidden" id="idsatisfaction_ask[]" name="idsatisfaction_ask[]" value="{line[id]}" />
				</td>
				<td class="SurveyAnswer">	
					<a href="#" onclick="reply( {line[offset]}, 0 ); return false;">
					<img id="survey_btn_{line[offset]}_0" src="{start_url}/www/icones/survey_btn_1_off.gif" alt="{translations[satisfaction_level_1]}" class="SurveyButton" />
					</a>
				</td>
				<td class="SurveyAnswer">	
					<a href="#" onclick="reply( {line[offset]}, 1 ); return false;">
					<img id="survey_btn_{line[offset]}_1" src="{start_url}/www/icones/survey_btn_2_off.gif" alt="{translations[satisfaction_level_2]}" class="SurveyButton" />
					</a>
				</td>
				<td class="SurveyAnswer">	
					<a href="#" onclick="reply( {line[offset]}, 2 ); return false;">
					<img id="survey_btn_{line[offset]}_2" src="{start_url}/www/icones/survey_btn_3_off.gif" alt="{translations[satisfaction_level_3]}" class="SurveyButton" />
					</a>
				</td>
				<td class="SurveyAnswer">	
					<a href="#" onclick="reply( {line[offset]}, 3 ); return false;">
					<img id="survey_btn_{line[offset]}_3" src="{start_url}/www/icones/survey_btn_4_off.gif" alt="{translations[satisfaction_level_4]}" class="SurveyButton" />
					</a>
				</td>
				<td class="SurveyAnswer">	
					<a href="#" onclick="reply( {line[offset]}, 4 ); return false;">
					<img id="survey_btn_{line[offset]}_4" src="{start_url}/www/icones/survey_btn_5_off.gif" alt="{translations[satisfaction_level_5]}" class="SurveyButton" />
					</a>
				</td>
			</tr>
			{end:}
		</table>
		</div>
		<br style="clear:both;" />
			{if:!isLogin}
			<p class="surveyEmail">{translations[survey_email]} :</p>
			<p class="contenerInputEmail"><input class="inputMail" type="text" name="email" value="" /></p>
			{end:}
			
			<br/><br/>
		<p class="surveyCommentTittle">{translations[survey_comment_title]} :</p>
		<p class="InputComment"><textarea name="comment" id="comment"></textarea></p>
		<p class="center"><input type="submit" class="btn_send" value="Envoyer" /></p>
		</form>
		{end:}

		<!-- fin div centre -->
		</div>
		
	<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
            

	