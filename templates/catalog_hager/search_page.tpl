<flexy:include src="head/header.tpl"></flexy:include>
<!--<flexy:include src="menu/menu_alt.tpl"></flexy:include>-->
<!--====================================== Contenu ======================================-->
<div class="search_page"><div id="center" class="page catalog ">

{if:iduser}<span style="color:red;font-weight: bold;">/search_page.tpl</span>{end:}
    <a></a>
   <div class="spacer"></div><br />
	{if:products}	
		<h1 class="title_search">{productCount} r&eacute;sultat(s) trouv&eacute;(s)</h1>
        <!-- Pagination -->
			<p class="pagination" flexy:if="Math.sup(pageCount,#1#)">
				<flexy:tojavascript page="page">
				<flexy:tojavascript pageCount="pageCount">
				<script type="text/javascript">
				/* <![CDATA[ */				
					/* ------------------------------------------------------------------------------------------------ */					
					function goToPage( page ){					
						$( "#SearchForm" ).find( "input[name=page]" ).val( page ); 
						$( "#SearchForm" ).submit();						
					}					
					/* ------------------------------------------------------------------------------------------------ */					
					var windowSize = 3;					
					if( page > 2 )
						document.write( '<a href="#" onclick="goToPage(1); return false;">D&eacute;but</a>' );						
					if( page > 1 ){					
						var prev = page - 1;
						document.write( '<a href="#" onclick="goToPage(' + prev + '); return false;">Pr&eacute;c&eacute;dent</a>' );					
					}					
					var i;					
					if( page - windowSize > 1 ){					
						document.write( "..." );
						i = page - windowSize;						
					}
					else i = 1;
					while( i <= pageCount && i < page + windowSize + 1 ){
						if( i == page )
							document.write( "<b>" + page + "</b>" );
						else document.write( '<a href="#" onclick="goToPage(' + i + '); return false;">' + i + '</a>' );
						i++;						
					}	
					if( page + windowSize < pageCount )
						document.write( "..." );						
					if( page < pageCount ){					
						var next = page + 1;
						document.write( '<a href="#" onclick="goToPage(' + next + '); return false;">Suivant</a>' );					
					}					
					if( page < pageCount -2 )
						document.write( '<a href="#" onclick="goToPage(' + pageCount + '); return false;">Fin</a>' );						
					/* ------------------------------------------------------------------------------------------------ */					
				/* ]]> */
				</script>
			</p>
        <div class="products">
		<ul class="list_product" >
			<li class="productDisplayOne" flexy:foreach="products,product">
				<flexy:include src="/product/product_detail_13.tpl"></flexy:include>			
			</li>
		</ul>				
        </div>
	{else:}
		<h1 class="title_search">Aucun r&eacute;sultat trouv&eacute;</h1>
	{end:}
    <div class="spacer"></div><br />
<div class="intro" style="margin:5px 0 0 0;">	
			<p class="pagination" flexy:if="Math.sup(pageCount,#1#)">
				<flexy:tojavascript page="page">
				<flexy:tojavascript pageCount="pageCount">
			</p>
		</div>
</div>
<script type="application/javascript">
	/* Suppression de ",00" du pourcentage de remise */
	$(".rate").each(function(index) {
		var discountFloat = $(this).text().trim();
		var discountInt = discountFloat.replace(/,[0-9]*\s%/g, "%");
		$(this).html(discountInt);
	});	
$('a.btn-buy').remove();
</script>
</div></div></div>
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>