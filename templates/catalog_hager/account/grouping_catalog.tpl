	<flexy:include src="head/header.tpl"></flexy:include>
<script type="text/javascript" language="javascript">
			<!--
			function CheckAll()
			{
				var checkboxes = document.forms.mainform.elements["IdArticles"];
				var atleastcheckbox = 0;
				
				for (i = 0; i < checkboxes.length; i++){
					
					if(checkboxes[i].checked){
						var valeur = checkboxes[i].value;
						var qtt = "quantity_"+valeur;
						if(document.forms.mainform.elements["quantity_"+valeur].value==""){
							alert ("Vous devez saisir une quantité pour chaque référence que vous voulez commander !");
							return false;
						} else {
							if(isNaN(document.forms.mainform.elements["quantity_"+valeur].value)){
								alert ("La quantité pour chaque référence doit être un nombre !");
								return false;
							} else {
								if(document.forms.mainform.elements["quantity_"+valeur].value <= 0){
									alert ("La quantité pour chaque référence doit être un nombre supérieur à 0 !");
									return false;
								}
							}
						}
						
						atleastcheckbox++;
					}
				}
				if(atleastcheckbox == 0){
					alert("Veuillez cocher au moins une case avant de commander !");
					return false;
				}else{
					return true;
				}	
			}
			// Auto check the box ! checkFlex Busta Flex
			function checktheBox(id){
				var myCheckboxes = document.forms.mainform.elements["IdArticles"];
				var myInputQuantity = document.forms.mainform.elements["quantity_"+id];
				
				for (i = 0; i < myCheckboxes.length; i++){
					if(myCheckboxes[i].value == id){
						if((myInputQuantity.value != "") && (myInputQuantity.value != 0) ){
							myCheckboxes[i].checked = true ;
						}
						else {
							myCheckboxes[i].checked = false ;
						}
					}
				}
			}
			
			// -->
		</script>

	<script type="text/javascript">
$( document ).ready(function() {

	// Find the toggles and hide their content
	$('.toggle').each(function(){
		$(this).find('.toggle-content').hide();
	});

	// When a toggle is clicked (activated) show their content
	$('.toggle a.toggle-trigger').click(function(){
		var el = $(this), parent = el.closest('.toggle');

		if( el.hasClass('active') )
		{
			parent.find('.toggle-content').slideToggle();
			el.removeClass('active');
		}
		else
		{
			parent.find('.toggle-content').slideToggle();
			el.addClass('active');
		}
		return false;
	});

});  //End
</script>
<!-- Header -->
<flexy:include src="menu/menu.tpl"></flexy:include>
<div id="content" class="account" >
{if:iduser}<span style="color:red;font-weight: bold;">/account/grouping_catalog.tpl</span>{end:}
		<!-- Menu Compte -->
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
		</div>
  <div id="categorybloc" class="account" >
		<div class="intro">
					<h2>Catégories avec remises spécifiques</h2>
			<div class="spacer"></div>
			<br><br>
       </div>
	   <table>
	<tr>
 {foreach:categorys,line}
 <td style="width:20%;"><article style="margin:5px; text-align: center;">
<!-- Promo -->
		<a href="{line[url]:h}" style="min-height:90px!important;">
		   <p style="color:#55C4C2;font-size:12px; text-align: center;"><b>{line[name_1]:h}</b></p>
		   <img width="50" height="auto" src="{start_url}/templates/catalog/images/{line[image]:h}" alt="{line[name_1]:h}" /><br><br><br>
		   <p style="color:#CC0000;font-size:12px; text-align: center;"><b>Remise  : {line[rate]:h}% </b></p>
		</a>
</article></td>
{end:}
	</tr></table>
<div class="spacer"></div>	
</div><br />
<br />
		<!-- Contenu -->
		<div id="categorybloc" class="account" >
			<div class="intro">
					<h2>Produits négociés - {buyerTitle:h} {buyerLastname:h}</h2>
			&nbsp;&nbsp;<span style="font-size:11px;display:block;margin: 0 0 10px 7px;">Veuillez trouvez ci-dessous votre selection de produits à prix négociés.</span>
			{if:sucess_msg}<br><br><center><span  style="font-size:13px;font-family:Arial, Helvetica, sans-serif;"><b>{sucess_msg} <b></span><center>{end:}
			<form name="mainform" id="mainform" action="{start_url}/catalog/grouping_catalog.php" method="POST" >
            <input type="hidden" name="validated" value="1">
			
			<div class="spacer"></div>
			{foreach:references,line}
			<a style="text-decoration:none;font-size: 12px;color:#000;padding:0 0 0 8px" href="{line[url_cat]:h}"><span ><b> {line[nameCat]:h}</b></span>	</a>
			{foreach:line[categorie],line1}
<div class="toggle">
<!-- Toggle Link -->
	<a href="#" title="{line1[name_cat]:h}" class="toggle-trigger"><span style="font-size: 12px;color: #666666;"> {line1[name_cat]:h}</span> </a>
<!--<span style="text-decoration:none;font-size: 11px;" href="{line[url_cat_parent]:h}">{line[name_cat_parent]:h}</span>-->

<div class="toggle-content">

			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">		
				
				<tr>
					<th></th>
					<th>Photo</th>
					<th>Référence</th>
					<th>Désignation article</th>
					<th>Prix Unit.</th>
					<th>Remise</th>
					<th>Prix Net</th>
					<th>Quantité</th>
					<th>Cder</th>
				</tr>
				
				{foreach:line1[article],value}
				
				<tr><td class="inProgress"><span onmouseover="document.getElementById('pop{value[idarticle]:h}').className='tooltiphover';" onmouseout="document.getElementById('pop{value[idarticle]:h}').className='tooltip';" style="position:relative; "><img src="{start_url}/templates/catalog/images/iconPlus.gif" alt="voir le détail" class="tooltipa VCenteredWithText" /><span id='pop{value[idarticle]:h}' class='tooltip' style="background-color:white;">{value[designation]:h}</span></td>
					<td class="inProgress"><a href="{value[url]:h}"><img src="{start_url}/templates/catalog/images/{value[image]:h}" border="0" width="40" alt="Icône"/></a></td>
					<td class="inProgress"><a class="overHrefBold" href="{value[url]:h}">{value[reference]:h}</a>
						</td>
					<td class="inProgress"><div style="margin-top:5px;"><a class="overHref" href="{value[url]:h}"><p style="border-top: 1px dotted #E4E4E4;
    color: #565656;
  
    font-size: 12px;">{value[name_1]:e}</p><b>{value[summary]:h}</b></a></div></td>
					<td class="inProgress">{value[basic_price]:h}</td>
					<td class="inProgress">{value[discount]:h}</td>
					<td class="inProgress">{value[discount_price]:h}</td>
					<td class="inProgress"><input type="text" id="quantity_{value[i]}" name="quantity_{value[i]}" size="2" onChange="checktheBox({value[idarticle]});" class="input_quantity" /></td>
					<td class="inProgress"><input type="checkbox" id="check_{value[i]}" name="check_{value[i]}" value="{value[i]}" /></td>
				</tr>
                <input type="hidden" name="ref_{value[i]}" value="{value[reference]:h}" >
			
				{end:}
				
				<tr>
					
			</table>
			<div style="clear:both"></div>
		</div>
<div style="clear:both"></div>
	</div>
				{end:}
				{end:}
			</form>	
            {if:refs_not_found}
<br><br>
<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
<th>Qté</th>
 <th>Référence</td>
</tr>
{foreach:list_articles_not_found,article}
<tr>
<td>{article[quantity]}</td>
<td>{article[reference]}</td>
</tr>
{end:}
</table>

</br></br>

<font color=black size=-1 face=arial><b>
Les articles ci-dessus n'ont pas été trouvées ou la quantité est fausse!
</br>
Merci de vérifier la référence et la quantité.

</b></font>
<br><br>
{end:}

{if:hasrefwoprice}

<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
 <th>Référence</td>
</tr>
{foreach:list_refs_hide_prices,ref}
<tr>
<td>{ref}</td>
</tr>
{end:}
</table>
		
<br><br>
		<p style="color:#000000; font-weight:bold">
		Le prix des références ci-dessus n'est pas disponible pour le moment
		</p>
		<p>
		Pour plus d'informations concernant ces références, <a href="mailto:{ad_mail}" style="text-decoration:underline;">contactez-nous par email</a> ou au <font style="font-weight:bold; color:#FF0000;">{ad_tel}</font>
		</p>
{end:}
	<div class="spacer"></div>
</div>	
<div class="spacer"></div>	
</div>
</br></br></br><div class="spacer"></div>	
</div>
</div>
</div>
<!-- footer -->	
<flexy:include src="foot.tpl"></flexy:include>
