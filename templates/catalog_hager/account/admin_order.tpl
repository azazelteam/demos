{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
	<!--</head>
	<body>-->
    <style type="text/css">
	
		
		div#contents div#main {
			float: left;
		}
	
	</style>
	<!-- Header -->
	

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/admin_order.tpl</span>{end:}
		<div id="leftbloc">
			<flexy:include src="menu/menu_compte.tpl"></flexy:include>
			
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
				<h2>Vos commandes</h2>
					
		{if:NoResults}
			<p class="blocTextNormal">Aucune commande trouvée</p>
		{else:}
			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th>n°</td>
				<th>Date commande</th>
				<th>Montant HT</th>
				<th>Statut</th>
				<th>PDF</th>
				<th style="background:#F9F9F9;">Détail</th>
			</tr>
			{foreach:array_order,line}
				<tr>
					<td><a href="{baseURL}/catalog/show_order.php?idorder={line[idorder]}">{line[orderid]:h}</a></td>
					<td><a href="{baseURL}/catalog/show_order.php?idorder={line[idorder]}">{line[DateHeure]:h}</a></td>
					<td class="tdPrice"><a href="{baseURL}/catalog/show_order.php?idorder={line[idorder]}">{line[total_amount_ht]:h}</a></td>
					<td><a href="{baseURL}/catalog/show_order.php?idorder={line[idorder]}">{line[status]:h}</a></td>
					<td>
						<a href="{baseURL}/catalog/pdf_order.php?idorder={line[idorder]}" target="_blank">
						<img src="{baseURL}/images/back_office/content/pdf_icon.gif" /></a>
					</td>
					
					<td><a href="{baseURL}/catalog/show_order.php?idorder={line[idorder]}">
					<img src="{baseURL}/images/back_office/content/iconsNextBack.png" style="vertical-align:middle;" /></a></td>
				</tr>
			{end:}					
			</table>		
		{end:}
	</div>
		<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
	</div>
	</div>
	
	<flexy:include src="foot.tpl"></flexy:include>
{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}			
			
