<flexy:include src="header.tpl"></flexy:include>
<flexy:include src="b_left.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/account/buyer_catalog.tpl</span>{end:}
{if:HasProducts}
	<div id="centre" style="position:relative; float: left; top: 0px; left: 10px; margin: 0 0 0px 0;">
		<h1 class="titleLstPerso">Votre liste de produits sélectionnés &nbsp;({NbAllProducts})</h1>
		<div id="productDisplay">
		{Detail_1:h}
		</div>
	</div>
	
{else:}
	<h3>Votre liste de produits sélectionnés</h3>

	<div id="msg_buyer_product">
		<h3>Votre catalogue ne contient aucun produit</h3>
			<p>
				<b>Principe:</b>
				<br />
				Disposer d'un catalogue de produits que vous souhaitez commander régulièrement
			</p>
			<p>
				<b>Conditions:</b>
				<br />
				Enregistrez-vous pour pouvoir bénéficier de ce service.
			</p>
			<p>
				<b>Comment l'utiliser ?</b>
				<br />
				Sur toutes fiches produits, vous disposez d'un bouton pour ajouter ce produit à la liste.
				<br />
				Dans la liste personnelle, vous pouvez supprimer les produits
			</p>
	</div>
{end:}

</div>
<!--====================================== Fin du contenu ======================================-->
            
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
