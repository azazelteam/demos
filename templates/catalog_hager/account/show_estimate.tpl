	<flexy:include src="head/header.tpl"></flexy:include>
</head>
<body>

<div id="content" class="account">
{if:iduser}<span style="color:red;font-weight: bold;">/account/show_estimate.tpl</span>{end:}
	<div id="leftbloc">
			<flexy:include src="menu/menu_compte.tpl"></flexy:include>
	</div>
	
	<div id="categorybloc" class="account">
		<div class="intro">
			<h2>N° devis : {IdEstimate}</h2>
			<div id="text">
				<p>Votre devis a été bien enregistré le {DateEstimate:h}.</p>

				<!-- ************  statut de la commande ************-->
				{if:Str_Status_ToDo}
					<p class="blocTextNormal"><strong>Le devis est actuellement en cours de traitement.</strong></p>	
				{end:}

				{if:Str_Status_InProgress}
					<p class="blocTextNormal"><strong>Le devis est actuellement en cours de traitement.</strong></p>	
				{end:}

				{if:Str_Status_Send}
					<p class="blocTextNormal"><strong>Le devis a été envoyé.</strong></p>	
				{end:}
				
				{if:Str_Status_Ordered}
					<p class="blocTextNormal"><strong>Le devis est confirmé.</strong></p>	
				{end:}
				

				<!-- *************** Articles de la commande *************** -->
				
				<table  class="accountTable" cellspacing="0" cellpadding="0" border="0">
					<tr>
						<th style="text-align:center;font-weight:bold;"> Icone </th>
						<th style="text-align:center;font-weight:bold;"> Référence </th>
						<th style="text-align:center;font-weight:bold;"> Désignation </th>
						<th style="text-align:center;font-weight:bold;"> Délai livraison </th>
						<th style="text-align:center;font-weight:bold;"> Prix HT </th>
						<th style="text-align:center;font-weight:bold;"> Remise </th>	
						<th style="text-align:center;font-weight:bold;"> Prix remisé </th>
						<th style="text-align:center;font-weight:bold;"> Quantité </th>
				
						{if:hasLot}
							<th style="text-align:center;font-weight:bold;">Qté / lot</th>
						{end:}
						
						<th style="text-align:center;font-weight:bold;">Total HT</th>
				
					{if:Str_Status_Send}
						<th style="text-align:center;font-weight:bold;">estimate_price</th>
					{end:}
				
					</tr>
					{foreach:estimate_articles,line}
						<tr>
							<td style="text-align:center;">
								{if:line[Icon]}
									<img src="{line[Icon]:h}" width="50" alt="Icon">
								{else:}
									no icon
								{end:}
							</td>
							<td style="text-align:center;">{line[reference]:h}</td>
							<td style="text-align:left;">{line[summary]:h}</td>
							<td style="text-align:center;">{line[delivdelay]:h}</td>
							<td style="text-align:center;">{line[unit_price]:h}</td>
							<td style="text-align:center;">{line[ref_discount]:h}</td>
							<td style="text-align:center;">{line[discount_price]:h}</td>       
							<td style="text-align:center;">{line[quantity]:h}</td>   
							{if:hasLot}
								 <td style="text-align:center;">{line[lot]:h}</td>
							{end:}
							<td style="text-align:center;" class="tdPrice">{line[total_price]:h}</td>
					
							{if:Str_Status_Send}
								<td style="text-align:center;">{line[total_discount_price]:h}</td>
							{end:}
						</tr>
					{end:}
				</table>
				
	<!-- Total du devis -->
		<table class="accountTable" border="0" cellpadding="0" cellspacing="0">	
		{if:remise}
			<tr>
				<th align="center">Montant HT</th>
				<th align="center">Taux remise</th>
				<th align="center">Total HT avec remise</th>
				<th align="center">Remise</th>
			{end:}
		
		{if:is_valid}
			
				<th align="center">Montant HT</th>
				<th align="center">Taux de remise commerciale</th>
				<th align="center">Remise commerciale</th>
			
		{end:}
				<th align="center">Frais de port et emballage</th>
				<th align="center">Total HT</th>
			    <th align="center">TVA</th>
			
		</tr>
		
		{if:remise}
			<tr>
				<td align="right">{total_amount_ht_avant_remise:h} &nbsp;</td>
				<td align="right">{remise_rate:h}&nbsp;</td>
				<td align="right">{total_amount_ht:h}&nbsp;</td>
				<td align="right">{discount:h}&nbsp;</td>
			
		{end:}
		
		{if:is_valid}
				<td align="right">{total_amount_ht_avant_remise:h} &nbsp;</td>
				<td align="right">{total_discount:h}&nbsp;</td>
				<td align="right">{total_discount_amount:h}&nbsp;</td>
		
		{end:}
		
		    	<td align="right">{total_charge_ht:h}</td>
				<td align="right">{total_amount_ht_wb:h}</td>
				<td align="right">{getVATAmount:h}</td>
			
			
		</tr>
		
		<tr>
			<th class="tdPrice" align="left" colspan="5"></th>
			<th class="tdPrice" align="left">Total TTC</th>
			<td class="tdPrice" align="right">{total_amount_ttc_wbi:h}</td>
		</tr>
		</table>
		
				<!-- ******************* Pied de commande ****************** -->
				<!--<flexy:include src="basket_amounts.tpl"></flexy:include>-->
				
				<form method="post" name="EstimateForm" action="show_estimate.php?IdEstimate={IdEstimate}">
					<input type="hidden" name="IdEstimate" value="{IdEstimate}">
					{foreach:payments,payment}			
						<input type="hidden" id="{payment[idpayment]}" name="idpayment[]" value="{payment[idpayment]}" />
					{end:}

					{if:Str_Status_Send}	
						<input type="submit" id="OK" name="OK" value="Commander" class="input_compte" style="margin:0 10px 0 0;" />
					{end:}
				</form>
				<div class="spacer"></div><br/><br/>
				

				<p class="blocTextNormal" style="float:left; width:200px;"><strong>Vos coordonnées :</strong><br />
					{buyer_company}<br />
					{buyer_title} {buyer_firstname} {buyer_lastname}<br />
					{buyer_adress} {buyer_adress_2}<br />
					{buyer_zipcode} {buyer_city}
				<p>
				
				{if:iddelivery}
				<p class="blocTextNormal" style="float:left; width:200px;"><strong>Adresse de Livraison :</strong><br />
					{d_buyer_company}<br />
					{d_buyer_title} {d_buyer_firstname} {d_buyer_lastname}<br />
					{d_buyer_adress} {d_buyer_adress_2}<br />
					{d_buyer_zipcode} {d_buyer_city}
				<p>
				{end:}
				
				{if:idbilling}
				<p class="blocTextNormal" style="float:left; width:200px;"><strong>Adresse de facturation :</strong><br />
					{b_buyer_company}</strong><br />
					{b_buyer_title} {b_buyer_firstname} {b_buyer_lastname}<br />
					{b_buyer_adress} {b_buyer_adress_2}<br />
					{b_buyer_zipcode} {b_buyer_city}
				<p>	
				{end:}
					
				<p class="blocTextNormal" style="float:left; width:200px;"><strong>Moyens de paiement :</strong><br />
				&#0155; {PaymentDelay:h}<br />
				&#0155; {PayMod:h}</p>
				
				{if:hasCommercial}
					<p class="blocTextNormal" style="float:left; width:290px;"><strong>Pour toutes informations complémentaires</strong>, n'hésitez pas à contacter votre commercial :<br/>
					{commercial_firstname:h} {commercial_lastname:h}<br />
					E-mail : {commercial_mailto:h}<br />
					Tél. : {commercial_phonenumber:h} Fax : {commercial_faxnumber:h}
					</p>
				{end:}

				<div class="spacer"></div><br/>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
	</div>
	<div class="spacer"></div>
	
</div>
</div>
</div>
<flexy:include src="foot.tpl"></flexy:include>							

