{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
        <link rel="stylesheet" type="text/css" href="{baseURL}/css/catalog/admin_grouping.css" />
        <link rel="stylesheet" type="text/css" href="{baseURL}/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
	</head>
	<body>
	<!-- Header -->


	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/order_logistics.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
				<h2>Ensemble des commandes du groupement <strong>{buyerGroup}</strong></h2>
					
				<br>
		
				{form:h}
		
				<br><br><br>
                    
                    
		{if:NoResults}
			<p class="blocTextNormal">Aucune commande trouvée</p>
		{else:}
			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th>n°</td>
				<th>Date commande</th>
				<th>Statut</th>
				<th>n° client</th>
				<th>Client</th>
				<th>Package</th>
			</tr>
			{foreach:array_order,line}
			<tr>
					<td>{line[idorder]:h}</td>
					<td>{line[DateHeure]:h}</td>
					<td>{line[status]:h}</td>
					<td>{line[idbuyer]:h}</td>
					<td>{line[company]:h}</td>
					<td><a href="{line[package]}" target="_blank">{line[package]}</a></td>
				</tr>
			{end:}					
			</table>		
		{end:}
	</div>
		<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
		
	<flexy:include src="foot.tpl"></flexy:include>
{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}			
			
