	<flexy:include src="head/header.tpl"></flexy:include>
	<script src="{baseURL}/js/jquery.simpletooltip.js" type="text/javascript"></script>
	<script src="{baseURL}/js/jquery/jquery.form.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	<!--

	// Test des champs du formulaire de login
	function verifLogin (){
		var login = document.forms.RegisterForm.elements[ 'Login' ].value;	 
		var password = document.forms.RegisterForm.elements[ 'Password' ].value;
			
		if(login == ''){
			alert('Le champ E-mail est vide !');
			return false;
		}
		
		if(password == ''){
			alert('Le champ Mot de passe est vide !');
			return false;
		}
		return true ;
	}
		
	
	function verifCreateAccount() {
		
		var data = $('#CreateAccount').formSerialize(); 
		
		$.ajax({
			url: "/catalog/account.php?createLoginAjax",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ },
		 	success: function( responseText ){
		 		
		 		if(responseText != '') {
					$("#returnCreateAccount").html(responseText); 
				} else {
					document.getElementById("CreateAccount").submit();
					
				}

			}
		});
	}
	
	
	// Test des champs du formulaire d'enregistrement
	function verifForm (){
		 
		var emailAddr = RegisterForm.elements[ 'email' ].value;
		
		if(emailAddr == ''){
			alert('Le champ E-mail est vide !');
			return false;
		}else{
			if(!isEmail(emailAddr)){
				alert('L\'identifiant saisie doit être une adresse e-mail  valide !');
				return false;
			}
		}		
			
	  
		if( RegisterForm.elements[ 'pwd' ].value == "") {
			
			alert('Le champ Mot de passe est vide !');
			return false;
			
		}else if( RegisterForm.elements[ 'pwd' ].value.length < 6) {
				
				alert('Le champ Mot de passe doit comporter au minimum 6 caractères !');
				return false;
	  
		}
	
		if( RegisterForm.elements[ 'pwd_confirm' ].value == "") {
		
			alert('Le champ confirmation mot de passe est vide !');
			return false;
			
		}else if( RegisterForm.elements[ 'pwd_confirm' ].value != RegisterForm.pwd.value) {
		
			alert('Le champ confirmation de mot de passe doit être identique au champ Mot de passe !');
			return false;
				
		}
		
		if(this.RegisterForm.elements['title'].options[this.RegisterForm.elements['title'].selectedIndex].value == ''){
			
			alert('Le champ Titre est vide !');
			return false;
		
		} 
		
		if( RegisterForm.elements[ 'lastname' ].value == '') {
		
			alert('Le champ Nom ?> est vide !');
			return false;
			
		}
			  
		if( RegisterForm.elements[ 'phonenumber' ].value == '') {
		
			alert('Le champ Téléphone est vide !');
			return false;
			
		}
		  
		if( RegisterForm.elements[ 'company' ].value == '') {
		  
			alert('Le champ Société est vide !');
			return false;
			
		}
		  
		if( RegisterForm.elements[ 'adress' ].value == '') {
		  
			alert('Le champ Adresse est vide !');
			return false;
			
		}
		
		if( RegisterForm.elements[ 'zipcode' ].value == "") {
		  
			alert('Le champ Code Postal est vide !');
			return false;
			
		}
		
		if( RegisterForm.elements[ 'city' ].value == "") {
		  
			alert('Le champ Ville est vide !');
			return false;
			
		}
		
		if( RegisterForm.elements[ 'siret' ].value == "") {
		  
			alert('Le champ N° Siret est vide !');
			return false;
			
		}
		  
		return true;
		 
		}
	
	function isEmail(strSaisie) {
		var verif = /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
		return ( verif.test(strSaisie) );
	}
	
	jQuery(document).ready(function() {
		$("#newUser .blocTitle").click(function () {
			$("#newUser .toggle").toggle("slow");
		});
	});
	
	// -->
	</script>    
<!--</head>
<body>-->
<!-- Header -->
<!--<flexy:include src="head/banner.tpl"></flexy:include>
<flexy:include src="menu/navigation.tpl"></flexy:include>-->
<div id="content" class="account">
{if:iduser}<span style="color:red;font-weight: bold;">/account/account.tpl</span>{end:}
	{if:isLogin}
	
		<!-- Menu Compte -->
		<div id="leftbloc">
			<!--<nav id="left">-->
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
             <!--   {if:logoGroup} 
				
               <ul class="menu">
	<li> <a href="/catalog/grouping_catalog.php"> <span>Votre catalogue</span></a>
    	
	</li>
</ul>
 {end:}
			
            {if:logoGroup}
		
             <a href="/catalog/grouping_catalog.php"> <img src="{logoGroup}" border="0" alt="Logo" width="180px" height="100px"/> </a>
             {end:}
			 -->
			</div>
		<!--</div>-->
		
		<!-- Contenu -->
		<div id="categorybloc" class="account">
			<div class="intro stretch-down">
				<h2>Bienvenue sur votre compte </h2>
				<br><br>

				<div id="text">
                    <p>
                        La création de votre compte vous permet dès à présent :
                    </p>
                    <ul>
                        <li>de gérer votre compte.</li>
                        <li>de créer et mémoriser votre catalogue perso.</li>
                        <li>de consulter vos demandes de devis et de suivre l'état de vos commandes.</li>
                    </ul>
                    <p style="margin-bottom: 10px;">
                        Toute notre &eacute;quipe vous remercie de votre confiance.<br />
                        Pour toutes informations complémentaires, n'hésitez pas à nous contacter au ......<sup>*</sup>.
                    </p>
                    <p class="small">
                        * Appel gratuit depuis un poste fixe.<br />
                        {if:buyerFirstname}
                            Si vous n'êtes pas <strong>{buyerFirstname} {buyerLastname}</strong>, merci de vous déconnecter <a href="{baseURL}/www/logoff.php" class="Account_logoffA">en cliquant ici</a>.
                        {end:}
                        {if:customer}
                            Si vous n'êtes pas {customer.get(#firstname#)} {customer.get(#lastname#)}, merci de vous déconnecter <a href="{baseURL}/www/logoff.php" class="Account_logoffA">en cliquant ici</a>.
                        {end:}</p>
				</div>
				
			</div>
		</div>
		<div class="spacer"></div>
		
	{else:}
		<flexy:include src="page/left.tpl"></flexy:include>
		
		<!-- Contenu -->
		<div id="categorybloc">
			<div class="intro">
				<h2>Accéder à mon compte.</h2>
				<div id="text">
					<p>Liste personnelle, historique de devis et commandes...</p>
									
					<!-- Déja client -->
					<div id="registeredUser" class="blocRight">
						<p><strong>Déjà client ? Connectez-vous.</strong></p>
						
						<form action="{baseURL}/catalog/account.php" method="post">
							{if:issetPostLoginAndNotLogged}
								<!-- Erreur lors de l'identification -->
								<p class="msgError">Votre e-mail ou mot de passe est incorrect.</p>
							{end:}
							
							<p>	
								<input type="text" name="Login" class="loginField input_identifiant" value="" />
								<input type="password" name="Password" class="loginField input_password" value="" />
								<input type="submit" class="submitField" value="Connexion" />
							</p>
							<div class="spacer"></div>
							
							<p>
								<a href="javascript:mail();" class="passwordForget">Mot de passe oublié ?</a>
							</p>
							<div class="spacer"></div>
						</form>				
						<div class="spacer"></div>
					</div>
                    
                    <div class="spacer"></div>
                    <hr />
                    
					<!-- Nouveau client -->
					<div id="newUser" class="bloc">
						<p class="blocTitle clickable"><strong>Nouveau client ? <a>Créer votre compte.</a></strong></p>
						
	                    <div class="toggle">
                            <p class="msgError" id="returnCreateAccount"></p>
                            
                            <!--
                            <form action="{baseURL}/catalog/register.php" method="post" id="CreateAccount">
                                <p>
                                    <input type="text" name="createLogin" class="loginField input_email" value="" />
                                    <input type="password" name="createPassword" class="loginFieldMedium input_password" value="" />
                                    <input type="password" name="createConfPassword" class="loginFieldMedium marginLeft input_conf_password" value="" />
                                    <input type="button" onClick="verifCreateAccount();" class="submitField" value="Etape suivante" />
                                </p>

                            </form>
                            <div class="spacer"></div>
                            -->

                            <form action="{baseURL}/catalog/register.php" id="RegisterForm" method="post" onSubmit="return verifForm();"> 
                                <p>Tous les champs marqués d'un <span class="required">fond bleu</span> sont obligatoires.</p>
                                <div class="spacer"></div><br />
                                
                                {if:errorMsgNotEmpty}					
                                    <p class="msgError">{errorMsg:h}</p><br />
                                {end:}
                                
                                <input type="hidden" name="FromScript" value="{scriptName}" />
                                <p><strong>Informations de connexion à mon compte</strong></p>
                                <div class="spacer"></div>
                                       
                                <p>
                                    <input type="text" name="email" value="{userInfoEmail:h}" class="loginField input_identifiant required" maxlenght="20" />
                                    <input type="password" name="pwd" value="{userInfoPwd:h}" class="loginField input_password required" maxlength="20" />
                                    <input type="password" name="pwd_confirm" value="{userInfoPwdConfirm:h}" class="loginFieldMedium marginLeft input_conf_password required" maxlength="20" />
                                </p>
                                <div class="spacer"></div><br/>
                                
                                <p><strong>Mes coordonnées</strong></p>
                                <div class="spacer"></div>
                            
                                <div class="formleft">
									<div class="selectTitle required">
									<select name="title">
										<option value="" selected="selected"></option>
										<option value="2">Mme</option>
										<option value="1">Mr</option>
										<option value="3">Mlle</option>
									</select>
									</div>

                                    <input type="text" name="firstname" value="{userInfoFirstname:h}" class="loginFieldSmall marginLeft input_prenom required" maxlength="20" />
                                    <input type="text" name="lastname" value="{userInfoLastname:h}" class="loginFieldSmall marginLeft input_nom required"  maxlength="20"/>
                                    <div class="spacer"></div>   
                                    <input type="text" name="phonenumber" value="{userInfoPhone:h}" class="loginFieldMedium input_phone required" maxlength="20" />
                                    <input type="text" name="faxnumber" value="{userInfoFax:h}" class="loginFieldMedium marginLeft input_fax" maxlength="20" />
                                    <div class="spacer"></div>
                                    <input type="text" name="gsm" value="{userInfoGsm:h}" class="loginFieldMedium input_gsm" maxlength="20" />
                                    <div class="spacer"></div>             
									<div class="selectField required">
										<select name="iddepartment">
											<option value="0">Sélectionner</option>
											<option value="1">Responsable achat</option>
											<option value="2">Service achats</option>
											<option value="3">Approvisionneur</option>
											<option value="4">Technicien - agent de maîtrise</option>
											<option value="5">Maintenance</option>
											<option value="6">PDG - DG - Gérant</option>
											<option value="7">Assistante - secrétaire</option>
											<option value="8">Technique</option>
											<option value="9">Commercial</option>
											<option value="10">Logistique</option>
											<option value="11">Production</option>
											<option value="12">Méthodes</option>
											<option value="13">Bureau d' études</option>
											<option value="14">Consultant</option>
											<option value="15">Profession libérale</option>
											<option value="16">Elu - cadre territorial</option>
											<option value="17">Autres</option>
											<option value="18">Administration</option>
											<option value="19">Immobilier</option>
											<option value="20">Industrie/Maintenance</option>
											<option value="21">Imprimerie/Presse/Editions</option>
											<option value="22">Agriculture/agro</option>
											<option value="23">BTP</option>
											<option value="24">Environnement</option>
											<option value="25">Artisanat</option>
											<option value="26">Vente/Distribution</option>
											<option value="27">Hôtellerie/Restauration</option>
											<option value="28">Ressources humaines</option>
											<option value="29">Santé</option>
											<option value="30">Automobile</option>
											<option value="31">Mode/Textile/Habillement</option>
											<option value="32">Informatique/Télécom</option>
											<option value="33">Secteur public</option>
											<option value="34">Electronique/Mécanique</option>
											<option value="35">Enseignement et Formation</option>
											<option value="36">Transports/Logistique</option>
											<option value="37">R &amp; D</option>
											<option value="39">Commerce/Marketing</option>
											<option value="40">Service qualité</option>
											<option value="41">Responsable qualité</option>
											<option value="42">Comptabilité</option>
										</select>
									</div>
                                    <div class="spacer"></div>
                                    <input type="text" name="company" value="{userInfoCompany:h}" class="loginField input_company required"  maxlength="50"/>
                                    <div class="spacer"></div>
                                    <input type="text" name="adress" value="{userInfoAdress:h}"  class="loginField input_adress required" maxlength="100"/>
                                    <div class="spacer"></div>
                                    <input type="text" name="adress_2" value="{userInfoAdress_2:h}" class="loginField input_adresscomp" maxlength="100"/>
                                    <div class="spacer"></div>
                                    <input type="text" name="zipcode" value="{userInfoZipcode:h}" class="loginFieldMedium input_cp required"  maxlength="10"/>
                                    <input type="text" name="city" value="{userInfoCity:h}" class="loginFieldMedium marginLeft input_city required"  maxlength="50"/>
                                    <div class="spacer"></div>
									<div class="selectField required">
										<select id="idstate" name="idstate">
											<option value="1">France</option><option value="3">Algérie</option><option value="2">Andorre</option><option value="4">Allemagne</option><option value="5">Arabie Saoudite</option><option value="6">Autriche</option><option value="7">Belgique</option><option value="8">Bulgarie</option><option value="9">Chypre</option><option value="10">Danemark</option><option value="11">Espagne</option><option value="12">Estonie</option><option value="13">Grande Bretagne</option><option value="14">Grèce</option><option value="15">Hongrie</option><option value="16">Irlande</option><option value="17">Islande</option><option value="18">Israel</option><option value="19">Italie</option><option value="20">Koweit</option><option value="21">Lettonie</option><option value="22">Liban</option><option value="23">Liechtenstein</option><option value="24">Lithuanie</option><option value="25">Luxembourg</option><option value="26">Maroc</option><option value="27">Monaco</option><option value="28">Norvège</option><option value="29">Pays-bas</option><option value="30">Pologne</option><option value="31">Portugal</option><option value="32">République Tchèque</option><option value="33">Roumanie</option><option value="34">Slovaquie</option><option value="35">Slovénie</option><option value="36">Suède</option><option value="37">Suisse</option><option value="38">Tunisie</option><option value="39">Turquie</option><option value="40">Ukraine</option><option value="41">Emirats Arabes Unis</option><option value="42">Sénégal</option><option value="43">Saint-Vincent-et-les-Grenadines</option><option value="44">France (DOM-TOM)</option><option value="45">Burkina Faso</option><option value="46">Canada</option><option value="47">Congo</option><option value="48">France H.T.</option><option value="49">Madagascar</option><option value="50">République de Maurice</option><option value="51">République du Congo</option><option value="52">Côte d'Ivoire</option>
										</select>
									</div>
                                    <div class="spacer"></div>
                                    <input type="text" name="siret" value="{userInfoSiret:h}" class="loginFieldMedium input_siret required"  maxlength="14"/>
                                    <input type="text" name="naf" value="{userInfoNaf:h}" class="loginFieldMedium marginLeft input_naf"  maxlength="15"/>
                                    <div class="spacer"></div><br />    
                                    <p><input type="checkbox" name="newsletter" value="1" />&nbsp;<strong>Abonnement à la newsletter du site.</strong> Tenez-moi informé de l'actualité, des promotions et des opérations de déstockage qui vous concernent.</p>
                                    <p class="small">Selon la loi "Informatique et libertés" du 05/01/76, vous disposez d'un droit d'accès, de rectification et de suppression des données qui vous concernent.</p>
                                </div>
                                <p><input type="submit" class="submitField" value="Créer mon compte" name="Register" /></p>
                                
                                <input type="hidden" name="idsource" value="{param_admin_default_buyer_source:h}" />
                            </form>
                        </div>
                        
						<div class="spacer"></div>
					</div>
					
				</div>					
			</div>
			<div class="spacer"></div>
			
			
			
		</div>
	{end:}
	<div class="spacer"></div>
		

<!-- footer -->	
<!--<flexy:include src="foot.tpl"></flexy:include>-->
<flexy:include src="foot.tpl"></flexy:include>