	<flexy:include src="head/header.tpl"></flexy:include>
	
	<script src="{baseURL}/templates/catalog/js/jquery/js/jquery-ui-1.8.4.custom.min.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	
	/* <![CDATA[ */
		$( document ).ready( function(){
			setProForm();
		} );
		
		function setParticulierForm(){
			$( '.proPart' ).each( function(){ $(this).hide(); } );
		}
		
		function setProForm(){
			$( '.proPart' ).each( function(){ $(this).show(); } );
		}
		
		function setFormType(){
			if( $( '#customer_profile :selected' ).val() == '1' )
				setParticulierForm();
			else
				setProForm();
		}
		
		// Test des champs du formulaire de login
		function verifLogin() {
			var returnVar = true;
			var objError = $("#registeredUser").find(".msgError")
			var login = $("#login_email");	 
			var password = $("#login_pwd");

			objError.html("");				
			initRequiredFields();
							
			if (login.val() == ''){
				login.css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			if (password.val() == ''){
				password.css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if (returnVar == true) {
				document.getElementById("LoginForm").submit();
			} else {
				objError.show();
				objError.html("Merci de renseigner les champs signalés en rouge.");
			}
		}
			
		//Création de compte
		function verifCreateAccount() {
			var data = $('#CreateAccount').formSerialize(); 
			var objError = $("#newUser").find(".msgError")
			objError.html("");

			$.ajax({
				url: "/catalog/account.php?createLoginAjax",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer votre compte." ); },
				success: function( responseText ){
									
					if(responseText != '') {
						
						if( responseText == '1') {
							objError.html("Le format de l'adresse email est invalide."); 
							$("#createLogin").css("border-color","#D12524");
							$("#createConfPassword").css("border-color","#CDCDCD");
							$("#createPassword").css("border-color","#CDCDCD");
							
						}
						if( responseText == '2') {
							objError.html(""); //Cette adresse email est déjà réservée.
							$("#createLogin").css("border-color","#D12524");
							$("#createConfPassword").css("border-color","#CDCDCD");
							$("#createPassword").css("border-color","#CDCDCD");
							
							$('#forgetEmailInput').val( $('#createLogin').val() );
							$( "#forgetEmail" ).dialog({
								title:"Cette adresse email est déjà utilisée!", bgiframe: true, modal: true, width:550, minHeight:10, overlay: {
									backgroundColor: '#000000',
									opacity: 0.5
								},
								close: function(event, ui) { $( "#forgetPassword" ).dialog( "destroy" ); }
							});
						}
						if( responseText == '3') {
							objError.html("Le format du mot de passe est incorrect. Il doit faire plus de 6 caractères et ne contenir que des lettres ou des chiffres."); 
							$("#createLogin").css("border-color","#CDCDCD");
							$("#createConfPassword").css("border-color","#D12524");
							$("#createPassword").css("border-color","#D12524");
						
						}
						if( responseText == '4') {
							objError.html("Le mot de passe et la confirmation du mot de passe ne sont pas identiques."); 
							$("#createLogin").css("border-color","#CDCDCD");
							$("#createConfPassword").css("border-color","#D12524");
							$("#createPassword").css("border-color","#D12524");
						}
	
						
					} else {
						//document.getElementById("CreateAccount").submit();
						$('#firstElementLogin').hide();
						$('#newLogin').val( $('#createLogin').val() );
						$('#newPassword').val( $('#createPassword').val() );
						$('#secondElementLogin').show();
					}
				}
			});
		}
				
		//Mot de passe oublié
		function forgetPassword() {
			$( "#forgetPassword" ).dialog({
				title: "Mot de passe oublié ?",
				bgiframe: true,
				modal: true,
				closeText: "X",
				width:550,
				minHeight:200,
				overlay: {
					backgroundColor: '#000000',
					opacity: 0.5
				},
				close: function(event, ui) { $( "#forgetPassword" ).dialog( "destroy" ); }
			});
		}
		
		//Contrôle de l'email saisie pour le mot de passe oublié
		function sendPassword( Email, divResponse ) {	
			data = "&Email="+Email;
			$.ajax({
	
				url: "/catalog/forgetmail.php",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer l'email" ); },
				success: function( responseText ) {				 		
					$(divResponse).html("<br/><p class='blocTextNormal' style='text-align:center;'>" + responseText + "</p>");
				}
				
			 });
		}
	
		function verifCreateAccountElements() {
			
			var returnVar = true;
			var objError = $("#newUser").find(".msgError")
			objError.html("");
			initRequiredFields();

			if( $("#email").val() == '' || $("#email").val() == 'E-mail' ){
				$("#email").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#pwd").val() == '' || $("#pwd").val() == 'Mot de passe' ){
				$("#pwd").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#pwd_confirm").val() == '' || $("#pwd_confirm").val() == 'Confirmer mot de passe' ){
				$("#pwd_confirm").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $( '#customer_profile :selected' ).val() != '1' && ($("#company").val() == '' || $("#company").val() == 'Raison sociale') || $("#company").val() == 'Entreprise') {
				$("#company").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#firstname").val() == '' || $("#firstname").val() == 'Prénom' ){
				$("#firstname").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}

			if( $("#lastname").val() == '' || $("#lastname").val() == 'Nom' ){
				$("#lastname").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#adress").val() == '' || $("#adress").val() == 'Adresse' ){
				$("#adress").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#zipcode").val() == '' || $("#zipcode").val() == 'Code postal' ){
				$("#zipcode").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#city").val() == '' || $("#city").val() == 'Ville' || $("#city").val() == 'Commune' ){
				$("#city").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}
			
			if( $("#phonenumber").val() == '' || $("#phonenumber").val() == 'Téléphone' ){
				$("#phonenumber").css("cssText", "background-color: #ffcccc !important;");
				returnVar = false;
			}	
	
			if(returnVar == true) {
				document.getElementById("RegisterForm").submit();
			} else {
				objError.show();
				objError.html("Merci de renseigner les champs signalés en rouge.");
			}
		}
		
	
		function initRequiredFields() {
			/* Normal fields */
			$('input').each( function(){ $(this).css("background-color", "#ffffff;"); } );
			
			/* Required fields */
			$("#email").css("cssText", "background-color: #ecfdfc !important;");
			$("#pwd").css("cssText", "background-color: #ecfdfc !important;");
			$("#pwd_confirm").css("cssText", "background-color: #ecfdfc !important;");
			$("#company").css("cssText", "background-color: #ecfdfc !important;");
			$("#firstname").css("cssText", "background-color: #ecfdfc !important;");
			$("#lastname").css("cssText", "background-color: #ecfdfc !important;");
			$("#phonenumber").css("cssText", "background-color: #ecfdfc !important;");
			$("#adress").css("cssText", "background-color: #ecfdfc !important;");
			$("#zipcode").css("cssText", "background-color: #ecfdfc !important;");
			$("#city").css("cssText", "background-color: #ecfdfc !important;");
		}

		
		
		jQuery(document).ready(function() {
			$("#newUser .blocTitle").click(function () {
				$("#newUser .toggle").toggle("slow");
			});
		});
		
	/* ]]> */
	
	</script>
</head>
<body onLoad="initRequiredFields();">
<flexy:include src="head/banner.tpl"></flexy:include>
<flexy:include src="menu/navigation.tpl"></flexy:include>

<div id="container">

	<!-- Contenu -->
	<div id="content">
{if:iduser}<span style="color:red;font-weight: bold;">/account/register_order.tpl</span>{end:}
		<flexy:include src="page/left.tpl"></flexy:include>
		
		<div id="categorybloc">
			<div class="intro">
				<h2>Identification : plus que quelques pas avant la livraison</h2>
				<ul class="basketSteps">
					<li class="step1"><a href="basket.php?voucher_code={voucher_code}">Validation du panier</a></li>
					<li class="step2"><a>Identification</a></li>
					<li class="step3"><a class="alpha40">Récapitulatif et paiement</a></li>
					<li class="step4"><a class="alpha40">Confirmation de commande</a></li>
				</ul>
			
				<div id="text">
					<!-- Déja client -->
					<div id="registeredUser">
						<p><strong>Déjà client ? Connectez-vous.</strong></p>
						<div class="spacer"></div><br />
				
						{if:issetPostLoginAndNotLogged}
							<!-- Erreur lors de l'identification -->
							<p class="msgError">Votre identifiant ou mot de passe est incorrect.</p>
						{else:}
							<p class="msgError"></p>
						{end:}
						
						<form action="{baseURL}/catalog/order.php" id="LoginForm" id="LoginForm" onSubmit="verifLogin(); return false;" method="post">
							<input type="text" name="Login" id="login_email" class="loginField input_email" value="" />
							<input type="password" name="Password" id="login_pwd" class="loginField input_password" value="" /> 		
							<p><a href="#" onClick="forgetPassword(); return false;" class="passwordForget">Mot de passe oublié ?</a></p>
							<div class="spacer"></div>
							
							<p><input type="submit" class="submitField" value="Connexion" /></p>
						</form>
						<div class="spacer"></div>
					</div>
	
					<!-- Nouveau client -->
					<div id="newUser" class="bloc">
						<p class="blocTitle clickable"><strong>Nouveau client ? <a>Créer votre compte.</a></strong></p>
						
						<div class="toggle">
							<h2 style="margin-top: 10px;">Quels avantages ?</h2>
							<p>De votre espace client consultez vos devis et vos commandes.<br/>
							Vous avez la possibilité de gérer vos adresses de livraison et de facturation.<br/>
							Accédez à la totalité de nos services (aides, liste personnelle, édition de factures...)</p>
							
							<p style="margin-top: 10px;">Tous les champs marqués d'un <span class="required">fond bleu</span> sont obligatoires.</p>
						
							{if:errorInscription}
								<p class="msgError">{errorInscription:h}</p>
							{else:}							
								<p class="msgError" style="display:none;"></p>
							{end:}
		
							<form action="{baseURL}/catalog/register.php" id="RegisterForm" name="RegisterForm" onSubmit="verifCreateAccountElements(); return false;" method="post"> 
								<input type="hidden" name="redirectTo" value="order.php" flexy:ignore/>									
								<input type="hidden" name="idsource" value="1" />
								
								<!--<input type="hidden" name="FromScript" value="{scriptName}" />-->
								<p><strong>Informations de connexion à mon compte</strong></p>
								<div class="spacer"></div>
									   
								<p>
									<input type="text" name="email" id="email" value="" class="loginField input_identifiant" maxlenght="20" />
									<input type="password" name="pwd" id="pwd" value="" class="loginField input_password" maxlength="20" />
									<input type="password" name="pwd_confirm" id="pwd_confirm" value="" class="loginFieldMedium marginLeft input_conf_password" maxlength="20" />
								</p>
								<div class="spacer"></div><br/>
								
								<p><strong>Informations personnelles</strong></p>	
								
								<p>						
									Je suis
									<select name="idcustomer_profile" id="customer_profile" onchange='setFormType();' class="selectIdcustomer" style="margin: 0;">
										<option value="2" selected="selected">une entreprise</option>
										<option value="1">un particulier</option>
										<option value="4">une administration</option>
										<option value="3">une association</option>
									</select>
								</p>
								<div class="spacer"></div> 
												
								<div class="proPart">
									<input type="text" name="company" id="company" maxlength="50" class="loginField input_company" value="" />
								</div>
								<div class="spacer"></div> 
														
								<p class="blocTextNormal" id="selectTitle" style="margin:8px 0 0 0;">
									Civilité : 
									<input type="radio" name="title" value="1" checked="checked" /> M.
									<input type="radio" name="title" value="2" /> Mme
									<input type="radio" name="title" value="3" /> Mlle
								</p>
								<div class="spacer"></div>
								
								<input type="text" name="firstname" id="firstname" maxlength="50" class="loginFieldMedium marginLeft input_prenom" value="" />
								<input type="text" name="lastname" id="lastname" maxlength="50" class="loginFieldMedium input_nom" value="" />
								<div class="spacer"></div>
								
								<div class="proPart">
									<div class="selectField">{userInfoDepartment:h}</div>
								</div>
								<div class="spacer"></div> 
								
								<input type="text" name="phonenumber" id="phonenumber" maxlength="20" class="loginFieldMedium input_phone" value="" />		
								<input type="text" name="faxnumber" maxlength="20" class="loginFieldMedium marginLeft input_fax" value="" />
								<div class="spacer"></div>
								
								<input type="text" name="gsm" maxlength="20" class="loginFieldMedium input_gsm" value="" />
								<div class="spacer"></div><br/>
								
								<!-- <p>Selon la loi "Informatique et libertés" du 05/01/76, vous disposez d'un droit d'accès, de rectification et de suppression des données qui vous concernent.</p> -->
								
								
								<p><strong>Informations complémentaires</strong></p>
								<div class="spacer"></div>
								
								<input type="text" name="siret" maxlength="14" class="loginField input_siret" value="" />	
								<div class="spacer"></div><br/>
								
								<p class="blocTextNormal"><strong>Adresse postale</strong></p>
												
								<input type="text" name="adress" id="adress" maxlength="100" class="loginField input_adress" value="" />
								<div class="spacer"></div>
								
								<input type="text" name="adress_2" maxlength="100" class="loginField input_adresscomp" value="" />
								<div class="spacer"></div>
								
								<input type="text" name="zipcode" id="zipcode" maxlength="10" class="loginFieldMedium input_cp" value="" />	
								<input type="text" name="city" maxlength="50" class="loginFieldMedium marginLeft input_city" value="" />				
								<div class="spacer"></div>
								
								<div class="selectField">{userInfoState:h}</div>			
								<!-- <input type="text" name="naf" maxlength="15" class="loginFieldMedium marginLeft input_naf" value="{userInfoNaf:h}" /> -->
								<div class="spacer"></div>
								
								<!-- <p><input type="checkbox" name="newsletter" value="1" checked="checked" />&nbsp;Abonnement à la newsletter du site.</p> -->
								
								<p><input type="submit" class="submitField" value="Etape suivante" name="btnRegister" /></p>
								<div class="spacer"></div>
								
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div id="forgetPassword">
			<div id="forgetPasswordResponse">
				<p class="blocTextNormal"><strong>Saisissez votre adresse e-mail pour récupérer vos identifiants</strong></p>
				<input type="text" id="Email" name="Email" value="" class="loginFieldMedium input_email" style="width:285px;" />
				<input type="submit" name="Envoyer" value="Envoyer" class="submitField" style="float:left;" onClick="sendPassword( $('#Email').val(), '#EmailResponse' );" />
			</div>
		</div>
		
		<div id="forgetEmail" style="display:none;">
			<div id="forgetEmailResponse" style="margin:0 0 0 50px; width:425px;">
				<p class="blocTextNormal" style='text-align:center;'><strong>Cliquez pour récupérer vos identifiants par email</strong><br/>
				<input type="hidden" id="forgetEmailInput" name="forgetEmailInput" value="" class="loginFieldMedium input_email" style="width:285px;" />
				<input type="submit" name="forgetEnvoyer" value="Récupérez mes identifiants" class="submitField" onClick="sendPassword( $('#forgetEmailInput').val(), '#forgetEmailResponse' );" style="float:none;" /></p>
			</div>
		</div>


	</div>
	<div class="spacer"></div>

</div>
	
<!-- footer -->	
<flexy:include src="foot.tpl"></flexy:include>