{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
	<!-- Header -->


	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/show_invoice.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
			</nav>
          
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">

	
		<h2>Facture n°{idinvoice}</h1>
		<br /><br />
		Votre facture a été enregistrée le {invoice_date:h}.
		<br /><br />

		<!-- ******************* Pied de commande ****************** -->
		<table class="accountTable" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<th><strong>Adresse de livraison :</strong></th>
				<th><strong>Adresse de facturation :</strong></th>
			</tr></tr>
				<td>{if:buyer_company}{buyer_company}<br />{end:}
			{buyer_title} {buyer_firstname} {buyer_lastname}<br />
			{buyer_adress:h}<br />
			{buyer_zipcode} {buyer_city}</td>
				<td>{if:d_buyer_company}{d_buyer_company}<br />{end:}
			{d_buyer_title} {d_buyer_firstname} {d_buyer_lastname}<br />
			{d_buyer_adress:h}<br />
			{d_buyer_zipcode} {d_buyer_city}</td>
			</tr>
		</table>

		
		
		<div class="spacer"></div><br/>

		<!-- *************** Articles de la commande *************** -->
		<table class="accountTable" cellspacing="0" cellpadding="0" border="0">
			<tr>			
				<th>Produit</th>
				<th>Référence & Caractéristiques</th>
				<th>Qté</th>
				{if:hasLot}
					<th>Qté / lot</th>
				{end:}
				<th>P.U. Brut HT</th>
				<th>Remise</th>	
				<th>P.U. Brut HT</th>
				<th style="background:#F9F9F9;">Total Net HT</th>
				
			</tr>
			
			<!-- ************ Construction du tableau des articles ************ -->
			{foreach:invoice_articles,line}
				<tr>
					<td style="text-align:center;">
						{if:line[Icon]}
							<img src="{line[Icon]:h}" alt="Icone" width="40" />
						{else:}
						
						{end:}
					</td>
					<td style="text-align:left;">
						<strong class="tdPrice">Référence : {line[reference]:h}</strong><br />
						{line[summary]:h}
					</td>
					<td>{line[quantity]:h}</td> 
					{if:hasLot}
						 <td>{line[lot]:h}</td>
					{end:}
					<td>{line[unit_price]:h}</td>
					<td>{line[ref_discount]:h}</td>
					<td class="tdPrice">{line[discount_price]:h}</td> 
					<td class="tdPrice">{line[RefTotalAmountHT]:h}</td>
				</tr>
			{end:}
		</table>
		<div class="spacer"></div>
		
		<!-- *************** Total de la commande *************** -->
		<!-- Conditions -->
		
		<table>
			<tr>
				</td>
	<table class="sumDevisTable" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="2" align="center">&nbsp;</td>
		</tr>
		<tr>
			<td align="center"><strong>Délai de paiement*</strong><br/>
			{PaymentDelay:h}</td>
			<td align="center"><strong>Mode de règlement</strong><br/>
			{PayMod:h}</td>
		
		</table>
		
				</td><td>
			

		<!-- Total du devis -->
		<table class="sumDevisTable" border="0" cellpadding="0" cellspacing="0">	
		{if:remise}
			<tr>
				<th align="left">Montant HT</th>
				<td align="right">{total_amount_ht_avant_remise:h} &nbsp;</td>
			</tr><tr>
				<th align="left">Taux remise</th>
				<td align="right">{remise_rate:h}&nbsp;</td>
			</tr><tr>
				<th align="left">Total HT avec remise</th>
				<td align="right">{total_amount_ht:h}&nbsp;</td>
			</tr><tr>	
				<th align="left">Remise</th>
				<td align="right">{discount:h}&nbsp;</td>
			</tr>
		{end:}
		
	
		<tr>
			<th align="left">Frais de port et emballage</th>
			<td align="right">{total_charge_ht:h}</td>
		</tr>
		<tr>
			<th align="left">Montant Total HT</th>
			<td align="right">{total_amount_ht_wb:h}</td>
		</tr>
		<tr>
			<th align="left">TVA ({getVATRate:h})</th>
			<td align="right">{getVATAmount:h}</td>
			
			
		</tr>
		<tr>
			<th class="tdPrice" align="left">Montant total TTC</th>
			<td class="tdPrice" align="right">{total_amount_ttc_wbi:h}</td>
		</tr>
		</table>
				</td>
			</tr>
		</table>

		

	<div class="spacer"></div>	
		</div>

	</div>
</div>	
</div>	
</div>	

<flexy:include src="/foot.tpl"></flexy:include>	

{end:}