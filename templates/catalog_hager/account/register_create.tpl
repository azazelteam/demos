	<flexy:include src="head/header.tpl"></flexy:include>
</head>
<body>
<!-- Header -->
<flexy:include src="head/banner.tpl"></flexy:include>

<div id="content">
{if:iduser}<span style="color:red;font-weight: bold;">/account/register_create.tpl</span>{end:}
	<flexy:include src="page/left.tpl"></flexy:include>
		
	<div id="categorybloc">
		<div class="intro">
			<h2>Identification</h2>
			
			<div id="text">
				
				<!-- Création de compte -->
				<form action="{baseURL}/catalog/{scriptName}?voucher_code=&create_account" id="RegisterForm" name="RegisterForm" method="post"> 
				{if:gift_token_serial}
					<!-- chèque-cadeau -->
					<input type="hidden" name="gift_token_serial" value="{gift_token_serial}" />
				{end:}
				{if:voucher_code}
					<!-- bon de réduction -->
					<input type="hidden" name="voucher_code" value="{voucher_code}" />
				{end:}
				<input type="hidden" name="FromScript" value="{scriptName}" />
				
				<p><strong>Informations personnelles</strong></p>
				
				<!-- Remplissez le formualire suivant -->
				{if:errorMsgNotEmpty}
					<p class="msgError">{errorMsg:h}</p>
				{end:}

				<!-- On récupère les infos passées en POST -->
				<input type="hidden" name="email" value="{userInfoEmail:h}" />
				<input type="hidden" name="pwd" value="{userInfoPwd:h}" />
										
				<div class="selectTitle">{userInfoTitle:h}</div>
				<input type="text" name="lastname" maxlength="50" class="loginFieldSmall marginLeft input_nom" maxlength="20" value="{userInfoLastname:h}" />
				<input type="text" name="firstname" maxlength="50" class="loginFieldSmall marginLeft input_prenom" value="{userInfoFirstname:h}" />
				<div class="spacer"></div>
				
				<div class="selectField">{userInfoDepartment:h}</div>
				<div class="spacer"></div>
				
				<input type="text" name="phonenumber" maxlength="20" class="loginFieldMedium input_phone" value="{userInfoPhone:h}" />		
				<input type="text" name="faxnumber" maxlength="20" class="loginFieldMedium marginLeft input_fax" value="{userInfoFax:h}" />
				<div class="spacer"></div>
				
				<input type="text" name="gsm" maxlength="20" class="loginFieldMedium input_gsm" value="{userInfoGsm:h}" />
				<div class="spacer"></div>
				
				<input type="text" name="optin" maxlength="20" class="loginFieldMedium input_optin" value="{userInfoOptin:h}" />
				<div class="spacer"></div>
				
				<input type="text" name="birthday" maxlength="20" class="loginFieldMedium input_birthday value="{userInfoBirthday:h}" />
				<div class="spacer"></div>
				
				<p><input type="checkbox" name="newsletter" value="1" checked="checked" />&nbsp;Abonnement à la newsletter du site.</p>
				<p>Selon la loi "Informatique et libertés" du 05/01/76, vous disposez d'un droit d'accès, de rectification et de suppression des données qui vous concernent.</p>
			

				<p><strong>Informations société</strong></p>
				
				<input type="text" name="company" maxlength="50" class="loginField input_company" value="{userInfoCompany:h}" />
				<div class="spacer"></div>
				
				<input type="text" name="adress" maxlength="100" class="loginField input_adress" value="{userInfoAdress:h}" />
				<div class="spacer"></div>
				
				<input type="text" name="adress_2" maxlength="100" class="loginField input_adresscomp" value="{userInfoAdress_2:h}" />
				<div class="spacer"></div>
				
				<input type="text" name="zipcode" maxlength="10" class="loginFieldMedium input_cp" value="{userInfoZipcode:h}" />	
				<input type="text" name="city" maxlength="50" class="loginFieldMedium marginLeft input_city" value="{userInfoCity:h}" />				
				<div class="spacer"></div>
				
				<div class="selectField">{userInfoState:h}</div>
				<div class="spacer"></div>	
				
				<input type="text" name="siret" maxlength="14" class="loginFieldMedium input_siret" value="{userInfoSiret:h}" />					
				<input type="text" name="naf" maxlength="15" class="loginFieldMedium marginLeft input_naf" value="{userInfoNaf:h}" />
				
				<input type="submit" class="submitField" value="Etape suivante" name="Register" />
				<div class="spacer"></div>

				<input type="hidden" name="idsource" value="{param_admin_default_buyer_source:h}" />
				<div class="spacer"></div>
				
				</form>
			</div>
			<div class="spacer"></div><br/>
		</div>
	</div>

</div>
<div class="spacer"></div>
	
<!-- footer -->	
<flexy:include src="foot.tpl"></flexy:include>	
		
		