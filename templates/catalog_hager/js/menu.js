
if (Modernizr.touch) {
$('#menu > ul > li > a').each(function(){
var mySub = $(this).parent().find('.submenu');
if (mySub.length > 0) $(this).attr('href', 'javascript:;');
});
$('#menu > ul li a span').mousedown(function(e){
showsubmenu(e);
});
} else {
if (ie8) {
$('#menu > ul li a span').mouseover(function(e){
showsubmenu(e);
});
} else {
$('#menu > ul li a span').hoverIntent({
over: function(e){showsubmenu(e);},
interval: 70,
out: function(){}
});
}
$('#menu > ul > li > a').mouseenter(function(e) {
clearTimeout(timerreset);
})
$('#menu .submenu').mouseenter(function(e) {
clearTimeout(timerreset);
})
$('#menu .submenu').mouseleave(function(e) {
timerreset = setTimeout(menureset, 500);
})
$('#menu > ul > li > a').mouseleave(function(e) {
timerreset = setTimeout(menureset, 500);
})
}
var timerreset;
function showsubmenu (e) {
e.stopPropagation();
clearTimeout(timerreset);
var myBtn = $(e.target).parent();
var mySub = $(e.target).parent().parent().find('.submenu');
menureset();
myBtn.animate({ color: '#f39900', backgroundColor: '#fff' }, 150, 'easeOutCirc' );
if (mySub.length == 0) myBtn.addClass('nosub');
if (mySub.position().top == 5 && mySub.length > 0) {
mySub
.css('margin-top','-15px')
.css('opacity','0')
.css('display','block')
.animate({ marginTop: '-5px', opacity: 1 }, 300, 'easeOutCirc' );
}
}
function menureset () {
clearTimeout(timerreset);
$('#menu > ul > li > a')
.stop()
.animate({ color: '#fff', backgroundColor: 'transparent' }, 100, 'easeOutCirc' )
.removeClass('nosub');
$('#menu').find('.submenu')
.css('marginTop','-5px')
.css('display','none');
}
