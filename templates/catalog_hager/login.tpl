<script src="{baseURL}/js/jquery.simpletooltip.js" type="text/javascript"></script>
<script src="{baseURL}/js/jquery/jquery.form.js" type="text/javascript"></script>


<script type='text/javascript'>
	<!--
	// Test des champs du formulaire de login
	
	function verifLogin (){
		var login = document.forms.RegisterForm.elements[ 'Login' ].value;	 
		var password = document.forms.RegisterForm.elements[ 'Password' ].value;
			
		if(login == ''){
			alert('Le champ Identifiant est vide !!!');
			return false;
		}
		
		if(password == ''){
			alert('Le champ Mot de passe est vide !!!');
			return false;
		}
		return true ;
	}
		
	
	function verifCreateAccount() {
		
		var data = $('#CreateAccount').formSerialize(); 
		
		$.ajax({
			url: "/catalog/account.php?createLoginAjax",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ },
		 	success: function( responseText ){
		 		
		 		if(responseText != '') {
					$("#returnCreateAccount").html(responseText); 
				} else {
					document.getElementById("CreateAccount").submit();
					
				}

			}
		});
	}
	
	
	
	
	// -->
	</script>	
		
		
				
{if:com_step_empty}
	<div id="centre">
	{if:iduser}<span style="color:red;font-weight: bold;">/login.tpl</span>{end:}
{end:}
<b>login.tpl<b>
	<!-- Si l'identification a échoué -->
	{if:issetPostLoginAndNotLogged}
		<!-- Erreur lors de l'identification -->
		<span class="msg_error" style="text-align: center;">{translateMSGErrorLoginPassword:h}</span>
	{end:}
	<!-- Formulaire d'identification -->
	<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/login.css" />
	
	<form action="{baseURL}/catalog/account.php" method="post">
						{if:issetPostLoginAndNotLogged}
							<!-- Erreur lors de l'identification -->
							<p class="msgError">Votre identifiant ou mot de passe est incorrect.</p>
						{end:}

		<table class="form_login" />
			<tr>
				<td colspan="2" style="padding:5px 0px 0px 5px;"><span class="txtAuthBig">Vous êtes déja client ?</span>&nbsp;<img src="{start_url}/templates/catalog/images/arow-grey.png">&nbsp; <span class="txtAuthSmall">Identifiez-vous</span></td>
			</tr>
			<tr>
				<td class="" style="padding-left:10px;"><span class="txtAuthMonCpt">{translateLogin:h}</span></td>
				<td><input type="text" name="Login" size="25" class="AuthInput" /></td>
			</tr>
			<tr>
				<td class="" style="padding-left:10px;"><span class="txtAuthMonCpt">{translatePassword:h}</span></td>
				<td ><input type="password" name="Password" size="25" class="AuthInput" /></td>
			</tr>
			<tr>
				<td class="forgetPassword" colspan="2" style="padding-left:10px;"><br /> <a href="javascript:mail()" class="forgetPassword">Vous avez oublié votre mot de passe - Cliquez ici</a></td>
			</tr>
			<tr>
				<td class="btOk" colspan="2"><input name="ok" type="image" src="{start_url}/templates/catalog/images/{img}" style="{style}" onclick="return verifLogin();">
<input type="submit" class="submitField" value="Connexion" />

</td>
			</tr>
		</table>
	</form>	
{if:com_step_empty}
	</div>
{end:}