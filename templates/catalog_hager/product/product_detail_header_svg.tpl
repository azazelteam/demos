
{if:iduser}<span style="color:red;font-weight: bold;">/product/product_detail_header.tpl</span>{end:}

<div class="zoom-lightbox" id="zoom-lightbox">
    <a href="javascript:void(0);" class="zoom-lightbox-close" id="zoom-lightbox-close">
        <i class="fas fa-times"></i>
        <span class="sr-only">Fermer</span>
    </a>
    <img  itemprop="image" src="{product.getZoom()}" alt="{product.get(#name_1#):h}" title="{product.get(#name_1#):h}">
</div>

<div class="visuel col-md-6">
    <div class="mainImage">
        {if:product.getZoom()}
        <a href="{product.getImage(#280#):h}" class="productZoom" id="zoomTrigger" title="{product.get(#name_1#):h}" data-rel='galProduct'>
            <div class="promoProduct" title="Promotion" flexy:if="product.getPromotionCount()">
                <span class="ruban promo"></span>    
            </div>
            <div class="labelZoom" title="Survoler le visuel pour l'agrandir"></div>
        {end:}
        <img id="prod-img" itemprop="image" src="{product.getImage(#280#):h}" alt="{product.get(#name_1#):h}" title="{product.get(#name_1#):h}">
        {if:product.getZoom()}	
            </a>
        {end:}
        <div class="spacer"></div>
    </div>        
    {if:product.getImages()}
    <div class="thumbs_list">
        <a href='{product.getZoomURI(index):h}' data-rel="{gallery: 'galProduct', smallimage: '{product.getImage(#180#,index):h}',largeimage: '{product.getImage(#800#,index):h}'}" title="{product.get(#name_1#):h}" flexy:foreach="product.getImages(1024,0),index,image"><img class="withbord" src="{product.getImage(#51#,index):h}" width="51" height="51" alt="{product.get(#name_1#):h} image {index}" title="{product.get(#name_1#):h} image {index}">
        </a>
    </div>
        <div class="spacer"></div>
    {end:}
    
    <ul class="normes">
        <li flexy:foreach="product.getPrimaryPictogram(),picto"><img src="{picto.image}" alt="{picto.name}" title="{picto.name}" class="pictos primary"></li>
        <li flexy:foreach="product.getSecondaryPictograms(),picto"><img src="{picto.image}" alt="{picto.name}" title="{picto.name}" class="pictos secondary"></li>
    </ul>
    
    
</div><!--visuel-->
			
<script type="text/javascript">$(document).ready(function(){var e={zoomType:"standard",lens:true,preloadImages:true,preloadText:"Chargement...",alwaysOn:false,zoomWidth:340,zoomHeight:340,title:false};$(".productZoom").jqzoom(e)})</script>
