


<!-- Zoom -->
{head.addCSS(#/templates/catalog/css/jquery/jquery.jqzoom.css#)}
{head.addScript(#/templates/catalog/js/jquery.jqzoom-core.js#)}
{head.addScript(#/templates/catalog/js/jquery.lightbox-0.5.js#)}
<!--  Onglets -->
{head.addCSS(#/templates/catalog/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css#)}


<!--
{head.addCSS(#/templates/catalog/css/popup_basket.css#)}
{head.addCSS(#/templates/catalog/css/jquery.lightbox-0.5.css#)}

{head.addScript(#/js/jquery.ui.stars-2.0/ui.stars.js#)}
{head.addCSS(#/js/jquery.ui.stars-2.0/ui.stars.css#)}
{head.addCSS(#/css/jquery/jquery.tabs.css#)}
{head.addScript(#/js/fireMyPopup.js#)}
{head.addCSS(#/js/thickbox/thickbox.css#)}
{head.addScript(#/js/jquery/jquery.qtip.min.js#)}
-->

<flexy:include src="header.tpl"></flexy:include>	
{lexiconJS:h}
<script type="text/javascript">function displayLexicon(e,t){if(!lexique[t])return;$(e).qtip({content:lexique[t]})}function forgotPassword(){$("#forgotPassword").dialog({title:"Mot de passe oubli&eacute; ?",bgiframe:true,modal:true,width:550,minHeight:10,closeText:"Fermer",close:function(e,t){$("#forgotPassword").dialog("destroy")}})}function sendPassword(e,t){data="&Email="+e;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(e,t,n){alert("Impossible de r&eacute;cup&eacute;rer l'email")},success:function(e){$(t).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+e+"</p>")}})}$(function(){$("#gallery a").lightBox()})</script>
<!--====================================== Contenu ======================================-->
<div id="center" class="page detail_produit catalog detail">
{if:iduser}<span style="color:red;font-weight: bold;">/product/chartep_one.tpl</span>{end:}<br>
	 <ul class="pathway">
                   <li><a href="http://www.abisco.fr" class="pwhome" title="Equipement de protection individuelle"><img class="noborder" alt="Accueil" width="16" height="15" src="{starturl}/templates/catalog/images/icon-home.png" /></a></li>			  		
                    <li flexy:foreach="product.getPath(),node">
                        <a href="{node.getURL()}" title="{node.get(#name_1#):h}">{node.get(#name_1#):h}</a>
                    </li>
                    <li>
                    	<a href="{producturl}">{product.get(#name_1#):h}</a>
                    </li>
                </ul>
   <!--<div id="panier-b">
            	<a title="Consulter mon panier" class="part-l" href="/catalog/basket.php">
                	<span class="total"><strong>Mon panier <span class="strong BasketItemCount">
                    {if:basket.getItemCount()}
                                        {basket.getItemCount()}</span> article(s)</span>
                                    {else:}
                                        0</span> article(s)</span>
                                    {end:}
                   </strong></span></a>     
                    <a title="Voir le panier" class="part-r" href="/catalog/basket.php">            
                    {if:basket.getItemCount()}
                    	<span class="total">Total : <span class="strong">{basket.getTotalET():h}</span></span>
                    {else:}
                    	<span class="total">Total : <span class="strong">0 &euro;</span></span>
                    {end:}
					<span class="btn-med">Voir le panier</span>
                </a>
   </div>-->
   <a class="btn-med grey" href="javascript:history.go(-1)" style="display:block;float:left">Retour &agrave; la liste</a>
   <div class="clear"></div>
   {if:category.getParentCateg()}
	<aside class="product centrale cbloc1">
	<h3>Catégories Voisines</h3>
	{category.getParentCateg():h}
	</aside>
{end:}
   
   
	<div id="product_detail" itemtype="http://schema.org/Product" itemscope="">
		<flexy:include src="product/product_detail_{productTemplate}.tpl"></flexy:include>
	</div>
</div>
<!-- autoselect de la première r&eacute;f&eacute;rence dans la liste des ref detail_8 -->
<script type="text/javascript">$(document).ready(function(){var e=document.getElementById("HeadingValues_0");if(e&&e.options){e.options[0].selected=true;var t=e.options[0].value;selectArticle(t,0);e.options[0].selected=true;if(e.options.length<=1){document.getElementById("selectionHeader").style.display="none"}}})</script>    
<!-- Ativando o jQuery lightBox plugin -->
<script type="text/javascript">$(function(){$('#gallery a').lightBox();});</script>
<!-- Fonctions qui font la mini popup -->
<FLEXY:TOJAVASCRIPT globalstarturl="baseURL">
<!--====================================== Fin du contenu ======================================-->        
</div>
</div>

        
<div id="waiting-block" style="display:none;position:fixed;top:50%;left:48%;z-index:1000;"><img src="{starturl}/images/loader.GIF" /></div>
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>