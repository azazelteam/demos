

<div class="discountRate" title="Promotion" flexy:if="product.getPromotionCount()" >
    	{Math.counter.reset(1)}
    	<span flexy:foreach="product.getArticles(),article">
			{if:Math.counter.value()}
	            {if:article.getDiscountRate()}
					<span class="rate">
						-{article.getDiscountRate():h}
						{if:Math.counter.value()}
							{Math.counter.dec()}
						{end:}
					</span>
                {end:}
        	{end:}
		</span>
    </div>
						
						<a title="" class="btn-buy"  href="{product.getURL()}" style="left: -44px;"></a>
                            <a href="{product.getURL()}" itemprop="url" >
                            	<span class="ruban promo" flexy:if="product.getPromotionCount()"></span>
                                <img  itemprop="image" width="170" class="product_image" height="170" src="{product.getImageURI(#170#)}" alt="{product.get(#name_1#):h}" />
                                <span class="small product_reference" ><span class="strong" >{product.getReferenceCount()} &nbsp;</span> r&eacute;f&eacute;rence(s)</span>
                                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="price">
                                    <small style="font-size:0.8em">&agrave; partir de</small><br />
                                    <span itemprop="price" class="strong">{product.getLowerPriceET():h} <small>HT</small></span>
									<br />
                                      <span class="strong smallttc">{product.getLowerPriceATI():h}<small>TTC</small></span>
                                </span>
                            </a>
								<h2 style="font-size:0.85em" itemprop="name"><a  class="product-name" href="{product.getURL()}">{product.get(#name_1#):h}</a></h2>

<img flexy:if="product.getLabel(#idlabel#)" src="{product.getLabel(#image#)}" itemprop="image"  alt="{product.getLabel(#name#):h}" title="{product.getLabel(#name#):h}" style="margin-left:5px;"/>