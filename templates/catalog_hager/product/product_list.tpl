{head.addCSS(#/templates/catalog/css/chartec.css#)}
{head.addCSS(#/templates/catalog/css/category.css#)}
<flexy:include src="header.tpl"></flexy:include>
<flexy:include src="leftBanner.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->

<div id="center" class="page">

		<h1 class="CategoryName">{category.get(#name_1#)}</h1>
  		<ul class="pathway">
			<li><a href="{baseURL}" class="pwhome" title="Equipement de protection individuelle">Equipement de protection individuelle</a></li>
	    	<li flexy:foreach="category.getPath(),node">
	    		<a href="{node.getURL()}" title="{node.get(#name_1#):h}">{node.get(#name_1#):h}</a>
	    	</li>
    	</ul>
	
		<div class="onglettop"></div>					
						
		<ul class="listprods" flexy:foreach="category.getProducts(),product">
			<li class="productDisplayOne" style="vertical-align:top; height:120px;">
				<flexy:include src="/product/product_detail_13.tpl"></flexy:include>			
			</li>
		</ul>										

	<br style="clear:both;">
	<p flexy:if="category.get(#description_plus_1#)">{category.get(#description_plus_1#):h}</p>	

    <div class="ongletbottom"><a href="#top" class="toppage">haut de page</a></div>

</div><!-- centre -->
<!--====================================== Fin du contenu ======================================-->
<!-- Colonne de droite de la page -->
<!--<flexy:include src="b_right.tpl"></flexy:include>    -->        
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 