<flexy:include src="/head/header.tpl"></flexy:include>
<!--
<flexy:include src="b_left.tpl"></flexy:include>-->
<!--flexy:include src="menu/menu.tpl"></flexy:include-->

<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/sse.css" />

<!--====================================== Contenu ======================================-->
<div id="center" class="page">
    {if:iduser}<span style="color:red;font-weight: bold;">/product_novelty.tpl</span>{end:}
    <!-- début div centre -->
    <h1 class="sections_title">Nouveautés</h1>
    <p class="category_description" id="categoryTitle">
        {foreach:descriptions,desc}
        {desc:h}
        {end:}
    </p>
    <div class="spacer"></div>
    <div class="pagination" flexy:if="Math.sup(pageCount,#1#)">
        <flexy:tojavascript page="page">
            <flexy:tojavascript pageCount="pageCount">
                <script type="text/javascript">
                    /* <![CDATA[ */
                    /* ------------------------------------------------------------------------------------------------ */
                    function goToPage(page) {
                        $("#SearchForm").find("input[name=page]").val(page);
                        $("#SearchForm").submit();
                    }
                    /* ------------------------------------------------------------------------------------------------ */
                    var windowSize = 3;
                    if (page > 2)
                        document.write('<a href="#" onclick="goToPage(1); return false;">D&eacute;but</a>');
                    if (page > 1) {
                        var prev = page - 1;
                        document.write('<a href="#" onclick="goToPage(' + prev + '); return false;">Pr&eacute;c&eacute;dent</a>');
                    }
                    var i;
                    if (page - windowSize > 1) {
                        document.write("...");
                        i = page - windowSize;
                    } else i = 1;
                    while (i <= pageCount && i < page + windowSize + 1) {
                        if (i == page)
                            document.write("<b class=\"active\">" + page + "</b>");
                        else document.write('<a href="#" onclick="goToPage(' + i + '); return false;">' + i + '</a>');
                        i++;
                    }
                    if (page + windowSize < pageCount)
                        document.write("...");
                    if (page < pageCount) {
                        var next = page + 1;
                        document.write('<a href="#" onclick="goToPage(' + next + '); return false;">Suivant</a>');
                    }
                    if (page < pageCount - 2)
                        document.write('<a href="#" onclick="goToPage(' + pageCount + '); return false;">Fin</a>');
                    /* ------------------------------------------------------------------------------------------------ */
                    /* ]]> */
                </script>
    </div>
    <div class="prod-list">
        <ul class="prod-list__wrapper row">
            {foreach:Articles,article}
            <li class="prod-item col-lg-3 col-md-4 col-sm-6" itemscope="" itemtype="http://schema.org/Product">


                <div id="CartAddConfirmation_{article[idArticle]}" style="display: none;">
                                <div class="borderConfirmDevis">
                                    <div class="bgContentDevis">
                                        <div class="contentConfirmCommande">
                                            <div class="spacer"></div>
                                            <br>
                                            <div class="panierConfirmDevis">
                                                <span class="h3">Vous venez d'ajouter &agrave; votre panier :</span>
                                              <div class="spacer"></div>
                                                <table>
                                                     <tr>
                                                        <td class="img"><img src="{article[imageSrc]}" onClick="$('#CartAddConfirmation_{article[idArticle]}').dialog('destroy');" width="70" alt="{article[name]}"></td>
                                                        <td class="txt">{article[name]}</td>
                                                    </tr>
                                                </table>
                                                <a href="#" onClick="$( '#CartAddConfirmation_{article[idArticle]}' ).dialog( 'destroy' ); return false;" class="btn-med grey">Poursuivre ma visite</a>
                                                <a href="{start_url}/catalog/basket.php" class="btn-med grey" >Acc&eacute;der &agrave; mon panier</a>
                                                <div class="spacer"></div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
        
                                <div class="spacer"></div>
                            </div>
                        </div>

                <div class="product-item" idproduct="{article[idproduct]}">
                    
                    <a class="product-item__info" href="{article[getURL]}" itemprop="url">
                        <!-- <span class="ruban promo"></span>  -->
                        <span class="ruban promo" flexy:if="article[getPromotionCount]"><span>-{article[getPromotionRate]:h} %</span></span>
                        <img itemprop="image" class="product-item__img" src="{article[imageSrc]}" alt="{article[imageSrc]:e}">

                        <h4 class="cat-item__title" itemprop="name">{article[name]:e}</h4>

                        <!-- <span class="small product_reference" style="text-align: center;">Réf. :
                            {article[getReference]}</span> -->
                        <div class="price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                            <!-- <small>à partir de</small><br> -->
                            <span itemprop="price" class="strong">
                                {article[getPriceATI]:h} 
                                <small>TTC</small>
                              </span>

                            <a title="Ajouter au panier" class="btn-det" href="#" onclick="addToCart( {article[idArticle]}, 1, 'order' );" >
                               <span>Ajout panier</span>
                            </a>
                        </div>

                    </a>

                </div>
            </li>
            {end:}
        </ul>
    </div>
    <div class="spacer"></div><br />
    <!-- <p>* hors frais de port.</p> -->
    <div class="pagination" flexy:if="Math.sup(pageCount,#1#)">
        <flexy:tojavascript page="page">
            <flexy:tojavascript pageCount="pageCount">
    </div>

    <!-- fin div centre -->
</div>
<!--
<flexy:include src="b_right.tpl"></flexy:include> -->
</div>


<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>

</div>