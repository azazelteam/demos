<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{translations[pageTitle]}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="fr" />
	<link rel="stylesheet" type="text/css" rev="stylesheet" href="{baseURL}/templates/catalog/css/popup.css" />
	<script type="text/javascript" language="javascript">
	<!--
		function checkForm(){
		
			var form = document.forms.MailForm;
			if( form.elements[ 'lastname' ].value == '' ){
				alert( 'Merci de bien vouloir renseigner le champ \'Nom\'' );
				return false;
			}
			
			if( form.elements[ 'email' ].value == '' ){
				alert( 'Merci de bien vouloir renseigner le champ \'Email\'' );
				return false;	
			}
			
			return true;
		}
	// -->
	</script>
    
    <flexy:include src="google_analytics.tpl"></flexy:include>
    
	{if:sent}
		{if:ret}
			<!-- Google Analytics Goal for demande info produit  -->
		{end:}
	{end:}
</head>
<body onload="document.getElementById( 'message' ).innerHTML = 'Saisissez votre message ici...';">
{if:iduser}<span style="color:red;font-weight: bold;">/mail_product.tpl</span>{end:}
<div class="poppupInfo">
	<h1 class="title">Demande d'information produit</h1>
	<h2 class="subtitle">Vous avez s&eacute;lectionn&eacute;</h2>	
	
	<table border="0" cellspacing="0" cellpadding="0" class="tableSelection">
		<tr>
			<td valign="top" align="center" width="75"><img src="{thumbURL}" alt="" /></td>
			<td valign="middle" align="left">{productName}</td>
		</tr>
	</table>
	
	<h2 class="subtitle">Formulaire de contact</h2>
	
	{if:sent}
		{if:ret}
			<p class="infosEnvoiMail">Votre mail a &eacute;t&eacute; envoy&eacute;
				<br />
				Nous vous remercions pour votre demande.
				<br />
				Nous allons vous répondre dans les plus brefs d&eacute;lais
			</p>
			
			<script type="text/javascript" language="javascript">
			<!-- setTimeout( 'window.close()', 5000 ); // -->
			</script>

			<!-- Google Analytics Goal for demande info produit  -->
			<script type="text/javascript">
			/* <![CDATA[ */
			//pageTracker._trackPageview("/analytics-events/product-info-requested");
			//_gaq.push(['_trackEvent', 'ProductInfo', 'Request']);
			_gaq.push(['_trackPageview', '/analytics-events/product-info-requested']);
			/* ]]> */
			</script>
			
			<!-- Google Code for demande info produit Conversion Page -->
			<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 1036812631;
			var google_conversion_language = "fr";
			var google_conversion_format = "3";
			var google_conversion_color = "ffffff";
			var google_conversion_label = "690mCKH_3AEQ14Ky7gM";
			var google_conversion_value = 0;
			/* ]]> */
			</script>
			<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
			</script>
			<noscript>
			<div style="display:inline;">
			<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1036812631/?label=690mCKH_3AEQ14Ky7gM&amp;guid=ON&amp;script=0"/>
			</div>
			</noscript>
		{else:}
			<p class="errorEnvoiMail">Impossible d'envoyer la demande</p>
		{end:}
	{end:}
	
	{if:displayForm}

	<form action="{baseURL}/catalog/mail_product.php" id="MailForm" name="MailForm" method="post" onsubmit="return checkForm( this );"> 
		<input type="hidden" name="idproduct" value="{idProduct}" />
		<div class="form_left">
			<label>{translations[gender]}&nbsp;</label>
			{buyer[title]:h}
			<div class="spacer"></div>
			
			<label>{translations[email]}&nbsp;<span class="asterix">*</span></label>
			<input type="text" name="email" value="{buyer[email]}" maxlength="50" />
			<div class="spacer"></div>
			
			<label>{translations[phonenumber]}</label>
			<input type="text" name="phonenumber" value="{buyer[phonenumber]}" maxlength="50" />
			<div class="spacer"></div>	
			
			<input type="hidden" name="subject" value="{subject}" />
		</div>
		<div class="form_left">
			<label>{translations[lastname]}&nbsp;<span class="asterix">*</span></label>
			<input type="text" name="lastname" value="{buyer[lastname]}" maxlength="50" />
			<div class="spacer"></div>
			
			<label>{translations[company]}</label>
			<input type="text" name="company" value="{buyer[company]}" maxlength="50" />
			<div class="spacer"></div>
			
			<label>{translations[faxnumber]}</label>
			<input type="text" name="faxnumber" value="{buyer[faxnumber]}"  maxlength="50" />
			<div class="spacer"></div>
		</div>
		
		<div class="form_right">
			<label>Pr&eacute;cisez votre demande :</label>
			<textarea name="message" id="message" onblur="if( this.innerHTML == '' ) this.innerHTML = 'Saisissez votre message ici...';" onclick="if( this.innerHTML == 'Saisissez votre message ici...' ) this.innerHTML = '';" rows="6"></textarea>
			
			<p class="pObligatoire">Les champs notés d'une <span class="asterix">*</span> sont obligatoires</p>
			<div class="spacer"></div>	
			<input type="submit" name="send" class="btn_send" value="Envoyez" />
		</div>
	</form>
	{end:}
</div>
</body>
</html>