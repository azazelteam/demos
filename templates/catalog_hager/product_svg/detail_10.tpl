{if:iduser}<span style="color:red;font-weight: bold;">/product/detail_10.tpl</span>{end:}
<link type="text/css" rel="stylesheet" href="{start_url}/js/thickbox/thickbox.css" type="text/css" media="screen" />
<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/popup_basket.css" media="screen" />
<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/product.css" media="screen" />
<span style="display:none;">
	<input type="hidden" id="idproduct" value="{product.getId()}" />
    <input type="hidden" id="idarticle{index}"  value="{article.getId()}" flexy:foreach="product.getArticles(),index,article" />
	<input type="hidden" id="prixtotal{article.getId()}" value="{article.getCeilingPriceET()}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="prixtotalremise{article.getId()}" value="{article.getPriceET()}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="discountRate{article.getId()}" value="{article.getDiscountRate()}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="deliveryTime{article.getId()}" value="{article.getDeliveryTime()}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="hidecost{article.getId()}" value="{article.get(#hidecost#)}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="designation{article.getId()}" value="{article.get(#designation_1#)}" flexy:foreach="product.getArticles(),article" />
    <input type="hidden" id="stockLevel{article.getId()}" value="{article.get(#stock_level#)}" flexy:foreach="product.getArticles(),article" />
	<input type="hidden" id="colorCode{color.id}" value="{color.code}" flexy:foreach="product.getColors(),color" />
	<input type="hidden" id="colorLabel{color.id}" value="{color.label}" flexy:foreach="product.getColors(),color" />
</span>

<script type="text/javascript" src="{baseURL}/js/cart.js"></script>
{if:iduser}<span style="color:red;font-weight: bold;">/product/detail_10.tpl</span>{end:}
<div id="CartAddConfirmation" style="display: none;">
	<div class="borderConfirmDevis">
		<!--<img src="{start_url}/images/front_office/global/topPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />-->
		<div class="bgContentDevis">
			<div class="contentConfirmCommande">
				<div class="spacer"></div>
				<br />
				<div class="panierConfirmDevis">
					<h3>Vous venez d'ajouter &agrave; votre panier :</h3>
					<div class="spacer"></div>
					<table class="tableConfirmDevis" cellspacing="0" cellpadding="0" border="0">
						 <tr>
							<td class="img"><img src="{product.getImageURI():h}" onclick="$('#CartAddConfirmation').dialog('destroy');" width="70" /></td>
							<td class="txt">{product.get(#name_1#):h}</td>
						</tr>
					</table>
					<a href="#" onclick="$( '#CartAddConfirmation' ).dialog( 'destroy' ); return false;" class="btn_continue">Poursuivre ma visite</a>
					<a id="CartRedirectionButton" href="{start_url}/catalog/basket.php" class="btn_send" >Acc&eacute;der &agrave; mon panier</a>
					<div class="spacer"></div>
					<br />
				</div>
			</div>
		</div>
		<img src="{start_url}/templates/catalog/images/bottomPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />
		<div class="spacer"></div>
	</div>
</div>

<div id="panel">
	<script type="text/javascript">
	/* <![CDATA[ */
		
 		/* ----------------------------------------------------------------------- */

		function getSelectedArticle( tableId ){
		
			/* utilisation de la référence */
			
			if( !$( "select[id^='HeadingSelector']" ).size() )
				return $( "#HeadingValues_" + tableId ).val();
				
			/* utilisation des intitulés */
			
			var selection = false;
			
			$( "#ArticleList" ).find( "li" ).each( function( i ){
			
				var identifier = $( this ).attr( "id" ).split( "_" );
				var idarticle = identifier[ 1 ];

				var selected = true;
				
				$( "select[id^='HeadingSelector'][id$='_" + tableId + "']" ).each( function( i ){
			
					var headingIndex = $( this ).attr( "id" ).substr( 16, 1 );
					
					if( $( "#HeadingValue_" + idarticle + "_" + headingIndex ).val() != $( this ).val() )
						selected = false;
					
				});
				
				if( selected )
					selection = idarticle;

			});
			
			return selection;
			
		}
						
		/* ----------------------------------------------------------------------------------------- */
	
	/* ]]> */
	</script>

	<div flexy:foreach="product.getArticleTables(),tableId,Articles">

		<div style="color:#fe7b00;; font-size: 12px; font-weight: bold;">
<!-- <span flexy:if="product.GetTable_name(tableId)"> {product.GetTable_name(tableId)}</span> -->
<span flexy:if="product.get(#table_name_1#)"> {product.get(#table_name_1#)}</span>
		</div>

		{Math.counter.reset(#-1#)}
        <ul id="ArticleList" style="display:none;">
            <li id="idarticle_{article.getId()}" style="{Math.counter.inc()}" flexy:foreach="product.getArticles(),article"></li>
        </ul>
		<div class="selection">        
	        <div style="float: left;">
                <div id="selectionHeader">
                {if:Math.counter.value}
                    <!-- Module de selection -->
                    {if:product.getSelectableHeadings()}
    <!--
                        <h2 class="sousTitre" style="border:none;">S&eacute;lectionnez un mod&egrave;le</h2>
                        <div class="spacer"></div>	
    -->				
                        <script type="text/javascript">
        
                            /* ----------------------------------------------------------------------------------------- */
                            
                            function selectHeading( headingIndex, headingValue, tableId, headingValue ){
    
                                /* listes suivantes */
    
                                var i = headingIndex + 1;
                                while( i < 3 ){
                        
                                    $( "#HeadingSelectorContainer_" + i + "_" + tableId ).hide();
                                    $( "#HeadingSelector_" + i + "_" + tableId ).val( "" );
                                                
                                    i++;
                                    
                                }
                                
                                /* prochaine liste */
                
                                var nextHeadingIndex = headingIndex + 1;
    
                                if( $( "#HeadingSelector_" + headingIndex + "_" + tableId ).val() != "" )
                                        $( "#HeadingSelectorContainer_" + nextHeadingIndex + "_" + tableId ).fadeIn( 700 );
                        
                                if( !$( "#HeadingSelector_" + nextHeadingIndex + "_" + tableId ).size() ){
                        
                                    var selection = getSelectedArticle( tableId );
                                    
                                    if( selection )
                                        selectArticle( selection, tableId );
            
                                    return;
                                    
                                }
                        
                                var $nextHeadingSelector = $( "#HeadingSelector_" + nextHeadingIndex + "_" + tableId );
                                
                                /* filtrer les valeurs de la prochaine liste */
                                
                                $nextHeadingSelector.children().each( function( i ){
                                
                                    if( $( this ).val() )
                                        $( this ).remove();
                                
                                });
                                
                                $( "#ArticleList" ).find( "li" ).each( function( i ){
                                
									var identifier = $( this ).attr( "id" ).split( "_" );
									var idarticle = identifier[ 1 ];
                                   
                                    if( $( "#HeadingValue_" + idarticle + "_" + headingIndex ).val() == headingValue ){
                                        $nextHeadingSelector.append( '<option value="' + $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() + '">' + $( "#HeadingValue_" + idarticle + "_" + nextHeadingIndex ).val() + '</option>' );
                                  
                                        }
                                    
                                });
        
                                $nextHeadingSelector.val( "" );
                                
                            }
    
                        </script>
						
						<!-- <div id=" " class="orderOptions clear ">
                       <label  class="modele floatleft">Modèle : </label>
                       
                    </div>-->
                        <span flexy:foreach="product.getSelectableHeadings(),headingIndex,heading">
                       
                            <span id="HeadingSelectorContainer_{headingIndex}_{tableId}" style="display:block;">
                                <input type="hidden" id="HeadingValue_{article.getId()}_{headingIndex}" value="{article.getHeadingValue(heading.id)}" flexy:foreach="product.getArticles(),article" />
                                <label for="HeadingValues_{headingIndex}_{tableId}" class="label_product"><strong>{heading.name:h} :</strong></label>
                                <select id="HeadingSelector_{headingIndex}_{tableId}" onchange="selectHeading( {headingIndex}, this.options[ this.selectedIndex ].innerHTML, {tableId}, this.options[ this.selectedIndex ].value );" class="select_product">
                                    <option value="">S&eacute;lection</option>
                                    <option value="{value}" flexy:foreach="heading.values,value">{value:h}</option>
                                </select>
                            </span>
                       
	                        <div class="spacer"></div>
                        </span>
                    {else:}
                        <!--<h2 class="sousTitre" style="border:none;">S&eacute;lectionnez une r&eacute;f&eacute;rence</h2>
                        <div class="spacer"></div>-->
                        <div class="orderOptions">
                            <label for="HeadingValues_{tableId}" class="label_product">
                            R&eacute;f&eacute;rence :</label>
                            <select id="HeadingValues_{tableId}" onchange="selectArticle( this.options[ this.selectedIndex ].value, {tableId} );" class="select_product">
                                <option value="">S&eacute;lection</option>
                                <option value="{article.getId()}" flexy:foreach="Articles,article">{article.get(#reference#):h}</option>			
                            </select>
                        </div>
                    {end:}
                {else:}
                    <!-- référence unique -->
                    <input type="hidden" id="HeadingValues_{tableId}" value="{article.getId()}" flexy:foreach="product.getArticles(),article" />
                    <flexy:toJavascript articleCount="Math.counter.value"></flexy:toJavascript>
                {end:}
                <div class="spacer"></div>
                
                
                    <!-- Generation de la quantité minimum -->
                    <FLEXY:TOJAVASCRIPT tableId="tableId">
                    <script type="text/javascript">
                    <!--
                                            
                        function change_quantity(reference) {
                            var quantityMin = document.getElementById("value_"+reference).value;
                            document.getElementById("HeadingQuantity_"+tableId).value = quantityMin;
                        }
                    
                        function fct_plus( tableId ) {
                        
                            var idarticle = getSelectedArticle(tableId);
                
                            if( idarticle ){
                                var q = document.getElementById("HeadingQuantity_"+tableId).value;
                                var plus = parseInt(q) + 1;
                                document.getElementById("HeadingQuantity_"+tableId).value = plus;
                            } else {
                                alert( "Vous devez d\'abord s\351lectionner une r\351f\351rence" );
                            }
    
                            document.getElementById("infoQuantityMin").style.display = "none" ;
    
                        }
                      
                        function fct_moins( tableId ) {
                        
                            var idarticle = getSelectedArticle(tableId);
                
                            if( idarticle ){
                            
                                var q = document.getElementById("HeadingQuantity_"+tableId).value;
                                var stock_min =  document.getElementById("value_"+idarticle).value;
                                                        
                                //Si quantité == 1 ou stock minimum == quantité
                                if(q == 1 || stock_min == q) {
                                    var moins = parseInt(q);
                                }
                                else {
                                    var moins = parseInt(q) - 1;
                                }
                                
                                //Affichage message?
                                if(stock_min == q) {
                                    document.getElementById("infoQuantityMin").style.display = "block" ;
                                }	
                                
                                document.getElementById("HeadingQuantity_"+tableId).value = moins;
                            } else {
                                alert( "Vous devez d\'abord s\351lectionner une r\351f\351rence" );
                            }
                        }
                        
                        
                        function chiffres(event) {
                            // Compatibilité IE / Firefox
                            if(!event&&window.event) {
                                event=window.event;
                            }
                            // IE
                            if(event.keyCode < 48 || event.keyCode > 57) {
                                event.returnValue = false;
                                event.cancelBubble = true;
                            }	
                            // DOM
                            if(event.which < 48 || event.which > 57) {
                                event.preventDefault();
                                event.stopPropagation();
                            }	
                        }
    
                        
                    // -->				
                    </script>
                    <!--<div id=" " class="orderOptions ">
                        <label  class="modele floatleft">Modèle : </label>
                       
                    </div>-->
<div id="select_qty" class="orderOptions quantite">
                        <label for="HeadingQuantity_{tableId}" class="label_product floatleft">Quantité : </label>
                        <input type="text" id="HeadingQuantity_{tableId}" name="quantity_{tableId}" value="1" class="select_product qty floatleft" style="width: 38px; text-align: center; background: none repeat scroll 0px 0px rgb(255, 255, 255); border: 2px solid rgb(204, 204, 204);" onkeypress="chiffres(event);" />
                        <div class="up-down" style="float: left; background: none repeat scroll 0% 0% rgb(114, 112, 108); padding: 3px;">
                            <a onclick="fct_plus({tableId});" style="float: left; margin-top: 4px;"><img src="{baseURL}/templates/catalog/images/btn_plus.png" alt="plus" /></a>
                            <a onclick="fct_moins({tableId});" style="float: left; clear: both; margin-top: 5px;" class="down"><img src="{baseURL}/templates/catalog/images/btn_moins.png" alt="moins" /></a>
                        </div>
                    </div>
                    <div class="spacer"></div>
					{if:product.getColors()}
                       <!-- <div id="color" class="orderOptions">-->
						 <div id="Colors{tableId}">
                            <label for="ColorForm" class="label_couleur">Couleur :</label>
							<form name="ColorForm" id="ColorForm" action="{baseURL}/catalog/product_one.php" method="get" flexy:ignore="yes">
												
							
							<span flexy:foreach="product.getColors(),color"><div class="prod_img">
                            <input type="radio" class="ProductColorRadioButton" name="Colors{tableId}[]" id="Colors{tableId}_{color.id}" value="{color.id}" flexy:ignore="yes" />
                            <a class="thumbnail" href="#thumb" onclick="$('#Colors{tableId}_{color.id}').attr('checked', 'checked'); ">
                            <img class="ProductColorImage" src="{color.image}" height="60"/></a>
                            <!--<span class="zoom"><img src="{color.image}" width="80"/></span>--></div>
                        </span>
                        <input type="hidden" flexy:ignore="name='displayedDetailTable'" value="{tableId}" />
							
                                <input type="hidden" flexy:ignore="name='displayedDetailTable'" value="{tableId}" />
                            </form>
                            <div class="spacer"></div>
                    	</div>
					{end:}

                    <span flexy:foreach="product.getArticles(),article">
                        {if:article.get(#min_cde#)}
                            <input id="value_{article.get(#idarticle#):h}" name="value[]" value="{article.get(#min_cde#):h}" type="hidden" flexy:ignore="yes" />
                        {else:}
                            <input id="value_{article.get(#idarticle#):h}" name="value[]" value="1" type="hidden" flexy:ignore="yes" />
                        {end:}
                    </span>
    
                    
                </div>
				
				<p id="infoQuantityMin" style="display:none;">
                    <img src="{baseURL}/templates/catalog/images/icons_warning_qty.jpg" alt="" width="20" height="18" />
                    Vous avez atteint la quantité minimum pour cette référence.</p>
                    
			</div>

			<hr />

			<p id="infoStockMin" style="display:none;">
				<img src="{baseURL}/templates/catalog/images/icons_warning_qty.jpg" alt="" width="20" height="18" />
				Article epuisé.</p>

				
			<div id="contentPrice" >
				<div id="selectionPrice">	
					<p class="description" id="myCaract" style="float:left; width:250px;"><strong></strong></p>
					<p class="description" id="myDelai" style="float:right; text-align:right; width:190px; font-style:italic;"><strong></strong></p>
				</div>					
				
				<p class="pButton">	
				
                	<input type="button" name="AddCde" id="AddCde" class="btn-big" value="Ajouter au panier" onclick="addToCart( getSelectedArticle({tableId}), $('#HeadingQuantity_{tableId}').val(), 'order' );" />
					<input type="button" name="AddSD" id="AddSD" value="Demander un devis" class="btn-big grey2" onclick="addToCart( getSelectedArticle({tableId}), $('#HeadingQuantity_{tableId}').val(), 'estimate' );" /> 
					
						
				<div class="spacer"></div>	
				</p>
			</div>
            
            

		</div>
	</div>	
</div>
<script type="text/javascript">
	var idarticle0 = $("#idarticle0").val();
	var stockLevel0 = $("#stockLevel" + idarticle0).val();
	// désactive la possibilité de commander dans le cas d'un produit avec un seul article (sans le menu déroulant de choix de référence)
	if (typeof articleCount != 'undefined' && articleCount == 0 && stockLevel0 == 0) {
		$("#orderOptions").hide();
		$("#infoStockMin").css("display", "block");
	}

	$(function() {
		var options = {  
			zoomType: 'standard',  
			lens:true,  
			preloadImages: true,  
			preloadText: 'Chargement...',
			alwaysOn:false,
			zoomWidth: 250,
			zoomHeight: 250,
			title: false
		};  
		$('.colorZoom').jqzoom(options);  
		
		$('.zoomPad').css('z-index','auto');

		//$('.productZoom').jqzoom();  
	});
</script>
