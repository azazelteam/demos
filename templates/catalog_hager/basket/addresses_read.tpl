 <link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/addresses.css" />

 {if:iduser}<span style="color:red;font-weight: bold;">/basket/addresses_read.tpl</span>{end:}
 <table class="tableInputBuyer">
	<tr>
	<td>
			<b>
			{buyer_company}
			<br/>{buyer_title} {buyer_lastname} {buyer_firstname}
			</b>
			<br/>{buyer_adress}
			{if:buyer_adress_2}<br />{buyer_adress_2}{end:}
			<br />{buyer_zipcode},{buyer_city}
			
	</td>
	</tr>				
</table>		
<!-- Adresse de livraison -->
{if:iddelivery}	
				<table class="adrDelivery">						
				<tr>
				  <td align="left"><img src="{start_url}/templates/catalog/images/livraison.gif"></td>
				</tr>
				<tr>
					<td>
					<b>
					{d_buyer_company}
					<br/>
					{d_buyer_title} {d_buyer_lastname} {d_buyer_firstname}
					</b>
					<br/>
					{d_buyer_adress} 	
					{d_buyer_adress_2}
					<br/>{d_buyer_zipcode}, {d_buyer_city}							
					</td>
				</tr>
				</table>
{end:}				
				
				
<!-- Adresse de facturation -->				
{if:idbilling}
				<table class="adrDelivery">						
				<tr>
					<td>
					<b>
					{b_buyer_company:e}
					<br/>
					{b_buyer_title} {d_buyer_lastname:e} {d_buyer_firstname:e}
					</b>
					<br/>
					{b_buyer_adress:e} 	
					{b_buyer_adress_2:e}
					<br/>
					{b_buyer_zipcode}, {d_buyer_city:e}					
					</td>
				</tr>
				</table>
{end:}				
