<flexy:include src="head/header.tpl"></flexy:include>
<div id="global">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/order_form_multisite.tpl</span>{end:}
	<!-- Haut de page -->
	<flexy:include src="head/banner.tpl"></flexy:include>
	
	<!-- Fil d'ariane -->
	<ul class="FilAriane">
		<li><a id="OrderStepAnchor1" href="/catalog/basket.php"><em id="panier"></em>&nbsp;<br />1- Panier</a></li>
		<li class="separator">&nbsp;</li>
		<li><a id="OrderStepAnchor2" href="/catalog/order_multisite.php" class="actif"><em id="identification"></em>&nbsp;<br />2- Coordonnées</a></li>
		<li class="separator">&nbsp;</li>
		<li class="big"><a id="OrderStepAnchor3" href="/catalog/order_multisite.php"><em id="paiement"></em>&nbsp;<br />3- Validation et paiement</a></li>
		<li class="separator">&nbsp;</li>
		<li><a id="OrderStepAnchor4" href="/catalog/order_multisite.php"><em id="confirm"></em>&nbsp;<br />4- Confirmation</a></li>
	</ul>
	<div class="spacer"></div>
		
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------ */
		/* passer à l'étape suivante/précédente */
		
		function setOrderStep( step ){
		
			//$( "div[id^='OrderStep' ]" ).fadeOut( 300 );
			$( "div[id^='OrderStep' ]" ).css( "display", "none" );
			
			if( step == 2 ){ //rappel des coordonnées de base pour la livraison/facturation
			
				$( "#default_company" ).html( $( "#company" ).val() );
				$( "#default_lastname" ).html( $( "#lastname" ).val() );
				$( "#default_address" ).html( $( "#address2" ).val().length ? $( "#address" ).val() + "<br />" + $( "#address2" ).val() : $( "#address" ).val() );
				$( "#default_zipcode" ).html( $( "#zipcode" ).val() );
				$( "#default_city" ).html( $( "#city" ).val() );
				
			}
			
			//$( "#OrderStep" + step ).fadeIn( 300 );
			$( "#OrderStep" + step ).css( "display", "block" );
			
			//fil d'arianne
			
			$( "a[id^=OrderStepAnchor]" ).removeClass( "actif" );
			$( "a[id^=OrderStepAnchor]" ).eq( step - 1 ).addClass( "actif" );
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* créer un compte */
		
		function register(){
		
			$( "#RegisterError" ).html( "" );
			
			//vérification validité de l'adress email
			
			if( !validateEmail( $( "#RegisterEmail" ).val() ) ){
			
				$( "#RegisterError" ).html( "L'adresse email saisie n'est pas valide" );
				return;
				
			}
			
			//vérification du mot de passe : taille >= 6
			
			if( $( "#RegisterPassword" ).val().length < 6 ){
			
				$( "#RegisterError" ).html( "Votre mot de passe doit contenir au moins 6 caractères alphanumériques" );
				return;
				
			}
			
			//vérification du mot de passe : que des caractères alphanumériques
			
			var reg = new RegExp( '^[a-zA-Z0-9]+$', 'i' );
			if( !reg.test( $( "#RegisterPassword" ).val() ) ){
			
				$( "#RegisterError" ).html( "Votre mot de passe ne doit contenir que des caractères alphanumériques" );
				return;
				
			}
			
			//vérification du mot de passe : correspond à la confirmation du mot de passe
			
			if( $( "#RegisterPassword" ).val() != $( "#RegisterPasswordConfirmation" ).val() ){
			
				$( "#RegisterError" ).html( "Le mot de passe et la confirmation du mot de passe saisis ne correspondent pas" );
				return;
				
			}
			
			//vérification disponibilité du login
			
			if( !isLoginAvailable( $( "#RegisterEmail" ).val() ) ){
			
				$( "#RegisterError" ).html( "Un compte client existe déjà avec cette adresse email" );
				return;
				
			}
			
			$( "#login" ).val( $( "#RegisterEmail" ).val() );
			$( "#password" ).val( $( "#RegisterPassword" ).val() );
			
			//passer à l'étape suivante
			
			setOrderStep( 2 );
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* vérifier le format de l'adresse email */
		
		function validateEmail( email ){
		
			var reg = new RegExp( '^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$', 'i' );
			return reg.test( email );
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* vérifier la disponibilité du login */
		
		function isLoginAvailable( login ){
		
			var available = false;
			
			$.ajax({
		 	
				url: "/catalog/order_multisite.php?login_available",
				async: false,
				type: "GET",
				data: "login=" + escape( login ),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le compte" ); },
			 	success: function( responseText ){
	
					available = responseText == "1";
					
				}
	
			});
		
			return available;
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* rappel du mot de passe oublié */
		
		function newPassword(){
		
			$.ajax({
		 	
				url: "/catalog/order_multisite.php?new_password",
				async: false,
				type: "GET",
				data: "login=" + escape( $( "#UserLogin" ).val() ),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Une erreur est survenue lors de la demande de rappel de mot de passe" ); },
			 	success: function( responseText ){
	
					$( "#LoginError" ).html( responseText == "1" ? "Un nouveau mot de passe vous a été envoyé par courriel à l'adresse indiquée" : "Impossible de retrouver le mot de passe pour l'adresse indiquée" );
						
				}
	
			});
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* identification */
		
		function login(){
		
			$( "#LoginError" ).html( "" );
			
			//vérifier les paramètres d'identification
			
			var $customer = null;
			
			$.ajax({
		 	
				url: "/catalog/order_multisite.php?identification",
				async: false,
				type: "GET",
				data: "login=" + escape( $( "#UserLogin" ).val() ) + "&password=" + escape( $( "#UserPassword" ).val() ),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Une erreur est survenue lors de l'identification" ); },
			 	success: function( responseXML ){

					if( $( responseXML ).find( "idbuyer" ).size() )
						$customer = $( responseXML );	
						
				}
	
			});
			
			//échec de l'identification
			
			if( !$customer ){
			
				$( "#LoginError" ).html( "L'identifiant ou le mot de passe est incorrect" );
				return;
			
			}
			
			//autocompletion du formulaire

			$( "#login" ).val( $( "#UserLogin" ).val() );
			$( "#password" ).val( $( "#UserPassword" ).val() );
			$( "#lastname" ).val( $customer.find( "lastname" ).text() );
			$( "#firstname" ).val( $customer.find( "firstname" ).text() );
			$( "#phonenumber" ).val( $customer.find( "phonenumber" ).text() );
			$( "#faxnumber" ).val( $customer.find( "faxnumber" ).text() );
			$( "#gsm" ).val( $customer.find( "gsm" ).text() );
			$( "#company" ).val( $customer.find( "company" ).text() );
			$( "#address" ).val( $customer.find( "address" ).text() );
			$( "#address2" ).val( $customer.find( "address2" ).text() );
			$( "#zipcode" ).val( $customer.find( "zipcode" ).text() );
			$( "#city" ).val( $customer.find( "city" ).text() );
			$( "#siret" ).val( $customer.find( "siret" ).text() );
			$( "#naf" ).val( $customer.find( "naf" ).text() );
			$( "#newsletter" ).attr( "checked", $customer.find( "newsletter" ).text() == "1" ? "checked" : "" );
			
			//étape suivante
				
			setOrderStep( 2 );
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* vérifier les champs obligatoires */
		
		function checkFormData(){
		
			$.watermark.hideAll();
  
  			//liste des champs obligatoires
  			
			var required = new Array( "lastname", "company", "address", "zipcode", "city", "siret" );
			
			var i = 0;
			while( i < required.length ){
			
				if( !$( "#" + required[ i ] ).val().length ){
				
					$( "#RequiredError" ).html( "Merci de bien vouloir renseigner tous les champs obligatoires" );
					
					$.watermark.showAll();
					$( "#" + required[ i ] ).css( "border-color", "#D12524" );
					
					return false;
					
				}
				else $( "#" + required[ i ] ).css( "border-color", "#999999" );
				
				i++;
				
			}
			
			$.watermark.showAll();
			
			//validation du siret
			
			if( !validateSiret() ){
			
				$( "#RequiredError" ).html( "Le numéro de SIRET saisi n'est pas valide" );
				return false;
				
			}
			
			return true;
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* validation du n° de siret */
		
		function validateSiret(){
		
			validated = false;
			
			$.ajax({
		 	
				url: "/catalog/order_multisite.php?check_siret",
				async: false,
				type: "GET",
				data: "siret=" + escape( $( "#siret" ).val() ),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Une erreur est survenue lors de la vérification du n° de siret" ); },
			 	success: function( responseText ){
	
					if( responseText == "1" )
						validated = true;	
						
				}
	
			});
			
			return validated;
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		/* commander */
		
		function createOrder(){
		
			if( !$( "#accept_condition" ).attr( "checked" ) ){
			
				$( "#OrderError" ).html( "Vous devez lire et accepter nos conditions générales de vente" );
				return;
				
			}
			
			$( "#OrderForm" ).submit();
			
		}
		
		/* ------------------------------------------------------------------------------------ */
		
	/* ]]> */
	</script>
	<!-- Contenu -->
	<div id="content">	
		<div id="OrderStep1" style="display:block;">
			<!-- Nouveau client -->
			<div class="bloc borderRight">
				<h1 class="titleUser">Nouveau client ? Créer votre compte.</h1>
				<p id="RegisterError" class="msgError"></p>
				<input type="text" id="RegisterEmail" name="RegisterEmail" class="loginField input_email" />
				<input type="password" id="RegisterPassword" name="RegisterPassword" class="loginFieldMedium input_password" />
				<input type="password" id="RegisterPasswordConfirmation" class="loginFieldMedium marginLeft input_conf_password" />
				<a class="prevField" href="/catalog/basket.php"> Retour à mon panier</a>
				<input type="button" onclick="register();" name="" class="submitField" value="Etape suivante" />
			</div>
			<!-- Déja client -->
			<div class="blocRight">
				<h1 class="titleUser">Déja client ? Connectez-vous.</h1>
				<p id="LoginError" class="msgError"></p>
				<input type="text" id="UserLogin" class="loginField input_identifiant" />
				<input type="password" id="UserPassword" class="loginField input_password" />
				<div class="spacer"></div>
				<a href="#" onclick="newPassword(); return false;" class="passwordForget">Mot de passe oublié?</a>
				<input name="" type="button" class="submitField" value="Connection" onclick="login();" />
				<div class="spacer"></div>	
				<div class="spacer"></div>
			</div>	
			<div class="spacer"></div>
			<br />
		</div><!-- Step2 -->
		<!-- ================================================================================================================== -->
		<form id="OrderForm" action="http://{host}/catalog/order_multisite.php" method="post" enctype="multipart/form-data">
			<input type="hidden" id="login" name="login" value="" flexy:ignore="yes" />
			<input type="hidden" id="password" name="password" value="" flexy:ignore="yes" />
			<input type="hidden" name="order" value="1" flexy:ignore="yes" />
			<input type="hidden" name="idsource" value="" />
			<input type="hidden" name="ref_supplier[]" value="{item.get(#ref_supplier#)}" flexy:foreach="basket.getItems(),item" flexy:ignore="yes" />
			<input type="hidden" name="quantity[]" value="{item.getQuantity()}" flexy:foreach="basket.getItems(),item" flexy:ignore="yes" />
			<input type="hidden" name="idcolor[]" value="{item.get(#idcolor#)}" flexy:foreach="basket.getItems(),item" flexy:ignore="yes" />
			<input type="hidden" name="idcloth[]" value="{item.get(#idcloth#)}" flexy:foreach="basket.getItems(),item" flexy:ignore="yes" />
			<!-- ================================================================================================================== -->
			<div id="OrderStep2" style="display:none;">
				<!-- Informations personnelles -->
				<div class="bloc borderRight">
					<h1 class="titleUser">Informations personnelles</h1>	
					<div class="selectTitle"></div>
					<input type="text" id="lastname" name="lastname" maxlength="50" class="loginFieldSmall marginLeft input_nom" maxlength="20" value="" />
					<input type="text" id="firstname" name="firstname" maxlength="50" class="loginFieldSmall marginLeft input_prenom" value="" />
					<div class="spacer"></div>
					<div class="selectField"></div>
					<div class="spacer"></div>
					<input type="text" id="phonenumber" name="phonenumber" maxlength="20" class="loginFieldMedium input_phone" value="" />		
					<input type="text" id="faxnumber" name="faxnumber" maxlength="20" class="loginFieldMedium marginLeft input_fax" value="" />
					<div class="spacer"></div>
					<input type="text" id="gsm" name="gsm" maxlength="20" class="loginFieldMedium input_gsm" value="" />
					<div class="spacer"></div>
					<p><input type="checkbox" id="newsletter" name="newsletter" value="1" checked="checked" />&nbsp;Abonnement à la newsletter du site.</p>
					<p>Selon la loi "Informatique et libertés" du 05/01/76, vous disposez d'un droit d'accès, de rectification et de suppression des données qui vous concernent.</p>
				</div>
				<div class="blocRight">
					<!-- Informations société -->
					<h1 class="titleUser">Informations société</h1>	
					<p id="RequiredError" class="msgError"></p>
					<input type="text" id="company" name="company" maxlength="50" class="loginField input_company" value="" />
					<div class="spacer"></div>
					<input type="text" id="address" name="address" maxlength="100" class="loginField input_adress" value="" />
					<div class="spacer"></div>
					<input type="text" id="address2" name="address2" maxlength="100" class="loginField input_adresscomp" value="" />
					<div class="spacer"></div>
					<input type="text" id="zipcode" name="zipcode" maxlength="10" class="loginFieldMedium input_cp" value="" />	
					<input type="text" id="city" name="city" maxlength="50" class="loginFieldMedium marginLeft input_city" value="" />				
					<div class="spacer"></div>
					<div class="selectField"></div>
					<div class="spacer"></div>	
					<input type="text" id="siret" name="siret" maxlength="14" class="loginFieldMedium input_siret" value="" />					
					<input type="text" id="naf" name="naf" maxlength="15" class="loginFieldMedium marginLeft input_naf" value="" />
					<div class="spacer"></div>
					<input type="button" onclick="if( checkFormData() ) setOrderStep( 3 );" class="submitField" value="Etape suivante" name="" />
					<div class="spacer"></div>
				</div>
				<div class="spacer"></div>
			</div><!-- Step 3 -->
			<!-- ================================================================================================================== -->
			<div id="OrderStep3" style="display:none;">
				<!-- Livraison & Facturation -->
				<div class="blocLarge">
					<h1 class="titleConfOrder"><span>Votre commande sera livrée & facturée à l'adresse ci-dessous.</span></h1>
				</div>
				<div class="spacer"></div>
				<div class="blocAddress">
					<h1 class="titleUser">Votre adresse</h1>
					<p>
						<span id="default_company"></span>
						<br />
						M/Mme <span id="default_lastname"></span>
						<br />
						<span id="default_address"></span>
						<br />
						<span id="default_zipcode"></span> <span id="default_city"></span>
					</p>
				</div>
				<!-- Livraison/Facturation -->
				<flexy:include src="basket/addresses_edit.tpl"></flexy:include>
				<div class="spacer"></div>
				<!-- Contenu de votre commande -->
				<div class="blocLarge">
					<h1 class="titleConfOrder3"><span>Confirmation et paiement.</span></h1>
				</div>
				<div class="spacer"></div>
				<!-- Liste des paiements -->
				<ul class="listPaiement" style="margin:5px 0 0 19px; width:800px;">					
					<li><input type="radio" name="idpayment" value="3" checked="checked" /> Virement</li>
					<li><input type="radio" name="idpayment" value="2" /> Ch&egrave;que</li>
					<li><input type="radio" name="idpayment" value="4" /> LCR</li>
				</ul>
				<div class="spacer"></div>
				<p id="OrderError" class="msgError" style="margin-left:25px;"></p>
				<p class="pCGV" id="AcceptConditions">
					<input id="accept_condition" name="accept_condition" name="AcceptCondition" value="1" type="checkbox" /> 
					J'ai lu et accepté les <a href="/www/cgv.pdf" target="_blank">conditions générales de vente</a>
				</p>
				<div class="btnPanierRight">
					<input type="button" class="submitField" name="" value="Finaliser ma commande" onclick="createOrder();" />
					<p><a href="/catalog/estimate_multisite.php" class="submitFieldEstimate">Demander un devis</a></p>
				</div>
				<div class="spacer"></div>
				<br />
				<div class="spacer"></div>
			</div><!-- Step 4 -->
		<!-- ================================================================================================================== -->
		</form><!-- OrderForm -->
	</div><!-- Content -->

<!-- footer -->	
<flexy:include src="footer/footer_basket.tpl"></flexy:include>