<flexy:include src="/head/header.tpl"></flexy:include>
<!-- <flexy:include src="b_left_alt.tpl"></flexy:include>-->
<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/basket.css" />
<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/jquery.bxslider.css" />
<script type="text/javascript" src="{start_url}/js/thickbox/thickbox.js"></script>
<script type="text/javascript" src="{start_url}/js/jquery/ui/blockUI.js"></script>
<link rel="stylesheet" href="{start_url}/js/thickbox/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript" language="javascript">
<!--
function checkAccept() {
	if( !document.getElementById('accept_condition').checked ){
	
		alert("Vous devez lire et accepter les conditions g\351n\351rales de vente");
		return false;
	
	}
	return true;
}
function checkpays() {
	if(document.getElementById('idstate').value == 1 && document.getElementById('codepostal').value != 97){
        document.getElementById('cb').style.visibility = "visible";
		document.getElementById('cb2').style.visibility = "visible";
	}
}

window.onload=function(){
	cbRadio = document.getElementById('cb') != null ? document.getElementById('cb').parentNode.innerHTML : false;
	if(document.getElementById('idstate').value == 1 && document.getElementById('codepostal').value == 97){
        var liCB = document.getElementById('cb').parentNode;
		document.getElementById('listPaiement').removeChild(liCB);
		document.getElementById('radio3').checked = true;
	}
	if(document.getElementById('idstate').value != 1){
		var liCB = document.getElementById('cb').parentNode;
		document.getElementById('listPaiement').removeChild(liCB);
		document.getElementById('radio3').checked = true;
	}
};

function checkIdstate()
{
	if (document.getElementById('delivery[idstate]').value != 1 || document.getElementById('idstate').value != 1)
	{
		if (document.getElementById('cb') != null)
		{
			var liCB = document.getElementById('cb').parentNode;
			document.getElementById('listPaiement').removeChild(liCB);
			document.getElementById('radio3').checked = true;
		}
	}
	else
	{
		if (document.getElementById('cb') == null && cbRadio != "undefined")
		{
			var liCB = document.createElement("li");
			liCB.innerHTML = cbRadio;
			ulElement = document.getElementById('listPaiement');
			ulElement.insertBefore(liCB, ulElement.childNodes[0]);
		}
	}
}
//-->
</script>
<!--====================================== Contenu ======================================-->
<div id="center" class="page order">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/order.tpl</span>{end:}
    <ul class="basket_fil">
    <li class="un"><a href="/catalog/basket.php"><span>Panier commande</span></a></li>
    <li class="deux"><span>Authentification</span></li>
    <li class="trois actif"><span>Livraison et facturation</span></li>
    <li class="quatre"><span>Confirmation</span></li>
	</ul>
	<h1 class="title_commande" style="width:96%;">R&eacute;capitulatif du panier</h1>
	<flexy:include src="articles_read.tpl"></flexy:include>
    <flexy:include src="basket_amounts.tpl"></flexy:include>
	<div class="spacer"></div><br />
    <h1 class="title_commande" style="width:96%">Adresses livraison et facturation</h1>
	<flexy:include src="addresses_read.tpl"></flexy:include>
	<div class="spacer"></div><br />
	<form id="ValidateAdresses" action="{start_url}/catalog/finish.php" method="post" onsubmit="return verifForm();">
	<flexy:include src="order_adresses_edit.tpl"></flexy:include>	
    <div class="spacer" style="height:20px"></div>
	<h1 class="title_commande" style="width:96%">S&eacute;lection du mode paiement</h1>
    		<ul class="listPaiement" id="listPaiement">
					<li><div id="cb" class="payment_block"><input style="width: auto;height:auto;float: none;" name="idpayment" value="1" checked="checked" type="radio" id="radio1"/> <label for="radio1"><span>Carte bancaire</span><br/>
					<div>Saisie de vos donn&eacute;es bancaires sur le site du Cr&eacute;dit Mutuel.</div>
					<img src="{baseURL}/templates/catalog/images/cb.png" /></div></label></li>	
					{foreach:radio,line}
						<li><div class="payment_block" ><input type="radio" style="width: auto;height:auto;float: none;" name="idpayment" value="{line[idpayment]}" id="radio{line[idpayment]}"/> <label for="radio{line[idpayment]}"> <span>{line[payment_name]:h}</span><br/>
						{if:line[image]}{line[image]:h}{end:}</div> </label>
						</li>
					{end:}	
	</ul>
	<div class="spacer"></div><br />
	<div class="spacer"></div><br />
	<input type="hidden" name="DocType" value="0" />	
	<!--<input type="hidden" name="iddelivery" value="{iddelivery}" />
	<input type="hidden" name="idbilling" value="{idbilling}" />-->		
	<input type="hidden" name="SPPrice" value="{SPPrice}" />
	<!-- gestion blocage d&eacute;partement 97 et autres pays -->
	<input type="hidden" name="idstate" id="idstate" value="{buyer_idstate}"/>
	<input type="hidden" name="codepostal" id="codepostal" value="{codepostal}"/>
	{if:orderid}<input type="hidden" name="orderid" value="{orderid:h}" />{end:}
	{if:code}<input type="hidden" name="voucher_code" value="{code:h}" />{end:}
	<!--<input type="hidden" name="idpayment" id="idpayment" value="{idpayment}"/>-->
	<input type="submit" style="float:right;margin-right:1%;width:220px;font-size:14px;" value="Valider ma commande et payer" />
	<p id="AcceptConditions" style="float:right;margin:10px 10% 0;">
        <input type="checkbox" id="accept_condition" style="background:none;width:auto;height:auto;" name="AcceptCondition" value="1" />
        &nbsp;J'ai lu et accept&eacute; les <a class="cgvlink" href="{start_url}/www/cgv.php" target="_blank">&nbsp;conditions g&eacute;n&eacute;rales de vente</a>
    </p>
	</form>
	
	
    <div class="spacer"></div>
</div>
</div>
</div>
 	
<!--<flexy:include src="b_right.tpl"></flexy:include>-->
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>