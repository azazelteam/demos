{if:iduser}<span style="color:red;font-weight: bold;">/basket/articles_read.tpl</span>{end:}
<span style="color:red;font-weight: bold;">/basket/articles_read.tpl</span>
  <table cellspacing="0" cellpadding="3" width="99%" border="0" class="content_basket">
  <!-- début de modification -->
    <tr style="background-color:#dbdbdb;font-weight:bolder;" class="trHead">
  <!-- fin de modification -->
      <th></th>
      <th></th>
      <th width="320">R&eacute;f. & Caract&eacute;ristiques</th>
      <th>D&eacute;lai de livraison</th>
	  <th>Prix unit. H.T.</th>
	  <th>Quantit&eacute;</th>
	  {if:haslot}
	  		<th>Qt&eacute; / lot</th>
	{end:}
      <th align="center" style="width: 60px;">Prix total H.T.</th>
    </tr>
    {foreach:basket_articles,art}      
	<tr id="Item{art[i]}" class="{art[idproduct]}">
        <td class="tdCenterRight" style="text-align:center;">
			<a href="#" onclick="$('#Item' + {art[i]}).fadeOut('slow',function(){document.location='basket.php?delete&amp;index={art[i]}';}); return false;">
				<img src="../templates/catalog/images/delete2.jpg" alt="Supprimer" />
			</a>
		</td>
        <td align="center" class="tdCenterLeft">
            <a href="{art[productURL]}">
                <img src="{art[icon]}" width="60" height="60" alt="Icon">
            </a>
        </td>
        <td >
            <div class="articleInfo">
                <div class="productName"><a href="{art[productURL]}">{art[summary_1]:h}</a></div>
                <div class="productDesc">{art[designation_1]:h}</div>
                <div>Référence : <span class="productRef">{art[reference]:h}</span></div>
										 							                             
            </div>
        </td>
        <!--<td>{art[designation]:h}<br/>R&eacute;f&eacute;rence : {art[reference]:h}</td>-->
        <td align="center" class="tdDelivery">{art[delay]:h}</td>
        <td align="center" class="tdPrice tdPriceUnit">{art[puht]:h}</td>
        <td align="center" class="tdCenterBold tdQuantity">{art[quantity]:h}</td>
        {if:haslot}
        <td align="center" class="tdCenterBold tdPrice">{art[lot]:h} / {art[unit]:h}</td>
        {end:}		
        <td align="center" class="tdCenterBold tdPrice">
        {art[total_price]:h}
        </td>
    </tr>
    {end:}

     {if:vouche}
                
    <tr>
                            
                            <td > <!--<a href="{baseURL}/catalog/basket.php?resetbon" >
              <img src="{baseURL}/images/front_office/icones/delete2.jpg" alt="Supprimer" />
               
            </a>--></td>
                     
          
          
          <td>
                     <div class="articleInfo">
                                <div class="productName">{code} : {summary} &nbsp;</div>
                                
                                <div>R&eacute;f&eacute;rence : <span class="productRef">{reference}</span></div>
                
                               <!--  <div>Exp&eacute;dition sous : <span class="shippingTime">Consultez-nous</span></div>
                               <div flexy:if="hasLot">
                                  {item.get(#lot#):h} / {item.get(#unit#):h}
                                </div>-->
                            </div>
                    
                    </td>
          <td align="center">-</td>
          <td align="center">-</td>
                    
          <td align="center">1</td>
          <td align="center" style="color:#00CC00;">-{reduction:h}</td>
        
                        
                        
                        </tr>
                        {end:}
                         {if:!voucherexist}
  <tr>  <p class="msgError">Ce bon de r&eacute;duction est d&eacute;j&agrave; utilis&eacute;.</p>  </tr>
                                {end:}
  </table>
<br />
<br />