<script type="text/javascript" language="javascript">
<!--
function displayForm(id) {
	//alert(id);
	tableForm = document.getElementById(id);
	//alert(tableForm);
	if(tableForm) {
		//alert(tableForm.style.display);
		if(!tableForm.style.display || tableForm.style.display == 'none') {
			tableForm.style.display = 'block';
		}
		else {
			tableForm.style.display = 'none';
		}
	}
}
//-->
</script>
{if:iduser}<span style="color:red;font-weight: bold;">/basket/addresses_edit.tpl</span>{end:}
    <h1 class="title_commande">Adresse de livraison</h1>
    <p><input style="background-color:white; background:none; height:13px;" border="0" id="delivery_checkbox" type="checkbox" name="delivery_checkbox" value="1" onclick="javascript: displayForm('delivery_different');" /> Cochez-cette case si vous souhaitez utiliser une adresse de livraison différente.</p>
    <div id="delivery_different" style="display:none; margin:5px 0;">
    	<p class="txt_oblg">Tous les champs marqués d'un <span class="ast_orange">*</span> sont à remplir obligatoirement.</p>
        <div class="form_left">
            <label>Titre</label>
    		{deliveryTitleList:h}
            <div class="spacer"></div>
            
            <label>Nom&nbsp;<span class="ast_orange">*</span> </label>
            <input type="text" size="33" class="AuthInput" name="delivery[Lastname]" id="delivery[lastname]" value="">
            <div class="spacer"></div>
            
            <label>Adresse&nbsp;<span class="ast_orange">*</span></label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Adress]" id="delivery[adress]" value="">
            <div class="spacer"></div>
            
            <label>Code postal&nbsp;<span class="ast_orange">*</span></label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Zipcode]" id="delivery[zipcode]" value="">
            <div class="spacer"></div>
            
            <label>Pays&nbsp;<span class="ast_orange">*</span></label>
    		{drawliftb_state:h}
            <div class="spacer"></div>
            
            <label>Téléphone&nbsp;<span class="ast_orange">*</span></label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Phonenumber]" id="delivery[phonenumber]" value="" maxlength="20">
            <div class="spacer"></div>
        </div>
        <div class="form_right">
            <label>Existant</label>
    		{drawliftdelivery:h}
            <div class="spacer"></div>
            
            <label>Prénom</label>
     		<input type="text" size="33" class="AuthInput" name="delivery[Firstname]" id="delivery[firstname]" value="">
            <div class="spacer"></div>
            
            <label>Compl. adresse</label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Adress_2]" id="delivery[adress_2]" value="">
            <div class="spacer"></div>
            
            <label>Ville&nbsp;<span class="ast_orange">*</span></label>
    		<input type="text" size="33" class="AuthInput" name="delivery[City]" id="delivery[city]" value="">
            <div class="spacer"></div>
            
            <label>Entreprise&nbsp;<span class="ast_orange">*</span></label>
    		<input type="text" size="33" name="delivery[Company]" id="delivery[company]" class="AuthInput" value="{d_company}">
            <div class="spacer"></div>
            
            <label>Fax</label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Faxnumber]" id="delivery[faxnumber]" value="" maxlength="20">
            <div class="spacer"></div>
        </div>
    </div>
    <div class="spacer"></div><br />


	
    <h1 class="title_commande">Adresse de facturation</h1>
    <p><input style="background-color:white; background:none; height:13px;" border="0" id="billing_checkbox" type="checkbox" name="billing_checkbox" value="1" onclick="javascript: displayForm('billing_different');" /> Cochez-cette case si vous souhaitez utiliser une adresse de facturation différente.</span></p>
    <div id="billing_different" style="display:none; margin:5px 0;">
    	<p class="txt_oblg">Tous les champs marqués d'un <span class="ast_orange">*</span> sont à remplir obligatoirement.</p>
    	<div class="form_left">
            <label>Titre</label>
			{billingTitleList:h}
            <div class="spacer"></div>
            
            <label>Nom&nbsp;<span class="ast_orange">*</span> </label>
			<input type="text" class="AuthInput" size="33" name="billing[Lastname]" id="billing[lastname]" value="{b_lastname}" />
            <div class="spacer"></div>
            
            <label>Adresse&nbsp;<span class="ast_orange">*</span></label>
			<input size="33" class="AuthInput" type="text" name="billing[Adress]" id="billing[adress]" value="{b_adress}" />
            <div class="spacer"></div>
            
            <label>Code postal&nbsp;<span class="ast_orange">*</span></label>
			<input size="33" type="text" class="AuthInput" name="billing[Zipcode]" id="billing[zipcode]" value="{b_zipcode}" />
            <div class="spacer"></div>
            
            <label>Pays&nbsp;<span class="ast_orange">*</span></label>
			{drawliftb_state:h}
            <div class="spacer"></div>
            
            <label>Téléphone&nbsp;<span class="ast_orange">*</span></label>
			<input size="33" type="text" class="AuthInput" name="billing[Phonenumber]" id="billing[phonenumber]" value="{b_phonenumber}" maxlength="20" />
            <div class="spacer"></div>
        </div>
        <div class="form_right">
            <label>Existant</label>
			{drawliftbilling:h}
            <div class="spacer"></div>
            
            <label>Prénom</label>
			<input type="text" class="AuthInput" size="33" name="billing[Firstname]" id="billing[firstname]" value="{b_firstname}" />
            <div class="spacer"></div>
            
            <label>Compl. adresse</label>
			<input size="33" class="AuthInput" type="text" name="billing[Adress_2]" id="billing[adress_2]" value="{b_adress_2}" />
            <div class="spacer"></div>
            
            <label>Ville&nbsp;<span class="ast_orange">*</span></label>
			<input type="text" size="33" class="AuthInput" name="billing[City]" id="billing[city]" value="{b_city}" />
            <div class="spacer"></div>
            
            <label>Entreprise&nbsp;<span class="ast_orange">*</span></label>
			<input type="text" class="AuthInput" size="33" name="billing[Company]" id="billing[company]" value="{b_company}" />
            <div class="spacer"></div>
            
            <label>Fax</label>
			<input size="33" type="text" class="AuthInput" name="billing[Faxnumber]" id="billing[faxnumber]" value="{b_faxnumber}" maxlength="20" />
            <div class="spacer"></div>
        </div>
    </div>
