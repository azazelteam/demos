<flexy:include src="/head/header.tpl"></flexy:include>
<flexy:include src="menu/menu_alt.tpl"></flexy:include>
<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/estimate.css" />
<!-- Google Code for Dmande de devis Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 1038552525;
var google_conversion_language = "fr";
var google_conversion_format = "1";
var google_conversion_color = "ffffff";
var google_conversion_label = "O4h5CLfAjgEQzZuc7wM";
var google_conversion_value = 0;
if (1.0) {
   google_conversion_value = 1.0;
}
/* ]]> */
</script>
<script type="text/javascript"  
src="http://www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt=""  
src="http://www.googleadservices.com/pagead/conversion/1038552525/?value=1.0&amp;label=O4h5CLfAjgEQzZuc7wM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/confirm_estimate.tpl</span>{end:}
    <h1 class="title_devis">Votre devis a bien &eacute;t&eacute; enregistr&eacute;...</h1>
    
    <p>Votre demande a &eacute;t&eacute; enregistr&eacute;e sous le num&eacute;ro de devis : <span class="ndevis_confirm"><b>{idEstimate}</b></span>
    <br /><br />
    Nous vous remercions de nous avoir consult&eacute;s.<br /><br />
    Votre devis est actuellement trait&eacute; par notre service commercial.
    Nous vous enverrons votre devis d&eacute;finitif par e-mail dans les plus brefs d&eacute;lais. Nous vous remercions de nous avoir consult&eacute;s.</p>
    <div class="spacer"></div><br />  
    
    <h1 class="title_devis">D&egrave;s r&eacute;ception du devis, vous pouvez passer commande</h1>
    <p><span class="libb_confirm">Par t&eacute;l&eacute;phone :</span> 03.88.66.02.03
    <br /><br />
    <span class="libb_confirm">Par fax :</span> en renvoyant votre devis dat&eacute;, sign&eacute; et cachet&eacute; au 03.59.56.99.95
    <br /><br />
    <span class="libb_confirm">Par email :</span> &agrave; l&acute;adresse <a href="mailto:commercial@abisco.fr">commercial@abisco.fr</a>
    </p>
    <div class="spacer"></div><br />  
    
    <p>Merci de la confiance que vous t&eacute;moignez &agrave; notre soci&eacute;t&eacute;. Pour toutes informations compl&eacute;mentaires, <span class="infocomp_orange">n'h&eacute;sitez pas &agrave; nous contacter.</span></p>
    
    <!-- <a href="{start_url}/index.php"><img src="{start_url}/templates/catalog/images/btn_poursuivre_droit.gif"  border="0" /></a> -->
</div> 
<!--====================================== Fin du contenu ======================================-->
<!-- Colonne de droite de la page -->
<!--<flexy:include src="b_right.tpl"></flexy:include> -->           
</div>
</div>

    

<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
            


