<flexy:include src="head/header.tpl"></flexy:include>
<!--<flexy:include src="/menu/menu_alt.tpl"></flexy:include>->
<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/basket.css" />
<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/jquery.bxslider.css" />
<script type="text/javascript" src="{start_url}/js/thickbox/thickbox.js"></script>
<script type="text/javascript" src="{start_url}/js/jquery/ui/blockUI.js"></script>
<link rel="stylesheet" href="{start_url}/js/thickbox/thickbox.css" type="text/css" media="screen" />
<script type="text/javascript">
/* <![CDATA[ */	
	/* ------------------------------------------------------------------------------------------------------ */
	/* utiliser un bon de réduction */	
	function useVoucher() {
			if( $( "#voucher_code" ).val() == '' ){
			 document.getElementById('errorvoucher').style.visibility = "visible";
			 
			//	alert( 'Vous devez saisir un numéro valide de bon de réduction' );
				//return false;
			}else {
		
			document.location = 'basket.php?voucher_code=' + $( "#voucher_code" ).val();
			var voucher_code = document.getElementById('voucher_code').value;
			}
		}	
	/* ------------------------------------------------------------------------------------------------------ */	
/* ]]> */
</script> -->

<!--====================================== Contenu ======================================-->
<script type="text/javascript" language="javascript">
	function updact() {
		document.forms.frmcde.action = 'basket.php?next=1';
	}

	function btncata() {
		document.forms.frmcde.action = '';
		document.forms.frmcde.submit();
	}

	function qtt_plus(itemIndex, isGift) {
		var curVal = $('input#quantity' + itemIndex).val();
		if (isGift == 1) {
			$('input#quantity' + itemIndex).val(1);
		} else {
			$('input#quantity' + itemIndex).val(parseInt(curVal) + 1);
		}

		$('#frmcde').submit();
	}

	function qtt_plus_gift(itemIndex) {

		var curVal = $('input#quantity' + itemIndex).val();
		$('input#quantity' + itemIndex).val(parseInt(curVal) + 1);
		//$('#frmcde').submit();
	}

	function qtt_moins(itemIndex) {
		var curVal = $('input#quantity' + itemIndex).val();
		$('input#quantity' + itemIndex).val(parseInt(curVal) - 1);
		$('#frmcde').submit();
	}

	function qtt_moins_gift(itemIndex) {
		var curVal = $('input#quantity' + itemIndex).val();
		$('input#quantity' + itemIndex).val(parseInt(curVal) - 1);
		//$('#frmcde').submit();
	}

	function sendCommand(idArticle, itemIndex) {
		location.href = "/catalog/basket.php?idarticle=" + idArticle + "&quantity=" + $('input#quantity' + itemIndex).val();
	}
	$(document).ready(function () {
		$('input[name="quantity[]"]').keyup(function () {
			if ($(this).val() != '')
				$('#frmcde').submit();
		});

		$('tr td a.addCart').click(function (e) {
			e.preventDefault();
			console.log("test");

		});




	});
</script>
<div id="center" class="page panier">
	{if:iduser}<span style="color:red;font-weight: bold;">/basket/basket.tpl</span>{end:}

	<div class="data">
		<h1 class="panier__title">Mon panier</h1>
		<div class="spacer"></div>
		<form action="{start_url}/catalog/basket.php?next=1" method="post" name="frmcde" id="frmcde">
			<input type="hidden" name="update" value="1" />
			{if:!basket.getItemCount()}
			<h3>Votre panier est vide.</h3>
			{else:}
			<table cellspacing="0" cellpadding="0" border="0" class="content_basket">
				<tr style="background-color:#dbdbdb;font-weight:bolder;" class="trHead">
					<th></th>
					<th></th>
					<!--<th>Réf.</th>-->
					<th width="320">Référence & Caractéristiques</th>
					<th>Délai de livraison</th>
					<th>Prix unit. H.T.</th>
					{if:hasDiscount}
					<th>Remise</th>
					<th>P.U. Net H.T.</th>
					{end:}
					<th>Quantité</th>
					{if:hasLot}
					<th>Qté / lot</th>
					{end:}
					<th>Prix total H.T.</th>
				</tr>
				{foreach:basketItems,item}
				<tr id="Item{item.getIndex()}" class="{item.get(#idproduct#):h}">
					<td class="tdCenterRight" style="text-align:center;">
						<a href="#" onclick="$('#Item' + {item.getIndex()}).fadeOut('slow',function(){document.location='basket.php?delete&amp;index={item.getIndex()}';}); return false;">
							<!-- <img src="http://www.abisco.fr/templates/catalog/images/delete2.jpg" alt="Supprimer" /> -->
							<i class="fas fa-trash-alt ico-supprimer"></i>
						</a>
					</td>
					<td class="tdCenterLeft" style="text-align:center;">
						<a href="{item.article.getURL()}"><img src="{item.getIcon()}" alt="Icon" width="60" /></a>
					</td>
					<td>
						<div class="articleInfo">
							<div class="productName"><a href="{item.article.getURL()}">{item.get(#summary_1#):h}</a></div>
							<div class="productDesc">{item.get(#designation_1#):h}</div>
							<div>Référence : <span class="productRef">{item.get(#reference#):h}</span></div>
							{if:item.color.name}
							<div>Couleur : <span class="productColor">{item.color.name:h}</span></div>
							{end:}
						</div>
					</td>
					<td align="center" class="tdDelivery">{item.getDeliveryTime():h}</td>
					<td class="tdPrice tdPriceUnit" align="center">
						{if:!item.get(#hidecost#)}
						{item.getPriceET():h}
						{else:}
						sur demande
						{end:}
					</td>
					{if:hasDiscount}
					<td class="tdPrice" align="center">
						{if:item.getDiscountRate()}
						{item.getDiscountRate():h}
						{else:}
						-
						{end:}
					</td>
					<td class="tdPrice" align="center">
						{if:item.getDiscountPriceET()}
						{item.getDiscountPriceET():h}
						{else:}
						-
						{end:}
					</td>
					{end:}
					<td class="tdCenterBold" align="center">
						<p style="display:inline-block; margin-bottom: 0">

							<input style="float:left;padding:4px" type="text" name="quantity[]" id="quantity{item.get(#idarticle#)}" value="{item.getQuantity()}"
							 class="input_quantity" flexy:ignore />

							<span class="up-down" style="float: left; background: none repeat scroll 0% 0% rgb(114, 112, 108); padding: 2px;">
								<a onclick="qtt_plus({item.get(#idarticle#)}, {item.isGift});" style="float: left; margin-top: 0px;"><img src="/templates/catalog/images/btn_plus.png"
									 alt="plus"></a>
								<a onclick="qtt_moins({item.get(#idarticle#)});" style="float: left; clear: both; margin-top: 0px;" class="down"><img
									 src="/templates/catalog/images/btn_moins.png" alt="moins"></a>
							</span>
							
							<input type="submit" name="Modify" class="input_actualiser" value="Recalculer"
							 flexy:ignore />
						</p>
					</td>
					{if:hasLot}
					<td align="center">{item.get(#lot#):h} / {item.get(#unit#):h}</td>
					{end:}
					<td class="tdPrice" align="right">
						{if:!item.get(#hidecost#)}
						{item.getTotalET():h}
						{else:}
						-
						{end:}
					</td>
				</tr>

				{end:}



				{if:customer}
				{if:voucher_code}
				{if:voucher}
				{if:voucherdate}
				{if:voucherexist}
				{if:voucheramountmin}
				{if:voucherproduct}
				<tr>
					<td class="tdCenterRight" style="text-align:center;">
						<a href="{baseURL}/catalog/basket.php?resetbon">
							<!-- <img src="{baseURL}/templates/catalog/images/delete2.jpg" alt="Supprimer" /> -->
							<i class="fas fa-trash-alt ico-supprimer"></i>
						</a>
					</td>
					<td> </td>
					<td>
						<div class="articleInfo">
							<div class="productName">{code} : {summary} &nbsp;</div>
							<div>Référence : <span class="productRef">{reference}</span></div>
						</div>
					</td>
					<td align="center">-</td>
					<td align="center">-</td>
					<td align="center">1</td>
					<td style="color:#00CC00;">-{reduction:h}</td>
				</tr>
				{end:}
				{end:}
				{end:}
				{end:}
				{end:}
				{end:}
				{end:}

			</table>
			<div class="spacer"></div>

			<!-- Bon de réduction -->
			<div class="btnPanierLeft">
				<div class="recap-wrapper">
					<div class="voucher-block" style="width:260px;margin-right:2em">
						{if:customer}
						{if:voucher_code}
						{if:!voucher}
						<p class="msgError">Le code saisi n'est pas valide.</p>
						{else:}
	
						{if:!voucherdate}
						<p class="msgError">Votre bon de réduction n'est plus valable.</p>
						{else:}
						{if:!voucherexist}
						<p class="msgError">Ce bon de réduction est déjà utilisé.</p>
						{else:}
						{if:!voucheramountmin}
						<p class="msgError">Ce bon de réduction est uniquement valable pour une commande supérieur à {amount_min} &nbsp; €</p>
						{else:}
	
						{if:!voucherproduct}
						<p class="msgError">Votre bon de réduction n'est pas valable pour les produits sélectionnés.</p>
	
						{end:}
	
						{end:}
	
						{end:}
						{end:}
	
	
						{end:}
	
	
						{end:}
	
						{if:voucher_code}
						{if:voucher}
						{if:voucherdate}
						{if:voucherexist}
						{if:voucheramountmin}
						{if:!voucherpromo}
						<p class="msgError">Vous ne pouvez pas cumuler un code promotion sur la référence {references} &nbsp;déjà en
							promotion</p>
						{end:}
						{end:}
						{end:}
						{end:}
						{end:}
						{end:}
						<div id="errorvoucher" style="visibility:hidden; display: none">
							<p class="msgError">Vous devez saisir un numéro valide de bon de réduction.</p>
	
						</div>
	
						{if:voucher_code}
						{if:voucher}
						{if:voucherdate}
						{if:voucherexist}
						{if:voucheramountmin}
						{if:voucherproduct}
	
						<script type="text/javascript">
							function vouchers() {
	
								document.getElementById('discount_voucher').style.display = "none";
	
							}
						</script>
						{end:}
						{end:}
						{end:}
						{end:}
						{end:}
						{end:}
	
						{if:!vouchervalider}
						{if:!reduction_ht}
						<div id="discount_voucher" style="display:block">Code réduction :<br />
							<input type="text" name="voucher_code" id="voucher_code" class="input_voucher" value="{voucher_code}" />
							<a href="#" onClick="useVoucher(); return false;" class="btn-med">
								<span>Valider</span>
							</a>
						</div>
						{end:}
	
						{end:}
						{end:}
						<input type="hidden" name="voucher_coder" id="voucher_coder" class="input_voucher" value="{voucher_code}" />
					</div>
					<!--<div class="spacer"></div>-->
					<div class="total-block">
						{if:remise}
						{if:first_order}
						<p class="first_order">Pour votre première commande, nous vous offrons une remise de {discount}</p>
						<div class="spacer"></div>
						{end:}
						<table cellspacing="0" cellpadding="3" width="100%" border="0" class="content_basket table-total">
							<tr>
								<th class="detail">{traduction(#total_price_ht#)}</th>
								<td class="center">{total_amount_ht_avant_remise}  </td>
								<th class="detail">{traduction(#taux_remise#)}</th>
								<td class="center">{remise_rate} </td>
							</tr>
							<tr>
								<th class="tdPrice">{traduction(#total_ht_remise#)}</th>
								<td class="tdPrice">{total_amount_ht} </td>
								<th class="tdPrice">{traduction(#discount#)}</th>
								<td class="tdPrice">{discount} </td>
							</tr>
						</table>
						<div class="spacer"></div>
						{end:}
		
						<flexy:include src="basket_amounts.tpl"></flexy:include>
					</div>
				</div>

				<!-- Ancien tableau récap de prix -->
				
				<!-- <table cellspacing="0" cellpadding="3" width="100%" border="0" class="recap_basket" style="margin-top:10px;margin-right:1%;clear:both;">
					{if:is_valid}
					<tr>
						<th class="Amounts_port">{traduction(#total_price_ht#)}</th>
						<td class="price">{total_amount_ht_avant_remise}  </td>
					</tr>
					<tr>
						<th class="Amounts_port">Taux de remise commerciale</th>
						<td class="pricet">{total_discount} </td>
					</tr>
					<tr>
						<th class="Amounts_port">Remise commerciale</th>
						<td class="price">{total_discount_amount} </td>
					</tr>
					{end:}
					{if:allowOrder} -->
					<!--<tr>
									<th class="Amounts_port">Frais de port et emballage T.T.C</th>
									<td class="price">
										{basket.getChargesATI():h}
									</td>
								</tr>-->
					<!-- <tr style="background-color:#dbdbdb;font-weight:bolder;">
						<th class="Amounts_port" style="font-weight:normal;text-align:left;width:190px;">
							<span style="font-weight:bold">Frais de port et emballage H.T.</span>

						</th>
						<td class="price" style="text-align:right">
							{basket.getChargesET():h}
						</td>
					</tr>
					{if:customer}
					{if:voucher_code}
					{if:voucher}
					{if:voucherdate}
					{if:voucherexist}
					{if:voucheramountmin}
					{if:voucherproduct}

					<tr class="allowOrder">
						<th align="left" style="color:#00CC00">Réduction total</th>
						<td align="right" style="color:#00CC00">-{reduction:h} &nbsp;</td>
					</tr>
					{end:}
					{end:}
					{end:}
					{end:}
					{end:}
					{end:}
					{end:}

					{if:gift_token_amount}
					<tr>
						<th class="Amounts_port_dotted">Montant de votre chèque-cadeau</th>
						<td class="Amounts_port_dotted">{gift_token_amount}</td>
					</tr>
					{end:}




					<tr>
						<th align="left">Montant Total HT</th>
						<td align="right">{net_to_pay_ht} €</td>
					</tr>
					{if:reduction_ht}
					<tr class="allowOrder">
						<th align="left" style="color:#00CC00">Réduction totale H.T</th>
						<td align="right" style="color:#00CC00">-{reduction_ht:h} €</td>
					</tr>
					{end:}
					<tr>
						<th align="left">Montant total TTC</th>
						<td align="right">{net_to_pay} €</td>
					</tr>
					{end:}
				</table> -->

				<div class="spacer"></div>
				{end:}
				<div class="spacer"></div>
				<p class="panier__bloc-action">
					<a href="javascript:history.back();" class="btn-med">Poursuivre ma visite</a>
					<a href="account.php" class="btn-med">Mon compte</a>
					<div class="perso1">
						{if:allowOrder}
						{if:customer}
						<input type="button" class="btn-med grey" value="Commander" onclick="document.location='order.php?voucher_code=' + $('#voucher_coder' ).val();" />
						{else:}
						<input type="button" class="btn-med grey" value="Commander" onclick="document.location='register.php?voucher_code=' + $('#voucher_coder' ).val();" />
						{end:}
						{end:}
					</div>
				</p>

				<div class="spacer"></div>
		</form>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>

	{if:basket.getItemsbCount()}
	<!-- La nouvelle structure, flexy à corriger -->

	<!-- <div class="data">
		<h1 class="panier__title">Voir aussi</h1> -->


		<!-- <form action="{start_url}/catalog/basket.php?next=1" method="post" name="frmcde3" id="frmcde">
			<input type="hidden" name="update3" value="1" />
			<div class="voir-aussi prod-list__wrapper row">
				{foreach:basketItemsBasket,itemG}
				<div id="Item{itemG.getIndex()}" class="{itemG.get(#idproduct#):h} prod-item col-md-4 col-sm-6">
					<div class="product-item">
						
						<a class="product-item__info" href="{itemG.article.getURL()}" itemprop="url">
							<span class="ruban promo" flexy:if="product.getPromotionCount()"></span>
							
							<img class="product-item__img" src="{itemG.getIcon()}" alt="{itemG.article.getURL()}" />

							<h4 class="cat-item__title">{itemG.get(#summary_1#):h}</h4>

							<div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
								<span class="tdPrice tdPriceUnit" align="center">
									{if:!itemG.get(#hidecost#)}
									{itemG.getPriceET():h}
									{else:}
									sur demande
									{end:}
								</span>
								{if:hasDiscountG}
								<span class="tdPrice" align="center">
									{if:itemG.getDiscountRate()}
									{itemG.getDiscountRate():h}
									{else:}
									-
									{end:}
								</span>
								<span class="tdPrice" align="center">
									{if:itemG.getDiscountPriceET()}
									{itemG.getDiscountPriceET():h}
									{else:}
									-
									{end:}
								</span>
							</div>

						</a>

					</div>
				</div>
				{end:}
			</div>
		</form> -->


		<!-- Ancien "Voir aussi" en tableau -->
		<!-- <form action="{start_url}/catalog/basket.php?next=1" method="post" name="frmcde3" id="frmcde">
			<input type="hidden" name="update3" value="1" />
			<table cellspacing="0" cellpadding="0" border="0" class="content_basket">
				<tr style="background-color:#dbdbdb;font-weight:bolder;" class="trHead">

					<th></th>
					<th width="320">Référence & Caractéristiques</th>
					<th>Délai de livraison</th>
					<th>Prix unit. H.T.</th>
					{if:hasDiscountG}
					<th>Remise</th>
					<th>P.U. Net H.T.</th>
					{end:}
					<th>Quantité</th>
					{if:hasLotG}
					<th>Qté / lot</th>
					{end:}

				</tr>
				{foreach:basketItemsBasket,itemG}
				<tr id="Item{itemG.getIndex()}" class="{itemG.get(#idproduct#):h}">

					<td class="tdCenterLeft" style="text-align:center;">
						<a href="{itemG.article.getURL()}"><img src="{itemG.getIcon()}" alt="Icon" width="60" /></a>
					</td>
					<td>
						<div class="articleInfo">
							<div class="productName"><a href="{itemG.article.getURL()}">{itemG.get(#summary_1#):h}</a></div>
							<div class="productDesc">{itemG.get(#designation_1#):h}</div>
							<div>Référence : <span class="productRef">{itemG.get(#reference#):h}</span></div>
							{if:itemG.color.name}
							<div>Couleur : <span class="productColor">{itemG.color.name:h}</span></div>
							{end:}
						</div>
					</td>
					<td align="center" class="tdDelivery">{itemG.getDeliveryTime():h}</td>
					<td class="tdPrice tdPriceUnit" align="center">
						{if:!itemG.get(#hidecost#)}
						{itemG.getPriceET():h}
						{else:}
						sur demande
						{end:}
					</td>
					{if:hasDiscountG}
					<td class="tdPrice" align="center">
						{if:itemG.getDiscountRate()}
						{itemG.getDiscountRate():h}
						{else:}
						-
						{end:}
					</td>
					<td class="tdPrice" align="center">
						{if:itemG.getDiscountPriceET()}
						{itemG.getDiscountPriceET():h}
						{else:}
						-
						{end:}
					</td>
					{end:}
					<td class="tdCenterBold" align="center">
						<p style="display:inline-block; margin-bottom: 0">
							<input style="float:left;padding:8px" type="text" name="quantity[]" id="quantity{itemG.get(#idarticle#)}" value="{itemG.getQuantity()}"
							 class="input_quantity" flexy:ignore />
							<span class="up-down" style="float: left; background: none repeat scroll 0% 0% rgb(114, 112, 108); padding: 2px;">
								<a onclick="qtt_plus_gift({itemG.get(#idarticle#)});" style="float: left; margin-top: 0px;"><img src="/templates/catalog/images/btn_plus.png"
									 alt="plus"></a>
								<a onclick="qtt_moins_gift({itemG.get(#idarticle#)});" style="float: left; clear: both; margin-top: 0px;" class="down"><img
									 src="/templates/catalog/images/btn_moins.png" alt="moins"></a>
							</span>
						</p>
						<br />

						<a href="#" class="btn-med addCart" idarticle="" onclick="sendCommand({itemG.get(#idarticle#):h}, {itemG.get(#idarticle#)}); ">
							Ajouter au panier <a />
					</td>
					{if:hasLotG}
					<td align="center">{itemG.get(#lot#):h} / {itemG.get(#unit#):h}</td>
					{end:}

				</tr>

				{end:}
			</table>
		</form> -->
	</div>

	<div class="spacer"></div>
	
	{end:}
	{if:basket.getItemsgCount()}
	<div class="data">
		<h1 class="panier__title">Autres cadeaux</h1>
		<form action="{start_url}/catalog/basket.php?next=1" method="post" name="frmcde2" id="frmcde">
			<input type="hidden" name="update2" value="1" />
			<table cellspacing="0" cellpadding="0" border="0" class="content_basket">
				<tr style="background-color:#dbdbdb;font-weight:bolder;" class="trHead">

					<th></th>
					<!--<th>Réf.</th>-->
					<th width="320">Référence & Caractéristiques</th>
					<th>Délai de livraison</th>
					<th>Prix unit. H.T.</th>
					{if:hasDiscountG}
					<th>Remise</th>
					<th>P.U. Net H.T.</th>
					{end:}
					<th>Quantité</th>
					{if:hasLotG}
					<th>Qté / lot</th>
					{end:}

				</tr>
				{foreach:basketItemsGift,itemG}
				<tr id="Item{itemG.getIndex()}" class="{itemG.get(#idproduct#):h}">

					<td class="tdCenterLeft" style="text-align:center;">
						<a href="{itemG.article.getURL()}"><img src="{itemG.getIcon()}" alt="Icon" width="60" /></a>
					</td>
					<td>
						<div class="articleInfo">
							<div class="productName"><a href="{itemG.article.getURL()}">{itemG.get(#summary_1#):h}</a></div>
							<div class="productDesc">{itemG.get(#designation_1#):h}</div>
							<div>Référence : <span class="productRef">{itemG.get(#reference#):h}</span></div>
							{if:itemG.color.name}
							<div>Couleur : <span class="productColor">{itemG.color.name:h}</span></div>
							{end:}
						</div>
					</td>
					<td align="center" class="tdDelivery">{itemG.getDeliveryTime():h}</td>
					<td class="tdPrice tdPriceUnit" align="center">
						{if:!itemG.get(#hidecost#)}
						{itemG.getPriceET():h}
						{else:}
						sur demande
						{end:}
					</td>
					{if:hasDiscountG}
					<td class="tdPrice" align="center">
						{if:itemG.getDiscountRate()}
						{itemG.getDiscountRate():h}
						{else:}
						-
						{end:}
					</td>
					<td class="tdPrice" align="center">
						{if:itemG.getDiscountPriceET()}
						{itemG.getDiscountPriceET():h}
						{else:}
						-
						{end:}
					</td>
					{end:}
					<td class="tdCenterBold" align="center">
						<p style="display:inline-block">
							<input style="float:left;padding:4px" type="text" name="quantity[]" id="quantity{itemG.get(#idarticle#)}" value="{itemG.getQuantity()}"
							 class="input_quantity" flexy:ignore disabled="" />
							<!--span class="up-down" style="float: left; background: none repeat scroll 0% 0% rgb(114, 112, 108); padding: 2px;">
														<a onclick="qtt_plus_gift({itemG.get(#idarticle#)});" style="float: left; margin-top: 0px;"><img src="/templates/catalog/images/btn_plus.png" alt="plus"></a>
														<a onclick="qtt_moins_gift({itemG.get(#idarticle#)});" style="float: left; clear: both; margin-top: 0px;" class="down"><img src="/templates/catalog/images/btn_moins.png" alt="moins"></a>
													</span-->
						</p>
						<br />
						<!--p style="display:inline-block"><input type="submit" name="Modify" class="input_actualiser" value="Recalculer" flexy:ignore /></p-->

						<a href="#" class="btn-med grey addCart" idarticle="" onclick="sendCommand({itemG.get(#idarticle#):h}, {itemG.get(#idarticle#)}); ">
							Ajouter au panier <a />
					</td>
					{if:hasLotG}
					<td align="center">{itemG.get(#lot#):h} / {itemG.get(#unit#):h}</td>
					{end:}

				</tr>

				{end:}
			</table>
		</form>
	</div>
	<div class="spacer"></div>
	<div class="spacer"></div>
	{end:}

	{if:complements}
	<div id="complements-products">
		<h3 class="crosssell"><span><strong>Découvrez</strong> également</span></h3>
		<div class="slider3">
			{foreach:complements,complement}
			<div class="slide">
				<a href="{complement.getURL()}"><img src="{complement.getImageURI(#170#)}" alt="{complement.get(#name_1#):h}" /></a>
				<p class="complements_name"><a href="{complement.getURL()}">{complement.get(#name_1#):h}</a></p>
				<p class="complements_price">&agrave; partir de <strong>{complement.getLowerPriceET():h} <small>HT</small></strong></p>
			</div>
			{end:}
		</div>
	</div>
	<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.bxslider.js"></script>
	<flexy:toJavascript slide_auto="slide_auto"></flexy:toJavascript>
	<script type="text/javascript">
		jQuery(document).ready(function () {
			jQuery('.slider3').bxSlider({
				slideWidth: 236,
				minSlides: 4,
				maxSlides: 4,
				moveSlides: 1,
				auto: slide_auto,
				responsive: true,
				slideMargin: 10,
				pager: false,
				controls: true,
				adaptiveHeight: true
			});
		});
	</script>
	{end:}
	{if:basket.getItemCount()}
</div>{end:}
</div>

<flexy:tojavascript allowOrder={allowOrder} />
<script type="text/javascript">
	function hidelogin() {
		$('#loginpanel').animate({
			top: -120
		}, {
			queue: false,
			easing: 'easeOutCirc',
			duration: 300
		});
	}
	$('#mylogin').click(function (e) {
		e.stopPropagation();
		$('#loginpanel').animate({
			top: 0
		}, {
			queue: false,
			easing: 'easeOutCirc',
			duration: 200
		});
	});
	$("body").click(function (e) {
		if (e.target.id == "loginpanel" || $(e.target).parents("#loginpanel").size()) e.stopPropagation();
		else hidelogin();
		if (e.target.id == "menu" || $(e.target).parents("#menu").size()) e.stopPropagation();
		else menureset();
	});
	$("form#login #email, form#login #password").textPlaceholder();
	$("form#search #search-text").textPlaceholder();
	$("form#newsletter #emailnews").textPlaceholder();
	$('#icons_compare').mouseenter(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_compare').mouseleave(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseenter(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseleave(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
</script>

<flexy:include src="foot.tpl"></flexy:include>
</div>
