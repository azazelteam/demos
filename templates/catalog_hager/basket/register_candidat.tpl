<flexy:include src="/head/header.tpl"></flexy:include>

<!--<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/basket.css" />-->
<!--<link rel="stylesheet" href="{start_url}/templates/catalog/css/bootstrap.min.css" />-->

<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.maskedinput.js"></script>

<!--====================================== Contenu ======================================-->
<div id="center" class="page">
    <script type='text/javascript'>

    jQuery(function($){
        $("#num_social").mask("9/99/99/999/999");
		$("#date_naissance").mask("9999/99/99");
    });

    jQuery(document).ready(function() {
        // validate the comment form when it is submitted
        jQuery("#loginpanel form").validate({
            onSubmit : true,
            eachInvalidField : function() {
                alert('Merci de renseigner une adresse email valide');
            }
        });
        jQuery('#RegisterForm').validate({
            onChange : true,
            onKeyup : true,
            onBlur : true,
            onSubmit : true,
            eachValidField : function() {
                if (jQuery(this).attr('name') != 'newsletter' && jQuery(this).attr('name') != 'title' )
                {
                    jQuery(this).removeClass('not_valid');
                    jQuery(this).addClass('valid');
                    // if (jQuery(this).siblings('span.check').length == 0)
                    //     jQuery(this).after('<span class="check"></span>');
                    // if (jQuery(this).siblings('span.not_valid').length != 0)
                    //     jQuery(this).siblings('span.not_valid').fadeOut('slow').remove();
                }
            },
            eachInvalidField : function() {
                if (jQuery(this).attr('name') == 'profile')
                {
                    if (jQuery(this).find('option:selected').val() == '-1')
                    {
                        jQuery('#registerblock').html('<h1>Nous ne pouvons traiter votre demande d\'inscription </h1><p>&nbsp;</p><p>Madame,Monsieur,</p><p>&nbsp;</p><p>Nos catalogues et nos offres internet s\'adressent exclusivement aux professionnels conform&eacute;ment &agrave; nos <a href="/cgv.php">Conditions G&eacute;n&eacute;rales de ventes</a>.</p><p>&nbsp;</p><p>&nbsp;</p><p>Cordialement</p><p>Le service Client Abisco.</p><br /><br /><a href="/">&#x3C; Retour &agrave; la page d\'accueil</a>');
                    }
                    else
                        jQuery(this).removeClass('valid').addClass('not_valid');
                }
                else
                {
                    if (jQuery(this).attr('name') == 'email'){
                        if (jQuery(this).siblings('span.not_valid').length == 0)
                            jQuery(this).after('<span class="not_valid">Merci de renseigner une adresse mail valide</span>');
                    }
                    if (jQuery(this).attr('name') == 'pwd'){
                        if (jQuery(this).siblings('span.not_valid').length == 0)
                            jQuery(this).after('<span class="not_valid">Le champ Mot de passe doit comporter au minimum 6 caract&egrave;res</span>');
                    }
                    jQuery(this).removeClass('valid').addClass('not_valid');
                    jQuery(this).siblings('span.valid').remove();
                }
                // if (jQuery(this).siblings('span.check').length == 0)
                //     jQuery(this).after('<span class="check"></span>');
            }
        });
        jQuery.validateExtend({
            email : {
                required : true,
                pattern : /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/,
                conditional : function(value) {
                    var verif = /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
                    return ( verif.test(value) );
                }
            },
            pwd : {
                required : true,
                conditional : function(value){
                    return value.length >= 6;
                }
            },
            profile : {
                required : true,
                conditional : function(value){
                    return value == '1' || value == 2 || value == 3;
                }
            },
            number : {
                required : true,
                pattern : /^[0-9]+$/,
                conditional : function(value){
                    return value.length < 21;
                }
            }
        });
        jQuery('input[name="pwd"]').focusout(function() {
            jQuery('input[name="pwd_confirm"]').val(jQuery(this).val());
        });
        jQuery('#loginblock form').validate({
            onKeyup : true,
            onBlur : true,
            onSubmit : true,
            eachValidField : function() {
                jQuery(this).removeClass('not_valid').addClass('valid');
                if (jQuery(this).siblings('span.not_valid').length != 0)
                    jQuery(this).siblings('span.not_valid').fadeOut('slow').remove();
            },
            eachInvalidField : function() {
                if (jQuery(this).attr('name') == 'Login'){
                    if (jQuery(this).siblings('span.not_valid').length == 0)
                        jQuery(this).after('<span class="not_valid">Merci de renseigner une adresse mail valide</span>');
                }
                jQuery(this).removeClass('valid').addClass('not_valid');
            },
        });
        jQuery.validateExtend({
            login : {
                required : true,
                pattern : /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/,
                conditional : function(value) {
                    var verif = /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
                    return ( verif.test(value) );
                }
            }
        });
    });
    </script>
    {if:customer}
        <flexy:include src="registerok.tpl"></flexy:include>
    {else:}
    
    <script type='text/javascript'>
    
    
    <!--
    
    // Test des champs du formulaire d'enregistrement
    /*function verifForm (){
         
        var emailAddr = RegisterForm.elements[ 'email' ].value;
        
        if(emailAddr == ''){
            alert('Le champ Email est vide !!!');
            return false;
        }else{
            if(!isEmail(emailAddr)){
                alert('Le champ Email n\'est pas valide !!!');
                return false;
            }
        }       
            
      
        if( RegisterForm.elements[ 'pwd' ].value == "") {
            
            alert('Le champ Mot de passe est vide !!!');
            return false;
            
        }else if( RegisterForm.elements[ 'pwd' ].value.length < 6) {
                
                alert('Le champ Mot de passe doit comporter au minimum 6 caract\350res !!!');
                return false;
      
        }
    
        if( RegisterForm.elements[ 'pwd_confirm' ].value == "") {
        
            alert('Le champ confirmation mot de passe est vide !!!');
            return false;
            
        }else if( RegisterForm.elements[ 'pwd_confirm' ].value != RegisterForm.pwd.value) {
        
            alert('Le champ confirmation de mot de passe doit \352tre identique au champ Mot de passe !!!');
            return false;
                
        }
        if(this.RegisterForm.elements['title'].options[this.RegisterForm.elements['title'].selectedIndex].value == ''){
            
            alert('Le champ Titre est vide !!!');
            return false;
        
        } 
        
        if( RegisterForm.elements[ 'lastname' ].value == '') {
        
            alert('Le champ Nom ?> est vide !!!');
            return false;
            
        }
          
        if( RegisterForm.elements[ 'siret' ].value.length > 14) {
        
            alert('Le champ Siret ne doit contenir plus de 14 caract\350res!!!');
            return false;
            
        }
         
        if( RegisterForm.elements[ 'naf' ].value.length > 5) {
        
            alert('Le champ Naf ne doit contenir plus de 5 caract\350res!!!');
            return false;
            
        }
              
        if( RegisterForm.elements[ 'phonenumber' ].value == '') {
        
            alert('Le champ T\351l\351phone est vide !!!');
            return false;
            
        }
          
        if( RegisterForm.elements[ 'company' ].value == '') {
          
            alert('Le champ Soci\351t\351 est vide !!!');
            return false;
            
        }
          
        if( RegisterForm.elements[ 'adress' ].value == '') {
          
            alert('Le champ Adresse est vide !!!');
            return false;
            
        }
        
        if( RegisterForm.elements[ 'zipcode' ].value == "") {
          
            alert('Le champ Code Postal est vide !!!');
            return false;
            
        }
        
        if( RegisterForm.elements[ 'city' ].value == "") {
          
            alert('Le champ Ville est vide !!!');
            return false;
            
        }
        
        if( RegisterForm.elements[ 'siret' ].value == "") {
          
            alert('Le champ N\260 Siret est vide !!!');
            return false;
            
        }
          
        return true;
         
    }*/
    
    function isEmail(strSaisie) {
        var verif = /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
        return ( verif.test(strSaisie) );
    }
    
    //-->
    </script>

    {if:orderInProgress}
    {if:iduser}<span style="color:red;font-weight: bold;">/basket/register.tpl</span>{end:}
    
   <ul class="basket_fil">
        <li class="un"><a href="/catalog/basket.php"><span>Panier commande</span></a></li>
        <li class="deux actif"><span>Authentification</span></li>
        <li class="trois"><span>Livraison et facturation</span></li>
        <li class="quatre"><span>Confirmation</span></li>
    </ul>   
    
    <div class="spacer"></div>
    {end:}
    {if:loginfail}
        {if:iduser}<span style="color:red;font-weight: bold;">/basket/register.tpl</span>{end:}
    <div id="loginfail" style="text-align:center;color:#f00;margin-bottom:15px">Votre adresse email ou mot de passe est incorrect.</div>{end:}
    {if:errorInscription}<div id="loginfail" style="text-align:center;color:#f00;margin-bottom:15px">{errorInscription:h}</div>{end:}
    
	<br><br>
	<span style="color:#E52516; font-size:22px"><strong> Formulaire enregistrement d'inscription</strong></span>
	<br><br>
	
    <div class="register_bloc no-border-r">
        <h2><input type="radio" onclick="$('#registerblock').slideDown('slow'); $('#loginblock').slideUp('fast');" value="0" name="action_account"> <strong>Nouveau candidat ?</strong> Cr&eacute;ez votre compte</h2>
    </div>
    
    <br>
    
    <div class="register_bloc">
        <div class="border">
            <h2><input type="radio" checked="checked" onclick="$('#registerblock').slideUp('fast'); $('#loginblock').slideDown('slow');" value="1" name="action_account"> <strong>D&eacute;j&agrave; inscript ?</strong> Connectez-vous</h2>
        </div><br>
        <br>
    </div>

    <br><br>

    <div id="loginblock" class="loginblock">
        <form method="post" name="Form_logon" target="_top" action="/gestra/account_candidat.php" id="logininregister">                    
            <label for="emailrlogin"><span style="font-size:14px !important;">Adresse email <span class="ast_orange">*</span></label>
            <input type="text" style="height:30px" id="emailrlogin" name="Login" class="floatL " placeholder="adresse email" data-required="true" data-validate="login" />                          
            <label for="passrwlogin" style="font-size:14px !important;"><span style="font-size:14px !important;">Mot de passe <span class="ast_orange">*</span></label>
    <br><br>
            
            <input type="password" style="height:30px" id="passrwlogin" name="Password" class="floatL" placeholder="mot de passe" data-required="true" />
            <input type="submit" value="S'identifier" />   
            <a href="#" onclick="forgetPassword(); return false;" style="float:right">Mot de passe oublié ?</a>
            {if:orderInProgress}
            <input type="hidden" name="redirectTo" id="rredirection" value="order.php" flexy:ignore />
            {else:}
                <input type="hidden" name="redirectTo" id="rredirection" value="../gestra/account_candidat.php" flexy:ignore />
            {end:}               
        </form>
    </div>
    <div class="spacer" style="height:20px"></div>
    <div id="registerblock" style="display: none;" >
        <div class="spacer"></div>
    <p class="txt_oblg">Tous les champs marqu&eacute;s d'un <span class="ast_orange">*</span> sont &agrave; remplir obligatoirement</p>
    <div class="spacer"></div><br/>
        <form action="{start_url}/catalog/register.php" id="RegisterForm" method="post"> 
    <!--<input type="hidden" name="FromScript" value="{ScriptName}" />-->
    {if:orderInProgress}
        <input type="hidden" name="redirectTo" id="redirection" value="order.php" flexy:ignore/>
    {else:}
        <input type="hidden" name="redirectTo" id="redirection" value="../gestra/account_candidat.php" flexy:ignore/>
    {end:}
    <div class="spacer"></div>
    
    {if:errorMsgNotEmpty}                   
        <p class="errorMsg">{errorMsg:h}</p>
    {end:}

    <h3>Mes informations de connexion</h3>
	
<table style="width: 100%;">
        
        <tbody>
            <tr>
                <td>
				<div class="form_line" style="display: block;">
                            <div class="form-label" style="display: inline-block;block;margin-top:14px !important;">
        <span style="font-size:14px !important;">E-mail : <span class="ast_orange">*</span></label>
		</div>
                <div style="display: inline-block;">
        <input type="text" name="email" value="{userInfoEmail:h}" class="AuthInput" data-required="true" size="40" data-validate="email" />
		</div>
</td><td>
<div class="form_line" style="display: block;">
                            <div class="form-label" style="display: inline-block;block;margin-top:14px !important;">
        <span style="font-size:14px !important;">Mot de passe :  <span class="ast_orange">*</span></label>
				</div>
                <div style="display: inline-block;">
        <input type="password" name="pwd" value="{userInfoPwd:h}" class="AuthInput" maxlength="20" size="40" data-required="true" data-validate="pwd" />
        </div>
        <div class="spacer"></div>
        
        <!--<label>Confirmation mot de passe <span class="ast_orange">*</span></label>-->
        <input type="password" name="pwd_confirm" value="{userInfoPwdConfirm:h}" class="AuthInput" maxlength="20" size="40" style="display:none"/>
        <!--<div class="spacer"></div>-->
		</div>
 </td>
</tr>
</table> 
	
	<br><br>
 
    
    <table style="width: 100%;">
        
        <tbody>
            <tr>
                <td>
                    <h3>Mes coordonn&eacute;es</h3>
                    <br><br>
                    <div class="spacer"></div>

                    <div class="form_niaina">
                        <div class="form-label" style="display: inline-block;"> <label>Civilit&eacute; <span class="ast_orange">*</span></label></div>
                        <div style="display: inline-block;"> 
                            <div>
                                <div style="display: inline-block;"><input type="radio" style="display: inline-block;height:auto" name="title" class="input_radio" value="1" checked="checked" /> M.</div>
                                <div style="display: inline-block;"><input type="radio" style="display: inline-block;height:auto" name="title" class="input_radio" value="2" /> Mme.</div>
                                <div style="display: inline-block;"><input type="radio" style="display: inline-block;height:auto" name="title" class="input_radio" value="3" /> Mlle.</div>
                            </div>
                        </div>
       
            
                        <div class="form_line" style="display: block;">
                            <div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Nom <span class="ast_orange">*</span></label></div>
                <div style="display: inline-block;"><input class="form-input" type="text" name="lastname" value="{userInfoLastname:h}" size="40" maxlength="20" data-required="true" /></div>
                </div>
                
                <div class="form_line" style="display: block;margin-top:12px !important;"><div class="form-label" style="display: inline-block;"><span style="font-size:14px !important;">Pr&eacute;nom </label></div>
                <div style="display: inline-block;"><input class="form-input" type="text" name="firstname" value="{userInfoFirstname:h}" size="40" maxlength="20" /></div>
                </div>
                
                <div class="form_line" style="display: block;"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">T&eacute;l. <span class="ast_orange">*</span></label></div>
                <div style="display: inline-block;"><input class="form-input" type="text" name="phonenumber" value="{userInfoPhone:h}" size="40" maxlength="20" data-required="true" data-validate="number" /></div>
                </div>
                
                <!--<div class="form_line"><label>T&eacute;l. portable </label>
                <input type="text" name="gsm" value="{userInfoGsm:h}" size="40" maxlength="20" />
                </div>-->
                
				<!--
                <div class="form_line" style="display: block;"><div class="form-label" style="display: inline-block;"><label>Fax. </label></div>
                <div style="display: inline-block;"><input class="form-input" type="text" name="faxnumber" value="{userInfoFax:h}" size="40" maxlength="20" /></div>
                </div>
				-->
                
                <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Adresse <span class="ast_orange">*</span></label></div>
                <div style="display: inline-block;"><input class="form-input" type="text" name="adress" value="{userInfoAdress:h}"  maxlength="100" data-required="true" /></div>
                </div>
                
				<div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Adresse compl. </label></div>
				<div style="display: inline-block;"><input type="text" name="adress_2" value="{userInfoAdress_2:h}" size="40"  maxlength="100"/></div>
				</div>
        
        <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Code postal <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="zipcode" value="{userInfoZipcode:h}" size="40"  maxlength="10" data-required="true" /></div>
        </div>
        
        <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Commune <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="city" value="{userInfoCity:h}" size="40"  maxlength="50" data-required="true"/></div>
        </div>
        <div class="form_line"><div class="form-label" style="display: inline-block;"><span style="font-size:14px !important;">Pays <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><select class="form-input" id="idstate" name="idstate"  style="height:40px;">
            <option value="1" selected="selected">France</option><option value="3">Alg&eacute;rie</option><option value="2">Andorre</option><option value="4">Allemagne</option><option value="5">Arabie Saoudite</option><option value="6">Autriche</option><option value="7">Belgique</option><option value="8">Bulgarie</option><option value="9">Chypre</option><option value="10">Danemark</option><option value="11">Espagne</option><option value="12">Estonie</option><option value="13">Grande Bretagne</option><option value="14">Gr&egrave;ce</option><option value="15">Hongrie</option><option value="16">Irlande</option><option value="17">Islande</option><option value="18">Israel</option><option value="19">Italie</option><option value="20">Koweit</option><option value="21">Lettonie</option><option value="22">Liban</option><option value="23">Liechtenstein</option><option value="24">Lithuanie</option><option value="25">Luxembourg</option><option value="26">Maroc</option><option value="27">Monaco</option><option value="28">Norv&egrave;ge</option><option value="29">Pays-bas</option><option value="30">Pologne</option><option value="31">Portugal</option><option value="32">R&eacute;publique Tch&egrave;que</option><option value="33">Roumanie</option><option value="34">Slovaquie</option><option value="35">Slov&eacute;nie</option><option value="36">Su&egrave;de</option><option value="37">Suisse</option><option value="38">Tunisie</option><option value="39">Turquie</option><option value="40">Ukraine</option><option value="41">Emirats Arabes Unis</option><option value="42">S&eacute;n&eacute;gal</option><option value="43">Saint-Vincent-et-les-Grenadines</option><option value="44">France (DOM-TOM)</option><option value="45">Burkina Faso</option><option value="46">Canada</option><option value="47">Congo</option><option value="48">France H.T.</option><option value="49">Madagascar</option><option value="50">R&eacute;publique de Maurice</option><option value="51">R&eacute;publique du Congo</option><option value="52">C&ocirc;te d'Ivoire</option>
        </select></div>
        </div>
        
             </td>
             <td>
<!--
                 <h3 style="margin-bottom: 5px">Informations société</h3>
                 <div class="spacer"></div>
                   <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><label>Profil <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block; margin-bottom: 5px"><select class="form-input" type="text" name="profile" data-required="true" data-validate="profile" style="height:40px;">
            <option value="0"></option>
            <option value="1">Professionnel</option>
            <option value="2">Association</option>
            <option value="3">Administration</option>
            <option value="-1">Particulier</option>
        </select></div>
        </div>
        <div class="spacer"></div>
        
        <div class="form_line"><div class="form-label" style="display: inline-block;"><label>Raison sociale  <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="company" value="{userInfoCompany:h}" size="40"  maxlength="50" data-required="true" /></div>
		

        </div>
        
        
        
        <div class="form_line"><div class="form-label" style="display: inline-block;"><label>SIRET </label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="siret" value="{userInfoSiret:h}" size="40"  maxlength="14"/></div>
		-->
        </div>
        <h3 style="margin-bottom: 5px">Information personnelles : </h3>
        <div class="spacer"></div>

        <!--<div class="form_line"><div class="form-label" style="display: inline-block;>Date de naissance <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input type="date" name="birthday" size="10" maxlength="10" value="" id="date_naissance"></div>-->

		 <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:14px !important;"><span style="font-size:14px !important;">Date naissance (AAAA/MM/JJ)<span class="ast_orange">*</span></label></div>
		 
		 <div style="display: inline-block;"> <input class="form-input" type="text" name="buyer_info10" value="{userInfoBuyer_info10:h}" size="10" id="date_naissance" maxlength="10" data-required="true" /></div>
		 
        <!--<div style="display: inline-block;"><input type="text" name="birthday" size="10" maxlength="10" value="" id="date_naissance"></div>-->
		
		
        <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Lieu de naissance <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="buyer_info1" value="{userInfoBuyer_info1:h}"  maxlength="100" data-required="true" /></div>
		
        


     </div>
    <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Nationalité <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="buyer_info2" value="{userInfoBuyer_info2:h}"  maxlength="100" data-required="true" /></div>
    </div>
 <!--
    <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">N° Sécurité Sociale <span class="ast_orange">*</span></label></div>
       <div style="display: inline-block;"> <input class="form-input" type="text" name="buyer_info3" value="{userInfoBuyer_info3:h}"  maxlength="100" data-required="true" /></div>
     </div> 
 -->
     <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><label>N° Sécurité Sociale <span class="ast_orange">*</span></label></div>
       <div style="display: inline-block;"> <input id="num_social" class="form-input" type="text" name="buyer_info3" value="{userInfoBuyer_info3:h}"  maxlength="100" data-required="true" /></div>
     </div>

      <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">Situation familiale <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;">
            <div style="display: inline-block;"><input style="display: inline-block;height:auto" type="radio" name="buyer_info4" class="input_radio" value="marié"/>&nbsp; Mari&eacute;</div>

            <div style="display: inline-block;"><input style="display: inline-block;height:auto" type="radio" name="buyer_info4" class="input_radio" value="divorce"/>&nbsp; Divorc&eacute;</div>
            
            
            <div style="display: inline-block;"><input style="display: inline-block;height:auto" type="radio" name="buyer_info4" class="input_radio" value="veuf"/>&nbsp; Veuf</div>
            
            <div style="display: inline-block;"><input style="display: inline-block;height:auto" type="radio" name="buyer_info4" class="input_radio" value="celibataire"/> &nbsp;C&eacute;libataire</div>
        </div>
      </div>

    <br>

    <div class="spacer"></div>
    <h3 style="margin-bottom: 5px">Information bancaires :</h3>
    <div class="spacer"></div>
     <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">IBAN <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="n_compte" value="{userInfoN_compte:h}" minlength="27" maxlength="27" data-required="true" /></div>
     </div>
    <div class="form_line"><div class="form-label" style="display: inline-block;block;margin-top:12px !important;"><span style="font-size:14px !important;">BIC <span class="ast_orange">*</span></label></div>
        <div style="display: inline-block;"><input class="form-input" type="text" name="code_etabl" value="{userInfoCode_etabl:h}" minlength="8" maxlength="8" data-required="true" /></div>
    </div>

             </td>
           </tr>
          
        </tbody>
        
    </table>

    <div class="form_left">
        
      
        <!--<div class="form_line"><label>NAF </label>
        <input type="text" name="naf" value="{userInfoNaf:h}" size="40"  maxlength="5"/>
        </div>-->
        
        <div class="spacer"></div>
       
        
    </div>
    <h3 style="margin-bottom: 5px">Vos disponibilités <span class="ast_orange">*</span></h3>
    <div class="spacer"></div>
    <div class="form_niaina">               
               <div style="display: inline-block;"> 
                <div>
               <div style="display: inline-block; margin-right: 5px">
                    <div style="display: inline-block;"><span style="font-size:14px !important;">&nbsp;Matin</label>
                    </div>
                    <input type="checkbox" style="display: inline-block;height:auto" name="buyer_info5" class="input_radio" value="oui"/>
                </div>

                <div style="display: inline-block; margin-right: 5px">
                    <div style="display: inline-block;"><span style="font-size:14px !important;">&nbsp;Après-midi</div>
                    <input type="checkbox" style="display: inline-block;height:auto" name="buyer_info6" class="input_radio" value="oui" />
                </div>
               <div style="display: inline-block; margin-right: 5px">
                <div style="display: inline-block;"><label>&nbsp;Nuit</label></div>
                <input type="checkbox" style="display: inline-block;height:auto" name="buyer_info7" class="input_radio" value="oui" /></div>
               <div style="display: inline-block; margin-right: 5px">
                <div style="display: inline-block;"><label>&nbsp;Samedi</label></div> 
                <input type="checkbox" style="display: inline-block;height:auto" name="buyer_info8" class="input_radio" value="oui" /></div>
               <div style="display: inline-block; margin-right: 5px">
                <div style="display: inline-block;"><label>&nbsp;Dimanche</label></div>
                <input type="checkbox" style="display: inline-block;height:auto" name="buyer_info9" class="input_radio" value="oui" /></div>
                </div>
               </div>
    
        
     <div class="spacer" style="height:10px;"></div>
    <p class="txt_oblg">Tous les champs marqu&eacute;s d'un <span class="ast_orange">*</span> sont &agrave; remplir obligatoirement</p>
    <div class="spacer"></div><br/>
    <div class="spacer"></div>
   
    <p class="txt_oblg">Selon la loi "Informatique et libert&eacute;s" du 05/01/76, vous disposez d'un droit d'acc&egrave;s, de rectification et de suppression des donn&eacute;es qui vous 
    concernent.</p>
  
    
    <input type="submit" style="background: none repeat scroll 0% 0% rgb(243, 153, 3); color: rgb(255, 255, 255); margin: -30px 0px 0px; font-size: 15px; float: right; line-height: 27px;margin-right:3%" name="Register" value="{registerSubmitValue:h}" class="btn_send_register" />
    <input type="hidden" name="idsource" value="{param_admin_default_buyer_source}" />
    </form>
	
	<br><br>
    </div>
{end:}
</div><!--
<flexy:include src="b_right.tpl"></flexy:include>-->
</div>


<script type="text/javascript">
function hidelogin () {
$('#loginpanel').animate({top:-120}, {queue:false, easing:'easeOutCirc', duration:300 });
}

    $('#mylogin').click(function(e) {
    e.stopPropagation();
    $('#loginpanel').animate({top:0}, {queue:false, easing:'easeOutCirc', duration:200});
    });
    $("body").click(function(e) {
    if (e.target.id == "loginpanel" || $(e.target).parents("#loginpanel").size()) e.stopPropagation();
    else hidelogin();
    if (e.target.id == "menu" || $(e.target).parents("#menu").size()) e.stopPropagation();
    else menureset();
    });
    $("form#login #email, form#login #password").textPlaceholder();
    $("form#search #search-text").textPlaceholder();
    $("form#newsletter #emailnews").textPlaceholder(); 
    
    $('#icons_compare').mouseenter(function(e) {
    $(this).find('div').stop().toggle('fast');
    })
    $('#icons_compare').mouseleave(function(e) {
    $(this).find('div').stop().toggle('fast');
    })
    $('#icons_consult').mouseenter(function(e) {
    $(this).find('div').stop().toggle('fast');
    })
    $('#icons_consult').mouseleave(function(e) {
    $(this).find('div').stop().toggle('fast');
    }) 
</script>

<!--====================================== Fin du contenu ======================================-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>
<script src="{start_url}/templates/catalog/js/jquery-validate.js" type="text/javascript"></script>
<script type="text/javascript">
jQuery(document).ready(function() {
    // validate the comment form when it is submitted
    jQuery("#login").validate({
        onSubmit : true,
        eachInvalidField : function(){
            //jQuery(this).css('border-color: #f00');
            alert('Merci de renseigner une adresse email valide');
        },
        eachValidField : function(){
            jQuery(this).css('border-color: #e0e0e0');
        }
    });
    jQuery.validateExtend({
        rlogin : {
            required : true,
            pattern : /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
        }
    });
});
</script>

<!-- Pied de page -->
{if:orderInProgress}
<flexy:include src="foot.tpl"></flexy:include>
{else:}
<flexy:include src="foot.tpl"></flexy:include>
{end:}