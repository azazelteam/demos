<flexy:include src="/head/header.tpl"></flexy:include>
<!--<flexy:include src="menu/menu_alt.tpl"></flexy:include>-->
<!--====================================== Contenu ======================================-->           
      <script type="text/javascript">	
			function checkMailForm(){				 
				if(document.forms["MailForm"].elements['dest'].options[document.forms["MailForm"].elements['dest'].selectedIndex].value == 0 ){
					alert( "Vous devez s&eacute;lectionner un destinataire !" );
					return false;
				}				
				if(document.forms["MailForm"].elements["lastname"].value == ''){
					alert( "Vous devez saisir votre nom !" );
					return false;
				}				
				if(document.forms["MailForm"].elements["firstname"].value == ''){
					alert( "Vous devez saisir votre pr&eacute;nom !" );
					return false;
				}				
				if(document.forms["MailForm"].elements["company"].value == ''){
					alert( "Vous devez saisir votre soci&eacute;t&eacute; !" );
					return false;
				}				
				if(document.forms["MailForm"].elements["adress"].value == ''){
					alert( "Vous devez saisir votre adresse !" );
					return false;
				}				
				if(document.forms["MailForm"].elements["zipcode"].value == ''){
					alert( "Vous devez saisir votre code postal !" );
					return false;
				}				
				if(document.forms["MailForm"].elements["city"].value == ''){
					alert( "Vous devez saisir votre ville !" );
					return false;
				}
				if(document.forms["MailForm"].elements["phonenumber"].value == ''){
					alert( "Vous devez saisir votre n&deg; de t&eacute;l&eacute;phone !" );
					return false;
				}												
				if(document.forms["MailForm"].elements["email"].value == ''){
					alert( "Vous devez saisir votre adresse email !" );
					return false;
				}else{
					var i;		
					isok=1;					
					emailAddr = document.forms["MailForm"].elements["email"].value;						
					i = emailAddr.indexOf("@");
					if (i == -1) {
						isok=0;
					}				
					var username = emailAddr.substring(0, i);
					var domain = emailAddr.substring(i + 1, emailAddr.length);
					i = 0;
					while ((username.substring(i, i + 1) == " ") && (i < username.length)) {
						i++;
					}
					if (i > 0) {
						username = username.substring(i, username.length);
					}			
					i = domain.length - 1;
					while ((domain.substring(i, i + 1) == " ") && (i >= 0)) {
						i--;
					}					
					if (i < (domain.length - 1)) {
						domain = domain.substring(0, i + 1);
					}				
					if ((username == "") || (domain == "")) {
						isok=0;
					}
					var ch;
					for (i = 0; i < username.length; i++) {
						ch = (username.substring(i, i + 1)).toLowerCase();
						if (!(((ch >= "a") && (ch <= "z")) || 
							((ch >= "0") && (ch <= "9")) ||
							(ch == "_") || (ch == "-") || (ch == "."))) {
							isok=0;
						}
					}
					for (i = 0; i < domain.length; i++) {
						ch = (domain.substring(i, i + 1)).toLowerCase();
						if (!(((ch >= "a") && (ch <= "z")) || 
							((ch >= "0") && (ch <= "9")) ||
							(ch == "_") || (ch == "-") || (ch == "."))) {
								isok=0;
						}
					}
					if(isok==0) {
						alert('Le format de l\'adresse email n\'est pas valide !');
						return false;
					}
				}				
				if(document.forms["MailForm"].elements["message"].value == ''){
					alert( "Vous devez saisir votre message !" );
					return false;
				}
				return true;				
			}
		</script> 
   	  <div id="center" class="page contact">
	 {if:iduser}<span style="color:red;font-weight: bold;">/contact.tpl</span>{end:}
	  
  	{if:successfullMail}
<div style="width:500px;margin-left:30px;">
			<br /><br /><h1 class="title_devis_contact"> Votre message a bien &eacute;t&eacute; envoy&eacute; &agrave; : {recipientName}</h1><br /><br />
			<br />Mail destinataire : <a class="mailto" href="mailto:{recipient}">{recipient}</a>
			<br /><br />
			{recipientName} ne tardera pas &agrave; vous r&eacute;pondre.
			<br /><br />
			Nous vous souhaitons une excellente journ&eacute;e.
			</p>
			<p class="Center"><input class="btn_send" type="button" value="Retour" onclick="javascript:document.location='{start_url}/catalog/contact.php';" />
</div>
{else:}
			{if:mandatoryError}
			<p style="color: #FF0000; font-weight: bold; text-align: center;">Tous les champs obligatoires n'ont pas &eacute;t&eacute; renseign&eacute;s</p>
			{end:}
			{if:sendError}
			<p style="color: #FF0000; font-weight: bold; text-align: center;">Erreur lors du traitement du mail</p>
			{end:}			   	    	  
   	  <br/><h3>Contact</h3>
	<hr class="yellowRule" />	
	<table>
		<tr>
			<td class="tdPres" >
				<table class="SubTableContact" >
				<tr>
					<td class="tdContact">
					<br />
						<ul class="contactList">
							<li>
							<strong>Service Commercial ...</strong><br />
							<table class="BorderedPhone">
								<tr>
									<td>T&eacute;l. :</td>
									<td>...</td>
								</tr>
                                <tr>	
									<td>Fax :</td>
									<td>...</td>
								</tr>
                                <tr>	
									<td>Courriel :</td>
									<td><a href="mailto:...">...</a></td>
								</tr>
							</table>
							<br />
							</li>
						</ul>
					</td>
				</tr>
                <tr>
					<td>
					<span class="foot_adress">
						<b>...</b><br />
							...<br />
							T&eacute;l. : ... - Fax : ...<br />
							N&deg; TVA intracommunautaire : ...<br />
					</span>
					</td>
                  </tr>
				</table>							
			</td>
		</tr>
	</table>
    <br style="clear:both;" />    
    <p>Nous vous informons que ce site est r&eacute;serv&eacute; uniquement aux <strong>professionnels</strong> (administration, entreprises, associations...)</p>  	     	    	     	 
  	<br style="clear:both;" />
		<div id="MailFormDiv" >   	     	  
   	  	<h1 class="title_devis_contact"> Formulaire contact</h1>	
		<form id="MailForm" action="contact.php?email" method="post" enctype="multipart/form-data" onsubmit="return checkMailForm();">		
		{if:mandatoryError}
			<p class="error">Tous les champs obligatoires n'ont pas &eacute;t&eacute; renseign&eacute;s!</p>
		{end:}		
		{if:sendError}
			<p class="error">Erreur lors du traitement du mail!</p>
		{end:}
<div class="form_left">
            <label>Titre <span class="asterix">*</span></label>
		<select name="titre" class="cells" style="height: auto;">
			<option value="Mr">Mr</option>
			<option value="Mme">Mme</option>
			<option value="Mlle">Mlle</option>
		</select>
		<div class="spacer"></div>
            <label>Nom <span class="asterix">*</span> </label>
		<input type="text" name="lastname" value="" />
		<div class="spacer"></div>
            <label>Pr&eacute;nom</label>
		<input type="text" name="firstname" value="" /> 
		<div class="spacer"> </div>
            <label>Email <span class="asterix">*</span></label>
		<input type="text" name="email" value="" /> 
		<div class="spacer"></div>
            <label>T&eacute;l. </label>
		<input type="text" name="phonenumber" value="" /> 
		<div class="spacer"></div>
            <label>Fax</label>
		<input type="text" name="faxnumber" value="" />
		<div class="spacer"></div>
            <label>Raison sociale</label>
		<input type="text" name="company" value="" />
		<div class="spacer"></div>
            <label>Sujet</label>
		<input type="text" name="subject" value="" />
		<div class="spacer"></div>
</div>
<table style="clear:both" class="BorderedTable">
    <tr>
    <th style="width: 90px; font-weight: normal; font-size: 11px; color: rgb(111, 111, 111);text-align:right;vertical-align:top;">Message <span class="asterix">*</span></th>
    <td class="mepChamps">
    <textarea rows="6" cols="37" name="message" style="height: 80px; border: 1px solid;"></textarea>
    </td>
    </tr>
</table>
<span class="asterix">* : Champs obligatoires</span>
<div>
<input type="submit" name="send" class="btn_send" value="Envoyez" />
</div>
    <input type="hidden" id="recipient" name="recipient" value="Nom_societe" />
    <input type="hidden" id="dest" name="dest" value=".....@mail.fr" />
</form>
</div>
   	  {end:}
	<a href="javascript:history.go(-1)" class="btn-return">Retour à la page précédente</a>
 </div>  	  
<!--====================================== Fin du contenu ======================================-->
</div>
</div>

<flexy:include src="foot.tpl"></flexy:include>            