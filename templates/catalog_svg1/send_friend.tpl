<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>Envoi a un ami</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="fr" />
	<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/general.css" />
	<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/specific.css" />
	
</head>
<style type="text/css">
<!--
* {
	margin: 0;
	padding: 0;	
}
body {
	color: #414141;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 11px;
	background: url({start_url}/www/_images/0_email/bg.png) bottom left no-repeat;
}
.title_devis_produit { color:#f5a900; font-size:14px; background:url(../templates/catalog/images/icons-devis.jpg) no-repeat 0 6px; height:22px; padding:7px 0 0 30px; border-bottom:1px dotted #cccccc; margin:10px 0 5px 0; float:left; width:500px; }
.title_devis_contact { color:#f5a900; font-size:14px; background:url(../templates/catalog/images/icons-contact.jpg) no-repeat 0 6px; height:22px; padding:7px 0 0 30px; border-bottom:1px dotted #cccccc; margin:10px 0 5px 0; float:left; width:500px; }

h1 {
	margin: 0 0 15px 0;
}
h2 {
	color: #f4a117;
	font-size: 11px;
	font-weight: bold;
	margin: 0 40px 15px 30px;
}
p {
	margin: 8px 40px 0 30px;
	text-align: justify;
}
a {
	color: #6f6f6f;
	text-decoration: underline;
}
a:hover {
	color: #f4a117;
	text-decoration: none;	
}
img {
	border: 0;
}
.adresse {
	display: block;
	width: 95%;
	font-size: 10px;
	border-bottom: 1px dotted #c6c6c6;
	margin: 70px 0 0 20px;
	padding: 0 0 15px 0;
	color: #6f6e6e;
}
.adresse strong {
	color: #f4a117;
	font-size: 11px;
}
.form_left { width:265px; float:left; }
.form_right { width:265px; float:right; }

.form_left label, .form_right label { width:90px; float:left; color:#6f6f6f; padding:0 5px 0 0; font-size:11px; text-align:right; margin:0 0 8px 0; }
.asterix { color:#f5a900; }
.form_left input, .form_right input { width:164px; float:left; color:#6f6f6f; font-size:11px; margin:0 0 8px 0;  background:url(../images/background-input-devis.jpg) no-repeat right bottom; height:13px; border:1px solid #6f6f6f; }
.form_left select, .form_right select { width:80px; float:left; color:#6f6f6f; font-size:11px; margin:0 0 8px 0;  background:url(../images/background-input-devis.jpg) no-repeat right bottom; height:18px; border:1px solid #6f6f6f; }
.btn_send { color:#ffffff; font-size:11px; background:url(../templates/images/background-submit-valid-devis.jpg) no-repeat; width:100px; height:30px; float:right; cursor:pointer; margin:0 0 0 0; padding:0 10px;  }

-->
</style>
<body>
{if:iduser}<span style="color:red;font-weight: bold;">/send_friend.tpl</span>{end:}
	<form name="send_friend" method="POST" action="/catalog/send_friend.php">
		<input type="hidden" name="IdProduct" value="{IdProduct}" />
		<div style="width:500px;margin-left:30px;">
			<h1 class="title_devis_contact"> Vos informations</h1>			
			<div class="form_left">			
				<label>Nom&nbsp;<span class="asterix">*</span></label>
				<input style="float:left;" type="text" name="firstnameSender" value="{buyer[lastname]}"  maxlength="20" />
				<div class="spacer"></div>

				<label>Prénom&nbsp;<span class="asterix">*</span> </label>
				<input style="float:left;" type="text" name="lastnameSender" value="{buyer[company]}" maxlength="20" />
				<div class="spacer"></div>

				<label>E-Mail&nbsp;<span class="asterix">*</span></label>
				<input style="float:left;" type="text" name="emailSender" value="{buyer[email]}" maxlength="50" />
				<div class="spacer"></div>
			</div>
		</div>
	
		<div style="width:500px;margin-left:30px;">
			<h1 class="title_devis_contact"> Ses informations</h1>
			
			<div class="form_left">			
				<label>Nom&nbsp;<span class="asterix">*</span></label>
				<input style="float:left;" type="text" name="firstnameReciever" value=""  maxlength="20" />
				<div class="spacer"></div>

				<label>Prénom&nbsp;<span class="asterix">*</span> </label>
				<input style="float:left;" type="text" name="lastnameReciever" value="" maxlength="20" />
				<div class="spacer"></div>

				<label>E-Mail&nbsp;<span class="asterix">*</span></label>
				<input style="float:left;" type="text" name="emailReciever" value="" maxlength="50" />
				<div class="spacer"></div>
			</div>
		</div>
        <div style="margin-left: 30px; clear: both; text-align: center; width: 500px; float: left;"><input type="submit" value="Envoyer" style="margin-left: 100px;"></div>
	
	</form>
</body>
</html>
