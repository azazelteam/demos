<flexy:include src="/head/header.tpl"></flexy:include><!--
<flexy:include src="b_left.tpl"></flexy:include>-->
<!--flexy:include src="menu/menu.tpl"></flexy:include-->

<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/sse.css" />

<!--====================================== Contenu ======================================-->
<div id="center" class="page">    		
{if:iduser}<span style="color:red;font-weight: bold;">/promotions.tpl</span>{end:}
	<!-- début div centre -->
	<!--<h1 class="CategoryName category-intro__title">Promotions</h1>-->
	<h1 class="sections_promotions">Promotions</h1>
	<p style="clear:both" id="categoryTitle">
			 {foreach:descriptions,desc}			 
			 	{desc:h}
			 {end:}
	</p>
    <div class="spacer"></div><br />
<div class="prod-list">
    <ul class="prod-list__wrapper row">		
    {foreach:articles,article}

    <div id="CartAddConfirmation_{article[idArticle]}" style="display: none;">
                                <div class="borderConfirmDevis">
                                    <div class="bgContentDevis">
                                        <div class="contentConfirmCommande">
                                            <div class="spacer"></div>
                                            <br>
                                            <div class="panierConfirmDevis">
                                                <span class="h3">Vous venez d'ajouter &agrave; votre panier :</span>
                                              <div class="spacer"></div>
                                                <table>
                                                     <tr>
                                                        <td class="img"><img src="{article[imageSrc]}" onClick="$('#CartAddConfirmation_{article[idArticle]}').dialog('destroy');" width="70" alt="{article[name]}"></td>
                                                        <td class="txt">{article[name]}</td>
                                                    </tr>
                                                </table>
                                                <a href="#" onClick="$( '#CartAddConfirmation_{article[idArticle]}' ).dialog( 'destroy' ); return false;" class="btn-med grey">Poursuivre ma visite</a>
                                                <a href="{start_url}/catalog/basket.php" class="btn-med grey" >Acc&eacute;der &agrave; mon panier</a>
                                                <div class="spacer"></div>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
        
                                <div class="spacer"></div>
                            </div>
                        </div>

    <li class="prod-item col-lg-3 col-md-4 col-sm-6" itemscope="" itemtype="http://schema.org/Product">
                <div class="product-item">
                    
                    <a class="product-item__info" href="{article[getURL]}" itemprop="url">
                        <span class="ruban promo"><span>-{article[getDiscountRate]:h}</span></span>
                        <img itemprop="image" class="product-item__img" src="{article[imageSrc]}" alt="{article[imageSrc]:e}">

                        <h4 class="cat-item__title" itemprop="name">{article[name]:e}</h4>

                        <!-- <span class="small product_reference" style="text-align: center;">Réf. :
                            {article[getReference]}</span> -->
                        <div class="price" itemprop="offers" itemscope="" itemtype="http://schema.org/Offer">
                            <!-- <small>à partir de</small><br> -->
                            {if:article[getDiscountPriceATI]}
                                {if:article[getDiscountRate]}
                                <del>{article[getCeilingPriceATI]:h} <small>TTC</small></del><br>
                                {end:}
                                <span class="strong">{article[getDiscountPriceATI]:h}<small> TTC</small></span>
                                {end:}
                           

                            <a title="Ajouter au panier" class="btn-det" href="#" onclick="addToCart( {article[idArticle]}, 1, 'order' );" >
                               <span>Ajout panier</span>
                            </a>
                        </div>

                    </a>

                </div>
            </li>
    {end:}
 </ul>
 </div>
    <div class="spacer"></div><br />
  					
<!-- fin div centre -->
</div>		<!--
<flexy:include src="b_right.tpl"></flexy:include> -->  

 
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
