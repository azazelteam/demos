<!--====================================== Baseline ======================================-->

<div class="clear"></div>

<!--====================================== Cat&eacute;gories ======================================-->
<div id="footer">
	{if:iduser}<span style="color:red;font-weight: bold;">/foot.tpl</span>{end:}

	<section id="offres" class="offres">
		<div class=" container-fluid">
			<div class="row">
				<div class="offres-list col-md-3">
					<div class="offres-ico">
						<img src="{start_url}/templates/catalog/images/page-home/livraison.png" alt="livraison">
					</div>
					<div class="offres-desc">
						<a href="{start_url}/pages/infos-livraison.php">
							<p>livraison sous 48h ou 72h</p>
						</a>
					</div>
				</div>

				<div class="offres-list col-md-3">
					<div class="offres-ico">
						<img src="{start_url}/templates/catalog/images/page-home/service.png" alt="services">
					</div>
					<div class="offres-desc">
						<a href="{start_url}/pages/nos-services-et-garanties.php">
							<p>services &amp; garanties</p>
						</a>
					</div>
				</div>

				<div class="offres-list col-md-3">
					<div class="offres-ico">
						<img src="{start_url}/templates/catalog/images/page-home/cadeau.png" alt="cadeaux">
					</div>
					<div class="offres-desc">
						
							<p>cadeau surprise</p>
						
					</div>
				</div>

				<div class="offres-list col-md-3">
					<div class="offres-ico">
						<img src="{start_url}/templates/catalog/images/page-home/paiement.png" alt="paiement securisé">
					</div>
					<div class="offres-desc">
						<a href="{start_url}/pages/paiements-securises.php">
							<p>paiement sécurisé</p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<footer class="footer">
		<div class="container-fluid bottom_border footer-top">
			<div class="row mb-2">
				<div class="footer-company col-md-3">
					<img src="{start_url}/templates/catalog/images/logo-cce-transparent.png" alt="CCE : Idées cadeaux, objets et accessoires maison">

					<p class="mb10">
						<strong>
							Implantée au cœur de l'Europe dans la zone économique de Strasbourg,
							la S.A Centre Commercial Européen est une société de vente par
							correspondance créée en 1987.
						</strong>
					</p>

					
				</div>
				<div class="footer-contact col-md-3">
					<h5 class="footer-title">contact</h5>

					<p><i class="fas fa-phone"></i> 03 90 290 290</p>
					<p>
						<i class="far fa-envelope"></i> <a href="mailto:contact@cce.eu">contact@cce.eu</a>
					</p>
					<p>
						<i class="far fa-clock"></i>
						De 8h à 12h et de 13h à 17h
					</p>
					<!--<ul class="social-network social-circle">
						<li>
							<a href="#" class="icoFacebook" title="Facebook"><i class="fab fa-facebook-f"></i></a>
						</li>
						<li>
							<a href="#" class="icoTwitter" title="Twitter"><i class="fab fa-twitter"></i></a>
						</li>
					</ul>-->

					<!-- footer_ul_amrc ends here -->
				</div>
				<div class="footer-cat col-md-2">
					<h5 class="footer-title">categories</h5>
					<a href="{start_url}/Les-Trouvailles-c-9">
						<p>Les Trouvailles</p>
					</a>
					<a href="{start_url}/Les-Petits-Gourmets-c-5">
						<p>Les petits gourmets</p>
					</a>
					<a href="{start_url}/Confort-Sante-c-7">
						<p>Confort &amp; Santé</p>
					</a>
					<a href="#"><p>Composition des produits </p></a>
				</div>
				<div class="footer-compte col-md-2">
					<h5 class="footer-title">informations</h5>
					<a href="#">
						<p>Mon compte</p>
					</a>
					
					
					<a href="{start_url}/pages/mentions-legales.php">
						<p>Mentions légales</p>
					</a>
					<a href="{start_url}/pages/faq.php"><p>FAQ</p></a>
					
						
					<a href="{start_url}/pages/conditions-generales-de-vente.php"><p>Conditions générales de vente</p> </a>
							
					
					
				</div>
				<div class="footer-fevad col-md-2">
					<img src="{start_url}/templates/catalog/images/page-home/fevad.png" alt="fevad">
					<p>Charte qualité du consommateur</p>
				</div>
			</div>
			<!-- row -->
		</div>

		<!--<div class="footer_lien row">
			<ul class="col-md-12">
				<li><a class="nav-link" href="{start_url}/pages/qui-sommes-nous.php">Qui sommes-nous ? </a></li>
				<li><a class="nav-link" href="{start_url}/pages/infos-livraison.php">Info Livraison </a></li>
				<li><a class="nav-link" href="{start_url}/pages/nos-services-et-garanties.php">Services &amp; Garanties </a></li>
				<li><a class="nav-link" href="{start_url}/pages/paiements-securises.php">Paiement Sécurisé </a></li>
			</ul>
		</div>-->

		<!-- container fluid -->
		<div class="container-fluid">
			<div class="row">
				<div class="copyright col-md-12">
					<p>
						Copyright © 2018 <strong>CCE</strong>. Réalisé avec la solution
						<img src="{start_url}/templates/catalog/images/akilae-logo.png" alt="Akilaé saas" />
						<a href="https://www.akilae-saas.fr/" target="_blank" rel="noopener">
							Akilaé.</a></p>
					<!--<nav class="copyright-links nav">
						<ul>
							<li><a class="nav-link" href="{start_url}/pages/faq.php">FAQ</a></li>
							<li><a class="nav-link" href="{start_url}/pages/mentions-legales.php">Mentions légales </a></li>
							<li>
								<a class="nav-link" href="{start_url}/pages/conditions-generales-de-vente.php">Conditions générales de vente </a>
							</li>
							<li><a class="nav-link" href="{start_url}/pages/composition_produits.php">Composition des produits </a></li>
						
						</ul>
					</nav>-->
				</div>
				<!-- copyright -->
			</div>
		</div>
		<!-- container -->
	</footer>
</div>



<flexy:include src="google_analytics.tpl" async></flexy:include>

<script src="{start_url}/js/85528.js" type="text/javascript" async></script>
<script type="text/javascript">
	/*$('select#search-marque ~ .sbHolder').find('.sbOptions')
	.attr('style', 'width: 118px !important')
	.hide();*/
	// $('#mylogin').click(function (e) {
	// 	e.stopPropagation();
	// 	$('#loginpanel').animate({
	// 		top: 0
	// 	}, {
	// 		queue: false,
	// 		easing: 'easeOutCirc',
	// 		duration: 200
	// 	});
	// 	if ($('#header').hasClass('opened')) {
	// 		$('#header + #menu #mainFrontMenu').fadeOut();
	// 		$('#header #SearchForm').fadeOut();
	// 	}
	// });
	// $("body").click(function (e) {
	// 	if (e.target.id == "loginpanel" || $(e.target).parents("#loginpanel").size()) e.stopPropagation();
	// 	else hidelogin();
	// 	if (e.target.id == "menu" || $(e.target).parents("#menu").size()) e.stopPropagation();
	// 	else menureset();
	// });
	$("form#login #email, form#login #password").textPlaceholder();
	$("form#search #search-text").textPlaceholder();
	$("form#newsletter #emailnews").textPlaceholder();
	$('#hometext .accordion').accordion({
		active: false,
		heightStyle: 'content',
		collapsible: true,
		animate: 'easeInOutCirc'
	});


	// function hidelogin() {
	// 	$('#loginpanel').animate({
	// 		top: -250
	// 	}, {
	// 		queue: false,
	// 		easing: 'easeOutCirc',
	// 		duration: 300
	// 	});
	// 	if ($('#header').hasClass('opened')) {
	// 		$('#header + #menu #mainFrontMenu').fadeIn();
	// 		$('#header #SearchForm').fadeIn();
	// 	}
	// }

	$('#icons_compare').mouseenter(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_compare').mouseleave(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseenter(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
	$('#icons_consult').mouseleave(function (e) {
		$(this).find('div').stop().toggle('fast');
	})
</script>
<script type="text/javascript">
	function verifLogin() {
		var login = document.forms.RegisterForm.elements['Login'].value;
		var password = document.forms.RegisterForm.elements['Password'].value;
		if (login == '') {
			alert('Le champ Identifiant est vide !!!');
			return false;
		}
		if (password == '') {
			alert('Le champ Mot de passe est vide !!!');
			return false;
		}
		return true;
	}

	function verifCreateAccount() {
		var data = $('#CreateAccount').formSerialize();
		$.ajax({
			url: "/catalog/account.php?createLoginAjax",
			async: true,
			type: "POST",
			data: data,
			error: function (XMLHttpRequest, textStatus, errorThrown) {},
			success: function (responseText) {
				if (responseText != '') {
					$("#returnCreateAccount").html(responseText);
				} else {
					document.getElementById("CreateAccount").submit();
				}
			}
		});
	}
	//Mot de passe oublié
	function forgotPassword() {
		$("#forgotPassword").dialog({
			title: "Mot de passe oublié ?",
			bgiframe: true,
			modal: true,
			width: 550,
			minHeight: 10,
			closeText: "Fermer",
			/*overlay: {
			backgroundColor: '#000000',
			opacity: 0.5
			},*/
			close: function (event, ui) {
				$("#forgotPassword").dialog("destroy");
			}
		});
	}
	//Contrôle de l'email saisie pour le mot de passe oublié
	function sendPassword(Email, divResponse) {
		data = "&Email=" + Email;
		$.ajax({
			url: "/catalog/forgetmail.php",
			async: true,
			type: "POST",
			data: data,
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				alert("Impossible de récupérer l'email");
			},
			success: function (responseText) {
				$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>" + responseText + "</p>");
			}
		});
	}
	// Fonction de sélection des informations newsletter
	function newsletter() {
		test = window.open(start_url + '/catalog/newsletter.php', 'hw',
			'width=380,height=450,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1')
	}

	function menureset() {
		$('#menu > ul > li > a')
			.stop()
			.animate({
				color: '#fff',
				backgroundColor: 'transparent'
			}, 100, 'easeOutCirc')
			.removeClass('nosub');
		$('#menu').find('.submenu')
			.css('marginTop', '-5px')
			.css('display', 'none');
	}

	(function () {

		var mobile = document.createElement('div');
		mobile.className = 'nav-mobile';
		document.querySelector('#menu').appendChild(mobile);

		function hasClass(elem, className) {
			return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
		}

		function toggleClass(elem, className) {
			var newClass = ' ' + elem.className.replace(/[\t\r\n]/g, ' ') + ' ';
			if (hasClass(elem, className)) {
				while (newClass.indexOf(' ' + className + ' ') >= 0) {
					newClass = newClass.replace(' ' + className + ' ', ' ');
				}
				elem.className = newClass.replace(/^\s+|\s+$/g, '');
			} else {
				elem.className += ' ' + className;
			}
		}

		var mobileNav = document.querySelector('.nav-mobile');
		var header = document.querySelector('#header');
		var largeur_fenetre = $(window).width();
		if (largeur_fenetre <= 999) {
			$('#mainFrontMenu > li').each(function (index) {
				$(this).click(function (e) {
					if (this.className == 'clicked') {
						this.className = '';
					} else {
						$('#mainFrontMenu > li').each(function (index) {
							this.className = '';
						});
						this.className = 'clicked';
						e.preventDefault();
					}
				});
			});
		}

		mobileNav.onclick = function (e) {
			toggleClass(this, 'nav-mobile-open');
			toggleClass(header, 'opened showMe');
			$('#menu > #mainFrontMenu').addClass('showMe');
		};
		$('div.category_description p').filter(function () {
			return this.innerHTML
				.replace("&nbsp;", " ")
				.trim() ==
				""
		}).remove();

	})();
</script>
<!-- Code Google de la balise de remarketing -->
{if:pagename}
{scriptRemarketing:h}
{else:}
{if:head}
{head.getScriptRemarketing():h}
{end:}
{end:}

<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = ......;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type="text/javascript" src="{start_url}/js/conversion.js">
</script>
<noscript>
	<div style="display:inline;">
		<img height="1" alt="google ads" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/........../?value=0&amp;guid=ON&amp;script=0" />
	</div>
</noscript>


<!-- Slider à plusieurs colonnes -->
<!-- <script type="text/javascript" href="{start_url}/templates/catalog/js/slick.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script> -->
<script type="text/javascript" src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery.slick/1.3.11/slick.min.js"></script>
<!-- <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script> -->

<script type="text/javascript">
	// Header mon compte login panel
	$('#mylogin').click(function (e) {
		$('#loginpanel').toggleClass("open");
		e.stopPropagation();
	});
	$("#monpanier").click(function (e) {
		e.stopPropagation();
		$("#panierMenuContainer").toggleClass("open");
	});
	$("body").click(function (e) {
		$('#loginpanel').removeClass("open");
		$("#panierMenuContainer").removeClass("open");
	});
	$('#loginpanel').click(function(e){
		e.stopPropagation();
	});
	$('#panierMenuContainer').click(function(e){
		e.stopPropagation();
	});


	// Les slider (Home & Fiche produit)
	$(document).ready(function () {
		$('#promo-slider').slick({
			dots: true,
			infinite: true,
			speed: 700,
			autoplay: false,
			arrows: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 520,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});

		$('#product-simi-slider').slick({
			dots: true,
			infinite: true,
			speed: 700,
			autoplay: false,
			arrows: true,
			slidesToShow: 4,
			slidesToScroll: 4,
			responsive: [
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 3,
						infinite: true,
						dots: true
					}
				},
				{
					breakpoint: 767,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 520,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});

	});
</script>

</body>

</html>