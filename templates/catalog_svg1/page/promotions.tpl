<flexy:include src="/head/header.tpl"></flexy:include><!--
<flexy:include src="b_left.tpl"></flexy:include>-->
<flexy:include src="menu/menu.tpl"></flexy:include>

<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/sse.css" />

<!--====================================== Contenu ======================================-->
<div id="center" class="page">    		
{if:iduser}<span style="color:red;font-weight: bold;">/promotions.tpl</span>{end:}
	<!-- début div centre -->
	<h1 class="title_promo">Promotions</h1>
	<p style="clear:both" id="categoryTitle">
			 {foreach:descriptions,desc}			 
			 	{desc:h}
			 {end:}
	</p>
    <div class="spacer"></div><br />
<div class="products">
    <ul class="products_poromotion list_product">		
    {foreach:promotions,line}
    <li itemscope="" itemtype="http://schema.org/Product">    
                                                    <a title="Ajouter au panier" class="btn-buy" href="{line[productURL]}" style="left: -44px;"><img width="60" alt="Ajouter au panier" height="60" src="/templates/catalog/images/list-btn-buy.png"></a>
                        <a href="{line[productURL]}" itemprop="url">
                            <span class="ruban promo"></span>                                <img itemprop="image" width="170" class="product_image" height="170" src="{line[thumbURL]}" alt="{line[name]:e}">
                            <span class="small product_reference" style="text-align: center;" >Réf. : {line[soldReference]}</span>
                            <span itemprop="offers" itemscope="" itemtype="http://schema.org/Offer" class="price">
                                <small style="font-size:0.8em">à partir de</small><br>
                                <span itemprop="price" class="strong">{line[pxmin]:h} <small>HT</small></span>
                                <br>
                                  <span class="strong"><del>{line[oldPrice]} &euro;<small>HT</small></del></span>
                            </span>
                        </a>
    <h2 style="font-size:0.85em" itemprop="name"><a class="product-name" href="{line[productURL]}">{line[name]:e}</a></h2>	
        </li>
    {end:}
 </ul>
 </div>
    <div class="spacer"></div><br />
    <p>* hors frais de port.</p>		
					
<!-- fin div centre -->
</div>		<!--
<flexy:include src="b_right.tpl"></flexy:include> -->  
</div>
</div>
 
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
