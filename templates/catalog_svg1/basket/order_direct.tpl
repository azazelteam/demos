<flexy:include src="/head/header.tpl"></flexy:include>
<flexy:include src="menu/menu.tpl"></flexy:include>

<!--====================================== Contenu ======================================-->
<style type="text/css">

	#contents{
		margin-bottom: 40px;	
	}

</style>
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/order_direct.tpl</span>{end:}
<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/products.css" />

<h3 style="margin-left: 10px;font-size:18px; color:#f1a801; font-style:italic" >Commande directe</h3>

<br />

<br />
<p style="margin-left:10px">Saisissez les références et quantités des produits
que vous souhaitez commander</p>
<br />
<!-- Formulaire pour la commande express de produits -->
<form name="rechref" action="{start_url}/catalog/order_direct.php" method="post">

<table cellspacing="3" class="BorderedTable" width="250px" style="margin-left:10px">
<tr>
<th>Qté</th>
 <th>Référence</th>
</tr>

{foreach:articles,article}
<tr>
<td>
<input type="texte" style="width:100px;" size="3" name="qtt_{article[i]}" value="{article[quantity]}" >
 </td>
<td> 
<input type='text' name='ref_{article[i]}' value="{article[reference]}" >
</td>
</tr>
{end:}
</table>

<br /><br />

<input type="hidden" name="validated" value="1" >
<input type="submit" name="AddToBasket" value="Valider" border="0" style="margin-right:85px;" >

<br /><br />
</form>

</br>
</br>

{if:refs_not_found}
<br><br>
<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
<th>Qté</th>
 <th>Référence</td>
</tr>
{foreach:list_articles_not_found,article}
<tr>
<td>{article[quantity]}</td>
<td>{article[reference]}</td>
</tr>
{end:}
</table>

</br></br>

<font color=black size=-1 face=arial><b>
Les articles ci-dessus n'ont pas été trouvées ou la quantité est fausse!
</br>
Merci de vérifier la référence et la quantité.

</b></font>
<br><br>
{end:}

{if:hasrefwoprice}

<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
 <th>Référence</td>
</tr>
{foreach:list_refs_hide_prices,ref}
<tr>
<td>{ref}</td>
</tr>
{end:}
</table>
		
<br><br>
		<p style="color:#000000; font-weight:bold">
		Le prix des références ci-dessus n'est pas disponible pour le moment
		</p>
		<p>
		Pour plus d'informations concernant ces références, <a href="mailto:{ad_mail}" style="text-decoration:underline;">contactez-nous par email</a> ou au <font style="font-weight:bold; color:#FF0000;">{ad_tel}</font>
		</p>
{end:}



</div> <!-- fin div center -->
</div>
</div>
<!--====================================== Fin du contenu ======================================-->
<!-- Colonne de droite de la page 
<flexy:include src="b_right.tpl"></flexy:include>      -->      
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
            


