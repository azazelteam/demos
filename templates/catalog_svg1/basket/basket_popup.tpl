<div class="border_top_basket"></div>
<div class="spacer"></div>
{if:basket.getItemCount()}
{if:iduser}<span style="color:red;font-weight: bold;">/basket/basket_popup.tpl</span>{end:}
	<table width="100%" border="0" cellpadding="2" cellspacing="0">
	{foreach:basketItems,item}
	<tr>
		<td class="borderB"><img src="{item.getIcon():h}" width="40" /></td>
		<td class="nameP borderB">{item.get(#summary#):h}</td>
		<td class="priceP borderB" align="right">{item.getTotalET():h}</td>
	</tr>
	{end:}
	</table>
{else:}
	<table width="100%" border="0" cellpadding="2" cellspacing="0">
	<tr><td colspan="3" style="text-align:center; font-size:11px; font-weight:bold;"><br/>Votre panier est vide<br/><br/></td></tr>
	</table>
{end:}
