<flexy:include src="header.tpl"></flexy:include>
<flexy:include src="b_left.tpl"></flexy:include>
<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/basket.css" />

<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/buying.tpl</span>{end:}
	<ul class="basket_fil">
    <li class="un">Panier commande</li>
    <li class="deux">Authentification</li>
    <li class="trois actif">Livraison et facturation</li>
    <li class="quatre">Confirmation</li>
    </ul>		
    <div class="spacer"></div>

    {if:isLogin}		
        <form action="{start_url}/catalog/order.php" method="post" onsubmit="javascript:return verifForm(this) && checkDifferentAddresses( this )">
                            
        <flexy:include src="addresses_edit.tpl"></flexy:include>
        <div class="spacer"></div>			

        <input type="hidden" name="DocType" value="0">
        <!-- Référence de commande <input type="text" name="n_order" value="" size="50"  maxlength="15" class="AuthInput"/> -->
        
        <!-- COMMENTAIRE (facultatif)
        <textarea name="Comment" cols="60" rows="3" style="border:solid 1px;"></textarea> -->
        <div class="spacer"></div><br />

        
        <p class="txt_oblg" style="color:#CCCCCC;">Selon la loi "informatique et libertés" du 05/01/76, vous disposez d'un droit d'accès, de rectification et de 
        suppression des données qui vous concernent.</p>
                    
        <input type="submit" name="Order" value="Poursuivre la commande" class="btn_send" />
        
        </form>
            
    {end:}					
</div>
<flexy:include src="b_right.tpl"></flexy:include>

<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
            


