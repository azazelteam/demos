<flexy:include src="head/header.tpl"></flexy:include>
    <!--<flexy:include src="b_left.tpl"></flexy:include>-->
    <flexy:include src="menu/menu.tpl"></flexy:include>
    <flexy:toJavascript maVariableJs="maVariableFlexy"></flexy:toJavascript>
    <!-- <span style="display: none;">{anana}</span> -->
    <flexy:toJavascript start_url="start_url"></flexy:toJavascript>
    <script style="text/javascript" language="javascript">
		<!--
		
		function updact(){
			document.forms.frmcde.action = 'estimate_basket.php?next=1';
		}
		function btncata(){
		document.forms.frmcde.action = 'estimate_basket.php';
		document.forms.frmcde.submit(); 
		}
		function LoginPop()
		{
			postop = (self.screen.height-170)/2;
			posleft = (self.screen.width-310)/2;
			window.open(start_url+'/catalog/popup_login.php', '_blank', 'top=' + postop + ',left=' + posleft + ',width=310,height=170,resizable,scrollbars=yes,status=0');
		}
		
		function confsupp(ref,id)
		{
			if (confirm( 'Confirmer la suppression de '+ref )){
				//document.forms.frmcde.action = 'estimate_basket.php?Delete_'+id+'=1&next=1';
		        //document.forms.frmcde.submit(); 
				
				//$('#Item' + 0).fadeOut('slow',function(){document.location='estimate_basket.php?delete&index='+id;});
				
				var data = "delete&index="+id;
				
				$.ajax({
					url: "/catalog/basket.php",
					async: true,
					type: "GET",
					data: data,
					error : function( XMLHttpRequest, textStatus, errorThrown ){ },
					success: function( responseText ){
						$('#Item' + id).fadeOut('slow');
					}
				});
			}
            return false;
		}	
		
		function verifForm() {
			
			if( document.frmcde.lastname.value == '') {
				alert('Le champ nom est vide !');
				return false;
			}
			
			if( document.frmcde.email.value == '') {
				alert('Le champ email est vide !');
				return false;
			}
			
			adresse = document.frmcde.email.value;
			var place = adresse.indexOf("@",1);
			var point = adresse.indexOf(".",place+1);
			
			if ( (adresse.length < 5) || (place < 1) || (point < 1) ) {
				alert('L\'adresse e-mail n\'est pas valide !');
				return false;
			}
			
			if( document.frmcde.phonenumber.value == '') {
				alert('Le champ téléphone est vide !');
				return false;
			}
			
			if( document.frmcde.zipcode.value == "") {
				alert('Le champ code postal est vide !');
				return false;
			}else{
				if(isNaN(document.frmcde.zipcode.value)){
					alert('Le code postal doit être un nombre !');
					return false;
				}else{
					if(document.frmcde.zipcode.value.length!=5){
						alert('Le code postal doit comporter 5 chiffres !');
						return false;	
					}
				}
			}
			
			document.forms.frmcde.action = start_url + '/catalog/' + buyingscript;
			
			return true;
			
		}
		//-->	
    </script>
    <link rel="stylesheet" type="text/css" href="{start_url}/templates/catalog/css/mail_product.css" />
    <link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/estimate.css" />

	<!--====================================== Contenu ======================================-->
   

	<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/estimate.tpl</span>{end:}
    	<h1 class="title_devis">Mon panier devis</h1>
        
        <table cellspacing="0" cellpadding="3" width="100%" border="0" class="tableBasket_articles">
        {foreach:basketItems,item}
		<tr id="Item{item.getIndex()}">
			<td class="tdProduct">
				<a href="{item.article.getURL()}" class="Estimate_prod_href" title="Aller à la fiche produit"><b>{item.get(#summary_1#):h}</b></a><br/>
                <a href="{item.article.getURL()}" class="Estimate_ref_href" title="Aller à la fiche produit">Réf. : {item.get(#reference#):h}</a>
			</td>
            <td class="tdQtt">
				<input type="text" onchange="updact();" size="2" name="{item.getIndex()}" value="{item.getQuantity()}"  style="text-align:center;font-weight:bold; height:15px"/>
			</td>
			<td class="tdDel">
				<input type="image" style="width:20px; height:20px; border:none;" border="0" src="{start_url}/templates/catalog/images/delete.png" align="middle" name="Delete_{item.getIndex()}" onclick="confsupp('{item.get(#reference#):h}', '{item.getIndex()}'); return false;" />
			</td>
		</tr>
        {end:}
        </table>
        
        <br>
         <h1>1 seule étape pour s'enregistrer</h1>
        <br>
        
        <p>Nos promesses :<br>
        - aucun engagement de votre part<br>
        - devis gratuit sous 24h<br>
        - informations non cédées à nos partenaires ou autre organisme<br>
        <br>
        </p>
        <p>Nous vous informons que ce site est réservé uniquement aux <strong>professionnels</strong> (administration, entreprises, associations...)</p>

        <h1 class="title_devis_contact">Veuillez remplir le formulaire ci-dessous</h1>
        
        {if:get_m}
		<p style="text-align:center; font-weight:normal; font-size:12px; color:#FF0000; margin-bottom:10px;">{get_m}</p>
		{end:}
        
        <form action="{start_url}/catalog/estimate_complete.php" method="post" name="frmcde">
        <!-- début de modification  -->
        <div class="form_left">
            <label>{formtranslations[title]}&nbsp;<span class="asterix">Civilité *</span></label>
            {titlelist:h}
	     <div class="spacer"></div>

	     <label>{formtranslations[lastname]}&nbsp;<span class="asterix">Nom *</span></label>
            <input type="text" name="lastname" value="{buyer[lastname]}" maxlength="50" />
            <div class="spacer"></div>
            
            <label>{formtranslations[email]}&nbsp;<span class="asterix">E-mail *</span></label>
            <input type="text" name="email" value="{buyer[email]}" maxlength="50" />
            <div class="spacer"></div>
            
            <label>{formtranslations[phonenumber]:e}&nbsp;<span class="asterix">Téléphone *</span></label>
            <input type="text" name="phonenumber" value="{buyer[phonenumber]}" maxlength="20" />
            <div class="spacer"></div>
            
            <label>{formtranslations[zipcode]}&nbsp;<span class="asterix">Code postal *</span></label>
            <input type="text" name="zipcode" value="{buyer[zipcode]}" maxlength="20" />
            <div class="spacer"></div>
               
            
            <div class="spacer"></div>
            
            <label>{formtranslations[company]}&nbsp;<span class="asterix">Société</span></label>
    		<input type="text" name="company" value="{buyer[company]}" maxlength="50" />
			<div class="spacer"></div>
            
            <label>{formtranslations[faxnumber]}&nbsp;<span class="asterix">Fax</span></label>
            <input type="text" name="faxnumber" value="{buyer[faxnumber]}" maxlength="20" />
            <div class="spacer"></div>
            
            <label>{formtranslations[city]}&nbsp;<span class="asterix">Ville</span></label>
            <input type="text" name="city" value="{buyer[city]}" maxlength="20" />
            <div class="spacer"></div>
        </div> 
        <!-- fin de modification  -->
        <div class="spacer"></div>
        
        <div class="form_center">       
            <label>Commentaire (facultatif) :</label>
            <textarea class="areaCom" name="message" onblur="if( this.innerHTML == '' ) this.innerHTML = 'Saisissez votre message ici...';" onclick="if( this.innerHTML == 'Saisissez votre message ici...' ) this.innerHTML = '';">{if:message}{message}{end:}</textarea>
		</div>
        <div class="spacer"></div>		
        
        <p class="pObligatoire2">Les champs notés d'un <span class="asterix">*</span> sont obligatoires</p>
        <div class="spacer"></div>	
        <input type="submit" name="Buy" onclick="return verifForm();" border="0" value="Envoyez-moi le devis" class="btn_send" /></td>
        <!--<input type="submit" name="CatBack" onclick="updact()" border="0" value="Poursuivre la visite" class="btn_continue" />-->
        <table style="table-layout: fixed;">
        	<tr>
            	<td><a href="javascript:history.back();" class="btn_continue">Poursuivre la visite</a></td><td><a class="btn_continue" href="account.php">Mon compte</a></td>
        </table>
        
		<!-- <input type="image" name="GoToCata" border="0" src="{start_url}/templates/catalog/images/add_products_off.gif" onmouseover="this.src='{start_url}/templates/catalog/images/add_products_on.gif'" onmouseout="this.src='{start_url}/templates/catalog/images/add_products_off.gif'" onclick="updact()" style="background:white;" border="0"/> -->
		
        </form>

        {if:commercial_set}
            <p class="pCenter"><a href="#" onclick="validateSelection(); return false;"><img src="{start_url}/templates/catalog/images/btn_back_to_adm_com.jpg" style="border-style:none;" /></a></p>
        {end:}

	</div><!--

	<flexy:include src="b_right.tpl"></flexy:include>-->
</div>
</div>

  
<!--====================================== Fin du contenu ======================================-->
            
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>