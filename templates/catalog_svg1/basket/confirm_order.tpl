<flexy:include src="header.tpl"></flexy:include>
<!--<flexy:include src="menu/menu_alt.tpl"></flexy:include>

<flexy:include src="b_left.tpl"></flexy:include>-->

<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/basket/confirm_order.tpl</span>{end:}
<div style="margin-top: 20px; position: absolute; margin-left :670px; _margin-left: 135px;"></div>
<h3>Votre commande a bien été enregistrée</h3>
<br/><br/><br/>
<table >
    <tr>
      <td  valign="top" >Merci de retenir le numéro de votre commande : <span class="ndevis_confirm"><b>{idorder}</b></span>
      <br /><br />
      Nous vous remercions de la confiance que vous témoignez à notre société.<br /><br />
      Votre commande est actuellement traitée par notre service commercial.<br /><br />
      Nous vous enverrons votre confirmation définitive par e-mail dans les plus brefs délais.
      </td>
      <td >&nbsp;</td>
      <td ><img src="{start_url}/templates/catalog/images/compte.gif" /><br>
       - Au suivi et à l'historique de vos commandes et devis
		<br /><br />- A votre catalogue personnalisé de produits
		<br /><br />- A la commande expresse
 		<br /><br />- A toutes les offres exclusivement réservées aux membres de Abisco
      <br />
      <div ><a href="{start_url}/catalog/account.php"><img src="{start_url}/templates/catalog/images/acces_compte.gif" border="0"/></a></div>
      </td>
    </tr>
	<tr>
		<td colspan="3" ><br /><br />&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3"> 
		<span>Récapitulatif de votre commande :</span>
		</td>
	</tr>
	<tr>
		<td colspan="3" class="txt_confirm">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="3">		
  		<!-- articles -->  			
  		<flexy:include src="articles_read.tpl"></flexy:include>		 			
  			
    	<br/>
    	
    	<!-- Montants de la commande -->  			
  		<flexy:include src="basket_amounts.tpl"></flexy:include>    	   	
    	
    	</td>
	</tr>
	<tr>
	<td colspan="3"><br />&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2">
			<flexy:include src="addresses_read.tpl"></flexy:include>
		<td>
		</tr>
	<table />
<tr>
				<td>
				</br></br></br></br>
				Mode de paiement utilisé : <b>{mode_payment_used}</b>				
				</td>
			</tr>
			
						</table>
	</td>
	</tr>
	</table>
	</td>
	</tr>
	<tr>
	<td colspan="3" ><br /><br/>&nbsp;</td>
	</tr>
	<tr>
	<td colspan="3" ><span class="infocomp_gris">Pour toutes informations complémentaires, </span><span class="infocomp_orange">n'hésitez pas à nous contacter</span></td>
	</tr>
	<tr>
	<td colspan="3">&nbsp;</td>
	</tr>
	<tr>
	<td colspan="3" >&nbsp;</td>
	</tr>
	<tr>
	<td colspan="2" ><a href="javascript:window.print();"><img src="{start_url}/templates/catalog/images/btn_imprimer.gif" border="0"/></a></td>
	<td style="text-align:right;"><a href="{start_url}/index.php"><img src="{start_url}/templates/catalog/images/btn_poursuivre_droit.gif" border="0" /></a></td>
	</tr>
</table>


</div>
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include>       


