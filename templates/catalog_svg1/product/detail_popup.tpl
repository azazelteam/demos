
{if:iduser}<span style="color:red;font-weight: bold;">/product/detail_popup.tpl</span>{end:}
<!-- Création du tableau des couleurs en JS -->
{setColorsJsData:h}

<!-- Création des tableau des références en JS -->
{setReferencesJsData:h}

<script type="text/javascript">
<!--

	var formParameters = new Array();
	
	function isNumber( str ){
		
		var validChars = "0123456789.";
		var isNumber = true;
		var char;
		
		for( i = 0; i < str.length && isNumber; i++ ){ 
			   
			char = str.charAt( i ); 
			if( validChars.indexOf( char ) == -1 ) 
			isNumber = false;
		
		}
			   
		return isNumber;
	   
	}
   
   	function checkSelection( idtable ){
   	
   		var headingValues = document.getElementById( 'HeadingValues_' + idtable );
   		
   		if( headingValues.selectedIndex == - 1 || headingValues.options[ headingValues.selectedIndex ].value == '' ){
   		
   			alert( 'Vous devez d\'abord s\351lectionner une r\351f\351rence' );

   			return false;
   			
   		}

   	}
   	
	function checkQuantity( form ){
	
		var min_cde = form.elements[ 'min_cde' ].value;
		var quantity = form.elements[ 'quantity' ].value;
		
		if( !isNumber( quantity ) || quantity < min_cde ){
		
			alert( 'La quantit\351 minimale pour cette r\351f\351rence est de ' + min_cde );
			form.elements[ 'quantity' ].value = min_cde;
			
			return false;
			
		}
		
		return true;
		
	}
	

	function selectArticle( idarticle, tableId ){
		
		// Mise à jour des champs si idarticle

		if(idarticle){
			document.getElementById("myCaract").innerHTML = articles[ idarticle ][ 'caract' ] ;
						
			if( articles[ idarticle ][ 'r' ] > '0' ){
				
				document.getElementById("pxTarrif").innerHTML = articles[ idarticle ][ 'prixtotal' ] +" \u20AC" ;
				document.getElementById("promoRate").innerHTML = articles[ idarticle ][ 'r' ] +" %" ;
				document.getElementById("myDelai").innerHTML = articles[ idarticle ][ 'delay' ] ;
				document.getElementById("htPrice").innerHTML = articles[ idarticle ][ 'prixtotalremise' ] +" \u20AC";

				document.getElementById("min").style.display = "block";
				document.getElementById("txtPromo").style.display = "block";
				document.getElementById("txtPTarrif").style.display = "block";
				document.getElementById("pxTarrif").style.display = "block";
				document.getElementById("pxTarrif").style.textDecoration = "line-through" ;
				document.getElementById("promoRate").style.display = "block";
				
				document.getElementById("AddSD").style.display = "block" ;
				document.getElementById("AddCde").style.display = "block" ;
				
			}else{
				
				if( articles[ idarticle ][ 'hidecost' ] == 0 && articles[ idarticle ][ 'prixtotal' ]!="0.00"){
					document.getElementById("min").style.display = "block";
					document.getElementById("htPrice").innerHTML = articles[ idarticle ][ 'prixtotal' ] +" \u20AC" ;
					document.getElementById("myDelai").innerHTML = articles[ idarticle ][ 'delay' ] ;
					
					document.getElementById("AddSD").style.display = "block" ;
					document.getElementById("AddCde").style.display = "block" ;
					
				}else{
				
					document.getElementById("htPrice").innerHTML = '<a href="http://localhost/catalog/mail_product.php?idproduct='+articles[ idarticle ][ 'IdProduct' ]+'" class="contactUs" >Contactez-nous</a> ' ;
					document.getElementById("myDelai").innerHTML = articles[ idarticle ][ 'delay' ] ;				
					document.getElementById("AddSD").style.display = "none" ;
					document.getElementById("AddCde").style.display = "none" ;
					document.getElementById("AddLD").style.display = "block" ;
	
				}
			
			}
			
			
					
			var formNames = new Array( 'OrderForm' );
			
			var j = 0;
			while( j < formNames.length ){
			
				var formName = formNames[ j ] + '_' + tableId;
				
				var form = document.getElementById( formName );
				
				if( idarticle == '' ){
				
					form.onsubmit = function(){
					
						alert( 'Vous devez d\'abord sélectionner une référence' );
						return false;
						
					}
	
					return;
					
				}
			
				form.onsubmit = function(){ return true; };
			
				form.elements[ 'quantity' ].value = articles[ idarticle ][ 'min_cde' ];
				form.elements[ 'IdArticle' ].value = idarticle;
				
				var elements = form.getElementsByTagName( 'INPUT' );
			
				var i = 0;
				while( i < elements.length ){
				
					var element = elements[ i ];
					
					if( element.getAttribute( 'type' ) == 'hidden' ){
					
						var name = element.name;
						
						if( articles[ idarticle ][ name ] != null )
							element.value = articles[ idarticle ][ name ];
	
					}
	
					i++;
					
				}
			
				j++;
			
			}
			
			
					
			var idcolor = getSelectedColor( tableId );
			
			if( idcolor )
				selectColor( idcolor, tableId );
		}
	}
	
	function getSelectedColor( tableId ){
	
		if( document.forms.ColorForm == null )
			return 0;
			
		var colors = document.forms.ColorForm.elements[ 'ColorRadio_' + tableId ];
		
		var i = 0;
		while( i < colors.length ){
		
			if( colors[ i ].checked )
				return colors[ i ].value;
				
			i++;
			
		}
		
		return 0;
		
	}
	
	function selectColor( idcolor, tableId ){
	
		if( !idcolor )
			return;
		
		var headingValues = document.getElementById( 'HeadingValues_' + tableId );
		
		if( headingValues.selectedIndex == -1 )
			return;
			
		var idarticle = headingValues.options[ headingValues.selectedIndex ].value;
		
		if( idarticle == '' )
			return;
				
		var formNames = new Array( 'OrderForm', 'EstimateForm' );
		
		var j = 0;
		while( j < formNames.length ){
		
			var formName = formNames[ j ] + '_' + tableId;
			
			var form = document.getElementById( formName );
			
			form.elements[ 'idcolor' ].value = idcolor;
			form.elements[ 'reftotal' ].value = articles[ idarticle ][ 'reference' ] + '-' + productColors[ idcolor ][ 'code_color' ];
			form.elements[ 'caract' ].value = articles[ idarticle ][ 'caract' ] + '<br />Couleur : ' + productColors[ idcolor ][ 'code_color' ];
			form.elements[ 'myDelai' ].value = articles[ idarticle ][ 'delay' ];
			form.elements[ 'option_price' ].value = productColors[ idcolor ][ 'price' ];
			form.elements[ 'prixtotal' ].value = parseFloat( productColors[ idcolor ][ 'price' ] ) + parseFloat( articles[ idarticle ][ 'prixtotal' ] );
			form.elements[ 'prixtotalremise' ].value = form.elements[ 'prixtotal' ].value;
			
			j++;
			
		}
		
	}

// -->
</script>

{foreach:ArticlesByTables,tableId,Articles}
	<div class="selection">
	<div id="selectionHeader">
	<h2 class="sousTitre"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="›" /> 
	S&eacute;lectionnez une r&eacute;f&eacute;rence pour afficher le prix et les d&eacute;tails.</h2>
	
	
	{if:hasPromo}
	
		<!-- Module de selection -->
		<label for="HeadingValues_{tableId}" class="label_product">Référence :</label>
		<select id="HeadingValues_{tableId}" onchange="selectArticle( this.options[ this.selectedIndex ].value, {tableId} );" class="select_product">
			<option value="">Votre sélection</option>
			{foreach:Headings,Heading}
				<optgroup label="{Heading[heading]:h}">
					{foreach:Heading[headingValues],headingValue}
						<option value="{headingValue[idarticle]}">{headingValue[headingValue]:h}</option>
					{end:}				
				</optgroup>
			{end:}
		</select>
		<div class="spacer"></div>
	</div>

		{if:countProductColors}
			<label for="ColorForm" class="label_couleur">Couleur :</label>
			<form name="ColorForm" id="ColorForm" action="{start_url}/catalog/product_one.php" method="get">
				{foreach:productColors,color}
					<div class="prod_img"><input checked="{color[check]}" type="radio" id="ColorRadio_{tableId}" name="Couleurs[]" onclick="selectColor( {color[idcolor]}, {tableId} ); this.form.submit()" class="ProductColorRadioButton" value="{color[idcolor]}"/>&nbsp;<img class="ProductColorImage" src="{color[image]}"  />
					&nbsp;&nbsp;	
					<!-- onmouseover="drcx('{color[code_color]}','{color[textColor]}');" onmouseout="nd();" -->				
					{color[hiddenFields]:h}</div>
				{end:}
			<input type="hidden" name="displayedDetailTable" value="{tableId}" />
			</form>
			<div class="spacer"></div>
		{end:}
		
		
		<p class="description" id="myCaract"><strong></strong></p>
		<p class="description" id="myDelai"><strong></strong></p>
		
		<p class="price_supp">
			<span class="min" id="txtPTarrif" style="display: none;">Prix unit. HT</span>
			<del><span class="txtOrderEstimate" id="pxTarrif"></span></del>
		</p>
		<p class="remise">
			<span class="min" id="txtPromo" style="display:none;">Remise</span>
			<span id="promoRate"></span>
		</p>
		<p class="price">
			<span class="min" id="min" style="display:none;">Prix remisé HT</span>
			<span class="htPrice" id="htPrice"></span>
		</p>
		<div class="spacer"></div>

	{else:}
		
		<!-- Module de selection -->
		<label for="reference" class="label_product">R&eacute;f&eacute;rence :</label>
		<select id="HeadingValues_{tableId}" onchange="selectArticle( this.options[ this.selectedIndex ].value, {tableId} );" class="select_product">
			<option value="">Votre s&eacute;lection</option>
			{foreach:Headings,Heading}
				<optgroup label="{Heading[heading]:h}">
					{foreach:Heading[headingValues],headingValue}
						<option value="{headingValue[idarticle]}">{headingValue[headingValue]:h}</option>
					{end:}				
				</optgroup>
			{end:}
		</select>
		<div class="spacer"></div>		
		</div>
		<p class="description" id="myCaract"><strong></strong></p>
		<p class="description" id="myDelai"><strong></strong></p>
		
		{if:countProductColors}
			<label for="ColorForm2" class="label_couleur">Couleur :</label>
						<form name="ColorForm2" id="ColorForm" action="{start_url}/catalog/product_one.php" method="get">
							{foreach:productColors,color}
								<input flexy:ignore="{color[checked]}" type="radio" id="ColorRadio_{tableId}" flexy:ignore="name='Couleurs[]'" onclick="selectColor( {color[idcolor]}, {tableId} ); this.form.submit()" class="ProductColorRadioButton" value="{color[idcolor]}" />
								<img class="ProductColorImage" src="{color[image]}" width="15" height="15"  />
								&nbsp;&nbsp;
								<!-- onmouseover="drcx( '{color[code_color]}', '{color[textColor]}' );" onmouseout="nd();" -->
								{color[hiddenFields]:h}
							{end:}
						<input type="hidden" flexy:ignore="name='displayedDetailTable'" value="{tableId}" />
						</form>
			<div class="spacer"></div>
		{end:}
		
		<p class="price">
			<span class="min" id="min" style="display:none;">Prix unit. HT</span>
			<span class="htPrice" id="htPrice"> </span>
		</p>
		<div class="spacer"></div>

	{end:}
	</div>
	

	<form name="OrderForm_{tableId}" id="OrderForm_{tableId}" method="post" action="{start_url}/catalog/basket.php" onsubmit="return checkSelection( {tableId} ) ; return checkOrderQuantity( this );">
	<p class="pButton">	
		<input type="hidden" name="idarticle" value="" />
		<input type="hidden" name="quantity" value="" />
		<input type="hidden" name="min_cde" value="" />
		<input type="hidden" name="idcolor" value="" />
		<input type="hidden" name="idtissus" value="" />
		<input type="hidden" name="category" value="{appart_catalog}" />
		{if:showCde}
			<input type="submit" onclick="document.OrderForm_{tableId}.action='{start_url}/catalog/estimate_basket.php';" name='AddSD' id='AddSD' value="Demande 
de devis" class="input_devis" /> 
			<input type="submit" name="AddCde" id="AddCde" class="input_commander" value="Commander" />
			<a href="#" class="input_contact" onclick="window.open('{start_url}/catalog/mail_product.php?idproduct={idproduct}','mailto','width=640,height=600,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');">
				Budget estimatif express
			</a>
		{else:}
			<input type="submit" onclick="document.OrderForm_{tableId}.action='{start_url}/catalog/estimate_basket.php';" name='AddLD' id='AddLD' value="Demande 
de devis" class="input_devis_big" />
			<a href="#" class="input_contact_big" onclick="window.open('{start_url}/catalog/mail_product.php?idproduct={idproduct}','mailto','width=640,height=600,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');">
				Budget estimatif express
			</a>
		{end:}
	</p></form>
	


{end:}	