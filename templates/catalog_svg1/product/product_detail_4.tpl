	<!-- d&eacute;tail produit pour les articles en g&eacute;n&eacute;ral -->
			<flexy:include src="/product/product_detail_header.tpl"></flexy:include>
			<!-- Fin Header fiche produit -->		
			<div id="descr">
			{if:iduser}<span style="color:red;font-weight: bold;">/product/product_detail_4.tpl</span>{end:}
		        	<h1 itemprop="name">{product.get(#name_1#):h}</h1>
									
					<!--{if:product.get(#name_secondary_1#)}
					 <h2>{product.get(#name_secondary_1#):h}</h2>                 
					{end:}-->
					
			        <small id="product_reference">{product.getReferenceCount()} r&eacute;f&eacute;rence(s) disponible(s)</small>
			        <div id="video-container" style="display:none">
						{if:product.getYVideo()}
							{product.getYVideo():h}
							<script type="text/javascript">
								$(document).ready(function(){
									$('#button-video-show').click(function(){
										$('#video-container').fadeIn('slow');
										var div = document.getElementById("video-container");
										var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
										iframe.postMessage('{"event":"command","func":"playVideo","args":""}', '*');
									});
									$('span.video_close').click(function(){
										$('#video-container').fadeOut('slow');
										var div = document.getElementById("video-container");
										var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
										iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
									});
									$('div#contents').css('position', 'static');
									$('#video-container').click(function(){
										$(this).fadeOut('slow');
										var div = document.getElementById("video-container");
										var iframe = div.getElementsByTagName("iframe")[0].contentWindow;
										iframe.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
									});
								});
								
							</script>
						{end:}
					</div>
                    {if:!product.getYVideo()}
					<ul id="services_guaranteed" style="width:130px">
                    {else:}
					<ul id="services_guaranteed">
					{end:}
						{if:!product.getYVideo()}
						<li style="background:none!important;">
						{else:}
						<li>
						{end:}
							{if:product.getBrandInfos()}
			<li flexy:foreach="product.getBrandInfos(),brand">
				{if:brand.filename}<a href="/{brand.filename:h}.php">{end:}
				<img height="50" src="/www/{brand.logo:h}" alt="{brand.alt:e}" title="{brand.alt:e}" />
				{if:brand.filename}</a>{end:}
			</li>
			{end:}
						</li>
                        {if:product.getYVideo()}
						<li style="background: none!important;">
							<a id="button-video-show">
								<span class="icon"></span>
								<span class="label"><span>Voir<br>la vidéo</span></span>
							</a>
						</li>
						{end:}
                    </ul>	            
                <br style="clear:both;">
                <div id="short_description_block" itemprop="description">
					{if:product.getTruncatedDescription(#550#)}
						{product.getTruncatedDescription(#550#):h}
					{else:}
						{product.get(#description_1#):h}
					{end:}

					{if:product.getTruncatedDescription(#550#)}
						<a href="#descriptif" style="display: block; margin: 20px 0;" class="full-description">Voir le descriptif complet</a>
					{end:}
					
				</div>
				
                <ul class="service">
<li>
	<!-- PDF -->			
	<a onClick="" href="{product.getPDFURL()}" rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_pdf.png"></a>
</li>
<li>
	<!-- Imprimer -->			
	<a  onclick="window.open(this.href); return false;" href="{product.getPDFURL()}" rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_imprimer.png"></a>
</li>
<li>		
	<!-- Envoyer par email -->					
	<a  style="cursor:pointer;" href="{baseURL}/catalog/send_friend.php?IdProduct={product.getId()}" onClick="window.open(this.href,'mailto','width=555,height=600,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=1'); return false;" rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_send_mail.png"></a>
</li>

{if:authentified}
	<li class="last">	
		<!-- liste personnelle -->			
		<a  href="{baseURL}/catalog/buyer_func.php?IdProduct?{product.getId()}" onClick="javascript:alert('Le produit a &eacute;t&eacute; ajout&eacute; à votre catalogue personnel.'); " rel="nofollow"><img src="{start_url}/templates/catalog/images/icon_addtolist.png" style="margin:0;border:none;"></a>			
	</li>
{end:}
</ul>
<br style="clear:both;">
<p>Documents :</p>
            <ul id="documentation" flexy:foreach="product.getDocuments(),doc">
		<li><img src="{baseURL}/templates/catalog/images/pdf_icon.gif" alt="pdf" class="pdf" /><a rel="notfollow" class="printProduct" id="doc" href="{doc.urldoc}"  title="Télécharger document">{doc.name}</a></li>
</ul>
				<br style="clear:both;">
				<br style="clear:both;">

				<div id="actions" itemprop="offers" itemscope itemtype="http://schema.org/Offer">

			
			{if:product.getLowerPriceET()}
				<link itemprop="itemCondition" href="http://schema.org/NewCondition">
				<meta itemprop="availability" content="http://schema.org/InStock"><input type="hidden" id="lowerprice" value="{product.getLowerPriceET()}" />
				<div class="htttc htttc-{article.getId():h} infos_prix2" flexy:foreach="product.getArticles(),article" data-price="{article.getPriceET()}">
					<span class="price">
					   <small> &agrave; partir de</small>
						{if:article.getDiscountRate()}
							<del>{article.getCeilingPriceET():h} <small>HT</small></del>
						{end:}
						<strong class="priceHT">{article.getPriceET():h} <small>HT</small></strong>
						<meta itemprop="price" content="{article.getPriceET():h}">
					</span><br />
					<span class="price" style="font-size:9px">
						{if:product.getPromotionCount()}
							<del>{article.getPriceATI():h}</del>
						{end:}
						<strong>
							{if:article.getDiscountPriceATI()}
								{article.getDiscountPriceATI():h}
							{else:}
								{article.getPriceATI():h}
							{end:} 
							<small>TTC</small>
						</strong>
					</span><br/>
				</div>
			{end:}
			
<div id="detail8" class="sidepanel" style="float:left;">
					<flexy:include src="/product/detail_8.tpl"></flexy:include>
				</div>
            </div>

			</div>

<script type="text/javascript">$(function(){
												$('#containerEstimate').tabs({ fxFade: true, fxSpeed: 'fast' });
								});
                                          </script>      
<div class="clear" style="clear:both"></div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		jQuery('a.full-description').click(function(){
			jQuery( "#containerEstimate ul li.item2 > a" ).trigger( "click" );
		});
	});
</script>
<div id="containerEstimate">
      <ul>            
            <li><a href="#caracteristique">{product.get(#name_1#):h}</a></li>
            {if:product.getRelatedProducts()}
            <li><a href="#options">Options</li>{end:}
            <!-- début de modification -->
            <li><a href="#product_compl">Produits compl&eacute;mentaires</a></li>
            <li><a href="#product_similaires">Produits similaires</a></li>
            <!-- fin de modification -->
            {if:product.getTruncatedDescription(#550#)}
				<li><a href="#descriptif">Description</a></li>
			{end:}
           <!--  -->
            {if:product.getDataSheetURI()}<li class="item item4"><a href="#plans"><img src="{start_url}/templates/catalog/images/btn_transparent.gif" alt="Plans" style="margin:0;border:none;" width="66"  height="39"></a></li>{end:}
      </ul>
      <div id="caracteristique">
        <div id="ProductOneDetailDiv" style="clear: both;">
        	{head.addCSS(#/templates/catalog/css/details.css#)}	       
               <flexy:include src="/product/detail_2.tpl"></flexy:include>
        </div>
     </div>
     <!-- début de modification -->
     <div id="product_compl">
            <div flexy:if="product.getComplements()" class="pdt_complementaire">
                <h3 class="crosssell"><span><strong>Produits compl&eacute;mentaires<strong> {product.getProductCategory(2):e}</strong></span></h3>
                <div class="products">
                                    <ul class="list_product">
                                        <li itemscope itemprop="isRelatedTo" itemtype="http://schema.org/Product" flexy:foreach="product.getComplements(),product">
                                            <a title="Ajouter au panier" class="btn-buy"  href="{product.getURL()}" style="left: -44px;"><img width="60" alt="Ajouter au panier" height="60" src="{starturl}/templates/catalog/images/list-btn-buy.png" /></a>
                                            <a href="{product.getURL()}" itemprop="url" >
                                            	<span class="ruban promo" flexy:if="product.getPromotionCount()"></span>
                                                <img  itemprop="image" width="170" class="product_image" height="170" src="{product.getImageURI(#170#)}" alt="{product.get(#name_1#):h}" />
                                                <span class="small product_reference" ><span class="strong" >{product.getReferenceCount()}</span> r&eacute;f&eacute;rence(s)</span>
                                                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer" class="price">
                                                    <small style="font-size:0.8em">&agrave; partir de</small><br />
                                                    <span itemprop="price" class="strong">{product.getLowerPriceET():h} <small>HT</small></span>
                                                    <br />
                                                      <span class="strong smallttc">{product.getLowerPriceATI():h}<small>TTC</small></span>
                                                    
                                                </span>
                                            </a>
                <h4 style="font-size:0.85em" itemprop="name"><a  class="product-name" href="{product.getURL()}">{product.get(#name_1#):h}</a></h4>
                                        </li>                        
                					</ul>
                				</div>
            </div>
     </div>
     <div id="product_similaires">
            <aside flexy:if="product.getSimilares()">
            	<div id="complementary">
            		<h2>Produits similaires</h2>
            		<ul>
            			<li flexy:foreach="product.getSimilares(),similar">
            				
                            <table>
                            	<tr>
                            <td style="vertical-align:top;">
            					<img src="{similar.getImageURI(#65#)}" width="60" height="60" style="margin-left:-10px;" alt="{similar.get(#name_1#):h}" title="{similar.get(#name_1#):h}" />
            				</td>
                            <td style="vertical-align:top;">
                                <div>
            						<a href="{similar.getURL()}" title="{similar.get(#name_1#):h}" style="text-decoration:none;"><h3>{similar.get(#name_1#):h}</h3></a>
            						{if:similar.getLowerPriceET()}
            							<p><span>&agrave; partir de</span><br/>{similar.getLowerPriceET():h}</p>
            						{else:}
            							<p>&nbsp;</p>
            						{end:}
            					</div>
            				</td>
            			</tr>
                            </table>
            			</li>			
            		</ul>
            	</div>
            </aside>
     </div>
     <!-- fin de modification -->
     <div id="options">
        {if:product.getRelatedProducts()}
		<div class="floatleft blocProductBig">
		<div class="spacer"></div>		
		<div class="blocProductBGBig">	
			<h2 class="title_specification_small">Options, accessoires : {product.get(#name_1#):h}</h2>
			<div class="spacer"></div>		
			<div class="specification_small">
{if:product.getRelatedProducts()}
<div class="addAccessoires">
<span flexy:foreach="product.getRelatedProducts(),relatedProduct">
<div class="accessoires" flexy:foreach="relatedProduct.getArticles(),relatedArticle" style="margin-bottom:9px;">
<table width="600">
	<tr>
		<td width="100">
			<img src="{relatedArticle.getImageURI()}" alt="{relatedProduct.get(#name_1#):h}" style="width:42px;">
		</td><td width="400">
			<strong>{relatedProduct.get(#name_1#):h}</strong>
			<br>
			<em>R&eacute;f.:&nbsp;{relatedArticle.get(#reference#)}</em><br>
			{if:relatedProduct.get(#designation_1#)}
			{relatedProduct.get(#designation_1#)}<br>
			{end:}
			{if:relatedProduct.getLowerPriceET()}
			<strong class="red">Prix HT : {relatedProduct.getLowerPriceET():h}</strong><br>
		</td><td width="100">
			<input type="button" onClick="addToCart('{relatedArticle.getId()}', '1', 'order');" name="AddCmd" value="" class="input_commander_small" flexy:ignore="yes">
			<br>
			<input type="button" onClick="addToCart('{relatedArticle.getId()}', '1', 'estimate');" name="AddDev" value="" class="input_devis_small" flexy:ignore="yes">
				
			{else:}
		</td><td width="100">
			<input type="button" onClick="addToCart('{relatedArticle.getId()}', '1', 'estimate');" name="AddDev" value="" class="input_devis_small" flexy:ignore="yes">
			{end:}
		</td>
	</tr>
</table>
</div>
</span>
</div>
{end:}
			 </div>
		<div class="spacer"></div>
		</div>
		<div class="spacer"></div>			
		<img src="{baseURL}/templates/catalog/images/img-bottom-product.png" width="730" height="8" alt="" class="floatleft">
		<div class="spacer"></div>
	</div>
	<div class="spacer"></div>
{end:}
</div>
{if:product.getTruncatedDescription(#550#)}
	<div id="descriptif">
		<div class="floatleft blocProductBig">
			<div class="spacer"></div>		
			<div class="blocProductBGBig">	
				<h2 class="title_specification_small">Description : {product.get(#name_1#):h}</h2>
				<div class="spacer"></div>			
				<div class="specification_small">
	{product.get(#description_1#):h}
	           </div>
			<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
	</div>
{end:}
{if:product.getDataSheetURI()}
<div id="plans">
	<div class="floatleft blocProductBig">
		<div class="spacer"></div>		
		<div class="blocProductBGBig">	
			<h2 class="title_specification_small">Plan : {product.get(#name_1#):h}</h2>
			<div class="spacer"></div>			
			<div class="specification_small">
				<img src="{product.getDataSheetURI()}" alt="">
  			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>			
		<img src="{baseURL}/templates/catalog/images/img-bottom-product.png" width="730" height="8" alt="" class="floatleft">
		<div class="spacer"></div>
	</div>
	<div class="spacer"></div>
</div>
{end:}   
</div>

<div class="spacer"></div>

<!-- Module de commentaire -->
<img src="{start_url}/images/front_office/global/top_gamme_star.jpg" width="972" height="3" class="top_star floatleft" alt=""/>
<div class="spacer"></div>
<h1 class="title_star"><img src="{start_url}/images/front_office/global/puce.jpg" width="5" height="7" alt="›" /> 
Avis consommateur : {productName:h}</h1>
<div class="star" id="comentaires">
	{if:commentAdded}
		<h2 style="font-size:12px;">&#0155; Merci de nous avoir donné votre avis.</h2>
	{end:}
    <!-- début de modification -->
	<!--{if:!commentAvailable}
		{if:authentified}
			<h2 style="font-size:12px;">&#0155; Vous avez déjà donné votre avis sur ce produit.</h2>
		{else:}
			<h2 style="font-size:12px;">&#0155; Veuillez vous identifier afin de poster un commentaire.</h2>
		{end:}
	{end:} -->
	{if:!authentified}
        <h2 style="font-size:12px;">&#0155; Veuillez vous identifier afin de poster un commentaire.</h2>
	{end:}
    <!-- fin de modification -->
	{if:hascomments} 
		<FLEXY:TOJAVASCRIPT avgNote="commentsAvg">
		
		<p>
			<strong>&#0155; Moyenne des notes attribuées à ce produit : </strong>
			<span id='myAvg'>
					<select name="avgGrade" disabled="disabled">
						<option value="0">0 étoile</option>
						<option value="1">1 étoile</option>
						<option value="2">2 étoiles</option>
						<option value="3">3 étoiles</option>
						<option value="4">4 étoiles</option>
					</select>
			</span>
		</p>
		<br />
		
		{foreach:comments,key,comment}
			
			<p>
				<strong>{comment[firstname]} {comment[lastname]}, le {comment[date]} </strong>
				<span id='commentNote_{key}_{comment[note_buyer]}' class="noters">

					<select name="buyerNote_{key}" disabled="disabled">
						<option value="0">0 étoile</option>
						<option value="1">1 étoile</option>
						<option value="2">2 étoiles</option>
						<option value="3">3 étoiles</option>
						<option value="4">4 étoiles</option>
					</select>
					
				</span>
				<br/>
				{comment[product_comment]}
			</p>

		{end:}
		
		<script type="text/javascript">		
		<!--		
			$('span').filter('.noters').each(function(){
				$("#"+$(this).attr("id")).stars({ 
				     inputType: "select",
				     cancelShow: false
				});
				
				noteTab = $(this).attr("id").split('_');
				note = noteTab[noteTab.length-1];
				
				$("#"+$(this).attr("id")).stars("select", Math.round(note));
			});
		//-->
		</script>
		
	{else:}	
		<p>Il n'y a pas encore de commentaire sur ce produit</p>
	{end:} 
	<div class="spacer"></div>
	
    <!-- début de modification -->
	{if:authentified}
    <!-- fin de modification -->
		<p><strong>&#0155; Ajouter un commentaire :</strong></p>
		<form name ="commentsForm" action="{actionCommentsForm}" method="post"  onSubmit="javascript: return validform_comments();">
			<textarea name="myComment"></textarea>
			<p style="width:45px; float:left; margin-top:7px;">	
				Note : <span id='myGrade' style="float:left;"></span>
	
				<div id="mySelects" style="float:left; margin-top:5px;">
                    <!-- début de modification -->
					<select name="myGrade" class="select_product">
                    <!-- fin de modification -->
						<option value="0">0 étoile</option>
						<option value="1">1 étoile</option>
						<option value="2">2 étoiles</option>
						<option value="3">3 étoiles</option>
						<option value="4">4 étoiles</option>
					</select>
				</div>	
			</p>
			
			<input type="submit" value="Poster mon message" class="validComments" />
		</form>

		<script type="text/javascript">
			$("#mySelects").stars({	inputType: "select" , cancelShow: false	});
			$("#mySelects").stars("select", 2);
					
			$("#myAvg").stars({ 
				 inputType: "select",
				 cancelShow: false
			});
			$("#myAvg").stars("select", Math.round(avgNote));
			
			$("#commentNote[]").stars({ 
				 inputType: "select",
				 cancelShow: false
			});
				
		</script>
		
	{end:}
	<div class="spacer"></div>

</div>

<!-- Fin Onja -->
<script>function setActions(){if($(window).width()<=1e3&&$(window).width()>692)$("#descr #servgar").after($(".sidepanel"));else $(".sidepanel").appendTo("#action")}function setscrollShadow(e){$("#hscroll-shad-g").css("display",e<0?"block":"none");$("#hscroll-shad-d").css("display",$("#hscroll").width()-$("#hscroll table").width()<e?"block":"none")}function hidelogin(){$("#loginpanel").animate({top:-120},{queue:false,easing:"easeOutCirc",duration:300})}$(document).ready(function(){$("select").selectbox();/*$("select#search-marque ~ .sbHolder").find(".sbOptions").attr("style","width: 118px !important").hide();*/$("input[type=radio]:not(.none), input[type=checkbox]:not(.none)").picker();$("#mylogin").click(function(e){e.stopPropagation();$("#loginpanel").animate({top:0},{queue:false,easing:"easeOutCirc",duration:200})});$("body").click(function(e){if(e.target.id=="loginpanel"||$(e.target).parents("#loginpanel").size())e.stopPropagation();else hidelogin();if(e.target.id=="menu"||$(e.target).parents("#menu").size())e.stopPropagation();else menureset()});$("form#login #email, form#login #password").textPlaceholder();$("form#search #search-text").textPlaceholder();$("#tabs1").tabs();var e=$("#zoom-target").easyZoom({parent:"div.zoom-container"});e.data("easyZoom").gallery("a.zoom-thumbnail");if(!ie8){myScroll=new iScroll("hscroll",{snap:"td",onScrollMove:function(){setscrollShadow(myScroll.x)},onScrollEnd:function(){setscrollShadow(myScroll.x)},onBeforeScrollStart:function(e){var t=e.target;while(t.nodeType!=1)t=t.parentNode;if(t.tagName!="SELECT"&&t.tagName!="INPUT"&&t.tagName!="TEXTAREA")e.preventDefault()}})}setActions()});$(window).resize(function(){setActions()});var myScroll;if(Modernizr.touch)$(".submenu").append('<a href="javascript:;" class="close" title="Fermer" onclick="menureset();"></a>');$('ul#colors input[type="radio"]').click(function(e){$("ul#colors input:radio").parent().find("label").removeClass("checked");$("ul#colors input:radio:checked").parent().find("label").addClass("checked");console.log($("ul#colors input:radio:checked").val())});$("ul.listprods li").mouseover(function(e){$(this).find("a.btn-buy").stop().animate({left:30},200,"easeInOutCirc")});$("ul.listprods li").mouseout(function(e){$(this).find("a.btn-buy").stop().animate({left:-44},200,"easeInOutCirc")});$("#icons_compare").mouseenter(function(e){$(this).find("div").stop().toggle("fast")});$("#icons_compare").mouseleave(function(e){$(this).find("div").stop().toggle("fast")});$("#icons_consult").mouseenter(function(e){$(this).find("div").stop().toggle("fast")});$("#icons_consult").mouseleave(function(e){$(this).find("div").stop().toggle("fast")})</script>




