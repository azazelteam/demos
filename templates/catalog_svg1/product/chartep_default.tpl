
<flexy:include src="header.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->
<div id="center" class="page catalog category">

	<ul class="pathway">
		<li><a href="{start_url}" class="pwhome" title="Accueil"><img class="noborder" alt="Accueil" width="16" height="15" src="{starturl}/templates/catalog/images/icon-home.png" /></a></li>

		{if:categoryPath}

		{foreach:categoryPath,cat}
		<li>
			<a href="{cat[href]}">{cat[name]}</a>
		</li>
		{end:}

		{end:}



	</ul>
<div id="container">

{if:iduser}<span style="color:red;font-weight: bold;">/product/chartep_default.tpl</span>{end:}
    {breadcrumbs:h}
	
	<!--<div id="panier-b">
            	<a title="Consulter mon panier" class="part-l" href="/catalog/basket.php">
                	<span class="h3">Mon <span class="strong">panier</span></span>
	                <span class="num"><span class="strong">
                    {if:basket.getItemCount()}
                                        {basket.getItemCount()}</span> article(s)</span>
                                                               		
                                    {else:}
                                        0</span> article(s)</span>
                                    {end:}
                   </a>     
                    <a title="Voir le panier" class="part-r" href="/catalog/basket.php">            
                    {if:basket.getItemCount()}
                    	<span class="total">Total : <span class="strong">{basket.getTotalET():h}</span></span>
                    {else:}
                    	<span class="total">Total : <span class="strong">0 &euro;</span></span>
                    {end:}
					<span class="btn-med">Voir le panier</span>
                </a>
   </div>-->
   <script type="text/javascript">
   	$(document).ready(function(){
		$("div.category_description p").each(function(i){
			var lng = $(this).text().length;
			if(lng == 1 || lng == 0){
				$(this).remove();
			}
 		});
	});
   </script>
  		<!--<a class="btn-med grey" href="javascript:history.go(-1)">Retour</a>-->
		<h1 class="CategoryName"><!--{category.get(#name_1#)}--><!--{cat_page_title}-->{category.getName():h}</h1>
        <div flexy:if="category.get(#description_1#)" id="category_description" class="category_description">{category.get(#description_1#):h}</div>

		
		
<div class='colonne_gauche'>
	<aside class="centrale cbloc1">	
	{category.getReducedSubCateg():h}

</aside>
{if:category.getSubCatIntitule()}
	<div id="filtersearch" class="searchfilter" style="margin-bottom:20px;">
	   {category.getSubCatIntitule():h}
	</div>
{end:}
</div>
<div id="multifiltersearch" class="searchfilter with-left">

        <div class="products">
        	<ul class="list_product" >
            <li itemscope itemtype="http://schema.org/Product" flexy:foreach="category.getChildren(),key,product">
        
            {if:Math.mod(key,4)}
            	
         
            <div style="clear:both"> </div> {end:}
				<flexy:include src="/product/product_detail_13.tpl"></flexy:include>
            </li>
            {if:category.getOthercategProd()}
			<li itemscope itemtype="http://schema.org/Product" flexy:foreach="category.getOthercategProd(),key,product">
        
            {if:Math.mod(key,4)}
                
         
            <div style="clear:both"> </div> {end:}
                <flexy:include src="/product/product_detail_13.tpl"></flexy:include>
            </li>
			{end:}
		</ul>

	<br style="clear:both;" />
    </div>	  
</div><!--fin multifiltersearch -->
		
		<div id="loader"style="color:#44474E; font-size:13px; font-weight:bold; padding:20px; text-align:center;display:none; position:fixed; z-index:10000000; background-color:#000; opacity:0.7; width:100%; height:100%; text-align:center; padding-top:300px; left:0px; top:0px;">
<img src="{starturl}/images/back_office/content/loading.gif" alt="Chargement..." /><br />
		Recherche en cours...
</div>
<div class="spacer" style="clear:both;margin-bottom:20px;"></div>



<div flexy:if="category.get(#description_plus_1#)" id="description_plus" class="category_description">{category.get(#description_plus_1#):h}</div>
</div><!--fin container -->
<script type="application/javascript">
	/* Suppression de ",00" du pourcentage de remise */
	$(".rate").each(function(index) {
		var discountFloat = $(this).text().trim();
		var discountInt = discountFloat.replace(/,[0-9]*\s%/g, "%");
		$(this).html(discountInt);
	});
	$('ul.listprods li').mouseover(function(e) {
$(this).find('a.btn-buy').stop().animate({left:28}, 200, "easeInOutCirc");
})
$('ul.listprods li').mouseout(function(e) {
$(this).find('a.btn-buy').stop().animate({left:-44}, 200, "easeInOutCirc");
}) 
</script>
<!--====================================== Fin du contenu ======================================-->
</div>
</div>

	</div><flexy:include src="foot.tpl"></flexy:include> 