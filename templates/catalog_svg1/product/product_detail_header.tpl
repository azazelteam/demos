
{if:iduser}<span style="color:red;font-weight: bold;">/product/product_detail_header.tpl</span>{end:}
<div id="visus" class="zoom-container">
	<div class="visuel">
    	<div class="mainImage">
            {if:product.getZoom()}
                <a href="{product.getZoom()}" class="productZoom" title="{product.get(#name_1#):h}" data-rel='galProduct'>
                    <div class="promoProduct" title="Promotion" flexy:if="product.getPromotionCount()"></div>
                    <div class="labelZoom" title="Survoler le visuel pour l'agrandir"></div>
            {end:}
            <img  itemprop="image" src="{product.getImage(#280#):h}" width="280" height="280" alt="{product.get(#name_1#):h}" title="{product.get(#name_1#):h}">
            {if:product.getZoom()}	
                </a>
            {end:}
            <div class="spacer"></div>
        </div>        
        {if:product.getImages()}
        <div class="thumbs_list">
            <a href='{product.getZoomURI(index):h}' data-rel="{gallery: 'galProduct', smallimage: '{product.getImage(#180#,index):h}',largeimage: '{product.getImage(#800#,index):h}'}" title="{product.get(#name_1#):h}" flexy:foreach="product.getImages(1024,0),index,image"><img class="withbord" src="{product.getImage(#51#,index):h}" width="51" height="51" alt="{product.get(#name_1#):h} image {index}" title="{product.get(#name_1#):h} image {index}">
            </a>
		</div>
            <div class="spacer"></div>
        {end:}
       
		<ul class="normes">
			<li flexy:foreach="product.getPrimaryPictogram(),picto"><img src="{picto.image}" alt="{picto.name}" title="{picto.name}" class="pictos primary"></li>
            <li flexy:foreach="product.getSecondaryPictograms(),picto"><img src="{picto.image}" alt="{picto.name}" title="{picto.name}" class="pictos secondary"></li>
		</ul>
		
		
	</div><!--visuel-->
</div><!-- ProductOneLeftDiv -->			
<script type="text/javascript">$(document).ready(function(){var e={zoomType:"standard",lens:true,preloadImages:true,preloadText:"Chargement...",alwaysOn:false,zoomWidth:340,zoomHeight:340,title:false};$(".productZoom").jqzoom(e)})</script>
