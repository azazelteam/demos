<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{translations[survey_title]}</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Language" content="fr" />
	<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/mails.css" />
</head>
<body>
{if:iduser}<span style="color:red;font-weight: bold;">/mail_survey.tpl</span>{end:}
	<p class="logo"><img src="http://{server_host}{ad_logo}" alt="" style="border-style:none;" /></p>
	<p id="SurveyTitle">{translations[survey_notification]}</p>
	<p>
		Une nouvelle enquête de satisfaction à été enregistrée le {date:h}
		sur le site <a href="{start_url}">{ad_sitename}</a>.
	</p>
	<p class="Bold">
		Informations concernant le visiteur :
	</p>
	<p>
{if:idBuyer}
	<table class="coordonnees" >
		<tr>
			<td class="tdRight">Civilité :</td>
			<td class="tdLeft" >{title}</td>
		</tr>
		<tr>
			<td class="tdRight">Nom :</td>
			<td class="tdLeft">{lastname}</td>
		</tr>
		<tr>
			<td class="tdRight">Prénom :</td>
			<td class="tdLeft">{firstname}</td>
		</tr>
		<tr>
			<td class="tdRight">Raison sociale :</td>
			<td class="tdLeft">{company}</td>
		</tr>
		<tr>
			<td class="tdRight">Téléphone :</td>
			<td class="tdLeft">{phonenumber}</td>
		</tr>
		<tr>
			<td class="tdRight">Fax :</td>
			<td class="tdLeft">{faxnumber}</td>
		</tr>
		<tr>
			<td class="tdRight">Email :</td>
			<td class="tdLeft"><a href="mailto:{email}">{email}</a></td>
		</tr>
		<tr>
			<td class="tdRight">Voir la fiche client :</td>
			<td class="tdLeft"><a href="{start_url}/sales_force/contact_buyer.php?key={idbuyer}&type={type}">Voir la fiche client</a></td>
		</tr>
	</table>
{else:}
	Visiteur inconnu
{end:}
	</p>
	<p id="SurveyTitle">{translations[survey_result]}</p>
		{surveyHTML}
	<br style="clear:both;" />
	<p class="Bold" style="margin-bottom:0px;">
		{translations[survey_user_comment]} :
	</p>
	<p style="margin-top:0px;">
		<div id="comment">{comment}</div>
	</p>
</body>
</html>