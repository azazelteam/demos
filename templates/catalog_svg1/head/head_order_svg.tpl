<!DOCTYPE HTML>
<html lang="fr">
<head>
	<meta charset="utf-8" />
	<title>{if:title}{title:h}{else:}{head.getTitle():h}{end:}</title>
    <meta name="robots" content="noindex,nofollow" />
    <meta content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1" name="viewport" />
	<meta name="description" content="{meta_description}" />
	<meta name="keywords" content="{meta_keywords}" />
	<meta name="author" content="Abisco" />	
	<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/general.css?" />
	<!--<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/specific.css"  />
	<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/scroll.css"  />
	<link rel="stylesheet" type="text/css" media="print" href="{start_url}/templates/catalog/css/printing.css" />
	<link rel="stylesheet" type="text/css" href="{start_url}/templates/catalog/css/menu.css" />  
    <link rel="stylesheet" type="text/css" media="screen" href="{start_url}/templates/catalog/css/superfish.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="{start_url}/templates/catalog/css/superfish-navbar.css" />
	<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/sse.css" />-->
	<!--<script src="{start_url}/www/_js/tooltip.js" type="text/javascript"></script>
	<script src="{start_url}/www/_js/bookmark.js" type="text/javascript"></script>-->
	<script src="{start_url}/www/_js/flashobject.js" type="text/javascript" async ></script>	
	<script type="text/javascript" src="{start_url}/js/jquery/jquery-1.6.2.min.js" ></script>
	<script type="text/javascript" src="{start_url}/js/jquery/jquery-ui-1.8.4.custom.min.js" defer></script>
	<script type="text/javascript" src="{start_url}/js/front.droppy.js"></script>
	<script type="text/javascript" src="{start_url}/js/js_search_filter.js" async></script>	
    {if:head}
    <link type="text/css" rel="stylesheet" href="{css}" flexy:foreach="head.getCSS(),css" />
	<script type="text/javascript" src="{script}" flexy:foreach="head.getScripts(),script" defer></script>
    {end:}    
	<!--[if lt IE 7.]>
	<script defer type="text/javascript" src="{start_url}/js/pngfix.js"></script>
	<link href="{start_url}/templates/catalog/css/ltie7.css" rel="stylesheet" type="text/css" />
	<![endif]-->
    <flexy:toJavascript baseURL={baseURL}></flexy:toJavascript>	
	<script type="text/javascript">
		function redirectToBasket(){
			document.location.href=urldep+"/catalog/basket.php"; 
		}		
		function redirectToEstimateBasket(){
			document.location.href=urldep+"/catalog/estimate_basket.php"; 
		}	
	</script>
	<!-- ScrollTo -->
	<script type="text/javascript" src="{start_url}/js/jquery/jquery.scrollTo-min.js" defer></script>
	<script type="text/javascript">
	<!--	
		function getHeight() {			
			var myHeight = 0;			
			if( typeof( window.innerHeight ) == 'number' ) {
				//Non-IE
				myHeight = window.innerHeight;
			} else if( document.documentElement && document.documentElement.clientHeight ) {
				//IE 6+ in 'standards compliant mode'
				myHeight = document.documentElement.clientHeight;
			} else if( document.body && document.body.clientHeight ) {
				//IE 4 compatible
				myHeight = document.body.clientHeight;
			}					
			return myHeight;			
		}		
		function scroll(){
			var height = getHeight();
			$.scrollTo( "+="+height+"px", 500 );
			return false;
		}
	// -->	
	</script>
	<FLEXY:TOJAVASCRIPT globalstarturl="start_url">		
	<script type="text/javascript">
        $(document).ready(function() {
                var inprog1 = 0;
                $("#commandeI").hover(function() {
                 if (inprog1 == 0) {
                     inprog1 = 1;                    
                    filename = globalstarturl+"/catalog/basket_popup.php";                
                    $.ajax(
                                {
                                    type: "GET",
                                    url: filename,
                                    data: "",
                                    dataType: "html",
                                    contentType : "text/html; charset=UTF-8",
                                    success:function(response){document.getElementById('basketOrder').innerHTML = response;}
                                }
                            );                     
                     $("#divcommande").slideDown(100);                     
                 }
            }, function() {
                 $("#divcommande").slideUp(100, function() {inprog1 = 0;});
            });
        });  
		function forgetPassword(){$("#forgetPassword").dialog({title:"Mot de passe oubli&eacute; ?",bgiframe:true,modal:true,width:550,minHeight:10,overlay:{backgroundColor:'#000000',opacity:0.5},close:function(event,ui){$("#forgetPassword").dialog("destroy");}});}
		function sendPassword(Email,divResponse){data="&Email="+Email;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(XMLHttpRequest,textStatus,errorThrown){alert("Impossible de r&eacute;cup&eacute;rer l'email");},success:function(responseText){$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+responseText+"</p>");}});}
    </script>    
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-transit-modified.js"></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-ui-1.10.3.custom.pack.js" ></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.selectbox-0.2.pack.js" ></script>
<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.textPlaceholder.js" ></script>
<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.sky.carousel-1.0.2.pack.js" ></script>   
    <script type="text/javascript" src="{start_url}/templates/catalog/js/layerslider.transitions.js" ></script>
	<script type="text/javascript" src="{start_url}/templates/catalog/js/layerslider.kreaturamedia.jquery.js"async></script> 
    <script type="text/javascript" src="{start_url}/templates/catalog/js/menu.js "></script>  
    
<script type="text/javascript">
var webleads_site_ids = webleads_site_ids || [];
webleads_site_ids.push(100650005);
(function() {
var s = document.createElement('script');
s.type = 'text/javascript';
s.async = true;
s.src = ( document.location.protocol == 'https:' ?
'https://stats.webleads-tracker.com/js' : 'http://stats.webleads-tracker.com/js'
);
( document.getElementsByTagName('head')[0] ||
document.getElementsByTagName('body')[0] ).appendChild( s );
})();
</script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/hoverIntent.js" async></script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/superfish.js" async></script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/supersubs.js" async></script>
</head>
<body>
<div class="ui-widget-overlay" id="bg-ui-clone" style="width: 100%; height: 100%;display: none; z-index: 1001;"></div>
<div style="width: auto; min-height: 0px; height: auto; top: 30%; left: 35%; position: fixed; z-index: 1002; background: none repeat scroll 0px 0px rgb(255, 255, 255); display: none; padding: 5px; border-radius: 5px;" id="forgetPassword" class="">
<p class="txt-oubl" style="float:left">Mot de passe oubli&eacute;?</p>
<p class="txt-clos" style="float:right; cursor: pointer;"><a onclick="closeBTN();">close</a></p>
			<div style="clear:both" id="EmailResponse">
				<p class="blocTextNormal">Saisissez votre adresse e-mail pour r&eacute;cup&eacute;rer vos identifiants</p>
				<input type="text" style="width:285px;" class="loginFieldMedium input_email" value="" name="Email" id="Email" />				<input type="submit" onclick="sendPassword( $('#Email').val(), '#EmailResponse' );" style="float:left;" class="submitField" value="Envoyer" name="Envoyer" />			</div>
		</div>
<div id="mainContainer">
	<div id="contents">      
		<div id="header" style="text-align:center;">
	        <div class="logo" style="float:none;display:inline-block;"><a><img src="{start_url}/templates/catalog/images/header/logo.png" alt="V&ecirc;tements de travail et chaussures de s&eacute;curit&eacute;" id="logo" /></a></div>
            <span class="tel" style="float:none;display:inline-block;">Le site <span class="em">d&eacute;di&eacute; aux professionnels</span><br /><span class="telstrong">03<small>.</small>88<small>.</small>66<small>.</small>02<small>.</small>03</span><br /></span>
			
            <span class="clear"></span>
		</div>
        <div id="main">
		
		