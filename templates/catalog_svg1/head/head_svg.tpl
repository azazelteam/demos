<!DOCTYPE HTML>
<html lang="fr">
<head>
	<meta charset="utf-8" />
	<title>{if:title}{title:h}{else:}{head.getTitle():e}{end:}</title>
    <meta content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1" name="viewport" />
	<meta name="description" content="{meta_description}" />
	<meta name="keywords" content="{meta_keywords}" />
	<meta name="author" content="...." />	
	<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/general.css?" />
	<!--<link rel="stylesheet" type="text/css" media="all" href="{start_url}/templates/catalog/css/scroll.css"  />
    <link rel="stylesheet" type="text/css" media="screen" href="{start_url}/templates/catalog/css/superfish.css" />
	<link rel="stylesheet" type="text/css" media="screen" href="{start_url}/templates/catalog/css/superfish-navbar.css" />-->
	<!--<script src="{start_url}/www/_js/tooltip.js" type="text/javascript"></script>
	<script src="{start_url}/www/_js/bookmark.js" type="text/javascript"></script>-->
	<script src="{start_url}/www/_js/flashobject.js" type="text/javascript" async ></script>	
	<script type="text/javascript" src="{start_url}/js/jquery/jquery-1.6.2.min.js" ></script>
	<script type="text/javascript" src="{start_url}/js/jquery/jquery-ui-1.8.4.custom.min.js" defer></script>
	<script type="text/javascript" src="{start_url}/js/front.droppy.js"></script>
	<script type="text/javascript" src="{start_url}/js/js_search_filter.js" async></script>	
    {if:head}
    <link type="text/css" rel="stylesheet" href="{css}" flexy:foreach="head.getCSS(),css" />
	<script type="text/javascript" src="{script}" flexy:foreach="head.getScripts(),script" defer></script>
    {end:}    
	<!--[if lt IE 7.]>
	<script defer type="text/javascript" src="{start_url}/js/pngfix.js"></script>
	<link href="{start_url}/templates/catalog/css/ltie7.css" rel="stylesheet" type="text/css" />
	<![endif]-->
    <flexy:toJavascript baseURL={baseURL}></flexy:toJavascript>	
	<script type="text/javascript">
		function redirectToBasket(){
			document.location.href=urldep+"/catalog/basket.php"; 
		}		
		function redirectToEstimateBasket(){
			document.location.href=urldep+"/catalog/estimate_basket.php"; 
		}	
	</script>
	<!-- ScrollTo -->
	<script type="text/javascript" src="{start_url}/js/jquery/jquery.scrollTo-min.js" defer></script>
	<script type="text/javascript">
	<!--	
		function getHeight() {			
			var myHeight = 0;			
			if( typeof( window.innerHeight ) == 'number' ) {
				//Non-IE
				myHeight = window.innerHeight;
			} else if( document.documentElement && document.documentElement.clientHeight ) {
				//IE 6+ in 'standards compliant mode'
				myHeight = document.documentElement.clientHeight;
			} else if( document.body && document.body.clientHeight ) {
				//IE 4 compatible
				myHeight = document.body.clientHeight;
			}					
			return myHeight;			
		}		
		function scroll(){
			var height = getHeight();
			$.scrollTo( "+="+height+"px", 500 );
			return false;
		}
	// -->	
	</script>
	<FLEXY:TOJAVASCRIPT globalstarturl="start_url">		
	<script type="text/javascript">
        $(document).ready(function() {
                var inprog1 = 0;
                $("#commandeI").hover(function() {
                 if (inprog1 == 0) {
                     inprog1 = 1;                    
                    filename = globalstarturl+"/catalog/basket_popup.php";                
                    $.ajax(
                                {
                                    type: "GET",
                                    url: filename,
                                    data: "",
                                    dataType: "html",
                                    contentType : "text/html; charset=UTF-8",
                                    success:function(response){document.getElementById('basketOrder').innerHTML = response;}
                                }
                            );                     
                     $("#divcommande").slideDown(100);                     
                 }
            }, function() {
                 $("#divcommande").slideUp(100, function() {inprog1 = 0;});
            });
        });  
		function forgetPassword(){$("#forgetPassword").dialog({title:"Mot de passe oubli&eacute; ?",bgiframe:true,modal:true,width:550,minHeight:10,overlay:{backgroundColor:'#000000',opacity:0.5},close:function(event,ui){$("#forgetPassword").dialog("destroy");}});}
		function sendPassword(Email,divResponse){data="&Email="+Email;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(XMLHttpRequest,textStatus,errorThrown){alert("Impossible de r&eacute;cup&eacute;rer l'email");},success:function(responseText){$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+responseText+"</p>");}});}
    </script>    
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-transit-modified.js"></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery-ui-1.10.3.custom.pack.js" ></script>
    <script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.selectbox-0.2.pack.js" ></script>
<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.textPlaceholder.js" ></script>
<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.sky.carousel-1.0.2.pack.js" ></script>   
    <script type="text/javascript" src="{start_url}/templates/catalog/js/layerslider.transitions.js" ></script>
	<script type="text/javascript" src="{start_url}/templates/catalog/js/layerslider.kreaturamedia.jquery.js"async></script> 
    <script type="text/javascript" src="{start_url}/templates/catalog/js/menu.js "></script>
<script type="text/javascript">
		function forgetPassword(){
			//$('#bg-ui-clone').show();
			$('#forgetPassword').show();
		}
		function sendPassword(Email,divResponse){data="&Email="+Email;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(XMLHttpRequest,textStatus,errorThrown){alert("Impossible de r&eacute;cup&eacute;rer l'email");},success:function(responseText){$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+responseText+"</p>");}});}
		function closeBTN(){
			//$('#bg-ui-clone').hide();
			$('#forgetPassword').hide();
		}
	</script>

<script type="text/javascript" src="{start_url}/js/superfish_menu/hoverIntent.js" async></script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/superfish.js" async></script>
<script type="text/javascript" src="{start_url}/js/superfish_menu/supersubs.js" async></script>
<script type="text/javascript">
(function() 
	{
		var s = document.createElement('script');
		s.type = 'text/javascript'; s.async = true;
		s.src = ('https:' == document.location.protocol ? 'https://' : 'http://')+'bp-1c51.kxcdn.com/prj/AS-2314166.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	}
)();
</script> 
<script type="text/javascript">
var google_replace_number = "03.88.66.02.03";
(function(a,e,c,f,g,b,d){var h={ak:"1038552525",cl:"YM1jCOiS3WcQzZuc7wM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");
</script> 
</head>
<body>
<div class="ui-widget-overlay" id="bg-ui-clone" style="width: 100%; height: 100%;display: none; z-index: 1001;"></div>

<div id="mainContainer">
{if:iduser}<span style="color:red;font-weight: bold;">/head/head.tpl</span>{end:}
	<div id="contents">      
		<div id="header">
	        Logo
            
			<div id="menutop">
            	<div id="loginpanel">
                	p>Mon <strong>compte</strong></p>
                    {if:customer}   
                    <style type="text/css">
						#header #loginpanel h3 {
							margin: 0;
							text-align: center;
							width: 100%;
						}
						#loginpanel > div {
							clear: both;
							float: left;
							margin: 10px 0;
							text-align: center;
							width: 100%;
						}
						#header #loginpanel ul {
							clear: both;
							float: left;
							text-align: center;
							width: 100%;
							margin: 0;
						}
					</style>   
                 <div>
                 	<span class="welcome" style="color: #fff; font-weight: bold;">Bienvenue</span>					
                	<span class="userlogin" style="color: #fff;">{if:buyer[firstname]}{buyer[firstname]}{else:}{buyerFirstname}{end:} {if:buyer[lastname]}{buyer[lastname]}{else:}{buyerLastname}{end:}</span>  
                 </div>
                 <ul>					
                    <li class="left"><a href="{start_url}/catalog/account.php"  rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none">Mon compte</a></li>					
                    <li class="left"><a href="{start_url}/www/logoff.php"  rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"> [D&eacute;connexion]</a></li>
                </ul>    
            {else:}
                   <form id="login" action="/catalog/account.php" target="_top" name="Form_logon" method="post">                    
                        <label for="emaillogin">Adresse email</label>
                        <input type="text" placeholder="adresse email" class="floatL" name="Login" id="emaillogin" style="height:30px; padding:0px;" />                        
                        <label for="passwlogin">Mot de passe</label>
                        <input type="password" placeholder="mot de passe" class="floatL" name="Password" id="passwlogin" style="height:30px; padding:0px;" />                        
                        <input type="submit" value="OK" />                    
                    </form>
                    <ul>
                    	<li><a href="#" onclick="forgetPassword(); return false;">Mot de passe oubli&eacute; ?</a></li>
                    	<li><a href="/catalog/register.php">Cr&eacute;er son compte client</a></li>
					</ul>
               {end:}
                    <a onclick="hidelogin();" title="Fermer" class="close" href="javascript:;"></a>
                </div>
                <ul>
                    <li><a href="/catalog/contact.php">Contact</a></li>
                    <li><a id="mylogin" href="javascript:;">Mon compte</a></li>
                    <li><a href="{start_url}/catalog/basket.php">Mon panier <small>
                    {if:basket.getItemCount()}
                                        ({basket.getItemCount()})
                    {else:}
                    	0
                    {end:}                    
                                        </small></a></li>
                </ul>
                
			</div>
            <form id="SearchForm" action="/catalog/search.php" method="post">
                <label for="search-text">Rechercher...</label>
                <input type="text" placeholder="Rechercher..." value="{search_value}" name="search_product" class="floatL" id="search-text" />
		{basket.GetBrand():h}
		{basket.GetJobs():h}
                <input type="hidden" value="" name="idcategoryname" />
                <input type="hidden" value="AND" name="type" />
                <input type="hidden" value="1" name="page" />
                <input type="hidden" value="20" name="resultPerPage" />
                <input type="submit" title="Rechercher" value="Rechercher" />
            </form>
            <span class="clear"></span>
		</div>
        <div id="main">
            <div style="width: auto; min-height: 0px; height: auto; top: 30%; left: 35%; z-index: 1002; background: none repeat scroll 0px 0px rgb(255, 255, 255); display: none; padding: 5px; border-radius: 5px;" id="forgetPassword" class="">
                <p class="txt-oubl" style="float:left">Mot de passe oubli&eacute;?</p>
                <p class="txt-clos" style="float:right; cursor: pointer;"><!--a onclick="closeBTN();">close</a--></p>
                <div style="clear:both" id="EmailResponse">
                    <p class="blocTextNormal">Saisissez votre adresse e-mail pour r&eacute;cup&eacute;rer vos identifiants</p>
                    <input type="text" style="width:285px;" class="loginFieldMedium input_email" value="" name="Email" id="Email" />				<input type="submit" onclick="sendPassword( $('#Email').val(), '#EmailResponse' );" style="float:left;" class="submitField" value="Envoyer" name="Envoyer" />			</div>
            </div>
        
