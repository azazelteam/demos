<flexy:include src="/head/header.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/product/compare.tpl</span>{end:}
<script type="text/javascript" language="javascript">
<!--
	function hide(linename) {			
			document.getElementById(linename).style.display='none';		
	}	
	function validQty(element,qtt){			
		re = /^\d+$/
		if(re.test(element.value)) {
			if(Number(element.value) < qtt) {
				alert('quantité minimum : '+qtt);
				return false;
			}
		} else {		
			alert('`'+element.value+"` n'est pas une quantité valide!");
			return false;			
		}		
		return true;	
	}	
	function PopupCentrer(page,largeur,hauteur,options) {
  var top=(screen.height-hauteur)/2;
  var left=(screen.width-largeur)/2;
  window.open(page,"","top="+top+",left="+left+",width="+largeur+",height="+hauteur+","+options);
}
//-->
</script>
<style type="text/css">
	@media print{ 	
	#banner, #left, #outils{
		display:none
	}
	#center{
		position:absolute;
		top:0px;
		left:0px;
	}
}
.tableau{
	border-collapse:collapse;
}
#ProductCompare .tableau_titre{
	font-family:Arial;
	color:#909090;
	font-size:22px;
	font-weight:normal;
	text-align:left;
	padding-top:15px;
	padding-bottom:15px;
}
#ProductCompare .tableau a,
#ProductCompare .tableau a:link{
	font-family:Arial;
	color:#909090;
	font-size:11px;
	font-weight:normal;
	text-decoration:none;
	text-align:right;
}
#ProductCompare .tableau a:hover{
	color:#000000;
}
#ProductCompare .tableau a:active{
	color:#000000;
}
#ProductCompare .tableau th{
	border:1px solid #D9DADC;
	background-color: #F7FAFB;
	font-family:Arial;
	color:#9BA0A3;
	font-size:12px;
	font-weight:bold;
	text-align:left;
	padding-left:10px;
}
#ProductCompare .td_titre{
	border:1px solid #D9DADC;
	background-color: #f4eefb;
	font-family:Arial;
	color:#000000;
	font-size:11px;
	font-weight:normal;
	text-align:center;
}
.td_blanc{
	border:1px solid #D9DADC;
	background-color: white;
	font-family:Arial;
	color:black;
	font-size:10px;
	font-weight:normal;
	text-align:center;
}
#ProductCompare .td_ref_val{
	border:1px solid #D9DADC;
	background-color: #fe8819;
	font-family:Arial;
	color:white;
	font-size:13px;
	font-weight:bold;
	text-align:center;
}
#ProductCompare .td_gris{
	border:1px solid #D9DADC;
	background-color: #ffffff;
	font-family:Arial;
	color:black;
	font-size:11px;
	font-weight:bold;
	text-align: center;
}
#ProductCompare .td_select{
	border:1px solid #D9DADC;
	background-color: #fffbe9;
	font-family:Arial;
	color:black;
	font-size:11px;
	font-weight:bold;
	text-align: center;
}
#ProductCompare .td_int_val{
	border:1px solid #D9DADC;
	background-color: #ffffff;
	font-family:Arial;
	color:black;
	font-size:11px;
	font-weight:normal;
	text-align: center;
}
#ProductCompare .td_intitule{
	border:1px solid #D9DADC;
	background-color: #fff0c0;
	font-family:Arial;
	color:black;
	font-size:11px;
	font-weight:bold;
	text-align: right;
	margin-right:7px;	
}
#ProductCompare .td_gris_titre{
	border:1px solid #D9DADC;
	background-color: #F7FAFB;
	font-family:Arial;
	color:black;
	font-size:11px;
	font-weight:bold;
	text-align: center;
}
#ProductCompare .td_ref_titre{
	border:1px solid #D9DADC;
	background-color: #fe8819;
	font-family:Arial;
	color:white;
	font-size:13px;
	font-weight:bold;
	text-align:right;
	padding-right:10px;
}
#ProductCompare .td_gris_titre {
	border:1px solid #D9DADC;
	background-color: #F7FAFB;
	font-family:Arial;
	color:black;
	font-size:11px;
	font-weight:bold;
	text-align:right;
	padding-left:10px;
}
#ProductCompare .sous_titre{
	color:#757573;
	font-size:16px;
	font-family:Arial;
	text-align:center;
	font-weight:normal;
	padding-top:5px;
	padding-bottom:5px;
}
#ProductCompare .input{
	border-top:1px solid #9B9C9E;
	border-left:1px solid #9B9C9E;
	border-bottom:1px solid #000000;
	border-right:1px solid #000000;
	background-color: white;
	height:15px;
	font-size:10px;
	padding:0px;
}
#ProductCompare .product_thumb{
	border-style:none;
}
#ProductCompare .productname{
	font-weight:bold;
}
#ProductCompare .reference{
	font-weight:bold;
}
#ProductCompare form{
	margin:0px;
	padding:0px;
}
#ProductCompare .button_img{
	border-style:none;
}</style>


<h1 style="font-size: medium; color: #888888; border: medium none; margin-top: 15px; margin-bottom: 5px; width: 100%;">Comparer les produits</h1>
{if:productsAvailable}
<div id="ProductCompare">
  <table width="96%" border="0" cellspacing="0" cellpadding="0" align="center">
    <tr>
      <td class="tableau_titre">
      	Comparer les produits
      </td>
    </tr>	
  </table> 
	<!-- Tableau avec intitulés dans la barre gauche -->
  <table width="96%" border="0" align="center" cellpadding="3" cellspacing="0" class="tableau">
    <tr>
      <td rowspan="3" valign="top">
      	<table border="0" align="center" cellpadding="3" cellspacing="0" class="noprint">
	        <tr>	
	          <td width="110"><div align="right"><a href="javascript:window.print();">imprimer</a></div></td>
	          <td width="20" align="left"><img src="{start_url}/templates/catalog/images/comparatif_imprimer.gif" border="0" alt="Imprimer le comparatif" />
	          </td>
	        </tr>
	        <tr>
	          	<td width="110"><div align="right"><a href="{start_url}/templates/catalog/images/product_compare.php?empty=yes">Vider le comparatif</a></div></td>
	          	<td width="20" align="left">
	          </td>
	        </tr>	        
	        <tr>
	          <td><div align="right"><a href="{start_url}/templates/catalog/images/product_compare.php">afficher tout</a></div></td>
	          <td align="left"> 
	          </td>
	        </tr>
	        <tr>
	          <td><div align="right"><a href="{start_url}/templates/catalog/images/pdf_compare.php">PDF</a></div></td>
	          <td align="left"> 
	          </td>
	        </tr>
	        <tr>
	          <td><div align="right"><a href='javascript:PopupCentrer("send_comparatif.php",550,300,"menubar=no,scrollbars=no,statusbar=no")'>Envoyer</a></div></td>
	          <td align="left"> 
	          </td>
	        </tr>
      </table>
         </td>
         	  
	  	{foreach:productsArray,p}		
	  	<!-- Icône du produit -->
			<td class="td_blanc">
				<a href="{p[href]}">
					<img class="product_thumb" src="{p[img]}" alt="{p[altIMG]}" />
			</td>
		{end:}			
		
      <td>&nbsp;</td>
    </tr>
    <tr>
    {foreach:productsArray,p}
    		<td class="td_titre">
				<div align="center">
					<a href="{p[href]}"class="productname">
						{p[name]:e}
					</a>
				</div>
			</td>

	{end:}

    <td class="empty">&nbsp;</td>
    </tr>
    <tr>


	{foreach:productsArray,p}
	<td class="td_blanc">
	      		<table width="50" border="0" align="center" cellpadding="0" cellspacing="0">
			        <tr>		      
			          <td>
				          <!-- Liste personnelle -->
				          <form action="buyer_func.php" method="post">
							<input type="hidden" name="IdProduct" value="{p[productID]}" />
							<input style="width:auto;height:auto" type="image" name="Add" src="{start_url}/templates/catalog/images/listeperso.gif" onclick="catal_perso_add(this,'{p[titre]},'p[IdBuyer]');" />
						  </form>	  
			         </td>
			          <!-- Supprimer -->
			          <td>
			          	<a href="{p[deleteLink]}">
			          		<img class="button_img" src="{start_url}/templates/catalog/images/supprimer.gif" alt="supprimer" />
			          	</a>
			          </td>
			        </tr>
	      		</table>
      		</td>
	{end:}      
	
	<td>&nbsp;</td>
    </tr>
    <tr id="reference">
    	<td class="td_ref_titre">R&eacute;f&eacute;rences</td>
    
    {foreach:productsArray,p}    
      	<td class="td_ref_val">{p[reference]}</td>
	{end:}
      <td class="empty"></td>
    </tr>
    <tr>      
     <td colspan="{productsAvailable}" class="sous_titre"><div align="center">&nbsp;</div></td>
    </tr>
    <!-- Caractéristiques techniques -->   
    
    	{foreach:specifications,spe}			
				<tr id="charac{spe[specifID]}">
     			<td class="td_intitule">{spe[specifLabel]:e}</td>				
				
					{foreach:spe[productSpecifValues],characValue}							
					<td class="td_int_val">{characValue:e}</td>
					{end:}					
					
					<td class="td_select">
				      	<table border="0" cellspacing="0" cellpadding="0" align="center">
					        <tr>
					          <td>
								<a href="{sortDown}{spe[URLspecifLabel]}">
									<img class="button_img" src="{start_url}/templates/catalog/images/haut.gif" alt="Trier" />
								</a>
							  </td>
					          <td>
					          	<a href="{sortUp}{spe[URLspecifLabel]}">
									<img class="button_img" src="{start_url}/templates/catalog/images/bas.gif" alt="Trier" />
								</a>
							  </td>
							  <td>
					          	<a href="javascript:hide('charac{spe[specifID]}');"><img class="button_img" src="{start_url}/templates/catalog/images/afficher.gif" alt="masquer" /></a></td>
					          </td>
					        </tr>
				      	</table>
				     </td>
				</tr>				
		{end:}	
	
	<!-- Devis ou commande -->
    <tr>
      <td colspan="{productsAvailable}" class="sous_titre">Devis/commande</td>
    </tr>
    <!-- Poids -->
    <tr id="weight">
      <td class="td_intitule">Poids (kg) </td>
      {foreach:productsArray,pro}
      <td class="td_int_val">{pro[weight]}</td>
      {end:}
      <td class="td_gris">
      	<table border="0" cellspacing="0" cellpadding="0" align="center">
	        <tr>
	          <td><a href="{sortDown}weight"><img class="button_img" src="{start_url}/templates/catalog/images/haut.gif" alt="Trier" /></a></td>
	          <td><a href="{sortUp}weight"><img class="button_img" src="{start_url}/templates/catalog/images/bas.gif" alt="Trier" /></a></td>
	          <td><a href="javascript:hide('weight');"><img class="button_img" src="{start_url}/templates/catalog/images/afficher.gif" alt="masquer" /></a></td>
	        </tr>
      	</table>
      </td>
    </tr>
	<!-- Quantité par lot -->
    <tr id="lot">
      <td class="td_intitule">Quantit&eacute; par lot</td>
          {foreach:productsArray,pro}
			<td class="td_int_val">{pro[lot]}&nbsp;{pro[unit]:e}</td>
		  {end:}			
      <td class="td_gris">
      	<table border="0" cellspacing="0" cellpadding="0" align="center">
	        <tr>
	          <td><a href="{sortDown}lot"><img class="button_img" src="{start_url}/templates/catalog/images/haut.gif" alt="Trier" /></a></td>
	          <td><a href="{sortUp}lot"><img class="button_img" src="{start_url}/templates/catalog/images/bas.gif" alt="Trier" /></a></td>
	          <td><a href="javascript:hide('lot');"><img class="button_img" src="{start_url}/templates/catalog/images/afficher.gif" alt="masquer" /></a></td>
	        </tr>
	      </table>
	     </td>
    </tr>
    <!-- Délais de livraison -->
    <tr id="delivdelay" class="td_gris">
      <td class="td_intitule">Départ Usine</td>
        
        {foreach:productsArray,pro}
		<td class="td_int_val">{pro[delay]:e}</td>
		{end:}
		
      <td class="td_gris">
      	<table border="0" cellspacing="0" cellpadding="0" align="center">
	        <tr>
	          <td><a href="{sortDown}delivdelay"><img class="button_img" src="{start_url}/templates/catalog/images/haut.gif" alt="Trier" /></a></td>
	          <td><a href="{sortUp}delivdelay"><img class="button_img" src="{start_url}/templates/catalog/images/bas.gif" alt="Trier" /></a></td>
	          <td><a href="javascript:hide('delivdelay');"><img class="button_img" src="{start_url}/templates/catalog/images/afficher.gif" alt="masquer" /></a></td>
	        </tr>
      	</table>
      </td>
    </tr>


{if:isLogin}
{if:promo}					
		<!-- Prix initial -->
	    <tr id="basicprice">
	      <td class="td_intitule">Prix tarif</td>	
	      {foreach:productsArray,pro}	      			
		  <td class="td_int_val">{pro[basic_price]:h}</td>
		  {end:}
	      <td class="td_intitule"><table border="0" cellspacing="0" cellpadding="0" align="center">
	        <tr>
	          <td><a href="{sortDown}basicprice"><img class="button_img" src="{start_url}/www/icones/haut.gif"  alt="Trier" /></a></td>
	          <td><a href="{sortUp}basicprice"><img class="button_img" src="{start_url}/www/icones/bast.gif" alt="Trier"/></a></td>
	          <td><a href="javascript:hide('basicprice');"><img class="button_img" src="{start_url}/www/icones/afficher.gif" alt="masquer" /></a></td>
	        </tr>
	      </table></td>
	    </tr>
	  	<!-- Réduction -->
	    <tr id="reducerate">
	      <td class="td_intitule">R&eacute;duction</td>
	     	{foreach:productsArray,pro}		
				<td class="td_int_val">{pro[reduceRate]:h}</td>
		 	{end:}
	      <td class="td_intitule"><table border="0" cellspacing="0" cellpadding="0" align="center">
	        <tr>
	          <td><a href="{sortDown}reducerate"><img class="button_img" src="{start_url}/templates/catalog/images/haut.gif" alt="Trier" /></a></td>
	          <td><a href="{sortUp}reducerate"><img class="button_img" src="{start_url}/templates/catalog/images/bas.gif" alt="Trier" /></a></td>
	          <td><a href="javascript:hide('reducerate');"><img class="button_img" src="{start_url}/templates/catalog/images/afficher.gif" alt="masquer" /></a></td>
	        </tr>
	      </table></td>
	    </tr>	
	{end:}
	{end:}
	
	
<!-- Prix H.T. -->
    <tr id="wotprice">
      <td class="td_intitule">Prix HT</td>
     {foreach:productsArray,pro}     		
			{if:!pro[wotprice]}
				<a href="{start_url}/catalog/contact.php">{askUs}</a>
			{else:}			
				<td class="td_int_val">{pro[wotprice]:h}</td>
			{end:}
	 {end:}
      <td class="td_gris">
      	<table border="0" cellspacing="0" cellpadding="0" align="center">
	        <tr>
	          <td><a href="{sortDown}wotprice"><img class="button_img" src="{start_url}/templates/catalog/images/haut.gif" alt="Trier" /></a></td>
	          <td><a href="{sortUp}wotprice"><img class="button_img" src="{start_url}/templates/catalog/images/bas.gif" alt="Trier" /></a></td>
	          <td><a href="javascript:hide('wotprice');"><img class="button_img" src="{start_url}/templates/catalog/images/afficher.gif" alt="masquer" /></a></td>
	        </tr>
	      </table>
	     </td>
    </tr>	
	
	
	<form action="" name="mainform" id="mainform" method="POST" enctype="multipart/form-data">
     
    <input type="hidden" id="IdArticle" name="IdArticle" value="">
    <input type="hidden" id="quantity" name="quantity" value="">
	
	<!-- Commander. -->
    <tr>
      <td class="td_intitule">Commander</td>
      	
         {foreach:productsArray,pro}			
			{if:pro[wotprice]}			
			<td class="td_int_val">
			<!-- Formulaire Commande -->													
			<table border="0" cellspacing="3" cellpadding="0" align="center">
				<tr>
				<td>
					<input type="text" class="input" size="3" maxlength="4" id="orderQty{pro[i]}" value="{pro[min_cde]}" />   
				</td>
				<td>					          
					<input type="image" src="{start_url}/templates/catalog/images/btn_ok_grey.png" style="background:none;width: auto;height: auto;" alt="Commander" OnClick="document.getElementById('IdArticle').value={pro[idarticle]};document.getElementById('quantity').value=document.getElementById('orderQty{pro[i]}').value;document.mainform.action='/catalog/basket.php';document.mainform.submit();"/>
				</td>
				</tr>
			</table>		
			</td>
			{else:}				
			<td class="td_int_val">N/A</td>
			{end:}				
		{end:}   
		
     </tr>
     <!-- Devis. -->
    <tr>
      <td class="td_intitule">Devis</td>
   {foreach:productsArray,pro}   				
   		<td class="td_int_val">
	
		<table border="0" cellspacing="3" cellpadding="0" align="center">
		<tr>
		<td>
			<input type="text" class="input" size="3" maxlength="4" id="estimQty{pro[i]}" value="{pro[min_cde]}" />  
		</td>
		<td>	
			<input type="image" src="{start_url}/templates/catalog/images/btn_ok_orange.png" style="background:none;width: auto;height: auto;" alt="Commander" OnClick="document.getElementById('IdArticle').value={pro[idarticle]};document.getElementById('quantity').value=document.getElementById('estimQty{pro[i]}').value;document.mainform.action='/catalog/estimate_basket.php';document.mainform.submit();"/>	
		</td>
		</tr>
		</table>
		</td>
	{end:}
      <td></td>
     </tr>
     
     </form>
     
</table>
</div>

{else:}
	<p style="margin-top: 1em;">Le tableau comparatif ne contient aucun produit.</p>
	<p style="margin-top: 1em;">Pour comparer des produits, vous devez d'abord cocher la case correspondante dans la fiche produit de chaque article.</p>

{end:}


</div>
<!--====================================== Fin du contenu ======================================-->
</div>
</div>  

          
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
            


