
<script type="text/javascript">
	
	/* ---------------------------------------------------------------------------------- */
	
	 function getElementsById(sId)
	 {
	    var outArray = new Array();	
		if(typeof(sId)!='string' || !sId)
		{
			return outArray;
		};
		
		if(document.evaluate)
		{
			var xpathString = "//*[@id='" + sId.toString() + "']"
			var xpathResult = document.evaluate(xpathString, document, null, 0, null);
			while ((outArray[outArray.length] = xpathResult.iterateNext())) { }
			outArray.pop();
		}
		else if(document.all)
		{
			
			for(var i=0,j=document.all[sId].length;i<j;i+=1){
			outArray[i] =  document.all[sId][i];}
			
		}else if(document.getElementsByTagName)
		{
		
			var aEl = document.getElementsByTagName( '*' );	
			for(var i=0,j=aEl.length;i<j;i+=1){
			
				if(aEl[i].id == sId )
				{
					outArray.push(aEl[i]);
				};
			};	
			
		};
		
		return outArray;
	 }

</script>



<!-- Affichage colonne poids? -->
{Math.counter.reset()}
<ul style='display:none;'>
    <li flexy:foreach="product.getArticles(),index,article">
    {if:article.getWeight()}
       {Math.counter.inc()}
    {end:}
    </li>
</ul><!--
{if:!Math.counter.value()}
<style type="text/css">   
    .noneWeight { display:none; }   
</style>
{end:}-->

<script type="text/javascript">
<!--
	
	 function getElementsById(sId)
	 {
	    var outArray = new Array();	
		if(typeof(sId)!='string' || !sId)
		{
			return outArray;
		};
		
		if(document.evaluate)
		{
			var xpathString = "//*[@id='" + sId.toString() + "']"
			var xpathResult = document.evaluate(xpathString, document, null, 0, null);
			while ((outArray[outArray.length] = xpathResult.iterateNext())) { }
			outArray.pop();
		}
		else if(document.all)
		{
			
			for(var i=0,j=document.all[sId].length;i<j;i+=1){
			outArray[i] =  document.all[sId][i];}
			
		}else if(document.getElementsByTagName)
		{
		
			var aEl = document.getElementsByTagName( '*' );	
			for(var i=0,j=aEl.length;i<j;i+=1){
			
				if(aEl[i].id == sId )
				{
					outArray.push(aEl[i]);
				};
			};	
			
		};
		
		return outArray;
	 }

-->
</script>
 {if:iduser}<span style="color:red;font-weight: bold;">/product/detail_2.tpl</span>{end:}
  
 <div class="infostech" style="margin:0;padding:0;">
        <!--<div class="ombretabD"></div>-->
        <div class="data" style="margin:0;padding:0;" flexy:foreach="product.getArticleTables(),tableId,Articles"> 
  	<div id="SelectionRef" >
<div style="color:#fe7b00;; font-size: 12px; font-weight: bold;">
		<span flexy:if="product.get(#table_name_1#)"> {product.get(#table_name_1#)}</span>
</div>
		{Math.counter.reset(#-1#)}


	<table>

	<!-- R&eacute;f&eacute;rence -->
	<tr class="tb_head">
		<th class="G">
			<strong>R&eacute;f&eacute;rence</strong>
		</th>
		<th flexy:foreach="product.getArticles(),article">
	  		<strong>
		  		{if:iduser}
		  			<a id="Link" href="{baseURL}/product_management/catalog/cat_product.php?idproduct={product.getId()}">{article.get(#reference#)}</a>
		  		{else:}
		  			{article.get(#reference#)}
		  		{end:}
		  	</strong>
		</th>
	</tr>
	<tr flexy:if="product.getPromotionCount()">
		<th class="tb_head">Prix unitaire HT</th>
		<td class="caracts" flexy:foreach="product.getArticles(),article">
			{if:article.getDiscountRate()}
				<del>{article.getCeilingPriceET():h}</del>
			{else:}
				{article.getPriceET():h}
			{end:}
		</td>
	</tr>
	
	<!--<tr flexy:if="product.getPromotionCount()">
		<th class="tb_head">Prix unitaire TTC</th>
		<td class="caracts" flexy:foreach="product.getArticles(),article">
			{if:article.getDiscountRate()}
				<del>{article.getCeilingPriceATI():h}</del>
			{else:}
				{article.getPriceATI():h}
			{end:}
		</td>
	</tr>-->
	
	
	<tr flexy:if="product.getPromotionCount()">
		<th class="intitule">Remise</th>
		<td class="priceCaracts" flexy:foreach="product.getArticles(),article">
			{if:article.getDiscountRate()}
				{article.getDiscountRate():h} %
			{else:}
				-
			{end:}
		</td>
	</tr>

	{if:product.getHasPriceRanges()}
		<!-- Remises au volume -->
		<tr>
			<th class="intitule">Remises sur volume</th>
			<td class="caracts" flexy:foreach="product.getArticles(),article">
				{if:article.getPriceRanges()}
                    <!-- début de modification -->
                    <span flexy:foreach="article.getPriceRanges(),range">
					      {range.min_quantity} &agrave; {range.max_quantity} : {range.rate}%<br>
                    </span> 
                    <!-- fin de modification -->   
				{else:}
					- 
				{end:}
			</td>
		</tr>
	{end:}
			
	<!-- Prix -->
	<tr>
		<th class="priceIntitule">
			{if:product.getPromotionCount()}
				Prix HT remis&eacute;
			{else:}
				Prix unitaire HT
			{end:}
		</th>
		<td class="priceCaracts" flexy:foreach="product.getArticles(),article">
			{if:article.getPriceET()}
				{article.getPriceET():h}
			{else:}
				Contactez-nous
			{end:}
		</td>
	</tr>
	
	<!--<tr>
		<th class="priceIntitule">
			{if:product.getPromotionCount()}
				Prix TTC remis&eacute;
			{else:}
				Prix unitaire TTC
			{end:}
		</th>
		<td class="priceCaracts" flexy:foreach="product.getArticles(),article">
			{if:article.getPriceATI()}
				{article.getDiscountPriceATI():h}
			{else:}
				Contactez-nous
			{end:}
		</td>
	</tr>-->
	<!-- Intitules -->
	<tr flexy:foreach="product.getHeadings(),heading">
		<th class="intitule">{heading.name:h} <span flexy:if="heading.cotation">({heading.cotation})</span></th>
		<td class="caracts" flexy:foreach="product.getArticles(),article">{article.getHeadingValue(heading.id):h}</td>	
	</tr>
	
	<!-- Poids -->
	<tr>
    	{if:!Math.counter.value()}
        	<th class="intitule noneWeight"  style="display: none;" >Poids (kg)</th>
            <td class="caracts noneWeight" style="display: none;" flexy:foreach="product.getArticles(),article"><strong>{article.getWeight():h}</strong></td>
        {else:}
        	<th class="intitule noneWeight"  >Poids (kg)</th>
            <td class="caracts noneWeight" flexy:foreach="product.getArticles(),article"><strong>{article.getWeight():h}</strong></td>
        {end:}
	</tr>
	
	<!-- D&eacute;lai -->
	<tr>
		<th class="intitule">D&eacute;part usine</th>
		<td class="caracts" flexy:foreach="product.getArticles(),article"><strong>{article.getDeliveryTime():h}</strong></td>
	</tr>
	
	<!-- Quantit&eacute; minimum -->
	<tr flexy:if="product.getHasMinimumOrder()">
		<th class="intitule">Quantit&eacute; minimum</th>
		<td class="caracts" flexy:foreach="product.getArticles(),article">
			{if:article.get(#min_cde#)}
				{article.get(#min_cde#):h}
			{else:}
				-
			{end:}
		</td>
	</tr>

	
	
	<!-- Comparateur -->
	<tr>
		<th class="intitule">
			<a href="{baseURL}/catalog/product_compare.php" class="compare">Cochez et comparer</a>
		</th>
		<td class="topCenter" flexy:foreach="product.getArticles(),article">
	  		<form action="{baseURL}/catalog/compare_func.php" method="post" enctype="multipart/form-data">
		  		<p>
		  			<input type="hidden" name="IdProduct" value="{article.getId()}" flexy:ignore="yes">
		  			<input id="tmpId{article.getId()}" style="margin-left: 80px;" name="incompare[]" value="{article.getId()}" type="checkbox" onClick="form.submit();" class="inputRef" flexy:ignore="yes">
		  		</p>
	  		</form>
		</td>
	</tr>
	
	<tr>
		<th class="intitule">Quantit&eacute;</th>
		<td class="caractsL" flexy:foreach="product.getArticles(),article">
			<input class="quantity qtt2" type="text" id="quantity{article.getId()}" value="{article.get(#min_cde#)}" style="width:58px; height:30px; padding:8px;" flexy:ignore="yes">
		</td>
	</tr>

	<!-- Commander si il est dispo-->
	{if:allowOrder}
		<tr>
			<th class="intitule">Commander</th>
			<td class="caractsL" flexy:foreach="product.getArticles(),article">
				{if:article.getPriceET()}
                	{ob_start()}{article.get(#stock_level#)}{ob_end()}
                    {if:Math.equal(ob_content,0)}
                    	Epuis&eacute;
                	{else:}
                        <p>
                            <input class="input_commander_small cmde1 btn-big"  type="button" name="AddCde" onClick="addToCart( '{article.getId()}', document.getElementById('quantity{article.getId()}').value, 'order' )" value="Ajouter" flexy:ignore="yes">
                        </p>
                	{end:}
				{else:}
					-
				{end:}
			</td>
		</tr>
	{end:}
	
	<!-- Demande de devis -->
	<tr>
		<th class="intitule">Demander un devis</th>
		<td class="caractsL" flexy:foreach="product.getArticles(),article">
			<p>
				<input class="input_devis_small btn-big devis1" type="button" name="AddDevis" onClick="addToCart( '{article.getId()}', document.getElementById('quantity{article.getId()}').value, 'estimate' )" flexy:ignore="yes" value="Devis">
			</p>
		</td>
	</tr>
	
</table>
			
		</div>
	</div>
</div>
