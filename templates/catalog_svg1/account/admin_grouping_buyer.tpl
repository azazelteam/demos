{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
        <link rel="stylesheet" type="text/css" href="{baseURL}/css/catalog/admin_grouping.css" />
        <link rel="stylesheet" type="text/css" href="{baseURL}/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
	<!--</head>
	<body>-->
	<!-- Header -->

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/admin_grouping_buyer.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
				<h2>Liste des clients du groupement <strong>{buyerGroup}</strong></h2>
                
                <br>
				{form:h}
		
				<br><br><br>
                
				<form id="searchform" name="searchform" action="" method="post">				
				<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th>n°</td>
                <th></td>
				<th>N° du client</th>
                <th>Contact</th>
                <th>Société</th>
                <th>Nbre devis</th>
                <th>Nbre commandes</th>
                <th>Nbre factures</th>
                
			</tr>
			{foreach:array_buyer,line}
				<tr>
					<td>{line[i]:h}</td>
                    <td><input type="checkbox" style="width: 15px; height: 15px;" name="list_buyer[]" value="{line[idbuyer]:h}"></td>
					<td>{line[idbuyer]:h}</td>
                    <td>{line[name]:h}</td>
                    <td>{line[company]:h}</td>
                    <td>{line[num_estimate]:h}</td>
                    <td>{line[num_order]:h}</td>
                    <td>{line[num_invoice]:h}</td>
				</tr>
			{end:}
            <tr>
			<input type="hidden" id="baseurl" name="baseurl" value="{baseURL}">
            <td colspan="6" align="right"><input type="button" onClick="ClicBoutonEstimate();" value="Voir devis"></td>
            <td><input type="button" onClick="ClicBoutonOrder();" value="Voir commandes"></td>
            <td><input type="button" onClick="ClicBoutonInvoice();" value="Voir factures"></td>
            </tr>			
			</table>		
			</form>
		

				<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
		
	</div>
	</div>
	</div>
	<flexy:include src="foot.tpl"></flexy:include>

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}

<script type="text/javascript">
   function ClicBoutonEstimate(){
	   var baseurl = document.getElementById("baseurl").value;
	   
	   document.getElementById( 'searchform' ).action = baseurl+"/catalog/admin_grouping_estimate.php?all";
	   document.getElementById("searchform").submit();
      
   }
   function ClicBoutonOrder(){
       var baseurl = document.getElementById("baseurl").value;
	   
	   document.getElementById( 'searchform' ).action = baseurl+"/catalog/admin_grouping_order.php?all";
	   document.getElementById("searchform").submit();
   }
   function ClicBoutonInvoice(){
       var baseurl = document.getElementById("baseurl").value;
	  
	   document.getElementById( 'searchform' ).action = baseurl+"/catalog/admin_grouping_invoice.php?all";
	   document.getElementById("searchform").submit();
   }
</script> 