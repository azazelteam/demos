<flexy:include src="/head/header.tpl"></flexy:include>


<!--====================================== Contenu ======================================-->

<!--<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/buyer.css" />-->
	<!-- Header -->
	


	<div id="content" class="account">
    {if:iduser}<span style="color:red;font-weight: bold;">/account/buyer_infos.tpl</span>{end:}
		<div id="leftbloc">
			<flexy:include src="menu/menu_compte.tpl"></flexy:include>
        </div>		
	
<!--<div id="center" class="page">-->
		<div id="categorybloc" class="account">
            <div class="intro stretch-down">


	
<p class="msgError">
{message:h}
</p>
	
<form id="buyer_info" action="{start_url}/catalog/buyer_infos.php" method="post" onSubmit="javascript:return verifForm(this) && checkDifferentAddresses( this )">
<table border="0" style="width:100%">
	<tr>
		<td valign="top" style="width:50%">
			<h1 style="font-size:14px;width:auto;">Informations personnelles</h1><br style="clear:both;"/><br style="clear:both;"/>
			<table cellspacing="0" style="width:100%">
				<tr>
					<td class="paddingTD" ><label>Civilité</label>{contactInfos[title]:h}</td>
				</tr>
					<tr>
					<td class="paddingTD"><label>Nom <span class="ast_orange">*</span></label><input type="text" size="19" name="lastname" value="{contactInfos[lastname]}" maxlength="20"></td>
				</tr>
				<tr>
					<td class="paddingTD"><label>Prénom</label><input type="text" size="19" name="firstname" value="{contactInfos[firstname]}" maxlength="20"></td>
				</tr>
				<tr>
        			<td class="paddingTD"><label>Adresse e-mail <span class="ast_orange">*</span></label><input size="19" type="text" name="email" value="{contactInfos[email]}" maxlength="50"></td>
   				</tr>
   				<tr>
        			<td class="paddingTD"><label>Téléphone <span class="ast_orange">*</span></label><input size="19" type="text" name="phonenumber" value="{contactInfos[phonenumber]}" maxlength="20"></td>     
    			</tr>
    			<tr>
        			<td class="paddingTD"><label>GSM</label><input size="19" type="text" name="gsm" value="{contactInfos[gsm]}" maxlength="20"></td>     
    			</tr>
    			<tr>
        			<td class="paddingTD"><label>Fax</label><input size="19" type="text" name="faxnumber" value="{contactInfos[faxnumber]}" maxlength="20"></td> 
    			</tr>
    			<tr>         
        			<th class="LibInput" align="left"></th>
        			
    			</tr>	
                <tr>         
        			
        			<td class="paddingTD fonctionuser" style="width:264px;"><label>Fonction</label>{contactInfos[department]:h}</td>
    			</tr>					    
		</table>
		</td>
        
		<td valign="top" >
		<h1 style="font-size:14px;width:auto;">Informations société</h1><br style="clear:both;"><br style="clear:both;"/>
			<table class="tableInputBuyer" cellspacing="0">
				<tr class="activityuser">
					<td><label>Profil</label>{buyer_infos[profile]:h}</td>
				</tr>
    			<tr>
      				<td><label>Raison sociale</label><input type="text" size="26" name="company" value="{buyer_infos[company]}" maxlength="50"></td>
				</tr>
                <tr class="activityuser">		
					
					<td><label>Activité de l'entreprise</label>{buyer_infos[activity]:h}</td>
				</tr>
                <tr>
	    			
     				<td><label>Adress</label><input size="26" type="text" name="adress" value="{buyer_infos[adress]:e}" maxlength="100"></td>
	 			</tr>
                <tr>
	    			<td><label>Adresse complémentaire</label><input size="26" type="text" name="adress_2" value="{buyer_infos[adress_2]:e}" maxlength="100"></td>
   				</tr>
                <tr>
					<td><label>Code postal <span class="ast_orange">*</span></label><input size="26" type="text" name="zipcode" value="{buyer_infos[zipcode]}" maxlength="10"></td>
				</tr>
                <tr>
  	    			<td><label>Ville</label><input type="text" size="26" name="city" value="{buyer_infos[city]}" maxlength="50"></td>
    			</tr>
    			<tr>
  	    			<td class="stateuser"><label>Pays <span class="ast_orange">*</span></label>{buyer_infos[state]:h}</td>
	 			</tr>
	 			<tr>
  	    			<td><label>Téléphone</label><input type="text" size="26" name="phone_standard" value="{buyer_infos[phone_standard]}" maxlength="20"></td>
    			</tr>
   				<tr>
      				<td><label>TVA Intracommunautaire</label><input type="text"size="26"  name="tva" value="{buyer_infos[tva]}" maxlength="30"></td>
				</tr>
				<tr>
        			<td><label>SIRET</label><input type="text" size="26" name="siret" value="{buyer_infos[siret]}" maxlength="20"></td>
 				</tr>
 				<tr>
   				    <td><label>NAF</label><input size="26" type="text" name="naf" value="{buyer_infos[naf]}" maxlength="15"></td>
				</tr>	
		</table>
		</td>
	</tr>
</table>
<div style="margin-top: 20px;margin-bottom:30px;">
<input type="hidden" name="user_changed" value="1" style="width: 78px; height:22px;" >
<input type="submit" style="margin-right: 250px;float:left;" border="0" name="Order"  />
<a href="{start_url}/catalog/account.php" style="width: 120px;margin-right: 30px;text-align: center;padding: 0.4em 1em 0.3em; border-radius: 14px 14px 14px 14px; background: none repeat scroll 0px 0px rgb(239, 137, 6); color: rgb(255, 255, 255); text-decoration: none; display: block; float: right; height: 22px; line-height: 20px; box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);">Retour</a>  
</div>
</form>
	
</div>
</div>
</div>
</div>
	
	</div>	
<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 



		