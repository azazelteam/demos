<flexy:include src="/head/header.tpl"></flexy:include>
<!--
<flexy:include src="b_left_cpt.tpl"></flexy:include>-->
<!--====================================== Contenu ======================================-->
<div id="center" class="page">
{if:iduser}<span style="color:red;font-weight: bold;">/account/buyer_password.tpl</span>{end:}
{if:isLogin} 
	
	<script type="text/javascript" language="javascript">
	<!--	
		function checkform( form ){
			
			if( form.pwd.value == "") {
  				alert('Le champ Mot de passe est vide !!!');
  				return false;
  			}else{
  				if( form.pwd.value.length < 6) {
  					alert('Le champ Mot de passe doit comporter au minimum 6 caractères !!!');
  					return false;
  				}
  			}
  	
  			if( form.pwd_confirm.value == "") {
  				alert('Le champ confirmation mot de passe est vide !!!');
  				return false;
  			}else{
  				if( form.pwd_confirm.value != form.pwd.value) {
  					alert('Le champ confirmation de mot de passe doit être identique au champ Mot de passe !!!');
  					return false;
  				}	
  			}
			
			return true;			
		}		
	// -->
	</script>
	
<link type="text/css" rel="stylesheet" href="{baseURL}/templates/catalog/css/buyer.css" />

<h3>Modification de votre mot de passe</h3>
<br />
<br />
<span class="txt_oblg">Tous les champs marqu&eacute;s d'un <span class="ast_orange">*</span> sont à remplir obligatoirement</span>
<br />
<br />
<br />
	<div>
	<center>
	<form action="" method="post" onsubmit="javascript:return checkform( this )">
	<table class="tableInputBuyer">
		{if:message}
			<tr>
			 	<td colspan="2" style='text-align:center;color:#FF0000;font-weight:bold;  vertical-align:top; text-align:center;'>{message}<br /><br /></td>
			</tr>
		{end:}
		<tr>
			<td class="LibInput" style="vertical-align:top;">
				Ancien mot de passe <span class="ast_orange">*</span>
			</td>
			<td  class="tdPassword" >
				<input type="password" name="old_pwd" class="AuthInput" maxlength="20" size="40" />
			</td>
		</tr>
		<!--<tr>	
			<td class="tdCommentaire" colspan="2"><span class="SubLibInput">Votre ancien mot de passe</span></td>
		</tr>-->
		<tr>
			<td class="LibInput" style="vertical-align:top; text-align:left;">
			Nouveau mot de passe <span class="ast_orange">*</span></td>
			<td rowspan="2" style="vertical-align:top; text-align:left;"><input type="password" name="pwd" class="AuthInput" maxlength="20" size="40"/></td>
		</tr>
		<tr>
			<td class="tdCommentaire" colspan="2"><span class="SubLibInput">6 caractères minimum</span></td>
		</tr>
		<tr>
			<td class="LibInput" style="white-space:nowrap; vertical-align:top;">Confirmation mot de passe <span class="ast_orange">*</span></td>
			<td rowspan="2" style="vertical-align:top; text-align:left;"><input type="password" name="pwd_confirm" class="AuthInput" maxlength="20" size="40"/></td>
		</tr>
		<tr>	
			<td class="tdCommentaire" colspan="2"><span class="SubLibInput">Veuillez confirmer le mot <br>de  passe</span></td>
		</tr>
   	</table>
	<p style="float: left; margin-left: 35%; margin-top: 20px; text-align: center; width: 30%;"><a href="http://www.abisco.fr/catalog/account.php" style="background: none repeat scroll 0px 0px rgb(239, 137, 6); text-decoration: none; height: 30px; padding: 0px 10px; margin: 0px 10px; line-height: 30px; color: rgb(255, 255, 255); float: right; border-radius: 14px 14px 14px 14px; box-shadow: 1px 1px 2px rgba(0, 0, 0, 0.2);">Retour</a><input type="submit" border="0" name="Modify" style="width: auto; float: left;" value="Modifier"></p>
	</form>
	
	</center>
	</div>
	{else:}	
		<flexy:include src="login.tpl"></flexy:include>
	{end:}		
</div>
</div>
</div>

<!--====================================== Fin du contenu ======================================-->
<!-- Pied de page -->
<flexy:include src="foot.tpl"></flexy:include> 
            
