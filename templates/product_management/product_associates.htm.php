<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Options
 */

include_once( "../../objects/Util.php" );

if( !count( $_FILES ) ){ //@todo : bug plugin Ajax?? l'encodage est différent selon que des fichiers soient postés ou non

	$_POST 		= Util::arrayMapRecursive( "Util::doNothing", $_POST );
	$_REQUEST 	= Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	$_GET 		= Util::arrayMapRecursive( "Util::doNothing", $_GET );

}

//--------------------------------------------------------------------------------------------------------
/* mise à jour des pages */

if( isset( $_REQUEST[ "pages" ] ) && isset( $_REQUEST[ "idproduct" ] ) ){

	include_once( "../../config/init.php" );

	$pages = explode( ",", $_REQUEST[ "pages" ] );
	// var_dump($pages);exit;

	/* pages déselectionnés */

	$query = "
	DELETE FROM page_product 
	WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'";

	if( count( $pages ) )
		$query .= " AND idpage NOT IN( " . $_REQUEST[ "pages" ] . " )";

	DBUtil::query( $query );

	/* nouveaux pages sélectionnés */

	$rs =& DBUtil::query( "SELECT DISTINCT idpage FROM page_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'" );
	$currentPages = array();

	while( !$rs->EOF() ){

		$currentPages[] = $rs->fields( "idpage" );
		$rs->MoveNext();

	}
	
	foreach( $pages as $idpage ){

		if( !in_array( $idpage, $currentPages ) ){

			// $rs =& DBUtil::query( "SELECT MAX( display ) + 1 AS display FROM page_product WHERE idpage = '$idpage'" );

			// $display = $rs->fields( "display" ) == NULL ? 1 : 0;

			$query = "
			INSERT INTO page_product( idpage, idproduct, lastupdate, username )
			VALUES(
				'$idpage',
				'" . intval( $_REQUEST[ "idproduct" ] ) . "',
				NOW(),
				'" . Util::html_escape( User::getInstance()->get( "login" ) ) . "'
			)";

			DBUtil::query( $query );
		
		}
	
	}

	exit( "1" );

}

//--------------------------------------------------------------------------------------------------------
/* suppression d'une page */

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "idpage" ] ) ){
	
	include_once( "../../config/init.php" );
	
	$rs =& DBUtil::query( "DELETE FROM page_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND idpage = '" . intval( $_REQUEST[ "idpage" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* mise à jour des métiers */

if( isset( $_REQUEST[ "trades" ] ) && isset( $_REQUEST[ "idproduct" ] ) ){

	include_once( "../../config/init.php" );

	$trades = explode( ",", $_REQUEST[ "trades" ] );

	/* métiers déselectionnés */

	$query = "
	DELETE FROM trade_product 
	WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'";

	if( count( $trades ) )
		$query .= " AND idtrade NOT IN( " . $_REQUEST[ "trades" ] . " )";

	DBUtil::query( $query );

	/* nouveaux métiers sélectionnés */

	$rs =& DBUtil::query( "SELECT DISTINCT idtrade FROM trade_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'" );
	$currentTrades = array();

	while( !$rs->EOF() ){

		$currentTrades[] = $rs->fields( "idtrade" );
		$rs->MoveNext();

	}

	foreach( $trades as $idtrade ){

		if( !in_array( $idtrade, $currentTrades ) ){

			$rs =& DBUtil::query( "SELECT MAX( display ) + 1 AS display FROM trade_product WHERE idtrade = '$idtrade'" );

			$display = $rs->fields( "display" ) == NULL ? 1 : 0;

			$query = "
			INSERT INTO trade_product( idtrade, idproduct, display, lastupdate, username )
			VALUES(
				'$idtrade',
				'" . intval( $_REQUEST[ "idproduct" ] ) . "',
				'$display',
				NOW(),
				'" . Util::html_escape( User::getInstance()->get( "login" ) ) . "'
			)";

			DBUtil::query( $query );
		
		}
	
	}

	exit( "1" );

}

//--------------------------------------------------------------------------------------------------------
/* suppression d'un métier */

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "idtrade" ] ) ){
	
	include_once( "../../config/init.php" );
	
	$rs =& DBUtil::query( "DELETE FROM trade_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND idtrade = '" . intval( $_REQUEST[ "idtrade" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* sélection d'une famille de produits similaires */

if( isset( $_REQUEST[ "idproduct_similar_1" ] ) ){

	include_once( "../../config/init.php" );

	$query = "
	SELECT MAX( similar_1_order ) + 1 AS similar_1_order
	FROM product
	WHERE idproduct_similar_1 = '" . intval( $_REQUEST[ "idproduct_similar_1" ] ) . "'";
	
	$rs =& DBUtil::query( $query );
	
	$similar_1_order = $rs->fields( "similar_1_order" ) == NULL ? 0 : $rs->fields( "similar_1_order" );
	
	$query = "
	UPDATE product 
	SET idproduct_similar_1 = '" . intval( $_REQUEST[ "idproduct_similar_1" ] ) . "', similar_1_order = '$similar_1_order' 
	WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' 
	LIMIT 1";

	DBUtil::query( $query );
	exit();
		
}

//--------------------------------------------------------------------------------------------------------
/* tri des produits associés */

if( isset( $_POST[ "sort" ] ) && isset( $_POST[ "Associates" ] ) ){

	include_once( "../../config/init.php" );
	
	$done = true;
	$i = 0;
	while( $i < count( $_POST[ "Associates" ] ) ){
		
		$idproduct = intval( $_POST[ "Associates" ][ $i ] );
		$rs =& DBUtil::query( "UPDATE associat_product SET displayorder = '$i' WHERE idproduct = '" . intval( $_POST[ "idproduct" ] ) . "' AND associat_product = '$idproduct' LIMIT 1" );

		$done &= $rs !== false;
		
		$i++;
		
	}
	
	exit( $done ? "1" : "0" );

}

//--------------------------------------------------------------------------------------------------------
/* tri des produits complémentaires */

if( isset( $_POST[ "sort" ] ) && isset( $_POST[ "Complementes" ] ) ){

	include_once( "../../config/init.php" );
	
	$done = true;
	$i = 0;
	while( $i < count( $_POST[ "Complementes" ] ) ){
		
		$idproduct = intval( $_POST[ "Complementes" ][ $i ] );
		$rs =& DBUtil::query( "UPDATE complement_product SET displayorder = '$i' WHERE idproduct = '" . intval( $_POST[ "idproduct" ] ) . "' AND complement_product = '$idproduct' LIMIT 1" );

		$done &= $rs !== false;
		
		$i++;
		
	}
	
	exit( $done ? "1" : "0" );

}

//--------------------------------------------------------------------------------------------------------
/* tri des produits similaires */

if( isset( $_POST[ "sort" ] ) && isset( $_POST[ "Similares" ] ) ){

	include_once( "../../config/init.php" );
	
	$done = true;
	$i = 0;
	while( $i < count( $_POST[ "Similares" ] ) ){

		$idproduct = intval( $_POST[ "Similares" ][ $i ] );
		$rs =& DBUtil::query( "UPDATE product SET similar_1_order = '$i' WHERE idproduct = '$idproduct' LIMIT 1" );

		$done &= $rs !== false;
		
		$i++;
		
	}
	
	exit( $done ? "1" : "0" );

}

//--------------------------------------------------------------------------------------------------------
/* ajout d'un produit associé */

if( isset( $_REQUEST[ "create" ] ) && isset( $_REQUEST[ "associates" ] ) ){

	include_once( "../../config/init.php" );
	
	$lang = User::getInstance()->getLang();

	/* vérifier si l'option existe */
	
	if( !DBUtil::query( "SELECT idproduct FROM product WHERE idproduct = '" . intval( $_REQUEST[ "associat_product" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( 0 );
	
	/* ajouter l'option */
	
	$displayOrder = DBUtil::query( "SELECT MAX( displayorder ) + 1 AS displayorder FROM associat_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'" )->fields( "displayorder" ) || 1;

	$query = "
	INSERT IGNORE INTO associat_product(
		idproduct, 
		associat_product, 
		displayorder, 
		lastupdate, 
		username
	) VALUES (
		'" . intval( $_REQUEST[ "idproduct" ] ) . "',
		'" . intval( $_REQUEST[ "associat_product" ] ) . "',
		'$displayOrder',
		NOW(),
		'" . Util::html_escape( User::getInstance()->get( "login" ) ) . "'
	)";
	
	DBUtil::query( $query );
	
	exit( "1" );
	
}

if( isset( $_REQUEST[ "create" ] ) && isset( $_REQUEST[ "complements" ] ) ){

	include_once( "../../config/init.php" );
	
	$lang = User::getInstance()->getLang();

	/* vérifier si l'option existe */
	
	if( !DBUtil::query( "SELECT idproduct FROM product WHERE idproduct = '" . intval( $_REQUEST[ "complement_product" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( 0 );
	
	/* ajouter l'option */
	
	$displayOrder = DBUtil::query( "SELECT MAX( displayorder ) + 1 AS displayorder FROM complement_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'" )->fields( "displayorder" ) || 1;

	$query = "
	INSERT IGNORE INTO complement_product(
		idproduct, 
		complement_product, 
		displayorder, 
		lastupdate, 
		username
	) VALUES (
		'" . intval( $_REQUEST[ "idproduct" ] ) . "',
		'" . intval( $_REQUEST[ "complement_product" ] ) . "',
		'$displayOrder',
		NOW(),
		'" . Util::html_escape( User::getInstance()->get( "login" ) ) . "'
	)";
	
	DBUtil::query( $query );
	
	exit( "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* suppression d'un produit associé */

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "associates" ] ) ){

	include_once( "../../config/init.php" );
	
	$rs =& DBUtil::query( "DELETE FROM associat_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND associat_product = '" . intval( $_REQUEST[ "associated" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* suppression d'un produit complémentaire */

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "complements" ] ) ){

	include_once( "../../config/init.php" );
	
	$rs =& DBUtil::query( "DELETE FROM complement_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND complement_product = '" . intval( $_REQUEST[ "associated" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* suppression d'un produit similaire */

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "similares" ] ) ){

	include_once( "../../config/init.php" );
	
	$rs =& DBUtil::query( "UPDATE product SET idproduct_similar_1 = 0, similar_1_order = 0 WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );

//---------------------------------------------------------------------------------------------

?>
<style type="text/css">
		
	#AssociateList, #SimilareList, #PageList, #TradeList, #AvailableTradeList, .relatedArticleList{
		
		margin: 20px auto 20px auto;
		list-style-type: none;
		padding: 0px;
		
	}
	
	#AvailableTradeList li, #AssociateList li, #SimilareList li, .relatedArticleList li{

		margin-bottom: 0px;
		margin-top: 4px;
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		margin:4px;
		
	}





    #TradeList li div{
        border:1px solid #E7E7E7;
        margin:4px;
    }
    #TradeList li{
        margin-bottom: 0px;
        margin-top: 4px;
        width:75px;
        height:80px;
        float:left;
        display:block;
        margin:4px;
    }
    #TradeList li span{
        height: 20px;
        display:block;
    }

	#AssociateList .placeholder, #SimilareList .placeholder, relatedArticleList .placeholder{
		
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		background-color:#F3F3F3;
		
	}
	
	#AssociateList li a, #SimilareList li a, .relatedArticleList li a{ cursor:move; }
	
	#ComplementList {
		
		margin: 20px auto 20px auto;
		list-style-type: none;
		padding: 0px;
		
	}
	
	#ComplementList li{
		
		margin-bottom: 0px;
		margin-top: 4px;
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		margin:4px;
		
	}

	#ComplementList .placeholder{
		
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		background-color:#F3F3F3;
		
	}
	
	#ComplementList li a{ cursor:move; }
	
	#PageList li {
		display: inline-block;
		width: auto!important;
		height: auto!important;
	}
	#AvailablePageList li{
		margin-bottom: 0px;
		margin-top: 4px;
		width: 30%;
		height: auto;
		float: left;
		margin: 4px;
		min-height: 30px;
		list-style : none;
	}
	
	#AvailablePageList li div {
		display: inline-block;
	}

	#AvailablePageList li div a {
		float: left;
		width: 130px;
		margin-left: 3px;
	}
	#PageDialog{padding:15px 5px 5px 5px;}
	
</style>
<script type="text/javascript">
/* <!CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
	/* inititalisation tri des produits associés */
	
	$(document).ready(function(){
	
		$("#AssociateList").sortable({
					
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			update: function(event, ui){ sortAssociateItems( $("#AssociateList").sortable('serialize') ); }
		
		});
		
		$("#AssociateList").disableSelection();
		
		
		$("#ComplementList").sortable({
					
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			update: function(event, ui){ sortComplementedItems( $("#ComplementList").sortable('serialize') ); }
		
		});
		
		$("#ComplementList").disableSelection();
		
		$("#SimilareList").sortable({
					
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			update: function(event, ui){ sortSimilareItems( $("#SimilareList").sortable('serialize') ); }
		
		});
		
		$("#SimilareList").disableSelection();
		
	});
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function deleteRelatedArticle( idarticle, idrelated_article ){

		$.ajax({

			url: "/templates/product_management/product_associates.htm.php?delete=related_article",
			async: true,
			type: "POST",
			data: "idarticle=" + idarticle + "&idrelated_article=" + idrelated_article,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load",6 );
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function deleteSimilare( idproduct ){
	
		var data = "idproduct=" + idproduct;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php?delete&similares",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load", 6 );
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function deleteAssociatedProduct( idproduct ){
	
		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&associated=" + idproduct;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php?delete&associates",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load",6 );
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function sortAssociateItems( serializedString ){
	
		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&sort=1&" + serializedString;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function createAssociatedProduct(){

		if( !$( "#AssociatedProduct" ).attr( "value" ).length )
			return;

		var postdata = "associat_product=" + escape( $( "#AssociatedProduct" ).attr( "value" ) );
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_associates.htm.php?create&associates",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer l'option" ); },
		 	success: function( responseText ){

				if( responseText == "0" )
					alert( "Impossible de créer l'option" );
				else $( "#ProductTabs" ).tabs( "load", 6 );
				
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function deleteComplementedProduct( idproduct ){
	
		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&associated=" + idproduct;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php?delete&complements",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load", 6);
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function sortComplementedItems( serializedString ){
	
		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&sort=1&" + serializedString;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function sortSimilareItems( serializedString ){

		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&sort=1&" + serializedString;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function createComplementedProduct(){

		if( !$( "#ComplementedProduct" ).attr( "value" ).length )
			return;

		var postdata = "complement_product=" + escape( $( "#ComplementedProduct" ).attr( "value" ) );
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_associates.htm.php?create&complements",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer l'option" ); },
		 	success: function( responseText ){

				if( responseText == "0" )
					alert( "Impossible de créer l'option" );
				else $( "#ProductTabs" ).tabs( "load", 6 );
				
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	
	
/* ]]> */
</script>
<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Accessoires</p>
</div>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_associates.htm.php</span>
<?php } ?>

<p style="margin:10px 0px;">
	N° du produit : 
	<input type="text" id="AssociatedProduct" name="" value="" class="textInput" onkeydown="if( event.keyCode == 13 ){ $( this ).bind( 'keyup', function(e){createAssociatedProduct();} ); return false; }" style="width:250px;" />
	<?php
	
		include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
		//AutoCompletor::completeFromDB( "AssociatedProduct", "product", "idproduct", 15, "createAssociatedProduct" ); 
		AutoCompletor::completeFromDBPattern( "AssociatedProduct", "product", "idproduct", "{name_1}", 15, "createAssociatedProduct" ); 
	
	?>
	<input type="button" name="" value="Créer" class="blueButton" onclick="createAssociatedProduct();" />
</p>
<p>
	<ul style="margin-top:20px;">
		<li>Glissez-déposez les éléments ci-dessous pour réorganiser l'ordre d'affichage des produits sur le site.</li>
		<li>Cliquez sur un produit ci-dessous pour accéder à sa fiche.</li>
	</ul>
</p>
<p>
<?php

	//-----------------------------------------------------------------------------------------------------

	$query = "
	SELECT p.idproduct, 
		p.name_1,
		p.available
	FROM associat_product ap, product p
	WHERE ap.idproduct = '" . $tableobj->get( "idproduct" ) . "'
	AND ap.associat_product = p.idproduct
	ORDER BY ap.displayorder ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<ul id="AssociateList">
	<?php 

		while( !$rs->EOF() ){
			
			$name = $rs->fields( "name_1" );
			
			?>
			<li id="Associates_<?php echo $rs->fields( "idproduct" ) ?>">
				<div style="position:relative; height:60px;">
					<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>">
						<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE,0 , 60, 60 ); ?>" title="<?php echo htmlentities( $name ); ?>" alt="" style="opacity:<?php echo $rs->fields( "available" ) ? "1.0" : "0.4"; ?>;" />
					</a>
					<a href="#" onclick="deleteAssociatedProduct( <?php echo $rs->fields( "idproduct" ) ?> ); return false;" style="cursor:pointer;">
						<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'option" alt="supprimer l'option" style="position:absolute; bottom:2px; left:2px;" />
					</a>
				</div>
			</li>
			<?php 
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
</p>
<div class="clear" style="margin-bottom:10px;"></div>
<?php 

//-----------------------------------------------------------------------------------------------------

?>
<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Complémentaires</p>
</div>
<p style="margin:10px 0px;">
	N° du produit : 
	<input type="text" id="ComplementedProduct" name="" value="" class="textInput" onkeydown="if( event.keyCode == 13 ){ $( this ).bind( 'keyup', function(e){createComplementedProduct();} ); return false; }" style="width:250px;" />
	<?php
	
		include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
		//AutoCompletor::completeFromDB( "ComplementedProduct", "product", "idproduct", 15, "createComplementedProduct" ); 
		AutoCompletor::completeFromDBPattern( "ComplementedProduct", "product", "idproduct", "{name_1}", 15, "createComplementedProduct" ); 
		
	?>
	<input type="button" name="" value="Créer" class="blueButton" onclick="createComplementedProduct();" />
</p>
<p>
	<ul style="margin-top:20px;">
		<li>Glissez-déposez les éléments ci-dessous pour réorganiser l'ordre d'affichage des produits sur le site.</li>
		<li>Cliquez sur un produit ci-dessous pour accéder à sa fiche.</li>
	</ul>
</p>
<p>
<?php

	//-----------------------------------------------------------------------------------------------------
	
	$query = "
	SELECT p.idproduct, 
		p.name_1,
		p.available
	FROM complement_product ap, product p
	WHERE ap.idproduct = '" . $tableobj->get( "idproduct" ) . "'
	AND ap.complement_product = p.idproduct
	ORDER BY ap.displayorder ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<ul id="ComplementList">
	<?php 

		while( !$rs->EOF() ){
			
			$name = $rs->fields( "name_1" );
			
			?>
			<li id="Complementes_<?php echo $rs->fields( "idproduct" ) ?>">
				<div style="position:relative; height:60px;">
					<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>">
						<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE,0 , 60, 60 ); ?>" title="<?php echo htmlentities( $name ); ?>" alt="" style="opacity:<?php echo $rs->fields( "available" ) ? "1.0" : "0.4"; ?>;" />
					</a>
					<a href="#" onclick="deleteComplementedProduct( <?php echo $rs->fields( "idproduct" ) ?> ); return false;" style="cursor:pointer;">
						<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'option" alt="supprimer l'option" style="position:absolute; bottom:2px; left:2px;" />
					</a>
				</div>
			</li>
			<?php 
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
</p>
<div class="clear" style="margin-bottom:10px;"></div>
<?php 

//-----------------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
/* <![CDATA[ */

	function selectSimilares( idproduct_similar_1 ){
	
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_associates.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&idproduct_similar_1=" + idproduct_similar_1,
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sélectionner la famille de produit similaires" ); },
		 	success: function( responseText ){

				if( responseText )
					alert( "Impossible de sélectionner la famille de produit similaires : " + responseText );
				else $( "#ProductTabs" ).tabs( "load", 7 );
				
			}

		});
		
	}

/* ]]> */
</script>
<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Produits similaires</p>
</div>
<p style="margin:10px 0px;">
	Famille de produits similaires
	<?php
	
		include_once( "$GLOBAL_START_PATH/objects/ui/DXComboBox.php" );
		include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" ); 

		XHTMLFactory::createSelectElement( $table = "product_similar", $columnAsValue = "idproduct_similare", $columnAsInnerHTML = "name", $orderByColumn = "name", $selectedValue = $tableobj->get( "idproduct_similar_1" ), $nullValue = "0", $nullInnerHTML = "-", $attributes = " name=\"idproduct_similar_1\" id=\"idproduct_similar_1\" style=\"width:auto;\" onchange=\"selectSimilares(this.options[ this.selectedIndex ].value);\"" );
		DXComboBox::create( "idproduct_similar_1" );
	
	?>
</p>
<p>
	<ul style="margin-top:20px;">
		<li>Glissez-déposez les éléments ci-dessous pour réorganiser l'ordre d'affichage des produits sur le site.</li>
		<li>Cliquez sur un produit ci-dessous pour accéder à sa fiche.</li>
	</ul>
</p>
<p>
<?php
	
	$query = "
	SELECT idproduct, 
		name_1,
		available
	FROM product
	WHERE idproduct_similar_1 = '" . $tableobj->get( "idproduct_similar_1" ) . "'
	AND idproduct_similar_1 <> 0
	ORDER BY similar_1_order ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<ul id="SimilareList">
	<?php 

		while( !$rs->EOF() ){
			
			$name = $rs->fields( "name_1" );
			
			?>
			<li id="Similares_<?php echo $rs->fields( "idproduct" ) ?>">
				<div style="position:relative; height:60px;">
					<a href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>">
						<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE,0 , 60, 60 ); ?>" title="<?php echo htmlentities( $name ); ?>" alt="" style="opacity:<?php echo $rs->fields( "available" ) ? "1.0" : "0.4"; ?>;" />
					</a>
					<a href="#" onclick="deleteSimilare( <?php echo $rs->fields( "idproduct" ) ?> ); return false;" style="cursor:pointer;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" title="supprimer le produit similaire" alt="supprimer le produit similaire" style="position:absolute; bottom:2px; left:2px;" />
					</a>
				</div>
			</li>
			<?php 
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
</p>
<div class="clear" style="margin-bottom:10px;"></div>
<?php

/* ------------------------------------------------------------------------------------------------------------ */

?>
<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------------ */
	
	function listTrades(){
	
		$( "#TradeDialog" ).dialog({
        		
			title: "Liste des métiers",
			width:  500,
			height: 500,
			modal: true,
			close: function(event, ui) { $("#TradeDialog").dialog( 'destroy' ); }
		
		});
		
	}

	/* ------------------------------------------------------------------------------------------------------ */
	
	function deleteTrade( idtrade ){
	
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php?delete&idtrade=" + idtrade + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>",
			async: true,
			type: "POST",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#TradeItem" + idtrade ).fadeOut( 700 ).remove();
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------ */
	
	function selectTrade( idtrade ){
	
		$('#idtrade_' + idtrade ).attr('checked', !$('#idtrade_' + idtrade).attr('checked') );
		$('#tradeItem_' + idtrade ).css( 'border-color', $('#idtrade_' + idtrade).attr('checked') ? '#1E9CD1' : '#E7E7E7' );
		
	}
	 
	/* ------------------------------------------------------------------------------------------------------ */
	
	function updateTradeSelection(){
	
		var selection = "";
		$( "#TradeDialog" ).find( "input:checkbox" ).each( function( i ){

			if( $( this ).attr( "checked" ) ){

				if( selection.length )
					selection += ",";
	
				selection += $( this ).val();

			}

		});

		var data = "trades=" + selection + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load", 6 );
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});

	}

	/* ------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Métiers</p>
</div>
<p style="margin:10px 0px;">
	<a class="blueLink" href="#" onclick="listTrades(); return false;">
		<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/flexigrid_button_create.png" alt="" />
		Sélectionner des métiers
	</a>
</p>
<p><?php listProductTrades( $tableobj->get( "idproduct" ) ); ?></p>
<div id="TradeDialog" style="display:none;">
	<ul id="AvailableTradeList">
	<?php
	
		$selectedTrades = array();
		$rs =& DBUtil::query( "SELECT DISTINCT idtrade FROM trade_product WHERE idproduct = '" . $tableobj->get( "idproduct" ) . "'" );
		
		while( !$rs->EOF() ){
			
			$selectedTrades[] = $rs->fields( "idtrade" );
			$rs->MoveNext();
				
		}
		
		$query = "
		SELECT idtrade, trade_1 AS name, image
		FROM trade
		ORDER BY trade_1 ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
		
			if( strlen( $rs->fields( "image" ) ) && file_exists( "$GLOBAL_START_PATH/www/" . $rs->fields( "image" ) ) )
				$uri = "/www/" . $rs->fields( "image" );
			else $uri = "/www/img_prod/photo_missing_icone.jpg";
		
			$checked = in_array( $rs->fields( "idtrade" ), $selectedTrades ) ? " checked=\"checked\"" : "";
			$borderColor = in_array( $rs->fields( "idtrade" ), $selectedTrades ) ? "#1E9CD1" : "#E7E7E7";
			
			?>
			<li style="word-wrap: break-word;overflow: hidden;">
				<div style="position:relative; height:500px;">
					<input type="checkbox"<?php echo $checked; ?> id="idtrade_<?php echo $rs->fields( "idtrade" ); ?>" value="<?php echo $rs->fields( "idtrade" ); ?>" style="position:absolute; top:0px; left:0px;" />

					<a title="<?php echo htmlentities( $rs->fields( "name" ) ); ?>" href="#" onclick="selectTrade(<?php echo $rs->fields( "idtrade" ); ?>); return false;">
                        <?php
                        if(file_exists("$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( $uri ))) {
                            ?>
                            <img id="tradeItem_<?php echo $rs->fields("idtrade"); ?>"
                                 alt="<?php echo utf8_decode($rs->fields("name")); ?>"
                                 src="<?php echo "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode($uri); ?>&amp;w=60&amp;h=60"
                                 style="border:2px solid <?php echo $borderColor; ?>;"/>
                            <?php
                        }
                        ?>

					</a><br>
					<div class="clear" style="margin-bottom:10px;"></div>
                    <?php
                    if(!file_exists("$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( $uri ))) {
                    ?>
                    <a title="<?php echo htmlentities( $rs->fields( "name" ) ); ?>" href="#" onclick="selectTrade(<?php echo $rs->fields( "idtrade" ); ?>); return false;">
					<?php echo utf8_decode( $rs->fields( "name" ) ); ?>
                    </a>
                        <?php
                    }
                    ?>
				</div>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
			
	?>
	</ul>
	<br style="clear:both;" />
	<p style="text-align:right;">
		<input type="button" class="blueButton" value="Annuler" onclick="$('#TradeDialog').dialog('destroy');" />
		<input type="button" class="blueButton" value="Valider" onclick="updateTradeSelection(); $('#TradeDialog').dialog('destroy');" />
	</p>
</div>
<div class="clear" style="margin-bottom:10px;"></div>
<?php

/* ----------------------------------------------------PAGES SPECIFIQUES-------------------------------------------------------- */

?>
<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------------ */
	
	function listPages(){
	
		$( "#PageDialog" ).dialog({
        		
			title: "Liste des pages thématiques",
			width:  500,
			modal: true,
			close: function(event, ui) { $("#PageDialog").dialog( 'destroy' ); }
		
		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------ */
		
	function deletePage( idpage ){
	
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php?delete&idpage=" + idpage + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>",
			async: true,
			type: "POST",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#PageItem" + idpage ).fadeOut( 700 ).remove();
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------ */
	
	function selectPage( idpage ){
	
		$('#idpage_' + idpage ).attr('checked', !$('#idpage_' + idpage).attr('checked') );
		$('#pageItem_' + idpage ).css( 'color', $('#idpage_' + idpage).attr('checked') ? '#1E9CD1' : '#44474E' );
		
	}
	 
	/* ------------------------------------------------------------------------------------------------------ */
	
	function updatePageSelection(){
	
		var selection = "";
		$( "#PageDialog" ).find( "input:checkbox" ).each( function( i ){

			if( $( this ).attr( "checked" ) ){

				if( selection.length )
					selection += ",";
	
				selection += $( this ).val();

			}

		});

		var data = "pages=" + selection + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";;
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){
				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load", 6 );
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications : " + responseText );
        		
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>

<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Pages thématiques</p>
</div>
<p style="margin:10px 0px;">
	<a class="blueLink" href="#" onclick="listPages(); return false;">
		<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/flexigrid_button_create.png" alt="" />
		Sélectionner les pages thématiques
	</a>
</p>
<p><?php listProductPages( $tableobj->get( "idproduct" ) ); ?></p>

<div id="PageDialog" style="display:none;">
	<ul id="AvailablePageList">
	<?php
	
		$selectedPages = array();
		$rs =& DBUtil::query( "SELECT DISTINCT idpage FROM page_product WHERE idproduct = '" . $tableobj->get( "idproduct" ) . "'" );
		
		while( !$rs->EOF() ){
			
			$selectedPages[] = $rs->fields( "idpage" );
			$rs->MoveNext();
				
		}
		
		$query = "
		SELECT idpage, page_1 AS name
		FROM page
		ORDER BY page_1 ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
			$checked = in_array( $rs->fields( "idpage" ), $selectedPages ) ? " checked=\"checked\"" : "";
			?>
			<li>
				<div style="position:relative;">
					<input type="checkbox"<?php echo $checked; ?> id="idpage_<?php echo $rs->fields( "idpage" ); ?>" value="<?php echo $rs->fields( "idpage" ); ?>" style="float:left;" />
					<a title="<?php echo htmlentities( $rs->fields( "name" ) ); ?>" href="#" onclick="selectPage(<?php echo $rs->fields( "idpage" ); ?>); return false;" style="display:inline-block;vertical-align:top;">
						<span id="pageItem_<?php echo $rs->fields( "idpage" ); ?>"><?php echo htmlentities( $rs->fields( "name" ) ); ?></span>
					</a>
				</div>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
			
	?>
	</ul>
	<br style="clear:both;" />
	<p style="text-align:right;">
		<input type="button" class="blueButton" value="Annuler" onclick="$('#PageDialog').dialog('destroy');" />
		<input type="button" class="blueButton" value="Valider" onclick="updatePageSelection(); $('#PageDialog').dialog('destroy');" />
	</p>
</div>


<?php

/* ------------------------------------------------------------------------------------------------------------ */

function listProductTrades( $idproduct ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
		
	$query = "
	SELECT DISTINCT t.idtrade, t.trade_1 AS name, t.image
	FROM trade_product tp, trade t
	WHERE tp.idproduct = '$idproduct'
	AND tp.idtrade = t.idtrade
	ORDER BY t.trade_1 ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p>Ce produit n'appartient à aucune branche métier</p>
		<?php
		
		return;
		
	}
	
	?>
	<p>Le produit est actuellement dans les métiers suivants :</p>
	<ul id="TradeList">
	<?php
	
	while( !$rs->EOF() ){
		
		if( strlen( $rs->fields( "image" ) ) && file_exists( "$GLOBAL_START_PATH/www/" . $rs->fields( "image" ) ) )
			$uri = "/www/" . $rs->fields( "image" );
		else $uri = "/www/img_prod/photo_missing_icone.jpg";
		
		?>
		<li id="TradeItem<?php echo $rs->fields( "idtrade" ); ?>">
            <span> <?php echo utf8_decode( $rs->fields( "name" ) ); ?> </span>
			<div style="position:relative; height:60px;">
				<a href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/trades.php?idtrade=<?php echo $rs->fields( "idtrade" ); ?>" onclick="window.open( this.href ); return false;">

					<img alt="<?php echo utf8_decode( $rs->fields( "name" ) ); ?>" src="<?php echo "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( $uri ); ?>&amp;w=60&amp;h=60" />
				</a>
				<a href="#" onclick="deleteTrade( <?php echo $rs->fields( "idtrade" ) ?> ); return false;" style="cursor:pointer;">
					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" title="supprimer le métier" alt="supprimer le métier" style="position:absolute; bottom:2px; left:2px;" />
				</a>
			</div>
		</li>
		<?php
		
		$rs->MoveNext();
		
	}
	
	?>
	</ul>
	<br style="clear:both;" />
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------ */

function listProductPages( $idproduct ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
		
	$query = "
	SELECT DISTINCT p.idpage, p.page_1 AS name
	FROM page_product pp, page p
	WHERE pp.idproduct = '$idproduct'
	AND pp.idpage = p.idpage
	ORDER BY p.page_1 ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p>Ce produit n'appartient à aucune page thématique</p>
		<?php
		
		return;
		
	}
	
	?>
	<p>Le produit est actuellement dans les pages thématiques suivantes :</p>
	<ul id="PageList">
	<?php
	
	while( !$rs->EOF() ){
		?>
		<li id="PageItem<?php echo $rs->fields( "idpage" ); ?>">
			<div style="position:relative;">
				<span><?php echo htmlentities( $rs->fields( "name" ) ); ?></span>
				<a href="#" onclick="deletePage( <?php echo $rs->fields( "idpage" ) ?> ); return false;" style="cursor:pointer;">
					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" title="supprimer la page" alt="supprimer la page" style="bottom:2px; left:2px;" />
				</a>
			</div>
		</li>
		<?php
		
		$rs->MoveNext();
		
	}
	
	?>
	</ul>
	<br style="clear:both;" />
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------ */

?>