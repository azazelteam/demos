<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Statistiques produits
 */
 
include_once( "../../config/init.php" );
include_once( "../../objects/DHTMLCalendar.php" );

header( "Content-Type: text/html; charset=utf-8" );

DHTMLCalendar::calendar( "StartDate" );
DHTMLCalendar::calendar( "EndDate" );

?>
<script type="text/javascript">
/* <![CDATA[ */
	
	function searchTrades( orderby, asc ){
	
		var tabURL =  "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_stats.php?idproduct=<?php echo $_REQUEST[ "idproduct" ]; ?>";
		tabURL += "&start=" + $( "#StartDate" ).val();
		tabURL += "&end=" + $( "#EndDate" ).val();
		tabURL += "&orderby=" + escape( orderby );
		tabURL += asc ? "&asc" : "&desc";
		
		$( "#ProductTabs" ).tabs( "url" , 8 ,tabURL );
		$( '#ProductTabs' ).tabs( "load", 8 );
		
	}

/* ]]> */
</script>
<style type="text/css">

	.statTable{ 
		width: 100%;
		border-collapse: collapse;
		border:1px solid #E7E7E7;
	}
	
	.statTable th.noTopBorder{
		border-top-style:none !important;
	}
	
	.statTable th.noTopBorder img{
		display:block;
		width:13px;
		margin:2px auto;
	}
	
	.statTable th{
		border-bottom-style:none !important;
	}
	
	.statTable th, .statTable td {
		border:1px solid #E7E7E7;
		height:20px;
		text-align:center;
		width:10%;	
	}

	.statTable th{
		background-color:#F3F3F3;
	}
	
	.trade a{
		margin:20px;
	}
	
</style>
<p style="margin-top:20px;">
	Rechercher les Devis et les Commandes effectués pour la période du : 
	<input class="calendarInput" type="text" id="StartDate" value="<?php if( isset( $_REQUEST[ "start" ] ) ) echo $_REQUEST[ "start" ]; ?>" />
	au
	<input class="calendarInput" type="text" id="EndDate" value="<?php if( isset( $_REQUEST[ "end" ] ) ) echo $_REQUEST[ "end" ]; ?>" />
	<input type="button" class="blueButton" value="Rechercher" onclick="searchTrades('t.DateHeure', true);" />
</p>
<div class="subTitleContainer" style="margin-top:15px;">
	<p class="subTitle">
		<a href="#" onclick="$( '#Summary' ).slideToggle( 'slow' ); return false;" style="text-decoration:none;">&#0155; Liste des Devis et Commandes Client</a>
	</p>
</div>
<div style="margin:10px 0px;"><?php summary(); ?></div>
<?php

	$references =& DBUtil::query( "SELECT idarticle FROM detail WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' ORDER BY display_detail_order ASC" );
	
	while( !$references->EOF() ){
		
		listReferenceTrades( $references->fields( "idarticle" ) );
		
		$references->MoveNext();
		
	}

/* ------------------------------------------------------------------------------------------- */

function summary(){

	if( isset( $_REQUEST[ "start" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "start" ] ) ){
	
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "start" ] );	
		$start = "$year-$month-$day";
	
	}
	else $start = '0000-00-00';
	
	if( isset( $_REQUEST[ "end" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "end" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "end" ] );	
		$end = "$year-$month-$day";
	
	}
	else $end = date( "Y-m-d" );
	
	$estimateCount 	= DBUtil::query( "SELECT COUNT( DISTINCT( t.idestimate ) ) AS `count` FROM estimate_row tr, estimate t, detail d WHERE d.idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND d.idarticle = tr.idarticle AND tr.idestimate = t.idestimate AND t.DateHeure >= '$start 00:00:00' AND t.DateHeure <= '$end 23:59:59'" )->fields( "count" );
	$orderCount 	= DBUtil::query( "SELECT COUNT( DISTINCT( t.idorder ) ) AS `count` FROM order_row tr, `order` t, detail d WHERE d.idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND d.idarticle = tr.idarticle AND tr.idorder = t.idorder AND t.status NOT LIKE 'Cancelled' AND t.DateHeure >= '$start 00:00:00' AND t.DateHeure <= '$end 23:59:59'" )->fields( "count" );

	if( !$estimateCount && !$orderCount ){
		
		?>
		<p style="text-align:center;">Aucun devis et aucune commande trouvés pour ce produit</p>
		<?php
		
		return;
		
	}
	
	?>
    
	<div class="tableContainer" id="Summary">
		<table class="statTable" id="SummaryTable" style="border:1px solid #D2D2D2;">
			<thead>
				<tr>
					<th>Référence</th>
					<th>Nombre de Devis</th>
					<th>Nombre de Commande</th>
					<th>Total Cdes HT</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$rs =& DBUtil::query( "SELECT idarticle, reference FROM detail WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' ORDER BY display_detail_order ASC" );
				
				$estimateCountSum = 0;
				$orderCountSum = 0;
				$totalETSum = 0.0;
				
				while( !$rs->EOF() ){
					
					$estimateCount 	= DBUtil::query( "SELECT COUNT( DISTINCT( tr.idestimate ) ) AS `count` FROM estimate_row tr, estimate t, detail d WHERE d.idarticle = '" . $rs->fields( "idarticle" ) . "' AND d.idarticle = tr.idarticle AND tr.idestimate = t.idestimate AND t.DateHeure >= '$start 00:00:00' AND t.DateHeure <= '$end 23:59:59'" )->fields( "count" );
					$orderCount 	= DBUtil::query( "SELECT COUNT( DISTINCT( tr.idorder ) ) AS `count` FROM order_row tr, `order` t, detail d WHERE tr.idarticle = '" . $rs->fields( "idarticle" ) . "' AND d.idarticle = tr.idarticle AND tr.idorder = t.idorder AND t.status NOT LIKE 'Cancelled' AND t.DateHeure >= '$start 00:00:00' AND t.DateHeure <= '$end 23:59:59'" )->fields( "count" );
	
					$totalET = DBUtil::query(

						"SELECT SUM( tr.quantity * tr.discount_price ) AS totalET
						FROM order_row tr, `order` t
						WHERE tr.idarticle = '" . $rs->fields( "idarticle" ) . "'
						AND tr.idorder = t.idorder
						AND t.`status` NOT LIKE 'Cancelled'
						AND t.DateHeure >= '$start 00:00:00' AND t.DateHeure <= '$end 23:59:59'" 
					
					)->fields( "totalET" );
					
					?>
					<tr>
						<td>
							<a class="blueLink" href="#<?php echo htmlentities( $rs->fields( "idarticle" ) ); ?>" onclick="if( $('#Reference<?php echo $rs->fields( "idarticle" ); ?>').css('display') == 'none' ) $('#Reference<?php echo $rs->fields( "idarticle" ); ?>').slideToggle( 'slow' );">
								<?php echo htmlentities( $rs->fields( "reference" ) ); ?>
							</a>
						</td>
						<td style="text-align:right;"><?php echo $estimateCount; ?></td>
						<td style="text-align:right;"><?php echo $orderCount; ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $totalET ); ?></td>
					</tr>
					<?php
					
					$estimateCountSum += $estimateCount;
					$orderCountSum += $orderCount;
					$totalETSum += $totalET;
					
					$rs->MoveNext();
					
				}
				
				?>
				<tr>
					<td></td>
					<td style="text-align:right;"><b><?php echo $estimateCountSum; ?></b></td>
					<td style="text-align:right;"><b><?php echo $orderCountSum; ?></b></td>
					<td style="text-align:right;"><b><?php echo Util::priceFormat( $totalETSum ); ?></b></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
			
}

/* ------------------------------------------------------------------------------------------- */

function listReferenceTrades( $idarticle ){
	
	?>
	<a name="<?php echo $idarticle; ?>"></a>
	<div class="subTitleContainer" style="margin-top:15px;">
		<p class="subTitle">
			<a href="#" onclick="$( '#Reference<?php echo $idarticle; ?>' ).slideToggle( 'slow' ); return false;" style="text-decoration:none;">&#0155; Référence <?php echo htmlentities( DBUtil::getDBValue( "reference", "detail", "idarticle", $idarticle ) ); ?></a>
		</p>
	</div>
	<div id="Reference<?php echo $idarticle; ?>" style="display:none;">
		<p style="margin:20px;">
			<a class="blueLink" href="#" onclick="$( '#EstimateList<?php echo $idarticle; ?>' ).slideToggle( 'slow' ); return false;" style="text-decoration:none; font-weight:bold;">&#0155; Liste des Devis</a>
		</p>
		<div style="margin:10px 0px;"><?php listEstimates( $idarticle ); ?></div>
		<p style="margin:20px;">
			<a class="blueLink" href="#" onclick="$( '#OrderList<?php echo $idarticle; ?>' ).slideToggle( 'slow' ); return false;" style="text-decoration:none; font-weight:bold;">&#0155; Liste des Commandes Client</a>
		</p>
		<div style="margin:10px 0px;"><?php listOrders( $idarticle ); ?></div>
	</div>
	<?php

}

/* ------------------------------------------------------------------------------------------- */

function listEstimates( $idarticle ){
	
	global $GLOBAL_START_URL;
	
	$query = "
	SELECT tr.reference,
		tr.idarticle, 
		t.idestimate, 
		t.DateHeure, 
		t.idbuyer,
		tr.quantity,	
		tr.unit_cost_amount,
		tr.unit_price,
		tr.ref_discount,
		tr.discount_price,
		tr.quantity * tr.discount_price AS totalET
	FROM estimate t, estimate_row tr
	WHERE tr.idarticle = '$idarticle'
	AND t.idestimate = tr.idestimate";
	
	if( isset( $_REQUEST[ "start" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "start" ] ) ){
	
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "start" ] );	
		$query .= " AND t.DateHeure >= '$year-$month-$day 00:00:00'";
	
	}
	
	if( isset( $_REQUEST[ "end" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "end" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "end" ] );	
		$query .= " AND t.DateHeure <= '$year-$month-$day 23:59:59'";
	
	}
	
	$order = isset( $_REQUEST[ "desc" ] ) ? "DESC" : "ASC";
	
	if( isset( $_REQUEST[ "orderby" ] ) )
		$query .= "
		ORDER BY " . $_REQUEST[ "orderby" ] . " $order";
	else $query .= " ORDER BY t.DateHeure DESC, t.idestimate DESC";
	 
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun devis trouvé</p>
		<?php
		
		return;
			
	}
	
	?>
	<div class="tableContainer" id="EstimateList<?php echo $idarticle; ?>">
		<table class="statTable" style="border:1px solid #D2D2D2;">
			<thead>
				<tr>
					<th style="text-align:center;">Référence</th>
					<th style="text-align:center;">Devis N°</th>
					<th style="text-align:center;">Date</th>
					<th style="text-align:center;">Client N°</th>
					<th style="text-align:center;">Quantité</th>
					<th style="text-align:center;">Prix Achat Net</th>
					<th style="text-align:center;">Prix Vente U.</th>
					<th style="text-align:center;">Remise %</th>
					<th style="text-align:center;">Prix Vente U. Net</th>
					<th style="text-align:center;">Total HT</th>
				</tr>
				<tr>
                	<th class="noTopBorder"></th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('t.DateHeure',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('t.DateHeure',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('t.idbuyer',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('t.idbuyer',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.unit_cost_amount',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.unit_cost_amount',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.unit_price',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.unit_price',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.discount_price',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.discount_price',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.discount_price * tr.quantity',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.discount_price * tr.quantity',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
				</tr>
			</thead>
			<tbody>
			<?php
		
				$estimateCount 			= 0;
				$customerCount 			= 0;
				$quantitySum 			= 0.0;
				$unit_cost_amountSum 	= 0.0;
				$unit_priceSum 			= 0.0;
				$ref_discountSum 		= 0.0;
				$discount_priceSum 		= 0.0;
				$totalETSum 			= 0.0;
				
				$customers 				= array();
				$last_idestimate = false;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td style="text-align:center;"><?php echo htmlentities( $rs->fields( "reference" ) ); ?></td>
						<td style="text-align:center;">
							<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $rs->fields( "idestimate" ); ?>" onclick="window.open(this.href); return false;">
								<?php echo $rs->fields( "idestimate" ); ?>
							</a>
						</td>
						<td style="text-align:center;"><?php echo Util::dateFormatEu( substr( $rs->fields( "DateHeure" ), 0, 10 ) ); ?></td>
						<td style="text-align:center;">
							<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>" onclick="window.open(this.href); return false;">
								<?php echo $rs->fields( "idbuyer" ); ?>
							</a>
						</td>
						<td><div style="text-align:right;"><?php echo $rs->fields( "quantity" ); ?></div></td>
						<td><div style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "unit_cost_amount" ) ); ?></div></td>
						<td><div style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "unit_price" ) ); ?></div></td>
						<td><div style="text-align:right;"><?php echo $rs->fields( "ref_discount" ) > 0.0 ? Util::rateFormat( $rs->fields( "ref_discount" ) ) : "-"; ?></div></td>
						<td><div style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "discount_price" ) ); ?></div></td>
						<td><div style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "totalET" ) ); ?></div></td>
					</tr>
					<?php
					
					if( $last_idestimate === false || $last_idestimate != $rs->fields( "idestimate" ) ){
					
						$last_idestimate = $rs->fields( "idestimate" );
						$estimateCount++;
						
					}
					
					if( !in_array( $rs->fields( "idbuyer" ), $customers ) )
						array_push( $customers, $rs->fields( "idbuyer" ) );
	
					$quantitySum 			+= $rs->fields( "quantity" );
					$unit_cost_amountSum 	+= $rs->fields( "unit_cost_amount" );
					$unit_priceSum 			+= $rs->fields( "unit_price" );
					$discount_priceSum 		+= $rs->fields( "discount_price" );
					$totalETSum 			+= $rs->fields( "totalET" );
					
					$rs->MoveNext();
		
				}
				
				?>
				<tr style="font-weight:bold;">
					<th>Total</th>
					<th><?php echo $estimateCount; ?></th>
					<th></th>
					<th><?php echo count( $customers ); ?></th>
					<th style="text-align:right;"><?php echo $quantitySum; ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $unit_cost_amountSum / $rs->RecordCount() ); ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $unit_priceSum  / $rs->RecordCount() ); ?></th>
					<th></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $discount_priceSum  / $rs->RecordCount() ); ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $totalETSum ); ?></th>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------- */

function listOrders( $idarticle ){
	
	global $GLOBAL_START_URL;
	
	$query = "
	SELECT tr.reference, 
		tr.idarticle,
		t.idorder, 
		t.DateHeure, 
		t.idbuyer,
		tr.quantity,	
		tr.unit_cost_amount,
		tr.unit_price,
		tr.ref_discount,
		tr.discount_price,
		tr.quantity * tr.discount_price AS totalET
	FROM `order` t, order_row tr
	WHERE tr.idarticle = '$idarticle'
	AND t.idorder = tr.idorder
	AND t.status NOT LIKE 'Cancelled'";
	
	if( isset( $_REQUEST[ "start" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "start" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "start" ] );	
		$query .= " AND t.DateHeure >= '$year-$month-$day 00:00:00'";
	
	}
	
	if( isset( $_REQUEST[ "end" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "end" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "end" ] );	
		$query .= " AND t.DateHeure <= '$year-$month-$day 23:59:59'";
		
	}
	
	$order = isset( $_REQUEST[ "desc" ] ) ? "DESC" : "ASC";
	
	if( isset( $_REQUEST[ "orderby" ] ) )
		$query .= "
		ORDER BY " . $_REQUEST[ "orderby" ] . " $order";
	else $query .= " ORDER BY t.DateHeure DESC, t.idorder DESC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune commande trouvée</p>
		<?php
		
		return;
			
	}
	
	?>
	<div class="tableContainer" id="OrderList<?php echo $idarticle; ?>">
		<table class="statTable">
			<thead>
				<tr>
					<th style="text-align:center;">Référence</th>
					<th style="text-align:center;">Cde N°</th>
					<th style="text-align:center;">Date</th>
					<th style="text-align:center;">Client N°</th>
					<th style="text-align:center;">Quantité</th>
					<th style="text-align:center;">Prix Achat Net</th>
					<th style="text-align:center;">Prix Vente U.</th>
					<th style="text-align:center;">Remise %</th>
					<th style="text-align:center;">Prix Vente U. Net</th>
					<th style="text-align:center;">Total HT</th>
				</tr>
				<tr>
                	<th class="noTopBorder"></th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('t.DateHeure',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('t.DateHeure',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('t.idbuyer',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('t.idbuyer',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.unit_cost_amount',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.unit_cost_amount',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.unit_price',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.unit_price',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.discount_price',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.discount_price',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.discount_price * tr.quantity',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.discount_price * tr.quantity',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
				</tr>
			</thead>
			<tbody>
			<?php
		
				$orderCount 			= 0;
				$customerCount 			= 0;
				$quantitySum 			= 0.0;
				$unit_cost_amountSum 	= 0.0;
				$unit_priceSum 			= 0.0;
				$ref_discountSum 		= 0.0;
				$discount_priceSum 		= 0.0;
				$totalETSum 			= 0.0;
				
				$customers 				= array();
				$last_idorder = false;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td style="text-align:center;"><?php echo htmlentities( $rs->fields( "reference" ) ); ?></td>
						<td style="text-align:center;">
							<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $rs->fields( "idorder" ); ?>" onclick="window.open(this.href); return false;">
								<?php echo $rs->fields( "idorder" ); ?>
							</a>
						</td>
						<td style="text-align:center;"><?php echo Util::dateFormatEu( substr( $rs->fields( "DateHeure" ), 0, 10 ) ); ?></td>
						<td style="text-align:center;">
							<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>" onclick="window.open(this.href); return false;">
								<?php echo $rs->fields( "idbuyer" ); ?>
							</a>
						</td>
						<td style="text-align:right;"><?php echo $rs->fields( "quantity" ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "unit_cost_amount" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "unit_price" ) ); ?></td>
						<td style="text-align:right;"><?php echo $rs->fields( "ref_discount" ) > 0.0 ? Util::rateFormat( $rs->fields( "ref_discount" ) ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "discount_price" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "totalET" ) ); ?></td>
					</tr>
					<?php
					
					if( $last_idorder === false || $last_idorder != $rs->fields( "idorder" ) ){
					
						$last_idorder = $rs->fields( "idorder" );
						$orderCount++;
						
					}
					
					if( !in_array( $rs->fields( "idbuyer" ), $customers ) )
						array_push( $customers, $rs->fields( "idbuyer" ) );
						
					$quantitySum 			+= $rs->fields( "quantity" );
					$unit_cost_amountSum 	+= $rs->fields( "unit_cost_amount" );
					$unit_priceSum 			+= $rs->fields( "unit_price" );
					$discount_priceSum 		+= $rs->fields( "discount_price" );
					$totalETSum 			+= $rs->fields( "totalET" );
					
					$rs->MoveNext();
		
				}
				
				?>
				<tr style="font-weight:bold;">
					<th>Total</th>
					<th><?php echo $orderCount; ?></th>
					<th></th>
					<th><?php echo count( $customers ); ?></th>
					<th style="text-align:right;"><?php echo $quantitySum; ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $unit_cost_amountSum / $rs->RecordCount() ); ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $unit_priceSum / $rs->RecordCount() ); ?></th>
					<th></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $discount_priceSum / $rs->RecordCount() ); ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $totalETSum ); ?></th>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------- */

?>