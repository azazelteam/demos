<?
//Script pour MIM pour mettre à jour la table détail en entier
//pour calculer le HT et la marge tout seul rien qu'en rentrant le TTC et le prix d'achat

include_once( "../../objects/classes.php" );

/**
 * Selection du mode d'affichage :
 * 	- B2B
 * 	- B2C
 * */
if(!isset($mode)){
	$mode = DBUtil::getParameterAdmin("b2c");
}


include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" ); 
?>
<div id="globalMainContent">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/recalculate.php</span>
<?php } ?>



	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Calcul des marges</p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">

<?
	if($mode == 0){
		//B2B
		$req=	"SELECT idarticle,buyingcost,sellingcost,suppliercost, sellingcostati,hidecost,ratecost
			FROM detail
			WHERE sellingcost<=0";
		
		$rs =& DBUtil::query($req);
	
		echo $rs->RecordCount()." enregistrements à traiter<br><br>";
		
		if(isset($_POST["calculate"])){
			$count=0;
			while(!$rs->EOF()){
				
				$idarticle=$rs->fields("idarticle");
				$sellingcost=round($rs->fields("sellingcostati")/1.196,4);
				$ratecost=$sellingcost-$rs->fields("buyingcost");
				$req2=	"UPDATE `detail` SET `sellingcost` = '$sellingcost',
						`ratecost` = '$ratecost'
						WHERE idarticle='$idarticle'";
				$rs2 =& DBUtil::query($req2);
			
				$count++;
				$rs->MoveNext();
			}
			echo "<br />$count enregistrements mis à jour<br /><br /><br /><br />";
		
			//Tracker
			$headers = 'From: AKILAE <webmaster@akilae-saas.fr>' . "\r\n";
			//mail("webmaster@akilae-saas.fr","Tracker calcul de prix MIM B2B","Mises à jour : ".$count,$headers); 
		}
			
		
	}
	else{
		//B2C
		
		$req=	"SELECT idarticle,buyingcost,sellingcost,suppliercost, sellingcostati,hidecost,ratecost
			FROM detail";
	
		$rs =& DBUtil::query($req);
	
		echo $rs->RecordCount()." enregistrements à traiter<br><br>";
		
		if(isset($_POST["calculate"])){
			$count=0;
			while(!$rs->EOF()){
				
				$idarticle=$rs->fields("idarticle");
				$sellingcost=round($rs->fields("sellingcostati")/1.196,4);
				$ratecost=$sellingcost-$rs->fields("buyingcost");
				$req2=	"UPDATE `detail` SET `sellingcost` = $sellingcost,
						`ratecost` = '$ratecost'
						WHERE idarticle='$idarticle'";
				$rs2 =& DBUtil::query($req2);
			
				$count++;
				$rs->MoveNext();
			}
			echo "<br />$count enregistrements mis à jour<br /><br /><br /><br />";
			
			//Tracker
			$headers = 'From: AKILAE <webmaster@akilae-saas.fr>' . "\r\n";
			//mail("webmaster@akilae-saas.fr","Tracker calcul de prix MIM B2C","Mises à jour : ".$count,$headers); 
		}
		
		
			
	}
	
	?>
	<form name="recalculate" action="recalculate.php" method="post">
		<input type="submit" class="blueButton" name="calculate" value="Calculer" style="float:right;" />
	</form>
						<div class="clear"></div>

           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>
	
	<?

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 
?>
