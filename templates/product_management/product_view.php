<?php
	include_once( "../../objects/classes.php" );
	include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );
	include_once( "$GLOBAL_START_PATH/objects/Supplier.php" );
	
	if( !headers_sent() )
		header( "Content-Type: text/html; charset=utf-8" );
	
	$notemplate = true;
	
	include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );
	
	$lang = User::getInstance()->getLang();
?>

<style type="text/css">
	.floatleft { float:left; }
	.spacer { clear:both; }
	
	/* Promotions */
	.promos { position:absolute; width: 250px; color: #CC3434; font-weight: bold; font-size:11px; text-align: center; }
		
	/* Image & action sur l'article  */
	.left_product { float:left; width:230px; margin: 10px 0; }
	
	/* Miniature image */
	.miniature { float:left; width:230px; margin:5px 0 10px 0;  }
	.miniature img { float:left; margin:5px 5px 0 0; border:1px solid #CCCCCC; }
	
	/* Titre & description */
	.right_product { margin: 10px 5px; float:left; width:550px; }
	.right_product h1 { font-size:18px; color:#000000; float:left; width:390px; margin:0; }
	h1.title { font-size:14px; color:#000000; float:left; width:100%; margin:8px 0 0 0; border-bottom:1px dotted #CCCCCC; }
	.right_product h2.sousTitre { width:97%; font-size:13px; color:#000000; margin:10px 5px 5px 0; float:left; border-bottom:1px dotted #CCCCCC; }
	.right_product .htPrice { font-size:20px; color:#C50106; font-weight:bold; text-align:right; line-height:16px; text-decoration:none; padding:5px 5px 5px 0; float:right; width:110px; margin:0 0 5px 0; }
	.right_product .htPrice .viewApartir { font-size:12px !important; display:block; }
	.description, .description p , .description div, .right_product p { font-size:12px; color:#505050; line-height:18px; text-align:justify;  } 
	
	/* Controle masquer */
	.subContent { padding:0 !important; }
	
	/* Tableau caractéristique */
	.blocTabReference { overflow-x:auto; overflow-y:hidden; width:780px;  margin:5px; }
	.tabReference { font-size:12px; color:#505050; line-height:18px; width:100%;  }
	.tabReference th.entete { background:url(../../images/front_office/global/bg-entete.jpg) no-repeat right bottom #FFFFFF !important; }
	.tabPrice{ color:#C50106; font-weight:bold; }
	.tabReference td, .tabReference th { border-bottom:1px solid #EEEEEE; padding:4px 3px; }
	
</style>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_view.php</span>
<?php } ?>

<div class="left_product">	
	<!-- Affichage image produit -->
	<center>
		<img src="<?php echo URLFactory::getProductImageURI( $tableobj->get( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, 200, 200 ); ?>" alt="<?php echo $tableobj->get("name_1"); ?>" />				
	</center>
	<div class="spacer"></div>
	
</div>
<div class="right_product">
	<?php
		$rs =& DBUtil::query( "SELECT name FROM supplier WHERE idsupplier = '".$tableobj->get( "idsupplier" )."'" );
		$nameSupplier = $rs->fields( "name" );
	?>
	<!-- Titre -->
	<h1><?php echo $tableobj->get("name_1"); ?></h1>
	<div class="spacer"></div>
	
	<!-- Fournisseur -->
	<p class="departUsine">
		&#0155; Fournisseur : <?php echo $nameSupplier; ?><br/>
		<?php if( $tableobj->get( "available" ) ) { ?>
			&#0155; Produit <strong>affiché</strong> sur le site internet.
		<?php } else { ?>	
			&#0155; Produit <strong>non affiché</strong> sur le site internet.
		<?php } ?>
	</p>
	<div class="spacer"></div><br/>
				
	<!-- Texte commercial -->
	<div class="textCommercial">
		<?php 
		$wysiwyg = new WYSIWYGEditor( "text_commercial_1" );
		$wysiwyg->setHTML( $tableobj->get( "text_commercial_1" ) );
		$wysiwyg->setDimension( 0, 30 );
		$wysiwyg->display(  );
		?>
	</div><br/>
	
	<!-- Description -->
	<div class="spacer"></div>
	
	<div class="description">
		<?php

		$wysiwyg = new WYSIWYGEditor( "description$lang" );
		$wysiwyg->setHTML( $tableobj->get( "description$lang" ) );
		$wysiwyg->setDimension( 0, 0 );
		$wysiwyg->display(  );

		?>
		<br style="clear:both;" />
	</div>
</div>
<div class="spacer"></div>

<?php $references = getReferences($tableobj->get( "idproduct" )); ?>
<?php $headings = getHeadings($tableobj->get( "idproduct" )); ?>
		
<script type="text/javascript">

	function hideHeading( idproduct, idintitule_title ){
	
		var available = $( "#Available_" + idintitule_title ).attr( "value" ) == "1" ? "0" : "1";
		var data = "available=" + available + "&idproduct=" + idproduct + "&idintitule_title=" + idintitule_title;
		
		$.ajax({
	
			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?hide",
			async: true,
			cache: false,
			type: "GET",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'afficher/masquer l'intitulé" ); },
		 	success: function( responseText ){
	
				if( responseText == "1" ){
	
					$( "#Available_" + idintitule_title ).attr( "value", available );
					$( "#Available_" + idintitule_title ).parent().parent().css( "background-color", available == "1" ? "#FFFFFF" : "#FAFAFA" );
					$( "#AvailableIcon_" + idintitule_title ).attr( "src", available == "1" ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif" );
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
	
				}
				else alert( "Impossible d'afficher/masquer l'intitulé" );
	
			}
	
		});
		
	}
</script>
<h1 class="title">Caractéristiques techniques</h1>
<div class="blocTabReference">
	<table class="tabReference" border="0" cellpadding="0" cellspacing="0">
	<tr> 
		<td></td>
		<th class="entete" style="width:150px;"></th>
		<?php foreach( $references as $reference ){ ?>
			<th class="entete" align="left"><span class="tabPrice">Référence&nbsp;:&nbsp;<?php echo htmlentities( $reference[ "reference" ] ); ?>&nbsp;</span><br/>
			Départ&nbsp;usine&nbsp;sous&nbsp;<?php echo getDelays( $reference[ "delivdelay" ] ); ?>&nbsp;</th>
		<?php } ?>	
	</tr>
	<tr> 
		<td></td>
		<th class="tabPrice" align="left" style="width:150px; background:#F9F9F9;">Prix HT</th>
		<?php foreach( $references as $reference ){ ?>
			<td class="tabPrice" align="left" style="background:#F9F9F9;"><?php echo htmlentities( $reference[ "sellingcost" ] ); ?>&euro;</td>
		<?php } ?>
	</tr>
	<?php displayHeadingsVertical( $headings, $references ); ?>
	
	
	</table>
</div>
<div class="spacer"></div>

<?php

//---------------------------------------------------------------------------------------------
function getReferences( $idproduct ){
	
	global 	$lang;
			
	$references = array();
	
	$selectedColumns = array( 
	
		"idarticle", 
		"reference",
		"available",
		"ref_supplier",
		"display_detail_order",
		"buyingcost",
		"sellingcost",
		"suppliercost",
		"ratecost",
		"idvat",
		"code_customhouse",
		"hidecost",
		"franco",
		"weight",
		"delivdelay",
		"stock_level",
		"minstock",
		"unit",
		"lot",
		"min_cde",
		"gencod",
		"overhead_charges",
		"indirect_expenses",
		"allowed_discount_bill",
		"garantee",
		"accept_nommenclature",
		"nomenclature",
		"weight_raw",
		"serial",
		"store",
		"span",
		"site"
	
	);
		
	$query = "
	SELECT " . implode( ", ", $selectedColumns ) . "
	FROM detail
	WHERE idproduct = '$idproduct'
	ORDER BY display_detail_order ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer références du produit" );
	
	$i = 0;
	while( !$rs->EOF() ){
		
		$references[ $i ] = array();
		
		foreach( $selectedColumns as $column )
			$references[ $i ][ $column ] = $rs->fields( $column );
		
		$rs->MoveNext();
		
		$i++;
		
	}

	return $references;
	
}


//---------------------------------------------------------------------------------------------
function getHeadings( $idproduct ){
	
	global 	$lang;
	
	$query = "
	 SELECT DISTINCT( il.idintitule_title ),
	 	il.available,
		ifam.idintitule_family,
		ifam.intitule_family$lang, 
		it.idintitule_title,
		it.intitule_title$lang
	FROM intitule_link il, 
		intitule_title it,
		intitule_family ifam
	WHERE il.idproduct = '$idproduct'
	AND il.idintitule_title = it.idintitule_title
	AND il.idfamily = ifam.idintitule_family
	ORDER BY il.idfamily ASC, il.family_display ASC, il.title_display ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les intitulés et leurs familles" );
		
	$headings = array();
	
	while( !$rs->EOF() ){
		
		$idintitule_family 	= $rs->fields( "idintitule_family" );
		$idintitule_title 	= $rs->fields( "idintitule_title" );
		$intitule_title 	= $rs->fields( "intitule_title$lang" );
		$available			= $rs->fields( "available" );
		
		$headings[ $idintitule_family ][ $idintitule_title ]['title'] = $intitule_title;
		$headings[ $idintitule_family ][ $idintitule_title ]['available'] = $available;
		
		$rs->MoveNext();
		
	}
	
	return $headings;
	
}

//---------------------------------------------------------------------------------------------
function getIntituleValue( $idintitule_value ){
	
	global $lang;
	
	if( !$idintitule_value )
		return "";

	$query = "
	SELECT intitule_value$lang AS value
	FROM intitule_value
	WHERE idintitule_value = '$idintitule_value'
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );

	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer la valeur de l'intitulé '$idintitule_title' pour l'article '$idarticle'" );
		
	return $rs->fields( "value" );
	
}

//---------------------------------------------------------------------------------------------
function getDelays( $selection ){
	
	global $lang;
	
	$query = "SELECT delay$lang AS delay FROM delay
	WHERE `iddelay` = '$selection'";
	
	$rs =& DBUtil::query( $query );
	if( $rs === false )
		die( "Impossible de récupérer la liste des délais de livraison" );
		
	return str_replace(' ', '&nbsp;', $rs->fields( "delay" ));
	
}

//---------------------------------------------------------------------------------------------
//Retourne un tableau des libellés des intitulés (caractéristiques)
function displayHeadings( &$headings, &$references ){
	
	foreach( $headings as $idintitule_family => $intitule_titles ){
		
		foreach( $intitule_titles as $idintitule_title => $intitule_title ){ ?>
			<th><?php echo htmlentities( $intitule_title[ 'title' ] ) ?></th>
		<?php }
		
	}
	
}

//---------------------------------------------------------------------------------------------
//Retourne un tableau des valeurs des intitulés (caractéristiques) pour un idarticle
function displayValue( $idarticle, &$reference, &$headings, $bgcolor ){
	
	foreach( $headings as $idintitule_family => $intitule_titles ){
		
		foreach( $intitule_titles as $idintitule_title => $intitule_title ){
		
			$idintitule_value = getIDIntituleValue( $idarticle, $idintitule_title );
			$intitule_value = $idintitule_value ? getIntituleValue( $idintitule_value ) : ""; ?>
			<td align="center" style="background:<?php echo $bgcolor; ?>"><?php echo htmlentities( $intitule_value ) ?></td>
		<?php }
		
	}
	
}

//---------------------------------------------------------------------------------------------
//Retourne un tableau des libellés + valeurs des intitulés (caractéristiques)
function displayHeadingsVertical( &$headings, &$references ){
	global $tableobj;
	
	foreach( $headings as $idintitule_family => $intitule_titles ){
		$i=0;
		foreach( $intitule_titles as $idintitule_title => $intitule_title ){ ?>
			<tr>
				<?php if ($i%2) $bgcolor="#F9F9F9;"; else $bgcolor="#FFFFFF;"; ?>
				
				<td style="text-align:center">
					<span class="available">
						<img id="AvailableIcon_<?php echo $idintitule_title; ?>" onclick="hideHeading( <?php echo $tableobj->get( "idproduct" ); ?>, <?php echo $idintitule_title; ?> );" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $intitule_title[ 'available' ] ? "visible.gif" : "hidden.gif"; ?>" alt="" />
						<input type="hidden" id="Available_<?php echo $idintitule_title; ?>" value="<?php echo $intitule_title[ 'available' ] ? "1" : "0"; ?>" />
					</span>				
				</td>
				
				<th align="left" style="background:<?php echo $bgcolor;?>;"><?php echo htmlentities( $intitule_title[ 'title' ] ) ?></th>
				
				<?php foreach( $references as $reference ){
					
					
					$idarticle = $reference[ "idarticle" ];
					$idintitule_value = getIDIntituleValue( $idarticle, $idintitule_title );
					$intitule_value = $idintitule_value ? getIntituleValue( $idintitule_value ) : ""; ?>
					
					<td class="characValue" style="background:<?php echo $bgcolor;?>"><?php echo htmlentities( $intitule_value ) ?></td>
				<?php } ?>
				
				<?php $i++ ?>
			</tr>
			<?php
		
		}
	}
}


?>



