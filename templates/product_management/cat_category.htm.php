<?php
/** 
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire pour la Gestion des Catégories
*/
// Récupére la session pour uploadify
if( isset( $_REQUEST['sid_uploadify'] ) )
{
    @session_id(  $_REQUEST['sid_uploadify']   );
    @session_start();
}

define( "THUMB_SIZE", 150 );
define( "PICTOGRAM_SIZE", 120 );

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression image banniére - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "banner" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$ret = DBUtil::query( "UPDATE category SET banner = '' WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload image banniére - ajax */

if( isset( $_FILES[ "banner" ] ) && strlen( $_FILES[ "banner" ][ "tmp_name" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
	$target =  $_SERVER[ "DOCUMENT_ROOT" ] . "/images/categories/banners/" . $_FILES[ "banner" ][ "name" ];

	@move_uploaded_file( $_FILES[ "banner" ][ "tmp_name" ], $target );
	@chmod( $target, 0755 );
	
	$ret = DBUtil::query( "UPDATE category SET banner = " . DBUtil::quote( $_FILES[ "banner" ][ "name" ] ) . " WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "/images/categories/banners/" . $_FILES[ "banner" ][ "name" ] : "" );
	

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* trier les pages statiques liées à une catégorie - ajax */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "custom_pages" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$ret = true;
	$index = 0;
	foreach( $_REQUEST[ "CustomPage" ] as $idpage ){
		
		$ret &= DBUtil::query( "UPDATE category_custom_pages SET `index` = '$index' WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "' AND  idpage = '$idpage' LIMIT 1" ) !== false;
		$index++;
		
	}
	
	exit( $ret ? "1" : "0" );
	
}

/*CustomPage[]=15&CustomPage[]=17*/
/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* lier une page statique à une catégorie - ajax */

if( isset( $_REQUEST[ "add" ] ) && $_REQUEST[ "add" ] == "custom_page" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$rs = DBUtil::query( "SELECT MAX( `index` ) + 1 AS `index`,idcategory FROM category_custom_pages WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "'" );
	
	$index = $rs->fields( "index" ) == NULL ? 1 : $rs->fields( "index" );
	
	if ($rs->fields( "index" ) != NULL)
	{
		$ret = DBUtil::query(
		"UPDATE category_custom_pages SET idpage = '" . intval( $_REQUEST[ "idpage" ] ) . "' WHERE idcategory = '" . intval( $rs->fields( "idcategory" )) . "'" ) !== false;
	}else
	{	
		$ret = DBUtil::query(
		
			"INSERT INTO category_custom_pages VALUES(
				'" . intval( $_REQUEST[ "idcategory" ] ) . "',
				'" . intval( $_REQUEST[ "idpage" ] ) . "',
				'$index'
			)" ) !== false;
	}
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* supprimer une page statique liée à une catégorie - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "custom_page" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$ret = DBUtil::query( "DELETE FROM category_custom_pages WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "' AND idpage = '" . intval( $_REQUEST[ "idpage" ] ) . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des pages statiques - ajax */

if( isset( $_REQUEST[ "get" ] ) && $_REQUEST[ "get" ] == "custom_pages" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	listCustomPages( intval( $_REQUEST[ "idcategory" ] ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* réinitialisation compteur de visites - ajax */

if( isset( $_REQUEST[ "reset" ] ) && $_REQUEST[ "reset" ] == "visit_count" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$ret = DBUtil::query( "UPDATE product SET hits = 0 WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* réinitialisation compteur de visites - ajax */

if( isset( $_REQUEST[ "get" ] ) && $_REQUEST[ "get" ] == "visit_count" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	listMostVisitedProducts( intval( $_REQUEST[ "idcategory" ] ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* ajout d'une promotion - ajax */

/*if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "promotion" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$ret = DBUtil::query( "DELETE FROM promo WHERE idpromo = '" . intval( $_REQUEST[ "idpromo" ] ) . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}
*/

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* ajout d'un produit star - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "star_product" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$ret = DBUtil::query( "DELETE FROM star_product WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "' AND idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* ajout d'un produit star */

if( isset( $_REQUEST[ "add" ] ) && $_REQUEST[ "add" ] == "star_product" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$rs = DBUtil::query( "SELECT MAX( `index` ) + 1 AS `index` FROM star_product WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "'" );
	
	$index = $rs->fields( "index" ) == NULL ? 0 : $rs->fields( "index" );
	
	$ret = DBUtil::query( "INSERT IGNORE INTO star_product VALUES( '" . intval( $_REQUEST[ "idcategory" ] ) . "', '" . intval( $_REQUEST[ "idproduct" ] ) . "', '$index' )" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des produits star */

if( isset( $_REQUEST[ "get" ] ) && $_REQUEST[ "get" ] == "star_products" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	listStarProducts( intval( $_REQUEST[ "idcategory" ] ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* publicité XML - ajax */

if( isset( $_REQUEST[ "get" ] ) && $_REQUEST[ "get" ] == "ad" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$query = "
	SELECT ad.*, ca.`index`
	FROM category_ad ca, ad
	WHERE ca.idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "'
	AND ca.idad = '" . intval( $_REQUEST[ "idad" ] ) . "'
	AND ca.idad = ad.idad
	GROUP BY ad.idad
	ORDER BY ca.`index` ASC";
	
	$rs =& DBUtil::query( $query );
	
	$document = new DomDocument( "1.0", "utf-8" );
	
	$root = $document->createElement(  Util::doNothing( "ad" ) );
	
	$elements = array( "idad", "name", "description", "start_date", "end_date", "index", "enabled" );
	
	foreach( $elements as $elements ){
		
		$domElement = $document->createElement(  Util::doNothing( $elements ) );
		$cddata = $document->createCDATASection(  Util::doNothing( $rs->fields( $elements ) ) );
		$domElement->appendChild( $cddata );
		$root->appendChild( $domElement );
	
	}
	
	$document->appendChild( $root );
	
	header( "Content-Type: text/xml;charset=utf-8" );
	
	echo $document->saveXML();
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* images d'une publicité - ajax */

if( isset( $_REQUEST[ "get" ] ) && $_REQUEST[ "get" ] == "ad_images" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	listAdImages( intval( $_REQUEST[ "idad" ] ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* tri des images de publicités - ajax */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "ad_images" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = true;

	$index = 0;
	foreach( $_REQUEST[ "AdImageIndexes" ] as $useless => $idad_image ){
	
		$ret &= DBUtil::query( "UPDATE ad_image SET `index` = '" . intval( $index ) . "' WHERE idad_image = '" . intval( $idad_image ) . "' LIMIT 1" ) !== false;
		$index++;
		
	}

	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload image publicité - ajax */

if( isset( $_FILES[ "AdImage" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
	$idad_image = DBUtil::getUniqueKeyValue( "ad_image", "idad_image" );
	
	$rs = DBUtil::query( "SELECT MAX( `index` ) + 1 AS `index` FROM ad_image WHERE idad = '" . intval( $_REQUEST[ "idad" ] ) . "'" );
	$index = $rs->fields( "index" ) == NULL ? 0 : $rs->fields( "index" );
	
	$ext = strtolower( substr( $_FILES[ "AdImage" ][ "name" ] , strrpos( $_FILES[ "AdImage" ][ "name" ] , "." ) + 1 ) );
	
	$filename 	= "{$idad_image}_{$index}.$ext";
	$target 	=  $_SERVER[ "DOCUMENT_ROOT" ] . "/images/ads/$filename";

	move_uploaded_file( $_FILES[ "AdImage" ][ "tmp_name" ], $target );
	chmod( $target, 0755 );
	
	$query = "
	INSERT INTO ad_image( idad, filename, `index` )VALUES( 
		'" . intval( $_REQUEST[ "idad" ] ) . "',
		" . DBUtil::quote( $filename ) . ",
		'$index'
	)";

	exit( DBUtil::query( $query ) === false ? "0" : "1" );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression image publicité - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "ad_image" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = DBUtil::query( "DELETE FROM ad_image WHERE idad_image = '" . intval( $_REQUEST[ "idad_image" ] ) . "' LIMIT 1" );
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression image publicité - ajax */

if( isset( $_REQUEST[ "create" ] ) && $_REQUEST[ "create" ] == "ad" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	DBUtil::query( "INSERT INTO ad( name ) VALUES( 'nouvelle publicité' )" );
	
	$idad = DBUtil::getInsertID();
	
	$rs = DBUtil::query( "SELECT MAX( `index` ) + 1 AS `index` FROM category_ad WHERE idcategory = '" . intval( $_GET[ "idcategory" ] ) . "'" );
	$index = $rs->fields( "index" ) == NULL ? 0 : $rs->fields( "index" );
	
	DBUtil::query( "INSERT INTO category_ad( idcategory, idad, `index` ) VALUES( '" . intval( $_GET[ "idcategory" ] ) . "', '$idad', '$index' )" );
	
	exit( strval( $idad ) );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* supprimer une publicités - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "ad" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	/* suppression du lien catégorie-pub */
		
	$ret = DBUtil::query( "DELETE FROM category_ad WHERE idad = '" . intval( $_REQUEST[ "idad" ] ) . "'" ) !== false;
	$ret &= DBUtil::query( "DELETE FROM ad WHERE idad = '" . intval( $_REQUEST[ "idad" ] ) . "' LIMIT 1" ) !== false;
		
		/* suppression des images */
		
	$rs = DBUtil::query( "SELECT filename FROM ad_image WHERE idad = '" . intval( $_REQUEST[ "idad" ] ) . "'" );
	while( !$rs->EOF() ){
		
		if( strlen( $rs->fields( "filename" ) ) && file_exists( $_SERVER[ "DOCUMENT_ROOT" ] . "/images/ads/" . $rs->fields( "filename" ) ) )
			@unlink( $_SERVER[ "DOCUMENT_ROOT" ] . "/images/ads/" . $rs->fields( "filename" ) );
			
		$rs->MoveNext();
		
	}
		
	$ret &= DBUtil::query( "DELETE FROM ad_image WHERE idad = '" . intval( $_REQUEST[ "idad" ] ) . "'" ) !== false;

	exit( $ret ? "1" : "0" );
	
}


/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload images produit */

if( isset( $_FILES[ "image" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
		if( !User::getInstance()->getId() )
		exit( "session expirée" );
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . $_REQUEST[ "folder" ] . "/";
	$ext = strtolower( substr( $_FILES[ "image" ][ "name" ] , strrpos( $_FILES[ "image" ][ "name" ] , "." ) + 1 ) );
	$basename = intval( $_REQUEST[ "index" ] ) ? $_REQUEST[ "idcategory" ] . "." . intval( $_REQUEST[ "index" ] ) : $_REQUEST[ "idcategory" ];
	$targetFile =  str_replace( "//", "/", $targetPath ) . $basename . ".$ext";

	$files = glob( $targetPath . $basename . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	foreach( $files as $file )
		unlink( $file );

	move_uploaded_file( $_FILES[ "image" ][ "tmp_name" ], $targetFile );
	chmod( $targetFile, 0755 );
	
	exit( URLFactory::getCategoryImageURI( $_REQUEST[ "idcategory" ], URLFactory::$IMAGE_CUSTOM_SIZE, intval( $_REQUEST[ "index" ] ), THUMB_SIZE, THUMB_SIZE ) );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

include_once( "../../objects/Util.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );
	
if( !count( $_FILES ) ){ //@todo : bug plugin Ajax?? l'encodage est différent selon que des fichiers soient postés ou non
	
	$_POST 		= Util::arrayMapRecursive( "Util::doNothing", $_POST );
	$_REQUEST 	= Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	$_GET 		= Util::arrayMapRecursive( "Util::doNothing", $_GET );

}

//--------------------------------------------------------------------------------------------------------
/* liste des pictos menu front office - ajax */

if( isset( $_GET[ "menu_images" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	listMenuImages( intval( $_GET[ "idcategory" ] ) );
	
	exit();
	
}

//--------------------------------------------------------------------------------------------------------
/* suppression image menu */

if( isset( $_GET[ "delete" ] ) && $_GET[ "delete" ] == "menu_image" ){

	include_once( "../../config/init.php" );
	
	DBUtil::query( "UPDATE category SET image_menu WHERE idcategory = '" . intval( $_GET[ "idcategory" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* tri des sous-catégories et produits - ajax */

if( isset( $_POST[ "sort" ] ) ){
	
	include_once( "../../config/init.php" );
	
	//sous-catégories
	
	if( isset( $_POST[ "Categories" ] ) ){
		
		$done = true;
		$i = 0;
		while( $i < count( $_POST[ "Categories" ] ) ){
			
			$idchild = intval( $_POST[ "Categories" ][ $i ] );
			$rs =& DBUtil::query( "UPDATE category_link SET displayorder = '$i' WHERE idcategorychild = '$idchild' LIMIT 1" );

			$done &= $rs !== false;
			
			$i++;
			
		}
		
		exit( $done ? "1" : "0" );
		
	}
	
	//produits
	
	if( isset( $_POST[ "Products" ] ) ){
		
		$done = true;
		$i = 0;
		while( $i < count( $_POST[ "Products" ] ) ){
			
			$idchild = intval( $_POST[ "Products" ][ $i ] );
			$rs =& DBUtil::query( "UPDATE product SET display_product_order = '$i' WHERE idproduct = '$idchild' LIMIT 1" );

			$done &= $rs !== false;
			
			$i++;
			
		}
		
		exit( $done ? "1" : "0" );
		
	}
	
	exit( "0" );

}

//--------------------------------------------------------------------------------------------------------
?>
<link rel="stylesheet" href="/js/jquery/ui/fancybox/jquery.fancybox-1.3.1.css" type="text/css" media="screen" />
<script type="text/javascript" src="/js/jquery/ui/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
<style type="text/css">
/* <![CDATA[ */


	div#global div#globalMainContent, 
	div#footer div#top div.right-site, 
	div#footer div#top div.left-site,
	.mainBackMenu { background:none !important; }

	div#global div#header div#menu { background:url(../../images/back_office/layout/bg-blue-menu.gif) repeat-x; }
	div#global div#globalMainContent{width: 1060px; margin-left: auto; margin-right: auto; }
	div#global div#globalMainContent div.mainContent{ width: 820px; }
	
	.ui-tabs .ui-tabs-nav {margin:20px 0 0 0 !important; border-bottom:1px solid #E6E6E6; }
	.ui-tabs .ui-tabs-panel { border-bottom:1px solid #E6E6E6; border-left:1px solid #E6E6E6; border-right:1px solid #E6E6E6; margin-bottom:10px; }
	div.mainContent div.content div.subContent { margin-top:0; padding:5px; }

	div.mainContent div.content div.subContent table.dataTable td, 
	div.mainContent div.content div.subContent table.dataTable th { border:1px solid #FFFFFF !important; }
	div.mainContent div.content div.subContent tr.filledRow, 
	div.mainContent div.content div.subContent th.filledCell { background:#EEEEEE !important; }
	
	/* outils */
	
	ul#ToolList{
		width:172px;
		margin:10px auto 0px auto;
		padding:0px;
		text-align:center;
		list-style-type:none;
	}
	
	ul#ToolList li{
		float:left;
		width:50px;
		margin:2px;
	}
	
	/* onglets */

	.ui-tabs .ui-tabs-nav li{ width: auto; } 

	/* patch tinyMCE vs Akilaé */
	
	div.mainContent div.content div.subContent table.dataTable td td,
	div.mainContent div.content div.subContent table.dataTable th th{
		border:0 none;
		margin:0;
		padding:0;
	}
	
	/* custom */
	
	.ui-tabs .ui-tabs-nav{ margin:20px auto 20px auto; }

	/* siblings */
	
	#CategorySiblingsContainer #CategorySiblings{ 
	
		margin:15px auto 5px auto; 
		padding:0px;
		list-style-type:none;
		
	}
	
	#CategorySiblingsContainer #CategorySiblings .placeholder{
		border:1px solid #E7E7E7;
		margin:4px auto 4px auto;
		background-color:#F3F3F3;
	}
	
	#CategorySiblingsContainer #CategorySiblings li{ 
		text-align:left; 
		margin:10px auto;
		height:40px;
	}
	
	#CategorySiblingsContainer #CategorySiblings li img{ border:1px solid #E7E7E7; }
	
	/* patch tinyMCE vs Akilaé */
	
	div.mainContent div.content div.subContent table.dataTable td td,
	div.mainContent div.content div.subContent table.dataTable th th{
		border:0 none;
		margin:0;
		padding:0;
	}
	
	td.showDragHandle{
		background-image: url(../../images/back_office/content/updown.gif);
		background-repeat: no-repeat;
		background-position: left;
		cursor: move;	
	}
/* ]]> */
</style>
<?php

WYSIWYGEditor::init( false );

?>
<script type="text/javascript">
/* <![CDATA[ */

    $(document).ready(function() { 
					
		var options = {
		
			beforeSubmit:  	categoryFormPreSubmitCallback,  // pre-submit callback 
			success:   		categoryFormPostSubmitCallback  // post-submit callback 

		};
		$('#myform').ajaxForm( options )
.ajaxStop(function(){
			setInterval(function(){ $.unblockUI(); }, 5000);

		})
		;
		
	});
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function explore(){
	
		$.ajax({

			url: "/product_management/search/explorer.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'explorateur" ); },
		 	success: function( responseText ){

				$( "#globalMainContent" ).append( '<div id="ExplorerDialog"></div>' );
				$("#ExplorerDialog").html( responseText );
        		$("#ExplorerDialog").dialog({
        		
        			title: "Explorateur",
        			width:  800,
        			height: 600,
        			close: function(event, ui) { $("#ExplorerDialog").dialog( 'destroy' ); $("#ExplorerDialog").remove(); }
        		
        		});
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	/*function automator(){

		if( $( "#AutomatorContainer" ).length )
			return;
			
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/search/automator.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&ondestroy=destroyAutomator",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'explorateur" ); },
		 	success: function( responseText ){

				$( "#tools" ).hide();
				$( "#CategoryContainer" ).hide();
				$( ".mainContent" ).css( "width", "100%" );
				$( responseText ).insertBefore( $( "#CategoryContainer" ) );
				
			}

		});
		
	}
	*/
	/* ------------------------------------------------------------------------------------------------------------ */
	
/*	function destroyAutomator(){
		
		$( "#AutomatorContainer" ).remove();
		$( ".mainContent" ).css( "width", "820px" );
		$( "#CategoryContainer" ).show();
		$( "#tools" ).show();
		
	}
	*/
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function createCategoryChild( type ){
	
		var explorerURL = "<?php echo $GLOBAL_START_URL ?>/product_management/search/catalog_tree.php?hide=products&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&onSelectCategoryCallback=";

		if( type == "product" )
			explorerURL += "createCategoryProduct&selectable=leafs";
		else explorerURL += "createSubcategory";
		
		$.ajax({

			url: explorerURL,
			async: false,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger la liste des catégories" ); },
		 	success: function( responseText ){
	
				$( "#globalMainContent" ).append( '<div id="CategoryParentDialog"></div>' );
				
				$("#CategoryParentDialog").html( responseText );
        		$("#CategoryParentDialog").dialog({
        		
        			title: "Sélection de la catégorie parente",
        			width:  500,
        			height: 500,
        			close: function(event, ui) { 
        			
        				$("#CategoryParentDialog").dialog( 'destroy' ); 
        				$("#CategoryParentDialog").remove();
        				
        			}
        		
        		});
        		
			}

		});
	
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function createSubcategory( idcategoryparent ){

		$("#CategoryParentDialog").dialog( "close" );
		$("#CategoryParentDialog").remove();

		document.location = "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?create&idcategoryparent=" + idcategoryparent;
	
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function createCategoryProduct( idcategoryparent ){
	
		$("#CategoryParentDialog").dialog( "close" );
		$("#CategoryParentDialog").remove();
		
		document.location = "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?create&idcategory=" + idcategoryparent;
	
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function categoryFormPreSubmitCallback( formData, jqForm, options ){	
	
		$( "#CategoryThumb" ).attr( "src", "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/cat_category.htm.php?thumbnail&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>" );
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function categoryFormPostSubmitCallback( responseText, statusText ){
		//if( responseText != "" )
		//	alert( responseText );
		
			
			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
			$.blockUI.defaults.css.fontSize = '12px';
	    	//$.growlUI( '', 'Modifications enregistrées' );
	    	$.blockUI({ 
    		css: { backgroundColor: '#9d0b5d', color: '#fff'},
    		message: '<h1> Modifications enregistrées... </h1>' 

    	});
		
		updateImage();
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function updateImage(){
		
		/* image principale */
		
		if( $( "#CategoryThumb" ).size() )
			$( "#CategoryThumb" ).attr( "src", "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/cat_category.htm.php?thumbnail&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&w=120&h=120" );

		/* pictos menu front office */

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/cat_category.htm.php?menu_images&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer la liste des images" ); },
		 	success: function( responseText ){
                $("#MenuImages").html( responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	$( document ).ready( function() { 
		
		$('#tabsContainer').tabs( {selected: <?php echo isset( $_GET[ "tab" ] ) ? intval( $_GET[ "tab" ] ) : "0"; ?>} ); 
		
	} );

	/* -------------------------------------------------------------------------------------------------- */
	
	function updateAvailableReferenceCount(){
		
		$("#AvailableReferenceCount").html( '<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/small_black_spinner.gif" alt="" />' );
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&updateAvailableReferenceCount",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de recalculer le nombre de références disponibles" ); },
		 	success: function( responseText ){

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
		    	$.growlUI( '', 'Modifications enregistrées' );
	    	
				$("#AvailableReferenceCount").html( responseText );
        		
			}

		});
		
	}
	
	/* -------------------------------------------------------------------------------------------------- */
	
	function updateAvailablePromotionCount(){
		
		$("#AvailablePromotionCount").html( '<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/small_black_spinner.gif" alt="" />' );
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&updateAvailablePromotionCount",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de recalculer le nombre de promotions" ); },
		 	success: function( responseText ){

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
		    	$.growlUI( '', 'Modifications enregistrées' );
	    	
				$("#AvailablePromotionCount").html( responseText );
        		
			}

		});
		
	}
	
	/* -------------------------------------------------------------------------------------------------- */
	
	function checkForm(){
	
		if( document.getElementById( 'myform' ).elements[ 'idparent' ].value == '' ){
		
			alert( 'Vous devez obligatoirement sélectionner une catégorie parente' );
			
			return false;
			
		}
		
		if( document.getElementById( 'myform' ).elements[ 'name_<?php echo User::getInstance()->getLang() ?>' ].value == '' ){
		
			alert( '<?php echo Dictionnary::translate( "category_name_required" ) ?>' );
			
			return false;
			
		}

		return true;
		
	}

	/* -------------------------------------------------------------------------------------------------- */

	function catset(formname) {
		window.open('cat_select_parent.php?po=1&formname='+formname,'choose_category','width=400,height=300,toolbar=0,location=0,directories=0,statusbar=0,menubar=0,scrollbars=1,resizable=1');
	}
	
	function catpopup(po) {
		window.open('cat_popup.php?po='+po,'cat_popop','width=350,height=400,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}
	
	function mask_search() {
		window.open('../mask/search.html','mask_search','width=580,height=700,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}
	
	function mask_product() {
		window.open('../mask/product.html','mask_product','width=580,height=700,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}
	
	function mask_article() {
		window.open('../mask/article.html','mask_article','width=580,height=700,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}
	
	function mask_article_pdf() {
		window.open('../mask/product_pdf.html','mask_article_pdf','width=580,height=700,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}
	
	function resetParentCategory(){

		document.getElementById( 'myform' ).elements[ 'idparent' ].value = '<?php echo $idparent ?>';
		document.getElementById( 'myform' ).elements[ 'parentName' ].value = '<?php echo str_replace( "'", "\'", $parentName ) ?>';
		
	}
	
	function getIntitulesFilter(){
		$.ajax({
			 	
			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/filter_creator.php?IdCategory=<?=$tableobj->get( "idcategory" )?>",
			async: true,
			type: "GET",
			data: null,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Erreur lors du chargement du contenu" ); },
		 	success: function( responseText ){

				if( responseText.length == 0 ){
					
					alert( "Cette catégorie ne contient aucun produit utilisant d'intitulés" ); 
					
				}
				
        		$( "#filter" ).html( responseText );

			}

		});
	
	}
	
	/* -------------------------------------------------------------------------------------------------- */
		
/* ]]> */
</script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablednd_0_5.js"></script>
<div id="globalMainContent">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/cat_category.htm.php</span>
<?php } ?>

	<div id="tools" class="leftTools">
		<div class="leftMenu">
			<h2 class="title">Vos outils</h2>
			<ul id="leftMenuList">
				<li>
					<a href="#" onclick="explore(); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_search.png" alt="" />
						Explorer
					</a>
				</li>
				<!--<li>
					<a href="#" onclick="automator(); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_automator.png" alt="" />
						Automate
					</a>
				</li>-->
				<li>
					<a href="#" onclick="createCategoryChild( 'category' ); return false;">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="" />
						Créer catégorie
					</a>
				</li>
				<li>
					<a href="#" onclick="createCategoryChild( 'product' );">
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create2.png" alt="" />
						Créer produit
					</a>
				</li>
				<?php
				
					if( $tableobj->get( "idcategory" ) ){
						
						?>
	 	       			<li>
	 	       				<a href="#content" onclick="$('#tabsContainer').tabs( 'select', 1 );">
	 	       					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_siblings.png" alt="" />
	 	       					Voir le contenu
	 	       				</a>
	 	       			</li>
	 	       			<li>
	 	       				<a href="<?php echo URLFactory::getCategoryURL( $tableobj->get( "idcategory" ) ); ?>" onclick="window.open( this.href ); return false;">
	 	       					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_preview.png" alt="" />
	 	       					Aper&ccedil;u
	 	       				</a>
	 	       			</li>
	 	       			<?php
	 	       			
					}
					
				?>
				
	   		</ul>
	   		<br /><br /><br /><br />
       		
				<?php
					$requestedURL = isset( $_REQUEST[ "logout" ] ) ? "$GLOBAL_START_URL/admin.php" : $GLOBAL_START_URL . $_SERVER[ "REQUEST_URI" ];
					//print $requestedURL;
					
					?>
                   <ul style=" padding: 30px 3px 0;">
                   
                    
                    <form name="LoginForm" id="loginForm" method="post" action="<?php echo $requestedURL ?>" enctype="multipart/form-data">                     
					
                    <?php
					$query = "SELECT * FROM language";
					$rs =& DBUtil::query( $query );
                    
					while( !$rs->EOF() ){
					
					$id_lang	= $rs->fields( "id_lang" );
					$name		= $rs->fields( "name" );
					$designation		= $rs->fields( "designation" );
					$flag		= $rs->fields( "flag" );
					
					$q1 = "SELECT active FROM language_active WHERE idsession ='". $_COOKIE["FrontOfficeNetlogix"] ."' ";
					$rs1 =& DBUtil::query( $q1 );
					
					$active		= $rs1->fields( "active" );
					
					if ($active == $id_lang){
					?>
					<!--<span style="color:#FF0000; font-weight:bold;"> <?php echo  $designation; ?></span> --> 
                   <!-- <input type="submit" name="<?php echo $name ?>" style="background:url(../../images/flags/<?php echo  $flag; ?>) no-repeat center;" class="submitField flagLang" value="<?php echo $designation ?>"  /> <span style="color:#FF0000; font-weight:bold;"> <?php echo  $designation; ?></span> &nbsp;-->
                   <?php
				   }else{
				   ?>
                   <!-- <input type="submit" name="<?php echo $name ?>" style="background:url(../../images/flags/<?php echo $flag; ?>) no-repeat center;" class="submitField flagLang" value="<?php echo $designation ?>"/><span style="font-weight:bold;"> <?php echo  $designation; ?></span>&nbsp;-->
					<?php
					}
					$rs->MoveNext();

					}
					?>
				</form>
		   	<div class="clear"></div>
		</div>
		<div class="leftMenu">
			<h2 class="title">Votre arborescence</h2>
		    <?php
		    
		    	/*if( $tableobj->get( "idcategory" ) )
					listSiblings( $tableobj->get( "idcategory" ) );*/
				//var_dump("$GLOBAL_START_URL/product_management/search/catalog_tree.php?hide=products&id=categoryNodes&idcategory=" . $tableobj->get( "idcategory" ));
				
				/* attribuez mot de passe et nom_utilisateur pour l'authentification de file_get_contents */
				$username = 'admin';
				$password = 'demo67';
				$context = stream_context_create(array(
					'http' => array(
					'header'  => "Authorization: Basic " . base64_encode("$username:$password")
					)
				));
				echo file_get_contents( "$GLOBAL_START_URL/product_management/search/catalog_tree.php?hide=products&id=categoryNodes&idcategory=" . $tableobj->get( "idcategory" ) ,false,$context);

		    ?>
		    <div class="clear"></div>
	    </div><!-- toolbox -->
    </div><!-- #tools -->
	<div class="mainContent borderProduct">
        <div class="content" id="CategoryContainer">		
		<?php
		
        	if( $action == "edit" ){
        		?>
	        	<p style="font-size:10px; margin:5px 5px 5px 0px;">
	        	<?php
	        	
	            	$treePath = getTreePath( $tableobj );
	
					$i = 0;
					while( $i < count( $treePath ) ){
						
						$treeNode = $treePath[ $i ];
						
						if( $i )
							echo " > ";
					
						echo "<a href=\"{$treeNode->href}\">{$treeNode->name}</a>";
						
						$i++;
						
					}
	
				?>
				</p>
				<?php	
        	}
        	?>
        	
        	<h1 class="maintitle">
            	<?php echo $tableobj->get( "idcategory" ) ? htmlentities( $tableobj->get( "name$lang" ) ) : "Créer une nouvelle catégorie"; ?>
		        (<span style=" color:#FF0000; font-size:10px;">
				<?php
                 	if( $action == "edit" )
              			echo "Cat. n. " . $tableobj->get( "idcategory" );	
                 ?>
				</span>)<!-- rightContainer -->
       		</h1>
            <div class="subContent">
			<?php

				if( isset( $_POST[ "DeleteSelection" ] ) || isset( $_POST[ "DeleteSelection_x" ] ) )
					deleteSelectedCategories();
				else if( isset( $_REQUEST[ "DeleteCategory" ] ) || isset( $_REQUEST[ "DeleteCategory_x" ] ) )
					deleteCategory();
				else if( $action == "search" )
					displaySearchResults();
				else displayCategoryForm();
		
			?>
			</div><!-- subContent -->
           	<div class="clear"></div>
   		</div><!-- .content #CategoryContainer -->
    </div><!-- mainContent -->
</div>
<?php

//------------------------------------------------------------------------------------------------------

function displayCategoryForm(){
	
	global 	$parentName,
			$idparent,
			$art,
			$action,
			$tableobj,
			$GLOBAL_START_URL;
	
	$langue = User::getInstance()->getLang();
	
	?>
	<form enctype="multipart/form-data" action="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php" method="post" id="myform" onsubmit="if( tinyMCE != null ) tinyMCE.triggerSave(true,true);">
	<?php
	
		switch( $action ){
		
			case "add" :
			
				?>
				<input type="hidden" name="append" value="1" />
				<?php
				
				break;
				
			case "edit" :
	
				?>
				<input type="hidden" name="update" value="1" />
				<input type="hidden" name="action" value="<?php echo $action ?>" />
				<input type="hidden" name="idcategory" value="<?php echo $tableobj->get( "idcategory" ); ?>" />
				<?php
				
				break;
			
		}
	
		?>
		<div class="clear" style="text-align:right; margin-top:10px; margin-bottom:10px;">
		<?php
			
			switch( $action ){
				
				case "edit" :
	
					?>
					<input type="button" name="" value="Supprimer" class="orangeButton" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette catégorie?' ) ) document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/index.php?delete&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>';" />
					<input type="submit" class="blueButton" name="edit" value="Sauvegarder" />
					<?php

					break;
		
				case "add" :
				
					?>
					<input type="submit" class="blueButton" name="add" value="Créer" />
					<?php
					
					break;
				
			}
				
			?>
		</div>
		<div style="float:left; width:605px;">
			<div class="tableContainer">
			<?php 
			$lang = User::getInstance()->getLang2();
			 $idlang=explode("_",$lang);
			$id_lang=$idlang[1];
			$query = "SELECT * FROM language where id_lang=$id_lang";
			$rs =& DBUtil::query( $query );
			$flag= $rs->fields( "flag" );
			
			$idcategory = $tableobj->get('idcategory');
			$name_link = DBUtil::getDBValue( "name_link", "category", "idcategory", $idcategory );
			$name_h1 = DBUtil::getDBValue( "name_H1", "category", "idcategory", $idcategory );
			$name_chemin = DBUtil::getDBValue( "name_chemin", "category", "idcategory", $idcategory );
			$name = DBUtil::getDBValue( "name$lang", "category", "idcategory", $idcategory );
            
            $str_category_name = $tableobj->get('name_1');
            if( $idcategory ==  0 ){
        
                $str_category_name = "Accueil";
            }
			
			?>

				<table class="dataTable">
					<tr>
						<th class="filledCell" style="width:200px;">Nom&nbsp;<!--<img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/Fr.png" style="width: 15px;
height: 10px;">--></th>
						<td>
							<input type="text" name="name_1" value="<?php echo $str_category_name; ?>" class="textInput" style="width:95%;float:left;"/><img src="/images/back_office/content/question.png" title="N'oubliez pas de cr&eacute;er une redirection" style="margin-left:5px;">
						</td>
					</tr>
					<?php if($lang!="_1"){?>
						<tr>
						<th class="filledCell" style="width:200px;">Nom (title)<!--<img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/<?php echo  $flag; ?>" style="width: 15px;
height: 10px;">--></th>
						<td>
							<input type="text" name="name<?php echo $lang;?>" value="<?php echo $name; ?>" class="textInput" <?php if ($tableobj->get('name$lang') ){ ?> disabled="disabled" <?php } ?> />
						</td>
					</tr>

					<?php } ?>
					
					
					<tr>
						<th class="filledCell" style="width:200px;">Nom menu</th>
						<td>
							<input type="text" name="name_link" value="<?php echo $name_link; ?>" class="textInput" />
						</td>
					</tr>
					
					
					<tr>
						<th class="filledCell" style="width:200px;">Nom H1</th>
						<td>
							<input type="text" name="name_h1" value="<?php echo $name_h1; ?>" class="textInput" />
						</td>
					</tr>
					
					
					<!--<tr>
						<th class="filledCell" style="width:200px;">Nom dans le chemin</th>
						<td>
							<input type="text" name="name_chemin" value="<?php echo $name_chemin; ?>" class="textInput" />
						</td>
					</tr>-->
					
					
					
					
					
					<tr>
						<th class="filledCell">Cat&eacute;gorie m&egrave;re</th>
						<td>
							<input type="hidden" name="idparent" id="idparent" value="<?php echo $idparent ?>" />
							<a style="float:right; margin-left:10px;" href="#" onclick="resetParentCategory(); return false;">Annuler</a>
							<a style="float:right;" href="#" onclick="window.open('category_popup.php?formID=myform&amp;elementID=idparent&amp;elementName=parentName','category_popup','width=260,height=300,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1'); return false;">Modifier</a>
							<input class="textInput" type="text" name="parentName" id="parentName" value="<?php echo $parentName ?>" readonly="readonly" style="width:250px; border:1px solid #EEEEEE;" />
						</td>
					</tr>
				</table>
			</div><!-- tableContainer -->
			<?php
			
				/* description */
			
					
					?>
					<div class="subTitleContainer">
					    <p class="subTitle">Description</p>
					</div>
					<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
					<?php
		
					$wysiwyg = new WYSIWYGEditor( "description$lang" );
					$wysiwyg->setHTML( $tableobj->get( "description$lang" ) );
					$wysiwyg->setDimension( 0, 100 );
					$wysiwyg->display();
		
					?>
					</div>
				</div>
		<div style="float:right; margin-top:10px;">
			<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
			<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
			<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
			<script type="text/javascript">
			/* <![CDATA[ */
			
				$( document ).ready( function(){

					var prefix = ""; /* patch pour forcer le rafraichissement de l'image sans utiliser le cache du navigateur */
					
					$( "#image" ).uploadify({
					
						'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
						'script'		: '/templates/product_management/cat_category.htm.php',
						'folder'		: '/images/categories',
						'scriptData'	: { 'idcategory': '<?php echo $tableobj->get( "idcategory" ); ?>', 'index': '0' , 'sid_uploadify' : '<?php echo session_id();?>'},
						'fileDataName'	: 'image',
						'buttonText' 	: "modifier",
						'buttonImg' 	: "/images/back_office/layout/bouttonPhoto.png", 
						'width'			: "115",
						'height'		: "32",
						'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
						'fileDesc'		: "Images",
						'auto'           : true,
						'multi'          : false,
						'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
						'onComplete' 	: function( event, queueID, fileObj, response, data ){
		
								prefix = "p" + prefix;
								$( "#ImageThumb" ).attr( "src", prefix + response );
								$( "a#ImageThumbFancyBox" ).attr( "href", prefix + $( "a#ImageThumbFancyBox" ).attr( "href" ) );
								
							}
		
					}); 

					$("a#ImageThumbFancyBox").fancybox();
					
				});
				
			/* ]]> */
			</script>
			<a id="ImageThumbFancyBox" href="<?php echo URLFactory::getCategoryImageURI( $tableobj->get( "idcategory" ), URLFactory::$IMAGE_MAX_SIZE ); ?>">
				<img id="ImageThumb" src="<?php echo URLFactory::getCategoryImageURI( $tableobj->get( "idcategory" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, 150, 150 ); ?>" alt="" />
			</a>
			<div style="text-align:center; margin-top:5px;">
                <!--<label for="image" class="label-file orangeButton">Choisir une image</label>-->
                <input type="file" id="image" class="orangeButton" value="ertertertert" style="display:none; width:150px;"/>
                <br/>
				<!--input type="file" id="image" class="orangeButton" value="" style="display: block; width:150px;" /-->
			</div>
		</div>
		<br style="clear:both;" />
		<div id="tabsContainer">
			<ul class="menu">
				<li class="item">
					<a href="#display">PERSONNALISATION</a>
				</li>
				<?php
				
					if( $action == "edit" ){
						
						?>
						<li class="item">
							<a href="#content">
							<?php
							
								if( DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '" . $tableobj->get( "idcategory" ) . "' LIMIT 1" )->RecordCount() )
									echo "SOUS-CATEGORIES";
								else if( DBUtil::query( "SELECT idproduct FROM product WHERE idcategory = '" . $tableobj->get( "idcategory" ) . "' LIMIT 1" )->RecordCount() )
									echo "PRODUITS";
								else echo "CONTENU";
								
							?>
							</a>
						</li>
						
						<?php
						
					}
				
				?>
				<li class="item">
					<a href="#referencing">REFERENCEMENT</a>
				</li>
				<!--<li class="item">
					<a href="#advertising">PRODUITS STARS</a>
				</li>-->
				<li class="item">
					<a href="#misc">CHIFFRES CLEFS</a>
				</li>
				<li class="item">
					<a href="#filter" onclick="getIntitulesFilter();">FILTRE</a>
				</li>
				<li class="item">
					<a href="/templates/product_management/category_stats.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>">
						<img src="/images/back_office/content/pie.png" alt="" />
					</a>
				</li>
			</ul>
			<div style="clear: both;"></div>
			<div class="subContent" id="display"><?php setDisplayTab( $tableobj, $action ); ?></div>
			<?php
			
				if( $action == "edit" ){
					
					?>
					<div class="subContent" id="content"><?php setContentTab( $tableobj ); ?></div>
					
					<?php
					
				}
				
			?>
			<div class="subContent" id="referencing"><?php setReferencingTab( $tableobj, $action ); ?></div>
			<div class="subContent" id="misc"><?php setMiscTab( $tableobj, $action ); ?></div>
			<div class="subContent" id="advertising"><?php displayAdvertising($tableobj, $action); ?></div>
			<div class="subContent" id="filter"></div>
			<div class="subContent" id="stats"></div>
		</div><!-- tabsContainer -->
		<p style="float:right;">
		<?php
			
			switch( $action ){
				
				case "edit" :
	
					?>
					<input type="button" name="" value="Supprimer" class="orangeButton" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette catégorie?' ) ) document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/index.php?delete&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>';" />
					<input type="submit" class="blueButton" name="edit" value="Sauvegarder" />
					<?php
					
					//affichage bouton supprimer
					
					$idcategory = $tableobj->get( "idcategory" );
					
					$query = "SELECT COUNT(*) AS childCount FROM category_link WHERE idcategoryparent = $idcategory";
					$hasChildren = DBUtil::query( "SELECT COUNT(*) AS childCount FROM category_link WHERE idcategoryparent = '$idcategory'" )->fields( "childCount" ) > 0;
					
					$query = "SELECT COUNT(*) AS productCount FROM product WHERE idcategory = $idcategory";
					$hasProducts = DBUtil::query( "SELECT COUNT(*) AS productCount FROM product WHERE idcategory = '$idcategory'" )->fields( "productCount" ) > 0;

					break;
		
				case "add" :
				
					?>
					<input type="submit" class="blueButton" name="add" value="Créer" />
					<?php
					
					break;
				
			}
			
		?>
		</p>
	</form>
	<?php

}

//------------------------------------------------------------------------------------------------------

function setDisplayTab( $tableobj, $action ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	?>
	<div class="tableContainer">
		<table class="dataTable">
		<?php
					
			if( DBUtil::getParameterAdmin('display_html') ){ 
				
				$rs =& DBUtil::query( "SELECT filename, CONCAT( '$GLOBAL_START_URL/', filename ) AS url FROM custom_pages ORDER BY filename ASC" );
				
				?>
				<tr>
					<th class="filledCell">Afficher une page web</th>
					<td>
					<?php
					
						if( $rs->RecordCount() ){
							
							?>
							<script type="text/javascript">
							/* <![CDATA[ */
							
								function customPagesDialog(){
								
									$("#CustomPagesDialog").dialog({
	        
								        modal: true,	
										title: "Liste des pages",
										width: 300,
										close: function(event, ui) { 
										
											$("#CustomPagesDialog").dialog( 'destroy' );
											$helper.fadeIn();
											
										}
									
									});
		
								}
							
							/* ]]> */
							</script>
							<div id="CustomPagesDialog" style="display:none;">
								<ul>
								<?php
								
									while( !$rs->EOF() ){
									
										?>
										<li>
											<a href="#" onclick="$( '#html' ).val( '<?php echo htmlentities( $rs->fields( "url" ) ); ?>' ); $( '#CustomPagesDialog' ).dialog( 'destroy' ); return false;">
												<?php echo htmlentities( $rs->fields( "filename" ) ); ?>
											</a>
										</li>
										<?php
											
										$rs->MoveNext();
										
									}
									
								?>
								</ul>
								<p style="text-align:right;">
									<input type="button" value="Annuler" onclick="$( '#CustomPagesDialog' ).dialog( 'destroy' );" />
								</p>
							</div>
							<?php
							
						}
						
						?>
						<input type="text" id="html" name="html" value="<?php echo $tableobj->get( "html" ); ?>" class="textInput" style="width:300px;" />
						<?php
						
						if( $rs->RecordCount() ){
							
							?>
							<a href="#" class="blueLink" onclick="customPagesDialog(); return false;">S&eacute;lectionner une pages personnalis&eacute;e</a>
							<?php
								
						}
						
					?>
					</td>
				</tr>
				<?php 
				
			}
			
			?>
			<!--<tr>		
				<th class="filledCell" width="200">Template cat&eacute;gorie</th>
				<td colspan="3">
				<?php 
				
					if( DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '" . $tableobj->get( "idcategory" ) . "' LIMIT 1" )->RecordCount() )
							listTemplates( "$GLOBAL_START_PATH/templates/catalog/category", "chartec_", "idcharte" ); 
					else 	listTemplates( "$GLOBAL_START_PATH/templates/catalog/product", "chartep_", "idcharte" ); 
					
					toolTipBox( Dictionnary::translate('help_idcharte'), 250, $html = "", $showicon = true );

				?>
				</td>
			</tr>-->
			<tr>
				<th class="filledCell">Afficher la cat&eacute;gorie en ligne</th>
				<td>
					<input type="radio" name="available" value="1"<?php if( $tableobj->get( "available" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="available" value="0"<?php if( !$tableobj->get( "available" ) ) echo " checked=\"checked\""; ?> /> non
				</td>
			</tr><tr>
				<th class="filledCell">Afficher la cat&eacute;gorie dans le menu haut</th>
				<td>
					<input type="radio" name="available_menu" value="1"<?php if( $tableobj->get( "available_menu" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="available_menu" value="0"<?php if( !$tableobj->get( "available_menu" ) ) echo " checked=\"checked\""; ?> /> non
				</td>
			</tr>
			
			<tr>
				<th class="filledCell">Nombre de r&eacute;f&eacute;rences disponibles sur le site</th>
				<td>
					<div id="AvailableReferenceCount" style="width:55px; float:left;"><?php echo $tableobj->get( "available_reference_count" ); ?></div>
					<a href="#" onclick="updateAvailableReferenceCount(); return false;" class="blueLink" style="float:left;">recalculer</a>
				</td>
			</tr>
			
			
			
						<tr>		
				<th class="filledCell" width="200" valign="top">Photo dans le menu principal</th>
				<td id="MenuImages">
					<?php listLogosAccueil( $tableobj->get( "idcategory" ) ); ?>
					<div>
						Ajouter une image
						<input type="file" name="categ_accueil" value="" size="5" />
					</div>
				</td>
			</tr> 

			<?php
			$idcategory = $tableobj->get( "idcategory" );
			$name_photo_menu = DBUtil::getDBValue( "name_photo_menu", "category", "idcategory", $idcategory );
			?>
			<tr>
						<th class="filledCell" style="width:200px;">Nom image menu principal</th>
						<td>
							<input type="text" name="name_photo_menu" size="20" value="<?php echo $name_photo_menu; ?>" size="15" />
						</td>
					</tr>
			<tr>
				<th class="filledCell" style="width:200px;">N. de t&eacute;l.</th>
				<!--<td><input type="text" name="info_plus" value="<?php echo $tableobj->get('info_plus'); ?>" size="15" class="textInput" /></td>-->
				<td><input type="text" name="info_plus" value="<?php echo $tableobj->get('info_plus'); ?>"  size="15"/></td>
			</tr>

			

			<!--<tr>
				<th class="filledCell"><?php echo Dictionnary::translate('nb_categories_by_pages'); ?></th>
				<td><input type="text" name="nb_categories_by_pages" value="<?php echo $tableobj->get('nb_categories_by_pages'); ?>" class="textInput" style="width:20px;" /></td>
			</tr>
			<tr>
				<th class="filledCell"><?php echo Dictionnary::translate('nb_column_categories'); ?></th>
				<td><input type="text" name="nb_column_categories" value="<?php echo $tableobj->get('nb_column_categories'); ?>" class="textInput" style="width:20px;" /></td>
			</tr>-->

		</table>
	</div>
	
	<div class="subTitleContainer" style="padding-top:10px;">
		<p class="subTitle">Questionnaire et commentaires pour les commerciaux en interne</p>
	</div>
	<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
	<?php
					
		$wysiwyg = new WYSIWYGEditor( "description_plus_2" );
		$wysiwyg->setHTML( $tableobj->get( "description_plus_2" ) );
		$wysiwyg->setDimension( 0, 110 );
		$wysiwyg->display();
		
	?>
	</div>
		
	<?php
		/*
	if ($tableobj->get("html") === false || $tableobj->get("html")== "") {
		displayAnchorForm( $tableobj->get( "idcategory" ) );
	}
		   */
	/*listPromotions( $tableobj->get( "idcategory" ) );*/
	if (DBUtil::getParameterAdmin('display_catcode') || (DBUtil::getParameterAdmin('display_use_select_intitule') && !getCategoryHeadingCount( $tableobj->get( "idcategory" ) ) ) ){
		
		?>
		<div class="tableContainer">
			<table class="dataTable">
			<?php
			
				if (DBUtil::getParameterAdmin('display_catcode')) { 
						
					?>
					<tr>
						<th class="filledCell" style="width:200px;">Code cat&eacute;gorie</th>
						<td><input type="text" name="code_cat" value="<?php echo $tableobj->get('code_cat'); ?>" class="textInput" size="15" style="width:100px;" /></td>
					</tr>
					<?php
							
				} 
	
				
				
			?>
			</table>
		</div>
		<?php
		
	}
	
}

//------------------------------------------------------------------------------------------------------

function setContentTab( $tableobj ){
		
	if( DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '" . $tableobj->get( "idcategory" ) . "' LIMIT 1" )->RecordCount() )
		listSubCategories( $tableobj );
	else if( DBUtil::query( "SELECT idproduct FROM product WHERE idcategory = '" . $tableobj->get( "idcategory" ) . "' LIMIT 1" )->RecordCount() )
		listProducts( $tableobj );
	else{
		
		?>
		<p style="text-align:center;">Cette cat&eacute;gorie est vide</p>
		<?php	
		
	}
	
}

//------------------------------------------------------------------------------------------------------

function setReferencingTab( $tableobj, $action ){
	
	$lang = User::getInstance()->getLang2();
			$idcategory = $tableobj->get('idcategory');
			$title = DBUtil::getDBValue( "title$lang", "category", "idcategory", $idcategory );
			$title_keyword = DBUtil::getDBValue( "title_keyword$lang", "category", "idcategory", $idcategory );
			$referencing = DBUtil::getDBValue( "referencing$lang", "category", "idcategory", $idcategory );
			$referencing_text = DBUtil::getDBValue( "referencing_text$lang", "category", "idcategory", $idcategory );

	if( DBUtil::getParameterAdmin( "display_actuality" ) ){ 
		
		?>
		<div class="subTitleContainer" style="padding-top:10px;">
	        <p class="subTitle">Sous-titre</p>
		</div>
		<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
		<?php
	    				
			$wysiwyg = new WYSIWYGEditor( "actuality$lang" );
			$wysiwyg->setHTML( $tableobj->get( "actuality$lang" ) );
			$wysiwyg->setDimension( 0, 110 );
			$wysiwyg->display();

		?>
		</div>
		<?php

	}

	
		
		?>
		<div class="subTitleContainer" style="padding-top:10px;">
	        <p class="subTitle">Description compl&eacute;mentaire</p>
		</div>
		<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
		<?php
	    				
			$wysiwyg = new WYSIWYGEditor( "description_plus$lang" );
			$wysiwyg->setHTML( $tableobj->get( "description_plus$lang" ) );
			$wysiwyg->setDimension( 0, 110 );
			$wysiwyg->display();
			
		?>
		</div>
		
		
		
		<?php
	
	

	
		?>	
		<div class="subTitleContainer" style="padding-top:10px;">
            <p class="subTitle">R&eacute;f&eacute;rencement</p>
		</div>
		<div class="tableContainer">
			<table class="dataTable">
				<tr>
					<th class="filledCell" style="width:200px;">Méta : titre</th>
					<td><input type="text" name="title<?php echo $lang;?>" value="<?php echo $title; ?>" class="textInput" style="width:400px;" /></td>
				</tr>
				<tr>
					<th class="filledCell">Mots-cl&eacute;s utilis&eacute;s dans le titre</th>
					<td><input type="text" name="title_keyword<?php echo $lang;?>" value="<?php echo $title_keyword; ?>" class="textInput" size="100" /></td>
				</tr>
				<tr>
					<th class="filledCell"><?php echo Dictionnary::translate('referencing_1'); ?></th>
					<td><textarea name="referencing<?php echo $lang;?>" cols="60" rows="2"><?php echo $referencing; ?></textarea></td>
				</tr>
				<tr>
					<th class="filledCell"><?php echo Dictionnary::translate('referencing_text_1'); ?></th>
					<td><textarea name="referencing_text<?php echo $lang;?>" cols="60" rows="4"><?php echo $referencing_text; ?></textarea></td>
				</tr>
			
			</table>
		</div>
		<?php
		
	
	if ( DBUtil::getParameterAdmin('display_list') ) { 
			
		?>
		<div class="subTitleContainer" style="padding-top:10px;">
            <p class="subTitle">Liste de produits</p>
		</div>	
		<div class="tableContainer">
			<table class="dataTable">
				<tr>		
					<th class="filledCell" style="width:200px;">Afficher la liste des tous les produits</th>
					<td>
						<input type="radio" name="list" value="1"<?php if( $tableobj->get( "list" ) ) echo " checked=\"checked\""; ?> /> oui
						<input type="radio" name="list" value="0"<?php if( !$tableobj->get( "list" ) ) echo " checked=\"checked\""; ?> /> non
					</td>
				</tr>
				<tr>
					<th class="filledCell" valign="top">Meta titre</th>
					<td ><input type="text" name="title_list_1" value="<?php echo $tableobj->get('title_list_1'); ?>" class="textInput" style="width:100%;" /></td>
				</tr>
				<tr>
					<th class="filledCell" valign="top"><?php echo Dictionnary::translate('text_product_list1_1'); ?></th>
					<td>
					<?php
			    				
						$wysiwyg = new WYSIWYGEditor( "text_product_list1$lang" );
						$wysiwyg->setHTML( $tableobj->get( "text_product_list1$lang" ) );
						$wysiwyg->setDimension( 0, 110 );
						$wysiwyg->display();
		
					?>
					</td>
				</tr>
				<tr>
					<th class="filledCell" valign="top"><?php echo Dictionnary::translate('text_product_list2_1'); ?></th>
					<td>
					<?php
			    				
						$wysiwyg = new WYSIWYGEditor( "text_product_list2$lang" );
						$wysiwyg->setHTML( $tableobj->get( "text_product_list2$lang" ) );
						$wysiwyg->setDimension( 0, 110 );
						$wysiwyg->display();
		
					?>
					</td>
				</tr>
			</table>
		</div>
		<?php
			
	}
	
}

//------------------------------------------------------------------------------------------------------

function setMiscTab( $tableobj, $action ){

	
	listBestsellers( $tableobj->get( "idcategory" ) );
	displayMostVisitedProductsForm( $tableobj->get( "idcategory" ) );
	
	$lang = User::getInstance()->getLang();
	if ( DBUtil::getParameterAdmin('display_council') ) { 

		?>
		<div class="subTitleContainer" style="padding-top:10px;">
            <p class="subTitle">Page conseil</p>
		</div>		
		<div class="tableContainer">
			<table class="dataTable">
				<tr>		
					<th class="filledCell" width="200" valign="top">Afficher la page conseil</th>
					<td>
						<input type="radio" name="council" value="1"<?php if( $tableobj->get( "council" ) ) echo " checked=\"checked\""; ?> /> oui
						<input type="radio" name="council" value="0"<?php if( !$tableobj->get( "council" ) ) echo " checked=\"checked\""; ?> /> non
					</td>
				</tr>
				<tr>
					<th class="filledCell" valign="top">Texte de pr&eacute;sentation de la cat&eacute;gorie</th>
					<td>
					<?php
		    				
						$wysiwyg = new WYSIWYGEditor( "referencing_description$lang" );
						$wysiwyg->setHTML( $tableobj->get( "referencing_description$lang" ) );
						$wysiwyg->setDimension( 0, 110 );
						$wysiwyg->display();
		
					?>
					</td>
				</tr>	
			</table>
		</div>
		<?php
		
	}
	
	if( DBUtil::getParameterAdmin('display_trade') ) { 
			
		?>
		<div class="subTitleContainer" style="padding-top:10px;">
            <p class="subTitle">M&eacute;tiers</p>
		</div>	
		<div class="tableContainer">
			<table class="dataTable">
				<tr>
					<th class="filledCell"><?php echo Dictionnary::translate('trade'); ?>1</th>
					<td><input type="text" name="trade_1" value="<?php echo $tableobj->get('trade_1'); ?>" class="textInput" /></td>
					<th class="filledCell"><?php echo Dictionnary::translate('trade'); ?>2</th>
					<td><input type="text" name="trade_2" value="<?php echo $tableobj->get('trade_2'); ?>" class="textInput" /></td>
				</tr>
				<tr>
					<th class="filledCell"><?php echo Dictionnary::translate('trade'); ?>3</th>
					<td><input type="text" name="trade_3" value="<?php echo $tableobj->get('trade_3'); ?>" class="textInput" /></td>
					<th class="filledCell"><?php echo Dictionnary::translate('trade'); ?>4</th>
					<td><input type="text" name="trade_4" value="<?php echo $tableobj->get('trade_4'); ?>" class="textInput" /></td>
				</tr>
				<tr>
					<th class="filledCell"><?php echo Dictionnary::translate('trade'); ?>5</th>
					<td><input type="text" name="trade_5" value="<?php echo $tableobj->get('trade_5'); ?>" class="textInput" /></td>
					<td colspan="2" style="border:1px solid #EEEEEE;"></td>
				</tr>
			</table>
		</div>
		<?php

	}
		
}

//---------------------------------------------------------------------------------------------------------

function listTemplates( $path, $pattern, $elementID ){
	
	global 	$GLOBAL_START_PATH,
			$tableobj;
	
	$dir = opendir( $path );
	
	if( !is_resource( $dir ) )
		die( "Impossible de récupérer la liste des masques" );
	
	echo "
	<select name=\"$elementID\">
		<option value=\"\">-</option>";
	
	while( $file = readdir( $dir ) ){
		
		if( preg_match( "/".$pattern . "[^.]*.tpl/i", $file ) && !strpos( $file, $pattern ) ){
			
			$pos = strpos( $file, ".tpl" );
			if( $pos !== false ){
				
				$len = strlen( $pattern );
				$value = substr( $file, $len, $pos - $len );
				
				if( ( isset( $_POST[ $elementID ] ) && $_POST[ $elementID ] == $value )
					|| $tableobj->get( $elementID ) == $value )
					$selected = " selected=\"selected\"";
				else $selected = "";
				
				echo "
				<option value=\"$value\"$selected>$value</option>";
			
			}
			
		}
		
	}
	
	echo "
	</select>";
	
}

//------------------------------------------------------------------------------------------------------

function deleteCategory(){
	
	global 	$tableobj;

	$langue = User::getInstance()->getLang();
	
	//suppression
	
	$idcategory = $_REQUEST[ "idcategory" ];
	
	$query = "DELETE FROM category_link WHERE idcategorychild = $idcategory";
	$rs =& DBUtil::query( $query );
	if( $rs === false )
		die( "Impossible de supprimer les liens vers d'autres catégories" );
		
	$query = "DELETE FROM category WHERE idcategory = $idcategory LIMIT 1";
	$rs =& DBUtil::query( $query );
	if( $rs === false )
		die( "Impossible de supprimer la catégorie" );


	?>
	<p style="text-align:center;">
		La cat&eacute;gorie a &eacute;t&eacute; supprim&eacute;e
	</p>
	<p style="text-align:center;">
		<a href="cat_category.php">Retour</a>
	</p>
	<?php
			
}

//------------------------------------------------------------------------------------------------------
/*
function deleteSelectedCategories(){
	
	global 	$tableobj,
			$langue,
			$GLOBAL_START_URL;
	
	//vérif sélection
	
	if( !isset( $_POST[ "selectedCategories" ] ) ){
		
		?>
		<p style="text-align:center;">Aucune catégorie n'a été sélectionnée</p>
		<p style="text-align:center;">
			<a href="#" onclick="history.back(); return false">Retour</a>
		</p>
		<?php
		
		return;
		
	}
	
	//demande de confirmation
	
	if( !isset( $_POST[ "ConfirmDelete" ] ) && !isset( $_POST[ "ConfirmDelete_x" ] ) ){
		
		?>
		<form name="DeleteForm" enctype="multipart/form-data" method="post" action="cat_category.php">
		<h4>Confirmer la suppression des catégories suivantes?</h4>
		<ul>
		<?php
		
		$i = 0;
		while( $i < count( $_POST[ "selectedCategories" ] ) ){
			
			$idcategory = $_POST[ "selectedCategories" ][ $i ];
			
			$query = "SELECT name$langue FROM category WHERE idcategory = $idcategory LIMIT 1";
			$rs =& DBUtil::query( $query );
			if( $rs === false )
				die( "Impossible de récupérer le nom de la catégorie" );
			
			$name = $rs->fields( "name$langue" );

			?>
			<li>
				<?php echo $name ?>
				<input type="hidden" name="selectedCategories[]" value="<?php echo $_POST[ "selectedCategories" ][ $i ] ?>" />
			</li>
			<?php
			
			$i++;
			
		}
		
		?>
		</ul>
		<p style="float:right;">
			<input type="hidden" name="ConfirmDelete" value="1" />
			<input type="submit" name="DeleteSelection" value="Confirmer" class="blueButton" />
			<input type="submit" name="Cancel" value="Annuler" onlick="history.back(); return false;" />
		</p>
		</form>
		<?php
		
		return;
		
	}
		
	//suppression
	
	$i = 0;
	while( $i < count( $_POST[ "selectedCategories" ] ) ){
		
		$idcategory = $_POST[ "selectedCategories" ][ $i ];
		
		$query = "DELETE FROM category_link WHERE idcategorychild = $idcategory";
		$rs =& DBUtil::query( $query );
		if( $rs === false )
			die( "Impossible de supprimer les liens vers d'autres catégories" );
			
		$query = "DELETE FROM category WHERE idcategory = $idcategory LIMIT 1";
		$rs =& DBUtil::query( $query );
		if( $rs === false )
			die( "Impossible de supprimer la catégorie" );
			
		$i++;
		
	}

	?>
	<p style="text-align:center;">
		Les cat&eacute;gories s&eacute;lectionn&eacute;es ont &eacute;t&eacute; supprim&eacute;es
	</p>
	<p style="text-align:center;">
		<a href="cat_category.php?action=search&name<?php echo $langue ?>=<?php if( isset( $_REQUEST[ "name$langue" ] ) ) urlencode( $_REQUEST[ "name$langue" ] ) ?>">Retour</a>
	</p>

	<?php
}
*/

//-----------------------------------------------------------------------------------------------------

function displayCustomPageList( $selectedValue ){
	
	global 	$tableobj,
			$GLOBAL_START_URL;
	
	$query = "SELECT filename FROM custom_pages ORDER BY filename ASC";
	
	$rs =& DBUtil::query( $query );
	if( $rs === false )
		die( "Impossible de récupérer la liste des pages statiques" );
		
	?>
	<select name="html">
		<option value="">&nbsp;</option>
		<?php
		
		while( !$rs->EOF() ){
			
			$filename = htmlentities( $rs->fields( "filename" ) );
			$url = $GLOBAL_START_URL . "/"  . $filename .  ".php";
			
			if( $selectedValue == $url )
				$selected=" selected=\"selected\"";
			else $selected = "";
			
			?>
			<option value="<?php echo $url ?>"<?php echo $selected ?>><?php echo $filename ?></option>
			<?php
			
			$rs->MoveNext();
			
		}
		
		?>
	</select>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listSiblings( $idcategory ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/productobject.php" );
	
	$query = "
	SELECT c.idcategory, 
		c.name" . User::getInstance()->getLang() . " AS name,
		c.available
	FROM category c, category_link cl1, category_link cl2
	WHERE cl1.idcategorychild = '$idcategory'
	AND cl1.idcategoryparent = cl2.idcategoryparent
	AND cl2.idcategorychild = c.idcategory
	ORDER BY cl2.displayorder ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------------ */
	
		/* tri des sous-catégories et produits */
	
		function sortCategorySiblings( serializedString ){
			
			var data = "sort=1&" + serializedString;
	
			$.ajax({
	
				url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/cat_category.htm.php",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
			 	success: function( responseText ){
	
					if( responseText == "1" ){
					
						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );
	        	
	        		}
	        		else alert( "Impossible d'enregistrer les modifications" + responseText );
	        		
				}
	
			});
			
		}
		
		/* ------------------------------------------------------------------------------------------ */
	
		$(document).ready(function(){

		    /* sortable */

			$("#CategorySiblings").sortable({
			
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function(event, ui){ sortCategorySiblings( $("#CategorySiblings").sortable('serialize') ); }
			
			});
			
			$("#CategorySiblings").disableSelection();
				
		    
		});
		
		
	/* ]]> */
	</script>
	<div id="CategorySiblingsContainer">
		<ul id="CategorySiblings">
		<?php
		
			while( !$rs->EOF() ){
			
				$productCount = DBUtil::query( "SELECT COUNT(*) AS `count` FROM product WHERE idcategory = '" . $rs->fields( "idcategory" ) . "'" )->fields( "count" );
				$subcategoryCount = $productCount ? 0 : DBUtil::query( "SELECT COUNT(*) AS `count` FROM category_link WHERE idcategoryparent = '" . $rs->fields( "idcategory" ) . "'" )->fields( "count" );
				
				$html = "<b>" . htmlentities( $rs->fields( "name" ) ) . "</b>";
				
				if( $productCount )
					$html .= "<br />$productCount produits";
				else if( $subcategoryCount )
					$html .= "<br />$subcategoryCount sous-catégories";
				else $html .= "<br />aucun contenu";

				?>
				<li id="Categories_<?php echo $rs->fields( "idcategory" ); ?>">
					<a href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?action=edit&amp;idcategory=<?php echo $rs->fields( "idcategory" ); ?>" title="<?php echo htmlentities( $rs->fields( "name" ) ); ?>" style="cursor:move;">
						<div style="<?php if( !$rs->fields( "available" ) ) echo " font-style:italic;\""; ?>">
								<img class="itemThumb" src="<?php echo URLFactory::getCategoryImageURI( $rs->fields( "idcategory" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" alt=""<?php if( !$rs->fields( "available" ) ) echo " style=\"opacity:0.4; filter:alpha(opacity=40);\""; ?> />
							</div>
							<?php echo $html; ?>
						</div>
					</a>
				</li>
				<?php
				
				
				$rs->MoveNext();
				
			}
		
		?>
		</ul>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function initSorter(){

	global $GLOBAL_START_URL;
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------------- */
		
		$(document).ready(function(){
			
			$("#children").sortable({
			
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function(event, ui){ sortChildren( $("#children").sortable('serialize') ); }
			
			});
			
			$("#children").disableSelection();
		
		});
		
		/* ------------------------------------------------------------------------------------------- */
		
		function sortChildren( serializedString ){
			
			var data = "sort=1&" + serializedString;

			$.ajax({
	
				url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/cat_category.htm.php",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
			 	success: function( responseText ){

					if( responseText == "1" ){
					
						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );
	        	
	        		}
	        		else alert( "Impossible d'enregistrer les modifications" );
	        		
				}
	
			});
		
		}
		
		/* ------------------------------------------------------------------------------------------- */
	
	/* ]]> */	
	</script>
	<style type="text/css">

		#children{ width: 440px; list-style-type:none; padding:0px; }
		
		#children .child, #children .placeholder{
			
			float:left; 
			height:90px; 
			width:90px; 
			margin:5px; 
			border:1px solid #E7E7E7; 
			padding:4px;
			text-align:center;
		
		}
		
		#children .child a{ cursor: move; }
		
		#children .placeholder{ background-color:#F3F3F3; }
		
		#children .child .priceInfo li{
			margin:0px;
			padding:0px;
			text-align:left;
		}
		
	</style>
	<?php
		
}

//-----------------------------------------------------------------------------------------------------

function listSubCategories( $tableobj ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	initSorter();
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT cl.idcategorychild,
		c.name$lang,
		c.available
	FROM category_link cl, category c
	WHERE cl.idcategoryparent = '" . $tableobj->get( "idcategory" ) . "' 
	AND cl.idcategorychild = c.idcategory
	ORDER BY cl.displayorder ASC";

	$rs =& DBUtil::query( $query );
	
	?>
	<div style="float:left; margin:10px;">
		<ul>
			<li>Glissez-d&eacute;posez les cat&eacute;gories ci-dessous pour r&eacute;organiser leur ordre d'affichage sur le site.</li>
			<li>Cliquez sur une cat&eacute;gorie ci-dessous pour acc&eacute;der &agrave; sa fiche.</li>
		</ul>
	</div>
	<div style="text-align:right; float:right; margin:10px;">
		Afficher <select onchange="$('#children').css('width', this.options[ this.selectedIndex ].value * 110 );">
		<?php
		
			$i = 1;
			while( $i <= $rs->RecordCount() ){
				
				?>
				<option value="<?php echo $i ?>"<?php if( $i == 4 ) echo " selected=\"selected\""; ?>><?php echo $i ?></option>
				<?php
				
				$i++;
				
			}
			
		?>
		</select> cat&eacute;gories par ligne
		<a href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?create&idcategoryparent=<?php echo $tableobj->get( "idcategory" ); ?>">
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="Ajouter un produit" />
			Ajouter une cat&eacute;gorie
		</a>
	</div>
	<br style="clear:both;" />
	<ul id="children">
	<?php
	
		while( !$rs->EOF() ){
			
			$opacity = $rs->fields( "available" ) ? "" : "opacity:0.4; filter:alpha(opacity=40);";
			
			?>
			<li id="Categories_<?php echo $rs->fields( "idcategorychild" ); ?>" class="child"><?php echo htmlentities( $rs->fields( "name$lang" ) ); ?>
				<a title="<?php echo htmlentities( $rs->fields( "name$lang" ) ); ?>" href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?action=edit&amp;idcategory=<?php echo $rs->fields( "idcategorychild" ); ?>">
					<!--<?php echo URLFactory::getCategoryImageURI( $rs->fields( "idcategorychild" ), URLFactory::$IMAGE_ICON_SIZE ); ?>-->
					<img style="<?php echo $opacity; ?>" src="<?php echo URLFactory::getCategoryImageURI( $rs->fields( "idcategorychild" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="" />
				</a>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<div class="clear"></div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listProducts( $tableobj ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	initSorter();
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT p.idproduct,
		p.name$lang,
		p.available
	FROM product p
	WHERE p.idcategory = '" . $tableobj->get( "idcategory" ) . "' 
	ORDER BY p.display_product_order ASC";

	$rs =& DBUtil::query( $query );
	
	include_once( "$GLOBAL_START_PATH/objects/productobject.php" );

	?>
	<div style="float:left; margin:10px;">
		<ul>
			<li>Glissez-d&eacute;posez les produits ci-dessous pour r&eacute;organiser leur ordre d'affichage sur le site.</li>
			<li>Cliquez sur un produit ci-dessous pour acc&eacute;der &agrave; sa fiche.</li>
		</ul>
	</div>
	<div style="text-align:right; float:right; margin:10px;">
		Afficher <select onchange="$('#children').css('width', this.options[ this.selectedIndex ].value * 110 );">
		<?php
		
			$i = 1;
			while( $i <= $rs->RecordCount() ){
				
				?>
				<option value="<?php echo $i ?>"<?php if( $i == 4 ) echo " selected=\"selected\""; ?>><?php echo $i ?></option>
				<?php
				
				$i++;
				
			}
			
		?>
		</select> produits par ligne
		<a href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?create&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>">
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="Ajouter un produit" />
			Ajouter un produit
		</a>
	</div>
	<br style="clear:both;" />
	<ul id="children">
	<?php
	
		while( !$rs->EOF() ){
			
			$lowerPrice 		= PRODUCT::getLowerPrice( $rs->fields( "idproduct" ) );
			$lowerMargin 		= 0.0;
			$greatestMargin 	= 0.0;
			setMarginRange( $rs->fields( "idproduct" ), $lowerMargin, $greatestMargin );
	
			$opacity = $rs->fields( "available" ) ? "" : "opacity:0.4; filter:alpha(opacity=40);";
			
			?>
			<li id="Products_<?php echo $rs->fields( "idproduct" ); ?>" class="child" style="height:120px;">
				<a title="<?php echo htmlentities( $rs->fields( "name$lang" ) ); ?>" href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>" style="text-decoration:none;">
					<img style="<?php echo $opacity; ?>" src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" alt="" />
					<?php echo htmlentities( $rs->fields( "name$lang" ) ); ?>
					<!--<p class="priceInfos">&agrave; partir de <?php echo Util::priceFormat( $lowerPrice ); ?></p>
					<p class="priceInfos">
					<?php
					
						if( $lowerMargin > 0.0 && $greatestMargin > 0.0 ){
							
							if( $lowerMargin != $greatestMargin )
									echo "Marge brute : entre " . Util::priceFormat( $lowerMargin ) . " et " . Util::priceFormat( $greatestMargin );
							else 	echo "Marge brute : " . Util::priceFormat( $lowerMargin );
							
						}
						
					?>
					</p>-->
				</a>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<div class="clear"></div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function getTreePath( &$tableobj ){
	
	global $GLOBAL_START_URL;
	
	$lang = User::getInstance()->getLang();
	
	$treePath[] = ( object )array(
	
		"name" 			=> $tableobj->get( "name$lang" ),
		"href" 			=> "$GLOBAL_START_URL/product_management/catalog/cat_category.php?action=edit&idcategory=" . $tableobj->get( "idcategory" )
		 
	);
	
	$idchild = $tableobj->get( "idcategory" );
	
	$done = false;
	while( !$done ){
		
		$query = "
		SELECT c.idcategory, c.name$lang AS name
		FROM category_link cl, category c 
		WHERE cl.idcategorychild = '$idchild'
		AND cl.idcategoryparent = c.idcategory
		LIMIT 1";

		$rs =& DBUtil::query( $query );

		$treePath[] = ( object )array( 

			"name" 			=> $rs->fields( "name" ),
			"href" 			=> "$GLOBAL_START_URL/product_management/catalog/cat_category.php?action=edit&idcategory=" . $rs->fields( "idcategory" )
		);

		$idchild = $rs->fields( "idcategory" );
		$done = $idchild == 0;
		
	}

	return array_reverse( $treePath );
		
}

//-----------------------------------------------------------------------------------------------------

function setMarginRange( $idproduct, &$lowerMargin, &$greatestMargin ){
	
	$rs =& DBUtil::query( "SELECT ROUND( sellingcost - buyingcost, 2 ) AS roughStroke FROM detail WHERE idproduct = '$idproduct' ORDER BY ( sellingcost - buyingcost ) ASC" );
	
	if( !$rs->RecordCount() ){
		
		$lowerMargin 		= 0.0;
		$greatestMargin 	= 0.0;
		
		return;
		
	}
	
	$lowerMargin = $rs->fields( "roughStroke" );
	$rs->MoveLast();
	$greatestMargin = $rs->fields( "roughStroke" );
	
}

//-----------------------------------------------------------------------------------------------------

function listMenuImages( $idcategory ){
	
	global $GLOBAL_START_PATH;
	
	$image_menu = DBUtil::getDBValue( "image_menu", "category", "idcategory", $idcategory );
	
	?>
	<input<?php if( !strlen( $image_menu ) ) echo " checked=\"checked\""; ?> type="radio" name="image_menu" value="" /> aucun
	<?php
				
	$dir = opendir( "$GLOBAL_START_PATH/www/img_cat/menu" );
	
	while( $file = readdir( $dir ) ){
		
		if( !is_dir( $file ) && $file != "." && $file != ".." && $file != "index.php" ){
			
			$checked = $file == $image_menu ? " checked=\"checked\"" : "";
			
			?>
			<input<?php echo $checked; ?> type="radio" name="image_menu" value="<?php echo htmlentities( $file ); ?>" />
			<img src="/www/img_cat/menu/<?php echo htmlentities( $file ); ?>" alt="" width="100"/>	
			<?php
		
		}

	}

}
/*
//-----------------------------------------------------------------------------------------------------

function listLogosCategories( $idcategory ){
	
	global $GLOBAL_START_PATH;
	
	$filename = "$GLOBAL_START_PATH/images/categories/logos/$idcategory";

if (!file_exists($filename)) {
	
	if (!mkdir($filename, 0777, true)) 
	{
	 die('Echec lors de la création des répertoires...');
	}
}

$files = $GLOBAL_START_PATH."/images/categories/".$idcategory.".jpg";
	
	$destination = $GLOBAL_START_PATH."/images/categories/logos/".$idcategory."/".$idcategory.".jpg";
		
	DBUtil::query( "UPDATE category SET logo_menu = '" . $idcategory . ".jpg' WHERE idcategory = '" . $idcategory . "' LIMIT 1" );
	
		
		if( !copy( $files, $destination) ){
			
			echo "Impossible de copier les images du produit";
			return;
			
		}
	
	$image_menu = DBUtil::getDBValue( "logo_menu", "category", "idcategory", $idcategory );
	
	?>
	<input<?php if( !strlen( $image_menu ) ) echo " checked=\"checked\""; ?> type="radio" name="logo_menu" value="" /> aucun
	<br>	
	<?php
				
	$dir = opendir( "$GLOBAL_START_PATH/images/categories/logos/$idcategory" );
	
	while( $file = readdir( $dir ) ){
		
		if( !is_dir( $file ) && $file != "." && $file != ".." && $file != "index.php" ){
			
			$checked = $file == $image_menu ? " checked=\"checked\"" : "";
			
			?>
			<input<?php echo $checked; ?> type="radio" name="logo_menu" value="<?php echo htmlentities( $file ); ?>" />
			<img src="/images/categories/logos/<?php echo $idcategory.'/'.htmlentities( $file ); ?>" alt="" width="80"/>	
			<br>
			<?php
		
		}

	}

}
*/
function listLogosAccueil( $idcategory ){
	
	global $GLOBAL_START_PATH;
	
	/*$filename = "$GLOBAL_START_PATH/images/categories/logos_accueil/$idcategory";

	if (!file_exists($filename))
	{
	
		if (!mkdir($filename, 0777, true)) 
		{
		 die('Echec lors de la création des répertoires...');
		}
	}*/
	
	
	
	
	
	
	$image_menu = DBUtil::getDBValue( "logo_accueil", "category", "idcategory", $idcategory );
	
	?>
	<input<?php if( !strlen( $image_menu ) ) echo " checked=\"checked\""; ?> type="radio" name="logo_accueil" value="" /> aucun
	<?php
				
	//$dir = opendir( "$GLOBAL_START_PATH/images/categories/logos_accueil/$idcategory" );
    $dir = opendir( "$GLOBAL_START_PATH/images/category/logos_accueil" );
	
	while( $file = readdir( $dir ) ){
		
		$needle = $idcategory."_";
        if( !is_dir( $file ) && $file != "." && $file != ".." && $file != "index.php" && substr($file, 0, strlen($needle)) === $needle){
			
			$checked = $file == $image_menu ? " checked=\"checked\"" : "";
			
			?>
			<input<?php echo $checked; ?> type="radio" name="logo_accueil" value="<?php echo htmlentities( $file ); ?>" />
			<!--<img src="/images/categories/logos_accueil/<?php echo $idcategory.'/'.htmlentities( $file ); ?>" alt="" width="50"/>-->
            <img src="/images/category/logos_accueil/<?php echo htmlentities( $file ); ?>" alt="" width="50"/>	
			<?php
		
		}

	}

}




//-----------------------------------------------------------------------------------------------------
/**
 * publicités
 */
function displayAdForm( $idcategory ){
	
	?>
	<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
	<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
	<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */

		/* ------------------------------------------------------------------------------------------------------------ */
		
		function selectAd( idad ){

			$( "#ad_name" ).val( "" );
			$( "#ad_description" ).val( "" );
			$( "#ad_index" ).val( "0" );
			$( "#ad_start_date" ).val( "" );
			$( "#ad_end_date" ).val( "" );
			$( "#ad_enabled_0" ).attr( "checked", "checked" );
			$( "#AddImages" ).html( "" );
				
			if( idad == "0" ){

				$( "#DeleteAdButton" ).css( "display", "none" );

				$( "#ad_name" ).attr( "disabled", "disabled" );
				$( "#ad_description" ).attr( "disabled", "disabled" );
				$( "#ad_index" ).attr( "disabled", "disabled" );
				$( "#ad_start_date" ).datepicker( "disable" );
				$( "#ad_end_date" ).datepicker( "disable" );
				$( "#ad_start_date" ).attr( "disabled", "disabled" );
				$( "#ad_end_date" ).attr( "disabled", "disabled" );
				$( "#ad_enabled_0" ).attr( "disabled", "disabled" );
				$( "#ad_enabled_1" ).attr( "disabled", "disabled" );

				return;

			}

			$( "#DeleteAdButton" ).css( "display", "inline" );

			$( "#ad_name" ).attr( "disabled", "" );
			$( "#ad_description" ).attr( "disabled", "" );
			$( "#ad_index" ).attr( "disabled", "" );
			$( "#ad_start_date" ).attr( "disabled", "" );
			$( "#ad_end_date" ).attr( "disabled", "" );
			$( "#ad_start_date" ).datepicker( "enable" );
			$( "#ad_end_date" ).datepicker( "enable" );
			$( "#ad_enabled_0" ).attr( "disabled", "" );
			$( "#ad_enabled_1" ).attr( "disabled", "" );
			
			$.ajax({
			 	
				url: "/templates/product_management/cat_category.htm.php?get=ad",
				async: true,
				type: "GET",
				data: "idad=" + idad + "&idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer la publicité" ); },
			 	success: function( responseXML ){

					$( "#ad_name" ).val( responseXML.getElementsByTagName( 'name' ).item( 0 ).firstChild.nodeValue );
					$( "#ad_description" ).val( responseXML.getElementsByTagName( 'description' ).item( 0 ).firstChild.nodeValue );
					$( "#ad_index" ).val( responseXML.getElementsByTagName( 'index' ).item( 0 ).firstChild.nodeValue );

					var tmp = responseXML.getElementsByTagName( 'start_date' ).item( 0 ).firstChild.nodeValue.split( " " );
					var tmp2 = tmp[ 0 ].split( "-" );
					$( "#ad_start_date" ).val( tmp2[ 2 ] + "-" + tmp2[ 1 ] + "-" + tmp2[ 0 ] );

					var tmp = responseXML.getElementsByTagName( 'end_date' ).item( 0 ).firstChild.nodeValue.split( " " );
					var tmp2 = tmp[ 0 ].split( "-" );
					$( "#ad_end_date" ).val( tmp2[ 2 ] + "-" + tmp2[ 1 ] + "-" + tmp2[ 0 ] );
					
					$( "#ad_enabled_0" ).attr( "checked", responseXML.getElementsByTagName( 'enabled' ).item( 0 ).firstChild.nodeValue == "0" ? "checked" : "" );
					
					listAdImages( idad );
					
				}

			});
			
		}

		/* ------------------------------------------------------------------------------------------------------------ */
		
		function listAdImages( idad ){

			$.ajax({
			 	
				url: "/templates/product_management/cat_category.htm.php?get=ad_images",
				async: true,
				type: "GET",
				data: "idad=" + idad + "&idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les images de la publicité" ); },
			 	success: function( responseText ){ $( "#AddImages" ).html( responseText ); }

			});

		}

		/* ------------------------------------------------------------------------------------------------------------ */
		
		function deleteAd( idad ){

			if( idad == "0" )
				return;

			$.ajax({
			 	
				url: "/templates/product_management/cat_category.htm.php?delete=ad",
				async: true,
				type: "GET",
				data: "idad=" + idad + "&idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer la publicité" ); },
			 	success: function( responseText ){

					if( responseText == "1" ){
						
						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
				    	$.growlUI( '', 'Modifications enregistrées' );
				    	
						selectAd( 0 );
						$( "#ad_idad option[value='" + idad + "']" ).remove();

					}
					else alert( "Une erreur est survenue : " + responseText );
					
				 }

			});

		}
		
		/* ------------------------------------------------------------------------------------------------------------ */
		
		function createAd(){

			$.ajax({
			 	
				url: "/templates/product_management/cat_category.htm.php?create=ad",
				async: true,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer une publicité" ); },
			 	success: function( responseText ){

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Modifications enregistrées' );

			    	$( "#ad_idad" ).append( '<option value="' + responseText + '" selected="selected">nouvelle publicité</option>' );
					selectAd( responseText );
					
				 }

			});
			
		}

		/* ------------------------------------------------------------------------------------------------------------ */
		
		function updateAdLabel(){

			var idad = $( "#ad_idad" ).val();

			if( idad == "0" )
				return;
			
			$( "#ad_idad option[value='" + idad + "']" ).html( $( "#ad_name" ).val() );
			
		}

		/* ------------------------------------------------------------------------------------------------------------ */
		
	/* ]]> */
	</script>
	<!--<div class="subTitleContainer" style="padding-top:10px;">
            <p class="subTitle">Publicit&eacute;s</p>
	</div>	
	<div class="tableContainer">
		<table class="dataTable">
			<tr>
				<th class="filledCell" width="200" valign="top">Publicit&eacute;s cr&eacute;&eacute;es</th>
				<td>
					<select name="ad_idad" id="ad_idad" onchange="selectAd(this.options[this.selectedIndex].value);">
						<option value="0">-</option>
						<?php
						
						$query = "
						SELECT ad.*, GROUP_CONCAT( ai.filename ORDER BY ai.`index` ASC ) AS images
						FROM category_ad ca, ad
						LEFT JOIN ad_image ai ON ai.idad = ad.idad
						WHERE ca.idcategory = '$idcategory'
						AND ca.idad = ad.idad
						GROUP BY ad.idad
						ORDER BY ad.start_date DESC, ca.`index` ASC";

						$rs =& DBUtil::query( $query );
						
						while( !$rs->EOF() ){
							
							?>
							<option value="<?php echo $rs->fields( "idad" ); ?>"><?php echo htmlentities( $rs->fields( "name" ) ); ?></option>
							<?php
							
							$rs->MoveNext();
							
						}
						
					?>
					</select>
					<input type="button" id="DeleteAdButton" class="blueButton" value="Supprimer la publicité sélectionnée" onclick="if( confirm('Etes-vous certains de vouloir supprimer cette publicité?') ) deleteAd($('#ad_idad').val());" style="display:none;" />
					<input type="button" value="Créer une publicité" class="blueButton" onclick="createAd();" />
				</td>
			</tr>
			<tr>
				<th class="filledCell" width="200" valign="top">Nom de mon action</th>
				<td>
					<input disabled="disabled" class="textInput" type="text" name="ad_name" id="ad_name" value="" style="width:350px;" onchange="updateAdLabel();" onkeyup="this.onchange(event);" />
				</td>
			</tr>
			<tr>
				<th class="filledCell" valign="top">Description</th>
				<td>
					<textarea disabled="disabled" name="ad_description" id="ad_description" style="width:350px; height:120px;"></textarea>
				</td>
			</tr>
			<tr>
				<th class="filledCell" width="200" valign="top">Date de validit&eacute;</th>
				<td>
					entre <input disabled="disabled" class="calendarInput" type="text" id="ad_start_date" name="ad_start_date" value="" />
					et le <input disabled="disabled" class="calendarInput" type="text" id="ad_end_date" name="ad_end_date" value="" />
					<?php
					
					include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );
					
					DHTMLCalendar::calendar( "ad_start_date" );
					DHTMLCalendar::calendar( "ad_end_date" );
					
				?>
				</td>
			</tr>
			<tr>
				<th class="filledCell" width="200" valign="top">Images</th>
				<td id="AddImages">
					aucune publicit&eacute; s&eacute;lectionn&eacute;e
				</td>
			</tr>
			<tr>
				<th class="filledCell" width="200" valign="top">Importance (ordre d'affichage)</th>
				<td>
					<input disabled="disabled" type="text" name="ad_index" id="ad_index" class="textInput" value="" style="width:150px;" />
				</td>
			</tr>
			<tr>
				<th class="filledCell" width="200" valign="top">Afficher la publicit&eacute; en ligne</th>
				<td>
					<input disabled="disabled" type="radio" id="ad_enabled_1" name="ad_enabled" value="1" /> oui
					<input disabled="disabled" type="radio" id="ad_enabled_0" name="ad_enabled" value="0" checked="checked" /> non
				</td>
			</tr>
		</table>
	</div>-->
	<?php
		
}

//-----------------------------------------------------------------------------------------------------
/**
 * produits stars
 */
function displayStarProductForm( $idcategory ){
	
	?>
	<style type="text/css">

		#StarProducts{ width: auto; list-style-type:none; padding:0px; }
		
		#StarProducts li{
			
			float:left; 
			height:90px; 
			width:90px; 
			margin:5px 5px 30px 5px; 
			border:1px solid #E7E7E7; 
			padding:4px;
			text-align:center;
			position:relative;
		
		}

		#StarProducts li .label{
		
			height:15px;
			line-height:15px;
			padding:0px;
			margin:0px;
			font-size:10px;
			margin-bottom:-15px;
			bottom:0px;
			position:absolute;
			
		}
		
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* -------------------------------------------------------------------------------------------------*/
		
		function deleteStarProduct( idproduct ){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?delete=star_product",
				async: true,
				cache: false,
				type: "GET",
				data: "idproduct=" + idproduct + "&idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sélectionner le produit" ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	        		listStarProducts();
	        		
				}
	
			});

		}
		
		/* -------------------------------------------------------------------------------------------------*/

		function addStarProduct(){

			var idproduct = $( "#StarProductId" ).val();

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?add=star_product",
				async: true,
				cache: false,
				type: "GET",
				data: "idproduct=" + idproduct + "&idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sélectionner le produit" ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	        		listStarProducts();
	        		
				}
	
			});

		}

		/* -------------------------------------------------------------------------------------------------*/
		
		function listStarProducts(){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?get=star_products",
				async: true,
				cache: false,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les produits stars" ); },
			 	success: function( responseText ){
	
					$( "#StarProductListContainer" ).html( responseText );
	        		
				}
	
			});

		}

		/* -------------------------------------------------------------------------------------------------*/
		
	/* ]]> */
	</script>
	<!--<div class="subTitleContainer" style="margin-top:20px; padding-top:10px;">
    	<p class="subTitle">Produits mis en avant</p>
	</div>
	<p style="text-align:right; margin:20px 10px;">
		Ajouter le produit n&deg;  
		<input id="StarProductId" type="text" class="textInput" value="" />
		<?php
		
			include_once( dirname( __FILE__ ) . "/../../objects/AutoCompletor.php" );
			AutoCompletor::completeFromDBPattern( "StarProductId", "product", "idproduct", "{name_1}", 15, "addStarProduct" );
			
		?>
	</p>
	<div id="StarProductListContainer">
		<?php listStarProducts( $idcategory ); ?>
	</div>
	<br style="clear:both;" />-->
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listStarProducts( $idcategory ){ 

	$rs = DBUtil::query( 
	
		"SELECT sp.idproduct, p.available, p.name_1
		FROM star_product sp, product p
		WHERE sp.idcategory = '" . intval( $idcategory ) . "'
		AND sp.idproduct = p.idproduct
		ORDER BY sp.`index` ASC"
	
	);
	
	if( !count( $rs->RecordCount() ) ){

		?>
		<p style="text-align:center;">Aucun produit star trouv&eacute;</p>
		<?php
		
		return;
		
	}
	
	?>
	<ul id="StarProducts">
	<?php
	
		while( !$rs->EOF() ){
			
			?>
			<li<?php if( !$rs->fields( "available" ) ) echo " style=\"opacity:0.4; filter:alpha(opacity=40);\""; ?>>
				<a href="#" title="<?php echo htmlentities( $rs->fields( "name_1" ) ); ?>" onclick="window.open('/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>');">
					<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, 70, 70 ); ?>" alt="" />
				</a>
				<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce produit ?' ) ) deleteStarProduct( <?php echo $rs->fields( "idproduct" ); ?> ); return false;" style="cursor:pointer;">
					<img src="/images/back_office/content/corbeille.jpg" title="désélectionner le produit" alt="désélectionner le produit" style="position:absolute; bottom:2px; left:2px;" />
				</a>
				<div class="label">
					<?php echo htmlentities( $rs->fields( "idproduct" ) ); ?>
				</div>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * publicités
 */
function listBestsellers( $idcategory ){
	
	?>
	<style type="text/css">

		#Bestsellers{ width: auto; list-style-type:none; padding:0px; }
		
		#Bestsellers li{
			
			float:left; 
			height:90px; 
			width:90px; 
			margin:5px 5px 30px 5px; 
			border:1px solid #E7E7E7; 
			padding:4px;
			text-align:center;
			position:relative;
		
		}

		#Bestsellers li .label{
		
			height:15px;
			line-height:15px;
			padding:0px;
			margin:0px;
			font-size:10px;
			margin-bottom:-15px;
			bottom:0px;
			position:absolute;
			
		}
		
	</style>
	<div class="subTitleContainer" style="margin-top:40px; padding-top:10px;">
    	<p class="subTitle">Meilleures ventes</p>
	</div>
	<?php
	
		include_once( dirname( __FILE__ ) . "/../../lib/products.php" );
		
		$children = lib_getCategoryLeafs( $idcategory );
		
		$query = "
		SELECT orow.idarticle, COUNT( orow.idorder ) AS `count`, d.reference, p.available, p.name_1
		FROM order_row orow, `order` o, detail d, product p
		WHERE orow.idcategory IN( " . implode( ",", $children ) . " )
		AND orow.idorder = o.idorder
		AND o.status <> 'Cancelled'
		AND orow.idarticle = d.idarticle
		AND d.type LIKE 'catalog'
		AND d.idproduct = p.idproduct
		GROUP BY orow.idarticle
		ORDER BY COUNT( orow.idorder ) DESC, RAND()
		LIMIT 10";
		
		$rs =& DBUtil::query( $query );
		
		if( !$rs->RecordCount() ){
	
			?>
			<p style="text-align:center;">Aucune vente trouv&eacute;e</p>
			<?php
			
			return;
			
		}
	
	?>
	<ul id="Bestsellers">
	<?php
	
		while( !$rs->EOF() ){
			
			?>
			<li<?php if( !$rs->fields( "available" ) ) echo " style=\"opacity:0.4; filter:alpha(opacity=40);\""; ?>>
				<a title="<?php echo htmlentities( $rs->fields( "name_1" ) ); ?>" href="#" onclick="window.open('/product_management/catalog/detail.php?idarticle=<?php echo $rs->fields( "idarticle" ); ?>');">
					<img src="<?php echo URLFactory::getReferenceImageURI( $rs->fields( "idarticle" ), URLFactory::$IMAGE_CUSTOM_SIZE, 70, 70 ); ?>" alt="" />
				</a>
				<div class="label">
					<?php echo htmlentities( $rs->fields( "reference" ) ); ?>
					<br />
					<?php echo $rs->fields( "count" ); ?> vente(s)
				</div>
			</li>
			<?php

			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<br style="clear:both;" />
	<?php
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * produits les plus visités en ligne
 */
function displayMostVisitedProductsForm( $idcategory ){
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function resetVisitCount( idproduct ){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?reset=visit_count",
				async: true,
				cache: false,
				type: "GET",
				data: "idproduct=" + idproduct,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de réinitialiser le nombre de visites" ); },
			 	success: function( responseText ){
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	        		listMostVisitedProducts();
	        		
				}
	
			});

		}

		/* ------------------------------------------------------------------------------------------ */
		
		function listMostVisitedProducts(){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?get=visit_count",
				async: true,
				cache: false,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer le nombre de visites" ); },
			 	success: function( responseText ){
	
					$( "#MostVisitedProductListContainer" ).html( responseText );
	        		
				}
	
			});

		}

		/* ------------------------------------------------------------------------------------------ */
		
	/* ]]> */
	</script>
	<style type="text/css">

		#MostVisitedProducts{ width: auto; list-style-type:none; padding:0px; }
		
		#MostVisitedProducts li{
			
			float:left; 
			height:90px; 
			width:90px; 
			margin:5px 5px 30px 5px; 
			border:1px solid #E7E7E7; 
			padding:4px;
			text-align:center;
			position:relative;
		
		}

		#MostVisitedProducts li .label{
		
			height:15px;
			line-height:15px;
			padding:0px;
			margin:0px;
			font-size:10px;
			margin-bottom:-15px;
			bottom:0px;
			position:absolute;
			
		}
		
	</style>
	<div class="subTitleContainer" style="margin-top:40px; padding-top:10px;">
    	<p class="subTitle">Produits les plus visit&eacute;s</p>
	</div>
	<div id="MostVisitedProductListContainer">
		<?php listMostVisitedProducts( $idcategory ); ?>
	</div>
	<br style="clear:both;" />
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listMostVisitedProducts( $idcategory ){
	
	include_once( dirname( __FILE__ ) . "/../../lib/products.php" );

	$children = lib_getCategoryLeafs( $idcategory );

	$query = "
	SELECT idproduct, hits, available, name_1
	FROM product 
	WHERE idproduct <> 0
	AND idcategory IN( " . implode( ",", $children ) . " )
	ORDER  BY hits DESC, available DESC, RAND()
	LIMIT 10";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){

		?>
		<p style="text-align:center;">Aucun produit trouv&eacute;</p>
		<?php
		
		return;
		
	}
	
	?>
	<ul id="MostVisitedProducts">
	<?php
	
		while( !$rs->EOF() ){
			
			?>
			<li id="MostVisited<?php echo $rs->fields( "idproduct" ); ?>"<?php if( !$rs->fields( "available" ) ) echo " style=\"opacity:0.4; filter:alpha(opacity=40);\""; ?>>
				<a href="#" title="<?php echo htmlentities( $rs->fields( "name_1" ) ); ?>" onclick="window.open('/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>');">
					<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, 70, 70 ); ?>" alt="" />
				</a>
				<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir réinitialiser le compteur de visites pour ce produit ?' ) ) resetVisitCount( <?php echo $rs->fields( "idproduct" ); ?> ); return false;" style="cursor:pointer;">
					<img src="/images/back_office/content/corbeille.jpg" title="réinitialiser le compteur de visites pour ce produit" alt="réinitialiser le compteur de visites pour ce produit" style="position:absolute; bottom:2px; left:2px;" />
				</a>
				<div class="label">
					<?php echo $rs->fields( "idproduct" ); ?>
					<br />vu <?php echo $rs->fields( "hits" ); ?> fois
				</div>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<?php
	
}



//-----------------------------------------------------------------------------------------------------

function listAdImages( $idad ){

	?>
	<style type="text/css">

		#AdImageList{ width: 440px; list-style-type:none; padding:0px; }
		
		#AdImageList li, #AdImageList .placeholder{
			
			float:left; 
			height:90px; 
			width:90px; 
			margin:5px; 
			border:1px solid #E7E7E7; 
			padding:4px;
			text-align:center;
			position:relative;
		
		}
		
		#AdImageList li{ cursor: move; }
		
		#AdImageList .placeholder{ background-color:#F3F3F3; }
		
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------------------- */
		/* sortable */
		
		$( document ).ready( function(){

			$( "#AdImageList" ).sortable({
				
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function( event, ui ){ sortAdImages(); }
			
			});

			$( "#AdImageList" ).disableSelection();

		});

		/* ------------------------------------------------------------------------------------------------- */
		/* upload */
		
		$( document ).ready( function(){

			$( "#AdImageUploader" ).uploadify({
			
				'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
				'script'		: '/templates/product_management/cat_category.htm.php',
				'folder'		: '/images/ads',
				'scriptData'	: { 'idad': '<?php echo $idad; ?>' , 'sid_uploadify' : '<?php echo session_id();?>' },
				'fileDataName'	: 'AdImage',
				'buttonText' 	: "Ajouter",
				'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
				'fileDesc'		: "Images",
				'auto'           : true,
				'multi'          : false,
				'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
				'onComplete' 	: function( event, queueID, fileObj, response, data ){
						
					listAdImages( <?php echo $idad; ?> );
						
				}

			}); 
			
		});
		
		/* ------------------------------------------------------------------------------------------------- */
		
		function sortAdImages(){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?sort=ad_images",
				async: true,
				cache: false,
				type: "GET",
				data: "idad=<?php echo $idad; ?>&" + $( "#AdImageList" ).sortable( "serialize" ),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier l'ordre des images" ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	        		listAdImages( <?php echo $idad; ?> );
	        		
				}
	
			});
			
		}

		/* ------------------------------------------------------------------------------------------------- */
		
		function deleteAdImage( idad_image ){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?delete=ad_image",
				async: true,
				cache: false,
				type: "GET",
				data: "idad_image=" + idad_image,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer l'image" ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	     	       listAdImages( <?php echo $idad; ?> );
	     	       
				}
	
			});

		}

		/* ------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<?php
	
	$rs =& DBUtil::query( "SELECT * FROM ad_image WHERE idad = '" . intval( $idad ) . "' ORDER BY `index` ASC" );
	
	if( $rs->RecordCount() ){
		
		?>
		<ul id="AdImageList">
		<?php
		
			while( !$rs->EOF() ){
				
				?>
				<li id="AdImageIndexes_<?php echo $rs->fields( "idad_image" ); ?>">
					<img src="/catalog/thumb.php?src=<?php echo urlencode( "/images/ads/" . htmlentities( $rs->fields( "filename" ) ) ); ?>&amp;w=90&amp;h=90" alt="" />
					<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette image ?' ) ) deleteAdImage( <?php echo $rs->fields( "idad_image" ); ?> ); return false;" style="cursor:pointer;">
						<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'image" alt="supprimer l'image" style="position:absolute; bottom:2px; left:2px;" />
					</a>
				</li>
				<?php
				
				$rs->MoveNext();
				
			}
			
		?>
		</ul>
		<?php
		
	}
	
	?>
	<p style="clear:both;">
		<input type="file" name="ad_image" id="AdImageUploader" value="" style="width:350px;" />
	</p>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------
/* templates spécifiques à la catégorie */

function displayAnchorForm( $idcategory ){
	
	?>
	<style type="text/css">

		#CustomPageSelector, 
		#CustomPageList{ width: auto; list-style-type:none; padding:0px; }
		
		#CustomPageSelector li, 
		#CustomPageList li{
			
			height:25px; 
			width:500px; 
			margin:5px; 
			border:1px solid #E7E7E7; 
			padding:4px;
			text-align:center;
			position:relative;
			cursor:move;
		
		}
		
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ---------------------------------------------------------------------------------------------- */
		
		function addCustomPage( idpage ){

			if( idpage == "0" )
				return;
	
			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?add=custom_page",
				async: true,
				cache: false,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>&idpage=" + idpage,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter la page statique" ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	     	       listCustomPages();
	     	       
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------- */
		
		function deleteCustomPage( idpage ){

			if( idpage == "0" )
				return;
	
			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?delete=custom_page",
				async: true,
				cache: false,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>&idpage=" + idpage,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer la page statique" ); },
			 	success: function( responseText ){
	
					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	     	       listCustomPages();
	     	       
				}
	
			});

		}
		
		/* ---------------------------------------------------------------------------------------------- */

		function listCustomPages(){

			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?get=custom_pages",
				async: true,
				cache: false,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les pages statiques" ); },
			 	success: function( responseText ){

	     	       $( "#CustomPageListContainer" ).html( responseText );
	     	       
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------- */

		function sortCustomPages( serialized ){
			
			$.ajax({
				
				url: "/templates/product_management/cat_category.htm.php?sort=custom_pages",
				async: true,
				cache: false,
				type: "GET",
				data: "idcategory=<?php echo $idcategory; ?>&" + serialized,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les pages statiques" ); },
			 	success: function( responseText ){

					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}
	
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

	     	       listCustomPages();
	     	       
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div class="subTitleContainer" style="margin-top:40px; padding-top:10px;">
    	<p class="subTitle">Pages statiques li&eacute;es &agrave; la cat&eacute;gorie</p>
	</div>
	<div id="CustomPageListContainer">
		<?php listCustomPages( $idcategory ); ?>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listCustomPages( $idcategory ){
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ---------------------------------------------------------------------------------------------- */
		
		$(document).ready(function(){
	
		    /* sortable */
	
			$("#CustomPageList").sortable({
			
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function(event, ui){ sortCustomPages( $( "#CustomPageList" ).sortable( "serialize" ) ); }
			
			});
			
			$( "#CustomPageList" ).disableSelection();
				
		    
		});

		/* ---------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<ul id="CustomPageList">
	<?php
			
			/* pages sélectionnées */
		
			$query = "
			SELECT cp.*
			FROM category_custom_pages ccp, custom_pages cp
			WHERE ccp.idcategory = '$idcategory'
			AND ccp.idpage = cp.idpage
			ORDER BY ccp.`index` ASC";
			 
			$rs =& DBUtil::query( $query );
			
			$selected = array();
			while( !$rs->EOF() ){
				
				?>
				<li id="CustomPage_<?php echo $rs->fields( "idpage" ); ?>">
					<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette page ?' ) ) deleteCustomPage( <?php echo $rs->fields( "idpage" ); ?> ); return false;" style="cursor:pointer; float:right;">
						<img src="/images/back_office/content/corbeille.jpg" title="supprimer la page" alt="supprimer la page" style="position:absolute; bottom:2px; left:2px;" />
					</a>
					<?php echo htmlentities( $rs->fields( "filename" ) ); ?>
				</li>
				<?php
				
				array_push( $selected, $rs->fields( "idpage" ) );
				
				$rs->MoveNext();
				
			}
			
	?>
	</ul>
	<ul id="CustomPageSelector">
		<li>
			<select onchange="addCustomPage(this.options[this.selectedIndex].value);">
				<option value="0" selected="selected">S&eacute;lectionner une page statique</option>
				<?php
			
					/* pages sélectionnées */
				
					$rs =& DBUtil::query( "SELECT * FROM custom_pages ORDER BY filename ASC" );
			
					while( !$rs->EOF() ){
						
						if( !in_array( $rs->fields( "idpage" ), $selected ) ){
							
							?>
							<option value="<?php echo $rs->fields( "idpage" ); ?>"><?php echo htmlentities( $rs->fields( "filename" ) ); ?></option>
							<?php
						
						}
						
						$rs->MoveNext();
						
					}
				
			?>
			</select>
		</li>
	</ul>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function displayAdvertising($tableobj, $action)
{
	displayAdForm( $tableobj->get( "idcategory" ) );
	displayStarProductForm( $tableobj->get( "idcategory" ) );
	if (DBUtil::getParameterAdmin('display_mask_cat')) {
		
		global $GLOBAL_START_PATH;
		
		?>
		<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
		<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
		<script type="text/javascript">
		/* <![CDATA[ */
		
			/* ------------------------------------------------------------------------------------------------- */
			/* upload */
			
			$( document ).ready( function(){
	
				$( "#BannerUploader" ).uploadify({
				
					'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
					'script'		: '/templates/product_management/cat_category.htm.php',
					'folder'		: '/images/categories/banners',
					'scriptData'	: { 'idcategory': '<?php echo $tableobj->get( "idcategory" ); ?>' , 'sid_uploadify' : '<?php echo session_id();?>'},
					'fileDataName'	: 'banner',
					'buttonText' 	: "Sélectionner",
					'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
					'fileDesc'		: "Images",
					'auto'           : true,
					'multi'          : false,
					'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
					'onComplete' 	: function( event, queueID, fileObj, response, data ){
					
						displayBanner( response );
							
					}
	
				}); 
				
			});

			/* ------------------------------------------------------------------------------------------------- */
			
			function displayBanner( banner ){

				$( "#BannerImage" ).attr( "src", "/catalog/thumb.php?src=" + escape( banner ) + "&w=200&h=200" );
				$( "#BannerPreview" ).css( "display", "block" );
				
			}
			
			/* ------------------------------------------------------------------------------------------------- */
	
			function deleteBanner(){

				$.ajax({
					
					url: "/templates/product_management/cat_category.htm.php?delete=banner",
					async: true,
					type: "POST",
					data: "idcategory=<?php echo $tableobj->get( "idcategory" ); ?>",
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
					success: function( responseText ){
		
						if( responseText!= "1" ){
							
							alert( "Une erreur est survenur : " + responseText );
							return;

						}

						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
						$.growlUI( '', 'Modifications enregistrées' );

						$( "#BannerPreview" ).css( "display", "none" );
						$( "#BannerImage" ).attr( "src", "" );
						
					}
		
				});
				
			}

			/* ------------------------------------------------------------------------------------------------- */
			
		/* ]]> */
		</script>
		<!--<div class="subTitleContainer" style="margin-top:10px;">
			<p class="subTitle"><?php echo Dictionnary::translate('gest_image_cat'); ?></p>
		</div>
		<div class="tableContainer">
			<table class="dataTable">
				<tr>
					<th class="filledCell" width="200">Banni&egrave;re personnalis&eacute;e</th>
					<td colspan="3">
						<?php $banner = URLFactory::getCategoryBannerURI( $tableobj->get( "idcategory" ) ); ?>
						<div id="BannerPreview" style="display:<?php echo $banner ? "block" : "none"; ?>">
							<img id="BannerImage" src="<?php if( $banner ){ ?>/catalog/thumb.php?src=<?php echo urlencode( $banner ); ?>&amp;w=200&amp;h=200<?php } ?>" alt="" />
							<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir la bannière ?' ) ) deleteBanner(); return false;" style="cursor:pointer;">
								<img src="/images/back_office/content/corbeille.jpg" title="supprimer la bannière" alt="supprimer la bannière" />
							</a>
						</div>
						<input type="file" id="BannerUploader" value="" />
					<?php
					
					?>
					</td>
				</tr>
				
			</table>
		</div>-->
		<?php
		
	}
	
		
}

//-----------------------------------------------------------------------------------------------------

?>
