<?php
include_once( "../../config/init.php" );

 ?>
 
<div style="float:left; width:600px;">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_othercategories.htm.php</span>
<?php } ?>
	
	 <center><p><b>Autre catégorie</b>&nbsp;&nbsp;<a href="#" onclick="getCategories();">
    					<img style="height:20px;"src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create2.png" alt="" />
    					
    </div>				</a></p></center>
<br><br>
<div class="subTitleContainer"  style="cursor:move;">
	<p class="subTitle">Liste des catégories</p>
</div>
	    <div  id="results" class="tableContainer">
    
		<table class="dataTable resultTable" >
			<thead><tr>
			<th> ID catégorie </th>
			<th>Nom catégorie</th>
			<th> ID catégorie </th>
			</tr></thead><tbody>
<?php

if( isset( $_REQUEST[ "idproduct" ] ) ){

	$idproduct=intval( $_REQUEST[ "idproduct" ] );
$rs=DBUtil::query( "SELECT * from category_product where  idproduct=$idproduct" );

if($rs->RecordCount()>0){

while( !$rs->EOF() ){
$idcategory=$rs->fields("idcategory");
$rs1=DBUtil::query( "SELECT name_1 from category where  idcategory=$idcategory" );
$name=$rs1->fields("name_1");
?>

<tr id="tr<?php echo $idcategory;?>"><td ><?php echo $idcategory;?></td><td><?php echo Util::doNothing($name) ; ?></td><td><img class="recycle" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" alt="" style="cursor:pointer;" onclick="deleteCateg( <?php echo $idcategory; ?> );" /></td></tr>
<?php

$rs->MoveNext();	
}
}
}
?>

</tbody>
		</table>
</div>

<script type="text/javascript">
function getCategories(){

		var explorerURL = "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_categories.htm.php?idproduct=<?php echo intval( $_REQUEST[ "idproduct" ] ); ?>&onSelectCategoryCallback=";
		

		$.ajax({

			url: explorerURL,
			async: false,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger la liste des catégories" ); },
		 	success: function( responseText ){

				$( "#globalMainContent" ).append( '<div id="CategoryParentDialog"></div>' );

				$("#CategoryParentDialog").html( responseText );
        		$("#CategoryParentDialog").dialog({

        			title: "Sélection d'une autre catégorie",
        			width:  500,
        			height: 500,
        			close: function(event, ui) {

        				$("#CategoryParentDialog").dialog( 'destroy' );
        				$("#CategoryParentDialog").remove();

        			}

        		});

			}

		});

	}
function deleteCateg( idcategNew ){

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo intval( $_REQUEST[ "idproduct" ] ); ?>&idcategNew=" + idcategNew+"&delete_cat",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert(responseText );
				else{

					$( "#tr" + idcategNew ).remove();

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

				}

			}

		});

	}
</script>