<?php

define( "IMAGE_TYPE_GIF", 1 );
define( "IMAGE_TYPE_JPEG", 2 );
define( "IMAGE_TYPE_PNG", 3 );

//---------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Codifier.php" );
include_once( "$GLOBAL_START_PATH/objects/Util.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

if( !count( $_FILES ) ){ //@todo : bug plugin Ajax?? l'encodage est différent selon que des fichiers soient postés ou non

	$_POST 		= Util::arrayMapRecursive( "Util::doNothing", $_POST );
	$_REQUEST 	= Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	$_GET 		= Util::arrayMapRecursive( "Util::doNothing", $_GET );

}

//---------------------------------------------------------------------------------------------
/* update - ajax */

if( isset( $_REQUEST[ "update" ] ) && isset( $_REQUEST[ "property" ] ) && isset( $_REQUEST[ "value" ] ) && isset( $_REQUEST[ "idarticle" ] ) ){
	
	$ret = DBUtil::query( "UPDATE detail SET `" . Util::html_escape( $_REQUEST[ "property" ] ) . "` = '" . Util::html_escape( $_REQUEST[ "value" ] ) . "' WHERE idarticle = '" . intval( $_REQUEST[ "idarticle" ] ) . "' LIMIT 1" );
	
	exit( $ret === false ? "0" : "1" );
	
}
//---------------------------------------------------------------------------------------------
/* suppression d'une référence - ajax */

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "idarticle" ] ) ){
	
	DBUtil::query( "DELETE FROM detail WHERE idarticle =  '" . Util::html_escape( $_REQUEST[ "idarticle" ] ) . "' LIMIT 1" );
	DBUtil::query( "DELETE FROM intitule_link WHERE idarticle =  '" . Util::html_escape( $_REQUEST[ "idarticle" ] ) . "'" );
	exit();	
}

//---------------------------------------------------------------------------------------------
/* champs rendu éditable */

if( isset( $_POST[ "editable" ] ) ){
	
	DBUtil::query( "UPDATE desc_field SET edit_avalaible = ". intval( $_POST[ "editable" ] ) . " WHERE tablename LIKE 'detail' AND fieldname LIKE '" . Util::html_escape( $_POST[ "fieldname" ] ) . "' LIMIT 1" );
	exit();
		
}

//---------------------------------------------------------------------------------------------
/* champs éditables */

$editableFields = array(

	"available" 			=> array( "name" => "Disponible à l'affichage", 	"editable" => true ),
	"ref_supplier" 			=> array( "name" => "Référence fournisseur", 		"editable" => true ),
	"display_detail_order"	=> array( "name" => "Ordre d'affichage", 			"editable" => true ),
	"buyingcost"			=> array( "name" => "Prix d'achat HT", 				"editable" => true ),
	"sellingcost"			=> array( "name" => "Prix de vente", 	     		"editable" => true ),
	"suppliercost" 			=> array( "name" => "Prix de vente fabricant", 		"editable" => true ),
	"ratecost"				=> array( "name" => "Marge brute %", 				"editable" => true ),
	"idvat"					=> array( "name" => "TVA Intracomm.", 				"editable" => true ),
	"code_customhouse"		=> array( "name" => "Code douane", 					"editable" => true ),
	"franco"				=> array( "name" => "Autoriser le franco", 			"editable" => true ),
	"weight"				=> array( "name" => "Poids (kg)",					"editable" => true ),
	"idcolor"				=> array( "name" => "Coloris",						"editable" => true ),
	"delivdelay"			=> array( "name" => "Délai de livraison", 			"editable" => true ),
	"stock_level"			=> array( "name" => "Stock", 						"editable" => true ),
	"minstock"				=> array( "name" => "Stock min.", 					"editable" => true ),
	"lot"					=> array( "name" => "Qté/Lot", 						"editable" => true ),
	"min_cde"				=> array( "name" => "Minimum de cde", 				"editable" => true ),
	"gencod"				=> array( "name" => "Gencod", 						"editable" => true ),
	"overhead_charges"		=> array( "name" => "Frais généraux", 				"editable" => true ),
	"indirect_expenses"		=> array( "name" => "% de coûts indirects", 		"editable" => true ),
	"allowed_discount_bill" => array( "name" => "Remise facture autorisée", 	"editable" => true ),
	"garantee"				=> array( "name" => "Garantie", 					"editable" => true ),
	"accept_nommenclature"	=> array( "name" => "Accepte la nomenclature", 		"editable" => true ),
	"nomenclature"			=> array( "name" => "Nomenclature", 				"editable" => true ),
	"weight_raw"			=> array( "name" => "Poids brut", 					"editable" => true ),
	"serial"				=> array( "name" => "Comparateur", 					"editable" => true ),
	"store"					=> array( "name" => "Allée", 						"editable" => true ),
	"span"					=> array( "name" => "Rayon", 						"editable" => true ),
	"site"					=> array( "name" => "Emplacement", 					"editable" => true ),
	"image_index"			=> array( "name" => "Image", 						"editable" => true )
	
);

uasort( $editableFields, "compareEditableFields" );

$fieldnames = array_keys( $editableFields );

$rs =& DBUtil::query( "SELECT fieldname, edit_avalaible FROM desc_field WHERE tablename LIKE 'detail' AND fieldname IN( '" . implode( "','", $fieldnames ) . "' )" );

while( !$rs->EOF() ){

	$editableFields[ $rs->fields( "fieldname" ) ][ "editable" ] = $rs->fields( "edit_avalaible" ) == 1;
	
	$rs->MoveNext();
		
}

//---------------------------------------------------------------------------------------------
/* list des champs editables */

if( isset( $_REQUEST[ "editable_fields" ] ) ){
	
	listEditableFields();
	exit();
	
}


//---------------------------------------------------------------------------------------------

$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );
$langue=User::getInstance()->getLang2();
//---------------------------------------------------------------------------------------------

$uploadErrors = array();

//---------------------------------------------------------------------------------------------		
//construction de la page

$idproduct = $tableobj->get( "idproduct" );

$references = getReferences( $idproduct );
$headings = getHeadings( $idproduct );

//nombre de références par ligne:

$referencePerLine = isset( $_REQUEST[ "referencePerLine" ] ) ? min( count( $references ), intval( $_REQUEST[ "referencePerLine" ] ) ) : min( count( $references ), 4 );

?>
<style type="text/css">
	.flexigrid input[type=text]{ border-style:none; background-color:transparent;}
	.flexigrid div.fbutton .add{ background: transparent url(<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/flexigrid_button_create.png) no-repeat scroll left center; }
	.flexigrid div.fbutton .headings, .flexigrid div.fbutton .editableFields{ background: transparent url(<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/flexigrid_button_headings.png) no-repeat scroll left center; }
	.flexigrid tr.erow td.empty{
		background:#FAFAFA url(<?php echo $GLOBAL_START_URL; ?>/js/flexigrid/css/flexigrid/images//fhbg.gif) repeat-x scroll center top;
		border-color:#CCCCCC;
		border-style:solid none solid none;
		border-width:1px 0 1px 0;
		height:20px;
		font-weight:bold;
	}
</style>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL; ?>/js/flexigrid/css/flexigrid/flexigrid.css">
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/flexigrid/flexigrid.js"></script>

<?php
include_once( $GLOBAL_START_PATH."/objects/DBUtil.php" );
?>

<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
		$( document ).ready( function() { 
	
		var akilae_crm= '<?php echo DBUtil::getParameterAdmin("akilae_crm"); ?>';
		var codification = '<?php echo DBUtil::getParameterAdmin("codification"); ?>';
        
		if ( akilae_crm == 1 )
		{
		
		$('.flexigrid').flexigrid({
		
			showTableToggleBtn: true,
			width: "auto",
			height: "auto",
			
			
				
			buttons : [
				{name: 'Voir les intitulés', bclass: 'headings', onpress : loadHeadings},
				{name: 'Liste des champs éditables', bclass: 'editableFields', onpress : listEditableFields},
				{separator: true}				
			],
		
			
			title: 'Références du produit',
			useRp: true,
			rp: 15
			
			});
		
		}
		else
        if( codification !='MANUEL'){
			$('.flexigrid').flexigrid({
		
			showTableToggleBtn: true,
			width: "auto",
			height: "auto",
			
			
				
			buttons : [
				{name: 'Voir les intitulés', bclass: 'headings', onpress : loadHeadings},
				{name: 'Liste des champs éditables', bclass: 'editableFields', onpress : listEditableFields},                
				{name: 'Ajouter une référence', bclass: 'add', onpress : createReference},    
				{separator: true}				
			],
		
			
			title: 'Références du produit',
			useRp: true,
			rp: 15
			
			});
		
		}
        else{
            
           	$('.flexigrid').flexigrid({
		
			showTableToggleBtn: true,
			width: "auto",
			height: "auto",
			
			
				
			buttons : [
				{name: 'Voir les intitulés', bclass: 'headings', onpress : loadHeadings},
				{name: 'Liste des champs éditables', bclass: 'editableFields', onpress : listEditableFields},                
				//{name: 'Ajouter une référence', bclass: 'add', onpress : createReference},               
				{separator: true}				
			],
		
			
			title: 'Références du produit',
			useRp: true,
			rp: 15
			
			});
        }
			
	});

		
	
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function duplicateReference( idarticle ){ 
        $.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&duplicate=reference&idarticle=" + idarticle,
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de dupliquer la référence" ); },
		 	success: function( responseText ){
		 		
				//$( '#ProductTabs' ).tabs( 'load', 4 );
        		$( '#ProductTabs' ).tabs( 'load', 3 );
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function deleteReference( idarticle ){ 

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_references.htm.php?delete&idarticle=" + idarticle,
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer la référence" ); },
		 	success: function( responseText ){

				$( '#ProductTabs' ).tabs( 'load', 4 );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function createReference(){ 
	      
      	   var codification = '<?php echo DBUtil::getParameterAdmin("codification"); ?>';
          if( codification == 'MANUEL'){
            if($('#reference').val() == '')
               alert('Veuillez saisir la nouvelle référence');
            else
    		document.location = "/product_management/catalog/detail.php?create&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&reference="+$('#reference').val();
		 }
         else
         	document.location = "/product_management/catalog/detail.php?create&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
         
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function loadHeadings(){
	
		$( '#ProductTabs' ).tabs( 'select', 1 );
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function listEditableFields(){
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_references.htm.php?editable_fields",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer la liste" ); },
		 	success: function( responseText ){
		 		
				$( "#globalMainContent" ).append( '<div id="EditableFieldsDialog"></div>' );
				$("#EditableFieldsDialog").html( responseText );
				$("#EditableFieldsDialog").dialog({
				
					title: "Liste des champs éditables",
					width:  500,
					height: 600,
					close: function(event, ui) { $("#EditableFieldsDialog").dialog( 'destroy' ); $("#EditableFieldsDialog").remove(); if( editableFieldsUpdated ) $( "#ProductTabs" ).tabs( "load", 1 ) }
				
				});
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function hidecost( idarticle ){
	
		var sellingCostEdit = document.getElementById( 'sellingcost_' + idarticle );
		var icon = document.getElementById( 'hidecostIcon_' + idarticle );
		var hidecost = document.getElementById( 'hidecost_' + idarticle );
		var defaultSellingcost = document.getElementById( 'defaultSellingcost_' + idarticle );
		
		if( hidecost.value == 1 ){
			
			sellingCostEdit.disabled = '';
			sellingCostEdit.value = defaultSellingcost.value;
			icon.src = '<?php echo  $GLOBAL_START_URL ?>/images/back_office/content/visible.gif';
			hidecost.value = 0;
			
		}
		else{
			
			sellingCostEdit.disabled = 'disabled';
			defaultSellingcost.value = sellingCostEdit.value;
			sellingCostEdit.value = 'Contactez-nous';
			icon.src = '<?php echo  $GLOBAL_START_URL ?>/images/back_office/content/hidden.gif';
			hidecost.value = 1;
			
		}
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function hideReference( idarticle, icon ){
		
		var available = $( "#available" + idarticle ).val() == "1" ? "0" : "1";
	
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_references.htm.php?update&idarticle=" + idarticle + "&property=available&value=" + available,
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de sauvegarder les modifications" ); },
		 	success: function( responseText ){

		 		if( responseText == "1" ){

		 			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '10px';
			    	$.growlUI( '', 'Modifications sauvegardées' );
			    	
					$( "#available" + idarticle ).val( available );
					icon.src = available == "1" ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/hidden.gif";

		 		}
		 		else alert( "Impossible de sauvegarder les modifications" + responseText );
		 		
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function setReferenceCountPerLine( referencePerLine ){

		$(":input").attr( "disabled", "disabled" );
		
		$.blockUI({ 
	
			message: "Chargement en cours...",
			css: { 'font-size': '10px', padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
			fadeIn: 0, 
        	fadeOut: 700
			
		});
		
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
		
		$.ajax({

			url: "<?php echo  $GLOBAL_START_URL ?>/templates/product_management/product_references.htm.php?referencePerLine=" + referencePerLine,
			async: true,
			cache: false,
			type: "GET",
			data: "<?php if( $tableobj->get( "idproduct" ) ) echo "idproduct=" . $tableobj->get( "idproduct" ); ?>",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger le template" ); },
		 	success: function( responseText ){

				$( "#FormContent" ).html( responseText );
        		initRequiredFields();
        		$(":input").attr( "disabled", "" );
        		$.unblockUI();
        		
			}

		});
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function displayPriceHistory( idarticle, page ){

		var url = "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/price_history.php?idarticle=" + idarticle + "&page=" + page;

		$.ajax({

			url: url,
			async: false,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'historique" ); },
		 	success: function( responseText ){
	
				if( !document.getElementById( 'PriceHistoryDialog' ) ){
				
					$( "#globalMainContent" ).append( '<div id="PriceHistoryDialog"></div>' );
					
					$("#PriceHistoryDialog").dialog({
	        		
	        			title: "Historique des prix",
	        			width:  500,
	        			height: 500,
	        			close: function(event, ui) { 
	        			
	        				$("#PriceHistoryDialog").dialog( 'destroy' ); 
	        				$("#PriceHistoryDialog").remove();
	        				
	        			}
	        		
	        		});
	        		
	        	}
	        	
				$("#PriceHistoryDialog").html( responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------ */
	
	var color_idarticle;
	
	function colorDialog( idarticle ){

		color_idarticle = idarticle;
		
		$.ajax({
		 	
			url: "/product_management/catalog/palette.php?callback=selectColor",
			async: true,
			type: "GET",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le popup" ); },
		 	success: function( responseText ){

				$( "#ColorDialog" ).html( responseText );
				$( "#ColorDialog" ).dialog({
			        
					modal: true,	
					title: "Sélectionner une couleur",
					close: function(event, ui) { 
					
						$("#ColorDialog").dialog( "destroy" );
						
					},
					width: 600,
					height: 600
					
				});

			}

		});

	}

	/* ------------------------------------------------------------------------------------------------------ */

	function selectColor( idreference_color, label, hexvalue, image ){

		if( color_idarticle == null )
			return;
		
		$( "#idreference_color" + color_idarticle ).val( idreference_color );

		var infos = '<div style="';
		infos += 'background-image:' + image + ';';
		infos += 'background-color:' + hexvalue + ';'; 
		
		infos += ' padding:0px; margin:0px; width:15px; height:15px; border:1px solid #919191;"></div> ';
		infos += label;
		infos += '<a href="#" onclick="deleteColor('+ color_idarticle + '); return false;"><img src="/images/back_office/content/corbeille.jpg" alt="Supprimer la couleur" /></a>';
		
		$( "#ColorInfos" + color_idarticle ).html( infos );

		$("#ColorDialog").dialog( "destroy" );
		
	}

	/* ------------------------------------------------------------------------------------------------------ */
	
	function deleteColor( idarticle ){

		$( "#idreference_color" + idarticle ).val( "0" );
		$( "#ColorInfos" + idarticle ).html( "" );
		
	}
	
	/* ------------------------------------------------------------------------------------------------------ */
	
	function imageIndexDialog( idarticle ){

		$("#ImageIndexDialog").dialog({
    		
			title: "Images disponibles",
			width:  650,
			height: 200,
			close: function(event, ui) { 
			
				$("#ImageIndexDialog").dialog( 'destroy' ); 
				
			}
		
		});

		$( "#ReferenceImageList li a" ).each( function( i ){

			$( this ).click( function(){

				var src = $( this ).find( "img" ).eq( 0 ).attr( "src" );
				var index = $( this ).attr( "id" ).split( "_" )[ 1 ];

				$( "#ImageIndex" + idarticle ).val( index );
				$( "#ReferenceImage" + idarticle ).attr( "src", src );
				
				$("#ImageIndexDialog").dialog( 'destroy' );
				 
				return false;
				
			});
			
		});
		
	}

	/* ------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<style type="text/css">

	#ReferenceImageList{
		
		margin: 20px auto 20px auto;
		list-style-type: none;
		padding: 0px;
		
	}
	
	#ReferenceImageList li{
		
		margin-bottom: 0px;
		margin-top: 4px;
		/*width:60px;*/
		/*height:60px;*/
		float:left;
		border:1px solid #E7E7E7;
		margin:4px;
		
	}

</style>
<div id="ImageIndexDialog" style="display:none;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<p>Sélectionner l'image que vous voulez associer à votre référence :</p>
	<ul id="ReferenceImageList">
		<li>
			<a id="ProductImage_0" href="#" onclick="return false;">
				<img src="<?php echo URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE, 0 ); ?>" alt="" />
			</a>
		</li>
		<?php
		
			$index = 1;
			while( $files = glob( dirname( __FILE__ ) . "/../../images/products/$idproduct.$index{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ){
			
				?>
				<li>
				  <a id="ProductImage_<?php echo $index; ?>" href="#" onclick="return false;">
					  <img src="<?php echo URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE, $index ); ?>" alt="" />
				  </a>
				</li>
				<?php
				
				$index++;
			
			}

	?>
	</ul>
</div>
<div id="ColorDialog" style="display:none;"></div>
<?php
//---------------------------------------------------------------------------------------------
//========================================
// Gestion des différents paramètres
//========================================
?>

<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Gestion des différents paramètres</p>
</div>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_references.htm.php</span>
<?php } ?>

<div class="tableContainer">
	<table class="dataTable">
	<?php if ($tableobj->GetParameterAdmin('display_hide_family')) { ?>
		<tr>
			<th class="filledCell" width="200">Utilisation famille d'intitulés</th>
			<td colspan="3">
				<?php $hide_family = $tableobj->get("hide_family"); ?>
				<select name="hide_family">
					<option value="1"<?php if( $hide_family == 1 ) echo " selected=\"selected\""; ?>>Ne pas utiliser</option>
					<option value="0"<?php if( $hide_family == 0 ) echo " selected=\"selected\""; ?>>Utiliser</option>
				</select>
			</td>
		</tr>
	<?php } ?>
	</table>
</div>
<div class="clear"></div>	


<?php
//---------------------------------------------------------------------------------------------
//========================================
// Gestion des références
//========================================
?>

<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Gestion des références </p>
</div>
<?php
displayForm( $idproduct, $headings, $references );

//---------------------------------------------------------------------------------------------

function getHeadings( $idproduct ){
	
$langue = User::getInstance()->getLang2();
			
	$query = "
	 SELECT DISTINCT( il.idintitule_title ), 
		ifam.idintitule_family, ifam.intitule_family$langue, 
		it.idintitule_title, it.intitule_title$langue
	FROM intitule_link il, 
		intitule_title it,
		intitule_family ifam
	WHERE il.idproduct = '$idproduct'
	AND il.idintitule_title = it.idintitule_title
	AND il.idfamily = ifam.idintitule_family
	ORDER BY il.idfamily ASC, il.family_display ASC, il.title_display ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les intitulés et leurs familles" );
		
	$headings = array();
	
	while( !$rs->EOF() ){
		
		//$family 			= $rs->fields( "intitule_family$langue" );
		$idintitule_family 	= $rs->fields( "idintitule_family" );
		$idintitule_title 	= $rs->fields( "idintitule_title" );
		$intitule_title 	= $rs->fields( "intitule_title$langue" );
		
		$headings[ $idintitule_family ][ $idintitule_title ] = $intitule_title;
		
		$rs->MoveNext();
		
	}
	
	return $headings;
	
}

//---------------------------------------------------------------------------------------------

function getReferences( $idproduct ){
	
$langue = User::getInstance()->getLang2();
			
	$references = array();
	
	$selectedColumns = array( 
	
		"idarticle", 
		"reference",
		"available",
		"ref_supplier",
		"display_detail_order",
		"buyingcost",
		"sellingcost",
		"suppliercost",
		"ratecost",
		"idvat",
		"code_customhouse",
		"hidecost",
		"franco",
		"weight",
		"idreference_color",
		"delivdelay",
		"stock_level",
		"minstock",
		"unit",
		"lot",
		"min_cde",
		"gencod",
		"overhead_charges",
		"indirect_expenses",
		"allowed_discount_bill",
		"garantee",
		"accept_nommenclature",
		"nomenclature",
		"weight_raw",
		"serial",
		"store",
		"span",
		"site",
		"image_index"
	
	);
		
	$query = "
	SELECT " . implode( ", ", $selectedColumns ) . "
	FROM detail
	WHERE idproduct = '$idproduct'
	ORDER BY display_detail_order ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer références du produit" );
	
	$i = 0;
	while( !$rs->EOF() ){
		
		$references[ $i ] = array();
		
		foreach( $selectedColumns as $column )
			$references[ $i ][ $column ] = $rs->fields( $column );
		
		$rs->MoveNext();
		
		$i++;
		
	}

	return $references;
	
}

//---------------------------------------------------------------------------------------------

function displayForm( $idproduct, $headings, $references ){

	global	
			$GLOBAL_START_URL,
			$referencePerLine,
			$tableobj;

	
	global $uploadErrors;
	$langue = User::getInstance()->getLang2();
	foreach( $uploadErrors as $uploadError )
		echo "<p style=\"color:#FF0000; font-weight:bold; margin:0px;\">" . htmlentities( $uploadError ) . "</p>";
	
	?>
	<input type="hidden" name="UpdateReferences" value="1" />
	<?php
	$codification_ref  = DBUtil::getParameterAdmin("codification");
	if( count( $references ) == 0 ){
	
		?>
	    <?php if( $codification_ref != 'MANUEL'):?>
            <p style="margin:20px 0px; text-align:center;">Ce produit ne possède aucune référence. Vous pouvez créer une référence en cliquant sur le lien ci-dessous :</p>
            <p><a   href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/detail.php?create&amp;idproduct=<?php echo $tableobj->get( "idproduct" ); ?>">
              <img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="" /> Créer une référence</a>
        <?php else:?>	
		<p style="margin:20px 0px; text-align:center;">Ce produit ne possède aucune référence. Vous pouvez saisir la nouvelle référence puis cliquer sur le lien ci-dessous :</p>
        <p style="margin:20px 0px; text-align:center;"><input type="text" id="reference"  />
			
				<a  onclick="javascript:createReference()"  href="#" >
               
                <img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="" /> Ajouter la référence
			</a>
       <?php endif ?>
			<a href="#" onclick="$( '#ProductTabs' ).tabs( 'select', 2 ); return false;" style="margin-left:15px;">
				<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_preview.png" alt="" /> Voir les intitulés
			</a>
		</p>	
		<?php
		
	
        
	}
   
    if( count( $references ) > 0 && $codification_ref == 'MANUEL'){?>
      <p style="margin:20px 0px; text-align:center;">	Nouvelle référence: <input type="text" id="reference"  />     
		
		<a  onclick="javascript:createReference()" href="#" >               
        <img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="" /> Ajouter
     	</a>
    </p>
            
    <?php 
      
    }
	
	?>
	<div style="margin-bottom:20px;">
	<?php
	
	$tableCount = $referencePerLine ? ceil( count( $references ) / $referencePerLine ) : 0;
	$i = 0;
	while( $i < $tableCount ){
	
		$start = $i * $referencePerLine;
	
		if( $i )
			echo "
			<br style=\"clear:both;\" />";
			
		displayTable( $i, $idproduct, $headings, array_slice ( $references , $start , $referencePerLine  ) );
			
		$i++;
		
	}

	?></div><?php
	
}

//---------------------------------------------------------------------------------------------

function displayTable( $tableIndex, $idproduct, $headings, $references ){

	global 	$langue,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL,
			$referencePerLine,
			$tableobj,
			$editableFields;

	$referenceCount = DBUtil::query( "SELECT COUNT(*) AS `count` FROM detail WHERE idproduct = '" . $tableobj->get( "idproduct" ) . "'" )->fields( "count" );
	
	?>
	<p style="text-align:right; margin:10px 0px;">
		Afficher <select name="referencePerLine" onchange="setReferenceCountPerLine( this.options[ this.selectedIndex ].value );"> 
		<?php

		$i = 1;
		while( $i <= $referenceCount ){
		
			$selected = $i == $referencePerLine ? " selected=\"selected\"" : "";
			
			?>
			<option value="<?php echo  $i ?>"<?php echo  $selected ?>><?php echo  $i ?></option>
			<?php
			
			$i++;
			
		}
		
		?>
		</select> référence(s) par ligne
	</p>
	<?php
	
	//les ordres d'affichage
	
	$rs =& DBUtil::query( "SELECT idarticle, display_detail_order FROM detail WHERE idproduct = '$idproduct' ORDER BY display_detail_order ASC" );
	$display_detail_orders = array();
	
	while( !$rs->EOF() ){
		
		$display_detail_orders[] = $rs->fields( "idarticle" );
		
		$rs->MoveNext();
		
	}

	//les tva
	
	$rs =& DBUtil::query( "SELECT idarticle, idvat FROM detail WHERE idproduct = '$idproduct'" );
	$idvats = array();
	
	while( !$rs->EOF() ){
		
		$idvats[ $rs->fields( "idarticle" ) ] = $rs->fields( "idvat" );
		
		$rs->MoveNext();
		
	}
	
	//NOTE : flexygrid supprimera les champs cachés placés entre <THEAD> et </THEAD>
	foreach( $references as $reference ){
		
		?>
		<input type="hidden" id="available<?php echo $reference[ "idarticle" ] ?>" name="available[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "available" ]; ?>" />
		<?php
		
	}
	
	//affichage du tableau
				
	?>
	<table class="flexigrid" id="ReferenceTable_<?php echo $tableIndex; ?>">
		<thead>
			<tr>
				<th width="<?php echo ceil( 720 / ( $referencePerLine + 1 ) ); ?>"><b>Références</b></th>
				<?php
	
				//liens vers les fiches Référence : faut mettre des machins dans la session :o/
					
				$_SESSION[ "detail.php" ][ "search_select" ] = "WHERE idproduct = '$idproduct'";
				$_SESSION[ "detail.php" ][ "search_order" ] = "ORDER BY A.reference ASC";
				$_SESSION[ "detail.php" ][ "search_startpage" ] = 0;
					
				foreach( $references as $reference ){

					$url = "/product_management/catalog/detail.php?idarticle=" . $reference[ "idarticle" ]

					?>
					<th width="<?php echo ceil( 720 / ( $referencePerLine + 1 ) ); ?>">
					<?php
					
					$codification = DBUtil::getParameterAdmin( "codification" );
	
					if( $codification == "AXESS" || $codification == "EPI" ){
						
						?>
						<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
							if( $akilae_crm == 0 ){ ?>
		<a href="#" onclick="duplicateReference( <?php echo $reference[ "idarticle" ]; ?> ); return false;" title="Dupliquer la référence">
							<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/editcopy.png" alt="Dupliquer la référence" />
						</a>
								<?php } ?>
						
						<?php
						
					}
					
					?>
						<a class="blueLink" href="<?php echo $url; ?>">
							<?php echo htmlentities( $reference[ "reference" ] ); ?>
						</a>
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo  $reference[ "available" ] ? "visible" : "hidden" ?>.gif" alt="" style="cursor:pointer;" onclick="hideReference('<?php echo $reference[ "idarticle" ]; ?>', this ); return false;" />
					</th>
					<?php
						
				}
					
				?>
			</tr>
		</thead>
		<tbody>
		<?php
		
				/* référence fournisseur */
				
				if( $editableFields[ "ref_supplier" ][ "editable" ] ){
					
					?>
					<tr>
						<td>Réf. fournisseur</td>
						<?php
						
							//référence fournisseur
							
							$i = 0;
							foreach( $references as $reference ){
								
								$class = $i % 2 ? "column1" : "column2";
								
								?>
								<td>
									<input type="text" name="ref_supplier[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "ref_supplier" ] ?>" />
								</td>
								<?php
								
								$i++;
								
							}
							
						?>
					</tr>
					<?php
				
				}

			?>
			<tr class="erow">
				<td colspan="<?php $colspan = 1 + count( $references ); echo $colspan; ?>" class="empty" style="height:15px;">Tarification</td>
			</tr>
			<?php
			
			/* Prix d'achat HT */
			
			if( $editableFields[ "buyingcost" ][ "editable" ] ){
				
				?>
				<tr>
					<td>Prix d'achat HT</td>
					<?php
					
						//prix d'achat
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<span style="float:right;">
									<?php displayPriceHistory( $reference[ "idarticle" ] ); ?>
								</span>
								<input type="text" name="buyingcost[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  Util::priceFormat( $reference[ "buyingcost" ], 2 ) ?>" onkeypress="return forceUnsignedFloatValue( event );" style="width:50px;" />								
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Prix de vente HT */
			
			if( $editableFields[ "sellingcost" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo $editableFields[ "sellingcost" ][ "name" ]; ?> <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?></td>
					<?php
					
						//prix de vente catalogue
						
						$i = 0;
						foreach( $references as $reference ){
							
							$hidecost = $reference[ "hidecost" ];
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<span style="float:right;">
									<?php displayPriceHistory( $reference[ "idarticle" ] ); ?>
								</span>
								<input type="hidden" name="hidecost[<?php echo  $reference[ "idarticle" ] ?>]" id="hidecost_<?php echo $reference[ "idarticle" ] ?>" value="<?php echo  $hidecost ?>" />
								<a href="#" onclick="hidecost( <?php echo  $reference[ "idarticle" ] ?> ); return false;"><img src="<?php echo  $GLOBAL_START_URL ?>/images/back_office/content/<?php echo  $hidecost ? "hidden" : "visible" ?>.gif" alt="" style="border-style:none;" id="hidecostIcon_<?php echo $reference[ "idarticle" ] ?>" style="vertical-align:top;" /></a>&nbsp;
								<input type="text" name="sellingcost[<?php echo  $reference[ "idarticle" ] ?>]" id="sellingcost_<?php echo $reference[ "idarticle" ] ?>" value="<?php echo  $hidecost ? "Contactez-nous" : Util::priceFormat( $reference[ "sellingcost" ], 2 ) ?>"<?php if( $hidecost ) echo " disabled=\"disabled\""; ?> onload="this.default='<?php echo  $reference[ "sellingcost" ] ?>';" onkeypress="return forceUnsignedFloatValue( event );" style="width:50px;" />
								<input type="hidden" id="defaultSellingcost_<?php echo $reference[ "idarticle" ] ?>" value="<?php echo  Util::priceFormat( $reference[ "sellingcost" ], 2 ) ?>" disabled="disabled" />
							</td>
							<?php
							
							$i++;
							
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Prix de vente fabricant */
			
			if( $editableFields[ "suppliercost" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "suppliercost" ][ "name" ] ) ?></td>
					<?php
					
						//prix fabricant
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="suppliercost[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  Util::priceFormat( $reference[ "suppliercost" ], 2 ) ?>" onkeypress="return forceUnsignedFloatValue( event );" style="width:50px;" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Marge brute éditable % */
			
			if( $editableFields[ "ratecost" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "ratecost" ][ "name" ] ) ?></td>
					<?php
											
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							$rough_stroke_amount = $reference[ "sellingcost" ] - $reference[ "buyingcost" ];
							$rough_stroke_rate = $reference[ "sellingcost" ] > 0.0 ? 100.0 * $rough_stroke_amount / $reference[ "sellingcost" ] : 0.0;
							
							?>
							<td>
								<?php echo Util::rateFormat( $rough_stroke_rate ); ?></div>
								<input type="hidden" name="ratecost[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo Util::numberFormat( $rough_stroke_amount );?>"  />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<tr>
					<td>Marge brute &euro;</td>
					<?php
											
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							$rough_stroke_amount = $reference[ "sellingcost" ] - $reference[ "buyingcost" ];
							
							?>
							<td>
								<?php echo Util::priceFormat( $rough_stroke_amount ); ?>
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}

			/* TVA Intracomm. */
			
			if( $editableFields[ "idvat" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "idvat" ][ "name" ] ) ?></td>
					<?php
					
						//TVA
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
							<?php
							
								$idarticle = $reference[ "idarticle" ];
		
								XHTMLFactory::createSelectElement( "tva", "idtva", "name", "name", $reference[ "idvat" ], $nullValue = 0, $nullInnerHTML = "-", $attributes = "name=\"idvat[ $idarticle ]\"" );
							
							?>
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
			
			/* franco */
			
			if( $editableFields[ "franco" ][ "editable" ] ){
				
				?>
				<!--<tr>
					<td><?php echo  htmlentities( $editableFields[ "franco" ][ "name" ] ) ?></td>
					<?php
					
						//autoriser le franco ou non pour cette référence
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="radio" name="franco[<?php echo  $reference[ "idarticle" ] ?>]" value="0"<?php if( !$reference[ "franco" ] ) echo " checked=\"checked\""; ?>> Non 
								<input type="radio" name="franco[<?php echo  $reference[ "idarticle" ] ?>]" value="1"<?php if( $reference[ "franco" ] ) echo " checked=\"checked\""; ?>> Oui	
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>-->
				<?php
			
			}
			
			/* frais généraux */
			
			if( $editableFields[ "overhead_charges" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "overhead_charges" ][ "name" ] ) ?></td>
					<?php
					
						//frais généraux
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="overhead_charges[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  round( $reference[ "overhead_charges" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* % de coûts indirects */
			
			if( $editableFields[ "indirect_expenses" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "indirect_expenses" ][ "name" ] ) ?></td>
					<?php
					
						//% de coûts indirects
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="indirect_expenses[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  round( $reference[ "indirect_expenses" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Remise facture autorisée */
			
			if( $editableFields[ "allowed_discount_bill" ][ "editable" ] ){
				
				?>
				<!--<tr>
					<td><?php echo  htmlentities( $editableFields[ "allowed_discount_bill" ][ "name" ] ) ?></td>
					<?php
					
						//Remise facture autorisée
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="allowed_discount_bill[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  round( $reference[ "allowed_discount_bill" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>-->
				<?php
			
			}
			
			/* Délai de livraison */
			
			if( $editableFields[ "delivdelay" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "delivdelay" ][ "name" ] ) ?></td>
					<?php
					
						//delais de livraison
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<?php listDelivDelays( $reference[ "idarticle" ], $reference[ "delivdelay" ] ) ?>
							</td>
							<?php
							
							$i++;
								
						}
						
					?>
				</tr>
				<?php
			
			}
			
			/* Minimum de cde */
			
			if( $editableFields[ "min_cde" ][ "editable" ] ){
				
				?>
				<!--<tr>
					<td><?php echo  htmlentities( $editableFields[ "min_cde" ][ "name" ] ) ?></td>
					<?php
					
						//minimum de commande
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="min_cde[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "min_cde" ] ?>" onkeypress="return forceUnsignedIntegerValue( event );" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>-->
				<?php
			
			}
			
			?>
			<tr class="erow">
				<td colspan="<?php $colspan = 1 + count( $references ); echo $colspan; ?>" class="empty" style="height:15px;">Caractéristiques</td>
			</tr>
			<?php
			
			//affichage des intitulés
			
			displayHeadings( $headings, $references );
			
			/* Coloris */
			
			if( $editableFields[ "idcolor" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "idcolor" ][ "name" ] ) ?></td>
					<?php
					
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input id="idreference_color<?php echo $reference[ "idarticle" ]; ?>" type="hidden" name="idreference_color[<?php echo $reference[ "idarticle" ]; ?>]" value="<?php echo $reference[ "idreference_color" ]; ?>" />
								<span id="ColorInfos<?php echo $reference[ "idarticle" ]; ?>">
								<?php
								
									if( $reference[ "idreference_color" ] ){
										
										$color = DBUtil::stdclassFromDB( "reference_color", "idreference_color", $reference[ "idreference_color" ] );
										
										?>
										<div style="<?php
										
											if( strlen( $color->image ) )
													echo "background-image:url(/images/colors/{$color->image})";
											else 	echo "background-color:#{$color->hexvalue}";
											
										?>; padding:0px; margin:0px; width:15px; height:15px; border:1px solid #919191;"></div> 
										<?php echo htmlentities( $color->name ); ?>
										<a href="#" onclick="deleteColor(<?php echo $reference[ "idarticle" ]; ?>); return false;"><img src="/images/back_office/content/corbeille.jpg" alt="Supprimer la couleur" /></a>
										<?php
								
									}
									
								?>
								</span>
								<a href="#" class="blueLink" style="display:block;" onclick="colorDialog(<?php echo $reference[ "idarticle" ]; ?>); return false;">sélectionnez</a>
							</td>
							<?php
							
							$i++;
							
						}
					
				?>
				</tr>
				<?php
			
			}
			
			/* Poids (kg) */
			
			if( $editableFields[ "weight" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "weight" ][ "name" ] ) ?></td>
					<?php
						
						//poids
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="weight[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "weight" ] ?>" onkeypress="return forceUnsignedFloatValue( event );" />
							</td>
							<?php
							
							$i++;
							
						}
						
					?>
				</tr>
				<?php
			
			}
			
			?>
			<tr class="erow">
				<td colspan="<?php $colspan = 1 + count( $references ); echo $colspan; ?>" class="empty" style="height:15px;">Divers</td>
			</tr>
			<?php

			/* Code douane */
			
			if( $editableFields[ "code_customhouse" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "code_customhouse" ][ "name" ] ) ?></td>
					<?php
					
						//code douane si le fournisseur est étranger
						
						//$localState 	= DBUtil::getDBValue( "paramvalue", "parameter_cat", "idparameter", "default_idstate" );
						//$supplierState 	= DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $tableobj->get( "idsupplier" ) );
								
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
							<?php
		
								$idarticle = $reference[ "idarticle" ];
		
								XHTMLFactory::createSelectElement( "code_customhouse", "code_customhouse", "code_customhouse_text", "code_customhouse_text", $reference[ "code_customhouse" ], $nullValue = "", $nullInnerHTML = "-", $attributes = "name=\"code_customhouse[ $idarticle ]\"" );
							
							?>
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
			
			
			
			/* image */
			/*
			if( $editableFields[ "image_index" ][ "editable" ] ){
				
				?>
				<tr>
					<td>Image</td>
					<?php
					
						$files = glob( dirname( __FILE__ ) . "/../../images/products/$idproduct.1{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
	
							if( !$files ){
								
								?>
								<td>par défaut</td>
								<?php
								
							}
							else{
								
								?>
								<td>
									<a href="#" class="blueLink" onclick="imageIndexDialog(<?php echo $reference[ "idarticle" ]; ?>); return false;">
										<img id="ReferenceImage<?php echo $reference[ "idarticle" ]; ?>" src="<?php echo URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE, $reference[ "image_index" ] ); ?>" alt="" />
									</a>
									<br />
									<a href="#" class="blueLink" onclick="imageIndexDialog(<?php echo $reference[ "idarticle" ]; ?>); return false;">modifier</a>
									<input id="ImageIndex<?php echo $reference[ "idarticle" ]; ?>" type="hidden" name="image_index[<?php echo $reference[ "idarticle" ]; ?>]" value="<?php echo $reference[ "image_index" ]; ?>" />
								</td>
								<?php
							
							}
							
							$i++;
							
						}
						
					?>
				</tr>
				<?php
			
			}
			*/
			/* ordre d'affichage */
			
			if( $editableFields[ "display_detail_order" ][ "editable" ] ){
				
				?>
				<tr>
					<td>Ordre d'affichage</td>
					<?php
					
						//ordre d'affichage
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<select name="display_detail_order[<?php echo  $reference[ "idarticle" ] ?>]">
								<?php
				
									$i = 1;
									while( $i <= count( $display_detail_orders ) ){
										
										$selected = $display_detail_orders[ $i - 1 ] == $reference[ "idarticle" ] ? " selected=\"selected\"" : "";
										
										?>
										<option value="<?php echo  $i ?>"<?php echo  $selected ?>><?php echo  $i ?></option>
										<?php
										
										$i++;
										
									}
									
								?>
								</select>
							</td>
							<?php
							
							$i++;
							
						}
						
					?>
				</tr>
				<?php
			
			}
			
			/* Stock */
			
			if( $editableFields[ "stock_level" ][ "editable" ] ){
				
				?>
				<tr>
					<td>
						<?php echo  htmlentities( $editableFields[ "stock_level" ][ "name" ] ) ?>
					</td>
					<?php
					
						//stock_level : quantité en stock ou -1 si on ne gère pas le stock pour cette référence
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							$defaultValue = $reference[ "stock_level" ] != -1 ? $reference[ "stock_level" ] : 1;
							
							?>
							<td>
								<ul style="list-style-type:none; display:inline;">
									<li style="float:left; padding-left:5px;">
										<input type="radio" name="stock_management_<?php echo $reference[ "idarticle" ] ?>" value="0"<?php if( $reference[ "stock_level" ] == -1 ) echo " checked=\"checked\""; ?> onclick="var element = document.getElementById( 'stock_level_<?php echo $reference[ "idarticle" ] ?>' ); element.style.display = 'none'; element.value = '-1';" style="vertical-align:bottom;" /> Non géré
									</li>
									<li style="float:left; padding-left:5px;">
										<input type="radio" name="stock_management_<?php echo $reference[ "idarticle" ] ?>" value="1"<?php if( $reference[ "stock_level" ] != -1 ) echo " checked=\"checked\""; ?> onclick="var element = document.getElementById( 'stock_level_<?php echo $reference[ "idarticle" ] ?>' ); element.style.display = 'block'; element.value = '<?php echo  $defaultValue ?>'; element.style.color= <?php echo  $defaultValue ?> ? '#000000' : '#FF0000';" style="vertical-align:bottom;" /> Géré
									</li>
									<li style="float:left; padding-left:5px;">
										<input class="editedField" type="text" id="stock_level_<?php echo $reference[ "idarticle" ] ?>" name="stock_level[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "stock_level" ] ?>" style="width:20px; display:<?php echo $reference[ "stock_level" ] == -1 ? "none" : "block"; ?>;<?php if( !$reference[ "stock_level" ] ) echo " color:#FF0000;"; ?>" onkeypress="return forceIntegerValue( event );" />
									</li>
								</ul>
							</td>
							<?php
							
							$i++;
								
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Stock min. */
			
			if( $editableFields[ "minstock" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "minstock" ][ "name" ] ) ?></td>
					<?php
					
						//minstock : stock minimum à partir duquel la référence apparaît dans la gestion du stock
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="minstock[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "minstock" ] ?>" onkeypress="return forceUnsignedIntegerValue( event );" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Qté/Lot */
			
			if( $editableFields[ "lot" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "lot" ][ "name" ] ) ?></td>
					<?php
					
						//quantité par lot
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td class="characValue <?php echo  $class ?>" style="white-space:nowrap;">
								<input type="text" name="lot[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "lot" ] ?>" style="text-align:right; width:25px;" onkeypress="return forceUnsignedIntegerValue();" />
								&nbsp;/&nbsp;<?php listUnits( $reference[ "idarticle" ], $reference[ "unit" ] ) ?>
							</td>
							<?php
							
							$i++;
								
						}
						
					?>
				</tr>
				<?php
			
			}
	
			/* Gencod */
			
			if( $editableFields[ "gencod" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "gencod" ][ "name" ] ) ?></td>
					<?php
					
						//code barre
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="gencod[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  $reference[ "gencod" ] ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}

			/* Garantie */
			
			if( $editableFields[ "garantee" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "garantee" ][ "name" ] ) ?></td>
					<?php
					
						//Garantie
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="garantee[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  htmlentities( $reference[ "garantee" ] ) ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Accepte la nomenclature */
			
			if( $editableFields[ "accept_nommenclature" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "accept_nommenclature" ][ "name" ] ) ?></td>
					<?php
					
						//Accepte la nommenclature
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="radio" name="accept_nommenclature[<?php echo  $reference[ "idarticle" ] ?>]" value="1"<?php if( $reference[ "accept_nommenclature" ] ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
								<input type="radio" name="accept_nommenclature[<?php echo  $reference[ "idarticle" ] ?>]" value="0"<?php if( !$reference[ "accept_nommenclature" ] ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Nomenclature */
			
			if( $editableFields[ "nomenclature" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "nomenclature" ][ "name" ] ) ?></td>
					<?php
					
						//Nommenclature
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="nomenclature[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  htmlentities( $reference[ "nomenclature" ] ) ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Poids brut */
			
			if( $editableFields[ "weight_raw" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "weight_raw" ][ "name" ] ) ?></td>
					<?php
					
						//Poids brut
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="weight_raw[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  round( $reference[ "weight_raw" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* N° de série */
			
			if( $editableFields[ "serial" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "serial" ][ "name" ] ) ?></td>
					<?php
					
						//n° série
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="serial[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  htmlentities( $reference[ "serial" ] ) ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Allée */
			
			if( $editableFields[ "store" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "store" ][ "name" ] ) ?></td>
					<?php
					
						//dépôt
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="store[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  htmlentities( $reference[ "store" ] ) ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Rayon */
			
			if( $editableFields[ "span" ][ "editable" ] ){
				
				?>
				<tr>
					<td><?php echo  htmlentities( $editableFields[ "span" ][ "name" ] ) ?></td>
					<?php
					
						//Rayonnage	
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="span[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  htmlentities( $reference[ "span" ] ) ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>
				<?php
			
			}
				
			/* Emplacement */
			
			if( $editableFields[ "site" ][ "editable" ] ){
				
				?>
				<!--<tr>
					<td><?php echo  htmlentities( $editableFields[ "site" ][ "name" ] ) ?></td>
					<?php
					
						//Emplacement	
						
						$i = 0;
						foreach( $references as $reference ){
							
							$class = $i % 2 ? "column1" : "column2";
							
							?>
							<td>
								<input type="text" name="site[<?php echo  $reference[ "idarticle" ] ?>]" value="<?php echo  htmlentities( $reference[ "site" ] ) ?>" />
							</td>
							<?php
						
							$i++;
									
						}
						
					?>
				</tr>-->
				<?php
				
			}
	
			//supprimer
			
			?>
			<tr class="erow">
				<td colspan="<?php $colspan = 1 + count( $references ); echo $colspan; ?>" class="empty" style="height:15px;"></td>
			</tr>
			<!--
			<tr>
				<td><b>Supprimer</b></td>
				<?php
				
					foreach( $references as $reference ){
						
						?>
						<td style="text-align:center;">
							<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" alt="Supprimer la référence" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette référence?' ) ) deleteReference( <?php echo $reference[ "idarticle" ]; ?> );" style="cursor:pointer;" />
						</td>
						<?php
								
					}
					
			?>
			</tr>
			-->
		</tbody>
	</table>
	<?php
	
}

//---------------------------------------------------------------------------------------------

function displayHeadings( &$headings, &$references ){
	$langue=User::getInstance()->getLang2();

	//$headings[ $idintitule_family ][ $idintitule_title ] = $intitule_title;
	
	foreach( $headings as $idintitule_family => $intitule_titles ){
		
		?>
		<!--
		<tr>
			<th colspan="<?php echo  $colspan = count( $references ) + 1; echo $colspan; ?>" style="font-weight:bold;">
				<?php echo htmlentities( getHeadingFamilyLabel( $idintitule_family ) ); ?>
			</th>
		</tr>
		-->
		<?php
		
		foreach( $intitule_titles as $idintitule_title => $intitule_title ){
		
			?>
			<tr>
				<td>
					<?php //echo htmlentities( $intitule_title );
						 echo  $intitule_title ; ?>
				</td>
				<?php
					
				$i = 0;
				foreach( $references as $reference ){
				
					$idarticle = $reference[ "idarticle" ];
					$idintitule_value = getIDIntituleValue( $idarticle, $idintitule_title );
					$intitule_value = $idintitule_value ? getIntituleValue( $idintitule_value ) : "";
					
					$class = $i % 2 ? "column1" : "column2";
					$inputClass = $i % 2 ? "input1" : "input2";
	
					?>

					<?php
					
					$ref = $reference[ "reference" ];
					$string = addslashes($intitule_title);
					
					$q1 = "
						SELECT idintitule_title
						FROM intitule_title
						WHERE intitule_title$langue  = '".$string."'
						
						";
	
						$res1 =& DBUtil::query( $q1 );
					$idtitle = $res1->fields( "idintitule_title" );

					$query = "
						SELECT d.idproduct,p.idcategory
						FROM detail d, product p
						WHERE d.idproduct = p.idproduct
						AND d.reference = '$ref'
						";
	
						$rs =& DBUtil::query( $query );
					$idcatparent = $rs->fields( "idcategory" );
					$idprods = $rs->fields( "idproduct" );
					
					$m = 0;
					while ( $idcatparent != 0 )
						{
	
						$qr4 = "
								SELECT idcategoryparent,idcategorychild
								FROM `category_link`
								WHERE 
								idcategorychild = '$idcatparent'
								";
						$res4 =& DBUtil::query( $qr4 );
	
						$idcatparent = $res4->fields( "idcategoryparent" );
						$idcatchild = $res4->fields( "idcategorychild" );
												
						$qr3 = "
								SELECT idcategory
								FROM search_filter
								WHERE 
								idcategory = '$idcatchild'
								";
						$res3 =& DBUtil::query( $qr3 );
						$cat = $res3->fields( "idcategory" );
						$n = $res3->RecordCount();						
						
						if ($n > 0)
						{
						$m++;

						}
						}
						
						if ($m > 0)
						{
						
						$q5 = "
						SELECT d.idproduct,p.idcategory
						FROM detail d, product p
						WHERE d.idproduct = p.idproduct
						AND d.reference = '$ref'
						";
	
						$res5 =& DBUtil::query( $q5 );
						$idcat = $res5->fields( "idcategory" );

						$qr4 = "
								SELECT idintitule_value
								FROM intitule_link
								WHERE 
								idcategory = '$idcat'
								AND idintitule_title = '$idtitle'
								";
						$res4 =& DBUtil::query( $qr4 );
						
						$tab1 = array();
						$tab2 = array();$k=0;	
						while( !$res4->EOF() )
						{
						$idval = $res4->fields( "idintitule_value" );

						$qr6 = "
								SELECT intitule_value$langue
								FROM intitule_value
								WHERE 
								idintitule_value = '". $idval. "'
								";
						$res6 =& DBUtil::query( $qr6 );
											
						$val = $res6->fields( "intitule_value$langue" );
						
						if ($val != 0 || $val != '')
						array_push( $tab1, $val );

						$res4->MoveNext();

						}
						
						$tab2 = array_unique($tab1);
						natsort($tab2);
						
						?>
<?php

			$qr5 = "SELECT *
				FROM search_filter
				WHERE 
				idintitule_title = '$idtitle'
				";
			$res5 =& DBUtil::query( $qr5 );


if ($res5->RecordCount() > 0)
{
?>						
						
<td class="characValue <?php echo  $class ?>">
						 <input list="intitule_values[<?php echo  $idarticle ?>,<?php echo  $idintitule_title ?>]" name="intitule_values[<?php echo  $idarticle ?>,<?php echo  $idintitule_title ?>]" value="<?php echo  htmlentities( $intitule_value ) ?>" style="width: 123px;" >

						<datalist id="intitule_values[<?php echo  $idarticle ?>,<?php echo  $idintitule_title ?>]">

<?php
foreach($tab2 as $resul2)
{
?>
 <option value="<?php echo $resul2; ?>">
<?php
}
?>						 
						</datalist> 
<?php

if( in_array( $intitule_value,$tab2 ))
{
?>
<img src="<?php $GLOBAL_START_URL ?>/images/back_office/content/enabled.png" alt="" />
<?php
}
else
{
?>
<img src="<?php $GLOBAL_START_URL ?>/images/back_office/content/croix.png" alt="" />
<?php
}
?>



 </td>
 <?php
}
else
{
?>

<td class="characValue <?php echo  $class ?>">
						<input type="text" name="intitule_values[<?php echo  $idarticle ?>,<?php echo  $idintitule_title ?>]" value="<?php echo  $intitule_value  ?>" class="<?php echo  $inputClass ?>" />
					</td>

 
 
 
						<?php
						}}
						else
						{
						?>
<td class="characValue <?php echo  $class ?>">
						<input type="text" name="intitule_values[<?php echo  $idarticle ?>,<?php echo  $idintitule_title ?>]" value="<?php echo  $intitule_value  ?>" class="<?php echo  $inputClass ?>" />
					</td>

<?php
						}

						?>
					


					
					<?php
					
					$i++;
					
				}
				
			?>
			</tr>
			<?php
		
		}
		
	}
	
}

//---------------------------------------------------------------------------------------------

function listDelivDelays( $idarticle, $selection ){
	
	global	$langue;
	
	$query = "
	SELECT iddelay,delay$langue AS delay
	FROM delay
	WHERE `available` = 1
	ORDER BY delay$langue ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des délais de livraison" );
		
	?>
	<select name="delivdelay[<?php echo  $idarticle ?>]">
	<?php
	
		while( !$rs->EOF() ){
			
			$delay = $rs->fields( "delay" );
			$iddelay = $rs->fields( "iddelay" );
			$selected = $iddelay == $selection ? " selected=\"selected\"" : "";
			
			?>
			<option value="<?php echo  $iddelay ?>"<?php echo  $selected ?>><?php echo  htmlentities( $delay ) ?></option>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</select>
	<?php

}

//---------------------------------------------------------------------------------------------

function listUnits( $idarticle, $selection ){
	
	global	$langue;
	
	$query = "
	SELECT idunit, unit$langue AS unit
	FROM unit
	ORDER BY unit$langue ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des unités" );

	?>
	<select name="unit[<?php echo  $idarticle ?>]" style="width:auto;">
	<?php
	
	while( !$rs->EOF() ){
		
		$unit = $rs->fields( "unit" );
		$idunit = $rs->fields( "idunit");
		
		$selected = $idunit == $selection ? " selected=\"selected\"" : "";
	
		?>
		<option value="<?php echo  $idunit ?>"<?php echo  $selected ?>><?php echo  htmlentities( $unit ) ?></option>
		<?php

		$rs->MoveNext();
		
	}
	
	?>
	</select>
	<?php
	
}

//---------------------------------------------------------------------------------------------

function getIntituleValue( $idintitule_value ){
	
	global $langue;
	
	if( !$idintitule_value )
		return "";

	$query = "
	SELECT intitule_value$langue AS value
	FROM intitule_value
	WHERE idintitule_value = '$idintitule_value'
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );

	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer la valeur de l'intitulé '$idintitule_title' pour l'article '$idarticle'" );
		
	return $rs->fields( "value" );
	
}

//---------------------------------------------------------------------------------------------

function getHeadingFamilyLabel( $idintitule_family ){

	global 	$langue;
			
	$query = "
	SELECT intitule_family$langue AS intitule_family
	FROM intitule_family 
	WHERE idintitule_family = '$idintitule_family' 
	LIMIT 1";

	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom de la famille d'intitulés" );

	return $rs->fields( "intitule_family" );
	
}

//---------------------------------------------------------------------------------------------

function listEditableFields(){
	
	global $GLOBAL_START_URL, $editableFields;
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		var editableFieldsUpdated = false;
		
		function setEditable( fieldname, editable ){
			
			var postData = "fieldname=" + escape( fieldname ) + "&editable=";
			postData += editable ? "1" : "0";
			
			$.ajax({

				url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_references.htm.php",
				async: true,
				cache: false,
				type: "POST",
				data: postData,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de rendre le champ éditable" ); },
			 	success: function( responseText ){
			 	
			 		editableFieldsUpdated = true;
			 	
			 	}
	
			});

		}
	
	/* ]]> */
	</script>
	<ul>
	<?php
	
		foreach( $editableFields as $fieldname => $properties ){
			
			?>
			<li>
				<input onclick="setEditable( '<?php echo $fieldname; ?>', this.checked );"<?php if( $properties[ "editable" ] ) echo " checked=\"checked\""; ?> type="checkbox" value="<?php echo htmlentities( $fieldname ); ?>" /> <?php echo htmlentities( $properties[ "name" ] ); ?>
			</li>
			<?php
			
		}
	
	?>
	</ul>
	<?php
	
}

//---------------------------------------------------------------------------------------------

function displayPriceHistory( $idarticle ){
	
	$query = "( SELECT idarticle FROM sellingcost_history WHERE idarticle = '$idarticle' ) UNION ALL ( SELECT idarticle FROM buyingcost_history WHERE idarticle = '$idarticle' ) LIMIT 1";
	
	if( !DBUtil::query( $query )->RecordCount() )
		return;
		
	global $GLOBAL_START_URL;
		
	?>
	<a title="Voir l'historique des prix" class="blueLink" href="#" onclick="displayPriceHistory( <?php echo $idarticle; ?>, 1 ); return false;">
		<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/history.png" alt="Voir l'istorique des prix" />
	</a>
	<?php

}

//---------------------------------------------------------------------------------------------

function compareEditableFields( $value1, $value2 ){

	return strcmp( strtolower( $value1[ "name" ] ), strtolower( $value2[ "name" ] ) );
	
}

//---------------------------------------------------------------------------------------------

?>
