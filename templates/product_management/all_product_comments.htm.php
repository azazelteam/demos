<?php

/* couleurs pour les champs obligatoires */

define( "DEFAULT_BACKGROUND_COLOR", "transparent" );
define( "DEFAULT_COLOR", "#000000" );
define( "REQUIRED_BACKGROUND_COLOR", "#DDDDDD" );
define( "REQUIRED_COLOR", "#000000" );
define( "ERROR_BACKGROUND_COLOR", "#FF8484" );
define( "ERROR_COLOR", "#000000" );

include_once( "../../objects/Util.php" );

if( !count( $_FILES ) ){ //@todo : bug plugin Ajax?? l'encodage est différent selon que des fichiers soient postés ou non

	$_POST 		= Util::arrayMapRecursive( "Util::doNothing", $_POST );
	$_REQUEST 	= Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	$_GET 		= Util::arrayMapRecursive( "Util::doNothing", $_GET );

}

//--------------------------------------------------------------------------------------------------------
/* tri des produits de la catégorie */

if( isset( $_POST[ "sort" ] ) && isset( $_POST[ "Siblings" ] ) ){

	include_once( "../../config/init.php" );

	$done = true;
	$i = 0;
	while( $i < count( $_POST[ "Siblings" ] ) ){

		$idchild = intval( $_POST[ "Siblings" ][ $i ] );
		$rs =& DBUtil::query( "UPDATE product SET display_product_order = '$i' WHERE idproduct = '$idchild' LIMIT 1" );

		$done &= $rs !== false;

		$i++;

	}

	exit( $done ? "1" : "0" );

}

//---------------------------------------------------------------------------------------------

include_once ("$GLOBAL_START_PATH/objects/WYSIWYGEditor.php");
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

WYSIWYGEditor::init( false );

?>
<style type="text/css">

	div#global div#globalMainContent,
	div#footer div#top div.right-site,
	div#footer div#top div.left-site,
	.mainBackMenu { background:none !important; }

	div#global div#header div#menu { background:url(../../images/back_office/layout/bg-blue-menu.gif) repeat-x; }

	div#global div#globalMainContent{width: 1060px; margin-left: auto; margin-right: auto; }
	div#global div#globalMainContent div.mainContent{ width: 820px; }

	div.mainContent div.content div.subContent table.dataTable td,
	div.mainContent div.content div.subContent table.dataTable th { border:1px solid #FFFFFF !important; }
	div.mainContent div.content div.subContent tr.filledRow,
	div.mainContent div.content div.subContent th.filledCell { background:#EEEEEE !important; }

	.ui-tabs .ui-tabs-nav li {background:#FFFFFF url(../../images/back_office/content/bg-onglet-categorie.png) repeat-x scroll 50% 50%;  }
	.ui-tabs .ui-tabs-nav li.ui-tabs-selected, .ui-tabs .ui-tabs-nav li:hover { background:#4C5F73; }

	.ui-tabs .ui-tabs-nav {margin:20px 0 0 0 !important; border-bottom:1px solid #E6E6E6; }
	div.mainContent div.content div.subContent { border:1px solid #E6E6E6; margin-bottom:10px; }
	div.mainContent div.content div.subContent { margin-top:0; padding:5px; }

	div#global div#globalMainContent{width: 1060px; margin-left: auto; margin-right: auto; }
	div#global div#globalMainContent div.mainContent{ width: 820px; float: right;}
	div#global div#globalMainContent #tools{ float: left; }

	/* outils */

	ul#ToolList{
		width:172px;
		margin:10px auto 0px auto;
		padding:0px;
		list-style-type:none;
		text-align:center;
	}

	ul#ToolList li{
		float:left;
		width:50px;
		margin:2px;
	}

	/* onglets */

	#ProductTabs .ui-tabs-nav{
		width:100%;
	}

	#ProductTabs .ui-tabs-nav li{
		width:auto;
	}

	/* siblings */

	#ProductSiblingsContainer #ProductSiblings{

		margin:15px auto 5px auto;
		list-style-type:none;
		padding:0px;

	}

	#ProductSiblingsContainer #ProductSiblings .placeholder{
		border:1px solid #E7E7E7;
		width:100%;
		margin:4px auto 4px auto;
		background-color:#F3F3F3;
	}

	#ProductSiblingsContainer #ProductSiblings li{
		text-align:left;
		margin:10px auto;
		height:auto;

	}

	#ProductSiblingsContainer #ProductSiblings li img{ border:1px solid #E7E7E7; }

	/* patch tinyMCE vs Akilaé */

	div.mainContent div.content div.subContent table.dataTable td td,
	div.mainContent div.content div.subContent table.dataTable th th{
		border:0 none;
		margin:0;
		padding:0;
	}

</style>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<link rel="stylesheet" href="/js/jquery/ui/fancybox/jquery.fancybox-1.3.1.css" type="text/css" media="screen" />
<script type="text/javascript" src="/js/jquery/ui/fancybox/jquery.fancybox-1.3.1.pack.js"></script>
<script type="text/javascript">
/* <[CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
	/* inititalisation onglets */

	$( document ).ready( function() {

		$('#ProductTabs').tabs( {

			selected: <?php echo isset( $_GET[ "tab" ] ) ? intval( $_GET[ "tab" ] ) : "2"; ?>,
			ajaxOptions: {

				async: true,
				cache: false,
				error: function(XMLHttpRequest, textStatus, errorThrown){ alert( errorThrown ); },
				dataFilter: function( data, type ){ parseTabContent( data, type );  },
				beforeSend: function( XMLHttpRequest ){ $(":input").attr( "disabled", "disabled" ); }
				
			},
			load: function( event, ui ){ 

				/* compatibilité TABS et FANCYBOX : réinitialiser fancybox à chaque chargement d'onglet */
				
				$("a[id^='DataSheetZoom']").fancybox();
				$("a[id^='ImageZoom']").fancybox();
				$("a#ImageThumbFancyBox").fancybox();
				
			}

		});

	});

	/* ------------------------------------------------------------------------------------------------------------ */
	/* mise à jour du contenu des onglets */

	function parseTabContent( data, type ){

		$( "#FormContent" ).html( data );

		initRequiredFields();

        $(":input").attr( "disabled", "" );

		return false;

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function editCategory( idcategory ){

		document.location = "<?php echo $GLOBAL_START_URL ?>/product_management/catalog/cat_category.php?action=edit&idcategory=" + idcategory;

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function explore(){

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/search/explorer.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'explorateur" ); },
		 	success: function( responseText ){

				$( "#globalMainContent" ).append( '<div id="ExplorerDialog"></div>' );
				$("#ExplorerDialog").html( responseText );
        		$("#ExplorerDialog").dialog({

        			title: "Explorateur",
        			width:  800,
        			height: 600,
        			close: function(event, ui) { $("#ExplorerDialog").dialog( 'destroy' ); $("#ExplorerDialog").remove(); }

        		});

			}

		});

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function automator(){

		if( $( "#AutomatorContainer" ).length )
			return;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/search/automator.php?idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&ondestroy=destroyAutomator",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'explorateur" ); },
		 	success: function( responseText ){

				$( "#tools" ).hide();
				$( "#ProductManagementContainer" ).hide();
				$( ".mainContent" ).css( "width", "100%" );
				$( responseText ).insertBefore( $( "#ProductManagementContainer" ) );

			}

		});

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function destroyAutomator(){

		$( "#AutomatorContainer" ).remove();
		$( ".mainContent" ).css( "width", "820px" );
		$( "#ProductManagementContainer" ).show();
		$( "#tools" ).show();

	}

	/* ------------------------------------------------------------------------------------------------------------ */
	/* inititalisation formulaire ajax */

	$(document).ready(function() {

		var options = {

			beforeSubmit:  productFormPreSubmitCallback,  // pre-submit callback
			success:       productFormPostSubmitCallback  // post-submit callback

		};

		$('#myform').ajaxForm( options );

	});

	/* ------------------------------------------------------------------------------------------------------------ */

	function productFormPreSubmitCallback( formData, jqForm, options ){

		if( !checkRequiredFields() )
			return false;

		return checkCategorySelection();

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function productFormPostSubmitCallback( responseText, statusText ){

		if( responseText != "" )
			alert( responseText );

		$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
		$.blockUI.defaults.css.fontSize = '12px';
    	$.growlUI( '', 'Modifications enregistrées' );

		initRequiredFields();

		
	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function checkCategorySelection(){

		if( !document.getElementById( 'myform' ).elements[ 'idcategory' ] )
			return true;

		if( document.getElementById( 'myform' ).elements[ 'idcategory' ].value == 0 ){

			alert( 'Veuillez sélectionner la catégorie dans laquelle vous souhaitez créer un nouveau produit' );
			return false;

		}

		return true;

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	var images = new Array( '<?php echo $GLOBAL_START_URL ?>/images/empty.jpg'<?php

		$query = "SELECT msg_commercial FROM msg_commercial ORDER BY idmsg_commercial ASC";
		$rs = DBUtil::query($query);
		if ($rs === false)
			die("Impossible de récupérer la liste des messages commerciaux");

		$i = 0;
		while (!$rs->EOF()) {

			$src = $GLOBAL_START_URL . "/www/" . $rs->fields("msg_commercial");

			echo ",'$src'";

			$rs->MoveNext();
			$i++;

		}

		echo ");";

	?>

	/* ------------------------------------------------------------------------------------------------------------ */
	//met en évidence les champs obligatoires du formulaire

	function initRequiredFields(){

		resetForm();

		var form = document.getElementById( 'myform' );
		var i = 0;
		while( i < form.elements.length ){

			if( form.elements[ i ].getAttribute( 'name' ) != null && ( isRequired( form.elements[ i ].getAttribute( 'name' ) ) ) && ( !form.elements[ i ].type || form.elements[ i ].type != 'hidden' ) ){

				form.elements[ i ].style.backgroundColor = "<?php echo REQUIRED_BACKGROUND_COLOR ?>";
				form.elements[ i ].style.color = "<?php echo REQUIRED_COLOR ?>";

				var mul = document.createElement( 'span' );

				mul.setAttribute( 'title', 'Required Symbol' );
				mul.style.color = '#FF0000';
				mul.style.fontWeight = 'bold';
				mul.style.fontSize = '14px';
				mul.style.padding = '5px';
				mul.innerHTML = '*';

				form.elements[ i ].parentNode.insertBefore( mul, form.elements[ i ].nextSibling );

			}

			i++;

		}

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function resetForm(){

		var elements = document.getElementById( 'myform' ).elements;
		var i = 0;

		while( i < elements.length ){

			if( !elements[ i ].type || elements[ i ].type != 'button' && elements[ i ].type != 'submit' && elements[ i ].type != 'image' ){

				elements[ i ].style.backgroundColor = "<?php echo DEFAULT_BACKGROUND_COLOR ?>";
				elements[ i ].style.color = "<?php echo DEFAULT_COLOR ?>";

				var nextSibling = elements[ i ].nextSibling;

				if( nextSibling && nextSibling.title && nextSibling.title == 'Required Symbol' )
					elements[ i ].parentNode.removeChild( nextSibling );

			}

			i++;

		}

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	//Return true si un nom de champ donné est obligatoire

	function isRequired( fieldname ){

		//champs obligatoires pour le produit

		var productRequiredFields = new Array(<?php

			//champs obligatoires dans la table product

			$rs = & DBUtil::query("SELECT DISTINCT fieldname FROM desc_field WHERE tablename LIKE 'product' AND mandatory = 1");

			$p = 0;
			while (!$rs->EOF()) {

				if ($p)
					echo ",";

				echo "'" . $rs->fields("fieldname") . "'";

				$p++;
				$rs->MoveNext();

			}

		?>);

		//champs obligatoires pour les références

		var referencesRequiredFields = new Array(<?php

		//champs obligatoires dans la table detail

		$idarticles = array ();

		$rs = & DBUtil::query("SELECT idarticle FROM detail WHERE idproduct = '" . $tableobj->get("idproduct") . "'");
		while (!$rs->EOF()) {

			$idarticles[] = $rs->fields("idarticle");
			$rs->MoveNext();

		}

		$rs = & DBUtil::query("SELECT DISTINCT fieldname FROM desc_field WHERE tablename LIKE 'detail' AND mandatory = 1");

		$p = 0;
		while (!$rs->EOF()) {

			reset($idarticles);

			foreach ($idarticles as $idarticle) {

				if ($p)
					echo ",";

				echo "'" . $rs->fields("fieldname") . "[ $idarticle ]'";

				$p++;

			}

			$rs->MoveNext();

		}

		//forcer la saisie du code douane si le fournisseur est étranger

		$localState = DBUtil::getDBValue("paramvalue", "parameter_cat", "idparameter", "default_idstate");
		$supplierState = DBUtil::getDBValue("idstate", "supplier", "idsupplier", $tableobj->get("idsupplier"));

		if ($supplierState != $localState) {

			reset($idarticles);

			foreach ($idarticles as $idarticle) {

				if ($p)
					echo ",";

				echo "'code_customhouse[ $idarticle ]'";

				$p++;

			}

		}

		?>);

		if( fieldname == null || fieldname.length == 0 )
			return false;

		var i = 0;
		while( i < productRequiredFields.length ){

			if( productRequiredFields[ i ].toLowerCase() == fieldname.toLowerCase() )
				return true;

			i++;

		}

		var i = 0;
		while( i < referencesRequiredFields.length ){

			if( referencesRequiredFields[ i ].toLowerCase() == fieldname.toLowerCase() )
				return true;

			i++;

		}

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	//return true si tous les champs obligatoires ont été renseignés

	function checkRequiredFields(){

		var form = document.getElementById( 'myform' );

		var hasError = false;
		var i = 0;
		while( i < form.elements.length ){

			var element = form.elements[ i ];

			if( element.getAttribute( 'name' ) != null && isRequired( element.getAttribute( 'name' ) ) ){

				if( !isFilled( element ) ){ //le champ obligatoire n'est pas renseigné

					element.style.backgroundColor = "<?php echo ERROR_BACKGROUND_COLOR ?>";
					element.style.color = "<?php echo ERROR_COLOR ?>";

					hasError = true;

				}
				else{ //le champ obligatoire est bien renseigné

					element.style.backgroundColor = "<?php echo REQUIRED_BACKGROUND_COLOR ?>";
					element.style.color = "<?php echo REQUIRED_COLOR ?>";

				}

			}

			i++;

		}

		if( hasError )
			alert( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "missing_required_fields" ) ) ?>' );

		return !hasError;

	}

	/* ------------------------------------------------------------------------------------------------------------ */
	/* import/export */

	function displayExportForm(){

		$.blockUI({

			message: $( '#ImportExportDiv' ),
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
			fadeIn: 700,
	    	fadeOut: 0

		});

		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function duplicateProduct(){

		document.location = "<?php echo $GLOBAL_START_URL ?>/product_management/catalog/cat_product.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&duplicate=product";

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function createCategoryChild( type ){

		var explorerURL = "<?php echo $GLOBAL_START_URL ?>/product_management/search/catalog_tree.php?hide=products&idcategory=<?php echo $tableobj->get( "idcategory" ); ?>&onSelectCategoryCallback=";

		if( type == "product" )
			explorerURL += "createCategoryProduct&selectable=leafs";
		else explorerURL += "createSubcategory";

		$.ajax({

			url: explorerURL,
			async: false,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger la liste des catégories" ); },
		 	success: function( responseText ){

				$( "#globalMainContent" ).append( '<div id="CategoryParentDialog"></div>' );

				$("#CategoryParentDialog").html( responseText );
        		$("#CategoryParentDialog").dialog({

        			title: "Sélection de la catégorie parente",
        			width:  500,
        			height: 500,
        			close: function(event, ui) {

        				$("#CategoryParentDialog").dialog( 'destroy' );
        				$("#CategoryParentDialog").remove();

        			}

        		});

			}

		});

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function createSubcategory( idcategoryparent ){

		$("#CategoryParentDialog").dialog( "close" );
		$("#CategoryParentDialog").remove();

		document.location = "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?create&idcategoryparent=" + idcategoryparent;

	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function createCategoryProduct( idcategoryparent ){

		$("#CategoryParentDialog").dialog( "close" );
		$("#CategoryParentDialog").remove();

		document.location = "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?create&idcategory=" + idcategoryparent;

	}

	/* ------------------------------------------------------------------------------------------------------------ */

/* ]]> */
</script>
<?php

	/* import/export */

	if ($tableobj->get("idproduct")) {

		?>
		<div id="ImportExportDiv" style="display:none; font-size:12px; font-weight:normal;">
		<?php

			if ($tableobj->GetParameterAdmin('display_import_ref')) {

					?>
					<form id="importOneProductCSV" action="cat_product.php?idproduct=<?php echo $tableobj->get( "idproduct" ) ?>" method="post" enctype="multipart/form-data">
						<input type="hidden" name="idproduct" value="<?php echo $tableobj->get( "idproduct" ) ?>" />
						<p style="text-align:left;">
							<b>Importer des références</b>
							<input type="file" name="userfile" class="textInput" maxlength="2097152" accept="text/*" size="6" />
							<input type="submit" class="blueButton" name="importOneProductCSV" value="ok" />
							<br />
							<?php toolTipBox(Dictionnary::translate('help_upload_ref'), 250, $html = "", $showicon = true); ?>
							<a href="ex_import_ref.csv" style="font-size:10px;">Exemple de fichier d'import
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="aide" />
							</a>
						</p>
					</form>
					<?php

			}

			?>
			<p style="text-align:left;">
				<a href="#" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/product_management/catalog/export_references.php?idproduct=<?php echo $tableobj->get( "idproduct" ) ?>'); $.unblockUI(); return false;">
					<b>Exporter les références</b> <span style="font-size:10px;">( cliquez ici )</span>
				</a>
			</p>
			<div style="bottom:0px; text-align:right;">
				<input type="button" class="blueButton" name="" value="Annuler" onclick="$.unblockUI(); return false;" />
			</div>
		</div><!-- ImportExportDiv -->
		<?php

	}

?>
<div id="globalMainContent">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/all_product_comments.htm.php</span>
<?php } ?>

	<div class="mainContent borderProduct">

	    <div id="ProductManagementContainer" class="content">
		<?php

			if($tableobj->get( "idproduct" ) ){

		    	?>
	    		<p style="font-size:10px; margin:5px 5px 5px 0px;">
	        	<?php

	            	$treePath = getTreePath( $tableobj );

					$i = 0;
					while( $i < count( $treePath ) ){

						$treeNode = $treePath[ $i ];

						if( $i )
							echo " > ";

						echo "<a href=\"{$treeNode->href}\">{$treeNode->name}</a>";

						$i++;

					}

				?>
				</p>
				<?php

			}

			?>
	        <div class="headTitle">
	        	<?php getTitle(); ?>
	        </div>
	        <div style="margin:0px; clear:both;">
				<?php displayProductTabs(); ?>
			</div>
			<?php

				//résultats de l'import

				if( isset ($ImportProductCSV) && isset ($resImport) && !empty ($resImport)) {

					?>
					<div id="ImportResultDiv" class="subContent">
						<div class="subTitleContainer">
							<span style="float:right;">
								<a class="blueLink" href="#" onclick="$( '#ImportResultDiv' ).remove(); return false;">Masquer</a>
							</span>
							<p class="subTitle">Résultat de l'import</p>
						</div>
						<?php

						$ImportProductCSV->showResult($resImport);
						echo "<br />";
						$ImportProductCSV->showErrors();

					?>
					</div>
					<?php

				}

			?>
			<div class="subContent">
				<form enctype="multipart/form-data" action="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo $tableobj->get( "idproduct" ) ?>" method="post" id="myform" onsubmit="if( tinyMCE != null ) tinyMCE.triggerSave(true,true);">
					<input type="hidden" name="update" value="1" />
					<div id="ProductContainer">
					<?php

						if( $tableobj->get( "idproduct" ) ){

							$adms->InputField('hidden', 'EncodedArrayK', base64_encode(serialize($k))); // identifiant d'enregistrement a modifier
							$adms->InputField('hidden', 'id_index0_name', 'idproduct');
							$adms->InputField('hidden', 'id_index0_value', $tableobj->get('idproduct'));
							$adms->InputField('hidden', 'idproduct', $tableobj->get('idproduct'));
							$adms->InputField('hidden', 'Int_StartP', $Int_StartP);

							?>
							<p style="text-align:right; margin-top:20px;" class="controls">
							<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
								if( $akilae_crm == 0 ){ ?>
									<input type="button" name="" value="Supprimer" class="orangeButton" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce produit?' ) ) document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/index.php?delete&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>';" />
									<input type="button" name="" value="Dupliquer" class="orangeButton" onclick="duplicateProduct();" />
										<?php } ?>
							
								<input type="submit" name="update" value="Sauvegarder" class="blueButton" />
								<a href="#" title="Rafraîchir le contenu" onclick="$( '#ProductTabs' ).tabs( 'load', $( '#ProductTabs' ).tabs( 'option', 'selected' ) );">
									<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/refresh.png" alt="" />
								</a>
							</p>
							<?php

						}
						else{

							?>
							
							<p style="text-align:right; margin-top:20px;" class="controls">
								<input type="submit" name="append" value="Sauvegarder" onclick="return checkCategorySelection();" class="blueButton" />
							</p>
							<?php

						}

						?>
						<div id="FormContent"></div>
						<div id="FormButtons" style="display:block;">
						<?php

							if( $tableobj->get( "idproduct" ) ){

								?>
								<p style="float:right;" class="controls">
								<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
										if( $akilae_crm == 0 ){ ?>
									<input type="button" name="" value="Supprimer" class="orangeButton" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce produit?' ) ) document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/index.php?delete&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>';" />
									<input type="button" name="" value="Dupliquer" class="orangeButton" onclick="duplicateProduct();" />
										<?php }  ?>
														
									<input type="submit" name="update" value="Sauvegarder" class="blueButton" />
								</p>
								<?php

							}
							else{

								?>
								<p style="text-align:right; margin-top:10px;" class="controls">
									<input type="submit" name="append" value="Sauvegarder" onclick="return checkCategorySelection();" class="blueButton" />
								</p>
								<?php

							}

						?>
					</div><!-- FormButtons -->
					<div class="clear"></div>
				</div><!-- ProductContainer -->
				</form>
			</div><!-- subContent -->
		</div><!-- content -->

	</div><!-- mainContent -->
	<div id="tools" class="leftTools">

		<div class="leftMenu">
			<h2 class="title">Vos outils</h2>
			<ul id="leftMenuList">
    			<li>
    				<a href="#" onclick="explore(); return false;">
    					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_search.png" alt="" />
    					Explorateur
    				</a>
    			</li>
    		
    			<li>
    				<a href="#" onclick="createCategoryChild( 'category' ); return false;">
    					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create.png" alt="" />
    					Créer catégorie
    				</a>
    			</li>
				<?php
    			$akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
   					if( $akilae_crm == 0 ){ ?>
				<li>
    				<a href="#" onclick="createCategoryChild( 'product' );">
    					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create2.png" alt="" />
    					Créer produit
    				</a>
    			</li>
	              <?php } ?>
				  
				 <?php if( $iduser == 33 ){ ?>
				  <li>
    				<a href="#" onclick="createCategoryChild( 'product' );">
    					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create2.png" alt="" />
    					Créer produit
    				</a>
    			</li>
				<?php } ?>
				
    			<?php

    				if( $tableobj->get( "idproduct" ) ){

    					?>
	 	       			<li>
	 	       				<a href="#" onclick="$( '#ProductTabs' ).tabs( 'select', 1 ); return false;">
	 	       					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_siblings.png" alt="" />
	 	       					Voir le contenu
	 	       				</a>
	 	       			</li>
	 	       			<li>
	 	       				<a href="<?php echo URLFactory::getProductURL( $tableobj->get( "idproduct" ) ); ?>" onclick="window.open( this.href ); return false;">
	 	       					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_preview.png" alt="" />
	 	       					Aperçu
	 	       				</a>
	 	       			</li>
	 	       			<li>
	 	       				<a href="#" onclick="displayExportForm(); return false;"; return false;">
	 	       					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button-import-export.png" alt="" />
	 	       					Import / Export
	 	       				</a>
	 	       			</li>
	 	       			<li>
	 	       				<a href="#" onclick="window.open('<?php echo URLFactory::getPDFURL( $tableobj->get( "idproduct" ) ); ?>'); return false;"; return false;">
	 	       					<img src="/images/back_office/content/button_pdf.png" alt="" />
	 	       					Fiche PDF
	 	       				</a>
	 	       			</li>
	 	       			<?php

    				}

    			?>

       		</ul>
 	       	<div class="clear"></div>
	    </div><!-- toolbox -->
	    <div class="leftMenu">
			<h2 class="title">Produits de la même catégorie</h2>
        	<div class="content" style="padding:5px;">
		    <?php

		    	if( $tableobj->get( "idproduct" ) )
					listSiblings( $tableobj->get( "idproduct" ) );

		    ?>
		    <div class="clear"></div>
	        </div>
	    </div><!-- toolbox -->
    </div><!-- #tools -->
</div><!-- globalMainContent -->
<?php

include_once ("$GLOBAL_START_PATH/templates/back_office/foot.php");

//-----------------------------------------------------------------------------------------------------

function getCategoryHeadingList($idcategory, & $headings) {

	global $tableobj, $langue;

	$query = "
		SELECT DISTINCT( it.intitule_title_$langue ) AS intitule_title
		FROM `intitule_title` it, intitule_link il
		WHERE idcategory = '$idcategory'
		AND il.idintitule_title = it.idintitule_title
		ORDER BY intitule_title ASC";

	$rs = DBUtil::Query($query);

	if ($rs === false)
		die("Impossible de récupérer la liste des intitulés pour la catégorie '$idcategory'");

	while (!$rs->EOF()) {

		$heading = $rs->fields("intitule_title");

		if (!in_array($heading, $headings))
			$headings[] = $rs->fields("intitule_title");

		$rs->MoveNext();

	}

	$query = "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategory' LIMIT 1";

	$rs = DBUtil::Query($query);

	if ($rs === false)
		die("Impossible de récupérer la liste des intitulés pour la catégorie '$idcategory'");

	if (!$rs->RecordCount())
		return $headings;

	$idparent = $rs->fields("idcategoryparent");

	getCategoryHeadingList($idparent, $headings);

}

//-----------------------------------------------------------------------------------------------------

function getTreePath( &$tableobj ){

	global $GLOBAL_START_URL;

	$lang = User::getInstance()->getLang();

	$treePath[] = ( object )array(

		"name" 			=> $tableobj->get( "name$lang" ),
		"href" 			=> "$GLOBAL_START_URL/product_management/catalog/cat_product.php?idproduct=" . $tableobj->get( "idproduct" )

	);

	$rs =& DBUtil::query( "SELECT name$lang AS name FROM category WHERE idcategory = '" . $tableobj->get( "idcategory" ) . "' LIMIT 1" );

	$treePath[] = ( object )array(

		"name" 			=> $rs->fields( "name" ),
		"href" 			=> "$GLOBAL_START_URL/product_management/catalog/cat_category.php?action=edit&idcategory=" . $tableobj->get( "idcategory" )

	);

	$idchild = $tableobj->get( "idcategory" );

	$done = false;

	while( !$done ){

		$query = "
		SELECT c.idcategory, c.name$lang AS name
		FROM category_link cl, category c
		WHERE cl.idcategorychild = '$idchild'
		AND cl.idcategoryparent = c.idcategory
		LIMIT 1";

		$rs =& DBUtil::query( $query );

		$treePath[] = ( object )array(

			"name" 			=> $rs->fields( "name" ),
			"href" 			=> "$GLOBAL_START_URL/product_management/catalog/cat_category.php?action=edit&idcategory=" . $rs->fields( "idcategory" )
		);

		$idchild = $rs->fields( "idcategory" );
		$done = $idchild == 0;

	}

	return array_reverse( $treePath );

}

//---------------------------------------------------------------------------------------------

//liste des intitulés disponibles
/*

	function displayProductHelpPopup( idcategory ){

		var postop = (self.screen.height-600)/2;
		var posleft = (self.screen.width-650)/2;

		var options ='directories=no, location=no, menubar=no, resizable=yes, left=' + posleft + ', top=' + postop + ', scrollbars=yes, status=no, toolbar=no, width=650, height=650';
		var location = '<?php echo $GLOBAL_START_URL ?>/product_management/catalog/product_help.php?idcategory=' + idcategory;

		window.open( location, '_blank', options ) ;

	}

$idcategory = $tableobj->get("idcategory");
?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle"><?php echo Dictionnary::translate( "product_help" ) ?></p>
	</div>
	<input type="button" class="blueButton" value="Aide intitulés" onclick="displayProductHelpPopup( <?php echo $idcategory ?> ); return false;" style="float:right; margin-right:10px; margin-top:7px;" />

<?php
*/

//-----------------------------------------------------------------------------------------------------

function getTitle(){

	global $tableobj;

	if( $tableobj->get( "idproduct" ) ){

    	?>
    	<h1 class="maintitle">
    		<a href="http://www.google.fr/search?q=<?php echo urlencode( $tableobj->get( "name" . User::getInstance()->getLang() ) ); ?>" onclick="window.open(this.href); return false;">
    		 <img style="float:right; margin:6px 0 0 0;" src="http://images.google.com/images/isr_g.png" alt="" width="60" />
    		</a>
	        <?php echo htmlentities( $tableobj->get( "name_1" ) ); ?>
	        (<span class="rightContainer" style="color:#FF0000; font-size:14px;">
			<?php

				if( $tableobj->get( "idproduct" ) )
				 	echo "Produit n° " . $tableobj->get( "idproduct" );

			 ?>
			 </span>)
		</h1><!-- rightContainer -->
		<?php

    }
    else echo "<h1 class=\"maintitle\">Créer un nouveau produit</h1>";

}

//-----------------------------------------------------------------------------------------------------

function displayProductTabs(){

	global 	$tableobj,
			$GLOBAL_START_URL;

	$query = "SELECT c.use_select_intitule FROM category c, product p WHERE p.idproduct = '" . $tableobj->get( "idproduct" ) . "' AND p.idcategory = c.idcategory LIMIT 1";

	$rs =& DBUtil::getConnection()->Execute( $query );

	if( $rs === false )
		die( "Impossible de récupérer le mode de gestion des intitulés pour le produit '$idproduct'" );

	$use_select_intitule = $rs->fields( "use_select_intitule" );

	$heading_template = $use_select_intitule ? "product_private_headings.htm.php" : "product_headings.htm.php";

	/* Si l'ordre des onglets doit être modifié, pensez à modifier également toutes les occurences de $( '#ProductTabs' ).tabs( 'select', tabIndex ); */

	?>
	<style type="text/css">
		.li_stats { background:#EEEEEE !important; }
		.li_stats:hover { background:#DDDDDD !important; }

		/* Menu */
		.menu { width:100% !important; height:22px !important; margin:10px 0 0 6px !important; float:none !important; background:none !important; border:none !important; }
		.menu li { float:left; list-style:none; margin:0 1px 0 0 !important; height:auto !important; background:none !important; padding:0 !important; }
		.menu li a { font-size:11px; text-align:center; color:#FFFFFF !important; display:block; padding:0 0 0 3px !important; height:22px;
					 font-weight:bold; text-decoration:none; cursor:pointer;
					 background:url(../../images/back_office/layout/left-onglet-back2.jpg) no-repeat left bottom !important; }
		.menu li a span { background:url(../../images/back_office/layout/right_back2.jpg) no-repeat right bottom;
						  padding:2px 11px 0 8px; height:20px; display:block; }
		.menu li a:hover, .menu li.ui-tabs-selected a { color:#505050 !important; background:url(../../images/back_office/layout/left-onglet-back2.jpg) no-repeat left top !important; }
		.menu li a:hover span, .menu li.ui-tabs-selected a span { background:url(../../images/back_office/layout/right_back2.jpg) no-repeat right top; }


	</style>
	<div id="ProductTabs">
		<ul class="menu">
			
			<li style="display:none;">
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_siblings.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>PRODUITS DANS LA MEME CATEGORIE</span></a>
			</li>
			<li style="display:none;">
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/<?php echo $heading_template; ?>?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>INTITULES</span></a>
			</li>
			<li>
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>GENERAL</span></a>
			</li>
			<li>
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_references.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>REFERENCES</span></a>
			</li>
			<li>
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_images.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>PHOTOS & DOCS</span></a>
			</li>
			<li>
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_texts.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>TEXTES</span></a>
			</li>
			<li>
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_associates.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>OPTIONS</span></a>
			</li>
			<li style="display:none;">
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_caracteristics.htm.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>AUTRES</span></a>
			</li>
			<li>
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_view.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>VISU</span></a>
			</li>
			<li style="display:block;">
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_comments.php?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>"><span>COMMENTAIRES</span></a>
			</li>
			<li class="li_stats">
				<a href="<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_stats.php?idproduct=<?php
				
					echo $tableobj->get( "idproduct" ); 
					
					if( isset( $_REQUEST[ "start" ] ) )
						echo "&amp;start=" . $_REQUEST[ "start" ];
					
					if( isset( $_REQUEST[ "end" ] ) )
						echo "&amp;end=" . $_REQUEST[ "end" ];
						
				?>">
					<span>
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pie.png" alt="" />
					</span>
				</a>
			</li>
		</ul>
	</div>
	<?php

}

//-----------------------------------------------------------------------------------------------------

function listSiblings( $idproduct ){

	global $GLOBAL_START_PATH, $GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/productobject.php" );

	$query = "
	SELECT p1.idproduct,
		p1.name" . User::getInstance()->getLang() . " AS name,
		p1.available,
		s.name AS supplierName
	FROM product p1, product p2, supplier s
	WHERE p2.idproduct = '$idproduct'
	AND p2.idcategory = p1.idcategory
	AND s.idsupplier = p1.idsupplier
	ORDER BY p1.display_product_order ASC";

	$rs =& DBUtil::query( $query );

	?>
	<script type="text/javascript">
	/* <![CDATA[ */

		/* ------------------------------------------------------------------------------------------ */

		/* tri des sous-catégories et produits */

		function sortProductSiblings( serializedString ){

			var data = "sort=1&" + serializedString;

			$.ajax({

				url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/cat_product.htm.php",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
			 	success: function( responseText ){

					if( responseText == "1" ){

						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );

	        		}
	        		else alert( "Impossible d'enregistrer les modifications" + responseText );

				}

			});

		}

		/* ------------------------------------------------------------------------------------------ */

		$(document).ready(function(){

		    /* sortable */

			$("#ProductSiblings").sortable({

				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function(event, ui){ sortProductSiblings( $("#ProductSiblings").sortable('serialize') ); }

			});

			$("#ProductSiblings").disableSelection();


		});

	/* ]]> */
	</script>
	<div id="ProductSiblingsContainer">
		<ul id="ProductSiblings">
		<?php

			while( !$rs->EOF() ){

				$html = "<b>" . htmlentities( $rs->fields( "name" ) ) . "</b>";
				$html .= "
				<br />
				Produit n° " . $rs->fields( "idproduct" ) . "
				<br />" . htmlentities( $rs->fields( "supplierName" ) );

				$referenceCount = DBUtil::query( "SELECT COUNT(*) AS `count` FROM detail WHERE idproduct = '" . $rs->fields( "idproduct" ) . "'" )->fields( "count" );
				$html .= $referenceCount ? "<br />$referenceCount référence(s)" : "<br />aucune référence";

				?>
				<li id="Siblings_<?php echo $rs->fields( "idproduct" ); ?>">
					<div style="position:relative;<?php if( !$rs->fields( "available" ) ) echo " font-style:italic;"; ?>">
						<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>" title="<?php echo htmlentities( $rs->fields( "name" ) ); ?>" style="cursor:move; text-decoration:none;">
							<img class="itemThumb" src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, 40, 40 ); ?>" alt="" style="position:absolute; left:0px;<?php if( !$rs->fields( "available" ) ) echo " opacity:0.4; filter:alpha(opacity=40);"; ?>" />
							<div style="position:relative; margin-left:45px;"><?php echo $html; ?></div>
						</a>
					</div>
				</li>
				<?php

				$rs->MoveNext();

			}

		?>
		</ul>
	</div>
	<?php

}

//-----------------------------------------------------------------------------------------------------

?>