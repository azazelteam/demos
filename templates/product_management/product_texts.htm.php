<?php

//---------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );


$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );
$langue=User::getInstance()->getLang();

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function updateMsgCommercial( idmsg ){
	
		var list = document.getElementById( 'myform' ).elements[ 'space_msg_' + idmsg ];
		var selected = list.options[ list.selectedIndex ].value;
		
		var elementID = 'msg_commercial_' + idmsg;
		document.getElementById( elementID ).src = images[ selected ];
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<?php 



//========================================
// Descriptif technique
//========================================

	?>
	
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_texts.htm.php</span>
<?php } ?>
	
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">Description technique</p>
	</div>
	<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">	  
    <?php
    	
    	$wysiwyg = new WYSIWYGEditor( "desc_tech$langue" );
		$wysiwyg->setHTML( $tableobj->get( "desc_tech$langue" ) );
		$wysiwyg->setDimension( 0, 120 );
		$wysiwyg->display();
    	
    ?>
	</div>
	<?php 
	

	
//========================================
// Descriptif complémentaire
//========================================


	?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">Description complémentaire</p>
	</div>
	<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
	<?php
    	
    	$wysiwyg = new WYSIWYGEditor( "desc_more$langue" );
		$wysiwyg->setHTML( $tableobj->get( "desc_more$langue" ) );
		$wysiwyg->setDimension( 0, 120 );
		$wysiwyg->display();
		  
	?>  	
	</div>
	<?php 


//========================================
// Mots clefs pour le moteur de recherche
//========================================


	?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">Mots clefs pour le moteur de recherche</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<tr>
				<th class="filledCell" style="width:200px;"><?php if(isset($art["key_word"][1])) echo $art["key_word"][1]; else echo " "; ?><?php echo htmlentities( Dictionnary::translate("key_word_1") ) ?></th>
				<td>
					<input type="text" name="key_word_1" value="<?php echo $tableobj->get("key_word_1") ?>" class="textInput" />
				</td>
			</tr>
		</table>
	</div>
	<?php 
		
//========================================
// Titre page + mots clefs référencement
//========================================

	?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">Référencement</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">	
			<tr>
				<th class="filledCell" style="width:200px;"><?php if(isset($art["title"][1])) echo $art["title"][1]; else echo " "; ?>Méta : titre</th>
				<td>
					<input type="text" name="title_1" value="<?php echo $tableobj->get("title_1") ?>" class="textInput" />
				</td>
			</tr>
			<tr>
				<th class="filledCell">
				<?php 
				
					if(isset($art["title_keyword"][1]))  
						echo  $art["title_keyword"][1]; 
					else echo " "; 
					
					echo htmlentities( Dictionnary::translate("title_keyword_1") ) ; 
						
				 ?>
				
				</th>
				<td>
					<input type="text" name="title_keyword_1" value="<?php echo $tableobj->get("title_keyword_1") ?>" class="textInput" />
				</td>
			</tr>
			<tr>
				<th class="filledCell">
				<?php /* 
				
					if(isset($art["meta_description"][1]))  
						echo  $art["meta_description"][1]; 
					else echo " ";
					
					echo  Dictionnary::translate("Méta description",true)  ; 
						
				*/ ?>
				Méta : description
				</th>
				<td>
					<input type="text" name="meta_description" value="<?php echo $tableobj->get("meta_description") ?>" class="textInput" />
				</td>
			</tr>
            <tr>
				<th class="filledCell">
				<?php
					
					echo  Dictionnary::translate("Catégorie de produit google",true)  ; 
						
				?>
				</th>
			
			</tr>
			<?php
			/*$productobject = new PRODUCT(intval($_POST['idproduct']));
			if (!$productobject->hasPromo())
			{*/
			?>
            <tr>
				<th class="filledCell">
				<?php
					
					echo htmlentities( Dictionnary::translate("ID promotion Google") ) ; 
						
				?>
				</th>
				<td>
					<input type="text" name="google_promotion_id" value="<?php echo $tableobj->get("google_promotion_id") ?>" class="textInput" />
				</td>
			</tr>
			<?php //} ?>
		</table>
	</div>
	<?php 
	

//========================================
// Documentation
//========================================

	?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">Documentation
		<?php toolTipBox( Dictionnary::translate('help_documentation'), 250, $html = "", $showicon = true ) ?></p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<tr>
				<th class="filledCell" style="width:200px;"><?php echo htmlentities( Dictionnary::translate("doc_1") ) ?></th>
				<td><input type="text" name="doc_1" value="<?php echo $tableobj->get("doc_1") ?>" class="textInput" /></td>
			</tr>
		</table>
	</div>
	<?php 

	
//========================================
// Titre des tableaux références
//========================================
/*
if (DBUtil::getParameterAdmin('display_table')) { 

	?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">Titres des tableaux références</p>
	</div>	
	<div class="tableContainer">
		<table class="dataTable">
			<tr>
				<th class="filledCell" style="width:200px;"><?php echo htmlentities( Dictionnary::translate('table_name_1') ) ; ?></th>
				<td><input type="table_name_1" name="table_name_1" id="table_name_1" value="<?php echo $tableobj->get('table_name_1'); ?>" class="textInput" /></td>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('use_color_1') ) ; ?></th>
				<td>
					<input type="radio" name="use_color_1" value="1"<?php if( $tableobj->get( "use_color_1" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="use_color_1" value="0"<?php if( !$tableobj->get( "use_color_1" ) ) echo " checked=\"checked\""; ?> /> non
				</td>			
			</tr>
			<tr>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('table_name_2') ) ; ?></th>
				<td><input type="table_name_2" name="table_name_2" id="table_name_2" value="<?php echo $tableobj->get('table_name_2'); ?>" class="textInput" /></td>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('use_color_2') ) ; ?></th>
				<td>
					<input type="radio" name="use_color_2" value="1"<?php if( $tableobj->get( "use_color_2" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="use_color_2" value="0"<?php if( !$tableobj->get( "use_color_2" ) ) echo " checked=\"checked\""; ?> /> non
				</td>	
			</tr>
			<tr>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('table_name_3') ) ; ?></th>
				<td><input type="table_name_3" name="table_name_3" id="table_name_3" value="<?php echo $tableobj->get('table_name_3'); ?>" class="textInput" /></td>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('use_color_3') ) ; ?></th>
				<td>
					<input type="radio" name="use_color_3" value="1"<?php if( $tableobj->get( "use_color_3" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="use_color_3" value="0"<?php if( !$tableobj->get( "use_color_3" ) ) echo " checked=\"checked\""; ?> /> non
				</td>
			</tr>
			<tr>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('table_name_4') ) ; ?></th>
				<td><input type="table_name_4" name="table_name_4" id="table_name_4" value="<?php echo $tableobj->get('table_name_4'); ?>" class="textInput" /></td>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('use_color_4') ) ; ?></th>
				<td>
					<input type="radio" name="use_color_4" value="1"<?php if( $tableobj->get( "use_color_4" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="use_color_4" value="0"<?php if( !$tableobj->get( "use_color_4" ) ) echo " checked=\"checked\""; ?> /> non
				</td>
			</tr>
			<tr>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('table_name_5') ) ; ?></th>
				<td><input type="table_name_5" name="table_name_5" id="table_name_5" value="<?php echo $tableobj->get('table_name_5'); ?>" class="textInput" /></td>
				<th class="filledCell"><?php echo htmlentities( Dictionnary::translate('use_color_5') ) ; ?></th>
				<td>
					<input type="radio" name="use_color_5" value="1"<?php if( $tableobj->get( "use_color_5" ) ) echo " checked=\"checked\""; ?> /> oui
					<input type="radio" name="use_color_5" value="0"<?php if( !$tableobj->get( "use_color_5" ) ) echo " checked=\"checked\""; ?> /> non
				</td>
			</tr>			
		</table>
	</div>
	<?php 

}
*/
//--------------------------------------------------------------------------------------------
	
function displayCommercialMsgList( $idmsg, $selected ){
	
	global 	$base,
			$langue;
	
	$query = "SELECT idmsg_commercial, msg_commercial, msg_commercial$langue FROM msg_commercial";
	$rs = DBUtil::getConnection()->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des messages commerciaux" );
		
	echo "
	<select id=\"space_msg_$idmsg\" name=\"space_msg_$idmsg\" onchange=\"updateMsgCommercial( $idmsg );\">
		<option value=\"0\">-</option>";
		
	while( !$rs->EOF() ){
		
		$idmsg_commercial = $rs->fields( "idmsg_commercial" );
		$src = $rs->fields( "msg_commercial" );
		$msg_commercial = $rs->fields( "msg_commercial$langue" );
		
		if( $selected == $idmsg_commercial )
			$status = " selected=\"selected\"";
		else $status = "";
		
		echo "
		<option value=\"$idmsg_commercial\"$status>$msg_commercial</option>";
		
		$rs->MoveNext();
		
	}
	
	echo "</select>";
	
}
	
//--------------------------------------------------------------------------------------------

function getMsgCommercial( $idmsg ){
	
	global 	$base,
			$GLOBAL_START_URL;
	
	if( !$idmsg )
		return "$GLOBAL_START_URL/images/back_office/content/disabled.png";
		
	$query = "SELECT msg_commercial FROM msg_commercial WHERE idmsg_commercial = $idmsg";
	$rs = DBUtil::getConnection()->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le message commercial" );
		
	if( !$rs->RecordCount() )
		return "$GLOBAL_START_URL/images/back_office/content/disabled.png";
		
	return $GLOBAL_START_URL . "/www/" . $rs->fields( "msg_commercial" );

}
	
//--------------------------------------------------------------------------------------------

?>