<?php

//---------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

$lang = User::getInstance()->getLang();

//---------------------------------------------------------------------------------------------
/*  transfert d'un intitulé vers une autre famille - ajax */

if( isset( $_GET[ "transfer" ] ) ){
	
	header( "Content-Type: text/html; charset=utf-8" );
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT family_display 
	FROM intitule_link 
	WHERE idfamily = '" . intval( $_GET[ "idfamily" ] ) . "'
	AND idproduct = '" . intval( $_GET[ "idproduct" ] ) . "'
	LIMIT 1";

	$family_display = DBUtil::query( $query )->fields( "family_display" );

	$query = "
	UPDATE intitule_link
	SET idfamily = '" . intval( $_GET[ "idfamily" ] ) . "',
	family_display = '$family_display'
	WHERE idproduct = '" . intval( $_GET[ "idproduct" ] ) . "'
	AND idintitule_title = '" . intval( $_GET[ "idintitule_title" ] ) . "'";

	$rs =& DBUtil::query( $query );
	
	exit( $rs === false ? "0" : "1" );
	
}

//---------------------------------------------------------------------------------------------
/*  afficher / masquer un intitulé - ajax */

if( isset( $_GET[ "hide" ] ) ){
	
	header( "Content-Type: text/html; charset=utf-8" );
	$lang = User::getInstance()->getLang();
				
	$query = "
	UPDATE intitule_link
	SET available = '" . intval( $_GET[ "available" ] ) . "'
	WHERE idproduct = '" . intval( $_GET[ "idproduct" ] ) . "'
	AND idintitule_title = '" . intval( $_GET[ "idintitule_title" ] ) . "'";
	
	$rs =& DBUtil::query( $query );
	
	exit( $rs === false ? "0" : "1" );
	
}

//---------------------------------------------------------------------------------------------
/*  récupération des intitulés d'une famille - ajax */

if( isset( $_GET[ "list" ] ) && isset( $_GET[ "idintitule_family" ] ) ){
	
	header( "Content-Type: text/html; charset=utf-8" );
	$lang = User::getInstance()->getLang();
				
	$query = "
	SELECT it.intitule_title$lang 
	FROM intitule_title it, intitule_title_family itf
	WHERE itf.idintitule_family = '" . intval( $_GET[ "idintitule_family" ] ) . "'
	AND itf.idintitule_title = it.idintitule_title
	ORDER BY it.intitule_title$lang ASC";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		?>
		<option value="<?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?>"><?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?></option>
		<?php
		
		$rs->MoveNext();
		
	}
	
	exit();
	
}

//---------------------------------------------------------------------------------------------
/* suppression intitulé - ajax */

if( isset( $_REQUEST[ "delete" ] ) ){

	/* récupérer les valeurs d'intitulé utilisées */
	
	$query = "
	SELECT DISTINCT( idintitule_value )
	FROM intitule_link
	WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'
	AND idintitule_title = '" . intval( $_REQUEST[ "idintitule_title" ] ) . "'
	AND idintitule_value <> 0";
	
	$rs =& DBUtil::query( $query );
	
	$idintitule_values = array();
	while( !$rs->EOF() ){
		
		$idintitule_values[] = $rs->fields( "idintitule_value" );
		$rs->MoveNext();
		
	}

	/* suppression de liens produit <--> intitulé */
	
	$query = "
	DELETE FROM intitule_link
	WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'
	AND idintitule_title = '" . intval( $_REQUEST[ "idintitule_title" ] ) . "'";
	
	DBUtil::query( $query );
	
	/* suppression des valeurs qui ne sont plus utilisées */
	
	foreach( $idintitule_values as $idintitule_value ){
	
		if( !DBUtil::query( "SELECT idintitule_value FROM intitule_link WHERE idintitule_value = '$idintitule_value' LIMIT 1" )->RecordCount() )
			DBUtil::query( "DELETE FROM intitule_value WHERE idintitule_value = '$idintitule_value' LIMIT 1" );
			
	}
	
	exit( "1" );
	
}

//---------------------------------------------------------------------------------------------
/* tri des familles d'intitulés - ajax */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "families" && isset( $_REQUEST[ "idintitule_family" ] ) ){
	
	$family_display = 1;

	foreach( $_REQUEST[ "idintitule_family" ] as $idintitule_family ){
		
		$query = "
		UPDATE intitule_link 
		SET family_display = '$family_display'
		WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'
		AND idfamily = '$idintitule_family'";

		DBUtil::query( $query );

		$family_display++;

	}
	
	exit( "1" );

}

//---------------------------------------------------------------------------------------------
/* tri des intitulés - ajax */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "headings" && isset( $_REQUEST[ "idintitule_title" ] ) ){
	
	$title_display = 1;

	foreach( $_REQUEST[ "idintitule_title" ] as $idintitule_title ){
		
		$query = "
		UPDATE intitule_link 
		SET title_display = '$title_display'
		WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'
		AND idintitule_title = '$idintitule_title'";

		DBUtil::query( $query );

		$title_display++;

	}
	
	exit( "1" );

}

//---------------------------------------------------------------------------------------------
/* cote - ajax */

if( isset( $_REQUEST[ "cotation" ] ) && isset( $_REQUEST[ "idintitule_title" ] ) ){

	$query = "
	UPDATE intitule_link 
	SET cotation = " . DBUtil::quote( stripslashes( $_REQUEST[ "cotation" ] ) ) . " 
	WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' 
	AND idintitule_title = '" . intval( $_REQUEST[ "idintitule_title" ] ) . "'";

	DBUtil::query( $query );

	exit( "1" );

}

//---------------------------------------------------------------------------------------------
/* cr&ation nouvel intitulé - ajax */

if( isset( $_REQUEST[ "create" ] ) ){

	//header( "Content-Type: text/html; charset=utf-8" );
$lang2 = User::getInstance()->getLang2();
if($lang2!="_1" )
{


	if( createProductHeading( stripslashes( utf8_decode ($_REQUEST[ "heading" ]) ),stripslashes( utf8_decode($_REQUEST[ "heading2" ]) ), intval( $_REQUEST[ "idintitule_family" ] ), intval( $_REQUEST[ "idproduct" ] ) ) )
			exit( "1" );
	else 	exit( "0" );
	}
	else{

		if( createProductHeading2( stripslashes( utf8_encode($_REQUEST[ "heading" ]) ), intval( $_REQUEST[ "idintitule_family" ] ), intval( $_REQUEST[ "idproduct" ] ) ) )
			exit( "1" );
	else 	exit( "0" );
	}
}

//---------------------------------------------------------------------------------------------

$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );

?>
<style type="text/css">

	#as_NewHeading ul{ width:400px;}
	#as_NewHeading .as_header,
	#as_NewHeading .as_footer{ width:388px;}
	
	#HeadingFamilyList{
		
		padding:0px;
		
	}
	
	#HeadingFamilyList, .headingFamily{
		
		list-style-type:none;
		margin:10px;
		
	}
	
	.headingFamily .heading{
	
		width:70%;
		height:15px;
		padding:10px;
		border:1px solid #E7E7E7;
		margin:1px;
		cursor:move;
		background-color:#FFFFFF;
		
	}
	.headingFamily .heading .recycle{
		
		position:absolute;
		margin-left:-40px;
			
	}
	
	.headingFamily .placeholder{
	
		width:70%;
		height:15px;
		padding:10px;
		margin:1px;
		background-color:#F3F3F3;
		
	}
	
	.headingFamily .heading .cotation{ float:right; }
	.headingFamily .heading .available{ float:right; margin-left:10px; cursor:pointer; }
	
</style>
<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------- */
	
	$(document).ready(function(){
			
	    /* sortable */

		$(".headingFamily").sortable({
		
			connectWith: '.headingFamily',
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			revert: true,
			remove: function( event, ui ){

				if( !( $( this ).find( "li" ).length ) )
					$( "#idintitule_family_" + $( this ).attr( "id" ).substr( 14 ) ).remove();
				else sortHeadings( $( this ).attr( "id" ).substr( 14 ), $( this ).sortable('serialize') );
		
			},
			receive: function( event, ui ){
		
				var idfamily = ui.item.parent().attr( "id" ).substr( 14 );
				var idintitule_title = ui.item.attr( "id" ).substr( ui.item.attr( "id" ).lastIndexOf( "_" ) + 1 );

				var data = "idfamily=" + idfamily + "&idintitule_title=" + idintitule_title + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		
				$.ajax({
		
					url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?transfer",
					async: true,
					cache: false,
					type: "GET",
					data: data,
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
				 	success: function( responseText ){
		
						if( responseText != "1" )
							alert(responseText );
						else sortHeadings( idfamily, $( "#" + idfamily ).sortable('serialize') );
						
					}
		
				});
		
			},
			update: function( event, ui ){ 
			
				if( $( this ).find( "li" ).length )
					sortHeadings( $( this ).attr( "id" ).substr( 14 ), $( this ).sortable('serialize') ); 
				else $( this ).remove();
				
			}
		
		}).disableSelection();
	 
	 	/* sortable */
	 	
	 	$("#HeadingFamilyList").sortable({
		
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			revert: true,
			update: function(event, ui){ sortFamilies( $( this ).sortable('serialize') ); }
		
		}).disableSelection();
		
	});
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function transferHeading( event, ui ){

		var idfamily = ui.item.attr( "id" ).substr( ui.item.attr( "id" ).lastIndexOf( "_" ) + 1 );
		var data = "idfamily=" + idfamily + "&idintitule_title=" + idintitule_title + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?transfer",
			async: true,
			cache: false,
			type: "GET",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert( "Impossible de déplacer l'intitulé " );
				
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function updateHeadingList( idintitule_family ){
	
		var data = "idintitule_family=" + idintitule_family;
		$( "#ExistingHeading" ).attr( "disbled", "disabled" );
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?list",
			async: true,
			cache: false,
			type: "GET",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText ){
	
					$( "#ExistingHeading" ).html( responseText );
        	
        		}
        		else alert( "Impossible de récupérer la liste des intitulés" );
        		
        		$( "#ExistingHeading" ).attr( "disbled", "" );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function deleteHeading( idintitule_title ){
	
		var postdata = "idintitule_title=" + idintitule_title + "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?delete",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
	
					$( "#ProductTabs" ).tabs( "load", 6 );

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications" + responseText );
        		
			}

		});
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function updateCotation( idintitule_title, cotation ){
	
		var postdata = "idintitule_title=" + idintitule_title;
		postdata += "&cotation=" + escape( cotation );
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications" + responseText );
        		
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function sortHeadings( idintitule_family, serializedString ){
	
		var postdata = "sort=headings&" + serializedString;
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		
			$.ajax({
	
				url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php",
				async: true,
				cache: false,
				type: "GET",
				data: postdata,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
			 	success: function( responseText ){

					if( responseText == "1" ){
					
						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );
	        	
	        		}
	        		else alert( "Impossible d'enregistrer les modifications" + responseText );
	        		
				}
	
			});	
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function sortFamilies( serializedString ){
	
		var data = "sort=families&" + serializedString;
		data += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		
			$.ajax({
	
				url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php",
				async: true,
				cache: false,
				type: "GET",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
			 	success: function( responseText ){

					if( responseText == "1" ){
					
						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );
	        	
	        		}
	        		else alert( "Impossible d'enregistrer les modifications" + responseText );
	        		
				}
	
			});	
	
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function createHeading(){
	
		var heading = "";

		if( document.forms.myform.elements[ 'headingField' ][ 0 ].checked )
			{heading = $( "#NewHeading" ).attr( "value" );
			heading2 = $( "#NewHeading2" ).attr( "value" );
	}
		else {heading = document.getElementById( 'ExistingHeading' ).options[ document.getElementById( 'ExistingHeading' ).selectedIndex ].value;
		heading2 = document.getElementById( 'ExistingHeading' ).options[ document.getElementById( 'ExistingHeading' ).selectedIndex ].value;
}
		if( !heading.length )
			return;

		var postdata = "heading=" + escape( heading )+"&heading2=" + escape( heading2 );
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		postdata += "&idintitule_family=" + document.getElementById( "HeadingFamilies" ).options[ document.getElementById( "HeadingFamilies" ).selectedIndex ].value;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?create",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
            dataType:'json',
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouvel intitutlé" ); },
		 	success: function( idintitule_title ){

				if( idintitule_title == "0" )
					alert( "Impossible de créer le nouvel intitulé" );
				else $( "#ProductTabs" ).tabs( "load", 6 );
				
			}

		});
		
		<?php
		
			$rs =& DBUtil::query( "SELECT c.use_select_intitule FROM category c, product p WHERE p.idproduct = '" . $tableobj->get( "idproduct" ) . "' AND p.idcategory = c.idcategory LIMIT 1" );
		
			$use_select_intitule = $rs->fields( "use_select_intitule" );
			$heading_template = $use_select_intitule ? "product_private_headings.htm.php" : "product_headings.htm.php";
			
		?>
		var heading_family = document.getElementById( "HeadingFamilies" ).options[ document.getElementById( "HeadingFamilies" ).selectedIndex ].value;
		var tabURL = "<?php echo $GLOBAL_START_URL ?>/templates/product_management/<?php echo $heading_template; ?>?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&heading_family=" + heading_family;
		
		$( "#ProductTabs" ).tabs( "url" , 6 , tabURL );
		
	}
	
	function createHeading2(){
	
		var heading = "";

		if( document.forms.myform.elements[ 'headingField' ][ 0 ].checked )
			{heading = $( "#NewHeading" ).attr( "value" );
			//heading2 = $( "#NewHeading2" ).attr( "value" );
			}
		else heading = document.getElementById( 'ExistingHeading' ).options[ document.getElementById( 'ExistingHeading' ).selectedIndex ].value;

		if( !heading.length )
			return;

		var postdata = "heading=" + escape( heading );
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";
		postdata += "&idintitule_family=" + document.getElementById( "HeadingFamilies" ).options[ document.getElementById( "HeadingFamilies" ).selectedIndex ].value;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?create",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le nouvel intitutlé" ); },
		 	success: function( idintitule_title ){

				if( idintitule_title == "0" )
					alert( "Impossible de créer le nouvel intitulé" );
				else $( "#ProductTabs" ).tabs( "load", 6 );
				
			}

		});
		
		<?php
		
			$rs =& DBUtil::query( "SELECT c.use_select_intitule FROM category c, product p WHERE p.idproduct = '" . $tableobj->get( "idproduct" ) . "' AND p.idcategory = c.idcategory LIMIT 1" );
		
			$use_select_intitule = $rs->fields( "use_select_intitule" );
			$heading_template = $use_select_intitule ? "product_private_headings.htm.php" : "product_headings.htm.php";
			
		?>
		var heading_family = document.getElementById( "HeadingFamilies" ).options[ document.getElementById( "HeadingFamilies" ).selectedIndex ].value;
		var tabURL = "<?php echo $GLOBAL_START_URL ?>/templates/product_management/<?php echo $heading_template; ?>?idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&heading_family=" + heading_family;
		
		$( "#ProductTabs" ).tabs( "url" , 6 , tabURL );
		
	}
	/* ------------------------------------------------------------------------------------------------- */
	
	function hideHeading( idproduct, idintitule_title ){
	
		var available = $( "#Available_" + idintitule_title ).attr( "value" ) == "1" ? "0" : "1";
		var data = "available=" + available + "&idproduct=" + idproduct + "&idintitule_title=" + idintitule_title;
		
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_headings.htm.php?hide",
			async: true,
			cache: false,
			type: "GET",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'afficher/masquer l'intitulé" ); },
		 	success: function( responseText ){

				if( responseText == "1" ){

					$( "#Available_" + idintitule_title ).attr( "value", available );
					$( "#Available_" + idintitule_title ).parent().parent().css( "background-color", available == "1" ? "#FFFFFF" : "#FAFAFA" );
					$( "#AvailableIcon_" + idintitule_title ).attr( "src", available == "1" ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif" );
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

				}
				else alert( "Impossible d'afficher/masquer l'intitulé" );

			}

		});
		
	}
	
/* ]]> */
</script>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_headings.htm.php</span>
<?php } ?>

<p style="text-align:right; margin-top:20px;">
	<a href="/product_management/catalog/detail.php?create&amp;idproduct=<?php echo $tableobj->get( "idproduct" ); ?>">
		<img src="/images/back_office/content/button_create.png" alt="" /> Créer une référence
	</a>
</p>
						<?php 
					$lang2 = User::getInstance()->getLang2();
					$idlang=explode("_",$lang2);
					$id_lang=$idlang[1];
					$query = "SELECT * FROM language where id_lang=$id_lang";
					$rs =& DBUtil::query( $query );
					$flag		= $rs->fields( "flag" );
					?>
<table style="margin:10px 0px;">

	<?php if($lang2!='_1') {?>
	<tr><input type="radio" name="headingField" value="input" checked="checked" />
			<label>Créer un nouvel intitulé :</label></tr>
	<tr>
		<td>
			
			&nbsp;<label>Intitulé 1: &nbsp;<img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/Fr.png" style="width: 15px;
height: 10px;"></label>
		</td>
		<td>

			<input style="width:250px;" type="text" name="NewHeading" id="NewHeading" value="" class="textInput" onkeydown="if( event.keyCode == 13 ){ $( this ).bind( 'keyup', function(e){createHeading();} ); return false; }" onclick="document.forms.myform.elements[ 'headingField' ][ 0 ].checked = true;" />
			<?php 
		
				include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
				AutoCompletor::completeFromDB( "NewHeading", "intitule_title", "intitule_title$lang", 15, "createHeading" ); 
			
			?>
		</td>
		<td rowspan="2">
			<label>dans la famille <?php listHeadingFamilies(); ?></label>
			<input type="button" name="" value="Ajouter" class="blueButton" onclick="createHeading();" />
		</td>
	</tr>

	<tr>
		<td>
			<label>Intitulé 2: &nbsp;<img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/<?php echo  $flag; ?>" style="width: 15px;
height: 10px;"></label>
		</td>
		<td>
			<input style="width:250px;" type="text" name="NewHeading2" id="NewHeading2" value="" class="textInput" onkeydown="if( event.keyCode == 13 ){ $( this ).bind( 'keyup', function(e){createHeading();} ); return false; }" onclick="document.forms.myform.elements[ 'headingField' ][ 0 ].checked = true;" />
			<?php 
		
				include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
				AutoCompletor::completeFromDB( "NewHeading2", "intitule_title", "intitule_title_2", 15, "createHeading" ); 
			
			?>
		</td>
		<td rowspan="2">
	
		</td>
	</tr>



	<tr>
		<td>
			<input type="radio" name="headingField" value="select" />
			<label>Rechercher un intitulé existant :</label>
		</td>
		<td>
			<select style="width:250px;" id="ExistingHeading" onclick="document.forms.myform.elements[ 'headingField' ][ 1 ].checked = true;" onkeydown="if( event.keyCode == 13 ){ createHeading(); return false; }">
			<?php
			
				$lang = User::getInstance()->getLang();
				$idintitule_family = isset( $_REQUEST[ "heading_family" ] ) ? intval( $_REQUEST[ "heading_family" ] ) : 0;
				
				$query = "
				SELECT DISTINCT( it.intitule_title$lang )
				FROM intitule_title it, intitule_title_family itf
				WHERE itf.idintitule_family = '$idintitule_family'
				AND itf.idintitule_title = it.idintitule_title
				ORDER BY it.intitule_title$lang ASC";
				
				$rs =& DBUtil::query( $query );
				
				while( !$rs->EOF() ){
					
					?>
					<option value="<?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?>"><?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?></option>
					<?php
					
					$rs->MoveNext();
					
				}
				
			?>
			</select>
		</td>
	</tr>
	<?php } else {?>
<tr>
		<td>
			<input type="radio" name="headingField" value="input" checked="checked" />
			Créer un nouvel intitulé :
		</td>
		<td>

			<input style="width:250px;" type="text" name="" id="NewHeading" value="" class="textInput" onkeydown="if( event.keyCode == 13 ){ $( this ).bind( 'keyup', function(e){createHeading();} ); return false; }" onclick="document.forms.myform.elements[ 'headingField' ][ 0 ].checked = true;" />
			<?php 
		
				include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
				AutoCompletor::completeFromDB( "NewHeading", "intitule_title", "intitule_title$lang", 15, "createHeading" ); 
			
			?>
		</td>
		<td rowspan="2">
			<label>dans la famille <?php listHeadingFamilies(); ?></label>
			<input type="button" name="" value="Ajouter" class="blueButton" onclick="createHeading();" />
		</td>
	</tr>


	<tr>
		<td>
			<input type="radio" name="headingField" value="select" />
			<label>Rechercher un intitulé existant :</label>
		</td>
		<td>
			<select style="width:250px;" id="ExistingHeading" onclick="document.forms.myform.elements[ 'headingField' ][ 1 ].checked = true;" onkeydown="if( event.keyCode == 13 ){ createHeading(); return false; }">
			<?php
			
				$lang = User::getInstance()->getLang();
				$idintitule_family = isset( $_REQUEST[ "heading_family" ] ) ? intval( $_REQUEST[ "heading_family" ] ) : 0;
				
				$query = "
				SELECT DISTINCT( it.intitule_title$lang )
				FROM intitule_title it, intitule_title_family itf
				WHERE itf.idintitule_family = '$idintitule_family'
				AND itf.idintitule_title = it.idintitule_title
				ORDER BY it.intitule_title$lang ASC";
				
				$rs =& DBUtil::query( $query );
				
				while( !$rs->EOF() ){
					
					?>
					<option value="<?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?>"><?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?></option>
					<?php
					
					$rs->MoveNext();
					
				}
				
			?>
			</select>
		</td>
		<td></td>
	</tr>

		<?php }?>
</table>
<?php
	
$query = "
SELECT DISTINCT( il.idintitule_title ), 
	il.available,
	it.intitule_title$lang, 
	ifam.intitule_family$lang, 
	il.cotation, 
	il.idfamily
FROM intitule_link il, intitule_title it, intitule_family ifam
WHERE il.idintitule_title = it.idintitule_title
AND il.idproduct = '" . $tableobj->get( "idproduct" ) . "'
AND il.idintitule_title = it.idintitule_title
AND il.idfamily = ifam.idintitule_family
ORDER BY il.family_display ASC, il.title_display ASC";

$rs =& DBUtil::query( $query );

if( !$rs->RecordCount() ){
	
	?>
	<p class="msg" style="text-align:center; margin:25px;">Aucun intitulé n'a été créé pour ce produit</p>
	<?php
		
}

if( $rs->RecordCount() )
	echo "<ul id=\"HeadingFamilyList\">";
	
$last_idintitule_family = false;
while( !$rs->EOF() ){
	
	if( $last_idintitule_family === false || $rs->fields( "idfamily" ) != $last_idintitule_family ){
		
		?>
		<li id="idintitule_family_<?php echo $rs->fields( "idfamily" ); ?>" class="headingFamilyListItem">
			<div class="subTitleContainer" style="cursor:move;">
				<p class="subTitle"><?php echo htmlentities( $rs->fields( "intitule_family$lang" ) ); ?></p>
			</div>
			<ul id="HeadingFamily_<?php echo $rs->fields( "idfamily" ); ?>" class="headingFamily">
			<?php
		
	}
	
	?>
	<li id="idintitule_title_<?php echo $rs->fields( "idintitule_title" ); ?>" class="heading" style="background-color:<?php echo $rs->fields( "available" ) ? "#FFFFFF" : "#FAFAFA"; ?>">
		<!--<span class="available">
			<img id="AvailableIcon_<?php echo $rs->fields( "idintitule_title" ); ?>" onclick="hideHeading( <?php echo $tableobj->get( "idproduct" ); ?>, <?php echo $rs->fields( "idintitule_title" ); ?> );" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $rs->fields( "available" ) ? "visible.gif" : "hidden.gif"; ?>" alt="" />
			<input type="hidden" id="Available_<?php echo $rs->fields( "idintitule_title" ); ?>" value="<?php echo $rs->fields( "available" ) ? "1" : "0"; ?>" />
		</span>
		<span class="cotation">
			cote <input type="text" style="width:35px;" class="textInput" value="<?php echo htmlentities( $rs->fields( "cotation" ) ); ?>" name="cotation" onkeyup="updateCotation(<?php echo $rs->fields( "idintitule_title" ); ?>, this.value);" />
		</span>-->
		<img class="recycle" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" alt="" style="cursor:pointer;" onclick="deleteHeading( <?php echo $rs->fields( "idintitule_title" ); ?> );" />
		<?php echo htmlentities( $rs->fields( "intitule_title$lang" ) ); ?>
	</li>
	<?php
	
	$last_idintitule_family =  $rs->fields( "idfamily" );
	
	if( $rs->fields( "idfamily" ) != $last_idintitule_family )
		echo "</li>";
		
	$rs->MoveNext();
	
	if( !$rs->EOF() && $last_idintitule_family != $rs->fields( "idfamily" ) ){
		
		?>
		</ul>
		<?php
		
	}
		
}

if( $rs->RecordCount() )
	echo "</ul>";
	
//-----------------------------------------------------------------------------------------------------

function getIntituleFamilyName( $idintitule_family ){

	global $base, $langue;
	
	$query = "SELECT intitule_family$langue AS intitule_family FROM intitule_family WHERE idintitule_family = '$idintitule_family' LIMIT 1";

	$rs = DBUtil::getConnection()->Execute( $query );
	
	if( !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "intitule_family" );
	
}

//-----------------------------------------------------------------------------------------------------

function warnHeadingError( $message ){

	global $GLOBAL_START_URL;
	
	?>
	<a class="tooltip" href="#" onclick="javascript:return false;">
		<img class="helpicon" src="<?php echo $GLOBAL_START_URL ?>/images/error.gif" alt="" border="0" />
		<div class="tooltiphelp" style="width:200px;"><?php echo htmlentities( $message ) ?></div>
	</a>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function createProductHeading( $heading,$heading2,$idintitule_family, $idproduct ){
	
	$lang = User::getInstance()->getLang();
	
	/* création de l'intitulé si nécéssaire */
	
	$rs =& DBUtil::query( "SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE '" . Util::html_escape( $heading ) . "' LIMIT 1" );

	if( !$rs->RecordCount() ){
		
		$query = "
		INSERT  INTO intitule_title( intitule_title$lang,intitule_title_2, lastupdate, username ) 
		VALUES( '" . Util::html_escape( $heading ) . "','" . Util::html_escape( $heading2 ) . "', NOW(), '" . Util::html_escape( User::getInstance()->get( "login" ) ) . "' )";
	
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			return false;

		$idintitule_title = DBUtil::getInsertID();
		
	}
	else $idintitule_title = $rs->fields( "idintitule_title" );

	/* ajout de l'intitulé à la famille sélectionnée */
	
	$rs =& DBUtil::query( "SELECT MAX( display ) + 1 AS display FROM intitule_title_family WHERE idintitule_family = '$idintitule_family'" );
	$display = $rs->fields( "display" ) == null ? 1 : $rs->fields( "display" );
	
	DBUtil::query( "INSERT IGNORE INTO intitule_title_family( idintitule_title, idintitule_family, display ) VALUES( '$idintitule_title', '$idintitule_family', '$display' )" );
	
	/* lier l'intitulé au produit */
	
	$rs =& DBUtil::query( "SELECT MAX( title_display ) + 1 AS title_display FROM intitule_link WHERE idproduct = '$idproduct' AND idfamily = '$idintitule_family'" );

	if( $rs === false )
		return false;
		
	$title_display = $rs->fields( "title_display" ) == null ? 1 : $rs->fields( "title_display" );

	$rs =& DBUtil::query( "SELECT family_display FROM intitule_link WHERE idproduct = '$idproduct' AND idfamily = '$idintitule_family'" );

	if( $rs === false )
		return false;
		
	if( $rs->RecordCount() )
		$family_display = $rs->fields( "family_display" );
	else{
		
		$rs =& DBUtil::query( "SELECT MAX( family_display ) + 1 AS family_display FROM intitule_link WHERE idproduct = '$idproduct'" );
		
		if( $rs === false )
			return false;
		
		$family_display = $rs->fields( "family_display" ) == null ? 1 : $rs->fields( "family_display" );
		
	}
		
	$ret =& DBUtil::query( "INSERT IGNORE INTO intitule_link( idproduct, idfamily, idintitule_title, family_display, title_display ) VALUES( '$idproduct', '$idintitule_family', '$idintitule_title', '$family_display', '$title_display' )" );

	if( $ret === false )
		return false;
	
	/* lier l'intitulé aux références du produit */
	
	$rs =& DBUtil::query( "SELECT idarticle, reference FROM detail WHERE idproduct = '$idproduct'" );
	
	if( $rs === false )
		return false;
		
	while( !$rs->EOF() ){
		
		$idarticle = $rs->fields( "idarticle" );
		$reference = $rs->fields( "reference" );

		$ret =& DBUtil::query( "INSERT IGNORE INTO intitule_link( idproduct, idarticle, reference, idfamily, idintitule_title, family_display, title_display ) VALUES( '$idproduct', '$idarticle', '$reference', '$idintitule_family', '$idintitule_title', '$family_display', '$title_display' )" );

		if( $ret === false )
			return false;
	
		$rs->MoveNext();
		
	}
	
	return true;
	
}
function createProductHeading2( $heading,$idintitule_family, $idproduct ){
	
	$lang = User::getInstance()->getLang2();
	
	/* création de l'intitulé si nécéssaire */
	
	$rs =& DBUtil::query( "SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE '" . Util::html_escape( $heading ) . "' LIMIT 1" );

	if( !$rs->RecordCount() ){
		
		$query = "
		INSERT  INTO intitule_title( intitule_title$lang, lastupdate, username ) 
		VALUES( '" . Util::html_escape( $heading ) . "', NOW(), '" . Util::html_escape( User::getInstance()->get( "login" ) ) . "' )";
	
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			return false;

		$idintitule_title = DBUtil::getInsertID();
		
	}
	else $idintitule_title = $rs->fields( "idintitule_title" );

	/* ajout de l'intitulé à la famille sélectionnée */
	
	$rs =& DBUtil::query( "SELECT MAX( display ) + 1 AS display FROM intitule_title_family WHERE idintitule_family = '$idintitule_family'" );
	$display = $rs->fields( "display" ) == null ? 1 : $rs->fields( "display" );
	
	DBUtil::query( "INSERT IGNORE INTO intitule_title_family( idintitule_title, idintitule_family, display ) VALUES( '$idintitule_title', '$idintitule_family', '$display' )" );
	
	/* lier l'intitulé au produit */
	
	$rs =& DBUtil::query( "SELECT MAX( title_display ) + 1 AS title_display FROM intitule_link WHERE idproduct = '$idproduct' AND idfamily = '$idintitule_family'" );

	if( $rs === false )
		return false;
		
	$title_display = $rs->fields( "title_display" ) == null ? 1 : $rs->fields( "title_display" );

	$rs =& DBUtil::query( "SELECT family_display FROM intitule_link WHERE idproduct = '$idproduct' AND idfamily = '$idintitule_family'" );

	if( $rs === false )
		return false;
		
	if( $rs->RecordCount() )
		$family_display = $rs->fields( "family_display" );
	else{
		
		$rs =& DBUtil::query( "SELECT MAX( family_display ) + 1 AS family_display FROM intitule_link WHERE idproduct = '$idproduct'" );
		
		if( $rs === false )
			return false;
		
		$family_display = $rs->fields( "family_display" ) == null ? 1 : $rs->fields( "family_display" );
		
	}
		
	$ret =& DBUtil::query( "INSERT IGNORE INTO intitule_link( idproduct, idfamily, idintitule_title, family_display, title_display ) VALUES( '$idproduct', '$idintitule_family', '$idintitule_title', '$family_display', '$title_display' )" );

	if( $ret === false )
		return false;
	
	/* lier l'intitulé aux références du produit */
	
	$rs =& DBUtil::query( "SELECT idarticle, reference FROM detail WHERE idproduct = '$idproduct'" );
	
	if( $rs === false )
		return false;
		
	while( !$rs->EOF() ){
		
		$idarticle = $rs->fields( "idarticle" );
		$reference = $rs->fields( "reference" );

		$ret =& DBUtil::query( "INSERT IGNORE INTO intitule_link( idproduct, idarticle, reference, idfamily, idintitule_title, family_display, title_display ) VALUES( '$idproduct', '$idarticle', '$reference', '$idintitule_family', '$idintitule_title', '$family_display', '$title_display' )" );

		if( $ret === false )
			return false;
	
		$rs->MoveNext();
		
	}
	
	return true;
	
}
//-----------------------------------------------------------------------------------------------------

function listHeadingFamilies(){
	
	$lang = User::getInstance()->getLang();
	
	?>
	<select id="HeadingFamilies" name="HeadingFamilies" onchange="updateHeadingList( this.options[ this.selectedIndex ].value );">
	<?php
	
		$rs =& DBUtil::query( "SELECT idintitule_family, intitule_family$lang FROM intitule_family ORDER BY intitule_family$lang ASC" );
		
		while( !$rs->EOF() ){
			
			$selected = ( isset( $_REQUEST[ "heading_family" ] ) && $_REQUEST[ "heading_family" ] == $rs->fields( "idintitule_family" ) ) || ( !isset( $_REQUEST[ "heading_family" ] ) && $rs->fields( "idintitule_family" ) == 0 )? " selected=\"selected\"" : "";
			
			?>
			<option value="<?php echo $rs->fields( "idintitule_family" ); ?>"<?php echo $selected; ?>><?php echo htmlentities( $rs->fields( "intitule_family$lang" ) ); ?></option>
			<?php
			
			$rs->MoveNext();
				
		}
		
	?>
	</select>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>