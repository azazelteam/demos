<?php
    include_once (dirname(__FILE__) . "/../../objects/classes.php");
?>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
<?php
    //Début Nbre de produits
    $query = "SELECT COUNT(*) AS total FROM product WHERE  available = 1";
	$db =& DBUtil::getConnection();
	$rs = $db->Execute( $query );
	$totalProducts = $rs->fields( "total" );
    //Fin Nbre de produits
    //Début Nbre de références
    $query = "SELECT COUNT(*) AS total FROM product p,detail d WHERE  p.available = 1 AND p.idproduct = d.idproduct";
	$db =& DBUtil::getConnection();
	$rs = $db->Execute( $query );
	$totalRefs = $rs->fields( "total" );
    //Fin Nbre de références
    //Début Commentaires produits non validés
    $query = "SELECT COUNT(*) AS total FROM product_comment WHERE accept_admin = 0";
	$db =& DBUtil::getConnection();
	$rs = $db->Execute( $query );
	$totaCom = $rs->fields( "total" );
    //Fin Commentaires produits non validés
    //Début Les 10 produits les plus visités
    $query = "SELECT sum(hits) as nb, name_1 FROM `product` GROUP BY idproduct ORDER BY nb DESC LIMIT 10";
	$db =& DBUtil::getConnection();
	$rsNbVist = $db->Execute( $query );
	$n= $rsNbVist->RecordCount(); 
    //Fin Les 10 produits les plus visités
?>
<div class="centerMax">
    <!-- Bloc de droite -->
    <div id="tools" class="rightTools">
    </div>
    <div class="contentDyn content_catalog">
	
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/ratio_product_management.php</span>
<?php } ?>
	
        <h1 class="titleSearch">
            <span class="textTitle" style="width:72%;">Tableau de bord : catalogue</span>
            
            <div class="spacer"></div>
        </h1>
        <div id="container" class="ui-tabs ui-widget ui-widget-content widget-bg-color-white">
            <div class="ratio-content widget-bg-color-white">
                <div class="row widget-row spacer">
                    <div class="col-md-3 ratio-col chefProd_right">
                        <!-- BEGIN WIDGET THUMB -->
                        <div class="widget-thumb  ">
                            <h4 class="widget-thumb-heading">Nombre de produits affichés</h4>
                            <div class="widget-thumb-wrap">
                                <div class="widget-thumb-body">
                                    <div class="current-year">
                                        <span class="widget-thumb-body-stat"  data-value="0,8"><?php echo $totalProducts ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-thumb  ">
                            <h4 class="widget-thumb-heading">Nombre de références affichées</h4>
                            <div class="widget-thumb-wrap">
                                <div class="widget-thumb-body new-hire">
                                    <div class="current-year">
                                        <span class="widget-thumb-body-stat"  data-value="9123"><?php echo $totalRefs ?></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="widget-thumb terminations">
                            <h4 class="widget-thumb-heading">Commentaires produits non validés</h4>
                            <div class="widget-thumb-wrap">
                                <div class="widget-thumb-body">
                                    <div class="current-year">
                                        <span class="widget-thumb-body-stat"  data-value="8767"><?php echo $totaCom ?></span>
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- END WIDGET THUMB -->
                    </div>
                    <?php if($n>0)
		                  {
                    ?>
                            <div class="col-md-9 ratio-col chefProd_left">
                                <div class="subTitleContainer" style="margin-bottom:6px;margin-top: 0px;">
                                    <h4 class="widget-thumb-heading">Les 10 produits les plus visités :</h4>
                                </div>
        				            <table class="dataTable resultTable">
                            
                    <?php 
                                        echo '<tr class="filledRow"><th>Nom</th><th>Quantité</th></tr>';
                                        for($i=0;$i<$n;$i++){
                                            echo'<tr>'.
                            						'<td>'.$rsNbVist->Fields('name_1').'</td>'.
                            						'<td>'.$rsNbVist->Fields('nb').'</td></tr>';
                                                $rsNbVist->Movenext();
                                        }
		                  }
                                    
                    ?>
                                    </table>
                                   
                                  </div>
                            </div>
                             <!--DEBUT-->
                                    <?php 
                                        set_time_limit(300);
                                        //rova: commenté car déjà déclaré sur la ligne 2
                                        //include_once (dirname(__FILE__) . "/../objects/classes.php");
                                        
                                        include_once("$GLOBAL_START_PATH/catalog/drawlift.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/drawlift_stats.php"); 
                                        include_once("$GLOBAL_START_PATH/objects/Menu.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/calendar.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/getfunctions.inc.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/statsFunctions.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/convertPeriod.php");
                                        
                                        // initialisation des critères de tri ----------------------------------------
                                        
                                        
                                        include_once("$GLOBAL_START_PATH/statistics/param_management.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/status.inc.php");
                                        include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
                                        
                                        // Include des pages de stats ------------------------------------------------
                                        include_once("$GLOBAL_START_PATH/statistics/objects/tableStats.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/init_arr_stat.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/camembert.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/graph.class.php");
                                        include_once("$GLOBAL_START_PATH/statistics/objects/marge_sort.php");
                                        
                                        ?>
                                        <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
                                        <?php
                                        echo '<script type="text/javascript" src="'.$GLOBAL_START_URL.'/js/statistics/graphs/FlotKit/jquery.flot.pie.js"></script>'."\n";
                                        
                                        echo '<!--[if IE]><script language="javascript" type="text/javascript" src="'.$GLOBAL_START_URL.'/js/statistics/graphs/FlotKit/excanvas.pack.js"></script><![endif]-->';
                                        echo '<link type="text/css" rel="stylesheet" href="'.$GLOBAL_START_URL.'/css/statistiques.css" />';
                                        echo '<link type="text/css" rel="stylesheet" href="'.$GLOBAL_START_URL.'/css/newstats.css" />';
                                        
                                        InitJSRadio();
                                        InitJSMenu();
                                        InitJS_CheckDate();
                                        
                                        echo '<div class="statBodyDiv statBodyDiv_ratio catalog"><center>';
                                        
                                        //include_once("$GLOBAL_START_PATH/statistics/show_criter.htm.php");
                                        
                                        // Chargement
                                        echo '<div id="load" class="loading" align="center" style="text-align: center;"><img src="img/loading.gif" align="absmiddle" alt="loading"/> Traitement en cours ...' .
                                        		'<br /><span style="font-size: 9px;">Veuillez patienter quelques instants</span></div>'."\n\n\n";
                                        
                                        if( ob_get_length() )
                                        	ob_end_flush(); 
                                        
                                        ob_start();
                                         
                                        // Paramétrage couleurs --------------------------------------------------------
                                        
                                        $filename = "$GLOBAL_START_PATH/statistics/colors.init.htm";
                                        $handle = fopen ($filename, "r"); 
                                        $filecolors = fread ($handle, filesize ($filename));
                                        fclose ($handle); 
                                        $camcolor   = $histocolor = preg_split("/,?#/",$filecolors,-1,PREG_SPLIT_NO_EMPTY);
                                            
                                        $arr=array();	
                                        $db = &DBUtil::getConnection();
                                        
                                        // Initialisation des abscisses histogramme et filtre de recherche
                                        $datesearch		= Date("Y"); //Date de filtre	
                                        $datesearch = $_SESSION['calendar']; 
                                        $datesearchsize	= strlen($datesearch);		//Taille du filtre de date
                                        $datesize 		= $datesearchsize+3;		//Taille en caracteres de la date en abscisse
                                        
                                        if($_SESSION['calendarType']==4){  $searchDateParam = "year"; 	} 
                                        if($_SESSION['calendarType']==7){  $searchDateParam = "month"; 	}
                                        if($_SESSION['calendarType']==11){ $searchDateParam = "period"; }
                                        $searchDateParam = "period";
                                        $from_ratios = true;
                                        
                                        $fromDate = date("Y-m-d", strtotime("-11 months"));
                                        $calendar = explode("-", $fromDate);
                                        $_SESSION['StatStartDateRatios'] 	= Date("Y-m-d", mktime(0,0,0,$calendar[1],1,$fromDate));
                                        
                                        $_POST['idcomm'] = $iduser;
                                        include("$GLOBAL_START_PATH/statistics/objects/marge_category_sort.php");	
                                        
                                        
                                        
                                        $data = ob_get_contents();
                                        ob_end_clean();
                                        
                                        echo $data;
                                        
                                        echo '</center></div>';
                                        echo '<script type="text/javascript" language="javascript">' .
                                        	 'document.getElementById(\'load\').style.display=\'none\';'.
                                        	 '</script>';
                                        
                                        ?>
                                        
                                        		<div class="spacer"></div>
                                        		</div>
                                        	</div>
                                            
                                            
                                        </div>
                
                                <!--FIN-->
                </div>
                
                </div>
                <div class="spacer"></div>
            </div>
        </div>
        <div class="spacer"></div>
        <div class="spacer"></div>

    </div>

		<div class="spacer"></div>
		</div>
	</div>
</div>

