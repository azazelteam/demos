<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 *....
 */

include_once( "../../config/init.php" );

//--------------------------------------------------------------------------------------------------------
if( isset( $_REQUEST[ "idproduct" ] ) ){

	$idproduct=intval( $_REQUEST[ "idproduct" ] );
}

if( isset( $_REQUEST[ "idcategory" ] ) )
	$idcategory = intval( $_REQUEST[ "idcategory" ] );
else if( isset( $_REQUEST[ "idproduct" ] ) )
	$idcategory = DBUtil::getDBValue( "idcategory", "product", "idproduct", intval( $_REQUEST[ "idproduct" ] ) );
else $idcategory = 0;

$id 						= isset( $_REQUEST[ "id" ] ) ? stripslashes( $_REQUEST[ "id" ] ) : "CategoryTree"; /* pouvoir faire coéxister plusieurs arbres */
$onSelectCategoryCallback 	= isset( $_REQUEST[ "onSelectCategoryCallback" ] ) ? stripslashes( $_REQUEST[ "onSelectCategoryCallback" ] ) : null;
$onSelectProductCallback 	= isset( $_REQUEST[ "onSelectProductCallback" ] ) ? stripslashes( $_REQUEST[ "onSelectProductCallback" ] ) : null;
$onlyLeafSelectable 		= isset( $_REQUEST[ "selectable" ] ) && $_REQUEST[ "selectable" ] == "leafs";
$hideProducts 				= isset( $_REQUEST[ "hide" ] ) && $_REQUEST[ "hide" ] == "products";

//--------------------------------------------------------------------------------------------------------

$root = new stdClass();

$root->id = 0;
$root->type = "category";
$root->name = "Accueil";
$root->children = array();
$root->hasChildren = false;
$root->depth = 0;
$root->selected = true;
$root->idparent = 0;
$root->available = true;

setChildren( $root, getTreePath( $idcategory ) );

?>
<style type="text/css">

	#<?php echo $id; ?> .categoryNode{ font-weight: normal; }
	#<?php echo $id; ?> .selectedCategoryNode{ font-weight: bold; }
	#<?php echo $id; ?> .hiddenCategoryNode a{ text-decoration: font-style: italic; }
	
	#<?php echo $id; ?>, #<?php echo $id; ?> ul{
		
		padding: 0px;
		
	}
	
	#<?php echo $id; ?> li{
				
		list-style-type:none;
		/*white-space: nowrap;*/
	}

	#<?php echo $id; ?> li a{
	
		color:#44474E;
		text-decoration:none;
			
	}
	
	#<?php echo $id; ?> li a img{
		
		border-style:none;

	}
			
</style>
<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function <?php echo $id; ?>ExpandNode( idcategory ){

		$.ajax({
							 	
			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_categories.htm.php?id=<?php echo $id; ?>&idcategory=" + idcategory + "<?php

				if( $onSelectCategoryCallback )
					echo "&onSelectCategoryCallback=$onSelectCategoryCallback";
					
				if( $onSelectProductCallback )
					echo "&onSelectProductCallback=$onSelectProductCallback";
				
				if( $onlyLeafSelectable )
					echo "&selectable=leafs";
				
				if( $hideProducts )
					echo "&hide=products";
							
			?>",
			async: true,
		 	success: function( responseText ){

				$( '#<?php echo $id; ?>' ).parent().html( responseText );

			}
		
		});
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_categories.htm.php</span>
<?php } ?>

<ul id="<?php echo $id; ?>">
<?php getListElement( $root, $idcategory ); ?>
</ul>
<?php

//--------------------------------------------------------------------------------------------------------

/**
 * @return void
 */
function setChildren( stdClass &$parentNode, $treePath ){

	global	$hideProducts;

	$lang = User::getInstance()->getLang();
	
	/* sous-catégories */
	
	$query = "
	SELECT cl.idcategorychild, name$lang, c.available
	FROM category_link cl, category c
	WHERE cl.idcategoryparent = '{$parentNode->id}'	
	AND cl.idcategorychild = c.idcategory
	ORDER BY cl.displayorder ASC";

	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() )
		$parentNode->hasChildren = true;
		
	while( !$rs->EOF() ){
		
		$childNode = new stdClass();
		
		$childNode->id = $rs->fields( "idcategorychild" );
		$childNode->type = "category";
		$childNode->name = $rs->fields( "name$lang" );
		$childNode->children = array();
		$childNode->depth = $parentNode->depth + 1;
		$childNode->selected = false;
		$childNode->hasChildren = false;
		$childNode->idparent = $parentNode->id;
		$childNode->available = $rs->fields( "available" ) == 1;
		
		if( in_array( $childNode->id, $treePath ) ){
			
			$childNode->selected = true;
			setChildren( $childNode, $treePath );
		
		}
		else{
			
			$childNode->hasChildren = DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '{$childNode->id}' LIMIT 1" )->RecordCount() == 1;
			//$childNode->hasChildren |= !$hideProducts && DBUtil::query( "SELECT idproduct FROM product WHERE idcategory = '{$childNode->id}' LIMIT 1" )->RecordCount() == 1;

		}
		
		array_push( $parentNode->children, $childNode );
			 
		$rs->MoveNext();
		
	}
}

//--------------------------------------------------------------------------------------------------------

/**
 * @return array la branche sélectionnée
 */
function getTreePath( $idcategory ){
	
	$treePath = array( $idcategory );
	
	if( !$idcategory )
		return $treePath;
		
	$idchild = $idcategory;
	$hasParent = true;

	while( $hasParent ){
		
		$rs =& DBUtil::query( "SELECT idcategoryparent AS idparent FROM category_link WHERE idcategorychild = '$idchild' LIMIT 1" );
		
		if( !$rs->RecordCount()  )
			$hasParent = false;
		else{
			
			$treePath[] = $idchild = $idparent = $rs->fields( "idparent" );

			if( !$idparent )
				$hasParent = false;
			
		}
	
	}

	return array_reverse( $treePath );

}

//--------------------------------------------------------------------------------------------------------

function getListElement( stdClass &$treeNode, $idcategory ){
	
	global $GLOBAL_START_URL,
			$onSelectCategoryCallback,
			$onSelectProductCallback,
			$onlyLeafSelectable,
			$id;

	$cssClass	= $treeNode->id == $idcategory ? "selectedCategoryNode" : "categoryNode";
	
	if( !$treeNode->available )
		$cssClass .= " hiddenCategoryNode";
		
	$leftMargin = $treeNode->depth * 10;
	
	echo "<li class=\"$cssClass\" style=\"margin-left:{$leftMargin}px\">";

	$folderIcon = $treeNode->type == "category" ? "folder_purple_16.png" : "folder_orange_16.png";
	
	/* expander/collapser */
	
	if( $treeNode->hasChildren && !$treeNode->selected ){
		
		?>
		<a href="#" onclick="<?php echo $id; ?>ExpandNode(<?php echo $treeNode->id; ?>); return false;">
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/expand.png" alt="" />
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $folderIcon; ?>" alt="" />
		</a>
		<?php
		
	}
	else{
		
		?>
		<a href="#" onclick="<?php echo $id; ?>ExpandNode(<?php echo $treeNode->idparent; ?>); return false;">
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/collapse.png" alt="" />
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $folderIcon; ?>" alt="" />
		</a>
		<?php
		
	}
	
	/* element name */
	
	if( $onlyLeafSelectable && $treeNode->hasChildren ){
		
		$href 		= "#"; 
		$onclick 	= $treeNode->hasChildren ? "{$id}ExpandNode( {$treeNode->id} ); return false;" : "{$id}ExpandNode( {$treeNode->idparent} ); return false;";
		
	}
	else if( $treeNode->type == "category"){
	
		//$href 		= $onSelectCategoryCallback ? "#" : "$GLOBAL_START_URL/product_management/catalog/cat_category.php?action=edit&amp;idcategory=" . $treeNode->id; 
		$href 		= "#"; 
		$onclick 	=(!$treeNode->hasChildren )? "addCateg( {$treeNode->id} );":null;	
		
	}
	else{
	
		$href 		= $onSelectProductCallback ? "#" : "$GLOBAL_START_URL/product_management/catalog/cat_product.php?idproduct=" . $treeNode->id; 
		$onclick 	= $onSelectProductCallback ? "$onSelectProductCallback( $treeNode->id ); return false;" : null;	
		
	}

	?>
	<a href="<?php echo $href; ?>"<?php if( $onclick ) echo " onclick=\"$onclick\""; ?>><?php echo htmlentities( $treeNode->name ); ?></a>
	<?php

	if( count( $treeNode->children ) ){
		
		echo "<ul>";
		
		$i = 0;
		while( $i < count( $treeNode->children ) ){
			
			
			getListElement( $treeNode->children[ $i ], $idcategory );
			
			$i++;
				
		}
	
		echo "</ul>";
		
	}
	
	echo "</li>";
	
}
?>
<script type="text/javascript">
function deleteCateg( idcategNew ){

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo intval( $_REQUEST[ "idproduct" ] ); ?>&idcategNew=" + idcategNew+"&delete_cat",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert(responseText );
				else{

					$( "#Comment" + idproduct_comment ).remove();

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

				}

			}

		});

	}
function addCateg(idcategNew){

		//var comment = $( "#comment" + idproduct_comment ).text();
		var idproduct = jQuery('#importOneProductCSV input[name=idproduct]').val();
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct="+idproduct+"&idcategNew=" + idcategNew+"&add_cat",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert(responseText );
				else{
	        		$.growlUI( '', 'Modifications enregistrées' );

        				$("#CategoryParentDialog").remove();
        				location.reload();

				}

			}

		});

	}
	</script>
