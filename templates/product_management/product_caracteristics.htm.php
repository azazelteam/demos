<?php

//---------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

$notemplate = true;

include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );


//-----------------------------------------------------------------------------------------------------

function displayOrderEditor( $idproduct ){

	global 	$tableobj,
			$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	
	$db =DBUtil::getConnection();
	
	$query = "SELECT idcategory AS idparent FROM product WHERE idproduct = '$idproduct' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer la catégorie parente" );
	
	$idparent = $rs->fields( "idparent" );
	
	$langue=User::getInstance()->getLang();
	$query = "
	SELECT p.idproduct AS idchild, 
		p.name$langue,
		p.display_product_order,
		p.available
	FROM product p
	WHERE p.idcategory = '$idparent'
	ORDER BY p.display_product_order ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer la liste des produits dans la même catégorie" );
		
	?>
	<style type="text/css">
		
		#SiblingList {
			
			margin-bottom: 1em;
			
		}
		
		#SiblingList li{
			
			margin-bottom: 0px;
			margin-top: 4px;
			width:60px;
			height:60px;
			float:left;
			border:1px solid #E7E7E7;
			margin:4px;
			
		}

		#SiblingList .placeholder{
			
			width:60px;
			height:60px;
			float:left;
			border:1px solid #E7E7E7;
			background-color:#F3F3F3;
			
		}
		
		#SiblingList li a{ cursor:move; }
	</style>
    
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_caracteristics.htm.php</span>
<?php } ?>
	
	<a href="#" onclick="displayOrderEditor(); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/eye.gif" alt="Voir" style="border-style:none; vertical-align:text-bottom;" /></a>
	&nbsp;<a href="#" onclick="displayOrderEditor(); return false;"><span id="orderEditorText">Afficher</span> le tri</a>
	<ul id="SiblingList" style="display:none;">
	<?php 
	
		$rs->MoveFirst();
		
		while( !$rs->EOF() ){
			
			$name = $rs->fields( "name$langue" );
			$idchild = $rs->fields( "idchild" );
			$displayorder = $rs->fields( "display_product_order" );
			$opacity = $rs->fields( "available" ) ? "1.0" : "0.7";
						
			?>
			<li id="Siblings_<?php echo $idchild ?>">
				<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $idchild; ?>">
					<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_ICON_SIZE ); ?>" title="<?php echo htmlentities( $name ); ?>" alt="" style="opacity:<?php echo $opacity; ?>;" />
				</a>
			</li>
			<?php 
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<input type="hidden" id="ProductListOrder" name="ProductListOrder" value="" />
	<?php 
	
}

//-----------------------------------------------------------------------------------------------------

?>