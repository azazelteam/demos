<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include_once( "../../config/init.php" );

header( "Content-Type: text/html; charset=utf-8" );
		
//--------------------------------------------------------------------------------------------------------

$idcategory = isset( $_REQUEST[ "idcategory" ] ) ? intval( $_REQUEST[ "idcategory" ] ) : 0;
$onExpandCallback 	= stripslashes( $_REQUEST[ "onExpandCallback" ] );
$onSelectCallback 	= stripslashes( $_REQUEST[ "onSelectCallback" ] );

$root = new stdClass();

$root->idcategory = 0;
$root->name = "Accueil";
$root->children = array();
$root->hasChildren = false;
$root->depth = 0;
$root->selected = true;
$root->idparent = 0;
$root->available = true;

setChildren( $root, getTreePath( $idcategory ) );

echo "<ul id=\"CategoryTree\">";
getListElement( $root, $idcategory, $onExpandCallback, $onSelectCallback );
echo "</ul>";

//--------------------------------------------------------------------------------------------------------

/**
 * @return void
 */
function setChildren( stdClass &$parentNode, $treePath ){

	$lang = User::getInstance()->getLang();
		
	$query = "
	SELECT cl.idcategorychild, name$lang, c.available
	FROM category_link cl, category c
	WHERE cl.idcategoryparent = '{$parentNode->idcategory}'	
	AND cl.idcategorychild = c.idcategory
	ORDER BY cl.displayorder ASC";

	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() )
		$parentNode->hasChildren = true;
		
	while( !$rs->EOF() ){
		
		$childNode = new stdClass();
		
		$childNode->idcategory = $rs->fields( "idcategorychild" );
		$childNode->name = $rs->fields( "name$lang" );
		$childNode->children = array();
		$childNode->depth = $parentNode->depth + 1;
		$childNode->selected = false;
		$childNode->hasChildren = false;
		$childNode->idparent = $parentNode->idcategory;
		$childNode->available = $rs->fields( "available" ) == 1;
		
		if( in_array( $childNode->idcategory, $treePath ) ){
			
			$childNode->selected = true;
			 setChildren( $childNode, $treePath );
		
		}
		else $childNode->hasChildren = DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '{$childNode->idcategory}' LIMIT 1" )->RecordCount() == 1;
		
		array_push( $parentNode->children, $childNode );
			 
		$rs->MoveNext();
		
	}
	
}

//--------------------------------------------------------------------------------------------------------

/**
 * @return array la branche sélectionnée
 */
function getTreePath( $idcategory ){
	
	$treePath = array( $idcategory );
	
	if( !$idcategory )
		return $treePath;
		
	$idchild = $idcategory;
	$hasParent = true;

	while( $hasParent ){
		
		$rs =& DBUtil::query( "SELECT idcategoryparent AS idparent FROM category_link WHERE idcategorychild = '$idchild' LIMIT 1" );
		
		if( !$rs->RecordCount()  )
			$hasParent = false;
		else{
			
			$treePath[] = $idchild = $idparent = $rs->fields( "idparent" );

			if( !$idparent )
				$hasParent = false;
			
		}
	
	}

	return array_reverse( $treePath );

}

//--------------------------------------------------------------------------------------------------------

function getListElement( stdClass &$treeNode, $idcategory, $onExpandCallback, $onSelectCallback ){
	
	global $GLOBAL_START_URL;

	$cssClass	= $treeNode->idcategory == $idcategory ? "selectedCategoryNode" : "categoryNode";
	
	if( !$treeNode->available )
		$cssClass .= " hiddenCategoryNode";
		
	$leftMargin = $treeNode->depth * 10;
	
	echo "<li class=\"$cssClass\" style=\"margin-left:{$leftMargin}px\">";
	
	if( $treeNode->hasChildren && !count( $treeNode->children ) ){
		
		?><a href="#" onclick="<?php echo $onExpandCallback; ?>(<?php echo $treeNode->idcategory; ?>); return false;"><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $treeNode->hasChildren ? "cat_close.jpg" : "cat_open.jpg"; ?>" alt="" /></a><?php
		
	}
	else{
		
		?><a href="#" onclick="<?php echo $onExpandCallback; ?>(<?php echo $treeNode->idparent; ?>); return false;"><img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/cat_open.jpg" alt="" /></a><?php
		
	}

	echo "<a href=\"#\" onclick=\"$onSelectCallback({$treeNode->idcategory}); return false;\">";
	echo htmlentities( $treeNode->name );
	echo "</a>";
	
	if( count( $treeNode->children ) ){
		
		$i = 0;
		while( $i < count( $treeNode->children ) ){
			
			echo "<ul>";
			getListElement( $treeNode->children[ $i ], $idcategory, $onExpandCallback, $onSelectCallback );
			echo "</ul>";
			
			$i++;
				
		}
	
	}
	
	echo "</li>";
	
}

//--------------------------------------------------------------------------------------------------------

?>