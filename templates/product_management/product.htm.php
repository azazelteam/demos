<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fiche produit
 */
 
// R?cup?re la session pour uploadify
if( isset( $_REQUEST['sid_uploadify'] ) )
{
    @session_id(  $_REQUEST['sid_uploadify']   );
    @session_start();
}


/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des couleurs dispos par d?faut */

if( isset( $_GET[ "update" ] ) && $_GET[ "update" ] == "default_color" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expiree" );
		
	$idcolor = DBUtil::getDBValue( "idcolor", "product", "idproduct", intval( $_GET[ "idproduct" ] ) );
	
	if( $idcolor )
		if( !DBUtil::query( "SELECT 1 FROM color_product WHERE idproduct = '" . intval( $_GET[ "idproduct" ] ) . "' AND idcolor = '$idcolor' LIMIT 1" )->RecordCount() )
			DBUtil::query( "UPDATE product SET idcolor = 0 WHERE idproduct = '" . intval( $_GET[ "idproduct" ] ) . "' LIMIT 1" );
			 
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=UTF-8" );
	
	listProductColors( intval( $_GET[ "idproduct" ] ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload images produit */

if( isset( $_FILES[ "image" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expiree" );

	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . $_REQUEST[ "folder" ] . "/";
	$ext = strtolower( substr( $_FILES[ "image" ][ "name" ] , strrpos( $_FILES[ "image" ][ "name" ] , "." ) + 1 ) );
	$basename = intval( $_REQUEST[ "index" ] ) ? $_REQUEST[ "idproduct" ] . "." . intval( $_REQUEST[ "index" ] ) : $_REQUEST[ "idproduct" ];
	$targetFile =  str_replace( "//", "/", $targetPath ) . $basename . ".$ext";

	$files = glob( $targetPath . $basename . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	foreach( $files as $file )
		unlink( $file );

	move_uploaded_file( $_FILES[ "image" ][ "tmp_name" ], $targetFile );
	chmod( $targetFile, 0755 );
	
	$uri = URLFactory::getProductImageURI( $_REQUEST[ "idproduct" ], URLFactory::$IMAGE_CUSTOM_SIZE, intval( $_REQUEST[ "index" ] ), 150, 150 );
	$uri .= "?" . md5_file( URLFactory::getProductImagePath( intval( $_REQUEST[ "idproduct" ] ) ) );
	
	exit( $uri );

}

//---------------------------------------------------------------------------------------------
/* combinaison d'intitul?s pour s?lectionner les r?f?rences en front office => v?rifier que chaque combinaison est unique - ajax */

if( isset( $_REQUEST[ "headingCombination" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );

	$idproduct = intval( $_REQUEST[ "idproduct" ] );
	$headings = array();
	
	foreach( explode( ",", $_REQUEST[ "headingCombination" ] ) as $idintitule_title )
		if( $idintitule_title )
		array_push( $headings, $idintitule_title );
		
	if( !count( $headings ) )
		exit( "1" );
	
	$rs =& DBUtil::query( "SELECT idarticle FROM detail WHERE idproduct = '$idproduct' AND type LIKE 'catalog' ORDER BY idarticle ASC" );
	
	if( !$rs->RecordCount() )
		exit( "1" );

	$combinations = array();
	
	while( !$rs->EOF() ){

		$combination = array();
		foreach( $headings as $idintitule_title ){
	
			$rs2 = DBUtil::query( 

				"SELECT CONCAT( $idintitule_title, '_', idintitule_value ) AS value
				FROM intitule_link
				WHERE idproduct = '$idproduct'
				AND idarticle = '" . $rs->fields( "idarticle" ) . "'
				AND idintitule_title = '$idintitule_title'
				LIMIT 1"
			
			);
			
			if( !$rs2->RecordCount() )
				exit( "0" );
				
			array_push( $combination, $rs2->fields( "value" ) );
			
		}
		
		array_push( $combinations, implode( ",", $combination ) );
		
		$rs->MoveNext();
		
	}
	
	exit( count( $combinations ) == count( array_unique( $combinations ) ) ? "1" : "0" );
	
}

//-----------------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );
include_once( "$GLOBAL_START_PATH/objects/Supplier.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=UTF-8" );

$notemplate = true;

include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );

$lang = User::getInstance()->getLang2();

//-----------------------------------------------------------------------------------------------------

$host = URLFactory::getHostURL();
$productURL = URLFactory::getProductURL($_REQUEST[ "idproduct" ]);
$prodUrl1 = substr($productURL, strlen($host));
$prodUrl = substr($prodUrl1, 1);
$ri = DBUtil::query( "SELECT * FROM redirection_items WHERE url_source LIKE '$prodUrl' or url_source LIKE '".$prodUrl."$"."' or url_cible LIKE '$prodUrl1'" );
if ($ri->RecordCount())
{
	$disabled = true;
}
else
{
	$disabled = false;
}

//-----------------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
/* <![CDATA[ */
	
	function catset(formname) {
		window.open('cat_select.php?formname='+formname,'choose_category','width=250,height=300,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}

	function catpopup(po) {
		window.open('cat_popup.php?po='+po,'cat_popop','width=450,height=300,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}

	function mask_product() {
		window.open('../mask/product.html','mask_product','width=600,height=600,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}

	function mask_article() {
		window.open('../mask/article.html','mask_article','width=600,height=600,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}

	function mask_product_pdf() {
		window.open('../mask/product_pdf.html','mask_product_pdf','width=450,height=450,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1');
	}

	/* --------------------------------------------------------------------------------------------------------- */

	function checkHeadingCombination(){

		/*$( "#HeadingCombinationChecker" ).html( '<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/small_black_spinner.gif" alt="" />' );*/

		var combination = "";

		$( "select[id^='HeadingSelector']" ).each( function ( i ){

			if( $( this ).val() )
				combination += combination.length ? "," + $( this ).val() : $( this ).val();

		});

		if( !combination.length ){

			$( "#HeadingCombinationChecker" ).html( "" );
			return;

		}

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product.htm.php?headingCombination=" + combination,
			async: true,
			cache: false,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'explorateur" ); },
		 	success: function( responseText ){

				if( responseText == "1" )
					$( "#HeadingCombinationChecker" ).html( "" );
				else $( "#HeadingCombinationChecker" ).html( '<span style="color:#FF0000;">Certaines combinaisons sont ambigues</span>' );

			}

		});

	}

	/* --------------------------------------------------------------------------------------------------------- */

/* ]]> */
</script>

<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
<script type="text/javascript">
/* <![CDATA[ */

	var prefix = ""; /* patch pour forcer le rafra?chissement de l'image sans utiliser le cache du navigateur */
	
	$( document ).ready( function(){ 
		
		$( "#image" ).uploadify({
			
			'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
			'script'		: '/templates/product_management/product.htm.php',
			'folder'		: '/images/products',
			'scriptData'	: { 'idproduct': '<?php echo $tableobj->get( "idproduct" ); ?>', 'index': '0' , 'sid_uploadify' : '<?php echo session_id();?>' },
			'fileDataName'	: 'image',
			'buttonText' 	: "modifier",
			'buttonImg' 	: "/images/back_office/layout/bouttonPhoto.png", 
			'width'			: "115",
			'height'		: "32",
			'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
			'fileDesc'		: "Images",
			'auto'           : true,
			'multi'          : false,
			'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
			'onComplete' 	: function( event, queueID, fileObj, response, data ){

					prefix  = "p" + prefix;
					$( "#ImageThumb" ).attr( "src", response );
					$( "a#ImageThumbFancyBox" ).attr( "href", prefix + $( "a#ImageThumbFancyBox" ).attr( "href" ) );
					
			}

		});

		$("a#ImageThumbFancyBox").fancybox();
        $('.label-file').click(function(){
            $('#image').click();
        });
	});
	
/* ]]> */
</script>


<?php
//---------------------------------------------------------------------------------------------
//========================================
// Gestion photos
//========================================
?>
<div style="float:right; margin-top:30px;">
	<p style="text-align:center; margin-top:4px;">
		<a id="ImageThumbFancyBox" href="<?php echo URLFactory::getProductImageURI( $tableobj->get( "idproduct" ), URLFactory::$IMAGE_MAX_SIZE ); ?>">
			<img id="ImageThumb" src="<?php echo URLFactory::getProductImageURI( $tableobj->get( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, 150, 150 ); ?>" alt="" />
		</a>
		<div style="text-align:center; margin-top:4px;">
        <!--<label for="image" class="label-file orangeButton">Choisir une image</label>-->
			<input type="file" id="image" class="orangeButton" value="ertertertert" style="display:none; width:150px;"/>
        <br/>
			<a href="#" class="blueLink" style="display:block;" onclick="$( '#ProductTabs' ).tabs( 'select', 4 ); return false;">Plus de photos</a>
		</div>
	</p>
</div>

<?php
//---------------------------------------------------------------------------------------------
//========================================
// Caract?ristiques du produit
//========================================
?>
<div style="float:left; width:600px;">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product.htm.php</span>
<?php } ?>

	<div class="subTitleContainer" style="padding-top:10px;">
	    <p class="subTitle">Caracteristiques du produit</p>
	</div>
	<div class="tableContainer">
    
		<table class="dataTable">
<!---->
			 <?php 
			 $idlang=explode("_",$lang);
			$id_lang=$idlang[1];
			$query = "SELECT * FROM language where id_lang=$id_lang";
			$rs =& DBUtil::query( $query );
			$flag= $rs->fields( "flag" );
							?>
<!--<tr>
				<th class="filledCell" style="width:200px;"> Nom lien </th>
				<td>
				<?php if ( $tableobj->get("name_1")  != "Nouveau produit"  ){  ?>
				<input type="text" name="name_1" value="<?php echo  $tableobj->get("name_1") ; ?>" class="textInput" style="width:270px;" readonly />
				<?php }else { ?>
				<input type="text" name="name_1" value="<?php echo  $tableobj->get("name_1") ; ?>" class="textInput" style="width:270px;" />
				<?php } ?>
                </td>
			
            </tr>
-->
              
					<?php
                    
                    include_once( dirname( __FILE__ ) . "/../../objects/URLFactory.php" );
                    $url = URLFactory::getProductURL( $tableobj->get("idproduct") );
                    
                    ?>
                
                <a style="float:right;" href="<?php echo $url; ?>" >Fiche produit</a>
                </td>
			
            </tr>
            

<!-- <tr>
            	<th class="filledCell" style="width:200px;">Nom affich? &nbsp; <?php if($lang!='_1') {?><img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/Fr.png" style="width: 15px;
height: 10px;"><?php } ?></th>
            	
                <td>
                <?php
				
				$idprod = $tableobj->get('idproduct');
				$name_link = DBUtil::getDBValue( "name_link", "product", "idproduct", $idprod );
				
				?>
                <input type="text" name="name_link" value="<?php echo  $name_link ; ?>" class="textInput" style="width:270px;"  />
                </td>
            
            </tr>
-->





		<tr>
				<th class="filledCell" style="width:200px;">Nom <?php if($lang!='_1') {?> <img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/<?php echo  $flag; ?>" style="width: 15px;
height: 10px;"><?php } ?>  </th>
				<td>

				<?php if (( $tableobj->get("name$lang")  != "Nouveau produit"  )&&($lang=='_1')){  ?>
					<?php if ($disabled) { ?>
						<input type="text" name="name<?php echo $lang;?>" value="<?php echo  $tableobj->get("name$lang") ; ?>" class="textInput" style="width:240px;" readonly />
					<?php }else{ ?>
						<input type="text" name="name<?php echo $lang;?>" value="<?php echo  $tableobj->get("name$lang") ; ?>" class="textInput" style="width:240px;" />
					<?php } ?>
				<?php }else { ?>
					<input type="text" name="name<?php echo $lang;?>" value="<?php echo  $tableobj->get("name$lang") ; ?>" class="textInput" style="width:240px;" />
				<?php } ?><?php echo $disabled ? '<span title="Vous devez modifier les redirections existantes pour cr&eacute;er une redirection">' : ''; ?>
				<?php if ($disabled) { ?>?
				<?php }else{ ?><input type="checkbox" name="choix1" value="1">
				<?php } ?>
				(cocher si redirection)<?php echo $disabled ? '</span>' : ''; ?>
				</td>
			  
			
            </tr>
			
<!---->

           <tr>
	    <th class="filledCell">Sous-titre</th>
		<td><input type="text" name="name_secondary_1" value="<?php echo $tableobj->get("name_secondary_1"); ?>" class="textInput"  style="width:240px;" /></td>
</tr>
            
			<tr>
				<th class="filledCell">Categorie</th>
				<td>
					<a style="float:right;" href="#" onclick="catset('myform'); return false;">Modifier</a>
			   		<?php   $can = DBUtil::getDBValue('name'.$lang,'category',"idcategory",$tableobj->get("idcategory")); ?>
			   		<input type="hidden" name="idcategory" value="<?php echo $tableobj->get("idcategory") ?>" class="textInput" />
			   		<input type="text" name="idcategoryname" value="<?php echo $can ?>" readonly="readonly" class="textInput" style="width:300px; border-style:none;" />
				</td>
			</tr>
			<tr>
		    	<th class="filledCell">Fournisseur</th>
		    	<td style="text-align:left; vertical-align:top;">
		    		<?php

		    			$idsupplier = $tableobj->get( "idsupplier" );
		    			if( $idsupplier ){

		    				$array = base64_encode( serialize( array( "idsupplier" => $tableobj->get( "idsupplier" ) ) ) );

		    				?>
		    				<a href="<?php echo $GLOBAL_START_URL ?>/product_management/enterprise/supplier.php?idsupplier=<?php echo $tableobj->get( "idsupplier" ); ?>" onclick="window.open( this.href ); return false;" style="float:right;">
		    					Acc&egrave;s fiche fournisseur
		    				</a>
		    				<?php

		    			}

		    		?>
		    		<select name="idsupplier">
		    			<option value="">-</option>
			    		<?php

			    			$rs =& DBUtil::query( "SELECT idsupplier, name FROM supplier ORDER BY name ASC" );
			    			while( !$rs->EOF() ){

			    				$selected = $rs->fields( "idsupplier" ) == $tableobj->get( "idsupplier" ) ? " selected=\"selected\"" : "";

			    				?>
			    				<option value="<?php echo $rs->fields( "idsupplier" ) ?>"<?php echo $selected ?>><?php echo  $rs->fields( "name" )  ?></option>
			    				<?php

			    				$rs->MoveNext();

			    			}

		    		?>
		    		</select>
		    	</td>
		   </tr>
		   <tr>
		 		  <th class="filledCell">Marque</th>
				<td><?php $tableobj->SelectField("idbrand",'modify',''); ?></td>
		   </tr>
<tr>
		   		<th class="filledCell">Afficher le produit en ligne</th>
		   		<td>
					<input type="radio" name="available" value="1"<?php if( $tableobj->get( "available" ) ) echo " checked=\"checked\""; ?> /> oui
		   			<input type="radio" name="available" value="0"<?php if( !$tableobj->get( "available" ) ) echo " checked=\"checked\""; ?> /> non
					
					<span style="float:right";><strong>Service</strong>
		   		
		   			<input type="radio" name="service" value="1"<?php if( $tableobj->get( "service" ) ) echo " checked=\"checked\""; ?> /> oui
		   			<input type="radio" name="service" value="0"<?php if( !$tableobj->get( "service" ) ) echo " checked=\"checked\""; ?> /> non</span>
		    	</td>
		      <tr>
		   		<th class="filledCell" width="200">Date de creation</th>
		    	<td>
					<input type="text" name="novelty" id="novelty" value="<?php echo $tableobj->get( "novelty" ); ?>" readonly="readonly" class="calendarInput" />
			 		<?php DHTMLCalendar::calendar( "novelty" ); ?>
		   		</td>
		   	</tr>
			<tr>
	    <th class="filledCell">Code EAN</th>
		<td><input type="text" style="width:100px;" name="code" value="<?php echo $tableobj->get("code"); ?>" class="textInput" /></td>
		</tr><tr>
		<th class="filledCell">Coloris</th>
		<td style="padding-left:4px;">
			<a href="#" onclick="window.open('color_popup.php?idsupplier=<?php echo $tableobj->get('idsupplier') ?>&idproduct=<?php echo $tableobj->get('idproduct') ?>','colorchooser', 'height=120,width=110,resizable=yes,scrollbars=yes,toolbar=no,titlebar=no'); return false;" >
			<img name="couleur" alt="Couleurs" src="/images/back_office/content/colorize.png" width="24" border="0" title="Selection des couleurs" ></a>
		</td>	
	</tr>
			
			<!--youtube-->
			<tr>
				<th class="filledCell" style="width:200px;"><?php echo Dictionnary::translate("Url video Youtube") ?> &nbsp;<?php if($lang!='_1') {?> <img src="<?php echo $GLOBAL_START_URL; ?>/images/flags/<?php echo  $flag; ?>" style="width: 15px;
height: 10px;"><?php } ?>  </th>
				<td>
					<input type="text" name="url_video_youtube" value="<?php echo  $tableobj->get("url_video_youtube") ; ?>" class="textInput" style="width:240px;"/>
				</td>
            </tr>
			<!--fin youtube-->
		</table>
	</div>

	<div class="subTitleContainer">
	    <p class="subTitle">Description</p>
	</div>
	<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
	<?php

		$wysiwyg = new WYSIWYGEditor( "description$lang" );
		$wysiwyg->setHTML( $tableobj->get( "description$lang" ) );
		$wysiwyg->setDimension( 0, 110 );
		$wysiwyg->display();

		?>
		<br style="clear:both;" />
	</div>
</div>
<!--
<div class="subTitleContainer">
	    <p class="subTitle">Description</p>
	</div>
	<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;">
	<?php

		$wysiwyg = new WYSIWYGEditor( "description_2" );
		$wysiwyg->setHTML( $tableobj->get( "description_2" ) );
		$wysiwyg->setDimension( 0, 110 );
		$wysiwyg->display();

		?>
		<br style="clear:both;" />
	</div>
</div>
-->
<div class="clear"></div>
<script type="text/javascript">
	function normalize(str) {
		var map = {
				"?": "a",
				"?": "a",
				"?": "a",
				"?": "c",
				"?": "e",
				"?": "e",
				"?": "e",
				"?": "e",
				"?": "i",
				"?": "i",
				"?": "o",
				"?": "o",
				"?": "u",
				"?": "u",
				"?": "u",
				"?": "oe"
		},
		nonWord = /\W/g,
		mapping = function (c) {
			return map[c] || c; 
		};

		var theWord = str.replace(nonWord, mapping);
		while (theWord.search(/[^0-9a-zA-Z\-]/) !== -1)
			theWord = theWord.replace(/[^0-9a-zA-Z\-]/, '-');
		while (theWord.indexOf('--') !== -1)
			theWord = theWord.replace('--', '-');
		if (theWord.length && theWord.lastIndexOf('-') === theWord.length - 1)
			theWord = theWord.substr(0, theWord.length - 1);
		if (theWord.length && theWord.indexOf('-') === 0)
			theWord = theWord.substr(1, theWord.length - 1);
		return theWord;
	};
	$(document).ready(function(){
		$('#choix2').change(function(){
			if ($(this).is(":checked"))
			{
				$('#block-genere').show();
				$('input[name^="alias"]').attr('readonly', '');
			}
			else
			{
				$('#block-genere').hide();
				$('input[name^="alias"]').attr('readonly', 'readonly');
			}
		});
		$('#genere').click(function(){
			var pName = $('input[name^="name_"]').val();
			gAlias = normalize(pName);
			$('input[name="alias"]').val(gAlias);
		});
	});
</script>


<div class="clear"></div>


<?php
//---------------------------------------------------------------------------------------------
//========================================
// Masques et produits similaires
//========================================
?>

<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Affichage produit</p>
</div>
<div class="tableContainer">
	<table class="dataTable">
			
		<tr>
			<th class="filledCell">Intitules utilises pour selectionner les references</th>
			<td style="text-align:left; vertical-align:top;">
			<script type="text/javascript">
				$( document ).ready( function(){ checkHeadingCombination(); } );
			</script>
			<?php

				$query = "
				SELECT DISTINCT il.idintitule_title, it.intitule_title" . User::getInstance()->getLang2()  ." AS intitule_title
				FROM intitule_link il, intitule_title it
				WHERE il.idproduct = '" . $tableobj->get( "idproduct" ) . "'
				AND il.idintitule_title = it.idintitule_title
				AND il.idarticle <> 0
				GROUP BY il.idintitule_title
				HAVING COUNT( DISTINCT il.idintitule_value ) > 1
				ORDER BY intitule_title ASC";

				$rs =& DBUtil::query( $query );

				if( !$rs->RecordCount() )
					echo "aucun intitule utilisable";
				else echo "<ul style=\"margin:0px; padding:0px;\">";

				$i = 1;
				while( $i < 4 ){
					if( $i > $rs->RecordCount() ){ ?>
						<input type="hidden" name="heading_selector_<?php echo $i; ?>" value="0" />
					<?php } else { ?>
						<li style="margin:2px;">
							<select id="HeadingSelector<?php echo $i; ?>" name="heading_selector_<?php echo $i; ?>" onchange="checkHeadingCombination();">
								<option value="0">Aucun</option>
								<?php
									$rs->MoveFirst();
									while( !$rs->EOF() ){
										$selected = $tableobj->get( "heading_selector_{$i}" ) == $rs->fields( "idintitule_title" ) ? " selected=\"selected\"" : "";

										?>
										<option<?php echo $selected; ?> value="<?php echo $rs->fields( "idintitule_title" ); ?>"><?php echo  $rs->fields( "intitule_title" ) ; ?></option>
										<?php

										$rs->MoveNext();
									}
								?>
							</select>
						</li>
					<?php } 
					$i++;
				}

				if( $rs->RecordCount() ) echo "</ul>";

				?>
				<span id="HeadingCombinationChecker"></span>
			</td>
		</tr>
	
	<tr>
		<th class="filledCell">Label du produit</th>
		<td>
			<span style="float:right;">
				<a href="labels_popup.php?add" onclick="window.open( this.href,'', 'height=500,width=700,resizable=yes,scrollbars=yes,toolbar=no,titlebar=no'); return false;">Ajouter </a>
				/ <a href="labels_popup.php?update" onclick="window.open( this.href,'', 'height=500,width=700,resizable=yes,scrollbars=yes,toolbar=no,titlebar=no'); return false;">Modifier</a>
			</span>
			<select name="idlabel">
				<option value="0"<?php if( $tableobj->get("idlabel") == 0 ) echo " selected=\"selected\""; ?>>Aucun</option>
				<?php
					$rslabels=DBUtil::query( "SELECT idlabel, name FROM product_labels" );
					while(!$rslabels->EOF){
						$labelName 		=	$rslabels->fields("name");
						$idlabel		=	$rslabels->fields("idlabel");
						$selectedLabel 	= 	"";

						if($idlabel == $tableobj->get("idlabel") )
							$selectedLabel = "selected=\"selected\"";

						echo"<option value=\"$idlabel\" $selectedLabel>$labelName</option>";
						$rslabels->MoveNext();
					}
				?>
			</select>
		</td>
	</tr><tr>
		<th class="filledCell">Tarification : Selection famille produits</th>
	    <td><?php $tableobj->SelectField("idproduct_family","modify",$tableobj->get("idproduct_family")); ?></td>
		
	</tr><tr>
		<th class="filledCell">Texte commercial</td>
		<td><input type="text" name="text_commercial_1" value="<?php echo  $tableobj->get("text_commercial_1") ; ?>" class="textInput" style="width:200px;"/></td>
	</tr>

	
		<!--<tr>
			<th class="filledCell">Famille de produits similaires</th>
			<td>
			<?php

				include_once( "$GLOBAL_START_PATH/objects/ui/DXComboBox.php" );
				include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );

				//$tableobj->SelectField("idproduct_similar_1",'modify','');

				XHTMLFactory::createSelectElement( $table = "product_similar", $columnAsValue = "idproduct_similare", $columnAsInnerHTML = "name", $orderByColumn = "name", $selectedValue = $tableobj->get( "idproduct_similar_1" ), $nullValue = "0", $nullInnerHTML = "-", $attributes = " name=\"idproduct_similar_1\" id=\"idproduct_similar_1\" style=\"width:auto;\"" );
	        	DXComboBox::create( "idproduct_similar_1" );

			?>
			</td>
		</tr>-->

	</table>
</div>
<div class="clear"></div>

<p style="float:right;" class="controls">
<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
	if( $akilae_crm == 0 ){ ?>
	<!--	<input type="button" name="" value="Supprimer" class="orangeButton" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce produit?' ) ) document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/index.php?delete&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>';" />-->
	<input type="button" name="" value="Dupliquer" class="orangeButton" onclick="duplicateProduct();" />
	<?php } ?>
	
	
	<input type="submit" name="update" value="Sauvegarder" class="blueButton" />
</p>




<?php
//---------------------------------------------------------------------------------------------
//========================================
// Commentaires chef produit
//========================================
?>		

<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Commentaires chef produit</p>
</div>
<div style="margin-top:10px; border:1px solid #CBCBCB; padding:4px;"> 
	<table class="dataTable">
		<tr>   
			<td>
				<?php $wysiwyg = new WYSIWYGEditor( "comment" ); $wysiwyg->setHTML( $tableobj->get( "comment" ) ); $wysiwyg->display(); ?></td>
		</tr>
	</table>
</div>
<div class="clear"></div>		
	<br style="clear:both;" />



<?php
//---------------------------------------------------------------------------------------------
//========================================
// Gestion des Masques Html
//========================================
?>		

<?php if($tableobj->GetParameterAdmin('display_manage_brands')){ ?>
	<div class="subTitleContainer" style="padding-top:10px;">
	    <p class="subTitle">Gestion des marques</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<tr>
				<th class="filledCell" width="200">Marques</th>
				<td style="text-align:left;"><?php $tableobj->SelectField("idbrand",'modify',''); ?></td>
			</tr>
		</table>
	</div>
	<div class="clear"></div>		
<?php } ?>	





<?php
//---------------------------------------------------------------------------------------------
//========================================
// Gestion des Masques PDF
//========================================
?>		

<?php /* if($tableobj->GetParameterAdmin('display_pdf')){ ?>
	<div class="subTitleContainer" style="padding-top:10px;">
	    <p class="subTitle">Gestion des Masques PDF</p>
	</div>
	<div class="tableContainer">
		<table class="dataTable">
			<th class="filledCell" width="200"><?php echo  Dictionnary::translate("productpdf")  ; ?></th>
			<td>
				<input type="text" size=3 name="productpdf" value="<?php echo $tableobj->get("productpdf"); ?>">
				<a href="javascript:mask_product_pdf()">Masque</a>
			</td>
		</table>
	</div>
	<div class="clear"></div>		
<?php } 
*/
?>	



<div class="clear"></div>	
<style type="text/css">
input[name=alias], input[name=url_video_youtube]{background-color:#ddd!important;}
</style>
<?php

//---------------------------------------------------------------------------------------------

function combinateArray( array $array1, array $array2 ){

	$combinations = array();

	foreach( $array1 as $val1 )
		foreach( $array2 as $val2 )
			$combinations[] = $val1 . "," . $val2;

	return $combinations;

}

//---------------------------------------------------------------------------------------------

function listProductColors( $idproduct ){
	
	$selectedColor = DBUtil::getDBValue( "idcolor", "product", "idproduct", $idproduct );
	
	$rs = DBUtil::query( 
			
		"SELECT c.* 
		FROM color_product cp, color c
		WHERE cp.idcolor = c.idcolor
		AND cp.idproduct = '$idproduct'
		ORDER BY cp.display ASC"
		
	);
			
	if( !$rs->RecordCount() ){
		
		echo "-";
		return;
		
	}
	
	?>
	<style type="text/css">
	
		.dropdown { float:left; }
		.dropdown dd { position:relative; z-index:9999; }
		.dropdown a, .dropdown a:visited { color:#404040; text-decoration:none; outline:none;}
		.dropdown a:hover, .dropdown dt a:hover, .dropdown dt a:focus  { color:#bf1923;}
		.dropdown dt a {background:#fff url(/images/front_office/global/icons-arrow.png) no-repeat scroll right center; display:block; padding-right:20px; border:1px solid #cccccc; width:159px; font-size:12px;}
		.dropdown dt a strong {cursor:pointer; display:block; padding:2px 5px; font-weight:normal; }
		.dropdown dd ul { background:#fff; border:1px solid #cccccc; color:#C5C0B0; display:none; left:0px; padding:5px 0px; position:absolute; top:2px; width:auto; width:100%; list-style:none; height:150px; overflow:auto;}
		.dropdown dd ul li a { padding:5px; display:block; font-size:12px; }
		.dropdown dd ul li a:hover { background-color:#ffffff;}
		.dropdown dd ul li a span, .dropdown a span { vertical-align:middle; }
	
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
        $(document).ready(function() {
            
            $(".dropdown dt a").click(function() {
                $(".dropdown dd ul").toggle();
                return false;
            });
                        
            $(".dropdown dd ul li a").click(function() {
                var text = $(this).html();
                $(".dropdown dt a strong").html(text);
                $(".dropdown dd ul").hide();	

                var tokens = $(this).attr('id').split( "_" );
                $("#idcolor").val( tokens[ 1 ] );
                return false;
            });
                        
            function getSelectedValue(id) {
                return $("#" + id).find("dt a em.value").html();
            }

            //fermeture auto du menu d?roulant d?sactiv?e ( = fait planter ajaxSubmit )
            /*$(document).bind('click', function(e) {
                var $clicked = $(e.target);
                if (! $clicked.parents().hasClass("dropdown"))
                    $(".dropdown dd ul").hide();
                return false;
            });*/

            $("#flagSwitcher").click(function() {
                $(".dropdown img.flag").toggleClass("flagvisibility");
                return false;
            });
            
        });

        function updateDefaultColor(){

        	$.ajax({
    		 	
    			url: "/templates/product_management/product.htm.php?update=default_color&idproduct=<?php echo $idproduct; ?>",
    			async: true,
    			type: "GET",
    			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de recuperer les couleurs" ); },
    		 	success: function( responseText ){

    				$( "#DefaultColor" ).html( responseText );
    				
    			}

    		});
    		
        }
        
    /* ]]> */
    </script>
	<dl class="dropdown">
		<dt>
			<a href="#"><strong><?php 
			
				$label = false;
				while( !$label && !$rs->EOF() ){
					
					if( $selectedColor == $rs->fields( "idcolor" ) )
						$label = "<img class=\"ProductColorImage\" src=\"" . substr( $rs->fields( "image" ), 2 ) . "\" style=\"width:10px; height:10px;\" />&nbsp;" .  $rs->fields( "name" ) ;
						
					$rs->MoveNext();
					
				}
				
				$rs->Move( 0 );
				
				echo $label ? $label : "Aucun"; 
				
			?></strong></a>
		</dt>
		<dd>
			<ul>
				<li>
					<a href="#" id="idcolor_0">Aucun</a>
				</li>
				<?php
				
					while( !$rs->EOF() ){
						
						?>
						<li>
							<a href="#" id="idcolor_<?php echo $rs->fields( "idcolor" ); ?>">
								<img class="ProductColorImage" src="<?php echo substr( $rs->fields( "image" ), 2 ); ?>" style="width:10px; height:10px;" />
								&nbsp;<?php echo  $rs->fields( "name" ) ; ?>
							</a>
						</li>
						<?php
						
						$rs->MoveNext();
						
					}
				
			?>
			</ul>
		</dd>
	</dl>	
	<input type="hidden" name="idcolor" id="idcolor" value="<?php echo $selectedColor; ?>" />
	<?php
			
}

//---------------------------------------------------------------------------------------------

?>
