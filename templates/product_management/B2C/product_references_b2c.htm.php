<?php

define( "IMAGE_TYPE_GIF", 1 );
define( "IMAGE_TYPE_JPEG", 2 );
define( "IMAGE_TYPE_PNG", 3 );

//---------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Codifier.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );


$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );
$langue=User::getInstance()->getLang();
//---------------------------------------------------------------------------------------------

$db =& DBUtil::getConnection();
$uploadErrors = array();

//---------------------------------------------------------------------------------------------
		
//construction de la page

$idproduct = $tableobj->get( "idproduct" );

if( isset( $_GET[ "duplicate" ] ) )
	duplicateReference( $_GET[ "duplicate" ] );
else if( isset( $_POST[ "UpdateReferences" ] ) )
	updateReferences( $idproduct );
	
$references = getReferences( $idproduct );
$headings = getHeadings( $idproduct );

//nombre de r�f�rences par ligne:

$referencePerLine = isset( $_GET[ "ReferencePerLine" ] ) ? intval( $_SESSION[ "ReferencePerLine" ] ) : 2;
$referencePerLine = min( $referencePerLine, count( $references ) );

?>

<style type="text/css">
	
	.column1{
	
		background-color: rgb(255, 255, 255);
			
	}
	
	.column2{
	
		background-color: rgb(255, 255, 255);
			
	}
	

</style>

<?php

$cssEditable = " class=\"textInput\" onclick=\"this.style.borderWidth='1px';\" onblur=\"this.style.borderWidth='0px';\" style=\"border-width:0px;\"";

displayForm( $idproduct, $headings, $references );

//---------------------------------------------------------------------------------------------

function getHeadings( $idproduct ){
	
	global 	$db,
			$langue;
			
	$query = "
	 SELECT DISTINCT( il.idintitule_title ), 
		ifam.idintitule_family, ifam.intitule_family$langue, 
		it.idintitule_title, it.intitule_title$langue
	FROM intitule_link il, 
		intitule_title it,
		intitule_family ifam
	WHERE il.idproduct = '$idproduct'
	AND il.idintitule_title = it.idintitule_title
	AND il.idfamily = ifam.idintitule_family
	ORDER BY il.idfamily ASC, il.family_display ASC, il.title_display ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer les intitul�s et leurs familles" );
		
	$headings = array();
	
	while( !$rs->EOF() ){
		
		//$family 			= $rs->fields( "intitule_family$langue" );
		$idintitule_family 	= $rs->fields( "idintitule_family" );
		$idintitule_title 	= $rs->fields( "idintitule_title" );
		$intitule_title 	= $rs->fields( "intitule_title$langue" );
		
		$headings[ $idintitule_family ][ $idintitule_title ] = $intitule_title;
		
		$rs->MoveNext();
		
	}
	
	return $headings;
	
}

//---------------------------------------------------------------------------------------------

function getReferences( $idproduct ){
	
	global 	$db,
			$langue;
			
	$references = array();
	
	$selectedColumns = array( 
	
		"idarticle", 
		"reference",
		"available",
		"ref_supplier",
		"display_detail_order",
		"buyingcost",
		"sellingcost",
		"suppliercost",
		"ratecost",
		"idvat",
		"code_customhouse",
		"hidecost",
		"franco",
		"weight",
		"delivdelay",
		"stock_level",
		"minstock",
		"unit",
		"lot",
		"min_cde",
		"gencod",
		"overhead_charges",
		"indirect_expenses",
		"allowed_discount_bill",
		"garantee",
		"accept_nommenclature",
		"nomenclature",
		"weight_raw",
		"serial",
		"store",
		"span",
		"site"
	
	);
		
	$query = "
	SELECT " . implode( ", ", $selectedColumns ) . "
	FROM detail
	WHERE idproduct = '$idproduct'
	ORDER BY display_detail_order ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer r�f�rences du produit" );
	
	$i = 0;
	while( !$rs->EOF() ){
		
		$references[ $i ] = array();
		
		foreach( $selectedColumns as $column )
			$references[ $i ][ $column ] = $rs->fields( $column );
		
		$rs->MoveNext();
		
		$i++;
		
	}

	return $references;
	
}

//---------------------------------------------------------------------------------------------

function displayForm( $idproduct, $headings, $references ){

	global	$db,
			$langue,
			$GLOBAL_START_URL,
			$referencePerLine,
			$tableobj;
	
	?>
	<div class="subTitleContainer" style="padding-top:10px;">
        <p class="subTitle">B2C R�f�rences du produit</p>
	</div>
	<?php
	
		global $uploadErrors;
		
		foreach( $uploadErrors as $uploadError )
			echo "<p style=\"color:#FF0000; font-weight:bold; margin:0px;\">" . htmlentities( $uploadError ) . "</p>";
	
	?>
	<input type="hidden" name="UpdateReferences" value="1" />
	<input type="hidden" name="referencePerLine" value="<?php echo $referencePerLine ?>" />
	<?php
	
	if( !count( $references ) ){
	
		?>
		<p style="text-align:center;" class="msg">Aucune r�f�rence cr��e</p>	
		<?php
		
	}
	else{
	
		?>
		<p style="text-align:right;">
			Afficher <select name="referencePerLine" onchange="setReferenceCountPerLine( this );"> 
			<?php

			$i = 1;
			while( $i <= count( $references ) ){
			
				$selected = $i == $referencePerLine ? " selected=\"selected\"" : "";
				
				?>
				<option value="<?php echo $i ?>"<?php echo $selected ?>><?php echo $i ?></option>
				<?php
				
				$i++;
				
			}
			
			?>
			</select> r�f�rence(s) par ligne
		</p>
		<?php
		
	}
	
	$tableCount = $referencePerLine ? ceil( count( $references ) / $referencePerLine ) : 0;
	$i = 0;
	while( $i < $tableCount ){
	
		$start = $i * $referencePerLine;
	
		if( $i )
			echo "
			<br style=\"clear:both;\" />";
			
		displayTable( $i, $idproduct, $headings, array_slice ( $references , $start , $referencePerLine  ) );
			
		$i++;
		
	}
	
?>
<?php
	
}

//---------------------------------------------------------------------------------------------

function displayTable( $tableIndex, $idproduct, $headings, $references ){

	global 	$db,
			$langue,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL,
			$referencePerLine,
			$cssEditable,
			$tableobj;

	//les ordres d'affichage
	
	$rs =& DBUtil::query( "SELECT idarticle, display_detail_order FROM detail WHERE idproduct = '$idproduct' ORDER BY display_detail_order ASC" );
	$display_detail_orders = array();
	
	while( !$rs->EOF() ){
		
		$display_detail_orders[] = $rs->fields( "idarticle" );
		
		$rs->MoveNext();
		
	}

	//les tva
	
	$rs =& DBUtil::query( "SELECT idarticle, idvat FROM detail WHERE idproduct = '$idproduct'" );
	$idvats = array();
	
	while( !$rs->EOF() ){
		
		$idvats[ $rs->fields( "idarticle" ) ] = $rs->fields( "idvat" );
		
		$rs->MoveNext();
		
	}
	
	//affichage du tableau
					
	?>
<div class="tableContainer">
	<table class="dataTable">
		<tr>
			<th class="filledCell"><?php echo htmlentities( Dictionnary::translate( "references" ) ) ?></th>
			<?php

			foreach( $references as $reference ){

				?>
				<th class="filledCell">
				<?php echo $reference[ "reference" ]; ?>
				</th>
				<?php
					
			}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "btn_edit_reference" ) ) ?></td>
			<?php
			
			//liens vers les fiches R�f�rence : faut mettre des machins dans la session :o/
			
			$_SESSION[ "detail.php" ][ "search_select" ] = "WHERE idproduct = '$idproduct'";
			$_SESSION[ "detail.php" ][ "search_order" ] = "ORDER BY A.reference ASC";
			$_SESSION[ "detail.php" ][ "search_startpage" ] = 0;
			
			foreach( $references as $reference ){
				
				$url = "$GLOBAL_START_URL/product_management/catalog/detail.php?idarticle=" . $reference[ "idarticle" ];
			
				?>
				<td>
					<input type="button" class="blueButton" value="Acc�s fiche" onclick="window.open('<?php echo $url ?>', '_blank', 'top=5,left=5,width=1024,height=768,resizable,scrollbars=yes,status=1' );" style="float:right;" />
				</td>
				<?php
					
			}
				
			?>
		</tr>
		<?php
		
		//duplication des r�f�rences
		
		$query = "SELECT paramvalue AS codification FROM parameter_admin WHERE idparameter LIKE 'codification' LIMIT 1";
				
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de r�cup�rer les param�tres de codifications des r�f�rences" );

		$codification = $rs->fields( "codification" );

		if( $codification == "AXESS" || $codification == "EPI" ){
	
			?>
			<tr>
				<td><?php echo htmlentities( Dictionnary::translate( "btn_duplicate_reference" ) ) ?></td>
				<?php
	
					
					foreach( $references as $reference ){
						
						$url = "$GLOBAL_START_URL/product_management/catalog/cat_product.php?idproduct=$idproduct&amp;selectedTab=4&amp;duplicate=" . $reference[ "idarticle" ];
						
						?>
						<td>
							<a href="<?php echo $url ?>">
							<input type="submit" class="blueButton" value="Dupliquer" style="float:right;" />
							
							</a>
						</td>
						<?php
							
					}
					
				?>
			</tr>
			<?php
			
		}
		
		//affichage des intitul�s
		
		displayHeadings( $headings, $references );
		
		?>
		<tr>
			<th class="filledCell" colspan="<?php $colspan = 1 + count( $references ); echo $colspan; ?>" class="empty" style="height:15px;"></td>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "available_display" ) ) ?></td>
			<?php
			
				//r�f�rence disponible � l'affichage
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input type="radio" name="available[<?php echo $reference[ "idarticle" ] ?>]" value="1"<?php if( $reference[ "available" ] ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" />&nbsp;Oui
						<input type="radio" name="available[<?php echo $reference[ "idarticle" ] ?>]" value="0"<?php if( !$reference[ "available" ] ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" />&nbsp;Non
					</td>
					<?php
					
					$i++;
					
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "ref_supplier" ) ) ?></td>
			<?php
			
				//r�f�rence fournisseur
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="ref_supplier[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "ref_supplier" ] ?>" class="textInput" />
					</td>
					<?php
					
					$i++;
					
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "display_detail_order" ) ) ?></td>
			<?php
			
				//ordre d'affichage
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<select name="display_detail_order[<?php echo $reference[ "idarticle" ] ?>]">
						<?php
		
							$i = 1;
							while( $i <= count( $display_detail_orders ) ){
								
								$selected = $display_detail_orders[ $i - 1 ] == $reference[ "idarticle" ] ? " selected=\"selected\"" : "";
								
								?>
								<option value="<?php echo $i ?>"<?php echo $selected ?>><?php echo $i ?></option>
								<?php
								
								$i++;
								
							}
							
						?>
						</select>
					</td>
					<?php
					
					$i++;
					
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "buyingcost" ) ) ?></td>
			<?php
			
				//prix d'achat
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input type="text" name="buyingcost[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "buyingcost" ], 2 ) ?>" onkeypress="return forceUnsignedFloatValue( event );" class="textInput" style="width:50px;" /> �
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td>
				<?php toolTipBox( Dictionnary::translate( "help_hidecost" ), 250, $html = "", $showicon = true ) ?>
				<?php echo htmlentities( Dictionnary::translate( "default_sellingcost" ) ) ?>
			</td>
			<?php
			
				//prix de vente catalogue
				
				$i = 0;
				foreach( $references as $reference ){
					
					$hidecost = $reference[ "hidecost" ];
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input type="hidden" name="hidecost[<?php echo $reference[ "idarticle" ] ?>]" id="hidecost_<?php echo $reference[ "idarticle" ] ?>" value="<?php echo $hidecost ?>" />
						<a href="#" onclick="hidecost( <?php echo $reference[ "idarticle" ] ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/<?php echo $hidecost ? "hidden" : "visible" ?>.gif" alt="" style="border-style:none;" id="hidecostIcon_<?php echo $reference[ "idarticle" ] ?>" style="vertical-align:top;" /></a>&nbsp;
						<input type="text" name="sellingcost[<?php echo $reference[ "idarticle" ] ?>]" id="sellingcost_<?php echo $reference[ "idarticle" ] ?>" value="<?php echo $hidecost ? "Contactez-nous" : round( $reference[ "sellingcost" ], 2 ) ?>"<?php if( $hidecost ) echo " disabled=\"disabled\""; ?> onload="this.default='<?php echo $reference[ "sellingcost" ] ?>';" onkeypress="return forceUnsignedFloatValue( event );" class="textInput" style="width:50px;" />&nbsp;&euro;
						<input type="hidden" id="defaultSellingcost_<?php echo $reference[ "idarticle" ] ?>" value="<?php echo round( $reference[ "sellingcost" ], 2 ) ?>" disabled="disabled" />
					</td>
					<?php
					
					$i++;
					
				}
				
			?>
		</tr>
		<tr>
			<td>Prix de vente TTC</td>
			<?php
			
				//prix de vente TTC
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input type="text" name="sellingcost[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "sellingcost" ], 2 ) ?>" onkeypress="return forceUnsignedFloatValue( event );" class="textInput" style="width:50px;" /> �
						<input type="hidden" name="suppliercost[<?php echo $reference[ "idarticle" ] ?>]" value="0" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td>Marge brute</td>
			<?php
			
				//marge brute
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input type="text" name="ratecost[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "ratecost" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" class="textInput" style="width:50px;" /> �
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "vat" ) ) ?></td>
			<?php
			
				//TVA
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
					<?php
					
						$idarticle = $reference[ "idarticle" ];

						XHTMLFactory::createSelectElement( "tva", "idtva", "name", "name", $reference[ "idvat" ], $nullValue = 0, $nullInnerHTML = "-", $attributes = "name=\"idvat[ $idarticle ]\"" );
					
					?>
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "code_customhouse" ) ) ?></td>
			<?php
			
				//code douane si le fournisseur est �tranger
				
				$localState 	= DBUtil::getDBValue( "paramvalue", "parameter_cat", "idparameter", "default_idstate" );
				$supplierState 	= DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $tableobj->get( "idsupplier" ) );
						
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
					<?php

						if( $localState != $supplierState ){
							
							$idarticle = $reference[ "idarticle" ];
	
							XHTMLFactory::createSelectElement( "code_customhouse", "code_customhouse", "code_customhouse_text", "code_customhouse_text", $reference[ "code_customhouse" ], $nullValue = "", $nullInnerHTML = "-", $attributes = "name=\"code_customhouse[ $idarticle ]\"" );
					
						}
						else{
						
							?>
							<input type="hidden" name="code_customhouse[<?php echo $idarticle ?>]" value="" />
							<?php
								
							echo Dictionnary::translate( "unavailable_costomhouse" );
							
						}
						
					?>
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "allow_reference_franco" ) ) ?></td>
			<?php
			
				//autoriser le franco ou non pour cette r�f�rence
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<select name="franco[<?php echo $reference[ "idarticle" ] ?>]">
							<option value="0"<?php if( $reference[ "franco" ] == 0 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "no" ) ?></option>
							<option value="1"<?php if( $reference[ "franco" ] == 1 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "yes" ) ?></option>
						</select>
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "weight_MKSA" ) ) ?></td>
			<?php
				
				//poids
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="weight[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "weight" ] ?>" onkeypress="return forceUnsignedFloatValue( event );" class="textInput" />
					</td>
					<?php
					
					$i++;
					
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "delivdelay" ) ) ?></td>
			<?php
			
				//delais de livraison
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<?php listDelivDelays( $reference[ "idarticle" ], $reference[ "delivdelay" ] ) ?>
					</td>
					<?php
					
					$i++;
						
				}
				
			?>
		</tr>
		<tr>
			<td>
				<?php echo htmlentities( Dictionnary::translate( "stock_level" ) ) ?>
			</td>
			<?php
			
				//stock_level : quantit� en stock ou -1 si on ne g�re pas le stock pour cette r�f�rence
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					$defaultValue = $reference[ "stock_level" ] != -1 ? $reference[ "stock_level" ] : 1;
					
					?>
					<td>
						<ul style="list-style-type:none; display:inline;">
							<li style="float:left; padding-left:5px;">
								<input type="radio" name="stock_management_<?php echo $reference[ "idarticle" ] ?>" value="0"<?php if( $reference[ "stock_level" ] == -1 ) echo " checked=\"checked\""; ?> onclick="var element = document.getElementById( 'stock_level_<?php echo $reference[ "idarticle" ] ?>' ).style.display = 'none'; element.value = '-1';" style="vertical-align:bottom;" /> Non g�r�
							</li>
							<li style="float:left; padding-left:5px;">
								<input type="radio" name="stock_management_<?php echo $reference[ "idarticle" ] ?>" value="1"<?php if( $reference[ "stock_level" ] != -1 ) echo " checked=\"checked\""; ?> onclick="var element = document.getElementById( 'stock_level_<?php echo $reference[ "idarticle" ] ?>' ); element.style.display = 'block'; element.value = '<?php echo $defaultValue ?>'; element.style.color= <?php echo $defaultValue ?> ? '#000000' : '#FF0000';" style="vertical-align:bottom;" /> G�r�
							</li>
							<li style="float:left; padding-left:5px;">
								<input class="editedField" type="text" id="stock_level_<?php echo $reference[ "idarticle" ] ?>" name="stock_level[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "stock_level" ] ?>" style="width:20px; display:<?php echo $reference[ "stock_level" ] == -1 ? "none" : "block"; ?>;<?php if( !$reference[ "stock_level" ] ) echo " color:#FF0000;"; ?>" onkeypress="return forceIntegerValue( event );" class="textInput" />
							</li>
						</ul>
					</td>
					<?php
					
					$i++;
						
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "minstock" ) ) ?></td>
			<?php
			
				//minstock : stock minimum � partir duquel la r�f�rence appara�t dans la gestion du stock
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="minstock[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "minstock" ] ?>" onkeypress="return forceUnsignedIntegerValue( event );" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "units" ) ) ?></td>
			<?php
			
				//quantit� par lot
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td class="characValue <?php echo $class ?>" style="white-space:nowrap;">
						<input type="text" name="lot[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "lot" ] ?>" style="text-align:right; width:25px;" onkeypress="return forceUnsignedIntegerValue();" class="textInput" />
						&nbsp;/&nbsp;<?php listUnits( $reference[ "idarticle" ], $reference[ "unit" ] ) ?>
					</td>
					<?php
					
					$i++;
						
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "min_cde" ) ) ?></td>
			<?php
			
				//minimum de commande
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="min_cde[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "min_cde" ] ?>" onkeypress="return forceUnsignedIntegerValue( event );" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "gencod" ) ) ?></td>
			<?php
			
				//code barre
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="gencod[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo $reference[ "gencod" ] ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "overhead_charges" ) ) ?></td>
			<?php
			
				//frais g�n�raux
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="overhead_charges[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "overhead_charges" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "indirect_expenses" ) ) ?></td>
			<?php
			
				//% de co�ts indirects
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="indirect_expenses[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "indirect_expenses" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "allowed_discount_bill" ) ) ?></td>
			<?php
			
				//Remise facture autoris�e
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="allowed_discount_bill[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "allowed_discount_bill" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "garantee" ) ) ?></td>
			<?php
			
				//Garantie
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="garantee[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo htmlentities( $reference[ "garantee" ] ) ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "accept_nommenclature" ) ) ?></td>
			<?php
			
				//Accepte la nommenclature
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input type="radio" name="accept_nommenclature[<?php echo $reference[ "idarticle" ] ?>]" value="1"<?php if( $reference[ "accept_nommenclature" ] ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
						<input type="radio" name="accept_nommenclature[<?php echo $reference[ "idarticle" ] ?>]" value="0"<?php if( !$reference[ "accept_nommenclature" ] ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "nomenclature" ) ) ?></td>
			<?php
			
				//Nommenclature
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="nomenclature[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo htmlentities( $reference[ "nomenclature" ] ) ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "weight_raw" ) ) ?></td>
			<?php
			
				//Poids brut
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="weight_raw[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo round( $reference[ "weight_raw" ], 2 ) ?>" onkeypress="return forceFloatValue( event );" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "serial" ) ) ?></td>
			<?php
			
				//n� s�rie
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="serial[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo htmlentities( $reference[ "serial" ] ) ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "store" ) ) ?></td>
			<?php
			
				//d�p�t
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="store[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo htmlentities( $reference[ "store" ] ) ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "span" ) ) ?></td>
			<?php
			
				//Rayonnage	
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="span[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo htmlentities( $reference[ "span" ] ) ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
		<tr>
			<td><?php echo htmlentities( Dictionnary::translate( "ref_site" ) ) ?></td>
			<?php
			
				//Emplacement	
				
				$i = 0;
				foreach( $references as $reference ){
					
					$class = $i % 2 ? "column1" : "column2";
					
					?>
					<td>
						<input<?php echo $cssEditable ?> type="text" name="site[<?php echo $reference[ "idarticle" ] ?>]" value="<?php echo htmlentities( $reference[ "site" ] ) ?>" class="textInput" />
					</td>
					<?php
				
					$i++;
							
				}
				
			?>
		</tr>
	</table>
</div>
	<?php
	
}

//---------------------------------------------------------------------------------------------

function displayHeadings( &$headings, &$references ){
	
	global $cssEditable;
	
	//$headings[ $idintitule_family ][ $idintitule_title ] = $intitule_title;
	
	foreach( $headings as $idintitule_family => $intitule_titles ){
		
		?>
		<tr>
			<th class="filledCell" colspan="<?php echo $colspan = count( $references ) + 1; echo $colspan; ?>" style="font-weight:bold;">
				<?php echo htmlentities( getHeadingFamilyLabel( $idintitule_family ) ); ?>
			</th>
		</tr>
		<?php
		
		foreach( $intitule_titles as $idintitule_title => $intitule_title ){
			
			?>
			<tr>
				<td>
					<?php echo htmlentities( $intitule_title ) ?>
				</td>
				<?php
					
				$i = 0;
				foreach( $references as $reference ){
				
					$idarticle = $reference[ "idarticle" ];
					$idintitule_value = getIDIntituleValue( $idarticle, $idintitule_title );

					$class = $i % 2 ? "column1" : "column2";
					$inputClass = $i % 2 ? "input1" : "input2";
					
					if( $idintitule_value ){ //modification d'une valeur d'intitul� existante
					
						$intitule_value = getIntituleValue( $idintitule_value );
						
						?>
						<td class="characValue <?php echo $class ?>">
							<input<?php echo $cssEditable ?> type="text" name="intitule_values[<?php echo $idarticle ?>,<?php echo $idintitule_title ?>,<?php echo $idintitule_value ?>]" value="<?php echo htmlentities( $intitule_value ) ?>" class="<?php echo $inputClass ?>" class="textInput" />
						</td>
						<?php
						
					}else{ //cr�ation d'une nouvelle valeur d'intitul�
					
						?>
						<td class="characValue <?php echo $class ?>">
							<input<?php echo $cssEditable ?> type="text" name="new_intitule_values[<?php echo $idarticle ?>,<?php echo $idintitule_title ?>]" value="" class="<?php echo $inputClass ?>" class="textInput" />
						</td>
						<?php
					
					}
					
					$i++;
					
				}
				
			?>
			</tr>
			<?php
		
		}
		
	}
	
}

//---------------------------------------------------------------------------------------------

function listDelivDelays( $idarticle, $selection ){
	
	global	$db,
			$langue;
	
	$query = "
	SELECT iddelay,delay$langue AS delay
	FROM delay
	WHERE `available` = 1
	ORDER BY delay$langue ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer la liste des d�lais de livraison" );
		
	?>
	<select name="delivdelay[<?php echo $idarticle ?>]">
	<?php
	
		while( !$rs->EOF() ){
			
			$delay = $rs->fields( "delay" );
			$iddelay = $rs->fields( "iddelay" );
			$selected = $iddelay == $selection ? " selected=\"selected\"" : "";
			
			?>
			<option value="<?php echo $iddelay ?>"<?php echo $selected ?>><?php echo htmlentities( $delay ) ?></option>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</select>
	<?php

}

//---------------------------------------------------------------------------------------------

function listUnits( $idarticle, $selection ){
	
	global	$db,
			$langue;
	
	$query = "
	SELECT idunit, unit$langue AS unit
	FROM unit
	ORDER BY unit$langue ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer la liste des unit�s" );

	?>
	<select name="unit[<?php echo $idarticle ?>]" style="width:auto;">
	<?php
	
	while( !$rs->EOF() ){
		
		$unit = $rs->fields( "unit" );
		$idunit = $rs->fields( "idunit");
		
		$selected = $idunit == $selection ? " selected=\"selected\"" : "";
	
		?>
		<option value="<?php echo $idunit ?>"<?php echo $selected ?>><?php echo htmlentities( $unit ) ?></option>
		<?php

		$rs->MoveNext();
		
	}
	
	?>
	</select>
	<?php
	
}

//---------------------------------------------------------------------------------------------

function getIDIntituleValue( $idarticle, $idintitule_title ){
	
	global $db;
	
	$query = "
	SELECT iv.idintitule_value
	FROM intitule_value iv, intitule_link il
	WHERE il.idarticle = '$idarticle'
	AND il.idintitule_title = '$idintitule_title'
	AND il.idintitule_value = iv.idintitule_value
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer la valeur de l'intitul� '$idintitule_title' pour l'article '$idarticle'" );

	if( !$rs->RecordCount() )
		return 0;
		
	return $rs->fields( "idintitule_value" );
	
}

//---------------------------------------------------------------------------------------------

function getIntituleValue( $idintitule_value ){
	
	global $db, $langue;
	
	if( !$idintitule_value )
		return "";

	$query = "
	SELECT intitule_value$langue AS value
	FROM intitule_value
	WHERE idintitule_value = '$idintitule_value'
	LIMIT 1";
	
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer la valeur de l'intitul� '$idintitule_title' pour l'article '$idarticle'" );
		
	return $rs->fields( "value" );
	
}

//---------------------------------------------------------------------------------------------

function updateReferences( $idproduct ){

	global 	$db,
			$langue;
	
	updateHeadings( $idproduct );
	updateReferencesValues( $idproduct );

	
}

//---------------------------------------------------------------------------------------------

function updateHeadings( $idproduct ){
	
	global 	$db,
			$langue;
	
	//mise � jour de la table `intitule_title`
	
	/*foreach( $_POST[ "intitule_titles" ] as $idintitule_title => $intitule_title ){
		
		$query = "
		UPDATE intitule_title
		SET intitule_title$langue = '$intitule_title'
		WHERE idintitule_title = '$idintitule_title'
		LIMIT 1";

		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de mettre � jour le nom de l'intitul� '$idintitule_title'" );
			
	}*/
	
	//cr�ation des nouvelles valeurs d'intitul�s
	
	if( isset( $_POST[ "new_intitule_values" ] ) )
		createNewHeadingValues( $idproduct );
	
	//mise � jour de la table `intitule_value`

	foreach( $_POST[ "intitule_values" ] as $ids => $intitule_value ){
		
		list( $idarticle, $idintitule_title, $idintitule_value ) = explode( ",", $ids );
		
		//v�rifier s'il y a une modification � faire ou non
		
		//echo "<br /><b>idarticle=$idarticle, idintitule_title=$idintitule_title, idintitule_value=$idintitule_value</b>";
		//echo "<br />Valeur d'intitul� � traiter : $intitule_value";
		//echo "<br />v�rifier s'il y a une modification � faire ou non";
		
		if( !$idintitule_value ){ //la r�f�rence n'a pas de valeur pour cet intitul�
			$old_value = "";
			//echo "<br />la r�f�rence n'a pas de valeur existante pour cet intitul�";	
		}
		else{	
				
			//echo "<br />la r�f�rence a d�j� une valeur existante pour cet intitul�";
			
			$query = "
			SELECT intitule_value$langue AS value
			FROM intitule_value
			WHERE idintitule_value = '$idintitule_value'
			LIMIT 1";

			$rs = $db->Execute( $query );
			
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de r�cup�rer la valeur d'origine pour l'intitul� $query" );
				
			$old_value = $rs->fields( "value" );
			//echo "<br />L'ancienne valeur d'intitul� est : $old_value";	
		}
		
		if( $old_value != $intitule_value ){ //la valeur de l'intitul� doit �tre modifi�e
		
			//echo "<br />L'ancienne valeur d'intitul� doit �tre modifi�e";	
			
			//v�rifier si la nouvelle valeur de l'intitul� existe d�j� ailleurs
			
			//echo "<br />v�rifier si la nouvelle valeur de l'intitul� existe d�j� ailleurs";	
			$query = "
			SELECT idintitule_value AS existing_idintitule_value
			FROM intitule_value
			WHERE intitule_value$langue LIKE '$intitule_value'
			LIMIT 1";

			$rs = $db->Execute( $query );
			
			if( $rs === false )
				die( "Impossible de v�rifier si la nouvelle valeur d'intitul� existe d�j�" );

			if( !$rs->RecordCount() ){ //la nouvelle valeur d'intitul� n'existe nulle part => la cr�er
				//echo "<br >la nouvelle valeur d'intitul� n'existe nulle part : la cr�er";	
				$username = isset( $_SESSION[ "commercialinfo" ] ) ? Util::html_escape( $_SESSION[ "commercialinfo" ][ "login" ] ) : "";
				$new_idintitule_value = getNewIdIntituleValue();
				
				$query = "
				INSERT INTO intitule_value(
					idintitule_value,
					intitule_value$langue,
					lastupdate,
					username
				) VALUES (
					'$new_idintitule_value',
					'$intitule_value',
					NOW(),
					'$username'
				)";
				//echo "<br />La nouvelle valeur d'intitul� '$intitule_value' a �t� cr��e avec le idintitule_value=$intitule_value";
				$rs = $db->Execute( $query );
				
				if( $rs === false )
					die( "Impossible de cr�er une nouvelle valeur d'intitul�" );
					
				$query = "
				UPDATE intitule_link
				SET idintitule_value = '$new_idintitule_value'
				WHERE idarticle = '$idarticle'
				AND idintitule_title = '$idintitule_title'
				LIMIT 1";
	
				$rs = $db->Execute( $query );
				
				if( $rs === false )
					die( "Impossible de valider la nouvelle valeur d'intitul� cr��e" );

				//echo "<br />Mise � jour de intitule_link : " . $db->Affected_Rows() . " ligne modifi�e";
				
			}
			else{ //la nouvelle valeur d'intitul� existe ailleurs => mise � jour
				
				//echo "<br />la nouvelle valeur d'intitul� existe ailleurs => mise � jour";
				$existing_idintitule_value = $rs->fields( "existing_idintitule_value" );	
				
				$query = "
				UPDATE intitule_link
				SET idintitule_value = '$existing_idintitule_value'
				WHERE idarticle = '$idarticle'
				AND idintitule_title = '$idintitule_title'
				LIMIT 1";
				
				$rs = $db->Execute( $query );
				
				if( $rs === false )
					die( "Impossible de r�utiliser une valeur d'intitul� existante" );
				
				//echo "<br />Mise � jour de intitule_link : " . $db->Affected_Rows() . " ligne modifi�e";
				
			}

			//v�rifier si l'ancienne valeur est encore utilis�e, sinon la supprimer
			
			//echo "<br />v�rifier si l'ancienne valeur est encore utilis�e, sinon la supprimer";
				
			$query = "
			SELECT COUNT(*) AS `used`
			FROM intitule_link
			WHERE idintitule_value = '$idintitule_value'";
		
			$rs = $db->Execute( $query );
			
			if( $rs === false || $rs->fields( "used" ) == null )
				die( "Impossible de v�rifier si l'ancienne valeur d'intitul� est encore utilis�e" );
				
			if( !$rs->fields( "used" ) ){ //l'ancienne valeur n'est plus utilis�e => suppression
				//echo "<br />l'ancienne valeur n'est plus utilis�e : suppression";
				$query = "
				DELETE FROM intitule_value
				WHERE idintitule_value = '$idintitule_value'
				LIMIT 1";
			
				$rs = $db->Execute( $query );
			
				if( $rs === false )
					die( "Impossible de supprimer l'ancienne valeur d'intitul� inutilis�e" );
				
			}
			//else echo "<br />l'ancienne valeur est encore utilis�e : pas touche!";
		}
		//else echo "<br />L'ancienne valeur d'intitul� n'a pas besoin d'�tre modifi�e";
		
	}
	
}

//---------------------------------------------------------------------------------------------

function updateReferencesValues( $idproduct ){
	
	global 	$db,
			$langue;
			
			
	//mise � jour de la table `detail`
	
	$columns = array( 
	
		"ref_supplier" 			=> "string",
		"available" 			=> "integer",
		"display_detail_order" 	=> "integer",
		"buyingcost" 			=> "float",
		"sellingcost" 			=> "float",
		"suppliercost" 			=> "float",
		"ratecost" 				=> "float",
		"idvat" 				=> "integer",
		"code_customhouse" 		=> "string",
		"hidecost" 				=> "integer",
		"franco" 				=> "integer",
		"weight" 				=> "float",
		"delivdelay" 			=> "integer",
		"stock_level" 			=> "integer",
		"minstock" 				=> "integer",
		"unit" 					=> "string",
		"lot" 					=> "integer",
		"min_cde" 				=> "integer",
		"gencod"				=> "string",
		"overhead_charges" 		=> "float",
		"indirect_expenses" 	=> "float",
		"allowed_discount_bill" => "float",
		"garantee" 				=> "string",
		"accept_nommenclature" 	=> "integer",
		"nomenclature" 			=> "string",
		"weight_raw" 			=> "float",
		"serial" 				=> "string",
		"store" 				=> "string",
		"span" 					=> "string",
		"site" 					=> "string"
		
	);
	
	foreach( $columns as $column => $type ){
	
		$postData = $_POST[ $column ];
		
		foreach( $postData as $idarticle => $value ){
		
			switch( $type ){
				
				case "float" :
				
					$value = str_replace( ",", ".", $value );
					$value = ereg_replace( "[^0123456789\.\-]+", "", $value );
					break;
					
				case "integer" : 
				
					$value = intval( $value );
					break;
					
				case "string" :
				default :
				
			}
	
			$query = "
			UPDATE detail
			SET `$column` = '" . Util::html_escape( $value ) . "'
			WHERE idarticle = '$idarticle'
			LIMIT 1";

			$rs = $db->Execute( $query );
			
			if( $rs === false )
				die( "Impossible de mettre � jour la colonne '$column' pour l'article '$idarticle'" );

		}
			
	}
	
}
//---------------------------------------------------------------------------------------------

function createNewHeadingValues( $idproduct ){

	foreach( $_POST[ "new_intitule_values" ] as $ids => $intitule_value ){
		
		if( !empty( $intitule_value ) ){
		
			list( $idarticle, $idintitule_title ) = explode( ",", $ids );
			createNewHeadingValue( $idproduct, $idarticle, $idintitule_title, $intitule_value );
			
		}
		
	}
	
}

//---------------------------------------------------------------------------------------------

function createNewHeadingValue( $idproduct, $idarticle, $idintitule_title, $intitule_value ){

	global 	$db,
			$langue;

	$username = isset( $_SESSION[ "commercialinfo" ] ) ? Util::html_escape( $_SESSION[ "commercialinfo" ][ "login" ] ) : "";
	
	//v�rifier si la valeur de l'intitul� existe d�j�
	
	$query = "
	SELECT idintitule_value
	FROM intitule_value
	WHERE intitule_value$langue LIKE '" . Util::html_escape( $intitule_value ) . "'
	LIMIT 1";
	//echo "<br />$query";
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de v�rifier si la valeur '$intitule_value' de l'intitul� '$idintitule_title' existe d�j�" );
		
	if( !$rs->RecordCount() ){ //la valeur de l'intitul� n'existe pas => cr�ation
	
		$idintitule_value = getNewIdIntituleValue();

		$query = "
		INSERT INTO intitule_value(
			idintitule_value,
			intitule_value$langue,
			lastupdate,
			username
		) VALUES (
			'$idintitule_value',
			'" . Util::html_escape( $intitule_value ) . "',
			NOW(),
			'$username'
		)";
		//echo "<br />$query";
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de cr�er la nouvelle valeur d'intitul�" );
			
	}
	else{ //la valeur de l'intitul� existe d�j� => r�cup�ration
	
		$idintitule_value = $rs->fields( "idintitule_value" );
		
	}
	
	//lier la r�f�rence � l'intitul�
	
	$title_display = getTitleDisplay( $idproduct, $idintitule_title );
	$reference = getArticleReference( $idarticle );
	
	$query = "
	INSERT INTO intitule_link(
		idarticle,
		idproduct,
		idintitule_title,
		title_display,
		idintitule_value,
		reference,
		lastupdate,
		username
	) VALUES (
		'$idarticle',
		'$idproduct',
		'$idintitule_title',
		'$title_display',
		'$idintitule_value',
		'" . Util::html_escape( $reference ) . "',
		NOW(),
		'$username'		
	)";

	$rs = $db->Execute( $query );
		
	if( $rs === false )
		die( "Impossible de lier la r�f�rence ( idarticle = '$idarticle' ) � la valeur d'intitul� '$idintitule_value'" );
	
}

//---------------------------------------------------------------------------------------------

function getTitleDisplay( $idproduct, $idintitule_title ){

	global 	$db,
			$langue;

	$query = "
	SELECT title_display
	FROM intitule_link
	WHERE idproduct = '$idproduct'
	AND idintitule_title = '$idintitule_title'
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer l'ordre d'affichage pour l'intitul� '$idintitule_title'" );
		
	return $rs->fields( "title_display" );
	
}

//---------------------------------------------------------------------------------------------

function getArticleReference( $idarticle ){

	global 	$db,
			$langue;

	$query = "
	SELECT reference
	FROM detail
	WHERE idarticle = '$idarticle'
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer la r�f�rence du l'article '$idarticle'" );
		
	return $rs->fields( "reference" );
	
}

//---------------------------------------------------------------------------------------------

function getHeadingFamilyLabel( $idintitule_family ){

	global 	$db,
			$langue;
			
	$query = "
	SELECT intitule_family$langue AS intitule_family
	FROM intitule_family 
	WHERE idintitule_family = '$idintitule_family' 
	LIMIT 1";

	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer le nom de la famille d'intitul�s" );

	return $rs->fields( "intitule_family" );
	
}

//---------------------------------------------------------------------------------------------

function getNewIdIntituleValue(){
	
	global 	$db,
			$langue;
			
	$query = "SELECT MAX( idintitule_value ) + 1 AS idintitule_value FROM intitule_value";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer un nouvel identifiant pour les valeurs d'intitul�s" );
	
	$max1 = $rs->fields( "idintitule_value" );
	
	$query = "SELECT MAX( idintitule_value ) + 1 AS idintitule_value FROM intitule_link";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer un nouvel identifiant pour les valeurs d'intitul�s" );
	
	$max2 = $rs->fields( "idintitule_value" );
		
	return max( $max1, $max2 );
	
}

//---------------------------------------------------------------------------------------------

function duplicateReference( $idarticle ){
	
	global 	$db,
			$langue;

	$query = "SELECT * FROM detail WHERE idarticle = '$idarticle' LIMIT 1";
	
	$reference = $db->GetRow( $query );
	
	if( !is_array( $reference ) || !count( $reference ) )
		die( "Impossible de r�cup�rer la r�f�rence � dupliquer" );
	
	$codifier = new Codifier();
		
	$reference[ "reference" ] = $codifier->getNewReference( $reference[ "idproduct" ] );
	$reference[ "idarticle" ] = $codifier->getNewArticle();
	
	$columns = array_keys( $reference );
	$values = array_values( $reference );
	
	$query = "
	INSERT INTO detail( ";

	$i = 0;
	while( $i < count( $columns ) ){
	
		if( $i )
			$query .= ",";
			
		$query .= "`" . $columns[ $i ] . "`";
		
		$i++;
		
	}
	
	$query .= " ) VALUES ( ";
	
	$i = 0;
	while( $i < count( $values ) ){
	
		if( $i )
			$query .= ",";
			
		$query .= "'" . Util::html_escape( $values[ $i ] ) . "'";
		
		$i++;
		
	}
	
	$query .= " )";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de dupliquer la r�f�rence $query" );

	//dupliquer les intitul�s
	
	duplicateHeadings( $idarticle, $reference[ "idarticle" ], $reference[ "reference" ] );
	
}

//---------------------------------------------------------------------------------------------

function duplicateHeadings( $idarticle_source, $idarticle_dest, $reference_dest ){

	
	global 	$db,
			$langue;
			
	$query = "SELECT * FROM intitule_link WHERE idarticle = '$idarticle_source'";
	
	$rs = $db->Execute( $query );
	
	while( $intitule_link = $rs->FetchRow() ){
	
		$intitule_link[ "idarticle" ] = $idarticle_dest;
		$intitule_link[ "reference" ] = $reference_dest;
		
		$columns = array_keys( $intitule_link );
		$values = array_values( $intitule_link );
		
		$query = "
		INSERT INTO intitule_link( ";
	
		$i = 0;
		while( $i < count( $columns ) ){
		
			if( $i )
				$query .= ",";
				
			$query .= "`" . $columns[ $i ] . "`";
			
			$i++;
			
		}
		
		$query .= " ) VALUES ( ";
		
		$i = 0;
		while( $i < count( $values ) ){
		
			if( $i )
				$query .= ",";
				
			$query .= "'" . Util::html_escape( $values[ $i ] ) . "'";
			
			$i++;
			
		}
		
		$query .= " )";
		
		$rs2 = $db->Execute( $query );
		
		if( $rs2 === false )
			die( "Impossible de dupliquer les intitul�s" );
			
	}
	
}

//---------------------------------------------------------------------------------------------

?>