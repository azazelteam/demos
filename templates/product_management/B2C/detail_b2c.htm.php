<?php
/**
 * Formulaire pour la Gestion des articles
 */
 
$langue = User::getInstance()->getLang();

WYSIWYGEditor::init( false );

?>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/core.js"></script>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/events.js"></script>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/css.js"></script>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/coordinates.js"></script>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/drag.js"></script>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/dragsort.js"></script>
								<script language="javaScript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tool-man/cookies.js"></script>
								<script language="javaScript" type="text/javascript">
								<!--
									
									var dragsort = ToolMan.dragsort();
									var junkdrawer = ToolMan.junkdrawer();
								
									window.onload = function() {
									
										var list = document.getElementById( "ReferenceOrderList" );
										
										if( list != null )
											dragsort.makeListSortable( list , verticalOnly, saveOrder);
								
									}
								
									function verticalOnly(item) {
									
										item.toolManDragGroup.verticalOnly();
										
									}
								
									function speak(id, what) {
										
										var element = document.getElementById(id);
										element.innerHTML = 'Clicked ' + what;
									
									}
								
									function saveOrder(item) {
									
										var group = item.toolManDragGroup;
										var list = group.element.parentNode;
										var id = list.getAttribute("id");
										
										if (id == null) 
											return;
											
										group.register('dragend', function() {
										
											ToolMan.cookies().set("list-" + id, junkdrawer.serializeList(list), 365);
										
										});
									
									}
								
									function saveReferenceListOrder(){
									
										var ReferenceOrderList = document.getElementById( 'ReferenceOrderList' );
										
										if( ReferenceOrderList == null )
											return;
											
										var referenceListOrder = ToolMan.junkdrawer().serializeList( ReferenceOrderList );
										
										document.forms.myform.elements[ 'ReferenceListOrder' ].value = referenceListOrder;
										
									}
									
									function displayOrderEditor(){
									
										var editor = document.getElementById( 'ReferenceOrderList' );
										var text = document.getElementById( 'ReferenceOrderListText' );
										
										editor.style.display = editor.style.display == 'block' ? 'none' : 'block';
										text.innerHTML = editor.style.display == 'block' ? "Masquer" : "Afficher";
										
									}
									
									// -->
								</script>
								<style type="text/css">
								<!--
								div.subContent table.dataTable span.mceEditor table.mceLayout td{
									
									border-width:1px;
									padding:0px;
									
								}
								
								
								div.subContent table.dataTable span.mceEditor table.mceLayout tr.mceFirst .mceToolbar td{
									
									border-style:none;
									
								}
								-->
								</style>
								<div class="tableContainer">
									<table class="dataTable">
										<tr>
											<th class="filledCell">R�f�rence</th>
											<td<?php echo !DBUtil::getParameterAdmin( "display_n_article" ) ? " colspan=\"3\"" : "" ?>>
												<input text="text" name="reference" value="<?php if( $paramId == "AXESS" || $paramId == "EPI" || $paramId == "MIM" || $modify ) echo $tableobj->get( "reference" ) ?>" maxlength="20" class="textInput" style="width:125px;" <?php if( $paramId == 'AXESS' || $paramId == 'EPI' ) echo ' readonly="readonly"'; ?> />
											</td>
										<?php if( DBUtil::getParameterAdmin( "display_n_article" ) ){ ?>
									    	<th class="filledCell">Article n�</th>
											<td>
												<input type="text" name="n_article" value="<?php echo $tableobj->get( "n_article" ) ?>" class="textInput" />
											</td>
									 	<?php } ?>
										</tr>
										<tr>
											<th class="filledCell" style="width:20%;">Fournisseur</th>
									    	<td style="width:30%;">
									    		<?php $tableobj->SelectField( "idsupplier","modify" ); ?>
									    	</td>
											<th class="filledCell" style="width:20%;">R�f�rence fournisseur</th>
											<td style="width:30%;">
												<input type="text" name="ref_supplier" value="<?php echo $tableobj->get( "ref_supplier" ) ?>" class="textInput" style="width:90%;" />
											</td>
										</tr>
										<?php if( DBUtil::getParameterAdmin( "display_des_short" ) ){ ?>
										<tr>
											<th class="filledCell">D�signation courte</th>
											<td colspan="3">
											<input type="text" name="summary_1" value="<?php
											
											if( $tableobj->get( "summary_1" ) != "" ){
												
												echo $tableobj->get( "summary_1" );
												
											}else{
												
												$rs =& DBUtil::query( "SELECT name_1 FROM product WHERE idproduct = '" . $tableobj->get( "idproduct" ) . "'" );
												
												$name_1 = $rs->fields( "name_1" );
												
												echo $name_1;
												
											}
											?>" class="textInput" style="width:95%;" /></td>
										</tr>
										<?php
										
										$Int_TRCount = 1; // already existing name_1
										$Str_FieldPart = "summary";
										
										if( sizeof( $art ) ){
											
											for( $i = 2 ; $i <= 5 ; $i++ ){
												
												if( isset( $art[ $Str_FieldPart ][ $i ] ) ){
													
													$Int_TRCount++;
													
										?>
										<tr>
											<th class="filledCell"><?php echo $art[ $Str_FieldPart ][ $i ] ?></th>
											<td colspan="3">
												<input type="text" name="<?php echo $Str_FieldPart . "_" . $i ?>" class="textInput" />
												<?php echo $tableobj->get( $Str_FieldPart . "_" . $i ) ?>
											</td>
										</tr>
										<?php
													
												}
												
											}
											
										}
										
										?>
										<?php } ?>		
										<?php if( DBUtil::getParameterAdmin( "display_des_long" ) ){ ?>
										<tr>
											<th class="filledCell">D�signation</th>
											<td colspan="3">
											<?php
										
												$wysiwyg = new WYSIWYGEditor( "designation$langue" );
												$wysiwyg->setHTML( $tableobj->get( "designation$langue" ) );
												//$wysiwyg->setDimension( 500, 250 );
												$wysiwyg->display();
												
											?>
											</td>
										</tr>
										<?php
										
										$Int_TRCount = 1; // already existing name_1
										$Str_FieldPart = "designation";
										
										if( sizeof( $art ) ){
											
											for( $i = 2 ; $i <= 5 ; $i++ ){
												
												if ( isset( $art[ $Str_FieldPart ][ $i ] ) ){
													
													$Int_TRCount++;
													
										?>
										<tr>
											<th class="filledCell"><?php echo $art[ $Str_FieldPart ][ $i ] ?></th>
											<td colspan="3">
											<?php
														
														$wysiwyg = new WYSIWYGEditor( $Str_FieldPart . "_" . $i );
														$wysiwyg->setHTML( $tableobj->get( $Str_FieldPart . "_" . $i ) );
														$wysiwyg->setDimension( 500, 250 );
														$wysiwyg->display();
														
											?>
											</td>
										</tr>
										<?php
													
												}
												
											}
											
										}
										
										?>
										<?php } ?>
										<tr>
											<th class="filledCell">Ordre d'affichage</th>
											<td colspan="3">
											<?php
												
												//gestion de l'ordre d'affichage dans la cat�gorie parente
												
												if( isset( $_POST[ "ReferenceListOrder" ] ) && !empty( $_POST[ "ReferenceListOrder" ] ) )
													updateDisplayOrder();
												
												$ret = displayOrderEditor( $tableobj->get( "reference" ) );
												
												if( $ret === false ){
													
													?>
													<input type="text" name="display_detail_order" value="<?php echo $tableobj->get( "display_detail_order ") ?>" class="textInput" style="width:20px;" />
													<?php 
													
													toolTipBox( Dictionnary::translate( "help_display_detail" ), 250, $html = "", $showicon = true );
													
												}
												
											?>	
											</td>
										</tr>
										<tr>
											<th class="filledCell"><?php echo Dictionnary::translate( "available_display" ) ?></th>
											<td colspan="3">
												<input type="radio" name="available" value="1"<?php if( $tableobj->get( "available" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" />&nbsp;Oui
												<input type="radio" name="available" value="0"<?php if( !$tableobj->get( "available" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" />&nbsp;Non
											</td>
										</tr>
									</table>
								</div>
								<div class="subTitleContainer" style="margin-top:10px;">
					                <p class="subTitle">Gestion des prix</p>
								</div>
								<div class="tableContainer">
									<table class="dataTable">
										<tr>
											<th class="filledCell">Prix de vente HT</th>
											<td<?php echo DBUtil::getParameterAdmin( "display_suppliercost" ) ? "" : " colspan=\"3\"" ?>>
												<input type="text" name="sellingcost" value="<?php echo number_format( $tableobj->get( "sellingcost" ), 2, ".", "" ) ?>" class="textInput" style="width:80px;" />
											</td>
											<?php if( DBUtil::getParameterAdmin( "display_suppliercost" ) ){ ?>
											<th class="filledCell">Prix d'achat HT</th>
											<td>
												<input type="text" name="buyingcost" value="<?php echo number_format( $tableobj->get( "buyingcost" ), 2, ".", "" ); ?>" class="textInput" style="width:80px;" />
											</td>
											<?php } ?>
										</tr>
										<tr>
											<th class="filledCell" style="width:20%;">Marge brute</th>
											<td style="width:30%;">
											<?php 
									
												$rough_stroke_amount = $tableobj->get( "sellingcost" ) - $tableobj->get( "buyingcost" );
												echo Util::priceFormat( $rough_stroke_amount );
												
											?>
											 soit 
											<?php
												$rough_stroke_rate = 100.0 * $rough_stroke_amount / $tableobj->get( "sellingcost" );
												echo Util::rateFormat( $rough_stroke_rate );
											?>
											
											</td>
											<th class="filledCell" style="width:20%;">Prix de vente TTC</th>
											<td style="width:30%;">
												<?php echo number_format( $tableobj->get( "sellingcost" ), 2, ".", "" ) ?>
											</td>
										</tr>
										<?php
										
											if( DBUtil::getParameterAdmin( "display_suppliercost" ) ){
												
												?>
												<tr>
													<th class="filledCell">Prix tarif fournisseur</th>
													<td>
														<input type="text" name="suppliercost" value="<?php echo number_format( $tableobj->get( "suppliercost" ), 2, ".", "" ) ?>" class="textInput" style="width:80px;" />
													</td>
													<th class="filledCell">Remise fournisseur (%)</th>
													<td>
													<?php 
											
														$supplier_discount_rate = $tableobj->get( "suppliercost" ) > 0.0 ? ( $tableobj->get( "suppliercost" ) - $tableobj->get( "buyingcost" ) ) * 100.0 / $tableobj->get( "suppliercost" ) : 0.0;
														echo Util::rateFormat( $supplier_discount_rate );
														
													?>
													</td>
												</tr>
												<?php
												
											}
											
										?>
										<tr>
											<th class="filledCell">Franco fournisseur</th>
											<?php $franco_supplier = DBUtil::getDBValue( "franco_supplier", "supplier", "idsupplier", $tableobj->get( "idsupplier" ) ); ?>
											<td colspan="3"><?php echo $franco_supplier > 0.0 ? "� partir de " . Util::priceFormat( $franco_supplier ) : "-" ?></td>
										</tr>
										<tr>
											<td colspan="4" class="tableSeparator"></td>
										</tr>
										<tr>
									    	<th class="filledCell">TVA intracommunautaire</th>
									    	<td><?php $tableobj->SelectField("idvat","modify"); ?></td>
									    	<th class="filledCell">Code douane</th>
											<td>
											<?php
									
												if( DBUtil::getParameter( "default_idstate" ) != DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $product->get( "idsupplier" ) ) )
													$tableobj->SelectField("code_customhouse","modify"); 
												else echo "<input type=\"hidden\" name=\"code_customhouse\" value=\"\" />" . Dictionnary::translate( "unavailable_costomhouse" );
												
											?>
											</td>
										</tr>
										<tr>
										<?php 
										
											if( DBUtil::getParameterAdmin( "display_hidecost" ) ) {
												
												?>		
												<th class="filledCell">Masquer les prix de vente sur le Front Office</th>
												<td>
													<input type="radio" name="hidecost" value="1"<?php if( $tableobj->get( "hidecost" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
													<input type="radio" name="hidecost" value="0"<?php if( !$tableobj->get( "hidecost" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
												</td>
												<?php
												
											}else {
												
												?>
												<td colspan="2"><input type="hidden" name="hidecost" value="<?php echo $tableobj->get( "hidecost" ) ?>" /></td>
												<?php 
												
											}
									
											if( DBUtil::getParameterAdmin( "display_franco_ref" ) ){
												
												?>	
										    	<th class="filledCell">Autoriser le franco de port</th>
												<td>
													<input type="radio" name="franco" value="1"<?php if( $tableobj->get( "franco" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
													<input type="radio" name="franco" value="0"<?php if( !$tableobj->get( "franco" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
												</td>
												<?php
												
											}else{
												
												?>
												<td colspan="2"><input type="hidden" name="franco" value="<?php echo $tableobj->get( "franco" ) ?>" /></td>
												<?php 
												
											}
											
										?>
										</tr>
										<tr>
											<th class="filledCell">Frais g�n�raux</th>
											<td>
												<input type="text" name="overhead_charges" value="<?php echo $tableobj->get( "overhead_charges" ) ?>" class="textInput" style="width:40px;" />
											</td>
											<th class="filledCell">Co�ts indirects (%)</th>
											<td>
												<input type="text" name="indirect_expenses" value="<?php echo $tableobj->get( "indirect_expenses" ) ?>" class="textInput" style="width:40px;" />
											</td>
										</tr>
										<tr>
											<th class="filledCell">Remise facture autoris�e</th>
											<td colspan="3">
												<input type="text" name="allowed_discount_bill" value="<?php echo round( $tableobj->get( "allowed_discount_bill" ), 2 ) ?>" class="textInput" style="width:40px;" />
											</td>
										</tr>
									</table>
								</div>
								<?php
								
								//prix au volume
								
								displayReferenceMultiprices( $tableobj->get( "reference" ) );
								
								?>
								<div class="subTitleContainer" style="margin-top:10px;">
					                <p class="subTitle">Gestion des caract�ristiques</p>
								</div>
								<div class="tableContainer">
									<table class="dataTable">
										<tr>
										    <th class="filledCell" style="width:20%;">Poids net</th>
											<td style="width:30%;"><input type="text" name="weight" value="<?php echo $tableobj->get( "weight" ) ?>" class="textInput" style="width:40px;" /></td>
									 		<th style="width:20%;" class="filledCell">Poids brut</th>
											<td style="width:30%;">
												<input type="text" name="weight_raw" value="<?php echo round( $tableobj->get( "weight_raw" ), 2 ) ?>" class="textInput" style="width:40px;" />
											</td>	
									  	</tr>
									  	<tr>
									    	<th class="filledCell">Unit� de vente</th>
											<td><?php $tableobj->SelectField( "unit", "modify" ); ?></td>
									    	<th class="filledCell">Quantit� par lot</th>
											<td><input type="text" name="lot" value="<?php echo $tableobj->get( "lot" ) ?>" class="textInput" style="width:40px;" /></td>
										</tr>
										<tr>
									    	<th class="filledCell">Accepte la nomenclature</th>
											<td>
												<input type="radio" name="accept_nommenclature" value="1"<?php if( $tableobj->get( "accept_nommenclature" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
												<input type="radio" name="accept_nommenclature" value="0"<?php if( !$tableobj->get( "accept_nommenclature" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
											</td>
									    	<th class="filledCell">Nomenclature</th>
											<td>
												<input type="text" name="nomenclature" value="<?php echo htmlentities( $tableobj->get( "nomenclature" ) ) ?>"<?php if( !$tableobj->get( "accept_nommenclature" ) ) echo " disabled=\"disabled\""; ?> class="textInput" />
											</td>
										</tr>
										<tr>
									    	<th class="filledCell">Garantie</th>
									    	<td>
									    		<input type="text" name="garantee" value="<?php echo htmlentities( $tableobj->get( "garantee" ) ) ?>" class="textInput" />
									    	</td>
									    	<th class="filledCell">D�lais de livraison</th>
									    	<td><?php $tableobj->SelectField( "delivdelay", "modify" ); ?></td>
									    </tr>
										<?php if( DBUtil::getParameterAdmin( "display_min_cde" ) ){ ?>	
									 	<tr>
									    	<th class="filledCell">Minimum de commande</th>
											<td colspan="3"><input type="text" name="min_cde" value="<?php echo $tableobj->get( "min_cde" ) ?>" class="textInput" style="width:40px;" /></td>
										</tr>
										<?php } ?>
										<?php if( DBUtil::getParameterAdmin( "display_table" ) ){ ?>	
									  	<tr>	
									    	<th class="filledCell">Tableau des r�f�rences</th>
											<td colspan="3"><?php listDetailTables( $tableobj->get( "idproduct" ), $tableobj->get( "table" ) ); ?></td>
										</tr>
										<?php } ?>	
									</table>
								</div>
								<div class="subTitleContainer" style="margin-top:10px;">
					                <p class="subTitle">Gestion du stock</p>
								</div>
								<div class="tableContainer">
									<table class="dataTable">
										<tr>
											<th class="filledCell" style="width:20%;">Stock</th>
											<td style="width:30%;">
												<input type="text" name="stock_level" value="<?php echo $tableobj->get( "stock_level" ) ?>" class="textInput" style="width:40px;" />
												<?php toolTipBox( Dictionnary::translate('help_stock'), 250, $html = "", $showicon = true ) ?>
											</td>
											<th class="filledCell" style="width:20%;">Stock minimum</th>
											<td style="width:30%;">
												<input type="text" name="minstock" value="<?php echo $tableobj->get("minstock"); ?>" class="textInput" style="width:40px;" />
												<?php toolTipBox( Dictionnary::translate('help_stock_level'), 250, $html = "", $showicon = true ) ?>
											</td>
										</tr>
										<tr>
											<th class="filledCell">Num�ro de s�rie</th>
											<td<?php echo !DBUtil::getParameterAdmin( "display_gencod" ) ? " colspan=\"3\"" : "" ?>>
												<input type="text" name="serial" value="<?php echo $tableobj->get( "serial" ) ?>" class="textInput" />
											</td>
											<?php 
											
												if( DBUtil::getParameterAdmin( "display_gencod" ) ){
													
													?>	
											    	<th class="filledCell">Gencod</th>
													<td>
														<input type="text" name="gencod" value="<?php echo $tableobj->get( "gencod" ) ?>" class="textInput" />
													</td>
													<?php
													
												}
												
											?>
										</tr>
										<tr>
											<th class="filledCell">All�e</th>
											<td>
												<input type="text" name="store" value="<?php echo $tableobj->get( "store" ) ?>" class="textInput" />
											</td>
											<th class="filledCell">Rayon</th>
											<td>
												<input type="text" name="span" value="<?php echo $tableobj->get( "span" ) ?>" class="textInput" />
											</td>
										</tr>
										<tr>
											<th class="filledCell">Site de stockage</th>
											<td colspan="3">
												<input type="text" name="site" value="<?php echo $tableobj->get( "site" ) ?>" class="textInput" />
											</td>
										</tr>
									</table>
								</div>
								<div class="subTitleContainer" style="margin-top:10px;">
					                <p class="subTitle">Gestion des intitul�s</p>
								</div>
<?php
								
								if( isset( $idproduct ) ){
									
?>
								<div class="tableContainer">
									<table class="dataTable">
<?php
									
									// Affichage des intitules
									
									foreach( $arp as $family ){
										
										$first = 0;
										
										foreach( $family as $key => $title ){
											
											if( $first == 0 ){
												
?>										<tr>
											<th colspan="2" class="filledCell"><?php echo $title[ 0 ] ?></th>
										</tr>
<?php
												
											}
											
											$first++;
											
?>
										<tr>
											<th class="filledCell" style="width:20%;"><?php echo $title[ 1 ] ?></th>
											<td style="width:80%;"><input type="text" name="parm_value_<?php echo $key ?>" value="<?php echo $title[ 2 ] ?>" class="textInput" /></td>
										</tr>
<?php
											
										}
										
									}
									
?>
									</table>
								</div>
<?php
									
								}else{
									
?>
								<div style="margin:15px 0px;">Il n'y a aucun intitul� pour cette cat�gorie</div>
<?php
									
								}
								
								//--------------------------------------------------------------------------------------------------------
								//promotions
								
								$reference = $tableobj->get( "reference" );
								$db =& DBUtil::getConnection();
								
?>
								<?php if( DBUtil::getParameterAdmin( "display_promo_ref" ) ){ ?>
								<div class="subTitleContainer" style="margin-top:10px;">
									<p class="subTitle"><?php echo Dictionnary::translate( "promotion" ) ; ?></p>
								</div>
								<div style="float:right;">
									<a href="#" onclick="window.open( '<?php echo $GLOBAL_START_URL ?>/product_management/catalog/promotions.php?reference=<?php echo $reference ?>', '_blank', 'directories=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, left=' + (self.screen.width-1000)/2 + ', top=' + (self.screen.height-700)/2 + ', width=1000, height=650' ); return false;">
										<input type="button" value="Modifier les promotions" class="blueButton" />
									</a>
								</div>
								<div class="clear"></div>
<?php
									
									$query = "SELECT * FROM promo WHERE reference LIKE '$reference' AND begin_date < NOW() AND end_date > NOW() LIMIT 1";
									$rs = $db->Execute( $query );
									
									if( $rs === false )
										trigger_error( "Impossible de r�cup�rer l'historique des promotions pour la r�f�rence '$reference'", E_USER_ERROR );
									
									if( !$rs->RecordCount() ){
										
?>
								<div style="margin:15px 0px;">Cette r�f�rence n'est pas en promotion actuellement</div>
<?php
										
									}else{
										
										$begin_date = $rs->fields( "begin_date" );
										$end_date 	= $rs->fields( "end_date" );
										$rate 		= $rs->fields( "rate" );
										
?>
								<div style="margin:15px 0px;">
									Cette r�f�rence est en promotion � <b>- <?php echo round( $rate ) ?>%</b> du <b><?php echo humanReadableDatetime( $begin_date ) ?></b> au <b><?php echo humanReadableDatetime( $end_date ) ?></b>.
								</div>
<?php
										
									}
									
								}
								
								//--------------------------------------------------------------------------------------------------------
								//destockage
								
								$reference = $tableobj->get( "reference" );
								$db =& DBUtil::getConnection();
								
?>
								<?php if( DBUtil::getParameterAdmin( "display_destocking_ref" ) ){ ?>	
								<div class="subTitleContainer" style="margin-top:10px;">
									<p class="subTitle">Destockage</p>
								</div>
								<div style="float:right;">
									<a href="#" onclick="window.open( '<?php echo $GLOBAL_START_URL ?>/product_management/catalog/destocking.php?reference=<?php echo $reference ?>', '_blank', 'directories=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, left=' + (self.screen.width-1000)/2 + ', top=' + (self.screen.height-700)/2 + ', width=1000, height=650' ); return false;">
										<input type="button" value="Modifier les destockages" class="blueButton" />
									</a>
								</div>
<?php
									
									$query = "SELECT * FROM destocking WHERE reference LIKE '$reference' AND begin_date < NOW() AND end_date > NOW() LIMIT 1";
									$rs = $db->Execute( $query );
									
									if( $rs === false )
										trigger_error( "Impossible de r�cup�rer l'historique des destockage pour la r�f�rence '$reference'", E_USER_ERROR );
									
									if( !$rs->RecordCount() ){
										
?>
								<div style="margin:15px 0px;">Cette r�f�rence n'est pas en destockage actuellement</div>
<?php
										
									}else{
										
										$begin_date = $rs->fields( "begin_date" );
										$end_date 	= $rs->fields( "end_date" );
										$rate 		= $rs->fields( "rate" );
										
?>
								<div style="margin:15px 0px;">
									Cette r�f�rence est en destockage � <b>- <?php echo round( $rate ) ?>%</b> du <b><?php echo humanReadableDatetime( $begin_date ) ?></b>  jusqu'au <b><?php echo humanReadableDatetime( $end_date ) ?></b>
								</div>
<?php
										
									}
									
								}
								
								//--------------------------------------------------------------------------------------------------------
								//Image
?>
								<?php if( DBUtil::getParameterAdmin( "display_img_ref" ) ){ ?>
								<div class="subTitleContainer" style="margin-top:10px;">
									<p class="subTitle">Image</p>
								</div>
								<div class="tableContainer">
									<table class="dataTable">
									<?php if( !isset( $idproduct ) ){ ?>
										<tr>
											<th>Num�ro de produit</th>
											<td><input type="text" name="idproduct" value="<?php echo $tableobj->get( "idproduct" ) ?>" class="textInput" /></td>
										</tr>
									<?php } ?>
									</table>
								</div>
								<?php } ?>
								<div><span class="asterix">*</span> Champs obligatoires</div>
								<div class="submitButtonContainer">
<?php

if ( $modify ){
	
	$adms->InputField("hidden",'old_reference', $tableobj->get("reference"));//en cas de chg de la r�f.
	
	$codifier = new Codifier();
	
	if( !$codifier->isReferenceGeneratorAvailable() ){
		
		$js = "if( document.forms.myform.elements[ 'reference' ].value == '";
		$js .= $tableobj->get("reference");
		$js .= "' ){ alert( '" . Dictionnary::translate( "cannot_duplicate" ) . "' ); return false; }";

?>
									<input type="submit" class="blueButton" value="Dupliquer" name="append" onclick="<?php echo $js ?>" />
<?php
		
	}else{
		
?>
									<input type="submit" class="blueButton" value="Dupliquer" name="append" />
<?php
		
	}
	
?>
									<input type="submit" class="blueButton" value="Mettre � jour" name="update" />
<?php

}else {
	
?>
									<input type="submit" class="blueButton" value="Valider" name="append" />
<?php
	

}

?>
								</div>
<?php

//-----------------------------------------------------------------------------------------------------

function displayOrderEditor( $reference ){

	global 	$tableobj,
			$langue,
			$GLOBAL_START_URL;
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT idproduct AS idparent FROM detail WHERE reference LIKE '$reference' LIMIT 1";
	
	$rs = $db->Execute( $query );
		
	if( $rs === false  )
		die( "Impossible de r�cup�rer le produit parent $query" );
	
	if( !$rs->RecordCount() )
		return;
		
	$idparent = $rs->fields( "idparent" );
		
	$query = "
	SELECT d.idarticle AS idchild, 
		d.reference,
		d.display_detail_order
	FROM detail d
	WHERE d.idproduct = '$idparent'
	ORDER BY d.display_detail_order ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer la liste des r�f�rences dans le m�me produit $query" );

	if( !$rs->RecordCount() ){
	
		?>
		<input type="hidden" id="ReferenceListOrder" name="ReferenceListOrder" value="" />
		<?php
		
		return;
		
	}
	
	?>
	<style type="text/css">
		
		#ReferenceOrderList {
			
			margin-bottom: 1em;
			
		}
		
		#ReferenceOrderList li{
			
			margin-bottom: 0px;
			margin-top: 4px;
			
		}
		
		ul.boxy {
			
			list-style-type: none;
			padding: 0px;
			margin: 0px;
			width:210px;
			font-size: 13px;
			font-family: Arial, sans-serif;
			
		}
		
		ul.boxy li {
			
			margin: 0px;
			cursor:move;
			padding: 2px 2px;
			border: 1px solid #ccc;
			background-color: #eee;
			
		}

	
	</style>
	<a href="#" onclick="displayOrderEditor(); return false;" class="blueLink">[<span id="ReferenceOrderListText">Afficher</span> le tri]</a>
	<ul id="ReferenceOrderList" class="boxy" style="display:none;">
	<?php
	
		$rs->MoveFirst();
		
		while( !$rs->EOF() ){
			
			$child_reference = $rs->fields( "reference" );
			$idchild = $rs->fields( "idchild" );
			$displayorder = $rs->fields( "display_detail_order" );
			
			$style = $child_reference == $reference ? " style=\"background-color:#FFFFFF;\"" : "";
			
			?>
			<li itemID="<?php echo $idchild ?>"<?php echo $style ?>">
				<?php echo htmlentities( $child_reference ) ?>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<input type="hidden" id="ReferenceListOrder" name="ReferenceListOrder" value="" />
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listDetailTables( $idproduct, $selectedIndex ){

	global $tableobj;
	
	$db =& DBUtil::getConnection();
	
	$query = "
	SELECT table_name_1, table_name_2, table_name_3, table_name_4, table_name_5
	FROM product
	WHERE idproduct = '$idproduct'
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de r�cup�rer la liste des tableaux de r�f�rences" );

	$tableUvailable = false;
	$i = 1;
	while( !$tableUvailable && $i < 6 ){
	
		$table = $rs->fields( "table_name_$i" );
		
		if( !empty( $table ) )
			$tableUvailable = true;
			
		$i++;
		
	}
	
	if( !$tableUvailable ){

		echo "<input type=\"hidden\" name=\"table\" value=\"\" />" . Dictionnary::translate( "unavailable" );
		
		return;
		
	}
	
	?>
	<select name="table">
		<option value="0"></option>
		<?php

		$i = 1;
		while( $i < 6 ){
		
			$selected = $selectedIndex == $i ? " selected=\"selected\"" : "";
			$table = $rs->fields( "table_name_$i" );
			
			if( !empty( $table ) ){
				
				?>
				<option value="<?php echo $i ?>"<?php echo $selected ?>><?php echo htmlentities( $table ) ?></option>
				<?php
			
			}

			$i++;;
			
		}
		
	?>
	</select>
	<?php

}

//-----------------------------------------------------------------------------------------------------

function displayReferenceMultiprices( $reference ){

	$buyingcost = DBUtil::getDBValue( "buyingcost", "detail", "reference", $reference );
	
	$query = "
	SELECT mf.*, bf.name
	FROM multiprice_family mf, buyer_family bf
	WHERE mf.reference LIKE '" . Util::html_escape( $reference ) . "'
	AND mf.end_date > NOW()
	AND mf.idbuyer_family = bf.idbuyer_family
	ORDER BY bf.name ASC, mf.begin_date ASC";
	
	$rs =& DBUtil::query( $query );
	
	$last_family = false;
	if( $rs->RecordCount() ){
		
		?>
		<p style="font-size:11px; font-weight:bold; margin-top:15px; text-decoration:underline;">Prix au volume</p>
		<table class="dataTable resultTable">
		<?php
		
		while( !$rs->EOF() ){
			
			if( $rs->fields( "name" ) != $last_family ){
				
				?>
			<thead>
				<tr>
					<td colspan="6" class="tableSeparator"></td>
				</tr>
				<tr>
					<th colspan="6"><?php echo htmlentities( $rs->fields( "name" ) ) ?></th>
				</tr>
				<tr>
					<th>P�riode du</th>
					<th>au</th>
					<th>Quantit� entre</th>
					<th>et</th>
					<th>Taux de remise</th>
					<th>Prix d'achat</th>
				</tr>
			</thead>
				<?php
				
				$last_family = $rs->fields( "name" );
				
			}
			
			$discountPrice = $buyingcost * ( 1.0 - floatval( $rs->fields( "rate" ) ) / 100.0 );
			
			?>
			<tbody>
				<tr>
					<td class="lefterCol" style="text-align:center;"><?php echo usDate2eu( $rs->fields( "begin_date" ) ) ?></th>
					<td style="text-align:center;"><?php echo usDate2eu( $rs->fields( "end_date" ) ) ?></th>
					<td style="text-align:right;"><?php echo $rs->fields( "quantity_min" ) ?></th>
					<td style="text-align:right;"><?php echo $rs->fields( "quantity_max" ) ?></th>
					<td style="text-align:right;"><?php echo Util::rateFormat( $rs->fields( "rate" ) ) ?></th>
					<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $discountPrice ) ?></th>
				</tr>
			</tbody>
			<?php
			
			$rs->MoveNext();
			
		}
		
		?>
		</table>
		<?php
	
	}	

//-----------------------------------------------------------------------------------------------------

}

?>