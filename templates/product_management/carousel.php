<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affiche un caroussel triable pour les produits ou les sous-catégories d'une catégoie donnée
*/
 
define( "CAROUSEL_THUMB_WIDTH", 50 );
define( "SHOW_HIDDEN_ELEMENTS", false );

include_once( dirname( __FILE__ ) . "/../../config/init.php" );

//--------------------------------------------------------------------------------------------------------
/* tri des sous-catégories et produits */

if( isset( $_POST[ "sort" ] ) ){

	//tri sous-catégories
	
	if( isset( $_POST[ "Categories" ] ) ){
		
		$done = true;
		$i = 0;
		while( $i < count( $_POST[ "Categories" ] ) ){
			
			$idchild = intval( $_POST[ "Categories" ][ $i ] );
			$rs =& DBUtil::query( "UPDATE category_link SET displayorder = '$i' WHERE idcategorychild = '$idchild' LIMIT 1" );

			$done &= $rs !== false;
			
			$i++;
			
		}
		
		exit( $done ? "1" : "0" );
		
	}
	
	//tri produits
	
	if( isset( $_POST[ "Products" ] ) ){
		
		$done = true;
		$i = 0;
		while( $i < count( $_POST[ "Products" ] ) ){
			
			$idchild = intval( $_POST[ "Products" ][ $i ] );
			$rs =& DBUtil::query( "UPDATE product SET display_product_order = '$i' WHERE idproduct = '$idchild' LIMIT 1" );

			$done &= $rs !== false;
			
			$i++;
			
		}
		
		exit( $done ? "1" : "0" );
		
	}
	
	exit( "0" );

}

//--------------------------------------------------------------------------------------------------------
/* affichage du caroussel */

if( isset( $_REQUEST[ "idproduct" ] ) )
	displayProductCarousel( intval( $_REQUEST[ "idproduct" ] ) );
else if(  isset( $_REQUEST[ "idcategory" ] ) )
	displayCategoryCarousel( intval( $_REQUEST[ "idcategory" ] ) );
	
//--------------------------------------------------------------------------------------------------------

function displayProductCarousel( $idproduct ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/productobject.php" );

	$elements = array();
	
	$query = "
	SELECT p1.idproduct, 
		p1.name" . User::getInstance()->getLang() . " AS name,
		p1.available
	FROM product p1, product p2
	WHERE p2.idproduct = '$idproduct'
	AND p2.idcategory = p1.idcategory";
	
	if( !SHOW_HIDDEN_ELEMENTS )
		$query .= " AND p1.available = 1";
	
	$query .= "
	ORDER BY p1.display_product_order ASC";

	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$style = $rs->fields( "available" ) ? "" : " style=\"font-style:italic;\"";
		$html = "<p{$style}>";
		
		$lowerPrice = Product:: getLowerPrice( $rs->fields( "idproduct" ) );

		if( $lowerPrice > 0.0 )
			$html .= "à partir de " . Util::priceFormat( $lowerPrice );
		else $html .= "prix non disponible";
		
		echo "</p>";
		
		$elements[] = ( object )array(

			"id" 	=> "Products_" . $rs->fields( "idproduct" ),
			"image" => URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, CAROUSEL_THUMB_WIDTH, CAROUSEL_THUMB_WIDTH ),
			"title" => htmlentities( $rs->fields( "name" ) ),
			"href" 	=> "$GLOBAL_START_URL/product_management/catalog/cat_product.php?idproduct=" . $rs->fields( "idproduct" ),
			"html" 	=> $html,
			"available" => $rs->fields( "available" ) == 1

		);
		
		$rs->MoveNext();
		
	}
	
	displayElementsCarousel( $elements, "productCarousel" );
	
}

//--------------------------------------------------------------------------------------------------------

function displayCategoryCarousel( $idcategory ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
	$elements = array();
	
	$query = "
	SELECT c.idcategory,
		c.name" . User::getInstance()->getLang() . " AS name,
		c.available
	FROM category c, category_link cl1, category_link cl2
	WHERE cl1.idcategorychild = '$idcategory'
	AND cl1.idcategoryparent = cl2.idcategoryparent
	AND cl2.idcategorychild = c.idcategory";
	
	if( !SHOW_HIDDEN_ELEMENTS )
		$query .= " AND c.available = 1";
		
	$query .= "
	ORDER BY cl1.displayorder ASC";

	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$elements[] = ( object )array(

			"id" 	=> "Categories_" . $rs->fields( "idcategory" ),
			"image" => URLFactory::getCategoryImageURI( $rs->fields( "idcategory" ), URLFactory::$IMAGE_CUSTOM_SIZE, 0, CAROUSEL_THUMB_WIDTH, CAROUSEL_THUMB_WIDTH ),
			"title" => htmlentities( $rs->fields( "name" ) ),
			"href" 	=> "$GLOBAL_START_URL/product_management/catalog/cat_category.php?action=edit&idcategory=" . $rs->fields( "idcategory" ),
			"html" 	=> htmlentities( $rs->fields( "name" ) ),
			"available" => $rs->fields( "available" ) == 1

		);
		
		$rs->MoveNext();
		
	}
	
	displayElementsCarousel( $elements, "categoryCarousel" );
	
}

//--------------------------------------------------------------------------------------------------------

function displayElementsCarousel( &$elements, $elementID ){
	
	global $GLOBAL_START_URL;
	
	?>
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL; ?>/js/jcarousel/lib/jquery.jcarousel.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL; ?>/js/jcarousel/skins/tango2/skin.css" />
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jcarousel/lib/jquery-1.2.1.pack.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jcarousel/lib/jquery.jcarousel.pack.js"></script>
	<style type="text/css">

		/**
		 * Overwrite for having a carousel with dynamic width.
		 */
		.jcarousel-skin-tango .jcarousel-container-horizontal {
		    width: 85%;
		}
		
		.jcarousel-skin-tango .jcarousel-clip-horizontal {
		    width: 100%;
		}
	
		.jcarousel-skin-tango .itemThumb{
			border:1px solid #E7E7E7;
			cursor:move;
		}
		
		.jcarousel-skin-tango .placeholder{
			border:1px solid #E7E7E7;
			width:<?php echo CAROUSEL_THUMB_WIDTH ?>px;
			height:<?php echo CAROUSEL_THUMB_WIDTH ?>px;
			background-color:#F3F3F3;
		}
		
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------------ */
	
		/* tri des sous-catégories et produits */
	
		function sortCarouselItems( serializedString ){
			
			var data = "sort=1&" + serializedString;
	
			$.ajax({
	
				url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/carousel.php",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
			 	success: function( responseText ){
	
					if( responseText == "1" ){
					
						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );
	        	
	        		}
	        		else alert( "Impossible d'enregistrer les modifications" + responseText );
	        		
				}
	
			});
			
		}
		
		/* ------------------------------------------------------------------------------------------ */
	
		$(document).ready(function(){
			
		    $('#<?php echo $elementID; ?>').jcarousel({
		    	
		        scroll:3,
		        size:<?php echo count( $elements ); ?>,
				visible:<?php echo min( count( $elements ), 6 ); ?>
		    
		    });
		    
		    /* sortable */

			$("#<?php echo $elementID; ?>").sortable({
			
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function(event, ui){ sortCarouselItems( $("#<?php echo $elementID; ?>").sortable('serialize') ); }
			
			});
			
			$("#<?php echo $elementID; ?>").disableSelection();
				
		    
		});
		
	/* ]]> */
	</script>
	
    <?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/carousel.php</span>
<?php } ?>

	<ul id="<?php echo $elementID; ?>" class="jcarousel-skin-tango">
	<?php
	
		foreach( $elements as $element ){

			?>
			<li id="<?php echo $element->id; ?>" style="text-align:center;">
				<a href="<?php echo $element->href; ?>" title="<?php echo $element->title; ?>">
					<img class="itemThumb" src="<?php echo $element->image; ?>" alt=""<?php if( !$element->available ) echo " style=\"opacity:0.4; filter:alpha(opacity=40);\""; ?> />
				</a>
				<br />
				<?php echo $element->html; ?>
			</li>
			<?php
			
		}
		
	?>
	</ul>
	<?php
	
}

//--------------------------------------------------------------------------------------------------------

?>