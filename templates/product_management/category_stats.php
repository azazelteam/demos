<?php

include_once( dirname( __FILE__ ) . "/../../config/init.php" );

/* ------------------------------------------------------------------------------------------------- */
/* nombre de devis - ajax */

if( isset( $_REQUEST[ "estimate_count" ] ) && isset( $_REQUEST[ "idproduct" ] ) && intval( $_REQUEST[ "idproduct" ] ) )
	exit( strval( getProductEstimateCount( intval( $_REQUEST[ "idproduct" ] ), $_REQUEST[ "start_date" ], $_REQUEST[ "end_date" ] ) ) );
elseif( isset( $_REQUEST[ "estimate_count" ] ) && isset( $_REQUEST[ "idcategory" ] ) && intval( $_REQUEST[ "idcategory" ] ) )
	exit( strval( getCategoryEstimateCount( intval( $_REQUEST[ "idcategory" ] ), $_REQUEST[ "start_date" ], $_REQUEST[ "end_date" ] ) ) );
	
/* ------------------------------------------------------------------------------------------------- */
	
header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );
header( "Content-Type: text/html; charset=utf-8" );

//$start_date = isset( $_REQUEST[ "start_date" ] ) ? $_REQUEST[ "start_date" ] : date( "Y-m-d", mktime( 0, 0, 0, date( "m" ) - 1, 1, date( "Y" ) ) );
//$end_date 	= isset( $_REQUEST[ "end_date" ] ) ? $_REQUEST[ "end_date" ] : date( "Y-m-d" );
$start_date = isset( $_REQUEST[ "start_date" ] ) ? $_REQUEST[ "start_date" ] : date( "d-m-Y", mktime( 0, 0, 0, date( "m" ) - 1, 1, date( "Y" ) ) );
$end_date 	= isset( $_REQUEST[ "end_date" ] ) ? $_REQUEST[ "end_date" ] : date( "d-m-Y" );


include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );

DHTMLCalendar::calendar( "StartDate" );
DHTMLCalendar::calendar( "EndDate" );

$root = createTreeNode();
$root->idcategory = intval( $_REQUEST[ "idcategory" ] );

getChildren( $root );
getTreeNodeStats( $root, $start_date, $end_date );


/* produits */
/*
if( !$rs->RecordCount() ){

	$rs =& DBUtil::query( "SELECT idproduct, name_1 AS name FROM product WHERE idcategory = '" . intval( $_REQUEST[ "idcategory" ] ) . "'" );
	
	while( !$rs->EOF() ){
	
		array_push( $categories, array( 
		
			"idproduct" 		=> $rs->fields( "idproduct" ),
			"idcategory" 		=> intval( $_REQUEST[ "idcategory" ] ),
			"name" 				=> $rs->fields( "name" ),
			"turnoverQuantity" 	=> 0,
			"turnoverAmount" 	=> 0.0,
			"purchaseQuantity" 	=> 0,
			"purchaseAmount" 	=> 0.0,
			"estimateCount" 	=> 0//getProductEstimateCount( $rs->fields( "idproduct" ), $start_date, $end_date )
		
		) );
		
		$rs->MoveNext();
		
	}
	
}
*/
?>
<style type="text/css">

	table.sortable th.header{ 
	
	    background-image: url(/images/small_sort.gif);     
	    cursor: pointer; 
	    font-weight: bold; 
	    background-repeat: no-repeat; 
	    background-position: 50% 85%; 
	    padding-left: 20px; 
	    border-right: 1px solid #dad9c7; 
	    margin-left: -1px; 
	    padding-bottom:15px;
	    text-align:center;
	    height:auto;
	
	}
	
	table.sortable th.headerSortUp{ background-image: url(/images/small_asc.gif); } 
	table.sortable th.headerSortDown{ background-image: url(/images/small_desc.gif); } 
	
	table.statTable{ 
	
		width: 100%;
		border-collapse: collapse;
		border:1px solid #E7E7E7;
		
	}

	.statTable th{ background-color:#F3F3F3; }
	
	.statTable th, .statTable td {
	
		border:1px solid #E7E7E7;
		text-align:center;	
		height:24px;
		white-space:nowrap;
		
	}

</style>
<script type="text/javascript" src="/js/jquery/jquery.tablesorter.min.js"></script>
<script type="text/javascript">
/* <![CDATA[ */

	/* ------------------------------------------------------------------------------------------------- */
	
	$( document ).ready( function(){

		$( "table.sortable" ).tablesorter({
	      	
	      	headers: { 
	
				0: {  sorter: false },
				1: {  sorter:'digit' },
				2: {  sorter:'text' },
				3: {  sorter:'digit' },
				4: {  sorter:'digit' },
				5: {  sorter:'digit' },
				6: {  sorter:'currency' },
				7: {  sorter:'currency' },
				8: {  sorter:'digit' },
				9: {  sorter:'currency' }
	      		
			} 
				        
		});
		
	});
             
	/* ------------------------------------------------------------------------------------------------- */
	
	function updateStats( idcategory ){

		var reg = new RegExp( "[0-9]{2}-[0-9]{2}-[0-9]{4}", "g" );

		if( !reg.test( $( "#StartDate" ).val() ) ){

			alert( "Le format de la date de début est incorrect" );
			return;
			
		}

		var reg = new RegExp( "[0-9]{2}-[0-9]{2}-[0-9]{4}", "g" );
		
		if( !reg.test( $( "#EndDate" ).val() ) ){

			alert( "Le format de la date de fin est incorrect" );
			return;
			
		}

		$( "#TurnoverDiv" ).html( '<p style="text-align:center; margin:15px;"><img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...</p>' );

		var start_date 	= $( "#StartDate" ).val().split( "-" );
		var end_date 	= $( "#EndDate" ).val().split( "-" );

		var data = "start_date=" + start_date[ 2 ] + "-" + start_date[ 1 ] + "-" + start_date[ 0 ];
		data 	+= "&end_date=" + end_date[ 2 ] + "-" + end_date[ 1 ] + "-" + end_date[ 0 ];

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/category_stats.php?ajax&idcategory=" + idcategory,
			async: true,
			type: "GET",
			data: data,
			error : function( xhr, textStatus, errorThrown ){ alert(xhr.responseText);},
		 	success: function( responseText ){
                $( "#TurnoverDiv" ).html( responseText );
				$( "#StatTable" ).html( responseText );
			}
		});
		
	}

	/* ------------------------------------------------------------------------------------------------- */
	
/* ]]> */
</script>
<?php  $cat_story_file =  dirname( __FILE__ )."/../../akilae_pro/cat_story.php"; ?>
    
    <!--  // Si fichier absent dans le répertoire, afficher "Fonction non présente" -->
    <?php  if( !file_exists($cat_story_file)) { ?>
    <div class="tableContainer" style="padding: 25px!important";><h4 >Fonction non présente</h4></div> 
     <?php 
      }
      else { 
         include_once( $cat_story_file );
      }
      ?>


<?php

	$idparent = DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '" . intval( $_REQUEST[ "idcategory" ] ) . "' LIMIT 1" )->fields( "idcategoryparent" );
	
	if( $idparent ){

		?>
		<p style="text-align:right; margin-bottom:10px;">
			<a class="blueLink" href="#" onclick="updateStats(<?php echo $idparent; ?> ); return false;">
				Retour à la catégorie parente
			</a>
		</p>
		<?php
		
	}

	?>

    
    
	
 
<?php
	
/* ------------------------------------------------------------------------------------------------------------- */
/* CA HT */

function getMargin( array &$categories ){

	?>
	<div style="margin-top: 10px;" class="subTitleContainer">
		<p class="subTitle">
			<a href="#" onclick="$( '#MarginDiv' ).slideToggle( 'slow' ); return false;" style="text-decoration:none;">&#0155; Marge Brute</a>
		</p>
	</div>
	<?php
	
	if( !count( $categories ) ){
		
		?>
		<p style="text-align:center; margin:15px;">
			Aucune vente trouvée
		</p>
		<?php
	
		return;
		
	}
	
	?>
	<div class="tableContainer" id="TurnoverDiv">
	
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/category_stats.php</span>
<?php } ?>

		<table class="dataTable resultTable">
			<thead>
				<tr>
					<th class="lefterCol">Intitulé</th>
					<th>Marge Brute</th>
					<th class="righterCol">Quantité</th>
				</tr>
			</thead>
			<tbody>
			<?php
	
				$turnoverSum = 0.0;
				$quantitySum = 0;
				
				foreach( $categories as $category ){
		
					?>
					<tr>
						<td class="lefterCol"><?php echo htmlentities( $child->name ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $child->turnoverAmount ); ?></td>
						<td class="righterCol" style="text-align:right;"><?php echo $child->turnoverQuantity; ?></td>
					</tr>
					<?php
					
					$turnoverQuantitySum 	+= $child->turnoverQuantity;
					$turnoverAmountSum 		+= $child->turnoverAmount;
					
				}
				
			?>
			</tbody>
			<tr style="font-weight:bold;">
				<td class="lefterCol"></td>
				<td style="text-align:right;"><?php echo Util::priceFormat( $turnoverAmountSum ); ?></td>
				<td class="righterCol" style="text-align:right;"><?php echo $turnoverQuantitySum; ?></td>
			</tr>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function getTreeNodeStats( stdclass &$treeNode, $start_date, $end_date ){
	
	/* feuille */
	
	if( !count( $treeNode->children ) ){
		
		getTreeNodeTurnover( $treeNode, $start_date, $end_date );
		getTreeNodePurchase( $treeNode, $start_date, $end_date );
		getTreeNodeEstimateCount( $treeNode, $start_date, $end_date );
	
		return;
		
	}
	
	/* autres noeuds */
	
	$i = 0;
	while( $i < count( $treeNode->children ) ){

		getTreeNodeStats( $treeNode->children[ $i ], $start_date, $end_date );
		$i++;
		
	}
	
	$i = 0;
	while( $i < count( $treeNode->children ) ){

		$treeNode->turnoverQuantity 	+= $treeNode->children[ $i ]->turnoverQuantity;
		$treeNode->turnoverAmount 		+= $treeNode->children[ $i ]->turnoverAmount;
		$treeNode->purchaseQuantity 	+= $treeNode->children[ $i ]->purchaseQuantity;
		$treeNode->purchaseAmount 		+= $treeNode->children[ $i ]->purchaseAmount;
		$treeNode->estimateCount 		+= $treeNode->children[ $i ]->estimateCount;
		
		$i++;
		
	}

}

/* ------------------------------------------------------------------------------------------------------------- */

function getTreeNodeTurnover( stdclass &$treeNode, $start_date, $end_date ){
	
	if( $treeNode->idcategory )
		getCategoryNodeTurnover( $treeNode, $start_date, $end_date );
	else getProductNodeTurnover( $treeNode, $start_date, $end_date );
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function getTreeNodePurchase( stdclass &$treeNode, $start_date, $end_date ){

	if( $treeNode->idcategory )
		getCategoryNodePurchase( $treeNode, $start_date, $end_date );
	else getProductNodePurchase( $treeNode, $start_date, $end_date );
	
}

/* -------------------------------------------------------------------------------------------------------- */

function getTreeNodeEstimateCount( stdclass &$treeNode, $start_date, $end_date ){
	
	if( $treeNode->idcategory )
		getCategoryNodeEstimateCount( $treeNode, $start_date, $end_date );
	else getProductNodeEstimateCount( $treeNode, $start_date, $end_date );
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function getCategoryNodeTurnover( stdclass &$treeNode, $start_date, $end_date ){
	
	$query = "
	SELECT orow.idcategory, SUM( orow.discount_price * orow.quantity ) AS turnover, SUM( orow.quantity ) AS quantity
	FROM `order` o, order_row orow
	WHERE o.status = 'Ordered'
	AND o.conf_order_date >= '$start_date 00:00:00' AND  o.conf_order_date <= '$end_date 23:59:59'
	AND o.idorder = orow.idorder
	AND orow.idcategory = '{$treeNode->idcategory}'
	GROUP BY orow.idcategory";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){

		$treeNode->turnoverQuantity = $rs->fields( "quantity" );
		$treeNode->turnoverAmount 	= $rs->fields( "turnover" );
		
	}
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function getProductNodeTurnover( stdclass &$treeNode, $start_date, $end_date ){

	$query = "
	SELECT orow.idcategory, SUM( orow.discount_price * orow.quantity ) AS turnover, SUM( orow.quantity ) AS quantity
	FROM `order` o, order_row orow, detail d
	WHERE d.idproduct = '{$treeNode->idproduct}'
	AND d.idarticle = orow.idarticle
	AND o.idorder = orow.idorder
	AND o.status = 'Ordered'
	AND o.conf_order_date >= '$start_date 00:00:00' AND  o.conf_order_date <= '$end_date 23:59:59'
	GROUP BY d.idproduct";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){

		$treeNode->turnoverQuantity = $rs->fields( "quantity" );
		$treeNode->turnoverAmount 	= $rs->fields( "turnover" );
		
	}
	
}

/* ------------------------------------------------------------------------------------------------------------- */

function getCategoryNodePurchase( stdclass &$treeNode, $start_date, $end_date ){
	
	$query = "
	SELECT osr.idcategory, SUM( osr.discount_price * osr.quantity ) AS purchase, SUM( osr.quantity ) AS quantity
	FROM order_supplier_row osr, order_supplier os
	WHERE os.DateHeure >= '$start_date 00:00:00' AND os.DateHeure <= '$end_date 23:59:59'
	AND os.idorder_supplier = osr.idorder_supplier
	AND osr.idcategory = '{$treeNode->idcategory}'
	GROUP BY osr.idcategory";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){

		$treeNode->purchaseQuantity = $rs->fields( "quantity" );
		$treeNode->purchaseAmount 	= $rs->fields( "purchase" );

	}

}

/* ------------------------------------------------------------------------------------------------------------- */

function getProductNodePurchase( stdclass &$treeNode, $start_date, $end_date ){

	$query = "
	SELECT osr.idcategory, SUM( osr.discount_price * osr.quantity ) AS purchase, SUM( osr.quantity ) AS quantity
	FROM order_supplier_row osr, order_supplier os, detail d
	WHERE d.idproduct = '{$treeNode->idproduct}'
	AND osr.idarticle = d.idarticle
	AND os.idorder_supplier = osr.idorder_supplier
	AND os.DateHeure >= '$start_date 00:00:00' AND os.DateHeure <= '$end_date 23:59:59'
	GROUP BY d.idproduct";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){

		$treeNode->purchaseQuantity = $rs->fields( "quantity" );
		$treeNode->purchaseAmount 	= $rs->fields( "purchase" );

	}
	
}

/* -------------------------------------------------------------------------------------------------------- */

function getCategoryNodeEstimateCount( stdclass &$treeNode, $start_date, $end_date ){

	$query = "
	SELECT COUNT( DISTINCT e.idestimate ) AS `count`
	FROM estimate_row er, estimate e
	WHERE er.idcategory = '{$treeNode->idcategory}'
	AND er.idestimate = e.idestimate
	AND e.status NOT LIKE 'Cancelled'
	AND e.DateHeure >= '$start_date 00:00:00' AND e.DateHeure <= '$end_date 23:59:59'";
	
	$treeNode->estimateCount = DBUtil::query( $query )->fields( "count" );
	
}

/* -------------------------------------------------------------------------------------------------------- */

function getProductNodeEstimateCount( stdclass &$treeNode, $start_date, $end_date ){

	$query = "
	SELECT COUNT( DISTINCT e.idestimate ) AS `count`
	FROM estimate_row er, estimate e, detail d
	WHERE d.idproduct = '{$treeNode->idproduct}'
	AND er.idarticle = d.idarticle
	AND er.idestimate = e.idestimate
	AND e.status NOT LIKE 'Cancelled'
	AND e.DateHeure >= '$start_date 00:00:00' AND e.DateHeure <= '$end_date 23:59:59'";
	
	$treeNode->estimateCount = DBUtil::query( $query )->fields( "count" );
	
}

/* -------------------------------------------------------------------------------------------------------- */

function getChildren( stdclass &$treeNode ){

	/* sous-catégories */
	
	$query = "
	SELECT cl.idcategorychild, c.name_1
	FROM category_link cl, category c
	WHERE cl.idcategoryparent = '{$treeNode->idcategory}'
	AND cl.idcategorychild = c.idcategory
	ORDER BY cl.displayorder ASC";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$category = createTreeNode();
		$category->depth = $treeNode->depth + 1;
		$category->idcategory = $rs->fields( "idcategorychild" );
		$category->name = $rs->fields( "name_1" );
		
		array_push( $treeNode->children, $category );
		
		$rs->MoveNext();
		
	}
	
	$i = 0;
	while( $i < count( $treeNode->children ) ){
		
		getChildren( $treeNode->children[ $i ] );
		$i++;
		
	}

	if( count( $treeNode->children ) || $treeNode->depth )
		return;
		
	/* produits */
		
	$rs = DBUtil::query( "SELECT idproduct, name_1 FROM product WHERE idcategory = '{$treeNode->idcategory}' ORDER BY display_product_order ASC" );
	
	while( !$rs->EOF() ){
		
		$product = createTreeNode();
		$product->depth = $treeNode->depth + 1;
		$product->idproduct = $rs->fields( "idproduct" );
		$product->name = $rs->fields( "name_1" );
		
		array_push( $treeNode->children, $product );
		
		$rs->MoveNext();
		
	}
	
}

/* -------------------------------------------------------------------------------------------------------- */

function createTreeNode(){
	
	$node = new stdclass();
	
	$node->depth 				= 0;
	$node->idcategory 			= 0;
	$node->idproduct 			= 0;
	$node->children 			= array();
	$node->name 				= "";
	$node->turnoverQuantity 	= 0;
	$node->turnoverAmount 		= 0.0;
	$node->purchaseQuantity 	= 0;
	$node->purchaseAmount 		= 0.0;
	$node->estimateCount 		= 0;

	return $node;
	
}

/* -------------------------------------------------------------------------------------------------------- */

?>