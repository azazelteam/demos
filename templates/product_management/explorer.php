<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

include_once( "../../config/init.php" );

//--------------------------------------------------------------------------------------------------------

header( "Content-Type: text/html; charset=utf-8" );
		
if( isset( $_REQUEST[ "idcategory" ] ) )
	$idcategory = intval( $_REQUEST[ "idcategory" ] );
else if( isset( $_REQUEST[ "idproduct" ] ) )
	$idcategory = DBUtil::getDBValue( "idcategory", "product", "idproduct", intval( $_REQUEST[ "idproduct" ] ) );
else $idcategory = 0;

//--------------------------------------------------------------------------------------------------------
/* récupération ajax du contenu de la catégorie */

if( isset( $_GET[ "list" ] ) ){
	
	getCategoryContent( $idcategory );
	exit();
	
}

//--------------------------------------------------------------------------------------------------------
/* récupération ajax de la description de la catégorie */

if( isset( $_GET[ "describe" ] ) ){
	
	describeCategory( $idcategory );
	exit();
	
}

//--------------------------------------------------------------------------------------------------------
/* récupération ajax de la description de la catégorie */

if( isset( $_GET[ "hide" ] ) ){
	
	$ret =& DBUtil::query( "UPDATE category SET available = !available WHERE idcategory = '$idcategory' LIMIT 1" );
	exit( $ret === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* tri des sous-catégories et produits */

if( isset( $_POST[ "sort" ] ) ){
	
	//sous-catégories
	
	if( isset( $_POST[ "Categories" ] ) ){
		
		$done = true;
		$i = 0;
		while( $i < count( $_POST[ "Categories" ] ) ){
			
			$idchild = intval( $_POST[ "Categories" ][ $i ] );
			$rs =& DBUtil::query( "UPDATE category_link SET displayorder = '$i' WHERE idcategorychild = '$idchild' LIMIT 1" );

			$done &= $rs !== false;
			
			$i++;
			
		}
		
		exit( $done ? "1" : "0" );
		
	}
	
	//produits
	
	if( isset( $_POST[ "Products" ] ) ){
		
		$done = true;
		$i = 0;
		while( $i < count( $_POST[ "Products" ] ) ){
			
			$idchild = intval( $_POST[ "Products" ][ $i ] );
			$rs =& DBUtil::query( "UPDATE product SET display_product_order = '$i' WHERE idproduct = '$idchild' LIMIT 1" );

			$done &= $rs !== false;
			
			$i++;
			
		}
		
		exit( $done ? "1" : "0" );
		
	}
	
	exit( "0" );

}

//--------------------------------------------------------------------------------------------------------

header( "Content-Type: text/html; charset=utf-8" );

?>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL; ?>/css/back_office/contents.css" />
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/jquery-1.3.2.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/jquery-ui-1.7.1.custom.min.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/ui/blockUI.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/ui/splitter-1.5.1/splitter.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
	
	/* ------------------------------------------------------------------------------------------ */
	
	/* splitters */
	
	$(document).ready(function(){

		$("#ExplorerSplitter").splitter({

			splitVertical: true,
			outline: true,
			sizeLeft: true,
			anchorToWindow: true,
			accessKey: "I"

		});		
		
		$("#RightPane").splitter({
		
			splitHorizontal: true,
			sizeTop: true,
			accessKey: "H"
			
		});
	
	});
	
	/* ------------------------------------------------------------------------------------------ */
	
	/* sélection de la catégorie */
	
	<?php
	
		if( isset( $_REQUEST[ "idcategory" ] ) ){
		
			?>
			$(document).ready(function(){ setCategory( <?php echo intval( $_REQUEST[ "idcategory" ] ); ?> );; });
			<?php
				
		}
		
	?>
	
	/* ------------------------------------------------------------------------------------------ */
	
	/* tri des sous-catégories et produits */

	function sortCatalogItems( serializedString ){
		
		var data = "sort=1&" + serializedString;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/explorer.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications" + responseText );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	function setCategory( idcategory ){
		
		/* sélection de la catégorie */
		
		$.ajax({
					 	
			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/category_tree.php?idcategory=" + idcategory + "&onExpandCallback=setCategory&onSelectCallback=setCategory",
			async: true,
		 	success: function( responseText ){

				document.getElementById( 'LeftPane' ).innerHTML = responseText;

			}
		
		});
		
		$.ajax({
					 	
			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/explorer.php?idcategory=" + idcategory + "&list",
			async: true,
		 	success: function( responseText ){

				document.getElementById( 'BottomPane' ).innerHTML = responseText;

				/* sortable */

				$("#CatalogItems").sortable({
				
					placeholder: 'placeholder',
					forcePlaceholderSize: true,
					update: function(event, ui){ sortCatalogItems( $("#CatalogItems").sortable('serialize') ); }
				
				});
				
				$("#CatalogItems").disableSelection();
		
			}
		
		});
		
		$.ajax({
					 	
			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/explorer.php?idcategory=" + idcategory + "&describe",
			async: true,
		 	success: function( responseText ){

				document.getElementById( 'TopPane' ).innerHTML = responseText;

			}
		
		});
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	function hideCategory( idcategory ){

		$.ajax({
					 	
			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/explorer.php?idcategory=" + idcategory + "&hide",
			async: true,
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					var available 	= document.getElementById( 'CategoryAvailable' );
					var button 		= document.getElementById( 'HideButton' );
					
					available.value = available.value == 1 ? 0 : 1;
					button.value = available.value == 1 ? "Masquer" : "Afficher";
					button.className =  available.value == 1 ? "blueButton" : "orangeButton";
					document.getElementById( 'CategoryThumb' ).style.opacity = available.value == 1 ? '1.0' : '0.7';

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
	        							
				}
				else alert( "Impossible d'enregistrer les modifications" );
				
			}
		
		});

	}
	
	/* ------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<style type="text/css" media="all">

	html, body{
		
		margin: 0;			/* Remove body margin/padding */
		padding: 0;
		overflow: hidden;	/* Remove scroll bars on browser window */
		font-size:12px;
		font-family:Arial,Helvetica,sans-serif;
		color:#44474E;
	
	}
	
	#header { padding: 1em; }
	
	/* arborescence catégories */
	
	#CategoryTree{	
	}
	
	#CategoryTree li{
		
		list-style-type:none;
		
	}
	
	#CategoryTree li a{
	
		color:#44474E;
		text-decoration:none;
			
	}
	
	#CategoryTree li a img{
		
		border-style:none;

	}
	
	/* splitters */

	#ExplorerSplitter {
		margin: 0;			/* Remove body margin/padding */
		padding: 0;
		overflow: hidden;	/* Remove scroll bars on browser window */
		border-style:none;
		min-width: 500px;	/* Splitter can't be too thin ... */
		min-height: 300px;	/* ... or too flat */
		width:100%;
		height: 100%;
	}
	
	#LeftPane {
		background-color: #FAFAFA;
		width: 30%;
		height:100%;
		min-width: 100px;
		overflow: auto;		/* Scroll bars appear as needed */
		padding:10px;
	}
	
	#RightPane {
		width:70%;
	}
	
	#TopPane {				/* Top nested in right pane */
		background-color: #FDFDFD;
		height: 40%;		/* Initial height */
		width:100%;
		min-height: 75px;	/* Minimum height */
		overflow: auto;	
		padding:10px;
	}
	
	#BottomPane {			/* Bottom nested in right pane */
		height:60%;
		width:100%;
		background-color: #FDFDFD;
		min-height: 100px;
		overflow: auto;
		padding:10px;
	}

	.vsplitbar {
		width: 3px;
		background: #F7F7F7 url(<?php echo $GLOBAL_START_URL; ?>/js/jquery/ui/splitter-1.5.1/img/vgrabber.gif) no-repeat center;
		border:1px groove #E7E7E7;
	}
	
	.vsplitbar:hover, .vsplitbar.active {
		opacity: 0.7;
		filter: alpha(opacity=70); /* IE */
		background: #F7F7F7 url(<?php echo $GLOBAL_START_URL; ?>/js/jquery/ui/splitter-1.5.1/img/vgrabber.gif) no-repeat center;
	}
	
	.hsplitbar {
		height: 3px;
		background: #F7F7F7 url(<?php echo $GLOBAL_START_URL; ?>/js/jquery/ui/splitter-1.5.1/img/hgrabber.gif) no-repeat center;
		border:1px groove #E7E7E7;
	}
	
	.hsplitbar.active, .hsplitbar:hover {
		opacity: 0.7;
		filter: alpha(opacity=70); /* IE */
		background: #F7F7F7 url(<?php echo $GLOBAL_START_URL; ?>/js/jquery/ui/splitter-1.5.1/img/hgrabber.gif) no-repeat center;
	}
	
	/* LeftPane content */
	
	.categoryNode{ font-weight: normal; }
	.selectedCategoryNode{ font-weight: bold; }
	.hiddenCategoryNode{ text-decoration: line-through; font-style: italic; }
		
	/* TopPane content */
	
	#CategoryThumb{ border:1px solid #E7E7E7; }
	#CategoryName{
		
		font-size:14px;
		font-weight:bold;

	}
	
	#CategoryNumber{ color:#FF0000; }
	
	/* BottomPane content */
	
	#CatalogItems{
		
		list-style-type:none;
		
	}
	
	.catalogItem{
	
		float:left; 
		height:120px; 
		width:120px; 
		margin:5px; 
		border:1px solid #E7E7E7; 
		padding:4px;
		
	}
	
	.catalogItem a img{
	
		border-style:none;
		
	}
		
	/* sortable */
	
	#CatalogItems { list-style-type: none; margin: 0; padding: 0; }
	#CatalogItems li {

		margin: 4px; 
		padding: 1px; 
		float: left; 
		width: 120px; 
		height: 120px; 
		font-size: 4em; 
		text-align: center; 
		border: 1px solid #D2D2D2;
		
	}
	
	#CatalogItems li a{ cursor: move; }
	
	#CatalogItems li:hover{
		/*border: 1px solid #EB6A0A; */
	}
	
	#CatalogItems .placeholder{
		width: 120px; 
		height: 120px; 
		padding: 1px; 
		margin: 3px 3px 3px 0;
		background-color:#F2F2F2;
	}
				
</style>
<div id="ExplorerSplitter">
	<div id="LeftPane">
		<?php echo file_get_contents( "$GLOBAL_START_URL/templates/product_management/category_tree.php?idcategory=$idcategory&onExpandCallback=setCategory&onSelectCallback=setCategory" ); ?>
	</div>
   	<div id="RightPane">
		<div id="TopPane">Sélectionnez une catégorie dans l'arborescence de gauche pour voir sa description</div>
		<div id="BottomPane">Sélectionnez une catégorie dans l'arborescence de gauche pour voir son contenu</div>
	</div><!-- RightPane -->
</div><!-- ExplorerSplitter -->
<?php

//-----------------------------------------------------------------------------------------------------

function describeCategory( $idcategory ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
	$lang = User::getInstance()->getLang();
	
	$rs =& DBUtil::query( "SELECT imagedirectory, imagename, available, name$lang AS name, description$lang AS description FROM category WHERE idcategory = '$idcategory' LIMIT 1" );
	
	$uri = "/www" . $rs->fields( "imagedirectory" ) . "/" . $rs->fields( "imagename" );
	
	$opacity 	= $rs->fields( "available" ) ? "" : "; opacity:0.4; filter:alpha(opacity=40);";
	
	if( !strlen( $rs->fields( "imagedirectory" ) ) || !strlen( $rs->fields( "imagename" ) ) || !file_exists( $GLOBAL_START_PATH . $uri ) )
			$src = "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( "/www/img_prod/photo_missing_icone.jpg" ) . "&amp;w=150&amp;h=150";
	else 	$src = "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( $uri ) . "&amp;w=150&amp;h=150";
			
	?>
	<div style="float:left;">
   
   <?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/explorer.php</span>
<?php } ?>
   
   
		<span id="CategoryName"><?php echo htmlentities( $rs->fields( "name" ) ); ?></span><br />
		<span id="CategoryNumber">catégorie n° <?php echo $idcategory; ?></span>
	</div>
	<div style="float:right;">
		<input type="button" id="HideButton" value="<?php echo $rs->fields( "available" ) ? "Masquer" : "Afficher"; ?>" class="<?php echo $rs->fields( "available" ) ? "blueButton" : "orangeButton"; ?>" onclick="hideCategory( <?php echo $idcategory; ?> );" />
		<input type="button" name="" value="Editer" class="blueButton" onclick="document.location='<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?action=edit&amp;idcategory=<?php echo $idcategory; ?>';" />
		<input type="hidden" id="CategoryAvailable" value="<?php echo $rs->fields( "available" ); ?>" />
	</div>
	<br style="clear:both;" />
	<div style="padding-top:10px;">
		<div style="float:left; margin:0px 10px 10px 0px;<?php echo $rs->fields( "available" ) ? "opacity:1.0;" : "opacity:0.7;"; ?>"><img id="CategoryThumb" src="<?php echo $src; ?>" alt="" /></div>
		<div><?php echo strlen( $rs->fields( "description" ) ) ? $rs->fields( "description" ) : "Aucune description disponible"; ?></div>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function getCategoryContent( $idcategory ){
	
	global $GLOBAL_START_URL;

	
	if( DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '$idcategory' LIMIT 1" )->RecordCount() )
		listSubCategories( $idcategory );
	else if( DBUtil::query( "SELECT idproduct FROM product WHERE idcategory = '$idcategory' LIMIT 1" )->RecordCount() )
		listProducts( $idcategory );
	else{
		
		?>
		<div style="float:left;">Cette catégorie est vide</div>
		<div style="float:right;">
			<input type="button" name="" value="Ajouter une sous-catégorie" class="blueButton" />
			<input type="button" name="" value="Ajouter un produit" class="blueButton" onclick="document.location='<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idcategory=<?php echo $idcategory; ?>';" />
		</div>
		<?php	
		
	}
	
}

//-----------------------------------------------------------------------------------------------------

function listSubCategories( $idcategory ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT cl.idcategorychild,
		c.name$lang,
		c.imagedirectory,
		c.imagename,
		c.available
	FROM category_link cl, category c
	WHERE cl.idcategoryparent = '$idcategory' 
	AND cl.idcategorychild = c.idcategory
	ORDER BY cl.displayorder ASC";

	$rs =& DBUtil::query( $query );
	
	?>
	<div style="float:left;"><?php echo $rs->RecordCount(); ?> sous-catégorie(s)</div>
	<div style="float:right;">
		<input type="button" name="" value="Ajouter une sous-catégorie" class="blueButton" />
	</div>
	<br style="clear:both;" />
	<ul id="CatalogItems" style="padding-top:10px;">
	<?php
	
		while( !$rs->EOF() ){
			
			$uri = "/www" . $rs->fields( "imagedirectory" ) . "/" . $rs->fields( "imagename" );
	
			$opacity = $rs->fields( "available" ) ? "" : "; opacity:0.4; filter:alpha(opacity=40);";
			$strike = $rs->fields( "available" ) ? "" : "; font-style:italic; text-decoration: line-through;";
			
			if( !strlen( $rs->fields( "imagedirectory" ) ) || !strlen( $rs->fields( "imagename" ) ) || !file_exists( $GLOBAL_START_PATH . $uri ) )
					$src = "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( "/www/img_prod/photo_missing_icone.jpg" ) . "&amp;w=120&amp;h=120";
			else 	$src = "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( $uri ) . "&amp;w=120&amp;h=120";
			
			?>
			<li id="Categories_<?php echo $rs->fields( "idcategorychild" ); ?>" class="catalogItem">
				<a title="<?php echo htmlentities( $rs->fields( "name$lang" ) ); ?>" href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?action=edit&amp;idcategory=<?php echo $rs->fields( "idcategorychild" ); ?>">
					<img style="border-style:none; width:120px; height:120px;<?php echo $opacity; ?>" src="<?php echo $src; ?>" alt="" />
				</a>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<div class="clear"></div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listProducts( $idproduct ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT p.idproduct,
		p.name$lang,
		p.imagedirectory,
		p.imagename,
		p.available,
		p.idcategory
	FROM product p
	WHERE p.idcategory = '$idproduct' 
	ORDER BY p.display_product_order ASC";

	$rs =& DBUtil::query( $query );
	
	?>
	<div style="float:left;"><?php echo $rs->RecordCount(); ?> produit(s)</div>
	<div style="float:right;">
		<input type="button" name="" value="Ajouter un produit" class="blueButton" onclick="document.location='<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idcategory=<?php echo $rs->fields( "idcategory" ); ?>';" />
	</div>
	<br style="clear:both;" />
	<ul id="CatalogItems" style="padding-top:10px;">
	<?php
	
		while( !$rs->EOF() ){
			
			$uri = "/www" . $rs->fields( "imagedirectory" ) . "/" . $rs->fields( "imagename" );
	
			if( !strlen( $rs->fields( "imagedirectory" ) ) || !strlen( $rs->fields( "imagename" ) ) || !file_exists( $GLOBAL_START_PATH . $uri ) )
					$src = "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( "/www/img_prod/photo_missing_icone.jpg" ) . "&amp;w=120&amp;h=120";
			else 	$src = "$GLOBAL_START_URL/catalog/thumb.php?src=" . urlencode( $uri ) . "&amp;w=120&amp;h=120";
			
			$opacity = $rs->fields( "available" ) ? "" : "; opacity:0.4; filter:alpha(opacity=40);";
			$strike = $rs->fields( "available" ) ? "" : "; font-style:italic; text-decoration: line-through;";
			
			?>
			<li id="Products_<?php echo $rs->fields( "idproduct" ); ?>" class="catalogItem">
				<a title="<?php echo htmlentities( $rs->fields( "name$lang" ) ); ?>" href="<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>">
					<img style="width:120px; height:120px;<?php echo $opacity; ?>" src="<?php echo $src; ?>" alt="" />
				</a>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<div class="clear"></div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>