<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Statistiques produits
 */
 
include_once( "../../config/init.php" );
include_once( "../../objects/DHTMLCalendar.php" );

header( "Content-Type: text/html; charset=utf-8" );

DHTMLCalendar::calendar( "StartDate" );
DHTMLCalendar::calendar( "EndDate" );

?>
<script type="text/javascript">
/* <![CDATA[ */
	
	function searchTrades( orderby, asc ){
	
		var tabURL =  "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_stats.php?idproduct=<?php echo $_REQUEST[ "idproduct" ]; ?>";
		tabURL += "&start=" + $( "#StartDate" ).val();
		tabURL += "&end=" + $( "#EndDate" ).val();
		tabURL += "&orderby=" + escape( orderby );
		tabURL += asc ? "&asc" : "&desc";
		
		$( "#ProductTabs" ).tabs( "url" , 8 ,tabURL );
		$( '#ProductTabs' ).tabs( "load", 8 );
		
	}

/* ]]> */
</script>
<style type="text/css">

	.statTable{ 
		width: 100%;
		border-collapse: collapse;
		border:1px solid #E7E7E7;
	}
	
	.statTable th.noTopBorder{
		border-top-style:none !important;
	}
	
	.statTable th.noTopBorder img{
		display:block;
		width:13px;
		margin:2px auto;
	}
	
	.statTable th{
		border-bottom-style:none !important;
	}
	
	.statTable th, .statTable td {
		border:1px solid #E7E7E7;
		height:20px;
		text-align:center;
		width:10%;	
	}

	.statTable th{
		background-color:#F3F3F3;
	}
	
	.trade a{
		margin:20px;
	}
	
</style>
<?php 
$product_story_file = dirname( __FILE__ )."/../../akilae_pro/product_story.php";
?>
    
<!--Si fichier absent dans le répertoire, afficher "Fonction non présente" -->
<?php
     if( !file_exists($product_story_file)) { ?>
      <div class="tableContainer" style="padding:25px!important;"><h4 >Fonction non présente</h4></div> 
     <?php }
     else { 
        
        include($product_story_file);
     }
     ?>
<?php
/* ------------------------------------------------------------------------------------------- */

function listOrders( $idarticle ){
	
	global $GLOBAL_START_URL;
	
	$query = "
	SELECT tr.reference, 
		tr.idarticle,
		t.idorder, 
		t.DateHeure, 
		t.idbuyer,
		tr.quantity,	
		tr.unit_cost_amount,
		tr.unit_price,
		tr.ref_discount,
		tr.discount_price,
		tr.quantity * tr.discount_price AS totalET
	FROM `order` t, order_row tr
	WHERE tr.idarticle = '$idarticle'
	AND t.idorder = tr.idorder
	AND t.status NOT LIKE 'Cancelled'";
	
	if( isset( $_REQUEST[ "start" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "start" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "start" ] );	
		$query .= " AND t.DateHeure >= '$year-$month-$day 00:00:00'";
	
	}
	
	if( isset( $_REQUEST[ "end" ] ) && preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_REQUEST[ "end" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_REQUEST[ "end" ] );	
		$query .= " AND t.DateHeure <= '$year-$month-$day 23:59:59'";
		
	}
	
	$order = isset( $_REQUEST[ "desc" ] ) ? "DESC" : "ASC";
	
	if( isset( $_REQUEST[ "orderby" ] ) )
		$query .= "
		ORDER BY " . $_REQUEST[ "orderby" ] . " $order";
	else $query .= " ORDER BY t.DateHeure DESC, t.idorder DESC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune commande trouvée</p>
		<?php
		
		return;
			
	}
	
	?>
	<div class="tableContainer" id="OrderList<?php echo $idarticle; ?>">
	
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_stats.php</span>
<?php } ?>
	
		<table class="statTable">
			<thead>
				<tr>
					<th style="text-align:center;">Référence</th>
					<th style="text-align:center;">Cde N°</th>
					<th style="text-align:center;">Date</th>
					<th style="text-align:center;">Client N°</th>
					<th style="text-align:center;">Quantité</th>
					<th style="text-align:center;">Prix Achat Net</th>
					<th style="text-align:center;">Prix Vente U.</th>
					<th style="text-align:center;">Remise %</th>
					<th style="text-align:center;">Prix Vente U. Net</th>
					<th style="text-align:center;">Total HT</th>
				</tr>
				<tr>
                	<th class="noTopBorder"></th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('t.DateHeure',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('t.DateHeure',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('t.idbuyer',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('t.idbuyer',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.unit_cost_amount',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.unit_cost_amount',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.unit_price',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.unit_price',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder"></th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.discount_price',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.discount_price',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
                    <th class="noTopBorder">
                    	<a href="#" onclick="searchTrades('tr.discount_price * tr.quantity',true); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                        <a href="#" onclick="searchTrades('tr.discount_price * tr.quantity',false); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                    </th>
				</tr>
			</thead>
			<tbody>
			<?php
		
				$orderCount 			= 0;
				$customerCount 			= 0;
				$quantitySum 			= 0.0;
				$unit_cost_amountSum 	= 0.0;
				$unit_priceSum 			= 0.0;
				$ref_discountSum 		= 0.0;
				$discount_priceSum 		= 0.0;
				$totalETSum 			= 0.0;
				
				$customers 				= array();
				$last_idorder = false;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td style="text-align:center;"><?php echo htmlentities( $rs->fields( "reference" ) ); ?></td>
						<td style="text-align:center;">
							<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $rs->fields( "idorder" ); ?>" onclick="window.open(this.href); return false;">
								<?php echo $rs->fields( "idorder" ); ?>
							</a>
						</td>
						<td style="text-align:center;"><?php echo Util::dateFormatEu( substr( $rs->fields( "DateHeure" ), 0, 10 ) ); ?></td>
						<td style="text-align:center;">
							<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>" onclick="window.open(this.href); return false;">
								<?php echo $rs->fields( "idbuyer" ); ?>
							</a>
						</td>
						<td style="text-align:right;"><?php echo $rs->fields( "quantity" ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "unit_cost_amount" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "unit_price" ) ); ?></td>
						<td style="text-align:right;"><?php echo $rs->fields( "ref_discount" ) > 0.0 ? Util::rateFormat( $rs->fields( "ref_discount" ) ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "discount_price" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "totalET" ) ); ?></td>
					</tr>
					<?php
					
					if( $last_idorder === false || $last_idorder != $rs->fields( "idorder" ) ){
					
						$last_idorder = $rs->fields( "idorder" );
						$orderCount++;
						
					}
					
					if( !in_array( $rs->fields( "idbuyer" ), $customers ) )
						array_push( $customers, $rs->fields( "idbuyer" ) );
						
					$quantitySum 			+= $rs->fields( "quantity" );
					$unit_cost_amountSum 	+= $rs->fields( "unit_cost_amount" );
					$unit_priceSum 			+= $rs->fields( "unit_price" );
					$discount_priceSum 		+= $rs->fields( "discount_price" );
					$totalETSum 			+= $rs->fields( "totalET" );
					
					$rs->MoveNext();
		
				}
				
				?>
				<tr style="font-weight:bold;">
					<th>Total</th>
					<th><?php echo $orderCount; ?></th>
					<th></th>
					<th><?php echo count( $customers ); ?></th>
					<th style="text-align:right;"><?php echo $quantitySum; ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $unit_cost_amountSum / $rs->RecordCount() ); ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $unit_priceSum / $rs->RecordCount() ); ?></th>
					<th></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $discount_priceSum / $rs->RecordCount() ); ?></th>
					<th style="text-align:right;"><?php echo Util::priceFormat( $totalETSum ); ?></th>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------- */

?>