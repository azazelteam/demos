<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 *......
 */
 
//---------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

$notemplate = true;

include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );

//---------------------------------------------------------------------------------------------

?>
<style type="text/css">
		
	#SiblingList {
		
		margin: 20px auto 20px auto;
		list-style-type: none;
		padding: 0px;
		
	}
	
	#SiblingList li{
		
		margin-bottom: 0px;
		margin-top: 4px;
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		margin:4px;
		
	}

	#SiblingList .placeholder{
		
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		background-color:#F3F3F3;
		
	}
	
	#SiblingList li a{ cursor:move; }
	
</style>
<script type="text/javascript">
/* <!CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
	/* inititalisation tri des produits voisins */
	
	$(document).ready(function(){
	
		$("#SiblingList").sortable({
					
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			update: function(event, ui){ sortSiblingItems( $("#SiblingList").sortable('serialize') ); }
		
		});
		
		$("#SiblingList").disableSelection();
		
	});
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function sortSiblingItems( serializedString ){
	
		var data = "sort=1&" + serializedString;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/cat_product.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications" );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_siblings.htm.php</span>
<?php } ?>


<div class="subTitleContainer" style="padding-top:10px;">
    <p class="subTitle">Produits dans la même catégorie</p>
</div>
<p>
	<ul style="margin-top:20px;">
		<li>Glissez-déposez les éléments ci-dessous pour réorganiser l'ordre d'affichage des produits sur le site.</li>
		<li>Cliquez sur un produit ci-dessous pour accéder à sa fiche.</li>
	</ul>
</p>
<p>
<?php

	//-----------------------------------------------------------------------------------------------------
	
	$db =DBUtil::getConnection();
	
	$query = "SELECT idcategory AS idparent FROM product WHERE idproduct = '$idproduct' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer la catégorie parente" );
	
	$idparent = $rs->fields( "idparent" );
	
	$langue=User::getInstance()->getLang();
	$query = "
	SELECT p.idproduct AS idchild, 
		p.name$langue,
		p.display_product_order,
		p.available
	FROM product p
	WHERE p.idcategory = '$idparent'
	ORDER BY p.display_product_order ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		trigger_error( "Impossible de récupérer la liste des produits dans la même catégorie", E_USER_ERROR );
		
	?>
	<ul id="SiblingList">
	<?php 
	
		$rs->MoveFirst();
		
		while( !$rs->EOF() ){
			
			$name = $rs->fields( "name$langue" );
			$idchild = $rs->fields( "idchild" );
			$displayorder = $rs->fields( "display_product_order" );
			$opacity = $rs->fields( "available" ) ? "1.0" : "0.7";
		
			?>
			<li id="Siblings_<?php echo $idchild ?>">
				<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $idchild; ?>">
					<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE,0 , 60, 60 ); ?>" title="<?php echo htmlentities( $name ); ?>" alt="" style="opacity:<?php echo $opacity; ?>;" />
				</a>
			</li>
			<?php 
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
</p>
<div class="clear" style="margin-bottom:10px;"></div>
<?php 

//-----------------------------------------------------------------------------------------------------

?>