<?php

/* ------------------------------------------------------------------------------------------------------- */
/* activation/désactivation de la tarification au volume - ajax */

if( isset( $_REQUEST[ "enable" ] ) && $_REQUEST[ "enable" ] == "quantity_strategy" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/catalog/CArticle.php" );
	
	DBUtil::query( "UPDATE product_price_strategy SET enabled = !enabled WHERE strategy LIKE 'QuantityStrategy' LIMIT 1" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayQuantityPrice( new CArticle( intval( $_REQUEST[ "idarticle" ] ) ) );
	
	exit();

}

/* ------------------------------------------------------------------------------------------------------- */
/* suppression prix au volume - ajax */

if( isset( $_REQUEST[ "create" ] ) && $_REQUEST[ "create" ] == "quantity_price" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/catalog/CArticle.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Util.php" );
	
	if( !intval( $_REQUEST[ "min_quantity" ] ) 
		|| ( !Util::text2num( $_REQUEST[ "rate" ] ) && !Util::text2num( $_REQUEST[ "price" ] ) ) ){
			
			?>
			<p style="text-align:center; color:#FF0000;">
				<b>Vous devez obligatoirement sélectionner une quantité minimale et un pourcentage de remise ou un prix net.</b>
			</p>
			<?php
			
			displayQuantityPrice( new CArticle( intval( $_REQUEST[ "idarticle" ] ) ) );
	
			exit();
	
	}
		
	$query = "
	INSERT IGNORE INTO quantity_price( idarticle, reference, min_quantity, max_quantity, rate, price, price_cost ) VALUES( 
		'" . intval( $_REQUEST[ "idarticle" ] ) . "',
		'" . stripslashes($_REQUEST[ "reference" ]) . "',
		'" . intval( $_REQUEST[ "min_quantity" ] ) . "',
		'" . intval( $_REQUEST[ "max_quantity" ] ) . "',
		'" . max( 0.0, min( Util::text2num( $_REQUEST[ "rate" ] ), 100.0 ) ) . "',
		'" . Util::text2num( $_REQUEST[ "price" ] ) . "', 
		'" . Util::text2num( $_REQUEST[ "price_cost" ] ) . "' 
	)";
	
	DBUtil::query( $query );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayQuantityPrice( new CArticle( intval( $_REQUEST[ "idarticle" ] ) ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------- */
/* suppression prix au volume - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "quantity_price" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	$query = "
	DELETE FROM quantity_price
	WHERE idarticle = '" . intval( $_REQUEST[ "idarticle" ] ) . "'
	AND min_quantity = '" . intval( $_REQUEST[ "min_quantity" ] ) . "'
	AND max_quantity = '" . intval( $_REQUEST[ "max_quantity" ] ) . "'
	LIMIT 1";
	
	exit( DBUtil::query( $query ) === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------- */
/* références identiques */

/*
if( isset( $_REQUEST[ "clone" ] ) && isset( $_REQUEST[ "reference" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	/* suppression d'une référence du groupe */
	/*
	if( isset( $_REQUEST[ "remove" ] ) ){
		
		$clone_id = DBUtil::getDBValue( "clone_id", "detail", "reference", stripslashes( $_REQUEST[ "reference" ] ) );
		
		DBUtil::query( "UPDATE detail SET clone_id = '0' WHERE idarticle = " . DBUtil::quote( $_REQUEST[ "clone" ] ) . " LIMIT 1" );
			
		/* suppression du groupe si inutile */
		/*
		if( DBUtil::query( "SELECT COUNT(*) AS `count` FROM detail WHERE clone_id = '$clone_id'" )->fields( "count" ) == 1 )
			DBUtil::query( "UPDATE detail SET clone_id = '0' WHERE clone_id = '$clone_id' LIMIT 1" );
				
		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}
	
	/* sélection non valide */
	/*
	if( strlen( $_REQUEST[ "clone" ] ) && !DBUtil::query( "SELECT idarticle FROM detail WHERE reference = " . DBUtil::quote( $_REQUEST[ "clone" ] ) . " LIMIT 1" )->RecordCount() ){

		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}
	
	/* annulation */
	/*
	if( !strlen( $_REQUEST[ "clone" ] ) ){

		$current_clone_id = DBUtil::getDBValue( "clone_id", "detail", "reference", stripslashes( $_REQUEST[ "reference" ] ) );
		
		if( $current_clone_id ){
			
			DBUtil::query( "UPDATE detail SET clone_id = '0' WHERE reference LIKE " . DBUtil::quote( $_REQUEST[ "reference" ] ) . " LIMIT 1" );
			
			/* suppression du groupe si inutile */
			/*
			if( DBUtil::query( "SELECT COUNT(*) AS `count` FROM detail WHERE clone_id = '$current_clone_id'" )->fields( "count" ) == 1 )
				DBUtil::query( "UPDATE detail SET clone_id = '0' WHERE clone_id = '$current_clone_id' LIMIT 1" );
			
		}
		
		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}
	
	$source_clone_id 	= DBUtil::getDBValue( "clone_id", "detail", "reference", stripslashes( $_REQUEST[ "reference" ] ) );
	$target_clone_id	= DBUtil::getDBValue( "clone_id", "detail", "reference", stripslashes( $_REQUEST[ "clone" ] ) );
	
	/* ajout de la référence à un groupe existant */
	/*
	if( !$source_clone_id && $target_clone_id ){

		$ret = DBUtil::query( "UPDATE detail SET clone_id = '$target_clone_id' WHERE reference LIKE " . DBUtil::quote( $_REQUEST[ "reference" ] ) . " LIMIT 1" );
		
		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}
	
	/* ajout d'un autre référence au groupe actuel */
	/*
	if( $source_clone_id && !$target_clone_id ){

		$ret = DBUtil::query( "UPDATE detail SET clone_id = '$source_clone_id' WHERE reference LIKE " . DBUtil::quote( $_REQUEST[ "clone" ] ) . " LIMIT 1" );
	
		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}
	
	/* création d'un nouveau groupe */
	/*
	if( !$source_clone_id && !$target_clone_id ){

		$clone_id = DBUtil::query( "SELECT MAX( clone_id ) + 1 AS clone_id FROM detail" )->fields( "clone_id" );
		$ret = DBUtil::query( "UPDATE detail SET clone_id = '$clone_id' WHERE reference IN( " . DBUtil::quote( $_REQUEST[ "reference" ] ) . ", " . DBUtil::quote( $_REQUEST[ "clone" ] ) . " )" );
		
		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}
	
	/* fusion de 2 groupes */
	/*
	if( $source_clone_id && $target_clone_id && $source_clone_id != $target_clone_id ){

		$replace 	= min( $source_clone_id, $target_clone_id );
		$search 	= max( $source_clone_id, $target_clone_id );
		
		$ret = DBUtil::query( "UPDATE detail SET clone_id = '$replace' WHERE clone_id = '$search'" );
	
		listClones( stripslashes( $_REQUEST[ "reference" ] ) );
		exit();
		
	}

	/* aucune modification */
	/*
	listClones( stripslashes( $_REQUEST[ "reference" ] ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------- */

/**
 * Formulaire pour la Gestion des articles
 */

include_once( dirname( __FILE__ ) . "/../../objects/Util.php" );

if( !count( $_FILES ) ){ //@todo : bug plugin Ajax?? l'encodage est différent selon que des fichiers soient postés ou non
	
	$_POST 		= Util::arrayMapRecursive( "Util::doNothing", $_POST );
	$_REQUEST 	= Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	$_GET 		= Util::arrayMapRecursive( "Util::doNothing", $_GET );

}

WYSIWYGEditor::init( false );

?>
<script type="text/javascript" src="/js/tool-man/core.js"></script>
<script type="text/javascript" src="/js/tool-man/events.js"></script>
<script type="text/javascript" src="/js/tool-man/css.js"></script>
<script type="text/javascript" src="/js/tool-man/coordinates.js"></script>
<script type="text/javascript" src="/js/tool-man/drag.js"></script>
<script type="text/javascript" src="/js/tool-man/dragsort.js"></script>
<script type="text/javascript" src="/js/tool-man/cookies.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
	
	/* ------------------------------------------------------------------------------------------------------- */
		
	var requiredColor = '#CFC3E9';
	var requiredFields = new Array(<?php

	$query = "
	SELECT fieldname
	FROM desc_field
	WHERE tablename LIKE 'detail'
	AND mandatory = 1";

	$rs =& DBUtil::query( $query );

	$requiredFieldCount = 0;
	while( !$rs->EOF() ){

		if( $requiredFieldCount )
			echo ",";

		echo "'" . htmlentities( $rs->fields( "fieldname" ) ) . "'";

		$rs->MoveNext();
		$requiredFieldCount++;

	}

	$supplierIdstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $article->get( "idsupplier" ) );
	$default_idstate = DBUtil::getParameter( "default_idstate" );
	
	$displayCodeCustomhouse = $default_idstate != $supplierIdstate;

	if($displayCodeCustomhouse){
		echo ",";
		echo "'code_customhouse'";
	}
	
	?>);

	$( document ).ready( function(){
		
		var i = 0;
		while( i < requiredFields.length ){
	
			var requiredField = requiredFields[ i ];
			var element = null;
	
			if( document.getElementById( 'ArticleForm' ).elements[ requiredField ] != null )
				element = document.getElementById( 'ArticleForm' ).elements[ requiredField ];
			else if( document.getElementById( 'ArticleForm' ).elements[ requiredField + '[]' ] != null )
				element = document.getElementById( 'ArticleForm' ).elements[ requiredField + '[]' ];
	
			if( element != null && ( !element.type || element.type != 'hidden' ) ){
	
				var mul = document.createElement( 'span' );
				mul.style.color = '#FF0000';
				mul.style.fontWeight = 'bold';
				mul.style.fontSize = '14px';
				mul.innerHTML = '&nbsp;*&nbsp;';
	
				element.parentNode.insertBefore( mul, element.nextSibling );
				element.style.backgroundColor = requiredColor;
	
			}
	
			i++;
	
		}

		$('#ArticleForm').ajaxForm( {
		 	
	        beforeSubmit:  preSubmitCallback,
	        success:       postSubmitCallback
  
	    } );

		$('#ArticleForm').bind('form-pre-serialize', function(e) {
		    tinyMCE.triggerSave();
		});
		
	});

	/* ---------------------------------------------------------------------------------------------------------------- */
	/* ATTENTION : bug preSubmitCallback : préférer <form onsubmit="... */
	function preSubmitCallback( formData, jqForm, options ){

		//tinyMCE.triggerSave();
		return checkRequiredFields();
		
	}
	
	/* ------------------------------------------------------------------------------------------------------- */
	
	function postSubmitCallback( responseText, statusText ){ 
	
		if( responseText != "1" ){

			alert( "Impossible de sauvegarder les informations : " + responseText );
			return;
			
		}

		/* confirmation */
		
		$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
		$.blockUI.defaults.css.fontSize = '12px';
    	$.growlUI( "", "Modifications enregistrées" );
    	
	}
	
	/* ------------------------------------------------------------------------------------------------------- */
	
	function checkRequiredFields( form ){

		var form = document.getElementById( 'ArticleForm' );

		if( !requiredFields.length )
			return true;

		var i = 0;
		while( i < requiredFields.length ){

			var fieldname = requiredFields[ i ];
			var element = form.elements[ fieldname ];

			if( element != null ){

				switch( element.tagName.toLowerCase() ){

					case 'input' :

						if( element.getAttribute( 'type' ) != null && element.getAttribute( 'type' ) == 'text' ){

							if( element.value == '' || element.name == "sellingcost" && parseInt( element.value ) == 0 ){

								element.style.backgroundColor = '#FF0000';
								element.style.color = '#FFFFFF';
								alert( '<?php echo str_replace( "'", "\'", "Certains champs obligatoires n'ont pas été renseignés" ) ?>' );

								return false;

							}

						}

						break;

					case 'select' :

						var selectedIndex 	= element.selectedIndex;

						if( selectedIndex == null )
							return false;

						if( element.getAttribute( 'multiple' ) == null ){

							var selectedValue = element.options[ element.selectedIndex ].value;

							if( selectedValue == '' ){

								element.style.backgroundColor = '#FF0000';
								element.style.color = '#FFFFFF';
								alert( '<?php echo str_replace( "'", "\'", "Certains champs obligatoires n'ont pas été renseignés" ) ?>' );

								return false;

							}

						}
						else{

							var selectedIndexes = element.selectedIndex;
							var i = 0;

							var done = false;
							while( !done && i < selectedIndexes.length ){

								var selectedIndex = selectedIndexes[ i ];

								if( element.option[ selectedIndex ].value != '' )
									done = true;

								i++;

							}

							if( !done ){

								element.style.backgroundColor = '#FF0000';
								element.style.color = '#FFFFFF';
								alert( '<?php echo str_replace( "'", "\'", "Certains champs obligatoires n'ont pas été renseignés" ) ?>' );

								return false;

							}

						}

						break;

					default : return false;

				}

				element.style.backgroundColor = requiredColor;
				element.style.color = '#000000';

			}

			i++;

		}

		return true;

	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	var dragsort = ToolMan.dragsort();
	var junkdrawer = ToolMan.junkdrawer();

	window.onload = function() {
	
		var list = document.getElementById( "ReferenceOrderList" );
		
		if( list != null )
			dragsort.makeListSortable( list , verticalOnly, saveOrder);

	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function verticalOnly(item) {
	
		item.toolManDragGroup.verticalOnly();
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function speak(id, what) {
		
		var element = document.getElementById(id);
		element.innerHTML = 'Clicked ' + what;
	
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function saveOrder(item) {
	
		var group = item.toolManDragGroup;
		var list = group.element.parentNode;
		var id = list.getAttribute("id");
		
		if (id == null) 
			return;
			
		group.register('dragend', function() {
		
			ToolMan.cookies().set("list-" + id, junkdrawer.serializeList(list), 365);
		
		});
	
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function saveReferenceListOrder(){
	
		var ReferenceOrderList = document.getElementById( 'ReferenceOrderList' );
		
		if( ReferenceOrderList == null )
			return;
			
		var referenceListOrder = ToolMan.junkdrawer().serializeList( ReferenceOrderList );
		
		document.getElementById( 'ArticleForm' ).elements[ 'ReferenceListOrder' ].value = referenceListOrder;
	
	}

	/* ------------------------------------------------------------------------------------------------------------ */
	
	function displayOrderEditor(){
	
		var editor = document.getElementById( 'ReferenceOrderList' );
		var text = document.getElementById( 'ReferenceOrderListText' );
		
		editor.style.display = editor.style.display == 'block' ? 'none' : 'block';
		text.innerHTML = editor.style.display == 'block' ? "Masquer" : "Afficher";
		
	}

	/* ------------------------------------------------------------------------------------------------------------ */

	function displayPriceHistory( idarticle, page ){

		var url = "/product_management/catalog/price_history.php?idarticle=" + idarticle + "&page=" + page;

		$.ajax({

			url: url,
			async: false,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de charger l'historique" ); },
		 	success: function( responseText ){
	
				if( !document.getElementById( 'PriceHistoryDialog' ) ){
				
					$( "#globalMainContent" ).append( '<div id="PriceHistoryDialog"></div>' );
					
					$("#PriceHistoryDialog").dialog({
	        		
	        			title: "Historique des prix",
	        			width:  500,
	        			height: 500,
	        			close: function(event, ui) { 
	        			
	        				$("#PriceHistoryDialog").dialog( 'destroy' ); 
	        				$("#PriceHistoryDialog").remove();
	        				
	        			}
	        		
	        		});
	        		
	        	}
	        	
				$("#PriceHistoryDialog").html( responseText );
        		
			}

		});
		
	}

	/* ------------------------------------------------------------------------------------------------------ */
	
	function updateClones(){
		
		$.ajax({

			url: "/templates/product_management/detail.htm.php?reference=<?php echo urlencode( $article->get( "reference" ) ); ?>&clone=" + escape( $( "#CloneInput" ).val() ),
			async: true,
		 	success: function( responseText ){
		 		
				$( "#Clones" ).html( responseText );
				$( "#CloneInput" ).val( "" );
				
			}

		});
	}

	/* ------------------------------------------------------------------------------------------------------ */
	
	function removeClone( idarticle ){
		
		$.ajax({

			url: "/templates/product_management/detail.htm.php?reference=<?php echo urlencode( $article->get( "reference" ) ); ?>&remove&clone=" + idarticle,
			async: true,
		 	success: function( responseText ){
		 		
				$( "#Clones" ).html( responseText );
				
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<style type="text/css">
<!--

	div.subContent table.dataTable span.mceEditor table.mceLayout td{
		
		border-width:1px;
		padding:0px;
		
	}

	div.subContent table.dataTable span.mceEditor table.mceLayout tr.mceFirst .mceToolbar td{
		
		border-style:none;
		
	}
	
-->
</style>
<?php $lang = User::getInstance()->getLang2();?>
<div class="submitButtonContainer" style="margin-top:20px;">
<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );

if( $akilae_crm == 0 ){ 
    
?>
		<!--<input type="button" class="orangeButton" value="Supprimer" name="" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer cette référence?' ) ) document.location='/product_management/catalog/detail.php?delete&idarticle=<?php echo $article->getId(); ?>';" />-->
	<?php  
    	$codification_ref  = DBUtil::getParameterAdmin("codification");  
         // Si Codification = MANUEL dans parameter_admin
         // Ne pas afficher les boutons "Dupliquer" et "Ajouter une référence" 
       if( $codification_ref != 'MANUEL'):?>
      <input type="button" class="blueButton" value="Dupliquer" onclick="document.location = '/product_management/catalog/detail.php?duplicate&amp;idarticle=<?php echo $article->getId(); ?>';" />
     <?php endif ?>
	<?php } ?>
	
	<input type="submit" class="blueButton" value="Sauvegarder" name="update" />
</div>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="tableContainer">
	<table class="dataTable">
		<tr>
			<th class="filledCell">Référence</th>
			<td><?php echo htmlentities( $article->get( "reference" ) ); ?></td>
	    	<th class="filledCell">Article n°</th>
			<td>
				<input type="text" name="n_article" value="<?php echo $article->get( "n_article" ); ?>" class="textInput" />
			</td>
		</tr>
		<tr>
			<th class="filledCell" style="width:20%;">Fournisseur</th>
	    	<td style="width:30%;">
	    	<?php echo DBUtil::getDBValue( "name", "supplier", "idsupplier", $article->get( "idsupplier" ) ); ?>
	    	</td>
			<th class="filledCell" style="width:20%;">Référence fournisseur</th>
			<td style="width:30%;">
				<input type="text" name="ref_supplier" value="<?php echo $article->get( "ref_supplier" ); ?>" class="textInput" style="width:90%;" />
			</td>
		</tr>
		<tr>
			<th class="filledCell">Désignation courte</th>
			<td colspan="3">
			<input type="text" name="summary<?php echo $lang;?>" value="<?php echo  htmlentities($article->get( "summary$lang" )) ; ?>" class="textInput" style="width:95%;" /></td>
		</tr>
	<?php if($lang!="_1"){ ?>
		<tr>
			<th class="filledCell">Désignation 1</th>
			<td colspan="3">
			<?php
		
				$wysiwyg = new WYSIWYGEditor( "designation_1" );
				$wysiwyg->setHTML( $article->get( "designation_1" ) );
				$wysiwyg->display();
				
			?>
			</td>
		</tr>
<?php } ?>
<tr>
			<th class="filledCell">Désignation</th>
			<td colspan="3">
			<?php
		
				$wysiwyg = new WYSIWYGEditor( "designation$lang" );
				$wysiwyg->setHTML( $article->get( "designation$lang" ) );
				$wysiwyg->display();
				
			?>
			</td>
		</tr>
		<tr>
			<th class="filledCell">Ordre d'affichage</th>
			<td colspan="3">
			<?php
				
				$ret = displayOrderEditor( $article->get( "reference" ) );
				
				if( $ret === false ){
					
					?>
					<input type="text" name="display_detail_order" value="<?php echo $article->get( "display_detail_order "); ?>" class="textInput" style="width:20px;" />
					<?php 
					
					toolTipBox( "Ordre d'affichage de ce détail par rapport aux autres détails de ce produit", 250 );
					
				}
				
			?>	
			</td>
		</tr>
		<tr>
			<th class="filledCell">Disponible à l'affichage</th>
			<td >
				<input type="radio" name="available" value="1"<?php if( $article->get( "available" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" />&nbsp;Oui
				<input type="radio" name="available" value="0"<?php if( !$article->get( "available" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" />&nbsp;Non
			</td>
			<th class="filledCell">Comparateur</th>
			<td>
		<input type="radio" name="serial" value="1"<?php if( $article->get( "serial" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
		<input type="radio" name="serial" value="0"<?php if( !$article->get( "serial" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
			
				<!--<input type="text" name="serial" value="<?php echo $article->get( "serial" ); ?>" class="textInput" />-->
			</td>
		</tr>
		<tr>			
	    	<th class="filledCell">Gencod</th>
			<td colspan="3">
				<input type="text" name="gencod" value="<?php echo $article->get( "gencod" ); ?>" class="textInput" />
			</td>
		</tr>
	</table>
</div>

<div class="subTitleContainer" style="margin-top:15px;">
    <p class="subTitle">Prix</p>
</div>
<div class="tableContainer">
	<table class="dataTable">
		<tr>
			<th class="filledCell">Prix de vente <?php if( B2B_STRATEGY ){ ?> HT <?php } 	else{ ?> TTC  <?php } ?> </th>
			<td>
				<input type="text" name="sellingcost" value="<?php echo number_format( $article->get( "sellingcost" ), 2, ".", "" ) ?>" class="textInput" style="width:60px;" /> ¤
				<?php displayPriceHistory( $article->getId() ); ?>
			</td>
			<th class="filledCell">Prix d'achat HT</th>
			<td>
				<input type="text" name="buyingcost" value="<?php echo number_format( $article->get( "buyingcost" ), 2, ".", "" ); ?>" class="textInput" style="width:60px;" /> ¤
				<?php displayPriceHistory( $article->getId() ); ?>
			</td>
		</tr>
		<tr>
			<th class="filledCell" style="width:20%;">Marge brute</th>
			<td style="width:30%;">
			<?php echo Util::priceFormat( $rough_stroke_amount = $article->get( "sellingcost" ) - $article->get( "buyingcost" ) ); ?>
			</td>
			<th class="filledCell" style="width:20%;">soit</th>
			<td style="width:30%;">
				<?php echo Util::rateFormat( $article->get( "sellingcost" ) > 0.0 ? 100.0 * $rough_stroke_amount / $article->get( "sellingcost" ) : 0.0 ); ?>
			</td>
		</tr>
		<tr>
			<th class="filledCell">Prix tarif fournisseur</th>
			<td>
				<input type="text" name="suppliercost" value="<?php echo Util::numberFormat( $article->get( "suppliercost" ) ); ?>" class="textInput" style="width:60px;" /> ¤
			</td>
			<th class="filledCell">Remise fournisseur (%)</th>
			<td>
				<?php echo Util::rateFormat( $article->get( "suppliercost" ) > 0.0 ? ( $article->get( "suppliercost" ) - $article->get( "buyingcost" ) ) * 100.0 / $article->get( "suppliercost" ) : 0.0 ); ?>
			</td>
		</tr>
		
	</table>
</div>
<script type="text/javascript">
/* <![CDATA[ */

	/* --------------------------------------------------------------------------------------------------- */
	
	function quantityPriceDialog(){
		
		$("#QuantityPriceDialog").dialog({
        		
        	title: "Prix au volume : créer une nouvelle tranche",
        	width: 400,
        	close: function(event, ui) { $( "#QuantityPriceDialog" ).dialog( "destroy" ); }
        
        });

	}

	/* --------------------------------------------------------------------------------------------------- */
	
	function createPriceRange(){

		var data = "min_quantity=" + $( "#min_quantity" ).val() + "&max_quantity=" + $( "#max_quantity" ).val() + "&rate=" + $( "#rate" ).val() + "&price=" + $( "#price" ).val() + "&price_cost=" + $( "#price_cost" ).val() + "&reference=" + $( "#reference" ).val();

		if( !parseInt( $( "#min_quantity" ).val() ) ){

			alert( "Vous devez obligatoirement saisir une quantité minimale" );
			return false;
			
		}

		if( parseFloat( $( "#rate" ).val().replace( new RegExp( ",", "g" ), "." ) ) == 0.0
				&& parseFloat( $( "#price" ).val().replace( new RegExp( ",", "g" ), "." ) ) == 0.0 ){

			alert( "Vous devez obligatoirement saisir un pourcentage de remise ou un prix net" );
			return false;
			
		}
			
		$.ajax({
		 	
			url: "/templates/product_management/detail.htm.php?create=quantity_price&idarticle=<?php echo $article->getId(); ?>",
			async: true,
			data: data,
		 	success: function( responseText ){

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );

	        	$( "#QuantityPriceRanges" ).html( responseText );

			}

		});

		return true;
		
	}
	
	/* --------------------------------------------------------------------------------------------------- */
	
	function deleteQuantityRange( min_quantity, max_quantity ){

		$.ajax({
		 	
			url: "/templates/product_management/detail.htm.php?delete=quantity_price&idarticle=<?php echo $article->getId(); ?>",
			async: true,
			data: "min_quantity=" + min_quantity + "&max_quantity=" + max_quantity,
		 	success: function( responseText ){

				if( responseText != "1" ){

					alert( "Impossible de supprimer la tranche de prix : " + responseText );
					return;
	
				}

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );

				$( "#QuantityRange" + min_quantity + "_" + max_quantity ).remove();

			}

		});

	}

	/* --------------------------------------------------------------------------------------------------- */
	
	function enableQuantityStrategy(){

		$.ajax({
		 	
			url: "/templates/product_management/detail.htm.php?enable=quantity_strategy&idarticle=<?php echo $article->getId(); ?>",
			async: true,
		 	success: function( responseText ){

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );

	        	$( "#QuantityPriceRanges" ).html( responseText );
	        	
			}

		});

	}
	
	/* --------------------------------------------------------------------------------------------------- */

/* ]]> */	
</script>
<div id="QuantityPriceDialog" style="display:none;">
	<div class="tableContainer">
		<table class="dataTable" style="margin-top:5px;">
			<tr>
				<th class="filledCell">Quantité minimum</th>
				<td><input class="textInput" type="text" value="1" id="min_quantity" /></td>
			</tr>
			<tr>
				<th class="filledCell">Quantité maximum ( facultatif )</th>
				<td><input class="textInput" type="text" value="" id="max_quantity" /></td>
			</tr>
			<tr>
				<th class="filledCell">Remise (%)</th>
				<td><input class="textInput" type="text" value="0.0" id="rate" /></td>
			</tr>
			<tr>
				<th class="filledCell">Prix Net HT (¤)</th>
				<td><input type="text" class="textInput" value="0.0" id="price" /></td>
			</tr>
			<tr>
				<th class="filledCell">Prix d'achat</th>
				<td><input type="text" class="textInput" value="0.0" id="price_cost" /></td>
			</tr>
		</table>
	</div>
	<p style="text-align:right; margin-top:5px;">
   
    <input type="hidden" class="textInput" value="<?php echo ( stripslashes( $article->get( "reference" )) ); ?>" id="reference" />
		<input type="button" class="blueButton" value="Annuler" onclick="$('#QuantityPriceDialog').dialog('destroy');" />
		<input type="button" class="blueButton" value="Créer" onclick="if( createPriceRange() ) $('#QuantityPriceDialog').dialog('destroy');" />
	</p>
</div>
<?php

	/* prix au volume */
	
	displayQuantityPrice( $article );
	
?>
<br style="clear:both;" />
<div class="subTitleContainer" style="margin-top:10px;">
    <p class="subTitle">Taxes</p>
</div>
<div class="tableContainer">
	<table class="dataTable">
		<tr>
	    	<th class="filledCell" style="width:25%;">TVA intracommunautaire</th>
	    	<td style="width:25%;">
	    	<?php 
	    	
	    		include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
	    		XHTMLFactory::createSelectElement( "tva", "idtva", "tva", "tva", $article->get( "idvat" ), false, "", "name=\"idvat\"");
	    		
	    	?> %
	    	</td>
	    	<th class="filledCell" style="width:25%;">Code douane</th>
			<td style="width:25%;">
			<?php
	
				include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
				XHTMLFactory::createSelectElement( "code_customhouse", "code_customhouse", "code_customhouse_text", "code_customhouse_text", $article->get( "code_customhouse" ), "", "-", "name=\"code_customhouse\"" );
				
			?>
			</td>
			
		</tr><tr>
		<th class="filledCell">Code eco-taxe</th>
		<td colspan="3">
			<?php /*
	
				include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
				XHTMLFactory::createSelectElement( "code", "code", "description", "description", $article->get( "ecotaxe_code" ), "", "-", "name=\"ecotaxe_code\"" );
				*/
			?>
			<?php
			
				include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
	    		XHTMLFactory::createSelectElement( "ecotaxe", "idecotaxe", "description", "description", $article->get( "unit" ), false, "", "name=\"unit\"");
	    		
			?>
			
			</td>
		</tr>
		<!--
		<tr>	
			<th class="filledCell">Masquer les prix de vente sur le Front Office</th>
			<td>
				<input type="radio" name="hidecost" value="1"<?php if( $article->get( "hidecost" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
				<input type="radio" name="hidecost" value="0"<?php if( !$article->get( "hidecost" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
			</td>
	    	<th class="filledCell">Autoriser le franco de port</th>
			<td>
				<input type="radio" name="franco" value="1"<?php if( $article->get( "franco" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Oui
				<input type="radio" name="franco" value="0"<?php if( !$article->get( "franco" ) ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> Non
			</td>
		</tr>
		<tr>
			<th class="filledCell">Frais généraux</th>
			<td>
				<input type="text" name="overhead_charges" value="<?php echo Util::numberFormat( $article->get( "overhead_charges" ) ); ?>" class="textInput" style="width:40px;" /> ¤
			</td>
			<th class="filledCell">Coûts indirects</th>
			<td>
				<input type="text" name="indirect_expenses" value="<?php echo Util::numberFormat( $article->get( "indirect_expenses" ) ); ?>" class="textInput" style="width:40px;" /> %
			</td>
		</tr>
		-->
	</table>
</div>
<?php

//prix au volume

displayReferenceMultiprices( $article->get( "reference" ) );

?>

<div class="subTitleContainer" style="margin-top:10px;">
    <p class="subTitle">Caractéristiques</p>
</div>
<div class="tableContainer">
	<table class="dataTable">
		<tr>
		    <th class="filledCell" style="width:20%;">Poids net</th>
			<td style="width:30%;">
				<input type="text" name="weight" value="<?php echo Util::numberFormat( $article->get( "weight" ) ); ?>" class="textInput" style="width:40px;" /> Kg
			</td>
	 		<th style="width:20%;" class="filledCell">Poids brut</th>
			<td style="width:30%;">
				<input type="text" name="weight_raw" value="<?php echo Util::numberFormat( $article->get( "weight_raw" ) ); ?>" class="textInput" style="width:40px;" /> Kg
			</td>	
	  	</tr>
	  	<tr>
	    	<th class="filledCell">Unité de vente</th>
			<td colspan="3">
			<?php
			
				include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
	    		XHTMLFactory::createSelectElement( "unit", "idunit", "unit_1", "unit_1", $article->get( "unit" ), false, "", "name=\"unit\"");
	    		
			?>
			</td>
		</tr>
		<tr>		
	    	<th class="filledCell">Qté/lot  client</th>
			<td><input type="text" name="lot" value="<?php echo $article->get( "lot" ) ?>" class="textInput" style="width:40px;" /></td>
			<th class="filledCell">Qté/lot fournisseur</th>
			<td ><input type="text" name="min_cde_supplier" value="<?php echo $article->get( "min_cde_supplier" ) ?>" class="textInput" style="width:40px;" /></td>
		</tr>
		
		<tr>
	    	<th class="filledCell">Garantie</th>
	    	<td>
	    		<input type="text" name="garantee" value="<?php echo htmlentities( $article->get( "garantee" ) ); ?>" class="textInput" />
	    	</td>
	    	<th class="filledCell">Délais de livraison</th>
	    	<td>
	    	<?php 
	    	
	    		include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
	    		XHTMLFactory::createSelectElement( "delay", "iddelay", "delay_1", "delay_1", $article->get( "delivdelay" ), false, "", "name=\"delivdelay\"" );
	    		
	    	?>
	    	</td>
	    </tr>
	 	<tr>
	    	<th class="filledCell">Min. cde client</th>
			<td colspan="3"><input type="text" name="min_cde" value="<?php echo $article->get( "min_cde" ) ?>" class="textInput" style="width:40px;" /></td>
			
		</tr>
		<?php $akilae_deadline = DBUtil::getParameterAdmin("akilae_deadline"); ?>

		<?php if($akilae_deadline==1){ ?>
		<tr>
	    	<th class="filledCell">Echéance (mois)</th>
			<td colspan="3"><input type="text" name="month_term" value="<?php echo $article->get( "month_term" ) ?>" class="textInput" style="width:40px;" /></td>
			
		</tr>
		<?php } ?>
	</table>
</div>
<div class="subTitleContainer" style="margin-top:10px;">
    <p class="subTitle">Stock</p>
</div>
<div class="tableContainer">
	<table class="dataTable">
		<tr>
			<th class="filledCell" style="width:20%;">Stock</th>
			<td style="width:30%;">
				<input type="radio" name="use_stock" value="0"<?php if( $article->get( "stock_level" ) == -1 ) echo " checked=\"checked\""; ?> onclick="$('#stock_level').val(''); $('#stock_level').attr('disabled','disabled' );" /> non géré
				<input type="radio" name="use_stock" value="1"<?php if( $article->get( "stock_level" ) != -1 ) echo " checked=\"checked\""; ?> onclick="$('#stock_level').val('0'); $('#stock_level').attr('disabled','' );" style="margin-left:10px;" /> géré
				<input type="text" id="stock_level" name="stock_level"<?php if( $article->get( "stock_level" ) == -1 ) echo " disabled=\"disabled\""; ?> value="<?php if( $article->get( "stock_level" ) != -1 ) echo $article->get( "stock_level" ); ?>" class="textInput" style="width:40px;" />
				
			</td>
			<th class="filledCell" style="width:20%;">Stock minimum</th>
			<td style="width:30%;">
				<input type="text" name="minstock" value="<?php echo $article->get("minstock"); ?>" class="textInput" style="width:40px;" />
				<?php toolTipBox( 'Stock minimum à partir du quel, les produits sont affichés dans Gestion / Stock', 250, $html = "", $showicon = true ) ?>
			</td>
		</tr>
		
		<tr>
			<th class="filledCell">Allée</th>
			<td>
				<input type="text" name="store" value="<?php echo $article->get( "store" ); ?>" class="textInput" />
			</td>
			<th class="filledCell">Rayon</th>
			<td>
				<input type="text" name="span" value="<?php echo $article->get( "span" ); ?>" class="textInput" />
			</td>
		</tr>
		<tr>
			<th class="filledCell">Site de stockage</th>
			<td colspan="3">
				<input type="text" name="site" value="<?php echo $article->get( "site" ); ?>" class="textInput" />
			</td>
		</tr>
	</table>
</div>
<div class="subTitleContainer" style="margin-top:10px;">
    <p class="subTitle">Intitulés</p>
</div>
<div class="tableContainer">
	<table class="dataTable">
	<?php
									
		// Affichage des intitules
								
		foreach( $product->getHeadingFamilies() as $idintitule_family => $name ){

			?>										
			<tr>
				<th colspan="2" class="filledCell"><?php echo htmlentities( $name ); ?></th>
			</tr>
			<?php
					
			$it = $product->getHeadings( $idintitule_family )->iterator();
			
			while( $it->hasNext() ){
				
				$heading = $it->next();
				
				?>
				<tr>
					<th class="filledCell" style="width:20%;"><?php echo $heading->getName(); ?></th>
					<td style="width:80%;">
						<input type="text" name="heading[<?php echo $heading->getId(); ?>]" value="<?php echo $article->getHeadingValue( $heading->getId() ); ?>" class="textInput" />
					</td>
				</tr>
				<?php
			
			}
			
		}
									
	?>
	</table>
</div>
<?php
					
//--------------------------------------------------------------------------------------------------------
//promotions

?>
<div class="subTitleContainer" style="margin-top:10px;">
	<p class="subTitle">Promotion</p>
</div>
<div style="float:right;">
	<a href="#" class="blueLink" onclick="window.open( '/product_management/catalog/promotions.php?reference=<?php echo $article->get( "reference" ); ?>', '_blank', 'directories=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, left=' + (self.screen.width-700)/2 + ', top=' + (self.screen.height-450)/2 + ', width=720, height=450' ); return false;">
		Modifier les promotions
	</a>
</div>
<div class="clear"></div>
<?php
									
	if( $promotion = $article->getPromotion() ){
										
		?>
		<div style="margin:15px 0px;">
			Cette référence est en promotion à <b>- <?php echo Util::rateFormat( $promotion->rate ); ?>
			</b> du <b><?php echo Util::dateFormat( $promotion->begin_date, "d/m/Y" ); ?></b> 
			au <b><?php echo Util::dateFormat( $promotion->end_date, "d/m/Y" ); ?></b>.
		</div>
		<?php
										
	}else{
		
		?>
		<div style="margin:15px 0px;">Cette référence n'est pas en promotion actuellement</div>
		<?php
	
	}
							


//-----------------------------------------------------------------------------------------------------

function displayOrderEditor( $reference ){
	
	$query = "SELECT idproduct AS idparent FROM detail WHERE reference LIKE '$reference' LIMIT 1";
	
	$rs = DBUtil::query( $query );
		
	if( $rs === false  )
		die( "Impossible de récupérer le produit parent $query" );
	
	if( !$rs->RecordCount() )
		return;
		
	$idparent = $rs->fields( "idparent" );
		
	$query = "
	SELECT d.idarticle AS idchild, 
		d.reference,
		d.display_detail_order
	FROM detail d
	WHERE d.idproduct = '$idparent'
	ORDER BY d.display_detail_order ASC";
	
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des références dans le même produit $query" );

	if( !$rs->RecordCount() ){
	
		?>
		<input type="hidden" id="ReferenceListOrder" name="ReferenceListOrder" value="" />
		<?php
		
		return;
		
	}
	
	?>
	<style type="text/css">
		
		#ReferenceOrderList {
			
			margin-bottom: 1em;
			
		}
		
		#ReferenceOrderList li{
			
			margin-bottom: 0px;
			margin-top: 4px;
			
		}
		
		ul.boxy {
			
			list-style-type: none;
			padding: 0px;
			margin: 0px;
			width:210px;
			font-size: 13px;
			font-family: Arial, sans-serif;
			
		}
		
		ul.boxy li {
			
			margin: 0px;
			cursor:move;
			padding: 2px 2px;
			border: 1px solid #ccc;
			background-color: #eee;
			
		}

	
	</style>
	<a href="#" onclick="displayOrderEditor(); return false;" class="blueLink">[<span id="ReferenceOrderListText">Afficher</span> le tri]</a>
	<ul id="ReferenceOrderList" class="boxy" style="display:none;">
	<?php
	
		$rs->MoveFirst();
		
		while( !$rs->EOF() ){
			
			$child_reference = $rs->fields( "reference" );
			$idchild = $rs->fields( "idchild" );
			$displayorder = $rs->fields( "display_detail_order" );
			
			$style = $child_reference == $reference ? " style=\"background-color:#FFFFFF;\"" : "";
			
			?>
			<li itemID="<?php echo $idchild ?>"<?php echo $style ?>">
				<?php echo htmlentities( $child_reference ) ?>
			</li>
			<?php
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
	<input type="hidden" id="ReferenceListOrder" name="ReferenceListOrder" value="" />
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listDetailTables( $idproduct, $selectedIndex ){
		
	$query = "
	SELECT table_name_1, table_name_2, table_name_3, table_name_4, table_name_5
	FROM product
	WHERE idproduct = '$idproduct'
	LIMIT 1";
	
	$rs = DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer la liste des tableaux de références" );

	$tableUvailable = false;
	$i = 1;
	while( !$tableUvailable && $i < 6 ){
	
		$table = $rs->fields( "table_name_$i" );
		
		if( !empty( $table ) )
			$tableUvailable = true;
			
		$i++;
		
	}
	
	if( !$tableUvailable ){

		echo "<input type=\"hidden\" name=\"table\" value=\"\" />non disponible";	
		return;
		
	}
	
	?>
	<select name="table">
		<option value="0"></option>
		<?php

		$i = 1;
		while( $i < 6 ){
		
			$selected = $selectedIndex == $i ? " selected=\"selected\"" : "";
			$table = $rs->fields( "table_name_$i" );
			
			if( !empty( $table ) ){
				
				?>
				<option value="<?php echo $i ?>"<?php echo $selected ?>><?php echo htmlentities( $table ) ?></option>
				<?php
			
			}

			$i++;;
			
		}
		
	?>
	</select>
	<?php

}

//-----------------------------------------------------------------------------------------------------

function displayReferenceMultiprices( $reference ){

	$buyingcost = DBUtil::getDBValue( "buyingcost", "detail", "reference", $reference );
	
	$query = "
	SELECT mf.*, bf.name
	FROM multiprice_family mf, buyer_family bf
	WHERE mf.reference LIKE '" . Util::html_escape( $reference ) . "'
	AND mf.end_date > NOW()
	AND mf.idbuyer_family = bf.idbuyer_family
	ORDER BY bf.name ASC, mf.begin_date ASC";
	
	$rs =& DBUtil::query( $query );
	
	$last_family = false;
	if( $rs->RecordCount() ){
		
		?>
		<p style="font-size:11px; font-weight:bold; margin-top:15px; text-decoration:underline;">Prix au volume</p>
		<table class="dataTable resultTable">
		<?php
		
		while( !$rs->EOF() ){
			
			if( $rs->fields( "name" ) != $last_family ){
				
				?>
			<thead>
				<tr>
					<td colspan="6" class="tableSeparator"></td>
				</tr>
				<tr>
					<th colspan="6"><?php echo htmlentities( $rs->fields( "name" ) ) ?></th>
				</tr>
				<tr>
					<th>Période du</th>
					<th>au</th>
					<th>Quantité entre</th>
					<th>et</th>
					<th>Taux de remise</th>
					<th>Prix d'achat</th>
				</tr>
			</thead>
				<?php
				
				$last_family = $rs->fields( "name" );
				
			}
			
			$discountPrice = $buyingcost * ( 1.0 - floatval( $rs->fields( "rate" ) ) / 100.0 );
			
			?>
			<tbody>
				<tr>
					<td class="lefterCol" style="text-align:center;"><?php echo usDate2eu( $rs->fields( "begin_date" ) ) ?></th>
					<td style="text-align:center;"><?php echo usDate2eu( $rs->fields( "end_date" ) ) ?></th>
					<td style="text-align:right;"><?php echo $rs->fields( "quantity_min" ) ?></th>
					<td style="text-align:right;"><?php echo $rs->fields( "quantity_max" ) ?></th>
					<td style="text-align:right;"><?php echo Util::rateFormat( $rs->fields( "rate" ) ) ?></th>
					<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $discountPrice ) ?></th>
				</tr>
			</tbody>
			<?php
			
			$rs->MoveNext();
			
		}
		
		?>
		</table>
		<?php
	
	}	

}

//-----------------------------------------------------------------------------------------------------

function displayPriceHistory( $idarticle ){
	
	$query = "( SELECT idarticle FROM sellingcost_history WHERE idarticle = '$idarticle' ) UNION ALL ( SELECT idarticle FROM buyingcost_history WHERE idarticle = '$idarticle' ) LIMIT 1";
	
	if( !DBUtil::query( $query )->RecordCount() )
		return;
				
	?>
	<a class="blueLink" href="#" onclick="displayPriceHistory( <?php echo $idarticle; ?>, 1 ); return false;">
		historique
	</a>
	<?php

}

//-----------------------------------------------------------------------------------------------------

function listClones( $reference ){
	
	$clone_id = DBUtil::getDBValue( "clone_id", "detail", "reference", $reference );
	
	if( !$clone_id )
		return;
		    				
	?>
	<div class="tableContainer" style="margin-top:5px;">
		<table class="dataTable" style="margin:5px;">
			<thead>
				<th class="filledCell"></th>
				<th class="filledCell" style="text-align:center;">Réf. Catalogue</th>
				<th class="filledCell" style="text-align:center;">Fournisseur</th>
				<th class="filledCell" style="text-align:center;">Réf. Fournisseur</th>
				<th class="filledCell" style="text-align:center;">Prix tarif fournisseur</th>
				<th class="filledCell" style="text-align:center;">Prix d'achat HT</th>
				<th class="filledCell" style="text-align:center;">Prix de vente HT</th>
			</thead>
			<tbody>
			<?php
		
				$query = "
				SELECT d.idarticle, d.reference, d.ref_supplier, d.idsupplier, d.buyingcost, d.suppliercost, d.sellingcost, d.stock_level, s.name AS supplierName
				FROM detail d, supplier s 
				WHERE d.clone_id = '$clone_id'
				AND s.idsupplier = d.idsupplier
				ORDER BY d.buyingcost, d.idsupplier, d.reference";
				
				$rs =& DBUtil::query( $query );
				
				while( !$rs->EOF() ){
				
					?>
					<tr<?php if( $rs->fields( "reference" ) == $reference ) echo " style=\"background-color:#F3F3F3;\""; ?>>
						<td>
							<a href="#" onclick="return false;">
								<img src="/images/back_office/content/corbeille.jpg" alt="" onclick="removeClone(<?php echo $rs->fields( "idarticle" ); ?>); return false;" />
							</a>
						</td>
						<td>
						<?php
						
							if( $rs->fields( "reference" ) != $reference ){
								
								?>
								<a href="/product_management/catalog/detail.php?idarticle=<?php echo $rs->fields( "idarticle" ); ?>" class="blueLink" onclick="window.open(this.href); return false;">
									<?php echo htmlentities( $rs->fields( "reference" ) ); ?>
								</a>
								<?php
								
							}
							else echo "<b>" . htmlentities( $rs->fields( "reference" ) ) . "</b>";
							
						?>
						</td>
						<td><?php echo htmlentities( $rs->fields( "supplierName" ) ); ?></td>
						<td>
						<?php
						
							if( $rs->fields( "reference" ) != $reference ){
								
								?>
								<a href="/product_management/catalog/detail.php?idarticle=<?php echo $rs->fields( "idarticle" ); ?>" class="blueLink" onclick="window.open(this.href); return false;">
									<?php echo htmlentities( $rs->fields( "ref_supplier" ) ); ?>
								</a>
								<?php
								
							}
							else echo "<b>" . htmlentities( $rs->fields( "ref_supplier" ) ) . "</b>";
							
						?>
						</td>
						<td style="text-align:right;"><?php echo $rs->fields( "suppliercost" ) > 0.0 ? Util::priceFormat( $rs->fields( "suppliercost" ) ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo $rs->fields( "buyingcost" ) > 0.0 ? Util::priceFormat( $rs->fields( "buyingcost" ) ) : "-"; ?></td>
						<td style="text-align:right;"><?php echo $rs->fields( "sellingcost" ) > 0.0 ? Util::priceFormat( $rs->fields( "sellingcost" ) ) : "-"; ?></td>
						<?php /*
						<td style="text-align:right;">
						<?php 
						
							switch( $rs->fields( "stock_level" ) ){
								
								case -1 : echo "non géré"; break;
								case 0  : echo "<span style=\"color:#FF0000;\">épuisé</span>"; break;
								default : echo $rs->fields( "stock_level" );
								
							}
							
						?>
						</td>
						*/
					?>
					</tr>
					<?php
					
					$rs->MoveNext();
						
				}
				
			?>
			</tbody>
		</table>
	</div>
	<?php
    				

}

//-----------------------------------------------------------------------------------------------------

function displayQuantityPrice( CArticle $article ){

	?>
	<div id="QuantityPriceRanges">
		<div class="tableContainer">
			<p><b>Prix au volume :</b></p>
			<p>
				<span style="float:right;">
					<a href="#" onclick="quantityPriceDialog(); return false;">
						<img src="/images/back_office/content/flexigrid_button_create.png" alt="" /> créer une nouvelle tranche
					</a>
				</span>
				<span style="float:left;">
					<?php $enabled = DBUtil::getDBValue( "enabled", "product_price_strategy", "strategy", "QuantityStrategy" ); ?>
					La tarification au volume est <?php echo $enabled ?  "activée" : "désactivée"; ?> sur le site.
					<a href="#" class="blueLink" onclick="enableQuantityStrategy(); return false;">Cliquez ici pour <?php echo $enabled ?  "désactiver" : "activer"; ?></a> cette tarification.
				</span>
			</p>
			<?php
			
				$ranges = $article->getPriceRanges();
				
				if( !$ranges ){
					
						?>
						</div>
					</div><!-- QuantityPriceRanges -->
					<?php
					
					return;
					
				}
				
			?>
			<table class="dataTable" style="margin-top:5px;">
				<tr>
					<th class="filledCell"></th>
					<th class="filledCell" style="text-align:center;">Nbre de pièces</th>
					<th class="filledCell" style="text-align:center;">Remise(%) ou Prix Net(¤)</th>
					<th class="filledCell" style="text-align:center;">Prix d'achat</th>
				</tr>
				<?php
				
					foreach( $ranges as $range ){
						
						?>
						<tr id="QuantityRange<?php echo $range->min_quantity; ?>_<?php echo $range->max_quantity ? $range->max_quantity : 0; ?>">
							<td style="width:15px; text-align:center;">
								<a href="#" onclick="deleteQuantityRange(<?php echo $range->min_quantity; ?>, <?php echo $range->max_quantity ? $range->max_quantity : 0; ?>); return false;">
									<img src="/images/back_office/content/corbeille.jpg" alt="" />
								</a>
							</td>
							<td style="text-align:center;">
							<?php
							
								if( $range->max_quantity )
									echo $range->min_quantity . " - " . $range->max_quantity;
								else echo "&#8805; " . $range->min_quantity . " pièces";
								
							?>
							</td>
							<td style="text-align:center;">
							<?php
							
								if( $range->rate > 0.0 )
										echo " -" . Util::rateFormat( $range->rate );
								else 	echo Util::priceFormat( $range->price );
								
							?>
							</td>
							<td>
							<?php echo Util::priceFormat( $range->price_cost ); ?>
							</td>
						</tr>
						<?php
		
				}
				
			?>
			</table>
		</div>
	</div><!-- QuantityPriceRanges -->
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>