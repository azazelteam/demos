<?php
/**
 * Gestion produits
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Produits associés
 */

include_once( "../../objects/Util.php" );

if( !count( $_FILES ) ){ //@todo : bug plugin Ajax?? l'encodage est différent selon que des fichiers soient postés ou non

	$_POST 		= Util::arrayMapRecursive( "Util::doNothing", $_POST );
	$_REQUEST 	= Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	$_GET 		= Util::arrayMapRecursive( "Util::doNothing", $_GET );

}

//--------------------------------------------------------------------------------------------------------
/* tri des produits associés */

if( isset( $_POST[ "sort" ] ) && isset( $_POST[ "Complementes" ] ) ){

	include_once( "../../config/init.php" );
	
	$done = true;
	$i = 0;
	while( $i < count( $_POST[ "Complementes" ] ) ){
		
		$idproduct = intval( $_POST[ "Complementes" ][ $i ] );
		$rs =& DBUtil::query( "UPDATE complement_product SET displayorder = '$i' WHERE idproduct = '" . intval( $_POST[ "idproduct" ] ) . "' AND complement_product = '$idproduct' LIMIT 1" );

		$done &= $rs !== false;
		
		$i++;
		
	}
	
	exit( $done ? "1" : "0" );

}

//--------------------------------------------------------------------------------------------------------
/* ajout d'un produit associé */

if( isset( $_REQUEST[ "create" ] ) ){

	include_once( "../../config/init.php" );
	
	$lang = User::getInstance()->getLang();

	/* vérifier si l'option existe */
	
	if( !DBUtil::query( "SELECT idproduct FROM product WHERE idproduct = '" . intval( $_REQUEST[ "complement_product" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( 0 );
	
	/* ajouter l'option */
	
	$displayOrder = DBUtil::query( "SELECT MAX( displayorder ) + 1 AS displayorder FROM complement_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'" )->fields( "displayorder" ) || 1;

	$query = "
	INSERT IGNORE INTO complement_product(
		idproduct, 
		complement_product, 
		displayorder, 
		lastupdate, 
		username
	) VALUES (
		'" . intval( $_REQUEST[ "idproduct" ] ) . "',
		'" . intval( $_REQUEST[ "complement_product" ] ) . "',
		'$displayOrder',
		NOW(),
		'" . Util::html_escape( User::getInstance()->get( "login" ) ) . "'
	)";
	
	DBUtil::query( $query );
	
	exit( "1" );
	
}

//--------------------------------------------------------------------------------------------------------
/* suppression d'un produit associé */

if( isset( $_REQUEST[ "delete" ] ) ){

	include_once( "../../config/init.php" );
	
	$rs =& DBUtil::query( "DELETE FROM complement_product WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND complement_product = '" . intval( $_REQUEST[ "associated" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "0" : "1" );
	
}

//--------------------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );

//---------------------------------------------------------------------------------------------

?>
<style type="text/css">
		
	#ComplementList {
		
		margin: 20px auto 20px auto;
		list-style-type: none;
		padding: 0px;
		
	}
	
	#ComplementList li{
		
		margin-bottom: 0px;
		margin-top: 4px;
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		margin:4px;
		
	}

	#ComplementList .placeholder{
		
		width:60px;
		height:60px;
		float:left;
		border:1px solid #E7E7E7;
		background-color:#F3F3F3;
		
	}
	
	#ComplementList li a{ cursor:move; }
	
</style>
<script type="text/javascript">
/* <!CDATA[ */

	/* ------------------------------------------------------------------------------------------------------------ */
	/* inititalisation tri des produits associés */
	
	$(document).ready(function(){
	
		$("#ComplementList").sortable({
					
			placeholder: 'placeholder',
			forcePlaceholderSize: true,
			update: function(event, ui){ sortComplementedItems( $("#ComplementList").sortable('serialize') ); }
		
		});
		
		$("#AssociateList").disableSelection();
		
	});
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function deleteComplementedProduct( idproduct ){
	
		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&associated=" + idproduct;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_complements.htm.php?delete",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        			$( "#ProductTabs" ).tabs( "load", 5 );
        		
        		}
        		else alert( "Impossible d'enregistrer les modifications" );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
	function sortComplementedItems( serializedString ){
	
		var data = "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&sort=1&" + serializedString;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/templates/product_management/product_complements.htm.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText == "1" ){
				
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
	        		$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
        	
        		}
        		else alert( "Impossible d'enregistrer les modifications" );
        		
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------- */
	
	function createComplementedProduct(){

		if( !$( "#ComplementedProduct" ).attr( "value" ).length )
			return;

		var postdata = "complement_product=" + escape( $( "#ComplementedProduct" ).attr( "value" ) );
		postdata += "&idproduct=<?php echo $tableobj->get( "idproduct" ); ?>";

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_complements.htm.php?create",
			async: true,
			cache: false,
			type: "GET",
			data: postdata,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer l'option" ); },
		 	success: function( responseText ){

				if( responseText == "0" )
					alert( "Impossible de créer l'option" );
				else $( "#ProductTabs" ).tabs( "load", 5 );
				
			}

		});

	}
	
	/* ------------------------------------------------------------------------------------------------------------ */
	
/* ]]> */
</script>
<div class="subTitleContainer" style="padding-top:10px;">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_complements.htm.php</span>
<?php } ?>


    <p class="subTitle">Complémentaires</p>
</div>
<p style="margin:10px 0px;">
	N° du produit : 
	<input type="text" id="ComplementedProduct" name="" value="" class="textInput" onkeydown="if( event.keyCode == 13 ){ $( this ).bind( 'keyup', function(e){createComplementedProduct();} ); return false; }" style="width:250px;" />
	<?php
	
		include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
		//AutoCompletor::completeFromDB( "ComplementedProduct", "product", "idproduct", 15, "createComplementedProduct" ); 
		AutoCompletor::completeFromDBPattern( "ComplementedProduct", "product", "idproduct", "{name_1}", 15, "createComplementedProduct" ); 
		
	?>
	<input type="button" name="" value="Créer" class="blueButton" onclick="createComplementedProduct();" />
</p>
<p>
	<ul style="margin-top:20px;">
		<li>Glissez-déposez les éléments ci-dessous pour réorganiser l'ordre d'affichage des produits sur le site.</li>
		<li>Cliquez sur un produit ci-dessous pour accéder à sa fiche.</li>
	</ul>
</p>
<p>
<?php

	//-----------------------------------------------------------------------------------------------------
	
	$lang = User::getInstance()->getLang();
	
	$query = "
	SELECT p.idproduct, 
		p.name$lang,
		p.available
	FROM complement_product ap, product p
	WHERE ap.idproduct = '" . $tableobj->get( "idproduct" ) . "'
	AND ap.complement_product = p.idproduct
	ORDER BY ap.displayorder ASC";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<ul id="ComplementList">
	<?php 

		while( !$rs->EOF() ){
			
			$name = $rs->fields( "name$lang" );
			
			?>
			<li id="Complementes_<?php echo $rs->fields( "idproduct" ) ?>">
				<div style="position:relative;">
					<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $rs->fields( "idproduct" ); ?>">
						<img src="<?php echo URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_CUSTOM_SIZE,0 , 60, 60 ); ?>" title="<?php echo htmlentities( $name ); ?>" alt="" style="opacity:<?php echo $rs->fields( "available" ) ? "1.0" : "0.4"; ?>;" />
					</a>
					<a href="#" onclick="deleteComplementedProduct( <?php echo $rs->fields( "idproduct" ) ?> ); return false;" style="cursor:pointer;">
						<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'option" alt="supprimer l'option" style="position:absolute; bottom:2px; left:2px;" />
					</a>
				</div>
			</li>
			<?php 
			
			$rs->MoveNext();
			
		}
		
	?>
	</ul>
</p>
<div class="clear" style="margin-bottom:10px;"></div>
<?php 

//-----------------------------------------------------------------------------------------------------

?>