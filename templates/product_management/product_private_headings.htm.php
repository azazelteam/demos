<?php

//-----------------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );


$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );
$langue=User::getInstance()->getLang();
//-----------------------------------------------------------------------------------------------------
//intitulés indépendants de la catégorie

$idproduct = $tableobj->get( "idproduct" );
displayPrivateHeadings( $idproduct );

//-----------------------------------------------------------------------------------------------------

function displayPrivateHeadings( $idproduct ){

	global $base, $langue;
	
	$db =& DBUtil::getConnection();

	//nombre d'intitulés privés utilisés
	
	$privateHeadingCount = 10;
	
	//afficher l'éditeur
	
	?>
    
	<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_private_headings.htm.php</span>
<?php } ?>
	
			<div class="subTitleContainer" style="padding-top:10px;">
                <p class="subTitle"><?php echo htmlentities( Dictionnary::translate( "gest_intitule" ) ) ; ?></p>
			</div>

	<input type="hidden" name="UpdatePrivateHeadings" value="1" />

<div class="tableContainer">
	<table class="dataTable">
	<?php
	
	//récupérer les intitulés spécifiques au produit ( indépendants de la catégorie )
	
	$query = "
	SELECT it.idintitule_title, 
		il.cotation,
		it.intitule_title$langue AS intitule_title, 
		il.title_display
	FROM intitule_link il, intitule_title it
	WHERE il.idcategory = 0
	AND il.idarticle = 0
	AND il.idfamily = 0
	AND il.idproduct = '$idproduct'
	AND il.idintitule_title = it.idintitule_title
	ORDER BY title_display ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les intitulés propres au produit '$idporduct'" );
		
	$i = 0;
	while( !$rs->EOF() ){
	
		?>
		<tr>
			<td>
				<input type="hidden" name="PrivateHeadingIds[]" value="<?php echo $rs->fields( "idintitule_title" ) ?>" />
				Ordre :<?php getOrderSelectElement( $privateHeadingCount, $rs->fields( "title_display" ) ); ?>
			</td>
			<td>
				<input type="text" name="PrivateHeadings[]" value="<?php echo htmlentities( $rs->fields( "intitule_title" ) ) ?>" class="textInput" />
			</td>
			<td>
				Cotes :<?php echo getCotationSelectElement( $rs->fields( "cotation" ) ) ?>
			</td>
		</tr>
		<?php
		
		$i++;
		
		$rs->MoveNext();
		
	}
	
	//intitulés privés disponibles ( non utilisés )
	
	while( $i < $privateHeadingCount ){
	
		?>
		<tr>
			<td>
				<input type="hidden" name="PrivateHeadingIds[]" value="0" />
				Ordre :<?php getOrderSelectElement( $privateHeadingCount ); ?>
			</td>
			<td>
				<input type="text" name="PrivateHeadings[]" value="" class="textInput" />
			</td>
			<td>
				Cotes :<?php echo getCotationSelectElement() ?>
			</td>
		</tr>
		<?php
		
		$i++;
		
	}
	
	?>
	</table>
</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function getCotationSelectElement( $selectedCotation = "" ){

	global $base, $langue;
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT idcotation, cotation FROM cotation ORDER BY cotation ASC";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des cotations" );

	?>
	<select name="PrivateCotations[]">
	<?php
	
	while( !$rs->EOF() ){
	
		$selected = $rs->fields( "cotation" ) == $selectedCotation ? " selected=\"selected\"" : "";

		?>
		<option value="<?php echo $rs->fields( "cotation" ) ?>"<?php echo $selected ?>><?php echo htmlentities( $rs->fields( "cotation" ) ) ?></option>
		<?php
		
		$rs->MoveNext();
		
	}
	
	?>
	</select>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function getOrderSelectElement( $optionCount, $selectedOrder = 0 ){

	?>
	<select name="PrivateHeadingOrders[]">
	<?php
	
	$i = 0;
	while( $i < $optionCount ){
	
		$selected = $selectedOrder == $i ? " selected=\"selected\"" : "";
		
		?>
		<option value="<?php echo $i ?>"<?php echo $selected ?>><?php echo $i ?></option>
		<?php
		
		$i++;
		
	}
	
	?>
	</select>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>