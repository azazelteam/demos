<?php
/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Commentaires produits
 */

 include_once( dirname( __FILE__ ) . "/../../config/init.php" );
if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );

if( !User::getInstance()->getId() ){

	include( dirname( __FILE__ ) . "/../back_office/head.php" );
	include( dirname( __FILE__ ) . "/../back_office/foot.php" );

	exit();

}
?>

<?php

include_once ("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
include_once ("$GLOBAL_START_PATH/objects/AutoCompletor.php");
/* ---------------------------------------------------------------------------------------------- */
/* enregistrement nouveau commentaire */

if( isset( $_REQUEST[ "idproduct_comment" ] ) && isset( $_REQUEST[ "validate" ] ) ){

	$ret = DBUtil::query( "UPDATE product_comment SET accept_admin = '" . intval( $_REQUEST[ "validate" ] ) . "' WHERE idproduct_comment = '" . intval( $_REQUEST[ "idproduct_comment" ] ) . "' LIMIT 1" );

	if( isset( $_REQUEST[ "referer" ] ) && $_REQUEST[ "referer" ] == "mail" )//depuis le courriel
		header( "Location: " . URLFactory::getHostURL() . "/product_management/catalog/cat_product.php?idproduct=" . intval( $_REQUEST[ "idproduct" ] ) . "&tab=0" );

	exit( $ret === false ? "0" : "1" );

}

/* ---------------------------------------------------------------------------------------------- */
/* suppression d'un commentaire */

if( isset( $_REQUEST[ "idproduct_comment" ] ) && isset( $_REQUEST[ "delete" ] ) ){

	$ret = DBUtil::query( "DELETE FROM product_comment WHERE idproduct_comment = '" . intval( $_REQUEST[ "idproduct_comment" ] ) . "' LIMIT 1" );

	exit( $ret === false ? "0" : "1" );

}

if( isset( $_REQUEST[ "idproduct_comment" ] ) && isset( $_REQUEST[ "update" ] ) ){
	$date_aff = date("Y-m-d H:i:s", strtotime(urldecode($_REQUEST[ "date_aff" ]) ));
	$ret = DBUtil::query( "UPDATE product_comment SET date_affich='" . $date_aff  . "' , product_comment = '" .addslashes(  $_REQUEST[ "comment" ] ) . "' WHERE idproduct_comment = '" . intval( $_REQUEST[ "idproduct_comment" ] ) . "' LIMIT 1" );
	
//echo "UPDATE product_comment SET product_comment = '" .addslashes(  $_REQUEST[ "comment" ] ) . "', date_affich='" . $date_aff  . "'  WHERE idproduct_comment = '" . intval( $_REQUEST[ "idproduct_comment" ] ) . "' LIMIT 1";
	if( isset( $_REQUEST[ "referer" ] ) && $_REQUEST[ "referer" ] == "mail" )//depuis le courriel
		header( "Location: " . URLFactory::getHostURL() . "/product_management/catalog/cat_product.php?idproduct=" . intval( $_REQUEST[ "idproduct" ] ) . "&tab=0" );

	exit( $ret === false ? "0" : "1" );

}
/* ---------------------------------------------------------------------------------------------- */
$rs =& DBUtil::query( "SELECT * FROM product_comment WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' ORDER BY date desc" );
//echo "SELECT * FROM product_comment WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' ORDER BY date $order_by";
if( !$rs->RecordCount() ){

	?>
	<p style="text-align:center;">Aucun commentaire trouvé</p>
	<?php

	exit();

}

?>
<style type="text/css">

	#CustomerComments{

		padding:0px;

	}

	#CustomerComments{

		list-style-type:none;
		margin:10px;

	}

	#CustomerComments li{

		width:70%;
		padding:10px;
		border:1px solid #E7E7E7;
		margin:1px;
		background-color:#FFFFFF;

	}
	#CustomerComments li .recycle{

		position:absolute;
		margin-left:-40px;

	}

	#CustomerComments li .available{ float:right; margin-left:10px; cursor:pointer; }

</style>
<script type="text/javascript">
/* <![CDATA[ */

	function validateComment( idproduct_comment ){

		var validate = $( "#accept_admin" + idproduct_comment ).val() == 1 ? 0 : 1;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_comments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&idproduct_comment=" + idproduct_comment + "&validate=" + validate,
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert(responseText );
				else{

					$( "#ValidateIcon" + idproduct_comment ).attr( "src", validate ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif" );
					$( "#accept_admin" + idproduct_comment ).val( validate );
					$( "#Comment" + idproduct_comment ).css( "background-color", validate ? "#FFFFFF" : "#FAFAFA" );

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

				}

			}

		});

	}



	function deleteComment( idproduct_comment ){

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_comments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&idproduct_comment=" + idproduct_comment + "&delete",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText != "1" )
					alert(responseText );
				else{

					$( "#Comment" + idproduct_comment ).remove();

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

				}

			}

		});

	}
function updateComment( idproduct_comment ){

		var comment = $( "#comment" + idproduct_comment ).text();
		var date_aff =encodeURI( $( "#date_aff" + idproduct_comment ).val());
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_comments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&idproduct_comment=" + idproduct_comment +"&comment="+comment+ "&date_aff="+date_aff+"&update",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
		 	success: function( responseText ){

				if( responseText =="1" )
					

					//$( "#ValidateIcon" + idproduct_comment ).attr( "src", validate ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif" );
					//$( "#accept_admin" + idproduct_comment ).val( validate );
					//$( "#Comment" + idproduct_comment ).css( "background-color", validate ? "#FFFFFF" : "#FAFAFA" );
				//$.('btn_update'+ idproduct_comment).css.display="none";
					$('#btn_update'+ idproduct_comment).css("display","none");
					$('#comment'+ idproduct_comment).css("height","");
					$('#comment'+ idproduct_comment).css("width","");
					$('#comment'+ idproduct_comment).css("position","");
					$('#comment'+ idproduct_comment).css("padding","");
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );

				

			}

		});

	}
	function enableEdit(idproduct_comment ){

		//var comment = $( "#comment" + idproduct_comment ).val();
	var editorBtn = document.getElementById('btn_update'+ idproduct_comment);
	var element = document.getElementById('comment'+ idproduct_comment);

	    element.contentEditable = 'true';
	    //editorBtn.innerHTML = 'Enable Editing';
	  document.getElementById('btn_update'+ idproduct_comment).style.display="block";

	element.style.position="relative";
	element.style.height="100px";
	element.style.width="100%";
	element.style.padding="0px";



}

/* ---------------------------- */
/* XMLHTTPRequest Enable   */
/* ---------------------------- */
function createObject() {
 var request_type;
 var browser = navigator.appName;
 if(browser == "Microsoft Internet Explorer"){
 request_type = new ActiveXObject("Microsoft.XMLHTTP");
 }else{
  request_type = new XMLHttpRequest();
 }
  return request_type;
}

var http = createObject();

/* -------------------------- */
/* SEARCH      */
/* -------------------------- */
function autosuggest() {
var date_tri =document.getElementById('date_tri').value;
nocache = Math.random();
http.open('get','<?php echo $GLOBAL_START_URL; ?>/templates/product_management/sort_productComments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&date_tri='+ date_tri);
http.onreadystatechange = autosuggestReply;
http.send(null);
}
function autosuggest2() {
var e = document.getElementById("statut_tri");

var statut_tri = e.options[e.selectedIndex].value;
//alert(statut_tri);
nocache = Math.random();
http.open('get','<?php echo $GLOBAL_START_URL; ?>/templates/product_management/sort_productComments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&statut_tri='+ statut_tri);
http.onreadystatechange = autosuggestReply;
http.send(null);
}
function autosuggestReply() {
if(http.readyState == 4){
 var response = http.responseText;
 e = document.getElementById('result');
 if(response!=""){
  e.innerHTML=response;
  e.style.display="block";
  allScripts = e.getElementsByTagName("script");
  for (n=0;n<allScripts.length;n++)
  {
  	// eval(allScripts[n].innerHTML);
  }
 } else {
  e.style.display="none";
 }
}
}
/*
  jQuery Document ready
*/

</script>
<style type="text/css">

.custom-date-style {
	background-color: red !important;
}

</style>

<style type="text/css">
.header{ background: url(<?php echo $GLOBAL_START_URL ?>/images/small_sort.gif)no-repeat 29px 7px;
 cursor: pointer;
 font-weight: bold;
border: 1px solid #ccc;
text-align: left;
width: 48px;
height: 22px;
background: url(http://www.astelos-sante.com/images/small_desc.gif) no-repeat 29px 7px;
padding: 2px 5px;
}
.headerSortUp{ background: url(<?php echo $GLOBAL_START_URL ?>/images/small_asc.gif)no-repeat 29px 7px; } 
					
.headerSortDown{ background: url(http://www.astelos-sante.com/images/small_desc.gif) no-repeat 29px 7px; }

</style>
<div class="subTitleContainer" style="cursor:move;">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_comments.php</span>
<?php } ?>


<!--<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery-ui-sliderAccess.js"></script>
<link rel="stylesheet" media="all" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/jquery/jquery-ui-timepicker-addon.css" />-->
<!--
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/jquery/jquery.datetimepicker.css"/>
<script type="text/javascript">
/*
$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	defaultDate:'8.12.1986', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
});

$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
});
$('#datetimepicker_mask').datetimepicker({
	mask:'9999/19/39 29:59'
});

$('#datetimepicker1').datetimepicker({
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker2').datetimepicker({
	yearOffset:222,
	lang:'ch',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	minDate:'-1970/01/02', // yesterday is minimum date
	maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
$('#datetimepicker3').datetimepicker({
	inline:true
});
$('#datetimepicker4').datetimepicker();
$('#open').click(function(){
	$('#datetimepicker4').datetimepicker('show');
});
$('#close').click(function(){
	$('#datetimepicker4').datetimepicker('hide');
});
$('#reset').click(function(){
	$('#datetimepicker4').datetimepicker('reset');
});
$('#datetimepicker5').datetimepicker({
	datepicker:false,
	allowTimes:['12:00','13:00','15:00','17:00','17:05','17:20','19:00','20:00'],
	step:5
});
$('#datetimepicker6').datetimepicker();
$('#destroy').click(function(){
	if( $('#datetimepicker6').data('xdsoft_datetimepicker') ){
		$('#datetimepicker6').datetimepicker('destroy');
		this.value = 'create';
	}else{
		$('#datetimepicker6').datetimepicker();
		this.value = 'destroy';
	}
});
var logic = function( currentDateTime ){
	if( currentDateTime.getDay()==6 ){
		this.setOptions({
			minTime:'11:00'
		});
	}else
		this.setOptions({
			minTime:'8:00'
		});
};
$('#datetimepicker7').datetimepicker({
	onChangeDateTime:logic,
	onShow:logic
});
$('#datetimepicker8').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date')
			.toggleClass('xdsoft_disabled');
	},
	minDate:'-1970/01/2',
	maxDate:'+1970/01/2',
	timepicker:false
});
$('#datetimepicker9').datetimepicker({
	onGenerate:function( ct ){
		$(this).find('.xdsoft_date.xdsoft_weekend')
			.addClass('xdsoft_disabled');
	},
	weekends:['01.01.2014','02.01.2014','03.01.2014','04.01.2014','05.01.2014','06.01.2014'],
	timepicker:false
});
var dateToDisable = new Date();
	dateToDisable.setDate(dateToDisable.getDate() + 2);
$('#datetimepicker11').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [false, ""]
		}

		return [true, ""];
	}
});
$('#datetimepicker12').datetimepicker({
	beforeShowDay: function(date) {
		if (date.getMonth() == dateToDisable.getMonth() && date.getDate() == dateToDisable.getDate()) {
			return [true, "custom-date-style"];
		}

		return [true, ""];
	}
});*/
$( document ).ready(function() {
$('#date_aff').datetimepicker({
dayOfWeekStart : 1,
lang:'fr',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#date_aff').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();
});
</script>-->
	<p class="subTitle">Commentaires visiteurs</p>
	
</div>
<br>
<div id="result">
<div style="float:right;margin-right: 9px;">

	<form name="tri" action="" method="POST">
		Tri par Date:
	<input type="hidden" id="date_tri" name="date_tri" value="<?php if(isset( $_REQUEST[ "date_tri" ]) && ($_REQUEST[ "date_tri" ]=="asc")) echo "desc"; else echo "asc";?>">
	<input name="sort_by_date" class="header <?php if(isset( $_REQUEST[ "date_tri" ]) && ($_REQUEST[ "date_tri" ]=="asc")) echo "headerSortUp"; else echo "headerSortDown";?>" type="button" onclick="autosuggest();" value="Date">
	&nbsp;&nbsp;&nbsp;
	par statut:
	<select id="statut_tri" name="statut_tri" onchange="autosuggest2();">
		<option value="0" disabled <?php if(!isset( $_REQUEST[ "statut_tri" ])) echo "selected"; else echo "";?>>choisir un statut</option>
		<option value="1" <?php if(isset( $_REQUEST[ "statut_tri" ])&&($_REQUEST[ "statut_tri" ]=="1")) echo "selected"; else echo "";?>>Valid&eacute;</option>
		<option value="0" <?php if(isset( $_REQUEST[ "statut_tri" ])&&($_REQUEST[ "statut_tri" ]=="0")) echo "selected"; else echo "";?>>En attente</option>
		</select>
	</form>
	</div>
	<br>
<!--<p class="header" style="width:59px;float:right;">Date</p>-->
<div  style="padding-left:50px;">
	<ul id="CustomerComments">
	
	<?php

		while( !$rs->EOF() ){
		$idbuyer=$rs->fields( "idbuyer" );
		$idcontact=$rs->fields( "idcontact" );
				$date_aff1=$rs->fields( "date_affich");
		$idproduct_comment=$rs->fields( "idproduct_comment" );;
		unset($city);
		if(($idbuyer!="0"))
				{ 
					
					$rs1 =& DBUtil::query( "SELECT firstname, lastname,company, mail, phonenumber FROM contact c inner join buyer b on c.`idbuyer`=b.`idbuyer`  WHERE c.`idbuyer` = '".$idbuyer."' AND c.`idcontact` = '".$idcontact."' LIMIT 1" );
					$firstname=$rs1->fields( "firstname" );
					$mail=$rs1->fields( "mail" );
					$lastname=$rs1->fields( "lastname" );
					$company=$rs1->fields( "company" );
					$phone=$rs1->fields( "phonenumber" );
				}
				else 
				 {
				 	$firstname=$rs->fields( "user_name" );;
					$lastname=$rs->fields( "user_surname" );;
					$city=$rs->fields( "user_city" );
					}


		//$rs1 =& DBUtil::query( "SELECT firstname, lastname,company, phonenumber FROM contact c inner join buyer b on c.`idbuyer`=b.`idbuyer`  WHERE c.`idbuyer` = '".$idbuyer."' AND c.`idcontact` = '".$idcontact."' LIMIT 1" );

			?>
			<li id="Comment<?php echo $rs->fields( "idproduct_comment" ); ?>" style="background-color:<?php echo $rs->fields( "accept_admin" ) ? "#FFFFFF" : "#FAFAFA"; ?>">
				<span class="available">
					<input type="hidden" id="accept_admin<?php echo $rs->fields( "idproduct_comment" ); ?>" value="<?php echo $rs->fields( "accept_admin" ); ?>" />
					<img id="ValidateIcon<?php echo $rs->fields( "idproduct_comment" ); ?>" onclick="validateComment( <?php echo $rs->fields( "idproduct_comment" ); ?> );" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $rs->fields( "accept_admin" ) ? "visible.gif" : "hidden.gif"; ?>" alt="" />
					<img id="UpdateIcon" onclick="enableEdit(<?php echo $rs->fields( "idproduct_comment" ); ?>);" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/icons-edit.png" alt="" />
					
					<input type="text" style=" width:137px; border: 2px solid #CCC; line-height:20px;height:8px; border-radius:10px; padding:5px;" name="date_aff" id="date_aff<?php echo $rs->fields( "idproduct_comment" ); ?>" class="calendarInput" value="<?php echo date("d-m-Y", strtotime($date_aff1));?>"/> <?php echo DHTMLCalendar::calendar( "date_aff$idproduct_comment" ) ?>
				</span>
				<img class="recycle" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" alt="" style="cursor:pointer;" onclick="deleteComment( <?php echo $rs->fields( "idproduct_comment" ); ?> );" />
				<div>
					<p>
						<?php 	if(($idbuyer!="0"))
				{ 
					?>
						<i><strong><?php echo $company."  N° ".$phone; ?></strong><br></i>
						

						<?php } ?>
						<i>Le <?php echo Util::dateFormatEu( $rs->fields( "date" ) );  ?> par <?php echo $firstname ." ".$lastname; ?> <?php echo ($city ? " à ".$city : ""); ?></i>
											
					<br />Mail : <?php echo $rs->fields( "user_mail" ); ?> <?php echo $mail ; ?>
					
					<div id="comment<?php echo $rs->fields( "idproduct_comment" ); ?>" style="cursor:pointer;  overflow:auto;" >
						<?php echo  Util::doNothing(htmlentities($rs->fields( "product_comment" )) ); ?>

					</div>
					<input type="button" id="btn_update<?php echo $rs->fields( "idproduct_comment" ); ?>" style="display:none;"  onclick="updateComment( <?php echo $rs->fields( "idproduct_comment" ); ?> );" value="Modifier">
				</div>
				<br style="clear:both;" />
			</li>
			<?php

			$rs->MoveNext();

		}

	?>
	</ul>
</div>
</div>