<?php

// Récupére la session pour uploadify
if( isset( $_REQUEST['sid_uploadify'] ) )
{
    @session_id(  $_REQUEST['sid_uploadify']   );
    @session_start();
}
define( "PICTOGRAM_SIZE", 80 );

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des pictogrammes - ajax */

if( isset( $_GET[ "list" ] ) && isset( $_GET[ "list" ] ) == "pictograms" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	listPictograms( intval( $_GET[ "idsupplier" ] ) );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* tri des pictogrammes fournisseur */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "pictograms" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = true;

	$index = 0;
	foreach( $_REQUEST[ "SupplierPictogramIndexes" ] as $index => $idpictogram ){
	
		$ret &= DBUtil::query( "UPDATE supplier_pictogram SET `index` = '$index' WHERE idsupplier = '" . intval( $_REQUEST[ "idsupplier" ] ) . "' AND idpictogram = '" . intval( $idpictogram ) . "' LIMIT 1" ) !== false;
		$index++;
		
	}
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression d'un pictogramme */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "pictogram" ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = DBUtil::query( "DELETE FROM supplier_pictogram WHERE idsupplier = '" . intval( $_REQUEST[ "idsupplier" ] ) . "' AND idpictogram = '" . intval( $_REQUEST[ "idpictogram" ] ) . "' LIMIT 1" );
	
	exit( $ret === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* ajout d'un pictogramme */

if( isset( $_REQUEST[ "add" ] ) && $_REQUEST[ "add" ] == "pictogram" ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
		
	if( DBUtil::query( "SELECT 1 FROM supplier_pictogram WHERE idsupplier = '" . intval( $_REQUEST[ "idsupplier" ] ) . "' AND idpictogram = '" . intval( $_REQUEST[ "idpictogram" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( "Ce pictogramme est déjà sélectionné pour le fournisseur" );

	$index = DBUtil::query( "SELECT COALESCE( MAX( `index` ) + 1, 0 ) AS `index` FROM supplier_pictogram WHERE idsupplier = '" . intval( $_REQUEST[ "idsupplier" ] ) . "'" )->fields( "index" );
	
	$ret = DBUtil::query( 
	
		"INSERT IGNORE INTO supplier_pictogram VALUES( 
			'" . intval( $_REQUEST[ "idsupplier" ] ) . "', 
			'" . intval( $_REQUEST[ "idpictogram" ] ) . "',
			'$index'
		)" 
	
	) !== false;
	
	exit( $ret === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* définir le contact principal - ajax */

if( isset( $_REQUEST[ "main_contact" ] ) && isset( $_REQUEST[ "idsupplier" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );
	
	include( dirname( __FILE__ ) . "/../../lib/contact_editor.php" );
	
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	$supplier->set( "main_contact", intval( $_GET[ "main_contact" ] ) );
	$supplier->save();
	
	$mainContact = false;
	$it = $supplier->getContacts()->iterator();
	
	while( $mainContact === false && $it->hasNext() ){

		$contact =& $it->next();
		
		if( $contact->isMainContact() )
			$mainContact = $contact;
		
	}
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $supplier->getContacts(), $mainContact->getId(), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression d'un contact - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "contact" && isset( $_REQUEST[ "idsupplier" ] ) && isset( $_REQUEST[ "idcontact" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );
	
	include( dirname( __FILE__ ) . "/../../lib/contact_editor.php" );
	
	SupplierContact::delete( intval( $_REQUEST[ "idsupplier" ] ), intval( $_REQUEST[ "idcontact" ] ) );
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $supplier->getContacts(), 0, "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* duplication d'un contact - ajax */

if( isset( $_REQUEST[ "duplicate" ] ) && $_REQUEST[ "duplicate" ] == "contact" && isset( $_REQUEST[ "idsupplier" ] ) && isset( $_REQUEST[ "idcontact" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );

	include( dirname( __FILE__ ) . "/../../lib/contact_editor.php" );
	
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	$done = false;
	$it = $supplier->getContacts()->iterator();
	while( $it->hasNext() ){
		
		$contact =& $it->next();
		
		if( $contact->getId() == intval( $_REQUEST[ "idcontact" ] ) ){

			$clone = $contact->duplicate();
			$done = true;
			
		}
		
	}

	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $supplier->getContacts(), $done ? $clone->getId() : 0, "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* création d'un nouveau contact - ajax */

if( isset( $_REQUEST[ "create" ] ) && $_REQUEST[ "create" ] == "contact" && isset( $_REQUEST[ "idsupplier" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );

	include( dirname( __FILE__ ) . "/../../lib/contact_editor.php" );
	
	$contact = SupplierContact::create( intval( $_REQUEST[ "idsupplier" ] ) );
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayContactEditor( $supplier->getContacts(), $contact->getId(), "setMainContact", "createContact", "deleteContact", "duplicateContact" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression d'une adresse usine - ajax */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "factory" && isset( $_REQUEST[ "idsupplier" ] ) && isset( $_REQUEST[ "idsupplier_factory" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/SupplierFactoryAddress.php" );
	
	include( dirname( __FILE__ ) . "/../../lib/address_editor.php" );
	
	SupplierFactoryAddress::delete( intval( $_REQUEST[ "idsupplier_factory" ] ) );
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayAddressEditor( $supplier->getFactories(), 0, "createFactory", "deleteFactory", "duplicateFactory" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* duplication d'une adresse usine - ajax */

if( isset( $_REQUEST[ "duplicate" ] ) && $_REQUEST[ "duplicate" ] == "factory" && isset( $_REQUEST[ "idsupplier" ] ) && isset( $_REQUEST[ "idsupplier_factory" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/SupplierFactoryAddress.php" );

	include( dirname( __FILE__ ) . "/../../lib/address_editor.php" );
	
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	$done = false;
	$it = $supplier->getFactories()->iterator();
	while( $it->hasNext() ){
		
		$factory =& $it->next();
		
		if( $factory->getId() == intval( $_REQUEST[ "idsupplier_factory" ] ) ){

			$clone = $factory->duplicate();
			$done = true;
			
		}
		
	}

	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayAddressEditor( $supplier->getFactories(), $clone->getId(), "createFactory", "deleteFactory", "duplicateFactory" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* création d'une adresse usine - ajax */

if( isset( $_REQUEST[ "create" ] ) && $_REQUEST[ "create" ] == "factory" && isset( $_REQUEST[ "idsupplier" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/Supplier.php" );
	include_once( dirname( __FILE__ ) . "/../../objects/SupplierFactoryAddress.php" );
	
	include( dirname( __FILE__ ) . "/../../lib/address_editor.php" );
	
	$factory = SupplierFactoryAddress::create( intval( $_REQUEST[ "idsupplier" ] ) );
	$supplier = new Supplier( intval( $_REQUEST[ "idsupplier" ] ) );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	displayAddressEditor( $supplier->getFactories(), $factory->getId(), "createFactory", "deleteFactory", "duplicateFactory" );
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* chiffres clés manageo - ajax */

if( isset( $_REQUEST[ "last_period" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	include_once( dirname( __FILE__ ) . "/../../lib/manageo.php" );
	
	header( "Content-Type: text/xml;charset=utf-8" );
	$document = manageo_stdclass2DomDocument( manageo_SIRET2stdclass( $_REQUEST[ "siret" ] ) );

	echo $document->saveXML();
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload logo */

if( isset( $_FILES[ "logo" ] ) ){
		if( !User::getInstance()->getId() )
		exit( "session expirée" );
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . $_REQUEST[ "folder" ] . "/";
	$ext = strtolower( substr( $_FILES[ "logo" ][ "name" ] , strrpos( $_FILES[ "logo" ][ "name" ] , "." ) + 1 ) );
	$targetFile =  str_replace( "//", "/", $targetPath ) . $_REQUEST[ "idsupplier" ] . ".$ext";

	$files = glob( "$targetPath/" . $_REQUEST[ "idsupplier" ] . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	
	foreach( $files as $file )
		unlink( $file );
		
	move_uploaded_file( $_FILES[ "logo" ][ "tmp_name" ], $targetFile );
	chmod( $targetFile, 0755 );
	
	exit( URLFactory::getSupplierImageURI( $_REQUEST[ "idsupplier" ], URLFactory::$IMAGE_CUSTOM_SIZE, 150, 150 ) );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* historique commandes et factures - ajax */

if( isset( $_GET[ "history" ] ) && isset( $_GET[ "idsupplier" ] ) ){

	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	$matches = array();
	if( preg_match( "/([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})/", $_GET[ "start_date" ], $matches ) )
			$start_date = $matches[ 3 ] . "-" . $matches[ 2 ] . "-" . $matches[ 1 ];
	else 	$start_date 	= date( "01-m-Y" );
	
	$matches = array();
	if( preg_match( "/([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})/", $_GET[ "end_date" ], $matches ) )
			$end_date = $matches[ 3 ] . "-" . $matches[ 2 ] . "-" . $matches[ 1 ];
	else 	$end_date 	= date( "Y-m-d" );
	
	getSupplierHistory( intval( $_GET[ "idsupplier" ] ), $start_date, $end_date );

	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des produits - ajax */

if( isset( $_GET[ "products" ] ) && isset( $_GET[ "idsupplier" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../../config/init.php" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	getSupplierProducts( intval( $_GET[ "idsupplier" ] ) );

	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );

header( "Content-Type: text/html; charset=utf-8" );

?>
<div id="SupplierEdit">

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/supplier_edit.php</span>
<?php } ?>


	<link rel="stylesheet" type="text/css" href="<?php echo  $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ---------------------------------------------------------------------------------------------------------------- */
	
		$(function() {
			
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
	
	    	$('#SupplierForm').ajaxForm( {
			 	
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback      
	  
		    } );
	    	
		});
	
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
		}
	
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
	
			if( responseText != "1" ){
	
				alert( "Impossible de sauvegarder les informations : " + responseText );
				return;
				
			}

			/* reset de certains champs */
			
			$( "#image" ).val( "" );

			/* confirmation */
			
			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
			$.blockUI.defaults.css.fontSize = '12px';
	    	$.growlUI( "", "Modifications enregistrées" );

	    	refreshUploadedImages();
	    	
		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function listPictograms(){

			$( "#PictogramListContainer" ).html( '<p style="text-align:center;"><img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...</p>' );
	
			$.ajax({
			 	
				url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&list=pictograms",
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les fournisseurs" ); },
			 	success: function( responseText ){
	
					$( "#PictogramListContainer" ).html( responseText );

				}
	
			});

		}
		
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function usersDialog(){
	
			$( "#UsersDialog" ).dialog({
	        
				modal: false,	
				title: "Liste des contacts",
				width: 500,
				close: function(event, ui) { 
				
					$("#UsersDialog").dialog( "destroy" );
					
				}
			
			});
	
			var users = $( "#users" ).val().split( "," );
			
			var i = 0;
			while( i < users.length ){
			
				$( "#UsersDialog" ).find( "input[value=" + users[ i ] + "]" ).attr( "checked", true );
				
				i++;
				
			}
			
		}
	
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function updateUserList(){
						
			var users = "";
			$( "#UsersDialog" ).find( "input:checked" ).each( function( i ){
			
				if( users.length )
					users += ",";
					
				users += $( this ).val();
				
			});
			
			$( "#users" ).val( users );
			
		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function getHistory(){

			var start_date = $( "#PurchaseStartDate" ).size() ? $( "#PurchaseStartDate" ).val() : "<?php echo date( "01-m-Y" ); ?>";
			var end_date = $( "#PurchaseEndDate" ).size() ? $( "#PurchaseEndDate" ).val() : "<?php echo date( "d-m-Y" ); ?>";
			
			$.ajax({
			 	
				url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&history",
				async: true,
				type: "GET",
				data: "start_date=" + start_date + "&end_date=" + end_date,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer l'historique du fournisseur" ); },
			 	success: function( responseText ){
	
					$( "#SupplierHistory" ).html( responseText );
				
				}
	
			});

		}
		
		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function getStatistics(){

			var tmp = $( "#StatStartDate" ).val().split(/-/);

			if( tmp.length != 3 ){

				alert( "Le format de la date de début est incorrect" );
				return;
				
			}

			var startDate = tmp[ 2 ] + "-" + tmp[ 1 ] + "-" + tmp[ 0 ];
			
			var tmp = $( "#StatEndDate" ).val().split(/-/);

			if( tmp.length != 3 ){

				alert( "Le format de la date de début est incorrect" );
				return;
				
			}
			
			var endDate = tmp[ 2 ] + "-" + tmp[ 1 ] + "-" + tmp[ 0 ];

			$( "#SupplierStats" ).html( '<p style="text-align:center;"><img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...</p>' );
			
			var data = "StatStartDate=" + startDate + "&StatEndDate=" + endDate;
			
			$.ajax({
			 	
				url: "/statistics/supplier.php?suppliers=<?php echo $supplier->getId(); ?>",
				async: true,
				type: "GET",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les statistiques" ); },
			 	success: function( responseText ){
	
					$( "#SupplierStats" ).html( responseText );
				
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function getProducts(){
            $.ajax({
			 	
				url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&products",
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les produits du fournisseur" ); },
			 	success: function( responseText ){
	
					$( "#SupplierProducts" ).html( responseText );
				
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function manageoDialog(){

			if( !$( "#name" ).val().length )
				return;

			var parameters = "cancelCallback=manageoCloseDialog&selectCallback=manageoSelectCompany&name=" + escape( $( "#name" ).val() );
			
			if( $( "#zipcode" ).val().length )
				parameters += "&zipcode=" + escape( $( "#zipcode" ).val() );
			
			$.ajax({
			 	
				url: "/sales_force/manageo.php?" + parameters,
				async: true,
				type: "GET",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'effectuer la recher" ); },
			 	success: function( responseText ){
					
					$( "#ManageoDialog").html( responseText );
					
					$( "#ManageoDialog" ).dialog({
				        
						modal: true,	
						title: "Recherche Manageo",
						width:850,
						close: function(event, ui) { 

							$("#ManageoDialog").dialog( "destroy" );
							
						}
					
					});
					
					
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------------------------- */

		function manageoCloseDialog(){ $("#ManageoDialog").dialog( "destroy" ); }

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function manageoSelectCompany( $company ){ 

			$( "#name" ).val( $company.find( "name" ).text() );
			$( "#legal_form" ).val( $company.find( "legal_form" ).text() );
			$( "#siret" ).val( $company.find( "siret" ).text() );
			$( "#naf" ).val( $company.find( "naf" ).text() );
			$( "#share_capital" ).val( $company.find( "share_capital" ).text() );
			$( "#adress" ).val( $company.find( "address" ).text() );
			$( "#adress_2" ).val( $company.find( "address2" ).text() );
			$( "#zipcode" ).val( $company.find( "zipcode" ).text() );
			$( "#city" ).val( $company.find( "city" ).text() );
			$( "#idstate" ).val( $company.find( "idstate" ).text() );
			$( "#faxnumber" ).val( $company.find( "faxnumber" ).text() );
			$( "#phonenumber" ).val( $company.find( "phonenumber" ).text() );
			$( "#manager_lastname" ).val( $company.find( "manager_lastname" ).text() );
			$( "#manager_firstname" ).val( $company.find( "manager_firstname" ).text() );
			$( "#manager_place_of_birth" ).val( $company.find( "manager_place_of_birth" ).text() );
			$( "#institution_count" ).val( $company.find( "institution_count" ).text() );
			
			if( $company.find( "manager_birthday" ).text() != "0000-00-00" ){
				
				var birthday = $company.find( "manager_birthday" ).text().split( "-" );
				$( "#manager_birthday" ).val( birthday[ 2 ] + "-" + birthday[ 1 ] + "-" + birthday[ 0 ] );

			}

			if( $company.find( "creation_date" ).text() != "0000-00-00" ){
				
				var creation_date = $company.find( "creation_date" ).text().split( "-" );
				$( "#creation_date" ).val( creation_date[ 2 ] + "-" + creation_date[ 1 ] + "-" + creation_date[ 0 ] );

			}
			
			$("#ManageoDialog").dialog( "destroy" ); 

		}

		/* ------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div id="ManageoDialog" style="display:none;"></div>
    <div id="UsersDialog" style="display:none;">
		<ul style="list-style-type:none; display:inline; float:left;">
		<?php
		
			$rs =& DBUtil::query( "SELECT login FROM `user` ORDER BY login ASC" );
			
			$i = 0;
			while( !$rs->EOF() ){
				
				if( $i && !( $i % 10 ) ){
					
					?>
					</ul>
					<ul style="list-style-type:none; display:inline; float:left;">
					<?php
					
				}
				
				?>
				<li>
					<input type="checkbox" value="<?php echo htmlentities( $rs->fields( "login" ) ); ?>" onclick="updateUserList();" /> 
					<a href="#" onclick="$( this ).prev().attr( 'checked', !$( this ).prev().attr( 'checked' ) ); updateUserList(); return false;">
						<?php echo htmlentities( $rs->fields( "login" ) ); ?>
					</a>
				</li>
				<?php
				
				$rs->MoveNext();
				$i++;
				
			}
			
		?>
		</ul>
		<p style="text-align:right; clear:both;">
			<input class="blueButton" type="button" value="Fermer" onclick="$('#UsersDialog').dialog( 'destroy' );" />
		</p>
	</div>

	<form action="/product_management/enterprise/supplier.php?update&amp;idsupplier=<?php echo $supplier->getId(); ?>" method="post" id="SupplierForm" enctype="multipart/form-data">
		<input type="hidden" name="update" value="1" />
		<input type="hidden" name="code_supplier" value="<?php echo strlen( $supplier->get( "code_supplier" ) ) ? $supplier->get( "code_supplier" ) : "F" . $supplier->getId(); /* @todo : DROP */ ?>" />
		<div class="contentResult">
			<!-- Titre -->
			<h1 class="titleSearch">
				<span class="textTitle">Société n°<?php echo  $supplier->getId(); ?> - <?php echo  $supplier->get("name"); ?></span>
				<div class="spacer"></div>
			</h1>
			<div class="blocResult" style="margin:0;">
				<div style="margin:5px;">	
					<link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
					<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
					<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
					<script type="text/javascript">
					/* <![CDATA[ */
					
						$( document ).ready( function(){ 
						
							$( "#logo" ).uploadify({
							
								'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
								'script'		: '/templates/product_management/supplier_edit.php',
								'folder'		: '/images/suppliers',
								'scriptData'	: { 'idsupplier': '<?php echo $supplier->getId(); ?>', 'sid_uploadify' : '<?php echo session_id();?>'},
								'fileDataName'	: 'logo',
								'buttonImg'		: '<?php echo URLFactory::getSupplierImageURI( $supplier->getId(), URLFactory::$IMAGE_CUSTOM_SIZE, 150, 150 ); ?>',
								'width' 		: 150,
								'height'		: 150,
								'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
								'fileDesc'		: "Images",
								'auto'           : true,
								'multi'          : false,
								'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
								'onComplete' 	: function( event, queueID, fileObj, response, data ){

										$( "#logo" ).uploadifySettings( "buttonImg", response );
										
									}

							});
						
						});
						
					/* ]]> */
					</script>
					<div class="blocMultiple blocMLeft" style="width:160px;">
						<input type="file" id="logo" name="logo" value="" />
						<div class="spacer"></div>
					</div>
				
					<div class="blocMultiple blocMLeft" style="width:33%;">

						<label><span><strong>Raison sociale</strong></span></label>
						<a href="#" onclick="manageoDialog(); return false;">
							<img id="ManageoDialogButton" src="/images/back_office/content/manageo.gif" alt="" style="vertical-align:bottom; float:right; margin-top:5px; opacity:<?php echo strlen( $supplier->get( "name" ) ) ? "1.0" : "0.6"; ?>" />
						</a>
						<input type="text" name="name" id="name" value="<?php echo  $supplier->get("name"); ?>" onkeyup="this.value=this.value.toUpperCase();" class="textInput" style="font-weight:bold; width:160px;" />
						<div class="spacer"></div>
						
						<label><span style="font-weight:normal; font-style:italic;">Suivie par</span></label>
						<input id="users" class="textInput inputSupplier" type="text" readonly="readonly" value="<?php echo  $supplier->get("contacts");  ?>" name="users" onclick="usersDialog(); return false;" />
						<div class="spacer"></div>	

						<div style="margin-top:15px;">
							<label><span>Adresse</span></label>
							<input type="text" name="adress" id="adress" value="<?php echo  $supplier->get("adress"); ?>" class="textInput inputSupplier" />
							<div class="spacer"></div>
							
							<label><span>Complément adresse</span></label>
							<input type="text" name="adress_2" id="adress_2" value="<?php echo  $supplier->get("adress_2"); ?>" class="textInput inputSupplier" />
							<div class="spacer"></div>
							
							<label><span>Code postal</span></label>
							<input type="text" name="zipcode" id="zipcode" value="<?php echo  $supplier->get("zipcode"); ?>" class="numberInput" maxlenght="5" />
							<div class="spacer"></div>
							
							<label><span>Ville</span></label>
							<input type="text" name="city" id="city" value="<?php echo  $supplier->get("city"); ?>" onkeyup="javascript:this.value=this.value.toUpperCase();" class="textInput inputSupplier" />
							<div class="spacer"></div>
							
							<label><span>Pays</span></label>
							<div class="selectSupplier">
								<?php  XHTMLFactory::createSelectElement( "state", "idstate", "name_1", "name_1", $supplier->get("idstate") ? $supplier->get("idstate") : 1, false, false, "name=\"idstate\" id=\"idstate\"" ); ?>
							</div>
							<div class="spacer"></div>
		
						</div>
						<div class="spacer"></div>

					</div>
					<div class="blocMultiple blocMRight">
						<label><span>Site Internet</span></label>
						<input style="width:64%;"  type="text" id="website" name="website" value="<?php echo $supplier->get( "website" ); ?>" class="textInput" />
						<a href="#" onclick="window.open(document.getElementById('website').value); return false;">
						<img class="floatleft" style="margin-top:5px;" src="/images/back_office/layout/icons-web.png"  /></a>
						<div class="spacer"></div>
						
						<label><span>Tél. standard</span></label>
						<input type="text" name="switchboard" id="switchboard" value="<?php echo  $supplier->get( "switchboard" );  ?>" class="textidInput" />
						
						<label class="smallLabel"><span>Fax</span></label>
						<input type="text" name="faxnumber" id="faxnumber" value="<?php echo  $supplier->get("faxnumber");  ?>" class="textidInput" />
						<div class="spacer"></div>
						
						<div style="margin-top:15px;">
							
							
							<label style="height:116px;"><span><strong>Commentaire<br/>&nbsp;</strong></span></label>
							<textarea name="comment" id="comment" class="textInput" style="height:111px; "><?php echo strip_tags(html_entity_decode($supplier->get( "comment" ))); ?></textarea>
							<div class="spacer"></div>
							
							<div class="spacer"></div>
							<label><span>Docs envoyés par</span></label>
							<div class="simpleSelect">
								<select name="send_to">
									<option<?php if($supplier->get("send_to") == "Mail") { ?> selected="selected"<?php } ?> value="Mail">Mail</option>
									<option<?php if($supplier->get("send_to") == "Fax") { ?> selected="selected"<?php } ?> value="Fax">Fax</option>
								</select>
							</div>

							<label class="smallLabel"><span>Langue</span></label>
							<div class="simpleSelect">
								<select name="language">
								<?php
								
									include_once( dirname( __FILE__ ) . "/../../objects/Dictionnary.php" );
									
									foreach( Dictionnary::getInstance()->getLanguages() as $id => $label ){
									
										$selected = $supplier->get( "language" ) == $id ? " selected=\"selected\"" : "";
										
										?>
										<option<?php echo $selected; ?> value="<?php echo $id; ?>"><?php echo htmlentities( $label ); ?></option>
										<?php
										
									}
							
								?>
								</select>
								<div class="spacer"></div>
							</div>
							<div class="spacer"></div>
							
							<label><span>Relances de confirmation de Cde</span></label>
							<span class="textSimple">
								<input<?php if( $supplier->get( "remind_confirmation" ) ) echo " checked=\"checked\""; ?> type="radio" name="remind_confirmation" value="1" /> automatiques
								<input<?php if( !$supplier->get( "remind_confirmation" ) ) echo " checked=\"checked\""; ?> type="radio" name="remind_confirmation" value="0" /> manuelles
							</span>
							<div class="spacer"></div>
							
							<label><span>Reminder Expédition</span></label>
							<span class="textSimple">
								<input<?php if( $supplier->get( "remind_dispatch" ) ) echo " checked=\"checked\""; ?> type="radio" name="remind_dispatch" value="1" /> automatique
								<input<?php if( !$supplier->get( "remind_dispatch" ) ) echo " checked=\"checked\""; ?> type="radio" name="remind_dispatch" value="0" /> manuel
							</span>
							<div class="spacer"></div>
							
						</div>
								
														
					</div>
					
					<!-- ajout info_... -->
					
					<div class="blocMultiple blocMRight">
						<h2>Informations complémentaires</h2>
					
						<div class="spacer"></div>
						
						
						<label><span><?php echo Dictionnary::translate('minbuyingamount') ; ?></span></label>
						<input type="text" name="minbuyingamount" id="minbuyingamount" value="<?php echo  $supplier->get( "minbuyingamount" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
											
						<label><span><?php echo Dictionnary::translate('franco_supplier') ; ?></span></label>
						<input type="text" name="franco_supplier" id="franco_supplier" value="<?php echo  $supplier->get( "franco_supplier" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
												
						<label><span>Info 1</span></label>
						<input type="text" name="info_1" id="info_1" value="<?php echo  $supplier->get( "info_1" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
												
						<label><span>Info 2</span></label>
						<input type="text" name="info_2" id="info_2" value="<?php echo  $supplier->get( "info_2" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
					
						<label><span>Info 3</span></label>
						<input type="text" name="info_3" id="info_3" value="<?php echo  $supplier->get( "info_3" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
						
						<!--
						<label><span<?php echo Dictionnary::translate('supplier_info_6') ; ?>></span></label>
						<input type="text" name="info_6" id="info_6" value="<?php echo  $supplier->get( "info_6" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
						
						<label><span><?php echo Dictionnary::translate('supplier_info_7') ; ?></span></label>
						<input type="text" name="info_7" id="info_7" value="<?php echo  $supplier->get( "info_7" );  ?>" class="textidInput" />
						<div class="spacer"></div>
						
						<label><span><?php echo Dictionnary::translate('supplier_info_8') ; ?></span></label>
						<input type="text" name="info_8" id="info_8" value="<?php echo  $supplier->get( "info_8" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
						
						<label><span><?php echo Dictionnary::translate('supplier_info_9') ; ?></span></label>
						<input type="text" name="info_9" id="info_9" value="<?php echo  $supplier->get( "info_9" );  ?>" class="textidInput" />
						<div class="spacer"></div>	
						
						<label><span><?php echo Dictionnary::translate('supplier_info_10') ; ?></span></label>
						<input type="text" name="info_10" id="info_10" value="<?php echo  $supplier->get( "info_10" );  ?>" class="textidInput" />
						-->
						
					</div>
					<!-- fin ajout info_... -->
					
					<div class="spacer"></div>		

				
					<div id="container">
						<!-- Onglet -->
						<ul class="menu">
							<li class="item"><a href="#contacts-supplier"><span>Contacts</span></a></li>
							<li class="item"><a href="#infos-supplier"><span>Infos société</span></a></li>
							<li class="item"><a href="#condition-supplier"><span>Facturation</span></a></li>
							<li class="item"><a href="#transport-supplier" style="display:none"><span>Transport</span></a></li>
							<li class="item"><a href="#tarification-supplier" style="display:none"><span>Tarification</span></a></li>
							
							<li class="item"><a href="#statistics-supplier" onclick="getStatistics(); return false;"><span>Statistiques</span></a></li>
							<li class="item"><a href="#products-supplier" onclick="getProducts(); return false;"><span>Liste des produits</span></a></li>
							<li class="item" style="display:none"><a href="#pictograms-supplier" onclick="listPictograms(); return false;"><span>Certifications</span></a></li>
							<li class="item" style="display:none"><a href="#pieces-supplier"><span>Pièces jointes</span></a></li>
							
							<li class="item"><a href="#history-supplier" onclick="getHistory(); return false;"><span>Historique</span></a></li>
							
						</ul>
						<div style="width:101%; border-top:1px solid #D0D0D0; margin:3px -5px 0 -5px;">&nbsp;</div>
							
		
						<!-- *********** -->
						<!-- Contacts 	 -->
						<!-- *********** -->
						<div id="contacts-supplier">
							<script type="text/javascript">
							/* <![CDATA[ */

							    /* -------------------------------------------------------------------------- */
								
								function createContact(){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&create=contact",
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le contact" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});
									
								}

								/* -------------------------------------------------------------------------- */
								
								function deleteContact( idcontact ){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&delete=contact&idcontact=" + idcontact,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le contact" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});

								}

								/* -------------------------------------------------------------------------- */
								
								function duplicateContact( idcontact ){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&duplicate=contact&idcontact=" + idcontact,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de dupliquer le contact" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});

								}

								/* -------------------------------------------------------------------------- */
								
								function setMainContact( idcontact ){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&main_contact=" + idcontact,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de définir le contact principal" ); },
									 	success: function( responseText ){
							
											$( "#ContactEditorContainer" ).html( responseText );
										
										}
							
									});

								}

								/* -------------------------------------------------------------------------- */
								
							/* ]]> */
							</script>
							<div id="ContactEditorContainer"> 
							<?php
							
								include( dirname( __FILE__ ) . "/../../lib/contact_editor.php" );
								displayContactEditor( $supplier->getContacts(), 0, "setMainContact", "createContact", "deleteContact", "duplicateContact" );
							
							?>
							</div><!-- blocMLeft -->
							<div class="spacer"></div>
							
						</div> <!-- #contacts-supplier -->	
						
						<!-- *********** -->
						<!--  Condition  -->
						<!-- *********** -->
						<div id="infos-supplier">	
						
							<div class="blocMultiple blocMLeft">
		
								<h2>Informations juridiques</h2>
								<label><span><strong><em>Code NAF</em></strong></span></label>
								<input type="text" name="naf" id="naf" value="<?php echo  $supplier->get("naf"); ?>" class="textInput" />
								<div class="spacer"></div>
								
								<label><span><strong><em>Activité APE/NAF</em></strong></span></label>
								<input type="text" value="<?php 
								
									$label = DBUtil::getDBValue( "naf_comment", "naf", "idnaf", $supplier->get( "naf" ) );
									
									if( $label ) 
										echo htmlentities( $label ); 
									
								?>" class="textInput" readonly="readonly" />
								<div class="spacer"></div>
								
								
								<div style="margin-top:15px;">
									<label><span><strong><em>Capital social</em></strong></span></label>
									<input type="text" name="share_capital" id="share_capital" value="<?php echo $supplier->get( "share_capital" ); ?>" class="textidInput" />
										
									<label class="smallLabel"><span><strong><em>Création</em></strong></span></label>
									<input type="text" name="creation_date" id="creation_date" value="<?php echo Util::dateFormat( $supplier->get( "creation_date" ), "d-m-Y" ); ?>" class="calendarInput" />
									<?php DHTMLCalendar::calendar( "creation_date" ); ?>
									<div class="spacer"></div>
									
									<label><span><strong><em>Forme juridique</em></strong></span></label>
									<input type="text" name="legal_form" id="legal_form" value="<?php echo htmlentities( $supplier->get( "legal_form" ) ); ?>" class="textInput" />
									<div class="spacer"></div>
		
									<label><span><strong><em>Siren / Siret</em></strong></span></label>
									<input type="text" name="siret" id="siret" value="<?php echo htmlentities( $supplier->get( "siret" ) ); ?>" class="textidInput" />
									<div class="spacer"></div>
									
									<label><span><strong><em>Nombre d'établissement</em></strong></span></label>
									<input type="text" name="institution_count" id="institution_count" value="<?php echo $supplier->get( "institution_count" ); ?>" class="textidInput" />
									<div class="spacer"></div>
									
								</div>
								<div class="spacer"></div>
								
								<h2>Dirigeant de la société</h2>
								
								<label><span><strong><em>Nom</em></strong></span></label>
								<input type="text" name="manager_lastname" id="manager_lastname" value="<?php echo htmlentities( $supplier->get( "manager_lastname" ) ); ?>" class="textInput" />
								
								<label><span><strong><em>Prénom</em></strong></span></label>
								<input type="text" name="manager_firstname" id="manager_firstname" value="<?php echo htmlentities( $supplier->get( "managerf_irstname" ) ); ?>" class="textInput" />
								
								<div class="spacer"></div>
			
								<label><span><strong><em>Date de naissance</em></strong></span></label>
								<input type="text" name="manager_birthday" id="manager_birthday" value="<?php echo Util::dateFormat( $supplier->get( "manager_birthday" ), "d-m-Y" ); ?>" class="calendarInput" />
								<?php DHTMLCalendar::calendar( "manager_birthday" ); ?>
								
								<label class="smallLabel"><span><strong><em>Lieu</em></strong></span></label>
								<input type="text" name="manager_place_of_birth" id="manager_place_of_birth" value="<?php echo htmlentities( $supplier->get( "manager_place_of_birth" ) ); ?>" class="textidInput" />							
								<div class="spacer"></div>
								
								<div style="margin-top:15px;">
									<label><span><strong><em>Tél.</em></strong></span></label>
									<input type="text" name="manager_phonenumber" id="manager_phonenumber" value="<?php echo Util::phonenumberFormat( $supplier->get( "manager_phonenumber" ) ); ?>" class="textidInput" />
									
									<label class="smallLabel"><span><strong><em>Portable</em></strong></span></label>
									<input type="text" name="manager_faxnumber" id="manager_faxnumber" value="<?php echo Util::phonenumberFormat( $supplier->get( "manager_faxnumber" ) ); ?>" class="textidInput" />							
									<div class="spacer"></div>
									
									<label><span><strong><em>Email</em></strong></span></label>
									<input type="text" name="manager_email" id="manager_email" value="<?php echo htmlentities( $supplier->get( "manager_email" ) ); ?>" class="textInput" />
									<div class="spacer"></div>
									
								</div>
		
							</div><!-- blocMLeft -->
							
							<script type="text/javascript">
							/* <![CDATA[ */
							
								function getLastPeriodInfos(){

									if( !$( "#siret" ).val().length ){

										alert( "Vous devez d'abord renseigner un N° SIRET/SIREN" );
										return;
										
									}

									$.blockUI({
										
										message: "Récupération des données...",
										css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
										fadeIn: 0, 
										fadeOut: 700
											
									}); 

									$( "#LastPeriod" ).find( "input[type='text']" ).val( "" );
									$('.blockOverlay').attr('title','Click to unblock').click( $.unblockUI );
									
									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?last_period",
										async: true,
										type: "POST",
										data: "siret=" + $( "#siret" ).val(),
										error : function( XMLHttpRequest, textStatus, errorThrown ){ $.unblockUI(); alert( "Impossible de récupérer les chiffres clés" ); },
									 	success: function( responseXML ){

											var elements = new Array( "fiscal_year","duration","closing_date","turnover","net_income","staff","added_value","gross_operating_profit","sale_efficiency","financial_efficiency" );
											var $xml = $( responseXML );

											var i = 0;
											while( i < elements.length ){
												
												$( "#" + elements[ i ] ).val( $xml.find( elements[ i ] ).text() );
												i++;

											}
											
											$.unblockUI();
											$( "#LastPeriod" ).fadeIn( 700 );
											
										}
							
									});
									
								}
								
							/* ]]> */
							</script>
							<div class="blocMultiple blocMRight">
								<h2>Chiffres clés</h2>
								<p style="text-align:right;">
									<a href="#" class="blueLink" onclick="getLastPeriodInfos(); return false;">Récupérer les chiffres du dernier exercice</a>
								</p>
								<div id="LastPeriod" style="display:none;">
								
									<label><span><em>Dernier exercice publié</em></span></label>
									<input id="fiscal_year" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Durée de l'exercice</em></span></label>
									<input id="duration" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Clôture de l'exercice</em></span></label>
									<input id="closing_date" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Chiffre d'affaires</em></span></label>
									<input id="turnover" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Résultat Net</em></span></label>
									<input id="net_income" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Valeur ajoutée</em></span></label>
									<input id="added_value" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Excédent brut d'exploitation</em></span></label>
									<input id="gross_operating_profit" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Rentabilité commerciale</em></span></label>
									<input id="sale_efficiency" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
									<label><span><em>Rentabilité financière</em></span></label>
									<input id="financial_efficiency" value="" class="calendarInput" type="text">
									<span style="margin: 9px 0pt 0pt 2px; float: left; width: 10px;">&nbsp;</span>
									<div class="spacer"></div>
									
								</div>
								
							</div><!-- blocMRight -->
		
						</div> <!-- #infos-supplier -->	
										
						<!-- *********** -->
						<!--  Condition  -->
						<!-- *********** -->
						<div id="condition-supplier">	
						
							<div class="blocMultiple blocMLeft">
								<h2>Conditions de règlement</h2>
								<label><span>Mode de règlement</span></label>
								<?php 
									$selectedValueMode = $supplier->get("idpaiement");
									XHTMLFactory::createSelectElementByEncodage( "payment", "idpayment", "name_1", "name_1", $selectedValueMode, false, false, "name=\"idpaiement\" id=\"idpaiement\"" ); 
								?>
								<div class="spacer"></div>
								
								<label><span>Délais de règlement</span></label>
								<?php 
									$selectedValueDelais = $supplier->get("pay_delay");
									XHTMLFactory::createSelectElementByEncodage( "payment_delay", "idpayment_delay", "payment_delay_1", "payment_delay_1", $selectedValueDelais, false, false, "name=\"pay_delay\" id=\"pay_delay\"" ); 
								?>
								<div class="spacer"></div>
								
								<label><span>Escompte</span></label>
								<input type="text" id="billing_rate" name="billing_rate" value="<?php echo Util::rateFormat( $supplier->get("billing_rate") );  ?>" class="textidInput" />
								<div class="spacer"></div>
								
								<label><span>N° compte SAGE</span></label>
								<input type="text" id="erp_name" name="erp_name" value="<?php echo  $supplier->get("erp_name");  ?>" class="textidInput" />
								<div class="spacer"></div>
								
								<label><span><em style="color: rgb(170, 170, 170);">Banque</em></span></label>
								<input name="" value="" class="textInput" type="text">
								<div class="spacer"></div>
								
								
								<h2>Comptable</h2>
								
								<label><span><em style="color: rgb(170, 170, 170);">Titre</em></span></label>
								<div class="simpleSelect">
								<?php 
									$selectedValueTitle = 0;
									XHTMLFactory::createSelectElement( "title", "idtitle", "title_1", "title_1", $selectedValueTitle, false, false, "name=\"???\" id=\"???\"" ); 
								?>
								</div>
								<div class="spacer"></div>
								
								<label><span><em style="color: rgb(170, 170, 170);">Nom</em></span></label>
								<input name="" value="" class="textidInput" type="text">
								
								<label class="smallLabel"><span><em style="color: rgb(170, 170, 170);">Prénom</em></span></label>
								<input name="" value="" class="textidInput" type="text">
								<div class="spacer"></div>
									
								<label><span><strong><em style="color: rgb(170, 170, 170);">Tél.</em></strong></span></label>
								<input type="text" name="" id="" value="" class="textidInput" />
								
								<label class="smallLabel"><span><strong><em style="color: rgb(170, 170, 170);">Fax</em></strong></span></label>
								<input type="text" name="" id="" value="" class="textidInput" />							
								<div class="spacer"></div>
								
							</div><!-- blocMLeft -->
							
							<div class="blocMultiple blocMRight">
								<h2>RIB</h2>
																
								<label><span>Code établissement</span></label>
								<input name="bic_bank_code" maxlength="5" value="<?php echo $supplier->get( "bic_bank_code" ) ?>" class="textInput" type="text">
								<div class="spacer"></div>
								
								<label><span>Code guichet</span></label>
								<input name="bic_branch_code" maxlength="5" value="<?php echo $supplier->get( "bic_branch_code" ) ?>" class="textInput" type="text">
								<div class="spacer"></div>
								
								<label><span>Numéro de compte</span></label>
								<input name="bic_account_number" maxlength="11" value="<?php echo $supplier->get( "bic_account_number" ) ?>" class="textInput" type="text">
								<div class="spacer"></div>
								
								<label><span>Clé RIB</span></label>
								<input name="bic_check_digits" maxlength="2" value="<?php echo $supplier->get( "bic_check_digits" ) ?>" class="textInput" type="text">
								<div class="spacer"></div>
								
								<label><span>Domiciliation</span></label>
								<input name="bic_banking_domiciliation" maxlength="40" value="<?php echo $supplier->get( "bic_banking_domiciliation" ) ?>" class="textInput" type="text">
								<div class="spacer"></div>
								
								<label><span>IBAN</span></label>
								<input name="IBAN" value="<?php echo $supplier->get( "IBAN" ) ?>" class="textInput" type="text">
								<div class="spacer"></div>									
								
							</div><!-- blocMRight -->
							<div class="spacer"></div>
						
						</div> <!-- #condition-supplier -->	
						
						
						<!-- *********** -->
						<!--  Transport  -->
						<!-- *********** -->
						<div id="transport-supplier">	
							<div class="blocMultiple blocMLeft">
								<h2>Transport</h2>
								
								<label><span>Mode de frais de port </span></label>
								<?php 
									$selectedValuePort = $supplier->get("select_charge");
									XHTMLFactory::createSelectElement( "charge_select", "idcharge_select", "charge_select_1", "charge_select_1", $selectedValuePort, false, false, "name=\"select_charge\" id=\"select_charge\"" ); 
								?>
								<div class="spacer"></div>
								
								<label><span>Franco</span></label>
								<input type="text" id="franco_supplier" name="franco_supplier" value="<?php echo  $supplier->get("franco_supplier");  ?>" class="textidInputMin" />
								
								<label class="smallLabelMin"><span>Pourcentage / poids</span></label>
								<input type="text" id="charge_rate" name="charge_rate" value="<?php echo  $supplier->get("charge_rate");  ?>" class="textidInputMin" />
								<div class="spacer"></div>
		
							</div>
							
							<div class="spacer"></div>
							<script type="text/javascript">
							/* <![CDATA[ */
							
							/* -------------------------------------------------------------------------- */
								
								function createFactory(){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&create=factory",
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer l'adresse usine" ); },
									 	success: function( responseText ){
							
											$( "#FactoryEditorContainer" ).html( responseText );
										
										}
							
									});
									
								}

								/* -------------------------------------------------------------------------- */
								
								function deleteFactory( idsupplier_factory ){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&delete=factory&idsupplier_factory=" + idsupplier_factory,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer l'adresse usine" ); },
									 	success: function( responseText ){
							
											$( "#FactoryEditorContainer" ).html( responseText );
										
										}
							
									});

								}

								/* -------------------------------------------------------------------------- */
								
								function duplicateFactory( idsupplier_factory ){

									$.ajax({
									 	
										url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $supplier->getId(); ?>&duplicate=factory&idsupplier_factory=" + idsupplier_factory,
										async: true,
										type: "GET",
										data: "",
										error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de dupliquer l'adresse usine" ); },
									 	success: function( responseText ){
							
											$( "#FactoryEditorContainer" ).html( responseText );
										
										}
							
									});

								}
								
								/* -------------------------------------------------------------------------- */
								
							/* ]]> */
							</script>
							<div id="FactoryEditorContainer"> 
							<?php
							
								include_once( dirname( __FILE__ ) . "/../../lib/address_editor.php" );
								displayAddressEditor( $supplier->getFactories(), 0, "createFactory", "deleteFactory", "duplicateFactory" );
							
							?>
							</div>
						</div> <!-- #transport-supplier -->	
						<div class="spacer"></div>	
						
						<!-- **********	-->
						<!--  Stats  	-->
						<!-- **********	-->
						<div id="statistics-supplier">	
						
							<div class="blocMultiple blocMLeft">
								<div style="margin-top:15px;">
									<label class="smallLabel"><span>Période du</span></label>
									<input type="text" id="StatStartDate" value="<?php $lastYear = date( "Y" ) - 1; echo date( "d-m-$lastYear" ); ?>" class="calendarInput" />
									
									<label class="smallLabel"><span>Au</span></label>
									<input type="text" id="StatEndDate" value="<?php echo date( "d-m-Y" ); ?>" class="calendarInput" />
									
									<?php 
									
										include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );
										DHTMLCalendar::calendar( "StatStartDate" ); 
										DHTMLCalendar::calendar( "StatEndDate" ); 
										
									?>
									<input type="button" value="Valider" onclick="getStatistics();" class="blueButton" style="margin:4px;" />
								</div>
							</div>
							<div class="spacer"></div>
							
							<div class="blocMultiple blocMLeft" style="width:100%;">
								<div id="SupplierStats">
									<p style="text-align:center;">
										<img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...
									</p>
								</div>
								<div class="spacer"></div>
							</div>
							<div class="spacer"></div>
						</div><!-- #statistics-supplier -->	
						
						<!-- **********	-->
						<!-- Historique  -->
						<!-- **********	-->
						<div id="history-supplier">	
							<div id="SupplierHistory">
								<p style="text-align:center;">
									<img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...
								</p>
							</div>
						</div><!-- history-supplier -->
						
						<!-- **********	-->
						<!-- Produits   -->
						<!-- **********	-->
						<div id="products-supplier">	
							<div id="SupplierProducts">
								<p style="text-align:center;">
									<img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...
								</p>
							</div>
						</div><!-- products-supplier -->
						
						
						<!-- ***************** -->
						<!--  Certifications   -->
						<!-- ***************** -->
						<div id="PictogramDialog" style="display:none;"></div>
						<script type="text/javascript">
						/* <![CDATA[ */
	
							/* ------------------------------------------------------------------------------------------------- */
							
							function sortSupplierPictograms(){
					
								$.ajax({
					
									url: "/templates/product_management/supplier_edit.php?sort=pictograms",
									async: true,
									cache: false,
									type: "GET",
									data: "idsupplier=<?php echo $supplier->getId(); ?>&" + $( "#SupplierPictogramList" ).sortable( "serialize" ),
									error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier l'ordre des pictogrammes" ); },
								 	success: function( responseText ){
					
										if( responseText != "1" ){
											
											alert( "Une erreur est survenue : " + responseText );
											return;
											
										}
					
										$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
										$.blockUI.defaults.css.fontSize = '12px';
						        		$.growlUI( '', 'Modifications enregistrées' );
						       
									}
					
								});
								
							}
					
							/* ------------------------------------------------------------------------------------------------- */
					
							function deletePictogram( idpictogram ){
					
								$.ajax({
					
									url: "/templates/product_management/supplier_edit.php?delete=pictogram",
									async: true,
									cache: false,
									type: "GET",
									data: "idsupplier=<?php echo $supplier->getId(); ?>&idpictogram=" + idpictogram,
									error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le pictogramme" ); },
								 	success: function( responseText ){
					
										if( responseText != "1" ){
											
											alert( "Une erreur est survenue : " + responseText );
											return;
											
										}
										
										$( "#SupplierPictogramIndexes_" + idpictogram ).remove();
					
										$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
										$.blockUI.defaults.css.fontSize = '12px';
						        		$.growlUI( '', 'Modifications enregistrées' );
						       
									}
					
								});
								
							}
					
							/* ----------------------------------------------------------------------------------------- */
							
							function pictogramDialog(){
					
								$.ajax({
								 	
									url: "/product_management/catalog/pictograms.php?callback=addPictogram",
									async: true,
									type: "GET",
									error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les pictogrammes" ); },
								 	success: function( responseText ){
						
										$( "#PictogramDialog" ).html( responseText );
										$( "#PictogramDialog" ).dialog({
									        
											modal: true,	
											title: "Sélectionner des pictogrammes",
											close: function(event, ui) { 
											
												$("#PictogramDialog").dialog( "destroy" );
												
											},
											width: 800,
											height: 600
											
										});
										
									}
						
								});
					
							}
					
							/* ----------------------------------------------------------------------------------------- */
					
							function addPictogram( idpictogram ){
			
								//$( "#PictogramDialog" ).dialog( "destroy" );
								
								$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
								$.blockUI.defaults.css.fontSize = '12px';
						    	$.growlUI( '', 'Sélection validée' );
	    	
								$.ajax({
								 	
									url: "/templates/product_management/supplier_edit.php?add=pictogram",
									async: true,
									type: "GET",
									data: "idsupplier=<?php echo $supplier->getId(); ?>&idpictogram=" + idpictogram,
									error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter le pictogramme" ); },
								 	success: function( responseText ){
						
										if( responseText != "1" ){
					
											alert( "Impossible d'ajouter le pictogramme : " + responseText );
											return;
											
										}
					
										$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
										$.blockUI.defaults.css.fontSize = '12px';
						        		$.growlUI( '', 'Modifications enregistrées' );
						        		
										listPictograms();
										
									}
						
								});
					
							}
					
							/* ----------------------------------------------------------------------------------------- */
						
						/* ]]> */
						</script>
						<div id="pictograms-supplier">	
							
							<div class="blocMultiple blocMLeft">
								<h2>Certifications disponibles</h2>
								<div id="PictogramListContainer">
									<img src="/images/back_office/content/small_black_spinner.gif" alt="" /> Chargement en cours...
								</div>
								<div class="spacer"></div>
							</div>
			
						</div> <!-- #transport-supplier -->	
						<div class="spacer"></div>
						
						
						
						<!-- **********	-->
						<!-- pieces jointes   -->
						<!-- **********	-->
						<div id="pieces-supplier">	
							<?php 
								/*define('CONFIG_SYS_DEFAULT_PATH', '../../images/'); //accept relative path only
								define('CONFIG_SYS_ROOT_PATH', '../../images/');	//accept relative path only
								include("../../js/ajaxfilemanager/ajaxfilemanager.php");*/ 
							?>
						</div><!-- pieces-supplier -->
						
						
						
						<!-- **********	-->
						<!-- tarification   -->
						<!-- **********	-->
						<div id="tarification-supplier">	
							<center><img src="/images/back_office/layout/postit.jpg" alt="" />	</center>
						</div><!-- tarification-supplier -->
						
						<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
							if( $akilae_crm == 0 ){
								?> <input type="submit" name="Modify" class="inputSave" value="Sauvegarder" />
								<?php  } ?>
						
						<?php
						
							if( $supplier->allowDelete() ){
								
								?>
								<input type="button" name="" class="inputSave" value="Supprimer" onclick="if( confirm('Etes-vous certains de vouloir supprimer ce fournisseur?') ) document.location = '<?php URLFactory::getHostURL(); ?>/product_management/enterprise/supplier.php?delete&idsupplier=<?php echo $supplier->getId(); ?>';" style="margin-right:10px;" />
								<?php
								
							}
							
						?>
						<div class="spacer"></div>	
		
			
					</div> <!--  #container -->
			<div class="spacer"></div>
				
	   </div><!-- contentResult -->
	   <div class="spacer"></div>
	
	</form>

</div>
<?php

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getSupplierHistory( $idsupplier, $start_date, $end_date ){
	
	?>
	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    padding-left: 20px; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
		    padding-bottom:15px;
		
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 
	
	</style>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<?php
	
		include_once( dirname( __FILE__ ) . "/../../objects/DHTMLCalendar.php" );
		
		DHTMLCalendar::calendar( "PurchaseStartDate"/*, true, "getHistory"*/ );
		DHTMLCalendar::calendar( "PurchaseEndDate"/*, true, "getHistory"*/ );
		
	?>
	<div class="blocMultiple blocMLeft">
	
		<label class="smallLabel"><span>Période du</span></label>
		<input type="text" id="PurchaseStartDate" value="<?php echo isset( $_REQUEST[ "start_date" ] ) ? $_REQUEST[ "start_date" ] : date( "01-m-Y" ); ?>" class="calendarInput" />
		<label class="smallLabel"><span>Au</span></label>
		<input type="text" id="PurchaseEndDate" value="<?php echo isset( $_REQUEST[ "end_date" ] ) ? $_REQUEST[ "end_date" ] : date( "d-m-Y" ); ?>" class="calendarInput" />
		<input type="button" value="ok" class="blueButton" onclick="getHistory();" style="margin-top:4px; margin-left:10px;" />
	</div>
	<br style="clear:both;" />
	<div class="blocMultiple blocMLeft">
		<h2>Commandes Fournisseur</h2>
		<?php listSupplierOrders( $idsupplier, $start_date, $end_date ); ?>
	</div>
	<div class="blocMultiple blocMRight">
		<h2>Factures Fournisseur</h2>
		<?php listSupplierInvoices( $idsupplier, $start_date, $end_date ); ?>
		<div class="spacer"></div>
		<h2>Proforma en cours</h2>
		<?php listSupplierProformas( $idsupplier, $start_date, $end_date ); ?>
		<h2>Avoirs</h2>
		<?php listSupplierCredits( $idsupplier, $start_date, $end_date ); ?>
	</div>
	<?php
					
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listSupplierCredits( $idsupplier, $start_date, $end_date ){
	
	$query = "SELECT sc.* , u.initial 
			  FROM credit_supplier sc 
			  LEFT JOIN user u ON u.iduser = sc.iduser
			  WHERE sc.idsupplier = '$idsupplier'
			  AND sc.Date >= '$start_date 00:00:00' AND sc.Date <= '$end_date 23:59:59'";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun avoir trouvé</p>
		<?php
		
		return;
		
	}

	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ 

			$( "#SupplierCredits" ).tablesorter({
		      	
	      		headers: { 
	      			0: {  sorter:'text' }, 
	      			1: {  sorter:'text' },
	      			2: {  sorter:'shortDate' },
	      			3: {  sorter:'text' },
	      			4: {  sorter:'currency' }
            	} 
        	
	      	}); 

		});

	/* ]]> */
	</script>
	
	<div class="tableContainer clear">
		<table id="SupplierCredits" class="dataTable resultTable sortable">
			<thead>
				<tr>
					<th>Com</th>
					<th>Avoir n°</th>
					<th>Date de création</th>
					<th>Facture</th>
					<th>Montant</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$total_amount_Sum = 0.0;
				
				while( !$rs->EOF() ){
					?>
					<tr>
						<td class="lefterCol"><?php echo strtoupper( $rs->fields( "initial" ) ); ?></td>
						<td><?php echo $rs->fields( "idcredit_supplier" ); ?></td>
						<td><?php echo Util::dateFormat( $rs->fields( "Date", "d/m/Y" ) ); ?></td>
						<td><?php echo $rs->fields( "billing_supplier" ); ?></td>
						<td class="righterCol"  style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "amount" ) ); ?></td>
					</tr>
					<?php
					
					$total_amount_Sum += $rs->fields( "amount" );
					
					$rs->MoveNext();
					
				}
				
			?>
			</tbody>
			<tr>
				<td colspan="4" class="lefterCol"></td>
				<td class="righterCol" style="text-align:right;"><b><?php echo Util::priceFormat( $total_amount_Sum ); ?></b></td>
			</tr>
		</table>
	</div><?php
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listSupplierOrders( $idsupplier, $start_date, $end_date ){
	
	include_once( dirname( __FILE__ ) . "/../../objects/Dictionnary.php" );
	
	$query = "
	SELECT os.idorder_supplier,
		os.status,
		os.total_amount_ht,
		SUBSTR( os.DateHeure, 1, 10 ) AS creation_date,
		u.initial,
		GROUP_CONCAT( osr.ref_supplier ) AS ref_supplier,
		GROUP_CONCAT( osr.designation ) AS designation,
		GROUP_CONCAT( osr.discount_price ) AS discount_price,
		GROUP_CONCAT( osr.quantity * osr.discount_price ) AS total
	FROM user u, order_supplier os
	LEFT JOIN order_supplier_row osr ON osr.idorder_supplier = os.idorder_supplier
	WHERE os.iduser = u.iduser
	AND os.idsupplier = '$idsupplier'
	AND os.DateHeure >= '$start_date 00:00:00' AND os.DateHeure <= '$end_date 23:59:59'
	GROUP BY os.idorder_supplier
	ORDER BY os.DateHeure ASC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune commande trouvée</p>
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ 

			$( "#SupplierOrderTable" ).tablesorter({
		      	
	      		headers: { 
	      			0: {  sorter:'text' }, 
	      			1: {  sorter:'currency' },
	      			2: {  sorter:'text' },
	      			3: {  sorter:'currency' },
	      			4: {  sorter:'shortDate' },
	      			5: { sorter: false }

            	} 
        	
	      	}); 

		});

	/* ]]> */
	</script>
    <div class="tableContainer clear">
		<table id="SupplierOrderTable" class="dataTable resultTable sortable">
			<thead>
				<tr>
					<th>Com</th>
					<th>Commande n°</th>
					<th>Statut</th>
					<th>Total HT</th>
					<th>Date de création</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$total_amount_htSum = 0.0;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td class="lefterCol"><?php echo strtoupper( $rs->fields( "initial" ) ); ?></td>
						<td>
							<a href="/sales_force/supplier_order.php?IdOrder=<?php echo $rs->fields( "idorder_supplier" ); ?>" onclick="window.open( this.href ); return false;">
								<?php echo $rs->fields( "idorder_supplier" ); ?>
							</a>
						</td>
						<td><?php echo Dictionnary::translate( $rs->fields( "status" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ); ?></td>
						<td><?php echo Util::dateFormat( $rs->fields( "creation_date" ), "d/m/Y" ); ?></td>
						<td class="righterCol">
							<a href="/catalog/pdf_supplier_order.php?idorder=<?php echo $rs->fields( "idorder_supplier" ); ?>" onclick="window.open( this.href ); return false;">
								<img src="/images/back_office/content/pdf_icon.gif" alt="Visualiser le PDF" />
							</a>
						</td>
					</tr>
					<?php
					
					$total_amount_htSum += $rs->fields( "total_amount_ht" );
					
					$rs->MoveNext();
					
				}
				
			?>
			</tbody>
			<tr>
				<td colspan="3" class="lefterCol"></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $total_amount_htSum ); ?></b></td>
				<td colspan="2" class="righterCol"></td>
			</tr>
		</table>
	</div>
	<?php

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listSupplierInvoices( $idsupplier, $start_date, $end_date ){
	
	include_once( dirname( __FILE__ ) . "/../../objects/Dictionnary.php" );
	
	$query = "
	SELECT bs.billing_supplier,
		bs.status,
		bs.total_amount_ht,
		bs.Date,
		u.initial,
		GROUP_CONCAT( orow.ref_supplier ) AS ref_supplier,
		GROUP_CONCAT( orow.designation ) AS designation,
		GROUP_CONCAT( orow.discount_price ) AS discount_price,
		GROUP_CONCAT( orow.quantity * orow.discount_price ) AS total
	FROM user u, billing_supplier bs
	LEFT JOIN billing_supplier_row bsr ON ( bsr.billing_supplier = bs.billing_supplier AND bsr.idsupplier = bs.idsupplier )
	LEFT JOIN bl_delivery_row blr ON ( bsr.idbl_delivery = blr.idbl_delivery AND bsr.idrow = blr.idrow )
	LEFT JOIN bl_delivery bl ON bl.idbl_delivery = blr.idbl_delivery
	LEFT JOIN order_row orow ON ( blr.idorder_row = orow.idrow AND orow.idorder = bl.idorder )
	WHERE bs.iduser = u.iduser
	AND bs.proforma = 0
	AND bs.idsupplier = '$idsupplier'
	AND bs.Date >= '$start_date 00:00:00' AND bs.Date <= '$end_date 23:59:59'
	GROUP BY bs.billing_supplier, bs.idsupplier
	ORDER BY bs.Date ASC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune facture trouvée</p>
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ $( "#SupplierInvoiceTable" ).tablesorter(); });

	/* ]]> */
	</script>
	<div class="tableContainer clear">
		<table id="SupplierInvoiceTable" class="dataTable resultTable sortable">
			<thead>
				<tr>
					<th>Com</th>
					<th>Facture n°</th>
					<th>Statut</th>
					<th>Total HT</th>
					<th>Date de création</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$total_amount_htSum = 0.0;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td class="lefterCol"><?php echo strtoupper( $rs->fields( "initial" ) ); ?></td>
						<td><?php echo htmlentities( $rs->fields( "billing_supplier" ) ); ?></td>
						<td><?php echo Dictionnary::translate( $rs->fields( "status" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ); ?></td>
						<td><?php echo Util::dateFormat( $rs->fields( "Date" ), "d/m/Y" ); ?></td>
					</tr>
					<?php
					
					$total_amount_htSum += $rs->fields( "total_amount_ht" );
					
					$rs->MoveNext();
					
				}
				
			?>
			</tbody>
			<tr>
				<td colspan="3" class="lefterCol"></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $total_amount_htSum ); ?></b></td>
				<td class="righterCol"></td>
			</tr>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listSupplierProformas( $idsupplier, $start_date, $end_date ){
	
	include_once( dirname( __FILE__ ) . "/../../objects/Dictionnary.php" );
	
	$query = "
	SELECT bs.billing_supplier,
		bs.status,
		bs.total_amount_ht,
		bs.Date,
		u.initial,
		GROUP_CONCAT( orow.ref_supplier ) AS ref_supplier,
		GROUP_CONCAT( orow.designation ) AS designation,
		GROUP_CONCAT( orow.discount_price ) AS discount_price,
		GROUP_CONCAT( orow.quantity * orow.discount_price ) AS total
	FROM user u , billing_supplier bs
	LEFT JOIN billing_supplier_row bsr ON ( bsr.billing_supplier = bs.billing_supplier AND bsr.idsupplier = bs.idsupplier )
	LEFT JOIN bl_delivery_row blr ON ( bsr.idbl_delivery = blr.idbl_delivery AND bsr.idrow = blr.idrow )
	LEFT JOIN bl_delivery bl ON bl.idbl_delivery = blr.idbl_delivery
	LEFT JOIN order_row orow ON ( blr.idorder_row = orow.idrow AND orow.idorder = bl.idorder )
	LEFT JOIN billing_proforma bp ON bp.proforma_id = bs.billing_supplier 
	WHERE bs.iduser = u.iduser
	AND bp.billing_supplier IS NULL
	AND bs.proforma = 1
	AND bs.idsupplier = '$idsupplier'
	AND bs.Date >= '$start_date 00:00:00' AND bs.Date <= '$end_date 23:59:59'
	GROUP BY bs.billing_supplier, bs.idsupplier
	ORDER BY bs.Date ASC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune proforma trouvée</p>
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ $( "#SupplierInvoiceTable" ).tablesorter(); });

	/* ]]> */
	</script>
	<div class="tableContainer clear">
		<table id="SupplierInvoiceTable" class="dataTable resultTable sortable">
			<thead>
				<tr>
					<th>Com</th>
					<th>Proforma n°</th>
					<th>Statut</th>
					<th>Total HT</th>
					<th>Date de création</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$total_amount_htSum = 0.0;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td class="lefterCol"><?php echo strtoupper( $rs->fields( "initial" ) ); ?></td>
						<td><?php echo htmlentities( $rs->fields( "billing_supplier" ) ); ?></td>
						<td><?php echo Dictionnary::translate( $rs->fields( "status" ) ); ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ); ?></td>
						<td><?php echo Util::dateFormat( $rs->fields( "Date" ), "d/m/Y" ); ?></td>
					</tr>
					<?php
					
					$total_amount_htSum += $rs->fields( "total_amount_ht" );
					
					$rs->MoveNext();
					
				}
				
			?>
			</tbody>
			<tr>
				<td colspan="3" class="lefterCol"></td>
				<td style="text-align:right;"><b><?php echo Util::priceFormat( $total_amount_htSum ); ?></b></td>
				<td class="righterCol"></td>
			</tr>
		</table>
	</div>
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getSupplierProducts( $idsupplier ){
	
	$query = "
	SELECT c.name_1 AS categoryName, 
		c.idcategory,
		GROUP_CONCAT( p.idproduct) AS idproduct,
		GROUP_CONCAT( p.name_1 SEPARATOR '\r\n' ) AS productName,
		COUNT( p.idproduct ) AS productCount
	FROM product p, category c
	WHERE p.idproduct <> 0
	AND p.idsupplier = '$idsupplier'
	AND p.idcategory = c.idcategory
	GROUP BY c.idcategory
	ORDER BY productCount DESC, categoryName ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun produit trouvé</p>
		<?php
			
		return;
		
	}
	
	$categoryCount = $rs->RecordCount();
	$productCount = 0;
	while( !$rs->EOF() ){
		
		$productCount += $rs->fields( "productCount" );
		$rs->MoveNext();
		
	}
	
	$rs->MoveFirst();
	
	?>
	<p style="text-align:right;"><?php echo $productCount; ?> produit(s) réparti(s) sur <?php echo $categoryCount; ?> catégorie(s)</p>
	<?php
	$root = getCategoryTree( $rs );
	displayNode( $root );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function getCategoryTree( &$rs ){
	
	$tree = createCategoryNode( 0 );

	while( !$rs->EOF() ){
		
		$category = createCategoryNode( $rs->fields( "idcategory" ) );
	
		$idproduct 		= explode( ",", $rs->fields( "idproduct" ) );
		$productName 	= explode( "\r\n", $rs->fields( "productName" ) );
		$i = 0;
		while( $i < count( $idproduct ) ){
		    $product = ( object )array(
			
				"id" 	=> $idproduct[ $i ],
				"name" 	=> $productName[ $i ],
				"image" => URLFactory::getProductImageURI( $idproduct[ $i ] ),
				"depth" => $category->depth + 1
			
			);
			
			array_push( $category->products, $product );
			
			$i++;
			
		}
		
		treePush( $tree, $category );
		
		$rs->MoveNext();
		
	}
	
	return $tree;
	
}


/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function treePush( stdclass &$tree, stdclass $category ){
	
	$p =& $tree;

	$treePath = getCategoryTreePath( $category->id );
	
	foreach( $treePath as $idcategory ){
	
		$done = false;
		$i = 0;
		while( !$done && $i < count( $p->categories ) ){
			
			if( $p->categories[ $i ]->id == $idcategory ){

				$p =& $p->categories[ $i ];
				$done = true;
				
			}
			
			$i++;
			
		}
		
		if( !$done ){
			
			$child = createCategoryNode( $idcategory );
			$child->depth = $p->depth + 1;
			
			array_push( $p->categories, $child );
		
			$p =& $p->categories[ count( $p->categories ) -1 ];
			
		}
		
	}
	
	$category->depth = $p->depth + 1;
	
	array_push( $p->categories, $category );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function createCategoryNode( $idcategory ){
	
	return ( object )array(
	
		"id" 			=> $idcategory,
		"name" 			=> DBUtil::getDBValue( "name_1", "category", "idcategory", $idcategory ),
		"products" 		=> array(),
		"categories" 	=> array(),
		"depth" 		=> 0
	
	);
	
}

/* -----------------------------array_reverse------------------------------------------------------------------------------------------------------------- */

function getCategoryTreePath( $idcategory ){
	
	$treePath = array();
	
	$done = false;
	while( !$done ){
		
		$rs =& DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategory' LIMIT 1" );	
	
		if( !$rs->RecordCount() )
			return array_reverse( $treePath );

		if( !$rs->fields( "idcategoryparent" ) )
			$done = true;
		else array_push( $treePath, $rs->fields( "idcategoryparent" ) );
		
		$idcategory = $rs->fields( "idcategoryparent" );
			
	}
	
	return array_reverse( $treePath );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function displayNode( stdclass &$node ){
	
	?>
	<ul id="Category<?php echo $node->id; ?>" style="margin-left:<?php echo $node->depth * 12; ?>px; display:<?php echo count( $node->products ) ? "none" : "block"; ?>">
	<?php

		/* sous-catégories */
	
		$i = 0;
		while( $i < count( $node->categories ) ){

			$category =& $node->categories[ $i ];
			
			?>
			<li>
				<a class="blueLink" href="#" onclick="$('#Category<?php echo $category->id; ?>').slideToggle(); return false;">
					<b>&#0155; <?php echo htmlentities( $category->name ); ?></b>
				</a>
				<?php 
				
					if( count( $category->products ) ) 
						echo " : " . count( $category->products ) . " produits"; 
					
					displayNode( $category );
						
			?>
			</li>
			<?php
	
			$i++;
			
		}

		/* produits */

		foreach( $node->products as $product ){
	       	?>
			<li>
				<a href="/product_management/catalog/cat_product.php?idproduct=<?php echo $product->id; ?>" onclick="window.open( this.href ); return false;">
					<!--<img src="/catalog/thumb.php?src=<?php echo urlencode( $product->image ); ?>&h=25&w=25" alt="" />-->
                    <img src="<?php echo URLFactory::getProductImageURI( $product->id, URLFactory::$IMAGE_CUSTOM_SIZE, 0, 50, 50 ); ?>" alt="" />
					<?php echo htmlentities( $product->name ); ?>
				</a>
			</li>
			<?php
		
		}
		
	?>
	</ul>
	<?php
		
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

function listPictograms( $idsupplier ){
	
	?>
	<p style="text-align:right;">
		<a class="blueLink" href="#" onclick="pictogramDialog(); return false;">
			Ajouter des pictogrammes
		</a>
	</p>
	<?php
	
	$rs = DBUtil::query( "SELECT p.* FROM supplier_pictogram sp, pictogram p WHERE sp.idsupplier = '$idsupplier' AND sp.idpictogram = p.idpictogram ORDER BY sp.`index` ASC" );
		
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun pictogramme n'a été sélectionné</p>
		<?php
		
		return;
		
	}
			
	?>
	<style type="text/css">
		
		.pictogramList img{ border-style:none; }
		
		.pictogramList{
			
			margin: 20px auto 20px auto;
			list-style-type: none;
			
		}
		
		.pictogramList li{
			
			margin-bottom: 0px;
			margin-top: 4px;
			width:<?php echo PICTOGRAM_SIZE; ?>px;
			float:left;
			border:1px solid #E7E7E7;
			margin:4px 4px 19px 4px;
			cursor:move;
			
		}
	
		.pictogramList li:hover{}
		
		.pictogramList li .label{
		
			height:15px;
			line-height:15px;
			padding:0px;
			margin:0px;
			font-size:10px;
			margin-bottom:-15px;
			bottom:0px;
			position:absolute;
			
		}
		
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){
			
			$( "#SupplierPictogramList" ).sortable({
				
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function( event, ui ){ sortSupplierPictograms(); }
			
			});
	
			$( "#SupplierPictogramList" ).disableSelection();
	
		});

	/* ]]> */
	</script>
	<ul id="SupplierPictogramList" class="pictogramList">
	<?php

	while( !$rs->EOF() ) {
		
		?>
		<li id="SupplierPictogramIndexes_<?php echo $rs->fields( "idpictogram" ); ?>" style="margin:4px 4px 19px 4px;">
			<div style="position:relative; height:<?php echo PICTOGRAM_SIZE; ?>px;">
				<img src="/catalog/thumb.php?src=<?php echo urlencode( "/images/pictograms/" . htmlentities( $rs->fields( "filename" ) ) ); ?>&amp;w=<?php echo PICTOGRAM_SIZE; ?>&amp;h=<?php echo PICTOGRAM_SIZE; ?>" alt="" />
				<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce pictogramme ?' ) ) deletePictogram( <?php echo $rs->fields( "idpictogram" ); ?> ); return false;" style="cursor:pointer;">
					<img src="/images/back_office/content/corbeille.jpg" title="supprimer le pictogramme" alt="supprimer le pictogramme" style="position:absolute; bottom:2px; left:2px;" />
				</a>
				<div class="label">hfghfghfghfgh
					<?php echo htmlentities( $rs->fields( "label" ) ); ?>
				</div>
			</div>
		
		</li>
		<?php

		$rs->MoveNext();
		
	}
	
	?>
	</ul>
	<?php
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */

?>
