<?php
// Récupére la session pour uploadify
if( isset( $_REQUEST['sid_uploadify'] ) )
{
    @session_id(  $_REQUEST['sid_uploadify']   );
    @session_start();
}

define( "THUMB_SIZE", 150 );
define( "PICTOGRAM_SIZE", 80 );


include_once( dirname( __FILE__ ) . "/../../config/init.php" );

/*
@todo upload par application flash 
=> l'appli utilise sa propre session
=> pas de cookie de session avec identification de l'utilisateur
if( !User::getInstance()->getId() )
	exit( "Session expirée" );
*/

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* tri des pictogrammes produit */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "product_pictograms" ){

	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = true;

	$index = 0;
	foreach( $_REQUEST[ "ProductPictogramIndexes" ] as $index => $idpictogram ){
	
		$ret &= DBUtil::query( "UPDATE product_pictogram SET `index` = '$index' WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND idpictogram = '" . intval( $idpictogram ) . "' LIMIT 1" ) !== false;
		$index++;
		
	}
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* tri des pictogrammes fournisseur */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "supplier_pictograms" ){

	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = true;

	$index = 0;
	foreach( $_REQUEST[ "SupplierPictogramIndexes" ] as $index => $idpictogram ){
	
		$ret &= DBUtil::query( "UPDATE supplier_pictogram SET `index` = '$index' WHERE idsupplier = '" . intval( $_REQUEST[ "idsupplier" ] ) . "' AND idpictogram = '" . intval( $idpictogram ) . "' LIMIT 1" ) !== false;
		$index++;
		
	}
	
	exit( $ret ? "1" : "0" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des pictogrammes produit */

if( isset( $_REQUEST[ "list" ] ) && $_REQUEST[ "list" ] == "product_pictograms" ){
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	listProductPictograms( intval( $_REQUEST[ "idproduct" ] ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des documents produit */

if( isset( $_REQUEST[ "list" ] ) && $_REQUEST[ "list" ] == "product_documents" ){
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	displayDocuments( intval( $_REQUEST[ "idproduct" ] ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* ajout d'un pictogramme produit */

if( isset( $_REQUEST[ "add" ] ) && $_REQUEST[ "add" ] == "pictogram" ){
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );

	$idsupplier = DBUtil::getDBValue( "idsupplier", "product", "idproduct", intval( $_REQUEST[ "idproduct" ] ) );
	
	if( DBUtil::query( "SELECT 1 FROM supplier_pictogram WHERE idsupplier = '$idsupplier' AND idpictogram = '" . intval( $_REQUEST[ "idpictogram" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( "Ce pictogramme est déjà sélectionné pour le fournisseur du produit" );

	if( DBUtil::query( "SELECT 1 FROM product_pictogram WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND idpictogram = '" . intval( $_REQUEST[ "idpictogram" ] ) . "' LIMIT 1" )->RecordCount() )
		exit( "Ce pictogramme est déjà sélectionné pour le produit" );
		
	$index = DBUtil::query( "SELECT COALESCE( MAX( `index` ) + 1, 0 ) AS `index` FROM product_pictogram WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "'" )->fields( "index" );
	
	$ret = DBUtil::query( 
	
		"INSERT IGNORE INTO product_pictogram VALUES( 
			'" . intval( $_REQUEST[ "idproduct" ] ) . "', 
			'" . intval( $_REQUEST[ "idpictogram" ] ) . "',
			'$index'
		)" 
	
	) !== false;
	
	exit( $ret === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression d'un pictogramme produit */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "product_pictogram" ){

	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$ret = DBUtil::query( "DELETE FROM product_pictogram WHERE idproduct = '" . intval( $_REQUEST[ "idproduct" ] ) . "' AND idpictogram = '" . intval( $_REQUEST[ "idpictogram" ] ) . "' LIMIT 1" );
	
	exit( $ret === false ? "0" : "1" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* tri des images produit ( inversion de l'ordre de 2 images ) */

if( isset( $_REQUEST[ "sort" ] ) && $_REQUEST[ "sort" ] == "images" ){
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$directory 	= dirname( __FILE__ ) . "/../../images/products";
	$idproduct 	= intval( $_REQUEST[ "idproduct" ] );
	$index1 	= intval( $_REQUEST[ "index1" ] );
	$index2 	= intval( $_REQUEST[ "index2" ] );
	
	$sources 		= glob( "$directory/$idproduct.$index1{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	$destinations 	= glob( "$directory/$idproduct.$index2{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	
	if( !$sources || !$destinations )
		exit( "L'image n'existe pas" );

	if( !copy( $sources[ 0 ], $destinations[ 0 ] . ".tmp" ) 
		|| !copy( $destinations[ 0 ], $sources[ 0 ] )
		|| !copy( $destinations[ 0 ] . ".tmp", $destinations[ 0 ] ) )
		exit( "Impossible de copier l'image" );
	
	/* mise à jour des références */
		
	$rs1 =& DBUtil::query( "SELECT idarticle FROM detail WHERE idproduct = '$idproduct' AND image_index = '$index1'" );
	$rs2 =& DBUtil::query( "SELECT idarticle FROM detail WHERE idproduct = '$idproduct' AND image_index = '$index2'" );
	
	$ret = true;
	while( !$rs1->EOF() ){
		
		$ret &= DBUtil::query( "UPDATE detail SET image_index = '$index2' WHERE idproduct = '$idproduct' AND idarticle = '" . $rs1->fields( "idarticle" ) . "'" ) !== false;
		$rs1->MoveNext();
		
	}
	
	while( !$rs2->EOF() ){
		
		$ret &= DBUtil::query( "UPDATE detail SET image_index = '$index1' WHERE idproduct = '$idproduct' AND idarticle = '" . $rs2->fields( "idarticle" ) . "'" ) !== false;
		$rs2->MoveNext();
		
	}
	
	exit( $ret ? "1" : "Impossible de mettre à jour les références" );
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des images complémentaires produit */

if( isset( $_REQUEST[ "list" ] ) && $_REQUEST[ "list" ] == "images" ){
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$index = 1;
	while( $image = URLFactory::getProductImageURI( intval( $_REQUEST[ "idproduct" ] ), URLFactory::$IMAGE_CUSTOM_SIZE, $index, THUMB_SIZE, THUMB_SIZE ) ){
	
		?>
		<li id="Image<?php echo $index; ?>" style="cursor:move;">
			<div style="position:relative; height:<?php echo THUMB_SIZE; ?>px;">
				<a id="ImageZoom<?php echo $index; ?>" rel="imageGroup" href="<?php echo URLFactory::getProductImageURI( intval( $_REQUEST[ "idproduct" ] ), URLFactory::$IMAGE_MAX_SIZE, $index ); ?>" style="cursor:pointer;">
					<img src="<?php echo $image; ?>?<?php echo md5_file( URLFactory::getProductImagePath( intval( $_REQUEST[ "idproduct" ] ), $index ) ); ?>" alt="" />
				</a>
				<a href="#" onclick="deleteImagetAt( <?php echo $index; ?> ); return false;" style="cursor:pointer;">
					<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'image" alt="supprimer l'image" style="position:absolute; bottom:2px; left:2px;" />
				</a>
			</div>
		</li>
		<?php
				
		$index++;
		
	}
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression image complémentaire produit */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "image" ){
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$directory 	= dirname( __FILE__ ) . "/../../images/products";
	$idproduct 	= intval( $_REQUEST[ "idproduct" ] );
	$index 		= intval( $_REQUEST[ "index" ] );

	$files = glob( "$directory/$idproduct.$index{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	if( !$files )
		exit( "L'image n'existe pas" );

	if( !unlink( $files[ 0 ] ) )
		exit( "Impossible de supprimer l'image " . $files[ 0 ] );

	DBUtil::query( "UPDATE detail SET image_index = 0 WHERE idproduct = '$idproduct' AND image_index = '$index'" );
	
	/* réindexation */

	$p = $index + 1;
	while( $files = glob( "$directory/$idproduct.$p{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ){
		
		$ext = strtolower( substr( $files[ 0 ] , strrpos( $files[ 0 ] , "." ) + 1 ) );
		copy( $files[ 0 ], "$directory/$idproduct.$index.$ext" );
		chmod( "$directory/$idproduct.$index.$ext", 0755 );
		unlink( $files[ 0 ] );
		
		$p++;
		$index++;
		
	}
		
	exit( "1" );
		
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload documents produits */

if( isset( $_FILES[ "productDocument" ] ) && isset( $_REQUEST[ 'idproduct' ] ) ){
	include_once( dirname( __FILE__ ) . '/../../objects/ProductDocument.php' );
			if( !User::getInstance()->getId() )
		exit( "session expirée" );
	$productFile = new ProductDocument( $_REQUEST[ 'idproduct' ] );
	$productFile->uploadDocument( $_FILES[ 'productDocument' ] );
	$productFile->setDocName( $_REQUEST[ 'document_name' ] );
	$productFile->setDisplayAvailable( false );
	$productFile->save();
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression document */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "product_document" ){
	include_once( dirname( __FILE__ ) . '/../../objects/ProductDocument.php' );
		
	ProductDocument::delete( $_REQUEST[ 'idproduct' ] , $_REQUEST[ 'iddocument' ] );
	
	exit( '1' );
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* Maj document */

if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "product_document" ){
	include_once( dirname( __FILE__ ) . '/../../objects/ProductDocument.php' );
		
	$productFile = new ProductDocument( $_REQUEST[ 'idproduct' ] , $_REQUEST[ 'iddocument' ] );
	$productFile->setDisplayAvailable( $_REQUEST[ 'viewable' ] );
	$productFile->update();
	
	exit( '1' );
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload images complémentaires produit */

if( isset( $_FILES[ "imageN" ] ) ){
		if( !User::getInstance()->getId() )
		exit( "session expirée" );
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . $_REQUEST[ "folder" ] . "/";
	$ext = strtolower( substr( $_FILES[ "imageN" ][ "name" ] , strrpos( $_FILES[ "imageN" ][ "name" ] , "." ) + 1 ) );
	
	$index = 1;
	while( $file = glob( $targetPath . $_REQUEST[ "idproduct" ] . ".$index{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) )
		$index++;	

	$basename = $_REQUEST[ "idproduct" ] . "." . $index;
	$targetFile =  str_replace( "//", "/", $targetPath ) . $basename . "." . $ext;

	$files = glob( $targetPath . $basename . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	foreach( $files as $file )
		unlink( $file );

	move_uploaded_file( $_FILES[ "imageN" ][ "tmp_name" ], $targetFile );
	chmod( $targetFile, 0755 );
	
	exit( URLFactory::getProductImageURI( $_REQUEST[ "idproduct" ], URLFactory::$IMAGE_CUSTOM_SIZE, $index, THUMB_SIZE, THUMB_SIZE ) );

}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* liste des plans techniques produit */

if( isset( $_REQUEST[ "list" ] ) && $_REQUEST[ "list" ] == "data_sheets" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$index = 0;
	while( $image = URLFactory::getProductDataSheetURI( intval( $_REQUEST[ "idproduct" ] ), URLFactory::$IMAGE_CUSTOM_SIZE, $index, THUMB_SIZE, THUMB_SIZE ) ){
	
		?>
		<li id="DataSheet<?php echo $index; ?>">
			<div style="position:relative; height:<?php echo THUMB_SIZE; ?>px;">
				<a id="DataSheetZoom<?php echo $index; ?>" rel="dataSheetGroup" href="<?php echo URLFactory::getProductDataSheetURI( intval( $_REQUEST[ "idproduct" ] ), URLFactory::$IMAGE_MAX_SIZE, $index ); ?>">
					<img src="<?php echo $image; ?>?<?php echo md5_file( URLFactory::getProductDataSheetPath( intval( $_REQUEST[ "idproduct" ] ), $index ) ); ?>" alt="" />
				</a>
				<a href="#" onclick="deleteDataSheetAt( <?php echo $index; ?> ); return false;" style="cursor:pointer;">
					<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'image" alt="supprimer l'image" style="position:absolute; bottom:2px; left:2px;" />
				</a>
			</div>
		</li>
		<?php
				
		$index++;
		
	}
	
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* suppression plan technique produit */

if( isset( $_REQUEST[ "delete" ] ) && $_REQUEST[ "delete" ] == "data_sheet" ){
	
	if( !User::getInstance()->getId() )
		exit( "session expirée" );
		
	$directory 	= dirname( __FILE__ ) . "/../../images/products";
	$idproduct 	= intval( $_REQUEST[ "idproduct" ] );
	$index 		= intval( $_REQUEST[ "index" ] );

	if( $index )
			$files = glob( "$directory/$idproduct.doc.$index{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	else 	$files = glob( "$directory/$idproduct.doc{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	
	if( !$files )
		exit( "L'image n'existe pas" );

	if( !unlink( $files[ 0 ] ) )
		exit( "Impossible de supprimer l'image " . $files[ 0 ] );

	/* réindexation */

	$p = $index + 1;
	while( $files = glob( "$directory/$idproduct.doc.$p{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ){
		
		$ext = strtolower( substr( $files[ 0 ] , strrpos( $files[ 0 ] , "." ) + 1 ) );
		
		if( $index ){
			
			copy( $files[ 0 ], "$directory/$idproduct.doc.$index.$ext" );
			chmod( "$directory/$idproduct.doc.$index.$ext", 0755 );
		
		}
		else{
			
			copy( $files[ 0 ], "$directory/$idproduct.doc.$ext" );
			chmod( "$directory/$idproduct.doc.$ext", 0755 );
			
		}
		
		unlink( $files[ 0 ] );
		
		$p++;
		$index++;
		
	}
		
	exit( "1" );
		
}

/* ------------------------------------------------------------------------------------------------------------------------------------------ */
/* upload plan technique */

if( isset( $_FILES[ "dataSheetN" ] ) ){
		if( !User::getInstance()->getId() )
		exit( "session expirée" );
	$targetPath = $_SERVER[ "DOCUMENT_ROOT" ] . $_REQUEST[ "folder" ] . "/";
	$ext = strtolower( substr( $_FILES[ "dataSheetN" ][ "name" ] , strrpos( $_FILES[ "dataSheetN" ][ "name" ] , "." ) + 1 ) );
	
	if( !( $file = glob( $targetPath . $_REQUEST[ "idproduct" ] . ".doc{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ) ){
		
		$index = 0;
		$basename = $_REQUEST[ "idproduct" ] . ".doc";
		
	}
	else{
		
		$index = 1;
		while( $file = glob( $targetPath . $_REQUEST[ "idproduct" ] . ".doc.$index{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) )
			$index++;	

		$basename = $_REQUEST[ "idproduct" ] . ".doc." . $index;
		
	}
	
	$targetFile =  str_replace( "//", "/", $targetPath ) . $basename . "." . $ext;

	$files = glob( $targetPath . $basename . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );

	foreach( $files as $file )
		unlink( $file );

	move_uploaded_file( $_FILES[ "dataSheetN" ][ "tmp_name" ], $targetFile );
	chmod( $targetFile, 0755 );
	
	exit( URLFactory::getProductDataSheetURI( $_REQUEST[ "idproduct" ], URLFactory::$IMAGE_CUSTOM_SIZE, $index, THUMB_SIZE, THUMB_SIZE ) );

}

//---------------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

if( !headers_sent() )
	header( "Content-Type: text/html; charset=utf-8" );


$notemplate = true;
include_once( "$GLOBAL_START_PATH/product_management/catalog/cat_product.php" );

?>
<!--link rel="stylesheet" type="text/css" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css" />
<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.min.js"></script>
<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script-->
<?php
		
//---------------------------------------------------------------------------------------------

//========================================
// Gestion des images complémentaires
//========================================

?>

<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/templates/product_management/product_image.htm.php</span>
<?php } ?>
	<div class="subTitleContainer" style="padding-top:10px;">
		<p class="subTitle">Photos complémentaires</p>
	</div>
    
	<div class="tableContainer">
		<?php displayImages( $tableobj->get( "idproduct" ) ); ?>
	</div>
	<?php 

//========================================
// Gestion des pictos
//========================================
?>
<style type="text/css">
		
	.pictogramList img{ border-style:none; }
	
	.pictogramList{
		
		margin: 20px auto 20px auto;
		list-style-type: none;
		
	}
	
	.pictogramList li{
		
		margin-bottom: 0px;
		margin-top: 4px;
		width:<?php echo PICTOGRAM_SIZE; ?>px;
		float:left;
		border:1px solid #E7E7E7;
		margin:4px 4px 19px 4px;
		cursor:move;
		
	}

	.pictogramList li:hover{}
	
	.pictogramList li .label{
	
		height:15px;
		line-height:15px;
		padding:0px;
		margin:0px;
		font-size:10px;
		margin-bottom:-15px;
		bottom:0px;
		position:absolute;
		
	}
	
</style>
<!--<div class="subTitleContainer" style="padding-top:10px;">
	<p class="subTitle">Pictogrammes fournisseur</p>
</div>
<br />
<div style="clear:both"></div>
<p style="text-align:right;">
	<a class="blueLink" href="/product_management/enterprise/supplier.php?idsupplier=<?php echo $tableobj->get( "idsupplier" ); ?>" onclick="window.open(this.href); return false;">
		Accéder à la fiche fournisseur
	</a>
</p>
<div id="SupplierPictogramListContainer"><?php listSupplierPictograms( $tableobj->get( "idsupplier" ) ); ?></div>-->
<div class="subTitleContainer" style="padding-top:10px;">
	<p class="subTitle">Pictogrammes produit</p>
</div>
<br />
<div style="clear:both"></div>
<script type="text/javascript">
/* <![CDATA[ */

	/* ----------------------------------------------------------------------------------------- */
	
	function pictogramDialog(){

		$.ajax({
		 	
			url: "/product_management/catalog/pictograms.php?callback=addPictogram",
			async: true,
			type: "GET",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de lister les pictogrammes" ); },
		 	success: function( responseText ){

				$( "#PictogramDialog" ).html( responseText );
				$( "#PictogramDialog" ).dialog({
			        
					modal: true,	
					title: "Sélectionner des pictogrammes",
					close: function(event, ui) { 
					
						$("#PictogramDialog").dialog( "destroy" );
						
					},
					width: 800,
					height: 600
					
				});
				
			}

		});

	}

	/* ----------------------------------------------------------------------------------------- */

	function addPictogram( idpictogram ){

		//$( "#PictogramDialog" ).dialog( "destroy" );
		
		$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
		$.blockUI.defaults.css.fontSize = '12px';
    	$.growlUI( '', 'Sélection validée' );
	    	
		$.ajax({
		 	
			url: "/templates/product_management/product_images.htm.php?add=pictogram",
			async: true,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&idpictogram=" + idpictogram,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible d'ajouter le pictogramme" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){

					alert( "Impossible d'ajouter le pictogramme : " + responseText );
					return;
					
				}

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
        		$.growlUI( '', 'Modifications enregistrées' );
        		
				listProductPictograms();
				
			}

		});

	}

	/* ----------------------------------------------------------------------------------------- */
	
	function listProductPictograms(){

		$.ajax({
		 	
			url: "/templates/product_management/product_images.htm.php?list=product_pictograms",
			async: true,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer la liste des pictogrammes" ); },
		 	success: function( responseText ){

				$( "#ProductPictogramListContainer" ).html( responseText );
				
			}

		});

	}

	/* ------------------------------------------------------------------------------------------------- */
	
	function sortProductPictograms(){

		$.ajax({

			url: "/templates/product_management/product_images.htm.php?sort=product_pictograms",
			async: true,
			cache: false,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&" + $( "#ProductPictogramList" ).sortable( "serialize" ),
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier l'ordre des pictogrammes" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Une erreur est survenue : " + responseText );
					return;
					
				}

				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
        		$.growlUI( '', 'Modifications enregistrées' );
       
			}

		});
		
	}

	/* ------------------------------------------------------------------------------------------------- */

	function deletePictogram( idpictogram ){

		$.ajax({

			url: "/templates/product_management/product_images.htm.php?delete=product_pictogram",
			async: true,
			cache: false,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&idpictogram=" + idpictogram,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le pictogramme" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Une erreur est survenue : " + responseText );
					return;
					
				}

				$( "#ProductPictogramIndexes_" + idpictogram ).remove();
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
        		$.growlUI( '', 'Modifications enregistrées' );
       
			}

		});
		
	}
	
	/* ----------------------------------------------------------------------------------------- */
	
	function documentDialog(){

		$( "#documentDialog" ).dialog({
			modal: true,	
			close: function(event, ui) { $("#documentDialog").dialog( "destroy" ); $('#productDocument').uploadifyClearQueue(); },
			width: 400,
			height: 200
		});

	}

	/* ----------------------------------------------------------------------------------------- */

	function listProductDocuments(){

		$.ajax({
			url: "/templates/product_management/product_images.htm.php?list=product_documents",
			async: true,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer la liste des documents" ); },
		 	success: function( responseText ){ $( "#documentListContainer" ).html( responseText ); }
		});

	}

	/* ------------------------------------------------------------------------------------------------- */	
	
	function deleteDocument( iddocument ){

		$.ajax({

			url: "/templates/product_management/product_images.htm.php?delete=product_document",
			async: true,
			cache: false,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&iddocument=" + iddocument,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le document" ); },
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Une erreur est survenue : " + responseText );
					return;
					
				}

				$( "#DocumentIndexes_" + iddocument ).remove();
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
        		$.growlUI( '', 'Modifications enregistrées' );
       
			}

		});
		
	}
	
	/* ------------------------------------------------------------------------------------------------- */	
	
	function displayDocument( viewable , iddocument ){

		visibility = 0;
		if( viewable )
			visibility = 1;
		
		$.ajax({
			url: "/templates/product_management/product_images.htm.php?update=product_document",
			async: true,
			cache: false,
			type: "GET",
			data: "idproduct=<?php echo $tableobj->get( "idproduct" ); ?>&iddocument=" + iddocument + "&viewable=" + visibility,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de mettre à jour le document" ); },
		 	success: function( responseText ){
	
				if( responseText != "1" ){
					
					alert( "Une erreur est survenue : " + responseText );
					return;
					
				}
	
				$( "#documentListContainer" ).html( listProductDocuments() );
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	    		$.growlUI( '', 'Modifications enregistrées' );
   
		}

	});		
		
	}

	/* ------------------------------------------------------------------------------------------------- */	
		
/* ]]> */
</script>
<div id="PictogramDialog" style="display:none;"></div>
<p style="text-align:right;">
	<a class="blueLink" href="#" onclick="pictogramDialog(); return false;">
		Ajouter des pictogrammes
	</a>
</p>
<div id="ProductPictogramListContainer"><?php listProductPictograms( $tableobj->get( "idproduct" ) ); ?></div>
<?php

//========================================
// Gestion croquis
//========================================

//if (DBUtil::getParameterAdmin('display_img_doc')) { 

	?>
	<!--<div class="subTitleContainer" style="padding-top:10px;">
		<p class="subTitle">Fichiers Croquis</p>
	</div>
	<div class="tableContainer">
		<?php //displayDataSheets( $tableobj->get( "idproduct" ) ); ?>
		
	</div>-->
	
	<div class="subTitleContainer" style="padding-top:10px;">
		<p class="subTitle">Documentations produit</p>
	</div>
	<div id="documentDialog" title="Ajouter un document" style="display:none;">
        <link type="text/css" rel="stylesheet" media="screen" href="/js/jquery/ui/uploadify-2.1.0/uploadify.css">
	    <script src="/js/jquery/ui/uploadify-2.1.0/jquery.uploadify.v2.1.0.js"></script>
		<script type="text/javascript">
		/* <![CDATA[ */
		
			$( document ).ready( function(){ 
				
				/*$( "#productDocument" ).uploadify({
					'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
					'script'		: '/templates/product_management/product_images.htm.php',
					'scriptData'	: { 'idproduct': '<?php echo $tableobj->get( "idproduct" ); ?>' , 'sid_uploadify' : '<?php echo session_id();?>' },
					'buttonText'	: 'Nouveau document',
					'fileDataName'	: 'productDocument',
					'fileDesc'		: "Documents",
					'auto'           : false,
					'multi'          : false,
					'onError' 		: function( event, queueID, fileObj, errorObj ){ alert( errorObj.info ); },
					'onComplete' 	: function( event, queueID, fileObj, response, data ){ listProductDocuments(); $("#documentDialog").dialog( "destroy" ); }
				});*/
			});
			
		/* ]]> */
		</script>
		<span style="float:left;">Nom du document :
            <input id="documentName" type="text" value="" onblur="" /></span>
		<input type="file"  name="productDocument" required value="" />
        <div id="productDocument"></div>
		<div style="clear:both"></div>
		<span style="float:right; margin-top:10px;"><input class="blueButton" type="button" name="" value="Sauvegarder" onclick="uploadFileInput('#documentDialog > input:nth-child(3)','/templates/product_management/product_images.htm.php',
		{ 
			'document_name': $('#documentName').val(),
			'idproduct': '<?php echo $tableobj->get( "idproduct" ); ?>' , 
			'sid_uploadify' : '<?php echo session_id();?>'
		},function(){
			listProductDocuments();
			$('#documentDialog').dialog( 'destroy' );
		}
		);" /></span>
			
	</div>
	<div class="tableContainer">
		<p style="text-align:right;">
			<a class="blueLink" href="#" onclick="documentDialog(); return false;">
				Ajouter un document
			</a>
		</p>
		<div id="documentListContainer">
		<?php displayDocuments( $tableobj->get( "idproduct" ) ); ?>
		</div>
	</div>

<?php 

//}
?>
<?php 
//-----------------------------------------------------------------------------------------------------

function displayImages( $idproduct ){

	?>
	<style type="text/css">
		
		#ImageList{
			
			margin: 20px auto 20px auto;
			list-style-type: none;
			padding: 0px;
			
		}
		
		#ImageList li{
			
			margin-bottom: 0px;
			margin-top: 4px;
			width:<?php echo THUMB_SIZE; ?>px;
			height:<?php echo THUMB_SIZE; ?>px;
			float:left;
			border:1px solid #E7E7E7;
			margin:4px;
			
		}
	
	</style>
	<div>
		<ul id="ImageList">
		<?php

			$index = 1;
			while(  $files = glob( dirname( __FILE__ ) . "/../../images/products/$idproduct.$index{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ){

				?>
				<li id="Image<?php echo $index; ?>" style="cursor:move;">
					<div style="position:relative; height:<?php echo THUMB_SIZE; ?>px;">
						<a id="ImageZoom<?php echo $index; ?>" rel="imageGroup"  href="<?php echo URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_MAX_SIZE, $index ); ?>" style="cursor:pointer;">
							<img src="<?php echo URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_CUSTOM_SIZE, $index, THUMB_SIZE, THUMB_SIZE ); ?>?<?php echo md5_file( URLFactory::getProductImagePath( $idproduct, $index ) ); ?>" alt="" />
						</a>
						<a href="#" onclick="deleteImagetAt( <?php echo $index; ?> ); return false;" style="cursor:pointer;">
							<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'image" alt="supprimer l'image" style="position:absolute; bottom:2px; left:2px;" />
						</a>
					</div>
				</li>
				<?php
				
				$index++;
				
			}
			
			echo "</ul>";
			
		?>
		</ul>
		<br style="clear:both;" />
		<script type="text/javascript">
		/* <![CDATA[ */
		
			$( document ).ready( function(){	
/*
				$( "#ImageN" ).uploadify({
				
					'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
					'script'		: '/templates/product_management/product_images.htm.php',
					'folder'		: '/images/products',
					'scriptData'	: { 'idproduct': '<?php echo $idproduct; ?>', 'sid_uploadify' : '<?php echo session_id();?>' },
					'buttonText'	: 'Nouvelle photo',
					'fileDataName'	: 'imageN',
					'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
					'fileDesc'		: "Images",
					'auto'           : true,
					'multi'          : false,
					'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
					'onComplete' 	: function( event, queueID, fileObj, response, data ){ listProductImages(); }
	
				});
*/
			$('#imageN').change(function(){
				
				uploadFileInput("#imageN",'/templates/product_management/product_images.htm.php',
				{ 
					'folder': '/images/products',
					'idproduct': '<?php echo $idproduct; ?>' , 
					'sid_uploadify' : '<?php echo session_id();?>',

				},function(){
					console.log("test");
					listProductImages();
					$('#documentDialog').dialog( 'destroy' );
				}
				);

			});
				$( "#ImageList" ).sortable({
					
					placeholder: 'placeholder',
					forcePlaceholderSize: true,
					update: function( event, ui ){ sortImages(); }
				
				});

				$( "#ImageList" ).disableSelection();
				
			});

			/* ---------------------------------------------------------------------------------------------------- */
			
			function sortImages(){

				var images = new Array();
				$( "#ImageList" ).find( "li" ).each( function( i ){

					if( $( this ).attr( "id" ).substr( 5 ) != i + 1 )
						images.push( $( this ).attr( "id" ).substr( 5 ) );
	
				});

				if( images.length != 2 ) //les 2 images à inverser
					return;
			
				$.ajax({

					url: "/templates/product_management/product_images.htm.php?sort=images",
					async: true,
					cache: false,
					type: "GET",
					data: "idproduct=<?php echo $idproduct; ?>&index1=" + images[ 0 ] + "&index2=" + images[ 1 ],
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier l'ordre des images" ); },
				 	success: function( responseText ){

						if( responseText != "1" ){
							
							alert( "Une erreur est survenue : " + responseText );
							return;
							
						}

						$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
		        		$.growlUI( '', 'Modifications enregistrées' );
		        		
						listProductImages();
						
					}

				});
				
			}
			
			/* ---------------------------------------------------------------------------------------------------- */
			
			function deleteImagetAt( index ){

				if( !confirm( "Etes-vous certains de vouloir supprimer cette photo?" ) )
					return;
				
				$.ajax({

					url: "/templates/product_management/product_images.htm.php?delete=image",
					async: true,
					cache: false,
					type: "GET",
					data: "idproduct=<?php echo $idproduct; ?>&index=" + index,
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible supprimer l'image sélectionnée" ); },
				 	success: function( responseText ){

						if( responseText != "1" ){
							
							alert( "Une erreur est survenue : " + responseText );
							return;
							
						}
						
						listProductImages();
						
					}

				});
				
			}

			/* ---------------------------------------------------------------------------------------------------- */
			
			function listProductImages(){

				$.ajax({

					url: "/templates/product_management/product_images.htm.php?list=images",
					async: true,
					cache: false,
					type: "GET",
					data: "idproduct=<?php echo $idproduct; ?>",
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les images" ); },
				 	success: function( responseText ){

						$( "#ImageList" ).html( responseText );
						$("a[id^='ImageZoom']").fancybox();
						
					}

				});
				
			}
			
			/* ---------------------------------------------------------------------------------------------------- */
			
		/* ]]> */
		</script>








        <label for="imageN" class="label-file orangeButton">Choisir une image</label>
        <input type="file" id="imageN" name="imageN" class="orangeButton" value="ertertertert" style="display:none; width:150px;"/>
        <br/>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function displayDocuments( $idproduct ){
	
	include_once( dirname( __FILE__ ) . '/../../objects/ProductDocument.php' );
	
	$documents = ProductDocument::getDocumentsProduct( $idproduct );
	
	if( !$documents ){
		echo "<p style='text-align:center;'>Aucun document n'a été uploadé pour ce produit</p>";
		return;
	}
	
	?>
	<table class="dataTable">
		<tr>
			<th class="filledCell" style="width:200px;">Document</th>
	    	<th class="filledCell" style="width:200px;">Date</th>
	    	<th class="filledCell" style="width:200px;">Visible</th>
	    	<th class="filledCell" style="width:200px;">Supprimer</th>
	    </tr>
	<?php 
	
	foreach( $documents as $document ){
		?>
		<tr id="DocumentIndexes_<?php echo $document->getDocumentId(); ?>">
			<td><a href="<?php echo $document->getDocumentURL(); ?>" target="_blank"><?php echo $document->getDocumentName(); ?></td>
			<td><?php echo $document->getUploadDate(); ?></td>
			<td>
			<?php if( $document->diplayDocument() ){ ?>
				<a href="#" onclick="displayDocument( false , <?php echo $document->getDocumentId(); ?> ); return false;">
					<img src="<?php echo HTTP_HOST ?>/images/back_office/content/eye.gif" alt="Afficher / Masquer" style="border-style:none; vertical-align:text-bottom;" />
				</a>
			<?php }else{ ?>
				<a href="#" onclick="displayDocument( true , <?php echo $document->getDocumentId(); ?> ); return false;">
					<img src="<?php echo HTTP_HOST ?>/images/back_office/content/hidden.gif" alt="Afficher / Masquer" style="border-style:none; vertical-align:text-bottom;" />
				</a>
			<?php } ?>
			</td>
			<td>
				<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce document ?' ) ) deleteDocument( <?php echo $document->getDocumentId(); ?> ); return false;" style="cursor:pointer;">
					<img src="<?php echo HTTP_HOST ?>/images/back_office/content/corbeille.jpg" title="supprimer le document" alt="supprimer le document" />
				</a>
			</td>
		</tr>
		<?php
	}
	?>
	</table><?php 
	
}

//-----------------------------------------------------------------------------------------------------

function displayDataSheets( $idproduct ){

	?>
	<style type="text/css">
		
		#DataSheetList{
			
			margin: 20px auto 20px auto;
			list-style-type: none;
			padding: 0px;
			
		}
		
		#DataSheetList li{
			
			margin-bottom: 0px;
			margin-top: 4px;
			width:<?php echo THUMB_SIZE; ?>px;
			height:<?php echo THUMB_SIZE; ?>px;
			float:left;
			border:1px solid #E7E7E7;
			margin:4px;
			
		}
	
	</style>
	<div>
		<ul id="DataSheetList">
		<?php

			$index = 0;
			$done = false;
			while( !$done ){

				$pattern = $index ? "doc.$index" : "doc";
				
				if( $files = glob( dirname( __FILE__ ) . "/../../images/products/$idproduct.$pattern{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ){
				
					?>
					<li id="DataSheet<?php echo $index; ?>">
						<div style="position:relative; height:<?php echo THUMB_SIZE; ?>px;">
							<a id="DataSheetZoom<?php echo $index; ?>" rel="dataSheetGroup" href="<?php echo URLFactory::getProductDataSheetURI( $idproduct, URLFactory::$IMAGE_MAX_SIZE, $index ); ?>">
								<img src="<?php echo URLFactory::getProductDataSheetURI( $idproduct, URLFactory::$IMAGE_CUSTOM_SIZE, $index, THUMB_SIZE, THUMB_SIZE ); ?>" alt="" />
							</a>
							<a href="#" onclick="deleteDataSheetAt( <?php echo $index; ?> ); return false;" style="cursor:pointer;">
								<img src="/images/back_office/content/corbeille.jpg" title="supprimer l'image" alt="supprimer l'image" style="position:absolute; bottom:2px; left:2px;" />
							</a>
						</div>
					</li>
					<?php

				}
				
				$done = count( $files ) == 0;
				$index++;
				
			}
			
			echo "</ul>";
			
		?>
		</ul>
		<br style="clear:both;" />
		<script type="text/javascript">
		/* <![CDATA[ */
		
			$( document ).ready( function(){ 
			/*
				$( "#DataSheetN" ).uploadify({
					
					'uploader'		: '/js/jquery/ui/uploadify-2.1.0/upload1.swf',
					'script'		: '/templates/product_management/product_images.htm.php',
					'folder'		: '/images/products',
					'scriptData'	: { 'idproduct': '<?php echo $idproduct; ?>' , 'sid_uploadify' : '<?php echo session_id();?>' },
					'buttonText'	: 'Nouveau croquis',
					'fileDataName'	: 'dataSheetN',
					'fileExt'		: "*.jpg;*.jpeg;*.gif;*.png",
					'fileDesc'		: "Images",
					'auto'           : true,
					'multi'          : false,
					'onError' 		: function( event, queueID, fileObj, errorObj ){ alert(errorObj.info ); },
					'onComplete' 	: function( event, queueID, fileObj, response, data ){ listProductDataSheets(); }
	
				});
*/
			$('#DataSheetN').change(function(){
				uploadFileInput("#DataSheetN",'/templates/product_management/product_images.htm.php',
				{ 
					'folder': '/images/products',
					'idproduct': '<?php echo $idproduct; ?>' , 
					'sid_uploadify' : '<?php echo session_id();?>',

				},function(){
					console.log("test");
					listProductImages();
					$('#documentDialog').dialog( 'destroy' );
				}
				);
			});

			});
			
			/* ---------------------------------------------------------------------------------------------------- */
			
			function deleteDataSheetAt( index ){

				if( !confirm( "Etes-vous certains de vouloir supprimer cette image?" ) )
					return;
				
				$.ajax({

					url: "/templates/product_management/product_images.htm.php?delete=data_sheet",
					async: true,
					cache: false,
					type: "GET",
					data: "idproduct=<?php echo $idproduct; ?>&index=" + index,
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible supprimer l'image sélectionnée" ); },
				 	success: function( responseText ){

						if( responseText != "1" ){
							
							alert( "Une erreur est survenue : " + responseText );
							return;
							
						}
						
						listProductDataSheets();
						
					}

				});
				
			}

			/* ---------------------------------------------------------------------------------------------------- */
			
			function listProductDataSheets(){

				$.ajax({

					url: "/templates/product_management/product_images.htm.php?list=data_sheets",
					async: true,
					cache: false,
					type: "GET",
					data: "idproduct=<?php echo $idproduct; ?>",
					error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les images" ); },
				 	success: function( responseText ){

						$( "#DataSheetList" ).html( responseText );
						$("a[id^='DataSheetZoom']").fancybox();
						
					}

				});
				
			}
			
			/* ---------------------------------------------------------------------------------------------------- */
			
		/* ]]> */
		</script>
		<input type="file" id="DataSheetN" value="" />
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listProductPictograms( $idproduct ){
	
	$rs = DBUtil::query( "SELECT p.* FROM product_pictogram pp, pictogram p WHERE pp.idproduct = '$idproduct' AND pp.idpictogram = p.idpictogram ORDER BY pp.`index` ASC" );
		
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun pictogramme n'a été sélectionné pour ce produit</p>
		<?php
		
		return;
		
	}
			
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------------------- */
		
		$( document ).ready( function(){

			$( "#ProductPictogramList" ).sortable({
				
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function( event, ui ){ sortProductPictograms(); }
			
			});

			$( "#ProductPictogramList" ).disableSelection();

		});

		/* ------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	
	<ul id="ProductPictogramList" class="pictogramList">
	<?php

	while( !$rs->EOF() ) {
		
		?>
		<li id="ProductPictogramIndexes_<?php echo $rs->fields( "idpictogram" ); ?>">
			<div style="position:relative; height:<?php echo PICTOGRAM_SIZE; ?>px;">
				<img src="/catalog/thumb.php?src=<?php echo urlencode( "/www/" . htmlentities( $rs->fields( "filename" ) ) ); ?>&amp;w=<?php echo PICTOGRAM_SIZE; ?>&amp;h=<?php echo PICTOGRAM_SIZE; ?>" alt="" />
				<a href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce pictogramme ?' ) ) deletePictogram( <?php echo $rs->fields( "idpictogram" ); ?> ); return false;" style="cursor:pointer;">
					<img src="/images/back_office/content/corbeille.jpg" title="supprimer le pictogramme" alt="supprimer le pictogramme" style="position:absolute; bottom:2px; left:2px;" />
				</a>
				<div class="label">
					<?php echo htmlentities( $rs->fields( "label" ) ); ?>
				</div>
			</div>
		
		</li>
		<?php

		$rs->MoveNext();
		
	}
	
	?>
	</ul>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

function listSupplierPictograms( $idsupplier ){
	
	$rs = DBUtil::query( "SELECT p.* FROM supplier_pictogram sp, pictogram p WHERE sp.idsupplier = '$idsupplier' AND sp.idpictogram = p.idpictogram ORDER BY sp.`index` ASC" );
		
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun pictogramme n'a été sélectionné pour ce fournisseur</p>
		<?php
		
		return;

	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ------------------------------------------------------------------------------------------------- */
		
		$( document ).ready( function(){

			$( "#SupplierPictogramList" ).sortable({
				
				placeholder: 'placeholder',
				forcePlaceholderSize: true,
				update: function( event, ui ){ sortSupplierPictograms(); }
			
			});

			$( "#SupplierPictogramList" ).disableSelection();

		});

		/* ------------------------------------------------------------------------------------------------- */
		
		function sortSupplierPictograms(){

			$.ajax({

				url: "/templates/product_management/product_images.htm.php?sort=supplier_pictograms",
				async: true,
				cache: false,
				type: "GET",
				data: "idsupplier=<?php echo $idsupplier; ?>&" + $( "#SupplierPictogramList" ).sortable( "serialize" ),
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier l'ordre des pictogrammes" ); },
			 	success: function( responseText ){

					if( responseText != "1" ){
						
						alert( "Une erreur est survenue : " + responseText );
						return;
						
					}

					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI( '', 'Modifications enregistrées' );
	       
				}

			});
			
		}

		/* ------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<ul id="SupplierPictogramList" class="pictogramList">
	<?php

	while( !$rs->EOF() ) {
		
		?>
		<li id="SupplierPictogramIndexes_<?php echo $rs->fields( "idpictogram" ); ?>">
			<div style="position:relative; height:<?php echo PICTOGRAM_SIZE; ?>px;">
				<img src="/catalog/thumb.php?src=<?php echo urlencode( "/images/pictograms/" . htmlentities( $rs->fields( "filename" ) ) ); ?>&amp;w=<?php echo PICTOGRAM_SIZE; ?>&amp;h=<?php echo PICTOGRAM_SIZE; ?>" alt="" />
				<div class="label">
					<?php echo htmlentities( $rs->fields( "label" ) ); ?>
				</div>
			</div>
		
		</li>
		<?php

		$rs->MoveNext();
		
	}
	
	?>
	</ul>
	<?php
	
}

//-----------------------------------------------------------------------------------------------------

?>
