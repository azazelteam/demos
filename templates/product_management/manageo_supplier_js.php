<script type="text/javascript">
	<!--
	<?php
	/**
	 * @name getInfos 
	 * @final 	Renvois le résultats de la recherche managéo, 
	 * 			dans le cas ou il y a plusieurs occurences possibles 
	 * 			on renvoi la liste des sociétés correspondantent
	 * 
	 * @param string divToComplete 		:	L'Id de la div dans laquelle la réponse sera insérer
	 * @param string nameFormSearch 	:	L'Id du champs HTML contenant le nom la société à chercher
	 * @param string deptFormSearch 	:	L'Id du champs HTML contenant le département de la société à chercher
	 * @param string sirenFormSearch 	: 	L'Id du champs HTML contenant le SIREN de la société à chercher
	 */
	?>
	function getInfos( divToComplete, nameFormSearch, deptFormSearch, sirenFormSearch ){
					
		var divToComplete = document.getElementById( divToComplete ) ;
		var companyName = document.getElementById( nameFormSearch ).value ;
		var deptCompany = document.getElementById( deptFormSearch ).value ;
		var sirenToManageo = document.getElementById( sirenFormSearch ).value ;
		
		if( (companyName == '' || deptCompany == '') && ( sirenToManageo == '' )){
			alert("Vous devez renseigné une société ainsi que son département ou bien le siren !");
			return;
		}
				
		if( sirenToManageo != ''){
			var data = 'sirenManageo=' + sirenToManageo ;
		}else{
			var data = 'companyName=' + companyName + '&dept='+ deptCompany ;
		}
		
		
		// On lock les champs
		companyName.disabled = true ;
		deptCompany.disabled = true ;
	
		
		divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br /> Recherche en cours ...</div>';
		
		if( sirenToManageo != ''){
			var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php?sirenManageo=' + sirenToManageo ;
		}else{
			var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php?companyName=' + companyName + '&dept='+ deptCompany;
		}

		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
		
		// CONTROLE DE L'ETAT DE LA REQUETE
		// CHAQUE CHANGEMENT D'ETAT AFFICHE UNE LIGNE DANS NOTRE DIV
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
				divToComplete.innerHTML = '<div style="padding:5px;">' + xhr_object.responseText + '</div>' ;
			else
				divToComplete.innerHTML = '<div style="text-align:center;padding:5px;">Impossible de trouver une correspondance !</div>' ;
			}
		}; 
		
		xhr_object.open("POST", location, true);
		// ne pas oublier ça pour le post
		xhr_object.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		xhr_object.send(data);
		
		// On unlock les champs
		companyName.disabled = false ;
		deptCompany.disabled = false ;

	}
	
	<?php
	/**
	 * @name 	getInfosNext 
	 * @final 	Dans le cas ou plusieurs société sont renvoyé par getInfos, 
	 * 			getInfosNext est chargé de recupérer les infos en fonction de la selection de l'utilisateur
	 * 
	 * @param string divToComplete 		:	L'Id de la div dans laquelle la réponse sera insérer
	 */
	
	?>
	function getInfosNext(){
	
		var myUrl = document.getElementById('urlToRecup').value ;
		var divToComplete = document.getElementById( 'infosDeOufs' ) ;
		
		var data = 'selectUrl=' + myUrl;
		
		divToComplete.innerHTML = '<div style="text-align:center; width:100%; height:100%; padding:5px;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Loading ..." /><br />Recherche en cours ...</div>';
		var location = '<?php echo $GLOBAL_START_URL ?>/sales_force/contact_infos.php?selectUrl=' + myUrl ;
		
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
		
		//
		// CONTROLE DE L'ETAT DE LA REQUETE
		// CHAQUE CHANGEMENT D'ETAT AFFICHE UNE LIGNE DANS NOTRE DIV
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
				divToComplete.innerHTML = '<div style="padding:3px; width:100%; height:100%;">' + xhr_object.responseText + '</div>';
			else
				divToComplete.innerHTML = '<div style="text-align:center;padding:5px;">Impossible de trouver une correspondance !</div>' ;
			}
		}; 
		//
		
		xhr_object.open("POST", location, true);
		// ne pas oublier ça pour le post
		xhr_object.setRequestHeader('Content-Type','application/x-www-form-urlencoded');
		xhr_object.send(data);
	}
	
	<?php
	/**
	 * @name 	autoCompleteFromManageo 
	 * @final 	Remplis un fomualire à partir des informations managéo
	 */
	?>
	function autoCompleteFromManageo(){
		var reg = new RegExp("(&nbsp;)", "g");

		// Onglet Infos société
		// document.getElementById("status_company").value = document.getElementById("forme_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("nbr_etab").value = document.getElementById("etablissement_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("last_exo").value = document.getElementById("exercice_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("lenght_exo").value = document.getElementById("dure_exercice_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("cloture_exo").value = document.getElementById("cloture_ewercice_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("ca_from_web").value = document.getElementById("ca_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, '');
		// document.getElementById("vala_web").value = document.getElementById("va_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''); ; 
		// document.getElementById("exed_exploit").value = document.getElementById("exed_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''); ; 
		// document.getElementById("renta_com").value = document.getElementById("rentabilite_com_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("renta_finan").value = document.getElementById("rentatbilite_finan_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("nom_boss").value = document.getElementById("nom_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("fonction_boss").value = document.getElementById("fonction_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("birthday_boss").value = document.getElementById("naissance_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// document.getElementById("birthday_boss_city").value = document.getElementById("lieu_naissance_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		// Fiche Suplier
		document.getElementById("adress").value = document.getElementById("adress_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("zipcode").value = document.getElementById("adress_web_cp").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("city").value = document.getElementById("adress_web_city").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("ape").value = document.getElementById("activite_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		//document.getElementById("Naf").value = document.getElementById("naf_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		//document.getElementById("phone_standard").value = document.getElementById("tel_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		//document.getElementById("fax_standard").value = document.getElementById("fax_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		//document.getElementById("siret").value = document.getElementById("siret_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		//document.getElementById("siren").value = document.getElementById("siren_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		document.getElementById("name").value = document.getElementById("denomination").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 

		//document.getElementById("creationDate").value = document.getElementById("creation_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"") ; 
		//document.getElementById("companyResult").value = document.getElementById("resultat_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''); ; 
		//document.getElementById("companyCapital").value = document.getElementById("capital_web").innerHTML.replace(reg, "").replace(/(^\s*)|(\s*$)/g,"").replace(/ /g, ''); ; 
	}
	
	<?php
	/**
	 * @name 	clearManageo 
	 * @final 	Remet la zone de recherche a zéro 
	 */
	?>
	function clearManageo(){
		var divToComplete = document.getElementById('infosDeOufs') ;
		divToComplete.innerHTML = '';
	}
	
	-->
</script>