<?php 

include_once( dirname( __FILE__ ) . "/../../config/init.php" ); 
include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" ); 
include_once( dirname( __FILE__ ) . "/../../objects/ui/DXComboBox.php" ); 
include_once( dirname( __FILE__ ) . "/../../objects/Contact.php" ); 

/* -------------------------------------------------------------------------------------------------------------------------------- */
							
/**
 * @param ArrayList<Contact> $contacts
 * @param $selectedContact identifiant du contact sélectionné par défaut
 * @param $mainContactCallback callback pour définir le contact sélectionné comme étant le contact principal ( paramètre requis : idcontact )
 * @param $createContactCallback callback pour créer un nouveau contact
 * @param $deleteContactCallback callback pour supprimer le contact sélectionné ( paramètre requis : idcontact )
 * @param $duplicateCallback callback pour dupliquer le contact sélectionné ( paramètre requis : idcontact )
 * @return void
 */
function displayContactEditor( ArrayList $contacts, $selectedContact = 0, $mainContactCallback = false, $createContactCallback = false, $deleteContactCallback = false, $duplicateCallback = false ){

	if( $contacts->size() && !( $contacts->get( 0 ) instanceof Contact ) )
		trigger_error( "ArrayList<Contact> required", E_USER_ERROR ) && exit();
		
	?>
	<style type="text/css">

		#ContactEditor {
			overflow: hidden;
			width: 900px;
			margin: 0 auto;
			padding: 10px;
			background: #f0f0f0;
			border: 1px solid #ccc;
		}

		#ContactEditor #Contacts {
			width: 588px; 
			height: 490px;
			float: left;
			background: #FFF;
			position: relative;
			overflow: hidden;
			border: 1px solid #ccc;
			padding:5px;
		}

		#ContactEditor #ContactList .contactLabel{ margin-top:5px; }
		
		#ContactEditor #ContactList .contactLabel small { 
			padding: 0 0 0 30px; 
			background: url(/images/back_office/content/icons-mailbox-active.png) no-repeat 0 center; 
			font-size: 1em; 
		}
		
		#ContactEditor #ContactList .contactLabel h2{
			border-style:none;
			font-size:12px;
			color:#555555;
			margin-left:5px !important;
		}
		
		#ContactEditor #ContactList .mainContact h2{
			border-style:none;
			font-size:12px;
			color:#1E9CD1;
			margin-left:5px !important;
		}
		
		#ContactEditor #ContactList {
			float: left;
			width: 298px;
			background: #f0f0f0;
			border-right: 1px solid #fff;
			border-left: 1px solid #fff;
			border-top: 1px solid #ccc;
		}
		#ContactEditor #ContactList img {
			border: 1px solid #ccc; 
			padding: 5px; 
			margin-right:5px;
			background: #fff; 
			float: left;
		}
		#ContactEditor #ContactList ul {
			margin: 0; padding: 0;
			list-style: none;
		}
		#ContactEditor #ContactList ul li{
			margin: 0; 
			padding: 5px !important;
			background: #f0f0f0 url(/images/back_office/content/bg_contact_editor_item.gif) repeat-x;
			width: 289px;
			float: left;
			border-bottom: 1px solid #ccc;
			border-top: 1px solid #fff;
			border-right: 1px solid #ccc;
		}
		#ContactEditor #ContactList ul li.hover {
			background: #ddd;
			cursor: pointer;
		}
		#ContactEditor #ContactList ul li.active {
			background: #fff;
			cursor: default;
		}
		#ContactEditor #ContactTools{
			width:580px;
			text-align:right;
		}
		#ContactEditor #ContactTools a{
			margin-left:20px;
		}
		#ContactEditor #AddContactButton{
			float:right;
			padding: 5px !important;
			margin:0px 2px 2px 0px;
			/*background: #f0f0f0 url(/images/back_office/content/bg_contact_editor_item.gif) repeat-x;*/
			width: 289px;
			color:#555555;
			text-align:center;
			/*border: 1px solid #ccc;*/
			cursor:pointer;
			font-weight:bold;
		}
		
	</style>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ----------------------------------------------------------------------------------------------------------------- */
		/* onglets */
		
		$( document ).ready( function() {	

			<?php
			
				if( $selectedContact ){
					
					?>
					$( "#Contact<?php echo $selectedContact; ?>" ).addClass( "active" ); 
					<?php
					
				}
				else{
					
					$done = false;
					$it = $contacts->iterator();
					
					while( !$done && $it->hasNext() ){
						
						$contact =& $it->next();
						
						if( $contact->isMainContact() ){
							
							?>
							$( "#Contact<?php echo $contact->getId(); ?>" ).addClass( "active" ); 
							<?php
							
							$done = true;
							
						}

					}

				}
				
			?>
			$( "#ContactList ul li.EditableContact" ).click( function(){ 

				var idcontact = $( this ).attr( "id" ).substring( 7 );

				$( "[id^=ContactForm]" ).animate( { opacity: 0, marginBottom: 0 }, 250 ).css( "display", "none" );
	
				$( "#ContactForm" + idcontact ).css( "display", "block" ).animate( { opacity: 1.0 }, 250 );

				$( "#ContactList ul li.EditableContact" ).removeClass( "active" );
	
				$( this ).addClass( "active" ); 

				if ( $( this ).is( ".active" ) )
					return false;

				return false;
				
			}).hover( function(){ $( this ).addClass( "hover" ); }, function() { $( this ).removeClass( "hover" ); });

		});

		/* ----------------------------------------------------------------------------------------------------------------- */
		
		function updateContactLabel( idcontact ){

			$( "#ContactLabel" + idcontact ).html( $( "#Contacts_firstname" + idcontact ).val() + " " + $( "#Contacts_lastname" + idcontact ).val() );
			
		}
		
		/* ----------------------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div id="ContactEditor">
	<?php

		if( $createContactCallback ){
			
			?>
			<div id="AddContactButton" onclick="<?php echo $createContactCallback; ?>(); return false;">
				Ajouter un contact
			</div>
			<?php
			
		}
				
		?>
		<br style="clear:both;" />
		<div id="Contacts">
		<?php
		
			$it =& $contacts->iterator();
			$i = 0;
			while( $it->hasNext() ){
				
				$contact =& $it->next();
				
				$displayed = $selectedContact == $contact->getId() || ( !$selectedContact && $contact->isMainContact() );
				
				?>
				<div id="ContactForm<?php echo $contact->getId(); ?>" class="blocMultiple blocMLeft contactForm" style="position:absolute; width:100%; display:<?php echo $displayed ? "block" : "none"; ?>; opacity:<?php echo $displayed ? "1.0" : "0.0"; ?>">
					<div id="ContactTools">
					<?php
		
						if( $deleteContactCallback && $contact->allowDelete() ){
							
							?>
							<a class="blueLink" href="#" onclick="if( confirm( 'Etes-vous certains de vouloir supprimer ce contact?' ) ) <?php echo $deleteContactCallback; ?>( <?php echo $contact->getId(); ?> ); return false;">Supprimer</a>
							<?php
							
						}
						
						if( $duplicateCallback ){
							
							?>
							<a class="blueLink" href="#" onclick="<?php echo $duplicateCallback; ?>( <?php echo $contact->getId(); ?> ); return false;">Dupliquer</a>
							<?php
							
						}
						
						if( $mainContactCallback && !$contact->isMainContact() ){
							
							?>
							<a class="blueLink" href="#" onclick="<?php echo $mainContactCallback; ?>( <?php echo $contact->getId(); ?> ); return false;">Définir comme contact principal</a>
							<?php
							
						}
						
					?>
					</div>
					<h2><?php 
					
						if( $contact->isMainContact() )
							echo "Contact principal : ";
							
						echo htmlentities( $contact->getFirstName() . " " . $contact->getLastName() ); 
	
					?>
					</h2>
					 
					<label><span>Titre</span></label>
					<div class="simpleSelect">
						<?php XHTMLFactory::createSelectElement( "title", "idtitle", "title_1", "title_1", $contact->getGender(), false, false, "name=\"contacts[idtitle][]\" id=\"Contacts_idtitle" . $contact->getId() . "\"" ); ?>
					</div>
					<div class="spacer"></div>
					
					<label><span>Nom</span></label>
					<input type="text" name="contacts[lastname][]" id="Contacts_lastname<?php echo $contact->getId(); ?>" value="<?php echo htmlentities( $contact->getLastName() );  ?>" class="textidInput" onkeyup="this.value=this.value.toUpperCase(); updateContactLabel(<?php echo $contact->getId(); ?>);" style="text-transform:capitalize;" />
					
					<label class="smallLabel"><span>Prénom</span></label>
					<input type="text" name="contacts[firstname][]" id="Contacts_firstname<?php echo $contact->getId(); ?>" value="<?php echo htmlentities( $contact->getFirstName() );  ?>" class="textidInput" onkeyup="this.value=this.value.toUpperCase(); updateContactLabel(<?php echo $contact->getId(); ?>);" style="text-transform:capitalize;" />
					<div class="spacer"></div>
										
					<div style="margin-top:15px;">
						<label><span>Tél.</span></label>
						<input type="text" name="contacts[phonenumber][]" id="Contacts_phonenumber<?php echo $contact->getId(); ?>" value="<?php echo htmlentities( $contact->getPhoneNumber() );  ?>" class="textidInput" />
						
						<label class="smallLabel"><span>GSM</span></label>
						<input type="text" name="contacts[gsm][]" id="Contacts_gsm<?php echo $contact->getId(); ?>" value="<?php echo htmlentities( $contact->getGSM() );  ?>" class="textidInput" />
					</div>
					<div style="margin-top:15px;">
						<label><span>Email</span></label>
						<input type="text" name="contacts[email][]" id="Contacts_email<?php echo $contact->getId(); ?>" value="<?php echo htmlentities( $contact->getEmail() );  ?>" class="textidInput" onkeyup="$('#ContactEmail<?php echo $contact->getId(); ?>').html(this.value);" />
						
						<label class="smallLabel"><span>Fax</span></label>
						<input type="text" name="contacts[faxnumber][]" id="Contacts_faxnumber<?php echo $contact->getId(); ?>" value="<?php echo htmlentities( $contact->getFaxnumber() );  ?>" class="textidInput" />
					</div>
					
					<div class="spacer"></div>	
					<div style="margin-top:15px;">
						<label><span>Fonction</span></label>
						<div class="simpleSelect">
						<?php 
		
			            	XHTMLFactory::createSelectElement( "function", "idfunction", "function_1", "function_1", $contact->getFunction(), false, "", "name=\"contacts[idfunction][]\" id=\"Contacts_idfunction" . $contact->getId() . "\"" );	
							DXComboBox::create( "idfunction" . $contact->getId() ); 

						?>
						</div>
						<label class="smallLabel"><span>Service</span></label>
						<div class="simpleSelect">
						<?php 
		
			            	XHTMLFactory::createSelectElement( "department", "iddepartment", "department_1", "department_1", $contact->getDepartment(), false, "", "name=\"contacts[iddepartment][]\" id=\"Contacts_iddepartment" . $contact->getId() . "\"" );	
							DXComboBox::create( "iddepartment" . $contact->getId() ); 

						?>
						</div>
					</div>

					<div class="spacer"></div>	

					<div style="margin-top:15px;">
						<label style="height:116px;"><span><strong>Commentaire<br/>&nbsp;</strong></span></label>
						<textarea style="height:116px; width:72%; padding:0px;" name="contacts[comment][]" id="Contacts_comment<?php echo $contact->getId(); ?>"><?php echo htmlentities( $contact->getComment() ); ?></textarea>
					</div>	
							
					<div class="spacer"></div>	
						
				</div><!-- blocMLeft -->
				<?php
				
				$i++;
				
			}
			
		?>
		</div><!-- Contacts -->
		<div id="ContactList">
			<ul>
			<?php
	
				$it =& $contacts->iterator();
				
				while( $it->hasNext() ){
					
					$contact =& $it->next();
					
					?>
					<li class="EditableContact<?php if( $contact->isMainContact() ) echo " mainContact"; ?>" id="Contact<?php echo $contact->getId(); ?>">
						<a href=#">
							<img src="http://www.lifelounge.com/resources/IMGTHUMB/Real_doll_gallery_thumb.jpg" style="width:50px; height:50px;" alt="photo" />
						</a>
						<div class="contactLabel">
							<h2 id="ContactLabel<?php echo $contact->getId(); ?>"><?php echo htmlentities( $contact->getFirstName() ) . " " . htmlentities( $contact->getLastName() ); ?></h2>
							<small>
								<a href="mailto:<?php echo $contact->getEmail(); ?>" id="ContactEmail<?php echo $contact->getId(); ?>">
									<?php echo htmlentities( $contact->getEmail() ); ?>
								</a>
							</small>
						</div>
					</li>
					<?php
					
				}
	
			?>
			</ul>
		</div>
	</div>
	<?php

}

/* -------------------------------------------------------------------------------------------------------------------------------- */

?>