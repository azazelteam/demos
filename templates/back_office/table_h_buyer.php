<table border="0" cellspacing="0" cellpadding="0" width="100%" class="ContentTable">
	<tr>
		<td class="TopLeftCorner"></td>
		<td class="FrameBorderTop"></td>
		<td class="TopRightCorner"></td>
		<td class="Empty" colspan="2"></td>
	</tr>
	<tr>
		<td class="FrameBorderLeft"></td>
		<td id="TitleCell">
			<?php
			
			if( $IdEstimate != 0 ){
				
				?>
				<span class="Estimate_num"> <?php echo Dictionnary::translate("n_estimate") ?> : <?php echo $IdEstimate ?></span>
				<?php 
				
			}
			else{
				
				?>
				<span class="Estimate_num"> <?php echo Dictionnary::translate("n_estimate") ?> : </span><span class="Creation">En création</span></h1>
				<?php 
				
			}
			
			?>
			<!-- staut du devis -->
			<br /><span class="Estimate_date"><?php echo humanReadableDatetime( $Order->getValue( "DateHeure" ) ) ?></span>
		</td>
		<td class="FrameBorderRight"></td>
		<td class="TableTabRight" style="width:200px;"></td>
		<td class="TableTabRight" style="width:10px;"></td>
	</tr>
	<tr>
		<td class="FrameBorderLeft"></td>
		<td colspan="3" class="TableContent">