<?php

	include_once( "../../config/init.php" );
	include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
?>
<link rel="stylesheet" type="text/css" href="/css/back_office/form_default.css" />

<div class="centerMax"> <!-- Bloc extensible selon la résolution de l'utilisateur -->
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<!-- Cas 1 : Formulaire avec un 2 colonnes -->
	<div class="rightTools"> <!-- Colonne de droite -->
		<h2>&nbsp;Mon titre de droite</h2>
		<div class="spacer"></div>
	
		<div class="blocSearch">
			<a href="#" class="normalLink blueText">Lien numéro 1</a>
			<a href="#" class="normalLink blueText">Lien numéro 2</a>
			<a href="#" class="normalLink blueText">Lien numéro 3</a>
		</div>
	</div> <!-- Fin .rightTools -->
	
	<div class="contentDyn"> <!-- Bloc de recherche -->
		<h1 class="titleSearch">
			<span class="textTitle">Mon titre de recherche</span>
			<span class="selectDate">Informations complémentaires</span>
			<div class="spacer"></div>
		</h1>
		<div class="spacer"></div>
		
		<div class="blocSearch">
			<div class="marginLeft">
				<!-- Affichage sur deux colonnes -->
				<div class="blocMultiple blocMLeft">
					<h2>Mon sous-titre numéro 1</h2>
					
					<!-- Label type 1 : Input default -->
					<label><span>Mon label</span></label>
					<input type="text" name="champ1" class="textInput" />
					<div class="spacer"></div>
					
					<!-- Label type 2 : Select -->
					<label><span>Mon label</span></label>
					<select name="champ2"><option value="">Choix1</option></select>
					<div class="spacer"></div>
					
					<!-- Label type 3 : Input zipcode - city -->
					<label><span>Code postal</span></label>
					<input type="text" name="champ3" class="numberInput" />
					<label class="smallLabel"><span>Ville</span></label>
					<input type="text" name="champ3" class="mediumInput upperCase" /> <!-- Attention : upperCase passe en majuscule la typo -->
					<div class="spacer"></div>
					
					<!-- Label type 4 : Input lastname - firstname  -->
					<label><span>Nom</span></label>
					<input type="text" name="champ3" class="textidInput" />
					<label class="smallLabel"><span>Prénom</span></label>
					<input type="text" name="champ3" class="textidInput" /> <!-- Attention : upperCase passe en majuscule la typo -->
					<div class="spacer"></div>
					
					<!-- Label type 5 : Info text -->
					<label class="smallLabelMin2"><span>Capital</span></label>
					<span class="textidSimpleMin">Blalal</span>
					<label class="smallLabelMin"><span>Capital</span></label>
					<span class="textidSimpleMin">Blalal</span>
					<div class="spacer"></div>
					
					<!-- Label type 6 : Input + Select -->
					<label><span>Titre</span></label>
					<div class="simpleSelect"><select name="champ5"><option value="">Mr</option></select></div>
					<label class="smallLabel"><span>Nom</span></label>
					<input type="text" name="champ9" class="textidInput" />
					<div class="spacer"></div>
					
				</div>
				<div class="blocMultiple blocMRight">
					<h2>Mon sous-titre numéro 1</h2>
					<div class="spacer"></div>
				</div>		
				<div class="spacer"></div>
						
			</div><!-- Fin .marginLeft -->
			<div class="spacer"></div>
			
		</div><!-- Fin .blocSearch -->
		<div class="spacer"></div>
		
	
	</div><!-- Fin .contentDyn -->
	<div class="spacer"></div>	
	
			
	<!-- Cas 2 : Resultat de la recherche -->
	<div class="contentResult">
		
		<h1 class="titleSearch">
			<span class="textTitle">Mon titre de recherche</span>
			<span class="selectDate">Informations complémentaires</span>
			<div class="spacer"></div>
		</h1>
		<div class="spacer"></div>

		<div class="blocResult mainContent">
			<div class="content">
				<table class="dataTable resultTable">
					<thead>
					<tr>
						<th>Com.</th>
						<th>Client n°</th>
						<th>Dép.</th>
						<th>Raison sociale</th>
						<th>Provenance</th>
						<th>Type</th>
						<th>Scoring</th>
						<th>Référencé</th>
						<th>Statut</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>Com.</td>
						<td>Client n°</td>
						<td>Dép.</td>
						<td>Raison sociale</td>
						<td>Provenance</td>
						<td>Type</td>
						<td>Scoring</td>
						<td>Référencé</td>
						<td>Statut</td>	
					</tr>					
					</tbody>
				</table>
			</div>				
		</div>
		<div class="spacer"></div>

	</div><!-- Fin .contentResult -->
	
	
	
	
	<!-- Cas 3 : Resultat de la recherche avec Onglet -->
	<div class="contentResult">
		<h1 class="titleSearch">
			<span class="textTitle">Mon titre de recherche</span>
			<span class="selectDate">Informations complémentaires</span>
			<div class="spacer"></div>
		</h1>
		<div class="spacer"></div>
		
		<ul class="menu">
            <li class="item"><a href="#onglet1"><span>Onglet 1</span></a></li>
            <li class="item"><a href="#onglet2"><span>Onglet 2</span></a></li>
            <li class="item"><a href="#onglet3"><span>Onglet 3</span></a></li>
            <li class="item"><a href="#onglet4"><span>Onglet 4</span></a></li>
        </ul>
		<div class="spacer"></div>

		<div class="blocResult mainContent withTab">
			<div class="content">
				<table class="dataTable resultTable">
					<thead>
					<tr>
						<th>Com.</th>
						<th>Client n°</th>
						<th>Dép.</th>
						<th>Raison sociale</th>
						<th>Provenance</th>
						<th>Type</th>
						<th>Scoring</th>
						<th>Référencé</th>
						<th>Statut</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>Com.</td>
						<td>Client n°</td>
						<td>Dép.</td>
						<td>Raison sociale</td>
						<td>Provenance</td>
						<td>Type</td>
						<td>Scoring</td>
						<td>Référencé</td>
						<td>Statut</td>	
					</tr>					
					</tbody>
				</table>
			</div>				
		</div>
		<div class="spacer"></div>

	</div> <!-- Fin .contentResult -->
	

</div> <!-- Fin .centerMax -->

<?php
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>