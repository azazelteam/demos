<?php

include_once( dirname( __FILE__ ) . '/../../config/init.php' ) ;

include_once( "$GLOBAL_START_PATH/templates/back_office/status_bar.fct.php" );

if( $_SERVER[ "PHP_SELF" ] == "/admin.php" )
	return;

if( !User::getInstance()->getId() )
	return;

if( isset( $banner ) && $banner == "no" )
	return;

//Ne pas envoyer de header quand on fait une inclusion du PIED de page
if( isset( $_POST[ "makeInit" ] ) ){
	
	header('Content-Type: text/html; charset=UTF-8');
	
	getContent();
	
	exit();
	
}

// On récupères les infos dont on a besoin : 

function getContent(){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;

	// on récup les infos du fichier txt
	if( file_exists( $GLOBAL_START_PATH."/templates/back_office/status_bar_values.txt") ){
		$array = file($GLOBAL_START_PATH."/templates/back_office/status_bar_values.txt");
		
		if( count( $array ) < 9 )
			exit('Erreur dans le fichier d\'informations');
			
		$seuilRentatbilite	= $array[0];
		$netMargin			= $array[1];
		$nbrEstimate		= $array[2];
		$nbrOrder			= $array[3];
		$resultatCumul 		= $array[4];
		$resultatCumulAn	= $array[5];
		$caDay 				= $array[6];
		$resultat			= $array[7];
		$nbConnectes 		= $array[8];
		//$resultCAn 			= $array[9];

	}else return;

?>
	<a href="#" onclick="toggleVisibility(); return false;" id="closeButton">Fermer</a>
	<div class="nav_menu" id="nav_menu">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<!-- Fomulaire pour les recherche auo sur le devis  -->
		<form method="post" id="searchStatusEstimate" action="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_search_estimate.php">
			<div style="display:inline;">
				<input type="hidden" name="search" value="1" />
				<input type="hidden" name="EstimateToDelete" value="0" />
				<input type="hidden" name="use_estimate" value="0" />
				<input type="hidden" name="date_type" value="creation_date" />
				<input type="hidden" name="interval_select" value="1" />
				<input type="hidden" name="minus_date_select" value="-30" />
				<input type="hidden" name="scoring_date" value="" />
				<input type="hidden" name="scoring_tx" value="" />
				<input type="hidden" name="sp_status" value="ToDo" />
			</div>
		</form>
		<form method="post" id="searchStatusOrder" action="<?php echo $GLOBAL_START_URL ?>/sales_force/order_search.php">
			<div style="display:inline;">
				<input type="hidden" name="OrderToDelete" value="0" />
				<input type="hidden" name="search" value="1" />
				<input type="hidden" name="interval_select" value="1" />
				<input type="hidden" name="minus_date_select" value="-30" />
				<input type="hidden" name="status" value="ToDo" />
			</div>
		</form>
		
		<?php
			
			if ( $nbConnectes == 0 )
				$connected = '<span class="grasBack">Aucun</span> visiteur';
			elseif ( $nbConnectes == 1 )
				$connected = '<span class="grasBack">1</span> visiteur';
			elseif ( $nbConnectes > 1 )
				$connected = '<span class="grasBack">' . $nbConnectes . '</span> visiteurs';
			
		?>
			<!-- <li><a href="http://www.google.com/analytics/fr-FR/" onclick="window.open(this.href); return false;"><?php echo $connected ?></a></li> 
			<li class="separator"></li>-->
			
			
			<p style="text-align:center;">
			<?php if( B2B_STRATEGY ){ ?>
				<a href="#" onclick="document.getElementById('searchStatusEstimate').submit();">
				<span class="grasBack"><?php echo $nbrEstimate ?></span> devis &agrave; traiter</a>
			<?php } ?>
			<span class="separator"></span>
			
			<a href="#" onclick="document.getElementById('searchStatusOrder').submit();">
			<span class="grasBack"><?php echo $nbrOrder ?></span> commandes &agrave; traiter</a>
			
			
			<?php if( B2B_STRATEGY ){ ?>
				<span class="separator"></span>
				
				<a href="<?php echo $GLOBAL_START_URL ?>/statistics/statistics.php?cat=month_stats">
				CA : <span class="grasBackBlue"><?php echo Util::priceFormat($caDay); ?></span></a>
				<span class="separator"></span>
				
				<a href="<?php echo $GLOBAL_START_URL ?>/statistics/statistics.php?cat=month_stats">
				Marge nette : <span class="grasBackBlue"><?php echo Util::priceFormat($netMargin); ?></span></a>
				<span class="separator"></span>
				
				<a href="<?php echo $GLOBAL_START_URL ?>/statistics/statistics.php?cat=month_stats">
				R&eacute;sultat du jour: <span class="<?php echo getCssColor($resultat) ?>"><?php echo Util::priceFormat($resultat); ?></span></a>
				<span class="separator"></span>
				
				<a href="<?php echo $GLOBAL_START_URL ?>/statistics/statistics.php?cat=month_stats">
				R&eacute;sultat mensuel : <span class="<?php echo getCssColor($resultatCumul) ?>"><?php echo Util::priceFormat($resultatCumul); ?></span></a>
				
				<?php /*
				<span class="separator"></span>
				<a href="<?php echo $GLOBAL_START_URL ?>/statistics/statistics.php?cat=month_stats">
				R&eacute;sultat annuel : 
				<span class="<?php echo getCssColor($resultCAn) ?>"><?php echo Util::priceFormat($resultCAn); ?></span>
				</a>
				*/
				?>
			<?php } ?>
			</p>
	</div>
<?php
	
}

?>
<div id="nav_menu_wrapper" class="noprint"><?php getContent(); ?></div>
<script type="text/javascript">
<!--
	
	$(document).ready(function(){
		
		initStatusBar();
		
	});
	
	function initStatusBar(){
		var cookieExist ;
		cookieExist = LireCookie( "statusBar" ) ;
		if( cookieExist == "true"){
			
			$("#nav_menu").slideUp();
			$("#closeButton").html( "Ouvrir" );
			EcrireCookie("statusBar", true);
			
		}
		else if( cookieExist == null || cookieExist == "false" ){
			
			$("#nav_menu").slideDown();
        	$("#closeButton").html( "Fermer" );
			EcrireCookie("statusBar", false);
			updateStatusBar();
			
		}
	}
	
	function getCookieVal(offset){
		var endstr=document.cookie.indexOf (";", offset);
		if (endstr==-1) endstr=document.cookie.length;
		return unescape(document.cookie.substring(offset, endstr));
	}
	
	function EcrireCookie( statusBar, valeur ){
	
		var argv=EcrireCookie.arguments;
		var argc=EcrireCookie.arguments.length;
		var expires=(argc > 2) ? argv[2] : null;
		//var path=(argc > 3) ? argv[3] : null;
		var path='/';
		var domain=(argc > 4) ? argv[4] : null;
		var secure=(argc > 5) ? argv[5] : false;
		document.cookie=statusBar+"="+escape(valeur)+
		((expires==null) ? "" : ("; expires="+expires.toGMTString()))+
		((path==null) ? "" : ("; path="+path))+
		((domain==null) ? "" : ("; domain="+domain))+
		((secure==true) ? "; secure" : "");
		
	}
	
	function LireCookie( nom ){
		
		var arg=nom+"=";
		var alen=arg.length;
		var clen=document.cookie.length;
		var i=0;
		while (i<clen)
		{
			var j=i+alen;
			if (document.cookie.substring(i, j)==arg) return getCookieVal(j);
			i=document.cookie.indexOf(" ",i)+1;
			if (i==0) break;
		}
		return null;
		
	}
	
	function toggleVisibility( show ){
		
		if( $("#nav_menu").is(":hidden") ){
        	
        	$("#nav_menu").slideDown();
        	$("#closeButton").html( "Fermer" );
			EcrireCookie("statusBar", false);
			updateStatusBar();
        	
        }else{
        	
        	$("#nav_menu").slideUp();
        	$("#closeButton").html( "Ouvrir" );
			EcrireCookie("statusBar", true);
        	
        }
		
	}
	
	function updateStatusBar(){
		
		if( $("#closeButton").html() == "Ouvrir" )
			return ;
		
		$.ajax({
			type: 'POST',
			url: '<?=$GLOBAL_START_URL?>/templates/back_office/status_bar.php',
			data: 'makeInit=false',
			success: function(response){
				$('#nav_menu_wrapper').html(response);
			}
		});
					
		setTimeout("updateStatusBar()", 30000); // 30 secondes
	}
			
//-->
</script>