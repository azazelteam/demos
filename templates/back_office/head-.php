<?php

include_once( dirname( __FILE__ ) . "/../../config/config.php" );
include_once( dirname( __FILE__ ) . "/../../script/global.fct.php" );

//------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/objects/DBUtil.php" );
include_once( "$GLOBAL_START_PATH/objects/Menu.php" );
include_once( "$GLOBAL_START_PATH/objects/Session.php" );
include_once( "$GLOBAL_START_PATH/objects/User.php" );
include_once( "$GLOBAL_START_PATH/objects/UserAccessControl.php" );

//------------------------------------------------------------------------------------------

/*
 * $js : tableau des javascript à inclure
 * $css : tableau des css à inclure
 * $bodytag : tags à inclure dans le body
 */

//connexion
if( isset( $_POST[ "AdminLogin" ] ) && isset( $_POST[ "AdminPassword" ] ) )
	User::getInstance()->login( stripslashes( $_POST[ "AdminLogin" ] ), md5( stripslashes( $_POST[ "AdminPassword" ] ) ) );

//déconnexion
if( isset( $_REQUEST[ "logout" ] ) ){
	
	if( isset( $_SESSION[ "UserMenu" ] ) )
		unset( $_SESSION[ "UserMenu" ] );
		
	User::getInstance()->logout();

}

//------------------------------------------------------------------------------------------

$Title = isset( $Title ) ? Util::checkEncoding($Title) : Util::checkEncoding(DBUtil::query( "SELECT name_1 FROM menu WHERE idmenu = '" . Menu::getMenuIdentifier() . "' LIMIT 1" )->fields( "name_1" ));

$Title_head = isset( $Title_head ) ? $Title_head : Util::checkEncoding($Title);

//------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------
// Vérification du login 
//---------------------------------------------------------------------------------------

if( !User::getInstance()->getId() ){
	
	$error = "";
	
	if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) )
		$error = "Echec lors de l'authentification.<br />Veuillez resaisir votre identifiant et votre mot de passe.";
	
	displayLogonPanel( $error );
	
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

//---------------------------------------------------------------------------------------
// Modification du mot de passe
//---------------------------------------------------------------------------------------

// Si la variable est affectée
if( isset( $_POST[ "changePassword" ] ) || isset( $_POST[ "changePassword_x" ] ) ){
	
	$changed = false;
	
	if( isset( $_POST[ "newPassword" ] ) && isset( $_POST[ "newPasswordConfirmation" ] ) && !empty( $_POST[ "newPassword" ] ) && $_POST[ "newPassword" ] == $_POST[ "newPasswordConfirmation" ] )
		$changed = User::getInstance()->changePassword( stripslashes( md5( $_POST[ "newPassword" ] ) ) );
		
	if( !$changed ){
		
		displayLogonPanel( "Votre mot de passe n'a pas été modifié.<br />Si le problème persiste, veuillez contacter un administrateur." );
		
		include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
		
		exit();
		
	}
		
}

//---------------------------------------------------------------------------------------
// Droits d'accès à la page demandée
//---------------------------------------------------------------------------------------

if( !UserAccessControl::allowAccess( User::getInstance()->getId(), Menu::getMenuIdentifier() ) ){
	
?>

		<p style="text-align:center;" class="alert">
			<?php echo Dictionnary::translate( "access_denied" ) ?>
		</p>

<?php
		
	exit();
		
}

$iduser = User::getInstance()->getId();

if( substr( $_SERVER[ "PHP_SELF" ], -7 ) != "admin.php" ){
	
	$skin = User::getInstance()->get( "skin" );
	

?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title><?php echo htmlentities( $Title_head ) ?></title>
    
    <link rel="stylesheet<?php echo empty( $skin ) || $skin == "default" || $skin == "blue" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout.css" title="blue" />
    <link rel="stylesheet<?php echo $skin == "green" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_green.css" title="green" />
    <link rel="stylesheet<?php echo $skin == "gray" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_gray.css" title="gray" />
    <link rel="stylesheet<?php echo $skin == "red" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_red.css" title="red" />
    <link rel="stylesheet<?php echo $skin == "orange" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_orange.css" title="orange" />
    <link rel="stylesheet<?php echo $skin == "pink" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_pink.css" title="pink" />
    <link rel="stylesheet<?php echo $skin == "yellow" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_yellow.css" title="yellow" />
    <link rel="stylesheet<?php echo $skin == "purple" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_purple.css" title="purple" />
    <link rel="stylesheet<?php echo $skin == "hermes" ? "" : " alternate" ?>" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/layout_hermes.css" title="hermes" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/struct.css" />
    
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/contents.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/js/autocompletemenu/autocompletion.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/jquery/jquery.tabs.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/status_bar.css" /> <!-- CSS Barre de status -->
    
    <?php
	//inclusion des css "externes"
	if( isset( $css ) ){
		foreach( $css as $cssline ){
			echo "	<link rel=\"stylesheet\" type=\"text/css\" rev=\"stylesheet\" href=\"$GLOBAL_START_URL/css/$cssline\" />\n";
		}
	}
	?>
	
    <!-- JavaScript -->
    <script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/autocompletemenu/autocompletemenu.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/displayorder.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery-1.3.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery-ui-1.7.2.custom.min.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/ui/i18n/ui.datepicker-fr.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/ui/blockUI.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.history_remote.pack.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.droppy.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.qtip.min.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery.jeditable.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/json.js"></script>
	
<?php
	//inclusion des JavaScripts "externes"
	if( isset( $js ) ){
		foreach( $js as $jsline ){
			echo "	<script type=\"text/javascript\" src=\"$GLOBAL_START_URL/js/$jsline\"></script>\n";
		}
	}
	
?>
	<script type="text/javascript">
	<!--
		
		//PopUp pour sélectionner un article existant dans la BD
		function catselsee(){
			
			postop = ( self.screen.height - 600 ) / 2;
			posleft = ( self.screen.width - 750 ) / 2;
			window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/cat_select.php?HideAdd', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
			
		}
		
		function showSubMenu( idpanelmenu, idsubmenu ){
			
			window.document.getElementById( "Menu_" + idpanelmenu + "_" + idsubmenu ).style.zIndex = "999";
			window.document.getElementById( "subMenu_" + idpanelmenu + "_" + idsubmenu ).style.display = "block";
			
		}
		
		function hideSubMenu( idpanelmenu, idsubmenu ){
			
			window.document.getElementById( "Menu_" + idpanelmenu + "_" + idsubmenu ).style.zIndex = "998";			
			window.document.getElementById( "subMenu_" + idpanelmenu + "_" + idsubmenu ).style.display = "none";
			
		}
		
		$(document).ready(function(){
			$("input[name='interval_select']").parent().click(function(){
				$(this).children('input').attr('checked',true);
			});
		});
		
		-->
	</script>
	
	<style type="text/css">
		@media print {
			.noprint {display:none !important;}
			.noshow {display:block !important;}
		}
		
		@media screen {
			/* .noprint {display:block !important;} */
			.noshow {display:none !important;}
		}
	</style>
</head>
<body<?php echo  isset( $bodytag ) ? " $bodytag" : "" ?>>
	<div id="global">
<?php

if( !isset( $banner ) || $banner != "no" ){
	
	include_once( dirname( __FILE__ ) . "/../../objects/Encryptor.php" );
	
	if( !isset( $_GET[ "reset_menu" ] ) && isset( $_SESSION[ "UserMenu" ] ) ){

		$q1 = "SELECT active FROM language_active WHERE idsession ='". $_COOKIE["FrontOfficeNetlogix"] ."' ";
					$rs1 =& DBUtil::query( $q1 );
					
					$active		= $rs1->fields( "active" );

		if( isset( $_POST["Francais"] ) || isset( $_POST["Anglais"] )|| $active!=0 )
		{
			$menu = new Menu();
			$_SESSION[ "UserMenu" ] = URLFactory::base64url_encode( Encryptor::encrypt( serialize( $menu ), $GLOBAL_DB_PASS ) );
		}
		
		
		$menu = unserialize( Encryptor::decrypt( URLFactory::base64url_decode(  $_SESSION[ "UserMenu" ] ), $GLOBAL_DB_PASS ) );
		$menu->update();
			
	}
	else{

		$menu = new Menu();
		$_SESSION[ "UserMenu" ] = URLFactory::base64url_encode( Encryptor::encrypt( serialize( $menu ), $GLOBAL_DB_PASS ) );
		
	}

	$menuArray = $menu->getMenu();
	
	//Récupération de l'environnement métier sélectionné
	foreach( $menuArray[ "children" ] as $index => $value ){		
		if( $value[ "selected" ] == 1 ){	
			$mainCategory = $index;
		}
	}
	
?>
        <div id="header" class="noprint">
        	<div id="left">
            	<div id="companyLogoContainer">
                    <a href="<?php echo $GLOBAL_START_URL ?>">
					<?php if( $GLOBAL_START_URL == "http://..." || $GLOBAL_START_URL == "http://...local" ){ ?>
						<img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/www/logo.png" alt="vers <?php echo $GLOBAL_START_URL ?>" />
					<?php }else{ ?>
						<img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/www/logo.png" alt="vers <?php echo $GLOBAL_START_URL ?>" />
					<?php } ?>
                    </a>
                </div>
            </div>
            <div id="middle">
            
            
            
            
            
              <?php
					$requestedURL = isset( $_REQUEST[ "logout" ] ) ? "$GLOBAL_START_URL/admin.php" : $GLOBAL_START_URL . $_SERVER[ "REQUEST_URI" ];
					//print $requestedURL;
					
					?>
                    <ul style=" padding: 10px 0 0;">
                   
                    
                    <form name="LoginForm" id="loginForm" method="post" action="<?php echo $requestedURL ?>" enctype="multipart/form-data">                     
					
                    <?php /*
					$query = "SELECT * FROM language";
					$rs =& DBUtil::query( $query );
                    
					while( !$rs->EOF() ){
					$id_lang	= $rs->fields( "id_lang" );
					$name		= $rs->fields( "name" );
					$designation		= $rs->fields( "designation" );
					$flag		= $rs->fields( "flag" );
					$q1 = "SELECT active FROM language_active WHERE idsession ='". $_COOKIE["FrontOfficeNetlogix"] ."' ";
					$rs1 =& DBUtil::query( $q1 );
					
					$active		= $rs1->fields( "active" );
				
					if ($active == $id_lang){
					?>
					 
                    <input type="submit" name="<?php echo $name ?>" style="background:url(../../images/flags/<?php echo  $flag; ?>) no-repeat center;" class="submitField flagLang" value="<?php echo $designation ?>"  /> <span style="color:#FF0000; font-weight:bold;"> <?php echo  $designation; ?></span> &nbsp;
                   <?php
				   }else{
				   ?>
                    <input type="submit" name="<?php echo $name ?>" style="background:url(../../images/flags/<?php echo  $flag; ?>) no-repeat center;" class="submitField flagLang" value="<?php echo $designation ?>"/><span style="font-weight:bold;"> <?php echo  $designation; ?></span>&nbsp;
					<?php
					}
					$rs->MoveNext();

					} 
					*/ ?>
					
					<!--<input type="submit" name="Francais" class="submitField" value="Fr" />-->
                    <!--<input type="submit" name="Anglais" class="submitField" value="En" />-->
				
					</form>

                    </ul>
            </div>
            <div id="right">
                <div id="rightLeft"></div>
                <div id="rightRight">
					<ul class="iconsMenu">
						<?php
							
							//Récupération des environnements métier accessibles
							foreach( $menuArray[ "children" ] as $index => $value )
							{
								$value[ "name" ] = $value[ "name" ];
								echo "<li class='icons$index" . ( $value[ "selected" ] == "1" ? " selectedMainCategory" : "" ) . "'><a href=\"$GLOBAL_START_URL" . $value[ "path" ] . "\"><span>&nbsp;</span>" . $value[ "name" ] . "</a></li>\n";	
							}
							
						?>
						
						<!-- <li class='iconsHouse'><a href="<?php echo $GLOBAL_START_URL ?>/admin.php"><span>&nbsp;</span>Accueil</a></li>
						<li class='iconsInfos<?php if($_GET[ "info" ] == "1") { echo " selectedMainCategory";} ?>'><a href="<?php echo $GLOBAL_START_URL ?>/help.php?info=1"><span>&nbsp;</span>Informations</a></li> -->
						<li class='iconsQuitter'><a href="<?php echo $GLOBAL_START_URL ?>/admin.php?logout=ok"><span>&nbsp;</span>Quitter</a></li>
					</ul>
                    <br />
                  
                    
                </div>
            	<div id="connectedBuyer" style="display:none;">
            	<div style="display:none;"><?php echo date( "H:m" ) ?></div>
            	<div style="display:none;">
				</div>
                </div>
            </div>
            	<!-- Menu -->
				<?php
	            if( User::getInstance()->getId() ){
					include( "$GLOBAL_START_PATH/templates/back_office/menu.php" );	 
                }
				?>
	            	            
        </div> <!-- FIN Menu -->
<?php

	}
	
}

//---------------------------------------------------------------------------------------			

/**
 * Formulaire pour le login
 */
function displayLogonPanel( $error = "" ){
		
	global	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	
	$requestedURL = isset( $_REQUEST[ "logout" ] ) ? "$GLOBAL_START_URL/admin.php" : $GLOBAL_START_URL . $_SERVER[ "REQUEST_URI" ];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Akilaé - Solution e-CRM / ERP e-B2B/B2C</title>
	<script src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/home.css" />
    
    
    <script type="text/javascript">
	<!--
		
		//Switch Login/Modification de mot de passe
		function switchPassword(){
			
			var targetElement = document.getElementById( "change_mdp" );
			var connectDiv =  document.getElementById( "mainConnect" );
			
			if( targetElement.style.display == "none" ){
				targetElement.style.display = "";
				connectDiv.style.display = "none";
			} else {
				
				targetElement.style.display = "none";
				connectDiv.style.display = "";
			}
		}
		
		//Controle du Login
		function checkLogin(){
			
			var data = "login=" + document.getElementById( "loginField" ).value + "&password=" + document.getElementById( "passwordField" ).value;
			
			$.ajax({
				url: "<?php echo $GLOBAL_START_URL ?>/secure/login.php",
				async: true,
				type: "GET",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert("Echec lors de l'authentification."); },
			 	success: function( responseText ){
			 		//Affichage des elements
			 		if(responseText == "0" ){
						document.getElementById( 'loginMessage' ).innerHTML = "Echec lors de l'authentification.";
					} else {	
						document.getElementById( 'loginForm' ).submit();	
					}		
				}
			});
						
		}
		
		
		//Value des champs input
		jQuery(document).ready(function(){

			/* Value des inputs */
			$.fn.defaultBehavior = function(el){
			    return this.each(function() {
			      var defaultValue = $(this).attr('title');
			      $(this).attr('title','');
			
			      $(this).focus(function(){
			        if($(this).val() == defaultValue)
			          $(this).val('');
			        $(this).removeClass('inputEmpty');
			      });
			      $(this).blur(function(){
			        var value = $(this).val();
			        if(jQuery.trim(value) == ''){
			          $(this).val(defaultValue);
			          $(this).addClass('inputEmpty');
			        }
			        else
			          $(this).removeClass('inputEmpty');
			      });
			
			      var value = $(this).val();
			      if (value == '' || value == defaultValue)
			      {
			        $(this).val(defaultValue);
			        $(this).addClass('inputEmpty');
			      }
			    });
			  };
			  $('.inputText').defaultBehavior();
				
		});
		
		
		
	-->
	</script>
</head>
<body onload="document.getElementById( 'loginField' ).focus();">
<div id="header">
	<a href="<?php echo $GLOBAL_START_URL ?>">
	<?php if( $GLOBAL_START_URL == "http://..." || $GLOBAL_START_URL == "http://....local" ){ ?>
		<img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/www/logo.png" alt="vers <?php echo $GLOBAL_START_URL ?>" />
	<?php }else{ ?>
		<img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/www/logo.png" alt="vers <?php echo $GLOBAL_START_URL ?>" />
	<?php } ?>
    </a>
</div>
<div id="global">
	<div class="form" id="mainConnect">
		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/logo.png" width="" />
		<div class="spacer"></div>
		
		<p id="loginMessage"><?php echo $error ?></p>
		
		<div id="loginDiv">
		<form name="LoginForm" id="loginForm" method="post" action="<?php echo $requestedURL ?>" enctype="multipart/form-data">                     
			<input type="text" name="AdminLogin" id="loginField" class="loginField inputText" title="Identifiant" />
			<input type="password" name="AdminPassword" id="passwordField" class="passwordField inputText" title="Mot de passe" />
			
			<input type="submit" name="Password" class="submitField" value="OK" onClick="checkLogin(); return false;" />
			<div class="spacer"></div>
			
			<a href="#" onClick="switchPassword(); return false;">Changer mon mot de passe</a>
		</form>
		</div>
	</div>
	
	<!-- Ici la partie changement du mot de passe -->
	<div class="form" id="change_mdp" style="display:none;">
		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/logo.png" width="" />
		<div class="spacer"></div>
		
		<p></p>		
		
		<form name ="MdpForm" method="post" action="<?php echo $GLOBAL_START_URL ?>/admin.php" enctype="multipart/form-data">

    	    <input type="text" class="loginField inputText" title="Identifiant" name="Login" value="" />

    	    <input type="text" class="loginField inputText" title="Ancien mot de passe" name="Password" value="" />
			<div class="spacer"></div>
			
    	    <input type="text" class="loginField inputText" title="Nouveau mot de passe" name="newPassword" value="" />
			
    	    <input type="text" class="loginField inputText" title="Confirmation nouveau mot de passe" name="newPasswordConfirmation" value="" />

            <input type="button" class="submitField" name="changePassword" value="Valider" />
			<div class="spacer"></div>
			
			<a href="#" onClick="switchPassword(); return false;">Se connecter</a>
        </form>
	</div> <!-- Fin du bloc mdp -->
	
	
	
</div>

		<?php
		
	}

	//---------------------------------------------------------------------------------------

?>