<?php
include_once( dirname( __FILE__ ) . "/../../config/config.php" );
include_once( dirname( __FILE__ ) . "/../../script/global.fct.php" );

$GLOBAL_START_TIME = microtime_float();

//------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/objects/DBUtil.php" );
include_once( "$GLOBAL_START_PATH/objects/GraphicFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Menu.php" );
include_once( "$GLOBAL_START_PATH/objects/User.php" );
include_once( "$GLOBAL_START_PATH/objects/UserAccessControl.php" );

//------------------------------------------------------------------------------------------

//connexion
if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) )
	User::getInstance()->login( stripslashes( $_POST[ "Login" ] ), md5( stripslashes( $_POST[ "Password" ] ) ) );
	
//déconnexion
if( isset( $_REQUEST[ "logout" ] ) )
	User::getInstance()->logout();

//------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------
	// Vérifiaction du login 
	//---------------------------------------------------------------------------------------

	if( !User::getInstance()->getId() ){
			
		//GraphicFactory::table_start( "Administration Akilaé" );

		displayLogonPanel();
	
		//GraphicFactory::table_end();
		
		include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
		
		exit();
				
	}

	//---------------------------------------------------------------------------------------
	// Droits d'accès à la page demandée
	//---------------------------------------------------------------------------------------

	if( !UserAccessControl::allowAccess( User::getInstance()->getId(), Menu::getMenuIdentifier() ) ){
		
		//GraphicFactory::table_start( "Administration Akilaé" );
?>

		<p style="text-align:center;" class="alert">
			<?php echo Dictionnary::translate( "access_denied" ) ?>
		</p>

<?php
		if( isset( $banner ) && $banner = "no" ){
	
			//GraphicFactory::table_end();
				
		}
		
		exit();
			
	}

	$iduser = User::getInstance()->getId();
				
	//---------------------------------------------------------------------------------------			

	/**
	 * Formulaire pour le login
	 */
	function displayLogonPanel(){
		
		global	$GLOBAL_START_URL,
				$GLOBAL_START_PATH;
		
		$requestedURL = isset( $_REQUEST[ "logout" ] ) ? "$GLOBAL_START_URL/admin.php" : $GLOBAL_START_URL . $_SERVER[ "REQUEST_URI" ];
		
		if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) ){
			
?>
			<p style="text-align:center;" class=""><?php echo Dictionnary::translate( "identification_failed" ) ?></p>
<?php
	
		}
		
		?>
		
		
		
		
			<html xmlns="http://www.w3.org/1999/xhtml">
			    <head>
			    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			    <title>Akilaé - Solution e-CRM / ERP e-B2B/B2C</title>
			    <link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/home.css" />
			    </head>
			    <body>
			    	<div id="global">
			        	<div id="header">
			            	<div id="left">
			                	<div id="companyLogoContainer">
			                        <a href="http://www.akilae-saas.fr"> 
			                            <img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/logo.png" alt="vers akilae-saas.fr" />
			                        </a>
			                    </div>
			                </div>
			                <div id="middle">&nbsp;</div>
			                <div id="right">
			                    <div id="left">
			                    </div>
			                    <div id="right"></div>
			                    <div id="headerMenu">
			                    	<!--<div class="separator">|</div>
			                        <a href="#">Bienvenue Xoxoxxoxo</a>
			                        <div class="separator">|</div>
			                        <a href="#">Favoris</a>
			                        <div class="separator">|</div>
			                        <a href="#">Quitter</a>
			                        <div class="separator">|</div>-->
			                    </div>
			            	</div>
			            </div>
			            <div id="globalMainContent">
                        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
			            	<div class="mainContent login">
			                    <div class="loginBox">
			                    	<div class="header">
			                        	<div class="topRight"></div><div class="topLeft"></div>
			                            <p class="title">Connexion</p>
			                        </div>
			                        <div class="content">
			                        	<form method="post" action="<?php echo $requestedURL ?>" enctype="multipart/form-data">
			                                <p class="label">Identifiant</p>
			                                <div class="textField">
			                                    <div class="topLeft"></div><div class="topMiddle"></div><div class="topRight"></div>
			                                    <div class="textFieldContent clear">
			                                        <input type="text" style="width:99%;" name="Login" value="<?php if( isset( $_POST[ "Login" ] ) ) echo stripslashes( htmlentities( $_POST[ "Login" ] ) ); ?>" />
			                                    </div>
			                                    <div class="bottomLeft"></div><div class="bottomMiddle"></div><div class="bottomRight"></div>
			                                    <div class="clear"></div>
			                                </div>
			                                <p class="label">Mot de passe</p>
			                                <div class="textField">
			                                    <div class="topLeft"></div><div class="topMiddle"></div><div class="topRight"></div>
			                                    <div class="textFieldContent clear">
			                                        <input type="password" style="width:99%;" name="Password" value="" />
			                                    </div>
			                                    <div class="bottomLeft"></div><div class="bottomMiddle"></div><div class="bottomRight"></div>
			                                    <div class="clear"></div>
			                                </div>
			                                <div style="text-align:right; margin-right:-4px; padding-top:10px;">
			                                    <input type="image" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loginSubmit.gif" alt="Valider" />
			                                </div>
			                        	</form>
			                        </div>
			                        <div class="footer">
			                        	<div class="bottomLeft"></div>
			                            <div class="bottomMiddle"></div>
			                            <div class="bottomRight"></div>
			                        </div>
			                    </div>
			                </div>
			            </div>
		<?php
		
	}

	//---------------------------------------------------------------------------------------


?>