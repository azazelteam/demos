<?php
if( !isset( $GLOBAL_START_PATH ) )
	include_once( "../../config/init.php" );
include_once( "$GLOBAL_START_PATH/script/global.fct.php" );

if( isset( $_GET[ 'searchOrderFromRefClt' ] ) && isset( $_POST[ 'refClt' ] ) ){
	
	$idordr = DBUtil::query( "SELECT idorder FROM `order` WHERE n_order LIKE '%" . $_POST[ 'refClt' ] . "%' LIMIT 1" )->fields( 'idorder' );
	echo "$idordr";
	exit( ); 
}

include_once( "$GLOBAL_START_PATH/objects/Bookmark.php" );
include_once( "$GLOBAL_START_PATH/objects/Node.php" );

//------------------------------------------------------------------------------------------

//Script Ajax pour aller sur un produit du Front depuis le slider

if( isset( $_GET[ "sliderReference" ] ) ){
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "reference", $_GET[ "sliderReference" ] );
	echo URLFactory::getProductURL( $idproduct );
	
	exit();
	
}

$bookmark = new Bookmark( "Favoris" );
$bookmarkArray = $bookmark->getBookmark();

$node = new Node( $bookmarkArray, "bookmark" );
	
?>
<!-- Menu principal -->
<div id="mainMenu">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<?php
	//Récupération de l'environnement métier sélectionné
	foreach( $menuArray[ "children" ] as $index => $value ){
		
		if( $value[ "selected" ] == 1 ){
			
			$mainCategory = $index;
		
		}
	
	}

	if( isset( $mainCategory ) ){
		
?>
	<ul>
<?php
		
		//Construction du premier niveau de menu
		$i = 0;
		
		foreach( $menuArray[ "children" ][ $mainCategory ][ "children" ] as $firstMenu ){
			
			if( $firstMenu[ "selected" ] == 1 ){
				
				$subCategory = $i;
				$selected = " mainMenuItemSelected";
				
			}else{
				
				$selected = "";
				
			}
			$firstMenu[ "name" ] = $firstMenu[ "name" ];
			//Teste l'existence de sous-menus
			//if( count( $firstMenu[ "children" ] ) || $firstMenu[ "path" ] == "" )
			if($firstMenu[ "path" ] == "" )
				echo "				<li class=\"mainMenuItem$selected\"><a href=\"#\" onclick=\"selectTab( '$i' ); return false;\"><span>" . $firstMenu[ "name" ] . "</span></a></li>\n";
			else
				echo "				<li class=\"mainMenuItem$selected\"><a href=\"" . $firstMenu[ "path" ] . "\" ><span>" . $firstMenu[ "name" ] . "</span></a></li>\n";
			
			if( $i < count( $menuArray[ "children" ][ $mainCategory ][ "children" ] ) -1 )
				//echo "				<div class=\"separator\"></div>\n";
			
			$i++;
			
		}
		
?>
</ul>
<!-- Recherche produit -->
<?php 
	
	$mainCat = $menu->getMainCategory();
	
	if( UserAccessControl::allowAccess( User::getInstance()->get( "iduser" ), 200 ) && $mainCat[ 0 ] == 200 )
		displayProductManagementSearch(); 
	
?>
</div>
<div class="clear"></div>
		
<div id="menu">
	
	<!-- ********************* 	Favoris *************************** -->
	
	<div id="FavoriteMenu">
		<?php echo $node->getHtml(); ?>
	</div>
	<!-- <div style="background-color:#7A8EA3; float:right; height:4px; margin:5px 0 0 0; width:4px;"></div> -->
	
	
	<script type='text/javascript'>
	<!--
	$(document).ready(function(){
		$( function() {
			$( '#bookmarkMenu' ).droppy();
		});
	});
	
	function selectTab( index ){
		
		$(".subMenu").hide();
		$("#AdminMenuPanel_"+index).show();
		
	}
	
	-->
	</script>
	
	
	<!-- ********************* 	Sous Menu *************************** -->
	
	
	
	<?php
		
	//Construction de tous les sous-menus
	//Le sous-menu sélectionné est affiché
	//Parcours de chacun des menus principaux
	$i = 0;
	//var_dump($menuArray[ "children" ][ $mainCategory ][ "children" ]);exit();
		foreach( $menuArray[ "children" ][ $mainCategory ][ "children" ] as $subMenu ){
			
			if( count( $subMenu[ "children" ] ) ){
				
				$lastMenu = false;
				
				if( isset( $subCategory ) && $i == $subCategory )
					echo "			<ul id=\"AdminMenuPanel_$i\" class=\"subMenu mainBackMenu\" style=\"display:block;\">\n";
				else
					echo "			<ul id=\"AdminMenuPanel_$i\" class=\"subMenu mainBackMenu\" style=\"display:none;\">\n";
				
				//Parcours de chaque seconde ligne de sous-menu
				$j = 0;
				
				foreach( $subMenu[ "children" ] as $key => $value ){
					$value[ "name" ] = $value[ "name" ];
					if( $value[ "selected" ] == 1 )
						$selected = " class=\"selectedSubMenuItem subMenuLien\"";
					else
						$selected = " class=\"subMenuLien\"";
					
					if( $value[ "path" ] == "" )
						echo "				<li class='subMenuli'><a href=\"#\"$selected><span>" . $value[ "name" ] . "</span></a>";
					else
						echo "				<li class='subMenuli'><a href=\"" . $value[ "path" ] . "\"$selected><span>" . $value[ "name" ] . "</span></a>";
					
					//S'il y a un dernier niveau de sous-menu dans un menu déroulant
					if( count( $value[ "children" ] ) ){
						
						$lastMenu = true;
						
						echo "\n					<ul>\n";
						
						foreach( $value[ "children" ] as $child ){
							$child[ "name" ] = $child[ "name" ];
							if( $child[ "path" ] == "" )
								echo "						<li><a href=\"#\">" . $child[ "name" ] . "</a></li>\n";
							else
								echo "						<li><a href=\"" . $child[ "path" ] . "\">" . $child[ "name" ] . "</a></li>\n";
							
						}
						
						echo "					</ul>\n				";
						
					}
					
					echo "</li>\n";
					
					if( $j < count( $menuArray[ "children" ][ $mainCategory ][ "children" ][ $i ][ "children" ] ) -1 )
						//echo "				<li class=\"separator\">|</li>\n";
					
					$j++;
					
				}
					
				echo "			</ul>\n";
				
				//S'il y a un dernier niveau de menu, inclusion du script de sous-menu
				if( $lastMenu ){
					
	?>
		<script type='text/javascript'>
		<!--
			$(function() {
				$('#AdminMenuPanel_<?php echo $i ?>').droppy();
			});
		-->
		</script>
		
	<?php
					
				}
				
			}
			
			$i++;
			
		}
		
?>
	<div id="Calculator"></div>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function calculator(){
		
			$.ajax({
					 	
				url: "<?php echo $GLOBAL_START_URL; ?>/accounting/calculator.php",
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){},
			 	success: function( responseText ){
			
					$("#Calculator").html( responseText );
					$("#Calculator").dialog({
	        
						modal: false,	
						title: "Calculatrice",
						position: ['right','top'],
						width:320,
						height:350,
						zIndex:2000,
						close: function(event, ui) { 
						
							$("#Calculator").dialog( 'destroy' );
							
						}
					
					});
					
					$( "#calc_resultat" ).focus();
							
				}
			
			});
			
		}
	
	/* ]]> */
	</script>
	
	
	<div style="float:right;" class="elt-calculator">
		<a href="#" onclick="calculator(); return false;" style="vertical-align:top;">
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/layout/calculator.png" alt="Calculatrice" style="margin-top:-2px;" />
		</a>
	</div>
	
	<div style="float:right;margin-right:10px;" class="elt-process">
		<a href="<?php echo $GLOBAL_START_URL; ?>/sales_force/process.php">Process</a>
	</div>
	<?php if(strpos($_SERVER['REQUEST_URI'], "recalculate.php" ) === false)  {?>
    	<!-- ********************* Recherche ************************** 
    	<form action="" id="form-search" onsubmit="return false;">
    		<div id="SearchMenu">
    			<input type="text" name="searchBar" id="searchBar" value="Recherche sur le site" style="color:#8B9298; width:200px;" onfocus="if( this.value == 'Recherche sur le site' ) this.value = ''; menuInitAutoComplete(document.getElementById('form-search'),this,document.getElementById('bouton-submit'));" />
    		</div>
    	</form>-->
<?php
	   }	
	}

	unset( $key );

?>
</div>
<div class="clear"></div>
<script type="text/javascript">
<!--
	
	$(document).ready(function(){
	
		$(".sliderButton").click(function(){
			if($(this).is(".active"))
				$("#sliderPanel").animate({"left": "-=197px"}, "normal");
			else
				$("#sliderPanel").animate({"left": "+=197px"}, "normal");
			$(this).toggleClass("active"); return false;
		});
		
		$(".mainMenuItem").click(function(){
			$(".mainMenuItem").each(function(){
				$(this).removeClass("mainMenuItemSelected");
			});
			$(this).addClass("mainMenuItemSelected");
		});
		
	});
	
	function goShortcut( form ){
		
		if( document.getElementById( "customerSearch" ).value != "" ){
			
			window.open( "<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=" + document.getElementById( "customerSearch" ).value );
			return false;
			
		}
	
		if( document.getElementById( "estimateSearch" ).value != "" ){
			
			window.open( "<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=" + document.getElementById( "estimateSearch" ).value );
			return false;
			
		}
		
		if( document.getElementById( "orderSearch" ).value != "" ){
			
			window.open( "<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=" + document.getElementById( "orderSearch" ).value );
			return false;
			
		}

		if( document.getElementById( "orderSearchClt" ).value != "" ){
		
			refClt = $( "#orderSearchClt" ).val();
			var data = "refClt=" + refClt;

			var ajaxIdOrder = "";
			
			$.ajax({
			 	
				url: "<?php echo URLFactory::getHostURL(); ?>/templates/back_office/menu.php?searchOrderFromRefClt",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible trouver la commande" ); },
			 	success: function( responseText ){

					ajaxIdOrder = responseText;

					if( ajaxIdOrder == '' ){
						alert( "Commande Introuvable !" );
						return false;		
					}
					
					window.open( "<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=" + ajaxIdOrder );
					
				}

			});

			return false;
			
		}		
		
		if( document.getElementById( "supplierOrderSearch" ).value != "" ){
			
			window.location = "<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=" + document.getElementById( "supplierOrderSearch" ).value;
			return false;
			
		}

		if( document.getElementById( "deliveryNoteSearch" ).value != "" ){
			
			window.location = "/sales_force/supplier_order.php?idbl_delivery=" + document.getElementById( "deliveryNoteSearch" ).value;
			return false;
			
		}

		if( document.getElementById( "invoiceSearch" ).value != "" ){
			
			window.location = "<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=" + document.getElementById( "invoiceSearch" ).value;
			return false;
			
		}
		
		if( document.getElementById( "creditSearch" ).value != "" ){
			
			window.location = "<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=" + document.getElementById( "creditSearch" ).value;
			return false;
			
		}
		
		if( document.getElementById( "productSearch" ).value != "" ){
			
			$.ajax({
				url: "/templates/back_office/menu.php?sliderReference=" + document.getElementById( "productSearch" ).value,
				cache: false,
				success: function( url ){
					window.open( url );
				}
			});
			
			$(".sliderButton").toggleClass("active");
			
			$("#sliderPanel").animate({"left": "-=197px"}, "normal");
			
			return false;
			
		}
		
		return false;
		
	}
	
-->
</script>
<div style="position:fixed; left:0;">
	<form action="#" onsubmit="return goShortcut(this);">
		<div id="sliderPanel" class="panel<?php echo B2B_STRATEGY ? "B2B" : "B2C" ?>">
			<table>
				<tr>
					<th>Client n°</th>
					<td><input type="text" class="sliderSearchInput" id="customerSearch" /></td>
				</tr>
				<tr>
					<th>Devis n°</th>
					<td><input type="text" class="sliderSearchInput" id="estimateSearch" /></td>
				</tr>
				<tr>
					<th>Commande n°</th>
					<td><input type="text" class="sliderSearchInput" id="orderSearch" /></td>
				</tr>
				<tr>
					<th>Ref. Client</th>
					<td><input type="text" class="sliderSearchInput" id="orderSearchClt" /></td>
				</tr>				
				<tr>
					<th>Commande fourn. n°</th>
					<td><input type="text" class="sliderSearchInput" id="supplierOrderSearch" /></td>
				</tr>
				<tr>
					<th>BL/OE. n°</th>
					<td><input type="text" class="sliderSearchInput" id="deliveryNoteSearch" /></td>
				</tr>
				<tr>
					<th>Facture n°</th>
					<td><input type="text" class="sliderSearchInput" id="invoiceSearch" /></td>
				</tr>
				<tr>
					<th>Référence prod. n°</th>
					<td><input type="text" class="sliderSearchInput" id="productSearch" /></td>
				</tr>
			</table>
			<input type="submit" value="Aller" class="blueButton" style="float:right; margin-right:4px; margin-top:5px;" />
			<p class="slider" style=""><a href="#" class="sliderButton">
			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAzCAYAAABxCePHAAAAAXNSR0IArs4c6QAAAAZiS0dEAP8A/wD/oL2nkwAAAAlwSFlzAAALEwAACxMBAJqcGAAAAAd0SU1FB9kGEAweKiBcShUAAAUgSURBVEjHtVZNiCVnFT2nqut7b15nunsmRuMko2L+jBEhC1FxE8FsZIJEUGchYhCyENEQFSFI8C/EETdudCOzcKcbRU3Ij4tBmUTF/Gj8CxOYQYwzGXqSzEy/N9+9t6qOi6nXVL+u7iSIFx48qm6d7957zr3fBTqLiDV3P+buFhFPRMSdkkanT5+Gmc3dkHOGJGyziNj8P5vNbjSzH7v7RTN7wd3vcfeDZjbKOXPu1wfetOl0ugi85u7fdPeX3V1m9kcze7+7l4uHFwBQ1zWWl5fRO2WPpFWSfwXwnKQzAK4G8HNJX3T3NQCoqmprak3TMCL2mdkHzOwbZvZndz9pZj+YzWbXd/W4y8zOuPuDEbFnqC6TiHjQzJ5399+b2b3uftWin7t/1MzC3W9tmgYAsNR7T0nnAXy9qqpfkIz+x03TsCgKkXzY3X8FYG/TNASgPnVVzvkWSUV3Yr9Gn3T3u+u63t8BHpBUbaM657xqZj919+9LSgCwvr5OM/uMu5909yMRsbKY3hagnPOSmR129xPufqRpmjeZ2ZfN7J9m9i13X+10hEHLOfdD/4S7/8Xd/2BmT5rZ52ez2WgOMKjWIXP3z5qZ3P0hSeOd1L3JiLt/BQDn+ZFsAFwh6eMkbwbwhKSnSD5O8ndVVW3LZwnAxzp6QXIebkvyoqQZgNtI3ixp2rbt0wBmQ5FcOZQnSUkqASSStaSZpOl4PG7xv1hEbCFhp4K+2d1/EhH3957dZ2ZHzezAToXtn3KFux9192fqur6pB/IRM/uPu38vIiavFcU1Zqac86fn42E+eNz9S2b2kJndNPRt0ZOvSLZFUewDgLIsQW4OslcBlABGQyD9Lg4AzwDIHTt9v7cBmEq6uCsIyRmA4wDuiojn2rY9D0AkDwK4HcBvAZwdAuHCzLi6rusjJN8n6dxlbK5JOkbygZTS6dlshslksh3E3ZFS6hf5vZLexcs5PZtSen4+4VNKi6kCZnbDGxHc0FVBd3+sqqpD7r5E8h4ABwE0C35J0lOSfjYej18ZKuztAELSUtcn+wHUC34jABVJ7RTeYQA4d+4cptNpioi1iFhd+K11h+DEiROvP89dlL0zxWa2F8B3SV4HoOkUPAGwV1Im+RsAP0opnd1NbAHgaUnnSTYkQ9ItJD/cDatHm6Z5dVexRQQkJUllpwMBKNu2vZLkUZJ/K8vyvrIsp4ORuDuqqgIAHzho6u6PS/pgRLwVwAuDIB3AjkZyHUACMN6xAbuci7Ztr63r+hqSbX88SDokaYPkK7t2cUTsk/QYgBsl1b05swTgSZJfSCm9OBipmWE0Gm1+cOnSpZXeTV+UZTkdjUYZADY2NjCZTFAUxTA7OeclksvdVkCSLcmcUspDgut3PSUhIt4t6TYA7wGwtxubRvJFAMclHQewQnIPyX9VVbWFRUbEHZK+Lakh+XcAF7oIlyQd7Lr6YQC3kvw3gK9tU62ZnTWz7+wyPw5365WZ2aFTp04NOl0YWjG6d/vN7IfuLnd/1MzeMdSwBclivnuMx+PFLegqAOsAHukYWwawyWb/3rkIYBttnZ0cjUb3A/i1pAAweJkXklZzzu9cjAIAxuPxnIUPddfFS4OKJfkIgF+6+wMA/kHyjKQGwErbtm8n+amOma+mlF4eVGxEHJD0OUl3kDwAoJTU8vKCsgHgTwCOkjyWUqr7Ct+i2IjY07btW0hOSK50aWZJFwCcr+t6fXl5WYtKfcP2mssNALRt+7qe/d/sv71ErPzo5E+1AAAAAElFTkSuQmCC" alt="" style="margin-left:3px; margin-top:14px;" /> 
			</a></p>
		</div>
	</form>
</div>
<?php

//-------------------------------------------------------------------------------------------------------------------------

function displayProductManagementSearch(){
	
	global 	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
		        beforeSubmit:  productManagementSearchPreSubmitCallback,  // pre-submit callback 
		        success:       productManagementSearchPostSubmitCallback  // post-submit callback 
  
		    };
    
    		$('#ProductManagementSearchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function productManagementSearchPreSubmitCallback( formData, jqForm, options ){
	
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function productManagementSearchPostSubmitCallback( responseText, statusText ){ 
		
			if( !responseText.length ){
			
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '10px';
		    	$.growlUI( '', 'La recherche n\'a donné aucun résultat' );
    	
				return;
				
			}
			
			$( "#globalMainContent" ).html( responseText );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function editSelectedCatalogElement( element ){

			if( element.info.substr( 0, 11 ) == 'produit n° ' ) //recherche par nom de produit
				document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=' + element.info.substr( 11 );
			else if( element.info == 'référence catalogue' ) //recherche par référence catalogue
				document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?reference=' + element.value;
			else if( element.info == 'référence fournisseur' ) //recherche par référence fournisseur
				document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/detail.php?ref_supplier=' + element.value;
			else if( element.info.substr( 0, 13 ) == 'catégorie n° ' ) //recherche par nom de catégorie
				document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_category.php?action=edit&idcategory=' + element.info.substr( 13 );
			else document.location = '<?php echo $GLOBAL_START_URL; ?>/product_management/catalog/cat_product.php?idproduct=' + element.value; //recherche par n° de produit
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<style type="text/css">

		#as_ProductManagementSearchInput ul{ width:400px;}
		#as_ProductManagementSearchInput .as_header,
		#as_ProductManagementSearchInput .as_footer{ width:388px;}
		
	</style>
	<div id="ProductManagementSearch">
		<form id="ProductManagementSearchForm" enctype="multipart/form-data" method="post" action="<?php echo $GLOBAL_START_URL ?>/product_management/search/search.php">
			<input id="ProductManagementSearchInput" type="text" name="search" value="Recherche dans le catalogue" onfocus="if( this.value == 'Recherche dans le catalogue' ) this.value = '';" onblur="if( this.value == '' ) this.value = 'Recherche dans le catalogue';" style="color:#8B9298; width:126px;height: 17px;" />
			<?php
			
				include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
		
				AutoCompletor::completeFromFile( "ProductManagementSearchInput", "$GLOBAL_START_URL/product_management/search/autocomplete.php", 10, "editSelectedCatalogElement" );
		
			?>
			<select name="type" style="color:#8B9298;">
				<option value="">tout</option>
				<option value="categories">catégories</option>
				<option value="products">produits</option>
				<option value="references">références</option>
			</select>
			<input type="submit" class="searchButton" value="&nbsp;" style="font-size:10px; padding:1px; width:30px;" />
		</form>
	</div>
	<?php

}

//-------------------------------------------------------------------------------------------------------------------------

?>