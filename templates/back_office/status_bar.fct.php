<?php

/**
 * Fonction retourant le seuil de rentabilité
 * 
 * @author Nicolas Gerner
 * @param date $year L'année actuel
 * @param date $mont Le mois actuel
 * @param date $day Le jour actuel
 * 
 * @return decimal Fonction retournant le seuil de rentabilité
 */

function getSeuilRentabilite( $year, $month, $day ){
	
	$db =& DBUtil::getConnection();
	
	$todayDay = date('d');
	$todayMonth = date('m');
	$nbjours = 0;
	if( $month == '' && $day == '' ){

		$month = 1;
		$select = '';
		
		while( $month <= $todayMonth ){
			if( $month == $todayMonth ){
				$today = $todayDay;
			}else{
				$today = intval( date( 't' , mktime( 0, 0, 0, $month, 1, $year ) ) );
			}
			
			$day = 1;
								
			while( $day <= $today ){
				if( strlen($select) > 0 )
					$select .= ' + ';
					
				$select .= " (SELECT amount
							FROM threshold_rentability
							WHERE date_threshold <= '$year-$month-$day'
							ORDER BY date_threshold DESC
							LIMIT 1) ";
							
				$day++;$nbjours++;
			}
			
			$month++;
		}
		
		$query = "
		SELECT ( $select ) as amount
		LIMIT 1";
		
	}else if( $month != '' && $day == '' ){
		// faut générer les OR pour le mois
		$day = 1;
		$select = '';
		
		while( $day <= $todayDay ){
			if( strlen($select) > 0 )
				$select .= ' + ';
			
			$select .= " (SELECT amount
						FROM threshold_rentability
						WHERE date_threshold <= '$year-$month-$day'
						ORDER BY date_threshold DESC
						LIMIT 1) ";
			
			$day++;
			
		}
		
		$query = "
		SELECT ( $select ) as amount
		LIMIT 1";
	}else{	
		
		$query = "
		SELECT amount
		FROM threshold_rentability
		WHERE date_threshold <= '$year-$month-$day'
		ORDER BY date_threshold DESC
		LIMIT 1";
		
	}
	
	$rs = $db->Execute( $query );
	
	if( !$rs->RecordCount() )
		return 0.0;
		
	return $rs->fields( "amount") ;
	
}

/**
 * Fonction retourant le nombre de devis à traiter
 * 
 * @author Nicolas Gerner
 * @return int Fonction retournant le nombre de devis à traiter
 */
 
function getNbrEstimate(){
	
	$query = 'SELECT count(`status`) as nbrDevis FROM `estimate` WHERE `status` = "ToDo" AND `idbuyer` <> 0';
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return 0;
	
	return $rs->fields( "nbrDevis") ;
	
}

/**
 * Retourne le nombre de commandes à traiter
 * 
 * @author Nicolas Gerner
 * @return int Fonction retournant le nombre de commandes à traiter
 */

function getNbrOrder(){
	
	$query ='SELECT count(`status`) as nbrOrder FROM `order` WHERE `status` = "ToDo" AND `idbuyer` <> 0';
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return 0;
	
	return $rs->fields( "nbrOrder") ;
}

/**
 * Retourne la marge Nette pour un jour donné
 * 
 * @author 
 * @param date $year L'année actuel
 * @param date $mont Le mois actuel
 * @param date $day Le jour actuel
 * @param string $type la valeur souhaité de retour : CA / Marge Net
 * @return decimal Fonction retournant la marge Nette pour un jour donné
 */

function getStatsValue( $year, $month = '' , $day = '' , $type ){
	
	global	$GLOBAL_START_PATH,
			$netMarginSum,
			$breakEventPointSum,
			$estimate_total_amount_ht_sum,
			$order_total_amount_ht_sum,
			$estimateCountSum,
			$orderCountSum;
	
	include_once( "$GLOBAL_START_PATH/objects/Order.php" );
	include_once( "$GLOBAL_START_PATH/objects/Estimate.php" );
	
	$stat_breakeventpoint = getSeuilRentabilite( $year, $month, $day );
	
	setlocale( LC_TIME, "fr_FR.utf8" );
	
	$timestamp = mktime( 0, 0, 0, $month, $day, $year );
	
	$strDayOfTheWeek = strftime( "%A", $timestamp );
	$intDayOfTheWeek = date( "w", $timestamp );
	
	$like = "$year-%";
	
	if( $month != '' ){
		$like = substr( $like , 0 , strlen( $like )-1 )."$month-%";
		
		if( $day != '' ){
			$like = substr( $like , 0 , strlen( $like )-1 )."$day%";
		}
	}
			
	//devis
	$query = "
	SELECT e.idestimate,
	e.total_amount_ht
	FROM estimate e
	WHERE e.DateHeure LIKE '$like' 
	AND e.status NOT IN( '".Estimate::$STATUS_TODO."' , '".Estimate::$STATUS_CANCELLED."' )
	AND e.use_estimate = 1
	";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les devis du $day-$month-$year $query" );

	$estimateCount = $rs->RecordCount();
	$estimateCountSum += $estimateCount;
	
	$estimate_total_amount_ht = 0.0;
	while( !$rs->EOF() ){
		$estimate_total_amount_ht += $rs->fields( "total_amount_ht" );
		$rs->MoveNext();
	}
	
	//commandes
	$query = "
	SELECT COUNT(idorder ) AS orderCount,
	SUM(total_amount_ht) AS total_amount_ht,
	SUM(net_margin_amount) AS net_margin_amount
	FROM `order` 
	WHERE conf_order_date LIKE '$like'
	AND `status` = '".Order::$STATUS_ORDERED."'";
	
	$rs =& DBUtil::query( $query );
	
	$orderCount = $rs->fields( "orderCount" );
	$orderCountSum += $orderCount;
	$order_total_amount_ht_daily_sum = 0.0;
	
	$current_day_order_total_amount_ht = $rs->fields( "total_amount_ht" );
	
	switch ($type){
		case "netMargin":
			return $rs->fields( "net_margin_amount" );
		case "CA":
			return $rs->fields( "total_amount_ht" );
	}
	
	
}


/**
 * Retourne la marge nette nette pour le mois en cours
 * @return decimal
 */

function getCurrentMonthNetMargin($day = 1 , $today = "" , $year="" , $month="" ){
	
	if($today=="" && $year=="" && $month==""){
		$today = date("d");
		$year = date( "Y" );
		$month = date( "m" );
	}

	$margeNetCumul = 0.0;
		
	$margeNet = getStatsValue( $year, $month, '', "netMargin" );

	return $margeNet;
}

/**
 * Retourne le seuil de rentabilité pour le mois en cours
 * @return decimal
 */

function getCurrentMonthSeuil($day = 1 , $today = "" , $year="" , $month=""){
	
	if($today=="" && $year=="" && $month==""){
		$today = date("d");
		$year = date( "Y" );
		$month = date( "m" );
	}
	
	$seuilJour = getSeuilRentabilite( $year, $month, '' );

	return $seuilJour;	
	
}

/**
 * Retourne la marge nette nette pour le mois en cours
 *  
 * @author Gerner Nicolas 
 * @return decimal Fonction retournant la marge nette nette pour un mois donné
 */

function getNetMarginMonthSum(){
	
	return getCurrentMonthNetMargin() - getCurrentMonthSeuil();
	
}

/**
 * Retourne la marge nette nette pour l'année en cours
 *  
 * @author SS attention a tes fess
 * @return decimal Fonction retournant la marge nette nette l'année en cours
 */

function getNetMarginYearSum(){
	$mymonth = date('m');
	$year	 = date('Y');
	$day	 = 1;
	
	$margeNet = getStatsValue( $year, '', '', "netMargin" );

	return $margeNet - getSeuilRentabilite( $year , '' , '' );
}

/**
 * Retourne le pourcentage de progression de la rentabilité du mois
 * @return int
 */

function getMonthProgression(){
	
	/*@todo sivouplé monsieur sinisa :o3= */return 0;
	
	$currentMonthNetMargin = getCurrentMonthNetMargin();
	$currentMonthSeuil = getCurrentMonthSeuil();
	
	if( $currentMonthSeuil == 0 )
		return 0;
	
	return floor( 100 - ( $currentMonthSeuil - $currentMonthNetMargin ) / $currentMonthSeuil * 100 );
	
}

/**
 * Chiffre d'affaire HT du jour
 */

function getCaDay(){
	
	$theDate = date( "Y-m-d" );
	$query = 'SELECT SUM(total_amount_ht) as ca_day_ht FROM `order` WHERE DateHeure like "'.$theDate.'%" and status = "Ordered"';
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->fields('ca_day_ht') == null){
		return 0 ;
	}
		
	return $rs->fields('ca_day_ht');

}

/**
 * Nbr visiteurs
 */

function getNbConnectedBuyers(){
		/*
		$nbConnectes = 0;
		$DateHeure = date( "Ymd" ) . date( "H" ) . date( "i" )-5 . date( "s" );
		$table = "stat" . date( "Ym" );

		//Session::getInstance()->TableStatCreate( $table );
		
		$query = "SELECT idsession FROM $table WHERE DateHeure >= $DateHeure GROUP BY idsession";

		$rs = DBUtil::query( $query );
		$nbConnectes = $rs->RecordCount();
		*/
		return 'NC' ;
		
}

/**
 * Retourne la classe CSS à utiliser pour la coloration des stats
 */

function getCssColor( $myInt ){
	
	if( $myInt <= 0 )
		$cssColor = "grasBackRed";
	else
		$cssColor = "grasBackGreen";
	
	return $cssColor ;
	
}

/**
 * création du fichier txt avec les valeurs
 */
 
function createParamFile(){
	global $GLOBAL_START_PATH;
	
 	$day = date( "d" );
	$year = date( "Y" );
	$month = date( "m" );
 
 	$array_valeurs[ 'seuilRentatbilite' ]	= getSeuilRentabilite( $year, $month, $day );
	$array_valeurs[ 'netMargin' ]			= getStatsValue( $year, $month, $day, "netMargin" );
	$array_valeurs[ 'nbrEstimate' ]			= getNbrEstimate();
	$array_valeurs[ 'nbrOrder' ]			= getNbrOrder();
	$array_valeurs[ 'resultatCumul' ]		= getNetMarginMonthSum();
	$array_valeurs[ 'resultatCumulAn' ]		= 0;
	$array_valeurs[ 'caDay' ]				= getStatsValue( $year, $month, $day, "CA" );
	$array_valeurs[ 'resultat' ]			= $array_valeurs[ 'netMargin' ] - $array_valeurs[ 'seuilRentatbilite' ];
	$array_valeurs[ 'nbConnectes' ] 		= getNbConnectedBuyers();
	//$array_valeurs[ 'resultCAn' ]			= getNetMarginYearSum();

	if( $compt = fopen( $GLOBAL_START_PATH.'/templates/back_office/status_bar_values.txt' , 'w+' ) ){
		foreach( $array_valeurs as $key => $valeur ){
			$val_a_ecrire = $array_valeurs[ $key ];
			fputs( $compt , $val_a_ecrire . "\r\n" );
		}
		fclose($compt);
	}

 }
?>