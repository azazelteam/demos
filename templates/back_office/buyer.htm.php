<?php
/**
 * Gestion commerciale
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des Acheteurs
 */

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

?>
<script type="text/javascript">
<!--
function checkPassword() {
	password = document.getElementById('password').value;
	password_confirm = document.getElementById('password_confirm').value;
	if( password == password_confirm) {
		return true;
	}
	else {
		alert('<?php echo Dictionnary::translate('password_failed') ?>');
		return false;
	}
}
// -->
</script>
<p style="text-align:right;">
	<a href="<?php echo $GLOBAL_START_URL ?>/administration/enterprise/buyer.php">
	<?php GraphicFactory::button( "btn_new_record", "", "style=\"border-style:none;\"" ); ?>
	</a>
</p>
		<h4><?php echo Dictionnary::translate('title_buyer_plus1') ; ?></h4>
		<table class="table" width="950px" cellspacing="1">
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('idbuyer') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><?php echo $tableobj->get('idbuyer'); ?><input type='hidden' name="idbuyer" value="<?php echo $tableobj->get('idbuyer'); ?>" readonly></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('company') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('idcompany','modify'); ?></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('activity') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('activity','modify'); ?><a href="javascript:popupwindow('activity')" >modifier</a></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('adress') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><input type="text" name="adress" value="<?php echo $tableobj->get('adress'); ?>" size=50></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('adress_2') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><input type="text" name="adress_2" value="<?php echo $tableobj->get('adress_2'); ?>" size=50></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('zipcode') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="zipcode" value="<?php echo $tableobj->get('zipcode'); ?>" size=5></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('city') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="city" value="<?php echo $tableobj->get('city'); ?>" size=10></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('state') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><?php $tableobj->SelectField('idstate','modify'); ?><a href="javascript:popupwindow('state')" >modifier</a></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('siren') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="siren" value="<?php echo $tableobj->get('siren'); ?>" size="6"></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('naf') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('naf','modify'); ?><a href="javascript:popupwindow('naf')" >modifier</a></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('tva') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><input type="text" name="tva" value="<?php echo $tableobj->get('tva'); ?>" size=15></td>
			</tr>
		</table>
		
		<h4><?php echo Dictionnary::translate('title_buyer_plus2') ; ?></h4>
		<table class="table" width="100%" cellspacing="1">
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('lastname') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="lastname" value="<?php echo $tableobj->get('lastname'); ?>" size=15></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('firstname') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="firstname" value="<?php echo $tableobj->get('firstname'); ?>" size=10></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('department') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('iddepartment','modify'); ?><a href="javascript:popupwindow('iddepartment')" >modifier</a></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('function') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('idfunction','modify'); ?><a href="javascript:popupwindow('idfunction')" >modifier</a></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('email') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="email" value="<?php echo $tableobj->get('email'); ?>" size=30></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('phonenumber') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="phonenumber" value="<?php echo $tableobj->get('phonenumber'); ?>" size=10></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('login') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="login" value="<?php echo $tableobj->get('login'); ?>" size="6"></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('faxnumber') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="faxnumber" value="<?php echo $tableobj->get('faxnumber'); ?>" size=10></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('password') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="password" name="password" id="password" value="<?php echo $tableobj->get('password'); ?>" size="6"></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('password_confirm') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="password" name="password_confirm" id="password_confirm" value="<?php echo $tableobj->get('password'); ?>" size="6"></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;" colspan="2"><?php echo Dictionnary::translate('accept_condition') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="2"><input type="accept_condition" name="accept_condition" id="accept_condition" value="<?php echo $tableobj->get('accept_condition'); ?>" size="1"></td>
			</tr>	
		</table>
		
		<h4><?php echo Dictionnary::translate('register_add') ; ?></h4>
		<table class="table" width="100%" cellspacing="1">
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('commercial') ; ?></td>
			    <td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('iduser','modify'); ?><a href="javascript:popupwindow('commercial')" >modifier</a></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('modif_deliv') ; ?></td>
				<td class="cel_result" style="text-align:left;"><select name="modif_deliv">
		<?php
			    if($tableobj->get('modif_deliv')) {
					echo "\t\t\t\t".'<option value="0">0'."\n";
		     		echo "\t\t\t\t".'<option selected="selected" value="1">1'."\n";
		     	} else {
					echo "\t\t\t\t".'<option selected="selected" value="0">0'."\n";
		     		echo "\t\t\t\t".'<option value="1">1'."\n";
		     	}
		?>
					</select>
					<?php toolTipBox( Dictionnary::translate('help_modif_deliv'), 250, $html = "", $showicon = true ) ?>
				</td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('idrate_family_b') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('idrate_family','modify'); ?><a href="javascript:popupwindow('rate_family')" >modifier</a></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('buyerfamily') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php $tableobj->SelectField('idbuyer_family','modify',false,'idbuyer_family'); ?><a href="javascript:popupwindow('buyer_family')" >modifier</a></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('newsletter') ; ?></td>
				<td class="cel_result" style="text-align:left;">
					<select name="newsletter">
		<?php
			    if($tableobj->get('newsletter')) {
					echo "\t\t\t\t".'<option value="0">0'."\n";
		     		echo "\t\t\t\t".'<option selected="selected" value="1">1'."\n";
		     	} else {
					echo "\t\t\t\t".'<option selected="selected" value="0">0'."\n";
		     		echo "\t\t\t\t".'<option value="1">1'."\n";
		     	}
		?>
					</select>
				</td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('lastorder') ; ?></td>
				<td class="cel_result" style="text-align:left;"><?php echo $tableobj->get('lastorder'); ?>
			    <input type='hidden' size='10' name="lastorder" value="<?php echo $tableobj->get('lastorder'); ?>" readonly></td>
			</tr>
			<?php if (DBUtil::getParameterAdmin('display_catalog_right')) { ?>
			<tr>
			<td class="cel_titre" style="text-align:right;" colspan="3"><?php echo Dictionnary::translate('buyer_catalog_right') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="catalog_right" name="catalog_right" id="catalog_right" value="<?php echo $tableobj->get('catalog_right'); ?>" size="2"></td>
			</tr>	
			<?php	} ?>	
		</table>

<?php if (DBUtil::getParameterAdmin('display_buyer_accept')) { ?>		
		<h4><?php echo Dictionnary::translate('admin_accept') ; ?></h4>
		<table class="table" width="100%" cellspacing="1">
			<tr>
			 	<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('admin_right') ; ?></td>
				<td class="cel_result" style="text-align:left;">
					<select name="admin_right">
		<?php
			    if($tableobj->get('admin_right')) {
					echo "\t\t\t\t".'<option value="0">0'."\n";
		     		echo "\t\t\t\t".'<option selected="selected" value="1">1'."\n";
		     	} else {
					echo "\t\t\t\t".'<option selected="selected" value="0">0'."\n";
		     		echo "\t\t\t\t".'<option value="1">1'."\n";
		     	}
		?>
					</select>
				</td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('admin') ; ?><a href="javascript:search()" class=aide> ? </a></td>
				<td class="cel_result" style="text-align:left;"><input size='1' type="text" name="budget" value="<?php echo $tableobj->get('admin'); ?>" size="6"></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;" colspan="3"><?php echo Dictionnary::translate('accept') ; ?></td>
				<td class="cel_result" style="text-align:left;">
					<select name="accept">
		<?php
		     	if($tableobj->get('accept')) {
					echo "\t\t\t\t".'<option value="0">0'."\n";
		     		echo "\t\t\t\t".'<option selected="selected" value="1">1'."\n";
		     	} else {
					echo "\t\t\t\t".'<option selected="selected" value="0">0'."\n";
		     		echo "\t\t\t\t".'<option value="1">1'."\n";
		     	}
		?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('budget') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input size='1' type="text" name="budget" value="<?php echo $tableobj->get('budget'); ?>" size="6"></td>
			  	<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('period') ; ?></td>
				<td class="cel_result" style="text-align:left;">
					<select name="periodt">
		<?php
		     	if($tableobj->get('period')) {
					echo "\t\t\t\t".'<option value="0">0'."\n";
		     		echo "\t\t\t\t".'<option selected="selected" value="1">1'."\n";
		     	} else {
					echo "\t\t\t\t".'<option selected="selected" value="0">0'."\n";
		     		echo "\t\t\t\t".'<option value="1">1'."\n";
		     	}
		?>
					</select>
				</td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('period_date') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><INPUT type="text" size='10' name="period_date" id="period_date" value="<?php echo $tableobj->get('period_date'); ?>" READONLY >
				<?php DHTMLCalendar::calendar( "period_date" ); ?>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('buyingamount') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="buyingamount" value="<?php echo $tableobj->get('buyingamount'); ?>" size="6"></td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('maxbuyingamount') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input type="text" name="maxbuyingamount" value="<?php echo $tableobj->get('maxbuyingamount'); ?>" size="6"></td>
			</tr>
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('max_order') ; ?></td>
				<td class="cel_result" style="text-align:left;" colspan="3"><input size='1' type="text" name="max_order" value="<?php echo $tableobj->get('max_order'); ?>" size="6"></td>
			</tr>
		</table>
<?php } ?>

<?php if (DBUtil::getParameterAdmin('display_buyer_pay')) { ?>		
		<h4><?php echo Dictionnary::translate('admin_payment') ; ?></h4>
		<table class="table" width="100%" cellspacing="1">
			<tr>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('deliv_payment') ; ?></td>
				<td class="cel_result" style="text-align:left;"><input size="1" type="text" name="deliv_payment" value="<?php echo $tableobj->get('deliv_payment'); ?>" size="8">
					<?php toolTipBox( Dictionnary::translate('deliv_payment'), 250, $html = "", $showicon = true ) ?>
				</td>
				<td class="cel_titre" style="text-align:right;"><?php echo Dictionnary::translate('cond_payment') ; ?></td>
				<td class="cel_result" style="text-align:left;">
					<input size="1" type="text" name="cond_payment" value="<?php echo $tableobj->get('cond_payment'); ?>" size="1">
					<?php toolTipBox( Dictionnary::translate('help_cond_payment'), 250, $html = "", $showicon = true ) ?>
				</td>
			</tr>
		</table>
<?php } ?>
