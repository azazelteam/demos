<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Akilaé - Solution e-CRM / ERP e-B2B/B2C</title>
    <link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/home.css" />
    </head>
    <body>
    	<div id="global">
        	<div id="header">
            	<div id="left">
                	<div id="companyLogoContainer">
                        <a href="http://www.akilae-saas.fr"> 
                            <img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/logo.png" alt="vers akilae-saas.fr" />
                        </a>
                    </div>
                </div>
                <div id="middle">&nbsp;</div>
                <div id="right">
                    <div id="left">
                    </div>
                    <div id="right"></div>
                    <div id="headerMenu">
                    	<div class="separator">|</div>
                        <a href="#">Bienvenue Xoxoxxoxo</a>
                        <div class="separator">|</div>
                        <a href="#">Favoris</a>
                        <div class="separator">|</div>
                        <a href="<?php echo $GLOBAL_START_URL ?>/admin.php?logout=ok">Quitter</a>
                        <div class="separator">|</div>
                    </div>
            	</div>
            </div>
            <div id="globalMainContent">
            	<div class="mainContent">
                    <h1>Bienvenue <?php echo User::getInstance()->getValue( "firstname" ) ?>sfsdfsfs</h1>
                    <p class="subTitle">    
                        Sit inim vel dolore conulla accum iusto deliquam zzriust incidunt lumsan hendre 
                        magna feu faccumm oleniatie diatet, veliquat. Pat wisisi lumsan hendre magna ex 
                        exer adionse.
                    </p>
                    <div id="menu">
                        <div class="leftColumn">
                            <a href="<?php echo $GLOBAL_START_URL ?>/sales_force/" class="item">
                                <p class="space">espace</p>
                                <p class="type">commercial</p>
                            </a>
                             <a href="<?php echo $GLOBAL_START_URL ?>/marketing/" class="item">
                                <p class="space">espace</p>
                                <p class="type">marketing</p>
                            </a>
                             <a href="<?php echo $GLOBAL_START_URL ?>/logistics/" class="item">
                                <p class="space">espace</p>
                                <p class="type">logistique</p>
                            </a>
                        </div>
                        <div class="rightColumn">
                             <a href="<?php echo $GLOBAL_START_URL ?>/product_management/" class="item">
                                <p class="space">espace</p>
                                <p class="type">chef de produit</p>
                            </a>
                             <a href="<?php echo $GLOBAL_START_URL ?>/accounting/" class="item">
                                <p class="space">espace</p>
                                <p class="type">comptabilité</p>
                            </a>
                            <a href="<?php echo $GLOBAL_START_URL ?>/administration/" class="item">
                                <p class="space">espace</p>
                                <p class="type">administration</p>
                            </a>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

?>