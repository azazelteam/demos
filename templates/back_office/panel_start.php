<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<table border="0" cellspacing="0" cellpadding="0" class="Panel">
	<tr>
		<td class="TopLeftCorner"></td>
		<td class="FrameBorderTop"></td>
		<td class="TopRightCorner"></td>
		<td class="Empty" style="width:10px;"></td>
	</tr>
	<tr>
		<td class="FrameBorderLeft"></td>
		<td class="PanelTitleCell">
			<p class="PanelTitle" style="background-color:<?php echo $TabColor ?>">
				<?php 
				
				if( isset( $TabTitle ) ){
					
					$chars = 	array( "é","è","ç","à","ù","ç","â","ê","û","î","ô","ä","ë","ÿ","ü","ï","ö" );
					$replace = 	array( "e","e","c","a","u","c","a","e","u","i","o","a","e","y","u","i","o" );
					
					$i = 0;
					while( $i < count( $chars ) ){
						
		
						$TabTitle = str_replace( $chars[ $i ], $replace[ $i ], $TabTitle );
						
						$i++;
						
					}
					
					echo htmlentities( strtoupper( $TabTitle ) );
					
				}
					
				?>
			</p>
		</td>
		<td class="FrameBorderRight"></td>
		<td class="FrameBorderBottom2" style="width:10px;"></td>
	</tr>
	<tr>
		<td class="FrameBorderLeft"></td>
		<td class="PanelContent" colspan="2">