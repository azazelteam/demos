<html>
<body>
{if:iduser}<span style="color:red;font-weight: bold;">/contact_mail.tpl</span>{end:}
	<p><img src="{logo}" alt="logo" /></p>
	<table class="tableMail">
		<tr>
			<td>De :</td>
			<td>{recipientName}</td>
		</tr>
		<tr>
			<td>Envoyé :</td>
			<td>{strdate:h}</td>
		</tr>
		<tr>
			<td>A :</td>
			<td> {gendre}{buyer}</td>
		</tr>
		<tr>
			<td>Object :</td>
			<td>Votre demande d'informations</td>
		</tr>
	</table>
	<p class="pConfirm">{gendre} {buyer},</p>
	<p class="pConfirm">Nous avons bien reçu votre message et vous remercions de l'interêt que vous portez à nos produits et à notre société.</p>
	<p class="aBientot">A très bientôt.</p>
	
	<p id="footer">
		{recipientName}<br />
		{if:phonenumber}
		Tél. {phonenumber}<br />
		{end:}
		{if:faxnumber}
		Fax. {faxnumber}<br />
		{end:}
		<a href="mailto:{recipient}">{recipient}</a>
	</p>
</body>
</html>