	<flexy:include src="/head/header.tpl"></flexy:include>
	<link type="text/css" rel="stylesheet" href="{baseURL}/css/catalog/error404.css" />
    <style type="text/css">
		#contents{max-height: 438px !important;min-height: inherit !important;overflow: hidden !important;}
		#footer {z-index:555;}
	</style>
</head>
<body>
<flexy:include src="menu/menu_alt.tpl"></flexy:include>
	<div id="content">
	
	{if:iduser}<span style="color:red;font-weight: bold;">/404.tpl</span>{end:}
	
    <div id="center" class="page">
<div class="ombrage">
<div class="corps">
		<!-- début div centre -->
		<h1 class="Title"><span class="big">ooOOOps....</span> Désolé, la page que vous tentez de visiter n'existe plus.</h1>
		<p class="Paragraphe"><strong>Poursuivez votre recherche en utilisant les liens suivants: </strong></p>

		<ul class="uLink" type="disc">
			<li class="iconsHome"><a href="{baseURL}" class="CategoryName">Page d'accueil</a></li>
			<li class="iconsSitemap"><a href="{baseURL}/catalog/plan.php" class="CategoryName">Plan du site</a></li>
			<li class="iconsContact"><a href="{baseURL}/catalog/contact.php" class="CategoryName">Contact</a></li>
			<li class="iconsSearch"><a href="{baseURL}/catalog/search.php" class="CategoryName">Recherche avancée</a></li>
		</ul>
		<div class="spacer"></div><br/><br/><br/><br/>
		<!--<flexy:include src="encart_pub.tpl"></flexy:include>-->
	</div>
</div> 
</div> 
</div>  
</div> 	
</div> 
<flexy:include src="foot.tpl"></flexy:include>