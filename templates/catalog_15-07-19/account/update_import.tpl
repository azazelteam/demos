{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
		<link rel="stylesheet" type="text/css" href="{baseURL}/templates/catalog/css/form_histo_groupement.css" />
		<link rel="stylesheet" type="text/css" href="{baseURL}/templates/catalog/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
	<!--</head>
	<body>-->
	<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				.dataTable{
					width:75% !important
				}
				
				td {
					border-bottom: 1px, solid, #EEEEEE !important;
					padding: 4px, 8px !importan;
				}
				
				.croix_rouge{
					color: red;
					font-size: 2.6em;
				}
				
			/* ]]> */
			</style>
			

	
	<!-- Header -->
	<!--<flexy:include src="head/banner.tpl"></flexy:include>-->

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/update_import.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>			
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
<h2>Historique import fichiers commandes</h2>
<br><br>

		<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th style="border-bottom: 1px, solid, #EEEEEE; width: 15%;">N° import</td>
				<th style="border-bottom: 1px, solid, #EEEEEE; width: 15%;">Date</th>
				<th style="border-bottom: 1px, solid, #EEEEEE; width: 40%;">Fichier</th>
				<th style="border-bottom: 1px, solid, #EEEEEE; width: 15%;">Liste Commandes</th>
				<th style="border-bottom: 1px, solid, #EEEEEE; width: 15%;">Nbre de lignes</th>
			</tr>
			{foreach:export,dataImport}
				<tr>
					<td style=" text-align:center;">{dataImport[0]}</td>
					<td style="border-bottom: 1px, solid, #EEEEEE; margin: 4px, 8px;">{dataImport[1]}</td>
					<td style="border-bottom: 1px, solid, #EEEEEE; margin: 4px, 8px;"><a href="{baseURL}{dataImport[2]}">{dataImport[4]}</a></td>
					<td style="text-align:center;"><a href="{baseURL}/catalog/order_logistics.php?file_selected={dataImport[0]}&all"><span class="croix_rouge">+</span></a></td>
					<td style="text-align:center;">{dataImport[3]}</td>
				</tr>
			{end:}					
		</table>
	</div>
	{form:h}
<br><br>
	{result:h}
	
			
	</div>
	<div class="spacer"></div>
	</div>

<flexy:include src="foot.tpl"></flexy:include>	

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}
