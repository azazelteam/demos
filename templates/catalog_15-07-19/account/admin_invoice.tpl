{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
	<!--</head>
	<body>-->
	<!-- Header -->
	<!--<flexy:include src="head/banner.tpl"></flexy:include>-->

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/admin_invoice.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">


	<h2>Vos factures</h2>
		{if:NoResults}
			<p class="blocTextNormal">Aucune facture trouvée</p>
		{else:}

			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th>n°</th>
				<th>Date facture</th>
				<!--<th>Echéance</th>-->
				<th>Montant TTC</th>
				<th>Statut</th>
				<th>PDF</th>
				<!-- <th style="background:#F9F9F9;">Détail</th> -->
			</tr>
			{foreach:array_invoice,line}
				<tr>
					<td><a href="{baseURL}/catalog/show_invoice.php?IdInvoice={line[idinvoice]}">{line[idinvoice]}</a></td>
					<td><a href="{baseURL}/catalog/show_invoice.php?IdInvoice={line[idinvoice]}">{line[DateHeure]:h}</a></td>
					<!--<td class="tdPrice"><a href="{baseURL}/catalog/show_invoice.php?IdInvoice={line[idinvoice]}">{line[echeance]:h}</a></td>-->
					<td class="tdPrice"><a href="{baseURL}/catalog/show_invoice.php?IdInvoice={line[idinvoice]}">{line[total_amount_ht]:h}</a></td>
					<td><a href="{baseURL}/catalog/show_invoice.php?IdInvoice={line[idinvoice]}">{line[status]:h}</a></td>

					<td>
						<a href="{baseURL}/catalog/pdf_invoice.php?idinvoice={line[idinvoice]}" target="_blank">
						<img src="{baseURL}/images/back_office/content/pdf_icon.gif" /></a>
					</td>					
					<!-- <td><a href="{baseURL}/catalog/show_invoice.php?IdInvoice={line[idinvoice]}">
					<img src="{baseURL}/images/back_office/content/iconsNextBack.png" style="vertical-align:middle;" /></a></td> -->
				</tr>
			{end:}
			</table>
		{end:}
	</div>
	<div class="spacer"></div>
	</div>
</div>
	
</div>
</div>

<flexy:include src="foot.tpl"></flexy:include>

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}
