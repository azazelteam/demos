{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
		<link rel="stylesheet" type="text/css" href="{baseURL}/templates/catalog/css/form_histo_groupement.css" />
		<link rel="stylesheet" type="text/css" href="{baseURL}/templates/catalog/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
	<!--</head>
	<body>-->
	<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				.dataTable{
					width:75% !important
				}
				
				td {
					border-bottom: 1px, solid, #EEEEEE !important;
					padding: 4px, 8px !importan;
				}
				
			/* ]]> */
			</style>
			

	
	<!-- Header -->
	<!--<flexy:include src="head/banner.tpl"></flexy:include>-->

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/stock_level.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>			
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
				<h2>Etat du stock</h2>
				<br><br>
				{form:h}
				<!-- <form  style="float: none!important;"action="/catalog/stock_level.php?all" method="post" id="SearchPop"> -->
					<!-- <div  style="float: right;width:65%;"> -->
						<!-- <div class="tableContainer"> -->
							<!-- <table class="dataTable" width="100%"> -->
								<!-- <tr> -->
									<!-- <td style="vertical-align: middle;"><span style="font-size: 12px;">Entre le</td> -->
									<!-- <td> <input type="text" name="date_order" id="start_date" class="calendarInput" value="" /> -->
										<!-- <a href="javascript:calendar(start_date)" id="test_a" ></a> -->
						
									<!-- </td>	 -->
									<!-- <td style="vertical-align: middle;"><span style="font-size: 12px;">Et le</td> -->
									<!-- <td> <input type="text" name="date_ordersup" id="end_date" class="calendarInput" value="" /> -->
										<!-- <a href="javascript:calendar(end_date)" id="test_b" ></a> -->
									<!-- </td> -->
							<!-- </table> -->
						<!-- </div> -->
						<!-- <div class="submitButtonContainer" style=""> -->
							<!-- <input type="submit" class="blueButton" value="Rechercher" style="color:black;" /> -->
						<!-- </div> -->
					<!-- </div> -->
				<!-- </form> -->
				
				
				
				

					<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
						<tr>
							<th style=" width: 20%;">Référence</td>
							<th style=" width: 20%;">Nom</th>
							<th style=" width: 20%;">Nom de la catégorie</th>
							<th style=" width: 20%;">Stock réel</th>
							<th style=" width: 20%;">Seuil minimum</th>
						</tr>
						{foreach:export,dataImport}
							<tr onmouseover="document.getElementById('pop{dataImport[5]}').className='tooltiphover';" onmouseout="document.getElementById('pop{dataImport[5]}').className='tooltip';">
								<td style=" text-align:center;" class="stock">{dataImport[6]:h}</td>
								<td style="">{dataImport[1]}</td>
								<td style="text-align:center;">{dataImport[2]}</td>
								<td style="text-align:center;">{dataImport[3]}</td>
								<td style="text-align:center;">{dataImport[4]}</td>
							</tr>
						{end:}					
					</table>
				
	</div>
<br><br>
	{result:h}
	
			
	</div>
	<div class="spacer"></div>
	</div>

<flexy:include src="foot.tpl"></flexy:include>	

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}

