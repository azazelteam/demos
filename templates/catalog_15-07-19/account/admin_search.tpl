{if:NoResults}
<p class="blocTextNormal">Pas de résultat</p>
{else:}

{if:iduser}<span style="color:red;font-weight: bold;">/account/admin_search.tpl</span>{end:}

	{if:adm_estim}
		<p class="blocTextNormal">Cliquez sur le numéro du devis pour accèder à son détail</p>
	{else:}
		<p class="blocTextNormal">Cliquez sur le numéro de la commande pour accèder à son détail</p>
	{end:}


	<table class="accountTable" cellspacing="0" cellpadding="0" border="0">
		
		
	{if:adm_estim}
			
			<tr>
			<th class="center">N° Devis</th>
			<th class="center">Date commande</th>
			<th class="center">Validité</th>
			<th class="center">Etat</th>
			<th class="center">Montant HT</th>
			</tr>
	
	{else:}
			
			<tr>
			<th class="center">N° commande</td>
			<th class="center">Date commande</th>
			<th class="center">Etat</th>
			<th class="center">Montant HT</th>
			<th class="center">Commande</th>
			<th class="center">Règlement</th>
			<th class="center">Bon de livraison</th>
			<th class="center"></th>
			</tr>			

	{end:}

		
	
	{if:adm_estim}
		{foreach:array_estimate,line}
		<tr>
			<td class="center"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}" class="Lien_Search_Devis_Table">{line[idestimate]}</a></td>
			<td class="center">{line[DateHeure]:h}</td>
			<td class="center">{line[valid_until]:h}</td>
			<td class="center">{line[status]:h}</td>
			<td class="center">{line[total_amount_ht]:h}</td>
		</tr>
		{end:}
	{else:}
		{foreach:array_order,line}
		<tr>
			<td class="center"><a href="{baseURL}/catalog/show_order.php?idorder={line[idorder]}" class="Lien_Search_Devis_Table">{line[idorder]}</a></td>
			<td class="center">{line[DateHeure]:h}</td>
			<td class="center">{line[status]:h}</td>
			<td class="center">{line[total_amount_ht]:h}&nbsp;</td>
			<td class="center"><a href="{baseURL}/pdf/pdf_infos.php?type=order&id={line[idorder]}" class="Lien_Search_Devis_Table">{line[idorder]}</a></td>
			<td class="center">
				{foreach:line[invoices],lineinvoice}
				<a href="{baseURL}/pdf/pdf_infos.php?type=invoice&id={lineinvoice}" class="Lien_Search_Devis_Table">{lineinvoice}</a>
				{end:}
			</td>
			<td class="center">
				{foreach:line[bls],linebl}
					<a href="{baseURL}/pdf/pdf_infos.php?type=delivery_order&id={linebl}" class="Lien_Search_Devis_Table">{linebl}</a>
				{end:}
			</td>
			<td>
				<a href="/catalog/finish.php?idorder={line[idorder]}" flexy:if="line[allow_debit_card_payment]">
					<img src="/images/logo_CB.jpg" alt="Paiement Direct" />
				</a>
			</td>
		</tr>
		{end:}
	{end:}
	
	</table>
	<div class="spacer"></div>
	<br/><br/><br/><br/>
{end:}