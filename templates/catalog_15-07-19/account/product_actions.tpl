
<a href="#" onclick="window.open('{baseURL}/catalog/mail_product.php?idproduct={product.getId()}','mailto','width=555,height=750,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=0,resizable=1');" style="cursor:pointer;" rel="nofollow"><img src="{start_url}/templates/catalog/images/btn_callback.png" style="margin:0;border:none;"></a>   

<!-- Télécharger la fiche technique -->
<script type="text/javascript" src="{baseURL}/js/jquery/jquery.form.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
	
	/* --------------------------------------------------------------------------------------------- */
	
	function downloadPDF( idproduct ){

		$( "#PDFDownloadForm" ).ajaxSubmit( {
		
			url : "/catalog/pdf_download.php?idproduct=" + idproduct,
			success: function( responseText , statusText ){
	
				if( responseText == "1" ){
				
					
					$( "#PDFDownloadForm" ).css( "display", "none" );
					$( "#PDFconfirm" ).css( "display", "block" );
					$( "#PDFbuttonConfirm" ).css( "display", "none" );
					$( "#PDFbuttonClose" ).css( "display", "block" );
					$( "#PDFText" ).css( "display", "none" );
					
					
					//$( '#PDFDownloadDialog' ).dialog( 'destroy' );
					
				}
	       		else alert( responseText );

			}
		    
		});
		
	}
	
	/* --------------------------------------------------------------------------------------------- */
	
	function checkPDFDownloadForm(){
	
		var required = false;

		$( "#PDFDownloadForm" ).find( ".required" ).each( function( i ){
	
			if( !$( this ).val() ){
				
				$( "#PDFDownloadRequiredField" ).css( "display", "block" );
				required = true;

			}
				
		});
		
		if( required )
			return false;

		$( "#PDFDownloadRequiredField" ).css( "display", "none" );
	
		return true;

	}
	
	/* --------------------------------------------------------------------------------------------- */
	
	function PDFDialog(){
	
		$( "#PDFDownloadDialog" ).dialog({
		
			title:"Téléchargez notre fiche technique",
			bgiframe: true,
			modal: true,
			width:500,
			overlay: {
			
				backgroundColor: '#000000',
				opacity: 0.5
				
			},
			close: function(event, ui) { $( "#PDFDownloadDialog" ).dialog( "destroy" ); }
			
		});
		
	}

	/* --------------------------------------------------------------------------------------------- */
	
/* ]]> */
</script>
<div style="display:none;" id="PDFDownloadDialog">
{if:iduser}<span style="color:red;font-weight: bold;">/account/product_actions.tpl</span>{end:}
	<div class="borderConfirmDevis">
		<img src="{baseURL}/images/front_office/global/topPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />
		<div class="bgContentDevis">
			<div class="contentConfirmPDF">
				<div class="spacer"></div>
				<br />
				
				<a style="position:absolute; margin:-17px 0 0 453px;" href="#" onclick="$( '#PDFDownloadDialog' ).dialog( 'destroy' ); return false;">
				<img src="{baseURL}/images/front_office/global/close-popup.png" width="25" height="23" /></a>
				
				
				<div class="panierConfirmDevis">
					<p id="PDFText" style="font-size:13px; color:#505050; text-align:center; margin:5px 5px 5px 5px; line-height:16px; width:340px;"><strong>Pour recevoir par mail notre fiche technique,<br/> veuillez renseigner le formulaire ci-dessous.</strong></p>
					<p id="PDFDownloadRequiredField" style="font-size:12px; text-align:center; color:#FF0000; display:none; width:340px; margin:5px 0;">Tous les champs obligatoires n'ont pas été renseignés</p>
					<p id="PDFconfirm" style="font-size:12px; text-align:center; color:#505050; display:none; width:340px; margin-top:50px;"><strong>Notre fiche technique vous a été envoyé avec succès.</strong></p>
					<style type="text/css">
						form#PDFDownloadForm label{ width:12%; margin:5px 10px 2px 5px; text-align:left; float:left; color:#505050; font-size:12px;  }
						form#PDFDownloadForm input, form#PDFDownloadForm select{ background-color:#FFFFFF; margin:2px 0 2px 0; padding:1px 0; border:1px solid #A2A2A2; float:left; font-size:12px; color:#505050; }
					</style>
					<form method="post" action="{baseURL}/catalog/pdf_download.php?idproduct={product.getId()}" id="PDFDownloadForm" enctype="multipart/form-data" style="margin-top:20px">
					
						<label><span style="color:#FF0000;">*</span> Civilité</label>
						<input type="radio" name="gender" value="1" class="required" /> <span style="font-size:12px; color:#505050; width:70px; float:left; margin:2px 0 0 2px;">Mr.</span>
						<input type="radio" name="gender" value="2" class="required" /> <span style="font-size:12px; color:#505050; width:70px; float:left; margin:2px 0 0 2px;">Mme.</span>
						<input type="radio" name="gender" value="3" class="required" /> <span style="font-size:12px; color:#505050; width:70px; float:left; margin:2px 0 0 2px;">Mlle.</span>
						<div class="spacer"></div>
						
						<label><span style="color:#FF0000;">*</span> Nom</label>
						<input tpye="text" id="lastname" name="lastname" value="" class="required" style="width:100px;" />
						
						<label><span style="color:#FF0000;">*</span> Prénom</label>
						<input tpye="text" id="firstname" name="firstname" value="" class="required" style="width:100px;" />
						<div class="spacer"></div>
												
						<label><span style="color:#FF0000;">*</span> Email</label>
						<input tpye="text" id="email" name="email" value="" class="required" style="width:275px;" />
						<div class="spacer"></div>
						
						<p style="text-align:center; width:340px;"><span style="color:#FF0000; font-size:10px;">* Champs obligatoires</span></p>
					</form>
					<div class="spacer"></div>
					<div id="PDFbuttonConfirm" style="text-align:center; width:420px; height:55px; margin:14px 0 0 0;">
					<a href="#" onclick="if( checkPDFDownloadForm() ) downloadPDF({product.getId()}); return false;" style="display:block;">
					<img src="{baseURL}/images/front_office/global/btn-technique.jpg" width="227" height="33" /></a>
					</div>
					<div class="spacer"></div>
					<a href="#" id="PDFbuttonClose" style="display:none;" onclick="$( '#PDFDownloadDialog' ).dialog( 'destroy' ); return false;" class="cancel_pdf2">Terminer</a>
				</div>
			</div>
		</div>
		<img src="{baseURL}/images/front_office/global/bottomPopUpDevis.png" alt="" width="499" height="10" class="floatleft" />
		<div class="spacer"></div>
	</div>
</div>
