{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
		<link rel="stylesheet" type="text/css" href="{baseURL}/templates/catalog/css/form_histo_groupement.css" />
		<link rel="stylesheet" type="text/css" href="{baseURL}/templates/catalog/css/jquery/ui-lightness/jquery-ui-1.7.1.custom.css" />
	</head>
	<body>
	<!-- Header -->

	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/groupement_history.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
			
			 			
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">


	<h2>Historique Groupement</h2>
	
	<br>
		
		{form:h}
		
		<br><br><br>
		
		{graph:h}
		
			{arrayrecap:h}
			
	
	
	
		<br><br><br>
	</div>
	<div class="spacer"></div>
	</div>
</div>	


<flexy:include src="foot.tpl"></flexy:include>

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}
