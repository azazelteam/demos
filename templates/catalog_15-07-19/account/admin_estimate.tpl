{if:isLogin}
		<flexy:include src="head/header.tpl"></flexy:include>
	<!--</head>
	<body>
	<flexy:include src="head/banner.tpl"></flexy:include>-->


	<div id="content" class="account">
	{if:iduser}<span style="color:red;font-weight: bold;">/account/admin_estimate.tpl</span>{end:}
		<div id="leftbloc">
				<flexy:include src="menu/menu_compte.tpl"></flexy:include>
		</div>
		
		<div id="categorybloc" class="account">
			<div class="intro">
				<h2>Vos devis</h2>
									
{if:NoResults}
			<p class="blocTextNormal">Aucun résultat</p>
		{else:}
				
			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">			
			<tr>
				<th>n°</th>
				<th>Date</th>
				<th>Montant TTC</th>
				<th>Délai de validité</th>
				<th>Statut</th>
				<th>PDF</th>
				<th style="background:#F9F9F9;">Détail</th>
			</tr>
			{foreach:array_estimate,line}
				{if:line[color]}
					<tr>
						<td class="inProgress"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[idestimate]}</a></td>
						<td class="inProgress"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[DateHeure]:h}</a></td>
						<td class="inProgress">-</td>
						<td class="inProgress"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[valid_until]:h}</a></td>
						<td class="inProgress"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[status]:h}</a></td>
						<td class="inProgress">-</td>
						<td class="inProgress">-</td>
					</tr>
				{else:}
					<tr>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[idestimate]}</a></td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[DateHeure]:h}</a></td>
						<td class="tdPrice"><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[total_amount_ht]:h}</a></td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[valid_until]:h}</a></td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">{line[status]:h}</a></td>
						<td>
							<a href="{baseURL}/catalog/pdf_estimate.php?idestimate={line[idestimate]}" target="_blank">
							<img src="/images/back_office/content/pdf_icon.gif" /></a>
						</td>
						<td><a href="{baseURL}/catalog/show_estimate.php?IdEstimate={line[idestimate]}">
						<img src="/images/back_office/content/iconsNextBack.png" style="vertical-align:middle;" /></a></td>
					</tr>
				{end:}				
			{end:}		
			</table>
		{end:}

				<div class="spacer"></div>
			</div>
			<div class="spacer"></div>
		</div>
		<div class="spacer"></div>
		
	</div>
	</div>
	</div>
	<flexy:include src="foot.tpl"></flexy:include>

{else:}
	<flexy:include src="account.tpl"></flexy:include>
{end:}

