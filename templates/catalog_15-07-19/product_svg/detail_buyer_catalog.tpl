{if:iduser}<span style="color:red;font-weight: bold;">/product/detail_buyer_catalog.tpl</span>{end:}
<FLEXY:TOJAVASCRIPT globalstarturl="start_url">
<script type="text/javascript">

	// Fonction qui gère le fondu
	function setOpacity( value ) {
		document.getElementById("page_action").style.opacity = value / 10;
		document.getElementById("page_action").style.filter = 'alpha(opacity=' + value * 10 + ')';
	}
	
	function fadeInMyPopup(pos,idprod) {
		for( var i = 0 ; i <= 100 ; i++ ){
			setTimeout( 'setOpacity(' + (i / 10) + ')' , 8 * i );
		}
		envoieRequete(pos,idprod)
	}
	
	function fadeOutMyPopup() {
		for( var i = 0 ; i <= 100 ; i++ ) {
			setTimeout( 'setOpacity(' + (10 - i / 10) + ')' , 8 * i );
		}
		setTimeout('closeMyPopup()', 800 );
	}
	
	// Fermeture du popup
	function closeMyPopup() {
		document.getElementById("page_action").style.display = "none"
	}
	
	// Lancement du popup
	function fireMyPopup(pos,idprod) {
		setOpacity( 0 );
		document.getElementById("page_action").style.display = "block";
		fadeInMyPopup(pos,idprod);
	}
	
	// Renvoi du contenu executé sur la page dans popup
	function envoieRequete(id,idproduit)
	{
		var xhr_object = null;
		var url=globalstarturl+'/catalog/product_one.php';
		var positionId = id;
		
		var params = "IdProduct="+idproduit+"&charte=chartep_popup.tpl";
		
		if(window.XMLHttpRequest){
			xhr_object = new XMLHttpRequest();
		}else{
		    if (window.ActiveXObject){
		    	xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		    }
		}
		
		// On ouvre la requete vers la page désirée
		xhr_object.open("GET", url+"?"+params, true);
		document.getElementById(positionId).innerHTML = " <h1>Chargement en cours...</h1> "; 
		xhr_object.onreadystatechange = function(){
			
			if ( xhr_object.readyState == 4 && xhr_object.status == 200)
			{
				// j'affiche dans la DIV spécifiée le contenu retourné par le fichier
				document.getElementById(positionId).innerHTML = xhr_object.responseText;
			}
		}
		// dans le cas du get
		xhr_object.send(params);
	}
</script>

<table class="tableProds">
	<tr>
		<td class="checkPromo" width="120" style="text-align: center;">
			
			<a href="{line[productURL]}"><div class="contenerImgChartep" style="text-align: center; z-index:984;"><img class="imgChartep" style="z-index:985;" width="100" border="0" name="image" src="{line[thumbURL]}" alt="{line[productName]:h}" onclick="document.location='{line[productURL]}'"/></div></a>	
			{if:line[haspromos]}
				<!--<br />-->
				<div class="affImgPromo" style="z-index:990;">
					<a href="{line[productURL]}"style="z-index: 995;"><img style="z-index:999;" border="0" src="{start_url}/templates/catalog/images/promo-bt.png" alt="Produit en promotion" /></a>
				</div>
			{end:}
			
		</td>
		</tr>
		<tr>
		<td style="vertical-align: top; text-align: center;">
			<h2 class="productTittle"><a href="{line[productURL]}">{line[name]}</a></h2>
			<p class="ref13">
				<span class="nbrefs">{line[nbref]}</span>&nbsp;<img src="{start_url}/www/ref/references.png" style="vertical-align:bottom;margin-bottom:1px;" alt="" />
			</p>
			{if:line[txtcom]}
			
				<p style="margin:5px; color: #878787;">{line[txtcom]:h}</p>

			{end:}
			<!-- Produit star -->
			{if:line[idlabel]}
				<img src="{line[labelImage]}" alt="{line[label]}" />
			{end:}
			{if:line[pxmin]}
				
				<p class="px13" style="margin-bottom: 0px; bottom: 0px;">
					<img style="margin-left: 22px;"class="px13" src="{start_url}/www/ref/a_partir_de.png" align="middle" alt="" /> 
					<br />
					<img style="margin-top: -30px;" src="{start_url}/templates/catalog/images/fleche_orange.png"> &nbsp;{line[pxmin]:h}
				</p>
			
			{end:}
			<p><a href="javascript:fireMyPopup('position','{line[idproduct]}');">Détail</a></p>
			<!--<p class="voir13">
				<img src="{start_url}/www/ref/see_products_off.jpg"  onmouseover="this.src='{start_url}/www/ref/see_products_on.jpg'" alt="" onmouseout="this.src='{start_url}/www/ref/see_products_off.jpg'" border="0" onclick="document.location='{line[productURL]:h}'" style="cursor:pointer" />
			</p>-->
		</td>
	</tr>
</table>

<!-- Div du popup -->
<div id="page_action" class="panel" style="position:absolute; top:278px; left:230px; width:528px; height:318px; display:none; z-index:1000;">
	<table width='380' cellpadding='0' cellspacing='0' border='0'>
		<tr>
			<td width="80%"></td>
			<td align="right"><a href='javascript:fadeOutMyPopup();'>Fermer</a></td>
		</tr>
		<tr>
			<td colspan='2' style='border:1px solid;top left; width: 380px; height:277px;' id="position">
				<h1>Chargement en cours...</h1>
			</td>
		</tr>
	</table>
</div>
