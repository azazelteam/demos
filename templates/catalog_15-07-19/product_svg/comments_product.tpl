{if:iduser}<span style="color:red;font-weight: bold;">/product/comments_product.tpl</span>{end:}
<!-- Commentaires produit --> 
  <script type="text/javascript"> 
               //Rating stars function
	function getRating(id,rating)
	{
		var name=document.getElementById(id).className;
		var rate=0;
		if(name=="star_off")
		{
			for(i=1;i<=rating;i++)
			{
				document.getElementById("etoile_"+i).className="star_on";
			}
			rate = rating;
		}
		else if(name=="star_on") {
			document.getElementById(id).className="star_off";
			for(i=1;i<=rating-1;i++)
			{
				document.getElementById("etoile_"+i).className="star_on";
			}
			for(i=rating;i<=5;i++)
			{
				document.getElementById("etoile_"+i).className="star_off";
			}
			rate = rating -1;
		}
		document.getElementById("rating").value= rate;
	}
      </script>      
	<script type="text/javascript"> 
	function envoyer()
	{
	var Chaine = document.getElementById("comment").value;
	var maj_chaine=Chaine.toUpperCase();	
	var Sous_Chaine1 = 'UNION';
	var Sous_Chaine2 = 'UNION SELECT';
	var Sous_Chaine3 = 'SELECT *';
	var Sous_Chaine4 = 'SELECT DISTINCT';
	var Sous_Chaine5 = 'PASSWORD';
	var Sous_Chaine6 = 'INSERT INTO';
	var Sous_Chaine7 = 'UPDATE';
	var Sous_Chaine8 = 'DELETE';
	var Sous_Chaine9 = 'TRUNCATE';
	var Sous_Chaine10 = 'SELECT';

	var Resultat1 = maj_chaine.indexOf(Sous_Chaine1);
	var Resultat2 = maj_chaine.indexOf(Sous_Chaine2);
	var Resultat3 = maj_chaine.indexOf(Sous_Chaine3);
	var Resultat4 = maj_chaine.indexOf(Sous_Chaine4);
	var Resultat5 = maj_chaine.indexOf(Sous_Chaine5);
	var Resultat6 = maj_chaine.indexOf(Sous_Chaine6);
	var Resultat7 = maj_chaine.indexOf(Sous_Chaine7);
	var Resultat8 = maj_chaine.indexOf(Sous_Chaine8);
	var Resultat9 = maj_chaine.indexOf(Sous_Chaine9);
	var Resultat10 = maj_chaine.indexOf(Sous_Chaine10);
	alert(maj_chaine);
	if ( (Resultat1 != -1) || (Resultat2 != -1) || (Resultat3 != -1) || (Resultat4 != -1) || (Resultat5 != -1) || (Resultat6 != -1) || (Resultat7 != -1) || (Resultat8 != -1) || (Resultat9 != -1) || (Resultat10 != -1) )
		{
			alert("Votre commentaire n'est pas valide.");
			document.getElementById("comment").value = "";	
			exit();
		}
		else
		{
			document.forms["commentsForm"].submit();
		}	
	}
      </script>
		<form name="commentsForm" id="commentsForm" action="{actionCommentsForm}" method="post"  onsubmit="javascript: return validform_comments();">
		<input id="rating" type="hidden" value="0"name="rating" >
 {if:!customer}
 <p><em>Veuillez vous identifier afin de poster un commentaire.</em></p>
	<p><strong>  Nom :</strong></p>
	<input type="text" name="name" id="name" value="">
	<p><strong>  Prenom :</strong></p>
<input type="text" name="sur_name" id="sur_name" value="">
<p><strong>  Email :</strong></p>
<input type="email" name="user_mail" id="user_mail" value="">
{end:}
<br>
	<p><strong>Laisser un commentaire :</strong></p>
    <textarea cols="50" name="comment" id="comment" rows="5"></textarea><br>
		<div>	
      <p><strong>  Note :</strong></p>
			<div id='myGrade'></div>
			<script type="text/javascript">
     			new Starbox('myGrade', 0, { stars: 5, buttons: 10, max: 10, rerate:true });
			</script>
      <div class="star_off" id='etoile_1' onClick="getRating('etoile_1',1);" ></div>
      <div id="etoile_2" onClick="getRating('etoile_2',2);" class="star_off"></div>
      <div id="etoile_3" onClick="getRating('etoile_3',3);" class="star_off"></div>
      <div id="etoile_4" onClick="getRating('etoile_4',4);" class="star_off"></div>
      <div id="etoile_5" onClick="getRating('etoile_5',5);" class="star_off"></div>
    </div>	
		<div style="text-align: center"><input type="button" value="Envoyer" class="btnComments" onClick="javascript:envoyer();"></div>
	</form>
<br>
<div class="spacer"></div>
<div class="spacer"></div>
{if:product.getComments()}
<ul class="listComments">
	<div class="listeavis">
  <!--<li flexy:foreach="product.getComments(),comment">-->
  	<h3 class="title"> <strong><span itemprop="ratingCount">{product.getTotal()} </span> clients ont donn&eacute; leurs avis sur {product.get(#name_1#):h}  </strong>  
	<span style="display:none;" itemprop="ratingValue">{product.getMoyenne()}</span>
<!-- img src="{baseURL}/templates/catalog/images/etoile{product.getMoyenne()}.png" alt="Etoiles" --></h3>
<div  flexy:foreach="product.getComments(),comment" itemprop="review" class="unAvis" itemscope="" itemtype="http://schema.org/Review">
    <!--<div itemprop="itemReviewed" itemscope="itemscope" itemtype="http://schema.org/Product">
    	<span itemprop="name">{product.get(#name_1#)}</span>
    </div>-->
<div class="entete">
<p class="date-auteur" style="text-align:right;" >
<em>
Avis d&eacute;pos&eacute; le {comment.date} par
<span itemprop="author">{comment.firstname} {comment.lastname}.</span>
</em>
</p>
<div itemprop="reviewRating" itemscope="" itemtype="http://schema.org/Rating">
<p class="note">
<!--{comment.rating} &eacute;toiles-->
<strong>Note attribu&eacute;e :</strong>
<img alt="Note" src="{baseURL}/templates/catalog/images/etoile{comment.rating}.png">
</p>
<meta content="{comment.rating}" itemprop="ratingValue">  
<meta itemprop="worstRating" content="1">
<meta itemprop="bestRating" content="5">
</div>
</div>
<div class="clear">
</div>
<div class="avis">
<div itemprop="description">
<strong>Commentaire :</strong><br>
{comment.text:e}
</div>
</div>
</div>
	 <!--</li>-->
</div>
</ul>
{else:}
	<p><em>Il n'y a pas encore de commentaire sur ce produit.</em></p>
{end:}
<div class="spacer"></div><br>