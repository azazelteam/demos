
<div class="product-item">
    <a class="product-item__info" href="{product.getURL()}" itemprop="url">
        <span class="ruban promo" flexy:if="product.getPromotionCount()"><span>-{product.getPromotionRate():h} %</span></span>

        <img class="product-item__img" itemprop="image" src="{product.getImageURI(#220#)}" alt="{product.get(#name_1#):h}" />
    
        <h4 class="cat-item__title" itemprop="name">
            {product.get(#name_1#):h}
        </h4>
    
        <!-- <small>
            <strong>{product.getReferenceCount()}</strong>
            r&eacute;f&eacute;rence(s)
        </small> -->
    
        <div class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
            <!-- <small>&agrave; partir de</small><br /> -->
            <span itemprop="price">
                <!-- {product.getLowerPriceET():h} <small>HT</small> -->
                <span class="ttc">
                    {product.getLowerPriceATI():h}
                  </span>
            </span>
            <a title="Ajouter au panier" class="btn-det" href="{product.getURL()}">
                <span>Ajout panier</span>
            </a>
        </div>
    </a>
</div>
