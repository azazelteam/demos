<!--====================================== Colonne droite ======================================-->
        
<flexy:toJavascript start_url="start_url"></flexy:toJavascript>        
        
<script type="text/javascript" language="javascript">

// Fonction d'oubli du mail
// Test des champs du formulaire de login
	
	function verifLogin (){
		var login = document.forms.RegisterForm.elements[ 'Login' ].value;	 
		var password = document.forms.RegisterForm.elements[ 'Password' ].value;
			
		if(login == ''){
			alert('Le champ Identifiant est vide !!!');
			return false;
		}
		
		if(password == ''){
			alert('Le champ Mot de passe est vide !!!');
			return false;
		}
		return true ;
	}
		function verifCreateAccount() {
		
		var data = $('#CreateAccount').formSerialize(); 
		
		$.ajax({
			url: "/catalog/account.php?createLoginAjax",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ },
		 	success: function( responseText ){
		 		
		 		if(responseText != '') {
					$("#returnCreateAccount").html(responseText); 
				} else {
					document.getElementById("CreateAccount").submit();
					
				}

			}
		});
	}
	

	//Mot de passe oublié
	function forgotPassword() {
		$( "#forgotPassword" ).dialog({
			title:"Mot de passe oublié ?",
			bgiframe: true,
			modal: true,
			width:550,
			minHeight:10,
			closeText: "Fermer",
			/*overlay: {
				backgroundColor: '#000000',
				opacity: 0.5
			},*/
			close: function(event, ui) { $( "#forgotPassword" ).dialog( "destroy" ); }
		});
	}
	
	//Contrôle de l'email saisie pour le mot de passe oublié
	function sendPassword( Email, divResponse ) {	
		data = "&Email="+Email;
		$.ajax({

			url: "/catalog/forgetmail.php",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer l'email" ); },
			success: function( responseText ) {				 		
				$(divResponse).html("<br/><p class='blocTextNormal' style='text-align:center;'>" + responseText + "</p>");
			}
			
		 });
	}


// Fonction de sélection des informations newsletter
function newsletter () {
	test = window.open(start_url + '/catalog/newsletter.php','hw','width=380,height=450,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1')
}
</script>
         
        	<div id="frameD">            
				{if:iduser}<span style="color:red;font-weight: bold;">/b_right.tpl</span>{end:}
				   <div class="module" id="mod-account">
        
        <h4></h4>	    		
        <div class="ctn">	 
		
            {if:authentified}   		
            
                <span class="welcome" style="color: #fff; font-weight: bold;">Bienvenue</span>					
                <span class="userlogin" style="color: #fff;">{buyerFirstname} {buyerLastname}</span>
                
                <br/>
                  <br/>
                <ul>					
                    <li class="left"><a href="{start_url}/catalog/account.php"  rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"> Mon compte</a></li>					
                    <li class="left"><a href="{start_url}/www/logoff.php"  rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"> [Déconnexion]</a></li>
                </ul>
    
            {else:}
              
                <form id="Form_logon" action="{start_url}/catalog/account.php" method="post" name="Form_logon" target="_top">
            
                    <!--<label for="emaillogin">email</label>-->
                    <span class="inputG"></span><input type="text"  value="email" name="Login" id="emaillogin" onclick="this.value=''" /><span class="inputD"></span>
                    <div class="clear"></div>

                    <!--<label for="passwlogin">mot de passe</label>-->
                    <span class="inputG" style="*margin-top:-7px;"></span><input style="*margin-top:-7px;" type="password" value="mot de passe" name="Password" id="passwlogin" onclick="this.value=''" /><span style="*margin-top:-7px;" class="inputD"></span>
                    <div class="clear"></div>

                    <input type="image" class="image png" src="http://www.abisco.fr/www/_images/0_general/picto_login.png" />
                        
                </form>
            
                <ul>
                    <li><a style="margin-left: 4px;" href="{start_url}/catalog/register.php" rel="nofollow">Créer un compte </a></li>				      				      
                    <li><a style="margin-left: 4px;" href="#" onclick="forgotPassword(); return false;" rel="nofollow">Mot de passe oublié ? </a></li>				
                </ul>
                        
            {end:}
        </div>
        <div class="module-bottom"></div>                        
    </div>
			
            </div>
			
			<div id="forgotPassword">
				<div id="EmailResponse" style="margin:0 0 0 50px; width:445px;">
					<p><strong>Saisissez votre adresse e-mail pour récupérer vos identifiants :</strong></p>
					<input type="text" id="Email" name="Email" value="" class="loginFieldMedium input_email" style="width:285px;" />
					<input type="submit" name="Envoyer" value="Envoyer" class="btn_send" onclick="sendPassword( $('#Email').val(), '#EmailResponse' );" />
				</div>
			</div>
        
        </div>        
        <div class="clear"></div>