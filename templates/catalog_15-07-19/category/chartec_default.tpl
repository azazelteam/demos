<flexy:include src="header.tpl"></flexy:include>
<!--<flexy:include src="leftBanner.tpl"></flexy:include>-->
{if:categoryName}
<h3 class="CategoryName">{categoryName}</h3>

{end:}
<!--====================================== Contenu ======================================-->
<div id="center" class="page catalog">
    <ul class="pathway">
        <li><a href="{start_url}" class="pwhome" title="Accueil"><img class="noborder" alt="Accueil" width="16" height="15" src="{starturl}/templates/catalog/images/icon-home.png" /></a></li>

        {if:categoryPath}

        {foreach:categoryPath,cat}
        <li>
            <a href="{cat[href]}">{cat[name]}</a>
        </li>
        {end:}

        {end:}



    </ul>
{if:iduser}<span style="color:red;font-weight: bold;">/category/chartec_default.tpl</span>{end:}
    {breadcrumbs:h}

	<!--<div id="panier-b">
            	<a title="Consulter mon panier" class="part-l" href="/catalog/basket.php">
                	<span class="h3">Mon <span class="strong">panier</span></span>             
	                <span class="num"><span class="strong">
                    {if:basket.getItemCount()}
                                        {basket.getItemCount()}</span> article(s)</span>
                                                               		
                                    {CategoryNameelse:}
                                        0</span> article(s)</span>
                                    {end:}
                   </a>     
                    <a title="Voir le panier" class="part-r" href="/catalog/basket.php">            
                    {if:basket.getItemCount()}
                    	<span class="total">Total : <span class="strong">{basket.getTotalET():h}</span></span>
                    {else:}
                    	<span class="total">Total : <span class="strong">0 &euro;</span></span>
                    {end:}
					<span class="btn-med">Voir le panier</span>
                </a>
   </div>-->
   <script type="text/javascript">
   	$(document).ready(function(){
		$("div#txtrub p").each(function(i){
			/*alert($(this).text().length);*/
			var lng = $(this).text().length;
			/*alert("123");*/
			if(lng == 1 || lng == 0){
				$(this).remove();
				/*alert($(this).text().length);*/
			}
 		});
		$("div#txtrub2 p").each(function(i){
			/*alert($(this).text().length);*/
			var lng = $(this).text().length;
			/*alert("123");*/
			if(lng == 1 || lng == 0){
				$(this).remove();
				/*alert($(this).text().length);*/
			}
 		});
	});
   </script>
   <div id="colG" class="colx1">



      		<h1><!--{category.get(#name_1#)}--><!--{cat_page_title}-->{category.getName():h}</h1>
    
    
   	<!--<div flexy:if="category.get(#description_1#)" id="category_description" class="category_description">{category.get(#description_1#):h}</div>
    <div class="category_image"><img src="{category.getImageURI(#300#)}" style="max-width: 100%;height: auto;" width="180" height="180" alt="{cat_page_title}" /></div>-->
	<div class="spacer" style="clear:both;margin-bottom:20px;"></div>

	{if:category.getIntitule()}
	<div id="filtersearch" class="searchfilter" style="margin-bottom:20px;">
	   {category.getIntitule():h}
	</div>
	{end:}

<div id="multifiltersearch" class="searchfilter with-left">
	<div class="subcategories">
        <ul class="subcategories_list">
            <li flexy:foreach="category.getChildren(),composite">
                {if:!composite.isCategCustomPage()}
                {if:!composite.get(#html#)}
                <h2><a href="{composite.getURL():h}" title="{composite.get(#name_1#):h}">{composite.get(#name_1#):h}</a></h2>
                <a class="btn-det" href="{composite.getURL():h}" style="left:0px;">
                   
                </a>
                <a href="{composite.getURL():h}" title="{composite.get(#name_1#):h}">
                    <img src="{composite.getImageURI(#130#)}" width="130" height="130" alt="{composite.get(#name_1#):h}" title="{composite.get(#name_1#):h}" />
                    <small>
                    <strong>
                    {if:composite.get(#html#)}x{else:}{if:composite.isCategCustomPage()}x{else:}{composite.getReferenceCount():h}{end:}{end:}
                    </strong>
                    r&eacute;f&eacute;rence(s)
                    </small>
                </a>
                {end:}
                {end:}
            </li>
        </ul>
	</div>
    <br style="clear:both;" />


<div id="loader"style="color:#44474E; font-size:13px; font-weight:bold; padding:20px; text-align:center;display:none; position:fixed; z-index:100000; background-color:#000; opacity:0.7; width:100%; height:100%; text-align:center; padding-top:300px; left:0px; top:0px;">
	<img src="{starturl}/images/back_office/content/loading.gif" alt="Chargement..." /><br />
			Recherche en cours...
	</div>

<script type="text/javascript">
	$('ul.subcategories_list li').mouseover(function(e) {
		$(this).find('a.btn-det').stop().animate({left:0}, 200, "easeInOutCirc");
	});
	$('ul.subcategories_list li').mouseout(function(e) {
		$(this).find('a.btn-det').stop().animate({left:-60}, 200, "easeInOutCirc");
	});
    $('ul.subcategories_list li').each(function (){
        if ( $(this).children().length == 0 )
            $(this).remove();
    });
</script>

<!--====================================== Fin du contenu ======================================-->
        
<!-- Pied de page -->
</div>

	{if:category.get(#description_plus_1#)}
		<div id="category_description_2" class="category_description">
			{category.get(#description_plus_1#):h}
			 <div class="category_image"><img src="{category.getImageURI(#600#)}" style="margin-right:27px" width="600" height="600" alt="{cat_page_title:h}" /></div>
		</div>
	{end:}


</div>

	</div>
	</div>
<flexy:include src="foot.tpl"></flexy:include> 