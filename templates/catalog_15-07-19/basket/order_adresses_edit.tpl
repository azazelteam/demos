<script type="text/javascript" language="javascript">
<!--
function displayForm(id) {
	//alert(id);
	tableForm = document.getElementById(id);
	//alert(tableForm);
	if(tableForm) {
		//alert(tableForm.style.display);
		if(!tableForm.style.display || tableForm.style.display == 'none') {
			tableForm.style.display = 'inline-block';
			document.location.hash = '#'+id;
		}
		else {
			tableForm.style.display = 'none';
			if (id == 'billing_different')
				if (document.getElementById("same_address").checked)
					document.getElementById("same_address").checked = false;
		}
	}
	
}
function setBillingAdress()
{
	displayForm('billing_different');
	var ids = ['title', 'lastname', 'adress', 'zipcode', 'idstate', 'phonenumber', 'firstname', 'adress_2', 'city', 'company', 'faxnumber'];
	if (document.getElementById("same_address").checked)
		document.getElementById("billing_checkbox").checked = true;
	else
		document.getElementById("billing_checkbox").checked = false;
	for (i = 0; i < ids.length; i++)
	{
		var deliveryElement = document.getElementById("delivery["+ids[i]+"]");
		var billingElement = document.getElementById("billing["+ids[i]+"]");
		if (deliveryElement.nodeName.toLowerCase() === 'select')
			billingElement.selectedIndex = deliveryElement.selectedIndex;
		else
			billingElement.value = deliveryElement.value;
	}
	document.getElementById("idbilling").selectedIndex = 0;
}
function verifForm (){
	if (document.getElementById('delivery_checkbox').checked)
	{
		if( ValidateAdresses.elements[ 'delivery[Lastname]' ].value == '') {
			alert('Le champ Nom est vide !!!');
			return false;
		}  
		if( ValidateAdresses.elements[ 'delivery[Phonenumber]' ].value == '') {
			alert('Le champ T\351l\351phone est vide !!!');
			return false;
		}
		if( ValidateAdresses.elements[ 'delivery[Company]' ].value == '') {
			alert('Le champ Entreprise est vide !!!');
			return false;
		}
		if( ValidateAdresses.elements[ 'delivery[Adress]' ].value == '') {
			alert('Le champ Adresse est vide !!!');
			return false;
		}
		if( ValidateAdresses.elements[ 'delivery[Zipcode]' ].value == "") {
			alert('Le champ Code Postal est vide !!!');
			return false;
		}	
		if( ValidateAdresses.elements[ 'delivery[City]' ].value == "") {
			alert('Le champ Ville est vide !!!');
			return false;
		}
	}
	if (document.getElementById('billing_checkbox').checked)
	{
		if( ValidateAdresses.elements[ 'billing[Lastname]' ].value == '') {
			alert('Le champ Nom est vide !!!');
			return false;
		}  
		if( ValidateAdresses.elements[ 'billing[Phonenumber]' ].value == '') {
			alert('Le champ T\351l\351phone est vide !!!');
			return false;
		}
		if( ValidateAdresses.elements[ 'billing[Company]' ].value == '') {
			alert('Le champ Entreprise est vide !!!');
			return false;
		}
		if( ValidateAdresses.elements[ 'billing[Adress]' ].value == '') {
			alert('Le champ Adresse est vide !!!');
			return false;
		}
		if( ValidateAdresses.elements[ 'billing[Zipcode]' ].value == "") {
			alert('Le champ Code Postal est vide !!!');
			return false;
		}	
		if( ValidateAdresses.elements[ 'billing[City]' ].value == "") {
			alert('Le champ Ville est vide !!!');
			return false;
		}
	}
	return checkAccept();
}
function selectDeliveryAddress(selectedDeliveryAdressID)
{
	if(selectedDeliveryAdressID)
	{
		var deliveryAdress = window['deliveryAddresses'+selectedDeliveryAdressID];
		for (var key in deliveryAdress)
		{
			CurrentElement = document.getElementById('delivery['+key+']');
			if (CurrentElement !== null)
			{
				if (CurrentElement.nodeName.toLowerCase() === 'select')
				{
					index = parseInt(deliveryAdress[key]);
					if (typeof index === 'number')
						CurrentElement.selectedIndex = index-1;
					else
					{
						for(i = 0; i < CurrentElement.lenght; i++)
						{
							if(CurrentElement.options.item(i).text == deliveryAdress[key])
							{
								CurrentElement.selectedIndex = i;
								break;
							}
						}
					}
				}
				else
					CurrentElement.value = deliveryAdress[key];
			}
			else{
				console.log(key);
			}
		}
	}
}
function selectBillingAddress(selectedBillingAddressID)
{
	if(selectedBillingAddressID)
	{
		var billingAdress = window['billingAddresses'+selectedBillingAddressID];
		for (var key in billingAdress)
		{
			CurrentElement = document.getElementById('billing['+key+']');
			if (CurrentElement !== null)
			{
				if (CurrentElement.nodeName.toLowerCase() === 'select')
				{
					index = parseInt(billingAdress[key]);
					if (typeof index === 'number')
						CurrentElement.selectedIndex = index-1;
					else
					{
						for(i = 0; i < CurrentElement.lenght; i++)
						{
							if(CurrentElement.options.item(i).text == billingAdress[key])
							{
								CurrentElement.selectedIndex = i;
								break;
							}
						}
					}
				}
				else
					CurrentElement.value = billingAdress[key];
			}
			else{
				console.log(key);
			}
		}
	}
}
//-->
</script>
{setDeliveryListJSData:h}
{setBillingListJSData:h}
{if:iduser}<span style="color:red;font-weight: bold;">/basket/order_adresses_edit.tpl</span>{end:}
    <p style="line-height: 40px;float:left;margin-right:20px">
		<label for="delivery_checkbox" style="display: block;height: 28px;cursor: pointer;">
		<span class="icon-delivery"></span>&nbsp;Utiliser une adresse de livraison différente.</label>
		<input style="display:none" id="delivery_checkbox" type="checkbox" name="delivery_checkbox" value="1" onclick="javascript: displayForm('delivery_different');" />
	</p>
    <div id="delivery_different" style="display:none; margin:5px 0;clear:both">
		<h2>Adresse de livraison</h2>
    	<p class="txt_oblg">Tous les champs marqués d'un <span class="ast_orange">*</span> sont à remplir obligatoirement.</p>
		<p class="same_address"><input style="background-color:white; background:none; height:13px;width:auto;" border="0" id="same_address" type="checkbox" name="same_address" value="1" onclick="javascript: setBillingAdress();" /><label for="same_address">&nbsp;Utiliser la même adresse pour la facturation</label></p>
        <div class="form_left" style="float:left">
            
            <div class="form_line"><label>Adresse existante</label>
    		{drawliftdelivery:h}</div>
            <div class="spacer"></div>
			
			<div class="form_line"><label>Entreprise&nbsp;<span class="ast_orange">*</span></label>
    		<input type="text" size="33" name="delivery[Company]" id="delivery[company]" class="AuthInput" value="">
            </div>
			
			<div class="form_line"><label>Titre</label>
    		{deliveryTitleList:h}
            </div>
			
            <div class="form_line"><label>Nom&nbsp;<span class="ast_orange">*</span> </label>
            <input type="text" size="33" class="AuthInput" name="delivery[Lastname]" id="delivery[lastname]" value="">
            </div>
            
            <div class="form_line"><label>Prénom</label>
     		<input type="text" size="33" class="AuthInput" name="delivery[Firstname]" id="delivery[firstname]" value="">
            </div>
            
            <div class="form_line"><label>Adresse&nbsp;<span class="ast_orange">*</span></label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Adress]" id="delivery[adress]" value="">
            </div>
            
            <div class="form_line"><label>Compl. adresse</label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Adress_2]" id="delivery[adress_2]" value="">
            </div>
            
            <div class="form_line"><label>Code postal&nbsp;<span class="ast_orange">*</span></label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Zipcode]" id="delivery[zipcode]" value="">
            </div>
                        
            <div class="form_line"><label>Ville&nbsp;<span class="ast_orange">*</span></label>
    		<input type="text" size="33" class="AuthInput" name="delivery[City]" id="delivery[city]" value="">
            </div>
            
            <div class="form_line"><label>Pays&nbsp;<span class="ast_orange">*</span></label>
    		{drawliftd_state:h}
            </div>
			<div class="spacer"></div>
            
            
            <div class="form_line"><label>Téléphone&nbsp;<span class="ast_orange">*</span></label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Phonenumber]" id="delivery[phonenumber]" value="" maxlength="20">
            </div>

            <div class="form_line"><label>Fax</label>
    		<input size="33" type="text" class="AuthInput" name="delivery[Faxnumber]" id="delivery[faxnumber]" value="" maxlength="20">
            </div>
        </div>
    </div>
    <div class="spacer"></div>
	
	
	<p style="line-height: 40px;float:left;margin-top:0px;position: absolute;top: 0;left: 300px;" id="delivery_btn">
		<label for="billing_checkbox" style="cursor:pointer;"><span class="icon-billing"></span>&nbsp;Utiliser une adresse de facturation différente.</label>
		<input style="display:none" id="billing_checkbox" type="checkbox" name="billing_checkbox" value="1" onclick="javascript: displayForm('billing_different');" />
	</p>
	
    <div id="billing_different" style="display:none; margin:5px 0;">
		<h2>Adresse de facturation</h2>
    	<p class="txt_oblg">Tous les champs marqués d'un <span class="ast_orange">*</span> sont à remplir obligatoirement.</p>
    	<div class="form_left" style="float:left">
            
            <div class="form_line"><label>Adresse existante</label>
			{drawliftbilling:h}</div>
            <div class="spacer"></div>
			
			<div class="form_line"><label>Entreprise&nbsp;<span class="ast_orange">*</span></label>
			<input type="text" class="AuthInput" size="33" name="billing[Company]" id="billing[company]" value="" />
            </div>
            
			<div class="form_line"><label>Titre</label>
			{billingTitleList:h}
            </div>
            
            <div class="form_line"><label>Nom&nbsp;<span class="ast_orange">*</span> </label>
			<input type="text" class="AuthInput" size="33" name="billing[Lastname]" id="billing[lastname]" value="" />
            </div>
            
            <div class="form_line"><label>Prénom</label>
			<input type="text" class="AuthInput" size="33" name="billing[Firstname]" id="billing[firstname]" value="" />
            </div>
            
            <div class="form_line"><label>Adresse&nbsp;<span class="ast_orange">*</span></label>
			<input size="33" class="AuthInput" type="text" name="billing[Adress]" id="billing[adress]" value="" />
            </div>
            
            <div class="form_line"><label>Compl. adresse</label>
			<input size="33" class="AuthInput" type="text" name="billing[Adress_2]" id="billing[adress_2]" value="" />
            </div>
            
            <div class="form_line"><label>Code postal&nbsp;<span class="ast_orange">*</span></label>
			<input size="33" type="text" class="AuthInput" name="billing[Zipcode]" id="billing[zipcode]" value="" />
            </div>
            
            <div class="form_line"><label>Ville&nbsp;<span class="ast_orange">*</span></label>
			<input type="text" size="33" class="AuthInput" name="billing[City]" id="billing[city]" value="" />
            </div>
            
            <div class="form_line"><label>Pays&nbsp;<span class="ast_orange">*</span></label>
			{drawliftb_state:h}
            </div>
			<div class="spacer"></div>
            
            <div class="form_line"><label>Téléphone&nbsp;<span class="ast_orange">*</span></label>
			<input size="33" type="text" class="AuthInput" name="billing[Phonenumber]" id="billing[phonenumber]" value="" maxlength="20" />
            </div>
			
            <div class="form_line"><label>Fax</label>
			<input size="33" type="text" class="AuthInput" name="billing[Faxnumber]" id="billing[faxnumber]" value="" maxlength="20" />
            </div>
        </div>
    </div>