{if:iduser}<span style="color:red;font-weight: bold;">/basket/basket_amounts.tpl</span>{end:}
<table style="padding: 0 10px;" border="0" width="100%" cellspacing="0" class="recap_basket">
{if:is_valid}
		<tr>
            <th class="Amounts_port" width="70%" align="left">{total_amount_ht}</th>
            <td class="price" width="30%" style="text-align:right">{total_amount_ht_avant_remise:h} &nbsp;</td>
		</tr>
		<tr>
            <th class="Amounts_port" align="left">Taux de remise commerciale</th>
            <td class="price" style="text-align:right">{total_discount:h}&nbsp;</td>
		</tr>
		<tr>
            <th class="Amounts_port" align="left">Remise commerciale</th>
            <td class="price" style="text-align:right">{total_discount_amount:h}&nbsp;</td>
		</tr>
{end:}
<tr style="background-color:#dbdbdb;font-weight:bolder;">
	<th class="Amounts_port" style="font-weight:normal;text-align:left;width:190px;">
		<span style="font-weight:bold">Frais de port et emballage H.T.</span>
	
	</th>
	<td class="price" style="text-align:right">
		{total_charge_ht:h}
	</td>
</tr>
<tr>
    <th class="Amounts_port" align="left">Montant total HT</th>
    <td class="price" style="text-align:right">{net_to_pay_ht:h}</td>
</tr>
<tr>
   <tr class="allowOrder">
        <th align="left" style="color:#00CC00">R&eacute;duction totale H.T</th>
        <td align="right" style="color:#00CC00">-{reduction:h}</td>
    </tr>
    <th class="Amounts_tot" align="left">Montant total T.T.C{if:estimate}*{end:}</th>
    <td class="price" style="text-align:right">{net_to_pays:h}</td>
</tr>
{if:estimate}
	<tr>
	<td coslpan="2" class="frais_port">
	Conditions ......
	</td>
	</tr>
{end:}
</table>


