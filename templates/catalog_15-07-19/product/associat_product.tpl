<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/associate_products.css" />
{if:iduser}<span style="color:red;font-weight: bold;">/product/associat_product.tpl</span>{end:}
<table class="optionTable" style="margin-top: 10px;">
	{foreach:associates,associate}
	<tr>
		<td class="" rowspan="2">
			<a href="{associate[url]}" >
			<!-- Affichage icône produit -->
			<div class="productImageDetail1">
					<img src="{associate[icone]}" alt ="{associate[name]}" border="0" />
				</a>
			</div>
			<!--<img class="AssociateProductIcon" src="{associate[icone]}" alt="fiche produit" /></a>-->
		</td>
		<td class="titleOption">{associate[name]}</td>
	</tr>
	<tr>
		<td align="center">
			{if:!associate[hidecost]}
				
					<table cellspacing="0" border="0" class="AssociateProductNoBorderTable">
						<tr>
						{if:associate[articleCount]}
							<th class="">Réf.:&nbsp;{associate[ref]}</th>
							<td class="">{associate[WotPrice]:h}</td>
							<td class=""><input type="text" name="quantity" value="1" size="1" /></td>
							<td>
							Quantité : <input id="quantity_{associate[idarticle]}"type="text" name="quantity" value="1" size="1" class="input_quantite" />
							<input type="button" onclick="addToCart('{associate[idarticle]}', $('#quantity_{associate[idarticle]}').val(), 'order');" name="AddCmd" value="Commander" class="input_basket_orange" />
							<input type="button" onclick="addToCart('{associate[idarticle]}', $('#quantity_{associate[idarticle]}').val(), 'estimate');" name="AddDev" value="Devis" class="input_basket_green" />
							</td>
						{else:}
							<th colspan="2"><a href="{associate[url]}">Voir toutes les r&eacute;f&eacute;rences</a></th>
							<td class="">{associate[WotPrice]:h}</td>
							<td></td>
						{end:}

						</tr>
					</table>

			{else:}
				
				<p class="center">
					<a class="linkContactoption" href="mailto:{associate[mail]}">Contactez-nous</a>
				</p>

			{end:}

		</td>
	</tr>

{end:}
</table>