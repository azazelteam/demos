<link type="text/css" rel="stylesheet" href="{start_url}/templates/catalog/css/details.css" />
<form action="" method="post" name="mainform" enctype="multipart/form-data">
	<input type="hidden" name="IdArticle" value="">
	<input type="hidden" name="quantity" value="">
	<input type="hidden" name="idcolor" value="">
	<input type="hidden" name="idtissus" value="">
</form>
{if:iduser}<span style="color:red;font-weight: bold;">/product/detail_1.tpl</span>{end:}
<table class="contenerDetails">
	<tr>
		<td align="left" valign="bottom">
			<table cellspacing="0" width="100%" class="DetailTable">
				<tr>
					<td style="border:none">&nbsp;</td>
					<td width="60" class="thcenter thtop characLabel" style="font-size: 12px; color: #7243b7;">
						<b>{if:chefProductRights}<a id="Link" href="{start_url}/product_management/catalog/cat_product.php?idproduct={idproduct}">{end:}Référence{if:chefProductRights}</a>{end:}</b>
					</td>
					{foreach:intitules,intitule}
					<th class="thcenter thtop characLabel">{intitule:h}</th>
					{end:}
					<th class="thcenter thtop characLabel"><b>Départ Usine</b></th>
					{if:hasPromo}
					<th class="thcenter thtop">Prix tarif</th>
					<th class="promo thcenter thtop">Promotion</th>
					{end:}
					<th class="thcenter thtop characLabel"><b>Prix HT</b></th>
					{if:showCde}
					<th id="order[0][label]" class="thcenter thtop characLabel">Commande</th>
					{end:}
					<th id="estimate[0][label]" class="thcenter thtop characLabel">Devis</th>
					<td width="20" class="icone_comp thtop Row0" style="color: #292929; font-size: 11px; font-weight: bold;"><form action="{start_url}/catalog/compare_func.php" method="post" style="color:#E57D88;" enctype="multipart/form-data"><a id="IncompareLink" style="color:#292929; font-weight: bold; font-size: 11px; text-decoration: underline;" href="{start_url}/catalog/product_compare.php">Comparer</a></form></td>
				</tr>
				{foreach:detail1,detailReference}
				<tr>
					<td style="border:none">&nbsp;</td>
					<td class="thcenter thtop characLabel" style="color: #7243b7" valign="middle"><b>{detailReference[reference]:h}</b></td>
					{foreach:detailReference[intitule],detailIntitule}
					<td class="carac Row{detailReference[offset]} characValue">{detailIntitule:h}</td>
					{end:}
					<td class="delay Row{detailReference[offset]} characValue"><b>{detailReference[delay]}</b></td>
					{if:hasPromo}
						<td>{detailReference[basicPrice]:h}</td>
						<td>{detailReference[reduceRate]:h}</td>
					{end:}
					<td  class="Row{detailReference[offset]}">{detailReference[prices][HT]:h} HT</td>
					{if:showCde}
					<td id="order[0][value][{detailReference[offset]}]" class="Row{detailReference[offset]} caracts">
					<!--<td id="order[0][value][{detailReference[offset]}]" class="Row{detailReference[offset]} OrderCell characValue">-->
						<!-- Commande -->
						{if:detailReference[order][has]}
							<!--<input type="image" name="AddCde" src="{start_url}/www/icones/ok_vert.gif" onclick="document.mainform.IdArticle.value={detailReference[order][idarticle]};document.mainform.quantity.value={detailReference[order][quantity]};document.mainform.action='{start_url}/catalog/basket.php';document.mainform.submit();" />-->
						   <!-- <input class="input_commander_small" value="Commander" type="button" name="AddCde" onclick="addToCart({detailReference[order][idarticle]},{detailReference[order][quantity]},'order');" /> -->
						{else:} - {end:}
					</td>
					{end:}
					<!--<td id="estimate[0][value][{detailReference[offset]}]" class="Row{detailReference[offset]} OrderCell characValue">-->
					<td id="estimate[0][value][{detailReference[offset]}]" class="Row{detailReference[offset]} caracts">
						<!-- Devis -->
							<!--<input type="image" name="AddDevis" src="{start_url}/www/icones/ok_rouge.gif" onclick="document.mainform.IdArticle.value={detailReference[articles][idarticle]};document.mainform.quantity.value={detailReference[articles][quantity]};document.mainform.action='{start_url}/catalog/estimate_basket.php';document.mainform.submit();" />-->
							<input type="button" class="input_devis_small" value="Devis" type="button" name="AddDevis" src="{start_url}/www/icones/bt_devis_ok.png"  onclick="addToCart({detailReference[order][idarticle]},{detailReference[order][quantity]},'estimate');" />
					</td>
					<td>
						<!-- Comparateur -->
						<form action="{start_url}/catalog/compare_func.php" method="post" enctype="multipart/form-data">
				  		  		<input type="hidden" name="IdProduct" value="{detailReference[articles][idarticle]}" >
							  	&nbsp;
							  	<input name="incompare[]" value="{detailReference[articles][idarticle]}" type="checkbox" OnClick="form.submit();">
						  		</form>
					</td>
				</tr>
				{end:}
			</table>
		</td>
	</tr>
	<tr>
		<td class="alignRight">
			<form action="{start_url}/catalog/compare_func.php" method="post" style="color:#E57D88;" enctype="multipart/form-data">
				<a id="IncompareLink{incompareGroup}" style="color:#CD0820;" href="{start_url}/catalog/product_compare.php">Cochez et comparez <img src="{start_url}/www/icones/ok_rouge.gif" alt="Comparez" style="vertical-align:text-bottom; border-style:none;" /></a>	
			</form>	
		</td>
	</tr>
</table>