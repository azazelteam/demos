<flexy:include src="/header.tpl"></flexy:include>
<!--====================================== Contenu ======================================-->
<div id="center" class="page catalog trade">

{if:iduser}<span style="color:red;font-weight: bold;">/trade.tpl</span>{end:}

	<!-- Contenu  -->
    <ul class="pathway">
        <li><a href="...." class="pwhome" title=""><img class="noborder" width="16" height="15" src="{starturl}/templates/catalog/images/icon-home.png" alt="" /></a></li>
        <li>
            <a href="{trade[tradeurl]:h}" title="{trade[name]:e}">{trade[name]:h}</a>
        </li>
    </ul>    
    <div id="panier-b">
            	<a title="Consulter mon panier" class="part-l" href="/catalog/basket.php">
                	<h3>Mon <strong>panier</strong></h3>             
	                <span class="num"><strong>
                    {if:basket.getItemCount()}
                                        {basket.getItemCount()}</strong> article(s)</span>
                                    {else:}
                                        0</strong> article(s)</span>
                                    {end:}
                   </a>     
                    <a title="Commander" class="part-r" href="/catalog/basket.php">            
                    {if:basket.getItemCount()}
                    	<span class="total">Total : <strong>{basket.getTotalET():h}</strong></span>
                    {else:}
                    	<span class="total">Total : <strong>0 &euro;</strong></span>
                    {end:}
					<div class="btn-med">Commander</div>
                </a>
   </div>   
   <script type="text/javascript">
   	$(document).ready(function(){
		$("div#trade_description p").each(function(i){
			var lng = $(this).text().length;
			if(lng == 1 || lng == 0){
				$(this).remove();
			}
 		});
		$("div#trade_description_plus p").each(function(i){
			var lng = $(this).text().length;
			if(lng == 1 || lng == 0){
				$(this).remove();
			}
 		});
	});
   </script>
   <h1 class="trade_name">{trade[name]:h}</h1>
                        <div class="trade_description" id="trade_description">
                        	{trade[description]:h}
                        </div>
                        <div class="trade_image"><img style="margin-right:27px;" width="180" src="{trade[image]:h}" alt="Equipement {trade[name]}" /></div>        
        {if:trade[hasProducts]}
		      	<!-- metier avec des produits -->
                    
                    <div id="loader"style="color:#44474E; font-size:13px; font-weight:bold; line-height:40px; padding:20px; text-align:center;display:none; position:fixed; z-index:100000; background-color:#000; opacity:0.7; width:100%; height:100%; text-align:center; padding-top:230px; left:0px; top:0px;">
                    <img src="{starturl}/images/back_office/content/loading.gif" alt="Chargement..." /><br />
                            Recherche en cours...
                    </div>                           
                     <div class="products">                            
                       {prdhtml:h}	
                    </div>               
	        {else:}
	        	<!-- metiers avec des sous-metiers -->
		        {if:trade[trades]}
		        	{foreach:trade[trades],trade}						
						<div class="metierClip">
						<a href="{trade[url]}">
						<img src="{trade[image]:h}" alt="{trade[trade]:h}"></a>
						</div>
					{end:}
		        {end:}		        
	        {end:} 
            <div class="trade_description_plus" id="trade_description_plus">
                  {trade[description_plus_1]:h}
             </div>    
</div>	
<script type="application/javascript">
	/* Suppression de ",00" du pourcentage de remise */
	$(".rate").each(function(index) {
		var discountFloat = $(this).text().trim();
		var discountInt = discountFloat.replace(/,[0-9]*\s%/g, "%");
		$(this).html(discountInt);
	});
	$('ul.listprods li').mouseover(function(e) {
$(this).find('a.btn-buy').stop().animate({left:28}, 200, "easeInOutCirc");
})
$('ul.listprods li').mouseout(function(e) {
$(this).find('a.btn-buy').stop().animate({left:-44}, 200, "easeInOutCirc");
}) 
</script>
<!--====================================== Fin du contenu ======================================-->
<!-- Colonne de droite de la page -->       
<!-- Pied de page -->
</div>
</div>

	</div><flexy:include src="foot.tpl"></flexy:include> 