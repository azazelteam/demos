<flexy:include src="header.tpl"></flexy:include>
<!-- Cat&eacute;gorie -->
<div id="content">
	{if:iduser}<span style="color:red;font-weight: bold;">/plan.tpl</span>{end:}
	<div id="categorybloc">
		<div class="intro" style="width: 979px !important;">
			<h1>Plan du site</h1>
			<!--
			<div id="text">
				<p><em>&agrave; r&eacute;diger...</em></p>
			</div>-->
		</div>
		<div class="spacer"></div>
		
		<article class="plan">
			<h2><a href="{baseURL}/"><strong>Abisco</strong></a></h2>
			<ul>
				<li><a href="{baseURL}" title="Accueil">Accueil</a></li>
				<li><a href="{baseURL}/qui.php" title="Qui sommes nous ?">Qui sommes nous ?</a></li>	
				<li><a href="{baseURL}/cgv.php" title="Conditions g&eacute;n&eacute;rales de vente">Conditions g&eacute;n&eacute;rales de vente</a></li>
				<li><a href="{baseURL}/catalog/contact.php" title="Contact">Contact</a></li>
			</ul>
		</article>
		
		<article flexy:foreach="category.getChildren(),composite" class="plan">
			{if:composite.getChildren()}
			<h2><a href="{composite.getURL()}" title="{composite.get(#name_1#):h}"><strong>{composite.get(#name_1#):h}</strong></a></h2>
			<ul>
				<li flexy:foreach="composite.getChildren(),index,souscat">
				{if:souscat.getChildren()}
				<h3><a href="{souscat.getURL()}" title="{souscat.get(#name_1#):h}">{souscat.get(#name_1#):h}</a></h3>
					<ul>
						<li flexy:foreach="souscat.getChildren(),souscat2">
						{if:souscat2.getChildren()}
							<h4><a href="{souscat2.getURL()}" title="{souscat2.get(#name_1#):h}">{souscat2.get(#name_1#):h}</a></h4>
							<ul>
								<li flexy:foreach="souscat2.getChildren(),souscat3">
								{if:souscat3.getChildren()}
									<h5><a href="{souscat3.getURL()}" title="{souscat3.get(#name_1#):h}">{souscat3.get(#name_1#):h}</a></h5>
								{end:}
								</li>
							</ul>
						{end:}
						</li>
					</ul>
				{end:}
				</li>
			</ul>
			{end:}
		</article>
		<div class="spacer"></div>
	</div>
	<div class="spacer"></div>
</div>
</div>
</div>
<style type="text/css">
	.plan h3, .plan h3 a {
	    font-weight: bold;
	}
</style>
<flexy:include src="foot.tpl"></flexy:include>