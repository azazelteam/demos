<?php 

/**
 * Supprime les images générées pour les statistiques et qui datent de plus d'un jour
 */
 
//-----------------------------------------------------------------------------------
 
include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

//-----------------------------------------------------------------------------------
 
//répertoires à nettoyer

$directories = array(

	"$GLOBAL_START_PATH/statistics/temp",
	"$GLOBAL_START_PATH/administration/statistics/temp"
	
);

//extenssions des fichiers à supprimer

$types = array( "png", "bmp" );

//-----------------------------------------------------------------------------------

$curdate = date( "d/m/Y H:i:s" );

foreach( $directories as $directory ){

	$deletedCount = 0;
	
	if( file_exists( $directory ) && is_dir( $directory ) ){
		
		$dir = opendir( $directory );
		if( !is_resource( $dir ) )
			die( "\nImpossible d'ouvrir le répertoire $directory" );
		
		while( $file = readdir( $dir ) ){
			
			if( $file != "." && $file != ".." && !is_dir( $file ) ){
			
				$i = 0;
				$deleted = false;
				while( $i < count( $types ) && !$deleted ){
					
					//vérifier l'extenssion
					
					$ext = strchr( $file, "." );
				
					if( $ext !== false && in_array( substr( $ext, 1 ), $types ) ){
						
						//vérifier la date du fichier
						
						$filedate = date( "Y-m-d", filemtime( "$directory/$file" ) );
						
						
						list( $fileyear, $filemonth, $fileday ) = explode( "-", $filedate );
						list( $curyear, $curmonth, $curday ) = explode( "-", date( "Y-m-d" ) );
					
						if( $fileyear < $curyear )
							$deleted = true;
						else if( $fileyear == $curyear ){
						
							if( $filemonth < $curmonth )
								$deleted = true;
							else if( $filemonth == $curmonth ){
							
								if( $fileday < $curday )
									$deleted = true;
									
							}
							
						}
						
						if( $deleted ){
							
							$deletedCount++;
							@unlink( "$directory/$file" ); 
							
						}
						
					}
					
					$i++;
					
				}
				
					
			}
				
		}
		
		//log

		echo "\n[ $curdate ] Suppression des fichiers ";
		
		$i = 0;
		while( $i < count( $types ) ){
			
			$type = $types[ $i ];
			
			if( $i )
				echo ", ";
				
			echo "*.$type";
			
			$i++;
			
		}

		echo " : $deletedCount suppression(s) dans $directory";
		
	}
	else echo "\nLe répertoire $directory n'existe pas";
	
}

?>