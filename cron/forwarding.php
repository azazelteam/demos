<?php

define( "DEBUG_EMAIL", false );
 
/**
 * Notifications de mise à disposition de la marchadise : dès que la date est renseignée
 * Notifications d'expédition de la marchandise aux clients : le jour de l'expédition
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/SupplierOrder.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );


User::fakeAuthentification();

//sendAvailabilityMails();
sendForwardingMails();

/* ------------------------------------------------------------------------------------------------ */

function sendForwardingMails(){

	$rs = DBUtil::query( 
	
		"SELECT idorder_supplier 
		FROM order_supplier 
		WHERE status NOT LIKE 'cancelled'
		AND idorder <> 0
		AND idbuyer <> '" . DBUtil::getParameter( "internal_customer" ) . "'
		AND dispatch_date LIKE '" . date( "Y-m-d" ) . "'
		AND delivery_mail_date LIKE '0000-00-00'
		AND delivery_mail_auto IS TRUE"
		
	);

	$cc_salesman = DBUtil::getParameterAdmin( "cc_salesman" );
	
	$errorCount = 0;
	while( !$rs->EOF() ){
	
		$supplierOrder 	= new SupplierOrder( $rs->fields( "idorder_supplier" ) );
		$editor 		= new MailTemplateEditor();
			
		$editor->setLanguage( 1 );
		$editor->setTemplate( "DELIVERY_IMMINENT" );
	
		$editor->setUseOrderTags( $supplierOrder->get( "idorder" ) );
		$editor->setUseSupplierOrderTags( $supplierOrder->getId() );
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( 
			$supplierOrder->getCustomer()->getId(), 
			$supplierOrder->getCustomer()->getContact()->getId() 
		);
		$editor->setUseCommercialTags( $supplierOrder->get( "iduser" ) );
	
		$mailer = new htmlMimeMail();
		
		$sender  = "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - ";
		$sender .= $supplierOrder->getSalesman()->get( "firstname" ) . " " . $supplierOrder->getSalesman()->get( "lastname" );
		$sender .= "\" <" . $supplierOrder->getSalesman()->get( "email" ) . ">";
	
		$mailer->setFrom( $sender );
	
		$mailer->setSubject( DEBUG_EMAIL ? "[ DEBUG ] " . $editor->unTagSubject() : $editor->unTagSubject() );
		$mailer->setHtml( $editor->unTagBody() );
	
		$recipients = array( DEBUG_EMAIL ? DEBUG_EMAIL : $supplierOrder->getCustomer()->getContact()->getEmail() );
		
		if( $cc_salesman )
			$recipients[] = User::getInstance()->get( "email" );
			
		if( $mailer->send( $recipients, "smtp" ) ){
			
			$supplierOrder->set( "delivery_mail_date", date( "Y-m-d" ) );
			$supplierOrder->save();
			
		}
		else $errorCount++;

		$rs->MoveNext();
		
	}

	$mailCount = $rs->RecordCount() - $errorCount;
	echo "\n[ " . date( "Y-m-d" ) . " ][ DELIVERY_IMMINENT ] $mailCount courriels envoyés, $errorCount erreur(s)";
	
}

/* ------------------------------------------------------------------------------------------------ */

function sendAvailabilityMails(){

	$rs = DBUtil::query( 
	
		"SELECT idorder_supplier 
		FROM order_supplier 
		WHERE status NOT LIKE 'cancelled'
		AND idorder <> 0
		AND idbuyer <> '" . DBUtil::getParameter( "internal_customer" ) . "'
		AND availability_date NOT LIKE '0000-00-00'
		AND availability_mail_date LIKE '0000-00-00'
		AND delivery_mail_auto IS TRUE"
		
	);
		
	$errorCount = 0;
	while( !$rs->EOF() ){
		
		$supplierOrder 	= new SupplierOrder( $rs->fields( "idorder_supplier" ) );
		$editor 		= new MailTemplateEditor();
			
		$editor->setLanguage( 1 );
		$editor->setTemplate( "ORDER_AVAILABILITY" );
	
		$editor->setUseOrderTags( $supplierOrder->get( "idorder" ) );
		$editor->setUseSupplierOrderTags( $supplierOrder->getId() );
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( 
			$supplierOrder->getCustomer()->getId(), 
			$supplierOrder->getCustomer()->getContact()->getId() 
		);
		$editor->setUseCommercialTags( $supplierOrder->get( "iduser" ) );
	
		$mailer = new htmlMimeMail();
		
		$sender  = "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - ";
		$sender .= $supplierOrder->getSalesman()->get( "firstname" ) . " " . $supplierOrder->getSalesman()->get( "lastname" );
		$sender .= "\" <" . $supplierOrder->getSalesman()->get( "email" ) . ">";
	
		$mailer->setFrom( $sender );
	
		$mailer->setSubject( DEBUG_EMAIL ? "[ DEBUG ] " . $editor->unTagSubject() : $editor->unTagSubject() );
		$mailer->setHtml( $editor->unTagBody() );
	
		if( $mailer->send( array( DEBUG_EMAIL ? DEBUG_EMAIL : $supplierOrder->getCustomer()->getContact()->getEmail() ), "smtp" ) )
			$supplierOrder->set( "availability_mail_date", date( "Y-m-d" ) );
		else $errorCount++;
		
		$supplierOrder->save();
		
		$rs->MoveNext();
		
	}

	$mailCount = $rs->RecordCount() - $errorCount;
	echo "\n[ " . date( "Y-m-d" ) . " ][ ORDER_AVAILABILITY ] $mailCount courriels envoyés, $errorCount erreur(s)";
	
}

/* ------------------------------------------------------------------------------------------------ */

?>