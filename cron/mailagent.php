<?php
include_once (dirname(__FILE__) . "/../script/configdb.php");
include_once (dirname(__FILE__) . "/../objects/classes.php");

User::fakeAuthentification();

$server_mail = "contact@akilae-saas.fr";

//--------------------------------------------------------------------------------------------------------------------------

echo "\n" . date("Y-m-d H:i:s") . "\n";

if (file_exists("$GLOBAL_START_PATH/data/running")) {
	echo "Script déjà en cours\n";
} 
else {

	touch("$GLOBAL_START_PATH/data/running"); //Création d'un fichier signifiant que le script est en cours
	//chmod ("$GLOBAL_START_PATH/data/running", 0755);
	
	set_time_limit(59);

	mailParsedInBox(); //début du script

	unlink("$GLOBAL_START_PATH/data/running");
	

}

//--------------------------------------------------------------------------------------------------------------------------

function mailParsedInBox() {
	include_once (dirname(__FILE__) . "/../objects/mailerobject.php");
	global $GLOBAL_START_URL;

	$now = date("Y-m-d H:i:s");
	$Query = "SELECT * FROM mail_queue WHERE dateenvoi < '$now' AND  statut = 0 order by id asc LIMIT 1";
	$rs = DBUtil::query($Query);

	if ($rs->RecordCount()) {
		$id = $rs->fields['id'];
		$emails = $rs->fields['destinataires'];
		$sujet = $rs->fields['sujet'];
		$contenu = $rs->fields['contenu'];
		$dests = explode(",", $emails);
		$mailType = $rs->fields['type'];
		$fax = $rs->fields['fax'];
		$noTag = $rs->fields['noTag'];

		$Query = "UPDATE mail_queue set statut=1,dateenvoi='$now' where id=$id;";
		DBUtil::query($Query);
		
		if ($fax == "1") {
			//SendFax($id,$sujet,$contenu,$dests,$mailType);
		} else {
			SendMail($id, $sujet, $contenu, $dests, $mailType, $noTag);
		}
	}
}

function SendMail($id, $sujet, $contenu, $dests, $mailType, $noTag) {
	include_once (dirname(__FILE__) . "/../objects/mime_mail/htmlMimeMail.php");
	include_once (dirname(__FILE__) . "/../objects/mailerobject.php");
	include_once (dirname(__FILE__) . "/../objects/classes.php");
	include_once (dirname(__FILE__) . "/../objects/TagsMenu.php");

	global $base, $GLOBAL_START_URL, $GLOBAL_START_PATH, $server_mail;
	
	$sender = DBUtil::getParameterAdmin("mailinglist_sender");
	$sendername = DBUtil::getParameterAdmin("mailinglist_name");

	//---------------------------------boucle pour les mails-------------------------------------
	$error_count = 0;
	$send_count = 0;
	$mail_count = count($dests);
	echo $mail_count;
	for ($i = 0; $i < $mail_count; $i++) {
		$mailer = new htmlMimeMail();
		if ($noTag == 0 || $noTag == "noImport"){
			$Query = "SELECT idbuyer, idcontact FROM contact WHERE mail = '".$dests[$i]."'";
			$rs = DBUtil::query($Query);
			if ($rs->recordcount() > 0){
				$idbuyer = $rs->fields("idbuyer");
				$idcontact = $rs->fields("idcontact");
				$message = untag($contenu, $idbuyer, $idcontact);
			}
			else{
				$message = $contenu;
			}
		}
		else{
			$message = $contenu;
		}
		$mailer->setHtml($message);
		//$mailer->set_message_type( $mailType );
		$mailer->setFrom('"' . $sendername . '" <' . $sender . '>');

		$mailer->setSubject(stripslashes($sujet));

		//$mailer->add_recipient( $dests[$i], $dests[$i] ); 
		

		$result = $mailer->send(array (
			$dests[$i]
		));
		if (!$result)
			$error_count++;
		else
			$send_count++;

	}


}

function SendFax($id, $sujet, $contenu, $dests, $mailType) {
	include_once (dirname(__FILE__) . "/../objects/mime_mail/htmlMimeMail.php");
	include_once (dirname(__FILE__) . "/../objects/mailerobject.php");
	include_once (dirname(__FILE__) . "/../objects/classes.php");

	global $GLOBAL_START_URL, $GLOBAL_START_PATH;

	$_SESSION["mailing_prospect"] = 0;
	$errors = "";
	$now = date("Y-m-d H:i:s");

	$error_count = 0;
	$mail_count = 0;

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			

	$tagProspect = array ();
	$tagClient = array ();
	$idbuyer = array ();
	$idcontact = array ();
	$devisglobal = array ();
	$destFax = array ();

	$i = 0;
	while ($i < count($dests)) {

		//-------------------------------------------------------------Recherche des paramètres par mail-------------------------------------
		//liste des idbuyer		

		if (preg_match("#[0-9]-[0-9]#", $dests[$i])) { //Si c'est une liste de couple idbuyer-idcontact
			$value = explode("-", $dests[$i]);
			$query = "SELECT idcontact,idbuyer,mail,lastname, firstname, faxnumber title FROM contact WHERE idbuyer='$value[0]' and idcontact='$value[1]' LIMIT 1";
		} else { //Sinon c'est une liste d'emails (fichier importé)
			$query = "SELECT idcontact,idbuyer,mail,lastname, firstname, faxnumber title FROM contact WHERE faxnumber='$dest[$i]' LIMIT 1";
		}

		$rs3 = DBUtil::query($query);

		//echo $query;

		if ($rs3 === false) { //insertions de valeurs bidons
			$tagClient[] = array (
				"tagClient:mail",
				"tagClient:lastname",
				"tagClient:firstname",
				"tagClient:title",
				"tagClient:company"
			);
			$tagProspect[] = array (
				"tagProspect:num",
				"tagProspect:date",
				"tagProspect:article",
				"tagProspect:listedevis"
			);
			$idbuyer[] = "0";
			$idcontact[] = "0";
			$devisglobal[] = "0";
			$destFax[] = $dests[$i];
		} else {

			$idbuyerf = $rs3->fields("idbuyer");
			$idcontactf = $rs3->fields("idcontact");
			//recherche des informations clients qui seront ajouté dans le tableau tagClient
			$query2 = "SELECT company FROM buyer WHERE idbuyer ='" . $idbuyerf . "' LIMIT 1";
			$rs2 = DBUtil::query($query2);

			$tagClientMail = $rs3->fields("mail");
			$tagClientLastname = "<span style='font-family: verdana,geneva; font-size: x-small;'>" . $rs3->fields("lastname") . "</span>";
			$tagClientFirstname = $rs3->fields("firstname");
			$tagClientTitle = "<span style='font-family: verdana,geneva; font-size: x-small;'>" . getTitle($rs3->fields("title")) . "</span>";
			$tagClientCompany = $rs2->fields("company");

			//echo $tagClientLastname;
			//echo $tagClientFirstname;

			$tagClient[] = array (
				$tagClientMail,
				$tagClientLastname,
				$tagClientFirstname,
				$tagClientTitle,
				$tagClientCompany
			);
			$idbuyer[] = $idbuyerf;
			$idcontact[] = $idcontactf;

			//Recherche des infos prospects
			$numdevis = "";
			$datedevis = "";
			$articlesdevis = "";
			$listedevis = "";

			//liste des devis et lignes articles				
			$queryesti = "SELECT idestimate, DateHeure FROM estimate WHERE  idbuyer ='" . $idbuyerf . "' AND idcontact='" . $idcontactf . "'AND DateHeure>='2008-01-01 00:00:00' AND status = 'Send' order by idestimate DESC ";
			$rsesti = DBUtil::query($queryesti);
			$datedevis = $rsesti->fields("DateHeure");
			$numdevis = $rsesti->fields("idestimate");
			$devis = array ();

			if ($rsesti->RecordCount() > 0) {
				while (!$rsesti->EOF()) {
					$numest = $rsesti->fields("idestimate");
					$listedevis .= "<span style='font-family: verdana,geneva; font-size: x-small;'>Devis n°" . $numest . ":<br /></span>";

					$queryesti_row = "SELECT quantity,summary, reference FROM estimate_row WHERE idestimate='$numest'";
					$rsesti_row = DBUtil::query($queryesti_row);

					$listedevis .= "<span style='font-family: verdana,geneva; font-size: x-small;'><ul>";
					while (!$rsesti_row->EOF()) {

						$articlesdevis .= $rsesti_row->fields("quantity") . " " . $rsesti_row->fields("summary") . ", ";

						$quantity = $rsesti_row->fields("quantity");
						$reference = $rsesti_row->fields("reference");
						$summary = $rsesti_row->fields("summary");

						$listedevis .= "<li>" . $quantity . " x " . $reference;

						if (!empty ($summary))
							$listedevis .= " : " . $summary;

						$listedevis .= "</li>";

						$rsesti_row->MoveNext();

					}
					$listedevis .= "</ul></span>";

					$rsesti->MoveNext();
				}

				$devis[] = $numest;
				$devisglobal[] = $devis;
				$tagProspect[] = array (
					$numdevis,
					$datedevis,
					$articlesdevis,
					$listedevis
				);
			}
		}

		$i++;
	}

	$lasupdate = $now;
	$sender = DBUtil::getParameterAdmin("mailinglist_sender");
	$sendername = DBUtil::getParameterAdmin("mailinglist_name");

	//---------------------récupération des informations sur l'entreprise--------------------------
	$tagEntreprise = array ();
	$select = "'ad_ape','ad_adress','ad_company','ad_fax','ad_firstname','ad_logo','ad_name','ad_zipcode','ad_city','ad_state','ad_tel','ad_mail','ad_site','ad_sitename','ad_siren','ad_siret','ad_tva-intra'";
	$query = "SELECT idparameter, paramvalue FROM parameter_admin WHERE idparameter IN ( $select )";

	$rs2 = DBUtil::query($query);

	if ($rs2 === false)
		die("Impossible de récupérer les marqueurs administrateurs");

	while (!$rs2->EOF()) {

		$idparameter = $rs2->fields("idparameter");
		$value = $rs2->fields("paramvalue");

		if ($idparameter == "ad_logo")
			$value = "<img src=\"$GLOBAL_START_URL" . $value . "\" alt=\"\" />";

		if ($idparameter == "ad_sitename")
			$value = "<a href=\"$GLOBAL_START_URL\">" . $value . "</a>";

		$tagEntreprise[] = array (
			$idparameter,
			$value
		);
		$rs2->MoveNext();

	}

	//---------------------------------boucle pour les mails-------------------------------------

	$send_count = 0;
	$mail_count = count($destFax);
	for ($i = 0; $i < $mail_count; $i++) {

		$mailer = new htmlMimeMail();

		$smtp_server = DBUtil::getParameterAdmin('fax_smtp_server');
		$smtp_sender = DBUtil::getParameterAdmin('fax_smtp_sender');
		$smtp_auth = DBUtil::getParameterAdmin('fax_smtp_auth');
		$smtp_user = DBUtil::getParameterAdmin('fax_smtp_user');
		$smtp_password = DBUtil::getParameterAdmin('fax_smtp_password');
		$smtp_recipient = DBUtil::getParameterAdmin('fax_smtp_recipient');
		$AdminName = DBUtil::getParameterAdmin("ad_name");

		$mailer->setSMTPParams($smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password);

		$mailer->setFrom('"' . $AdminName . '" <' . $smtp_user . '>');

		//Si c'est un mail de test, alors on interprete pas les tags clients
		$message = Taginterpret($contenu, $i, $tagClient, $tagEntreprise, $tagProspect);
		$mailer->setHtml($message);

		$subject = TagSubjectinterpret($sujet, $i, $tagClient, $tagEntreprise, $tagProspect);

		$faxnumber = $destFax[$i];

		$fax = getFaxNumber($faxnumber);

		$mailer->setSubject($fax);

		//PDF

		if ($_SESSION["mailing_prospect"] == 1) {
			if ($devisglobal[$i] != 0) {

				if (is_array($devisglobal[$i])) {

					$list = $devisglobal[$i];

					for ($nb = 0; $nb < count($list); $nb++) {

						$IdEstimate = $list[$nb];

						//On ajoute le PDF du devis
						$pdf_name = "Devis_$IdEstimate.pdf";
						
						include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );
						$odf = new ODFEstimate( $IdEstimate );
						
						$mailer->addAttachment($odf->convert( ODFDocument::$DOCUMENT_PDF ), $pdf_name, 'application/pdf');

					}
				}

			}
		}

		$result = $mailer->send(array (
			$smtp_recipient,
			"supportpro@alinto.net"
		));

		if (!$result) {
			$error_count++;
			echo $result;
		} else
			$send_count++;

	}

	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////			

	//infos pour l'admin

	$end_date = date("d/m/Y à H:i:s");
	$msg = "<p>Nombre de faxs (id:$id) envoyés : $send_count sur $mail_count</p><p>smtp : $smtp_recipient</p><p>fax : $fax</p>";

	$mailer = new EVM_Mailer();

	$subject = "mailing $id du $now ($sujet)";

	$mailer->set_message($msg);
	$mailer->set_message_type("html");
	$mailer->set_sender("mailagent", $sender);
	$mailer->set_subject($sujet);
	$mailer->add_recipient("gromhm@hotmail.com", "gromhm@hotmail.com");
	echo $msg . "\n";
	$mailer->send();
}



function getTitle($idtitle) {

	$query = "SELECT title_1 FROM title WHERE idtitle = '$idtitle'";
	$rs = DBUtil::query($query);

	if ($rs->Recordcount() > 0)
		return $rs->fields("title_1");
	else
		return "";
}
?>