<?php

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );

if( DBUtil::query( "SELECT date FROM deb_declaration WHERE date LIKE '" . date( "Y-m" ) . "' LIMIT 1" )->RecordCount() )
	exit();
	
ob_start();

?>
<p>Pensez à effectuer votre Déclaration d'Echange de Bien pour le mois de <?php echo strftime( "%B", mktime( 0, 0, 0, date( "m" ) - 1, 1, date( "Y" ) ) ) . " " . date( "Y" ); ?></p>
<p>
	<a href="<?php echo URLFactory::getHostURL(); ?>/accounting/deb.php?month=<?php echo date( "Y" ) . "-" . strftime( "%B", mktime( 0, 0, 0, date( "m" ) - 1, 1, date( "Y" ) ) ); ?>">
		Cliquez ici pour récupérer votre fichier de déclaration du mois dernier
	</a>
</p>
<?php

$mailer = new htmlMimeMail();

$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - noreply\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
$mailer->setSubject( "Votre Déclaration d'Echange de Biens" );

$mailer->setHtml( ob_get_contents() );
ob_end_clean();

$mailer->send( array( DBUtil::getDBValue( "email", "user", "iduser", DBUtil::getParameter( "contact_commercial" ) ) ) );

?>