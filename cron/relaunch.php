<?php 

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );

User::fakeAuthentification();

$base = DBUtil::getConnection();

//--------------------------------------------------------------------------------------------------------------------------

echo "\n" . date( "Y-m-d H:i:s" )."\n";

if(file_exists("$GLOBAL_START_PATH/data/running")){
	echo "Script déjà en cours\n";
}
else{
	//Création d'un fichier signifiant que le script est en cours
	touch("$GLOBAL_START_PATH/data/running");
	//time limit d'execution au cas ou ça foire
	set_time_limit( 60 );
	//début du script
	RelaunchEstimates(); 
	//suppression du fichier de test de fonctionnement
	unlink("$GLOBAL_START_PATH/data/running");

}
//--------------------------------------------------------------------------------------------------------------------------


function RelaunchEstimates(){

	$base = DBUtil::getConnection();
	
	$req="SELECT paramvalue FROM parameter_cat
		  WHERE idparameter LIKE 'delay_relaunch_last_estimate'";
	$rsreq=DBUtil::query($req);
	$relaunchdelay=$rsreq->Fields( "paramvalue" );
	
	//Date du jour
	$dateauj = date("Y-m-d");
	
	//fractionner la date en jour, mois, année
	list( $year, $month, $day ) = explode( "-", $dateauj );
	
	//Enlever X jours a la date
	$nvelledateNF = mktime( 0, 0, 0, $month, $day + $relaunchdelay, $year );
	
	//convertir le résultat en date
	$nvelledate = date( "Y-m-d", $nvelledateNF );
	
	//récuperation des devis qui n'ont pas été traités et dont la délai est arrivé
	$Query = "SELECT idestimate, idbuyer, idcontact
			  FROM estimate
			  WHERE valid_until='$nvelledate'
			  AND estimate_last_relaunch='0000-00-00'
			  AND status LIKE 'Send'";	
	$rs =& DBUtil::query($Query);	
	
	if($rs->RecordCount()>=1){
		while( !$rs->EOF() ){
			Envoilemail($rs->fields( 'idestimate' ));
			$rs->MoveNext();
		}
	}
}

//fonction d'envoi des mails un par un puis aller simple pour l'update du devis	
function Envoilemail($idestimate){
	
	global $GLOBAL_START_PATH;
	
	$base = DBUtil::getConnection();
	$Order = new Estimate($idestimate);	
	$mailer =new htmlMimeMail();
	$iduser=$Order->getCustomer()->get("iduser");
	
	$CommercialName = User::getInstance()->get('firstname').' '.User::getInstance()->get('lastname');
	$CommercialEmail = User::getInstance()->get('email');
	
	//Expéditeur
	$mailer->setFrom( '"'.$CommercialName.'" <'.$CommercialEmail.'>');
	
	//charger le modèle de mail
	$editor = new MailTemplateEditor();
	$editor->setTemplate( "RELAUNCH_AUTO" );//==> le nom du template de mail à créer ds la base
	
	//tags à utiliser
	$editor->setUseEstimateTags( $idestimate );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $Order->get( "idbuyer" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
				
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );

	//ajout du pdf
	$pdf_name = "Devis_$idestimate.pdf";
	
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );
	$odf = new ODFEstimate( $idestimate );

	$mailer->addAttachment( $odf->convert( ODFDocument::$DOCUMENT_PDF ),$pdf_name,'application/pdf');
		
	//et c'est partiiiiiii! youhouuuuu!!!
	$ret = $mailer->send(array($Order->getCustomer()->getContact()->get( "mail" )));

	if( $ret == false )
		trigger_error( "Impossible d'envoyer la relance par courriel", E_USER_ERROR );
		
	DBUtil::query( "UPDATE estimate SET estimate_last_relaunch = NOW() WHERE idestimate = '$idestimate' LIMIT 1" );
	
}

?>