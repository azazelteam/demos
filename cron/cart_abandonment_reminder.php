<?php

/**
 * Envoi auto d'une relance d'abandon de panier
 */

define( "DEBUG_EMAIL", "sitraka@shasama.com" ); /* email pour les test */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) ."/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );
include_once( dirname( __FILE__ ) . "/../sales_force/send-reminder.php" );

User::fakeAuthentification();

/* ----------------------------------------------------------------------------------------------------- */
//sender
	
$query = "
SELECT u.email, u.firstname, u.lastname
FROM `user` u, parameter_cat pc 
WHERE pc.idparameter LIKE 'contact_commercial'
AND pc.paramvalue = u.iduser
LIMIT 1";

$rs =& DBUtil::query( $query );

$senderNameDefault 	= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
$senderEmailDefault	= $rs->fields( "email" );

//requette
$query = "
SELECT ca.idcart, ca.idbuyer, ca.date_add, ca.reminder, ca.auto_reminder, ca.reduction, ca.date_add, co.title, co.lastname, co.firstname, co.mail
FROM contact co, cart ca
WHERE ca.status = 0
AND ca.auto_reminder = 1
AND ca.idbuyer = co.idbuyer
AND ((ca.reminder = 0 AND ca.date_add < ".(time() - TIME_DIFF).") OR (ca.reminder = 1 AND ca.date_add < ".(time() - TIME_DIFF - 86400)."))";
$rs =& DBUtil::query( $query );

$reminded = array();
$failed_remind = array();
$datas = array();
while( !$rs->EOF() ){
	switch ($rs->fields['title'])
	{
		case '1' : 
			$title = 'Mr';
			break;
		case '2' :
			$title = 'Mme';
			break;
		case '3' :
			$title = 'Mlle';
			break;
		default :
			$title = '';
			break;
	}
	$data = array(
		'idcart' => $rs->fields['idcart'],
		'title' => $title,
		'lastname' => $rs->fields['lastname'],
		'firstname' => $rs->fields['firstname'],
		'mail' => $rs->fields['mail'],
		'reduction' => $rs->fields['reduction'],
		'date_add' => $rs->fields['date_add'],
		'reminder' => $rs->fields['reminder'],
		'items' => ''
	);
	$it = DBUtil::query( "SELECT idarticle, quantity FROM `cart_detail` WHERE `idcart` = ".$rs->fields['idcart'] );
	while (!$it->EOF())
	{
		$data['items'] .= '<li>'.$it->fields['quantity'].' X '.DBUtil::getDBValue( 'summary_1', 'detail', 'idarticle', $it->fields['idarticle'])."</li>";
		$it->MoveNext();
	}
	//envoi mail
	$send_mail = sendCartAbandonmentMail($data, $senderNameDefault, $senderEmailDefault);
	if ($send_mail)
	{
		$reminder = (int)$rs->fields['reminder'] + 1;
		$update = "UPDATE cart SET `reminder`=".$reminder." WHERE `idcart`=".$rs->fields['idcart'];//mise à jour nombre de relances
		DBUtil::getConnection()->Execute($update);

		array_push( $reminded, $rs->fields( "idcart" ) );
	}
	$datas[] = $data;
	$rs->MoveNext();
	
}
/*
sendReminderReportMail( DEBUG_EMAIL ? DEBUG_EMAIL : $senderNameDefault, $reminded, $datas );

function sendReminderReportMail ($recipient, $reminded, $datas)
{
	$success = '';
	$failed = '';
	if (empty($datas)
		$html = "<p>Aucun panier abandonné pour la relance automatique.</p>";
		// $html = "<p>Cron relance automatique.</p>";
	else
	{
		foreach ($datas as $data)
		{
			if (in_array($data['idcart'], $reminded))
				$success .= '<tr><td style="border:solid 1px #000;padding: 5px;">'.date('d/m/Y', $data['date_add']).'</td>'.'<td style="border:solid 1px #000;padding: 5px;">'.$data['title'].' '.$data['lastname'].' ('.$data['mail'].')</td><td style="border:solid 1px #000;padding: 5px;">'.$data['items'].'</td></tr>';
			else
				$failed .= '<tr><td style="border:solid 1px #000">'.$data['date_add'].'</td>'.'<td style="border:solid 1px #000;padding: 5px;">'.$data['title'].' '.$data['lastname'].' ('.$data['mail'].')</td><td style="border:solid 1px #000;padding: 5px;">'.$data['items'].'</td></tr>';
		}
		if ($success != '')
			$html_success = '<p><h3>Liste de(s) relance(s) éffectuée(s)</h3><table><tr><th style="border:solid 1px #000;padding: 5px;">Date</th><th style="border:solid 1px #000;padding: 5px;">Client</th><th style="border:solid 1px #000;padding: 5px;">Contenu panier</th></tr>'.$success.'</table></p>';
		if ($failed != '')
			$html_failed = '<p><h3>Liste de(s) relance(s) échouée(s)</h3><table><tr><th style="border:solid 1px #000;padding: 5px;">Date</th><th style="border:solid 1px #000;padding: 5px;">Client</th><th style="border:solid 1px #000;padding: 5px;">Contenu panier</th></tr>'.$failed.'</table></p>';
		
		$html = $html_success.$html_failed;
	}
	$mailer = new htmlMimeMail();
	$mailer->setSubject( "Résumé [ Tâche planifiée ] - Relance automatique Abandon de Panier du ".date('d/m/Y H:i:s', time()) );
	$mailer->setHtml($html);
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - noreply\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	$mailer->send( array( $recipient ) );
}*/
