<?php
/*
 * Created on 21 oct. 2006
 * @author Lionel STAHL
 */

include_once( "objects/classes.php" );
include_once( "objects/GoogleMap.php" );

//--------------------------------------------------------------------------------------

//include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div>
        <div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Sitemap généré</p>
                <div class="rightContainer"></div>
            </div>
            <div class="subContent">
				<?php crawl(); ?>
			</div><!-- subContent -->
		</div><!-- content -->
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div><!-- mainContent -->
	<div class="clear"></div>
</div><!-- globalMainContent -->
<?php

//include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//--------------------------------------------------------------------------------------

function crawl(){
 	 
 	 $googlemap = new GoogleMap();
 	 
 	//post data

 	$googlemap->setOutputFile( stripslashes( $_POST[ "outputfile" ] ) );
 	$googlemap->setGetLastModificationMethod( intval( $_POST[ "lastmodification" ] ) );
 	$googlemap->setCategoryChangeFrequency( $_POST[ "categoryChangeFrequency" ] );
 	$googlemap->setProductChangeFrequency( $_POST[ "productChangeFrequency" ] );
 	$googlemap->setPDFChangeFrequency( $_POST[ "pdfChangeFrequency" ] );
 	$googlemap->setStaticPageChangeFrequency( $_POST[ "staticPageChangeFrequency" ] );
 	$googlemap->setIndexPDF( intval( $_POST[ "indexpdf" ] ) ? true : false );
	//$googlemap->clearStaticPages();
	$staticpagesTab=array();

	$m = 0;
	//$tokens=array("/50-consommable-medical-t-3","/bariatrique-XXL-t-2","/definition_pansement_technique.php","/definition_gant_examen.php","/marques.php","/invacare.php","/acime-frame.php","/aks-nausicaa.php","/carpenter.php","/dupont-medical.php","/ecopostural.php","/france-hopital.php","/herdegen.php","/hms-vilgo.php","/innov-sa.php","/kern.php","/medicatlantic-askle.php","/multiroir.php","/navailles.php","/pharmaouest.php","/promotal.php","/praticdose.php","/reval.php","/saint-romain.php","/seca.php","/seim-medical.php","/socamel.php","/sotec.php","/spengler.php","/systam.php","/vermeiren.php","/tournus.php","/AlterEco.php","/Alvi.php","/Anios.php","/Carina.php","/sav_materiel_medical.php");
		while( $m < $googlemap->getStaticPageCount() ){
				//echo "\n";
			array_push($staticpagesTab,$googlemap->getStaticPageAt($m)->getURI()) ;
			//$tokens[$m]=$googlemap->getStaticPageAt( $m)->getURI();

			$m++;
	
		}	

 	//$tokens = explode( "\n", stripslashes( $_POST[ "staticpages" ] ) );
		$tokens =$staticpagesTab;
	/*$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->addStaticPage( trim( $tokens[ $i ] ) );
			
		$i++;
		
	}*/

	//$googlemap->clearBlackLists();

$tokens = $googlemap->getBlackListedCategories();


	//$tokens = explode( "\n", stripslashes( $_POST[ "categoryBlackList" ] ) );
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->blackListCategory( intval( trim( $tokens[ $i ] ) ) );
			
		$i++;
		
	}
	
//$tokens=array(170106,170107,170108,170109,170110,170111,170113,170114,170115,17011,	170127,170128,170129,170130,170131,170132,170134,170135,170136,170137,170138,170139,170140,170141,170142,170145,170146,170925,170873,170874,170875,170876,170877,170878,170879,170880,170881,170882,170883,170884,170885,170886,170887,170888,170889,170890,170891,170892,170893,170894,170895,170896,170897,170898,170899,170900,170901,170902,170903,170904,170905,170906,170907,170908,170909,170910,170911,170912,170913,170914,170915,170916,170917,170918,171021,171038,171023,171040,171025,171042,171027,171029,171031,171033,171035,171020,171037,171022,171039,171024,171041,171026,171028,171030,171032,171034,171036,120035,120036,120037,120038,120039,120040,120041,120042,120043,120044,120045,120046,120047,120048,120049,120050,120051,120052,120053,120053,120054,120055,120056,120057,120058,120060,120061,120062,120063,120064,120065,120066,120067,120068,120069,120070,120071,120072,120073,120074,120075,120076,120077,120078,120079,120080,120081,120082,120083,120084,120085,120086,120087,120088,120089,120090,120091,120092,120093,120094,120095,120096,120097,120098,120099,120100,120101,120102,120103,120104,120105,120106,120107,120108,120109,120110,120111,120112,120113,120114,120115,120116,120117,120118,120119,120120,120121,120122,120123,120124,120125,120126,120127,120128,120129,120130,120131,120132,120133,120134,120135,120136,120137,120138,120139,120140,120141,1201402,120143,120144,120145,120146,120147,120148,120151,120152,120153,120154,120205,120206,120207,120208,120209,120210,120212,120211,120213,270263,120372,170288,170293,170294,170295,170291,170290,120117,170263,170246,170265,170269,170248,170260,170254,120105);
	//$tokens = explode( "\n", stripslashes( $_POST[ "productBlackList" ] ) );
	$tokens =$googlemap->getBlackListedProducts();
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->blackListProduct( intval( trim( $tokens[ $i ] ) ) );
			
		$i++;
		
	}


	//$tokens = explode( "\n", stripslashes( $_POST[ "pdfBlackList" ] ) );

$tokens = $googlemap->getBlackListedPDF();
									
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->blackListPDF( intval( trim( $tokens[ $i ] ) ) );
			
		$i++;
		
	}
 	
	//crawl

	if( isset( $_POST[ "saveConfig" ] ) )
		$googlemap->saveConfig();
	
	$googlemap->crawl();
 	
 	global $GLOBAL_START_URL;
 	
 	if( isset( $_POST[ "saveConfig" ] ) ){
 		
 		?>
 		<p style="font-weight:bold;">Votre configuration a bien été enregistrée.</p>
 		<?php
 			
 	}
 	
	?>
	<p>Nombre de liens ajoutés au sitemap : <b><?php echo $googlemap->getChildCount() ?></b></p>
	<p style="margin-top:15px;">Le sitemap ci-dessous a été enregistré à l'emplacement suivant : <a href="<?php echo $GLOBAL_START_URL . stripslashes( $_POST[ "outputfile" ] ) ?>" onclick="window.open(this.href); return false;"><?php echo $GLOBAL_START_URL . stripslashes( $_POST[ "outputfile" ] ) ?></a></p>
	<textarea style="width:100%; height:350px; margin-top:20px; overflow:auto; background:transparent; border:1px solid #E7E7E7;"><?php echo $googlemap->getXMLDocument() ?></textarea>
	<p style="text-align:center; margin-top:15px;">
		<input type="button" name="" value="Retour" class="blueButton"  onclick="history.back();" />
	</p>
	<?php
 	
 }

 //--------------------------------------------------------------------------------------
 
?>