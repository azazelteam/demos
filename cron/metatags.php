<?php
/**
 * .....
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures fournisseurs
 */

set_time_limit( 3600 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );


updateProductMetaTags();
updateCategoryMetaTags();

//---------------------------------------------------------------------------------------

function updateProductMetaTags(){
	
	$start = 0;
	$limit = 50;
	$done = false;
	
	while( !$done ){
	
		$rs =& DBUtil::query( "SELECT idproduct FROM product WHERE idproduct <> 0 ORDER BY idproduct ASC LIMIT $start,$limit" );
		
		while( !$rs->EOF() ){
			
			$metadesc 		= getProductMetaDescription( $rs->fields( "idproduct" ) );
			$metakeywords 	= getProductMetaKeywords( $rs->fields( "idproduct" ) );
			
			DBUtil::query( "UPDATE product SET meta_description = '" . Util::html_escape( $metadesc ) . "', meta_keywords = '" . Util::html_escape( $metakeywords ) . "' WHERE idproduct = '" . $rs->fields( "idproduct" ) . "' LIMIT 1" );

			$rs->MoveNext();
			
		}

		$done = $rs->RecordCount() == 0;
		$start += $limit;

	}
	
}

//---------------------------------------------------------------------------------------

function updateCategoryMetaTags(){
	
	$start = 0;
	$limit = 50;
	$done = false;
	
	while( !$done ){
	
		$rs =& DBUtil::query( "SELECT idcategory FROM category ORDER BY idcategory ASC LIMIT $start,$limit" );
		
		while( !$rs->EOF() ){
			
			$metadesc 		= getCategoryMetaDescription( $rs->fields( "idcategory" ) );
			$metakeywords 	= getCategoryMetaKeywords( $rs->fields( "idcategory" ) );
			
			DBUtil::query( "UPDATE category SET meta_description = '" . Util::html_escape( $metadesc ) . "', meta_keywords = '" . Util::html_escape( $metakeywords ) . "' WHERE idcategory = '" . $rs->fields( "idcategory" ) . "' LIMIT 1" );

			$rs->MoveNext();
			
		}
		
		$done = $rs->RecordCount() == 0;
		$start += $limit;
			
	}
	
}

/* --------------------------------------------------------------------------------------------- */

function getCategoryMetaDescription( $idcategory ){

	$meta_desc = "";

	$description = DBUtil::query( "SELECT referencing_text_1 FROM category WHERE idcategory = '$idcategory' LIMIT 1" )->fields( "referencing_text_1" );

	if( !empty( $description ) )
		return $description;

	//récupérer le nom de la catégorie

	$name = DBUtil::query( "SELECT name_1 FROM category WHERE idcategory = '$idcategory' LIMIT 1" )->fields( "name_1" );

	$meta_desc .= " $name";
	
	$title_keyword = DBUtil::query( "SELECT title_keyword_1 FROM category WHERE idcategory = '$idcategory' LIMIT 1" )->fields( "title_keyword_1" );
	
	if( !empty( $title_keyword ) )
		$meta_desc .= " ($title_keyword)";
		
	
	$meta_desc .= " : " . Dictionnary::translate( "meta_free_text" );
	$meta_desc .= " : ";
	
	// categrories parentes ou filles tout dépend ou on est
	
	$rs =& DBUtil::query( 
	
		"SELECT c.name_1 as name,c.idcategory 
		FROM category c, category_link cl 
		WHERE cl.idcategorychild = '$idcategory' 
		AND cl.idcategoryparent = c.idcategory LIMIT 1"
		
	);
	
	$p = 0;
	$parent = $rs->fields("idcategory");
	
	if($parent > 0){				
		$meta_desc .= $rs->fields("name");			
		$p++;
	}
	
	if($parent == 0) 
		$parent = $idcategory;
	
	//ensuite on reprend toutes les copines						
	$rs2 = DBUtil::query(

		"SELECT c.name_1 as name,c.idcategory 
		FROM category c, category_link cl 
		WHERE cl.idcategoryparent = $parent 
		AND cl.idcategorychild = c.idcategory"
	
	);

	while(!$rs2->EOF()){
		
		if($p > 0)
			$meta_desc .= ", ";
		
		$meta_desc .= $rs2->fields("name");
		
		$p++;
		$rs2->MoveNext();
		
	}

	return $meta_desc;
		
}

/* --------------------------------------------------------------------------------------------- */

function getProductMetaDescription( $idproduct ){

	$idcategory = DBUtil::getDBValue( "idcategory", "product", "idproduct", $idproduct );
	
	$meta_desc = "";
	
	//récupérer le champ referencing_text_1

	$rs =& DBUtil::query( "SELECT referencing_text_1 FROM category WHERE idcategory = '$idcategory' LIMIT 1" );
	
	$description = $rs->fields( "referencing_text_1" );
	
	if( !empty( $description ) )
		return $description;
		
	//récupérer le nom du produit
	
	$rs =& DBUtil::query( "SELECT name_1 FROM product WHERE idproduct = '$idproduct' LIMIT 1" );
	
	$meta_desc .= $rs->fields( "name_1" );
	
	//récupérer le nom de la catégorie

	$rs =& DBUtil::query( "SELECT name_1 FROM category WHERE idcategory = '$idcategory' LIMIT 1" );
	
	$meta_desc .= $rs->fields( "name_1" );
	
	//ajouter le champ title_keyword_1
	
	$rs =& DBUtil::query( "SELECT title_keyword_1 FROM category WHERE idcategory = '$idcategory' LIMIT 1" );
	
	$title_keyword = $rs->fields( "title_keyword_1" );
	if( !empty( $title_keyword ) )
		$meta_desc .= " ($title_keyword)";
	
	$meta_desc .= " : ".Dictionnary::translate( "meta_free_text" );
	$meta_desc .= " : ";
	
	//récupérer les noms des parents
	
	$rs =& DBUtil::query( "SELECT name_1, idcategory AS idparent FROM product WHERE idproduct = '$idproduct'" );	

	$j = 0;
	while( $rs->fields( "idparent" ) ){
	
		$query = "
		SELECT cat.name_1 AS parentname, catlink.idcategoryparent AS idparent 
		FROM category cat, category_link catlink 
		WHERE cat.idcategory = " . $rs->Fields( "idparent" ) . " 
		AND catlink.idcategorychild = " . $rs->Fields( "idparent" );
			
		$rs =& DBUtil::query( $query );
		
		if( $rs !== false  && strlen( $rs->fields( "parentname" ) ) )
			
			if( $j )
				$meta_desc .= ", ";
				
			$meta_desc .= $rs->fields( "parentname" );
		
		$j++;
		
	}

	// categrories parentes ou filles tout dépend ou on est
	
	$rs =& DBUtil::query("	SELECT c.name_1 as name,c.idcategory 
							FROM category c, category_link cl 
							WHERE cl.idcategorychild = '$idcategory' 
							AND cl.idcategoryparent = c.idcategory LIMIT 1");
	$ziz = 0;
	
	$parent = $rs->fields("idcategory");
	if($parent > 0){				
		$meta_desc .= $rs->fields("name");			
		$ziz++;
	}
	
	if($parent == 0) $parent = $idcategory;
	
	//ensuite on reprend toutes les copines						
	$rs2 = DBUtil::query("	SELECT c.name_1 as name,c.idcategory 
							FROM category c, category_link cl 
							WHERE cl.idcategoryparent = $parent 
							AND cl.idcategorychild = c.idcategory");
	
	while(!$rs2->EOF()){
		if($ziz > 0)
			$meta_desc .= ", ";
		
		$meta_desc .= $rs2->fields("name");
		
		$ziz++;
		$rs2->MoveNext();
	}
			
	

	return $meta_desc;
	
}

/* --------------------------------------------------------------------------------------------- */

function getProductMetaKeywords( $idproduct ){

	$lang = "_1";
	
	$path = array();
	
	//name and idparent
	
	$rs =& DBUtil::query( "SELECT name$lang AS name, idcategory AS idparent FROM product WHERE idproduct = '$idproduct'" );	

	if( strlen( $rs->Fields( "name" ) ) )
		$path[] = $rs->Fields( "name" );

	while( strlen( $rs->Fields( "idparent" ) ) ){
		
		$query = "SELECT cat.name$lang AS parentname, catlink.idcategoryparent AS idparent FROM category cat, category_link catlink WHERE cat.idcategory = " . $rs->Fields( "idparent" ) . " AND catlink.idcategorychild = " . $rs->Fields( "idparent" );
			
		$rs =& DBUtil::query( $query );
		
		if( strlen( $rs->Fields( "parentname" ) ) )
			$path[] = $rs->Fields( "parentname" );
		
	}
	
	$ret = "";
	$i = 0;
	while( $i < count( $path ) ){
		
		if( $i )
			$ret .= ", ";
		
		$ret .= $path[ $i ];
		
		$i++;
		
	}
	
	if( $i )
		$ret .= ", ";
	
	$ret .= DBUtil::getParameterAdmin( "ad_sitename" );
	
	return $ret;
	
}

/* --------------------------------------------------------------------------------------------- */

function getCategoryMetaKeywords( $idcategory ){

	$lang = "_1";
	
	$path = array();
	
	//name and idparent

	$rs =& DBUtil::query( "SELECT cat.name$lang AS name, catlink.idcategoryparent AS idparent FROM category cat, category_link catlink WHERE cat.idcategory = '$idcategory' AND catlink.idcategorychild = '$idcategory'" );	

	if( strlen( $rs->Fields( "name" ) ) )
		$path[] = $rs->Fields( "name" );

	while( strlen( $rs->Fields( "idparent" ) ) ){
		
		$query = "SELECT cat.name$lang AS parentname, catlink.idcategoryparent AS idparent FROM category cat, category_link catlink WHERE cat.idcategory = " . $rs->Fields( "idparent" ) . " AND catlink.idcategorychild = " . $rs->Fields( "idparent" );
			
		$rs =& DBUtil::query( $query );
		
		if( strlen( $rs->Fields( "parentname" ) ) )
			$path[] = $rs->Fields( "parentname" );
		
	}
	
	$ret = "";
	$i = 0;
	while( $i < count( $path ) ){
		
		if( $i )
			$ret .= ", ";
		
		$ret .= $path[ $i ];
		
		$i++;
		
	}
	
	if( $i )
		$ret .= ", ";
	
	$ret .= DBUtil::getParameterAdmin( "ad_sitename" );
	
	return $ret;
	
}

/* --------------------------------------------------------------------------------------------- */

?>