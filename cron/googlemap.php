<?php
/**
 * Générateur Googlemap
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures fournisseurs
 */

include_once( "../config/init.php" );
include_once( "$GLOBAL_START_PATH/objects/GoogleMap.php" );

$googlemap = new GoogleMap();
$googlemap->crawl();

echo "\n$GLOBAL_START_URL : " . date( "Y-m-d H:i:s" ) . " : " . $googlemap->getChildCount() . " nodes created";

?>