<?php 
/**
 * Cron éxecuté toute les 5 minutes.
 * Chargé de vérifier si un accusé de réception OU un accusé de lecture à été recu lors de l'envoi 
 *  - d'un devis, 
 *  - d'une commande
 *	- d'une facture
 *
 * Si un des accusé est intercepté, le champs delivery_notification (accusé de lecture) OU return_receipt (accusée de reception)
 * est mis à jour !
 */

/* Etapes 
 * - Est ce que l'accusé de reception est ACTIF ?
 * - Est ce que l'accusé de lecture est ACTIF ?
 * 	- Récupérer les confirmations
 *  - Récupérer le N° de Devis ou de Commande
 *  - Est ce que la confirmation est envoyé par le client (et non les commerciaux) ?
 *  	- Si OUI, on met a jour les champs ack_mail_notification OU ack_mail_receipt
 *  	- On supprime les mails ....
 */

include_once( dirname( __FILE__ ) . '/../config/init.php' );
include_once( "$GLOBAL_START_PATH/objects/ToolsForRobots.php" );

//header('Content-Type: text/html; charset=UTF-8'); 

// Verification si le suivi des mails est activé
if( DBUtil::getParameterAdmin( "use_return_receipt_to" ) == 0 && DBUtil::getParameterAdmin( "use_delivery_notification" ) == 0 ){
	die("Le suivi des devis et commandes par mail est désactivé !\r\n");
}else{
	parseNotifications(); 
	parseSpamNotification();
}
	

	
/********************************************************** FONCTIONS *********************************************************/

/**
 * Fonction principale
 * @param array() $mailList
 */
function parseNotifications(){
	
	// Récupérer tous les mails
	$listOfMail = getMails();
	$nbrRecord 	= count($listOfMail);
	
	if($nbrRecord == 0){
		echo("Aucun mail ... dans la boite normal \r\n");
	}
		
	for( $i = 0 ; $i < $nbrRecord ; $i++ ){
		
		// Récupérer le type de mail (accusé commande ou devis)
		$type 	= typeOfMailNotification( $listOfMail[$i]['subject'] );
		if($type != false){
			// On recupère le numéro de commande ou de devis !
			$itemNumber = getNumber( $listOfMail[$i]['subject'], $type );
			echo "ITEM-NUMBER = ".$itemNumber." SUJET =".$listOfMail[$i]['subject']." \r\n" ; 
		}
		
		if( isset($itemNumber) && $itemNumber != false ){
			// DEBUG	
			echo "$type n° ".$itemNumber."\n" ;
			$mailBuyer = getBuyerMail($itemNumber, $type) ; 
			
			if($mailBuyer == $listOfMail[$i]['from']){
				updateStatus($itemNumber, $type);
			}
		
		}
	}

	return true ; 	
}

function parseSpamNotification(){
	echo "-----SPAMBOX ".date("l jS \of F Y h:i:s A")."-----\r\n";
	echo "Parsage de la boite mail (SPAM) en cours ...\r\n";
	// Récupérer tous les mails
	$listOfMail = getMails("spambox");
	
	$nbrRecord 	= count($listOfMail);
	
	if($nbrRecord == 0){
		echo "Aucun mail dans la boite SPAM ...\r\n";
	}
	echo $nbrRecord." mail(s) recu dans la boite SPAM\r\n";
	
	
	for( $i = 0 ; $i < $nbrRecord ; $i++ ){
		
		// Récupérer le type de mail (accusé commande ou devis)
		$type 	= typeOfMailNotification( $listOfMail[$i]['subject'] );
		echo "I = ".$i." SUJET = ".$listOfMail[$i]['subject']."TYPE = ".$type."\r\n" ; 
		if($type != false){
			// On recupère le numéro de commande ou de devis !
			$itemNumber = getNumber( $listOfMail[$i]['subject'], $type );
			echo "ITEM-NUMBER = ".$itemNumber." SUJET =".$listOfMail[$i]['subject']." \r\n" ; 
		}
		
		if( isset($itemNumber) && $itemNumber != false ){
			// DEBUG	
			echo "$type N ".$itemNumber."\r\n" ;
			$mailBuyer = getBuyerMail($itemNumber, $type) ; 
			
			if($mailBuyer == $listOfMail[$i]['from']){
				updateStatus($itemNumber, $type);
			}
		
		}
	}

	return true ; 	
}

/**
 * Met à jour le devis ou la commande
 * @param int $id ID du devis ou commande
 * @param string $typeReception --> Le type de réception, est ce une notification de lecture ou un accusé de récéption
 * @param string $typeMail --> Le type de mail envoyé, une commande ou un devis 
 * @return boolean
 */
function updateStatus($id, $typeMail, $typeReception = null){
	
	if($typeMail == 'estimate'){
		$query = "UPDATE estimate SET status_return_receipt = 1, status_delivery_notification = 1 WHERE idestimate = ".$id ;
	}elseif($typeMail == 'order'){
		$query = "UPDATE `order` SET status_return_receipt = 1, status_delivery_notification = 1 WHERE `idorder` = ".$id ;
	}
	
	$rs = DBUtil::query($query);
	
	if($rs === false){
		return false ;
	}
	echo "Mise à jour du status pour le $typeMail n° $id \n\n" ; 
	return true ;
}


/**
 * Récupère l'adresse mail à laquelle le devis ou la commande à été envoyé(e)
 * @param int $id de la commande ou du devis
 * @param string $type commande ou devis
 * @return string l'adresse email
 */
function getBuyerMail($id, $type){
	// DEBUG
	
	if( $type == false )
		die("TRACE ERREUR --> getBuyerMail() : Impossible de récupérer l'email d'envoi du devis ou de la commande.") ; 
	
	if( $type == "estimate"){
		$queryIdsBuyer = "SELECT idbuyer, idcontact FROM estimate WHERE idestimate = ".$id ;
	}
	else{
		// $type = Order
		$queryIdsBuyer = "SELECT idbuyer, idcontact FROM `order` WHERE idorder = ".$id ;
	}
	
	$rs = DBUtil::query($queryIdsBuyer) ;
	
	if( !$rs->RecordCount() )
		return "";
		
	$queryMail = "SELECT mail FROM contact WHERE idbuyer = ".$rs->fields('idbuyer')." AND idcontact = ".$rs->fields('idcontact');
	$rsMail = DBUtil::query($queryMail);
	$mail = $rsMail->fields('mail');
	
	echo "Récupération de l'email du buyer ($mail)\n" ; 
	
	return $mail ; 
}

/**
 * Est-ce une confirmation de devis / de commandes
 * @param string $subject l'objet du mail.
 * @return string estimate || order
 */
function typeOfMailNotification($subject){
	
	if( substr_count($subject, "devis") || substr_count($subject, "Devis") || substr_count($subject, "DEVIS") ){
		return "estimate" ;
	}elseif( substr_count($subject, "commande") || substr_count($subject, "Commande") || substr_count($subject, "COMMANDE") ){
		return "order" ;
	}else{
		return false ; 
	}
}

/**
 * Recupère l'id du devis ou de la commande
 * @param string $subject l'objet du mail.
 * @return integer --> Le numéro de Commandes ou de Devis 
 */
function getNumber($subject, $type){
	
	if( $type == 'estimate'){
		preg_match("'[0-9]{5}'" ,$subject,$out);
	}
	else{
		preg_match("'[0-9]{4}'" ,$subject,$out);
	}

	echo "Contenue de OUT[0] -->\r\n";
	
	if(!is_numeric($out[0])){
		echo "Aucun numéro de devis / commande trouvé !\n";
		return false ; 
	}else{
		echo "N° de devis / commande $out[0] \n";
		return $out[0] ; 
	}
}

/**
 * Initialise la connection a BAL
 * @return $mbox 	--> La connection à la BAL
 */
function getConnectionToMailBox(){

	$user 		= DBUtil::getParameterAdmin( "mail_delivery_notification" );
	$password 	= DBUtil::getParameterAdmin( "password_delivery_notification" );
	$host 		= 'mail.akilae-saas.fr' ;
	
	$mbox = imap_open('{'.$host.':143/imap/notls}', $user, $password) or die(imap_last_error()."<br>Connection failure!");

	return $mbox;
}
/**
 * Il se peut que des retours passe dans les spams
 * @return unknown_type
 */
function getConnectionToSpamMailBox(){
	$user 		= 'spamtrap@.....com';
	$password 	= 'spamtrap';
	$host 		= 'mail.akilae-saas.fr' ;
	
	$mbox = imap_open('{'.$host.':143/imap/notls}', $user, $password) or die(imap_last_error()."<br>Connection failure!");

	return $mbox;
}

/**
 * @return array() 	--> L'enssemble des mails recu sur la BAL
 */
function getMails($box = null){
	
	if($box == "spambox"){
		$mbox = getConnectionToSpamMailBox(); 
	}else{
		$mbox = getConnectionToMailBox();
	}
	
	
	$nMessage = imap_num_msg($mbox);
 	
 	if($nMessage != 0 || $nMessage != false ){
 		$k = 0 ;
 	
		for ($index=1; $index <= $nMessage; $index++)
		{
			$header = imap_headerinfo( $mbox, $index );
			$fromHeader = $header->sender ;
			$from = $fromHeader[0]->mailbox.'@'.$fromHeader[0]->host ;
			
			if( !substr_count($from, 'akilae')){
					$mailList[$k]['from'] 		= $from;
					if($box == "spambox"){
						$mailList[$k]['subject'] 	= recode_utf8( $header->subject ) ; 
						echo $mailList[$k]['subject'] ;
					}else{
						$mailList[$k]['subject'] 	= imap_utf8($header->subject);
					}
					
					$mailList[$k]['messageId'] 	= $index;
					$mailList[$k]['body'] 		= imap_fetchbody($mbox, $headerValue[ $index ], 1);
					$k++ ;
			}
			// Supression du mail
			deleteMail($mbox);
		}
 	}
 	else{
 		$mailList = false ;
 	}
 	 	
	return $mailList ;
}

/**
 * Supprime un message dans la BAL
 * @param $mbox			--> Connexion à la BAL
 * @param $messageId	--> L'id du message à supprimer
 * @return unknown_type	--> BOOLEAN, True / False
 */
function deleteMail($mbox, $messageId = null){

	// On marque le message comme supprimée
	//imap_delete($mbox, $messageId) ;
	imap_delete($mbox, 1 );
	// On supprime le message
	$ret = imap_expunge($mbox);
	
	return true ;
}

/**
 * Ferme la connection à la BAL
 * @param $mbox
 * @return unknown_type
 */
function closeMailBox( $mbox ){
		
	imap_close($mbox) ;
	return true ;
}
/**
 * 
 * @param unknown_type $str
 * @return unknown_type
 */
function recode_utf8($str) {
   preg_match_all("/=\?UTF-8\?B\?([^\?]+)\?=/i",$str, $arr);
       
   for ($i=0;$i<count($arr[1]);$i++){
     $str=ereg_replace(ereg_replace("\?","\?",
              $arr[0][$i]),base64_decode($arr[1][$i]),$str);
   }
        return $str;
}
?>