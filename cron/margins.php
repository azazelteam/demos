<?php

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/TradeUtil.php" );

echo "\n" . date( "Y-m-d H:i:s" ) . " [" . $_SERVER[ "DOCUMENT_ROOT" ] . " ]";

updateEstimates();
updateOrders();
updateInvoices();

/* -------------------------------------------------------------------------------------------------------------- */

function updateEstimates(){

	$rs =& DBUtil::query( "SELECT `idestimate` FROM `estimate` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateEstimate( $rs->fields( "idestimate" ) );
		$rs->MoveNext();
		
	}
	
	$rs =& DBUtil::query( "SELECT `idestimate` FROM `estimate_row` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateEstimate( $rs->fields( "idestimate" ) );
		$rs->MoveNext();
		
	}
	
	$rs =& DBUtil::query( "SELECT `idestimate` FROM `estimate_charges` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateEstimate( $rs->fields( "idestimate" ) );
		$rs->MoveNext();
		
	}

}

/* -------------------------------------------------------------------------------------------------------------- */

function updateOrders(){

	$rs =& DBUtil::query( "SELECT `idorder` FROM `order` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateOrder( $rs->fields( "idorder" ) );
		$rs->MoveNext();
		
	}
	
	$rs =& DBUtil::query( "SELECT `idorder` FROM `order_row` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateOrder( $rs->fields( "idorder" ) );
		$rs->MoveNext();
		
	}
	
	$rs =& DBUtil::query( "SELECT `idorder` FROM `order_charges` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateOrder( $rs->fields( "idorder" ) );
		$rs->MoveNext();
		
	}
	
}

/* -------------------------------------------------------------------------------------------------------------- */

function updateInvoices(){
	
	$rs =& DBUtil::query( "SELECT `idbilling_buyer` FROM `billing_buyer` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateInvoice( $rs->fields( "idbilling_buyer" ) );
		$rs->MoveNext();
		
	}
	
	$rs =& DBUtil::query( "SELECT `idbilling_buyer` FROM `billing_buyer_row` WHERE updated <> '0000-00-00 00:00:00'" );
	
	while( !$rs->EOF() ){
		
		updateInvoice( $rs->fields( "idbilling_buyer" ) );
		$rs->MoveNext();
		
	}
	
}

/* -------------------------------------------------------------------------------------------------------------- */

function updateEstimate( $idestimate ){
	
	DBUtil::query(
	
		"UPDATE estimate SET 
		net_margin_amount = '" . TradeUtil::getEstimateNetMarginAmount( $idestimate ) . "',
		net_margin_rate = '" . TradeUtil::getEstimateNetMarginRate( $idestimate ) . "',
		gross_margin_amount = '" . TradeUtil::getEstimateGrossMarginAmount( $idestimate ) . "',
		gross_margin_rate = '" . TradeUtil::getEstimateGrossMarginRate( $idestimate ) . "',
		updated = '0000-00-00 00:00:00'
		WHERE idestimate = '$idestimate'
		LIMIT 1"
	
	);
	
	DBUtil::query( "UPDATE estimate_row SET updated = '0000-00-00 00:00:00' WHERE idestimate = '$idestimate'" );
	DBUtil::query( "UPDATE estimate_charges SET updated = '0000-00-00 00:00:00' WHERE idestimate = '$idestimate'" );
	
}

/* -------------------------------------------------------------------------------------------------------------- */

function updateOrder( $idorder ){
	
	DBUtil::query(
	
		"UPDATE `order` SET 
		net_margin_amount = '" . TradeUtil::getOrderNetMarginAmount( $idorder ) . "',
		net_margin_rate = '" . TradeUtil::getOrderNetMarginRate( $idorder ) . "',
		gross_margin_amount = '" . TradeUtil::getOrderGrossMarginAmount( $idorder ) . "',
		gross_margin_rate = '" . TradeUtil::getOrderGrossMarginRate( $idorder ) . "',
		updated = '0000-00-00 00:00:00'
		WHERE idorder = '$idorder'
		LIMIT 1"
	
	);
	
	DBUtil::query( "UPDATE order_row SET updated = '0000-00-00 00:00:00' WHERE idorder = '$idorder'" );
	DBUtil::query( "UPDATE order_charges SET updated = '0000-00-00 00:00:00' WHERE idorder = '$idorder'" );
	
}

/* -------------------------------------------------------------------------------------------------------------- */

function updateInvoice( $idbilling_buyer ){
	
	DBUtil::query(
	
		"UPDATE `billing_buyer` SET 
		net_margin_amount = '" . TradeUtil::getInvoiceNetMarginAmount( $idbilling_buyer ) . "',
		net_margin_rate = '" . TradeUtil::getInvoiceNetMarginRate( $idbilling_buyer ) . "',
		gross_margin_amount = '" . TradeUtil::getInvoiceGrossMarginAmount( $idbilling_buyer ) . "',
		gross_margin_rate = '" . TradeUtil::getInvoiceGrossMarginRate( $idbilling_buyer ) . "',
		updated = '0000-00-00 00:00:00'
		WHERE idbilling_buyer = '$idbilling_buyer'
		LIMIT 1"
	
	);
	
	DBUtil::query( "UPDATE billing_buyer_row SET updated = '0000-00-00 00:00:00' WHERE idbilling_buyer = '$idbilling_buyer'" );
	
}

/* -------------------------------------------------------------------------------------------------------------- */

?>