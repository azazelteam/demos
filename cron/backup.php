<?php

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../objects/MySQLDump.php" );

//------------------------------------------------------------------------------
//sauvegarde des donn�es

deleteOutOfDateFiles();
backupDatabase();
backupStatistics();

//------------------------------------------------------------------------------

function deleteOutOfDateFiles(){

	global 
			$GLOBAL_START_PATH;
	
	$directories = array( 

		"$GLOBAL_START_PATH/backups/data" => DBUtil::getParameterAdmin( "data_backup_availability" ), 
		"$GLOBAL_START_PATH/backups/stats" => DBUtil::getParameterAdmin( "statistics_backup_availability" )
		
	);

	foreach( $directories as $directory => $availability ){
		
		$dp = opendir( $directory );
		
		if( !is_resource( $dp ) )
			die( "Impossible d'acc�der au r�pertoire '$directory'" );
			
		while( $file = readdir( $dp ) ){

			if( $file != "." && $file != ".." && !is_dir( $file ) ){
			
				$path = "$directory/$file";
				
				$filetime = filemtime( $path );
				
				if( isFileOutOfDate( $filetime, $availability ) ){
					
					echo "\n[ ? ] $file IS OUT OF DATE AND WILL BE DELETED";
					@unlink( $path );
					
				}
					
			}
			
		}
		
	}
	
}

//------------------------------------------------------------------------------

function backupDatabase(){
	
	global 
			$GLOBAL_START_PATH,
			$GLOBAL_DB_USER,
			$GLOBAL_DB_PASS,
			$GLOBAL_DB_NAME,
			$GLOBAL_BACKUP_PATH;
			
	$data_backup_auto = DBUtil::getParameterAdmin( "data_backup_auto" );
	$data_backup_frequency = DBUtil::getParameterAdmin( "data_backup_frequency" );

	if( !$data_backup_auto )
		return;
		
	$backupNow = false;
	
	switch( $data_backup_frequency ){
		
		case "hourly" :
		
			if( date( "i" ) == "00" )
				$backupNow = true;
			
			break;
				
		case "daily" :
		
			if( date( "G" ) == 0 )
				$backupNow = true;
			
			break;
	
		case "weekly" :
		
			if( date( "w" ) == 1 )
				$backupNow = true;
			
			break;
			
		case "monthly" :
		
			if( date( "j" ) == 1 )
				$backupNow = true;
			
			break;
			
		case "yearly" :
		
			if( date( "j" ) == 1 && date( "n" ) == 1 )
				$backupNow = true;
			
			break;
			
	}
	
	if( !$backupNow )
		return;
	
	$tables = getDBTables();
	if( $tables === false )
		die( "Impossible de r�cup�rer la liste des tables � sauvegarder" );
		
	$mysqlDump = new MySQLDump();
	
	$mysqlDump->setOutputDirectory( "$GLOBAL_START_PATH/backups/data" );
	$mysqlDump->setOutputFile( "$GLOBAL_DB_NAME-" . date( "Ymd" ) );
	$mysqlDump->setGunzipFile( true );
	$mysqlDump->setUseCompleteInsert( false );
	$mysqlDump->setUseExtendedInsert( true );
	
	foreach( $tables as $table )
		$mysqlDump->addTable( $table );
		
	$mysqlDump->connect( "localhost", $GLOBAL_DB_USER, $GLOBAL_DB_PASS, $GLOBAL_DB_NAME );
	$mysqlDump->dump();
	$mysqlDump->disconnect();
	
}

//------------------------------------------------------------------------------

function backupStatistics(){
	
	global 
			$GLOBAL_START_PATH,
			$GLOBAL_DB_USER,
			$GLOBAL_DB_PASS,
			$GLOBAL_DB_NAME,
			$GLOBAL_BACKUP_PATH;
			
	$statistics_backup_auto = DBUtil::getParameterAdmin( "statistics_backup_auto" );
	$statistics_backup_frequency = DBUtil::getParameterAdmin( "statistics_backup_frequency" );

	if( !$statistics_backup_auto )
		return;
		
	$backupNow = false;
	
	switch( $statistics_backup_frequency ){
		
		case "hourly" :
		
			if( date( "i" ) == "00" )
				$backupNow = true;
			
			break;
				
		case "daily" :
		
			if( date( "G" ) == 0 )
				$backupNow = true;
			
			break;
	
		case "weekly" :
		
			if( date( "w" ) == 1 )
				$backupNow = true;
			
			break;
			
		case "monthly" :
		
			if( date( "j" ) == 1 )
				$backupNow = true;
			
			break;
			
		case "yearly" :
		
			if( date( "j" ) == 1 && date( "n" ) == 1 )
				$backupNow = true;
			
			break;
			
	}
	
	if( !$backupNow )
		return;
	
	$tables = getStatTables();
	if( $tables === false )
		die( "Impossible de r�cup�rer la liste des tables de statistiques � sauvegarder" );
		
	$mysqlDump = new MySQLDump();
	
	$mysqlDump->setOutputDirectory( "$GLOBAL_START_PATH/backups/stats" );
	$mysqlDump->setOutputFile( "stats-" . date( "Ymd" ) );
	$mysqlDump->setGunzipFile( true );
	$mysqlDump->setUseCompleteInsert( false );
	$mysqlDump->setUseExtendedInsert( true );
	
	foreach( $tables as $table )
		$mysqlDump->addTable( $table );
		
	$mysqlDump->connect( "localhost", $GLOBAL_DB_USER, $GLOBAL_DB_PASS, $GLOBAL_DB_NAME );
	$mysqlDump->dump();
	$mysqlDump->disconnect();
	
}

//------------------------------------------------------------------------------

function getDBTables(){
	
	
	
	$db =& DBUtil::getConnection();
	
	$query = "SHOW TABLE STATUS";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer la liste des tables" );
	
	$pattern = "^stat[[:digit:]]{6}\$";
	
	$tables = array();	
	while( !$rs->EOF() ){
		
		$tablename = $rs->fields( "Name" );

		if( !ereg( $pattern, $tablename ) && $tablename != "stat_ip" )
			$tables[] = $tablename;
			
		$rs->MoveNext();
			
	}
	
	return count( $tables ) ? $tables : false;
	
}

//------------------------------------------------------------------------------

function getStatTables(){
	
	global 	$GLOBAL_DB_NAME;
	
	$db =& DBUtil::getConnection();
	
	$query = "SHOW TABLES LIKE 'stat______'";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de r�cup�rer la liste des tables de statistiques" );
	
	$pattern = "^stat[[:digit:]]{6}\$";
	$key = "Tables_in_$GLOBAL_DB_NAME (stat______)";
	
	$tables = array();
	while( !$rs->EOF() ){

		if( ereg( $pattern, $rs->fields( $key ) ) )
			$tables[] = $rs->fields( $key );
		
		$rs->MoveNext();
		
	}
	
	return $tables;
	
}

//------------------------------------------------------------------------------

function isFileOutOfDate( $filetime, $availability ){
	
	$curtime = time();

	switch( $availability ){
		
		case "week" : 		return $curtime - $filetime > 7 * 24 * 60 * 60;
		case "month" : 		return $curtime - $filetime > 30 * 24 * 60 * 60;
		case "year" : 		return $curtime - $filetime > 365 * 24 * 60 * 60;
		case "semester" : 	return $curtime - $filetime > 6 * 30 * 24 * 60 * 60;
		case "forever" : 	return false;
		
		default : die( "Unknown availability '$availability'" );
		
	}
	
}

//------------------------------------------------------------------------------

?>