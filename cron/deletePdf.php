<?php 
/**
 * Description: Suppression fichiers .pdf de la répertoire /tmp/
 * Author: Sitraka
 */

define( "PDFSTEMP_DIR", "/tmp/" );

// include_once( dirname( __FILE__ ) . "/../config/init.php" );
// include_once( dirname( __FILE__ ) ."/../objects/classes.php" );

if (is_dir(PDFSTEMP_DIR)) {
	if($tmpDir = opendir(PDFSTEMP_DIR))
	{
		while (($file = readdir($tmpDir)) !== false) {
			$arrFile = explode('.',$file);
			$arrFileCount = count($arrFile);
			if ($arrFileCount)
			{
				if ($arrFile[$arrFileCount - 1] == "pdf" || $arrFile[$arrFileCount - 1] == "PDF")
				{
					unlink(PDFSTEMP_DIR.$file);
				}
			}
		}
	}
}