<?php

/**
 * Envoi auto de demandes d'accusé de réception des devis peu temps après le premier envoi
 */

define( "DEBUG_EMAIL", false ); /* email pour les test */
define( "TIME_DIFF", '24' ); /* temps en heures entre la date d'envoi et la demande d'accusé de réception */


include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );
include_once( dirname( __FILE__ ) . "/../objects/Estimate.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );


User::fakeAuthentification();

/* ----------------------------------------------------------------------------------------------------- */
//sender
	
$query = "
SELECT u.email, u.firstname, u.lastname
FROM `user` u, parameter_cat pc 
WHERE pc.idparameter LIKE 'contact_commercial'
AND pc.paramvalue = u.iduser
LIMIT 1";

$rs =& DBUtil::query( $query );

$senderNameDefault 	= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
$senderEmailDefault	= $rs->fields( "email" );

/* ----------------------------------------------------------------------------------------------------- */
//notification
	
$editor = new MailTemplateEditor();

$editor->setTemplate( MailTemplateEditor::$ESTIMATE_AKNOWLEDGEMENT_OF_RECEIPT );
$editor->setUseAdminTags();
$editor->setUseUtilTags();
$editor->setLanguage( 1 );
	
$query = "
SELECT e.idestimate, e.iduser, e.idbuyer, c.mail, c.faxnumber, u.firstname, u.lastname, u.email, e.idcontact
FROM contact c, estimate e
LEFT JOIN estimate_aknowledgement_of_receipt ear ON ear.idestimate = e.idestimate
LEFT JOIN `user` u ON u.iduser = e.iduser
WHERE e.status LIKE " . DBUtil::quote( Estimate::$STATUS_SENT ) . "
AND e.aknowledgement_of_receipt IS TRUE
AND ear.datetime IS NULL
AND TIME_FORMAT( TIMEDIFF( NOW(), e.sent_date ), '%H' ) >= " . TIME_DIFF . "
AND TIME_FORMAT( TIMEDIFF( NOW(), e.sent_date ), '%H' ) <= ( 24 + " . TIME_DIFF . " )
AND c.idbuyer = e.idbuyer
AND c.idcontact = e.idcontact
AND ( c.mail NOT LIKE '' OR c.faxnumber NOT LIKE '' )";

$rs =& DBUtil::query( $query );

notificationLog( $rs->RecordCount() . " estimate notification(s)" );

$notifications = array();

while( !$rs->EOF() ){
	
	$editor->setUseCommercialTags( $rs->fields( "iduser" ) );
	$editor->setUseEstimateTags( $rs->fields( "idestimate" ) );
	$editor->setUseBuyerTags( $rs->fields( "idbuyer" ) , $rs->fields( 'idcontact' ) );
	
	if( $rs->fields( "firstname" ) && $rs->fields( "lastname" ) && $rs->fields( "email" )){
		$senderName 	= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
		$senderEmail 	= $rs->fields( "email" );
	}else{
		$senderName 	= $senderNameDefault;
		$senderEmail 	= $senderEmailDefault;
	}
	
	//envoi par email
	
	if( strlen( $rs->fields( "mail" ) ) )
		$ret = sendNotificationMail( 
		
			$editor, 
			$rs->fields( "idestimate" ), 
			DEBUG_EMAIL ? DEBUG_EMAIL : $rs->fields( "mail" ), 
			$senderName, 
			$senderEmail 
			
		);
	else if( strlen( $rs->fields( "faxnumber" ) ) )
		$ret = sendNotificationFax( 
		
			$editor, 
			$rs->fields( "idestimate" ),
			$rs->fields( "faxnumber" ),
			$senderEmail
			
		);
	else $ret = false;
	
	if( $ret === false )
		sendNotificationFailure( DEBUG_EMAIL ? DEBUG_EMAIL : $senderEmail, $rs->fields( "idestimate" ) );
	else{

		array_push( $notifications, $rs->fields( "idestimate" ) );
		
		if( !DEBUG_EMAIL )
			DBUtil::query( "INSERT INTO estimate_aknowledgement_of_receipt VALUES( '" . $rs->fields( "idestimate" ) . "', NOW() )" );

	}
	
	$rs->MoveNext();
	
}

if( count( $notifications ) )
	sumUpNotifications( DEBUG_EMAIL ? DEBUG_EMAIL : $senderEmail, $notifications );

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Envoi par courriel
 * @param MailTemplateEditor $editor
 * @param int $idestimate
 * @param string $recipient
 * @param string $senderName
 * @param string $senderEmail
 * @return bool
 */
function sendNotificationMail( MailTemplateEditor &$editor, $idestimate, $recipient, $senderName, $senderEmail ){

	$mailer = new htmlMimeMail();

	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSubject( $editor->unTagSubject() );

	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );
	$odf = new ODFEstimate( $idestimate );

	$mailer->addAttachment( $odf->convert( ODFDocument::$DOCUMENT_PDF ), "Devis_$idestimate.pdf", "application/pdf" );
	
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - $senderName\" <$senderEmail>" );
	
	return $mailer->send( array( $recipient ) );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Envoi par fax
 * @param MailTemplateEditor $editor
 * @param int $idestimate
 * @param string $faxnumber
 * @param string $senderEmail
 * @return bool
 */
function sendNotificationFax( MailTemplateEditor &$editor, $idestimate, $faxnumber, $senderEmail ){

	if( DEBUG_EMAIL )
		return;
		
	$smtp_server 	= DBUtil::getParameterAdmin('fax_smtp_server');
	$smtp_sender 	= DBUtil::getParameterAdmin('fax_smtp_sender');
	$smtp_auth 		= DBUtil::getParameterAdmin('fax_smtp_auth');
	$smtp_user 		= DBUtil::getParameterAdmin('fax_smtp_user');
	$smtp_password 	= DBUtil::getParameterAdmin('fax_smtp_password');
	$smtp_recipient = DBUtil::getParameterAdmin('fax_smtp_recipient');

	$mailer = new htmlMimeMail();
	
	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSMTPParams( $smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password );
	$mailer->setSubject( $faxnumber );

	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );
	$odf = new ODFEstimate( $idestimate );
	
	$mailer->addAttachment( $odf->convert( ODFDocument::$DOCUMENT_PDF ), "Devis_$idestimate.pdf", "application/pdf" );

	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . "\" <$smtp_user>" );
	
	return $mailer->send( array( $smtp_recipient ), "smtp" );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Prévenir de l'échec de l'envoi de demande d'accusé de réception
 * @param string $recipient
 * @param int $idestimate
 * @return void
 */
function sendNotificationFailure( $recipient, $idestimate ){
	
	$msg = "Un problème est survenu lors l'envoi automatique de la demande d'accusé de réception du devis n°  $idestimate";
	
	mail( $recipient, "Erreur Demande d'accusé de réception de devis", $msg );
	
}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Récapitulatif envoyé au commercial
 * @param string $recipient
 * @param array $estimates
 * @return void
 */
function sumUpNotifications( $recipient, array $estimates ){
	
	ob_start();
	
	?>
	<p>Les demandes d'accusé de réception des devis suivants ont été envoyées automatiquement :</p>
	<table style="border-collapse:collapse; border:1px solid #000;" border="1" cellspacing="0" cellpadding="4">
		<tr>
			<th>Devis N°</th>
			<th>Raison Sociale</th>
			<th>Contact</th>
			<th>Email</th>
			<th>Tél.</th>
			<th>Fax</th>
			<th>Date de création</th>
			<th>Dernière date d'envoi du devis</th>
		</tr>
		<?php
		
			foreach( $estimates as $idestimate ){
				
				$query = "
				SELECT e.DateHeure, e.sent_date, e.idbuyer,
					b.company, CONCAT( t.title_1, ' ', c.firstname, ' ', c.lastname ) AS contact,
					c.mail, c.faxnumber, c.phonenumber
				FROM estimate e, buyer b, contact c, title t
				WHERE e.idestimate = '$idestimate'
				AND b.idbuyer = e.idbuyer
				AND c.idbuyer = e.idbuyer
				AND c.idcontact = e.idcontact
				AND t.idtitle = c.title
				LIMIT 1";
				
				$rs =& DBUtil::query( $query );
				
				?>
				<tr>
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $idestimate; ?>">
							<?php echo htmlentities( $idestimate ); ?>
						</a>
					</td>		
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>">
							<?php echo htmlentities( $rs->fields( "company" ) ); ?>
						</a>
					</td>
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ); ?>">
							<?php echo htmlentities( $rs->fields( "contact" ) ); ?>
						</a>
					</td>
					<td>
						<a href="mailto:<?php echo $rs->fields( "mail" ); ?>">
							<?php echo $rs->fields( "mail" ); ?>
						</a>
					</td>
					<td><?php echo Util::phonenumberFormat( $rs->fields( "phonenumber" ) ); ?></td>
					<td><?php echo Util::phonenumberFormat( $rs->fields( "faxnumber" ) ); ?></td>
					<td><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ) ); ?></td>
					<td><?php echo Util::dateFormatEu( $rs->fields( "sent_date" ) ); ?></td>
				</tr>
				<?php
				
			}
			
	?>
	</table>
	<?php
	
	$mailer = new htmlMimeMail();
	
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - noreply\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	$mailer->setSubject( "[ Tâche planifiée ] Demandes d'accusé de réception de devis" );
	
	$mailer->setHtml( ob_get_contents() );
	ob_end_clean();
	
	$mailer->send( array( $recipient ) );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Log
 * @param string $message
 * @return void
 */
function notificationLog( $message ){
	
	echo "\n[ " . date( "Y-m-d H:i:s" ) . " ] ";
	
	if( DEBUG_EMAIL )
		echo "[ DEBUG : " . DEBUG_EMAIL . " ] ";
		
	echo $message;
	
}

/* ----------------------------------------------------------------------------------------------------- */

?>