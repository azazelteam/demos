<?php

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/marketing/trackers/trackersFunc.inc.php" );

if(file_exists("$GLOBAL_START_PATH/data/.updateTrackers")){
	die("Script déjà en cours de traitement ! <a href=\"$GLOBAL_START_URL/marketing/trackers/trackers.php\">Retour></a>");
}
else{
    
	// Création d'un fichier signifiant que le script est en cours
	touch("$GLOBAL_START_PATH/data/.updateTrackers");
	// Limite d'execution au cas ou ça foire
	set_time_limit( 120 );
	// Début du script
	$origineDate = date('d-m-Y');
    parserApacheLog($origineDate);
	makeCommingFromGoogleAdwords($origineDate);
	// Suppression du fichier de test de fonctionnement
	unlink("$GLOBAL_START_PATH/data/.updateTrackers");
}
?>