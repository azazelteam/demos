<?php 
/**
 * Description: Suppression fichiers .pdf de la répertoire /tmp/
 * Author: Sitraka
 */

define( "PDFSTEMP_DIR", "/tmp/" );

if (is_dir(PDFSTEMP_DIR)) {
	if($tmpDir = opendir(PDFSTEMP_DIR))
	{
		while (($file = readdir($tmpDir)) !== false) {
			//echo PDFSTEMP_DIR.$file."<br>";
            
            unlink(PDFSTEMP_DIR.$file);
		}
	}
}