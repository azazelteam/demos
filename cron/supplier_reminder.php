<?php

/**
 * Relance mail des fournisseurs
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures fournisseurs
 */
 
define( "DEBUG_EMAIL", false );

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );
	
/**
 * Relances par courriel auprès des fournisseurs :
 * 1. Pour confirmation de la commande fournisseur, 36h après la demande de confirmation
 * 2. Pour confirmation de la date réèlle d'expédition de la marchandise, le jour de la date d'expédition théorique
 * Si la constante MANUAL_SUPPLIER_REMINDER est définie, les relances ne sont pas envoyées automatiquement
 */

if( !defined( "MANUAL_SUPPLIER_REMINDER" ) ){
	
	User::fakeAuthentification();
	
	/* 1. */ confirmationReminder();
	/* 2. */ dispatchReminder();

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Relance auprès du fournisseur pour obtenir une confirmation de commande ainsi que sa date de mise à disposition
 * @param array $supplierOrders, optionnel, tableau de N° de Cde Fournisseur pour une relance forcée et manuelle
 */
function confirmationReminder( $supplierOrders = false ){

	//sender
	
	$query = "
	SELECT u.email, u.firstname, u.lastname
	FROM `user` u, parameter_cat pc 
	WHERE pc.idparameter LIKE 'contact_commercial'
	AND pc.paramvalue = u.iduser
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	$senderName 	= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
	$senderEmail 	= $rs->fields( "email" );
	
	//reminder
	
	$editor = new MailTemplateEditor();
	
	$editor->setTemplate( MailTemplateEditor::$SUPPLIER_CONFIRMATION_REMINDER );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
			
	$query = "
	SELECT os.idorder_supplier, 
		os.iduser, 
		u.email AS userEmail, 
		CONCAT( u.firstname, ' ', u.lastname ) AS userName,
		s.idsupplier,
		s.language, 
		sc.email AS supplierEmail, 
		sc.faxnumber, 
		s.send_to
	FROM supplier s 
	LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact ), 
	`user` u, order_supplier os
	LEFT JOIN supplier_confirmation_reminder scr ON scr.idorder_supplier = os.idorder_supplier
	WHERE os.remind_confirmation IS TRUE
	AND os.idsupplier = s.idsupplier
	AND s.remind_confirmation IS TRUE
	AND os.iduser = u.iduser
	AND s.idsupplier <> '" . DBUtil::getParameterAdmin( "internal_supplier" ) . "'";
	
	if( $supplierOrders && is_array( $supplierOrders ) ){ /* relance manuelle forcée pour une Cde donnée */
		
		$query .= " AND os.idorder_supplier IN( '" . implode( "','", $supplierOrders ) . "' )";
		
	}
	else{ /* relances automatiques */
		
		$query .= "
		AND os.confirmation_date LIKE '0000-00-00' 
		AND os.`status` IN( 'SendByFax', 'SendByMail' )
		AND os.`status` NOT LIKE 'cancelled'
		AND TIMEDIFF( NOW(), os.send_date ) >= '36:00:00'
		AND scr.idorder_supplier IS NULL";
	
	}
	
	$rs =& DBUtil::query( $query );

	reminderLog( $rs->RecordCount() . " confirmation reminder(s)" );
	
	$reminded = array();
	
	while( !$rs->EOF() ){

		$editor->setLanguage( $rs->fields( "language" ) );
		$editor->setUseCommercialTags( $rs->fields( "iduser" ) );
		$editor->setUseSupplierTags( $rs->fields( "idsupplier" ) );
		$editor->setUseSupplierOrderTags( $rs->fields( "idorder_supplier" ) );
		
		
		if( DEBUG_EMAIL
			|| ( $rs->fields( "send_to" ) == "Mail" && strlen( $rs->fields( "supplierEmail" ) ) )
			|| !strlen( $rs->fields( "send_to" ) ) && strlen( $rs->fields( "supplierEmail" ) ) )
			$ret = sendReminderMail(
			
				$editor, 
				$rs->fields( "idorder_supplier" ),
				DEBUG_EMAIL ? DEBUG_EMAIL : $rs->fields( "supplierEmail" ),
				$senderName,//$rs->fields( "userName" ),
				$senderEmail//$rs->fields( "userEmail" )
				
			);
		elseif( $rs->fields( "send_to" ) == "Fax" && strlen( $rs->fields( "faxnumber" ) )
			|| !strlen( $rs->fields( "send_to" ) ) && strlen( $rs->fields( "faxnumber" ) ) )
			$ret = sendReminderFax(
			
				$editor, 
				$rs->fields( "idorder_supplier" ),
				$rs->fields( "faxnumber" ),
				$senderEmail//$rs->fields( "userEmail" )
				
			);
		else $ret = false;
		
		if( $ret === false )
			sendConfirmationReminderFailure( 
		
				DEBUG_EMAIL ? DEBUG_EMAIL : $senderEmail,
				$rs->fields( "idorder_supplier" )

			);
		else{

			$reminded[ DEBUG_EMAIL ? DEBUG_EMAIL : $senderEmail ][] = $rs->fields( "idorder_supplier" );
			
			if( !DEBUG_EMAIL )
				DBUtil::query( "INSERT INTO supplier_confirmation_reminder VALUES( '" . $rs->fields( "idorder_supplier" ) . "', NOW() )" );
		
		}
		
		$rs->MoveNext();
		
	}
	
	foreach( $reminded as $recipient => $supplierOrders )
		sumUpConfirmations( $recipient, $supplierOrders );
	
}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Reminder envoyé 1 jour après la date d'expédition théorique donnée par le fournisseur
 */
function dispatchReminder(){

	//sender
	
	$query = "
	SELECT u.email, u.firstname, u.lastname
	FROM `user` u, parameter_cat pc 
	WHERE pc.idparameter LIKE 'contact_commercial'
	AND pc.paramvalue = u.iduser
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	$senderName 	= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
	$senderEmail 	= $rs->fields( "email" );
	
	//reminder
	
	$editor = new MailTemplateEditor();
	
	$editor->setTemplate( MailTemplateEditor::$SUPPLIER_DISPATCH_REMINDER );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
			
	$query = "
	SELECT os.idorder_supplier, 
		os.iduser, 
		u.email AS userEmail, 
		CONCAT( u.firstname, ' ', u.lastname ) AS userName,
		s.idsupplier,
		s.language, 
		sc.email AS supplierEmail, 
		sc.faxnumber, 
		s.send_to
	FROM supplier s
	LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact ),
	`user` u, order_supplier os
	LEFT JOIN supplier_dispatch_reminder ser ON ser.idorder_supplier = os.idorder_supplier
	WHERE os.remind_dispatch IS TRUE
	AND os.idsupplier = s.idsupplier
	AND s.remind_dispatch IS TRUE
	AND os.iduser = u.iduser
	AND s.idsupplier <> '" . DBUtil::getParameterAdmin( "internal_supplier" ) . "'
	AND os.dispatch_date LIKE '0000-00-00' 
	AND os.availability_date NOT LIKE '0000-00-00'
	AND DATEDIFF( NOW(), os.availability_date ) >= 1
	AND DATEDIFF( NOW(), os.availability_date ) <= 2
	AND os.`status` NOT IN( 'dispatch', 'cancelled' )
	AND ser.idorder_supplier IS NULL";
	
	$rs =& DBUtil::query( $query );

	reminderLog( $rs->RecordCount() . " dispatch reminder(s)" );
	
	$reminded = array();
	
	while( !$rs->EOF() ){

		$editor->setLanguage( $rs->fields( "language" ) );
		$editor->setUseCommercialTags( $rs->fields( "iduser" ) );
		$editor->setUseSupplierTags( $rs->fields( "idsupplier" ) );
		$editor->setUseSupplierOrderTags( $rs->fields( "idorder_supplier" ) );
		
		
		if( DEBUG_EMAIL
			|| ( $rs->fields( "send_to" ) == "Mail" && strlen( $rs->fields( "supplierEmail" ) ) )
			|| !strlen( $rs->fields( "send_to" ) ) && strlen( $rs->fields( "supplierEmail" ) ) )
			$ret = sendReminderMail(
			
				$editor, 
				$rs->fields( "idorder_supplier" ),
				DEBUG_EMAIL ? DEBUG_EMAIL : $rs->fields( "supplierEmail" ),
				$senderName,//$rs->fields( "userName" ),
				$senderEmail//$rs->fields( "userEmail" )
				
			);
		elseif( $rs->fields( "send_to" ) == "Fax" && strlen( $rs->fields( "faxnumber" ) )
			|| !strlen( $rs->fields( "send_to" ) ) && strlen( $rs->fields( "faxnumber" ) ) )
			$ret = sendReminderFax(
			
				$editor, 
				$rs->fields( "idorder_supplier" ),
				$rs->fields( "faxnumber" ),
				$senderEmail//$rs->fields( "userEmail" )
				
			);
		else $ret = false;
		
		if( $ret === false )
			sendDispatchReminderFailure( 
		
				DEBUG_EMAIL ? DEBUG_EMAIL : $senderEmail,
				$rs->fields( "idorder_supplier" )	
			
			);
		else{

			$reminded[ DEBUG_EMAIL ? DEBUG_EMAIL : $senderEmail ][] = $rs->fields( "idorder_supplier" );
			
			if( !DEBUG_EMAIL )
				DBUtil::query( "INSERT INTO supplier_dispatch_reminder VALUES( '" . $rs->fields( "idorder_supplier" ) . "', NOW() )" );
				
		}
		
		$rs->MoveNext();
		
	}
	
	foreach( $reminded as $recipient => $supplierOrders )
		sumUpDispatch( $recipient, $supplierOrders );
		
}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Envoi par courriel
 * @param MailTemplateEditor $editor
 * @param int $idorder_supplier
 * @param string $recipient
 * @param string $senderName
 * @param string $senderEmail
 * @return bool
 */
function sendReminderMail( MailTemplateEditor &$editor, $idorder_supplier, $recipient, $senderName, $senderEmail ){

	$mailer = new htmlMimeMail();

	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSubject( $editor->unTagSubject() );

	/*include_once( dirname( __FILE__ ) . "/../objects/odf/ODFSupplierOrder.php" );
	$odf = new ODFSupplierOrder( $idorder_supplier );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment( $pdfData , "Commande_Fournisseur_$idorder_supplier.pdf", "application/pdf" );*/
	
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - $senderName\" <$senderEmail>" );
	
	return $mailer->send( array( $recipient ) );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Envoi par fax
 * @param MailTemplateEditor $editor
 * @param int $idorder_supplier
 * @param string $faxnumber
 * @param string $senderEmail
 * @return bool
 */
function sendReminderFax( MailTemplateEditor &$editor, $idorder_supplier, $faxnumber, $senderEmail ){

	if( DEBUG_EMAIL )
		return;
		
	$smtp_server 	= DBUtil::getParameterAdmin('fax_smtp_server');
	$smtp_sender 	= DBUtil::getParameterAdmin('fax_smtp_sender');
	$smtp_auth 		= DBUtil::getParameterAdmin('fax_smtp_auth');
	$smtp_user 		= DBUtil::getParameterAdmin('fax_smtp_user');
	$smtp_password 	= DBUtil::getParameterAdmin('fax_smtp_password');
	$smtp_recipient = DBUtil::getParameterAdmin('fax_smtp_recipient');

	$mailer = new htmlMimeMail();
	
	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSMTPParams( $smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password );
	$mailer->setSubject( $faxnumber );

	/*include_once( dirname( __FILE__ ) . "/../objects/odf/ODFSupplierOrder.php" );
	$odf = new ODFSupplierOrder( $idorder_supplier );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment( $pdfData, "Commande_Fournisseur_$idorder_supplier.pdf", "application/pdf" );*/

	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . "\" <$smtp_user>" );
	
	return $mailer->send( array( $smtp_recipient ), "smtp" );
		
		
}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Prévenir de l'échec de l'envoi de demande de confirmation
 * @param string $recipient
 * @param int $idorder_supplier
 * @return void
 */
function sendConfirmationReminderFailure( $recipient, $idorder_supplier ){
	
	$query = "
	SELECT s.idsupplier, s.name, s.send_to, sc.faxnumber, sc.email
	FROM order_supplier os, supplier s
	LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact )
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier";
	
	$rs =& DBUtil::query( $query );
	
	$updateLink = "<a href=\"" . URLFactory::getHostURL() . "/product_management/enterprise/supplier.php?idsupplier=" . $rs->fields( "idsupplier" ) . "\">modifier</a>";
	
	ob_start();
	
	?>
	<p>Un problème est survenu lors l'envoi automatique de la demande de confirmation de commande auprès du fournisseur</p>
	<p>Merci de bien vouloir vérifier les informations ci-dessous :</p>
	<p>Fournisseur : <a href="<?php echo URLFactory::getHostURL(); ?>/product_management/enterprise/supplier.php?idsupplier=<?php echo $rs->fields( "idsupplier" ); ?>"><?php echo htmlentities( $rs->fields( "name" ) ); ?></a></p>
	<p>Commande fournisseur N° : 
		<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier; ?>"><?php echo $rs->fields( "idorder_supplier" ); ?></a>
	</p>
	<p>Mode d'envoi sélectionné pour ce fournisseur : <?php 
	
		switch( $rs->fields( "send_to" ) ){

			case "Mail" : echo "courriel"; break;
			case "Fax" : echo "fax"; break;
			default : echo "<span style=\"color:#FF0000;\">Non défini</span>";
			
		}
		
		echo " ( $updateLink )";
		
	?>
	</p>
	<p>Email : <?php
	
		if( !strlen( $rs->fields( "email" ) ) && $rs->fields( "send_to" ) == "Mail" ){

			?>
			<span style="color:#FF0000;">Non défini</span>
			<?php
			
		}
		elseif( !strlen( $rs->fields( "email" ) ) )
			echo "non défini";
		else{
			
			?>
			<a href="mailto:<?php echo htmlentities( $rs->fields( "email" ) ); ?>"><?php echo htmlentities( $rs->fields( "email" ) ); ?></a>
			<?php
			
		}
		
		echo " ( $updateLink )";
		
	?>
	</p>
	<p>Fax :<?php
	
		if( !strlen( $rs->fields( "faxnumber" ) ) && $rs->fields( "send_to" ) == "Fax" ){

			?>
			<span style="color:#FF0000;">Non défini</span>
			<?php
			
		}
		elseif( !strlen( $rs->fields( "faxnumber" ) ) )
			echo "non défini";
		else echo Util::phonenumberFormat( $rs->fields( "faxnumber" ) );
		
		echo " ( $updateLink )";
		
	?>
	</p>
	<?php

	include_once( dirname( __FILE__ ) . "/objects/mime_mail/htmlMimeMail.php" );
	
	$mailer = new htmlMimeMail();
	
	$mailer->setHtml( ob_get_contents() );
	ob_end_clean();
	
	$mailer->setSubject( "Erreur Relance Fournisseur" );
	$mailer->setFrom( "\"" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) ."\" <" . User::getInstance()->get( "email" ) . ">" );
	
	return $mailer->send( array( $recipient ) );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Prévenir de l'échec de l'envoi du rappel d'expédition
 * @param string $recipient
 * @param int $idorder_supplier
 * @return void
 */
function sendDispatchReminderFailure( $recipient, $idorder_supplier ){

	$query = "
	SELECT s.idsupplier, s.name, s.send_to, sc.faxnumber, sc.email
	FROM order_supplier os, supplier s
	LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact )
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier";
	
	$rs =& DBUtil::query( $query );
	
	$updateLink = "<a href=\"" . URLFactory::getHostURL() . "/product_management/enterprise/supplier.php?idsupplier=" . $rs->fields( "idsupplier" ) . "\">modifier</a>";
	
	ob_start();
	
	?>
	<p>Un problème est survenu lors l'envoi automatique du Reminder Expédition</p>
	<p>Merci de bien vouloir vérifier les informations ci-dessous :</p>
	<p>Fournisseur : <a href="<?php echo URLFactory::getHostURL(); ?>/product_management/enterprise/supplier.php?idsupplier=<?php echo $rs->fields( "idsupplier" ); ?>"><?php echo htmlentities( $rs->fields( "name" ) ); ?></a></p>
	<p>Commande fournisseur N° : 
		<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier; ?>"><?php echo $rs->fields( "idorder_supplier" ); ?></a>
	</p>
	<p>Mode d'envoi sélectionné pour ce fournisseur : <?php 
	
		switch( $rs->fields( "send_to" ) ){

			case "Mail" : echo "courriel"; break;
			case "Fax" : echo "fax"; break;
			default : echo "<span style=\"color:#FF0000;\">Non défini</span>";
			
		}
		
		echo " ( $updateLink )";
		
	?>
	</p>
	<p>Email : <?php
	
		if( !strlen( $rs->fields( "email" ) ) && $rs->fields( "send_to" ) == "Mail" ){

			?>
			<span style="color:#FF0000;">Non défini</span>
			<?php
			
		}
		elseif( !strlen( $rs->fields( "email" ) ) )
			echo "non défini";
		else{
			
			?>
			<a href="mailto:<?php echo htmlentities( $rs->fields( "email" ) ); ?>"><?php echo htmlentities( $rs->fields( "email" ) ); ?></a>
			<?php
			
		}
		
		echo " ( $updateLink )";
		
	?>
	</p>
	<p>Fax :<?php
	
		if( !strlen( $rs->fields( "faxnumber" ) ) && $rs->fields( "send_to" ) == "Fax" ){

			?>
			<span style="color:#FF0000;">Non défini</span>
			<?php
			
		}
		elseif( !strlen( $rs->fields( "faxnumber" ) ) )
			echo "non défini";
		else echo Util::phonenumberFormat( $rs->fields( "faxnumber" ) );
		
		echo " ( $updateLink )";
		
	?>
	</p>
	<?php
	
	include_once( dirname( __FILE__ ) . "/objects/mime_mail/htmlMimeMail.php" );
	
	$mailer = new htmlMimeMail();
	
	$mailer->setHtml( ob_get_contents() );
	ob_end_clean();
	
	$mailer->setSubject( "Erreur Reminder Expédition" );
	$mailer->setFrom( "\"" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) ."\" <" . User::getInstance()->get( "email" ) . ">" );
	
	return $mailer->send( array( $recipient ) );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Récapitulatif envoyé aux commerciaux pour le rappel les demandes de confirmation de commande
 * @param int $iduser
 * @param array $supplierOrders
 * @return void
 */
function sumUpConfirmations( $recipient, array $supplierOrders ){
	
	ob_start();
	
	?>
	<p>Les demandes de confirmation de commandes fournisseur suivantes ont été envoyées automatiquement :</p>
	<table style="border-collapse:collapse; border:1px solid #000;" border="1" cellspacing="0" cellpadding="4">
		<tr>
			<th>Fournisseur</th>
			<th>Email</th>
			<th>Tél.</th>
			<th>Fax</th>
			<th>Cde N°</th>
			<th>Date de création</th>
		</tr>
		<?php
		
			foreach( $supplierOrders as $idorder_supplier ){
				
				$query = "
				SELECT s.name, sc.phonenumber, sc.faxnumber, sc.email, SUBSTR( os.DateHeure, 1, 10 ) AS DateHeure
				FROM order_supplier os, supplier s
				LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact )
				WHERE idorder_supplier = '$idorder_supplier' 
				AND s.idsupplier = os.idsupplier 
				LIMIT 1";
				
				$rs =& DBUtil::query( $query );
				
				?>
				<tr>
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/product_management/enterprise/supplier.php?idsupplier=<?php echo $rs->fields( "idsupplier" ); ?>">
							<?php echo htmlentities( $rs->fields( "name" ) ); ?>
						</a>
					</td>		
					<td>
						<a href="mailto:<?php echo $rs->fields( "email" ); ?>">
							<?php echo htmlentities( $rs->fields( "email" ) ); ?>
						</a>
					</td>
					<td><?php echo Util::phonenumberFormat( $rs->fields( "phonenumber" ) ); ?></td>
					<td><?php echo Util::phonenumberFormat( $rs->fields( "faxnumber" ) ); ?></td>
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier; ?>">
							<?php echo $idorder_supplier; ?>
						</a>
					</td>
					<td><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ) ); ?></td>
				</tr>
				<?php
				
			}
			
	?>
	</table>
	<?php
	
	$mailer = new htmlMimeMail();
	
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - noreply\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	$mailer->setSubject( "[ Tâche planifiée ] Reminder Expédition Fournisseur" );
	
	$mailer->setHtml( ob_get_contents() );
	ob_end_clean();
	
	$mailer->send( array( $recipient ) );

}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Récapitulatif envoyé aux commerciaux pour les rappels d'expédition de la marchandise
 * @param string $recipient
 * @param array $supplierOrders
 * @return void
 */
function sumUpDispatch( $recipient, array $supplierOrders ){
	
	ob_start();
	
	?>
	<p>Les rappels d'expédition de la marchandise ont été envoyés automatiquement pour les commandes suivantes :</p>
	<table style="border-collapse:collapse; border:1px solid #000;" border="1" cellspacing="0" cellpadding="4">
		<tr>
			<th>Fournisseur</th>
			<th>Email</th>
			<th>Tél.</th>
			<th>Fax</th>
			<th>Cde N°</th>
			<th>Date d'expédition confirmée</th>
		</tr>
		<?php
		
			foreach( $supplierOrders as $idorder_supplier ){
				
				$query = "
				SELECT s.name, sc.phonenumber, sc.faxnumber, sc.email, os.dispatch_date
				FROM order_supplier os, supplier s
				LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact )
				WHERE idorder_supplier = '$idorder_supplier' 
				AND s.idsupplier = os.idsupplier 
				LIMIT 1";
				
				$rs =& DBUtil::query( $query );
				
				?>
				<tr>
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/product_management/enterprise/supplier.php?idsupplier=<?php echo $rs->fields( "idsupplier" ); ?>">
							<?php echo htmlentities( $rs->fields( "name" ) ); ?>
						</a>
					</td>		
					<td>
						<a href="mailto:<?php echo $rs->fields( "email" ); ?>">
							<?php echo htmlentities( $rs->fields( "email" ) ); ?>
						</a>
					</td>
					<td><?php echo Util::phonenumberFormat( $rs->fields( "phonenumber" ) ); ?></td>
					<td><?php echo Util::phonenumberFormat( $rs->fields( "faxnumber" ) ); ?></td>
					<td>
						<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier; ?>">
							<?php echo $idorder_supplier; ?>
						</a>
					</td>
					<td><?php echo Util::dateFormatEu( $rs->fields( "dispatch_date" ) ); ?></td>
				</tr>
				<?php
				
			}
			
	?>
	</table>
	<?php
	
	$mailer = new htmlMimeMail();
	
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . " - noreply\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	$mailer->setSubject( "[ Tâche planifiée ] Demandes de confirmations de commandes fournisseur" );
	
	$mailer->setHtml( ob_get_contents() );
	ob_end_clean();
	
	$mailer->send( array( $recipient ) );
	
}

/* ----------------------------------------------------------------------------------------------------- */
/**
 * Log
 * @param string $message
 * @return void
 */
function reminderLog( $message ){
	
	if( defined( "MANUAL_SUPPLIER_REMINDER" ) )
		return;
		
	echo "\n[ " . date( "Y-m-d H:i:s" ) . " ] ";
	
	if( DEBUG_EMAIL )
		echo "[ DEBUG : " . DEBUG_EMAIL . " ] ";
		
	echo $message;
	
}

/* ----------------------------------------------------------------------------------------------------- */

?>