<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object pour rendre un client inutilisable et le détache de son administrateur
 */

include_once( dirname( __FILE__ ) . "/Session.php" );


class COMBUYER extends DEPRECATEDBUYER
{

	/**
	 * Rend un client inutilisable et le détache de son administrateur
	 * @param int $idbuyer l'id du client
	 * @return void
	 */
	public static function DeleteBuyer($idbuyer=0)
	{
	$Base = &Session::getInstance()->GetDB();
	$l = substr(md5(date('r')),5,10); 
	$p = substr(md5(date('r')),11,10); 
	$Now_Time = date( "Y-m-d H:i:s");
	$user=addslashes(stripslashes($this->buyerinfo['login']));
	DBUtil::query("update buyer set idcommercial=0, admin=0, login='$l', password='$p', firstname='DELETED', username='$user', lastupdate='$Now_Time', newsletter='0' where idbuyer='$idbuyer'");
	}

}//EOC
?>