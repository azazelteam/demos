<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entête facture client
 */
 
 include_once( dirname( __FILE__ ) . "/Address.php" );
 
 
 interface Invoicing{
 	
 	//----------------------------------------------------------------------------
	/**
	 * Définit l'adresse de facturation à utiliser
	 * @param Address $invoiceAddress une adresse de facturation
	 * @return void
	 */
	public function setInvoiceAddress( Address $invoiceAddress );
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne l'adresse de facturation utilisée
	 * @return EditableAddress une référence vers l'adresse de facturation si elle existe, sinon NULL
	 */
	public function &getInvoiceAddress();
	
	//----------------------------------------------------------------------------
	
 }
 
?>