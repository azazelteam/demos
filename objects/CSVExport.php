<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export CSV
 */

include_once( dirname( __FILE__ ) . "/CSVHandler.php" );

set_time_limit( 0 );
ini_set( "memory_limit", "128M" );

define( "DEBUG_CSVEXPORT", 0 );
 
class CSVExport
{

	//--------------------------------------------------------------------------------------------------
	//metadata
	
	/*
	metadata = array(
	
		"TABLE1" => array(
	
			"tablekeys" 		=> $string,
			"field_date"		=> $string,
			"export_filter" 	=> $string,
			"function_export" 	=> $string,
			"columns" => array(
	
				"COLUMN1" => array(
	
					"fieldformat" 		=> $string,
					"erp_fieldname" 	=> $string,
					"erp_fieldformat" 	=> $string,
					"size" 				=> $integer
					
				),
				
				"COLUMN2" => array(
	
					"fieldformat" 		=> $string,
					"erp_fieldname" 	=> $string,
					"erp_fieldformat" 	=> $string,
					"size" 				=> $integer
					
				),
				
				
			)
			
		),
		...
	*/
	
	//--------------------------------------------------------------------------------------------------
	
	protected $con;
	
	protected $erp_name;
	
	protected $separator;
	protected $text_delimiter;
	protected $breakline;
	
	protected $metadata;
	protected $data;
	protected $headers;

	protected $output_type;
	protected $mime_type;
	protected $output_file;
	protected $filename;
	protected $export_directory;
	protected $upload_file;

	protected $date_sorter;
	protected $low_date;
	protected $up_date;
	
	protected $errors;
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param string $erp_name le nom de l'erp
	 * @param string $output_type le type de fichier de résultat
	 * @param string $separator le séparateur
	 * @param string $text_delimiter l'indicateur de texte
	 * @param string $breakline l'indicateur de saut de ligne
	 * @param bool $upload_file sortie standard ou non (cron)
	 * @param string $date_sorter type d'export (semaine, mois ou interval de temps)
	 * @param string $low_date date de dÃ©but de l'export
	 * @param string $up_date date de fin de l'export
	 */
	function __construct( $erp_name, $output_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\n", $upload_file = true, $date_sorter = "week", $low_date = "", $up_date = "" ){
	
		$this->erp_name 		= $erp_name;
		
		$this->separator 		= $separator;
		$this->text_delimiter 	= "\"";
		$this->breakline 		= $breakline;
		
		$this->outputfile		= "";
		$this->mime_type 		= "";
		$this->filename 		= "";
		$this->export_directory = "";
		$this->upload_file 		= $upload_file;
		
		$this->metadata = array();
		$this->data 	= array();
		$this->headers 	= array();

		$this->date_sorter			= $date_sorter;
		if($date_sorter == "interval") {
			$this->low_date			= $low_date;
			$this->up_date			= $up_date;
		} else if($date_sorter == "week") {
			$this->low_date			= date("Y-m-d", mktime(0, 0, 0, date("m"), date("d")-6-date("N"), date("Y")));
			$this->up_date			= date("Y-m-d", mktime(23, 59, 59, date("m"), date("d")-date("N"), date("Y")));
		} else {
			$this->low_date			= date("Y-m-d", mktime(0, 0, 0, date("m")-1, 1, date("Y")));
			$this->up_date			= date("Y-m-d", mktime(23, 59, 59, date("m"), 0, date("Y")));
		}

		$this->errors 	= array();
		
		$this->setOutputType( $output_type );
		
		if( $this->connect() === false )
			die( "Connection failed" );
			
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Initialise le type de fichier de sortie
	 * @param string $output_type
	 * @return void
	 */
	protected function setOutputType( $output_type ){
	
		switch( $output_type ){
		
			case "csv" :
			//case "xls" @todo export xls:
			//case "xml" @todo export xml:
			
				$this->output_type = $output_type;
				$this->mime_type = "csv/text";
				
				break;
				
			default : die( "Unknown output mode '$output_type'" );
		
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Exporte le fichier
	 * @return int le nombre de lignes
	 */
	public function export(){
	
		if( !$this->createOutputFile() )
			return 0;
			
		$this->setMetaData();
		$this->setHeaders();
		
		if( $this->setData() === false )
			return 0;	
			
		$this->setTransValues();
			
		$this->disconnect();
			
		if( !$this->writeOuputFile() )
			return 0;
			
		$this->closeOutputFile();
		$this->applyExportFunctions();
			
		if( $this->upload_file )
			$this->output();
		
		return count( $this->data );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Connexion à la base de données
	 * @return bool true en cas de succès, sinon false
	 */
	protected function connect(){
	
		global 	$GLOBAL_DB_HOST,
				$GLOBAL_DB_NAME,
				$GLOBAL_DB_USER,
				$GLOBAL_DB_PASS;
				
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>connect( $GLOBAL_DB_HOST, $GLOBAL_DB_NAME, $GLOBAL_DB_USER )</u>" );

		if( !isset( $GLOBAL_DB_HOST )
			|| !isset( $GLOBAL_DB_NAME )
			|| !isset( $GLOBAL_DB_USER )
			|| !isset( $GLOBAL_DB_PASS ) )
			die( "Connection parameters not found" );
		
		$this->con = @mysqli_connect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS );
		
		if( !is_resource( $this->con ) ){ 
		
			$this->errors[] = "Connection to '$GLOBAL_DB_HOST' failed"; 
			
			return false; 
		
		}

		$ret = @mysqli_select_db( $this->con,$GLOBAL_DB_NAME );
		
		if( $ret === false ) {
		
			$this->errors[] = "Cannot select database $GLOBAL_DB_NAME : " . @mysqli_error($this->con); 
			return false; 
		
		}

		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Déconnexion de la base de données
	 * @return void
	 */
	protected function disconnect(){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>disconnect()</u>" );
			
		if( is_resource( $this->con ) )
			@mysqli_close( $this->con );
		
	}

	//--------------------------------------------------------------------------------------------------

	/**
	 * Exécute une requête SQL
	 * @param string $query la requête à éxécuter
	 * @return mixed la l'objet resource mysql ou false en cas d'échec
	 */
	protected function query( $query ){

		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>query()</u><br />$query" );
			
		$result = @mysqli_query( $this->con,$query );
		
		if( !is_resource( $result ) )
			$this->errors[] = "SQL Error : $query ( " . @mysqli_error($this->con) . " )"; 

		return $result;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur
	 * @param string $separator le séparateur
	 * @return void
	 */
	public  function setSeparator( $separator ){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setSeparator( $separator )</u>" );
			
		$this->separator = $separator;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur de texte
	 * @param string $text_delimiter le séparateur
	 * @return void
	 */
	public function setTextDelimiter( $text_delimiter ){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setTextDelimiter( $text_delimiter )</u>" );
			
		$this->text_delimiter = $text_delimiter;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le caractère de saut de ligne
	 * @param string $breakline le caractère de saut de ligne
	 * @return void
	 */
	public  function setBreakline( $breakline ){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setBreakline( $breakline )</u>" );
			
		$this->breakline = $breakline;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les métadonnées
	 * @return void
	 */ 
	protected function setMetaData(){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setMetaData()</u>" );
			
		$this->metadata = array();
		
		//tables
		
		$query = "
		SELECT tablename, 
			tablekeys,
			export_filter,
			field_date,
			function_export,
			x_order
		FROM erp_table
		WHERE erp_name LIKE '{$this->erp_name}'
		AND used = 1
		ORDER BY x_order ASC";
		
		$result = $this->query( $query );
		
		while( $row = @mysqli_fetch_assoc( $result ) ){
		
			$this->metadata[ $row[ "tablename" ] ] = array(
			
				"tablekeys" 		=> $row[ "tablekeys" ],
				"columns" 			=> array(),
				"field_date" 		=> $row[ "field_date" ],
				"export_filter" 	=> $row[ "export_filter" ],
				"function_export" 	=> $row[ "function_export" ]
			
			);
				
				
		}

		//columns
		
		$tables = array_keys( $this->metadata );
		
		foreach( $tables as $tablename ){
		
			$query = "
			SELECT tablename,
				fieldname,
				fieldformat,
				erp_fieldname,
				erp_fieldformat,
				size,
				order_view,
				trans_table,
				trans_id,
				trans_text
			FROM erp_field
			WHERE erp_name LIKE '{$this->erp_name}'
			AND tablename LIKE '$tablename'
			AND used = 1
			AND export_avalaible <> 0
			ORDER BY order_view ASC";
		
			$result = $this->query( $query );
		
			while( $field = @mysqli_fetch_object( $result ) ){
		
				$this->metadata[ $tablename ][ "columns" ][ $field->fieldname ] = array(
				
					"fieldformat" 		=> $field->fieldformat,
					"erp_fieldname" 	=> empty( $field->erp_fieldname ) ? $field->fieldname : $field->erp_fieldname,
					"erp_fieldformat" 	=> empty( $field->erp_fieldformat ) ? $field->fieldformat : $field->erp_fieldformat,
					"size" 				=> $field->size,
					"trans_table" 		=> $field->trans_table,
					"trans_id" 			=> $field->trans_id,
					"trans_text" 		=> $field->trans_text
					
  				);
				
			}
			
		}

		@mysqli_free_result( $result );
	
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Génère les entetes du fichiers
	 * @return void
	 */
	protected function setHeaders(){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setHeaders()</u>" );
			
		$this->headers = array();
		
		reset( $this->metadata );
		foreach( $this->metadata as $tablename => $tableMetadata ){
			
			foreach( $tableMetadata[ "columns" ] as $fieldname => $fieldMetadata ){
			
				$this->headers[] = $fieldMetadata[ "erp_fieldname" ];
				$this->formats[] = $fieldMetadata[ "erp_fieldformat" ];
				
			}

		}
		
		if( DEBUG_CSVEXPORT )
			print_r( $this->headers );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le type des colonnes
	 * @return array les types
	 */
	protected function getColumnTypes(){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>getColumnTypes()</u>" );
			
		$types = array();
		
		reset( $this->metadata );
		foreach( $this->metadata as $tablename => $tableMetadata ){
			
			foreach( $tableMetadata[ "columns" ] as $fieldname => $fieldMetadata )
				$types[] = $fieldMetadata[ "erp_fieldformat" ];

		}
		
		if( DEBUG_CSVEXPORT )
			print_r( $types );

		return $types;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Génère les données
	 * @return void
	 */
	protected function setData(){
		
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setData()</u>" );
		
		$this->data = array();
		
		$limit = 120; //récupèrer les données par paquets de $limit lignes
		
		$selectPattern = $this->getSelectPattern();
		
		if( $selectPattern === false )
			return false;
			
		$done = false;

		$start = 0;
		$rowCount = 0;
		while( !$done ){
		
			$query = "$selectPattern LIMIT $start, $limit";
			
			$result = $this->query( $query );
			
			if(!is_resource( $result ) ){
			
				$this->errors[] = "QUERY ERROR : $query";
				$this->abort();
				
				return;
				
			}
			
			if( !@mysqli_num_rows( $result ) )
				$done = true;
				
			while( $row = mysqli_fetch_array( $result ) ){
	
				$this->data[ $rowCount ] = $row;
					
				$rowCount++;

			}

			$start += $limit;
			
		}
		
		if( DEBUG_CSVEXPORT )
			print_r( $this->data );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Retourne la requete a executer
	 * @return string $selectPattern la requete
	 */
	protected function getSelectPattern(){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>getSelectPattern()</u>" );
			
		$selectedFields = array(); 	//champs sélectionnés
		$exportFilters 	= array(); 	//filtres d'export
		$selectedTables = array();	//tables sélectionnés
		
		reset( $this->metadata );
		foreach( $this->metadata as $tablename => $tableMetadata ){
			
			//tables sélectionnés
			
			$selectedTables[] = "`$tablename`";
			
			//champs sélectionnés
			
			$fieldnames = array_keys( $tableMetadata[ "columns" ] );
			
			foreach( $fieldnames as $fieldname )
				$selectedFields[] = "`$tablename`.`$fieldname`";

			//filtres de date
			
			if( !empty( $tableMetadata[ "field_date" ] ) )
				$exportDates = "`$tablename`.`".$tableMetadata[ "field_date" ]."`";

			//filtres d'export
			
			if( !empty( $tableMetadata[ "export_filter" ] ) )
				$exportFilters[] = $tableMetadata[ "export_filter" ];
				
		}
		
		if( !count( $selectedFields ) || !count( $selectedTables ) ){
		
			$this->errors[] = "No table or no column selected";
			return false;

		}
		
		if( count( $selectedTables ) > 1 && !count( $exportFilters ) ){
		
			$this->errors[] = "Cannot link selected tables";
			return false;
		
		}
		
		$selectPattern  = "SELECT " . implode( ",", $selectedFields );
		$selectPattern .= " FROM " . implode( ",", $selectedTables );
		
		if( count( $exportFilters ) )
			$selectPattern .= " WHERE " . implode( " AND ", $exportFilters );

		if( isset( $exportDates ) ) {
			if( !count( $exportFilters ) ) {
				$selectPattern .= " WHERE ";
			} else {
				$selectPattern .= " AND ";
			}
			$selectPattern .= $exportDates[0].">='$this->low_date' AND ".$exportDates[0]."<='$this->up_date'" ;
			for( $i = 1 ; $i < count( $exportDates ) ; $i++ ) {
				$selectPattern .= " AND $tablename.".$exportDates[$i].">='$this->low_date' AND $tablename.".$exportDates[$i]."<='$this->up_date'" ;				
			}
		}

		if( DEBUG_CSVEXPORT )
			$this->debug( "SELECT pattern = $selectPattern" );
			
		return $selectPattern;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Modifie les données pour l'erp
	 * ex : le nom du pays à la place de idstate
	 * @return void
	 */
	protected function setTransValues(){
		
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setTransValues()</u>" );
			
		reset( $this->metadata );
		
		foreach( $this->metadata as $tablename => $tableMetadata ){
		
			$columnIndex = 0;
			foreach( $tableMetadata[ "columns" ] as $fieldname => $columnMetadata ){
			
				$trans_table = 	$tableMetadata[ "columns" ][ $fieldname ][ "trans_table" ];
				$trans_id = 	$tableMetadata[ "columns" ][ $fieldname ][ "trans_id" ];
				$trans_text = 	$tableMetadata[ "columns" ][ $fieldname ][ "trans_text" ];
				
				if( !empty( $trans_table ) && !empty( $trans_id ) && !empty( $trans_text ) )
					$this->setTransColumnValues( $columnIndex, $trans_table, $trans_id, $trans_text );

				$columnIndex++;
				
			}
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Modifie le nom des colonnes pour l'ERP
	 * ex : COUNTRY CODE à la place de idstate
	 * @param mixed $columnIndex le nom du champ
	 * @param string $trans_table le nom de la table de 'traduction'
	 * @param string $trans_id l'id de la table de 'traduction'
	 * @param string $trans_text le nom du champ de la table de 'traduction'
	 * @return void
	 */
	protected function setTransColumnValues( $columnIndex, $trans_table, $trans_id, $trans_text ){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>setTransColumnValues( $columnIndex, $trans_table, $trans_id, $trans_text )</u>" );
			
		$i = 0;
		while( $i < count( $this->data ) ){
		
			$value = $this->data[ $i ][ $columnIndex ];
			
			$query = "
			SELECT `$trans_id`
			FROM `$trans_table`
			WHERE `$trans_text` = '$value'
			LIMIT 1";
			
			$result = $this->query( $query );
			
			if( !is_resource( $result ) || !@mysqli_num_rows( $result ) ){
			
				//$this->errors[] = "Cannot get foreign value ( $trans_table, $trans_id, $trans_text ) : " . @mysql_error();
				$this->data[ $i ][ $columnIndex ] = "";
				
			}
			//else $this->data[ $i ][ $columnIndex ] = @mysql_result( $result, 0, $trans_id );
			else $this->data[ $i ][ $columnIndex ] = Util::mysqli_result( $result, 0, 0 );
			$i++;
		
		}
		
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Execute les fonctions d'export
	 * @return void
	 */
	protected function applyExportFunctions(){

		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>applyExportFunctions()</u>" );
			
		new CSVHandler( $this->erp_name, "export" );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ecrit le fichier
	 * @return bool true en cas de succès, false sinon
	 */
	protected function writeOuputFile(){
		
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>writeOuputFile()</u>" );
			
		if( !is_resource( $this->output_file ) ){
		
			$this->errors[] = "Cannot create output file";
			$this->abort();
			
			return false;
			
		}
		
		$this->writeHeaders();
		$this->writeData();
		
		return true;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Ecrit les entetes
	 * @return void
	 */
	protected function writeHeaders(){
		
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>writeHeaders()</u>" );
			
		$i = 0;
		while( $i < count( $this->headers ) ){
		
			if( $i )
				@fwrite( $this->output_file, $this->separator );

			$str  = $this->text_delimiter;
			$str .= str_replace( "\"", "\"\"", $this->headers[ $i ] );
			$str .= $this->text_delimiter;
				
			@fwrite( $this->output_file, $str );
			
			$i++;
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Ecrit les données
	 * @return void
	 */
	function writeData(){

		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>writeData()</u>" );

		if( !count( $this->data ) )
			return;
			
		$rowCount 		= count( $this->data );
		$columnCount 	= count( $this->headers );
		
		$types = $this->getColumnTypes();
		
		$row = 0;
		while( $row < $rowCount ){

			@fwrite( $this->output_file, $this->breakline ); //même pour la première ligne à cause des en-têtes
					
			$column = 0;
			while( $column < $columnCount ){
			
				if( $column )
					@fwrite( $this->output_file, $this->separator );
	
				if( $types[ $column ] == "dec" || $types[ $column ] == "decimal" )
					$value = number_format( $this->data[ $row ][ $column ], 2, ",", " " );
				else $value = $this->data[ $row ][ $column ];
				
				$str  = $this->text_delimiter . str_replace( "\"", "\"\"", $value ) . $this->text_delimiter;

				@fwrite( $this->output_file, $str );

				$column++;
				
			}
			
			$row++;
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Créé le fichier d'export
	 * @return bool true en cas de succes, false en cas d'échec
	 */
	function createOutputFile(){
		
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>createOutputFile()</u>" );
			
		$query = "
		SELECT export_directory, export_name
		FROM erp_config 
		WHERE erp_name LIKE '{$this->erp_name}' 
		LIMIT 1";
		
		$result = $this->query( $query );
		
		if( !is_resource( $result ) ){
		
			$this->errors[] = "Cannot get export directory";
			$this->abort();
			return false;
			
		}
			
		//$this->export_directory = @mysql_result( $result, 0, "export_directory" );
		//$this->filename 		= @mysql_result( $result, 0, "export_name" );
        
        $this->export_directory = Util::mysqli_result( $result, 0, 0 );
		$this->filename 		= Util::mysqli_result( $result, 0, 1 );
		
		if( empty( $this->export_directory ) || empty( $this->filename ) ){
		
			//$this->errors[] = "Export directory or file undefined";
			$this->abort();
			
			return false;
			
		}

		@mysqli_free_result( $result );

		if( !is_dir( $this->export_directory ) ){

			if( @mkdir( $this->export_directory ) === false ){

				$this->errors[] = "Cannot create export directory";
				return false;
			
			}
			
			/*if( @chmod( $this->export_directory, 0755 ) === false ){
			
				$this->errors[] = "Cannot create export directory";
				return false;
			
			}*/
			
		}
		
		$path = $this->export_directory . $this->filename . "." . $this->output_type;

		$this->output_file = fopen( $path, "w+" );
		
		if( !is_resource( $this->output_file ) ){
		
			$this->errors[] = "Cannot open output file";
			$this->abort();
			
			return false;
		
		}
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Ferme le fichier
	 * @return void
	 */
	protected function closeOutputFile(){
	
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>closeOutputFile()</u>" );
			
		if( is_resource( $this->output_file ) )
			fclose( $this->output_file );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Génère le fichier
	 * @return void
	 */
	protected function output(){

		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>output()</u>" );
	
		if( headers_sent() ){
		
			$this->errors[] = "Headers already sent";
			$this->abort();
			
			return;
			
		}
		
		header( "Content-Type: {$this->mime_type}" );
		header( "Content-disposition: inline; filename={$this->filename}.{$this->output_type}" );
			
		readfile( $this->export_directory . $this->filename . "." . $this->output_type );
	
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Affiche les erreurs
	 */
	protected function report(){
			
		echo implode( "<br />", $this->errors );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Arrete l'import et affiche les erreurs
	 * @return void
	 **/
	protected function abort(){
		
		if( DEBUG_CSVEXPORT )
			$this->debug( "<u>abort()</u>" );
			
		$this->disconnect();
		$this->closeOutputFile();
		
		if( DEBUG_CSVEXPORT )
			$this->report();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les erreurs
	 * @return string les erreurs
	 */
	public function getErrors(){
	
		return $this->errors;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	function debug( $string ){
	
		echo "<br />$string";
		flush();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
}

?>