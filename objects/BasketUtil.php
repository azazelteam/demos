<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object librairie de fonctions utiles
 */
 
abstract class BasketUtil{
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total TTC ( All Taxes Included ) d'un devis donné, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @param int $idestimate le numéro de devis
	 * @return float
	 */
	public static function getEstimateTotalATI( $idestimate ){
		
		//pays de livraison
		
		$iddelivery = DBUtil::getDBValue( "iddelivery", "estimate", "idestimate", $idestimate );
		
		if( $iddelivery )
			$idstate = DBUtil::getDBValue( "idstate", "delivery", "iddelivery", $iddelivery );
		else{
			
			$rs =& DBUtil::query( "SELECT idstate FROM buyer b, estimate e WHERE e.idbuyer = b.idbuyer AND e.idestimate = '$idestimate'" );
			$idstate = $rs->fields( "idstate" );
			 	
		}
		
		//TVA pour le pays de livraison
		
		$useVAT = floatval( DBUtil::getDBValue( "bvat", "state", "idstate", $idstate ) );

		//calcul du TTC
		
		$query = "
		SELECT SUM( ROUND( erow.discount_price * erow.quantity * ( 1.0 + erow.vat_rate * $useVAT / 100.0 ) * ( 1.0 - e.total_discount / 100.0 ), 2 ) )
			- ( e.billing_amount * ( 1.0 + erow.vat_rate * $useVAT / 100.0 ) )
			+ ( e.total_charge_ht * ( 1.0 + erow.vat_rate * $useVAT / 100.0 ) )
		AS totalATI
		FROM estimate e, estimate_row erow
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY erow.idestimate";
		
		$rs =& DBUtil::query( $query );
		
		return round( $rs->fields( "totalATI" ), 2 );
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total TTC ( All Taxes Included ) d'une commande donnée, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @param int $idorder le numéro de commande
	 * @return float
	 */
	public static function getOrderTotalATI( $idorder ){
		
		//pays de livraison
		
		$iddelivery = DBUtil::getDBValue( "iddelivery", "order", "idorder", $idorder );
		
		if( $iddelivery )
			$idstate = DBUtil::getDBValue( "idstate", "delivery", "iddelivery", $iddelivery );
		else{
			
			$rs =& DBUtil::query( "SELECT idstate FROM buyer b, `order` o WHERE o.idbuyer = b.idbuyer AND o.idorder = '$idorder'" );
			$idstate = $rs->fields( "idstate" );
			 	
		}
		
		//TVA pour le pays de livraison
		
		$useVAT = floatval( DBUtil::getDBValue( "bvat", "state", "idstate", $idstate ) );
		
		//calcul du TTC
		
		$query = "
		SELECT SUM( ROUND( orow.discount_price * orow.quantity * ( 1.0 + orow.vat_rate * $useVAT / 100.0 ) * ( 1.0 - o.total_discount / 100.0 ), 2 ) )
			- ( o.billing_amount * ( 1.0 + orow.vat_rate * $useVAT / 100.0 ) )
			+ ( o.total_charge_ht * ( 1.0 + orow.vat_rate * $useVAT / 100.0 ) )
		AS totalATI
		FROM `order` o, order_row orow
		WHERE o.idorder = '$idorder'
		AND o.idorder = orow.idorder
		GROUP BY orow.idorder";
		
		$rs =& DBUtil::query( $query );
		
		return round( $rs->fields( "totalATI" ), 2 );
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total TTC ( All Taxes Included ) d'une commande fournisseur donnée, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @todo
	 * @param int $idorder_supplier le numéro de commande fournisseur
	 * @return float
	 */
	public static function getSupplierOrderTotalATI( $idorder_supplier ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total TTC ( All Taxes Included ) d'une facture donnée, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @param int $idbilling_buyer le numéro de facture
	 * @return float
	 */
	public static function getInvoiceTotalATI( $idbilling_buyer ){
		
		//pays de livraison
		
		$query = "
		SELECT o.iddelivery
		FROM `order` o, billing_buyer bb
		WHERE bb.idbilling_buyer = $idbilling_buyer
		AND bb.idorder = o.idorder";
		
		$rs =& DBUtil::query( $query );
		
		$iddelivery = $rs->fields( "iddelivery" );
		
		if( $iddelivery )
			$idstate = DBUtil::getDBValue( "idstate", "delivery", "iddelivery", $iddelivery );
		else{
			
			$rs =& DBUtil::query( "SELECT idstate FROM buyer b, `billing_buyer` bb WHERE bb.idbuyer = b.idbuyer AND bb.idbilling_buyer = '$idbilling_buyer'" );
			$idstate = $rs->fields( "idstate" );
			 	
		}
		
		//TVA pour le pays de livraison
		
		$useVAT = floatval( DBUtil::getDBValue( "bvat", "state", "idstate", $idstate ) );
		
		DBUtil::query( "SET @totalATI:= 0.0" );
		
		//calcul du TTC
		
		$query = "
		SELECT SUM( ROUND( bbrow.discount_price * bbrow.quantity * ( 1.0 + bbrow.vat_rate * $useVAT / 100.0 ) * ( 1.0 - bb.total_discount / 100.0 ), 2 ) )
			- ( bb.billing_amount * ( 1.0 + bbrow.vat_rate * $useVAT / 100.0 ) )
			+ ( bb.total_charge_ht * ( 1.0 + bbrow.vat_rate * $useVAT / 100.0 ) )
		AS totalATI
		FROM billing_buyer bb, billing_buyer_row bbrow
		WHERE bb.idbilling_buyer = '$idbilling_buyer'
		AND bb.idbilling_buyer = bbrow.idbilling_buyer
		GROUP BY bbrow.idbilling_buyer";
		
		$rs =& DBUtil::query( $query );
		
		return round( $rs->fields( "totalATI" ), 2 );
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total TTC ( All Taxes Included ) d'un avoir donné, avec ports et remise
	 * @static
	 * @access public
	 * @todo
	 * @param int $idcredit le numéro d'avoir
	 * @return float
	 */
	 public static function getCreditTotalATI( $idcredit ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total HT ( Exclusive of Taxe ) d'un devis donné, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @todo
	 * @param int $idestimate le numéro de devis
	 * @return float
	 */
	public static function getEstimateTotalET( $idestimate ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total HT ( Exclusive of Taxe ) d'une commande donnée, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @todo
	 * @param int $idorder le numéro de commande
	 * @return float
	 */
	public static function getOrderTotalET( $idorder ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total HT ( Exclusive of Taxe ) d'une commande fournisseur donnée, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @todo
	 * @param int $idorder_supplier le numéro de commande fournisseur
	 * @return float
	 */
	public static function getSupplierOrderTotalET( $idorder_supplier ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total HT ( Exclusive of Taxe ) d'une facture donnée, avec ports, remise, accompte et remise pour paiement comptant
	 * @static
	 * @access public
	 * @todo
	 * @param int $idbilling_buyer le numéro de facture
	 * @return float
	 */
	public static function getInvoiceTotalET( $idbilling_buyer ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retoune le montant total HT ( Exclusive of Taxe ) d'un avoir donné, avec ports et remise
	 * @static
	 * @access public
	 * @todo
	 * @param int $idcredit le numéro d'avoir
	 * @return float
	 */
	public static function getCreditTotalET( $idcredit ){
		
		return 0.0;
		
	}
	
	//----------------------------------------------------------------------------
	//----------------------------------------------------------------------------
	
	/**
	 * Calcul du montant H.T. des lignes articles
	 * @param string $tableName le nom de la table où sont stockés les lignes article
	 * @param int $primaryKeyValue la valeur de la clé primaire dans la table principale ( idestimate, idorder, ... )
	 * @return float
	 */
	private static function getTotalET( $tableName, $primaryKeyValue ){
		
	}
	
	private static function getTotalATI( $tableName, $primaryKeyValue ){
		
	}
	
	private static function getItemsTotalET( $tableName, $primaryKeyValue ){
		
	}
	
	//----------------------------------------------------------------------------
		
}

?>