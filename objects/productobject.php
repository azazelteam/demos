<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des produits
 * @deprecated 	préférer la classe CProduct
 */

include_once (dirname(__FILE__) . "/tableobject.php");
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );


class PRODUCT extends TABLEDESC {
	protected $id;
	protected $NBrealproduct;
	protected $curart;
	protected $lang;
	protected $article;
	protected $LimiteMin;
	protected $LimiteMax;
	protected $associates;
	protected $DEBUG;
	protected $tissus;
	protected $couleurs;
	protected $light; // objet léger (pas de chargement des noms des familles)

	/**
	 * Contructeur
	 * @param int $idp L'id du produit
	 * @param int $idart L'id de l'article
	 * @param mixed $light si true pas d'intitulés, si 2 pas de famille et conservation de l'ordre
	 * @param mixed $selected_tissus false si pas de tissu sélectionné pour l'article, tableau avec infos du tissu sélectionné sinon
	 * @param mixed $selected_couleurs false si pas de couleur sélectionnée pour l'article, tableau avec infos de la couleur sélectionnée sinon
	 */
	function __construct($idp = 0, $idart = 0, $light = true, $selected_tissus = false, $selected_couleurs = false) {
		
		if( $idp == 0 )	
			trigger_error( "Produit zéro instancié", E_USER_ERROR );
		
		$this->id = 0;
		$this->NBrealproduct = 0;
		$this->curart = 0;
		$this->lang = '_1';
		$this->LimiteMin = 0;
		$this->LimiteMax = 1;
		$this->DEBUG = 'none';
		$this->tissus = array ();
		$this->couleurs = array ();
		$this->light = true; // objet léger (pas de chargement des noms des familles)
		$this->lang = "_1";
		$this->tablename = 'product';
		$this->multilangfields = array (
			'name',
			'description',
			'doc',
			'designation',
			'key_word',
			'desc_more',
			'desc_tech',
			'text_commercial',
			'summary'
		);
		// 'description' pas multilingue! attention fonction GetDescrition
		$this->load_design();
		if ($idart > 0)
			$idp = $this->GetIdFromArt($idart, $idp);
		$this->id = $idp;
		$this->light = $light;
		$this->article = array ();
		$this->associates = array ();
		$this->SetArticle($selected_tissus, $selected_couleurs);

	}

	public function & getArticles() {
		return $this->article;
	}
	public function & getArticleAt($index) {
		return $this->article[$index];
	}
	public function getArticleCount() {
		return count($this->article);
	}

	/**
	 * Retourne l'id du produit
	 */
	public function GetId() {
		return $this->id;
	}

	/**
	 * Retourne le nom du produit
	 */
	public function GetName() {

		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->getvalue("name");
	}

	/**
	 * Retourne le sous-titre du produit
	 */
	public function GetName_secondary_1() {
		return $this->getvalue('name_secondary_1');
	}
	/**
	 * Retourne la description du produit
	 */
	public function GetDescrition() {
		return $this->getvalue('description');
	}

	/**
	 * Retourne la description technique du produit
	 */
	public function GetDesc_tech() {
		return $this->getvalue('desc_tech_1');
	}

	/**
	 * Retourne la date création du produit
	 */
	public function GetNovelty() {
		return $this->getvalue('novelty');
	}

	/**
	 * Retourne la description complémentaire du produit
	 */
	public function GetDesc_more() {
		return $this->getvalue('desc_more_1');
	}

	/**
	 * Retourne le texte commercial du produit
	 */
	public function GetText_com() {
		return $this->getvalue('text_commercial_1');
	}

	/*
	 *Retourne le chemin de l'image à partir de la racine du projet 
	 */
	public function GetImagePath() {

		return URLFactory::getProductImagePath( $idproduct );
	}

	/**
	 * Retourne les id fournisseur
	 */
	public function GetIdSupplier() {
		return $this->getvalue('idsupplier');
	}

	/**
	 * Retourne les certifications fournisseur
	 * @deprecated
	 */
	public function GetCertifications() {

		return array();
	}

	/**
	 * Retourne les pictos
	 * @deprecated
	 */
	public function GetPictos() { return array(); }

	/**
	 * Retourne les pubs
	 */
	public function GetPub() {
		$pubs = array ();

		for ($i = 1; $i < 7; $i++) {
			if ($this->GetValue('space_msg_' . $i) != 0) {

				$idpub = $this->GetValue('space_msg_' . $i);
				$Bddd = & DBUtil::getConnection();

				//On sélectionne les images des pubs
				$rpub = "SELECT msg_commercial FROM msg_commercial WHERE idmsg_commercial=$idpub";
				$rspub = $Bddd->Execute($rpub);

				$pubs[] = $rspub->Fields('msg_commercial');
			}
		}

		return $pubs;
	}

	/**
	 * Retroune le chemin vers l'image 'page'
	 * @deprecated
	 */
	public function Page() {
		return $this->getvalue('page');
	}

	/**
	 * Retourne les noms des tableaux des références
	 */
	public function GetTable_name($x) {
		return $this->getvalue("table_name_$x");
	}

	/**
	 * Retourne les paramètres d'affichage des tableaux de références
	 */
	public function GetUse_color($x) {
		$t = $this->getvalue('table');
		$m = $this->getvalue('manage');
		if (!empty ($m) AND empty ($t))
			$res = 1;
		else
			$res = $this->getvalue("use_color_$x");
		return $res;
	}

	/**
	 * Retourne lid de la catégorie du produit
	 */
	public function GetCategoryID() {
		return $this->getvalue('idcategory', 0);
	}

	/**
	 * Retourne le masque article du produit
	 */
	public function GetIdMaskDetail() {
		return $this->getvalue('detailmask', 0);
	}

	/**
	 * Retourne le masque produit du produit
	 */
	public function GetIdMaskProduct() {
		return $this->getvalue('productmask', 0);
	}

	/**
	 * Retourne le masque pdf sarticle du produit
	 */
	public function GetIdMaskPdfDetail() {
		return $this->getvalue('detailpdf', 0);
	}

	/**
	 * Retourne le masque pdf produit du produit
	 */
	public function GetIdMaskPdfProduct() {
		return $this->getvalue('productpdf', 0);
	}

	/**
	 * Retourne emplacement message commercial
	 */
	public function GetSpace_1() {
		return $this->getvalue('space_msg_1', 0);
	}
	public function GetSpace_2() {
		return $this->getvalue('space_msg_2', 0);
	}
	public function GetSpace_3() {
		return $this->getvalue('space_msg_3', 0);
	}
	public function GetSpace_4() {
		return $this->getvalue('space_msg_4', 0);
	}
	public function GetSpace_5() {
		return $this->getvalue('space_msg_5', 0);
	}
	public function GetSpace_6() {
		return $this->getvalue('space_msg_6', 0);
	}

	/**
	 * Retourne le nombre d'articles du produit
	 */
	public function realcount() {
		return $this->NBrealproduct;
	}

	/**
	 * Place le pointeur sur le premier article
	 * @deprecated
	 */
	function first() {
		$this->curart = 0;
	}

	/**
	 * Place le pointeur sur l'article suivant
	 * @deprecated
	 */
	function next() {
		$this->curart++;
	}

	/**
	 * Place le pointeur un l'article
	 * @param int $i L'index de l'article
	 */
	public function rand($i) {
		$this->curart = $i;
	}

	/**
	 * Retourne une valeur pour l'article courrant
	 * @param string $key Le nom de la valeur
	 * @param mixed $default valeur retournée par défaut
	 * @return mixed, valeur correpondant à $key
	 */
	public function getvalue($key, $default = '') {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->getvalue($key, $default);
	}

	/**
	 * Retourne le prix d'achat
	 */
	public function GetBuyingPrice() {
		return $this->getvalue('buyingcost');
	}

	/**
	 * Retourne les données pour les tableaux
	 * @deprecated 1.62 - 18 sept. 2006
	 * @see DEPRECATEDARTICLE::GetRefTable
	 */
	function GetRefTable($Compteur, $n, & $ExtraCol, & $FamilyCol, $filter = 0) {
		$this->curart = $Compteur;
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetRefTable($Compteur, $n, $ExtraCol, $FamilyCol, $filter);
		return array ();
	}

	/**
	 * Retourne les données pour les tableaux
	 * @param string $default valeur renvoyé par défaut si un champ est vide
	 */
	public function GetRefArray($default) {
		$ArticlesByTables = array ();

		foreach ($this->article as $id => $Article) {
			$tableId = $Article->GetTable();
			if (empty ($tableId))
				$tableId = 0; // 0 au lieu de '' = clef du tableau $ArticlesByTables

			if (!isset ($ArticlesByTables[$tableId]))
				$ArticlesByTables[$tableId] = array (
					'id' 				=> array (),
					'checked' 			=> array (),
					'summary' 			=> array (),
					'reference' 		=> array (),
					'reference_base' 	=> array (),
					'weight' 			=> array (),
					'lot' 				=> array (),
					'unit' 				=> array (),
					'delay' 			=> array (),
					'parm' 				=> array (),
					'cot' 				=> array (),
					'reducerate' 		=> array (),
					'basicprice' 		=> array (),
					'promo' 			=> array (),
					'woprice' 			=> array (),
					'wtprice' 			=> array (),
					'rowspan' 			=> array (),
					'stock_level'		=> array(),
					'hiddenFields' 		=> array (),
					'buyingcost' 		=> array (),
					'sellingcost' 		=> array (),
					'ref_supplier' 		=> array (),
					'hide_cost' 		=> array ()
				);


			$a = null;
			$a = $Article->getArticle();

			$ArticlesByTables[$tableId]['id'][] 			= $Article->GetId();
			$ArticlesByTables[$tableId]['checked'][] 		= $Article->IsSelected();
			$ArticlesByTables[$tableId]['summary'][] 		= $Article->get('summary', '&nbsp;');
			$ArticlesByTables[$tableId]['reference'][] 		= $Article->GetVisualRef();
			$ArticlesByTables[$tableId]['weight'][] 		= getValueOrDefaultIfEmpty($Article->GetWeight(), $default);
			$ArticlesByTables[$tableId]['delay'][] 			= Util::getDelay($Article->GetDelivDelay());
			$ArticlesByTables[$tableId]['lot'][] 			= $Article->get('lot', $default);
			$ArticlesByTables[$tableId]['unit'][] 			= $Article->get('unit', $default);
			$ArticlesByTables[$tableId]['reference_base'][] = $Article->get('reference', $default);
			$ArticlesByTables[$tableId]['buyingcost'][] 	= $Article->get('buyingcost', $default);
			$ArticlesByTables[$tableId]['sellingcost'][] 	= $Article->get('sellingcost', $default);
			$ArticlesByTables[$tableId]['ref_supplier'][] 	= $Article->get('ref_supplier', $default);
			$ArticlesByTables[$tableId]['hidecost'][] 		= $Article->get('hidecost', 0);
			$ArticlesByTables[$tableId]['stock_level'][] 	= $Article->GetStock();
			
			$reference = $Article->GetVisualRef();
			
			if(isset($a['fam']['label']))
				$familyLabels = array_count_values($a['fam']['label']);
			else $familyLabels = array();

			$rspan = 0;
			$hideFamilies = false;
			if($hideFamilies OR $Article->get('hide_family')) { $rspan ++;
				for( $j=1 ; $j < 100 ; $j++ )
				{
					$characLabel = $Article->get("parm_name_$j");
					$characValue = $Article->get("parm_value_$j");
					if(!empty($characLabel)) { $rspan ++;
						$ArticlesByTables[$tableId]['parm']['noFam'][$characLabel][] = $characValue;
						$ArticlesByTables[$tableId]['cot'][$characLabel] = !empty($a[$characLabel]) ? $a[$characLabel] : '&nbsp;';
					} else break;	
				}
			} else {
				foreach( $familyLabels as $familyLabel => $familyColspan ) {
					$rspan ++;
					// Caractéristiques techniques
					foreach( $a['int'][$familyLabel]['label'] as $k => $characLabel ) {
						$rspan ++;
						$characValue = !empty($a[$familyLabel][$characLabel]) ? $a[$familyLabel][$characLabel] : $default;
						$ArticlesByTables[$tableId]['parm'][$familyLabel][$characLabel][] = $characValue;
						$ArticlesByTables[$tableId]['cot'][$characLabel] = !empty($a[$characLabel]) ? $a[$characLabel] : '&nbsp;';
					}
				}
			}
	
			$reducerate = $Article->GetReduceRate();
			$basicprice = $Article->GetBasicPrice();
			
			if($Article->hasMultiPriceRange()){
				$ArticlesByTables[$tableId]['hasMultiPriceRange'][]="ok";
			}else{
				$ArticlesByTables[$tableId]['hasMultiPriceRange'][]="";
			}
			
			$arrayMultiprice = $Article->getMultiPrices();
			$ArticlesByTables[$tableId]['Multiprice'][]=$arrayMultiprice;
			
			$woprice = $Article->GetWoPrice();
			$wtprice = $Article->GetWtPrice();
			
			if($Article->IsReduceRate()){
				$ArticlesByTables[$tableId]['reducerate'][] = !empty($reducerate) ? Util::rateFormat($reducerate) : $default;
			}else{
				$ArticlesByTables[$tableId]['reducerate'][] = !empty($reducerate) ? Util::priceFormat($reducerate) : $default;
			}
			
	
			$ArticlesByTables[$tableId]['basicprice'][] = !empty($basicprice) ? Util::priceFormat($basicprice) : $default;
			
			if( Product::isPromo( $reference ) ){
				
				$ArticlesByTables[$tableId]['promo'][] = 'promo';
				$ArticlesByTables[$tableId]['hasPromo'][] = true;
				$ArticlesByTables[$tableId]['promo_price'][] = Util::priceFormat( $woprice );
				$ArticlesByTables[$tableId]['promo_rate'][] = Util::numberFormat($reducerate );
				
			}
			else{
				
				$ArticlesByTables[$tableId]['promo'][] = "";
				$ArticlesByTables[$tableId]['hasPromo'][] = false;
				$ArticlesByTables[$tableId]['promo_price'][] = "-";
				$ArticlesByTables[$tableId]['promo_rate'][] = "-";
				
			}
			
			if( Product::isDestocking( $reference ) ){
				
				$ArticlesByTables[$tableId]['isDestocking'][] = true;
				$rate = Product::getDestockingRate( $reference );
				$ArticlesByTables[$tableId]['destocking_rate'][] = $rate > 0.0 ? Util::rateFormat( $rate ) : "-";
				$ArticlesByTables[$tableId]['destocking_price'][] = Util::priceFormat( $basicprice - ($basicprice * $rate / 100.0 ));
				
			}
			else{
				
				$ArticlesByTables[$tableId]['isDestocking'][] = false;
				$ArticlesByTables[$tableId]['destocking_price'][] = "-";
				$ArticlesByTables[$tableId]['destocking_rate'][] = "-";
				
			}
			
			$ArticlesByTables[$tableId]['woprice'][] = !empty($woprice) ? Util::priceFormat($woprice) : 'Nous consulter';
			$ArticlesByTables[$tableId]['wtprice'][] = !empty($woprice) ? Util::priceFormat($wtprice) : $default;
			$ArticlesByTables[$tableId]['rowspan'][] = !empty($woprice) ? '' : ' rowspan="2"';
			// attention $Article->article['idreal_product'] peut être différent de $Article->GetId() si couleur ou tissu
	
			$ArticlesByTables[$tableId]['hiddenFields'][] = array(
												'IdRealProduct'		=>	$Article->get('idarticle' ), /////GetId(),
												'reftotal'			=>	$Article->GetRef().$Article->getRefExtra(),
												'prixtotal'			=>	$Article->GetBasicPrice(),
												'prixtotalremise'	=>	$Article->GetWoPrice(),
												'caract'			=>	$Article->get('designation'),
												'r'					=>	$Article->GetReduceRate(),
												'poids'				=>	$Article->GetWeight(),
												'delay'				=>  $Article->GetDelivDelay(),
												'min_cde'			=>  $Article->get('min_cde','1'),
												'idtissus'			=>  $Article->GetIdTissus(),
												'idcolor'			=>  $Article->GetIdColor(),
												'option_price'		=>  $Article->GetOptionPrice()
												);
		}

		ksort($ArticlesByTables);

		return $ArticlesByTables;
	}

	public function GetDelay() {
		$myDelay = $Article->GetDelivDelay();
		return $myDelay;
	}

	/**
	 * Retourne les couleurs
	 * @return array Tableau de couleurs
	 */
	public function LoadColors() {
		$Base = & DBUtil::getConnection();
		$Query = 'SELECT `p`.`idcolor`, `p`.`delay`, `p`.`price`, `p`.`percent`,' .
		' `c`.`name`, `c`.`image`, `c`.`code_color`' .
		' FROM `color_product` `p`, `color` `c`' .
		" WHERE `idproduct` = '" . $this->id . "'" .
		' AND `c`.`idcolor` = `p`.`idcolor`' .
		' ORDER BY `p`.`display`';
		$rs = & DBUtil::query($Query);
		while ($rs && !$rs->EOF) {
			$k = $rs->Fields('idcolor');
			$this->couleurs[$k] = $rs->fields;
			$rs->MoveNext();
		}

		return $this->couleurs;
	}

	/**
	 * Retourne les couleurs
	 * @return array Tableau de couleurs
	 */
	public function GetColors() {
		$this->LoadColors();
		return $this->couleurs;
	}

	/**
	 * Formulaire pour les couleurs
	 */
	public function GetColorsInput($type = 'radio', $start = '', $end = '<br />') {
		global $Arr_Couleurs;
		if (!isset ($Arr_Couleurs))
			return;

		
		$Couleurs = $this->GetColors();
		switch ($type) {
			case 'checkbox' :
				// Version cases à cocher -> plusieurs sélections possibles
				foreach ($Couleurs as $v) {
					$idt = $v['idcouleur'];
					echo $start, '<input name="Couleurs[]" style="margin-bottom:15px;margin-left:30px" type="checkbox" value="', $idt, '"';
					if (in_array($idt, $Arr_Couleurs))
						echo ' checked="checked"';
					echo ' /><img src="', $v['image'], '" title="', $v['name'], '" class="ProductColorImage" />', $end;
				}
				echo '<input style="margin-bottom:8px;margin-left:30px" type="image" name="CHG_COLOR" src="../www/icones/ajout_cde.gif" />';
				break;
			case 'radio' :
				// Version boutons radio -> choix unique et submit automatique
				foreach ($Couleurs as $v) {
					$idt = $v['idcolor'];
					echo $start, '<input type="radio" name="Couleurs[]" class="ProductColorRadioButton" value="', $idt, '"  OnClick="this.form.submit()"';
					if (in_array($idt, $Arr_Couleurs))
						echo ' checked="checked"';
					echo ' /><img class="ProductColorImage" src="', $v['image'], '" onmouseover="drcx(' . "'" . $v['code_color'] . "'" . ',' . "'" . addslashes($v['name']);
					if ($v['price'] > 0)
						echo '<br />Supplément : ', Util::priceFormat($v['price']);
					elseif ($v['price'] < 0) echo '<br />Réduction : ', Util::priceFormat($v['price']);
					if ($v['percent'] > 0)
						echo '<br />Supplément : ', Util::rateFormat($v['percent']);
					elseif ($v['percent'] < 0) echo '<br />Réduction : ', Util::rateFormat($v['percent']);
					echo '<br />D&eacute;lai : ', $v['delay'];
					echo "'" . ')" onmouseout="nd()" />', $end;
				}
				break;
		}
	}

	/**
	 * Retourne les tissus
	 * @return array Tableau des tissus
	 */
	public function LoadTissus() {
		$Base = & DBUtil::getConnection();
		$Query = 'SELECT `p`.`idtissus_product`,`p`.`idtissus`, `p`.`delay`, `p`.`price`,' .
		' `p`.`rate`, `t`.`name`, `t`.`image`, `t`.`link_tissus`' .
		' FROM `tissus_product` `p`, `tissus` `t`' .
		" WHERE `idproduct` = '" . $this->id . "'" .
		' AND `t`.`idtissus` = `p`.`idtissus`';
		$rs = & DBUtil::query($Query);
		while ($rs && !$rs->EOF) {
			$k = $rs->Fields('idtissus');
			$this->tissus[$k] = $rs->fields;
			$rs->MoveNext();
		}

		return $this->tissus;
	}

	/**
	 * Retourne les tissus
	 * @return array Tableau des tissus
	 */
	public function GetTissus() {
		return $this->tissus;
	}

	/**
	 * Formulaire pour les tissus
	 */
	public function GetTissusInput($type = 'radio', $start = '', $end = '<br />') {
		global $Arr_Tissus, $Ref_Tissus;
		if (!isset ($Arr_Tissus))
			return;

		
		$Tissus = $this->GetTissus();
		switch ($type) {
			case 'checkbox' :
				// Version cases à cocher -> plusieurs sélections possibles
				foreach ($this->tissus as $v) {
					$idt = $v['idtissus'];
					echo $start, '<input name="Tissus[]" style="margin-bottom:15px;margin-left:30px" type="checkbox" value="', $idt, '"';
					if (in_array($idt, $Arr_Tissus))
						echo ' checked="checked"';
					echo ' /><a href="' . $v['link_tissus'] . '" target="Tissus" ><img src="', $v['image'], '" title="', $v['name'], '"></a>';
				}
				echo '<input style="margin-bottom:8px;margin-left:30px" type="image" name="CHG_TISSUS" src="../www/icones/ajout_cde.gif" />', $end;
				break;
			case 'radio' :
				// Version boutons radio -> choix unique et submit automatique
				foreach ($Tissus as $v) {
					$idt = $v['idtissus'];
					echo $start, '<input type="radio" name="Tissus[]" style="margin-bottom:15px;margin-left:30px" value="', $idt, '"  OnClick="this.form.submit()"';
					if (in_array($idt, $Arr_Tissus))
						echo ' checked="checked" disabled /> ', $Ref_Tissus;
					else
						echo ' disabled />', "\n";
					echo ' <a href="' . $v['link_tissus'] . '?Tissus=' . $idt . '" target="Tissus" ';
					echo "onclick=\"window.open(this.href,'rdv','width=440,height=550,scrollbars=yes'); return false;\"";
					echo ' /><img src="', $v['image'], '" onmouseover="drcx(' . "'" . addslashes($v['name']) . "'" . ',' . "'";
					if ($v['price'] > 0)
						echo 'Supplément : ', Util::priceFormat($v['price']);
					elseif ($v['price'] < 0) echo 'Réduction : ', Util::priceFormat($v['price']);
					if ($v['rate'] > 0)
						echo 'Supplément : ', Util::rateFormat($v['rate']);
					elseif ($v['rate'] < 0) echo 'Réduction : ', Util::rateFormat($v['rate']);
					echo '<br />D&eacute;lai : ', $v['delay'];
					echo "'" . ')" onmouseout="nd()" /></a>', $end;
				}
				break;
		}
	}

	/**
	 * Indique si l'article courrant est sélectionné
	 */
	public function IsSelected() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->IsSelected();
	}

	/**
	 * Retourne le prix unitaire HT pour l'article courrant
	 */
	public function GetWotPrice() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetWoPrice();
	}

	/**
	 * Retourne le prix unitaire TTC pour l'article courrant
	 */
	public function GetWtPrice() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetWtPrice();
	}

	/**
	 * Retourne le prix catalogue unitaire HT pour l'article courrant
	 */
	public function GetBasicPrice() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetBasicPrice();
	}

	/**
	 * Retourne le taux de remise pour l'article courrant
	 */
	public function GetReduceRate() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetReduceRate();
	}

	/**
	 * Retourne la TVA pour l'article courrant
	 */
	public function GetVAT() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetVAT();
	}

	/**
	 * Retourne la référence de l'article courrant
	 */
	public function GetRef() {
		return $this->article[$this->curart]->GetRef();
	}

	/**
	 * Retourne l'id de l'article courrant
	 */
	public function GetArtID() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetId();
	}

	/**
	 * Retourne l'article demandé
	 * @param $idreal_product l'article demandé
	 */
	public function GetArticleById($idreal_product) {

		$i = 0;
		while ($i < count($this->article)) {

			if ($this->article[$i]->GetId() == $idreal_product)
				return $this->article[$i];

			$i++;

		}

		return null;

	}

	/**
	 * Retourne la désignation de l'article courrant
	 */
	public function GetDesignation() {
		if (isset ($this->article[$this->curart]))
			return $this->article[$this->curart]->GetDesignation();
	}

	/**
	 * Retourne les prix par quantité pour l'article courrant ou spécifié
	 * @param int $idreal optionnel - l'id 'real' de l'article
	 * @return array Un tableau avec les prix sinon false
	 */
	public function GetQuantityPrices($idreal = 0) {
		$buyerfamily = 0 + DBUtil::getParameter('IdBuyerFamily');
		if ($buyerfamily == 0 && !Session::getInstance()->getCustomer() )
			return FALSE;
		if ($idreal > 0)
			$i = $this->GetIdByIdreal($idreal);
		else
			$i = $this->curart;
		$Data = $this->article[$i]->SearchAllFamilyprice($buyerfamily);
		if (count($Data) > 0)
			return $Data;
		return FALSE;
	}

	/**
	 * Retourne la référence formattée de l'article courrant
	 */
	public function GetVisualRef() {
		return $this->article[$this->curart]->GetVisualRef();
	}

	/**
	 * Récupère les produids associés
	 * @return int Le nombre deproduits associés
	 */
	public function GetAssociate() {
		$this->associates = array ();

		$Result = 0;
		$Base = & DBUtil::getConnection();
		$IdProduct = $this->id;
		$SQL_Query = "select associat_product from associat_product where idproduct='$IdProduct' order by displayorder";
		$rs = & DBUtil::query($SQL_Query);

		if (empty ($rs)) { // TODO : Gestion des erreurs

			trigger_error("Une erreur s'est produite, merci de contacter l'administrateur du site", E_USER_ERROR);
		}
		elseif ($rs->RecordCount() > 0) {
			for ($i = 0; $i < $rs->RecordCount(); $i++) {
				$this->associates[] = $rs->Fields('associat_product');
				$rs->MoveNext();
				$Result++;
			}
		}
		return $Result;
	}

	/**
	 * Affichage des produits associés
	 * s'il y en a
	 */
	public function CreateAssociateLink() {
		global $GLOBAL_START_PATH;
		global $GLOBAL_CATALOG_PATH;
		global $GLOBAL_START_URL;
		global $ScriptName;

		
		$n = count($this->associates);
		if ($n > 0) {
			$ProductArray = & $this->associates;
			
		} else
			echo "<!-- aucun produits -->";
	}

	/**
	 * @deprecated 1 - 15 mars 2006
	 */
	function GetFamilyRates() {
		return 'familyrates';
	} // not implemented

	/**
	 * Retourne l'id du produit à partir de l'id d'un article
	 * @param int $idart L'id de l'article
	 * @param int $ipd L'id produit retoruné si non trouvé
	 */
	private function GetIdFromArt($idart, $idp) {
		$res = $idp;
		$Base = & DBUtil::getConnection();
		$rs = & DBUtil::query("select idproduct from detail where idarticle=$idart");
		if ($rs->RecordCount() > 0) {
			$res = $rs->Fields('idproduct');
		}
		return $res;
	}

	/**
	 * Retourne l'index d'un article à partir de la référence
	 * @param idreal $idreal L'id 'real' de l'article
	 * @return int L'index de l'article
	 */
	private function GetIdByIdreal($idreal) {
		for ($i = 0; $i < $this->NBrealproduct; $i++) {
			if ($this->article[$i]->GetId() == $idreal)
				return $i;
		}
		echo 'erreur';
		/**
		 * @todo trigger
		 */
		return $this->curart;
	}

	/**
	 * Charge les articles du produit
	 * @param mixed $selected_tissus false si pas de tissu sélectionné pour l'article, tableau avec infos du tissu sélectionné sinon
	 * @param mixed $selected_couleurs false si pas de couleur sélectionnée pour l'article, tableau avec infos de la couleur sélectionnée sinon
	 * @return int Le nombre d'articles
	 */
	public function SetArticle($selected_tissus, $selected_couleurs) {
		
		include_once( dirname( __FILE__ ) . "/articleobject.php" );
		
		$Base = & DBUtil::getConnection();
		//$Articles = $this->article;
		$Str_Fields = $this->fields;
		$idproduct = $this->id;
		$SQL_select = "SELECT $Str_Fields" .
		" FROM `product` as `p`" .
		" WHERE `p`.`idproduct`='$idproduct'";
		// ." ORDER BY `rp`.`display_detail_order`,`rp`.`reference`";
		$rs = & DBUtil::query($SQL_select);

		if ($rs != false && !$rs->EOF) {
			////$this->NBrealproduct = $rs->RecordCount();
			$p = $rs->fields;
			// Chargement des couleurs et des tissus si le produit les gère
			$manage = !empty ($rs->fields['manage']) ? $rs->fields['manage'] : '00';
			if (!empty ($manage {
				0 }))
			$this->LoadColors();
			if (!empty ($manage {
				1 }))
			$this->LoadTissus();

			$detail_objet = new TABLEDESC();
			$detail_objet->tablename = 'detail';
			$detail_objet->multilangfields = $this->multilangfields;
			$detail_objet->lang = $this->lang;
			$detail_objet->load_design(', d.');
			$detail_objet->fields;
			/* Mysql 5
			$SQL_select = 'SELECT l.idcategory, l.idarticle,f.intitule_family_1 as `if`,t.intitule_title_1 as it ,v.intitule_value_1 as iv,l.cotation,' .
					'l.title_display as dt, l.family_display as df, l.level, d.' .
				$detail_objet->fields .
			' FROM intitule_link l,intitule_title t, intitule_value v,intitule_family f,detail d  WHERE d.idproduct = ' . $idproduct .
			' AND l.idarticle = d.idarticle' .
			' AND l.idintitule_title = t.idintitule_title' .
			' AND l.idintitule_value = v.idintitule_value' .
			' AND f.idintitule_family = l.idfamily' .
			' ORDER BY l.reference,l.family_display,l.title_display';
			*/
			//Mysql 4 START
			/*$SQL_select = '
						SELECT l.idcategory, l.idarticle,f.intitule_family_1 as `if`,t.intitule_title_1 as it ,v.intitule_value_1 as iv,l.cotation,
							l.title_display as dt, l.family_display as df, l.level, l.reference
						FROM detail d, intitule_link l,intitule_title t, intitule_value v,intitule_family f 
						WHERE d.idproduct = ' . $idproduct . '
						AND l.reference LIKE d.reference
						AND d.stock_level <> 0
						AND l.idintitule_title = t.idintitule_title
						AND l.idintitule_value = v.idintitule_value
						AND f.idintitule_family = l.idfamily
						ORDER BY d.display_detail_order ASC,l.reference,l.family_display,l.title_display';*/

			//Mysql 4 END
			//echo "<br />SQL  $SQL_select  SQL<br />";
			
			$andStockLevel = " AND d.stock_level <> 0 ";
			
			if(DBUtil::getParameter("use_not_available_products")){
				$andStockLevel = "";
			}
			$query = "
			SELECT d.idarticle,
				d.reference,
				il.idcategory, 
				ifam.intitule_family_1 AS `if`,
				it.intitule_title_1 AS it ,
				iv.intitule_value_1 AS iv,
				il.cotation,
				il.title_display AS dt, 
				il.family_display AS df, 
				il.level,
				il.available
			FROM detail d
				LEFT JOIN intitule_link il ON d.idarticle = il.idarticle
				LEFT JOIN intitule_title it ON il.idintitule_title = it.idintitule_title
				LEFT JOIN intitule_value iv ON il.idintitule_value = iv.idintitule_value
				LEFT JOIN intitule_family ifam ON il.idfamily = ifam.idintitule_family
			WHERE d.idproduct = '{$this->id}'
			$andStockLevel
			AND d.available = 1
			ORDER BY d.display_detail_order ASC,
				il.idarticle ASC,
				il.family_display ASC,
				il.title_display ASC";
	
			$rs = & DBUtil::query( $query );

			$a = array ();

			if ($rs->EOF) { // aucun intitulé!
				$SQL_select = "
								SELECT  d.idarticle,d.reference 
								FROM detail d 
								WHERE d.stock_level <> 0 
								AND d.idproduct = '$idproduct'
								ORDER BY d.display_detail_order ASC";

				$rs = & DBUtil::query($SQL_select);
				$a['fam']['label'] = array ();
			}

			$j = 0;
			$i = 0;
			$lastref = 'No Ref!';
			while (!$rs->EOF) {

				$t = $rs->fields;
				$rs->MoveNext();

				if ($t['idarticle'] < 1)
					continue;

				$ref = $t['reference'];

				if ($ref != $lastref) {

					//Mysql 4 START
					$SQL_select2 = 'SELECT  d.' . $detail_objet->fields . ' FROM detail d WHERE d.stock_level <> 0 AND d.idarticle = ' . $t['idarticle'];
					$rs2 = & DBUtil::query($SQL_select2);
					if ($rs2->RecordCount())
						$t += $rs2->fields;

					//Mysql 4 END
					if ($i) {
						$this->article[$j] = new DEPRECATEDARTICLE($a, true, $this->tissus, $selected_tissus, $this->couleurs, $selected_couleurs);
						$j++;
						//	echo " idarticle ${a['idarticle']} ";
					}
					$lastref = $ref;
					$i = 1;
					//$a = $p + $t;
					if($p == null) $p = array();
					if($t==null) $t = array();
					$a = array_merge($p, $t);
					$a['idreal_product'] = $a['idarticle']; // pour compatiblité ascendante
					if (isset ($a['if'])) {
						$a["parm_fam_$i"] = $pf_x = $a['if'];
						$a["parm_name_$i"] = $pn_x = $a['it'];
						$a["parm_value_$i"] = $pv_x = $a['iv'];
						$a["cotation_$i"] = $ct_x = $a['cotation'];
						$a["level_$i"] = $a['level'];
						$a["fdisplay_$i"] = $a['df'];
						$a["idisplay_$i"] = $a['dt'];
					}
				} else {
					$i++;
					$a['idreal_product'] = $a['idarticle']; // pour compatiblité ascendante
					if (isset ($a['if'])) {
						$a["parm_fam_$i"] = $pf_x = $t['if'];
						$a["parm_name_$i"] = $pn_x = $t['it'];
						$a["parm_value_$i"] = $pv_x = $t['iv'];
						$a["cotation_$i"] = $ct_x = $t['cotation'];
						$a["level_$i"] = $t['level'];
						$a["fdisplay_$i"] = $t['df'];
						$a["idisplay_$i"] = $t['dt'];
					}
				}

				if (isset ($pf_x)) {
					$a['int'][$pf_x]['label'][$i] = $pn_x;
					$a['int'][$pf_x]['cot'][$i] = $ct_x;
					$a['int'][$pf_x]['display'][$i] = 0;
					$a['fam']['label'][$i] = $pf_x;
					$a['int'][$pf_x]['label'][$i] = $pn_x;
					$a[$pf_x][$pn_x] = $pv_x;
					$a["parm_fam_$i"] = $pf_x;
					$a[$pn_x] = $ct_x;
				}
			}

			$this->article[$j] = new DEPRECATEDARTICLE($a, true, $this->tissus, $selected_tissus, $this->couleurs, $selected_couleurs);

			$this->NBrealproduct = $j +1;
		} else {
			$this->NBrealproduct = 0;
			$this->article[0] = new DEPRECATEDARTICLE(array ());
		}
		//die('END PRODUCT::SetArticle('.$this->id.')');
	}

	/**
	 * Retourne le texte généré à partir des intitulés
	 */
	function GetTextFromIntitule() {
		$txt = '';
		$newline = '';
		for ($i = 1; $i < 31; $i++) {
			if ($this->article[0]->getvalue("parm_text_$i")) {
				$t = $this->_GetTextFromValues($i);
				if ($t == '')
					continue;
				$txt .= $newline . '<b>' . $this->article[0]->getvalue("parm_name_$i") . ' : </b>';
				$txt .= $t;
				$newline = " <br />\n";
			}
		}
		return $txt;
	}

	/**
	 * Retourne le texte des valeurs des intitulés
	 * @param int $j Index de l'intitulé
	 */
	private function _GetTextFromValues($j) {
		$a = array ();
		for ($i = 0; $i < $this->NBrealproduct; $i++) {
			$a[] = $this->article[$i]->getvalue("parm_value_$j");
		}
		return implode(', ', array_unique($a));
	}

	/**
	 * Calcule le nombre de produits à afficher sur la page
	 * @param int $NbByPages le nombre de produits par pages
	 * @param int $CurrentPageNumber la page courante
	 */
	public function CalculateLimiteToDisplay($NbByPages, $CurrentPageNumber) {
		if ($CurrentPageNumber < 1)
			$CurrentPageNumber = 1;
		$this->LimiteMin = $NbByPages * ($CurrentPageNumber -1);
		$this->LimiteMax = $NbByPages;
		return ($this->LimiteMin + $NbByPages);
	}

	/**
	 * Cherche les produit similaires
	 * @return array Tableau associatif clef->idprodtuit valeur->nom
	 */
	public function GetSimilar() {
		

		$similars = array ();
		$Base = & DBUtil::getConnection();
		$Langue = $this->lang;
		$simids = array ();
		$cr = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		for ($i = 1; $i < 4; $i++) {
			$s = $this->getvalue("idproduct_similar_$i");
			if ($s > 0)
				$simids[] = $s;
		}
		if (count($simids) == 0)
			return ($similars);
		$comma_separated = implode(",", array_unique($simids));
		for ($i = 1; $i < 4; $i++) {
			$IdSimilar = $this->id;
			$Query = "SELECT distinct p.idproduct as psid ,name$Langue as name FROM product p,detail d WHERE p.idproduct != '" .
			$this->id . "' AND p.idproduct=d.idproduct AND (d.stock_level>0 OR d.stock_level=-1 ) AND p.available=1 AND p.catalog_right=1=<$cr AND p.idproduct_similar_$i IN ($comma_separated)";

			//die ($Query);
			$rs = & DBUtil::query($Query);
			$n = $rs->RecordCount();
			if ($n) {
				for ($Compteur = 0; $Compteur < $n; $Compteur++) {
					$k = $rs->Fields('psid');
					$similars[$k] = $rs->Fields('name');
					$rs->MoveNext();
				}
			}
		}
		return ($similars);
	}

	/**
	 * Recherche le prix le plus bas
	 */
	public function GetMinPrice() {
		$minprice = $this->article[0]->GetWoPrice();
		for ($i = 1; $i < $this->NBrealproduct; $i++)
			min($minprice, $this->article[$i]->GetWoPrice());
		return $minprice;
	}

	/**
	 * Debugage
	 */
	function dump() {
		echo "<br />Produit : $this->id <br />\n";
		for ($i = 0; $i < $this->NBrealproduct; $i++)
			$this->article[$i]->dump();
		echo "<br />===============<br />\n";
	}

	//----------------------------------------------------------------------------------
	/**
	 * Vérifie si le produit possède une référence en promotion
	 * @static
	 * @return true si oui, false sinon
	 */
	public function hasPromo() {

		$db = & DBUtil::getConnection();

		$query = "
				SELECT COUNT(*) AS hasPromo 
				FROM detail d, promo p
				WHERE d.stock_level <> 0
				AND d.idproduct = {$this->id}
				AND d.reference = p.reference
				AND p.begin_date <= NOW()
				AND p.end_date >= NOW()";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer les promotions du produit");

		return $rs->fields("hasPromo");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Récupere la date de début d'une promo pour un produit
	 * @static
	 * @return la date de début
	 */
	public static function GetPromoRate($idproduct = "X") {

		$db = & DBUtil::getConnection();

		$gestStock = DBUtil::getParameter("use_stock_levels");
		$ANDLINES = "";
		if ($gestStock) {
			$ANDLINES = " AND d.stock_level<>0 ";
		}
		if ($idproduct == "X")
			$idprod = $this->id;
		else
			$idprod = $idproduct;

		$query = "
				SELECT MAX( p.rate ) AS promoRate
				FROM detail d, promo p
				WHERE d.idproduct = {$idprod}
				$ANDLINES
				AND d.reference = p.reference
				AND p.begin_date <= NOW()
				AND p.end_date >= NOW()";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer les promotions du produit");

		return $rs->fields("promoRate");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Récupere la date de début d'une promo pour un produit
	 * @static
	 * @return la date de début
	 */
	public static function GetBeginPromoDate($idproduct = "X") {

		$db = & DBUtil::getConnection();

		$gestStock = DBUtil::getParameter("use_stock_levels");
		$ANDLINES = "";
		if ($gestStock) {
			$ANDLINES = " AND d.stock_level<>0 ";
		}
		if ($idproduct == "X")
			$idprod = $this->id;
		else
			$idprod = $idproduct;

		$query = "
				SELECT MAX( p.begin_date ) AS beginDate
				FROM detail d, promo p
				WHERE d.idproduct = {$idprod}
				$ANDLINES
				AND d.reference = p.reference
				AND p.begin_date <= NOW()
				AND p.end_date >= NOW()";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer les promotions du produit");

		return $rs->fields("beginDate");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Récupere la date de fin d'une promo pour un produit
	 * @static
	 * @return la date de fin
	 */
	public static function GetEndPromoDate($idproduct = "X") {

		$db = & DBUtil::getConnection();

		$gestStock = DBUtil::getParameter("use_stock_levels");
		$ANDLINES = "";
		if ($gestStock) {
			$ANDLINES = " AND d.stock_level<>0 ";
		}

		if ($idproduct == "X")
			$idprod = $this->id;
		else
			$idprod = $idproduct;

		$query = "
				SELECT MAX( p.end_date ) AS endDate 
				FROM detail d, promo p
				WHERE d.idproduct = {$idprod}
				$ANDLINES
				AND d.reference = p.reference
				AND p.begin_date <= NOW()
				AND p.end_date >= NOW()";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer les promotions du produit");

		return $rs->fields("endDate");

	}

	//----------------------------------------------------------------------------------
	/**
	 * Vérifie s'il existe au moins une référence en stock pour ce produit
	 * @static
	 * @return true s'il existe au moins une référence en stock pour ce produit, sinon false
	 */

	public function hasAtLeastOneReferenceInStock() {

		$db = & DBUtil::getConnection();

		$query = "
				SELECT COUNT(*) AS hasStock 
				FROM detail d
				WHERE d.idproduct = {$this->id}
				AND d.stock_level <> 0";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount() || $rs->fields("hasStock") == null)
			die("Impossible de récupérer le nombre de références en stock pour ce produit");

		return $rs->fields("hasStock") ? true : false;

	}

	//----------------------------------------------------------------------------

	/**
	 * Vérifie si le produit possède une référence en déstockage
	 * @param string $reference la référence
	 * @return true si oui, false sinon
	 */
	public static function isDestocking($reference) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$date = time();
		$query = "
				SELECT COUNT(*) AS isDestocking 
				FROM destocking 
				WHERE reference LIKE '$reference' 
				AND UNIX_TIMESTAMP( begin_date ) <= '$date' and UNIX_TIMESTAMP( end_date ) >= '$date'
				LIMIT 1";

		$rs = $db->Execute($query);
		if ($rs === false || !$rs->RecordCount() || $rs->fields("isDestocking") == null)
			die("Impossible de vérifier si la référence est en déstockage ou non");

		$isDestocking = $rs->fields("isDestocking") ? true : false;

		return $isDestocking;

	}

	//----------------------------------------------------------------------------

	/**
	 * Retourne le stock pour une référence
	 * @param string $reference la référence
	 * @return true si oui, false sinon
	 */
	public static function getStockLevel($reference) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$date = time();
		$query = "
				SELECT stock_level FROM detail WHERE reference LIKE '$reference' LIMIT 1";

		$rs = $db->Execute($query);
		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer le stock disponible pour la référence $reference");

		return $rs->fields("stock_level");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Indique si un reference est en promotion
	 * @param string $reference la référence
	 * @return true si oui, false sinon
	 */
	public static function isPromo($reference) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$query = "
				SELECT COUNT(*) AS hasPromo 
				FROM detail d, promo p
				WHERE p.reference LIKE '$reference'
				AND d.stock_level <> 0
				AND d.reference = p.reference
				AND p.begin_date <= NOW()
				AND p.end_date >= NOW()";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount() || $rs->fields("hasPromo") == null)
			die("Impossible de vérifier si la référence est en promotion");

		return $rs->fields("hasPromo") ? true : false;

	}

	//----------------------------------------------------------------------------

	/**
	 * Retourne le taux de rabais si la référence est en déstockage
	 * @param string $reference la référence
	 * @return float le taux
	 */
	public static function getDestockingRate($reference) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$date = time();

		$query = "
				SELECT rate 
				FROM destocking 
				WHERE reference LIKE '$reference' 
				AND UNIX_TIMESTAMP( begin_date ) <= '$date' and UNIX_TIMESTAMP( end_date ) >= '$date'
				LIMIT 1";

		$rs = $db->Execute($query);
		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer le prix pour le déstockage de la référence $reference");

		return $rs->fields("rate");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Retourne les intitulés pourune référence
	 * @param string $reference la référence
	 * @return array le tableau des intitulés
	 */
	public static function getReferenceHeadings($reference) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();
		$lang = "_1";

		$query = "
				SELECT intitule_title$lang AS title, intitule_value$lang AS value
				FROM intitule_link il, intitule_title it, intitule_value iv, detail d
				WHERE d.reference LIKE '$reference'
				AND d.idarticle = il.idarticle
				AND il.idintitule_title = it.idintitule_title
				AND il.idintitule_value = iv.idintitule_value";

		$rs = $db->Execute($query);
		if ($rs === false)
			die("Impossible de récupérer les intitulés pour la référence $reference");

		$headings = array ();

		while (!$rs->EOF()) {

			$headings[$rs->fields("title")] = $rs->fields("value");

			$rs->MoveNext();

		}

		return $headings;

	}

	/**
	 * Recherche la référence la moins chère pour un produit donné ( utilisé pour afficher "à partir de xxx euros" dans le front office )
	 * @static
	 * @param int idproduct l'identifiant du produit pour lequel on cherche le plus petit prix
	 * @return le prix de la référence la moins chère pour le produit spécifié
	 */

	public static function getLowerPriceIdArticle($idproduct) {

		include_once( dirname( __FILE__ ) . "/catalog/CProduct.php" );
		
		$product = new CProduct( $idproduct );
		
		$prices = array();
		$it = $product->getArticles()->iterator();

		while( $it->hasNext() ){
			
			$article =& $it->next();
			$prices[ $article->getId() ] = $article->getDiscountPriceET();
			
		}

		asort( $prices );

		foreach( $prices as $idarticle => $price )
			if( $price > 0.0 )
				return $idarticle;

		return false;

	}
	//----------------------------------------------------------------------------------

	/**
	 * Retourne une valeur spécifique pour une référence
	 * @param string $reference la référence
	 * @param string $fieldname la valeur à retourner
	 */
	public  function getReferenceAttribute($reference, $fieldname) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$query = "SELECT `$fieldname` FROM detail WHERE reference LIKE '$reference' LIMIT 1";

		$rs = $db->Execute($query);
		if ($rs === false)
			die("Impossible de récupérer la valeur du champ '$fieldname' pour la référence '$reference'");

		return $rs->fields($fieldname);

	}

	//----------------------------------------------------------------------------------

	/**
	 * Retourne une valeur spécifique pour un produit
	 * @param int $idproduct l'id du produit
	 * @param string $fieldname la valeur à retourner
	 */
	public static function getProductAttribute($idproduct, $fieldname) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$query = "SELECT `$fieldname` FROM product WHERE idproduct = '$idproduct' LIMIT 1";

		$rs = $db->Execute($query);
		if ($rs === false)
			die("Impossible de récupérer la valeur du champ '$fieldname' pour le produit '$idproduct'");

		return $rs->fields($fieldname);

	}

	//----------------------------------------------------------------------------------
	/**
	 * Retourne le nombre de références
	 */
	public static function GetReferenceCount() {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$query = "
				SELECT COUNT(*) AS count
				FROM detail 
				WHERE stock_level <> 0";

		$rs = $db->Execute($query);
		if ($rs === false)
			die("Impossible de récupérer le nombre de référence");

		return $rs->fields("count");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Retourne le nombre de références affichables dans le catalogue
	 * @static
	 * @access public
	 * @param int $idproduct l'identifiant du produit
	 */

	public static function getAvailableReferenceCount($idproduct) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$query = "
		SELECT COUNT(*) AS count
		FROM detail
		WHERE idproduct = '$idproduct'
		AND `type` IN( 'catalog', 'specific', 'destocking' )
		AND stock_level <> 0
		AND available = 1";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer le nombre de références pour le produit '$idproduct'");

		return $rs->fields("count");

	}

	//----------------------------------------------------------------------------------

	/**
	 * Retourne le nombre de références affichables dans le catalogue
	 * @static
	 * @access public
	 * @param int $idproduct l'identifiant du produit
	 */

	public static function hasPromotionInStock($idproduct) {

		 //pour contexte statique

		$db = & DBUtil::getConnection();

		$query = "
		SELECT COUNT(*) AS hasPromo 
		FROM detail d, promo p
		WHERE d.stock_level <> 0
		AND d.idproduct = '$idproduct'
		AND d.reference = p.reference
		AND p.begin_date <= NOW()
		AND p.end_date >= NOW()";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer les promotions du produit");

		return $rs->fields("hasPromo") > 0 ? true : false;

	}

	//----------------------------------------------------------------------------------

	/**
	 * Permet d'ajouter un commentaire à un produit
	 * @access public
	 * @param String $productComment commentaire du produit
	 * @param int    $productGrade   note du produit
	 */
	public function addComment($productComment, $productGrade) {
		

		$db = & DBUtil::getConnection();

		$buyerId = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$contactId = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getId() : 0;
				
		$query = "INSERT INTO product_comment (
			idbuyer, 
			idcontact, 
			idproduct, 
			product_comment, 
			date,
			accept_admin, 
			note_buyer) 
		VALUES ('$buyerId', 
		'$contactId', 
		'$this->id', 
		'$productComment',
		NOW(),
		0,
		'$productGrade' 
		)";

		;
		if (!DBUtil::query($query))
			die("Impossible d'ajouter le commentaire");
	}

	//----------------------------------------------------------------------------------

	/**
	 * Permet de retourner les commentaires du produit courant
	 * @access public
	 * @return $tabComments tableau de commentaires propres au produit
	 */
	public function getComments() {
		$db = & DBUtil::getConnection();

		$query = "SELECT product_comment.idbuyer,
						 product_comment.product_comment,
						 product_comment.note_buyer,
						 product_comment.date,
						 contact.firstname,
						 contact.lastname
						 FROM product_comment JOIN contact
						 ON contact.idcontact = product_comment.idcontact
						 AND contact.idbuyer = product_comment.idbuyer
						 WHERE product_comment.idproduct = $this->id
						 AND product_comment.accept_admin = '1'
						 ORDER BY product_comment.date DESC";

		$rs = $db->Execute($query);

		return $rs;
	}

	//----------------------------------------------------------------------------------

	/**
	 * Permet de retourner les commentaires du produit courant
	 * @access public
	 * @return $tabComments tableau de commentaires propres au produit
	 */
	public function availableComment() {
		$db = & DBUtil::getConnection();
		
		$buyerId = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		
		$query = "	SELECT COUNT(idbuyer) as count
					FROM product_comment
					WHERE idproduct = $this->id
					AND idbuyer = $buyerId";

		$rs = $db->Execute($query);
	
		$available = true;
		
		if($rs->fields("count") > 0)
			$available = false;
			
		return $available;
	}
	
	//----------------------------------------------------------------------------------

	/**
	 * Permet de retourner le nombre de commentaire sur un produit
	 * @access public
	 * @return $nbComments nombre de commentaire du produit
	 */
	public function getNbComments() {
		$db = & DBUtil::getConnection();

		$query = "SELECT * FROM product_comment WHERE idproduct = '$this->id' AND accept_admin = '1'";

		$rs = $db->Execute($query);

		$nbComments = $rs->RecordCount();

		return $nbComments;
	}

	//----------------------------------------------------------------------------------

	/**
	 * Méthode qui calcul et renvoie la moyenne des notes attribuées au produit
	 * @access public
	 * @return $avg moyenne des notes du produit
	 */
	public function getCommentsAvg() {
		$db = & DBUtil::getConnection();

		$query = "SELECT note_buyer FROM product_comment WHERE idproduct = '$this->id' AND accept_admin = '1'";

		$rs = $db->Execute($query);

		$totalGrades = 0;
		while (!$rs->EOF){
			$totalGrades += $rs->fields('note_buyer');
			$rs->MoveNext();
		}
		$avg = $totalGrades / $this->getNbComments();

		return $avg;
	}

	//----------------------------------------------------------------------------------	

	/**
	 * Retourne true si le produit est disponible à l'affichage dans le front office
	 * Dépend :
	 * 	- des droits de consultation du produit et des catégories parentes
	 *  - du masquage du produit ou d'une des catégories parentes
	 *  - du nombre de références disponibles
	 *  - du stock disponible
	 * @access public
	 * @static
	 * @return bool
	 */
	public static function isAvailable( $idproduct ) {

		/*pas de produits spécifiques ou alternatifs disponible*/
		
		if( !$idproduct )
			return false;
		
		/*vérifier si le produit existe*/
		
		$rs =& DBUtil::query( "SELECT idproduct FROM product WHERE idproduct = '$idproduct' LIMIT 1" );
		
		if( !$rs->RecordCount() )
			return false;
				
		/*masquage du produit*/

		if (!DBUtil::getDBValue("available", "product", "idproduct", $idproduct))
			return false;
			
		/*droits de consultation du visiteur*/

		$buyerCatalogRight = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$productCatalogRight = DBUtil::getDBValue("catalog_right", "product", "idproduct", $idproduct);

		if ($productCatalogRight > $buyerCatalogRight)
			return false;

		/*récupérer toutes les catégories parentes afin d'examiner leur disponibilité*/
	
		$idcategory = DBUtil::query( "SELECT idcategory FROM product WHERE idproduct = '$idproduct' LIMIT 1" )->fields( "idcategory" );
	
		$parents = array( $idcategory );
		$idcategorychild = $idcategory;
		$done = false;
		while( !$done ){
			
			$rs =& DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategorychild' LIMIT 1" );
			
			$done = $rs->RecordCount() == 0 || $rs->fields( "idcategoryparent" ) == 0;
			
			if( !$done )
				$parents[] = $idcategorychild = $rs->fields( "idcategoryparent" );
			
		}

		/*droits de consultation des catégories parentes*/
		
		foreach( $parents as $idparent ){
			
			if( !DBUtil::getDBValue( "available", "category", "idcategory", $idparent ) )
				return false;
		
		}
		
		/*droits de consultation du visiteur pour les catégories parentes*/
		
		foreach( $parents as $idparent ){
			
			$parentCatalogRight = DBUtil::getDBValue( "catalog_right", "category", "idcategory", $idparent );
			
			if( $parentCatalogRight > $buyerCatalogRight )
				return false;
			
		}

		/*vérifier qu'il y ait au moins une référence de disponible*/

		$rs = & DBUtil::query( "SELECT COUNT(*) AS referenceCount FROM detail WHERE idproduct = '$idproduct' AND available = 1 AND stock_level <> 0" );

		if ( !$rs->fields( "referenceCount" ) )
			return false;

		return true;

	}

	//----------------------------------------------------------------------------------
	
	public static function patchHeadings( $idproduct ){

		if( !intval( $idproduct ) )
			return;
		
		$query = "
		SELECT DISTINCT( idfamily ), MAX( family_display ) AS family_display
		FROM intitule_link
		WHERE idproduct = '$idproduct'
		GROUP BY idfamily
		ORDER BY MAX( family_display ) ASC";
		
		$rs =& DBUtil::query( $query );
		
		$affectedFamilies 	= 0;
		$family_display 	= 1;
		while( !$rs->EOF() ){
		
			$query = "
			UPDATE intitule_link
			SET family_display = '$family_display'
			WHERE idproduct = '$idproduct'
			AND idfamily = '" . $rs->fields( "idfamily" ) . "'";
			
			DBUtil::query( $query );
			
			$affectedFamilies +=  DBUtil::getAffectedRows();
			
			$rs->MoveNext();
			$family_display++;
			
		}
		
		$query = "
		SELECT DISTINCT( idintitule_title ) AS idintitule_title, MAX( idfamily ) AS idfamily, MAX( family_display ) AS family_display, MAX( title_display ) AS title_display
		FROM intitule_link
		WHERE idproduct = '$idproduct' 
		GROUP BY idintitule_title
		ORDER BY MAX( idfamily ) ASC, MAX( title_display ) ASC";
		
		$rs =& DBUtil::query( $query );
		
		$title_display 		= 1;
		$last_idfamily 		= false;
		$affectedHeadings 	= 0;
		
		while( !$rs->EOF() ){
			
			if( $last_idfamily !== false && $rs->fields( "idfamily" ) == $last_idfamily )
				$title_display++;
			else $title_display = 1;
			
			$query = "
			UPDATE intitule_link
			SET title_display = '$title_display',
			idfamily = '" . $rs->fields( "idfamily" ) . "',
			family_display = '" . $rs->fields( "family_display" ) . "'
			WHERE idproduct = '$idproduct'
			AND idintitule_title = '" . $rs->fields( "idintitule_title" ) . "'";
			
			DBUtil::query( $query );

			$affectedHeadings += DBUtil::getAffectedRows();

			$last_idfamily = $rs->fields( "idfamily" );
			$rs->MoveNext();
			
		}

		if( $affectedHeadings || $affectedFamilies ){
				
			$fp = fopen( dirname( __FILE__ ) . "/../logs/headings.txt", "a+" );
			
			fwrite( $fp, date( "Y-m-d H:i:s" ) . ", idproduct=$idproduct, " . User::getInstance()->get( "login" ) . ", affected families : $affectedFamilies, affected headings : $affectedHeadings\n" );
			fclose( $fp );
				
		}
			
	}
	
	//----------------------------------------------------------------------------------

	/**
	 * Recherche le prix de la référence la moins chère pour un produit donné ( utilisé pour afficher "à partir de xxx euros" dans le front office )
	 * @static
	 * @deprecated
	 * @param int idproduct l'identifiant du produit pour lequel on cherche le plus petit prix
	 * @return le prix de la référence la moins chère pour le produit spécifié
	 */

	public static function getLowerPrice($idproduct) {

		include_once( dirname( __FILE__ ) . "/catalog/CProduct.php" );
		
		$product = new CProduct( $idproduct );
		
		return $product->getLowerPriceET();
	
	}
	
	
	//----------------------------------------------------------------------------------
	
} //EOC
?>