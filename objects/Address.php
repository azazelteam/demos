<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresse client
 */
 
interface Address{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string la raison sociale
	 */
	public function getCompany();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le prénom
	 */
	public function getFirstName();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le nom de famille
	 */
	public function getLastName();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return int l'identifiant de la civilité
	 */
	public function getGender();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le numéro de téléphone
	 */
	public function getPhonenumber();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le numéro de fax
	 */
	public function getFaxnumber();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le numéro de GSM
	 */
	public function getGSM();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string l'adresse email
	 */
	public function getEmail();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string l'adresse
	 */
	public function getAddress();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le complément d'adresse
	 */
	public function getAddress2();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le code postal
	 */
	public function getZipcode();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return string le nom de la ville
	 */
	public function getCity();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return int l'identifiant du pays
	 */
	public function getIdState();
	
	/* ---------------------------------------------------------------------------------------------------- */

}

?>