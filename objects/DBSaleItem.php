<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object lignes factures clients
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/TradeItem.php" );



abstract class DBSaleItem extends DBObject implements TradeItem{

	//----------------------------------------------------------------------------
	/**
	 * @var DBSale
	 */
	protected $sale;

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBObject#save()
	 */
	public function save(){
		
		$this->recordSet[ "updated" ] = date( "Y-m-d H:i:s" );
		parent::save();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getQuantity()
	 */
	public function getQuantity(){ return $this->recordSet[ "quantity" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getPriceET()
	 */
	public function getPriceET(){
	
		if( B2B_STRATEGY )
			return $this->recordSet[ "unit_price" ];

		return round( $this->recordSet[ "unit_price" ] / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getPriceATI()
	 */
	public function getPriceATI(){
	
		if( !B2B_STRATEGY || !$this->sale->applyVAT() )
			return $this->recordSet[ "unit_price" ];
			
		return round( $this->recordSet[ "unit_price" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );

	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountPriceET()
	 */
	public function getDiscountPriceET(){ 
		
		if( B2B_STRATEGY )
			return $this->recordSet[ "discount_price" ];

		return round( $this->recordSet[ "discount_price" ] / ( 1.0 + $this->getVATRate() / 100.0 ), 2 ); 
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountPriceATI()
	 */
	public function getDiscountPriceATI(){
	
		if( !B2B_STRATEGY || !$this->sale->applyVAT() )
			return $this->recordSet[ "discount_price" ];
			
		return round( $this->recordSet[ "discount_price" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );

	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountRate()
	 */
	public function getDiscountRate(){ 
	
		/*if( B2B_STRATEGY || !$this->sale->applyVAT() )
			return $this->getPriceET() > 0.0 ? 100.0 * ( 1.0 - $this->getDiscountPriceET() / $this->getPriceET() ) : 0.0;

		return $this->getPriceATI() > 0.0 ? 100.0 * ( 1.0 - $this->getDiscountPriceATI() / $this->getPriceATI() ) : 0.0;*/
		
		return $this->recordSet[ "ref_discount" ];
	
	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getVATRate()
	 */
	public function getVATRate(){ return $this->sale->applyVAT() ? $this->recordSet[ "vat_rate" ] : 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getWeight()
	 */
	public function getWeight(){ return $this->recordSet[ "weight" ]; }

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#isPostagePaid()
	 */
	public function isPostagePaid(){ return $this->recordSet[ "franco" ] == 1; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getTotalET(){ 	return $this->getQuantity() * $this->getDiscountPriceET(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getTotalATI(){ 	return $this->getQuantity() * $this->getDiscountPriceATI(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @return string
	 */
	public function getImageURI(){ return URLFactory::getReferenceImageURI( $this->recordSet[ "idarticle" ], URLFactory::$IMAGE_ICON_SIZE ); }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return Color or false
	 * @todo couleurs absentes dans la facture
	 */
	public function getColor(){
		
		if( !isset( $this->recordSet[ "idcolor" ] ) || !$this->recordSet[ "idcolor" ] ||  !$this->recordSet[ "idarticle" ] )
			return false;

		static $color = NULL;
		
		if( $color !== NULL )
			return $color;
			
		include_once( dirname( __FILE__ ) . "/Color.php" );
		
		$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $this->recordSet[ "idarticle" ] );
		$color = new Color( $idproduct, intval( $this->recordSet[ "idcolor" ] ) );
		
		return $color;
		
	}

	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return Cloth or false
	 * @todo tissus absents dans la facture
	 */
	 public function getCloth(){
	 	
	 	if( !isset( $this->recordSet[ "idcloth" ] ) || !$this->recordSet[ "idcloth" ] || !$this->recordSet[ "idarticle" ] )
			return false;

		static $cloth = NULL;
		
		if( $cloth !== NULL )
			return $cloth;
			
		include_once( dirname( __FILE__ ) . "/Cloth.php" );
		
		$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $this->recordSet[ "idarticle" ] );
		$cloth = new Cloth( $idproduct, intval( $this->recordSet[ "idcloth" ] ) );

		return $cloth;
		
	 }
	 
	 /* ------------------------------------------------------------------------------------------ */
	
}

?>