<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object édition adresses clients
 */

include_once( dirname( __FILE__ ) . "/Address.php" );

interface EditableAddress extends Address{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * identifiant unique
	 * @return mixed
	 */
	public function getId();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $company
	 * @return void
	 */
	public function setCompany( $company );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $firstname
	 * @return void
	 */
	public function setFirstName( $firstname );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $lastname
	 * @return void
	 */
	public function setLastName( $lastname );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idtitle
	 * @return void
	 */
	public function setGender( $idtitle );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $phonenumber
	 * @return void
	 */
	public function setPhonenumber( $phonenumber );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $faxnumber
	 * @return string
	 */
	public function setFaxnumber( $faxnumber );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $gsm
	 * @return string
	 */
	public function setGSM( $gsm );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $email
	 * @return string
	 */
	public function setEmail( $email );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $address
	 * @return void
	 */
	public function setAddress( $address );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $address2
	 * @return void
	 */
	public function setAddress2( $address2 );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $zipcode
	 * @return void
	 */
	public function setZipcode( $zipcode );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param string $city
	 * @return void
	 */
	public function setCity( $city );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idstate
	 * @return void
	 */
	public function setIdState( $idstate );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return void
	 */
	public function save();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return bool
	 */
	public function allowDelete();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @param int $identifier
	 * @return void
	 */
	public static function delete( $identifier );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return EditableAddress
	 */
	public static function create( $identifier );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return EditableAddress
	 */
	public function duplicate();
	
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>