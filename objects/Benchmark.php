<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ...? est-ce utilisé ?
 */
 
include_once ( "../../script/configdb.php" ); 
include_once( "$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/adm_base.php" ); 

define( "DEBUG", "1" );

class Benchmark{

	//data members

	var $urls;
	var $counts;
	var $elapsedTimes;
	var $datetimes;
  
	//----------------------------------------------------------------------------
	/*
	 * Constructeur
	 */
	function Benchmark(){

		$this->urls = array();
		$this->counts = array();
		$this->elapsedTimes = array();
		$this->datetimes = array();
		
	}

	//----------------------------------------------------------------------------
	/*
	 * Retourne la index-ième URL testée
	 * @param index	index de l'URL
	 */
	function GetURLAt( $index ){
	
		return $this->URIs[ $index ];
		
	}


	//----------------------------------------------------------------------------
	/*
	 * Retourne le nombre de fois où la index-ième URL sera testée
	 * @param index	index de l'URL
	 */
	function GetURLGetCountAt( $index ){
	
		return $this->counts[ $index ];
		
	}

	//----------------------------------------------------------------------------
	/*
	 * Retourne le temps moyen de chargement de la index-ième URL
	 * @param index	index de l'URL
	 */
	function GetURLAverageTimeAt( $index ){
	
		$average_time = 0;
		$i = 0;
		while( $i < count( $this->elapsedTimes[ $index ] ) ){
		
			$average_time += $this->elapsedTimes[ $index ][ $i ];
		
			$i++;
			
		}
		
		return $average_time / $i;
		
	}
	
	//----------------------------------------------------------------------------
	/*
	 * Ajoute une nouvelle URI à tester
	 * @param uri	URI à tester
	 * @param count	nombre de fois où le test sera répété
	 */
	function AddURI( $uri, $count ){

		$url = "http://" . $_SERVER['HTTP_HOST'] . "/" . $uri;
		
		array_push( $this->urls, $url );
		array_push( $this->counts, $count );
		array_push( $this->elapsedTimes, array() );

	}

	//----------------------------------------------------------------------------
	/*
	 * Exécute la série de tests
	 */
	function Run(){

		$i = 0;
		while( $i < count( $this->urls ) ){
			
			$url = $this->urls[ $i ];
			
			$j = 0;
			
			while( $j < $this->counts[ $i ] ){

				list( $usec, $sec ) = explode( " ", microtime() );
				$start_time = (float)$usec + (float)$sec;

				$elapsed_time = 0;
				
				if( file_get_contents( $url ) ){
					
					list( $usec, $sec ) = explode( " ", microtime() );
					$end_time = (float)$usec + (float)$sec;
					
					$elapsed_time += $end_time - $start_time;
					$this->elapsedTimes[ $i ][ $j ] = $elapsed_time;
	
					if( DEBUG )
						echo "
						<br />[$elapsed_time sec] GET {$url }";
						
				}
				else{
				
					$this->elapsedTimes[ $i ][ $j ] = 0;
					
					if( DEBUG )
						echo "
						<br />[ERROR] GET {$url }";
						
				}
					
				$j++;

			}
			
			$this->datetimes[ $i ] = date("Y-m-d H:i:s");

			if( DEBUG )
				echo "
				<p>AVERAGE TIME : " . ceil( $this->GetURLAverageTimeAt( $i ) * 1000 ) . " ms, " . $this->GetURLGetCountAt( $i ) . " GETS</p>";

			$i++;

		}

	}

	
//----------------------------------------------------------------------------

	 function Clear(){
	 
	 	$this->uris = array();
		$this->counts = array();
		$this->elapsedTimes = array();
		$this->datetimes = array();
		
	}
	 
//----------------------------------------------------------------------------

	 function LoadFromDatabase(){
	 
	 	$this->Clear();
					
		$query = "SELECT * FROM benchmarks";
		$result = DBUtil::getConnection()->Execute( $query );
		
		while( $get = adodb_fet_object( $result ) ){
		
			$this->AddURI( $get->uri, $get->count );
			array_push( $this->datetimes, $get->datetime );
			
		}			

	 }

	//----------------------------------------------------------------------------

	function SaveToDatabase(){
		
		$query = "DELETE FROM benchmarks WHERE 1";
		
		DBUtil::getConnection()->Execute( $query );
		
		$i = 0;
		while( $i < count( $this->urls ) ){
		
			$elapsedtime = $this->GetURLAverageTimeAt( $i );
			$datetime = $this->datetimes[ $i ];
			
			$query = "INSERT INTO benchmarks( url, elapsedtime, datetime ) VALUES( '{$this->urls[ $i ]}', '{$elapsedtime}', '$datetime' )";
			DBUtil::getConnection()->Execute( $query );
			
			$i++;
			
		}
		
	}
	
	//----------------------------------------------------------------------------

}

?>

