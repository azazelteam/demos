<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object nouveau produits
 */

include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/catalog/CProduct.php" );

class CATALNOVELTY
{
	private $id;
	private $buyer;
	private $lang;
	private $LimiteMin; 
	private $LimiteMax;
	private $LimiteMinP; 
	private $LimiteMaxP;
	private $products;
	private $NBProduct;
	private $CurrProduct;
	private $prodnotloaded;
	private $NbByPages;

	/**
	 * Contructeur
	 */
	function __construct()
		{
		$this->id = 'novelty';
		$this->buyer = 0 ;
		$this->lang ='_1';
		$this->LimiteMin = 0; 
		$this->LimiteMax = 1;
		$this->LimiteMinP = 0; 
		$this->LimiteMaxP = 1;
		$this->NBProduct = 0;
		$this->CurrProduct = 0 ;
		$this->prodnotloaded = 1;
		$this->NbByPages = 12;
		$reload=1;
		$this->buyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$this->lang = "_1";
		//if( isset($_SESSION['noveltyproducts'])  ) $reload=0; 
		if( $reload ) { $this->SetProducts(); }
		else { 
			$this->products = &$_SESSION['noveltyproducts'] ;
			$this->NBProduct = 0 +  count($this->products);
			}

			//$this->LoadProducts();
		}
	
	/**
	 * Retourne le nombre des produits
	 * @return int le nombre de produits
	 */
	public function GetNbProductsInCategory() { return $this->NBProduct; }
	
	/**
	 * Retourne le nombre des produits
	 * @return int le nombre de produits
	 */
	public function GetNBProduct() { return $this->NBProduct; }
	
	/**
	 * Retourne novelty
	 * @return int l'id novelty
	 */
	public function getid() { return $this->id; }
	
	/**
	 * Retourne l'id du produit courrant
	 * @return int l'id du produit courant
	 */
	public function GetProdductId() { $i = $this->CurrProduct; return  $this->products[$i]['idproduct']; }
	
	/**
	 * Retourne le nom du produit courrant
	 * @return string le nom du produit
	 */
	public function GetName() { $i = $this->CurrProduct;return  "PRODUIT $i ".$this->products[$i]['name']; }
	
	/**
	 * @deprecated
	 * Retourne l'iône du produit courrant
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	function GetIcon($origine = '../www') { 
		$result='';
		}
	
	/**
	 * Retourne l'objet produit d'un des produits similaires
	 * @param int $numprod L'index du produit
	 * @return object $p l'objet du produit similaire
	 */
	public function &GetProduct($numprod) {
		$p = new PRODUCT($this->products[$numprod]['idproduct']);
		return $p;
		//if($this->prodnotloaded) $this->LoadProducts(); return $this->productinfos[$numprod];
		}
	/**
	 * Retourne l'objet produit d'un des produits similaires
	 * @param int $numprod L'index du produit
	 * @return object $p l'objet du produit similaire
	 */
	public function &GetProduct2($numprod) {
		$p = new CProduct($numprod);
		return $p;
		//if($this->prodnotloaded) $this->LoadProducts(); return $this->productinfos[$numprod];
		}
	
	/**
	 * Navigation
	 * Premier produit de la page
	 * @param int $CurrentPageNumber la page courante
	 * @return void
	 */
	public function FirstProduct($CurrentPageNumber=1) { 
		//$this->NbByPages = DBUtil::getParameter('nb_products_by_pages');
		if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
	        $this->LimiteMinP = $this->NbByPages * ($CurrentPageNumber-1);
	        $this->LimiteMaxP = min($this->LimiteMinP + $this->NbByPages, $this->NBProduct);
		$this->CurrProduct = $this->LimiteMinP - 1 ;
	  
	}
	
	/**
	 * Navigation
	 * Produit suivant
	 * @return int l'id du produit, 0 si dernier
	 */
	public function NextProduct() { $this->CurrProduct++; if( $this->CurrProduct > $this->LimiteMaxP) return 0; else return $this->CurrProduct; }
	
	/**
	 * Navigation
	 * Indique si il y a plus de produits
	 * @return int 1 si oui, 0 sinon
	 */
	public function MoreProducts() { if( $this->CurrProduct < $this->LimiteMaxP-1) return 1; return 0; }
	
	/**
	 * Navigation
	 * Indique si il y a plus de pages produits
	 * @return int 1 si oui, 0 sinon
	 */
	public function MoreProdPages() { if( $this->LimiteMaxP < $this->NBProduct ) return 1; return 0; }
	
	/**
	 * Calcule l'intervalle des produits à afficher dans la page
	 * @param int $NbByPages le nombre de produits par page
	 * @param int $CurrentPageNumber la page courrante
	 * @return int l'intervalle
	 */
	public function CalculateLimiteProdToDisplay( $NbByPages,$CurrentPageNumber)
	{	
		$this->NbByPages = $NbByPages ;
		if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
	        $this->LimiteMinP = $NbByPages * ($CurrentPageNumber-1);
	        $this->LimiteMaxP = min($this->LimiteMinP + $NbByPages, $this->NBProduct);
		return ($this->LimiteMinP) ;
	}
	
	/**
	 * Calcule l'index du premier produit à afficher
	 * @param int $NbByPages le nombre de produits par page
	 * @param int $CurrentPageNumber la page courrante
	 * @return int la limite
	 */
	public function CalculateLimiteToDisplay( $NbByPages, $CurrentPageNumber)
	{	
		// $NbByPages = DBUtil::getParameter('nb_categories_by_pages');
		if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
	        $this->LimiteMin = $NbByPages * ($CurrentPageNumber-1);
	        $this->LimiteMax = $NbByPages;
		return ($this->LimiteMin + $NbByPages) ;
	}
	
	/**
	 * Recherche les nouveaux produits
	 * @return int Le nombre de produits
	 */
	public function SetProducts()
		{
		$Langue = $this->lang;
		$Base = &DBUtil::getConnection();
	        $Time=time();
	        $Query ="SELECT idproduct,novelty,name_1 as name FROM product WHERE novelty<>'' and novelty >= NOW() ORDER BY novelty desc";
		$rs=& DBUtil::query($Query);
		if($rs->RecordCount()>0)
			{
			$this->NBProduct = $rs->RecordCount();
			for($j=0 ; $j < $this->NBProduct ; $j++)
				{
				$this->products[] = $rs->fields;
				$rs->MoveNext();
				}
			}
		else 
			{
			$this->products = array();
			$this->NBProduct = 0;
			}
		return $this->NBProduct ;
		}	
	
	/**
	 * Charge les produits
	 * @return void
	 */
	public function LoadProducts()
	{
	 $this->productinfos = array();
	 for($i=0; $i < $this->NBProduct ; $i++) $this->productinfos[] = new PRODUCT($this->products[$i]);
	 $this->prodnotloaded = 0;
	}

	public function GetAllProduct(){
		return $this->products;
	}

	
	/**
	 * debugage
	 */	
	function dump()
	{
	  echo "<br />CatalPromo : $this->id <br />\n";
	  print_r($this->products);
	  echo "<br />---------------<br />\n";
	}

}//EOC
?>