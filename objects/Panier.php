<?php

include_once("flexy/proxies/BasketProxy.php");
//include_once("catalog/BasketProxy.php");
include_once("URLFactory.php");

class MenuPanier{
	
	protected $_basket;
	protected $html;

	public function __construct(BasketProxy $Basket){
		$this->_basket = $Basket;
		$this->html = "";
	}

	public function PanierMenu(){
		$this->html.="<div class=\"menu-container\">
        <div class=\"menu\">";
		$this->createMenu();
		$this->html.="</div>
			</div>";
	}

	public function createMenu(){

		$it = $this->_basket->getItems();


		
$this->html .= "<div id='panierMenuContainer'>";
		$this->html .= "<table>";
		if(count($it)){
			for($i=0;$i<count($it);$i++){
				$article = $it[$i]->getArticle();
					if( B2B_STRATEGY ){
				$this->BuildArticleItem($article,$it[$i]->getQuantity(),$it[$i]->getTotalATI());
					
					}else{	
				$this->BuildArticleItem($article,$it[$i]->getQuantity(),$it[$i]->getTotalET());					
					}
			}
		}else{
			$this->html .= "<tr id='panierFooter'>";
				$this->html .= "<td colspan='3'>";
					$this->html .= 'Aucun article dans le panier';
				$this->html .= "</td>";
			$this->html .= "</tr>";
		}
		$this->html .= "<tr id='panierFooter'>";
			$this->html .= "<td colspan='3'>";
				$this->html .= '<a href="/catalog/basket.php"> Voir le panier </a>';
			$this->html .= "</td>";
		$this->html .= "</tr>";
		$this->html .= "</table>";
$this->html .= "</ul>";


	}

	public function BuildArticleItem($article, $quatity,$price){
		$ImageUrl = URLFactory::getProductImageURI($article->get("idproduct"))? URLFactory::getProductImageURI($article->get("idproduct")) :"/images/products/default.jpg";

		$this->html .= "<tr>";
			$this->html .= "<td rowspan=2>";
				$this->html .= "<img src='$ImageUrl' style='height:50px;'/>";
			$this->html .= "</td>";
				
			$this->html .= "<td colspan=2>";
				$this->html .= "<b>" . $article->GetProductName() . "</b>";
			$this->html .= "</td>";

		$this->html .= "</tr>";
		$this->html .= "<tr>";
			$this->html .= "<td>";
				$this->html .= "Quantité : " . $quatity;
			$this->html .= "</td>";
			$this->html .= "<td>";
				$this->html .=  $price;
			$this->html .= "</td>";
		$this->html .= "</tr>";
	}

	public function GeneratePanierMenu(){
		
		$this->createMenu();
		return $this->html;
	}
}



?>