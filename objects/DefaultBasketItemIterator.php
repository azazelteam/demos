<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object itération ligné article panier
 */
 

include_once( dirname( __FILE__ ) . "/util/IIterator.php" );

/**
 * Itérateur pour les lignes articles du panier
 * @see SimpleIterator
 */
 
class DefaultBasketItemIterator implements IIterator{
	
	//----------------------------------------------------------------------------
	//data members
	
	private $current;
	private $collection;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param $items un tableau d'instances de BasketItem
	 */
	 
	public function __construct( array &$items ){
		
		$this->collection 	=& $items;
 		$this->current 		= NULL;
 		
    }
     
    //----------------------------------------------------------------------------
	  
    /**
    * Retourne true si la collection possède d'autres éléments
    * @access public
    * @return boolean
    */
    
    public function hasNext(){
    	
    	$elementCount = count( $this->collection );
    	
    	if( !$elementCount )
    		return false;
    	
    	if( $this->current == NULL )
    		return true;
    			
    	return $this->current !== $this->collection[ $elementCount - 1 ];
    	
    }
    
    //----------------------------------------------------------------------------

    /**
    * Retourne le prochain élément de l'itération
    * @access public
    * @return BasketItem
    */
    
    public function &next(){
    	
    	if( !$this->hasNext() )
    		return NULL;
    		
    	if( $this->current == NULL ){
    		
    		$this->current =& $this->collection[ 0 ];
    		
    		return $this->current;
    			
    	}
    	
    	$i = 0;
    	while( $i < count( $this->collection ) - 1 ){
    		
    		if( $this->collection[ $i ] === $this->current ){

				$this->current =& $this->collection[ $i + 1 ];
				
    			return $this->current;
    				
    		}
    		
    		$i++;
    		
    	}
    	
    }
    
    //----------------------------------------------------------------------------

    /**
     * Supprime l'élément courant de la collection
     * @access public
     * @todo
     * @return void
     */
     
    public function remove(){
    }

	//----------------------------------------------------------------------------
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>