<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object transports
 */
 
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Deliverer.php" );
include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
include_once( dirname( __FILE__ ) . "/Carrier.php" );
include_once( dirname( __FILE__ ) . "/ForwardingAddress.php" );
include_once( dirname( __FILE__ ) . "/SupplierFactoryAddress.php" );


class Carriage extends DBObject{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Adresse de provenance
	 * L'adresse de provenance est soit l'adresse principale du fournisseur, soit celle de l'une de ses usines
	 * @access private
	 * @var Address $provenance instance of SupplierFactoryAddress or SupplierFactoryAddress
	 */
	private $provenance;
	
	/**
	 * Adresse de destination
	 * L'adresse de destination est soit celle du BL, soit celle du dépôt interne
	 * @access private
	 * @var Address $destination instance of CustomerAddress or ForwardingAddress
	 */
	private $destination;
	
	/**
	 * Transporteur
	 * Le transport peut être le fournisseur lui-même ou un transporteur pro
	 * @access private
	 * @var Deliverer $deliverer instance of Supplier ou Carrier
	 */
	private $deliverer;
	
	/**
	 * 
	 * @var DeliveryNote $deliveryNote
	 */
	private $deliveryNote;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @param int $idcarriage identifiant unique, identique au N° de BL
	 */
	public function __construct( $idcarriage ){
		
		parent::__construct( "carriage", false, "idcarriage",  $idcarriage  );
		
		$this->deliveryNote = new DeliveryNote( $idcarriage  );
		
		/* livreur : fournisseur ou transporteur pro */
		
		if( $this->recordSet[ "idcarrier" ] )
				$this->deliverer = new Carrier( $this->recordSet[ "idcarrier" ] );
		else 	$this->deliverer = $this->deliveryNote->getSupplier();
		
		/* adresse d'expédition : adresse principale fournisseur ou adresse usine fournisseur */
		
		if( $this->recordSet[ "idsupplier_factory" ] )
				$this->provenance = new SupplierFactoryAddress( $this->recordSet[ "idsupplier_factory" ] );
		else 	$this->provenance = $this->deliveryNote->getSupplier()->getAddress();
		
		/* adresse de livraison : adresse principale client ou adresse livraison client ou dépôt */
		
		if( $this->recordSet[ "iddelivery" ] )
				$this->destination 	= new ForwardingAddress( $this->recordSet[ "iddelivery" ] );
		else 	$this->destination 	= $this->deliveryNote->getCustomer()->getAddress();
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * identifiant unique
	 * @return int
	 */
	public function getId(){ return $this->recordSet[ "idcarriage" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return DeliveryNote
	 */
	public function &getDeliveryNote(){ return $this->deliveryNote; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Fournisseur ou Transporteur assurant la livraison
	 * @return Deliverer
	 */
	public function &getDeliverer(){ return $this->deliverer; }

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Address l'adresse de provenance
	 */
	public function getProvenance(){ return $this->provenance; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Address l'adresse de destination
	 */
	public function getDestination(){ return $this->destination; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param Deliverer $deliverer instance of Supplier or Carrier
	 */
	public function setDeliverer( Deliverer $deliverer ){
		
		$idcarrier = $deliverer instanceof Carrier ? $deliverer->getId() : 0;
		
		$this->deliverer = $deliverer;
		$this->set( "idcarrier", $idcarrier );
		
		/* report devis, cde client */
		
		$rs = DBUtil::query( 
		
			"SELECT o.idorder, e.idestimate
			FROM bl_delivery bl, `order` o
			LEFT JOIN estimate e ON e.idestimate = o.idestimate
			WHERE bl.idbl_delivery = '" . $this->getDeliveryNote()->getId() . "'
			AND o.idorder = bl.idorder
			LIMIT 1" 
		
		);
		 
		if( !$rs->RecordCount() )
			return;
		
		DBUtil::query( 
		
			"UPDATE order_charges 
			SET idcarrier = '$idcarrier' 
			WHERE idorder = '" . $rs->fields( "idorder" ) . "' 
			AND idsupplier = '" . $this->getDeliveryNote()->getSupplier()->getId() . "' 
			LIMIT 1" 
		
		);
		
		if( $rs->fields( "idestimate" ) )
			DBUtil::query( 
		
				"UPDATE estimate_charges 
				SET idcarrier = '$idcarrier' 
				WHERE idestimate = '" . $rs->fields( "idestimate" ) . "' 
				AND idsupplier = '" . $this->getDeliveryNote()->getSupplier()->getId() . "' 
				LIMIT 1" 
			
			);
			
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
     * @access public
     * @param Address $address instance of SupplierFactoryAddress or SupplierFactoryAddress
     * @return bool true en cas de succès, sinon false
     */
	public function setProvenance( Address $address ){
		
		if( !( $address instanceof SupplierFactoryAddress ) && !( $address instanceof SupplierAddress ) )
			return false;

		$this->set( "idsupplier_factory", $address instanceof SupplierFactoryAddress ? $address->getId() : 0 );
		$this->provenance = $address;
		
	}

/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * L'adresse de destination peut être soit l'adresse principale du client, soit une de ses adresses de livraison, ou encore l'adresse du dépôt interne
     * @access public
     * @param Address $address instance of CustomerAddress or ForwardingAddress
     * @return bool true en cas de succès, sinon false
     */
	public function setDestination( Address $address ){
		
		if( !( $address instanceof CustomerAddress ) && !( $address instanceof ForwardingAddress ) )
			return false;

		$this->set( "iddelivery", $address instanceof ForwardingAddress ? $address->getId() : 0 );
		$this->destination = $address;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>