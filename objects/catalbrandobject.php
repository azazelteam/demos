<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object produits par métiers
 */
 

include_once( "catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CATALBRAND extends CATALBASE
{
	
	/**
	 * Constructeur
	 * @param int $IdBrand
	 * @param int $reload
	 */
	function __construct($IdBrand, $reload=1)
	{

	$this->id = $IdBrand ;
	$this->buyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$this->lang = "_1";

	if( $reload ) {
		$this->NBProduct = 0 + $this->GetBrandProducts();
		}
	else { 
		$this->products = &$_SESSION['Brandproducts'] ;
		$this->NBProduct = 0 +  count($this->products);
		}
	$this->NbByPages = DBUtil::getParameter('nb_products_by_pages');
	}

	public function NBProduct(){
		return $this->NBProduct;
	}

	public function NbByPages(){
		return $this->NbByPages;
	}

	/**
	 * Cherche la liste des marques
	 * @return array la liste des métiers
	 */
	public function GetBrandsList()
	{
		$IdBrand = $this->id;
		$Langue = $this->lang;
		$BrandsList = array();
		$Query = "SELECT `idbrand`, `brand` AS `name` FROM `brand` ORDER BY `brand`";
		$Base = &DBUtil::getConnection();
		$rs =& DBUtil::query($Query);
		trigger_error(showDebug($Query,'CATALBRAND::GetBrandsList() => $SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATALBRAND::GetBrandsList() => $SQL_select','log'),E_USER_ERROR);
		while( !$rs->EOF ) {
			$BrandsList[$rs->fields['idbrand']] = $rs->fields['name'];
			$rs->MoveNext();
		}
		//showDebug($BrandsList,'CATALBRAND::GetBrandsList() => $BrandsList','log');
		return $BrandsList;
	}

	/**
	 * Cherche les produit 
	 * @return array la liste des produits
	 */
	function GetBrandProducts()
	{
		$IdBrand = $this->id;
		$Langue = $this->lang;
		$Query = "SELECT DISTINCT `r`.`idproduct`, `r`.`name$Langue` AS `name`, `t`.`brand` AS `brand`" .
				"FROM `product` `r`, `brand` `t` " .
				"WHERE `r`.`idbrand` = `t`.`idbrand`" .
				"AND `r`.`idbrand` = '$IdBrand' " .
				"ORDER BY `r`.`name$Langue`";
		//showDebug($Query,'CATALBRAND::GetBrandProducts() => $Query','log');
		$n = $this->SetProducts($Query);
		$_SESSION['Brandproducts'] = &$this->products;
		//$rs=DBUtil::query($Query);
		
		return $n;
	}
	
	function GetBrandProductsInfos(){
		$IdBrand = $this->id;
		$Langue = $this->lang;
		$Query = "SELECT DISTINCT `r`.`idproduct`, `r`.`name$Langue` AS `name`, `t`.`brand` AS `brand`" .
				"FROM `product` `r`, `brand` `t` " .
				"WHERE `r`.`idbrand` = `t`.`idbrand` " .
				"AND `r`.`idbrand` = '$IdBrand' " .
				"ORDER BY `r`.`name$Langue`";
		$rs=DBUtil::query($Query);
		
		return $rs;
	}

	/**
	 * Cherche le nom du métier
	 * @return string le nom du métier, error si pas trouvé
	 */
	public function GetBrandName()
	{
		$IdBrand = $this->id;
		$Langue = $this->lang;
		$Query = "SELECT `brand` AS `name` FROM `brand` WHERE `idbrand` = '$IdBrand'";
		$rs = DBUtil::query($Query);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATALBRAND::GetBrandName() => $SQL_select','log'),E_USER_ERROR);
		if( !$rs->EOF ) {
			return $rs->fields['name'];
		}
		return 'error';
	}
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}


}//EOC
?>