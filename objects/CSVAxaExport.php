<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export Factures CSV pour AXA
 */

include_once( dirname( __FILE__ ) . "/CSVInvoiceExport.php" );

 
class CSVAxaExport extends CSVInvoiceExport
{
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $filename le nom du fichier de résultat
	 * @param string $output_type le type de fichier de résultat
	 * @param string $separator le séparateur
	 * @param string $text_delimiter l'indicateur de texte
	 * @param string $breakline l'indicateur de saut de ligne
	 */
	function __construct( $filename, $output_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\n" ){
	
		parent::__construct( $filename, $output_type, $separator, $text_delimiter, $breakline );
			
	}

	/**
	 * Formatte les données
	 * @return void
	 */	
	protected function setData(){
		
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>setData()</u>" );
		
		$this->data = array();
		
		$this->data[ 0 ] = array(
		
			"N° cde",
			"N° siret",
			"NDS ou agrément",
			"Date d'acceptation AXA",
			"N° de facture",
			"Date de facturation"
		);
		
		$i = 0;
		while( $i < count( $this->idorders ) ){
		
			$idorder = $this->idorders[ $i ];
			
			$query = "
			SELECT b.siret,
				o.factor_type,
				o.factor_status_date,
				bb.idbilling_buyer,
				bb.DateHeure
			FROM `order` o, buyer b, billing_buyer bb
			WHERE o.idorder = '$idorder'
			AND bb.`status` LIKE 'Invoiced'
			AND o.factor_status=3
			AND bb.idorder = o.idorder
			AND o.idbuyer = b.idbuyer
			
			";
			
				
			//@todo : order.factor contient un peu tout et n'importe quoi pour dire 'pas de factor' ( il y a des NULL et des '' et des '0' )
			
			$result = $this->query( $query );
			
			if( mysqli_num_rows( $result ) ){
			
				$invoice = @mysqli_fetch_object( $result );
				
				//date facturation
				
				$dateh = substr($invoice->DateHeure,0,10);
				
				$tmp = explode( "-", $dateh );
				if( count( $tmp ) === 3 ){
				
					list( $year, $month, $day ) = explode( "-", $dateh );
					$year = substr( $year, 2, 2 );
					$billing_date = "$day/$month/$year";
				
				}
				else $billing_date = $dateh;
				
				//date d'acceptation
				
				$tmp = explode( "-", $invoice->factor_status_date);
				if( count( $tmp ) === 3 ){
				
					list( $year, $month, $day ) = explode( "-", $invoice->factor_status_date);
					$year = substr( $year, 2, 2 );
					$factor_status_date = "$day/$month/$year";
				
				}
				else $factor_status_date = $invoice->factor_status_date;
					
				$row = array(
				
					$idorder,
					$invoice->siret,
					$invoice->factor_type,
					$factor_status_date,
					$invoice->idbilling_buyer,
					$billing_date	
				);
				
				$this->data[] = $row;
			
			}
			
			$i++;
			
		}
		
		if( DEBUG_CSVInvoiceExport )
			print_r( $this->data );
			
	}
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	
}