<?php
 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object fonctions générales
 */
 

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );


abstract class Util{
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne un mot de passe alphanumérique aléatoire d'une longueur donnée
	 * @static
	 * @param int $passwordLength la taille du mot de passe souhaitée, 6 par défaut
	 * @return string
	 */
	public static function getRandomPassword( $passwordLength = 6 ){

		$chars = "azertyuiopqsdfghjklmwxcvbnAZERTYUIOPQSDFGHJKLMWXCVBN0123456789";
		$password = "";
		
		srand( ( double ) microtime() * 1000000 );
		
		$i = 0;
		while( $i < $passwordLength ){
			
			$pos = rand( 0, strlen( $chars ) - 1 );
			$password .= substr( $chars, $pos, 1 );
			
			$i++;
			
		}
	 
		return $password;
	
	}
    
    /**
     * C'est l'équivalent de la fonction mysql_result en PHP 7
     * @access public
	 * @static 
     * @param $res stocke des enregistrements
     * @param int $row numéro de ligne 
     * @param int $col numéro de colonne 
     */  
    public static function mysqli_result($res,$row=0,$col=0){ 
        $numrows = mysqli_num_rows($res); 
        if ($numrows && $row <= ($numrows-1) && $row >=0){
            mysqli_data_seek($res,$row);
            $resrow = (is_numeric($col)) ? mysqli_fetch_row($res) : mysqli_fetch_assoc($res);
            if (isset($resrow[$col])){
                return $resrow[$col];
            }
        }
        return false;
    }
    
    /**
	 * Cette fonction remplace mysql_real_escape_string utilisée dans les anciennes versions de PHP
	 * @access public
	 * @static
	 * @param string $html_escape la chaine à traiter
	 * @return string
	 */
    public static function html_escape($html_escape) {
        /*$html_escape = htmlspecialchars( Util::doNothing($html_escape), ENT_QUOTES | ENT_COMPAT,'UTF-8');
        return $html_escape;*/
        return mysqli_real_escape_string(DBUtil::getConnectionId(), $html_escape);
    }

	//----------------------------------------------------------------------------
	/**
	 * Convertir une date donnée au format souhaité
	 * @access public
	 * @static
	 * @param string $date une date au format US ou EU
	 * @param string $format le format de date souhaité ( cf. manuel PHP )
	 * @return string
	 */
	public static function dateFormat( $date, $format ){
		
		$regs = array();
		
		if( preg_match( "^[0-9]{4}(.{1})[0-9]{2}.{1}[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\$^", $date, $regs ) ){ //datetime US

			list( $date, $time ) 			= explode( " ", $date );
			list( $year, $month, $day ) 	= explode( $regs[ 1 ], $date );
			list( $hour, $minute, $second ) = explode( ":", $time );
			
		}
		else if( preg_match( "^[0-9]{4}(.{1})[0-9]{2}.{1}[0-9]{2}\$^", $date, $regs ) ){ //date US
			
			list( $year, $month, $day ) = explode( $regs[ 1 ], $date );
			$hour = $minute = $second = 0;
			
		}
		else if( preg_match( "^[0-9]{2}(.{1})[0-9]{2}.{1}[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}\$^", $date, $regs ) ){ //datetime EU
			
			list( $date, $time ) 			= explode( " ", $date );
			list( $day, $month, $year ) 	= explode( $regs[ 1 ], $date );
			list( $hour, $minute, $second ) = explode( ":", $time );
			
		}
		else if( preg_match( "^[0-9]{2}(.{1})[0-9]{2}.{1}[0-9]{4}\$^", $date, $regs ) ){ //date EU
			
			list( $day, $month, $year ) = explode( $regs[ 1 ], $date );
			$hour = $minute = $second 	= 0;
			
		}
		else{
			
			//trigger_error( "Format de date inconnu : $date", E_USER_ERROR );
			return "";
			
		}
		
		$timestamp = mktime( $hour, $minute, $second, $month, $day, $year );

		return $timestamp == mktime( 0, 0, 0, 0, 0, 0 ) ? "" : date( $format, $timestamp );
		
	}

	//----------------------------------------------------------------------------
		
	/**
	 * Retourne un nombre sous forme de prix, avec la devise courante
	 * @todo encodage UTF8 pour permettre l'affichage du glyphe 'euro'
	 * @param float $value le nombre à formater
	 * @param deprecated $precision la précision
	 * @param int $encode_signe (0: affichage € dans version HTML | 1: affichage € dans version PDF)
	 * @return string
	 */
	public static function priceFormat( $value, $precision = 2, $encode_signe = 0 ){
		return ($encode_signe) ? Util::numberFormat( $value, $precision ) . " €" : Util::numberFormat( $value, $precision ) . " &euro;";
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne un nombre sous la forme d'un pourcentage ( ex : '50 %' )
	 * @param float $value le nombre flottant à formatter
	 * @param int $precision la précision du pourcentage, 1 par défaut
	 * @return string
	 */
	public static function rateFormat( $value, $precision = 2 ){
		
	 return Util::numberFormat( $value, $precision ) . " " . "%";
	 
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Formate un nombre donné avec une précision donnée en fonction de la langue courante
	 * @param float $value le nombre flottant à formatter
	 * @param int $precision la précision du pourcentage, 1 par défaut
	 * @return string
	 */
	public static function numberFormat( $value, $precision = 2 ){
	
		/*
		 * @todo : aucune logique dans tout ça :
		 * $lang 				= DBUtil::getParameter( "affnumber_mode" ); 	//0 = anglais, 1 = français
		 * $defaultPrecision	= DBUtil::getParameter( "affnumber_decimal" ); 	//nombre de décimales après la virgule
		 * $round 				= DBUtil::getParameter( "affnumber_round" ); 	//précision
		 */

		$lang = "_1";
		
		switch( $lang ){
			
			case "_2" :	$dec_separator = ","; $thousands_separator = "."; break;
			case "_1" : 	
			default   : $dec_separator = ","; $thousands_separator = " ";
			
		}

		return number_format( round( $value, $precision ), $precision, $dec_separator, $thousands_separator );
	
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Convertit un texte en nombre à virgule
	 * @static
	 * @param string $text le text à convertir
	 * @return float la valeur décimale
	 */
	 
	public static function text2num( $text ){

		$text = str_replace( ",", ".", $text );
		$text = preg_replace( "([^-0123456789.])", '', $text );
		
		return 0.0 + $text;

	}

	//----------------------------------------------------------------------------
	
	public static function phonenumberFormat( $phonenumber ){
		
		if( strlen( str_replace( " ", "", $phonenumber ) ) != 10 )
			return $phonenumber;

		$phonenumber = str_replace( " ", "", $phonenumber );
		$hrphonenumber = "";
		$i = 0;
		while( $i < strlen( $phonenumber ) / 2 ){
			
			$hrphonenumber .= substr( $phonenumber, $i * 2, 2 ) . " ";
			
			$i++;
			
		}

		return trim( $hrphonenumber );
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * map une fonction de façon récursive sur tous les éléments d'un tableau donné ( indexé ou indicé )
	 * @param array $mixed
	 * @return array
	 */
	public static function &arrayMapRecursive( $callback, &$mixed ){

		if( is_array( $mixed ) ){
			
			foreach( $mixed as $key => $value )
				$mixed[ $key ] = is_array( $value ) ? self::arrayMapRecursive( $callback, $value ) : $callback( $value );
		
		}
		
		return $mixed;
		
	}
	
	//----------------------------------------------------------------------------
	
	public static function upperCase( $string ){
	
		$accents = "äâàáåãéèëêòóôõöøìíîïùúûüýñçþÿæ½ðø";
		$replace = "ÄÂÀÁÅÃÉÈËÊÒÓÔÕÖØÌÍÎÏÙÚÛÜÝÑÇÞÝÆ¼ÐØ"; 
		
		return strtr( strtoupper( $string ), $accents, $replace );
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * @deprecated préférer DBUtil::getDBValue()
	 */
	public static function getState( $idstate ){
		
		$lang = "_1";
		
		$rs = DBUtil::getDBValue( "name$lang", "state", "idstate", $idstate );
		
		return $rs;

	}

	/**
	 * @deprecated préférez DBUtil::getDBValue( ... )
	 */
	public static function getDelay( $iddelay ){
		
		$lang = "_1";
		
		$rs = DBUtil::getDBValue( "delay$lang", "delay", "iddelay", $iddelay );
		
		return $rs;

	}

	/**
	 * @deprecated
	 */
	public static function getTitle( $idtitle ){
		
		$lang = "_1";
		
		$rs = DBUtil::getDBValue( "title$lang", "title", "idtitle", $idtitle );
		
		return $rs;

	}
	
	/**
	 * @deprecated préférer Util::dateFormatEu( $date, $format )
	 */
	public static function dateFormatEu( $date, $whith_hour = true, $return = "", $shortYear = false ){
		
		$registers 			= array();
		$datePattern 		= "^([0-9]{4})-([0-9]{2})-([0-9]{2})^";
		$dateTimePattern 	= "/($datePattern ([0-9]{2}):([0-9]{2}):([0-9]{2}))/";
		//echo $dateTimePattern;
		if( substr( $date, 0, 10 ) == "0000-00-00" )
			return $return;
		
		if( preg_match( $dateTimePattern, $date, $registers ) && $whith_hour && $shortYear )
			return sprintf( "%02d/%02d/%02d à %dh%02d", $registers[ 3 ], $registers[ 2 ], substr( $registers[ 1 ], 2, 2 ), $registers[ 4 ], $registers[ 5 ] );
		
		if( preg_match( $dateTimePattern, $date, $registers ) && $whith_hour && !$shortYear )
			return sprintf( "%02d/%02d/%0d à %dh%02d", $registers[ 3 ], $registers[ 2 ], $registers[ 1 ], $registers[ 4 ], $registers[ 5 ] );
		
		if( preg_match( $datePattern, $date, $registers ) && $shortYear )
			return sprintf( "%02d/%02d/%02d", $registers[ 3 ],$registers[ 2 ], substr( $registers[ 1 ], 2, 2 ) );
		
		if( preg_match( $datePattern, $date, $registers ) && !$shortYear )
			return sprintf( "%02d/%02d/%0d", $registers[ 3 ],$registers[ 2 ],$registers[ 1 ]);
		
		return $date;

	}

	/**
	 * @deprecated préférer Util::dateFormatEu( $date, $format )
	 */
	public static function dateFormatUs( $date, $whith_hour = true, $return = "", $shortYear = false ){
		
		$registers 			= array();
		$datePattern 		= "^([0-9]{2})-([0-9]{2})-([0-9]{4})^";
		$dateTimePattern 	= "/($datePattern ([0-9]{2}):([0-9]{2}):([0-9]{2}))/";
		
		if( substr( $date, 0, 10 ) == "00-00-0000" )
			return $return;
		
		if( preg_match( $dateTimePattern, $date, $registers ) && $whith_hour && $shortYear )
			return sprintf( "%02d-%02d-%02d %dh:%02d", $registers[ 3 ], $registers[ 2 ], substr( $registers[ 1 ], 2, 2 ), $registers[ 4 ], $registers[ 5 ] );
		
		if( preg_match( $dateTimePattern, $date, $registers ) && $whith_hour && !$shortYear )
			return sprintf( "%0d-%02d-%02d %dh:%02d", $registers[ 3 ], $registers[ 2 ], $registers[ 1 ], $registers[ 4 ], $registers[ 5 ] );
		
		if( preg_match( $datePattern, $date, $registers ) && $shortYear )
			return sprintf( "%02d-%02d-%02d", $registers[ 3 ],$registers[ 2 ], substr( $registers[ 1 ], 2, 2 ) );
		
		if( preg_match( $datePattern, $date, $registers ) && !$shortYear )
			return sprintf( "%0d-%02d-%02d", $registers[ 3 ],$registers[ 2 ],$registers[ 1 ]);
		
		return $date;

	}
	
	//----------------------------------------------------------------------------

	/**
 	* Retourne l'encodage d'un string
 	*/
	public static function detectEncoding($str)
	{
		if (mb_detect_encoding($str ,mb_detect_order(), true) != 'UTF-8')
			return 'ISO-8859';
		else
			return 'UTF-8';
	}

	/**
 	* Détecte l'encodage d'un string et l'encode en UTF-8 si ne l'est pas
 	*/
 	public static function checkEncoding($str)
 	{
 		/*$encode = self::detectEncoding($str);
		if ($encode != 'UTF-8')
			return Util::doNothing($str);
		else*/
			return $str;
 	}
    
    public static function doNothing($str)
 	{
 		return $str;
 	}

}

?>