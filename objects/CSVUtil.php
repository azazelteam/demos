<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object fonctions générales CSV
*/


set_time_limit( 900 );
ini_set( "memory_limit", "128M" );

define( "DEBUG_CSVUTIL", 0 );

class CSVUtil{

	//-----------------------------------------------------------------------------------
	
	function CSVUtil(){
		
		die( "Cannot instanciate static class CSVUtil" );
		
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Retourne l'indice de la colonne dont le nom est $columnName
	 * @param array $headers array les en-têtes du fichier csv
	 * @param string $columnName le nom de la colonne recherchée
	 * @param bool $case_sensitive sensibilité à la case ou non
	 * @return int l'indice de la colonne recherchée si elle existe, sinon false
	 */
	 
	function getColumnIndex( &$headers, $columnName, $case_sensitive = true ){
		
		if( DEBUG_CSVUTIL )
			echo "
			<br /><b>getColumnIndex( $headers, $columnName, $case_sensitive )</b>";
			
		$i = 0;
		while( $i < count( $headers ) ){
			
			if( ( $case_sensitive && strcmp( $headers[ $i ][ "fieldname" ], $columnName ) == 0 )
				|| ( !$case_sensitive && strcasecmp( $headers[ $i ][ "fieldname" ], $columnName ) == 0 ) ){
					
				if( DEBUG_CSVUTIL )
					echo "<br />return $i";
			
				return $i;
				
			}
				
			$i++;
			
		}
	
		if( DEBUG_CSVUTIL )
			echo "<br />return false";
			
		return false;
		
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Remplace les valeurs de la colonne dont l'indice est $index
	 * @param array $buffer le tableau à traiter
	 * @param int $columnIndex l'indice de la colonne à traiter
	 * @param string $search la valeur à remplacer
	 * @param string $replace la valeur de remplacement
	 * @param bool $case_sensitive sensibilité à la case ou non
	 * @return void
	 */
	 
	function replaceColumnValue( &$buffer, $columnIndex, $search, $replace, $case_sensitive = true ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>replaceColumnValue( $buffer, $columnIndex, $search, $replace, $case_sensitive )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		$rowCount = count( $buffer );
		$replaced = 0;
		
		$i = 1;
		while( $i < $rowCount ){
			
			if( ( $case_sensitive && !strcmp( $buffer[ $i ][ $columnIndex ], $search ) ) 
					|| ( !$case_sensitive && !strcasecmp( $buffer[ $i ][ $columnIndex ], $search ) ) ){
				
				$buffer[ $i ][ $columnIndex ] = $replace;
				$replaced++;
				
			}
			
			$i++;
				
		}
		
		if( DEBUG_CSVUTIL )
			echo "<br />$replaced replaced values";

	}
	
//-----------------------------------------------------------------------------------
	
	/**
	 * Remplace les valeurs de la ( $columnIndex1 + 1 )ième colonne par $replace lorsque la ( $columnIndex2 + 1 )ième colonne vaut $search
	 * @param array $buffer le tableau à traiter
	 * @param int $columnIndex1 l'indice de la colonne à traiter
	 * @param int $columnIndex2 l'indice de la colonne de référence
	 * @param string $search la valeur à rechercher dans la ( $columnIndex2 + 1 )ième colonne
	 * @param string $replace la valeur de remplacement pour la ( $columnIndex1 + 1 )ième colonne
	 * @param bool $case_sensitive sensibilité à la case ou non
	 * @return void
	 */
	 
	function replaceColumnValueForMatchingColumnValue( &$buffer, $columnIndex1, $replace, $columnIndex2, $search, $case_sensitive = true ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>replaceColumnValueForMatchingColumnValue( $buffer, $columnIndex1, $replace, $columnIndex2, $search, $case_sensitive )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		$rowCount = count( $buffer );
		$replaced = 0;
		
		$i = 1;
		while( $i < $rowCount ){
			
			if( ( $case_sensitive && !strcmp( $buffer[ $i ][ $columnIndex2 ], $search ) ) 
					|| ( !$case_sensitive && !strcasecmp( $buffer[ $i ][ $columnIndex2 ], $search ) ) ){
				
				$buffer[ $i ][ $columnIndex1 ] = $replace;
				
				$replaced++;
				
			}
			
			$i++;
				
		}
		
		if( DEBUG_CSVUTIL )
			echo "<br />$replaced replaced values";

	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Supprime les lignes dont les valeurs de la ( $columnIndex + 1 )ième colonne valent $value
	 * @param array $buffer le tableau à traiter
	 * @param int $columnIndex l'indice de la colonne à traiter
	 * @param $value la valeur recherchée
	 * @param bool $case_sensitive sensibilité à la case ou non
	 * @return void
	 */
	 
	function unsetLineWithColumnStartingWith( &$buffer, $columnIndex, $value, $case_sensitive = true ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetLineWithColumnStartingWith( $buffer, $columnIndex, $value, $case_sensitive )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		$rowCount = count( $buffer );
		if( !$rowCount )
			return;
		$columnCount = count( $buffer[ 0 ] );
		
		$tmp = array();
		$valueLength = strlen( $value );
		$deletedCount = 0;
		
		$i = 0;
		$j = 0;
		while( $i < $rowCount ){

			$ignoreLine = false;
			
			if( ( $case_sensitive && strcmp( substr( $buffer[ $i ][ $columnIndex ], 0, $valueLength ), $value ) == 0 )
				|| ( !$case_sensitive && strcasecmp( substr( $buffer[ $i ][ $columnIndex ], 0, $valueLength ), $value ) == 0 ) )
				$ignoreLine = true;
				
			if( !$ignoreLine ){

				$k = 0;
				while( $k < $columnCount ){
					
					$tmp[ $j ][ $k ] = $buffer[ $i ][ $k ];
					
					$k++;
					
				}
				
				$j++;
					
			}
			else $deletedCount++;
			
			$i++;
				
		}
		
		$buffer = $tmp;
		
		if( DEBUG_CSVUTIL )
			echo "<br />$deletedCount/$rowCount deleted lines";
			
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Supprime les lignes où la ( $columnIndex + 1 )ième colonne n'est pas renseignée
	 * @param array $buffer le tableau à traiter
	 * @param int $columnIndex l'indice de la colonne à traiter
	 * @return void
	 */
	 
	function unsetLineWithEmptyColumnValue( &$buffer, $columnIndex ){
	
		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetLineWithEmptyColumnValue( $buffer, $columnIndex )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		CSVUtil::unsetLineWithColumnValue( $buffer, $columnIndex, "", true );
			
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Supprime les lignes dont les valeurs de la ( $columnIndex + 1 )ième colonne valent $value
	 * @param array $buffer le tableau à traiter
	 * @param int $columnIndex l'indice de la colonne à traiter
	 * @param $value la valeur recherchée
	 * @param bool $case_sensitive sensibilité à la case ou non
	 * @return void
	 */
	 
	function unsetLineWithColumnValue( &$buffer, $columnIndex, $value, $case_sensitive = true ){
		

		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetLineWithColumnValue( $buffer, $columnIndex, $value, $case_sensitive )</b>
			<br />Parsing " . count( $buffer ) . " lines";
		
		$rowCount = count( $buffer );
		
		if( !$rowCount )
			return;
		$columnCount = count( $buffer[ 0 ] );
		
		$deletedCount = 0;
		$tmp = array();
		
		$i = 0;
		$j = 0;
		while( $i < $rowCount ){
			
			if( !$i
				|| ( $case_sensitive && strcmp( $buffer[ $i ][ $columnIndex ], $value ) != 0 )
				|| ( !$case_sensitive && strcasecmp( $buffer[ $i ][ $columnIndex ], $value ) != 0 ) ){
					
					$k = 0;
					while( $k < $columnCount ){
						
						$tmp[ $j ][ $k ] = $buffer[ $i ][ $k ];
						
						$k++;
						
					}
					
					$j++;
					
			}
			else $deletedCount++;

			$i++;
				
		}
		
		$buffer = $tmp;
		
		if( DEBUG_CSVUTIL )
			echo "<br />$deletedCount/$rowCount lines deleted";
			
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Supprime les lignes dont les valeurs de la ( $columnIndex + 1 )ième colonne ne valent pas $value
	 * @param array $buffer le tableau à traiter
	 * @param int $columnIndex l'indice de la colonne à traiter
	 * @param $value la valeur à exclure
	 * @param bool $case_sensitive sensibilité à la case ou non
	 * @return void
	 */
	 
	function unsetLineWithoutColumnValue( &$buffer, $columnIndex, $value, $case_sensitive = true ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetLineWithoutColumnValue( $buffer, $columnIndex, $value, $case_sensitive )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		$rowCount = count( $buffer );
		if( !$rowCount )
			return;
		$columnCount = count( $buffer[ 0 ] );
		
		$deletedCount = 0;
		$tmp = array();
		
		$i = 0;
		$j = 0;
		while( $i < $rowCount ){

			if( !$i
				|| ( $case_sensitive && strcmp( $buffer[ $i ][ $columnIndex ], $value ) == 0 )
				|| ( !$case_sensitive && strcasecmp( $buffer[ $i ][ $columnIndex ], $value ) == 0 ) ){
					
					$k = 0;
					while( $k < $columnCount ){
						
						$tmp[ $j ][ $k ] = $buffer[ $i ][ $k ];
						
						$k++;
						
					}
					
					$j++;
					
			}
			else $deletedCount++;
			
			$i++;
				
		}
		
		$buffer = $tmp;
		
		if( DEBUG_CSVUTIL )
			echo "<br />$deletedCount/$rowCount lines deleted";
			
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Supprime la ( $lineIndex + 1 )ième ligne
	 * @param array $buffer le tableau à traiter
	 * @param int $lineIndex l'indice de la ligne à supprimer
	 * @return void
	 */
	 
	function unsetLineIndex( &$buffer, $lineIndex ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetLineIndex( $buffer, $lineIndex )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		$rowCount = count( $buffer );
		if( !$rowCount )
			return;
		$columnCount = count( $buffer[ 0 ] );
		
		$tmp = array();
		
		$i = 0;
		$j = 0;
		while( $i < $rowCount ){
			
			if( $i != $lineIndex ){
					
					$k = 0;
					while( $k < $columnCount ){
						
						$tmp[ $j ][ $k ] = $buffer[ $i ][ $k ];
						
						$k++;
						
					}
					
					$j++;
					
			}
			else if( DEBUG_CSVUTIL )
				echo "<br />Line $lineIndex deleted";
			
			$i++;
				
		}
		
		$buffer = $tmp;
		
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Supprime les lignes où l'intervalle de dates n'est pas valide
	 * Si plusieurs intervalles valides trouvés, ne conserve que celui qui commence le plus tard
	 * @param array $buffer le tableau à traiter
	 * @param array $uniqueKeys tableau indicé contenant les indice des colonnes à utiliser comme combinaisons uniques pour la comparaison des dates
	 * @param int $startDateColumnIndex l'indice de la colonne contenant la date de début
	 * @param int $endDateColumnIndex l'indice de la colonne contenant la date de fin
	 * @return void
	 */
	 
	function unsetInvalidDates( &$buffer, $uniqueKeys, $startDateColumnIndex, $endDateColumnIndex ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetInvalidDates( $buffer, $primaryKeyColumnIndex, $startDateColumnIndex, $endDateColumnIndex )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		$rowCount = count( $buffer );
		if( !$rowCount )
			return;
		$columnCount = count( $buffer[ 0 ] );
	
		//sélection des dates par référence

		//récupérer la date la plus proche pour chaque article
		
		$uniqueIdentifiers = array(); // array( primaryKey => selectedRow )
		
		$deletedCount = 0;
		$i = 1;
		while( $i < $rowCount ){
			
			//créer un identifiant à partir des valeurs des colonnes qui forment une clé unique
			
			$uniqueIdentifier = "";
			$p = 0;
			$missingKey = false;
			while( $p < count( $uniqueKeys ) && !$missingKey ){
				
				$uniqueKeyIndex = $uniqueKeys[ $p ];
				
				if( $p )
					$uniqueIdentifier .= "-";
					
				if( empty( $buffer[ $i ][ $uniqueKeyIndex ] ) )
					$missingKey = true;
					
				$uniqueIdentifier .= $buffer[ $i ][ $uniqueKeyIndex ];
				
				$p++;
				
			}
			
			$currentStartDate = $buffer[ $i ][ $startDateColumnIndex ];
			$currentEndDate = $buffer[ $i ][ $endDateColumnIndex ];
			
			$now = date( "d/m/Y" );
				
			$replace = true;
			
			if( !$missingKey
				&& CSVUtil::dateCompare( $currentStartDate, $now ) <= 0 
				&& CSVUtil::dateCompare( $currentEndDate, $now ) >= 0 ){
				
				if( isset( $uniqueIdentifiers[ "$uniqueIdentifier" ] ) ){
				
					$selected = $uniqueIdentifiers[ "$uniqueIdentifier" ];
					
					$selectedStartDate = $buffer[ $selected ][ $startDateColumnIndex ];
					$selectedEndDate = $buffer[ $selected ][ $endDateColumnIndex ];
			
					//sélection de la date dépassée la plus proche
	
					if( CSVUtil::dateCompare( $currentStartDate, $selectedStartDate ) > 0 )
						$uniqueIdentifiers[ "$uniqueIdentifier" ] = $i;
		
					$deletedCount++;
					
				}
				else $uniqueIdentifiers[ "$uniqueIdentifier" ] = $i;
			
			}
			else $deletedCount++;
			
			$i++;
			
		}
	
		//supprimer les lignes avec des dates invalides
	
		$tmp = array();
		
		$i = 0;
		$i2 = 0;
		while( $i < $rowCount ){
			
			$uniqueIdentifier = "";
			$p = 0;
			$missingKey = false;
			while( $p < count( $uniqueKeys ) && !$missingKey ){
				
				$uniqueKeyIndex = $uniqueKeys[ $p ];
				
				if( $p )
					$uniqueIdentifier .= "-";
					
				if( empty( $buffer[ $i ][ $uniqueKeyIndex ] ) )
					$missingKey = true;
					
				$uniqueIdentifier .= $buffer[ $i ][ $uniqueKeyIndex ];
				
				$p++;
				
			}

			if( !$i )
				$ignoreLine = false;
			else if( $missingKey )
				$ignoreLine = true;
			else if( !isset( $uniqueIdentifiers[ "$uniqueIdentifier" ] ) )
				$ignoreLine = true;
			else if( $uniqueIdentifiers[ "$uniqueIdentifier" ] == $i )
				$ignoreLine = false;
			else $ignoreLine = true;
			
			if( !$ignoreLine ){
				
				$tmp[ $i2 ] = array();
				
				$j = 0;
				while( $j < $columnCount ){
	
					$tmp[ $i2 ][ $j ] = $buffer[ $i ][ $j ];
	
					$j++;
					
				}
				
				$i2++;
					
			}
			
			$i++;
			
		}
	
		$buffer = $tmp;
		
		if( DEBUG_CSVUTIL )
			echo "<br />$deletedCount/$rowCount lines deleted";
			
	}

	//-----------------------------------------------------------------------------------
	
	function unsetUnexistingPrimaryKey( &$buffer, $table, $primaryKey, $primaryKeyColumnIndex ){
		
		if( DEBUG_CSVUTIL )
			echo "<br /><b>unsetUnexistingPrimaryKey( $buffer, $table, $primaryKey, $primaryKeyColumnIndex )</b>
			<br />Parsing " . count( $buffer ) . " lines";
			
		global 	$GLOBAL_DB_NAME,
				$GLOBAL_DB_USER,
				$GLOBAL_DB_PASS,
				$GLOBAL_DB_HOST;
	
		if( !isset( $GLOBAL_DB_NAME ) || empty( $GLOBAL_DB_NAME ) 
			|| !isset( $GLOBAL_DB_USER ) || empty( $GLOBAL_DB_USER )
			|| !isset( $GLOBAL_DB_PASS ) || empty( $GLOBAL_DB_PASS )
			|| !isset( $GLOBAL_DB_HOST ) || empty( $GLOBAL_DB_HOST ) )
			die( "No connection to database available" );
			
		$db = &NewADOConnection( "mysql", "pear" );
		$db->Connect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS, $GLOBAL_DB_NAME );
		
		$rowCount = count( $buffer );
		if( !$rowCount )
			return;
		$columnCount = count( $buffer[ 0 ] );
		
		$deletedCount = 0;
		$tmp = array();
		
		$i = 0;
		$i2 = 0;
		while( $i < $rowCount ){
			
			$ignoreLine = false;
			
			if( $i ){
				
				$primaryKeyValue = $buffer[ $i ][ $primaryKeyColumnIndex ];
				if( empty( $primaryKeyValue ) )
					$ignoreLine = true;
				else{
				
					$query = "
					SELECT `$primaryKey` FROM `$table` WHERE `$primaryKey` = '$primaryKeyValue' LIMIT 1";
	
					$rs = $db->Execute( $query );
					
					if( !$rs->RecordCount() )
						$ignoreLine = true;
						
				}
					
			}
			
			if( !$i || !$ignoreLine ){
				
				$tmp[ $i2 ] = array();
				
				$j = 0;
				while( $j < $columnCount ){
	
					$tmp[ $i2 ][ $j ] = $buffer[ $i ][ $j ];
	
					$j++;
					
				}
				
				$i2++;
					
			}
			else $deletedCount++;
			
			$i++;
			
		}
		
		$buffer = $tmp;
		
		$db->Close();
		
		if( DEBUG_CSVUTIL )
			echo "<br />$deletedCount/$rowCount lines deleted";
			
	}

	//-----------------------------------------------------------------------------------
	
	/**
	 * @param $date1 string date au format dd/mm/AAAA 
	 * @param $date2 string date au format dd/mm/AAAA
	 * @return int -1 si date1 est inférieure à date2 ou 0 si date1 est égale à date2, sinon 1
	 */
	 
	function dateCompare( $date1, $date2 ){
		
		list( $day1, $month1, $year1 ) = explode( "/", $date1 );
		list( $day2, $month2, $year2 ) = explode( "/", $date2 );
		
		if( $year1 > $year2 )
			return 1;
		
		if( $year1 < $year2 )
			return -1;
				
		if( $month1 > $month2 )
			return 1;
			
		if( $month1 < $month2 )
			return -1;
			
		if( $day1 > $day2 )
			return 1;
			
		return $day1 == $day2 ? 0 : -1;
		
	}
	
	//-----------------------------------------------------------------------------------
	
	/**
	 * Duplique les valeurs de la ($columnIndex + 1)ième colonne dans une nouvelle colonne '$columnName'
	 * @param array $headers array les en-têtes du fichier csv
	 * @param array $buffer le tableau à traiter
	 * @param int $$columnIndex l'indice de la colonne à dupliquer
	 * @param string $columnName le nom de la nouvelle colonne créée
	 * @return void
	 */
	
	function duplicateColumn( &$headers, &$buffer, $columnIndex, $columnName ){
	
		if( DEBUG_CSVUTIL )
			echo "<br /><b>duplicateColumn( $buffer, $columnIndex, $columnName )</b>
			<br />Parsing " . count( $buffer ) . " lines";
		
		$rowCount = count( $buffer );
		$columnCount = count( $buffer[ 0 ] );
		
		$headers[ $columnCount ] = $columnName;
		$buffer[ 0 ][ $columnCount ] = $columnName;
		
		$i = 1;
		while( $i < $rowCount ){
		
			$buffer[ $i ][ $columnCount ] = $buffer[ $i ][ $columnIndex ];
			
			$i++;
			
		}
		
		if( DEBUG_CSVUTIL )
			echo "<br />$duplicated duplicated values";

	}
	
	//-----------------------------------------------------------------------------------
	
}

?>
