<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object lignes articles
 */

include_once( dirname( __FILE__ ) . "/../DBObject.php" );
include_once( dirname( __FILE__ ) . "/../util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/SaleableOption.php" );
include_once( dirname( __FILE__ ) . "/PriceStrategy.php" );


/**
 * @package catalog
 * @todo gestion des intitulés
 */
class CArticle extends DBObject{

	/* ------------------------------------------------------------------------------------------ */
	/**
	 * identifiant
	 * @var int $idarticle
	 * @access protected
	 */
	protected $idarticle;
	
	/**
	 * stratégies de calcul de prix
	 * @var ArrayList<PriceStrategy> $priceStrategies
	 * @access protected
	 */
	protected $priceStrategies;
	
	/**
	 * options
	 * @var ArrayList<SaleableOption> $options
	 * @access protected
	 */
	protected $options;

	/**
	 * Délai de livraison
	 * @var stdclass $deliveryTime
	 * @access protected
	 */
	protected $deliveryTime;

	/**
	 * Valeurs d'intitulés
	 * @avar array<string> $headingValues
	 * @access protected
	 */
	protected $headingValues;
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @param int $idarticle
	 * @param int $idcolor
	 * @param int $idcloth
	 */
	 public function __construct( $idarticle, $idcolor = 0, $idcloth = 0 ){
	 	
	 	if( !intval( $idarticle ) )
	 		trigger_error( "Impossible d'instancier la référence", E_USER_ERROR ) && exit();
	 	
	 	$this->idarticle 		= intval( $idarticle );
	 	$this->headingValues 	= array();
	 	
	 	$this->priceStrategies 	= new ArrayList( "PriceStrategy" );
	 	$this->options 			= new ArrayList( "SaleableOption" );

	 	parent::__construct( "detail", false, "idarticle", intval( $idarticle ) );
	 	
	 	if( $idcolor || $idcolor = DBUtil::getDBValue( "idcolor", "product", "idproduct", $this->recordSet[ "idproduct" ] ) ){
			
			include_once( dirname( __FILE__ ) . "/../../objects/Color.php" );
			$this->addOption( new Color( $this->recordSet[ "idproduct" ], intval( $idcolor ) ) );
			
		}
		
		if( $idcloth ){
				
			include_once( dirname( __FILE__ ) . "/../../objects/Cloth.php" );
			$this->addOption( new Cloth( $this->recordSet[ "idproduct" ], intval( $idcloth ) ) );
			
		}
	
	 	$this->setPriceStrategies();
		$this->setHeadingValues();
		
	 	$this->deliveryTime = DBUtil::stdclassFromDB( "delay", "iddelay", $this->get( "delivdelay" ) );
	
	 }
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return int
	 */
	public function getId(){ return $this->idarticle; }
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @param SaleableOption $option
	 * @return void
	 */
	 public function addOption( SaleableOption $option ){
	 	
	 	if( !$this->options->contains( $option ) )
		 	$this->options->add( $option );
	 	
	 }
	 
	 /* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return ArrayList<SaleableOption>
	 */
	 public function getOptions(){ return $this->options; }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return Color or false
	 */
	 public function &getColor(){ 
	 	
	 	$i = 0;
	 	while( $i < $this->options->size() ){
	 		
	 		if( $this->options->get( $i ) instanceof Color )
	 			return $this->options->get( $i ); 
	 	
	 		$i++;
	 		
	 	}
	 
	 	return false;
	 	
	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return Cloth or false
	 */
	 public function getCloth(){
	 
	 	$i = 0;
	 	while( $i < $this->options->size() ){
	 		
	 		if( $this->options->get( $i ) instanceof Cloth )
	 			return $this->options->get( $i ); 
	 	
	 		$i++;
	 		
	 	}
	 
	 	return false;
	 	
	 }
	 	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @param int $idintitule_title
	 * @return string
	 */
	public function getHeadingValue( $idintitule_title ){ return isset( $this->headingValues[ $idintitule_title ] ) ? $this->headingValues[ $idintitule_title ] : false; }
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @todo
	 * @access public
	 * @return bool
	 */
	public function hasPromo(){ return $this->getPromotion() != false; }
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * prix HT avant remise, sans option
	 * @access public
	 * @param int $idbuyer
	 * @param int $quantity
	 * @return float
	 */
	public function getPriceET(){
		
		if( B2B_STRATEGY )
			return $this->recordSet[ "sellingcost" ] ;
		
		return round( $this->recordSet[ "sellingcost" ] / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * prix TTC avant remise, sans option
	 * @access public
	 * @param int $idbuyer
	 * @param int $quantity
	 * @return float
	 */
	public function getPriceATI(){
		
		if( B2B_STRATEGY )
			return round( $this->recordSet[ "sellingcost" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
			
		return $this->recordSet[ "sellingcost" ];
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * prix de vente HT ( remisé, avec options )
	 * @access public
	 * @param int $idbuyer
	 * @param int $quantity
	 * @return float
	 */
	public function getDiscountPriceET( $idbuyer = 0, $quantity = 1 ){
		
		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
				
		$priceET = $this->getPriceET() + $this->getOptionsPriceET();
		
		$it = $this->priceStrategies->iterator();
		
		while( $it->hasNext() ){
			
			$strategyPrice = $it->next()->getPriceET( $this, $idbuyer, $quantity );

			if( $strategyPrice > 0.0 )
				$priceET = min( $priceET, $strategyPrice );

		}
		
		return $priceET;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * prix de vente TTC ( remisé, avec options )
	 * @access public
	 * @param int $idbuyer
	 * @param int $quantity
	 * @return float
	 */
	public function getDiscountPriceATI( $idbuyer = 0, $quantity = 1 ){
		
		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	
		$priceATI = $this->getPriceATI() + $this->getOptionsPriceATI();
		
		$it = $this->priceStrategies->iterator();
		
		while( $it->hasNext() ){
			
			$strategyPrice = $it->next()->getPriceATI( $this, $idbuyer, $quantity );

			if( $strategyPrice > 0.0 )
				$priceATI = min( $priceATI, $strategyPrice );

		}
		
		return $priceATI;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * prix de vente TTC ( remisé, avec options )
	 * @access public
	 * @param int $idbuyer
	 * @param int $quantity
	 * @return float
	 */
	public function getDiscountRate( $idbuyer = 0, $quantity = 1 ){

		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;

		$discountRate = 0.0;
		
		$it = $this->priceStrategies->iterator();
		
		while( $it->hasNext() )

			$discountRate = max( $discountRate, $it->next()->getDiscountRate( $this, $idbuyer, $quantity ) );

		return $discountRate;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float option's price
	 */
	public function getOptionsPriceET(){

		$optionPriceET = 0.0;
		
		$it = $this->options->iterator();
		while( $it->hasNext() ){
		
			$option =& $it->next();
			
			if( $option->getAmount() > 0.0 )
					$optionPriceET += B2B_STRATEGY ? $option->getAmount() : round( $option->getAmount() / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
			else 	$optionPriceET += round( $this->getPriceET() * $option->getRate() / 100.0, 2 );
				
		}
		
		return $optionPriceET;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float option's price
	 */
	public function getOptionsPriceATI(){

		$optionPriceATI = 0.0;
		
		$it = $this->options->iterator();
		while( $it->hasNext() ){
		
			$option =& $it->next();

			if( $option->getAmount() > 0.0 )
					$optionPriceATI += B2B_STRATEGY ? round( $option->getAmount() / ( 1.0 + $this->getVATRate() / 100.0 ), 2 ) : $option->getAmount();
			else 	$optionPriceATI += round( $this->getPriceATI() * $option->getRate() / 100.0, 2 );

		}
		
		return $optionPriceATI;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @return stdclass
	 */
	public function getDeliveryTime(){ return $this->deliveryTime; }
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return stdclass or false
	 */
	public function getPromotion(){
		
		$query = "
		SELECT * 
		FROM promo
		WHERE reference = " . DBUtil::quote( $this->recordSet[ "reference" ] ) . "
		AND begin_date <= NOW() AND end_date >= NOW() 
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		return $rs->RecordCount() ? ( object )$rs->fields : false;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return stdclass or false
	 */
	public function getDestocking(){

		$query = "
		SELECT *
		FROM destocking
		WHERE reference = " . DBUtil::quote( $this->recordSet[ "reference" ] ) . "
		AND begin_date <= NOW() AND end_date >= NOW()
		LIMIT 1";
		 
		$rs =& DBUtil::query( $query );
		
		return $rs->RecordCount() ? ( object )$rs->fields : false;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Define price strategies
	 * @access protected
	 * @return void
	 */
	protected function setPriceStrategies(){

		$rs =& DBUtil::query( "SELECT * FROM product_price_strategy WHERE enabled IS TRUE AND `auto` IS TRUE" );
		
		while( !$rs->EOF() ){
			
			$strategy = $rs->fields( "strategy" );
			
			include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
			$this->priceStrategies->add( new $strategy() );
			
			$rs->MoveNext();
			
		}

	}


	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access protected
	 * @return void
	 */
	protected function setHeadingValues(){
		
		$query = "
		SELECT il.idintitule_title, iv.intitule_value_1
		FROM intitule_link il, intitule_value iv
		WHERE il.idarticle = '{$this->idarticle}'
		AND il.idintitule_value = iv.idintitule_value
		ORDER BY il.title_display ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
			
			$this->headingValues[ $rs->fields( "idintitule_title" ) ] = $rs->fields( "intitule_value_1" );
			
			$rs->MoveNext();
				
		}
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @static
	 * @param int $idproduct
	 * @return CArticle
	 */
	public static function create( $idproduct ){
	
		include_once( dirname( __FILE__ ) . "/../Codifier.php" );
		include_once( dirname( __FILE__ ) . "/CProduct.php" );
		
		$codifier = new Codifier();
		
		if( !$codifier->isReferenceGeneratorAvailable() ){
			
			trigger_error( "Codification impossible", E_USER_ERROR );
			return;
				
		}

		$reference = $codifier->getNewReference( intval( $idproduct ) );
		
		DBUtil::query( "INSERT INTO detail( reference, idproduct ) VALUES( " . DBUtil::quote( $reference ) . ", '$idproduct' )" );
		
		$article = new self( DBUtil::getInsertID() );
		$product = new CProduct( intval( $idproduct ) );
		
		$display_detail_order = 0;
		$it = $product->getArticles()->iterator();
		
		while( $it->hasNext() )
			$display_detail_order = max( $display_detail_order, $it->next()->get( "display_detail_order" ) );
			
		if( $display_detail_order )
			$display_detail_order++;
			
		$article->set( "display_detail_order", 	$display_detail_order );
		$article->set( "available", 			0 );
		$article->set( "idsupplier", 			$product->get( "idsupplier" ) );
		$article->set( "summary_1", 			$product->get( "name_1" ) );
		
		$article->save();
		
		return $article;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Suppression
	 * @param int $idarticle
	 * @return bool en cas de succès, sinon false
	 */
	public static function delete( $idarticle ){
		
		if( !intval( $idarticle ) )
			return false;

		$tables = array(
		
			"analyse_price",
			"destocking",
			"grouping_catalog",
			"multiprice_buyer",
			"multiprice_family",
			"promo",
			"tender_history"
		
		);
		
		foreach( $tables as $table )
			if( DBUtil::query( "DELETE `$table`.* FROM `$table`, detail d WHERE d.idarticle = '" . intval( $idarticle ) . "' AND d.reference = `$table`.reference" ) === false )
				return false;

		$tables = array(
		
			"buyingcost_history",
			"intitule_link",
			"sellingcost_history",
			"spe_categories",
			"customer_price",
			"group_price",
			"customer_profil_price",
			"detail"
			
		);
		
		foreach( $tables as $table )
			if( DBUtil::query( "DELETE FROM `$table` WHERE idarticle = '" . intval( $idarticle ) . "'" ) === false )
				return false;

		if( $files = glob( dirname( __FILE__ ) . "/../../images/references/" . intval( $idarticle ) . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) )
			@unlink( $files[ 0 ] );
			
		return true;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Prix au volume
	 * @return array<stdclass> ou NULL
	 */
	public function getPriceRanges(){
		
		$query ="
		SELECT *
		FROM quantity_price 
		WHERE idarticle = '{$this->idarticle}'
		ORDER BY min_quantity ASC";
		
		$rs =& DBUtil::query( $query );
		
		if( !$rs->RecordCount() )
			return NULL;

		$ranges = array();
		
		while( !$rs->EOF() ){
			
			array_push( $ranges, ( object )array(
				"reference" 	=> $rs->fields( "reference" ),
				"min_quantity" 	=> $rs->fields( "min_quantity" ),
				"max_quantity" 	=> $rs->fields( "max_quantity" ) > 0 ? $rs->fields( "max_quantity" ) : false,
				"price" 		=> $rs->fields( "price" ) > 0.0 ? $rs->fields( "price" ) : false,
				"price_cost" 		=> $rs->fields( "price_cost" ) > 0.0 ? $rs->fields( "price_cost" ) : false,				
				"rate" 			=> $rs->fields( "rate" ) > 0.0 ? $rs->fields( "rate" ) : false
			
			) );
			
			$rs->MoveNext();
			
		}
		
		return $ranges;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * ajout dynamique d'une stratégie de calcul de prix
	 * @param PriceStrategy $strategy
	 * @return void
	 */
	public function addPriceStrategy( PriceStrategy $strategy ){
		
		$i = 0;
		while( $i < $this->priceStrategies->size() ){
			
			if( get_class( $this->priceStrategies->get( $i ) == get_class( $strategy ) ) )
				return;
				
			$i++;
				
		}
		
		$this->priceStrategies->add( $strategy );
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float le taux de TVA pouvant être appliqué
	 */
	public function getVATRate(){
		
		if( !$this->recordSet[ "idvat" ] )
			return 0.0;
			
		return DBUtil::getDBValue( "tva", "tva", "idtva", $this->recordSet[ "idvat" ] );
		
	}
	
	
	/* ------------------------------------------------------------------------------------------ */
	
}

?>