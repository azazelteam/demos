<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object fournisseurs de service
 */


include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/ProviderContact.php" );
include_once( dirname( __FILE__ ) . "/ProviderAddress.php" );


class Provider extends DBObject{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var ArrayList $contacts
	 */
	protected $contacts;
	/**
	 * @var ProviderContact $currentContact
	 */
	protected $currentContact;
	/**
	 * @var ProviderAddress $address
	 */
	protected $address;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idprovider
	 * @param int $idcontact
	 */
	public function __construct( $idprovider, $idcontact = false ){
		
		parent::__construct( "provider", false, "idprovider", intval( $idprovider ) );

		$this->setContacts( intval( $idcontact ) ? intval( $idcontact ) : $this->recordSet[ "main_contact" ] );
		
		$this->address = new ProviderAddress( intval( $idprovider ) );
		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Charge les informations des contacts
	 * @param int $idcontact le contact courant
	 * @return void
	 */
	protected function setContacts( $idcontact ){

		$this->contacts = new ArrayList( "ProviderContact" );
		
		$query = "
		SELECT * FROM `provider_contact` 
		WHERE `idprovider` = '" . $this->recordSet[ "idprovider" ] . "'
		ORDER BY idcontact ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){

			$this->contacts->add( new ProviderContact( $this->recordSet[ "idprovider" ], $rs->fields( "idcontact" ) ) );
			$rs->MoveNext();
			
		}
		
		/* contact courant */
		
		$it = $this->contacts->iterator();
		
		while( $it->hasNext() ){

			$contact =& $it->next();
			
			if( ( !$idcontact && $contact->isMainContact() ) || $contact->getId() == $idcontact )	
				$this->currentContact =& $contact;

		}
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @return Provider
	 */
	public static function create(){

		if( DBUtil::query( "INSERT INTO `provider` VALUES()" ) === false ){
			 
			trigger_error( "Impossible de créer le nouveau client", E_USER_ERROR );
			die();
			
		}
		
		$idprovider = DBUtil::getInsertId();
		
		$contact = ProviderContact::create( $idprovider );
		
		return new Provider( $idprovider, $contact->getId() );

	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'identifiant du prestataire
	 * @return int
	 */
	public function getId(){ return $this->recordSet[ "idprovider" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return ArrayList
	 */
	public function &getContacts(){ return $this->contacts; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return ProviderContact
	 */
	public function &getContact(){ return $this->currentContact; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'adresse du prestataire
	 * L'adress n'est pas éditable car les données sont dans la table provider, préférer la classe Provider pour modifier les données
	 * @see objects/Deliverer#getAddress()
	 * @return ProviderAddress l'adresse du prestataire
	 */
	public function &getAddress(){ return $this->address; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @see objects/DBObject#save()
	 */
	public function save(){

		parent::save();

		$it = $this->contacts->iterator();
		
		while( $it->hasNext() )
			$it->next()->save();

		$this->address = new ProviderAddress( $this->recordSet[ "idprovider" ] );
			
	}

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access static
	 * @param int $idprovider
	 * @return void
	 */
	public static function delete( $idprovider ){
		
		$provider = new Provider( $idprovider );
		
		if( !$provider->allowDelete() ){
			
			trigger_error( "Impossible de supprimer le prestataire de service", E_USER_ERROR );
			return;
			
		}
		
		DBUtil::query( "DELETE FROM provider WHERE idprovider = '" . intval( $idprovider ) . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM provider_contact WHERE idprovider = '" . intval( $idprovider ) . "'" );
		DBUtil::query( "DELETE FROM carrier WHERE idprovider = '" . intval( $idprovider ) . "' LIMIT 1" );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return bool
	 * @return bool
	 */
	public function allowDelete(){
		
		global $GLOBAL_DB_NAME;
		
		$rs =& DBUtil::query( "SHOW TABLES" );
	
		while( !$rs->EOF() ){
		
			$tablename = $rs->fields( "Tables_in_$GLOBAL_DB_NAME" );
			
			if( $tablename != "provider" && $tablename != "provider_contact" ){
				
				$rs2 =& DBUtil::query( "SHOW COLUMNS FROM `$tablename` LIKE 'idprovider'" );
				
				if( $rs2->RecordCount() ){
				
					if( DBUtil::query( "SELECT idprovider FROM `$tablename` WHERE idprovider = '" . $this->recordSet[ "idprovider" ] . "' LIMIT 1" )->RecordCount() )
						return false;
						
				}
			
			}
			
			$rs->MoveNext();
			
		}
	
		return true;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>