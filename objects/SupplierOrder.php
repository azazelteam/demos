<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entêtes commandes fournisseurs
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/SupplierOrderItem.php" );
include_once( dirname( __FILE__ ) . "/ForwardingAddress.php" );
include_once( dirname( __FILE__ ) . "/Purchase.php" );
include_once( dirname( __FILE__ ) . "/Delivery.php" );
include_once( dirname( __FILE__ ) . "/SecuredDocument.php" );
include_once( dirname( __FILE__ ) . "/Supplier.php" );


/**
 * Commande fournisseur
 */
class SupplierOrder extends DBObject implements SecuredDocument, Purchase, Delivery{
   
   	//----------------------------------------------------------------------------
	//static members
	
	public static $STATUS_CONFIRMED 			= "confirmed";
	public static $STATUS_DISPATCHED 			= "dispatch";
	public static $STATUS_SENT_BY_FAX 			= "SendByFax";
	public static $STATUS_STANDBY 				= "standby";
	public static $STATUS_INVOICED 				= "invoiced";
	public static $STATUS_SENT_BY_MAIL			= "SendByMail";
	public static $STATUS_RECEIVED 				= "received";
	public static $STATUS_DELIVERED 			= "delivered";
	public static $STATUS_CANCELLED 			= "cancelled";
	
	public static $BUYER_FORWARDING_ADDRESS 	= 0;
	public static $INTERNAL_FORWARDING_ADDRESS 	= 1;
	
	public static $STOCK_TYPE_INTERNAL 			= 1;
	public static $STOCK_TYPE_EXTERNAL			= 2;
	
	//----------------------------------------------------------------------------
	/**
	 * @var int $idorder_supplier le numéro de la commande fournisseur
	 * @access private
	 */
	 private $idorder_supplier;
	 /**
	 * @var TradePriceStrategy $priceStrategy
	 */
	protected $priceStrategy;
	 /**
	 * @var Address l'adresse de livraison
	 * @access protected
	 */
	private $forwardingAddress;
	/**
	 * @var Supplier supplier
	 * @access private
	 */
	private $supplier;
	/**
	 * @var Customer $customer
	 */
	private $customer;
	/**
	 * @var Salesman $salesman
	 */
	private $salesman;
	
	//----------------------------------------------------------------------------
	/**
	 * @param int $idorder_supplier
	 */
	function __construct( $idorder_supplier ){
		
		$this->idorder_supplier =  $idorder_supplier;
		
		parent::__construct( "order_supplier", false, "idorder_supplier",  $idorder_supplier  );
		
		$this->setPriceStrategy();
		$this->setItems();
		
		$this->salesman = new Salesman( $this->recordSet[ "iduser" ] );
		$this->supplier = new Supplier( $this->recordSet[ "idsupplier" ] );
		$this->customer = new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );

		if( $this->recordSet[ "iddelivery" ] )
				$this->forwardingAddress = new ForwardingAddress( $this->recordSet[ "iddelivery" ] );
		else 	$this->forwardingAddress = $this->customer->getAddress();

			
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access protected
	 * @return void
	 */
	protected function setPriceStrategy(){
		
		$rs =& DBUtil::query( 
		
			"SELECT strategy 
			FROM trade_price_strategy 
			WHERE `datetime` <= '" . $this->recordSet[ "DateHeure" ] . "'
			ORDER BY datetime DESC
			LIMIT 1" 
		
		);
		
		if( !$rs->RecordCount() 
			|| !file_exists( dirname( __FILE__ ) . "/strategies/" . $rs->fields( "strategy" ) . ".php" ) ){

			trigger_error( "stratégie de calcul de prix inconnue", E_USER_ERROR );
			exit();
			
		}

		$strategy = $rs->fields( "strategy" );
		
		include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
		$this->priceStrategy = new $strategy( $this );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return int
	 */
	public function getId(){ return $this->recordSet[ "idorder_supplier" ]; }

	//----------------------------------------------------------------------------
	/**
	 * Retourne le fournisseur de la commande
	 * @return Supplier le fournisseur de la commande
	 */
	public function &getSupplier(){ return $this->supplier; }
	
	//----------------------------------------------------------------------------

    public function allowRead(){
    
    	if( User::getInstance()->getId() == $this->salesman->getId() || User::getInstance()->getHasAdminPrivileges() ) 
			return true;
		
		return false;
		
    }
    
    //----------------------------------------------------------------------------

    public function allowEdit(){
    
		if( User::getInstance()->getId() == $this->getSalesman()->getId() ) 
			return true;
		
		if( $this->getCustomer()->get( "iduser" ) == User::getInstance()->getId() )
			return true;

		if( User::getInstance()->getHasAdminPrivileges() )
			return true;
	
		return false;
		
    }
    
    //----------------------------------------------------------------------------

	public static function allowCreationForCustomer( $idbuyer ){
	
		return User::getInstance()->getHasAdminPrivileges() 
			|| User::getInstance()->getId() == DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $idbuyer );
	
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "SupplierOrderItem" );
		
		$rs =& DBUtil::query( "SELECT idrow FROM order_supplier_row WHERE idorder_supplier = '{$this->idorder_supplier}' ORDER BY idrow ASC" );

		while( !$rs->EOF() ){
		
			$this->items->add( new SupplierOrderItem( $this, $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}

	//----------------------------------------------------------------------------
	/**
     * @access protected
     * @return SupplierOrderItem une ligne article
     */
	protected function &createItem(){
		
		$idrow = $this->items->size() + 1;
		
		DBUtil::query( "INSERT INTO order_supplier_row( idorder_supplier, idrow ) VALUES( '{$this->idorder_supplier}', '$idrow' )" );

		$this->items->add( new SupplierOrderItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBObject#save()
	 */
	public function save(){
		
		if( !$this->allowEdit() ){
			
			trigger_error( "Appel de DBObject::save() non autorisé", E_USER_ERROR );
			return;
			
		}
		
		$this->recalculateAmounts();
		
		DBUtil::query( "DELETE FROM order_supplier_row WHERE idorder_supplier = '{$this->idorder_supplier}'" );
		
		$it = $this->items->iterator();
	
		while( $it->hasNext() )
			$it->next()->save();
			
		parent::save();			
	
	}
		
	//----------------------------------------------------------------------------
	/**
	 * Supprime une commande fournisseur donnée
	 * @static
	 * @param int $idorder_supplier le numéro de la commande fournisseur
	 * @return bool
	 */
	public static function delete( $idorder_supplier ){

		if( DBUtil::query( 
			"SELECT 1 FROM bl_delivery 
			WHERE idorder_supplier = '" .  $idorder_supplier . "' 
			AND idbilling_buyer <> 0
			LIMIT 1" )->RecordCount() )
			return false;
	
		if( DBUtil::query( 
			"SELECT 1 
			FROM bl_delivery bl, billing_supplier_row bsr
			WHERE bl.idorder_supplier = '" .  $idorder_supplier  . "' 
			AND bl.idbl_delivery = bsr.idbl_delivery
			LIMIT 1" )->RecordCount() )
			return false;
			
		DBUtil::query( "DELETE FROM `order_supplier` WHERE `idorder_supplier` = '" .  $idorder_supplier  . "'" );
		DBUtil::query( "DELETE FROM `order_supplier_row` WHERE `idorder_supplier` = '" .  $idorder_supplier . "'" );
		DBUtil::query( "DELETE FROM `supplier_dispatch_reminder` WHERE `idorder_supplier` = '" .  $idorder_supplier  . "'" );
		DBUtil::query( "DELETE FROM `supplier_confirmation_reminder` WHERE `idorder_supplier` = '" .  $idorder_supplier  . "'" );

		$rs = DBUtil::query( "SELECT idbl_delivery FROM bl_delivery WHERE `idorder_supplier` = '" .  $idorder_supplier  . "'" );
		
		if( !$rs->RecordCount() )
			return true;
			
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		while( !$rs->EOF() ){
			
			DeliveryNote::delete( $rs->fields( "idbl_delivery" ) );
			$rs->MoveNext();
			
		}
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return void
	 */
	public function cancel(){
	
		$this->recordSet[ "status" ] = self::$STATUS_CANCELLED;
		$this->save();

		//suppression des BL
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		$it = $this->getDeliveryNotes()->iterator();
		
		while( $it->hasNext() )
			DeliveryNote::delete( $it->next()->getId() );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Basket#setCustomer($customer)
	 */
	public function setCustomer( Customer $customer ){
	
		$this->recordSet[ "idbuyer" ] 			= $customer->getId();
		$this->recordSet[ "idcontact" ] 		= $customer->getContact()->getId();
		$this->recordSet[ "iddelivery" ] 		= 0;
		
		$this->forwardingAddress 			= $customer->getAddress();
		$this->customer 					= $customer;
	
		//recalcul des montants
		
		$this->recalculateAmounts();	
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @param CArticle $article l'identifiant de l'article à ajouter
	 * @param int $quantity la quantité souhaitée, ( optionnel, 1 par défaut )
	 * @return void
	 */
	public function addArticle( CArticle $article, $quantity = 1, $idorder_supplier_original = 0 ){
		
		if( $this->recordSet[ "idsupplier" ] != DBUtil::getParameterAdmin( "internal_supplier" )
			&& $this->recordSet[ "idsupplier" ] != DBUtil::getDBValue( "idsupplier", "detail", "idarticle", $article->getId() ) )
			return;
			
		$item =& $this->createItem();
		
		/* attention aux produits spé et alternatifs */
		
		if( $article->get( "type" ) == "specific" )	
				$idcategory = ( $ret = DBUtil::getDBValue( "idcategory", "spe_categories", "idarticle", $article->getId() ) ) ? $ret : 0;
		else 	$idcategory = $article->get( "idproduct" ) ? DBUtil::getDBValue( "idcategory", "product", "idproduct", $article->get( "idproduct" ) ) : 0;
	
		$item->set( "idarticle", 					$article->getId() );
		$item->set( "quantity", 					$quantity );
		$item->set( "reference", 					$article->get( "reference" ) );
		$item->set( "reference_base", 				$article->get( "reference" ) );
		$item->set( "n_article", 					$article->get( "n_article" ) );
		$item->set( "gencod", 						$article->get( "gencod" ) );
		$item->set( "code_customhouse", 			$article->get( "code_customhouse" ) );
		$item->set( "ref_supplier", 				$article->get( "ref_supplier" ) );
		$item->set( "unit", 						$article->get( "unit" ) );
		$item->set( "lot", 							$article->get( "lot" ) );
		$item->set( "designation", 					"<b>" . $article->get( "summary_1" )."</b><br />" . $article->get( "designation_1" ) );
		$item->set( "summary", 						$article->get( "summary_1" ) );
		$item->set( "delivdelay", 					$article->get( "delivdelay" ) );
		$item->set( "weight", 						$article->get( "weight" ) );
		$item->set( "idcategory", 					$idcategory );
		$item->set( "useimg", 						1 );
		$item->set( "quantity_per_linear_meter",	$article->get( "quantity_per_linear_meter" ) );
		$item->set( "quantity_per_truck", 			$article->get( "quantity_per_truck" ) );
		$item->set( "quantity_per_pallet", 			$article->get( "quantity_per_pallet" ) );
		$item->set( "idorder_supplier_original",	$idorder_supplier_original );

		/* prix d'achat */
		
		$item->set( "vat_rate", 			$article->getVATRate() );
		$item->set( "buyingcost", 			$article->get( "buyingcost" ) );
		$item->set( "suppliercost", 		$article->get( "suppliercost" ) );
		$item->set( "unit_price", 			round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "discount_price", 		round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "ref_discount", 		0.0 );
		
		$this->save();

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Ajoute un article de la commande
	 * @param OrderItem l'article de la commande à ajouter
	 * @return void
	 */
	public function addOrderItem( OrderItem $orderItem ){
		
		$internalSupplierID = DBUtil::getParameterAdmin( "internal_supplier" );
		
		if( $this->recordSet[ "idsupplier" ] == $internalSupplierID && $orderItem->get( "quantity" ) == $orderItem->get( "external_quantity" ) )
			return;
		
		if( $this->recordSet[ "idsupplier" ] != $internalSupplierID && $orderItem->get( "external_quantity" ) == 0 )
			return;
		
		$quantity = $this->recordSet[ "idsupplier" ] == $internalSupplierID ? $orderItem->get( "quantity" ) - $orderItem->get( "external_quantity" ) : $orderItem->get( "external_quantity" );
		
		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$this->addArticle( new CArticle( $orderItem->get( "idarticle" ) ), $quantity );
		
		$supplierOrderItem = $this->items->get( $this->items->size() - 1 );
	
		$supplierOrderItem->set( "idorder_row", 		$orderItem->get( "idrow" ) );
		$supplierOrderItem->set( "unit_price", 			$orderItem->get( "unit_cost" ) );
		$supplierOrderItem->set( "reference", 			$orderItem->get( "reference" ) );
		$supplierOrderItem->set( "reference_base", 		$orderItem->get( "reference_base" ) );
		$supplierOrderItem->set( "alternative", 		$orderItem->get( "alternative" ) );
		$supplierOrderItem->set( "n_article", 			$orderItem->get( "n_article" ) );
		$supplierOrderItem->set( "gencod", 				$orderItem->get( "gencod" ) );
		$supplierOrderItem->set( "code_customhouse", 	$orderItem->get( "code_customhouse" ) );
		$supplierOrderItem->set( "ref_supplier", 		$orderItem->get( "ref_supplier" ) );
		$supplierOrderItem->set( "unit", 				$orderItem->get( "unit" ) );
		$supplierOrderItem->set( "lot", 				$orderItem->get( "lot" ) );
		$supplierOrderItem->set( "designation", 		$orderItem->get( "designation" ) );
		$supplierOrderItem->set( "summary", 			$orderItem->get( "summary" ) );
		$supplierOrderItem->set( "ref_discount", 		$orderItem->get( "unit_cost_rate" ) );
		$supplierOrderItem->set( "discount_price", 		$orderItem->get( "unit_cost_amount" ) );
		$supplierOrderItem->set( "delivdelay", 			$orderItem->get( "delivdelay" ) );
		$supplierOrderItem->set( "vat_rate", 			$orderItem->get( "vat_rate" ) );
		$supplierOrderItem->set( "weight", 				$orderItem->get( "weight" ) );
		$supplierOrderItem->set( "useimg", 				$orderItem->get( "useimg" ) );
		$supplierOrderItem->set( "iditinerary", 		DBUtil::getDBValue( "iditinerary", "supplier", "idsupplier", $orderItem->get( "idsupplier" ) ) );
		$supplierOrderItem->set( "quantity_per_linear_meter", $orderItem->get( "quantity_per_linear_meter" ) );		
		$supplierOrderItem->set( "quantity_per_truck", 	$orderItem->get( "quantity_per_truck" ) );	
		$supplierOrderItem->set( "quantity_per_pallet", $orderItem->get( "quantity_per_pallet" ) );	
		$supplierOrderItem->set( "idcolor", 			$orderItem->get( "idcolor" ) );
		$supplierOrderItem->set( "idtissus", 			$orderItem->get( "idtissus" ) );
		
		if( strlen( $orderItem->get( "ref_replace" ) ) ){
		
			$supplierOrderItem->set( "reference",      $orderItem->get( "ref_replace" ) );
			$supplierOrderItem->set( "reference_base", $orderItem->get( "ref_base_replace" ) );
			$supplierOrderItem->set( "ref_supplier",   $orderItem->get( "ref_supplier_replace" ) );
			$supplierOrderItem->set( "alternative",    $orderItem->get( "alternative_replace" ) );
		
		}

		$this->recalculateAmounts();

	}

	//----------------------------------------------------------------------------
	/**
	 * @return Address une référence vers l'adresse de livraison
	 */
	public function &getForwardingAddress(){ return $this->forwardingAddress; }
	
	//----------------------------------------------------------------------------
	/**
	 * Définit l'adresse de livraison à utiliser
	 * @param Address $address une adresse de livraison
	 * @return void
	 */
	public function setForwardingAddress( Address $address ){
		
		$this->forwardingAddress = $address;
		$this->set( "iddelivery", $this->forwardingAddress instanceof ForwardingAddress ? $this->forwardingAddress->getID() : 0 );

	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#applyVAT()
	 */
	public function applyVAT(){

		if( $this->recordSet[ "no_tax_export" ] == 2 )
			return true;

		if( $this->recordSet[ "no_tax_export" ] == 1 )
			return false;
			
		if( !DBUtil::getDBValue( "bvat", "state", "idstate", $this->supplier->get( "idstate" ) ) )
			return false;
		
		if( !DBUtil::getDBValue( "bvat", "state", "idstate", $this->forwardingAddress->getIdState() ) )
			return false;

		return true;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le taux de TVA à appliquer
	 * @return float
	 */
	public function getVATRate(){

		static $defaultVATRate = NULL;
		
		if( !$this->applyVAT() )
			return 0.0;
			
		if( $defaultVATRate === NULL )
			$defaultVATRate = DBUtil::getParameter( "vatdefault" );
			
		return $defaultVATRate;
	
	}

	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le montant total HT
	 */
	public function getTotalET(){ return $this->priceStrategy->getTotalET(); }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le montant total TTC
	 */
	public function getTotalATI(){ return $this->priceStrategy->getTotalATI(); }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getChargesET($forced)
	 */
	public function getChargesET(){ return $this->recordSet[ "total_charge_ht" ]; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Calcule les frais de port vente TTC
	 * @return float
	 */
	public function getChargesATI(){ return round( $this->getChargesET() * ( 1.0 + $this->getVATRate() / 100 ), 2 ); }
	
	/* ----------------------------------------------------------------------------------------------------- */	/**
	 * @return float le net à payer sur facture
	 */
	public function getNetBill(){ return $this->priceStrategy->getNetBill(); }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getDiscountRate()
	 */
	public function getDiscountRate(){ return $this->recordSet[ "total_discount" ]; }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getBillingRate()
	 */
	public function getBillingRate(){ return $this->recordSet[ "billing_rate" ]; }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getRebateAmount()
	 */
	public function getRebateAmount(){ return $this->recordSet[ "rebate_type" ] == "amount" ? $this->recordSet[ "rebate_value" ] : round( $this->getTotalATI(), 2 ) * $this->recordSet[ "rebate_value" ] / 100.0; }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getDownPaymentAmount()
	 */
	public function getDownPaymentAmount(){ return $this->recordSet[ "down_payment_type" ] == "rate" ? round( $this->getTotalATI() * $this->recordSet[ "down_payment_value" ] / 100.0, 2 ) : $this->recordSet[ "down_payment_value" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le pourcentage d'acompte
	 */
	public final function getDownPaymentRate(){

		if( $this->recordSet[ "down_payment_type" ] == "rate" )
			return $this->recordSet[ "down_payment_value" ];

		return $this->getTotalATI() > 0.0 ? round( $this->recordSet[ "down_payment_value" ] / $this->getTotalATI() * 100, 2 ) : 0.0;

	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * @todo
	 * (non-PHPdoc)
	 * @see objects/Trade#getCreditAmount()
	 */
	public function getCreditAmount(){ return 0.0; }

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne le poids total
	 * @return float
	 */
	public function getWeight(){ 
		
		$weight = 0.0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			$weight += $item->getWeight() * $item->getQuantity();
			
		}
		
		return $weight; 
		
	}

	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return ArrayList<TradeItem>
     */
    public function &getItems(){ return $this->items; }
    
	//----------------------------------------------------------------------------
	/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public final function getItemCount() { return $this->items->size(); }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le $index-ième élément du panier
	 * @return SupplierOrderItem
	 */
	public final function &getItemAt( $index ) { return $this->items->get( $index ); }
	
	//----------------------------------------------------------------------------
	/**
	 * Suppression de tous les articles du panier
	 * @access public
	 * @return void
	 */
	public function removeItems(){

		$this->items = new ArrayList();
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Supprime un élément donné du panier
	 * @param int $index Le $index-ième élément à supprimer
	 * @return void
	 */
	public function removeItemAt( $index ){
		
		$this->items->remove( $this->items->get( $index ) );
		
		/* réindexation */
		
		$index = 1;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$it->next()->set( "idrow", $index++ );
			
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return Customer
	 */
	public function &getCustomer(){ return $this->customer; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Purchase#getSalesman()
	 */
	public function &getSalesman(){ return $this->salesman; }
	
	//----------------------------------------------------------------------------
	/**
	 * Assigne un commercial donné à la commande
	 * @param Salesman $salesman
	 * @return void
	 */
	public function setSalesman( Salesman $salesman ){
		
		$this->set( "iduser", $salesman->getId() );
		$this->salesman = $salesman;
				
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Récupère la liste des BL pour une commande fournisseur
	 * @access public
	 * @param int $idorder_supplier le numéro de la commande fournisseur
	 * @return ArrayList<DeliveryNote>
	 */
	public function getDeliveryNotes(){
	
		static $deliveryNotes = NULL;
		
		if( $deliveryNotes !== NULL )
			return $deliveryNotes;
		
		$deliveryNotes = new ArrayList( "DeliveryNote" );
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		$rs = DBUtil::query( "SELECT idbl_delivery FROM bl_delivery WHERE idorder_supplier = '{$this->idorder_supplier}' ORDER BY idbl_delivery ASC" );
	
		while( !$rs->EOF() ){
			
			$deliveryNotes->add( new DeliveryNote( $rs->fields( "idbl_delivery" ) ) );
			$rs->MoveNext();
			
		}
		
		return $deliveryNotes;
	
	}

	//----------------------------------------------------------------------------
	
	protected function recalculateAmounts(){

		$this->recordSet[ "total_amount" ]         = $this->getTotalATI();
		$this->recordSet[ "total_amount_ht" ]      = $this->getTotalET();

		$articlesET = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();

		$this->recordSet[ "total_discount_amount" ] = round( $articlesET * $this->get( "total_discount" ) / 100.0, 2 );
        $this->recordSet[ "billing_amount" ]		= round( $this->recordSet[ "billing_rate" ] * ( $articlesET - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge_ht" ] ) / 100.0, 2 );
		$this->recordSet[ "charge_vat_rate" ] 		= $this->getVATRate();

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Calcul le délai prévisionnel
	 * Calculé à partir du délai de livraison le plus grand dans les lignes articles
	 * @static
	 * @access public
	 * @param int $idorder_supplier le numéro de la commande fournisseur
	 * @return int le délai en jours
	 */
	public static function getProvisionalDelay( $idorder_supplier ){
			// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "	SELECT
						MAX( delaydatetime ) AS delay
					FROM
						delay,
						detail d,
						order_supplier_row osr
					WHERE
						osr.idorder_supplier = '$idorder_supplier'
						AND osr.idarticle = d.idarticle
						AND delay.iddelay = d.delivdelay";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			trigger_error( "Problème lors de la récupération du délais de livraison prévisionnel de la commande.", E_USER_ERROR );
		
		if( !$rs->RecordCount() )
			return 0;
		
		return $rs->fields( "delay" );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Calcul la date prévisionnelle de mise à disposition
	 * Calculé à partir du délai de livraison le plus grand dans les lignes articles
	 * @static
	 * @access public
	 * @param int $idorder_supplier le numéro de la commande fournisseur
	 * @return string la date de mise à disposition au format MySQL
	 */
	public static function getProvisionalAvailabilityDate( $idorder_supplier ){
		
		$delay = SupplierOrder::getProvisionalDelay( $idorder_supplier );
		
		if( $delay == 0 )
			return false;
			// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "SELECT DateHeure FROM order_supplier WHERE idorder_supplier = '$idorder_supplier'";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			trigger_error( "Problème lors de la récupération du délais de livraison prévisionnel de la commande.", E_USER_ERROR );
		
		return date( "Y-m-d", mktime( 0, 0, 0, substr( $rs->fields( "DateHeure" ), 5, 2 ), substr( $rs->fields( "DateHeure" ), 8, 2 ) + $delay, substr( $rs->fields( "DateHeure" ), 0, 4 ) ) );
		
	}

	//----------------------------------------------------------------------------
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//============================================================================
	// 								Deprecated members
	//============================================================================
	 
	//-------------------------------------------------------------------------------
	
	/**
	 * Mise a jour des adresses de livraison et de facturation d'un devis
	 * @deprecated
	 */
	public static function updateAddress($id,$addressField,$idAddress){

		return DBUtil::query("UPDATE `order_supplier` SET `$addressField`='$idAddress' WHERE `idorder_supplier`='$id'");
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * @deprecated
	 * @todo déplacer cette méthode, cf. facturation partielle
	 * @todo ne pas traiter les formulaires dans les objets
	 * Met à jour les dates de livraisons et de mise à disposition dans le bl
	 * @return void
	 * 
	 */
	function UpdateBlDeliveryDates(){
		
		//date d'expédition
		
		if( isset( $_POST[ "dispatch_date" ] ) && !empty( $_POST[ "dispatch_date" ] ) ){

			$tmp = explode( "-", $_POST[ "dispatch_date" ] );
			
			if( count( $tmp ) == 3 ){
			
				list( $day, $month, $year ) = $tmp;
				
				if( checkdate( $month, $day, $year ) )
						$dispatch_date =  "$year-$month-$day";
				else 	$dispatch_date = false;
				
			}
			
		}
		
		//date de mise à disposition
		
		if( isset( $_POST[ "availability_date" ] ) && !empty( $_POST[ "availability_date" ] ) ){

			$tmp = explode( "-", $_POST[ "availability_date" ] );
			
			if( count( $tmp ) == 3 ){
			
				list( $day, $month, $year ) = $tmp;
				
				if( checkdate( $month, $day, $year ) )
						$availability_date =  "$year-$month-$day";
				else 	$availability_date = false;
				
			}
			
		}
		

		if( $availability_date == false && $dispatch_date != false )
			$availability_date = $dispatch_date;
		
		//mettre tous les BL à jour
		//@todo : facturation partielle : ne pas gérer ces dates depuis la commande fournisseur sivouplé
		
		$query = "
		UPDATE `bl_delivery` 
		SET availability_date = '$availability_date', 
			dispatch_date='$availability_date' 
		WHERE idorder_supplier = '" . $this->recordSet[ "idorder_supplier" ] . "'";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs===false )
			die("Impossible de mettre à jour les dates des BL");
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Récupère la liste des BL pour une commande fournisseur
	 * @deprecated préférer SupplierOrder::getDeliveryNotes()	
	 * @static
	 * @access public
	 * @param int $idorder_supplier le numéro de la commande fournisseur
	 * @return array un tableau indicé contenant tous les numéros de BL
	 */
	public static function getBLs( $idorder_supplier ){
		
		$rs = DBUtil::query( "SELECT idbl_delivery FROM bl_delivery WHERE idorder_supplier = '$idorder_supplier' ORDER BY idbl_delivery ASC" );
		
		$deliveryNotes = array();
		while( !$rs->EOF() ){
			
			array_push( $deliveryNotes, $rs->fields( "idbl_delivery" ) );
			$rs->MoveNext();
			
		}
		
		return $deliveryNotes;
		
	}
	
	//-------------------------------------------------------------------------------
	
}

?>