<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object bons de réduction (non utilisé)
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );

class Voucher extends DBObject{
	
	//----------------------------------------------------------------------------
	
	/**
	 * @var string $TYPE_AMOUNT
	 * @static
	 * @access public
	 */
	public static $TYPE_AMOUNT 	= "amount";
	/**
	 * @var string $TYPE_RATE
	 * @static
	 * @access public
	 */
	public static $TYPE_RATE 	= "rate";
	
	private $fields;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idvoucher le numéro de bon de réduction
	 */
	
	function __construct( $idvoucher ){
		
		parent::__construct( "vouchers", false, "idvoucher", intval( $idvoucher ) );
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Crée un bon de réduction
	 * @return Voucher une référence vers le nouvel objet créé
	 */
	
	public static function &create( $code, $type, $value, $expiration_date ){
		
		if( $type != self::$TYPE_AMOUNT && $type != self::$TYPE_AMOUNT )
			trigger_error( "Type de bon de réduction inconnu", E_USER_ERROR );
			
		$query = "
		INSERT INTO `vouchers` ( 
			`code`,  
			`expiration_date`,
			`type`,
			`value`,
			`creation_date`
		) VALUES ( 
			" . DBUtil::quote( $code ) . ", 
			" . DBUtil::quote( $expiration_date ) . ", 
			" . DBUtil::quote( $type ) . ", 
			" . v( $value ) . ",
			NOW()
		)";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			trigger_error( "Impossible de créer le bon de réduction", E_USER_ERROR );
		
		$voucher = new Voucher( DBUtil::getInsertId() );
		
		return $voucher;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Vérifie si le bon de réduction est valable
	 * @param string $code la numéro du bon
	 * @return boolean true si le bon est valide, sinon false
	 */
	
	public static function isAvailable( $code ){
		
		$query = "
		SELECT idvoucher 
		FROM `vouchers` 
		WHERE `code` = " . DBUtil::quote( $code ) . "
		AND expiration_date >= NOW()
		LIMIT 1";
	
		return DBUtil::query( $query )->RecordCount() != 0;
		
	}

	//----------------------------------------------------------------------------
	
}

?>