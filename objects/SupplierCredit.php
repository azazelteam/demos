<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object avoirs fournisseurs
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Supplier.php" );


class SupplierCredit extends DBObject{

	//----------------------------------------------------------------------------
    /**
     * @var Supplier $supplier
     */
    private $supplier;

    //----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $idcredit_supplier le numéro de l'avoir fournisseur
	 * @param int $idsupplier le numéro du fournisseur
	 */
	function __construct( $idcredit_supplier , $idsupplier ){
		
		parent::__construct( "credit_supplier", false, "idcredit_supplier", $idcredit_supplier, "idsupplier",  $idsupplier  );

		$this->supplier	= new Supplier( $this->recordSet[ "idsupplier" ] );
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Créé un nouvel avoir fournisseur vierge pour un fournisseur donné
	 * @static
	 * @param string $idcredit_supplier le numéro de l'avoir fournisseur
	 * @param string $billing_supplier le numéro de la facture fournisseur
	 * @param int $idsupplier le numéro du fournisseur
	 * @return suppliercredit un nouvel avoir fournisseur
	 */
	public static function &createSupplierCredit( $idcredit_supplier, $billing_supplier, $idsupplier ){
	 	
	 	//réserver le numero de l'avoir fournisseur
	 		 	
	 	$query = "
		INSERT INTO `credit_supplier` ( 
			idcredit_supplier, 
			idsupplier,
			billing_supplier,
			iduser 
		) VALUES ( 
			" . DBUtil::quote( $idcredit_supplier ) . ",
			'" .  $idsupplier . "',
			" . DBUtil::quote( $billing_supplier ) . ",
			'" . User::getInstance()->getId() . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer le nouvel avoir fournisseur", E_USER_ERROR );
			die();
			
		}
			
		//créer l'avoir fournisseur
		
		$suppliercredit = new SupplierCredit( $idcredit_supplier, $idsupplier );
	 	
	 	return $suppliercredit;
	 	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * @access public
	 * @static
	 * @param string $credit_supplier
	 * @param int $idsupplier
	 * @return void
	 */
	public static function delete( $credit_supplier, $idsupplier ){
		
		/* vérifier si l'avoir n'est pas utilisé */
		$query = "SELECT count( billing_supplier ) as cpte FROM credit_supplier_regulation WHERE idcredit_supplier = " . DBUtil::quote( $credit_supplier ) . " LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs->fields( "cpte" ) > 0 ){
			
			trigger_error( "Impossible de supprimer un avoir fournisseur déjà utilisé", E_USER_ERROR );
			return;
				
		} 
		
		DBUtil::query( "DELETE FROM credit_supplier WHERE idcredit_supplier = " . DBUtil::quote( $credit_supplier ) . " AND idsupplier = '" .  $idsupplier . "' LIMIT 1" );
		
	}	
	
	//----------------------------------------------------------------------------
	
}
?>