<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des plans comptables
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );

abstract class AccountPlan{
	
	/**
	 * Contructeur
	 */
	function AccountPlan(){}
	
	/**
	 * R?cup?re le num?ro de pays ? partir de l'adresse de livraison
	 * @param int $iddelivery l'identifiant de l'adresse de livraison
	 * @param int $idcustomer le num?ro du client
	 * @return int $idstate l'identifiant du pays
	 */
	static public function getStateId( $iddelivery, $idcustomer ){
		
		if( $iddelivery == 0 )
			$rs =& DBUtil::query( "SELECT idstate FROM buyer WHERE idbuyer = $idcustomer" );
		else
			$rs =& DBUtil::query( "SELECT idstate FROM delivery WHERE iddelivery = $iddelivery" );		
		
		return $rs->fields( "idstate" );
		
	}
	
	/**
	 * Ins?re un enregistrement dans la table du plan comptable
	 * @param int $idcustomer le num?ro de client
	 * @param int $iddelivery l'identifiant de livraison
	 * @param int $idaccounting l'identifiant de cas d'utilisation du plan comptable
	 * @return bool true si tout va bien
	 */
	static public function insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4 = false, $action, $lettering = "" ){
		global $generatedRows;
		
		if( $account_plan_4 === false )
			$account_plan_4 = DBUtil::getDBValue( "forward_carry", "accounting_plan", "idaccounting_plan", $idaccount_plan );
		
		$rs =& DBUtil::query( "INSERT INTO account_plan (
			idaccount_plan,
			idrelation,
			relation_type,
			idaccount_record,
			record_type,
			date,
			amount,
			amount_type,
			designation,
			code_eu,
			code_vat,
			payment,
			payment_date,
			account_plan_1,
			account_plan_2,
			account_plan_3,
			account_plan_4,
			lettering,
			action,
			creation_date,
			username,
			lastupdate
		) VALUES (
			'" . Util::html_escape( $idaccount_plan ) . "',
			'" . Util::html_escape( $idrelation ) . "',
			'" . Util::html_escape( $relation_type ) . "',
			'" . Util::html_escape( $idaccount_record ) . "',
			'" . Util::html_escape( $record_type ) . "',
			'" . Util::html_escape( $date ) . "',
			'" . Util::html_escape( $amount ) . "',
			'" . Util::html_escape( $amount_type ) . "',
			'" . Util::html_escape( $designation ) . "',
			'" . Util::html_escape( $code_eu ) . "',
			'" . Util::html_escape( $code_vat ) . "',
			'" . Util::html_escape( $payment ) . "',
			'" . Util::html_escape( $payment_date ) . "',
			'" . Util::html_escape( $account_plan_1 ) . "',
			'" . Util::html_escape( $account_plan_2 ) . "',
			'" . Util::html_escape( $account_plan_3 ) . "',
			'" . Util::html_escape( $account_plan_4 ) . "',
			'" . Util::html_escape( $lettering ) . "',
			'" . Util::html_escape( $action ) . "',
			'" . date( "Y-m-d H:i:s" ) . "',
			'" . User::getInstance()->get( "login" ) . "',
			'" . date( "Y-m-d H:i:s" ) . "'
		)" );
		
		if( $rs === false )
			trigger_error( "Probl?me lors de l'insertion des donn?es", E_USER_ERROR );
		
		if( isset( $generatedRows ) )
			$generatedRows++;
		
		return true;
		
	}
	
	/**
	 * R?cup?re le code EU d'un client ? partir de son adresse de livraison
	 * @param int $idcustomer le num?ro de client
	 * @param int $iddelivery l'identifiant de livraison
	 * @return string $code_eu le code EU correspondant ? l'adresse de livraison du client
	 */
	static public function getCodeEUFromDelivery( $idcustomer, $iddelivery ){
		
		if( $iddelivery == 0 )
		
			//aide lettrage		
			$rs =& DBUtil::query( "SELECT code_eu FROM state, buyer WHERE state.idstate = buyer.idstate AND buyer.idbuyer = '$idcustomer' " );
		else
			$rs =& DBUtil::query( "SELECT code_eu FROM state, delivery WHERE state.idstate = delivery.idstate AND delivery.iddelivery = $iddelivery" );
		
		if( $rs === false )
			trigger_error( "Impossible de r?cup?rer le code du pays", E_USER_ERROR );
		
		return $rs->fields( "code_eu" );
		
	}
	
	/**
	 * R?cup?re le num?ro de plan comptable ? partir de l'adresse de livraison du client
	 * @param int $idcustomer le num?ro de client
	 * @param int $iddelivery l'identifiant de livraison
	 * @param int $idaccounting l'identifiant de cas d'utilisation du plan comptable
	 * @param int $no_tax_export force le code EU dans certains cas
	 * 		no_tax_export = 0 :	Bas? sur l'adresse de livraison
	 * 		no_tax_export = 1 :	Forc? en HT
	 * 		no_tax_export = 2 :	Forc? en France
	 * @return int $idaccounting_plan le num?ro de plan comptable correspondant
	 */
	static public function getAccountNumberFromDelivery( $idcustomer, $iddelivery, $idaccounting, $no_tax_export = 0, $idtva=0 ){
		
		if( $no_tax_export == 1 )
			$codeEU = "EXPORT";
		elseif( $no_tax_export == 2 )
			$codeEU = "NATIONAL";
		else
			$codeEU = AccountPlan::getCodeEUFromDelivery( $idcustomer, $iddelivery );
		
		$rs =& DBUtil::query( "SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_use = $idaccounting AND idtva = '$idtva' AND ( accounting_plan_eu = '$codeEU' OR accounting_plan_eu = '' )" );
		
		if( $rs === false )
			trigger_error( "Impossible de r?cup?rer le num?ro de compte", E_USER_ERROR );
		
		return $rs->fields( "idaccounting_plan" );
		
	}
	
	/**
	 * R?cup?re le num?ro de plan comptable ? partir du code EU
	 * @param int $code_eu le code EU
	 * @param int $idaccounting l'identifiant de cas d'utilisation du plan comptable
	 * @return int $idaccounting_plan le num?ro de plan comptable correspondant
	 */
	static public function getAccountNumberFromCodeEU( $code_eu, $idaccounting ){
		
		$rs =& DBUtil::query( "SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_use = $idaccounting AND ( accounting_plan_eu = '$code_eu' OR accounting_plan_eu = '' )" );
		
		if( $rs === false )
			trigger_error( "Impossible de r?cup?rer le num?ro de compte", E_USER_ERROR );
		
		return $rs->fields( "idaccounting_plan" );
		
	}
	
	/**
	 * R?cup?re le nom du client
	 * Si le client a un nom (soci?t?), on retourne le nom, sinon (particulier), on retourne le nom du contact
	 * @param int $idcustomer le num?ro de client
	 * @return string $customerName le nom du client
	 */
	static public function getCustomerName( $idcustomer ){
		
		//aide lettrage
		$rs =& DBUtil::query( "SELECT company FROM buyer WHERE idbuyer = '$idcustomer' LIMIT 1" );
		
		$customerName = $rs->fields( "company" );
		
		if( $customerName == "" ){
			
			//aide lettrage
			$rs =& DBUtil::query( "SELECT firstname, lastname FROM contact WHERE idbuyer = '$idcustomer' " );
			$customerName = $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
			
		}
		
		return $customerName;
		
	}
	
	/**
	 * R?cup?re la date de la derni?re g?n?ration du plan comptable pour une cat?gorie
	 * @param string $action la cat?gorie concern?e
	 * @return string la date de derni?re insertion
	 */
	static public function getLastInsertion( $action ){
		
		$rs =& DBUtil::query( "SELECT creation_date FROM account_plan WHERE action = '$action' ORDER BY date DESC LIMIT 1" );
		
		if( $rs === false )
			trigger_error( "Erreur lors de la r?cup?ration de la derni?re date de g?n?ration", E_USER_ERROR );
		
		if( !$rs->RecordCount() )
			return "0000-00-00";
		
		$date = $rs->fields( "date" );
		$date = mktime( 0, 0, 0, substr( $date, 5, 2 ), substr( $date, 8, 2 ) + 1, substr( $date, 0, 4 ) );
		$date = date( "Y-m-d", $date );
		
		return $date;
		
	}
	
	/**
	 * R?cup?re le dernier compte parent d'un num?ro de compte
	 * @param int $idaccount_plan le num?ro de compte
	 * @return int le dernier compte parent
	 */
	static public function getLastParent( $idaccount_plan ){
		
		$rs =& DBUtil::query( "SELECT accounting_plan_parent FROM accounting_plan WHERE idaccounting_plan = '$idaccount_plan'" );
		
		if( $rs->fields( "accounting_plan_parent" ) == 0 )
			return $idaccount_plan;
		else
			return AccountPlan::getLastParent( $rs->fields( "accounting_plan_parent" ) );
		
	}
	
	/**
	 * R?cup?re l'ensemble des comptes enfants d'un compte
	 * @param array $accounts l'ensemble des comptes dont il faut trouver les enfants
	 * @return array le tableau avec l'ensemble des enfants et le compte de base
	 */
	static public function &getAccountChildrens( $accounts ){
		
		$i = 0;
		while( $i < count( $accounts ) ){
			
			$rs =& DBUtil::query( "SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_parent = '" . $accounts[ $i ] . "'" );
			
			while( !$rs->EOF() ){
				
				$accounts[] = $rs->fields( "idaccounting_plan" );
				
				$rs->MoveNext();
				
			}
			
			$i++;
			
		}
		
		return $accounts;
		
	}
	
	/**
	 * R?cup?re l'ensemble des num?ros de pi?ces d?j? rentr?s
	 * @param string $action le type d'action pass?e
	 * @return string la cha?ne de caract?re MySQL
	 */
	static public function &getNotIn( $action ){
		
		$rs =& DBUtil::query( "SELECT idaccount_record FROM account_plan WHERE action = '$action'" );
		
		if( $rs === false )
			trigger_error( "Impossible de r?cup?rer la liste des pi?ces d?j? tra?t?es ($action)", E_USER_ERROR );
		
		$notIn = array();
		while( !$rs->EOF ){
			
			if( !in_array( $rs->fields( "idaccount_record" ), $notIn ) )
				$notIn[] = $rs->fields( "idaccount_record" );
			
			$rs->MoveNext();
			
		}
		
		$notIn = "'" . implode( "', '", $notIn ) . "'";
		
		return $notIn;
		
	}
	
	/**
	 * R?cup?re l'ensemble des num?ros de pi?ces d?j? rentr?s dans le cas d'une cl? primaire double
	 * @param string $action le type d'action pass?e
	 * @return string la cha?ne de caract?re MySQL
	 */
	static public function &getNotInCouple( $action ){
		
		$rs =& DBUtil::query( "SELECT idaccount_record, idrelation FROM account_plan WHERE action = '$action' GROUP BY idaccount_record, idrelation" );
		
		if( $rs === false )
			trigger_error( "Impossible de r?cup?rer la liste des pi?ces d?j? tra?t?es ($action)", E_USER_ERROR );
		
		$notIn = "( '', '' )";
		
		while( !$rs->EOF ){
			
			if( !strstr( $notIn, "( '" . $rs->fields( "idaccount_record" ) . "', '" . $rs->fields( "idrelation" ) . "' )" ) )
				$notIn .= ", ( '" . $rs->fields( "idaccount_record" ) . "', '" . $rs->fields( "idrelation" ) . "' )";
			
			$rs->MoveNext();
			
		}
		
		return $notIn;
		
	}
	
	/**
	 * R?cup?re le prochain lettr?che ? partir du lettrage pr?c?dent
	 * @param string $lettering le pr?c?dent lettrage
	 * Si $lettering est false, on r?cup?re dans la base la derni?re valeur du lettrage
	 * @return string le nouveau lettrage
	 * 
	 * @todo la fonction, ainsi que la requ?te MySQL sont s?rement loin d'?tre optimis?s donc ? voir
	 */
	static public function getNextLettering( $idrelation, $date, $relation_type = "buyer", $lettering = false ){
		
		$alpha = array( "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" );
		
		if( $lettering === false ){
			
			//On r?cup?re les dates de d?but et de fin de l'exercice comptable auquel correspond le lettrage
			
			$rs =& DBUtil::query( "
				SELECT start_date
				FROM fiscal_years
				WHERE start_date <= '$date'
				ORDER BY start_date DESC
				LIMIT 1
			" );
			
			if( $rs === false )
				trigger_error( "Impossible de r?cup?rer la date de d?but d'exercice comptable", E_USER_ERROR );
			
			$start = $rs->RecordCount() ? $rs->fields( "start_date" ) : "0000-00-00";
			
			$rs =& DBUtil::query( "
				SELECT start_date
				FROM fiscal_years
				WHERE start_date > '$date'
				ORDER BY start_date ASC
				LIMIT 1
			" );
			
			if( $rs === false )
				trigger_error( "Impossible de r?cup?rer la date de fin d'exercice comptable", E_USER_ERROR );
			
			$end = $rs->RecordCount() ? $rs->fields( "start_date" ) : date( "Y-m-d" );
			
			$rs =& DBUtil::query( "
				SELECT lettering
				FROM account_plan
				WHERE idrelation = '$idrelation'
					AND relation_type = '$relation_type'
					AND date >= '$start'
					AND date < '$end'
				ORDER BY LENGTH( lettering ) DESC, lettering DESC
				LIMIT 1
			" );
			
			if( $rs === false )
				trigger_error( "Impossible de r?cup?rer la valeur du dernier lettr?che", E_USER_ERROR );
			
			if( !$rs->RecordCount() )
				$lettering = "";
			else
				$lettering = $rs->fields( "lettering" );
			
		}
		
		if( $lettering == "" )
			return $alpha[ 0 ];
		
		$newstring = "";
		$increment = true;
		$len = strlen( $lettering ) - 1;
		
		while( $len >=  0 ){
			
			$letter = substr( $lettering, $len, 1 );
			$index = array_search( $letter, $alpha ); //Indice de la lettre
			
			if( $increment )
				$newstring = $alpha[ ( $index + 1 ) % count( $alpha ) ] . $newstring;
			else
				$newstring = $letter . $newstring;
			
			if( $increment && ( $index + 1 ) >= count( $alpha ) )
				 $increment = true;
			else
				$increment = false;
			
			$len--;
			
		}
		
		return $newstring;
		
	}
	
	/**
	 * Affiche la liste des exercices comptables
	 * 
	 * @todo la fonction, ainsi que la requ?te MySQL sont s?rement loin d'?tre optimis?s donc ? voir
	 */
	static public function displayFiscalYears(){
		
		global $GLOBAL_START_PATH;
		
		include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
		
		$rs =& DBUtil::query( "SELECT * FROM fiscal_years ORDER BY start_date ASC" );
		
		if( $rs === false )
			trigger_error( "Impossible de r?cup?rer les dates des exercices comptables" );
		
		while( !$rs->EOF ){
			
			$name	= $rs->fields( "name" );
			$start	= Util::dateFormatEu( $rs->fields( "start_date" ) );
			$closed	= $rs->fields( "closed" );
			$id		= $rs->fields( "idfiscal_year" );
			
			$rs->MoveNext();
			
			?>
			<div style="margin-top:5px;"><strong><?php echo $name ?></strong></div>
			<div style="margin-left:10px;">
			<?php
				
				if( $rs->EOF )
					echo "En cours";
				else
					echo "$start au " . Util::dateFormatEu( DateUtil::getPreviousDay( $rs->fields( "start_date" ) ) );
				
			?></div><?php
			
		}
		
	}
	
}

?>