<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object création devis
 */
 
include_once( dirname( __FILE__ ) . "/../config/init.php" );


abstract class BasketFactory{
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un devis à partir du panier du front office
	 * @static
	 * @access public
	 * @return Estimate le nouveau devis
	 */
	public static function createEstimateFromBasket(){
		
		include_once( dirname( __FILE__ ) . "/Basket.php" );
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
		
		$idbuyer 	= Basket::getInstance()->getCustomer()->getId();
		$idcontact 	= Basket::getInstance()->get( "idcontact" );
		
		$total_amount_wo_device = round( Basket::getInstance()->getTotalATI() - Basket::getInstance()->get( "billing_amount" ) * Basket::getInstance()->get( "device" ), 2 ); //@todo : deprecated
		
		$estimate = self::createEstimate( $idbuyer, $idcontact );
		
		$estimate->setSalesman( Basket::getInstance()->getSalesman() );

		$estimate->setValue( "idcommercant" , 			User::getInstance()->getId() ); //@todo : deprecated
		$estimate->setValue( "idcurrency" , 			Basket::getInstance()->get( "idcurrency" ) );
		$estimate->setValue( "total_weight", 			Basket::getInstance()->getWeight() ); //@todo : total_weight deprecated
		$estimate->setValue( "idpayment", 				DBUtil::getParameter( "default_idpayment" ) );
		$estimate->setValue( "status", 					Estimate::$STATUS_TODO );
		$estimate->setValue( "comment", 				Basket::getInstance()->get( "comment" ) );
		$estimate->setValue( "total_amount_wo_device", 	$total_amount_wo_device ); //@todo : deprecated
		$estimate->setValue( "status_report", 			1 );
		$estimate->setValue( "total_estimate", 			round( Basket::getInstance()->getTotalATI() - Basket::getInstance()->get( "billing_amount" ), 2 ) ); //@todo : deprecated
		$estimate->setValue( "exportable", 				1 );
		$estimate->setValue( "device" , 				Basket::getInstance()->get( "device" ) );
		$estimate->setValue( "deliv_payment" , 			Basket::getInstance()->get( "deliv_payment" ) );
		$estimate->setValue( "down_payment_type" , 		Basket::getInstance()->get( "down_payment_type" ) );
		$estimate->setValue( "down_payment_value" , 	Basket::getInstance()->get( "down_payment_value" ) );
		$estimate->setValue( "billing_rate" , 			Basket::getInstance()->get( "billing_rate" ) );
		$estimate->setValue( "billing_amount" , 		Basket::getInstance()->get( "billing_amount" ) );
		$estimate->setValue( "total_discount" , 		Basket::getInstance()->get( "total_discount" ) );
		$estimate->setValue( "total_discount_amount", 	Basket::getInstance()->get( "total_discount_amount" ) );
		$estimate->setValue( "idpayment_delay", 		DBUtil::getParameter( "default_idpayment_delay" ) );
		$estimate->setValue( "factor" , 				Basket::getInstance()->get( "factor" ) );
		$estimate->setValue( "type_estimate" , 			DBUtil::getParameterAdmin( "default_type_estimate" ) );
		$estimate->setValue( "total_charge_auto" , 		Basket::getInstance()->get( "total_charge_auto" ) );
		$estimate->setValue( "idorder_rate_buyer" , 	Basket::getInstance()->get( "idorder_rate_buyer" ) );

		if( Basket::getInstance()->get( "iddelivery" ) )
				$estimate->setUseForwardingAddress( new ForwardingAddress( Basket::getInstance()->get( "iddelivery" ) ) );
		else 	$estimate->setUseForwardingAddress( Basket::getInstance()->getCustomer()->getAddress() );
		
		if( Basket::getInstance()->get( "idbilling_adress" ) )
				$estimate->setUseInvoiceAddress( new InvoiceAddress( Basket::getInstance()->get( "idbilling_adress" ) ) );
		else 	$estimate->setUseInvoiceAddress( Basket::getInstance()->getCustomer()->getAddress() );
		
		//ajout des articles
		
		$it = Basket::getInstance()->getItems()->iterator();
		
		while( $it->hasNext() )
			$estimate->addBasketItem( $it->next() );
			
		//pas de frais de port si total à 0 ( pour la forme )
		
		if( $estimate->getValue( "total_amount_ht" ) == 0.0 ){
			
			$estimate->setValue( "charge_vat_rate", Basket::getInstance()->getVATRate() );
			$estimate->setValue( "charge_vat", 		0.0 );
			$estimate->setValue( "total_charge", 	0.0 );
			$estimate->setValue( "total_charge_ht", 0.0 );
			
		}
		
		//assurance crédit
		
		$estimate->insure();
		
		//sauvegarde
		
		$estimate->save();
	
		//destruction du panier
		
		Basket::getInstance()->destroy();
		
		return $estimate;
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une commande à partir du panier du front office
	 * @static
	 * @access public
	 * @return Order la nouvelle commande
	 */
	public static function createOrderFromBasket(){
	
		include_once( dirname( __FILE__ ) . "/Basket.php" );
		
		$idbuyer 	= Basket::getInstance()->getCustomer()->getId();
		$idcontact 	= Basket::getInstance()->get( "idcontact" );
		
		$total_amount_wo_device = round( Basket::getInstance()->getTotalATI() - Basket::getInstance()->get( "billing_amount" ) * Basket::getInstance()->get( "device" ), 2 ); //@todo : deprecated
		
		$order = self::createOrder( $idbuyer, $idcontact );
		
		$order->setSalesman( Basket::getInstance()->getSalesman() );

		$order->setValue( "idcommercant" , 			User::getInstance()->getId() ); //@todo : deprecated
		$order->setValue( "idcurrency" , 			Basket::getInstance()->get( "idcurrency" ) );
		$order->setValue( "total_weight", 			Basket::getInstance()->getWeight() ); //@todo : total_weight deprecated
		$order->setValue( "idpayment", 				DBUtil::getParameter( "default_idpayment" ) );
		$order->setValue( "status", 				Order::$STATUS_TODO );
		$order->setValue( "comment", 				Basket::getInstance()->get( "comment" ) );
		$order->setValue( "total_amount_wo_device", $total_amount_wo_device ); //@todo : deprecated
		$order->setValue( "status_report", 			1 );
		$order->setValue( "exportable", 			1 );
		$order->setValue( "device" , 				Basket::getInstance()->get( "device" ) );
		$order->setValue( "deliv_payment" , 		Basket::getInstance()->get( "deliv_payment" ) );
		$order->setValue( "down_payment_type" , 	Basket::getInstance()->get( "down_payment_type" ) );
		$order->setValue( "down_payment_value" , 	Basket::getInstance()->get( "down_payment_value" ) );
		$order->setValue( "billing_rate" , 			Basket::getInstance()->get( "billing_rate" ) );
		$order->setValue( "billing_amount" , 		Basket::getInstance()->get( "billing_amount" ) );
		$order->setValue( "total_discount" , 		Basket::getInstance()->get( "total_discount" ) );
		$order->setValue( "total_discount_amount", 	Basket::getInstance()->get( "total_discount_amount" ) );
		$order->setValue( "idpayment_delay", 		DBUtil::getParameter( "default_idpayment_delay" ) );
		$order->setValue( "factor" , 				Basket::getInstance()->get( "factor" ) );
		$order->setValue( "total_charge_auto" , 	Basket::getInstance()->get( "total_charge_auto" ) );
		$order->setValue( "idorder_rate_buyer" , 	Basket::getInstance()->get( "idorder_rate_buyer" ) );

		if( Basket::getInstance()->get( "iddelivery" ) )
				$order->setUseForwardingAddress( new ForwardingAddress( Basket::getInstance()->get( "iddelivery" ) ) );
		else 	$order->setUseForwardingAddress( Basket::getInstance()->getCustomer()->getAddress() );
		
		if( Basket::getInstance()->get( "idbilling_adress" ) )
				$order->setUseInvoiceAddress( new InvoiceAddress( Basket::getInstance()->get( "idbilling_adress" ) ) );
		else 	$order->setUseInvoiceAddress( Basket::getInstance()->getCustomer()->getAddress() );
		
		//ajout des articles
		
		$it =& Basket::getInstance()->getItems()->iterator();
		
		while( $it->hasNext() )
			$order->addBasketItem( $it->next() );
			
		//pas de frais de port si total à 0 ( pour la forme )
		
		if( $order->getValue( "total_amount_ht" ) == 0.0 ){
			
			$order->setValue( "charge_vat_rate", 	Basket::getInstance()->getVATRate() );
			$order->setValue( "charge_vat", 		0.0 );
			$order->setValue( "total_charge", 		0.0 );
			$order->setValue( "total_charge_ht", 	0.0 );
			
		}
		
		//assurance crédit
		
		$order->insure();
		
		//sauvegarde
		
		$order->save();
	
		//destruction du panier
		
		Basket::getInstance()->destroy();
		
		return $order;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Convertit un devis en commande
	 * @static
	 * @access public
	 * @param Estimate $estimate le devis à commander
	 * @param string $status le status de la commande
	 * @return Order la commande créée
	 */
	public static function createOrderFromEstimate( Estimate &$estimate, $status ){
		
		global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
		include_once( dirname( __FILE__ ) . "/Order.php" );
		include_once( dirname( __FILE__ ) . "/Session.php" );
		
		$rs = DBUtil::query( "SELECT idorder FROM `order` WHERE idestimate = '" . $estimate->getId() . "' AND `status` NOT LIKE '" . Order::$STATUS_CANCELLED . "' LIMIT 1" );
		
		if( $rs->RecordCount() )
			return new Order( $rs->fields( "idorder" ) );
	
		$idbuyer 	= $estimate->getValue( "idbuyer" );
		$idcontact 	= $estimate->getValue( "idcontact" );
		
		$order = self::createOrder( $idbuyer, $idcontact );
		
		$order->setSalesman( $estimate->getSalesman() );
		
		//lignes articles
		
		$iterator = $estimate->getItems()->iterator();
		
		while( $iterator->hasNext() ){
			
			$estimateItem =& $iterator->next();
			
			$order->addEstimateItem( $estimateItem );
		
			//destockage
			//@todo stock dispo pas vérifié
			
			if( $status == Order::$STATUS_ORDERED ){
				
				$query = "
				UPDATE detail
				SET stock_level = GREATEST( 0, stock_level - " . $estimateItem->get( "quantity" ) . " )
				WHERE idarticle = '" . $estimateItem->get( "idarticle" ) . "'
				AND stock_level <> -1
				LIMIT 1";
				
				DBUtil::query( $query );

			}
			
			//enregistrement dans les stats
			
			$idarticle = $estimateItem->get( "idarticle" );
			$quantity = $estimateItem->get( "quantity" );
			$reference = $estimateItem->get( "reference" );
			
			//@todo : idcategory ne devrait pas être récupéré dans la BDD car la référence peut avoir été supprimée
			//mais si on stock le champ idcategory et que la catégorie a été supprimée c'est le même topo
			// => Il y a une erreur de conception quelque part
				
			$rs =& DBUtil::query( "SELECT p.idcategory FROM product p, detail d WHERE d.idproduct = p.idproduct AND d.idarticle = '$idarticle'" );
			
			$idcategory = $rs->fields( "idcategory" );
			
		}
     	
		//fichiers joints
      	
        self::estimateAttachmentToOrderAttachment( $estimate, $order );
        
        //remise commerciale
        
        DEPRECATEDBUYER::applyCommercialDiscount( $estimate->getValue( "idbuyer" ), $estimate->getValue( "total_discount_amount" ) );

		//assurance crédit
		
		$order->insure( $estimate->getValue( "insurance" ) == 1 );
		
        //propriétés de la commande
            
		$order->setValue( "idestimate", 			$estimate->getId() );
		$order->setValue( "estimate_date", 			$estimate->getValue( "DateHeure" ) );
		$order->setValue( "idcurrency", 			$estimate->getValue( "idcurrency" ) );
		$order->setValue( "iddelivery", 			$estimate->getValue( "iddelivery" ) );
		$order->setValue( "idbilling_adress", 		$estimate->getValue( "idbilling_adress" ) );
		$order->setValue( "total_amount", 			$estimate->getValue( "total_amount" ) );
		$order->setValue( "total_amount_ht", 		$estimate->getValue( "total_amount_ht" ) );
		$order->setValue( "total_charge", 			$estimate->getValue( "total_charge" ) );
		$order->setValue( "total_charge_ht", 		$estimate->getValue( "total_charge_ht" ) );
		$order->setValue( "charge_free", 			$estimate->getValue( "charge_free" ) );
		$order->setValue( "total_weight", 			$estimate->getValue( "total_weight" ) );
		$order->setValue( "idpayment", 				$estimate->getValue( "idpayment" ) );
		$order->setValue( "idpayment_delay", 		$estimate->getValue( "idpayment_delay" ) );
		$order->setValue( "status", 				$status );
		$order->setValue( "comment", 				$estimate->getValue( "comment" ) );
		$order->setValue( "commercial_comment",		$estimate->getValue( "commercial_comment" ) );
		$order->setValue( "total_amount_wo_device", $estimate->getValue( "total_amount_wo_device" ) );
		$order->setValue( "status_report", 			0 );
		$order->setValue( "idaffiliate", 			Session::getInstance()->GetIdaffiliate() );
		$order->setValue( "exportable", 			$estimate->getValue( "exportable" ) );
		$order->setValue( "device", 				$estimate->getValue( "device" ) );
		$order->setValue( "no_tax_export", 			$estimate->getValue( "no_tax_export" ) );
		$order->setValue( "charge_vat_rate", 		$estimate->getValue( "charge_vat_rate" ) );
		$order->setValue( "charge_vat", 			$estimate->getValue( "charge_vat" ) );
		$order->setValue( "billing_amount", 		$estimate->getValue( "billing_amount" ) );
		$order->setValue( "billing_rate", 			$estimate->getValue( "billing_rate" ) );
		$order->setValue( "charge_supplier", 		$estimate->getValue( "charge_supplier" ) );
		$order->setValue( "charge_supplier_rate", 	$estimate->getValue( "charge_supplier_rate" ) );
		$order->setValue( "total_discount", 		$estimate->getValue( "total_discount" ) );
		$order->setValue( "total_discount_amount", 	$estimate->getValue( "total_discount_amount" ) );
		$order->setValue( "charge_supplier_auto", 	$estimate->getValue( "charge_supplier_auto" ) );
		$order->setValue( "conf_order_date", 		date( "Y-m-d H:i:s" ) );
		//On met total_charge_auto à 0 car en cas de changement de port par défaut les calculs sont faux
		$order->setValue( "total_charge_auto", 		0 );
		$order->setValue( "n_order", 				$estimate->getValue( "n_order" ) );
		$order->setValue( "remote_creation_date", 	$estimate->getValue( "remote_creation_date" ) );
		$order->setValue( "pro_forma", 				0 );
		$order->setValue( "global_delay", 			$estimate->getValue( "global_delay" ) );
		$order->setValue( "idorder_rate_buyer", 	$estimate->getValue( "idorder_rate_buyer" ) );
        $order->setValue( "balance_date", 			$estimate->getValue( "balance_date" ) );
        $order->setValue( "paid",		 			$estimate->getValue( "paid" ) );
		$order->setValue( "rebate_type", 			$estimate->getValue( "rebate_type" ) ); 
        $order->setValue( "rebate_value", 			$estimate->getValue( "rebate_value" ) );
        $order->setValue( "credit_amount", 			$estimate->getValue( "credit_amount" ) );
        
        // Acompte
		$order->setValue( "down_payment_type",		$estimate->getValue( "down_payment_type" ) );
		$order->setValue( "down_payment_value", 	$estimate->getValue( "down_payment_value" ) );
		$order->setValue( "down_payment_idpayment", $estimate->getValue( "down_payment_idpayment" ) );
		$order->setValue( "down_payment_comment", 	$estimate->getValue( "down_payment_comment" ) );
		$order->setValue( "down_payment_date", 		$estimate->getValue( "down_payment_date" ) );
		$order->setValue( "down_payment_idregulation",$estimate->getValue( "down_payment_idregulation" ) );
		$order->setValue( "down_payment_idbank", 	$estimate->getValue( "down_payment_idbank" ) );
		$order->setValue( "down_payment_piece", 	$estimate->getValue( "down_payment_piece" ) );
		
		// Paiement comptant
        $order->setValue( "balance_amount", 		$estimate->getValue( "balance_amount" ) );
        $order->setValue( "cash_payment", 			$estimate->getValue( "cash_payment" ) );
        $order->setValue( "payment_comment", 		$estimate->getValue( "payment_comment" ) ); 
        $order->setValue( "payment_idpayment", 		$estimate->getValue( "payment_idpayment" ) ); 
        $order->setValue( "cash_payment_idregulation",$estimate->getValue( "cash_payment_idregulation" ) );
        $order->setValue( "cash_payment_idbank", 	$estimate->getValue( "cash_payment_idbank" ) );
        $order->setValue( "cash_payment_piece", 	$estimate->getValue( "cash_payment_piece" ) );
        
        //Assurance crédit
        //Ca s'appelle factor mais c'est l'assurance crédit. Et oui, c'est beau !
        //On ne copie pas depuis le devis étant donné que c'est info est rarement renseignée dans le devis et est parfois fausse
        //Elle est par exemple fausse quand le client n'est passé à l'assurance qu'une fois le devis créé
        
		$total_amount		= $estimate->getValue( "total_amount" );
		$buyerFactor		= $estimate->getCustomer()->get( "factor" );
		$buyerFactorStatus	= $estimate->getCustomer()->get( "factor_status" );
		$factor_minbuying	= DBUtil::getParameterAdmin( "factor_minbuying" );
		$factor_maxbuying	= DBUtil::getParameterAdmin( "factor_maxbuying" );
		
		$allowFactor = true;
		$allowFactor &= !empty( $buyerFactor ); 												//à condition qu'il y ait un factor
		$allowFactor &= ( $buyerFactorStatus == "Accepted" ); 									//à condition que le factor soit accepté
		$allowFactor &= ( empty( $factor_minbuying ) || $total_amount >= $factor_minbuying ); 	//à condition que le montant soit au dessus du montant minimum autorisé si ce dernier existe
		$allowFactor &= ( empty( $factor_maxbuying ) || $total_amount <= $factor_maxbuying ); 	//à condition que le montant soit en dessous du montant maximum autorisé si ce dernier existe
		
        if( $allowFactor )
        		$order->setValue( "factor", $estimate->getCustomer()->get( "factor" ) );
        else 	$order->setValue( "factor", 0 );

        //date de paiement
        
        $delaipaye = $estimate->getCustomer()->get( "deliv_payment" ) * 3600 * 24; // delai en secondes 

		if( $estimate->getCustomer()->get( "cond_payment" ) == '1' ) //@todo : comprends pas... => ne pas se baser sur la valeur d'une clé primaire pour faire un algo!!! 
				$echeancepaye = $delaipaye + mktime(0,0,0,date('m')+1,1,date('Y')) - 2 ; // delai + debut du mois suivant à 0h - 2 secondes
		else 	$echeancepaye  = time() + $delaipaye ; //date de paiement à le seconde près!
		
		$ADatePaye =getdate($echeancepaye);	
		$DatePaye = sprintf("%04d-%02d-%02d",$ADatePaye['year'], $ADatePaye['mon'], $ADatePaye['mday']) ;
		
        $order->setValue( "deliv_payment", $DatePaye );
        
        $order->insure();
        //sauvegarde
		
		$order->save();
		
		//frais de port fournisseur
		
		self::estimateChargesToOrderCharges( $estimate->getId(), $order->getId() );

		//On met à jour le devis
		
		$estimate->setValue( "status_report", 	1 ) ;
		$estimate->setValue( "status", 			Estimate::$STATUS_ORDERED );
		
		$estimate->save();

	 	return $order;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé les commandes fournisseur pour une commande donnée
	 * @static
	 * @access public
	 * @param Order $order la commande
	 * @return void
	 */
	public static function createSupplierOrders( Order &$order ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		if( DBUtil::query( "SELECT idorder_supplier FROM order_supplier WHERE idorder = '" . $order->getId() . "' LIMIT 1" )->RecordCount() )
			return;
		
	    $useInternalSupplier 	= false;
	    $useExternalSuppliers 	= false;
	    
	    $it = $order->getItems()->iterator();
	    while( $it->hasNext() ){
	    
	    	$orderItem =& $it->next();
	    	
	    	$useInternalSupplier |= $orderItem->getValue( "quantity" ) > $orderItem->getValue( "external_quantity" );
	    	$useExternalSuppliers |= $orderItem->getValue( "external_quantity" ) > 0;
	    	
	    }
	    
	    //créer la commande fournisseur interne
	    
	    if( $useInternalSupplier )
			self::createInternalSupplierOrder( $order );
		
		//créer les commandes fournisseur externes
			
		if( $useExternalSuppliers )
			self::createExternalSupplierOrders( $order );
		
		//créer un bon de livraison pour chaque petite commande fournisseur générée
		
		$query = "
		SELECT idorder_supplier
		FROM order_supplier
		WHERE idorder = '" . $order->getId() . "'";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
			
			$idorder_supplier = $rs->fields( "idorder_supplier" );
			
			self::createDeliveryNoteFromSupplierOrder( new SupplierOrder( $idorder_supplier ) );
		
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créer la commande fournisseur interne pour une commande donnée
	 * @access private
	 * @static
	 * @param Order $order la commande
	 * @return void
	 */
	private static function createInternalSupplierOrder( Order &$order ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		$internalSupplierId = DBUtil::getParameterAdmin( "internal_supplier" );
		
		if( !$internalSupplierId ){
			
			trigger_error( "Impossible de trouver le fournisseur interne", E_USER_ERROR );
			exit();
			
		}
		
		$supplierOrder = self::createSupplierOrderFromOrder( $order, $internalSupplierId );
			
		$total_weight = 0.0;
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
				
			//ne traiter que le stock interne
			
			if( $orderItem->getValue( "quantity") > $orderItem->getValue( "external_quantity") ){
				
				$internal_quantity 	= $orderItem->getValue( "quantity") - $orderItem->getValue( "external_quantity");
				$total_weight 		+= $internal_quantity * $orderItem->getValue( "weight");
	
				$supplierOrder->addOrderItem( $orderItem );
				
			}

		}

		//propriétés de la commande fournisseur
		
		$supplierOrder->setValue( "iddelivery", 			$order->getValue( "iddelivery" ) );
		$supplierOrder->setValue( "no_tax_export", 			$order->getValue( "no_tax_export" ) );
		$supplierOrder->setValue( "charge", 				$order->getValue( "charge_supplier" ) );
		$supplierOrder->setValue( "charge_rate", 			$order->getValue( "charge_supplier_rate" ) );
		$supplierOrder->setValue( "total_charge_ht", 		$order->getValue( "total_charge_ht" ) );
		$supplierOrder->setValue( "total_charge", 			$order->getValue( "total_charge" ) );
		$supplierOrder->setValue( "charge_vat", 			$order->getValue( "charge_vat" ) );
		$supplierOrder->setValue( "charge_vat_rate", 		$order->getValue( "charge_vat_rate" ) );
		$supplierOrder->setValue( "total_weight", 			$total_weight );
		$supplierOrder->setValue( "stock", 					SupplierOrder::$STOCK_TYPE_INTERNAL );
		$supplierOrder->setValue( "idpayment", 				DBUtil::getDBValue( "idpaiement", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) ) ); 		//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->setValue( "idpayment_delay", 		DBUtil::getDBValue( "pay_delay", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) ) ); 	//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->setValue( "charge_carrier", 		0.0 );
		$supplierOrder->setValue( "charge_carrier_cost", 	0.0 );
		$supplierOrder->setValue( "charge_carrier_sell", 	0.0 );
		$supplierOrder->setValue( "delivery_mail_auto",		1 );
		
		//sauvegarde
		
		$supplierOrder->save();

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé la commande fournisseur externe pour une commande donnée et un fournisseur donné
	 * @param Order la commande
	 * @param int $idsupplier l'identifiant du fournisseur
	 * @static
	 * @access private
	 * @return void
	 */
	 private static function createExternalSupplierOrder( Order &$order, $idsupplier ){
	 	
	 	include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
	 	
		$supplierOrder = self::createSupplierOrderFromOrder( $order, $idsupplier );
				
		$total_weight = 0.0;
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
				
			//ne traiter que le stock externe
			
			if( $orderItem->getValue( "idsupplier" ) == $idsupplier && $orderItem->getValue( "external_quantity") ){
				
				$total_weight += $orderItem->getValue( "external_quantity" ) * $orderItem->getValue( "weight" );

				$supplierOrder->addOrderItem( $orderItem );
				
			}

		}
		
		//propriétés de la commande fournisseur
		
		$supplierOrder->setValue( "iddelivery", 		$order->getValue( "iddelivery" ) );
		$supplierOrder->setValue( "total_weight", 		$total_weight );
		$supplierOrder->setValue( "stock", 				SupplierOrder::$STOCK_TYPE_EXTERNAL );
		$supplierOrder->setValue( "idpayment", 			DBUtil::getDBValue( "idpaiement", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) ) ); 		//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->setValue( "idpayment_delay", 	DBUtil::getDBValue( "pay_delay", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) ) ); 	//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->setValue( "billing_rate", 		DBUtil::getDBValue( "billing_rate", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) ) );
		$supplierOrder->setValue( "billing_amount", 	round( $supplierOrder->getTotalET() * $supplierOrder->getValue( "billing_rate" ) / 100.0, 2 ) );
		
		//livraison
		
		$default_deliv = DBUtil::getDBValue( "default_deliv", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) );
		$iddelivery = $default_deliv ? $default_deliv : $order->getValue( "iddelivery" );
		
		$supplierOrder->setValue( "iddelivery", 		$iddelivery );
		
		//frais de port
		
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) );
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $supplierOrder->getValue( "idsupplier" ) );
		$bvat	 = DBUtil::getDBValue( "bvat", "state", "idstate", $idstate );
		$rs 	 =& DBUtil::query( "SELECT * FROM `order_charges` WHERE idorder = '" . $order->getId() . "' AND idsupplier = '" . $supplierOrder->getValue( "idsupplier" ) . "' LIMIT 1" );

		//champs de compta navision
		
		$charge_carrier 		= $transit ? $rs->fields( "charges" ) + $rs->fields( "internal_supplier_charges" ) : 0.0;
		$charge_carrier_cost 	= $transit ? $rs->fields( "charges" ) : 0.0;
		$charge_carrier_sell 	= $transit ? $rs->fields("internal_supplier_charges") : 0.0;

		$total_charge_ht = $rs->fields( "charges" );
		$total_charge 	 = $bvat ? $rs->fields( "charges" ) * ( 1 + $order->getValue( "charge_vat_rate" ) / 100.0 ) : $rs->fields( "charges" );
		$charge 		 = $bvat ? $rs->fields( "charges" ) * ( 1 + $order->getValue( "charge_vat_rate" ) / 100.0 ) : $rs->fields( "charges" );
		$charge_rate 	 = $charge * 100.0 / $order->getNetBill();

		$supplierOrder->setValue( "charge_vat_rate", 		$order->getValue( "charge_vat_rate" ) );
		$supplierOrder->setValue( "charge_vat", 			$order->getValue( "charge_vat" ) );
		$supplierOrder->setValue( "charge", 				$charge );
		$supplierOrder->setValue( "charge_rate", 			$charge_rate );
		$supplierOrder->setValue( "charge_carrier", 		$charge_carrier );
		$supplierOrder->setValue( "total_charge_ht", 		$total_charge_ht );
		$supplierOrder->setValue( "total_charge", 			$total_charge );
		$supplierOrder->setValue( "charge_carrier_cost", 	$charge_carrier_cost );
		$supplierOrder->setValue( "charge_carrier_sell", 	$charge_carrier_sell );
		
		//sauvegarde

		$supplierOrder->save();

	 }
	 
	//----------------------------------------------------------------------------
	/**
	 * Créé les commandes fournisseur externes pour une commande donnée
	 * @param Order la commande
	 * @static
	 * @access private
	 * @return void
	 */
	private static function createExternalSupplierOrders( Order &$order ){
	
		//fournisseurs distincts
		
		$internalSupplierId = DBUtil::getParameterAdmin( "internal_supplier" );
		$externalSupplierIds = array();
		
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			$itemSupplierId = $orderItem->getValue( "idsupplier" );
			if( $itemSupplierId != $internalSupplierId && !in_array( $itemSupplierId, $externalSupplierIds ) )
				$externalSupplierIds[] = $itemSupplierId;

		}
		
		//créer les commandes fournisseur
		
		foreach( $externalSupplierIds as $idsupplier )
			self::createExternalSupplierOrder( $order, $idsupplier );
  
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un bon de livraison pour une commande fournisseur donnée
	 * @static
	 * @access public
	 * @todo objectiser la création
	 * @param SupplierOrder $supplierOrder la commande fournisseur
	 * @return DeliveryNote un bon de livraison
	 */
	 if ( INDEX_STRATEGY ){
	public static function createDeliveryNoteFromSupplierOrder( SupplierOrder &$supplierOrder ){
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		/*if( DBUtil::query( "SELECT idbl_delivery FROM `bl_delivery` WHERE idorder_supplier = '" . $supplierOrder->getId() . "' LIMIT 1" )->RecordCount() )
			return;*/
			
		//récupérer un nouveau numéro de BL disponible
		
		$rs1 =& DBUtil::query( "SELECT MAX( `idbl_delivery` ) + 1 AS idbl_delivery FROM `bl_delivery`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `idbl_delivery` ) + 1 AS idbl_delivery FROM `bl_delivery_row`" );
		
		$newPrimaryKeyValue 		= $rs1->fields( "idbl_delivery" );
		$newItemsPrimaryKeyValue	= $rs2->fields( "idbl_delivery" );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;
				
	 	$idbl_delivery = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//création

	 	$query = "
		INSERT INTO `bl_delivery` ( 
			idbl_delivery,
			idorder,
			idorder_supplier,
			iduser,
			idbuyer,
			idcontact,
			idsupplier,
			DateHeure,
			total_weight,
			date_send,
			availability_date,
			dispatch_date,
			iddelivery,
			username,
			lastupdate
		) VALUES ( 
			'$idbl_delivery',
			'" . $supplierOrder->getValue( "idorder" ) . "',
			'" . $supplierOrder->getId() . "',
			'" . User::getInstance()->getId() . "',
			'" . $supplierOrder->getValue( "idbuyer" ) . "',
			'" . $supplierOrder->getValue( "idcontact" ) . "',
			'" . $supplierOrder->getValue( "idsupplier" ) . "',
			NOW(),
			'" . $supplierOrder->getValue( "total_weight" ) . "',
			'0000-00-00',
			'" . $supplierOrder->getValue( "availability_date" ) . "',
			'" . $supplierOrder->getValue( "dispatch_date" ) . "',
			'" . $supplierOrder->getValue( "iddelivery" ) . "',
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			NOW()

		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
				
			trigger_error( "Impossible de créer le nouveau bon de livraison", E_USER_ERROR );
			exit();
					
		}
	
		//lignes article
		
		$iterator = $supplierOrder->getItems()->iterator();
		
		while( $iterator->hasNext() ){
			
			$supplierOrderItem =& $iterator->next();	
		
			$designation = $supplierOrderItem->getValue( "designation" );
			$extraFields = array( "site", "span", "store" );
			
			$rs =& DBUtil::query( "SELECT `" . implode( "`,`", $extraFields ) . "` FROM detail WHERE idarticle = '" . $supplierOrderItem->getValue( "idarticle" ) . "' LIMIT 1" );

			foreach( $extraFields as $extraField ){
				
				if( strlen( $rs->fields( $extraField ) ) )
					$designation .= "<br /><b>" . Dictionnary::translate( $extraField ) . "</b> : " . $rs->fields( $extraField );
			
			}
		
			$data = array( 
			
				"idbl_delivery" 	=> $idbl_delivery,
				"idrow" 			=> $supplierOrderItem->getValue( "idrow" ),
				"idorder_row" 		=> $supplierOrderItem->getValue( "idorder_row" ),
				"idarticle" 		=> $supplierOrderItem->getValue( "idarticle" ),
				"reference" 		=> $supplierOrderItem->getValue( "reference" ),
				"ref_supplier" 		=> $supplierOrderItem->getValue( "ref_supplier" ),
				"quantity" 			=> $supplierOrderItem->getValue( "quantity" ),
				"unit" 				=> $supplierOrderItem->getValue( "unit" ),
				"lot" 				=> $supplierOrderItem->getValue( "lot" ),
				"designation" 		=> $designation,
				"summary" 			=> $supplierOrderItem->getValue( "summary" ),
				"width" 			=> $supplierOrderItem->getValue( "width" ),
				"length" 			=> $supplierOrderItem->getValue( "length" ),
				"area" 				=> $supplierOrderItem->getValue( "area" ),
				"comment" 			=> $supplierOrderItem->getValue( "comment" ),
				"availability_date" => $supplierOrderItem->getValue( "availability_date" ),
				"useimg" 			=> $supplierOrderItem->getValue( "useimg" )
	
			);
			
			foreach( $data as $field => $value )
				$data[ $field ] = DBUtil::quote( $value );
				
			$query = "
			INSERT INTO bl_delivery_row( `" . implode( "`,`", array_keys( $data ) ) . "` )
			VALUES( " . implode( ",", array_values( $data ) ) . " )";

			$ret =& DBUtil::query( $query );
			
			if( $ret === false ){
				
				trigger_error( "Impossible de créer les lignes article du BL n°$idbl_delivery", E_USER_ERROR );
				exit();
					
			}
			
		}

		/* transport */
		
		$rs = DBUtil::query( "
	 	SELECT oc.idcarrier
	 	FROM order_supplier os, order_charges oc
	 	WHERE os.idorder_supplier = '". $supplierOrder->getId() . "'
	 	AND oc.idorder = os.idorder
	 	AND oc.idsupplier = os.idsupplier
	 	LIMIT 1" );
		
		$idcarrier = $rs->RecordCount() ? $rs->fields( "idcarrier" ) : 0;
		
	 	$query = "
	 	INSERT INTO carriage( idcarriage, idsupplier_factory, iddelivery, idcarrier )
	 	VALUES( '$idbl_delivery', '0', '" . $supplierOrder->getValue( "iddelivery" ) . "', '$idcarrier' )";
	 	
	 	DBUtil::query( $query );

		return new DeliveryNote( $idbl_delivery );
		
	}
	}else{
	
		public static function createDeliveryNoteFromSupplierOrder( SupplierOrder &$supplierOrder ){
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		/*if( DBUtil::query( "SELECT idbl_delivery FROM `bl_delivery` WHERE idorder_supplier = '" . $supplierOrder->getId() . "' LIMIT 1" )->RecordCount() )
			return;*/
			
		//récupérer un nouveau numéro de BL disponible
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		$table = 'bl_delivery';
		$idbl_delivery = TradeFactory::Indexations($table);
		//$rs1 =& DBUtil::query( "SELECT MAX( `idbl_delivery` ) + 1 AS idbl_delivery FROM `bl_delivery`" );
		//$rs2 =& DBUtil::query( "SELECT MAX( `idbl_delivery` ) + 1 AS idbl_delivery FROM `bl_delivery_row`" );
		
		//$newPrimaryKeyValue 		= $rs1->fields( "idbl_delivery" );
		//$newItemsPrimaryKeyValue	= $rs2->fields( "idbl_delivery" );
		
		//if( $newPrimaryKeyValue == null )
			//$newPrimaryKeyValue = 1;
		
		if( $idbl_delivery == null )
			$idbl_delivery = 1;
				
	 //	$idbl_delivery = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//création

	 	$query = "
		INSERT INTO `bl_delivery` ( 
			idbl_delivery,
			idorder,
			idorder_supplier,
			iduser,
			idbuyer,
			idcontact,
			idsupplier,
			DateHeure,
			total_weight,
			date_send,
			availability_date,
			dispatch_date,
			iddelivery,
			username,
			lastupdate
		) VALUES ( 
			'$idbl_delivery',
			'" . $supplierOrder->getValue( "idorder" ) . "',
			'" . $supplierOrder->getId() . "',
			'" . User::getInstance()->getId() . "',
			'" . $supplierOrder->getValue( "idbuyer" ) . "',
			'" . $supplierOrder->getValue( "idcontact" ) . "',
			'" . $supplierOrder->getValue( "idsupplier" ) . "',
			NOW(),
			'" . $supplierOrder->getValue( "total_weight" ) . "',
			'0000-00-00',
			'" . $supplierOrder->getValue( "availability_date" ) . "',
			'" . $supplierOrder->getValue( "dispatch_date" ) . "',
			'" . $supplierOrder->getValue( "iddelivery" ) . "',
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			NOW()

		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
				
			trigger_error( "Impossible de créer le nouveau bon de livraison", E_USER_ERROR );
			exit();
					
		}
	
		//lignes article
		
		$iterator = $supplierOrder->getItems()->iterator();
		
		while( $iterator->hasNext() ){
			
			$supplierOrderItem =& $iterator->next();	
		
			$designation = $supplierOrderItem->getValue( "designation" );
			$extraFields = array( "site", "span", "store" );
			
			$rs =& DBUtil::query( "SELECT `" . implode( "`,`", $extraFields ) . "` FROM detail WHERE idarticle = '" . $supplierOrderItem->getValue( "idarticle" ) . "' LIMIT 1" );

			foreach( $extraFields as $extraField ){
				
				if( strlen( $rs->fields( $extraField ) ) )
					$designation .= "<br /><b>" . Dictionnary::translate( $extraField ) . "</b> : " . $rs->fields( $extraField );
			
			}
		
			$data = array( 
			
				"idbl_delivery" 	=> $idbl_delivery,
				"idrow" 			=> $supplierOrderItem->getValue( "idrow" ),
				"idorder_row" 		=> $supplierOrderItem->getValue( "idorder_row" ),
				"idarticle" 		=> $supplierOrderItem->getValue( "idarticle" ),
				"reference" 		=> $supplierOrderItem->getValue( "reference" ),
				"ref_supplier" 		=> $supplierOrderItem->getValue( "ref_supplier" ),
				"quantity" 			=> $supplierOrderItem->getValue( "quantity" ),
				"unit" 				=> $supplierOrderItem->getValue( "unit" ),
				"lot" 				=> $supplierOrderItem->getValue( "lot" ),
				"designation" 		=> $designation,
				"summary" 			=> $supplierOrderItem->getValue( "summary" ),
				"width" 			=> $supplierOrderItem->getValue( "width" ),
				"length" 			=> $supplierOrderItem->getValue( "length" ),
				"area" 				=> $supplierOrderItem->getValue( "area" ),
				"comment" 			=> $supplierOrderItem->getValue( "comment" ),
				"availability_date" => $supplierOrderItem->getValue( "availability_date" ),
				"useimg" 			=> $supplierOrderItem->getValue( "useimg" )
	
			);
			
			foreach( $data as $field => $value )
				$data[ $field ] = DBUtil::quote( $value );
				
			$query = "
			INSERT INTO bl_delivery_row( `" . implode( "`,`", array_keys( $data ) ) . "` )
			VALUES( " . implode( ",", array_values( $data ) ) . " )";

			$ret =& DBUtil::query( $query );
			
			if( $ret === false ){
				
				trigger_error( "Impossible de créer les lignes article du BL n°$idbl_delivery", E_USER_ERROR );
				exit();
					
			}
			
		}

		/* transport */
		
		$rs = DBUtil::query( "
	 	SELECT oc.idcarrier
	 	FROM order_supplier os, order_charges oc
	 	WHERE os.idorder_supplier = '". $supplierOrder->getId() . "'
	 	AND oc.idorder = os.idorder
	 	AND oc.idsupplier = os.idsupplier
	 	LIMIT 1" );
		
		$idcarrier = $rs->RecordCount() ? $rs->fields( "idcarrier" ) : 0;
		
	 	$query = "
	 	INSERT INTO carriage( idcarriage, idsupplier_factory, iddelivery, idcarrier )
	 	VALUES( '$idbl_delivery', '0', '" . $supplierOrder->getValue( "iddelivery" ) . "', '$idcarrier' )";
	 	
	 	DBUtil::query( $query );

		return new DeliveryNote( $idbl_delivery );
		
	}
	
	
	}
	//----------------------------------------------------------------------------
	/**
	 * Met à jour les BL d'une commande fournisseur
	 * @static
	 * @access public
	 * @param SupplierOrder $supplierOrder la commande fournisseur
	 * @return void
	 */
	public static function synchronizeDeliveryNoteFromSupplierOrder( SupplierOrder &$supplierOrder ){
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		if( !$supplierOrder->getValue( "internal_supply" ) )
			return;
			
		$idorder_supplier 	= $supplierOrder->getValue( "idorder_supplier" );
	 	$idbl_delivery 		=& DBUtil::getDBValue( "idbl_delivery", "bl_delivery", "idorder_supplier", $idorder_supplier );
		$deliveryNote 		= new DeliveryNote( $idbl_delivery );
		
		$deliveryNote->removeItems();
			
		//données à synchroniser dans les lignes article
		
		$supplierOrderIterator = $supplierOrder->getItems()->iterator();
		
		$total_weight = 0.0; //@todo ALTER TABLE `bl_delivery` DROP `total_weight`
		while( $supplierOrderIterator->hasNext() ){
			
			$supplierOrderItem =& $supplierOrderIterator->next();	
			$total_weight += $supplierOrderItem->getValue( "weight" ); //@todo AAAARGH... encore un DROP

			$deliveryNote->addSupplierOrderItem( $supplierOrderItem );

		}
		
		//données à synchroniser dans la table principale
		
		$deliveryNote->setValue( "total_weight", 		$total_weight );
		$deliveryNote->setValue( "availability_date", 	$supplierOrder->getValue( "availability_date" ) );
		$deliveryNote->setValue( "dispatch_date", 		$supplierOrder->getValue( "dispatch_date" ) );
		$deliveryNote->setValue( "iddelivery", 			$supplierOrder->getValue( "iddelivery" ) );
		
		$deliveryNote->save();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande vierge pour un client donné
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Order la nouvelle commande
	 */
	  if ( INDEX_STRATEGY ){
	public static function createOrder( $idbuyer, $idcontact ){

	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `order` ( 
			iduser, 
			idbuyer, 
			idcontact 
		) VALUES ( 
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact' 
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$idorder = DBUtil::getInsertId();
		$order = new Order( $idorder );
		
		//assigner le client à la commande
		
		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
		$order->setCustomer( new Customer( $idbuyer, $idcontact ) );
	 	
	 	$order->setValue( "DateHeure", 			date( "Y-m-d H:i:s" ) );
	 	$order->setValue( "idpayment", 			DBUtil::getParameter( "default_idpayment" ) );
	 	$order->setValue( "idpayment_delay", 	DBUtil::getParameter( "default_idpayment_delay" ) );
		$order->setValue( "paid",				0 );
		$order->setValue( "status",				Order::$STATUS_IN_PROGRESS );
		$order->setValue( "rebate_type", 		"rate" );
		$order->setValue( "rebate_value", 		max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $idbuyer ) ) );
		
		$order->insure();
		
	 	$order->save();
	 	
	 	//les prospect deviennent des clients
	 	
	 	DBUtil::query( "UPDATE buyer SET contact = 0 WHERE idbuyer ='" . $order->getValue( "idbuyer" ) . "' AND contact = 1 LIMIT 1" );
	 	
	 	return $order;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande fournisseur vierge pour une commande donnée
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param Order $order la commande
	 * @return SupplierOrder la nouvelle commande
	 */
	private static function createSupplierOrderFromOrder( Order &$order, $idsupplier ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		//récupérer un nouveau numéro de commande disponible
		
		$rs1 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier_row`" ); //bof...
		
		$newPrimaryKeyValue 		= $rs1->fields( "idorder_supplier" );
		$newItemsPrimaryKeyValue	= $rs2->fields( "idorder_supplier" );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;
				
	 	$idorder_supplier = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );
	 	
	 	//réserver le numero de commande fournisseur
	 	
	 	$query = "
		INSERT INTO `order_supplier` ( 
			idorder_supplier, 
			idsupplier,
			idorder,
			iduser,
			idbuyer,
			idcontact
		) VALUES ( 
			'$idorder_supplier', 
			'$idsupplier',
			'" . $order->getId() . "',
			'" . User::getInstance()->getId() . "',
			'" . $order->getCustomer()->getId() . "',
			'" . $order->getCustomer()->getContact()->getId() . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande fournisseur", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$supplierOrder = new SupplierOrder( $idorder_supplier );

	 	$supplierOrder->setValue( "DateHeure", 			date( "Y-m-d H:i:s" ) );
		$supplierOrder->setValue( "status",				SupplierOrder::$STATUS_STANDBY );
		$supplierOrder->setValue( "delivery_mail_auto",	1 );
		 
	 	$supplierOrder->save();
	 	
	 	return $supplierOrder;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un nouveau devis vierge pour un client donné
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact ( optionnel, 0 par défaut )
	 * @return Estimate un nouveau devis
	 */
	public static function createEstimate( $idbuyer, $idcontact = 0 ){
		
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
		
		//récupérer un nouveau numéro de devis disponible
		
		$rs1 =& DBUtil::query( "SELECT MAX( `idestimate` ) + 1 AS idestimate FROM `estimate`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `idestimate` ) + 1 AS idestimate FROM `estimate_row`" );
		
		$newPrimaryKeyValue 		= $rs1->fields( "idestimate" );
		$newItemsPrimaryKeyValue	= $rs2->fields( "idestimate" );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;
				
	 	$idestimate = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de devis
	 	
	 	$query = "
		INSERT INTO `estimate` ( 
			idestimate, 
			iduser, 
			idbuyer, 
			idcontact 
		) VALUES ( 
			'$idestimate', 
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact' 
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$estimate = new Estimate( $idestimate );
		
		//assigner le client à la commande
		
		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
		$estimate->setCustomer( new Customer( $idbuyer, $idcontact ) );
	 	
	 	$estimate->setValue( "DateHeure", 		date( "Y-m-d H:i:s" ) );
	 	$estimate->setValue( "idpayment", 		DBUtil::getParameter( "default_idpayment" ) );
	 	$estimate->setValue( "idpayment_delay", DBUtil::getParameter( "default_idpayment_delay" ) );
		$estimate->setValue( "status", 			Estimate::$STATUS_IN_PROGRESS );
		$estimate->setValue( "rebate_type", 	"rate" );
		$estimate->setValue( "rebate_value", 	max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $idbuyer ) ) );
		
	 	$estimate->save();
	 	
	 	return $estimate;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle facture pour un client et une commande donnés
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param int $idorder le numéro de commande
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Order la nouvelle commande
	 */
	private static function createInvoice( $idorder, $idbuyer, $idcontact ){
		
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		//récupérer un nouveau numéro de commande disponible
		
		$rs1 =& DBUtil::query( "SELECT MAX( `idbilling_buyer` ) + 1 AS idbilling_buyer FROM `billing_buyer`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `idbilling_buyer` ) + 1 AS idbilling_buyer FROM `billing_buyer_row`" ); //bof...
		
		$newPrimaryKeyValue 		= $rs1->fields( "idbilling_buyer" );
		$newItemsPrimaryKeyValue	= $rs2->fields( "idbilling_buyer" );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;
				
	 	$idbilling_buyer = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `billing_buyer` ( 
			idbilling_buyer,
			idorder, 
			iduser, 
			idbuyer, 
			idcontact,
			DateHeure,
			`status`
		) VALUES ( 
			'$idbilling_buyer',
			'$idorder', 
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact',
			NOW(),
			'" . Invoice::$STATUS_INVOICED . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle facture", E_USER_ERROR );
			exit();
			
		}
		
		//créer la facture
		
		$invoice = new Invoice( $idbilling_buyer );

	 	return $invoice;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un nouvel avoir vierge
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param int $idbilling_buyer le numéro de facture à partir duquel on créé l'avoir
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Credit un nouveau lavoir
	 */
	private static function createCredit( $idbilling_buyer, $idbuyer, $idcontact ){
		
		//récupérer un nouveau numéro de lavoir disponible
		
		$rs1 =& DBUtil::query( "SELECT MAX( `idcredit` ) + 1 AS idcredit FROM `credits`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `idcredit` ) + 1 AS idcredit FROM `credit_rows`" ); //bof...
		
		$newPrimaryKeyValue 		= $rs1->fields( "idcredit" );
		$newItemsPrimaryKeyValue	= $rs2->fields( "idcredit" );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;

	 	$idcredit = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `credits` ( 
			idcredit,
			idbilling_buyer,
			iduser,
			idbuyer,
			idcontact,
			DateHeure
		) VALUES ( 
			'$idcredit',
			'$idbilling_buyer',
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact',
			NOW()
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer le nouvel avoir", E_USER_ERROR );
			exit();
			
		}
		
		//créer le zavoir
		
		$credit = new Credit( $idcredit );

	 	return $credit;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Transfert les frais de port fournisseur du devis à la commande
	 * @access private
	 * @param int $idestimate le numéro du devis
	 * @param int $idorder le numéro de la commande
	 * @return void
	 */
	private static function estimateChargesToOrderCharges( $idestimate, $idorder ){
		
		assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "intval( $idestimate ) > 0" );
		assert( "intval( $idorder ) > 0" );
		
		DBUtil::query( "DELETE FROM order_charges WHERE idorder = '$idorder'" );
		
		$rs =& DBUtil::query( "SELECT * FROM estimate_charges WHERE idestimate = '$idestimate'" );
	
		while( !$rs->EOF() ){

			$query = "
			INSERT INTO order_charges(
				idorder,
				idsupplier,
				charges,
				internal_supplier_charges,
				idcarrier,
				charges_auto,
				internal_supplier_charges_auto
			) VALUES (
				'$idorder',
				'" . $rs->fields( "idsupplier" ) . "',
				'" . $rs->fields( "charges" ) . "',
				'" . $rs->fields( "internal_supplier_charges" ) . "',
				'" . $rs->fields( "idcarrier" ) . "',
				'" . $rs->fields( "charges_auto" ) . "',
				'" . $rs->fields( "internal_supplier_charges_auto" ) . "'
			)";
			
			DBUtil::query( $query );
	
			$rs->MoveNext();
			
		}
		
	}
	}else{
	
	/**************************************************use : gestion des indexations *****************************************/
	/*************************************************************************************************************************/
	
		public static function createOrder( $idbuyer, $idcontact ){
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
	 	//réserver le numero de commande
	 	$table = 'order';
		$idorder = TradeFactory::Indexations($table);
	 	$query = "
		INSERT INTO `order` ( idorder,
			iduser, 
			idbuyer, 
			idcontact 
		) VALUES ( 
		'$idorder',
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact' 
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		//$idorder = DBUtil::getInsertId();
		$order = new Order( $idorder );
		
		//assigner le client à la commande
		
		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
		$order->setCustomer( new Customer( $idbuyer, $idcontact ) );
	 	
	 	$order->setValue( "DateHeure", 			date( "Y-m-d H:i:s" ) );
	 	$order->setValue( "idpayment", 			DBUtil::getParameter( "default_idpayment" ) );
	 	$order->setValue( "idpayment_delay", 	DBUtil::getParameter( "default_idpayment_delay" ) );
		$order->setValue( "paid",				0 );
		$order->setValue( "status",				Order::$STATUS_IN_PROGRESS );
		$order->setValue( "rebate_type", 		"rate" );
		$order->setValue( "rebate_value", 		max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $idbuyer ) ) );
		
		$order->insure();
		
	 	$order->save();
	 	
	 	//les prospect deviennent des clients
	 	
	 	DBUtil::query( "UPDATE buyer SET contact = 0 WHERE idbuyer ='" . $order->getValue( "idbuyer" ) . "' AND contact = 1 LIMIT 1" );
	 	
	 	return $order;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande fournisseur vierge pour une commande donnée
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param Order $order la commande
	 * @return SupplierOrder la nouvelle commande
	 */
	private static function createSupplierOrderFromOrder( Order &$order, $idsupplier ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		//récupérer un nouveau numéro de commande disponible
		$table = 'order_supplier';
		$idorder_supplier = TradeFactory::Indexations($table);
		//$rs1 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier`" );
	//	$rs2 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier_row`" ); //bof...
		
	//	$newPrimaryKeyValue 		= $rs1->fields( "idorder_supplier" );
	//	$newItemsPrimaryKeyValue	= $rs2->fields( "idorder_supplier" );
		
	//	if( $newPrimaryKeyValue == null )
		//	$newPrimaryKeyValue = 1;
		
		if( $idorder_supplier == null )
			$idorder_supplier = 1;
				
	 //	$idorder_supplier = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );
	 	
	 	//réserver le numero de commande fournisseur
	 	
	 	$query = "
		INSERT INTO `order_supplier` ( 
			idorder_supplier, 
			idsupplier,
			idorder,
			iduser,
			idbuyer,
			idcontact
		) VALUES ( 
			'$idorder_supplier', 
			'$idsupplier',
			'" . $order->getId() . "',
			'" . User::getInstance()->getId() . "',
			'" . $order->getCustomer()->getId() . "',
			'" . $order->getCustomer()->getContact()->getId() . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande fournisseur", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$supplierOrder = new SupplierOrder( $idorder_supplier );

	 	$supplierOrder->setValue( "DateHeure", 			date( "Y-m-d H:i:s" ) );
		$supplierOrder->setValue( "status",				SupplierOrder::$STATUS_STANDBY );
		$supplierOrder->setValue( "delivery_mail_auto",	1 );
		 
	 	$supplierOrder->save();
	 	
	 	return $supplierOrder;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un nouveau devis vierge pour un client donné
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact ( optionnel, 0 par défaut )
	 * @return Estimate un nouveau devis
	 */
	public static function createEstimate( $idbuyer, $idcontact = 0 ){
		
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		//récupérer un nouveau numéro de devis disponible
		$table = 'estimate';
		$idestimate = TradeFactory::Indexations($table);
		//$rs1 =& DBUtil::query( "SELECT MAX( `idestimate` ) + 1 AS idestimate FROM `estimate`" );
		//$rs2 =& DBUtil::query( "SELECT MAX( `idestimate` ) + 1 AS idestimate FROM `estimate_row`" );
		
		//$newPrimaryKeyValue 		= $rs1->fields( "idestimate" );
		//$newItemsPrimaryKeyValue	= $rs2->fields( "idestimate" );
		
		//if( $newPrimaryKeyValue == null )
			//$newPrimaryKeyValue = 1;
		
		if( $idestimate == null )
			$idestimate = 1;
				
	 	//$idestimate = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de devis
	 	
	 	$query = "
		INSERT INTO `estimate` ( 
			idestimate, 
			iduser, 
			idbuyer, 
			idcontact 
		) VALUES ( 
			'$idestimate', 
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact' 
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$estimate = new Estimate( $idestimate );
		
		//assigner le client à la commande
		
		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
		$estimate->setCustomer( new Customer( $idbuyer, $idcontact ) );
	 	
	 	$estimate->setValue( "DateHeure", 		date( "Y-m-d H:i:s" ) );
	 	$estimate->setValue( "idpayment", 		DBUtil::getParameter( "default_idpayment" ) );
	 	$estimate->setValue( "idpayment_delay", DBUtil::getParameter( "default_idpayment_delay" ) );
		$estimate->setValue( "status", 			Estimate::$STATUS_IN_PROGRESS );
		$estimate->setValue( "rebate_type", 	"rate" );
		$estimate->setValue( "rebate_value", 	max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $idbuyer ) ) );
		
	 	$estimate->save();
	 	
	 	return $estimate;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle facture pour un client et une commande donnés
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param int $idorder le numéro de commande
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Order la nouvelle commande
	 */
	private static function createInvoice( $idorder, $idbuyer, $idcontact ){
		
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		//récupérer un nouveau numéro de commande disponible
		$table = 'billing_buyer';
		$idbilling_buyer = TradeFactory::Indexations($table);
		//$rs1 =& DBUtil::query( "SELECT MAX( `idbilling_buyer` ) + 1 AS idbilling_buyer FROM `billing_buyer`" );
		//$rs2 =& DBUtil::query( "SELECT MAX( `idbilling_buyer` ) + 1 AS idbilling_buyer FROM `billing_buyer_row`" ); //bof...
		
	//	$newPrimaryKeyValue 		= $rs1->fields( "idbilling_buyer" );
	//	$newItemsPrimaryKeyValue	= $rs2->fields( "idbilling_buyer" );
		
		//if( $newPrimaryKeyValue == null )
			//$newPrimaryKeyValue = 1;
		
		if( $idbilling_buyer == null )
			$idbilling_buyer = 1;
				
	 	//$idbilling_buyer = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `billing_buyer` ( 
			idbilling_buyer,
			idorder, 
			iduser, 
			idbuyer, 
			idcontact,
			DateHeure,
			`status`
		) VALUES ( 
			'$idbilling_buyer',
			'$idorder', 
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact',
			NOW(),
			'" . Invoice::$STATUS_INVOICED . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle facture", E_USER_ERROR );
			exit();
			
		}
		
		//créer la facture
		
		$invoice = new Invoice( $idbilling_buyer );

	 	return $invoice;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un nouvel avoir vierge
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param int $idbilling_buyer le numéro de facture à partir duquel on créé l'avoir
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Credit un nouveau lavoir
	 */
	private static function createCredit( $idbilling_buyer, $idbuyer, $idcontact ){
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		//récupérer un nouveau numéro de lavoir disponible
		$table = 'credits';
		$idcredit = TradeFactory::Indexations($table);
		//$rs1 =& DBUtil::query( "SELECT MAX( `idcredit` ) + 1 AS idcredit FROM `credits`" );
	//	$rs2 =& DBUtil::query( "SELECT MAX( `idcredit` ) + 1 AS idcredit FROM `credit_rows`" ); //bof...
		
		//$newPrimaryKeyValue 		= $rs1->fields( "idcredit" );
		//$newItemsPrimaryKeyValue	= $rs2->fields( "idcredit" );
		
		//if( $newPrimaryKeyValue == null )
			//$newPrimaryKeyValue = 1;
		
		if( $idcredit == null )
			$idcredit = 1;

	 //	$idcredit = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `credits` ( 
			idcredit,
			idbilling_buyer,
			iduser,
			idbuyer,
			idcontact,
			DateHeure
		) VALUES ( 
			'$idcredit',
			'$idbilling_buyer',
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact',
			NOW()
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer le nouvel avoir", E_USER_ERROR );
			exit();
			
		}
		
		//créer le zavoir
		
		$credit = new Credit( $idcredit );

	 	return $credit;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Transfert les frais de port fournisseur du devis à la commande
	 * @access private
	 * @param int $idestimate le numéro du devis
	 * @param int $idorder le numéro de la commande
	 * @return void
	 */
	private static function estimateChargesToOrderCharges( $idestimate, $idorder ){
		
		assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
	//	assert( "intval( $idestimate ) > 0" );
	//	assert( "intval( $idorder ) > 0" );
		
		DBUtil::query( "DELETE FROM order_charges WHERE idorder = '$idorder'" );
		
		$rs =& DBUtil::query( "SELECT * FROM estimate_charges WHERE idestimate = '$idestimate'" );
	
		while( !$rs->EOF() ){

			$query = "
			INSERT INTO order_charges(
				idorder,
				idsupplier,
				charges,
				internal_supplier_charges,
				idcarrier,
				charges_auto,
				internal_supplier_charges_auto
			) VALUES (
				'$idorder',
				'" . $rs->fields( "idsupplier" ) . "',
				'" . $rs->fields( "charges" ) . "',
				'" . $rs->fields( "internal_supplier_charges" ) . "',
				'" . $rs->fields( "idcarrier" ) . "',
				'" . $rs->fields( "charges_auto" ) . "',
				'" . $rs->fields( "internal_supplier_charges_auto" ) . "'
			)";
			
			DBUtil::query( $query );
	
			$rs->MoveNext();
			
		}
		
	}
	
	/*********************************************** fin : gestion des indexations ******************************************/
	}
	//----------------------------------------------------------------------------
	/**
	 * Transfert des pièces jointe du devis à la commande
	 * @access private
	 * @param int $estimate
	 * @param int $order
	 * @return void
	 */
	private static function estimateAttachmentToOrderAttachment( Estimate &$estimate, Order &$order ){

		global $GLOBAL_START_PATH;

		$attachments = array( 
		
			"estimate_doc" 		=> "order_doc", 
			"offer_signed" 		=> "offer_signed", 
			"supplier_offer" 	=> "supplier_offer",
			"supplier_doc" 		=> "supplier_doc" 
		
		);
		
		foreach( $attachments as $estimateAttachment => $orderAttachment ){
			
			if( strlen( $estimate->getValue( $estimateAttachment ) ) ){
				
				if( !file_exists( "$GLOBAL_START_PATH/data/order/" . date( "Y" ) ) ){
					
					@mkdir( "$GLOBAL_START_PATH/data/order/" . date( "Y" ) );
					@chmod( "$GLOBAL_START_PATH/data/order/" . date( "Y" ), 0755 );
					
				}
				
				$filename 	= substr( $estimate->getValue( $estimateAttachment ), strrpos( $estimate->getValue( $estimateAttachment ), "/" ) + 1 );
				$source 	= "$GLOBAL_START_PATH/data/estimate/" . $estimate->getValue( $estimateAttachment );
				$dest 		= "$GLOBAL_START_PATH/data/order/" . date( "Y" ) . "/$filename";
				
				 if( @copy( $source, $dest ) === false || @chmod( $dest, 0755 ) === false )
	        		trigger_error( "Echec lors de la copie de la pièce jointe lors de la confirmation du devis n°" . $estimate->getId(), E_USER_ERROR );
	     	
		      	$order->setValue( $orderAttachment, date( "Y" ) . "/$filename" );
		      	
			}

		}
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture à partir de tous les BLs qui n'ont pas été facturés pour une commande donnée
	 * @static
	 * @param Order $order la commande à facturer
	 * @return void
	 */
	public static function createInvoiceFromOrder( Order &$order ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		$idorder = $order->getId();
		
		//récupérer toutes les lignes articles de la commande qui n'ont pas encore été facturée ( cf. facturation partielle )
		
		$idorder_rows 		= array();
		$idbl_deliveries 	= array();
		
		$query = "
		SELECT blrow.idbl_delivery, blrow.idorder_row
		FROM bl_delivery bl, bl_delivery_row blrow, order_supplier os
		WHERE bl.idbl_delivery = blrow.idbl_delivery
		AND bl.idorder = '$idorder'
		AND bl.idorder_supplier = os.idorder_supplier
		AND os.internal_supply = 0
		AND os.idorder <> 0
		AND bl.idbilling_buyer = 0";

		$rs =& DBUtil::query( $query );

		while( !$rs->EOF() ){
		
			$idorder_rows[] = $rs->fields( "idorder_row" );
			
			if( !in_array( $rs->fields( "idbl_delivery" ), $idbl_deliveries ) )
				$idbl_deliveries[] = $rs->fields( "idbl_delivery" );
			
			$rs->MoveNext();
			
		}		
		
		//si rien à facturer
		
		if( !count( $idorder_rows ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}
		
		//créer la facture
		
		$idbuyer 	= $order->getValue( "idbuyer" );
		$idcontact 	= $order->getValue( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer
		
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			if( in_array( $orderItem->getValue( "idrow" ), $idorder_rows ) )
				$invoice->addOrderItem( $orderItem );
				
		}
		
		//calcul des frais port qui n'ont pas encore été facturés ( cf. facturation partielle )
		
		$total_charge_ht 	= $order->getValue( "total_charge_ht" );
		$total_charge 		= $order->getValue( "total_charge" );
		$charge_vat 		= $order->getValue( "charge_vat" );
		
		$query = "
		SELECT SUM( total_charge ) AS total_charge,
		SUM( total_charge_ht ) AS total_charge_ht,
		SUM( charge_vat ) AS charge_vat
		FROM billing_buyer
		WHERE idorder = '$idorder'
		GROUP BY idorder";
	
		$invoicesRS =& DBUtil::query( $query );
		$invoiceCount = $invoicesRS->RecordCount();
		$isPartialInvoice = $invoiceCount > 1;
		
		while( !$invoicesRS->EOF() ){
		
			$total_charge 				-= $invoicesRS->fields( "total_charge" );
			$total_charge_ht 			-= $invoicesRS->fields( "total_charge_ht" );
			$charge_vat 				-= $invoicesRS->fields( "charge_vat" );

			$invoicesRS->MoveNext();
				
		}
		
		$invoice->setValue( "total_charge", 	$total_charge );
		$invoice->setValue( "total_charge_ht", 	$total_charge_ht );
		$invoice->setValue( "charge_vat", 		$total_charge - $total_charge_ht );
		$invoice->setValue( "charge_vat_rate", 	$order->getValue( "charge_vat_rate" ) ); //ne correspond pas vraiment si facturation partielle
		
		//appliquer les pourcentages de remise, d'acompte et de remise pour paiement comptant

		if( !$isPartialInvoice ){
			
			//remise sur facture
			
			$invoice->setValue( "total_discount" , $order->getValue( "total_discount" ) );
			$invoice->setValue( "total_discount_amount" , $order->getValue( "total_discount_amount" ) );
			
			//remise pour paiement comptant
			
			$invoice->setValue( "billing_rate" , $order->getValue( "billing_rate" ) );
			$invoice->setValue( "billing_amount" , $order->getValue( "billing_amount" ) );
			
			//acompte
			
			$invoice->setValue( "down_payment_type" , $order->getValue( "down_payment_type" ) );
			$invoice->setValue( "down_payment_value" , $order->getValue( "down_payment_value" ) );

		}
		else{ //on est en train de facturer partiellement le reste de la commande
			
			$articlesET = 0.0;
			$it = $invoice->getItems()->iterator();
			while( $it->hasNext() )
				$articlesET += $it->next()->getTotalET();
				
			$total_discount_rate = $order->getValue( "total_discount" );
			$total_discount_amount = $articlesET * $total_discount_rate / 100.0;
			$invoice->setValue( "total_discount_amount" , round( $total_discount_amount, 2 ) );
			$invoice->setValue( "total_discount" , $total_discount_rate );
			
			$billing_rate = $order->getValue( "billing_rate" );
			$ht = $articlesET - $invoice->get( "total_discount_amount" );
			$total_charge_ht = $invoice->getValue( "total_charge_ht" );
			$billing_amount = 0.0 + $billing_rate * ( $ht + $total_charge_ht ) / 100.0;
			$invoice->setValue( "billing_amount" , $billing_amount );
			
			//acompte
			
			$invoice->setValue( "down_payment_type" , $order->getValue( "down_payment_type" ) );
			$invoice->setValue( "down_payment_value" , $order->getValue( "down_payment_value" ) );
			
		}
		
		//propriétés de la facture héritées de la commande

		$invoice->setValue( "iderp", 				$order->getValue( "iderp" ) );
		$invoice->setValue( "idbuyer_erp", 			$order->getValue( "idbuyer_erp" ) );
		$invoice->setValue( "idbilling_adress_erp", $order->getValue( "idbilling_adress_erp" ) );
		$invoice->setValue( "idcurrency", 			$order->getValue( "idcurrency" ) );
		$invoice->setValue( "idcommercant", 		$order->getValue( "idcommercant" ) );
		$invoice->setValue( "idpayment", 			$order->getValue( "idpayment" ) );
		$invoice->setValue( "idpayment_delay", 		$order->getValue( "idpayment_delay" ) );
		$invoice->setValue( "send_by", 				$order->getValue( "send_by" ) );
		$invoice->setValue( "n_order", 				$order->getValue( "n_order" ) );
		$invoice->setValue( "remote_creation_date", $order->getValue( "remote_creation_date" ) );
		$invoice->setValue( "exportable", 			$order->getValue( "exportable" ) );
		$invoice->setValue( "device", 				$order->getValue( "device" ) );
		$invoice->setValue( "factor", 				$order->getValue( "factor" ) );
		$invoice->setValue( "svg_mail", 			$order->getValue( "svg_mail" ) );
		$invoice->setValue( "svg_fax", 				$order->getValue( "svg_fax" ) );
		$invoice->setValue( "pro_forma", 			$order->getValue( "pro_forma" ) );
		$invoice->setValue( "global_delay", 		$order->getValue( "global_delay" ) );
		$invoice->setValue( "no_tax_export", 		$order->getValue( "no_tax_export" ) );
		$invoice->setValue( "deliv_payment", 		Invoice::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->getValue( "idpayment_delay" ) ) ); //? @todo
		$invoice->setValue( "credit_amount", 		$order->getValue( "credit_amount" ) );
		$invoice->setValue( "charge_auto", 			0 );
		$invoice->setValue( "total_charge_auto", 	0 );
		
		//adresses de livraison et de facturation
		
		$idbilling_adress 	= $order->getValue( "idbilling_adress" );
		$iddelivery 		= $order->getValue( "iddelivery" );
		$idbuyer 			= $order->getValue( "idbuyer" );
		
		if( $idbilling_adress )
				$invoice->setUseInvoiceAddress( new InvoiceAddress( $idbilling_adress ) );
		else 	$invoice->setUseInvoiceAddress( $order->getCustomer()->getAddress() );
		
		if( $iddelivery )
				$invoice->setUseForwardingAddress( new ForwardingAddress( $iddelivery ) );
		else 	$invoice->setUseForwardingAddress( $order->getCustomer()->getAddress() );

		//escompte
		
		if( $order->getValue( "rebate_value" ) > 0.0 ){
			
			switch( $order->getValue( "rebate_type" ) ){
				
				case "rate" :
				
					$invoice->setValue( "rebate_type", 	"rate" ); 
	        		$invoice->setValue( "rebate_value", $order->getValue( "rebate_value" ) );
	        
					break;
					
				case "amount" :
				
					if( $isPartialInvoice ){
						
						$invoice->save();
	
						$rebate_rate 	= $order->getValue( "rebate_value" ) * 100.0 / $order->getTotalATI();
						$rebate_value 	= $invoice->getTotalATI() * $rebate_rate / 100.0;
						
						$invoice->setValue( "rebate_type", "amount" );
						$invoice->setValue( "rebate_value", $rebate_value );
					
					}
					else{
						
						$invoice->setValue( "rebate_type", "amount" ); 
	        			$invoice->setValue( "rebate_value", $order->getValue( "rebate_value" ) );
	        		
					}
					
					break;			
				
			}

		}
		
		//mettre à jour le numéro de facture dans les BLs concernés : @todo : la jointure est faite dans le mauvais sens!
		
		$idbilling_buyer = $invoice->getId();
		
		foreach( $idbl_deliveries as $idbl_delivery )
			DBUtil::query( "UPDATE bl_delivery SET idbilling_buyer = '$idbilling_buyer' WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
				
		
		/* enregistrement des paiements */
		
		self::registerInvoicePayments( $invoice );
		
		//assurance crédit
		
		$invoice->insure( $order->getValue( "insurance" ) == 1 );
		
		/* sauvegarde */
		
		$invoice->save();
		
		return $invoice;
	
	}

	//----------------------------------------------------------------------------
	/**
	 * @todo gestion des règlements multiples
	 */
	private function registerInvoicePayments( Invoice &$invoice ){

		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$unpaidAmount = $invoice->getNetBill();
		$idbilling = $invoice->getValue('idbilling_buyer');
		
		if( $unpaidAmount == 0.0 )
			return;
			
		/* Règlement par chèque cadeau */
		
		$rs =& DBUtil::query( "SELECT idtoken FROM gift_token WHERE idorder = '" . $invoice->getValue( "idorder" ) . "' LIMIT 1" );
		
		if( $rs->RecordCount() ){
			
			include_once( dirname( __FILE__ ) . "/GiftToken.php" );
			
			$query = "
			SELECT gtm.value 
			FROM gift_token_model gtm, gift_token gt 
			WHERE gt.idtoken = '" . $rs->fields( "idtoken" ) . "'
			AND gtm.type LIKE '" . GiftToken::$TYPE_AMOUNT . "' 
			AND gt.idtoken_model = gtm.idmodel
			LIMIT 1";
		
			$rs =& DBUtil::query( $query );
			$gift_token_amount = $rs->RecordCount() ? min( $rs->fields( "value" ), $unpaidAmount ) : 0.0;
			
			if( $gift_token_amount > 0.0 ){
				
				include_once( dirname( __FILE__ ) . "/Payment.php" );
				
				if( $gift_token_amount < $invoice->getNetBill() )
						RegulationsBuyer::createPartialPayment( $invoice, Payment::$PAYMENT_MOD_GIFT_TOKEN, $gift_token_amount );
				else 	RegulationsBuyer::createPayment( $invoice );
				
				$unpaidAmount -= $gift_token_amount;
				
				$invoice->setValue( "balance_amount", 	$invoice->getValue( "balance_amount" ) + $gift_token_amount );
				$invoice->setValue( "balance_date", 	date( "Y-m-d" ) );
				$invoice->setValue( "status", $unpaidAmount > 0.0 ? Invoice::$STATUS_INVOICED : Invoice::$STATUS_PAID );
				$invoice->save();
			
			}
			
		}
	
		if( $unpaidAmount == 0.0 )
			return;
			
		/* Règlement par bon d'achat ( avoir ) */
		
		$rs =& DBUtil::query( "SELECT credit_amount FROM `order` WHERE idorder = '" . $invoice->getValue( "idorder" ) . "' LIMIT 1" );
		
		if( $rs->RecordCount() ){

			$credit_balance = DBUtil::getDBValue( "credit_balance", "buyer", "idbuyer", $invoice->getValue( "idbuyer" ) );
			$credit_amount 	= min( $rs->fields( "credit_amount" ), $unpaidAmount, $credit_balance );
	
			if( $credit_amount > 0.0 ){

				$query = "
				UPDATE buyer 
				SET credit_balance = GREATEST( 0.0, credit_balance - $credit_amount ) 
				WHERE idbuyer = '" . $invoice->getValue( "idbuyer" ) . "' 
				AND credit_balance >= '$credit_amount'
				LIMIT 1";

				DBUtil::query( $query );
				
				if( !DBUtil::getAffectedRows() ){
					
					trigger_error( "transaction non autorisée", E_USER_ERROR );
					exit();
					
				}

				$unpaidAmount -= $credit_amount;
				
				$invoice->setValue( "balance_amount", 	$invoice->getValue( "balance_amount" ) + $credit_amount );
				$invoice->setValue( "balance_date", 	date( "Y-m-d" ) );
				$invoice->setValue( "status", $unpaidAmount > 0.0 ? Invoice::$STATUS_INVOICED : Invoice::$STATUS_PAID );
				$invoice->save();
			
			}
			
		}
	
		if( $unpaidAmount == 0.0 )
			return;
		
		//création du reglement pour l'acompte si acompte tt seul. bisou
		$down_payment_amount = $invoice->getDownPaymentAmount();

		if( $down_payment_amount > 0 ){
			$rs =& DBUtil::query( "SELECT down_payment_idpayment,down_payment_idregulation, down_payment_date, down_payment_comment FROM `order` WHERE idorder = '" . $invoice->getValue( "idorder" ) . "' LIMIT 1" );
			
			$dateAcompte = $rs->fields("down_payment_date");
 			$idDownPayment = $rs->fields("down_payment_idpayment");
 			$commentAcompte = $rs->fields("down_payment_comment");
 			
			$idregulationToAdd = $rs->fields("down_payment_idregulation");
			
 			// comme le règlement est créé à l'avance, on fait juste le lien dans la table billing_regulations_buyer
			//RegulationsBuyer::createPartialPayment( $invoice, $idDownPayment , $down_payment_amount, 1 ,$dateAcompte,$commentAcompte);
			if( $idregulationToAdd == 0 ){
				$idregulationToAdd = self::createMyRegulation($invoice->getValue( "idorder" ),"order",$invoice->getValue( "idbuyer" ),$down_payment_amount,$idDownPayment,$dateAcompte,0,"aucune info","créé automatiquement cause ancienne commande",1);
			}
				// ------- Mise à jour de l'id gestion des Index  #1161
			DBUtil::query("INSERT INTO billing_regulations_buyer VALUES('$idbilling', $idregulationToAdd,$down_payment_amount, 0.0, " . DBUtil::quote( User::getInstance()->get( "login" ) ) . ", NOW())");
		
		}
		
		
		/*
		 * paiement direct à la commande ( CB, fianet, ... )
		 * Note : dans le cas d'un paiement direct lors du processus de commande en ligne, le champ cash_payment est renseigné à 1
		 * et le montant réglé est stocké dans balance_amount.
		 * Attention : on ne gère pas ici de paiement partiel! Le montant qui a été réglé par CB correspond à la total de la commande ou 
		 * au total auquel ont été déduits le montant des avoirs et des chèques cadeaux utilisés
		 */

		$rs =& DBUtil::query( "SELECT cash_payment,cash_payment_idregulation FROM `order` WHERE idorder = '" . $invoice->getValue( "idorder" ) . "' LIMIT 1" );
		
		if( $rs->RecordCount() && $rs->fields( "cash_payment" ) ){
			$invoice->setValue( "balance_amount", 	$invoice->getNetBill() );
			$invoice->setValue( "balance_date", 	date( "Y-m-d" ) );
			
			$idregulationToAdd = $rs->fields( "cash_payment_idregulation" );
			
			if( $idregulationToAdd == 0 ){
				//pour les anciennes on créé le reglement...
				$idregulationToAdd = self::createMyRegulation($invoice->getValue( "idorder" ),"order",$invoice->getValue( "idbuyer" ),$invoice->getNetBill(),$invoice->getValue( "idpayment" ),date( "Y-m-d" ),1,"aucune info","règlement créé automatiquement cause ancienne commande");
			}
				// ------- Mise à jour de l'id gestion des Index  #1161
			if( $unpaidAmount < $invoice->getNetBill() ){
				//RegulationsBuyer::createPartialPayment( $invoice, $invoice->getValue( "idpayment" ), $unpaidAmount );
				$myAmountC = $unpaidAmount;
				DBUtil::query("INSERT INTO billing_regulations_buyer VALUES('$idbilling', $idregulationToAdd,$myAmountC, 0.0, " . DBUtil::quote( User::getInstance()->get( "login" ) ) . ", NOW())");
			}else{ 	
				//RegulationsBuyer::createPayment( $invoice );
				$myAmountC = $invoice->getNetBill();
				DBUtil::query("INSERT INTO billing_regulations_buyer VALUES('$idbilling', $idregulationToAdd,$myAmountC, 0.0, " . DBUtil::quote( User::getInstance()->get( "login" ) ) . ", NOW())");
			}
			
			$invoice->setValue( "status", Invoice::$STATUS_PAID );
			$invoice->save();
	
			return;
			
		}
			
		/* modes paiement différés */

		$invoice->setValue( "status", Invoice::$STATUS_INVOICED );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture partielle à partir d'une commande et d'un numéro de BL donnés
	 * @static
	 * @param Order $order la commande à facturer partiellement
	 * @param int $idbl_delivery le numéro BL contenant les articles à facturer
	 * @param float $total_charge_ht le montant des frais de port HT répartis dans cette facture
	 * @return void
	 */
	public static function createInvoiceFromDeliveryNote( Order &$order, $idbl_delivery, $total_charge_ht = 0.0 ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$idorder = $order->getId();
	
		/* ne pas refacturer */
		
		if( DBUtil::query( "SELECT idbilling_buyer FROM bl_delivery WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" )->fields( "idbilling_buyer" ) )
			return;
			
		//récupérer les lignes article à facturer
		
		$idorder_rows = array();
		$rs =& DBUtil::query( "SELECT idorder_row FROM bl_delivery_row WHERE idbl_delivery = '$idbl_delivery'" );

		while( !$rs->EOF() ){
		
			$idorder_rows[] = $rs->fields( "idorder_row" );
			
			$rs->MoveNext();
			
		}		
		
		//si rien à facturer
		
		if( !count( $idorder_rows ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}

		//créer la facture
		
		$idbuyer 	= $order->getValue( "idbuyer" );
		$idcontact 	= $order->getValue( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer

		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			if( in_array( $orderItem->getValue( "idrow" ), $idorder_rows ) )
				$invoice->addOrderItem( $orderItem );
			
		}
		
		//appliquer les frais de port
		
		$charge_vat_rate 	= $invoice->getVATRate() / 100.0;
		$total_charge 		= $total_charge_ht * ( 1 + $charge_vat_rate );
		
		$invoice->setValue( "total_charge_ht", $total_charge_ht );
		$invoice->setValue( "total_charge", $total_charge );
		$invoice->setValue( "charge_vat", $total_charge - $total_charge_ht );
		$invoice->setValue( "charge_vat_rate", $charge_vat_rate );
		
		//appliquer les pourcentages de remise, d'acompte et de remise pour paiement comptant
		
		$articlesET = 0.0;
		$it = $invoice->getItems()->iterator();
		while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
				
		$total_discount_rate = $order->getValue( "total_discount" );
		$total_discount_amount = $articlesET * $total_discount_rate / 100.0;
		$invoice->setValue( "total_discount_amount" , round( $total_discount_amount, 2 ) );
		$invoice->setValue( "total_discount" , $total_discount_rate );
		
		$billing_rate = $order->getValue( "billing_rate" );
		$ht = $articlesET - $invoice->get( "total_discount_amount" );
		$total_charge_ht = $invoice->getValue( "total_charge_ht" );
		$billing_amount = 0.0 + $billing_rate * ( $ht + $total_charge_ht ) / 100.0;
		$invoice->setValue( "billing_amount" , $billing_amount );
		
		$invoice->setValue( "down_payment_type" , $order->getValue( "down_payment_type" ) );
		$invoice->setValue( "down_payment_value" , $order->getValue( "down_payment_value" ) );

		//propriétés de la facture héritées de la commande

		$invoice->setValue( "iderp", 				$order->getValue( "iderp" ) );
		$invoice->setValue( "idbuyer_erp", 			$order->getValue( "idbuyer_erp" ) );
		$invoice->setValue( "idbilling_adress_erp", $order->getValue( "idbilling_adress_erp" ) );
		$invoice->setValue( "idcurrency", 			$order->getValue( "idcurrency" ) );
		$invoice->setValue( "idcommercant", 		$order->getValue( "idcommercant" ) );
		$invoice->setValue( "idpayment", 			$order->getValue( "idpayment" ) );
		$invoice->setValue( "idpayment_delay", 		$order->getValue( "idpayment_delay" ) );
		$invoice->setValue( "send_by", 				$order->getValue( "send_by" ) );
		$invoice->setValue( "n_order", 				$order->getValue( "n_order" ) );
		$invoice->setValue( "remote_creation_date", $order->getValue( "remote_creation_date" ) );
		$invoice->setValue( "exportable", 			$order->getValue( "exportable" ) );
		$invoice->setValue( "device", 				$order->getValue( "device" ) );
		$invoice->setValue( "factor", 				$order->getValue( "factor" ) );
		$invoice->setValue( "svg_mail", 			$order->getValue( "svg_mail" ) );
		$invoice->setValue( "svg_fax", 				$order->getValue( "svg_fax" ) );
		$invoice->setValue( "pro_forma", 			$order->getValue( "pro_forma" ) );
		$invoice->setValue( "global_delay", 		$order->getValue( "global_delay" ) );
		$invoice->setValue( "deliv_payment", 		Invoice::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->getValue( "idpayment_delay" ) ) );
		
		if( $order->getValue( "cash_payment" ) ){
			
			$invoice->setValue( "status", Invoice::$STATUS_PAID );
			$invoice->setValue( "balance_amount", $invoice->getValue( "total_amount_ht" ) );
			$invoice->setValue( "balance_date", $order->getValue( "balance_date" ) );

		}
		else{
			
			$invoice->setValue( "status", Invoice::$STATUS_INVOICED );
			$invoice->setValue( "balance_amount", 0.0 );
			$invoice->setValue( "balance_date", "0000-00-00" );
			
		}
		
		$invoice->setValue( "charge_auto", 0 );
		$invoice->setValue( "total_charge_auto", 0 );
		
		
		//adresses de livraison et de facturation
		
		$idbilling_adress 	= $order->getValue( "idbilling_adress" );
		$iddelivery 		= $order->getValue( "iddelivery" );
		$idbuyer 			= $order->getValue( "idbuyer" );
		
		if( $idbilling_adress )
				$invoice->setUseInvoiceAddress( new InvoiceAddress( $idbilling_adress ) );
		else 	$invoice->setUseInvoiceAddress( $order->getCustomer()->getAddress() );
		
		if( $iddelivery )
				$invoice->setUseForwardingAddress( new ForwardingAddress( $iddelivery ) );
		else 	$invoice->setUseForwardingAddress( $order->getCustomer()->getAddress() );

		//escompte
		
		$invoice->save();
		$isPartialInvoice = Invoice::isPartialInvoice( $invoice->getId() );
		
		if( $order->getValue( "rebate_value" ) > 0.0 ){
			
			switch( $order->getValue( "rebate_type" ) ){
				
				case "rate" :
				
					$invoice->setValue( "rebate_type", 	"rate" ); 
	        		$invoice->setValue( "rebate_value", $order->getValue( "rebate_value" ) );
	        
					break;
					
				case "amount" :
				
					if( $isPartialInvoice ){
	
						$rebate_rate 	= $order->getValue( "rebate_value" ) * 100.0 / $order->getTotalATI();
						$rebate_value 	= $invoice->getTotalATI() * $rebate_rate / 100.0;
						
						$invoice->setValue( "rebate_type", "amount" );
						$invoice->setValue( "rebate_value", $rebate_value );
					
					}
					else{
						
						$invoice->setValue( "rebate_type", "amount" ); 
	        			$invoice->setValue( "rebate_value", $order->getValue( "rebate_value" ) );
	        		
					}
					
					break;			
				
			}

		}
		
		//assurance crédit
		
		$invoice->insure( $order->getValue( "insurance" ) == 1 );
		
		//sauvegarde
		
		$invoice->save();
		
		//mettre à jour le numéro de facture dans le BL concerné
		
		$idbilling_buyer = $invoice->getId();
		
		DBUtil::query( "UPDATE bl_delivery SET idbilling_buyer = '$idbilling_buyer' WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
		
		//paiement direct
		
		if( $order->getValue( "cash_payment" ) )
			RegulationsBuyer::createPayment( $invoice );
			
		return $invoice;
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture partielle à partir d'une commande et d'une liste de numéros de BL donnée
	 * @static
	 * @param Order $order la commande à facturer partiellement
	 * @param array $deliveryNotes un tableau indicé contenant les numéros BL à facturer
	 * @param float $total_charge_ht le montant des frais de port HT répartis dans cette facture
	 * @return void
	 */
	public static function createInvoiceFromDeliveryNotes( Order &$order, $deliveryNotes, $total_charge_ht = 0.0 ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$idorder = $order->getId();
	
		/* ne pas refacturer */
		
		$tmp = array();
		$i = 0;
		while( $i < count( $deliveryNotes ) ){
	
			if( DBUtil::query( "SELECT idbilling_buyer FROM bl_delivery WHERE idbl_delivery = '" . $deliveryNotes[ $i ] . "' LIMIT 1" )->fields( "idbilling_buyer" ) == 0 )
				$tmp[] = $deliveryNotes[ $i ];
		
			$i++;
			
		}
		
		if( !count( $tmp ) )
			return;
			
		$deliveryNotes = $tmp;
		
		//récupérer les lignes article à facturer
		
		$idorder_rows = array();
		
		foreach( $deliveryNotes as $idbl_delivery ){
			
			$rs =& DBUtil::query( "SELECT idorder_row FROM bl_delivery_row WHERE idbl_delivery = '$idbl_delivery'" );
	
			while( !$rs->EOF() ){
			
				$idorder_rows[] = $rs->fields( "idorder_row" );
				
				$rs->MoveNext();
				
			}		
		
		}
		
		//si rien à facturer
		
		if( !count( $idorder_rows ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}

		//créer la facture
		
		$idbuyer 	= $order->getValue( "idbuyer" );
		$idcontact 	= $order->getValue( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer

		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			if( in_array( $orderItem->getValue( "idrow" ), $idorder_rows ) )
				$invoice->addOrderItem( $orderItem );
			
		}
		
		//appliquer les frais de port
		
		$charge_vat_rate 	= $invoice->getVATRate() / 100.0;
		$total_charge 		= $total_charge_ht * ( 1 + $charge_vat_rate );
		
		$invoice->setValue( "total_charge_ht", 	$total_charge_ht );
		$invoice->setValue( "total_charge", 	$total_charge );
		$invoice->setValue( "charge_vat", 		$total_charge - $total_charge_ht );
		$invoice->setValue( "charge_vat_rate", 	$charge_vat_rate );
		
		//appliquer les pourcentages de remise, d'acompte et de remise pour paiement comptant
		
		$articlesET = 0.0;
		$it = $invoice->getItems()->iterator();
		while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
			
		$total_discount_rate = $order->getValue( "total_discount" );
		$total_discount_amount = $articlesET * $total_discount_rate / 100.0;
		$invoice->setValue( "total_discount_amount" , round( $total_discount_amount, 2 ) );
		$invoice->setValue( "total_discount" , $total_discount_rate );
		
		$billing_rate = $order->getValue( "billing_rate" );
		$ht = $articlesET - $invoice->get( "total_discount_amount" );
		$total_charge_ht = $invoice->getValue( "total_charge_ht" );
		$billing_amount = 0.0 + $billing_rate * ( $ht + $total_charge_ht ) / 100.0;
		$invoice->setValue( "billing_amount" , $billing_amount );
		
		$invoice->setValue( "down_payment_type" , $order->getValue( "down_payment_type" ) );
		$invoice->setValue( "down_payment_value" , $order->getValue( "down_payment_value" ) );

		//propriétés de la facture héritées de la commande

		$invoice->setValue( "iderp", 				$order->getValue( "iderp" ) );
		$invoice->setValue( "idbuyer_erp", 			$order->getValue( "idbuyer_erp" ) );
		$invoice->setValue( "idbilling_adress_erp", $order->getValue( "idbilling_adress_erp" ) );
		$invoice->setValue( "idcurrency", 			$order->getValue( "idcurrency" ) );
		$invoice->setValue( "idcommercant", 		$order->getValue( "idcommercant" ) );
		$invoice->setValue( "idpayment", 			$order->getValue( "idpayment" ) );
		$invoice->setValue( "idpayment_delay", 		$order->getValue( "idpayment_delay" ) );
		$invoice->setValue( "send_by", 				$order->getValue( "send_by" ) );
		$invoice->setValue( "n_order", 				$order->getValue( "n_order" ) );
		$invoice->setValue( "remote_creation_date", $order->getValue( "remote_creation_date" ) );
		$invoice->setValue( "exportable", 			$order->getValue( "exportable" ) );
		$invoice->setValue( "device", 				$order->getValue( "device" ) );
		$invoice->setValue( "factor", 				$order->getValue( "factor" ) );
		$invoice->setValue( "svg_mail", 			$order->getValue( "svg_mail" ) );
		$invoice->setValue( "svg_fax", 				$order->getValue( "svg_fax" ) );
		$invoice->setValue( "pro_forma", 			$order->getValue( "pro_forma" ) );
		$invoice->setValue( "global_delay", 		$order->getValue( "global_delay" ) );
		$invoice->setValue( "deliv_payment", 		Invoice::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->getValue( "idpayment_delay" ) ) );
		
		if( $order->getValue( "cash_payment" ) ){
			
			$invoice->setValue( "status", Invoice::$STATUS_PAID );
			$invoice->setValue( "balance_amount", $invoice->getValue( "total_amount_ht" ) );
			$invoice->setValue( "balance_date", $order->getValue( "balance_date" ) );

		}
		else{
			
			$invoice->setValue( "status", Invoice::$STATUS_INVOICED );
			$invoice->setValue( "balance_amount", 0.0 );
			$invoice->setValue( "balance_date", "0000-00-00" );
			
		}
		
		$invoice->setValue( "charge_auto", 0 );
		$invoice->setValue( "total_charge_auto", 0 );
		
		
		//adresses de livraison et de facturation
		
		$idbilling_adress 	= $order->getValue( "idbilling_adress" );
		$iddelivery 		= $order->getValue( "iddelivery" );
		$idbuyer 			= $order->getValue( "idbuyer" );
		
		if( $idbilling_adress )
				$invoice->setUseInvoiceAddress( new InvoiceAddress( $idbilling_adress ) );
		else 	$invoice->setUseInvoiceAddress( $order->getCustomer()->getAddress() );
		
		if( $iddelivery )
				$invoice->setUseForwardingAddress( new ForwardingAddress( $iddelivery ) );
		else 	$invoice->setUseForwardingAddress( $order->getCustomer()->getAddress() );

		//escompte
		
		$invoice->save();
		$isPartialInvoice = Invoice::isPartialInvoice( $invoice->getId() );
		
		if( $order->getValue( "rebate_value" ) > 0.0 ){
			
			switch( $order->getValue( "rebate_type" ) ){
				
				case "rate" :
				
					$invoice->setValue( "rebate_type", 	"rate" ); 
	        		$invoice->setValue( "rebate_value", $order->getValue( "rebate_value" ) );
	        
					break;
					
				case "amount" :
				
					if( $isPartialInvoice ){
	
						$rebate_rate 	= $order->getValue( "rebate_value" ) * 100.0 / $order->getTotalATI();
						$rebate_value 	= $invoice->getTotalATI() * $rebate_rate / 100.0;
						
						$invoice->setValue( "rebate_type", "amount" );
						$invoice->setValue( "rebate_value", $rebate_value );
					
					}
					else{
						
						$invoice->setValue( "rebate_type", "amount" ); 
	        			$invoice->setValue( "rebate_value", $order->getValue( "rebate_value" ) );
	        		
					}
					
					break;			
				
			}

		}
		
		//assurance crédit
		
		$invoice->insure( $order->getValue( "insurance" ) == 1 );
		
		//sauvegarde
		
		$invoice->save();
		
		//mettre à jour le numéro de facture dans le BL concerné
		
		$idbilling_buyer = $invoice->getId();
		
		foreach( $deliveryNotes as $idbl_delivery )
			DBUtil::query( "UPDATE bl_delivery SET idbilling_buyer = '$idbilling_buyer' WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
		
		//paiement direct
		
		if( $order->getValue( "cash_payment" ) )
			RegulationsBuyer::createPayment( $invoice );
			
		return $invoice;
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un avoir depuis une facture
	 * @param Invoice $invoice la facture à partir de laquelle on souhaite créer l'avoir
	 * @return Credit le nouvel avoir
	 */
	public static function createCreditFromInvoice( Invoice &$invoice ){
	
		$idbuyer 	= $invoice->getValue( "idbuyer" );
		$idcontact 	= $invoice->getValue( "idcontact" );
		
		//créer un avoir vierge
		
		$credit = self::createCredit( $invoice->getId(), $idbuyer, $idcontact );
		
		//lignes article
		
		$it = $invoice->getItems()->iterator();
		while( $it->hasNext() )
			$credit->addInvoiceItem( $it->next() );
		
		//TVA
		
		$invoice_total_amount_ht 	= $invoice->getTotalET();
		$invoice_total_amount 		= $invoice->getTotalATI();
		$vat_rate 					= $invoice_total_amount_ht > 0.0 ? ( $invoice_total_amount / $invoice_total_amount_ht - 1 ) * 100.0 : 0.0;
		
		$credit->setValue( "vat_rate", round( $vat_rate, 2 ) );

		//objet du navoir
		
		list( $date, $time ) = explode( " ", $invoice->getValue( "DateHeure" ) );
		list( $year, $month, $day ) = explode( "-", $date );
									
		$credit->setValue( "object", "ANNULATION TOTALE DE N/ FACTURE " . $credit->getValue( "idbilling_buyer" ) . " DU $day/$month/$year" );
		
		$credit->setValue( "credited_charges", $invoice->getValue( "total_charge_ht" ) ); //port recrédité
		$credit->setValue( "total_charge_ht", 0.0 ); //port refacturé
		
		//remise sur facture
		//@todo : écarts de plusieurs cents possibles car le type de remise à conserver ( montant ou % ) est inconnu
		if( $invoice->getValue( "total_discount" ) > 0.0 ){
			
			$it = $credit->getItems()->iterator();
			while( $it->hasNext() ){
			
				$creditItem =& $it->next();
				$creditItem->setValue( "discount_rate", $invoice->getValue( "total_discount" ) );
				
				$discount_price = $creditItem->getValue( "unit_price" ) * ( 1 - $creditItem->getValue( "discount_rate" ) / 100.0 );
				$creditItem->setValue( "discount_price", round( $discount_price, 2 ) );
			
			}

		}

		/* escompte sur facture */
		
		if( $invoice->getValue( "rebate_value" ) > 0.0 ){
			
			$rebate_rate = $invoice->getValue( "rebate_type" ) == "rate" ? $invoice->getValue( "rebate_value" ) : $invoice->getValue( "rebate_value" ) * 100.0 / $invoice->getTotalATI();

			$it = $credit->getItems()->iterator();
			while( $it->hasNext() ){
			
				$creditItem =& $it->next();
				
				$discount_price = $creditItem->getValue( "discount_price" ) * ( 1 - $rebate_rate / 100.0 );
				$discount_rate 	= $discount_price * 100.0 / $creditItem->getValue( "unit_price" );

				$creditItem->setValue( "discount_rate", $discount_rate );
				$creditItem->setValue( "discount_price", $discount_price );
			
			}
			
		}
		
		//sauvegarde
		
		$credit->save();
		
		return $credit;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un avoir depuis un litige. L'avoir contiendra uniquement la ligne article litigieuse de la facture
	 * @param Litigation $litigation le litige à partir de laquelle on souhaite créer l'avoir
	 * @return Credit le nouvel avoir
	 */
	public static function createLitigationCredit( Litigation $litigation ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		$invoice = new Invoice( $litigation->getInvoiceItem()->getOwner()->get( "idbilling_buyer" ) );

		//créer un avoir vierge
		
		$credit = self::createCredit( $invoice->getId(), $invoice->getValue( "idbuyer" ), $invoice->getValue( "idcontact" ) );
		
		$credit->setValue( "idlitigation", $litigation->getIdLitigation() );
		
		//ligne article

		$credit->addInvoiceItem( $litigation->getInvoiceItem() );

		//TVA
		
		$invoice_total_amount_ht 	= $invoice->getTotalET();
		$invoice_total_amount 		= $invoice->getNetBill();
		$vat_rate 					= ( $invoice_total_amount / $invoice_total_amount_ht - 1 ) * 100.0;
		
		$credit->setValue( "vat_rate", round( $vat_rate, 2 ) );

		//objet du navoir
		
		list( $date, $time ) = explode( " ", $invoice->getValue( "DateHeure" ) );
		list( $year, $month, $day ) = explode( "-", $date );
						
		$credit->setValue( "object", "ANNULATION PARTIELLE DE N/ FACTURE " . $credit->getValue( "idbilling_buyer" ) . " DU $day/$month/$year" );
		
		$credit->setValue( "credited_charges", $invoice->getValue( "total_charge_ht" ) ); //port recrédité
		$credit->setValue( "total_charge_ht", 0.0 ); //port refacturé
		
		//remise sur facture
		/* @todo : écarts de plusieurs cents possibles car le type de remise à conserver ( montant ou % ) est inconnu
		if( $invoice->getValue( "total_discount" ) > 0.0 ){
			
			$it = $credit->getItems()->iterator();
			while( $it->hasNext() ){
			
				$creditItem =& $it->next();
				$creditItem->setValue( "discount_rate", $invoice->getValue( "total_discount" ) );
				
				$discount_price = $creditItem->getValue( "unit_price" ) * ( 1 - $creditItem->getValue( "discount_rate" ) / 100.0 );
				$creditItem->setValue( "discount_price", round( $discount_price, 2 ) );
			
			}
		
		}
		*/
		
		//sauvegarde
		
		$credit->save();
		
		return $credit;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande fournisseur de réapprovisionnement
	 * @static
	 * @access private
	 * @return SupplierOrder la nouvelle commande
	 */
	  if ( INDEX_STRATEGY ){
	public static function createSupplierOrder( $idsupplier, $idbuyer, $idcontact = 0 ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		//récupérer un nouveau numéro de commande disponible
		
		$rs1 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier_row`" ); //bof...
		
		$newPrimaryKeyValue 		= $rs1->fields( "idorder_supplier" );
		$newItemsPrimaryKeyValue	= $rs2->fields( "idorder_supplier" );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;
				
	 	$idorder_supplier = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de commande fournisseur
	 	
	 	$query = "
		INSERT INTO `order_supplier` ( 
			idorder_supplier, 
			idsupplier,
			idorder,
			iduser,
			idbuyer,
			idcontact
		) VALUES ( 
			'$idorder_supplier', 
			'$idsupplier',
			NULL,
			'" . User::getInstance()->getId() . "',
			'$idbuyer',
			'$idcontact'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande fournisseur", E_USER_ERROR );
			exit();
			
		}
			
		//créer la commande
		
		$supplierOrder = new SupplierOrder( $idorder_supplier );

		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
	 	$supplierOrder->setValue( "DateHeure", 	date( "Y-m-d H:i:s" ) );
		$supplierOrder->setValue( "status",		SupplierOrder::$STATUS_STANDBY );
		
		//Commentaire par défaut
		
		$comment =	"Merci de nous communiquer notre commande et le délai prévisionnel.";
		$comment .= "\nDès que la marchandise sera prête, nous vous communiquerons le bon de livraison sur demande.";
		$comment .= "\nMerci d'avance pour votre collaboration.";
		
		$supplierOrder->setValue( "comment", $comment );
		
	 	$supplierOrder->save();

	 	return $supplierOrder;
	 	
	}
	}else{
		public static function createSupplierOrder( $idsupplier, $idbuyer, $idcontact = 0 ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		//récupérer un nouveau numéro de commande disponible
			$table = 'order_supplier';
		$idorder_supplier = TradeFactory::Indexations($table);
	//	$rs1 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier`" );
	//	$rs2 =& DBUtil::query( "SELECT MAX( `idorder_supplier` ) + 1 AS idorder_supplier FROM `order_supplier_row`" ); //bof...
		
	//	$newPrimaryKeyValue 		= $rs1->fields( "idorder_supplier" );
	//	$newItemsPrimaryKeyValue	= $rs2->fields( "idorder_supplier" );
		
	//	if( $newPrimaryKeyValue == null )
			//$newPrimaryKeyValue = 1;
		
		if( $idorder_supplier == null )
			$idorder_supplier = 1;
				
	 	//$idorder_supplier = max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );

	 	//réserver le numero de commande fournisseur
	 	
	 	$query = "
		INSERT INTO `order_supplier` ( 
			idorder_supplier, 
			idsupplier,
			idorder,
			iduser,
			idbuyer,
			idcontact
		) VALUES ( 
			'$idorder_supplier', 
			'$idsupplier',
			NULL,
			'" . User::getInstance()->getId() . "',
			'$idbuyer',
			'$idcontact'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande fournisseur", E_USER_ERROR );
			exit();
			
		}
			
		//créer la commande
		
		$supplierOrder = new SupplierOrder( $idorder_supplier );

		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
	 	$supplierOrder->setValue( "DateHeure", 	date( "Y-m-d H:i:s" ) );
		$supplierOrder->setValue( "status",		SupplierOrder::$STATUS_STANDBY );
		
		//Commentaire par défaut
		
		$comment =	"Merci de nous communiquer notre commande et le délai prévisionnel.";
		$comment .= "\nDès que la marchandise sera prête, nous vous communiquerons le bon de livraison sur demande.";
		$comment .= "\nMerci d'avance pour votre collaboration.";
		
		$supplierOrder->setValue( "comment", $comment );
		
	 	$supplierOrder->save();

	 	return $supplierOrder;
	 	
	}
	
	
	}
	//----------------------------------------------------------------------------
	/**
	 * Créé une commande de chèque-cadeau
	 * @todo adapter le logiciel pour ce genre de service
	 * @static
	 * @access public
	 * @param GiftToken $giftToken le chèque-cadeau
	 * @return Order la nouvelle commande
	 */
	public static function createOrderFromGiftToken( GiftToken &$giftToken, $idbuyer, $idcontact = 0 ){
	
		include_once( dirname( __FILE__ ) . "/Basket.php" );
		include_once( dirname( __FILE__ ) . "/GiftToken.php" );
		
		assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		$type = DBUtil::getDBValue( "type", "gift_token_model", "idmodel", $giftToken->getValue( "idtoken_model" ) );
		
	//	assert( "intval( \"$idbuyer\" ) > 0" );
		assert( "strcmp( \"$type\", \"" . GiftToken::$TYPE_AMOUNT . "\" ) === 0" );

		$amount = round( DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $giftToken->getValue( "idtoken_model" ) ), 2 );

		$order = self::createOrder( $idbuyer, $idcontact );
				
		$order->setSalesman( Basket::getInstance()->getSalesman() );

		include_once( dirname( __FILE__ ) . "/Payment.php" );
		
		$order->setValue( "idcommercant" , 			0 ); //@todo : deprecated
		$order->setValue( "idcurrency" , 			DBUtil::getParameter( "idcurrency" ) );
		$order->setValue( "total_weight", 			0.0 ); //@todo : deprecated
		$order->setValue( "idpayment", 				Payment::$PAYMENT_MOD_DEBIT_CARD );
		$order->setValue( "status", 				Order::$STATUS_PAID );
		$order->setValue( "comment", 				"" );
		$order->setValue( "total_amount_wo_device", $amount ); //@todo : deprecated
		$order->setValue( "status_report", 			1 );
		$order->setValue( "exportable", 			1 );
		$order->setValue( "device" , 				1.0 );
		$order->setValue( "deliv_payment" , 		date( "Y-m-d H:i:s" ) );
		$order->setValue( "down_payment_type" ,		"amount" );
		$order->setValue( "down_payment_value" , 	0.0 );
		$order->setValue( "billing_rate" , 			0.0 );
		$order->setValue( "billing_amount" , 		0.0 );
		$order->setValue( "total_discount" , 		0.0 );
		$order->setValue( "total_discount_amount", 	0.0 );
		$order->setValue( "idpayment_delay", 		1 ); //@todo sécuriser
		$order->setValue( "paid", 					0 );
		$order->setValue( "cash_payment", 			0 );
		$order->setValue( "balance_date", 			"0000-00-00" );
		$order->setValue( "factor" , 				0 );
		$order->setValue( "total_charge_auto" , 	0 );
		$order->setValue( "idorder_rate_buyer" , 	0 );
		$order->setValue( "charge_vat_rate", 		0.0 );
		$order->setValue( "charge_vat", 			0.0 );
		$order->setValue( "total_charge", 			0.0 );
		$order->setValue( "total_charge_ht", 		0.0 );
		
		$order->setUseForwardingAddress( $order->getCustomer()->getAddress() );
		$order->setUseInvoiceAddress( $order->getCustomer()->getAddress() );
		
		//sauvegarde
		
		$order->save(); //bidouille
		
		//ajout du chèque-cadeau de façon brutale et non sécurisée :

		$fields = array(
		
			"idorder"			=> $order->getId(),
			"idrow"				=> 1,
			"quantity"			=> 1,
			"external_quantity" => 0,
			"unit_price" 		=> $amount,
			"discount_price" 	=> $amount,
			"basic_price"       => $amount,
			"idsession" 		=> date( "Y-m-d H:i:s" ),
			"unit" 				=> DBUtil::getDefaultValue( "unit", "detail" ),
			"lot"				=> DBUtil::getDefaultValue( "unit", "detail" ),
			"designation" 		=> "Chèque-cadeau",
			"summary"			=> "Chèque-cadeau",
			"delivdelay"        => 0,
			"usepdf" 			=> 0,
			"useimg"			=> 0,
			"lastupdate" 		=> date( "Y-m-d H:i:s" ),
			"username" 			=> User::getInstance()->get( "login" )
			
		);
		
		foreach( $fields as $key => $value )
			$fields[ $key ] = DBUtil::quote( $value );
			
		$query = "
		INSERT INTO `order_row` ( `" . implode( "`,`", array_keys( $fields ) ) . "` )
		VALUES( " . implode( ",", array_values( $fields ) ) . " )";
		
		$rs =& DBUtil::query( $query );
		
		$order = new Order( $order->getId() ); //bidouille
		
		//assurance crédit
		
		$order->insure();
		
		$order->save(); //bidouille
		
		return $order;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture pour un chèque cadeau
	 * @todo adapter le logiciel pour ce genre de service
	 * @static
	 * @param Order $order la commande à facturer
	 * @return void
	 */
	public static function createInvoiceFromGiftTokenOrder( Order &$order ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$idorder = $order->getId();
		
		//si rien à facturer
		
		if( !count( $order->getItemCount() ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}
		
		//créer la facture
		
		$idbuyer 	= $order->getValue( "idbuyer" );
		$idcontact 	= $order->getValue( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer
		
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();

			$invoice->addOrderItem( $orderItem );
				
		}

		$invoice->setValue( "total_charge", 			0.0 );
		$invoice->setValue( "total_charge_ht", 			0.0 );
		$invoice->setValue( "charge_vat", 				0.0 );
		$invoice->setValue( "charge_vat_rate", 			0.0 );
		$invoice->setValue( "total_discount" , 			0.0 );
		$invoice->setValue( "total_discount_amount", 	0.0 );
		$invoice->setValue( "billing_rate" , 			0.0 );
		$invoice->setValue( "billing_amount" , 			0.0 );
		$invoice->setValue( "down_payment_type" , 		"amount" );
		$invoice->setValue( "down_payment_value" , 		0.0 );
		$invoice->setValue( "charge_auto", 				0 );
		$invoice->setValue( "total_charge_auto", 		0 );
		
		//propriétés de la facture héritées de la commande

		$invoice->setValue( "iderp", 				$order->getValue( "iderp" ) );
		$invoice->setValue( "idbuyer_erp", 			$order->getValue( "idbuyer_erp" ) );
		$invoice->setValue( "idbilling_adress_erp", $order->getValue( "idbilling_adress_erp" ) );
		$invoice->setValue( "idcurrency", 			$order->getValue( "idcurrency" ) );
		$invoice->setValue( "idcommercant", 		$order->getValue( "idcommercant" ) );
		$invoice->setValue( "idpayment", 			$order->getValue( "idpayment" ) );
		$invoice->setValue( "idpayment_delay", 		$order->getValue( "idpayment_delay" ) );
		$invoice->setValue( "send_by", 				$order->getValue( "send_by" ) );
		$invoice->setValue( "n_order", 				$order->getValue( "n_order" ) );
		$invoice->setValue( "exportable", 			$order->getValue( "exportable" ) );
		$invoice->setValue( "device", 				$order->getValue( "device" ) );
		$invoice->setValue( "factor", 				$order->getValue( "factor" ) );
		$invoice->setValue( "svg_mail", 			$order->getValue( "svg_mail" ) );
		$invoice->setValue( "svg_fax", 				$order->getValue( "svg_fax" ) );
		$invoice->setValue( "pro_forma", 			$order->getValue( "pro_forma" ) );
		$invoice->setValue( "global_delay", 		$order->getValue( "global_delay" ) );
		$invoice->setValue( "deliv_payment", 		Invoice::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->getValue( "idpayment_delay" ) ) );
		$invoice->setValue( "status", 				Invoice::$STATUS_PAID );
		$invoice->setValue( "balance_amount", 		$order->getNetBill() );
		$invoice->setValue( "balance_date", 		$order->getValue( "balance_date" ) );
		$invoice->setValue( "rebate_type", 			$order->getValue( "rebate_type" ) );
		$invoice->setValue( "rebate_value", 		$order->getValue( "rebate_value" ) );

		//adresses de livraison et de facturation
		
		$invoice->setUseInvoiceAddress( $order->getCustomer()->getAddress() );
		$invoice->setUseForwardingAddress( $order->getCustomer()->getAddress() );

		//assurance crédit
		
		$invoice->insure( $order->getValue( "insurance" ) == 1 );
		
		//sauvegarde
		
		$invoice->save();

		//paiement direct
		
		RegulationsBuyer::createPayment( $invoice );
		
		return $invoice;
	
	}
	
	//----------------------------------------------------------------------------
 if ( INDEX_STRATEGY ){
	
	function createMyRegulation($iddocument,$typeDoc,$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,$fromDP=0){
	
		// ------- Mise à jour de l'id gestion des Index  #1161
		//$maxNumRegul = DBUtil::query("SELECT (MAX(idregulations_buyer) + 1) as newId FROM regulations_buyer")->fields('newId');
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		$table = 'regulations_buyer';
		$maxNumRegul = TradeFactory::Indexations($table);
		
		
				
		$insertReg=			
		"INSERT INTO regulations_buyer (
			idregulations_buyer,
			idbuyer,
			iduser,
			amount,
			idbank,
			piece_number,
			comment,
			idpayment,
			payment_date,
			accept,
			lastupdate,
			username,
			from_down_payment
		) VALUES (
			'$maxNumRegul',
			'$idbuyer',
			'" . User::getInstance()->getId() . "',
			$amount,
			$idbank,
			'".addslashes($piece)."',
			'".addslashes($comment)."',
			$idpayment,
			'$payment_date',
			1,
			NOW(),
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			$fromDP
		)";
	
		DButil::query($insertReg);
		
		$setline = "";
		
		//maj devis ou commande
		if($fromDP){
			$setline = "down_payment_idregulation = '$maxNumRegul', 
						down_payment_idbank = $idbank, 
						down_payment_comment = '".addslashes($comment)."', 
						down_payment_piece = '".addslashes($piece)."', 
						down_payment_idpayment = '$idpayment',
						down_payment_date = '$payment_date'";
		}else{
			$setline = "cash_payment_idregulation= '$maxNumRegul',
						cash_payment_idbank = $idbank, 
						payment_comment = '".addslashes($comment)."', 
						cash_payment_piece = '".addslashes($piece)."', 
						payment_idpayment = '$idpayment',
						balance_date = '$payment_date'";
		}
		$query = "UPDATE `$typeDoc` SET $setline where id$typeDoc = $iddocument";	
		DButil::query($query);
		
		// si c'est une commande on doit aussi transferer au devis
		if($typeDoc=='order'){
			$rsidestimate = DBUtil::query("SELECT idestimate FROM `order` WHERE idorder=$iddocument LIMIT 1");
			
			if($rsidestimate->RecordCount() && $rsidestimate->fields('idestimate')!=0 && $rsidestimate->fields('idestimate')!='' ){
				$idestimateassocie = $rsidestimate->fields('idestimate');
				$query = "UPDATE `estimate` SET $setline where idestimate = $idestimateassocie";
				DButil::query($query);
			}
		}
		
		// de même si le devis a une commande ce qui a mon avis est rare et stupide de permettre de passer en commande
		if($typeDoc=='estimate'){
			$rsido = DBUtil::query("SELECT idorder FROM `order` WHERE idestimate=$iddocument LIMIT 1");
			
			if($rsido->RecordCount() && $rsido->fields('idorder')!='' && $rsidestimate->fields('idorder')!=0 ){
				$idorderassocie = $rsido->fields('idorder');
				$query = "UPDATE `order` SET $setline where idorder = $idorderassocie";
				DButil::query($query);
			}
		}
		
		return $maxNumRegul;
	}
	}else{
	
		function createMyRegulation($iddocument,$typeDoc,$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,$fromDP=0){
	
		// ------- Mise à jour de l'id gestion des Index  #1161
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		$table = 'regulations_buyer';
		$maxNumRegul = TradeFactory::Indexations($table);
		//$maxNumRegul = DBUtil::query("SELECT (MAX(idregulations_buyer) + 1) as newId FROM regulations_buyer")->fields('newId');
				
		$insertReg=			
		"INSERT INTO regulations_buyer (
			idregulations_buyer,
			idbuyer,
			iduser,
			amount,
			idbank,
			piece_number,
			comment,
			idpayment,
			payment_date,
			accept,
			lastupdate,
			username,
			from_down_payment
		) VALUES (
			'$maxNumRegul',
			'$idbuyer',
			'" . User::getInstance()->getId() . "',
			$amount,
			$idbank,
			'".addslashes($piece)."',
			'".addslashes($comment)."',
			$idpayment,
			'$payment_date',
			1,
			NOW(),
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			$fromDP
		)";
	
		DButil::query($insertReg);
		
		$setline = "";
		
		//maj devis ou commande
		if($fromDP){
			$setline = "down_payment_idregulation = '$maxNumRegul', 
						down_payment_idbank = $idbank, 
						down_payment_comment = '".addslashes($comment)."', 
						down_payment_piece = '".addslashes($piece)."', 
						down_payment_idpayment = '$idpayment',
						down_payment_date = '$payment_date'";
		}else{
			$setline = "cash_payment_idregulation= '$maxNumRegul',
						cash_payment_idbank = $idbank, 
						payment_comment = '".addslashes($comment)."', 
						cash_payment_piece = '".addslashes($piece)."', 
						payment_idpayment = '$idpayment',
						balance_date = '$payment_date'";
		}
		$query = "UPDATE `$typeDoc` SET $setline where id$typeDoc = $iddocument";	
		DButil::query($query);
		
		// si c'est une commande on doit aussi transferer au devis
		if($typeDoc=='order'){
			$rsidestimate = DBUtil::query("SELECT idestimate FROM `order` WHERE idorder=$iddocument LIMIT 1");
			
			if($rsidestimate->RecordCount() && $rsidestimate->fields('idestimate')!=0 && $rsidestimate->fields('idestimate')!='' ){
				$idestimateassocie = $rsidestimate->fields('idestimate');
				$query = "UPDATE `estimate` SET $setline where idestimate = $idestimateassocie";
				DButil::query($query);
			}
		}
		
		// de même si le devis a une commande ce qui a mon avis est rare et stupide de permettre de passer en commande
		if($typeDoc=='estimate'){
			$rsido = DBUtil::query("SELECT idorder FROM `order` WHERE idestimate=$iddocument LIMIT 1");
			
			if($rsido->RecordCount() && $rsido->fields('idorder')!='' && $rsidestimate->fields('idorder')!=0 ){
				$idorderassocie = $rsido->fields('idorder');
				$query = "UPDATE `order` SET $setline where idorder = $idorderassocie";
				DButil::query($query);
			}
		}
		
		return $maxNumRegul;
	}
	
	}
	
	
	
	
	
	
	
	
	
	
	
}
?>