<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion de l'affichage Html des formulaires (stdform)
 */

include_once('adm_table.php');

class ADMFORM
{
	var $table;
	var $DEBUG = 'none';
	/*
	function ADMFORM(&$tableobj)
	{
		$this->table = &$tableobj;
	}
	*/
	
	/**
	 * Affichage d'un 'input'
	 * @param string $Str_ButtonType type de l'input
	 * @param string $Str_ButtonName Nom de l'input
	 * @param string $Str_ButtonValue Valeur de l'input
	 * @return void
	 */
	public function InputField( $Str_ButtonType, $Str_ButtonName, $Str_ButtonValue)
	{
		echo '<input type="'.$Str_ButtonType.'"  size="8" name="'.$Str_ButtonName.'" value="'.$Str_ButtonValue."\" />\n";
	}
	
	/**
	 * Affichage d'un 'input' avec traduction
	 * @param string $Str_ButtonType type de l'input
	 * @param string $Str_ButtonName Nom de l'input
	 * @param string $Str_ButtonValue Valeur de l'input
	 * @return void
	 */
	public function InputButton( $Str_ButtonType, $Str_ButtonName, $Str_ButtonValue)
	{
		echo '<input type="'.$Str_ButtonType.'"  size="8" name="'.$Str_ButtonName.'" value="'.Dictionnary::translate($Str_ButtonValue)."\" />\n";
	}
	
	/**
	 * Affichage du début du formulaire
	 * @param string $Str_ActionName Action du formulaire
	 * @param string $Str_FormName Nom du formulaire
	 * @return void
	 */
	public function BeginPostForm($Str_ActionName, $Str_FormName='', $Str_Javascript='',$Str_EncType='')
	{
		echo '<form '.$Str_EncType.' action="'.$Str_ActionName.'" method="post" name="'.$Str_FormName."\"$Str_Javascript style=\"margin:0;\">\n";
	}
	
	/**
	 * Affichage de fin de formulaire
	 * @return void
	 */
	public function EndForm()
	{
		echo "</form>\n";
	}
	
	/**
	 * Affichage d'une liste déroulante
	 * @static
	 * @param array $Array_Fields Les paramètres
	 * @param string $Str_Action ???
	 * @param string $Str_Default La valeur sélectionnée
	 * @return bool true quoiqu'il arrive
	 */
	public static function SelectField(array &$Array_Fields, $Str_Action, $Str_Default = null)
	{
		

		$Str_FName = $Array_Fields["fieldname"];
		$Str_OrigF  = $Array_Fields["{$Str_Action}_select_orig_field"];
		$Str_FinalF  = empty($Array_Fields["{$Str_Action}_select_final_field"]) ? $Str_OrigF : $Array_Fields["{$Str_Action}_select_final_field"];
		$Str_Table  = $Array_Fields["{$Str_Action}_select_table"];
		$SQL_select = "SELECT `$Str_OrigF`, `$Str_FinalF` FROM `$Str_Table` ORDER BY `$Str_OrigF`";
		$res = DBUtil::getConnection()->GetAll($SQL_select);
		$SQL_default_exist = "SELECT fieldname FROM desc_field WHERE tablename = '$Str_Table'";
		$exist_res = DBUtil::getConnection()->GetAll($SQL_default_exist);
		$exist = false;
		for($n = 0 ; $n < count($exist_res) ; $n++) {
			if(!empty($exist_res[$n]['default']))
				$exist = true;
		}
		if($exist) {
			$SQL_default = "SELECT `default` FROM `$Str_Table` ORDER BY `$Str_OrigF`";
			$def = DBUtil::getConnection()->GetAll($SQL_default);
			if(count($def))
				$default = true;
			else
				$default = false;
		} else {
			$default = false;
		}

		$n = count($res);
		if ($n>0) // At least one row to display
		{
			echo '<select name="'.$Str_FName.'">'."\n";
			
			if( !isset( $Array_Fields[ "mandatory" ] ) || !$Array_Fields[ "mandatory" ] )
				echo "<option value=\"\">-</option>";
				
			if ( $Str_Action=="search" || !is_null($Str_Default) ) echo '<option value="">'.$Str_Default."</option>\n";
		    for ($i=0; $i<$n; $i++)
		    {
				$Str_KValue = $res[$i][$Str_FinalF]; // the key name value from foreign table
				echo "\t".'<option value="'.$Str_KValue.'"'; 
				if ((isset($Array_Fields['value']) && $Array_Fields['value']==$Str_KValue) || ($default && $def[$i]['default'] == 1)) { echo ' selected="selected"'; }
				//echo '>'.Util::checkEncoding($res[$i][$Str_OrigF])."</option>\n"; 
                              
                // echo '>'.mb_convert_encoding($res[$i][$Str_OrigF], "ISO-8859-1", mb_detect_encoding($res[$i][$Str_OrigF], "UTF-8, ISO-8859-15", false))."</option>\n";
   	               $Str_Option = $res[$i][$Str_OrigF];
                   $Str_Encodage = mb_detect_encoding($res[$i][$Str_OrigF]);
                   
                   switch( $Str_Encodage ){
                     case 'UTF-8':
                       //$Str_Option = mb_convert_encoding($Str_Option,"ISO-8859-1");
                        $Str_Option = $Str_Option;
                       break;
                      case 'ASCII':
                       $Str_Option =utf8_encode($Str_Option);
                       break;
                   }
                   echo '>'.$Str_Option."</option>\n";
             }
			echo "</select>\n";
		}
		else
		{
			echo '<input type="text" name="'.$Str_FName.'" size="'.$Array_Fields['size'].'" class="textInput" />'."\n";
		}
		return true;
	}
	
	/**
	 * Affichage d'un message d'erreur
	 * @param string $Str_ErrContext Le contexte
	 * @param string $Str_ErrMessage Le message
	 * @param mixed $Int_ErrCode Le code d'erreur
	 * @return void
	 */
	public function Error($Str_ErrContext, $Str_ErrMessage, $Int_ErrCode ="")
	{
		echo "<br /><span class=\"mes_error\">";
		echo Dictionnary::translate("fatal_error");
		echo "";
		echo $Str_ErrContext;
		echo "</span>";
		echo "<br /><span class=\"mes_error\">";
		echo $Str_ErrMessage;
		echo "</span>";
		echo "<br />";
		exit($Int_ErrCode);
	}
	
	/**
	 * Récupère toutes les lignes dans une table donnée qui répondent à une condition donnée
	 * @param string $Str_NameTable Le nom de la table
	 * @param string $Str_FieldNames non utilisé
	 * @param string $Str_Condition La clause sql WHERE
	 * @return resource la resource ADODB
	 */
	public function GetTableRowsBy( $Str_NameTable, $Str_FieldNames, $Str_Condition){
		
		//echo "select * from ".$Str_NameTable.' '.$Str_Condition."XXXXXXXXXX";
		return DBUtil::getConnection()->GetRow("select * from ".$Str_NameTable.' '.$Str_Condition);
		
	}
	
	/**
	 * Génére la condition pour la fonction d'effacement
	 * @param array $k les critères encodés
	 * @return string La clause sql WHERE encodée
	 */
	public function CreateDeletePattern( $k )
	{
		$Str_Condition="WHERE ";
		for ($i=0;;$i++){
		//echo "<pre>CreateDeletePattern "; print_r($k); echo "</pre>\n";
			if(!isset($k[$i])) break;
			$v=unserialize(base64_decode($k[$i]));
			reset ($v);//echo "<pre>CreateDeletePattern "; print_r($v); echo "</pre>\n";reset ($v);
			foreach($v as  $ii=>$iii)
			 	{
			 		if($v[$ii]=='') return "ERROR CreateDeletePattern - void value!";
					$Str_Condition.=" $ii = '".$v[$ii]."' AND"; //between keys
				}
			$Str_Condition=substr($Str_Condition,0,strlen($Str_Condition)-3);
			$Str_Condition.="  OR"; // between records
		}
		$Str_Condition=substr($Str_Condition,0,strlen($Str_Condition)-3); //echo "DDDDDDDD  $Str_Condition DDDDDDDD";
	return base64_encode(serialize($Str_Condition));
	}
}//EOC
?>