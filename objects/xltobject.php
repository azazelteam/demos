<?php
 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object généraion XML
 */

class XLTRANSFROM
{
var $handle;
var $filename;



//contructeur
function XLTRANSFROM($filename)
{
$this->filename = $filename;
$this->init($filename.'.xsl');
}

function init($filename)
{
//$filename = "../xml/data.xsl";
if (!$this->handle = fopen($filename, 'wb')) {
         echo "Impossible d'ouvrir le fichier ($filename)";
         return;
    }
    if ( fwrite($this->handle, '<?php xml version="1.0" encoding="ISO-8859-15"?>' ) === FALSE) {
       echo "Impossible d'écrire dans le fichier ($filename)";
       return;
    }    
fwrite($this->handle,"\n".'<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">'."\n");
}

function produce()
{
$this->startdoc();
$this->page();
$this->buyer();
$this->produit();
$this->article();
$this->entete();
$this->hcol();
$this->col();
$this->description();
$this->span();
$this->basetpl();
$this->prix();
$this->fin();
$this->close();
}

function startdoc()
{
fwrite($this->handle,' <xsl:template match="/">
  <html>
   <head>
   <title>Fiche produit</title>
   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta http-equiv="Content-Language" content="fr" />
   <link rel="stylesheet" href="');
   fwrite($this->handle,$this->filename);
   fwrite($this->handle,'.css" />
   </head>
   <body>
	<xsl:apply-templates select="/brochure/page">
	<xsl:sort select="@num" order="ascending"/>
	</xsl:apply-templates>
   </body>
  </html>
 </xsl:template>');
 $this->nl();
}

function page()
{
//    <p>page <xsl:value-of select="@num" /></p>
fwrite($this->handle,' <xsl:template match="page">
   <div class="page">
   <xsl:apply-templates select="/brochure/buyer" />
    <div>
    <xsl:apply-templates select="product" >
    <xsl:sort select="@num" order="ascending"/>
    </xsl:apply-templates>
    </div>
   </div>
 </xsl:template>');
 $this->nl();
}

function buyer()
{
fwrite($this->handle,' <xsl:template match="buyer">
   <div class="header">
   <xsl:element name="img"><xsl:attribute name="src"><xsl:value-of select="buyerlogo" /></xsl:attribute>
   <xsl:attribute name="class">headerlogo</xsl:attribute>
   </xsl:element>
    <xsl:value-of select="title" />
   </div>
   <div class="msgcom">
   <xsl:copy-of select="msgcom" />
   </div>
 </xsl:template>');
 $this->nl();
}

function buyerlogo()
{
fwrite($this->handle,' <xsl:template match="buyer">
   <div class="header">
	<xsl:apply-templates select="buyerlogo" />
	<xsl:apply-templates select="buyercoord" />
   </div>
 </xsl:template>');
 $this->nl();
}

function produit()
{
fwrite($this->handle,' <xsl:template match="product">
 <xsl:choose>
 <xsl:when test="@mask = \'3\'">
    <div class="produit" >
   <table width="100%">
   <tr>
   <td class="vide">
   <table height="100%">
   <tr>  
      <td class="vide">
      <p class="titre_1"><xsl:value-of select="name" /></p>
	<div class="milieu">
      <xsl:apply-templates select="desc" />
      </div>
      </td>
      <td class="imgcellcenter">
      <xsl:variable name="img" select="image"/>
      <img  class="imgcat" src="{$img}" />
    </td></tr>
    </table>  
    </td>
      </tr></table>
     <table  class="detail" style="width:100%">
      <tr>
      <xsl:apply-templates select="entete" />
      </tr>
 <xsl:apply-templates select="article" />   
     </table>
    </div> 
</xsl:when>
<xsl:when test="@mask = \'1\'">
         <div class="produit">
   <table width="100%">
   <tr>
   <td class="vide" >
	<table height="100%">
	<tr>
	<td class="imgcellcenter">
	<xsl:variable name="img" select="image"/>
	<img  class="imgcat" src="{$img}" />
	</td>
	</tr>
    </table>  
    </td>  
      <td  class="vide">
      <p class="titre_1"><xsl:value-of select="name" /></p>
	<div class="milieu">
      <xsl:apply-templates select="desc" />
      </div>
      </td></tr></table>
     <table  class="detail" style="width:100%">
      <tr>
      <xsl:apply-templates select="entete" />
      </tr>
 <xsl:apply-templates select="article" />   
     </table>
     </div>
</xsl:when>       

<xsl:otherwise>
   <div class="produit">
   <p class="titre_1"><xsl:value-of select="name" /></p>
   <table width="100%"><tr>
   <td class="vide">
   <table height="100%">
   <tr><td class="imgcellcenter">
      <xsl:variable name="img" select="image"/>
      <img  class="imgcat" src="{$img}" />
    </td>  
      <td class="milieu" >
   
      <xsl:apply-templates select="desc" />
      
      </td>
      </tr>
    </table>  
    </td>
      </tr></table>
      <div class="detail">
     <table style="width:100%">
     <xsl:choose>    
'); 
//      <xsl:variable name="wd" select="@width"/>
//      <xsl:variable name="lp" select="@xpos"/>
//      <xsl:variable name="tp" select="@ypos"/>
//    <div class="produit" style="position:fixed; left:{$lp}pt; top:{$tp}pt; width:{$wd}pt;" >
    
fwrite($this->handle,'
       <xsl:when test="@tabletype = \'N\'"> 
      <tr>
      <xsl:apply-templates select="entete" />
      </tr>
 <xsl:apply-templates select="article" />
 </xsl:when>
 <xsl:otherwise>
');

for($i=1;$i<15;$i++) 
fwrite($this->handle,"         
	   <tr>
	<xsl:apply-templates select=\"entete/hcolmn[$i]\" />
	<xsl:apply-templates select=\"article/colmn[$i]\" />
    </tr>
");

 
fwrite($this->handle,'
    </xsl:otherwise>
</xsl:choose>
     </table>
     </div>
    </div> 
  </xsl:otherwise>
  </xsl:choose>
  <br />
 </xsl:template>');
 $this->nl();
}

function hcol()
{
fwrite($this->handle,'
<xsl:template match="hcolmn">
<xsl:if test="@num &lt; 203">
	<th><xsl:value-of select="." /></th>
</xsl:if>
</xsl:template> 

');
}

function article()
{ //<td><xsl:value-of select="." /></td>
fwrite($this->handle,' <xsl:template match="article">
   <tr>
    <xsl:apply-templates select="colmn" />
   </tr>
 </xsl:template> ');
 $this->nl();
}

function articleold()
{ 
fwrite($this->handle,' <xsl:template match="article">
   <tr>
     <xsl:for-each select="colmn">
 <xsl:sort select="@num" order="ascending"/>
    <td><xsl:value-of select="." /></td>
  </xsl:for-each>
   </tr>
 </xsl:template> ');
 $this->nl();
}

function entete()
{
fwrite($this->handle,' <xsl:template match="entete">
 <xsl:for-each select="hcolmn">
 <xsl:sort select="@num" order="ascending"/>
    <xsl:if test="@num &lt; 203">
    <th><xsl:value-of select="." /></th>
    </xsl:if>
  </xsl:for-each>
 </xsl:template>');
 $this->nl();
}

function col()
{
fwrite($this->handle," <xsl:template match=\"colmn\">
<xsl:if test=\"@num &lt; 203\">
    <xsl:choose>
     <xsl:when test=\"@type = 'price'\">
     <xsl:element name=\"td\"><xsl:attribute name=\"style\">text-align:right; padding-right:1em</xsl:attribute><xsl:value-of select=\".\" /></xsl:element>
     </xsl:when>
     <xsl:when test=\"@type = 'promo'\">
     <xsl:element name=\"td\"><xsl:attribute name=\"style\">text-align:right; font-weight:bold; padding-right:1em</xsl:attribute><xsl:value-of select=\".\" /></xsl:element>
     </xsl:when>
     <xsl:when test=\"@type = 'ref'\"><xsl:element name=\"td\"><xsl:attribute name=\"style\">font-weight:bold </xsl:attribute><xsl:value-of select=\".\" /></xsl:element></xsl:when>
     <xsl:otherwise><xsl:element name=\"td\"><xsl:copy-of select=\".\" /></xsl:element></xsl:otherwise>
    </xsl:choose>
    </xsl:if>
  </xsl:template>");
 $this->nl();
}

function description()
{
fwrite($this->handle,' <xsl:template match="desc">
 <xsl:for-each select="line">
    <p class="milieu" ><xsl:copy-of select="."/></p>
  </xsl:for-each>
 </xsl:template>');
 $this->nl();
}

function span()
{
fwrite($this->handle,
' <xsl:template match="span">
    <xsl:element name="span"><xsl:attribute name="style"><xsl:value-of select="@style" /></xsl:attribute>
          <xsl:value-of select="." />
   </xsl:element>
</xsl:template>');
 $this->nl();
}


function basetpl()
{
fwrite($this->handle,
' <xsl:template match="x">
          <xsl:value-of select="." />
	  <xsl:text>some texte </xsl:text>
</xsl:template>');
 $this->nl();
}

function prix()
{
fwrite($this->handle," <xsl:template match=\"price\">
    <xsl:choose>
     <xsl:when test=\"@currency = 'USD'\">$ </xsl:when>
     <xsl:when test=\"@currency = 'GBP'\">£ </xsl:when>
     <xsl:when test=\"@currency = 'EURO'\">&#x20AC; </xsl:when>
     <xsl:when test=\"@currency = 'YEN'\">Y </xsl:when>
     <xsl:otherwise>? </xsl:otherwise>
    </xsl:choose>
   <xsl:value-of select=\"format-number(., '#,##0.00')\"/>
 </xsl:template>");
 $this->nl();
}

function fin() { fwrite($this->handle,'</xsl:stylesheet>');  $this->nl();}

function nl() { fwrite($this->handle,"\n"); }
//close the file
function close() { fclose($this->handle); }
}
?>