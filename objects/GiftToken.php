<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object cadeau (non utilisé)
 */

include_once( dirname( __FILE__ ) . "/Session.php" );


class GiftToken{
	
	//----------------------------------------------------------------------------
	
	/**
	 * @var string $TYPE_AMOUNT
	 * @static
	 * @access public
	 */
	public static $TYPE_AMOUNT 	= "amount";
	/**
	 * @var string $TYPE_RATE
	 * @static
	 * @access public
	 */
	public static $TYPE_RATE 	= "rate";
	
	private $idtoken;
	private $idmodel;
	private $serial;
	private $fields;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * Créé le chèque cadeau dont le numéro est donné
	 * Pour créer un nouveau chèque cadeau, utilisez GiftToken::create( int $idmodel )
	 * @param int $idmodel le numéro du modèle de mchèque cadeau
	 */
	
	function __construct( $idtoken ){
		
		$this->idtoken = intval( $idtoken );
		
		$this->idmodel	= null;
		$this->serial	= null;
		$this->fields	= array();
		
		//données de la base
		$this->load();
				
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les informations du chèque cadeau
	 * @return void
	 */
	
	private function load(){
		
		$this->fields = array();
		
		$query = "
		SELECT * FROM `gift_token`
		WHERE `idtoken` = '" . $this->idtoken . "'";
		
		$rs = DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée la chèque cadeau suivant le modèle $idmodel pour un client donné
	 * @param int $idmodel le modèle de chèque à utiliser
	 * @param int $idbuyer le numéro de client pour qui créer le chèque-cadeau
	 * @return GiftToken l'objet créé
	 */
	
	public static function &create( $idmodel, $idbuyer, $idcontact = 0 ){
		
		if( !GiftToken::isAvailableModel( $idmodel ) )
			die( "Ce type de chèque cadeau n'est pas disponible actuellement." );
		
		$serial = GiftToken::generateUniqueSerial();
		
		$period_validity = GiftToken::getPeriodValidity( $idmodel );
		$expiration_date = date( "Y-m-d H:i:s", mktime( date( "H" ), date( "i" ), date( "s" ), date( "m" ), date( "d" ) + $period_validity, date( "Y" ) ) );
		
		//Création du chèque cadeau
		$query = "
		INSERT INTO `gift_token` ( 
			`idtoken_model`, 
			`idbuyer`, 
			`idcontact`,
			`issue_date`,
			`expiration_date`, 
			`serial` 
		) VALUES ( 
			'$idmodel',
			'$idbuyer', 
			'$idcontact', 
			NOW(),
			'$expiration_date', 
			'$serial' 
		)";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de créer le nouveau chèque cadeau" );
		
		//Récupération de l'identifiant du chèque cadeau créé
		$idtoken = DBUtil::getConnection()->Insert_ID();
		
		//Instanciation du chèque cadeau
		$token = new GiftToken( $idtoken );
		
		return $token;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Vérifie si le chèque cadeau $serial est utilisable
	 * @param int $serial la numéro du chèque à utiliser
	 * @return boolean la disponibilité du chèque cadeau
	 */
	
	public static function isAvailableGiftToken( $serial ){
		
		$query = "
		SELECT * 
		FROM `gift_token` 
		WHERE `serial` = '$serial' 
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		//le chèque n'existe pas
		
		if( !$rs->RecordCount() )
			return false;
			
		//le chèque-cadeau est un chèque-cadeau de parrainage et l'utilisateur n'est pas idetifié ou n'est pas le bénéficiaire du chèque
		
		if( !Session::getInstance()->getCustomer() )
			return false;
			
		if( $rs->fields( "idbuyer" ) && $rs->fields( "idbuyer" ) != Session::getInstance()->getCustomer()->getId() )
			return false;
	
		//le chèque cadeau est un chèque de parrainage, mais le filleul n'a pas encore payé sa première commande
		
		if( $rs->fields( "idbuyer" ) ){
			
			$query = "
			SELECT COUNT(*) AS allowGiftTokenUsage 
			FROM `order`
			WHERE idbuyer = '" . $rs->fields( "idbuyer" ) . "'
			AND idcontact = '" . $rs->fields( "idcontact" ) . "'
			AND paid = 1";
			
			$rs2 =& DBUtil::query( $query );
			
			if( !$rs2->fields( "allowGiftTokenUsage" ) )
				return false;
				
		}
		
		//le chèque a déjà été utilisé
		
		if( $rs->fields( "idorder" ) || $rs->fields( "use_date" ) != "0000-00-00 00:00:00" )
			return false;
		
		//la durée de validité du chèque a expiré
		
		if( strcmp( $rs->fields( "expiration_date" ), date( "Y-m-d H:i:s" ) ) < 0 )
			return false;

		return true;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Vérifie si ce type de modèle est disponible à la date actuelle
	 * @return boolean la disponibilité du modèle
	 */
	
	private static function isAvailableModel( $idmodel ){
		
		$idsponsorship_model = DBUtil::getParameterAdmin( "sponsoring_gift_token_model" );
		
		$start_date = DBUtil::getDBValue( "start_date", "gift_token_model", "idmodel", $idmodel, false );
		$end_date = DBUtil::getDBValue( "end_date", "gift_token_model", "idmodel", $idmodel, false );
		$now = date( "Y-m-d H:i:s" );
		
		if( ( gettype( $start_date ) != "string" || gettype( $end_date ) != "string" || strcmp( $now, $start_date ) < 0 || strcmp( $now, $end_date ) > 0 ) && $idmodel != $idsponsorship_model )
			return false;
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la liste des chèques cadeau du client disponibles
	 * @param int $idcustomer l'identifiant du client
	 * @return int array la liste des chèques cadeau disponibles
	 */
	
	public function getAvailableTokens( $idcustomer ){
		
		//Liste des chèques cadeau du client
		$query = "SELECT `idtoken` FROM `gift_token` 
		WHERE `idbuyer` = '$idcustomer'
		AND `issue_date` < NOW() 
		AND `expiration_date` > NOW()
		AND `idorder` = 0 
		AND `use_date` = '0000-00-00 00:00:00'";
		
		$rs = DBUtil::query( $query );
		
		$available_tokens = array();
		
		while( !$rs->EOF ){
			
			if( isAvailableGiftToken( $rs->fields( "idtoken" ) ) )
				$available_tokens[] = $rs->fields( "idtoken" );
			
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée un identifiant unique pour le chèque
	 * @return string l'identifiant créé (format WWW-XXX-YYY-ZZZZ)
	 */
	
	private static function generateUniqueSerial(){
		
		$serial = uniqid();
		
		$formated_serial = substr( $serial, 0, 3 ) . "-" . substr( $serial, 3, 3 ) . "-" . substr( $serial, 6, 3 ) . "-" . substr( $serial, 9, 4 );
		
		return $formated_serial;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Récupère la durée de validité du modèle de chèque cadeau
	 * @param int $idmodel l'identifiant du modèle de chèque cadeau
	 * @return int durée de validité en jours
	 */
	
	private static function getPeriodValidity( $idmodel ){
		
		$query = "SELECT `period_validity` FROM `gift_token_model` WHERE `idmodel` = $idmodel";
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la durée de validité du chèque cadeau" );
		
		return $rs->fields( "period_validity" );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet
	 * @return void
	 */
	 
	 public function update(){
	
		$updatableFields = array();
		
		foreach( $this->fields as $fieldname => $value ){
			
			if( $fieldname != "idtoken" )
				$updatableFields[] = $fieldname;
			
		}
		
		$query = "UPDATE `gift_token`
		SET ";
		
		$i = 0;
		
		foreach( $updatableFields as $fieldname ){
			
			if( $i )
				$query .= ", ";
			
			$query .= "`$fieldname` = '" . Util::html_escape( $this->fields[ $fieldname ] ) . "'";
			
			$i++;
			
		}
		
		$query.=" WHERE `idtoken` = '" . $this->getId() . "'";
		
		DBUtil::query( $query );
		
	 }
	 
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un attribut donné du chèque cadeau
	 * @param string $key l'attribut du chèque cadeau dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	
	public function get( $key ){
		
		return $this->fields[ $key ];
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Affecte une valeur à une propriété du chèque cadeau donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 	 
	public function set( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne l'identifiant du chèque cadeau
	 * @return int
	 */
	
	public function getId(){
		
		return $this->idtoken;
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>