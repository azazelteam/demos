<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object popup Liste de catégories
 */

define ( "DEBUG_CATEGORYPOPUP", 0 );

class CategoryPopup{
	
	//-------------------------------------------------------------------------------------------

	private $root;
	private $html;
	private $lang;
	private $categoryCount;
	private $idcategory;
	private $selectedTreePath;
	private $allowParentSelection;
	
	//-------------------------------------------------------------------------------------------
	
	/*
	 * Constructeur
	 * @param int idCategory facultatif, permet de construire le menu à partir de la catégorie spécifiée
	 * @param bool $allowParentSelection autorise ou non la sélection des noeuds parents
	 */
	 
	function CategoryPopup( $idcategory = 0, $allowParentSelection = true ){
		
		global $GLOBAL_START_URL;
		
		$this->lang = $lang;
		$this->idcategory = $idcategory;
		$this->selectedTreePath = array();
	
		$this->setSelectedTreePath();
	
		$this->allowParentSelection = $allowParentSelection;
		
		$this->root = array( 
			"id" 			=> 0, 
			"name" 			=> "Accueil",
			"children" 		=> array(),
			"productCount" => $this->getProductCount( 0 )
		);
		
		$this->categoryCount = 1;
		
		$this->html = "
		<style type=\"text/css\">
			
			ul, li, li a{

				margin:0px;
				padding:0px;
				font-weight:normal;
				font-size:11px;

			}

			ul{
		
				margin-left:15px;

			}

			.selectedCategory{

				font-weight:bold;
				color:#FF7000;

			}
			
			.categoryLink{

				font-weight:normal;
				color:#444444;

			}

			.searchResult a{
				
				font-weight:bold;
				color:#FF0000;

			}

		</style>
		<script type=\"text/javascript\" language=\"javascript\">
		<!--

			function explodeCategoryNode( treeNodeID, icon ){
	
				var categoryNode = document.getElementById( treeNodeID );
				
				if( icon != null ){

					if( icon.src == '$GLOBAL_START_URL/images/back_office/content/cat_open.jpg' )
						icon.src = '$GLOBAL_START_URL/images/back_office/content/cat_close.jpg';
					else if( icon.src == '$GLOBAL_START_URL/images/back_office/content/cat_close.jpg' )
						icon.src = '$GLOBAL_START_URL/images/back_office/content/cat_open.jpg';
					else icon.src = '';

				}

				var children = categoryNode.childNodes;
				
				var i = 0;
				while( i < children.length ){

					var child = children[ i ];
				
					if( child.tagName == 'UL' ){

						var display = child.style.display == 'none' ? 'block' : 'none';
						child.style.display = display;

						var items = child.childNodes;
						var j = 0;
						while( j < items.length ){

							if( items[ j ].tagName == 'LI' )
								items[ j ].style.display = display;

							j++;

						}


					}

					i++;

				}

			}

			function collapseAll(){

				var root = document.getElementById( 'CategoryTreeRoot' );
				expand( root, false );

			}

			function expandAll(){

				var root = document.getElementById( 'CategoryTreeRoot' );
				expand( root, true );

			}

			function expand( node, expanded ){

				var items = node.getElementsByTagName( 'LI' );
				var i = 0;
				while( i < items.length ){

					item = items[ i ];
					
					if( expanded )
							item.style.display = 'block';
						else item.style.display = 'none';

					i++;

				}

				var items = node.getElementsByTagName( 'UL' );
				i = 0;
				while( i < items.length ){

					item = items[ i ];
					
					if( expanded )
							item.style.display = 'block';
						else item.style.display = 'none';

					i++;

				}

				var icons = node.getElementsByTagName( 'IMG' );
				i = 0;
				while( i < icons.length ){

					icon = icons[ i ];
					
					if( expanded && icon.src == '$GLOBAL_START_URL/images/back_office/content/cat_close.jpg' )
						icon.src = '$GLOBAL_START_URL/images/back_office/content/cat_open.jpg';
					else if( !expanded && icon.src == '$GLOBAL_START_URL/images/back_office/content/cat_open.jpg' )
						icon.src = '$GLOBAL_START_URL/images/back_office/content/cat_close.jpg';

					i++;

				}

			}

			function searchCategory(){

				var searchString = new String( document.getElementById( 'searchString' ).value ).toLowerCase();
				var root = document.getElementById( 'CategoryTreeRoot' );
				
				collapseAll();

				var items = root.getElementsByTagName( 'LI' );
				var i = 0;
				while( i < items.length ){

					var item = items[ i ];
					
					var name = item.getAttribute( 'name' );
					var match = name.toLowerCase().indexOf( searchString ) != -1;

					if( match ){

						item.className = 'searchResult';
						setMatchingNode( item );

					}

					i++;

				}
				
			}

			function setMatchingNode( node ){
	
				var parent = node.parentNode;
			
				expand( parent, true );
					
				if( parent.parentNode.tagName.toLowerCase() == 'li' )
					setMatchingNode( parent.parentNode );

			}

		// -->
		</script>";
		
		//$this->setCategoryChildren( $this->root[ "children" ][ 0 ] );
		$this->setCategoryChildren( $this->root );
		
		$display = $this->idcategory != "" ? "block" : "none";
		
		if( $this->idcategory != "" )
			$icon = "$GLOBAL_START_URL/images/back_office/content/cat_open.jpg";
		else $icon = "$GLOBAL_START_URL/images/back_office/content/cat_close.jpg";

		$this->html .= "
		<p style=\"margin-bottom:10px; margin-left:-8px\">
			<a href=\"#\" class=\"categoryLink\" onclick=\"expandAll(); return false;\"><img src=\"$GLOBAL_START_URL/images/expand_all.jpg\" alt=\"Ouvrir tout\" style=\"border-style:none;\" /> Ouvrir tout</a>
			<a href=\"#\" class=\"categoryLink\" onclick=\"collapseAll(); return false;\"><img src=\"$GLOBAL_START_URL/images/collapse_all.jpg\" alt=\"Fermer tout\" style=\"border-style:none;\" /> Fermer tout</a>
			<a href=\"#\" class=\"categoryLink\" onclick=\"window.print(); return false;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/print.gif\" alt=\"Imprimer cette page\" style=\"border-style:none;\" /> Imprimer</a>
		</p>
		<p style=\"margin-bottom:10px; margin-left:-8px\">
			<input type=\"text\" id=\"searchString\" value=\"\" />
			<a href=\"#\" onclick=\"searchCategory(); return false;\"><img src=\"$GLOBAL_START_URL/administration/images/button.php??label=Rechercher&r=100&g=100&b=100\" alt=\"Rechercher\" /></a>
		</p>
		<ul style=\"display:block; margin-left:-10px\">
			<li id=\"CategoryTreeRoot\" name=\"root\" style=\"display:block;\">
				<a href=\"#\" class=\"categoryLink\" onclick=\"explodeCategoryNode( 'CategoryTreeRoot', this.childNodes[ 0 ] ); return false;\"><img style=\"border-style:none;\" src=\"$icon\" id=\"CategoryTreeRootImage\" alt=\"\" /></a>
				Accueil";

		$this->createNode( $this->root );
		
		$this->html .= "
			</li>
		</ul>";
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Créer les neouds fils d'un noeud donné
	 * @param array $parent le noeud parent
	 * @return void
	 */
	private function setCategoryChildren( array &$parent ){

		$query = "SELECT cat.idcategory AS id, cat.name_{$this->lang} AS name";
		$query .= " FROM category cat, category_link catlink";
		$query .= " WHERE catlink.idcategorychild = cat.idcategory";
		$query .= " AND catlink.idcategoryparent = " . $parent[ "id" ];
		$query .= " ORDER BY catlink.displayorder";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		$this->categoryCount += $rs->RecordCount();
		
		if( DEBUG_CATEGORYPOPUP ){
			
			echo "<p><u>setCategoryChildren()</u></p>";
			echo "<p>@param parent: [" . $parent[ "id" ] . "]" . $parent[ "name" ] . "</p>";
			echo "<p>query : " . $query . "</p>";
			echo "<p>results : " . $rs->RecordCount() . "</p>";
			
			
		}
		
		while( !$rs->EOF() ){
			
			$category  = array( 
				"id" 			=> $rs->fields( "id" ), 
				"name" 			=> $rs->fields( "name" ),
				"children" 		=> array(),
				"productCount"	=> $this->getProductCount( $rs->fields( "id" ) )
			);
			
			$pos = array_push( $parent[ "children" ], $category );
			$child = &$parent[ "children" ][ $pos - 1 ];
			
			$this->setCategoryChildren( $child );

			$rs->MoveNext();
		
		}
			
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Fonction récursive pour la création de l'arborescence des catégories
	 * @param array $parent un noeud donné
	 * @return void
	 */
	private function createNode( &$parent ){
		
		global $GLOBAL_START_URL;
		
		$idparent = $parent[ "id" ];
		
		if( $parent[ "name" ] == "ROOT" || in_array( $idparent, $this->selectedTreePath ) )
				$display = "block";
			else $display = "none";
			
		$this->html .= "
		<ul id=\"categoryNode_$idparent\" class=\"CategoryList\" style=\"display:$display;\">";

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){
			
			$child = &$parent[ "children" ][ $i ];
			$idchild = $child[ "id" ];
			$childName = $child[ "name" ];

			if( !count( $child[ "children" ] ) )
				$icon = "$GLOBAL_START_URL/images/back_office/content/cat_nochild.jpg";
			else if( in_array( $idchild, $this->selectedTreePath ) )
				$icon = "$GLOBAL_START_URL/administration/images/cat_open.jpg";
			else $icon = "$GLOBAL_START_URL/images/back_office/content/cat_nochild.jpg";

			$this->html .= "
			<li id=\"CategoryTreeNode_$idchild\" name=\"" . htmlentities( $child[ "name" ] ) ."\" class=\"CategoryListItem\" style=\"display:$display;\">";
			
			if( count( $child[ "children" ] ) )
				$this->html .= "
				<a href=\"#\" class=\"categoryLink\" onclick=\"explodeCategoryNode( 'CategoryTreeNode_$idchild', this.childNodes[ 0 ] ); return false;\"><img style=\"border-style:none;\" src=\"$GLOBAL_START_URL/images/back_office/content/cat_close.jpg\" id=\"CategoryNodeImage_$idchild\" alt=\"\" /></a>";
			else $this->html .= "
			<img style=\"border-style:none;\" src=\"$GLOBAL_START_URL/images/back_office/content/cat_nochild.jpg\" id=\"CategoryNodeImage_$idchild\" alt=\"\" />";

			$cssClass = $child[ "id" ] == $this->idcategory ? " class=\"selectedCategory\"" : " class=\"categoryLink\"";
			
			$allowSelection = $this->allowParentSelection || count( $child[ "children" ] ) == 0;
			
			if( !empty( $idchild ) && $allowSelection )
				$this->html .= "
					<a{$cssClass} href=\"#\" onclick=\"selectCategory(" . $child[ "id" ] . "); return false;\">" . htmlentities( $child[ "name" ] ) . "</a>";
			else $this->html .= htmlentities( $child[ "name" ] );
			
			
			if( count( $child[ "children" ] ) )
				$this->createNode( $child );
						
			$this->html .= "
			</li>";

			$i++;
			
		}

		$this->html .= "
		</ul>";
		
	}

	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nombre de produits dans une catégorie donnée
	 * @param int $idcategory le numéro de la catégorie
	 * @return int
	 */
	private function getProductCount( $idcategory ){
	
		$query = "SELECT COUNT(*) AS productCount FROM product WHERE idcategory = $idcategory";
		$rs = DBUtil::getConnection()->Execute( $query );
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le nombre de produits pour la catégorie $idcategorie" );
			
		return $rs->fields( "productCount" );
			
	}
	
	//-------------------------------------------------------------------------------------------

	/*
	 * Retourne le code HTML de l'arbre
	 * @return string
	 */ 
	public function getHTML(){
			
		return $this->html;
		
	}

	//-------------------------------------------------------------------------------------------
	
	/**
	 * Définit le chemin de l'arbre sélectionné
	 * @return void
	 */
	private function setSelectedTreePath(){
		
		$treePath = array();
		$treePath[ 0 ] = $this->idcategory;
		
		$idchild = $this->idcategory;
		$hasParent = true;

		while( $hasParent ){
			
			$query = "SELECT idcategoryparent AS idparent FROM category_link WHERE idcategorychild = $idchild";
			$rs = DBUtil::getConnection()->Execute( $query );
			
			if( $rs === false )
				die( "Impossible de récupérer la catégorie parente" );
				
			if( !$rs->RecordCount()  )
				$hasParent = false;
			else{
				
				$idparent = $rs->fields( "idparent" );
				$treePath[] = $idparent;
				$idchild = $idparent;
				
				if( !$idparent )
					$hasParent = false;
				
			}
		
		}
	
		$this->selectedTreePath = array_reverse( $treePath );
		
	}
	
	//-------------------------------------------------------------------------------------------
	
}
?>