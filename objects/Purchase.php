<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ....?
 */

include_once( dirname( __FILE__ ) . "/Trade.php" );


interface Purchase extends Trade{
	
	//----------------------------------------------------------------------------
	/**
	 * @return Supplier
	 */
	public function &getSupplier();
	
	//----------------------------------------------------------------------------
	/**
	 * @return Salesman
	 */
	public function &getSalesman();
	
	//----------------------------------------------------------------------------
	
}

?>