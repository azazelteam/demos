<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object favoris / bookmark
 */
 

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
include_once( dirname( __FILE__ ) . "/Dictionnary.php" );


class Bookmark {
	
	//---------------------------------------------------------------------------------

	private $db;

	var $root;

	//---------------------------------------------------------------------------------
	
	public function __construct($name){
		
		$this->root = array(
			"id"			=> 0,
			"idparent"		=> 0,
			"name"	 		=> $name,
			"url"			=> "",
			"type"			=> "",
			"children" 		=> array()
		);
		
		$this->db = DBUtil::getConnection();
		
		$this->iduser = User::getInstance()->getId();
		
		if( $this->iduser ){
			
			$this->getBookmarkItems( $this->root );	
		}
		
	}
			
	//---------------------------------------------------------------------------------

	/*
	 * Récupération des données de la base dans la structure créée précedemment
	 */
	 	 
	private function getBookmarkItems( &$parent ){
		
		$idparent = $parent[ "id" ];

		$query = "	SELECT *
					FROM bookmark b
					WHERE b.parent = " . $idparent . "
					GROUP BY b.idbookmark
					ORDER BY b.display_order ASC ";

		$rs = DBUtil::query( $query );

		if( $rs === false )
			die( "Impossible de récupérer les éléments des favoris" );

		while( !$rs->EOF() ){

			$idchild		= $rs->fields( "idbookmark" );
			$name			= Dictionnary::translate( $rs->fields( "bookmark_1" ) );
			$url			= $rs->fields( "url" );
			$type			= $rs->fields( "type" );

			$parent[ "children" ][] = array (
				"id"			=> $idchild,
				"idparent"		=> $idparent,
				"name" 			=> $name,
				"url"			=> $url,
				"type"			=> $type,
				"children" 		=> array()
			);

			$rs->MoveNext();

		}

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){

			$child = &$parent[ "children" ][ $i ];
			$this->getBookmarkItems( $child );
			$i++;

		}

	}
	
	//---------------------------------------------------------------------------------
	
	/*
	 * Retourne l'arborescence complète des favoris
	 */

	public function getBookmark(){
		
		return $this->root;
		
	}
	
	//---------------------------------------------------------------------------------
}
	
?>
