<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object implémentation de la livraison
 */
 
 include_once( dirname( __FILE__ ) . "/Address.php" );
 /**
  * @todo problème de conception au niveau des adresses ( adresse client, de facturation, de livraison )
  * 
  * Les données ne devraient pas être dispatchées dans 3 tables différentes ( surtout pour les adresses clients )
  * 	
  * 	=> les adresses client sont utilisées comme des adresses de facturation ou de livraison mais il
  * 		il n'existe aucune contrainte qui garantie que les données d'une adresse client soient les mêmes
  * 		que celles d'une adresse de livraison ou de facturation
  * 	=> une adresse saisie pour la facturation ne peut pas être réutilisée pour la livraison ( et vice et versa ), il faut resaisir l'adresse
  * 	=> on doit pouvoir récupérer ou modifier une propriété de l'adresse, que ce soit l'adresse par défaut ou une adresse de livraison
  * 	=> dans tous les PDF et dans la gestion commerciale, on doit à chaque fois tester s'il y a une 
  * 		adresse de livraison différente ou non pour savoir quelle donnée afficher.
  * 
  * Proposition : 
  * 
  * 	=> toutes les adresses sont stockées dans une même table
  * 	=> une commande possède toujours une adresse de livraison et de facturation ( ibid facture...)
  */

 interface Delivery{
 	
 	//----------------------------------------------------------------------------

	/**
	 * Définit l'adresse de livraison à utiliser
	 * @param Address $forwardingAddress une adresse de livraison
	 * @return void
	 */
	public function setForwardingAddress( Address $forwardingAddress );
	
	/**
	 * Retourne l'adresse de livraison utilisée
	 * @return Address une référence vers l'adresse de livraison si elle existe, sinon NULL
	 */
	public function &getForwardingAddress();
	
	//----------------------------------------------------------------------------
	
 }
 
?>