<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object cookie front-office
*/
 
define( "DEBUG_COOKIE", false );

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/Basket.php" );


class Cookie{

	//-----------------------------------------------------------------------------------------------------------

	/**
	 * @var Cookie $instance
	 * @static
	 * @access private
	 */
	private static $instance = NULL;
	
	/**
	 * nombre de produits consultés dans l'historique
	 * @var int $VIEWED_PRODUCT_COUNT
	 * @static
	 * @access private
	 */
	private static $VIEWED_PRODUCT_COUNT = 25;
	
	
	/**
	 * @var array $encryptedData Les donées à crypter dans le cookie
	 * @static
	 */
	 private static $encryptedData = array(
	 
	 	"sessid",
	 	"idbuyer",
	 	"idcontact",
	 	"login",
	 	"password",
	 	"title",
	 	"lastname",
	 	"firstname",
	 	"email"
	 	
	 );
	 
	/**
	 * @var string $expire Un timestamp UNIX pour la date d'expiration
	 */
	private $expire;
	
	/**
	 * @var string name Le nom du cookie
	 */
	private $name;
	
	/**
	 * @var $path Le chemin sur le serveur sur lequel le cookie sera disponible
	 */
	private $path;
	
	/**
	 * @var array Les données stockées dans le cookie
	 */
	private $data;
	
	/**
	 * @var string version
	 * @static
	 * Permet de déclencher l'expiration du cookie lorsque la structure de celui-ci a été modifiée afin d'éviter les conflits de version
	 */
	private static $VERSION = "5.0";
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * @static
	 * @access public
	 * @return Cookie
	 */
	public static function &getInstance(){
		
		if( self::$instance === NULL )
			self::$instance = new Cookie();
			
		return self::$instance;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @access private
	 */
	private function __construct(){

		$this->data = array(
			
			"sessid" 			=> 0,	
			"idbuyer" 			=> 0,
			"idcontact" 		=> 0,
			"login" 			=> "",
			"password" 			=> "",
			"title" 			=> "",
			"lastname" 			=> "",
			"firstname" 		=> "",
			"email" 			=> "",
			"basket_items" 		=> array(),
			"viewed_products" 	=> array(),
			"php_self" 			=> $_SERVER[ "PHP_SELF" ],
			"lastupdate" 		=> date( "Y-m-d H:i:s" ),
			"version" 			=> self::$VERSION
		
		);
		
		$this->name = $this->getName();
		$this->expire = time() + 60 * 60 * 24 * 30 * 2; //2 months
		$this->path = "/";
	
		if( !$this->cook() )
			$this->destroy();
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Initialisation du gateau
	 * @access private
	 * @return true en cas de succès, sinon false
	 */
	private function cook(){
	
		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:cook()";
			
		//charger les infos du cookie
	
		if( !$this->read() )
			return false;
		
		//décoder les données sensibles
		
		if( !$this->decrypt() )
			return false;
		
		//récupérer l'état des paniers devis/commande
		
		if( $this->data[ "sessid" ] != session_id() ){

			$this->restoreCart();		
			$this->data[ "sessid" ] = session_id();
			
		}
		
		//mémoriser le compte utilisateur

		if( Session::getInstance()->getCustomer() 
			&& ( !$this->data[ "idbuyer" ] || Session::getInstance()->getCustomer()->getId() != $this->data[ "idbuyer" ] ) )
			$this->setCustomerData();

		//login auto
		
		if( !Session::getInstance()->getCustomer() && $this->data[ "idbuyer" ] ){
			
			if( $this->login() )
				header( "location: " . $_SERVER[ "PHP_SELF" ] );
		
		}
		
		//sauvegarder l'état des paniers
		
		$this->backupCart();
		
		//historique produit
			
		if( basename( $_SERVER[ "PHP_SELF" ] ) == "product_one.php" 
			&& isset( $_GET[ "IdProduct" ] ) 
			&& intval( $_GET[ "IdProduct" ] ) 
			&& DBUtil::query( "SELECT 1 FROM product WHERE idproduct = '" . intval( $_GET[ "IdProduct" ] ) . "' LIMIT 1" )->RecordCount() ){
					
			if( in_array( intval( $_GET[ "IdProduct" ] ), $this->data[ "viewed_products" ] ) )
				$this->data[ "viewed_products" ] = array_diff( $this->data[ "viewed_products" ], array( intval( $_GET[ "IdProduct" ] ) ) );
				
			if( count( $this->data[ "viewed_products" ] ) == self::$VIEWED_PRODUCT_COUNT )
				array_pop( $this->data[ "viewed_products" ] );
				
			array_unshift( $this->data[ "viewed_products" ], intval( $_GET[ "IdProduct" ] ) );

		}
		
		//crypter les données sensibles
		
		if( !$this->encrypt() )
			return false;
		
		//sauvegarder le gateau
		
		return $this->write();
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * liste des derniers produits visités
	 * @access public
	 * @return array<int>
	 */
	public function getViewedProducts(){ 
		
		return $this->data[ "viewed_products" ];
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Détruit le cookie
	 * @access public
	 * @return void
	 */
	public function destroy(){

		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:detroy()";
		
		$this->expire = time() - 3600;
		
		$this->data = array();
		
		setcookie( $this->name, "", $this->expire, $this->path );
		
		self::$instance = NULL;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Charge les données depuis le cookie
	 * @access private
	 * @return bool true en cas de succès, sinon false
	 */
	private function read(){

		global $GLOBAL_DB_PASS;
		
		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:read()";
			
		if( !isset( $_COOKIE[ $this->name ] ) )
			return true;

		if( DEBUG_COOKIE )
				echo "\n<br />COOKIE RELOADED";

		//lecture
		
		$cookie = unserialize( base64_decode( $_COOKIE[ $this->name ] ) );

		//incompatibilité de version
		
		if( isset( $cookie[ "version" ] ) && $cookie[ "version" ] != self::$VERSION ){
			
			if( !headers_sent() ){
			
				header ("Cache-Control: no-cache, must-revalidate");
				header ("Pragma: no-cache");
				header("Expires: " . gmdate("D, d M Y 00:00:00") . " GMT" );
    			header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
			
			}

			if( DEBUG_COOKIE )
				echo "\n<br />COOKIE RELOADED";
				
			return false;

		}
		
		//récupération
		
		$this->data[ "sessid" ] 		= $cookie[ "sessid" ];
		$this->data[ "idbuyer" ] 		= $cookie[ "idbuyer" ];
		$this->data[ "idcontact" ]		= $cookie[ "idcontact" ];
		$this->data[ "login" ]			= $cookie[ "login" ];
		$this->data[ "password" ]		= $cookie[ "password" ];
		$this->data[ "title" ]			= $cookie[ "title" ];
		$this->data[ "lastname" ]		= $cookie[ "lastname" ];
		$this->data[ "firstname" ]		= $cookie[ "firstname" ];
		$this->data[ "email" ]			= $cookie[ "email" ];
		$this->data[ "basket_items" ] 	= $cookie[ "basket_items" ];
		
		foreach( $cookie[ "viewed_products" ] as $idproduct ){
			
			/* vérifier si le produit existe encore */
			
			if( DBUtil::getDBValue( "idproduct", "product", "idproduct", $idproduct ) )
				array_push( $this->data[ "viewed_products" ], $idproduct );
		
		}
		
		$this->data[ "version" ]		= self::$VERSION;

		if( DEBUG_COOKIE )
			$this->dump();
			
		return true;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Décrypte les données définies dans sefl::$encryptedData
	 * @access private
	 * @return void
	 */
	private function decrypt(){
		
		global $GLOBAL_DB_PASS;
		
		include_once( dirname( __FILE__ ) . "/Encryptor.php" );
		
		if( !strlen( $GLOBAL_DB_PASS ) )
			return false;
		
		foreach( self::$encryptedData as $data )
			if( strlen( $this->data[ $data ] ) )
				$this->data[ $data ] = Encryptor::decrypt( $this->data[ $data ], $GLOBAL_DB_PASS );

		return true;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Crypte les données définies dans sefl::$encryptedData
	 * @access private
	 * @return bool
	 */
	private function encrypt(){
		
		global $GLOBAL_DB_PASS;
		
		include_once( dirname( __FILE__ ) . "/Encryptor.php" );
		
		if( !strlen( $GLOBAL_DB_PASS ) )
			return false;
		
		foreach( self::$encryptedData as $data )
			if( strlen( $this->data[ $data ] ) )
				$this->data[ $data ] = Encryptor::encrypt( $this->data[ $data ], $GLOBAL_DB_PASS );
		
		return true;
			
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Enregistre les données dans le cookie
	 * @access private
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function write(){

		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:write()";

		return setcookie( $this->name, base64_encode( serialize( $this->data ) ), $this->expire, $this->path );
		
	}

	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Génère un nom de gateau valide
	 * @access private
	 * @return string Un très bon nom de gateau
	 */
	private function getName(){

		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:getName()";
			
		$pattern = "/[^a-zA-Z0-9]+/";
		
		return preg_replace( $pattern, "_", $_SERVER[ "SERVER_NAME" ] );
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Sauvegarde l'état d'avancement des devis et commandes inachevés
	 * @access private
	 * @return void
	 */
	private function backupCart(){
		
		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:backupCart()";
			
		if( isset( $_SESSION[ "basket_items" ] ) && count( $_SESSION[ "basket_items" ] )  ){
	
			$i = 0;
			while( $i < count( $_SESSION[ "basket_items" ] ) ){
			
				//structure anormale de la session ( en cas de plantage lors d'un devis/cde )
				
				if( !isset( $_SESSION[ "basket_items" ][ $i ][ "idarticle" ] )
					|| !isset( $_SESSION[ "basket_items" ][ $i ][ "quantity" ] ) )
					return;
					
				//structure normale
				
				$idarticle = $_SESSION[ "basket_items" ][ $i ][ "idarticle" ];
				$quantity = $_SESSION[ "basket_items" ][ $i ][ "quantity" ];
				
				if( $idarticle && $quantity )
					$this->data[ "basket_items" ][ $i ] = array( "idarticle" => $idarticle,  "quantity" => $quantity );
				
				$i++;
				
			}
			
		}
		else $this->data[ "basket_items" ] = array();

	}

	//-----------------------------------------------------------------------------------------------------------
	/**
	 * login automatique
	 * @access private
	 * @return bool
	 */
	private function login(){
	
		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:login()";
			
		if( !Session::getInstance()->login( $this->data[ "login" ], $this->data[ "password" ] ) )
			return false;
		
		if( DEBUG_COOKIE )
			echo "\n<br />LOGIN SUCCEED";
			
		if( !Session::getInstance()->getCustomer() )
			return false;
			
		$this->setCustomerData();

		return true;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Restaure l'état d'avancement des devis et commandes inachevés
	 * @return void
	 */
	
	private function restoreCart(){
		
		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:restoreCart()";
			
		if( count( $this->data[ "basket_items" ] ) ){
			
			$i = 0;
			while( $i < count( $this->data[ "basket_items" ] ) ){
			
				//structure anormale du cookie
				
				if( !isset( $this->data[ "basket_items" ][ $i ][ "idarticle" ] ) 
					|| !isset( $this->data[ "basket_items" ][ $i ][ "quantity" ] ) )
					return;
				
				//structure normale
					 
				$idarticle = $this->data[ "basket_items" ][ $i ][ "idarticle" ];
				$quantity = $this->data[ "basket_items" ][ $i ][ "quantity" ];
				
				include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
				
				if( DBUtil::getDBValue( "idarticle", "detail", "idarticle", $idarticle ) )
					Basket::getInstance()->addArticle( new CArticle( $idarticle ), $quantity );
				
				$i++;
				
			}
		
		}

	}

	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Mise à jour des données utilisateur de l'objet si l'utilisateur est identifié sur le site
	 * @return void
	 */
	private function setCustomerData(){
	
		if( DEBUG_COOKIE )
			echo "\n<br />Cookie:setCustomerData()";
			
		$query = "
		SELECT login, password
		FROM user_account
		WHERE idbuyer = '" . Session::getInstance()->getCustomer()->getId() . "' 
		AND idcontact = '" . Session::getInstance()->getCustomer()->getContact()->getId() . "' 
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );

		if( $rs === false || !$rs->RecordCount() )
			return;
			
		$this->data[ "idbuyer" ] 	= Session::getInstance()->getCustomer()->getId();
		$this->data[ "idcontact" ] 	= Session::getInstance()->getCustomer()->getContact()->getId();
		$this->data[ "login" ] 		= $rs->fields( "login" );
		$this->data[ "password" ]	= $rs->fields( "password" );
		$this->data[ "title" ]		= Session::getInstance()->getCustomer()->getContact()->get( "title" );
		$this->data[ "lastname" ]	= Session::getInstance()->getCustomer()->getContact()->get( "lastname" );
		$this->data[ "firstname" ]	= Session::getInstance()->getCustomer()->getContact()->get( "firstname" );
		$this->data[ "email" ]		= Session::getInstance()->getCustomer()->getContact()->get( "mail" );

	}

	//-----------------------------------------------------------------------------------------------------------
	/**
	 * Debug
	 * @return void
	 */
	private function dump(){
		
		echo "\n<br />Encypted Cookie";
		
		print_r( $this->data );
		
		echo "\n<br />Decrypted Cookie";
		
		$this->decrypt();
		
		print_r( $this->data );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
}

?>