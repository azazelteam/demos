<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object droits accès commercial client ou affaire
 */
 

include_once( dirname( __FILE__ ) . "/Trade.php" );

interface Sale extends Trade{
	
	//----------------------------------------------------------------------------
	/**
	 * Client de l'affaire
	 * @access public
	 * @return Customer
	 */
	public function &getCustomer();
	/**
	 * Commercial de l'affaire
	 * @access public
	 * @return Salesman
	 */
	public function &getSalesman();
	
	//----------------------------------------------------------------------------
	
}

?>