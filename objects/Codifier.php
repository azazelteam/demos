<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object codificateur pour les n° de catégories, de produits et les références
 */
 
define( "DEBUG_CODIFIER", 0 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );


 
class Codifier{
	
	private static $CODIFICATION_CODIF_1 		= "CODIF_1";
	private static $CODIFICATION_CODIF_2 		= "CODIF_2";
	private static $CODIFICATION_CODIF_3 	= "CODIF_3";
	private static $CODIFICATION_CODIF_4 		= "CODIF_4";
	private static $CODIFICATION_CATEGORY 	= "CATEGORY";
	private static $CODIFICATION_CODIF_5 		= "CODIF_5";
	private static $CODIFICATION_CODIF_6 		= "AXESS";
	private static $CODIFICATION_CODIF_7 		= "MANUEL";
	
	private $database;
	
	//----------------------------------------------------------------------------------
	
	public function __construct( $database = false ){
		
		global $GLOBAL_DB_NAME;
		
		$this->database = $database ? $database : $GLOBAL_DB_NAME;
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Récupère la codification du site actuel pour la création de catégories, produits, références
	 * @return string la codification du site actuel
	 */
	
	public function getCodification(){


		$codification = DBUtil::query( "SELECT paramvalue FROM `{$this->database}`.parameter_admin WHERE idparameter LIKE 'codification'" )->fields( "paramvalue" );
		

		if( empty( $codification ) )
			die( "Impossible de récupérer le paramètre d'administration 'codification'" );
		
		return $codification;
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Informe si le codifieur peut générer une référence automatiquement en fonction de la codification du logiciel
	 * si la génération dynamique de références est possible ou non
	 * @return bool true si la génération dynamique de références est possible, sinon false
	 */
	
	public function isReferenceGeneratorAvailable(){
	
		$codification = $this->getCodification();
		
		switch( $codification ){
		
			case self::$CODIFICATION_CODIF_1 :
			case self::$CODIFICATION_CODIF_2 :
			case self::$CODIFICATION_CODIF_3 :
			case self::$CODIFICATION_CODIF_4 :
			case self::$CODIFICATION_CODIF_5 :
			case self::$CODIFICATION_CODIF_6 :
			case self::$CODIFICATION_CODIF_7 :
				return true;;
			
			case self::$CODIFICATION_CATEGORY :	
			default : 
			
				return false;
		
		}
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Récupère une nouvelle référence dispobible
	 * @param int $idproduct l'identifiant du produit parent
	 * @param int $idcategory l'identifiant de la catégorie parente du produit parent ( optionnel ). Ce paramètre est utilisé pour la création de nouveaux produits à travers le module d'import, lorsque le produit parent n'existe pas encore
	 * @return string une nouvelle référence
	 */
	
	public function getNewReference( $idproduct, $idcategory = false ){
	
		$codification = $this->getCodification();
        if( DEBUG_CODIFIER )
			echo "<br />Codification : $codification";

		switch( $codification ){
		
			case self::$CODIFICATION_CATEGORY :
			case self::$CODIFICATION_CODIF_6 : 
			
			case self::$CODIFICATION_CODIF_1 : 		return $this->getCODIF_1Reference( $idproduct, $idcategory ); 		
			case self::$CODIFICATION_CODIF_2 : 			return $this->getCODIF_2Reference( $idproduct, $idcategory ); 		
			case self::$CODIFICATION_CODIF_3 : 		return $this->getCODIF_3Reference( $idproduct, $idcategory ); 
			case self::$CODIFICATION_CODIF_4 : 			return $this->getCODIF_4Reference( $idproduct, $idcategory );
			case self::$CODIFICATION_CODIF_5 : 		return $this->getCODIF_5Reference( $idproduct );

			case self::$CODIFICATION_CODIF_7 : return $_REQUEST["reference"];
			default :  								return ""; 															
		
		}
		
	}

	//----------------------------------------------------------------------------------
	
	/*
	 * Récupère un nouvel identifiant article disponible
	 * @return int un nouvel identifiant article
	 */
	
	public function getNewArticle(){
	
		/*
		maintenant que les références peuvent être du type "spe_..." ou "dest_..." ou "10.0286.01_1"
		on évitera de codifier l'idarticle afin d'éviter la cata
		*/
		
		return $this->getNewPrimaryKeyValue( "detail", "idarticle" );
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Récupère un nouvel identifiant produit disponible
	 * @param int $idcategory l'identifiant de la catégorie parente
	 * @return int un nouvel identifiant produit
	 */
	
	public function getNewProduct( $idcategory ){
	
		$codification = $this->getCodification();
		
		if( DEBUG_CODIFIER )
			echo "<br />Codification : $codification";
			
		switch( $codification ){
		
			case self::$CODIFICATION_CATEGORY :
			case self::$CODIFICATION_CODIF_1 :
			case self::$CODIFICATION_CODIF_5 :
			case self::$CODIFICATION_CODIF_2 : 	
			case self::$CODIFICATION_CODIF_3 :
			case self::$CODIFICATION_CODIF_4 :	
			
				return $this->getDEFAULTProduct( $idcategory ); 
			
			default : return $this->getNewPrimaryKeyValue( "product", "idproduct" );
		
		}
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Récupère un nouvel identifiant catégorie disponible
	 * @param int $idparent l'identifiant de la catégorie parente
	 * @return int un nouvel identifiant catégorie
	 */
	
	public function getNewCategory( $idparent = 0 ){
	
		$codification = $this->getCodification();
		
		if( DEBUG_CODIFIER )
			echo "<br />Codification : $codification";
			
		switch( $codification ){
		
			case self::$CODIFICATION_CATEGORY :
			case self::$CODIFICATION_CODIF_1 : 
			case self::$CODIFICATION_CODIF_5 :		
			case self::$CODIFICATION_CODIF_2 : 		
			case self::$CODIFICATION_CODIF_3 :
			case self::$CODIFICATION_CODIF_4 :
			
				return $this->getDEFAULTCategory( $idparent );
	
			default : return $this->getNewPrimaryKeyValue( "category", "idcategory" );
		
		}
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Récupère la première valeur disponible d'une clé primaire numérique dans une table
	 * @param string $table le nom de la table
	 * @param string $primaryKey le nom de la clé primaire
	 * @return int la première valeur disponible de primaryKey dans une table ou 1 si la table est vide
	 */
	
	public function getNewPrimaryKeyValue( $table, $primaryKey ){

		$query = "SELECT MAX( `$primaryKey` ) + 1 AS newid FROM `{$this->database}`.`$table`";
		
		if( DEBUG_CODIFIER )
			echo "<br />$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer une nouvelle valeur de '$primaryKey' dans la table '$table'" );
			
		if( $rs->fields( "newid" ) == null )
			return 1;
			
		return $rs->fields( "newid" );
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Crée une nouvelle référence avec la codification CODIF_1
	 * Format des références : ^[0-9]{2}\.[0-9]{4}\.[0-9]{2}$
	 * Codification des références : 'A.B.C' avec :
	 * A = 2 premiers chiffres de l'identifiant de la catégorie
	 * B = 4 derniers chiffres de l'identifiant du produit
	 * C = plus petit nombre à 2 chiffres supérireur à 0 et non utilisé par les références commençant par 'A.B.' ( de 01 à 99 )
	 * @CODIF_1 private
	 * @param int $idproduct l'identifiant du produit parent
	 * @param mixed $idcategory l'identifiant de la catégorie parente du produit parent ( optionnel, false par défaut ). Ce paramètre est utilisé pour la création de nouveaux produits à travers le module d'import, lorsque le produit parent n'existe pas encore
	 * @return string une nouvelle référence
	*/
	
	private function getCODIF_1Reference( $idproduct, $idcategory = false ){

		//idproduct
		
		if( empty($idproduct ) )
			return false;

		if( strlen( strval( $idproduct ) ) < 4 )
			return false;

		$codeB = substr( strval( $idproduct ), strlen( strval( $idproduct ) ) - 4 );
		
		//idcategory
		
		if( $idcategory === false ){
		
			$query = "SELECT idcategory FROM `{$this->database}`.product WHERE idproduct = '$idproduct' LIMIT 1";
			
			if( DEBUG_CODIFIER )
				echo "<br />$query";
				
			$rs =& DBUtil::query( $query );
			
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de récupérer la catégorie du produit '$idproduct'" );
				
			$idcategory = $rs->fields( "idcategory" );
		
		}
		else $idcategory = intval( $idcategory );
		
		if( strlen( strval( $idcategory ) ) < 2 )
			return false;
			
		$codeA = substr( strval( $idcategory ), 0, 2 );
		
		//reference
		
		$query = "SELECT MAX( reference ) AS lastref FROM `{$this->database}`.detail WHERE reference REGEXP( '^$codeA\.$codeB\.[0-9]{2}\$' )";
		
		if( DEBUG_CODIFIER )
			echo "<br />$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la dernière référence utilisée" );
			
		if( $rs->fields( "lastref" ) == null )
			$codeC = "01";
		else{
		
			$lastref = $rs->fields( "lastref" );
			$lastnum = intval( substr( $lastref, 8, 2 ) );
			
			if( $lastnum == 99 ) //limite atteinte
				return false;
				
			$lastnum++;
		
			$codeC = sprintf( "%02d", $lastnum );
		
		}
		
		if( DEBUG_CODIFIER )
			echo "<br />reference : $codeA.$codeB.$codeC";
			
		return "$codeA.$codeB.$codeC";
		
	}
	
//----------------------------------------------------------------------------------
	
	/*
	 * Crée une nouvelle référence avec la codification CODIF_5
	 * Format des références : ^[0-9]{4}\.NI\.[0-9]{2}$
	 * Codification des références : 'ABC' avec :
	 * A = l'identifiant du produit
	 * B = initiales de CODIF_5-Industries : 'NI'
	 * C = plus petit nombre à 2 chiffres supérireur à 0 et non utilisé par les références commençant par 'AB' ( de 01 à 99 )
	 * @CODIF_1 private
	 * @param int $idproduct l'identifiant du produit parent
	 * @return string une nouvelle référence
	*/
	
	private function getCODIF_5Reference( $idproduct ){

		//idproduct
		
		if( empty( $idproduct ) )
			return false;

		$codeA = strval( $idproduct );
		$codeB = "NI";
		
		//reference
		
		$query = "SELECT MAX( reference ) AS lastref FROM `{$this->database}`.detail WHERE reference REGEXP( '^{$codeA}{$codeB}[0-9]{2}\$' )";
		
		if( DEBUG_CODIFIER )
			echo "<br />$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la dernière référence utilisée" );
			
		if( $rs->fields( "lastref" ) == null )
			$codeC = "01";
		else{
		
			$lastref = $rs->fields( "lastref" );
			$lastnum = intval( substr( $lastref, strlen( $codeA ) + strlen( $codeB ), 2 ) );
			
			if( $lastnum == 99 ) //limite atteinte
				return false;
				
			$lastnum++;
		
			$codeC = sprintf( "%02d", $lastnum );
		
		}
		
		if( DEBUG_CODIFIER )
			echo "<br />reference : {$codeA}{$codeB}{$codeC}";
			
		return "{$codeA}{$codeB}{$codeC}";
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Crée une nouvelle référence avec la codification CODIF_4 ( Mode In Motion )
	 * Format des références : ^[0-9]{2}[0-9]{2}\.[0-9]{4}\.[0-9]{2}$
	 * Codification des références : 'AB.C.d' avec :
	 * A = 2 premiers chiffres de l'identifiant de la catégorie de premier niveau ( tout de suite après l'accueil ) ou les 2 premiers chiffres de la catégorie courante s'il s'agit d'une catégorie de premier nveau
	 * B = 2 derniers chiffres de l'identifiant de la catégorie de 2eme niveau si elle existe, sinon '00'
	 * C = 4 derniers chiffres de l'identifiant du produit
	 * D = plus petit nombre à 2 chiffres supérireur à 0 et non utilisé par les références commençant par 'AB.' ( de 01 à 99 )
	 * @CODIF_1 private
	 * @param int $idproduct l'identifiant du produit parent
	 * @return string une nouvelle référence
	*/
	
	private function getCODIF_4Reference( $idproduct, $idcategory = false ){

		//idproduct
		
		if( empty($idproduct ) )
			return false;

		if( strlen( strval( $idproduct ) ) < 4 )
			return false;

		//idcategory
		
		if( $idcategory === false )
			$idcategory = DBUtil::query( "SELECT idcategory FROM `{$this->database}`.product WHERE idproduct = '$idproduct'" )->fields( "idcategory" );
		else $idcategory = intval( $idcategory );
		
		if( $idcategory === false )
			return false;
			
		//A et B

		$done = false;	
		$firstCategory = $idcategory;
		$secondCategory = 0;
		while( !$done ){
			
			$rs =& DBUtil::query( "SELECT idcategoryparent FROM `{$this->database}`.category_link WHERE idcategorychild = '$firstCategory' LIMIT 1" );	
			
			$idparent = $rs->fields( "idcategoryparent" );
			$done = $idparent == 0;
		
			if( !$done ){
			
				$secondCategory = $firstCategory;
				$firstCategory = $idparent;
				
			}
			
		}
		
		if( $firstCategory != 0 && strlen( strval( $firstCategory ) ) < 2 )
			return false;

		if( !$firstCategory ){ //idcategory est une catégorie de 1er niveau
		
			$codeA = substr( strval( $idcategory ), 0, 2 );
			$codeA = sprintf("%02d", $codeA );
			
		}
		else{
			
			$codeA = substr( strval( $firstCategory ), 0, 2 );
			$codeA = sprintf("%02d", $codeA );
		
		}
		
		if( !$secondCategory )
			$codeB = 0;
		else $codeB = substr( strval( $secondCategory ), strlen(strval( $secondCategory ) ) - 2, 2 );
		
		$codeB = sprintf("%02d", $codeB );
		
		//C
		
		$codeC = substr( strval( $idproduct ), strlen( strval( $idproduct ) ) - 4 );
	
		//D
		
		$query = "SELECT MAX( reference ) AS lastref FROM `{$this->database}`.detail WHERE reference REGEXP( '^{$codeA}{$codeB}\.{$codeC}\.[0-9]{2}\$' )";
		
		if( DEBUG_CODIFIER )
			echo "<br />$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la dernière référence utilisée" );
			
		if( $rs->fields( "lastref" ) == null )
			$codeD = "01";
		else{
		
			$lastref = $rs->fields( "lastref" );
			$lastnum = intval( substr( $lastref, 10, 2 ) );
			
			if( $lastnum == 99 ) //limite atteinte
				return false;
				
			$lastnum++;
		
			$codeD = sprintf( "%02d", $lastnum );
		
		}
		
		if( DEBUG_CODIFIER )
			echo "<br />{$codeA}{$codeB}.{$codeC}.{$codeD}";
			
		return "{$codeA}{$codeB}.{$codeC}.{$codeD}";
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Crée une nouvelle référence avec la codification CODIF_2
	 * Format des références : ^[0-9a-zA-Z]*[0-9]{4}\.[0-9]{2}$
	 * Codification des références : 'AB.C' avec :
	 * A = code de la catégorie, peut être vide ( colonne 'code_cat' dans la table 'category' )
	 * B = 4 derniers chiffres de l'identifiant du produit
	 * C = plus petit nombre à 2 chiffres supérireur à 0 et non utilisé par les références commençant par 'A.B.' ( de 01 à 99 )
	 * @access private
	 * @param int $idproduct l'identifiant du produit parent
	 * @param int $idcategory l'identifiant de la catégorie parente du produit parent ( optionnel, false par défaut ). Ce paramètre est utilisé pour la création de nouveaux produits à travers le module d'import, lorsque le produit parent n'existe pas encore
	 * @return string une nouvelle référence
	*/
	
	private function getCODIF_2Reference( $idproduct, $idcategory = false ){

		//idproduct
		
		if( empty($idproduct ) )
			return false;

		if( strlen( strval( $idproduct ) ) < 4 )
			return false;

		$codeB = substr( strval( $idproduct ), strlen( strval( $idproduct ) ) - 4 );
		
		//idcategory
		
		if( $idcategory === false ){
		
			$query = "
			SELECT c.idcategory, c.code_cat
			FROM `{$this->database}`.product p, `{$this->database}`.category c
			WHERE p.idproduct = '$idproduct' 
			AND p.idcategory = c.idcategory
			LIMIT 1";
			
			if( DEBUG_CODIFIER )
				echo "$query";
				
			$rs =& DBUtil::query( $query );
			
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de récupérer la catégorie du produit '$idproduct'" );
				
			$idcategory = $rs->fields( "idcategory" );
			
		}
		else $idcategory = intval( $idcategory );
		
		$code_cat 	= $rs->fields( "code_cat" );
		
		if( strlen( strval( $idcategory ) ) < 2 )
			return false;

		$codeA = $code_cat;
		
		//reference
		
		if( empty( $code_cat ) )
			$pattern ="^$codeB\.[0-9]{2}\$";
		else $pattern ="^{$codeA}{$codeB}\.[0-9]{2}\$";
		
		$query = "SELECT MAX( reference ) AS lastref FROM `{$this->database}`.detail WHERE reference REGEXP( '$pattern' )";
		
		if( DEBUG_CODIFIER )
			echo "$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la dernière référence utilisée" );
			
		if( $rs->fields( "lastref" ) == null )
			$codeC = "01";
		else{
		
			$lastref = $rs->fields( "lastref" );
			$lastnum = intval( substr( $lastref, strlen( $lastref ) - 2, 2 ) );
			
			if( $lastnum == 99 ) //limite atteinte
				return false;
				
			$lastnum++;
			
			$codeC = sprintf( "%02d", $lastnum );
		
		}
		
		if( DEBUG_CODIFIER )
			echo "reference : {$codeA}{$codeB}.$codeC";
			
		return "{$codeA}{$codeB}.$codeC";
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Crée une nouvelle référence avec la codification CODIF_3
	 * Format des références : ^[0-9a-zA-Z]*[0-9]{4}\.[0-9]{2}$
	 * Codification des références : 'AB-C' avec :
	 * A = code de la catégorie, peut être vide ( colonne 'code_cat' dans la table 'category' )
	 * B = 4 derniers chiffres de l'identifiant du produit
	 * C = plus petit nombre à 2 chiffres supérireur à 0 et non utilisé par les références commençant par 'A.B.' ( de 01 à 99 )
	 * @access private
	 * @param int $idproduct l'identifiant du produit parent
	 * @param int $idcategory l'identifiant de la catégorie parente du produit parent ( optionnel, false par défaut ). Ce paramètre est utilisé pour la création de nouveaux produits à travers le module d'import, lorsque le produit parent n'existe pas encore
	 * @return string une nouvelle référence
	*/
	
	private function getCODIF_3Reference( $idproduct, $idcategory = false ){

		//idproduct
		
		if( empty($idproduct ) )
			return false;

		if( strlen( strval( $idproduct ) ) < 4 )
			return false;

		$codeB = substr( strval( $idproduct ), strlen( strval( $idproduct ) ) - 4 );
		
		//idcategory
		
		if( $idcategory === false ){
		
			$query = "
			SELECT c.idcategory, c.code_cat
			FROM `{$this->database}`.product p, `{$this->database}`.category c
			WHERE p.idproduct = '$idproduct' 
			AND p.idcategory = c.idcategory
			LIMIT 1";
			
			if( DEBUG_CODIFIER )
				echo "$query";
				
			$rs =& DBUtil::query( $query );
			
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de récupérer la catégorie du produit '$idproduct'" );
				
			$idcategory = $rs->fields( "idcategory" );
			
		}
		else $idcategory = intval( $idcategory );
		
		$code_cat 	= $rs->fields( "code_cat" );
		
		if( strlen( strval( $idcategory ) ) < 2 )
			return false;

		$codeA = $code_cat;
		
		//reference
		
		if( empty( $code_cat ) )
			$pattern ="^$codeB\.[0-9]{2}\$";
		else $pattern ="^{$codeA}{$codeB}\.[0-9]{2}\$";
		
		$query = "SELECT MAX( reference ) AS lastref FROM `{$this->database}`.detail WHERE reference REGEXP( '$pattern' )";
		
		if( DEBUG_CODIFIER )
			echo "$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la dernière référence utilisée" );
			
		if( $rs->fields( "lastref" ) == null )
			$codeC = "01";
		else{
		
			$lastref = $rs->fields( "lastref" );
			$lastnum = intval( substr( $lastref, strlen( $lastref ) - 2, 2 ) );
			
			if( $lastnum == 99 ) //limite atteinte
				return false;
				
			$lastnum++;
			
			$codeC = sprintf( "%02d", $lastnum );
		
		}
		
		if( DEBUG_CODIFIER )
			echo "reference : {$codeA}{$codeB}-$codeC";
			
		return "{$codeA}{$codeB}-$codeC";
		
	}
	
	//----------------------------------------------------------------------------------
	
	/*
	 * Crée une nouvelle catégorie avec la codification par défaut
	 * Format des catégories : ^[1-9]{1}[0-9]+$
	 * Codification des références : 'AB' avec :
	 * A = identifiant de la catégorie parente
	 * B = plus petit nombre à 2 chiffres supérireur à 0 et non utilisé ( de 01 à 99 )
	 * @access private
	 * @param int $idparent l'identifiant de la catégorie parente
	 * @return int un nouvel identifiant catégorie
	*/
	
	private function getDEFAULTCategory( $idparent ){

		//attention aux catégories déplacées => ne pas chercher dans category_link
		
		$min = $idparent ? $idparent * 100 + 1 : 10;
		$max = $idparent ? $idparent * 100 + 99 : 99;
		
		$query = "
		SELECT MAX( idcategory ) + 1 AS `newid` 
		FROM `{$this->database}`.category
		WHERE idcategory >= '$min' AND idcategory <= '$max'
		LIMIT 1";
		
		if( DEBUG_CODIFIER )
			echo "$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer un nouvel identifiant de catégorie" );
	
		if( $rs->fields( "newid" ) == null ) //il s'agit de la première catégorie fille
			return $idparent ? $idparent * 100 + 1 : 10;
		
		$newid = $rs->fields( "newid" );
		
		if( $newid > $max ) //limite atteinte
			return false;
		
		return $newid;
		
	}
	
	//----------------------------------------------------------------------------------

	/*
	 * Crée un nouvel identifiant produit avec la codification par défaut
	 * Format des produits : ^[1-9]{2}[0-9]{4}$
	 * Codification des références : 'AB' avec :
	 * A = deux premiers chiffres de l'identifiant de la catégorie parente
	 * B = plus petit nombre à 4 chiffres supérireur à 0 et non utilisé ( de 0001 à 9999 )
	 * @access private
	 * @param int $idcategory l'identifiant de la catégorie parente
	 * @return int un nouvel identifiant produit
	*/
	
	private function getDEFAULTProduct( $idcategory ){

		if( empty( $idcategory ) ) //pas de produit orphelin, c'est trop triste
			return false;
			
		if( strlen( strval( $idcategory ) ) < 2 )
			return false;
			
		$codeA = substr( $idcategory, 0, 2 );
		
		$query = "
		SELECT MAX( idproduct ) + 1 AS newid
		FROM `{$this->database}`.product
		WHERE CAST( idproduct AS CHAR ) REGEXP( '^{$codeA}[0-9]{4}\$' )";
		
		if( DEBUG_CODIFIER )
			echo "$query";
			
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer un nouvel identifiant pour le produit" );
			
		if( $rs->fields( "newid" ) == null )
			return $codeA * 10000 + 1;
			
		$idproduct = intval( $rs->fields( "newid" ) );
		
		if( $idproduct > 10000 * ( $codeA + 1 ) - 1 ) //limite atteinte
			return false;
			
		return $idproduct;
	
	}
	
	//----------------------------------------------------------------------------------
	
}

?>