<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export CSS avoirs clients
 */

define( "DEBUG_CSVCreditExport", 0 );
 
class CSVCreditExport
{

	//--------------------------------------------------------------------------------------------------
	
	private $con;
	private $separator;
	private $text_delimiter;
	private $breakline;
	private $metadata;
	private $data;
	private $idcredits;
	
	private $output_type;
	private $mime_type;
	private $filename;
	private $errors;
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param string $filename le nom du fichier de résultat
	 * @param string $output_type le type de fichier de résultat
	 * @param string $separator le séparateur
	 * @param string $text_delimiter l'indicateur de texte
	 * @param string $breakline l'indicateur de saut de ligne
	 */
	function __construct( $filename, $output_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\n" ){
	
		$this->separator 		= $separator;
		$this->text_delimiter 	= "\"";
		$this->breakline 		= $breakline;
		$this->mime_type 		= "";
		$this->filename 		= $filename;
		$this->export_directory = "";
		$this->metadata	 		= array();
		$this->data 			= array();
		$this->idcredits 		= array();
		$this->errors 			= array();
		
		switch( $output_type ){
		
			case "csv" :

				$this->output_type = $output_type;
				$this->mime_type = "text/x-csv";
				
				break;
			
			//case "xls" @todo export xls:
			//case "xml" @todo export xml:
				
			default : die( "Unknown output mode '$output_type'" );
		
		}
		
		if( $this->connect() === false )
			die( "Connection failed" );
			
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le tableau des factures
	 * @param array $idcredits le tableau des factures
	 * @return void
	 */
	public function setCredits( $idcredits ){
	
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>setCredits( $idcredits )</u>" );
			
		if( is_array( $idcredits ) )
			$this->idcredits = $idcredits;

		if( DEBUG_CSVCreditExport )
			print_r( $idcredits );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Ajoute une facture à la fin du tableau
	 * @param int $idcredit l'id de la facture à ajouter
	 * @return void
	 */
	private function addCredit( $idcredit ){
	
		$this->idcredits[] = $idcredit;
			
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Exporte les données
	 * @return int le nombre de lignes exportées
	 */
	public function export(){

		if( $this->setData() === false )
			return 0;	

		$this->disconnect();

		$this->output();
		
		return count( $this->data );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Connexion à la base de données
	 * @return bool true en cas de succès, sinon false
	 */
	private function connect(){
	
		global 	$GLOBAL_DB_HOST,
				$GLOBAL_DB_NAME,
				$GLOBAL_DB_USER,
				$GLOBAL_DB_PASS;
				
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>connect( $GLOBAL_DB_HOST, $GLOBAL_DB_NAME, $GLOBAL_DB_USER )</u>" );

		if( !isset( $GLOBAL_DB_HOST )
			|| !isset( $GLOBAL_DB_NAME )
			|| !isset( $GLOBAL_DB_USER )
			|| !isset( $GLOBAL_DB_PASS ) )
			die( "Connection parameters not found" );
		
		$this->con = @mysqli_connect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS );
		
		if( !is_resource( $this->con ) ){ 
		
			$this->errors[] = "Connection to '$GLOBAL_DB_HOST' failed"; 
			
			return false; 
		
		}

		$ret = @mysqli_select_db( $this->con, $GLOBAL_DB_NAME  );
		
		if( $ret === false ) {
		
			$this->errors[] = "Cannot select database $GLOBAL_DB_NAME : " . @mysqli_error($this->con); 
			return false; 
		
		}

		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Déconnexion de la base de données
	 * @return void
	 */
	private function disconnect(){
	
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>disconnect()</u>" );
			
		if( is_resource( $this->con ) )
			@mysqli_close( $this->con );
		
	}

	//--------------------------------------------------------------------------------------------------

	/**
	 * Exécute une requête SQL
	 * @param string $query la requête à éxécuter
	 * @return mixed la l'objet resource mysql ou false en cas d'échec
	 */
	private function query( $query ){

		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>query()</u><br />$query" );
			
		$result = @mysqli_query( $this->con, $query  );
		
		if( !is_resource( $result ) )
			$this->errors[] = "SQL Error : $query ( " . @mysqli_error($this->con) . " )"; 
		
		return $result;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur
	 * @param string $separator le séparateur
	 * @return void
	 */
	public function setSeparator( $separator ){
	
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>setSeparator( $separator )</u>" );
			
		$this->separator = $separator;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur de texte
	 * @param string $text_delimiter le séparateur
	 * @return void
	 */
	public function setTextDelimiter( $text_delimiter ){
	
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>setTextDelimiter( $text_delimiter )</u>" );
			
		$this->text_delimiter = $text_delimiter;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le caractère de saut de ligne
	 * @param string $breakline le caractère de saut de ligne
	 * @return void
	 */	
	public function setBreakline( $breakline ){
	
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>setBreakline( $breakline )</u>" );
			
		$this->breakline = $breakline;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Formatte les données
	 * @return void
	 */
	protected function setData(){
		
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>setData()</u>" );
		
		$lang = User::getInstance()->getLang();
		
		$this->data = array();
		
		$this->data[ 0 ] = array(
		
			"Numéro de Facture ou Avoir",
			"Numéro du client facturé",
			"Montant à payer TTC",
			"Type de la pièce (facture/avoir)",
			"Date de la pièce",
			"Échéance de la pièce",
			"Mode de règlement",
			"Devise",
			"Numéro de facture du client",
			"Référence complémentaire",
			"Raison sociale du client facturé",
			"Enseigne",
			"Sigle",
			"Adresse 1",
			"Adresse 2",
			"Code Postal",
			"Ville",
			"SIREN ou COFACE",
			"NIC",
			"Nom du responsable",
			"Téléphone",
			"RIB",
			"Compte comptable",
			"LCR Magnétique N/A O/N",
			"Nom de la banque du tiré",
			"Code pays facturé"
			
		);
		
		$i = 0;
		while( $i < count( $this->idcredits ) ){
		
			$idcredit = $this->idcredits[ $i ];
			
			$query = "
			SELECT c.idcredit,
				c.idbuyer,
				SUM( 
					ROUND( cr.discount_price * cr.quantity * ( 1.0 + c.vat_rate / 100.0 ), 2 )
					+ ROUND( c.credited_charges * ( 1.0 + c.vat_rate / 100.0 ), 2 )
					- ROUND( c.total_charge_ht * ( 1.0 + c.vat_rate / 100.0 ), 2 )
				) AS total_amount,
				c.DateHeure,
				15 AS idpayment,
				1 AS idcurrency,
				c.idbilling_buyer,
				b.company,
				b.adress,
				REPLACE( b.adress_2, '\r\n', ' - ' ) AS adress_2,
				b.zipcode,
				b.city,
				b.siren,
				b.siret,
				b.naf,
				b.idstate,
				t.title$lang AS title,
				ct.lastname,
				ct.phonenumber
			FROM credits c, credit_rows cr, buyer b, contact ct, title t
			WHERE c.idcredit = '$idcredit'
			AND cr.idcredit = c.idcredit
			AND c.idbuyer = b.idbuyer
			AND b.idbuyer = ct.idbuyer
			AND ct.idcontact = c.idcontact
			AND ct.title = t.idtitle
			GROUP BY c.idcredit";

			$result = $this->query( $query );
			
			if( mysqli_num_rows( $result ) ){
			
				$credit = @mysqli_fetch_object( $result );
				
				//date facturation
				
				list( $date, $time ) = explode( " ", $credit->DateHeure );
				list( $year, $month, $day ) = explode( "-", $date );
				$year = substr( $year, 2, 2 );
				$DateHeure = "$day/$month/$year";
				
				//date échéance pour un avoir : 45J+FM
				
				//45J
				$deliv_payment = date( "Y-m-d", mktime( 0, 0, 0, $month + 1, $day + 15,  $year ) );
				
				//FM
				list( $year, $month, $day ) = explode( "-", $deliv_payment );
				$lastday = date( "t", mktime( 0, 0, 0, $month, 1, $year ) );
				$deliv_payment = date( "Y-m-d", mktime( 0, 0, 0, $month, $lastday,  $year ) );
				
				//siren
	
				if( empty( $credit->siren ) && !empty( $credit->siret ) && strlen( $credit->siret >= 9 ) )
					$credit->siren = substr( $credit->siret, 0, 9 );
	
				//type de pièce
					
				$row = array(
				
					$credit->idcredit,
					$credit->idbuyer,
					number_format( $credit->total_amount, 2, ",", " " ),
					"avoir",
					$DateHeure,
					$deliv_payment,
					$credit->idpayment,
					$credit->idcurrency,
					$credit->idbilling_buyer,
					"",
					$credit->company,
					"",
					"",
					$credit->adress,
					$credit->adress_2,
					$credit->zipcode,
					$credit->city,
					$credit->siren,
					$credit->naf,
					$credit->title . " " . $credit->lastname,
					$credit->phonenumber,
					"",
					"",
					"",
					"",
					$credit->idstate
					
				);
				
				$this->data[] = $row;
			
			}
			
			$i++;
			
		}
		
		if( DEBUG_CSVCreditExport )
			print_r( $this->data );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Génère le fichier
	 * @return void
	 */
	private function output(){

		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>output()</u>" );
		
		if( headers_sent() ){
		
			$this->errors[] = "Headers already sent";
			$this->abort();
			
			return;
			
		}
		
		header( "Content-Type: {$this->mime_type}" );
		header( "Content-disposition: attachment; filename={$this->filename}.{$this->output_type}" );
			
		$rowCount 		= count( $this->data );
		$columnCount 	= count( $this->data[ 0 ] );
		
		$row = 0;
		while( $row < $rowCount ){

			if( $row )
				echo $this->breakline;
					
			$column = 0;
			while( $column < $columnCount ){
			
				if( $column )
					echo $this->separator;

				$value = $this->data[ $row ][ $column ];
				
				if( !empty( $this->text_delimiter ) )
					$value = str_replace( $this->text_delimiter . $this->text_delimiter, $this->text_delimiter, $value );
				
				$value = str_replace( $this->separator, "", $value );
				
				$str  = $this->text_delimiter . $value . $this->text_delimiter;

				echo $str;

				$column++;
				
			}
			
			$row++;
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Affiche les erreurs
	 */
	private function report(){
			
		echo implode( "<br />", $this->errors );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Arrete l'import et affiche les erreurs
	 * @return void
	 **/
	private function abort(){
		
		if( DEBUG_CSVCreditExport )
			$this->debug( "<u>abort()</u>" );
			
		$this->disconnect();
		
		if( DEBUG_CSVCreditExport )
			$this->report();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les erreurs
	 * @return string les erreurs
	 */
	private function getErrors(){
	
		return $this->errors;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	function debug( $string ){
	
		echo "<br />$string";
		flush();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
}

?>