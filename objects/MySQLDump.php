<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object sauvegardes des données ...?
 */


set_time_limit( 3600 );


class MySQLDump{

	//---------------------------------------------------------------------------
	
	private $db;
	
	private $database;
	private $user;
	private $password;
	private $host;
	private $tables;
	
	private $useCompleteInsert;
	private $useExtendedInsert;
	
	private $outputdir;
	private $outputfile;
	private $gunzipFile;
	
	//---------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	 
	function __construct(){
		
		$this->tables 				= array();
		
		$this->gunzipFile 			= true;
		$this->useCompleteInsert 	= false;
		$this->useExtendedInsert 	= true;
		
	}
	
	//---------------------------------------------------------------------------
	
	/**
	 * Définit le répertoire de sortie pour le fichier de sauvegarde mais ne le crée pas
	 * @param string $outputdir le chemin absolu du répertoire de sortie pour le fichier de sauvegarde
	 * @return void
	 */
	 
	public function setOutputDirectory( $outputdir )			{ $this->outputdir = $outputdir; }
	
	/**
	 * Définit le nom du fichier de sauvegarde
	 * @param string $outputfile le nom du fichier de sauvegarde
	 * @return void
	 */
	 
	public function setOutputFile( $outputfile )			{ $this->outputfile = $outputfile; }
	
	/**
	 * Active/Désactive la compression gunzip
	 * @param bool $gunzipFile l'état d'activation de la compression gzip
	 * @return void
	 */
	 
	public function setGunzipFile( $gunzipFile )				{ $this->gunzipFile = $gunzipFile; }
	
	/**
	 * Active/Désactive les insertions complètes
	 * @param bool $useCompleteInsert l'état d'activation des insertions complètes
	 * @return void
	 */
	
	public function setUseCompleteInsert( $useCompleteInsert )	{ $this->useCompleteInsert = $useCompleteInsert; }
	
	/**
	 * Active/Désactive les nouvelles insertions étendues plus rapides
	 * @param bool $useExtendedInsert l'état d'activation des insertions étendues
	 * @return void
	 */
	 
	public function setUseExtendedInsert( $useExtendedInsert )	{ $this->useExtendedInsert = $useExtendedInsert; }
	
	//---------------------------------------------------------------------------
	
	/**
	 * Connection à la base de données
	 * @access public
	 * @param string $host nom ou adresse serveur auquel se connecter
	 * @param string $user nom d'utilisateur pour la connexion
	 * @param string $password mot de passe pour la connexion
	 * @param string $database nom de la base de données à sauvegarder
	 * @return void
	 */
	 
	public function connect( $host = "", $user = "", $password = "", $database = "" ){
		
		$this->host 	= $host;
		$this->user 	= $user;
		$this->password = $password;
		$this->database = $database;
		
		$this->db =& NewADOConnection( "mysql", "pear" );
		$this->db->Connect( $this->host, $this->user, $this->password, $this->database );
		
		if( !$this->db->IsConnected() )
			die( "\n[ " . date( "d-m-Y H:i:s" ) . " ][ ERROR ] CONNECTION FAILED ( host={$this->host}, database={$this->database}, user={$this->user}" );
			
	}
	
	//---------------------------------------------------------------------------
	
	/**
	 * Ferme la connexion à la base de données
	 * @return void
	 */
	 
	public function disconnect(){
		
		if( $this->db->IsConnected() )
			$this->db->Close();
		
	}
	
	//---------------------------------------------------------------------------
	
	/**
	 * Ajoute une table à sauvegarder
	 * @param string $table le nom de la table à sauvegarder
	 * @return void
	 */
	public function addTable( $table ){
	
		if( !in_array( $table, $this->tables ) )
			$this->tables[] = $table;	
			
	}
	
	//---------------------------------------------------------------------------
	
	/**
	 * Effectue la sauvegarde
	 * @return void
	 */
	 
	public function dump(){
		
		if( empty( $this->outputdir ) || !file_exists( $this->outputdir ) || !is_dir( $this->outputdir ) )
			die( "\n[ " . date( "d-m-Y H:i:s" ) . " ][ ERROR ] OUTPUT DIRECTORY DOES NOT EXIST : {$this->outputdir}" );
			
		$filename = $this->outputfile;
		
		if( $this->gunzipFile )
			$filename .= ".gzip";

		$cmd = "mysqldump -h {$this->host} -u {$this->user} --password={$this->password} {$this->database}";
		
		if( $this->useCompleteInsert )
			$cmd .= " -c";
			
		if( $this->useExtendedInsert )
			$cmd .= " -e";
			
		if( count( $this->tables ) ){
			
			$tables = implode( " ", $this->tables );
			$cmd .= " --tables $tables";	
			
		}
		
		if( $this->gunzipFile )
			$cmd .= " | gzip";

		$cmd .= " > {$this->outputdir}/{$this->outputfile}";
		
		if( $this->gunzipFile )
			$cmd .= ".gz";
		else $cmd .= ".sql";
		
		system( $cmd, $ret = null );
		
		echo "\n[ " . date( "d-m-Y H:i:s" ) . " ][ OK ] database={$this->database}";
		
		echo " output={$this->outputdir}/{$this->outputfile}";
		
		if( $this->gunzipFile )
			@chmod( "{$this->outputdir}/{$this->outputfile}.gz", 0444 );
		else @chmod( "{$this->outputdir}/{$this->outputfile}.sql", 0444 );
		
	}
	
	//---------------------------------------------------------------------------
	
}

?>