<?
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object historique des documents
 */

include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );

class DocumentHistory{
	
	private $htmlMail;
	private $documents;
	private $folder;
	private $type;
	private $extension;
	private $counter;
	private $prefix;
	
	public function __construct( $type , $iddocument , $pref = '' ){
		global $GLOBAL_START_PATH,$GLOBAL_START_URL;
		
		$this->prefix = $pref;
		
		switch( $type ) {
			
			case "relaunch_estimate":
				$this->folder = "$GLOBAL_START_PATH/data/history/estimate_relaunch/$iddocument";
				break;
				
			case "refuse_estimate":
				$this->folder = "$GLOBAL_START_PATH/data/history/estimate_relaunch/$iddocument";
				break;
			
			default :
				die( 'Aucun type renseigné' );
				break;

		
		}

		$this->type = $type;
		$this->counter = 0;
		$this->documents = array();
		$this->extension = ".txt";
		$this->checkFolder();
		
		if( strlen( $this->prefix ) )
			$this->prefix .= '_';
	}
	
	/** @public : Ajouter le mail **/
	public function addMail( $html ){
		if( $html == '' )
			return;
			
		$this->htmlMail = $html;
	}
	
	/** @public : Ajouter un document **/
	public function addDocument( $document , $content ){
		
		$this->documents[ $this->counter ][ 'document' ] = $this->prefix . $document;
		$this->documents[ $this->counter ][ 'content' ] = $content;
		
		$this->counter++;
	}
	
	/** @public : Enregistrer **/
	public function save(){
		$this->storeMail();
		$this->storeDocuments();
	}
	
	/** @private : Regarde si le dossier existe et le créé **/
	private function checkFolder(){

		if( !is_dir( $this->folder ) )
			mkdir( $this->folder , 0755 );
		
		chmod( $this->folder , 0755 );
		
	}
	
	/** @private : Enregistre le mail dans un fichier avec extension souhaitée **/
	private function storeMail(){
		global $GLOBAL_START_PATH,$GLOBAL_START_URL;
		
		$filename = $this->prefix . "mail_" . date( "d-m-Y" ) . $this->extension ;
		
		$fp = fopen( $this->folder . "/$filename" , "w+" );
		
		if( !is_resource( $fp ) )
			exit();
			
		$ret = fwrite( $fp, $this->htmlMail, strlen( $this->htmlMail ) );
	
		fclose( $fp );
						
	}
	
	/** @private : Enregistre les documents joints **/
	private function storeDocuments(){
		global $GLOBAL_START_PATH,$GLOBAL_START_URL;
		
		for( $i = 0 ; $i < count( $this->documents ) ; $i++ ){
			$this->documents[ $i ][ 'document' ];
			
			$fp = fopen( $this->folder . '/' . $this->documents[ $i ][ 'document' ] , "w+" );
			
			if( !is_resource( $fp ) )
				exit();
				
			$ret = fwrite( $fp , $this->documents[ $i ][ 'content' ] , strlen( $this->documents[ $i ][ 'content' ] ) );
		
			fclose( $fp );
					
		}
		
	}
}
?>