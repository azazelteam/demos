<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des erreurs PHP
 */

include_once( dirname( __FILE__ ) . "/../config/config.php" );
include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
//include_once( dirname( __FILE__ ) . "/Session.php" );


abstract class ErrorHandler{
	
	//----------------------------------------------------------------------------
	
	public static $ERROR_HANDLER_PROD 	= "prod";
	public static $ERROR_HANDLER_DEV 	= "dev";
	public static $ERROR_HANDLER_SQL 	= "sql";
	public static $ERROR_HANDLER_INSANE = "insane";
	
	private static $MAX_ERROR_COUNT = 24; 	/* nombre maximum d'erreurs traitées */
	private static $errorCount 		= 0;	/* nombre d'erreurs traitées */
	
	private static $handler 		= null;
	private static $ERRORS 			= array (
		
        E_ERROR           	=> "Erreur",
        E_WARNING         	=> "Alerte",
        E_PARSE           	=> "Erreur d'analyse",
        E_NOTICE          	=> "Note",
        E_CORE_ERROR      	=> "Core Error",
        E_CORE_WARNING    	=> "Core Warning",
        E_COMPILE_ERROR   	=> "Compile Error",
        E_COMPILE_WARNING 	=> "Compile Warning",
        E_USER_ERROR      	=> "Erreur utilisateur",
        E_USER_WARNING    	=> "Alerte utilisateur",
        E_USER_NOTICE     	=> "Note utilisateur",
		E_STRICT 			=> "Runtime notice"

	);
			
	//----------------------------------------------------------------------------
	
	public static function setErrorHandler( $handler ){
		
		switch( $handler ){
			
			case self::$ERROR_HANDLER_PROD :

				ini_set( "display_errors", "Off" );
				error_reporting( 0 );
				DBUtil::getConnection()->debug = false;
				
				break;
				
			case self::$ERROR_HANDLER_DEV :
			
				ini_set( "display_errors", "On" );
				error_reporting( E_ALL );
				DBUtil::getConnection()->debug = false;
				
				break;
				
			case self::$ERROR_HANDLER_SQL :
				
				ini_set( "display_errors", "On" );
				error_reporting( E_ERROR | E_WARNING | E_PARSE | E_NOTICE );
				DBUtil::getConnection()->debug = true;
				
				break;

			case self::$ERROR_HANDLER_INSANE :
				
				ini_set( "display_errors", "On" );
				error_reporting( E_ALL );
				DBUtil::getConnection()->debug = false;
				
				break;
				
			default : trigger_error( "Unknown error handler '$handler'", E_FATAL_ERROR ) && die();
			
		}
		
		self::$handler = $handler;
		set_error_handler( array( "ErrorHandler", "handleError" ) );
		
	}
	
	//----------------------------------------------------------------------------
	
	public static function handleError( $type, $error, $file, $line, $context = array() ){

		if( self::$errorCount > self::$MAX_ERROR_COUNT )
			return;
			
		if( self::$handler == null )
			self::$handler = self::$ERROR_HANDLER_PROD;
		
		switch( self::$handler ){
			
			case self::$ERROR_HANDLER_DEV :
			case self::$ERROR_HANDLER_SQL : 	self::devErrorHandler( $type, $error, $file, $line, $context ); 	break;
			case self::$ERROR_HANDLER_INSANE : 	self::insaneErrorHandler( $type, $error, $file, $line, $context ); 	break;
			case self::$ERROR_HANDLER_PROD : 	
			default :							self::prodErrorHandler( $type, $error, $file, $line, $context ); break;
			
		}
		
		self::$errorCount++;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Fonction de traitement des erreurs PHP (trigger_error)
	 * Mode PRODUCTION
	 * @param integer $type Niveau d'erreur, sous la forme d'un entier
	 * @param string $error Message d'erreur, sous forme de chaîne
	 * @param string $file Nom du fichier dans lequel l'erreur a été identifiée
	 * @param string $line Numéro de ligne à laquelle l'erreur a été identifiée
	 * @param array $context Tableau qui pointe sur la table des symboles actifs lors de l'erreur ( contient un tableau avec toutes les variables qui existaient lorsque l'erreur a été déclenchée)
	 */
	private static function prodErrorHandler( $type, $error, $file, $line, $context ){
		
		global $GLOBAL_START_URL;
		
		switch( $type ){
			
			case E_USER_ERROR:
			case E_ERROR :
	        case E_PARSE :
	        case E_CORE_ERROR :
	       	case E_COMPILE_ERROR :

				/* message à l'écran */
				
				self::error( "Une erreur inattendue est survenue, l'administrateur du site a été prévenu afin que cela soit corrigé" );
				
				/* log PHP */
				
				error_log( "$error IN $file AT LINE $line" );
				
				/* mail envoyé à l'administrateur du site */
				
				self::mailError( "Erreur fatale sur $GLOBAL_START_URL", $type, $error, $file, $line, $context );
				
				break;

		}

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Fonction de traitement des erreurs PHP (trigger_error)
	 * Mode DEV
	 * @param integer $type Niveau d'erreur, sous la forme d'un entier
	 * @param string $error Message d'erreur, sous forme de chaîne
	 * @param string $file Nom du fichier dans lequel l'erreur a été identifiée
	 * @param string $line Numéro de ligne à laquelle l'erreur a été identifiée
	 * @param array $context Tableau qui pointe sur la table des symboles actifs lors de l'erreur ( contient un tableau avec toutes les variables qui existaient lorsque l'erreur a été déclenchée)
	 */
	private static  function devErrorHandler( $type, $error, $file, $line, $context=array() ){

		global $GLOBAL_START_URL;
	
		/* @todo tempo, update PEAR + HTML_Template_Flexy */
		if( preg_match( 	"/HTML_Template_Flexy/", 				$error )
			|| preg_match( 	"/HTML\/Template\/Flexy\/Compiler/", 	$file )
			|| preg_match( 	"/PEAR::getStaticProperty/", 			$error ) )
				return;
			
		self::error( "[ $type ] $error IN $file AT LINE $line" );

		//print_r( debug_backtrace() );
		
		error_log( "$error IN $file AT LINE $line" );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Fonction de traitement des erreurs PHP (trigger_error)
	 * Mode INSANE
	 * @param integer $type Niveau d'erreur, sous la forme d'un entier
	 * @param string $error Message d'erreur, sous forme de chaîne
	 * @param string $file Nom du fichier dans lequel l'erreur a été identifiée
	 * @param string $line Numéro de ligne à laquelle l'erreur a été identifiée
	 * @param array $context Tableau qui pointe sur la table des symboles actifs lors de l'erreur ( contient un tableau avec toutes les variables qui existaient lorsque l'erreur a été déclenchée)
	 */
	private static  function insaneErrorHandler( $type, $error, $file, $line, $context=array() ){

		/* message à l'écran */

		self::error( "$error IN $file AT LINE $line" );
		
		/* log PHP */
		
		error_log( "$error IN $file AT LINE $line" );

	}
	
	//----------------------------------------------------------------------------
	
	private static function mailError( $subject, $type, $error, $file, $line, $context ){
		
		global $GLOBAL_ADMIN_MAIL;

		if( empty( $GLOBAL_ADMIN_MAIL ) )
			return;
			
		if( User::getInstance()->getId() )
				$username = User::getInstance()->get( "login" );
		else 	$username = "";
				
		$msg  = "[ FICHIER ] => $file (l.$line)";
		$msg .= "\r\n[ URI ] => " . $_SERVER[ "REQUEST_URI" ];
		
		if( isset( $_SERVER[ "HTTP_REFERER" ] ) )
			$msg .= "\r\n[ HTTP_REFERER ] => " . $_SERVER[ "HTTP_REFERER" ];

		$msg .= "\r\n[ DATE ] => " . date( "d-m-Y H:i:s" );
		$msg .= "\r\n[ UTILISATEUR ] => $username";
		$msg .= "\r\n[ IP ] => " . $_SERVER[ "REMOTE_ADDR" ];
		$msg .= "\r\n[ ERREUR ] => $error";
		//$msg .= "\r\n[ BACKTRACE ] => " . var_export( debug_backtrace(), true ); BUG CONTEXTE AFFICHE SUR LA SORTIE STANDARD
		$msg .= "\r\n[ CONTEXT ] => " . var_export( $context, true );
		
		mail( $GLOBAL_ADMIN_MAIL, $subject, wordwrap( str_replace( "\n.", "\n..", $msg ), 70 ) );

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Affiche une erreur à l'utilisateur
	 * @access private
	 * @param string msg Message d'erreur affiché à l'écran pour l'utilisateur
	 * return void
	 */
	private static function error( $msg ){
		
	   //echo "\n<pre>" . htmlentities( $msg ) . "</pre>";
	   echo "\n<pre>" .  Util::doNothing($msg)  . "</pre>";
	}

	//----------------------------------------------------------------------------
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>
