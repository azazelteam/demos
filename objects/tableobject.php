<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object table (stdform)
 */

class TABLEDESC
{
	protected $tablename;
	protected $tabledesc = false;
	protected $design = false;
	protected $fields = '';
	protected $multilangfields = array();
	
	/**
	 * TABLEDESC::load_design()	Recherche les informations sur la table dans les tables desc_table et desc_field 
	 */
	function load_design($spliter=', ')
	{
		
		$SQL_select_1 = "SELECT `export_avalaible`, `export_name`, `description`, `tablekeys` FROM `desc_table` WHERE `tablename` = '".$this->tablename."'";
		$rs =& DBUtil::query($SQL_select_1);
		//trigger_error(showDebug($SQL_select_1,'TABLEDESC::load_design() => $SQL_select_1','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($SQL_select_1,'TABLEDESC::load_design() => $SQL_select_1','log'),E_USER_ERROR);
		if( !$rs->EOF ) {
			//convert tablekeys into an array
			$this->tabledesc = $rs->fields;
			$keys = explode(",",$this->tabledesc['tablekeys']);
			$this->tabledesc['tablekeys']=array();
			foreach($keys as $v) $this->tabledesc['tablekeys'][]=trim($v);
		}
		//get the fields
		//$fields = 'fieldname,type,size,search_select_table,search_avalaible,display_avalaible,help,modify_select_table,append_select_table,edit_avalaible,default_value';
		$fields = 'fieldname'; //what we need
		$SQL_select_2 = "SELECT $fields FROM `desc_field` WHERE `tablename` = '".$this->tablename."' ORDER BY `order_view`";
		$this->design = DBUtil::getConnection()->GetAll($SQL_select_2);
		//trigger_error(showDebug($SQL_select_2,'TABLEDESC::load_design() => $SQL_select_2','sql'),E_USER_NOTICE);
		//champs exclus
		$exclude_fields = array('username','lastupdate');
		$n = count($this->design);
		$fields = array();
		for($i=0;$i<$n;$i++) {
			if(in_array($this->design[$i]['fieldname'],$exclude_fields)) continue;
			if (preg_match('/^([a-z]*)(_[1-5])$/i',$this->design[$i]['fieldname'],$matches)) {
				if( in_array($matches[1],$this->multilangfields)) {
					 if($matches[2]==$this->lang) $fields[] = '`'.$this->design[$i]['fieldname'] .'` AS '. $matches[1];
				} 
				else $fields[] = '`'.$this->design[$i]['fieldname'].'`' ;
			}
			else $fields[] = '`'.$this->design[$i]['fieldname'].'`' ;
		}
		$this->fields = implode($spliter,$fields);
		//echo $this->fields;
	}

}//EOC
?>