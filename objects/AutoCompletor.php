<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object Auto-complétion Ajax
 */
 
include_once( dirname( __FILE__ ) . "/Encryptor.php" );

abstract class AutoCompletor {

	//----------------------------------------------------------------------------
	
	/**
	 * @var string $FORMAT_DATE
	 * Type de données : date
	 * @access public
	 * @static
	 */
	public static $FORMAT_DATE = 0;
	
	/**
	 * @var string $FORMAT_TEXT
	 * Type de données : texte
	 * @access public
	 * @static
	 */
	public static $FORMAT_TEXT = 0;
	
	/**
	 * @var string $FORMAT_DATE
	 * Type de données : image
	 * @access public
	 * @static
	 */
	public static $FORMAT_IMAGE = 0;
	
	/**
	 * @var string $TIMEOUT
	 * temps d'affichage des résultats
	 * @access public
	 * @static
	 */
	public static $TIMEOUT = 2500;
	
	//----------------------------------------------------------------------------
	//config
	
	/**
	 * @var string $INSTALLATION_FOLDER
	 * Répertoire où sont installées les librairies javascript
	 * @access private
	 * @static
	 */
	private static $INSTALLATION_FOLDER = "/js/autocomplete";
	
	/**
	 * @var string $DEFAULT_MAX_RESULT_COUNT
	 * Nombre maximum de propositions pour l'auto-complétion
	 * @access private
	 * @static
	 */
	private static $DEFAULT_MAX_RESULT_COUNT = 10;

	/**
	 * @var string $DATE_FORMAT
	 * Format utilisé pour la date
	 * %d : jour, %m : mois, %Y : année
	 * @access private
	 * @static
	 */
	private static $DATE_FORMAT = "%d-%m-%Y";
	
	/**
	 * @var bool $INITIALIZED
	 * Vaut true si le calendrier a déjà été initialisé une fois
	 * @access private
	 * @static
	 * @access private
	 */
	private static $INITIALIZED = false;

	//----------------------------------------------------------------------------
	
	/**
	 * Charge les librairies javascript nécessaire à l'auto-complétion si cela n'a pas encore été fait
	 * @access private
	 * @static
	 * @return void
	 */
	private static function init(){
		
		global 	$GLOBAL_START_URL;
		
		?>
		<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?><?php echo self::$INSTALLATION_FOLDER ?>/js/bsn.AutoSuggest_2.1.3.js" charset=utf-8"></script>
		<link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?><?php echo self::$INSTALLATION_FOLDER ?>/css/autosuggest_inquisitor.css" type="text/css" media="screen" charset=utf-8" />
		<?php

		self::$INITIALIZED = true;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé une auto-complétion à partir d'un champ donné dans une table donnée
	 * @static
	 * @param string $inputFieldID l'identifiant du champ dans lequel se fera l'auto-complétion
	 * @param string $table le nom de la table
	 * @param int $max_result_count le nombre maximal de propositions, optionnel, vaut AutoCompletor::$DEFAULT_MAX_RESULT_COUNT par défaut
	 * @param string $callback fonction callback
	 * @return void
	 */
	public static function completeFromDB( $inputFieldID, $table, $column, $max_result_count = false, $callback = false ){

		global 	$GLOBAL_START_URL,
				$GLOBAL_DB_PASS;
		
		if( !self::$INITIALIZED )
			self::init();
	
		$max_result_count = $max_result_count === false ? self::$DEFAULT_MAX_RESULT_COUNT : intval( $max_result_count );
		
		if( !$max_result_count )
			$max_result_count = self::$DEFAULT_MAX_RESULT_COUNT;

		$urlPattern = $GLOBAL_START_URL . self::$INSTALLATION_FOLDER . "/autocomplete.php";
		$urlPattern .= "?p1=" . URLFactory::base64url_encode( Encryptor::encrypt( $table, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p2=" . URLFactory::base64url_encode( Encryptor::encrypt( $column, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p3=" . URLFactory::base64url_encode( Encryptor::encrypt( $max_result_count, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p4=";
		
		?>
		<script type="text/javascript">
		/* <![CDATA[ */

			var options = {
			
				script: function( input ){ return "<?php echo $urlPattern ?>" + input; },
				varname:"input",
				shownoresults: true,
				noresults: 'Aucune suggestion',
				json: false,
				cache:true,
				maxentries:<?php echo $max_result_count ?>,
				minchars: 1,
				className: "autosuggest",
				delay: 300,
				timeout: <?php echo self::$TIMEOUT; ?>,
				offsety: -5<?php
					
				if( $callback )
					echo ", callback: $callback";
					
				?>
			
			};
			
			var as_xml = new bsn.AutoSuggest( '<?php echo $inputFieldID ?>', options);

		/* ]]> */
		</script>
		<?php
			
	}
	
//----------------------------------------------------------------------------
	
	/**
	 * Créé une auto-complétion à partir d'un motif donné dans une table donnée
	 * @static
	 * @param string $inputFieldID l'identifiant du champ dans lequel se fera l'auto-complétion
	 * @param string $table le nom de la table
	 * @param string $column le nom de la colonne contenant les valeurs à sélectionner
	 * @param string $pattern le motif utilisé pour construire chaque suggestion :
	 * 	Les valeurs des colonnes de la table $table peuvent être affichées si elles sont entre accolades
	 * 	ex : pour la table `buyer` : "Client n°{idbuyer} - société : {company}"
	 * @param int $max_result_count le nombre maximal de propositions, optionnel, vaut AutoCompletor::$DEFAULT_MAX_RESULT_COUNT par défaut
	 * @param string $callback fonction callback
	 * @return void
	 */
	public static function completeFromDBPattern( $inputFieldID, $table, $column, $pattern, $max_result_count = false, $callback = false ){

		global 	$GLOBAL_START_URL,
				$GLOBAL_DB_PASS;
		
		if( !self::$INITIALIZED )
			self::init();
	
		$max_result_count = $max_result_count === false ? self::$DEFAULT_MAX_RESULT_COUNT : intval( $max_result_count );
		
		if( !$max_result_count )
			$max_result_count = self::$DEFAULT_MAX_RESULT_COUNT;

		$urlPattern = $GLOBAL_START_URL . self::$INSTALLATION_FOLDER . "/autocomplete_pattern.php";
		$urlPattern .= "?p1=" . URLFactory::base64url_encode( Encryptor::encrypt( $table, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p2=" . URLFactory::base64url_encode( Encryptor::encrypt( $column, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p3=" . URLFactory::base64url_encode( Encryptor::encrypt( $pattern, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p4=" . URLFactory::base64url_encode( Encryptor::encrypt( $max_result_count, $GLOBAL_DB_PASS ) );
		$urlPattern .= "&p5=";
		
		?>
		<script type="text/javascript">
		/* <![CDATA[ */

			var options = {
			
				script: function( input ){ return "<?php echo $urlPattern ?>" + input; },
				varname:"input",
				shownoresults: true,
				noresults: 'Aucune suggestion',
				json: false,
				cache:true,
				maxentries:<?php echo $max_result_count ?>,
				minchars: 1,
				className: "autosuggest",
				delay: 300,
				timeout: <?php echo self::$TIMEOUT; ?>,
				offsety: -5<?php
					
				if( $callback )
					echo ", callback: $callback";
					
				?>
			
			};
			
			var as_xml = new bsn.AutoSuggest( '<?php echo $inputFieldID ?>', options);

		/* ]]> */
		</script>
		<?php
			
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé une auto-complétion à partir d'un fichier XML donné
	 * @todo
	 * @static
	 * @param string $inputFieldID l'identifiant du champ dans lequel se fera l'auto-complétion
	 * @param string $filepath le chemin absolu du fichier XML utilisé pour l'auto-complétion
	 * @param int $max_result_count le nombre maximal de propositions, optionnel, vaut AutoCompletor::$DEFAULT_MAX_RESULT_COUNT par défaut
	 * @return void
	 */
	public static function completeFromFile( $inputFieldID, $url, $max_result_count = false, $callback = false, $initialized = false ){
	
		global 	$GLOBAL_START_URL;
		
		if( $initialized )
			self::$INITIALIZED = true;
		
		if( !self::$INITIALIZED )
			self::init();
	
		$max_result_count = $max_result_count === false ? self::$DEFAULT_MAX_RESULT_COUNT : intval( $max_result_count );
		
		if( !$max_result_count )
			$max_result_count = self::$DEFAULT_MAX_RESULT_COUNT;
		
		?>
		<script type="text/javascript">
		/* <![CDATA[ */

			$(document).ready(function(){
			
				var options = {
				
					script: function( input ){ return "<?php echo $url; ?>?search=" + input + "&limit=<?php echo $max_result_count; ?>"; },
					varname:"input",
					shownoresults: true,
					noresults: 'Aucune suggestion',
					json: false,
					cache:true,
					maxentries:<?php echo $max_result_count ?>,
					minchars: 1,
					className: "autosuggest",
					delay: 300,
					timeout: <?php echo self::$TIMEOUT; ?>,
					offsety: -5<?php
					
					if( $callback )
						echo ", callback: $callback";
						
					?>
				
				};
				
				var as_xml = new bsn.AutoSuggest( '<?php echo $inputFieldID ?>', options);

			});
			
		/* ]]> */
		</script>
		<?php
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>