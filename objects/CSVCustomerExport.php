<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export Produit CSV
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );

set_time_limit( 120 );
ini_set( "memory_limit", "32M" );

define( "DEBUG_CSVCUSTOMEREXPORT", 0 );

class CSVCustomerExport {
	
	//--------------------------------------------------------------------------------------------------

	private $erp_name;
	
	private $separator;
	private $text_delimiter;
	private $breakline;
	
	private $mime_type;
	private $output_type;
	private $filename;
	
	private $metadata;
	private $data;
	private $headers;
	private $errors;
	
	private $default_action;
	private $customizeColumnSelection;
	private $filters;
	private $sorts;
	
	//--------------------------------------------------------------------------------------------------
	
	private static $ACTION_INSERT 	= "création";
	private static $ACTION_UPDATE 	= "mise à jour";
	private static $ACTION_DELETE 	= "suppression";
	
	public static $OUTPUT_TYPE_CSV = "csv";
	public static $OUTPUT_TYPE_XLS = "xls";
	public static $OUTPUT_TYPE_XML = "xml";
	
	public static $OS_LINUX 		= 0;
	public static $OS_WINDOWS		= 1;
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $output_type le type de fichier de résultat
	 * @param string $separator le séparateur
	 * @param string $text_delimiter l'indicateur de texte
	 * @param string $breakline l'indicateur de saut de ligne
	 */
	function __construct( $output_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\n" ){

		$this->separator 		= $separator;
		$this->text_delimiter 	= "\"";
		$this->breakline 		= $breakline;
		
		$this->mime_type 		= "";
		$this->filename 		= "export_clients_" . date( "Y-m-d" );
		
		$this->metadata 		= array();
		$this->data 			= array();
		$this->headers 			= array();
		$this->errors 			= array();
		$this->sorts 			= array();
		$this->filters 			= array();
		
		$this->setOutputType( $output_type );
	
		$this->customizeColumnSelection = false;
		$this->default_action 			= CSVCustomerExport::$ACTION_UPDATE;	
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Initialise le séparateur
	 * @param string $separator le séparateur
	 * @return void
	 */
	public  function setSeparator( $separator ){
	
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>setSeparator( $separator )</u>" );
			
		$this->separator = $separator;
		
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Initialise le séparateur de texte
	 * @param string $text_delimiter le séparateur
	 * @return void
	 */
	public function setTextDelimiter( $text_delimiter ){
	
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>setTextDelimiter( $text_delimiter )</u>" );
			
		$this->text_delimiter = $text_delimiter;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Initialise le caractère de saut de ligne
	 * @param string $breakline le caractère de saut de ligne
	 * @return void
	 */
	public  function setBreakline( $breakline ){
	
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>setBreakline( $breakline )</u>" );
			
		$this->breakline = $breakline;
		
	}
	
	//--------------------------------------------------------------------------------------------------

	/**
	 * Exécute une requête SQL
	 * @param string $query la requête à éxécuter
	 * @return ADODBResultset le résultat de la requête ou false en cas d'échec
	 */
	private function &query( $query ){

		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>query()</u><br />$query" );
			
		$result =& DBUtil::query( $query );
		
		if( $result === false )
			$this->errors[] = "SQL Error : $query ( " . DBUtil::getConnection()->ErrorMsg() . " )"; 

		return $result;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Initialise le type de fichier de sortie
	 * @param string $output_type
	 * @return void
	 */
	public function setOutputType( $output_type ){
	
		switch( $output_type ){
		
			case CSVCustomerExport::$OUTPUT_TYPE_CSV :
			
				$this->output_type = $output_type;
				$this->mime_type = "csv/text";
				
				break;
				
			case CSVCustomerExport::$OUTPUT_TYPE_XLS :
			case CSVCustomerExport::$OUTPUT_TYPE_XML :
			
			default : die( "Unknown output mode '$output_type'" );
		
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Génère le fichier
	 * @return le fichier
	 */
	public function export(){
	
		if( !$this->customizeColumnSelection )
			$this->setMetaData();
		
		$this->setHeaders();

		$exportedRows = $this->output();
		
		return $exportedRows;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Applique les colonnes personnalisées
	 */
	public function setCustomizeColumnSelection( $customizeColumnSelection ){
	
		$this->customizeColumnSelection = $customizeColumnSelection;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute une colonne dans l'export
	 * @param string $table le nom de la table
	 * @param string $column le nom du champ
	 * @return void
	 */
	public function addColumn( $table, $column ){
	
		if( !isset( $this->metadata[ $table ] ) )
			$this->metadata[ $table ] = array( "columns" => array()	);
			
		if( isset( $this->metadata[ $table ][ "columns" ][ $column ] ) )
			return;
		
		$query = "
		SELECT type, size, export_name, export_info_column
		FROM desc_field
		WHERE tablename LIKE '$table' AND fieldname LIKE '$column'
		LIMIT 1";
		
		$result =& $this->query( $query );

		if( !$result->RecordCount() )
			return;
			
		if( !strlen( $result->fields( "export_info_column" ) ) ){
			
			$this->metadata[ $table ][ "columns" ][ $column ] = array(
				
				"fieldformat" 		=> $result->fields( "type" ),
				"erp_fieldname" 	=> $result->fields( "export_name" ),
				"erp_fieldformat" 	=> $result->fields( "type" ),
				"size" 				=> $result->fields( "size" )
					
  			);
  			
  			$this->metadata[ $table ][ "local_key" ] 		= "";
  			$this->metadata[ $table ][ "foreign_key" ] 		= "";
  			$this->metadata[ $table ][ "foreign_table" ] 	= "";
  			
		}
		else{ /* colonne étrangère */
			
			list( $foreign_table, $foreign_column, $foreign_key, $useless ) = explode( ":", $result->fields( "export_info_column" ) );
			
			$query = "
			SELECT type, size
			FROM desc_field
			WHERE tablename = '$foreign_table'
			AND fieldname = '$foreign_column'
			LIMIT 1";
			
			$rs =& $this->query( $query );
			
			$this->metadata[ $foreign_table ][ "columns" ][ $foreign_column ] = array(
				
				"fieldformat" 		=> $rs->fields( "type" ),
				"erp_fieldname" 	=> $result->fields( "export_name" ),
				"erp_fieldformat" 	=> $rs->fields( "type" ),
				"size" 				=> $rs->fields( "size" )
					
  			);
  			
  			$this->metadata[ $foreign_table ][ "local_key" ] 		= $foreign_key;
  			$this->metadata[ $foreign_table ][ "foreign_table" ] 	= $table;
  			$this->metadata[ $foreign_table ][ "foreign_key" ] 		= $column;
  			
  			/* @todo */
  			
  			if( count( $this->metadata[ $foreign_table ][ "columns" ] ) > 1 )
  				trigger_error( "Impossible d'effectuer 2 jointures sur la table $foreign_table", E_USER_ERROR );
  			
		}
	
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * définit le format des données
	 * @return void
	 */	
	private function setMetaData(){
	
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>setMetaData()</u>" );

		$this->metadata = array();
		
		//tables
		
		$this->metadata = array(

			"buyer" 	=> array( 
			
				"columns" 			=> array(),
				"tablekeys" 		=> "idbuyer",
				"export_filter" 	=> "",
				"function_export" 	=> ""
		
			),
			"contact" 	=> array( 
			
				"columns" 			=> array(),
				"tablekeys" 		=> "idbuyer, idcontact",
				"export_filter" 	=> "",
				"function_export" 	=> ""
		
			)
			
		);
	
		$query = "
		SELECT fieldname,
			tablename,
		FROM desc_field
		WHERE tablename IN( 'buyer', 'contact' )
		AND export_avalaible <> 0
		ORDER BY tablename ASC, order_view ASC";
		
		$result =& $this->query( $query );
		
		while( !$result->EOF() ){
			
			$this->addColumn( $result->fields( "tablename" ), $result->fields( "fieldname" ) );
			
			$result->MoveNext();
			
		}
	
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * remplit les données
	 * @param int $start limitmin pour la requete
	 * @param int $limit limitmax pour la requete
	 * @return bool true si tout se passe bien, false sinon
	 */	
	protected function setData( $start, $limit ){

		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>setData()</u>" );

		$this->data = array();
		
		$selectPattern = $this->getSelectPattern();
		
		if( $selectPattern === false )
			return false;
			
		$query = "$selectPattern LIMIT $start, $limit";
		
		$result =& $this->query( $query );
		
		if( $result === false ){
		
			$this->errors[] = "QUERY ERROR : $query";
			$this->abort();
			
			return false;
			
		}
		
		if( !$result->RecordCount() )
			return false;


		while( !$result->EOF() ){
			
			$this->data[] = array_values( $result->fields );

			$result->MoveNext();
			
		}
		
		if( DEBUG_CSVCUSTOMEREXPORT )
			print_r( $this->data );
			
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Retourne la requete a executer
	 * @return string $pattern la requete
	 */
	private function getSelectPattern(){
	
		$tables 		= array_keys( $this->metadata );
		$buyerColumns 	= array_keys( $this->metadata[ "buyer" ][ "columns" ] );
		$contactColumns = array_keys( $this->metadata[ "contact" ][ "columns" ] );
	
		$buyerSelect 	= "buyer.`" . implode( "`, buyer.`", $buyerColumns ) . "`";
		$contactSelect 	= "contact.`" . implode( "`, contact.`", $contactColumns ) . "`";
		$foreignSelect 	= "";
		
		$i = 0;
		foreach( $this->metadata as $table => $metadata )
			if( $metadata[ "foreign_table" ] && $metadata[ "foreign_key" ] && $metadata[ "local_key" ] )
				foreach( array_keys( $metadata[ "columns" ] ) as $column ){
				
					if( $i )
						$foreignSelect .= ",";
						
					$foreignSelect .= "COALESCE( `$table`.`$column`, '' )";
					$i++;
					
				}
				
		$pattern = "
		SELECT '{$this->default_action}', $buyerSelect, $contactSelect";
		
		if( strlen( $foreignSelect ) )
			$pattern .= ", $foreignSelect";
			
		$pattern .= "
		FROM buyer";
		
		foreach( $this->metadata as $table => $metadata )
			if( $metadata[ "foreign_table" ] && $metadata[ "foreign_table" ] == "buyer" && $metadata[ "foreign_key" ] && $metadata[ "local_key" ] )
				$pattern .= " 
				LEFT JOIN `$table`
				ON `$table`.`" . $metadata[ "local_key" ] . "` = buyer.`" . $metadata[ "foreign_key" ] . "`";
				
		$pattern .= ", contact";
		
		foreach( $this->metadata as $table => $metadata )
			if( $metadata[ "foreign_table" ] && $metadata[ "foreign_table" ] == "contact" && $metadata[ "foreign_key" ] && $metadata[ "local_key" ] )
				$pattern .= " 
				LEFT JOIN `$table`
				ON `$table`.`" . $metadata[ "local_key" ] . "` = contact.`" . $metadata[ "foreign_key" ] . "`";
		
		$pattern .= "
		WHERE buyer.idbuyer = contact.idbuyer";

		/* colonnes étrangères */
		/*
		foreach( $this->metadata as $table => $metadata )
			if( $metadata[ "foreign_table" ] && $metadata[ "foreign_key" ] && $metadata[ "local_key" ] )
				foreach( array_keys( $metadata[ "columns" ] ) as $column ){
					
					$pattern .= "
					AND `$table`.`" . $metadata[ "local_key" ] . "` = `" . $metadata[ "foreign_table" ] . "`.`" . $metadata[ "foreign_key" ] . "`";
					
				}
		*/
		//filtres
		
		$i = 0;
		while( $i < count( $this->filters ) ){
		
			$pattern .= " AND " . $this->filters[ $i ];

			$i++;
			
		}
		
		//tris
		
		if( !count( $this->sorts ) ){
		
			$pattern .= "
			ORDER BY buyer.idbuyer ASC, contact.idcontact ASC";
		
		}
		else{
		
			$pattern .= " ORDER BY";
			
			$i = 0;
			while( $i < count( $this->sorts ) ){
			
				if( $i )
					$pattern .= ",";
					
				$pattern .= " " . $this->sorts[ $i ];
				
				$i++;
				
			}
		
		}
		
		return $pattern;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Construit les entetes
	 * @return void
	 */
	private function setHeaders(){
	
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>setHeaders()</u>" );
			
		$this->headers = array();
		$this->formats = array();

		//action par défaut
		
		$this->headers[ 0 ] = "action";
		$this->formats[ 0 ] = "char";
		
		//buyer
		
		reset( $this->metadata );
		
		foreach( $this->metadata[ "buyer" ][ "columns" ] as $fieldname => $fieldMetadata ){
			
			$this->headers[] = $fieldMetadata[ "erp_fieldname" ];
			$this->formats[] = $fieldMetadata[ "erp_fieldformat" ];
			
		}
		
		//contact
		
		reset( $this->metadata );
		
		foreach( $this->metadata[ "contact" ][ "columns" ] as $fieldname => $fieldMetadata ){
			
			$this->headers[] = $fieldMetadata[ "erp_fieldname" ];
			$this->formats[] = $fieldMetadata[ "erp_fieldformat" ];
			
		}
		
		//tables étrangères
		
		reset( $this->metadata );
		
		foreach( $this->metadata as $table => $metadata )
			if( $metadata[ "foreign_table" ] )
				foreach( $metadata[ "columns" ] as $column ){
			
					$this->headers[] = $column[ "erp_fieldname" ];
					$this->formats[] = $column[ "erp_fieldformat" ];
					
				}
		
		if( DEBUG_CSVCUSTOMEREXPORT )
			print_r( $this->headers );
			
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Récupére le type de données de chaque colonne
	 * @return array les types
	 */
	private function getColumnTypes(){
	
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>getColumnTypes()</u>" );
			
		$types = array();
		
		//action par défaut
		
		$types[ 0 ] = "char";
		
		//buyer
		
		reset( $this->metadata );
		foreach( $this->metadata[ "buyer" ][ "columns" ] as $fieldname => $fieldMetadata )
			$types[] = $fieldMetadata[ "erp_fieldformat" ];
			
		//contact
		
		reset( $this->metadata );
		foreach( $this->metadata[ "contact" ][ "columns" ] as $fieldname => $fieldMetadata )
			$types[] = $fieldMetadata[ "erp_fieldformat" ];

		//autres tables
		
		reset( $this->metadata );
		foreach( $this->metadata as $table => $metadata )
			if( $metadata[ "foreign_table" ] )
				foreach( $metadata[ "columns" ] as $column )
					$types[] = $column[ "erp_fieldformat" ];
			
		if( DEBUG_CSVCUSTOMEREXPORT )
			print_r( $types );

		return $types;
		
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Génère le fichier
	 * @return le fichier
	 */
	public function output(){

		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>output()</u>" );
	
		if( headers_sent() ){
		
			$this->errors[] = "Headers already sent";
			$this->abort();
			
			return;
			
		}
		
		header( "Content-Type: {$this->mime_type}" );
		header( "Content-disposition: inline; filename={$this->filename}.{$this->output_type}" );
			
		$this->outputHeaders();
		$exportedRows = $this->outputData();
	
		return $exportedRows;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Génère les entetes
	 * @return void
	 */
	private function outputHeaders(){
		
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>outputHeaders()</u>" );
		
		$i = 0;
		while( $i < count( $this->headers ) ){
		
			if( $i )
				echo $this->separator;

			$str  = $this->text_delimiter;
			$str .= str_replace( "\"", "\"\"", $this->headers[ $i ] );
			$str .= $this->text_delimiter;
				
			echo $str;
			
			$i++;
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Génère les données
	 * @return void
	 */
	private function outputData(){

		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>outputData()</u>" );

		$start = 0;
		$limit = 120; //récupérer les données par paquets de 120 lignes
		
		$exportedRows = 0;
		
		$ret = true;
		while( 1 ){
		
			$ret = $this->setData( $start, $limit );
			
			if( !$ret )
				return $exportedRows;
				
			$rowCount 		= count( $this->data );
			$columnCount 	= count( $this->headers );
	
			$exportedRows 	+= $columnCount;

			$types = $this->getColumnTypes();
			
			$row = 0;
			while( $row < $rowCount ){
	
				echo $this->breakline; //même pour la première ligne à cause des en-têtes
						
				$column = 0;
				while( $column < $columnCount ){
				
					if( $column )
						echo $this->separator;
		
					switch( $types[ $column ] ){
						
						case "dec" :
						case "decimal" : 
						case "float" :
							
							$value = number_format( $this->data[ $row ][ $column ], 2, ",", " " );
							break;
							
						case "date" : 
							
							$value = Util::dateFormat( $this->data[ $row ][ $column ], "d/m/Y" );
							break;

						case "datetime" : 
							
							$value = Util::dateFormat( $this->data[ $row ][ $column ], "d/m/Y H:i:s" );
							break;
							
						default : $value = $this->data[ $row ][ $column ];
						
					}
					
					$value = str_replace( "\"", "\"\"", $value );
					$str  = $this->text_delimiter . $value . $this->text_delimiter;
	
					echo $str;
	
					$column++;
					
				}
				
				$row++;
				
			}
		
			$start += $limit;
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Execute les fonctions d'export
	 * @return void
	 */
	protected function applyExportFunctions(){

		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>applyExportFunctions()</u>" );
			
		new CSVHandler( $this->erp_name, "export" );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute un filtre à la requête
	 * @param string le filtre
	 * @return void
	 */
	public function addFilter( $filter )	{ $this->filters[] = $filter; }
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute un tri à la requête
	 * @param string le tri
	 * @return void
	 */
	public function addSort( $sort )		{ $this->sorts[] = $sort; }
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Arrete l'import et affiche les erreurs
	 * @return void
	 **/
	protected function abort(){
		
		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->debug( "<u>abort()</u>" );

		if( DEBUG_CSVCUSTOMEREXPORT )
			$this->report();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les erreurs
	 * @return string les erreurs
	 */
	public function getErrors(){
	
		return $this->errors;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	function debug( $string ){
	
		echo "<br />$string";
		flush();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Affiche les erreurs
	 */
	protected function report(){
			
		echo implode( "<br />", $this->errors );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//--------------------------------------------------------------------------------------------------
}