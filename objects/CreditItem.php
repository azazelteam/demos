<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ligne article avoirs clients
*/

include_once( dirname( __FILE__ ) . "/TradeItem.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );


class CreditItem extends DBObject implements TradeItem{

	//----------------------------------------------------------------------------
	/**
	 * @var Credit $credit
	 * @access private
	 */
	private $credit;
	
	//----------------------------------------------------------------------------
	/**
	 * @param Credit $credit l'avoir client
	 * @param int $idrow le numéro de la ligne article ( 1- n )
	 */
	public function __construct( Credit $credit, $idrow ){

		parent::__construct( "credit_rows", false, "idcredit", $credit->getId(), "idrow", intval( $idrow ) );

		$this->credit =& $credit;
		
	}
	
		/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getQuantity()
	 */
	public function getQuantity(){ return $this->recordSet[ "quantity" ]; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getPriceET()
	 */
	public function getPriceET(){ return $this->recordSet[ "unit_price" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getPriceATI()
	 */
	public function getPriceATI(){
	
		if( !$this->credit->applyVAT() )
			return $this->recordSet[ "unit_price" ];
			
		return round( $this->recordSet[ "unit_price" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );

	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountPriceET()
	 */
	public function getDiscountPriceET(){ return $this->recordSet[ "discount_price" ]; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountPriceATI()
	 */
	public function getDiscountPriceATI(){
		
		if( !$this->credit->applyVAT() )
			return $this->recordSet[ "discount_price" ];
			
		return round( $this->recordSet[ "discount_price" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountRate()
	 */
	public function getDiscountRate(){ return $this->recordSet[ "discount_rate" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getVATRate()
	 */
	public function getVATRate(){ return $this->credit->applyVAT() ? $this->recordSet[ "vat_rate" ] : 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getWeight()
	 */
	public function getWeight(){ return $this->credit->getInvoice()->getItems()->get( $this->recordSet[ "idrow" ] - 1 )->get( "weight" ); }

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#isPostagePaid()
	 */
	public function isPostagePaid(){ return $this->credit->getInvoice()->getItems()->get( $this->recordSet[ "idrow" ] - 1 )->get( "franco" ) == 1; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getTotalET(){ return $this->getQuantity() * $this->getDiscountPriceET(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getTotalATI(){ return $this->getQuantity() * $this->getDiscountPriceATI(); }
	
	//----------------------------------------------------------------------------
	
}

?>