<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Page accueil administration
 */

include_once( dirname( __FILE__ ) . "/config/init.php" );
include_once( dirname( __FILE__ ) . "/templates/back_office/head.php" );

// Date de dernière connexion
$englishDate = date( "l d F Y à H:i", strtotime(User::getInstance()->get( "last_connection" )));
$lastConnection = date_fr($englishDate);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Akilaé - Solution e-CRM / ERP e-B2B/B2B</title>
	<link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/home.css" />
</head>
<body>
	<div id="global">
		<div id="header">
			<div id="left">
				<div id="companyLogoContainer">
					<a href="<?php echo $GLOBAL_START_URL ?>">
					<?php if( $GLOBAL_START_URL == "http://..." || $GLOBAL_START_URL == "http:/....local" ){ ?>
						<img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/www/logo.png" alt="vers <?php echo $GLOBAL_START_URL ?>" />
					<?php }else{ ?>
						<img id="companyLogo" src="<?php echo $GLOBAL_START_URL ?>/www/logo.png" alt="vers <?php echo $GLOBAL_START_URL ?>" />
					<?php } ?>
					</a>
				</div>
			</div>
			<div id="middle">&nbsp;</div>
			<div id="right">
				<div id="rightLeft"></div>
				<div id="rightRight"></div>
				<div id="headerMenu">
					<ul class="iconsMenu">
						<li class="iconsQuitter">
							<a href="<?php echo $GLOBAL_START_URL ?>/admin.php?logout=ok"><span>&nbsp;</span>Quitter</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div id="globalMainContent">
			<div class="mainContent">
				<h1>Bienvenue <?php echo User::getInstance()->get( "firstname" ) ?></h1>
				<p class="subTitle">Dernière connexion <?php echo $lastConnection ?></p>
				<?php if( isset( $changed ) && $changed === true ){ ?><p class="subTitle">Votre mot de passe a été modifié avec succès.</p><?php } ?>
				<div id="menu">
					<div class="leftColumn">
					
						<!--<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/<?php echo B2B_STRATEGY ? "com_admin_search_estimate.php" : "" ?>" class="item">-->
						<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_follow.php" class="item">
						<span class="icons0">
						<p class="space">espace</p>
						<p class="type">commercial</p>
						</span>
						</a>
						
						<a href="<?php echo $GLOBAL_START_URL ?>/marketing/" class="item">
						<span class="icons1">
						<p class="space">espace</p>
						<p class="type">marketing</p>
						</span>
						</a>
						
						<a href="<?php echo $GLOBAL_START_URL ?>/logistics/" class="item">
						<span class="icons2">
						<p class="space">espace</p>
						<p class="type">logistique</p>
						</a>
						</span>
					
					</div>
					
					<div class="rightColumn">
					
					<a href="<?php echo $GLOBAL_START_URL ?>/product_management/" class="item">
					<span class="icons3">
					<p class="space">espace</p>
					<p class="type">chef de produit</p>
					</span>
					</a>
					
					<a href="<?php echo $GLOBAL_START_URL ?>/accounting/" class="item">
					<span class="icons4">
					<p class="space">espace</p>
					<p class="type">comptabilité</p>
					</span>
					</a>
					<a href="<?php echo $GLOBAL_START_URL ?>/administration/" class="item">
					<span class="icons5">
					<p class="space">espace</p>
					<p class="type">paramétrage</p>
					</span>
					</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
<?php

// Mise à jour de la date de dernière connection
$iduser = User::getInstance()->get( "iduser" );
User::getInstance()->setLastConnection($iduser);

// Fonction date en Fr
function date_fr($stringDate){
	// Jour
	$stringDate = str_replace ("Monday","Lundi", $stringDate);
	$stringDate = str_replace ("Tuesday","Mardi", $stringDate);
	$stringDate = str_replace ("Wednesday","Mercredi", $stringDate);
	$stringDate = str_replace ("Thursday","Jeudi", $stringDate);
	$stringDate = str_replace ("Friday","Vendredi", $stringDate);
	$stringDate = str_replace ("Saturday","Samedi", $stringDate);
	$stringDate = str_replace ("Sunday","Dimanche", $stringDate);
	// Mois
	$stringDate = str_replace ("January","Janvier", $stringDate);
	$stringDate = str_replace ("February","Février", $stringDate);
	$stringDate = str_replace ("March","Mars", $stringDate);
	$stringDate = str_replace ("April","Avril", $stringDate);
	$stringDate = str_replace ("May","Mai", $stringDate);
	$stringDate = str_replace ("June","Juin", $stringDate);
	$stringDate = str_replace ("July","Juillet", $stringDate);
	$stringDate = str_replace ("August","Août", $stringDate);
	$stringDate = str_replace ("September","Septembre", $stringDate);
	$stringDate = str_replace ("October","Octobre", $stringDate);
	$stringDate = str_replace ("November","Novembre", $stringDate);
	$stringDate = str_replace ("December","Décembre", $stringDate);
	return ($stringDate); 
}

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>