<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object deprecated Préférer Supplier
 */

include_once( dirname( __FILE__ ) . "/Session.php" );

class DEPRECATED_SUPPLIER
{
	var $id = 0;
	var $supplierinfo;

	function SUPPLIER($id=0)
	{

		if($id > 0 )
		{
		$this->GetSupplierInfo($id);
		}
		else
		{
		if( isset($_SESSION['supplierinfo'] ) ) $this->supplierinfo = &$_SESSION['supplierinfo'];
		else $this->SetsupplierInfo() ;
		}
		$this->id = $this->GetId();
	}

	/**
	 * @deprecated
	 * Retourne l'id du fournisseur
	 */
	function GetId() { return $this->supplierinfo['idsupplier']; }
	
	/**
	 * @deprecated
	 * Retourne le login du fournisseur
	 */
	function GetLogin() { return $this->supplierinfo['login']; }
	
	/**
	 * @deprecated
	 * Retourne l'id du pays du fournisseur
	 */
	function GetStateId() {  return $this->supplierinfo['idstate']; }
	
	/**
	 * @deprecated
	 * Retourne le titre du fournisseur
	 */
	function GetTitle() {  return $this->supplierinfo['title']; }
	
	/**
	 * @deprecated
	 * Retourne le n° de téléphone du fournisseur
	 */
	function GetPhone() { return $this->supplierinfo['phonenumber']; }
	
	/**
	 * @deprecated
	 * Retourne le n° de télécopie du fournisseur
	 */
	function GetFax() { return $this->supplierinfo['faxnumber']; }
	
	/**
	 * Retourne une info du fournisseur
	 * @param string $k Le nom de l'info'
	 */
	function Get($k) { return $this->supplierinfo[$k]; }

	/**
	 * @deprecated
	 * Récupere l'identifiant d'un pays en fonction de son nom et de la langue
	 */ 
	function GetIdState()
	{
	    $Result='';
		$Base = &Session::getInstance()->GetDB();
		$Langue = Session::getInstance()->getlang();
		$State= Session::getInstance()->GetParameter('statedefault');
	    $rs =& DBUtil::query("select idstate from state where name$Langue='$State'");
	    if($rs && !$rs->EOF)
	    {
			$Result = $rs->Fields('idstate');
	    }
	    return $Result;
	}

	/**
	 * @deprecated
	 * 	recupere le nom du pays suivant la langue
	 */
	function GetStateName($idp=0)
	{
		$Result="-"; $Langue = Session::getInstance()->getlang();
		$Base = &Session::getInstance()->GetDB();
		if( $idp > 0 ) $s = $idp;
		else $s = $this->supplierinfo['idstate'] ;
		$rs =& DBUtil::query("select name$Langue as name from state where idstate='$s'");
		if($rs && !$rs->EOF)
		{
			$Result =$rs->Fields('name');
		}
		return $Result;
	}

	/**
	 * @deprecated
	 * Initialise avec les valeurs par défaut
	 */
	function SetsupplierInfo()
	{
		$this->id = 0;
		$this->supplierinfo['idsupplier'] =  0;
		$this->supplierinfo['idsubsidiary'] = 0;
		$this->supplierinfo['title'] = '';
		$this->supplierinfo['firstname'] = '';
		$this->supplierinfo['lastname'] = '';
		$this->supplierinfo['adress'] = '';
		$this->supplierinfo['adress_2'] = '';
		$this->supplierinfo['zipcode'] = '';
		$this->supplierinfo['city'] = '';
		$this->supplierinfo['idstate'] = $this->GetIdState();
		$this->supplierinfo['phonenumber'] = '';
		$this->supplierinfo['faxnumber'] = '';
		$this->supplierinfo['email'] = '';
		$this->supplierinfo['login'] = '';
		$this->supplierinfo['password'] = '';
		$this->supplierinfo['seeproduct'] = 1;
		$this->supplierinfo['nbproduct'] = -1;
		$_SESSION['supplierinfo'] = &$this->supplierinfo ;
	}

	/**
	 * @deprecated
	 * Charge les info depuis la BDD
	 */
	function GetSupplierInfo($id)
	{
		$Base = &Session::getInstance()->GetDB();
		$rs =& DBUtil::query("select * from supplier where idsupplier='$id'");
		if($rs && !$rs->EOF) {
			$this->supplierinfo = $rs->fields;
			$this->supplierinfo['title'] = $this->supplierinfo['managertitle'];
			$this->supplierinfo['firstname'] = $this->supplierinfo['managerfirstname'];
			$this->supplierinfo['lastname'] = $this->supplierinfo['managerlastname'];
			$this->supplierinfo['city'] = $rs->fields('city');
		}
		else $this->SetsupplierInfo();
		$this->supplierinfo['state'] = $this->GetStateName();
		$this->id = $this->supplierinfo['idsupplier'];
		$_SESSION['supplierinfo'] = &$this->supplierinfo ;
	}


	/**
	 * @deprecated
	 * Met à jour lesinfo du fournisseur
	 */
	function UpdateSupplier()
	{
		$Result=0;
		$Base = &Session::getInstance()->GetDB();
		$rs =& DBUtil::query("Select idsupplier from supplier where idsupplier='". $this->supplierinfo['idsupplier'] ."'");
		if($rs && !$rs->EOF)
		{
		if( isset($_POST['lastname']) ) $this->supplierinfo['lastname'] = $_POST['lastname'];
		if( isset($_POST['firstname']) ) $this->supplierinfo['firstname'] = $_POST['firstname'];
		if( isset($_POST['title']) ) $this->supplierinfo['title'] = $_POST['title'];
		if( isset($_POST['department']) ) $this->supplierinfo['department'] = $_POST['department'];
		if( isset($_POST['activity']) ) $this->supplierinfo['activity'] = $_POST['activity'];
		if( isset($_POST['function']) ) $this->supplierinfo['function'] = $_POST['function'];
		if( isset($_POST['adress']) ) $this->supplierinfo['adress'] = $_POST['adress'];
		if( isset($_POST['adress_2']) ) $this->supplierinfo['adress_2'] = $_POST['adress_2'];
		if( isset($_POST['zipcode']) ) $this->supplierinfo['zipcode'] = $_POST['zipcode'];
		if( isset($_POST['city']) ) $this->supplierinfo['city'] = $_POST['city'];
		if( isset($_POST['idstate']) ) $this->supplierinfo['idstate'] = $_POST['idstate'];
		if( isset($_POST['title']) ) $this->supplierinfo['title'] = $_POST['title'];
		if( isset($_POST['idstate']) ) $this->supplierinfo['idstate'] = $_POST['idstate'];
		if( isset($_POST['phonenumber']) ) $this->supplierinfo['phonenumber'] = $_POST['phonenumber'];
		if( isset($_POST['faxnumber']) ) $this->supplierinfo['faxnumber'] = $_POST['faxnumber'];
		if( isset($_POST['email']) ) $this->supplierinfo['email'] = $_POST['email'];
		$rs =& DBUtil::query("update supplier set managerlastname='".$this->supplierinfo['lastname']."',
				managerfirstname='".$this->supplierinfo['firstname']."',
				department='".$this->supplierinfo['department']."',
				activity='".$this->supplierinfo['activity']."',
				function='".$this->supplierinfo['function']."',
				adress='".$this->supplierinfo['adress']."',
				adress_2='".$this->supplierinfo['adress_2']."',
				zipcode='".$this->supplierinfo['zipcode']."',
				city='".$this->supplierinfo['city']."',
				idstate='".$this->supplierinfo['idstate']."',
				title='".$this->supplierinfo['managertitle']."',
				phonenumber='".$this->supplierinfo['phonenumber']."',
				faxnumber='".$this->supplierinfo['faxnumber']."',
				email='".$this->supplierinfo['email']."',  where idsupplier='". $this->supplierinfo['idsupplier'] ."'");
		if($rs) {
			$Result=1;}
	    }
	    return $Result;
	}

	/**
	 * @deprecated
	 * statistiques
	 */
	function statistique()
	{
	// quantity | idcategory | idproduct | reference 
	  $Str_Insert = "'0','0','0',''";
	  return  $Str_Insert;
	}

	//-------------------------------------------------------------------------------
	
	/**
	 * @deprecated
	 * Retourne l'identifiant du fournisseur interne s'il existe, sinon false
	 * @return mixed
	 */
	public static function getInternalSupplierId(){
		
		$internalSupplierId = intval( Session::getInstance()->getParameterAdmin( "internal_supplier" ) );
		
		return empty( $internalSupplierId ) ? false : $internalSupplierId;
		
	}
	
	//---------------------------------------------------------------------------------------
	
}//EOC
?>