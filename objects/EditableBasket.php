<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object panier éditable par un commercial
 */

include_once( dirname( __FILE__ ) . "/Basket.php" );
include_once( dirname( __FILE__ ) . "/EditableBasketItem.php" );
include_once( dirname( __FILE__ ) . "/SecuredDocument.php" );
include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );


abstract class EditableBasket extends Basket implements SecuredDocument {

	//----------------------------------------------------------------------------
	/**
	 * @access protected
	 * @var Customer $customer
	 */
	protected $customer;
	
	/**
	 * @access protected
	 * @var Salesman $salesman
	 */
	protected $salesman;
	
    //----------------------------------------------------------------------------
    //abstract members
    
    /**
     * Créer une nouvelle ligne article
     * @access protected
     * @abstract
     * @param int $idarticle l'identifiant de l'article à créer
     * @return EditableBasketItem une ligne article éditable
     */
	protected abstract function &createItem();
    /**
     * Retourne le nom de la table principale
     * @access public
     * @abstract
     * @return string
     */
    public abstract function getTableName();
    /**
     * Retourne le nom de la table pour les lignes articles
     * @access public
     * @abstract
     * @return string
     */
    public abstract function getItemsTableName();
    /**
     * Retourne le nom de la clé primaire dans la table principale
     * @access public
     * @abstract
     * @return string
     */
    public abstract function getPrimaryKeyName();
    /**
     * Retourne la valeur de la clé primaire dans la table principale
     * @access public
     * @abstract
     * @return string
     */
	public abstract function getPrimaryKeyValue();
	/**
     * Définit la valeur de la clé primaire
     * @access protected
     * @abstract
     * @return void
     */
    protected abstract function setPrimaryKeyValue( $primaryKeyValue );
	
	//----------------------------------------------------------------------------

	/**
	 * Retourne l'identifiant de l'objet ( n° de commande, de facture... )
	 * @return int
	 */
	public function getId(){ return $this->getPrimaryKeyValue(); }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne true si l'utilisateur est autorisé à consulter l'objet
	 * @return bool
	 */
	 
	public function allowRead(){
			
		return $this->allowCustomerRead() || $this->allowUserRead();
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Retourne true si l'utilisateur est autorisé à modifier l'objet
	 * @return bool
	 */
	 
	public function allowEdit(){

		//édition auto front office
		
		if( Session::getInstance()->islogin() == $this->get( "idbuyer" ) )
			return true;
		
		//le commercial identifié est le commercial propriétaire de l'objet
		
		$iduser = User::getInstance()->getId();
		
		if( $iduser == $this->get( "iduser" ) ) 
			return true;
		
		//le commercial identifié est le commercial du client
		
		$salesman = DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $this->get( "idbuyer" ) );
		
		if( $salesman == $iduser )
			return true;
	
		//le commercial identifié a des droits administrateur
		
		if( User::getInstance()->getHasAdminPrivileges() )
			return true;
		
		//aucun droit

		return false;
		
	}

    //----------------------------------------------------------------------------
    
    /**
     * Retourne true si le client identifié est autorisé à consulter l'objet
     * @access private
     * @return bool
     */
     
    private function allowCustomerRead(){
    	
		//le client identifié est le client propriétaire de l'objet
		 
		$idbuyer = Session::getInstance()->islogin();
		
		if( !$idbuyer )
			return false;
			
		return $idbuyer == $this->get( "idbuyer" );
		
    }
    
    //----------------------------------------------------------------------------
    
    /**
     * Retourne true si le commercial identifié est autorisé à consulter l'objet
     * @access private
     * @return bool
     */
     
    private function allowUserRead(){
    	
		$iduser = User::getInstance()->getId();

		if( !$iduser )
			return false;
		
		
		//le commercial identifié a des droits administrateur
		
		if( User::getInstance()->getHasAdminPrivileges() )
			return true;

								
		//le commercial identifié est le commercial propriétaire de l'objet
		
		if( $iduser == $this->get( "iduser" ) ) 
			return true;

			
		//le commercial identifié est le commercial du client
		
		$salesman = DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $this->get( "idbuyer" ) );

		if( $salesman == $iduser )
			return true;	

		return false;
		
    }

	//----------------------------------------------------------------------------
	
	/**
	 * Retourne true si le commercial enregistré a le droit de créer un document pour un client donné
	 * Bien entendu, la méthode retourne false si aucun commercial n'est enregistré
	 * @static
	 * @access public
	 * @param int $idbuyer le client
	 * @return bool
	 */
	 
	public static function allowCreationForCustomer( $idbuyer ){
		
		//droits du commercial

		$iduser = User::getInstance()->getId();
		
		//aucun commercial identifié
		
		if( !$iduser )
			return false;
		
		//le commercial identifié est le commercial du client
		
		$salesman = DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $idbuyer );
			
		if( $salesman == $iduser )
			return true;
			
		//le commercial identifié a des droits administrateur
		
		if( User::getInstance()->getValue( "admin" ) )
			return true;
		
		//aucun droit

		return false;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Assigne un commercial donné à la commande
	 * @param Salesman $salesman
	 * @return void
	 */
	public function setSalesman( Salesman $salesman ){
		
		$this->set( "iduser", $salesman->getId() );
		$this->salesman = $salesman;
				
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Initialise les propriétés de l'objet avec leurs valeurs par défaut
	 * @access protected
	 * @return void
	 */
	protected function init(){
		
		$tableName = $this->getTableName();
		
		$rs =& DBUtil::query( "DESCRIBE `$tableName`" );
		
		while( !$rs->EOF() ){
		
			$field 			= $rs->fields( "Field" );
			$defaultValue 	= $rs->fields( "Default" );
			
			$this->setValue( $field, $defaultValue );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Ajoute un article au panier
	 * Surcharge Basket::addArticle()
	 * Etant donné les spécificités dees tables où sont enregistrées les lignes articles ( estimate_row, 
	 * order_row, ... ), la structure du tableau associatif EditableBasketItem::items est définie dans 
	 * EditableBasket::createItem(). De cette façon nous pouvons généraliser les interactions avec la base 
	 * de données dans EditableBasketItem
	 * @param int $idarticle l'identifiant de l'article à ajouter
	 * @param int $quantity la quantité souhaitée, ( optionnel, 1 par défaut )
	 * @param int $idcolor la couleur souhaitée ( optionnel, aucune par défaut )
	 * @param int $idcloth le tissu souhaité ( optionnel, aucun par défaut )
	 * @return void
	 */
	 
	public function addArticle( $idarticle, $quantity = 1, $idcolor = 0, $idcloth = 0 ){
		
		$item =& $this->createItem();
		
		$item->setArticle( $idarticle );
		$item->setQuantity( $quantity );
		
		if( $idcolor )
			$item->setColor( new Color( Product::getReferenceAttribute( $item->getValue( "reference" ), "idproduct" ), $idcolor ) );
			
		if( $idcloth )
			$item->setCloth( new Cloth( Product::getReferenceAttribute( $item->getValue( "reference" ), "idproduct" ), $idcloth ) );
			
		$this->items->add( $item );
		
		$this->recalculateAmounts();
		
		$reference 	= DBUtil::getDBValue( "reference", "detail", "idarticle", $item->getValue( "idarticle" ) );
		$idproduct 	= DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->getValue( "idarticle" ) );
		$idcategory = DBUtil::getDBValue( "idcategory", "product", "idproduct", $idproduct );
 		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Ajoute une ligne article du panier du front office à l'objet ( sélection par la catalogue )
	 * @param BasketItem $basketItem la ligne article du panier
	 * @return void
	 */
	public function addBasketItem( BasketItem &$basketItem ){
		
		$idarticle 	= $basketItem->getValue( "idarticle" );
		$quantity 	= $basketItem->getValue( "quantity" );
		$reference 	= $basketItem->getValue( "reference" );
		
		$item =& $this->createItem();
		
		$item->setArticle( $idarticle );
		$item->setQuantity( $quantity );
		
		if( ( $color = $basketItem->getColor() ) !== NULL )
			$item->setColor( $basketItem->getColor() );
			
		if( ( $cloth = $basketItem->getCloth() ) !== NULL )
			$item->setCloth( $basketItem->getCloth() );
		
		$this->items->add( $item );
			
		$this->recalculateAmounts();
		
		//@todo : idcategory ne devrait pas être récupéré dans la BDD car la référence peut avoir été supprimée
		//mais si on stock le champ idcategory et que la catégorie a été supprimée c'est le même topo
		// => Il y a une erreur de conception quelque part
				
		$rs =& DBUtil::query( "SELECT p.idcategory FROM product p, detail d WHERE d.idproduct = p.idproduct AND d.idarticle = '$idarticle'" );
		
		$idcategory = $rs->fields( "idcategory" );
				
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Supprime un élément donné du panier
	 * Surcharge Basket::removeItemAt( int $index )
	 * @param int $index Le $index-ième élément à supprimer
	 * @return void
	 */
	public function removeItemAt( $index ){
		
		$this->items->remove( $this->items->get( $index ) );
		
		/* réindexation */
		
		$index = 1;
		$it =& $this->items->iterator();
		while( $it->hasNext() )
			$it->next()->setIdRow( $index++);
			
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge l'objet depuis la base de donnée
	 * @access protected
	 * @return void
	 */
	 
	protected final function load(){

		$this->loadFromMainTable();
		$this->loadFromItemsTable();			
	
		if( !$this->allowRead() ){
			
			trigger_error( "Appel de EditableBasket::load() non autorisé", E_USER_ERROR );
			die();
			
		}
			
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet dans la base de données
	 * @access protected
	 * @return void
	 */
	protected final function update(){
		
		if( !$this->allowEdit() )
			die();
			
		$this->setLastUpdateInfos();
		$this->updateMainTable();	
		$this->updateItemsTable();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet de façon sécurisée
	 * @access public
	 * @return void
	 */
	 
	public final function save(){
		
		if( !$this->allowEdit() ){
			
			trigger_error( "Appel de EditableBasket::save() non autorisé", E_USER_ERROR );
			die();
			
		}
		
		$this->recalculateAmounts();
		$this->update();			
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée un nouvel objet dans la base de données, sans ligne article
	 * @access protected
	 * @return void
	 */
	 
	protected final function create(){
			
		$tableName 			= $this->getTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		$primaryKeyValue 	= $this->getNewPrimaryKeyValue();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
		
		//création
		
		$query = "INSERT INTO `$tableName` ( `$primaryKeyName` ) VALUES ( " . DBUtil::quote( $primaryKeyValue ) . " )";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false )
			die( "Impossible de créer le nouvel objet" );
			
		$this->setPrimaryKeyValue( $primaryKeyValue );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la première valeur de clé primaire disponible dans la base de donnée
	 * @access private
	 * @return int
	 */
	 
	 private final function getNewPrimaryKeyValue(){
	 	
		$tableName 			= $this->getTableName();
		$itemsTableName 	= $this->getItemsTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$itemsTableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		
		//récupération
		
		$rs1 =& DBUtil::query( "SELECT MAX( `$primaryKeyName` ) + 1 AS `$primaryKeyName` FROM `$tableName`" );
		$rs2 =& DBUtil::query( "SELECT MAX( `$primaryKeyName` ) + 1 AS `$primaryKeyName` FROM `$itemsTableName`" );
		
		$newPrimaryKeyValue 		= $rs1->fields( $primaryKeyName );
		$newItemsPrimaryKeyValue	= $rs2->fields( $primaryKeyName );
		
		if( $newPrimaryKeyValue == null )
			$newPrimaryKeyValue = 1;
		
		if( $newItemsPrimaryKeyValue == null )
			$newItemsPrimaryKeyValue = 1;
				
	 	return max( $newPrimaryKeyValue, $newItemsPrimaryKeyValue );
	 			
	 }
	 
	//----------------------------------------------------------------------------
	
	/**
	 * Supprime un objet dont l'identifiant est donné de la base de données
	 * @static
	 * @param int $primaryKeyValue l'identifiant de l'objet à supprimer ( estimate.idestimate, order.idorder, ... )
	 * @return void
	 */
	 
	public static function delete( $primaryKeyValue ){
		
		$tableName 			= $this->getTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
		
		//suppression
		
		DBUtill::query( "DELETE FROM `$tableName` WHERE `$primaryKeyName` = '$primaryKeyValue' LIMIT 1" );
		DBUtill::query( "DELETE FROM `$tableName` WHERE `$primaryKeyName` = '$primaryKeyValue'" );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les données depuis la table principale ( `estimate`, `order`, ... )
	 * @access private
	 * @return void
	 */
	 
	private final function loadFromMainTable(){
		
		$tableName 			= $this->getTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		$primaryKeyValue 	= $this->getPrimaryKeyValue();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
		
		//chargement
		
		$this->fields = array();

		$query = "
		SELECT * FROM `$tableName`
		WHERE `$primaryKeyName` = " . DBUtil::quote( $primaryKeyValue ) ."
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les lignes article depuis la base de données ( `estimate_row`, `order_row`, ... )
	 * @access private
	 * @return void
	 */
	 
	private final function loadFromItemsTable(){
		
		$tableName 			= $this->getItemsTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		$primaryKeyValue 	= $this->getPrimaryKeyValue();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
		
		//chargement
		
		$this->items = new ArrayList();

		$query = "
		SELECT idrow, idarticle FROM `$tableName`
		WHERE `$primaryKeyName` = " . DBUtil::quote( $primaryKeyValue ) . "
		ORDER BY idrow ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){

			$item =& $this->createItem();
			$item->load();
			
			$this->items->add( $item );
			
			$rs->MoveNext();
				
		}
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet dans la table principale ( `estimate`, `order`, ... )
	 * @access private
	 * @return void
	 */
	 
	 private final function updateMainTable(){

		$tableName 			= $this->getTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		$primaryKeyValue 	= $this->getPrimaryKeyValue();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
	
		//mise à jour

		$updatableFields = array();
		foreach( $this->fields as $fieldname => $value ){
			
			if( $fieldname != $primaryKeyName )
				$updatableFields[] = $fieldname;
			
		}
		
		$query = "
		UPDATE `$tableName`
		SET ";
		
		$i = 0;
		foreach( $updatableFields as $fieldname ){
		
			if( $i )
				$query .= ", ";
					
			$query .= "`$fieldname` = " . DBUtil::quote( $this->fields[ $fieldname ] );
		
			$i++;
			
		}
		$query.=" WHERE `$primaryKeyName` = '$primaryKeyValue'";
		
		DBUtil::query( $query );
		
	 }
	 
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde les lignes article dans la base de données ( `estimate_row`, `order_row`, ... )
	 * @access private
	 * @return void
	 */
	 
	 private final function updateItemsTable(){

		//suppression des anciennes lignes

		$this->deleteFromItemsTable();
		
		//insertions des nouvelles lignes articles
		
		$iterator = $this->items->iterator();
		while( $iterator->hasNext() ){
			
			$item =& $iterator->next();
			$item->insert();
				
		}
		
	 }
	 
	//----------------------------------------------------------------------------
	
	/**
	 * Supprime l'objet de la table principale ( `estimate`, `order`, ... )
	 * @access private
	 * @return void
	 */
	 
	private final function deleteFromMainTable(){
		
		$tableName 			= $this->getTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		$primaryKeyValue 	= $this->getPrimaryKeyValue();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
		
		//suppression

		DBUtil::query( "DELETE FROM `$tableName` WHERE `$primaryKeyName` = '$primaryKeyValue' LIMIT 1" );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Supprime les lignes article de la base de données ( `estimate_row`, `order_row`, ... )
	 * @access private
	 * @return void
	 */
	 
	private final function deleteFromItemsTable(){
		
		$tableName 			= $this->getItemsTableName();
		$primaryKeyName 	= $this->getPrimaryKeyName();
		$primaryKeyValue 	= $this->getPrimaryKeyValue();
		
		//contrôles
		
	 	assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "strlen( \"$tableName\" ) > 0" );
		assert( "strlen( \"$primaryKeyName\" ) > 0" );
		assert( "intval( $primaryKeyValue ) > 0" );
		
		//suppression

		DBUtil::query( "DELETE FROM `$tableName` WHERE `$primaryKeyName` = '$primaryKeyValue'" );
		
	}

	//----------------------------------------------------------------------------

}

?>