<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresses clients
 */

include_once( dirname( __FILE__ ) . "/Address.php" );		

class CustomerAddress implements Address{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $idbuyer le numéro du client
	 * @access protected
	 */
	protected $idbuyer;
	/**
	 * @var int $idcontact le numéro du contact
	 * @access protected
	 */
	protected $idcontact;
	/**
	 * @var array $recordSet
	 * @access protected
	 */
	protected $recordSet;

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Constructeur
	 * @param int $idbuyer l'identifiant du client
	 * @param int $idcontact l'identifiant du contact client
	 */
	function __construct( $idbuyer, $idcontact = 0 ){
		// ------- Mise à jour de l'id gestion des Index  #1161
		$this->idbuyer 		= $idbuyer ;
		$this->idcontact 	= intval( $idcontact );
		$this->recordSet 	= array();
		
		$this->load();
		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	/* Address interface */
	
	public function getCompany(){ 		return $this->recordSet[ "company" ]; }
	public function getFirstName(){ 	return $this->recordSet[ "firstname" ]; }
	public function getLastName(){ 		return $this->recordSet[ "lastname" ]; }
	public function getGender(){ 		return $this->recordSet[ "idtitle" ]; }
	public function getPhonenumber(){ 	return $this->recordSet[ "phonenumber" ]; }
	public function getGSM(){ 			return $this->recordSet[ "gsm" ]; }
	public function getEmail(){ 		return $this->recordSet[ "email" ]; }
	public function getFaxnumber(){ 	return $this->recordSet[ "faxnumber" ]; }
	public function getAddress(){ 		return $this->recordSet[ "address" ]; }
	public function getAddress2(){ 		return $this->recordSet[ "address2" ]; }
	public function getZipcode(){ 		return $this->recordSet[ "zipcode" ]; }
	public function getCity(){ 			return $this->recordSet[ "city" ]; }
	public function getIdState(){ 		return $this->recordSet[ "idstate" ]; }

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access protected
	 * @return void
	 */
	protected function load(){

		$query = "
		SELECT c.firstname,
			c.lastname,
			c.phonenumber,
			c.faxnumber,
			c.title AS idtitle,
			c.mail AS email,
			c.gsm,
			b.adress AS address,
			b.adress_2 AS address2,
			b.city,
			b.zipcode,
			b.idstate,
			b.company
		FROM buyer b, contact c
		WHERE b.idbuyer = '{$this->idbuyer}'
		AND c.idbuyer = b.idbuyer
		AND c.idcontact = '{$this->idcontact}'
		LIMIT 1";
	
		$this->recordSet = DBUtil::query( $query )->fields;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/*
		@author:Behzad
	*/
	public function get( $key ){
		
		return $this->$key;
		
	}
	
}

?>