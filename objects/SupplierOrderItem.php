<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object lignes commandes fournisseurs
 */


include_once( dirname( __FILE__ ) . "/TradeItem.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );


class SupplierOrderItem extends DBObject implements TradeItem{

	//----------------------------------------------------------------------------
	/**
	 * @var SupplierOrder $supplierOrder
	 * @access private
	 */
	private $supplierOrder;
	
	//----------------------------------------------------------------------------
	/**
	 * @param SupplierOrder $owner la commande fournisseur
	 * @param int $idrow le numéro de la ligne article ( 1- n )
	 */
	public function __construct( SupplierOrder $supplierOrder, $idrow ){

		parent::__construct( "order_supplier_row", false, "idorder_supplier", $supplierOrder->getId(), "idrow", intval( $idrow ) );

		$this->supplierOrder =& $supplierOrder;
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getQuantity()
	 */
	public function getQuantity(){ return $this->recordSet[ "quantity" ]; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getPriceET()
	 */
	public function getPriceET(){ return $this->recordSet[ "unit_price" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getPriceATI()
	 */
	public function getPriceATI(){
	
		if( !$this->supplierOrder->applyVAT() )
			return $this->recordSet[ "unit_price" ];
			
		return round( $this->recordSet[ "unit_price" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );

	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountPriceET()
	 */
	public function getDiscountPriceET(){ return $this->recordSet[ "discount_price" ]; }
	
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountPriceATI()
	 */
	public function getDiscountPriceATI(){
		
		if( !$this->supplierOrder->applyVAT() )
			return $this->recordSet[ "discount_price" ];
			
		return $this->recordSet[ "discount_price" ] * ( 1.0 + $this->getVATRate() / 100.0 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getDiscountRate()
	 */
	public function getDiscountRate(){ 
	
		if( !$this->supplierOrder->applyVAT() )
			return $this->getPriceET() > 0.0 ? 100.0 * ( 1.0 - $this->getDiscountPriceET() / $this->getPriceET() ) : 0.0;

		return $this->getPriceATI() > 0.0 ? 100.0 * ( 1.0 - $this->getDiscountPriceATI() / $this->getPriceATI() ) : 0.0;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getVATRate()
	 */
	public function getVATRate(){ return $this->supplierOrder->applyVAT() ? $this->recordSet[ "vat_rate" ] : 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#getWeight()
	 */
	public function getWeight(){ return $this->recordSet[ "weight" ]; }

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * (non-PHPdoc)
	 * @see objects/TradeItem#isPostagePaid()
	 */
	public function isPostagePaid(){ return false; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getTotalET(){ return $this->getQuantity() * $this->getDiscountPriceET(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getTotalATI(){ return $this->getQuantity() * $this->getDiscountPriceATI(); }
	
	//----------------------------------------------------------------------------
	
}

?>