<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export CSV (page commune servant d'en-tête à tester l'export ERP)
*/

/**
 * Classe pour import-export CSV
 * Initialisation des variables globales
 * => require_once('configdb.php');
 */
 set_time_limit( 0 );
 define( "DEBUG_CSV", 0 );

include_once( "CSVHandler.php" );

class CsvEngine
{
	var $dataindir = '/tmp/';
	var $dataoutdir = '/tmp/';
	var $export_mode=true; // true if export, false if import
	var $mime_type = "application/octet-stream"; // default mime type
	var $default_date_fr = false;
	var $default_number_format = 2; // 2 = fr, 4 = en, 5 = math
	var $db; // db connection
	var $result = FALSE; // query result
	var $result2 = FALSE; // 2nd query result
	var $use_filter = TRUE; // utilisation du filtre pour l'export
	var $aErrors;
	var $tables; // tables infos
	var $exports;
	var $fp; // file handle for excel
	
	var $erp_fields=array();
	var $csv_fields=array(); //fields in csv to care about...

	var $csv_file=''; //file to open
	var $csv_data; //data in file
	var $csv_array = array(); //array of data in file
	var $csv_sel_fields = array(); //selection of fields 
	var $csv_data_all = array(); //all the data
	var $csv_data_keyed = array(); //all the data
	var $tmp_array=array();
	
	
	var $mysql_array=array();
	var $table = array(); // erp table infos
	var $mysql_link; //database
	var $instructions=array(); //sql and default settings
	var $instruct_functions = array(); //function calls
	var $debug=0; // show debug information - step by step messages
	var $return_debug=0; // return debug information instead of echoing it
	var $vals_per_insert=100; //number of values to add per insert statement of SQL
	var $format='win'; // 'linux';
	var $linebreak_char="\r\n" ; //"\n";
	var $delimit_char = ";";
	var $headerset = TRUE;
	var $unlink_import_file = true;
	
//--------------------------------------------------------------------------------------------------

function CsvEngine($export_mode=true)
{
	
	if( DEBUG_CSV )
		echo "<p><b>CONSTRUCTOR</b></p>";
		
	//echo "CsvEngine start\n";
	$this->export_mode =$export_mode;
	$this->aErrors = array();

}

//--------------------------------------------------------------------------------------------------

function set_export() { $this->export_mode=true;}
//-----------------------------------------------------------------------------------------

function set_import() { $this->export_mode=false;}
// DATABASE FUNCTIONS
//-----------------------------------------------------------------------------------------

function connect()
{
global $GLOBAL_DB_HOST,$GLOBAL_DB_NAME,$GLOBAL_DB_USER,$GLOBAL_DB_PASS;
$this->db = mysqli_connect($GLOBAL_DB_HOST,$GLOBAL_DB_USER,$GLOBAL_DB_PASS);
if(!$this->db) { $this->aErrors[] = "Impossible de se connecter à " . $GLOBAL_DB_HOST ; return FALSE; }
// Sélection de la base de données db
$db_selected = mysqli_select_db($this->db,$GLOBAL_DB_NAME);
if (!$db_selected) {
	$this->aErrors[] = "Impossible d'utiliser la base $GLOBAL_DB_NAME : " . mysqli_error($this->db); return FALSE; }

//recherche des paramètres
$n = $this->select("SELECT paramvalue FROM parameter_admin WHERE idparameter = 'admin_email'");
if($n) { $row =  $this->getrow(); $this->msgdest = $row['paramvalue']; }
else { $this->aErrors[] = "Paramètre 'admin_email' manquant dans 'parameter_admin'."; return FALSE; }
$this->free_result();
/*
$n = $this->select("SELECT paramvalue FROM parameter_admin WHERE idparameter = 'export_dir'");
if($n) { $row =  $this->getrow(); $this->dataoutdir = $row['paramvalue']; }
else { $this->aErrors[] = "Paramètre 'export_dir' manquant dans 'parameter_admin'."; return FALSE; }
$this->free_result();
$n = $this->select("SELECT paramvalue FROM parameter_admin WHERE idparameter = 'import_dir'");
if($n) { $row =  $this->getrow(); $this->dataindir = $row['paramvalue']; }
else { $this->aErrors[] = "Paramètre 'import_dir' manquant dans 'parameter_admin'."; return FALSE; }
$this->free_result();
if(!is_dir($this->dataoutdir)) @mkdir($this->dataoutdir);
if(!is_dir($this->dataindir)) @mkdir($this->dataindir);
*/
return TRUE;
}

//-----------------------------------------------------------------------------------------

function setdb(&$db)
{
	if( DEBUG_CSV )
		echo "<p><b>SETDB $db</b></p>";
		
	$this->db = &$db;
	
}

//-----------------------------------------------------------------------------------------

function disconnect()
{
	if( DEBUG_CSV )
		echo "<p><b>DISCONNECT</b></p>";
		
	mysqli_close($this->db);
	
}

//-----------------------------------------------------------------------------------------

function select($query)
{
	
	/*if( DEBUG_CSV )
		echo "<hr /><p>SELECT $query</p>";*/
		
	$this->result = mysqli_query($this->db,$query);
	
	if(!$this->result) { 
		
		$this->aErrors[] = "Erreur SQL : $query - " . mysqli_error($this->db); 
		return 0; 
		
	}
	 
	return mysqli_num_rows($this->result);
	
}

//-----------------------------------------------------------------------------------------

function getrow()
{
return mysqli_fetch_assoc($this->result);
}

//-----------------------------------------------------------------------------------------

function free_result(){
	
if( is_resource( $this->result ) )
	@mysqli_free_result($this->result);
	
}

//-----------------------------------------------------------------------------------------

function select2($query)
{
	/*if( DEBUG_CSV )
		echo "<hr /><p>SELECT2 $query</p>";*/
		
$this->result2 = mysqli_query($this->db,$query);
if(!$this->result2) { $this->aErrors[] = "Erreur SQL : $query - " . mysqli_error($this->db); return 0; }
 return mysqli_num_rows($this->result2);
}
//-----------------------------------------------------------------------------------------

function getrow2()
{
return mysqli_fetch_assoc($this->result2);
}
//-----------------------------------------------------------------------------------------

function free_result2(){

	if( $this->result2 !== FALSE )
		@mysqli_free_result( $this->result2 );

}

//-----------------------------------------------------------------------------------------

function update($query)
{ 

	if( DEBUG_CSV || isset( $_GET[ "dev" ] ) )
		echo "<p>UPDATE $query</p>";
	
	if( isset( $_GET[ "test" ] ) )
		return;
	
	 if( !mysqli_query($this->db,$query) ) {
	 	 
	 	$this->aErrors[] = "Erreur SQL : $query - " . mysqli_error($this->db); return 0; 
	 	
	 }
	 
	 return mysqli_affected_rows($this->db);
	 
}


//---------------------------------

//CSV FUNCTIONS
//-----------------------------------------------------------------------------------------

function set_delimit_char($ch){
	$this->delimit_char = $ch;
	}

//-----------------------------------------------------------------------------------------

function set_table($t){
	
	if( DEBUG_CSV )
		echo "<h2>SET_TABLE $t</h2>";
		
	if($t=='') $t=$this->exports[0];
	$this->table = $t;
	$query="select erp_name,export_directory,export_name,import_directory,import_name from erp_config where erp_name='$t'";
	$n = $this->select($query);
	$this->table = array();
	for($i=0;$i<$n;$i++)
		{
		$this->table =  $this->getrow();
		$this->dataoutdir = $this->table['export_directory'];
		$this->dataindir = $this->table['import_directory'];
		if($this->table['export_name']=='') $this->table['export_name'] = $this->table['erp_name'];
		if($this->table['import_name']=='') $this->table['import_name'] = $this->table['erp_name'];
		}
	$this->free_result();
	}

//-----------------------------------------------------------------------------------------

function set_format($format){
	
	if( DEBUG_CSV )
		echo "<p><b>SET_FORMAT $format</b></p>";
		
	$this->format = $format;
	if($format == 'win') $this->linebreak_char = "\r\n";
	else $this->linebreak_char = "\n";
}

//-----------------------------------------------------------------------------------------

function set_date_format($format){
	$this->default_date_fr = false;
	if($format == 'fr') $this->default_date_fr = true;
	//TODO autres langues
}

//-----------------------------------------------------------------------------------------

function set_number_format($format){
	$this->default_number_format = 2;
	if($format == 'en') $this->default_number_format = 4;
	if($format == 'math') $this->default_number_format = 5;
	//TODO autres langues
}

//-----------------------------------------------------------------------------------------

function set_header($b=TRUE){
	$this->headerset = $b;
	}

//-----------------------------------------------------------------------------------------

function newid($t)
{
if($t == 0 ) return '';
return trim($t);
}

//-----------------------------------------------------------------------------------------

function get_sql_tables()
{
	if( DEBUG_CSV )
		echo "<p><b>GET_SQL_TABLES</b></p>";
		
	$erpname=$this->table['erp_name'];
	$query="
	SELECT `tablename`,
		`export_filter`,
		`tablekeys`,
		 `alternative_keys`,
		`selectkeys`,
		`empty_on_import`,
		`import_action`,
		`x_order`,
		`separator`,
		function_import,
		function_export
	FROM erp_table 
	WHERE erp_name='$erpname' 
	AND used <> 0 
	ORDER BY x_order";

$n = $this->select($query);
$this->tables = array();
for($i=0;$i<$n;$i++)
	{
	$this->tables[] =  $this->getrow();
	}
if( count( $this->tables ) && $this->tables[0]['separator'])	
	$this->delimit_char = $this->tables[0]['separator'];
else $this->delimit_char =';';
$this->free_result();
}

//-----------------------------------------------------------------------------------------

function get_sql_fields($fsel=0)
{
	if( DEBUG_CSV )
		echo "<p><b>GET_SQL_FIELDS $fsel</b></p>";
		
$this->get_sql_tables();
$query="
SELECT `tablename`,
	`fieldname`,
	`fieldformat`,
	`erp_fieldformat`,
	`erp_fieldname`,
	`mandatory`,
	`trans_table`,
	`trans_id`,
	`trans_text`,
	`order_view`,
	`size` 
FROM erp_field 
WHERE `erp_name`='".$this->table['erp_name']."' 
AND `export_avalaible` >= $fsel 
ORDER BY `order_view`,`fieldname`";

// echo $query;
$n = $this->select($query);
$this->csv_fields=array();

$this->erp_fields=array();
for($i=0;$i<$n;$i++)
	{
	$this->erp_fields[$i] = $row =  $this->getrow();
	$this->csv_fields[$i]= $row['erp_fieldname'];
	$this->erp_fields[$i]['erp_fieldformat']= $this->get_format($row['erp_fieldformat']);
	$this->erp_fields[$i]['fieldformat']= $this->get_format($row['fieldformat']);
	
	}

$this->free_result();
}

//-----------------------------------------------------------------------------------------

function translate2num($i,$str,$res=0)
{
	$table = $this->erp_fields[$i]['trans_table'];
	$id = $this->erp_fields[$i]['trans_id'];
	$alias = $this->erp_fields[$i]['trans_text'];
	
	//list($table,$id,$alias) = explode(';',$tableparam);
	$query="select $id as id from `$table` where $alias = '$str'";
	
	if( isset( $_GET[ "dev" ] ) )
		echo "<p><i><b>translate2num</b> $query</i></p>"; 
		
	$n = $this->select($query);
	if($n>0)
	{
		$row =  $this->getrow();
		$res=$row['id'];
	}
	else{

		$this->Error('Dans ' . $this->csv_file . ' la valeur  : '. $str . " n'existe pas dans colonne '$alias' de la table '$table'." );
		$res = false;
		
	}
	
	$this->free_result();
	
	return $res;

}

//-----------------------------------------------------------------------------------------

function translate2text( $i, $num, $res='' )
{
	
	$table = $this->erp_fields[$i]['trans_table'];
	$id = $this->erp_fields[$i]['trans_id'];
	$alias = $this->erp_fields[$i]['trans_text'];
	
	$query = "SELECT `$id` FROM `$table` WHERE `$alias`= '$num'"; 
	
	if( isset( $_GET[ "dev" ] ) )
		echo "<p><i><b>translate2text</b> $query</i></p>"; 
	
	$n = $this->select2($query);
	
	if($n>0)
	{
		$row =  $this->getrow2();
		$res = $row[ $id ];
	}
	else{
	
		$this->Error( "Impossible de trouver une valeur de '$id' dans la table '$table' lorsque '$alias' vaut '$num'" );
		$res = false;
		
	}
	
	$this->free_result2();
	
	return $res;
	
}

//get the tables 
//-----------------------------------------------------------------------------------------

function get_tables($tool='')
{
	
	if( DEBUG_CSV )
		echo "<p><b>GET_TABLES $tool</b></p>";
		
$query="select erp_name from erp_config WHERE tools='$tool' order by erp_name";
$n = $this->select($query);
$this->exports=array();
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$this->exports[$i]=$row['erp_name'];
	}
$this->free_result();
return $n;
}

//-----------------------------------------------------------------------------------------

function list_tables($tool='',$cuurtable='')
{
	if( DEBUG_CSV )
		echo "<p><b>LIST_TABLES $tool</b></p>";
		
$n = $this->get_tables($tool);
for($i=0;$i<$n;$i++)
	{
	echo '<option value="',$this->exports[$i],'"';
	if($this->exports[$i] == $cuurtable) echo ' SELECTED ';
	echo '>',$this->exports[$i],"</option>\n";
	}
}

//-----------------------------------------------------------------------------------------

function list_tools($tool)
{
	
	if( DEBUG_CSV )
		echo "<p><b>LIST_TOOLS $tool</b></p>";
		
$query="select distinct tools from erp_config order by tools";
$n = $this->select($query);
$this->exports=array();
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	echo '<option value="',$row['tools'],'"';
	if($row['tools'] == $tool) echo ' SELECTED ';
	echo '>',$row['tools'],"</option>\n";
	}
$this->free_result();
}

//-----------------------------------------------------------------------------------------

function set_sqlfilter($b=TRUE)
{
$this->use_filter = $b;
}


//-----------------------------------------------------------------------------------------

function set_csv($f='no used')
{
	if( DEBUG_CSV )
		echo "<p><b>SET_CSV $f</b></p>";
	
if($this->export_mode){

	$this->csv_file = $this->dataoutdir . $this->table['export_name'] . ".csv";
	
}
else{
	
	if( empty( $this->table[ "import_directory" ] ) )
		return $this->Error( "Aucun répertoire n'a été défini pour l'import de l'interface '" . $this->table[ "erp_name" ]  . "'" );
		
	if( empty( $this->table[ "import_name" ] ) )
		return $this->Error( "Aucun nom de fichier n'a été défini pour l'import de l'interface '" . $this->table[ "erp_name" ] . "'" );
		
	$this->csv_file = $this->dataindir . $this->table['import_name'] . ".csv";
	
}

if( $this->export_mode AND !is_dir($this->dataoutdir) ) 
	return;// $this->Error('Export directory does not exist: '.$this->dataoutdir);
elseif( !$this->export_mode && !is_dir($this->dataindir) ) 
	return $this->Error('Import directory does not exist: '.$this->dataindir);
if( is_file($this->csv_file) ) return TRUE;
return FALSE;
}

//-----------------------------------------------------------------------------------------

function force_csv($f)
{
$this->csv_file =  $f ;
if( is_file($this->csv_file) ) return TRUE;
return FALSE;
}

//-----------------------------------------------------------------------------------------

function set_filter($v)
{
$this->csv_sel_fields = $v; 
}

//-----------------------------------------------------------------------------------------

function do_filter()
{
	if( empty($this->csv_file) ) 
		return;
		
	if( empty($this->csv_sel_fields) ) 
		return;
		
	$n = count($this->erp_fields);
	
	for($i=0;$i<$n;$i++){
		
		if(array_search($i, $this->csv_sel_fields) === FALSE)
			$this->erp_fields[$i]['fieldformat'] = -1;
	
		
	}

}

//-----------------------------------------------------------------------------------------

function sql2csv()
{

	if( DEBUG_CSV )
		echo "<p><b>SQL2CSV</b></p>";

	$this->do_filter(); 
	$msg='';
	$this->mime_type = 'text/x-csv';
	if(!is_dir($this->dataoutdir)){
	
		if( empty( $this->dataoutdir ) )
			return;
			
		@mkdir($this->dataoutdir);// try to create the directory
		
	}
	
	if(!is_dir($this->dataoutdir) ) 
		return;// $this->Error('Export directory does not exist: '.$this->dataoutdir);
		
	if( is_file($this->csv_file ) )  
		unlink($this->csv_file);
		
	$handle = fopen( $this->csv_file , 'wb' );
	if(!$handle) 
		return $this->Error('Unable to create output file: '.$this->csv_file);
		
	$erpFieldCount = count($this->erp_fields);
	$fieldlist='';$virg='';

	for($j=0;$j<$erpFieldCount;$j++){
	 	
		if( $this->erp_fields[$j]['fieldformat'] >= 0 ){
			
			$fieldlist .= $virg . "`" . $this->erp_fields[$j]['fieldname'] . "`";
			$virg=',';
			
		} 
		
	}

	//la première table
	
	$table = "";
	
	if( count( $this->tables ) )
		$table = $this->tables[0]['tablename'];
		
	if( empty($table) ){
		
		$rowCount = 0;
		$this->Error( 'Pour ' . $this->csv_file . ' il manque un enregistrement dans la table erp_table.' );
	
	}
	else{
		
		if( !empty( $this->tables[0]['export_filter'] ) )
			$this->tables[0]['export_filter'] = " WHERE " . $this->tables[0]['export_filter'];
		
		$query= "SELECT * FROM `$table`" . $this->tables[0]['export_filter'] ;
		$rowCount = $this->select($query);
	
	}

	//écrire les en-têtes
	
	if( $this->headerset ){ 
		
		$virg='';
	 
	 	for($j=0;$j<$erpFieldCount;$j++){
	 		
			if( $this->erp_fields[$j]['fieldformat'] >= 0 ){
				
				$erp_fieldname = $this->erp_fields[$j]['erp_fieldname'];
				fwrite($handle, $virg.$erp_fieldname);
				$virg=$this->delimit_char;
				
			} 
			
		}
		
	 	fwrite($handle, $this->linebreak_char);
	 
	 }

	//écrire les données de la première table
	
	$buffer=array(); 
	for($i=0;$i<$rowCount;$i++){
		
		$row =  $this->getrow(); 
		$buffer[$i]=array(); ////echo "$i <br />\n"; flush();
		
		for($j=0;$j<$erpFieldCount;$j++){
			
			if( $table == $this->erp_fields[$j]['tablename'] && $this->erp_fields[$j]['fieldformat'] >= 0 ){
				
				$erp_fieldname = $this->erp_fields[$j]['fieldname'];
				$erpfield = $this->erp_fields[$j]['erp_fieldname'];
				//$v = $this->csv_field($row[$erp_fieldname],$j);
				$buffer[$i][$erpfield] = &$row[$erp_fieldname]; //$v;
			
			}
		
		}
		
	}
	
	
	$this->free_result();
	
	//les autres tables...
	
	$tableCount = count($this->tables);
	
	for($nt=1;$nt<$tableCount;$nt++){
		
		$table = $this->tables[$nt]['tablename'];
		for($i=0;$i<$rowCount;$i++){
			
			$tblk = $this->tables[$nt]['tablekeys']; $erpk = $this->tables[$nt]['selectkeys'];
			
			$query= "SELECT `$table`.* FROM `$table` WHERE  $tblk ='".$buffer[$i][$erpk]."'" ;
			$ret = $this->select($query);// echo $query;
			
			
			if( $ret == 1 ){
				
			    $row =  $this->getrow();
			    
			    
			    for($j=0;$j<$erpFieldCount;$j++){
			    	
					if( $table == $this->erp_fields[$j]['tablename'] && $this->erp_fields[$j]['fieldformat'] >= 0 ){
						
						$fieldname = $this->erp_fields[$j]['fieldname'];
						$erpfield = $this->erp_fields[$j]['erp_fieldname'];
						//$v = $this->csv_field($row[$fieldname],$j);
						$buffer[$i][$erpfield] = &$row[$fieldname]; //$v;
						
					}
					
				}
			    
			}
		  
			$this->free_result();	
		  
		  }  
		
	}
	
	//écrire dans le fichier d'export
	
	for($i=0;$i<$rowCount;$i++){
		
		$row =  $buffer[$i];
		$virg='';
		
		for($j=0;$j<$erpFieldCount;$j++){
			
			if( $this->erp_fields[$j]['fieldformat'] >= 0 )
				{
				$erpfield = $this->erp_fields[$j]['erp_fieldname'];
				if(isset($buffer[$i][$erpfield])) 
					{
					$v = $this->csv_field($buffer[$i][$erpfield],$j);
					if( $v !== false )
						fwrite($handle, $virg.$v);
					else fwrite($handle, $virg);
					}
				else fwrite($handle, $virg);
				$virg=$this->delimit_char;
				}
		
		}
		
		fwrite($handle, $this->linebreak_char);
		
	}

	fclose($handle);
	
	$this->applyExportFunctions();
	
	chmod( $this->csv_file, 0666);
	unset($buffer);

	$msg .= "EXPORT to <a href=\"download.php?file=" . urlencode( $this->csv_file ) . "\" target=\"_blank\">" . $this->csv_file . "</a> : $rowCount rows.<br />\n";
	return $msg;
	
}

//Réorganise les tables de transposition en fonction des entetes du fichier csv 
//-----------------------------------------------------------------------------------------

function translate_table($data)
{
	
	if( DEBUG_CSV )
		echo "<p><b>TRANSLATE_TABLE $data</b></p>";
		
$num = count($data);
//$sqlfields = array();
$sqltrans = array();


for ($c=0; $c < $num; $c++)
	{
	$i = array_search($data[$c],$this->csv_fields);

	if($i === FALSE)
	{

		$sqltrans[$c] = $this->erp_fields[$i];
		$sqltrans[$c]['fieldformat']=-1;
		$sqltrans[$c]['erp_fieldname']='_IGNORE_';
		$sqltrans[$c]['fieldname']='_IGNORE_';
		//$sqlfields[$c]='_IGNORE_';
		//$sqlformats[$c]=-1;
		echo "<p class=\"msg\">La colonne '" . $data[ $c ] . "' est inconnue</p>";
		
		} 
	else
		{
		//$sqlfields[$c] = $this->erp_fields[$i]['fieldname'];
		$sqltrans[$c] = $this->erp_fields[$i];
		}
	}
$this->csv_fields = $data;

$this->erp_fields = $sqltrans;

}

//-----------------------------------------------------------------------------------------

function csv2sql()
{
	
	if( DEBUG_CSV )
		echo "<p><b>CSV2SQL</b></p>";
		
	$this->mime_type = 'text/x-sql';
	
	$row = 1;
	
	//ouvrir le fichier d'import
	
	if( !is_dir($this->dataindir) ) 
		return $this->Error('Import directory does not exist: '.$this->dataindir);
		
	$handle = fopen($this->csv_file, 'rb');
	if(!$handle) 
		return $this->Error('Unable to open intput file: '.$this->csv_file);
	
	//lire le fichier d'import
	
	if (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE) {
		
		$rowCount = count($data);
		
		if( $rowCount == 1 && $data[0]==NULL ) 
			continue;
			
		$matches=0;
		$badfields = array();
		
		//verification des entetes
		
		for ($c=0; $c < $rowCount; $c++){
			 
			 if( in_array($data[$c],$this->csv_fields ) ) 
			 	$matches++;
			 else $badfields[]=$data[$c];
			
		}
		
		if( $matches == $rowCount ) 
			$this->translate_table($data);
		else if ( $matches > 0 ){
		
			fclose($handle);
			return $this->Error('Dans ' . $this->csv_file . ' certains champs ne correspondent pas : '. implode(', ',$badfields) );
		}
		else if ( $rowCount != count($this->erp_fields) ){
			
			fclose($handle);
			return $this->Error('Dans ' . $this->csv_file . ' le nombre de champs ne correspondent pas : '. implode(', ',$badfields) );
		
		}
		else{
			
			fclose($handle);
			$handle = fopen($this->csv_file, 'rb');
		
		}
		
	}

	//créer le fichier sql
	
	$out = fopen($this->csv_file .'.sql', 'wb');
	if(!$out) 
		return $this->Error('Unable to open output file: '.$this->csv_file.'.sql');
		
	//écrire le fichier sql
	
	while (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE) {
		
	    $rowCount = count($data);
	    if( $rowCount == 1 && $data[0]==NULL ) continue;
	   
	    $row++; 
	    $sep= '';
	    
	    fwrite($out,"REPLACE {$this->table['erp_name']} SET ");
	    
	    for ($c=0; $c < $rowCount; $c++) {
	    	
		    if( $this->erp_fields[$c]['fieldformat'] >= 0 ){ 
		    
		    	$value = $this->sql_field($data[$c],$c);
		    	
		    	if( $value !== false ){
		    	
		    		fwrite($out, $sep . $this->erp_fields[$c]['fieldname'] . "=" . $value );
		    		$sep=",";
			
		    	}
		    	
		    }
			
	     } 
	        
	    fwrite($out,';'.$this->linebreak_char);
	    
	}
	
	fclose($handle);
	fclose($out);
	chmod($this->csv_file.'.sql', 0666);
	
	return true;
	
}

//-----------------------------------------------------------------------------------------

function csvimport(){

	if( DEBUG_CSV )
		echo "<p><b>CSVIMPORT</b></p>";
		
	$ups = 0 ; 
	$ins = 0; 
	$msg='';
	$this->mime_type = 'text/x-sql';
	$rows = 1;
	
	if( !file_exists( $this->csv_file ) )
		return $this->Error( "File does not exist : " . $this->csv_file );
	
	$this->applyImportFunctions();
	
	$handle = fopen($this->csv_file, 'rb');
	
	if(!$handle) 
		return $this->Error('Unable to open intput file: '.$this->csv_file);
	
	if (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE) {

		$num = count($data);
		
		if( $num == 1 && $data[0]==NULL ) 
			return $this->Error('Dans ' . $this->csv_file . ' 1ère ligne vide.');
		
		$matches=0;
		$badfields = array();
		
		//verification des entetes
		
		for ($c=0; $c < $num; $c++) {
			
			 if( in_array($data[$c],$this->csv_fields ) ) 
			 	$matches++;
			 else $badfields[]=$data[$c];
		
		}
		
		if( DEBUG_CSV ){
			
			echo "<p><b>Checking headers</b></p>";
			echo "<br />Matching fields : $matches";
			echo "<br />Uknown fields : " . count( $badfields );
			echo " (" . implode( ",", $badfields ) . ")";
			
		}
		
		// Vérification des champs obligatoires
		
		$numf = count($this->erp_fields); 
		$badfields = array();
		$matches=0;
		for ($c=0; $c < $numf; $c++) {
			
			if( $this->erp_fields[$c]['mandatory'] ){
				
				if( in_array($this->csv_fields[$c],$data) ) 
					$matches++;
				else $badfields[]=$this->csv_fields[$c];
			
			}
		
		}
		
		if( DEBUG_CSV ){
			
			echo "<p><b>Checking required fields</b></p>";
			echo "<br />Missing fields : " . count( $badfields );
			echo " (" . implode( ",", $badfields ) . ")";
			
		}
		
		if( count($badfields) > 0 ) 
			return $this->Error('Dans ' . $this->csv_file . ' des champs obligatoires manquent : '. implode(', ',$badfields) );
	
	}

	$this->translate_table($data);
	$nbtables=count($this->tables);
	
	for($nt=0;$nt<$nbtables;$nt++){
		
		$ups = 0 ; 
		$ins = 0;
		
		$table = $this->tables[$nt]['tablename'];
		if( $this->tables[$nt]['import_action'] == 'IGNORE' )
			continue;

		if( DEBUG_CSV ){
			
			if( $this->tables[$nt]['import_action'] == 'IGNORE' )
				echo "<p><b>Ignoring table : $table</b></p>";
			else echo "<p><b>Current table : $table</b></p>";
			
		}
		
		if( $this->tables[$nt]['empty_on_import'] == 1 ){

			$this->update("TRUNCATE `$table`");
			
			if( DEBUG_CSV )
				echo "<p><b>$table truncated</b></p>";
				
		}
		
		if($nt)	{
			
			fclose($handle); 
			$rows = 1;
			$handle = fopen($this->csv_file, 'rb');
			$data = fgetcsv($handle, 8100, $this->delimit_char);
			
		}
		
		//------------------------------------------------------------------------------------
	
		//récupération des clés alternatives :o(
		
		$alternative_keys = explode( ",", $this->tables[ $nt ][ "alternative_keys" ] );
		$alternative_keys_idx = array();

		foreach( $alternative_keys as $k=>$v ) 
			$alternative_keys[$k]=trim($v);
			
		$c = 0;
		while( $c < count( $this->erp_fields ) ){
			
			if( in_array( $this->erp_fields[$c]['fieldname'], $alternative_keys ) ){
				
				$v = $this->erp_fields[$c]['fieldname']; 
				$alternative_keys_idx[$v]=$c;
				
			}
			
			$c++;
			
		}
		
		if( DEBUG_CSV ){
			
			if( count( $alternative_keys ) )
				echo "<p><b>Alternative keys found :</b> " . $this->tables[ $nt ][ "alternative_keys" ] . "</p>";
			else echo "<p><b>No alternative key found</b></p>";

		}
		
		//récupération des clés uniques
		
		$tblidx = array();	
		$tksa = explode(',',$this->tables[$nt]['tablekeys']);


		foreach($tksa as $k=>$v) 
			$tksa[$k]=trim($v);
		
		for ($c=0; $c < $num; $c++) {
			
			if(in_array($this->erp_fields[$c]['fieldname'],$tksa)) { 
				
				$v=$this->erp_fields[$c]['fieldname']; 

				$tblidx[$v]=$c; 
				
			}

		}
		
		if( DEBUG_CSV ){
			
			if( count( $alternative_keys ) )
				echo "<p><b>Primary keys found :</b> " . $this->tables[ $nt ][ "tablekeys" ] . "</p>";
			else echo "<p><b>No primary key found</b></p>";

		}
		
		//spécificités de certaines tables
		
		 $fiderp=-1; $foldid=-1; $ffield = '';
		 if( $table == 'buyer' || $table == 'delivery' 
		 	|| $table == 'real_product' || $table == 'product' 
		 	|| $table == 'estimate' || $table == 'order'){
				
			$ffield = 'id' . $table ;
			
			if($table == 'order') 
				$ffield='idorder';
			
			for ($c=0; $c < $num; $c++){
				
				if( $this->erp_fields[$c]['fieldname'] == 'iderp' ) $fiderp = $c;
				if( $this->erp_fields[$c]['fieldname'] == $ffield ) $foldid = $c; 
				
			}
				
		}
		
		//lecture du fichier CSV
		
		 while (($data = fgetcsv( $handle, 8100, $this->delimit_char)) !== FALSE){
		    
		 	if( DEBUG_CSV && isset( $_GET[ "limit" ] ) ){
		 	
		 		if( $rows > intval( $_GET[ "limit" ] ) )
		 			exit( "limit reached" );
		 		
		 	}
		 	
		    $rows++; 
		    
		    $num = count($data);
		   
		    if( $num == 1 && $data[0]==NULL )  //ligne vide
		    	continue;
		    	
		    
			if( $foldid > -1 &&  $fiderp > -1 &&  $data[$foldid] <= 0 ) //get old id to prevent autoincrement on replace
			{
				
				$query = "SELECT $ffield  as oldid FROM `$table`  WHERE iderp='" . $data[$fiderp] . "'" ;
				
				$n = $this->select($query);
				if($n) { 
					
					$row =  $this->getrow(); 
					$id=$row['oldid']; 
					$data[$foldid]=$id; 
					
				}
				
				$this->free_result();
	
			}  

		    $sep= ''; 

		    if( $this->tables[$nt]['import_action'] == 'UPDATE' )
		    {

			    $primaryKeysAvailable = count( $tblidx ) > 0; //des clés primaires sont là
   
			    $alternativeKeysAvailable = !$primaryKeysAvailable;
			    $alternativeKeysAvailable &= count( $alternative_keys_idx ) > 0; //des clés alternatives sont là
			    $alternativeKeysAvailable &= count( $alternative_keys_idx ) == count( $alternative_keys ); //toutes les clés alternatives sont là
			    $alternativeKeysAvailable &= count( $tblidx ) == 0; //aucune clé primaire n'est là
				  
				//vérifier s'il existe des valeurs pour les clés uniques dans la ligne courante
				
				$missingKeys = array();
				
				if( $primaryKeysAvailable ){
					
				    foreach( $tblidx as $k => $v ){
				    
				    	$value = $this->sql_field( $data[$v], $v );
				     	if(   $value == "NULL" || $value === false )
				     		$missingKeys[] =  $k;

				    }
				     	
				}
				else if( $alternativeKeysAvailable ){
					
					foreach( $alternative_keys_idx as $k => $v ){
					
						$value = $this->sql_field( $data[$v], $v );
				     	if( $value  == "NULL" || $value === false )
				     		$missingKeys[] =  $k;
				     		
					}
				     	
				}
			     
			     if( count( $missingKeys ) ){
			     	 
			     	echo "<p class=\"msg\">La ligne $rows a été ignorée car le format des clés suivantes est incorrect : " . implode( ",", $missingKeys ) . "</p>";

			     }
			     else if( $primaryKeysAvailable ){ //vérifier un enregistrement existe déjà pour les clés uniques

				    $query = "SELECT ". $this->tables[$nt]['tablekeys'] . " FROM `$table` WHERE ";
			   		$and='';
			   		foreach($tblidx as $k=>$v) {
		
			   			$query .= $and. "`$k`=".$this->sql_field($data[$v],$v);
			   			$and=' AND ';
			   		}
			   		
			   		$query .= ' LIMIT 1';
		
					$n = $this->select($query);
			    	$this->free_result();
					
			    }
			    else if( $alternativeKeysAvailable ){ //vérifier un enregistrement existe déjà pour les clés alternatives :o(

			    	$query = "SELECT ". $this->tables[$nt]['alternative_keys'] . " FROM `$table` WHERE ";
			   		$and='';
			   		foreach($alternative_keys_idx as $k=>$v) {
			   			$query .= $and. "`$k`=".$this->sql_field($data[$v],$v);
			   			$and=' AND ';
			   		}
			   		
			   		$query .= ' LIMIT 1';
			    
			    	$n = $this->select($query);
			    	
			   	 	$this->free_result();
			    	
			    }	
				else $n = 0;
				
			    if( $n > 0 ) { 
			    	
			    	$query = "UPDATE `$table` SET "; 
			    	$action = "UPDATE";
			    	
			    }
			    else if( $n == 0 ){ 
			    	
			    	$query = "INSERT INTO `$table`"; 
			    	$action = "INSERT"; 
			    	
			    }

			}
			else if( $this->tables[$nt]['import_action'] == 'REPLACE' ){
				
				$query = "REPLACE `$table` SET ";
				$action = "REPLACE";
				die( "Do not use action 'REPLACE'" );
				
			}
			else{
				
				?><p class="msg">Unknow action '<?php echo $this->tables[$nt]['import_action'] ?>' for table '<?php echo $table ?>'"</p><?php 
				
				return $this->Error( "Action inconnue '" . $this->tables[$nt]['import_action'] . "' pour la table '$table'" );
				
			}

			if( count( $missingKeys ) ) //ne pas traiter la ligne
				continue;
					 
			//-----------------------------------------------------------------------------------------------------
			
			if( $action == "UPDATE" ){ //UPDATE
				
				for ($c=0; $c < $num; $c++) {
							
			   		if( $this->erp_fields[$c]['fieldformat'] >= 0 && $this->erp_fields[$c]['tablename']==$table && $data[$c] !='' ){
			
			   			$value = $this->sql_field($data[$c],$c);
			   			
			   			if( $value !== false ){
			   			
					    	$query .=  $sep . "`" . $this->erp_fields[$c]['fieldname'] . "`" ."=" . $value;
					    	$sep=",";
					    	
			   			}
			    	
					} 
					   
			    }
			    
			}
			else if( $action == "INSERT" ){ //INSERT
				
				$query .= " (";
				$sep = "";
				
				//ajout des clés uniques
				
				for ( $c=0; $c < count( $tksa ); $c++ ){
					
					$key = $tksa[ $c ];
					
					if( !$this->erp_field_exists( $key )  ){
						
						$query .= "$sep`$key`";
						$sep = ",";
						
					}
					
				}
				
				//ajout des autres colonnes
			
				for ( $c=0; $c < $num; $c++ ){
					
					if( $this->erp_fields[$c]['fieldformat'] >= 0 
						&& $this->erp_fields[$c]['tablename']==$table 
						&& $data[$c] !='' ){
						
						$query .= "$sep`" . $this->erp_fields[ $c ][ "fieldname" ] . "`";
						$sep = ",";
						
					}
					
				}
				
				//valeurs des clés uniques
				
				$query .= " ) VALUES (";
				$sep = "";

				for ( $c=0; $c < count( $tksa ); $c++ ){
					
					$key = $tksa[ $c ];
					
					if( !$this->erp_field_exists( $key )  ){
						
						$query .= "$sep'" . $this->getNewValue( $table, $key ) . "'";
						$sep = ",";
						
					}
					
				}
				
				//valeurs des autres colonnes
				
				for ( $c=0; $c < $num; $c++ ){

					if( $this->erp_fields[$c]['fieldformat'] >= 0 
						&& $this->erp_fields[$c]['tablename']==$table 
						&& $data[$c] !='' ){
						
						$value = $this->sql_field( $data[ $c ], $c );
						if( $value !== false ){
						
							$query .= $sep . $value;
							$sep = ",";
							
						}
						
					}
					
				}
					
				$query .= " )";
	
				
			}

			//CONDITION
			
		    if( $action == "UPDATE" ) {
		    	
		    	if( $primaryKeysAvailable ){
		    		
			    	$query .= ' WHERE ';
					$and='';
					
			    	foreach($tblidx as $k=>$v) {
			    		
			    		$value = $this->sql_field($data[$v],$v);
			    		
			    		if( $value !== false ){
			    		
				   			$query .= $and. "`$k`=".$value;
				   			$and=' AND ';
				   			
			    		}
			   			
			   		}
			   		
			   		$query .= ' LIMIT 1';
			   		
		    	}
		    	else if( $alternativeKeysAvailable ){
		    		
		    		$query .= " WHERE";
			    	$p = 0;
					foreach( $alternative_keys_idx as $k => $v ){
						
						if( $p )
							$query .= " AND";
						
						$value = $this->sql_field( $data[ $v ], $v );
						
						if( $value !== false ){
						
							$query .= " `$k` = " . $value;
							$p++;
							
						}
						
					}
					
					$query .= ' LIMIT 1';
				
		    	}
		    	else die( "oups..." );
		   		
			}
			
		    $n = $this->update($query);
    		
		  /////  echo "status = $n $query<br />\n";
		   if($n == 2 )$ups++;
		   elseif($n == 1 && $action == "UPDATE" )$ups++;
		   elseif($n == 1 && $action == "INSERT" )$ins++;
		 	
		 }
		 
		 if( $nt == 0 ) 
		 	$msg .= "<p>$rows LIGNES TRAITEES DANS LE FICHIER '{$this->csv_file}'</p>";
		 	
		 $msg .= "<p>IMPORT in table $table : $ups rows updated , $ins rows inserted.</p>";
		
	}
		
	fclose($handle);

	return $msg ;
	
}



//-----------------------------------------------------------------------------------------

function update_indexes()
{
	
	if( DEBUG_CSV )
		echo "<p><b>UPDATE_INDEXES</b></p>";
		
//DELIVERY-----BUYER
$n = $this->select('SELECT iderp,idbuyer_erp FROM delivery WHERE idbuyer=0 OR idbuyer is NULL');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$idbuyer_erp = $row['idbuyer_erp'];
	$iderp=$row['iderp'];
	if( $idbuyer_erp != '0')
	 {
	 $nn = $this->select2("SELECT idbuyer FROM buyer WHERE iderp='$idbuyer_erp'");
	 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'acheteur iderp='$idbuyer_erp' dans delivery iderp='$iderp'"; 
	 else
		{
		$row2 =  $this->getrow2();
		$idbuyer = $row2['idbuyer'];
		$this->update("UPDATE delivery SET idbuyer='$idbuyer' WHERE iderp='$iderp'");
		//echo "UPDATE delivery SET idbuyer=$idbuyer WHERE iderp=$iderp";
		}
	$this->free_result2();
	 }
	}
$this->free_result();
//order----BUYER
$n = $this->select('SELECT iderp,idbuyer_erp FROM `order` WHERE idbuyer=0 OR idbuyer is NULL');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$iderp = $row['iderp'];
	$idbuyer_erp = $row['idbuyer_erp'];
	if( $idbuyer_erp != '')
	 {
	 $nn = $this->select2("SELECT idbuyer FROM buyer WHERE iderp='$idbuyer_erp'");
	 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'acheteur iderp='$idbuyer_erp' dans order iderp='$iderp'"; 
	 else
		{
		$row2 =  $this->getrow2();
		$idbuyer = $row2['idbuyer'];
		$this->update("UPDATE `order` SET idbuyer='$idbuyer' WHERE iderp='$iderp'");
		//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
		}
	 $this->free_result2();
	 }
	}
$this->free_result();
//order-----DELIVERY
$n = $this->select('SELECT iderp,iddelivery_erp FROM `order` WHERE iddelivery_erp>0 AND iddelivery=0');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$iderp = $row['iderp'];
	$iddeliveryr_erp = $row['iddelivery_erp'];
	if( $iddeliveryr_erp != '')
	 {
	 $nn = $this->select2("SELECT iddelivery FROM delivery WHERE iderp='$iddeliveryr_erp'");
	 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'adresse iderp='$iddeliveryr_erp' dans order iderp='$iderp'"; 
	 else
		{
		$row2 =  $this->getrow2();
		$iddelivery = $row2['iddelivery'];
		$this->update("UPDATE `order` SET iddelivery='$iddelivery' WHERE iderp='$iderp'");
		//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
		}
	 $this->free_result2();
	 }
	}
$this->free_result();


//ORDER_ROW-----order
$n = $this->select('SELECT distinct idorder_erp FROM order_row WHERE idorder=0 or idorder is NULL');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$iderp = $row['idorder_erp'];
	if( $iderp != '')
	 {
	 $nn = $this->select2("SELECT idorder FROM `order` WHERE iderp='$iderp'");
	 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver la commande iderp='$iderp'"; 
	 else
		{
		$row2 =  $this->getrow2();
		$idorder = $row2['idorder'];
		$this->update("UPDATE order_row SET idorder='$idorder' WHERE idorder_erp='$iderp'");
		}
	 $this->free_result2();
	 }
	}
$this->free_result();




//ESTIMATE----BUYER
$n = $this->select('SELECT iderp,idbuyer_erp FROM estimate WHERE idbuyer=0 OR idbuyer is NULL');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$iderp = $row['iderp'];
	$idbuyer_erp = $row['idbuyer_erp'];
	if( $idbuyer_erp != 0)
	 {
	 $nn = $this->select2("SELECT idbuyer FROM buyer WHERE iderp='$idbuyer_erp'");
	 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'acheteur iderp='$idbuyer_erp' dans estimate iderp='$iderp'"; 
	 else
		{
		$row2 =  $this->getrow2();
		$idbuyer = $row2['idbuyer'];
		$this->update("UPDATE estimate SET idbuyer='$idbuyer' WHERE iderp='$iderp'");
		//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
		}
	 $this->free_result2();
	 }
	}
$this->free_result();

//ESTIMATE-----DELIVERY
$n = $this->select('SELECT iderp,iddelivery_erp FROM estimate WHERE iddelivery_erp>0 AND iddelivery=0');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$iderp = $row['iderp'];
	$iddeliveryr_erp = $row['iddelivery_erp'];
	$nn = $this->select2("SELECT iddelivery FROM delivery WHERE iderp='$iddeliveryr_erp'");
	if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'adresse iderp='$iddeliveryr_erp' dans estimate iderp='$iderp'"; 
	else
		{
		$row2 =  $this->getrow2();
		$iddelivery = $row2['iddelivery'];
		$this->update("UPDATE estimate SET iddelivery='$iddelivery' WHERE iderp='$iderp'");
		//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
		}
	$this->free_result2();
	}
$this->free_result();

//ESTIMATE_ROW-----ESTIMATE
$n = $this->select('SELECT distinct idestimate_erp FROM estimate_row WHERE idestimate=0 or idestimate is NULL');
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	$iderp = $row['idestimate_erp'];
	$nn = $this->select2("SELECT idestimate FROM estimate WHERE iderp='$iderp'");
	if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver la commande iderp='$iderp' dans estimate_row idestimate_erp='".$row['idestimate_erp']."'"; 
	else
		{
		$row2 =  $this->getrow2();
		$idoder = $row2['idestimate'];
		$this->update("UPDATE estimate_row SET idestimate='$idoder' WHERE idestimate_erp='$iderp'");
		}
	$this->free_result2();
	}
$this->free_result();

}


//-----------------------------------------------------------------------------------------

function get_format($v)
{
//int(11) varchar(200) decimal(11,2)
if( strpos($v,'char') !== FALSE )  return 0;
if( strpos($v,'int') !== FALSE )  return 1;
if( strpos($v,'dec') !== FALSE )  return $this->default_number_format;
if( strpos($v,'float') !== FALSE )  return $this->default_number_format;
if( strpos($v,'dateheure') !== FALSE )  return 8;
if( strpos($v,'date') !== FALSE )  return 7;
if( strpos($v,'text') !== FALSE )  return 0;
if( strpos($v,'autoincrement') !== FALSE )  return 20;
if( strpos($v,'ignore') !== FALSE )  return -1;
return 0;
}

//-----------------------------------------------------------------------------------------

function sql_field($text,$i){

$text = str_replace("\\",'',$text);

switch($this->erp_fields[$i]['fieldformat']) {
 	case 0 : 
 	
 		if( !empty($this->erp_fields[$i]['trans_table']) ){
 		
 			$value = $this->translate2text($i,$text,$text);
 			
 			if( $value === false )
 				return false;
 				
 			return '"'. $value .'"';
 			
 		}
	
 		break;
 		
 	case 1 : 
	
		if( !empty($this->erp_fields[$i]['trans_table']) ){
		
			$text = $this->translate2num($i,$text);

			if( $text === false )
				return false;
				
		}
			
		 $text = ereg_replace("([^-0123456789])", '', $text);
		 
		 if( $text === '' ) 
		 	return 'NULL';
		 	
		 break;
		 
	case 2 : 

		$text = str_replace(',', '.',$text);
		$text = ereg_replace("([^-0123456789.])", '', $text);
		$text = number_format( $text, 2, ".", "" );

		return $text;
	
	case 3 : $text = number_format($text,2,',',' '); //Français
		break;
	case 4 : $text = str_replace(',', '',$text); 
			$text = number_format($text); //Anglais
			break;
	case 5 : $text = number_format($text, 2, '.', ''); //Math
		break;
	case 7 : $text = $this->sql_date_format($text); //date
		break;
	case 8 : $text = $this->sql_dateheure_format($text); //date	
		break;
	case 20 : return $this->newid($text);
	}
return "'" . mysqli_escape_string($text) . "'"; ///////str_replace("'","\\'",$text);
}

//-----------------------------------------------------------------------------------------

function excel_field($text,$i)
{
 switch($this->erp_fields[$i]['fieldformat']) {
 	case 0 : return trim($text);
	case 1 : return ereg_replace("([^-0123456789])", '', $text);
	case 2 : return number_format($text,2,',',' '); //Français
	case 3 : $text = str_replace(',', '.',$text);
		$text = ereg_replace("([^-0123456789.])", '', $text);
		break;
	case 4 : return number_format($text); //Anglais
	case 5 : return number_format($text, 2, '.', ''); //Math
	case 7 : return $this->csv_date_format($text,$i); //date
	case 8 : return $this->sql_dateheure_format($text); //date
	case 20 : return ereg_replace("([^-0123456789])", '', $text);//$this->newid();
	}
return $text;
}

//-----------------------------------------------------------------------------------------

function csv_field($text,$i){

$text = str_replace("\\",'',$text);
 switch($this->erp_fields[$i]['erp_fieldformat']) {
 	case 0 :  
 	
 		if( !empty($this->erp_fields[$i]['trans_table']) ) 
 			return '"'.$this->translate2text($i,$text,$text).'"';
 			
 			return '"'.str_replace('"','""',trim($text)).'"';
 			
	case 1 : 
	
		if( !empty($this->erp_fields[$i]['trans_table']) ) 
			return $this->translate2text($i,$text,$text); 
		 $text = ereg_replace("([^-0123456789])", '', $text);
		 $x = 0 + $this->erp_fields[$i]['size'];
		 if( strlen($text) > $x )  $text = substr($text,0,$x);
		 return $text;
	case 2 : 
	
		if( $this->delimit_char == ',' ) 
			return '"'.number_format($text,2,',',' ').'"';
		return number_format($text,2,',',' '); //Français
		
	case 3 : $text = str_replace(',', '.',$text);
		$text = ereg_replace("([^-0123456789.])", '', $text);
		break;
	case 4 : return number_format($text,2); //Anglais
	case 5 : return number_format($text, 2, '.', ''); //Math
	case 7 : return $this->csv_date_format($text,$i); //date
	case 8 : return $this->csv_dateheure_format($text,$i); //date
	case 20 : return ereg_replace("([^-0123456789])", '', $text);//$this->newid();
	}
return '"'.str_replace('"','""',$text).'"';
}

//-----------------------------------------------------------------------------------------

function sql_date_format($d)
{
if (preg_match ("([0-9]{1,4})[^0-9]([0-9]{1,2})[^0-9]([0-9]{1,4})", $d, $regs)) {
   // echo "$regs[3].$regs[2].$regs[1]";
    if( $regs[3] > 1900 ) return sprintf("%d-%02d-%02d",$regs[3],$regs[2],$regs[1]);
    if( $regs[1] > 1900 ) return sprintf("%d-%02d-%02d",$regs[1],$regs[2],$regs[3]); // 
    if( strpos($d,'/') !== FALSE ) return sprintf("%d-%02d-%02d",$regs[3]+2000,$regs[2],$regs[1]); // french format
    if( $this->default_date_fr ) return sprintf("%d-%02d-%02d",$regs[3]+2000,$regs[2],$regs[1]); // french format
    else return sprintf("%4d-%02d-%02d",$regs[3]+2000,$regs[1],$regs[2]);// english format
} else {
	return $this->Error('Dans ' . $this->csv_file . ' Format de date invalide : '. $d );
	}
	//return $d;
}

//-----------------------------------------------------------------------------------------

function sql_dateheure_format($d)
{
if (preg_match ("([0-9]{1,4})[^0-9]([0-9]{1,2})[^0-9]([0-9]{1,4}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})", $d, $regs)) {
   // echo "$regs[3].$regs[2].$regs[1]";
    if( $regs[3] > 1900 ) return sprintf("%d-%02d-%02d",$regs[3],$regs[2],$regs[1]);
    if( $regs[1] > 1900 ) return sprintf("%d-%02d-%02d",$regs[1],$regs[2],$regs[3]); // 
    if( strpos($d,'/') !== FALSE ) return sprintf("%d-%02d-%02d",$regs[3]+2000,$regs[2],$regs[1]); // french format
    if( $this->default_date_fr ) return sprintf("%d-%02d-%02d %02d:%02d:%02d",$regs[3]+2000,$regs[2],$regs[1],$regs[4],$regs[5],$regs[6]); // french format
    else return sprintf("%4d-%02d-%02d %02d:%02d:%02d",$regs[3]+2000,$regs[1],$regs[2],$regs[4],$regs[5],$regs[6]);// english format
} else {
	return $this->Error('Dans ' . $this->csv_file . ' Format de date/heure invalide : '. $d );
	}
	//return $d;
}

//-----------------------------------------------------------------------------------------

function csv_date_format($d,$i)
{
if (preg_match ("([0-9]{1,4})[^0-9]([0-9]{1,2})[^0-9]([0-9]{1,4})", $d, $regs)) {
   // echo "$regs[3].$regs[2].$regs[1]";
   if( $this->default_date_fr ) {
    if( $regs[1] > 1900 ) return sprintf("%02d/%02d/%d",$regs[3],$regs[2],$regs[1]);
    else return sprintf("%02d/%02d/%d",$regs[1],$regs[2],$regs[3]+2000);
   } else {
   	if( $regs[1] > 1900 ) return sprintf("%d/%02d/%02d",$regs[1],$regs[2],$regs[3]);
    else return sprintf("%02d/%02d/%d",$regs[1]+2000,$regs[2],$regs[3]);
   }
} else {
    // echo "Format de date invalide : $d"; 
    return $this->Error('Dans (' . $this->table['erp_name'] . ')['.$this->erp_fields[$i]['fieldname'].'] Format de date invalide : '. $d );
}
//return $d;
}

//-----------------------------------------------------------------------------------------

function csv_dateheure_format($d,$i)
{
if (preg_match ("([0-9]{1,4})[^0-9]([0-9]{1,2})[^0-9]([0-9]{1,4}) ([0-9]{1,2}):([0-9]{1,2}):([0-9]{1,2})", $d, $regs)) {
   // echo "$regs[3].$regs[2].$regs[1]";
    if( $this->default_date_fr ) {
    if( $regs[1] > 1900 ) return sprintf("%02d/%02d/%d %02d:%02d:%02d",$regs[3],$regs[2],$regs[1],$regs[4],$regs[5],$regs[6]);
    else return sprintf("%02d/%02d/%d %02d:%02d:%02d",$regs[1],$regs[2],$regs[3]+2000,$regs[4],$regs[5],$regs[6]);
    } else {
    	if( $regs[1] > 1900 ) return sprintf("%d-%02d-%02d %02d:%02d:%02d",$regs[1],$regs[2],$regs[3],$regs[4],$regs[5],$regs[6]);
		else return sprintf("%d-%02d-%02d %02d:%02d:%02d",$regs[1]+2000,$regs[2],$regs[3],$regs[4],$regs[5],$regs[6]);
    }
} else {
    // echo "Format de date invalide : $d"; 
    return $this->Error('Dans (' . $this->table['erp_name'] . ')['.$this->erp_fields[$i]['fieldname'].'] Format de date/heure invalide : '. $d );
}
//return $d;
}

//-----------------------------------------------------------------------------------------

function clean_file($text){
	$rows = explode($this->linebreak_char,$text);
	$return = '';
	while(list($key,$val)=each($rows)){
	//replace commas apostrophies and quotes.... these are flipped back to propper values in the create_sql function	
		$val = preg_replace("/\,\"(.*)([\,])(.*)\"\,/", ',"\\1&comma;\\3",',  $val, -1);
		$val = str_replace("\"\"", "&quote;", $val);
		$val = str_replace("'", "&appos;", $val);
		$return .= $val."\n";
	}
return($return);
}

//-----------------------------------------------------------------------------------------

function ExcelOpen($file)
{
$header = <<<EOH
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=us-ascii">
<meta name=ProgId content=Excel.Sheet>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:LastAuthor>Interact</o:LastAuthor>
  <o:Created>2005-07-14T08:47:23Z</o:Created>
  <o:LastSaved>2005-07-14T08:59:23Z</o:LastSaved>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:"\ ";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:solid grey 1pt;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	white-space:normal;}
.xl25
        {mso-style-parent:style0;
        mso-number-format:General;
        text-align:right;
        white-space:normal;}
.xl26
        {mso-style-parent:style0;
        font-weight:700;
        font-family:Arial, sans-serif;
        mso-font-charset:0;
        text-align:center;
        background:#DDDDDD;
        mso-pattern:auto none;
        white-space:normal;}
.xl27
        {mso-style-parent:style0;
        mso-number-format:"\#\,\#\#0\.00\\ \0022\20AC\0022";}
.xl28
        {mso-style-parent:style0;
        mso-number-format:"d\/m\/yyyy";}
.xl29
        {mso-style-parent:style0;
        mso-number-format:Fixed;
        text-align:right;
        white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
	<x:Name>feuille 1</x:Name>
	<x:WorksheetOptions>
	 <x:Selected/>
	 <x:ProtectContents>False</x:ProtectContents>
	 <x:ProtectObjects>False</x:ProtectObjects>
	 <x:ProtectScenarios>False</x:ProtectScenarios>
	</x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>10005</x:WindowHeight>
  <x:WindowWidth>10005</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>135</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>
<table x:str border=0 cellpadding=0 cellspacing=0 style='border-collapse: collapse;'>
EOH;
if( is_file($file ) )  unlink($file);
$this->fp=fopen($file,'wb');
if(!$this->fp) return $this->Error('Unable to create output file: '.$file);
fwrite($this->fp,$header);
return $header;
}

//-----------------------------------------------------------------------------------------

function ExcelClose()
{
fwrite($this->fp,"</table>\n</body>\n</html>\n");
fclose($this->fp);
}

//-----------------------------------------------------------------------------------------

function writeExcelRow($line_arr)
{
	fwrite($this->fp,"<tr>");
	foreach($line_arr as $col)
		fwrite($this->fp,"<td class=xl24 width=120 >$col</td>");
	fwrite($this->fp,"</tr>");
}

//-----------------------------------------------------------------------------------------

function excel()
{
$this->do_filter();
$this->mime_type = 'application/msexcel';
$this->ExcelOpen($this->csv_file); //$handle = fopen($this->csv_file, 'wb');
$m = count($this->erp_fields);
$fieldlist='';$virg='';
 for($j=0;$j<$m;$j++) 
	{
	if( $this->erp_fields[$j]['fieldformat'] >= 0 )
		{
		$fieldlist .= $virg  . "`" . $this->erp_fields[$j]['fieldname'] . "`" ;
		$virg=',';
		} 
	}
$query= "SELECT * FROM " . $this->tables[0]['tablename'] .' '. $this->tables[0]['export_filter'] ;
$n = $this->select($query);


for($j=0;$j<$m;$j++) 
	if( $this->erp_fields[$j]['fieldformat'] >= 0 ) 
		fwrite($this->fp,"<col width=138 style='mso-width-source:userset;mso-width-alt:5046;width:104pt'>\n");


fwrite($this->fp,"<tr>\n");
for($j=0;$j<$m;$j++) if( $this->erp_fields[$j]['fieldformat'] >= 0 ) fwrite($this->fp,"<td class=xl26 width=120>".$this->erp_fields[$j]['erp_fieldname']."</td>\n");
fwrite($this->fp,"</tr>\n");
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();
	fwrite($this->fp,"<tr>\n");
	for($j=0;$j<$m;$j++)
		{
		if( $this->erp_fields[$j]['fieldformat'] >= 0 )
			{
			$f = $this->erp_fields[$j]['fieldname'];
			//$v = $this->excel_field($row[$f],$j);
			$v = str_replace('>','&gt;',str_replace('<','&lt;',$this->excel_field($row[$f],$j)));
			if($this->erp_fields[$j]['fieldformat'] == 2 )  fwrite($this->fp,"<td class=xl29 align=right x:num=\"".$row[$f]."\" width=64 >$v</td>\n");
			else if($this->erp_fields[$j]['fieldformat'] == 1 )  fwrite($this->fp,"<td class=xl25 align=right width=64 x:num>$v</td>\n");
			else if($this->erp_fields[$j]['fieldformat'] == 7 )  
				{
				$t = floor( mktime(7, 0, 0, substr($row[$f],5,2),substr($row[$f],8,2),substr($row[$f],0,4)) / 86400 )+25569;
				fwrite($this->fp,"<td class=xl28 align=right  width=64 x:num=\"$t\" >$v</td>\n");
				}
			else fwrite($this->fp,"<td class=xl24 width=150 >$v</td>\n");
			}
		}
	fwrite($this->fp,"</tr>\n");
	}
$this->ExcelClose();
}

//-----------------------------------------------------------------------------------------

function exportstat()
{
$this->mime_type = 'text/x-csv';
$csv__fields = array('idsession','DateHeure','action','quantity','idcategory','url','parmsearch','reference','username');
$sql__fields = array('idsession','DateHeure','action','quantity','idcategory','url','parmsearch','reference','username');
$sql__formats = array(0,7,0,1,1,0,0,0,0);
for($i=0;$i<9;$i++) { $this->erp_fields[$i]['fieldformat']=0;$this->erp_fields[$i]['trans_table']=''; }
$this->erp_fields[1]['fieldformat']=7;$this->erp_fields[3]['fieldformat']=1;$this->erp_fields[4]['fieldformat']=1;
$m = count($csv__fields);
$avant_yier = mktime(0, 0, 0, date('m'), date('d')-2, date('Y'));
$yier =  mktime(0, 0, 0, date('m'), date('d')-1, date('Y'));
if( $this->csv_file == '' ) $this->csv_file = $this->dataoutdir .'stat_'. date("d-m",$avant_yier) . date("_d-m-Y",$yier) .  ".csv";
else $this->csv_file .= date("d-m",$avant_yier) . date("_d-m-Y",$yier) .  ".csv";
if( is_file($this->csv_file ) )  unlink($this->csv_file);
$handle = fopen($this->csv_file, 'wb');
if(!$handle) return $this->Error('Unable to create output file: '.$this->csv_file);
$avant_yier = mktime(0, 0, 0, date('m'), date('d')-2, date('Y'));
$yier =  mktime(0, 0, 0, date('m'), date('d')-1, date('Y'));
$this->table['erp_name'] = 'stat'. date('m',$avant_yier);
$n = $this->select('SELECT * FROM '.$this->table['erp_name']." WHERE DateHeure like '". date("Y-m-d",$avant_yier) . "%'" );
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();$virg='';
	for($j=0;$j<$m;$j++)
		{
		if( $sql__formats[$j] >= 0 )
			{
			$f = $sql__fields[$j];
			$v = $this->csv_field($row[$f],$j);
			
			if( $v !== false )
				fwrite($handle,$virg.$v);
			else fwrite($handle,$virg);
			
			$virg=$this->delimit_char;
			}
		}
	fwrite($handle, $this->linebreak_char);
	}
$this->free_result();
$this->table['erp_name'] = 'stat'. date('m',$yier);
$n = $this->select('SELECT * FROM '.$this->table['erp_name']." WHERE DateHeure like '". date("Y-m-d",$yier) . "%'" );
for($i=0;$i<$n;$i++)
	{
	$row =  $this->getrow();$virg=0;
	for($j=0;$j<$m;$j++)
		{
		if( $sql__formats[$j] >= 0 )
			{
			$f = $sql__fields[$j];
			$v = $this->csv_field($row[$f],$j);

			if($virg){
			
				if( $v !== false )
					fwrite($handle,$this->delimit_char.$v);
				else fwrite($handle,$this->delimit_char);
				
			}
			
			$virg=1;
			}
		}
	fwrite($handle, $this->linebreak_char);
	}
$this->free_result();
fclose($handle);
chmod($this->csv_file, 0666);
}


//-----------------------------------------------------------------------------------------

function Output($name='',$dest='')
{
	//Output to some destination
	$dest=strtoupper($dest);
	if($dest=='')
	{
		if($name=='')
		{
			$name='feuille.xls';
			$dest='I';
		}
		else
			$dest='F';
	}
	switch($dest)
	{
		case 'I':
			//Send to standard output
			if(ob_get_contents())
				return $this->Error('Some data has already been output, can\'t send file');
			if(php_sapi_name()!='cli')
			{
			//We send to a browser
			///////header('Content-Type: application/msexcel');
			header('Content-Type: '.$this->mime_type);
			if(headers_sent())
				return $this->Error('Some data has already been output to browser, can\'t send file');
			/////////header('Content-Length: '.strlen($this->buffer));
			header('Content-disposition: inline; filename="'.$name.'"');
			}
			//echo $this->buffer;
			
			readfile($this->csv_file); exit();
			break;
		case 'D':
			//Download file
			if(ob_get_contents())
				return $this->Error('Some data has already been output, can\'t send file');
			if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
				header('Content-Type: application/force-download');
			else
				header('Content-Type: application/octet-stream');
			if(headers_sent())
				return $this->Error('Some data has already been output to browser, can\'t send file');
			///////header('Content-Length: '.strlen($this->buffer));
			header('Content-disposition: attachment; filename="'.$name.'"');
			readfile($this->csv_file); exit();//echo $this->buffer;
			break;
		case 'Z':
			//Download Zip file
			if(ob_get_contents())
				return $this->Error('Some data has already been output, can\'t send file');
			if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
				header('Content-Type: application/force-download');
			else
				header('Content-Type: application/octet-stream');
			if(headers_sent())
				return $this->Error('Some data has already been output to browser, can\'t send file');
			///////header('Content-Length: '.strlen($this->buffer));
			header('Content-disposition: attachment; filename="'.$name.'.gz"');
			$data = implode("", file($this->csv_file));
			echo gzencode($data, 9);exit();
			//readfile($this->csv_file); //echo $this->buffer;
			break;
		case 'F':
			//Save to local file
			$f=fopen($name,'wb');
			if( is_file($name) )  unlink($name);
			if(!$f) return $this->Error('Unable to create output file: '.$name);
			$data = implode("", file($this->csv_file));
			fwrite($f,$data,strlen($data));
			fclose($f);
			break;
		case 'S':
			//Return as a string
			readfile($this->csv_file);
			break;
		case 'C': //do nothing 
			echo 'Fichier créé : '.$this->csv_file;
			break;
		default:
			return $this->Error('Incorrect output destination: '.$dest);
	}
	return '';
}

//Error handler
//-----------------------------------------------------------------------------------------

function Error($msg)
{
	$this->aErrors[] = $msg;
	return FALSE;
}

//-----------------------------------------------------------------------------------------

function iserror()
{
 return count($this->aErrors);
}

//-----------------------------------------------------------------------------------------

function geterrors()
{
 return $this->aErrors;
}

//-----------------------------------------------------------------------------------------

function report()
{
 $n = count($this->aErrors);
 for($i=0;$i<$n;$i++) echo "<b>" . $this->aErrors[$i],"</b><br />\n";
}

//-----------------------------------------------------------------------------------------

function dump()
{
$row = 1;
$handle = fopen($this->csv_file, 'rb');
echo "Fichier : $this->csv_file\n";
echo "---------------------------------------\n";
while (($data = fgetcsv($handle, 8100, ",")) !== FALSE) {
    $num = count($data);
    echo "<p> $num fields in line $row: <br /></p>\n";
    $row++;
    echo "---------------------------------------\n";
    for ($c=0; $c < $num; $c++) {
        echo $data[$c] . "\n";
    }
}
fclose($handle);
chmod($this->csv_file, 0666);
}

//-----------------------------------------------------------------------------------------

	/**
	 * Retourne une nouvelle valeur disponible pour une clé primaire
	 * @param $table le nom de la table
	 * @param $column le nom de la colonne
	 * @return int une nouvelle valeur disponible pour la colonne $column dans la table $table
	 */
	function getNewValue( $table, $column ){
		
		$query = "SELECT MAX( `$column` ) + 1 FROM `$table`";
		
		$result = mysqli_query( $this->db,$query  );
		$value = Util::mysqli_result( $result, 0 );
		
		if( $value == null )
			return 1;
			
		return $value;
		
	}

//-----------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si une colonne est présente dans le fichier d'import/export
	 * @param $fieldname le nom du champ
	 * @return boolean true si le champ est présent, sinon false
	 */
	function erp_field_exists( $fieldname ){
		
		$i = 0;
		while( $i < count( $this->erp_fields ) ){
		
			if( $this->erp_fields[ $i ][ "fieldname" ] == $fieldname )
				return true;
					
			$i++;
			
		}
		
		return false;
		
	}

//-----------------------------------------------------------------------------------------

function applyImportFunctions(){
	
	$csvhandler = new CSVHandler( $this->table[ "erp_name" ], "import" );
	
}

//-----------------------------------------------------------------------------------------

function applyExportFunctions(){
	
	$csvhandler = new CSVHandler( $this->table[ "erp_name" ], "export" );
	
}

//-----------------------------------------------------------------------------------------

function setUnlinkImportFile( $unlink_import_file = true ){
	
	$this->unlink_import_file = $unlink_import_file;
	
}

//-----------------------------------------------------------------------------------------

}//EOC
?>
