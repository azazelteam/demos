<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object traduction
 */
 
include_once( dirname( __FILE__ ) . "/Session.php");
 
class Dictionnary{
	
	//----------------------------------------------------------------------------
	
	private static $instance = NULL;
	
	private $lingualNumber=1;
	private $language = "_1" ;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Contructeur
	 * @access private
	 */
	 
	private function __construct(){
	  
	  if( isset($_GET["langue"]) ) 
	  	$this->Set($_GET["langue"]);

	  if( isset( $_SESSION["Langue"]) ) 
	  	$this->language = $_SESSION["Langue"];
	  
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne le dictionnaire multilingue
	 * @static
	 * @return Dictionnary le dictionnaire
	 */
	 
	public static function &getInstance(){
		
		if( Dictionnary::$instance == NULL )
			Dictionnary::$instance = new Dictionnary();
			
		return Dictionnary::$instance;
			
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Retourne une traduction
	 * @static
	 * @param string $textIdentifier l'identifiant du texte à traduire ( translation.idtext )
     * @param boolean $isEndoding si true, le résultat sera encodé en UTF-8
	 * @return string la traduction correspondante si elle existe, sinon l'identifiant du texte
	 */
	public static function translate( $textIdentifier ,$isEndoding = false){

		$query = "
		SELECT language" . self::getInstance()->lingualNumber . " AS `translation`
		FROM `translation`
		WHERE `idtext` LIKE '" . Util::html_escape($textIdentifier ) . "'
		LIMIT 1";
		
		$db =& DBUtil::getConnection();

	  	$rs = $db->Execute( $query );
	  	
	  	if( $rs === false )
	  		trigger_error( "Impossible de récupérer la traduction du code '$textIdentifier'", E_USER_ERROR );
	  		
	  	if( $rs->RecordCount() )
	  	{
	  		/*if($isEndoding)return Util::doNothing($rs->fields( "translation" ));
            else* return Util::checkEncoding($rs->fields( "translation" ));*/
            return Util::checkEncoding($rs->fields( "translation" ));
	  	}
	  		
		return $textIdentifier;

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne la liste des langues
	 * @return array
	 */
	public function getLanguages(){

		$query = "
		SELECT SUBSTR( idparameter, 5 ) AS `id`, paramvalue AS `label`
		FROM parameter_admin
		WHERE idparameter REGEXP( '^lang[0-9]{1}\$' )
		ORDER BY SUBSTR( idparameter, 5 ) ASC";
		
		$rs =& DBUtil::query( $query );
		
		$languages = array();
		
		while( !$rs->EOF() ){
			
			$languages[ $rs->fields( "id" ) ] = $rs->fields( "label" ); 
			
			$rs->MoveNext();
			
		}
		
		return $languages;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @deprecated
	 * retourne la langue actuelle sous la forme '_?' 
	 */
	public function Get(){
		
		return $this->language;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * @deprecated
	 * Initilise la langue
	 * @param int $LangageNumber Le n° de la langue
	 */
	public function SetN( $langageNumber ){
		
		 $this->lingualNumber = $langageNumber;
		 $Langue = "_" . $langageNumber;
		 $this->Set( $langageNumber );
	 
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @deprecated
	 * Initilise la langue
	 * @param string $language Le code de la langue
	 */
	public function Set( $language ){

		$this->language = $language ;
		$_SESSION[ "language" ] = $this->language ;
		$this->lingualNumber = substr( $language, 1 );
	
	}
	
	//----------------------------------------------------------------------------
}//EOC
?>