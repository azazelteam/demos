<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresses fournisseurs
 */

include_once( dirname( __FILE__ ) . "/Address.php" );

final class SupplierAddress implements Address{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $idsupplier l'identifiant du fournisseur
	 * @access private
	 */
	private $idsupplier;
	/**
	 * @var array $recordSet
	 * @access private
	 */
	private $recordSet;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idsupplier
	 */
	function __construct( $idsupplier ){
		
		$this->idsupplier 	=  $idsupplier;
		$this->recordSet 	= array();
		
		$this->load();
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* Address interface */
	
	public function getCompany(){ 		return $this->recordSet[ "company" ]; }
	public function getFirstName(){ 	return $this->recordSet[ "firstname" ]; }
	public function getLastName(){ 		return $this->recordSet[ "lastname" ]; }
	public function getGender(){ 		return $this->recordSet[ "idtitle" ]; }
	public function getPhonenumber(){ 	return $this->recordSet[ "phonenumber" ]; }
	public function getFaxnumber(){ 	return $this->recordSet[ "faxnumber" ]; }
	public function getGSM(){ 			return $this->recordSet[ "gsm" ]; }
	public function getEmail(){ 		return $this->recordSet[ "email" ]; }
	public function getAddress(){ 		return $this->recordSet[ "address" ]; }
	public function getAddress2(){ 		return $this->recordSet[ "address2" ]; }
	public function getZipcode(){ 		return $this->recordSet[ "zipcode" ]; }
	public function getCity(){ 			return $this->recordSet[ "city" ]; }
	public function getIdState(){ 		return $this->recordSet[ "idstate" ]; }

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access private
	 * @return void
	 */
	protected function load(){
		
		$query = "
		SELECT s.name AS company,
			s.adress AS address,
			s.adress_2 AS address2,
			s.city,
			s.zipcode,
			s.idstate,
			sc.phonenumber,
			sc.gsm,
			sc.email,
			sc.faxnumber,
			sc.firstname,
			sc.lastname,
			sc.idtitle
		FROM supplier s, 
		supplier_contact sc
		WHERE s.idsupplier = '{$this->idsupplier}'
		AND sc.idsupplier = s.idsupplier
		AND sc.idcontact = s.main_contact
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->recordSet = $rs->fields;
		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>