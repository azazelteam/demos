<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object script de base
 * @deprecated 	NE PLUS INCLURE CE FICHIER ( 12MB de RAM requis à la compilation )  ...?
 */

/**
 * @todo préférer l'inclusion du fichier /config/init.php
 */

//--------------------------------------------------------------------------------------------------------------

$sessid = session_id();

if( empty( $sessid ) ){ //@todo : temporaire jusqu'à ce que toutes les inclusions de classes.php aient disparu

	setlocale( LC_TIME, "fr_FR.utf8" );
	
	$previous_name = session_name('FrontOfficeNetlogix');

	if( session_start() === false )
		trigger_error( "session_start() FAILED", E_USER_ERROR );
	
	if( file_exists( dirname( __FILE__ ) . "/../script/configdb.php" ) )
		include_once( dirname( __FILE__ ) ."/../script/configdb.php" );
	else trigger_error( "Configuration file not found", E_USER_ERROR );
	
	if ( isset($_SESSION['compteur']) ) $_SESSION['compteur']++;
	else   $_SESSION['compteur'] = 0;
	
	include_once( dirname( __FILE__ ) . "/Session.php" );
	include_once( dirname( __FILE__ ) . "/DBUtil.php" );
	include_once( dirname( __FILE__ ) . "/Dictionnary.php" );
	include_once( dirname( __FILE__ ) . "/categoryobject.php" );
	include_once( dirname( __FILE__ ) . "/articleobject.php" );
	include_once( dirname( __FILE__ ) . "/productobject.php" );
	include_once( dirname( __FILE__ ) . "/User.php" );
	include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
	include_once( dirname( __FILE__ ) . "/Basket.php" );
	include_once( dirname( __FILE__ ) . "/Estimate.php" );
	include_once( dirname( __FILE__ ) . "/Order.php" );
	include_once( dirname( __FILE__ ) . "/Invoice.php" );
	include_once( dirname( __FILE__ ) . "/Credit.php" );
	include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
	include_once( dirname( __FILE__ ) . "/XHTMLFactory.php" );
	include_once( dirname( __FILE__ ) . "/Firewall.php" );
	include_once( dirname( __FILE__ ) . "/Util.php" );
	include_once( dirname( __FILE__ ) . "/URLFactory.php" );
	include_once( dirname( __FILE__ ) . "/Arbo.php" );
	 
	// create some objects we will need anyway
	
	$Langue = "_1";

	// set pagination vars  
	if( isset($_REQUEST['CurrentPageNumberC']) ) $CurrentPageNumberC = 0 + $_REQUEST['CurrentPageNumberC']; 
	else $CurrentPageNumberC = 1;
	
	if( isset($_REQUEST['CurrentPageNumberP']) ) $CurrentPageNumberP = 0 + $_REQUEST['CurrentPageNumberP']; 
	else  $CurrentPageNumberP = 1 ;
	
	if( isset($_REQUEST['FromScript']) ) $FromScript = $_REQUEST['FromScript'];
	else $FromScript = 'product.php';
	
	if( isset($_REQUEST['IdRealProduct']) ) $IdRealProduct = $_REQUEST['IdRealProduct'];
	
	if( isset($_REQUEST['IdCategory']) ) $IdCategory = $_REQUEST['IdCategory'];
	else $IdCategory = 0;
	
	 //filtrer les adresses IP
	 $Firewall = new Firewall();

	 //Setting time language
	 setlocale( LC_TIME, "fr_FR.utf8" );
	
	include_once( "$GLOBAL_START_PATH/objects/Cookie.php" );

	Cookie::getInstance();

	if( !Session::getInstance()->getCustomer() && isset( $_POST['Login'] ) && isset( $_POST['Password'] ) )
		Session::getInstance()->login( stripslashes( $_POST['Login'] ), md5( stripslashes( $_POST['Password'] ) ) );
		
}

?>