<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object menu back-office
 */

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/User.php" );

                  
					
					
					$query = "SELECT id_lang, name, designation FROM language";
					$rs =& DBUtil::query( $query );
                    
					while( !$rs->EOF() )
						{
						$id_lang = $rs->fields( "id_lang" );
						$name		= $rs->fields( "name" );
						$designation		= $rs->fields( "designation" );
						
						if (isset ($_POST[$name] ))
							{
							
							$query = "SELECT idsession FROM `language_active` WHERE idsession = '". $_COOKIE["FrontOfficeNetlogix"] ."' ";
							$rs =& DBUtil::query( $query );
							
							
											
							if ($rs->RecordCount() == 0 )
								{
								
								$qr = "INSERT INTO `language_active` (
										idsession,
										iduser,
										active
									) VALUES (
									'" . $_COOKIE["FrontOfficeNetlogix"] . "',
									'" . User::getInstance()->getId() . "',
									'" . $id_lang . "'
									)";
									
								$res =& DBUtil::query( $qr );
								
								}
								else 
								{
								
								$req1 = "UPDATE `language_active` SET active = '$id_lang' WHERE idsession = '" . $_COOKIE["FrontOfficeNetlogix"] . "'";
								$rs1 =& DBUtil::query( $req1 );
								
								}
							
							}
						
						
						$rs->MoveNext();
	
						}

class Menu {

	//---------------------------------------------------------------------------------

	private $iduser;
	private $root;
	private $treePath;
	
	//---------------------------------------------------------------------------------

	/**
	 * Construction du menu
	 */
	function __construct(){
	    $this->root = array(
			"id"		=> 0,
			"idparent"	=> 0,
			"name"	 	=> "root",
			"path"		=> "",
			"rights"	=> "",
			"selected"	=> false,
			"children" 	=> array()
		);

		$this->iduser = User::getInstance()->getId();

		if( $this->iduser ){

			$this->treePath = $this->getTreePath();	
			
			$this->getMenuItems( $this->root );
			
			$this->selectTreePath();
			
		}

	}

	//---------------------------------------------------------------------------------
	/**
	 * Retourne l'identifiant de l'outil courant
	 * @todo réécrire cette méthode (en se basant sur l'URI?) pour la nouvelle gestion des droits
	 * @return int
	 */
	public static function getMenuIdentifier(){

		global	$GLOBAL_START_URL;
        

		/* Bidouille toute pourrie pour gérer les idmenu dans les statistiques car il est impossible (pour le moment) de se baser sur les URI à cause des paramètres _GET pas toujours placés dans l'URL */
		if( ( $_SERVER[ "PHP_SELF" ] == "/statistics/statistics.php" ) && ( isset( $_GET[ "cat" ] ) || isset( $_SESSION[ "statcat" ] ) ) ){

			if( isset( $_GET[ "cat" ] ) )
				$cat = $_GET[ "cat" ];

			else if( isset( $_SESSION[ "statcat" ] ) )
				$cat = $_SESSION[ "statcat" ];
            
			$query = "SELECT idmenu FROM menu WHERE path LIKE '/statistics/statistics.php?cat=" .  $cat  . "' LIMIT 1";
			
            $rs =& DBUtil::query( $query );

			if( $rs === false )
				die( "Impossible de récupérer les éléments du menu" );

			$idmenu = $rs->fields( "idmenu" );

		} else if( ( $_SERVER[ "PHP_SELF" ] == "/statistics/statistics.php" ) && !isset( $_GET[ "cat" ] ) && !isset( $_SESSION[ "statcat" ] ) ){

			$rs =& DBUtil::query( "SELECT idmenu FROM menu WHERE path LIKE '%page_statistic.php' LIMIT 1" );

			if( $rs === false )
				die( "Impossible de récupérer les éléments du menu" );

			$idmenu = $rs->fields( "idmenu" );

		} else {

			$query = "SELECT idmenu FROM menu WHERE path LIKE '".$_SERVER[ "REQUEST_URI" ]."%' ";
			$rs =& DBUtil::query( $query );

			if( $rs === false )
				die( "Impossible de récupérer les éléments du menu sélectionné" );

			if( !$rs->RecordCount() ){

				$query = "SELECT idmenu FROM menu WHERE path LIKE '".$_SERVER[ "PHP_SELF" ]."%'";
				$rs =& DBUtil::query( $query );

				if( $rs === false )
					die( "Impossible de récupérer les éléments du menu sélectionné" );

			}

			$idmenu = $rs->fields( "idmenu" );

		}

		if( !isset( $idmenu ) )
			$idmenu = 0;

		return $idmenu;

	}

	//---------------------------------------------------------------------------------

	private function getMenuItems( &$parent ){
        
		$idparent = $parent[ "id" ];
			$q1 = "SELECT active FROM language_active WHERE idsession ='". $_COOKIE["FrontOfficeNetlogix"] ."' ";
					$rs1 =& DBUtil::query( $q1 );
					
					$active		= $rs1->fields( "active" );


					if ($active!=0)
					$lang = "_".$active;
					else
						$lang = "_1";
		$query = "
			SELECT DISTINCT(m.idmenu), m.name$lang, m.path, m.parent, m.display_order, m.displayed, MAX(a.ExpDIEdR) AS rights
			FROM access_rights a
			LEFT JOIN menu m ON a.idmenu = m.idmenu
			LEFT JOIN user_group ug ON a.idgroup = ug.idgroup
			LEFT JOIN user u ON ug.iduser = u.iduser
			WHERE u.iduser = " . $this->iduser . " AND m.parent = " . $idparent . "  AND m.displayed = 1
			GROUP BY m.idmenu
			ORDER BY m.display_order ASC";

		$rs =& DBUtil::query( $query );

		if( $rs === false )
			die( "Impossible de récupérer les éléments du menu" );

		while( !$rs->EOF() ){

			$idchild	= $rs->fields( "idmenu" );
			$name		= $rs->fields( "name$lang" );
			$path		= $rs->fields( "path" );
			$rights		= $rs->fields( "rights" );

			$parent[ "children" ][] = array (
				"id"		=> $idchild,
				"idparent"	=> $idparent,
				"name" 		=> $name,
				"path"		=> $path,
				"rights"	=> $rights,
				"selected"	=> false,
				"children" 	=> array()
			);

			$rs->MoveNext();

		}

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){

			$child = &$parent[ "children" ][ $i ];
			
			//if( in_array( $child[ "id" ], $this->treePath ) || $this->hasParentInTreePath( $child[ "id" ] ) )
				$this->getMenuItems( $child );
	
			$i++;

		}

	}

	//---------------------------------------------------------------------------------

	/**
	 * Affiche le menu
	 */
	public function display(){

		global $GLOBAL_START_URL;

		if( !count( $this->root[ "children" ] ) ){

			return false;

		}
		
		$tabs = array();
		//echo '		<div id="AdminMenu">';
		echo '<div id="menu">';
		echo '<div id="mainMenu">';
		$i = 0;
		while( $i < count( $this->root[ "children" ] ) ){

			$menuItem = &$this->root[ "children" ][ $i ];

			if( $menuItem[ "selected" ] )
				$tabs = &$menuItem[ "children" ];

			//$cssClass = $menuItem[ "selected" ] ? "activeMenuItem" : "inactiveMenuItem";
			$cssClass = "mainMenuItem" ;
			if( $menuItem[ "path" ] ){

				$href = "$GLOBAL_START_URL" . $menuItem[ "path" ];
				$onclick = "";

			} else {

				$href = "#";
				$onclick = ' onclick="return false;"';

			}

			//echo '<div class="'.$cssClass.'"><a href="' . $href . '"' . $onclick . ">" . $menuItem[ "name" ] . "</a></div>";
			echo '<a class="mainMenuItem" href="' . $href . '"' . $onclick . ">" . $menuItem[ "name" ] . "</a><div class=\"separator\"></div>";
			$i++;

		}

?>

		</div> <!-- id="menu" -->
		</div> <!-- id="mainMenu" -->
<?php

		if( count( $tabs ) ){

			$this->displayTabs( $tabs );

		}
			
	}

	//---------------------------------------------------------------------------------

	/**
	 * Affiche les onglets du menu sélectionné
	 */
	private function displayTabs( &$menuItems ){

		global $GLOBAL_START_URL;

		//echo '		<div id="AdminMenuTabs">';
		echo '<div id="menu">';
		echo '<div id="mainMenu">';
		$i = 0;
		while( $i < count( $menuItems ) ){

			$menuItem = &$menuItems[ $i ];
			//$cssClass = $menuItem[ "selected" ] ? "activeTab" : "inactiveTab";
			//$cssClass = "mainMenuItem" ;
			if( count( $menuItem[ "children" ] ) ){

				$href = "#";
				$onclick = " onclick=\"selectTab( $i ); return false;\"";

			} else {

				$href = "$GLOBAL_START_URL" . $menuItem[ "path" ];
				$onclick = "";

			}

			//echo "<div class=\"$cssClass\" id=\"AdminMenuTab_$i\"";
			echo "<div id=\"AdminMenuTab_$i\">";
			//$delimiter = $i ? " class=\"hasDelimiter\"" : "";

			//echo '><a href="' . $href . '"' . $delimiter . $onclick  . ">" . strtoupper( $this->formatLabel( $menuItem[ "name" ] ) ) . "</a></div>";
			echo '<a class="mainMenuItem" href="' . $href . '"'. $onclick  . ">" . strtoupper( $this->formatLabel( $menuItem[ "name" ] ) ) . "</a><div class=\"separator\"></div></div>";
			$i++;

		}

		echo "
		</div> <!-- id=\"menu\" -->
		</div> <!-- id=\"mainMenu\" -->
		<!-- Bidouille pour avoir une fin de tableau correcte -->
		<!--<div style=\"clear: both;\"></div>-->";

		$this->displayTabMenu( $menuItems );

	}

	//---------------------------------------------------------------------------------

	/**
	 * Affiche les panneaux associés aux onglets
	 */
	private function displayTabMenu( &$menuItems ){
		
		global $GLOBAL_START_URL;

		$i = 0;
		while( $i < count( $menuItems ) ){

			$menuItem = &$menuItems[ $i ];
			$style = $menuItem[ "selected" ] ? "display: block; background-color: #d4dee6;" : "display: none;";
			$children = &$menuItem[ "children" ];

			//echo "<div id=\"AdminMenuPanel_$i\" style=\"$style\">";
			echo "<div id=\"AdminMenuPanel_$i\" class=\"subMenu\" style=\"$style\">";
			if( count( $children ) )
				$this->displayListMenu( $children, $i );

			echo "</div>";

			$i++;

		}

	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Affiche les menus déroulants dans les panneaux des onglets
	 */
	private function displayListMenu( &$menuItems, $firstIndex, $subIndex = false, $depth = 0 ){

		global $GLOBAL_START_URL;

		if( !count( $menuItems ) )
			return;
			
		if( !$subIndex )
			$subIndex = 0;

		$cssClass = $depth ? "AdminMenuSubList" : "AdminMenuList";
		
		$sub = $cssClass == "AdminMenuSubList" ? true : false;
		
		if( $sub )
			echo "
					<div class=\"$cssClass\" id=\"subMenu_" . $firstIndex . "_" . $subIndex . "\">";
		else
			echo "
			<div class=\"$cssClass\">";

		$i = 0;
		while( $i < count( $menuItems ) ){

			$child = &$menuItems[ $i ];
			$cssClass = $child[ "selected" ] ? "activeItem" : "inactiveItem";
		
			$js =  count( $child[ "children" ] ) ? "id=\"Menu_" . $firstIndex . "_" . $subIndex. "\" onmouseover=\"javascript: showSubMenu($firstIndex, $subIndex)\" onmouseout=\"javascript: hideSubMenu($firstIndex, $subIndex)\"" : "";
			$cssClass .=  count( $child[ "children" ] ) ? " withChild" : "";

			if( $sub ){
				
				echo "\n						";
				
			}else{
				
				echo "
				<div class=\"$cssClass\"$js>";
				
			}

			if( !empty( $child[ "path" ] ) ){

				$href = "$GLOBAL_START_URL" . $child[ "path" ];
				$js = "";

			} else {

				$href= "#";
				$js = " onclick=\"return false;\"";

			}
			if(($i+1) == count( $menuItems ) || $sub){
				echo "<a href=\"$href\"$js>" . $child[ "name" ] . "</a>";
			}else{
				echo "<a href=\"$href\"$js>" . $child[ "name" ] . "&nbsp;&nbsp;|</a>";
			}
			
			if( count( $child[ "children" ] ) )
				$this->displayListMenu( $child[ "children" ], $firstIndex, $subIndex, $depth + 1 );

			if( !$sub )
				echo "</div>";

			$i++;
			$subIndex++;

		}

		if( $sub )
			echo "
					</div>
				";
		else
			echo "
			</div>
		";

	}

	//---------------------------------------------------------------------------------

	/**
	 * Recherche la localisation de l'utilisateur dans le menu
	 */
	private function getTreePath(){

		global	$GLOBAL_START_URL;
				
		$treePath = array();

		$idmenu = $this->getMenuIdentifier();

		$query = "SELECT idmenu, parent FROM menu WHERE idmenu = $idmenu";
		$rs =& DBUtil::query( $query );

		if( $rs === false )
			die( "Impossible de récupérer les éléments du menu sélectionnés" );

		$idparent = $rs->fields( "parent" );
		$treePath[] = $idmenu;
	
		while( $idparent ){
			
			$query = "SELECT idmenu, parent FROM menu WHERE idmenu = $idparent LIMIT 1";
			$rs =& DBUtil::query( $query );

			if( $rs === false )
				die( "Impossible de récupérer les éléments du menu sélectionnés" );

			$idmenu = $rs->fields( "idmenu" );
			$idparent = $rs->fields( "parent" );
			$treePath[] = $idmenu;

		}

		return array_reverse( $treePath );

	}

	//---------------------------------------------------------------------------------

	private function selectTreePath(){

		$p = &$this->root[ "children" ];

		$i = 0;
		while( $i < count( $this->treePath ) ){

			$selected = $this->treePath[ $i ];
			$j = 0;
			$done = false;

			while( $j < count( $p ) && !$done ){

				if( $p[ $j ][ "id" ] == $selected ){

					$p[ $j ][ "selected" ] = true;
					$p = &$p[ $j ][ "children" ];
					$done = true;

				}

				$j++;

			}

			$i++;

		}

	}

	//---------------------------------------------------------------------------------
	
	public function getMenu(){
		
		return $this->root;
		
	}
	
	//---------------------------------------------------------------------------------

	private function formatLabel( $label ){

		$a = "àáâãäåòóôõöøèéêëçìíîïùúûüÿñ";
		$b = "aaaaaaooooooeeeeciiiiuuuuyn";

		return ( strtr( $label, $a, $b ) );

	}

	//---------------------------------------------------------------------------------

	private function hasParentInTreePath( $idmenu ){

		if( in_array( $idmenu, $this->treePath ) )
			return true;
		
		while( 1 ){
			
			$rs =& DBUtil::query( "SELECT parent FROM menu WHERE idmenu = '$idmenu'" );
			
			if( !$rs->RecordCount() )
				return false;
				
			if( !$rs->fields( "parent" ) )
				return false;
				
			if( in_array( $rs->fields( "parent" ), $this->treePath ) )
				return true;
				
			$idmenu = $rs->fields( "parent" );
				
		}
		
	}

	//---------------------------------------------------------------------------------

	public function getMainCategory(){

		return $this->treePath;
		
	}
	
	//---------------------------------------------------------------------------------
	
	private function resetSelection( &$node ){
		
		$node[ "selected" ] = false;
		
		$i = 0;
		while( $i < count( $node[ "children" ] ) ){
			
			$this->resetSelection( $node[ "children" ][ $i ] );
			$i++;
			
		}
		
	}
	
	//---------------------------------------------------------------------------------
	
	public function update(){
		
		$this->resetSelection( $this->root );
		
		$this->treePath = $this->getTreePath();	
		$this->selectTreePath();
		
	}
	
	//---------------------------------------------------------------------------------
	
}

?>