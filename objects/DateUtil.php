<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object dates fériées
 */
 

define( "DEBUG_DATEUTIL", 0 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );


abstract class DateUtil{
	
	//------------------------------------------------------------------------------------
	
	private static $holidays = NULL;
	
	//------------------------------------------------------------------------------------
    /**
     * Calcul de la date de Pâques
     * @param mixed $iYear l'année de calcul
     * @return string la dete de Pâques
     */
    public static function getEaster( $iYear = null ) {
	
		if (is_null ($iYear)) {
			$iYear = (int)date ("Y");
	    }
	    
	    $iN = $iYear - 1900;
	    $iA = $iN%19;
	    $iB = floor (((7*$iA)+1)/19);
	    $iC = ((11*$iA)-$iB+4)%29;
	    $iD = floor ($iN/4);
	    $iE = ($iN-$iC+$iD+31)%7;
	    $iResult = 25-$iC-$iE;
	    
	    if ($iResult > 0) {
	    	$iEaster = strtotime ($iYear."/04/".$iResult);
	    } else {
	    	$iEaster = strtotime ($iYear."/03/".(31+$iResult));
	    }
	    
	    return $iEaster;
    
    }
    
    //------------------------------------------------------------------------------------
	/**
	 * Donne la date correspondant à n jours ouvrés après une date donnée
	 * @param string $date Date au format yyyy-mm-dd
	 * @param int $delta Nombre de jours ouvrés depuis $date
	 * @return string la date du prochain jour ouvré au format US
	 */
    public static function getNextOpenDay( $date, $delta ) {
    	
    	if( self::$holidays == NULL )
    		self::getHolidays();
    		
    	$iDate = strtotime ( $date );
	    $iEnd = $delta * 86400;
	    
	    $i = 0;
	    while ($i < $iEnd) {
	    	
		    $i = strtotime ("+1 day", $i);
		    
		    $isHoliday = false;
		    
		    //samedi ou dimanche
		    
		    if ( in_array ( date ( "w", $iDate + $i ), array ( 0, 6 ) ) ){
		    
		    	list( $year, $month, $day ) = explode( "-", date ( "Y-m-d", $iDate + $i ) );
		    	$isHoliday = true;
		    	
		    }
		    else{
		    	
		    	//jour férié
		    	
		    	list( $year, $month, $day ) = explode( "-", date ( "Y-m-d", $iDate + $i ) );
		    	
		    	if( isset( $holidays[ $year ][ $month ][ $day ] ) || isset( $holidays[ "0" ][ $month ][ $day ] ) )
		    		$isHoliday = true;

		    }
		    
		    if ( $isHoliday ){
		    	
			    $iEnd = strtotime ("+1 day", $iEnd);
			    $delta ++;
			    
	    	}
	    	
	    }
	    
	    return date( "Y-m-d", $iDate + ( 86400 * $delta ) );
	    
    }

    //------------------------------------------------------------------------------------
    /**
     * Récupère la liste des jours fériés pour l'année en cours et l'année suivante
     * @return array tableau indéxé des jours fériés
     */
    public static function getHolidays(){
    	
		self::$holidays = array();
   		$curYear = date( "Y" );
   		
   		//ajout des dates de Pâques
   		
   		$i = 0;
   		while( $i < 2 ){
   		
   			$date = date( "Y-m-d", easter_date( $curYear + $i ) );
					
			list( $year, $month, $day ) = explode( "-", $date );
			
			self::$holidays[ $year ][ $month ][ $day ] = "Pâques";
						
   			$i++;
   			
   		}
   		
   		//ajout des autres dates
  
   		$rs = DBUtil::query( "SELECT * FROM holidays" );
   		
   		if( $rs === false )
   			die( "Impossible de récupérer la liste des jours fériés" );

   		if( !$rs->RecordCount() )
   			return self::$holidays;
   		
   		while( !$rs->EOF() ){
   			
   			
   			$easter_delta 	= $rs->fields( "easter_delta" );
   			$holiday 		= $rs->fields( "name" );
   			
   			if( $easter_delta != 0 ){ //jours fériés déterminés par rapport à Pâques
   				
   				$i = 0;
   				while( $i < 2 ){
   					
	   				$date = date( "Y-m-d", easter_date( $curYear + $i ) + $easter_delta * 3600 * 24 );
					
					list( $year, $month, $day ) = explode( "-", $date );
					
					self::$holidays[ $year ][ $month ][ $day ] = $holiday;
					
					$i++;
					
   				}
   				
   			}
   			else{ //jours fériés fixes
   				
   				$date = $rs->fields( "date" );
   				
   				list( $year, $month, $day ) = explode( "-", $date );
   				
   				if( $year == "0000" ){
   					
   					$i = 0;
	   				while( $i < 2 ){
	
						$year = $curYear + $i;
						self::$holidays[ $year ][ $month ][ $day ] = $holiday;
						
		   				$i++;
		   				
	   				}
   				
   				}
   				else self::$holidays[ $year ][ $month ][ $day ] = $holiday;

   			}
   			
   			$rs->MoveNext();
   			
   		}
   		
   		return self::$holidays;
   		
	}
    
    //------------------------------------------------------------------------------------
    /**
     * Indique si une date fait partie des vacances
     * @param string $date la date a tester
     * @return bool true pour oui, false pour non
     */
    public static function isHoliday( $date ){
    	
    	if( self::$holidays == NULL )
    		self::getHolidays();
    		
    	$tmp = explode( "-", $date );
    	
    	if( count( $tmp ) != 3 )
    		return false;
    		
    	list( $year, $month, $day ) = $tmp;
    	
    	return isset( self::$holidays[ "0000" ][ $month ][ $day ] ) || isset( self::$holidays[ $year ][ $month ][ $day ] );	
    	
    }
    
    //------------------------------------------------------------------------------------
	/**
	 * Retourne la date du jour précédent au format MySQL
	 * @param string $date la date au format MySQL (avec ou sans les heures)
	 * @return string la date du jour précédent
	 */
	public static function getPreviousDay( $date ){
		
		return date( "Y-m-d", mktime( 0, 0, 0, substr( $date, 5, 2 ), substr( $date, 8, 2 ) - 1, substr( $date, 0, 4 ) ) );
		
	}
    
    //------------------------------------------------------------------------------------
    
}

?>