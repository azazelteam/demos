<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export CSV commandes clients
 */

define( "DEBUG_CSVEXPORTARRAY", 0 );
 
class CSVExportOrderArray
{

	//--------------------------------------------------------------------------------------------------

	private $exportableArray;
	
	private $separator;
	private $text_delimiter;
	private $breakline;
	
	private $filename;
	private $export_directory;
	private $output_type;
	private $mime_type;
	private $charset;
	
	private $display_headers;
	
	private $errors;
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param array $exportableArray tableau a exporter
	 * @param string $filename le nom du fichier a exporter
	 * @param string $output_type le type du fichier
	 * @param string $separator le séparateur
	 * @param string $text_delimiter le délimteur de texte
	 * @param string $breakline le saut de ligne
	 */
	function __construct( &$exportableArray, $filename, $output_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\r\n" ){

		if( !is_array( $exportableArray ) )
			die( "The first argument should be an array" );
			
		$this->exportableArray 	=& $exportableArray;
		
		$this->separator 		= $separator;
		$this->text_delimiter 	= $text_delimiter;
		$this->breakline 		= $breakline;
		
		$this->filename 		= $filename;
		$this->export_directory	= "";
		$this->mime_type 		= "";
		$this->charset	 		= "charset=UTF-8";
		
		$this->errors 	= array();
		
		$this->setOutputType( $output_type );
		
		$this->display_headers = true;
			
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le type de fichier de sortie
	 * @param string $output_type
	 * @return void
	 */
	private function setOutputType( $output_type ){
	
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>setOutputType( $output_type )</u>" );
		
		switch( $output_type ){
		
			case "csv" :
			case "txt" :
			//case "xls" @todo export xls:
			//case "xml" @todo export xml:
			
				$this->output_type = $output_type;
				$this->mime_type = "csv/text";
				
				break;
				
			default : die( "Unknown output mode '$output_type'" );
		
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ecrit le fichier
	 * @return bool true en cas de succès, false sinon
	 */
	public function writeOuputFile( $export_directory = "/data/export/", $filename = false ){
		
		global $GLOBAL_START_PATH;
		
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>writeOuputFile()</u>" );
		
		$this->export_directory = $GLOBAL_START_PATH . $export_directory;
		
		if( $filename !== false )
			$this->filename = $filename;
		
		$this->createOutputFile();
		
		if( !is_resource( $this->output_file ) ){
		
			$this->errors[] = "Cannot create output file";
			
			return false;
			
		}
		
		$this->writeData();
		
		$this->closeOutputFile();
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Créé le fichier d'export
	 * @return bool true en cas de succes, false en cas d'échec
	 */
	private function createOutputFile(){
		
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>createOutputFile()</u>" );
			
		if( !is_dir( $this->export_directory ) ){

			if( @mkdir( $this->export_directory ) === false ){

				$this->errors[] = "Cannot create export directory";
				return false;
			
			}
			
			/*if( @chmod( $this->export_directory, 0755 ) === false ){
			
				$this->errors[] = "Cannot create export directory";
				return false;
			
			}*/
			
		}
		
		$path = $this->export_directory . $this->filename . "." . $this->output_type;

		$this->output_file = fopen( $path, "w+" );
		
		if( !is_resource( $this->output_file ) ){
		
			$this->errors[] = "Cannot open output file";
			
			return false;
		
		}
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Ecrit les données
	 * @return bool true pour succes, false sinon
	 */
	private function writeData(){
		
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>writeData()</u>" );
		
		$data = $this->getData();
		
		@fwrite( $this->output_file, $data );
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Ferme le fichier
	 * @return void
	 */
	private function closeOutputFile(){
	
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>closeOutputFile()</u>" );
			
		if( is_resource( $this->output_file ) )
			fclose( $this->output_file );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Exporte le fichier
	 * @return bool true pour succes, false sinon
	 */
	public function export(){
		
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>output()</u>" );
		
		if( headers_sent() ){
			
			$this->errors[] = "Headers already sent";
			
			return false;
			
		}
		
		header( "Content-Type: {$this->mime_type}; {$this->charset}" );
		header( "Content-disposition: inline; filename={$this->filename}.{$this->output_type}" );
		
		$data = $this->getData();
		
		echo $data;
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les données d'export
	 * @return string les données
	 */
	private function getData(){
		
		$headings = array_keys( $this->exportableArray );
		$valueCount = count( $this->exportableArray[ $headings[ 0 ] ] );
		
		$str = "";
		
		//output headings
		
		if( $this->display_headers ){
			
			$i = 0;
			foreach( $headings as $heading ){
				
				if( $i )
					$str .= $this->separator;
				
				if( $this->text_delimiter == "\"" )
					$heading = str_replace( "\"", "\"\"", $heading );
				
				$str .= $this->text_delimiter;
				$str .= $heading;
				$str .= $this->text_delimiter;
				
				$i++;
				
			}
			
		}
		
		//output data
		
		$i = 0;
		while( $i < $valueCount ){
			
			if($this->display_headers || $i!=0)		
				$str .= $this->breakline;
			
			$p = 0;
			while( $p < count( $headings ) ){
				
				if( $p )
					$str .= $this->separator;
				
				if( $this->text_delimiter == "\"" )
					$value = str_replace( "\"", "\"\"", $this->exportableArray[ $headings[ $p ] ][ $i ] );
				else $value = $this->exportableArray[ $headings[ $p ] ][ $i ];
				
				$str .= $this->text_delimiter;
				$str .= $value;
				$str .= $this->text_delimiter;
				
				$p++;
				
			}
			
			$i++;
			
		}
		
		return $str;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Masque les entetes de colonnes
	 * @return bool false pour masquer les entetes
	 */
	 public function hideHeaders(){
	 	
	 	$this->display_headers = false;
	 		
	 }

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur
	 * @param string $separator le séparateur
	 * @return void
	 */
	private function setSeparator( $separator ){
	
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>setSeparator( $separator )</u>" );
			
		$this->separator = $separator;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur de texte
	 * @param string $text_delimiter le séparateur
	 * @return void
	 */	
	private function setTextDelimiter( $text_delimiter ){
	
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>setTextDelimiter( $text_delimiter )</u>" );
			
		$this->text_delimiter = $text_delimiter;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le caractère de saut de ligne
	 * @param string $breakline le caractère de saut de ligne
	 * @return void
	 */
	private function setBreakline( $breakline ){
	
		if( DEBUG_CSVEXPORTARRAY )
			$this->debug( "<u>setBreakline( $breakline )</u>" );
			
		$this->breakline = $breakline;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Affiche les erreurs
	 */
	protected function report(){
			
		echo implode( "<br />", $this->errors );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les erreurs
	 * @return string les erreurs
	 */
	protected function getErrors(){
	
		return $this->errors;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	function debug( $string ){
	
		echo "<br />$string";
		flush();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
}