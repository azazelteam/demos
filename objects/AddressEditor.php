<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object modification adresses
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/EditableAddress.php" );
include_once( dirname( __FILE__ ) . "/XHTMLFactory.php" );


class AddressEditor{
	
	//----------------------------------------------------------------------------
	
	private static $initialized 			= false;
	
	public static $ADDRESS_TYPE_FORWARDING 	= "forwarding_address";
	public static $ADDRESS_TYPE_INVOICE 	= "invoice_address";
	
	/**
	 * @var int $addressType le type d'adresse à éditer ( self::$ADDRESS_TYPE_FORWARDING ou self::$ADDRESS_TYPE_INVOICE )
	 */
	private $addressType;
	
	/**
	 * @var EditableAddress $editableAddress l'adress à éditer
	 */
	private $editableAddress;
	
	/**
	 * @var int $idbuyer l'identifiant du client
	 */
	private $idbuyer;

	/**
	 * @var bool $readonly vaut true si l'adresse ne doit être qu'en lecture seule
	 */
	private $readonly;
	
	/**
	 * @var string style CSS à appliquer sur le container de l'éditeur
	 */
	private $style;
	
	/**
	 * @var string le nom de la fonction javascript qui sera appelée après chaque modification
	 */
	private $callback;
	
	//----------------------------------------------------------------------------
	
	/**
	 * @param int $idbuyer numéro de client
	 * @param string $addressType AddressEditor::$ADDRESS_TYPE_FORWARDING ou AddressEditor::$ADDRESS_TYPE_INVOICE
	 * @param EditableAddress $editableAddress l'adresse à modifier, false par défaut
	 * @param bool $readonly afficher l'adresse en lecture seule
	 * @param string $style style CSS inline
	 * @param string $callback le nom de la fonction javascript qui sera appelée après chaque modification
	 */
	function __construct( $idbuyer, $addressType, $editableAddress = false, $readonly = false, $style = "", $callback = false ){

		$this->idbuyer 			= $idbuyer;
		$this->editableAddress 	= $editableAddress;
		$this->readonly 		= $readonly;
		$this->style 			= $style;
		$this->callback 		= $callback;
			
		switch( $addressType ){
			
			case self::$ADDRESS_TYPE_FORWARDING :
			case self::$ADDRESS_TYPE_INVOICE :
			
				$this->addressType = $addressType;
				break;
			
			default : trigger_error( "Type d'adresse inconnu", E_USER_ERROR );
			
		}
		
		if( !self::$initialized && !$this->readonly ){
			
			$this->init();
			self::$initialized = true;
				
		}
		
		if( $this->editableAddress || !$this->readonly )
			$this->display();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Inclusion des scripts nécéssaires au fonctionnement de l'éditeur
	 */
	private function init(){
		
		global $GLOBAL_START_URL;
		
		?>
		<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
		<script type="text/javascript">
		/* <![CDATA[*/ 
		
			//------------------------------------------------------------------------------------------------
			
			function displayEditor( addressType, display ){
			
				document.getElementById( addressType + 'Div' ).style.display = display ? 'block' : 'none';
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function load( addressType, id ){
			

				var xhr = getXMLHttpRequest();
				
				xhr.onreadystatechange = function(){
		
					if( xhr.readyState == 4 && xhr.status == 200 ){
					
						if( !xhr.responseXML ){
						
							displayProgressBar( false );
							xhrAlert( 'Impossible de récupérer les données' );
		
							return;
							
						}

						parseXMLAddress( addressType, xhr.responseXML );
						displayProgressBar( false );
						
					}
					
				}
			
				displayProgressBar( true );
				
				var request = '<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?type=' + addressType + '&id=' + id;

				xhr.open( "GET", request, true );
				xhr.send( null );
		
			}
			
			//------------------------------------------------------------------------------------------------
			
			function parseXMLAddress( addressType, XMLDocument ){

				var primaryKey = getPrimaryKeyName( addressType );

				var data = new Array();
				data[ primaryKey ] 		= XMLDocument.getElementsByTagName( primaryKey ).item( 0 ).getAttribute( 'value' );
				data[ 'title' ] 		= XMLDocument.getElementsByTagName( 'title' ).item( 0 ).getAttribute( 'value' );
				data[ 'company' ] 		= XMLDocument.getElementsByTagName( 'company' ).item( 0 ).getAttribute( 'value' );
				data[ 'lastname' ] 		= XMLDocument.getElementsByTagName( 'lastname' ).item( 0 ).getAttribute( 'value' );
				data[ 'firstname' ] 	= XMLDocument.getElementsByTagName( 'firstname' ).item( 0 ).getAttribute( 'value' );
				data[ 'adress' ] 		= XMLDocument.getElementsByTagName( 'adress' ).item( 0 ).getAttribute( 'value' );
				data[ 'adress_2' ] 		= XMLDocument.getElementsByTagName( 'adress_2' ).item( 0 ).getAttribute( 'value' );
				data[ 'zipcode' ] 		= XMLDocument.getElementsByTagName( 'zipcode' ).item( 0 ).getAttribute( 'value' );
				data[ 'city' ] 			= XMLDocument.getElementsByTagName( 'city' ).item( 0 ).getAttribute( 'value' );
				data[ 'idstate' ] 		= XMLDocument.getElementsByTagName( 'idstate' ).item( 0 ).getAttribute( 'value' );
				data[ 'phonenumber' ] 	= XMLDocument.getElementsByTagName( 'phonenumber' ).item( 0 ).getAttribute( 'value' );
				data[ 'faxnumber' ] 	= XMLDocument.getElementsByTagName( 'faxnumber' ).item( 0 ).getAttribute( 'value' );
				data[ 'gsm' ] 			= XMLDocument.getElementsByTagName( 'gsm' ).item( 0 ).getAttribute( 'value' );
				data[ 'email' ] 		= XMLDocument.getElementsByTagName( 'email' ).item( 0 ).getAttribute( 'value' );
				
				setAddressData( addressType, data );

			}
			
			//------------------------------------------------------------------------------------------------
				
			function updateSelection( addressType ){
	
				var primaryKey 	= getPrimaryKeyName( addressType );
				var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
				var checked 	= document.getElementById( addressType + '_checkbox' ).checked;
				
				var selectedAddress = 0;
				
				if( checked )
					selectedAddress = addresses.options[ addresses.selectedIndex ].value;
				
				<?php echo $this->callback ?>( addressType, selectedAddress );
				
			}
		
			//------------------------------------------------------------------------------------------------
			
			function selectCity( targetID, zipcode ){
			
				var xhr = getXMLHttpRequest();
				
				xhr.onreadystatechange = function(){
		
					if( xhr.readyState == 4 && xhr.status == 200 ){
					
						if( !xhr.responseXML )
							return;

						createCitySelection( targetID, xhr.responseXML );
						
					}
					
				}
			
				var url = '<?php echo $GLOBAL_START_URL ?>/sales_force/commune.php?zipcode=' + zipcode;
	
				xhr.open( "GET", url, true );
				xhr.send( null );
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function createCitySelection( targetID, xmlDocument ){
				
				var target = document.getElementById( targetID );
				var communes = xmlDocument.getElementsByTagName( 'commune' );
		
				if( !communes.length )
					return;
					
				if( communes.length == 1 ){
				
					target.value = communes[ 0 ].getAttribute( 'value' );
					return;
					
				}
				
				var selection = document.getElementById( 'CitySelection' );

				var innerHTML 	='<p style="background-color:#EAEAEA; padding:7px; font-weight:bold;">Communes proposées</p>';
				innerHTML 		+= '<div style="overflow:auto; height:120px;">';
				innerHTML 		+= '<ul style="list-style-type:none;">';
				
				var i = 0;
				while( i < communes.length ){

					var commune = communes[ i ].getAttribute( 'value' );
					
					innerHTML += '<li style="text-align:left;">';
					innerHTML += '<input type="radio" onclick="document.getElementById( \'' + targetID + '\' ).value = \'' + commune + '\'; setTimeout( $.unblockUI, 10 );" />&nbsp;';
					innerHTML += '<a style="font-size:11px; text-decoration:none;" href="#" class="blueLink" onclick="document.getElementById( \'' + targetID + '\' ).value = \'' + commune + '\'; setTimeout( $.unblockUI, 10 ); return false;">';
					innerHTML += commune;
					innerHTML += '</a>';
					innerHTML += '</li>';
					
					i++;
					
				}
				
				innerHTML += '</ul>';
				innerHTML += '</div>';
				innerHTML += '<p style="text-align:right;">';
				innerHTML += '<input type="button" value="Annuler" class="blueButton" onclick="setTimeout( $.unblockUI, 10 );" />';
				innerHTML += '</p>';
				
				selection.innerHTML = innerHTML;
				
				$.blockUI({ 

					message: $('#CitySelection'),
					css: { padding: '15px', cursor: 'pointer' }
					
				});
					
			}
			
			function getPrimaryKeyName( addressType ){
			
				var primaryKey = '';
				
				switch( addressType ){
				
					case 'forwarding_address' 	: return 'iddelivery';
					case 'invoice_address' 		: return 'idbilling_adress';
					default : return false;
					
				}
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function resetAddressForm( addressType ){
			
				var primaryKey = getPrimaryKeyName( addressType );
				
				var data = new Array();
				data[ primaryKey ] 		= 0;
				data[ 'title' ] 		= 0;
				data[ 'company' ] 		= '';
				data[ 'lastname' ] 		= '';
				data[ 'firstname' ] 	= '';
				data[ 'adress' ] 		= '';
				data[ 'adress_2' ] 		= '';
				data[ 'zipcode' ] 		= '';
				data[ 'city' ] 			= '';
				data[ 'idstate' ] 		= <?php echo DBUtil::getParameter( "default_idstate" ) ?>;
				data[ 'phonenumber' ] 	= '';
				data[ 'faxnumber' ] 	= '';
				data[ 'gsm' ] 	= '';
				data[ 'email' ] 	= '';
				
				setAddressData( addressType, data );
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function setAddressData( addressType, data ){
			
				var primaryKey = getPrimaryKeyName( addressType );
				
				document.getElementById( addressType + '[company]' ).value 							= data[ 'company' ];
				document.getElementById( addressType + '[lastname]' ).value 						= data[ 'lastname' ];
				document.getElementById( addressType + '[firstname]' ).value 						= data[ 'firstname' ];
				document.getElementById( addressType + '[adress]' ).value 							= data[ 'adress' ];
				document.getElementById( addressType + '[adress_2]' ).value 						= data[ 'adress_2' ];
				document.getElementById( addressType + '[zipcode]' ).value 							= data[ 'zipcode' ];
				document.getElementById( addressType + '[city]' ).value 							= data[ 'city' ];
				document.getElementById( addressType + '[phonenumber]' ).value 						= data[ 'phonenumber' ];
				document.getElementById( addressType + '[faxnumber]' ).value 						= data[ 'faxnumber' ];
				document.getElementById( addressType + '[gsm]' ).value 								= data[ 'gsm' ];
				document.getElementById( addressType + '[email]' ).value 							= data[ 'email' ];
				
				//civilité
				
				var genders = document.getElementsByName( addressType + '[title]' );
				var i = 0;
				while( i < genders.length ){
				
					genders[ i ].checked = genders[ i ].value == data[ 'title' ];

					i++;
					
				}
				
				//pays
				
				var states = document.getElementById( addressType + '[idstate]' );
				var i = 0;
				var done = false;
				while( !done && i < states.options.length ){
				
					if( states.options[ i ].value == data[ 'idstate' ] ){
					
						states.selectedIndex = i;
						done = true;
						
					}

					i++;
					
				}
				
				if( !done )
					states.selectedIndex = 0;
				
				//adresse
				
				var addresses = document.getElementById( addressType + '[' + primaryKey + ']' );
				var i = 0;
				var done = false;
				while( !done && i < addresses.options.length ){
				
					if( addresses.options[ i ].value == data[ primaryKey ] ){
					
						addresses.selectedIndex = i;
						done = true;
						
					}
					
					i++;
					
				}
				
				if( !done )
					addresses.selectedIndex = 0;
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function save( addressType ){
			
				var primaryKey = getPrimaryKeyName( addressType );
				var addresses = document.getElementById( addressType + '[' + primaryKey + ']' );
				var id = addresses.options[ addresses.selectedIndex ].value;
				
				if( id != 0 )
						update( addressType );
				else 	create( addressType );
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function create( addressType ){
				
				var xhr = getXMLHttpRequest();
				
				xhr.onreadystatechange = function(){
		
					if( xhr.readyState == 4 && xhr.status == 200 ){
					
						if( !xhr.responseXML ){
						
							xhrAlert( 'Impossible de récupérer les données' );
	
							return;
							
						}
						else{
						
							xhrAlert( 'Modifications sauvegardées' );
							
							var primaryKey 	= getPrimaryKeyName( addressType );
							var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
							var label 		= xhr.responseXML.getElementsByTagName( 'lastname' ).item( 0 ).getAttribute( 'value' );
							var id 			= xhr.responseXML.getElementsByTagName( primaryKey ).item( 0 ).getAttribute( 'value' );
							
							addresses.options[ addresses.options.length ] = new Option( label, id );
							addresses.options[ addresses.options.length - 1 ].value = id;
							addresses.options[ addresses.options.length - 1 ].innerHTML = label;
							
							addresses.selectedIndex = addresses.options.length - 1;
						
							parseXMLAddress( addressType, xhr.responseXML );
							
							<?php if( $this->callback ) echo "updateSelection( addressType );"; ?>
							
						}
						
					}
					
				}
	
				var request = '<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?type=' + addressType + '&create';
				
				xhr.open( "POST", request, true );
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); 
				xhr.send( getPostString( addressType ) );
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function update( addressType ){
			
				var xhr = getXMLHttpRequest();
				
				xhr.onreadystatechange = function(){
		
					if( xhr.readyState == 4 && xhr.status == 200 ){
					
						if( !xhr.responseText || xhr.responseText == '0' ){
						
							xhrAlert( 'Impossible de sauvegarder les données' );
	
							return;
							
						}
						else{
						
							xhrAlert( 'Modifications sauvegardées' );
							
							var label 		= xhr.responseXML.getElementsByTagName( 'lastname' ).item( 0 ).getAttribute( 'value' );
							var addresses 	= document.getElementById( addressType + '[' + primaryKey + ']' );
							
							addresses.options[ addresses.selectedIndex ].innerHTML = label;
							
						}
						
					}
					
				}

				var primaryKey = getPrimaryKeyName( addressType );
				var addresses = document.getElementById( addressType + '[' + primaryKey + ']' );
				var id = addresses.options[ addresses.selectedIndex ].value;
				
				if( !id )
					return '';
				
				var request = '<?php echo $GLOBAL_START_URL ?>/sales_force/edit_address.php?type=' + addressType + '&update&id=' + id
				
				xhr.open( "POST", request, true );
				xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded;charset=utf-8"); 
				xhr.send( getPostString( addressType ) );
		
			}
			
			//------------------------------------------------------------------------------------------------
			
			function getPostString( addressType ){
			
				data = 'idbuyer=' 		+ urlencode( '<?php echo $this->idbuyer ?>' );
				data += '&company=' 	+ urlencode( document.getElementById( addressType + '[company]' ).value );
				data += '&lastname=' 	+ urlencode( document.getElementById( addressType + '[lastname]' ).value );
				data += '&firstname=' 	+ urlencode( document.getElementById( addressType + '[firstname]' ).value );
				data += '&adress=' 		+ urlencode( document.getElementById( addressType + '[adress]' ).value );
				data += '&adress_2=' 	+ urlencode( document.getElementById( addressType + '[adress_2]' ).value );
				data += '&zipcode=' 	+ urlencode( document.getElementById( addressType + '[zipcode]' ).value );
				data += '&city=' 		+ urlencode( document.getElementById( addressType + '[city]' ).value );
				data += '&phonenumber=' + urlencode( document.getElementById( addressType + '[phonenumber]' ).value );
				data += '&faxnumber=' 	+ urlencode( document.getElementById( addressType + '[faxnumber]' ).value );
				data += '&gsm=' 		+ urlencode( document.getElementById( addressType + '[gsm]' ).value );
				data += '&email=' 		+ urlencode( document.getElementById( addressType + '[email]' ).value );
				data += '&username=' 	+ urlencode( '<?php echo str_replace( "'", "\'", User::getInstance()->getId() ) ?>' );
				data += '&lastupdate=' 	+ urlencode( '<?php echo date( "Y-m-d H:i:s" ) ?>' );

				var states = document.getElementById( addressType + '[idstate]' );
				data += '&idstate=' + urlencode( states.options[ states.selectedIndex ].value );
				
				var genders = document.getElementsByName( addressType + '[title]' );
				
				var i = 0;
				var done = false;
				while( i < genders.length ){
				
					if( genders[ i ].checked ){
					
						data += '&title=' + urlencode( genders[ i ].value );
						done = true;
						
					}
					
					i++;
					
				}
                return data;
				
			}
			
			//------------------------------------------------------------------------------------------------
			
			function displayProgressBar( display ){
			
				if( !display ){
				
					setTimeout( $.unblockUI, 80 );
					return;
					
				}
				
				$.blockUI({ 

					message: $('#ProgressBar' ),
					css: { padding: '20px', color: '#44474E' }
					
				});
	
			}
			
			//------------------------------------------------------------------------------------------------
			
			function urlencode( data ){
			
			    return escape( data.replace(/%/g, '%25').replace(/\+/g, '%2B')).replace(/%25/g, '%');
			
			} 
			
			//------------------------------------------------------------------------------------------------
			
			function xhrAlert( message ){
			
				document.getElementById( 'XHRResponse' ).innerHTML = message;
							
					$.blockUI({ 

						message: $('#XHRResponse' ),
						css: { padding: '20px', color: '#44474E' }
						
					});
		
					setTimeout( $.unblockUI, 1000 );
							
			}

			//------------------------------------------------------------------------------------------------
			
		/* ]]> */
		</script>
		<div id="CitySelection" style="display:none;"></div>
		<div id="ProgressBar" style="display:none; font-weight:bold; text-align:center;">
			<img src="<?php echo $GLOBAL_START_URL ?>/images/loading.gif" alt="loading" style="vertical-align:middle;" />
			Chargement en cours...
		</div>
		<div id="XHRResponse" style="display:none; font-weight:bold; text-align:center;"></div>
		<?php
		
	}
	
	//----------------------------------------------------------------------------
	
	private function display(){
		
		if( $this->readonly )
				self::displayAddress( $this->editableAddress, $this->addressType, $this->style );
		else 	$this->displayEditor();

	}
	
	//----------------------------------------------------------------------------
	
	public static function displayAddress( &$address, $addressType, $style ){
		
		switch( $addressType ){
			
			case self::$ADDRESS_TYPE_FORWARDING :
			case self::$ADDRESS_TYPE_INVOICE :
			case self::$ADDRESS_TYPE_FORWARDING : break;
			
			default : trigger_error( "Type d'adresse inconnu", E_USER_ERROR );
			
		}
	
		?>
		<div class="blocMultiple3"<?php if( strlen( $style ) ) echo " style=\"$style\""; ?>>
			
			<h2 style="margin-top:0;"><input type="checkbox" checked="checked" disabled="disabled" /> <?php echo Dictionnary::translate( $addressType ) ?></h2>
			
			<p><strong><?php echo Dictionnary::translate( $addressType ) ?> :</strong> <?php echo htmlentities( $address->getLastName() ) ?></p>
			<p><strong><?php echo htmlentities( $address->getCompany() ) ?></strong></p>
			<p><?php echo htmlentities( DBUtil::getDBValue( "title_1", "title", "idtitle", $address->getGender() ) ) ?>
			<?php echo htmlentities( $address->getLastName() ) ?>
			<?php echo htmlentities( $address->getFirstName() ) ?></p>
			<p><?php echo htmlentities( $address->getAddress() ) ?>
			<?php if($address->getAddress2()) { ?>- <?php echo htmlentities( $address->getAddress2() ) ?><?php } ?></p>
			<p><strong><?php echo htmlentities( $address->getZipcode() ) ?> <?php echo htmlentities( $address->getCity() ) ?></strong> - <?php echo htmlentities( DBUtil::getDBValue( "name_1", "state", "idstate", $address->getIdState() ) ) ?></p>
			
			<p style="margin-top:10px;">Téléphone : <?php echo htmlentities( $address->getPhonenumber() ) ?> - Fax : <?php echo htmlentities( $address->getFaxnumber() ) ?></p>
			<p style="margin-top:10px;">GSM : <?php echo htmlentities( $address->getGSM() ) ?> - Email : <?php echo htmlentities( $address->getEmail() ) ?></p>

		</div><!-- mainContent -->
		<?php
		
	}
	
	//----------------------------------------------------------------------------
	
	private function displayEditor(){

		global $GLOBAL_START_URL;
		
		$onclick 	= $this->readonly ? "" : "onclick=\"displayEditor( '{$this->addressType}', this.checked );";
		
		if( !$this->readonly && $this->callback )
			$onclick .= " updateSelection( '{$this->addressType}' );";
		
		$onclick 	.= "\"";
		
		$checked 	= $this->editableAddress ? " checked=\"checked\"" : "";
		$display 	= $this->editableAddress ? "block" : "none";
		$disabled 	= $this->readonly ? " disabled=\"disabled\"" : "";
		
		?>
		
		<?php if ( DBUtil::getParameterAdmin('display_link') ) { ?>
<span class="showdevinfo">/objects/AdressEditor.php</span>
<?php } ?>
		<div style="<?php if( strlen( $this->style ) ) echo $this->style.";" ?> margin-top:3px;" class="blocMultiple blocMLeft">

			<input type="checkbox" id="<?php echo $this->addressType ?>_checkbox" name="<?php echo $this->addressType ?>_checkbox" value="1"<?php echo $onclick ?><?php echo $checked ?><?php echo $disabled ?> />
			<span class="orangeText" style="margin-left:10px; font-size:12px;"><b><?php echo Dictionnary::translate( "{$this->addressType}" ) ?></b></span>
			
			<div id="<?php echo $this->addressType ?>Div" style="display:<?php echo $display ?>; margin:3px;">
					    
			<label><span><?php echo Dictionnary::translate( $this->addressType ) ?></span></label>
			<?php $this->listAddresses(); ?>	    
			<div class="spacer"></div>		    
			
			<label><span>Entreprise</span></label>		    
			<input type="text" class="textInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[company]" id="<?php echo $this->addressType ?>[company]" value="<?php echo $this->editableAddress ? $this->editableAddress->getCompany() : "" ?>" />
			<div class="spacer"></div>	
			
			<label><span>Civilité</span></label>	
			<span class="textSimple">	    
			<?php $this->listGenders(); ?>
			</span>		    
			<div class="spacer"></div>	
			
			<label><span>Nom</span></label>		    
			<input type="text" class="textidInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[lastname]" id="<?php echo $this->addressType ?>[lastname]" value="<?php echo $this->editableAddress ? $this->editableAddress->getLastName() : "" ?>" />
			
			<label class="smallLabel"><span>Prénom</span></label>		    
			<input type="text" class="textidInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[firstname]" id="<?php echo $this->addressType ?>[firstname]" value="<?php echo $this->editableAddress ? $this->editableAddress->getFirstName() : "" ?>" />
			<div class="spacer"></div>	
			
			<label><span>Adresse</span></label>		    
			<input type="text" class="textInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[adress]" id="<?php echo $this->addressType ?>[adress]" value="<?php echo $this->editableAddress ? $this->editableAddress->getAddress() : "" ?>" />
			<div class="spacer"></div>	
			
			<label><span>Adresse complémentaire</span></label>		    
			<input type="text" class="textInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[adress_2]" id="<?php echo $this->addressType ?>[adress_2]" value="<?php echo $this->editableAddress ? $this->editableAddress->getAddress2() : "" ?>" />
			<div class="spacer"></div>	
			
			<label><span>Code postal</span></label>
			<input type="text" class="numberInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[zipcode]" id="<?php echo $this->addressType ?>[zipcode]" value="<?php echo $this->editableAddress ? $this->editableAddress->getZipcode() : "" ?>" onkeyup="if( this.value.length == 5 ) selectCity( '<?php echo $this->addressType ?>[city]', this.value );" />

			<label class="smallLabel"><span>Ville</span></label>
			<input type="text" class="mediumInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[city]" id="<?php echo $this->addressType ?>[city]" value="<?php echo $this->editableAddress ? $this->editableAddress->getCity() : "" ?>" />
			<div class="spacer"></div>	
			
			<label><span>Pays</span></label>
			<?php echo $this->listStates() ?>
			<div class="spacer"></div>	
			
			<label><span>Téléphone</span></label>
			<input type="text" class="textidInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[phonenumber]" id="<?php echo $this->addressType ?>[phonenumber]" value="<?php echo $this->editableAddress ? $this->editableAddress->getPhonenumber() : "" ?>" maxlength="20" />
			
			<label class="smallLabel"><span>Fax</span></label>
			<input type="text" class="textidInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[faxnumber]" id="<?php echo $this->addressType ?>[faxnumber]" value="<?php echo $this->editableAddress ? $this->editableAddress->getFaxnumber() : "" ?>" maxlength="20" />
			<div class="spacer"></div>	

			<label><span>GSM</span></label>
			<input type="text" class="textidInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[gsm]" id="<?php echo $this->addressType ?>[gsm]" value="<?php echo $this->editableAddress ? $this->editableAddress->getGSM() : "" ?>" maxlength="20" />
			
			<label class="smallLabel"><span>Email</span></label>
			<input type="text" class="textidInput"<?php echo $disabled ?> name="<?php echo $this->addressType ?>[email]" id="<?php echo $this->addressType ?>[email]" value="<?php echo $this->editableAddress ? $this->editableAddress->getEmail() : "" ?>" maxlength="20" />
			<div class="spacer"></div>
			
			<?php /*@todo <p class="orangeText" style="margin:0px;">*&nbsp;<?php echo Dictionnary::translate( "required_fields" ) ?></p>*/ ?>
			
			<?php $akilae_crm = DBUtil::getDBValue( "paramvalue" , "parameter_admin", "idparameter", "akilae_crm" );
								if( $akilae_crm == 0 ){ ?>
			<input type="button" class="blueButton" style="float:right; margin:5px 10px 0 0;" value="Sauvegarder" onclick="save( '<?php echo $this->addressType ?>' );" />
								<?php } ?>
			
			<div class="spacer"></div>	
			
			</div>
			
		</div>

		<?php
			
	}

	//----------------------------------------------------------------------------
	
	private function listAddresses(){
	
		switch( $this->addressType ){
		
			case self::$ADDRESS_TYPE_FORWARDING : 	$primaryKey = "iddelivery"; 		$table = "delivery"; 		break;
			case self::$ADDRESS_TYPE_INVOICE : 		$primaryKey = "idbilling_adress"; 	$table = "billing_adress"; 	break;
				
		}

		$rs =& DBUtil::query( "SELECT `$primaryKey` AS id, `lastname` AS label FROM `$table` WHERE idbuyer = '{$this->idbuyer}' ORDER BY `lastname` ASC" );
		
		$disabled = $this->readonly ? " disabled=\"disabled\"" : "";
		
		$onchange = "if( this.selectedIndex ){ load( '{$this->addressType}', this.options[ this.selectedIndex ].value ); }else resetAddressForm( '{$this->addressType}' );";
		
		if( !$this->readonly && $this->callback )
			$onchange .= " updateSelection( '{$this->addressType}' );";
		
		?>
		<select id="<?php echo $this->addressType ?>[<?php echo $primaryKey ?>]" name="<?php echo $this->addressType ?>[<?php echo $primaryKey ?>]"<?php echo $disabled ?> onchange="<?php echo $onchange ?>" onkeyup="this.onchange();">
			<option value="0"><?php echo htmlentities( "Créer ou sélectionner une adresse de livraison" ) ?></option>
			<?php
				while( !$rs->EOF() ){
				
					?>
					<option value="<?php echo $rs->fields( "id" ) ?>"<?php if( $this->editableAddress && $this->editableAddress->getId() == $rs->fields( "id" ) ) echo " selected=\"selected\""; ?>><?php echo htmlentities( $rs->fields( "label" ) ) ?></option>
					<?php
						
					$rs->MoveNext();
					
				}
				
		?>
		</select>
		<?php
			
	}
	
	//----------------------------------------------------------------------------
	
	private function listGenders(){
		
		$selectedValue = $this->editableAddress ? $this->editableAddress->getGender() : 0;
		
		$checked = !$selectedValue ? " checked=\"checked\"" : "";
		$disabled = $this->readonly ? " disabled=\"disabled\"" : "";
			
		$query = "SELECT idtitle, title_1 AS `gender` FROM title ORDER BY idtitle ASC";
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
			
			$checked = $rs->fields( "idtitle" ) == $selectedValue ? " checked=\"checked\"" : "";

			?>
			<input type="radio" id="<?php echo $this->addressType ?>[title]" name="<?php echo $this->addressType ?>[title]"<?php echo $checked ?><?php echo $disabled ?> value="<?php echo $rs->fields( "idtitle" ) ?>" style="vertical-align:bottom;" />&nbsp;<?php echo strlen( $rs->fields( "gender" ) ) ? htmlentities( $rs->fields( "gender" ) ) : "Non renseignée" ?>
			<?php

			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	
	private function listStates(){
			
		$selectedValue = $this->editableAddress ? $this->editableAddress->getIdState() : DBUtil::getParameter( "default_idstate" );
		$disabled = $this->readonly ? " disabled=\"disabled\"" : "";
		
		XHTMLFactory::createSelectElement( "state", "idstate", "name_1", "name_1", $selectedValue, false, "", "name=\"{$this->addressType}[idstate]\" id=\"{$this->addressType}[idstate]\"$disabled" );
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>