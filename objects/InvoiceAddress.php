<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresses de livraison
 */
 

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/EditableAddress.php" );		
	// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
 
 
final class InvoiceAddress implements EditableAddress{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $idbilling_adress
	 * @access private
	 */
	private $idbilling_adress;
	/**
	 * @var array $recordSet
	 * @access private
	 */
	private $recordSet;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idbilling_adress
	 */
	function __construct( $idbilling_adress ){
		
		$this->idbilling_adress =  $idbilling_adress ;
		$this->recordSet 		= array();
		
		$this->load();
		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	/* Address interface */
	
	public function getCompany(){ 		return $this->recordSet[ "company" ]; }
	public function getFirstName(){ 	return $this->recordSet[ "firstname" ]; }
	public function getLastName(){ 		return $this->recordSet[ "lastname" ]; }
	public function getGender(){ 		return $this->recordSet[ "idtitle" ]; }
	public function getPhonenumber(){ 	return $this->recordSet[ "phonenumber" ]; }
	public function getFaxnumber(){ 	return $this->recordSet[ "faxnumber" ]; }
	public function getAddress(){ 		return $this->recordSet[ "address" ]; }
	public function getAddress2(){ 		return $this->recordSet[ "address2" ]; }
	public function getZipcode(){ 		return $this->recordSet[ "zipcode" ]; }
	public function getCity(){ 			return $this->recordSet[ "city" ]; }
	public function getIdState(){ 		return $this->recordSet[ "idstate" ]; }
	public function getGSM(){ 			return $this->recordSet[ "gsm" ]; }
	public function getEmail(){ 		return $this->recordSet[ "email" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* EditableAddress interface */

	public function getId(){ 						return $this->idbilling_adress; }
	public function setCompany( $company ){ 		$this->recordSet[ "company" ] 		= $company; }
	public function setFirstName( $firstname ){ 	$this->recordSet[ "firstname" ] 	= $firstname; }
	public function setLastName( $lastname ){ 		$this->recordSet[ "lastname" ] 		= $lastname; }
	public function setGender( $idtitle ){ 			$this->recordSet[ "idtitle" ] 		= intval( $idtitle ); }
	public function setPhonenumber( $phonenumber ){ $this->recordSet[ "phonenumber" ] 	= $phonenumber; }
	public function setFaxNumber( $faxnumber ){ 	$this->recordSet[ "faxnumber" ] 	= $faxnumber; }
	public function setGSM( $gsm ){ 				$this->recordSet[ "gsm" ] 			= $gsm; }
	public function setEmail( $email ){ 			$this->recordSet[ "email" ] 		= $email; }
	public function setAddress( $address ){ 		$this->recordSet[ "address" ] 		= $address; }
	public function setAddress2( $address2 ){ 		$this->recordSet[ "address2" ] 		= $address2; }
	public function setZipcode( $zipcode ){ 		$this->recordSet[ "zipcode" ] 		= $zipcode; }
	public function setCity( $city ){ 				$this->recordSet[ "city" ] 			= $city; }
	public function setIdState( $idstate ){ 		$this->recordSet[ "idstate" ] 		= intval( $idstate ); }
	public function save(){
		
		$query = "
		UPDATE billing_adress
		SET firstname = "			. DBUtil::quote( $this->recordSet[ "firstname" ] ) . ",
			lastname = "			. DBUtil::quote( $this->recordSet[ "lastname" ] ) . ",
			phonenumber = "			. DBUtil::quote( $this->recordSet[ "phonenumber" ] ) . ",
			faxnumber = "			. DBUtil::quote( $this->recordSet[ "faxnumber" ] ) . ",
			gsm = "					. DBUtil::quote( $this->recordSet[ "gsm" ] ) . ",
			email = "				. DBUtil::quote( $this->recordSet[ "email" ] ) . ",
			title = "				. DBUtil::quote( $this->recordSet[ "idtitle" ] ) . ",
			adress = "				. DBUtil::quote( $this->recordSet[ "address" ] ) . ",
			adress_2 = "			. DBUtil::quote( $this->recordSet[ "address2" ] ) . ",
			city = "				. DBUtil::quote( $this->recordSet[ "city" ] ) . ",
			zipcode = "				. DBUtil::quote( $this->recordSet[ "zipcode" ] ) . ",
			idstate = "				. DBUtil::quote( $this->recordSet[ "idstate" ] ) . ",
			company = "				. DBUtil::quote( $this->recordSet[ "company" ] ) . "
		WHERE idbilling_adress = " 	. DBUtil::quote( $this->idbilling_adress ) . "
		LIMIT 1";
		
		DBUtil::query( $query );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access private
	 * @return void
	 */
	private function load(){

		$query = "
		SELECT firstname,
			lastname,
			phonenumber,
			faxnumber,
			gsm,
			email,
			title AS idtitle,
			adress AS address,
			adress_2 AS address2,
			city,
			zipcode,
			idstate,
			company
		FROM billing_adress
		WHERE idbilling_adress = '{$this->idbilling_adress}'
		LIMIT 1";
	
		$this->recordSet = DBUtil::query( $query )->fields;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idbuyer
	 * @return InvoiceAddress
	 */
	public static function create( $idbuyer ){
	
	// ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'delivery';
		$iddelivery = TradeFactory::Indexations($table);
		DBUtil::query( "INSERT INTO delivery ( iddelivery, idbuyer ) VALUES( '" .  $iddelivery  . "', '" .  $idbuyer  . "' )" );
		
	//	return new self( DBUtil::getInsertID() );
	return new self( $iddelivery );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return bool
	 */
	public function allowDelete(){
		
		global $GLOBAL_DB_NAME;
		
		$rs =& DBUtil::query( "SHOW TABLES" );
	
		while( !$rs->EOF() ){
		
			$tablename = $rs->fields( "Tables_in_$GLOBAL_DB_NAME" );
			
			if( $tablename != "billing_adress" ){
				
				$rs2 =& DBUtil::query( "SHOW COLUMNS FROM `$tablename` LIKE 'idbilling_adress'" );
				
				if( $rs2->RecordCount() ){
				
					if( DBUtil::query( "SELECT idbilling_adress FROM `$tablename` WHERE idbilling_adress = '{$this->idbilling_adress}' LIMIT 1" )->RecordCount() )
						return false;
						
				}
			
			}
			
			$rs->MoveNext();
			
		}
	
		return true;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @param int $idbilling_adress
	 * @return void
	 */
	public static function delete( $idbilling_adress ){
		
		DBUtil::query( "DELETE FROM billing_adress WHERE idbilling_adress = '" .  $idbilling_adress . "' LIMIT 1" );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return InvoiceAddress
	 */
	public function duplicate(){
		
		$duplicate = self::create( $this->recordSet[ "idbuyer" ] );
		
		foreach( $this->recordSet as $key => $value )
			if( $key != "idbilling_adress" )
				$duplicate->set( $key, $value );
			
		$duplicate->save();

		return $duplicate;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>