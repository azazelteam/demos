<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object multi pdf
 */

class MULTIPDF extends PDF{

	//---------------------------------------------------------------------------------------------
	
	/**
	 * En-têtes
	 * @return void
	 */
	public function Header(){
		
		$headfg = ''; // no border
		$logox = $this->headerparams['logox'];
		$logoy = $this->headerparams['logoy'];
		$logow = $this->headerparams['logow'];
		$logoh = $this->headerparams['logoh'];
		$headfontfamily = 'Arial';
		$headfonttype = '';
		$headfontsize = 9;
		$headfontcolor = $this->headerparams['fgcolor']; //'000000';
		$headxpos = 100;
		$headalign = 'L';
		$headheight = 9;
		$headwidth = 150;
		$headbg = $this->headerparams['bgcolor']; //'FFFFFF';
	
	    if( !empty($this->papergcolor) )  { $this->setbgcolor($this->papergcolor); $this->Rect(0,0,$this->w,$this->h,'F'); }
	    //test $this->Image('fond.png',0,0,$this->w);
	
	    //Image logo
	    if( !is_file($this->logo ) ) $this->logo = '../pdf/logo.jpg';
	    $this->Image($this->logo,$logox,$logoy,0,$logoh);// $logow mis à 0
	
		$this->SetXY($logox*2+$logow,$this->pagetopmargin + 25);
		$this->SetFont($headfontfamily,'B',12);
		$this->settcolor('550000');
		$this->MultiCell(0,15,$this->footerparams['titrebas3'],0,'L',$this->setbgcolor('FFFFFF'));
		
		//CopyRight
		$this->settcolor($this->vdateparams['fgcolor']);
		$this->SetFont('Arial','',8);
		$this->TextWithRotation($this->rightmargin + 50,$this->pagetopmargin+$this->yhrline + 10, $this->vdateparams['texte'].'   Page '.$this->PageNo().'/{nb}',0,0,0);
		//$this->TextWithRotation(20,$this->h/1.8,$this->vdateparams['texte'],90,0);
		
	    $this->objectlist = array();
	    $this->SetY($this->pagetopmargin+$this->yhrline + 25);// start from here
	}
	
	//---------------------------------------------------------------------------------------------
	
	/**
	 * Pied de page
	 * @return void
	 */
	public function Footer(){
	
		$footfg = ''; // no border
		$footfontfamily = 'Arial';
		$footfonttype = '';
		$footfontsize = 14;
		$footfontcolor = '6B6B6B';
		$footxpos = 0;
		$footypos = -58;
		$footalign = 'C';
		$footheight = 22;
		$footwidth = 0;
		$footbg = $this->footerparams['bgcolor']; //'FFFFFF';
		
		// Affichage date
		$this->mySetTextColor(125,125,125);
		$this->SetFont($footfontfamily,$footfonttype,8);
		$this->TextWithRotation($this->rightmargin + 10,$this->pagetopmargin+$this->yhrline + 10, date("d/m/y"),0,0);
		//$this->Code39($this->rightmargin + 10,$this->pagetopmargin+$this->yhrline + 2);
		
		//$this->TextWithRotation($this->w-$this->pagerightmargin+10 ,$this->h/1.8,date("d/m/y"),90,0);
	
	    //Positionnement à x pts du bas
	    $this->SetY($footypos);
	    $this->SetMargins($this->pageleftmargin, $this->pagetopmargin,$this->pagerightmargin);
	    $this->SetX($this->pageleftmargin);
	
	    //Police
	    $this->SetFont($footfontfamily,$footfonttype,$footfontsize);
	    $this->settcolor($footfontcolor);
	
	    //Numéro de page
	    //$this->Cell(0,10,'Page '.$this->PageNo(),0,0,'C');
	
	    $this->Cell($footwidth,$footheight,$this->footerparams['titrebas'],$this->setfgcolor($footfg),2,$footalign,$this->setbgcolor($footbg));
	
	    $this->SetFont($footfontfamily,$footfonttype,9); $this->settcolor($this->footerparams['fgcolor']);
	    $this->Cell($footwidth,14,$this->footerparams['titrebas2'],$this->setfgcolor($footfg),2,$footalign,$this->setbgcolor($footbg));
		//    $this->SetFont('Arial',$footfonttype,8);
		//    $this->MultiCell($footwidth,10,$this->footerparams['titrebas3'],$this->setfgcolor($footfg),$footalign,$this->setbgcolor($footbg));
	    //if(!$this->currpage)
	
	    //Cadre
	    $this->SetX( $this->leftmargin);
	    $this->SetDrawColor(0,0,0);
	    $this->SetLineWidth(0.2);
	    $this->Rect($this->pagerightmargin,$this->pagetopmargin,$this->w-$this->pagerightmargin-$this->pageleftmargin,$this->h-$this->pagetopmargin-$this->framebottom);
	    $this->Line($this->pagerightmargin,$this->pagetopmargin+$this->yhrline,$this->w-$this->pagerightmargin,$this->pagetopmargin+$this->yhrline);
	
	}
	
	//---------------------------------------------------------------------------------------------
	
	/**
	 * Inutile
	 * @deprecated
	 */
	function Contact(){
		// rien
	}

	//---------------------------------------------------------------------------------------------
	
}//EOC
?>