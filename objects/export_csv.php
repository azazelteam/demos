<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object permettant d'exporter au format csv
 * partir de données de type sql ou tableau php
*/

include_once( dirname( __FILE__ ) . "/Session.php" );


class ExportCSV {
	
	var $filename;
	var $sqlquery;
	var $tabvar;
	var $separator;
	var $type;
	
	
	// Constructeur --------------------------
	function ExportCSV($type,$separator=";"){
		$this->filename = time().'csv';
		$this->sqlquery = '';
		$this->tabvar = '';
		$this->separator = $separator;
		$this->type = $type;
	}
	
	// Export sql csv ------------------------
	function setSQL($sql){
		$this->sqlquery = $sql;
	}
	
	// Export array csv ----------------------
	function setData(&$data){
		$this->tabvar = $data;
	}
	
	function generateCSV($name=''){

		include_once('../script/global.fct.php');
		if($name== ''){ $name=$this->filename; }
		if(strrchr($name,'.')=='.csv'){  $this->filename = $name; } else { $this->filename = $name.'.csv'; }
		header("Content-Type: application/csv-tab-delimited-table");
		header("Content-disposition: filename=".$this->filename);

		$db = DBUtil::getConnection();
		
		if($this->type=='sql' and $this->sqlquery!=''){
			
			$resQuery = $db->Execute($this->sqlquery);

			if ($resQuery->RecordCount() > 0) {
  				// titre des colonnes
  				$nbfields = $resQuery->FieldCount();
  				$i = 0;
  				while ($i < $nbfields) {
    				$fieldname = $resQuery->FetchField($i);
    				echo $fieldname->name.$this->separator;
   					$i++;
  				}
  				echo "\r\n";
	
  				// données de la table
			    $arr = $db->GetArray($this->sqlquery);
   				for($i=0;$i<$resQuery->RecordCount();$i++){
   				    foreach($arr[$i] as $value) {
    			   		echo $value.$this->separator;
   					}
   					echo "\r\n";
  				}
			}
		}
		elseif($this->type=='estimate'){
		 	
		 	$rs = $db->Execute($this->sqlquery);
			
		 	 //Entete --
		 	 echo Dictionnary::translate("com").$this->separator.Dictionnary::translate("estimate_N").
		 	 	   $this->separator.Dictionnary::translate("idbuyer").$this->separator.
		 	 	   Dictionnary::translate("CP").$this->separator.Dictionnary::translate("company").$this->separator.
		 	 	    Dictionnary::translate("b_contact_firstname").$this->separator.
		 	 	    Dictionnary::translate("b_contact_lastname").$this->separator.
		 	 	  Dictionnary::translate("address").$this->separator.
		 	 	  Dictionnary::translate("adress2"). $this->separator.
		 	 	    Dictionnary::translate("city").$this->separator.
		 	 	   Dictionnary::translate("name").$this->separator.str_replace("<br />","",Dictionnary::translate("source")).$this->separator.
		 	 	   Dictionnary::translate("order_status").$this->separator.Dictionnary::translate("order_date").$this->separator.
		 	 	   Dictionnary::translate("valid_until").$this->separator.Dictionnary::translate("Relance").$this->separator.Dictionnary::translate("total_HT")
		 	 	   .$this->separator.Dictionnary::translate("charge_vat").$this->separator.Dictionnary::translate("total_TTC")."\n" ; 
	        
	        //Données de la table --
			for ($i=0;$i<$rs->RecordCount();$i++){
				$idsource = $rs->fields( "idsource" );
				$idbuyer=$rs->Fields("idbuyer");
				$rs2 = $db->Execute("SELECT c.`firstname`,c.`lastname`,b.`adress`,b.`adress_2`,b.`city` FROM contact c , buyer b WHERE b.`idbuyer`=c.`idbuyer` and b.`idbuyer`='".$idbuyer."' ");
				//Affichage --
				echo
				$rs->fields( "initial" ).$this->separator.
				$rs->Fields("idestimate").$this->separator.
				$rs->Fields("idbuyer").$this->separator.
				$rs->fields( "zipcode" ).$this->separator.
				$rs->Fields("company").$this->separator.
				$rs2->Fields("firstname").$this->separator.
				$rs2->Fields("lastname").$this->separator.
				$rs2->Fields("adress").$this->separator.
				$rs2->Fields("adress_2").$this->separator.
				$rs2->Fields("city").$this->separator.
				getTitle( $rs->Fields( "title" ) ) . " " . $rs->Fields( "lastname" ) . $this->separator.
				$this->getSourceText( $idsource ).$this->separator.
				Dictionnary::translate($rs->Fields("status")).$this->separator.
				usDatetime2eu(substr($rs->Fields("DateHeure"),0,10)).$this->separator.
				usDate2eu($rs->Fields("valid_until")).$this->separator.
				usDate2eu($rs->Fields("relaunch")).$this->separator.
				number_format($rs->Fields("total_amount_ht"),2,',','').$this->separator.
				number_format($rs->Fields("total_amount")-$rs->Fields("total_amount_ht"),2,',','').$this->separator.
				number_format($rs->Fields("total_amount"),2,',','').$this->separator."\r\n";
				$rs->MoveNext();
			}
		}
		elseif($this->type=='order'){
		 	
			$selected_date = isset( $_GET[ "selected_date" ] ) ? base64_decode($_GET[ "selected_date" ]) : "DateHeure" ;
			
		 	$rs = $db->Execute($this->sqlquery);
			//Entete --
		 	 echo Dictionnary::translate("com").$this->separator.
		 	 	   Dictionnary::translate("order_N").$this->separator.
		 	 	   Dictionnary::translate("billing_N").$this->separator.
		 	 	   "Cde Frs".$this->separator.
		 	 	   Dictionnary::translate("estimate_N").$this->separator.
		 	 	   Dictionnary::translate("idbuyer").$this->separator.
		 	 	   Dictionnary::translate("CP").$this->separator.
		 	 	   Dictionnary::translate("company").$this->separator.
		 	 	    Dictionnary::translate("b_contact_firstname"). $this->separator. 
		 	 	    Dictionnary::translate("b_contact_lastname").$this->separator.
		 	 	 	Dictionnary::translate("address"). $this->separator.
 	 	 			 Dictionnary::translate("adress2").$this->separator. 
		 	 	  	Dictionnary::translate("city").$this->separator.
		 	 	  	 Dictionnary::translate("name").$this->separator.
		 	 	   Dictionnary::translate("Tel").$this->separator.
		 	 	   str_replace("<br />","",Dictionnary::translate("source")).$this->separator.
		 	 	   Dictionnary::translate("order_status").$this->separator.
		 	 	   Dictionnary::translate( $selected_date ).$this->separator.
		 	 	   Dictionnary::translate("total_HT").$this->separator.
		 	 	   Dictionnary::translate("charge_vat").$this->separator.
		 	 	   Dictionnary::translate("total_TTC").$this->separator.
				   Dictionnary::translate("net_margin").$this->separator.
				   "Profil client"."\n" ;  
	        
	        //Données de la table --
			for ($i=0;$i<$rs->RecordCount();$i++){
				$idsource = $rs->Fields( "idsource" );
				if(($rs->Fields("status")=='Ordered')||($rs->Fields("status")=='Invoiced')){
		                  $query="SELECT idorder_supplier FROM order_supplier WHERE idorder=".$rs->Fields("idorder");	
		          		  $rs2 = $db->Execute($query);	
		                  if ($rs2->RecordCount()>0){ 	$idordersup=$rs2->fields("idorder_supplier");	} else { $idordersup=''; }
				}else{
					
					$idordersup = "";
					
				}
				
				//factures
				
				$invoices = Order::getInvoices( $rs->fields( "idorder" ) );
				//@todo : factures partielles => plusieurs factures
				$idbilling_buyer = count( $invoices ) ? $invoices[ 0 ] : "";
				
				$net_margin = DBUtil::getDBValue( "net_margin_amount", "order", "idorder", $rs->Fields("idorder") );
				$catalog_right = $this->getBuyerCatalogRight($rs->Fields("idbuyer"));
				$idbuyer=$rs->Fields("idbuyer");
				$rs1 = $db->Execute("SELECT c.`firstname`,c.`lastname`,b.`adress`,b.`adress_2`,b.`city` FROM contact c , buyer b WHERE b.`idbuyer`=c.`idbuyer` and b.`idbuyer`='".$idbuyer."' ");
				//Affichage --
				echo
				$rs->Fields("initial").$this->separator.
				$rs->Fields("idorder").$this->separator.
				$idbilling_buyer.$this->separator.
				$idordersup.$this->separator.
				$rs->Fields("idestimate").$this->separator.
				$rs->Fields("idbuyer").$this->separator.
				$rs->fields( "zipcode" ).$this->separator.
				$rs->Fields("company").$this->separator.
				$rs1->Fields("firstname").$this->separator.
				$rs1->Fields("lastname").$this->separator.
				$rs1->Fields("adress").$this->separator.
				$rs1->Fields("adress_2").$this->separator.
				$rs1->Fields("city").$this->separator.
				getTitle( $rs->Fields( "title" ) ) . " " . $rs->Fields( "firstname" ) . " " . $rs->Fields( "lastname" ) . $this->separator.
				$rs->Fields("phonenumber").$this->separator.
				$this->getSourceText( $idsource ).$this->separator.
				Dictionnary::translate($rs->Fields("status")).$this->separator.
				usDate2eu(substr($rs->Fields($selected_date),0,10)).$this->separator.
				number_format($rs->Fields("total_amount_ht"),2,',','').$this->separator.
				number_format($rs->Fields("total_amount")-$rs->Fields("total_amount_ht"),2,',','').$this->separator.
				number_format($rs->Fields("total_amount"),2,',','').$this->separator.
				number_format($net_margin,2,',','').$this->separator.
				$catalog_right."\n";
		    $rs->MoveNext();
			}
		}
		elseif($this->type=='order_supplier'){
		 	
		 	$rs = $db->Execute($this->sqlquery);
		 	$lang = User::getInstance()->getLang();
			
		 	 //Entete --
		 	 echo Dictionnary::translate("com").$this->separator.
	          	  Dictionnary::translate("order_supplier_N").$this->separator.
	          	  Dictionnary::translate("order_status").$this->separator.
	          	  Dictionnary::translate("supplier").$this->separator.
	          	  Dictionnary::translate("order_date").$this->separator.
	          	  Dictionnary::translate("total_HT").$this->separator.
	          	  Dictionnary::translate("order_N").$this->separator.
	          	  Dictionnary::translate("bl_N").$this->separator.
	          	  Dictionnary::translate("zipcode")."\n" ; 
	        
	        //Données de la table --
			for ($i=0;$i<$rs->RecordCount();$i++){
		 	
			 	$rszip = $db->Execute("SELECT zipcode FROM buyer WHERE idbuyer = ".$rs->Fields("idbuyer")." LIMIT 1");
				
				$bls = SupplierOrder::getBLs($rs->Fields("idorder_supplier"));
				
				$delivery_note = '';
				
				for ($b=0;$b<count($bls);$b++){
					
					if($b)
						$delivery_note.=",";
						
					$delivery_note.=$bls[$b];
						
				}
				
				//$query = "SELECT order_supplier_status, order_supplier_status$lang AS ordersupstatus FROM order_supplier_status WHERE order_supplier_status='".$rs->Fields("status")."'";
        		//$rs2 = $db->Execute( $query );
        		//$ordersupstatus = $rs2->Fields('ordersupstatus');
				//Affichage --
				echo
				$rs->Fields("initial").$this->separator.
				$rs->Fields("idorder_supplier").$this->separator.
				$rs->Fields("order_supplier_status$lang").$this->separator.
				$rs->fields( "name").$this->separator.
				usDate2eu(substr($rs->Fields("DateHeure"),0,10)).$this->separator.
				$rs->Fields("total_amount_ht").$this->separator.
				$rs->Fields("idorder").$this->separator.
				$delivery_note.$this->separator.
				$rszip->Fields("zipcode")."\n";
		    $rs->MoveNext();
			}
		}
		elseif($this->type=='billing_waiting'){
		 	$rs = $db->Execute($this->sqlquery);
		 	$lang = User::getInstance()->getLang();
			
		 	 //Entete --
		 	 echo Dictionnary::translate("idbuyer").$this->separator.
	          	  Dictionnary::translate("gest_com_buyer_company").$this->separator.
	          	  Dictionnary::translate("gest_com_idbilling").$this->separator.
	          	  Dictionnary::translate("factor").$this->separator.
	          	  ucfirst(strtolower(Dictionnary::translate( "gest_com_DateHeure" ))).$this->separator.
	          	  Dictionnary::translate("gest_com_invoice_total_amount_ht").$this->separator."\n" ;
	          	  
	        
	        //Données de la table --
			for ($i=0;$i<$rs->RecordCount();$i++){
				
				//$query = "SELECT order_supplier_status, order_supplier_status$lang AS ordersupstatus FROM order_supplier_status WHERE order_supplier_status='".$rs->Fields("status")."'";
        		//$rs2 = $db->Execute( $query );
        		//$ordersupstatus = $rs2->Fields('ordersupstatus');
				//Affichage --
				echo
				$rs->Fields("idbuyer").$this->separator.
				$rs->Fields("company").$this->separator.
				$rs->Fields("idbilling_buyer").$this->separator.
				$rs->fields( "factor").$this->separator.
				usDate2eu(substr($rs->Fields("DateHeure"),0,10)).$this->separator.
				$rs->Fields("total_amount_ht").$this->separator."\n" ;
		    $rs->MoveNext();
			}
		}
	}
	
	function getSourceText( $idsource ){
	
	$db = DBUtil::getConnection();
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT source$lang AS source FROM source WHERE idsource = $idsource ";
		$rs = $db->Execute( $query );
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_recover_source") );
	
	return $rs->Fields('source');
	
	}
	
	
	function getSupplierName( $idsupplier ){
	
	$db = DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( !$rs->RecordCount() ) return '';
		
	return $rs->fields( "name" );
	
	}
	
	function getBuyerCatalogRight( $idbuyer ){
		
		$db = & DBUtil::getConnection();
		$lang = "_1";
			
		$query = "SELECT catalog_right$lang as name FROM catalog_right cat, buyer b
				 WHERE idbuyer = $idbuyer
				AND b.catalog_right = cat.idcatalog_right LIMIT 1";
		$rs = $db->Execute( $query );
	
		if( !$rs->RecordCount() ) return '';
		
		return $rs->fields( "name" );
	
	}

}

function getTitle( $idtitle ){
	
	
	
	$db = & DBUtil::getConnection();
	$lang = "_1";
	
	if(!$idtitle)
		return;
		
	$query = "SELECT title$lang as title FROM title WHERE idtitle='$idtitle'";
	$rs = $db->Execute($query);
	
	if($rs===false)
		die("Impossible de récupérer le titre du client");
		
	return $rs->fields("title");
	
}

?>