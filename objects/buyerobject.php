<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object clients
 */

include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
 //------
class DEPRECATEDBUYER{
	
	private $id;
	private $buyerinfo;
	private $lang;
	private $idcurrency;
	private $device;
	private $currencyalias;
	private $currencyname;
	private $BVat; // tva appliqué par défaut
	private $deliveryinfo;
	private $billinginfo;
	private $sessionkey;
	private $DEBUG;

	/**
	 * Contructeur
	 */
	function __construct(){
		
		//$this->lang = Session::getInstance()->GetLang();
		$this->id = 0;
		$this->lang = '_1';
		$this->idcurrency = 1;
		$this->device = 1.0;
		$this->currencyalias = '&euro;';
		$this->currencyname = 'Euro';
		$this->BVat = 1; // tva appliqué par défaut
		$this->sessionkey = 'buyerinfo';
		$this->DEBUG = 'none';
		
		if( isset($_SESSION[$this->sessionkey] )  )
			$this->buyerinfo = &$_SESSION[$this->sessionkey];
		else $this->SetBuyerInfo() ; //$this->buyerinfo = array('idbuyer'=> 0);
		
		$this->id = $this->GetId();
	
		$this->LoadDeliveryAddresses();
		$this->LoadBillingAddresses();
		
		$this->SetChoiceVat();
		
	}

	/**
	 * Retourne l'id du client
	 * @return int id du client
	 */	
	public function GetId() { return $this->buyerinfo[ "idbuyer" ]; }
	
	/**
	 * Retourne le login du client
	 * @return string le login du client si il existe ou chaine vide sinon
	 */
	public function GetLogin() { return isset( $this->buyerinfo[ "login" ] ) ? $this->buyerinfo[ "login" ] : ""; }
	
	/**
	 * Retourne l'id du pays du client
	 * @return int l'id du pays du client
	 */
	public function GetStateId() {  return $this->buyerinfo[ "idstate" ]; }
	
	/**
	 * Retourne la fonction du client
	 * @return int l'id de la fonction du client
	 */
	public function GetFunction() {  return $this->buyerinfo[ "idfunction" ]; }
	
	/**
	 * Retourne le d&partement du client
	 * @return int l'id du service
	 */
	public function GetDepartment() {  return $this->buyerinfo[ "iddepartment" ]; }
	
	/**
	 * Retourne l'activité du client
	 * @return int l'id du dimaine d'activité du client
	 */
	public function GetActivity() {  return $this->buyerinfo[ "idactivity" ]; }

	/**
	 * Retourne la provenance de l`acheteur
	 * @return int l'id de la provenance
	 */
	public function GetSource() {  return $this->buyerinfo[ "idsource" ]; }
	
	/**
	 * Retourne un commentaire sur l`acheteur
	 * @return string le commentaire sur le client
	 */
	public function GetComment() {  return $this->buyerinfo[ "comment" ]; }
		
	/**
	 * Retourne le titre du client
	 * @return int l'id du titre du client
	 */
	public function GetTitle() {  return $this->buyerinfo[ "title" ]; }
	
	/**
	 * Retourne le titre du client
	 * @return string le prénom du client
	 */
	public function GetName() {  return $this->buyerinfo[ "firstname" ]; }
	
	/**
	 * Retourne le titre du client
	 * @return string le nom du client
	 */
	public function GetLastName() {  return $this->buyerinfo[ "lastname" ]; }
	
	/**
	 * Retourne accord cgv
	 * @return int 1 pour acceptation des CGV, 0 sinon
	 */
	public function GetAccept_condition() {  return $this->buyerinfo[ "accept_condition" ]; }
	
	/**
	 * Retourne l'id du commercial du client
	 * @return int l'id du commercial du client ou le commercial par défaut si pas de commercial affecté
	 */
	public function GetCom() {
		
		if( isset( $this->buyerinfo[ "iduser" ] ) ) 
			return $this->buyerinfo[ "iduser" ];
			
		return DBUtil::getParameter( "contact_commercial" );
	
	}
	
	/**
	 * Retourne l'id de la devise du client
	 * @return int l'id de la devise du client
	 */
	public function GetCurrency() { 
		
		if(isset($this->buyerinfo[ "idcurrency" ])){
			return $this->buyerinfo[ "idcurrency" ];
		}else{
		 	return DBUtil::getParameter( "idcurrency" );	
		}	
	}

	/**
	 * Retourne la famille du client
	 * @return int l'id de la famille du client
	 */
	public function GetFamily() { return isset( $this->buyerinfo[ "idbuyer_family" ] ) ? $this->buyerinfo[ "idbuyer_family" ] : 0; }
	

	/**
	 * Vérifie l'affichage des prix dans les PDF
	 * @return int 0 ou 1
	 */
	public function GetPdfPrice() { return isset( $this->buyerinfo[ "pdf_price" ] ) ? $this->buyerinfo[ "pdf_price" ] : 0; }
	
	
	/**
	 * Retourne une info client
	 * @param string $key Le nom de l'info
	 * @return mixed la valeur de l'info demandée
	 */
	public function getvalue( $key ) { return $this->buyerinfo[ $key ]; }
	
	/**
	 * Retourne une info de l'adresse de livraison
	 * @param string $key Le nom de l'info
	 * @return mixed la valeur de l'info demandée
	 */
	public function getdelivery( $key ){
		
		if( empty( $this->deliveryinfo ) )
			return "";
		
		return $this->deliveryinfo[ $key ];
		
	}
	
	/**
	 * Retourne une info de l'adresse de facturation
	 * @param string $key Le nom de l'info
	 * @return mixed la valeur de l'info demandée
	 */
	public function getbilling( $key ){
		
		if( empty( $this->billinginfo ) )
			return "";
		
		return $this->billinginfo[ $key ];
		
	}
	
	/**
	 * Retourne l'adresse mail du client
	 * @return string l'adresse mail du client
	 */
	public function GetMail() { return $this->buyerinfo[ "email" ]; }
	
	/** 
	 * Retourne le numéro de téléphone du client
	 * @return string le numéro de téléphone du client
	 */
	public function GetPhoneNumber() { return $this->buyerinfo[ "phonenumber" ]; }
	
	/** 
	 * Retourne le numéro de fax du client
	 * @return string le numéro de téléphone du client
	 */
	public function GetFaxNumber() { return $this->buyerinfo[ "phonenumber" ]; }

	/**
	 * Retourne le coeff multiplicateur de prix suivant la monnaie du client
	 * @return float le coefficient multiplicateur
	 */
	public function GetDevice() { return $this->device; }
	
	/** 
	 * Retourne l'information si le client est prospect ou non
	 * @return int 1 pour prospect, 0 pour client
	 */
	public function GetContact() { return $this->buyerinfo[ "contact" ]; }
	
	/** 
	 * Retourne le siret
	 * @return string le numéro de siret
	 */
	public function GetSiret() { return $this->buyerinfo[ "siret" ]; }
	
	
	/** 
	 * Retourne le numéro de téléphone de la société
	 * @return string le numéro de téléphone de la société
	 */
	public function GetPhoneStandard() { return $this->buyerinfo[ "phone_standard" ]; }

	/** 
	 * Retourne le numéro de téléphone de la société
	 * @return string le numéro de fax de la société
	 */
	public function GetFaxStandard() { return $this->buyerinfo[ "fax_standard" ]; }
	
	/** 
	 * Retourne le GSM du client
	 * @return string le numéro de portable du client
	 */
	public function GetGsm() { return $this->buyerinfo[ "gsm" ]; }
	
	/** 
	 * Retourne optin client
	 * @return string optin du client
	 */
	public function GetOptin() { return $this->buyerinfo[ "optin" ]; }
	
	/** 
	 * Retourne la date de naissance du client
	 * @return string la date de naisance du client
	 */
	public function GetBirthday() { return $this->buyerinfo[ "birthday" ]; }
	
	/** 
	 * Retourne l'effectif de la société
	 * @return string l'intervalle du nombre de salariés dans la société
	 */
	public function GetManpower() { 
		
		if( !$this->buyerinfo[ "idmanpower" ] )
			return 0;
			
		$query = "SELECT qty FROM manpower WHERE idmanpower = " . $this->buyerinfo[ "idmanpower" ];
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			return 0;
			
		return $rs->Fields( "qty" );
		
	 }
	
	/**
	 * Retourne l'identifiant de l'adresse de livraison
	 * @return int l'identifiant de l'adresse de livraison
	 */
	public function GetDeliveryAddress(){	return $this->deliveryinfo[ "iddelivery" ]; }
	
	/**
	 * Retourne l'identifiant de l'adresse de facturation
	 * @return int l'identifiant de l'adresse de facturation
	 */
	public function GetBillingAddress(){	return $this->billinginfo[ "idbilling_adress" ]; }
	
	/** 
	 * Retourne des données complémentaire du client
	 */
	public function GetBuyer_info1() { return $this->buyerinfo[ "buyer_info1" ]; }
	public function GetBuyer_info2() { return $this->buyerinfo[ "buyer_info2" ]; }
	public function GetBuyer_info3() { return $this->buyerinfo[ "buyer_info3" ]; }
	public function GetBuyer_info4() { return $this->buyerinfo[ "buyer_info4" ]; }
	public function GetBuyer_info5() { return $this->buyerinfo[ "buyer_info5" ]; }
	public function GetBuyer_info6() { return $this->buyerinfo[ "buyer_info6" ]; }
	public function GetBuyer_info7() { return $this->buyerinfo[ "buyer_info7" ]; }
	public function GetBuyer_info8() { return $this->buyerinfo[ "buyer_info8" ]; }
	public function GetBuyer_info9() { return $this->buyerinfo[ "buyer_info9" ]; }
	public function GetBuyer_info10() { return $this->buyerinfo[ "buyer_info10" ]; }
	
	/**
	 * Retourne l'alias de la devise du client
	 * @return string l'alias de la devise, ex : Euro
	 */
	public function GetCurrencyAlias() { return $this->currencyalias; }
	
	/**
	 * Retourne une valeur
	 * @param string $k la valeur à retourner
	 * @return mixed la valeur recherchée
	 */
	public function Get( $k ) { return $this->buyerinfo[ $k ]; }

	/**
	 * Retourne la index-ième adresse de livraison
	 * @param int index, l'id de l'adresse à retourner 
	 * @return array l'adresse de livraison
	 */
	public function GetDeliveryAddressAt( $index ){ return $this->deliveryAddresses[ $index ]; }
	
	/**
	 * Retourne la index-ième adresse de facturation
	 * @param int index, l'id de l'adresse à retourner 
	 * @return array l'adresse de facturation
	 */
	public function GetBillingAddressAt( $index ){ return $this->billingAddresses[ $index ]; }
	
	/**
	 * Retourne le nombre d'adresses de livraison
	 * @return int le nombre d'adresses de livraison du client
	 */	
	public function GetDeliveryAddressesCount(){ return count( $this->deliveryAddresses ); }
	
	/**
	 * Retourn le nombre d'adresses de facturation
	 * @return int le nombre d'adresses de facturation du client
	 */
	public function GetBillingAddressesCount(){ return count( $this->billingAddresses ); }
	
	/**
	 * Récupere les données d'un acheteur d'un administrateur
	 * @param int $idbuyer L'id de l'acheteur
	 * @param string $fields la donnée à récupérer
	 * @return mixed la valeur recherchée
	 */
	public function GetInfoForBuyer($idbuyer,$fields='*')
	{
		$Result=array();
		$data = DBUtil::getConnection()->GetRow("select $fields from buyer where idbuyer='$idbuyer'");
		if($data)
		{
			$Result = &$data;
		}
		return $Result;
	}

	/**
	 * Modifie un acheteur
	 *  @deprecated : est-ce qu'on va avoir des commandes de clients qui ont refusés les conditions générales de ventes?!?
	 *  @param int $idbuyer L'id de l'acheteur
	 *  @param string $fields sous la forme "champ='valeur'"
	 * 	@return int $Result 1 en cas de succès, 0 sinon
	 */
	public function SetInfoForBuyer($idbuyer,$fields)
	{
		$Result=0;
		$SQL_update = "update buyer set $fields where idbuyer='$idbuyer'";
		$rs =& DBUtil::query($SQL_update);
		showDebug($SQL_update,'SetInfoForBuyer('."$idbuyer,$fields".') => $SQL_update',$this->DEBUG);
	    if($rs)  $Result=1;
	    return $Result;
	}

	/**
	 * Retourne le nom du pays suivant la langue
	 * @return string result le nom du pays
	 */
	public function GetState()
	{
	    if( isset($this->buyerinfo[ "state" ]) ) 
	    	return $this->buyerinfo[ "state" ];
		else $Result = $this->GetStateName();
		
		$this->buyerinfo[ "state" ]= $Result ;
		return $Result;
	}

	/**
	 * Retourne le nom du pays suivant la langue
	 * @param int $ipd optionnel - l'id du pays
	 * @return string result le nom du pays
	 */
	public function GetStateName($idp=0)
	{
		$Result="-"; $Langue = $this->lang;
		if( $idp > 0 ) $s = $idp;
		else $s = $this->buyerinfo[ "idstate" ] ;
		$rs =& DBUtil::query("select name$Langue as name from state where idstate='$s'");
		if($rs->RecordCount()>0)
		{
			$Result =$rs->Fields('name');
		}
		return $Result;
	}


	/**
	 * Récupere l'identifiant d'un pays en fonction de son nom et de la langue
	 * @return int $Result l'id du pays
	 */
	public function GetIdState()
	{
		
		$Result='';
		$Langue = $this->lang;
		$State= DBUtil::getParameter('statedefault');
	        $rs=& DBUtil::query("select idstate from state where name$Langue='$State'");
	        if($rs->RecordCount())
	        {
	                $Result = $rs->Fields('idstate');
	        }
	        return $Result;
	}

	/**
	 * Initialise avec les valeurs par défaut
	 * Ces valeurs sont utilisés tant que le client n'est pas identifié
	 * @return void
	 */
	public function SetBuyerInfo(){
		
		$this->id = 0;
        $this->buyerinfo[ "idbuyer" ]			=  0;
        $this->buyerinfo[ "iderp" ]				=  0;
		$this->buyerinfo[ "admin" ]				=  0;
        $this->buyerinfo[ "idcompany" ]			= 0;
        $this->buyerinfo[ "company" ]			= "";
        $this->buyerinfo[ "iduser" ]			= DBUtil::getParameter( "contact_commercial" );
        $this->buyerinfo[ "idbuyer_family" ]	= 0 ;
	    $this->buyerinfo[ "idactivity" ]		= DBUtil::getParameter( "activitydefault" );
	  //  $this->buyerinfo[ "activity" ]			= $this->getLabel( "activity", $this->buyerinfo[ "idactivity" ] );
        $this->buyerinfo[ "title" ]				= DBUtil::getParameter( "titledefault" );
        $this->buyerinfo[ "firstname" ] 		= "";
        $this->buyerinfo[ "lastname" ]			= "guest";
        $this->buyerinfo[ "iddepartment" ]		= 0;
        $this->buyerinfo[ "department" ]		= "";
        $this->buyerinfo[ "idfunction" ]		= 0;
     //   $this->buyerinfo[ "function" ]			= $this->getLabel( "function", $this->buyerinfo[ "idfunction" ] );
        $this->buyerinfo[ "level" ]				= "";
        $this->buyerinfo[ "adress" ]			= "";
        $this->buyerinfo[ "adress_2" ]			= "";
        $this->buyerinfo[ "zipcode" ]			= "";
        $this->buyerinfo[ "city" ]				= "";
        $this->buyerinfo[ "idstate" ]			= DBUtil::getParameter( "default_idstate" );
        $this->buyerinfo[ "state" ]				= $this->GetStateName();
        $this->buyerinfo[ "phonenumber" ]		= "";
        $this->buyerinfo[ "faxnumber" ]			= "";
        $this->buyerinfo[ "email" ]				= "";
        $this->buyerinfo[ "firstorder" ]		= "";
        $this->buyerinfo[ "totalbonus" ]		= 0;
        $this->buyerinfo[ "idcurrency" ]		= DBUtil::getParameter( "idcurrency" );
        $this->buyerinfo[ "login" ]				= "guest";
        $this->buyerinfo[ "password" ]			= "";
        $this->buyerinfo[ "logo" ]				= DBUtil::getParameterAdmin( "ad_logo" );
        $this->buyerinfo[ "catalog_right" ]		= 0;
        $this->buyerinfo[ "idcustomer_profile" ]= 0;
		$this->buyerinfo[ "iddelivery" ]		= 0;
		$this->buyerinfo[ "comment" ]			= "";
		$this->buyerinfo[ "idsource" ]			= 0;
		$this->buyerinfo[ "source" ]			= "";
		$this->buyerinfo[ "cond_payment" ]		= 0;
		$this->buyerinfo[ "rebate_rate" ]		= 0.0;
		$this->buyerinfo[ "deliv_payment" ]		= 0;
		$this->buyerinfo[ "idmanpower" ]		= 0;
		$this->buyerinfo[ "pdf_price" ]			= 0;
		$this->buyerinfo[ "idscoring_buyer" ]	= 0;
		$this->buyerinfo[ "buyer_ref_us" ]		= "-";
		$_SESSION[ $this->sessionkey ] = &$this->buyerinfo ;
		
	}
	
	/**
	 * Charge les info de du client depuis la BDD
	 * @return void
	 */
	public function GetBuyerInfo(){
		
		$Langue = $this->lang;
		
		//Client principal
		$data = DBUtil::getConnection()->GetRow( "SELECT * FROM buyer WHERE idbuyer = '" . $this->id . "'" );
		if( $data ){
			
			$this->buyerinfo = &$data;
			
			//intitulés
			
			//$this->buyerinfo[ "function" ] = $this->getLabel( "function", $this->buyerinfo[ "idfunction" ] );
			//$this->buyerinfo[ "department" ] = $this->getLabel( "department", $this->buyerinfo[ "iddepartment" ] );
			$this->buyerinfo[ "source" ]			= $this->getLabel( "source", $this->buyerinfo[ "idsource" ] );
			$this->buyerinfo[ "activity" ]			= $this->getLabel( "activity", $this->buyerinfo[ "idactivity" ] );
			$this->buyerinfo[ "idstate" ]			= $data[ "idstate" ];
			$this->buyerinfo[ "state" ]				= $this->GetStateName();

			unset( $this->buyerinfo[ "colors" ] );
			
		}else{
			
			$this->SetBuyerInfo();
			
		}
		
		//Contact
		if( isset( $_SESSION[ "contact" ] ) ){
			
			$idcontact = $_SESSION[ "contact" ];
			$rs =& DBUtil::query( "SELECT * FROM contact WHERE idcontact = '" . $idcontact . "' AND idbuyer = '" . $this->id . "'" );
			
			//On modifie les champs propres au contact
			$this->buyerinfo[ "lastname" ]		= $rs->fields( "lastname" );
			$this->buyerinfo[ "firstname" ]		= $rs->fields( "firstname" );
			$this->buyerinfo[ "iddepartment" ]	= $rs->fields( "iddepartment" );
			$this->buyerinfo[ "department" ]	= $this->getLabel( "department", $this->buyerinfo[ "iddepartment" ] );
			$this->buyerinfo[ "idfunction" ]	= $rs->fields( "idfunction" );
			$this->buyerinfo[ "function" ]		= $this->getLabel( "function", $this->buyerinfo[ "idfunction" ] );
			//$this->buyerinfo[ "idstate" ]		=  $rs->fields( "idstate" );
			//$this->buyerinfo[ "state" ]		= $this->GetStateName();
			$this->buyerinfo[ "faxnumber" ]		= $rs->fields( "faxnumber" );
			$this->buyerinfo[ "phonenumber" ]	= $rs->fields( "phonenumber" );
			$this->buyerinfo[ "email" ]			= $rs->fields( "mail" );
			$this->buyerinfo[ "title" ]			= $rs->fields( "title" );
			$this->buyerinfo[ "comment" ]		= $rs->fields( "comment" );
			$this->buyerinfo[ "gsm" ]			= $rs->fields( "gsm" );
			$this->buyerinfo[ "optin" ]			= $rs->fields( "optin" );
			$this->buyerinfo[ "birthday" ]		= $rs->fields( "birthday" );
			$this->buyerinfo[ "contact_info1" ]	= $rs->fields( "contact_info1" );
			$this->buyerinfo[ "contact_info2" ]	= $rs->fields( "contact_info2" );
			$this->buyerinfo[ "contact_info3" ]	= $rs->fields( "contact_info3" );
			$this->buyerinfo[ "contact_info4" ]	= $rs->fields( "contact_info4" );
			$this->buyerinfo[ "contact_info5" ]	= $rs->fields( "contact_info5" );															
			
			//login
			$q = "SELECT login FROM user_account WHERE idcontact = '" . $idcontact . "' AND idbuyer = '" . $this->id . "'";
			$r =& DBUtil::query( $q );
			
			$this->buyerinfo[ "login" ] = $r->fields( "login" );
			
		}
		
		$this->buyerinfo[ "iddelivery" ] = 0; //initialisation
		
		if( $this->buyerinfo[ "idcurrency" ] <= 0 )
			$this->buyerinfo[ "idcurrency" ] = DBUtil::getParameter( "idcurrency" );
		
		$this->SetChoiceVat();
		$this->id = $this->buyerinfo[ "idbuyer" ];
		
		$_SESSION[ $this->sessionkey ] = &$this->buyerinfo;
		
	}
	
	/**
	 * Calcule la date limite de paiement
	 * @return string la date limite de paiement
	 */
	public function GetDatePayment()
	{
		$Date=getdate(time());
		$DateHeure = sprintf ("%04d-%02d-%02d %02d:%02d:%02d", $Date['year'], $Date['mon'], $Date['mday'], $Date['hours'], $Date['minutes'], $Date['seconds']);
		//calcul de la date de paiement 	
		$DatePaye = sprintf ("%04d-%02d-%02d", $Date['year'], $Date['mon'], $Date['mday']);
		$delaipaye = 0 + $this->buyerinfo[ "deliv_payment" ] ; // delai en jours 
		$delaipaye = $delaipaye * 3600 *24 ; // delai en secondes 
		$echeancepaye  = time() + $delaipaye ; //date de paiement à le seconde près!
		if( $this->buyerinfo[ "cond_payment" ] == '1' ) 
			{
			$echeancepaye = $delaipaye + mktime(0,0,0,date('m')+1,1,date('Y')) - 2 ; // delai + debut du mois suivant à 0h - 2 secondes
			}
		$ADatePaye =getdate($echeancepaye);	
		$DatePaye = sprintf("%04d-%02d-%02d",$ADatePaye['year'], $ADatePaye['mon'], $ADatePaye['mday']) ;
		return $DatePaye;
	}
	
	
	/**
	 * Retourne l'entreprise dans laquelle est le client
	 * @return string $this->buyerinfo[ "company" ] l'entreprise dans laquelle travaille le client
	 */
	public function GetCompany(){ return $this->buyerinfo[ "company" ]; }

	/**
	 * Convertit un texte en nombre
	 * @param string $text la chaine a convertir en nombre
	 * @return float $text le nombre convertit
	 */
	public function text2num( $text ){
		
		$text = str_replace( ",", ".", $text );
		$text = ereg_replace( "([^-0123456789.])", "", $text );
		
		return 0 + $text;
		
	}
	
	/**Modifie le client et enregistre dans la base
	 * @param int $idbuyer l'id du client
	 * @param int $adm 1 pour adm commerciale, 0 sinon
	 * @param int $idcontact l'id du contact
	 * @return mixed int $idbuyer en cas de succès, erreurs sinon
	 */
	public function UpdateBuyer( $idbuyer = 0, $adm = 0, $idcontact = 0 ){
		
		global $GLOBAL_START_PATH;
				
		$now = getdate();
		$Now_Time = date( "Y-m-d H:i:s");
		
		//Vérification de l'unicité du mail de l'entreprise
		if( $adm == 1 && isset( $_POST[ "company_mail" ] ) ){
			
			$company_mail = $_POST[ "company_mail" ];
			
			if( strlen( $company_mail ) != 0 ){
				
				$Query = "SELECT idbuyer FROM buyer WHERE company_mail = '$company_mail'";
				$rs = DBUtil::query( $Query );
				
				if( $rs->RecordCount() ){		
					
					$idb = $rs->fields( "idbuyer" );
					
					if( $idb != $idbuyer )
						return array( "badmail", $idb );
					
				}
				
			}
			
		}
		
		//vérification de l'unicité du mail du contact principal
		if( $adm != 1 )
			$email = $_POST[ "email" ];
		else
			$email = $_POST[ "Email" ];
		
		if( strlen( $email ) != 0 ){
			
			$Query = "SELECT idbuyer, idcontact FROM contact WHERE mail = '$email'";
			$rs = DBUtil::query( $Query );
			
			if( $rs->RecordCount() ){			
				
				$idb = $rs->fields( "idbuyer" );
				$idc = $rs->fields( "idcontact" );
				
				if( $idb != $idbuyer && $idc != $idcontact )
					return array( "badmail", $idb );
				
			}
			
		}
		
		//vérification de l'unicité du siret
		//Pas de siret si B2C
		
		
		//vérification de l'unicité du siren
		//Pas de siren si B2C
		
				
		$fields = array(
		"idcompany",
		"iduser",
		"idbuyer_family",
		"siren",
		"tva",
		"naf",
		"contact",
		"phone_standard",
		"fax_standard",
		"accountant",
		"accountant_phonenumber",
		"accountant_faxnumber",
		"accountant_email",
		"siren",
		"siret",
		"kbis",
		"idmanpower",
		"comment",
		"idsource",
		"level",
		"adress",
		"adress_2",
		"zipcode",
		"idstate",
		"newsletter",
		"firstorder",
		"idcurrency",	
		"username",	
		"buyingamount",
		"idrate_family",
		"deliv_payment",
		"cond_payment",
		"modif_deliv",
		"logo",
		"colors",
		"idactivity",
		"admin",
		"period",
		"period_date",
		"last_period",
		"accept",
		"catalog_right",
		"idcustomer_profile",
		"factor",
		"code_etabl",
		"code_guichet",
		"n_compte",
		"cle_rib",
		"domiciliation",
		"buyer_info1",
		"buyer_info2",
		"buyer_info3",
		"buyer_info4",
		"buyer_info5",
		"buyer_info6",
		"buyer_info7",
		"buyer_info8",
		"buyer_info9",
		"buyer_info10",							
		"factor_status",
		"idscoring_buyer",
		"buyer_ref_us",
		"www",
		"rebate_rate");
		
		//Table contact
		
		$cfields= array(
		"lastname",
		"firstname",
		"iddepartment",
		"idfunction",
		"faxnumber",
		"phonenumber",
		"gsm",
		"optin",
		"birthday",
		"title",
		"contact_info1",
		"contact_info2",
		"contact_info3",
		"contact_info4",
		"contact_info5",
		"username",										
		"lastupdate");		
		
		$numfields = array( "idcurrency", "buyingamount", "rebate_rate" );
		$Result = 0;
		$me = 0;
		
		if( $idbuyer <= 0 ){ 
			
			$idbuyer = $this->buyerinfo[ "idbuyer" ];
			$me = 1;
			
		}
		
		if( $adm != 1 ){
			
			$lastname = ucfirst( strtolower( $_POST[ "lastname" ] ) );
			$firstname = ucfirst( strtolower( $_POST[ "firstname" ] ) );
			
			//Si B2C, PAS DE SOCIETE
			if(B2B_STRATEGY) //B2B
				$company = strtoupper( no_accent( $_POST[ "company" ] ) );
			
			$city = strtoupper( $_POST[ "city" ] );
			$user = addslashes( stripslashes( $this->buyerinfo[ "login" ] ) );
			
		}else{
			
			$lastname = ucfirst( strtolower( $_POST[ "Lastname" ] ) );
			$firstname = ucfirst( strtolower( $_POST[ "Firstname" ] ) );
			
			//Si B2C, PAS DE SOCIETE
			if( B2B_STRATEGY ) //B2B
				$company = strtoupper( no_accent( $_POST[ "Company" ] ) );
			
			$city = strtoupper( $_POST[ "City" ] );
			$user = User::getInstance()->get( "login" );
			
		}
		
		$title = $_POST[ "title" ];
		$cupdstr = "lastname = '$lastname', ";
		$cupdstr .= "firstname = '$firstname', ";
		
		if (isset( $_POST[ "commentcp" ] ) ){
			
			$commentairecp = addslashes( $_POST[ "commentcp" ] );
			$cupdstr .= "comment = '$commentairecp', ";
			
		}
		
		$cupdstr .= "lastupdate = '$Now_Time', username = '$user'";
		
		foreach( $_POST as $k => $v ){
			
			$lk = strtolower( $k );
			
			if( in_array( $lk, $cfields ) ){
				
				if( in_array( $lk, $numfields ) )
					$v = Util::text2num( $v );
				
				$cupdstr .= ", $lk = '" . addslashes( stripslashes( $v ) ) . "'";
				
				if( $me )
					$this->buyerinfo[ $lk ] = stripslashes( $v );
				
			}
			
		}
		
		if( isset( $_POST[ "email" ] ) && strlen( $_POST[ "email" ] ) || isset( $_POST[ "Email" ] ) && strlen( $_POST[ "Email" ] ) )
			$cupdstr .= ", mail = '" . addslashes( stripslashes( $email ) ) . "'";
		
		$updstr = "city = '$city', ";
		
		//Selon le mode du logiciel (B2C / B2B)
		if( B2B_STRATEGY ) //B2B
			$updstr .= "company = '$company', ";
		
		$updstr .= "lastupdate = '$Now_Time', username = '$user'";
		
		if( isset( $company_mail ) )
			$updstr .= ", company_mail = '$company_mail'";
		
		if( isset( $_POST[ "factor_refusal_date" ] ) )
			$updstr.=", factor_refusal_date = '" . euDate2us( $_POST[ "factor_refusal_date" ] ) . "'";
		
		if( isset( $_POST[ "factor_amount_creation_date" ] ) )
			$updstr.=", factor_amount_creation_date = '" . euDate2us( $_POST[ "factor_amount_creation_date" ] ) . "'";
		
		if( isset( $_POST[ "factor_amount" ] ) )
			$updstr.=", factor_amount = '" . Util::text2num( $_POST[ "factor_amount" ] ) . "'";
			
		if( isset( $_POST[ "factor_status" ] ) )
			$updstr.=", factor_status = '" . Util::html_escape( stripslashes( $_POST[ "factor_status" ] )  ). "'";
		
		if( isset( $_POST[ "idscoring_buyer" ] ) )
			$updstr.=", idscoring_buyer = " . $_POST[ "idscoring_buyer" ];
		
		if( isset( $_POST[ "buyer_ref_us" ] ) &&  $_POST[ "buyer_ref_us" ] != "-" )
			$updstr.= ", buyer_ref_us = " . $_POST[ "buyer_ref_us" ];
		
		if( isset( $_POST[ 'comName' ] ) ){
			
			$comName = $_POST[ 'comName' ] ;
			$updstr .= ', nomCommercial = "'.$comName.'"';

		}

		if( isset( $_POST[ "insurance_status" ] ) ){
			
			$insurance = $_POST[ "insurance_status" ] == "Accepted" ? "1" : "0";
			$updstr.=", insurance = '$insurance'";
			
		}
		
		if( isset( $_POST[ "insurance_amount" ] ) )
			$updstr.=", insurance_amount = '" . Util::text2num( $_POST[ "insurance_amount" ] ) . "'";
		
		if( isset( $_POST[ "insurance_amount_creation_date" ] ) )
			$updstr.=", insurance_amount_creation_date = '" . euDate2us( $_POST[ "insurance_amount_creation_date" ] ) . "'";
		
		if( isset( $_POST[ "insurance_refusal_date" ] ) )
			$updstr.=", insurance_refusal_date = '" . euDate2us( $_POST[ "insurance_refusal_date" ] ) . "'";

		if( isset( $_POST[ "insurance_deletion_date" ] ) )
			$updstr.=", insurance_deletion_date = '" . euDate2us( $_POST[ "insurance_deletion_date" ] ) . "'";
			
		if( isset( $_POST[ "insurance_termination_date" ] ) )
			$updstr.=", insurance_termination_date = '" . euDate2us( $_POST[ "insurance_termination_date" ] ) . "'";
			
			
		if( isset( $_POST[ "insurance_status" ] ) )
			$updstr.=", insurance_status = '" . euDate2us( $_POST[ "insurance_status" ] ) . "'";
					
		if( $me ){ 
			
			$this->buyerinfo[ "lastupdate" ] = $Now_Time;
			$this->buyerinfo[ "username" ] = $user;
			
		}

		//Champs Navision
		
		$idstate = $_POST[ "idstate" ];
		
		$query = "SELECT code_eu FROM state WHERE idstate = $idstate LIMIT 1";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le code Navision pour l'identifiant du pays '$idstate'" );
		
		
		// Mise a jour de l'adresse de facturation
		$companyBilling 		= 	$_POST['companyBilling'];
		$titleBilling 			= 	$_POST['titleBilling'];
		$nomBilling 			= 	$_POST['nomBilling'];
		$prenomBilling 			= 	$_POST['prenomBilling'];
		$adrBilling 			= 	$_POST['adrBilling'];
		$moreAdrBilling			= 	$_POST['moreAdrBilling'];
		$cpBilling 				= 	$_POST['cpBilling'];
		$cityBilling 			= 	$_POST['cityBilling'];
		$telBilling				= 	$_POST['telBilling'];
		$faxBilling				=	$_POST['faxBilling'];
		
		// On vérifie si l'enregistrement n'est pas présent dans la base de données
		$preReqBilling = "SELECT * FROM billing_adress WHERE idbuyer = '$idbuyer' 
						AND company = '$companyBilling'
						AND firstname = '$prenomBilling' 
						AND lastname = '$nomBilling' 
						AND adress = '$adrBilling' 
						AND zipcode = '$cpBilling' 
						AND city = '$cityBilling' 
						AND phonenumber = '$telBilling' 
						AND faxnumber = '$faxBilling' ";
		
		$rsPreBilling =& DBUtil::query( $preReqBilling );
			  // ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'billing_adress';
		$idbilling_adress = TradeFactory::Indexations($table);
		if( $rsPreBilling->RecordCount() < 1 ){
					
			$qBilling = "INSERT INTO billing_adress (
			idbilling_adress,
					iderp,
					idbuyer,
					idbuyer_erp,
					firstname,
					lastname,
					adress,
					adress_2,
					zipcode,
					city,
					idstate,
					phonenumber,
					faxnumber,
					title,
					company,
					lastupdate,
					username )
					VALUES(
					'$idbilling_adress',
					'',
					'$idbuyer',
					'',
					'$prenomBilling',
					'$nomBilling',
					'$adrBilling',
					'$moreAdrBilling',
					'$cpBilling',
					'$cityBilling',
					'$idstate',
					'$telBilling',
					'$faxBilling',
					'$titleBilling',
					'$companyBilling',
					now(),
					'$user')				
					";
		
		$rsBilling =& DBUtil::query( $qBilling );
		if($rsBilling === false)
			die( "Impossible de créer l'adresse de facturation du client $idbuyer !" );
		
		}

		$code_eu = $rs->fields( "code_eu" );
		$updstr .= ", nav_buyer_1 = '$code_eu', nav_buyer_5 = '$code_eu', nav_buyer_8 = '$code_eu'";
		
		// MAJ infos Managéo
		$status_company 	= $_POST['status_company'];
		$nbr_etab 			= $_POST['nbr_etab'];
		$last_exo 			= $_POST['last_exo'];
		$lenght_exo 		= $_POST['lenght_exo'];
		$cloture_exo 		= $_POST['cloture_exo'];
		
		$ca_from_web 		= str_replace( " ", "", $_POST['ca_from_web'] );
		$vala_web 			= str_replace( " ", "", $_POST['vala_web'] );
		$exed_exploit 		= str_replace( " ", "", $_POST['exed_exploit'] );
		
		$renta_com 			= $_POST['renta_com'];
		$renta_finan 		= $_POST['renta_finan'];
		$nom_boss 			= $_POST['nom_boss'];
		$fonction_boss 		= $_POST['fonction_boss'];
		$birthday_boss 		= $_POST['birthday_boss'];
		$birthday_boss_city = $_POST['birthday_boss_city'];
		$creationDate 		= $_POST['creationDate'];
		
		$idcustomer_profile = isset( $_POST['idcustomer_profile'] ) ? $_POST['idcustomer_profile'] : false;
		
		$companyCapital 	= str_replace( " ", "", $_POST['companyCapital'] );
		$companyResult 		= str_replace( " ", "", $_POST['companyResult']);
		
		$accountantLastName = $_POST['accountantLastName'];
		
		$updstr .= ", forme_juridique = '$status_company', nbr_etablissement = '$nbr_etab', dern_exercice = '$last_exo',
				duree_exercice = '$lenght_exo', ca = '$ca_from_web', cloture_exercice = '$cloture_exo',
				valeur_ajoute = '$vala_web', renta_com = '$renta_com', renta_finan = '$renta_finan', boss_name = '$nom_boss',
				boss_birthday = '$birthday_boss', boss_birthday_city = '$birthday_boss_city', boss_fonction = '$fonction_boss',
				creation_date = '$creationDate', capital = '$companyCapital', resultat = '$companyResult', exed_exploit = '$exed_exploit', accountantLastName = '$accountantLastName'";
		
		if( $idcustomer_profile !== false ){
			$updstr .= " , idcustomer_profile = $idcustomer_profile ";
		}
		
		foreach( $_POST as $k => $v ){
			
			$lk = strtolower( $k );
			
			if( in_array( $lk, $fields ) ){
				
				if( in_array( $lk, $numfields ) )
					$v = Util::text2num( $v );
				
				$updstr .= ", $lk = '" . addslashes( stripslashes( $v ) ) . "'";
				
				if( $me )
					$this->buyerinfo[ $lk ] = stripslashes( $v );
				
			}
			
		}
		
		

		$rsc =& DBUtil::query( "UPDATE contact SET $cupdstr WHERE idbuyer = '$idbuyer' AND idcontact = 0" );	
		$rs =& DBUtil::query( "UPDATE buyer SET $updstr WHERE idbuyer = '$idbuyer'" );
		
		// Si idcustomer_profile_right == 7 (MAIRIE) alors on met egalement à jour les champs de la table mairie
		if( $idcustomer_profile == 7 ){
			
			$nbrHabitant 	= $_POST["nbr_habitant"];
			$nomMaire 		= $_POST["nom_Maire"];
			$prenomMaire 	= $_POST["prenom_Maire"];
			$regionMairie 	= $_POST["mairie_region"];
			$deptMairie 	= $_POST["mairie_dpt"];
			$inseeMairie	= $_POST["insee_code"];
			// ------- Mise à jour de l'id gestion des Index  #1161
			$queryMairie	= "UPDATE mairie SET 
								nom_maire = '$nomMaire', 
								prenom_maire = '$prenomMaire', 
								region_mairie = '$regionMairie',
								departement_mairie = '$deptMairie',
								code_insee = '$inseeMairie',
								nbr_habitant = '$nbrHabitant'
								WHERE idbuyer = '$idbuyer'" ;
			
			$rsMairie =& DBUtil::query( $queryMairie );
			if($rsMairie === false){
				die( "Impossible de mettre à jour les champs liés à la mairie !" );
			}
		}
		
		
		if( $rs && $rsc )
	      	$Result = 1;
	    
	    //Gestion du compte client pour le Front Office
	    if( $adm == 1 ){
		    
		    //Création d'un compte client Front Office - Aucun compte existant
		    if( isset( $_POST[ "GenerateLogin" ] ) ){
		    	
		    	if( isset( $_POST[ "Email" ] ) && strlen( $_POST[ "Email" ] ) )
					$login = stripslashes( $_POST[ "Email" ] );
				else if( isset( $_POST[ "Faxnumber" ] ) && strlen( $_POST[ "Faxnumber" ] ) )
					$login = stripslashes( $_POST[ "Faxnumber" ] );
				else $login = Util::getRandomPassword();
		    	
		    	$password = $this->createAccount( $idbuyer, $idcontact, $login );
		    	
		    	$return = $this->sendRegistrationMail( $idbuyer, 0, $password );
		    	
		    }
		    
		    //Génération d'un nouveau mot de passe pour le compte du Front Office
		    if( isset( $_POST[ "GeneratePassword" ] ) ){
		    	
		    	$password = Util::getRandomPassword();
		    	DBUtil::query( "UPDATE user_account SET password = MD5('$password') WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1" );

		    	$return = $this->sendRegistrationMail( $idbuyer, 0, $password );
		    	
		    }
			
	    }
		
	    return $Result;
	    
	}

	/**
	 * @deprecated
	 * Crée un identifiant de connexion pour le Front Office
	 * @param string $email l'email à utiliser comme login 
	 * @return $password le mot de passe créé pour le compte
	 */
	public function createAccount( $idbuyer, $idcontact, $login ){

		$suffix = false;
		$login_tmp = $login;
		while( DBUtil::query( "SELECT login FROM user_account WHERE login = " . DBUtil::quote( $login_tmp ) )->RecordCount() ){
	
			$suffix = $suffix ? ++$suffix : 1;
			$login_tmp = $login . $suffix;
			
		}

		$login = $login_tmp;
		
		$password = Util::getRandomPassword();
		
		$rs =& DBUtil::query( "INSERT INTO user_account (
				idbuyer, idcontact, login, password
			) VALUES (
				'" . $idbuyer . "',
				'" . $idcontact . "',
				" . DBUtil::quote( $login ) . ",
				MD5( '$password' )
			)" );
		
		
		if( $rs === false )
			trigger_error( "Un problème est survenu lors de la création du compte de l'utilisateur $login", E_USER_ERROR );
		
		return $password;
		
	}
	
	/**
	 * Rend un client initilisable et le détache de son administrateur
	 * @param int $idbuyer l'id du client
	 * @return void
	 */
	public static function DeleteBuyer( $idbuyer = 0 ){
		
		$l = substr( md5( date( "r" ) ), 5, 10 );
		$p = substr( md5( date( "r" ) ), 11, 10 );
		$Now_Time = date( "Y-m-d H:i:s");
		$user = addslashes( stripslashes( $this->buyerinfo[ "login" ] ) );
		DBUtil::query( "UPDATE buyer SET admin = 0, login = '$l', password = '$p', firstname = 'DELETED', username = '$user', lastupdate = '$Now_Time', newsletter = '0' WHERE idbuyer = '$idbuyer'" );
		
	}

	/**
	 * Vérifier la disponibilité de l'adresse email
	 * @param string $email L'adresse email à tester
	 * @return bool true si email disponible, false sinon
	 */
	public function IsEmailAvailable( $email ){
		
		if( empty( $email ) )
			return false;	
		
		$query2 = "SELECT COUNT(*) AS `exists` FROM contact WHERE mail LIKE '$email' LIMIT 1";
		$rs2 =& DBUtil::query( $query2 );
		
		if( $rs->fields( "exists" ) == 0 && $rs2->fields( "exists" ) == 0 )
			return true;
		else
			return false;
		
	}
	
	/**
	 * Vérifier la disponibilité du nom d'utilisateur
	 * @param string $login Le nom d'utilisateur à tester
	 * @return bool true si le login est disponible, false sinon
	 */
	public function IsLoginAvailable( $login ){
		
		if( empty( $login ) )
			return false;
		
		$query = "SELECT COUNT(*) AS `exists` FROM user_account WHERE login LIKE '$login' LIMIT 1";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			return false;
		
		return $rs->fields( "exists" ) == 0;
		
	}
	
	/**
	 * Ajoute un client
	 * Méthode seulement utilisée dans le Back Office
	 * Ne pas utiliser dans le Front Office
	 * @param int $idbuyer optionnel - L'id de l'administrateur
	 * @return mixed l'id du client créé en cas de succès, message d'erreurs sinon
	 */
	public function AddBuyer( $idbuyer = 0 ){
		
		global $GLOBAL_START_PATH;
		
		/* protection XSS */
		$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );
		
		// Définition du mode dans lequel se trouve le logiciel (B2C / B2B)
		
		$now = getdate();
		$Now_Time = date( "Y-m-d H:i:s");
		
		$fields= array("idcompany",
		"iduser",
		"siren",
		"siret",
		"kbis",
		"idbuyer_family",
		"siren",
		"tva",
		"naf",
		"iddepartment",
		"contact",
		"phone_standard",
		"fax_standard",
		"accountant",
		"accountant_phonenumber",
		"accountant_faxnumber",
		"accountant_email",
		"idmanpower",
		"idsource",
		"comment",
		"buyer_info1",
		"buyer_info2",
		"buyer_info3",
		"buyer_info4", 
		"buyer_info5",
		"buyer_info6",
		"buyer_info7",
		"buyer_info8",
		"buyer_info9",
		"buyer_info10",
		"level",
		"adress",
		"adress_2",
		"zipcode",
		"idstate",
		"newsletter",
		"firstorder",
		"totalbonus",
		"idcurrency",
		"lastupdate",
		"username",
		"buyingamount",
		"idrate_family",
		"deliv_payment",
		"cond_payment",
		"modif_deliv",
		"logo",
		"colors",
		"idactivity",
		"admin",
		"period",
		"period_date",
		"last_period",
		"accept",
		"idcustomer_profile",
		"factor",
		"code_etabl",
		"code_guichet",
		"n_compte",
		"cle_rib",
		"domiciliation",
		"factor_status",
		"factor_amount",
		"idscoring_buyer",
		"buyer_ref_us",
		"www",
		"rebate_rate");
		
		$numfields = array( "idcurrency", "buyingamount", "rebate_rate" );
		$Result = 0;
		
		//Vérification de l'unicité du mail
		$email = $_POST[ "Email" ];
		
		if( strlen( $email ) != 0 ){
			
			$Query = "SELECT idbuyer FROM contact WHERE mail = '$email'";
			$rs = DBUtil::query( $Query );
			
			if( $rs->RecordCount() ){			
				
				$idb = $rs->fields( "idbuyer" );
				return array( "badmail", $idb );
				
			}
			
			$Query = "SELECT idbuyer FROM user_account WHERE login = '$email'";
			$rs = DBUtil::query( $Query );
			
			if( $rs->RecordCount() ){			
				
				$idb = $rs->fields( "idbuyer" );
				return array( "badmail", $idb );
				
			}
			
		}
		
		//Vérification de l'unicité du siret. Pas de siret pour le B2C
		
		
		//Vérification de l'unicité du siren. Pas de siren pour le B2C
		
		$fldstr = "";
		$updstr = "";
		
		//City en majuscule
		$city = $_POST[ "City" ];
		$city = strtoupper( $city );
		
		
		
		$fldstr.= "city"; 
		$updstr .= "'$city'";
		
		//Company en majuscule
		if( B2B_STRATEGY ){ //B2B
			
			$company = $_POST[ "Company" ];
			$company = strtoupper( no_accent( $company ) );
			
			$fldstr.= ", company"; 
			$updstr .= ", '$company'";
			
		}
		
		//Firstname et Lastname premiere lettre en maj
		$firstname = addslashes( ucfirst( strtolower( $_POST[ "Firstname" ] ) ) );
		$name = addslashes( ucfirst( strtolower( $_POST[ "Lastname" ] ) ) );
		
		//username, lastupdate et admin
		$user = User::getInstance()->get( "login" );
		$fldstr .= ", lastupdate, username, admin";
		$updstr .= ", '$Now_Time', '$user', 0";
		
		//Date de refus factor
		//Pas de factor en mode B2C
		if( B2B_STRATEGY ){ //B2B
			
			$date_refusal = isset( $_POST[ "factor_refusal_date" ] ) ? $_POST[ "factor_refusal_date" ] : "";
			
			if( !empty( $date_refusal ) ){
				
				//On transforme la date pour la base
				
				list( $d, $m, $y ) = explode( "-", $date_refusal );
				$updstr .= ", '$y-$m-$d'";
				$fldstr .= ", factor_refusal_date";
				
			}else{
				
				$updstr .= ",'0000-00-00'";
				$fldstr .= ", factor_refusal_date";
			}
			
		}
		
		//Date d'accord encours
		//Pas de factor en mode B2C
		if( B2B_STRATEGY ){ //B2B
			
			$factor_amount_creation_date = isset( $_POST[ "factor_amount_creation_date" ] ) ? $_POST[ "factor_amount_creation_date" ] : "";
			
			if( !empty( $factor_amount_creation_date ) ){
				
				//On transforme la date pour la base
				list( $d, $m, $y ) = explode( "-", $factor_amount_creation_date );
				$updstr .= ", '$y-$m-$d'";
				$fldstr .= ", factor_amount_creation_date";
			}else{
				
				$updstr .= ", '0000-00-00'";
				$fldstr .= ", factor_amount_creation_date";
				
			}
			
		}
		
		//Champs Navision
		$idstate = $_POST[ "idstate" ];
		
		$query = "SELECT code_eu FROM state WHERE idstate = $idstate LIMIT 1";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le code Navision pour l'identifiant du pays '$idstate' $query" );
			
		$code_eu = $rs->fields( "code_eu" );
		$fldstr .= ", nav_buyer_1, nav_buyer_5, nav_buyer_8";
		$updstr .= ", '$code_eu', '$code_eu', '$code_eu'";
		
		foreach( $_POST as $k => $v ){
			
			$lk = strtolower( $k );
			
			if( in_array( $lk, $fields ) ){
				
				if( in_array( $lk, $numfields ) )
					$v = Util::text2num( $v );
				
				$fldstr .= ", $lk";
				$updstr .= ", '" . addslashes( stripslashes( $v ) ) . "'";
				
			}
			
		}
		
		//Date de création
		$fldstr .= ", create_date";
		$updstr .= ", NOW()";
		
		// Ajout des infos en provenance de managéo (table Buyer)
		$forme_juridique = $_POST[ "status_company" ] ;
		$nbr_etab = $_POST[ "nbr_etab" ] ;
		$dern_exo = $_POST[ "last_exo" ] ;
		$duree_exo = $_POST[ "lenght_exo" ] ;
		$ca_company = $_POST[ "ca_from_web" ] ;
		$cloture_exo = $_POST[ "cloture_exo" ] ;
		$va_company = $_POST[ "vala_web" ] ;
		$renta_com = $_POST[ "renta_com" ] ;
		$renta_finan = $_POST[ "renta_finan" ] ;
		$boss_birthday = $_POST[ "birthday_boss" ] ;
		$boss_name = $_POST[ "nom_boss" ] ;
		$boss_city = $_POST[ "birthday_boss_city" ] ;
		$boss_fontion = $_POST[ "fonction_boss" ] ;
		$creation_date = $_POST[ "creationDate" ] ;
		$capital_company = $_POST[ "companyCapital" ] ;
		$resultat_company = $_POST[ "companyResult" ] ;
		$exed_exploit = $_POST[ "exed_exploit" ];
		$nomCommercial = $_POST[ "comName" ];
		$accountantLastName = $_POST["accountantLastName"];

		$upManageo = $_POST["manageoLoaded"] ;
				
		$fldstr .= ", forme_juridique, nbr_etablissement, dern_exercice, duree_exercice, ca, cloture_exercice, valeur_ajoute, renta_com, renta_finan, boss_name, boss_birthday, boss_birthday_city, boss_fonction, creation_date, capital, resultat, exed_exploit, manageoLoaded, nomCommercial, accountantLastName";
		$updstr .= ", '$forme_juridique', '$nbr_etab', '$dern_exo', '$duree_exo', '$ca_company', '$cloture_exo', '$va_company', '$renta_com', '$renta_finan', '$boss_name', '$boss_birthday', '$boss_city', '$boss_fontion', '$creation_date', '$capital_company', '$resultat_company', '$exed_exploit', '$upManageo' ,'$nomCommercial', '$accountantLastName' ";
				
		//Création du client
		$query = "INSERT INTO buyer ( $fldstr ) VALUES ( $updstr )";	
		$rs =& DBUtil::query( $query );
	    
	    if( $rs === false ) 
	    	return $Result;	
		
		$idbuyer = DBUtil::getConnection()->Insert_ID();
		
		//Session::getInstance()->statistique( "buyer_add", $this );
		
		//On insère les données dans la table contact
		//$iddepartment = $_POST["iddepartment"];
		$idfunction = $_POST[ "idfunction" ];
		$faxnumber = addslashes( $_POST[ "Faxnumber" ] );
		$phonenumber = addslashes( $_POST[ "Phonenumber" ] );
		$email = addslashes( $_POST[ "Email" ] );
		
		// Si B2B, alors commentaire
		if( B2B_STRATEGY )
			$commentcont = addslashes( $_POST[ "commentcp" ] );
		
		$gsm = addslashes( $_POST[ "gsm" ] );
		$title = addslashes( $_POST[ "title" ] );
		$contact_info1 = "";//addslashes($_POST[ "contact_info1" ]);
		$contact_info2 = "";//addslashes($_POST[ "contact_info2" ]);	
		$contact_info3 = "";//addslashes($_POST[ "contact_info3" ]);	
		$contact_info4 = "";//addslashes($_POST[ "contact_info4" ]);									
		$contact_info5 = "";//addslashes($_POST[ "contact_info5" ]);
		
		if( B2B_STRATEGY ){ //B2B
			
			$qcont = "INSERT INTO contact ( idcontact,
				idbuyer,
				title,
				lastname,
				firstname,
				idfunction,
				faxnumber,
				phonenumber,
				mail,
				comment,
				gsm,
				optin,
				birthday,
				contact_info1,
				contact_info2,
				contact_info3,
				contact_info4,																
				contact_info5 )
			VALUES (
				'0',
				'$idbuyer',
				'$title',
				'$name',
				'$firstname',
				'$idfunction',
				'$faxnumber',
				'$phonenumber',
				'$email',
				'$commentcont',
				'$gsm',
				'$optin',
				'$birthday',
				'$contact_info1',
				'$contact_info2',
				'$contact_info3',
				'$contact_info4',			
				'$contact_info5'
			)";
			
		}else{ //B2C
			
			$qcont = "INSERT INTO contact ( idcontact,
				idbuyer,
				title,
				lastname,
				firstname,
				idfunction,
				faxnumber,
				phonenumber,
				mail,
				gsm,
				optin,
				birthday,
				contact_info1,
				contact_info2,
				contact_info3,
				contact_info4,																
				contact_info5 )
			VALUES (
				'0',
				'$idbuyer',
				'$title',
				'$name',
				'$firstname',
				'$idfunction',
				'$faxnumber',
				'$phonenumber',
				'$email',
				'$gsm',
				'$optin',
				'$birthday',
				'$contact_info1',
				'$contact_info2',
				'$contact_info3',
				'$contact_info4',			
				'$contact_info5'
			)";
			
		}
	
		$rscont =& DBUtil::query( $qcont );
		
		if( $rscont === false )
			die( "Impossible de mettre à jour le contact principal" );
		
		// Création de l'adresse de facturation
		// Si il ya une adresse on la créer dasn la table billing_adress
		
		$companyBilling 		= 	$_POST['companyBilling'];
		$titleBilling 			= 	$_POST['titleBilling'];
		$nomBilling 			= 	$_POST['nomBilling'];
		$prenomBilling 			= 	$_POST['prenomBilling'];
		$adrBilling 			= 	$_POST['adrBilling'];
		$moreAdrBilling			= 	$_POST['moreAdrBilling'];
		$cpBilling 				= 	$_POST['cpBilling'];
		$cityBilling 			= 	$_POST['cityBilling'];
		$telBilling				= 	$_POST['telBilling'];
		$faxBilling				=	$_POST['faxBilling'];
		
		// On vérifie si l'enregistrement n'est pas présent dans la base de données
		$preReqBilling = "SELECT * FROM billing_adress
						WHERE idbuyer = '$idbuyer'
						AND company = '$companyBilling'
						AND firstname = '$prenomBilling'
						AND lastname = '$nomBilling'
						AND adress = '$adrBilling'
						AND zipcode = '$cpBilling'
						AND city = '$cityBilling'
						AND phonenumber = '$telBilling'
						AND faxnumber = '$faxBilling'";
		
		$rsPreBilling =& DBUtil::query( $preReqBilling );
				   // ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'billing_adress';
		$idbilling_adress = TradeFactory::Indexations($table);
		if( $rsPreBilling->RecordCount() < 1 ){
					
			$qBilling = "INSERT INTO billing_adress (
			idbilling_adress,
					iderp,
					idbuyer,
					idbuyer_erp,
					firstname,
					lastname,
					adress,
					adress_2,
					zipcode,
					city,
					idstate,
					phonenumber,
					faxnumber,
					title,
					company,
					lastupdate,
					username )
					VALUES(
					'$idbilling_adress',
					'',
					'$idbuyer',
					'',
					'$prenomBilling',
					'$nomBilling',
					'$adrBilling',
					'$moreAdrBilling',
					'$cpBilling',
					'$cityBilling',
					'$idstate',
					'$telBilling',
					'$faxBilling',
					'$titleBilling',
					'$companyBilling',
					now(),
					'$user')				
					";
		
		$rsBilling =& DBUtil::query( $qBilling );
		if($rsBilling === false)
			die( "Impossible de créer l'adresse de facturation du client $idbuyer !" );
		
		}
		
		// Si le client est une MAIRIE (idcustomer_profile = 7)
		// Alors on met également la table MAIRIE à jour !
		if($_POST['idcustomer_profile'] == 7){
			
			$nbrHabitant 	= $_POST["nbr_habitant"];
			$nomMaire 		= $_POST["nom_Maire"];
			$prenomMaire 	= $_POST["prenom_Maire"];
			$regionMairie 	= $_POST["mairie_region"];
			$deptMairie 	= $_POST["mairie_dpt"];
			$inseeMairie	= $_POST["insee_code"];
			
			$queryMairie	= "INSERT INTO mairie (
								idbuyer,
								nom_maire,
								prenom_maire,
								region_mairie,
								departement_mairie,
								code_insee,
								nbr_habitant )
								VALUES(
								'$idbuyer',
								'$nomMaire',
								'$prenomMaire',
								'$regionMairie',
								'$deptMairie',
								'$inseeMairie',
								'$nbrHabitant')";
			
			$rsMairie =& DBUtil::query( $queryMairie );
			if($rsMairie === false){
				die( "Impossible de créer les champs liés à la mairie !" );
			}
		}
		
		if( isset( $_POST[ "Email" ] ) && strlen( $_POST[ "Email" ] ) )
			$login = stripslashes( $_POST[ "Email" ] );
		else if( isset( $_POST[ "Faxnumber" ] ) && strlen( $_POST[ "Faxnumber" ] ) )
			$login = stripslashes( $_POST[ "Faxnumber" ] );
		else $login = Util::getRandomPassword();

		$password = $this->createAccount( $idbuyer, 0, $login );
		
		$return = $this->sendRegistrationMail( $idbuyer, 0, $password );
				
	    return $idbuyer;
	    
	}

	/**
	 * Met à jour les choix des couleurs du client
	 * @param string $colors les couleurs
	 * @return int $Result 1 en cas de succès, 0 sinon
	 */
	public function UpdateBuyerColors( $colors ){
		
		$Result = 0;
		$rs =& DBUtil::query( "UPDATE buyer SET colors = '" . $colors . "' WHERE idbuyer = '". $this->buyerinfo[ "idbuyer" ] . "'" );
		
		if( $rs )
			$Result = 1;
		
		return $Result;
		
	}
	
	/**
	 * Retourne les couleurs personnalisées du client
	 * @return string les couleurs 
	 */
	public function GetBuyerColors(){
		
		$Result = "";
		
		$rs =& DBUtil::query( "SELECT colors FROM buyer WHERE idbuyer = '" . $this->buyerinfo[ "idbuyer" ] . "'" );
		
		if( $rs->RecordCount() > 0 )
			$Result = $rs->Fields( "colors" );
		
		return $Result;
		
	}

	/**
	 * Retourne une adresse de livraison
	 * @param int $IdDelivery L'id de l'adresse
	 * @return void
	 */
	public function GetDeliveryInfo( $IdDelivery ){ 
		
		$this->buyerinfo[ "iddelivery" ] = $IdDelivery;
		$data = DBUtil::getConnection()->GetRow( "SELECT * FROM delivery WHERE iddelivery = '$IdDelivery'" );
		
		if( $data ){
			
			$this->deliveryinfo = $data;
			$this->deliveryinfo[ "state" ] = $this->GetStateName( $this->deliveryinfo[ "idstate" ] );
			
		}
		
	}
	
	/**
	 * Retourne une adresse de facturation
	 * @param int $IdBilling L'id de l'adresse
	 * @return void
	 */
	public function GetBillingInfo( $IdBilling ){ 
		
		$this->buyerinfo[ "idbilling_adress" ] = $IdBilling;
		$data = DBUtil::getConnection()->GetRow( "SELECT * FROM billing_adress WHERE idbilling_adress = '$IdBilling'" );
		
		if( $data ){
			
			$this->billinginfo = $data;
			$this->billinginfo[ "state" ] = $this->GetStateName( $this->billinginfo[ "idstate" ] );
			
		}
		
	}

	/**
	 * Verifie si on doit utiliser l'adresse de livraison
	 * et insertion dans la table delivery
	 * @param array l'adresse de livraison
	 * @return int 0 en cas d'échec, l'id de l'adresse de livraison sinon
	 */
	public function UseDelivery($delivery=array())
	{
		if(empty($delivery)) $delivery = $_REQUEST;
	        $Result=0;
	/*	if( empty($delivery[ "Adress" ]) ) echo "Adress missing";
		if( empty($delivery[ "Zipcode" ]) ) echo "zipcode missing";
		if( empty($delivery[ "City" ])) echo "City missing";
		if( empty($$delivery[ "Firstname" ])) echo "Firstname missing";
		if( empty($delivery[ "Lastname" ])) echo "Lastname missing";  */
	        if( !empty($delivery[ "Adress" ]) && !empty($delivery[ "Zipcode" ]) && !empty($delivery[ "City" ]) && !empty($delivery[ "Lastname" ]) )
	        {
		$Adress = addslashes(stripslashes($delivery[ "Adress" ]));
		$Adress_2 = addslashes(stripslashes($delivery[ "Adress_2" ]));
		$Zipcode = addslashes(stripslashes($delivery[ "Zipcode" ]));
		$City = addslashes(stripslashes($delivery[ "City" ]));
		$IdState = addslashes(stripslashes($delivery[ "idstate" ]));
		$Firstname = addslashes(stripslashes($delivery[ "Firstname" ]));
		$Lastname = addslashes(stripslashes($delivery[ "Lastname" ]));
		$Company =  addslashes(stripslashes($delivery[ "Company" ]));
		$Title = addslashes(stripslashes($delivery[ "Title" ]));
		$Phonenumber = addslashes(stripslashes($delivery[ "Phonenumber" ]));
		$Faxnumber = addslashes(stripslashes($delivery[ "Faxnumber" ]));
			
		$IdBuyer = $this->id;
		// ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'delivery';
		$iddelivery = TradeFactory::Indexations($table);
		
	    $rs =& DBUtil::query("insert into delivery (iddelivery,adress,adress_2,zipcode,city,idstate,firstname,lastname,idbuyer,title,company,phonenumber,faxnumber) values('$iddelivery','$Adress','$Adress_2','$Zipcode','$City','$IdState','$Firstname','$Lastname','$IdBuyer','$Title','$Company','$Phonenumber','$Faxnumber')");
		if($rs)
			{
		/*	$rs =& DBUtil::query("Select iddelivery from delivery order by iddelivery DESC");
			if($rs->RecordCount()>0)
				{
				$Result=$rs->Fields( "iddelivery" );
				}
			}*/
		//$this->buyerinfo[ "iddelivery" ]= $Result;
		$Result=$iddelivery;
		$this->GetDeliveryInfo($Result);
	        }}
			//---------------
	    return $Result;
	}

	/**
	 * Adresse de livraison - mise à jour dans la table delivery
	 * @param int $IdDelivery l'id de l'adresse de livraison
	 * @param array $delivery le contenu de l'adresse
	 * @param int $adm 1 si adm commerciale, 0 sinon
	 * @return int 0 en cas d'échec, id de l'adresse de livraison sinon
	 */
	public function UpdateDelivery($IdDelivery,$delivery=array(),$adm=0)
	{
		
		if(empty($delivery)) $delivery = $_REQUEST;
	        $Result=0;
	
	        if( !empty($delivery[ "Adress" ]) && !empty($delivery[ "Zipcode" ]) && !empty($delivery[ "City" ]) && !empty($delivery[ "Lastname" ]) )
	        {

				$Adress = $delivery[ "Adress" ]; 
				$Adress_2 = $delivery[ "Adress_2" ];
				$Zipcode = $delivery[ "Zipcode" ];
				$City = $delivery[ "City" ];
				$IdState = $delivery[ "idstate" ];	
				$Firstname = $delivery[ "Firstname" ]; 
				$Lastname = $delivery[ "Lastname" ];
				$Company = $delivery[ "Company" ];
				$Title = $delivery[ "Title" ];
				$Phonenumber = $delivery[ "Phonenumber" ];
				$Faxnumber = $delivery[ "Faxnumber" ];
				$username = $this->buyerinfo[ "login" ];
				$lastupdate = date( "Y-m-d H:i:s" );
				
				$IdBuyer = $this->id;
				$query = "
				UPDATE delivery 
				SET adress='$Adress',
					adress_2='$Adress_2',
					zipcode='$Zipcode',
					city='$City' ,
					idstate='$IdState',
					firstname='$Firstname',
					lastname='$Lastname',
					idbuyer='$IdBuyer',
					title='$Title', 
					company='$Company', 
					phonenumber='$Phonenumber', 
					faxnumber='$Faxnumber',
					lastupdate='$lastupdate', 
					username='$username'
				WHERE iddelivery=$IdDelivery";

			    $rs =& DBUtil::query( $query );
			    
			    if($rs){
			    	
					$Result= $IdDelivery;
					$this->GetDeliveryInfo($Result);
			    }
			    
	        }
	   	
	   		return $Result;
	}

	/**
	 * Verifie si on doit utiliser l'adresse de facturation
	 * et insertion dans la table billing_adress
	 * @param array l'adresse de facturation
	 * @return int 0 en cas d'échec, l'id de l'adresse de facturation sinon
	 */
	public function UseBilling($billing)
	{
	        $Result=0;
	        if( !empty($billing[ "Adress" ]) && !empty($billing[ "Zipcode" ]) && !empty($billing[ "City" ]) && !empty($billing[ "Lastname" ]) )
	        {
		$Adress = addslashes(stripslashes($billing[ "Adress" ]));
		$Adress_2 = addslashes(stripslashes($billing[ "Adress_2" ]));
		$Zipcode = addslashes(stripslashes($billing[ "Zipcode" ]));
		$City = addslashes(stripslashes($billing[ "City" ]));
		$IdState = addslashes(stripslashes($billing[ "idstate" ]));
		$Firstname = addslashes(stripslashes($billing[ "Firstname" ]));
		$Lastname = addslashes(stripslashes($billing[ "Lastname" ]));
		$Company =  addslashes(stripslashes($billing[ "Company" ]));
		$Title = addslashes(stripslashes($billing[ "Title" ]));
		$Phonenumber = addslashes(stripslashes($billing[ "Phonenumber" ]));
		$Faxnumber = addslashes(stripslashes($billing[ "Faxnumber" ]));
			
	
		$IdBuyer = $this->id;
		   // ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'billing_adress';
		$idbilling_adress = TradeFactory::Indexations($table);
	    $rs =& DBUtil::query("insert into billing_adress (idbilling_adress,adress,adress_2,zipcode,city,idstate,firstname,lastname,idbuyer,title,company,phonenumber,faxnumber) values('$idbilling_adress','$Adress','$Adress_2','$Zipcode','$City','$IdState','$Firstname','$Lastname','$IdBuyer','$Title','$Company','$Phonenumber','$Faxnumber')");
		if($rs)
			{
		/*	$rs =& DBUtil::query("Select idbilling_adress from billing_adress order by idbilling_adress DESC");
			if($rs->RecordCount()>0)
				{
				$Result=$rs->Fields( "idbilling_adress" );
				}*/
			$Result=idbilling_adress;
			}
		//$this->buyerinfo[ "idbilling_adress" ]= $Result;
		$this->GetBillingInfo($Result);
	        }
	    return $Result;
	}

	/**
	 * Adresse de facturation - mise à jour dans la table billing_adress
	 * @param int $IdBilling l'id de l'adresse de facturation
	 * @param array $billing le contenu de l'adresse
	 * @param int $adm 1 si adm commerciale, 0 sinon
	 * @return int 0 en cas d'échec, id de l'adresse de facturation sinon
	 */
	
	
	function UpdateBilling( $IdBilling , $billing = array(),$adm=0 )
	{
		if(empty($billing)) $billing = $_REQUEST;
	        $Result=0;
	
	        if( !empty($billing[ "Adress" ]) && !empty($billing[ "Zipcode" ]) && !empty($billing[ "City" ]) && !empty($billing[ "Lastname" ]) )
	        {
	        	
	        	if( $billing[ "Adress" ] != $this->buyerinfo[ "adress" ]
	        		|| $billing[ "Adress_2" ] != $this->buyerinfo[ "adress_2" ]
	        		|| $billing[ "Zipcode" ] != $this->buyerinfo[ "zipcode" ]
	        		|| $billing[ "City" ] != $this->buyerinfo[ "city" ]
	        		|| $billing[ "idstate" ] != $this->buyerinfo[ "idstate" ]
	        		|| $billing[ "Firstname" ] != $this->buyerinfo[ "firstname" ]
	        		|| $billing[ "Lastname" ] != $this->buyerinfo[ "lastname" ] )
	        		$this->SetUserChanged( true );
	        		
				$Adress = $billing[ "Adress" ];
				$Adress_2 = $billing[ "Adress_2" ];
				$Zipcode = $billing[ "Zipcode" ];
				$City = $billing[ "City" ];
				$IdState = $billing[ "idstate" ];	
				$Firstname = $billing[ "Firstname" ]; 
				$Lastname = $billing[ "Lastname" ];
				$Company = $billing[ "Company" ];
				$Title = $billing[ "Title" ];
				$Phonenumber = $billing[ "Phonenumber" ];
				$Faxnumber = $billing[ "Faxnumber" ];
				$username = $this->buyerinfo[ "login" ];
				$lastupdate = date( "Y-m-d H:i:s" );
				
				$IdBuyer = $this->id;
				$query = "
				UPDATE billing_adress 
				SET adress='$Adress',
					adress_2='$Adress_2',
					zipcode='$Zipcode',
					city='$City' ,
					idstate='$IdState',
					firstname='$Firstname',
					lastname='$Lastname',
					idbuyer='$IdBuyer',
					title='$Title', 
					company='$Company', 
					phonenumber='$Phonenumber', 
					faxnumber='$Faxnumber',
					username='$username',
					lastupdate='$lastupdate' 
				WHERE idbilling_adress=$IdBilling";
			   
			    $rs =& DBUtil::query($query);
	            
	            if($rs){
	            	
					$Result= $IdBilling;
					$this->GetBillingInfo($Result);
					
	            }
	            
	        }
	    return $Result;
	}

	/**
	 * Retourne un taux de remise
	 * @param float $TotalAmount Le montant des achats
	 * @return float 0 en cas d'échec, le taux sinon
	 */
	public function SearchRemise( $TotalAmount ){
		
		$Result = 0.0;
		
		$IdBuyer = $this->id;
		
		$Query = "SELECT MAX(o.rate) AS remrate FROM buyer b,order_rate o WHERE idbuyer = '$IdBuyer' AND  b.idrate_family = o.idrate_family AND '$TotalAmount' > min_price";
		$rs =& DBUtil::query( $Query );
		
		if( $rs->RecordCount() > 0 )
			$Result = $rs->fields( "remrate" );
		
		return $Result;
		
	}

	/**
	 * Récupère les infos de la devise, monnaie en fonction du buyer
	 * @return l'id de la devise
	 */
	public function GetChangeInfo(){
		
		$idstate = $this->buyerinfo[ "idstate" ];
	        if( $this->id > 0) // cas client identifié
	        {
	                $rs =& DBUtil::query("select idcurrency from statecurrency where idstate='". $idstate ."'");
	                if($rs->RecordCount())
	                {
	                        $this->buyerinfo[ "idcurrency" ] = $rs->Fields( "idcurrency" );
	                }
	                else
	                {
	                        $this->buyerinfo[ "idcurrency" ] = DBUtil::getParameter( "idcurrency" );
	                }
	        }
	        else // cas client invité
	        {
	                $this->buyerinfo[ "idcurrency" ] = DBUtil::getParameter( "idcurrency" );
	        }
	
	        GetCurrencyInfo();
	
	        return $this->buyerinfo[ "idcurrency" ];
	}

	/**
	 * Récupere les infos de la devise
	 * @return int $Result 1 en cas de succès, 0 en cas d'échec
	 */
	private function GetCurrencyInfo(){
		
		$Result=0;
		
		$IdCurrency = $this->buyerinfo[ "idcurrency" ];
		$Langue = $this->lang;
	        $rs=& DBUtil::query("select name$Langue ,alias,device from currency where idcurrency='$IdCurrency'");
	        if($rs->RecordCount()>0)
	        {
	                $this->currencyname =stripslashes($rs->Fields("name$Langue"));
	                $this->currencyalias = stripslashes($rs->Fields( "alias" ));
	                $this->devise = $rs->Fields( "device" );
	                $Result=1;
	        }
	        return $Result;
	}


	/**
	 * Vérifie si le pays es assujeti à la TVA
	 * @return int 1 si assujetti, 0 sinon
	 */
	private function SetChoiceVat()
	{
		
		if( !$this->GetStateId() ){
			
			$this->BVat = 1;
			return 1;
			
		}
		
		$State =  $this->GetStateId();
        $rs=& DBUtil::query("select bvat from state where  idstate='$State'");
        
        if($rs->RecordCount())
            $this->BVat = $rs->Fields( "bvat" );

        return $this->BVat;
        
	}

	public function getBVat(){ return $this->BVat; }
	
	/**
	 * Met la TVA à zéro si le pays n'est pas assujeti
	 * @param int l'id de tva
	 * @return int 1 si assujetti, 0 sinon
	 */
	public function ChoiceVat($ProductVat)
	{
		$Result = 0;
		if($this->BVat) $Result = $ProductVat;
		return $Result;
	}

	/**
	 * Récupere la TVA par defaut d'un pays en fonction de son identifiant
	 * Renvoie la tva par defaut si pays assujeti, sinon 0
	 */
	public function GetStateVat()
	{
		$Result=0;
		
		if($this->BVat) 
			$Result = DBUtil::getParameter( "vatdefault" );
			
		return $Result;
		
	}
	
	/**
	 * statistiques
	 * @return string $Str_Insert la chaine a insérer
	 */
	public function statistique()
	{
	// quantity | idcategory | idproduct | reference 
	  $Str_Insert = "'0','0','0',''";
	  return  $Str_Insert;
	}
	
	/**
	 * Récupère toutes les adresses de livraison du client
	 * @return array tableau des adresses de livraison
	 */
	public function LoadDeliveryAddresses(){
		
		$this->deliveryAddresses = array();
		$rs = DBUtil::query(
		 "SELECT iddelivery AS id, adress, adress_2, zipcode, city, idstate, firstname, lastname, company, title, phonenumber, faxnumber FROM delivery WHERE idbuyer = $this->id");
	//	$db = & Session::getInstance()->GetDB();
	//	$rs = $db->Execute( $query );
		
		$i = 0;
		while( !$rs->EOF() ){
			
			foreach( $rs->fields as $key => $value )
				$this->deliveryAddresses[ $i ][ $key ] = $value;
			
			$rs->MoveNext();
			$i++;
			
		}
		
	}
	
	/**
	 * Récupère toutes les adresses de facturation du client
	 * @return array tableau des adresses de facturation
	 */
	public function LoadBillingAddresses(){
		
		$this->billingAddresses = array();
			$rs = DBUtil::query(
		 "SELECT idbilling_adress AS id, adress, adress_2, zipcode, city, idstate, firstname, lastname, company, title, phonenumber, faxnumber FROM billing_adress WHERE idbuyer = $this->id");
		///$db = & Session::getInstance()->GetDB();
		//$rs = $db->Execute( $query );

		$i = 0;
		while( !$rs->EOF() ){
			
			foreach( $rs->fields as $key => $value )
				$this->billingAddresses[ $i ][ $key ] = $value;
			
			$rs->MoveNext();
			$i++;
			
		}
		
	}

	/**
	 * Retourne les infos de l'adresse de livraison en fonction de l'id
	 * @param int $i l'id de l'adresse de livraison
	 * @return mixed tableau des infos de l'adresse en cas de succès, null sinon
	 */
	public function GetDeliveryAddressById( $id ){
		
		$i = 0;
		while( $i < count( $this->deliveryAddresses ) ){
			
			if( $this->deliveryAddresses[ $i ][ "iddelivery" ] == $id )
				return $this->deliveryAddresses[ $i ];
				
			$i++;
		
		}
		
		return null;
		
	}
	
	/**
	 * Retourne les infos de l'adresse de facturation en fonction de l'id
	 * @param int $i l'id de l'adresse de facturation
	 * @return mixed tableau des infos de l'adresse en cas de succès, null sinon
	 */
	public function GetBilllingAddressById( $id ){
		
		$i = 0;
		while( $i < count( $this->billingAddresses ) ){
			
			if( $this->billingAddresses[ $i ][ "id_billing_adress" ] == $id )
				return $this->billingAddresses[ $i ];
				
			$i++;
		
		}
		
		return null;
		
	}
	
	/**
	 * Ajoute une adresse de facturation
	 * @param array $billing tableau des infos de l'adresse de facturation
	 * @param int $adm, 1 pour l'administration commerciale, 0 sinon
	 */ 
	public function AddBillingAddress( $billing = array(),$adm=0 ){
		
		if(empty($billing)) $billing = $_REQUEST;
	        $Result=0;

        if( !empty($billing[ "Adress" ]) && !empty($billing[ "Zipcode" ]) && !empty($billing[ "City" ]) && !empty($billing[ "Lastname" ]) ){
	        
			$Adress = addslashes(stripslashes($billing[ "Adress" ])); 
			$Adress_2 = addslashes(stripslashes($billing[ "Adress_2" ]));
			$Zipcode = addslashes(stripslashes($billing[ "Zipcode" ]));
			$City = addslashes(stripslashes($billing[ "City" ]));
			$IdState = addslashes(stripslashes($billing[ "idstate" ]));	
			$Firstname = addslashes(stripslashes($billing[ "Firstname" ]));
			$Lastname = addslashes(stripslashes($billing[ "Lastname" ]));
			$Company =  addslashes(stripslashes($billing[ "Company" ]));
			$Title = addslashes(stripslashes($billing[ "Title" ]));
			$Phonenumber = addslashes(stripslashes($billing[ "Phonenumber" ]));
			$Faxnumber = addslashes(stripslashes($billing[ "Faxnumber" ]));
			$username = $this->buyerinfo[ "login" ];
			$lastupdate = date( "Y-m-d H:i:s" );
				
			$IdBuyer = $this->id;
		     // ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'billing_adress';
		$idbilling_adress = TradeFactory::Indexations($table);
		    $query = "
			INSERT INTO billing_adress (
			idbilling_adress,
				adress,
				adress_2,
				zipcode,
				city,
				idstate,
				firstname,
				lastname,
				company,
				idbuyer,
				title,
				phonenumber,
				faxnumber,
				username,
				lastupdate)
			VALUES(
			'$idbilling_adress',
				'$Adress',
				'$Adress_2',
				'$Zipcode',
				'$City',
				'$IdState',
				'$Firstname',
				'$Lastname',
				'$Company',
				'$IdBuyer',
				'$Title',
				'$Phonenumber',
				'$Faxnumber',
				'$username',
				'$lastupdate')";
			
		    $rs =& DBUtil::query( $query );
			if( $rs !== false ){
			/*	
				$rs =& DBUtil::query( "Select MAX(idbilling_adress) AS idbilling from billing_adress" );
				if($rs->RecordCount()>0){
					
					$Result=$rs->Fields( "idbilling" );
					
				}*/
				$Result=$idbilling_adress;
		
				
			}
			
			
			$this->GetBillingInfo( $Result );
	    
	    }
	    
	    return $Result;
	    
	}
	
	
	/**
	 * Ajoute une adresse de livraison
	 * @param array $delivery tableau des infos de l'adresse de livraison
	 * @param int $adm, 1 pour l'administration commerciale, 0 sinon
	 */ 
	public function AddDeliveryAddress( $delivery = array(),$adm=0 ){
	
		if(empty($delivery)) 
			$delivery = $_REQUEST;
			
	    $Result=0;
	        
	
	    if( !empty($delivery[ "Adress" ]) && !empty($delivery[ "Zipcode" ]) && !empty($delivery[ "City" ]) && !empty($delivery[ "Lastname" ]) ){

		$Adress = addslashes(stripslashes($delivery[ "Adress" ])); 
		$Adress_2 = addslashes(stripslashes($delivery[ "Adress_2" ]));
		$Zipcode = addslashes(stripslashes($delivery[ "Zipcode" ]));
		$City = addslashes(stripslashes($delivery[ "City" ]));
		$IdState = addslashes(stripslashes($delivery[ "idstate" ]));	
		$Firstname = addslashes(stripslashes($delivery[ "Firstname" ]));
		$Lastname = addslashes(stripslashes($delivery[ "Lastname" ]));
		$Company =  addslashes(stripslashes($delivery[ "Company" ]));
		$Title = addslashes(stripslashes($delivery[ "Title" ]));
		$Phonenumber = addslashes(stripslashes($delivery[ "Phonenumber" ]));
		$Faxnumber = addslashes(stripslashes($delivery[ "Faxnumber" ]));
		$username = $this->buyerinfo[ "login" ];
		$lastupdate = date( "Y-m-d H:i:s" );
				
		$IdBuyer = $this->id;
			// ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'delivery';
		$iddelivery = TradeFactory::Indexations($table);
		$query = "
		INSERT INTO delivery (
		iddelivery,
			adress,
			adress_2,
			zipcode,
			city,
			idstate,
			firstname,
			lastname,
			company,
			idbuyer,
			title,
			phonenumber,
			faxnumber,
			username,
			lastupdate)
		VALUES( 
		'$iddelivery',
			'$Adress',
			'$Adress_2',
			'$Zipcode',
			'$City',
			'$IdState',
			'$Firstname',
			'$Lastname',
			'$Company',
			'$IdBuyer',
			'$Title',
			'$Phonenumber',
			'$Faxnumber',
			'$username',
			'$lastupdate')";
		
	    $rs =& DBUtil::query( $query );
		if($rs)
			{
		/*	$rs =& DBUtil::query("Select MAX(iddelivery) AS iddelivery FROM delivery");
			if($rs->RecordCount()>0)
				{
				$Result=$rs->Fields( "iddelivery" );
				}*/
					$Result=$iddelivery;
			}
		//$this->buyerinfo[ "iddelivery" ]= $Result;
		$this->GetDeliveryInfo($Result);
	    }
	    //------------------
	    return $Result;
	
	}
	
	/**
	 * Définit l'adresse de livraison à utiliser
	 * @param int $IdDelivery l'id de l'adresse de livraison
	 * @return void
	 */
	public function SetUseDeliveryAddress( $IdDelivery ){
		
		$this->GetDeliveryInfo( $IdDelivery );
		
	}
	
	/**
	 * Définit l'adresse de facturation à utiliser
	 * @param int $IdBilling l'id de l'adresse de facturation
	 * @return void
	 */
	function SetUseBillingAddress( $IdBilling ){
		
		$this->GetBillingInfo( $IdBilling );
		
	}
	
	/**
	 * @deprecated
	 */
	public function SetUserChanged( $useless = true ){
		
		return true;
		
	}
	
	/**
	* Recharge les infos de la base dans les variables de session
	* @param int $idbuy l'id du client
	* @param int $idcontact l'id du contact
	* @return void
	*/
	public function ReloadBuyerInfo( $idbuy, $idcontact ){
			
			$rsdb = &Session::getInstance()->GetDB();
			
			$queryb = "SELECT * FROM buyer WHERE idbuyer = " . $idbuy . "";
			$dbb = $rsdb->Execute( $queryb );
			
			$queryc = "SELECT * FROM contact WHERE idbuyer = " . $idbuy . " AND idcontact = " . $idcontact . "";
			$dbc=$rsdb->Execute( $queryc );
			
			$queryu = "SELECT * FROM user_account WHERE idbuyer = " . $idbuy . " AND idcontact = " . $idcontact . "";
			$dbu = $rsdb->Execute( $queryu );
			
			if( $dbb === false )
				die( "Impossible de récupérer les données de l'acheteur" );
			
			if( $dbc === false )
				die( "Impossible de récupérer les données du contact" );
			
			if( $dbu === false )
				die( "Impossible de récupérer les données d'identification du contact" );
			
			$this->id = $idbuy;
	        $this->buyerinfo[ "idbuyer" ]			= $idbuy;
			$this->buyerinfo[ "admin" ]				= $dbb->fields( "admin" );
	        $this->buyerinfo[ "idcompany" ]			= $dbb->fields( "idcompany" );
	        $this->buyerinfo[ "company" ]			= $dbb->fields( "company" );
	        $this->buyerinfo[ "iduser" ]			= $dbb->fields( "iduser" );
	        $this->buyerinfo[ "idbuyer_family" ]	= $dbb->fields( "idbuyer_family" );
		    $this->buyerinfo[ "idactivity" ]		= $dbb->fields( "idactivity" );
	        $this->buyerinfo[ "title" ]				= $dbc->fields( "title" );
	        $this->buyerinfo[ "firstname" ]			= $dbc->fields( "firstname" );
	        $this->buyerinfo[ "lastname" ]			= $dbc->fields( "lastname" );
	        $this->buyerinfo[ "iddepartment" ]		= $dbc->fields( "iddepartment" );
	        $this->buyerinfo[ "department" ]		= $this->getLabel( "department", $this->buyerinfo[ "iddepartment" ] );
	        $this->buyerinfo[ "idfunction" ]		= $dbc->fields( "idfunction" );	
	        $this->buyerinfo[ "function" ]			= $this->getLabel( "function", $this->buyerinfo[ "idfunction" ] );	
	        $this->buyerinfo[ "adress" ]			= $dbb->fields( "adress" );
	        $this->buyerinfo[ "adress_2" ]			= $dbb->fields( "adress_2" );
	        $this->buyerinfo[ "zipcode" ]			= $dbb->fields( "zipcode" );
	        $this->buyerinfo[ "city" ]				= $dbb->fields( "city" );
	        $this->buyerinfo[ "idstate" ]			= $dbb->fields( "idstate" );
	        $this->buyerinfo[ "state" ]				= $this->GetStateName();
	        $this->buyerinfo[ "phonenumber" ]		= $dbc->fields( "phonenumber" );
	        $this->buyerinfo[ "faxnumber" ]			= $dbc->fields( "faxnumber" );
	        $this->buyerinfo[ "email" ]				= $dbc->fields( "mail" );
	        $this->buyerinfo[ "firstorder" ]		= $dbb->fields( "firstorder" );
	        $this->buyerinfo[ "totalbonus" ]		= $dbb->fields( "totalbonus" );
	        $this->buyerinfo[ "idcurrency" ]		= $dbb->fields( "idcurrency" );
	        $this->buyerinfo[ "login" ]				= $dbu->fields( "login" );
	        $this->buyerinfo[ "password" ]			= $dbu->fields( "password" );
	        $this->buyerinfo[ "logo" ]				= $dbb->fields( "logo" );
	        $this->buyerinfo[ "catalog_right" ]		= $dbb->fields( "catalog_right" );
	        $this->buyerinfo[ "idcustomer_profile" ]= $dbb->fields( "idcustomer_profile" );
			//$this->buyerinfo[ "iddelivery" ]		= $dbb->fields( "iddelivery" );
			$this->buyerinfo[ "comment" ]			= $dbb->fields( "comment" );
			$this->buyerinfo[ "idsource" ]			= $dbb->fields( "idsource" );	
			$this->buyerinfo[ "source" ]			= $this->getLabel( "source", $this->buyerinfo[ "idsource" ] );	
			$this->buyerinfo[ "cond_payment" ]		= $dbb->fields( "cond_payment" );
			$this->buyerinfo[ "deliv_payment" ]		= $dbb->fields( "deliv_payment" );
			$this->buyerinfo[ "buyer_info1" ]		= $dbb->fields( "buyer_info1" );
			$this->buyerinfo[ "buyer_info2" ]		= $dbb->fields( "buyer_info2" );
			$this->buyerinfo[ "buyer_info3" ]		= $dbb->fields( "buyer_info3" );
			$this->buyerinfo[ "buyer_info4" ]		= $dbb->fields( "buyer_info4" );
			$this->buyerinfo[ "buyer_info5" ]		= $dbb->fields( "buyer_info5" );
			$this->buyerinfo[ "buyer_info6" ]		= $dbb->fields( "buyer_info6" );
			$this->buyerinfo[ "buyer_info7" ]		= $dbb->fields( "buyer_info7" );
			$this->buyerinfo[ "buyer_info8" ]		= $dbb->fields( "buyer_info8" );
			$this->buyerinfo[ "buyer_info9" ]		= $dbb->fields( "buyer_info9" );		
			$this->buyerinfo[ "buyer_info10" ]		= $dbb->fields( "buyer_info10" );
			$this->buyerinfo[ "rebate_rate" ]		= $dbb->fields( "rebate_rate" );																									
			
			$_SESSION[ $this->sessionkey ] = &$this->buyerinfo;
			
			
			Session::getInstance()->setBuyer( $idbuy, $idcontact );
			
	}
	
	/**
	 * Génère un mot de passe aléatoire
	 * @param int $longueur la longueur du mot de passe
	 * @return string le mot de passe généré
	 */
	private function RandomPwd( $longueur ){
		
		$minuscules = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
		
		$majuscules = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
		
		$code = ""; //On déclare notre variable
		for( $i = 1 ; $i <= $longueur ; $i++ ){
			//On génère un type aléatoire
			$type = rand( 0, 2 );
			
			switch( $type ){
				case 0:
					$caractere = rand( 0, 9 );
					$code .= $caractere;
					break;
				case 1:
					$nbre_aleatoire = rand( 0, 25 );
					$caractere = $majuscules[ $nbre_aleatoire ];
					$code .= $caractere;
					break;
				case 2:
					$nbre_aleatoire = rand( 0, 25 );
					$caractere = $minuscules[ $nbre_aleatoire ];
					$code .= $caractere;
					break;
			}
			
		}
		
		return $code; //On retourne le code généré au complet
		
	}
	
	/**
	 * Ajout d'un contact
	 * Utilisé seulement dans le Back Office
	 * @param int $idbuyer l'id du client
	 * @return mixed true en cas de succès, message d'erreur sinon
	 */
	public function AddContact( $idbuyer ){
		
		global $GLOBAL_START_PATH;
		
		/* protection XSS */
		$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );
		
		if( !$idbuyer )
			return false;
		
		//Vérification de l'unicité du mail
		
		$email = $_POST[ "mail" ];
					
		if( strlen( $email ) != 0 ){
			
			$Query = "SELECT idbuyer FROM contact WHERE mail = '$email'";
			$rs = DBUtil::query( $Query );
			
			if( $rs->RecordCount() ){			
				
				$idb = $rs->fields( "idbuyer" );
				return "badmail";
				
			}
			
		}
		
		$reqid = "SELECT MAX( idcontact ) as id FROM contact WHERE idbuyer = $idbuyer";
		$rsid =& DBUtil::query( $reqid );
		
		if( $rsid === false || !$rsid->RecordCount() )
			die( "Impossible de récupérer l'identifiant du contact" );
		
		$idcontact			= ( $rsid->fields( "id" ) + 1 );
		$lastname			= isset( $_POST[ "lastname" ] )			? addslashes( stripslashes( ucfirst( strtolower( $_POST[ "lastname" ] ) ) ) ) : "";
		$firstname			= isset( $_POST[ "firstname" ] )		? addslashes( stripslashes( ucfirst( strtolower( $_POST[ "firstname" ] ) ) ) ) : "";
		$iddepartment		= isset( $_POST[ "iddepartment" ] )		? $_POST[ "iddepartment" ] : 0;
		$idfunction			= isset( $_POST[ "idfunction" ] )		? $_POST[ "idfunction" ] : 0;
		$faxnumber			= isset( $_POST[ "faxnumber" ] )		? $_POST[ "faxnumber" ] : "";
		$phonenumber		= isset( $_POST[ "phonenumber" ] )		? $_POST[ "phonenumber" ] : "";
		$mail				= isset( $_POST[ "mail" ] )				? $_POST[ "mail" ] : "";
		$title				= isset( $_POST[ "title" ] )			? $_POST[ "title" ] : "";
		$gsm				= isset( $_POST[ "gsm" ] )				? $_POST[ "gsm" ] : "";
		$comment			= isset( $_POST[ "comment" ] )			? addslashes( stripslashes( $_POST[ "comment" ] ) ) : "";
		$contact_info1		= isset( $_POST[ "contact_info1" ] )	? addslashes( stripslashes( $_POST[ "contact_info1" ] ) ) : "";
	
		/*$contact_info2	= isset( $_POST[ "contact_info2" ] )	? $_POST[ "contact_info2" ] : "";
		$contact_info3		= isset( $_POST[ "contact_info3" ] )	? $_POST[ "contact_info3" ] : "";
		$contact_info4		= isset( $_POST[ "contact_info4" ] )	? $_POST[ "contact_info4" ] : "";
		$contact_info5		= isset( $_POST[ "contact_info5" ] )	? $_POST[ "contact_info5" ] : "";*/									
		
		$username = Util::html_escape( User::getInstance()->get( "login" ) );
		$lastupdate = date( "Y-m-d H:i:s" );
		
		$query = "INSERT INTO contact (
				idcontact,
				idbuyer,
				title,
				lastname,
				firstname,
				iddepartment,
				idfunction,
				faxnumber,
				phonenumber,
				mail,
				comment,
				gsm,
				optin,
				birthday,
				username,
				lastupdate,
				contact_info1
			)VALUES(
				'$idcontact',
				'$idbuyer',
				'$title',
				'$lastname',
				'$firstname',
				'$iddepartment',
				'$idfunction',
				'$faxnumber',
				'$phonenumber',
				'$mail',
				'$comment',
				'$gsm',
				'$optin',
				'$birthday',
				'$username',
				'$lastupdate',
				'$contact_info1'
			)";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible d'enregistrer le contact" );
		
		if( isset( $_POST[ "mail" ] ) && strlen( $_POST[ "mail" ] ) )
			$login = stripslashes( $_POST[ "mail" ] );
		else if( isset( $_POST[ "faxnumber" ] ) && strlen( $_POST[ "faxnumber" ] ) )
			$login = stripslashes( $_POST[ "faxnumber" ] );
		else $login = Util::getRandomPassword();
			
		$password = $this->createAccount( $idbuyer, $idcontact, $email );
		$this->sendRegistrationMail( $idbuyer, $idcontact, $password );

		return true;
				
	}
	
	/**
	 * Modification d'un contact
	 * @param int $idcontact l'id du contact
	 * @param int $idbuyer l'id du client
	 * @return mixed true en cas de succès, message d'erreur sinon
	 */
	public function UpdateContact( $idcontact = 0, $idbuyer){
		
		global $GLOBAL_START_PATH;

		$lastname =			isset( $_POST[ "lastname" ] )		? addslashes( stripslashes( ucfirst( strtolower( $_POST[ "lastname" ] ) ) ) ) : "";
		$firstname =		isset( $_POST[ "firstname" ] )		? addslashes( stripslashes( ucfirst( strtolower( $_POST[ "firstname" ] ) ) ) ) : "";
		$title =			isset( $_POST[ "title" ] )			? $_POST[ "title" ] : "";
		$iddepartment =		isset( $_POST[ "iddepartment" ] )	? $_POST[ "iddepartment" ] : 0;
		$idfunction =		isset( $_POST[ "idfunction" ] )		? $_POST[ "idfunction" ] : 0;
		$faxnumber =		isset( $_POST[ "faxnumber" ] )		? $_POST[ "faxnumber" ] : "";
		$phonenumber =		isset( $_POST[ "phonenumber" ] )	? $_POST[ "phonenumber" ] : "";
		$mail =				isset( $_POST[ "mail" ] )			? $_POST[ "mail" ] : "";
		$gsm =				isset( $_POST[ "gsm" ] )			? $_POST[ "gsm" ] : "";
		$optin =			isset( $_POST[ "optin" ] )			? $_POST[ "optin" ] : "";
		$birthday =			isset( $_POST[ "birthday" ] )		? $_POST[ "birthday" ] : "";
		$comment =			isset( $_POST[ "comment" ] )		? addslashes( stripslashes( $_POST[ "comment" ] ) ) : "";
		$contact_info1 =	isset( $_POST["contact_info1"] )	? addslashes( stripslashes( $_POST[ "contact_info1" ] ) ): "";

		/*$contact_info2 =	isset( $_POST[ "contact_info2" ] )	? $_POST[ "contact_info2" ] : "";
		$contact_info3 =	isset( $_POST[ "contact_info3" ] )	? $_POST[ "contact_info3" ] : "";
		$contact_info4 =	isset( $_POST[ "contact_info4" ] )	? $_POST[ "contact_info4" ] : "";
		$contact_info5 =	isset( $_POST[ "contact_info5" ] )	? $_POST[ "contact_info5" ] : "";*/							
		$username = Util::html_escape( User::getInstance()->get( "login" ) );
		$lastupdate = date( "Y-m-d H:i:s" );
		
		if( strlen( $mail ) != 0 ){
			
			$Query = "SELECT idbuyer, idcontact FROM contact WHERE mail = '$mail' AND idbuyer != $idbuyer AND idcontact != $idcontact";
			$rs =& DBUtil::query( $Query );
			
			if( $rs->RecordCount() ){			
				
				$idb = $rs->fields( "idbuyer" );
				$idc = $rs->fields( "idcontact" );
				
				if( $idb != $idbuyer && $idc != $idcontact )
					return array( "badmail", $idb );
				
			}
			
			$Query = "SELECT idbuyer, idcontact FROM user_account WHERE login = '$mail' AND idbuyer != $idbuyer AND idcontact != $idcontact";
			$rs =& DBUtil::query( $Query );
			
			if( $rs->RecordCount() ){			
				
				$idb = $rs->fields( "idbuyer" );
				$idc = $rs->fields( "idcontact" );
				
				if( $idb != $idbuyer && $idc != $idcontact )
					return array( "loginalreadyexist", $idb );
				
			}
			
		}
		
		$query = "UPDATE contact SET
				lastname = '$lastname',
				firstname = '$firstname',
				iddepartment = '$iddepartment',
				idfunction = '$idfunction',
				faxnumber = '$faxnumber',
				phonenumber = '$phonenumber',
				mail = '$mail',
				gsm = '$gsm',
				optin = '$optin',
				bithday = '$birthday',
				comment = '$comment',
				username = '$username',
				lastupdate = '$lastupdate',
				title = '$title',
				contact_info1 = '$contact_info1'
				WHERE idcontact = '$idcontact' AND idbuyer = '$idbuyer'";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de modifier le contact" );
		
		//Gestion du compte client pour le Front Office
	    
	    //Création d'un compte client Front Office - Aucun compte existant
	    if( isset( $_POST[ "GenerateLogin" ] ) ){
	    	
	    	if( isset( $_POST[ "mail" ] ) && strlen( $_POST[ "mail" ] ) )
				$login = stripslashes( $_POST[ "mail" ] );
			else if( isset( $_POST[ "faxnumber" ] ) && strlen( $_POST[ "faxnumber" ] ) )
				$login = stripslashes( $_POST[ "faxnumber" ] );
			else $login = Util::getRandomPassword();
				
	    	$password = $this->createAccount( $idbuyer, $idcontact, $login );
	    	
	    	$return = $this->sendRegistrationMail( $idbuyer, $idcontact, $password );
	    	
	    }
	    
	    //Génération d'un nouveau mot de passe pour le compte du Front Office
	    if( isset( $_POST[ "GeneratePassword" ] ) ){
	    	
	    	$password = Util::getRandomPassword();
		    DBUtil::query( "UPDATE user_account SET password = MD5('$password') WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1" );

	    	$return = $this->sendRegistrationMail( $idbuyer, $idcontact, $password );
	    	
	    }
		
		return true;
		
	}
	
	/**
	 * Récupère une valeur
	 * @param string $table le nom de la table
	 * @param int $primaryKeyValue la valeur de la clé primaire
	 * @return mixed la valeur
	 */
/*	private function getLabel( $table,  $primaryKeyValue ){
	
		//$db =& Session::getInstance()->GetDB();
		//$lang = Session::getInstance()->getlang();
		
	/*	$query = "
		SELECT `$table$lang` AS label
		FROM `$table`
		WHERE `id$table` = '$primaryKeyValue'
		LIMIT 1";*/
		
		//$rs = $db->Execute( $query );
/*		$rs = DBUtil::query("SELECT `$table$lang` AS label
		FROM `$table`
		WHERE `id$table` = '$primaryKeyValue'
		LIMIT 1");
		if( $rs === false )
			die( "Impossible de récupérer les informations de l'acheteur" );
		
		if( !$rs->RecordCount() )
			return "";
			
		return $rs->fields( "label" );
		
	}*/
	
	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Autorise le client à changer d'email ou non
	 * @param string $email l'adresse email sélectionnée par le client
	 * @param int $idbuyer l'identifiant du client
	 * @return true si le client est autorisé à utiliser cette adresse email, sinon false
	*/
	
	public static function emailAvailable( $email, $idbuyer ){
		
		$db =&Session::getInstance()->GetDB();
		
		$query = "
		SELECT idbuyer
		FROM contact
		WHERE idbuyer <> '$idbuyer'
		AND mail LIKE '$email'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de vérifier la disponibilité de l'adresse email" );
			
		return $rs->RecordCount() == 0;
	
	}

	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Envoie le mail d'enregistrement
	 * @param int $idbuyer l'id du client
	 * @param int $idcontact l'id du contact
	 * @param string password le mot de passe pour le compte
	 * @return true si le mail est envoyé, false sinon
	 */
	private function sendRegistrationMail( $idbuyer, $idcontact = 0, $password ){
		
		global	$GLOBAL_START_PATH,
				$GLOBAL_START_URL;
		
		$query = "
		SELECT b.iduser,
			ua.login,
			c.mail
		FROM buyer b, contact c, user_account ua
		WHERE b.idbuyer = '$idbuyer'
		AND b.idbuyer = c.idbuyer
		AND c.idcontact = '$idcontact'
		AND b.idbuyer = ua.idbuyer
		AND ua.idcontact = '$idcontact'
		LIMIT 1";
		
		$db =& Session::getInstance()->GetDB();
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de confirmer l'inscription au client" );
		
		$iduser		= $rs->fields( "iduser" );
		$login		= $rs->fields( "login" );
		$email		= $rs->fields( "mail" );
		
		if( empty( $email ) )
			return;
		
		//charger le modèle de mail
		
		include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
		
		$editor = new MailTemplateEditor();
		
		$editor->setLanguage( substr( Session::getInstance()->getlang(), 1 ) ); //@todo : imposer la langue du destinataire
		$editor->setTemplate( "BUYER_REGISTRATION" );
		
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( $idbuyer, $idcontact );
		$editor->setUseBuyerPassword( $password );
		$editor->setUseCommercialTags( $iduser );
		
		$message = $editor->unTagBody();
		$subject = $editor->unTagSubject();
		
		//envoyer le mail
		
		include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
		
		$mailer = new htmlMimeMail();
		
		$mailer->setFrom( '"' . DBUtil::getParameterAdmin( "ad_sitename" ) . '" <' . DBUtil::getParameterAdmin( "ad_mail" ) . '>');
		
		$mailer->setSubject( $subject );
		$mailer->setHtml( $message );
		
		return $mailer->send( array( $email ) );
		
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne une valeur de la table buyer pour un client
	 * @static
	 * @param int $idbuyer l'id du client
	 * @param string $fieldName le nom du champ à récupérer
	 * @return mixed la valeur
	 */
	public static function getDBValue( $idbuyer, $fieldName ){
		
		$query = "SELECT `$fieldName` FROM buyer WHERE idbuyer = '$idbuyer' LIMIT 1";
		
		$db =&Session::getInstance()->GetDB();
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les valeurs de `$fieldName` pour le client n°$idbuyer" );
			
		if( !$rs->RecordCount() )
			return false;

		return $rs->fields( $fieldName );

	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Indique si le client à été relancé
	 * @static
	 * @param int $idbuyer l'id du client
	 * @return bool true si le client a déjà été relancé, false sinon
	 */
	 
	public static function isBuyerRelauched( $idbuyer ){
		
		$db = &Session::getInstance()->GetDB();
		
		$q = "
		SELECT COUNT(*) AS relaunchCount
		FROM `billing_buyer`
		WHERE idbuyer = '$idbuyer'
		AND (
			relaunch_date_1 <> '0000-00-00'
			OR relaunch_date_2 <> '0000-00-00'
			OR relaunch_date_3 <> '0000-00-00'
		)";
		
		$r = $db->Execute($q);
		
		if($r===false)
			die ("Impossible de contrôler les relances du client");
		
		return $r->fields( "relaunchCount" ) > 0;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	/*
	 * Retourne l'encours du client
	 * @param $IdBuyer l'identifiant du client
	 * return float
	 */
	 
	public static function GetEncours( $IdBuyer ){
		// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "
		SELECT total_amount, down_payment_type, down_payment_value 
		FROM `order` 
		WHERE idbuyer = '$IdBuyer' 
		AND ( status='" . Order::$STATUS_ORDERED . "' OR status='" . Order::$STATUS_INVOICED . "' )";
		
		$rs =& DBUtil::query( $query );
	
		if( $rs === false )
			die( "Impossible de récupérer l'encours du client" );
		
		$encours = 0;
		
		for( $i = 0 ; $i < $rs->Recordcount() ; $i++ ){
			
			if( $rs->fields( "down_payment_type" ) == "rate" )
				$down_payment_amount = round( $rs->fields( "total_amount" ) * $rs->fields( "down_payment_value" ) / 100, 2 );
			else
				$down_payment_amount = $rs->fields( "down_payment_value" );
			
			$encours = $encours + $rs->fields( "total_amount" ) - $down_payment_amount;
			
			$rs->MoveNext();
			
		}
		
		return $encours;
		
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne la remise commerciale du client
	 * @param int $idbuyer l'id du client
	 * @return mixed array tableau avec id de la remise et taux de remise ou bool false si pas de remise
	 */	
 	public static function getBuyerCommercialRemise($idbuyer){
 // ------- Mise à jour de l'id gestion des Index  #1161		
 		$rs = DBUtil::query( 
 		
 			"SELECT idorder_rate_buyer, rate FROM order_rate_buyer
			WHERE idbuyer='$idbuyer'
			AND amount_solde > 0.0
			AND end_date> NOW()"
 		
 		);
 		
		
		if( !$rs->RecordCount() ){
			return false;
			
			$idorder_rate_buyer = $rs->fields("idorder_rate_buyer");
			$rate = $rs->fields("rate");
			
			return array($idorder_rate_buyer,$rate);
		
		}else{
			
			return false;
			
		}
 	}
 	
 	//------------------------------------------------------------------------------------------------------------
 	
 	/**
 	 * D
 	 */
 	 public static function applyCommercialDiscount( $idbuyer, $discount_amount ){
 	 	
 	 	if( $discount_amount == 0.0 )
 	 		return;
 	 		
 	 	$order_rate_buyer = self::getBuyerCommercialRemise( $idbuyer );
		
		if( !is_array( $order_rate_buyer ) )
			return;
			
		$idorder_rate_buyer = $order_rate_buyer[ 0 ];
		
		$solde = self::GetBuyerCommercialSolde( $idorder_rate_buyer );
        	
        $solde = $discount_amount > $solde ? 0.0 : $solde - $discount_amount;
 	
   		$ret =& DBUtil::query( "UPDATE order_rate_buyer SET amount_solde = '$solde' WHERE idorder_rate_buyer = '$idorder_rate_buyer' LIMIT 1" );
	
    	if( $ret ===false )
   			die("Impossible de mettre à jour le solde de la remise commerciale");
      
 	 }
 	 
 	//------------------------------------------------------------------------------------------------------------
 	/**
	 * Retourne le solde de la remise commerciale du client
	 * @param int $idorder_rate_buyer l'id de la remise commerciale
	 * @return float le solde de la remise commerciale
	 */	
 	public static function getBuyerCommercialSolde($idorder_rate_buyer){
 		
 		$db = &Session::getInstance()->GetDB();
 		
 	 	$Query = "SELECT amount_solde FROM order_rate_buyer
				WHERE idorder_rate_buyer=$idorder_rate_buyer";
		$rs = $db->Execute($Query);
		
		if($rs===false)
			die("Impossible de récupérer le solde de la remise commerciale du client");
		

		return $rs->fields("amount_solde");
			
 	}
 	//------------------------------------------------------------------------------------------------------------
 	/**
 	 * Indique si la remise commerciale du client est valide
 	 * @param int $id_order_rate_buyer l'id de la remise commerciale
 	 * @return bool true si la remise est valide, false sinon
 	 */
 	 
 	public static function checkBuyerCommercialRemiseValidity($idorder_rate_buyer){
 		
 		$db = &Session::getInstance()->GetDB();
 		
 		$today = date("Y-m-d");
 		
 		$Query = "SELECT rate FROM order_rate_buyer
				WHERE idorder_rate_buyer=$idorder_rate_buyer
				AND amount_solde>0
				AND end_date>'$today'";
		$rs = $db->Execute($Query);
		
		if($rs===false)
			die("Impossible de contrôler la validité de la remise commerciale du client");
		

		if($rs->RecordCount()>0)
			return true;
		else
			return false;
			
 	}

 	//------------------------------------------------------------------------------------------------------------
 	
 	/**
 	 * Retourne la liste des informations personnalisée pour les clients
 	 * @static
 	 * @return array un tableau avec pour clés `buyer_extra_information`.`idbuyer_extra_information` et pour valeurs `buyer_extra_information`.`information$lang`
 	 */
 	public static function getExtraInformations(){
 		
 		$lang = Session::getInstance()->getLang();
 		
 		$query = "
		SELECT idbuyer_extra_information, information$lang
		FROM buyer_extra_information
		ORDER BY idbuyer_extra_information ASC";
		
		$rs =& DBUtil::query( $query );
		
		$extra_information = array();
		while( !$rs->EOF() ){
			
			$extra_information[ $rs->fields( "idbuyer_extra_information" ) ] = $rs->fields( "information$lang" );
			
			$rs->MoveNext();
			
		}
 		
 		return $extra_information;
 		
 	}
	
	//------------------------------------------------------------------------------------------------------------
 	
 	/**
 	 * Retourne l'information demandée pour un client donné
 	 * @static
 	 * @param $idbuyer int le numéro de client
 	 * @param int $idbuyer_extra_information l'identifiant de l'information demandée
 	 * @return mixed l'information demandée si elle est trouvée, sinon false
 	 */
	public static function getExtraInformationValue( $idbuyer, $idbuyer_extra_information ){
		
		$query = "
		SELECT `value`
		FROM `buyer_extra_information_values`
		WHERE idbuyer = '$idbuyer'
		AND idbuyer_extra_information = '$idbuyer_extra_information'
		LIMIT 1";

		$rs =& DBUtil::query( $query );
		
		return $rs->RecordCount() ? $rs->fields( "value" ) : false;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
 	
 	/**
 	 * Retourne la liste des informations ERP pour les clients
 	 * @static
 	 * @return array un tableau avec pour clés `buyer_erp_information`.`idbuyer_erp_information` et pour valeurs `buyer_erp_information`.`information$lang`
 	 */
 	public static function getERPInformations(){
 		
 		$lang = Session::getInstance()->getLang();
 		
 		$query = "
		SELECT idbuyer_erp_information, information$lang
		FROM buyer_erp_information
		ORDER BY idbuyer_erp_information ASC";
		
		$rs =& DBUtil::query( $query );
		
		$erp_information = array();
		while( !$rs->EOF() ){
			
			$erp_information[ $rs->fields( "idbuyer_erp_information" ) ] = $rs->fields( "information$lang" );
			
			$rs->MoveNext();
			
		}
 		
 		return $erp_information;
 		
 	}
	
	//------------------------------------------------------------------------------------------------------------
 	
 	/**
 	 * Retourne l'information demandée pour un client donné
 	 * @static
 	 * @param $idbuyer int le numéro de client
 	 * @param int $idbuyer_erp_information l'identifiant de l'information demandée
 	 * @return mixed l'information demandée si elle est trouvée, sinon false
 	 */
	public static function getERPInformationValue( $idbuyer, $idbuyer_erp_information ){
		
		$query = "
		SELECT `value`
		FROM `buyer_erp_information_values`
		WHERE idbuyer = '$idbuyer'
		AND idbuyer_erp_information = '$idbuyer_erp_information'
		LIMIT 1";

		$rs =& DBUtil::query( $query );
		
		return $rs->RecordCount() ? $rs->fields( "value" ) : false;
		
	}
	
	//------------------------------------------------------------------------------------------------------------

 	/**
 	 * Retourne le montant des avoirs
 	 * @deprecated
 	 * @param int $idbuyer le numéro du client
 	 * @return float le montant des avoirs
 	 */
 	 
 	public static function getBuyerCreditBalance($idbuyer){
 		
 		$db = &Session::getInstance()->GetDB();
 // ------- Mise à jour de l'id gestion des Index  #1161			
 		$Query = "SELECT credit_balance FROM buyer
				WHERE idbuyer = '$idbuyer'";
		$rs = $db->Execute($Query);
		
		if($rs===false)
			die("Impossible de récupérer le solde des avoirs du client");
		

		return $rs->fields("credit_balance");
				
 	}
 	
 	//------------------------------------------------------------------------------------------------------------
 	
	/**
	 * Teste l'unicité du login
	 * @deprecated
	 * @param string $ log le login à tester
	 * @return bool true si le login est disponible, false sinon
	 */
	private function IsUnique($log,$idbuyer,$idcontact=0){
		
		$bdd = &Session::getInstance()->GetDB();
		$Query = "SELECT idbuyer FROM user_account WHERE login = '$log' AND idbuyer<>$idbuyer AND idcontact<>".$idcontact."";
		$rs = $bdd->Execute ($Query);
		
		
		if($rs->RecordCount()){
			return false;
		}else{
			return true;
		}		
	}
	
	//------------------------------------------------------------------------------------------------------------
 	
}

?>