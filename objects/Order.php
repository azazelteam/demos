<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entêtes commandes clients
 */
 

include_once( dirname( __FILE__ ) . "/OnCharterSale.php" );
include_once( dirname( __FILE__ ) . "/OrderItem.php" );
include_once( dirname( __FILE__ ) . "/InvoiceAddress.php" );
include_once( dirname( __FILE__ ) . "/ForwardingAddress.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );


/**
 * Commande client
 */ 
final class Order extends OnCharterSale{
	
	//----------------------------------------------------------------------------
	
	public static $STATUS_ORDERED 				= "Ordered"; 			//commande validée par le commercial @todo : DROP, inutile
	public static $STATUS_REFUSED 				= "Refused"; 			//commande refusée
	public static $STATUS_CANCELLED 			= "Cancelled"; 			//commande annulée
	public static $STATUS_SENT 					= "Send"; 				//commande envoyée par mail ou fax @todo : DROP, inutile
	public static $STATUS_DEMAND_IN_PROGRESS 	= "DemandInProgress"; 	//demande de confirmation en attente @todo : DROP, inutile
	public static $STATUS_TODO 					= "ToDo"; 				//commande à traiter par le commercial
	public static $STATUS_IN_PROGRESS 			= "InProgress"; 		//commande prise en charge par un commercial ( ?? )
	public static $STATUS_PAID 					= "Paid"; 				//commande payée 	@todo : DROP, inutile
	public static $STATUS_TO_PAY				= "ToPay"; 				//commande à payer 	@todo : DROP, inutile
	public static $STATUS_INVOICED 				= "Invoiced"; 			//commande facturée @todo : DROP, inutile
	public static $STATUS_PICKING 				= "picking"; 			//commande en préparation
	public static $STATUS_DELIVERED 			= "delivered"; 			//marchandise expédiée
	
	//----------------------------------------------------------------------------
    
    //@todo PHP 5.3 late state binding( protect static $TABLE_NAME, ... )
	protected $TABLE_NAME 			= "order";
	protected $ITEMS_TABLE_NAME		= "order_row";
	protected $PRIMARY_KEY_NAME 	= "idorder";
	
	//----------------------------------------------------------------------------
	/**
	 * @var int $idorder le numéro de la commande
	 * @access private
	 */
	private $idorder;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé la commande dont le numéro est donné
	 * Pour créer une nouvelle commande vierge, utilisez Order::createOrder( int $idbuyer, $idcontact = 0 )
	 * @param int $idorder le numéro de la commande
	 */
	function __construct( $idorder ){
		
		//@todo PHP 5.3 late state binding( protect static $TABLE_NAME, ... )
		$this->TABLE_NAME 			= "order";
		$this->ITEMS_TABLE_NAME 	= "order_row";
		$this->PRIMARY_KEY_NAME 	= "idorder";
		
		$this->idorder =  $idorder ;
		
		DBObject::__construct( "order", false, "idorder",  $idorder );	
		
		$this->setPriceStrategy();
		$this->setChargesStrategy();
		
		$this->setItems();	
		
		$this->salesman = new Salesman( $this->recordSet[ "iduser" ] );	
		$this->customer = new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );	
		$this->forwardingAddress = $this->recordSet[ "iddelivery" ] ? new ForwardingAddress( $this->recordSet[ "iddelivery" ] ) : $this->customer->getAddress();
		$this->invoiceAddress = $this->recordSet[ "idbilling_adress" ] ? new InvoiceAddress( $this->recordSet[ "idbilling_adress" ] ) : $this->customer->getAddress();	

	}
	
	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return int
     */
    public function getId(){ return $this->idorder; }

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "TradeItem" );
		
		$rs =& DBUtil::query( "SELECT idrow FROM order_row WHERE idorder = '{$this->idorder}' ORDER BY idrow ASC" );

		while( !$rs->EOF() ){
		
			$this->items->add( new OrderItem( $this, $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}

	//----------------------------------------------------------------------------
	/**
	 * @param CArticle $article
	 * @param int $quantity
	 * @return void
	 */
	public function addArticle( CArticle $article, $quantity = 1 ){
		$item =& $this->createItem();
		
		/* attention aux produits spé et alternatifs */
		
		if( $article->get( "type" ) == "specific" )	
				$idcategory = ( $ret = DBUtil::getDBValue( "idcategory", "spe_categories", "idarticle", $article->getId() ) ) ? $ret : 0;
		else 	$idcategory = $article->get( "idproduct" ) ? DBUtil::getDBValue( "idcategory", "product", "idproduct", $article->get( "idproduct" ) ) : 0;
		
		$ecotaxe_code =  $article->get( "ecotaxe_code" );
		$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
		$ecotaxe_amount = $rs1->fields( "amount" ) * intval( $quantity );
			
		$idbuyer = $this->get('idbuyer');
		include_once( dirname( __FILE__ ) . "/GroupingCatalog.php" );
		$idgrouing=GroupingCatalog::getBuyerGrouping($idbuyer);
		$date=date("Y-m-d");
        $group_price=0.00;

       $rs1 = DBUtil::query( "SELECT price from  group_price
        						WHERE reference = '" . $article->get( "reference" )."' and idgrouping=".$idgrouing." 
        						 ");
       
        		if($rs1->RecordCount()>0)

        		{

        			$group_price=$rs1->fields("price");
        		}

	if( B2B_STRATEGY ){
			
			$unit_price=($group_price!=0.00)?$group_price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceET(), 2 );
		
		}
		else{
			$unit_price=($group_price!=0.00)?$group_price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceATI(), 2 );

			
		}



		$item->set( "idarticle", 		$article->getId() );
		$item->set( "quantity", 		intval( $quantity ) );
		$item->set( "reference", 		$article->get( "reference" ) );
		$item->set( "reference_base", 	$article->get( "reference" ) );
		$item->set( "ref_supplier", 	$article->get( "ref_supplier" ) );
		$item->set( "n_article", 		$article->get( "n_article" ) );
		$item->set( "gencod", 			$article->get( "gencod" ) );
		$item->set( "code_customhouse", $article->get( "code_customhouse" ) );
		$item->set( "ecotaxe_code", 	$article->get( "ecotaxe_code" ) );
		$item->set( "ecotaxe_amount", 	$ecotaxe_amount );
		$item->set( "idsupplier", 		$article->get( "idproduct" ) ? DBUtil::getDBValue( "idsupplier", "product", "idproduct", $article->get( "idproduct" ) ) : $article->get( "idsupplier" ) ); /* attention aux produits spé et alternatifs */
		$item->set( "unit", 			$article->get( "unit" ) );
		$item->set( "lot", 				$article->get( "lot" ) );
		//$item->set( "designation", 		"<b>" . Util::doNothing($article->get( "summary_1" )) ."</b><br />" . Util::checkEncoding(Util::doNothing($article->get( "designation_1" )) ));
		$item->set( "designation", 		"<b>" . $article->get( "summary_1" ) ."</b><br />" . $article->get( "designation_1" ) );
		$item->set( "summary", 			$article->get( "summary_1" ) );
		$item->set( "idcategory", 		$idcategory );
		$item->set( "delivdelay", 		$article->get( "delivdelay" ) );
		$item->set( "usepdf", 			1 );
		$item->set( "weight", 			$article->get( "weight" ) );
		$item->set( "idtissus", 		$article->getCloth() ? $article->getCloth()->getId() : 0 );
		$item->set( "idcolor", 			$article->getColor() ? $article->getColor()->getId() : 0 );
		$item->set( "min_cde", 			$article->get( "min_cde" ) );
		$item->set( "doc", 				$article->get( "doc" ) );
		$item->set( "hasPromo", 		$article->getPromotion() !== false ? 1 : 0 );
		$item->set( "isDestocking", 	$article->getDestocking() !== false ? 1 : 0 );
		$item->set( "vat_rate", 		$article->getVATRate() );
		$item->set( "franco", 			DBUtil::getDBValue( "franco", "product", "idproduct", $article->get( "idproduct" ) ) );
		
		/* quantité externe : bof */
	
		if( $article->get( "idsupplier" ) == DBUtil::getParameterAdmin( "internal_supplier" ) )	 	
			$item->set( "external_quantity", 0 );
		else if( $article->get( "stock_level" ) == -1 )
			$item->set( "external_quantity", intval( $quantity ) );
		else if( $article->get( "stock_level" ) < intval( $quantity ) )
			$item->set( "external_quantity", intval( $quantity ) - $article->get( "stock_level" ) );
		else $item->set( "external_quantity", 0 );
	
		/* prix de vente */
		
		if( B2B_STRATEGY ){
			
			$item->set( "unit_price", 			$unit_price );
			$item->set( "discount_price", 		round( $article->getDiscountPriceET( $this->customer->getId(), $item->get( "quantity" ) ), 2 ) );
			
		}
		else{
			
			$item->set( "unit_price", 			$unit_price);
			$item->set( "discount_price", 		round( $article->getDiscountPriceATI( $this->customer->getId(), $item->get( "quantity" ) ), 2 ) );
			
		}
		
		$item->set( "ref_discount",  		$article->getDiscountRate( $this->customer->getId(), $item->get( "quantity" ) ) );
		$item->set( "ref_discount_base", 	$article->getDiscountRate( $this->customer->getId(), $item->get( "quantity" ) ) );

		/* prix d'achat */
		
		$item->set( "unit_cost", 			round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "unit_cost_amount", 	round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "unit_cost_rate", 		0.0 );
		
		$item->set( "buyingcost", 			$article->get( "buyingcost" ) );
		$item->set( "suppliercost", 		$article->get( "suppliercost" ) );

		$item->set( "quantity_per_linear_meter", $article->get( "quantity_per_linear_meter" ) );
		$item->set( "quantity_per_truck", 		$article->get( "quantity_per_truck" ) );
		$item->set( "quantity_per_pallet", 		$article->get( "quantity_per_pallet" ) );
	
		$this->save();
		
	}
	//----------------------------------------------------------------------------
	/**
	 * @param CArticle $article
	 * @param int $quantity
	 * @return void
	 */
	public function addArticle2( CArticle $article, $quantity = 1 ){
		$item =& $this->createItem2();
		
		/* attention aux produits spé et alternatifs */
		
		if( $article->get( "type" ) == "specific" )	
				$idcategory = ( $ret = DBUtil::getDBValue( "idcategory", "spe_categories", "idarticle", $article->getId() ) ) ? $ret : 0;
		else 	$idcategory = $article->get( "idproduct" ) ? DBUtil::getDBValue( "idcategory", "product", "idproduct", $article->get( "idproduct" ) ) : 0;
		
		$ecotaxe_code =  $article->get( "ecotaxe_code" );
		$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
		$ecotaxe_amount = $rs1->fields( "amount" ) * intval( $quantity );
			
		$idbuyer = $this->get('idbuyer');
		include_once( dirname( __FILE__ ) . "/GroupingCatalog.php" );
		$idgrouing=GroupingCatalog::getBuyerGrouping($idbuyer);
		$date=date("Y-m-d");
        $group_price=0.00;

       $rs1 = DBUtil::query( "SELECT price from  group_price
        						WHERE reference = '" . $article->get( "reference" )."' and idgrouping=".$idgrouing." 
        						 ");
       
        		if($rs1->RecordCount()>0)

        		{

        			$group_price=$rs1->fields("price");
        		}

	if( B2B_STRATEGY ){
			
			$unit_price=($group_price!=0.00)?$group_price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceET(), 2 );
		
		}
		else{
			$unit_price=($group_price!=0.00)?$group_price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceATI(), 2 );

			
		}



		$item->set( "idarticle", 		$article->getId() );
		$item->set( "quantity", 		intval( $quantity ) );
		$item->set( "reference", 		$article->get( "reference" ) );
		$item->set( "reference_base", 	$article->get( "reference" ) );
		$item->set( "ref_supplier", 	$article->get( "ref_supplier" ) );
		$item->set( "n_article", 		$article->get( "n_article" ) );
		$item->set( "gencod", 			$article->get( "gencod" ) );
		$item->set( "code_customhouse", $article->get( "code_customhouse" ) );
		$item->set( "ecotaxe_code", 	$article->get( "ecotaxe_code" ) );
		$item->set( "ecotaxe_amount", 	$ecotaxe_amount );
		$item->set( "idsupplier", 		$article->get( "idproduct" ) ? DBUtil::getDBValue( "idsupplier", "product", "idproduct", $article->get( "idproduct" ) ) : $article->get( "idsupplier" ) ); /* attention aux produits spé et alternatifs */
		$item->set( "unit", 			$article->get( "unit" ) );
		$item->set( "lot", 				$article->get( "lot" ) );
		//$item->set( "designation", 		"<b>" . Util::doNothing($article->get( "summary_1" )) ."</b><br />" . Util::checkEncoding(Util::doNothing($article->get( "designation_1" )) ));
		$item->set( "designation", 		"<b>" . $article->get( "summary_1" ) ."</b><br />" . $article->get( "designation_1" ) );
		$item->set( "summary", 			$article->get( "summary_1" ) );
		$item->set( "idcategory", 		$idcategory );
		$item->set( "delivdelay", 		$article->get( "delivdelay" ) );
		$item->set( "usepdf", 			1 );
		$item->set( "weight", 			$article->get( "weight" ) );
		$item->set( "idtissus", 		$article->getCloth() ? $article->getCloth()->getId() : 0 );
		$item->set( "idcolor", 			$article->getColor() ? $article->getColor()->getId() : 0 );
		$item->set( "min_cde", 			$article->get( "min_cde" ) );
		$item->set( "doc", 				$article->get( "doc" ) );
		$item->set( "hasPromo", 		$article->getPromotion() !== false ? 1 : 0 );
		$item->set( "isDestocking", 	$article->getDestocking() !== false ? 1 : 0 );
		$item->set( "vat_rate", 		$article->getVATRate() );
		$item->set( "franco", 			DBUtil::getDBValue( "franco", "product", "idproduct", $article->get( "idproduct" ) ) );
		
		/* quantité externe : bof */
	
		if( $article->get( "idsupplier" ) == DBUtil::getParameterAdmin( "internal_supplier" ) )	 	
			$item->set( "external_quantity", 0 );
		else if( $article->get( "stock_level" ) == -1 )
			$item->set( "external_quantity", intval( $quantity ) );
		else if( $article->get( "stock_level" ) < intval( $quantity ) )
			$item->set( "external_quantity", intval( $quantity ) - $article->get( "stock_level" ) );
		else $item->set( "external_quantity", 0 );
	
		/* prix de vente */
		
		if( B2B_STRATEGY ){
			
			$item->set( "unit_price", 			$unit_price );
			$item->set( "discount_price", 		round( $article->getDiscountPriceET( $this->customer->getId(), $item->get( "quantity" ) ), 2 ) );
			
		}
		else{
			
			$item->set( "unit_price", 			$unit_price);
			$item->set( "discount_price", 		round( $article->getDiscountPriceATI( $this->customer->getId(), $item->get( "quantity" ) ), 2 ) );
			
		}
		
		$item->set( "ref_discount",  		$article->getDiscountRate( $this->customer->getId(), $item->get( "quantity" ) ) );
		$item->set( "ref_discount_base", 	$article->getDiscountRate( $this->customer->getId(), $item->get( "quantity" ) ) );

		/* prix d'achat */
		
		$item->set( "unit_cost", 			round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "unit_cost_amount", 	round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "unit_cost_rate", 		0.0 );
		
		$item->set( "buyingcost", 			$article->get( "buyingcost" ) );
		$item->set( "suppliercost", 		$article->get( "suppliercost" ) );

		$item->set( "quantity_per_linear_meter", $article->get( "quantity_per_linear_meter" ) );
		$item->set( "quantity_per_truck", 		$article->get( "quantity_per_truck" ) );
		$item->set( "quantity_per_pallet", 		$article->get( "quantity_per_pallet" ) );
		
	}
    public function addArticlevoucher( ){
		
		$item =& $this->createItem();
		$quantity = 1 ; $reduce = 0.00;
		if( isset( $_SESSION[ "voucher" ] ) ){
			$code = urldecode($_SESSION[ "voucher" ]);
			if($this->setUseVoucher( $code )){
				$reduce = -($this->Getreduction( stripslashes( $code  ) ));
			}
		}
		//print $this->reduce;exit;
		//$code = $_SESSION[ "voucher" ];
		$req = "SELECT reference FROM discount_voucher WHERE code = '" .  stripslashes( $_SESSION[ "voucher" ] )."' ";
		$res = DBUtil::query($req);
		//print $_SESSION[ "voucher" ];exit;
		$reference = $res->fields("reference");
		
		$reqs = "SELECT * FROM detail WHERE reference = '".$reference."' ";
		$ress = DBUtil::query($reqs);
		$idarticle = $ress->fields("idarticle");
		$reference = $ress->fields("reference");
		$idproduct = $ress->fields("idproduct");
		$summary = $ress->fields("summary_1");
		$sellingcost = $ress->fields("sellingcost");
		$idsupplier = $ress->fields("idsupplier");
	
		$item->set( "idarticle", 		$idarticle );
		$item->set( "quantity", 		1 );
		$item->set( "reference", 		$reference );
		$item->set( "reference_base", 	$reference );
		
		$item->set( "idsupplier", 		$idsupplier ); /* attention aux produits spé et alternatifs */
	
		$item->set( "designation", 		$summary );
		$item->set( "summary", 			$summary );
	
		$item->set( "usepdf", 			1 );
		$item->set( "vat_rate", 			$this->getVATRate() );
		/* prix de vente */
		
		$item->set( "unit_price", 			floatval($reduce) );
		$item->set( "discount_price", 		floatval($reduce) );
			
		/* prix d'achat */
		
		$item->set( "unit_cost", 			floatval($reduce));
		$item->set( "unit_cost_amount", 	floatval($reduce) );
		$item->set( "unit_cost_rate", 		0.0 );
	
		$this->save();
		
	}
	//----------------------------------------------------------------------------
	/**
     * @access protected
     * @return OrderItem une ligne article éditable
     */
	protected function &createItem(){
		
		$idrow = $this->items->size() + 1;
		
		DBUtil::query( "INSERT INTO order_row( idorder, idrow ) VALUES( '{$this->idorder}', '$idrow' )" );

		$this->items->add( new OrderItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}
	//----------------------------------------------------------------------------
	/**
     * @access protected
     * @return OrderItem une ligne article éditable
     */
	protected function &createItem2(){
		
		$idrow = $this->items->size() + 1;
		
		//DBUtil::query( "INSERT INTO order_row( idorder, idrow ) VALUES( '{$this->idorder}', '$idrow' )" );

		$this->items->add( new OrderItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}

	//----------------------------------------------------------------------------
	/**
	 * @param Customer $customer
	 * @return void
	 */
	public function setCustomer( Customer $customer ){
	
		$this->recordSet[ "idbuyer" ] 			= $customer->getId();
		$this->recordSet[ "idcontact" ] 		= $customer->getContact()->getId();
		$this->recordSet[ "iddelivery" ] 		= 0;
		$this->recordSet[ "idbilling_adress" ] = 0;
		$this->recordSet[ "factor" ] 			= DBUtil::getDBValue( "factor", "buyer", "idbuyer", $customer->getId() );
		
		$this->forwardingAddress 			= $customer->getAddress();
		$this->invoiceAddress 				= $customer->getAddress();
		$this->customer 					= $customer;

		//recalcul des montants
		
		$this->recalculateAmounts();

	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute une ligne article du devis à la commande
	 * @param EstimateItem $estimateItem la ligne article du devis
	 * @return void
	 */
	public function addEstimateItem( EstimateItem &$estimateItem ){

		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$this->addArticle( new CArticle(
		
				$estimateItem->get( "idarticle" ),
				$estimateItem->get( "idcolor" ),
				$estimateItem->get( "idtissus" )
				
			), $estimateItem->get( "quantity" )
			
		);
		
		$orderItem = $this->items->get( $this->items->size() - 1 );
		
		$orderItem->set( "unit_price", 				$estimateItem->get( "unit_price" ) );
		$orderItem->set( "idrow_line", 				$estimateItem->get( "idrow_line" ) );
		$orderItem->set( "n_article", 				$estimateItem->get( "n_article" ) );
		$orderItem->set( "designation", 			$estimateItem->get( "designation" ) );
		$orderItem->set( "summary", 				$estimateItem->get( "summary" ) );
		$orderItem->set( "unit_cost", 				$estimateItem->get( "unit_cost" ) );
		$orderItem->set( "unit_cost_rate", 			$estimateItem->get( "unit_cost_rate" ) );
		$orderItem->set( "unit_cost_amount", 		$estimateItem->get( "unit_cost_amount" ) );
		$orderItem->set( "ref_discount", 			$estimateItem->get( "ref_discount" ) );
		$orderItem->set( "ref_discount_base", 		$estimateItem->get( "ref_discount_base" ) );
		$orderItem->set( "ref_discount_add", 		$estimateItem->get( "ref_discount_add" ) );
		$orderItem->set( "discount_price", 			$estimateItem->get( "discount_price" ) );
		$orderItem->set( "delivdelay", 				$estimateItem->get( "delivdelay" ) );
		$orderItem->set( "alternative", 			$estimateItem->get( "alternative" ) );
		$orderItem->set( "doc", 					$estimateItem->get( "doc" ) );
		$orderItem->set( "usepdf", 					$estimateItem->get( "usepdf" ) );
		$orderItem->set( "external_quantity", 		$estimateItem->get( "external_quantity" ) );
		$orderItem->set( "hasPromo", 				$estimateItem->get( "hasPromo" ) );
		$orderItem->set( "isDestocking", 			$estimateItem->get( "isDestocking" ) );
		$orderItem->set( "useimg", 					$estimateItem->get( "useimg" ) );
		$orderItem->set( "franco", 					$estimateItem->get( "franco" ) );
		
		$this->recalculateAmounts();
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Substitue une ligne article par une autre ligne article
	 * Méthode à modifier également dans Estimate.php
	 * @param int $idrow la ligne article à substituer
	 * @param int $&IdRow la ligne article utilisée pour la substitution
	 * @param bool $deleteSubstitute booléen, supprime la ligne article utilisée pour la substitution si true, sinon la ligne article est conservée
	 */
	function substituteRowByRow( $idrow, $substituteIdRow, $deleteSubstitute = true ){
		
		$this->getItemAt( $idrow)->set("ref_replace", 			$this->getItemAt( $substituteIdRow)->get( "reference" ) );
		$this->getItemAt( $idrow)->set("ref_base_replace", 		$this->getItemAt( $substituteIdRow)->get( "reference_base" ) );
		$this->getItemAt( $idrow)->set("ref_supplier_replace", 	$this->getItemAt( $substituteIdRow)->get( "ref_supplier" ) );
		$this->getItemAt( $idrow)->set("alternative_replace", 	$this->getItemAt( $substituteIdRow)->get( "alternative" ) );
			
		$substituableFields = array(
		
			"unit_price",
			"idsupplier",
			"ref_supplier",
			"unit_cost",
			"unit_cost_rate",
			"unit_cost_amount",
			"ref_discount",
			"discount_price",
			"delivdelay",
			"vat_rate",
			"weight",
			"idtissus",
			"idcolor",
			"min_cde",
			"stock",
			"hasPromo",
			"isDestocking",
			"suppliercost",
			"buyingcost"

		);
		
		foreach( $substituableFields as $substituableField )
			$this->getItemAt( $idrow)->set( $substituableField, $this->getItemAt( $substituteIdRow)->get( $substituableField ) );
			
		if( $deleteSubstitute )
			$this->removeItemAt( $substituteIdRow );
			
		$this->recalculateAmounts();

	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Substitue une ligne article par une autre référence
	 * Méthode à modifier également dans Estimate.php
	 * @param int $idrow la ligne article à substituer
	 * @param string $reference la référence du substitut
	 * @return void
	 */
	public function substituteRowByReference( $idrow, $reference ){
		
		$quantity 			= $this->getItemAt( $idrow)->get( "quantity" );
		$substituteIdRow 	= $this->items->size();
		
		//référence catalogue
		
		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$this->addArticle( new CArticle( DBUtil::getDBValue( "idarticle", "detail", "reference", $reference ) ), $quantity );
		
		$this->substituteRowByRow( $idrow, $substituteIdRow, true );

	}
	
	//----------------------------------------------------------------------------
	/**
	 * fonction de destockage
	 * @deprecated
	 * @todo décrémentation du stock lors de la validation ( méthode à créer )
	 * @todo méthode de validation pour la commande, ne pas modifier bêtement le statut, il faut aussi gérer le stock, le montant d'achat autorisé pour le client, etc...
	 * @todo access private : ne pas manipuler le stock manuellement, il faudrait simplement valider la commande
	 */
	public function unstock(){
		
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();	
			
			$Stock = DBUtil::getDBValue( "stock_level", "detail", "reference", $item->get( "reference_base" ) );

			if( $Stock != -1 && $Stock - $item->get( "quantity" ) >= 0 ){
					
					$Stock -= $item->get( "quantity" );
					DBUtil::query( "UPDATE detail SET stock_level = '$Stock' WHERE reference LIKE " . DBUtil::quote( $item->get( "reference_base" ) ) . " LIMIT 1" );
				
			}
			else return false;
			
		}
		
		return true;
		
	}

	 //----------------------------------------------------------------------------
	 
	 /**
	 * Récupère la liste des BL pour une commande
	 * @deprecated
	 * @static
	 * @param int $idorder
	 * @return array un tableau indicé contenant tous les numéros de BL
	 */
	public static function getBLs( $idorder ){
		
		$query = "
		SELECT bl_delivery.idbl_delivery
		FROM bl_delivery, order_supplier
		WHERE order_supplier.idorder = '$idorder'
		AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier";
	
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les BL de la commande '$idorder'" );
		
		$bls = array();
			
		while( !$rs->EOF() ){
			
			$bls[] = $rs->fields( "idbl_delivery" );
			
			$rs->MoveNext();
			
		}
		
		return $bls;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * @deprecated
	 * Récupère la liste des factures pour une commande
	 * @static
	 * @param int $idorder
	 * @return array un tableau indicé contenant tous les numéros de facture
	 */
	public static function getInvoices( $idorder ){
		// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "
		SELECT idbilling_buyer
		FROM billing_buyer
		WHERE idorder = '$idorder'";

		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les factures de la commande '$idorder'" );
		
		$invoices = array();
			
		while( !$rs->EOF() ){
			
			$invoices[] = $rs->fields( "idbilling_buyer" );
			
			$rs->MoveNext();
			
		}
		
		return $invoices;
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Supprime une commande de la base de données
	 * @static
	 * @param int $idorder le numéro de la commande à supprimer
	 * @return bool true en cas de succès, sinon false
	 */
	public static function delete( $idorder ){
		
		if( DBUtil::query( "SELECT 1 FROM bl_delivery WHERE idorder = '" .  $idorder  . "' AND idbilling_buyer <> 0 LIMIT 1" )->RecordCount() )
			return false;
			
		DBUtil::query( "DELETE FROM `order_charges` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `order_row` WHERE `idorder` = '" .  $idorder . "'" );
		DBUtil::query( "DELETE FROM `order` WHERE `idorder` = '" .  $idorder  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM `contact_send` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `exchanges` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `gift_token` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `litigation_history` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `order_comments` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `vouchers_order` WHERE `idorder` = '" .  $idorder  . "'" );
		DBUtil::query( "DELETE FROM `order_row_product_document` WHERE `idorder` = '" .  $idestimate  . "'" );

		return true;
		
	}

	//------------------------------------------------------------------------------------------------------------
	/**
	 * Annule la commande 
	 * Le stock interne est recrédité et les commandes fournisseur supprimées
	 * @return void
	 */
	public function cancel(){

		$this->set( "status", Order::$STATUS_CANCELLED );
		$this->save();

		//recréditer le stock interne
		
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			
			$idarticle 			= $item->get( "idarticle" );
			$quantity 			= $item->get( "quantity" );
			$reference_base 	= $item->get( "reference_base" );
			$external_quantity 	= $item->get( "external_quantity" );
			$internal_quantity 	= $quantity - $external_quantity;
			$stock_level 		= DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
			
			if( $internal_quantity > 0 && $stock_level != -1 ){
		
				$stock_level += $internal_quantity;
				
				$query = "
				UPDATE detail 
				SET stock_level = '$stock_level' 
				WHERE idarticle = '$idarticle' 
				LIMIT 1";

				DBUtil::query( $query );
				
			}
			
		}
		
	}

	//------------------------------------------------------------------------------------------------------------
	/**
	 * Récupère les numéros de colis de la commande
	 * @param int le numéro de commande
	 * @return array un tableau contenant les numéros de colis
	 */
	public static function getOrderPackages( $idorder ){
	// ------- Mise à jour de l'id gestion des Index  #1161	
		$query = "
		SELECT DISTINCT(package)
		FROM `bl_delivery`
		WHERE idorder='$idorder'
		AND package <> 0";
	//-----	
		$rs =& DBUtil::query($query);
		
		if( $rs===false || !$rs->Recordcount() )
			return false;
			
		$packages = array();
		
		for($i=0;$i<$rs->Recordcount();$i++){
			
			$pack = $rs->fields("package");
			
			if($pack==0)
				$pack="";
				
			$packages[] = $pack;
			$rs->MoveNext();	
		
		}
		
		return $packages;
			
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le chèque cadeau utilisé pour passer la commande s'il existe, sinon false
	 * @return GiftToken
	 */
	public function getGiftToken(){

		$rs =& DBUtil::query( "SELECT idtoken FROM gift_token WHERE idorder = '" . $this->getId() . "' LIMIT 1" );
	
		if( !$rs->RecordCount() )
			return false;

		include_once( dirname( __FILE__ ) . "/GiftToken.php" );
		
		return new GiftToken( $rs->fields( "idtoken" ) );

	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * Assurance crédit
	 * @access public
	 * @param bool $insure
	 * @return bool true si la commande a pû être assurée, sinon false 
	 */
	public function insure( $insure = true ){
		
		$this->set( "insurance_auto", 0 );
		
		/* désactivation */
		
		if( !$insure ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* assurance crédit non modifiable */
		
		if( !in_array( $this->recordSet[ "status" ], array(
			
			Order::$STATUS_DEMAND_IN_PROGRESS,
			Order::$STATUS_IN_PROGRESS, 
			Order::$STATUS_TO_PAY,
			Order::$STATUS_TODO
	
		) ) )
			return $this->recordSet[ "insurance" ] == 1;
		
		/* pas d'assurance crédit pour les paiements comptant */
						
		if( $this->recordSet[ "cash_payment" ] ){
			
			$this->set( "insurance", 0 );
			
			return false;
				
		}

		$idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) );
	
		/* assurance crédit désactivée */
		
		if( !$idcredit_insurance ){
			
			$this->set( "insurance", 0 );
			
			return false;
				
		}
		
		$creditInsurance	= ( object )DBUtil::query( "SELECT * FROM credit_insurance WHERE idcredit_insurance = '$idcredit_insurance' LIMIT 1" )->fields;
		$customer 			= new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );
		$insurance 			= $this->recordSet[ "insurance" ];
		$minimum 			= floatval( $creditInsurance->minimum );
		$maximum 			= floatval( $creditInsurance->maximum );
		$netBill 			= $this->getNetBill();
		$netBillET 			= round( $netBill / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();
		
		/* client réfusé ou non assuré */
	
		if( $customer->get( "insurance_status" ) != "Accepted" || !$customer->get( "insurance" ) ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* montant non couvert */
	
		if( $netBillET < $minimum || $netBillET > $maximum ){
		
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* encours maximum atteint */
	
		if( $outstandingCredits + $netBillET > $customer->get( "insurance_amount" ) ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
	
		/* commande assurable */
		
		$this->set( "insurance", 1 );
		
		return true;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * Factor
	 * @access public
	 * @param bool $factor
	 * @return bool true si la commande a pû être factorisée, sinon false 
	 */
	public function factorize( $factor = true ){
		
		/* désactivation */
		
		if( !$factor ){
			
			$this->set( "factor", 0 );
			
			return false;
			
		}
		
		/* pas de factor pour les paiements comptant */
		
		if( $this->recordSet[ "cash_payment" ] ){
			
			$this->set( "factor", 0 );
			
			return false;
			
		}
		
		/* pas de factor pour les paiements à la commande */
		
		$idpayment_delay = DBUtil::getDBValue( "idpayment_delay", "payment_delay", "payment_code", "CD" );
		
		if( $this->recordSet[ "idpayment_delay" ] == $idpayment_delay ){
			
			$this->set( "factor", 0 );
			
			return false;
				
		}
		
		/* pas de factor pour les paiements à la réception de facture */
		
		$idpayment_delay = DBUtil::getDBValue( "idpayment_delay", "payment_delay", "payment_code", "RF" );
		
		if( $this->recordSet[ "idpayment_delay" ] == $idpayment_delay ){
			
			$this->set( "factor", 0 );
			
			return false;
				
		}

		$isfactor = DBUtil::getParameterAdmin( "ad_factor1" );
	
		/* factor désactivée */
		
		if( empty( $isfactor ) ){
			
			$this->set( "factor", 0 );
			
			return false;
				
		}
		
		/* commande non assurée */
		
		if( $this->recordSet[ "insurance" ] ){
			
			$this->set( "factor", 0 );
			
			return false;
			
		}
		
		$minimum 			= floatval( DBUtil::getParameterAdmin( "factor_minbuying" ) );
		$maximum 			= floatval( DBUtil::getParameterAdmin( "factor_maxbuying" ) );
		$netBill 			= $this->getNetBill();
		
		/* montant non couvert */
		
		if( $netBill < $minimum || ( $netBill > $maximum && $maximum > 0 ) ){
			
			$this->set( "factor", 0 );
			
			return false;
			
		}
		
		/* pays = France */
		
		$idstateDefault = DBUtil::getParameter( "default_idstate" );
		$idstateOrder = $this->invoiceAddress->getIdState();
		
		if( $idstateDefault != $idstateOrder ){
			
			$this->set( "factor", 0 );
			
			return false;
			
		}
		
		/* profil non particulier */
		
		$customer = new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );
		
		if( $customer->get( "catalog_right" ) <= 1 ){
			
			$this->set( "factor", 0 );
			
			return false;
			
		}
		
		/* commande assurable */
		
		$this->set( "factor", 1 );
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Reste à payer
	 * @return float
	 */
	public function getBalanceOutstanding() {

		/* règlements déjà enregistrés */
		
		$query = "
		SELECT SUM( brb.amount ) AS paid
		FROM billing_regulations_buyer brb,	regulations_buyer rb, billing_buyer bb, bl_delivery bl
		WHERE bl.idorder = '" . $this->getId() . "'
		AND bl.idbilling_buyer = brb.idbilling_buyer
		AND brb.idregulations_buyer = rb.idregulations_buyer
		AND brb.idbilling_buyer = bb.idbilling_buyer
		AND rb.from_down_payment = 0";

		$rs =& DBUtil::query($query);

		$paid = $rs->RecordCount() ? $rs->fields( "paid" ) : 0.0;
		
		return $this->getNetBill() - $paid;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Duplique la commande
	 * @access public
	 * @return Order
	 */
	 	/**
	
	 */
	public function addOrderItem( OrderItem &$orderItem ){

		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$this->addArticle( new CArticle(
		
				$orderItem->get( "idarticle" ),
				$orderItem->get( "idcolor" ),
				$orderItem->get( "idtissus" )
				
			), $orderItem->get( "quantity" )
			
		);
		
		$ordItem = $this->items->get( $this->items->size() - 1 );
		
		$ordItem->set( "unit_price", 				$orderItem->get( "unit_price" ) );
		$ordItem->set( "n_article", 				$orderItem->get( "n_article" ) );
		$ordItem->set( "designation", 			$orderItem->get( "designation" ) );
		$ordItem->set( "summary", 				$orderItem->get( "summary" ) );
		$ordItem->set( "unit_cost", 				$orderItem->get( "unit_cost" ) );
		$ordItem->set( "unit_cost_rate", 			$orderItem->get( "unit_cost_rate" ) );
		$ordItem->set( "unit_cost_amount", 		$orderItem->get( "unit_cost_amount" ) );
		$ordItem->set( "ref_discount", 			$orderItem->get( "ref_discount" ) );
		$ordItem->set( "ref_discount_base", 		$orderItem->get( "ref_discount_base" ) );
		$ordItem->set( "ref_discount_add", 		$orderItem->get( "ref_discount_add" ) );
		$ordItem->set( "discount_price", 			$orderItem->get( "discount_price" ) );
		$ordItem->set( "delivdelay", 				$orderItem->get( "delivdelay" ) );
		$ordItem->set( "alternative", 			$orderItem->get( "alternative" ) );
		$ordItem->set( "doc", 					$orderItem->get( "doc" ) );
		$ordItem->set( "usepdf", 					$orderItem->get( "usepdf" ) );
		$ordItem->set( "external_quantity", 		$orderItem->get( "external_quantity" ) );
		$ordItem->set( "hasPromo", 				$orderItem->get( "hasPromo" ) );
		$ordItem->set( "isDestocking", 			$orderItem->get( "isDestocking" ) );
		$ordItem->set( "useimg", 					$orderItem->get( "useimg" ) );
		$ordItem->set( "franco", 					$orderItem->get( "franco" ) );
		
		$this->recalculateAmounts();
		
	}
	public function duplicate(){

		/* création */
		
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
		
		$order = TradeFactory::createOrder( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );

		$order->setForwardingAddress( $this->getForwardingAddress() );
		$order->setInvoiceAddress( $this->getInvoiceAddress() );

		$order->set( "status", Order::$STATUS_TODO );
		
		/* propriétés dupliquées */
		
		$fields = array(
		
			"payment_idpayment",
			"idpayment",
			"idpayment_delay",
			"deliv_payment",
			"no_tax_export",
			"charge_vat_rate",
			"charge_vat",
			"billing_rate",
			"billing_amount",
			"installment_rate",
			"installment_amount",
			"delivery_delay",
			"total_charge_auto",
			"charge_auto",
			"global_delay",
			"charge_vat_rate", 
			"charge_vat", 
			"total_charge", 
			"total_charge_ht"
			
		);

		foreach( $fields as $field )
			$order->set( $field, $this->recordSet[ $field ] );
			
		/* lignes articles */
	
		
		$idrow = 0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() )
			$order->addOrderItem( $it->next()->duplicate( $order, ++$idrow ) );

		$order->save();
	
		return $order;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Utiliser un bon de réduction
	 * @access public
	 * @return void
	 */
	/*public function setUseVoucher( Voucher &$voucher ){
		
		/* bon de réduction valide */
		
	/*	if( !Voucher::isAvailable( $voucher->get( "code" ) ) )
			return;
		
		/* montant minimum de commande atteint */
				
	/*	if( $voucher->get( "min_order_amount" ) ){
		
			if( !B2B_STRATEGY ){
				
				$order_amount = 0.0;
				$it = $this->items->iterator();
				while( $it->hasNext() )
					$order_amount += $it->next()->getTotalET();

			}
			else{
				
				$order_amount = 0.0;
				$it = $this->items->iterator();
				while( $it->hasNext() )
					$order_amount += $it->next()->getTotalATI();
		
			}
			
			if( $voucher->get( "min_order_amount" ) > $order_amount )
				return;
		
		}
		
		/* articles qui sont dans une catégorie ou une marque donnant droit à une réduction */

/*		$items = array();
		$it = $this->items->iterator();
		while( $it->hasNext() ){
			
			$item =& $it->next();
	
			if( $item->get( "idarticle" ) ){ //exclure les services, chèques-cadeaux, etc...
				
				$idproduct 	= DBUtil::query( "SELECT idproduct FROM detail WHERE idarticle = '" . $item->get( "idarticle" ) . "' LIMIT 1" )->fields( "idproduct" ); //@todo idproduct existe dans BasketItem et non dans OrderItem.
				$idcategory = DBUtil::query( "SELECT idcategory FROM product WHERE idproduct = '$idproduct' LIMIT 1" )->fields( "idcategory" );
				$idbrand 	= DBUtil::query( "SELECT idbrand FROM product WHERE idproduct = '$idproduct' LIMIT 1" )->fields( "idbrand" );
				
				$allowUsage  = !$voucher->get( "idbrand" ) && !$voucher->get( "idcategory" );
				$allowUsage |= !$voucher->get( "idbrand" ) && $voucher->get( "idcategory" ) == $idcategory;
				$allowUsage |= !$voucher->get( "idcategory" ) && $voucher->get( "idbrand" ) == $idbrand;
				$allowUsage |= $voucher->get( "idbrand" ) == $idbrand && $voucher->get( "idcategory" ) == $idcategory;
				
				if( $allowUsage )
					$items[] =& $item;

				/* vérifier si la catégorie donne droit à la réduction en fonction de la marque du produit */
				
	/*			$idcategorychild = $idcategory;
				while( !$allowUsage ){
					
					$rs =& DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategorychild' LIMIT 1" );
					
					if( $rs->fields( "idcategoryparent" ) == $voucher->get( "idcategory" ) 
						&& ( !$voucher->get( "idbrand" ) || $voucher->get( "idbrand" ) == $idbrand ) )
						$items[] =& $item;
						
					$allowUsage = $rs->fields( "idcategoryparent" ) == 0 || $rs->fields( "idcategoryparent" ) == $voucher->get( "idcategory" );
					
					$idcategorychild = $rs->fields( "idcategoryparent" );
					
				}
				
			}
			
		}
		
		if( !count( $items ) )
			return;
			
		/* appliquer la remise */
		
	/*	switch( $voucher->get( "type" ) ){
		
			//la remise de type "montant" s'effectue sur le T.T.C. en B2C et sur le H.T. en B2B
			
			case Voucher::$TYPE_AMOUNT :
	
				$discountableAmount = 0.0;
				$i = 0;
				while( $i < count( $items ) ){
				
					$item =& $items[ $i ];
					
					$price = $item->get( "quantity" ) * $item->get( "unit_price" );
					
					if( !B2B_STRATEGY )
						$price *= 1.0 + $item->get( "vat_rate" ) / 100.0;
						
					$discountableAmount += $price;
					$i++;

				}
				
				$discountRate = min( 100.0, $voucher->get( "value" ) * 100.0 / $discountableAmount );
				
				$i = 0;
				while( $i < count( $items ) ){
					
					$item =& $items[ $i ];
					
					if( $discountRate > $item->get( "ref_discount" ) ){ /* ne pas appliquer aux produits mieux remisés */
						
			/*			$item->set( "ref_discount", $discountRate );
						$item->set( "discount_price", $item->get( "unit_price" ) * ( 1.0 - $discountRate / 100.0 ) );
						
					}
					
					$i++;
					
				}
				
				break;
				
			case Voucher::$TYPE_RATE :
	
				$discountRate = $voucher->get( "value" );
				
				$i = 0;
				while( $i < count( $items ) ){
					
					$item =& $items[ $i ];
					
					if( $discountRate > $item->get( "ref_discount" ) ){ /* ne pas appliquer aux produits mieux remisés */
						
			/*			$item->set( "ref_discount", $discountRate );
						$item->set( "discount_price", $item->get( "unit_price" ) * ( 1.0 - $discountRate / 100.0 ) );
						
					}
				
					$i++;
					
				}
				
				break;
				
			default : trigger_error( "Type de bon de réduction inconnu", E_USER_ERROR );
				
		}
		
	}*/
	
	public function setUseVoucher(  $voucher ){
		

//--------- vérifier si le bon de réduction exist --------------//
	if( !DBUtil::query( "SELECT 1 FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) ) . " LIMIT 1" )->RecordCount() )
	return false;
	
	//-------------- vérifier les dates début et de fin -------------//
	$dates = date("Y-m-d");
	
	if( !DBUtil::query( 'SELECT 1 FROM discount_voucher WHERE  date_start < "'.$dates.'" AND date_end > "'.$dates.'" AND code = ' . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) ) . " LIMIT 1" )->RecordCount() )
	return false;
	
	
		//-------------------------vérifier si le bon de réduction n'est pas utiliser par le mme client -------------//
	
	if( isset( $_SESSION[ "idbuyer" ]))
$idbuyer = urldecode($_SESSION[ "idbuyer" ]);
	
	$req = "SELECT voucher_code FROM `order` WHERE voucher_code = ".DBUtil::quote( stripslashes( $voucher ) )." AND idbuyer = " . DBUtil::quote( stripslashes( $idbuyer ) )." ";
	$res = DBUtil::query($req);

//	$voucher_code = $res->fields("voucher_code");
	//$basketController->setData( "idbuyer"  ,$idbuyer );
	
if ( $res->RecordCount() > 0 ) {
	return false;
	}
	
	//---------------- vérifier si montant de la commande > amount_min --------------//

	  
	$req = "SELECT amount_min FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) )." ";
	$res = DBUtil::query($req);

	$amount_min = $res->fields("amount_min");
	
	if( $amount_min > 0 ){
			$order_amount = 0.0;
			//$it = $this->items->iterator();
			$it = Basket::getInstance()->getItems()->iterator();
			while( $it->hasNext() ){
				
				if( B2B_STRATEGY )
						$order_amount += $it->next()->getTotalET();
				else 	$order_amount += $it->next()->getTotalATI();
				
			}
	
			if( $amount_min > $order_amount )
			return false;
		
		}
			return true;
	}	
	
	public function Getreduction( $voucher ){
		$reduction = 0.0;
		$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) )." ";
		$ress = DBUtil::query($reqs);
		$idcategory = $ress->fields("idcategory");
	
		$idproduct = $ress->fields("idproduct");

		$type = $ress->fields("type");
			
		$reference = $ress->fields("reference");
			
		
		$reqs = "SELECT reference,sellingcost FROM detail WHERE reference = '".$reference."' ";
		$ress = DBUtil::query($reqs);
		$sellingcost = $ress->fields("sellingcost");
		
		$exist = 0; $exists = 0;
		$order_amount = 0.0;
		
		for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
			$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
			
			$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
			$rs = DBUtil::query($rq);
			$references = $rs->fields("reference");
			$dates = date("Y-m-d");
			
			$q = "SELECT reference FROM promo WHERE reference = '".$references."'  ";
			$rs = DBUtil::query($q);
	
			if ( !$rs->RecordCount() ) {	
				$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
				$results = DBUtil::query($qqs);

				$idcategorys = $results->fields("idcategory");
				$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
				$rs = DBUtil::query($q);

				$idcategoryparent = $rs->fields("idcategoryparent");
				
				if( B2B_STRATEGY )
					$amountht = Basket::getInstance()->getItemAt( $i)->getTotalET();
				else 	 
					$amountttc = Basket::getInstance()->getItemAt( $i)->getTotalATI(); 

				if( $idproduct > 0  ){
					if($idproduct ==  $idprod)
					{
						if($type == "rate"){
							if( B2B_STRATEGY )
									$order_amount = $amountht;
							else	
								$order_amount =  $amountttc;
						
							$reduction = (($order_amount * $sellingcost)/100);
						}
						if($type == "amount"){		
							$reduction =  $sellingcost;						
						}
	
					}	
 				}
				else  { 
				
					if( $idcategory > 0 ){
					
						if( $idcategory ==  $idcategoryparent ){
							if($type == "rate"){
								if( B2B_STRATEGY )
										$order_amount += $amountht;
								else	
									$order_amount +=  $amountttc;
								
								$reduction = (($order_amount * $sellingcost)/100);
							}
							if($type == "amount"){
								$reduction +=  $sellingcost;
							}
					 	}
				 	}
				 	else // modif
					{
						if($type == "rate"){
							if( B2B_STRATEGY )
								$order_amount += $amountht;
							else	
				 				$order_amount +=  $amountttc;
			 				$reduction = (($order_amount * $sellingcost)/100);
			
						}
						if($type == "amount"){
							if( B2B_STRATEGY )
								$reduction +=  $sellingcost ;		
							else	
							$reduction +=  $sellingcost / 1.196;
			
			
						}
					}// fin modif
				}
	
			}	
		}
		return $reduction;
	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#getChargesET()
	 */
	public function getChargesET(){
		if( in_array( $this->recordSet[ "status" ], array( 
		
				self::$STATUS_ORDERED, 
				self::$STATUS_REFUSED, 
				self::$STATUS_CANCELLED, 
				self::$STATUS_SENT,
				self::$STATUS_PAID,
				self::$STATUS_INVOICED,
				self::$STATUS_PICKING,
				self::$STATUS_DELIVERED
				
		) ) || !$this->recordSet[ "total_charge_auto" ] )
		return B2B_STRATEGY ? $this->recordSet[ "total_charge_ht" ] : round( $this->recordSet[ "total_charge" ] / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
				
		//return $this->chargesStrategy->getChargesET();
        return 0.0 ;
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#getChargesATI()
	 */
	public function getChargesATI(){

		if( in_array( $this->recordSet[ "status" ], array( 
		
				self::$STATUS_ORDERED, 
				self::$STATUS_REFUSED, 
				self::$STATUS_CANCELLED, 
				self::$STATUS_SENT,
				self::$STATUS_PAID,
				self::$STATUS_INVOICED,
				self::$STATUS_PICKING,
				self::$STATUS_DELIVERED
				
		) ) || !$this->recordSet[ "total_charge_auto" ] )
		return B2B_STRATEGY ? round( $this->recordSet[ "total_charge_ht" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 ) : $this->recordSet[ "total_charge" ];
			
		//return $this->chargesStrategy->getChargesATI();
		return 0.0 ;
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getNetMarginAmount()
	 */
	public function getNetMarginAmount(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getOrderNetMarginAmount( $this->idorder );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getNetMarginRate()
	 */
	public function getNetMarginRate(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getOrderNetMarginRate( $this->idorder );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getGrossMarginAmount()
	 */
	public function getGrossMarginAmount(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getOrderGrossMarginAmount( $this->idorder );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getGrossMarginRate()
	 */
	public function getGrossMarginRate(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getOrderGrossMarginRate( $this->idorder );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#recalculateAmounts()
	 */
	protected function recalculateAmounts(){
	$reduce = 0.00;
	include_once( dirname( __FILE__ ) . "/../objects/Basket.php" );
	if( isset( $_SESSION[ "voucher" ] ) ){
	$code = urldecode($_SESSION[ "voucher" ]);
	
	$reduce = $this->Getreduction( stripslashes( $code  ) );

	}
//print $this->reduce;exit;
//$netapayer = $this->getTotalATI();
		//$this->recordSet[ "total_amount" ] 			= $netapayer - $reduce;
	//	$this->recordSet[ "total_amount_ht" ] 		= $this->getTotalET();
		
//	}	else{


		//$this->recordSet[ "total_amount" ] 			= $this->getTotalATI();
		//$this->recordSet[ "total_amount_ht" ] 		= $this->getTotalET();
	
		//$this->recordSet[ "total_amount" ] = $this->recordSet[ "total_amount" ] - $reduce;
	//	}
		/*
		 * @todo : pour les champs ci-dessous, on garde arbitrairement le pourcentage et non le montant : 
		 * @todo RENAME `total_discount` `total_discount_rate`
		 */
		
		if( B2B_STRATEGY ){
			
			$articlesET = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
		
			$articlesATI = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
			$articlesATI += $it->next()->getTotalATI();
			
			$articlesVAT = 0.0;
			$it = $this->items->iterator();
			$itt = $this->items->iterator();
			while( $it->hasNext() && $itt->hasNext() )
			{
			
			$articlesVAT += ($it->next()->getTotalET()) * ($itt->next()->getVATRate()) /100;
			
			
			}
			
			$tot_ht = round($articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+$this->getChargesET(),2);
			$tot_vat = round($articlesVAT,2);
			$total_vat = round((( $tot_vat * ( 1- $this->recordSet[ "total_discount" ]/100 ) ) + ($this->getChargesET() * $this->getVATRate()/100)  ),2);
			$tot_ttc = $tot_ht + $total_vat;
		
            $this->recordSet[ "total_amount" ] 	= $articlesATI - ($articlesATI*$this->recordSet[ "total_discount" ]/100) + $this->getChargesATI();
			
			

			$this->recordSet[ "total_amount_ht" ]= $articlesET-$this->recordSet[ "total_discount_amount" ]+$this->getChargesET();
			
			$this->set( "total_discount_amount", 	round( $articlesET * $this->recordSet[ "total_discount" ] / 100.0, 2 ) );
			$this->set( "billing_amount", 			round( $this->recordSet[ "billing_rate" ] * ( $articlesET - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge_ht" ] ) / 100.0, 2 ) );
		
		}
		else{
		
			$articlesET = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
				$articlesET += $it->next()->getTotalET();
		
		$articlesATI = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
				$articlesATI += $it->next()->getTotalATI();
		
		
			$this->recordSet[ "total_amount" ] 	= $articlesATI - ($articlesATI*$this->recordSet[ "total_discount" ]/100) + $this->getChargesATI();
			
			$this->recordSet[ "total_amount_ht" ] 		= $articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+  ( $this->getChargesET() );
					
			
			$this->set( "total_discount_amount", 	round( $articlesATI * $this->recordSet[ "total_discount" ] / 100.0, 2 ) );
			$this->set( "billing_amount", 			round( $this->recordSet[ "billing_rate" ] * ( $articlesATI - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge" ] ) / 100.0, 2 ) );
			
		}
		
		
		//recalcul des frais de port
		
		$this->updateCharges();
		$this->updateSupplierCharges();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Recalcul des frais de port automatiquement
	 */
	protected function updateCharges(){
		
		$this->recordSet[ "charge_vat_rate" ] 	= $this->getVATRate();
		$this->recordSet[ "charge_vat" ] 		= $this->recordSet[ "total_charge" ] - $this->recordSet[ "total_charge_ht" ];
		
		if( !$this->recordSet[ "total_charge_auto" ] )
			return; //manuel : pas de chagement
			
		// commenté
		//$this->recordSet[ "total_charge_ht" ] 	= $this->getChargesET();
		//$this->recordSet[ "total_charge" ] 		= $this->getChargesATI();

		/* port "Départ Usine" pour la Corse, les DOM-TOMs et les pays étrangers */
		
		if( $this->getForwardingAddress()->getIdState() != 1 || ( 
				strlen( $this->getForwardingAddress()->getZipcode() ) > 1 
				&& substr( $this->getForwardingAddress()->getZipcode(), 0, 2 ) == "20" ) 
			)
			$this->recordSet[ "charge_free" ] = 0;

		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	
}

?>