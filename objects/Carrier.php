<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object transporteurs
 */
 

include_once( dirname( __FILE__ ) . "/Provider.php" );
include_once( dirname( __FILE__ ) . "/Deliverer.php" );


class Carrier extends Provider implements Deliverer{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $idcarrier
	 */
	protected $idcarrier;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idprovider
	 * @param int $idcontact
	 */
	public function __construct( $idcarrier, $idcontact = false ){

		$this->idcarrier = $idcarrier;
		
		parent::__construct( DBUtil::getDBValue( "idprovider", "carrier", "idcarrier", $this->idcarrier ), intval( $idcontact ) );
	
	}

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'identifiant du transporteur
	 * @override Provider::getId()
	 * @return int
	 */
	public function getId(){ return $this->idcarrier; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Nom ou raison sociale
	 * @access public
	 * @return string
	 */
	public function getName(){ return $this->recordSet[ "name" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @override Provider::create()
	 * @override Provider::create()
	 * @return Carrier
	 */
	public static function create(){

		$provider = parent::create();
		
		DBUtil::query( "INSERT INTO carrier ( idprovider ) VALUES( '" . $provider->getId() . "' )" );

		return new self( DBUtil::getInsertId(), $provider->getContact()->getId() );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access static
	 * @param int $idcarrier
	 * @override Provider::delete()
	 * @return void
	 */
	public static function delete( $idcarrier ){
		
		parent::delete( DBUtil::getDBValue( "idprovider", "carrier", "idcarrier",  $idcarrier ) );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @override Provider::allowDelete()
	 * @return bool
	 * @return bool
	 */
	public function allowDelete(){
		
		if( !parent::allowDelete() )
			return false;
			
		global $GLOBAL_DB_NAME;
		
		$rs =& DBUtil::query( "SHOW TABLES" );
	
		while( !$rs->EOF() ){
		
			$tablename = $rs->fields( "Tables_in_$GLOBAL_DB_NAME" );
			
			if( $tablename != "carrier" ){
				
				$rs2 =& DBUtil::query( "SHOW COLUMNS FROM `$tablename` LIKE 'idcarrier'" );
				
				if( $rs2->RecordCount() ){
				
					if( DBUtil::query( "SELECT idcarrier FROM `$tablename` WHERE idcarrier = '{$this->idcarrier}' LIMIT 1" )->RecordCount() )
						return false;
						
				}
			
			}
			
			$rs->MoveNext();
			
		}
	
		return true;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * TVA pour le transport de $source vers $destination
	 * @param Address $source
	 * @param Address $destination
	 * @return float
	 */
	public function getCarriageVATRate( Address $source, Address $destination ){
		
		if( $this->getVATRate() == 0.0 )
			return 0.0;
		
		$sourceVAT 		= DBUtil::getDBValue( "bvat", "state", "idstate", $source->getIdState() );
		$destinationVAT = DBUtil::getDBValue( "bvat", "state", "idstate", $destination->getIdState() );
		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>