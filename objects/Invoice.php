<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object factures clients
 */

include_once( dirname( __FILE__ ) . "/DBSale.php" );
include_once( dirname( __FILE__ ) . "/InvoiceItem.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/InvoiceAddress.php" );
include_once( dirname( __FILE__ ) . "/ForwardingAddress.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );


class Invoice extends DBSale{
	
	//----------------------------------------------------------------------------

	public static $STATUS_INVOICED 	= "Invoiced";
	public static $STATUS_PAID 		= "Paid";
		
	//----------------------------------------------------------------------------
    
    //@todo PHP 5.3 late state binding( protect static $TABLE_NAME, ... )
	protected $TABLE_NAME 			= "billing_buyer";
	protected $ITEMS_TABLE_NAME		= "billing_buyer_row";
	protected $PRIMARY_KEY_NAME 	= "idbilling_buyer";	
	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var int $idbilling_buyer le numéro de facture en franglish
	 */
    private $idbilling_buyer;

    //----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idbilling_buyer le numéro de facture
	 */
	function __construct( $idbilling_buyer ){
		
		$this->idbilling_buyer =  $idbilling_buyer;
	
		DBObject::__construct( "billing_buyer", false, "idbilling_buyer",  $idbilling_buyer  );	
		
		$this->setPriceStrategy();
		$this->setItems();	
		
		$this->salesman = new Salesman( $this->recordSet[ "iduser" ] );	
		
		$this->customer = new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );

		$iddelivery = DBUtil::getDBValue( "iddelivery", "order", "idorder", $this->recordSet[ "idorder" ] );
			
		$this->forwardingAddress = $iddelivery ? new ForwardingAddress( $iddelivery ) : $this->customer->getAddress();
		
		$this->invoiceAddress = $this->recordSet[ "idbilling_adress" ] ? new InvoiceAddress( $this->recordSet[ "idbilling_adress" ] ) : $this->customer->getAddress();	
	
	}

	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return int
     */
    public function getId(){ return $this->idbilling_buyer; }
    
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "TradeItem" );
		
		$rs =& DBUtil::query( "SELECT idrow FROM billing_buyer_row WHERE idbilling_buyer = '{$this->idbilling_buyer}' ORDER BY idrow ASC" );

		while( !$rs->EOF() ){
		
			$this->items->add( new InvoiceItem( $this, $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}

	//----------------------------------------------------------------------------
	/**
     * @access protected
     * @return OrderItem une ligne article éditable
     */
	protected function createItem(){
		
		$idrow = $this->items->size() + 1;
		
		DBUtil::query( "INSERT INTO billing_buyer_row( idbilling_buyer, idrow ) VALUES( '{$this->idbilling_buyer}', '$idrow' )" );

		$this->items->add( new InvoiceItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Supprime une facture
	 * @static
	 * @param int $idbilling_buyer le numéro de la facture à supprimer
	 * @return bool
	 */
	public static function delete( $idbilling_buyer ){

		if( DBUtil::getDBValue( "idbilling_buyer", "billing_regulations_buyer", "ididbilling_buyer", $this->idbilling_buyer ) )
			return false;
		
			if( DBUtil::getDBValue( "idbilling_buyer", "credits", "ididbilling_buyer", $this->idbilling_buyer ) )
			return false;
			
		DBUtil::query( "DELETE FROM `billing_buyer_row` WHERE `idbilling_buyer` = '" .  $idbilling_buyer  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM `billing_buyer` WHERE `idbilling_buyer` = '" .  $idbilling_buyer  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM `billing_relaunch` WHERE `idbilling_buyer` = '" .  $idbilling_buyer  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM `litigation_history` WHERE `idbilling_buyer` = '" .  $idbilling_buyer  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM `litigations` WHERE `idbilling_buyer` = '" .  $idbilling_buyer  . "' LIMIT 1" );
		
	}

	//------------------------------------------------------------------------------------------------------------
	
	public function addOrderItem( OrderItem &$orderItem ){

		$invoiceItem =& $this->createItem();
				
		$invoiceItem->set( "idarticle", 			$orderItem->get( "idarticle" ) );
		$invoiceItem->set( "quantity", 				$orderItem->get( "quantity" ) );
		$invoiceItem->set( "idorder_row", 			$orderItem->get( "idrow" ) );
		$invoiceItem->set( "unit_price", 			$orderItem->get( "unit_price" ) );
		$invoiceItem->set( "reference", 			$orderItem->get( "reference" ) );
		$invoiceItem->set( "n_article", 			$orderItem->get( "n_article" ) );
		$invoiceItem->set( "gencod", 				$orderItem->get( "gencod" ) );
		$invoiceItem->set( "code_customhouse", 		$orderItem->get( "code_customhouse" ) );
		$invoiceItem->set( "unit", 					$orderItem->get( "unit" ) );
		$invoiceItem->set( "lot", 					$orderItem->get( "lot" ) );
		$invoiceItem->set( "designation", 			$orderItem->get( "designation" ) );
		$invoiceItem->set( "ecotaxe_code", 			$orderItem->get( "ecotaxe_code" ) );
		$invoiceItem->set( "ecotaxe_amount", 		$orderItem->get( "ecotaxe_amount" ) );
		$invoiceItem->set( "summary", 				$orderItem->get( "summary" ) );
		$invoiceItem->set( "unit_cost", 			$orderItem->get( "unit_cost" ) );
		$invoiceItem->set( "unit_cost_rate", 		$orderItem->get( "unit_cost_rate" ) );
		$invoiceItem->set( "unit_cost_amount", 		$orderItem->get( "unit_cost_amount" ) );
		$invoiceItem->set( "ref_discount", 			$orderItem->get( "ref_discount" ) );
		$invoiceItem->set( "ref_discount_base", 	$orderItem->get( "ref_discount_base" ) );
		$invoiceItem->set( "ref_discount_add", 		$orderItem->get( "ref_discount_add" ) );
		$invoiceItem->set( "discount_price", 		$orderItem->get( "discount_price" ) );
		$invoiceItem->set( "idcategory", 			$orderItem->get( "idcategory" ) );
		$invoiceItem->set( "idsupplier", 			$orderItem->get( "idsupplier" ) );
		$invoiceItem->set( "status", 				$orderItem->get( "status" ) );
		$invoiceItem->set( "vat_rate", 				$orderItem->get( "vat_rate" ) );
		$invoiceItem->set( "weight", 				$orderItem->get( "weight" ) );
		$invoiceItem->set( "franco", 				$orderItem->get( "franco" ) );
		
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Vérifie si une facture est partielle ou non
	 * @static
	 * @param int $idbilling_buyer le numéro de facture en franglish
	 * @return bool true si la facture est partielle, sinon false
	 */
	 public static function isPartialInvoice( $idbilling_buyer ){
	 
		$query = "
		SELECT COUNT( orow.idrow ) AS orderRowCount, COUNT( bbrow.idrow ) AS invoiceRowCount
		FROM order_row orow, billing_buyer_row bbrow, `order` o, billing_buyer bb
		WHERE bb.idbilling_buyer = '$idbilling_buyer'
		AND bbrow.idbilling_buyer = bb.idbilling_buyer
		AND bb.idorder = o.idorder
		AND orow.idorder = o.idorder";

	 	$rs =& DBUtil::query( $query );
	 	
	 	if( $rs === false )
	 		die( "Impossible de déterminer si la facture '$idbilling_buyer' est une facture partielle ou non" );
	 		
	 	return $rs->fields( "invoiceRowCount" ) < $rs->fields( "orderRowCount" );
	 	
	 }

	 //----------------------------------------------------------------------------
	/**
	 * Reste à payer
	 * Calcule le montant TTC qui reste à payer dans le cas de paiement multiples 
	 * @return le montant TTC qui reste à payer putain de merde
	 */
	public function getBalanceOutstanding() {
		
		$invoice = $this->recordSet[ "idbilling_buyer" ];
		
		// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "
			SELECT SUM( billing_regulations_buyer.amount )  AS suma
			FROM billing_regulations_buyer,	regulations_buyer, billing_buyer
			WHERE billing_regulations_buyer.idbilling_buyer = '$invoice'
				AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer
				AND billing_regulations_buyer.idbilling_buyer = billing_buyer.idbilling_buyer
				AND regulations_buyer.from_down_payment = 0";
		
		$rs = DBUtil::query($query);

		$bigBill = $this->getNetBill();
		
		$amount = $bigBill - $rs->fields("suma");
		
		$overpayment_tolerance = DBUtil::getParameterAdmin("solde_tolerance");
		
		return $amount>$overpayment_tolerance?$amount:0.00;
		
	}

	//------------------------------------------------------------------------------------------------------------
	/**
	 * Assurance crédit
	 * @access public
	 * @param bool $insure
	 * @return bool true si la facture a pû être assuréé, sinon false 
	 */
	public function insure( $insure = true ){
		
		/* désactivation */
		
		if( !$insure ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}

		/* pas d'assurance crédit pour les paiements comptant */
						
		if( $this->recordSet[ "cash_payment" ] ){
			
			$this->set( "insurance", 0 );
			
			return false;
				
		}

		$idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) );
	
		/* assurance crédit désactivée */
		
		if( !$idcredit_insurance ){
			
			$this->set( "insurance", 0 );
			
			return false;
				
		}
		
		$creditInsurance	= ( object )DBUtil::query( "SELECT * FROM credit_insurance WHERE idcredit_insurance = '$idcredit_insurance' LIMIT 1" )->fields;
		$customer 			= new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );
		$insurance 			= $this->recordSet[ "insurance" ];
		$minimum 			= floatval( $creditInsurance->minimum );
		$maximum 			= floatval( $creditInsurance->maximum );
		$netBill 			= $this->getNetBill();
		$netBillET 			= round( $netBill / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();
		
		/* client réfusé ou non assuré */
	
		if( $customer->get( "insurance_status" ) != "Accepted" || !$customer->get( "insurance" ) ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* montant non couvert */
	
		if( $netBillET < $minimum || $netBillET > $maximum ){
		
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* encours maximum atteint */
	
		if( $outstandingCredits + $netBillET > $customer->get( "insurance_amount" ) ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
	
		/* facture assurable */
		
		$this->set( "insurance", 1 );
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#getChargesET()
	 */
	public function getChargesET(){ 
		
		return B2B_STRATEGY ? $this->recordSet[ "total_charge_ht" ] : round( $this->recordSet[ "total_charge" ] / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );

	}
		
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#getChargesATI()
	 */
	public function getChargesATI(){
	
		return B2B_STRATEGY ? round( $this->recordSet[ "total_charge_ht" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 ) : $this->recordSet[ "total_charge" ];
		
	}
		
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getNetMarginAmount()
	 */
	public function getNetMarginAmount(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getInvoiceNetMarginAmount( $this->idbilling_buyer );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getNetMarginRate()
	 */
	public function getNetMarginRate(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getInvoiceNetMarginRate( $this->idbilling_buyer );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getGrossMarginAmount()
	 */
	public function getGrossMarginAmount(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getInvoiceGrossMarginAmount( $this->idbilling_buyer );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getGrossMarginRate()
	 */
	public function getGrossMarginRate(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getInvoiceGrossMarginRate( $this->idbilling_buyer );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#recalculateAmounts()
	 */
	protected function recalculateAmounts(){
		
		$articlesET = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
		$articlesET += $it->next()->getTotalET();
		
			
		if( B2B_STRATEGY ){
		
		
		//$this->recordSet[ "total_amount" ] 	= ($articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+$this->getChargesET())*(1 +$this->recordSet[ "charge_vat_rate" ]/100 );
		
		$this->recordSet[ "total_amount_ht" ] = $articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+$this->getChargesET();
		
				
		$total_vat_2 = $total_amount_2 = $total_vat_1 = $total_amount_1 = 0.00;
		$price = 0;
		$vat_rate = 0;
		$quantity = 0;
		$rs =& DBUtil::query( "SELECT * FROM billing_buyer_row WHERE idbilling_buyer = '{$this->idbilling_buyer}'" );
		
		while( !$rs->EOF() ){
		
		$price = $rs->fields["discount_price"];
		$vat_rate = $rs->fields["vat_rate"];
		$quantity = $rs->fields["quantity"];
		$p = $quantity*$price;
		switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_2 = round ($total_vat_2, 2 );
								$total_amount_2 = $total_amount_2 + $quantity*$price ;
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + ($p) ;
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + ($p) ;
							break;							
							
							default:					
							break;
						}
						$rs->MoveNext();
						
						}
						$disc = $this->recordSet[ "total_discount" ];
						
						$total_amount_1 = $total_amount_1 - $total_amount_1 * $disc / 100;
						$total_amount_2 = $total_amount_2 - $total_amount_2 * $disc / 100;
						$total_vat_1 = $total_vat_1 - $total_vat_1 * $disc / 100;
						$total_vat_2 = $total_vat_2 - $total_vat_2 * $disc / 100;
						
						
						
						$this->recordSet[ "total_amount_1" ] = $total_amount_1;
		
						$this->recordSet[ "total_amount_2" ] = $total_amount_2;
			
						$this->recordSet[ "total_vat_1" ] = $total_vat_1;
		
						$this->recordSet[ "total_vat_2" ] = $total_vat_2;
						
			
		
		//////////////////////////////////				
									
		}
		else{
		
		$articlesATI = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
		$articlesATI += $it->next()->getTotalATI();
		
		
		$total_vat_2 = $total_amount_2 = $total_vat_1 = $total_amount_1 = 0.00;
		$price = 0;
		$vat_rate = 0;
		$quantity = 0;
		$rs =& DBUtil::query( "SELECT * FROM billing_buyer_row WHERE idbilling_buyer = '{$this->idbilling_buyer}'" );
		
		while( !$rs->EOF() ){
		
		$price = $rs->fields["discount_price"];
		$vat_rate = $rs->fields["vat_rate"];
		$quantity = $rs->fields["quantity"];
		$p = $quantity*$price;
		
		switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 ) / 1.055;
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 ) / 1.055;
								$total_vat_2 = round ($total_vat_2, 2 );
								//$total_amount_2 =  ($total_amount_2 + $quantity*$price ) /1.055 ;
								$total_amount_2 = $total_amount_2 + ( $quantity*$price ) /1.055 ;
								$total_amount_2 = round ($total_amount_2, 2 );
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 ) / 1.196;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ($quantity*$price) / 1.196 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 ) / 1.20;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ($quantity*$price) / 1.20 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							
							default:					
							break;
						}
		
						$rs->MoveNext();
						
						}
						$disc = $this->recordSet[ "total_discount" ];
						
						$total_amount_1 = $total_amount_1 - $total_amount_1 * $disc / 100;
						$total_amount_2 = $total_amount_2 - $total_amount_2 * $disc / 100;
						$total_vat_1 = $total_vat_1 - $total_vat_1 * $disc / 100;
						$total_vat_2 = $total_vat_2 - $total_vat_2 * $disc / 100;
						
						
						
						$this->recordSet[ "total_amount_1" ] = $total_amount_1;
		
						$this->recordSet[ "total_amount_2" ] = $total_amount_2;
			
						$this->recordSet[ "total_vat_1" ] = $total_vat_1;
		
						$this->recordSet[ "total_vat_2" ] = $total_vat_2;
						
					
			$articlesET = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
		
			$this->recordSet[ "total_amount" ] 	= $articlesATI - ($articlesATI*$this->recordSet[ "total_discount" ]/100) + $this->getChargesATI();
			
			$this->recordSet[ "total_amount_ht" ] 		= $articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+  ( $this->getChargesATI() / ( 1 + $this->getVATRate()/100 ) );

		
		}	
		/*
		 * @todo : pour les champs ci-dessous, on garde arbitrairement le pourcentage et non le montant : 
		 * @todo RENAME `total_discount` `total_discount_rate`
		 */

		if( B2B_STRATEGY ){
			
			$articlesET = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
			
			$articlesATI = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
			$articlesATI += $it->next()->getTotalATI();
			
			
			$articlesVAT = 0.0;
			$it = $this->items->iterator();
			$itt = $this->items->iterator();
			while( $it->hasNext() && $itt->hasNext() )
			{
			
			$articlesVAT += ($it->next()->getTotalET()) * ($itt->next()->getVATRate()) /100;
			
			
			}
			
			$tot_ht = round($articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+$this->getChargesET(),2);
			$tot_vat = round($articlesVAT,2);
			$total_vat = round((( $tot_vat * ( 1- $this->recordSet[ "total_discount" ]/100 ) ) + ($this->getChargesET() * $this->getVATRate()/100)  ),2);
			$tot_ttc = $tot_ht + $total_vat;
			
							
			
			$this->recordSet[ "total_amount" ] =  $tot_ttc;
			
			//$articlesATI - ( ( $articlesATI * $this->recordSet[ "total_discount" ]/100 )  + $this->getChargesET())*(1 +$this->recordSet[ "charge_vat_rate" ]/100 );exit;
			
			$this->set( "total_discount_amount", 	round(($articlesET*$this->recordSet[ "total_discount" ]/100), 2 ) );
			$this->set( "billing_amount", 			round( $this->recordSet[ "billing_rate" ] * ( $articlesET - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge_ht" ] ) / 100.0, 2 ) );
		
		}
		else{
			
			$articlesATI = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
				$articlesATI += $it->next()->getTotalATI();
			
			$this->set( "total_discount_amount", 	round(($articlesATI*$this->recordSet[ "total_discount" ]/100), 2 ) );
			$this->set( "billing_amount", 			round( $this->recordSet[ "billing_rate" ] * ( $articlesATI - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge" ] ) / 100.0, 2 ) );
			
		}
		
		//recalcul des frais de port
		
		$this->updateCharges();
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Recalcul des frais de port automatiquement
	 */
	protected function updateCharges(){
		
		$this->recordSet[ "charge_vat_rate" ] 	= $this->getVATRate();
		$this->recordSet[ "charge_vat" ] 		= $this->recordSet[ "total_charge" ] - $this->recordSet[ "total_charge_ht" ];
	
	}

    //============================================================================
	// 								Deprecated members
	//============================================================================
	
	/**
	 * @deprecated
	 */
	 public function getDelay($idBilling){
	 	
	 	$query = "SELECT delay_1 FROM delay WHERE iddelay =( SELECT delivdelay FROM order_row orow, billing_buyer bb WHERE bb.idorder = orow.idorder AND idbilling_buyer = $idBilling LIMIT 1 )";
	 	$rs = DBUtil::query($query);
	 	
	 	if( $rs === false ){
	 		return "Impossible de récupérer de delay d'expédition pour la facture $idBilling" ;
	 	}
	 	else{
	 		return $rs->fields("delay_1");
	 	}
	 	 	
	 }

	//----------------------------------------------------------------------------
	
}

?>