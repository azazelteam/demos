<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object règlements clients
 */

include_once( dirname( __FILE__ ) . "/classes.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );


class RegulationsBuyer{

	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var int $idregulations_buyer le numéro de paiement en franglish
	 */
    private $idregulations_buyer;
    
    //----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idregulations_supplier le numéro de paiement
	 */
	function __construct( $idregulations_buyer ){
		
		$this->idregulations_buyer 	= $idregulations_buyer;

		$this->fields 				= array();
		
		//données de la base
		
		$this->load();
		$this->salesman = new Salesman( $this->fields[ "iduser" ] );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un attribut donné de la facture fournisseur
	 * @param string $key l'attribut de la facture fournisseur dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	 
	public final function get( $key ){ 
		
		return $this->fields[ $key ]; 
		
	}
	
	//----------------------------------------------------------------------------
		
	/**
	 * Affecte une valeur à une propriété de la facture fournisseur donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 	 
	public final function set( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les données depuis la base de données
	 */
	public function load(){
		
		$query = "
		SELECT * FROM `regulations_buyer`
		
		 WHERE `idregulations_buyer` = '" .  $this->idregulations_buyer   ."'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau paiement vierge pour une ou plusieurs factures données
	 * @static
	 * @param array $billings les numéros de factures concernés par le paiement
	 * @return regulationsbuyer un nouveau paiement
	 */
	public static function &createRegulationsBuyer( $billings , $idpayer ){
	 	
		/* tempo debug */
		
		if( !is_array( $billings ) )
			$billings = explode(",",$billings);
			
		foreach( $billings as $invoice )
			if( floatval( $invoice[ "amount" ] ) == 0.0 )
				trigger_error( "BUG CREATION REGLEMENT FOURNISSEUR POUR 0 EURO", E_USER_ERROR );
		
		/* fin debug */
				
	 	$iduser = User::getInstance()->getId();
	 	$login = User::getInstance()->get( "login" );
	 	/*
	 	//chercher un identifiant libre
	 	$q = "SELECT MAX(idregulations_buyer) as id FROM regulations_buyer";
	 	$r =& DBUtil::query( $q );

	 	if( $r === false )
	 		die( "Impossible de récupérer un identifiant pour le paiement" );
	 	
	 	if( $r->fields( "id" ) == null )
	 		$id = 1;	
	 	else $id = $r->fields("id") + 1;
	 	*/
		// ------- Mise à jour de l'id gestion des Index  #1161

		$table = 'regulations_buyer';
		$id = TradeFactory::Indexations($table);
	 	//réserver le numero de paiement	 	
	 	$query = "
		INSERT INTO `regulations_buyer` ( 
			idregulations_buyer,
			idbuyer,
			iduser,
			DateHeure
		) VALUES ( 
			'$id',
			'$idpayer',
			'$iduser',
			NOW()
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false )
			trigger_error( "Impossible de créer le nouveau paiement", E_USER_ERROR );
		
		//enregistrer le numéro de paiement pour les factures concernées
		if(!is_array($billings))
			$bil = explode(",",$billings);
		else
			$bil = $billings;
		
		foreach( $bil as $invoice ){
			
			$idbilling_buyer = $invoice[ "idbilling" ];
			$amount = $invoice[ "amount" ];
			$rebate = isset( $invoice[ "rebate_amount" ] ) ? $invoice[ "rebate_amount" ] : 0.0;
				// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "
				INSERT INTO billing_regulations_buyer (
					idbilling_buyer,
					idregulations_buyer,
					amount,
					rebate_amount,
					username,
					lastupdate
				) VALUES (
					'$idbilling_buyer',
					'$id',
					'$amount',
					'$rebate',
					'$login',
					NOW()
				)";
			
			$ret =& DBUtil::query( $query );
			
			if( $ret === false )
				trigger_error( "Impossible d'enregistrer le paiement pour la facture $idbilling_buyer", E_USER_ERROR );
			
		}
			
		//créer le paiement
		
		$regulationsbuyer = new RegulationsBuyer( $id );
	 	
	 	return $regulationsbuyer;
	 	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la liste des factures concernées par le règlement
	 * @return array $billings, les factures correspondantes au règlement
	 */
	 
	 public function getBillings(){
	 	
	 	$q = "SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer=".$this->idregulations_buyer."";
	 	$r =& DBUtil::query( $q );
	 	
	 	return $r;
	 }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet dans la table
	 * @access public
	 * @return void
	 */
	 
	public function update(){
		
		$username = User::getInstance()->get( "login" );

		$lastupdate = date( "Y-m-d H:i:s" );
		
		$this->set( "username" , $username );
		$this->set( "lastupdate" , $lastupdate );
		
		//mise à jour

		$updatableFields = array();
		foreach( $this->fields as $fieldname => $value ){
			
			if( $fieldname != "idregulations_buyer" )
				$updatableFields[] = $fieldname;
			
		}
		
		$query = "
		UPDATE `regulations_buyer`
		SET ";
		
		$i = 0;
		foreach( $updatableFields as $fieldname ){
		
			if( $i )
				$query .= ", ";
					
			$query .= "`$fieldname` = '" . Util::html_escape( $this->fields[ $fieldname ] ) . "'";
		
			$i++;
			
		}
		$query.=" WHERE `idregulations_buyer` = '".$this->idregulations_buyer."'";
		
		if( DBUtil::query( $query ) === false )
			trigger_error( "Impossible de mettre à jour le règlement", E_USER_ERROR );
		
	 }

	//----------------------------------------------------------------------------
	
	/**
	 * Enregistre
	 */
	public static function createPayment( Invoice $invoice ){
	
		//On contrôle si il y a déjà un règlement pour la commande
		
		$query = "SELECT brb.idregulations_buyer, brb.idbilling_buyer
				FROM billing_buyer bb, billing_regulations_buyer brb
				WHERE bb.idorder = '" . $invoice->get( "idorder" ) . "'
				AND bb.idbilling_buyer = brb.idbilling_buyer";
		$rsbil = & DBUtil::query( $query );
		
		if($rsbil===false)
			die("Impossible de vérifier les factures pour la commande");
			
		if(!$rsbil->RecordCount()){
			$billing[0]["idbilling"] = $invoice->get("idbilling_buyer") ;
			$billing[0]["amount"]    = $invoice->getNetBill() ;
			//On créé un règlement
			$regulationsbuyer = &RegulationsBuyer::createRegulationsBuyer( $billing  , $invoice->get( "idbuyer" ) );
		
			//On affecte les valeurs
			
			$regulationsbuyer->set( "payment_date" , 	$invoice->get( "balance_date" ) ); //est-ce qu'il ne s'agit pas de la date actuelle?
			$regulationsbuyer->set( "deliv_payment" , 	$invoice->get( "balance_date" ) ); //?
			$regulationsbuyer->set( "idpayment" , 		$invoice->get( "idpayment" ) ); 	//dupliquata :o(
			$regulationsbuyer->set( "amount" , 		$invoice->getNetBill() );
			$regulationsbuyer->set( "accept" , 		1 );
		
			//On sauvegarde
			$regulationsbuyer->update();
		
		}else{
		
			$idrb = $rsbil->fields("idregulations_buyer");
			
			//on associe le règlement à cette facture
			$idbb = $invoice->get("idbilling_buyer");
				// ------- Mise à jour de l'id gestion des Index  #1161
			$regl = "INSERT INTO billing_regulations_buyer (idbilling_buyer,idregulations_buyer) VALUES ('$idbb',$idrb)";
			$rsregl = & DBUtil::query( $regl );
		
			if($rsregl===false)
				die("Impossible de mettre à jour le règlement de la facture");
				
		}
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Enregistre un règlement partiel pour une facture donnée
	 * @param Invoice la facture
	 * @param float $amount le montant réglé
	 */
	public static function createPartialPayment( Invoice &$invoice, $idpayment, $amount , $acompte = false, $acompteDate = false, $commentAcompte = ""){
		
		$regulationsbuyer =& RegulationsBuyer::createRegulationsBuyer( $invoice->getId() , $invoice->get( "idbuyer" ));
		
		if($acompte){
			$regulationsbuyer->set( "from_down_payment" , $acompte	);
			$regulationsbuyer->set( "comment" , $commentAcompte	);
			
			if($acompteDate)
				$regulationsbuyer->set( "payment_date" , $acompteDate	);
			else
				$regulationsbuyer->set( "payment_date" , date("Y-m-d")	);

		}else{
			$regulationsbuyer->set( "payment_date" , 	$invoice->get( "balance_date" ) ); //est-ce qu'il ne s'agit pas de la date actuelle?
			
		}
		
		$regulationsbuyer->set( "deliv_payment" , 	$invoice->get( "balance_date" ) ); //?
		$regulationsbuyer->set( "idpayment" , 		$idpayment ); 							//dupliquata :o(
		$regulationsbuyer->set( "amount" , 		min( $amount, $invoice->getNetBill() ) );
			
		$regulationsbuyer->set( "accept" , 		1 );
	
		//On sauvegarde
		$regulationsbuyer->update();
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Liste les règlements pour une facture
	 * @param Invoice la facture
	 */
	public static function getRegulations( $idInvoice ){
		
		$arrayRegulations=array();
		
		$rsRegul = DBUtil::query("SELECT rb.idregulations_buyer, 
										rb.amount,
										brb.amount as detailledAmount, 
										rb.comment, 
										rb.idpayment, 
										rb.payment_date, 
										rb.deliv_payment, 
										rb.DateHeure, 
										rb.bank_date, 
										rb.accept, 
										rb.from_down_payment,
										rb.factor,
										rb.piece_number,
										u.initial,b.bank_name
									FROM billing_regulations_buyer brb, billing_buyer bb, regulations_buyer rb
									LEFT JOIN `user` u ON u.iduser = rb.iduser
									LEFT JOIN bank b ON b.idbank = rb.idbank
									WHERE rb.idregulations_buyer = brb.idregulations_buyer
									AND bb.idbilling_buyer = brb.idbilling_buyer
									AND brb.idbilling_buyer = '$idInvoice'
									ORDER BY idregulations_buyer ASC");
		
		if($rsRegul->RecordCount() && $rsRegul->fields( "idregulations_buyer" )){
			$i=0;
			while(!$rsRegul->EOF()){
			
				$arrayRegulations[$i]["idregulations_buyer"] 	= $rsRegul->fields("idregulations_buyer");
				$arrayRegulations[$i]["amount"] 				= $rsRegul->fields("amount");
				$arrayRegulations[$i]["detailledAmount"] 		= $rsRegul->fields("detailledAmount");
				$arrayRegulations[$i]["comment"] 				= $rsRegul->fields("comment");
				$arrayRegulations[$i]["idpayment"] 				= $rsRegul->fields("idpayment");
				$arrayRegulations[$i]["payment_date"] 			= $rsRegul->fields("payment_date");
				$arrayRegulations[$i]["deliv_payment"] 			= $rsRegul->fields("deliv_payment");
				$arrayRegulations[$i]["DateHeure"] 				= $rsRegul->fields("DateHeure");
				$arrayRegulations[$i]["bank_date"] 				= $rsRegul->fields("bank_date");
				$arrayRegulations[$i]["accept"] 				= $rsRegul->fields("accept");
				$arrayRegulations[$i]["from_down_payment"] 		= $rsRegul->fields("from_down_payment");
				$arrayRegulations[$i]["factor"] 				= $rsRegul->fields("factor");
				$arrayRegulations[$i]["initial"] 				= $rsRegul->fields("initial");
				$arrayRegulations[$i]["bank"] 					= $rsRegul->fields("bank_name");
				$arrayRegulations[$i]["piece"] 					= $rsRegul->fields("piece_number");
				
				$rsRegul->MoveNext();
				$i++;
			}
		}
		
		return( $arrayRegulations);
	}
	
	/**
	 * Liste les règlements pour une commande
	 * @param idorder la commande
	 */
	public static function getOrderRegulations( $idOrder ){
		
		$arrayRegulations=array();
		// ------- Mise à jour de l'id gestion des Index  #1161
		$rsRegul = DBUtil::query("SELECT rb.idregulations_buyer, 
										rb.amount,
										rb.comment, 
										rb.idpayment, 
										rb.payment_date, 
										rb.deliv_payment, 
										rb.DateHeure, 
										rb.bank_date, 
										rb.accept,
										rb.from_down_payment,
										rb.piece_number,
										u.initial,
										b.bank_name
									FROM regulations_buyer rb
									JOIN `order` o ON ( rb.idregulations_buyer = o.cash_payment_idregulation ) OR ( rb.idregulations_buyer = o.down_payment_idregulation )
									LEFT JOIN `user` u ON rb.iduser = u.iduser
									LEFT JOIN bank b ON b.idbank = rb.idbank
									WHERE o.idorder = '$idOrder'
									ORDER BY idregulations_buyer ASC");
		
		if($rsRegul->RecordCount() && $rsRegul->fields( "idregulations_buyer" )){
			$i=0;
			while(!$rsRegul->EOF()){
			
				$arrayRegulations[$i]["idregulations_buyer"] 	= $rsRegul->fields("idregulations_buyer");
				$arrayRegulations[$i]["amount"] 				= $rsRegul->fields("amount");
				$arrayRegulations[$i]["detailledAmount"] 		= $rsRegul->fields("detailledAmount");
				$arrayRegulations[$i]["comment"] 				= $rsRegul->fields("comment");
				$arrayRegulations[$i]["idpayment"] 				= $rsRegul->fields("idpayment");
				$arrayRegulations[$i]["payment_date"] 			= $rsRegul->fields("payment_date");
				$arrayRegulations[$i]["deliv_payment"] 			= $rsRegul->fields("deliv_payment");
				$arrayRegulations[$i]["DateHeure"] 				= $rsRegul->fields("DateHeure");
				$arrayRegulations[$i]["bank_date"] 				= $rsRegul->fields("bank_date");
				$arrayRegulations[$i]["accept"] 				= $rsRegul->fields("accept");
				$arrayRegulations[$i]["from_down_payment"] 		= $rsRegul->fields("from_down_payment");
				$arrayRegulations[$i]["factor"] 				= $rsRegul->fields("factor");
				$arrayRegulations[$i]["initial"] 				= $rsRegul->fields("initial");
				$arrayRegulations[$i]["bank"] 					= $rsRegul->fields("bank_name");
				$arrayRegulations[$i]["piece"] 					= $rsRegul->fields("piece_number");
				
				$rsRegul->MoveNext();
				$i++;
			}
		}
		
		return( $arrayRegulations);
	}
	
	/**
	 * Liste les règlements pour un devis
	 * @param idestimate le devis
	 */
	public static function getEstimateRegulations( $idEstimate ){
		
		$arrayRegulations=array();
			// ------- Mise à jour de l'id gestion des Index  #1161
		$rsRegul = DBUtil::query("SELECT rb.idregulations_buyer, 
										rb.amount,
										rb.comment, 
										rb.idpayment, 
										rb.payment_date, 
										rb.deliv_payment, 
										rb.DateHeure, 
										rb.bank_date, 
										rb.accept,
										rb.from_down_payment,
										rb.piece_number,
										u.initial,
										b.bank_name
									FROM regulations_buyer rb
									JOIN `estimate` es ON ( rb.idregulations_buyer = es.cash_payment_idregulation ) OR ( rb.idregulations_buyer = es.down_payment_idregulation )
									LEFT JOIN `user` u ON rb.iduser = u.iduser
									LEFT JOIN bank b ON b.idbank = rb.idbank
									WHERE es.idestimate = '$idEstimate'
									ORDER BY idregulations_buyer ASC");
		
		if($rsRegul->RecordCount() && $rsRegul->fields( "idregulations_buyer" )){
			$i=0;
			while(!$rsRegul->EOF()){
			
				$arrayRegulations[$i]["idregulations_buyer"] 	= $rsRegul->fields("idregulations_buyer");
				$arrayRegulations[$i]["amount"] 				= $rsRegul->fields("amount");
				$arrayRegulations[$i]["detailledAmount"] 		= $rsRegul->fields("detailledAmount");
				$arrayRegulations[$i]["comment"] 				= $rsRegul->fields("comment");
				$arrayRegulations[$i]["idpayment"] 				= $rsRegul->fields("idpayment");
				$arrayRegulations[$i]["payment_date"] 			= $rsRegul->fields("payment_date");
				$arrayRegulations[$i]["deliv_payment"] 			= $rsRegul->fields("deliv_payment");
				$arrayRegulations[$i]["DateHeure"] 				= $rsRegul->fields("DateHeure");
				$arrayRegulations[$i]["bank_date"] 				= $rsRegul->fields("bank_date");
				$arrayRegulations[$i]["accept"] 				= $rsRegul->fields("accept");
				$arrayRegulations[$i]["from_down_payment"] 		= $rsRegul->fields("from_down_payment");
				$arrayRegulations[$i]["factor"] 				= $rsRegul->fields("factor");
				$arrayRegulations[$i]["initial"] 				= $rsRegul->fields("initial");
				$arrayRegulations[$i]["bank"] 					= $rsRegul->fields("bank_name");
				$arrayRegulations[$i]["piece"] 					= $rsRegul->fields("piece_number");
				
				$rsRegul->MoveNext();
				$i++;
			}
		}
		
		return( $arrayRegulations);
	}
}
?>