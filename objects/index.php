<?php

include_once( "./config/init.php" );

/* ------------------------------------------------------------------------------------- */
/* N° de page */

if( isset( $_GET[ "pageNumber" ] ) ){
	
	header( "Location: " . getUrlFromPageNumber( intval( $_GET[ "pageNumber" ] ) ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------- */
  
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CCategoryProxy.php" );

$controller = new PageController( "index.tpl" );
$controller->setData( "category", new CCategoryProxy( new CCategory( 0 ) ) );

$controller->output();

/* ------------------------------------------------------------------------------------- */

function getUrlFromPageNumber($idpage){
		
	global $GLOBAL_START_URL,$GLOBAL_START_PATH;
	
	$pageUrl = "index.php";

	//parser idpage pour recup le 1er chiffre
	$firstElement = substr($idpage,0,1);
			
	//si cat ou prod => requete => return[$idcomposérequete]
	if($firstElement == 1 || $firstElement == 2){
		$pageNumber	  = substr($idpage,-strlen($idpage)+1);
		if($firstElement == 1){
			$rsIdproduct = DBUtil::query("SELECT idproduct,name_1 as name FROM product ORDER BY idproduct ASC LIMIT $pageNumber,1");
			
			$pageUrl = URLFactory::getProductURL($rsIdproduct->fields("idproduct"),$rsIdproduct->fields("name"));
		}else{
			$rsIdcategory = DBUtil::query("SELECT idcategory,name_1 as name FROM category ORDER BY idcategory ASC LIMIT $pageNumber,1");
			
			$pageUrl = URLFactory::getCategoryURL($rsIdcategory->fields("idcategory"),$rsIdcategory->fields("name"));
		}
	}else{
		//sinon return du nom du ficher par le tableau des pages
		$pages = getPagesNumbers();
		$pageNumber	  = $idpage;
		$pageName = $pages[$pageNumber];
		if (array_key_exists($pageNumber, $pages)) {
			if($firstElement == 3){
				$pageUrl = "$GLOBAL_START_URL/$pageName";
			}else{
				$pageUrl = "$GLOBAL_START_URL/catalog/$pageName";
			}
		}else{
			$pageUrl = $GLOBAL_START_URL;
		}
	}
	
	return $pageUrl;
	
}

/* ------------------------------------------------------------------------------------- */

function getPagesNumbers(){
	
	global $GLOBAL_START_PATH;
	
	$dirs = array();
	$pageNumbers = array();
	
	// DEFINITION DES PAGES A SURTOUT PAS INCLURE
	$pagesInterdites = array("admin.php");
	
	// DEFINITION DES PAGES AUTORISEES dans catalog
	$pagesCatalog = array(	"account.php",
							"basket.php",
							"buyer_catalog.php",
							"buyer_infos.php",
							"buying.php",
							"buyer_password.php",
							"club.php",
							"composition.php",
							"contact.php",
							"destock.php",
							"estimate_basket.php",
							"faq.php",
							"forgetmail.php",
							"gondole.php",
							"legales.php",
							"newsletter.php",
							"promotions.php",
							"plan.php",
							"product_brand.php",
							"product_compare.php",
							"product_novelty.php",
							"product_list.php",
							"product_trade.php",
							"qui.php",
							"register.php",
							"search.php",
							"survey.php"
						);
	
	
	// ALLONS Y POUR LES REPERTOIRES
	
	$dirs[0]["num"]  = 3;
	$dirs[0]["name"] = "root";
	
	$dirs[1]["num"]  = 4;
	$dirs[1]["name"] = "catalog/";
	
	for( $a=0 ; $a < count($dirs) ; $a++ ){
		
		if($dirs[$a]["name"] == "root"){
			$rep = "$GLOBAL_START_PATH/";
		}else{
			$rep = "$GLOBAL_START_PATH/".$dirs[$a]["name"];
		}
		
		$dir = opendir($rep);
		
		$primaryNum = $dirs[$a]["num"];
		$numfile = 0;
		
		
		$p = 0;
		while ($f = readdir($dir)) {
			$path_info = pathinfo($f);
		
		   if(is_file($rep.$f) && $path_info['extension'] == "php" && !in_array($f,$pagesInterdites) ){
		   		
		   		if(($primaryNum == 4 && in_array($f,$pagesCatalog)) || $primaryNum == 3){
		   			$pageNumbers[$primaryNum.$p]=$f;
		   		
		   			$p++;
		   		}
		   }
		   
		} 
	}	

	return $pageNumbers;
}

/* ------------------------------------------------------------------------------------- */

?>