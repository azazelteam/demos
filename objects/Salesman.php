<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object logo + signature clients
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );

class Salesman extends DBObject{
	
	//----------------------------------------------------------------------------
	
	public function __construct( $iduser, $secureDataMembers = false ){
	
		parent::__construct( "user", $secureDataMembers, "iduser", intval( $iduser ) );

	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return int un identifiant unique
	 */
	public function getId(){ return $this->recordSet[ "iduser" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return string l'URI de la photo de l'utilisateur si elle existe, sinon false
	 */
	public function getPhotoURI(){
		
		$files = glob( dirname( __FILE__ ) . "/../images/users/" . $this->recordSet[ "iduser" ] . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
		
		return count( $files ) ? "/images/users/" . basename( $files[ 0 ] ) : false;
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return string l'URI de la signature de l'utilisateur si elle existe, sinon false
	 */
	public function getSignatureURI(){
		
		$files = glob( dirname( __FILE__ ) . "/../images/users/signatures/" . $this->recordSet[ "iduser" ] . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
		
		return count( $files ) ? "/images/users/signatures/" . basename( $files[ 0 ] ) : false;
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @param string login
	 * @return Salesman
	 */	
	public static function create( $login ){
		
		include_once( dirname( __FILE__ ) . "/Util.php" );
		
		/* création de l'utilisateur */
	
		DBUtil::query(
		
			"INSERT INTO `user` ( login, password ) 
			VALUES( " . DBUtil::quote( $login ) . ", 
			" . DBUtil::quote( Util::getRandomPassword() ) . " )" 
		
		);
		
		$salesman = new self( DBUtil::getInsertID() );
		
		/* création du groupe de l'utilisateur */
		
		DBUtil::query( "INSERT INTO `group`( name ) VALUES( " . DBUtil::quote( $login ) . " )" );
		$salesman->set( "idusergroup", DBUtil::getInsertID() );
		
		/* affectation aux groupes utilisateurs */
		
		DBUtil::query( 
		
			"INSERT INTO `user_group` ( `iduser`, `idgroup` ) 
			VALUES ( '" . $salesman->getId() . "', 1 ), 
			( '" . $salesman->getId() . "', '" . $salesman->get( "idusergroup" ) . "' )"
			
		);
		
		$salesman->save();
		
		return $salesman;
		
	}

	//-----------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBObject#save()
	 */
	public function save(){

		/* mise à jour du groupe de l'utilisateur */
		
		DBUtil::query( 
		
			"UPDATE `group` 
			SET name = " . DBUtil::quote( $this->recordSet[ "login" ] ) . "
			WHERE idgroup = '" . $this->recordSet[ "idusergroup" ] . "'
			LIMIT 1" 
		
		);

		parent::save();

	}

	//-----------------------------------------------------------------------------------
	
}

?>