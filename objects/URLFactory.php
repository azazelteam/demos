<?php
 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object génération des URL
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );


class URLFactory{

	/* ------------------------------------------------------------------------------------------ */
	/* taille des images produit, catégorie, logos, ... */
	
	public static $IMAGE_DEFAULT_SIZE 	= 1;
	public static $IMAGE_ICON_SIZE 		= 2;
	public static $IMAGE_THUMB_SIZE 	= 3;
	public static $IMAGE_MAX_SIZE 		= 4;
	public static $IMAGE_CUSTOM_SIZE 	= 5;
    public static $IMAGE_DEFAULT_CUSTOM_SIZE 	= 6;
	
	/* ------------------------------------------------------------------------------------------ */
	
	public static function getHostURL(){ 
		
		return "https://" . $_SERVER[ "HTTP_HOST" ];
	
	}

	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Formatage d'une chaîne de caractères pour le référencement des URLs
	 * @access public
	 * @static
	 * @param string
	 * @return string
	 */
	public static function rewrite( $string ) {
   
   		$chars 		= array( "é","è","ê","ë","à","â","ä","ç","ù","û","ü","ô","ö","½","î","ï" );
		$replace 	= array( "e","e","e","e","a","a","a","c","u","u","u","o","o","oe","i","i" );
	
		$string = str_replace( $chars, $replace, $string );
		
	   	$string = preg_replace( "/[^0-9a-zA-Z\-]{1}/", "-", $string );
    
    	while( strpos( $string, "--" ) !== false )
			$string = str_replace( "--", "-", $string );
		
		if( strlen( $string ) && strrpos( $string, "-" ) === strlen( $string ) - 1 )
			$string = substr( $string, 0, strlen( $string ) - 1 );
			
		if( strlen( $string ) && strpos( $string, "-" ) === 0 )
			$string = substr( $string, 1 );
			
		return $string;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Encodage en base64 pour le passage de données dans l'URL
	 * @author Andi (http://nl2.php.net/manual/en/function.base64-encode.php#82200)
	 * @access public
	 * @static
	 * @param string $plainText
	 * @return string
	 */
	public static function base64url_encode( $plainText ) {
   
	    $base64 = base64_encode( $plainText );
	    
	    $base64url = strtr( $base64, "+/=", "-_," );
	    
	    return $base64url;
    
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Décodage en base64 pour le passage de données dans l'URL
	 * @author Andi (http://nl2.php.net/manual/en/function.base64-encode.php#82200)
	 * @access public
	 * @static
	 * @param string $plainText
	 * @return string
	 */
	public static function base64url_decode( $plainText ) {
	   
	    $base64url = strtr( $plainText, "-_,", "+/=" );
	   
	    $base64 = base64_decode( $base64url );
	    
	    return $base64;
	     
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	public static function getProductURL( $idproduct , $gclid = ''){ 
	
		if( !intval( $idproduct ) )
			return self::getHostURL();
		$gclid = ($gclid) ? '?gclid='.$gclid : '';
		$uri = "/" . self::getProductRewritePattern();
		$uri = str_replace( "%id", $idproduct, $uri );
		
		$uri = str_replace( "%name", self::rewrite( DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct ) ), $uri );
		
		//$uri = $uri = str_replace( "%name", DBUtil::getDBValue( "alias", "product", "idproduct", $idproduct ), $uri );

		return self::getHostURL() . $uri.$gclid;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	public static function getCategoryURL( $idcategory , $gclid = ''){ 

		if( !intval( $idcategory ) )
			return self::getHostURL();
		$gclid = ($gclid) ? '?gclid='.$gclid : '';
		
		$uri = "/" . self::getCategoryRewritePattern();
		$uri = str_replace( "%id", $idcategory, $uri );
		$uri = str_replace( "%name", self::rewrite( DBUtil::getDBValue( "name" . "_1", "category", "idcategory", $idcategory ) ), $uri );
	//var_dump(self::getHostURL() . $uri.$gclid);exit();
		return self::getHostURL() . $uri.$gclid;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	public static function getPDFURL( $idproduct ){

		if( !intval( $idproduct ) )
			return self::getHostURL();

		$uri = "/" . self::getPDFRewritePattern();
		$uri = str_replace( "%id", $idproduct, $uri );
		$uri = str_replace( "%name", self::rewrite( DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct ) ), $uri );
	
		return self::getHostURL() . $uri;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	public static function getProductListURL( $idcategory ){

		if( !intval( $idcategory ) )
			return self::getHostURL();

		$uri = "/" . self::getProductListRewritePattern();
		$uri = str_replace( "%id", $idcategory, $uri );
		$uri = str_replace( "%name", self::rewrite( DBUtil::getDBValue( "name" . "_1", "category", "idcategory", $idcategory ) ), $uri );
	
		return self::getHostURL() . $uri;
		
	}

	/* ------------------------------------------------------------------------------------------ */
	
	public static function getBrandURL( $idbrand ){

		if( !intval( $idbrand ) )
			return self::getHostURL();

		$uri = HTTP_HOST . '/' . self::rewrite( DBUtil::getDBValue( 'brand', 'brand', 'idbrand', $idbrand ) ) . "-brand-$idbrand";
	
		return $uri;
		
	}	

	/* ------------------------------------------------------------------------------------------ */
	
	public static function getCollectionURL( $idcollection ){

		if( !intval( $idcollection ) )
			return self::getHostURL();
		
		$idbrand = DBUtil::getDBValue( 'idbrand', 'collection', 'idcollection', $idcollection );
			
		$uri = HTTP_HOST . '/' . self::rewrite( DBUtil::getDBValue( 'brand', 'brand', 'idbrand', $idbrand ) . ' ' . DBUtil::getDBValue( 'name', 'collection', 'idcollection', $idcollection ) ) . "-col-$idcollection";
	
		return $uri;
		
	}	
		
	/* ------------------------------------------------------------------------------------------ */
	 /**
	  * @access public
	  * @static
	  * @return bool
	  */
	 public static function getProductRewritePattern(){

	 	static $pattern = NULL;
	 	
	 	if( $pattern === NULL )
	 		$pattern = DBUtil::getParameter( "URLRewritingProductPattern");
	 	
	 	return $pattern;
	 	
	 }
	
	/* ------------------------------------------------------------------------------------------ */
	 /**
	  * @access public
	  * @static
	  * @return bool
	  */
	 public static function getCategoryRewritePattern(){

	 	static $pattern = NULL;
	 	
	 	if( $pattern === NULL )
	 		$pattern = DBUtil::getParameter( "URLRewritingCategoryPattern");
	 	
	 	return $pattern;
	 	
	 }
	 
	 /* ------------------------------------------------------------------------------------------ */
	 /**
	  * @access public
	  * @static
	  * @return bool
	  */
	 public static function getPDFRewritePattern(){

	 	static $pattern = NULL;
	 	
	 	if( $pattern === NULL )
	 		$pattern = DBUtil::getParameter( "URLRewritingPDFPattern");
	 	
	 	return $pattern;
	 	
	 }
	 
	 /* ------------------------------------------------------------------------------------------ */
	 /**
	  * @access protected
	  * @static
	  * @return bool
	  */
	 protected static function getProductListRewritePattern(){

	 	static $pattern = NULL;
	 	
	 	if( $pattern === NULL )
	 		$pattern = DBUtil::getParameter( "URLRewritingProductlistPattern");
	 	
	 	return $pattern;
	 	
	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de l'image de la bannière personnailisée pour la catégorie si elle existe
	 * @access public
	 * @param int $idcategory
	 * @return string l'URI de l'image bannière ou false
	 */
	 public static function getCategoryBannerURI( $idcategory ){

		$banner = basename( trim( DBUtil::getDBValue( "banner", "category", "idcategory", intval( $idcategory ) ) ) );
		
		if( !strlen( $banner ) )
			return false;
			
		if( file_exists( dirname( __FILE__ ) . "/../images/categories/banners/" . basename( $banner ) ) )
			return "/images/categories/banners/" . basename( $banner );
			
		return false;

	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de l'image de la catégorie
	 * @access public
	 * @param int $idcategory
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $index la $index-ième image souhaitée
	 * @param int $width largeur de l'image souhaitée, utilisée que si $Size vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de l'image de la catégorie ou false si la $index-ième image n'existe pas
	 */
	 public static function getCategoryImageURI( $idcategory, $imageSize = 1, $index = 0, $width = false, $height = false ){
	$categName = preg_replace("/\s/","_",DBUtil::getDBValue( "name_1", "category", "idcategory", intval( $idcategory ) ));
	 	$uri = self::getImageURI( 
	 	
	 		$categName . "-c",
	 		"/images/categories",
	 		intval( $index ) > 0 ? $idcategory . "." . intval( $index ) : $idcategory,
	 		$imageSize,
	 		$width,
	 		$height
	 		
	 	);

	 	if( $uri == false )
	 		return self::getImageURI( 
	 	
		 		"c",
		 		"/images/categories",
		 		"default",
		 		$imageSize,
		 		$width,
		 		$height
		 		
		 	);

	 	return $uri;
	 	
	 }
	 
	 /* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de l'image de la catégorie sur la page d'accueil
	 * @access public
	 */
	 public static function getCategoryImageAccueilURI( $idcategory, $imageSize = 1, $index = 0, $width = false, $height = false ){
	
	$name = explode(".", DBUtil::getDBValue( "logo_accueil", "category", "idcategory", intval( $idcategory ) ));

	 	$uri = self::getImageURI( 
	 	
	 		//$idcategory . "-c",
            "",
	 		//"/images/categories/logos_accueil/".$idcategory."/",
	 		"/images/category/logos_accueil/",
	 		$name[0],
	 		$imageSize,
	 		$width,
	 		$height
	 		
	 	);

	 	if( $uri == false )
	 		return $index ? false : self::getImageURI( 
	 	
		 		"c",
		 		"/images/categories",
		 		"default",
		 		$imageSize,
		 		$width,
		 		$height
		 		
		 	);
		
	 	return $uri;
	 	
	 }

	 /* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de l'image du produit
	 * @access public
	 * @param int $idproduct
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE; prend la valeur
	 * @param int $index la $index-ième image souhaitée 
	 * @param int $width largeur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de l'image du produit ou false si la $index-ième image n'existe pas.
	 */
	 public static function getProductImageURI( $idproduct, $imageSize = 1, $index = 0, $width = false, $height = false ){

	 	$uri = self::getImageURI( 
	 	
	 		DBUtil::getDBValue( "name_1", "product", "idproduct", intval( $idproduct ) ) . "-p",
	 		"/images/products",
	 		intval( $index ) > 0 ? $idproduct . "." . intval( $index ) : $idproduct,
	 		$imageSize,
	 		$width,
	 		$height
	 		
	 	);

	 	if( $uri == false )
	 		return intval( $index ) ? false : self::getImageURI( 
	 	
		 		"p",
		 		"/images/products",
		 		"default",
		 		$imageSize,
		 		$width,
		 		$height
		 		
		 	);

	 	return $uri;
	 	
	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de la fiche technique du produit
	 * @access public
	 * @param int $idproduct
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE; prend la valeur
	 * @param int $index la $index-ième image souhaitée 
	 * @param int $width largeur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de la fiche technique du produit ou false si la documentation n'existe pas.
	 */
	 public static function getProductDataSheetURI( $idproduct, $imageSize = 1, $index = 0, $width = false, $height = false ){

	 	return self::getImageURI( 
	 	
	 		DBUtil::getDBValue( "name_1", "product", "idproduct", intval( $idproduct ) ) . "-p",
	 		"/images/products",
	 		intval( $index ) ? $idproduct . ".doc." . intval( $index ) : $idproduct . ".doc",
	 		$imageSize,
	 		$width,
	 		$height
	 		
	 	);
	 	
	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de l'image de la référence
	 * @access public
	 * @param int $idarticle
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE; prend la valeur 
	 * @param int $width largeur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de l'image de la référence
	 */
	 public static function getReferenceImageURI( $idarticle, $imageSize = 1, $width = false, $height = false ){
	 	
	 	$rs = DBUtil::query( "SELECT p.name_1, p.idproduct, d.type, d.reference, d.image_index FROM product p, detail d WHERE d.idarticle = '" . intval( $idarticle ) . "' AND p.idproduct = d.idproduct LIMIT 1" );
	 	
	 	if( !$rs->RecordCount() )
	 		return self::getImageURI( 
	 	
		 		"p",
		 		"/images/products",
		 		"default",
		 		$imageSize,
		 		$width,
		 		$height
		 		
		 	);
		 	
	 	switch( $rs->fields( "type" ) ){
	 		
	 		case "destocking" :
	 		case "alternativ" :
	 		case "specific" :
	 			
	 			$uri = self::getImageURI( 
	 	
			 		"reference",
			 		"/images/references",
			 		$idarticle,
			 		$imageSize,
			 		$width,
			 		$height
			 		
			 	);

			 	if( $uri === false )
				 	return self::getImageURI( 
		 	
				 		"p",
				 		"/images/products",
				 		"default",
				 		$imageSize,
				 		$width,
				 		$height
				 		
			 		);
			 	 
			 	return $uri;
			 	
	 		case "catalog" :
	 		default :
	
	 			$uri = self::getImageURI( 
	 	
			 		$rs->fields( "name_1" ) . "-p",
			 		"/images/products",
			 		intval( $rs->fields( "image_index" ) ) ? $rs->fields( "idproduct" ) . "." . intval( $rs->fields( "image_index" ) ) : $rs->fields( "idproduct" ),
			 		$imageSize,
			 		$width,
			 		$height
			 		
			 	);
	 			
			 	if( $uri == false )
			 		return self::getImageURI( 
	 	
				 		"p",
				 		"/images/products",
				 		"default",
				 		$imageSize,
				 		$width,
				 		$height
				 		
				 	);
			 	
			 	return $uri;
			 	
	 	}
	 	
	 }

	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI du logo du fournisseur
	 * @access public
	 * @param int $idsupplier
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE; prend la valeur
	 * @param int $width largeur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de l'image du logo du fournisseur
	 */
	 public static function getSupplierImageURI( $idsupplier, $imageSize = 1, $width = false, $height = false ){

	 	$uri = self::getImageURI( 
	 	
	 		"supplier",
	 		"/images/suppliers",
	 		$idsupplier,
	 		$imageSize,
	 		$width,
	 		$height
	 		
	 	);

	 	if( $uri == false )
	 		return "/images/suppliers/default.jpg";

	 	return $uri;
	 	
	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI du logo du prestataire
	 * @access public
	 * @param int $idprovider
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE; prend la valeur
	 * @param int $width largeur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de l'image du logo du prestataire
	 */
	 public static function getProviderImageURI( $idprovider, $imageSize = 1, $width = false, $height = false ){

	 	$uri = self::getImageURI( 
	 	
	 		"provider",
	 		"/images/providers",
	 		$idprovider,
	 		$imageSize,
	 		$width,
	 		$height
	 		
	 	);

	 	if( $uri == false )
	 		return "/images/providers/default.jpg";

	 	return $uri;
	 	
	 }
	 
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne l'URI de l'image
	 * @access private
	 * @param string $name nom de l'image pour le référencement
	 * @param string $directory chemin absolu du répertoire de l'image par rapport à la racine du site ( ex : '/images/products' )
	 * @param int $basename nom du fichier de l'image, sans extension
	 * @param int $imageSize taille de l'image; peut prendre l'une des valeurs suivantes : URLFactory::$IMAGE_ICON_SIZE, URLFactory::$IMAGE_THUMB_SIZE, URLFactory::$IMAGE_MAX_SIZE, URLFactory::$IMAGE_DEFAULT_SIZE, URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $width largeur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @param int $height hauteur de l'image souhaitée, utilisée que si $imageSize vaut URLFactory::$IMAGE_CUSTOM_SIZE
	 * @return string l'URI de l'image ou false si elle n'existe pas
	 */
	private static function getImageURI( $name, $directory, $basename, $imageSize, $width = false, $height = false ){ 
		$directory = str_replace( "..", ".", $directory );
		
		if( !( $files = glob( dirname( __FILE__ ) . "/..$directory/$basename{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ) )
			return false;
		
		$ext 	= substr( $files[ 0 ], strrpos( $files[ 0 ], "." ) + 1 );
		$file 	= urlencode( $basename );
		$name 	= strtolower( self::rewrite( $name ) );
		$width 	= intval( $width );
		$height = intval( $height );
		
				
		switch( $imageSize ){
			
			case self::$IMAGE_ICON_SIZE: 	return "/$name-$file-icon.$ext";
			case self::$IMAGE_THUMB_SIZE: 	return "/$name-$file-thumb.$ext";
			case self::$IMAGE_MAX_SIZE: 	return "/$name-$file-zoom.$ext";
			case self::$IMAGE_DEFAULT_SIZE: return "/$name-$file.$ext";
			case self::$IMAGE_CUSTOM_SIZE:	return "/$name-$file-{$width}x{$height}.$ext";
            case self::$IMAGE_DEFAULT_CUSTOM_SIZE:	return "/$file.$ext";
			default: 						return "/$name-$file.$ext";
		}
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne le chemin absolu de l'image de la catégorie
	 * @access public
	 * @param int $idcategory
	 * @param int $index la $index-ième image souhaitée
	 * @return string l'URI de l'image de la catégorie ou false si la $index-ième image n'existe pas.
	 */
	public static function getCategoryImagePath( $idcategory, $index = 0 ){
	
		$path = dirname( __FILE__ ) . "/../images/categories/$idcategory";
	  	$path = intval( $index ) ? self::getImagePath( "$path.$index" ) : self::getImagePath( $path );
	
	 	if( $path == false )
	 		return $path ? false : dirname( __FILE__ ) . "/../images/categories/default.jpg";
	
	 	return $path;
	 	
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne le chemin absolu de l'image du produit
	 * @access public
	 * @param int $idproduct
	 * @param int $index la $index-ième image souhaitée
	 * @return string l'URI de l'image du produit ou false si la $index-ième image n'existe pas.
	 */
	public static function getProductImagePath( $idproduct, $index = 0 ){
	
		$path = dirname( __FILE__ ) . "/../images/products/$idproduct";
	  	$path = intval( $index ) ? self::getImagePath( "$path.$index" ) : self::getImagePath( $path );
	
	 	if( $path == false )
	 		return $index ? false : dirname( __FILE__ ) . "/../images/products/default.jpg";
	
	 	return $path;
	 	
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne le chemin absolu de l'image technique du produit
	 * @access public
	 * @param int $idproduct
	 * @param int $index la $index-ième image technique souhaitée
	 * @return string l'URI de l'image technique du produit ou false si l'image technique n'existe pas.
	 */
	public static function getProductDataSheetPath( $idproduct, $index = 0 ){
	
		$path = dirname( __FILE__ ) . "/../images/products/$idproduct.doc";
	  	
		return intval( $index ) ? self::getImagePath( "$path.$index" ) : self::getImagePath( $path );
	
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne le chemin absolu de l'image de la référence
	 * @access public
	 * @param int $idarticle
	 * @return string le chemin absolu de l'image de la référence
	 */
	 public static function getReferenceImagePath( $idarticle ){
	 	
	 	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", intval( $idarticle ) );
	 	
	 	if( !$idproduct ){
	 		
	 		$files = glob( dirname( __FILE__ ) . "/../images/references/" . intval( $idarticle ) . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
	 		
	 		return $files ? $files[ 0 ] : dirname( __FILE__ ) . "/../images/products/default.jpg";
	 		
	 	}
	 		
	 	return self::getProductImagePath( $idproduct );
	 	
	 }

	/* ------------------------------------------------------------------------------------------ */
	/**
	 * Retourne le chemin de l'image
	 * @access private
	 * @param int $path chemin du fichier de l'image, sans extension
	 * @return string le chemin absolu de l'image ou false si elle n'existe pas
	 */
	private static function getImagePath( $path ){ 

		if( !( $files = glob( "$path{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ) )
			return false;
		
		return $files[ 0 ];

	}
	
	/* ------------------------------------------------------------------------------------------ */
	 /**
	  * @deprecated
	  * @access public
	  * @static
	  * @return bool true
	  */
	 public static function getRewriteEngine(){ return true; }

	/* ------------------------------------------------------------------------------------------ */
	
}

?>
