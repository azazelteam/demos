<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ...?
*/

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/User.php" );

class Commercial extends User{
	
	//----------------------------------------------------------------------------
	
	public function __construct( $iduser ){

		$this->iduser 	= intval( $iduser );
		$this->fields 	= array();
		
		//récupérer les informations de l'utilisateur
		
		$rs =& DBUtil::query( "SELECT * FROM `user` WHERE iduser = '{$this->iduser}' LIMIT 1" );
		
		if( $rs === false || !$rs->RecordCount() ){
		
			trigger_error( "Commerical #{$this->iduser} doesn't exist", E_USER_ERROR );
			die();
				
		}
		
		$i = 0;
		while( $i < count( $rs->fields ) ){
			
			$field 		= $rs->FetchField( $i );
			$fieldname 	= $field->name;
	
			if( !in_array( $fieldname, array( "lastupdate", "username" ) ) )
				$this->fields[ $fieldname ] = $rs->fields( $fieldname );
	
			$i++;
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>