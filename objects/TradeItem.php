<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object appels fonction lignes articles
 */
 
interface TradeItem{
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return int la quantité
	 */
	public function getQuantity();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return float le prix unitaire HT avant remise
	 */
	public function getPriceET();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return float le prix unitaire HT après remise
	 */
	public function getDiscountPriceET();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return float le prix unitaire HT avant remise
	 */
	public function getPriceATI();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return float le prix unitaire TTC après remise
	 */
	public function getDiscountPriceATI();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return return float le taux de remise
	 */
	public function getDiscountRate();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return float le taux de TVA à appliquer ( ex : 19.6 )
	 */
	public function getVATRate();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return float le poids unitaire
	 */
	public function getWeight();
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return bool true si le produit est vendu franco, sinon false
	 */
	public function isPostagePaid();
	
	/* ----------------------------------------------------------------------------------------------------- */
	
}

?>