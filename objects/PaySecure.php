<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object paiement SSL CMDP
 */

include_once( dirname( __FILE__ ) . "/../script/CMCIC_HMAC.inc.php");


class PaySecure {
	
	public $CMCIC_Tpe = "";
	public $CtlHmac = "";
	
	// ----------------------------------------------------------------------------
	function __construct(){
	
		if( !@file_exists( dirname( __FILE__ ) . "/../config/CMCIC_config.php" ) ){
		
			trigger_error( "Configuration du TPE absente", E_USER_ERROR );
			die();
			
		}
		
		@include( dirname( __FILE__ ) . "/../config/CMCIC_config.php" );

		if ( !is_array( $CMCIC_CONFIG ) ){
			
			trigger_error( "Configuration du TPE absente", E_USER_ERROR );
			die();
			
		}

		$this->CMCIC_Tpe 	= $CMCIC_CONFIG;               
		$this->CtlHmac		= CMCIC_CtlHmac( $this->CMCIC_Tpe );		
	}	
	
	// ----------------------------------------------------------------------------
	/**
	IN: Paramètres du Tpe
	Champs du formulaire
	OUT: Résultat vérification
	description: Vérifier le MAC et préparer la Reponse
	*/
	
	function TesterHmac($CMCIC_Tpe, $CMCIC_bruteVars )
	{
	   @$php2_fields = sprintf(CMCIC_PHP2_FIELDS, $CMCIC_bruteVars['retourPLUS'], 
	                                              $CMCIC_Tpe["tpe"], 
                                              $CMCIC_bruteVars["date"],
                                              $CMCIC_bruteVars['montant'],
                                              $CMCIC_bruteVars['reference'],
                                              $CMCIC_bruteVars['texte-libre'],
                                               CMCIC_VERSION,
                                              $CMCIC_bruteVars['code-retour']);


	    if ( strtolower($CMCIC_bruteVars['MAC'] ) == CMCIC_hmac($CMCIC_Tpe, $php2_fields) ):
	        $result  = $CMCIC_bruteVars['code-retour'];
	        $receipt = CMCIC_PHP2_MACOK;
	    else: 
	        $result  = 'None';
	        $receipt = CMCIC_PHP2_MACNOTOK.$php2_fields;
	    endif;
	
	    $mnt_lth = strlen($CMCIC_bruteVars['montant'] ) - 3;
	    if ($mnt_lth > 0):
	        $currency = substr($CMCIC_bruteVars['montant'], $mnt_lth, 3 );
	        $amount   = substr($CMCIC_bruteVars['montant'], 0, $mnt_lth );
	    else:
	        $currency = "";
	        $amount   = $CMCIC_bruteVars['montant'];
	    endif;
	
	    return array( "resultatVerifie" => $result ,
	                  "accuseReception" => $receipt ,
	                  "tpe"             => $CMCIC_bruteVars['TPE'],
	                  "reference"       => $CMCIC_bruteVars['reference'],
	                  "texteLibre"      => $CMCIC_bruteVars['texte-libre'],
	                  "devise"          => $currency,
	                  "montant"         => $amount);
	}
	
	//------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Création du formulaire pour le passage sur le serveur du Credit Mutuel
	 */
	
	function CreerFormulaireHmac($Amount , $Order_Reference , $Currency = 'EUR' , $Order_Comment = '' , $Language_Code = 'FR' , $Button_Text = 'Paiement sécurisé'){
	
	
    	// Préparation du lien de retour. Un contexte est ajouté au lien.
    	$Return_Context = "?order_ref=".$Order_Reference;

		//chèque-cadeau et bon de réduction
		
		if( isset( $_REQUEST[ "gift_token_serial" ] ) || isset( $_REQUEST[ "voucher_code" ] ) ){
	
			if( isset( $_REQUEST[ "gift_token_serial" ] ) )
				$Return_Context .= "&gift_token_serial=" . stripslashes( $_REQUEST[ "gift_token_serial" ] );
			
			if( isset( $_REQUEST[ "voucher_code" ] ) )
				$Return_Context .= "&voucher_code=" . stripslashes( $_REQUEST[ "voucher_code" ] );
			
		}
	
		if ($Order_Comment == "") { $Order_Comment .= "-"; }

	    $Order_Date = date("d/m/Y:H:i:s");
	    $Language_2 = substr($Language_Code, 0, 2);
	
	    $PHP1_FIELDS = sprintf(	CMCIC_PHP1_FIELDS, 
	    						"",
								$this->CMCIC_Tpe["tpe"],
								$Order_Date,
								$Amount,
								$Currency,
								$Order_Reference,
								$Order_Comment,
								CMCIC_VERSION,
								$Language_2,
								$this->CMCIC_Tpe['soc']);

  		$keyedMAC = CMCIC_hmac( $this->CMCIC_Tpe, $PHP1_FIELDS );
	

		echo "<html><head></head><body>";  
   		echo sprintf( 	CMCIC_PHP1_FORM, 
   						$this->HtmlEncode( CMCIC_SERVER ),
						$this->HtmlEncode( CMCIC_DIR ),
						$this->HtmlEncode( CMCIC_VERSION ), 
						$this->HtmlEncode( $this->CMCIC_Tpe["tpe"] ),
						$this->HtmlEncode( $Order_Date ),
						$Amount ,
						$this->HtmlEncode( $Currency ),
						$this->HtmlEncode( $Order_Reference ),
						$this->HtmlEncode( $keyedMAC ),
						$this->HtmlEncode( $this->CMCIC_Tpe["retourko"] ),
						$this->HtmlEncode( '' ),
						$this->HtmlEncode( $this->CMCIC_Tpe["retourok"] ),
						$this->HtmlEncode( $Return_Context ),
						$this->HtmlEncode( $this->CMCIC_Tpe["retourko"] ),
						$this->HtmlEncode( $Return_Context ),
						$this->HtmlEncode( $Language_2 ),
						$this->HtmlEncode( $this->CMCIC_Tpe['soc'] ),
						$this->HtmlEncode( $Order_Comment ),
						$this->HtmlEncode( $Button_Text ));
		echo"<script language=\"javascript\" type=\"text/javascript\">document.forms.PaymentRequest.submit();</script>";
		echo "</body></html>";
	}
	
	/**
	 * Forcer une des variables CMCIC_Tpe
	 */
	public function forceConfig( $param , $value ){
		if( isset( $this->CMCIC_Tpe[ $param ] ) && !empty( $value ) )
			$this->CMCIC_Tpe[ "$param" ] = $value;
		else
			return false;
	}
	
	
	//------------------------------------------------------------------------------------------------------------------------------

	
	function HtmlEncode ($data){
	    $SAFE_OUT_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890._-";
	    $encoded_data = "";
	    $result = "";
	    for ($i=0; $i<strlen($data); $i++)
	    {
	        if (strchr($SAFE_OUT_CHARS, $data{$i})) {
	            $result .= $data{$i};
	        }
	        else if (($var = bin2hex(substr($data,$i,1))) <= "7F"){
	            $result .= "&#x" . $var . ";";
	        }
	        else
	            $result .= $data{$i};
	            
	    }
	    return $result;
	}
}
?>