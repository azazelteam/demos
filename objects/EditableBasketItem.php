<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object modèle de ligne article pour le back office
 * @see DefaultBasketItem
 */
 
include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/DefaultBasketItem.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
		
		
abstract class EditableBasketItem extends DefaultBasketItem{
	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * Numéro de la ligne article dans le panier ( doit commencer à 1 pour les ERP )
	 */
	protected $idrow;
	
	//----------------------------------------------------------------------------
	//abstract members
	
	/*
	  Définit le numéro de l'article et ses propriétés
	  @abstract
	  @access protected
	  @param int $idarticle le numéro de l'article
	  @return void
	*/
	//protected abstract function setArticle( $idarticle );
	/*
	  Recalcule les prix de l'article
	  @access protected
	  @abstract
	  @return void
	*/
	//protected abstract function recalculateAmounts();
	/*
	  Modifie la quantité de l'article
	  @abstract
	  @access public
	  @param int $quantity la quantité souhaitée
	  @return void
	*/
	//public abstract function setQuantity( $quantity );
	
	//----------------------------------------------------------------------------
	//members
	
	/**
	 * Charge une ligne article depuis la base de données
	 * @return void
	 */
	 
	public function load(){
		
		$tableName 				= $this->owner->getItemsTableName();
		$ownerPrimaryKey 		= $this->owner->getPrimaryKeyName();
		$ownerPrimaryKeyValue 	= $this->owner->getPrimaryKeyValue();

		$query = "
		SELECT *
		FROM `$tableName`
		WHERE `$ownerPrimaryKey` = '$ownerPrimaryKeyValue'
		AND idrow = '{$this->idrow}'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
		$this->idarticle 	= $rs->fields( "idarticle" );
		$this->idrow 		= $rs->fields( "idrow" );
		
		$this->setLastUpdateInfos();
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Met à jour la ligne article dans la base de données
	 * @return void
	 */
	 
	public function update(){
		
		$tableName 				= $this->owner->getItemsTableName();
		$ownerPrimaryKey 		= $this->owner->getPrimaryKeyName();
		$ownerPrimaryKeyValue 	= $this->owner->getPrimaryKeyValue();

		$this->setLastUpdateInfos();
		
		$query = "
		UPDATE `$tableName`
		SET ";
		
		$i = 0;
		foreach( $this->fields as $fieldname => $value ){
			
			if( $i )
				$query .= ", ";
			
			$query .= "`$fieldname` = " . DBUtil::quote( $value);
			
			$i++;
			
		}
		
		$query .= "
		WHERE `$ownerPrimaryKey` = '$ownerPrimaryKeyValue'
		AND idrow = '{$this->idrow}'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * Insère la ligne article dans la base de données
	 * @return void
	 */
	 
	public function insert(){
		
		$tableName 	= $this->owner->getItemsTableName();
	
		$this->setLastUpdateInfos();
		
		$query = "
		INSERT INTO `$tableName` ( `";
		
		$fieldnames = array_keys( $this->fields );
		
		$query .= implode( "`,`", $fieldnames );
		
		$query .= "` ) VALUES ( ";
		
		$i = 0;
		foreach( $this->fields as $fieldname => $value ){
			
			if( $i )
				$query .= ", ";
				
			$query .= DBUtil::quote( $value );
		
			$i++;
			
		}
		
		$query .= " )";
		
		DBUtil::query( $query );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Supprime la ligne article de la base de données
	 * @todo suppression depuis l'objet propriétaire
	 * @param int $idrow le numéro de la ligne à charger, composant de la clé unique
	 * @param int $ownerPrimaryKeyValue, le numéro devis, cde, facture ou bl...
	 * @return void
	 */
	 
	public function delete(){
		
		$tableName 				= $this->owner->getItemsTableName();
		$ownerPrimaryKey 		= $this->owner->getPrimaryKeyName();
		$ownerPrimaryKeyValue 	= $this->owner->getPrimaryKeyValue();

		$query = "
		DELETE FROM `$tableName` 
		WHERE `$ownerPrimaryKey` = '$ownerPrimaryKeyValue' 
		AND idrow = '{$this->idrow}' 
		LIMIT 1";
		
		DBUtil::query( $query );
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Modifie l'identifiant de la ligne article
	 * @todo : pas de gestion de idrow dans les objets ( attention lors de la suppression d'un article, il faut réactualiser tous les idrow, bof...)
	 * => que ce soit automatique lors de la sauvegarde
	 */
	public function setIdRow( $idrow ){
	
		$this->idrow = $idrow;
		$this->setValue( "idrow", $idrow );
			
	}
	
	//----------------------------------------------------------------------------
	
	private final function setLastUpdateInfos(){
	
		$this->fields[ "lastupdate" ] = date( "Y-m-d H:i:s" );
		
		if( User::getInstance()->getId() )
			$username = User::getInstance()->getValue( "login" );
		else if( isset( $_SESSION[ "buyerinfo" ] ) && isset( $_SESSION[ "buyerinfo" ][ "login"] ) )
			$username = $_SESSION[ "buyerinfo" ][ "login"];
		else $username = "guest";
		
		$this->fields[ "username" ] = $username;
			
	}
	
	//----------------------------------------------------------------------------
	
	public function getCatalogValue( $key ){
		
		if( !$this->idarticle )
			return false;
			
		$rs =& DBUtil::query( "SELECT `$key` AS `value` FROM `detail` WHERE idarticle = '{$this->idarticle}' LIMIT 1" );
		
		if( !$rs->RecordCount() )
			return false;
			
		return $rs->fields( "value" );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>