<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object calcul marges
 */
 
abstract class TradeUtil{
	
	//----------------------------------------------------------------------------
	/**
	  * Calcul le montant de la marge nette d'un devis
	  * @static
	  * @param int $idestimate le numéro du devis
	  * @return mixed le montant de la marge nette ou false en cas d'erreur
	  */
	 public static function getEstimateNetMarginAmount( $idestimate ){
		
		$selling_price = 0.0;
		$charges = 0.0;
		$purchase_price = 0.0;
		
		//vente
			
		
		 if( B2B_STRATEGY ){ 
			$query = "
		SELECT SUM( erow.discount_price * erow.quantity) AS discount_price,
			SUM( erow.unit_cost_amount * erow.quantity ) AS purchase_price,
			e.total_discount, e.total_charge_ht
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
			 
			  } 
				else{ 
			$query = "
		SELECT SUM(( erow.discount_price * erow.quantity) / ( 1.00 + vat_rate / 100.00 )) AS discount_price,
			SUM( erow.unit_cost_amount * erow.quantity ) AS purchase_price,
			e.total_discount, e.total_charge_ht
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
	         } 
			 
		
		
		$rs = & DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de calculer la marge nette pour le devis '$idestimate'" );
			
		if( !$rs->RecordCount() )
			return false;
		 if( B2B_STRATEGY ){ 
			 $selling_price = ($rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100)) + $rs->fields( "total_charge_ht" );
			  } 
				else{ 
			 $selling_price = ($rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100)) + $rs->fields( "total_charge_ht" );				
			//$selling_price = ($rs->fields( "discount_price" )/ ( 1.00 + vat_rate / 100.00 )*(1-$rs->fields( "total_discount" )/100)) + $rs->fields( "total_charge_ht" );
	         } 

		$purchase_price = $rs->fields( "purchase_price" );
		
		//Coûts Ports
			
		$queryoc = "SELECT SUM( charges + internal_supplier_charges ) as charges
					FROM estimate_charges
					WHERE idestimate = '$idestimate'";
		$rsoc = & DBUtil::query( $queryoc );
			
		if( $rsoc === false )
			die( "Impossible de récupérer les couts ports du devis '$idestimate'" ); 
				
		$charges = $rsoc->fields( "charges" );
					
		
		//marge nette
		
		$netMarginAmount = $selling_price - ($purchase_price + $charges );
		
		return $netMarginAmount;
	 
	 }
	 
	//------------------------------------------------------------------------------------------------------------
	 /**
	  * Calcul le pourcentage de marge nette d'un devis donné
	  * @static
	  * @param int $idestimate le numéro du devis
	  * @return mixed le pourcentage de marge nette ou false en cas d'erreur
	  */
	 public static function getEstimateNetMarginRate( $idestimate ){

		//montant marge nette
		
		$netMarginAmount = self::getEstimateNetMarginAmount( $idestimate );
		
		if( $netMarginAmount === false )
			return false;
		
		//devis
		
		//recalcul de total_amount_ht

		
		 if( B2B_STRATEGY ){ 
		 $query = "
			 SELECT SUM( CONVERT( erow.discount_price * erow.quantity, DECIMAL(11,2) ) ) AS discount_price,
			 e.total_discount, e.total_charge_ht
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
			  } 
				else{ 
		$query = "		
			SELECT SUM( CONVERT( erow.discount_price * erow.quantity / ( 1.00 + vat_rate / 100.00 ), DECIMAL(11,2) ) ) AS discount_price,
			e.total_discount, e.total_charge_ht
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
	         } 
			
			
		
		$rs = & DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de calculer la marge nette pour le devis '$idestimate'" );
			
		if( !$rs->RecordCount() )
			return false;
		
		$selling_price = ($rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100)) + $rs->fields( "total_charge_ht" );
			
		$netMarginRate = $selling_price > 0.0 ? $netMarginAmount / $selling_price * 100 : 0.0;
		
		return round( $netMarginRate, 2 );
		
	 }
	 
	//------------------------------------------------------------------------------------------------------------
	/**
	  * Calcul le montant de la marge brute d'un devis
	  * @static
	  * @param int $idestimate le numéro du devis
	  * @return mixed le montant de la marge brute ou false en cas d'erreur
	  */
	 public static function getEstimateGrossMarginAmount( $idestimate ){
		
		//devis
		
				
		 if( B2B_STRATEGY ){ 
		 $query = "
			 SELECT SUM( erow.discount_price * erow.quantity) AS discount_price,
			 SUM( erow.unit_cost_amount * erow.quantity ) AS purchase_price,
			e.total_discount
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
			  } 
				else{ 
		$query = "
			SELECT SUM( erow.discount_price * erow.quantity / ( 1.00 + vat_rate / 100.00 )) AS discount_price,
			SUM( erow.unit_cost_amount * erow.quantity ) AS purchase_price,
			e.total_discount
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
	         } 
		
			
	
		$rs = & DBUtil::query( $query );
	
		if( $rs === false )
			die( "Impossible de calculer la marge brute pour le devis '$idestimate'" );
		
		if( !$rs->RecordCount() )
			return false;
	
		$estimate_amount = $rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100);
		
		//marge brute
		
		$rough_stroke_amount = $estimate_amount - $rs->fields("purchase_price");
		
		return $rough_stroke_amount;
	 
	 }
	
	//------------------------------------------------------------------------------------------------------------
	 /**
	  * Calcul le pourcentage de marge brute d'un devis donné
	  * @static
	  * @param int $idestimate le numéro du devis
	  * @return mixed le pourcentage de marge brute ou false en cas d'erreur
	  */
	 
	 public static function getEstimateGrossMarginRate( $idestimate ){
	 	
		//marge brute
		
		$rough_stroke_amount = self::getEstimateGrossMarginAmount( $idestimate );

		//devis
		
		
		 if( B2B_STRATEGY ){ 
		 $query = "
			 SELECT SUM( erow.discount_price * erow.quantity) AS discount_price,
			 SUM( erow.unit_cost_amount * erow.quantity ) AS purchase_price,
			e.total_discount
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
			  } 
				else{ 
		$query = "		
			SELECT SUM(( erow.discount_price * erow.quantity) / ( 1.00 + vat_rate / 100.00 )) AS discount_price,
			SUM( erow.unit_cost_amount * erow.quantity ) AS purchase_price,
			e.total_discount
		FROM estimate_row erow, estimate e
		WHERE e.idestimate = '$idestimate'
		AND e.idestimate = erow.idestimate
		GROUP BY e.idestimate";
	         } 
		
		
			
		
		$rs = & DBUtil::query( $query );
	
		if( $rs === false )
			die( "Impossible de calculer la marge brute pour le devis '$idestimate'" );
		
				
		if( !$rs->RecordCount() )
			return false;
				 if( B2B_STRATEGY ){ 
			 $estimate_amount = $rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100);
			  } 
				else{ 
			$estimate_amount = $rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100)/ ( 1.00 + vat_rate / 100.00 );
	         } 
		
		
		// % marge brute
			
		$rough_stroke_rate = $estimate_amount > 0.0 ? $rough_stroke_amount / $estimate_amount * 100.0 : 0.0;
		
		return round( $rough_stroke_rate, 2 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	  * Calcul le montant de la marge nette d'une commande
	  * @static
	  * @param int $idorder
	  * @return float le montant de la marge nette
	  */
	 public static function getOrderNetMarginAmount( $idorder ){

		if( is_array( $idorder ) ){
			
			$netMargin = 0.0;
			
			foreach( $idorder as $id )
				$netMargin += self::getOrderNetMarginAmount( $id );
				
			return $netMargin;
			
	 	}

	 	if( !$idorder )
	 		return 0.0;
	 		
		//coût port
			
		$query = "
		SELECT SUM( charges + internal_supplier_charges ) as charges
		FROM order_charges
		WHERE idorder = '$idorder'";
		
		$rs =& DBUtil::query( $query );
					
		$charges = $rs->fields( "charges" );
		
		//achats
	
		$query = "
		SELECT SUM( osr.discount_price * osr.quantity )  AS discount_price, os.total_discount
		FROM order_supplier_row osr, `order_supplier` os
		WHERE os.idorder = '$idorder'
		AND os.idorder_supplier = osr.idorder_supplier
		GROUP BY os.idorder_supplier";
	
		$rs =& DBUtil::query( $query );
		
		$purchase_price = 0.0;
		while( !$rs->EOF() ){
		
			//commande client avec commandes fournisseur
			 if( B2B_STRATEGY ){ 
			 $purchase_price += $rs->fields( "discount_price" ) * ( 1.0 - $rs->fields( "total_discount" ) / 100.0 );
			  } 
				else{ 
			 $purchase_price += $rs->fields( "discount_price" ) * ( 1.0 - $rs->fields( "total_discount" ) / 100.0 );
			//$purchase_price += $rs->fields( "discount_price" ) * ( 1.0 - $rs->fields( "total_discount" ) / 100.0 ) / ( 1.00 + vat_rate / 100.00 );
	         } 
			
			
			$rs->MoveNext();
		
		}
		
		//commande client sans commande fournisseur @todo : synchro
		
		if( !$rs->RecordCount() ){

				
			
			 $query = "
			 SELECT SUM( orow.unit_cost_amount * orow.quantity ) AS discount_price
			 FROM order_row orow
			WHERE orow.idorder = '$idorder'";
						 
			
			$purchase_price = DBUtil::query( $query )->fields( "discount_price" );

		}

		//avoirs client
		
		/*$query = "
		SELECT COALESCE( SUM( cr.discount_price * cr.quantity ), 0.0 ) + c.credited_charges - c.total_charge_ht AS creditAmount
		FROM bl_delivery bl, credits c
		LEFT JOIN credit_rows cr ON cr.idcredit = c.idcredit
		WHERE bl.idorder = '$idorder'
		AND bl.idbilling_buyer = c.idbilling_buyer";
		
		$rs =& DBUtil::query( $query );
		
		$customerCreditAmount = $rs->RecordCount() ? $rs->fields( "creditAmount" ) : 0.0;
		
		//avoirs fournisseur
		
		$query = "
		SELECT cs.amount, COALESCE( bs.vat_rate, 0.0 ) AS vat_rate
		FROM credit_supplier cs, billing_supplier_row bsr, bl_delivery bl, billing_supplier bs
		WHERE cs.billing_supplier = bs.billing_supplier
		AND cs.idsupplier = bs.idsupplier
		AND bs.billing_supplier = bsr.billing_supplier
		AND bs.idsupplier = bsr.idsupplier
		AND bsr.idbl_delivery = bl.idbl_delivery
		AND bl.idorder = '$idorder'
		GROUP BY cs.billing_supplier, cs.idsupplier";
		
		$rs =& DBUtil::query( $query );
		
		$supplierCreditAmount = 0.0;
		
		while( !$rs->EOF() ){

			$supplierCreditAmount += $rs->fields( "amount" ) / ( 1.0 + $rs->fields( "vat_rate" ) / 100.0 );

			$rs->MoveNext();
			
		}
		*/
		//marge nette
		
		return self::getOrderNetAmount( $idorder ) - $purchase_price - $charges/* - $customerCreditAmount + $supplierCreditAmount*/;
	 
	 }
	
	//----------------------------------------------------------------------------
	/**
	  * Calcul le pourcentage de marge nette d'une commande donnée
	  * @static
	  * @param int $idorder le numéro de la commande
	  * @return mixed le pourcentage de marge nette ou false en cas d'erreur
	  */ 
	 public static function getOrderNetMarginRate( $idorder ){
		
		if( is_array( $idorder ) ){
			
			$selling_price = 0.0;
			
			foreach( $idorder as $id )
				$selling_price += self::getOrderNetAmount( $id );
		
		}
		else $selling_price = self::getOrderNetAmount( $idorder );

		return $selling_price > 0.0 ? round( self::getOrderNetMarginAmount( $idorder ) / $selling_price * 100.0, 2 ) : 0.0;

	 }
	 
	//----------------------------------------------------------------------------
	/**
	 * @access protected
	 * @param int $idorder
	 * @return float
	 */
	protected static function getOrderNetAmount( $idorder ){
		

		
		 if( B2B_STRATEGY ){ 
		$query = "		 
			 SELECT SUM(( orow.discount_price * orow.quantity ) / ( 1.0 + o.total_discount / 100.0 )) + o.total_charge_ht AS amount
			 		FROM order_row orow, `order` o
		WHERE o.idorder = '$idorder'
		AND o.idorder = orow.idorder
		GROUP BY o.idorder";
			  } 
				else{ 
			
		$query = "				
			SELECT SUM((( orow.discount_price * orow.quantity ) / ( 1.0 + o.total_discount / 100.0 )) / ( 1.00 + vat_rate / 100.00 ))+ o.total_charge_ht AS amount
					FROM order_row orow, `order` o
		WHERE o.idorder = '$idorder'
		AND o.idorder = orow.idorder
		GROUP BY o.idorder";
		
	         } 
			 
		
		$rs =& DBUtil::query( $query );
		
		return $rs->fields( "amount" );
		
	}

	//----------------------------------------------------------------------------
	/**
	  * Calcul le montant de la marge brute d'une commande
	  * @static
	  * @param int $idorder
	  * @return float le montant de la marge brute
	  */ 
	 public static function getOrderGrossMarginAmount( $idorder ){
		
		if( is_array( $idorder ) ){
			
			$roughStrokeAmount = 0.0;
			
			foreach( $idorder as $id )
				$roughStrokeAmount += self::getOrderGrossMarginAmount( $id );
				
			return $roughStrokeAmount;
			
	 	}

	 	if( !$idorder )
	 		return 0.0;
	 		
		//commandes fournisseur
	
		$query = "
		SELECT SUM( osr.discount_price * osr.quantity ) / ( 1.0 + os.total_discount / 100.0 ) AS amount
		FROM order_supplier_row osr, `order_supplier` os
		WHERE os.idorder = '$idorder'
		AND os.idorder_supplier = osr.idorder_supplier
		GROUP BY os.idorder_supplier";
	
		$rs =& DBUtil::query( $query );
	
		//commande client avec commandes fournisseur
		
		$order_supplier_amount = 0.0;
		while( !$rs->EOF() ){
		
			$order_supplier_amount += $rs->fields( "amount" );

			$rs->MoveNext();
	
		}
		
		if( !$rs->RecordCount() ){ //commande client sans commande fournisseur @todo : synchro
		
			
			$query = "			 
			 SELECT SUM( orow.unit_cost_amount * orow.quantity ) AS discount_price
			FROM order_row orow
			WHERE orow.idorder = '$idorder'";			 
			 
			
			$order_supplier_amount = DBUtil::query( $query )->fields( "discount_price" );
		
		}
	
	 	//avoirs client
		
		/*$query = "
		SELECT COALESCE( SUM( cr.discount_price * cr.quantity ), 0.0 ) AS creditAmount
		FROM bl_delivery bl, credits c
		LEFT JOIN credit_rows cr ON cr.idcredit = c.idcredit
		WHERE bl.idorder = '$idorder'
		AND bl.idbilling_buyer = c.idbilling_buyer";
		
		$rs =& DBUtil::query( $query );
		
		$customerCreditAmount = $rs->RecordCount() ? $rs->fields( "creditAmount" ) : 0.0;
		
		//avoirs fournisseur
		
		$query = "
		SELECT cs.amount, COALESCE( bs.vat_rate, 0.0 ) AS vat_rate
		FROM credit_supplier cs, billing_supplier_row bsr, bl_delivery bl, billing_supplier bs
		WHERE cs.billing_supplier = bs.billing_supplier
		AND cs.idsupplier = bs.idsupplier
		AND bs.billing_supplier = bsr.billing_supplier
		AND bs.idsupplier = bsr.idsupplier
		AND bsr.idbl_delivery = bl.idbl_delivery
		AND bl.idorder = '$idorder'
		GROUP BY cs.billing_supplier, cs.idsupplier";
		
		$rs =& DBUtil::query( $query );
		
		$supplierCreditAmount = 0.0;
		
		while( !$rs->EOF() ){

			$supplierCreditAmount += $rs->fields( "amount" ) / ( 1.0 + $rs->fields( "vat_rate" ) / 100.0 );

			$rs->MoveNext();
			
		}
		*/
		//marge brute
		
		$rough_stroke_amount = self::getOrderRoughAmount( $idorder ) - $order_supplier_amount/* - $customerCreditAmount + $supplierCreditAmount*/;
		
		return $rough_stroke_amount;
	 
	 }
	 
	 //----------------------------------------------------------------------------
	/**
	  * Calcul le pourcentage de la marge brute d'une commande ou plusieurs commandes
	  * @static
	  * @param int $idorder
	  * @return mixed le pourcentage de la marge brute ou false en cas d'erreur
	  */
	 public static function getOrderGrossMarginRate( $idorder ){
		
		if( is_array( $idorder ) ){
			
			$order_amount = 0.0;
			
			foreach( $idorder as $id )
				$order_amount += self::getOrderRoughAmount( $id );
		
		}
		else $order_amount = self::getOrderRoughAmount( $idorder );
		
	
		return $order_amount > 0.0 ? round( self::getOrderGrossMarginAmount( $idorder ) / $order_amount * 100.0, 2 ) : 0.0;
	 
	 }
	 
	 //----------------------------------------------------------------------------
	/**
	 * @access protected
	 * @param int $idorder
	 * @return float
	 */
	protected static function getOrderRoughAmount( $idorder ){
	
		 if( B2B_STRATEGY ){ 
		$query = "		 
			 SELECT SUM(( orow.discount_price * orow.quantity ) / ( 1.0 + o.total_discount / 100.0 ))AS amount
			 		FROM order_row orow, `order` o
		WHERE o.idorder = '$idorder'
		AND o.idorder = orow.idorder
		GROUP BY o.idorder";
			  } 
				else{ 
		$query = "				
			SELECT SUM((( orow.discount_price * orow.quantity ) / ( 1.0 + o.total_discount / 100.0 )) / ( 1.00 + vat_rate / 100.00 ) )AS amount
					FROM order_row orow, `order` o
		WHERE o.idorder = '$idorder'
		AND o.idorder = orow.idorder
		GROUP BY o.idorder";
	         } 
		

		
		$rs =& DBUtil::query( $query );
		
		return $rs->fields( "amount" );
		
	}

	//----------------------------------------------------------------------------
	/**
	  * Calcul le montant de la marge brute d'une facture
	  * @todo
	  * @static
	  * @param int $idbilling_buyer le numéro de la facture en franglish
	  * @return float le montant de la marge brute ou false en cas d'erreur
	  */
	 public static function getInvoiceGrossMarginAmount( $idbilling_buyer ){
	 	
	 	//@todo pouvoir calculer les marges pour les factures partielles
	 	
	 	include_once( dirname( __FILE__ ) . "/Invoice.php" );
	 	
	 	if( Invoice::isPartialInvoice( $idbilling_buyer ) ) //@todo : pas possible di calculer li marges corrictiment
			return 0.0;
		
		//commandes fournisseur
		
		$query = "
		SELECT os.idorder_supplier,
			os.billing_amount,
			os.total_discount_amount,
			SUM( osr.discount_price * osr.quantity ) AS discount_price
		FROM bl_delivery bl, order_supplier os, order_supplier_row osr
		WHERE  bl.idbilling_buyer = '$idbilling_buyer'
		AND bl.idorder_supplier = os.idorder_supplier
		AND os.idorder_supplier = osr.idorder_supplier
		GROUP BY osr.idorder_supplier";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			return false;
			
		$discount_price = 0.0;
		$billing_amount = 0.0;
		$total_discount_amount = 0.0;
		while( !$rs->EOF() ){
			
			$discount_price 		+= $rs->fields( "discount_price" );
			$billing_amount 		+= $rs->fields( "billing_amount" );
			$total_discount_amount 	+= $rs->fields( "total_discount_amount" );
			
			$rs->MoveNext();
				
		}
		
		//commande
		
		$query = "
		SELECT o.total_amount_ht
		FROM `order` o, billing_buyer bb
		WHERE bb.idbilling_buyer = '$idbilling_buyer'
		AND bb.idorder = o.idorder
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			return false;
		
		$total_amount_ht = $rs->fields( "total_amount_ht" );

	 	//avoirs client
		
		/*$query = "
		SELECT COALESCE( SUM( cr.discount_price * cr.quantity ), 0.0 ) AS creditAmount
		FROM credits c
		LEFT JOIN credit_rows cr ON cr.idcredit = c.idcredit
		WHERE c.idbilling_buyer = '$idbilling_buyer'";
		
		$rs =& DBUtil::query( $query );
		
		$customerCreditAmount = $rs->RecordCount() ? $rs->fields( "creditAmount" ) : 0.0;
		
		//avoirs fournisseur
		
		$query = "
		SELECT cs.amount, COALESCE( bs.vat_rate, 0.0 ) AS vat_rate
		FROM credit_supplier cs, billing_supplier_row bsr, bl_delivery bl, billing_supplier bs
		WHERE cs.billing_supplier = bs.billing_supplier
		AND cs.idsupplier = bs.idsupplier
		AND bs.billing_supplier = bsr.billing_supplier
		AND bs.idsupplier = bsr.idsupplier
		AND bsr.idbl_delivery = bl.idbl_delivery
		AND bl.idbilling_buyer = '$idbilling_buyer'
		GROUP BY cs.billing_supplier, cs.idsupplier";
		
		$rs =& DBUtil::query( $query );
		
		$supplierCreditAmount = 0.0;
		
		while( !$rs->EOF() ){

			$supplierCreditAmount += $rs->fields( "amount" ) / ( 1.0 + $rs->fields( "vat_rate" ) / 100.0 );

			$rs->MoveNext();
			
		}
		*/
		//marge brute
		
		$rough_stroke = $total_amount_ht - $discount_price + $billing_amount + $total_discount_amount/* + $supplierCreditAmount - $customerCreditAmount*/;
		
		return $rough_stroke;
			
	 }
	 
 	//----------------------------------------------------------------------------
	/**
	  * Calcul le pourcentage de la marge brute
	  * @static
	  * @param int $idbilling_buyer
	  * @return mixed le pourcentage de la marge brute ou false en cas d'erreur
	  */
	 public static function getInvoiceGrossMarginRate( $idbilling_buyer ){
		
		if( is_array( $idbilling_buyer ) ){
			
			$invoice_amount = 0.0;
			
			foreach( $idorder as $id )
				$invoice_amount += self::getInvoiceRoughAmount( $id );
		
		}
		else $invoice_amount = self::getInvoiceRoughAmount( $idbilling_buyer );
		
	
		return $invoice_amount > 0.0 ? round( self::getInvoiceGrossMarginAmount( $idbilling_buyer ) / $invoice_amount * 100.0, 2 ) : 0.0;
	 
	 }
	 
	//----------------------------------------------------------------------------
	/**
	 * @access protected
	 * @param int $idorder
	 * @return float
	 */
	protected static function getInvoiceRoughAmount( $idbilling_buyer ){
		
		$query = "
		SELECT SUM( bbr.discount_price * bbr.quantity ) * ( 1.0 - bb.total_discount / 100.0 ) AS amount
		FROM billing_buyer_row bbr, `billing_buyer` bb
		WHERE bb.idbilling_buyer = '$idbilling_buyer'
		AND bb.idbilling_buyer = bbr.idbilling_buyer
		GROUP BY bb.idbilling_buyer";
	
		return DBUtil::query( $query )->fields( "amount" );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	  * Calcul le montant de la marge nette d'une facture
	  * @todo
	  * @static
	  * @param int $idbilling_buyer le numéro de la facture en franglish
	  * @return float le montant de la marge nette ou false en cas d'erreur
	  */
	 public static function getInvoiceNetMarginAmount( $idbilling_buyer ){
	 
	 	//@todo pouvoir calculer les marges pour les factures partielles
	 	
	 	include_once( dirname( __FILE__ ) . "/Invoice.php" );
	 	
	 	if( Invoice::isPartialInvoice( $idbilling_buyer ) ) //@todo : pas possible di calculer li marges corrictiment
			return 0.0;
		
		//facture
	
		$query = "
		SELECT total_amount_ht
		FROM billing_buyer
		WHERE idbilling_buyer = '$idbilling_buyer'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de calculer la marge nette pour la facture '$idbilling_buyer'" );
			
		if( !$rs->RecordCount() )
			return false;
			
		$invoice_total_amount_ht = $rs->fields( "total_amount_ht" );
		
		//commande fournisseur
		
		$query = "
		SELECT SUM( osr.discount_price * osr.quantity ) AS discount_price,
			os.total_charge_ht,
			os.total_discount_amount,
			os.billing_amount,
			os.charge_carrier_cost,
			os.charge_carrier_sell
		FROM billing_buyer bb, billing_buyer_row bbr, order_supplier_row osr, `order_supplier` os
		WHERE bb.idbilling_buyer = '$idbilling_buyer'
		AND bb.idorder <> 0
		AND os.idorder = bb.idorder
		AND os.idorder_supplier = osr.idorder_supplier
		AND bb.idbilling_buyer = bbr.idbilling_buyer
		AND bbr.idorder_row = osr.idorder_row
		GROUP BY os.idorder_supplier";

		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de calculer la marge nette pour la facture '$idorder'" );
		
		$order_supplier_discount_price 			= 0.0;
		$order_supplier_total_discount_amount 	= 0.0;
		$order_supplier_total_charge_ht 		= 0.0;
		$order_supplier_billing_amount 			= 0.0;
		$order_supplier_charge_carrier_cost 	= 0.0;
		$order_supplier_charge_carrier_sell 	= 0.0;
			
		while( !$rs->EOF() ){
		
			$order_supplier_discount_price 			+= $rs->fields( "discount_price" );
			$order_supplier_total_discount_amount 	+= $rs->fields( "total_discount_amount" );
			$order_supplier_total_charge_ht 		+= $rs->fields( "total_charge_ht" );
			$order_supplier_billing_amount 			+= $rs->fields( "billing_amount" );
			$order_supplier_charge_carrier_cost 	+= $rs->fields( "charge_carrier_cost" );
			$order_supplier_charge_carrier_sell 	+= $rs->fields( "charge_carrier_sell" );
		
			$rs->MoveNext();
			
		}
		
		//avoirs client
		
		/*$query = "
		SELECT COALESCE( SUM( cr.discount_price * cr.quantity ), 0.0 ) + c.credited_charges - c.total_charge_ht AS creditAmount
		FROM credits c
		LEFT JOIN credit_rows cr ON cr.idcredit = c.idcredit
		WHERE c.idbilling_buyer = '$idbilling_buyer'";
		
		$rs =& DBUtil::query( $query );
		
		$customerCreditAmount = $rs->RecordCount() ? $rs->fields( "creditAmount" ) : 0.0;
		
		//avoirs fournisseur
		
		$query = "
		SELECT cs.amount, COALESCE( bs.vat_rate, 0.0 ) AS vat_rate
		FROM credit_supplier cs, billing_supplier_row bsr, bl_delivery bl, billing_supplier bs
		WHERE cs.billing_supplier = bs.billing_supplier
		AND cs.idsupplier = bs.idsupplier
		AND bs.billing_supplier = bsr.billing_supplier
		AND bs.idsupplier = bsr.idsupplier
		AND bsr.idbl_delivery = bl.idbl_delivery
		AND bl.idbilling_buyer = '$idbilling_buyer'
		GROUP BY cs.billing_supplier, cs.idsupplier";
		
		$rs =& DBUtil::query( $query );
		
		$supplierCreditAmount = 0.0;
		
		while( !$rs->EOF() ){

			$supplierCreditAmount += $rs->fields( "amount" ) / ( 1.0 + $rs->fields( "vat_rate" ) / 100.0 );

			$rs->MoveNext();
			
		}
		*/
		//marge nette
		
		$netMarginAmount = $invoice_total_amount_ht;
		$netMarginAmount -= $order_supplier_discount_price + $order_supplier_total_charge_ht - $order_supplier_total_discount_amount - $order_supplier_billing_amount;
		/*$netMarginAmount += $supplierCreditAmount - $customerCreditAmount;*/
		//$netMarginAmount -= $order_supplier_charge_carrier_cost + $order_supplier_charge_carrier_sell;
		
		return $netMarginAmount;
	 
	 }
	 
	//----------------------------------------------------------------------------
	 
	 /**
	  * Calcul le pourcentage de marge nette d'une facture
	  * @todo
	  * @static
	  * @param int $idbilling_buyer le numéro de la facture en franglish
	  * @return float le pourcentage de marge nette ou false en cas d'erreur
	  */
	 public static function getInvoiceNetMarginRate( $idbilling_buyer ){
	 
	 	include_once( dirname( __FILE__ ) . "/Invoice.php" );
	 	
	 	if( Invoice::isPartialInvoice( $idbilling_buyer ) ) //pas possible di calculer li marges corrictiment
			return 0.0;
	 	
		//montant marge nette
		
		$netMarginAmount = self::getInvoiceNetMarginAmount( $idbilling_buyer );
		
		if( $netMarginAmount === false )
			return false;
		
		//pourcentage marge nette

	 	$query = "
		SELECT total_amount_ht
		FROM `billing_buyer`
		WHERE idbilling_buyer = '$idbilling_buyer'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de calculer la marge nette pour la facture '$idbilling_buyer'" );
			
		if( !$rs->RecordCount() )
			return false;
			
		$total_amount_ht = $rs->fields( "total_amount_ht" );
		
		$netMarginRate = $total_amount_ht > 0.0 ? $netMarginAmount / $total_amount_ht * 100 : 0.0;
		
		return round( $netMarginRate, 2 );
		
	 }
	 
	//----------------------------------------------------------------------------
	
}

?>