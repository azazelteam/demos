<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object cryptage et decryptage de données
 */
 
abstract class Encryptor{
	
	//----------------------------------------------------------------------------
	
	/**
	 * var string $DEFAULT_CIPHERS
	 * algorithme de cryptage par défaut
	 */
	public static $DEFAULT_CIPHERS = MCRYPT_DES;
	
	/**
	 * var string $DEFAULT_MODE
	 * module de cryptage par défaut
	 */
	public static $DEFAULT_MODE = MCRYPT_MODE_ECB;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Initialise le module de chiffrement
	 * access private
	 * static
	 * param string $key clé de cryptage
	 * return resource une référence vers un pointeur de cryptage
	 */
	private static function &init( $key ){
		
		//charger le chiffrement

		$td = mcrypt_module_open( self::$DEFAULT_CIPHERS, "", self::$DEFAULT_MODE, "" );
		
		if( $td === false )
			return false;
			
		//initialisation du générateur de nombres aléatoires
		
		srand();
		
		//vecteur d'initialisation et taille maximale de la clé
		
		$iv = mcrypt_create_iv( mcrypt_enc_get_iv_size( $td ), MCRYPT_RAND ); //windows ne supporte que MCRYPT_RAND
		$ks = mcrypt_enc_get_key_size( $td );
		
		//clé de chiffrement
		
		$key = substr( md5( $key ), 0, $ks );
		
		//initialisation du chiffrement
		
		$ret = mcrypt_generic_init( $td, $key, $iv );
		
		switch( $ret ){
			
			case -3 :		//taille de la clé incorrecte
			case -4 : 		//problème d'allocation de mémoire
			case false : 	//mauvais usage
			
				$ret = false; 
				
			default : 
			
				$ret = $td;
			
		}
		
		return $ret;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Libère le gestionnaire de chiffrement et ferme le module de cryptage
	 * Le pointeur de cryptage est détruit après fermeture du module et ne peut être réutilisé
	 * access private
	 * static
	 * param resource $td pointeur de cryptage
	 * return void
	 */
	private static function deinit( &$td ){
		
		if( mcrypt_generic_deinit( $td ) === false )
			return false;
			
		if( mcrypt_module_close( $td ) === false )
			return false;
		
		$td = null;
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crypte une chaîne donnée avec une clé donnée
	 * access public 
	 * static
	 * param string $data les données à crypter
	 * param $key la clé de cryptage
	 * return mixed la chaîne $data cryptée avec la clé $key en cas de succès, sinon false
	 */
	public static function encrypt( $data, $key ){
		
		if( !strlen( $key ) )
			trigger_error( "Encryption key is empty", E_USER_ERROR );
			
		$td =& self::init( $key );
		
		if( $td === false )
			return false;
			
		$encrypted = mcrypt_generic( $td, $data );
		
		if( self::deinit( $td ) === false )
			return false;
		
		return $encrypted;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Décrypte une chaîne donnée avec une clé donnée
	 * access public 
	 * static
	 * param string $data les données à crypter
	 * param $key la clé de cryptage
	 * return mixed la chaîne $data décryptée avec la clé $key en cas de succès, sinon false
	 */
	public static function decrypt( $data, $key ){
		
		if( !strlen( $key ) )
			trigger_error( "Encryption key is empty", E_USER_ERROR );
			
		$td =& self::init( $key );
		
		if( $td === false )
			return false;
			
		$decrypted = mdecrypt_generic( $td, $data );
		
		if( self::deinit( $td ) === false )
			return false;
		
		return trim( $decrypted );
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>