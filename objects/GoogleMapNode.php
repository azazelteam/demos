<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object noeud de l'arborescence XML pour le classe GoogleMap
 */
 

 include_once( "GoogleMap.php" );
 
 class GoogleMapNode{
 	 	
 	//-------------------------------------------------------------------------------------------
	
 	private $name;
 	private $uri;
 	private $getLastModificationMethod;
 	
 	private $loc;
 	private $lastmod;
 	private $changeFreq;
 	private $priority;
 	
 	//-------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $name le nom du noeud
	 * @param string $uri l'URI du noeud
	 * @param $owner GoogleMap
	 */
	public function GoogleMapNode( $name, $uri, GoogleMap &$owner ){
	
		global $GLOBAL_START_URL;
		
		$this->name = $name;
		$this->uri = $uri;
		$this->getLasModificationMethod = GoogleMap::$LAST_MOD_CURRENTDATE;

		$this->loc = $GLOBAL_START_URL . $uri;
		$this->lastmod = date( "Y-m-d" );
		$this->changeFreq = GoogleMap::$CHANGE_FREQ_ALWAYS;
		
		$this->priority = 0.5;
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nom du noeud
	 * @return string
	 */
	function getName(){ return $this->name; }
	
	/**
	 * Retourne l'id du noeud
	 * @return string
	 */
	function getId(){ return DBUtil::getDBValue( "idcategory", "category", "name_1", $this->name );}
	
	 /**
	 * Retourne l'URI du noeud
	 * @return string
	 */
	function getURI(){ return $this->uri; }
	/**
	 * Retourne la méthode utilisée pour la reconnaissance de la date de dernière modification
	 * @return int
	 */
	function getGetLastModificationMethod(){ return $this->getLastModificationMethod; }
	/**
	 * Retourne le contenu de la balise 'loc' de l'élément XML
	 * @return string
	 */
	function getLocation(){ return $this->loc; }
	/**
	 * Retourne le contenu de la balise 'lastmod' de l'élément XML
	 * @return string
	 */
	function getLastModification(){ return $this->lastmod; }
	/**
	 * Retourne le contenu de la balise 'changefreq' de l'élément XML
	 * @return string
	 */
	function getChangeFrequency(){ return GoogleMap::$CHANGE_FREQ_ALWAYS; }
	/**
	 * Retourne le contenu de la balise 'priority' de l'élément XML
	 * @return string
	 */
	function getPriority(){ return $this->priority; }
		
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Définit le nom du noeud
	 * @param string $name le nom du noeud
	 * @return void
	 */
	function setName( $name ){ $this->name=  $name; }
	/**
	 * Définit la méthode utilisée pour la reconnaissance de la date de dernière modification
	 * @param int $method la méthode utilisée pour la reconnaissance de la date de dernière modification
	 * @return void
	 */
	function setGetLastModificationMethod( $method ){ $this->getLastModificationMethod=  $method; }
	/**
	 * Définit le contenu de la balise 'loc'
	 * @param $url string le contenu de la balise 'loc'
	 * @return void
	 */
	function setLocation( $url ){ $this->loc =  $url; }
	/**
	 * Définit le contenu de la balise 'lastmod'
	 * @param string $datetime le contenu de la balise 'lastmod'
	 * @return void
	 */
	function setLastModification( $datetime ){ $this->lastmod =  $datetime; }
	/**
	 * Définit le contenu de la balise 'changefreq'
	 * @param string $frequency le contenu de la balise 'changefreq'
	 * @return void
	 */
	function setChangeFrequency( $frequency ){ $this->changeFreq =  $frequency; }
	/**
	 * Définit le contenu de la balise 'priority'
	 * @param string $priority le contenu de la balise 'priority'
	 * @return void
	 */
	function setPriority( $priority ){ $this->priority=  $priority; }
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Récupère la date de dernière modification du fichier
	 * @return void
	 */
	public function readLastModificationDate(){
		
		global 	$GLOBAL_START_PATH,
				$GLOBAL_START_URL;
		
		switch( $this->getLastModificationMethod ){
			
			case GoogleMap::$LAST_MOD_READFROMFILE :

				$this->lastmod = date( "Y-m-d", filemtime( $GLOBAL_START_PATH . $this->uri ) );
				break;
				
			case GoogleMap::$LAST_MOD_CHECKSUM :
				
				$data = file_get_contents( $GLOBAL_START_URL . $this->uri );
				$this->lastmod = crc32( $data );
				break;
				
			case GoogleMap::$LAST_MOD_CURRENTDATE :
			default : 
				
				$this->lastmod = date( "Y-m-d" );
			
		}
				
	}
	
	//-------------------------------------------------------------------------------------------
	
 }
 
?>
