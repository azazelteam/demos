<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet devis client sur le front-office 
 * Gestion des tables de la base de données
 */

//java() method replaced by the inclusion of the "js/arbocategory.js" file in head

include_once( dirname( __FILE__ ) . "/adm_base.php" );

class ARBOCAT extends ADMBASE {
	
	//-----------------------------------------------------------------------------------------------------------
	
	protected $Arbo = false;
	protected $Allparents = false ;
	protected $current_page ='';
	protected $OnlyCat=true;
	protected $OnlyProd=true;
	protected $link2cat=true;
	protected $link2prod;
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	function ARBOCAT(){
		
		//ADMBASE::ADMBASE();
		$this->current_page = basename($_SERVER['PHP_SELF']);
		$this->Arbo= array();
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Chargement des id des catégories parentes
	 * @return void
	 */
	protected function loadallparents(){
		
		//if( isset($_SESSION['allparents']) ) { $this->Allparents = $_SESSION['allparents']; return; }
		$Allparents = array();
		$this->Allparents = DBUtil::getConnection()->GetAll("select distinct idcategoryparent from category_link");
		$n =count($this->Allparents);
		for($i=0;$i<$n;$i++) $this->Allparents[$i] = $this->Allparents[$i]['idcategoryparent'];
		//$_SESSION['allparents'] = $this->Allparents;
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Affichage de l'arbre
	 * @param string $link2cat Nom de la foncton javascript de choix de la catégorie
	 * @param string $link2prod Nom de la foncton javascript de choix de produits
	 * @param bool $OnlyCat n'afficher que les catégories ( optionnel, true par défaut )
	 * @param bool $OnlyProd n'afficher que les produits ( optionnel, true par défaut )
	 * @param bool $Expand_all développer l'arborescence ( optionnel, false par défaut )
	 * @return void
	 */
	public function show($link2cat='setcatg',$link2prod='setprodct',$OnlyCat=true,$OnlyProd=true,$Expand_all=false){
			
		$this->OnlyCat=$OnlyCat;
		$this->OnlyProd=$OnlyProd;
		$this->link2cat = $link2cat ;
		$this->link2prod = $link2prod;
		if(isset($_GET['PIdCategory']) ) $PIdCategory= $_GET['PIdCategory'];  else $PIdCategory=0;
		$this->loadallparents();
		$this->MakeArbo(0,'_1',0,$PIdCategory,$Expand_all);
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Affichage de la balise de lien vers la fonction javascript
	 * pour une catégorie
	 * @param int $i L'id de la catégorie
	 * @param string $t Me nom de la catégorie
	 * @return void
	 */
	protected function linktocat( $i, $t){
		
		$t= addslashes($t);
		echo '<a href="javascript:',$this->link2cat,"($i,'$t')",'">';
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Affichage de la balise de lien vers la fonction javascript
	 * pour des produits
	 * @param int $i L'id de la catégorie
	 * @param string $t Me nom de la catégorie
	 * @return void
	 */
	protected function linktoprod($i,$t){
		
		$t= addslashes($t);
		echo '<a href="javascript:',$this->link2prod,"($i,'$t')",'">';
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Fonction récursive de contruction de l'arbre
	 * @param int $IdCategory L'id de la catégorie de départ
	 * @param string $lang le numéro de la langue préfixé d'un underscore
	 * @param int $First Le niveau dans l'arbre
	 * @param int $PIdCategory L'id d'une catégorie à déployer ou à fermer(si négatif)
	 * @param bool $Expand_all développer toute l'arborescence ou non
	 * @return void
	 */
	protected function MakeArbo($IdCategory=0,$lang,$First=0,$PIdCategory,$Expand_all=false){
		global $GLOBAL_START_URL;
		
		$ScriptName  = $this->current_page ;
	
		if(!isset($_SESSION['openlevels'])) $_SESSION['openlevels']=array(); 
		$openlevels = &$_SESSION['openlevels'];
		if($First == 0)
		{	echo "<table width=\"100%\"><tr><td>".Dictionnary::translate("racine")."</td>\n";
			if($this->OnlyCat) {
			echo "<td>\n";
			$this->linktocat(0,Dictionnary::translate("racine")); //echo "<a href='javascript:setsel2(0,0)'>";
			echo "<img style=\"border-style:none;\" src=\"$GLOBAL_START_URL/images/back_office/content/cat_modif.jpg\" alt=\"Ajouter une categorie\" />\n";
			echo "</td>\n";
			}
			$rs2 = DBUtil::getConnection()->Execute('select qty_product from category where idcategory=0');
			echo '<td><span style="margin-left:20pt"><b>(' .$rs2->fields["qty_product"].')</b> produits</span></td>';
			echo "</tr></table>\n";
	
		if($PIdCategory > 0)
			{
			if( !in_array($PIdCategory,$openlevels, FALSE) ) { $openlevels[] = $PIdCategory; }
			}
		else if($PIdCategory < 0)
			{
			$closelevels=array(abs($PIdCategory)) ; $openlevels= array_diff($openlevels,$closelevels);
			}
		}// else echo "SECOND $First ";
	
	
		$rs = DBUtil::getConnection()->Execute("select idcategorychild from category_link where idcategoryparent='$IdCategory'");
		$n = $rs->RecordCount();
		if($n)
		{
			while(!$rs->EOF)
			{
				$IdCategoryChild=$rs->fields["idcategorychild"];
				if($IdCategory!=$IdCategoryChild)
				{
					$rs2 = DBUtil::getConnection()->Execute("select name$lang,qty_product from category where idcategory='$IdCategoryChild'");
					$mt = $First * 10;
					if($First > 0 ) $cstyle = "opened_level"; else $cstyle = "active_level";
					$iname = htmlspecialchars($rs2->fields["name$lang"]);
					if( in_array(0+$IdCategoryChild,$this->Allparents, FALSE) )
					{
						if( !in_array($IdCategoryChild,$openlevels, FALSE) AND !$Expand_all)
						{
						echo "<table style=\"margin-left:".$mt."px;\" width=\"90%\" ><tr><td width=\"85%\">";
						echo "<a href=\"$ScriptName?PIdCategory=$IdCategoryChild&po=" . $this->OnlyCat . "\"><img src=\"$GLOBAL_START_URL/images/back_office/content/cat_close.jpg\" style=\"border-style:none;\" /></a>\n";
						echo $IdCategoryChild." - ".$iname;
						echo "</td>";
						}
						else
						{
						echo '<table style="margin-left:'.$mt.'px;" width="90%"><tr><td width="85%">';
						if(!$Expand_all) echo "<a href=$ScriptName?PIdCategory=-$IdCategoryChild&po=".$this->OnlyCat."><img src=\"$GLOBAL_START_URL/images/back_office/content/cat_open.jpg\" style=\"border-style:none;\" /></a>\n";
						echo $IdCategoryChild." - ".$iname;
						if($Expand_all) echo '<span style="margin-left:20pt" ><b>(' .$rs2->fields["qty_product"].')</b> produits</span>';
						echo "</td>";
						}
					if($this->OnlyCat){
					echo "<td width=12 align=right>";
					$this->linktocat($IdCategoryChild,$iname); //echo "<a href='javascript:setsel2($IdCategoryChild,0)'>";
					echo "<img src=\"$GLOBAL_START_URL/images/back_office/content/cat_modif.jpg\" alt=\"Ajouter une categorie\" style=\"border-style:none; float:right;\" />\n";
					echo "</a></td>";
					}
					/*if($this->OnlyProd) {
					$mt+=8;
					//echo "<table style=\"margin-left:".$mt."px;\" width=200 border=0><tr><td width=160>";
					echo "<td width=12 align=right>";
					echo "<span class=\"arbo\">";
					$this->linktoprod($IdCategoryChild,$iname);
					echo "<img src=../img/button_product.jpg border=0 alt=\"Ajouter une categorie\">\n";
					echo "</a></td>\n";
					} */
					}
					else
					{
					/*	if( !in_array($IdCategoryChild,$openlevels, FALSE) )
						{
						echo "<table style=\"margin-left:".$mt."px;\" width=200 border=0><tr><td width=160>";
						echo "<a href=$ScriptName?PIdCategory=$IdCategoryChild class=\"arbo_link\"><b>+</b></a>\n";
						echo  "<span class=\"arbo\">";
						echo $IdCategoryChild." - ".$iname;
						echo "</td>";
						}
						else
						{ */
						echo "<table style=\"margin-left:".$mt."px;\" width=\"80%\"><tr><td>";
						//echo "<a href=$ScriptName?PIdCategory=-$IdCategoryChild class=Liens><b>-</b></a>\n";
						echo $IdCategoryChild." - ".$iname;
						echo "</td>";
					//	}
					$npd = $rs2->fields["qty_product"];
						if($this->OnlyCat ){
					//	$npd = $rs2->fields["qty_product"];
						if( $npd < 1 )
						{
					echo "<td width=12 align=right>";
					$this->linktocat($IdCategoryChild,$iname); //echo "<a href='javascript:setsel2($IdCategoryChild,0)'>";
					echo "<img src=\"$GLOBAL_START_URL/images/back_office/content/cat_modif.jpg\" alt=\"Ajouter une categorie\" style=\"border-style:none; float:right;\" />\n";
					echo "</a></td>";
						}
						else echo "<td align=right><span class=\"arbo_qty\"><b>($npd)</b> produits</span></td>";
					}
					elseif( $npd > 0 ) echo "<td align=right><span class=\"arbo_qty\"><b>($npd)</b> produits</span></td>";
					if($this->OnlyProd) {
					
					echo "</td><td width=12>";
					$this->linktoprod($IdCategoryChild,$iname); //echo "<a href='javascript:setsel($IdCategoryChild,0)'>";
					echo "<img src=\"$GLOBAL_START_URL/images/back_office/content/cat_modif.jpg\" alt=\"Ajouter un produit\" style=\"border-style:none; float:right;\" />\n";
					echo "</a></td>\n";
						}
					}
					echo "</tr></table>\n";
	
					if( $Expand_all OR in_array($IdCategoryChild,$openlevels, FALSE) )
					{
					//echo "<pre> IdCategory = $IdCategory $IdCategoryChild $First</pre>\n";
						$this->MakeArbo($IdCategoryChild,$lang,$First+1,$PIdCategory,$Expand_all);
					}
				}
				$rs->MoveNext();
			}
		}
	}

	//-----------------------------------------------------------------------------------------------------------
	
}//EOC

?>