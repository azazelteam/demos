<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object parrainage client (non utilisé)
 */

include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/GiftToken.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/mime_mail/htmlMimeMail.php");

 
class Sponsorship{
	
	//----------------------------------------------------------------------------
	
	private $idsponsorship;
	private $sponsor;
	private $guest;
	private $fields;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * Créé l'object parrainage dont le numéro est donné
	 * Pour créer une nouveau parrainage, utilisez Sponsorship::create( int $idsponsor, int $guest_email, int $idcontact_sponsor = 0 )
	 * @param int $idsponsorship le numéro du parrainage
	 */
	function __construct( $idsponsorship ){
		
		$this->idsponsorship	= intval( $idsponsorship );
		
		$this->sponsor			= array();
		$this->guest			= array();
		$this->fields			= array();
		
		//Données de la base
		$this->load();
		
		//Infos parrain
		$this->loadSponsorInfos();
		
		//Infos filleul
		$this->loadGuestInfos();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les informations du parrainage
	 * @return void
	 */
	
	private function load(){
		
		$this->fields = array();
		
		$query = "
		SELECT * FROM `sponsorship`
		WHERE `idsponsorship` = '" . $this->idsponsorship . "'
		LIMIT 1";
		
		$rs = DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les informations du parrain
	 * @return void
	 */
	
	private function loadSponsorInfos(){
		
		
		
		$this->sponsor = array();
		
		$query = "
		SELECT b.adress, 
			b.adress_2, 
			b.zipcode, 
			b.city, 
			b.idstate, 
			c.title, 
			c.lastname, 
			c.firstname, 
			c.mail, 
			t.title" . "_1" . " AS title
		FROM buyer b, contact c, title t 
		WHERE b.idbuyer = '" . $this->fields[ "idsponsor" ] . "' 
		AND b.idbuyer = c.idbuyer 
		AND c.idcontact = '0' 
		AND c.title = t.idtitle
		LIMIT 1";
		
		$rs = DBUtil::query( $query );
		
		$this->sponsor = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les informations du filleul
	 * @return void
	 */
	
	private function loadGuestInfos(){
		
		$this->guest = array();
		
		$query = "
		SELECT b.adress,
			b.adress_2,
			b.zipcode,
			b.city,
			b.idstate,
			c.title,
			c.lastname,
			c.firstname,
			c.mail
		FROM buyer b, contact c
		WHERE b.idbuyer = '" . $this->fields[ "idguest" ] . "'
		AND c.idcontact = '0'
		LIMIT 1";
		
		$rs = DBUtil::query( $query );
		
		$this->guest = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée le parrainage entre le parrain $idsponsor et le filleul $idguest
	 * @param int $idsponsor l'identifiant du parain dans la table buyer
	 * @param int $idguest_email l'email du filleul
	 * @param int $idcontact_sponsor l'identifiant du contact du parain dans la table contact
	 * @return Sponsorship l'objet créé
	 */
	
	//@todo: enregistrement du message du parrain
	//@todo: gérer le send_type du chèque créé
	//@todo: envoyer le mail au filleul
	public static function &create( $idsponsor, $guest_email, $guest_firstname, $guest_lastname, $idcontact_sponsor = 0 ){
		
		$guest = Customer::create();
		$guest->getContact()->set( "mail", $guest_email );
		$guest->getContact()->set( "firstname", $guest_firstname );
		$guest->getContact()->set( "lastname", $guest_lastname );
		
		if( strlen( $guest_email ) )
			$guest->getContact()->setAccount( $guest_email );
		
		$guest->save();
		
		//Création du parrainage
		$query = "INSERT INTO `sponsorship` ( 
			`idsponsor`, 
			`idcontact_sponsor`, 
			`idguest`, 
			`sponsorship_date` 
		) VALUES ( 
			'$idsponsor', 
			'$idcontact_sponsor', 
			'" . $guest->getId() . "', 
			NOW() 
		)";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de créer le nouveau parrainage" );
		
		//Récupération de l'identifiant du parrainage créé
		$idsponsorship = DBUtil::getConnection()->Insert_ID();
		
		//Instanciation du parrainage
		$sponsorship = new Sponsorship( $idsponsorship );
		
		//Création du chèque cadeau du parrain
		$idsponsor_token = $sponsorship->createSponsorToken();
		
		//Création du chèque cadeau du filleul
		$idguest_token = $sponsorship->createGuestToken();
		
		$sponsorship->set( "idgift_token", $idsponsor_token );
		
		$sponsorship->update();
		
		//Envoi du mail de parrainage au parrain
		$sponsorship->sendSponsorEmail();
		
		//Envoi du mail de parrainage au filleul
		$sponsorship->sendGuestEmail($guest_email,$password=""); //@todo mot de passe crypté par MD5
		
		return $sponsorship;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée le chèque cadeau du parrain qu'il reçoit suite au parrainage
	 * @return int l'identifiant du chèque cadeau créé
	 */
	
	private function createSponsorToken(){
		
		$idtoken_model = DBUtil::getParameterAdmin( "sponsoring_gift_token_model" );
		
		//Création du chèque cadeau
		$token = GiftToken::create( $idtoken_model, $this->get( "idsponsor" ) );

		$token->update();
		
		return $token->getId();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée le chèque cadeau du filleul qu'il reçoit suite au parrainage
	 * @return int l'identifiant du chèque cadeau créé
	 */
	
	private function createGuestToken(){
		
		return false;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Envoi le mail parrainage au parrain
	 * @return void
	 */
	
	private function sendSponsorEmail(){
		
		
		
		$editor = new MailTemplateEditor();
		
		$editor->setLanguage( substr( "_1", 1 ) );
		
		$editor->setTemplate( "SPONSORSHIP_SPONSOR" );
		
		$mailer = new htmlMimeMail();
		
		$mailer->setFrom( DBUtil::getParameterAdmin( "b_contact_email" ) );
		
		$editor->setUseAdminTags();
		$editor->setUseSponsorshipSponsorTags( $this->fields[ "idsponsor" ] );
		
		$Message = $editor->unTagBody();
		$Subject = $editor->unTagSubject();
		
		$mailer->setHtml( $Message );
		$mailer->setSubject( $Subject );
		
		$result = $mailer->send( array( $this->getSponsorValue( "mail" ) ) );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Envoi le mail parrainage au filleul
	 * @param string $guest_email l'adresse email du filleul
	 * @param string $password le mot de passe du client
	 * @return void
	 */
	
	private function sendGuestEmail( $guest_email, $password ){
		
		
		
		$editor = new MailTemplateEditor();
		
		$editor->setLanguage( substr( "_1", 1 ) );
		
		$editor->setTemplate( "SPONSORSHIP_GUEST" );
		
		$mailer = new htmlMimeMail();
		
		$sponsor_firsname = $this->getSponsorValue( "firstname" );
		$sponsor_lastname = $this->getSponsorValue( "lastname" );
		$sponsor_email = $this->getSponsorValue( "mail" );
		
		$mailer->setFrom( "\"$sponsor_firsname $sponsor_lastname \" <$sponsor_email>" );
		
		$editor->setUseBuyerTags( $this->get( "idsponsor" ) );
		
		$editor->setUseAdminTags();
		$editor->setUseSponsorshipSponsorTags( $this->fields[ "idsponsor" ] );
		$editor->setUseSponsorshipGuestTags( $guest_email, $password );
		
		$Message = $editor->unTagBody();
		$Subject = $editor->unTagSubject();
		
		$mailer->setHtml( $Message );
		$mailer->setSubject( $Subject );
		
		$result = $mailer->send( array( $guest_email ) );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet
	 * @return void
	 */
	 
	 public function update(){
	
		$updatableFields = array();
		
		foreach( $this->fields as $fieldname => $value ){
			
			if( $fieldname != "idsponsorship" )
				$updatableFields[] = $fieldname;
			
		}
		
		$query = "UPDATE `sponsorship`
		SET ";
		
		$i = 0;
		
		foreach( $updatableFields as $fieldname ){
			
			if( $i )
				$query .= ", ";
			
			$query .= "`$fieldname` = '" . Util::html_escape( $this->fields[ $fieldname ] ) . "'";
			
			$i++;
			
		}
		
		$query.=" WHERE `idsponsorship` = '" . $this->getId() . "'";
		
		DBUtil::query( $query );
		
	 }
	 
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un attribut donné du parrain
	 * @param string $key l'attribut du parrain dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	
	public function getSponsorValue( $key ){
		
		return $this->sponsor[ $key ];
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un attribut donné du parrainage
	 * @param string $key l'attribut du parrainage dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	
	public function get( $key ){
		
		return $this->fields[ $key ];
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Affecte une valeur à une propriété du parrainage donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 	 
	public function set( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne l'identifiant du parrainage
	 * @return int
	 */
	
	public function getId(){
		
		return $this->idsponsorship;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne true si un client donné est parrainé, sinon false
	 * @param int $idbuyer le numéro de client1
	 * @todo gérer les contacts ( idbuyer + idcontact )
	 * @return bool
	 */
	public static function isSponsorshiped( $idbuyer ){
		
		$query = "
		SELECT COUNT(*) AS isSponsorShipped
		FROM sponsorship
		WHERE idguest = '$idbuyer'";
		
		$rs =& DBUtil::query( $query );
		
		return $rs->fiedls( "isSponsorShipped" ) != 0;
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>