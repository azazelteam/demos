<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entête factures clients
 */

include_once( dirname( __FILE__ ) . "/Sale.php" );
include_once( dirname( __FILE__ ) . "/SecuredDocument.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Invoicing.php" );
include_once( dirname( __FILE__ ) . "/Delivery.php" );



abstract class DBSale extends DBObject implements Sale, SecuredDocument, Invoicing, Delivery{
	
	//----------------------------------------------------------------------------
	/**
	 * @var TradePriceStrategy $priceStrategy
	 */
	protected $priceStrategy;
    /**
	 * @var Customer $customer
	 */
	protected $customer;
	/**
	 * @var Salesman $salesman
	 */
	protected $salesman;
   	/**
	 * @var Address $invoiceAddress
	 */
	protected $invoiceAddress;
    /**
	 * @var Address $forwardingAddress
	 */
    protected $forwardingAddress;
    
    //----------------------------------------------------------------------------
    
    //@todo PHP 5.3 late state binding( protect static $TABLE_NAME, ... )
	protected $TABLE_NAME;
	protected $ITEMS_TABLE_NAME;
	protected $PRIMARY_KEY_NAME;	
	
	//----------------------------------------------------------------------------
	
	public abstract function getId();
	
    //----------------------------------------------------------------------------
	/**
	 * recalculs des montants pré-calculés avant sauvegarde
	 * @return void
	 */
	protected abstract function recalculateAmounts();

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access protected
	 * @return void
	 */
	protected function setPriceStrategy(){
		
		$rs =& DBUtil::query( 
		
			"SELECT strategy 
			FROM trade_price_strategy 
			WHERE `datetime` <= '" . $this->recordSet[ "DateHeure" ] . "'
			ORDER BY datetime DESC
			LIMIT 1" 
		
		);
		
		if( !$rs->RecordCount() 
			|| !file_exists( dirname( __FILE__ ) . "/strategies/" . $rs->fields( "strategy" ) . ".php" ) ){

			trigger_error( "stratégie de calcul de prix inconnue", E_USER_ERROR );
			exit();
			
		}

		$strategy = $rs->fields( "strategy" );
		
		include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
		$this->priceStrategy = new $strategy( $this );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/SecuredDocument#allowRead()
	 */
    public function allowRead(){ return $this->allowCustomerRead() || $this->allowUserRead(); }

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/SecuredDocument#allowCreationForCustomer()
	 */
	public static function allowCreationForCustomer( $idbuyer ){

		if( !User::getInstance()->getId() )
			return false;
		
		//le commercial identifié est le commercial du client
		
		$salesman = DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $idbuyer );
			
		if( $salesman == User::getInstance()->getId() )
			return true;
			
		//le commercial identifié a des droits administrateur
		
		if( User::getInstance()->get( "admin" ) )
			return true;
		
		//aucun droit

		return false;
		
	}
	
	//----------------------------------------------------------------------------
    /**
     * Retourne true si le client identifié est autorisé à consulter le document
     * @access private
     * @return bool
     */
    private function allowCustomerRead(){
	
    	include_once( dirname( __FILE__ ) . "/Session.php" );
		return Session::getInstance()->getCustomer() && Session::getInstance()->getCustomer()->getId() == $this->customer->getId();
		
    }
    
    //----------------------------------------------------------------------------
    /**
     * Retourne true si le commercial identifié est autorisé à consulter le document
     * @access private
     * @return bool
     */
    private function allowUserRead(){
    	
		if( User::getInstance()->getId() == $this->salesman->getId() 
			|| User::getInstance()->getId() == $this->customer->get( "iduser" )
			|| User::getInstance()->getHasAdminPrivileges() ) 
			return true;
		
		return false;
		
    }
    
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/SecuredDocument#allowEdit()
	 */
	public function allowEdit(){

		include_once( dirname( __FILE__ ) . "/Session.php" );
		
		//édition auto front office
		
		if( Session::getInstance()->getCustomer() && Session::getInstance()->getCustomer()->getId() == $this->getCustomer()->getId() )
			return true;
		
		//le commercial identifié est le commercial propriétaire de l'objet

		if( User::getInstance()->getId() == $this->getSalesman()->getId() ) 
			return true;
		
		//le commercial identifié est le commercial du client

		if( $this->getCustomer()->get( "iduser" ) == User::getInstance()->getId() )
			return true;
	
		//le commercial identifié a des droits administrateur
		
		if( User::getInstance()->getHasAdminPrivileges() )
			return true;
		
		//aucun droit

		return false;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBObject#save()
	 */
	public function save(){
		
		if( !$this->allowEdit() ){
			
			trigger_error( "Appel de DBObject::save() non autorisé", E_USER_ERROR );
			return;
			
		}
		
		$this->recalculateAmounts();
		
		DBUtil::query( "DELETE FROM `{$this->ITEMS_TABLE_NAME}` WHERE `{$this->PRIMARY_KEY_NAME}` = '" . $this->getId() . "'" );
		
		$it = $this->items->iterator();
	
		while( $it->hasNext() )
			$it->next()->save();

		parent::save();

		/* marges */
		
		$this->recordSet[ "net_margin_amount" ] = $this->getNetMarginAmount();
		$this->recordSet[ "net_margin_rate" ] = $this->getNetMarginRate();
		$this->recordSet[ "gross_margin_amount" ] = $this->getGrossMarginAmount();
		$this->recordSet[ "gross_margin_rate" ] = $this->getGrossMarginRate();
		
		DBUtil::query(
		
			"UPDATE `{$this->TABLE_NAME}`
			SET net_margin_amount = '" . $this->getNetMarginAmount() . "',
			net_margin_rate = '" . $this->getNetMarginRate() . "',
			gross_margin_amount = '" . $this->getGrossMarginAmount() . "',
			gross_margin_rate = '" . $this->getGrossMarginRate() . "',
			updated = '0000-00-00 00:00:00'
			WHERE `{$this->PRIMARY_KEY_NAME}` = '" . $this->getId() . "' LIMIT 1"
			
		);
		
	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getVATRate()
	 */
	public function getVATRate(){
		
		static $defaultVATRate = NULL;
		
		if( !$this->applyVAT() )
			return 0.0;

		if( $defaultVATRate === NULL )
			$defaultVATRate = DBUtil::getParameter( "vatdefault" );
			
		return $defaultVATRate;
				
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#applyVAT()
	 */
	public function applyVAT(){

		if( !B2B_STRATEGY || $this->recordSet[ "no_tax_export" ] == 2 )
			return true;

		if( $this->recordSet[ "no_tax_export" ] == 1 )
			return false;

		if( $this->recordSet[ "no_tax_export" ] == 2 )
			return true;
			
		$bvat = DBUtil::getDBValue( "bvat", "state", "idstate", $this->getForwardingAddress()->getIdState() );

		return $bvat ? true : false;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le montant total HT
	 */
	public function getTotalET(){ return $this->priceStrategy->getTotalET(); }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le montant total TTC
	 */
	public function getTotalATI(){ return $this->priceStrategy->getTotalATI(); }

	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le net à payer sur facture
	 */
	public function getNetBill(){ return $this->priceStrategy->getNetBill(); }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getDiscountRate()
	 */
	public function getDiscountRate(){ return $this->recordSet[ "total_discount" ]; }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getBillingRate()
	 */
	public function getBillingRate(){ return $this->recordSet[ "billing_rate" ]; }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getRebateAmount()
	 */
	public function getRebateAmount(){ return $this->recordSet[ "rebate_type" ] == "amount" ? $this->recordSet[ "rebate_value" ] : round( $this->getTotalATI() * $this->recordSet[ "rebate_value" ] / 100.0, 2 ); }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getDownPaymentAmount()
	 */
	public function getDownPaymentAmount(){ return $this->recordSet[ "down_payment_type" ] == "rate" ? round( $this->getTotalATI() * $this->recordSet[ "down_payment_value" ] / 100.0, 2 ) : $this->recordSet[ "down_payment_value" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le pourcentage d'acompte
	 */
	public final function getDownPaymentRate(){

		if( $this->recordSet[ "down_payment_type" ] == "rate" )
			return $this->recordSet[ "down_payment_value" ];

		return $this->getTotalATI() > 0.0 ? round( $this->recordSet[ "down_payment_value" ] / $this->getTotalATI() * 100, 2 ) : 0.0;

	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getCreditAmount()
	 */
	public function getCreditAmount(){ return $this->recordSet[ "credit_amount" ]; }
	
	//----------------------------------------------------------------------------
	/**
	 * Montant de la marge nette
	 * @abstract
	 * @access public
	 * @return float
	 */
	public abstract function getNetMarginAmount();
	/**
	 * Pourcentage de marge nette
	 * @abstract
	 * @access public
	 * @return float
	 */
	public abstract function getNetMarginRate();
	/**
	 * Montant de la marge brute
	 * @abstract
	 * @access public
	 * @return float
	 */
	public abstract function getGrossMarginAmount();
	/**
	 * Pourcentage de marge brute
	 * @abstract
	 * @access public
	 * @return float
	 */
	public abstract function getGrossMarginRate();
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le poids total
	 * @return float
	 */
	public function getWeight(){ 
		
		$weight = 0.0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			$weight += $item->getWeight() * $item->getQuantity();
			
		}
		
		return $weight; 
		
	}
	
	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return Customer
     */
    public function &getCustomer(){ return $this->customer; }
	
    //----------------------------------------------------------------------------
	/**
     * @access public
     * @return Salesman
     */
	public function &getSalesman(){ return $this->salesman; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return Address l'adresse de facturation
	 */
	public function &getInvoiceAddress(){ return $this->invoiceAddress; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return Address une référence vers l'adresse de livraison
	 */
	public function &getForwardingAddress(){ return $this->forwardingAddress; }
	
    //----------------------------------------------------------------------------
	/**
     * @access public
     * @return ArrayList<TradeItem>
     */
    public function &getItems(){ return $this->items; }
    
	//----------------------------------------------------------------------------
	/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public final function getItemCount() { return $this->items->size(); }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le $index-ième élément du panier
	 * @return DBSaleItem
	 */
	public final function &getItemAt( $index ) { return $this->items->get( $index ); }
	
	//----------------------------------------------------------------------------
	/**
	 * Suppression de tous les articles du panier
	 * @access public
	 * @return void
	 */
	public function removeItems(){

		$this->items = new ArrayList();
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Supprime un élément donné du panier
	 * @param int $index Le $index-ième élément à supprimer
	 * @return void
	 */
	public function removeItemAt( $index ){
		
		$this->items->remove( $this->items->get( $index ) );
		
		/* réindexation */
		
		$index = 1;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$it->next()->set( "idrow", $index++ );
			
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Assigne un commercial donné à la commande
	 * @param Salesman $salesman
	 * @return void
	 */
	public function setSalesman( Salesman $salesman ){
		
		$this->set( "iduser", $salesman->getId() );
		$this->salesman = $salesman;
				
	}

	//----------------------------------------------------------------------------
	/**
	 * Définit l'adresse de facturation à utiliser
	 * @param Address $address une adresse de facturation
	 * @return void
	 */
	public function setInvoiceAddress( Address $address ){ 
		
		$this->invoiceAddress = $address;
		$this->set( "idbilling_adress", $this->invoiceAddress instanceof InvoiceAddress ? $this->invoiceAddress->getId() : 0 ); 
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Définit l'adresse de livraison à utiliser
	 * @param Address $address une adresse de livraison
	 * @return void
	 */
	public function setForwardingAddress( Address $address ){ 
		
		$this->forwardingAddress = $address;
		$this->set( "iddelivery", $this->forwardingAddress instanceof ForwardingAddress ? $this->forwardingAddress->getId() : 0 );
	
	}
	
	//----------------------------------------------------------------------------
	
}

?>