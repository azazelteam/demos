<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object liste des modèles de mails
 */


include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( "$GLOBAL_START_PATH/objects/Util.php" );


class MailingListTemplateEditor{
	
	//----------------------------------------------------------------------------------------
	
	private $body;
	private $description;
	private $idmailtemplate;
	private $language;
	private $name;
	private $subject;
		//----------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idmail_template l'id du template de mail
	 */
	function __construct( $idmail_template = 0 ){
		
		$this->init();
		
	}
	
		//----------------------------------------------------------------------------------------
	/**
	 * Initialisation des données
	 */
	private function init(){

		$this->body 				= array();
		$this->description 			= array();
		$this->idmail_template 		= 0;
		$this->language 			= substr( User::getInstance()->getLang(), 1 );
		$this->name 				= array();
		$this->subject	 			= array();
		
	}
	
	// Gayteurs
	
	public function getBody(){
		return $this->body[$this->language];
	}
	
	public function getDescription(){
		return $this->description[$this->language];
	}

	public function getIdmail_template(){
		return $this->idmail_template;
	}
	
	public function getLanguage(){
		return $this->language;
	}
	
	public function getName(){
		return $this->name[$this->language];
	}
	public function getSubject(){
		return $this->subject[$this->language];
	}
	
	// Setteurs
	
	public function setBody($newBody){
		$this->body[$this->language] = $newBody;
	}
	
	public function setDescription($newDescription){
		$this->description[$this->language] = $newDescription;
	}
	
	public function setName($newName){
		$this->name[$this->language] = $newName;
	}
	
	public function setSubject($newSubject){
		$this->subject[$this->language] = $newSubject;
	}
	
	
	// Méthodes
	
	/**
	 * Récupère toutes les informations concernant un modèle
	 * @param int $idmail_template
	 * @return template template ayant pour id $idmail_template
	 */
	public function loadTemplate($idmail_template){
		
		
		
		$query = "
		SELECT *
		FROM mailing_mail_template
		WHERE idmailing_mail_template = '$idmail_template'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de charger le template '{$this->idmail_template}'" );
			
		if( !$rs->RecordCount() )
			return;
		
		$this->idmail_template 	= $rs->fields( "idmailing_mail_template" );
		
		$language = $this->language;
		
		while( $language <= 5 ){
			
			$this->name[ $language ] 		= $rs->fields( "name_".$language );
			$this->description[ $language ] = $rs->fields( "description_".$language );
			$this->subject[ $language ]		= $rs->fields( "subject_".$language );
			$this->body[ $language ]		= $rs->fields( "body_".$language );
			
			$language++;
			
		}
		
		return($this);
		
	}
	
	public function updateTemplate($idmail_template){
				
		
		$language = $this->language;
		$query = "
		UPDATE mailing_mail_template
		SET	name_".$language."			=	'".Util::html_escape($this->name[ $language ])."',
			description_".$language."	=	'".Util::html_escape($this->description[ $language ])."',
			subject_".$language."		=	'".Util::html_escape($this->subject[ $language ])."',
			body_".$language."			=	'".Util::html_escape($this->body[ $language ])."',
			lastupdate					=	NOW()
		WHERE idmailing_mail_template = ".$idmail_template."";
		DBUtil::query( $query );
	}
	
	public function eraseTemplate(){

		$query = "
		DELETE 
		FROM mailing_mail_template
		WHERE idmailing_mail_template = ".$this->idmail_template."";
		DBUtil::query( $query );
	}
	
	public function saveNewTemplate(){
				
		$language = $this->language;
		$query = "
		INSERT
		INTO mailing_mail_template ( name_".$language.", description_".$language.", subject_".$language.", body_".$language.", lastupdate)
		VALUES	('".Util::html_escape($this->name[ $language ])."',
				'".Util::html_escape($this->description[ $language ])."',
				'".Util::html_escape($this->subject[ $language ])."',
				'".Util::html_escape($this->body[ $language ])."',
				NOW())";
		DBUtil::query( $query );
	}
	
}
?>