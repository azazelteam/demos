<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object métiers
 */
 
include_once( "catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CATALTRADE extends CATALBASE
{
	
	/**
	 * Constructeur
	 * @param int $IdTrade
	 * @param int $reload
	 */
	function __construct($IdTrade, $reload=1)
	{

	$this->id = $IdTrade ;
	$this->buyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$this->lang = "_1";

	if( $reload ) {
		$this->NBProduct = 0 + $this->GetTradeProducts();
		}
	else { 
		$this->products = &$_SESSION['tradeproducts'] ;
		$this->NBProduct = 0 +  count($this->products);
		}
	$this->NbByPages = DBUtil::getParameter('nb_products_by_pages');
	}

	/**
	 * Cherche la liste des métiers
	 * @return array la liste des métiers
	 */
	public function GetTradesList()
	{
		$IdTrade = $this->id;
		$Langue = $this->lang;
		$TradesList = array();
		$Query = "SELECT `idtrade`, `trade$Langue` AS `name` FROM `trade` ORDER BY `display`";
		$Base = &DBUtil::getConnection();
		$rs =& DBUtil::query($Query);
		trigger_error(showDebug($Query,'CATALTRADE::GetTradesList() => $SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATALTRADE::GetTradesList() => $SQL_select','log'),E_USER_ERROR);
		while( !$rs->EOF ) {
			$TradesList[$rs->fields['idtrade']] = $rs->fields['name'];
			$rs->MoveNext();
		}
		//showDebug($TradesList,'CATALTRADE::GetTradesList() => $TradesList','log');
		return $TradesList;
	}

	/**
	 * Cherche les produit 
	 * @return array la liste des produits
	 */
	function GetTradeProducts()
	{
		$IdTrade = $this->id;
		$Langue = $this->lang;
		$Query = "SELECT DISTINCT `r`.`idproduct`, `r`.`name$Langue` AS `name`, `t`.`trade$Langue` AS `trade`" .
				"FROM `trade_product` `tp`, `product` `r`, `trade` `t` " .
				"WHERE `tp`.`idproduct` = `r`.`idproduct` " .
				"AND `tp`.`idtrade` = `t`.`idtrade` " .
				"AND `tp`.`idtrade` = '$IdTrade' " .
				"ORDER BY `tp`.`display`";
		//showDebug($Query,'CATALTRADE::GetTradeProducts() => $Query','log');
		$n = $this->SetProducts($Query);
		$_SESSION['tradeproducts'] = &$this->products;
		//$rs=DBUtil::query($Query);
		
		return $n;
	}
	
	function GetTradeProductsInfos(){
		$IdTrade = $this->id;
		$Langue = $this->lang;
		$Query = "SELECT DISTINCT `r`.`idproduct`, `r`.`name$Langue` AS `name`, `t`.`trade$Langue` AS `trade`" .
				"FROM `trade_product` `tp`, `product` `r`, `trade` `t` " .
				"WHERE `tp`.`idproduct` = `r`.`idproduct` " .
				"AND `tp`.`idtrade` = `t`.`idtrade` " .
				"AND `tp`.`idtrade` = '$IdTrade' " .
				"ORDER BY `tp`.`display`";
		$rs=DBUtil::query($Query);
		
		return $rs;
	}

	/**
	 * Cherche le nom du métier
	 * @return string le nom du métier, error si pas trouvé
	 */
	public function GetTradeName()
	{
		$IdTrade = $this->id;
		$Langue = $this->lang;
		$Query = "SELECT `trade$Langue` AS `name` FROM `trade` WHERE `idtrade` = '$IdTrade'";
		$rs = DBUtil::query($Query);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATALTRADE::GetTradeName() => $SQL_select','log'),E_USER_ERROR);
		if( !$rs->EOF ) {
			return $rs->fields['name'];
		}
		return 'error';
	}
	
	public function GetArboList()
	{
		$IdArbo = $this->id;
		$Langue = $this->lang;
		$ArboList = array();
		$Query = "SELECT `idarbo`, `arbo$Langue` AS `name` FROM `arbo` WHERE arbo_parent = 0 ORDER BY `display`";
		$Base = &DBUtil::getConnection();
		$rs =& DBUtil::query($Query);
		//trigger_error(showDebug($Query,'CATALTRADE::GetArboList() => $SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATALTRADE::GetArboList() => $SQL_select','log'),E_USER_ERROR);
		while( !$rs->EOF ) {
			$ArboList[$rs->fields['idarbo']] = $rs->fields['idarbo'];
			$rs->MoveNext();
		}
		//showDebug($TradesList,'CATALTRADE::GetTradesList() => $TradesList','log');
		return $ArboList;
	}
	public function GetArboListName()
	{
		$IdArbo = $this->id;
		$Langue = $this->lang;
		$ArboList = array();
		$Query = "SELECT `idarbo`, `arbo$Langue` AS `name` FROM `arbo` WHERE arbo_parent = 0 ORDER BY `display`";
		$Base = &DBUtil::getConnection();
		$rs =& DBUtil::query($Query);
		//trigger_error(showDebug($Query,'CATALTRADE::GetArboList() => $SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATALTRADE::GetArboList() => $SQL_select','log'),E_USER_ERROR);
		while( !$rs->EOF ) {
			$ArboList[$rs->fields['idarbo']] = $rs->fields['name'];
			$rs->MoveNext();
		}
		//showDebug($TradesList,'CATALTRADE::GetTradesList() => $TradesList','log');
		return $ArboList;
	}

	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}


}//EOC
?>