<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ligne article
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/TradeItem.php" );


class BasketItem implements TradeItem{

	//----------------------------------------------------------------------------
	/**
	 * @var CArticle $article
	 */
	private $article;
	/**
	 * @var int $quantity
	 */
	private $quantity;
	protected $isGift;
	
	//----------------------------------------------------------------------------
	/**
	 * @param CArticle $article
	 * @param int $quantity, optionnel
	 */
	public function __construct( CArticle $article, $quantity = 1, $_isGift =false ){
		
		$this->article = $article;
		$this->quantity = intval( $quantity );
		$this->isGift = $_isGift;
		
	}

	public function getIsGift(){

		return $this->isGift;
	} 
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return CArticle
	 */
	public function getArticle(){ return $this->article; }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return int
	 */
	public function getQuantity(){ return $this->quantity; }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @param int $quantity
	 * @return void
	 */
	public function setQuantity( $quantity ){ $this->quantity = intval( $quantity ); }
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le prix unitaire HT avant remise
	 */
	public function getPriceET(){
	
		if( B2B_STRATEGY )
			return $this->article->get( "sellingcost" );

		return round( $this->article->get( "sellingcost" ) / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le prix unitaire TTC avant remise
	 */
	public function getPriceATI(){
	
		if( !B2B_STRATEGY || !Basket::getInstance()->applyVAT() )
			return $this->article->get( "sellingcost" );
			
		return round( $this->article->get( "sellingcost" ) * ( 1.0 + $this->getVATRate() / 100.0 ), 2 );

	}

	//----------------------------------------------------------------------------
	/**
	 * @return return float le prix unitaire HT après remise
	 */
	public function getDiscountPriceET(){ 


		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		
		return $this->article->getDiscountPriceET( $idbuyer, $this->quantity ); 
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le prix unitaire TTC après remise
	 */ 
	public function getDiscountPriceATI(){ 
		
		if( !Basket::getInstance()->applyVAT() )
			return $this->getDiscountPriceET();
			
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		
		return $this->article->getDiscountPriceATI( $idbuyer, $this->quantity ); 
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return return float le taux de remise
	 */
	public function getDiscountRate(){
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		//var_dump($this->quantity);
		return $this->article->getDiscountRate( $idbuyer, $this->quantity );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le taux de TVA à appliquer
	 */
	public function getVATRate(){ return Basket::getInstance()->applyVAT() ? $this->article->getVATRate() : 0.0; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return float le poids unitaire
	 */
	public function getWeight(){ return $this->article->get( "weight" ); }
	public function getIdproduct(){ return $this->article->get( "idproduct" ); }
	public function getReference(){ return $this->article->get( "reference" ); }
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return bool true si le produit est vendu franco, sinon false
	 */
	public function isPostagePaid(){ 
		
		return DBUtil::getDBValue( "franco", "product", "idproduct", $this->article->get( "idproduct" ) ) == 1; 

	}
		
	//----------------------------------------------------------------------------
	/**
	 * @todo deprecated
	 */
	public function getTotalET(){
        $totalPrice = ($this->getDiscountPriceET() * $this->quantity );
	    return $totalPrice;
	}
	public function getTotalATI(){ return $this->getDiscountPriceATI() * $this->quantity; }
	
	/**
	 * @deprecated préférer BasketItem::getArticle()::get()
	 * @param $key
	 */
	public function get( $key ){ return $this->getArticle()->get( $key ); }
	
	//----------------------------------------------------------------------------
	
	public function getIndex(){
		
		$i = 0;
		while( $i < Basket::getInstance()->getItemCount() ){
			
			if( Basket::getInstance()->getItemAt( $i ) === $this )
				return $i;
				
			$i++;
			
		}
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return Color or false
	 */
	public function getColor(){

		$it = $this->article->getOptions()->iterator();
		
		while( $it->hasNext() )
			if( ( $option = $it->next() ) instanceof Color )
				return $option;

		return false;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return Cloth or false
	 */
	public function getCloth(){

		$it = $this->article->getOptions()->iterator();
		
		while( $it->hasNext() )
			if( ( $option = $it->next() ) instanceof Cloth )
				return $option;

		return false;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>