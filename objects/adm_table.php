<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des tables (stdform)
 */

include_once( dirname( __FILE__ ) . "/adm_base.php" );
include_once( dirname( __FILE__ ) . "/Codifier.php" );

class ADMTABLE extends ADMBASE{
	
	//--------------------------------------------------------------------------------------
	
	protected $tablename;
	protected $tabledesc = false;
	protected $design = false;
	protected $Array_DisplayFields = false;
	public $current_record = false;
	protected $location = '';
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE()	Contructeur
	 * @param string $tblname Nom de la table
	 */
	function __construct($tblname){
		
		parent::__construct();
		$this->tablename = $tblname;
		$this->load_design();
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nom de la table editée
	 * @return string
	 */
	public function getTableName(){ return $this->tablename; }
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Retourne une propriété donnée de la table éditée
	 * @param string $attribute la propriété souhaitée
	 * @return string
	 */
	public function getTableAttribute( $attribute ){ return $this->tabledesc[ $attribute ]; }
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::load_design()	Recherche les informations sur la table dans les tables desc_table et desc_field 
	 * @return void
	 */
	protected function load_design()
	{
		$rs = DBUtil::getConnection()->Execute("SELECT `export_avalaible`, `export_name`, `description`, `tablekeys` FROM `desc_table` WHERE `tablename`='".$this->tablename."'");

		if($rs->EOF) exit($this->tablename." : table not exists!");
		//convert tablekeys into an array
		$this->tabledesc = $rs->fields;
		$keys = explode(",",$this->tabledesc['tablekeys']);
		$this->tabledesc['tablekeys']=array();
		foreach($keys as $v) $this->tabledesc['tablekeys'][]=trim($v);
		//get fields
		$this->design = DBUtil::getConnection()->GetAll("SELECT * FROM desc_field WHERE tablename='".$this->tablename."' ORDER BY order_view ");
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::KeyRecord()	Initialise k pour réafficher l'enregistrement
	 * 
	 * @return	array	Valeurs encodées de l'enregistrement
	 */
	public function KeyRecord()
	{
		$x = array();
		$k = array();
		foreach( $this->tabledesc['tablekeys'] as $key) {
			$x[$key] = $this->current_record[$key];
		}
		$k[] = base64_encode(serialize($x)); // identifiant d'enregistrement
		return $k;
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::GetDesign()	Retourne un tableau contenant les infos sur la table
	 * 
	 * @return	array	Tableau contenant les infos sur la table
	 */	
	public function GetDesign()
	{
		return $this->design;
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::store()	Enregistre les champs de la table passée par la méthode POST
	 * @param bool $full ??? ( optionnel, false par défaut )
	 */
	public function store($full=false)
	{
		$n = count($this->design);
		for ($i=0 ; $i<$n ; $i++) {
			$f = $this->design[$i]['fieldname'];
			if (isset($_POST[$f])) {
				if ($f == 'alias')
				{
					if($_POST['choix2'] == '1')
						$this->current_record[$f] = $_POST[$f];
				}
				elseif ($f == 'url_video_youtube')
				{
					$filtered_url = str_replace('https://', '', $_POST[$f]);
					$this->current_record[$f] = $filtered_url;
				}
				else
				{
					$this->current_record[$f] = $_POST[$f];
				}
				if($full) $this->set($f, $_POST[$f]);
			}
		}
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un champ de la table
	 * @param	string	$Field	Nom du champ
	 */
	public function get($Field){
		
		$n = count($this->Array_DisplayFields);
		for ($i=0 ; $i<$n ; $i++) {
			if ($this->Array_DisplayFields[$i]['fieldname'] == $Field) {
				return $this->Array_DisplayFields[$i]['value'];
			}
		}
		
		return false;
		
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Place une valeur pour un champ de la table
	 * @param string $Field Nom du champ
	 * @param String $Value Valeur
	 * @return void
	 */
	function set($Field, $Value){
		
		$n = count($this->Array_DisplayFields); //$n = $this->GetFieldsForEditing();
		for($i = 0 ; $i < $n ; $i++) {
			if($this->Array_DisplayFields[$i]['fieldname'] == $Field) {
				$this->Array_DisplayFields[$i]['value'] = $Value;
				break; 
			}
		}
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Place une valeur pour un champ de la table
	 * @param string $Field Nom du champ
	 * @param String $Value Valeur
	 * @return void
	 */
	function add($Field, $Value) {
		$n = count($this->Array_DisplayFields);
		$new = true;
		for($i = 0 ; $i < $n ; $i++) {
			if($this->Array_DisplayFields[$i]['fieldname'] == $Field) {
				$this->Array_DisplayFields[$i]['value'] = $Value;
				$new = false;
				break;
			}
		}
		if($new)
			$this->Array_DisplayFields[] = array('fieldname'=>$Field,'value'=>$Value);
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	* Récupère les valeurs par défaut de la table desc_field
	* @return void
	*/
	function set_defaults() {
		$n = count($this->design);
		for($i = 0 ; $i < $n ; $i++) {
			if(!$this->design[$i]['edit_avalaible'])
				continue;
			$f = $this->design[$i]["fieldname"];
			if(isset($_POST[$f]))
				$this->current_record[$f] = $this->design[$i]["default_value"];
		}
	}

	//--------------------------------------------------------------------------------------
	
	/**
	* Initialise l'enregistrement avec les valeurs passées en paramètres
	* @param array $ArrayRecord Tableau associatif avec les valeurs
	* @param boolean $EditOnly Vrai (par défaut) si on ne prend que les champs éditables
	* @return void
	*/
	function set_record(array $ArrayRecord,$EditOnly=true)
	{

		$n = count($this->design);
		for ( $i=0 ; $i<$n ; $i++ )
		{
			if ( $EditOnly && !$this->design[$i]['edit_avalaible'] )
				continue;
			$f = $this->design[$i]["fieldname"];
			if ( isset($ArrayRecord[$f]) )
				$this->current_record[$f] = $ArrayRecord[$f];
		}

	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Initialise le chemin pour le téléchargement
	 * @param string $path
	 * @return void
	 */
	function set_location($path) {
		$this->location = $path;
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Retourne le type d'un champ [int,char,dec]
	 * @param string $field Nom du champ
	 * @return string Le type de champ ou 'char' si le champ n'est pas trouvé
	 */
	function get_type($field){
		
		$n = count($this->design);
		for($i=0;$i<$n;$i++)
			{
			if( $this->design[$i]["fieldname"] == $field) return $this->design[$i]["type"];
			}
		return 'char';
		
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Retourne le cahmp 'help' d'un champ
	 * @param string $field Nom du champ
	 * @return string Le type de champ ou '' si le champ n'est pas trouvé
	 */
	function get_help($field)
	{
		$n = count($this->design);
		for($i=0;$i<$n;$i++)
		{
			if( $this->design[$i]["fieldname"] == $field) return $this->design[$i]["help"];
		}
		return '';
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::GetNewID()	Fonction qui recherche un id libre
	 * Selon le client, la codification des Id peut ètre personalisée
	 * AXESS : 10 0001 (idcategory idproduct)
	 * default : auto_increment
	 * @param	string	$Field	Nom du champ
	 * @return	int		L'id libre
	 */
	function GetNewID( $field ){
	
		global $GLOBAL_START_PATH;
		
		include_once( "$GLOBAL_START_PATH/objects/Codifier.php" );
		
		$codifier = new Codifier();
		
		if( $this->tablename == "category" && $field == "idcategory" ){
		
			$idcategoryparent = $this->get('idcategory');
			$ret = $codifier->getNewCategory( $idcategoryparent );
			
		}
		else if( $this->tablename == "product" && $field == "idproduct" ){

			$idcategoryparent = $this->get('idcategory');
		
			if( empty( $idcategoryparent ) )
				$idcategoryparent = $this->current_record['idcategory'];
		
			$ret = $codifier->getNewProduct( $idcategoryparent );

		}
		else if( $this->tablename == "detail" && $field == "reference" ){
		
			$idproduct = $this->get('idproduct');
			
			if( empty( $idproduct ) ){
			
				$idproduct = $this->current_record[ "idproduct" ];
				
				if (empty($idproduct)){
				
					$idproduct = "100001";
				
				}
				
			}
					
			$ret = $codifier->getNewReference( $idproduct );
				
		}
		else{
		
			if( !isset( $_REQUEST[ "append" ] ) && !isset( $_REQUEST[ "append_y" ] ) && $this->isEditable( $field ) && ( isset( $this->current_record[ $field ] ) && !empty( $this->current_record[ $field ] ) ) )
				$ret = $this->current_record[ $field ];
			else{
		
				$row = DBUtil::getConnection()->GetRow( "SELECT MAX( `$field` ) + 1 AS newid FROM `{$this->tablename}`" );
				$ret = empty( $row ) ? 1 : $row[ "newid" ];
				
			}
					
		}

		if( $ret === false )
			trigger_error( "ADMTABLE::GetNewID( $field ) => Erreur", E_USER_ERROR );

		$this->current_record[ $field ] = $ret;
		
		return $ret;

	}
	
	//--------------------------------------------------------------------------------------
	public function Error($Str_ErrContext, $Str_ErrMessage, $Int_ErrCode ="")
	{
		echo "<br /><span class=\"mes_error\">";
		echo Dictionnary::translate("fatal_error");
		echo "";
		echo $Str_ErrContext;
		echo "</span>";
		echo "<br /><span class=\"mes_error\">";
		echo $Str_ErrMessage;
		echo "</span>";
		echo "<br />";
		exit($Int_ErrCode);
	}
	/**
	 * Retourne un tableau avec les champs disponibles pour la recherche
	 * @return array Tableau avec les infos sur les champs
	 */
	function GetFieldsForSearching()
	{
	$Array_DisplayFields=array();// in $Array_DisplayFields the fields
	$n = count($this->design);
	if ($n < 1) $this->Error(" ".Dictionnary::translate("cell_desk_field")." ", 001);// At least one row to display
		$ii=0;
		for($i=0;$i<$n;$i++)
		{
		if(!$this->design[$i]['search_avalaible']) continue;
		$Array_DisplayFields[$ii]['fieldname']=$this->design[$i]['fieldname'];
		$Array_DisplayFields[$ii]['type']=$this->design[$i]['type'];
		$Array_DisplayFields[$ii]['size']=$this->design[$i]['size'];
		$Array_DisplayFields[$ii]['help']=$this->design[$i]['help'];
		$Str_SelectTableField = $this->design[$i]['search_select_table']; // table and field names separated by ':'
		if($Str_SelectTableField)
			{
			$Arr_SelectTableField = explode(":",$Str_SelectTableField);
			$Array_DisplayFields[$ii]["search_select_table"] = $Arr_SelectTableField[0];
			$Array_DisplayFields[$ii]["search_select_orig_field"] = $Arr_SelectTableField[1];
			if(isset($Arr_SelectTableField[2])) { 
				$Array_DisplayFields[$ii]["search_select_final_field"] = $Arr_SelectTableField[2];
			} else {
				$Array_DisplayFields[$ii]["search_select_final_field"]=$Arr_SelectTableField[1];
			}
			}
		else $Array_DisplayFields[$ii]["search_select_table"]='';
		$ii++;
		}
	 return $Array_DisplayFields;
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Recherche les champs disponibles pour l'éditon
	 * @return int le nombre de champs trouvés 
	 */
	function GetFieldsForEditing()
	{
	$this->Array_DisplayFields = array();// in $this->Array_DisplayFields the fields

	$n = count($this->design);

	if ($n < 1) Error(" ".Dictionnary::translate("cell_desk_field")." ", 001);// At least one row to display
		$ii = 0;
		for($i = 0 ; $i < $n ; $i++) {
			if(!$this->design[$i]['display_avalaible']) continue;
			if(!$this->design[$i]['edit_avalaible']) continue;

			$this->Array_DisplayFields[$ii]["fieldname"]=	$this->design[$i]["fieldname"];
			$this->Array_DisplayFields[$ii]["type"]=	$this->design[$i]["type"];
			$this->Array_DisplayFields[$ii]["size"]=	$this->design[$i]["size"];
			$this->Array_DisplayFields[$ii]["help"]=	$this->design[$i]["help"];
			$this->Array_DisplayFields[$ii]["value"]=	$this->design[$i]["default_value"];
			$this->Array_DisplayFields[$ii]["export_name"]=	$this->design[$i]["export_name"];
			$Str_SelectTableField = $this->design[$i]["modify_select_table"]; // table and field names separated by ':'
			if($Str_SelectTableField) {
				$Arr_SelectTableField = explode(":",$Str_SelectTableField);
				$this->Array_DisplayFields[$ii]["modify_select_table"] = $Arr_SelectTableField[0];
				$this->Array_DisplayFields[$ii]["modify_select_orig_field"] = $Arr_SelectTableField[1];
				$this->Array_DisplayFields[$ii]["modify_select_final_field"] = $Arr_SelectTableField[2];
			}
			else $this->Array_DisplayFields[$ii]["modify_select_table"]='';
			//echo "III $i $ii ".$this->design[$i]["fieldname"]."/".$this->Array_DisplayFields[$ii]["fieldname"]."III";
			$ii++;
		}
		return $ii;
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Charge un enregistrement de la BDD
	 * @param $Select Condition SQL
	 * @param bool $getfields
	 * @return array un tableau avec les champs éditables
	 */
	public function LoadRecord($Select,$getfields=true){
		
		if($getfields) $this->GetFieldsForEditing();
		//$n = count($this->Array_DisplayFields);// echo "<pre>xx"; print_r($thi>Array_DisplayFields) ; echo "</pre>";
		$n = count($this->design);
		$Str_FieldsDisplayed ='';
		//for ($i=0;$i<$n;$i++) $Str_FieldsDisplayed .= ','.$this->Array_DisplayFields[$i]["fieldname"];
		$Str_FieldsDisplayed=substr($Str_FieldsDisplayed,1);
		//echo "SQL select $Str_FieldsDisplayed from ".$this->tablename.' '.$Select." SQL";
		//$rs = DBUtil::getConnection()->Execute("select $Str_FieldsDisplayed from ".$this->tablename.' '.$Select);
		$rs = DBUtil::getConnection()->Execute( "SELECT * FROM `" . $this->tablename . "`" . $Select );
		$data = $rs->fields;
		for ($i=0;$i<$n;$i++)
			{
			$Tmp_FieldName=$this->design[$i]["fieldname"];
			if(isset($data[$Tmp_FieldName])) $fn=$data[$Tmp_FieldName];
			else continue; //missing in desc_field!!!
			$this->design[$i]["value"]=$data[$Tmp_FieldName];
			$this->current_record[$Tmp_FieldName]=$data[$Tmp_FieldName];
			}
		$n = count($this->Array_DisplayFields);
		for ($i=0;$i<$n;$i++)
			{
			$Tmp_FieldName=$this->Array_DisplayFields[$i]["fieldname"];
			if( isset($data[$Tmp_FieldName]) ) $this->Array_DisplayFields[$i]["value"] = $data[$Tmp_FieldName];
			}
		//echo "<pre>";print_r($this->Array_DisplayFields);echo "</pre>";
		return $this->Array_DisplayFields;
		
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Crée un enregsitrement avec les valeurs par défaut
	 * @param nt $IdCategory Optionnel - id de catégorie
	 * @return array un tableau avec les champs éditables
	 */
	public function MakeEmptyRecord($IdCategory = 0) {

		$this->GetFieldsForEditing();
		$n = count($this->Array_DisplayFields);// echo "<pre>xx"; print_r($thi>Array_DisplayFields) ; echo "</pre>";
		for ($i = 0 ; $i < $n ; $i++) {
			if($this->Array_DisplayFields[$i]["help"] == 'date') $this->Array_DisplayFields[$i]["value"]= date('Y-m-d');
			elseif($IdCategory && $this->Array_DisplayFields[$i]['fieldname'] == 'idcategory' )  $this->Array_DisplayFields[$i]["value"]= $IdCategory;
		}
		return $this->Array_DisplayFields;

	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Met à jour un enregsitrement dans la BDD
	 * @param string $Str_Condition Optionnel - Condition SQL
	 * @return int le nombre d'enregistrements modifiés dans la base de données
	 */
	public function ModifyRecord($Str_Condition=false){
	
		if( !$this->checkRequiredFields() )
			return 0;
			
		if(!$Str_Condition)
			{	
			$Str_Condition ="WHERE ".$_POST['id_index0_name']." = '".$_POST['id_index0_value']."'";
			if( isset($_POST['id_index1_name']) && isset($_POST['id_index1_value']) ) $Str_Condition .=" AND ".$_POST['id_index1_name']." = '".$_POST['id_index1_value']."'";
			}
		$Array_ModifyFields = &$this->current_record;
		
		if (count($Array_ModifyFields)=="0") // Si nous n'avons pas des fields pour edition
			{
				Error("ModifyRecord", " ".Dictionnary::translate("cell_desk_field")." ", 003);
				return(0);
			}
		// si un champ username ou lastupdate, on l'initialise
		// Change le sens de la date pour les champs de date
		$n = count($this->design);
		for($i=0;$i<$n;$i++)
		{
			if($this->design[$i]['fieldname'] == 'username') 
				$Array_ModifyFields['username']=User::getInstance()->get("login");
			if($this->design[$i]['fieldname'] == 'lastupdate') 
				$Array_ModifyFields['lastupdate']=date("Y-m-d H:i:s");	
			
			if($this->design[$i]['type'] == 'date' && $this->design[$i]['fieldname'] != 'lastupdate' && isset( $Array_ModifyFields[ $this->design[ $i ][ "fieldname" ] ] ) ) {
				
				$value = $Array_ModifyFields[ $this->design[$i]['fieldname'] ];
				
				if( preg_match( "/^[0-9]{2}-[0-9]{2}-[0-9]{4}\$^/", $value ) ){
			   		
			   		list( $day, $month, $year ) = explode( "-", $value );
			   		$date = "$year-$month-$day";
			   		
				}
				
				// Patch B2C
			   	if(!isset($date)){
			   		$date =  date("Y-m-d"); ;
			   	}
			   	
				$Array_ModifyFields[ $this->design[$i]['fieldname'] ] = $date;
				
			}	
			
		}
	
		//cryptage des mots de passe
		
		$n = count( $this->design );
		$i = 0;
		while( $i < $n ){
		
			if( $this->design[ $i ][ "help" ] == "password" && isset( $Array_ModifyFields[ $this->design[ $i ][ "fieldname" ] ] ) ){
		
				if( strlen( $Array_ModifyFields[ $this->design[ $i ][ "fieldname" ] ] ) )	
					$Array_ModifyFields[ $this->design[ $i ][ "fieldname" ] ] = md5( $Array_ModifyFields[ $this->design[ $i ][ "fieldname" ] ] );
				else unset( $Array_ModifyFields[ $this->design[ $i ][ "fieldname" ] ] ); //ne mettre à jour le mot de passe que s'il a été modifié
			
			}
			
			$i++;
			
		}
		
		$this->UploadFiles();
	
		reset ($Array_ModifyFields);
		$Str_ModifyFields=''; //local fields 2modify
		foreach($Array_ModifyFields as $k=>$v){ //browsing key names from passed array
			
			//$Str_ModifyFields.=", `$k` = '".addslashes(stripslashes(Util::doNothing($v)))."'";
            $Str_ModifyFields.=", `$k` = '".addslashes(stripslashes($v))."'";
		}
		
		$Str_ModifyFields=substr($Str_ModifyFields,1); // drop first ,
		$sql_update = "UPDATE `".$this->tablename."` SET $Str_ModifyFields $Str_Condition";
		$res= DBUtil::getConnection()->Execute($sql_update);

			if (!$res) {
				trigger_error("ADMTABLE::ModifyRecord => sql_search=".$sql_update, E_USER_ERROR);
				return 0;
		}
	
		return DBUtil::getConnection()->Affected_Rows() ; //return number of affected rows
	
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si tous les champs obligatoires ont bien été renseignés
	 * @return bool true si tous les champs obligatoires ont bien été renseignés, sinon false
	 */
	protected function checkRequiredFields(){
	
		foreach( $this->current_record as $fieldname => $value ){
		
			if( $this->isRequired( $fieldname ) && $value === "" ){ //@todo enregistrements avec clé primaire = 0
			
				$export_name = $this->getExportName( $fieldname );
				
				$this->Warning( Dictionnary::translate( "missing_required_fields" ), str_replace( "%fieldname", $export_name, Dictionnary::translate( "missing_required_field" ) ) );
				
				return false;
				
			}
		
		}
		
		return true;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nom d'export d'une colonne de la base de données
	 * @param string le nom de la colonne
	 * @return string
	 */
	protected function getExportName( $fieldname ){
	
		foreach( $this->design as $index => $fieldInfos ){
		
			if( $fieldInfos[ "fieldname" ] == $fieldname )
				return $fieldInfos[ "export_name" ];
				
		}
		
		return $fieldname;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si un champ est obligatoire ou non
	 * @param string $fieldname le champ à tester
	 * @return bool true si le champ $fieldname est obligatoire, sinon false
	 */
	public function isRequired( $fieldname ){
	
		foreach( $this->design as $index => $fieldInfos ){
		
			if( $fieldInfos[ "fieldname" ] == $fieldname )
				return $fieldInfos[ "mandatory" ] == 1;
				
		}
		
		return false;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::InsertRecord()		Insertion d'un enregistrement dans la BDD
	 * @param	string	$UniqueKeyName	Le nom éventuel de l'index primaire
	 * @return int 1 en cas de succès, sinon 0
	 */
	public function InsertRecord($UniqueKeyName='')
	{
		/*
		 * @todo : même si l'insert échoue et que la fonction retourne autre chose que 1, 
		 * tous les formulaires de l'administration passent en mode 'édition' alors qu'il n'y a
		 * rien à éditer vu que l'insert a échoué
		 */
		$Str_NameTable = $this->tablename;
		$Array_InsertParams = &$this->current_record;
		
		if (empty($Array_InsertParams)) { // No parameters in $Array_InsertParams!
			echo "FAdministrativeInsertValues , Vous devez passer des parametres pour l'enregistrement dans la table $Str_NameTable";
			return 0;
		}
		
		$this->UploadFiles();
		
		reset ($this->tabledesc['tablekeys']);
		$Str_KeyNames = '';
		$Str_KeyFind = '';
		// Recherche de l'existance selon la liste des clefs primaires de la table
		
		//initialiser les clés primaires
		
		foreach ($this->tabledesc['tablekeys'] as $k) {
			
			if ($UniqueKeyName == $k) {
				
				if ($UniqueKeyName == 'idarticle'){
					
					$codifier = new Codifier();
					$Array_InsertParams[$k] = $codifier->getNewArticle();
					
				}
				else $Array_InsertParams[$k] = $this->GetNewID($UniqueKeyName);
				
			}
			
			$Str_KeyNames .= ", `$k`";
			$Str_KeyFind .= "AND `$k` = '" . $Array_InsertParams[$k] . "' ";
		
		}
		
		//vérifier les champs obligatoires
		
		if( !$this->checkRequiredFields() ){
		
			$this->resetKeys();
			return 0;
			
		}
		
		$Str_KeyNames = substr($Str_KeyNames,1);
		$Str_KeyFind = substr($Str_KeyFind,3);
		$sql_search = "SELECT $Str_KeyNames FROM $Str_NameTable WHERE $Str_KeyFind";

		$rs = DBUtil::getConnection()->Execute($sql_search); //executing fast select query (on index)
		if (!$rs->EOF) {
			$this->Warning(Dictionnary::translate("insert_table")." $Str_NameTable ".Dictionnary::translate("insert_allready")." $Str_KeyFind", " ".Dictionnary::translate("data_verify")." ");
			return 0;
		}

		reset ($Array_InsertParams);
		$Arr_FieldsInsert = array(); //fields inserted
		$Arr_ValuesInsert = array(); // values inserted
		$Arr_FieldsInsert = array(); //fields inserted
		$Date_Fields = array(); // date fields to be treated separately
		/*
		$Arr_FieldsInsert[] = 'username';
		$Arr_ValuesInsert[] = "'".addslashes(stripslashes(User::getInstance()->get("login")))."'";
		$Arr_FieldsInsert[] = 'lastupdate';
		$Arr_ValuesInsert[] = "'".date('Y-m-d H:i:s')."'";
		*/
		$n = count($this->design);
		for($i=0;$i<$n;$i++)
		{
			if($this->design[$i]['fieldname'] == 'username'){
				$Arr_FieldsInsert[] = 'username';
				$Arr_ValuesInsert[] = "'".addslashes(stripslashes(User::getInstance()->get("login")))."'";
			} 
			if($this->design[$i]['fieldname'] == 'lastupdate') {
				$Arr_FieldsInsert[] = 'lastupdate';
				$Arr_ValuesInsert[] = "'".date('Y-m-d H:i:s')."'";
			}
			if($this->design[$i]['type'] == 'date') {
				$Date_Fields[] = $this->design[$i]['fieldname'];
			}
			
		}
		//var_dump($Array_InsertParams);die;
		foreach ($Array_InsertParams as $field=>$value) {
			if( $field != 'username' && $field != 'lastupdate' ) {
				
				if( in_array( $field, $Date_Fields ) ) {
					
					$Arr_FieldsInsert[] = "`$field`";
					
					if( preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $value ) ){
			   		
			   			list( $day, $month, $year ) = explode( "-", $value );
			   			$date = "$year-$month-$day";
			   			
					}
			   			
					$Arr_ValuesInsert[] = "'$date'";
					
					
				}
				else{
					
					$Arr_FieldsInsert[] = "`$field`";
					$Arr_ValuesInsert[] = "'".addslashes(stripslashes($value))."'";
					
				}
				
			}
		}
	
		//cryptage des mots de passe
		
		$i = 0;
		while( $i < count( $this->design ) ){
		
			if( $this->design[ $i ][ "help" ] == "password" ){

				$fieldname = "`" . $this->design[ $i ][ "fieldname" ] . "`";
				
				$j = 0;
				$encrypted = false;
				while( !$encrypted && $j < count( $Arr_FieldsInsert ) ){
					
					if( $Arr_FieldsInsert[ $j ] == $fieldname ){
		
						//suppression des quotes
						
						$value = str_replace( "'", "", $Arr_ValuesInsert[ $j ] );
		
						$Arr_ValuesInsert[ $j ] = "'" . md5( $value ) . "'";
						$encrypted = true;

					}
					
					$j++;
					
				}
				
			}
				
			$i++;
			
		}
		
		$Str_FieldsInsert = implode(', ',$Arr_FieldsInsert);
		$Str_ValuesInsert = implode(', ',$Arr_ValuesInsert);
		$sql_insert = "INSERT INTO `$Str_NameTable` ($Str_FieldsInsert) VALUES($Str_ValuesInsert)";
		
		//var_dump($sql_insert);die;
		$res = DBUtil::getConnection()->Execute($sql_insert); //executing insert query

		if (!$res) trigger_error("ADMTABLE::InsertRecord => sql_search=".$sql_insert, E_USER_ERROR);
		return 1;
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Supprime des enregistrements dans la BDD
	 * @param string $Str_Condition la clause SQL WHERE
	 * @return int le nombre de lignes supprimées dans la BDD
	 */
	public function DeleteRecords($Str_Condition){
		
		if ($Str_Condition) {
		$Str_Condition = $this->SerialDecode($Str_Condition);
		//echo "XXDeleteRecords $Str_Condition XX";
		DBUtil::getConnection()->Execute("DELETE FROM `".$this->tablename."` $Str_Condition"); //delete some records
		if ($this->tablename == "category") {
			// Effacement category_link
			$CatLinkQuery = $this->CreateCatLinkDeleteQuery($Str_Condition);
			if ($CatLinkQuery != "")
				$res = DBUtil::getConnection()->Execute("DELETE FROM `category_link` $CatLinkQuery");
				if(!$res) echo "category_link DELETE ERROR $CatLinkQuery ";
				// echo "YYDeleteRecords $CatLinkQuery YY";
			}
		} else {
			Error("DeleteRecords", "Vous devez passer une condition pour effacer de la table ". $this->tablename , 004);
		}
		return DBUtil::getConnection()->Affected_Rows(); // return number of affected rows
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Construit la condition SQL pour supprimer un enreg dans catagory_link
	 * @param string $Condition La condition sql pour la table category
	 * @return string
	 */
	protected function CreateCatLinkDeleteQuery($Condition)
	{
		$ExpSC=explode("OR",$Condition);$Query='';
		for($i=0 ; $i < count($ExpSC) ; $i++)
		{
			$ExpSC1=explode("=",$ExpSC[$i]);
			$IdCategory=trim($ExpSC1[1]);
			$Query.="OR idcategoryparent=$IdCategory OR idcategorychild=$IdCategory ";
		}
		if($Query!="")
		{
			$Query=substr($Query,2);
			$Query="where $Query";
		}
		return $Query;
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * fonction pour faire l'upload de fichiers
	 * @return void
	 */
	protected function UploadFiles(){
		
		global $GLOBAL_START_PATH;
		
		if( empty( $_FILES ) )
			return;
		
		//validation des noms de fichier
		
		$directory = $GLOBAL_START_PATH . "/www/" . $this->location;
		
	 	foreach( $_FILES as $Str_LoadFileName => $v ){
			
			if( preg_match( "'(upload_)(.*)'", $Str_LoadFileName, $regs ) ){	
				$Str_FieldName = $regs[ 2 ];
				$help = $this->get_help( $Str_FieldName );
				
				if( $help != "upload" )
					continue; // pas de type upload ou pas un champ de la table!
				
				if( isset( $this->current_record[ $Str_FieldName ] ) )
					$default_value = $this->current_record[ $Str_FieldName ];
				else
					$default_value = $this->get( $Str_FieldName );
				
				if( !empty( $_FILES[ $Str_LoadFileName ][ "name" ] ) ){ //test si un fichier a été chargé
					
					if( $_FILES[ $Str_LoadFileName ][ "error" ] == UPLOAD_ERR_OK && $_FILES[ $Str_LoadFileName ][ "size" ] > 0 ){ // test si ça s'est bien passé
					
						if( !is_dir( $directory ) )
							$this->Warning( Dictionnary::translate( "repertory_attention" ) . " $directory " . Dictionnary::translate( "no_exist" ), " " . Dictionnary::translate( "no_exist" ) . " " );
						else{
							
							$imagename = $_FILES[ $Str_LoadFileName ][ "tmp_name" ];
							
							if( !empty( $default_value ) ){
								
								if( preg_match( "/([^/]*)$/", $default_value, $regss ) )
									$imagename_name = $regss[ 1 ];
								else
									$imagename_name = $default_value;
								
							}else
								$imagename_name = $this->validateFileName( $_FILES[ $Str_LoadFileName ][ "name" ] );
							
							$path = $directory . "/" . $imagename_name;
							
							if( is_file( $path ) )
								unlink( $path );
							
							if( !@copy( $imagename, $path ) )
								$this->Warning( Dictionnary::translate( "error_copy_list" ), " $path " . Dictionnary::translate( "writting_ok" ) . " " );
							else{
								
								chmod( $path, 0777 );
								$this->set( $Str_FieldName, $this->location . "/" . $imagename_name );
								$this->current_record[ $Str_FieldName ] = $this->location . "/" . $imagename_name;
								
							}
							
						}
						
					}else
						$this->Warning( Dictionnary::translate( "error_upload" ) . " " . $Str_LoadFileName . " " . $_FILES[ $Str_LoadFileName ][ "name" ] . " " . $_FILES[ $Str_LoadFileName ][ "size" ], " " . Dictionnary::translate( "error_upload" ) . " " ); // echo "<br /> ". Dictionnary::translate("error_upload")." !<br /> ";
					
				}
				
			} //eregi
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Affichage d'une liste déroulante
	 * Si la table dans laquelle on recherche les valeurs est vide
	 * alors on affiche un input de type text.
	 * @param string $Field Nom du champ dans la table courante
	 * @param string $Str_Action modify ou insert
	 * @param string $Str_Default Valeur sélectionnée
	 * @return void
	 */
	public function SelectField($Field, $Str_Action, $Str_Default = ""){
		
		//$this->GetFieldsForEditing();
		$n = count($this->Array_DisplayFields);
		for($i=0;$i<$n;$i++) 
		{
			if( $this->Array_DisplayFields[$i]['fieldname'] == $Field)
			{
				$Array_Fields = &$this->Array_DisplayFields[$i];
			
				break;
			}
		}
		
		ADMFORM::SelectField($Array_Fields,$Str_Action,$Str_Default);
			
		
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Affiche toute la table
	 * @return void
	 */
	public function dump(){
		
		$rs = DBUtil::getConnection()->Execute("select * from ".$this->tablename);
		echo "<pre>\n";
		while (!$rs->EOF) {
			print_r($rs->fields);
			$rs->MoveNext();
			}
		echo "</pre>\n";
		
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::GetTableRow()
	 * Retourne un enregistrement d'une table selon $Str_Condition
	 * @param string $Str_Condition Condition SQL WHERE
	 * @return bool FALSE en cas d'erreur
	 */
	public function GetTableRow($Str_Condition=''){
		
		$Str_NameTable = $this->tablename;
		$SQL_Query = "SELECT * FROM `$Str_NameTable` $Str_Condition";
		$data = DBUtil::getConnection()->GetRow($SQL_Query);

		if ($data) return $data;
		return false;
	
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * ADMTABLE::GetTableRows()
	 * Retourne les enregistrements d'une table selon $Str_Condition
	 * @param string $Str_Condition Condition SQL WHERE
	 * @return bool FALSE en cas d'erreur
	 */
	public function GetTableRows($Str_Condition=''){
		
		$Str_NameTable = $this->tablename;
		$SQL_Query = "SELECT * FROM `$Str_NameTable` $Str_Condition";
		$Rs = DBUtil::getConnection()->Query($SQL_Query);
		if (!$Rs) trigger_error("ADMTABLE::GetTableRows => SQL_Query=".$SQL_Query, E_USER_ERROR);
		$Arr_Fields=array();

//		$Arr_Fields[$Rs->_currentRow] = $Rs->fields;
//		$Rs->MoveNext();
		while (!$Rs->EOF) {

			$Arr_Fields[$Rs->_currentRow] = $Rs->fields;
			$Rs->MoveNext();
		}

		return $Arr_Fields;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Formate une nom de fichier en remplaçant les caractères non valides
	 * @param string $filename un nom de fichier à tester
	 * @return string un nom de fichier valide
	 */
	protected function validateFileName( $filename ){
		
		$pattern = "[^a-zA-Z0-9/.]";
	
		return preg_replace( $pattern, "_", $filename );
	
	}

	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifier si d'autres tables vont être affectées par la suppression de données dans la table courante
	 * Si c'est le cas, affiche un message d'erreur
	 * @return void
	 */
	public function hasForeignKeyWarnings(){
		
		global $GLOBAL_START_URL;
		
		$warnings = 0;
		
		$query = "
		SELECT present_in, keyname
		FROM relations
		WHERE tablename LIKE '{$this->tablename}'
		AND present_in NOT LIKE ''";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la liste des autres tables pouvant être affectées par la suppression de données dans la table '{$this->tablename}'" );
			
		if( !$rs->RecordCount() )
			return 0;

		$where = $this->SerialDecode( $_POST[ "delete_condition" ] );
		
		while( !$rs->EOF() ){
		
			$present_in = $rs->fields( "present_in" );
			$keyname = $rs->fields( "keyname" );
			
			$tables = explode( " ", $present_in );
			
			foreach( $tables as $tablename ){
			
				$query = "SELECT `$keyname` FROM `{$this->tablename}` $where";
				
				$rs2 = DBUtil::getConnection()->Execute( $query );
				
				if( $rs2 === false )
					die( "$query Impossible de récupérer les colonnes liées à d'autre tables dans les lignes supprimées" );
				
					while( !$rs2->EOF() ){
					
						$value = $rs2->fields( $keyname );
						
						$query = "SELECT `$keyname` FROM `$tablename` WHERE `$keyname` = '$value'";
						
						$rs3 = DBUtil::getConnection()->Execute( $query );
						
						if( $rs3 === false )
							die( "$query Impossible de récupérer les lignes affectées par suppression des données dans les autres tables" );

						$affected_rows = $rs3->RecordCount();
						
						if( $affected_rows ){
						
							?>
							<p class="msg">
								<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/alert.gif" alt="" style="vertical-align:text-bottom;" />
								&nbsp;Vous ne pouvez pas supprimer de données dans la table <?php echo $this->tablename ?> pour le critère suivant : <i><?php echo $keyname ?> = <?php echo $value ?></i>
								car Il reste <u><?php echo $affected_rows ?> enregistrement(s)</u> dans la table <b><?php echo $tablename ?></b> avec <i><?php echo $keyname ?> = <?php echo $value ?></i>
							</p>
							<?php
							
							$warnings++;
							
							$rs3->MoveNext();

						}
						
						$rs2->MoveNext();
						
					}
			}
			
			$rs->MoveNext();
			
		}
		
		return $warnings;
			
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si un champ est éditable ou non
	 * @param string $field le om du champ
	 * @return bool true si le champ est éditable, sinon false
	 */
	protected function isEditable( $field ){
	
		$i = 0;
		while( $i < count( $this->design ) ){
		
			if( $this->design[ $i ][ "fieldname" ] == $field )
				return $this->design[ $i ][ "edit_avalaible" ] == 1;
				
			$i++;
			
		}
		
		return false;
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Correctif : Efface les valeurs des clés primaires si elles sont à zéro
	 * @todo il vaudrait mieux ne pas générer des valeur de clés primaires nulles
	 * @return void
	 */
	function resetKeys(){
	
		foreach( $this->tabledesc[ "tablekeys" ] as $key ){
		
			if( !isset( $_POST[ $key ] ) || empty( $_POST[ $key ] ) )
				if( isset( $this->current_record[ $key ] ) )
					$this->current_record[ $key ] = "";
			
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Affiche un message d'erreur mais n'arrete pas l'execution du script
	 *
	 * @param       string  $Str_ErrContext         le contexte de l'erreur
	 * @param       string  $Str_ErrMessage         le message d'erreur
	 */
	private function Warning($Str_ErrContext, $Str_ErrMessage)
	{
	        echo "$Str_ErrContext\n$Str_ErrMessage";
	}

}//EOC
?>