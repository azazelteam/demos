<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des devis
 */

include_once( dirname( __FILE__ ) . "/adm_buyer.php" );
include_once( dirname( __FILE__ ) . "/adm_base.php" );

class ADMESTIMATE extends ADMBASE
{
var $DEBUG = false;
var $id_estimate = 0;
var $buyer = false;
var $estimateinfo = false;
var $estimaterows = false;

	/**
	 * Constructeur
	 * @param int $id Id du devis
	 */	
	function __construct($id=0)
	{
		ADMBASE::ADMBASE();
		$this->id_estimate = $id;
		if($id>0) $this->LoadEstimate();
		//print_r($this->estimateinfo);
	}

	/**
	 * Retourne une valeur d'un article
	 * @param int $i L'index de l'article
	 * @param string $key Le nom de la valeur recherchée
	 */
	function GetItem($i,$key)
	{
	return $this->estimaterows[$i][$key];	
	}
	
	/**
	 * Retrourne une valeur du devis
	 * @param string $key Le nom de la valeur recherchée
	 */
	function Get($key)
	{
	return $this->estimateinfo[$key];	
	}
	
	/**
	 * Retourne une valeur de l'acheteur
	 * @param string $key Le nom de la valeur recherchée
	 */
	function getBuyer($key)
	{
	return $this->buyer->buyerinfo[$key];	
	}
	
	/**
	 * Recherche le devise - nom et taux
	 */
	function GetCurrency()
	{
		$estimateinfoArray['devicevalue']=1.0;
		$IdCurrency = $this->estimateinfo['idcurrency'];
		$lang =  '_'.$this->LingualNumber;
		$res= DBUtil::getConnection()->Execute("SELECT name$lang as devicename ,device FROM currency WHERE idcurrency='$IdCurrency'");
		if($this->DEBUG) echo "SELECT name$lang as devicename as ,device FROM currency WHERE idcurrency='$IdCurrency'";
		if ($res->RecordCount()>0) // At least one row resulted
		{
			$this->estimateinfo['devicevalue']=$res->fields['device'];
			$this->estimateinfo['devicename']=$res->fields['devicename'];
		}
		else
		{
		$this->estimateinfo['devicevalue']=1.0;
		$this->estimateinfo['devicename']='ECU';
		/**
		 * @todo trigger warning
		 */	
		}
	}

	/**
	 * Charge un devis à partir de la base de données
	 */
	function LoadEstimate()
	{
		$IdEstimate = $this->id_estimate;
		$lang =  '_'.$this->LingualNumber;
			
		$this->estimateinfo = DBUtil::getConnection()->GetRow("SELECT * FROM estimate WHERE idestimate=$IdEstimate");
		if($this->estimateinfo) {
			$this->buyer = new ADMBUYER($this->estimateinfo['idbuyer']);
			$this->GetCurrency();
			} else {
				echo "Estimate NO FOUND $IdEstimate ";
			}
		
		$this->estimaterows = DBUtil::getConnection()->GetAll("SELECT * FROM estimate_row  WHERE idestimate='$IdEstimate'");
		$this->estimateinfo['RC']=count($this->estimaterows);
			
	
	    for ($i=0 ; $i < $this->estimateinfo['RC'] ; $i++)
			{
			$RefVAT = $this->GetReferenceVAT($this->estimaterows[$i]['reference']);
			$this->estimaterows[$i]['VAT'] = $VAT = $this->ChoiceVat($RefVAT);
			$this->estimaterows[$i]['totalprice']= $this->estimaterows[$i]['unit_price'] * $this->estimaterows[$i]['quantity'];

			}
	
	/*
			$Str_FieldList = "total_amount,total_charge,total_weight,total_amount_wo_device";
			$res= DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM estimate WHERE idestimate=$IdEstimate");
			if ($res->RecordCount()>0)
			{
				// first rec
				$estimateinfoArray['total_amount'] = $res->fields['total_amount'];
				$estimateinfoArray['total_weight'] = $res->fields['total_weight'];
		        $estimateinfoArray['total_amount_wo_device'] = $res->fields['total_amount_wo_device'];
			}
	*/		
			
			$Str_FieldList = "d.lastname,d.firstname,d.adress,d.adress_2,d.city,d.zipcode,d.idstate";
			$res= DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM delivery d WHERE iddelivery=".$this->estimateinfo['iddelivery']);
			if ($res->RecordCount()>0)
			{
				$this->estimateinfo['iddelivery'] = $res->fields['iddelivery'];
				$this->estimateinfo['d_firstname'] = $res->fields['firstname'];
				$this->estimateinfo['d_lastname'] = $res->fields['lastname'];
				$this->estimateinfo['d_adress'] = stripslashes($res->fields['adress']);
				$this->estimateinfo['d_adress_2'] = stripslashes($res->fields['adress_2']);
				$this->estimateinfo['d_city'] = stripslashes($res->fields['city']);
				$this->estimateinfo['d_zipcode'] = $res->fields['zipcode'];
				$this->estimateinfo['d_idstate'] = $res->fields['idstate'];
				$this->estimateinfo['d_state']=$this->GetState($this->estimateinfo['d_idstate']);
			} else {
				/**
				 * @todo Trigger warning if iddelivery > 0 and nothing found
				 */
				 $this->estimateinfo['iddelivery']=0;
			}
	
	
	}
	
	/**
	 * Supprime un devis
	 */
	function Delete()
	{
		$id = $this->id_estimate;
		$SQL_Query = "DELETE FROM estimate set WHERE idestimate='$id'";
		$res= DBUtil::getConnection()->Execute($SQL_Query);
		if(!$res) echo "Erreur dans la suppression de la commande !<br />";
		$SQL_Query = "DELETE FROM estimate_row set WHERE idestimate='$id'";
		$res= DBUtil::getConnection()->Execute($SQL_Query);
		if(!$res) echo "Erreur dans la suppression de la commande !<br />";
		$this->id_estimate=0;
	}

	/**
	 * Vérification du stock
	 * @return int 1 si stock suffisant sinon 0
	 */
	function VerifyStock()
	{
		$Result=1;
		$n= $this->estimateinfo['RC'];
		for($i = 0 ; $i < $n && $Result ; $i++)
		{
			$res= DBUtil::getConnection()->Execute("select stock_level from detail where reference='" . $this->estimaterows[$i]['reference'] . "'");
			if($res->RecordCount()>0)
			{
				// first rec
				$Stock=$res->fields['stock_level'];
				$Qtt=$this->estimaterows[$i]['quantity'];
	
				if($Stock>=0)
				{	if(($Stock-$Qtt)<0)
					{	$Result=0;}
				}
			}
		}
	
		return $Result;
	}
	
	/**
	 * Décrémente le stock après une commande validée
	 */
	function Unstock()
	{
		$Result=0;
		$n= $this->estimateinfo['RC'];
		for($i = 0 ; $i < $n ; $i++)
		{
			$res= DBUtil::getConnection()->Execute("select stock_level from detail where reference='" . $this->estimaterows[$i]['reference'] . "'");
			if($res->RecordCount()>0)
			{
				// first rec
				$Stock=$res->fields['stock_level'];
				$Qtt=$this->estimaterows[$i]['quantity'];
				if($Stock>=0)
				{	if(($Stock-$Qtt)>=0)
					{
						$Stock-=$Qtt;
						$res= DBUtil::getConnection()->Execute("update detail set stock_level='$Stock' where reference='" . $this->estimaterows[$i]['reference'] . "'");
						if($res)
						{	
							$res= DBUtil::getConnection()->Execute("update detail set stock_level='$Stock' where reference='" . $this->estimaterows[$i]['reference'] . "'");
							if($res)
							{	$Result=1;}
						}
						else
	                        echo "<br /><center> ".Dictionnary::translate("stock_no")."<br />";
					}
				}
				else
				{	$Result=1;}
			}
		}
	
		return $Result;
	}

	/**
	 * Attribut le status 'confirmé' au devis
	 */
	function ConfirmEstimate()
	{
		$Result=0;
		$Date=getdate(time());
		$DateHeure = sprintf ("%04d-%02d-%02d %02d:%02d:%02d", $Date["year"],$Date["mon"], $Date["mday"], $Date["hours"], $Date["minutes"],$Date["seconds"]);
		$idestimate = $this->estimateinfo['idestimate'];
		$res= DBUtil::getConnection()->Execute("UPDATE estimate SET status='confirme' where idestimate='$idestimate'");
		if($res) {
			$this->estimateinfo['status']= 'confirme';
			$Result=1;
		}
	
		return $Result;
	}

	/**
	 * Vérifie si l'acheteur est soumis à la TVA
	 * @param float $ProductVat La taux de TVA du produit
	 * @return float Le taux de TVA appliqué
	 */
	function ChoiceVat($ProductVat)
	{
		$lang =  '_'.$this->LingualNumber;
		$State =  $this->buyer->buyerinfo['state'];
		$Result=0;
		$res= DBUtil::getConnection()->Execute("select bvat from state where name$lang='$State'");
		if($res->RecordCount())
		{
			// first rec
			$BVat=$res->fields['bvat'];
			if($BVat)
				$Result=$ProductVat;
		}
		return $Result;
	}

	/**
	 * Recherche le taux de TVA
	 * @param int $IdVAT Id du la TVA
	 * @return float Le taux de TVA
	 */
	function GetVAT($IdVAT)
	{
		$Result=0;
	
		$res= DBUtil::getConnection()->Execute("select * from tva where idtva='$IdVAT'");
		if($res->RecordCount()>0)
		{
			// first rec
			$Result=$res->fields['tva'];
		}
	
		return $Result;
	}

	/**
	 * Recherche le taux de TVA pour une référence
	 * @param string $Reference La référence de l'article
	 * @return float Le taux de TVA
	 */
	function GetReferenceVAT($Reference)
	{
		$Result=0;
	
		$res= DBUtil::getConnection()->Execute("select idvat from detail where reference='$Reference'");
		if($res->RecordCount())
		{
			$IdVAT=$res->fields['idvat'];
			$Result=$this->GetVAT($IdVAT);
		}
		return $Result;	
	}

	/**
	 * Recherche du nom d'un pays à partir de son id
	 * @param int $IdState L'id du pays
	 * @return string Le nom du pays dans la langue courante
	 */
	function GetState($IdState)
	{
		$Result="-";
		$lang =  '_'.$this->LingualNumber;
		$res= DBUtil::getConnection()->Execute("select name$lang as name from state where idstate='$IdState'");
		if($res->RecordCount()>0)
		{
			$Result=$res->fields['name'];
		}
		return $Result;
	}

	/**
	 * Recherhce le taux de TVA pour un pays
	 * @param int $IdState L'id du pays
	 */
	function GetStateVat($IdState)
	{
		$lang =  '_'.$this->LingualNumber;
		
		$Result=0;
	
		if($IdState=='')
		{
			$State=GetParameter('statedefault');
			$IdState=GetIdState($State,$lang);
		}
		
		$res= DBUtil::getConnection()->Execute("select bvat from state where idstate='$IdState'");
		if($res->RecordCount()>0)
		{
			// first rec
			$BVat=$res->fields['bvat'];
			if($BVat)
			{
				$Result=GetParameter('vatdefault');
			}
		}
	
		return $Result;
	}

	/**
	 * Export d'un devis vers Excel
	 * @param int $IdEstimate L'id du devis
	 */
	function Estimate2Excel($IdEstimate)
	{
		$OEstimate = new ADMESTIMATE($IdEstimate);
		$Path='/tmp/';	//TODO : mettre en paramètre ou variable globale
		
		$CSV[] = "N° de commande:" . $this->estimateinfo["idestimate"]. ";Date;".$this->estimateinfo["DateHeure"].";;;\n";
	
		//ligne du details
	
		$CSV[].= "Idrow;Designation;Reference;Quantité;Prix unitaire;Prix total;\n";
	
		for($i=0 ; $i < $this->estimateinfo["RC"] ; $i++)
		{	
			$CSV[].= $OEstimate->estimaterows[$i]["idrow"] .";". $OEstimate->estimaterows[$i]["designation"] .";". $OEstimate->estimaterows[$i]["reference"] .";". $OEstimate->estimaterows[$i]["quantity"] .";". $OEstimate->estimaterows[$i]["unit_price"] .";". $OEstimate->estimaterows[$i]["totalprice"] ."\n";
		}
		
		$CSV[].= "\n\n";
	
		$CSV[] .= "Poids total;" . $OEstimate->estimateinfo["total_weight"] .";;;\n";
		$CSV[] .= "Frais de port;" . $OEstimate->estimateinfo["total_charge"] .";;;\n";
		$CSV[] .= "Montant total de la commande;" . $OEstimate->estimateinfo["total_amount"] .";;;\n";
	
		$CSV[] .= "\n\n";
	
		$CSV[] .= "Adresse de facturation;;;;\n";
	      $CSV[] .= "Id;" . $OEstimate->estimateinfo["idbuyer"] .";;;\n";
		$CSV[] .= "Nom;" . $OEstimate->buyer->buyerinfo["lastname"] .";Prénom;" . $OEstimate->buyer->buyerinfo["firstname"] .";\n";
		$CSV[] .= "Adresse;" . $OEstimate->buyer->buyerinfo["adress"] .";;;\n";
		$CSV[] .= "Adresse compl.;" . $OEstimate->buyer->buyerinfo["adress_2"] .";;;\n";
		$CSV[] .= "Ville;" . $OEstimate->buyer->buyerinfo["city"] .";;;\n";
		$CSV[] .= "Code postal;" . $OEstimate->buyer->buyerinfo["zipcode"] .";Pays;". $OEstimate->buyer->buyerinfo["state"] .";\n";
	
		$CSV[] .= "\n";
	
		if($OEstimate->estimateinfo["iddelivery"])
		{
			$CSV[] .= "Adresse de livraison;" . $OEstimate->estimateinfo["iddelivery"] .";\n";
			$CSV[] .= "Nom;" . $OEstimate->estimateinfo["d_lastname"] .";Prénom;" . $OEstimate->estimateinfo["d_firstname"] .";\n";
			$CSV[] .= "Adresse;" . $OEstimate->estimateinfo["d_adress"] .";\n";
			$CSV[] .= "Adresse compl.;" . $OEstimate->estimateinfo["d_adress_2"] .";;;\n";
			$CSV[] .= "Ville;" . $OEstimate->estimateinfo["d_city"] .";";
			$CSV[] .= "Code postal;" . $OEstimate->estimateinfo["d_zipcode"] .";Pays;" . $OEstimate->estimateinfo["d_state"] .";\n";
		}
		$CSV[] .= "\n";
	
		$CSV[] .= "Commentaire;\n";
		$CSV[] .= $OEstimate->estimateinfo["comment"] .";\n";
	
		$CSV[] .= "N° de carte;\n";
		$CSV[] .= $OEstimate->estimateinfo["cardnumber"] .";\n";
		$CSV[] .= "Date de validité;\n";
		$CSV[] .= $OEstimate->estimateinfo["exp_date_card"] .";\n";
		$CSV[] .= "Nom sur la carte;\n";
		$CSV[] .= $OEstimate->estimateinfo["name_card"] .";\n";
	
		$Date=getdate(time());
		$Fic=$Path."Estimate-".$this->estimateinfo["idestimate"]."_".$Date["mday"]."_".$Date["mon"]."_".$Date["year"].".csv";
		$Fp=fopen($Fic,"w+");
		for($i=0 ; $i < count($CSV) ; $i++)
		{
			fputs($Fp, $CSV[$i]);
		}
		fclose($Fp);
	}
	
	/**
	 * Export de devis en csv
	 * @param array $IdEstimateArray Les id des devis
	 */
	function ExportEstimate($IdEstimateArray)
	{
	
		$Path='/tmp/';
		$CSV[0]="N° de commande;Designation;Reference;Quantité;Prix unitaire;Prix total;idbuyer;Nom;Prénom;Adresse;Adresse compl.;Ville;Code postal;Pays;Nom;Prenom;Adresse;Adresse compl.;Ville;Code postal;Pays;DateHeure;Poids total;Frais de port;Montant total de la commande;Commentaire;N° de carte;Date d expiration; Nom sur la carte\n";
		
		for($i=0 ; $i < count($IdEstimateArray) ; $i++)
		{
			$OEstimate = new ADMESTIMATE($IdEstimateArray[$i]);
			$CSV[$i].= $OEstimate->estimateinfo[$i]["idestimate"].";";
			
			// loop for all lines'Estimate
			for($j=0 ; $j < $OEstimate->estimateinfo['RC'] ; $j++)
			{
				$CSV[$i].=$OEstimate->estimaterows[$j]["designation"] .";". $OEstimate->estimaterows[$j]["reference"] .";". $OEstimate->estimaterows[$j]["quantity"] .";". $OEstimate->estimaterows[$j]["unit_price"] .";". $OEstimate->estimaterows[$j]["totalprice"].";";
			}
			// Info's Buyer
			$CSV[$i].=$OEstimate->estimateinfo[$i]["idbuyer"] .";" . $OEstimate->buyer->buyerinfo["lastname"] .";" . $OEstimate->buyer->buyerinfo["firstname"].";".$OEstimate->buyer->buyerinfo["adress"].";".$OEstimate->buyer->buyerinfo["adress_2"].";".$OEstimate->buyer->buyerinfo["city"].";".$OEstimate->buyer->buyerinfo["zipcode"].";".$OEstimate->buyer->buyerinfo["state"].";";
			// Info's Delivrery
			if($OEstimate->estimateinfo["iddelivery"])
			{
				$CSV[$i].=$OEstimate->estimateinfo["d_lastname"].";".$OEstimate->estimateinfo["d_firstname"].";".$OEstimate->estimateinfo["d_adress"].";".$OEstimate->estimateinfo["d_adress_2"].";".$OEstimate->estimateinfo["d_city"].";".$OEstimate->estimateinfo["d_zipcode"].";".$OEstimate->estimateinfo["d_state"].";";
			}
			else
			{
				$CSV[$i].=";;;;;;;";
			}
			// Info's estimater
			$CSV[$i].=$OEstimate->estimateinfo["DateHeure"].";".$OEstimate->estimateinfo["total_weight"].";"."\n";
	
		}
		$Date=getdate(time());
		$Fic=$Path."ExportEstimate_".$Date["mday"]."_".$Date["mon"]."_".$Date["year"].".csv";
		$Fp=fopen($Fic,"w+");
		for($i=0 ; $i < count($CSV) ; $i++)
		{
			fputs($Fp, $CSV[$i]);
		}
		fclose($Fp);
	
	}
	
	/**
	 * Formattage d'un nombre pour l'affichage
	 */
	function AffNumber($Number)
	{
		$Mode=1;
		$Decimal=2;
		$Round=3;
		settype($Number,"string"); 
		$Result=$Number;
		
		if($Round!="")	$Result=round($Number,$Round);
		
		if($Mode) 	$Result=number_format($Result,$Decimal,".","");
		else 	$Result=number_format($Result,$Decimal);
		return $Result;
	}

}
?>