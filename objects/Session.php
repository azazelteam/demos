<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object session front-office
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/Customer.php");


class Session{
	
	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * @var Session $instance
	 * @access private
	 */
	private static $instance = NULL;
	
	/**
	 * @var string $session_id
	 * @access private
	 */
	private $session_id;
	
	/**
	 * @var Customer $customer
	 * @access private
	 */
	private $customer;

	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @access public
	 * @return Session
	 */
	public static function getInstance(){
	
		if( self::$instance === NULL )
            self::$instance = new Session();
	
        return self::$instance;
       
	}
	
	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * @access private
	 */
	private function __construct(){
		
		$this->session_id 		= session_id();
		$this->customer 		= NULL;
	// ------- Mise à jour de l'id gestion des Index  #1161
	//	if( isset( $_SESSION[ "idbuyer" ] ) )
		//	$this->customer = new Customer( intval( $_SESSION[ "idbuyer" ] ), isset( $_SESSION[ "idcontact" ] ) ? intval( $_SESSION[ "idcontact" ] ) : 0 );
				if( isset( $_SESSION[ "idbuyer" ] ) )
		$this->customer = new Customer( $_SESSION[ "idbuyer" ] , isset( $_SESSION[ "idcontact" ] ) ?  $_SESSION[ "idcontact" ]  : 0 );
//---------------
	}

	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return string
	 */
	public function getId(){ return $this->session_id; }
public function getIdbuyer(){ return $this->session_id; }
	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Customer or NULL
	 */
	public function getCustomer(){ return $this->customer; }
	
	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * identification du visiteur
	 * @param string $login identifiant
	 * @param string $password MD5 du mot de passe
	 * @return bool
	 */
	public function login( $login, $md5password ){

		$md5password = preg_replace( "/[^a-zA-Z0-9]+/Uis", "", $md5password );
		
		$query = "
		SELECT ua.idbuyer, ua.idcontact, c.lastname, c.firstname, b.idstate, b.catalog_right 
		FROM user_account ua, buyer b, contact c 
		WHERE ua.login = " . DBUtil::quote( $login ) . "
		AND ua.password = " . DBUtil::quote( $md5password ) . "
		AND ua.idbuyer = c.idbuyer 
		AND ua.idcontact = c.idcontact
		AND ua.idbuyer = b.idbuyer";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false || $rs->RecordCount() != 1 )	
			return false;

		$this->customer = new Customer( $rs->fields( "idbuyer" ), $rs->fields( "idcontact" ) );
		
		$_SESSION['idbuyer'] 	= $rs->fields( "idbuyer" );
		$_SESSION['idcontact'] 	= $rs->fields( "idcontact" );
	
		return true;
	
	}
	
	/* --------------------------------------------------------------------------------------------------- */
	/**
	 * déconnexion du visiteur
	 */
	public function logout(){
		
		if( isset( $_SESSION[ "idbuyer" ] ) )
			unset( $_SESSION[ "idbuyer" ] );

		if( isset( $_SESSION[ "idcontact" ] ) )
			unset( $_SESSION[ "idcontact" ] );
		
		if ( isset( $_COOKIE[ session_name() ] ) )
			setcookie( session_name(), '', time()-42000, '/' );

		$this->customer = NULL;

	}

	/* --------------------------------------------------------------------------------------------------- */
	
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}

	/* --------------------------------------------------------------------------------------------------- */
	
}

?>