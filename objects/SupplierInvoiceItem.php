<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ligne commande fournisseurs
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );

class SupplierInvoiceItem extends DBObject{
	
	//----------------------------------------------------------------------------
	
	public function __construct( $billing_supplier, $idsupplier, $idbl_delivery, $idrow ){
	
		parent::__construct(

			"billing_supplier_row", 
			false, 
			"billing_supplier", 
			$billing_supplier,
			"idsupplier",
			 $idsupplier,
			"idbl_delivery",
			 $idbl_delivery,
			"idrow",
			 $idrow 
			
		);
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getPriceET(){
			
		$query = "
		SELECT osr.* 
		FROM order_supplier os, order_supplier_row osr, bl_delivery bl, bl_delivery_row blr
		WHERE blr.idbl_delivery = '" . $this->get( "idbl_delivery" ) . "'
		AND blr.idrow = '" . $this->get( "idrow" ) . "'
		AND blr.idbl_delivery = bl.idbl_delivery
		AND os.idorder_supplier = bl.idorder_supplier
		AND osr.idorder_supplier = os.idorder_supplier
		AND osr.idorder_row = blr.idorder_row
		LIMIT 1"; 

		$rs =& DBUtil::query( $query );
		
		return round( $rs->fields( "discount_price" ), 2 );
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>