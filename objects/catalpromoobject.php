<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object promotions
 */

include_once( dirname( __FILE__ ) . "/catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CATALPROMO extends CATALBASE
{

	function __construct()
	{

	$reload=1;
	$this->id = 'promo' ;
	$this->buyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$this->lang = "_1";
	//if( isset($_SESSION['promoproducts'])  ) $reload=0; 
	if( $reload ) {
		$this->GetPromoProducts();
		}
	else { 
		$this->products = &$_SESSION['promoproducts'] ;
		$this->NBProduct = 0 +  count($this->products);
		}
	$this->NbByPages = DBUtil::getParameter('nb_products_by_pages');
	}

	/**
	 * Cherche les produit en promo
	 * @return array les produits en promo
	 */
	private function GetPromoProducts()
	{
		$IdSimilar = $this->id;
		$Langue = $this->lang;
		$Time=time();
		$Query ="SELECT r.idproduct,name$Langue as name from promo p , product r, detail d where p.reference=d.reference AND r.idproduct=d.idproduct AND UNIX_TIMESTAMP(p.begin_date)<='$Time' and UNIX_TIMESTAMP(p.end_date)>='$Time' ORDER BY rate desc ,display_detail_order";
		$n = $this->SetProducts($Query);
		$_SESSION['promoproducts'] = &$this->products ;	
		return $n;
	}
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}


}//EOC
?>