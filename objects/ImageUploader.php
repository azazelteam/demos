<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object téléchargement image photos
 */

include_once( dirname( __FILE__ ) . "/FileUploader.php" );

define( "IMAGE_TYPE_GIF", 1 );
define( "IMAGE_TYPE_JPEG", 2 );
define( "IMAGE_TYPE_PNG", 3 );

class ImageUploader extends FileUploader{
	
	//---------------------------------------------------------------------------------	
	
	protected $maxWidth;
	protected $maxHeight;
	
	protected $resizeImage;
	protected $quality;
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Constructeur
	 * @param string $fieldName Le nom du champ 'file' dans le formulaire à traiter
	 * @param string $destination Le répertoire de destination
	 * @param string $fileName optionnel, le nom du fichier une fois téléchargé ( si absent, garde le même nom ) 
	 */
	 
	function __construct( $fieldName, $destination, $fileName = "" ){
		
		parent::__construct( $fieldName, $destination, $fileName );
		
		$this->resizeImage = false;
		$this->maxWidth = 0;
		$this->maxHeight = 0;
		$this->quality = 100;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Modifie la qualité de l'image crée
	 * @param int $quality La qualité de l'image générée ( entre 0 et 100 )
	 * @return void
	 */
	 
	public function setQuality( $quality ){
		
		$this->quality = $quality;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Définit la largeur maximale de l'image générée
	 * @param int $maxWidth La largeur maximale
	 * @return void
	 */
	 
	public function setMaxWidth( $maxWidth ){
		
		$this->maxWidth = $maxWidth;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Définit la hauteur maximale de l'image générée
	 * @param int $maxHeight La hauteur maximale
	 * @return void
	 */
	 
	public function setMaxHeight( $maxHeight ){
		
		$this->maxHeight = $maxHeight;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Active le redimensionnement de l'image
	 * @param bool $resizeImage
	 * @return void
	 */
	 
	public function setResizeImage( $resizeImage = true ){
		
		$this->resizeImage = $resizeImage;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Télécharge l'image
	 * @return true en cas de succès, sinon false
	 */
	 
	public function upload(){
		
		if( !parent::upload() )
			return false;

		return $this->handleUploadedFile();
			
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Traite l'image téléchargée
	 * @return true en cas de succès, sinon false
	 */
	 
	private function handleUploadedFile(){
		
		$imageInfos = getImageSize( "{$this->destination}/{$this->fileName}" );
		
		$type 	= $imageInfos[ 2 ];
		$width 	= $imageInfos[ 0 ];
		$height = $imageInfos[ 1 ];
		
		if( $this->resizeImage ){
			
			$maxWidth 	= !$this->maxWidth ? $width : $this->maxWidth;
			$maxHeight 	= !$this->maxHeight ? $height : $this->maxHeight;
		
		}
		else{
			
			$maxWidth 	= $width;
			$maxHeight 	= $height;
			
		}
		
		if( $width > $maxWidth || $height > $maxHeight ){
			
			$wRatio = 0.0 + $width / $maxWidth;
			$hRatio = 0.0 + $height / $maxHeight;
			
			if( $wRatio > $hRatio ){
				
				$newWidth = $width / $wRatio;
				$newHeight = $height / $wRatio;
				
			}
			else{
				
				$newWidth = ceil( $width / $hRatio );
				$newHeight = ceil( $height / $hRatio );
				
			}
			
		}
		else{
			
			$newWidth = $width;
			$newHeight = $height;
				
		}
		
		switch( $type ){
			
			case IMAGE_TYPE_JPEG :
			
				$tmp = imagecreatefromjpeg( "{$this->destination}/{$this->fileName}" );
				break;
				
			case IMAGE_TYPE_GIF :
			
				$tmp = imagecreatefromgif( "{$this->destination}/{$this->fileName}" );
				break;
				
			case IMAGE_TYPE_PNG :
			
				$tmp = imagecreatefrompng( "{$this->destination}/{$this->fileName}" );
				break;
				
			default : 

				$this->errors[] = "Unsupported input type";
				return false;
				
		}
	
		if( !strlen( $tmp ) ){
			
			$this->errors[] = "Cannot create image";
			
			return false;
			
		}
		
		$newimg = imagecreateTrueColor( $newWidth, $newHeight );
		
		imagecopyresampled( $newimg, $tmp, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height );
		
		imagejpeg( $newimg, "{$this->destination}/{$this->fileName}" , 100 );
		
		@chmod( "{$this->destination}/{$this->fileName}", 0755 );
		
		return true;
		
	}
	
	//---------------------------------------------------------------------------------	
	
}

?>