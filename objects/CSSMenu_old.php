<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object menu de navigation CSS
 */

define ( "DEBUG_CSSMENU", 0 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );


class CSSMenu{

	//-------------------------------------------------------------------------------------------

	private $root;
	private $depth;
	private $html;
	private $staticItems;
	private $catalog_right;
	private $categoryCount;
	private $showHomeItem;
	private $treePath;
	private $typeEncoding;
	//-------------------------------------------------------------------------------------------

	/**
	 * Constructeur
	 * @param int depth facultatif, permet d'afficher les sous-catégories sur 'depth' niveaux
	 * @param int idCategory facultatif, permet de construire le menu à partir de la catégorie spécifiée
	 */
	function __construct( $depth = -1, $idcategory = 0, $typeEncoding = 1 ){

		$this->depth 	= $depth;
        $this->typeEncoding 	= $typeEncoding;
        $this->html 	= "";

		if( User::getInstance()->getId() )
				$this->catalog_right =   6;
		else 	$this->catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;

		$this->showHomeItem = false;

		$this->root = array(
			"id" 			=> 0,
			"name" 			=> "ROOT",
			"children" 		=> array()
		);

		$this->categoryCount 	= 0;
		$this->staticItems 		= array( "labels" => array(), "hrefs" => array(), "styles" => array() , "ids" => array());

		$this->treePath = array();

		if( $idcategory )
			$this->setTreePath( intval( $idcategory ) );

	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Afficher l'élément 'Accueil' en premier ou non
	 * @param bool $showHomeItem
	 * @return void
	 */
	public function showHomeItem( $showHomeItem = true ){

		$this->showHomeItem = $showHomeItem;

	}

	//-------------------------------------------------------------------------------------------
	/**
	 * Construit le menu
	 * @access private
	 * @return void
	 */
	public function buildMenu(){

		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );

		$this->createNode( $this->root );

	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Récupère les catégories filles
	 * @param array $parent les catégories mères
	 * @param int $depth le niveau
	 * @return void
	 */
	private function setCategoryChildren( &$parent, $depth ){

	$hide_categ = DBUtil::getParameter( "hide_category" );

		$query = "
		SELECT cat.idcategory AS id, cat.name_1 AS name, cat.image_menu AS image
		FROM category cat, category_link catlink
		WHERE catlink.idcategorychild = cat.idcategory
		AND cat.available = 1
		AND cat.available_menu = 1
		AND cat.idcategory != $hide_categ
		AND catlink.idcategoryparent = " . $parent[ "id" ] . "
		AND cat.catalog_right <= " . $this->catalog_right . "
		ORDER BY catlink.displayorder";

		$rs = DBUtil::query( $query );
		$this->categoryCount += $rs->RecordCount();

		while( !$rs->EOF() ){

			$category  = array(
				"id" 			=> $rs->fields( "id" ),
				"name" 			=> $rs->fields( "name" ),
				"image" 		=>  $rs->fields( "image" ),
				"children" 		=> array()
			);

			$pos = array_push( $parent[ "children" ], $category );
			$child = &$parent[ "children" ][ $pos - 1 ];

			if( $this->depth == -1 )
				$this->setCategoryChildren( $child, $depth );
			else if( $depth > 1 )
				$this->setCategoryChildren( $child, $depth - 1 );

			$rs->MoveNext();

		}

	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Génère les noeux de l'arbre pour chaque catégorie
	 * @param array $parent les catégories mère
	 * @return void
	 */
	private function createNode( &$parent ){
		global $GLOBAL_START_URL,$GLOBAL_START_PATH;
		if( $parent[ "name" ] == "ROOT" ){

			$this->html .= "<ul id=\"mainFrontMenu\">";

			//if( $this->showHomeItem)
				//$this->html .= "<li id=\"accueilMenu\"><a href=\"/\"><span>Accueil</span></a></li>";

		}
		else

		 $this->html .= "<ul class='displayNone'>";
			//$this->html .= "<ul>";

		$categories_query = DBUtil::query( "SELECT * FROM category_custom_pages WHERE 1");
		$static_categories = array();
		$h = 0;
		while ($h < $categories_query->RecordCount())
		{
			$static_categories[] = $categories_query->fields("idcategory");
			$categories_query->MoveNext();
			$h++;
		}
		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){

			$child = &$parent[ "children" ][ $i ];

			$cssClass = array();

			$cat_html = DBUtil::query( "SELECT * FROM category WHERE idcategory='".$child[ "id" ]."' ");

			if( count( $child[ "children" ] ) && $parent[ "name" ] != "ROOT" )
				$cssClass[] = "ParentNode";

			if( in_array( $child[ "id" ], $this->treePath ) )
				$cssClass[] = "selected";

			$this->html .= "
			 
			<li id=\"ID" . $child[ "id" ] . "\" ".(/*in_array($child[ "id" ], $static_categories) ||*/ $cat_html->fields("html") ? ' class="displayNone"' : '');

			if( count( $cssClass ) )
				$this->html .= " class=\"" . implode( " ", $cssClass ) . "\"";

			$this->html .= ">";

			$href 		= htmlentities( URLFactory::getCategoryURL( $child[ "id" ] ) );
			$isFirst 	= $parent[ "name" ] == "ROOT" ? " class=\"first\"" : "";


			$qr = "
				SELECT name_link,name_photo_menu,logo_accueil
				FROM category
				WHERE idcategory = " . $child[ "id" ] . "
				";

			$res = DBUtil::query( $qr );

			if ($res->fields( "logo_accueil" ) != ""){
				//$nameImg = str_replace('.png','.jpg',$res->fields( "logo_accueil" ) );
				$nameImg = $res->fields( "logo_accueil" );
				$url_image = "images/category/logos_accueil/".$nameImg;
			}
			if($res->fields( "name_photo_menu" )!= ""){
				$isTitle 	= Util::doNothing($res->fields( "name_photo_menu" ));
			}else{
				$isTitle 	= $this->typeEncoding == 1 ? Util::doNothing($child[ "name" ]) : $child[ "name" ];
			}

			/* ajout par rapport au fichier de www.abisco.fr */

			if( $parent[ "name" ] == "ROOT" ) {
				$root_class = " class=\"first icon-link\"";
				$this->html .= "<a href=\"$href\"$root_class title=\"$isTitle\" >";
				$this->html .= "<span id='img_menu_icon'";
				$this->html .= " \">";
				$this->html .= "</span>";
				$this->html .= "</a>";
			}
			$this->html .= "<a href=\"$href\"$isFirst title=\"$isTitle\">";

			/* ajout par rapport au fichier de www.abisco.fr */
			if( $parent[ "name" ] == "ROOT" )
				$this->html .= "<span class='itemMenuColor'>";

			if($parent[ "name" ] != "ROOT")
			{

			$query = "
				SELECT *
				FROM category_link
				WHERE idcategoryparent = " . $child[ "id" ] . "
				";

				$rs = DBUtil::query( $query );

				$count = $rs->RecordCount();
			}


			if  ($count)
			{
			$this->html .= "<span>";
			if ($res->fields("name_link"))
				$this->html .=  Util::doNothing($res->fields("name_link"));
			else
				$this->html .=  $this->typeEncoding == 1 ? Util::doNothing($child[ "name" ]) : $child[ "name" ];
			$this->html .= "</span>";

			}
		else
			{
			if ($res->fields("name_link"))
				$this->html .=  Util::doNothing($res->fields("name_link"));
			else
				$this->html .=  $this->typeEncoding == 1 ? Util::doNothing($child[ "name" ]) : $child[ "name" ];
			}
			/* image */

			if( strlen( $child[ "image" ] ) )
			$this->html .= "<img class=\"menuItemImage\" src=\"../templates/img_cat/menu/" . htmlentities( $child[ "image" ] ) . "\" />";


			//$image = str_replace(" ", "-", $child[ "name" ]);

			include_once( dirname( __FILE__ ) . "/../URLFactory.php" );
          		$image = URLFactory::getCategoryImageAccueilURI( $child[ "id" ],6 );


			//$url_image = $GLOBAL_START_URL . "/" . $image . "-c-" .  $child[ "id" ] . ".jpg";
			//$url_image = $GLOBAL_START_URL . $image ;
			$url_image = "images/category/logos_accueil" . $image ;

			/* ajout par rapport au fichier de www.abisco.fr */
			if( $parent[ "name" ] == "ROOT" )
				//$this->html .= "</span><img src=\"$url_image\" width='180' height='180' alt=\"$isTitle\"/>";
				$this->html .= "</span>";

			$this->html .= "</a>";

			if( count( $child[ "children" ] ) )
				$this->createNode( $child );

			$this->html .= "</li>";

			$i++;
		}

		//add static items

		if( $parent[ "name" ] == "ROOT" ){

			$k = 0;
			while( $k < count( $this->staticItems[ "labels" ] ) ){

				$label 	= $this->staticItems[ "labels" ][ $k ];
				$href 	= $this->staticItems[ "hrefs" ][ $k ];
				$style 	= $this->staticItems[ "styles" ][ $k ];
				$id 	= $this->staticItems[ "ids" ][ $k ];

				$this->html .= "<li id=\"$id\"><a href=\"$href\"";

				//if( !empty( $style ) )
					//$this->html .= " style=\"$style\"";

				$this->html .= ">" . htmlentities( $label ) . "</a></li>";

				$k++;

			}

		}

		$this->html .= "</ul>";
		/* ajout par rapport au fichier de www.abisco.fr */
		//$this->html .= "<hr style='border: 0 none; clear: both; height: 25px; position: relative; width: 100%; max-width: 977px;margin-bottom: -14px !important'>";

	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne le code HTML du menu
	 * @return string le code html du menu
	 */
	public function getHTML(){

		/*if( isset( $_SESSION[ "CustomerMenu" ] ) )
			$this->html = unserialize( URLFactory::base64url_decode(  $_SESSION[ "CustomerMenu" ] ) );
		else if( !strlen( $this->html ) ){

			$this->buildMenu();
			$_SESSION[ "CustomerMenu" ] = URLFactory::base64url_encode( serialize( $this->html ) );

		}*/

		if( !strlen( $this->html ) )
			$this->buildMenu();

		return $this->html;

	}
	/* MOBILE MENU */

	private function createNodeMobile( &$parent,$noselect = 0){
		global $GLOBAL_START_URL,$GLOBAL_START_PATH;
		$this->html .= '<ul';
		$this->html .= (!$noselect) ? ' id="nav">' : '>';
		foreach($parent['children'] as $pmenu){
			$href 		= htmlentities( URLFactory::getCategoryURL( $pmenu[ "id" ] ) );
			$this->html .= '<li>';
			$niveau = '';
			if ($noselect)
				for ($j=1; $j <= $noselect; $j++)
					$niveau .= 	'-';
				$this->html .= '<a href="'.$href.'">'.$pmenu['name'].'</a>';
			if ($pmenu['children'])
			{
				$noselect++;
				$this->createNodeMobile( $pmenu, $noselect);
			}
			$this->html .= '</li>';
		}
		$this->html .= '</ul>';
	}

	public function buildMenuMobile(){

		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );

		$this->createNodeMobile( $this->root );

	}
	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne le code HTML du menu Mobile
	 * @return string le code html du menu Mobile
	 */
	public function getHTMLMobile(){

		if( !strlen( $this->html ) )
			$this->buildMenuMobile();

		return $this->html;

	}
	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne le nom du menu
	 * @return string le nom du menu
	 */
	public function getName(){

		return $this->root[ "name" ];

	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne le nombre de fils du premier niveau
	 * @return int le nombre de fils du premier niveau
	 */
	private function getFirstChildCount(){

		return count( $this->root[ "children" ] );

	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Permet d'ajouter un menu static
	 * @param string $label le nom du menu
	 * @param string $href le lien
	 * @param string $style le css
	 * @return void
	 */
	public function addStaticItem( $label, $href, $style = "" , $id = ""){

		$this->staticItems[ "labels" ][] 	= $label;
		$this->staticItems[ "hrefs" ][] 	= $href;
		$this->staticItems[ "styles" ][] 	= $style;
		$this->staticItems[ "ids" ][] 		= $id;

	}

	//-------------------------------------------------------------------------------------------

	private function setTreePath( $idcategory ){

		$treePath = array( $idcategory );

		$done = false;
		while( !$done ){

			$rs = DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategory' LIMIT 1" );

			if( $rs->RecordCount() )
				array_push( $treePath, $idcategory =  $rs->fields( "idcategoryparent" ) );
			else $done = true;

		}

		$this->treePath = array_reverse( $treePath );

	}

	//-------------------------------------------------------------------------------------------

}

?>

