<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object mode de paiement
 */


abstract class Payment{

	public static $PAYMENT_MOD_UNDEFINED 			= 0; //non d襩ni
	public static $PAYMENT_MOD_DEBIT_CARD 			= 1; //carte de bancaire
	public static $PAYMENT_MOD_CHEQUE 				= 2; //ch簵e
	public static $PAYMENT_MOD_TRANSFER 			= 3; //virement
	public static $PAYMENT_MOD_DRAFT 				= 4; //traite
	public static $PAYMENT_MOD_CREDIT_DOCUMENTAIRE 	= 5; //cr裩t documentaire
	public static $PAYMENT_MOD_DOWN_PAYMENT_CHEQUE 	= 6; //ch簵e d'acompte
	public static $PAYMENT_MOD_FIANET_CB 			= 7; //fianet par CB
	public static $PAYMENT_MOD_FIANET_CREDIT 		= 8; //cr褤it FIANET
	public static $PAYMENT_MOD_MONEY_ORDER 			= 9; //mandat administratif
	public static $PAYMENT_MOD_GIFT_TOKEN 			= 14; //ch簵e cadeau
	public static $PAYMENT_MOD_CREDIT 				= 15; //bon d'achat ( avoir )
	
	//------------------------------------------------------------------------------------------------------------
	 /**
	  * Retourne la date de paiement calcul裠artir du d諡is de paiement
	  * @return la date de paiement calcul裠artir du d諡is de paiement
	  */
	 public static function getPaymentDelayDate( $date, $idpayment_delay ){
	 	
	 	$payment_delay = DBUtil::getDBValue( "payment_delay", "payment_delay", "idpayment_delay", intval( $idpayment_delay ) );

	 	if( !$payment_delay )
	 		return false;

	 	if( strpos( $date, " " ) !== false )
			list($date,$time)=explode(" ",$date);
				
		$res = @explode( "-", $date );
		
		if( count( $res ) != 3 )
			return false;
			
		@list( $billing_year, $billing_month, $billing_day ) = $res;
		
		//calcul de la date de paiement
		
		switch( $payment_delay ){
 		
	 		case "0" :
	 		case "0J": 			
	 		
	 			$payment_date = $date;
	 			
	 			break;
	 			
	 		case "30J": 		
	 		
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 1, $billing_day,  $billing_year ) );
	 			
	 			break;
	 			
	 			
	 		case "FM+30J": 		
	
				//30J
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 1, $billing_day,  $billing_year ) );
				//FM
				list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
	 			$month = mktime( 0, 0, 0, $billing_month, 1, $billing_year ); 
				$lastday = date( "t", $month );
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, $lastday,  $billing_year ) );
				
				
	 			break;
	 			
	 		case "45J+FM": 		
	
				//45J
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 1, $billing_day + 15,  $billing_year ) );
				
				//FM
				list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
	 			$month = mktime( 0, 0, 0, $billing_month, 1, $billing_year ); 
				$lastday = date( "t", $month );
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, $lastday,  $billing_year ) );
				
				
	 			break;
	 			
	 		case "1M+FM+10J":
	 		
	 			//1M+FM
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 2, $billing_day,  $billing_year ) );
	 			//10J
	 			list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, 10,  $billing_year ) );
	 			
	 			break;
	 		
	 		case "1M+FM+15J":
	
				//1M+FM
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 2, $billing_day,  $billing_year ) );
	 			//15J
	 			list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, 15,  $billing_year ) );
				
				break;
				
	 		case "60J":
	 		
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 2, $billing_day,  $billing_year ) );
	 			
	 			break;
	 		
	 		case "2M+FM":
	 		
	 			//2M
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 2, $billing_day,  $billing_year ) );
	 			//FM
	 			list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
	 			$month = mktime( 0, 0, 0, $billing_month, 1, $billing_year ); 
				$lastday = date( "t", $month );
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, $lastday,  $billing_year ) );
	 			
	 			break;
	 		
	 		case "2M+FM+10J":
	 		
	 			//2M+FM
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 3, $billing_day,  $billing_year ) );
	 			//10J
	 			list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, 10,  $billing_year ) );
	 			
	 			break;
	 		
	 		case "2M+FM+20J":
	 		
	 			//2M+FM
	 			$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 3, $billing_day,  $billing_year ) );
	 			//10J
	 			list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, 20,  $billing_year ) );
	 			
	 			break;
	 		
	 		case "90J": 		
	
				//90J
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month + 3, $billing_day,  $billing_year ) );
				//FM
				list( $billing_year, $billing_month, $billing_day ) = explode( "-", $payment_date );
	 			$month = mktime( 0, 0, 0, $billing_month, 1, $billing_year ); 
				$lastday = date( "t", $month );
				$payment_date = date( "Y-m-d", mktime( 0, 0, 0, $billing_month, $lastday,  $billing_year ) );
				
				
	 			break;
	 			
	 		default : $payment_date = "";
	 		
	 	}
		
	 	return $payment_date;
	
	 }
	 
	//----------------------------------------------------------------------------
	/**
	 * V豩fie si les paiements sont enregistr豠mais non valid豠pour une facture
	 * @static
	 * @param int $idbilling_buyer le num豯 de facture en franglish
	 * @return bool true si c'est le cas, sinon false
	 */
	 public static function checkRegulations( $idbilling_buyer ){
	// ------- Mise à jour de l'id gestion des Index  #1161
	 	$query = "SELECT brb.idregulations_buyer
				FROM billing_regulations_buyer brb, regulations_buyer rb
				WHERE brb.idbilling_buyer = '$idbilling_buyer'
				AND rb.accept=0
				AND brb.idregulations_buyer = rb.idregulations_buyer";
		$rsregul = DBUtil::query($query);
		
		if( $rsregul === false )
			die ( "Impossible de r袵p豥r les r禬ements pour la facture $idbilling_buyer" );	
		
		if($rsregul->RecordCount()>0)
			return true;
		else
			return false;
	 	
	 }
	
	 //------------------------------------------------------------------------------------------------------------
	 
}

?>