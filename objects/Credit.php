<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entête avoirs clients
*/

include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/SecuredDocument.php" );
include_once( dirname( __FILE__ ) . "/Sale.php" );
include_once( dirname( __FILE__ ) . "/CreditItem.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
include_once( dirname( __FILE__ ) . "/Invoice.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );


class Credit extends DBObject implements Sale, SecuredDocument{

	//----------------------------------------------------------------------------	
	/**
	 * @var int $idcredit le numéro d'avoir
	 */
    private $idcredit;
    /**
	 * @var TradePriceStrategy $priceStrategy
	 */
	protected $priceStrategy;
	/**
	 * @var Salesman $salesman
	 * @access private
	 */
	private $salesman;
	/**
	 * @var Invoice $invoice
	 * @access private
	 */
	private $invoice;
	
    //----------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param int $idcredit le numéro d'avoir
	 */
	function __construct( $idcredit ){
		
		$this->idcredit =  $idcredit;
		
		parent::__construct( "credits", false, "idcredit",  $idcredit  );
		
		$this->invoice = new Invoice( $this->get( "idbilling_buyer" ) );
		$this->salesman = new Salesman( $this->get( "iduser" ) );

		$this->setPriceStrategy();
		$this->setItems();
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return int
	 */
	public function getId(){ return $this->idcredit; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access protected
	 * @return void
	 */
	protected function setPriceStrategy(){
		
		$rs =& DBUtil::query( 
		
			"SELECT strategy 
			FROM trade_price_strategy 
			WHERE `datetime` <= '" . $this->recordSet[ "DateHeure" ] . "'
			ORDER BY datetime DESC
			LIMIT 1" 
		
		);
		
		if( !$rs->RecordCount() 
			|| !file_exists( dirname( __FILE__ ) . "/strategies/" . $rs->fields( "strategy" ) . ".php" ) ){

			trigger_error( "stratégie de calcul de prix inconnue", E_USER_ERROR );
			exit();
			
		}

		$strategy = $rs->fields( "strategy" );
		
		include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
		$this->priceStrategy = new $strategy( $this );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "CreditItem" );
		
		$rs =& DBUtil::query( "SELECT idrow FROM credit_rows WHERE idcredit = '{$this->idcredit}' ORDER BY idrow ASC" );

		while( !$rs->EOF() ){
		
			$this->items->add( new CreditItem( $this, $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/SecuredDocument#allowRead()
	 */
	public function allowRead(){ return $this->allowCustomerRead() || $this->allowUserRead(); }
	
	//----------------------------------------------------------------------------
    /**
     * Retourne true si le client identifié est autorisé à consulter le document
     * @access private
     * @return bool
     */
    private function allowCustomerRead(){
	
    	include_once( dirname( __FILE__ ) . "/Session.php" );
		return Session::getInstance()->getCustomer() && Session::getInstance()->getCustomer()->getId() == $this->invoice->getCustomer()->getId();
		
    }
    
    //----------------------------------------------------------------------------
    /**
     * Retourne true si le commercial identifié est autorisé à consulter le document
     * @access private
     * @return bool
     */
    private function allowUserRead(){
    	
		if( User::getInstance()->getId() == $this->salesman->getId() 
			|| User::getInstance()->getId() == $this->invoice->getCustomer()->get( "iduser" )
			|| User::getInstance()->getHasAdminPrivileges() ) 
			return true;
		
		return false;
		
    } 
    
    //----------------------------------------------------------------------------
	/**
	 * Retourne true si le commercial identifié à le droit de créer un document pour un client donné
	 * @static
	 * @access public
	 * @param int $idbuyer le n° de compte client
	 * @return bool
	 */
	public static function allowCreationForCustomer( $idbuyer ){

		$iduser = DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $idbuyer );
		
		return User::getInstance()->getHasAdminPrivileges() || User::getInstance()->getId() == $iduser;
		
	}
	
    //----------------------------------------------------------------------------
    /**
     * (non-PHPdoc)
     * @see objects/SecuredDocument#allowEdit()
     */
    public function allowEdit(){ return User::getInstance()->getHasAdminPrivileges() || User::getInstance()->getId() == $this->salesman->getId(); }

    //----------------------------------------------------------------------------
    /**
     * (non-PHPdoc)
     * @see objects/DBObject#save()
     */
    public function save(){
    	
    	if( !$this->allowEdit() ){
			
			trigger_error( "Appel de DBObject::save() non autorisé", E_USER_ERROR );
			return;
			
		}

		parent::save();
		
		DBUtil::query( "DELETE FROM credit_rows WHERE idcredit = '{$this->idcredit}'" );
		
		$it = $this->items->iterator();
	
		while( $it->hasNext() )
			$it->next()->save();
	
    }
    
    //----------------------------------------------------------------------------
    /**
     * @return Invoice
     */
    public function &getInvoice(){ return $this->invoice; }
    //----------------------------------------------------------------------------
    /**
     * @return Salesman
     */
    public function &getSalesman(){ return $this->salesman; }
    
    //----------------------------------------------------------------------------
    /**
     * @return Customer
     */
    public function &getCustomer(){ return $this->invoice->getCustomer(); }
    
        //----------------------------------------------------------------------------
	/**
     * @access public
     * @return ArrayList<TradeItem>
     */
    public function &getItems(){ return $this->items; }
    
	//----------------------------------------------------------------------------
	/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public final function getItemCount(){ return $this->items->size(); }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le $index-ième élément du panier
	 * @return CreditItem
	 */
	public final function &getItemAt( $index ){ return $this->items->get( $index ); }
	
	//----------------------------------------------------------------------------
	/**
	 * Suppression de tous les articles du panier
	 * @access public
	 * @return void
	 */
	public function removeItems(){ $this->items = new ArrayList( "CreditItem" ); }
	
	//----------------------------------------------------------------------------
	/**
	 * Supprime un élément donné du panier
	 * @param int $index Le $index-ième élément à supprimer
	 * @return void
	 */
	public function removeItemAt( $index ){
		
		$this->items->remove( $this->items->get( $index ) );
		
		/* réindexation */
		
		$index = 1;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$it->next()->set( "idrow", $index++ );
		
	}
	
	//----------------------------------------------------------------------------
	/**
     * Créer une nouvelle ligne article
     * @access protected
     * @return CreditItem une ligne article
     */
	protected function &createItem(){
		
		$idrow = $this->items->size() + 1;
		
		DBUtil::query( "INSERT INTO credit_rows( idcredit, idrow ) VALUES( '{$this->idcredit}', '$idrow' )" );

		$this->items->add( new CreditItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	public function addInvoiceItem( InvoiceItem $invoiceItem ){
		
		$creditItem =& $this->createItem();
		
		$creditItem->set( "quantity", $invoiceItem->get( "quantity" ) );
		$creditItem->set( "idbilling_buyer_row", 	$invoiceItem->get( "idrow" ) );
		$creditItem->set( "unit_price", 			$invoiceItem->get( "discount_price" ) );
		$creditItem->set( "discount_price", 		$invoiceItem->get( "discount_price" ) );
		$creditItem->set( "vat_rate", 				$invoiceItem->get( "vat_rate" ) );
		$creditItem->set( "designation", 			$invoiceItem->get( "designation" ) );

		$creditItem->save();
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Supprime un avoir complètement jusquà ce qu'il n'y en ait plus
	 * @param int $idcredit le numéro d'avoir à supprimer
	 */
	 public final static function delete( $idcredit ){
	 	// ------- Mise à jour de l'id gestion des Index  #1161
	 	$editable =& DBUtil::query( "SELECT editable FROM `credits` WHERE idcredit = '$idcredit' LIMIT 1" )->fields('editable');
	 	
	 	if( !$editable )
	 		return false;
	 		
	 	DBUtil::query( "DELETE FROM `credits` WHERE idcredit = '$idcredit' LIMIT 1" );
	 	DBUtil::query( "DELETE FROM `credit_rows` WHERE idcredit = '$idcredit'" );
	 	//@todo : décréditer
	 	
	 	return true;
	 	
	 }
	 
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne true si l'avoir est une annulation partielle de facture, sinon false
	 * Ne tient pas compte de la refacturation du port
	 * @return bool
	 */
	public function isPartial(){
		
		$invoiceQuantity = DBUtil::query( 

			"SELECT SUM( quantity ) AS quantity
			FROM billing_buyer_row
			WHERE idbilling_buyer = '" . $this->invoice->getId() . "'
			GROUP BY idbilling_buyer"
		
		)->fields( "quantity" );
	
		$creditQuantity = DBUtil::query( 
		
			"SELECT SUM( quantity ) AS quantity
			FROM credit_rows
			WHERE idcredit = '" . $this->getId() . "'
			GROUP BY idcredit" 
		
		)->fields( "quantity" );

		return 	$creditQuantity != $invoiceQuantity;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * @param Salesman $salesman
	 * @return void
	 */
	public function setSalesman( Salesman $salesman ){
		
		$this->recordSet[ "iduser" ] = $salesman->getId();
		$this->salesman =& $salesman;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getWeight()
	 */
	public function getWeight(){
	
		$weight = 0.0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			$weight += $item->get( "weight" ) * $item->get( "quantity" );
			
		}
		
		return $weight; 
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getChargesET()
	 */
	public function getChargesET(){ 
		
		if( B2B_STRATEGY )
			return $this->get( "credited_charges" ) - $this->get( "total_charge_ht" ); 
	
		return $this->get( "credited_charges" ) - round( $this->get( "total_charge" ) / ( 1.0 + $this->getVATRate() / 100.0 ), 2 ); 
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getChargesATI()
	 */
	public function getChargesATI(){
	
		if( B2B_STRATEGY )
			return $this->get( "credited_charges" ) - round( $this->get( "total_charge_ht" ) * ( 1.0 + $this->getVATRate() / 100.0 ), 2 ); 
	
		return $this->get( "credited_charges" ) - $this->get( "total_charge" );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#applyVAT()
	 */
	public function applyVAT(){ return $this->get( "vat_rate" ) > 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getVATRate()
	 */
	public function getVATRate(){ return $this->get( "vat_rate" ); }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getDiscountRate()
	 */
	public function getDiscountRate(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getBillingRate()
	 */
	public function getBillingRate(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getRebateAmount()
	 */
	public function getRebateAmount(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getDownPaymentAmount()
	 */
	public function getDownPaymentAmount(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#getCreditAmount()
	 */
	public function getCreditAmount(){ return 0.0; }

	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le montant total HT
	 */
	public function getTotalET(){ return $this->priceStrategy->getTotalET(); }
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * @return float le montant total TTC
	 */
	public function getTotalATI(){ return $this->priceStrategy->getTotalATI(); }

	//----------------------------------------------------------------------------
	/**
	 * Net à payer sur facture
	 * @return float
	 */
	public function getNetBill() { return $this->priceStrategy->getTotalATI(); }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne l'adresse de facturation utilisée
	 * @return Address une référence vers l'adresse de facturation
	 */
	public function &getInvoiceAddress(){ return $this->invoice->getInvoiceAddress(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @return Address une référence vers l'adresse de livraison
	 */
	public function &getForwardingAddress(){ return $this->invoice->getForwardingAddress(); }

	//----------------------------------------------------------------------------

}

?>