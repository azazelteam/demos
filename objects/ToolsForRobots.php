<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ...?
 */


include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
include_once( dirname( __FILE__ ) . "/Util.php" );
include_once( dirname( __FILE__ ) . "/CustomerAddress.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class ToolsForRobots{
	
	//----------------------------------------------------------------------------
	
	private $idcustomer;
	private $idcontact;
	private $password;
	private $contactFields;
	private $fields;
	private $address;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * Créé l'object client dont le numéro est donné
	 * Pour créer une nouveau client, utilisez Customer::create()
	 * @param int $idcustomer le numéro du client
	 */
	function __construct( $idcustomer, $idcontact = 0 ){
		
		$this->idcustomer		=  $idcustomer;
		$this->idcontact		=  $idcontact;
		
		$this->password			= array();
		$this->contactFields	= array();
		$this->fields			= array();
		
		$this->address 			= new CustomerAddress( $this->idcustomer, $this->idcontact );
		
		//Données de la base
		$this->load();
		$this->loadContact();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les informations du client
	 * @return void
	 */
	
	private function load(){
		
		$this->fields = array();
		
		$query = "
		SELECT * FROM `buyer`
		WHERE `idbuyer` = '" . $this->idcustomer . "'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les informations du contact
	 * @param int $idcontact l'identifiant du contact
	 * @return void
	 */
	
	private function loadContact(){
		
		$this->contactFields = array();
		
		$query = "
		SELECT * FROM `contact` 
		WHERE `idbuyer` = '{$this->idcustomer}'
		AND `idcontact` = '{$this->idcontact}' 
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->contactFields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Crée le client dont l'adresse email est $email, son contact principal et le compte utilsateur qui va avec
	 * Gérer les autres informations ailleurs via des setValue()
	 * @param int $email l'email du client à créer
	 * @return Customer l'objet créé
	 */
	
	public static function &create( $email, $firstname, $lastname ){
		
		//Vérification de l'existence du client dans la base
		$query = "SELECT `mail`, `idcontact`, `idbuyer` FROM `contact` WHERE `mail` = '$email' LIMIT 1";
		$rs = DBUtil::query( $query );
		
		if( $rs->RecordCount() ){
			
			$error[0] = $rs->fields("idcontact");
			$error[1] = $rs->fields("idbuyer");
			return $error ;
			die( "Cette adresse email est déjà utilisée par un client : $email" );
		}
			
		
		//Création du client
		$username = 'Service Com.' ; 
		$now = date( "Y-m-d H:i:s" );
		
		$query = "INSERT IGNORE INTO `buyer` (
			`iduser`, 
			`create_date`, 
			`lastupdate`, 
			`username`
		 ) VALUES ( 
			'" . DBUtil::getParameter( "contact_commercial" ) . "', 
			'$now', 
			'$now', 
			'$username' 
		)";

		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de créer le nouveau client" );
		
		$idcustomer = DBUtil::getConnection()->Insert_ID();
		
		//@todo: méthode de création d'un contact
		//Création du contact principal
		$query = "INSERT IGNORE INTO `contact` ( 
			`idbuyer`, 
			`idcontact`,
			`lastname`,
			`firstname`,
			`mail` 
		) VALUES ( 
			'$idcustomer', 
			'0',
			" . DBUtil::quote( $lastname ) . ",
			" . DBUtil::quote( $firstname ) . ",
			" . DBUtil::quote( $email ) . "
		)";

		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de créer le nouveau contact" );
		
		//@todo: faire une méthode qui crée un compte utilisateur pour le contact
		//Création du compte utilisateur du contact principal
		$query = "INSERT IGNORE INTO `user_account` (
			`idbuyer`, 
			`idcontact`, 
			`login`,
			`password`
		) VALUES ( 
			'$idcustomer', 
			'0', 
			" . DBUtil::quote( $email ) . ",
			MD5(" . DBUtil::quote( Util::getRandomPassword() ) . " )
		)";

		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de créer le nouveau compte utilisateur" );
		
		//Instanciation du client
		return new ToolsForRobots( $idcustomer );

	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne le mot de passe du contact
	 * @param int $idcontact le numéro de contact
	 * @return string le mot de passe du contact
	 */
	
	public function getContactPassword( $idcontact = 0 ){
		
		return $this->password[ $idcontact ];
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Affecte un mot de passe au contact
	 * @param int $idcontact l'identifiant du contact
	 * @param string $password le mot de passe du contact
	 * @return string le mot de passe
	 */
	
	public function setContactPassword( $password = false ){
		
		if( !$password || strlen( $password ) < 6 )
			$password = Util::getRandomPassword();
		
		$query = "UPDATE `user_account` 
		SET `password` = MD5( '$password' ) 
		WHERE `idbuyer` = '" . $this->idcustomer . "' 
		AND `idcontact` = '" . $this->idcontact . "'";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de créer le compte utilisateur" );
		
		$this->password[ $this->idcontact ] = $password;
		
		return $password;
		
	}

	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Crée un identifiant de connexion pour le Front Office
	 * @param string $email l'email à utiliser comme login 
	 * @return $password le mot de passe créé pour le compte
	 */
	public function createAccount( $email ){
		
		$rs =& DBUtil::query( "SELECT login FROM user_account WHERE login = '$email'" );
		
		if( $rs === false )
			trigger_error( "Un problème est survenu lors de la vérification de l'identifiant", E_USER_ERROR );
		
		if( $rs->RecordCount() )
			return false;
		
		$password = Util::getRandomPassword();
		
		$rs =& DBUtil::query( "INSERT INTO user_account (
				idbuyer, idcontact, login, password
			) VALUES (
				" . $this->idcustomer . ",
				" . $this->idcontact . ",
				'$email',
				MD5( '$password' )
			)" );
		
		
		if( $rs === false )
			trigger_error( "Un problème est survenu lors de la création du compte de l'utilisateur $email", E_USER_ERROR );
		
		return $password;
		
	}

	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Crée un identifiant de connexion pour le Front Office
	 * @param string $email l'email à utiliser comme login 
	 * @return $password le mot de passe créé pour le compte
	 */
	public function regeneratePassword(){
		
		$password = Util::getRandomPassword();
		
		$rs =& DBUtil::query( "UPDATE user_account SET password = '$password' WHERE idbuyer = " . $this->idcustomer . " AND idcontact = " . $this->idcontact );
		
		if( $rs === false )
			trigger_error( "Un problème est survenu lors de la modification du mot de passe de l'utilisateur", E_USER_ERROR );
		
		return $password;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet
	 * @return void
	 * @todo UPDATE de la table contact
	 */
	 
	 public function update(){
	
		$updatableFields = array( 
		
			"company", 
			"adress", 
			"zipcode",
			"city",
			"idstate",
			"idsource",
			"iduser"
		
		);
		
		foreach( $this->fields as $fieldname => $value ){
			
			if( $fieldname != "idbuyer" )
				$updatableFields[] = $fieldname;
			
		}
		
		$query = "UPDATE `buyer`
		SET ";
		
		$i = 0;
		
		foreach( $updatableFields as $fieldname ){
			
			if( $i )
				$query .= ", ";
			
			$query .= "`$fieldname` = '" . Util::html_escape( $this->fields[ $fieldname ] ) . "'";
			
			$i++;
			
		}
		
		$query.=" WHERE `idbuyer` = '" . $this->getId() . "'";
		
		DBUtil::query( $query );
		
	 }
	 	 
	//----------------------------------------------------------------------------
	/**
	 * Affecte une valeur à une propriété du contact donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 
	public function setContactFields($key, $value){
		
		$query = "UPDATE contact SET `".$key."` = '".$value."' WHERE idbuyer = '".$this->getId()."'" ;
		DBUtil::query( $query );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne la valeur d'un attribut donné du client
	 * @param string $key l'attribut du client dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	
	public function get( $key ){
		
		return $this->fields[ $key ];
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un attribut donné du contact
	 * @param string $key l'attribut du client dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	
	public function getContactValue( $key ){
		
		return $this->contactFields[ $key ];
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Affecte une valeur à une propriété du client donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 	 
	public function set( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Affecte une valeur à une propriété du contact donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 	 
	public function setContactValue( $key, $value ){
		
		$this->contactFields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne l'identifiant du client
	 * @return int
	 */
	
	public function getId(){
		
		return $this->idcustomer;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne l'adresse du client
	 * @return CustomerAddress une référence vers l'adresse du client
	 */
	public function &getAddress(){
		
		$address =& $this->address;
		
		return $address;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * abonne le client à la newsletter du site
	 * @return bool true en cas de succès, sinon false
	 */
	public function subscribeNewsletter(){
		
		include_once( "Validate.php" );
		include_once( "Validate/FR.php" );
		
		if( !Validate::email( $this->getContactValue( "mail" ) ) )
			return false;
			
		$query = "
		INSERT IGNORE INTO newsletter_subscriptions ( email, gender, lastname, firstname, company, address, address2, zipcode, city, phonenumber )
		VALUES(
			'" . Util::html_escape( $this->getContactValue( "mail" ) ) . "',
			'" . intval( $this->getContactValue( "title" ) ) . "',
			'" . Util::html_escape( $this->getContactValue( "lastname" ) ) . "',
			'" . Util::html_escape( $this->getContactValue( "firstname" ) ) . "',
			'" . Util::html_escape( $this->get( "company" ) ) . "',
			'" . Util::html_escape( $this->get( "adress" ) ) . "',
			'" . Util::html_escape( $this->get( "adress_2" ) ) . "',
			'" . Util::html_escape( $this->get( "zipcode" ) ) . "',
			'" . Util::html_escape( $this->get( "city" ) ) . "',
			'" . Util::html_escape( $this->getContactValue( "mail" ) ) . "'
		)";
		
		DBUtil::query( $query );
	
		return DBUtil::getAffectedRows() == 1;
		
	}
	
	//-------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * Indique si le client à été refusé au factor
	 * @static
	 * @param int $idcustomer l'id du client
	 * @return bool true si le client a déjà été réfusé, false sinon
	 */
	 
	public static function isRefusedByFactor( $idcustomer ){
		
		$query = "
		SELECT COUNT(*) AS refusalCount
		FROM `billing_buyer`
		WHERE idbuyer = '$idcustomer'
		AND factor_refusal_date != '0000-00-00'";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de contrôler les refus du factor du client" );
		
		return $rs->fields( "refusalCount" ) > 0;
		
	}
	
	//----------------------------------------------------------------------------
	/* deprecations */
	
	/**
	 * Regarde si le client appartient à un groupement
	 * @deprecated préférer DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $idcustomer );
	 * @param void
	 * @return int le numéro de groupement du client
	 */
	
	public static function getGrouping( $idcustomer ){
		
		$query = "SELECT idgrouping FROM buyer WHERE idbuyer ='".$idcustomer."'";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false ) 
			trigger_error( "Impossible de récupérer le groupe du client", E_USER_ERROR );
		
		$idgrouping = $rs->fields("idgrouping");
		
		if( $idgrouping == 0 )
			return false;
		else
			return $idgrouping ;
		
	}
	//----------------------------------------------------------------------------
	
	public static function getState($country){
		
		$db	= DBUtil::getConnection();
		$query = "SELECT idstate FROM state WHERE name_1 LIKE '%".$country."%' LIMIT 1" ;
		$rsState = $db->Execute($query);
		
		return $rsState->RecordCount() ? $rsState->fields('idstate') : 0;
		
	}
	
	public static function getSource($source){
		
		$db	= DBUtil::getConnection();
		$query = "SELECT idsource FROM source WHERE source_code like \"%$source%\" LIMIT 1" ;
		$rs = $db->Execute($query);
		return $rs->fields('idsource') ;
		
	}
	
	public static function getUserSc(){
		
		$db	= DBUtil::getConnection();
		$query = "SELECT iduser FROM user WHERE initial = 'SC' AND login like '%Commercial%'  LIMIT 1" ;
		$rs = $db->Execute($query);
	
		return $rs->fields('iduser') ;
	}
	
	public static function doCleanPhone($tel){
		
		$tel = str_replace('.', "", $tel);
		$tel = str_replace(' ', "", $tel);
		$tel = str_replace('/', "", $tel);
		$tel = str_replace('|', "", $tel);
		$tel = str_replace(',', "", $tel);
		$tel = str_replace('-', "", $tel);
		$tel = str_replace('\\', "", $tel);
		
		return $tel ; 
	
	}
	
	public static function getConnectionToMailBox($devMailBox, $devPassword, $prodMailBox, $prodPassword){
		
		global $GLOBAL_DEBUG_LEVEL ;
		
		// Login, pwd
		if($GLOBAL_DEBUG_LEVEL == 'dev'){
			$user 		= $devMailBox ;
			$password 	= $devPassword ;
		}
		elseif($GLOBAL_DEBUG_LEVEL == 'prod'){
			$user 		= $prodMailBox ;
			$password 	= $prodPassword ;
		}
		
		$host 		= 'mail.akilae-saas.fr' ;
		$mbox = imap_open('{'.$host.':143/imap/notls}', $user, $password) or die(imap_last_error()."<br>Connection failure!");
	
		return $mbox;

	}
	
	public static function closeMailBox($mailBox){
		imap_close($mailBox) ;
		return true ;
	}
	
	public static function deleteMailFromMailBox($mailBox, $messageId){
		
		// On marque le message comme supprimée
		//imap_delete($mbox, $messageId) ;
		imap_delete($mailBox, 1 );
		// On supprime le message
		$ret = imap_expunge($mailBox);
	
		return true ;
	
	}
	//----------------------------------------------------------------------------
	
	public static function extractPart($begin, $end, $text){
		
		$debut = strpos( $text, $begin ) + strlen( $begin );
		if($end != ''){
			$fin = strpos( $text, $end );
		}
		else{
			$fin = strlen($text);
		}
		
		$search = substr( $text, $debut, $fin - $debut ); 
		
		return $search ; 
	
	}
	
	
}

?>