<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object GoogleMap
 */
 
define( "DEBUG_GOOGLEMAP", 0 );

include_once( "$GLOBAL_START_PATH/objects/sitemapobject.php" );
include_once( "$GLOBAL_START_PATH/objects/GoogleMapNode.php" );
include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
include_once( "$GLOBAL_START_PATH/objects/URLFactory.php" );


class GoogleMap{
	
	//-------------------------------------------------------------------------------------------

	/**
	 * @var $MAX_URL_COUNT
	 * Nombre d'URL maximum autorisé par google pour un fichier sitemap
	 */
	public static $MAX_URL_COUNT 			= 50000;
	/**
	 * Méthodes de récupération de la date de dernière modification des fichiers du site
	 */
	public static $LAST_MOD_CURRENTDATE 	= 0;
	public static $LAST_MOD_CHECKSUM 		= 1;
	public static $LAST_MOD_READFROMFILE	= 2;
	/**
	 * Fréquence de mise à jour des pages du site
	 */
	public static $CHANGE_FREQ_NEVER 		= "never";
	public static $CHANGE_FREQ_ALWAYS 		= "always";
	public static $CHANGE_FREQ_HOURLY 		= "hourly";
	public static $CHANGE_FREQ_DAILY 		= "daily";
	public static $CHANGE_FREQ_WEEKLY 		= "weekly";
	public static $CHANGE_FREQ_MONTHLY 		= "monthly";
	public static $CHANGE_FREQ_YEARLY 		= "yearly";

	//-------------------------------------------------------------------------------------------
	
	private $staticPages;
	private $outputfile;
	private $getLastModificationMethod;
	private $categoryChangeFrequency;
	private $productChangeFrequency;
	private $pdfChangeFrequency;
	private $staticPageChangeFrequency;
	private $indexPDF;
	private $blackLists;

	private $configPath;
	private $children;
	private $lastCreationDate;
	
	private $rewriteURLs;
	private $URLRewritingCategoryPattern;
	private $URLRewritingProductPattern;
	private $URLRewritingPDFPattern;
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $name le nom du noeud
	 */
	public function GoogleMap(){
		
		global 	$GLOBAL_START_PATH;
		
		$this->children 					= array();
		$this->lastCreationDate 			= "";
		$this->configPath 					= "$GLOBAL_START_PATH/config/sitemap.xml";
		$this->outputfile 					= "/sitemap.xml";

		$this->getLastModificationMethod 	= self::$LAST_MOD_CURRENTDATE;
		$this->categoryChangeFrequency	 	= self::$CHANGE_FREQ_MONTHLY;
		$this->productChangeFrequency 		= self::$CHANGE_FREQ_MONTHLY;
		$this->pdfChangeFrequency 			= self::$CHANGE_FREQ_MONTHLY;
		$this->staticPageChangeFrequency 	= self::$CHANGE_FREQ_MONTHLY;
		
		$this->indexPDF 					= false;
		$this->staticPages 					= array();
		$this->blackLists 					= array( 
		
			"categories" 	=> array(),
			"products" 		=> array(),
			"pdf" 			=> array()
			
		);

		//URL Rewritting
		
		$this->rewriteURLs = DBUtil::getParameter( "RewriteEngine" ) ? true : false;
		
		$this->URLRewritingCategoryPattern	=  $this->rewriteURLs ? DBUtil::getParameter( "URLRewritingCategoryPattern" ) : "";
		$this->URLRewritingProductPattern	=  $this->rewriteURLs ? DBUtil::getParameter( "URLRewritingProductPattern" ) : "";
		$this->URLRewritingPDFPattern		=  $this->rewriteURLs ? DBUtil::getParameter( "URLRewritingPDFPattern" ) : "";
	
		$this->readConfig();
		
	}
	
	//-------------------------------------------------------------------------------------------

	/**
	 * Définit le chemin absolu du fichier XML à générer
	 * @param string $name le chemin absolu du fichier XML à générer
	 * @return void
	 */
	public function setOutputFile( $outputfile ){ $this->outputfile = $outputfile; }
	/**
	 * Définit la méthode de reconnaissance pour la date de dernière modification
	 * @param int $method la méthode de reconnaissance ( $LAST_MOD_CURRENTDATE, $LAST_MOD_CHECKSUM, $LAST_MOD_READFROMFILE )
	 * @return void
	 */
	public function setGetLastModificationMethod( $method ){ $this->getLastModificationMethod = $method; }
	/**
	 * Définit la fréquence de mise à jour pour les catégories
	 * @param int $frequency la fréquence de mise à jour ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return void
	 */
	public function setCategoryChangeFrequency( $frequency ){ $this->categoryChangeFrequency = $frequency; }
	/**
	 * Définit la fréquence de mise à jour pour les produits
	 * @param int $frequency la fréquence de mise à jour ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return void
	 */
	public function setProductChangeFrequency( $frequency ){ $this->productChangeFrequency = $frequency; }
	/**
	 * Définit la fréquence de mise à jour pour les PDF
	 * @param int $frequency la fréquence de mise à jour ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return void
	 */
	public function setPDFChangeFrequency( $frequency ){ $this->pdfChangeFrequency = $frequency; }
	/**
	 * Définit la fréquence de mise à jour pour les pages statiques
	 * @param int $frequency la fréquence de mise à jour ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return void
	 */
	public function setStaticPageChangeFrequency( $frequency ){ $this->staticPageChangeFrequency = $frequency; }
	/**
	 * Définit s'il faut indexer les PDF ou non
	 * @param bool $indexPDF
	 * @return void
	 */
	public function setIndexPDF( $indexPDF = true ){ $this->indexPDF = $indexPDF; }
	/**
	 * Définit s'il faut ou non utiliser le module apache d'URL rewriting
	 * @param bool $rewriteURLs
	 * @return void
	 */
	private function setRewriteURLs( $rewriteURLs ){ $this->rewriteURLs = $rewriteURLs; }
	/**
	 * Définit le motif de réécriture des URLs pour les catégories ( Apache RewriteEngine )
	 * @param string $pattern le motif de réécriture des URLs ( Apache RewriteEngine )
	 * @return void
	 */
	public function setURLRewritingCategoryPattern( $pattern ){ $this->URLRewritingCategoryPattern = $pattern; }
	/**
	 * Définit le motif de réécriture des URLs pour les produits ( Apache RewriteEngine )
	 * @param string $pattern le motif de réécriture des URLs ( Apache RewriteEngine )
	 * @return void
	 */
	public function setURLRewritingProductPattern( $pattern ){ $this->URLRewritingProductPattern = $pattern; }
	/**
	 * Définit le motif de réécriture des URLs pour les PDFs ( Apache RewriteEngine )
	 * @param string $pattern le motif de réécriture des URLs ( Apache RewriteEngine )
	 * @return void
	 */
	public function setURLRewritingPDFPattern( $pattern ){ $this->URLRewritingPDFPattern = $pattern; }
	/**
	 * Ajoute une catégorie à la liste des catégories à ignorer lors de l'indexation
	 * @param int $idCategory l'identifiant de la catégorie à ignorer
	 * @return void
	 */
	public function blackListCategory( $idCategory ){ $this->blackLists[ "categories" ][] = $idCategory; }
	/**
	 * Ajoute un produit à la liste des produits à ignorer lors de l'indexation
	 * @param int $idProduct l'identifiant du produit à ignorer
	 * @return void
	 */
	public function blackListProduct( $idProduct ){ $this->blackLists[ "products" ][] = $idProduct; }
	/**
	 * Ajoute un produit à la liste des PDFs à ignorer lors de l'indexation
	 * @param int $idProduct l'identifiant du produit à ignorer
	 * @return void
	 */
	public function blackListPDF( $idProduct ){ $this->blackLists[ "pdf" ][] = $idProduct; }
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le chemin absolu du fichier générer
	 * @return string
	 */
	public function getOutputFile(){ return $this->outputfile; }
	/**
	 * Retourne la méthode utilisée pour la reconnaissance de la date de dernière modification ( $LAST_MOD_CURRENTDATE, $LAST_MOD_CHECKSUM, $LAST_MOD_READFROMFILE )
	 * @return int
	 */
	public function getGetLastModificationMethod(){ return $this->getLastModificationMethod; }
	/**
	 * Retourne la fréquence de mise à jour des catégories ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return int
	 */
	public function getCategoryChangeFrequency(){ return $this->categoryChangeFrequency; }
	/**
	 * Retourne la fréquence de mise à jour des produits ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return int
	 */
	public function getProductChangeFrequency(){ return $this->productChangeFrequency; }
	/**
	 * Retourne la fréquence de mise à jour des PDF ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return int
	 */
	public function getPDFChangeFrequency(){ return $this->pdfChangeFrequency; }
	/**
	 * Retourne la fréquence de mise à jour des pages statiques ( $CHANGE_FREQ_NEVER, $CHANGE_FREQ_MONTHLY,... )
	 * @return int
	 */
	public function getStaticPageChangeFrequency(){ return $this->staticPageChangeFrequency; }
	
	/**
	 * Retourne true si les PDFs sont indéxés, sinon false
	 * @return bool
	 */
	public function getIndexPDF(){ return $this->indexPDF; }
	/**
	 * Retourne le nombre de pages statiques à indexer
	 * @return int
	 */
	public function getStaticPageCount(){ return count( $this->staticPages ); }
	/**
	 * Retourne l'URI de la $index-ième page statique
	 * @return string
	 */
	public function &getStaticPageAt( $index ){ return $this->staticPages[ $index ]; }
	/**
	 * Retourne le nombre de noeuds à indexer
	 * @return int
	 */
	public function getChildCount(){ return count( $this->children ); }
	/**
	 * Retourne le $index-ième noeud à indexer
	 * @return array
	 */
	public function &getChildAt( $index ){ return $this->children[ $index ]; }
	/**
	 * Retourne l'arbre XML du fichier sitemap
	 * @return string
	 */
	public function getXMLDocument(){ return $this->buildXMLDocument(); }
	/**
	 * Retourne le motif de réécriture des URLs pour les catégories ( Apache RewriteEngine )
	 * @return string
	 */
	public function getURLRewritingCategoryPattern(){ return $this->URLRewritingCategoryPattern; }
	/**
	 * Retourne le motif de réécriture des URLs pour les produits ( Apache RewriteEngine )
	 * @return string
	 */
	public function getURLRewritingProductPattern(){ return $this->URLRewritingProductPattern; }
	/**
	 * Retourne le motif de réécriture des URLs pour les PDFs ( Apache RewriteEngine )
	 * @return string
	 */
	public function getURLRewritingPDFPattern(){ return $this->URLRewritingPDFPattern; }
	/**
	 * Retourne la liste des catégories blacklistées
	 * @return array
	 */
	public function getBlackListedCategories(){ return $this->blackLists[ "categories" ]; }
	/**
	 * Retourne la liste des produits blacklistés
	 * @return array
	 */
	public function getBlackListedProducts(){ return $this->blackLists[ "products" ]; }
	/**
	 * Retourne la liste des catégories blacklistés
	 * @return array
	 */
	public function getBlackListedPDF(){ return $this->blackLists[ "pdf" ]; }
	/**
	 * Retourne la date du dernier sitemap généré
	 * @return string
	 */
	public function getLastCreationDate(){ return $this->lastCreationDate; }
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute un noeud
	 * @param string $name le nom du nom
	 * @param string $uri l'URI du noeud par rapport à la racine du site
	 * @return void
	 */
	private function addChild( $name, $uri ){
		
		$this->children[] = new GoogleMapNode( $name, $uri, $this );
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Génère un fichier sitemap
	 * @return void
	 */
	public function crawl(){
	
		$this->mapCategories();
		
		$this->readLastModificationDates();
		
	//	if( strlen( $this->outputfile ) )

			$this->saveToXMLFile();
			
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Récupère les dates de dernières modifications de tous les noeuds
	 * @return void
	 */
	private function readLastModificationDates(){
		
		$i = 0;
		while( $i < $this->getChildCount() ){
			
			$child = &$this->getChildAt( $i );
			$child->readLastModificationDate();
			$i++;
			
		}
		
		$i = 0;
		while( $i < $this->getStaticPageCount() ){
			
			$page = &$this->getStaticPageAt( $i );
			$page->readLastModificationDate();
			
			$i++;
			
		}
		
	}
	
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne un document XML
	 * @return string
	 */
	private function buildXMLDocument(){
		
		$xmlDocument = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		//$xmlDocument .= "\n\t<urlset xmlns=\"http://www.google.com/schemas/sitemap/0.84\">";
		$xmlDocument .= "\n\t<urlset  xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\" xmlns:image=\"http://www.google.com/schemas/sitemap-image/1.1\">";
			
		$i = 0;

		while( $i <count($this->children) ){
			
			$xmlDocument .= $this->getChildURLElement( $this->children[ $i ] );
			
			$i++;
			
		}
		
		$i = 0;
		while( $i < count( $this->staticPages ) ){
			
			$xmlDocument .= $this->getChildURLElement( $this->staticPages[ $i ] );
			
			$i++;
			
		}
		
		$xmlDocument .= "\n\t</urlset>";
		
		return $xmlDocument;
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne un élément XML d'après un noeud
	 * @param GoogleMapNode $child le noeud
	 * @return string une élément XML
	 */
	private function getChildURLElement( GoogleMapNode &$child ){
		global $GLOBAL_START_URL;
		$XMLElement 		= "";
		$idchild = $child->getId();
		
		include_once( "$GLOBAL_START_PATH/objects/DBUtil.php" );
		include_once( "catalog/CProduct.php" );
		include_once( "flexy/proxies/CProductProxy.php" );
		//create category node
		$blogCategory =array();
		
		 $query = "SELECT tr.idproduct, p.idcategory
					FROM trade_product tr, product p
					WHERE p.idproduct = tr.idproduct
					";
			
		$rs =& DBUtil::query( $query );
			
		while( !$rs->EOF )
		{
			$idcategory = $rs->fields("idcategory");
			if(!in_array($idcategory , $blogCategory))
				array_push($blogCategory , $idcategory);
						

			$rs->MoveNext();

		}
		
		if( !in_array( $idchild, $this->blackLists[ "categories" ] ) && !in_array( $idchild, $blogCategory )  )
		{
			$Name 			    = $child->getName();
			$location 			= $child->getLocation();
			$lastmodification 	= $child->getLastModification();
			$changeFrequency 	= $child->getChangeFrequency();
			$priority 			= $child->getPriority();
			
			$locc = strstr($location, '.php', true);
			$pageurl=$location; 					
			$XMLElement .= "\n\t\t<url>";
				
			$XMLElement .= "\n\t\t\t<loc>$location</loc>";
			//$XMLElement .= "\n\t\t\t<image:image>";
			
			if ($locc != "")
				$location = $locc;
				$imageloc=$location.".jpg";

			$add_zoom_indexed_image=0;
			 $query11 = "SELECT  `paramvalue` FROM  `parameter_admin` WHERE  `idparameter` =  'add_zoom_indexed_image'";
			$rs11 =& DBUtil::query( $query11 );
			$add_zoom_indexed_image=0;
			if($rs11->RecordCount()>0){
			$add_zoom_indexed_image=$rs11->fields('paramvalue');
			}

			if (((strpos($pageurl,'php')== false)&&((strpos($pageurl,'-t-')== false))) ||((strpos($pageurl,'php')!== false)&&(file_exists($imageloc)))) {
				//((strpos($pageurl,'-t-')!== false)&&(file_exists($imageloc)))

			if( $add_zoom_indexed_image==1)
			{
			if(strpos($pageurl,'-p-')==false)
			{
				$XMLElement .= "\n\t\t\t<image:image>";
				//if(file_exists ($imageloc )){
				$XMLElement .= "\n\t\t\t<image:loc>$location".".jpg</image:loc>"."";
				//}
				//$XMLElement .= "\n\t\t\t<image:title>$Name</image:title>";
				//$XMLElement .= "\n\t\t\t<image:caption>$Name</image:caption>";
				$XMLElement .= "\n\t\t\t</image:image>";
				}
				}
			else{
					$XMLElement .= "\n\t\t\t<image:image>";
				//if(file_exists ($imageloc )){
				$XMLElement .= "\n\t\t\t<image:loc>$location".".jpg</image:loc>"."";
				//}
				//$XMLElement .= "\n\t\t\t<image:title>$Name</image:title>";
				//$XMLElement .= "\n\t\t\t<image:caption>$Name</image:caption>";
				$XMLElement .= "\n\t\t\t</image:image>";
			}
			if(strpos($pageurl,'-p-')== true)
			{

		

			$XMLElement .= "\n\t\t\t<image:image>";
			//if(file_exists ($imageloc )){
			$XMLElement .= "\n\t\t\t<image:loc>$location-zoom".".jpg</image:loc>"."";
			//}
			//$XMLElement .= "\n\t\t\t<image:title>$Name</image:title>";
			//$XMLElement .= "\n\t\t\t<image:caption>$Name</image:caption>";
			$XMLElement .= "\n\t\t\t</image:image>";

				$pos=strpos($pageurl,"-p-");
				$idproduct=substr($pageurl, $pos+3);
			
				$idproduct=trim($idproduct);
				
				$idproduct=intval( $idproduct );
				 $query1 = "SELECT *from product where idproduct=$idproduct";
			
		$rs1 =& DBUtil::query( $query1 );
		
		
		if($rs1->RecordCount()>0){
			$cproduct= new CProduct($idproduct);

			$cproductproxy=new CProductProxy($cproduct);

			$images=$cproductproxy->getImages();
			
			if(count($images)>1)
			{
				$m=1;
				if($add_zoom_indexed_image==1)
				{
					while($m<count($images))
					{

					$uri=$GLOBAL_START_URL.$cproductproxy->getZoomURI($m);	
					$XMLElement .= "\n\t\t\t<image:image>";
					$XMLElement .= "\n\t\t\t<image:loc>$uri</image:loc>"."";
					$XMLElement .= "\n\t\t\t</image:image>";
					$m++;
					}
				}
				else{
					while($m<count($images))
					{ 
						$uri=$GLOBAL_START_URL.$cproductproxy->getImage(1024,$m);
						$XMLElement .= "\n\t\t\t<image:image>";
						$XMLElement .= "\n\t\t\t<image:loc>$uri</image:loc>"."";
						$XMLElement .= "\n\t\t\t</image:image>";
						$m++;
					}
				}

			}

				//}
				
			
		}}		
			}
			//$XMLElement .= "\n\t\t\t<image:loc>$location".".jpg</image:loc>"."";
			//$XMLElement .= "\n\t\t\t<image:title>$Name</image:title>";
			//$XMLElement .= "\n\t\t\t<image:caption>$Name</image:caption>";
			//$XMLElement .= "\n\t\t\t</image:image>";
			$XMLElement .= "\n\t\t\t<lastmod>$lastmodification</lastmod>";
			$XMLElement .= "\n\t\t\t<changefreq>$changeFrequency</changefreq>";
			$XMLElement .= "\n\t\t\t<priority>$priority</priority>";
	
			$XMLElement .= "\n\t\t</url>";
		}
		
		return $XMLElement;	
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Construit la liste de toutes les catégories
	 * @return void
	 */
	private function mapCategories(){
			
		$sitemap = new Sitemap( true );
		
		$root =& $sitemap->getRoot();
		$nodes = $root[ "children" ];
		$i = 0;
		while( $i < count( $nodes ) ){
			 
			$child = $nodes[ $i ];
			$this->addSitemapChild( $child, 1.0 );
			
			$i++;
			
		}
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau noeud
	 * @param array $child un tableau associatif
	 * @param $priority la valeur de l'attribut 'priority' dans le sitemap
	 * @return void
	 */
	private function addSitemapChild( array &$child, $priority ){
		
		include_once( "$GLOBAL_START_PATH/objects/DBUtil.php" );
		
		$qr1 = "SELECT available
					FROM category
					WHERE idcategory =". $child[ "id" ]."
					";
			
		$rs1 =& DBUtil::query( $qr1 );
		
		$available = $rs1->fields("available");
		
		if( $child[ "isCategory" ] && $available == 1 ){
		 	
			
		 	//create category node
		 	$blogCategory =array();
			$blackListCategory = array();
		 	$query = "SELECT tr.idproduct, p.idcategory
					FROM trade_product tr, product p
					WHERE p.idproduct = tr.idproduct
					";
			
			$rs =& DBUtil::query( $query );
			
			while( !$rs->EOF )
			{
				$idcategory = $rs->fields("idcategory");
				if(!in_array($idcategory , $blogCategory))
					array_push($blogCategory , $idcategory);
						

				$rs->MoveNext();

			}
			
			
			foreach($this->blackLists[ "categories" ] as $black_list)
			{
				
			
				if(!in_array($black_list , $blogCategory))
					array_push($blackListCategory , $black_list);
			
				
			}
			
			//if( !in_array( $child[ "id" ], $this->blackLists[ "categories" ] ) ){
		 		
			if( !in_array( $child[ "id" ], $blackListCategory) )
			{
		 		
		 		$this->addCategoryChild( $child, $priority );
		 		
		 		//create children nodes
		 		
		 		$i = 0;
		 		while( $i < count( $child[ "children" ] ) ){
		 			
		 			$this->addSitemapChild( $child[ "children" ][ $i ], $priority - 0.1 );
		 			
		 			$i++;
		 			
		 		}
		 		
		 	}
		 	
		 }
		 else{
		 	
		 	//create product node
		 	
		 	if( !in_array( $child[ "id" ], $this->blackLists[ "products" ] ) )
		 		$this->addProductChild( $child, $priority );
		 	
		 	//create pdf node
		 	
		 	if( $this->indexPDF  && !in_array( $child[ "id" ], $this->blackLists[ "pdf" ] ) )
		 		$this->addPDFChild( $child, $priority );
		 	
		 }
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau noeud catégorie
	 * @param array $child un tableau associatif
	 * @param $priority la valeur de l'attribut 'priority' dans le sitemap
	 * @return void
	 */
	private function addCategoryChild( array &$child, $priority ){
			
	 	$uri = "/";
	 
		if( $this->rewriteURLs && !empty( $this->URLRewritingCategoryPattern ) ){
			
			$name = URLFactory::rewrite( $child[ "name" ] );
			$id = $child[ "id" ];
			
			$tmp = str_replace( "%id", $id, $this->URLRewritingCategoryPattern );
			$tmp = str_replace( "%name", $name, $tmp );
			
			$uri .=  $tmp;
	
		}
		else $uri .= "catalog/product.php?IdCategory=" . $child[ "id" ];
	
		$category = new GoogleMapNode( $child[ "name" ], $uri, $this );
		$category->setGetLastModificationMethod( $this->getLastModificationMethod );
	 	$category->setPriority( $priority );
	 	$category->setChangeFrequency( $this->categoryChangeFrequency );
	 	
	 	$this->children[] = $category;
	 	
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau noeud pour un produit
	 * @param array $child un tableau associatif
	 * @param $priority la valeur de l'attribut 'priority' dans le sitemap
	 * @return void
	 */
	private function addProductChild( array &$child, $priority ){
				
	 	$uri = "/";
	 	
		if( $this->rewriteURLs && !empty( $this->URLRewritingProductPattern ) ){
			
			// $name = URLFactory::rewrite( $child[ "name" ] );
			// $id = $child[ "id" ];
			
			// $tmp = str_replace( "%id", $id, $this->URLRewritingProductPattern );
			// $tmp = str_replace( "%name", $name, $tmp );
			$tmpp = URLFactory::getProductURL($child[ "id" ]);
			$host_url = URLFactory::getHostURL();
			$tmp = str_replace($host_url.'/', '', $tmpp);
			
			$uri .=  $tmp;
			
		}	
		else $uri .= "catalog/product_one.php?IdProduct=" . $child[ "id" ];
	
		$product = new GoogleMapNode( $child[ "name" ], $uri, $this );
		$product->setGetLastModificationMethod( $this->getLastModificationMethod );
	 	$product->setPriority( $priority );
	 	$product->setChangeFrequency( $this->productChangeFrequency );
	 	
	 	$this->children[] = $product;
	 	
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau noeud PDF
	 * @param array $child un tableau associatif
	 * @param $priority la valeur de l'attribut 'priority' dans le sitemap
	 * @return void
	 */
	function addPDFChild( array &$child, $priority ){			
					
	 	$uri = "/";
	 	
		if( $this->rewriteURLs && !empty( $this->URLRewritingPDFPattern ) ){
			
			$name = URLFactory::rewrite( $child[ "name" ] );
			$id = $child[ "id" ];
			
			$tmp = str_replace( "%id", $id, $this->URLRewritingPDFPattern );
			$tmp = str_replace( "%name", $name, $tmp );
			
			$uri .=  $tmp;
			
		}	
		else $uri .= "catalog/pdf_product.php?idproduct=" . $child[ "id" ];
	
		$pdf = new GoogleMapNode( "[PDF]" . $child[ "name" ], $uri, $this );
		$pdf->setGetLastModificationMethod( $this->getLastModificationMethod );
	 	$pdf->setPriority( $priority );
	 	$pdf->setChangeFrequency( $this->pdfChangeFrequency );
	 	
	 	$this->children[] = $pdf;
	 	
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute une page statique à indexer
	 * @param string $name le nom de la page
	 * @param string $uri l'URI de la page statique par rapport à la racine du site
	 * @return void
	 */
	public function addStaticPage( $uri ){
		
		$page = new GoogleMapNode( "static", $uri, $this );
		$page->setGetLastModificationMethod( $this->getLastModificationMethod );
		$page->setPriority( 1.0 );
	 	$page->setChangeFrequency( $this->staticPageChangeFrequency );	
		
		$this->staticPages[] = $page;
		 
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Supprime toutes les pages statiques
	 * @return void
	 */
	public function clearStaticPages(){
		
		$this->staticPages = array();
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Supprime toutes les pages blacklistées
	 * @return void
	 */
	public function clearBlackLists(){
		
		$this->blackLists = array( 
		
			"categories" 	=> array(),
			"products" 		=> array(),
			"pdf" 			=> array()
			
		);
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Sauvegarde le sitemap
	 * @return void
	 */
	private function saveToXMLFile(){
		
		global $GLOBAL_START_PATH;
		
		
		$fp = fopen( $GLOBAL_START_PATH . "/sitemap.xml", "w+" );
  
	
		if( !is_resource( $fp ) )
			die( "Impossible de créer le fichier XML" );
			
		$this->lastCreationDate = date( "Y-m-d H:i:s" );
			
		$xmlDocument = $this->getXMLDocument();
			
		fwrite( $fp, $xmlDocument, strlen( $xmlDocument ) );
		
		fclose( $fp );
		
		@chmod( $GLOBAL_START_PATH . $this->outputfile, 0755 );
		
	}

	//-------------------------------------------------------------------------------------------
	
	/**
	 * @private
	 * @return DOMDocument
	 */
	private function getXMLConfig(){
		
		
		$document = new DomDocument( "1.0", "utf-8" );
		
		$config = $document->createElement(  Util::doNothing( "config" ) );
		
		//output_file

		$outputFile = $document->createElement( Util::doNothing( "output_file" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->outputfile ) ) );
		$outputFile->appendChild( $value );
		$config->appendChild( $outputFile );
		
		//get_last_modification_method
		
		$getLastModificationMethod = $document->createElement( Util::doNothing( "get_last_modification_method" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->getLastModificationMethod ) ) );
		$getLastModificationMethod->appendChild( $value );
		$config->appendChild( $getLastModificationMethod );
		
		//category change frequency
		
		$categoryChangeFrequency = $document->createElement( Util::doNothing( "category_change_frequency" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->categoryChangeFrequency ) ) );
		$categoryChangeFrequency->appendChild( $value );
		$config->appendChild( $categoryChangeFrequency );
		
		//product change frequency
		
		$productChangeFrequency = $document->createElement( Util::doNothing( "product_change_frequency" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->productChangeFrequency ) ) );
		$productChangeFrequency->appendChild( $value );
		$config->appendChild( $productChangeFrequency );
		
		//PDF change frequency
		
		$pdfChangeFrequency = $document->createElement( Util::doNothing( "pdf_change_frequency" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->pdfChangeFrequency ) ) );
		$pdfChangeFrequency->appendChild( $value );
		$config->appendChild( $pdfChangeFrequency );
		
		//static page change frequency
		
		$staticPageChangeFrequency = $document->createElement( Util::doNothing( "static_page_change_frequency" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->staticPageChangeFrequency ) ) );
		$staticPageChangeFrequency->appendChild( $value );
		$config->appendChild( $staticPageChangeFrequency );
		
		//indexPDF
		
		$indexPDF = $document->createElement( Util::doNothing( "index_pdf" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->indexPDF ? "1" : "0" ) ) );
		$indexPDF->appendChild( $value );
		$config->appendChild( $indexPDF );
		
		//staticPages
		
		$staticPages = $document->createElement( Util::doNothing( "static_pages" ) );
		
		foreach( $this->staticPages as $googleMapNode ){
			
			$staticPage = $document->createElement( Util::doNothing( "uri" ) );
			$value = $document->createAttribute( Util::doNothing( "value" ) );
			$value->appendChild( $document->createTextNode( Util::doNothing( $googleMapNode->getURI() ) ) );
			$staticPage->appendChild( $value );
			$staticPages->appendChild( $staticPage );
			
		}
		
		$config->appendChild( $staticPages );
		
		//blackLists
		
		$blackLists = $document->createElement( Util::doNothing( "black_lists" ) );
		
		//catégories
		
		$categoryBlackList = $document->createElement( Util::doNothing( "categories" ) );
		
		foreach( $this->blackLists[ "categories" ] as $idcategory ){
			
			$category = $document->createElement( Util::doNothing( "category" ) );
			$id = $document->createAttribute( Util::doNothing( "idcategory" ) );
			$id->appendChild( $document->createTextNode( Util::doNothing( $idcategory ) ) );
			$category->appendChild( $id );
			$categoryBlackList->appendChild( $category );
		
		}
		
		$blackLists->appendChild( $categoryBlackList );
		
		//produits
		
		$productBlackList = $document->createElement( Util::doNothing( "products" ) );
		
		foreach( $this->blackLists[ "products" ] as $idproduct ){
			
			$product = $document->createElement( Util::doNothing( "product" ) );
			$id = $document->createAttribute( Util::doNothing( "idproduct" ) );
			$id->appendChild( $document->createTextNode( Util::doNothing( $idproduct ) ) );
			$product->appendChild( $id );
			$productBlackList->appendChild( $product );
		
		}
		
		$blackLists->appendChild( $productBlackList );
		
		//pdf
		
		$pdfBlackList = $document->createElement( Util::doNothing( "pdf" ) );
		
		foreach( $this->blackLists[ "pdf" ] as $idproduct ){
			
			$product = $document->createElement( Util::doNothing( "product" ) );
			$id = $document->createAttribute( Util::doNothing( "idproduct" ) );
			$id->appendChild( $document->createTextNode( Util::doNothing( $idproduct ) ) );
			$product->appendChild( $id );
			$pdfBlackList->appendChild( $product );
		
		}
		
		$blackLists->appendChild( $pdfBlackList );
		
		$config->appendChild( $blackLists );
		
		//lastCreationDate
		
		$lastCreationDate = $document->createElement( Util::doNothing( "last_creation_date" ) );
		$value = $document->createAttribute( Util::doNothing( "value" ) );
		$value->appendChild( $document->createTextNode( Util::doNothing( $this->lastCreationDate ) ) );
		$lastCreationDate->appendChild( $value );
		$config->appendChild( $lastCreationDate );
		
		//root
		
		$document->appendChild( $config );
		
		return $document->saveXML();
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	private function readConfig(){
		
		if( !file_exists( $this->configPath ) )
			return;
			
		$document= new DOMDocument();
		
		if( !$document->load( $this->configPath ) )
			trigger_error( "Impossible de lire le fichier de configuration du sitemap" );

		//outputfile
		
		$this->outputfile = $document->getElementsByTagName( "output_file" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;

		//get_last_modification_method
		
		$this->getLastModificationMethod = $document->getElementsByTagName( "get_last_modification_method" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
		
		//category_change_frequency
		
		$this->categoryChangeFrequency = $document->getElementsByTagName( "category_change_frequency" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
		
		//product_change_frequency
		
		$this->productChangeFrequency = $document->getElementsByTagName( "product_change_frequency" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
		
		//pdf_change_frequency
		
		$this->pdfChangeFrequency = $document->getElementsByTagName( "pdf_change_frequency" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
		
		//static_page_change_frequency
		
		$this->staticPageChangeFrequency = $document->getElementsByTagName( "static_page_change_frequency" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
		
		//indexPDF
		
		$this->indexPDF = intval( $document->getElementsByTagName( "index_pdf" )->item(0)->attributes->getNamedItem( "value" )->nodeValue ) == 1;
		
		//staticPages
		
		$this->staticPages = array();
		
		$staticPages = $document->getElementsByTagName( "static_pages" )->item(0);
		if( $staticPages->hasChildNodes() ){
			
			$i = 0;
			while( $i < $staticPages->childNodes->length ){

				$this->addStaticPage( $staticPages->childNodes->item( $i )->attributes->getNamedItem( "value" )->nodeValue );
				
				$i++;
				
			}
			
		}
		
		//blackLists
		
		$this->clearBlackLists();
		
		$blackLists = $document->getElementsByTagName( "black_lists" )->item(0);
		
		$i = 0;
		while( $i < $blackLists->childNodes->length ){
			
			$blackList 		= $blackLists->childNodes->item( $i );
			$blackListName 	= $blackList->nodeName;
			
			if( $blackList->hasChildNodes() ){
				
				$j = 0;
				while( $j < $blackList->childNodes->length ){

					switch( $blackListName ){
					
						case "categories" : 
						
							$this->blackListCategory( $blackList->childNodes->item( $j )->getAttributeNode( "idcategory" )->value );
							break;
							
						case "products" : 
						
							$this->blackListProduct( $blackList->childNodes->item( $j )->getAttributeNode( "idproduct" )->value ); 
							break;
							
						case "pdf" : 
						
							$this->blackListPDF( $blackList->childNodes->item( $j )->getAttributeNode( "idproduct" )->value ); 
							break;
							
					}
					
					$j++;
					
				}
	
			}
			
			$i++;
			
		}
		
		//lastCreationDate
		
		$this->lastCreationDate = $document->getElementsByTagName( "last_creation_date" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	public function saveConfig(){

		$fp = @fopen( $this->configPath, "w+" );
		
		if( !is_resource( $fp ) )
			trigger_error( "Impossible de sauvegarder la configuration du sitemap" );
			
		fwrite( $fp, $this->getXMLConfig() );
		
		fclose( $fp );
		
		@chmod( $this->configPath, 0755 );
		
	}
	
	//-------------------------------------------------------------------------------------------
	
 }
 
?>