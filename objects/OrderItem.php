<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object lignes commandes clients
 */

include_once( dirname( __FILE__ ) . "/DBSaleItem.php" );


	
class OrderItem extends DBSaleItem{

	//----------------------------------------------------------------------------
	/**
	 * Crée une nouvelle ligne article pour la commande client
	 * @param Order $order la commande client
	 * @param int $idrow le numéro de la ligne article ( doit commencer à 1 pour les ERP )
	 */
	public function __construct( Order $order, $idrow ){

		DBObject::__construct( "order_row", false, "idorder", $order->getId(), "idrow", intval( $idrow ) );

		$this->sale =& $order;

	}
	
	//----------------------------------------------------------------------------
	
	public function duplicate( Order &$order, $idrow ){
		
		$clone = clone $this;
		
		$clone->recordSet[ "idrow" ] = $idrow;
		
		$clone->sale =& $order;
		$clone->recordSet[ "idorder" ] = $order->getId();
		
		return $clone;
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>