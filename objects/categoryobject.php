<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object produits
 */

include_once( dirname( __FILE__ ) . "/catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );


class CATEGORY extends CATALBASE
{
	protected $childs;
	protected $path ;
	protected $categoryinfo;
	protected $NBProduct;
	protected $idmasksearch;
	protected $DEBUG;

/**
 * @deprecated préférer CCategory
 */
function __construct($IdCategory=0)
{
	die( "deprecated usage" );
}

	/**
	 * Retourne le nom de la catégorie
	 * @return string le nom de la catégorie
	 */
	public function getname() { return $this->categoryinfo['name']; }
	
	/**
	 * @deprecated
	 * Retourne le nom de l'image de la catégorie
	 * @return string le nom de l'image de la catégorie
	 */
	public function getimage() { return ""; }
	
	/**
	 * Retourne le nom de l'image de la catégorie pour le menu
	 * @return string le nom de l'image menu de la catégorie
	 */
	public function getimage_menu() { return $this->categoryinfo['image_menu']; }
	
	
	/**
	 * Retourne le nom de l'image de la catégorie pour le menu roll
	 * @return string le nom de l'image roll menu de la catégorie
	 */
	public function getimage_menu_roll() { return $this->categoryinfo['image_menu_roll']; }
	
	
	
	/**
	 * Retourne les mots clé de la catégorie
	 * @return string les mots clé de la catégorie
	 */
	public function getreferencing() { return $this->categoryinfo['referencing']; }
	
	/**
	 * Retourne le texte de référencement de la catégorie
	 * @return string les mots clé de la catégorie
	 */
	public function getreferencing_text() { return $this->categoryinfo['referencing_text']; }			
	
	/**
	 * Retourne le nombre de références pour la catégorie
	 * @return int le nombre de références
	 */
	public function getqty_ref() { return $this->categoryinfo['qty_reference']; }
	
	/**
	 * Retourne le code de la catégorie
	 * @return string le code de la catégorie
	 */
	function getcode_cat() { return $this->categoryinfo['code_cat']; }
	
	/**
	 * Retourne le texte complémentaire de la catégorie
	 * @return string le texte complémentaire
	 */
	public function getinfo_plus() { return $this->categoryinfo['info_plus']; }
	
	/**
	 * Retourne les sous catégories
	 * @return array les sous catégories
	 */	
	public function getSubCategories() { return array_slice( $this->childs,$this->LimiteMin,$this->LimiteMax); }
	
	/**
	 * Retourne le nombre de sous catégories
	 * @return int le nombre de sous catégories
	 */
	public function GetNbSubCategory() { return $this->NBCategory; }
	
	/**
	 * Retourne l'id du masque produit
	 * @return int l'id du masque
	 */
	public function Getidmaskproduct() { if( empty($this->categoryinfo['idmaskproduct']) ) return '_0';  return '_'.$this->categoryinfo['idmaskproduct']; }
	
	/**
	 * Retourne l'id du masque detail
	 * @return int l'id du masque
	 */
	public function Getidmaskdetail() { if( empty($this->categoryinfo['idmaskdetail']) ) return '_0';  return '_'.$this->categoryinfo['idmaskdetail']; }
	
	/**
	 * Retourne l'id du masque categorie
	 * @return int l'id du masque catégorie
	 */
	public function Getidmask() { if( empty($this->categoryinfo['idmask']) ) return '';  return  $this->categoryinfo['idmask']; }
	
	/**
	 * Retourne l'id du masque de recherche
	 * @return int l'id du masque recherche
	 */
	public function GetIdMaskSearch() { return $this->idmasksearch; }
	
	/**
	 * Retourne la sélection du moteur de recherche
	 * @return int l'id sélectionné
	 */
	public function GetSearch() { return  $this->categoryinfo['search']; }
	
	/**
	 * Retourne le nombre de catégories à afficher par page
	 * @return int le nombre de catégories
	 */
	public function GetNbCategoriesByPages() { return $this->categoryinfo['nb_categories_by_pages']; }
	
	/**
	 * Retourne le nombre de colonnes à afficher
	 * @return int le nombre de colonnes
	 */
	public function GetNbColumnCategorie() { if( !isset( $this->categoryinfo ) || !isset( $this->categoryinfo["nb_column_categories"]) ) return 2; return $this->categoryinfo['nb_column_categories']; }
	
	/**
	 * Retourne le chemin vers la catégorie
	 * @return string le chemin
	 */
	public function getPath(){ return $this->path; }
	
	/**
	 * Retourne la charte de la catégorie
	 * @return string la charte
	 */
	public function GetCharteCategory() { 
		if( empty($this->categoryinfo['idcharte']) ) $Result= '_default'; 
		else  $Result = '_'.$this->categoryinfo['idcharte']; 
		return $Result;
		}
	
	/**
	 * Retourne l'adresse de la page spécifique
	 * @return string l'adresse de la page spécifique
	 */
	public function GetHTML() { return $this->categoryinfo['html']; }
	
	/**
	 * Retourne le paramètre d'affichage de la liste des catégories
	 * @return int 1 pour oui, 0 pour non
	 */
	public function GetList() { if( isset($this->categoryinfo['list']) ) return $this->categoryinfo['list']; }
	
	/**
	 * Affiche le chemin vers la catégorie
	 * @return void
	 */
	public function AffPathCat() {
		echo $this->path;
		}
		
	/**
	 * Retourne le chemin vers la cégorie
	 * @return string le chemin
	 */
	public function GetAffPathCat() {

		return $this->path;
		
		}
		
	/**
	 * Recherche le chemin vers la catégorie
	 * @return strin le chemin
	 */
	public function GetPathCat()
	{	
		
		global $GLOBAL_START_URL;
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
		$Base = &DBUtil::getConnection();
		
		$IdCategory = $this->id ;
		$Langue = $this->lang;
		$Path = '';
		while( $IdCategory > 0 )
		{
			$rs =& DBUtil::query("SELECT idcategoryparent,name$Langue FROM category_link,category  WHERE idcategorychild='$IdCategory' and idcategorychild=idcategory;");
			if( $rs->RecordCount() ) {
				
				$name= URLFactory::rewrite( $rs->Fields( "name$Langue" ) );
				
				$href = URLFactory::getCategoryURL( $IdCategory, $name );

				if( empty($Path) ) {
					
					// dernier niveau
					$Path = "<a href=\"$href\" class=\"category\">" . htmlentities( $rs->Fields("name$Langue") ) . "</a>";
				
				}
				else $Path =  "<a href=\"$href\">" . htmlentities( $rs->Fields("name$Langue") ) . "</a>&nbsp;&gt;&nbsp;" . $Path ; 

				// Au suivant ...
				$IdCategory = $rs->Fields('idcategoryparent');
				
			}
			else $IdCategory = 0;
		}
		/* pour afficher la racine utiliser les deux lignes suivantes
		$rs=& DBUtil::query("SELECT name$Langue FROM category WHERE idcategory=0");
		if($rs->RecordCount()) $Path =  $rs->Fields("name$Langue") .'>' .$Path ; 
		*/
		$this->path = $Path; // &substr($Path,0,-4); 
		$_SESSION['categorypath'] = &$this->path ;
		return $this->path;
	}

	/**
	 * Recherche la catégorie parente la plus vieille (juste dessous la racine)
	 * @return string Plus vieil ancêtre de la catégorie après la racine
	 */
	public function GetParentCat()
	{	
		$Base = &DBUtil::getConnection();
		$IdCategory = $this->id ;
		$Langue = $this->lang;
		$Parent = null;
		while( $IdCategory > 0 )
		{
			$rs =& DBUtil::query("SELECT `idcategoryparent`, `name$Langue` FROM `category_link`, `category`  WHERE `idcategorychild` = '$IdCategory' AND `idcategorychild` = `idcategory`;");
			if( $rs->RecordCount() ) {
				// On écrase le parent par son ancêtre jusqu'à la racine excluse
				$Parent =  $rs->Fields("name$Langue"); 
				// Au suivant ...
				$IdCategory = $rs->Fields('idcategoryparent');
			}
			else $IdCategory = 0;
		}
		return $Parent;
	}
	
	/**
	 * Charge la catégorie
	 * @return void
	 */
	public function SetCategory()
	{
		$Langue = $this->lang;
		$_SESSION['category'] = $this->id ;
		$Base = &DBUtil::getConnection();
		$query  = 'SELECT '.$this->fields.' FROM `category`';
		$query .= " WHERE `idcategory` = '".$this->id."'";
		$rs =& DBUtil::query($query);
		//trigger_error(showDebug($query,'CATEGORY::SetCategory() => $SQL_select_1','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($query,'CATEGORY::SetCategory() => $SQL_select_1','log'),E_USER_ERROR);
		elseif($rs->RecordCount()>0) {
			$this->categoryinfo = $rs->fields ;
		}
		else $this->categoryinfo = array();
		$_SESSION['categoryinfo'] = &$this->categoryinfo ;

		$this->childs = array();
		$query = "
		SELECT {$this->fields} 
		FROM `category`, `category_link`
		WHERE `idcategorychild` = `idcategory`
		AND `category`.`available` = 1
		AND `idcategoryparent` = '{$this->id}'";		
			
		$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$query .= " AND `category`.`catalog_right` <= '$catalog_right'";
		$query .= " ORDER BY displayorder, name$Langue";
		$rs =& DBUtil::query($query);
		//trigger_error(showDebug($query,'CATEGORY::SetCategory() => $SQL_select_2','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($query,'CATEGORY::SetCategory() => $SQL_select_2','log'),E_USER_ERROR);
		elseif($rs->RecordCount()>0) {
			$this->NBCategory = $rs->RecordCount();
			for($j=0 ; $j < $this->NBCategory ; $j++)
			{
				$this->childs[$j] = $rs->fields ;
				$rs->MoveNext();
			}
			$_SESSION['categories'] = &$this->childs ;	
		}
		else $this->NBCategory = 0;
	}

	/**
	 * Charge les produits
	 * @return void
	 */
	public function SetProducts( $query )
	{
		$Langue = $this->lang;
		$IdCategory = $this->id ;
		$Base = &DBUtil::getConnection();

		$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$query .= " AND `catalog_right` <= '$catalog_right'";
		$query .= " ORDER by display_product_order ASC";
		$rs =& DBUtil::query($query);
		//trigger_error(showDebug($query,'CATEGORY::SetProducts() => SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($query,'CATEGORY::SetProducts() => SQL_select','log'),E_USER_ERROR);
		if($rs->RecordCount()>0) {
			$this->NBProduct = $rs->RecordCount();
			for($j=0 ; $j < $this->NBProduct ; $j++)
			{
				$this->products[] = $rs->fields('idproduct') ;
				$this->productnames[] = $rs->fields('name_1') ;
				$rs->MoveNext();
			}
		}
		else 
		{
			$this->products = array();
			$this->productnames = array();
			$this->NBProduct = 0;
		}
		
		$_SESSION['products'] = &$this->products ;
		$_SESSION['productnames'] = &$this->productnames ;
	}

	
	/**
	 * Retourne la position du produit
	 * @param int $id le produit
	 * @return int la position du produit 
	 */
	private function GetProductPos($id)
	{
		$n = 0;
		foreach($this->products as $i) {
		if($i == $id) return $n;
		$n++;
		}
	        return 0;
	}

	/**
	 * Retourne l'id du produit à la position $pos
	 * @param int la position du produit
	 * @return in l'id du produit 
	 */
	public function GetProductId($pos)
	{
		return $this->products[$pos];
	}
			
	/**
	 * Retourne le nombre de produits 
	 * @todo ne tient pas compte de la disponibilité des références ( nombre de ref. + stock )
	 * @return int le nombre de produits de la catégorie
	 */
	public function GetNbProductsInCategory()
	{
		$n = 0;
		$Con = &DBUtil::getConnection();
					
		$andStockLevel = " AND d.stock_level <> 0 ";
		
		$ANDLINES = "";
		$FROMLINE = "";
		
		
		if(isset($_REQUEST["IdTrade"]) && $_REQUEST["IdTrade"]!=""){
			$id=$_REQUEST["IdTrade"];
			$FROMLINE.=" ,trade_product pt";
			$ANDLINES.=" AND pt.idproduct=p.idproduct
						AND pt.idtrade=$id";
						
			$ParameterOutputArray["IdTrade"] = $id;
		}
		
		if(isset($_REQUEST["IdBrand"]) && $_REQUEST["IdBrand"]!=""){
			$id=$_REQUEST["IdBrand"];
			$ANDLINES.=" AND p.idbrand=$id ";

			$ParameterOutputArray["IdBrand"] = $id;
		}
				
		if(isset($_REQUEST["code"]) && $_REQUEST["code"]!=""){
			$cod=$_REQUEST["code"];
			$ANDLINES.=" AND code=$cod ";
		}

		// TRUC DE MERDE POUR LES INTITULES //
		if((isset($_REQUEST["Size"]) && $_REQUEST["Size"]!="") || (isset($_REQUEST["Color"]) && $_REQUEST["Color"]!="") || (isset($_REQUEST["Field"]) && $_REQUEST["Field"]!="")){
			$FROMLINE.=" , intitule_link il ";
			$ANDLINES.=" AND d.idarticle=il.idarticle ";
		}
		
		if(isset($_REQUEST["Size"]) && $_REQUEST["Size"]!=""){
			$size=$_REQUEST["Size"];
			$rsintit=DBUtil::query("SELECT idintitule_value as idintitule FROM intitule_value WHERE intitule_value_1='$size' LIMIT 1");
			$idintitule_value=$rsintit->fields("idintitule");
			$ANDLINES.=" AND il.idintitule_value=$idintitule_value";
		}
		
		if(isset($_REQUEST["Color"]) && $_REQUEST["Color"]!=""){
			$size=$_REQUEST["Color"];
			$rsintit=DBUtil::query("SELECT idintitule_value as idintitule FROM intitule_value WHERE intitule_value_1='$size' LIMIT 1");
			$idintitule_value=$rsintit->fields("idintitule");
			$ANDLINES.=" AND il.idintitule_value=$idintitule_value";
		}
		
		if(isset($_REQUEST["Field"]) && $_REQUEST["Field"]!=""){
			$size=$_REQUEST["Field"];
			$rsintit=DBUtil::query("SELECT idintitule_value as idintitule FROM intitule_value WHERE intitule_value_1 LIKE '$size' LIMIT 1");
			$idintitule_value=$rsintit->fields("idintitule");
			$ANDLINES.=" AND il.idintitule_value=$idintitule_value";
		}
		
		
		
		$query = "	SELECT p.idproduct as nbcat 
					FROM product p, detail d $FROMLINE
					WHERE p.idcategory ='".$this->id."'";
		
		if(DBUtil::getParameter("use_not_available_products")){
			$andStockLevel = "";
		}
								

		$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$query .= " AND p.idproduct=d.idproduct 
					AND p.catalog_right <= '$catalog_right' 
					AND p.available=1 
					$andStockLevel 
					$ANDLINES 
					GROUP BY p.idproduct";

		$rs = $Con->Execute($query) ;
		
		if( empty($rs) ) trigger_error(showDebug($query,'CATEGORY::GetNbProductsInCategory() => SQL_select','log'),E_USER_ERROR);
		
		if($rs) $n = $rs->RecordCount();
		return $n;
	}
	
	/**
	 * Retourne la description de la catégorie
	 * @return string la description
	 */
	public function GetDesc(){
			
		$lang = "_1";
		$query = "SELECT description$lang AS catdesc FROM category WHERE idcategory = {$this->id}";
		$rs = DBUtil::query( $query );
	
		if( !$rs->RecordCount() )
			return "";
			
		return $rs->Fields( "catdesc" );
		
	}
	
	/**
	 * Retourne la description plus de la catégorie
	 * @return string la description plus de la catégorie
	 */
	public function GetDescPlus(){
			
		$lang = "_1";
		$query = "SELECT description_plus$lang AS catdesc FROM category WHERE idcategory = {$this->id}";
		$rs = DBUtil::query( $query );
	
		if( !$rs->RecordCount() )
			return "";
			
		return $rs->Fields( "catdesc" );
		
	}


	
	/**
	 * Retourne la description de la liste de produits de la catégorie
	 * @return string la description
	 */
	public function GetDescProductList(){
				
		$lang = "_1";
		$query = "SELECT text_product_list1$lang AS catdesc FROM category WHERE idcategory = {$this->id}";
		$rs = DBUtil::query( $query );
	
		if( !$rs->RecordCount() )
			return "";
			
		return $rs->Fields( "catdesc" );
		
	}
	
	/**
	 * Retourne la description plus de la liste de produits de la catégorie
	 * @return string la description
	 */
	public function GetDescProductListPlus(){
				
		$lang = "_1";
		$query = "SELECT text_product_list2$lang AS catdesc FROM category WHERE idcategory = {$this->id}";
		$rs = DBUtil::query( $query );
	
		if( !$rs->RecordCount() )
			return "";
			
		return $rs->Fields( "catdesc" );
		
	}
	
		
	/**
	 * Retourne l'actualité de la catégorie
	 * @return string le texte d'actualité
	 */
	public function GetActu(){
				
		$lang = "_1";
		$query = "SELECT actuality$lang AS catdesc FROM category WHERE idcategory = {$this->id}";
		$rs = DBUtil::query( $query );
	
		if( !$rs->RecordCount() )
			return "";
			
		return $rs->Fields( "catdesc" );
		
	}

	/**
	 * Charge le masque de recherche
	 * @return void
	 */
	private function SetIdMaskSearch(){
	
		$query = "SELECT idmasksearch FROM category WHERE idcategory = '" .$this->id."'";
		$rs = DBUtil::query( $query );
		
		if( $rs->RecordCount() ) 
			$this->idmasksearch = $rs->fields( "idmasksearch" );
		else $this->idmasksearch = 0;
		
	}

	//------------------------------------------------------------------------
	
	/**
	 * Retourne la catégorie parente
	 * @return int l'id de la catégorie parente
	 */
	 
	public function getParent(){
	
		$query = "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '{$this->id}' LIMIT 1";
				
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la catégorie parente" );
			
		if( !$rs->RecordCount() )
			return false;
			
		return $rs->fields( "idcategoryparent" );
		
	}
	
	//------------------------------------------------------------------------
	
	/**
	 * Retourne true si la catégorie est disponible à l'affichage dans le front office
	 * Dépend :
	 * 	- des droits de consultation de la catégorie et des catégories parentes
	 *  - du masquage de la catégorie ou d'une des catégories parentes
	 *  - du nombre de sous-catégories, produits et de références disponibles
	 *  - du stock disponible
	 * @access public
	 * @static
	 * @return bool
	 */
	public static function isAvailable( $idcategory ){

		/*accueillir tout le monde*/
		
		if( !$idcategory )
			return true;
			
		/*vérifier si la catégorie existe*/
		
		$rs =& DBUtil::query( "SELECT idcategory FROM category WHERE idcategory = '$idcategory' LIMIT 1" );
		
		if( !$rs->RecordCount() )
			return false;
			
		/*masquage de la catégorie*/
		
		if( !DBUtil::getDBValue( "available", "category", "idcategory", $idcategory ) )
			return false;
				
		/*droits de consultation du visiteur*/
		
		$buyerCatalogRight = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$categoryCatalogRight = DBUtil::getDBValue( "catalog_right", "category", "idcategory", $idcategory );

		if( $categoryCatalogRight > $buyerCatalogRight )
			return false;

		/*récupérer toutes les catégories parentes afin d'examiner leur disponibilité*/
	
		$parents = array();
		$done = false;
		$idcategorychild = $idcategory;
		while( !$done ){
			
			$rs =& DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategorychild' LIMIT 1" );
			
			$done = $rs->RecordCount() == 0 || $rs->fields( "idcategoryparent" ) == 0;
			
			if( !$done )
				$parents[] = $idcategorychild = $rs->fields( "idcategoryparent" );
			
		}
		
		/*droits de consultation des catégories parentes*/
		
		foreach( $parents as $idparent ){
			
			if( !DBUtil::getDBValue( "available", "category", "idcategory", $idparent ) )
				return false;
		
		}
		
		/*droits de consultation du visiteur pour les catégories parentes*/
		
		foreach( $parents as $idparent ){
			
			$parentCatalogRight = DBUtil::getDBValue( "catalog_right", "category", "idcategory", $idparent );
			
			if( $parentCatalogRight > $buyerCatalogRight )
				return false;
			
		}

		/*s'il s'agit d'une catégorie avec des sous-catégories : véfier s'il existe au moins une sous-catégorie disponible*/
		
		$rs =& DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '$idcategory'" );
		
		if( $rs->RecordCount() ){
			
			$available = false;
			while( !$available && !$rs->EOF() ){
	
				if( CATEGORY::isAvailable( $rs->fields( "idcategorychild" ) ) ) //si il existe au moins une sous-catégorie disponible
					$available = true;
					
				$rs->MoveNext();
			
			}

			if( !$available ) //aucune des sous-catégories n'est disponible
				return false;
				
		}
		
		/*s'il s'agit d'une catégorie de produtis : véfier s'il existe au moins un produit disponible*/
		
		$rs =& DBUtil::query( "SELECT idproduct FROM product WHERE idcategory = '$idcategory'" );
		
		if( $rs->RecordCount() ){
			
			$available = false;
			while( !$available && !$rs->EOF() ){
	
				if( PRODUCT::isAvailable( $rs->fields( "idproduct" ) ) ) //si il existe au moins un produit disponible
					$available = true;
					
				$rs->MoveNext();
			
			}

			if( !$available ) //aucun des produit n'est disponible
				return false;
				
		}
		
		/*
		/*pas d'objection votre honneur*/
		
		return true;
			
	}
	
	//------------------------------------------------------------------------
	
	/**
	 * Retourne le nombre de références disponibles à l'affichage dans le front office pour une catégorie donnée
	 * @access public
	 * @static
	 * @param int $idcategory
	 * @return int
	 */
	public static function getAvailableReferenceCount( $idcategory ){

		$buyerCatalogRight = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$categoryCatalogRight = DBUtil::getDBValue( "catalog_right", "category", "idcategory", $idcategory );
		
		if( $categoryCatalogRight > $buyerCatalogRight )
			return 0;

		//sous-catégories
		
		$referenceCount = 0;
		
		$query = "
		SELECT c.idcategory
		FROM category c, category_link cl
		WHERE cl.idcategoryparent = '$idcategory'
		AND cl.idcategorychild = c.idcategory
		AND c.available = 1
		AND c.catalog_right <= '$buyerCatalogRight'";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
			
			$referenceCount += CATEGORY::getAvailableReferenceCount( $rs->fields( "idcategory" ) );
			
			$rs->MoveNext();
			
		}
		
		if( $rs->RecordCount() )
			return $referenceCount;
			
		//produits de la catégorie
		
		$query = "
		SELECT COUNT( d.idarticle ) AS referenceCount
		FROM product p, detail d
		WHERE p.idcategory = '$idcategory'
		AND p.available = 1
		AND p.catalog_right <= '$buyerCatalogRight'
		AND p.idproduct = d.idproduct
		AND d.stock_level <> 0
		AND d.available = 1";
			
		$rs =& DBUtil::query( $query );
		
		return $rs->fields( "referenceCount" );
			
	}
	
	//------------------------------------------------------------------------
	/**
	 * Retourne le prix le moins cher pour une catégorie donnée
	 * */
	public function getProductsLowerPrice( $idcategory ){

		$lowerPrice = 0;
		
		$queryYoupi = "	SELECT p.idproduct
						FROM product p, category c, detail d
						WHERE p.idcategory = c.idcategory
						AND d.idproduct = p.idproduct
						AND p.available = 1
						AND c.available = 1
						AND d.available = 1
						AND d.sellingcost > 0
						AND c.idcategory LIKE '$idcategory%'
						GROUP BY p.idproduct";
						
		$rsYoupi = DBUtil::query($queryYoupi);
		
		if($rsYoupi->RecordCount() && $rsYoupi->RecordCount() > 0){
			
			while(!$rsYoupi->EOF()){
				$idproduct = $rsYoupi->fields( "idproduct" );
				$jaipete = PRODUCT::getLowerPrice($idproduct);
				if(($lowerPrice > $jaipete && $jaipete > 0) || $lowerPrice == 0){
					$lowerPrice = $jaipete ;
				}
				
				$rsYoupi->MoveNext();
			}
			
		}
		
		return $lowerPrice;
			
	}
	
	//------------------------------------------------------------------------
	
	public static function getPromoCount( $idcategory ){
		
		$idcategory = intval( $idcategory );
		
		$query = "
		SELECT COUNT(*) AS promoCount
		FROM promo, detail d, product p
		WHERE p.idcategory = '$idcategory'
		AND p.idproduct = d.idproduct
		AND d.reference = promo.reference
		AND promo.begin_date <= NOW()
		AND promo.end_date >= NOW()";

		$rs =& DBUtil::query( $query );

		if( $rs->fields( "promoCount" ) > 0 )
			return $rs->fields( "promoCount" );
			
		/* sous-catégories */

		$rs =& DBUtil::query( "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '$idcategory'" );
		
		$promoCount = 0;
		while( !$rs->EOF() ){
		
			$promoCount += self::getPromoCount( $rs->fields( "idcategorychild" ) );
			
			$rs->MoveNext();
				
		}
		
		return $promoCount;
		
	}
	
	//------------------------------------------------------------------------

	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
}//EOC
?>