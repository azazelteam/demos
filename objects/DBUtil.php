<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ...?
 */


define( "PERSISTENT_CONNECTION", false );

//include_once( dirname( __FILE__ ) . "/../config/init.php" );
/*
 * @static
 */
 
class DBUtil{
	
	//--------------------------------------------------------------------
	
	private static $con;
	
	//--------------------------------------------------------------------
    
    /*public static function &qstr($string, $magic_quotes=false){
	
		$rs = self::getConnection()->qstr($string, $magic_quotes);
		
		return $rs;
		
	}*/
	
	/**
	 * Retourne une référence vers une connection ADO
	 * @static
	 * @return ADOConnection
	 */
	public static function &getConnection(){
		
		include( dirname( __FILE__ ) . "/../config/config.php" );
		
		if( !isset( self::$con ) ){
			
			self::$con = NewADOConnection( "mysqli", "pear" );
			
			if( PERSISTENT_CONNECTION )
					self::$con->PConnect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS, $GLOBAL_DB_NAME );
			else 	self::$con->Connect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS, $GLOBAL_DB_NAME );
			
		}
		return self::$con;
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Récupère la valeur d'une colonne donnée dans une table donnée pour un critère donné
	 * Exécuté une requête SQL du type : SELECT `$fieldname` FROM `$table` WHERE `$uniqueKey` = '$uniqueKeyValue' LIMIT 1
	 * @param string $fieldname le nom de la colonne à sélectionner
	 * @param string $table la table à sélectionné
	 * @param string $uniqueKey le nom de la clé unique
	 * @param mixed $uniqueKeyValue la valeur de la clé unique
	 * @static
	 * @return string le résultat de la recherche ou false si aucun résultat retourné
	 */
	 
	public static function getDBValue( $fieldname, $table, $uniqueKey, $uniqueKeyValue, $allowEmptyResultset = true ){
		
		$query = "
		SELECT `$fieldname`
		FROM `$table`
		WHERE `$uniqueKey` = " . self::quote( $uniqueKeyValue ) . "
		LIMIT 1";
		
		$rs =& self::query( $query );
		
		if( $rs === false || ( !$rs->RecordCount() && !$allowEmptyResultset ) )
			trigger_error( "Erreur SQL : $query", E_USER_ERROR );

		if( !$rs->RecordCount() )
			return false;
			
		return $rs->fields( $fieldname );
		
	}	
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne le résultat d'une requête SQL donnée
	 * @param string $query la requête SQL à éxécuter
	 * @param bool $allowEmptyResultset autorise ou non l'abscence de résultat ( optionnel, true par défaut )
	 * @param bool $dieOnError interrompt l'éxécution du script PHP en cas d'erreur ou non ( optionnel, true par défaut )
	 * @param string $errorMessage message d'erreur affiché si $dieOnError vaut true et que l'éxécution de la requête produit une erreur
	 * @return mixed une resource ADODB en cas de succès. Si l'éxécution de la requête génère une erreur,
	 * la méthode retourne false $dieOnError vaut false, sinon rien 
	 */
	public static function &query( $query, $allowEmptyResultset = true ){
	
		$rs = self::getConnection()->Execute( $query );
		
		if( $rs === false || ( !$rs->RecordCount() && !$allowEmptyResultset ) )
			trigger_error( "Erreur SQL : $query", E_USER_ERROR );
		
		return $rs;
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne la plus petite valeur de $fieldname disponible ( non utilisée ) dans la table $tableName
	 * @static
	 * @param string $tableName le nom de la table
	 * @param string $fieldname le nom du champ
	 * @return int
	 */
	public static function getUniqueKeyValue( $tablename, $fieldname ){

		$rs =& self::query( "SELECT MAX( `$fieldname` ) + 1 AS `$fieldname` FROM `$tablename`" );
		
		if( $rs === false || !$rs->RecordCount() )
			trigger_error( "Impossible de récupérer une nouvellle valeur pour la clé $keyname dans la table $tablename", E_USER_ERROR );
			
		return $rs->fields( $fieldname ) == null ? 1 : $rs->fields( $fieldname );
		
	}
	
	//--------------------------------------------------------------------
	
	public static function stdclassFromDB( $table, $uniqueKey, $uniqueKeyValue ){
		
		$query = "
		SELECT *
		FROM `$table`
		WHERE `$uniqueKey` =". self::quote( $uniqueKeyValue ) . "
		LIMIT 1";
		
		$rs =& self::query( $query );
		
		if( $rs === false  )
			trigger_error( "Erreur SQL : $query", E_USER_ERROR );
			
		return ( object )$rs->fields;

	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Recherche un paramètre
	 */
	public static function getParameter( $ParamName ){

		$query = "SELECT `paramvalue` FROM `parameter_cat` WHERE `idparameter` = '$ParamName'";
		$rs =& self::query( $query );

		if( $rs === false )
			die( "Impossible de récupérer la valeur du paramètre $ParamName." );

		if( !$rs->RecordCount() )
			return false;

		return $rs->fields( "paramvalue" );

	}
	
	//--------------------------------------------------------------------

	/**
	 * Recherche un paramètre de l'administration
	 */
	public static function getParameterAdmin( $ParamName ){

		$query = "SELECT `paramvalue` FROM `parameter_admin` WHERE `idparameter` = '$ParamName'";
		$rs =& self::query( $query );

		if( $rs === false )
			die( "Impossible de récupérer la valeur du paramètre $ParamName." );

		if( !$rs->RecordCount() )
			return false;

		return $rs->fields( "paramvalue" );

	}

	//--------------------------------------------------------------------

	/**
	 * Retourne la valeur par défaut d'un champ donné d'une table donnée
	 * @static
	 * @param string $tableName le nom de la table
	 * @param string $fieldName le nom du champ
	 * @return string
	 */
	public static function getDefaultValue( $tableName, $fieldName ){
		
		$query = "SELECT default_value FROM desc_field WHERE tablename LIKE " . self::quote( $tableName ) . " AND fieldname LIKE " . self::quote( $fieldName ) . " LIMIT 1";
		
		$rs =& self::query( $query );
		
		if( !$rs->RecordCount() )
			return false;
			
		return $rs->fields( "default_value" );
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne le nombre de lignes affectées par la dernière requête SQL
	 * @return int
	 */
	public static function getAffectedRows(){
		
		return self::getConnection()->Affected_Rows();
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * @return string
	 */
	public static function quote( $string ){
	
		return self::getConnection()->quote( $string );
		
	}
    
    /**
	 * Retourne l'identifiant de la connexion en cours
	 * @return int
	 */
    public static function getConnectionId(){
		
		return self::getConnection()->getConnectionId();
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne la dernière valeur d'autoincrement mise à jour
	 * @return int
	 */
	public static function getInsertID(){
		
		return self::getConnection()->Insert_ID();
		
	}
	
	//--------------------------------------------------------------------
	
}

?>