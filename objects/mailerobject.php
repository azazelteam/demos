<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des mails
 */


class EVM_Mailer {

	private $subject;
	private $Sendername;
	private $Senderaddy;
	private $message;
	private $Recipients;
	private $CC_list;
	private $BCC_list;
	private $attachments; // filename(as attachment name),location(path+filename of attachment),mime-type
	private $ishtml; // 0=text, 1=html
	private $extraheaders;
	private $warnings;
	private $action_msgs;
	private $error_msgs;
	private $DEBUG;
	private $extraparams;
	private $notice;
	
	function  __construct($extraparams=NULL) {
		$this->subject = '';
		$this->Sendername = '';
		$this->Senderaddy = '';
		$this->message = '';
		$this->Recipients = '';
		$this->CC_list = '';
		$this->BCC_list = '';
		$this->attachments = array(); // filename(as attachment name),location(path+filename of attachment),mime-type
		$this->ishtml = 0; // 0=text, 1=html
		$this->extraheaders = array();
		$this->warnings = array();
		$this->action_msgs = array();
		$this->error_msgs = array();
		$this->DEBUG ='none';
		$this->extraparams = NULL;
		$this->notice = false;
		$this->extraparams = $extraparams;
	}
	
	/**
	 * Lit un fichier
	 * @param string $filename Chemin du fichier
	 * @return bool true si la lecture est ok, false sinon
	 */
	private function get_file( $filename){ 
		if ($fp = fopen($filename, 'rb')) { 
			$return = fread($fp, filesize($filename)); 
			fclose($fp); 
			return $return; 
		} else {
			return FALSE;
		}
	}
	
	/**
	 * Ajoute le sujet
	 * @param string $msg Le sujet du message
	 * @return void
	 */
	public function set_subject( $msg) {
		if (strlen($msg) > 255) { // Limit subject line to 255 characters
			$msg = substr($msg,0,255);
			array_push($this->warnings,"Sujet tronqué à 255 caractères.");
		}
		$this->subject = $msg;
		array_push($this->action_msgs,"Sujet mis à : " . $msg);
	}

	/**
	 * Ajoute l'expéditeur
	 * @param string $name Nom de l'expéditeur
	 * @param string $addy Adresse de l'expéditeur
	 * @return void
	 */
	public function set_sender( $name, $addy) {
		if (strlen($addy) > 255) { // Limit sender address to 255 characters
			array_push($this->error_msgs,"Adresse de l'expéditeur trop longue. (255 car. max)");
		} else {
			if (strlen($name) != 0) {
				$this->Sendername = $name;
				array_push($this->action_msgs,"Nom de l'expéditeur : " . $name);
			} else {
				array_push($this->action_msgs,"Nom de l'expéditeur vide");
			}
			if (strlen($addy) == 0) {
				array_push($this->error_msgs,"Adresse de l'expéditeur vide");
			} else { // validate email address here
				//if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $addy)) {
				/*if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*", $addy)) { //JB jean@localhost !!
					array_push($this->error_msgs,"Format d'adresse mail incorrect");
				} else {*/
					array_push($this->action_msgs,"Format d'adresse mail validé : $addy");
					$this->Senderaddy = $addy;
				//}
			}
		}
	}

	/**
	 * Ajoute le corps du message
	 * @param string $msg Le corps du message
	 * @return void
	 */
	public function set_message( $msg) {
		$this->message = $msg;
		array_push($this->action_msgs,'Contenu du message mis en place');
	}

	/**
	 * Ajoute un destinataire
	 * @param string $name Nom du destinataire
	 * @param string $addy Adresse du destinataire
	 * @return void
	 */
	public function add_recipient( $name, $addy) {
		if (strlen($addy) > 255) { // Limit sender address to 255 characters
			array_push($this->error_msgs,"Adresse du destinataire trop longue. (255 car. max) " . $addy);
		} else {
			if (strlen($name) == 0) {
				array_push($this->warnings,'Pas de nom pour ' . $addy);
			} else {
				$build_address = (strlen($name) > 0) ? "\"$name\"" . ' ' : '';
				$build_address .= "<";
				$build_address .= $addy;
				$build_address .= ">";

				if (strlen($this->Recipients) > 0) {
					$this->Recipients .= ", " . $build_address;
					array_push($this->action_msgs,"Adresse ajoutée : " . $build_address);
				} else {
					$this->Recipients = $build_address;
					array_push($this->action_msgs,"Adresse ajoutée : " . $build_address);
				}
			}
			showDebug($this->Recipients,'EVM_Mailer::add_recipient('."'$name','$addy'".') => $this->Recipients',$this->DEBUG);
		}
	}

	/**
	 * Ajoute un entête
	 * @param String $name Nom de l'entête
	 * @param string $addy Valeur de l'entête
	 * @return void
	 */
	public function add_header( $name, $exth) {
		if (strlen($exth) > 255) { // Limit sender address to 255 characters
			array_push($this->warnings,"Entete trop long. (255 car. max) " . $exth);
		} else {
			if (strlen($name) == 0) {
				array_push($this->warnings,"Pas de nom pour l'entete" . $exth);
			} else {
				$build_header = $name .  ": " .  $exth;
				array_push($this->extraheaders,$build_header);
				array_push($this->action_msgs,"Entete '$name' ajouté : $exth");
			}
		}
	}

	/**
	 * Ajoute un destinataire CC (Copie Conforme)
	 * @param string $name Nom du destinataire
	 * @param string $addy Adresse du destinataire
	 * @return void
	 */
	public function add_CC( $name, $addy) {
		if (strlen($addy) > 255) { // Limit sender address to 255 characters
			array_push($this->error_msgs,"CC email adresse trop longue. (255 car. max) " . $addy);
		} else {
			if (strlen($name) == 0) {
				array_push($this->warnings,'Pas de nom CC pour ' . $addy);
			} else {
				$build_address = (strlen($name) == 0) ? $name . ' ' : '';
				$build_address .= "<";
				$build_address .= $addy;
				$build_address .= ">";

				if (count($this->CC_list) > 1) $this->CC_list .= ", " . $build_address;
				else $this->CC_list .= $build_address;
			array_push($this->action_msgs,"CC ajoutée : " . $addy);
			}
		}
	}

	/**
	 * Ajoute un destinataire CC (Copie Confrome Cachéé)
	 * @param string $name Nom du destinataire
	 * @param string $addy Adresse du destinataire
	 * @return void
	 */
	public function add_BCC( $name, $addy) {
		if (strlen($addy) > 255) { // Limit sender address to 255 characters
			array_push($this->error_msgs,"BCC email adresse trop longue. (255 car. max) " . $addy);
		} else {
			if (strlen($name) == 0) {
				array_push($this->warnings,'Pas de nom BCC pour ' . $addy);
			} else {
				$build_address = (strlen($name) == 0) ? $name . ' ' : '';
				$build_address .= "<";
				$build_address .= $addy;
				$build_address .= ">";

				if (count($this->BCC_list) > 1) $this->BCC_list .= ", " . $build_address;
				else $this->BCC_list .= $build_address;
			array_push($this->action_msgs,"BCC ajoutée : " . $addy);
			}
		}
	}

	/**
	 * Attache un fichier au mail
	 * @param string $aname Nom du fichier dans le mail
	 * @param string $location Emplacement du fichier
	 * @param string $mimetype Type de fichier (mime)
	 * @return void
	 */
	public function add_attachment( $aname, $location,$mimetype='') {
		if ($mimetype == '') {
		   $mimetype = 'application/octet-stream';
			array_push($this->warnings,"No mime type for attachment : '$location' - 'application/octet-stream' used");
		}
		if ($aname == '') {
			$aname = md5(uniqid(rand()).microtime());
			array_push($this->warnings,"No attachment name specified for attachment :  '$location' using '$aname'");
		}
		if ($location == '') {
			array_push($this->error_msgs,"No file name specified for attachment '$aname'");
		} else {
			if (!file_exists($location)) {
				array_push($this->error_msgs,"attachment: " . $location . ' does not exist');
			} else {
				if (!is_readable($location)) {
					array_push($this->error_msgs,"attachment: " . $location . ' could not be read');
				} else {
					$x = count($this->attachments);
					$toarray = array(0 => $aname);
					$toarray[1] = $location;
					$toarray[2] = $mimetype;
					$toarray[3] ='file';
					$this->attachments[$x] = &$toarray;
					array_push($this->action_msgs,$location . " mis en pièce jointe");
				}
			}
		}
	}

	/**
	 * Attache un document au mail
	 * @param string $aname Nom du document dans le mail
	 * @param string $data Contenu du document
	 * @return void
	 */
	public function add_pdf(  $aname, &$data ) {
		
		if ($aname == '') {
		
		    $aname = md5(uniqid(rand()).microtime()).'.pdf';
			array_push($this->warnings,"No attachment name specified pdf - using : " . $aname);
			/**/die( "HERE" );
		}
		
		$x = count($this->attachments);

		$this->attachments[$x] = array();
		$this->attachments[$x][ 0 ] = $aname;
		$this->attachments[$x][ 1 ] = $data;
		$this->attachments[$x][ 2 ] = 'application/pdf';
		$this->attachments[$x][ 3 ] ='buffer';
		
		array_push($this->action_msgs,"Document pdf '$aname' mis en pièce jointe");
		
	}
	
	/**
	 * Attache un document au mail
	 * @param string $aname Nom du document dans le mail
	 * @param string $data Contenu du document
	 * @return void
	 */
	public function addAttachment( $filename, &$data, $mimeType ) {
		
		if ($filename == '') {
		
		    $filename = md5(uniqid(rand()).microtime());
			array_push($this->warnings,"No attachment name specified - using : " . $filename);

		}
		
		$x = count($this->attachments);

		$this->attachments[$x] = array();
		$this->attachments[$x][ 0 ] = $filename;
		$this->attachments[$x][ 1 ] = $data;
		$this->attachments[$x][ 2 ] = $mimeType;
		$this->attachments[$x][ 3 ] ='buffer';
		
		array_push($this->action_msgs,"New attachment : $filename");
		
	}
	
	/**
	 * Défini le type de message
	 * @param string $mtype 'text' ou 'html'
	 * @return void
	 */
	public function set_message_type( $mtype) {

		switch ($mtype) {
			case 'text':
				$this->ishtml = 0;
				array_push($this->action_msgs,'Message de type texte');
				break;
			case 'html':
				$this->ishtml = 1;
				array_push($this->action_msgs,'Message au format html');
				break;
			default:
				$this->ishtml = 0;
				array_push($this->action_msgs,'Message de type texte');
				break;
		}
	}

	/**
	 * Demande un accusé de réception
	 * @param bool $notice, true pour l'accusé réception, false sinon
	 * @return void
	 */
	 public function set_use_notification( $notice = true ){
	 	
	 	$this->notice = $notice;
	 }
	 
	/**
	 * Retourne toutes les actions
	 * @return string $returnstring les actions 
	 */
	function get_actions() {
		$returnstring = '';
		if (count($this->action_msgs) > 0) {
			for ($i=0;$i<count($this->action_msgs);$i++) {
				$returnstring .= $this->action_msgs[$i] . "<br />\n";
			}
		}
		return $returnstring;
	}

	/**
	 * Retourne toutes les avertissements
	 * @return string $returnstring les avertissements
	 */
	public function get_warnings() {
		$returnstring = '';
		if (count($this->warnings) > 0) {
			for ($i=0;$i<count($this->warnings);$i++) {
				$returnstring .= $this->warnings[$i] . "<br />\n";
			}
		}
		return $returnstring;
	}

	/**
	 * Retourne toutes les erreurs
	 * @return string $returnstring les erreurs
	 */
	public function get_errors() {
		$returnstring = '';
		if (count($this->error_msgs) > 0) {
			for ($i=0;$i<count($this->error_msgs);$i++) {
				$returnstring .= $this->error_msgs[$i] . "<br />\n";
			}
		}
		return $returnstring;
	}

	/**
	 * Renvoie les erreurs lors de l'envoi
	 * @return int le nombre d'erreurs
	 */
	public function is_errors() { return count($this->error_msgs); }
	
	/**
	 * Envoi du message
	 * @return bool, true si le mail est envoyé, false sinon
	 */ 
	public function send() {
		if (count($this->error_msgs) == 0) 
		{
			$boundary = '=_'.md5(uniqid(rand()).microtime()); 

			$headers = "MIME-Version: 1.0\r\n";
			$headers .= "Date: ". date('r',time()) . "\r\nFrom: ";
			$headers .= ($this->Sendername == '') ? '' : $this->Sendername . ' ';
			$headers .= "<";
			$headers .= $this->Senderaddy;
			$headers .= ">\r\n";
//			$headers .= "To: ";
//			$headers .= $this->Recipients;
//			$headers .= "\r\n";
			if (strlen($this->CC_list) > 0) {
				$headers .= "CC: ";
				$headers .= $this->CC_list;
				$headers .= "\r\n";
			}
			if (strlen($this->BCC_list) > 0) {
				$headers .= "Bcc: ";
				$headers .= $this->BCC_list;
				$headers .= "\n";
			}

			if( $this->notice ){
				
				$headers .= "Disposition-Notification-To: {$this->Senderaddy}";
				$headers .= "\r\n";
				
			}
			
			foreach($this->extraheaders as $h ) $headers .= "$h\r\n";


			$headers .= "Content-Type: multipart/mixed; boundary=\"" . $boundary . "\"\r\n";
			$headers .= "Content-Transfer-Encoding: 8bit\r\n";

			$meat = "--$boundary\r\n";
			if ($this->ishtml == 0) $meat .= "Content-Type: text/plain; charset=\"UTF-8\"\r\n";
			if ($this->ishtml == 1) $meat .= "Content-Type: text/html; charset=\"UTF-8\"\r\n";

			$meat .= "Content-Transfer-Encoding: 8bit\r\n\r\n";
			$meat .= $this->message;
			$meat .= "\r\n\r\n\r\n--";
			$meat .= $boundary;
			$meat .= (count($this->attachments) > 0) ? "\r\n" : "--\r\n";
			$na = count($this->attachments) -1;


			if ($na >= 0) {
				
				//set_magic_quotes_runtime(0); @TODO : deprecated function : fuite de mémoire observée ??? NE PAS ACTIVER
				for ($j=0;$j<=$na;$j++) {
					$meat .= "Content-Type: ";
					$meat .= "Content-Type: ";
					$meat .= $this->attachments[$j][2]; //$mimetype;
					$meat .= "; name=\"";
					$meat .= $this->attachments[$j][0]; // filename
					$meat .= "\"\n";
					$meat .= "Content-Transfer-Encoding: base64\n";
					$meat .= "Content-Disposition:attachement; filename=\"" . $this->attachments[$j][0] . "\"\n";
					if( $this->attachments[$j][3]=='file' ) $meat .= chunk_split(base64_encode( file_get_contents($this->attachments[$j][1])));
					else $meat .= chunk_split(base64_encode($this->attachments[$j][1]));
					$this->attachments[$j]='';
					$meat .= "\r\n--";
					$meat .= $boundary;
					if ($j == $na) $meat .= "--";
					else $meat .= "\r\n";
				}
				//set_magic_quotes_runtime(get_magic_quotes_gpc());
			}

			showDebug($meat,'EVM_Mailer::send() => $meat',$this->DEBUG);
			showDebug($headers,'EVM_Mailer::send() => $headers',$this->DEBUG);
			$res = mail($this->Recipients,html_entity_decode($this->subject),$meat,$headers);
			showDebug($res,'EVM_Mailer::send() => mail($this->Recipients,$this->subject,$meat,$headers)',$this->DEBUG);
			if(!$res) array_push($this->error_msgs,"Erreur lors de l'envoi du mail");
			return $res;

		} else {
			return FALSE;
		}
	}
}
//EOC
?>