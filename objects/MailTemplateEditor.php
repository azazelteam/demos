<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des modèles de mails
 */


include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/Util.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );


class MailTemplateEditor{
	
	//----------------------------------------------------------------------------------------
	
	public static $BUYER_FORGOTTEN_PASSWORD 		= "BUYER_FORGOTTEN_PASSWORD";
	public static $BUYER_REGISTRATION 				= "BUYER_REGISTRATION";
	public static $CANCEL_ORDER 					= "CANCEL_ORDER";
	public static $ESTIMATE_COMMERCIAL 				= "ESTIMATE_COMMERCIAL";
	public static $ESTIMATE_PRO_FORMA 				= "ESTIMATE_PRO_FORMA";
 	public static $ESTIMATE_NOTIFICATION 			= "ESTIMATE_NOTIFICATION";
	public static $FAX_ESTIMATE 					= "FAX_ESTIMATE";
	public static $FAX_GET_ORDER_CONFIRMATION 		= "FAX_GET_ORDER_CONFIRMATION";
	public static $FAX_ORDER_CONFIRMATION 			= "FAX_ORDER_CONFIRMATION";
	public static $FRONT_OFFICE_ESTIMATE 			= "FRONT_OFFICE_ESTIMATE";
	public static $GET_ORDER_CONFIRMATION 			= "GET_ORDER_CONFIRMATION";
	public static $INVOICE_RELAUNCH_1 				= "INVOICE_RELAUNCH_1";
	public static $INVOICE_RELAUNCH_2 				= "INVOICE_RELAUNCH_2";
	public static $INVOICE_RELAUNCH_3 				= "INVOICE_RELAUNCH_3";
	public static $INVOICE_RELAUNCH_FACTOR_1		= "INVOICE_RELAUNCH_FACTOR_1";
	public static $INVOICE_RELAUNCH_FACTOR_2		= "INVOICE_RELAUNCH_FACTOR_2";
	public static $INVOICE_RELAUNCH_FACTOR_3		= "INVOICE_RELAUNCH_FACTOR_3";
	public static $ORDER_CONFIRMATION 				= "ORDER_CONFIRMATION";
	public static $ORDER_INVOICE 					= "ORDER_INVOICE";
	public static $ORDER_NOTIFICATION 				= "ORDER_NOTIFICATION";
	public static $ORDER_NOTIFICATION_1 			= "ORDER_NOTIFICATION_1";
	public static $ORDER_NOTIFICATION_2 			= "ORDER_NOTIFICATION_2";
	public static $ORDER_NOTIFICATION_3 			= "ORDER_NOTIFICATION_3";
	public static $PRODUCT_INFORMATION 				= "PRODUCT_INFORMATION";
	public static $PRO_FORM 						= "PRO_FORM";
	public static $SUPPLIER_ORDER 					= "SUPPLIER_ORDER";
	public static $CREDIT_NOTIFICATION 				= "CREDIT_NOTIFICATION";
	public static $SPONSORSHIP_GUEST				= "SPONSORSHIP_GUEST";
	public static $SPONSORSHIP_SPONSOR				= "SPONSORSHIP_SPONSOR";
	public static $SPONSORSHIP_GIFT_TOKEN			= "SPONSORSHIP_GIFT_TOKEN";
	public static $DELIVERY_OF_GOODS_VALIDATED		= "DELIVERY_OF_GOODS_VALIDATED";
	public static $DELIVERY_OF_GOODS_NOT_VALIDATED	= "DELIVERY_OF_GOODS_NOT_VALIDATED";
	public static $RETURN_TO_SENDER					= "RETURN_TO_SENDER";
	public static $RETURN_TO_SENDER_CONFIRMATION	= "RETURN_TO_SENDER_CONFIRMATION";
	public static $REPAYMENT 						= "REPAYMENT";
	public static $SHIPPING_CONFIRMATION			= "SHIPPING_CONFIRMATION";
	public static $LITIGATION						= "LITIGATION";
	public static $LITIGATION_ORDER					= "LITIGATION_ORDER";
	public static $PDF_DOWNLOAD						= "PDF_DOWNLOAD";
	public static $SUPPLIER_CONFIRMATION_REMINDER	= "SUPPLIER_CONFIRMATION_REMINDER";
	public static $SUPPLIER_DISPATCH_REMINDER		= "SUPPLIER_DISPATCH_REMINDER";
	public static $SUPPLIER_ORDER_CANCELLATION		= "SUPPLIER_ORDER_CANCELLATION";
	public static $ESTIMATE_REMINDER				= "ESTIMATE_REMINDER";
	public static $ESTIMATE_AKNOWLEDGEMENT_OF_RECEIPT= "ESTIMATE_AKNOWLEDGEMENT_OF_RECEIPT";
	public static $REMOTE_ESTIMATE_REFUSAL 			= "REMOTE_ESTIMATE_REFUSAL";
	public static $REMOTE_ESTIMATE_REFUSAL_AUTO 	= "REMOTE_ESTIMATE_REFUSAL_AUTO";
	public static $DELIVERY_ORDER 					= "DELIVERY_ORDER";
	public static $CARRIAGE 						= "CARRIAGE";
	public static $CARRIER_CALLS_FOR_TENDER 		= "CARRIER_CALLS_FOR_TENDER";
	public static $ORDER_AVAILABILITY 				= "ORDER_AVAILABILITY";
	public static $ECHEANCIER 				        = "ECHEANCIER";
	public static $PRODUITS_ECHEANCIER 				        = "PRODUITS_ECHEANCIER";
	//----------------------------------------------------------------------------------------
	
	private $idmailtemplate;
	private $identifier;
	private $name;
	private $subjects;
	private $bodies;
	private $language;
	private $description;
	private $tags;
	private $tagDefinitions;
	private $previewAvailable;
	private $generatePassords;
	private $useBuyerPasswordTag;
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idmail_template l'id du template de mail
	 */
	function __construct( $idmail_template = 0 ){

		$this->init();
		
		//chargement d'un modèle
	
		if( isset( $_POST[ "idmail_template" ] ) &&	!empty( $_POST[ "idmail_template" ] )
				&& isset( $_POST[ "language" ] ) && !empty( $_POST[ "language" ] ) ){
				
			$this->loadTemplate( intval( $_POST[ "idmail_template" ] ) );
			$this->setLanguage( $_POST[ "language" ] );	
			
		}
		else if( $idmail_template )
			$this->loadTemplate( $idmail_template );
			
		//mise à jour
		
		if( isset( $_POST[ "UpdateTemplate" ] ) )	
			$this->updateTemplate();	
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialisation des données
	 */
	private function init(){
		
		$this->language 			= substr( "_1", 1 );
		$this->idmail_template 		= 0;
		$this->identifier 			= "";
		$this->name 				= "";
		$this->subjects 			= array();
		$this->bodies 				= array();
		$this->tags 				= array();
		$this->description 			= "";
		$this->tagDefinitions 		= array();
		$this->previewAvailable 	= false;
		$this->useBuyerPasswordTag 	= false;
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Chargement des données du template
	 * @param int $idmail_template l'id du template à charger
	 * @return void
	 */
	public function loadTemplate( $idmail_template ){
	
		global 	$GLOBAL_DB_HOST,
				$GLOBAL_DB_USER,
				$GLOBAL_DB_PASS,
				$GLOBAL_DB_NAME;
		
		$db =& DBUtil::getConnection();

		$lang = $lang = "_1";
		
		$query = "
		SELECT *,REPLACE(body_1,'\\\\r\\\\n', '') as repbody_1,REPLACE(body_2,'\\\\r\\\\n', '') as repbody_2,REPLACE(body_3,'\\\\r\\\\n', '') as repbody_3,REPLACE(body_4,'\\\\r\\\\n', '') as repbody_4,REPLACE(body_5,'\\\\r\\\\n', '') as repbody_5
		FROM mail_template
		WHERE idmail_template = '$idmail_template'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de charger le template '{$this->idmail_template}'" );
			
		if( !$rs->RecordCount() )
			return;
		
		$this->idmail_template 	= $rs->fields( "idmail_template" );
		$this->identifier 		= $rs->fields( "identifier" );
		$this->name 			= $rs->fields( "name$lang" );
		$this->description 		= $rs->fields( "description$lang" );
			
		$language = 1;
		while( $language <= 5 ){

			$this->subjects[ $language ]	= $rs->fields( "subject_$language" );
            $this->bodies[ $language ]		= $rs->fields( "repbody_$language" );
            $language++;
			
		}

		//construction des menus de marqueurs
		
		$tagList = $rs->fields( "tags" );
		$tagMenus = explode( ",", $rs->fields( "tags" ) );
		
		$i = 0;
		while( $i < count( $tagMenus ) ){
			
			$menu = $tagMenus[ $i ];
			
			if( !empty( $menu ) ){
				
				switch( $menu ){
					
					case "ADMIN" 					: $this->setUseAdminTags(); 				break;
					case "BUYER" 					: $this->setUseBuyerTags(); 				break;
					case "COMMERCIAL" 				: $this->setUseCommercialTags();			break;
					case "ESTIMATE" 				: $this->setUseEstimateTags(); 				break;
					case "ORDER" 					: $this->setUseOrderTags(); 				break;
					case "INVOICE" 					: $this->setUseInvoiceTags(); 				break;
					case "SUPPLIER" 				: $this->setUseSupplierTags(); 				break;
					case "SUPPLIER_ORDER" 			: $this->setUseSupplierOrderTags();	 		break;
					case "UTIL" 					: $this->setUseUtilTags(); 					break;
					case "PRODUCT" 					: $this->setUseProductTags(); 				break;
					case "BUYER_PRODUCT_INFO" 		: $this->setUseBuyerProductInfoTags(); 		break;
					case "PASSWORD" 				: $this->setUseBuyerPasswordTags(); 		break;
					case "TENDER" 					: $this->setUseTenderTags(); 				break;
					case "CREDIT" 					: $this->setUseCreditTags(); 				break;
					case "SPONSORSHIP_GUEST" 		: $this->setUseSponsorshipGuestTags();		break;
					case "SPONSORSHIP_SPONSOR" 		: $this->setUseSponsorshipSponsorTags();	break;
					case "GIFT_TOKEN" 				: $this->setUseGiftTokenTags();				break;
					case "RETURN_TO_SENDER" 		: $this->setUseReturnToSenderTags();		break;
					case "REPAYMENT" 				: $this->setUseRepaymentTags();				break;
					case "LITIGATION" 				: $this->setUseLitigationTags();			break;
					case "LITIGATION_ORDER" 		: $this->setUseLitigationTags();			break;
					case "REMOTE_ESTIMATE" 			: $this->setUseRemoteEstimateTags();		break;
					case "DELIVERER" 				: $this->setUseDelivererTags();				break;
					case "CARRIER_CALLS_FOR_TENDER" : $this->setUseCarrierCallsForTenderTags(); break;
					case "CARRIAGE" 				: $this->setUseCarriageTags();              break;
					case "PRODUITS_ECHEANCIER"      : $this->setUseecheanceTags();              break;
					default 						: die( "Unknown Menu identifier '$menu'" );
						
				}
			
			}
			
			$i++;
			
		}

	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Mise à jour du template
	 * @return void
	 */
	private function updateTemplate(){
		
		
		
		$lang = $lang = "_1";
		
		$idmail_template 	= intval( $_POST[ "idmail_template" ] );
		$language 			= intval( $_POST[ "language" ] );
		$body 				= $this->setAbsoluteURIs( stripslashes( $_POST[ "body" ] ) );
		$subject 			= stripslashes( $_POST[ "subject" ] );

		$db =& DBUtil::getConnection();
		$lang = $lang = "_1";
		$subject  = str_replace("&nbsp;", " ", $subject);
		$subject  = str_replace("<br>", " ", $subject);
		$query = "
		UPDATE mail_template
		SET name$lang = '" . addslashes( $this->name ) . "',
		subject_$language = '" . addslashes( $subject ) . "',
		body_$language = '" . addslashes( $body ) . "'
		WHERE idmail_template = '$idmail_template'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de mettre à jour le modèle" );
		
		$this->loadTemplate( $idmail_template );
		$this->setLanguage( $language );
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Définit la langue à utiliser dans le templates
	 * @param int $language l'id de la langue
	 * @return void
	 */
	public function setLanguage( $language ){ 
		
		$this->language = $language; 
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Récupère le sujet du template
	 * @return mixed,string le sujet si il existe, false sinon
	 */
	public function getSubject(){ 
		
		return isset( $this->subjects[ $this->language ] ) ? $this->subjects[ $this->language ] : false; 
	
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Récupère le corps du texte
	 * @return mixed,string le corps du message si il existe, false sinon
	 */
	public function getBody(){ 
		
		return isset( $this->bodies[ $this->language ] ) ? $this->bodies[ $this->language ] : false; 
	
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Récupère le nom du template
	 * @return string le nom du template
	 */
	public function getName(){ return $this->name; }
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Affiche le module de sélection des templates
	 * @return void
	 */
	public function displayTemlateSelector(){
		
		global $GLOBAL_START_URL;
		
		?>
		<p style="padding:4px; text-align:center; white-space:nowrap; margin-top:20px; font-weight:bold;">
			Choix du modèle de mail :
			<?php $this->listAvailableTemplates(); ?>
			&nbsp;Langue : <?php $this->listAvailableLanguages(); ?>
			&nbsp;<input type="submit" value="Choisir" class="blueButton" />
		</p>
		<?php
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Affiche la liste des tags disponibles pour le template sélectionné
	 * @return void
	 **/
	public function listTags(){
		
		global $GLOBAL_START_URL;
		
		if( !count( $this->tags ) )
			return;

		$menuWidth = 840;
		$itemWidth = floor( $menuWidth / count( $this->tags ) ) - 5;
		
		?>
		<style type="text/css">

		#basic-accordion{
			
			border:5px solid #EEEEEE;
			padding:2px;
			width:220px;
			
		}
		
		</style>
		<div id="basic-accordion">
		<?php
		
		$i = 0;
		foreach( $this->tags as $menu => $tags ){
			if ($menu == "produits_echeancier")
			$menus =  htmlentities(  $menu );	
			else
			$menus = htmlentities( $this->translate( $menu ) );
			
			
			?>
			
			<h3 id="test<?php echo $i ?>-header"><a href="#"><?php echo  $menus   ?></a></h3>
			<div id="test<?php echo $i ?>-content">
			<?php
				
				$j = 0;
				foreach( $tags as $tag => $value ){
				if ($menu == "produits_echeancier")
					$label =  $tag;
					else
					$label = $this->translate( $menu, $tag );
					$imageID = "{$menu}.{$tag}";
					
					if( $j )
						echo "<br />";
					
					?>
					<img id="<?php echo htmlentities( $imageID ) ?>" name="<?php echo htmlentities( $imageID ) ?>" src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( $label ) ?>" alt="<?php echo  htmlentities( $label ) ?>" style="cursor:pointer;" />
					<?php
			
					$j++;
					
				}
			
			?>
			</div>
			<?php
			
			$i++;
			
		}

		?>
		</div>
		<script type="text/javascript">
		<!--
			
			$(document).ready(function(){
				$("#basic-accordion").accordion({ autoHeight: false });
			});
			
		//-->
		</script>

		<?php
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Affiche le modèle de mail
	 * @return void
	 */
	public function display(){
		
		global 	$GLOBAL_START_URL;
		
		include_once( dirname( __FILE__ ) . "/WYSIWYGEditor.php" );
		
		if( isset( $_POST[ "language" ] ) )
			$this->setLanguage( intval( $_POST[ "language" ] ) );
			
		if( isset( $_POST[ "idmail_template" ] ) )
			$this->loadTemplate( intval( $_POST[ "idmail_template" ] ) );
			
		?>
		<script type="text/javascript" language="javascript">
		<!--
		
			sfHover = function(){
			
				var sfEls = document.getElementById( 'cssTagMenu' ).getElementsByTagName( 'li' );
				
				for ( var i = 0; i < sfEls.length; i++ ){
					
					sfEls[i].onmouseover = function(){
					
						this.className += " sfhover";
						
					}
					
					sfEls[i].onmouseout = function(){
					
						this.className = this.className.replace( new RegExp( " sfhover\\b" ), "" );
					
					}
					
				}
				
			}
			
			if( window.attachEvent )
				window.attachEvent( "onload", sfHover );
			
			//----------------------------------------------------------------------------------------
			
			function updateSubject(){
			
				var subject = document.getElementById( 'subjectEditor' ).innerHTML;
				document.forms.TemplateForm.elements[ 'subject' ].value = subject;
				
			}
			
			//----------------------------------------------------------------------------------------
			
			function selectTemplate(){
			
				var templateList = document.getElementById( 'idmail_template' );
				var selectedTemplate = templateList.options[ templateList.selectedIndex ].value;
				
				if( !selectedTemplate ){
				
					clearEditor();
					return;
					
				}
				
				var languageList = document.getElementById( 'languages' );
				var selectedLanguage = languageList.options[ languageList.selectedIndex ].value;
				
				if( !selectedLanguage ){
				
					clearEditor();
					return;
					
				}
				
				var subjectEditor = document.getElementById( 'subjectEditor' );
				subjectEditor.innerHTML = templates[ selectedTemplate ][ 'subject_' + selectedLanguage ];
				
				var editor = document.getElementById( 'body_ifr' );
				var doc = editor.contentWindow.document;
				doc.body.innerHTML = templates[ selectedTemplate ][ 'body_' + selectedLanguage ];
				
			}
			
			//----------------------------------------------------------------------------------------
			
			function clearEditor(){
			
				var editor = document.getElementById( 'body_ifr' );
				var doc = editor.contentWindow.document;
				
				doc.body.innerHTML = '';
				
			}
			<?php
			
			/*
			//----------------------------------------------------------------------------------------
			
			function insertTag( idmenu, tag, label ){
				
				var target = document.forms.TemplateForm.elements[ 'TagTarget' ];
			
  				if( target[ 0 ].checked==true )
  					insertSubjectTag( idmenu, tag, label );
				else insertBodyTag( idmenu, tag, label );
				
			}
			
			//----------------------------------------------------------------------------------------
			
			function insertSubjectTag( idmenu, tag, label ){

				var imgID = idmenu + '.' + tag;
				
				var editor = document.getElementById( 'subjectEditor' );
				editor.focus();
				
				var src = '<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=' + label;

				//doc.execCommand( 'insertimage', '', src );

				if(editor.selection) { //ie

					var imgTag = '<img id="' + imgID + '" name="' + imgID + '" src="' + src + '" alt="" style="vertical-align:middle;" />';
					var sel = editor.selection;
					var range = sel.createRange();
					
					range.collapse( false );
					range.pasteHTML( imgTag );
					
				}
				else if( window.getSelection ){ //mozilla
				
					var sel = window.getSelection();
					var range = sel.getRangeAt(0);
					range.collapse(false);
					
					var img = document.createElement( "img" );
			
					img.src = src;
					img.alt = '';
					img.id = imgID;
					img.name = imgID;
					img.setAttribute( 'style', 'vertical-align:text-bottom;' );
					
					range.insertNode( img );
					
				}
				else alert( 'Unsupported browser' );
				
			}
			
			//----------------------------------------------------------------------------------------
			
			function insertBodyTag( idmenu, tag, label ){

				var imgID = idmenu + '.' + tag;
				
				var editor = document.getElementById( 'body_ifr' );
				var doc = editor.contentWindow.document;
				var win = editor.contentWindow;
				
				var src = '<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=' + label;

				//doc.execCommand( 'insertimage', '', src );

				if(doc.selection) { //ie

					var imgTag = '<img id="' + imgID + '" name="' + imgID + '" src="' + src + '" alt="" style="vertical-align:text-bottom" />';
					var sel = doc.selection;
					var range = sel.createRange();
					
					range.collapse( false );
					range.pasteHTML( imgTag );
				}
				else if( win.getSelection ){ //mozilla
				
					var sel = win.getSelection();
					var range = sel.getRangeAt(0);
					range.collapse(false);
					
					var img = doc.createElement( "img" );
			
					img.src = src;
					img.alt = '';
					img.id = imgID;
					img.name = imgID;
					img.setAttribute( 'style', 'vertical-align:text-bottom;' );
					
					range.insertNode( img );
					
				}
				else alert( 'Unsupported browser' );
				
			}

			*/
			?>
			//----------------------------------------------------------------------------------------
			
			function previewTemplate( idmail_template, lang ){
				
				postop = (self.screen.height-700)/2;
				posleft = (self.screen.width-750)/2;
				
				var options ='directories=no, location=no, menubar=no, resizable=yes, left=' + posleft + ', top=' + postop + ', scrollbars=yes, status=no, toolbar=no, width=750, height=650';
				var location = 'mail_template_preview.php?idmail_template=' + idmail_template + '&lang=' + lang;
			
				window.open( location, '_blank', options ) ;
				
			}
			
			//----------------------------------------------------------------------------------------
			
		// -->
		</script>
		<form action="mail_template_editor.php" enctype="multipart/form-data" method="post">
			<?php $this->displayTemlateSelector(); ?>
			<p style="text-align:center;">
				
			</p>
		</form>
		<?php
		
		if( !$this->idmail_template || !$this->language )
			return;
			
		?>
		<form name="TemplateForm" action="mail_template_editor.php" enctype="multipart/form-data" method="post" onsubmit="updateSubject();">
			<input type="hidden" name="UpdateTemplate" value="1" />
			<input type="hidden" name="idmail_template" value="<?php echo $this->idmail_template ?>" />
			<input type="hidden" name="language" value="<?php echo $this->language ?>" />		
		      <table class="dataTable">
				<tr>
					<td colspan="2" style="vertical-align:top; text-align:left; padding-bottom:15px; border-bottom:2px dotted #EEEEEE;">
						<span style="float:right;">
						<?php
			
							$preview = $this->preview();
							
							if( $preview === false ){
							
								?>
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/oeil.gif" alt="" style="vertical-align:middle;" />
								<span class="msg">Aucun aperçu disponible</p>
								<?php
								
							}
							else{
							
								?>
								<a href="#Preview" onclick="previewTemplate( <?php echo $this->idmail_template ?>, <?php echo $this->language ?> );">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/
									oeil.gif" alt="" style="vertical-align:middle; border-style:none;" />
								</a>
								<a href="#Preview" onclick="previewTemplate( <?php echo $this->idmail_template ?>, <?php echo $this->language ?> );">Cliquez ici pour avoir un aperçu</a>
								<?php
								
							}
								
						?>
						</span>
						<p style="font-weight:bold; margin:5px 0px;">Editer le modèle : <?php echo $this->getName() ?></p>
						<p style="font-weight:bold; margin:5px 0px;"><?php echo Dictionnary::translate( "description") ?> : <?php echo htmlentities( $this->description ) ?></p>
					</td>
				<tr>
					<td rowspan="2" style="vertical-align:top;">
						<p style="font-weight:bold; margin:0px 0px 10px 0px;">
							<?php echo Dictionnary::translate( "tags" ) ?> :
						</p>
						<?php $this->listTags(); ?>
						<p style="text-align:left; padding:15px;"><?php echo Dictionnary::translate( "drag_n_drop_mail_tags" ) ?></p>
					</td>
					<td>
						<p style="font-weight:bold; margin:0px 0px 10px 0px;">
							<?php echo Dictionnary::translate( "subject" ) ?> :
						</p>
						<div id="subjectEditor" contentEditable="true" style="width:100%; height:22px; padding:2px; vertical-align:middle; text-align:left; border:1px solid #E7E7E7;"><?php echo $this->getSubject() ?></div>
						<input type="hidden" name="subject" id="subject" value="" />
					</td>
				</tr>
				<tr>
					<td style="vertical-align:top; padding-bottom:20px;">
						<p style="font-weight:bold; margin:0px 0px 10px 0px;">
							<?php echo Dictionnary::translate( "body" ) ?> :
						</p>
						<?php WYSIWYGEditor::createEditor( "body", $this->getBody(), "style=\"width:100%; height:650px;\"" ); ?>
					</td>
				</tr>
			</table>
			<input type="submit" class="blueButton" value="Modifier" style="float:right; margin-top:7px;" />
		</form>
		<?php

	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Remplace tous les marqueurs par leurs vraies valeurs
	 * @param string $data
	 * @return le contenu avec les tags remplacés
	 */
	private function unTag( $data ){
		
		if( !$this->idmail_template )
			die( "Aucun modèle de mail défini" );

		if( !$this->checkTagDefinitions() )
			exit();

		$pattern = "/<img[^>]*src=\"[^>]*\?label=[^>]*\"[^>]*>/i";
		$matches = array();
		
		preg_match_all( $pattern, $data, $matches );
	
		if( !count( $matches ) || !count( $matches[ 0 ] ) )
			return $data;
		
		$i = 0;
		while( $i < count( $matches[ 0 ] ) ){
			
			$match = $matches[ 0 ][ $i ];
			
			$start 	= strpos( $match, "name=\"" ) + 6;
			$end 	= strpos( $match, "\"", $start );
			
			if( $start !== false && $end !== false ){
				
				$tagID 	= substr( $match, $start, $end - $start );
				
				list( $menu, $tag ) = explode( ".", $tagID );
				
				$value = $this->getTagValue( $menu, $tag );
				
				/*if( $value === false )
					die( "Impossible de récupérer la valeur du marqueur '$tagID'" );
*/
				$pattern = "/<img[^>]*name=\"$tagID\"[^>]*>/i";
				$data = preg_replace( $pattern, $value, $data );

			}
			
			$i++;
			
		}
		
		return $data;
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Remplace tous les marqueurs du corps du mail par leurs vraies valeurs
	 * @return le contenu avec les tags remplacés
	 */
	public function unTagBody(){

		return $this->untag( $this->getBody() );
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Remplace tous les marqueurs du sujet par leurs vraies valeurs
	 * @return le contenu avec les tags remplacés
	 */
	function unTagSubject(){
		
		return $this->untag( $this->getSubject() );
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Retourne la valeur du tag
	 * @param string $menu le nom du menu
	 * @param string $tag le nom du tag
	 * @return mixed la valeur du tag, false en cas de problème
	 */
	private function getTagValue( $menu, $tag ){

		if( isset( $this->tags[ $menu ] ) && isset( $this->tags[ $menu ][ $tag ] ) )
			return $this->tags[ $menu ][ $tag ];
		
		return false;
		
	}

	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs
	 * @param string $menu le nom du menu
	 * @param string $table le nom de la table
	 * @param string $columns les champs à récupérer
	 * @param string $primaryKey la clé primaire de la table
	 * @param int $primaryKeyValue la valeur de la clé primaire de la table
	 * @return void
	 */
	private function setTags( $menu, $table, $columns, $primaryKey, $primaryKeyValue = 0 ){

		$db =& DBUtil::getConnection();
		
		$this->tags[ $menu ] = array();
		
		foreach( $columns as $column )
			$this->tags[ $menu ][ $column ] = "";
			
		if( !$primaryKeyValue )
			return;
			
		$select = "";
		//reset( $columns );
		foreach( $columns as $column ){
			
			if( !empty( $select ) )
				$select .= ", ";
				
			$select .= "`$column`";
			
		}
			
		$query = "
		SELECT $select
		FROM `$table`
		WHERE `$primaryKey` = '$primaryKeyValue'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer les valeur des marqueurs<br />" );
		
		//reset( $columns );
		foreach( $columns as $column )
			$this->tags[ $menu ][ $column ] = $rs->fields( $column );
			
	}
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs appel d'offre
	 * @param string $menu le nom du menu
	 * @param string $table le nom de la table
	 * @param string $columns les champs à récupérer
	 * @param string $primaryKey la clé primaire de la table
	 * @param int $primaryKeyValue la valeur de la clé primaire de la table
	 * @return void
	 */
	private function setTagsTender( $menu, $table, $columns, $primaryKey, $primaryKeyValue = 0 , $primaryKey2, $primaryKey2Value){

		$db =& DBUtil::getConnection();
		
		$this->tags[ $menu ] = array();
		
		foreach( $columns as $column )
			$this->tags[ $menu ][ $column ] = "";
			
		if( !$primaryKeyValue )
			return;
			
		$select = "";
		//reset( $columns );
		foreach( $columns as $column ){
			
			if( !empty( $select ) )
				$select .= ", ";
				
			$select .= "`$column`";
			
		}
			
		$query = "
		SELECT $select
		FROM `$table`
		WHERE `$primaryKey` = '$primaryKeyValue'
		AND  `$primaryKey2` = '$primaryKey2Value'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer les valeur des marqueurs<br />" );
		
		//reset( $columns );
		foreach( $columns as $column )
			$this->tags[ $menu ][ $column ] = $rs->fields( $column );
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le transport
	 * @param int $idcarriage
	 * @return void
	 */
	public function setUseCarriageTags( $idcarriage = 0 ){
	
		$this->tagDefinitions[ "CARRIAGE" ] = $idcarriage;
		
		$this->tags[ "carriage" ][ "idcarriage" ] 	= $idcarriage;
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le client
	 * @param int $idbuyer l'id du client
	 * @param int $idcontact l'id du contact
	 * @return void
	 */
	public function setUseBuyerTags( $idbuyer = 0, $idcontact = 0 ){
		
		$db =& DBUtil::getConnection();
		$lang = "_1";
			
		$this->tagDefinitions[ "BUYER" ] = $idbuyer;
		
		$menu 		= "buyer";
		$table 		= "buyer";
		$primaryKey = "idbuyer";
		
		$columns = array(

			"idbuyer",
			"company",
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idbuyer );
		
		$this->tags[ $menu ][ "title" ] 		= "";
		$this->tags[ $menu ][ "firstname" ] 	= "";
		$this->tags[ $menu ][ "lastname" ] 		= "";
		$this->tags[ $menu ][ "login" ] 		= "";
		$this->tags[ $menu ][ "city" ] 			= "";
		$this->tags[ $menu ][ "zipcode" ] 		= "";
		$this->tags[ $menu ][ "adress" ] 		= "";
		$this->tags[ $menu ][ "state" ]			= "";
		
		if( $this->useBuyerPasswordTag )
			$this->tags[ $menu ][ "password" ] 	= "******";
		
		if( !$idbuyer )
			return;
			
		$query = "
		SELECT t.title_{$this->language} AS title, c.firstname, c.lastname
		FROM contact c
		LEFT JOIN title t
		ON c.title = t.idtitle
		WHERE c.idbuyer = '$idbuyer' 
		AND c.idcontact = '$idcontact'
		LIMIT 1";

		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les coordonnées du contact" );
			
		$this->tags[ $menu ][ "title" ] 	= $rs->fields("title") == NULL ? "" : $rs->fields( "title" );
		$this->tags[ $menu ][ "firstname" ] = $rs->fields("firstname");
		$this->tags[ $menu ][ "lastname" ] 	= $rs->fields("lastname");
		
		
		$qb = "SELECT b.zipcode, b.adress, b.adress_2, b.city, b.idstate FROM buyer b WHERE idbuyer='$idbuyer'";
		$rsb = $db->Execute( $qb );
		
		if( $rsb === false )
			die( "Impossible de récupérer les coordonnées du client" );
			
		$this->tags[ $menu ][ "city" ] 		= $rsb->fields("city");
		$this->tags[ $menu ][ "zipcode" ] 	= $rsb->fields("zipcode");
		
		$adress = $rsb->fields("adress");
		$adress_2 = $rsb->fields("adress_2");
		
		if($adress_2!='')
			$adress_full = $adress."<br />".$adress_2;
		else
			$adress_full = $adress;
			
		$this->tags[ $menu ][ "adress" ] 	= $adress_full;
		
		//pays
		
		$idstate = $rsb->fields("idstate");
		
		$qstate = "SELECT name$lang as state FROM state WHERE idstate = '$idstate'";
		
		$rstate = $db->Execute( $qstate );
		
		if( $rstate === false )
			die( "Impossible de récupérer le nom du pays" );
		
		if( $rstate->RecordCount() )
			$this->tags[ $menu ][ "state" ] = $rstate->fields( "state" );
			
		
		//paramètres d'identification
		
		$query = "SELECT login FROM user_account WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les paramètres d'identification" );
			
		if( $rs->RecordCount() )
			$this->tags[ $menu ][ "login" ] = $rs->fields( "login" );
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour un client faisant une demande d'information produit
	 * @param int $idtitle l'identifiant du titre
	 * @param string $name le nom
	 * @param string $email l'email
	 * @param string $company l'entreprise
	 * @param string $phone le numéro de téléphone
	 * @param string $fax le numéro de fax
	 * @param string $message le message d'accompagnement
	 * @return void
	 */
	public function setUseBuyerProductInfoTags( $title = "", $name = "", $email = "", $company = "", $phone = "", $fax = "" ){
		
		$lang = "_1";
		
		$this->tagDefinitions[ "BUYER_PRODUCT_INFO" ] = $email;
		
		$menu = "buyer_product_info";
		
		$this->tags[ $menu ][ "title" ] = $title;
		$this->tags[ $menu ][ "name" ] = $name;
		$this->tags[ $menu ][ "email" ] = $email;
		$this->tags[ $menu ][ "company" ] = $company;
		$this->tags[ $menu ][ "phone" ] = $phone;
		$this->tags[ $menu ][ "fax" ] = $fax;
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le chèque-cadeau
	 * @param int
	 * @return void
	 */
	public function setUseGiftTokenTags( $idtoken = 0 ){
		
		$lang = "_1";
		
		$this->tagDefinitions[ "GIFT_TOKEN" ] = $idtoken;
		
		$menu 		= "gift_token";
		$table 		= "gift_token";
		$primaryKey = "idtoken";
		
		$columns = array(

			"serial",
			"expiration_date"

		);

		$this->setTags( $menu, $table, $columns, $primaryKey, $idtoken );
		
		$this->tags[ $menu ][ "amount" ]  = ""; //montant du chèque
		
		//correctifs
		
		if( !$idtoken )
			return;
		
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
		$query = "
		SELECT gtm.`value`
		FROM gift_token_model gtm, gift_token gt
		WHERE gt.idtoken = '$idtoken'
		AND gt.idtoken_model = gtm.idmodel
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->tags[ $menu ][ "expiration_date" ] 	= humanReadableDate2( substr( $this->tags[ $menu ][ "expiration_date" ], 0, 10 ) );
		$this->tags[ $menu ][ "amount" ] 	= Util::priceFormat( $rs->fields( "value" ) );

	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les produits
	 * @param int $idproduct l'id du produit
	 * @return void
	 */
	public function setUseTenderTags( $idestimate = 0, $idestimate_row = 1 ){
		
		global 	$GLOBAL_START_URL;
	
		$db =& DBUtil::getConnection();
		$lang = "_1";
	
		//autoriser ou non l'envoi du mail
		
		$this->tagDefinitions[ "TENDER" ] = $idestimate;

		//construire le menu et le sous-menu
		
		$this->tags[ "tender" ][ "ligne_article" ] 			= "";
		
		$table="estimate_row";
		
		$columns = array(
			"idestimate",
			"idrow",
			"quantity",
			"summary",
			"designation"
		);

		//remplacer les tags par les vraies valeurs
		
		if( !$idestimate || !$idestimate_row )
			return;
																						 			
		$this->tags[ "tender" ][ "ligne_article" ] = $this->getReferenceTender( "estimate_row", "idestimate", $idestimate, "idrow", $idestimate_row );
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le parrainage filleul
	 * @param string $mail_guest l'email du filleul
	 * @param string $password le mot de passe du filleul
	 * @return void
	 */
	public function setUseSponsorshipGuestTags( $mail_guest = 0, $password = 0 ){
		
		global 	$GLOBAL_START_URL;
	 
		$db =& DBUtil::getConnection();
		$lang = "_1";
		
		$this->tagDefinitions[ "SPONSORSHIP_GUEST" ] = $mail_guest;
		
		$menu = "sponsorship_guest";
		$this->tags[ $menu ][ "guest_login" ] = "";
		$this->tags[ $menu ][ "guest_password" ] = "";
		
		if( !$mail_guest || !$password )
			return;
			
		$this->tags[ $menu ][ "guest_login" ] = $mail_guest;
		$this->tags[ $menu ][ "guest_password" ] = $password;

		
	}
	
	
//--------------------------------------------------------------------------------------------------------------------
	public function setUseecheanceTags( $idproduct_term = 1 ){
		$this->tagDefinitions[ "PRODUITS_ECHEANCIER" ] = $idproduct_term;
		
		$menu 		= "produits_echeancier";
		$table 		= "product_term";
		$primaryKey = "idproduct_term";
		
		$columns = array(

			"reference",
				"idorder",
			"designation",
			"term_date"
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idproduct_term );
		
	
		
	}



	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le parrainage parrain
	 * @param int $idbuyer le numéro de client du parrain
	 * @return void
	 */
	public function setUseSponsorshipSponsorTags( $idbuyer = 0, $idcontact = 0 ){
		
		$lang = "_1";
		
		$this->tagDefinitions[ "SPONSORSHIP_SPONSOR" ] = $idbuyer;
		
		$menu = "sponsorship_sponsor";
		
		$this->tags[ $menu ][ "title" ] 		= "";
		$this->tags[ $menu ][ "firstname" ] 	= "";
		$this->tags[ $menu ][ "lastname" ] 		= "";
		$this->tags[ $menu ][ "city" ] 			= "";
		$this->tags[ $menu ][ "zipcode" ] 		= "";
		$this->tags[ $menu ][ "adress" ] 		= "";
		$this->tags[ $menu ][ "state" ]			= "";
		$this->tags[ $menu ][ "company" ]		= "";
		
		if( !$idbuyer )
			return;
			
		$query = "
		SELECT t.title_{$this->language} AS title, c.firstname, c.lastname
		FROM contact c
		LEFT JOIN title t
		ON c.title = t.idtitle
		WHERE c.idbuyer = '$idbuyer' 
		AND c.idcontact = '$idcontact'
		LIMIT 1";

		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les coordonnées du contact" );
			
		$this->tags[ $menu ][ "title" ] 	= $rs->fields("title") == NULL ? "" : $rs->fields( "title" );
		$this->tags[ $menu ][ "firstname" ] = $rs->fields("firstname");
		$this->tags[ $menu ][ "lastname" ] 	= $rs->fields("lastname");
		
		
		$qb = "SELECT b.company, b.zipcode, b.adress, b.adress_2, b.city, b.idstate FROM buyer b WHERE idbuyer='$idbuyer'";
		$rsb = DBUtil::query( $qb );
		
		if( $rsb === false )
			die( "Impossible de récupérer les coordonnées du client" );
			
		$this->tags[ $menu ][ "city" ] 		= $rsb->fields("city");
		$this->tags[ $menu ][ "zipcode" ] 	= $rsb->fields("zipcode");
		$this->tags[ $menu ][ "company" ] 	= $rsb->fields("company");
		
		$adress = $rsb->fields("adress");
		$adress_2 = $rsb->fields("adress_2");
		
		if($adress_2!='')
			$adress_full = $adress."<br />".$adress_2;
		else
			$adress_full = $adress;
			
		$this->tags[ $menu ][ "adress" ] 	= $adress_full;
		
		//pays
		
		$idstate = $rsb->fields("idstate");
		
		$qstate = "SELECT name$lang as state FROM state WHERE idstate = '$idstate'";
		
		$rstate = DBUtil::query( $qstate );
		
		if( $rstate === false )
			die( "Impossible de récupérer le nom du pays" );
		
		if( $rstate->RecordCount() )
			$this->tags[ $menu ][ "state" ] = $rstate->fields( "state" );
		
	}

//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les produits
	 * @param int $idproduct l'id du produit
	 * @return void
	 */
	public function setUseProductTags( $idproduct = 0 ){
		
		global 	$GLOBAL_START_URL;
	
		$db =& DBUtil::getConnection();
		$lang = "_1";
		
		$menu = "product";
		
		$this->tagDefinitions[ "PRODUCT" ] = $idproduct;

		$this->tags[ $menu ][ "product_name" ] 				= "";
		$this->tags[ $menu ][ "product_description" ] 		= "";
		$this->tags[ $menu ][ "product_tech_description" ] 	= "";
		$this->tags[ $menu ][ "pdf_url" ] 					= "";
		$this->tags[ $menu ][ "product_url" ] 				= "";
		$this->tags[ $menu ][ "product_thumb" ] 			= "";
		
		//correctifs
		
		if( !$idproduct )
			return;
			
		$query = "
		SELECT name_{$this->language} AS name, 
			description_{$this->language} AS description, 
			desc_tech_{$this->language} AS tech_description
		FROM product 
		WHERE idproduct = '$idproduct' 
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les informations du produit" );
			
		$name = $rs->fields( "name" );

		$src = URLFactory::getHostURL() . URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE );
		$this->tags[ $menu ][ "product_thumb" ] = "<img src=\"$src\" alt=\"\" />";
		
		
		$PDFURL = URLFactory::getPDFURL( $idproduct, $name );
		$productURL = URLFactory::getProductURL( $idproduct, $name );
		
		$this->tags[ $menu ][ "product_name" ] 				= $rs->fields( "name" );
		$this->tags[ $menu ][ "product_description" ] 		= $rs->fields( "description" );
		$this->tags[ $menu ][ "product_tech_description" ] 	= $rs->fields( "tech_description" );
		$this->tags[ $menu ][ "pdf_url" ] 					= "<a href=\"$PDFURL\">" . htmlentities( $PDFURL ) . "</a>"; 
		$this->tags[ $menu ][ "product_url" ] 				= "<a href=\"$productURL\">" . htmlentities( $productURL ) . "</a>";
		
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le commercial
	 * @param int $iduser l'id du commercial
	 * @return void
	 */
	public function setUseCommercialTags( $iduser = 0 ){
		
		
		$this->tagDefinitions[ "COMMERCIAL" ] = $iduser;
		
		$menu 		= "commercial";
		$table 		= "user";
		$primaryKey = "iduser";
		
		$columns = array(

			"firstname",
			"lastname",
			"phonenumber",
			"faxnumber",
			"email"
			
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $iduser );
		
		//civilité
		
		$this->tags[ $menu ][ "title" ] = "";
			
		if( !$iduser )
			return;
		
		$db =& DBUtil::getConnection();
	
		//email
		
		$email = $this->tags[ $menu ][ "email" ];
	
		$this->tags[ $menu ][ "email" ] = "<a href=\"mailto:$email\">$email</a>";
	
		//civilité
	
		$query = "
		SELECT t.title_{$this->language} AS title, u.iduser
		FROM user u
		LEFT JOIN title t
		ON u.gender = t.idtitle
		WHERE u.iduser = '$iduser'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la civilité du commercial" );
		
		$this->tags[ $menu ][ "title" ] = $rs->fields( "title" ) == NULL ? "" : $rs->fields( "title" );

		
	}

	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le devis
	 * @param int $idestimate l'id du devis
	 * @return void
	 */
	public function setUseEstimateTags( $idestimate = 0 ){
		
		global $GLOBAL_START_URL;
		
		$this->tagDefinitions[ "ESTIMATE" ] = $idestimate;
		
		$menu 		= "estimate";
		$table 		= "estimate";
		$primaryKey = "idestimate";
		
		$columns = array(

			"idestimate",
			"DateHeure",
			"date_signed",
			"valid_until"
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idestimate );
		
		//références du devis
		
		$this->tags[ $menu ][ "estimate_rows" ] = "estimate_rows";
		
		if( $idestimate )
			$this->tags[ $menu ][ "estimate_rows" ] = $this->getReferenceList( "estimate_row", $primaryKey, $idestimate );
			
		//correctifs
		
		if( !$idestimate )
			return;
		
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
			
		$this->tags[ $menu ][ "DateHeure" ] = humanReadableDate( substr($this->tags[ "$menu" ][ "DateHeure" ], 0, 10 ) );
		$this->tags[ $menu ][ "valid_until" ] = humanReadableDate( $this->tags[ "$menu" ][ "valid_until" ] );
		$this->tags[ $menu ][ "date_signed" ] = humanReadableDate( substr( $this->tags[ "$menu" ][ "date_signed" ], 0, 10 ) );
		$this->tags[ "admin" ][	"ad_logo" ] = "<img src=\"$GLOBAL_START_URL/catalog/logo.php?idestimate=$idestimate&seen=1\" alt=\"\" />";
	
	}
	

	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour la commande
	 * @param int $idorder l'id de la commande
	 * @return void
	 */
	public function setUseOrderTags( $idorder = 0 ){
		
		global 	$GLOBAL_START_URL;
		
		$this->tagDefinitions[ "ORDER" ] = $idorder;
		
		
		$menu 		= "order";
		$table 		= "order";
		$primaryKey = "idorder";
		
		$columns = array(

			"idorder",
			"DateHeure",
			"date_signed",
			"total_amount",
			"total_amount_ht",
			"idpayment",
			"idpayment_delay"

		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idorder );
		
		$this->tags[ $menu ][ "total_amount" ] = DBUtil::getDBValue( "total_amount", "order", "idorder", $idorder );
		
		//lien pour l'accusé de réception
		
		//$this->tags[ $menu ][ "notification_link" ] = "";

		//références de la commande
		
		$this->tags[ $menu ][ "order_rows" ] = "";
		
		//correctifs
		
		if( !$idorder )
			return;
		
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
			
		$db =& DBUtil::getConnection();

		//lien pour l'accusé de réception
		
		$href = "$GLOBAL_START_URL/catalog/order_notification.php?or=";
		$href .= base64_encode( serialize( $idorder ) );
		
		$notification_link = "<a href=\"$href\">Merci d'accuser la réception de votre confirmation de commande en cliquant ici</a>";
		
		//$this->tags[ $menu ][ "notification_link" ] = $notification_link;
		
		//mode de paiement
	
		$query = "
		SELECT name_{$this->language} AS payment
		FROM payment
		WHERE idpayment = '" . $this->tags[ $menu ][ "idpayment" ] . "'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le mode de paiement pour la commande '$idorder'" );
		
		if( $rs->RecordCount() )
				$this->tags[ $menu ][ "idpayment" ] = $rs->fields( "payment" );
		else 	$this->tags[ $menu ][ "idpayment" ] = "";
		
		//délai de paiement
			
		$query = "
		SELECT payment_delay_{$this->language} AS payment_delay
		FROM payment_delay
		WHERE idpayment_delay = '" . $this->tags[ $menu ][ "idpayment_delay" ] . "'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le délai de paiement pour la commande '$idorder'" );
		
		if( $rs->RecordCount() )
				$this->tags[ $menu ][ "idpayment_delay" ] = $rs->fields( "payment_delay" );
		else 	$this->tags[ $menu ][ "idpayment_delay" ] = "";
		
		//montants et dates
		
		$this->tags[ $menu ][ "DateHeure" ] 		= humanReadableDate( substr( $this->tags[ "$menu" ][ "DateHeure" ], 0, 10 ) );
		$this->tags[ $menu ][ "date_signed" ] 		= humanReadableDate( substr( $this->tags[ "$menu" ][ "date_signed" ], 0, 10 ) );
		$this->tags[ $menu ][ "total_amount" ] 		= Util::priceFormat( $this->tags[ $menu ][ "total_amount" ], 2 );
		$this->tags[ $menu ][ "total_amount_ht" ] 	= Util::priceFormat( $this->tags[ $menu ][ "total_amount_ht" ], 2 );
		
		//références de la commande
		
		$this->tags[ $menu ][ "order_rows" ] = $this->getReferenceList( "order_row", $primaryKey, $idorder );
		$this->tags[ "admin" ][	"ad_logo" ] = "<img src=\"$GLOBAL_START_URL/catalog/logo.php?idorder=$idorder&seen=1\" alt=\"\" />";
		
		//Suivi de colis
		
		$this->tags[ $menu ][ "package" ] = $this->getPackageInfos( $idorder );
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour la facture
	 * @param int $idbilling_buyer l'id de la facture
	 * @return void
	 */
	function setUseInvoiceTags( $idbilling_buyer = 0 ){

		global 	$GLOBAL_START_URL;
		
		
		$this->tagDefinitions[ "INVOICE" ] = $idbilling_buyer;
		
		$menu 		= "invoice";
		$table 		= "billing_buyer";
		$primaryKey = "idbilling_buyer";
		
		$columns = array(

			"idbilling_buyer",
			"DateHeure",
			"total_amount",
			"total_amount_ht",
			"n_order",
			"idpayment",
			"idpayment_delay",
			"relaunch_date_1",
			"relaunch_date_2",
			"relaunch_date_3",
			"deliv_payment"
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idbilling_buyer );
		
		//lien pour l'accusé de réception
		
		//@todo
		//$this->tags[ $menu ][ "notification_link" ] = "";

		
		//références de la facture
		
		$this->tags[ $menu ][ "invoice_rows" ] = "";
		
		//correctifs
		
		$db =& DBUtil::getConnection();
		
		//@todo
		//lien pour l'accusé de réception
		
		/*
		$href = "$GLOBAL_START_URL/catalog/invoice_notification.php?or=";
		$href .= base64_encode( serialize( $idorder ) );
		
		$notification_link = "<a href=\"$href\">" . Dictionnary::translate( "invoice_notification" ) . "</a>";
		
		$this->tags[ $menu ][ "notification_link" ] = $notification_link;
		*/
		
		//mode de paiement
	
		$query = "
		SELECT name_{$this->language} AS payment
		FROM payment
		WHERE idpayment = '" . $this->tags[ $menu ][ "idpayment" ] . "'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le mode de paiement pour la facture '$idbilling_buyer'" );
		
		if( $rs->RecordCount() )
				$this->tags[ $menu ][ "idpayment" ] = $rs->fields( "payment" );
		else 	$this->tags[ $menu ][ "idpayment" ] = "";
		
		//délai de paiement
			
		$query = "
		SELECT payment_delay_{$this->language} AS payment_delay
		FROM payment_delay
		WHERE idpayment_delay = '" . $this->tags[ $menu ][ "idpayment_delay" ] . "'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le délai de paiement pour la facture '$idbilling_buyer'" );
		
		if( $rs->RecordCount() )
				$this->tags[ $menu ][ "idpayment_delay" ] = $rs->fields( "payment_delay" );
		else 	$this->tags[ $menu ][ "idpayment_delay" ] = "";
			
		//dates et montants
		
		//Seulement la date
		
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
		$Date = substr($this->tags[ $menu ][ "DateHeure" ],0,10);
		$this->tags[ $menu ][ "DateHeure" ] = humanReadableDate2( $Date );
		$this->tags[ $menu ][ "relaunch_date_1" ] = humanReadableDate2( $this->tags[ $menu ][ "relaunch_date_1" ] );
		$this->tags[ $menu ][ "relaunch_date_2" ] = humanReadableDate2( $this->tags[ $menu ][ "relaunch_date_2" ] );
		$this->tags[ $menu ][ "relaunch_date_3" ] = humanReadableDate2( $this->tags[ $menu ][ "relaunch_date_3" ] );
		$this->tags[ $menu ][ "total_amount" ] 		= Util::priceFormat( $this->tags[ $menu ][ "total_amount" ], 2 );
		$this->tags[ $menu ][ "total_amount_ht" ] 	= Util::priceFormat( $this->tags[ $menu ][ "total_amount_ht" ], 2 );
		$this->tags[ $menu ][ "deliv_payment" ] =  humanReadableDate2($this->tags[ $menu ][ "deliv_payment" ]);

		//références de la facture
		
		$this->tags[ $menu ][ "invoice_rows" ] = $this->getReferenceList( "billing_buyer_row", $primaryKey, $idbilling_buyer );
		
		//Contact facturation
		$firstname = DBUtil::getParameterAdmin("b_contact_firstname");
		$lastname = DBUtil::getParameterAdmin("b_contact_lastname");
		$phonenumber = DBUtil::getParameterAdmin("b_contact_tel");
		$faxnumber = DBUtil::getParameterAdmin("b_contact_fax");
		$email = DBUtil::getParameterAdmin("b_contact_email");
		
		$contact = $firstname." ".$lastname."<br />";
		$contact.= "Tél. :". $phonenumber." - Fax : ".$faxnumber."<br />";
		$contact.= "Mail : <a href=\"mailto:".$email."\">".$email."</a>";
		
		$this->tags[ $menu ]["b_contact"] = $contact;	
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour la commande fournisseur
	 * @param int $idorder_supplier l'id de la commande fournisseur
	 * @return void
	 */
	public function setUseSupplierOrderTags( $idorder_supplier = 0 ){

		global 	$GLOBAL_START_URL;
		
		$bdd =& DBUtil::getConnection();
		
		$this->tagDefinitions[ "SUPPLIER_ORDER" ] = $idorder_supplier;
		
		$menu 		= "order_supplier";
		$table 		= "order_supplier";
		$primaryKey = "idorder_supplier";
		
		$columns = array(

			"idorder_supplier",
			"DateHeure",
			"delivery_delay"
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idorder_supplier );
	
		//lien pour la commande fournisseur
		
		//$this->tags[ $menu ][ "notification_link" ] = "";
		
		//références de la commande fournisseur
		
		$this->tags[ $menu ][ "order_supplier_rows" ] = "";
		
		if( !$idorder_supplier )
			return;
	
		//dates
		
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
		$this->tags[ $menu ][ "DateHeure" ] = humanReadableDate( substr( $this->tags[ "$menu" ][ "DateHeure" ], 0, 10 ) );
		
		//références de la commande fournisseur
		
		$this->tags[ $menu ][ "order_supplier_rows" ] = $this->getReferenceList( "order_supplier_row", $primaryKey, $idorder_supplier );


	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour le fournisseur
	 * @param int $idsupplier l'id du fournisseur
	 * @return void
	 */
	public function setUseSupplierTags( $idsupplier = 0 ){

		$this->tagDefinitions[ "SUPPLIER" ] = $idsupplier;
		
		$menu 		= "supplier";
		$table 		= "supplier";
		$primaryKey = "idsupplier";
		
		$columns = array(

			"name"
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idsupplier );
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les données de la société
	 * @return void
	 */
	public function setUseAdminTags(){

		global 	$GLOBAL_START_URL;
		
		$this->tagDefinitions[ "ADMIN" ] = 1;
		
		$db =& DBUtil::getConnection();
		
		$this->tags[ "admin" ] = array();
	
		$parameters = array(

			"ad_name",
			"ad_firstname",
			
			"ad_company",
			"ad_adress",
			"ad_zipcode",
			"ad_city",
			"ad_state",
			"ad_tel",
			"ad_fax",
			
			"ad_logo",
			"ad_mail",
			"ad_site",
			"ad_sitename",
			
			"ad_siren",
			"ad_siret",
			"ad_ape",
			"ad_tva-intra",
			"ad_registered_brand"
			
		);
		
		$select = "";
		foreach( $parameters as $parameter ){
			
			if( !empty( $select ) )
				$select .= ", ";
				
			$select .= "'$parameter'";
		}
		
		$query = "SELECT idparameter, paramvalue FROM parameter_admin WHERE idparameter IN ( $select )";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les marqueurs administrateurs" );
			
		while( !$rs->EOF() ){
		
			$idparameter 	= $rs->fields( "idparameter" );
			$value 			= $rs->fields( "paramvalue" );
			
			$this->tags[ "admin" ][	$idparameter ] = $value;
			
			$rs->MoveNext();
			
		}
		
		//correctifs
		
		$this->tags[ "admin" ][	"ad_logo" ] = "<img src=\"$GLOBAL_START_URL" . $this->tags[ "admin" ][	"ad_logo" ] . "\" alt=\"\" />";
		$this->tags[ "admin" ][	"ad_sitename" ] = "<a href=\"$GLOBAL_START_URL\">" . $this->tags[ "admin" ][	"ad_sitename" ] . "</a>";
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les outils
	 * @return void
	 */
	public function setUseUtilTags(){

		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
		$this->tagDefinitions[ "UTIL" ] = 1;
		
		$this->tags[ "util" ] = array();
	
		$this->tags[ "util" ][ "human_readable_date" ] = humanReadableDate2( date( "Y-m-d" ) );
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise le marqueur du mot de passe pour le client
	 * @param bool $useBuyerPasswordTag utilisation ou non
	 * @return void
	 */
	private function setUseBuyerPasswordTags( $useBuyerPasswordTag = true ){
	
		$this->useBuyerPasswordTag = $useBuyerPasswordTag;
	
		if( !isset( $this->tags[ "buyer" ] ) )
			$this->tags[ "buyer" ] = array();
			
		$this->tags[ "buyer" ][ "password" ] 	= "******";
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Initialise les marqueurs pour l'avoir
	 * @param int $idcredit le numérod e l'avoir
	 * @return void
	 */
	public function setUseCreditTags( $idcredit = 0 ){

		$this->tagDefinitions[ "CREDIT" ] = $idcredit;
		
		$menu 		= "credit";
		$table 		= "credits";
		$primaryKey = "idcredit";
		
		$columns = array(

			"idcredit",
			"DateHeure",
			"object",
			"comment"
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idcredit );
		
		$this->tags[ $menu ][ "total_amount_et" ] 	= "";
		$this->tags[ $menu ][ "total_amount_ati" ] 	= ""; 
		$this->tags[ $menu ][ "total_charges_et" ] 	= "";
		$this->tags[ $menu ][ "total_charges_ati" ] = ""; 
		
		if( !$idcredit )
			return;
		
		include_once( dirname( __FILE__ ) . "/Credit.php" );
		
		$credit = new Credit( $idcredit );
		
		$query = "
		SELECT credits.total_charge_ht AS total_charges_et,
		ROUND( credits.total_charge_ht * ( 1.0 + credits.vat_rate / 100.0 ), 2 ) AS total_charges_ati,
		ROUND( SUM( credit_rows.discount_price * credit_rows.quantity ) - credits.total_charge_ht, 2 ) AS total_amount_et,
		ROUND( SUM( credit_rows.discount_price * credit_rows.quantity * ( 1.0 + credit_rows.vat_rate / 100.0 ) ) - credits.total_charge_ht * ( 1.0 + credits.vat_rate / 100.0 ), 2 ) AS total_amount_ati
		FROM credits, credit_rows
		WHERE credits.idcredit = '$idcredit'
		AND credit_rows.idcredit = credits.idcredit
		GROUP BY credit_rows.idcredit";
		
		$rs =& DBUtil::query( $query );
		
		$this->tags[ $menu ][ "total_amount_et" ] 	= Util::priceFormat( $credit->getTotalET() );
		$this->tags[ $menu ][ "total_amount_ati" ] 	= Util::priceFormat( $credit->getTotalATI() );
		$this->tags[ $menu ][ "total_charges_et" ] 	= Util::priceFormat( $credit->get( "total_charge_ht" ) );
		$this->tags[ $menu ][ "total_charges_ati" ] = Util::priceFormat( $credit->get( "total_charge_ht" ) * ( 1.0 + $credit->get( "vat_rate" ) / 100.0 ) );
			
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les retours de marchandise
	 * @return void
	 */
	public function setUseReturnToSenderTags( $idorder_supplier = 0, $idrow = 1 ){

		$this->tagDefinitions[ "RETURN_TO_SENDER" ] = $idorder_supplier;
		
		$this->tags[ "return_to_sender" ][ "idorder_supplier" ] 	= ""; 
		$this->tags[ "return_to_sender" ][ "reference" ] 			= "";
		$this->tags[ "return_to_sender" ][ "quantity" ] 			= ""; 
		$this->tags[ "return_to_sender" ][ "return_date" ] 			= ""; 
		
		if( $idorder_supplier ){
			
			$query = "
			SELECT rts.return_date, rts.quantity, osr.reference, osr.designation
			FROM return_to_sender rts, order_supplier_row osr
			WHERE rts.idorder_supplier = '" .  $idorder_supplier  . "'
			AND rts.idrow = '" .  $idrow  . "'
			AND osr.idorder_supplier = rts.idorder_supplier
			AND osr.idrow = rts.idrow
			LIMIT 1";
		
			$rs =& DBUtil::query( $query );
			
			include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
			$this->tags[ "return_to_sender" ][ "idorder_supplier" ] 	= $idorder_supplier; 
			$this->tags[ "return_to_sender" ][ "reference" ] 			= $rs->fields( "reference" );
			$this->tags[ "return_to_sender" ][ "quantity" ] 			= $rs->fields( "quantity" ); 
			$this->tags[ "return_to_sender" ][ "return_date" ] 			= humanReadableDate( substr( $rs->fields( "return_date" ), 0, 10 ) ); 
		
		}
	
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les remboursements
	 * @return void
	 */
	public function setUseRepaymentTags( $idrepayment = 0 ){

		$this->tagDefinitions[ "REPAYMENT" ] =  $idrepayment ;
		
		$this->tags[ "repayment" ][ "idrepayment" ] 	= ""; 
		$this->tags[ "repayment" ][ "mode" ] 			= "";
		$this->tags[ "repayment" ][ "amount" ] 			= ""; 
		$this->tags[ "repayment" ][ "creation_date" ] 	= ""; 
		$this->tags[ "repayment" ][ "payment_date" ] 	= ""; 
		
		if(  $idrepayment  ){
	
			include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
		
			$rs =& DBUtil::query( "SELECT * FROM repayments WHERE idrepayment = '$idrepayment' LIMIT 1" );
			
			$this->tags[ "repayment" ][ "idrepayment" ] 	= $idrepayment; 
			$this->tags[ "repayment" ][ "mode" ] 			= DBUtil::getDBValue( "name_{$this->language}", "payment", "idpayment", $rs->fields( "idpayment" ) );
			$this->tags[ "repayment" ][ "amount" ] 			= Util::priceFormat( $rs->fields( "amount" ) ); 
			$this->tags[ "repayment" ][ "creation_date" ] 	= humanReadableDate( substr( $rs->fields( "creation_date" ), 0, 10 ) );
			$this->tags[ "repayment" ][ "payment_date" ] 	= humanReadableDate( substr( $rs->fields( "payment_date" ), 0, 10 ) );
		
		}
	
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour un litige donné
	 * @return void
	 */
	public function setUseLitigationTags( $idlitigation = 0 , $type = "billing_buyer" ){
		
		$this->tagDefinitions[ "LITIGATION" ] = $idlitigation;
		
		$this->tags[ "litigation" ][ "idlitigation" ] 		= "";
		$this->tags[ "litigation" ][ "idbilling_buyer" ] 	= "";
		$this->tags[ "litigation" ][ "idorder" ] 			= "";
		$this->tags[ "litigation" ][ "reference" ] 			= ""; 
		$this->tags[ "litigation" ][ "quantity" ] 			= ""; 
		$this->tags[ "litigation" ][ "creation_date" ] 		= ""; 
		$this->tags[ "litigation" ][ "management_fees" ] 	= ""; 
		$this->tags[ "litigation" ][ "comment" ] 			= ""; 
		
				
		if( $idlitigation ){
			
			include_once( dirname( __FILE__ ) . "/Litigation.php" );
			include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
			
			$litigation = new Litigation( $idlitigation , $type );
			
			$this->tags[ "litigation" ][ "idlitigation" ] 		= strval( $litigation->getIdLitigation() );
			
			switch( $type ){
				case Litigation::$TYPE_BILLING_BUYER:
					$this->tags[ "litigation" ][ "idbilling_buyer" ] = $litigation->getIdInvoice();
					break;
					
				case Litigation::$TYPE_ORDER:
					$this->tags[ "litigation" ][ "idorder" ] = $litigation->getIdOrder();
					break;
					
			}
						
			$this->tags[ "litigation" ][ "reference" ] 			= $litigation->getItem() ? $litigation->getItem()->get( "reference" ) : ""; 
			$this->tags[ "litigation" ][ "quantity" ] 			= $litigation->getQuantity(); 
			$this->tags[ "litigation" ][ "creation_date" ] 		= humanReadableDate( substr( $litigation->getCreationDate(), 0, 10 ) ); 
			$this->tags[ "litigation" ][ "management_fees" ] 	= Util::priceFormat( $litigation->getManagementFees() ); 
			$this->tags[ "litigation" ][ "comment" ] 			= $litigation->getComment(); 
		
		}
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour un litige donné
	 * @return void
	 */
	public function setUseDelivererTags( Deliverer $deliverer = NULL ){
	
		$this->tagDefinitions[ "DELIVERER" ] = $deliverer ? $deliverer->getId() : 0;
		
		$this->tags[ "deliverer" ][ "name" ] 		= ""; 
		$this->tags[ "deliverer" ][ "gender" ] 		= ""; 
		$this->tags[ "deliverer" ][ "firstname" ] 	= "";
		$this->tags[ "deliverer" ][ "lastname" ] 	= ""; 
		
		if( $deliverer ){
	
			$this->tags[ "deliverer" ][ "name" ] 		= $deliverer->getName(); 
			$this->tags[ "deliverer" ][ "gender" ] 		= DBUtil::getDBValue( "title_1", "title", "idtitle", $deliverer->getContact()->getGender() ); 
			$this->tags[ "deliverer" ][ "firstname" ] 	= $deliverer->getContact()->getFirstName();
			$this->tags[ "deliverer" ][ "lastname" ] 	= $deliverer->getContact()->getLastName(); 
		
		}
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les demandes de devis logismarket, edgb2b, ...
	 * @param int $idsource source du devis
	 * @param int $idcategory
	 * @return void
	 */
	public function setUseRemoteEstimateTags( $idsource = 0, $idcategory = 0 ){
		
		$this->tagDefinitions[ "REMOTE_ESTIMATE" ] = true;
		
		$this->tags[ "remote_estimate" ][ "category_anchor" ] = '<a id="CategoryAnchor" href="' . URLFactory::getHostURL() . '">' . DBUtil::getParameterAdmin( "ad_sitename" ) . '</a>';
		$this->tags[ "remote_estimate" ][ "source" ] = "";
		
		if( $idcategory ){
	
			$name = DBUtil::getDBValue( "name_1", "category", "idcategory", $idcategory );
		
			$this->tags[ "remote_estimate" ][ "category_anchor" ] = '<a href="' . URLFactory::getCategoryURL( $idcategory ) . '" title="' . htmlentities( $name ) . '">' . htmlentities( $name ) . '</a>'; 
		
		}
		
		if(  $idsource  )
			$this->tags[ "remote_estimate" ][ "source" ] = DBUtil::getDBValue( "source_1", "source", "idsource",  $idsource  );
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Initialise les marqueurs pour les demandes de devis logismarket, edgb2b, ...
	 * @param int $idsource source du devis
	 * @param int $idcategory
	 * @return void
	 */
	public function setUseCarrierCallsForTenderTags(){
		
		$this->tagDefinitions[ "CARRIER_CALLS_FOR_TENDER" ] = true;
		
		$this->tags[ "carrier_calls_for_tender" ] = array(
		
			"provenance_zipcode" 	=> "<span id=\"ProvenanceZipcodeAnchor\"><b>Code postal d'enlèvement</b></span>",
			"provenance_city" 		=> "<span id=\"ProvenanceCityAnchor\"><b>Ville d'enlèmenet</b></span>",
			"provenance_state"		=> "<span id=\"ProvenanceStateAnchor\"><b>Pays d'enlèvement</b></span>",
			
			"destination_zipcode" 	=> "<span id=\"DestinationZipcodeAnchor\"><b>Code postal de livraison</b></span>",
			"destination_city" 		=> "<span id=\"DestinationCityAnchor\"><b>Ville de livraison</b></span>",
			"destination_state"		=> "<span id=\"DestinationStateAnchor\"><b>Pays de livraison</b></span>",
		
			"linear_meters"			=> "<span id=\"LinearMetersAnchor\"><b>Nb mètres linéaires</b></span>",
			"weight"				=> "<span id=\"WeightAnchor\"><b>Poids</b></span>"
			
		);
		
	}

	//----------------------------------------------------------------------------------------
	/**
	 * Récupère la traduction pour les tags
	 * @param string $menu le nom du menu
	 * @param string $item le nom du tag
	 * @return string la traduction
	 */
	private function translate( $menu, $item = false ){
		
		$lang = User::getInstance()->getLang();
		$db =& DBUtil::getConnection();
		
		$query = "
		SELECT language" . substr( $lang, 1 ) . " AS translation 
		FROM translation 
		WHERE idtext LIKE 'tag_$menu";
		
		if( $item )
			$query .= "_$item";
			
		$query .= "' 
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le nom du marqueur '$text'" );
			
		return $rs->fields( "translation" );
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Génère la liste des modèles de mails
	 * @return void
	 */
	private function listAvailableTemplates(){
		
			
		$db =& DBUtil::getConnection();
		$lang = "_1";
		
		$query = "
		SELECT idmail_template,
			name$lang,
			subject_1,
			subject_2,
			subject_3,
			subject_4,
			subject_5,
			body_1,
			body_2,
			body_3,	
			body_4,
			body_5
		FROM mail_template 
		ORDER BY name$lang ASC";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les modèles de mail" );
			
		?>	
		<select name="idmail_template" id="idmail_template">
			<option value="0">Sélectionnez un modèle</option>
		<?php
			
			while( !$rs->EOF() ){
			
				$idmail_template 	= $rs->fields( "idmail_template" );
				$name 				= $rs->fields( "name$lang" );
				
				$selected = $idmail_template == $this->idmail_template ? " selected=\"selected\"" : "";
				
				?>
				<option value="<?php echo "$idmail_template" ?>"<?php echo $selected ?>><?php echo htmlentities( $name ) ?></option>
				<?php
					
				$rs->MoveNext();
				
			}
	
		?>
		</select>
		<?php
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Affiche la liste des langues pour les templates
	 * @return void
	 */
	private function listAvailableLanguages(){
		
		$db =& DBUtil::getConnection();
		
		?>
		<select name="language" id="language">
			<option value="0">Sélectionnez une langue</option>
		<?php
		
		$language = 1;
		while( $language <= 5 ){
			
			$query = "SELECT paramvalue AS language FROM parameter_admin WHERE idparameter LIKE 'lang$language' LIMIT 1";
			
			$rs = $db->Execute( $query );
			
			if( $rs === false )
				die( "Impossible de récupérer le nom de la langue $i" );
				
			$selected = $language == $this->language ? " selected=\"selected\"" : "";
			
			?>
			<option value="<?php echo $language; ?>"<?php echo $selected ?>><?php echo htmlentities( Dictionnary::translate( $rs->fields( "language" ) ) ) ?></option>
			<?php
			
			$language++;
			
		}
		
		?>
		</select>
		<?php
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Liste les références d'un devis, d'une commande ou d'une commande fournisseur
	 * @param string $table le nom de la table à utiliser
	 * @param string $primaryKey le nom de la clé primaire
	 * @param int $primaryKeyValue le valeur de la clé primaire
	 * @return string le html de la liste des lignes articles
	 */
	private function getReferenceList( $table, $primaryKey, $primaryKeyValue ){
		
		$db =& DBUtil::getConnection();
		
		$query = "
		SELECT quantity, reference, summary
		FROM `$table`
		WHERE `$primaryKey` = '$primaryKeyValue'";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les lignes article" );
			
		if( !$rs->RecordCount() )	
			return "";
			
		$html ="<ul>";
		
		while( !$rs->EOF() ){
			
			$quantity 	= $rs->fields( "quantity" );
			$reference 	= $rs->fields( "reference" );
			$summary 	= $rs->fields( "summary" );
			
			$html .= "<li>$quantity x $reference";

			if( !empty( $summary ) )
				$html .= " : $summary";
				
			$html .= "</li>";
			
			$rs->MoveNext();
			
		}
		
		$html .= "</ul>";
		
		return $html;
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Liste la référence d'un appel d'offre
	 * @param string $table le nom de la table à utiliser
	 * @param string $primaryKey le nom de la clé primaire
	 * @param int $primaryKeyValue le valeur de la clé primaire
	 * @return string le html de la liste des lignes articles
	 */
	private function getReferenceTender( $table, $primaryKey, $primaryKeyValue, $primaryKey2, $primaryKey2Value){
		
		$db =& DBUtil::getConnection();
		$lang = "_1";
		
		$query = "
		SELECT quantity, reference, summary, designation
		FROM `$table`
		WHERE `$primaryKey` = '$primaryKeyValue'
		AND `$primaryKey2` = '$primaryKey2Value'";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la ligne article" );
			
		if( !$rs->RecordCount() )	
			return "";
			
		$html ="<ul>";
		
		while( !$rs->EOF() ){
			
			$query2 = "
			SELECT zipcode, city, idstate
			FROM buyer
			WHERE idbuyer =(SELECT idbuyer FROM estimate WHERE `$primaryKey` = '$primaryKeyValue')";		
			$rs2 = $db->Execute( $query2 );
			
			$idstate=$rs2->fields("idstate");
			$query3 = "
			SELECT name$lang
			FROM state
			WHERE idstate =$idstate";
			$rs3 = $db->Execute( $query3 );
			
			$quantity 	= $rs->fields( "quantity" );
			$reference 	= $rs->fields( "reference" );
			$summary 	= $rs->fields( "summary" );
			$designation= $rs->fields( "designation" );
			$zipcode = $rs2->fields( "zipcode" );
			$city = $rs2->fields( "city" );
			$state = $rs3->fields( "name$lang" );

			if( !empty( $summary ) ){
				$html = "<ul /><li style=\"font-size: x-small; font-family: Verdana;FONT-SIZE: 10pt;\" /><b />$summary</b><br />";
				$summary2=$summary."<br />";
				if (!preg_match($designation,$summary2)){
					$designationmod=str_replace($summary2,"",$designation);
					$html .= "$designationmod ";
				}else{
					$html .= "$designation ";
				}
			}else{
				$html = "<li style=\"font-size: x-small; font-family: Verdana;FONT-SIZE: 10pt;\" />$designation </li>";
			}
		
			$html .= "<br /><br /><li style=\"font-size: x-small; font-family: Verdana;FONT-SIZE: 10pt;\" /><b />Quantité : $quantity</b></li>" .
					 "<li style=\"font-size: x-small; font-family: Verdana;FONT-SIZE: 10pt;\" /><b />Lieu de livraison :</b> $zipcode&nbsp;$city&nbsp;$state<br /></li>";
			
			$rs->MoveNext();
			
		}
		
		$html .= "</ul>";
		
		return $html;
		
	}
	//----------------------------------------------------------------------------------------
	
	/**
	 * Définit et charge le modèle de mail à utiliser
	 * @param string $identifier l'identifiant du modèle de mail à utiliser
	 * @return void
	 */
	public function setTemplate( $identifier ){
		
		$db = DBUtil::getConnection();
		
		$query = "
		SELECT idmail_template
		FROM mail_template
		WHERE identifier LIKE '$identifier'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le modèle de mail" );
		
		$idmail_template = $rs->fields( "idmail_template" );
			
		$this->loadTemplate( $idmail_template );
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Vérifie que tous les marqueurs ont été initialisés correctement
	 * @return bool true si tous les marqueurs ont été initialisés correctement
	 */
	private function checkTagDefinitions(){
	
		reset( $this->tags );
		
		foreach( $this->tags as $menu => $tags ){
		
			$identifier = $this->getIdentifier( $menu );
			
			if( $identifier === false ){
				
				trigger_error( "MailTemplateEditor : identifiant inconnu pour : $menu)" );
				return false;
				
			}
				
			if( !isset( $this->tagDefinitions[ $identifier ] ) || empty( $this->tagDefinitions[ $identifier ] ) ){
				
				trigger_error( "MailTemplateEditor : marqueur non initialisé ( template :{$this->identifier}, marqueur : $identifier)" );
				return false;
			}
			
		}
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------------------
	/**
	 * Retourne l'identifiant du menu
	 * @param string $menu le nom du menu
	 * @return mixed string l'identifiant du menu, false en cas de problème
	 */
	private function getIdentifier( $menu ){
	
		switch( strtoupper( $menu ) ){
		
			case "BUYER" :
			case "COMMERCIAL" :
			case "ESTIMATE" :
			case "ORDER" :
			case "INVOICE" :
			case "SUPPLIER" :
			case "PRODUCT" :
			case "BUYER_PRODUCT_INFO" :
			case "UTIL" :
			case "ADMIN" :
			case "PASSWORD" :
			case "TENDER" :
			case "CREDIT" : 
			case "SPONSORSHIP_GUEST" :
			case "SPONSORSHIP_SPONSOR" :
			case "GIFT_TOKEN" : 
			case "LITIGATION" : 
			case "PDF_DOWNLOAD" : 
			case "REPAYMENT" :
			case "RETURN_TO_SENDER" :
			case "DELIVERER" :
			case "CARRIER_CALLS_FOR_TENDER" :
			case "CARRIAGE" :
			case "REMOTE_ESTIMATE" :  				return strtoupper( $menu );
			case "ORDER_SUPPLIER" : 				return "SUPPLIER_ORDER";
			case "PRODUITS_ECHEANCIER" : return "PRODUITS_ECHEANCIER";
			default : 								return false;
			
		}
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Affiche un aperçu du mail
	 * @return void
	 */
	public function preview(){
		
		$this->previewAvailable = true;
		
		if( !$this->idmail_template || !isset( $this->bodies[ $this->language ] ) )
			return "<p class=\"msg\">Aucun modèle de mail défini</p>";
			
			foreach( $this->tags as $menu => $tags ){
			
				$identifier = $this->getIdentifier( $menu );
				
				switch( $identifier ){
				
					case "BUYER" 				: $this->setUseBuyerTags( $this->getRandomKeyValue( "buyer", "idbuyer" ) ); 							break;
					case "PASSWORD" 			: $this->setUseBuyerPassword(); 																		break;
					case "COMMERCIAL" 			: $this->setUseCommercialTags( $this->getRandomKeyValue( "user", "iduser" ) ); 							break;
					case "ESTIMATE" 			: $this->setUseEstimateTags( $this->getRandomKeyValue( "estimate", "idestimate" ) ); 					break;
					case "ORDER" 				: $this->setUseOrderTags( $this->getRandomKeyValue( "order", "idorder" ) ); 							break;
					case "INVOICE" 				: $this->setUseInvoiceTags( $this->getRandomKeyValue( "billing_buyer", "idbilling_buyer" ) ); 			break;
					case "SUPPLIER" 			: $this->setUseSupplierTags( $this->getRandomKeyValue( "supplier", "idsupplier" ) ); 					break;
					case "SUPPLIER_ORDER" 		: $this->setUseSupplierOrderTags( $this->getRandomKeyValue( "order_supplier", "idorder_supplier" ) ); 	break;
					case "PRODUCT" 				: $this->setUseProductTags( $this->getRandomKeyValue( "product", "idproduct" ) ); 						break;
					case "BUYER_PRODUCT_INFO"	: $this->setUseBuyerProductInfoTags( $this->getRandomKeyValue( "title", "title_{$this->language}" ), $this->getRandomKeyValue( "contact", "lastname" ), $this->getRandomKeyValue( "contact", "mail" ), $this->getRandomKeyValue( "buyer", "company" ), $this->getRandomKeyValue( "contact", "phonenumber" ), $this->getRandomKeyValue( "contact", "faxnumber" ) );	break;
					case "TENDER" 				: $this->setUseTenderTags( $this->getRandomKeyValue( "estimate","idestimate") ); 						break;
					case "SPONSORSHIP_GUEST" 	: $this->setUseSponsorshipGuestTags( "XXXXXXX@XXX.XX","XXXXXXXX" ); 									break;
					case "SPONSORSHIP_SPONSOR" 	: $this->setUseSponsorshipSponsorTags( $this->getRandomKeyValue( "buyer", "idbuyer" ) );				break;
					case "GIFT_TOKEN" 			: $this->setUseGiftTokenTags( $this->getRandomKeyValue( "gift_token", "idtoken" ) ); 					break;
					case "CREDIT" 				: $this->setUseCreditTags( $this->getRandomKeyValue( "credits","idcredit") ); 							break;
					case "RETURN_TO_SENDER" 	: $this->setUseReturnToSenderTags( $this->getRandomKeyValue( "return_to_sender", "idorder_supplier"), 1 ); break;
					case "REPAYMENT" 			: $this->setUseRepaymentTags( $this->getRandomKeyValue( "repayments", "idrepayment") ); 				break;
					case "LITIGATION" 			: $this->setUseLitigationTags( $this->getRandomKeyValue( "litigations", "idlitigation") ); 				break;
					case "REMOTE_ESTIMATE" 		: $this->setUseRemoteEstimateTags( $this->getRandomKeyValue( "source", "idsource"), $this->getRandomKeyValue( "category", "idcategory") ); break;
					case "CARRIER_CALLS_FOR_TENDER":$this->setUseCarrierCallsForTenderTags(); break;
					case "CARRIAGE"				:$this->setUseCarriageTags($this->getRandomKeyValue( "carriage", "idcarriage") ); 						break;
					case "DELIVERER" 			: 
						
							include_once( dirname( __FILE__ ) . "/Supplier.php" );
							$this->setUseDelivererTags( new Supplier( $this->getRandomKeyValue( "supplier", "idsupplier") ) ); 					
							break;
							
					case "UTIL" 				:			
					case "ADMIN" 				:	break;
					case "PRODUITS_ECHEANCIER" 				: $this->setUseecheanceTags( $this->getRandomKeyValue( "product_term", "idproduct_term" ) ); break;
					
					default : 					die( "Unknown Identifier : $identifier" );
				
				}
				
			}
			
			if( !$this->previewAvailable )
				return false;
				
			return $this->unTagBody();
			
	}
	
	//----------------------------------------------------------------------------------------

	/**
	 * Récupère une valeur aléatoire pour la clé primaire $key dans la table $table
	 * @param string $table la table où rechercher une valeur aléatoire de la clé primaire $key
	 * @param string $key le nom de la clé primaire pour laquelle on souhaite obtenir une valeur aléatoire
	 * @return mixed la valeur aléatoire
	 */
	private function getRandomKeyValue( $table, $key ){
	
		
		
		$db =& DBUtil::getConnection();
		
		$query = "
		SELECT `$key` AS RandomValue
		FROM `$table`
		ORDER BY RAND()
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer une valeur aléatoire pour la clé '$key' dans la table '$table'" );
			
		if( !$rs->RecordCount() ){
		
			$this->previewAvailable = false;
			
			return 0;

		}
		
		return $rs->fields( "RandomValue" );
		
	}
	
	//----------------------------------------------------------------------------------------	
	/**
	 * Correction des liens relatifs en absolu
	 * @param string $html le corps du mail
	 * @return le lien formatté
	 */
	private function setAbsoluteURIs( $html ){
	
		global $GLOBAL_START_URL;

		$patterns = array(
		
			//"href=\"../([^\"]*)\"" 	=> "href=\"$GLOBAL_START_URL/\\1\"",
			//"src=\"../([^\"]*)\"" 	=> "src=\"$GLOBAL_START_URL/\\1\""
            "/href=\"..\/([^\"]*)\"/" 	=> "href=\"$GLOBAL_START_URL/\\1\"",
			"/src=\"..\/([^\"]*)\"/" 	=> "src=\"$GLOBAL_START_URL/\\1\""
			
		);
		
		foreach( $patterns as $pattern => $replacement )
			$html = preg_replace( $pattern, $replacement, $html );
		return $html;
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Défini le mot de passe du client
	 * @param string $password le mot de passe du client
	 * @return void
	 */
	public function setUseBuyerPassword( $password ){
	
		if( isset( $this->tags[ "buyer" ] ) 
			&& isset( $this->tags[ "buyer" ][ "idbuyer" ] )
			&& !empty( $this->tags[ "buyer" ][ "idbuyer" ] ) )
				$this->tags[ "buyer" ][ "password" ] = $password;
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Récupère le(s) lien(s) pour le suivi de colis de la commande
	 * @param $idorder le numéro de la commande
	 * @return string le html des liens pour le suivi de colis
	 */
	private function getPackageInfos( $idorder ){
		
		//On récupère les numéros de colis pour chaque BL
		include_once( dirname( __FILE__ ) . "/Order.php" );
		
		$packages = Order::getOrderPackages($idorder);
		
		$html = "";
		
		if( is_array($packages) ){
			
			foreach( $packages as $package ){
			
				$html .= "<a href=\"http://www.coliposte.net/particulier/suivi_particulier.jsp?colispart=$package\">Suivi colis n° $package</a>";
				
				$html .= "<br />";
			
			}
		
		}
		
		return $html;
		
	}
	
	//----------------------------------------------------------------------------------------
	
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------------------
	
}

?>