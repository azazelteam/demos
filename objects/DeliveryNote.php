<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object bons de livrasion
 */


include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/SecuredDocument.php" );
include_once( dirname( __FILE__ ) . "/DeliveryNoteItem.php" );
include_once( dirname( __FILE__ ) . "/Delivery.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
include_once( dirname( __FILE__ ) . "/Supplier.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/ForwardingAddress.php" );



class DeliveryNote extends DBObject implements Delivery, SecuredDocument{

	//----------------------------------------------------------------------------	
	/**
	 * @var int le numéro de BL en franglish
	 * @access private
	 */
	private $idbl_delivery;
	/**
	 * @var EditableAddress $forwardingAddress
	 */
	private $forwardingAddress;
	/**
	 * @var Supplier $supplier
	 * @access private
	 */
	private $supplier;
	/**
	 * @var Customer $customer
	 * @access private
	 */
	private $customer;
	
	/**
	 * @var Salesman $salesman
	 * @access private
	 */
	private $salesman;
	
	//----------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param int $dl_delivery le numéro du Bon de Livraison
	 */
	function __construct( $idbl_delivery ){
		
		$this->idbl_delivery 		=  $idbl_delivery ;
		
		parent::__construct( "bl_delivery", false, "idbl_delivery",  $idbl_delivery  );
		
		$this->salesman = new Salesman( $this->get( "iduser" ) );
		$this->customer = new Customer( $this->get( "idbuyer" ), $this->get( "idcontact" ) );
		$this->supplier = new Supplier( $this->get( "idsupplier" ) );
		$this->forwardingAddress = $this->get( "iddelivery" ) ? new ForwardingAddress( $this->get( "iddelivery" ) ) : $this->customer->getAddress();

		$this->setItems();
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "DeliveryNoteItem" );
		
		$rs =& DBUtil::query( "SELECT idrow FROM bl_delivery_row WHERE idbl_delivery = '{$this->idbl_delivery}' ORDER BY idrow ASC" );

		while( !$rs->EOF() ){
		
			$this->items->add( new DeliveryNoteItem( $this, $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/SecuredDocument#allowRead()
	 */
	public function allowRead(){ return $this->allowCustomerRead() || $this->allowUserRead(); }
	
	//----------------------------------------------------------------------------
    /**
     * Retourne true si le client identifié est autorisé à consulter le document
     * @access private
     * @return bool
     */
    private function allowCustomerRead(){
	
    	include_once( dirname( __FILE__ ) . "/Session.php" );
		return Session::getInstance()->getCustomer() && Session::getInstance()->getCustomer()->getId() == $this->customer->getId();
		
    }
    
    //----------------------------------------------------------------------------
    /**
     * Retourne true si le commercial identifié est autorisé à consulter le document
     * @access private
     * @return bool
     */
    private function allowUserRead(){
    	
		if( User::getInstance()->getId() == $this->salesman->getId() 
			|| User::getInstance()->getId() == $this->customer->get( "iduser" )
			|| User::getInstance()->getHasAdminPrivileges() ) 
			return true;
		
		return false;
		
    }
    
    //----------------------------------------------------------------------------
    /**
     * (non-PHPdoc)
     * @see objects/SecuredDocument#allowEdit()
     */
    public function allowEdit(){ return User::getInstance()->getHasAdminPrivileges() || User::getInstance()->getId() == $this->salesman->getId(); }
    
    //----------------------------------------------------------------------------
	/**
	 * Retourne true si le commercial identifié à le droit de créer un document pour un client donné
	 * @static
	 * @access public
	 * @param int $idbuyer le n° de compte client
	 * @return bool
	 */
	public static function allowCreationForCustomer( $idbuyer ){

		return User::getInstance()->getHasAdminPrivileges() 
			|| User::getInstance()->getId() == DBUtil::getDBValue( "iduser", "buyer", "idbuyer", $idbuyer );
		
	}
	
	//----------------------------------------------------------------------------
	/**
     * Retourne numéro du BL
     * @access public
     * @return int
     */
	public function getId(){ return $this->idbl_delivery; }
	
	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return Supplier
     */
	public function &getSupplier(){ return $this->supplier; }
	
	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return Salesman
     */
	public function &getSalesman(){ return $this->salesman; }

	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return Customer
     */
	public function &getCustomer(){ return $this->customer; }

//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBObject#save()
	 */
	public function save(){
		
		if( !$this->allowEdit() ){
			
			trigger_error( "Appel de DBObject::save() non autorisé", E_USER_ERROR );
			return;
			
		}
				
		DBUtil::query( "DELETE FROM bl_delivery_row WHERE idbl_delivery = '{$this->idbl_delivery}'" );
		
		$it = $this->items->iterator();
	
		while( $it->hasNext() )
			$it->next()->save();
			
		parent::save();			
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Supprime un bon de livraison de la base de données
	 * @static
	 * @param int $idbl_delivery le numéro de BL à supprimer
	 * @return false
	 */
	public static function delete( $idbl_delivery ){

		if( DBUtil::getDBValue( "idbl_delivery", "billing_supplier_row", "idbl_delivery", $idbl_delivery ) )
			return false;

		if( DBUtil::getDBValue( "idbilling_buyer", "bl_delivery", "idbl_delivery", $idbl_delivery ) )
			return false;
			
		DBUtil::query( "DELETE FROM `bl_delivery` WHERE `idbl_delivery` = '" .  $idbl_delivery  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM `bl_delivery_row` WHERE `idbl_delivery` = '" .  $idbl_delivery  . "'" );
		DBUtil::query( "DELETE FROM `carriage` WHERE `idcarriage` = '" .  $idbl_delivery  . "' LIMIT 1" );

		return true;

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Itérateur
	 * @todo deprecated
	 * @return CArrayIterator<DeliveryNoteItem>
	 */
	public final function iterator(){ return $this->items->iterator(); }

	//----------------------------------------------------------------------------
	/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public final function getItemCount() { return $this->items->size(); }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le $index-ième élément du panier
	 * @return DeliveryNoteItem
	 */
	public final function &getItemAt( $index ) { return $this->items->get( $index ); }

	//----------------------------------------------------------------------------
	/**
	 * Assigne un commercial donné à la commande
	 * @param Salesman $salesman
	 * @return void
	 */
	public function setSalesman( Salesman $salesman ){
		
		$this->set( "iduser", $salesman->getId() );
		$this->salesman = $salesman;
		
	}

	//----------------------------------------------------------------------------
	/**
     * Créer une nouvelle ligne article
     * @access protected
     * @return DeliveryNoteItem une ligne article
     */
	protected function &createItem(){
		
		$idrow = $this->items->size() + 1;
		
		DBUtil::query( "INSERT INTO bl_delivery_row( idbl_delivery, idrow ) VALUES( '{$this->idbl_delivery}', '$idrow' )" );

		$this->items->add( new DeliveryNoteItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
     * Ajoute une ligne article d'une commande fournisseur
     * @param SupplierOrderItem $supplierOrderItem la ligne article à ajouter
     * @return void
     */
	public function addSupplierOrderItem( SupplierOrderItem &$supplierOrderItem ){
		
		$item =& $this->createItem();
		
		$designation = $supplierOrderItem->get( "designation" );
		$extraFields = array( 
		
			"site" 	=> "Dépôt",
			"span" 	=> "Rayon", 
			"store" => "Allée"
		
		);
		
		$rs =& DBUtil::query( "SELECT `" . implode( "`,`", array_keys( $extraFields ) ) . "` FROM detail WHERE idarticle = '" . $supplierOrderItem->get( "idarticle" ) . "' LIMIT 1" );

		foreach( $extraFields as $extraField => $label ){
			
			if( strlen( $rs->fields( $extraField ) ) )
				$designation .= "<br /><b>$label</b> : " . $rs->fields( $extraField );
		
		}
			
		$item->set( "idarticle", 		$supplierOrderItem->get( "idarticle" ) );
		$item->set( "idorder_row", 		$supplierOrderItem->get( "idorder_row" ) );
		$item->set( "reference", 		$supplierOrderItem->get( "reference" ) );
		$item->set( "ref_supplier", 	$supplierOrderItem->get( "ref_supplier" ) );
		$item->set( "unit", 			$supplierOrderItem->get( "unit" ) );
		$item->set( "quantity", 		$supplierOrderItem->get( "quantity" ) );
		$item->set( "lot", 				$supplierOrderItem->get( "lot" ) );
		$item->set( "designation", 		$designation );
		$item->set( "summary", 			$supplierOrderItem->get( "summary" ) );
		$item->set( "width", 			$supplierOrderItem->get( "width" ) );
		$item->set( "length", 			$supplierOrderItem->get( "length" ) );
		$item->set( "area", 			$supplierOrderItem->get( "area" ) );
		$item->set( "comment", 			$supplierOrderItem->get( "comment" ) );
		$item->set( "availability_date",$supplierOrderItem->get( "availability_date" ) );
		$item->set( "useimg", 			$supplierOrderItem->get( "useimg" ) );
			
	}
	
	//----------------------------------------------------------------------------
	/**
     * Ajoute une ligne article
     * @param CArticle $article
     * @param int $quantity
     * @return void
     */
	public function addArticle( CArticle $article, $quantity = 1 ){
		
		$item =& $this->createItem();

		$designation = "<b>" . $article->get( "summary_1" ) . "</b><br />" . $article->get( "designation_1" );
		$extraFields = array( 
		
			"site" 	=> "Dépôt",
			"span" 	=> "Rayon", 
			"store" => "Allée"
		
		);
		
		$rs =& DBUtil::query( "SELECT `" . implode( "`,`", array_keys( $extraFields ) ) . "` FROM detail WHERE idarticle = '" . $item->get( "idarticle" ) . "' LIMIT 1" );

		foreach( $extraFields as $extraField => $label ){
			
			if( strlen( $rs->fields( $extraField ) ) )
				$designation .= "<br /><b>$label</b> : " . $rs->fields( $extraField );
		
		}
			
		$item->set( "idarticle", 			$article->getId() );
		$item->set( "reference", 			$article->get( "reference" ) );
		$item->set( "ref_supplier", 		$article->get( "ref_supplier" ) );
		$item->set( "unit", 				$article->get( "unit" ) );
		$item->set( "quantity", 			$quantity );
		$item->set( "lot", 					$article->get( "lot" ) );
		$item->set( "designation", 			$designation );
		$item->set( "summary", 				$article->get( "summary_1" ) );
		$item->set( "useimg", 				1 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Supprime les lignes zarticles
	 * @param int $index l'identifiant de la ligne article à supprimer
	 * @return void
	 */
	public function removeItems(){ $this->items = new ArrayList( "DeliveryNoteItem" ); }
	
	//----------------------------------------------------------------------------
	/**
	 * @param int $index
	 * @return void
	 */
	public function removeItemAt( $index ){ 
		
		$this->items->remove( $this->items->get( $index ) ); 
	
		/* réindexation */
		
		$index = 1;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$it->next()->set( "idrow", $index++ );//$it->next()->setIdRow( $index++);
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Définit l'adresse de livraison à utiliser
	 * @param Address $forwardingAddress une adresse de livraison
	 * @return void
	 */
	public function setForwardingAddress( Address $forwardingAddress ){
		
		$this->forwardingAddress =& $forwardingAddress;
		$this->set( "iddelivery", $forwardingAddress instanceof CustomerAddress ? 0 : $forwardingAddress->getId() );
			
	}

	//----------------------------------------------------------------------------
	/**
	 * Retourne l'adresse de livraison utilisée
	 * @return EditableAddress une référence vers l'adresse de livraison
	 */
	public function &getForwardingAddress(){ return $this->forwardingAddress; }

	//----------------------------------------------------------------------------
	/**
	 * @return ArrayList<DeliveryNoteItem>
	 */
	public final function &getItems(){ return $this->items; }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le poids total
	 * @return float
	 */
	public function getWeight(){ 
		
		$weight = 0.0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			$weight += $item->get( "weight" ) * $item->get( "quantity" );
			
		}
		
		return $weight; 
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
}//EOC

?>
