<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object formulaires de recherche et résultat de la recherche (stdform)
 */
 

include_once('adm_form.php');

class ADMSEARCH extends ADMFORM{

	//--------------------------------------------------------------------------------------
	
	private $current_page;
	private $newsearch = false;
	private $enable_sort = false;

	//--------------------------------------------------------------------------------------
	
	/**
	 * Contructeur
	 * @param ADMTABLE $tableobj Object ADMTABLE
	 * @param bool $enable_sort tri autorisé
	 */
	function __construct(ADMTABLE &$tableobj, $enable_sort = false ) {
		
		global $ScriptName; // Pour pouvoir changer la valeur en amont (popup)
		
		$this->table = &$tableobj;
		$this->current_page = $ScriptName;
		$this->enable_sort = $enable_sort;
		
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Affichage du formulaire de recherche
	 * @param int $CatSel 1->recherche de catégorie, 0->recherche de produit, -1->rien
	 * @param string $Str_FormName Nom du formulaire
	 * @return void
	 */
	public function display_search_form($CatSel=-1,$Str_FormName='searchform',$Array_ExtraFields=false){
		global $GLOBAL_START_URL, $GLOBAL_START_PATH;
		
		$Array_FieldsDisplayed = $this->table->GetFieldsForSearching();
		if($Array_ExtraFields) $Array_FieldsDisplayed += $Array_ExtraFields;
		$n=count($Array_FieldsDisplayed);
		
?>
	<!-- BEGIN search form -->
	<form action="<?php echo $this->current_page ?>" method="post" name="<?php echo $Str_FormName ?>" style="margin:0; padding:0;">
		<table class="searchTable">
			<?php if( $CatSel != -1 ){ ?>
			<tr>
				<th>
					<a href="javascript:catpopup(<?php echo $CatSel ?>)">
						Sélection par arborescence
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/cat_close.jpg" style="border-width:0px;" />
					</a>
				</th>
			</tr>
			<?php } ?>
			<?php foreach( $Array_FieldsDisplayed as $arf){ ?>
			<tr>
				<th>
					<input type="hidden" name="k[]" value="<?php echo base64_encode( serialize( $arf[ "fieldname" ] ) ) ?>" />
					Par <?php echo Dictionnary::translate($arf["fieldname"]) ?>
				</th>
			</tr>
			<tr>
				<td>
				<?php 
				
					if( strlen( $arf[ "help" ] ) > 4 && substr( $arf[ "help" ], 0, 4 ) == "enum" ) {
					
						$pattern = "/[']{1}([^']+)[']{1}/i";
						$matches = array();
						preg_match_all( $pattern, $arf[ "help" ], $matches );
							
						if( !count( $matches ) || !count( $matches[ 1 ] ) )
							trigger_error( "Erreur de syntaxe pour la description du champ '" . $arf[ "fieldname" ] . "'", E_USER_ERROR );
					
						echo "
						<select name=\"" . $arf[ "fieldname" ] . "\">
							<option value=\"\">-</option>";
						
						foreach( $matches[ 1 ] as $match ){
							
							$selected = isset( $_POST[ $arf[ "fieldname" ] ] ) && $_POST[ $arf[ "fieldname" ] ] == $match ? " selected=\"selected\"" : "";
							echo "<option value=\"" . htmlentities( $match ) . "\"$selected>" . htmlentities( Dictionnary::translate( $match ) ) . "</option>";
							
						}
						
						echo "</select>";
						
					}
					else switch( $arf[ "help" ] ){
						
						case "cpopup" :
						
							?>
							<input type="text" name="<?php echo $arf[ "fieldname" ] ?>name" readonly="readonly" class="textInput" style="width:99%;" />
							<input type="hidden" name="<?php echo $arf[ "fieldname" ] ?>" value="" />
							<img type="image" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/cat_nochild.jpg" name="category_sel" value=" ? " onclick="javascript:window.open('<?php echo $GLOBAL_START_URL ?>/formbase/category_select.php?target=<?php echo $arf[ "fieldname" ] ?>&amp;targetform=<?php echo $Str_FormName ?>&amp;actualvalue=0','', 'height=120,width=110,resize,scrollbars=1');" style="border-width:0px;" />
							<?php 
						
							break;
							
					
						case "password" :
						
							?>
							<input type="password" name="<?php echo $arf[ "fieldname" ] ?>" class="textInput" />
							<?php
							
							break;
						
						case "date" :
						case "Calendrier" :
						
							include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
							
							?>
							<input type="password" name="<?php echo $arf[ "fieldname" ] ?>" id="<?php echo $arf[ "fieldname" ] ?>Calendar" class="textInput" />
							<?php
							
							DHTMLCalendar::calendar($arf[ "fieldname" ]. "Calendar", false );
							
							break;
						
						default :
						
							if( $arf[ "search_select_table" ] == "" ){ 
						
								?> 
								<input type="text" name="<?php echo $arf[ "fieldname" ] ?>" value="" class="textInput" style="width:99%;" />
								<?php 
							
							}
							else $this->SelectField( $arf, "search" );
					
					}
	
				?>
				</td>
			</tr>
			<?php } ?>
		</table>
		<input type="submit" class="blueButton" name="search" value="Rechercher" style="margin:5px;" />
	</form>
	<!-- END search form -->
<?php
		
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Affichage du formulaire du résultat de la recherche
	 * @param array $Array_DisplayFields Les champs à afficher
	 * @param int $Int_StartP L'index du premier produit à afficher (pagination)
	 * @param int $Int_LimitP L'index du dernier produit à afficher
	 * @param array $ExtraFields Champs 'hidden' à ajouter au formulaire
	 * @return int Le nombre total d'enregsitrements trouvés
	 */
	public function display_search_result(array $Array_DisplayFields , $Int_StartP=0, $Int_LimitP = -1,$ExtraFields=false,$Sort='',$ExternFields=false){
		
		global $GLOBAL_START_URL, $limitDisplay;
		
		if (count($Array_DisplayFields)=="0") // Si nous n'avons pas de champs
			 trigger_error("Vous devez passer des fields pour les voir dans la table $Str_NameTable", E_USER_ERROR);
			
		$Str_ActionName = $this->current_page;
		$Str_NameTable = "`" . $this->table->getTableName() . "` A";
		$descfields = $this->table->GetDesign(); //get fields from desc_field
		$n=count($descfields);
	
		reset($Array_DisplayFields);
		$Str_FieldsView=''; //liste des champs pour la commande sql
		$Array_LocalFields= array();
		$Array_TableAliases = array( 0 => 'A');
		// récupération des critères depuis la session	
		if( isset($_SESSION[$Str_ActionName]['search_select']) ) {
			$Str_Condition = $_SESSION[$Str_ActionName]['search_select'];
		}
		else $Str_Condition=' WHERE 1';
		
		
		$nn=0;
		for($i=0;$i<$n;$i++)
			{
				$r = array_search($descfields[$i]['fieldname'],$Array_DisplayFields);
				if($r !== false) {
					// on ne tient pas compte de if($descfields[$i]['display_avalaible'])
					if($descfields[$i]['append_select_table'] != '') {
						
						$arr = explode(':',$descfields[$i]['append_select_table']);
					
						$t = chr(66+$r);
						$Str_NameTable .= ', `'.$arr[0].'` '.$t;
						/**/$Str_FieldsView.= ', A.`'.$descfields[$i]['fieldname'].'`, '.$t.'.`'.$arr[1].'` AS `' . $descfields[$i]['fieldname'] . '`';
						/**/$Array_LocalFields[$r] = $descfields[$i]['fieldname']; //$arr[1]; 

						$Array_TableAliases[$r]=$t;
						$Str_Condition .= ' AND A.`'.$descfields[$i]['fieldname'].'`='.$t.'.`'.$arr[2].'`';
					
					} else  {
						$t='A';
						$Str_FieldsView.=', '.$t.'.`'.$descfields[$i]['fieldname'].'`';
						$Array_LocalFields[$r] = $descfields[$i]['fieldname'];
					}
					$nn++;
				} else $Str_FieldsView.= ', A.`'.$descfields[$i]['fieldname'].'`' ;
				
			}
		$Str_FieldsView=substr($Str_FieldsView,1); // drop first 
		
		//vérification si champs corrects 
		if (count($Array_DisplayFields) != $nn) {
				 trigger_error("Vous devez remplir la table DESC_FIELD correctement pour $Str_NameTable", E_USER_ERROR);
			}
	
			//Tri
			$Sens=' ASC';
			if($Sort) {
				$r = array_search($Sort,$Array_DisplayFields);
				if($r !== false) $Sort = $Array_LocalFields[$r];
				if( strstr($_SESSION[$Str_ActionName]['search_order'],$Sort.' ')!==false) {
					// la colonne est déjà triée, on change le sens du tri
					if( strstr($_SESSION[$Str_ActionName]['search_order'],'ASC')!==false) $Sens=' DESC';
				}
				$ta='';if(isset($Array_TableAliases[$r])) $ta=$Array_TableAliases[$r].'.';
				$_SESSION[$Str_ActionName]['search_order']= ' ORDER BY '.$ta.$Sort.$Sens;
			}
			elseif( !isset($_SESSION[$Str_ActionName]['search_order']) || $_SESSION[$Str_ActionName]['search_order']=='') 
				$_SESSION[$Str_ActionName]['search_order']=' ORDER BY '.$Array_TableAliases[0].'.'.$Array_LocalFields[0].' ASC';
				// par défaut, on trie sur la première colonne en ordre coroissant
		
		
		$sql_search = "SELECT DISTINCT $Str_FieldsView FROM $Str_NameTable $Str_Condition".$_SESSION[$Str_ActionName]['search_order'];
	
		$rs = DBUtil::getConnection()->Execute($sql_search);
		if(!$rs) {
			trigger_error("SQL ERROR : $sql_search" , E_USER_ERROR);
		}

		if ($rs) 	
			$max = $Int_RecordCount = $rs->RecordCount();
			
		if( !$Int_RecordCount )
			echo "<p style=\"text-align:center;\">" . Dictionnary::translate( "Aucun résultat" ) . "</p>";
			
		$Int_LimitP = $Int_RecordCount;
		if( $Int_LimitP != -1 ){
			if( !isset($_POST['Int_StartP']) ){
				if($Int_StartP < 0 ) $Int_StartP = max($max-$Int_LimitP,4);
				elseif( isset($_SESSION[$Str_ActionName]['search_startpage']) ) $Int_StartP  = $_SESSION[$Str_ActionName]['search_startpage'];
				else $Int_StartP=0;
				if($this->newsearch) $Int_StartP=0;
			} else {
				if($Int_StartP < 0 ) $Int_StartP = max($max-$Int_LimitP,0);
			}
			$_SESSION[$Str_ActionName]['search_startpage'] = $Int_StartP;
			$rs->Move($Int_StartP);
			$max= $Int_LimitP;
		}
	
		if ( $Int_RecordCount > 0 ){
			
?>
	<!-- BEGIN search result -->
	<?php $this->BeginPostForm( $Str_ActionName, "resultform" ); ?>
<?php
		
		if( $ExtraFields ){
			
			foreach( $ExtraFields as $k => $v ){
				
?>
	<input type="hidden" name="<?php echo $k ?>" value="<?php echo $v ?>" />
<?php
				
			}
			
		}
		
?>
	<table class="dataTable">
		<thead>
			<tr>
			<?php for( $ii = 0 ; $ii < count( $Array_LocalFields ) ; $ii++ ){ ?>
				<th class="filledCell" style="text-align:center; white-space:nowrap;">&nbsp;
					<?php echo Dictionnary::translate( $Array_LocalFields[ $ii ] ) ?><br />
					<?php if( $this->enable_sort ){ ?>
						<?php if( strstr( $_SESSION[ $Str_ActionName ][ "search_order" ], $Array_LocalFields[ $ii ] . " " ) !== false ){ ?>
							<?php if( strstr( $_SESSION[ $Str_ActionName ][ "search_order" ], "DESC" ) !== false ){ ?>
							<input type="image" name="Tri<?php echo $ii ?>" value="Tri" style="border-style:none; margin-left:4px; vertical-align:text-bottom;" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" />
							<?php }else{ ?>
							<input type="image" name="Tri<?php echo $ii ?>" value="Tri" style="border-style:none; margin-left:4px; vertical-align:text-bottom;" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" />
							<?php } ?>
						<?php }else{ ?>
						<input type="image" name="Tri<?php echo $ii ?>" value="Tri" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png"  style="border-style:none; vertical-align:text-bottom; margin-left:4px;" />
						<?php } ?>
					<?php } ?>
				</th>
<?php
					
					$foreign_label = $this->getForeignHeading( $Array_LocalFields[ $ii ] );
					
					if( $foreign_label !== false ){
						
?>
				<th style="white-space:nowrap;">&nbsp;<?php echo $foreign_label ?></th>
				<?php } ?>
			<?php } ?>
			</tr>
		</thead>
<?php
		
		for( $i = 0 ; $i < $max ; $i++ ){
			
			$rowColor = $i % 2 ? "#f5f5f5" : "#ffffff"; /* alternance de couleur chaque ligne */
			//Cellule résultat:	echo "|i=$i|";
			
			if( $rs->EOF )
				break;
			
?>
		<tbody>
			<tr>
				<td class="lefterCol" style="background-color:<?php echo $rowColor ?>">
<?php
			
			$tablekeys = $this->table->getTableAttribute( "tablekeys" );
			
			foreach( $tablekeys as $k) 
				$Array_LocalKeyValues[$k]=$rs->fields[$k];
			
			$value = base64_encode( serialize( $Array_LocalKeyValues) );
			$checked = "";
			
			if( isset( $_POST[ "k" ] ) && is_array( $_POST[ "k" ] ) ){
				
				$postdata = $_POST[ "k" ];
				
				$count = 0;
				
				while( $count < count( $postdata ) ){
					
					if( stripslashes( $postdata[ $count ] ) == $value )
						$checked = " checked=\"checked\"";
					
					$count++;
					
				}
				
			}
			
			$style = empty( $checked ) ? "" : " style=\"font-weight:bold;\"";
			
			if( !isset( $limitDisplay ) || !$limitDisplay )
				echo "\n<input type=\"checkbox\" name=\"k[]\" value=\"$value\"$checked style=\"vertical-align:middle;\" />\n";
			
			$href = "?modify&amp;nbDisp=$Int_LimitP&amp;k=" . base64_encode( serialize( $Array_LocalKeyValues ) );

			if( $ExtraFields )
				foreach( $ExtraFields as $key => $value )
					$href .= "&amp;" . urlencode( $key ) . "=" . urlencode( $value );

			if( !isset( $limitDisplay ) || !$limitDisplay )
				echo "<a class=\"normalLink\" href=\"$href\"$style>";
				
			echo $rs->fields[$Array_LocalFields[0]];
			
			if( !isset( $limitDisplay ) || !$limitDisplay )
				echo "</a>";
			
			echo "\n   </td>";
			
			// field values
			for( $ii = 1 ; $ii < count( $Array_LocalFields ) ; $ii++ ){
				
				if( $ii == count( $Array_LocalFields ) - 1 )
					$cssClass = " class=\"righterCol\"";
				else
					$cssClass = "";
				
				echo "\n   <td$cssClass style='background-color:".$rowColor.";'>";
				
				$href = "?modify&amp;nbDisp=$Int_LimitP&amp;k=" . base64_encode( serialize( $Array_LocalKeyValues ) );
				
				if( $ExtraFields )
					foreach( $ExtraFields as $key => $value )
						$href .= "&amp;" . urlencode( $key ) . "=" . urlencode( $value );

				if( !isset( $limitDisplay ) || !$limitDisplay )
					echo "<a class=\"normalLink\" href=\"$href\">";
				
				$help = $this->table->get_help( $Array_LocalFields[ $ii ] );
				
				if( strlen( $help ) > 4 && substr( $help, 0, 4 ) == "enum" )
					$innerHTML = htmlentities( Dictionnary::translate( $rs->fields[ $Array_LocalFields[ $ii ] ] ) );
				else switch( $help ){
					
					case "Calendrier":
					case "date" : 
					
						$innerHTML = usDate2eu( substr( $rs->fields[$Array_LocalFields[$ii]], 0, 10 ) );
						break;
						
					case "cpopup" : 
					
						$lang = User::getInstance()->getLang();
						$innerHTML = $rs->fields[ $Array_LocalFields[ $ii ] ] . " - " .htmlentities( DBUtil::getDBValue( "name$lang", "category", "idcategory", $rs->fields[ $Array_LocalFields[ $ii ] ] ) );
						break;
						
					case "password" :
					
						$innerHTML = "******";
						break;
						
                     //default : 	$innerHTML = utf8_decode( $rs->fields[ $Array_LocalFields[ $ii ] ] );	
					//default : 	$innerHTML = htmlentities( $rs->fields[ $Array_LocalFields[ $ii ] ] );
                    //default : 	$innerHTML =  Util::doNothing($rs->fields[ $Array_LocalFields[ $ii ] ]);
                     default:
                     $innerHTML = $rs->fields[ $Array_LocalFields[ $ii ] ];
                      $Str_Encodage = mb_detect_encoding($innerHTML);                   

                   switch( $Str_Encodage ){ 

                     case 'UTF-8': 
                       //$innerHTML = mb_convert_encoding($innerHTML,"ISO-8859-1");                       
                       $innerHTML = $innerHTML;
                       break; 

                      case 'ASCII': 

                       $innerHTML =utf8_encode($innerHTML); 

                       break; 

                   } 
				}
				//echo $innerHTML;	
				echo stripslashes( $innerHTML );
				
				if( !isset( $limitDisplay ) || !$limitDisplay )
					echo "</a>";
		
				//echo htmlentities ($con->Field($Array_LocalFields[$ii])) ; // for <b> like characters
				echo "</td>";
				
				$foreign_label = $this->getForeignLabel( $Array_LocalFields[ $ii ], $rs->fields[ $Array_LocalFields[ $ii ] ] );
	
				if( $foreign_label !== false ){
					
					?>
					<td<?php echo $style ?>>
						<?php echo htmlentities( $foreign_label ) ?>
					</td>
					<?php 
					
				}
				
			}
			
?>
			</tr>
<?php
			
			$rs->MoveNext();
			
		}
		
?>
	</table>
<?php if( !isset( $limitDisplay ) || !$limitDisplay ){ ?>
	<div>
		<input type="checkbox" name="select_all" onclick="selectAll(this);" style="vertical-align:middle;" />
		Tout <span id="selectAll">sélectionner</span>
		<script type="text/javascript">
		<!--
		function selectAll( elmt ){
			
			for( var i = 0 ; i <= <?php echo $Int_RecordCount ?> ; i++ )
				elmt.form.elements[i].checked = elmt.checked;
			
			if( elmt.checked )
				document.getElementById( 'selectAll' ).innerHTML = "désélectionner";
			else
				document.getElementById( 'selectAll' ).innerHTML = "sélectionner";
			
		}
		-->
		</script>
	</div>
	<div style="float:right; margin:3px 7px 0 0;">
		<input type="submit" name="delete_wanted" value="Supprimer la sélection" class="blueButton" />
	</div>
	<div class="clear"></div>
<?php } ?>
<?php
			
			$this->EndForm();
			
			echo "\n<!-- END search result -->\n";
			
		}
		
		return $Int_RecordCount;
		
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Affiche les boutons pour la pagination
	 * @param int $Int_StartP L'index du premier produit affiché
	 * @param int $Int_LimitP L'index du dermier produit affiché
	 * @param int $Int_NumRecords Le nombre total de produits
	 * @return void
	 */
	public function PositionButtons( $Int_StartP, $Int_LimitP, $Int_NumRecords){
		global	$GLOBAL_START_URL, $GLOBAL_START_PATH;
				
		//echo "start= $Int_StartP, limit=$Int_LimitP, numrec=$Int_NumRecords";
		$Bool_LocalVar = 0;
		if ( $Int_StartP > 0) {
			  	//$this->InputButton("hidden", "Int_StartP" , $Int_StartP);//No pour commencer l'affichage
				echo "\n		<input type=\"image\" name=\"back\" value=\"Derrière 5\" src=\"$GLOBAL_START_URL/images/back_office/content/btn_previous.jpg\">";
		}
		if (  $Int_StartP+$Int_LimitP < $Int_NumRecords  ) {
				echo "\n		<input type=\"image\" name=\"next\" value=\"Suivant 5\" src=\"$GLOBAL_START_URL/images/back_office/content/btn_next.jpg\">";	
		}
		$Int_EndP = min($Int_StartP+$Int_LimitP , $Int_NumRecords); $Int_StartP++;
		echo "\n		<span class=\"text_b\"> $Int_StartP à $Int_EndP parmi $Int_NumRecords </span>\n		";
		$this->InputButton("hidden", "Int_StartP" , $Int_StartP );
	
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Affichage un formulaire de confirmation de suppression
	 * @param array $k	Les produits concernés
	 * @param array $ExtraFields Champs 'hidden' à ajouter au formulaire
	 * @return void
	 */
	public function display_DeleteConfirmationForm(&$k,$ExtraFields=null){
		
		?>
			<div class="mainContent fullWidthContent">
			    <div class="topRight"></div><div class="topLeft"></div>
			    <div class="content">
			    	<div class="headTitle">
			            <p class="title"><?php echo Dictionnary::translate('delete_confirm_title') ?></p>
			            <div class="rightContainer">&nbsp;</div>
			        </div>
			        <div class="subContent">
			            <!--<div class="resultTableContainer">-->
			            <?php 
			            	global	$GLOBAL_START_URL, $GLOBAL_START_PATH;
							
								$Int_NumDelRec = count($k);
							////////////	$this->VerifyDeleteRelations();
								$this->BeginPostForm($this->current_page,'confirm');
								$this->InputField('hidden', 'delete_condition' , $this->CreateDeletePattern($k) );
							       if($ExtraFields)
							               {
										foreach($ExtraFields as $k=>$v)
							               $this->InputField("hidden",$k,$v);
							               }
								//echo 'display_DeleteConfirmationForm' ; print_r($this->table);
								echo "\t\t".'<div id="confirm" style="text-align:center;">'."\n";
								echo "\t\t\t".'<h3>'.Dictionnary::translate('txt_delete_form1')." $Int_NumDelRec ".Dictionnary::translate("txt_delete_form2").'</h3>'."\n";
								
								
								echo "<input type='submit' class='blueButton' style='margin-right:10px;' name='delete_confirmed' value='". Dictionnary::translate('btn_confirm') ."' />";
								echo "<input type='submit' class='blueButton' name='search' value='". Dictionnary::translate('btn_cancel') ."' />";
							
								echo "\t\t".'</div>'."\n";
							//	$this->InputButton("hidden", "HFselect", $HFselect);
								$this->EndForm();
			            ?>
						<!--</div>-->
			        </div>
				</div>
				<div class="bottomRight"></div><div class="bottomLeft"></div>
			</div>
			</div>
		<?php
		
		include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
		
		exit();
		
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Génère des champs caché à partir des params de $k
	 * @param array $k le fameux $k
	 * @return void
	 */
	public function MakeHiddenFields( array &$k){
		
		for ($i=0;;$i++) {
			
			if(!isset($k[$i])) break;
			$v=unserialize(base64_decode($k[$i]));
			reset ($v);
			foreach($v as  $ii=>$iii) {
				if($v[$ii]=='') continue;
				$this->InputField('hidden',$ii,$v[$ii]);
			}
			
		}
		
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Vérifie s'il existe des enregistrements liés
	 * @return void
	 */
	public function VerifyDeleteRelations(){
		
		$Str_NameTable = $this->table->getTableName();
		$Str_Report=''; //total relationships report
		$TableName = "relations";
		$Str_FieldNames	= "keyname, present_in, parent_of";
		$Str_Condition = " WHERE tablename='$Str_NameTable' ";
		$Arr = $this->GetTableRowsBy($TableName, $Str_FieldNames, $Str_Condition);
	
		if (count($Arr)) {
			reset($Arr);
			while ( list ($k, $v) = each ($Arr) ) {
				if (trim($v)) {
	//				echo "<br />k=$k, v=".print_array($v);
					$Str_FieldName	= $v["keyname"];
					$Str_TableNames	= $v["present_in"];
					if(!empty($Str_TableNames))
					{
						$Str_Report .= "<br /> ".Dictionnary::translate("field")." $Str_FieldName ".Dictionnary::translate("present_direct")." $Str_TableNames ";
						$Str_Report .= "<br />".Dictionnary::translate("field")."  $Str_FieldName ".Dictionnary::translate("present_undirect")." est present indirect aussi dans les tables ";
					}
					$Str_RecursiveSearchInto	= $v["parent_of"];
					$ArrSons = split (" ",$Str_RecursiveSearchInto);
					while ( list ($key, $val) = each ($ArrSons) ) {
						if (trim($val)) {
							$Str_Report .= $this->RecurseSubRelations ($val);
						}
					}
				}
			}
		}
		if (trim($Str_Report)) {
			Warning ("  ".Dictionnary::translate("empty_table")." $Str_NameTable","$Str_Report");
		}
		
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Vérifie récursivement s'il existe des enregistrements liés
	 * @param int $Int_IdRelation le numéro de la relation
	 * @return string un message de rapport
	 */
	public function RecurseSubRelations($Int_IdRelation)
	{
		$Str_FieldNames	= "keyname, present_in, parent_of";
		$Str_Condition	= " WHERE idrelation='$Int_IdRelation' ";
		$Arr = $this->GetTableRowsBy('relations', $Str_FieldNames, $Str_Condition);
		if (count($Arr)) {
	// key search, so one call
			list ($k, $v) = each ($Arr);
			$Str_FieldName			= $v["keyname"];
			$Str_TableNames			= $v["present_in"];
			$Str_RecursiveSearchInto	= $v["parent_of"];
			$ArrSons = split (" ",$Str_RecursiveSearchInto);
			while ( list ($key, $val) = each ($ArrSons) ) {
				if (trim($val)) {
					$Str_Report = $this->RecurseSubRelations($val);
				}
			}
		}
		return " $Str_TableNames ". $Str_Report;
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Génère la clause sql WHERE pour une recherche
	 * @param array $k les critères de recherche
	 * @param string $defcnd La condition par défaut si $k est vide
	 * @param mixed $Array_ExternFields un tableau de champs externes
	 * @return string une clause SQL WHERE
	 */
	public function CreateSearchPattern( array $k,$defcnd='1',$Array_ExternFields=false){
		
		$Str_SelectCondition="";
		$Str_ActionName = $this->current_page;
		$_SESSION[$Str_ActionName]['search_startpage']=0;
		for ($i=0;;$i++)
		{
			
			if( !isset($k[$i]) ) 
				break;
				
			$v=unserialize(base64_decode($k[$i]));
			if(is_array($v)) break;
			if( !isset($_POST[$v]) ) break;
			$this->newsearch = true;
			
			
			$x = $_POST[$v];
				if ($x != "")
				{ //test type
				$_SESSION[$Str_ActionName][$v]=$x;
				if($Array_ExternFields) {
					$ax=0;
					foreach($Array_ExternFields as $ak=>$av) if( $av['fieldname'] == $v) $ax=$ak;
					 if( $ax ) $Str_SelectCondition.='AND `'.$Array_ExternFields[$ax]['table'].'`.`'.$v.'` LIKE '."'%".$x."%' ";
					 else $Str_SelectCondition.='AND A.`'.$v.'` LIKE '."'%".$x."%' ";
				} elseif( $this->table->get_type($v) == 'int' ) $Str_SelectCondition.="AND A.$v='$x' ";
				else $Str_SelectCondition.='AND A.`'.$v.'` LIKE '."'%".$x."%' ";
				}
			
		}
		$Str_SelectCondition=substr($Str_SelectCondition, 3); //delete first AND
		if($Str_SelectCondition!="") {
			if($Array_ExternFields) $Str_SelectCondition= ','.$Array_ExternFields[999]['table'].' WHERE '.$Str_SelectCondition.' AND '.$Array_ExternFields[999]['table_link'];
			else $Str_SelectCondition="WHERE ".$Str_SelectCondition;
		} else 	$Str_SelectCondition="WHERE $defcnd";
		
		//echo "SELECT $Str_SelectCondition SELECT";
		$_SESSION[$Str_ActionName]['search_order']='';
		$_SESSION[$Str_ActionName]['search_select'] = $Str_SelectCondition;
		
		return $Str_SelectCondition;
	
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Enregistre les critères de recherche dans la session
	 * @param string $p clause SQL SELECT
	 * @return void
	 */
	public function SetSearchPattern( $p){
		
		//echo "SELECT $p SELECT";	
		$Str_ActionName = $this->current_page;
		$_SESSION[$Str_ActionName]['search_select'] = $p;
		$_SESSION[$Str_ActionName]['search_startpage'] = 0;
		$_SESSION[$Str_ActionName]['search_order']='';
	
	}

	//--------------------------------------------------------------------------------------
	
	/**
	 * Retourne un nom significatif pour une clé étrangère ( ex : retourne 'state' pour 'idstate' )
	 * @param string $fieldname le nom de la clé étrangère ( ex : idstate dans la table buyer )
	 * @return string
	 */
	public function getForeignHeading( $fieldname ){
		
		$lang =  User::getInstance()->getLang();
		
		$tablename = $this->table->getTableName();
		
		$query = "
		SELECT foreign_label
		FROM desc_field 
		WHERE fieldname LIKE '$fieldname' 
		AND tablename LIKE '$tablename' 
		LIMIT 1";

		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( !$rs->RecordCount() )
			return false;
			
		if( $rs === false )
			die( "Impossible de récupérer le label pour la clé étrangère '$fieldname'" );
			
		$foreign_label = $rs->fields( "foreign_label" );
		
		if( empty( $foreign_label ) )
			return false;
		
		$exploded = explode( ".", $rs->fields( "foreign_label" ) );
		if( count( $exploded ) != 2 )
			die( "Valeur incorrect pour la colonne 'foreign_label' dans la table 'desc_field'" );
		
		list( $foreign_table, $foreign_column ) = $exploded;
			
		return Dictionnary::translate( $foreign_column . "$lang" );
		
	}
	
	public function getCurrentPage() {
		return $this->current_page;
	} 
	
	//--------------------------------------------------------------------------------------
			
	/**
	 * Retourne un nom significatif pour une colonne donnée et une valeur donnée
	 * @param string $fieldname le nom de la colonne
	 * @param string $value la valeur
	 * @return string
	 */
	public function getForeignLabel( $fieldname, $value ){
		
		$lang =  User::getInstance()->getLang();
		
		$tablename = $this->table->getTableName();
		
		$query = "
		SELECT foreign_label 
		FROM desc_field 
		WHERE fieldname LIKE '$fieldname' 
		AND tablename LIKE '$tablename' 
		LIMIT 1";

		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( !$rs->RecordCount() )
			return false;
			
		if( $rs === false )
			die( "Impossible de récupérer le label pour la clé étrangère '$fieldname'" );
		
		$foreign_label = $rs->fields( "foreign_label" );
			
		if( empty( $foreign_label ) )
			return false;
		
		$exploded = explode( ".", $rs->fields( "foreign_label" ) );
		if( count( $exploded ) != 2 )
			die( "Valeur incorrect pour la colonne 'foreign_label' dans la table 'desc_field'" );

		list( $foreign_table, $foreign_column ) = $exploded;
		
		$foreign_key = $fieldname;
		$foreign_label = $foreign_column . "$lang";
		
		$query = "
		SELECT `$foreign_label` AS foreign_label FROM `$foreign_table` WHERE `$foreign_key` = '$value' LIMIT 1";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( !$rs->RecordCount() )
			return $value;
			
		if( $rs === false )
			die( "Impossible de récupérer le label pour la clé étrangère '$fieldname'" );
		
		$foreign_label = $rs->fields( "foreign_label" );
		
		if( !$rs->RecordCount() || empty( $foreign_label ) )
			return "";
			
		return $rs->fields( "foreign_label" );

	}
	
	//--------------------------------------------------------------------------------------
	
}//EOC
?>