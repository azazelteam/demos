<?php
 
 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorb�
 * Object fournisseurs
 */
 
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Deliverer.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/SupplierContact.php" );
include_once( dirname( __FILE__ ) . "/SupplierAddress.php" );
include_once( dirname( __FILE__ ) . "/SupplierFactoryAddress.php" );


class Supplier extends DBObject implements Deliverer{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var SupplierAddress $address
	 */
	private $address;
	/**
	 * @var ArrayList<SupplierFactoryAddress> $factories
	 */
	private $factories;
	/**
	 * @var ArrayList<SupplierContact> $contacts
	 */
	private $contacts;
	/**
	 * @var SupplierContact $currentContact
	 */
	private $currentContact;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idsupplier
	 * @param int $idcontact
	 */
	public function __construct( $idsupplier, $idcontact = false ){
	
		parent::__construct( "supplier", false, "idsupplier",  $idsupplier  );

		$this->address 		= new SupplierAddress( $this->recordSet[ "idsupplier" ] );
		$this->factories 	= new ArrayList( "SupplierFactoryAddress" );
		$this->contacts 	= new ArrayList( "SupplierContact" );
		
		$this->setContacts( $idcontact ?  $idcontact : $this->get( "main_contact" ) );
		$this->setFactories();
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Charge les informations des contacts
	 * @param int $idcontact le contact courant
	 * @return void
	 */
	private function setContacts( $idcontact ){

		$query = "
		SELECT * FROM `supplier_contact` 
		WHERE `idsupplier` = '" . $this->recordSet[ "idsupplier" ] . "'
		ORDER BY idcontact ASC";
	
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){

			$this->contacts->add( new SupplierContact( $this->recordSet[ "idsupplier" ], $rs->fields( "idcontact" ) ) );
			$rs->MoveNext();
			
		}
		
		/* contact courant */
		
		$it = $this->contacts->iterator();
		
		while( $it->hasNext() ){

			$contact =& $it->next();
			
			if( ( !$idcontact && $contact->isMainContact() ) || $contact->getId() == $idcontact )	
				$this->currentContact =& $contact;

		}
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Charge les informations des usines
	 * @return void
	 */
	private function setFactories(){
		
		$rs =& DBUtil::query( "SELECT idsupplier_factory FROM supplier_factory WHERE idsupplier = '" . $this->recordSet[ "idsupplier" ] . "'" );
		
		while( !$rs->EOF() ){
			
			$this->factories->add( new SupplierFactoryAddress( $rs->fields( "idsupplier_factory" ) ) );
			
			$rs->MoveNext();
			
		}
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return ArrayList<SupplierAddressFactory> la liste des adresses usines
	 */
	public function &getFactories(){ return $this->factories; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access static
	 * @return Supplier
	 */
	public static function create(){
		
		if( DBUtil::query( "INSERT INTO supplier VALUES()" ) === false ){

			trigger_error( "Impossible de cr�er un nouveau fournisseur", E_USER_ERROR );
			return;
			
		}
		
		$idsupplier = DBUtil::getInsertId();
		$contact 	= SupplierContact::create( $idsupplier );

		return new Supplier( $idsupplier, $contact->getId() );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return bool
	 */
	public function allowDelete(){
		
		global $GLOBAL_DB_NAME;
		
		$rs =& DBUtil::query( "SHOW TABLES" );
	
		while( !$rs->EOF() ){
		
			$tablename = $rs->fields( "Tables_in_$GLOBAL_DB_NAME" );
			
			if( $tablename != "supplier" ){
				
				$rs2 =& DBUtil::query( "SHOW COLUMNS FROM `$tablename` LIKE 'idsupplier'" );
				
				if( $rs2->RecordCount() ){
				
					if( DBUtil::query( "SELECT idsupplier FROM `$tablename` WHERE idsupplier = '" . $this->recordSet[ "idsupplier" ] . "' LIMIT 1" )->RecordCount() )
						return false;
						
				}
			
			}
			
			$rs->MoveNext();
			
		}
	
		return true;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access static
	 * @param int $idsupplier l'identifiant du fournisseur � supprimer
	 * @return void
	 */
	public static function delete( $idsupplier ){
		
		$supplier = new Supplier( $idsupplier );
		
		if( !$supplier->allowDelete() ){
			
			trigger_error( "Impossible de supprimer le fournisseur", E_USER_ERROR );
			return;
			
		}
		
		DBUtil::query( "DELETE FROM supplier WHERE idsupplier = '" .  $idsupplier  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM supplier_contact WHERE idsupplier = '" .  $idsupplier  . "'" );
		DBUtil::query( "DELETE FROM supplier_factory WHERE idsupplier = '" .  $idsupplier  . "'" );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return int un identifiant unique
	 */
	public function getId(){ return $this->recordSet[ "idsupplier" ]; }
	
	/**
	 * Nom ou raison sociale
	 * @access public
	 * @return string
	 */
	public function getName(){ return $this->recordSet[ "name" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return ArrayList la liste des contacts
	 */
	public function &getContacts(){ return $this->contacts; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return SupplierContact le contact principal
	 */
	public function &getContact(){ return $this->currentContact; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @see objects/DBObject#save()
	 */
	public function save(){

		parent::save();

		$it = $this->contacts->iterator();
		
		while( $it->hasNext() )
			$it->next()->save();

		$this->address = new SupplierAddress( $this->recordSet[ "idsupplier" ] );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'adresse du fournisseur
	 * L'adress n'est pas �ditable car les donn�es sont dans la table supplier, pr�f�rer la classe Supplier pour modifier les donn�es
	 * @see objects/Deliverer#getAddress()
	 * @return SupplierAddress l'adresse du fournisseur
	 */
	public function &getAddress(){ return $this->address; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * return float
	 */
	public function getVATRate(){
		
		$query = "
		SELECT state.bvat 
		FROM supplier, state 
		WHERE supplier.idsupplier = '" . $this->recordSet[ "idsupplier" ] . "' 
		AND supplier.idstate = state.idstate 
		LIMIT 1";
	
		$rs =& DBUtil::query( $query );
		
		if( floatval( $rs->fields( "bvat" ) ) > 0.0 )
			return floatval( DBUtil::getParameter( "vatdefault" ) );
		
		return 0.0;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * TVA appliqu�e pour le transport de $source vers $destination
	 * @param Address $source
	 * @param Address $destination
	 * @return float
	 */
	public function getCarriageVATRate( Address $source, Address $destination ){
		
		if( $this->getVATRate() == 0.0 )
			return 0.0;
			
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>