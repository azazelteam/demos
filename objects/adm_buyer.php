<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des acheteurs
*/

include_once('adm_table.php');

/**
 * Gestion des acheteurs
 * 
 * @package    	Administration
 * @uses 		class ADMBASE
 */
class ADMBUYER extends ADMTABLE
{
var $DEBUG = false;
var $id_buyer = 0;
var $buyerinfo = false;

/**
 * Constructeur
 * @param int $id le numéro du client
 */
function __construct( $id)
{
	ADMTABLE::ADMTABLE('buyer');
	$this->id_buyer = $id;
	$this->MakeEmptyRecord();
	//if($id>0) $this->LoadBuyer();
 $this->LoadBuyer();
}

/**
 * Charge les informations de l'acheteur depuis la base de données
 * @return void
 */
private function LoadBuyer()
{	// ------- Mise à jour de l'id gestion des Index  #1161
	$this->buyerinfo = DBUtil::getConnection()->GetRow("SELECT * FROM buyer WHERE idbuyer='".$this->id_buyer."'");
	$this->LoadRecord("WHERE idbuyer='".$this->id_buyer."'");
	if($this->DEBUG) echo 'DEBUG SELECT * FROM buyer WHERE idbuyer=',$this->id_buyer,' DEBUG';
	$this->buyerinfo['state'] = $this->GetState($this->buyerinfo['idstate']);
}

/**
 * récupère le nom d'un pays d'après un identifiant
 * @param int $IdState l'identifiant du pays
 * @return string le nom du pays
 */
public function GetState($IdState)
{
	$Result="-";
	$lang = '_' . $this->LingualNumber;
	$res= DBUtil::getConnection()->Execute("select name$lang as name from state where idstate='$IdState'");
	if($res->RecordCount()>0)
	{
		$Result=$res->fields['name'];
	}
	return $Result;
}

}//EOC

?>
