<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entêtes factures fournisseurs services
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );

include_once( dirname( __FILE__ ) . "/Provider.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/ProviderInvoiceItem.php" );
include_once( dirname( __FILE__ ) . "/User.php" );


class ProviderInvoice extends DBObject{

	/* ----------------------------------------------------------------------------------------------------- */

    private $idprovider_invoice;
    private $idprovider;
    
    /**
     * @var ArrayList $items
     */
    private $items;
    
    /**
     * @var Salesman $salesman
     */
    private $salesman;
    
    /**
     * @var Provider $provider
     */
    private $provider;
    
    /* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Constructeur
	 * @param string $idprovider_invoice le numéro de facture fournisseur
	 */
	function __construct( $idprovider_invoice , $idprovider ){	
		
		parent::__construct( "provider_invoice", false, "idprovider_invoice", $idprovider_invoice, "idprovider", intval( $idprovider ) );
		
		$this->idprovider_invoice 	= $idprovider_invoice;
		$this->idprovider		 	= $idprovider;
		
		$this->salesman 			= new Salesman( $this->recordSet[ "iduser" ] );
		$this->provider				= new Provider( $this->recordSet[ "idprovider" ] );

		$this->setItems();

	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return string
	 */
	public function getId(){ return $this->idprovider_invoice; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles de la facture
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "ProviderInvoiceItem" );
		
		$query = "
		SELECT iditem
		FROM provider_invoice_item
		WHERE idprovider_invoice = " . DBUtil::quote( $this->idprovider_invoice ) . "
		AND idprovider = '{$this->idprovider}'
		ORDER BY iditem ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
		
			$this->items->add( new ProviderInvoiceItem( $rs->fields( "iditem" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * supprime les lignes articles
	 * @access public
	 * @return void
	 */
	public function removeItems(){
		
		$this->items = new ArrayList( "ProviderInvoiceItem" );
		$this->save();
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Salesman le commercial de la facture
	 */
	public function &getSalesman(){ return $this->salesman; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Provider le prestataire de la facture
	 */
	public function &getProvider(){ return $this->provider; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Sauvegarde
	 * @access private
	 * @return void
	 */
	public function save(){

		$this->set( "username" , 	User::getInstance()->get( "login" ) );
		$this->set( "lastupdate" , date( "Y-m-d H:i:s" ) );
		
		parent::save();
		
		DBUtil::query( "DELETE FROM provider_invoice_item WHERE idprovider_invoice = '{$this->idprovider_invoice}' AND idprovider = '{$this->idprovider}'" );

		$i = 0;
		while( $i < $this->items->size() ){
		
			$this->items->get( $i )->save();

			$i++;
			
		}

	 }
	 
	/* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Créé une nouvelle facture fournisseur
	 * @static
	 * @param string $billing_buyer le numéro de la facture fournisseur
	 * @param int $idprovider le numéro du fournisseur
	 * @return ProviderInvoice une nouvelle facture fournisseur
	 */
	public static function create( $idprovider_invoice, $idprovider, $total_amount_ht, $total_amount, $vat_amount ){

	 	$query = "
		INSERT INTO `provider_invoice` ( 
			idprovider_invoice, 
			idprovider,
			iduser,
			total_amount_ht,
			total_amount,
			vat_amount,
			creation_date
		) VALUES ( 
			" . DBUtil::quote( $idprovider_invoice ) . ",
			'$idprovider', 
			'" . User::getInstance()->getId() . "',
			'$total_amount_ht',
			'$total_amount',
			'$vat_amount',
			NOW()
		)";
			
		if( DBUtil::query( $query ) === false )
			trigger_error( "Impossible de créer la nouvelle facture", E_USER_ERROR );

		return new ProviderInvoice( $idprovider_invoice, $idprovider );
	 	
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return ArrayList<ProviderInvoiceItem>
	 */
	public function &getItems(){ return $this->items; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @param int $idservice
	 * @param int $quantity
	 * @return ProviderInvoiceItem
	 */
	public function addService( $idservice, $vat_rate, $quantity = 1 ){
		
		if( !intval( $idservice ) )
			return;
		
		$service = DBUtil::stdclassFromDB( "service", "idservice", $idservice );
			
		$query = "
		INSERT INTO provider_invoice_item( 
			idservice,
			label,
			price,
			quantity,
			idprovider_invoice, 
			idprovider,
			vat_rate )
		VALUES( 
			'{$service->idservice}',
			" . DBUtil::quote( $service->label ) . ",
			'{$service->price}',
			'" . intval( $quantity ) . "',
			" . DBUtil::quote( $this->idprovider_invoice ) . ", 
			'" . $this->provider->getId() . "',
			" . intval($vat_rate) . "
		)";
		
		DBUtil::query( $query );

		$item = new ProviderInvoiceItem( DBUtil::getInsertID() );
		
		$this->items->add( $item );	
		
		return $item;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @static
	 * @param string $idprovider_invoice
	 * @param int $idprovider
	 * @return void
	 */
	public static function delete( $idprovider_invoice, $idprovider ){

		DBUtil::query( "DELETE FROM provider_invoice WHERE idprovider_invoice = " . DBUtil::quote( $idprovider_invoice ) . " AND idprovider = '" . intval( $idprovider ) . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM provider_invoice_item WHERE idprovider_invoice = " . DBUtil::quote( $idprovider_invoice ) . " AND idprovider = '" . intval( $idprovider ) . "'" );
		DBUtil::query( "
			DELETE brp.*, rp.* FROM billing_regulations_provider brp, regulations_provider rp
			WHERE brp.idprovider_invoice = " . DBUtil::quote( $idprovider_invoice ) . " 
			AND brp.idprovider = '" . intval( $idprovider ) . "'
			AND rp.idregulations_provider = brp.idregulations_provider" 
		);
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return float le montant total TTC
	 * @return unknown_type
	 */
	public function getTotalATI(){
		
		$totalATI = 0.0;
		$vat = array();
		
		$it = $this->items->iterator();
		
		while( $it->hasNext() )
			$totalATI += $it->next()->getTotalET();

		$totalATI += $this->recordSet[ "vat_amount" ];
			
		return $totalATI;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return float le montant total HT
	 * @return unknown_type
	 */
	public function getTotalET(){
		
		$totalET = 0.0;
		
		$it = $this->items->iterator();
		
		while( $it->hasNext() )
			$totalET += $it->next()->getTotalET();
			
		return $totalET;
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Net à payer
	 * TTC - acompte - escompte - avoirs
	 * @return float
	 */
	public function getNetBill(){

		return $this->getTotalATI() - $this->recordSet[ "rebate_amount" ] - $this->recordSet[ "down_payment_amount" ];

	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Reste à payer
	 * Net à payer - Somme des règlements
	 * @return float
	 */
	public function getBalanceOutstanding(){

		$paid = DBUtil::query( "
			SELECT COALESCE( SUM( amount ), 0.0 ) AS paid
			FROM billing_regulations_provider
			WHERE idprovider_invoice = " . DBUtil::quote( $this->idprovider_invoice ) . " 
			AND idprovider = '" . $this->provider->getId() . "'
			GROUP BY idprovider_invoice" )->fields( "paid" );
		
		return max( 0.0, $this->getNetBill() - $paid );

	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	
}

?>