<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object paiement SSL CMDP
 */
 

// important classes et scripts CMCIC
include_once( dirname( __FILE__ ) . "/../script/CMCIC_Tpe.inc.php");

class PaySecureV3 {
	
	// ----------------------------------------------------------------------------
	function __construct(){
			
	     if( !@file_exists( dirname( __FILE__ ) . "/../config/CMCIC_ConfigV3.php" ) ){
		
			trigger_error( "Configuration du TPE absente", E_USER_ERROR );
			die();
			
		}
		
		@include( dirname( __FILE__ ) . "/../config/CMCIC_ConfigV3.php" );		
	
	}	
	

	/**
	 * Création du formulaire pour le passage sur le serveur du Credit Mutuel
	 * Phase Aller = On créé le formulaire pour le passage au CM
	 */
	
	function CreerFormulaireHmac( $sEmail , $sMontant , $sReference , $sDate , $Return_Context = "" , $sTexteLibre = "" , $sLangue = 'FR' , $sDevise = 'EUR' , $Button_Text = '' ){
	
		// pas d'options on est pauvres
		$sOptions = "";

    	// Préparation du lien de retour. Un contexte est ajouté au lien.
    	$Return_Context = "?order_ref=".$sReference;

		//chèque-cadeau et bon de réduction
		
		if( isset( $_REQUEST[ "gift_token_serial" ] ) || isset( $_REQUEST[ "voucher_code" ] ) ){
	
			if( isset( $_REQUEST[ "gift_token_serial" ] ) )
				$Return_Context .= "&gift_token_serial=" . stripslashes( $_REQUEST[ "gift_token_serial" ] );
			
			if( isset( $_REQUEST[ "voucher_code" ] ) )
				$Return_Context .= "&voucher_code=" . stripslashes( $_REQUEST[ "voucher_code" ] );
			
		}
		
		
		// ----------------------------------------------------------------------------
		// PAS UTLILISE CHEZ NOUS ET TANT MIEUX ! MOUAHAHAHAHAHAHA
		$sNbrEch = "";
		$sDateEcheance1 = "";
		$sMontantEcheance1 = "";
		$sDateEcheance2 = "";
		$sMontantEcheance2 = "";
		$sDateEcheance3 = "";
		$sMontantEcheance3 = "";
		$sDateEcheance4 = "";
		$sMontantEcheance4 = "";
		
		// ----------------------------------------------------------------------------
		
		$oTpe = new CMCIC_Tpe($sLangue);     		
		$oHmac = new CMCIC_Hmac($oTpe);      	        
		
		// Control String for support
		$CtlHmac = sprintf(CMCIC_CTLHMAC, $oTpe->sVersion, $oTpe->sNumero, $oHmac->computeHmac(sprintf(CMCIC_CTLHMACSTR, $oTpe->sVersion, $oTpe->sNumero)));
		
		if( !$CtlHmac )
			die( "Erreur lors de la vérifiction de la clé" );
			
		// Data to certify
		$PHP1_FIELDS = sprintf(CMCIC_CGI1_FIELDS,     $oTpe->sNumero,
		                                              $sDate,
		                                              $sMontant,
		                                              $sDevise,
		                                              $sReference,
		                                              $sTexteLibre,
		                                              $oTpe->sVersion,
		                                              $oTpe->sLangue,
		                                              $oTpe->sCodeSociete, 
		                                              $sEmail,
		                                              $sNbrEch,
		                                              $sDateEcheance1,
		                                              $sMontantEcheance1,
		                                              $sDateEcheance2,
		                                              $sMontantEcheance2,
		                                              $sDateEcheance3,
		                                              $sMontantEcheance3,
		                                              $sDateEcheance4,
		                                              $sMontantEcheance4,
		                                              $sOptions);
		
		// MAC computation
		$sMAC = $oHmac->computeHmac($PHP1_FIELDS);
		
		// --------------------------------------------------- End Stub ---------------	
	
		// FORMULAIRE YAHOUUUUUU
	
		?>
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
		<head></head>
		<body>
			<form action="<?php echo $oTpe->sUrlPaiement;?>" method="post" id="PaymentRequest">
			<p>
				<input type="hidden" name="version"             id="version"        value="<?php echo $oTpe->sVersion;?>" />
				<input type="hidden" name="TPE"                 id="TPE"            value="<?php echo $oTpe->sNumero;?>" />
				<input type="hidden" name="date"                id="date"           value="<?php echo $sDate;?>" />
				<input type="hidden" name="montant"             id="montant"        value="<?php echo $sMontant . $sDevise;?>" />
				<input type="hidden" name="reference"           id="reference"      value="<?php echo $sReference;?>" />
				<input type="hidden" name="MAC"                 id="MAC"            value="<?php echo $sMAC;?>" />
				<input type="hidden" name="url_retour"          id="url_retour"     value="<?php echo $oTpe->sUrlKO;?>" />
				<input type="hidden" name="url_retour_ok"       id="url_retour_ok"  value="<?php echo $oTpe->sUrlOK.$Return_Context;?>" />
				<input type="hidden" name="url_retour_err"      id="url_retour_err" value="<?php echo $oTpe->sUrlKO;?>" />
				<input type="hidden" name="lgue"                id="lgue"           value="<?php echo $oTpe->sLangue;?>" />
				<input type="hidden" name="societe"             id="societe"        value="<?php echo $oTpe->sCodeSociete;?>" />
				<input type="hidden" name="texte-libre"         id="texte-libre"    value="<?php echo HtmlEncode($sTexteLibre);?>" />
				<input type="hidden" name="mail"                id="mail"           value="<?php echo $sEmail;?>" />
				<!-- Uniquement pour le Paiement fractionné -->
				<input type="hidden" name="nbrech"              id="nbrech"         value="<?php echo $sNbrEch;?>" />
				<input type="hidden" name="dateech1"            id="dateech1"       value="<?php echo $sDateEcheance1;?>" />
				<input type="hidden" name="montantech1"         id="montantech1"    value="<?php echo $sMontantEcheance1;?>" />
				<input type="hidden" name="dateech2"            id="dateech2"       value="<?php echo $sDateEcheance2;?>" />
				<input type="hidden" name="montantech2"         id="montantech2"    value="<?php echo $sMontantEcheance2;?>" />
				<input type="hidden" name="dateech3"            id="dateech3"       value="<?php echo $sDateEcheance3;?>" />
				<input type="hidden" name="montantech3"         id="montantech3"    value="<?php echo $sMontantEcheance3;?>" />
				<input type="hidden" name="dateech4"            id="dateech4"       value="<?php echo $sDateEcheance4;?>" />
				<input type="hidden" name="montantech4"         id="montantech4"    value="<?php echo $sMontantEcheance4;?>" />
				
				<input type="hidden" name="bouton"              id="bouton"         value="Connexion / Connection" />
				
			</p>
			</form>
			<!-- FIN FORMULAIRE TYPE DE PAIEMENT / END PAYMENT FORM TEMPLATE -->
			<script language="javascript" type="text/javascript">document.forms.PaymentRequest.submit();</script> 
		</body>
		</html>
		<?php
	}
}
?>