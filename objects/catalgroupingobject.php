<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object groupememt
 */

include_once( dirname( __FILE__ ) . "/catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CATALGROUPING extends CATALBASE
{

	/**
	 * Constructeur
	 */
	function __construct()
	{

	$reload=1;
	$this->id =  $this->buyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$this->lang = "_1";
	//if( isset($_SESSION['catalperso'])  ) $reload=0; 
	if( $reload ) { $this->SetPersoProducts(); }
	else { 
		$this->products = &$_SESSION['persoproducts'] ;
		$this->NBProduct = 0 +  count($this->products);
		}
	$this->NbByPages = DBUtil::getParameter('nb_products_by_pages');
	}

	/**
	 * Retourne le nombre de pages
	 * @return int
	 */
	public function getPageCount(){ return $this->NbByPages; }
	
	/**
	 * Recherhce les produits
	 * @return int Le nombre de produits
	 */
	private function SetPersoProducts()
		{
		$IdBuyer = $this->buyer;
		if($IdBuyer == 0 ) { $this->products = array(); $this->NBProduct = 0; $_SESSION['catalperso']='o'; return; }
		$Langue = $this->lang;
		$Query = "SELECT DISTINCT d.idproduct,p.name$Langue as name
				FROM detail d, grouping_catalog gp, product p
				WHERE gp.idgrouping = 1
				AND gp.reference = d.reference
				AND d.idproduct = p.idproduct
				ORDER BY name";
		$n = $this->SetProducts($Query);
		$_SESSION['persoproducts'] = &$this->products ;
		return $n;
	}
	
	/**
	 * Supprime un produit du catalogue perso
	 * @param int $IdProduct l'id du produit à supprimer
	 * @return void
	 */
	function DelProduct($IdProduct)
	{
		$IdBuyer = $this->buyer;
		if( $IdBuyer > 0 )
			{
			$Base = &DBUtil::getConnection();
			$Query = "DELETE FROM buyer_catalog WHERE idproduct='$IdProduct' AND idbuyer='$IdBuyer'";
			DBUtil::query ($Query); // echo " $Query <br />\n";
			}
		$Query = "SELECT idproduct from buyer_catalog WHERE idbuyer='$IdBuyer'";	
		$rs=& DBUtil::query($Query);
		$this->NBProduct = $rs->RecordCount();
	}
	
	/**
	 * Vide le catalogue perso
	 * @return void
	 */
	public function Destroy()
	{
		$IdBuyer = $this->buyer;
		if( $IdBuyer > 0 )
			{
			$Base = &DBUtil::getConnection();
			$Query = "DELETE FROM buyer_catalog WHERE idbuyer='$IdBuyer'";
			$rs= DBUtil::query ($Query); // echo " $Query <br />\n";
			}
		if( isset($_SESSION['persoproducts']) ) unset($_SESSION['persoproducts']);
		if( isset($_SESSION['catalperso']) ) unset($_SESSION['catalperso']);
		$this->NBProduct = 0;
	}
	
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}

}//EOC
?>