<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object appels fonction produits métiers
 */
 

interface Trade{
	
	//----------------------------------------------------------------------------
	/**
	 * @return ArrayList<TradeItem>
	 */
	public function &getItems();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getWeight();
	
	//----------------------------------------------------------------------------
	/**
	 * @param bool $forced
	 * @return float
	 */
	public function getChargesET();
	
	//----------------------------------------------------------------------------
	/**
	 * @return bool
	 */
	public function applyVAT();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getVATRate();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getDiscountRate();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getBillingRate();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getRebateAmount();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getDownPaymentAmount();
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 */
	public function getCreditAmount();
	
	//----------------------------------------------------------------------------
		
}
?>