<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object critères catégorie
 */
 
class CategoryFilter{
	
	private $category;
	private $htmlProducts;
	private $nodes;
	private $selectedNodes;
	private $products;
	private $productCount;
	private $paginate;
	private $actualPageNumber;
	private $page;
	
	/*------------------------------------------------------------------------------------------------------------------------------------*/
	
	public function __construct( CCategory $cat ){
		
		$this->productCount = 0;
		$this->selectedNodes = array();
		$this->paginate = false;

		$this->category = $cat;		
		if( $this->availableFilter( $this->category->getId() ) === false )
			return false;

	}
	
	/*------------------------------------------------------------------------------------------------------------------------------------*/
	
	public function selectNode( $node , $value ){
		
		$this->selectedNodes[ $node ] = $value;
		
	}

	/*------------------------------------------------------------------------------------------------------------------------------------*/	
	
	public function createNodes(){
		$IdCategory = $this->category->getId();
		
		$myFilter =& DBUtil::query("SELECT sf.idintitule_title, COALESCE(it.intitule_title_1,'') as intitule_title, type
									FROM search_filter sf
									LEFT JOIN intitule_title it ON sf.idintitule_title = it.idintitule_title
									WHERE idcategory = $IdCategory
									ORDER BY display_order ASC");		
	
		// a ceux ci on a plus qu'a rajouter les valeurs
		
		$categoryChildrens = $this->category->getChildren();

		$it = $categoryChildrens->iterator();
		$inLine = array( $IdCategory );
		while( $it->hasNext() ){
		
				$child =& $it->next();
				
				if( $child->get( "available" ) )
					array_push( $inLine, $child->getId() );
					
		}		

		$inLineTextual = implode("','" , $inLine);
		
		$catcond = " AND ( p.idcategory = " . implode( ' OR p.idcategory = ' , $inLine ) . " ) ";

		$i = 0;
							
		while( !$myFilter->EOF() ){
					
			$idintitule_title = $myFilter->fields('idintitule_title');
			$intitule_title = $myFilter->fields('intitule_title');
			
			$filterByPosition = '';
			
			// si on a un intitulé
			if( $intitule_title != "" ){
			
				// On cherche les valeurs possibles
				$myFilterElements =& DBUtil::query("	SELECT iv.idintitule_value, iv.intitule_value_1, il.idintitule_title
													FROM product p, intitule_link il, intitule_value iv, detail d
													WHERE il.idarticle <> 0
													AND p.idproduct <> 0
													AND p.idproduct = il.idproduct
													AND il.idintitule_value=iv.idintitule_value
													AND d.idarticle = il.idarticle
													AND d.available = 1
													AND p.available = 1
													AND p.idproduct = d.idproduct
													AND il.idintitule_title=$idintitule_title
													$catcond
													$filterByPosition
													 GROUP BY il.idintitule_value
													ORDER BY iv.intitule_value_1 ASC");				
				
				$arrayRows = array();
				$j = 0;
				
				if( $myFilterElements->RecordCount() ){
					$this->nodes[$i]['idintitule_title'] = $idintitule_title;
					$this->nodes[$i]['intitule_title'] = $myFilter->fields('intitule_title');
					$this->nodes[$i]['type'] = $myFilter->fields('type');
	
					while(!$myFilterElements->EOF()){
	
						if($myFilterElements->fields('idintitule_value') && isset( $this->selectedNodes[ $idintitule_title ] ) && $this->selectedNodes[ $idintitule_title ] == $myFilterElements->fields('idintitule_value') ){
							$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idintitule_value');
							$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('intitule_value_1');
							$arrayRows[$j]['selected'] = '1';
						}else{
							$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idintitule_value');
							$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('intitule_value_1');
							$arrayRows[$j]['selected'] = '';
						}
						
						$myFilterElements->MoveNext();
						$j++;
					
					}
				}
				
				if( count( $this->nodes[$i] ) ){
					$this->nodes[$i]['values'] = $arrayRows;
				}else{
					unset( $this->nodes[$i] );
				}
			}else{ // si on a une ss catégorie, une marque.. tout ce qui n'est pas un intitulé.
				
				switch( $idintitule_title ){
					case "sscategory":
	
						$myFilterElements =& DBUtil::query("	SELECT cl.idcategorychild, c2.name_1
															FROM category_link cl, category c, category c2
															WHERE cl.idcategoryparent IN ('$inLineTextual')
															AND c2.idcategory = cl.idcategorychild
															$filterByPosition
															 GROUP BY cl.idcategorychild
															ORDER BY cl.idcategorychild ASC");
						
						if( $myFilterElements->RecordCount()){
							
							$this->nodes[$i]['idintitule_title'] = 'sscategory';
							$this->nodes[$i]['intitule_title'] = 'Type';
							$this->nodes[$i]['type'] = $myFilter->fields('type');
														
							$j = 0;
							$arrayRows = array();
							while(!$myFilterElements->EOF()){
			
								if($myFilterElements->fields('idcategorychild') && isset( $this->selectedNodes[ 'sscategory' ] ) && $this->selectedNodes[ 'sscategory' ] == $myFilterElements->fields('idcategorychild') ){
									$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idcategorychild');
									$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('name_1');
									$arrayRows[$j]['selected'] = '1';
								}else{
									$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idcategorychild');
									$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('name_1');
									$arrayRows[$j]['selected'] = '';
								}
								$myFilterElements->MoveNext();
								$j++;
							}							
							
							if( count( $this->nodes[$i] ) ){
								$this->nodes[$i]['values'] = $arrayRows;
							}else{
								unset( $this->nodes[$i] );
							}
							
						}

					break;
						
					case "color":
						
						$myFilterElements =& DBUtil::query("	SELECT c.idcolor, c.name
															FROM product p, color c, color_product cp
															WHERE p.idproduct = cp.idproduct
															AND p.available = 1
															AND c.idcolor = cp.idcolor
															$catcond
															$filterByPosition 
															GROUP BY c.idcolor
															ORDER BY cp.display ASC");
															
						if($myFilterElements->RecordCount() > 0 ){												
		
							$this->nodes[$i]['idintitule_title'] = 'color';
							$this->nodes[$i]['intitule_title'] = 'Couleur';
							$this->nodes[$i]['type'] = $myFilter->fields('type');
		
							$j = 0;
							$arrayRows = array();	
							while(!$myFilterElements->EOF()){
		
								if($myFilterElements->fields('idcolor') && isset( $this->selectedNodes[ 'color' ] ) && $myFilterElements->fields('idcolor') == $this->selectedNodes[ 'color' ]){
									$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idcolor');
									$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('name');
									$arrayRows[$j]['selected'] = '1';
								}else{
									$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idcolor');
									$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('name');
									$arrayRows[$j]['selected'] = '';
								}
								
								$myFilterElements->MoveNext();
								$j++;
								
							}
							
							if( count( $this->nodes[$i] ) ){
								$this->nodes[$i]['values'] = $arrayRows;
							}else{
								unset( $this->nodes[$i] );
							}
						
						}
						
					break;
				
					case "brand":
									
						$myFilterElements =& DBUtil::query("	SELECT b.idbrand, b.brand
															FROM product p, brand b
															WHERE p.idbrand = b.idbrand
															AND p.available = 1
															$catcond
															$filterByPosition 
															 GROUP BY b.idbrand
															ORDER BY brand ASC");
						
						if($myFilterElements->RecordCount() > 0 ){									
		
							$this->nodes[$i]['idintitule_title'] = 'brand';
							$this->nodes[$i]['intitule_title'] = 'Marque';
							$this->nodes[$i]['type'] = $myFilter->fields('type');
		
							$j = 0;	
							$arrayRows = array();	
							while(!$myFilterElements->EOF()){
		
								if($myFilterElements->fields('idbrand') && isset( $this->selectedNodes[ 'brand' ] ) && $myFilterElements->fields('idbrand') == $this->selectedNodes[ 'brand' ]){
									$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idbrand');
									$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('brand');
									$arrayRows[$j]['selected'] = '1';
								}else{
									$arrayRows[$j]['idintitule_value'] = $myFilterElements->fields('idbrand');
									$arrayRows[$j]['intitule_value'] = $myFilterElements->fields('brand');
									$arrayRows[$j]['selected'] = '';
								}
								
								$myFilterElements->MoveNext();
								$j++;
								
							}
							
							if( count( $this->nodes[$i] ) ){
								$this->nodes[$i]['values'] = $arrayRows;
							}else{
								unset( $this->nodes[$i] );
							}
	
						}
						
					break;
						
				}
				
			}
				
			$myFilter->MoveNext();
			$i++;	
		}
	
	}

	/*------------------------------------------------------------------------------------------------------------------------------------*/	
	
	public function getNodes(){
		
		return $this->nodes;
		
	}

	/*------------------------------------------------------------------------------------------------------------------------------------*/	
	
	public function availableFilter( $idcategory ){
		
		$check = DBUtil::query( "SELECT count( idcategory ) as verif FROM search_filter WHERE idcategory = $idcategory LIMIT 1" )->fields( 'verif' ) > 0 ? true : false ;

		return $check;
		
	}

	/*------------------------------------------------------------------------------------------------------------------------------------*/	
	
	public function getProductCount(){
		
		return $this->productCount;
		
	}
	
	/*------------------------------------------------------------------------------------------------------------------------------------*/	
	
	public function getProducts(){

		$categoryChildrens = $this->category->getChildren();
		
		$IdCategory = $this->category->getId();
		
		$it = $categoryChildrens->iterator();
		$inLine = array( $IdCategory );
		while( $it->hasNext() ){
		
				$child =& $it->next();
				
				if( $child->get( "available" ) )
					array_push( $inLine, $child->getId() );
					
		}		

		$inLineTextual = implode("','" , $inLine);		

		$catcond = " AND ( p.idcategory = " . implode( ' OR p.idcategory = ' , $inLine ) . " ) ";
		
		$andOrFields = array();
		$andOfFieldsBrand = "";
		$andOfFieldsColor = "";
		
		$bonusTable = "";
		$bonusJoint = "";
		
		$m = 0;
		$b = 0;
		
		$page = 0;
		if( $this->page > 0){
			$page = $this->page;
		}

		$setIdint = count( $this->selectedNodes ) ? $this->selectedNodes : false;

		if( count( $this->selectedNodes ) ){
			
			foreach( $this->selectedNodes as $key => $value ){
				
				if( is_numeric( $key ) ){
						$i = 0;
						
						$idintitule = $key;
						$idintitule_value = $value;
						if( $idintitule_value != "" ){
							
							if( is_array( $idintitule_value ) ){
								$andOrFields[$m][$i] = "( il.idintitule_title = $idintitule AND ( il.idintitule_value = " . implode( ' OR il.idintitule_value = ' , $idintitule_value ) . " ) ) ";
							}else{
								$andOrFields[$m][$i] = "( il.idintitule_title = $idintitule AND il.idintitule_value = $idintitule_value ) ";
							}
							
							$i++;
							$m++;
						}

				}else{
					
					if( $key == 'color' ){
			
						$bonusTable .= ", product_color pc";
						$bonusJoint .= " AND p.idproduct = pc.idproduct ";
				
						$idintitule_value = $value;
						if( $idintitule_value != "" && !is_array( $idintitule_value ) ){
							$andOfFieldsColor = " AND pc.idcolor = $idintitule_value ";
						}
						
					}
					
					if( $key == 'brand' ){
					
						$idintitule_value = $value;
						if( $idintitule_value != "" && !is_array( $idintitule_value ) ){
							$andOfFieldsBrand = " AND p.idbrand = $idintitule_value ";
						}
						
					}
					
				}
			}
			
			$andOrFieldsTextual = "";
			$having = "";
			
			if( count( $andOrFields ) > 0 ){
				
				for( $l = 0 ; $l < count( $andOrFields ) ; $l++ ){
					
					if( $l==0 )
						$andOrFieldsTextual .= " AND ( ";
					else
						$andOrFieldsTextual .= " OR ";
						
					for( $j = 0 ; $j < count( $andOrFields[ $l ] ) ; $j++ ){
						if( $j==0 ){
							$andOrFieldsTextual .= $andOrFields[ $l ][ $j ];
						}else{
							$andOrFieldsTextual .= " OR " . $andOrFields[ $l ][ $j ];
						}
					}
					
					if( $l == count( $andOrFields ) - 1 )
						$andOrFieldsTextual .= " )";
				
				}
				
				$having = " HAVING cpt>=" . count( $andOrFields ) . " ";
			}
			
			$query = "	SELECT p.idproduct, count(p.idproduct) as cpt
						FROM product p, detail d, intitule_link il $bonusTable
						WHERE d.available = 1
						AND p.available = 1
						$catcond
						$andOrFieldsTextual
						$andOfFieldsBrand
						$andOfFieldsColor
						$bonusJoint
						AND d.idproduct = p.idproduct
						AND il.idarticle = d.idarticle
						AND il.idproduct = d.idproduct
						AND p.idproduct > 0
						GROUP BY p.idproduct
						$having";
			
			$nbaff =& DBUtil::query( "SELECT nb_categories_by_pages as nb FROM category WHERE idcategory = $IdCategory LIMIT 1" )->fields( 'nb' );
			
			if( $page > 0 ){
					
				$limitMax = $nbaff;
				$limitMin = ($limitMax * $page) - $nbaff;
				
				$pagenum = $page;
				
			}else{
				$limitMax = $nbaff;
				$limitMin = 0;
				
				$pagenum = 1;
			}
			
			$rs =& DBUtil::query( $query );
			
			$prodsAll = array();
			while( !$rs -> EOF() ){
				array_push( $prodsAll, $rs->fields( "idproduct" ) );
			
				$rs -> MoveNext();
			}
			
			$query .= " LIMIT $limitMin , $limitMax ";
			
			$rs =& DBUtil::query( $query );
			
			$prods = array();
			
			$countProducts =& DBUtil::query( "SELECT p.idproduct, count(p.idproduct) as cpt
											FROM product p, detail d, intitule_link il $bonusTable
											WHERE d.available = 1
											AND p.available = 1
											$catcond
											$andOrFieldsTextual
											$andOfFieldsBrand
											$andOfFieldsColor
											$bonusJoint
											AND d.idproduct = p.idproduct
											AND il.idarticle = d.idarticle
											AND il.idproduct = d.idproduct
											AND p.idproduct > 0
											GROUP BY p.idproduct $having" )->RecordCount();
			
			if( $countProducts > 0 ){
				
				include_once( DOCUMENT_ROOT . "/objects/catalog/CProduct.php" );
				include_once( DOCUMENT_ROOT . "/objects/flexy/proxies/CProductProxy.php" );
				
				while( !$rs->EOF() ){
					array_push( $prods , new CProductProxy( new Cproduct( $rs->fields("idproduct") ) ) );
				
					$rs->MoveNext();
				}
								
				$this->productCount = $countProducts;
				$this->products		= $prods;
				
				if( $countProducts > $nbaff ){
					
					$pageNumbers = array();
					
					for( $i = 0 ; $i < ceil( $countProducts / $nbaff ) ; $i++ ){
						array_push( $pageNumbers , $i+1 );
					}
					
					$this->paginate = $pageNumbers;
					
				}
				
				$this->actualPageNumber = $pagenum;
				
			}
			
		}		
		
		return $this->products;
		
	}
	
}

?>