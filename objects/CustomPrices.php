<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object prix clients
 */

include_once( dirname( __FILE__ ) . "/Session.php" );

class CustomPrices{
	
	//----------------------------------------------------------------------------
	
	/**
	 * Calcul le prix pour un article donné
	 * @param object $article l'article pour lequel ou souhaite calculer le prix
	 * @param int $quantity la quantité d'articles, 1 par défaut
	 * @return float le prix de l'article
	 */
	 
	public static function getPrice( &$article, $quantity = 1 ){
	
		$quantity = max( $quantity, $article->get( "min_cde" , 1 ) );
		$idarticle = $article->GetId();
	
		$idbuyer = $article->getIdBuyer();
		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getIdBuyer();
		
		//remises
		
		$familyProductBuyerRate 	= CustomPrices::getFamilyProductBuyerPriceRate( $article );
		$multipriceGroupingPrice 	= CustomPrices::getMultipriceGroupingPrice( $article );
		$tariffCodePrice 			= CustomPrices::getTariffCodePricePrice( $article );
		
		//prix
		
		$basicPrice = $article->GetBasicPrice();
		/*if( $basicPrice <= 0.0 )
			trigger_error( "Impossible de trouver un prix pour l'artcile '$idarticle'", E_USER_ERROR );*/
		
		$prices = array();
		
		if( $familyProductBuyerRate != -1 ){
			
			$familyProductBuyerPrice 	= $basicPrice * ( 1.0 - $familyProductBuyerRate / 100.0 );
			$prices[] = array( "rate" => $familyProductBuyerRate, 	"price" => $familyProductBuyerPrice );
			
		}
		
		if( $multipriceGroupingPrice != -1 ){
			
			$multipriceGroupingRate = 100.0 - $multipriceGroupingPrice * 100.0 / $basicPrice;
			$prices[] = array( "rate" => $multipriceGroupingRate, 	"price" => $multipriceGroupingPrice );
		
		}
		
		if( $tariffCodePrice != -1 ){
			
			$tariffCodeRate = 100.0 - $tariffCodePrice * 100.0 / $basicPrice;
			//$prices[] = array( "rate" => $tariffCodeRate, 	"price" => $tariffCodePrice );
			
		}
		
		//si code tarif prendre celui-là en priorité
		
		if( $tariffCodePrice > 0.0 ){
			
			$lowerPrice = $tariffCodePrice;
			$highestRate = 0.0;
			
			$article->setReduceRate( $highestRate );
			$article->set( "ref_discount", $highestRate );
			$article->set( "wtprice", $lowerPrice * ( 1.0 + $article->GetVAT() / 100.0 ) );
			
		}
		else{ //sinon récupérer le plus ptit prix
			
			$lowerPrice = $basicPrice;
			$highestRate 	= 0.0;
			$i = 0;
			while( $i < count( $prices ) ){
			
				if( $prices[ $i ][ "price" ] < $lowerPrice ){
					
					$lowerPrice = $prices[ $i ][ "price" ];
					$highestRate = $prices[ $i ][ "rate" ];
					
				}
				
				$i++;
					
			}

			$article->setReduceRate( max( $article->GetReduceRate(), $highestRate ) );
			$article->set( "ref_discount", $highestRate );
			$article->set( "wtprice", $lowerPrice * ( 1.0 + $article->GetVAT() / 100.0 ) );
		
		}
		
	    if( $idbuyer ){ //adm_commercial
	    	
	    	$query = "
			SELECT currency.device AS device 
			FROM currency, buyer 
			WHERE currency.idcurrency = buyer.idcurrency 
			AND buyer.idbuyer = '$idbuyer' 
			LIMIT 1";
				
	    	$db =& DBUtil::getConnection();
	    	$rs = $db->Execute( $query );
	    	
	    	if( $rs === false || !$rs->RecordCount() )
	    		trigger_error( "Impossible de calculer le prix de l'article", E_USER_ERROR );
	    	
	    	$device = $rs->fields( "device" );	
	    
	    }
	    else{
	    
	    	$buyerobject = Session::getInstance()->getDeprecatedBuyer();
	    	$device = $buyerobject->getDevice(); //front office
	    	 
	    }
	    
	    return $lowerPrice * $device;
	    
	}
	    
	//----------------------------------------------------------------------------
 	
 	/**
 	 * Calcul le pourcentage de remise par famille de client et de produit pour un article donné
 	 * @param object $article l'article pour lequel on souhaite le pourcentage de remise
 	 * @return int le pourcentage de remise ( entre 1 et 100 ) s'il existe, sinon -1
 	 */
 	 
	public static function getFamilyProductBuyerPriceRate( &$reference ){
		re
		if( isset( $_GET[ "dev" ] ) )
			echo "<h1>getFamilyProductBuyerPriceRate( $article )</h1>";
			
		
		
		$idarticle = $article->GetId();
		$idbuyer = $article->getIdBuyer();
		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getIdBuyer();
			
		$query = "
		SELECT fpb.rate
		FROM detail d, buyer b, family_product_buyer fpb
		WHERE d.idarticle = '$idarticle'
		AND d.idproduct_family = fpb.idproduct_family
		AND b.idbuyer = '$idbuyer'
		AND b.idcustomer_profile = fpb.idcustomer_profile
		AND (fpb.begin_date <= NOW()
		AND fpb.end_date >= NOW()
		OR fpb.end_date='0000-00-00')
		LIMIT 1";
		
		if( isset( $_GET[ "dev" ] ) )
			echo "<p>$query</p>";
			
		$db = & DBUtil::getConnection();
		
		$rs = $db->Execute( $query );
		if( $rs === false )
			trigger_error( "Impossible de calculer le prix par famille de produits et de clients pour l'artcile '$idarticle'", E_USER_ERROR );
		
		if( !$rs->RecordCount() )
			return -1;
			
		if( isset( $_GET[ "dev" ] ) )
			echo "<p>RESULT : " . $rs->fields( "rate" ) . "</p>";
			
		return $rs->fields( "rate" );
		
	}
	
	//----------------------------------------------------------------------------
 	
 	/**
 	 * Calcul le prix remisé par groupement pour un article donné
 	 * @param object $article l'article pour lequel on souhaite le prix
 	 * @return float le prix remisé s'il existe, sinon -1
 	 */
 	 
 	public static function getMultipriceGroupingPrice( &$article ){
 		
 		
 		
 		if( isset( $_GET[ "dev" ] ) )
			echo "<h1>getMultipriceGroupingPrice( $article )</h1>";
			
 		$idbuyer = $article->getIdBuyer();
 		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getIdBuyer();
 		$idarticle = $article->GetId();
 		
 		//on utilise le champ multiprice.price à la place de multiprice.rate pour cette table :o(
 		//le champ multiprice.idbuyer est soit un id groupement, soit un code tarif mais jamais un idbuyer :o(
 		
 		$query = "
		SELECT m.price
		FROM customer_price m, detail d, buyer b
		WHERE d.idarticle = '$idarticle'
		AND d.reference LIKE m.reference
		AND b.idbuyer = '$idbuyer'
		AND b.idgrouping = m.idbuyer
		AND (m.begin_date <= NOW()
		AND m.end_date >= NOW()
		OR m.end_date='0000-00-00')
		LIMIT 1";
	
		if( isset( $_GET[ "dev" ] ) )
			echo "<p>$query</p>";
		
		$db = & DBUtil::getConnection();
		
		$rs = $db->Execute( $query );
		if( $rs === false )
			trigger_error( "Impossible de calculer le prix par groupement pour l'artcile '$idarticle'", E_USER_ERROR );
		
		if( !$rs->RecordCount() )
			return -1;
			
		if( isset( $_GET[ "dev" ] ) )
			echo "<p>RESULT : " . $rs->fields( "price" ) . "</p>";
			
		return $rs->fields( "price" );
		
 	}
 	
 	//----------------------------------------------------------------------------
 	
 	/**
 	 * Calcul le prix remisé par code tarif pour un article donné
 	 * @param object $article l'article pour lequel on souhaite le prix
 	 * @return float le prix remisé s'il existe, sinon -1
 	 */
 	 
 	public static function getTariffCodePricePrice( $article ){
 		
 		
 		
 		if( isset( $_GET[ "dev" ] ) )
			echo "<h1>getTariffCodePricePrice( $article )</h1>";
			
 		$idbuyer = $article->getIdBuyer();
 		if( !$idbuyer )
			$idbuyer = Session::getInstance()->getIdBuyer();
 		$idarticle = $article->GetId();
 		
 		//on utilise le champ multiprice.price à la place de multiprice.rate pour cette table :o(
 		//le champ multiprice.idbuyer est soit un id groupement, soit un code tarif mais jamais un idbuyer :o(
 		//le code tarif est stocké dans buyer.nav_buyer_2
 		
 		$query = "
		SELECT m.price
		FROM customer_price m, detail d, buyer b
		WHERE d.idarticle = '$idarticle'
		AND d.reference LIKE m.reference
		AND b.idbuyer = '$idbuyer'
		AND b.nav_buyer_2 = m.idbuyer
		AND (m.begin_date <= NOW()
		AND m.end_date >= NOW()
		OR m.end_date='0000-00-00')
		LIMIT 1";
		
		if( isset( $_GET[ "dev" ] ) )
			echo "<p>$query</p>";
			
		$db = & DBUtil::getConnection();
		
		$rs = $db->Execute( $query );
		if( $rs === false )
			trigger_error( "Impossible de calculer le prix par code client et groupement pour l'artcile '$idarticle'", E_USER_ERROR );
		
		if( !$rs->RecordCount() )
			return -1;
			
		if( isset( $_GET[ "dev" ] ) )
			echo "<p>RESULT : " . $rs->fields( "price" ) . "</p>";
			
		return $rs->fields( "price" );
 		
 	}
 	
 	//----------------------------------------------------------------------------
 	
}

?>
