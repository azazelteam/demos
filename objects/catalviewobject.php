<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ...?
 */

include_once( dirname( __FILE__ ) . "/catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );


class CATALVIEW extends CATALBASE
{
	private $Allparents;
	private $Categories;
	private $IdCategories;
	private $Root;
	private $niveaux; // nombre de niveau 0 niveau, 1 niveaux, 2 niv...
	private $idTrade;
	private $tradeName;
	private $catalog_right;
	
	/**
	 * Constructeur
	 * @param int $IdCategory l'id de la catégorie
	 * @param int $nvx nombre de niveau 0=1 niveau, 1=2 niveaux, 2=3 niv...
	 * @param int $idTrade l'id du métier
	 */
	function __construct($IdCategory, $nvx=1, $idTrade = 0 ){
		
		$this->Categories = array();
		$this->IdCategories = array();
		$this->Root = 'Liste vide!';
		$this->niveaux = 1; // nombre de niveau 0 niveau, 1 niveaux, 2 niv...
	
		$this->DB = &Session::getInstance()->GetDB();
		$this->lang = Session::getInstance()->getlang();
		$this->id =  $IdCategory ;
		$this->niveaux = $nvx;
		$this->idTrade = $idTrade;
		
		if( $this->idTrade ){
		
			//get trade name
			
			$lang = Session::getInstance()->GetLang();
			$query = "SELECT trade$lang AS name FROM trade WHERE idtrade = " . $this->idTrade . " LIMIT 1";
			$rs = $this->DB->Execute( $query );
			
			if( $rs->RecordCount() )
				$this->tradeName = $rs->fields["name"];
				
				
		}
			
		//get buyer's catalog_right
		
		$buyer = new DEPRECATEDBUYER();
		$buyer->GetBuyerInfo();
		$this->catalog_right = $buyer->get("catalog_right" );
		
		$this->SetTheProducts();
		
	}
	
	public function getRoot(){ return $this->Root; }
	public function &getCategories(){ return $this->Categories; }
	public function &getIdCategories(){ return $this->IdCategories; }
	public function &getCurrProduct(){ return $this->CurrProduct; }
	public function setCurrProduct( &$product ){ $this->CurrProduct =& $product; }
	public function getProductAttribute( $index, $attribute ){ return $this->products[ $index ][ $attribute ]; }
	
	/**
	 * Charge les produits depuis la BDD
	 * @return int le nombre de produits
	 */
	private function SetTheProducts(){
		
		//Get the parent categories	
		$this->Allparents = $this->DB->GetAll('SELECT DISTINCT idcategoryparent FROM category_link');
		$n =count($this->Allparents);
		for($i=0;$i<$n;$i++) $this->Allparents[$i] = $this->Allparents[$i]['idcategoryparent'];
		
		$rs = $this->DB->Execute('SELECT name'.$this->lang.' as name FROM category WHERE idcategory='.$this->id);
		if(!$rs->EOF){	
			
			if( $this->idTrade )
				$this->Root = $this->tradeName . " -> " . $rs->fields['name'];	
			else $this->Root = $rs->fields['name'];
			
		}
		
		$this->Categories[$this->id]=$this->Root;
		$this->GetProducts($this->id,$this->id, 0);	
		$this->NBProduct = count($this->products);
		
		return $this->NBProduct;
		
	}	
	
	/**
	 * Récupère les produits
	 * @param int $IdCategory l'id de la categorie
	 * @param int $IdMainCategory l'id de la catégorie mère
	 * @param int $Level le niveau
	 * @return void
	 */
	public function GetProducts($IdCategory,$IdMainCategory,$Level)
	{
				
		$name = 'name'.$this->lang;
		
		if( !in_array(0+$IdCategory,$this->Allparents, FALSE) ){
			
			$SQL_select_1 = "SELECT `idcategory` as `idcategorychild`, `$name`";
			$SQL_select_1 .= " FROM `category` WHERE `idcategory`='$IdCategory'";
			$SQL_select_1 .= " AND catalog_right <= " . strval( $this->catalog_right );
			
		}
		else {
			
			$SQL_select_1 = "SELECT catlink.idcategorychild, $name";
			$SQL_select_1 .= " FROM category cat, category_link catlink";
			$SQL_select_1 .= " WHERE catlink.idcategoryparent = $IdCategory";
			$SQL_select_1 .= " AND cat.idcategory = catlink.idcategorychild";
			$SQL_select_1 .= " AND cat.catalog_right <= " . strval( $this->catalog_right );
			$SQL_select_1 .= " ORDER BY catlink.displayorder";
			
		}
			
		
		$rs = $this->DB->Execute($SQL_select_1);
		//trigger_error(showDebug($SQL_select_1,"GetProducts($IdCategory,$IdMainCategory,$Level) => ".'$SQL_select_1','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($SQL_select_1,"GetProducts($IdCategory,$IdMainCategory,$Level) => ".'$SQL_select_1','log'),E_USER_ERROR);
		while(!$rs->EOF) {
			
			$IdCategoryChild = $rs->fields['idcategorychild'];
			
			if( in_array(0+$IdCategoryChild,$this->Allparents, FALSE) ) {				
				
				if( $Level < $this->niveaux ) {
					
					$IdMainCategory = $IdCategoryChild ;
					$this->IdCategories[$IdCategoryChild] = $Level;
					
				} 
				else {	
					
				//$IdMainCategory = $IdCategory ;
				
				}
				$this->Categories[$IdCategoryChild] =  $rs->fields[$name];
				$this->GetProducts($IdCategoryChild,$IdMainCategory,$Level+1);
				
			} else {
								
				if ($Level < $this->niveaux ) {
					
					$IdMainCategory = $IdCategoryChild;
					$this->Categories[$IdMainCategory]= $rs->fields[$name];
					
				}
				
				$aidr = array(); 	
				
				if( $this->idTrade ){
					
					$SQL_select_2 = "SELECT MIN(rp.sellingcost),rp.idarticle FROM product p, detail rp, trade t, trade_product tp";
					$SQL_select_2 .= " WHERE p.idcategory=" . $IdCategoryChild;
					$SQL_select_2 .= " AND p.idproduct = rp.idproduct ";
					$SQL_select_2 .= " AND p.idproduct = tp.idproduct AND tp.idtrade = " . $this->idTrade;
					$SQL_select_2 .= " AND p.catalog_right <= " . strval( $this->catalog_right );
					$SQL_select_2 .= " AND (rp.stock_level>0 OR rp.stock_level=-1 ) AND p.available=1";
					$SQL_select_2 .= " GROUP BY p.idproduct";
				
				}
				else {
					
					$SQL_select_2 = "SELECT MIN(sellingcost),idarticle FROM product, detail";
					$SQL_select_2 .= " WHERE idcategory=" . $IdCategoryChild;
					$SQL_select_2 .= " AND product.idproduct = detail.idproduct ";
					$SQL_select_2 .= " AND catalog_right <= " . strval( $this->catalog_right );
					$SQL_select_2 .= " AND (detail.stock_level>0 OR detail.stock_level=-1 ) AND product.available=1";
					$SQL_select_2 .= " GROUP BY detail.idproduct";
				
				}
				
		
				$res = $this->DB->Execute($SQL_select_2);
				//trigger_error(showDebug($SQL_select_2,"GetProducts($IdCategory,$IdMainCategory,$Level) => ".'$SQL_select_2','sql'),E_USER_NOTICE);
				if( empty($rs) ) 
					trigger_error(showDebug($SQL_select_2,"GetProducts($IdCategory,$IdMainCategory,$Level) => ".'$SQL_select_2','log'),E_USER_ERROR);
				
				if($res->RecordCount()) {
					
					while(!$res->EOF) {
						
						$aidr[] = $res->fields['idarticle'];
						$res->MoveNext();
						
					}
					
					$idr = implode(',',$aidr);		
					$SQL_select_3 = 'SELECT product.idproduct, '.$name.
							' AS name,sellingcost FROM product, detail' .
							' WHERE idarticle in ('.$idr.')  AND product.idproduct = detail.idproduct ORDER BY display_product_order,'.$name ;	
					$res = $this->DB->Execute($SQL_select_3);
					//trigger_error(showDebug($SQL_select_3,"GetProducts($IdCategory,$IdMainCategory,$Level) => ".'$SQL_select_3','sql'),E_USER_NOTICE);
					
					if( empty($rs) ) 
						trigger_error(showDebug($SQL_select_3,"GetProducts($IdCategory,$IdMainCategory,$Level) => ".'$SQL_select_3','log'),E_USER_ERROR);
					
					while(!$res->EOF) {
						
						$ipd = $res->fields['idproduct'];
						
						if( !isset($this->IdCategories[$IdMainCategory]) ) 
							$this->IdCategories[$IdMainCategory]=array();
						
						if(!is_array($this->IdCategories[$IdMainCategory])) 
							$this->IdCategories[$IdMainCategory]=array();
							
						$this->IdCategories[$IdMainCategory][] = $ipd ;
						$this->products[$ipd] = $res->fields;
						$res->MoveNext();
					
					}
				}
			}
			$rs->MoveNext();
		}
	}
	
	/**
	 * Retourne le prix du produit courrant
	 * @param float $rate le taux à appliquer
	 * @return string le prix formatté pour l'affichage
	 */
	 public function GetPrice($rate=1) {
        $i = $this->CurrProduct;
        if( $this->products[$i]['sellingcost'] > 0 )
            return Dictionnary::translate('text_price_list')." ".Util::priceFormat($this->products[$i]['sellingcost']*$rate); //TODO : Internationalisation
        return '';
    }
	 			
	/**
	 * Statistiques
	 * @return string $Str_Insert la requete de stats
	 */
	public function statistique()
	{
	// quantity | idcategory | idproduct | reference
	  $n=$this->NBProduct;
	  $refs= implode(' ',$this->products);
	  $Str_Insert = "'$n','-876','0','this->id'";
	  return  $Str_Insert;
	}
	
	/**
	 * Retourne le nom du métier
	 * @return string le nom du métier
	 */
	public function GetTradeName(){
		
		return $this->idTrade ? $this->tradeName : "";
		
	}
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}


}//EOC
?>
