<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object litige client
 */


include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/Exchange.php" );
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
include_once( dirname( __FILE__ ) . "/Credit.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/ReturnToSender.php" );
include_once( dirname( __FILE__ ) . "/Repayment.php" );
include_once( dirname( __FILE__ ) . "/Invoice.php" );
include_once( dirname( __FILE__ ) . "/Order.php" );


class Litigation{
	
	//---------------------------------------------------------------------------------------------------------------
	/*static data members*/
	
	public static $GOODS_DELIVERY_LOCATION_NONE 	= "none";
	public static $GOODS_DELIVERY_LOCATION_LOCAL 	= "local";
	public static $GOODS_DELIVERY_LOCATION_SUPPLIER = "supplier";
	public static $GOODS_DELIVERY_LOCATION_BOTH 	= "both";
	public static $TYPE_ORDER						= "order";
	public static $TYPE_BILLING_BUYER				= "billing_buyer";
		
	//---------------------------------------------------------------------------------------------------------------
	/*data members*/
	
	/**
	 * @var int $idlitigation le numéro de litige
	 */
	private $idlitigation;

	/**
	 * @var int $idlitigation_motive le motif du litige ( cf. `litigation_motives`.`idlitigation_motive` )
	 */
	private $idlitigation_motive;
	
	/**
	 * @var string $creation_date la date de création du litige
	 */
	private $creation_date;
	
	/**
	 * @var string $comment les commentaires du commercial traitant le litige
	 */
	private $comment;
	
	/**
	 * @var DBSaleItem $item la ligne article litigieuse de la facture
	 */
	private $item;
	
	/**
	 * @var DBSale $sale
	 */
	private $sale;
	
	/**
	 * @var int $quantity la quantité litigieuse facturée
	 */
	private $quantity;
	
	/**
	 * @var Customer $customer le client
	 */
	private $customer;
	
	/**
	 * @var Salesman $salesman le commercial
	 */
	private $salesman;
	
	/**
	 * @var float $management_fees montant des frais de dossier
	 */
	private $management_fees;
	
	/**
	 * @var bool $wayout_require_goods_delivery vaut true si l'issue du litige se fait sur retour de la marchandise
	 * Note : l'issue du litige peut être immédiate bien que la marchandise soit retournée chez l'expéditeur
	 */
	private $wayout_require_goods_delivery;
	 
	 /**
	 * @var bool $editable litige éditable ou non
	 */
	 private $editable;
	 
	 /**
	 * @var ReturnToSender $return_to_sender retourn de marchandise à l'éxpéditeur, null par défaut
	 */
	 private $return_to_sender;
	
	 /**
	 * @var int management_fees_invoice N° de facture pour les frais de dossier
	 */
	 private $management_fees_invoice;
	 
	 /**
	 * @var string typeLitigation , Type de litige ( sur facture, commande ... )
	 */
	 private $typeLitigation;
	 
	 
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * constructor
	 */
	function __construct( $idlitigation , $type = "billing_buyer" ){

		$rsLitigation =& DBUtil::query( "SELECT * FROM litigations WHERE idlitigation = '$idlitigation' LIMIT 1" ) || trigger_error( "Impossible de charger le litige", E_USER_ERROR );
		

		if( !$rsLitigation->RecordCount() ){
				
			trigger_error( "Litige inexistant", E_USER_ERROR );
			exit();
					
		}
		
		
		switch( $type ){
			
			case self::$TYPE_BILLING_BUYER :
				$rs =& DBUtil::query( "SELECT * FROM litigations_billing_buyer lb WHERE lb.idlitigation = '$idlitigation'" ) || trigger_error( "Impossible de charger le litige", E_USER_ERROR );
				if( !$rs->RecordCount() ){
					
					trigger_error( "Litige sur facture inexistant", E_USER_ERROR );
					exit();
					
				}
				
				$this->sale = new Invoice( $rs->fields( "idbilling_buyer" ) );
				$this->item = $rs->fields( "idrow" ) ? $this->sale->getItems()->get( $rs->fields( "idrow" ) - 1 ) : 0;
				
				break;
				
			case self::$TYPE_ORDER :
				$rs =& DBUtil::query( "SELECT * FROM litigations_order lo WHERE lo.idlitigation = '$idlitigation'" ) || trigger_error( "Impossible de charger le litige", E_USER_ERROR );
				if( !$rs->RecordCount() ){
					
					trigger_error( "Litige sur commande inexistant", E_USER_ERROR );
					exit();
					
				}
				
				$this->sale = new Order( $rs->fields( "idorder" ) );
				$this->item = $rs->fields( "idrow" ) ? $this->sale->getItems()->get( $rs->fields( "idrow" ) - 1 ) : 0;
				break;

			default:
				die( "Type de litige inconnu ! " );
				break;
				
		}
		
		$this->typeLitigation					= $type;
		$this->salesman 						= new Salesman( $rsLitigation->fields( "iduser" ) );
		$this->idlitigation 					=  $idlitigation ;
		$this->quantity 						= $rs->fields( "quantity" );
		$this->idlitigation_motive 				= $rsLitigation->fields( "idlitigation_motive" );
		$this->creation_date 					= $rsLitigation->fields( "creation_date" );
		$this->management_fees 					= $rsLitigation->fields( "management_fees" );
		$this->management_fees_invoice 			= $rsLitigation->fields( "management_fees_invoice" );
		$this->comment 							= $rsLitigation->fields( "comment" );
		$this->wayout_require_goods_delivery 	= $rsLitigation->fields( "wayout_require_goods_delivery" );
		$this->editable 						= $rsLitigation->fields( "editable" );
		$this->return_to_sender 				= null;
		
		/*retour de la marchandise*/
		if( $type == self::$TYPE_BILLING_BUYER )
			$this->setReturnToSender();

	}
	
	//---------------------------------------------------------------------------------------------------------------
	/*common getters*/
	
	public function getIdLitigation()				{ return $this->idlitigation; }
	public function getIdInvoice()					{ return $this->sale instanceof Invoice ? $this->sale->getId() : 0; }
	public function getIdOrder()					{ return $this->sale instanceof Order ? $this->sale->getId() : 0; }
	public function getManagementFeesIdInvoice()	{ return $this->management_fees_invoice; }
	public function getQuantity()					{ return $this->quantity; }
	public function getIdLitigationMotive()			{ return $this->idlitigation_motive; }
	public function getCreationDate()				{ return $this->creation_date; }
	public function getComment()					{ return $this->comment; }
	public function &getItem()						{ return $this->item; }
	public function &getSale()						{ return $this->sale; }
	public function &getCustomer() 					{ return $this->sale->getCustomer(); }
	public function &getSalesman() 					{ return $this->sale->getSalesman(); }
	public function getManagementFees()				{ return $this->management_fees; }
	public function getWayoutRequireGoodsDelivery()	{ return $this->wayout_require_goods_delivery ? true : false; }
	public function getGoodsDeliveryLocation()		{ return $this->return_to_sender == null ? self::$GOODS_DELIVERY_LOCATION_NONE : $this->return_to_sender->get( "location" ); }
	public function isEditable()					{ return $this->editable; }
	public function &getReturnToSender()			{ return $this->return_to_sender; }
	public function getType()						{ return $this->typeLitigation; }
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne true si la marchandise a bien été retournée
	 * @return bool
	 */
	public function getGoodsHasBeenDelivered(){

		return $this->return_to_sender != null && $this->return_to_sender->get( "return_date" ) != "0000-00-00 00:00:00";

	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne true si la marchandise a bien été retournée et validée par la magazinier
	 * @return bool
	 */
	public function getGoodsDeliveryHasBeenValidated(){
		
		return $this->return_to_sender != null && $this->return_to_sender->get( "return_date" ) != "0000-00-00 00:00:00" && $this->return_to_sender->get( "validated" ) == 1;

	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nom d'échanges qui ont été créés pour ce litige
	 * @access public
	 * @return bool
	 */
	public function getExchangeCount(){
		
		$query = "
		SELECT COUNT(*) AS `count`
		FROM litigations l, credits c, exchanges e
		WHERE l.idlitigation = '{$this->idlitigation}'
		AND l.idlitigation = c.idlitigation
		AND c.idcredit = e.idcredit";
		
		return DBUtil::query( $query )->fields( "count" );
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nombre d'avoirs qui ont été créés pour ce litige
	 * @access public
	 * @return int
	 */
	public function getCreditCount(){
		
		$query = "
		SELECT COUNT(*) AS `count`
		FROM litigations l, credits c
		WHERE l.idlitigation = '{$this->idlitigation}'
		AND l.idlitigation = c.idlitigation";

		return DBUtil::query( $query )->fields( "count" );
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nombre de remboursements qui ont été créés pour ce litige
	 * @access public
	 * @return int
	 */
	public function getRepaymentCount(){
		
		$query = "
		SELECT COUNT(*) AS `count`
		FROM litigations l, credits c, repayments r
		WHERE l.idlitigation = '{$this->idlitigation}'
		AND l.idlitigation = c.idlitigation
		AND c.idcredit = r.idcredit";
		
		return DBUtil::query( $query )->fields( "count" );
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	/*common setters*/
	
	public function setIdLitigationMotive( $idlitigation_motive )	{ if( !$this->editable ) return; $this->idlitigation_motive 			=  $idlitigation_motive ; }
	public function setComment( $comment )							{ if( !$this->editable ) return; $this->comment 						= $comment; }
	public function setManagementFees( $managementFees )			{ if( !$this->editable ) return; $this->management_fees 				= $managementFees; }
	public function setWayoutRequireGoodsDelivery( $require )		{ if( !$this->editable ) return; $this->wayout_require_goods_delivery 	= $require ? 1 : 0; }
	public function setQuantity( $quantity ) 						{ if( !$this->editable ) return; $this->quantity 						= max( 1, min( $quantity, $this->item ? $this->item->get( "quantity" ) : 0 ) ); }
	public function setSalesman( Salesman $salesman )				{ if( !$this->editable ) return; $this->salesman = $salesman; }
	 
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Définit le lieu de retour de la marchandise
	 * @param string $location peut prendre les valeurs $GOODS_DELIVERY_LOCATION_NONE, $GOODS_DELIVERY_LOCATION_LOCAL, $GOODS_DELIVERY_LOCATION_SUPPLIER ou $GOODS_DELIVERY_LOCATION_BOTH
	 * @return void
	 */
	public function setGoodsDeliveryLocation( $location ){
		
		if( !$this->editable )
			return;
			
		if( $location != self::$GOODS_DELIVERY_LOCATION_NONE
			&& $location != self::$GOODS_DELIVERY_LOCATION_LOCAL
			&& $location != self::$GOODS_DELIVERY_LOCATION_SUPPLIER
			&& $location != self::$GOODS_DELIVERY_LOCATION_BOTH ){
				
			trigger_error( "Unknown location type", E_USER_ERROR );
			return;
			
		}
		
		self::deleteReturnToSender( $this->idlitigation );
		$this->return_to_sender = null;
		
		if( $location == self::$GOODS_DELIVERY_LOCATION_NONE )
			return;
		
		$this->createReturnToSender();
		$this->return_to_sender->set( "location", $location );
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Sauvegarde le litige
	 * @return void
	 */
	public function save(){

		if( !$this->editable )
			return;
	
		$query = "UPDATE litigations 
				  SET 	iduser = '" . $this->salesman->get( "iduser" ) . "', 
						management_fees = '{$this->management_fees}',
						idlitigation_motive = '{$this->idlitigation_motive}',
						wayout_require_goods_delivery = '{$this->wayout_require_goods_delivery}',
						`comment` = " . DBUtil::quote( $this->comment ) . "
				  WHERE idlitigation = '{$this->idlitigation}'
				  LIMIT 1";
						
			
		DBUtil::query( $query ) || triger_error( "Impossible de mettre à jour le litige", E_USER_ERROR );
		
		switch ( $this->typeLitigation ){
			case self::$TYPE_BILLING_BUYER :
				$query = "
				UPDATE litigations_billing_buyer
				SET idbilling_buyer = '" . $this->sale->get( "idbilling_buyer" ) . "',
					idrow = '" . ( $this->item ? $this->item->get( "idrow" ) : 0 ) . "',
					quantity = '{$this->quantity}'
				WHERE idlitigation = '{$this->idlitigation}'
				LIMIT 1";
				
				DBUtil::query( $query ) || triger_error( "Impossible de mettre à jour le litige sur facture", E_USER_ERROR );
				
				$query = "
				
				";
				break;
				
			case self::$TYPE_ORDER :
				$query = "
				UPDATE litigations_order
				SET idorder = '" . $this->sale->get( "idorder" ) . "',
					idrow = '" . ( $this->item ? $this->item->get( "idrow" ) : 0 ) . "',
					quantity = '{$this->quantity}'
				WHERE idlitigation = '{$this->idlitigation}'
				LIMIT 1";
								
				DBUtil::query( $query ) || triger_error( "Impossible de mettre à jour le litige sur commande", E_USER_ERROR );
				break;
				
			default:
				die( "Type de litige inconnu ! " );
				break;
		}
			

		if( $this->return_to_sender != null )
			$this->return_to_sender->save();
			
	}

	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Valide / vérouille le litige
	 * @return void
	 */
	public function validate(){
		
		if( !$this->editable )
			return;
		$this->save();
		$this->editable = false;
		
		DBUtil::query( "UPDATE litigations SET `editable` = FALSE WHERE idlitigation = '{$this->idlitigation}' LIMIT 1" ) || trigger_error( "Impossible de valider le litige", E_USER_ERROR );
		
		//$this->invoiceManagementFees();
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau litige pour une ligne article de document donnée
	 * @param Item $item la ligne article litigieuse
	 * @return Litigation un nouveau litige
	 */
	public static function &create( &$item = 0 , $type ){
		  // ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'litigations';
		$idlitigation = TradeFactory::Indexations($table);
		
		$query = "
		INSERT INTO litigations(idlitigation, iduser , creation_date )VALUES( '$idlitigation','" . User::getInstance()->getId() . "' , NOW() )";
		DBUtil::query( $query ) || trigger_error( "Impossible de créer le litige", E_USER_ERROR );
		
		//$idlitigation = DBUtil::getInsertID();

		$idrow = $item ? $item->get( "idrow" ) : 0;
		$quantity = $item ? $item->get( "quantity" ) : 0;
		
		switch ( $type ){
			case self::$TYPE_BILLING_BUYER :

				$idbilling_buyer = $item ? $item->get( "idbilling_buyer" ) : $_REQUEST[ 'idbilling_buyer' ];
				
				$query = "
				INSERT INTO litigations_billing_buyer ( idlitigation , idbilling_buyer , idrow , quantity )
				VALUES (
					'" . $idlitigation . "',
					'" . $idbilling_buyer . "',
					'" . $idrow . "',
					'" . $quantity . "'
				)";
				
				DBUtil::query( $query )  || trigger_error( "Impossible de créer le litige sur facture", E_USER_ERROR );								
				break;
				
			case self::$TYPE_ORDER :

				$idorder = $item ? $item->get( "idorder" ) : $_REQUEST[ 'idorder' ];
				
				$query = "
				INSERT INTO litigations_order ( idlitigation , idorder , idrow , quantity )
				VALUES (
					'" . $idlitigation . "',
					'" . $idorder . "',
					'" . $idrow . "',
					'" . $quantity . "'
				)";
				
				DBUtil::query( $query ) || trigger_error( "Impossible de créer le litige sur commande", E_USER_ERROR );				
				break;
			
			default:
				die( "Type de litige inconnu ! " );
				break;
		}
		
		$litigation = new Litigation( $idlitigation , $type );
		
		$litigation->setQuantity( ( $item ? $item->get( "quantity" ) : 0 ) );
		
		$litigation->save();

		return $litigation;
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	private function allowWayoutCreation(){
		
		return $this->editable && ( !$this->getWayoutRequireGoodsDelivery() || $this->getGoodsHasBeenDelivered() );

	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	
	/**
	 * Supprime un litige ainsi que toutes ses issues ( avoirs, échanges, remboursements )
	 * @static
	 * @param int $idlitigation
	 * @param string $type ( Litigation::$TYPE_ORDER ou Litigation::$TYPE_BILLING_BUYER )
	 * @return true en cas de succès, sinon false
	 */
	public static function delete( $idlitigation, $type ){
		
		if( !DBUtil::getDBValue( "editable", "litigations", "idlitigation", intval( $idlitigation ) ) )
			return;
			
		self::deleteRepayments( $idlitigation );		/*suppression des remboursements*/
		self::deleteCredits( $idlitigation );			/*suppression des avoirs*/
		self::deleteExchanges( $idlitigation );			/*suppression des échanges*/
		self::deleteReturnToSender( $idlitigation );	/*suppression des retour marchandise*/
		
		/*suppression du litige*/
		switch ( $type ){
			case self::$TYPE_BILLING_BUYER :
				DBUtil::query( "DELETE FROM litigations_billing_buyer WHERE idlitigation = '" .  $idlitigation  . "'" ) || trigger_error( "Impossible de supprimer le litige sur facture", E_USER_ERROR );
				break;
				
			case self::$TYPE_ORDER :
				DBUtil::query( "DELETE FROM litigations_order WHERE idlitigation = '" .  $idlitigation  . "'" ) || trigger_error( "Impossible de supprimer le litige sur commande", E_USER_ERROR );
				break;
				
			default:
				die( "Type de litige inconnu ! " );
				break;
		}
				
		DBUtil::query( "DELETE FROM litigations WHERE idlitigation = '" .  $idlitigation  . "' LIMIT 1" ) || trigger_error( "Impossible de supprimer le litige", E_USER_ERROR );
		
		return DBUtil::getAffectedRows() == 1;
		
	}

	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime les avoirs d'un litige donné
	 * Ne supprime pas les avoirs liés à des commandes d'échange
	 * Ne supprime pas les avoirs liés à des remboursements
	 * @static
	 * @access private
	 * @param $idlitigation le numéro de litige
	 * @return void
	 */
	private static function deleteCredits( $idlitigation ){
		
		$query = "
		SELECT idcredit
		FROM credits
		WHERE idlitigation = '$idlitigation'
		AND idcredit NOT IN( SELECT ex.idcredit FROM exchanges ex, credits c WHERE ex.idcredit = c.idcredit AND c.idlitigation = '$idlitigation' )
		AND idcredit NOT IN( SELECT r.idcredit FROM repayments r, credits c WHERE r.idcredit = c.idcredit AND c.idlitigation = '$idlitigation' )";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
		
			Credit::delete( $rs->fields( "idcredit" ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime les échanges d'un litige donné
	 * @static
	 * @access private
	 * @param $idlitigation le numéro de litige
	 * @return void
	 */
	private static function deleteExchanges( $idlitigation ){

		$rs =& DBUtil::query( "SELECT ex.idexchange FROM exchanges ex, credits c WHERE ex.idcredit = c.idcredit AND c.idlitigation = '$idlitigation'" );
		
		while( !$rs->EOF() ){
		
			Exchange::delete( $rs->fields( "idexchange" ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime les remboursements d'un litige donné
	 * @static
	 * @access private
	 * @param $idlitigation le numéro de litige
	 * @return void
	 */
	private static function deleteRepayments( $idlitigation ){
		
		$rs =& DBUtil::query( "SELECT r.idrepayment FROM repayments r, credits c WHERE r.idcredit = c.idcredit AND c.idlitigation = '" .  $idlitigation . "'" );
		
		while( !$rs->EOF() ){
		
			Repayment::delete( $rs->fields( "idrepayment" ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	private function setReturnToSender(){
		
		$query = "
		SELECT rts.idorder_supplier, rts.idrow
		FROM litigations_billing_buyer l, 
			billing_buyer_row bbr,
			bl_delivery bl,
			bl_delivery_row blr, 
			order_supplier_row osr,
			return_to_sender rts
		WHERE l.idlitigation = '{$this->idlitigation}'
		AND bbr.idbilling_buyer = '" . $this->sale->get( "idbilling_buyer" ) . "'
		AND bbr.idrow = '" . ( $this->item ? $this->item->get( "idrow" ) : 0 ) . "'
		AND bl.idbilling_buyer = bbr.idbilling_buyer
		AND bl.idbl_delivery = blr.idbl_delivery
		AND blr.idorder_row = bbr.idorder_row
		AND osr.idorder_supplier = bl.idorder_supplier
		AND osr.idorder_row = blr.idorder_row
		AND rts.idorder_supplier = osr.idorder_supplier
		AND rts.idrow = osr.idrow";
		
		$rs =& DBUtil::query( $query ); 
		
		if( !$rs->RecordCount() )
			return;
		
		$this->return_to_sender = new ReturnToSender( $rs->fields( "idorder_supplier" ), $rs->fields( "idrow" ) );
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime les retours marchandise d'un litige donné
	 * @static
	 * @access private
	 * @param $idlitigation le numéro de litige
	 * @return void
	 */
	private static function deleteReturnToSender( $idlitigation ){
		
		$query = "
		SELECT rts.idorder_supplier, rts.idrow
		FROM litigations_billing_buyer l, 
			billing_buyer_row bbr,
			bl_delivery bl,
			bl_delivery_row blr, 
			order_supplier_row osr,
			return_to_sender rts
		WHERE l.idlitigation = '$idlitigation'
		AND bbr.idbilling_buyer = l.idbilling_buyer
		AND bbr.idrow = l.idrow
		AND bl.idbilling_buyer = bbr.idbilling_buyer
		AND bl.idbl_delivery = blr.idbl_delivery
		AND blr.idorder_row = bbr.idorder_row
		AND osr.idorder_supplier = bl.idorder_supplier
		AND osr.idorder_row = blr.idorder_row
		AND rts.idorder_supplier = osr.idorder_supplier
		AND rts.idrow = osr.idrow
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( !$rs->RecordCount() )
			return;
									
		$query = "
		DELETE FROM return_to_sender 
		WHERE idorder_supplier = '" . $rs->fields( "idorder_supplier" ) . "' 
		AND idrow = '" . $rs->fields( "idrow" ) . "' 
		LIMIT 1";
		
		DBUtil::query( $query ) || trigger_error( "Impossible de supprimer les retours de marchandise", E_USER_ERROR );

	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Prévoit un retour de marchandise
	 * @access private
	 * @return void
	 */
	private function createReturnToSender(){
	
		if( !$this->editable )
			return;
		
		$query = "
		SELECT osr.idorder_supplier, osr.idrow
		FROM litigations_billing_buyer l, 
			billing_buyer_row bbr,
			bl_delivery bl,
			bl_delivery_row blr, 
			order_supplier_row osr
		WHERE l.idlitigation = '{$this->idlitigation}'
		AND bbr.idbilling_buyer = l.idbilling_buyer
		AND bbr.idrow = l.idrow
		AND bl.idbilling_buyer = bbr.idbilling_buyer
		AND bl.idbl_delivery = blr.idbl_delivery
		AND blr.idorder_row = bbr.idorder_row
		AND osr.idorder_supplier = bl.idorder_supplier
		AND osr.idorder_row = blr.idorder_row";

		$rs =& DBUtil::query( $query );
		
		$query = "
		INSERT INTO return_to_sender(
			idorder_supplier,
			idrow,
			quantity,
			location
		) VALUES (
			'" . $rs->fields( "idorder_supplier" ) . "',
			'" . $rs->fields( "idrow" ) . "',
			'{$this->quantity}',
			'" . self::$GOODS_DELIVERY_LOCATION_LOCAL . "'
		)";

		$rs =& DBUtil::query( $query ) || trigger_error( "Impossible d'enregistrer le retour de marchandise", E_USER_ERROR );
		
		$this->setReturnToSender();
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * créé une nouvelle issue au litige de type 'avoir'
	 * @return une référence vers le nouvel avoir créé
	 */
	public function createCredit(){

		if( !$this->allowWayoutCreation() )
			return false;
		
		return TradeFactory::createLitigationCredit( $this );
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * créé une nouvelle issue au litige de type 'échange' ( un échange = un avoir + une nouvelle commande )
	 * @return void
	 */
	public function &createExchange(){
		
		if( !$this->allowWayoutCreation() )
			return;
			
		$invoice = new Invoice( $this->sale->get( "idbilling_buyer" ) );
		
		$credit = TradeFactory::createLitigationCredit( $this );
		$order 	= TradeFactory::createOrder( $this->sale->get( "idbuyer" ), $invoice->get( "idcontact" ) );

		/* ligne article facturée */
		
		$invoicedOrder = new Order( $invoice->get( "idorder" ) );
		$invoicedOrderItem =& $invoicedOrder->getItemAt( $this->item->get( "idorder_row" ) - 1 );
		
		/* clonage de la ligne article facturée */
		
		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$order->addArticle( 
		
			new CArticle( 
			
				$invoicedOrderItem->get( "idarticle" ), 
				$invoicedOrderItem->get( "idcolor" ), 
				$invoicedOrderItem->get( "idtissus" )
				
			), $invoicedOrderItem->get( "quantity" )
			
		);
		
		$item =& $order->getItemAt( 0 );
		$item->duplicate( $invoicedOrder , $this->item->get( "idorder_row" ) - 1 );
		$item->set( "quantity", $this->quantity );
		
		$order->setInvoiceAddress( $invoicedOrder->getInvoiceAddress() );
		$order->setForwardingAddress( $invoicedOrder->getForwardingAddress() );
		
		$order->save();
		
		$exchange =& Exchange::create( $credit->getId(), $order->getId() );
			
		return $exchange;
			
	 }
	 
	 //---------------------------------------------------------------------------------------------------------------
	
	/**
	 * créé une nouvelle issue au litige de type 'remboursement'
	 * @param float $amount le montant à rembourser
	 * @param int $idpayment le mode de paiement ( cf. `payment`.`idpayment` )
	 * @return bool true en cas de succès, sinon false
	 */
	 public function createRepayment( $amount, $idpayment, $idbuyer ){
		
		if( !$this->allowWayoutCreation() )
			return;
	
		$credit =TradeFactory::createLitigationCredit( $this );
		$repayment = Repayment::create( $credit->getId(), floatval( $amount ),  $idpayment ,  $idbuyer  );
		
		return $repayment;
		
	 }
	 
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime un remboursement
	 * @param int $idrepayment le n° de remboursement
	 * @return void
	 */
	public function dropRepayment( $idrepayment, $dropCredit = true  ){

		if( !$this->editable )
			return;
			
		$repayment = new Repayment(  $idrepayment  );
		
		if( $dropCredit && !$repayment->getCredit()->get( "editable" ) ){
		
			trigger_error( "Impossible de supprimer un remboursement dont l'avoir a été confirmé", E_USER_ERROR );
			
			return false;
				
		}

		if( $dropCredit )
			Credit::delete( $repayment->getCredit()->getId() );
		
		DBUtil::query( "DELETE FROM repayments WHERE idrepayment = '" .  $idrepayment  . "' LIMIT 1" );
		
		return DBUtil::getAffectedRows() == 1;
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime un échange
	 * Conduit à la suppression de la commande et de l'avoir qui constituent l'échange
	 * @param int $idexchange le n° d'échange
	 * @param bool $dropCredit supprimer également l'avoir ou non
	 * @return bool true en cas de succès, sinon false;
	 */
	public function dropExchange( $idexchange, $dropCredit = true ){
		
		if( !$this->editable )
			return;
			
		$exchange = new Exchange(  $idexchange  );
		
		if( $dropCredit && !$exchange->getCredit()->get( "editable" ) ){
		
			trigger_error( "Impossible de supprimer un échange dont l'avoir a été confirmé", E_USER_ERROR );
			
			return false;
				
		}
		
		if( $exchange->getOrder()->get( "status" ) != Order::$STATUS_TODO && $exchange->getOrder()->get( "status" ) != Order::$STATUS_IN_PROGRESS ){
		
			trigger_error( "Impossible de supprimer un échange dont la commande a été confirmée", E_USER_ERROR );
			
			return false;
			
		}
		
		Order::delete( $exchange->getOrder()->getId() );
		
		if( $dropCredit )
			Credit::delete( $exchange->getCredit()->getId() );
		
		DBUtil::query( "DELETE FROM exchanges WHERE idexchange = '" .  $idexchange  . "' LIMIT 1" );
		
		return DBUtil::getAffectedRows() == 1;
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
	public function toXML(){
		
		$serializableDataMembers = array( 
		
			"idlitigation" 			=> $this->idlitigation,
			"iduser"  				=> $this->salesman->get( "iduser" ),
			"quantity" 				=> $this->quantity,
			"idlitigation_motive" 	=> $this->idlitigation_motive,
			"creation_date" 		=> Util::dateFormatEu( $this->creation_date ),
			"management_fees" 		=> Util::numberFormat( $this->management_fees ),
			"allow_wayout_creation" => $this->allowWayoutCreation() ? "1" : "0"
			
		);
		
		$document = new DomDocument( "1.0", "utf-8" );
	
		$root = $document->createElement( Util::doNothing( "litigation" ) );
	
		foreach( $serializableDataMembers as $dataMember => $value ){
			
			$element 	= $document->createElement( Util::doNothing( $dataMember ) );
			$attribute 	= $document->createAttribute( Util::doNothing( "value" ) ); 
			
			$attribute->appendChild( $document->createTextNode( Util::doNothing( $value ) ) );
			$element->appendChild( $attribute );
			
			$root->appendChild( $element );
		
		}
		
		$element 	= $document->createElement( Util::doNothing( "comment" ) );
		$cddata 	= $document->createCDATASection( Util::doNothing( $this->comment ) );
		$element->appendChild( $cddata );
		$root->appendChild( $element );
		
		$document->appendChild( $root );

		return $document->saveXML();
	
	}

	//---------------------------------------------------------------------------------------------------------------
	
	/**
	 * Facturer les frais de dossier
	 * @access private
	 * @return void
	 */
	private function invoiceManagementFees(){

		if( !$this->management_fees )
			return;

		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		/* facture litigieuse */
		
		$contentiousInvoice = new Invoice( $this->sale->get( "idbilling_buyer" ) );
		/* création facture pour les frais de dossier */
		
		
		// ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'billing_buyer';
		$idbilling_buyer = TradeFactory::Indexations($table);
		
		$query = "
		INSERT INTO `billing_buyer` (
		idbilling_buyer,
			idorder, 
			iduser, 
			idbuyer, 
			idcontact,
			DateHeure,
			`status`
		) VALUES ( 
'" .$idbilling_buyer. "', 
			'0', 
			'" . User::getInstance()->get( "iduser" ) . "', 
			'" . $this->getCustomer()->getAddress()->get("idbuyer") ."', 
			'" .  $this->getCustomer()->getAddress()->get("idcontact") ."', 
			NOW(),
			'" . Invoice::$STATUS_INVOICED . "'
		)";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false ){
			
			trigger_error( "Impossible de facturer les frais de dossier", E_USER_ERROR );
			exit();
			
		}
				// ------- Mise à jour de l'id gestion des Index  #1161
		//$idbilling_buyer = DBUtil::getInsertID();
		
		/* sauvegarde des frais de dossier */
		
		$fields = array(
		
			"idbilling_buyer"	=> $idbilling_buyer,
			"idrow"				=> 1,
			"idorder_row" 		=> 0,
			"idarticle" 		=> 0,
			"quantity"      	=> 1,
			"unit_price" 		=> $this->management_fees,
			"reference" 		=> "Frais de dossier",
			"unit" 				=> 1,
			"lot" 				=> 1,
			"designation"		=> "Frais de dossier",
			"summary"			=> "Frais de dossier",
			"unit_cost"			=> 0.0,
			"unit_cost_rate"	=> 0.0,
			"unit_cost_amount"	=> 0.0,
			"discount_price"	=> $this->management_fees,
			"idcategory"		=> 0,
			"idsupplier"		=> 0,
			"vat_rate"			=> $this->sale->get( "charge_vat_rate" )

		);
		
		foreach( $fields as $key => $value )
			$fields[ $key ] = DBUtil::quote( $value );
			
		$query = "
		INSERT INTO `billing_buyer_row` ( `" . implode( "`,`", array_keys( $fields ) ) . "` )
		VALUES( " . implode( ",", array_values( $fields ) ) . " )";
	
		$rs =& DBUtil::query( $query );


		$invoice = new Invoice( $idbilling_buyer );
	
		$invoice->setForwardingAddress( $contentiousInvoice->getForwardingAddress() );

		$invoice->setInvoiceAddress( $contentiousInvoice->getInvoiceAddress() );
			
		$invoice->set( "idcurrency", 1 );
		$invoice->set( "idcommercant", User::getInstance()->getId() );
		$invoice->set( "idpayment", DBUtil::getParameter( "default_idpayment" ) );
		$invoice->set( "idpayment_delay", DBUtil::getParameter( "default_idpayment_delay" ) );
		$invoice->set( "factor", "" );
		$invoice->set( "charge_vat_rate", $this->sale->get( "charge_vat_rate" ) );
		$invoice->set( "no_tax_export", $this->sale->get( "no_tax_export" ) );
		$invoice->set( "charge_auto", 0 );
		$invoice->set( "global_delay", 0 );
		$invoice->set( "idcurrency", 1 );
		$invoice->save();

		$this->management_fees_invoice = $invoice->getId();
		
		$rs =& DBUtil::query( "UPDATE litigations SET management_fees_invoice = '" . $this->management_fees_invoice . "' WHERE idlitigation = '" . $this->idlitigation . "' LIMIT 1" );
		

		return DBUtil::getAffectedRows() == 1;
		
	}
	
	//---------------------------------------------------------------------------------------------------------------
	
}

?>