<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresse du client par défaut
 * @todo cette classe doit être supprimée. Elle permet de manipuler l'adresse par défaut
 * du client comme une adresse de livraison ou de facturation. => revoir la gestion des adresses
 */

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/EditableAddress.php" );		
include_once( dirname( __FILE__ ) . "/Session.php" );
	

final class DefaultAddress extends EditableAddress{
	
	//----------------------------------------------------------------------------
	//static data members
	
	private static $TABLE_NAME 			= "buyer";
	private static $PRIMARY_KEY_NAME 	= "idbuyer";
	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var int $idbuyer le numéro du client
	 * @access private
	 */
	private $idbuyer;
	/**
	 * @var int $idcontact le numéro du contact
	 * @access private
	 */
	private $idcontact;

	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idbuyer le numéro du client
	 */
	function __construct( $idbuyer, $idcontact = 0 ){
		
		$this->idbuyer 		= $idbuyer;
		$this->idcontact 	= $idcontact;
		$this->fields 		= array();
		
		$this->load();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
     * Retourne le nom de la table principale
     * @access public
     * @abstract
     * @return string
     */
	protected final function getTableName(){ return DefaultAddress::$TABLE_NAME; }
	
	//----------------------------------------------------------------------------
	/**
     * Retourne le nom de la clé primaire dans la table principale
     * @access public
     * @abstract
     * @return string
     */
	protected final function getPrimaryKeyName(){ return DefaultAddress::$PRIMARY_KEY_NAME; }
	
	//----------------------------------------------------------------------------
	
	/**
     * Retourne la valeur de la clé primaire dans la table principale
     * @access public
     * @abstract
     * @return string
     */
	public final function getPrimaryKeyValue(){ return $this->idbuyer; }
	
	//----------------------------------------------------------------------------

	/**
     * Ne pas utiliser!!!
     */

	public function update(){ return; /*@todo : pas d'édition tant que les addresses clients ne sont pas gérées*/ }
	public function insert(){ return; /*@todo : pas d'insertion tant que les addresses clients ne sont pas gérées*/ }
	
	//----------------------------------------------------------------------------
	//surcharges
	
	/**
	 * Charge l'adresse depuis la base de données
	 * @access protected
	 * @return void
	 */
	protected function load(){

		$lang = Session::getInstance()->getLang();
		
		$query = "
		SELECT c.firstname,
			c.lastname,
			c.phonenumber,
			c.faxnumber,
			c.title,
			b.adress,
			b.adress_2,
			b.city,
			b.zipcode,
			b.idstate,
			b.company,
			s.name$lang AS `state`
		FROM buyer b, contact c, state s
		WHERE b.idbuyer = '{$this->idbuyer}'
		AND c.idbuyer = b.idbuyer
		AND c.idcontact = '{$this->idcontact}'
		AND s.idstate = b.idstate
		LIMIT 1";
		
		
		$rs =& DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>