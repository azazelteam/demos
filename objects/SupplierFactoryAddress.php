<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresse usine fournisseur
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/EditableAddress.php" );


final class SupplierFactoryAddress extends DBObject implements EditableAddress{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $idsupplier_factory identifiant unique
	 * @access private
	 */
	private $idsupplier_factory;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idfactory_address
	 */
	function __construct( $idsupplier_factory ){
		
		$this->idsupplier_factory =  $idsupplier_factory ;
		
		parent::__construct( "supplier_factory", false, "idsupplier_factory", $this->idsupplier_factory );

	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* Address interface */
	
	public function getCompany(){ 		return $this->recordSet[ "company" ]; }
	public function getFirstName(){ 	return $this->recordSet[ "firstname" ]; }
	public function getLastName(){ 		return $this->recordSet[ "lastname" ]; }
	public function getGender(){ 		return $this->recordSet[ "idtitle" ]; }
	public function getPhonenumber(){ 	return $this->recordSet[ "phonenumber" ]; }
	public function getFaxnumber(){ 	return $this->recordSet[ "faxnumber" ]; }
	public function getGSM(){ 			return $this->recordSet[ "gsm" ]; }
	public function getEmail(){ 		return $this->recordSet[ "email" ]; }
	public function getAddress(){ 		return $this->recordSet[ "address" ]; }
	public function getAddress2(){ 		return $this->recordSet[ "address2" ]; }
	public function getZipcode(){ 		return $this->recordSet[ "zipcode" ]; }
	public function getCity(){ 			return $this->recordSet[ "city" ]; }
	public function getIdState(){ 		return $this->recordSet[ "idstate" ]; }

	/* ---------------------------------------------------------------------------------------------------- */
	/*EditableAddress interface */

	public function getId(){ 						return $this->idsupplier_factory; }
	public function setCompany( $company ){ 		$this->recordSet[ "company" ] 		= $company; }
	public function setFirstName( $firstname ){ 	$this->recordSet[ "firstname" ] 	= $firstname; }
	public function setLastName( $lastname ){ 		$this->recordSet[ "lastname" ] 		= $lastname; }
	public function setGender( $idtitle ){ 			$this->recordSet[ "idtitle" ] 		= intval( $idtitle ); }
	public function setPhonenumber( $phonenumber ){ $this->recordSet[ "phonenumber" ] 	= $phonenumber; }
	public function setFaxNumber( $faxnumber ){ 	$this->recordSet[ "faxnumber" ] 	= $faxnumber; }
	public function setGSM( $gsm ){ 				$this->recordSet[ "gsm" ] 			= $gsm; }
	public function setEmail( $email ){ 			$this->recordSet[ "email" ] 		= $email; }
	public function setAddress( $address ){ 		$this->recordSet[ "address" ] 		= $address; }
	public function setAddress2( $address2 ){ 		$this->recordSet[ "address2" ] 		= $address2; }
	public function setZipcode( $zipcode ){ 		$this->recordSet[ "zipcode" ] 		= $zipcode; }
	public function setCity( $city ){ 				$this->recordSet[ "city" ] 			= $city; }
	public function setIdState( $idstate ){ 		$this->recordSet[ "idstate" ] 		= intval( $idstate ); }

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return bool
	 */
	public function allowDelete(){
		
		global $GLOBAL_DB_NAME;
		
		$rs =& DBUtil::query( "SHOW TABLES" );
	
		while( !$rs->EOF() ){
		
			$tablename = $rs->fields( "Tables_in_$GLOBAL_DB_NAME" );
			
			if( $tablename != "supplier_factory" ){
				
				$rs2 =& DBUtil::query( "SHOW COLUMNS FROM `$tablename` LIKE 'idsupplier_factory'" );
				
				if( $rs2->RecordCount() ){
				
					if( DBUtil::query( "SELECT idsupplier_factory FROM `$tablename` WHERE idsupplier_factory = '{$this->idsupplier_factory}' LIMIT 1" )->RecordCount() )
						return false;
						
				}
			
			}
			
			$rs->MoveNext();
			
		}
	
		return true;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @param int $idsupplier_factory
	 * @return void
	 */
	public static function delete( $idsupplier_factory ){
		
		DBUtil::query( "DELETE FROM supplier_factory WHERE idsupplier_factory = '" .  $idsupplier_factory  . "' LIMIT 1" );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idsupplier
	 * @return EditableAddress
	 */
	public static function create( $idsupplier ){
		
		DBUtil::query( "INSERT INTO `supplier_factory` ( idsupplier ) VALUES( '" .  $idsupplier  . "' )" ); 
		
		return new self( DBUtil::getInsertID() );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return SupplierFactoryAddress
	 */
	public function duplicate(){
		
		$duplicate = self::create( $this->recordSet[ "idsupplier" ] );
		
		foreach( $this->recordSet as $key => $value ){

			if( !in_array( $key, array_keys( $this->primaryKeys ) ) )
				$duplicate->set( $key, $value );
			
		}
		
		$duplicate->save();

		return $duplicate;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>