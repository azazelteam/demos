<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object recherche par critères / filtres
 */
 

set_time_limit( 60 );


class Filter {

    /*
     * Elements de la requete principale
     */
    private $select;
    private $from;
    private $where;
    private $group_by;
    private $having;
    private $order_by;
    private $limit;

    /*
     * Champs de la requete principale
     */
    private $criterias;

    /*
     * Tableau de résultats pour arborescence multiselect
     */
    private $categories = array();
    private $products = array();

    /*
     * RecordSet des requetes des listes déroulantes
     */
    private $rs_act;
    private $rs_cat;
    private $rs_eff;
    private $rs_naf;
    private $rs_state;
    private $rs_user;
    private $tabProd;
    private $tabCat;
    private $dateMaxi;
    private $dateMini;

/*
 * Constructeur
 */
    public function __construct( $criterias ){

        /*
         * Initialisation des éléments de la requête principale
         */
        $this->select = "DISTINCT (b.idbuyer), b.contact"; // champs que l'on veut garder
        $this->from = "buyer b"; // table(s) utilisée(s)
        $this->where = "b.newsletter = 1"; //condition(s) obligatoire(s)
        $this->group_by = "";
        $this->having = "";
        $this->order_by = "";
        $this->limit = 10;
		$this->tabCat = array();
		$this->dateMaxi = null;
		$this->dateMini = null;
        /*
         * Attribution du tableau récupérant tous les critères
         */
        $this->criterias = $criterias;

        /*
         * Exécution des méthodes de tri
         */
        $this->applyFilters();
    }

/*
 * Remplit l'attribut where de toutes les conditions demandées
 * Implémentation du select, from, where etc ...
 */
    private function applyFilters(){
    	global $GLOBAL_START_PATH;
		include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
        $criteriasArray = array();

        $i = 0;
        $bool = false;
        
        if( isset( $this->criterias[ 0 ][ "critere" ] ) ){

            foreach( $this->criterias as $criteria ){
                if( array_key_exists( $criteria[ "critere" ], $criteriasArray ) )
                    $criteriasArray[ $criteria[ "critere" ] ][] = $criteria[ "valueField" ];
                else{
                    $criteriasArray[ $criteria[ "critere" ] ] = array();
                    $criteriasArray[ $criteria[ "critere" ] ][] = $criteria[ "valueField" ];
                }
                $i++;
            }

            foreach( $criteriasArray as $typeCriteria => $values ){

                switch ( $typeCriteria ) {
                    case 1: // Clients(0) / Prospects (1)
                        $i = 0;
                        $nb = count( $values );
                        if ($nb == 1 && $values[0] != 2){
                        	$this->where .= " AND b.contact = ".$values[0];
                        }
                        else{
                        	if (!in_array(2, $values)){
                        		if (in_array()){
                        			
                        		}
                        	}
                        }
                        break;
                        
                    case 2: // Type d'organisation
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $nb == 1 )
                                $this->where .= " AND b.catalog_right = $value";
                            else{
                                if( $i == 0 )
                                    $this->where .= " AND (b.catalog_right = $value";
                                else if( $i == ( $nb - 1 ) )
                                    $this->where .= " OR b.catalog_right = $value)";
                                else
                                    $this->where .= " OR b.catalog_right = $value";
                            }
                            $i++;
                        }
                        break;
                    
                    case 3: // Zipcode
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $nb == 1 )
                                $this->where .= " AND b.zipcode LIKE '$value%'";
                            else{
                                if( $i == 0 )
                                    $this->where .= " AND (b.zipcode LIKE '$value%'";
                                else if( $i == ( $nb - 1 ) )
                                    $this->where .= " OR b.zipcode LIKE '$value%')";
                                else
                                    $this->where .= " OR b.zipcode LIKE '$value%'";
                            }
                            $i++;
                        }
                        break;
                    
                    case 4: // Pays
                        // Si la table n'est pas déja incluse dans le from, alors on l'inclut

                        //On fait la jointure
                        $i = 0;
                        $nb = count( $values );
                        // Pour chaque valeur correspondant au critère Pays
                        foreach( $values as $value ){
                            // S'il n'y a qu'une valeur
                            if( $nb == 1 )
                                $this->where .= " AND b.idstate = $value";
                            // S'il y en a plusieurs, selon si c'est la première la dernière ou une au milieu, on adapte la syntaxe
                            else{
                                if( $i == 0 )
                                    $this->where .= " AND (b.idstate = $value";
                                else if( $i == ( $nb - 1 ) )
                                    $this->where .= " OR b.idstate = $value)";
                                else
                                    $this->where .= " OR b.idstate = $value";
                            }
                            $i++;
                        }
                        break;
                    
                    case 5: // NAF / APE
                        if( !$this->isInFrom( "naf" ) ){ $this->from .= ", naf n"; }

                        $this->where .= " AND b.naf = n.idnaf";
                        $this->where .= " AND n.idnaf IN (";
                        $i = 1;
                        $nb = count( $values );
						
						$tmpTabNaf = array($values[0]);
						$i = 0;
						while ($i<count($tmpTabNaf)){
							
							$query = "select idnaf from naf where naf_parent = '$tmpTabNaf[$i]'";
							$rs = DBUtil::query($query);
							while(!$rs->EOF()){
							$tmpTabNaf[] = $rs->fields("idnaf");
							$rs->MoveNext();
							}
							$i++;
						}
						
						for ($j = 0; $j<count($tmpTabNaf); $j++){
							$this->where .= "'$tmpTabNaf[$j]'";
							if ($j < (count($tmpTabNaf)-1)){
								$this->where .= ", ";
							}
						}
						
						$this->where .= ")";   

                            break;

                    case 6: // Effectif
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $nb == 1 )
                                $this->where .= " AND b.idmanpower = $value";
                            else{
                                if( $i == 0 )
                                    $this->where .= " AND (b.idmanpower = $value";
                                else if( $i == ( $nb - 1 ) )
                                    $this->where .= " OR b.idmanpower = $value)";
                                else
                                    $this->where .= " OR b.idmanpower = $value";
                            }
                            $i++;
                        }
                        break;

                    case 7: // Secteur d'activité
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $value !=0  ){
                                if( $nb == 1 )
                                    $this->where .= " AND b.idactivity = $value";
                                else{
                                    if( $i == 0 )
                                        $this->where .= " AND (b.idactivity = $value";
                                    else if( $i == ( $nb - 1 ) )
                                        $this->where .= " OR b.idactivity = $value)";
                                    else
                                        $this->where .= " OR b.idactivity = $value";
                                }
                                $i++;
                            }
                        }
                        break;

                    case 8: // Provenance
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $nb == 1 )
                                $this->where .= " AND b.idsource = $value";
                            else{
                                if( $i == 0 )
                                    $this->where .= " AND (b.idsource = $value";
                                else if( $i == ( $nb - 1 ) )
                                    $this->where .= " OR b.idsource = $value)";
                                else
                                    $this->where .= " OR b.idsource = $value";
                            }
                            $i++;
                        }
                        break;

                    case 9: // Commercial
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $nb == 1 )
                                $this->where .= " AND b.iduser = $value";
                            else{
                                if( $i == 0 )
                                    $this->where .= " AND (b.iduser = $value";
                                else if( $i == ( $nb - 1 ) )
                                    $this->where .= " OR b.iduser = $value)";
                                else
                                    $this->where .= " OR b.iduser = $value";
                            }
                            $i++;
                        }
                        break;

                    case 10: // Catégorie - Gamme / Produit
                        // Pour chaque critère de type produit
                        
                        foreach( $values as $value ){
                        	$type = substr( $value, 0, 1 );
	                        $ref = substr( $value, 2 );
							
                        	if( $type == "C" ){ //C = Categorie
		                        
								$tmpTabCat = array($ref);
								$i = 0;
								while ($i<count($tmpTabCat)){
									$query = "select idcategorychild from category_link where idcategoryparent = '$tmpTabCat[$i]'";
									$rs = DBUtil::query($query);
									while(!$rs->EOF()){
										$tmpTabCat[] = $rs->fields("idcategorychild");
										$rs->MoveNext();
									}
									$i++;
								}
	                        	$this->tabCat = array_merge($this->tabCat, $tmpTabCat);
                        	}
                        	
                        	if ($type == "P"){
                        		$this->tabProd[] = $ref;
                        	}
                        }
						
						if (count($this->tabCat)>0){
	                        if( !$this->isInFrom( "estimate" ) ){ $this->from .= ", estimate e"; }
	                        $this->from .= ", estimate_row erow";
	                        $this->select .= ", erow.idcategory";
	                        $this->where .= " AND e.idbuyer = b.idbuyer AND erow.idestimate = e.idestimate";
						}
						
						if (count($this->tabProd)>0){
							if( !$this->isInFrom( "estimate" ) ){ $this->from .= ", estimate e"; }
							if( !$this->isInFrom( "estimate_row" ) ){ $this->from .= ", estimate_row erow"; }
							$this->select .= ", p.idproduct";
	                        $this->from .= ", product p, detail d";
                        	$this->where .= " AND e.idbuyer = b.idbuyer AND e.idestimate = erow.idestimate AND erow.idarticle = d.idarticle AND d.idproduct = p.idproduct";
						}		                
                        break;
                    
                    case 11: // Fax / Email
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $nb == 0 ){
                                if ( $value == 0 )
                                    $this->where .= " AND b.fax_standard = '$value'";
                                else
                                    $this->where .= " AND b.fax_standard = '$value'";
                            }
                            else{
                                if( $value == 0 )
                                    $this->where .= " AND b.fax_standard != ''";
                                else if( $value == 1)
                                    $this->where .= " AND b.company_mail != ''";
                                else
                                    $this->where .= " AND b.fax_standard != '' AND b.company_mail != ''";
                            }
                        }
                        break;
                    
                    case 12: // Statut des devis
                        if( !$this->isInFrom( "estimate" ) ){ $this->from .= ", estimate e"; }
						if (!strpos("AND e.idbuyer = b.idbuyer", $this->where)){
                        	$this->where .= " AND e.idbuyer = b.idbuyer";
                        }
                        $i = 0;
                        $nb = count( $values );
                        foreach( $values as $value ){
                            if( $value != 2 ){
                                if( $nb == 1 )
                                    $this->where .= " AND e.status = '$value'";
                                else{
                                    if( $i == 0 )
                                        $this->where .= " AND (e.status = '$value'";
                                    else if( $i == ( $nb - 1 ) )
                                        $this->where .= " OR e.status = '$value')";
                                    else
                                        $this->where .= " OR e.status = '$value'";
                                }
                            }
                            $i++;
                        }
                        break;
                    
                    case 13: // Volume d'achat minimum
                        if( $bool == false ){
                            foreach( $values as $value ){
                                $this->select .= ", SUM( o.total_amount_ht ) AS volume_achat";
                                $this->from .= ", `order` o";
                                $this->where .= " AND b.idbuyer = o.idbuyer";
                                $this->group_by .= "o.idbuyer";
                                $this->having .= " volume_achat >= $value";
                            }
                        }
                        else
                            foreach( $values as $value ){
                                $this->having .= " AND volume_achat >= $value";
                            }

                        $bool = true;
                        break;


                    case 14: // Volume d'achat maximum
                        if( $bool == false ){
                            foreach( $values as $value ){
                                $this->select .= ", SUM( o.total_amount_ht ) AS volume_achat";
                                $this->from .= ", `order` o";
                                $this->where .= " AND b.idbuyer = o.idbuyer";
                                $this->group_by .= "o.idbuyer";
                                $this->having .= " volume_achat <= $value";
                            }
                        }
                        else
                            foreach( $values as $value ){
                                $this->having .= " AND volume_achat <= $value";
                            }

                        $bool = true;
                        break;
                    case 15: // devis antérieur à dateMaxi
                    	if( !$this->isInFrom( "estimate" ) ){ $this->from .= ", estimate e"; }
                    	if (!strpos("AND e.idbuyer = b.idbuyer", $this->where)){
                        	$this->where .= " AND e.idbuyer = b.idbuyer";
                        }
                        
                    	$this->select .= ", e.DateHeure, e.idestimate";
                    	$date = euDate2us( $values[0] );
                    	$valuableDate = $date;
                    	foreach ($values as $value){
							$date = euDate2us( $value );
							if (strcmp($date, $valuableDate) > 0){
								$valuableDate = $date;
							}
						}
						$this->dateMaxi = $valuableDate;
						
						break;
						
					case 16: // Devis postérieur à dateMini
						if( !$this->isInFrom( "estimate" ) ){ $this->from .= ", estimate e"; }
                    	if (!strpos("AND e.idbuyer = b.idbuyer", $this->where)){
                        	$this->where .= " AND e.idbuyer = b.idbuyer";
                        }
                        
						$this->select .= ", e.DateHeure, e.idestimate";
                    	$date = euDate2us( $values[0] );
                    	$valuableDate = $date;
                    	foreach ($values as $value){
							$date = euDate2us( $value );
							if (strcmp($date, $valuableDate) < 0){
								$valuableDate = $date;
							}
						}
						$this->dateMini = $valuableDate;
						
						break;
						
                    default:
                        break;
                }            
            }
        }
    }


/*
 * Retourne si la table a déja été incluse dans le FROM de la requête
 */
    private function isInFrom( $table ){
        $return = strpos($this->from, $table);
        return( $return );
    }

/*
 *  Lance les requêtes nécessaires à la création de menus déroulants
 */
    private function getDropDownLists(){

        // Liste des secteurs d'activités (banque, hopital etc.)
        $req_act = "SELECT idactivity, activity_1 FROM activity";
        $this->rs_act = DBUtil::query( $req_act );
        // Liste des types d'organisation (administration, entreprise etc.)
        $req_cat = "SELECT idcatalog_right, catalog_right_1 FROM catalog_right";
        $this->rs_cat = DBUtil::query( $req_cat );
        // Liste des tranches d'effectif
        $req_eff = "SELECT idmanpower, qty FROM manpower";
        $this->rs_eff = DBUtil::query( $req_eff );
        // Liste des pays
        $req_state = "SELECT idstate, name_1 FROM state";
        $this->rs_state = DBUtil::query( $req_state );
        // Liste des commerciaux
        $req_user = "SELECT iduser, firstname, lastname FROM user";
        $this->rs_user = DBUtil::query( $req_user );
    }

/*
 * Retourne le recordset de la requete principale
 */
    public function &getResult(){
    
    $query = "SELECT $this->select FROM $this->from WHERE $this->where";
    $rs =& DBUtil::query( $query );
    $results = array();
    $i = 0;
    if (count( $this->tabCat )){
		while( !$rs->EOF() ){
			if( in_array( $rs->fields( "idcategory" ), $this->tabCat ) ){
				$results[] = $rs->fields;
			}
			$rs->MoveNext();
		}
    }
    if (count( $this->tabProd )){
    	while( !$rs->EOF() ){
			if( in_array( $rs->fields( "idproduct" ), $this->tabProd ) ){
				$results[] = $rs->fields;
			}
			$rs->MoveNext();
		}
    }
    if (!count( $this->tabCat ) && !count( $this->tabProd )){
		while( !$rs->EOF() ){
			$results[] = $rs->fields;
			$rs->MoveNext();
		}
	}
	$finalResult = array();
	foreach ($results as $result){
		if ($this->dateMini != null && $this->dateMaxi != null){
			if( strcmp($result["DateHeure"], $this->dateMini) > 0 && strcmp($result["DateHeure"], $this->dateMaxi) < 0){
				$finalResult[] = $result;
			}
		}
		else if ($this->dateMaxi != null){
			if(strcmp($result["DateHeure"], $this->dateMaxi) < 0){
				$finalResult[] = $result;
			}
		}
		else if ($this->dateMini != null){
			if(strcmp($result["DateHeure"], $this->dateMini) > 0){
				$finalResult[] = $result;
			}
		}
		else{
			$finalResult[] = $result;
		}
	}
		
    return $finalResult;

    }

/*
 * Accesseur des recordset par référence
 */
    public function &getRecordset( $selectedRecordset ){

        switch ( $selectedRecordset ){
            case "rs_act":
                return $this->rs_act;
                break;
            case "rs_cat":
                return $this->rs_cat;
                break;
            case "rs_eff":
                return $this->rs_eff;
                break;
            case "rs_state":
                return $this->rs_state;
                break;
            case "rs_user":
                return $this->rs_user;
                break;
            default:
                break;
        }
    }
}
?>