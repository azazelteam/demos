<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des catégories
 */
 
include_once('adm_table.php');

class ADMCATEGORY extends ADMTABLE
{
	
	/**
	 * Constructeur
	 * @param string $tblname nom de la table
	 */
	function __construct( $tblname )
	{
		//ADMTABLE::ADMTABLE($tblname);
        $this->tablename = $tblname;
		$this->load_design();
		$this->MakeEmptyRecord();
		//$this->productobj = new ADMPRODUCT('product');
	}
	
	/**
	 * ADMCATEGORY::FindCatQuery()
	 * Retourne la condition SQL pour la recherche à partir de la catégorie parente
	 * @param int $IdCategoryParent	Id de la catégorie parente
	 * @return string
	 */
	public function FindCatQuery($IdCategoryParent)
	{
		$Result='WHERE 0';
		//'Error ADMCATEGORY::FindCatQuery La catégorie '.$IdCategoryParent . ' est sans child';
		$sep='';
		$WhereCond ="WHERE idcategory IN(";
		$res= DBUtil::getConnection()->Execute("SELECT `idcategorychild` FROM `category_link` WHERE `idcategoryparent`='$IdCategoryParent'");
		$n = $res->RecordCount();
		if($n)
		{
			for($i=0 ; $i < $n ; $i++)
			{
				$WhereCond.= $sep.$res->fields["idcategorychild"];
				$sep=',';
				$res->MoveNext();
			}
			$Result = $WhereCond.')';
			$Str_ActionName = basename($_SERVER['PHP_SELF']);
			$_SESSION[$Str_ActionName]['search_select'] = $Result;
		}
		return $Result;
	}
	
	/**
	 * ADMCATEGORY::InsertCL()
	 * Insertion ou mise à jour de la relation et de l'ordre d'affichage dans category_link
	 * @param int $IdCategoryParent Catégorie parente
	 * @param int $idcategory Catégorie concernée
	 * @param int $displayorder Ordre d'affichage'
	 * @return void
	 */
	public function InsertCL( $IdCategoryParent, $idcategory, $displayorder)
	{
			
		//showDebug( $IdCategoryParent, "InsertCL IdCategoryParent", "code" );
		//showDebug( $idcategory, "InsertCL idcategory", "code" );
		
		$res= DBUtil::getConnection()->Execute("SELECT * FROM category_link where idcategorychild='$idcategory' LIMIT 1");
		$n = $res->RecordCount();
		$date = date('Y-m-d H:i:s');
		$username = addslashes(stripslashes(User::getInstance()->get("login")));
		if($n){
			
			$query = "UPDATE `category_link` SET `displayorder`='$displayorder', `idcategoryparent`='$IdCategoryParent', `lastupdate`='$date', `username`='$username' WHERE `idcategorychild`='$idcategory' LIMIT 1";
			//showDebug( $query, "InsertCL query", "code" );
			
			$rs = DBUtil::getConnection()->Execute( $query );
			if( $rs === false )
				die( "Impossible de déplacer la catégorie" );
		
		}
		else{
			
			$query = "INSERT INTO category_link (idcategoryparent,idcategorychild,displayorder,lastupdate,username) VALUES('$IdCategoryParent','$idcategory','$displayorder','$date','$username')";
			$rs = DBUtil::getConnection()->Execute( $query );
			//showDebug( $query, "InsertCL query", "code" );
			
			if( $rs === false )
				die( "Impossible lier la catégorie à la catégorie parente" );
				
		}
		
	}
	
	/**
	 * ADMCATEGORY::FindDisplayOrder()
	 * Recherche l'ordre d'affichage dans la table category_link
	 * @param int $IdCategoryChild
	 * @param int $IdCategoryParent
	 * @return int l'ordre d'affichage
	 */
	public function FindDisplayOrder( $IdCategoryChild,$IdCategoryParent)
	{
		$Result=0;
		$res= DBUtil::getConnection()->Execute("select displayorder from category_link where idcategoryparent='$IdCategoryParent' and idcategorychild='$IdCategoryChild'");
		if($res->RecordCount())
		{
			$Result= $res->fields["displayorder"];
		}
		$this->current_record['displayorder']=$Result; $this->add('displayorder',$Result);
		return $Result;
	}
	
	/**
	 * Retourne le première catégorie parente d'une sous-catégorie
	 * @param int $IdCategoryChild Catégorie fille
	 * @todo Faut voir ce qu'il faut faire si plusieurs parents existent.
	 * @return int
	 */
	public function FindParentCategory($IdCategoryChild)
	{
		$Result= 0;
		$res= DBUtil::getConnection()->Execute("select idcategoryparent from category_link where idcategorychild='$IdCategoryChild'");
		$n = $res->RecordCount();
		if($n) $Result=$res->fields["idcategoryparent"];
		return $Result;
	}
	
	/**
	 * Affiche un menu déroulant pour la sélection du masque de catégorie
	 * @param string $Select Optionnel - la valeur actuelle
	 * @return void
	 */
	public function SelectMaskCat($Select="")
	{
		$FileArray=file("../../catalog/category_display.php");
		echo "<select name=idmask>\n";
		echo "<option value=0>-</option>\n";
		for($i=0 ; $i < count($FileArray) ; $i++)
		{
			if(strstr($FileArray[$i],"function DisplayCategory"))
			{	
				$ExpF=explode("_",$FileArray[$i]);
				$ExpMask=explode("(",$ExpF[1]);
				if($ExpMask[0]!="")
				{
					if($Select==$ExpMask[0])
						$Selected="selected";
					else
						$Selected="";
					echo "<option $Selected value=\"$ExpMask[0]\">$ExpMask[0]</option>\n";
				}
			}
		}
		echo "</select>";				
	}
	
	/**
	 * Affiche un menu déroulant pour la sélection du masque de produit
	 * @param $Select Optionnel - la valeur actuelle
	 * @return void
	 */
	function SelectMaskProduct($Select="")
	{
		$FileArray=file("../../catalog/product_display_product.php");
		echo "<select name=idmaskproduct>\n";
		echo "<option value=0>-</option>\n";
		for($i=0 ; $i < count($FileArray) ; $i++)
		{
			if(strstr($FileArray[$i],"function DisplayProduct"))
			{	
				$ExpF=explode("_",$FileArray[$i]);
				$ExpMask=explode("(",$ExpF[1]);
				if($ExpMask[0]!="")
				{
					if($Select==$ExpMask[0])
						$Selected="selected";
					else
						$Selected="";
					echo "<option $Selected value=\"$ExpMask[0]\">$ExpMask[0]</option>\n";
				}
			}
		}
		echo "</select>";				
	}
	
	/**
	 * Affiche un menu déroulant pour la sélection du masque des références
	 * @param $Select Optionnel - la valeur actuelle
	 * @return void
	 */
	function SelectMaskDetail($Select="")
	{
		$FileArray=file("../../catalog/product_display_detail.php");
		echo "<select name=productmask>\n";
		if($Select==-1)
			$Selected="selected"; 
		else
			$Selected="";
		echo "<option $Selected value=-1>Aucun</option>\n";
		if($Select=="")
			$Selected="selected"; 
		else
			$Selected="";
		echo "<option $Selected value=\"\">0</option>\n";
		for($i=0 ; $i < count($FileArray) ; $i++)
		{
			if(strstr($FileArray[$i],"function DisplayDetail"))
			{	
				$ExpF=explode("_",$FileArray[$i]);
				$ExpMask=explode("(",$ExpF[1]);
				if($ExpMask[0]!="")
				{
					if($Select==$ExpMask[0])
						$Selected="selected";
					else
						$Selected="";
					echo "<option $Selected value=\"$ExpMask[0]\">$ExpMask[0]</option>\n";
				}
			}
		}
		echo "</select>";				
	}
	
	
	/**
	 * Affiche le chemin vers le catégorie
	 * @todo en bouble dans adm_prod
	 * @param int $IntCategory le numéro de la catégorie
	 * @param string $Path le préfixe à utiliser devant le chemin ( optionnel )
	 * @param string $lang le numéro de la langue à utiliser (1-5), préfixé d'un underscore
	 * @return void
	 */
	public function AffPathCat( $IdCategory, &$Path, $lang)
	{
		if($Path=="") $Path=$IdCategory;
	
		if($IdCategory!=0)
		{
			$rs = DBUtil::getConnection()->Execute("select idcategoryparent from category_link where idcategorychild='$IdCategory'");
			if($rs->RecordCount())
			{
				$IdCategory=$rs->fields["idcategoryparent"];
				$Path.= "," . $IdCategory;
				$this->AffPathCat($IdCategory,$Path,$lang);
				return $Path;
			}
		}
		else
		echo '<div id="navigation">';
	
		{
			$ExpPath=explode(",",$Path);
			$Link='';
			for($i=count($ExpPath)-1 ; $i>=0 ; $i--)
			{
				$rs= DBUtil::getConnection()->Execute("select name$lang from category where idcategory='$ExpPath[$i]'");
				if($rs->RecordCount())
				{
					$CatName=$rs->fields["name$lang"];
					$Link.="$CatName > ";
				}
			}
			$Link=substr($Link,0,-1);
		}
		echo $Link;
		echo '</div>'."\n";
	}
	
	/**
	 * Déplace une catégorie dans une autre
	 * @param int $IdCategory la catégorie à déplacer
	 * @param int $IdCategoryParent la catégorie de départ
	 * @param int $Move2Cat la catégorie de destination
	 */
	public function Move2Cat($IdCategory,$IdCategoryParent,$Move2Cat)
	{
		$res= DBUtil::getConnection()->Execute("select * from category_link where idcategoryparent='$IdCategoryParent' and idcategorychild='$IdCategory'");
		if($res->RecordCount())
		{
			$res= DBUtil::getConnection()->Execute("update category_link set idcategorychild='$IdCategory', idcategoryparent='$Move2Cat' where idcategoryparent='$IdCategoryParent' and idcategorychild='$IdCategory'");
		}
	}

}
?>
