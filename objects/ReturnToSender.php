<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object retour de marchandise à l'expéditeur
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );
 
class ReturnToSender extends DBObject{
	
	//----------------------------------------------------------------------------
	
	public static $GOODS_DELIVERY_LOCATION_LOCAL 	= "local";
	public static $GOODS_DELIVERY_LOCATION_SUPPLIER = "supplier";
	public static $GOODS_DELIVERY_LOCATION_BOTH 	= "both";
	
	//----------------------------------------------------------------------------

	public function __construct( $idorder_supplier, $idrow ){
		
		parent::__construct( "return_to_sender", false, "idorder_supplier",  $idorder_supplier , "idrow", intval( $idrow ) );
		
	}
	
	//----------------------------------------------------------------------------
	
	//----------------------------------------------------------------------------
	
}

?>