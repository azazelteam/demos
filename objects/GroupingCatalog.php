<?php

/**
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion du catalogue personnel pour les groupes d'acheteurs
 */

include_once( dirname( __FILE__ ) . "/Session.php" );
//include_once( "../script/global.fct.php");

class GroupingCatalog{
	
	
	/**
	 * Constructeur
	 */
	 
	function __construct(){

		$this->idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		
		$this->lang = "_1";
		$this->idgrouping = $this->getBuyerGrouping($this->idbuyer);
		
		$this->grouping = array();
		$this->references = array();
		
		if($this->idgrouping>0){
			
			$this->getGroupingInfos( $this->idgrouping );

		}
		
	}
	
	
	/**
	 * Retourne le numéro du groupement de l'acheteur
	 * @return int le numéro du groupement
	 */
	 
	public static function getBuyerGrouping ($idbuyer){
			// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "SELECT idgrouping FROM buyer WHERE idbuyer = '".$idbuyer."'";
		//---
	   
		$rs = DBUtil::query( $query );
		
		if( $rs === false ) 
			die( "Impossible de récupérer le groupe du client" );
			
		$idgrouping = $rs->fields("idgrouping");
		
		
		return $idgrouping ;
		
	} 
	
	
	/**
	 * Charge les infos du groupement depuis la base de données
	 * @param int le numéro du groupement
	 * @return les infos du groupement
	 */
	 
	function getGroupingInfos ( $idgrouping ){
		
		$query = "SELECT * FROM grouping WHERE idgrouping=$idgrouping";
		$rs = DBUtil::query( $query );
		
		if( $rs === false ) 
			die( "Impossible de récupérer les infos du groupement" );
			
		$this->grouping = $rs->fields;
		
		//Titre
		if( $this->grouping[ "title" ] )
				$this->grouping[ "title" ] = DBUtil::getDBValue( "title".$this->lang."", "title", "idtitle", $this->grouping[ "title" ] );
		else 	$this->grouping[ "title" ] = "";
		
		//Pays
		$this->grouping[ "state" ] = DBUtil::getDBValue( "name".$this->lang."", "state", "idstate", $this->grouping[ "idstate" ] );
		
	} 
	
	
	/**
	 * Retourne la valeur d'un attribut donné du groupement
	 * @param string $key le champ
	 * @param string $defaultValue la valeur retournée par défaut si l'attribut n'existe pas
	 * @return mixed la valeur de l'attribut $key
	 */
	 
	public final function getGrouping( $key, $defaultValue = "" ){ 
		
		return isset( $this->grouping[ $key ] ) ? $this->grouping[ $key ] : $defaultValue; 
		
	}
	
	
	/**
	 * Recupere les infos des references du groupement pour le catalogue perso
	 * @param int $idgrouping le numéro du groupement
	 * @return les références et leurs spécificités propres au groupement
	 */
	 
	function setGroupingReferencesDatas (){ 

		$infos = array();
		$date = date("Y-m-d");
		$query = "SELECT g.`reference`,g.`rate`, g.`price` FROM detail d, group_price g WHERE g.`idgrouping`=".$this->idgrouping." AND g.`start_date` < '".$date."' AND   '".$date."' < g.`end_date` AND g.`reference` != '' AND d.`available`=1  and d.`reference`=g.`reference`  ORDER BY g.`reference` ASC";

		$rs = DBUtil::query( $query );
		
		if( $rs === false ) 
			die( "Impossible de récupérer les références du catalogue du groupement" );
		
		for($i=0;$i<$rs->RecordCount();$i++){
			
			/**
			 * Autres infos
			 */
			
		//var_dump($rs);exit;

			$reference = $rs->fields("reference"); 
			
			//$summary = $rs->fields("summary");
			 
			$query = "SELECT d.idproduct, d.idarticle, p.name_1, d.sellingcost, d.designation".$this->lang." as designation
					FROM detail d, product p
					WHERE d.reference = '$reference'
					AND d.idproduct = p.idproduct
					
					"
					;
					
			$rsp = DBUtil::query( $query );
			
			if( $rsp === false ) 
				die( "Impossible de récupérer les informations du produit pour la référence $reference" );
				
			$idproduct = $rsp->fields("idproduct");
			$idarticle = $rsp->fields("idarticle");
			$designation = $rsp->fields("designation");
			$name_1 = $rsp->fields("name_1");
			/**
			 * Prix spécifique
			 */
			 
			/*$query = "SELECT rate, price
					FROM group_price
					WHERE 
					idarticle = '$idarticle'
					AND idgrouping = ".$this->idgrouping."
					AND quantity_min = 0";
			$rspx = DBUtil::query( $query );
			*/
		//	if( $rspx === false ) 
				//die( "Impossible de récupérer le prix pour la référence $reference" );
			//Calcul du prix

			if($rs->fields("rate")>0){
				
				$price = round($rsp->fields("sellingcost") * ( 1-$rs->fields("rate")/100 ),2);
				$rate = $rs->fields("rate");
				
			}
			else if($rs->fields("price")>0){
				
				//$rate = round(100.0 * $rs->fields("price") / $rsp->fields("sellingcost"),2);
				$rate = $rsp->fields("sellingcost") - $rs->fields("price");
				$price = $rs->fields("price");
			}
			
			//URL Produit
			$url = URLFactory::getProductURL($idproduct);
				
			//Et hop on remplit le tableau
			
			$infos[$i]["idproduct"] = $idproduct;
			$infos[$i]["idarticle"] = $idarticle;
			$infos[$i]["name_1"] = $name_1;
			$infos[$i]["image"] = URLFactory::getReferenceImageURI( $idarticle, URLFactory::$IMAGE_ICON_SIZE );
			$infos[$i]["reference"] = $reference;
			//$infos[$i]["summary"] = $summary;
			$infos[$i]["designation"] = $designation;
			$infos[$i]["basic_price"] = Util::priceFormat($rsp->fields("sellingcost"));
			if($rs->fields("rate")>0)
			$infos[$i]["discount"] = Util::rateFormat($rate);	
			else
			$infos[$i]["discount"] = Util::priceFormat($rate);	
			$infos[$i]["discount_price"] = Util::priceFormat($price);
			$infos[$i]["url"] = $url;	
		$infos[$i]["i"] = $i;
			$rs->MoveNext();
			
		}
		
		return $infos;
	}
	function setGroupingcategory (){ 

		$infos_cat = array();
		$date = date("Y-m-d");
		$query = "SELECT idcategory, rate, start_date, end_date FROM group_category WHERE idgrouping=".$this->idgrouping." AND start_date < '".$date."' AND   '".$date."' < end_date   ORDER BY idcategory ASC";
		$rs = DBUtil::query( $query );
		
		if( $rs === false ) 
			die( "Impossible de récupérer les catégories du groupement" );
		
		for($i=0;$i<$rs->RecordCount();$i++){
			
			/**
			 * Autres infos
			 */
			 
			$idcategory = $rs->fields("idcategory");
			$rate = $rs->fields("rate");
			 
			$query = "SELECT d.idcategory, d.name_1, d.imagename 
					FROM category d
					WHERE d.idcategory = '$idcategory'
					";
			$rsp = DBUtil::query( $query );
			
			if( $rsp === false ) 
				die( "Impossible de récupérer les informations du categorie" );
				
			$idcategory = $rsp->fields("idcategory");
			$name_1 = $rsp->fields("name_1");
			$imagename = $rsp->fields("imagename");
			
			/**
			 * Prix spécifique
			 */
			 
			/*$query = "SELECT rate, price
					FROM group_price
					WHERE idarticle = '$idarticle'
					AND idgrouping = ".$this->idgrouping."
					AND quantity_min = 0";
			$rspx = DBUtil::query( $query );
			
			if( $rspx === false ) 
				die( "Impossible de récupérer le prix pour la référence $reference" );
*/
			//Calcul du prix

			/*if($rspx->fields("rate")>0){
				
				$price = round($rsp->fields("sellingcost") * ( 1-$rspx->fields("rate")/100 ),2);
				$rate = $rspx->fields("rate");
				
			}
			else if($rspx->fields("price")>0){
				
				$rate = round(100.0 * $rspx->fields("price") / $rsp->fields("sellingcost"),2);
				$price = $rspx->fields("price");
			}
			*/
			//URL Produit
			$url = URLFactory::getCategoryURL($idcategory);
				
			//Et hop on remplit le tableau
			
			$infos_cat[$i]["idcategory"] = $idcategory;
			$infos_cat[$i]["name_1"] = $name_1;
			$infos_cat[$i]["image"] = URLFactory::getCategoryImageURI( $idcategory, URLFactory::$IMAGE_ICON_SIZE );
			$infos_cat[$i]["imagename"] = $imagename;
			$infos_cat[$i]["rate"] = $rate;
			$infos_cat[$i]["url"] = $url;	
		
			$rs->MoveNext();
			
		}
		
		return $infos_cat;
	}









}

?>
