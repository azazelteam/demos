<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object CSC outil ERP
*/


if( file_exists( "../../script/configdb.php" ) ){	
	include_once( "../../script/configdb.php" );
}else{
	include_once( "../script/configdb.php" );
}
//----------------------------------------------------------------------------------------------------

/**
 * Manipulation de fichiers CSV
 */
 
class CSVHandler{
	
	//----------------------------------------------------------------------------------------------------
	
	//data members
	
	private $erp_name;
	private $tables;
	private $file;
	private $action;
	private $queue;
	
	private $db;
	
	private $linebreak;
	private $csvHeaders;
	private $buffer;
	
	//----------------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param $erp_name string le nom de l'interface erp
	 * @param $action string le type d'action éxécutée ( import ou export )
	 */
	 
	function __construct( $erp_name, $action ){
		
		if( $action != "import" && $action != "export" )
			die();
			
		$this->erp_name = $erp_name;
		$this->tables = array();
		$this->action = $action;
		$this->queue = array();
		$this->file = "";
		$this->linebreak = "\r\n";
		$this->csvHeaders = array();
		$this->buffer = array();
		
		global 	$GLOBAL_DB_NAME,
				$GLOBAL_DB_USER,
				$GLOBAL_DB_PASS,
				$GLOBAL_DB_HOST;
	
		$this->db = &NewADOConnection( "mysql", "pear" );
		$this->db->Connect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS, $GLOBAL_DB_NAME );
	
		$this->initQueue();
		$this->loadFile();
		$this->setHeaderInfos();
		$this->handle();
		$this->saveFile();
		
	}
	
	//----------------------------------------------------------------------------------------------------
	
	/**
	 * Construit la liste des tâches à éxécuter
	 * @return void
	 */
	 
	private function initQueue(){
		
		//fonctions d'import/export
	
		$query = "
		SELECT et.tablename, etf.name, etf.type, etf.link_erp
		FROM erp_table et, erp_table_function etf
		WHERE et.erp_name = '{$this->erp_name}'
		AND et.used = 1
		AND et.function_{$this->action} <> 0
		AND et.function_{$this->action} = etf.iderp_table_function
		AND etf.type LIKE '{$this->action}'";
		
		$rs = $this->db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupèrer la liste des tables" );

		while( !$rs->EOF() ){
			 
			$this->queue[] = array(
			
				"table" 	=> $rs->fields( "tablename" ),
				"function" 	=> $rs->fields( "name" ),
				"filename" 	=> $rs->fields( "link_erp" )
				
			);
			
			if( !in_array( $rs->fields( "tablename" ), $this->tables ) )
				$this->tables[] = $rs->fields( "tablename" );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------------------------------
	
	/**
	 * Execute les fonctions d'import/export
	 * @return void
	 */
	 
	protected function handle(){

		global $GLOBAL_START_PATH;
		
		while( count( $this->queue ) ){
			
			$task = &array_pop( $this->queue );
			
			$table = $task[ "table" ];
			$function = $task[ "function" ];
			$filename = $task[ "filename" ];
			
			if( !file_exists( "$GLOBAL_START_PATH/erp/functions/{$this->action}/$filename" ) )
				die( "Le fichier de définition de la fonction n'existe pas : $GLOBAL_START_PATH/erp/functions/{$this->action}/$filename" );
				
			$functions = array();
			include_once( "$GLOBAL_START_PATH/erp/functions/{$this->action}/$filename" );
			
			if( !isset( $functions[ $function ] ) )
				die( "Aucune définition trouvée pour la fonction '$function' dans $GLOBAL_START_PATH/erp/functions/{$this->action}/$filename" );
				
			$db = &$this->db;
			$functions[ $function ]( $this->csvHeaders, $this->buffer );
			
		}
		
	}
	
	//----------------------------------------------------------------------------------------------------
	/**
	 * Charge le fichier
	 * @return void
	 */
	protected function loadFile(){
		
		$this->buffer = array();
		
		$query = "
		SELECT {$this->action}_directory AS directory, {$this->action}_name AS filename
		FROM erp_config
		WHERE erp_name LIKE '{$this->erp_name}'
		LIMIT 1";
		
		$rs = $this->db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le fichier à traiter" );
			
		$directory = $rs->fields( "directory" );
		$filename = $rs->fields( "filename" ) . ".csv";
		$this->file = "$directory$filename";
		
		if( empty( $directory ) || empty( $filename ) )
			die( "Aucun fichier n'a été défini pour cette interface" );
			
		if( !file_exists( $this->file ) )
			die( "Le fichier spécifié n'existe pas : {$this->file}" );
			
		$fp = fopen( $this->file, "r" );
		
		if( !is_resource( $fp ) )
			die( "Impossible d'ouvrir le fichier en lecture : $this->file" );
		
		while( ( $data = fgetcsv ( $fp, 8100, ";"  , "\""  ) ) !== false )
			$this->buffer[] = $data;
		
		fclose( $fp );
		
	}
	
	//----------------------------------------------------------------------------------------------------
	/**
	 * Enregistre le fichier
	 * @return void
	 */
	protected function saveFile(){
		
		/*if( empty( $this->file ) || !file_exists( $this->file ) )
			return;*/

		$maxX = $this->maxX();
		$maxY = $this->maxY();
		
		$fp = fopen( $this->file, "w+" );

		if( !is_resource( $fp ) )
			die( "Impossible d'ouvrir le fichier en écriture : {$this->file}" );
		
		for( $x = 0; $x < $maxX; $x++ ){
			
			for( $y = 0; $y < $maxY; $y++ ){
				
				if( strlen( $this->buffer[ $x ][ $y ] ) )
					fwrite( $fp, "\"" . $this->buffer[ $x ][ $y ] . "\"" );
				
				if( $y < $maxY - 1 )
					fwrite( $fp, ";", 1 );
				
			}
		
			fwrite( $fp, $this->linebreak, 2 );
		
		}

		fclose( $fp );

	}
	
	//----------------------------------------------------------------------------------------------------
	/**
	 * Retourne le nombre de lignes
	 * @return int le nombre de lignes
	 */
	protected function maxX(){
	
		return count( $this->buffer );

	}
	
	//----------------------------------------------------------------------------------------------------
	/**
	 * Retourne le nombre de lignes
	 * @return int le nombre de lignes
	 */
	protected function maxY(){
		
		 return count( $this->csvHeaders );
		
	} 

	//----------------------------------------------------------------------------------------------------
	/**
	 * Initialise le caractère de saut de ligne
	 * @param string $breakline le caractère de saut de ligne
	 * @return void
	 */
	private function setLineBreak( $linebreak ){
	
		$this->linebreak = $linebreak;
		
	}
	
	//----------------------------------------------------------------------------------------------------
	/**
	 * Génère les entetes du fichiers
	 * @return void
	 */
	private function setHeaderInfos(){
		
		if( !count( $this->buffer ) )
			return;
			
		$headers = &$this->buffer[ 0 ];
		
		$i = 0;
		while( $i < count( $headers ) ){
			
			$translation = $headers[ $i ];
			$fieldname = $this->getFieldName( $translation );
			$table = $this->getFieldTable( $fieldname );

			$this->csvHeaders[] = array(
			
				"fieldname" => $fieldname,
				"table" 	=> $table,
				"translation" => $translation
				
			);
			
			$i++;
			
		}
		
	}
	
	//----------------------------------------------------------------------------------------------------

	private function getFieldTable( $fieldname ){
		
		//TODO : pas sécurisé car plusieurs tables possibles
		
		if( !count( $this->tables ) )
			return "";
			
		return $this->tables[ 0 ];
		
	}
	
	//----------------------------------------------------------------------------------------------------
	/**
	 * Retourne le champ correspondant à une traduction
	 * @param string $translation la traduction
	 * @return string $fieldname le champ
	 */
	private function getFieldName( $translation ){
		
		if( !count( $this->tables ) )
			return $translation;
			
		if( !strlen( $translation ) )
			return "";
			
		$query = "
		SELECT fieldname 
		FROM desc_field
		WHERE export_name LIKE '$translation'
		AND tablename IN ( '" . implode( "','", $this->tables ) . "' )";
		
		$rs = $this->db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le nom du champ pour la chaîne de caractères '$translation'" );
			
		if( !$rs->RecordCount() )
			return $translation;
		
		//TODO : plusieurs tables autorisées pour une interface => on peut trouver des export_name diffèrent pour le même champ :o(
		
		$fieldname = $rs->fields( "fieldname" );
		
		while( !$rs->EOF() ){
			
			if( $rs->fields( "fieldname" ) != $fieldname )
				die( "Le champ $fieldname est ambigü" );
			
			$rs->MoveNext();
			
		}
		
		return $fieldname;
		
	}
	
	//----------------------------------------------------------------------------------------------------
	
	function debug(){

		echo "<p>erp_name : {$this->erp_name}</p>";
		echo "<p>tables : " . implode( ",", $this->tables ) . "</p>";
		echo "<p>file : {$this->file}</p>";
		echo "<p>action : {$this->action}</p>";
		echo "<p>queue : </p>"; print_r( $this->queue );
		echo "<p>linebreak : {$this->linebreak}</p>";
		echo "<p>buffer : </p>"; print_r( $this->buffer );
		echo "<p>csvHeaders : </p>"; print_r( $this->csvHeaders );
		
	}
	
	//----------------------------------------------------------------------------------------------------
	
}

?>
