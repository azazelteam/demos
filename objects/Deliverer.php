<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object interface transporteur de marchandise
 */
 

include_once( dirname( __FILE__ ) . "/Address.php" );


interface Deliverer{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Identifiant unique
	 * @access public
	 * @return mixed
	 */
	public function getId();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Nom ou raison sociale
	 * @access public
	 * @return string
	 */
	public function getName();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * TVA appliquée pour le transport de $source vers $destination
	 * @param Address $source
	 * @param Address $destination
	 * @return float
	 */
	public function getCarriageVATRate( Address $source, Address $destination );
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Liste des contacts
	 * @access public
	 * @return ArrayList<Contact>
	 */
	public function &getContacts();
	 
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Contact principal
	 * @access public
	 * @return Contact
	 */
	public function &getContact();
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Adresse principale
	 * @access public
	 * @return Address
	 */
	public function &getAddress();

	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>