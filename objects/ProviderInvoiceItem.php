<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object lignes factures fournisseurs services
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );

class ProviderInvoiceItem extends DBObject{

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $iditem
	 */
	private $iditem;
	
	/* ----------------------------------------------------------------------------------------------------- */
	
	public function __construct( $idtem ){
		
		parent::__construct( "provider_invoice_item", false, "iditem", $idtem );
		
		$this->iditem = intval( $idtem );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return float
	 */
	public function getTotalET(){
		
		return $this->get( "price" ) * $this->get( "quantity" );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	
}

?>