<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object création ligne + duplication devis
 */

include_once( dirname( __FILE__ ) . "/DBSaleItem.php" );

	
class EstimateItem extends DBSaleItem{

	//----------------------------------------------------------------------------
	/**
	 * Crée une nouvelle ligne article pour le devis
	 * @param Estimate $estimate le devis
	 * @param int $idrow le numéro de la ligne article ( doit commencer à 1 pour les ERP )
	 */
	public function __construct( Estimate $estimate, $idrow ){

		DBObject::__construct( "estimate_row", false, "idestimate", $estimate->getId(), "idrow", intval( $idrow ) );

		$this->sale = $estimate;

	}
	
	//----------------------------------------------------------------------------
	
	public function duplicate( Estimate $estimate, $idrow ){
		
		$clone = clone $this;
		
		$clone->recordSet[ "idrow" ] = $idrow;
		
		$clone->sale = $estimate;
		$clone->recordSet[ "idestimate" ] = $estimate->getId();
		
		return $clone;
		
	}

	//----------------------------------------------------------------------------
	
}

?>