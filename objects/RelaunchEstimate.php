<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object relance devis
 */

include_once( dirname( __FILE__ ) . "/Estimate.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

		
class RelaunchEstimate extends Estimate{
	
	public static $relaunchModes = array( 'tel' => 'Téléphone' , 'fax' => 'Fax' , 'mail' => 'Mail' , 'courrier' => 'Courrier' );	
	
	/**
	 * Constructeur
	 * @param int $idestimate le numéro du devis
	 */
	public function __construct( $idestimate ){
		$this->idestimate 	= $idestimate;
		$this->idrelaunch 	= 'SELECT ifnull( count( idrelaunch ) , 0 ) + 1 as relaunchNum FROM estimate_relaunch WHERE idestimate = ' . $idestimate . ' GROUP BY idestimate LIMIT 1' )->fields( 'relaunchNum' );
		$this->dateRelaunch = date('Y-m-d H:i:s');
		$this->iduser		= User::getInstance()->getId();
	}

	public function setDatas( $comment , $relaunchMode ){
		$this->comment		= $comment;
		$this->relaunchMode = $relaunchMode;
	}

	public function save(){
		self::insertRelaunch( );
	}

	/**
	 * Retourne les relances faites pour un devis
	 * @static
	 * @param int $idestimate le numéro du devis
	 * @return array() // les relances 
	 */
	public static function getRelaunches( $idestimate ){
		
		$relaunchesQuery = DBUtil::query( 'SELECT * FROM estimate_relaunch WHERE idestimate = ' . $idestimate . ' ORDER BY idelaunch ASC' );
		$relaunches = array();
		
		while( !$relaunchesQuery->EOF() ){
			$idrelaunch = $relaunchesQuery->fields('idrelaunch');
			
			$relaunches[ $idrelaunch ][ 'DateHeure' ] = $relaunchesQuery->fields( 'DateHeure' );
			$relaunches[ $idrelaunch ][ 'comment' ] = $relaunchesQuery->fields( 'comment' );
			$relaunches[ $idrelaunch ][ 'relaunch_mode' ] = $relaunchesQuery->fields( 'relaunch_mode' );
			$relaunches[ $idrelaunch ][ 'iduser' ] = $relaunchesQuery->fields( 'iduser' );
			
			$relaunchesQuery->MoveNext();
		}
		
		return $relaunches;
	}
	
	private function insertRelaunch( ){
	
		DBUtil::query( "INSERT INTO estimate_relaunch VALUES( $this->idestimate , $this->idrelaunch , '$this->dateRelaunch' , '$this->comment' , '$this->relaunchMode' , $this->iduser )" );
		
	}
	
}