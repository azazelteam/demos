<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ligne article facture
 */

include_once( dirname( __FILE__ ) . "/DBSaleItem.php" );

	
class InvoiceItem extends DBSaleItem{

	//----------------------------------------------------------------------------
	/**
	 * Crée une nouvelle ligne article pour la commande client
	 * @param Order $order la commande client
	 * @param int $idrow le numéro de la ligne article ( doit commencer à 1 pour les ERP )
	 */
	public function __construct( Invoice $invoice, $idrow ){

		DBObject::__construct( "billing_buyer_row", false, "idbilling_buyer", $invoice->getId(), "idrow", intval( $idrow ) );

		$this->sale =& $invoice;

	}
	
	//----------------------------------------------------------------------------
	
	public function duplicate( Invoice &$invoice, $idrow ){
		
		$clone = clone $this;
		
		$clone->recordSet[ "idrow" ] = $idrow;
		
		$clone->sale =& $invoice;
		$clone->recordSet[ "idbilling_buyer" ] = $invoice->getId();
		
		return $clone;
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>