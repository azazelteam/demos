<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object catalogue des produits par ordre alphabétique
 */

include_once( dirname( __FILE__ ) . "/catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CATALALPHA extends CATALBASE{
	
	/**
	 * Constructeur
	 * @param char	$letter	Lettre
	 * @return void
	 */
	function __construct( $letter ){
		
		$reload = 1;
		$this->id = $letter ;
		$this->buyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$this->lang = "_1";
		
		if( $reload ){
			
			$this->SetAlphaProducts();
			
		}else{ 
			
			$this->products = &$_SESSION[ "alphaproducts" ] ;
			$this->NBProduct = 0 +  count( $this->products );
			
		}
		
	}
	
	
	/**
	 * Charge les produits depuis la BDD
	 * @return int $n le nombre de produits
	 */
	private function SetAlphaProducts(){
		
		$Langue = $this->lang;
		$chars = preg_split( "//", $this->id, -1, PREG_SPLIT_NO_EMPTY );
		$inrequest = implode( "','", $chars );
		$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		$catright = "`catalog_right`<= '$catalog_right' AND";
		
		$query = "	SELECT idproduct,
						name$Langue as name
					FROM product
					WHERE $catright
						SUBSTRING( name$Langue, 1, 1 ) IN ( '$inrequest' )
					ORDER BY name$Langue,
						display_product_order";
		
		$n = $this->SetProducts( $query );
		$_SESSION[ "alphaproducts" ] = &$this->products ;
		
		return $n;
		
	}
	
	/**
	 * Recherhce les produits
	 * @param string $Query Requête sql
	 * @return int Le nombre de produits
	 */
	public function SetProducts( $Query ){
		
		$this->NBProduct = 0;
		$this->products = array();
		$Base = &DBUtil::getConnection();
		$rs =& DBUtil::query( $Query );
		
		if( empty( $rs ) )
			 trigger_error( showDebug( $Query, "CATEGORY::SetCategory() => $SQL_select", "log"), E_USER_ERROR );
		
		if( !$rs->EOF ){
			
			while( !$rs->EOF ){
				
				if( PRODUCT::isAvailable( $rs->fields[ "idproduct" ] ) ){
					
					$this->products[] = $rs->fields;
					$this->NBProduct++;
					
				}
				
				$rs->MoveNext();
				
				
			}
			
		}
		
		return $this->NBProduct ;
		
	}

	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
		
	}
	
}

?>