<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des commandes
 */
 
include_once( dirname( __FILE__ ) . "/adm_base.php" );
include_once( dirname( __FILE__ ) . "/adm_buyer.php" );

class ADMORDER extends ADMBASE
{
var $DEBUG = false;
var $id_order = 0;
var $buyer = false;
var $orderinfo = false;
var $orderows = false;

	/**
	 * Constructeur
	 * @param int $id Id de la commande
	 */
	function __construct($id=0)
	{
		ADMBASE::ADMBASE();
		$this->id_order = $id;
		if($id>0) $this->loadOrder();
		//print_r($this->orderinfo);
	}

	/**
	 * Retourne une valeur d'un article
	 * @param int $i L'index de l'article
	 * @param string $key Le nom de la valeur recherchée
	 */
	function GetItem($i,$key)
	{
	return $this->orderows[$i][$key];	
	}
	
	/**
	 * Retrourne une valeur de la commande
	 * @param string $key Le nom de la valeur recherchée
	 */
	function Get($key)
	{
	return $this->orderinfo[$key];	
	}
	
	/**
	 * Retourne une valeur de l'acheteur
	 * @param string $key Le nom de la valeur recherchée
	 */
	function getBuyer($key)
	{
	return $this->buyer->buyerinfo[$key];	
	}
	
	/**
	 * Recherche le devise - nom et taux
	 */
	function GetCurrency()
	{
		$OrderInfoArray['devicevalue']=1.0;
		$IdCurrency = $this->orderinfo['idcurrency'];
		$lang =  '_'.$this->LingualNumber;
		$res= DBUtil::getConnection()->Execute("SELECT name$lang as devicename ,device FROM currency WHERE idcurrency='$IdCurrency'");
		if($this->DEBUG) echo "SELECT name$lang as devicename as ,device FROM currency WHERE idcurrency='$IdCurrency'";
		if ($res->RecordCount()>0) // At least one row resulted
		{
			$this->orderinfo['devicevalue']=$res->fields['device'];
			$this->orderinfo['devicename']=$res->fields['devicename'];
		}
		else
		{
		$this->orderinfo['devicevalue']=1.0;
		$this->orderinfo['devicename']='ECU';
		/**
		 * @todo trigger warning
		 */	
		}
	}
	
	/**
	 * Charge une commande à partir de la base de données
	 */
	function LoadOrder()
	{
		$IdOrder = $this->id_order;
		$lang =  '_'.$this->LingualNumber;
			
		$this->orderinfo = DBUtil::getConnection()->GetRow("SELECT * FROM  `order` WHERE idorder=$IdOrder");
		if($this->orderinfo) {
			$this->buyer = new ADMBUYER($this->orderinfo['idbuyer']);
			$this->GetCurrency();
			} else {
				echo "ORDER NO FOUND $IdOrder ";
			}
		
		$this->orderows = DBUtil::getConnection()->GetAll("SELECT * FROM order_row  WHERE idorder='$IdOrder'");
		$this->orderinfo['RC']=count($this->orderows);
			
	
	    for ($i=0 ; $i < $this->orderinfo['RC'] ; $i++)
			{
			$RefVAT = $this->GetReferenceVAT($this->orderows[$i]['reference']);
			$this->orderows[$i]['VAT'] = $VAT = $this->ChoiceVat($RefVAT);
			$this->orderows[$i]['totalprice']= $this->orderows[$i]['unit_price'] * $this->orderows[$i]['quantity'];

			}
	
	/*
			$Str_FieldList = "total_amount,total_charge,total_weight,total_amount_wo_device";
			$res= DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM `order` WHERE idorder=$IdOrder");
			if ($res->RecordCount()>0)
			{
				// first rec
				$OrderInfoArray['total_amount'] = $res->fields['total_amount'];
				$OrderInfoArray['total_weight'] = $res->fields['total_weight'];
		        $OrderInfoArray['total_amount_wo_device'] = $res->fields['total_amount_wo_device'];
			}
	*/		
			if ($this->orderinfo['iddelivery']>0)
			{
				$this->LoadDelivery($this->orderinfo['iddelivery']);
			} else {
				/**
				 * @todo Trigger warning if iddelivery > 0 and nothing found
				 */
				 $this->orderinfo['iddelivery']=0;
			}
	}
	
	/**
	 * Charge l'adresse de livraison
	 */
	function LoadDelivery($Int_IdDelivery)
	{
	$Delivery = new ADMTABLE('delivery');
	$Delivery->LoadRecord('WHERE iddelivery='.$Int_IdDelivery);
	$this->orderinfo['iddelivery']	= 	$Delivery->get('iddelivery');
	$this->orderinfo['d_firstname']	= 	$Delivery->get('firstname');
	$this->orderinfo['d_lastname']	= 	$Delivery->get('lastname');
	$this->orderinfo['d_adress']	=	$Delivery->get('adress');
	$this->orderinfo['d_adress_2']	= 	$Delivery->get('adress_2');
	$this->orderinfo['d_city']		=	$Delivery->get('city');
	$this->orderinfo['d_zipcode']	= 	$Delivery->get('zipcode');
	$this->orderinfo['d_idstate']	= 	$Delivery->get('idstate');
	$this->orderinfo['d_state']		=	$this->GetState($this->orderinfo['d_idstate']);
	}
	
	/**
	 * Supprime une commande de la base de données
	 */
	function Delete()
	{
		$id = $this->id_order;
		$SQL_Query = "DELETE FROM `order` WHERE idorder='$id'";
		$res= DBUtil::getConnection()->Execute($SQL_Query);
		if(!$res) echo "Erreur dans la suppression de la commande !<br />";
		$SQL_Query = "DELETE FROM order_row WHERE idorder='$id'";
		$res= DBUtil::getConnection()->Execute($SQL_Query);
		if(!$res) echo "Erreur dans la suppression de la commande !<br />";
		$this->id_order=0;
	}
	
	/**
	 * Modification d'une commande de la base de données
	 */
	function Modify($table,$action,$condition)
	{
		$id = $this->id_order;
		$SQL_Query = "UPDATE $table $action $condition";
		$res= DBUtil::getConnection()->Execute($SQL_Query);
		if(!$res) {
			echo "Erreur dans la modification de la commande : $SQL_Query<br />";
			return false;
		}
		return true;
	}
	
	/**
	 * Vérification du stock
	 * @return int 1 si stock suffisant sinon 0
	 */
	function VerifyStock()
	{
		$Result=1;
		$n= $this->orderinfo['RC'];
		for($i = 0 ; $i < $n && $Result ; $i++)
		{
			$res= DBUtil::getConnection()->Execute("select stock_level from detail where reference='" . $this->orderows[$i]['reference'] . "'");
			if($res->RecordCount()>0)
			{
				// first rec
				$Stock=$res->fields['stock_level'];
				$Qtt=$this->orderows[$i]['quantity'];
	
				if($Stock>=0)
				{	if(($Stock-$Qtt)<0)
					{	$Result=0;}
				}
			}
		}
	
		return $Result;
	}
	
	/**
	 * Décrémente le stock après une commande validée
	 */
	function Unstock()
	{
		$Result=0;
		$n= $this->orderinfo['RC'];
		for($i = 0 ; $i < $n ; $i++)
		{
			$res= DBUtil::getConnection()->Execute("select stock_level from detail where reference='" . $this->orderows[$i]['reference'] . "'");
			if($res->RecordCount()>0)
			{
				// first rec
				$Stock=$res->fields['stock_level'];
				$Qtt=$this->orderows[$i]['quantity'];
				if($Stock>=0)
				{	if(($Stock-$Qtt)>=0)
					{
						$Stock-=$Qtt;
						$res= DBUtil::getConnection()->Execute("update detail set stock_level='$Stock' where reference='" . $this->orderows[$i]['reference'] . "'");
						if($res)
						{	
							$res= DBUtil::getConnection()->Execute("update detail set stock_level='$Stock' where reference='" . $this->orderows[$i]['reference'] . "'");
							if($res)
							{	$Result=1;}
						}
						else
	                        echo "<br /><center> ".Dictionnary::translate("stock_no")."<br />";
					}
				}
				else
				{	$Result=1;}
			}
		}
	
		return $Result;
	}

	/**
	 * Attribut le status 'confirmé' à la commande
	 */
	function ConfirmOrder()
	{
		trigger_error( "ne plus utiliser", "E_USER_ERROR" ); //remplacer par E_DEPRECATED quand php >= 5.3
	}

	/**
	 * Vérifie si l'acheteur est soumis à la TVA
	 * @param float $ProductVat La taux de TVA du produit
	 * @return float Le taux de TVA appliqué
	 */
	function ChoiceVat($ProductVat)
	{
		$lang =  '_'.$this->LingualNumber;
		$State =  $this->buyer->buyerinfo['state'];
		$Result=0;
		$res= DBUtil::getConnection()->Execute("select bvat from state where name$lang='$State'");
		if($res->RecordCount())
		{
			// first rec
			$BVat=$res->fields['bvat'];
			if($BVat)
				$Result=$ProductVat;
		}
		return $Result;
	}
	
	/**
	 * Recherche le taux de TVA
	 * @param int $IdVAT Id du la TVA
	 * @return float Le taux de TVA
	 */
	function GetVAT($IdVAT)
	{
		$Result=0;
	
		$res= DBUtil::getConnection()->Execute("select * from tva where idtva='$IdVAT'");
		if($res->RecordCount()>0)
		{
			// first rec
			$Result=$res->fields['tva'];
		}
	
		return $Result;
	}
	
	/**
	 * Recherche le taux de TVA pour une référence
	 * @param string $Reference La référence de l'article
	 * @return float Le taux de TVA
	 */
	function GetReferenceVAT($Reference)
	{
		$Result=0;
	
		$res= DBUtil::getConnection()->Execute("select idvat from detail where reference='$Reference'");
		if($res->RecordCount())
		{
			$IdVAT=$res->fields['idvat'];
			$Result=$this->GetVAT($IdVAT);
		}
		return $Result;	
	}
	
	/**
	 * Recherche du nom d'un pays à partir de son id
	 * @param int $IdState L'id du pays
	 * @return string Le nom du pays dans la langue courante
	 */
	function GetState($IdState)
	{
		$Result="-";
		$lang =  '_'.$this->LingualNumber;
		$res= DBUtil::getConnection()->Execute("select name$lang as name from state where idstate='$IdState'");
		if($res->RecordCount()>0)
		{
			$Result=$res->fields['name'];
		}
		return $Result;
	}
	
	/**
	 * Recherhce le taux de TVA pour un pays
	 * @param int $IdState L'id du pays
	 */
	function GetStateVat($IdState)
	{
		$lang =  '_'.$this->LingualNumber;
		
		$Result=0;
	
		if($IdState=='')
		{
			$State=GetParameter('statedefault');
			$IdState=GetIdState($State,$lang);
		}
		
		$res= DBUtil::getConnection()->Execute("select bvat from state where idstate='$IdState'");
		if($res->RecordCount()>0)
		{
			// first rec
			$BVat=$res->fields['bvat'];
			if($BVat)
			{
				$Result=GetParameter('vatdefault');
			}
		}
	
		return $Result;
	}
	
	/**
	 * Export d'une commande vers Excel
	 * @param int $IdEstimate L'id du devis
	 */
	function Order2Excel($IdOrder)
	{
		$OOrder = new ADMORDER($IdOrder);
		$Path='/tmp/';	//TODO : mettre en paramètre ou variable globale
		
		$CSV[] = "N° de commande:" . $this->orderinfo["idorder"]. ";Date;".$this->orderinfo["DateHeure"].";;;\n";
	
		//ligne du details
	
		$CSV[].= "Idrow;Designation;Reference;Quantité;Prix unitaire;Prix total;\n";
	
		for($i=0 ; $i < $this->orderinfo["RC"] ; $i++)
		{	
			$CSV[].= $OOrder->orderows[$i]["idrow"] .";". $OOrder->orderows[$i]["designation"] .";". $OOrder->orderows[$i]["reference"] .";". $OOrder->orderows[$i]["quantity"] .";". $OOrder->orderows[$i]["unit_price"] .";". $OOrder->orderows[$i]["totalprice"] ."\n";
		}
		
		$CSV[].= "\n\n";
	
		$CSV[] .= "Poids total;" . $OOrder->orderinfo["total_weight"] .";;;\n";
		$CSV[] .= "Frais de port;" . $OOrder->orderinfo["total_charge"] .";;;\n";
		$CSV[] .= "Montant total de la commande;" . $OOrder->orderinfo["total_amount"] .";;;\n";
	
		$CSV[] .= "\n\n";
	
		$CSV[] .= "Adresse de facturation;;;;\n";
	      $CSV[] .= "Id;" . $OOrder->orderinfo["idbuyer"] .";;;\n";
		$CSV[] .= "Nom;" . $OOrder->buyer->buyerinfo["lastname"] .";Prénom;" . $OOrder->buyer->buyerinfo["firstname"] .";\n";
		$CSV[] .= "Adresse;" . $OOrder->buyer->buyerinfo["adress"] .";;;\n";
		$CSV[] .= "Adresse compl.;" . $OOrder->buyer->buyerinfo["adress_2"] .";;;\n";
		$CSV[] .= "Ville;" . $OOrder->buyer->buyerinfo["city"] .";;;\n";
		$CSV[] .= "Code postal;" . $OOrder->buyer->buyerinfo["zipcode"] .";Pays;". $OOrder->buyer->buyerinfo["state"] .";\n";
	
		$CSV[] .= "\n";
	
		if($OOrder->orderinfo["iddelivery"])
		{
			$CSV[] .= "Adresse de livraison;" . $OOrder->orderinfo["iddelivery"] .";\n";
			$CSV[] .= "Nom;" . $OOrder->orderinfo["d_lastname"] .";Prénom;" . $OOrder->orderinfo["d_firstname"] .";\n";
			$CSV[] .= "Adresse;" . $OOrder->orderinfo["d_adress"] .";\n";
			$CSV[] .= "Adresse compl.;" . $OOrder->orderinfo["d_adress_2"] .";;;\n";
			$CSV[] .= "Ville;" . $OOrder->orderinfo["d_city"] .";";
			$CSV[] .= "Code postal;" . $OOrder->orderinfo["d_zipcode"] .";Pays;" . $OOrder->orderinfo["d_state"] .";\n";
		}
		$CSV[] .= "\n";
	
		$CSV[] .= "Commentaire;\n";
		$CSV[] .= $OOrder->orderinfo["comment"] .";\n";
	
		$CSV[] .= "N° de carte;\n";
		$CSV[] .= $OOrder->orderinfo["cardnumber"] .";\n";
		$CSV[] .= "Date de validité;\n";
		$CSV[] .= $OOrder->orderinfo["exp_date_card"] .";\n";
		$CSV[] .= "Nom sur la carte;\n";
		$CSV[] .= $OOrder->orderinfo["name_card"] .";\n";
	
		$Date=getdate(time());
		$Fic=$Path."Order-".$this->orderinfo["idorder"]."_".$Date["mday"]."_".$Date["mon"]."_".$Date["year"].".csv";
		$Fp=fopen($Fic,"w+");
		for($i=0 ; $i < count($CSV) ; $i++)
		{
			fputs($Fp, $CSV[$i]);
		}
		fclose($Fp);
	}
	
	/**
	 * Export de commandes en csv
	 * @param array $IdOrderArray Les id des devis
	 */
	function ExportOrder($IdOrderArray)
	{
	
		$Path='/tmp/';
		$CSV[0]="N° de commande;Designation;Reference;Quantité;Prix unitaire;Prix total;idbuyer;Nom;Prénom;Adresse;Adresse compl.;Ville;Code postal;Pays;Nom;Prenom;Adresse;Adresse compl.;Ville;Code postal;Pays;DateHeure;Poids total;Frais de port;Montant total de la commande;Commentaire;N° de carte;Date d expiration; Nom sur la carte\n";
		
		for($i=0 ; $i < count($IdOrderArray) ; $i++)
		{
			$OOrder = new ADMORDER($IdOrderArray[$i]);
			$CSV[$i].= $OOrder->orderinfo[$i]["idorder"].";";
			
			// loop for all lines'order
			for($j=0 ; $j < $OOrder->orderinfo['RC'] ; $j++)
			{
				$CSV[$i].=$OOrder->orderows[$j]["designation"] .";". $OOrder->orderows[$j]["reference"] .";". $OOrder->orderows[$j]["quantity"] .";". $OOrder->orderows[$j]["unit_price"] .";". $OOrder->orderows[$j]["totalprice"].";";
			}
			// Info's Buyer
			$CSV[$i].=$OOrder->orderinfo[$i]["idbuyer"] .";" . $OOrder->buyer->buyerinfo["lastname"] .";" . $OOrder->buyer->buyerinfo["firstname"].";".$OOrder->buyer->buyerinfo["adress"].";".$OOrder->buyer->buyerinfo["adress_2"].";".$OOrder->buyer->buyerinfo["city"].";".$OOrder->buyer->buyerinfo["zipcode"].";".$OOrder->buyer->buyerinfo["state"].";";
			// Info's Delivrery
			if($OOrder->orderinfo["iddelivery"])
			{
				$CSV[$i].=$OOrder->orderinfo["d_lastname"].";".$OOrder->orderinfo["d_firstname"].";".$OOrder->orderinfo["d_adress"].";".$OOrder->orderinfo["d_adress_2"].";".$OOrder->orderinfo["d_city"].";".$OOrder->orderinfo["d_zipcode"].";".$OOrder->orderinfo["d_state"].";";
			}
			else
			{
				$CSV[$i].=";;;;;;;";
			}
			// Info's Order	
			$CSV[$i].=$OOrder->orderinfo["DateHeure"].";".$OOrder->orderinfo["total_weight"].";".$OOrder->orderinfo["total_charge"].";".$OOrder->orderinfo["total_amount"].";".$OOrder->orderinfo["comment"].";".$OOrder->orderinfo["cardnumber"].";".$OOrder->orderinfo["exp_date_card"].";".$OOrder->orderinfo["name_card"]."\n";
	
		}
		$Date=getdate(time());
		$Fic=$Path."ExportOrder_".$Date["mday"]."_".$Date["mon"]."_".$Date["year"].".csv";
		$Fp=fopen($Fic,"w+");
		for($i=0 ; $i < count($CSV) ; $i++)
		{
			fputs($Fp, $CSV[$i]);
		}
		fclose($Fp);
	
	}
	
	/**
	 * Formattage d'un nombre pour l'affichage
	 */
	function AffNumber($Number)
	{
		$Mode=1;
		$Decimal=2;
		$Round=3;
		settype($Number,"string"); 
		$Result=$Number;
		
		if($Round!="")	$Result=round($Number,$Round);
		
		if($Mode) 	$Result=number_format($Result,$Decimal,".","");
		else 	$Result=number_format($Result,$Decimal);
		return $Result;
	}

}
?>