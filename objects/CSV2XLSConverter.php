<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object convertisseur CSS XLS
 */
 

require_once "Spreadsheet/Excel/Writer.php";
include_once( dirname( __FILE__ ) . "/CSVExportArray.php" );

class CSV2XLSConverter{

	//----------------------------------------------------------------------------
	
	private $inputFile;
	private $outputFile;
	
	private $csvDelimiter;
	private $csvEnclosure;
	private $workbook;
	
	//----------------------------------------------------------------------------
	
	function __construct( $inputFile, $outputFile ){
		
		$this->csvDelimiter = ";";
		$this->csvEnclosure = "\"";
		
		$this->inputFile = $inputFile;
		$this->outputFile = $outputFile;

		$this->workbook = new Spreadsheet_Excel_Writer();
		
		$this->convert();

	}
	
	//----------------------------------------------------------------------------
	
	private function convert(){
	
		$infile = fopen( $this->inputFile, "r" );

		$format_bold =& $this->workbook->addFormat();
		$format_bold->setBold();

		$TextLeft_format =& $this->workbook->addFormat();
		$TextLeft_format->setHAlign( "left" );

		$TextRight_format =& $this->workbook->addFormat();
		$TextRight_format->setHAlign( "right" );

		$date_format =& $this->workbook->addFormat();
		$date_format->setNumFormat( "DD-MM-YYYY" );
		$date_format->setHAlign( "right" );

		$D0_format =& $this->workbook->addFormat();
		$D0_format->setNumFormat( "#,##0" );
		$D0_format->setHAlign( "right" );

		$D1_format =& $this->workbook->addFormat();
		$D1_format->setNumFormat( "#,##0.0" );
		$D1_format->setHAlign( "right" );

		$D2_format =& $this->workbook->addFormat();
		$D2_format->setNumFormat( "#,##0.00" );
		$D2_format->setHAlign( "right" );

		$D3_format =& $this->workbook->addFormat();
		$D3_format->setNumFormat( "#,##0.000" );
		$D3_format->setHAlign( "right" );

		$D4_format =& $this->workbook->addFormat();
		$D4_format->setNumFormat( "#,##0.0000" );
		$D4_format->setHAlign( "right" );

		$format_title =& $this->workbook->addFormat();
		$format_title->setBold();
		$format_title->setColor( "white" );
		$format_title->setFgColor( "blue" );
		$format_title->setAlign( "merge" );

		// A Workbook can have multiple Worksheets
		
		$this->workbook->send( $this->outputFile );
		
		$worksheet =& $this->workbook->addWorksheet( "Sheet1" );

		$Row = 1; $First = true; $MaxCols = 0;
		$ColHeads = Array();
		$ColFormats = Array();

		While(!feof( $infile )) {
			
			$Fields = fgetcsv($infile, 1024, $this->csvDelimiter, $this->csvEnclosure );
			
			if ($First) {
				
				$First = false;
				//	grab the first row as the title
				$Title = $Fields[0];
				$worksheet->write(0, 0, $Title, $format_title);
				//	merge empty cells to contain the title
				for ($Col = 1; $Col < 10; $Col++) {
					$worksheet->write(0, $Col, "", $format_title);
				}
				//	get column formats from the second row
				$Fields = fgetcsv($infile, 1024, $this->csvDelimiter, $this->csvEnclosure );
				for ($Col = 0; $Col < count($Fields); $Col++) {
					switch ($Fields[$Col]) {
					case "TextLeft":
						$ColFormats[$Col] = "TextLeft";
						$Wid = 20.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "TextRight":
						$ColFormats[$Col] = "TextRight";
						$Wid = 10.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "Date":
						$ColFormats[$Col] = "Date";
						$Wid = 10.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "Decimal0":
						$ColFormats[$Col] = "Decimal0";
						$Wid = 11.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "Decimal1":
						$ColFormats[$Col] = "Decimal1";
						$Wid = 12.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "Decimal2":
						$ColFormats[$Col] = "Decimal2";
						$Wid = 12.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "Decimal3":
						$ColFormats[$Col] = "Decimal3";
						$Wid = 12.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					case "Decimal4":
						$ColFormats[$Col] = "Decimal4";
						$Wid = 12.0;
						$worksheet->setColumn($Col,$Col,$Wid);
						break;
					default:
						$ColFormats[$Col] = "TextLeft";
					}
				}
				//	get column headings from the third row,
				$Fields = fgetcsv($infile, 1024, $this->csvDelimiter, $this->csvEnclosure );
				$MaxCols = count($Fields);
				for ($Col = 0; $Col < $MaxCols; $Col++) {
					$ColHeads[$Col] = $Fields[$Col];
				}
				$Row++;
				for ($Col = 0; $Col < $MaxCols; $Col++) {
					$worksheet->write($Row, $Col, $ColHeads[$Col], $format_bold);
				}
			} else {
			//	now, process the csv file line contents as rows and columns 
				if (is_array($Fields) && count($Fields) > 0 ) {
					$Row++;
					for ($Col = 0; $Col < count($Fields); $Col++) {
						switch ($ColFormats[$Col]) {
						case "TextLeft":
							$worksheet->write($Row, $Col, $Fields[$Col], $TextLeft_format);
							break;
						case "TextRight":
							$worksheet->write($Row, $Col, $Fields[$Col], $TextRight_format);
							break;
						case "Date":
							$Date = $Fields[$Col];
							$Val = $this->conv_to_xls_date($Date);
							$worksheet->write($Row, $Col, $Val, $date_format);
							break;
						case "Decimal0":
							$worksheet->write($Row, $Col, $Fields[$Col], $D0_format);
							break;
						case "Decimal1":
							$worksheet->write($Row, $Col, $Fields[$Col], $D1_format);
							break;
						case "Decimal2":
							$worksheet->write($Row, $Col, $Fields[$Col], $D2_format);
							break;
						case "Decimal3":
							$worksheet->write($Row, $Col, $Fields[$Col], $D3_format);
							break;
						case "Decimal4":
							$worksheet->write($Row, $Col, $Fields[$Col], $D4_format);
							break;
						default:
							$worksheet->write($Row, $Col, $Fields[$Col], "");
						}
					}
				}
			}
	}

		//
		//	create totals lines for any Decimal formatted Column, ex: =SUM(D4:D1185)
		//
		$LastRow = $Row + 1;
		$Row++;
		for ($Col = 0; $Col < $MaxCols; $Col++) {
			$CF = $ColFormats[$Col];
			if (substr($CF,0,3) == "Dec") {
				$worksheet->writeString($Row, $Col, "==========", $TextRight_format);
				$FromCell = chr($Col + 65).'4';
				$ToCell   = chr($Col + 65).($LastRow);
				$Formula = '=SUM('.$FromCell.':'.$ToCell.')';
				switch ($CF) {
				    case "Decimal0":
					$worksheet->write($Row+1, $Col, $Formula, $D0_format);
					break;
				    case "Decimal1":
					$worksheet->write($Row+1, $Col, $Formula, $D1_format);
					break;
				    case "Decimal2":
					$worksheet->write($Row+1, $Col, $Formula, $D2_format);
					break;
				    case "Decimal3":
					$worksheet->write($Row+1, $Col, $Formula, $D3_format);
					break;
				    case "Decimal4":
					$worksheet->write($Row+1, $Col, $Formula, $D4_format);
					break;
				}
			}
		}
		
		$this->workbook->close();
		
	}

	//----------------------------------------------------------------------------
	
	private function conv_to_xls_date( $Date ){
		
		// Returns the Excel/Calc internal date integer from either an ISO date YYYY-MM-DD or MM/DD/YYYY formats.
		return (int) (25569 + (strtotime("$Date 12:00:00") / 86400));
	
	}

	//----------------------------------------------------------------------------

}

?>