<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des produits similaires
 */

include_once( dirname( __FILE__ ) . "/catalobjectbase.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );


class SIMILAR extends CATALBASE
{

	/**
	 * Contructeur
	 * @param int $idp L'id du produit
	 */
	function __construct($idp=0)
		{

		$this->id = $idp; $reload=0;
	
		$this->lang = "_1";
		if( !isset($_SESSION['similar']) )  $_SESSION['similar']=0;
		if( $_SESSION['similar'] != $idp ) $reload=1;
		if( $reload ) { $_SESSION['similar']= $idp;  $this->GetProductFromSimilar(); }
		else { 
			$this->products = &$_SESSION['similar_products'] ;
			$this->NBProduct = 0 +  count($this->products);
			}
		}
	
	/**
	 * Cherche les produit similaires
	 * @return $n les produits similiares
	 */
	public function GetProductFromSimilar()
	{
	$IdSimilar = $this->id;
	$query="SELECT distinct idproduct FROM product WHERE (idproduct_similar_1 = '$IdSimilar' OR idproduct_similar_2 = '$IdSimilar' OR idproduct_similar_3 = '$IdSimilar')  AND (stock_level>0 OR stock_level=-1 )";
	$n = $this->SetProducts($query);
	$_SESSION['similar_products'] = &$this->products ;	
	return $n;
	}
	
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
}//EOC
?>