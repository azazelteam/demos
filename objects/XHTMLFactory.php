<?php
 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion universelle des listes déroulantes
 */
 
class XHTMLFactory{

	//-----------------------------------------------------------------------------------------------
	
	function __construct(){
	
		die( "Cannot instanciate static class" );
		
	}
    
    /**
	 * Créer un élément SELECT à partir des valeurs d'une colonne donnée dans une table donnée
	 * @static
	 * @access public
	 * @param string $table le nom de la table
	 * @param string $columnAsValue le nom de la colonne utilisée pour les valeurs des OPTIONS
	 * @param mixed $columnAsInnerHTML le nom de la colonne affichée dans le menu déroulant, ou un tableau indicé de noms de colonnes
	 * @param string $orderByColumn le nom de la colonne sur laquelle est effectué le tri par ordre croissant lors de la récupération des données dans la base ( optionnel )
	 * @param string $selectedValue la valeur sélectionnée dans le menu déroulant
	 * @param string $nullValue la valeur par défaut lorsqu'il n'y a aucune sélection ( optionnel )
	 * @param string $nullInnerHTM le texte affiché dans le menu déroulant lorsque la valeur par défaut est sélectionnée
	 * @param string $attributes les attributs de l'élément SELECT
	 * @param boolean $returnHtml : afficher le resultat oubien le retourner 
	 * @param int	$ignoreValue : Ne pas afficher les valeurs dans le Select (plusieur valeurs possible séparé par des virgules)
	 */
	
	public static function createSelectElementByEncodage( $table, $columnAsValue, $columnAsInnerHTML, $orderByColumn = false, $selectedValue = false, $nullValue = false, $nullInnerHTML = "", $attributes = "", $returnHtml = false, $ignoreValue = false, $SQL_Condition = "",$typeEncodage = ""){
	
		$db =& DBUtil::getConnection();
		
		if( $orderByColumn === false )
			$orderByColumn = is_array( $columnAsInnerHTML ) ? $columnAsInnerHTML[ 0 ] : $columnAsInnerHTML;
			
		$query = "
		SELECT `$columnAsValue` AS `value`,";
		
		if( is_array( $columnAsInnerHTML ) ){
			
			$query .= " CONCAT( `";
			$query .= implode( "`, ' ',`", $columnAsInnerHTML );
			$query .= "` ) AS innerHTML";
			
		}
		else $query .= " `$columnAsInnerHTML` AS innerHTML";
		
		if( $ignoreValue === false ){
			$query .= "
			FROM `$table`
			WHERE 1
			$SQL_Condition
			ORDER BY `$orderByColumn` ASC";
		}else{
			$query .= "
			FROM `$table`
			WHERE `$columnAsValue` not in ($ignoreValue)
			$SQL_Condition
			ORDER BY `$orderByColumn` ASC";
		}
		
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			trigger_error( "Impossible de créer un menu déroulant pour la colonne '$table.$columnAsValue'", E_USER_ERROR );
		
		if( !empty( $attributes ) )
			$attributes = " $attributes";
			
		
		$html = "<select{$attributes}>" ;
				
		if( $nullValue !== false ){
			
			if(!$typeEncodage)$html .= '<option value="'.  $nullValue  .'">'. $nullInnerHTML  .'</option>' ;
            else $html .= '<option value="'.  $nullValue  .'">'.  $nullInnerHTML  .'</option>' ;
			
		}
	
		while( !$rs->EOF() ){
		
			$value 		= $rs->fields( "value" );
			$innerHTML 	= $rs->fields( "innerHTML" );
			$selected 	= $selectedValue !== false && $selectedValue == $value ? " selected=\"selected\"" : "";
			
			
			if(!$typeEncodage)$html .= '<option value="'.  $value  .'"'. $selected .'>'.  $innerHTML  .'</option>';
			else $html .= '<option value="'.  $value  .'"'. $selected .'>'.  $innerHTML  .'</option>';
			$rs->MoveNext();
			
		}
		
		
		$html .= '</select>' ; 
		
		if( $returnHtml == false ){
			echo $html ; 
		}else{
			return $html ; 
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------
	
	/**
	 * Créer un élément SELECT à partir des valeurs d'une colonne donnée dans une table donnée
	 * @static
	 * @access public
	 * @param string $table le nom de la table
	 * @param string $columnAsValue le nom de la colonne utilisée pour les valeurs des OPTIONS
	 * @param mixed $columnAsInnerHTML le nom de la colonne affichée dans le menu déroulant, ou un tableau indicé de noms de colonnes
	 * @param string $orderByColumn le nom de la colonne sur laquelle est effectué le tri par ordre croissant lors de la récupération des données dans la base ( optionnel )
	 * @param string $selectedValue la valeur sélectionnée dans le menu déroulant
	 * @param string $nullValue la valeur par défaut lorsqu'il n'y a aucune sélection ( optionnel )
	 * @param string $nullInnerHTM le texte affiché dans le menu déroulant lorsque la valeur par défaut est sélectionnée
	 * @param string $attributes les attributs de l'élément SELECT
	 * @param boolean $returnHtml : afficher le resultat oubien le retourner 
	 * @param int	$ignoreValue : Ne pas afficher les valeurs dans le Select (plusieur valeurs possible séparé par des virgules)
	 */
	
	public static function createSelectElement( $table, $columnAsValue, $columnAsInnerHTML, $orderByColumn = false, $selectedValue = false, $nullValue = false, $nullInnerHTML = "", $attributes = "", $returnHtml = false, $ignoreValue = false, $SQL_Condition = "",$typeEncodage = 0 ){
	
		$db =& DBUtil::getConnection();
		
		if( $orderByColumn === false )
			$orderByColumn = is_array( $columnAsInnerHTML ) ? $columnAsInnerHTML[ 0 ] : $columnAsInnerHTML;
			
		$query = "
		SELECT `$columnAsValue` AS `value`,";
		
		if( is_array( $columnAsInnerHTML ) ){
			
			$query .= " CONCAT( `";
			$query .= implode( "`, ' ',`", $columnAsInnerHTML );
			$query .= "` ) AS innerHTML";
			
		}
		else $query .= " `$columnAsInnerHTML` AS innerHTML";
		
		if( $ignoreValue === false ){
			$query .= "
			FROM `$table`
			WHERE 1
			$SQL_Condition
			ORDER BY `$orderByColumn` ASC";
		}else{
			$query .= "
			FROM `$table`
			WHERE `$columnAsValue` not in ($ignoreValue)
			$SQL_Condition
			ORDER BY `$orderByColumn` ASC";
		}
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			trigger_error( "Impossible de créer un menu déroulant pour la colonne '$table.$columnAsValue'", E_USER_ERROR );
		
		if( !empty( $attributes ) )
			$attributes = " $attributes";
			
		
		$html = "<select{$attributes}>" ;
				
		if( $nullValue !== false ){
			
			$html .= '<option value="'.  $nullValue  .'">'.  $nullInnerHTML  .'</option>' ;
			
		}
	
		while( !$rs->EOF() ){
		
			$value 		= $rs->fields( "value" );
			$innerHTML 	= $rs->fields( "innerHTML" );
            if(!$innerHTML) $innerHTML = $value;
			$selected 	= $selectedValue !== false && $selectedValue == $value ? " selected=\"selected\"" : "";
			
			if($typeEncodage == 1)$html .= '<option value="'.  $value  .'"'. $selected .'>'.  Util::doNothing($innerHTML)  .'</option>';
			else $html .= '<option value="'.  $value  .'"'. $selected .'>'.  $innerHTML  .'</option>';
			
			$rs->MoveNext();
			
		}
		
		
		$html .= '</select>' ; 
		
		if( $returnHtml == false ){
			echo $html ; 
		}else{
			return $html ; 
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------
	
	/**
	 * Créer un groupe d'éléménts RADIO à partir des valeurs d'une colonne donnée dans une table donnée
	 * @static
	 * @access public
	 * @param string $table le nom de la table
	 * @param string $columnAsValue le nom de la colonne utilisée pour les valeurs des OPTIONS
	 * @param string $columnAsInnerHTML le nom de la colonne affichée dans le menu déroulant
	 * @param string $orderByColumn le nom de la colonne sur laquelle est effectué le tri par ordre croissant lors de la récupération des données dans la base ( optionnel )
	 * @param string $checkedValue la valeur sélectionnée dans le menu déroulant
	 * @param string $attributes les attributs pour chaque élément RADIO
	 * @param bool $returnHTML
	 */
	
	public static function createRadioGroup( $table, $columnAsValue, $columnAsInnerHTML, $orderByColumn = false, $checkedValue = false, $attributes = "", $returnHTML = false ){
	
		if( $returnHTML )
			ob_start();
			
		if( $orderByColumn === false )
			$orderByColumn = $columnAsInnerHTML;
			
		$query = "
		SELECT `$columnAsValue` AS `value`, `$columnAsInnerHTML` AS innerHTML
		FROM `$table`
		ORDER BY `$orderByColumn` ASC";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			trigger_error( "Impossible de créer un groupe de bouton radio pour la colonne '$table.$columnAsValue'", E_USER_ERROR );
		
		if( !empty( $attributes ) )
			$attributes = " $attributes";

		while( !$rs->EOF() ){
		
			$value 		= $rs->fields( "value" );
			$innerHTML 	= $rs->fields( "innerHTML" );
			$checked 	= $checkedValue !== false && $checkedValue === $value ? " checked=\"checked\"" : "";
			
			?>
			<input type="radio" value="<?php echo  $value  ?>"<?php echo $checked ?><?php echo $attributes ?> /><?php echo  $innerHTML  ?>
			<?php
			
			$rs->MoveNext();
			
		}
		
		if( $returnHTML ){

			$data = ob_get_contents();
			ob_end_clean();
			
			return $data;
			
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------
	
	public static function truncateHTML( $html, $length, $ending = '...', $exact = false) {
	    
	    if(strlen(preg_replace('/<.*?>/', '', $html)) <= $length) {
	        return $html;
	    }
	    preg_match_all('/(<.+?>)?([^<>]*)/is', $html, $matches, PREG_SET_ORDER);
	    $total_length = 0;
	    $arr_elements = array();
	    $truncate = '';
	    foreach($matches as $element) {
	        if(!empty($element[1])) {
	            if(preg_match('/^<\s*.+?\/\s*>$/s', $element[1])) {
	            } else if(preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $element[1], $element2)) {
	                $pos = array_search($element2[1], $arr_elements);
	                if($pos !== false) {
	                    unset($arr_elements[$pos]);
	                }
	            } else if(preg_match('/^<\s*([^\s>!]+).*?>$/s', $element[1], $element2)) {
	                array_unshift($arr_elements,
	                strtolower($element2[1]));
	            }
	            $truncate .= $element[1];
	        }
	        $content_length = strlen(preg_replace('/(&[a-z]{1,6};|&#[0-9]+;)/i', ' ', $element[2]));
	        if($total_length >= $length) {
	            break;
	        } elseif ($total_length+$content_length > $length) {
	            $left = $total_length>$length?$total_length-$length:$length-$total_length;
	            $entities_length = 0;
	            if(preg_match_all('/&[a-z]{1,6};|&#[0-9]+;/i', $element[2], $element3, PREG_OFFSET_CAPTURE)) {
	                foreach($element3[0] as $entity) {
	                    if($entity[1]+1-$entities_length <= $left) {
	                        $left--;
	                        $entities_length += strlen($entity[0]);
	                    } else break;
	                }
	            }
	            $truncate .= substr($element[2], 0, $left+$entities_length);
	            break;
	        } else {
	            $truncate .= $element[2];
	            $total_length += $content_length;
	        }
	    }
	    if(!$exact) {
	        $spacepos = strrpos($truncate, ' ');
	        if(isset($spacepos)) {
	            $truncate = substr($truncate, 0, $spacepos);
	        }
	    }
	    $truncate .= $ending;
	    foreach($arr_elements as $element) {
	        $truncate .= '</' . $element . '>';
	    }
	    
	    return $truncate;
	}
	
}

?>