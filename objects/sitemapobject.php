<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object génération sitemap
 */
 
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( "$GLOBAL_START_PATH/script/global.fct.php" );

define ( "DEBUG_SITEMAP", 0 );

class Sitemap{
	
	//-------------------------------------------------------------------------------------------
	
	public static $UNORDERED_LIST 	= 0;
	public static $ORDERED_LIST 	= 1;

	//-------------------------------------------------------------------------------------------

	private $root;
	private $depth;
	private $map;
	private $listTagName;
	private $showProducts;
	private $alwaysShowLeafProducts;
	private $catalogRight;
	
	private $categoryCount;
	private $productCount;
	
	private $rewriteURLs;
	private $URLRewritingCategoryPattern;
	private $URLRewritingProductPattern;
	private $URLRewritingPDFPattern;
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param showProducts booléen, permet d'afficher les produits en plus des catégories
	 * @param depth facultatif, permet d'afficher les sous-catégories sur 'depth' niveaux
	 * @param idCategory facultatif, permet de construire la carte à partir de la catégorie spécifiée
	 */
	public function Sitemap( $showProducts, $depth = -1, $idCategory = 0 ){	

		$this->depth = $depth;
		$this->map ="";
		$this->listTagName = "ul";
		$this->showProducts = $showProducts;
		$this->catalogRight = 0;
		$this->alwaysShowLeafProducts = false;
		
		//url rewriting
	
		$this->rewriteURLs 					= DBUtil::getParameter( "RewriteEngine" ) == "on";
		$this->URLRewritingCategoryPattern 	= DBUtil::getParameter( "URLRewritingCategoryPattern" );
		$this->URLRewritingProductPattern 	= DBUtil::getParameter( "URLRewritingProductPattern" );
		$this->URLRewritingPDFPattern 		= DBUtil::getParameter( "URLRewritingPDFPattern" );
		
		$this->root = array( 
			"id" 			=> $idCategory, 
			"name" 			=> "",
			"children" 		=> array(), 
			"isCategory" 	=> true
		);
	
		$this->categoryCount = $idCategory ? 1 : 0;
		$this->productCount = 0;
		
		$this->setName( $idCategory );				
		
		$this->buildMap();
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'une propriété donnée de la racine de l'arbre
	 * @param string $value la propriété souhaitée
	 * @return string
	 */
	public function getRootValue( $value ){
		
		return $this->root[ $value ];
		
	}
	
	//-------------------------------------------------------------------------------------------
	/**
	 * Construit la carte du site
	 * @return void
	 */
	private function buildMap(){
			
		$start_time = microtime();
		
		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );
		else if( $this->showProducts && $this->alwaysShowLeafProducts )
			$this->setProducts( $this->root );
		
		$elapsed_time = ceil( ( microtime() - $start_time ) / 1000 );
		
		$this->map = $this->getCssStyle();
		
		if( strlen( $this->root["name"] ) ){
			
			$this->map .= "<" . $this->listTagName . ">";
			$this->map .= "<li>" . $this->root[ "name" ] . "</li>";
			
		}
		
		$this->createMapNode( $this->root );	
		
		if( strlen( $this->root["name"] ) )
			$this->map .= "</" . $this->listTagName . ">";
	
		if( DEBUG_SITEMAP ){
			
			$query = "SELECT COUNT(*) AS total FROM product WHERE  available = 1 AND catalog_right <= {$this->catalogRight}";
			$rs =& DBUtil::query( $query );
			
			$totalProducts = $rs->fields( "total" );

			$query = "SELECT COUNT(*) AS total FROM category WHERE catalog_right <= {$this->catalogRight}";
			$rs =& DBUtil::query( $query );
			$totalCategories = $rs->fields( "total" );
		
			echo "<p><u>buildMap()</u></p>";
			echo "<p>elapsed time : " . $elapsed_time . " ms</p>";
			echo "<p>mapped categories : " . $this->categoryCount . "</p>";
			echo "<p>mapped products : " . $this->productCount . "</p>";
			
		}
		
	}
	
	//-------------------------------------------------------------------------------------------
	/**
	 * Créé tous les noeuds enfants d'un noeud donné
	 * Fonction récursive
	 * @param array $parent le noeud parent
	 * @param int $depth variable de contrôle pour la récursivité
	 * @return void
	 */ 
	private function setCategoryChildren( &$parent, $depth ){
			
		$lang = "_1";
		
		$query = "SELECT cat.idcategory AS id, cat.name$lang AS name";
		$query .= " FROM category cat, category_link catlink";
		$query .= " WHERE catlink.idcategorychild = cat.idcategory";
		$query .= " AND catlink.idcategoryparent = " . $parent[ "id" ];
		$query .= " AND cat.catalog_right <= {$this->catalogRight}";	
		$query .= " ORDER BY catlink.displayorder";
		
		$rs =& DBUtil::query( $query );
		$this->categoryCount += $rs->RecordCount();
		
		if( DEBUG_SITEMAP ){
			
			echo "<p><u>setCategoryChildren()</u></p>";
			echo "<p>@param parent: [" . $parent[ "id" ] . "]" . $parent[ "name" ] . "</p>";
			echo "<p>@param depth: " . $depth . "</p>";
			echo "<p>query : " . $query . "</p>";
			echo "<p>results : " . $rs->RecordCount() . "</p>";
			
			
		}
		
		if( !$rs->RecordCount() && $this->showProducts ){
			
				$this->setProducts( $parent );
			
			return;
			
		}
		
		while( !$rs->EOF() ){
			
			$category  = array( 
				"id" 			=> $rs->fields( "id" ), 
				"name" 			=> $rs->fields( "name" ),
				"children" 		=> array(),
				"isCategory" 	=> true,
			);
			
			$pos = array_push( $parent[ "children" ], $category );
			$child = &$parent[ "children" ][ $pos - 1 ];
			
			if( $this->depth == -1 )
				$this->setCategoryChildren( $child, $depth );
			else if( $depth > 1 )
				$this->setCategoryChildren( $child, $depth - 1 );
			else if( $this->showProducts )
				$this->setProducts( $child );
			
			$rs->MoveNext();
		
		}
			
	}
	
	//-------------------------------------------------------------------------------------------
	/**
	 * Créé tous les noeuds 'produit' pour le noeud parent donné
	 * @param array $parent
	 * @return void
	 */ 
	private function setProducts( &$parent ){
		
		$lang = "_1";
		
		if( $this->alwaysShowLeafProducts ){
			
			$query = "SELECT idproduct AS id, name$lang AS name";
			$query .= " FROM product";
			$query .= " WHERE CAST( idcategory AS CHAR ) LIKE '" . $parent[ "id"] . "%'";
			$query .= " AND catalog_right <= {$this->catalogRight}";
			$query .= " AND available = 1";
			
		}
		else{
			
			$query = "SELECT idproduct AS id, name$lang AS name";
			$query .= " FROM product";
			$query .= " WHERE idcategory = " . $parent[ "id"];
			$query .= " AND catalog_right <= {$this->catalogRight}";
			$query .= " AND available = 1";

		}
		
		$rs =& DBUtil::query( $query );
		$this->productCount += $rs->RecordCount();
		
		if( DEBUG_SITEMAP ){
			
			echo "<p><u>setProducts()</u></p>";
			echo "<p>@param : [" . $parent[ "id" ] . "]" . $parent[ "name" ] . "</p>";
			echo "<p>query : " . $query . "</p>";
			echo "<p>results : " . $rs->RecordCount() . "</p>";
			
			
		}
		
		while( !$rs->EOF() ){
			
			$product = array( 
				"id" 			=> $rs->fields( "id" ), 
				"name" 			=> $rs->fields( "name" ),
				"children" 		=> null,
				"isCategory" 	=> false,
			);
			
			array_push( $parent[ "children"], $product );
			
			$rs->MoveNext();
			
		}
			
	}
	
	//-------------------------------------------------------------------------------------------
	/**
	 * Ajoute une noeud donné à l'arborescence HTML
	 * @param $parent les idcategories mere
	 * @return void
	 */
	private function createMapNode( &$parent ){
		
		global $GLOBAL_START_URL;

		$this->map .= "<" . $this->listTagName . ">";
		
		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){
			
			$child = &$parent[ "children" ][ $i ];
			
			$cssClass = $child[ "isCategory" ] ? "CategoryListItem" : "ProductListItem";
			$this->map .= "<li class=\"$cssClass\">";
			
			$pattern = $child[ "isCategory" ] ? $this->URLRewritingCategoryPattern : $this->URLRewritingProductPattern;
			if( $this->rewriteURLs && !empty( $pattern ) ){

				$tmp = str_replace( "%id", $child[ "id" ], $pattern );
				$tmp = str_replace( "%name", URLFactory::rewrite( $child[ "name" ] ), $tmp );
				
				$href = "$GLOBAL_START_URL/$tmp";

			}
			else{
				
				$href ="$GLOBAL_START_URL/catalog/product.php?";
				$href .= $child[ "isCategory"] ? "IdCategory=" : "IdProduct=";
				$href .= $child[ "id" ];
					
			}
			
			$this->map .= "<a href=\"$href\">";
			
			$this->map .= $child[ "name" ];
			
			if( $child[ "isCategory" ] )
				$this->map .= "&nbsp;(" . count( $child[ "children"] ) . ")";
			
			$this->map .= "</a>";
			
			$this->createMapNode( $child );
			$this->map .= "</li>";
			
			$i++;
			
		}
		
		$this->map .= "</" . $this->listTagName . ">";
		
	}

	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne la racine de l'arbre dees catégories
	 * @return array
	 */ 
	public function &getRoot(){
			
		return $this->root;
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne la carte du site au format HMTL
	 * @return string
	 */ 
	public function getMap(){
			
		return $this->map;
		
	}
	
	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne le nom de la carte
	 * return string
	 */
	public function getName(){
		
		return $this->root[ "name" ];
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Spécifie le type de liste utilisé
	 * @param int $type Sitemap::$ORDERED_LIST ou Sitemap::$UNORDERED_LIST
	 * @return void
	 */
	public function setLisType( $type ){
		
		switch( $type ){
			
			case Sitemap::$ORDERED_LIST:			
				$this->listTagName = "<ol>";
				break;
				
			case Sitemap::$UNORDERED_LIST:		
				$this->listTagName = "<ul>";
				break;
				
			default:			
				$this->listTagName = "<ul>";
				
		}
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Donne le nom d'une catégorie à l'arborescence
	 * @param int $idCategory l'identifiant de la catégorie
	 * @return void
	 */
	private function setName( $idCategory ){
		
		if( !$idCategory )
			$this->root[ "name" ] = "";
		else{
			
			$lang = "_1";
			
			$query = "SELECT name$lang AS name";
			$query .= " FROM category";
			$query .= " WHERE idcategory = " . $idCategory;
			$query .= " AND catalog_right <= {$this->catalogRight}";
	
			$rs =& DBUtil::query( $query );
			
			$this->root[ "name" ] = $rs->RecordCount() ? $rs->fields( "name" ) : "";
			
		}
		
		if( DEBUG_SITEMAP ){
			
				echo "<p><u>setName()</u></p>";
				echo "<p>@param : $idCategory</p>";
				echo "<p>query : " . $query . "</p>";
				echo "<p>result : " . $this->root["name"] . "</p>";
				
		}
		
	}

	//-------------------------------------------------------------------------------------------
	/**
	 * Récupère la css de l'arbre
	 * @return string le code css
	 */
	private function getCssStyle(){
	
		global $GLOBAL_START_PATH;
			
		$filepath = "$GLOBAL_START_PATH/css/catalog/sitemap.css";
		$cssCode = "";
		
		if( @file_exists( $filepath ) ){
			
			$cssCode = "
			<style type=\"text/css\">";
			
			$cssCode .= @file_get_contents( $filepath );
			
			$cssCode .= "
			</style>";
		
		}
		
		return $cssCode;
		
	}
	
	//-------------------------------------------------------------------------------------------
	
}
?>