<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object documents produits
 */
 
include_once( dirname( __FILE__ ) . '/../config/init.php' );
include_once( dirname( __FILE__ ) . '/Util.php' );

class ProductDocument{
	
	/** Lieu ou sont stockés les documents **/
	public static $document_root = '/data/documentation_product/' ;
	
	private $idproduct 	= 0;
	private $iddocument	= 1;
	private $docName 	= '';
	private $docPath 	= '';
	private $docURL 	= '';
	private $display 	= '';
	private $dateUpload = '';
	private $uniqueid	= 0;
	
	/**
	 * Retourne les informations nécessaires à un document produit
	 * @param int $idproduct le numéro du produit
	 * @param int $iddocument le numéro du document
	 */
	public function __construct( $idprod , $iddocument = 0 ){
		
		$this->idproduct 	= $idprod;

		if( $iddocument != 0 ){
			$document = &DBUtil::query( "SELECT * FROM product_document WHERE idproduct  = $idprod AND idproduct_document = $iddocument LIMIT 1" );
			
			$this->docName		= $document->fields( 'document_name' );
			$this->iddocument	= $iddocument;
			$this->document		= $document->fields( 'document_path' );
			$this->docPath		= self::$document_root . $idprod . '/';
			$this->docURL		= HTTP_HOST . $this->docPath . $this->document;
			$this->display		= $document->fields( 'display' ) ? true : false;
			$this->dateUpload	= Util::dateFormat( $document->fields( 'date' ) , 'd-m-Y') ;
			$this->uniqueid	= $document->fields( 'unique_id' );
		}
		
	}

	/**
	 * récupère l'id unique document
	 * @access public
	 * @return int
	 */
	public function getDocumentUniqueId(){
		return $this->uniqueid;
	}	
	
	/**
	 * récupère l'id document
	 * @access public
	 * @return int
	 */
	public function getDocumentId(){
		return $this->iddocument;
	}
	
	
	/**
	 * récupère le nom du document
	 * @access public
	 * @return string
	 */
	public function getDocumentName(){
		return $this->docName;
	}

	/**
	 * récupère le lien vers le fichier
	 * @access public
	 * @return string
	 */
	public function getDocumentPath(){
		return $this->document;
	}

	/**
	 * récupère lurl du fichier
	 * @access public
	 * @return string
	 */
	public function getDocumentURL(){
		return $this->docURL;
	}

	/**
	 * Affiché en front ou pas
	 * @access public
	 * @return bool
	 */
	public function diplayDocument(){
		return $this->display;
	}

	/**
	 * récupère la date d'upload du document
	 * @access public
	 * @return string date( 'd-m-Y' )
	 */
	public function getUploadDate(){
		return $this->dateUpload;
	}
	
	/**
	 * créér un document
	 * @access public
	 * @param $idproduct
	 * @param $file
	 * @return object ProductDocument
	 */
	public function createDocument( $idprod  ){
				
		return new ProductDocument( $idprod  );
			
	}

	/**
	 * upload un document
	 * @access public
	 * @param $file
	 */
	public function uploadDocument( $file ){
		
		$idprod = $this->idproduct;
		
		if( isset( $file ) && is_uploaded_file( $file[ "tmp_name" ] ) ){
		
			$name_file = $file[ 'name' ];
			$tmp_file = $file[ 'tmp_name' ];
			
			$extension = substr( $name_file , strrpos( $name_file , "." ) );
			
			if( ( $extension != '.doc' ) && ( $extension != '.jpg' ) && ( $extension != '.png' ) && ( $extension != '.xls' ) && ( $extension != '.pdf' ) && ( $extension != '.DOC' ) && ( $extension != '.JPG' ) && ( $extension != '.XLS' ) && ( $extension != '.PDF' ) && ( $extension != '.PNG' ) ){
	   			exit( "Format de fichier invalide" );
			}		
		
			if( !file_exists( DOCUMENT_ROOT . self::$document_root ) )
				mkdir( DOCUMENT_ROOT . self::$document_root );

			/*if( chmod( DOCUMENT_ROOT . self::$document_root , 0755 ) === false )
				exit( "Impossible de modifier les droits du répertoire " . self::$document_root );	*/			
				
			if( !file_exists( DOCUMENT_ROOT . self::$document_root . $idprod ) )
				mkdir( DOCUMENT_ROOT . self::$document_root . $idprod );
			
			$dest = DOCUMENT_ROOT . self::$document_root . $idprod;
				
		/*	if( chmod( DOCUMENT_ROOT . self::$document_root . $idprod , 0755 ) === false )
				exit( "Impossible de modifier les droits du répertoire $idprod" );	*/			
			
	    	if( !move_uploaded_file( $tmp_file , $dest . '/' . $name_file ) )
	        	exit( "Impossible de déplacer le fichier uploadé vers sa destination $dest" );
		
		/*	if( chmod( $dest , 0755 ) === false )
				exit( "Impossible de modifier les droits du fichier $dest" );	*/

			$this->document		= $name_file;
			$this->docPath		= self::$document_root . $idprod . '/';
			$this->docURL		= HTTP_HOST . $this->docPath . $this->document;				
				
			$this->setUploadDate();
		
		}
		
	}

	/**
	 * Set Document Date
	 * @access private
	 * @param $date 
	 */
	private function setUploadDate(){
		$this->dateUpload = date( 'd-m-Y' );
	}

	/**
	 * Set Document Name
	 * @access public
	 * @param $name 
	 */
	public function setDocName( $name ){
		$this->docName = $name;
	}

	/**
	 * Set Document Visibility
	 * @access private
	 * @param bool true false 
	 */
	public function setDisplayAvailable( $hop ){
		$this->display = $hop == true ? 1 : 0;
	}
	
	/**
	 * Save document
	 * @access public
	 */
	public function save(){

		$iddoc = DBUtil::query( "SELECT ifnull( MAX( idproduct_document ) , 0 ) as maxIdDoc FROM product_document WHERE idproduct = '" . $this->idproduct . "' LIMIT 1" )->fields( 'maxIdDoc' );
		
		$this->iddocument = $iddoc + 1;
		
		DBUtil::query( "INSERT INTO `product_document` ( `idproduct_document` , `idproduct` , `document_name` , `document_path` , `display` , `date` ) 
						VALUES
						( '".$this->iddocument."' , '".$this->idproduct."' , '".$this->docName."' , '".$this->document."' , '".$this->display."' , '".Util::dateFormat( $this->dateUpload , 'Y-m-d' )."')" );
		
		$this->uniqueid = DBUtil::getInsertId();
		
	}
	
	/**
	 * Update document
	 * @access public
	 */
	public function update(){
		DBUtil::query( "UPDATE `product_document` SET 
		 				`document_name` = '".$this->docName."' ,
		 				`document_path` = '".$this->document."' ,
		 				`display` = '".$this->display."' ,
		 				`date` = '".Util::dateFormat( $this->dateUpload , 'Y-m-d' )."'
		 				WHERE idproduct = " . $this->idproduct . " AND idproduct_document = " . $this->iddocument );
	}
	
	/**
	 * Delete document
	 * @access public static
	 */
	public static function delete( $idprod , $iddocument ){
		// suppression du document uploadé
		$docinfos = DBUtil::query( "SELECT * FROM `product_document` WHERE idproduct_document = $iddocument AND idproduct = $idprod LIMIT 1" );
		if( $docinfos === false || !$docinfos->RecordCount() )
			die( 'Document introuvable' );
			
		unlink( DOCUMENT_ROOT . self::$document_root . $idprod . '/' . $docinfos->fields( 'document_path' ) );
		
		// suppression base
		DBUtil::query( "DELETE FROM `product_document` WHERE idproduct_document = $iddocument AND idproduct = $idprod" );
	}

	/**
	 * Renvoie tous les documents d'un produit
	 * @access public static
	 * @return array
	 */
	public static function getDocumentsProduct( $idprod ){
		$docs = DBUtil::query( "SELECT idproduct_document FROM product_document WHERE idproduct = $idprod ORDER BY idproduct_document ASC" );
		
		if( !$docs->RecordCount() )
			return false;

		$documents = array();
		
		while( !$docs->EOF() ){
			$iddoc = $docs->fields( 'idproduct_document' );
			array_push( $documents , new ProductDocument( $idprod , $iddoc ) );
			
			$docs->MoveNext();
		}
		
		return $documents;
	}
	
}
?>