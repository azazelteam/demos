<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object couleur pour une référence
*/

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/catalog/SaleableOption.php" );
		
		
class Color implements SaleableOption{
	
	//----------------------------------------------------------------------------
	
	private $idproduct;
	private $idcolor;
	
	private $idcolor_product;
	private $display;
	private $price;
	private $percent;
	private $delay;
	private $idsupplier;
	private $code_color;
	private $name;
	private $image;
		
	/* ------------------------------------------------------------------------------------------ */
	
	function __construct( $idproduct, $idcolor ){
		
		$this->idproduct 	= $idproduct;
		$this->idcolor 		= $idcolor;
		
		$this->init();
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float
	 */
	public function getAmount(){
		
		return $this->price > 0.0 ? $this->price : false;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float
	 */
	public function getRate(){
		
		return $this->percent > 0.0 ? $this->percent : false;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	private function init(){
	
		$query = "
		SELECT * 
		FROM color_product
		WHERE idproduct = '{$this->idproduct}'
		AND idcolor = '{$this->idcolor}'
		LIMIT 1";

		$rs =& DBUtil::query( $query );

		$this->idproduct 		= $rs->fields( "idproduct" );
		$this->idcolor_product 	= $rs->fields( "idcolor_product" );
		$this->display 			= $rs->fields( "display" );
		$this->delay 			= $rs->fields( "delay" );
		$this->percent 			= $rs->fields( "percent" );
		$this->price 			= $rs->fields( "price" );		

		$rs =& DBUtil::query( "SELECT * FROM color WHERE idcolor = '{$this->idcolor}' LIMIT 1" );
		
		$this->idsupplier		= $rs->fields( "idsupplier" );
		$this->code_color 	= $rs->fields( "code_color" );
		$this->name 		= $rs->fields( "name" );
		$this->image 		= $rs->fields( "image" );
			
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	public function getId(){ return $this->idcolor; }
	public function getName(){ return $this->name; }
	public function getCode(){ return $this->code_color; }
	public function getImageURI(){ return $this->image; }

	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @deprecated : temporaire pour pouvoir instancier un objet ARTICLE ( =>
	 * revoir le constructeur de l'objet ARTICLE )
	 * @return un tableau bizarre pour pouvoir instancier un objet ARTICLE avec une couleur
	 */
	 
	public function getColorInfos(){
		
		$color = array(
		
			"idcolor_product"	=> $this->idcolor_product,
			"idproduct"			=> $this->idproduct,
			"idcolor"			=> $this->idcolor,
			"display"			=> $this->display,
			"price" 			=> $this->price,
			"percent" 			=> $this->percent,
			"delay" 			=> $this->delay,
			"idsupplier" 		=> $this->idsupplier,
			"code_color" 		=> $this->code_color,
			"name" 				=> $this->name,
			"image" 			=> $this->image
			
		);

		return array( $this->idcolor => $color );
	
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	/* ------------------------------------------------------------------------------------------ */
	
}

?>