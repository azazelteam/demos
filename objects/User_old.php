<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion utilisateur
 */

include_once( dirname( __FILE__ ) . "/DBUtil.php" );

class User{
	
	//----------------------------------------------------------------------------
	
	protected static $instance = NULL;
	
	protected $iduser;
	protected $fields;
	
	//----------------------------------------------------------------------------
	
	private function __construct(){
		$this->iduser 	= 0;
		$this->fields 	= array();
		
		$rs =& DBUtil::query( "DESCRIBE `user`" );
		
		while( !$rs->EOF() ){
		
			$field 			= $rs->fields( "Field" );
			$defaultValue 	= $rs->fields( "Default" );
			
			$this->fields[ $field ] = $defaultValue;
				
			$rs->MoveNext();
			
		}
	
		if( isset( $_SESSION[ "AdminLogin" ] ) && isset( $_SESSION[ "AdminPassword" ] ) )
			$this->login( $_SESSION[ "AdminLogin" ], $_SESSION[ "AdminPassword" ] );
			
	}

	//----------------------------------------------------------------------------
	
	public static function getInstance(){
		
		if( self::$instance == NULL )
			self::$instance = new User();
		self::createSessionForUser(self::$instance);
		return self::$instance;
		
	}
	
	//----------------------------------------------------------------------------
	
	//getters

	public function get( $fieldname ){ return $this->fields[ $fieldname ]; }

	//----------------------------------------------------------------------------
	
	public function logout(){
		
		$this->fields = array();
		$this->iduser = 0;
		
		if( isset( $_SESSION[ "AdminPassword" ] ) )
			unset( $_SESSION[ "AdminPassword" ] );
		
		if( isset( $_SESSION[ "AdminLogin" ] ) )
			unset( $_SESSION[ "AdminLogin" ] );

	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Identifier un utilisateur
	 * @param string $login l'identifiant
	 * @param string $password le MD5 du mot de passe
	 * @return bool true en cas de succès, sinon false
	 */
	public function login( $login, $md5password ){

		//tentative d'identification
		
		$query = "
		SELECT *
		FROM user
		WHERE login LIKE " . DBUtil::quote( $login ) . "
		AND password LIKE " . DBUtil::quote( $md5password ) . "
		LIMIT 1";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible d'identifier l'utilisateur" );
		
		//login ou mot de passe incorrect
			
		if( !$rs->RecordCount() ){
			
			$this->logout();
			
			return false;
		
		}
		
		//récupérer les informations de l'utilisateur
		
		$this->iduser = $rs->fields( "iduser" );
		
		$i = 0;
		while( $i < count( $rs->fields ) ){
			
			$field 		= $rs->FetchField( $i );
			$fieldname 	= $field->name;
	
			if( !in_array( $fieldname, array( "lastupdate", "username" ) ) )
				$this->fields[ $fieldname ] = $rs->fields( $fieldname );
	
			$i++;
			
		}
		
		//sauvegarde dans la session pour identification automatique
		
		$_SESSION[ "AdminLogin" ] 		= $login;
		$_SESSION[ "AdminPassword" ] 	= $md5password;
		
		return true;
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Retoune la langue de l'utilisateur
	 * @todo : la langue par défaut n'est définie nulle part
	 * @todo : virer le underscore!!!
	 */
	public static function getLang(){

		return "_1";				
	}
	public static function getLang2(){
	
$query = "SELECT active FROM `language_active` WHERE idsession = '". $_COOKIE["FrontOfficeNetlogix"] ."' ";
$rs =& DBUtil::query( $query );
		if ($rs->RecordCount() == 0 )
		{
		return "_1";	
		}
		else
		{
		$id_lang = $rs->fields( "active" );
		 return "_".$id_lang;	
		}			
	}

public static function getActiveLang(){
	/*
$query = "SELECT active FROM `front_language_active` WHERE idsession = '". session_id() ."' ";
$rs =& DBUtil::query( $query );
		if ($rs->RecordCount() == 0 )
		{
		return "_1";	
		}
		else
		{
		$id_lang = $rs->fields( "active" );
		 return "_".$id_lang;	
		}	*/	
		return "_1";	
	}
	//-----------------------------------------------------------------------------------
	
	/**
	 * Indique si l'utilisateur a les droits sur un menu
	 * @return bool true si il a les droits, false sinon
	 */
	public function getMenuPrivileges( $idmenu ){
		
		$query = "SELECT COUNT( * ) as rights FROM access_rights a RIGHT JOIN user_group ug ON a.idgroup = ug.idgroup WHERE ug.iduser = " . $this->getId() . " AND a.idmenu = $idmenu";
		$rs = DBUtil:: query( $query );
		
		if( $rs->fields( "rights" ) )
			return true;
		else
			return false;
		
	}

	//-----------------------------------------------------------------------------------

	/**
	 * Retourne l'identifiant de l'utilisateur identifié
	 * @return int sl'identifiant de l'utilisateur identifié ou 0
	 */
	public function getId(){

		return $this->iduser;
		
	}
	
	//----------------------------------------------------------------------------
	
	/* Change le mot de passe d'un utilisateur
	 * access public
	 * param string $login identifiant
	 * param string $pwd mot de passe à crypter et updater
	 * return true si le changement a été pris en compte
	 */
	
	public function changePassword( $pwd ){
		
		$query = "UPDATE user SET password = " . DBUtil::quote( $pwd ) . " WHERE iduser = " . $this->getId();
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false ){
			
			trigger_error( "Une erreur est survenue lors du changement du mot de passe.", E_USER_ERROR );
			return false;
			
		}
		
		return true;
		
	}

	//----------------------------------------------------------------------------

	protected function getIdCurrency( $idstate ){

		$query = "SELECT idcurrency FROM statecurrency WHERE idstate = '$idstate' LIMIT 1";
		$rs =& DBUtil::query( $query );

		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer la devise pour le pays '$idstate'" );

		return $rs->fields( "idcurrency" );

	}

	//----------------------------------------------------------------------------
	/**
	 * Met à jour la date de dernière connection
	 */
	public function setLastConnection($iduser){
		
		$rs =& DBUtil::query( "UPDATE user SET last_connection = Now() WHERE iduser = ".$iduser." LIMIT 1" );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * autorisé l'accès à certaines fonctionnalités du logiciel pour des clients non identifiés( banque, PHP via file_get_contents(), etc, ... )
	 * la durée des droits est limitée au script PHP en cours d'éxécution ( cf. User::__destruct() )
	 */
	public static function fakeAuthentification(){
	
		if( self::getInstance()->iduser )
			return false;
		
		$iduser = intval( DBUtil::getParameter( "contact_commercial" ) );
		
		if( !$iduser )
			return false;
			
		$rs =& DBUtil::query( "SELECT login, password FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
		
		$_SESSION[ "AdminLogin" ] 		= $rs->fields( "login" );
		$_SESSION[ "AdminPassword" ] 	= $rs->fields( "password" );
		
		self::$instance = new User();
		
		return self::getInstance()->iduser > 0;
		
	}
	
	//----------------------------------------------------------------------------
	
	public static function unfakeAuthentification(){
		
		if( self::$instance === NULL || !self::$instance->iduser )
			return;
		
		$iduser = intval( DBUtil::getParameter( "contact_commercial" ) );
		
		if( !$iduser || self::$instance->iduser != $iduser )
			return;
		
		$_SESSION[ "AdminLogin" ] 		= "";
		$_SESSION[ "AdminPassword" ] 	= "";
				
	}
	
	//----------------------------------------------------------------------------
	
	function __destruct(){
		
		self::unfakeAuthentification();
		
	}
	
	//----------------------------------------------------------------------------

	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//-----------------------------------------------------------------------------------
	/* deprecations */
	
	/**
	 * Indique si l'utilisateur a les droits administrateur
	 * @deprecated non cohérent par rapport à la nouvelle gestion des droits
	 * @return bool true si il a les droits, false sinon
	 */
	public function getHasAdminPrivileges(){
		
		return $this->get( "admin" ) == 1;
		
	}

	//-----------------------------------------------------------------------------------
	
	/**
	*	Ses variable seront disponible dans tout le back office
	*/
	private static function createSessionForUser($user){
		
		if ( !is_null($user->get('iduser')) ) {
			$dataUserRate 		=& 	DBUtil::query( "SELECT rate FROM username_rate WHERE iduser =".$user->get('iduser') );
			$_SESSION['rate'] 	= 	$dataUserRate->fields('rate');
			$_SESSION['salesman_user'] = $user->get('salesman_user'); 	
		} else {
			return 0;
		}
		
	}
}

?>