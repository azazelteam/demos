<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object téléchargement fiichiers
 */

class FileUploader{
	
	//---------------------------------------------------------------------------------	
	
	protected $fieldName;
	protected $fileName;
	
	protected $overrideExistingFile;
	protected $destination;
	protected $supportedTypes;
	
	protected $errors;
	protected $prefix;
	//---------------------------------------------------------------------------------	
	
	/**
	 * Constructeur
	 * @param string $fieldName Le nom du champ 'file' dans le formulaire à traiter
	 * @param string $destination Le répertoire de destination
	 * @param string $fileName optionnel, le nom du fichier une fois téléchargé ( si absent, garde le même nom ) 
	 */
	 
	function __construct( $fieldName, $destination, $fileName = "" , $prefix = ""){
		
		$this->fieldName = $fieldName;
		$this->fileName = $fileName;
		$this->prefix = $prefix;
		$this->destination = $destination;
		
		$this->supportedTypes = array();
		$this->overrideExistingFile = false;
		$this->errors = array();
		
		if( strlen( $this->destination ) && substr( $this->destination, strlen( $this->destination ) - 1, 1 ) == "/" )
			$this->destination = substr( $this->destination, 0, strlen( $this->destination ) - 1 );
			
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Permet d'écraser le fichier s'il existe déjà
	 * @param bool $overrideExistingFile
	 * @return void
	 */
	 
	public function setOverrideExistingFile( $overrideExistingFile = true ){ 
		
		$this->overrideExistingFile = $overrideExistingFile; 
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Définit les types de fichiers autorisés pour le téléchargement
	 * @param array $supportedTypes tableau indicé contenant les extenssions autorisées ( ex : array( "pdf", "html", "doc" ) )
	 * @return void
	 */
	 
	public function setSupportedTypes( $supportedTypes ){ 
		
		if( !is_array( $supportedTypes ) )
			return;
		
		$i = 0;
		while( $i < count( $supportedTypes ) ){
			
			$this->addSupportedType( $supportedTypes[ $i ] );
			
			$i++;
			
		} 
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Autorise un type de fichier à être téléchargé
	 * @param $supportedType l'extenssion de fichier à autoriser pour le téléchargement
	 * @return void
	 */
	 
	public function addSupportedType( $supportedType ){ 
		
		if( empty( $supportedType ) )
			return;
			
		$type = strtolower( $supportedType );
		
		if( !in_array( $supportedType, $this->supportedTypes ) )
			$this->supportedTypes[] = $supportedType;
			
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Télécharge le fichier
	 * @return bool true en cas de succès, sinon false
	 */
	 
	public function upload(){
		
		//vérifier si le répertoire de destination existe
		
		if( !$this->checkTargetDirectory() )
			return false;

		//vérifier si le fichier a bien été posté
		
		if( !isset( $_FILES[ $this->fieldName ] ) || !isset( $_FILES[ $this->fieldName ][ "tmp_name" ] ) ){
			
			$this->errors[] = "File not found in post data";
			
			return false;
			
		}
		
		if( !is_uploaded_file( $_FILES[ $this->fieldName ][ "tmp_name" ] ) ){
		
			$this->errors[] = "File could not be created by rfc1867 upload";
			
			return false;
				
		}
		
		//renommer le fichier de destination ou conserver le même nom
		
		if( empty( $this->fileName ) )
			$this->fileName = $_FILES[ $this->fieldName ][ "name" ];
		if($this->prefix)	$this->fileName = $this->prefix."_" .$this->fileName;
		if( !$this->isSupportedType() ){
			
			$this->errors[] = "File type not supported";
			
			return false;
			
		}
		
		//vérifier si le fichier de destination existe déjà
		
		$fileExists = file_exists( "{$this->destination}/{$this->fileName}" );
		
		if( $fileExists ){
			
			if( !$this->overrideExistingFile ){
			
				$this->errors[] = "Destination file already exists";
				
				return false;
					
			}
			else{
				
				@chmod( "{$this->destination}/{$this->fileName}", 0755 );
				
				if( !@unlink( "{$this->destination}/{$this->fileName}" ) ){
					
					$this->errors[] = "Cannot override existing file";
					
					return false;
						
				}
				
			}
			
		}
		
		//déplacer le fichier temporaire téléchargé
		
		if( !move_uploaded_file( $_FILES[ $this->fieldName ][ "tmp_name" ], "{$this->destination}/{$this->fileName}" ) ){
			
			$this->errors[] = "Cannot move uploaded file";
			
			return false;
			
		}
		
		//messages d'erreur serveur éventuels
		
		$error = $this->getUploadError();
		if( !empty( $error ) ){
			
			$this->errors[] = $error;
			
			return false;
			
		}
		
		//modifier les droits du fichier téléchargé
		
		@chmod( "{$this->destination}/{$this->fileName}", 0755 );
		
		return true;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Vérifie si l'extenssion du fichier à télécharger est autorisée pour l'upload
	 * @return bool true si l'extenssion de fichier est autorisée, sinon false
	 */
	 
	private function isSupportedType(){
		
		if( !count( $this->supportedTypes ) )
			return true;
			
		$pos = strrpos( $this->fileName, "." );
		
		if( $pos === false || $pos == strlen( $this->fileName ) - 1 )
			return false;
		
		$type = substr( $this->fileName, $pos + 1 );	
		
		return in_array( $type, $this->supportedTypes );
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Vérifie si le répertoire de destination existe
	 * @return bool true si le répertoire existe, sinon false
	 */
	 
	private function checkTargetDirectory(){
	
		$pos = strrpos( $this->destination, "/" );
		
		if( $pos === false ){
			
			$this->errors[] = "Invalid destination path";
			
			return false;
			
		}
		
		if( !file_exists( $this->destination ) || !is_dir( $this->destination ) ){
			
			$this->errors[] = "Target directory does not exist : {$this->destination}";
			
			return false;
				
		}
        
        return true;
		
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Récupère le message d'erreur du serveur pendant le téléchargement
	 * @return string le message d'erreur serveur s'il existe, sinon vide
	 */
	 
	public function getUploadError(){
		
		if( !isset( $_FILES[ $this->fieldName ][ "error" ] ) )
			return "";
			
		$error = $_FILES[ $this->fieldName ][ "error" ];
	
		switch( $error ){
			
			case UPLOAD_ERR_OK : 
			
				return "";
			
			case UPLOAD_ERR_INI_SIZE :
			
				return "File size exceeds the maximum upload size";
				
			case UPLOAD_ERR_FORM_SIZE :
				
				return "File size exceeds the maximum post data size";
				
			case UPLOAD_ERR_PARTIAL :
			
				return "File was partially uploaded";
				
			case UPLOAD_ERR_NO_FILE :
			
				return "No file uploaded";
				
			case UPLOAD_ERR_NO_TMP_DIR :
			
				return "Missing temporary directory for uploading files";
				
			/*case UPLOAD_ERR_CANT_WRITE : //PHP5.2.0
			/*case UPLOAD_ERR_EXTENSION : //PHP5.2.0*/
				
			default : return "";
			
		}
	
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Retourne le $index-ième message d'erreur
	 * @param $index l'indice de l'erreur souhaitée
	 * @return string le $index-ième message d'erreur
	 */
	 
	public function getErrorAt( $index ){
		
		return $this->errors[ $index ];
			
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Affiche sur la sortie standard toutes les erreurs survenues lors du téléchargement
	 * @return void
	 */
	 
	public function report(){
		
		foreach( $this->errors as $error )
			echo "<br />$error";
			
	}
	
	//---------------------------------------------------------------------------------	
	
	/**
	 * Retourne le nom du fichier modifié
	 * @return string le nom du fichier
	 */
	
	public function getFileName(){
	
		return $this->fileName;
		
	}
}

?>
