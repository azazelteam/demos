<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object remboursements clients
 */


include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Credit.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
 //------
final class Repayment extends DBObject{
	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var $credit Credit avoir
	 */
	private $credit;
	
	//----------------------------------------------------------------------------
	//members
	
	public function __construct( $idrepayment ){
		
		parent::__construct( "repayments", true, "idrepayment",  $idrepayment  );

		$this->credit = new Credit( $this->recordSet[ "idcredit" ] );
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Retourne une référence vers l'avoir constituant le remboursement
	 * @return Credit
	 */
	public function &getCredit(){ return $this->credit; }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne le n° de litige dont le remboursement est une issu
	 * @return int
	 */
	public function getIdLitigation(){ return $this->credit->get( "idlitigation" ); }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé un nouvel échange à partir d'un avoir donné et d'une commande donnée
	 * @static
	 * @param idcredit int le numéro d'avoir
	 * @param idorder int le numéro de commande
	 * @return Exchange le nouvel échange
	 */
	 public static function create( $idcredit, $amount, $idpayment, $idbuyer ){
	
	 // ------- Mise à jour de l'id gestion des Index  #1161

		$table = 'repayments';
		$idrepayment = TradeFactory::Indexations($table);
	 	$query = "
		INSERT INTO repayments(
		idrepayment,
			idcredit,
			idbuyer,
			amount,
			idpayment,
			creation_date
		) VALUES (
			'" .  $idrepayment  . "',			
			'" .  $idcredit  . "',
			'" .  $idbuyer  . "',
			'" .  $amount  . "',
			'" .  $idpayment  ."',
			NOW()
		)";
		
	 	DBUtil::query( $query ) || trigger_error( "Impossible de créer l'échange", E_USER_ERROR );
	 	
	 //	return new Repayment( DBUtil::getInsertID() );
	 	 // ------- Mise à jour de l'id gestion des Index  #1161
			return new Repayment( 	$idrepayment  );
	 	
	 }
	 
	//----------------------------------------------------------------------------
	
	public static function delete( $idrepayment ){
		
		$rs =& DBUtil::query( "SELECT idcredit FROM repayments WHERE idrepayment = '" .  $idrepayment  . "' LIMIT 1" );
		
		if( !$rs->RecordCount() )
			return;
			
		Credit::delete( $rs->fields( "idcredit" ) );
		
		DBUtil::query( "DELETE FROM repayments WHERE idrepayment = '" .  $idrepayment  . "' AND payment_date LIKE '0000-00-00 00:00:00' LIMIT 1" );
		
		return DBUtil::getAffectedRows() == 1;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Définit la date de remboursement
	 */
	public function validate(){
		
		assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		assert( "\$this->recordSet[ \"payment_date\" ] == \"0000-00-00 00:00:00\"" );
		
		$this->recordSet[ "payment_date" ] = date( "Y-m-d H:i:s" );
		
		$this->save();
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>