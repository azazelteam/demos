<?php


 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object entete facture fournisseurs
 */
 
include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ ) . "/RegulationsSupplier.php" );
include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
include_once( dirname( __FILE__ ) . "/SupplierInvoiceItem.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/Supplier.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );

 
class SupplierInvoice extends DBObject{
	
	/* ----------------------------------------------------------------------------------------------------- */
	//static data members
	
	public static $STATUS_STANDBY 				= "StandBy";
	public static $STATUS_PAID 					= "Paid";
	
	/* ----------------------------------------------------------------------------------------------------- */

    private $billing_supplier;
    private $idsupplier;
    
    /**
     * @var ArrayList $items
     */
    private $items;
    
    /**
     * @var Salesman $salesman
     */
    private $salesman;
    
    /**
     * @var Supplier $supplier
     */
    private $supplier;
    
    
    /* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Constructeur
	 * @param string $billing_supplier le numéro de facture fournisseur
	 */
	function __construct( $billing_supplier , $idsupplier ){	
		
		parent::__construct( "billing_supplier", false, "billing_supplier", $billing_supplier, "idsupplier",  $idsupplier );
		
		$this->billing_supplier 	= $billing_supplier;
		$this->idsupplier		 	= $idsupplier;
		
		$this->salesman 			= new Salesman( $this->recordSet[ "iduser" ] );
		$this->supplier				= new Supplier( $this->recordSet[ "idsupplier" ] );
		$this->items 				= new ArrayList( "SupplierInvoiceItem" );
		
		$this->setItems();
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles de la facture
	 * @access private
	 * @return void
	 */
	private function setItems(){
		
		$query = "
		SELECT idbl_delivery, idrow FROM billing_supplier_row
		WHERE billing_supplier = " . DBUtil::quote( $this->billing_supplier ) . "
		AND idsupplier = '{$this->idsupplier}'
		ORDER BY idrow ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
		
			$this->items->add( new SupplierInvoiceItem( $this->billing_supplier, $this->idsupplier, $rs->fields( "idbl_delivery" ), $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Salesman le commercial de la facture
	 */
	public function &getSalesman(){ return $this->salesman; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return Supplier le fournisseur de la facture
	 */
	public function &getSupplier(){ return $this->supplier; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Sauvegarde l'objet dans la table
	 * @access private
	 * @return void
	 */
	public function save(){
		
		//Calculs des montants
		
		$this->recalculateAmounts();

		$this->set( "username" , 	User::getInstance()->get( "login" ) );
		$this->set( "lastupdate" , date( "Y-m-d H:i:s" ) );
		
		parent::save();
		
		DBUtil::query( "DELETE FROM billing_supplier_row WHERE billing_supplier = '{$this->billing_supplier}' AND idsupplier = '{$this->idsupplier}' AND idrow <> 0" );

		$i = 0;
		while( $i < $this->items->size() ){
		
			if( $this->items->get( $i )->get( "quantity" ) > 0 )
				$this->items->get( $i )->save();

			$i++;
			
		}

	 }
	 
	/* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Créé une nouvelle facture fournisseur
	 * @static
	 * @param string $billing_buyer le numéro de la facture fournisseur
	 * @param int $idsupplier le numéro du fournisseur
	 * @param array $deliveryNotes les numéros de BL concernés par la facture fournisseur
	 * @return SupplierInvoice une nouvelle facture fournisseur
	 */
	public static function create( $billing_supplier, $idsupplier , $deliveryNotes, $total_ht=0 , $total_vat=0 ){
	 	
	 	$query = "
		INSERT INTO `billing_supplier` ( 
			billing_supplier, 
			idsupplier,
			iduser,
			status,
			`comment`,
			creation_date
		) VALUES ( 
			" . DBUtil::quote( $billing_supplier ) . ",
			'$idsupplier', 
			'" . User::getInstance()->getId() . "',
			'".SupplierInvoice::$STATUS_STANDBY."',
			'',
			'" . date( "Y-m-d" ) . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false )
			trigger_error( "Impossible de créer la nouvelle facture fournisseur", E_USER_ERROR );
		
		/* lignes article */
		
		foreach( $deliveryNotes as $idbl_delivery ){
		   $deliveryNote = new DeliveryNote( $idbl_delivery );
		//	self::invoiceDeliveryNote( $billing_supplier, $idsupplier, new DeliveryNote( $idbl_delivery ) );
        	self::invoiceDeliveryNote( $billing_supplier, $idsupplier, $deliveryNote );
        }
		//créer la facture fournisseur
		
		$supplierinvoice = new SupplierInvoice( $billing_supplier, $idsupplier );
	 	
		//montants
		
		$totalET = 0.0;
		$it = $supplierinvoice->getItems()->iterator();
		
		while( $it->hasNext() )
			$totalET += $it->next()->getPriceET();
$supplierinvoice->set( "total_vat", $total_vat );
		$supplierinvoice->set( "total_amount_ht", $total_ht );
		$supplierinvoice->set( "vat_rate", $supplierinvoice->supplier->getVATRate() );
		$supplierinvoice->set( "total_amount", round( $totalET * ( 1.0 + $supplierinvoice->supplier->getVATRate() / 100.0 ), 2 ) );
		
		$supplierinvoice->save();
		 
	 	return $supplierinvoice;
	 	
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access private
	 * @param $billing_supplier
	 * @param $idsupplier
	 * @param $deliveryNote
	 * @return void
	 */
	private static function invoiceDeliveryNote( $billing_supplier, $idsupplier, DeliveryNote &$deliveryNote ){
		
		/* BL vides ( cf. commandes fournisseurs sans ligne article = cde de port ) */
		
		if( !$deliveryNote->getItemCount() ){

			$query = "
			INSERT INTO billing_supplier_row( 
				billing_supplier,
				idsupplier,
				idbl_delivery,
				idrow,
				quantity
			) VALUES (
				" . DBUtil::quote( $billing_supplier ) . ",
				'" .  $idsupplier  . "',
				'" .  $deliveryNote->getId()  ."',
				'0',
				'0'
			)";
			
			DBUtil::query( $query ) || trigger_error( "Impossible de créer la nouvelle facture fournisseur", E_USER_ERROR );
			
			return;
			
		}
		
		/* BL avec lignes article */
				
		$it = $deliveryNote->getItems()->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			
			$query = "
			INSERT INTO billing_supplier_row( 
				billing_supplier,
				idsupplier,
				idbl_delivery,
				idrow,
				quantity
			) VALUES (
				" . DBUtil::quote( $billing_supplier ) . ",
				'" .  $idsupplier  . "',
				'" .  $deliveryNote->getId()  ."',
				'" . $item->get( "idrow" ) . "',
				'" . $item->get( "quantity" ) . "'
			)";
			
			DBUtil::query( $query ) || trigger_error( "Impossible de créer la nouvelle facture fournisseur", E_USER_ERROR );
			
		}	
	
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @return ArrayList<SupplierInvoiceItem>
	 */
	public function &getItems(){ return $this->items; }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @param SupplierInvoiceItem $item
	 * @return void
	 */
	public function addItem( SupplierInvoiceItem $item ){
		
		$i = 0;
		while( $i < $this->items->size() ){
		
			if( $this->items->get( $i )->get( "idbl_delivery" ) == $item->get( "idbl_delivery" )
				&& $this->items->get( $i )->get( "idrow" ) == $item->get( "idrow" ) ){
							
					$this->items->get( $i )->set( "quantity", $this->items->get( $i )->get( "quantity" ) + $item->get( "quantity" ) );
					
					$this->set( "total_amount", 	$this->get( "total_amount" ) + $item->get( "quantity" ) * $item->getPriceET() * ( 1.0 + $this->get( "vat_rate" ) / 100.0 ) );
					$this->set( "total_amount_ht", 	$this->get( "total_amount_ht" ) + $item->get( "quantity" ) * $item->getPriceET() );

					return;
					
			}
				
			$i++;
			
		}

		$this->items->add(
		
			self::createItem( 
			
				$this->billing_supplier, 
				$this->idsupplier, 
				$item->get( "idbl_delivery" ), 
				$item->get( "idrow" ), 
				$item->get( "quantity" )
				
			)
			
		);
		
		$this->set( "total_amount", 	$this->get( "total_amount" ) + $item->get( "quantity" ) * $item->getPriceET() * ( 1.0 + $this->get( "vat_rate" ) / 100.0 ) );
		$this->set( "total_amount_ht", 	$this->get( "total_amount_ht" ) + $item->get( "quantity" ) * $item->getPriceET() );
					
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @static
	 * @return SupplierInvoiceItem
	 */
	public static function createItem( $billing_supplier, $idsupplier, $idbl_delivery, $idrow, $quantity = 1 ){
		
		$query = "
		INSERT INTO billing_supplier_row ( billing_supplier, idsupplier, idbl_delivery, idrow, quantity )
		VALUES( 
			" . DBUtil::quote( $billing_supplier ) . ", 
			'" .  $idsupplier  . "', 
			'" .  $idbl_delivery  ."', 
			'" .  $idrow  . "', 
			'" .  $quantity  ."' 
		)";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			return NULL;
			
		return new SupplierInvoiceItem( $billing_supplier, $idsupplier, $idbl_delivery, $idrow );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @param string $billing_supplier
	 * @return bool TRUE en cas de succès sinon FALSE
	 */
	public function renameTo( $billing_supplier ){
		
		if( !$this->get( "editable" ) )
			return false;
			
		$query = "
		SELECT billing_supplier
		FROM billing_supplier
		WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
		AND idsupplier = '{$this->idsupplier}'
		LIMIT 1";
		
		if( DBUtil::query( $query )->RecordCount() != 0 )
			return false;
		
		$tables = array(
		
			"billing_supplier",
			"billing_supplier_row",
			"credit_supplier",
			"billing_regulations_supplier"
		
		);
			
		$ret = true;
		foreach( $tables as $table )
			$ret &= DBUtil::query( "UPDATE `$table` SET billing_supplier = " . DBUtil::quote( $billing_supplier ) . " WHERE billing_supplier = " . DBUtil::quote( $this->billing_supplier ) . " AND idsupplier = '{$this->idsupplier}'" ) !== false;	
	
		return $ret !== false;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Créé un duplicata de la facture ( hors montants facturés )
	 * La facture créée est modifiable ( donc ne peut être comptabilisée )
	 * @access public
	 * @return SupplierInvoice
	 */
	public function &duplicate(){

		$suffix = 1;
		do{
			
			$suffix++;
			$rs =& DBUtil::query( "SELECT billing_supplier FROM billing_supplier WHERE billing_supplier = " . DBUtil::quote( $this->billing_supplier . "/$suffix" ) . " AND idsupplier = '{$this->idsupplier}' LIMIT 1" );
			
		}
		while( $rs->RecordCount() );
	
		$supplierInvoice = self::create( "{$this->billing_supplier}/$suffix", $this->idsupplier , array() );
		
		$supplierInvoice->set( "Date", 				date( "Y-m-d" ) );
		$supplierInvoice->set( "creation_date", 	date( "Y-m-d" ) );
		
		$supplierInvoice->set( "iduser", 			User::getInstance()->getId() );
		$supplierInvoice->set( "idpayment", 		$this->get( "idpayment" ) );
		$supplierInvoice->set( "idpayment_delay", 	$this->get( "idpayment_delay" ) );
		$supplierInvoice->set( "deliv_payment", 	$this->get( "deliv_payment" ) );
		$supplierInvoice->set( "payment_date", 		$this->get( "payment_date" ) );
		$supplierInvoice->set( "status", 			$this->get( "status" ) );
		$supplierInvoice->set( "proforma", 			$this->get( "proforma" ) );
		$supplierInvoice->set( "comment", 			"" );
		$supplierInvoice->set( "billing_rate", 		$this->get( "billing_rate" ) );
		$supplierInvoice->set( "editable", 			"1" );
		
		
		//on créé le lien avec les proformas
		$proformas = DBUtil::query("SELECT proforma_id FROM billing_proforma WHERE billing_supplier LIKE '{$this->billing_supplier}'");
		if( $proformas->RecordCount() > 0 ){
			while( !$proformas->EOF() ){
				$proforma_id = $proformas->fields( 'proforma_id' );
				
				DBUtil::query("INSERT INTO `billing_proforma` ( `billing_supplier` , `proforma_id` ) VALUES ( '{$this->billing_supplier}/$suffix' , '$proforma_id' )");
				
				$proformas->MoveNext();
			}
		}
		
		if( $this->get( "rebate_type" ) == "rate" ){
			
			$supplierInvoice->set( "rebate_type", 	"rate" );
			$supplierInvoice->set( "rebate_value", 	$this->get( "rebate_value" ) );
		
		}
		
		if( $this->get( "down_payment_type" ) == "rate" ){
		
			$supplierInvoice->set( "down_payment_type",		"rate" );
			$supplierInvoice->set( "down_payment_value", 	$this->get( "billing_rate" ) );
			
		}
		
		$supplierInvoice->save();
		
		return $supplierInvoice;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */

	private function recalculateAmounts(){
		
		$vat_rate = $this->get( "vat_rate" );
		
		$total_amount = $this->get( "total_amount" );
		$total_charge_ht = $this->get( "total_charge_ht" );
	$total_amount_ht =	 $this->get( "total_amount_ht" );
	$total_vat =	 $this->get( "total_vat" );
	$total_charge =	 $this->get( "total_charge" );
		/*if( $vat_rate == 0.0 ){
			$total_amount_ht = $total_amount;
			$total_charge = $total_charge_ht;
		}else{
			$total_amount_ht = $total_amount / ( 1 + $vat_rate / 100 ) ;
			$total_amount_ht = round($total_amount_ht,2);
			$total_charge = $total_charge_ht * ( 1 + $vat_rate / 100 ) ;
			$total_charge = round($total_charge,2);
		}*/
		
		$this->set( "total_amount_ht" , $total_amount_ht );
		$this->set( "total_vat" , $total_vat );
		$this->set( "total_charge" , $total_charge );	
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Reste à payer
	 * Calcule le montant TTC qui reste à payer dans le cas de paiement multiples 
	 * @return le montant TTC qui reste à payer putain de merde
	 * @deprecated
	 */
	 
	public static function getBalanceOutstanding( $billing_supplier, $idsupplier ) {
		
		// alors changement de plan, on arrete de transférer les paiements d'une proforma à une facture, c'est trop le bordel
		// de plus on a plus de tracabilité et on coince si par malheur un jour on reçoit 2 factures pour une proforma.
		// donc ci dessous je récup les règlements sur les proforma et sur les factures
		// rien de bien violent mais c'est très chiant
		// Et il ne faudra donc pas s'inquiéter d'avoir des factures payées mais sans règlement ( ben oui il est ptet sur la proforma )

		$isProForma = DBUtil::getDBValue( "proforma" , "billing_supplier" , "billing_supplier" , DBUtil::quote( $billing_supplier ) );
		$orBillings = "";
		
		$prosSum = 0.00;
		
		if( !$isProForma ){
			// on recup les paiements sur les proforma
			$queryPro = DBUtil::query( "SELECT bp.proforma_id 
										FROM billing_proforma bp, billing_supplier bs 
										WHERE bs.billing_supplier = bp.billing_supplier
										AND bp.billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
										AND bs.idsupplier = $idsupplier" );
									
			if( $queryPro->RecordCount() ){
				while( !$queryPro->EOF() ){
					$proSupplier = $queryPro->fields( 'proforma_id' );
					
					$prosSum += DBUtil::query("
									SELECT COALESCE(( SUM( billing_regulations_supplier.amount ) + SUM(regulations_supplier.overpayment)),0 ) AS suma
									FROM billing_regulations_supplier,	regulations_supplier, billing_supplier
									WHERE billing_regulations_supplier.billing_supplier = " . DBUtil::quote( $proSupplier ) . "
									AND billing_regulations_supplier.idsupplier = $idsupplier
									AND billing_supplier.idsupplier = $idsupplier
									AND billing_regulations_supplier.idregulations_supplier = regulations_supplier.idregulations_supplier
									AND billing_regulations_supplier.billing_supplier = billing_supplier.billing_supplier
									AND regulations_supplier.from_down_payment = 0")->fields( 'suma' );
					
					$queryPro->MoveNext();
				}
			}
			
		}
		
		// puis pour la facture elle même
		$query = "
		SELECT COALESCE(( SUM( billing_regulations_supplier.amount ) + SUM(regulations_supplier.overpayment)),0 ) AS suma
		FROM billing_regulations_supplier,	regulations_supplier, billing_supplier
		WHERE billing_regulations_supplier.billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
		AND billing_regulations_supplier.idsupplier = $idsupplier
		AND billing_supplier.idsupplier = $idsupplier
		AND billing_regulations_supplier.idregulations_supplier = regulations_supplier.idregulations_supplier
		AND billing_regulations_supplier.billing_supplier = billing_supplier.billing_supplier
		AND regulations_supplier.from_down_payment = 0";

		$rs = DBUtil::query($query);

		$bigBill = SupplierInvoice::getNetBill( $billing_supplier, $idsupplier );
		
		$amount = $bigBill - abs($rs->fields("suma")) - abs( $prosSum );
		
		// on rajoute la somme des avoirs utilisés sur la facture
		// pas d'avoir sur proforma inutile de faire une requete
		
		if( !$isProForma ){
			$amount -= DBUtil::query("SELECT ifnull( SUM(csr.amount) , 0.00 ) as myAmount
							FROM credit_supplier_regulation csr 
							WHERE csr.billing_supplier = ". DBUtil::quote( $billing_supplier )."
							GROUP BY csr.billing_supplier")->fields('myAmount');
		}
		
		return $amount ;
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	
	/**
	 * Net à payer ( après escompte )
	 * @return le montant TTC du panier avec charges, remise ,remise pour paiement comptant, escompte et acompte
	 * @deprecated
	 */
	 
	public static function getNetBill( $billing_supplier , $idsupplier) {
	
		$query = "SELECT total_amount, rebate_value, rebate_type, credit_amount
				 FROM billing_supplier
				WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
				AND billing_supplier.idsupplier = $idsupplier";
				
		$rs = DBUtil::query($query);
		
		$netBill = $rs->fields( "total_amount" );
		
		if( $rs->fields( "rebate_value" ) > 0.00 )
			$netBill -= $rs->fields( "rebate_type" ) == "amount" ? $rs->fields( "rebate_value" ) : $netBill * $rs->fields( "rebate_value" ) / 100.0;
		
		$netBill -= SupplierInvoice::getDownPaymentAmount( $billing_supplier, $idsupplier );
		
		if( $rs->fields( "credit_amount" ) > 0.00 )
			$netBill -= $rs->fields( "credit_amount" );
		
		return round( $netBill, 2 );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	 
	/**
	 * Récupère le pourcentage d'acompte
	 * @return float le pourcentage d'acompte
	 * @deprecated
	 */
	 
	public static function getDownPaymentRate( $billing_supplier, $idsupplier ){
		
		$query = "SELECT down_payment_value, down_payment_type, total_amount
				FROM billing_supplier
				WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
				AND idsupplier = $idsupplier";
		
		$rs = DBUtil::query($query);
			
		$down_payment_value = $rs->fields( "down_payment_value" );
		
		if( $rs->fields( "down_payment_type" ) == "rate" )
			return $down_payment_value;
			
		return $rs->fields( "total_amount" ) > 0.0 ? round( $down_payment_value / $rs->fields( "total_amount" ) * 100, 2 ) : 0.0;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	 
	/**
	 * Récupère le montant de l'acompte
	 * @return float le montant de l'acompte
	 * @deprecated
	 */
	 
	public static function getDownPaymentAmount( $billing_supplier, $idsupplier ){
		
		$query = "SELECT down_payment_value, down_payment_type, total_amount
				FROM billing_supplier
				WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
				AND idsupplier = $idsupplier";
		
		$rs = DBUtil::query($query);
			
		$down_payment_value = $rs->fields( "down_payment_value" );
		
		if( $rs->fields( "down_payment_type" ) == "rate" )
			
			return round( $rs->fields( "total_amount" ) * $down_payment_value / 100, 2 );
			
		else
			return $down_payment_value;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * Récupère le pourcentage d'escompte
	 * @return float le pourcentage d'escompte
	 * @deprecated
	 */
	public static function getRebateRate( $billing_supplier, $idsupplier ){
		
		$query = "SELECT rebate_value, rebate_type, total_amount
				FROM billing_supplier
				WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
				AND idsupplier = $idsupplier";
		
		$rs = DBUtil::query($query);
			
		$rebate_value = $rs->fields( "rebate_value" );
		
		if( $rs->fields( "rebate_type" ) == "rate" )
			return $rebate_value;
			
		return $rs->fields( "total_amount" ) > 0.0 ? round( $rebate_value / $rs->fields( "total_amount" ) * 100, 2 ) : 0.0;
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	 
	/**
	 * Récupère le montant de l'escompte
	 * @return float le montant de l'escompte
	 * @deprecated
	 */
	public static function getRebateAmount( $billing_supplier, $idsupplier ){
		
		$query = "SELECT rebate_value, rebate_type, total_amount
				FROM billing_supplier
				WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
				AND idsupplier = $idsupplier";
		
		$rs = DBUtil::query($query);
			
		$rebate_value = $rs->fields( "rebate_value" );
		
		if( $rs->fields( "rebate_type" ) == "rate" )
			return round( $rs->fields( "total_amount" ) * $rebate_value / 100, 2 );
			
		return $rebate_value;
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	 
	/**
	 * Transfère les règlements de la facture proforma dans la facture définitive
	 * @param $proforma, lid de la facture proforma
	 * @param $billing_supplier, lid de la facture définitive
	 * @param $idsupplier, lid du fournisseur
	 * @deprecated
	 */
	 public static function forwardRegulationsSupplier ( $proforma, $billing_supplier, $idsupplier ){
	 				
		$query = "UPDATE billing_regulations_supplier
				SET billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
				WHERE billing_supplier = '$proforma'
				AND idsupplier = $idsupplier";
		$rsu = DBUtil::query($query);
		
		if( $rsu === false)
			trigger_error("Impossible de mettre à jour les règlements pour les factures fournisseur $proforma -> $billing_supplier", E_USER_ERROR);
			
	 }
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access public
	 * @static
	 * @param string $billing_supplier
	 * @param int $idsupplier
	 * @return void
	 */
	public static function delete( $billing_supplier, $idsupplier ){
		
		/* vérifier si la facture n'est pas payée */
		
		$query = "SELECT payment_date FROM billing_supplier WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . " AND idsupplier = '" .  $idsupplier  . "' LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs->fields( "payment_date" ) != "0000-00-00" ){
			
			trigger_error( "Impossible de supprimer une facture fournisseur déjà payée", E_USER_ERROR );
			return;
				
		} 
		
		DBUtil::query( "DELETE FROM billing_supplier WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . " AND idsupplier = '" .  $idsupplier  . "' LIMIT 1" );
		DBUtil::query( "DELETE FROM billing_supplier_row WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . " AND idsupplier = '" .  $idsupplier  . "'" );
		
	}

	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @deprecated bas niveau
	 */
	public static function getInvoicedDeliveryNotes( $billing_supplier, $idsupplier ){
		
		$query = "
		SELECT GROUP_CONCAT( DISTINCT( idbl_delivery )  ) AS deliveryNotes
		FROM billing_supplier_row 
		WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
		AND idsupplier = '$idsupplier'";
		
		$rs =& DBUtil::query( $query );
		
		return explode( ",", $rs->fields( "deliveryNotes" ) );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @deprecated bas niveau
	 */
	public static function getSupplierInvoiceOS( $billing_supplier, $idsupplier ){
		
		$query = "
		SELECT GROUP_CONCAT( DISTINCT bl.idorder_supplier ) AS orders 
		FROM bl_delivery bl, billing_supplier_row bsr 
		WHERE bsr.billing_supplier = " . DBUtil::quote( $billing_supplier ) . " 
		AND bsr.idsupplier = '$idsupplier' 
		AND bsr.idbl_delivery = bl.idbl_delivery";
		
		$rs =& DBUtil::query( $query );
		
		return explode( ",", $rs->fields( "orders" ) );
	
	}
	
	/* ----------------------------------------------------------------------------------------------------- */

}

?>