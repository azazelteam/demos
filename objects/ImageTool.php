<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object fonctions principales gestion images  photos
 */
/*
 * Fonctions utiles sur les images.
 */

class ImageTool {

	//----------------------------------------------------------------------------

	/*
	 * Convertit une image en noir et blanc et l'enregistre dans l'emplacement indiqué en deuxième paramètre
	 * img_src : url de l'image à convertir
	 * img_dest : url de la nouvelle image en noir et blanc
	 * 
	 */
	public static function convertImageToBw($url_src, $url_dest) {

		//Fonction imagecreatefromjpeg pas dispo sur Paris
		return;
		
		//détection de l'extenstion de l'image à convertir
		$file = pathinfo($url_src);
		$ext = $file['extension'];

		//appel de la bonne fonction selon l'extansion
		switch ($ext) {

			case 'jpeg' || 'jpg' :
				$img_src = imagecreatefromjpeg($url_src);
				break;

			case 'gif' :
				$img_src = imagecreatefromgif($url_src);
				break;

			case 'png' :
				$img_src = imagecreatefrompng($url_src);
				break;

			default :
				return false;
		}

		//détection l'extension de l'image convertie
		$file = pathinfo($url_dest);
		$ext = $file['extension'];

		//appel de la bonne fonction selon l'extansion
		switch ($ext) {

			case 'jpeg' || 'jpg' :
				$img_dest = imagecreatefromjpeg($url_src);
				break;

			case 'gif' :
				$img_dest = imagecreatefromgif($url_src);
				break;

			case 'png' :
				$img_dest = imagecreatefrompng($url_src);
				break;

			default :
				return false;
		}

		//calcul de la taille de l'image source
		$height = imagesy($img_src);
		$width = imagesx($img_src);

		// Copie et fusionne - Gris 
//		imagecopymergegray($img_dest, $img_src, 0, 0, 0, 0, $width, $height, 1);

		//enregistrement de la nouvelle image sur le disque 
		//appel de la bonne fonction selon l'extansion

		//détection l'extension de l'url de destination
		$file = pathinfo($url_dest);
		$ext = $file['extension'];
		switch ($ext) {

			case 'jpeg' || 'jpg' :
				imagejpeg($img_dest, $url_dest);
				break;

			case 'gif' :
				imagegif($img_dest, $url_dest);
				break;

			case 'png' :
				imagepng($img_dest, $url_dest);
				break;

			default :
				return false;
		}


//die("après écriture".$url_dest);

		//suppression de l'image en mémoire
		imagedestroy($img_src);
		imagedestroy($img_dest);
	}

	//----------------------------------------------------------------------------

	/*Convertit une image en noir et blanc si l'argument bw est à true 
	 * et la place dans le répertoire temporaire indiqué par l'argument repTemp
	 */
	public static function imageFormat($pathImage, $bw = false, $repTemp) {

		global $GLOBAL_START_PATH, $GLOBAL_START_URL;

		if (is_file($GLOBAL_START_PATH . $pathImage)) {

			if ($bw == true) { //si on a la couleur noir et blanc

				$urlLogo = $GLOBAL_START_PATH . "/" . $pathImage;

				$file = pathinfo($urlLogo);

				$fileName = $file['filename'];
				$fileName .= "_bw";

				$fileExt = $file['extension'];

				$urlLogoTemp = $repTemp . "/" . $fileName . "." . $fileExt; //on met le fichier temporaire à la racine du répertoire pour les pdfs	

				ImageTool::convertImageToBw($urlLogo, $GLOBAL_START_PATH . $urlLogoTemp);

				$finalImgPath = $GLOBAL_START_URL . $urlLogoTemp;

			} else {

				$finalImgPath = $GLOBAL_START_URL . "/" . $pathImage;
			}
		} else 
		{
			$finalImgPath = null;
		}
		return $finalImgPath;

	}

	/*
	 * Supprime toutes les images d'un répertoire spécifié en argument
	 */
	public static function cleanRep($rep) {

		$dir = opendir($rep);

		while ($f = readdir($dir)) {

			$filePath = $rep . "/" . $f;

			if (is_file($filePath)) {

				$pathInfos = pathinfo($filePath);

				$ext = $pathInfos['extension'];

				if ($ext == "jpg" || $ext == "jpeg" || $ext == "gif" || $ext == "bmp" || $ext == "png") { //si il s'agit d'une image					
					unlink($filePath); //on supprime le fichier
				}
			}
		}
	}

}
?>