<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion de l'éditeur HTML
 */

class WYSIWYGEditor{

	//----------------------------------------------------------------------
	
	private $html;
	private $elementID;
	private $selector;
	
	private $width;
	private $height;
	
	//----------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $elementID l'identifiant HTML de l'éditeur
	 */
	
	function __construct( $elementID ){
	
		$this->elementID 	= $elementID;
		$this->html 		= "";
		$this->selector 	= "WYSIWGYEditor";
		
		$this->width = 0; //100%
		$this->height = 0; //auto
		
	}
	
	//----------------------------------------------------------------------
	
	/*
	 * Défini sélecteur tinyMCE utilisé pour initialiser les textareas
	 * @access public
	 * @param string $$selector le sélecteur utilisé pour initialiser les textareas
	 */
	
	/*function setSelector( $selector ){
	
		$this->selector = $selector;
		
	}*/
	
	//----------------------------------------------------------------------
	
	/**
	 * Défini le code HTML à éditer
	 * @access public
	 * @param string $html le code HTML à éditer
	 */
	
	public function setHTML( $html ){
	
		$this->html = $html;
		
	}
	
	//----------------------------------------------------------------------
	
	/**
	 * Défini la largeur de l'éditeur
	 * @access public
	 * @param string $width la largeur de l'éditeur
	 * @return void
	 */
	
	public function setWidth( $width ){
	
		if( !is_numeric( $width ) )
			return;
			
		$this->width = $width;
		
	}
	
	//----------------------------------------------------------------------
	
	/**
	 * Défini la hauteur de l'éditeur
	 * @access public
	 * @param string $height la hauteur de l'éditeur
	 * @return void
	 */
	
	function setHeight( $height ){
	
		if( !is_numeric( $height ) )
			return;
		
		$this->height = $height;
		
	}
	
//----------------------------------------------------------------------
	
	/**
	 * Défini la taille de l'éditeur
	 * @access public
	 * @param string $width la largeur de l'éditeur
	 * @param string $height la hauteur de l'éditeur
	 * @return void
	 */
	
	public function setDimension( $width, $height ){
	
		$this->setWidth( $width );
		$this->setHeight( $height );
		
	}
	
	//----------------------------------------------------------------------
	
	/**
	 * Affiche l'éditeur
	 * @access public
	 * @return void
	 */
	
	public function display(){

		global $GLOBAL_START_URL;
		
		$width 	= $this->width ? $this->width . "px" : "100%";
		$height = $this->height ? $this->height . "px" : "auto";
        if( $height == "auto" && empty( $this->html ) )
			$height = "250px";
		$this->html = htmlspecialchars_decode($this->html);
        $this->html = htmlspecialchars_decode($this->html);
        $this->html = htmlspecialchars_decode($this->html);
		?>
		<div id="<?php echo htmlentities( $this->elementID ) ?>_parent" style="position:relative; text-align:left; height:<?php echo $height ?>; width:<?php echo $width ?>; padding:0px;">
			<?php
			/*<p id="<?php echo htmlentities( $this->elementID ) ?>_help" style="text-align:right; vertical-align:top; width:100%">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/btn_create.gif" alt="" style="vertical-align:middle; border-style:none;" />
				<a href="#" onclick="html2wysiwyg( document.getElementById( '<?php echo  $this->elementID ?>' ) ); return false;" style="background-color:#FFFFFF; font-weight:bold; font-size:12px;">Cliquez pour éditer</a>
			</p>*/
			?>
			<div id="<?php echo htmlentities( $this->elementID ) ?>" style="cursor:pointer; position:relative; margin:0px; top:0px; left:0px; width:<?php echo $width ?>; height:<?php echo $height ?>; overflow:auto;" onclick="html2wysiwyg( this );">
				<?php echo $this->html ?>
			</div>
			<input type="hidden" name="<?php echo htmlentities( $this->elementID ) ?>" id="<?php echo htmlentities( $this->elementID ) ?>_hidden" value="<?php echo htmlentities( $this->html ) ?>" />
		</div>
		<?php
		
	}
	
	//----------------------------------------------------------------------
	
	/**
	 * Intègre les fonctions javascript nécéssaires au fonctionnement de l'éditeur dans la page HTML 
	 * @access public
	 * @static
	 */
	
	public static function init( $useAdvancedTheme = true, $width = 0, $height = 0, $onReadyStateCallback = false ){
	
		global $GLOBAL_START_URL;
		
		?>
		<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript" language="javascript">
		<!--
		
		function initWYSIWGEditors() {
	
		<?php
		
		if( $useAdvancedTheme ){
		
			?>
			tinyMCE.init({
				mode : "textareas",
				theme : "advanced",
				language : 'fr',
				plugins : "table,save,advhr,advimage,advlink,emotions,iespell,spellchecker,insertdatetime,preview,zoom,flash,searchreplace,print,contextmenu",
				theme_advanced_buttons1_add_before : "save,separator",
				theme_advanced_buttons1_add : "fontselect,fontsizeselect",
				theme_advanced_buttons2_add : "separator,insertdate,inserttime,preview,zoom,fullscreen,separator,forecolor,backcolor",
				theme_advanced_buttons2_add_before: "cut,copy,paste,separator,search,replace,separator",
				theme_advanced_buttons3_add_before : "tablecontrols,separator",
				theme_advanced_buttons3_add : "emotions,iespell,flash,advhr,separator,print",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				plugin_insertdate_dateFormat : "%d-%m-%Y",
				plugin_insertdate_timeFormat : "%H:%M:%S",
				extended_valid_elements : "style[*],a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
				external_link_list_url : "example_data/example_link_list.js",
				external_image_list_url : "example_data/example_image_list.js",
				flash_external_list_url : "example_data/example_flash_list.js",
				elements : 'abshosturls',
				relative_urls : false,
				remove_script_host : false,
				spellchecker_languages : "+French=fr,English=en,German=de,Italian=it,Spanish=es",
				spellchecker_rpc_url : '<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/plugins/spellchecker/rpc.php'<?php
						
					if( $onReadyStateCallback ){
						
						?>,
						setup : function( ed ){
					      ed.onInit.add(function(ed){
					          <?php echo $onReadyStateCallback; ?>();
					      });
   						}
   						<?php
					}
				
				?>
			});
			 <?php
			 
		}
		else{
		
			?>
			tinyMCE.init({
				mode : "textareas",
				editor_selector : "WYSIWYGEditor",
				theme : "advanced",
				language : 'fr',		
				plugins : "table,advimage,advlink,iespell,spellchecker,preview,contextmenu,paste,directionality,fullscreen,noneditable,xhtmlxtras,template",
				theme_advanced_buttons1 : "newdocument,|,undo,redo,|,cut,copy,paste,pastetext,pasteword,|,cleanup,removeformat,|,preview,fullscreen",
				theme_advanced_buttons2 : "bold,italic,underline,strikethrough,fontselect,fontsizeselect,spellchecker,|,bullist,|,outdent,indent,|,hr,|,link,unlink,code",
				theme_advanced_buttons3 : false,
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : false,
				elements : 'abshosturls',
				relative_urls : false,
				remove_script_host : false,
				spellchecker_languages : "+French=fr,English=en,German=de,Italian=it,Spanish=es",
				spellchecker_rpc_url : '<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/plugins/spellchecker/rpc.php'<?php
						
					if( $onReadyStateCallback ){
						
						?>,
						setup : function( ed ){
					      ed.onInit.add(function(ed){
					          <?php echo $onReadyStateCallback; ?>();
					      });
   						}
   						<?php
					}
				
				?>
			});
		<?php
		
		}
		
		?>
		
			   
			}
			
			function html2wysiwyg( HTMLDiv ){
				
				var html = HTMLDiv.innerHTML;
				var elementID = HTMLDiv.getAttribute( 'id' );

				/*var helpID = elementID + '_help';
				var helpDiv = document.getElementById( helpID );*/

				var parentID = elementID + '_parent';
				var parentNode = document.getElementById( parentID );

				var hiddenID = elementID + '_hidden';
				var hiddenElement = document.getElementById( hiddenID );
				
				parentNode.removeChild( HTMLDiv );
				/*parentNode.removeChild( helpDiv );*/
				parentNode.removeChild( hiddenElement );
				
				var useragent = navigator.userAgent;

				if( useragent.toLowerCase().indexOf( 'msie' ) == -1 ){

					var textarea = document.createElement( 'textarea' );
	
					textarea.setAttribute( 'id', elementID ); 
					textarea.setAttribute( 'name', elementID );
					textarea.setAttribute( 'class', 'WYSIWYGEditor' );
					
					textarea.value = html;
					textarea.style.width = '<?php echo $width ? intval( $width ) . "px" : "100%"; ?>';
					textarea.style.height = '<?php echo $height ? intval( $height ) . "px" : "250px"; ?>';
					
					parentNode.style.width = '<?php echo $width ? intval( $width ) . "px" : "100%"; ?>';
					parentNode.style.height = '<?php echo $height ? intval( $height ) . "px" : "250px"; ?>';
					
					parentNode.appendChild( textarea );

					initWYSIWGEditors();
					
					textarea.setAttribute( 'class', '' );
					
				}
				else{
				
					parentNode.style.width = '<?php echo $width ? intval( $width ) . "px" : "100%"; ?>';
					parentNode.style.height = '<?php echo $height ? intval( $height ) . "px" : "250px"; ?>';
					
					var innerHTML = '<textarea id="' + elementID + '" name="' + elementID + '" class="WYSIWYGEditor" style="height:<?php echo $height ? intval( $height ) . "px" : "250px"; ?>; width:<?php echo $width ? intval( $width ) . "px" : "100%"; ?>;">' + html + '</textarea>';
					
					parentNode.innerHTML = innerHTML;

					initWYSIWGEditors();
		
					document.getElementById( elementID ).className = '';
					
				}

			}

		// -->
		</script>
		<?php
	}
	
	//----------------------------------------------------------------------
	
	/**
	 * Créer un éditeur wysiwyg
	 * @static
	 * @param string $id l'identifiant HTML de l'éditeur à créer
	 * @param string $html le contenu HTML éditable
	 * @param string $onReadyStateCallback
	 * @return void
	 */
	public static function createEditor( $id, $html = "", $attributes = "", $onReadyStateCallback = false ){
	
		global $GLOBAL_START_URL;
		
		if( !empty( $attributes ) )
			$attributes = " " . $attributes;
		?>
		<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
		<script type="text/javascript">
		/* <![CDATA[ */
		
			tinyMCE.init({
				mode : "exact",
				theme : "advanced",
				language : 'fr',
				plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,spellchecker,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
				theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,spellchecker,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
				theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
				theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				plugin_insertdate_dateFormat : "%d-%m-%Y",
				plugin_insertdate_timeFormat : "%H:%M:%S",
				extended_valid_elements : "style[*],a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
				template_external_list_url : "example_template_list.js",
				elements : "<?php echo htmlentities( $id ) ?>,abshosturls",
				relative_urls : false,
				remove_script_host : false,
				spellchecker_languages : "+French=fr,English=en,German=de,Italian=it,Spanish=es",
				spellchecker_rpc_url : '<?php echo $GLOBAL_START_URL ?>/js/tinymce/tiny_mce/jscripts/tiny_mce/plugins/spellchecker/rpc.php'<?php
				
					if( $onReadyStateCallback ){
						
						?>,
						setup : function( ed ){
					      ed.onInit.add(function(ed){
					          <?php echo $onReadyStateCallback; ?>();
					      });
   						}
   						<?php
					}
				
				?>
			});
		
		/* ]]> */
		</script>
		<textarea id="<?php echo htmlentities( $id ) ?>" name="<?php echo htmlentities( $id ) ?>" class="WYSIWYGEditor"<?php echo $attributes ?>><?php echo $html ?></textarea>
		<?php
		
	}
	
	//----------------------------------------------------------------------
	
	/**
	 * Créer un éditeur wysiwyg
	 * @static
	 * @param string $id l'identifiant HTML de l'éditeur à créer
	 * @param string $html le contenu HTML éditable
	 * @param string $onReadyStateCallback
	 * @return void
	 */
	public static function createSimpleEditor( $id, $html = "", $attributes = "", $onReadyStateCallback = false ){
	
		global $GLOBAL_START_URL;
		
		if( !empty( $attributes ) )
			$attributes = " " . $attributes;
		?>
		<script language="javascript" type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
		<script language="javascript" type="text/javascript">
		/* <![CDATA[ */
		
			tinyMCE.init({
				mode : "exact",
				theme : "advanced",
				language : 'fr',
				plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,spellchecker,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,bullist,numlist",
				theme_advanced_buttons2 : "",
				theme_advanced_buttons3 : "",
				theme_advanced_buttons4 : "",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				plugin_insertdate_dateFormat : "%d-%m-%Y",
				plugin_insertdate_timeFormat : "%H:%M:%S",
				extended_valid_elements : "style[*],a[name|href|target|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name],hr[class|width|size|noshade],font[face|size|color|style],span[class|align|style]",
				template_external_list_url : "example_template_list.js",
				elements : "<?php echo htmlentities( $id ) ?>,abshosturls",
				relative_urls : false,
				remove_script_host : false,
				spellchecker_languages : "+French=fr,English=en,German=de,Italian=it,Spanish=es",
				spellchecker_rpc_url : '<?php echo $GLOBAL_START_URL ?>/js/tinymce/tiny_mce/jscripts/tiny_mce/plugins/spellchecker/rpc.php'<?php
						
					if( $onReadyStateCallback ){
						
						?>,
						setup : function( ed ){
					      ed.onInit.add(function(ed){
					          <?php echo $onReadyStateCallback; ?>();
					      });
   						}
   						<?php
					}
				
				?>
			});
		
		/* ]]> */
		</script>
		<textarea id="<?php echo htmlentities( $id ) ?>" name="<?php echo htmlentities( $id ) ?>" class="WYSIWYGEditor"<?php echo $attributes ?>><?php echo $html ?></textarea>
		<?php
		
	}
	
	//----------------------------------------------------------------------
	
}

?>