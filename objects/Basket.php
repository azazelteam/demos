<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object panier
 */
include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ ) . "/BasketItem.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Sale.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );


class Basket implements Sale{
	
	//----------------------------------------------------------------------------
	
	private static $instance = NULL;
	
	//----------------------------------------------------------------------------
	//data members
    /**
     * @var ArrayList<BasketItem> $items
     * @access private
     */
    private $items;
    /**
	 * @var TradePriceStrategy $priceStrategy
     * @access private
	 */
	private $priceStrategy;
	/**
	 * @var ChargesStrategy $chargesStrategy
     * @access private
	 */
	private $chargesStrategy;
    /**
     * @var Salesman $salesman commercial
     * @access private
     */
    private $salesman;
    /**
     * @var Voucher $voucher bon de réduction
     * @access private
     */
    private $voucher;
    /**
     * @var float $discountRate % remise commerciale
     * @access private
     */
    private $discountRate;

    /**
    **
    */
    protected $product_gift = [];
    protected $article_autom = [];
    protected $product_basket;

    /**
    *
    *
    **/
    protected $item_gift;
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return Basket
	 */
	public static function &getInstance(){
		
		if( self::$instance == NULL ){
			
			self::$instance = new Basket();
			self::$instance->setDiscounts();
			
		}
		
		return self::$instance;
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Contructeur
	 * @access private
	 */
	private function __construct(){
		
		$this->items 		= new ArrayList( "BasketItem" );
		$this->item_gift = new ArrayList( "BasketItem" );
		$this->product_basket = new ArrayList( "BasketItem" );
		$this->voucher 		= NULL;

		$this->discountRate = 0.0;
	
		$this->restore();

		if( Session::getInstance()->getCustomer() )
				$this->salesman = new Salesman( Session::getInstance()->getCustomer()->get( "iduser" ) );
		else 	$this->salesman = new Salesman( DBUtil::getParameter( "contact_commercial" ) );
		
		$this->setPriceStrategy();

		$this->setChargesStrategy();
		//sauvegarde
		
		$this->save();
//
		$rs =& DBUtil::query( "SELECT * FROM product_gift") ;

		while ($row =& $rs->fetchRow()) {
			if($row["selection"] == 3)
		    	array_push($this->product_gift, $row);
		    else{

		    	$it = DBUtil::query("SELECT idarticle, idproduct FROM `detail` WHERE `reference` = '".$row['reference']."'");
				/*while (!$it->EOF())
				{

					$idarticle = $it->fields['idarticle'];
					if(Basket::getInstance()->getItems()->get( "idarticle" ) == $idarticle) continue;

					$article = new CArticle($idarticle);
					
					$this->addArticle(new CArticle($idarticle));
					$it->MoveNext();		
				}*/
		    	array_push($this->article_autom, $row);
		    }
		}



	}

	public function resetArticleGift(){
		$this->item_gift = new ArrayList( "BasketItem" );
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return TradePriceStrategy
	 */
	public function getPriceStrategy(){ return $this->priceStrategy; }
		public function getIdproduct(){ return $this->idproduct; }
	public function getReference(){ return $this->reference; }
	/* ----------------------------------------------------------------------------------------------------- */

	/******************************************/
	public function getProductGift(){
		return $this->product_gift;
	}
	/******************************************/

	/******************************************/
	public function getProductAutom(){
		return $this->article_autom;
	}
	/******************************************/
	/******************************************/
	public function removeArticle0Quantity(){
		$it = $this->items->iterator();
		$b = array();
		//$_SESSION["RM_BASKET"] = isset($_SESSION["RM_BASKET"])?$_SESSION["RM_BASKET"]:[];
		while( $it->hasNext() ){
		
			$item =& $it->next();
			
			if($item->getQuantity() < 1){
				$this->removeItemAt($item->getIndex());
			}
		}
		//var_dump(count($this->items->iterator()));
		//$_SESSION["RM_BASKET"] = $b;
		//var_dump($b);
		//die;
	}
	/******************************************/
	/**
	 * @access private
	 * @return void
	 */
	private function setPriceStrategy(){
		
		$rs =& DBUtil::query( 
		
			"SELECT strategy 
			FROM trade_price_strategy 
			WHERE `datetime` <= NOW()
			ORDER BY datetime DESC
			LIMIT 1" 
		
		);
		
		if( !$rs->RecordCount() 
			|| !file_exists( dirname( __FILE__ ) . "/strategies/" . $rs->fields( "strategy" ) . ".php" ) ){

			trigger_error( "stratégie de calcul de prix inconnue", E_USER_ERROR );
			exit();
			
		}

		$strategy = $rs->fields( "strategy" );
		
		include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
		$this->priceStrategy = new $strategy( $this );
		
	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access private
	 * @return void
	 */
	private function setChargesStrategy(){
		
		$rs =& DBUtil::query( 
		
			"SELECT strategy 
			FROM charge_strategy 
			WHERE `datetime` <= NOW()
			ORDER BY datetime DESC
			LIMIT 1" 
		
		);
		
		if( !$rs->RecordCount() 
			|| !file_exists( dirname( __FILE__ ) . "/strategies/" . $rs->fields( "strategy" ) . ".php" ) ){

			trigger_error( "stratégie de calcul de frais de port inconnue", E_USER_ERROR );
			exit();
			
		}

		$strategy = $rs->fields( "strategy" );
		
		include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
		$this->chargesStrategy = new $strategy( $this );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * application de diverses remises automatiques
	 * @access private
	 * @return void
	 */
	private function setDiscounts(){
		
		$this->discountRate = 0.0;
		
		if( !Session::getInstance()->getCustomer() )
			return;
			
		$this->setCustomerDiscount();
		$this->setWelcomeOffer();

	}
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getTotalET(){ return $this->priceStrategy->getTotalET(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getTotalATI(){ return $this->priceStrategy->getTotalATI(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getNetBill(){ return $this->priceStrategy->getNetBill(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getChargesET(){ return $this->chargesStrategy->getChargesET(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getChargesATI(){ return $this->chargesStrategy->getChargesATI(); }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getDiscountRate(){ return $this->discountRate; }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getBillingRate(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getRebateAmount(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getDownPaymentAmount(){ return 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return float
	 */
	public function getCreditAmount(){ 0.0; }
	
	//----------------------------------------------------------------------------
	/**
	 * @return Customer ou NULL
	 */
	public function &getCustomer(){ return Session::getInstance()->getCustomer(); }

	//----------------------------------------------------------------------------
	/**
	 * @return Salesman
	 */
	public function &getSalesman(){ return $this->salesman; }
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public function getItemCount() { return $this->items->size(); }

	/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public function getItemsgCount() { 
		return $this->item_gift->size(); }

		/**
	 * Retourne le nombre d'articles dans le panier
	 * @return int le nombre d'articles dans le panier
	 */
	public function getItemsbCount() { 

		return $this->product_basket->size(); }

	//----------------------------------------------------------------------------
	/**
	 * Retourne le $index-ième élément du panier
	 * @return BasketItem
	 */
	public function &getItemAt( $index ) { return $this->items->get( $index ); }

	public function &getItemFromIdArticle( $idArticle ) { 
		$basketItem = &$this->items->toArray();
		$neededObject = array_filter(
			    $basketItem,
			    function ($e) use (&$idArticle) {

			        return $e->getArticle()->getId() == $idArticle;
			    }
			);
		return $this->getItemAt(key($neededObject));
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return ArrayList
	 */
	public function &getItems(){ return $this->items; }

	public function &getItemsGift(){ return $this->item_gift; }

	//-----------------------------------------------------------------------------------------------
	/**
	 * Supprime un article donné du panier
	 * @param int $index Le $index-ième article à supprimer
	 * @return void
	 */
	public function removeItemAt( $index ){

		
		
		
		
		//-****************************************************
			//public function Getreduction( $voucher ){
		
	if(Basket::getInstance()->getItemAt( $index) == null) return ;
	
	$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $_SESSION[ "voucher_code" ] ) )." ";
	$ress = DBUtil::query($reqs);
$idcategory = $ress->fields("idcategory");
	
	$idproduct = $ress->fields("idproduct");

	$type = $ress->fields("type");
		
	$reference = $ress->fields("reference");
		
	
	$reqs = "SELECT reference,sellingcost FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$sellingcost = $ress->fields("sellingcost");
	
	$exist = 0; $exists = 0;
	$order_amount = 0.0;
	
	//for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
	
	$idprod =  Basket::getInstance()->getItemAt( $index)->getArticle()->get( "idproduct" );
	
	$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
	$rs = DBUtil::query($rq);
	$references = $rs->fields("reference");
	$dates = date("Y-m-d");
	
	$q = "SELECT reference FROM promo WHERE reference = '".$references."'  ";
	$rs = DBUtil::query($q);
	$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	$tab1=array();
	$idcatparent =$idcategorys;
	while($idcatparent!=0){
	array_push($tab1,$idcatparent);
	$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
	$rs2 = DBUtil::query($q2);
	$idcatparent =$rs2->fields("idcategoryparent") ;
	}
	if ( !$rs->RecordCount() ) {
	
	
		$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	
	
	$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
	$rs = DBUtil::query($q);

	$idcategoryparent = $rs->fields("idcategoryparent");
	
	/*if( B2B_STRATEGY )
			$amountht = Basket::getInstance()->getItemAt( $index)->getTotalET();
		else 	 
		$amountttc = Basket::getInstance()->getItemAt( $index)->getTotalATI(); */
if( $idproduct > 0  ){
	

	if($idproduct ==  $idprod)
	{
	if( isset( $_SESSION[ "voucher" ] ) )
		unset( $_SESSION[ "voucher" ] );
	
	
		
	}
	
	}	
 }
	else  { 
	
		if( $idcategory > 0 ){
		
		//if( $idcategory ==  $idcategoryparent ){
			if(in_array($idcategory,$tab1)){
		if( isset( $_SESSION[ "voucher" ] ) )
		unset( $_SESSION[ "voucher" ] );

	 
	 }
	 
	 }
		
		}
	
		//******************************************************
		
		
		$this->items->remove( $this->items->get( $index ) );
		$this->save();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Suppression de tous les articles du panier
	 * @access public
	 * @return void
	 */
	public function removeItems(){

		$this->items = new ArrayList( "BasketItem" );
if( isset( $_SESSION[ "voucher" ] ) )
		unset( $_SESSION[ "voucher" ] );
		$this->save();
		
	}
	public function removeBon(){

	if( isset( $_SESSION[ "voucher" ] ) )
		unset( $_SESSION[ "voucher" ] );

		$this->save();
		
	}
	//----------------------------------------------------------------------------
	/**
	 * Ajoute un article au panier
	 * @param CArticle $article l'identifiant de l'article à ajouter
	 * @param int $quantity la quantité souhaitée, ( optionnel, 1 par défaut )
	 * @return void
	 */
	public function addArticle( CArticle $article, $quantity = 1 ){

	    //if($quantity > 0) 
	    {
            //$this->items->add(new BasketItem($article, max(1, max($article->get("min_cde"), intval($quantity)))));
            $this->items->add(new BasketItem($article, ($quantity)));
            $this->save();
        }
	}

	/**
	 * Ajoute un article cadeau
	 */
	public function addArticleGift( CArticle $article, $quantity = 1 ){
	    //if($quantity > 0) 
	    {
            $this->item_gift->add(new BasketItem($article, max(1, max($article->get("min_cde"), intval($quantity)))));
            $this->save();
        }
	}

	/**
	 * @return void
	 */
	public function addArticleBasket( CArticle $article, $quantity = 1 ){
	    //if($quantity > 0) 
	    {
            $this->product_basket->add(new BasketItem($article, max(1, max($article->get("min_cde"), intval($quantity)))));
            $this->save();
        }
	}


public function addVoucher_code( $voucher ){
	
		$this->voucher = $voucher;
 		
		$this->save();
 		
	}
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return void
	 */
	public function save(){
		
		//suppression des anciennes sauvegardes

		if( isset( $_SESSION[ "basket_items" ] ) )
			unset( $_SESSION[ "basket_items" ] );
//if( isset( $_SESSION[ "voucher" ] ) )
		//	unset( $_SESSION[ "voucher" ] );
		//sauvegarde du panier
	
	//	$_SESSION[ "voucher" ] =  $voucher;
	
		//sauvegarde du panier
	
		$_SESSION[ "basket_items" ] = array();
		
		$it = $this->items->iterator();
		

		while( $it->hasNext() ){
			
			$item =& $it->next();

			$_SESSION[ "basket_items" ][] = array(
			
				"idarticle" => $item->getArticle()->getId(),
				"quantity" 	=> $item->getQuantity(),
				"idcolor" 	=> $item->getColor() ? $item->getColor()->getId() : 0,
				"idtissus" 	=> $item->getCloth() ? $item->getCloth()->getId() : 0
				
			);
			
		}
		
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Restaure l'état du panier depuis la session
	 */
	private function restore(){
	
		if( !isset( $_SESSION[ "basket_items" ] ) )
			return;
			
		$this->items = $this->item_gift = new ArrayList( "BasketItem" );
		$this->product_basket = new ArrayList( "BasketItem" );
		
		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
	
		$items = $_SESSION[ "basket_items" ];
        if(array_key_exists("voucher",$_SESSION))
		  $voucher = $_SESSION[ "voucher" ];
		$i = 0;

		while( $i < count( $items ) ){
			if( intval( $items[ $i ][ "idarticle" ] )
				&& DBUtil::getDBValue( "idarticle", "detail", "idarticle", $items[ $i ][ "idarticle" ] ) )
			$this->addArticle( new CArticle(
			
					intval( $items[ $i ][ "idarticle" ] ),
					intval( $items[ $i ][ "idcolor" ] ),
					intval( $items[ $i ][ "idtissus" ] )
					
				), intval( $items[ $i ][ "quantity" ] )
				
			);
			
			$i++;
			
		}
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Suppression du panier dans la session
	 */
	public function destroy(){

		if( isset( $_SESSION[ "basket_items" ] ) )	
			unset( $_SESSION[ "basket_items" ] );
if( isset( $_SESSION[ "voucher" ] ) )
		unset( $_SESSION[ "voucher" ] );
		$this->items 	= $this->item_gift = new ArrayList( "BasketItem" );
		$this->voucher  = NULL;
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le taux de TVA à appliquer
	 * En mode B2C, la TVA est forcée, elle sera récupérée par le client s'il est extracom
	 * @return float
	 */
	public function getVATRate(){
		
		static $defaultVATRate = NULL;
		
		if( $defaultVATRate === NULL )
			$defaultVATRate = DBUtil::getParameter( "vatdefault" );
			
		if( $this->applyVAT() )
			return $defaultVATRate;
		
		return 0.0;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Trade#applyVAT()
	 */
	public function applyVAT(){

		if( !B2B_STRATEGY )
			return true;

		if( !Session::getInstance()->getCustomer() )
				$idstate = DBUtil::getParameter( "statedefault" );
		else 	$idstate = Session::getInstance()->getCustomer()->get( "idstate" );
		
		$bvat = DBUtil::getDBValue( "bvat", "state", "idstate", $idstate );

		return $bvat ? true : false;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Retourne le poids total
	 * @return float
	 */
	public function getWeight(){ 
		
		$weight = 0.0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			
			$weight += $item->getArticle()->get( "weight" ) * $item->getQuantity();
			
		}
		
		return $weight; 
		
	}


/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

 * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction * 
 
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */



	//----------------------------------------------------------------------------
	
	public function &getVoucher(){ return $this->voucher; }
	
	//----------------------------------------------------------------------------
	/**
	 * Utiliser un bon de réduction
	 * @access public
	 * @return bool
	 */
/*	 public function Voucher_amount_min( Voucher $voucher ){
	 
	 	$req = "SELECT amount_min FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) )." ";
	$res = DBUtil::query($req);

	$amount_min = $res->fields("amount_min");
	$basketController->setData( "amount_min"  ,$amount_min );
	
	if( $amount_min > 0 ){
			$order_amount = 0.0;
			$it = $this->items->iterator();
			
		while( $it->hasNext() ){
				
				if( B2B_STRATEGY )
						$order_amount += $it->next()->getTotalET();
				else 	$order_amount += $it->next()->getTotalATI();
				
			}
	
	if( $amount_min < $order_amount )
			return true;
			else return false;
			
		
		}
	 
	 }
	 */
/*	 function verifypromo ( $reference ){
	$q = "SELECT reference FROM promo WHERE reference = '".$reference."' ";
	$rs = DBUtil::query($q);
	if ( $rs->RecordCount() > 0 ) 
	return true;
	else
	return false;
	}*/
	public function setUseVoucher(  $voucher )
	{
		//--------- vérifier si le bon de réduction exist --------------//
		if( !DBUtil::query( "SELECT 1 FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $voucher ) ) . " LIMIT 1" )->RecordCount() )
			return false;
		
		//-------------- vérifier les dates début et de fin -------------//
		$dates = date("Y-m-d");
		
		if( !DBUtil::query( 'SELECT 1 FROM discount_voucher WHERE  date_start < "'.$dates.'" AND date_end > "'.$dates.'" AND code = ' . DBUtil::quote( stripslashes(  $voucher  ) ) . " LIMIT 1" )->RecordCount() )
			return false;
		// else 
		// 	var_dump('voucherdate OK');
		
		//-------------------------vérifier si le bon de réduction n'est pas utiliser par le mme client -------------//
		
		if( isset( $_SESSION[ "idbuyer" ]))
			$idbuyer = urldecode($_SESSION[ "idbuyer" ]);
		
		$req = "SELECT voucher_code FROM `order` WHERE voucher_code = ".DBUtil::quote( stripslashes( $voucher ) )." AND idbuyer = " . DBUtil::quote( stripslashes( $idbuyer ) )." ";
		$res = DBUtil::query($req);

		//	$voucher_code = $res->fields("voucher_code");
		//$basketController->setData( "idbuyer"  ,$idbuyer );
		
		if ( $res->RecordCount() > 0 ) {
			return false;
		}
		
		//---------------- vérifier si montant de la commande > amount_min --------------//

		  
		$req = "SELECT amount_min FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $voucher ) )." ";
		$res = DBUtil::query($req);

		$amount_min = $res->fields("amount_min");
		
		if( $amount_min > 0 ){
			$order_amount = 0.0;
			//$it = $this->items->iterator();
			$it = Basket::getInstance()->getItems()->iterator();
			while( $it->hasNext() ){
				
				if( B2B_STRATEGY )
						$order_amount += $it->next()->getTotalET();
				else 	$order_amount += $it->next()->getTotalATI();
				
			}

			if( $amount_min > $order_amount )
				return false;
		
		}
		//-----------------------------------------------vérifier cohérence avec produit et categories ---------------//
		$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes(  $voucher) )." ";
		$ress = DBUtil::query($reqs);
		$idcategory = $ress->fields("idcategory");
		
		$idproduct = $ress->fields("idproduct");

		$type = $ress->fields("type");
			
		$reference = $ress->fields("reference");
			
		
		$reqs = "SELECT reference,sellingcost FROM detail WHERE reference = '".$reference."' ";
		$ress = DBUtil::query($reqs);
		$sellingcost = $ress->fields("sellingcost");
		
		$exist = 0; $exists = 0;
		$order_amount = 0.0;
		
		for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
		
			$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
			
			$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
			$rs = DBUtil::query($rq);
			$references = $rs->fields("reference");
			$dates = date("Y-m-d");
			$date_promo=date("Y-m-d H:i:s");
			$q = "SELECT reference FROM promo WHERE reference = '".$references."' and  begin_date < '".$date_promo."' AND end_date > '".$date_promo."'  ";
			$rs = DBUtil::query($q);
			$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
			$results = DBUtil::query($qqs);

			$idcategorys = $results->fields("idcategory");
			$tab1=array();
			$idcatparent =$idcategorys;
			while($idcatparent!=0){
				array_push($tab1,$idcatparent);
				$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
				$rs2 = DBUtil::query($q2);
				$idcatparent =$rs2->fields("idcategoryparent") ;
			}
			if ( $rs->RecordCount() ) {
			 	return false;
			} else {
			
				$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
				$results = DBUtil::query($qqs);

				$idcategorys = $results->fields("idcategory");
				
				
				$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
				$rs = DBUtil::query($q);

				$idcategoryparent = $rs->fields("idcategoryparent");
		
		
				if( $idproduct > 0  ){
					

					if($idproduct ==  $idprod)
					{
					return true;
					
					}	
			 	}
				else  { 
				
					if( $idcategory > 0 ){
					
					//if( $idcategory ==  $idcategoryparent ){
						if(in_array($idcategory,$tab1)){
							return true;

				 
				 		}
				 
			 		}
					
				}
		
			}	
		}
		return true;	
	}
	
	public function Getreduction( $voucher ){
		$reduction = 0.0;
	
		$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) )." ";
		$ress = DBUtil::query($reqs);
		$idcategory = $ress->fields("idcategory");
	
		$idproduct = $ress->fields("idproduct");

		$type = $ress->fields("type");
			
		$reference = $ress->fields("reference");
			
		$reqs = "SELECT reference,sellingcost FROM detail WHERE reference = '".$reference."' ";
		$ress = DBUtil::query($reqs);
		$sellingcost = $ress->fields("sellingcost");
		
		$exist = 0; $exists = 0;
		$order_amount = 0.0;
		
		for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
				
			$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
			
			$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
			$rs = DBUtil::query($rq);
			$references = $rs->fields("reference");
			$dates = date("Y-m-d");
			$date_promo=date("Y-m-d H:i:s");
			$q = "SELECT reference FROM promo WHERE reference = '".$references."' and  begin_date < '".$date_promo."' AND end_date > '".$date_promo."' ";
			$rs = DBUtil::query($q);
			$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
			$results = DBUtil::query($qqs);

			$idcategorys = $results->fields("idcategory");
			$tab1=array();
			$idcatparent =$idcategorys;
			while($idcatparent!=0){
				array_push($tab1,$idcatparent);
				$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
				$rs2 = DBUtil::query($q2);
				$idcatparent =$rs2->fields("idcategoryparent") ;
			}
			if ( !$rs->RecordCount() ) {
				$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
				$results = DBUtil::query($qqs);

				$idcategorys = $results->fields("idcategory");
				
				$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
				$rs = DBUtil::query($q);

				$idcategoryparent = $rs->fields("idcategoryparent");
			
				if( B2B_STRATEGY )
					$amountht = Basket::getInstance()->getItemAt( $i)->getTotalET();
				else 	 
					$amountttc = Basket::getInstance()->getItemAt( $i)->getTotalATI(); 

				if( $idproduct > 0  ){
					if($idproduct ==  $idprod)
					{
						if($type == "rate"){
							if( B2B_STRATEGY )
								$order_amount = $amountht;
							else	
								$order_amount =  $amountttc;
						
							$reduction = (($order_amount * $sellingcost)/100);
						}
						if($type == "amount"){
							 $reduction =  $sellingcost;
						}
					}	
				}
				else  { 
				
					if( $idcategory > 0 ){
					
						//if( $idcategory ==  $idcategoryparent ){
						if(in_array($idcategory,$tab1)){
							if($type == "rate"){
								if( B2B_STRATEGY )
									$order_amount += $amountht;
								else	
									$order_amount +=  $amountttc;

							 	$reduction = (($order_amount * $sellingcost)/100);
							}
							if($type == "amount"){
								$reduction +=  $sellingcost;
								
							}
			 			}
					}
					else // modif
					{
						if($type == "rate"){
							if( B2B_STRATEGY )
								$order_amount += $amountht;
							else	
				 				$order_amount +=  $amountttc;
			 				$reduction = (($order_amount * $sellingcost)/100);
			
						}
						if($type == "amount"){
							if( B2B_STRATEGY )
								$reduction +=  $sellingcost ;		
							else	
							$reduction +=  $sellingcost / 1.196;
			
			
						}
					}// fin modif
				}
			}	
		}
		return $reduction;
	}
		/* stratégie désactivée */
		
	/*	if( !DBUtil::getDBValue( "enabled", "product_price_strategy", "strategy", "VoucherStrategy" ) )
			return false;
			
		/* bon de réduction valide */
		
	/*	if( !Voucher::isAvailable( $voucher->get( "code" ) ) )
			return false;
		
		/* montant minimum de commande atteint */
		
	/*	if( $voucher->get( "min_order_amount" ) > 0.0 ){
	
			$order_amount = 0.0;
			$it = $this->items->iterator();
			
			while( $it->hasNext() ){
				
				if( B2B_STRATEGY )
						$order_amount += $it->next()->getTotalET();
				else 	$order_amount += $it->next()->getTotalATI();
				
			}
	
			if( $voucher->get( "min_order_amount" ) > $order_amount )
				return false;
		
		}
		
		/* bon de réduction valide */
		
		/*$this->voucher = $voucher;

		include_once( dirname( __FILE__ ) . "/catalog/strategies/VoucherStrategy.php" );
		
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$it = $this->items->iterator();
		
		while( $it->hasNext() ){

			$item =& $it->next();
			$item->getArticle()->addPriceStrategy( new VoucherStrategy( $item->getArticle(), $idbuyer, $item->getQuantity() ) );
		
		}

		return true;
		
	}

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

 * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction * 
 
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

	//----------------------------------------------------------------------------
	/**
	 * remise automatique par client et par tranches de prix
	 * @access private
	 * @return void
	 */
	private function setCustomerDiscount(){

		if( !Session::getInstance()->getCustomer() )
			return;
			
		$total = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$total += B2B_STRATEGY ? $it->next()->getTotalET() : $it->next()->getTotalATI();
		
		$rs = DBUtil::query ( 
		
			"SELECT MAX( orate.rate ) AS rate 
			FROM buyer b, order_rate orate 
			WHERE b.idbuyer = '" . Session::getInstance()->getCustomer()->getId() . "' 
			AND  b.idrate_family = orate.idrate_family 
			AND '$total' >= orate.min_price"
		
		);
		
		if( !$rs->RecordCount() )
			return;
			
		$this->discountRate = max( $this->discountRate, floatval( $rs->fields( "rate" ) ) );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * remise automatique de bienvenue
	 * @access private
	 * @return void
	 */
	private function setWelcomeOffer(){
		
		if( !Session::getInstance()->getCustomer() )
			return;
		
		$discountValue = DBUtil::getParameterAdmin( "first_discount_value" );
		
		if( round( floatval( $discountValue ), 2 ) == 0.0 )
			return;
			
		if( DBUtil::query( "SELECT COUNT(*) AS `count` FROM `order` WHERE idbuyer = '" .Session::getInstance()->getCustomer()->getId() . "' AND `status` NOT LIKE 'Cancelled'" )->fields( "count" ) > 0 )
			return;
	
		switch( DBUtil::getParameterAdmin( "first_discount_type" ) ){
		
			case "amount" : 
				
				$total = 0.0;
				$it = $this->items->iterator();
				while( $it->hasNext() )
					$total += B2B_STRATEGY ? $it->next()->getTotalET() : $it->next()->getTotalATI();
			
				$this->discountRate = max( $this->discountRate, round( $discountValue * 100.0 / $total, 2 ) ); 
				break;	
				
			case "rate" : 	
				
				$this->discountRate = max( $this->discountRate, $discountValue ); 
				break;
				
			default : return;
			
		}

	}
	
	//----------------------------------------------------------------------------
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float
	 * @deprecated transitoire
	 */
	public function getDiscountAmount()		{ 
		
		$total = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
			$total += B2B_STRATEGY ? $it->next()->getTotalET() : $it->next()->getTotalATI();
	
		return round( $total * $this->discountRate / 100.0, 2 ); 
	
	}

	/**
	*/

	public function InitDiscountRate(){
		$it = $this->items->toArray();
		$arrayBuff = array();

		$i = 0;

		for(;$i<$this->items->size();$i++){
			$article = $it[$i]->getArticle();
			$idCategory = $article->GetIdCategory();
			$arrayBuff[$idCategory] = isset($arrayBuff[$idCategory])? $arrayBuff[$idCategory] + $it[$i]->getQuantity() : $it[$i]->getQuantity();
		}
		$_SESSION["VOLUME_DISCOUNT_RATE"] = json_encode($arrayBuff);
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Fonctions pour les tables cart et cart-detail
	 */
	public function checkSessionId($session_id, $id_buyer)
	{
		$rs = DBUtil::query("SELECT id_cart, status FROM cart WHERE `id_session`='$session_id' AND `id_buyer`=$id_buyer");
		if (!$rs)
			return false;
		if (intval($rs->fields("status")) == 1)
		{
			$delete = "DELETE FROM cart_detail WHERE `id_cart` =".$rs->fields("id_cart");
			DBUtil::getConnection()->Execute($delete);
			$query = "UPDATE cart SET `status`=0, `date_add`=".time()." WHERE `id_cart`=".$rs->fields("id_cart");
			DBUtil::getConnection()->Execute($query);
		}
		return $rs->fields("id_cart");
	}
	
	public function updateCart($id_cart)
	{
		$it = $this->items->iterator();
		while( $it->hasNext() )
		{
			$item =& $it->next();
			$rs = DBUtil::query("SELECT id_article, quantity FROM cart_detail WHERE `id_cart`=$id_cart AND `id_article`=".$item->getArticle()->getId());
			$quantity = (int)$rs->fields("quantity");
			if ($rs->fields)
			{
				if ($quantity != $item->getQuantity())
				{
					$update = "UPDATE cart_detail SET `quantity`=".$item->getQuantity()." WHERE `id_cart`=$id_cart AND `id_article`=".$item->getArticle()->getId();
					DBUtil::getConnection()->Execute($update);
				}
			}	
			else
			{
				DBUtil::query("INSERT INTO cart_detail (id_cart, id_product, id_article, quantity) VALUES ($id_cart, ".$item->getArticle()->get( "idproduct" ).", ".$item->getArticle()->getId().", ".$item->getQuantity().")");
			}
		}
	}
	
	public function addCart($session_id, $id_buyer)
	{
		DBUtil::query("INSERT INTO cart (id_buyer, id_session, date_add) VALUES ('$id_buyer', '$session_id', ".time().")");
		$cart_id = DBUtil::getInsertID();
		$it = $this->items->iterator();
		while( $it->hasNext() )
		{
			$item =& $it->next();
			DBUtil::query("INSERT INTO cart_detail (id_cart, id_product, id_article, quantity) VALUES ($cart_id, ".$item->getArticle()->get( "idproduct" ).", ".$item->getArticle()->getId().", ".$item->getQuantity().")");
		}
		return $cart_id;
	}
	public function changeCartStatus($cart_id)
	{
		$query = "UPDATE cart SET `status`=1 WHERE `id_cart`=$cart_id";
		DBUtil::getConnection()->Execute($query);
	}
	public function insertOrderID($id_cart, $orderid)
	{
		$query = "UPDATE cart SET `orderid`='$orderid' WHERE `id_cart`=$id_cart";
		DBUtil::getConnection()->Execute($query);
	}
	//----------------------------------------------------------------------------

}

?>