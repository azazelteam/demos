<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object produits
 */

include_once( dirname( __FILE__ ) . "/tableobject.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CATALBASE extends TABLEDESC
{
	protected $id;
	protected $buyer;
	protected $curart;
	protected $lang;
	protected $LimiteMin; 
	protected $LimiteMax;
	protected $LimiteMinP; 
	protected $LimiteMaxP;
	protected $products ;
	protected $NBProduct;
	protected $CurrProduct;
	protected $prodnotloaded;
	protected $productinfos;
	protected $NbByPages;
	protected $DB;
	
	/**
	 * Contructeur
	 */
	function __construct()
		{
		
		$this->id = 0;
		$this->buyer = 0 ;
		$this->curart = 0;
		$this->lang ='_1';
		$this->LimiteMin = 0; 
		$this->LimiteMax = 1;
		$this->LimiteMinP = 0; 
		$this->LimiteMaxP = 1;
		$this->NBProduct = 0;
		$this->CurrProduct = 0 ;
		$this->prodnotloaded = 1;
		$this->productinfos = array();
		$this->NbByPages = 12;
		$this->DB;
		echo "do not use directly";
		}
	
	/**
	 * Retourne le nombre des produits de la catégorie
	 * @return int le nombre de produits
	 */
	public function GetNbProductsInCategory() { return $this->NBProduct; }
	
	/**
	 * Retourne le nombre des produits
	 * @return int le nombre de produits
	 */
	public function GetNBProduct() { return $this->NBProduct; }
	
	/**
	 * Retourne le nombre des produits par page
	 * @return int le nombre de produits
	 */
	public function GetNbByPages() { return $this->NbByPages; }
	
	/**
	 * Retourne l'id du produit
	 * @return int l'id du produit
	 */
	public function getid() { return $this->id; }
	
	/**
	 * Retourne l'id du produit courant
	 * @return int l'id du produit courant
	 */
	public function GetProdductId() { $i = $this->CurrProduct; if( isset($this->products[$i]['idproduct']) ) return  $this->products[$i]['idproduct']; }
	
	/**
	 * Retourne le nom du produit courant
	 * @return string le nom du produit
	 */
	public function GetName() { $i = $this->CurrProduct; if( isset($this->products[$i]['name']) ) return  $this->products[$i]['name']; }
	
	/**
	 * Retourne l'iône du produit courant
	 * @deprecated
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetIcon($origine = '../www') { 
		$result='';
		}
		
	/**
	 * Retourne la vignette du produit courant
	 * @deprecated
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetProd($origine = '../www') { 
		$result='';
		}
	/**
	 * Retourne l'objet produit d'un des produits similaires
	 * @param int $numprod L'index du produit
	 * @param bool $light affichage ou non des familles
	 * @return array le tableau des infos du produit
	 */
	public function &GetProduct($numprod,$light=false) {  if( !isset($this->productinfos[$numprod]) ) $this->LoadProduct($numprod,$light); return $this->productinfos[$numprod]; }
	
	/**
	 * Place le premier produit comme produit courant
	 * @param int $CurrentPageNumber la page courante
	 * @return void
	 */
	public function FirstProduct($CurrentPageNumber=1) { 
		if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
	        $this->LimiteMinP = $this->NbByPages * ($CurrentPageNumber-1);
	        $this->LimiteMaxP = min($this->LimiteMinP + $this->NbByPages, $this->NBProduct);
		$this->CurrProduct = $this->LimiteMinP - 1 ;
	  }
	
	/**
	 * Place le produit suivant comme produit courant
	 * @return int l'id du produit courant
	 */
	public function NextProduct() {
		$this->CurrProduct++;
		if( $this->CurrProduct > $this->LimiteMaxP) return 0;
		else return $this->CurrProduct;
		}
	
	/**
	 * Vérifie s'il y a d'autres produits après le produit courant
	 * @return int 1 si il y a des produits, 0 sinon
	 */
	public function MoreProducts() { if( $this->CurrProduct < $this->LimiteMaxP-1) return 1; return 0; }
	
	/**
	 * Vérifie s'il y a d'autres pages après la page courante
	 * @return int 1 si il y a des pages, 0 sinon
	 */
	public function MoreProdPages() { if( $this->LimiteMaxP < $this->NBProduct ) return 1; return 0; }
	
	/**
	 * Calcule l'intervalle des produits à afficher dans la page
	 * @param int $NbByPages Le nombre de produits par page
	 * @param int $CurrentPageNumber La page
	 * @return int l'intervalle
	 */
	public function CalculateLimiteProdToDisplay( $NbByPages,$CurrentPageNumber)
	{	
		$this->NbByPages = $NbByPages ;
		if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
	        $this->LimiteMinP = $NbByPages * ($CurrentPageNumber-1);
	        $this->LimiteMaxP = min($this->LimiteMinP + $NbByPages, $this->NBProduct);
		return ($this->LimiteMinP) ;
	}
	
	/**
	 * Calcule l'index du premier produit à afficher 
	 * @param int $NbByPages Le nombre de produits par page
	 * @param int $CurrentPageNumber La page
	 * @return int l'index
	 */
	public function CalculateLimiteToDisplay($NbByPages, $CurrentPageNumber)
	{	
		if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
	        $this->LimiteMin = $NbByPages * ($CurrentPageNumber-1);
	        $this->LimiteMax = $NbByPages;
		return ($this->LimiteMin + $NbByPages) ;
	}
	
	/**
	 * Recherhce les produits
	 * @param string $Query Requête sql
	 * @return int Le nombre de produits
	 */
	public function SetProducts($Query)
	{
		$this->NBProduct = 0;
		$this->products = array();
		$Base = &DBUtil::getConnection();
		$rs =& DBUtil::query($Query);
		//trigger_error(showDebug($Query,'CATEGORY::SetCategory() => $SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($Query,'CATEGORY::SetCategory() => $SQL_select','log'),E_USER_ERROR);
		if( !$rs->EOF )
		{
			$this->NBProduct = $rs->RecordCount();
			while( !$rs->EOF )
			{
				$this->products[] = $rs->fields;
				$rs->MoveNext();
			}
		}
		return $this->NBProduct ;
	}	
	
	/**
	 * Charge les produits
	 * @param bool $light affichage ou non des familles de produits
	 * @return void
	 */
	public function LoadProducts($light=true)
	{
	 $this->productinfos = array();
	 
	 for($i=0; $i < $this->NBProduct ; $i++) {
	 	if( is_array($this->products[$i])) $idp= $this->products[$i]['idproduct'];
	 else $idp= $this->products[$i];
	 $this->productinfos[] = new PRODUCT($idp,0,$light);
	
	 }
	 $this->prodnotloaded = 0;
	}

	/**
	 * Charge un produits
	 * @param int $i Index dans la table $this->products
	 * @param boolean $light chargement familles oui ou non(ture)
	 * @return void
	 */
	public function LoadProduct($i,$light=false)
	{
	 if( is_array($this->products[$i])) $idp= $this->products[$i]['idproduct'];
	 else $idp= $this->products[$i];
	 $this->productinfos[$i] = new PRODUCT($idp,0,$light);
	}
		
	/**
	 * debugage
	 */	
	function dump()
	{
	  echo "<br />Catalogue : $this->id <br />\n";
	  print_r($this->products);
	  echo "<br />---------------<br />\n";
	}

	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
}//EOC
?>