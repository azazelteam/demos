<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object calendrier
 */
 
abstract class DHTMLCalendar {

	//----------------------------------------------------------------------------
	//config
	
	/**
	 * @var string $INSTALLATION_FOLDER
	 * Répertoire où sont installées les librairies javascript
	 * @access private
	 * @static
	 */
	private static $INSTALLATION_FOLDER = "/js/jquery/jquery-ui-1.7.1.custom.min.js";
	
	/**
	 * @var string
	 * Répertoire où sont installées les librairies javascript
	 * @access private
	 * @static
	 */
	private static $LANGUAGE_FILE = "ui.datepicker-fr.js";
	
	/**
	 * @var string $DATE_FORMAT
	 * Format utilisé pour la date
	 * %d : jour, %m : mois, %Y : année
	 * @access private
	 * @static
	 */
	private static $DATE_FORMAT = "%d-%m-%Y";
	
	//------------------------------------------------------------------------------------

	/**
	 * Affiche un joli calendrier
	 * @static
	 * @param string $inputFieldID l'identifiant du champ où la date sélectionnée sera renseignée
	 * @param string $displayIcon vaut true si l'on souhaite afficher une icone clickable du calendrier. Sinon il faut cliquer sur le champ $inputFieldID. Optionnel, vaut true par défaut.
	 * @param string $calledFunction le nom de la fonction appelée lors de la fermeture du calendrier (sélection de date ou fermeture manuelle). La fonction javascript reçoit comme paramètre l'objet javascript calendar
	 * @return void
	 */
	public static function calendar( $inputFieldID, $displayIcon = true, $calledFunction = false, $defautlDate = false ){
		
		global $GLOBAL_START_URL;
			
		?>
		
		<script type="text/javascript">
		/* <![CDATA[ */
		$(document).ready(function() { 
		
			$('#<?php echo htmlentities( $inputFieldID ) ?>').datepicker({
				buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
				buttonImageOnly: true,
				changeMonth: true,
				changeYear: true,
				//dateFormat: 'yy-mm-dd',
				dateFormat: 'dd-mm-yy',
				showAnim: 'fadeIn',
				showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
				onClose: function(){
					<?php echo $calledFunction ?>( this );
				}
				<?php } ?>
			});
			
			if(document.getElementById('<?php echo htmlentities( $inputFieldID ) ?>').disabled)
				$('#<?php echo htmlentities( $inputFieldID ) ?>').datepicker('disable');
   	        <?php if( $defautlDate ){ ?>
               $('#<?php echo htmlentities( $inputFieldID ) ?>').datepicker("setDate", new Date());
			<?php } ?>	
		});
		/* ]]> */
		</script>
		<?php
		
	}
    
    public static function calendarTimepicker( $inputFieldID, $displayIcon = true, $calledFunction = false, $defautlDate = false ){
		
		global $GLOBAL_START_URL;
			
		?>
		
		<script type="text/javascript">
		/* <![CDATA[ */
		$(document).ready(function() { 
		  $('#<?php echo htmlentities( $inputFieldID ) ?>').datetimepicker({
				buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
				buttonImageOnly: true,
				changeMonth: true,
				changeYear: true,
				//timeFormat: 'HH:mm:ss',
				showAnim: 'fadeIn',
				showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
				onClose: function(){
					<?php echo $calledFunction ?>( this );
				}
				<?php } ?>
			});
			
			if(document.getElementById('<?php echo htmlentities( $inputFieldID ) ?>').disabled)
				$('#<?php echo htmlentities( $inputFieldID ) ?>').datetimepicker('disable');
   	        <?php if( $defautlDate ){ ?>
               $('#<?php echo htmlentities( $inputFieldID ) ?>').datetimepicker("setDate", new Date());
			<?php } ?>	
		});
		/* ]]> */
		</script>
		<?php
		
	}

	//----------------------------------------------------------------------------
	
}

?>