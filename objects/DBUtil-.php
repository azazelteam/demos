<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorb?
 * Object ...?
 */


define( "PERSISTENT_CONNECTION", false );

//include_once( dirname( __FILE__ ) . "/../config/init.php" );
/*
 * @static
 */
 
class DBUtil{
	
	//--------------------------------------------------------------------
	
	private static ¤con;
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne une r?f?rence vers une connection ADO
	 * @static
	 * @return ADOConnection
	 */
	public static function &getConnection(){
		
		include( dirname( __FILE__ ) . "/../config/config.php" );
		
		if( !isset( self::¤con ) ){
			
			self::¤con = NewADOConnection( "mysql", "pear" );
			
			if( PERSISTENT_CONNECTION )
					self::¤con->PConnect( ¤GLOBAL_DB_HOST, ¤GLOBAL_DB_USER, ¤GLOBAL_DB_PASS, ¤GLOBAL_DB_NAME );
			else 	self::¤con->Connect( ¤GLOBAL_DB_HOST, ¤GLOBAL_DB_USER, ¤GLOBAL_DB_PASS, ¤GLOBAL_DB_NAME );
			
		}
		return self::¤con;
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * R?cup?re la valeur d'une colonne donn?e dans une table donn?e pour un crit?re donn?
	 * Ex?cut? une requ?te SQL du type : SELECT Á¤fieldnameÁ FROM Á¤tableÁ WHERE Á¤uniqueKeyÁ = '¤uniqueKeyValue' LIMIT 1
	 * @param string ¤fieldname le nom de la colonne ? s?lectionner
	 * @param string ¤table la table ? s?lectionn?
	 * @param string ¤uniqueKey le nom de la cl? unique
	 * @param mixed ¤uniqueKeyValue la valeur de la cl? unique
	 * @static
	 * @return string le r?sultat de la recherche ou false si aucun r?sultat retourn?
	 */
	 
	public static function getDBValue( ¤fieldname, ¤table, ¤uniqueKey, ¤uniqueKeyValue, ¤allowEmptyResultset = true ){
		
		¤query = "
		SELECT Á¤fieldnameÁ
		FROM Á¤tableÁ
		WHERE Á¤uniqueKeyÁ = " . self::quote( ¤uniqueKeyValue ) . "
		LIMIT 1";
		
		¤rs =& self::query( ¤query );
		
		if( ¤rs === false || ( !¤rs->RecordCount() && !¤allowEmptyResultset ) )
			trigger_error( "Erreur SQL : ¤query", E_USER_ERROR );

		if( !¤rs->RecordCount() )
			return false;
			
		return ¤rs->fields( ¤fieldname );
		
	}	
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne le r?sultat d'une requ?te SQL donn?e
	 * @param string ¤query la requ?te SQL ? ?x?cuter
	 * @param bool ¤allowEmptyResultset autorise ou non l'abscence de r?sultat ( optionnel, true par d?faut )
	 * @param bool ¤dieOnError interrompt l'?x?cution du script PHP en cas d'erreur ou non ( optionnel, true par d?faut )
	 * @param string ¤errorMessage message d'erreur affich? si ¤dieOnError vaut true et que l'?x?cution de la requ?te produit une erreur
	 * @return mixed une resource ADODB en cas de succ?s. Si l'?x?cution de la requ?te g?n?re une erreur,
	 * la m?thode retourne false ¤dieOnError vaut false, sinon rien 
	 */
	public static function &query( ¤query, ¤allowEmptyResultset = true ){
	
		¤rs = self::getConnection()->Execute( ¤query );
		
		if( ¤rs === false || ( !¤rs->RecordCount() && !¤allowEmptyResultset ) )
			trigger_error( "Erreur SQL : ¤query", E_USER_ERROR );
		
		return ¤rs;
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne la plus petite valeur de ¤fieldname disponible ( non utilis?e ) dans la table ¤tableName
	 * @static
	 * @param string ¤tableName le nom de la table
	 * @param string ¤fieldname le nom du champ
	 * @return int
	 */
	public static function getUniqueKeyValue( ¤tablename, ¤fieldname ){

		¤rs =& self::query( "SELECT MAX( Á¤fieldnameÁ ) + 1 AS Á¤fieldnameÁ FROM Á¤tablenameÁ" );
		
		if( ¤rs === false || !¤rs->RecordCount() )
			trigger_error( "Impossible de r?cup?rer une nouvellle valeur pour la cl? ¤keyname dans la table ¤tablename", E_USER_ERROR );
			
		return ¤rs->fields( ¤fieldname ) == null ? 1 : ¤rs->fields( ¤fieldname );
		
	}
	
	//--------------------------------------------------------------------
	
	public static function stdclassFromDB( ¤table, ¤uniqueKey, ¤uniqueKeyValue ){
		
		¤query = "
		SELECT *
		FROM Á¤tableÁ
		WHERE Á¤uniqueKeyÁ =". self::quote( ¤uniqueKeyValue ) . "
		LIMIT 1";
		
		¤rs =& self::query( ¤query );
		
		if( ¤rs === false  )
			trigger_error( "Erreur SQL : ¤query", E_USER_ERROR );
			
		return ( object )¤rs->fields;

	}
	
	
	//--------------------------------------------------------------------

	/**
	 * Recherche un param?tre
	 */
	public static function getParameter( ¤ParamName ){

		¤query = "SELECT ÁparamvalueÁ FROM Áparameter_catÁ WHERE ÁidparameterÁ = '¤ParamName'";
		¤rs =& self::query( ¤query );

		if( ¤rs === false )
			die( "Impossible de r?cup?rer la valeur du param?tre ¤ParamName." );

		if( !¤rs->RecordCount() )
			return false;

		return ¤rs->fields( "paramvalue" );

	}
	
	//--------------------------------------------------------------------

	/**
	 * Recherche un param?tre de l'administration
	 */
	public static function getParameterAdmin( ¤ParamName ){

		¤query = "SELECT ÁparamvalueÁ FROM Áparameter_adminÁ WHERE ÁidparameterÁ = '¤ParamName'";
		¤rs =& self::query( ¤query );

		if( ¤rs === false )
			die( "Impossible de r?cup?rer la valeur du param?tre ¤ParamName." );

		if( !¤rs->RecordCount() )
			return false;

		return ¤rs->fields( "paramvalue" );

	}

	//--------------------------------------------------------------------

	/**
	 * Retourne la valeur par d?faut d'un champ donn? d'une table donn?e
	 * @static
	 * @param string ¤tableName le nom de la table
	 * @param string ¤fieldName le nom du champ
	 * @return string
	 */
	public static function getDefaultValue( ¤tableName, ¤fieldName ){
		
		¤query = "SELECT default_value FROM desc_field WHERE tablename LIKE " . self::quote( ¤tableName ) . " AND fieldname LIKE " . self::quote( ¤fieldName ) . " LIMIT 1";
		
		¤rs =& self::query( ¤query );
		
		if( !¤rs->RecordCount() )
			return false;
			
		return ¤rs->fields( "default_value" );
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne le nombre de lignes affect?es par la derni?re requ?te SQL
	 * @return int
	 */
	public static function getAffectedRows(){
		
		return self::getConnection()->Affected_Rows();
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * @return string
	 */
	public static function quote( ¤string ){
		
		return self::getConnection()->quote( ¤string );
		
	}
	
	//--------------------------------------------------------------------
	
	/**
	 * Retourne la derni?re valeur d'autoincrement mise ? jour
	 * @return int
	 */
	public static function getInsertID(){
		
		return self::getConnection()->Insert_ID();
		
	}
	
	//--------------------------------------------------------------------
	
}

?>