<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des produits
 */
 

include_once('adm_table.php');

class ADMPRODUCT extends ADMTABLE
{
/* deefined by parent class 
var $tablename;
var $tabledesc = false;
var $design=false;
var $Array_DisplayFields;
var $current_record = false ;
*/

	//var $Array_DisplayFields = false;
	private $intitile_titles;
	private $intitile_orders;
	private $cotations;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Contructeur
	 * @param String $tablename Nom de la table
	 */
	function __construct($tblname){
		
		$this->tablename = $tblname;
		$this->load_design();
        //ADMTABLE::ADMTABLE($tblname);
		//$this->tablename = $tblname;
		$this->intitile_titles = array();
		$this->intitile_orders = array();
		$this->cotations = array();
		$this->MakeEmptyRecord();
		$this->intitule_design();
        
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Définition de la structure des intitulés dans l'objet
	 * @return void
	 */
	private function intitule_design() {
		
		$n = count($this->Array_DisplayFields);
		$Array_IntituleFields=array();
 		for($i=$n,$j=1;$j< 31;$i++,$j++) {
	 		$Array_IntituleFields[$i]['fieldname']=	"parm_name_$j";
			$Array_IntituleFields[$i]['type']=	'char';
			$Array_IntituleFields[$i]['size']=	'30';
			$Array_IntituleFields[$i]['help']=	'';
			$Array_IntituleFields[$i]['value']=	'';
			$Array_IntituleFields[$i]['search_select_table']= '';
			$Array_IntituleFields[$i]['append_select_table']= '';
 		}

 		for($i=$n+30,$j=1;$j< 31;$i++,$j++) {
	 		$Array_IntituleFields[$i]['fieldname']=	"cotation_$j";
			$Array_IntituleFields[$i]['type']=	'char';
			$Array_IntituleFields[$i]['size']=	'5';
			$Array_IntituleFields[$i]['help']=	'';
			$Array_IntituleFields[$i]['value']=	'';
 		}
 				
		$this->Array_DisplayFields += $Array_IntituleFields;
		//var_dump($this->Array_DisplayFields);
		//echo'<pre> Array_DisplayFields';print_r($this->Array_DisplayFields);echo "\n Array_IntituleFields \n";	print_r($Array_IntituleFields);	echo'</pre>';
		//exit();
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * ADMPRODUCT::store_intitule()	Gère les champs intitulés supplémentaires x_param...
	 * Copie du champ x_parm_name... si non vide dans  parm_name...
	 * @return void
	 */
	public function store_intitule(){
		
		foreach ($_POST as $key=>$value) {
			if( preg_match("/parm_name_([0-9]*)/",$key,$regs) ) {
				$i=$regs[1];
				$this->intitile_titles[$key]=$value;
				$parm_order="parm_order_$i";
				
				if(isset($_POST[$parm_order])) 
					$this->intitile_orders[$key]=0+$_POST[$parm_order];
				else $this->intitile_orders[$key]=0+$i;
				
				$cotation="cotation_$i";
				if(isset($_POST[$cotation])) 
					$this->cotations[$key]=$_POST[$cotation];
				else $this->cotations[$key]='';
				
			}
		}

	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la categorie d'un produit
	 * @param int $IdProduct L'id du produit
	 * @return int
	 */
	public function LoadCategoryFromProduct( $IdProduct ){
		
		$data = DBUtil::getConnection()->GetRow("select distinct(idcategory) from product where idproduct='$IdProduct'");
		if($data) return $data["idcategory"];
		return 0;
	
	}

	//----------------------------------------------------------------------------
	
	/**
	 * mise à jour des intitulés
	 * @param int $idc le numéro de la catégorie
	 * return void
	 */
	public function ModifyIntitule($idc=0) {
		
		
		if( empty($this->intitile_titles) ) return; // rien à faire
		$dirty=false;
		//recherche des inititules dans la base
		$todel= array(); $intitules = array(); $cotations = array();
		$SQL_select='SELECT DISTINCT idcategory, idintitule_title, title_display, cotation FROM intitule_link WHERE idproduct='.$this->current_record['idproduct'];
		$rs = DBUtil::getConnection()->Execute($SQL_select);
		while(!$rs->EOF) {
			$x=$rs->fields['idintitule_title'];
			$intitules[$x]=$rs->fields['title_display'];
			$cotations[$x]=$rs->fields['cotation'];
			$idc=$rs->fields['idcategory'];
			$todel[$x]=$x;
			$rs->MoveNext();
		}
		$i=1;
		array_multisort($this->intitile_orders,SORT_NUMERIC,$this->intitile_titles,$this->cotations);
		foreach($this->intitile_titles as $k=>$v) {
			$idt = $this->add_intitule_title($v);
		  if($idt>0) {
			if( array_key_exists($idt,$intitules)) { //l'intitulé existe
				unset($todel[$idt]);
				if($i != $intitules[$idt] OR $this->cotations[$k] != $cotations[$idt] ) {
					$SQL_select='UPDATE intitule_link SET title_display='.$i.', cotation='.DBUtil::getConnection()->qstr($this->cotations[$k]).' WHERE idintitule_title='.$idt.' AND idproduct='.$this->current_record['idproduct'];
					DBUtil::getConnection()->Execute($SQL_select); $dirty=true;
				}
			} else {
				/*TODO idfamily and family_display
				$SQL_select='INSERT INTO intitule_link SET idcategory= '.$idc.', title_display='.$i.', idintitule_title='.$idt.
						', idproduct='.$this->current_record['idproduct'];*/
				
				//famille d'intitulé
				
				$familyInfos = $this->getHeadingFamilyInfos( $idt ); //$familyInfos = array( "idintitule_family" => $idintitule_family, "family_display" => $family_display );

				if( $familyInfos === false ){
				
					$idintitule_family = 0;
					$family_display = 0;
					
				}
				else{
				
					$idintitule_family 	= $familyInfos[ "idintitule_family" ];
					$family_display 	= $familyInfos[ "family_display" ];
					
				}
				
				$query = "
				INSERT INTO intitule_link ( 
					idcategory,
					title_display,
					idintitule_title,
					idproduct,
					idfamily,
					family_display
				) VALUES (
					'$idc',
					'$i',
					'$idt',
					'" . $this->current_record[ "idproduct" ] . "',
					'$idintitule_family',
					'$family_display'
				)";
				
				DBUtil::getConnection()->Execute( $query );
				
				$intitules[$idt]=$i; $dirty=true;
			}
		  }//idt>0
		$i++;
		}

		//patch ordre des intitulés ---------------------------------------
		
		$idproduct = $this->current_record[ "idproduct" ];
		$lang = User::getInstance()->getLang();
		
		$headings = array();
		foreach( $_POST as $key => $value ){
			
			if( preg_match( "/^parm_order_[0-9]+\$/", $key ) ){
			
				$index = substr( $key, 11 );
				
				$order = $_POST[ "parm_order_$index" ];
				$intitule_title = $_POST[ "parm_name_$index" ];
				
				$headings[ $order ] = $intitule_title;
				
			}
			
		}
		
		ksort( $headings, SORT_NUMERIC );
		$keys = array_keys( $headings );
		
		$i = 0;
		while( $i < count( $headings ) ){
		
			$intitule_title = $headings[ $keys[ $i ] ];
			$idintitule_title = DBUtil::getDBValue( "idintitule_title", "intitule_title", "intitule_title$lang", $intitule_title );
			
			$query = "
			UPDATE intitule_link
			SET title_display = '$i'
			WHERE idproduct = '$idproduct'
			AND idintitule_title = '$idintitule_title'";
			
			DBUtil::query( $query );
				
			$i++;
			
		}
		
		//-----------------------------------------------------------
		
		foreach($todel as $v) { //deletes
			$SQL_select='DELETE FROM intitule_link WHERE idintitule_title='.$v.' AND idproduct='.$this->current_record['idproduct'];
			DBUtil::getConnection()->Execute($SQL_select); $dirty=true;
		 }
		//si modif recharge des intitulés
		if( $dirty) $this->LoadIntitules($this->current_record['idproduct']);
	}

	//----------------------------------------------------------------------------
	
	
	/**
	 * Recherche parmis toutes les catégories parentes, à quelle famille d'intitulés appartient l'intitulé spécifié
	 * @param int $idintitule_title
	 * @param int $idcategory réservé pour la récursivité, ne pas utiliser
	 * @return mixed un tableau indéxé contenant l'identifiant de la famille d'intitulé à laquelle appartient l'intitulé spécifié ainsi que son ordre d'affichage, false si aucune famille trouvée
	 */
	private function getHeadingFamilyInfos( $idintitule_title, $idcategory = false ){
	
		if( $idcategory === false ){
		
			$idproduct = $this->current_record[ "idproduct" ];
			
			$query = "SELECT idcategory FROM product WHERE idproduct = '$idproduct' LIMIT 1";
			
			$rs = DBUtil::getConnection()->Execute( $query );
			
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de récupérer la catégorie du produit" );
				
			$idcategory = $rs->fields( "idcategory" );
			
		}
		
		$query = "
		SELECT itf.idintitule_family, itf.display
		FROM intitule_title_family itf, intitule_category ic
		WHERE itf.idintitule_title = '$idintitule_title'
		AND itf.idintitule_family = ic.idintitule_family
		AND ic.idcategory = '$idcategory'
		LIMIT 1";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de vérifier à quelle famille d'intitulés appartient l'intitulé '$idintitule_title'" );
			
		if( $rs->RecordCount() )
			return array( 
			
				"idintitule_family" => $rs->fields( "idintitule_family" ),
				"family_display" 	=> $rs->fields( "display" )
			
			);

		$query = "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcategory' LIMIT 1";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la catégorie parente pour la catégorie '$idcategory'" );
			
		if( !$rs->RecordCount() )
			return false;
			
		return $this->getHeadingFamilyInfos( $idintitule_title, $rs->fields( "idcategoryparent" ) );
			
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Recherche ou créé un intitulé et retourne son identifiant
	 * @param string $v le nom de l'intitulé
	 * @return int l'identifiant de l'intitulé
	 */
	public function add_intitule_title($v) {
		
		if($v=='') return 0;
		$query = "SELECT idintitule_title FROM intitule_title WHERE intitule_title_1=".DBUtil::getConnection()->qstr($v,get_magic_quotes_gpc());
		$rsc = DBUtil::getConnection()->Execute($query);
		if($rsc->EOF) {
			$query = "SELECT MAX(idintitule_title) as idmax FROM intitule_title";
			$rsd = DBUtil::getConnection()->Execute($query);
			$newid = 1 + $rsd->fields['idmax'];
			$query = "INSERT INTO intitule_title SET idintitule_title=$newid, intitule_title_1=".DBUtil::getConnection()->qstr($v,get_magic_quotes_gpc());
			DBUtil::getConnection()->Execute($query); //TODO ajout username et date
		} else {
			$newid = $rsc->fields['idintitule_title'];
		}
		return $newid;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Recherche des masques dans product_display_detail.php
	 * @todo corriger - n'est pas utilisé
	 * @return void
	 */
	public function SelectMaskDetail($Select=""){
		
		$FileArray=file("../../catalog/product_display_detail.php");
		echo "<select name=productmask>\n";
		if($Select==-1)
			$Selected="selected"; 
		else
			$Selected="";
		echo "<option $Selected value=-1>Aucun</option>\n";
		if($Select=="")
			$Selected="selected"; 
		else
			$Selected="";
		echo "<option $Selected value=\"\">0</option>\n";
		for($i=0 ; $i < count($FileArray) ; $i++)
		{
			if(strstr($FileArray[$i],"function DisplayDetail"))
			{	
				$ExpF=explode("_",$FileArray[$i]);
				$ExpMask=explode("(",$ExpF[1]);
				if($ExpMask[0]!="")
				{
					if($Select==$ExpMask[0])
						$Selected="selected";
					else
						$Selected="";
					echo "<option $Selected value=\"$ExpMask[0]\">$ExpMask[0]</option>\n";
				}
			}
		}
		echo "</select>";
						
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Génère la clause sql WHERE
	 * @param int $IdCategory L'id de la catégorie
	 * @param int $IdSupplier L'id du fournisseur (facultatif)
	 * @return string
	 */
	public function FindCatProductQuery($IdCategory,$IdSupplier=''){
		
		$Result="where idcategory='$IdCategory'";
		if($IdSupplier!="") $Result .= " and idsupplier='$IdSupplier'";
		return $Result;
	
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Suppression de la valeur d'un intitulé si le nom est vide
	 * @return void
	 */
	public function UpdateIntituleValue(){
		
		$idp = $this->current_record['idproduct'];
		$Query = "DELETE FROM intitule_link WHERE idproduct=$idp AND idintitule_title < 1";

		$rs = DBUtil::getConnection()->Execute($Query);
		if(!$rs) echo "ERROR UpdateIntituleValue $Query <br />";
	
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Récupère tous les intitulés d'un produit donné
	 * @param int $idproduct le produit dont on souhaite récupérer les intitulés
	 * @return void
	 */
	public function InsertIntitule($idproduct) {

		if($idproduct < 1 ) return;
		//recherche des inititules dans la base
		$todel= array(); $intitules = array(); $cotations = array();
		$SQL_select='SELECT DISTINCT idcategory, idintitule_title, title_display, idfamily, family_display, cotation FROM intitule_link WHERE idproduct='.$idproduct;
		$rs = DBUtil::getConnection()->Execute($SQL_select);
		while(!$rs->EOF) {
			$idt=$rs->fields['idintitule_title'];
			$td=$rs->fields['title_display'];
			$cot=$rs->fields['cotation'];
			$idc=$rs->fields['idcategory'];
			$idf=$rs->fields['idfamily'];
			$fd=$rs->fields['family_display'];
			$SQL_select='INSERT INTO intitule_link SET idcategory= '.$idc.', title_display='.$td.', idintitule_title='.$idt.', idfamily='.$idf.', family_display='.$fd.
						', idproduct='.$this->current_record['idproduct'];

			DBUtil::getConnection()->Execute($SQL_select);
			$rs->MoveNext();
		}

	}

	//----------------------------------------------------------------------------
	
	/**
	 * Chargement des intitulés d'un produit donné
	 * @param int $idproduct le numéro du produit
	 * @return voids
	 */	
 	public function LoadIntitules($idproduct=0) {
 		if($idproduct==0) $idproduct= 0+$this->get('idproduct');
 		if($idproduct == 0 ) return;
		foreach( $this->Array_DisplayFields as $i=>$v ) if( $v['fieldname']== 'parm_name_1') break;
 		/*$SQL_select = 'SELECT DISTINCT t.intitule_title_1 as it ,l.cotation,' .
			'l.title_display as dt, l.family_display as df' .
			' FROM intitule_link l,intitule_title t WHERE l.idproduct = ' . $idproduct .
			' AND l.idintitule_title = t.idintitule_title' .
			' ORDER BY l.family_display,l.title_display';*/
 		
 		$SQL_select = "
 		SELECT DISTINCT( t.intitule_title_1 ) AS it, 
 			t.idintitule_title,
 			l.cotation,
			MAX( l.title_display ) AS dt, 
			MAX( l.family_display ) AS df
		FROM intitule_link l,intitule_title t 
		WHERE l.idproduct = '$idproduct'
		AND l.idintitule_title = t.idintitule_title
		GROUP BY t.intitule_title_1
		ORDER BY l.title_display";
 		
		$rs = DBUtil::getConnection()->Execute($SQL_select);

		while(!$rs->EOF) {
			$this->Array_DisplayFields[$i]["value"]=	$rs->Fields('it');
			$this->Array_DisplayFields[$i+30]["value"]=	$rs->Fields('cotation');
			$rs->MoveNext();
			$i++;
		}
		
 	}
 	
 	//----------------------------------------------------------------------------
	
	/**
	 * Supprime un produit et ses images
	 * @param string $Str_CodedCondition La clause sql WHERE
	 * @return void
	 */ 
	public function DeleteProduct($Str_CodedCondition)
	{
		global $GLOBAL_START_PATH;
		$Str_Condition = trim($this->SerialDecode($Str_CodedCondition));
		if($Str_Condition == '') exit("Error DeleteProduct");
		$Directory= $GLOBAL_START_PATH .'/www';
		$rs= DBUtil::getConnection()->Execute("select idproduct from ".$this->tablename." ". $Str_Condition );
		if($rs->RecordCount()){
			
			$idproduct = $rs->fields( "idproduct" );
			$files = glob( dirname( __FILE__ ) . "/../images/products/$idproduct*{.jpg,.jpeg,.png,.gif}", GLOB_BRACE );
			
			foreach( $files as $file )
				unlink( $file );
				
		}
		DBUtil::getConnection()->Execute("DELETE FROM ".$this->tablename." $Str_Condition");
		DBUtil::getConnection()->Execute("DELETE FROM intitule_link $Str_Condition");
		DBUtil::getConnection()->Execute("DELETE FROM detail $Str_Condition");
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Affiche une liste déroulante avec les intitulés pour une famille donnée
	 * @param int $Family L'id de la famille
	 * @param string $Select La valeur sélectionné
	 * @param boolean $addempty Si true ajoute un élément vide dans la liste
	 * @param string $HtmlField Le nom html de la liste (cas particuliers)
	 * @return void
	 */
	public function SelectIntitule($Family,$Select="",$addempty=false,$HtmlField=""){
	
		$found=false;//test si le $Select se trouve dans la liste
		$Query = "SELECT `intitule_title`.`idintitule_title` AS `id`, `intitule_title`.`intitule_title_1` AS `nom` " .
				"FROM `intitule_title`, `intitule_title_family` " .
				"WHERE `intitule_title`.`idintitule_title`=`intitule_title_family`.`idintitule_title` ";
		$Query .= "AND `intitule_title_family`.`idintitule_family`='$Family' ";
		$Query .= "ORDER BY nom ASC";
		
		//$Query .= "ORDER BY `intitule_title_family`.`display`";
		$rs = DBUtil::getConnection()->Execute($Query);
		if(!$rs)
			showDebug($Query,"ADMPRODUCT::SelectIntitule($Family,$Select,$addempty,$HtmlField)",$this->DEBUG);
		if($rs && $rs->RecordCount()>0)
		{
			echo "<select name=\"$HtmlField\">\n";
			if($addempty) echo "<option value=\"\"></option>\n";
			while(!$rs->EOF)
			{
				$Value=$rs->fields['nom'];
				if($Select==$Value) { echo "<option selected value=\"$Value\">$Value</option>\n"; $found=true;}
				else echo "<option value=\"$Value\">$Value</option>\n";
			$rs->MoveNext();
			}
			if($Select != "" && !$found)  echo "<option selected value=\"$Select\">$Select</option>\n";
			echo "</select>\n";
		}
		else echo "<input type=\"text\" name=\"$HtmlField\" size=\"30\" value=\"$Select\">";
	
		/*global $GLOBAL_START_URL;
		
		$query = "
		SELECT COUNT( `intitule_title`.`idintitule_title` ) AS count
		FROM `intitule_title`, `intitule_title_family`
		WHERE `intitule_title`.`idintitule_title`=`intitule_title_family`.`idintitule_title`
		AND `intitule_title_family`.`idintitule_family`='$Family'";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la liste des intitulés" );
		
		$count = $rs->fields( "count" );
		
		if( !$count )
			echo Dictionnary::translate( "no_heading_available" );
		else{
	
			$allowEmptyValue = $addempty ? "true" : "false";
			$url = "$GLOBAL_START_URL/administration/catalog/heading_list.php?idintitule_family=$Family&target=$HtmlField&selection=" . urlencode( $Select ) . "&allowEmptyValue=$allowEmptyValue";
			$onclick = "window.open( '$url', '_blank', 'directories=no, location=no, menubar=no, resizable=yes, scrollbars=yes, status=no, toolbar=no, width=650, height=650' ); return false;";
			
			$label = empty( $Select ) ? "btn_select" : "btn_modify";
			
			?><input type="button" value="<?php echo $label ?>" onclick="<?php echo $onclick ?>" class="blueButton" /><?php
		
		}
		
		?>
		<input type="text" readonly="readonly" id="<?php echo $HtmlField ?>" name="<?php echo $HtmlField ?>" value="<?php echo htmlentities( $Select ) ?>" style="border-style:none; width:250px;" />
		<?php*/
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne l'id de la famille d'un intitulé
	 * @param string $Parm_Name Le nom de l'intitulé
	 * @param int $lang La langue
	 * @return int L'id de la famille ou -1 sinon
	 */
	public function GetIntituleFamily($Parm_Name,$lang){
		
		$Family = -1;
		$eSelect = addslashes(stripslashes($Parm_Name));
		$Query = 'SELECT `intitule_title_family`.`idintitule_family` AS `id` ' .
				'FROM `intitule_title`, `intitule_title_family` ' .
				'WHERE `intitule_title`.`idintitule_title`=`intitule_title_family`.`idintitule_title` ';
		$Query .= "AND `intitule_title`.`intitule_title$lang`='$eSelect' ";
		//echo "Q $Query Q<br />";
		$rs = DBUtil::getConnection()->Execute($Query);
		if( $rs->RecordCount() > 0  ) {
			$Family = $rs->fields['id'];
		}
		return $Family;
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Retourne les id de la famille d'un intitulé
	 * @param string $Parm_Name Le nom de l'intitulé
	 * @param int $lang La langue
	 * @return string Les id de la famille ou -1 sinon ou -2 si l'intitulé n'est pas géré
	 */
	public function GetIntituleFamilyArray($Parm_Name,$lang)
	{
		$Family = -1; // pas de famille -> -1
		$eSelect = addslashes(stripslashes($Parm_Name));
		$Query = 'SELECT `idintitule_title` AS `id` FROM `intitule_title` ' ."WHERE `intitule_title$lang`='$eSelect' ";
		$rs = DBUtil::getConnection()->Execute($Query);
		if( $rs->RecordCount() > 1  ) return -3; // doublon!
		if( $rs->RecordCount() > 0  ) {
			$idintit = $rs->fields['id'];
		} else return -2; // non géré
		
		$Query = 'SELECT `idintitule_family` AS `id` ' .
				'FROM `intitule_title_family` ' .
				'WHERE `idintitule_title` = '. $idintit;
		//echo "Q $Query Q<br />";
		$rs = DBUtil::getConnection()->Execute($Query);
		if( $rs->RecordCount() > 0  ) {
			$Family =''; $sep='';
			while(!$rs->EOF) {
				$Family .= $sep.$rs->fields['id'];
				$sep=',';
				$rs->MoveNext();	
			}
		}
		return $Family;
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Affiche une liste déroulante avec les intitulés pour une famille donnée
	 * @param string $Parm_Name Le nom de l'intitulé
	 * @param int $lang La langue
	 * @param string $Select La valeur sélectionné
	 * @param boolean $addempty Si true ajoute un élément vide dans la liste
	 * @param string $HtmlField Le nom html de la liste (cas particuliers)
	 * @return void
	 */
	private function SelectIntituleModif($Parm_Name,$lang,$Select,$addempty=false,$HtmlField="")
	{
		$found=false;//test si le $Select se trouve dans la liste
		$eSelect = addslashes(stripslashes($Select));
		$Query = 'SELECT `intitule_title_family`.`idintitule_family` AS `id` ' .
				'FROM `intitule_title`, `intitule_title_family` ' .
				'WHERE `intitule_title`.`idintitule_title`=`intitule_title_family`.`idintitule_title` ';
		$Query .= "AND `intitule_title`.`intitule_title$lang`='$eSelect' ";
		$rs = DBUtil::getConnection()->Execute($Query);
		if( $rs->RecordCount() < 1 ) {
			echo "<input type=\"text\" name=\"$HtmlField\" size=\"30\" value=\"$Select\">";
			return;
		}
		$Family = $rs->fields['id'];
		$Query = "SELECT `intitule_title`.`idintitule_title` AS `id`, `intitule_title`.`intitule_title_1` AS `nom` " .
				"FROM `intitule_title`, `intitule_title_family` " .
				"WHERE `intitule_title`.`idintitule_title`=`intitule_title_family`.`idintitule_title` ";
		$Query .= "AND `intitule_title_family`.`idintitule_family`='$Family' ";
		$Query .= "ORDER BY `nom`";
		//$Query .= "ORDER BY `intitule_title_family`.`display`";
		$rs = DBUtil::getConnection()->Execute($Query);
		if(!$rs)
			showDebug($Query,"ADMPRODUCT::SelectIntitule($Family,$Select,$addempty,$HtmlField)",$this->DEBUG);
		if($rs && $rs->RecordCount()>0)
		{
			echo "<select name=\"$HtmlField\">\n";
			if($addempty) echo "<option value=\"\"></option>\n";
			while(!$rs->EOF)
			{
				$Value=$rs->fields['nom'];
				if($Select==$Value) { echo "<option selected value=\"$Value\">$Value</option>\n"; $found=true;}
				else echo "<option value=\"$Value\">$Value</option>\n";
			$rs->MoveNext();
			}
			if($Select != "" && !$found)  echo "<option selected value=\"$Select\">$Select</option>\n";
			echo "</select>\n";
		}
		else echo "<input type=\"text\" name=\"$HtmlField\" size=\"30\" value=\"$Select\">";
}

	//----------------------------------------------------------------------------
	
	/**
	 * Affiche le hiérachie de la catégorie
	 * @param int $IdCategory L'id de la catégorie
	 * @param string $Path Le chemin (mettre une chaîne vide)
	 * @param string $lang La langue utilisée préfixée d'un underscore
	 * Fonction récursive
	 * @return string le chemin
	 */
	public function AffPathCat($IdCategory, &$Path,$lang)
	{
		if($Path=="") $Path=$IdCategory;
	
		if($IdCategory!=0)
		{
			$rs = DBUtil::getConnection()->Execute("select idcategoryparent from category_link where idcategorychild='$IdCategory'");
			if($rs->RecordCount())
			{
				$IdCategory=$rs->fields["idcategoryparent"];
				$Path.= "," . $IdCategory;
				$this->AffPathCat($IdCategory,$Path,$lang);
				return $Path;
			}
		}
		else
		echo '<div id="navigation">';
	
		{
			$ExpPath=explode(",",$Path);
			$Link='';
			for($i=count($ExpPath)-1 ; $i>=0 ; $i--)
			{
				$rs= DBUtil::getConnection()->Execute("select name$lang from category where idcategory='$ExpPath[$i]'");
				if($rs->RecordCount())
				{
					$CatName=$rs->fields["name$lang"];
					$Link.="$CatName > ";
				}
			}
			$Link=substr($Link,0,-1);
		}
		echo $Link;
		echo '</div>'."\n";
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Affiche les liens vers les aticles d'un produit
	 * @param string $TableName le nom de la table où rechercher les informations sur le produit
	 * @param string $Str_ActionName 
	 * @param string $TableKey le nom du champ à récupérer dans la table $TableName
	 * @param string $FieldName le nom de la colonne contenant le numéro du produit dans la table $TableName
	 * @param int $Int_IdProduct le numéro du produit
	 * @param string $FileHref le lien à afficher
	 * @return void
	 */
	public function ShowLinksToRelatedCatalogs_1 ( $TODOdeleteThisUselessParameter, $TableName, $Str_ActionName, $TableKey, $FieldName, $Int_IdProduct, $FileHref){
		
		//@todo : non mais ça va pas la tête ????
		foreach( $_SESSION as $key => $value ){
			
			if( $key != "__session__" )
				unset( $_SESSION[ $key ] );
					
		}
		
		$SQL_Query = "SELECT $TableKey FROM $TableName WHERE $FieldName='$Int_IdProduct' ORDER BY $TableKey ";
		$rs= DBUtil::getConnection()->Execute($SQL_Query); // find out the field values
		if ($rs->RecordCount()>0) // At least one row found
		{
		echo '<table border=0 class="table_b" width="100%" cellpadding=1  cellspacing=1>';
		echo '<tr><td><?php  echo Dictionnary::translate("new_in1"); ?></td></tr>';
		for ($i=0;!$rs->EOF;$i++)
			{
			$Arr_Tmp=array();
			$Arr_Tmp[$TableKey] =$rs->fields[$TableKey];
			$Arr_Tmp["idproduct"] = $Int_IdProduct;
			$Str_EncodedFinder = base64_encode(serialize($Arr_Tmp)); //convention $j>=1 ->modify, $j=0 ->append
			$j = $i+1;

			$_SESSION[ "Str_ExpandActionName_$j" ] = $Str_ActionName;
			$_SESSION[ "$Str_ActionName_$j" ] = "modify";
			$_SESSION[ "SerialisedArray_k_$j" ] = $Str_EncodedFinder;
			
			echo '<tr><td>';
			echo "\n  \n   <A href=\"$FileHref". "&id=$j\" class=\"lien\">";
			echo $Arr_Tmp[$TableKey] . " </a></td></tr>\n";
			$rs->MoveNext();
			}
		echo "</table>\n";
		}
	
		if ($Str_ActionName =="action")
		{
			unset ($Arr_Tmp);
			$Arr_Tmp[$FieldName] = $Int_IdProduct;
			$Str_EncodedFinder = base64_encode(serialize($Arr_Tmp));
		
			$_SESSION[ "Str_ExpandActionName_0" ] = $Str_ActionName;
			$_SESSION[ "$Str_ActionName_0" ] = "append";
			$_SESSION[ "SerialisedArray_k_0" ] = $Str_EncodedFinder;
			
			echo "\n  <br />\n   <A href=\"$FileHref". "&id=0\" class=\"lien_g\">";
			echo Dictionnary::translate("new_in2");
			echo "</a>";
		}
		
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Affiche un 'input' pour un formulaire, la valeur est traduite
	 * @param stirng $Str_ButtonType Le type du bouton
	 * @param strring $Str_ButtonName Le nom du bouton
	 * @param string $Str_ButtonValue La valeur du bouton
	 * @return void
	 */
	public function InputButton($Str_ButtonType, $Str_ButtonName, $Str_ButtonValue,$Str_Altro='')
	{
		echo '<input type="',$Str_ButtonType,'"  size="8" name="',$Str_ButtonName,'" value="', Dictionnary::translate($Str_ButtonValue),'" ',$Str_Altro,' />',"\n";
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Enregistre le produit
	 * @param string $UniqueKeyName le nom de la clé primaire
	 * @return int 1 en cas de succès, sinon 0
	 */
	public function InsertRecord( $UniqueKeyName = "" ){
	
		$ret = parent::InsertRecord( $UniqueKeyName );
		
		$this->setDefaultHeadings();
		
		return $ret;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Récupère tous les intitulés spécifiques au produit ( ceux qui ne dépendent pas de la catégorie )
	 * @access private
	 * @return void
	 */
	private function setDefaultHeadings(){
	
		$idproduct = $this->current_record[ "idproduct" ];
		
		//récupérer les catégories parentes
		
		$parents = array( $this->current_record[ "idcategory" ] );
		
		$done = false;
		$idchild = $this->current_record[ "idcategory" ];
		while( !$done ){
		
			$query = "
			SELECT idcategoryparent 
			FROM category_link
			WHERE idcategorychild = '$idchild'";

			$rs = DBUtil::getConnection()->Execute( $query );
			
			if( $rs === false )
				die( "Impossible de récupérer la catégorie parente" );
				
			$done = $rs->RecordCount() == 0;
			
			if( !$done ){
			
				$idchild 	= $rs->fields( "idcategoryparent" );
				$parents[] 	= $rs->fields( "idcategoryparent" );
				
			}
			
		}
		
		//récupérer les familles d'intitulés autos
		
		$query = "
		SELECT DISTINCT( itf.idintitule_family )
		FROM intitule_category ic, intitule_title_family itf
		WHERE itf.idintitule_family = ic.idintitule_family
		AND itf.`default` = 1
		AND ic.idcategory IN( " . implode( ",", $parents ) . " )";

		$rs = DBUtil::getConnection()->Execute( $query );
			
		if( $rs === false )
			die( "Impossible de récupérer les familles d'intitulés par défaut" );
		
		while( !$rs->EOF() ){
		
			$this->setDefaultHeadingFamily( $rs->fields( "idintitule_family" ) );
			
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Récupère tous les intitulés d'une famille donnée et qui sont spécifiques au produit ( ceux qui ne dépendent pas de la catégorie )
	 * @access private
	 * @param int $idintitule_family le numéro de la famille d'intitulés
	 * @return void
	 */
	private function setDefaultHeadingFamily( $idintitule_family ){

		$idproduct 		= $this->current_record[ "idproduct" ];
		$idcategory 	= $this->current_record[ "idcategory" ];
		$username	 	= User::getInstance()->get( "login" );
		
		//récupérer les intitulés de la famille
		
		$query = "
		SELECT DISTINCT( idintitule_title ) 
		FROM intitule_title_family
		WHERE idintitule_family = '$idintitule_family'";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les intitulés par défaut" );
			
		while( !$rs->EOF() ){
		
			$idintitule_title = $rs->fields( "idintitule_title" );
			$title_display = 0;
			
			$query = "
			INSERT INTO intitule_link(
				idarticle,
				idproduct,
				idcategory,
				idfamily,
				family_display,
				idintitule_title,
				title_display,
				idintitule_value,
				reference,
				cotation,
				level,
				dyn_text,
				lastupdate,
				username
			) VALUES (
				0,
				$idproduct,
				$idcategory,
				$idintitule_family,
				0,
				$idintitule_title,
				$title_display,
				0,
				'',
				'',
				0,
				'',
				NOW(),
				'$username'
			)";

			$rs2 = DBUtil::getConnection()->Execute( $query );
			
			if( $rs2 === false )
				die( "Impossible de créer les intitulés par défaut" );
				
			$title_display++;
			
			$rs->MoveNext();
			
		}
		
	}

	//----------------------------------------------------------------------------
	
}//EOC
?>