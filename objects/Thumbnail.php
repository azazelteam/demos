<?php
 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object dimensionnement des images photos
 */

include_once( dirname( __FILE__ ) . "/../config/config.php" );


abstract class Thumbnail{
 	
 	/* ------------------------------------------------------------------------------------------------ */
 	
 	private static $defaultImageSize = NULL;
 	private static $maximumImageSize = NULL;

	/* ------------------------------------------------------------------------------------------------ */
 	/**
 	 * @static
 	 * @access public
 	 * @param string $sourcePath
 	 * @param int $outputWidth
 	 * @param int $outputHeight
 	 * @return void
 	 */
 	public static function createThumb( $sourcePath, $outputWidth = 0, $outputHeight = 0 ){

 		/* création de la resource */
 		
		$pos = strrpos( $sourcePath, "." );
		
		if( $pos === false || $pos == strlen( $sourcePath ) - 1 )
			return false;

		$infos = getimagesize( $sourcePath );
		$imageType = $infos[ 2 ];
		
		switch( $imageType ){
			
			case IMAGETYPE_JPEG :
			
				$inputImage = imagecreatefromjpeg( $sourcePath );
				break;

			case IMAGETYPE_PNG :
			
				$inputImage = imagecreatefrompng( $sourcePath );
				imagealphablending( $inputImage, true );
				imagesavealpha( $inputImage, true );
				break;
			
			case IMAGETYPE_GIF :
			
				$inputImage = imagecreatefromgif( $sourcePath );
				//imagealphablending( $inputImage, true );
				//imagesavealpha( $inputImage, true );
				break;
					
			default : return false;
		
		}
		
		/* dimensions de l'image en sortie */
		
		if( self::$maximumImageSize === NULL )
 			self::$maximumImageSize = DBUtil::getParameter( "maximum_image_size" );

 		if( self::$defaultImageSize === NULL )
 			self::$defaultImageSize = min( self::$maximumImageSize, DBUtil::getParameter( "default_image_size" ) );
 
 		//les dimension de l'image ne peuvent pas être supérieures à la taille maximale fixée
 		
 		$outputWidth 	= min( intval( $outputWidth ), 	self::$maximumImageSize );
 		$outputHeight 	= min( intval( $outputHeight ), self::$maximumImageSize );
 		
 		//sélectionner les dimensions par défaut si les dimension de l'image ne sont pas données
 		
 		if( !$outputWidth )
 			$outputWidth = self::$defaultImageSize;

 		if( !$outputHeight )
 			$outputHeight = self::$defaultImageSize;
 			
 		//les dimensions de l'image ne peuvent être supérieures à celles de l'image d'origine
 		
		$inputWidth 	= imageSX( $inputImage );
		$inputHeight 	= imageSY( $inputImage );
		
		$outputWidth 	= min( $outputWidth, $inputWidth );
		$outputHeight 	= min( $outputHeight, $inputHeight );
		
		//conserver le ratio hauteur/largeur d'origine ( ne pas étirer l'image )
		
		if( $inputWidth > $inputHeight )
			$outputHeight = $inputHeight * ( $outputWidth / $inputWidth);

		if( $inputWidth < $inputHeight )
			$outputWidth =  $inputWidth * ( $outputHeight / $inputHeight );
		
		//dessine moi un mouton
		
		$outputImage = imagecreatetruecolor( $outputWidth, $outputHeight );
		
		imagealphablending( $outputImage, false );
		imagecopyresampled( $outputImage, $inputImage, 0, 0, 0, 0, $outputWidth, $outputHeight, $inputWidth, $inputHeight ); 
		imagesavealpha( $outputImage, true );
		
		switch( $imageType ){
			
			case IMAGETYPE_JPEG :
			
				header( "Content-type: image/jpg" );
				imagejpeg( $outputImage, NULL, 100 );
				break;

			case IMAGETYPE_PNG :
			
				header( "Content-type: image/png" );
				imagepng( $outputImage, NULL, 9 );
				break;
			
			case IMAGETYPE_GIF :
			
				header( "Content-type: image/gif" );
				imagegif( $outputImage );
				break;
					
			default : return false;
		
		}

		imagedestroy( $inputImage );
		imagedestroy( $outputImage );
		
	}
	
    /* ------------------------------------------------------------------------------------------------ */
    
}
 
?>