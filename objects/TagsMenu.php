<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object classe uniquement utilisée pour les mailing-lists mais pouvant être modifiée de manière à créer n'importe
 * quel "accordéon" contenant des tags (tagsArray à passer en paramètre)
*/

include_once( dirname( __FILE__ ) . "/Session.php" );


class TagsMenu{
 	
 	private $id;
	private $tags;
	private $tagDefinitions;
	private $width;
	private $height;
 	
 	//----------------------------------------------------------------------------------------
 	
 	/**
 	 * 
 	 */
 	function __construct($id = ""){
 		$this->init($id);
 	}
 	
 	//----------------------------------------------------------------------------------------
 	
 	/**
 	 * 
 	 */
 	private function init($id){
 		
 		$this->id				=	$id;
 		$this->tags				=	array();
 		$this->tagDefinitions	=	array();
 		$this->width 			=	200;
 		$this->height			=	500;
 		
 	}
 	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Permet de récupérer la liste des tags à afficher
	 */
	public function getTagsArray(){
		return $this->tags;
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Passe un tableau de paramètre reprenant les tags à afficher
	 * @return void
	 */
	public function setTagsArray($tags){
		$this->tags	=	$tags;
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Permet de définir une dimension pour la liste de tags
	 * @return void
	 */
	public function setDimension($width = 200, $height = 500){
		$this->width		=	$width;
		$this->height		=	$height;
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Crée le tableau contenant la liste des tags
	 * @return void
	 */
	public function createTagList($idbuyer = 0, $idcontact = 0){
		
		$iduser = User::getInstance()->getId();
		
		$this->setUseAdminTags();
		$this->setUseBuyerTags($idbuyer, $idcontact);
		$this->setUseCommercialTags($iduser);
		$this->setUseUtilTags();
		$this->setUseEstimateTags($idbuyer, $idcontact);
		//$this->setUseOrderTags();
		
	}

	//----------------------------------------------------------------------------------------
	
	/**
	* Affiche la liste des tags
	* @return void
	**/
	public function displayTagList(){
		
		$this->createTagList();
		
		global $GLOBAL_START_URL;
		
		if( !count( $this->tags ) )
			return;

		if (isset($this->width)){
			$menuWidth = $this->width;
		}
		else{
			$menuWidth = 200;
		}
		
		$itemWidth = floor( $menuWidth / count( $this->tags ) ) - 5;
		
		?>
		<style type="text/css">

		#<?php echo $this->id ?>_basic-accordion {
			
			border:5px solid #EEEEEE;
			padding:2px;
			width:200px;
			
		}
		
		</style>
		<div id="<?php echo $this->id ?>_basic-accordion">
		<?php
		
		$i = 0;
		foreach( $this->tags as $menu => $tags ){
			
			?>
			
			<h3 id="<?php echo $this->id."_".$i ?>-header"><a href="#"><?php echo htmlentities( $this->translate( $menu ) ) ?></a></h3>
			<div id="<?php echo $this->id."_".$i ?>-content">
			<?php
				
				$j = 0;
				foreach( $tags as $tag => $value ){
				
					$label = $this->translate( $menu, $tag );
					$imageID = "{$menu}.{$tag}";
					
					if( $j )
						echo "<br />";
					
					?>
					<img id="<?php echo htmlentities( $imageID ) ?>" name="<?php echo htmlentities( $imageID ) ?>" src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( $label ) ?>" alt="<?php echo htmlentities( $label ) ?>" style="cursor:pointer;" />
					<?php
			
					$j++;
					
				}
			
			?>
			</div>
			<?php
			
			$i++;
			
		}

		?>
		</div>
		<script type="text/javascript">
		<!--
			
			$(document).ready(function(){
				$("#<?php echo $this->id ?>_basic-accordion").accordion({ autoHeight: false });
			});
			
		//-->
		</script>

		<?php
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Récupère la traduction pour les tags
	 * @param string $menu le nom du menu
	 * @param string $item le nom du tag
	 * @return string la traduction
	 */
	private function translate( $menu, $item = false ){
		
		$lang = User::getInstance()->getLang();
		$db =& DBUtil::getConnection();
		
		$query = "
		SELECT language" . substr( $lang, 1 ) . " AS translation 
		FROM translation 
		WHERE idtext LIKE 'tag_$menu";
		
		if( $item )
			$query .= "_$item";
			
		$query .= "' 
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le nom du marqueur '$text'" );
			
		return $rs->fields( "translation" );
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur du tag
	 * @param string $menu le nom du menu
	 * @param string $tag le nom du tag
	 * @return mixed la valeur du tag, false en cas de problème
	 */
	public function getTagValue( $menu, $tag ){

		if( isset( $this->tags[ $menu ] ) && isset( $this->tags[ $menu ][ $tag ] ) )
			return $this->tags[ $menu ][ $tag ];
		
		return false;
		
	}

	//----------------------------------------------------------------------------------------
	
	/**
	 * Initialise les marqueurs
	 * @param string $menu le nom du menu
	 * @param string $table le nom de la table
	 * @param string $columns les champs à récupérer
	 * @param string $primaryKey la clé primaire de la table
	 * @param int $primaryKeyValue la valeur de la clé primaire de la table
	 * @return void
	 */
	private function setTags( $menu, $table, $columns, $primaryKey, $primaryKeyValue = 0 ){

		$db =& DBUtil::getConnection();
		
		$this->tags[ $menu ] = array();
		
		foreach( $columns as $column )
			$this->tags[ $menu ][ $column ] = "";
			
		if( !$primaryKeyValue )
			return;
			
		$select = "";
		//reset( $columns );
		foreach( $columns as $column ){
			
			if( !empty( $select ) )
				$select .= ", ";
				
			$select .= "`$column`";
			
		}
			
		$query = "
		SELECT $select
		FROM `$table`
		WHERE `$primaryKey` = '$primaryKeyValue'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer les valeurs des marqueurs<br />$query" );
		
		//reset( $columns );
		foreach( $columns as $column )
			$this->tags[ $menu ][ $column ] = $rs->fields( $column );
			
	}

	//----------------------------------------------------------------------------------------
	
	/**
	 * Initialise les marqueurs pour le client
	 * @param int $idbuyer l'id du client
	 * @param int $idcontact l'id du contact
	 * @return void
	 */
	public function setUseBuyerTags( $idbuyer = 0, $idcontact = 0 ){
		
		$db =& DBUtil::getConnection();
		$lang = "_1";
		$language = substr($lang, 1);
			
		$this->tagDefinitions[ "BUYER" ] = $idbuyer;
		
		$menu 		= "buyer";
		$table 		= "buyer";
		$primaryKey = "idbuyer";
		
		$columns = array(

			"idbuyer",
			"company",
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idbuyer );
		
		$this->tags[ $menu ][ "title" ] 		= "";
		$this->tags[ $menu ][ "firstname" ] 	= "";
		$this->tags[ $menu ][ "lastname" ] 		= "";
		$this->tags[ $menu ][ "login" ] 		= "";
		$this->tags[ $menu ][ "city" ] 			= "";
		$this->tags[ $menu ][ "zipcode" ] 		= "";
		$this->tags[ $menu ][ "adress" ] 		= "";
		$this->tags[ $menu ][ "state" ]			= "";

		
		if( !$idbuyer )
			return;
			
		$query = "
		SELECT t.title_{$language} AS title, c.firstname, c.lastname
		FROM contact c
		LEFT JOIN title t
		ON c.title = t.idtitle
		WHERE c.idbuyer = '$idbuyer' 
		AND c.idcontact = '$idcontact'
		LIMIT 1";

		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les coordonnées du contact" );
			
		$this->tags[ $menu ][ "title" ] 	= $rs->fields("title") == NULL ? "" : $rs->fields( "title" );
		$this->tags[ $menu ][ "firstname" ] = $rs->fields("firstname");
		$this->tags[ $menu ][ "lastname" ] 	= $rs->fields("lastname");
		
		
		$qb = "SELECT b.zipcode, b.adress, b.adress_2, b.city, b.idstate FROM buyer b WHERE idbuyer='$idbuyer'";
		$rsb = $db->Execute( $qb );
		
		if( $rsb === false )
			die( "Impossible de récupérer les coordonnées du client" );
			
		$this->tags[ $menu ][ "city" ] 		= $rsb->fields("city");
		$this->tags[ $menu ][ "zipcode" ] 	= $rsb->fields("zipcode");
		
		$adress = $rsb->fields("adress");
		$adress_2 = $rsb->fields("adress_2");
		
		if($adress_2!='')
			$adress_full = $adress."<br />".$adress_2;
		else
			$adress_full = $adress;
			
		$this->tags[ $menu ][ "adress" ] 	= $adress_full;
		
		//pays
		
		$idstate = $rsb->fields("idstate");
		
		$qstate = "SELECT name$lang as state FROM state WHERE idstate = '$idstate'";
		
		$rstate = $db->Execute( $qstate );
		
		if( $rstate === false )
			die( "Impossible de récupérer le nom du pays" );
		
		if( $rstate->RecordCount() )
			$this->tags[ $menu ][ "state" ] = $rstate->fields( "state" );
			
		
		//paramètres d'identification
		
		$query = "SELECT login FROM user_account WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les paramètres d'identification" );
			
		if( $rs->RecordCount() )
			$this->tags[ $menu ][ "login" ] = $rs->fields( "login" );
		
	}

	//----------------------------------------------------------------------------------------
	
	/**
	 * Initialise les marqueurs pour le commercial
	 * @param int $iduser l'id du commercial
	 * @return void
	 */
	public function setUseCommercialTags( $iduser ){
		
		$lang = "_1";
		$language = substr($lang, 1);
		$this->tagDefinitions[ "COMMERCIAL" ] = $iduser;
		
		$menu 		= "commercial";
		$table 		= "user";
		$primaryKey = "iduser";
		
		$columns = array(

			"firstname",
			"lastname",
			"phonenumber",
			"faxnumber",
			"email"
			
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $iduser );
		
		//civilité
		
		$this->tags[ $menu ][ "title" ] = "";
			
		if( !$iduser )
			return;
		
		$db =& DBUtil::getConnection();
	
		//email
		
		$email = $this->tags[ $menu ][ "email" ];
	
		$this->tags[ $menu ][ "email" ] = "<a href=\"mailto:$email\">$email</a>";
	
		//civilité
	
		$query = "
		SELECT t.title_{$language} AS title, u.iduser
		FROM user u
		LEFT JOIN title t
		ON u.gender = t.idtitle
		WHERE u.iduser = '$iduser'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer la civilité du commercial" );
		
		$this->tags[ $menu ][ "title" ] = $rs->fields( "title" ) == NULL ? "" : $rs->fields( "title" );

		
	}

	//----------------------------------------------------------------------------------------
	
	/**
	 * Initialise les marqueurs pour les données de la société
	 * @return void
	 */
	public function setUseAdminTags(){

		global 	$GLOBAL_START_URL;
		
		$this->tagDefinitions[ "ADMIN" ] = 1;
		
		$db =& DBUtil::getConnection();
		
		$this->tags[ "admin" ] = array();
	
		$parameters = array(

			"ad_name",
			"ad_firstname",
			
			"ad_company",
			"ad_adress",
			"ad_zipcode",
			"ad_city",
			"ad_state",
			"ad_tel",
			"ad_fax",
			
			"ad_logo",
			"ad_mail",
			"ad_site",
			"ad_sitename",
			
			"ad_siren",
			"ad_siret",
			"ad_ape",
			"ad_tva-intra",
			"ad_registered_brand"
			
		);
		
		$select = "";
		foreach( $parameters as $parameter ){
			
			if( !empty( $select ) )
				$select .= ", ";
				
			$select .= "'$parameter'";
		}
		
		$query = "SELECT idparameter, paramvalue FROM parameter_admin WHERE idparameter IN ( $select )";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les marqueurs administrateurs" );
			
		while( !$rs->EOF() ){
		
			$idparameter 	= $rs->fields( "idparameter" );
			$value 			= $rs->fields( "paramvalue" );
			
			$this->tags[ "admin" ][	$idparameter ] = $value;
			
			$rs->MoveNext();
			
		}
		
		//correctifs
		
		$this->tags[ "admin" ][	"ad_logo" ] = "<img src=\"$GLOBAL_START_URL" . $this->tags[ "admin" ][	"ad_logo" ] . "\" alt=\"\" />";
		$this->tags[ "admin" ][	"ad_sitename" ] = "<a href=\"$GLOBAL_START_URL\">" . $this->tags[ "admin" ][	"ad_sitename" ] . "</a>";
		
	}
	
	//----------------------------------------------------------------------------------------
	
	/**
	 * Initialise les marqueurs pour les outils
	 * @return void
	 */
	public function setUseUtilTags(){

		global $GLOBAL_START_PATH;
		include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
		
		$this->tagDefinitions[ "UTIL" ] = 1;
		
		$this->tags[ "util" ] = array();
	
		$this->tags[ "util" ][ "human_readable_date" ] = humanReadableDate2( date( "Y-m-d" ) );
		
	}
	
	
	public function setUseEstimateTags( $idbuyer = 0, $idcontact = 0 ){
		
		global $GLOBAL_START_URL;
		
		$query = "SELECT max(idestimate)
				FROM estimate
				WHERE idbuyer = $idbuyer AND idcontact = $idcontact
				AND lower(status) like 'send'";
		
		$rs =& DBUtil::query($query);
		
		$idestimate = $rs->fields("max(idestimate)");

		
		$this->tagDefinitions[ "ESTIMATE" ] = $idestimate;
		
		$menu 		= "estimate";
		$table 		= "estimate";
		$primaryKey = "idestimate";
		
		$columns = array(

			"idestimate",
			"DateHeure",
			"valid_until"
			
		);
		
		$this->setTags( $menu, $table, $columns, $primaryKey, $idestimate );
		
		//références du devis
		
		$this->tags[ $menu ][ "estimate_rows" ] = "estimate_rows";
		
		if( $idestimate )
			$this->tags[ $menu ][ "estimate_rows" ] = $this->getReferenceList( "estimate_row", $primaryKey, $idestimate );
			
		//correctifs
		
		if( !$idestimate )
			return;
		
		include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
			
		$this->tags[ $menu ][ "DateHeure" ] = humanReadableDatetime( $this->tags[ "$menu" ][ "DateHeure" ] );
		$this->tags[ $menu ][ "valid_until" ] = usDate2eu( $this->tags[ "$menu" ][ "valid_until" ] );
		$this->tags[ "admin" ][	"ad_logo" ] = "<img src=\"$GLOBAL_START_URL/catalog/logo.php?idestimate=$idestimate&seen=1\" alt=\"\" />";
	
	}
	
	private function getReferenceList( $table, $primaryKey, $primaryKeyValue ){
	
	$db =& DBUtil::getConnection();
	
	$query = "
	SELECT quantity, reference, summary
	FROM `$table`
	WHERE `$primaryKey` = '$primaryKeyValue'";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les lignes article" );
		
	if( !$rs->RecordCount() )	
		return "";
		
	$html ="<ul>";
	
	while( !$rs->EOF() ){
		
		$quantity 	= $rs->fields( "quantity" );
		$reference 	= $rs->fields( "reference" );
		$summary 	= $rs->fields( "summary" );
		
		$html .= "<li>$quantity x $reference";

		if( !empty( $summary ) )
			$html .= " : $summary";
			
		$html .= "</li>";
		
		$rs->MoveNext();
		
	}
	
	$html .= "</ul>";
	
	return $html;
	
	}
 }
 
 	//----------------------------------------------------------------------------------------
	
	/**
	 * Remplace tous les marqueurs par leurs vraies valeurs
	 * @param string $data
	 * @return le contenu avec les tags remplacés
	 */
	function unTag( $data, $idbuyer, $idcontact ){
		
		
		$pattern = "/<img[^>]*src=\"[^>]*\?label=[^>]*\"[^>]*>/i";
		$matches = array();
		
		preg_match_all( $pattern, $data, $matches );
	
		if( !count( $matches ) || !count( $matches[ 0 ] ) )
			return $data;
		
		$i = 0;
		while( $i < count( $matches[ 0 ] ) ){
			
			$match = $matches[ 0 ][ $i ];
			
			$start 	= strpos( $match, "name=\"" ) + 6;
			$end 	= strpos( $match, "\"", $start );
			
			if( $start !== false && $end !== false ){
				
				$tagsmenu = new	TagsMenu();
				$tagsmenu->createTagList($idbuyer, $idcontact);
				
				$tagID 	= substr( $match, $start, $end - $start );
				
				list( $menu, $tag ) = explode( ".", $tagID );
				
				$value = $tagsmenu->getTagValue( $menu, $tag );
				
				if( $value === false )
					die( "Impossible de récupérer la valeur du marqueur '$tagID'" );

				$pattern = "<img[^>]*name=\"$tagID\"[^>]*>";
				$data = eregi_replace( $pattern, $value, $data );

			}
			
			$i++;
			
		}
		
		return $data;
		
	}
 
?>
