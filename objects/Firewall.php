<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object firewall logiciel
 */

class Firewall{

	//----------------------------------------------------------------------------------------------------------
	
	private $self;
	private $remote_addr;
	private $http_user_agent;
	
	private $maxPostPerMin;
	private $maxPostPerHour;
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $maxPostPerMin Nombre de posts maximum par minute
	 * @param int $maxPostPerHour Nombre de posts maximum par heure
	 */
	
	function __construct( $maxPostPerMin = 320, $maxPostPerHour = 19200 ){

		$db =& DBUtil::getConnection();
		
		$this->maxPostPerMin 	= $maxPostPerMin;
		$this->maxPostPerHour 	= $maxPostPerHour;
		
		$this->self 			= $_SERVER[ "PHP_SELF" ];
		$this->remote_addr 		= $_SERVER[ "REMOTE_ADDR" ];
		$this->http_user_agent 	= isset( $_SERVER[ "HTTP_USER_AGENT" ] ) ? $_SERVER[ "HTTP_USER_AGENT" ] : "";

		if( count( $_POST ) )
			$this->logPost();
		
		if( !$this->allowAccess() ){
		
			header( "Status: 404 Not Found" );
			
			exit();

		}

		$this->cleanUp();
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si l'accès est autorisé pour le visiteur courant
	 * @return bool true si l'accès est autorisé, sinon false
	 */
	
	private function allowAccess(){
	
		if( $this->isClientWhiteListed() )
			return true;
			
		if( $this->isClientBlackListed() )
			return false;

		if( count( $_POST ) && !$this->allowPostData() ){
		
			$this->blackListClient();
			$this->report();
			
			return false;
			
		}
			
		return true;
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si le visiteur courant à le droit de poster des données
	 * @return bool true si le visiteur est autorisé à poster des données, sinon false
	 */
	
	private function allowPostData(){

		$db =& DBUtil::getConnection();
		
		list( $year, $month, $day, $hour, $min, $sec ) = explode( "-", date( "Y-m-d-H-i-s" ) );
		
		$time_limit = date( "Y-m-d H:i:s", mktime( $hour, $min - 1, $sec, $month, $day,  $year ) );
		
		$query = "
		SELECT COUNT( * ) AS count
		FROM user_post
		WHERE date_time >= '$time_limit'
		AND remote_addr LIKE '{$this->remote_addr}'";
	
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les droits d'accès pour le visiteur" );
		
		if( $rs->fields( "count" ) > $this->maxPostPerMin )
			return false;
		
		$time_limit = date( "Y-m-d H:i:s", mktime( $hour, $min - 1, $sec, $month, $day,  $year ) );
		
		$query = "
		SELECT COUNT( * ) AS count
		FROM user_post
		WHERE date_time >= '$time_limit'
		AND remote_addr LIKE '{$this->remote_addr}'";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les droits d'accès pour le visiteur" );
		
		if( $rs->fields( "count" ) > $this->maxPostPerHour )
			return false;
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie l'adresse IP du visiteur courant est sur la liste noire
	 * @return bool true si le visiteur est sur la liste noire, sinon false
	 */
	
	private function isClientBlackListed(){
	
		$db =& DBUtil::getConnection();
		
		$query = "SELECT idblack_list FROM black_list WHERE remote_addr LIKE '" . $_SERVER[ "REMOTE_ADDR" ] . "' LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible d'autoriser l'accès au visiteur" );
			
		return $rs->RecordCount() > 0;
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie l'adresse IP du visiteur courant est sur la liste blanche
	 * @return bool true si le visiteur est sur la liste blanche, sinon false
	 */
	
	private function isClientWhiteListed(){
	
		$db =& DBUtil::getConnection();
		
		$query = "SELECT COUNT(*) AS `count` FROM white_list WHERE remote_addr LIKE '" . $_SERVER[ "REMOTE_ADDR" ] . "'";
		
		$rs = $db->Execute( $query );
		
		return $rs->fields( "count" ) == 1;
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Ajoute l'adress IP du visiteur courant à la liste noire
	 * @return void
	 */
	
	private function blackListClient(){
	
		$db =& DBUtil::getConnection();

		$query = "
		INSERT INTO black_list(
			remote_addr,
			date_time
		) VALUES (
			'" . $_SERVER[ "REMOTE_ADDR" ] . "',
			NOW()
		)";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible d'interdire l'accès au visiteur" );
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Enregistre l'heure et l'adresse IP du dernier post utilisateur
	 * @return void
	 */
	
	private function logPost(){
		
		$db =& DBUtil::getConnection();

		$query = "INSERT INTO user_post(
			remote_addr,
			php_self,
			http_user_agent,
			date_time
		) VALUES (
			'" . $_SERVER[ "REMOTE_ADDR" ] . "',
			'" . $_SERVER[ "PHP_SELF" ] . "',
			'" . $_SERVER[ "HTTP_USER_AGENT" ] . "',
			NOW()
		)";
		$rs = $db->Execute( $query );
		if( $rs === false )
			die( "Impossible d'enregistrer le post" );
			
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime les enregistrements dépassées des posts utilisateur
	 * @return void
	 */
	
	private function cleanUp(){
		
		$db =& DBUtil::getConnection();
		
		list( $year, $month, $day, $hour, $min, $sec ) = explode( "-", date( "Y-m-d-H-i-s" ) );
		
		$time_limit = date( "Y-m-d H:i:s", mktime( $hour - 1, $min, $sec, $month, $day,  $year ) );
		
		$query = "DELETE FROM user_post WHERE date_time < '$time_limit'";

		$rs = $db->Execute( $query );
		
		if( $rs === false )	
			die( "Impossible de mettre à jour les posts utilisateurs" );
		
	}
	
	//----------------------------------------------------------------------------------------------------------
	
	/**
	 * Avertit l'administrateur du site lorsqu'un nouvel utilisateur a été ajouté à la liste noire
	 * @return void
	 */
	
	private function report(){
	
		global 	$GLOBAL_START_PATH,
				$GLOBAL_START_URL,
				$GLOBAL_ADMIN_MAIL;
		
		include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );

		ob_start();
		
		?>
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html xmlns="http://www.w3.org/1999/xhtml">
			<head></head>
			<body>
				<p>Nouvel utilisateur ajouté à la liste noire</p>
			    <p>L'utilisateur suivant a enfreind les règles de sécurité définies sur le site <a href="<?php echo $GLOBAL_START_URL ?>"><?php echo htmlentities( DBUtil::getParameterAdmin( "ad_sitename" ) ) ?></a>. Tout accès au site lui sera désormais prohibé.</p>
			    <p><b>Adresse IP</b> : <?php echo $this->remote_addr ?></p>
			    <p><b>Vous pouvez, si vous le souhaitez, débloquer l'accès au site pour ce visiteur en cliquant sur le lien ci-dessous :</b></p>
			    <p class="center">
			    	<a href="<?php echo $GLOBAL_START_URL ?>/administration/secure/firewall.php?allow=<?php echo urlencode( $this->remote_addr ) ?>"><?php echo $GLOBAL_START_URL ?>/administration/secure/firewall.php?allow=<?php echo urlencode( $this->remote_addr ) ?></a>
			    </p>
			</body>
		</html>
	    <?php	
		
		$html = ob_get_contents();
		ob_end_clean();

		include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
		$mailer = new htmlMimeMail();
		
		$mailer->setHtml( $html );
		$mailer->setSubject( DBUtil::getParameterAdmin( "ad_company" ) . " : " . Dictionnary::translate( "remote_addr_black_listed" ) );
		$mailer->setFrom( "\"admin\" <$GLOBAL_ADMIN_MAIL>" );
		
		return $mailer->send( array( $GLOBAL_ADMIN_MAIL ) );
	
	}
	
	//----------------------------------------------------------------------------

	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------------------------------------
	
}

?>