<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion de la table parameter_admin
 */

include_once( dirname( __FILE__ ) . "/tableobject.php" );

class ParameterAdmin {
	
	/**
	 * Met à jour la table parameter_admin
	 * @static
	 * @param string $idParam l'id du paramètre à modifier (ex: 'sitemap_name')
	 * @param string $paramValue la valeur du nouveau paramètre
	 * @return void
	 */
	public static function updateParam($idParam, $paramValue) {
		$db = &DBUtil::getConnection();
		$query = "UPDATE parameter_admin
				  SET paramvalue = '".$paramValue."' 
				  WHERE idparameter = '$idParam'";		 
		$db->Execute($query);
	}
}

?>