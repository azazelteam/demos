<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object contacts fournisseurs services
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Contact.php" );


class ProviderContact extends DBObject implements Contact{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idprovider
	 * @param int $idcontact
	 */
	public function __construct( $idprovider, $idcontact = 1 ){
		
		parent::__construct( "provider_contact", false, "idprovider", $idprovider, "idcontact", $idcontact );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
	public function getId(){ return $this->recordSet[ "idcontact" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Duplique un contact
	 * @return ProviderContact
	 */
	public function duplicate(){
		
		$clone = self::create( $this->get( "idprovider" ) );
		
		foreach( $this->recordSet as $key => $value ){

			if( !in_array( $key, array_keys( $this->primaryKeys ) ) )
				$clone->set( $key, $value );
			
		}
		
		$clone->save();

		return $clone;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @param int $idprovider
	 * @return ProviderContact
	 */
	public static function create( $idprovider ){
	
		$rs =& DBUtil::query( "SELECT MAX( idcontact ) + 1 AS idcontact FROM provider_contact WHERE idprovider = '" . intval( $idprovider ) . "'" );
		
		$idcontact = $rs->fields( "idcontact" ) === NULL ? 1 : $rs->fields( "idcontact" );
		
		$query = "
		INSERT INTO `provider_contact` ( 
			`idprovider`, 
			`idcontact`
		) VALUES ( 
			'" . intval( $idprovider ) . "', 
			'$idcontact'
		)";
		
		if( DBUtil::query( $query ) === false ){
			
			trigger_error( "Impossible de créer le nouveau contact", E_USER_ERROR );
			exit();
			
		}
		
		return new ProviderContact( $idprovider, $idcontact );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
	public static function delete( $idprovider, $idcontact ){

		$contact = new ProviderContact( intval( $idprovider ), intval( $idcontact ) );
		
		if( !$contact->allowDelete() )
			return false;
			
		return DBUtil::query( "DELETE FROM provider_contact WHERE idprovider = '" . intval( $idprovider ) . "' AND idcontact = '" . intval( $idcontact ) . "' LIMIT 1" ) !== false;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* Contact implementation */
	
	public function getGender()			{ return $this->recordSet[ "idtitle" ]; }
	public function getFirstName()		{ return $this->recordSet[ "firstname" ]; }
	public function getLastName() 		{ return $this->recordSet[ "lastname" ]; }
	public function getFaxnumber() 		{ return $this->recordSet[ "faxnumber" ]; }
	public function getPhonenumber() 	{ return $this->recordSet[ "phonenumber" ]; }
	public function getGSM() 			{ return $this->recordSet[ "gsm" ]; }
	public function getEmail() 			{ return $this->recordSet[ "email" ]; }
	public function getComment() 		{ return $this->recordSet[ "comment" ]; }
	public function getDepartment()		{ return $this->recordSet[ "iddepartment" ]; }
	public function getFunction() 		{ return $this->recordSet[ "idfunction" ]; }
	public function getAddress() 		{ return $this->recordSet[ "address" ]; }
	public function getAddress2() 		{ return $this->recordSet[ "address2" ]; }
	public function getZipcode()		{ return $this->recordSet[ "zipcode" ]; }
	public function getCity()			{ return $this->recordSet[ "city" ]; }
	public function getState()			{ return $this->recordSet[ "idstate" ]; }
	public function allowDelete()		{ /* @todo */ return !$this->isMainContact(); }
	public function isMainContact() 	{ return DBUtil::query( "SELECT main_contact FROM provider WHERE idprovider = '" . $this->recordSet[ "idprovider" ]. "'" )->fields( "main_contact" ) == $this->getId(); }

	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>