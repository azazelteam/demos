<?php
/**
 * Permet de créer des arborescences génériques suivant un modèle implanté dans une bdd
 * 
 * @package Objets génériques
 * @subpackage Arborescences
 * @author Christophe Scholly
 */
include_once( dirname( __FILE__ ) . "/classes.php" );

class Arbo {
	
	private $tableName; //nom de la table associée à l'arborescence
	private $root; //element racine
	
	/**
	 *  Constructeur
	 * 
	 * @param String $tableName Nom de la table dans la bdd décrivant l'arborescence à construire
	 */ 
	public function __construct($tableName) {
		$this->tableName = $tableName;
		
		$this->arboArray = array();
		$this->root = array("id" => 0, "children" => array());
		$this->buildArbo($this->root);
		//$this->displayArbo( $this->root);
	}
	
	/**
	 *  Renvoi un tableau contenant les enfants du parent passé en paramètre
	 * 
	 * @param int $parent id du parent dont lequel on désire connaître les enfants
	 * @return array $childrenArray tableau généré d'enfants
	 */ 
	public function getChildrenOf($parent) {
		
		if( empty( $parent[ "id" ] ) )
			return array();
			
		$childrenArray = array();
		$sql = &DBUtil::getConnection();
		$query = "SELECT idarbo FROM ".$this->tableName." WHERE idarbo_parent = ".$parent['id']." ORDER BY display_order ASC";
		$children = $sql->Execute($query);
		
		//on ajoute chaque enfant au tableau d'enfants
		foreach($children as $child) {
			$childrenArray[] = $child['idarbo'];
		}
		
		return $childrenArray;
	}
	
	/**
	 *  Permet de connaître la nomenclature du noeud à l'aide de son identifiant
	 * 
	 * @param int $elementId id du noeud dont on souhaite connaître la nomenclature
	 * @return String $elementNomenclature le nom du noeud en question
	 */
	public function getNomenclatureOf($elementId) {
		$sql = &DBUtil::getConnection();
		$query = "SELECT ".$this->tableName."_1 FROM $this->tableName WHERE idarbo = ".$elementId;
		$result = $sql->Execute($query);
		$elementNomenclature = $result->Fields($this->tableName.'_1');
		
		return $elementNomenclature;
	}
	
	/**
	 * Construit l'arborescence en ajoutant les enfants à la racine
	 * 
	 * @param array &$currentArray référence sur le tableau que l'on ai en train de créer
	 * @return void
	 */
	public function buildArbo(&$currentArray) {
		if($children = $this->getChildrenOf($currentArray)) {
			foreach($children as $child) {
				$currentArray['children'][] = array('id' => $child['idarbo'], 'children' => array());
				$this->buildArbo($currentArray['children'][count($currentArray['children'])-1]);
			}
		}
	}
	
	/**
	 * Permet d'afficher l'arborescence
	 * 
	 * @param array $parent arborscence que l'on souhaite afficher 
	 * @param int $paddingLeft décalage vers la droite des éléments enfants 
	 * @return String arborescence sous forme de liste
	 */
	public function displayArbo() {
		return $this->showArbo($this->root);
	} 
	
	public function showArbo($parent) {
		$children = $parent['children']; //on n'affiche pas la racine
		static $return; 
		$return .= '<ul>';
		foreach( $children as $child ){
			$return .= '<li>'.$this->getNomenclatureOf($child['id']).'</li>';
			$this->showArbo($child);
		}
		$return .= '</ul>';
		return $return;
	}
};
?>