<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des images
 * Utilise les parmètres admin suivant :
 * 'img_quality' : qualité de l'image, facultatif, 70 par défaut
 * 'prod_img_record' : la taille à enregistrer dans la table product, facultatif, ex.: _zoom
 * 'cat_img_record' : la taille à enregistrer dans la table catagory, facultatif, ex.: _prod
 * Les tailles des images à créer, avec le préfixe 'C_img' pour les catégories et 'P_img' pour les produits
 * et le suffixe '_heigth' ou '_width' pour la hauteur et la largeur des images 
 * exemples : 
 * 'C_img_icone_heigth' : la hauteur de l'icône de la catégorie
 * 'P_img_zoom_width' : la largeur de l'image zoom pour le produit
 * 'P_img_prod_1_width', 'P_img_prod_1_heigth'
 */
 
include_once( dirname( __FILE__ ) . "/adm_base.php" );

class ADMIMAGE extends ADMBASE
{
	var $imgsrc; // chemin de l'image originale
	var $imgh; // hauteur de l'image originale
	var $imgw; // largeur de l'image originale
	var $imgtype; // type d'image (1 = GIF , 2 = JPG , 3 = PNG , 4 = SWF , 5 = PSD , 6 = BMP , 7 = TIFF)
	var $idp =0; // id du produit
	var $idc =0; // id de la catégorie
	var $imgstocreate = array(); // les images à créer...
	var $imgsfields = array(); // et les champs de la table correspondants
	var $directory; // le répertoire des images
	var $imagename; // nom de l'image source
	
	/**
	 * Contructeur
	 * @param int $idproduct Id du produit ( optionnel )
	 * @param int $idcat Id de la catégorie utilisé si $idproduct est null ( optionnel )
	 */
	function __construct($idproduct=0,$idcat=0)
	{
		global $GLOBAL_START_PATH;
		ADMBASE::ADMBASE();
		
		if($idproduct > 0 ) {
			$this->idp = $idproduct;
			$data = DBUtil::getConnection()->GetRow('SELECT imagedirectory,imagename FROM product WHERE idproduct='.$idproduct);
			//Paramètres produit
			$this->imgstocreate = array('_zoom','_prod','_prod_1','_icone','_prod_2');
			$this->imgsfields = array('_zoom'=>'zoomname','_prod'=>'imagename','_prod_1'=>'prodname_1','_icone'=>'iconname','_prod_2'=>'prodname_2');
		} elseif ( $idcat > 0) {
			$this->idc = $idcat;
			$data = DBUtil::getConnection()->GetRow('SELECT imagedirectory,imagename,image_menu,image_menu_roll FROM category WHERE idcategory='.$idcat);
			//Paramètres catégorie
			$this->imgstocreate = array('_prod','_icone','_desc','_menu','_menu_roll');
			$this->imgsfields = array('_prod'=>'imagename','_icone'=>'iconname','_desc'=>'imagedesc','_menu'=>'image_menu','_menu_roll'=>'image_menu_roll');
		} else $data = false;
		
		if($data) {
			$nameimage =  $data['imagename'];
			$this->directory = $GLOBAL_START_PATH .'/www'. $data['imagedirectory'].'/';
			if( eregi("(.*)(_prod|_prod_1|_icone|_zoom)(\.jpg)",$nameimage, $regs) )  $this->imagename = $regs[1] . $regs[3] ; // récup nom original
			else $this->imagename = $nameimage;
		}
	}
	
	/**
	 * Création des images icone, zoom, ...
	 * selon les paramères de la table parameter_admin
	 * @return string un rapport sur la création des images
	 */
	public function ResizeImages()
	{
		$Result='';
		if($this->imagename == '') return '';
		// Le chemin de l'image de base dans $imgsrc
		$this->imgsrc = $this->directory . $this->imagename ;
		// test si fichier existe
		if (!is_file($this->imgsrc)) { 
	    	$this->imgsrc = strtolower($this->imgsrc);
			if (!is_file($this->imgsrc)) return 'Le fichier '. $this->imgsrc . " n'existe pas!"; 
		}
		
		if (!is_writable($this->directory))  return 'Le répertoire '. $this->directory . " n'est pas accessible en écriture!"; 
		// Recherche la taille de l'image 
		$this->mygetimagesize();
		
		// Recherche qualité
		$quality = 100;
		
		// Taille à enregister dans la base
		if( $this->idc > 0 ) $baseimg = $this->GetParameterAdmin('cat_img_record'); 
		else $baseimg = $this->GetParameterAdmin('prod_img_record');
		
		// Création des noms des images
		if( !eregi("(.*)(\.jpg)",$this->imagename, $regs) ) return "L'image source n'a pas l'extention .jpg!";

		foreach($this->imgstocreate as $v) {
			$destname = $regs[1] . $v .  $regs[2];
			$imgdest = $this->directory . $destname;
			// $lastmodif = filemtime($imgdest);
			if( $this->idc > 0 ) {
				$heigth = $this->GetParameterAdmin('C_img'.$v.'_height');
				$width = $this->GetParameterAdmin('C_img'.$v.'_width');
			} else {
				$heigth = $this->GetParameterAdmin('P_img'.$v.'_height');
				$width = $this->GetParameterAdmin('P_img'.$v.'_width');
			}
			$imgdest = $this->directory . $regs[1] .$v.  $regs[2] ;
			$res= '';
			if( $heigth > 0 AND $width > 0 ) $res = $this->copyimg($imgdest,$width,$heigth,$quality);
	    	else if( $width > 0 )$res =  $this->resizetowidth($imgdest,$width,$quality);
	    	else if( $heigth > 0 ) $res = $this->resizetoheigth($imgdest,$heigth,$quality);
	    	$Result .= $res;
	    	if( $heigth > 0 OR $width > 0 ) {
		    	// recherche la taille de la nouvelle image
		    	$newsize  = @getimagesize( $imgdest);
		    	if($baseimg == $v ) $storesize= true; else $storesize=false;
		    	if($newsize) $this->storedb($v,$destname,$newsize,$storesize);
		    	else $Result .= ' Erreur sur '.$imgdest;
	    	}
		}
	
	 return $Result ; 
	}

	/**
	 * Mets à jour la taille des images dans la base de données
	 * @access private
	 * @param string $field ???
	 * @param string $destname ???
	 * @param $newsize ???
	 * @param bool $storesize ???
	 * @return void ???
	 */
	private function storedb($field, $destname, array $newsize, $storesize)
	{
		$sql = $this->imgsfields[$field] ."='$destname'";
		if($storesize) $sql .= ',imagewidth='.$newsize[0].', imageheight='.$newsize[1];
		
		if( $this->idp > 0 ) {
			$SQL_Query = 'UPDATE product SET '.$sql.' WHERE idproduct='.$this->idp;
			$sqlres = DBUtil::getConnection()->Execute($SQL_Query);
			if($sqlres == false) return "ERREUR SQL $SQL_Query ";
			$SQL_Query = 'UPDATE product SET '.$sql.' WHERE idproduct='.$this->idp;
			$sqlres = DBUtil::getConnection()->Execute($SQL_Query);
			if($sqlres == false) return "ERREUR SQL $SQL_Query ";
		}
		
		if( $this->idc > 0 ) {
			$SQL_Query = 'UPDATE category SET '.$sql.' WHERE idcategory='.$this->idc;
			$sqlres = DBUtil::getConnection()->Execute($SQL_Query);
			if($sqlres == false) return  "ERREUR SQL $SQL_Query ";
		}
	}

 
	/**
	 * resize a jpeg image to a fixed size
	 * the new image will have exactely the given size with a background of the topleft pixel color
	 * the original image is resized copied and centered in the new image.
	 * @param int $quality jpeg quality between 0 and 100 , defaults to 70
	 * @return string une chaîne vide qui sert à rien
	 */
	private function copyimg( $imgdest, $width,$heigth,$quality=100 )
	{
		$ratew = $width / $this->imgw ;
		$rateh = $heigth / $this->imgh ;
		$rate = min($ratew,$rateh);
		 
		$imgsrc = imagecreatefromjpeg( $imgsrc ); 
		$rgb = ImageColorAt($this->imgsrc, 0, 0);
		$r = ($rgb >> 16) & 0xFF;
		$g = ($rgb >> 8) & 0xFF;
		$b = $rgb & 0xFF;
		// create new image
		$w = round($this->imgw*$rate);
		$h = round($this->imgh*$rate);
		$xoffset = round(($width-$w)/2);
		$yoffset = round(($heigth-$h)/2);
		$newimg = imagecreateTrueColor($width,$heigth);
		$white = imagecolorallocate($newimg, $r,$g,$b);
		 
		imagefill($newimg,0,0,$white);
		imagecopyresampled($newimg,$this->imgsrc,$xoffset,$yoffset,0,0,$w,$h,$this->imgw,$this->imgh);
		imagejpeg($newimg, $imgdest ,$quality);
		imagecolordeallocate($newimg, $white);
		imagedestroy($imgsrc);
		imagedestroy($newimg);
		chmod ($imgdest, 0666);
		return '';
	}	
	
	/**
	 * resize to a max width
	 * @param string $imgdest le chemin de l'image
	 * @param int $width la largeur de rééchantillonnage
	 * @param int $quality la qualité de rééchantillonnage ( optionnel, 70 par défaut )
	 * @return string une chaîne vide ou un message d'erreur
	 */
	private function resizetowidth( $imgdest, $width, $quality=100 )
	{
	  $rate = $width / $this->imgw ;
	  $res = $this->resizeimg($imgdest,$rate,$quality);
	  chmod ($imgdest, 0666);
	  return $res;
	}
	
	/**
	 * resize to a max heigth
	 * @param string $imgdest le chemin de l'image
	 * @param int $heigth la hauteur de rééchantillonnage
	 * @param int $quality la qualité de rééchantillonnage ( optionnel, 70 par défaut )
	 * @return string une chaîne vide ou un message d'erreur
	 */
	private function resizetoheigth( $imgdest,$heigth, $quality=100)
	{
	  $rate = $heigth / $this->imgh ;
	  $res = $this->resizeimg($imgdest,$rate,$quality);
	  chmod ($imgdest, 0666);
	  return $res;
	}
	
	/**
	 * Récupère le type, la hauteur et la largeur de l'image
	 * Array ( [0] => 519, [1] => 228, [2] => 3, [3] => width="519" height="228", [bits] => 8, [mime] => image/png )
	 * @return true en cas de succès, sinon false
	 */
	private function mygetimagesize()
	{
		$size  = @getimagesize( $this->imgsrc );
		if( $size ) {
			$this->imgw = 0 + $size[0];
			$this->imgh= 0 + $size[1] ;
			$this->imgtype = 0 + $size[2] ;
			}
		else return false;
		return true ;
	}
	
	/**
	 * generic resize function
	 * @param string $imgdest le chemin de l'image
	 * @param float $rate le taux de redimensionnement
	 * @param int $quality la qualité de rééchantillonnage ( optionnel, 70 par défaut )
	 * @return une chaîne vide ou un message d'erreur
	 */
	private function resizeimg( $imgdest,$rate, $quality=100)
	{
	// type : 1 = GIF , 2 = JPG , 3 = PNG , 4 = SWF , 5 = PSD , 6 = BMP , 7 = TIFF
		$imgsrc = false;
		$res ='';
		 switch($this->imgtype)
		 {
		 case 1 : $imgsrc = imagecreatefromgif($this->imgsrc); break;
		 case 2 : $imgsrc = imagecreatefromjpeg($this->imgsrc); break;
		 case 3 : $imgsrc = imagecreatefrompng($this->imgsrc); break;
		 case 6 : $imgsrc = imagecreatefromwbmp($this->imgsrc); break;
		 default : $res = 'image format not supported'; break;
		 }
		if($imgsrc)
		{ // create new image
		 $w = round($this->imgw*$rate); 
		 $h = round($this->imgh*$rate);
		 $newimg = imagecreateTrueColor($w,$h);
		 imagecopyresampled($newimg,$imgsrc,0,0,0,0,$w,$h,$this->imgw,$this->imgh);
		 
		 $fout = $this->getimgformat($imgdest);
		 switch($fout)
		 {
		 case 1 : imagegif($newimg, $imgdest ); break;
		 case 2 : imagejpeg($newimg, $imgdest ,$quality); break;
		 case 3 : imagepng($newimg, $imgdest ); break;
		 case 6 : imagewbmp($newimg, $imgdest ); break;
		 default : $res = 'image format not supported'; break;
		 }
		 imagedestroy($imgsrc);
		 imagedestroy($newimg);
		}
		else $res = "ERREUR de copie.";
		return $res;
	}
	
	/**
	 * get the image format by file extention
	 * @param string $imgdest le chemin de l'image
	 * @return int : 1 = GIF, 2 = JPG/JPEG, 3 = PNG, 6 = BMP, 0 = non géré
	 */
	private function getimgformat($imgdest)
	{
		$path_parts = pathinfo($imgdest);
		/*
		echo "<br />";
		echo $path_parts['dirname'], "<br />\n";
		echo $path_parts['basename'], "<br />\n";
		echo $path_parts['extension'], "<br />\n"; 
		*/
		$fileext = strtoupper($path_parts['extension']);
	
	  if( $fileext == 'JPG' ) return 2;
	  if( $fileext == 'JPEG' ) return 2;
	  if( $fileext == 'PNG' ) return 3;
	  if( $fileext == 'GIF' ) return 1;
	  if( $fileext == 'BMP' ) return 6;
	  return 0; 
	}	
 	
}
?>