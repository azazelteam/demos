<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion de la pagination
 */
 
include_once("classes.php");

/**
 * Créé un lien 'GET' vers une page
 */
function CreateLinkToPage($ScriptName, $ParameterArray)
{
        $Link = "$ScriptName?";
        if ( is_array($ParameterArray) )
        {
         reset($ParameterArray);
          while (list($ParmName, $ParmValue) = each($ParameterArray))
          {
                $Link .= "$ParmName=$ParmValue&";
          }
        }
        return ($Link);
}

/**
 * Créé un lien 'POST' vers une page
 */
function CreateFormToPage(&$ScriptName, $Image, &$ParameterArray, $align='right')
{
        echo "<form action=\"$ScriptName\" method=\"post\" style=\"text-align:$align;\" >\n";

        if ( is_array($ParameterArray) )
        {
        reset($ParameterArray);
          while (list($ParmName, $ParmValue) = each($ParameterArray))
          {
                        echo "<input type=hidden name=$ParmName value=\"$ParmValue\">\n";
          }
        }
        echo "<input type=image border=0 src=\"$Image\" >\n";
        echo "</form>\n";
}

/**
 * Affiche une séquence de pages
 */
function CreatePageSeq(&$ScriptName,$deb,$fin,$CurPageNumber,&$ParameterArray )
{
	global $GLOBAL_START_URL;
	
   if($fin > 1 ) echo 'pages '; else { echo "page <u>$CurPageNumber</u>"; return; }
   for($i=$deb;$i <= $fin; $i++ )
   {
        if($i > $deb ) echo ' - ';
        if($i ==  $CurPageNumber ) echo "<u>$i</u>";
        else
        {
        echo "<a href=\"$GLOBAL_START_URL/catalog/$ScriptName" ;
          if ( is_array($ParameterArray) )
          {
                reset($ParameterArray);
                echo '?';
                while (list($ParmName, $ParmValue) = each($ParameterArray))
                {
                if( $ParmName == 'CurrentPageNumberP' ) $ParmValue =  $i ;
                if( $ParmName == 'CurrentPageNumberC' ) $ParmValue =  $i ;
                echo  "$ParmName=$ParmValue&";
                }
          }
        echo "\" >$i</a>\n";
        }
    }
}

/**
 * Affiche la pagination
 */
function Paginate(&$ScriptName,$NbAll,$NbByPages,$CurrentPageNumber,&$ParameterOutputArray)
{
global $Category;
if( $NbAll > $NbByPages ) {
	?>
	<table bgcolor="red" border="1" style="width:100%; " >
	<tr>
	<td style="width:40%; text-align:right;" >
	<?php
	if ($CurrentPageNumber > 1)
		{	
		$ParameterOutputArray['CurrentPageNumberP']	= $CurrentPageNumber-1;
		CreateFormToPage($ScriptName,Dictionnary::translate('PreviousButton'), $ParameterOutputArray,'right');
		$ParameterOutputArray['CurrentPageNumberP']=$CurrentPageNumber;
		}
	?>	
	</td><td style="width:20%; text-align:center; border: 1px solid #DDDDDD;">	
	<?php
		CreatePageSeq($ScriptName,1,ceil($NbAll / $NbByPages ),$CurrentPageNumber,$ParameterOutputArray );
	?>	
	</td><td style="width:40%; text-align:left;">	
	<?php	
		if ( $Category->MoreProdPages() )
		{	
		$ParameterOutputArray['CurrentPageNumberP']	= $CurrentPageNumber+1;
		CreateFormToPage($ScriptName,Dictionnary::translate('NextButton'), $ParameterOutputArray,'left');
		$ParameterOutputArray['CurrentPageNumberP']=$CurrentPageNumber;
		}
	?>
	</td></tr>
	</table>
	<?php
	}

}

?>