<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object devis entêtes + lignes
 */

include_once( dirname( __FILE__ ) . "/OnCharterSale.php" );
include_once( dirname( __FILE__ ) . "/EstimateItem.php" );
include_once( dirname( __FILE__ ) . "/Customer.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );
include_once( dirname( __FILE__ ) . "/InvoiceAddress.php" );
include_once( dirname( __FILE__ ) . "/ForwardingAddress.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . '/GroupingCatalog.php');

/**
 * Devis
 * @use OnCharterSale implémentation des frais de port fournisseur
 * @use Delivery implémentation du mode de livraison
 * @use Invoicing implémentation du mode de facturation
 */
final class Estimate extends OnCharterSale{
	
   //----------------------------------------------------------------------------
	//static members
	
	public static $STATUS_ORDERED 				= "Ordered";
	public static $STATUS_CANCELLED 			= "Cancelled";
	public static $STATUS_SENT 					= "Send";
	public static $STATUS_TODO 					= "ToDo";
	public static $STATUS_IN_PROGRESS 			= "InProgress";
	public static $STATUS_REFUSED 				= "Refused";
	public static $STATUS_DEMAND_IN_PROGRESS	= "DemandInProgress";

	//----------------------------------------------------------------------------
    
    //@todo PHP 5.3 late state binding( protect static $TABLE_NAME, ... )
	protected $TABLE_NAME 			= "estimate";
	protected $ITEMS_TABLE_NAME		= "estimate_row";
	protected $PRIMARY_KEY_NAME 	= "idestimate";
	
	//----------------------------------------------------------------------------	
	/**
	 * @var int $idestimate le numéro du devis
	 * @access private
	 */
	private $idestimate;
	
	//----------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param int $idestimate le numéro du devis
	 */
	function __construct( $idestimate ){
		
		//@todo PHP 5.3 late state binding( protect static $TABLE_NAME, ... )
		$this->TABLE_NAME 			= "estimate";
		$this->ITEMS_TABLE_NAME 	= "estimate_row";
		$this->PRIMARY_KEY_NAME 	= "idestimate";
	
		$this->idestimate =  $idestimate;

		DBObject::__construct( "estimate", false, "idestimate",  $idestimate );	
		
		$this->setPriceStrategy();
		$this->setChargesStrategy();
		
		$this->setItems();
		
		$this->salesman = new Salesman( $this->recordSet[ "iduser" ] );
		$this->customer = new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );
		$this->forwardingAddress = $this->recordSet[ "iddelivery" ] ? new ForwardingAddress( $this->recordSet[ "iddelivery" ] ) : $this->customer->getAddress();
		$this->invoiceAddress = $this->recordSet[ "idbilling_adress" ] ? new InvoiceAddress( $this->recordSet[ "idbilling_adress" ] ) : $this->customer->getAddress();	
			
	}
		
	//----------------------------------------------------------------------------
	/**
     * @access public
     * @return int
     */
    public function getId(){ return $this->idestimate; }
    
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @return bool
	 */
	private function hasFrancoAvailable(){
		
		$totalET = 0.0; 

		$francoAvailable = true;
		$it = $this->items->iterator();
		
		while( $it->hasNext() )
			if( !$it->next()->get( "franco" ) )
				return false;

		return true;

	}
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * récupère les lignes articles
	 * @access private
	 * @return void
	 */
	private function setItems(){

		$this->items = new ArrayList( "TradeItem" );
		
		$rs =& DBUtil::query( "SELECT idrow FROM estimate_row WHERE idestimate = '{$this->idestimate}' ORDER BY idrow ASC" );

		while( !$rs->EOF() ){
		
			$this->items->add( new EstimateItem( $this, $rs->fields( "idrow" ) ) );
				
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @param CArticle $article
	 * @param int $quantity
	 * @return void
	 */
	public function addArticle( CArticle $article, $quantity = 1 ){
		
		$item = $this->createItem();
		
		/* attention aux produits spé et alternatifs */
		
		if( $article->get( "type" ) == "specific" )	
				$idcategory = ( $ret = DBUtil::getDBValue( "idcategory", "spe_categories", "idarticle", $article->getId() ) ) ? $ret : 0;
		else 	$idcategory = $article->get( "idproduct" ) ? DBUtil::getDBValue( "idcategory", "product", "idproduct", $article->get( "idproduct" ) ) : 0;
		
		$ecotaxe_code =  $article->get( "ecotaxe_code" );
		$rs1 =& DBUtil::query( "SELECT amount FROM ecotaxe WHERE code = '$ecotaxe_code'" );
		
		$ecotaxe_amount = $rs1->fields( "amount" ) * intval( $quantity );
		
		$idbuyer = $this->get('idbuyer');
		$idgrouing=GroupingCatalog::getBuyerGrouping($idbuyer);
		$date=date("Y-m-d");
        $group_price=0.00;

       $rs1 = DBUtil::query( "SELECT price from  group_price
        						WHERE reference = '" . $article->get( "reference" )."' and idgrouping=".$idgrouing." 
        						 ");
       
        		if($rs1->RecordCount()>0)

        		{

        			$group_price=$rs1->fields("price");
        		}

        		//$discount_price=($price!=0.00)?$price:Util::numberFormat( $Order->getItemAt( $i)->get( "discount_price" ) );

        		
        		//$discount_price=($price!=0.00)?$price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceATI(), 2 );
if( B2B_STRATEGY ){
			
			$unit_price=($group_price!=0.00)?$group_price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceET(), 2 );
		
		}
		else{
			$unit_price=($group_price!=0.00)?$group_price:round( $article->get( "sellingcost" ) + $article->getOptionsPriceATI(), 2 );

			
		}




		$item->set( "idarticle", 		$article->getId() );
		$item->set( "quantity", 		intval( $quantity ) );
		$item->set( "reference", 		$article->get( "reference" ) );
		$item->set( "reference_base", 	$article->get( "reference" ) );
		$item->set( "ref_supplier", 	$article->get( "ref_supplier" ) );
		$item->set( "n_article", 		$article->get( "n_article" ) );
		$item->set( "gencod", 			$article->get( "gencod" ) );
		$item->set( "code_customhouse", $article->get( "code_customhouse" ) );
		$item->set( "ecotaxe_code", 	$article->get( "ecotaxe_code" ) );
		$item->set( "ecotaxe_amount", 	$ecotaxe_amount );
		$item->set( "idsupplier", 		$article->get( "idproduct" ) ? DBUtil::getDBValue( "idsupplier", "product", "idproduct", $article->get( "idproduct" ) ) : $article->get( "idsupplier" ) ); /* attention aux produits spé et alternatifs */
		$item->set( "unit", 			$article->get( "unit" ) );
		$item->set( "lot", 				$article->get( "lot" ) );
		$item->set( "designation", 		"<b>" . $article->get( "summary_1" ) ."</b><br />" . $article->get( "designation_1" ) );
		$item->set( "summary", 			$article->get( "summary_1" ) );
		$item->set( "idcategory", 		$idcategory );
		$item->set( "delivdelay", 		$article->get( "delivdelay" ) );
		$item->set( "usepdf", 			1 );
		$item->set( "weight", 			$article->get( "weight" ) );
		$item->set( "idtissus", 		$article->getCloth() ? $article->getCloth()->getId() : 0 );
		$item->set( "idcolor", 			$article->getColor() ? $article->getColor()->getId() : 0 );
		$item->set( "min_cde", 			$article->get( "min_cde" ) );
		$item->set( "doc", 				$article->get( "doc" ) );
		$item->set( "hasPromo", 		$article->getPromotion() !== false ? 1 : 0 );
		$item->set( "isDestocking", 	$article->getDestocking() !== false ? 1 : 0 );
		$item->set( "vat_rate", 		$article->getVATRate() );
		
		/* quantité externe : bof */
	
		if( $article->get( "idsupplier" ) == DBUtil::getParameterAdmin( "internal_supplier" ) )	 	
			$item->set( "external_quantity", 0 );
		else if( $article->get( "stock_level" ) == -1 )
			$item->set( "external_quantity", intval( $quantity ) );
		else if( $article->get( "stock_level" ) < intval( $quantity ) )
			$item->set( "external_quantity", intval( $quantity ) - $article->get( "stock_level" ) );
		else $item->set( "external_quantity", 0 );
	
		/* prix de vente */
		
		if( B2B_STRATEGY ){
			
			$item->set( "unit_price", 			$unit_price);
			$item->set( "discount_price", 		round( $article->getDiscountPriceET( $this->customer->getId(), $item->get( "quantity" ) ), 2 ) );
		
		}
		else{
			
			$item->set( "unit_price", 			$unit_price );
			$item->set( "discount_price", 		round( $article->getDiscountPriceATI( $this->customer->getId(), $item->get( "quantity" ) ), 2 ) );
			
		}
		
		$item->set( "ref_discount",  		$article->getDiscountRate( $this->customer->getId(), $item->get( "quantity" ) ) );
		$item->set( "ref_discount_base", 	$article->getDiscountRate( $this->customer->getId(), $item->get( "quantity" ) ) );
			
		/* prix d'achat */
		
		$item->set( "unit_cost", 			round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "unit_cost_amount", 	round( $article->get( "buyingcost" ), 2 ) );
		$item->set( "unit_cost_rate", 		0.0 );
		
		$item->set( "quantity_per_linear_meter", $article->get( "quantity_per_linear_meter" ) );
		$item->set( "quantity_per_truck", 		$article->get( "quantity_per_truck" ) );
		$item->set( "quantity_per_pallet", 		$article->get( "quantity_per_pallet" ) );
	
		$this->save();
		
	}

	//----------------------------------------------------------------------------
	/**
     * @access protected
     * @return EstimateItem une ligne article éditable
     */
	protected function createItem(){
		
		$idrow = $this->items->size() + 1;
		
		DBUtil::query( "INSERT INTO estimate_row( idestimate, idrow ) VALUES( '{$this->idestimate}', '$idrow' )" );

		$this->items->add( new EstimateItem( $this, $idrow ) );
	
		return $this->items->get( $this->items->size() - 1 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Supprime le devis dont le numéro est donné
	 * @static
	 * @param int $idestimate l'identifiant du devis à supprimer
	 * @return bool
	 */
	public static function delete( $idestimate ){

		DBUtil::query( "DELETE FROM `estimate_charges` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `contact_send` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `documentation_automailstory` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate_aknowledgement_of_receipt` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate_comments` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate_relaunch` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate_reminder` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `logismarket_categories` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `tender_history` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate_row_product_document` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate_row` WHERE `idestimate` = '" .  $idestimate  . "'" );
		DBUtil::query( "DELETE FROM `estimate` WHERE `idestimate` = '" .  $idestimate  . "' LIMIT 1" );
				
		return true;
		
	}
	//------------------------------------------------------------------------------------------------------------------------------------
	
	
	public function addEstimateItem( EstimateItem &$estimateItem ){

		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$this->addArticle( new CArticle(
		
				$estimateItem->get( "idarticle" ),
				$estimateItem->get( "idcolor" ),
				$estimateItem->get( "idtissus" )
				
			), $estimateItem->get( "quantity" )
			
		);
		
		$eItem = $this->items->get( $this->items->size() - 1 );
		
		$eItem->set( "unit_price", 				$estimateItem->get( "unit_price" ) );
		$eItem->set( "idrow_line", 				$estimateItem->get( "idrow_line" ) );
		$eItem->set( "n_article", 				$estimateItem->get( "n_article" ) );
		$eItem->set( "designation", 			$estimateItem->get( "designation" ) );
		$eItem->set( "summary", 				$estimateItem->get( "summary" ) );
		$eItem->set( "unit_cost", 				$estimateItem->get( "unit_cost" ) );
		$eItem->set( "unit_cost_rate", 			$estimateItem->get( "unit_cost_rate" ) );
		$eItem->set( "unit_cost_amount", 		$estimateItem->get( "unit_cost_amount" ) );
		$eItem->set( "ref_discount", 			$estimateItem->get( "ref_discount" ) );
		$eItem->set( "ref_discount_base", 		$estimateItem->get( "ref_discount_base" ) );
		$eItem->set( "ref_discount_add", 		$estimateItem->get( "ref_discount_add" ) );
		$eItem->set( "discount_price", 			$estimateItem->get( "discount_price" ) );
		$eItem->set( "delivdelay", 				$estimateItem->get( "delivdelay" ) );
		$eItem->set( "alternative", 			$estimateItem->get( "alternative" ) );
		$eItem->set( "doc", 					$estimateItem->get( "doc" ) );
		$eItem->set( "useimg", 					$estimateItem->get( "useimg" ) );
		$eItem->set( "external_quantity", 		$estimateItem->get( "external_quantity" ) );
		$eItem->set( "hasPromo", 				$estimateItem->get( "hasPromo" ) );
		$eItem->set( "isDestocking", 			$estimateItem->get( "isDestocking" ) );
		$eItem->set( "useimg", 					$estimateItem->get( "useimg" ) );
		$eItem->set( "franco", 					$estimateItem->get( "franco" ) );
		
		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Clone le devis
	 * @access public
	 * @return Estimate une copie du devis
	 */
	public function duplicate(){
		
		$idbuyer 			= $this->recordSet[ "idbuyer" ];
		$idcontact 			= $this->recordSet[ "idcontact" ];
		$iduser 			= User::getInstance()->getId();
		$iddelivery 		= $this->recordSet[ "iddelivery" ];
		$idbilling_adress 	= $this->recordSet[ "idbilling_adress" ];
		
		//création
        
		$idracine  = explode('-',$this->idestimate)[0];       
		$estimate = TradeFactory::createEstimate( $idbuyer, $idcontact, $idracine );

		//adresse de livraison
		
		$forwardingAddress = $iddelivery ? new ForwardingAddress( $iddelivery ) : $this->customer->getAddress();
		$estimate->setForwardingAddress( $forwardingAddress );
		
		//adresse de facturation
		
		$invoiceAddress = $idbilling_adress ? new InvoiceAddress( $idbilling_adress ) : $this->customer->getAddress();
		$estimate->setInvoiceAddress( $invoiceAddress );
		
		//lignes articles
			$this->recordSet[ "total_charge_ht" ] 	= $this->getChargesET();
		$this->recordSet[ "total_charge" ] 		= $this->getChargesATI();
		$idrow = 0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
		//var_dump($estimate->items->add( $it->next()->duplicate( $estimate, ++$idrow ) ));exit;
			$estimate->addEstimateItem( $it->next()->duplicate( $estimate, ++$idrow ) );
			
		
		
		
		
		
		$estimate->set( "status", self::$STATUS_TODO );
		$estimate->set( "title_offer", 			$this->recordSet[ "title_offer" ] );
		$estimate->set( "total_charge", 		$this->recordSet[ "total_charge" ] );
		$estimate->set( "total_charge_ht", 		$this->recordSet[ "total_charge_ht" ] );
		$estimate->set( "no_tax_export", 		$this->recordSet[ "no_tax_export" ] );
		$estimate->set( "billing_rate", 		$this->recordSet[ "billing_rate" ] );
		$estimate->set( "billing_amount", 		$this->recordSet[ "billing_amount" ] );
		$estimate->set( "down_payment_type",	$this->recordSet[ "down_payment_type" ] );
		$estimate->set( "down_payment_value", 	$this->recordSet[ "down_payment_value" ] );
		$estimate->set( "total_charge_auto", 	$this->recordSet[ "total_charge_auto" ] );
		$estimate->set( "charge_auto", 			$this->recordSet[ "charge_auto" ] );
		
		$estimate->save();
		
		return $estimate;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * @param Customer $customer
	 * @return void
	 */
	public function setCustomer( Customer $customer ){
	
		$this->recordSet[ "idbuyer" ] 			= $customer->getId();
		$this->recordSet[ "idcontact" ] 		= $customer->getContact()->getId();
		$this->recordSet[ "iddelivery" ] 		= 0;
		$this->recordSet[ "idbilling_adress" ] 	= 0;
		$this->recordSet[ "factor" ] 			= DBUtil::getDBValue( "factor", "buyer", "idbuyer", $customer->getId() );
		
		$this->forwardingAddress 			= $customer->getAddress();
		$this->invoiceAddress 				= $customer->getAddress();
		$this->customer 					= $customer;

		//escompte automatique
		
		$this->set( "rebate_type", 	"rate" );
		$this->set( "rebate_value", 	max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $customer->getId() ) ) );

		//recalcul des montants
		
		$this->recalculateAmounts();

	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * Substitue une ligne article par une autre ligne article
	 * Méthode à modifier également dans Order.php
	 * @param int $idrow la ligne article à substituer
	 * @param int $substituteIdRow la ligne article utilisée pour la substitution
	 * @param bool $deleteSubstitute booléen, supprime la ligne article utilisée pour la substitution si true, sinon la ligne article est conservée
	 */
	function substituteRowByRow( $idrow, $substituteIdRow, $deleteSubstitute = true ){
		
		$this->getItemAt( $idrow)->set("ref_replace", 			$this->getItemAt( $substituteIdRow)->get( "reference" ) );
		$this->getItemAt( $idrow)->set("ref_base_replace", 		$this->getItemAt( $substituteIdRow)->get( "reference_base" ) );
		$this->getItemAt( $idrow)->set("ref_supplier_replace", 	$this->getItemAt( $substituteIdRow)->get( "ref_supplier" ) );
		$this->getItemAt( $idrow)->set("alternative_replace", 	$this->getItemAt( $substituteIdRow)->get( "alternative" ) );
			
		$substituableFields = array(
		
			"unit_price",
			"idsupplier",
			"ref_supplier",
			"unit_cost",
			"unit_cost_rate",
			"unit_cost_amount",
			"ref_discount",
			"discount_price",
			"delivdelay",
			"vat_rate",
			"weight",
			"idtissus",
			"idcolor",
			"min_cde",
			"stock",
			"hasPromo",
			"isDestocking"

		);
		
		foreach( $substituableFields as $substituableField )
			$this->getItemAt( $idrow)->set( $substituableField, $this->getItemAt( $substituteIdRow)->get( $substituableField ) );
			
		if( $deleteSubstitute )
			$this->removeItemAt( $substituteIdRow );
			
		$this->recalculateAmounts();

	}
	
	//------------------------------------------------------------------------------------------------------------
	/**
	 * Substitue une ligne article par une autre référence
	 * Méthode à modifier également dans Order.php
	 * @param int $idrow la ligne article à substituer
	 * @param string $reference la référence du substitut
	 * @return void
	 */
	public function substituteRowByReference( $idrow, $reference ){
		
		$quantity 			= $this->getItemAt( $idrow)->get( "quantity" );
		$substituteIdRow 	= $this->items->size();
		
		//référence catalogue
		
		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		
		$this->addArticle( new CArticle( DBUtil::getDBValue( "idarticle", "detail", "reference", $reference ) ), $quantity );
		$this->substituteRowByRow( $idrow, $substituteIdRow, true );

	}

	//------------------------------------------------------------------------------------------------------------
	/**
	 * Assurance crédit
	 * @param bool $insure
	 * @access public
	 * @return bool true si le devis a pû être assuré, sinon false 
	 */
	public function insure( $insure = true ){
		
		$this->set( "insurance_auto", 0 );
		
		/* désactivation */
		
		if( !$insure ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* assurance crédit non modifiable */
		
		if( in_array( $this->recordSet[ "status" ], array(
			
			self::$STATUS_REFUSED,
			self::$STATUS_ORDERED
	
		) ) )
			return $this->recordSet[ "insurance" ] == 1;
		
		/* pas d'assurance crédit pour les paiements comptant */
						
		if( $this->recordSet[ "cash_payment" ] ){
			
			$this->set( "insurance", 0 );
			
			return false;
				
		}

		$idcredit_insurance = intval( DBUtil::getParameterAdmin( "idcredit_insurance" ) );
	
		/* assurance crédit désactivée */
		
		if( !$idcredit_insurance ){
			
			$this->set( "insurance", 0 );
			
			return false;
				
		}
		
		$creditInsurance	= DBUtil::stdclassFromDB( "credit_insurance", "idcredit_insurance", $idcredit_insurance );
		$customer 			= new Customer( $this->recordSet[ "idbuyer" ], $this->recordSet[ "idcontact" ] );
		$insurance 			= $this->recordSet[ "insurance" ];
		$minimum 			= floatval( $creditInsurance->minimum );
		$maximum 			= floatval( $creditInsurance->maximum );
		$netBill 			= $this->getNetBill();
		$netBillET 			= round( $netBill / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
		$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();
		
		/* client réfusé ou non assuré */
	
		if( $customer->get( "insurance_status" ) != "Accepted" || !$customer->get( "insurance" ) ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* montant non couvert */
	
		if( $netBillET < $minimum || $netBillET > $maximum ){
		
			$this->set( "insurance", 0 );
			
			return false;
			
		}
		
		/* encours maximum atteint */
	
		if( $outstandingCredits + $netBillET > $customer->get( "insurance_amount" ) ){
			
			$this->set( "insurance", 0 );
			
			return false;
			
		}
	
		/* devis assurable */
		
		$this->set( "insurance", 1 );
		
		return true;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#getChargesET()
	 */
	public function getChargesET(){

		if( in_array( $this->recordSet[ "status" ], array( 
		
				self::$STATUS_ORDERED, 
				self::$STATUS_CANCELLED, 
				self::$STATUS_SENT, 
				self::$STATUS_REFUSED 
				
		) ) || !$this->recordSet[ "total_charge_auto" ] )
		return B2B_STRATEGY ? $this->recordSet[ "total_charge_ht" ] : round( $this->recordSet[ "total_charge" ] / ( 1.0 + $this->getVATRate() / 100.0 ), 2 );
				
		//return $this->chargesStrategy->getChargesET();
        return 0.0;
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#getChargesATI()
	 */
	public function getChargesATI(){

		if( in_array( $this->recordSet[ "status" ], array( 
		
				self::$STATUS_ORDERED, 
				self::$STATUS_CANCELLED, 
				self::$STATUS_SENT, 
				self::$STATUS_REFUSED 
				
		) ) || !$this->recordSet[ "total_charge_auto" ] )
		return B2B_STRATEGY ? round( $this->recordSet[ "total_charge_ht" ] * ( 1.0 + $this->getVATRate() / 100.0 ), 2 ) : $this->recordSet[ "total_charge" ];
			
		//return $this->chargesStrategy->getChargesATI();
		return 0.0;
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getNetMarginAmount()
	 */
	public function getNetMarginAmount(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getEstimateNetMarginAmount( $this->idestimate );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getNetMarginRate()
	 */
	public function getNetMarginRate(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getEstimateNetMarginRate( $this->idestimate );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getGrossMarginAmount()
	 */
	public function getGrossMarginAmount(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getEstimateGrossMarginAmount( $this->idestimate );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/Sale#getGrossMarginRate()
	 */
	public function getGrossMarginRate(){
		
		include_once( dirname( __FILE__ ) . "/TradeUtil.php" );
		return TradeUtil::getEstimateGrossMarginRate( $this->idestimate );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * (non-PHPdoc)
	 * @see objects/DBSale#recalculateAmounts()
	 */
	protected function recalculateAmounts(){
	
		/*
		 * @todo : pour les champs ci-dessous, on garde arbitrairement le pourcentage et non le montant : 
		 * @todo RENAME `total_discount` `total_discount_rate`
		 */

		if( B2B_STRATEGY ){
		
		$articlesET = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
		$articlesET += $it->next()->getTotalET();
				
		$articlesATI = 0.0;
		$it = $this->items->iterator();
		while( $it->hasNext() )
		$articlesATI += $it->next()->getTotalATI();
			
		
		$articlesVAT = 0.0;
			$it = $this->items->iterator();
			$itt = $this->items->iterator();
			while( $it->hasNext() && $itt->hasNext() )
			{
			
			$articlesVAT += ($it->next()->getTotalET()) * ($itt->next()->getVATRate()) /100;
			
			
			}
			
			$tot_ht = round($articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+$this->getChargesET(),2);
			$tot_vat = round($articlesVAT,2);
			$total_vat = round((( $tot_vat * ( 1- $this->recordSet[ "total_discount" ]/100 ) ) + ($this->getChargesET() * $this->getVATRate()/100)  ),2);
			$tot_ttc = $tot_ht + $total_vat;
					
			$this->recordSet[ "total_amount" ] 	= $tot_ttc;
			
			$this->recordSet[ "total_amount_ht" ] 		= round(($articlesET - ($articlesET * $this->recordSet[ "total_discount" ]/100)+ $this->getChargesET() ),2);
			
			
			$this->set( "total_discount_amount", 	round( $articlesET * $this->recordSet[ "total_discount" ] / 100.0, 2 ) );
			$this->set( "billing_amount", 			round( $this->recordSet[ "billing_rate" ] * ( $articlesET - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge_ht" ] ) / 100.0, 2 ) );
		
		}
		else{
		
			$articlesET = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
				$articlesET += $it->next()->getTotalET();
				
			$articlesATI = 0.0;
			$it = $this->items->iterator();
			while( $it->hasNext() )
				$articlesATI += $it->next()->getTotalATI();
		
		
			$this->recordSet[ "total_amount" ] 	= $articlesATI - ($articlesATI*$this->recordSet[ "total_discount" ]/100) + $this->getChargesATI();
			
			$this->recordSet[ "total_amount_ht" ] 		= $articlesET - ($articlesET*$this->recordSet[ "total_discount" ]/100)+  ( $this->getChargesET() );
			
						
			$this->set( "total_discount_amount", 	round( $articlesATI * $this->recordSet[ "total_discount" ] / 100.0, 2 ) );
			$this->set( "billing_amount", 			round( $this->recordSet[ "billing_rate" ] * ( $articlesATI - $this->recordSet[ "total_discount_amount" ] + $this->recordSet[ "total_charge" ] ) / 100.0, 2 ) );
			
		}
		
		//recalcul des frais de port
		
		$this->updateCharges();
		$this->updateSupplierCharges();
		
	}

	//------------------------------------------------------------------------------------------------------------
	/**
	 * @access protected
	 * @return void
	 */
	protected function updateCharges(){
		
		$this->recordSet[ "charge_vat_rate" ] 	= $this->getVATRate();
		$this->recordSet[ "charge_vat" ] 		= $this->recordSet[ "total_charge" ] - $this->recordSet[ "total_charge_ht" ];
		
		if( !$this->recordSet[ "total_charge_auto" ] )
			return; //manuel : pas de chagement
			
		
		$this->recordSet[ "total_charge_ht" ] 	= $this->getChargesET();
		$this->recordSet[ "total_charge" ] 		= $this->getChargesATI();
		
		/* port "Départ Usine" pour la Corse, les DOM-TOMs et les pays étrangers */
		
		if( $this->getForwardingAddress()->getIdState() != 1
			|| ( strlen( $this->getForwardingAddress()->getZipcode() ) > 1 
				&& substr( $this->getForwardingAddress()->getZipcode(), 0, 2 ) == "20" ) ){
			
			$this->recordSet[ "charge_free" ] = 0;
			$this->recordSet[ "ex_factory" ] =  1;
	
		}
	
	}
	
	//------------------------------------------------------------------------------------------------------------
	
}

?>