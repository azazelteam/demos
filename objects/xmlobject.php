<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object génération XmlExport
*/

include_once( dirname( __FILE__ ) . "/Session.php" );


class XLMEXPORT
{
var $handle;
var $session;
var $showprices = true;

function XLMEXPORT()
{

if(DBUtil::getParameter('edition_hideprices')) $this->showprices = false;
}

function init($filename)
{
if (!$this->handle = fopen($filename, 'wb')) {
         echo "Impossible d'ouvrir le fichier ($filename)";
         return 0;
    }
    if ( fwrite($this->handle, '<?php xml version="1.0" encoding="UTF-8"?>' ) === FALSE) {
       echo "Impossible d'écrire dans le fichier ($filename)";
       return 0;
    } 
    return 1;   
}

function setstyle($filename)
{
$this->nl();
fwrite($this->handle, '<?php xml-stylesheet type="text/xsl" href="'. $filename .'"?>');
$this->nl();
}

function export()
{
fwrite($this->handle,"\n<brochure>\n");
$this->buyer();
$this->pages();
fwrite($this->handle,"\n</brochure>\n");
}


function buyer()
{
fwrite($this->handle,"\n<buyer>");
$bb = $_SESSION['coord'] ;
fwrite($this->handle,"\n  <buyercoord>$bb</buyercoord>");
$bb = $_SESSION['msgcom'] ;
fwrite($this->handle,"\n  <msgcom>$bb</msgcom>");
$bb = $_SESSION['vdate'] ;
fwrite($this->handle,"\n  <vdate>$bb</vdate>");
$bb = $_SESSION['title'] ;
fwrite($this->handle,"\n  <title>$bb</title>");
if(Session::getInstance()->getCustomer() )
	$logo=Session::getInstance()->getCustomer()->get( "logo" );
else $logo ='../xpdf/logo.jpg';
fwrite($this->handle,"\n  <buyerlogo>$logo</buyerlogo>\n");
fwrite($this->handle,"</buyer>\n");
}


function pages()
{
fwrite($this->handle,"<page num=\"1\">\n");
$this->produits();
fwrite($this->handle,"</page>\n");
}

function unproduit($idp)
{
fwrite($this->handle,"\n<brochure>\n");
fwrite($this->handle,"<page num=\"1\">\n");
	fwrite($this->handle,"<product num=\"1\">\n");
	$this->produit($idp);
	fwrite($this->handle,"\n</product>\n");
fwrite($this->handle,"</page>\n");
fwrite($this->handle,"\n</brochure>\n");
}

function produits()
{
if( !isset($_SESSION['produits']) ) return;
$n = count($_SESSION['produits']);
for($i=0; $i < $n; $i++)
	{
	$j=$i+1;$mask = 0; $ttype = 'N';
	$idp = $_SESSION['produits'][$i];
	if( isset($_SESSION['gabarit'][$idp]) ) $mask = $_SESSION['gabarit'][$idp];
	if( isset($_SESSION['gabtables'][$mask]['type']) ) $ttype = $_SESSION['gabtables'][$mask]['type'];
	fwrite($this->handle,"<product num=\"$j\" mask=\"$mask\" tabletype=\"$ttype\" >\n");
	$this->produit($idp);
	fwrite($this->handle,"\n</product>\n");
	}
}

function produit($idp)
{
	global $GLOBAL_START_PATH;
$Product = new PRODUCT($idp);
$n = $Product->realcount();

//	fwrite($this->handle,"<product num=\"1\">\n");
		fwrite($this->handle,"<name>");
		fwrite($this->handle,$Product->GetName());
		fwrite($this->handle,"</name>\n");
		fwrite($this->handle,"<image>");
		fwrite($this->handle,$GLOBAL_START_PATH .'/www/'.$Product->GetZoom());//fwrite($this->handle,$Product->GetImage());
		fwrite($this->handle,"</image>\n");
		fwrite($this->handle,"<desc>");
		fwrite($this->handle,$this->desctxt($Product->GetDescrition()));
		//fwrite($this->handle,$Product->GetDescrition());
		fwrite($this->handle,"</desc>\n");
$x = count($_SESSION['articles'][$idp]);
if( $x <= 0 ) return;
	fwrite($this->handle,"<entete>\n");
		//////fwrite($this->handle,"<col num=\"101\">N°</col>");
		fwrite($this->handle,"<hcolmn num=\"102\">Référence</hcolmn>");
//ssss		fwrite($this->handle,"<hcolmn num=\"112\">Désignation</hcolmn>");
if($this->showprices) {
	fwrite($this->handle,"<hcolmn num=\"120\">Prix Unitaire</hcolmn>");
	fwrite($this->handle,"<hcolmn num=\"122\">Remise</hcolmn>");
	fwrite($this->handle,"<hcolmn num=\"124\">Prix HT</hcolmn>");
	fwrite($this->handle,"<hcolmn num=\"126\">Prix TTC</hcolmn>");
}	
	$Product->rand(0);
	$nparm = 0 ;
	for($j=1; $j < 16 ; $j++){
		$p = "parm_name_$j" ;
		$infop = $Product->getvalue($p) ;
		if( $infop != '' ) {
			$k = $j + 200 ;
			fwrite($this->handle,"<hcolmn num=\"$k\">");
			//$v = "parm_value_$j";
			fwrite($this->handle,trim($Product->getvalue($p)));
			fwrite($this->handle,"</hcolmn>\n");
			}
			else 
			{
			$nparm = $j; break;
			}
		}
	fwrite($this->handle,"</entete>\n");
$ii=0;
	for($i=0; $i < $n; $i++)
	{
	$Product->rand($i);
$id = $Product->GetArtID();	
$key = array_search( $id ,$_SESSION['articles'][$idp] );
if( $key === FALSE ) continue;
		$ii++;
		fwrite($this->handle,"<article num=\"$ii\">\n");
			//////fwrite($this->handle,"<col num=\"101\" type=\"id\" >");
			//////fwrite($this->handle,$Product->GetArtID());
			//////fwrite($this->handle,"</col>\n");
			fwrite($this->handle,"<colmn num=\"102\" type=\"ref\" >");
			fwrite($this->handle,$Product->GetRef());
			fwrite($this->handle,"</colmn>\n");
//ssss			fwrite($this->handle,"<colmn num=\"112\" type=\"name\" >");
//			fwrite($this->handle,str_replace("<br />","<br />",$Product->GetName()));
//ssss			fwrite($this->handle,"</colmn>\n");
if($this->showprices) {
			fwrite($this->handle,"<colmn num=\"120\" type=\"price\" currency=\"EURO\">");
			if( $Product->GetBasicPrice() > 0)
			fwrite($this->handle,number_format($Product->GetBasicPrice(),2,","," ").' '.chr(128));
			else fwrite($this->handle,'-');
			fwrite($this->handle,"</colmn>\n");
			fwrite($this->handle,"<colmn num=\"122\" type=\"rate\" >");
			$rate = 0; if( $Product->GetBasicPrice() > 0)
			$rate = (1.0-$Product->GetWotPrice()/$Product->GetBasicPrice())*100.0;
			if( $rate > 0.001 ) 
			fwrite($this->handle,number_format($rate,2,","," ").' %');
			else
			fwrite($this->handle,'-');
			fwrite($this->handle,"</colmn>\n");
			fwrite($this->handle,"<colmn num=\"124\" type=\"promo\" currency=\"EURO\">");
			if( $Product->GetWotPrice() > 0)
			fwrite($this->handle,number_format($Product->GetWotPrice(),2,","," ").' '.chr(128));
			else fwrite($this->handle,'-');
			fwrite($this->handle,"</colmn>\n");
			fwrite($this->handle,"<colmn num=\"126\" type=\"ttc\" currency=\"EURO\">");
			if( $Product->GetWtPrice() > 0)
			fwrite($this->handle,number_format($Product->GetWtPrice(),2,","," ").' '.chr(128));
			else fwrite($this->handle,'-');
			fwrite($this->handle,"</colmn>\n");
}
			for($j=1; $j < $nparm ; $j++){
				$v = "parm_value_$j";
				$infop = $Product->getvalue($v);
				if( $infop != '' ) {
				$k = $j + 200 ;
				fwrite($this->handle,"<colmn num=\"$k\">");
				fwrite($this->handle,trim($Product->getvalue($v)));
				fwrite($this->handle,"</colmn>\n");
				  }
			}
		fwrite($this->handle,"</article>\n");
	}
}

function desctxt($t)
{
    $s="\n";
    //$string = ereg_replace("<[^>]*>", '', $t);
    $t = ereg_replace("\"+" , '"', $t);//provisoire : bug d'import
    $t = ereg_replace("<span[^>]*>", '', $t);//provisoire : bug d'import
    $t = ereg_replace("</span>", '', $t);//provisoire : bug d'import
    //$string = $this->escapedata($t);
   $string =  html_entity_decode($t);
    $string = str_replace("<br />","<br />",$string);
    $string = ereg_replace('&nbsp;', '', $string);
    $string = ereg_replace('&rsquo;', "'", $string);
//    $string = str_replace(". ",".£",$string);
    //$string = str_replace("<br />","",$string);
    $lines= explode('£',$string);
//    while (list($k, $v) = each($lines))
//    $s .= "<line>$v</line>\n";
 $s .= "<line>$string</line>\n";
    return $s;
}

Function escapedata($data)
{
	$data = str_replace("<br />","<br />",$data);
	$position=0;
	$length=strlen($data);
	$escapeddata="";
	for(;$position<$length;)
	{
		$character=substr($data,$position,1);
		$code=Ord($character);
		switch($code)
		{
			case 34:
				$character="&quot;";
				break;
			case 38:
				$character="&amp;";
				break;
			case 39:
				$character="&apos;";
				break;
			case 60:
				$character="&lt;";
				break;
			case 62:
				$character="&gt;";
				break;
			default:
				if($code<32)
					$character=("&#".strval($code).";");
				break;
		}
		$escapeddata.=$character;
		$position++;
	}
	return $escapeddata;
}

function nl() { fwrite($this->handle,"\n"); }
//close the file
function close() { fclose($this->handle); }
}

?>