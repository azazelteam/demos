<?php
/** 
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des articles
 */

include_once( dirname( __FILE__ ) . "/tableobject.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class DEPRECATEDARTICLE extends TABLEDESC{
	
	//---------------------------------------------------------------------------------------------------------
	//static members
	
	public static $TYPE_CATALOG 		= "catalog";
	public static $TYPE_SPECIFIC 		= "specific";
	public static $TYPE_ALTERNATIVE 	= "alternativ";
	public static $TYPE_DESTOCKING 		= "destocking";
	
	//---------------------------------------------------------------------------------------------------------
	
	protected $id;
	protected $lang;
	protected $article;
	protected $BasicPrice;
	protected $MultiPriceFamily;
	protected $MultiPriceRate;
	protected $MultiPriceBuyerRate;
	protected $MultiPriceGroupingRate;
	protected $MultiPriceGroupingAmount;
	protected $CustomMultiPriceFamilyRate;
	protected $MultiPrice;
	protected $MultiPriceBuyer;
	protected $CustomMultiPriceFamily;
	protected $MultiPriceGrouping;
	protected $PriceFamilyProduct;
	protected $PricePromoProduct;
	protected $PriceDestockProduct;
	protected $PromoRate;
	protected $PromoPrice;
	protected $comproduct;
	protected $ReduceRate;
	protected $DEBUG; // ne pas mettre 'code' sinon le pdf plante
	protected $Tissus;
	protected $Couleurs;
	protected $Selected_Tissus;
	protected $Selected_Couleurs;
	protected $delay;
	protected $ref_extra; //extention de la référence pour tissus/couleurs
	protected $old;
	protected $parmFam;
	protected $light;
	protected $idtissus;
	protected $idcolor;
	protected $option_price;
	protected $idbuyer; 
	
	/**
	 * Contructeur
	 * @param array $art Les données article
	 * @param mixed $light si true pas d'intitulés, si 2 pas de famille et conservation de l'ordre
	 * @param mixed $tissus false si pas de tissu pour l'article, tableau des tissus disponibles sinon
	 * @param mixed $selected_tissus false si pas de tissu sélectionné pour l'article, tableau avec infos du tissu sélectionné sinon
	 * @param mixed $couleurs false si pas de couleur pour l'article, tableau des couleurs disponibles sinon
	 * @param mixed $selected_couleurs false si pas de couleur sélectionnée pour l'article, tableau avec infos de la couleur sélectionnée sinon
	 **/
	function __construct($art,$light=true,$tissus=false,$selected_tissus=false,$couleurs=false,$selected_couleurs=false)
		{
		
		$this->id = 0;
		$this->lang ='_1';
		$this->BasicPrice = -1;
		$this->MultiPriceFamily = 0;
		$this->MultiPriceRate = 0.0 ;
		$this->MultiPriceBuyerRate = 0.0 ;
		$this->MultiPriceGroupingRate = 0.0;
		$this->CustomMultiPriceFamilyRate = 0.0;
		$this->MultiPrice = -1 ;
		$this->MultiPriceBuyer = -1 ;
		$this->CustomMultiPriceFamily = -1;
		$this->MultiPriceGrouping = -1;
		$this->PriceFamilyProduct = 0;
		$this->PricePromoProduct = 0 ;
		$this->PriceDestockProduct = 0 ;
		$this->PromoRate = 0.0;
		$this->PromoPrice = 0;
		$this->comproduct ='';
		$this->ReduceRate = 0.0;
		$this->DEBUG = 'none'; // ne pas mettre 'code' sinon le pdf plante
		$this->Tissus = false;
		$this->Couleurs = false;
		$this->Selected_Tissus = false;
		$this->Selected_Couleurs = false;
		$this->delay = '';
		$this->ref_extra = ''; //extention de la référence pour tissus/couleurs
		$this->old = false;
		$this->parmFam = array();
		$this->light = true;
		$this->idtissus = 0;
		$this->idcolor = 0;
		$this->option_price = 0.0;
		$this->idbuyer = 0; 
		$this->lang = "_1";
		$this->Tissus = &$tissus;
		$this->Couleurs = &$couleurs;
		$this->Selected_Tissus = &$selected_tissus;
		$this->Selected_Couleurs = &$selected_couleurs;
		$this->tablename = 'detail';
		$this->multilangfields = array('name','description','doc','designation','key_word','desc_more','desc_tech','text_commercial','summary');
		// $this->load_design(); mostly not needed - done in Load fubction
		$this->article = $art;
		$this->light = $light; // Evite de charger les noms des familles si ça n'est pas nécessaire
		if(isset($art['idarticle'])) {
			$this->initArt();
			//showDebug($this->id,'DEPRECATEDARTICLE::ARTICLE() => initArt($this->id)',$this->DEBUG);
		}
		
	}

	/**
	 * Initalise l'id de l'article
	 * @param int $id l'id de l'article
	 * @return void
	 */
	public function SetId($id) { $this->id = 0+$id; }

	public function getRefExtra(){ return $this->ref_extra; }
	
	/**
	 * Retroune le chemin vers l'icône
	 * @param string $origine racine du répertoire de stockage
	 * @return string $Result chemin complet de l'image
	 */
	public function GetIcon( $deprecated = '../www')
	{
		return URLFactory::getReferenceImageURI( $this->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE );
	
	}

	/**
	 * Retroune le chemin vers l'image produit n°1
	 * @param string $origine racine du répertoire de stockage
	 * @return string $Result chemin complet de l'image
	 */
	public function GetProd($origine = '../www')
	{
		return URLFactory::getReferenceImageURI( $this->get( "idarticle" ), URLFactory::$IMAGE_THUMB_SIZE );
	}
	
	/**
	 * Retroune le chemin vers l'image produit n°2
	 * @param string $origine racine du répertoire de stockage
	 * @return string $Result chemin complet de l'image
	 */
	public function GetProd2($origine = '../www')
	{
		return "";
	}

	/**
	 * Retroune le chemin vers l'image
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetImage($origine = '../www') { 
		return URLFactory::getReferenceImageURI( $this->get( "idarticle" ), URLFactory::$IMAGE_DEFAULT_SIZE );
	}
	
	/**
	 * Retourne la bordure image
	 * @return int taille de la bordure
	 */
	public function GetBorder() { return 0; }


	/**
	 * Retroune le chemin vers l'image zoom n°1
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetZoom1($origine = '../www') { 
		return URLFactory::getReferenceImageURI( $this->get( "idarticle" ), URLFactory::$IMAGE_MAX_SIZE );
	}

	/**
	 * Retroune le chemin vers l'image zoom n°2
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetZoom2($origine = '../www') { 
		return URLFactory::getProductImageURI( $this->get( "idproduct" ), URLFactory::$IMAGE_MAX_SIZE, 1 );
	}
	
	/**
	 * Retroune le chemin vers l'image zoom n°3
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetZoom3($origine = '../www') { 
		return URLFactory::getProductImageURI( $this->get( "idproduct" ), URLFactory::$IMAGE_MAX_SIZE, 2 );
	}
		
	/**
	 * Retroune le chemin vers le logo n°1
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetLogo1($origine = '../www') { return ""; }
	
	/**
	 * Retroune le chemin vers le logo n°2
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetLogo2($origine = '../www'){ return ""; }
	
	/**
	 * Retroune le chemin vers le logo n°3
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetLogo3($origine = '../www'){ return ""; }
	
	/**
	 * Retroune le chemin vers le logo n°4
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetLogo4($origine = '../www'){ return ""; }
	
	/**
	 * Retroune le chemin vers le logo n°5
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de l'image
	 */
	public function GetLogo5($origine = '../www'){ return ""; }
	
	/**
	 * Retourne l'image logo n° 6
	 * @param string $origine racine du répertoire de stockage
	 */
	public function GetLogo6($origine = '../www'){ return ""; }
	
	/**
	 * Retourne l'image logo n° 7
	 * @param string $origine racine du répertoire de stockage
	 */
	public function GetLogo7($origine = '../www'){ return ""; }
	
	/**
	 * Retourne l'image logo n° 8
	 * @param string $origine racine du répertoire de stockage
	 */
	public function GetLogo8($origine = '../www'){ return ""; }
	
	/**
	 * Retourne l'image logo n° 9
	 * @param string $origine racine du répertoire de stockage
	 */
	public function GetLogo9($origine = '../www'){ return ""; }
	
	/**
	 * Retourne l'image logo n° 10
	 * @param string $origine racine du répertoire de stockage
	 */
	public function GetLogo10($origine = '../www'){ return ""; }
	
	/**
	 * Retroune le chemin vers l`image documentation
	 * @param string $origine racine du répertoire de stockage
	 * @return string $result chemin complet de la documentation
	 */
	public function GetImg_doc($origine = '../www') { 
		return URLFactory::getProductDataSheetURI( $this->get( "idproduct" ) );
	}
	
	/**
	 * Retourne si la remise est en % ou $
	 */
	public function IsReduceRate(){
		$isRate=true;
		if($this->MultiPriceGroupingAmount>0){
			$isRate=false;
		}
		
		return $isRate;
	}
	
	/**
	 * Retourne si il y a multiprice ou pas true ou false
	 */
	public function hasMultiPriceRange(){
		$hasMultiPriceRange = false;
		
		if( $this->idbuyer ) //sales_force
			$idbuyer = $this->idbuyer;
		else $idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0; //front office
		
		$this->MultiPriceBuyerRate = 0;
		$this->MultiPriceBuyer = -1;
		
		if( $idbuyer <= 0 ) 
			return 0;
			
		if( $this->BasicPrice <= 0) 
			return 0;
		
		$Reference = $this->GetRef();
		$Date = time();
		
		$query="SELECT count(idmultiprice_buyer) as compte
				FROM multiprice_buyer
				WHERE idbuyer = $idbuyer
				AND reference = '$Reference'
				AND (UNIX_TIMESTAMP(begin_date)<='$Date'
				AND UNIX_TIMESTAMP(end_date)>='$Date'
				OR end_date='0000-00-00')";
		$resultSet=DBUtil::query($query);
		
		if($resultSet->fields("compte")>0){
			$hasMultiPriceRange=true;
		}
		
		return $hasMultiPriceRange;
	}
	
	/**
	 *Retourne le table de multiprix quantitatif d'une référence 
	 */
	public function getMultiPrices(){
		$arrayMultipricesTotal=array();
		$arrayMultiprices=array();
		if(!$this->hasMultiPriceRange()){
			/*$arrayMultiprices["quantity_min"]="";
			$arrayMultiprices["quantity_max"]="";
			$arrayMultiprices["rate"]="";*/
			
			$arrayMultipricesTotal[]="";
			return $arrayMultipricesTotal;
		}			
		
		if( $this->idbuyer ) //sales_force
			$idbuyer = $this->idbuyer;
		else $idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0; //front office
		
		$this->MultiPriceBuyerRate = 0;
		$this->MultiPriceBuyer = -1;
		
		if( $idbuyer <= 0 ) 
			return 0;
			
		if( $this->BasicPrice <= 0) 
			return 0;
				
		$Reference = $this->GetRef();
		$Date = time();
		
		$query="SELECT reference,quantity_min,quantity_max,rate
				FROM multiprice_buyer
				WHERE idbuyer = $idbuyer
				AND reference = '$Reference'
				AND (UNIX_TIMESTAMP(begin_date)<='$Date'
				AND UNIX_TIMESTAMP(end_date)>='$Date'
				OR end_date='0000-00-00')";
		
		$resultSet=DBUtil::query($query);
		
		while(!$resultSet->EOF()){
			$arrayMultiprices = array(	"quantity_min"=>$resultSet->fields("quantity_min"),
										"quantity_max"=>$resultSet->fields("quantity_max"),
										"rate"=>Util::numberFormat($resultSet->fields("rate"))
									 );

			$arrayMultipricesTotal[]=$arrayMultiprices;
			$resultSet->MoveNext();
		}
		return $arrayMultipricesTotal;
	}	
	
	/**
	 * Retroune le chemin vers la page du catalogue papier
	 * @return string le chemin du produit dans le catalogue papier
	 */
	public function GetPage() { return $this->get('page'); }
	
	/**
	 * Retourne le chemin vers 'table', utilisé pour le tri dans les masques
	 * @return int numéro du tableau du masque
	 */
	public function GetTable() { return $this->get('table'); }
	
	/**
	 * Retourne l'id de l'article
	 * @return int id de l'article
	 */
	public function GetId() { return $this->id; }
	
	/**
	 * Retourne l'id du produit
	 * @return int id du produit
	 */
	public function GetProductId() { return $this->get('idproduct'); }
	
	/**
	 * Retourne le nom de l'article
	 * @return string nom du produit
	 */
	public function GetName() { return $this->get('name'); }
	
	/**
	 * Retourne la référence de l'article
	 * @return string reference de l'article
	 */
	public function GetRef() { return $this->get('reference'); }
	
	/**
	 * Retourne la désignation de l'article
	 * @return string designation de l'article
	 */
	public function GetDesignation() { return $this->get('designation'); }
	
	/**
	 * Retourne le poids de l'article
	 * @return float poids de l'article
	 */
	public function GetWeight() { return $this->get('weight'); }
	
	/**
	 * Retourne le code douane de l'article
	 * @return string code douane de l'article
	 */
	public function GetCode_Customhouse() { return $this->get('code_customhouse'); }
	
	/**
	 * Retourne le gencod de l'article
	 * @return string gencod de l'article
	 */
	public function GetGencod() { return $this->get('gencod'); }
	
	/**
	 * Retourne le n° article de l'article
	 * @return string le n° de l'article
	 */
	public function GetNarticle() { return $this->get('n_article'); }
	
	/**
	 * Retourne le prix HT de l'article
	 * @return float le prix HT de l'article
	 */
	public function GetWoPrice() { return $this->get('wotprice'); }
	
	/**
	 * Retourne le prix TTC de l'article
	 * @return float le prix TTC de l'article
	 */
	public function GetWtPrice() { return $this->get('wtprice'); }
	
	/**
	 * Retourne le prix catalogue
	 * @return float le prix catalogue de l'article 
	 */
	public function GetBasicPrice() { return $this->BasicPrice; }
	
	/**
	 * Retourne le taux de remise
	 * @return float le taux de remise sur l'article
	 */
	public function GetReduceRate() { return $this->ReduceRate; }
	
	/**
	 * Retourne le taux de promo
	 * @return float le taux de promo sur l'article
	 */
	public function GetPromoRate() { return $this->PromoRate; }
	
	/**
	 * Retourne une valeur
	 * @param string $key Le nom de la valeur
	 * @param mixed $default valeur retournée par défaut
	 * @return mixed, valeur correpondant à $key
	 */
	public function getvalue($key,$default='') {
		return $this->get($key,$default);
	} 
	
	/**
	 * Retourne le prix d'achat 
	 * @return float prix d'achat
	 */
	public function GetBuyingPrice() { return $this->get('buyingcost'); }
	
	/**
	 * Retourne le prix par quantité
	 * @return float prix par quantité
	 */
	public function GetMultiPrice() { return $this->MultiPrice; }
	
	/**
	 * Retourne le prix au volume par acheteur
	 * @return float prix au volume
	 */
	public function GetMultiPriceBuyer() { return $this->MultiPrice; }
	
	/**
	 * Retourne le prix promo
	 * @return float prix de la promotion
	 */
	public function GetPromoPrice() { return $this->PricePromoProduct; }
	
	/**
	 * Retourne l'id tissus
	 * @return int id du tissus
	 */
	public function GetIdTissus() { return $this->idtissus; }
	
	/**
	 * Retourne l'id couleur
	 * @return int id de la couleur
	 */
	public function GetIdColor() { return $this->idcolor; }
	
	/**
	 * Retourne le supplément de prix pour une option
	 * @return float supplément de prix
	 */
	public function GetOptionPrice() { return $this->option_price; }
	
	/**
	 * Retourne la catégorie
	 * @return int id de la catégorie
	 */
	public function GetIdCategory() { return $this->get('idcategory'); }
	
	/**
	 * Retourne le délai de livraison
	 * @return int id du délai de livraison
	 */
	public function GetDelivDelay() { if(!empty($this->delay) ) return $this->delay; return $this->get('delivdelay'); }

	/**
	 * Retourne l'id du fournisseur
	 * @return int l'id du fournisseur 
	 */
	public function GetIdSupplier() { return $this->get('idsupplier'); }
	
	/**
	 * Retourne le taux de remise appliqué
	 * @return float la remise appliquée
	 */
	public function GetRefDiscount() { return $this->get('ref_discount'); }
	
	/**
	 * Indique si l'article est sélectionné dans le comparatif
	 * @return mixed false si le produit n'est pas dans le comparatif, checked si il y est
	 */
	public function IsSelected() { return $this->comproduct; }
	
	/**
	 * Retourne une valeur
	 * @param string $key Le nom de la valeur
	 * @param mixed $default valeur retournée par défaut
	 * @return mixed valeur correpondant à $key 
	 */
	public function get($key,$default='')
	{
		if( empty($this->article['idarticle']) && !empty($this->id) ) $this->Load();
		
		///if( !isset($this->article[$key]) ) return " $key no set";
		if( isset($this->article[$key]) && $this->article[$key] != '' ) return Util::doNothing($this->article[$key]);
		else return $default;
	}
	
	/**
	 * Retourne l'idbuyer
	 * @return int l'idbuyer 
	 */
	public function getIdBuyer(){
		
		return $this->idbuyer;
		
	}
	
	/**
	 * Affecte une valeur à une propriété de l'article
	 * @param string $key Le nom de la valeur
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	public function set( $key, $value ){
	
		$this->article[ $key ] = $value;
			
	}
	
	/**
	 * Affecte une valeur pour le taux de remise
	 * @param float $reduceRate le taux à affecter
	 * @return void
	 */
	public function setReduceRate( $reduceRate ){
	
		$this->ReduceRate = $reduceRate;
			
	}
	
	
	/**
 	* Calcul du prix de base avec options tissus/couleurs
 	* @return void
 	*/	
 	private function CalculBasicPrice()
 	{
		
 		$this->BasicPrice = $bp = $this->get('sellingcost');
 		$this->article['ref_extra']='';
 		
 		if( empty($this->article['manage']) ) return; // pas de gestion de tissus/couleurs si 0 ou vide
 		if( !empty($this->article['table']) ) {
 			$table = $this->article['table'];
 			if( $this->get("use_color_$table") <= 0 ) return; // pas de gestion de tissus/couleurs
 		}
 		
 		// chg prix pour les tissus
 		if( !empty($this->Tissus) AND !empty($this->Selected_Tissus) ) {
	 		foreach($this->Selected_Tissus as $i) {
	 			if($this->Tissus[$i]['price'] != 0 AND $this->BasicPrice > 0) $this->BasicPrice += $this->Tissus[$i]['price'];
	 			elseif($this->Tissus[$i]['rate'] != 0) $this->BasicPrice += $this->get('sellingcost') * ($this->Tissus[$i]['rate']/100.0);
	 			$this->delay = $this->Tissus[$i]['delay'];
	 			$this->idtissus = $this->Tissus[$i]['idtissus_product']; 
	 			$this->article['ref_extra'] = $this->ref_extra .= '-'.$this->Selected_Couleurs['RefTissus'];
	 			$this->article['idarticle'] = $this->article['idarticle'];
	 			$this->article['designation'] = $this->get('designation') . '<br />' . Dictionnary::translate( "cloth" ) . " : " . $this->Tissus[$i]['name'].'<br />'.$this->Selected_Couleurs['RefTissus'];
	 			$this->option_price = $this->BasicPrice - $bp;
	 		}
 		}
 		// chg prix pour les couleurs
 		if( !empty($this->Couleurs) AND !empty($this->Selected_Couleurs) ) {
	 		foreach($this->Selected_Couleurs as $i) {
	 			if($this->Couleurs[$i]['price'] != 0 AND $this->BasicPrice > 0) $this->BasicPrice += $this->Couleurs[$i]['price'];
	 			elseif($this->Couleurs[$i]['percent'] != 0) $this->BasicPrice += $this->article['sellingcost'] * ($this->Couleurs[$i]['percent']/100.0);
	 			$this->delay = $this->Couleurs[$i]['delay'];
	 			$this->article['ref_extra'] = $this->ref_extra .= '-'.$this->Couleurs[$i]['code_color'];
	 			$this->idcolor = $this->Couleurs[$i]['idcolor'];
	 			$this->article['idarticle'] = $this->article['idarticle'];
	 			$this->article['designation'] = $this->get('designation').'<br />' . Dictionnary::translate( "color" ) . " : " . $this->Couleurs[$i]['code_color'];
	 			$this->option_price = $this->BasicPrice - $bp;
	 		}
 		}
 	}
 	
	/**
	 * Initialise l'article
	 * Calcul des prix, des intitulés, etc...
	 * @param int $qtt Quantité
	 * @return void
	 */
	private function initArt($qtt=1)
	{
		$Langue = $this->lang;
//print "123456";exit;
		$a = array();
		foreach( $this->article as $k=>$v ) {
			// On ne garde que les champs ayant une valeur non vide
			if( empty($v) ) 
				continue;
			$a[$k] = $v;
		}
		
		$this->article = $a;
		//showDebug($this->article,'DEPRECATEDARTICLE::initArt('.$this->id.') => $this->article',$this->DEBUG);
		
		$this->id = $this->article['idarticle'];

		$this->article['vat'] = $this->GetVAT();

		$this->article['delivdelay'] = $this->GetDelivDelay();
		// CALCUL DES PRIX

		$this->article['wotprice'] = $this->CalculPrice($qtt);
		
		//comparatif
		if( isset($_SESSION['comproducts']) ) 
		{
		$key = array_search( $this->id,$_SESSION['comproducts'] ) ;
			if($key === FALSE) $this->comproduct = '';
			else $this->comproduct ='checked="checked"';
		}
		// Ancienne version pour rechercher les familles d'intitulés (encore utilisé pour le pdf)
		//if($this->old) $this->GetFamInt();
		//if(!$this->light OR $this->light==2) $this->SetParmFam();
	}
	
	public function getArticle(){
		
		return $this->article;
		
	}
	
	/**
	 * Recherche les familles des intitulés
	 * @param int $i Numéro de l'intutulé
	 * @param array $sorter Tableau des intitulés
	 * @param boolean $compute_families true si on gère les familles
	 * @return array tableau des familles d'intitulés
	 */
	function GetFamIntitule($i,&$sorter, $compute_families=true)
	{
		if( empty($this->article["parm_name_$i"]) ) return 0;
		$intitule = $this->article["parm_name_$i"];
		$eintitule = addslashes($intitule);
		$value = !empty($this->article["parm_value_$i"]) ? $this->article["parm_value_$i"] : '';
		$Langue = $this->lang;
		$Base = &DBUtil::getConnection();
		
		$idsfam = array();
		$idcategory = $this->article['idcategory'];
		$idcats= array($idcategory);
		$IdCategoryParent = $idcategory;
		while (!empty($IdCategoryParent)) {
			// On prend toutes la hiérachie des catégories
			$query = 'SELECT `idcategoryparent` FROM `category_link` WHERE `idcategorychild`='.$IdCategoryParent;
			$rs = DBUtil::getConnection()->GetRow($query);
			if($rs) { $idcats[] =  $IdCategoryParent = $rs['idcategoryparent']; }
			else {  $idcats[] = $IdCategoryParent = 0; }
		}
		
		$SQL_select = 'SELECT DISTINCT `idintitule_family` FROM `intitule_category` WHERE `idcategory` IN ('.implode(',',$idcats).') ORDER BY `display`';

		$rs =& DBUtil::query($SQL_select);
		if( empty($rs) ) trigger_error('DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $SQL_select',E_USER_ERROR);
		while ($rs && !$rs->EOF) {
			$idsfam[] = $rs->Fields('idintitule_family');
			$rs->MoveNext();
		}
		$SQL_search ="SELECT i.level,i.dyn_text,f.display as o,f.idintitule_family as p,f.idintitule_title as x " .
				"FROM intitule_title i,intitule_title_family f " .
				"WHERE i.idintitule_title = f.idintitule_title " .
				"AND intitule_title$Langue = '$eintitule'";
		if( !empty($idsfam)) $SQL_search .= ' AND f.idintitule_family IN('.implode(',',$idsfam).')';
		//showDebug($SQL_search,'DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $SQL_search','sql');
		$rs =& DBUtil::query($SQL_search);
		//showDebug($SQL_select,'DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $SQL_select',$this->DEBUG);
		//showDebug($rs,'DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $rs',$this->DEBUG);
		if( empty($rs) ) trigger_error('DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $SQL_search',E_USER_ERROR);
		elseif( $rs->RecordCount() == 0 )	{
			$display = 99 + $i ;// display order
			$idi	 = 0; // id intitulé
			$level   = 0; // type d'intitulé
			$dyn_text = 1; // texte dynamique
			$idf	= 0 ; // id family	
		} else {
			$display = 0+$rs->Fields('o'); // display order
			$idi	 = $rs->Fields('x'); // id intitulé
			$level   = $rs->Fields('level'); // type d'intitulé
			$dyn_text = $rs->Fields('dyn_text'); // texte dynamique
			$idf	= 0+$rs->Fields('p'); // id family
		}
		if($compute_families) { // TODO : à optimiser !!!!! => à remplacer par DEPRECATEDARTICLE::GetParmFam()
			$SQL_select = "SELECT intitule_family$Langue as r " .
					"FROM intitule_family WHERE idintitule_family = $idf";
			$rs =& DBUtil::query($SQL_select);
			//showDebug($SQL_select,'DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $SQL_select',$this->DEBUG);
			//showDebug($rs,'DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $rs',$this->DEBUG);
			if( empty($rs) ) trigger_error('DEPRECATEDARTICLE::GetFamIntitule('.$i.') => $SQL_select',E_USER_ERROR);
			else $res = $rs->RecordCount();
		} else {
			$res =false;
			$idi = 0;
		}
		if(!empty($res))
		{
			if($idf) $sorter[0][] = $idf;
			else $sorter[0][] = 999999 + $idi;
			$sorter[1][] = $display;
			$sorter[2][] = $intitule;
			$sorter[3][] = $value;
			$sorter[4][] = $rs->Fields('r');
		}
		else
		{
			$sorter[0][] = 999999 + $idi;
			$sorter[1][] = $display;
			$sorter[2][] = $intitule;
			$sorter[3][] = $value;
			$sorter[4][] = '';
		}
		$sorter[5][] = $level;
		$sorter[6][] = $dyn_text;
		
		return $res;
	}

	/**
	 * Retourne un tableau avec les familles d'intitulés et les intitulés
	 * @return array tableau avec les familles d'intitulés et les intitulés
	 */
	public function GetParmFamArr()
	{
		return $this->parmFam;
	}



	/**
	 * Récupération des infos pour afficher les données pour les tableaux
	 * @param int $Compteur Index de la référence, si 0 on récupère les noms des intitulés
	 * @param int $n Le nombre d'intitulé utilisé
	 * @param array $ExtraCol Colonnes contenant les valeurs des intitulés
	 * @param array $FamilyCol Les familles d'intitulés
	 * @param int $filter Le type d'intitulé recherché (0=tous, 1=commercial, 2=technique)
	 * @return array Une ligne du tableau
	 */
	public function GetRefTable($Compteur, $n, &$ExtraCol, &$FamilyCol,$filter=0)
	{
		$wbasicprice = Util::priceFormat($this->GetBasicPrice());
		$reducerate = Util::rateFormat($this->GetReduceRate()); 
		$xwoprice =Util::priceFormat($this->GetWoPrice());
		$xwtprice = Util::priceFormat($this->GetWtPrice());
		$xfamilyprice = 'familyrates';
		$ligneprod = array(
							$this->getvalue('idarticle'),
							'idarticle' => $this->getvalue('idarticle'),
							$this->getvalue('summary'),
							'summary' => $this->getvalue('summary'),
							$this->GetVisualRef(),
							'reference' => $this->GetVisualRef(),
							$this->GetDelivDelay(),
							'delivdelay' => $this->GetDelivDelay(),
							$wbasicprice,
							'xbasicprice' => $wbasicprice,
							$reducerate,
							'reducerate' => $reducerate,
							$xwoprice,
							'xwoprice' => $xwoprice,
							$xwtprice,
							'xwtprice' => $xwtprice,
							$xfamilyprice,
							'xfamilyprice' => $xfamilyprice,
							$this->IsSelected(),
							'checked' => $this->IsSelected(),
							'poids' => $this->GetWeight(),
							'basicprice' => $this->GetBasicPrice(),
							'woprice' => $this->GetWoPrice(),
							'min_cde' => $this->getvalue('min_cde')
							);
		$arflast='';
		for($i=1;$i<=$n;$i++) {
			$pn = "parm_name_$i"; $pl = "parm_level_$i"; $fml =''; $cot = "cotation_$i";
			if($Compteur == 0) {
				$arf = $this->getvalue("parm_fam_$i");
				//if(  $arf[2] != $arflast ) $fml = $arf[2];
				//$arflast = $arf[2];
				if(  $arf != $arflast ) $fml = $arf;
				$arflast = $arf;
			}
			$flt = true;
			if( $filter > 0 && $this->getvalue($pl) != 0 && $this->getvalue($pl) != $filter) $flt = false;
			if( $flt && $this->getvalue($pn) != '') {
				if($Compteur == 0) {
					$ExtraCol[] = array('parm'=>$this->getvalue($pn),'cotation'=>$this->getvalue($cot)) ;
					$FamilyCol[] = $fml;
				}
				$pv="parm_value_$i";
				$ligneprod[] = $this->getvalue($pv);
				$ligneprod[$this->getvalue($pn)] = $this->getvalue($pv);
			}
			else $arflast = '';
		}
	return $ligneprod;
	}

	/**
	 * Retourne la référence formatée de l'article courant
	 * @return string référence formattée
	 */
	public function GetVisualRef() {
	 $v = $this->getvalue('reference');
	 if( preg_match ("(^[0-9]+$)",$v) ) return str_replace(' ','&nbsp;',number_format($this->getvalue('reference'),0,"-"," ")).$this->ref_extra;
	 else return $v.$this->ref_extra;
	}
	
	/**
	 * debug
	 */
	function dump()
	{
	  echo "<br />Article : $this->id <br />\n";
	  print_r($this->article);
	  echo "<br />---------------<br />\n";
	}
		
	/**
	 * Chargement de l'article depuis la bdd
	 * @param int $qtt Quantité
	 * @param int $idbuyer l'id du buyer
	 * @return void
	 */
	public function Load($qtt = 1,$idbuyer = 0 )
	{
		
		$this->idbuyer = $idbuyer; //sales_force
		
		$Base = &DBUtil::getConnection();
		$lang = "_1";
		
		$product_objet = new TABLEDESC();
		$product_objet->tablename='product';
		$product_objet->session = Session::getInstance() ;
		$product_objet->multilangfields = $this->multilangfields;
		$product_objet->lang = $this->lang;
		$product_objet->load_design(', p.');
		$Str_Fields = $product_objet->fields;
		$SQL_select = "SELECT p.$Str_Fields" .
				" FROM `product` p, `detail` d" .
				" WHERE `d`.`idarticle`='".$this->id."'" .
				" AND `p`.`idproduct`=`d`.`idproduct`";
		
		$rs =& DBUtil::query($SQL_select);

		if( !$rs->EOF ) 
			$p = $rs->fields;
		
		else trigger_error('DEPRECATEDARTICLE::Load('.$this->id.') => $SQL_select',E_USER_ERROR);
			
		$this->load_design(', d.');
		
		////$SQL_select = 'SELECT '.$this->fields.' FROM detail WHERE idarticle='.$this->id;
		$SQL_select = 'SELECT l.idcategory, l.idarticle,f.intitule_family_1 as `if`,t.intitule_title_1 as it ,v.intitule_value_1 as iv,l.cotation,' .
					'l.title_display as dt, l.family_display as df, l.level, d.' .
				$this->fields .
			' FROM intitule_link l,intitule_title t, intitule_value v,intitule_family f,detail d  WHERE d.idarticle = ' . $this->id .
			' AND l.idarticle = d.idarticle' .
			' AND l.idintitule_title = t.idintitule_title' .
			' AND l.idintitule_value = v.idintitule_value' .
			' AND f.idintitule_family = l.idfamily' .
			' GROUP BY l.reference,l.family_display,l.title_display';
			$rs =& DBUtil::query($SQL_select);

			$a = array();
			
			if($rs->EOF) { // aucun intitulé!
				$SQL_select = 'SELECT  d.idarticle,d.'.$this->fields.' FROM detail d WHERE d.idarticle = '.$this->id;
				$rs =& DBUtil::query($SQL_select);
				$a['fam']['label']= array();
				
			}
						
			$this->article = &$a;
			$j = 0; 
			$i = 0; 
			$lastref='No Ref!';
			while( !$rs->EOF ) {
				//$t = $rs->fields;
				$ref = $rs->fields['reference'];
				if( $ref != $lastref ) {
					if($i) {
						$this->article[$j] = &$a;
						$j++;
					}
					$lastref = $ref;
					$i = 1; $a = $p + $rs->fields;//$p + $rs->fields;
					$a['idreal_product'] = $a['idarticle'];
					if(isset($a['if'])) {
						$a["parm_fam_$i"] = $pf_x = $a['if'];
						$a["parm_name_$i"] = $pn_x = $a['it'];
						$a["parm_value_$i"] = $pv_x = $a['iv'];
						$a["cotation_$i"] = $ct_x = $a['cotation'];
						$a["level_$i"] = $a['level'];
						$a["fdisplay_$i"] = $a['df'];
						$a["idisplay_$i"] = $a['dt'];
					}
				} else {
					$i++;
					$a['idreal_product'] = $a['idarticle'];
					if(isset($rs->fields['if'])) {
						$a["parm_fam_$i"] = $pf_x = $rs->fields['if'];
						$a["parm_name_$i"] = $pn_x = $rs->fields['it'];
						$a["parm_value_$i"] = $pv_x = $rs->fields['iv'];
						$a["cotation_$i"] = $ct_x = $rs->fields['cotation'];
						$a["level_$i"] = $rs->fields['level'];
						$a["fdisplay_$i"] = $rs->fields['df'];
						$a["idisplay_$i"] = $rs->fields['dt'];
					}
				}
				
				if(isset($pf_x)) {
					$this->parmFam['int'][$pf_x]['label'][$i] = $pn_x;
					$this->parmFam['int'][$pf_x]['cot'][$i] = $ct_x;
					$this->parmFam['int'][$pf_x]['display'][$i] = 0;
					$this->parmFam['fam']['label'][$i] = $pf_x;
					$this->parmFam['int'][$pf_x]['label'][$i] = $pn_x;
					$a[$pf_x][$pn_x] = $pv_x;
					$a["parm_fam_$i"] = $pf_x;
					$a[$pn_x] = $ct_x;
				}
				
				$rs->MoveNext();
			}
			$a+=$this->parmFam;
			
			//$this->article = &$a;
		
		$this->article[ "idproduct" ] = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $this->id );
		
		//références qui ne sont pas dans le catalogue
		
		if( !$this->article[ "idproduct" ] ){
			
			$db = &DBUtil::getConnection();
			$lang = "_1";
			
			$query = "SELECT `type`, idsupplier, reference, summary$lang FROM detail WHERE idarticle = {$this->id} LIMIT 1";
			
			$rs = $db->Execute( $query );
			if( $rs === false || !$rs->RecordCount() )
				trigger_error( "Impossible de récupérer les infos dans la table détail", E_USER_ERROR );

			$reference = $rs->fields( "reference" );
			$type = $rs->fields( "type" );
			$idsupplier = $rs->fields( "idsupplier" );
			
			//fournisseur
			
			$this->article[ "idsupplier" ] = $idsupplier;
	
		}
		
		
		if( empty($this->article) ) 
			trigger_error('Produit inexistant !',E_USER_WARNING);
		else $this->initArt($qtt);
		
		
	}

	/**
	 * Calcul du prix
	 * @deprecated
	 * @param int $Qtt la quantité
	 * @return float le prix
	 */
	 
	public function CalculPrice( $quantity = 1 )
	{
				
		include_once( dirname( __FILE__ ) . "/catalog/CArticle.php" );
		$article = new CArticle( $this->id );
		
		return $article->getDiscountPriceET( $this->idbuyer, $quantity);
	
	}

	/**
	 * Recherche les prix par quantité pour une famille d'acheteur
	 * @param int $IdBuyerFamily Un id de famille d'acheteur
	 * @return array tableau de prix
	 */
	public function SearchAllFamilyprice($IdBuyerFamily=0)
	{
		$Rates = array();
		
		if( empty( $this->idbuyer ) )
			$IdBuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		else $IdBuyer = $this->idbuyer;
		
		if( $IdBuyerFamily == 0 AND $IdBuyer <= 0 ) 
			return $Rates;
			
		$Reference = DBUtil::getParameter('IdBuyerFamilyRef');
		if($Reference == '') 
			$Reference = $this->GetRef();
			
		$Price = $this->GetBasicPrice();
		
		if($IdBuyerFamily == 0){
			
			if( $this->idbuyer ){//sales_force
				
				$query = "SELECT idbuyer_family FROM buyer WHERE idbuyer = " . $this->idbuyer . " LIMIT 1";
				$db = &DBUtil::getConnection();
				$rs = $db->Execute( $query );
				if( $rs === false || !$rs->RecordCount() )
					die( "Impossible de récupérer la famille de l'acheteur" );
				
				$IdBuyerFamily = $rs->fields( "idbuyer_family" );
					
			}
			else $IdBuyerFamily = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "idbuyer_family" ) : 0;//front office
			
		}
		
		$Base = &DBUtil::getConnection();
		$Date = time();
		$query= "select rate,quantity_min,quantity_max from multiprice_family where idcustomer_profile='$IdBuyerFamily' and reference='$Reference' and UNIX_TIMESTAMP(begin_date)<='$Date' and UNIX_TIMESTAMP(end_date)>='$Date' ORDER BY quantity_min";
		$rs =& DBUtil::query($query);
		$NbRec = $rs->RecordCount();
		if( $NbRec > 0 )
		{
			for($i=0 ; $i < $NbRec ; $i++)
				{
				$r = array();
				$r[]= $rs->Fields('quantity_min');
				$r[]= $rs->Fields('quantity_max');
				$t  = $rs->Fields('rate') ;
				$r[]= Util::priceFormat( $Price*(1.0-$t/100.0) );
				$Rates[] = $r;
				$rs->MoveNext();
				}
			}
		return $Rates;
	}

	/**
	 * Recherche la TVA appliquée
	 * @param int $idbilling le numéro de la facture
	 */
	public function GetVAT( $idbilling = 0 )
	{
		
		if( $this->idbuyer ){ //sales_force
			
			if($idbilling ==0){
				$query = "SELECT state.bvat FROM buyer, state WHERE buyer.idbuyer = ' $this->idbuyer' AND buyer.idstate = state.idstate LIMIT 1";
				$db = &DBUtil::getConnection();
	        	$rs = $db->Execute( $query );
				if( $rs === false  )
					die( "Impossible de récupérer la tva du pays de l'acheteur" );
				
				$bvat = $rs->fields( "bvat" );
			}else{
				$query = "SELECT idstate FROM billing_adress WHERE idbilling_adress = $idbilling LIMIT 1";
				$db = &DBUtil::getConnection();
	        	$rs = $db->Execute( $query );
			
				if( $rs === false  )
					die( "Impossible de récupérer la tva du pays de l'acheteur" );
				
				$idstate = $rs->fields( "idstate" );
				
				$qtva = "SELECT bvat FROM state WHERE idstate=$idstate";
				$rstva = $db->Execute( $qtva );
				
				$bvat = $rstva->fields( "bvat" );
			}
			
			
			
		}
		else if( !Session::getInstance()->getCustomer() )
				$bvat = DBUtil::getDBValue( "bvat", "state", "idstate", DBUtil::getParameter( "default_idstate" ) );
		else 	$bvat = DBUtil::getDBValue( "bvat", "state", "idstate", Session::getInstance()->getCustomer()->getAddress()->getIdState() );

	 if( $bvat ){
	 	
	        if( isset($this->article['tva']) ) 
	        	return $this->article['tva'];
	        	
			$this->article['tva'] = 0;
			
			if( !isset($this->article['idvat']) ) 
				return 0;
			
			$IdVAT = $this->article['idvat'];
			$Base =& DBUtil::getConnection();
	        $rs =& DBUtil::query("select tva from tva where idtva='$IdVAT'");
	       
	        if($rs->RecordCount()>0)
	                $this->article['tva'] = $rs->Fields('tva');
	
	        return $this->article['tva'];
	  }
	  return 0;
	}
	
	/**
	 * Retourne la valeur du stock
	 * @return int la valeur du stock
	 */
	
	public function GetStock(){
		return $this->get("stock_level");
	}
	
	
	/**
	 * Contrôle le stock disponible
	 * @param int $qte la quantité
	 * @return bool true si stock suffisant, false sinon
	 */
	
	public function VerifyStock($qte){
		$stock = $this->GetStock();
		
		if($qte>$stock){
			return false;
		}else{
			return true;
		}
	}
	
	//---------------------------------------------------------------------------------------------------------
	/**
	 * Affecte une couleur à l'article
	 * @param int $idcolor, l'id de la couleur
	 * @return bool true si on peut affecter une couleur false sinon
	 */
	
	public function setColor( $idcolor ){
		
		/*
		 * @todo : revoir le constructeur de l'objet
		 * => l'objet devrait initialiser ses données membres lui-même
		 * pour plus de souplesse dans les devis/commandes
		 */
		
		$db =& DBUtil::getConnection();
		
		$idproduct = $this->get( "idproduct" );
		$idsupplier = $this->get( "idsupplier" );
		
		//infos couleur au niveau du produit
		
		$query = "
		SELECT manage, use_color_1, use_color_2, use_color_3, use_color_4, use_color_5
		FROM product
		WHERE idproduct = '$idproduct'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les informations pour la couleur du produit '$idproduct'" );
			
		if( !$rs->RecordCount() )
			return false;
			
		$manage = $rs->fields( "manage" );
		if( empty( $manage ) ) // pas de gestion de tissus/couleurs si 0 ou vide
			return false; 

		$i = 1;
		while( $i < 6 ){
		
			$this->article[ "use_color_$i" ] = $rs->fields( "use_color_$i" );
			
			$i++;
			
		}
		
		//???
		
		if( !empty( $this->article[ "table" ] ) ) {
		
 			$table = $this->article[ "table" ];
 			
 			if( $this->get( "use_color_$table" ) <= 0 ) // pas de gestion de tissus/couleurs
 				return false; 
 			
		}

		//supplément dû à  la couleur
		
		$query = "
		SELECT * FROM color_product
		WHERE idcolor = '$idcolor' 
		AND idproduct = '$idproduct'
		LIMIT 1";

		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer les informations pour la couleur '$idcolor' du produit '$idproduct'" );
			
		if( !$rs->RecordCount() )
			return false;

		$price = $rs->fields( "price" );
		$percent = $rs->fields( "percent" );
		$delay = $rs->fields( "delay" );
		
		//code couleur
		
		$query = "
		SELECT code_color FROM color
		WHERE idcolor = '$idcolor' 
		AND idsupplier = '$idsupplier'
		LIMIT 1";
		
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le code couleur pour la couleur '$idcolor' du fournisseur '$idsupplier'" );
			
		if( !$rs->RecordCount() )
			return false;
		
		$code_color = $rs->fields( "code_color" );

		//modification du prix de base
		
		if( $price > 0.0 ){
		
			$this->BasicPrice = $this->get( "sellingcost" ) + $price;
			$this->option_price = $this->BasicPrice - $this->get( "sellingcost" );
			
		}
		else if( $percent > 0.0 ){
		
			$this->BasicPrice = $this->get( "sellingcost" ) + $this->article[ "sellingcost" ] * ( $percent / 100.0 );
			$this->option_price = $this->BasicPrice - $this->get( "sellingcost" );
			
		}	
		else{
		
			$this->BasicPrice = $this->get( "sellingcost" );
			$this->option_price = 0.0;
			
		}
		
		$color = array(
		
			"price" 		=> $price,
			"percent" 		=> $percent,
			"delay" 		=> $delay,
			"idcolor" 		=> $idcolor,
			"code_color" 	=> $code_color,
			
		);
		
		$this->article[ "ref_extra" ] = $this->article[ "reference" ] . "-" . $code_color;
		$this->delay = $delay;
		$this->article[ "designation" ] .= "<br />" . Dictionnary::translate( "color" ) . " : " . $code_color;

		//affecter la couleur
		
		$this->Couleurs = array( $idcolor => $color );
		$this->Selected_Couleurs = array( $idcolor );
		$this->idcolor = $idcolor;
		
		return true;
		
	}

	//---------------------------------------------------------------------------------------------------------
	
	/**
	 * @todo les enregistrements dans la table destocking sont reliés à la table detail par la colonne référence
	 * Il faut utiliser des colonnes uniques pour ce genre de choses => préférer idarticle
	 * @todo ALTER table `detail` CHANGE `reference` `reference` VARCHAR( 50 ) UNIQUE NOT NULL
	 */
	 
	public static function isDestocking( $idarticle ){

		$query = "
		SELECT COUNT(*) AS isDestocking 
		FROM destocking, detail
		WHERE detail.idarticle = '$idarticle'
		AND detail.reference = destocking.reference
		AND destocking.begin_date <= NOW() AND destocking.end_date >= NOW()";
		
		$db = & DBUtil::getConnection();
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() || $rs->fields( "isDestocking" ) == null )
			die( "Impossible de vérifier si l'article n°$idarticle est en déstockage" );
			
		return $rs->fields( "isDestocking" ) > 0;
		
	}
	
	//---------------------------------------------------------------------------------------------------------
	
	/**
	 * @todo les enregistrements dans la table promo sont reliés à la table detail par la colonne référence
	 * Il faut utiliser des colonnes uniques pour ce genre de choses => préférer idarticle
	 * @todo utiliser idarticle et non reference dans la table promo car la reference n'est pas UNIQUE
	 * ou => ALTER table `detail` CHANGE `reference` `reference` VARCHAR( 50 ) UNIQUE NOT NULL
	 */
	 
	public static function hasPromo( $idarticle ){

		$query = "
		SELECT COUNT(*) AS hasPromo 
		FROM promo, detail
		WHERE detail.idarticle = '$idarticle'
		AND detail.reference = promo.reference
		AND promo.begin_date <= NOW() AND promo.end_date >= NOW()";
		
		$db =& DBUtil::getConnection();
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() || $rs->fields( "hasPromo" ) == null )
			die( "Impossible de vérifier si l'article n°$idarticle est en promotion" );
			
		return $rs->fields( "hasPromo" ) > 0;
		
	}
	
	//---------------------------------------------------------------------------------------------------------
	/**
	
	/**
	 * Destockage d'une référence donnée
	 * @param int idarticle l'identifiant de l'article à destocker
	 * @param int quantity la quantité à destocker
	 * @return void
	 */
	public static function unstock( $idarticle, $quantity ){
				
		$val_stock = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
		
		if( $val_stock )
			return;
				
		$stock = max( 0, $val_stock - $quantity );
					
		$rssto =& DBUtil::query( "UPDATE detail SET stock_level = '$stock' WHERE idarticle = '$idarticle' LIMIT 1" );
				
		if( $rssto === false )
			die( "Impossible de mettre a jour le stock<br />$query" );
					
	}
	
	//---------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne true si un article donné est disponible à l'affichage sur le front office
	 * @access public
	 * @static
	 * @param int $idarticle
	 * @return bool
	 */
	public static function isAvailable( $idarticle ){
		
		$query = "SELECT idproduct, available * stock_level AS `available` FROM detail WHERE idarticle = '$idarticle' LIMIT 1";
		
		$rs =& DBUtil::query( $query );

		return $rs->fields( "available" ) != 0 && PRODUCT::isAvailable( $rs->fields( "idproduct" ) );

	}
	
	//---------------------------------------------------------------------------------------------------------
	
	/**
	 * magique!
	 */
	 
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
}//EOC
?>
