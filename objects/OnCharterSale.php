<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object implémentation des frais de port fournisseur 
 */


include_once( dirname( __FILE__ ) . "/DBSale.php" );


abstract class OnCharterSale extends DBSale{
	
	//----------------------------------------------------------------------------
	
	public static $ALWAYS_FRANCO 	= 9;	//franco permanent
	public static $EX_FACTORY 		= 12; 	//prix départ usine
	
	//----------------------------------------------------------------------------
	/**
	 * @var ChargesStrategy $chargesStrategy
	 */
	protected $chargesStrategy;
	
	/* ----------------------------------------------------------------------------------------------------- */
	/**
	 * @access protected
	 * @return void
	 */
	protected function setChargesStrategy(){
		
		$rs =& DBUtil::query( 
		
			"SELECT strategy 
			FROM charge_strategy 
			WHERE `datetime` <= '" . $this->recordSet[ "DateHeure" ] . "'
			ORDER BY datetime DESC
			LIMIT 1" 
		
		);
		
		if( !$rs->RecordCount() 
			|| !file_exists( dirname( __FILE__ ) . "/strategies/" . $rs->fields( "strategy" ) . ".php" ) ){

			return;
            trigger_error( "stratégie de calcul de frais de port inconnue", E_USER_ERROR );
			exit();
			
		}

		$strategy = $rs->fields( "strategy" );
		
		include_once( dirname( __FILE__ ) . "/strategies/{$strategy}.php" );
			
		$this->chargesStrategy = new $strategy( $this );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @access public
	 * @return ChargesStrategy
	 */
	public function getChargesStrategy(){ return $this->chargesStrategy; }
	
	//----------------------------------------------------------------------------
	/**
	 * Récupére tous les identifiants des fournisseur utilisés pour le devis/la commande
	 * @return array un tableau indexé contenant les identifiants fournisseur
	 */
	 
	public function getDistinctIdSuppliers(){
		
		$i = 0;
		$idsuppliers = array();
		while( $i < $this->items->size() ){
			
			$idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
			if( !empty( $idsupplier ) && !in_array( $idsupplier, $idsuppliers ) )
				$idsuppliers[] = $idsupplier;
				
			$i++;
			
		}
		
		return $idsuppliers;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne le montant des frais de port fournisseur pour toutes les références d'un fournisseur donné
	 * @param int idsupplier l'identifiant du fournisseur
	 * @param bool $forceAuto force le calcul automatique
	 * @return float le montant des frais de port fournisseur pour toutes les références d'un fournisseur donné
	 */
	 
	public function getSupplierCharges( $idsupplier, $forceAuto = false ){
		
		if( empty( $idsupplier ) )
			return 0.0;

		//frais de port manuels
		
		if( !$forceAuto && !$this->hasAutomaticSupplierCharge( $idsupplier ) ){
			// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "
			SELECT charges FROM " . $this->TABLE_NAME . "_charges
			WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
			AND idsupplier = '$idsupplier'
			LIMIT 1";
			
			$rs = DBUtil::query( $query );
			if( $rs === false )
				die( "Impossible de récupérer les frais de port pour le fournisseur $idsupplier" );
				
			if( !$rs->RecordCount() )
				return 0.0;
				
			return $rs->fields( "charges" );
			
		}
		
		//frais de port auto
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		$isInternalSupplier = $idsupplier == $internal_supplier;
		
		$select_charge = $this->getSupplierChargeSelect( $idsupplier );
		//print $select_charge;exit;
		if( !$select_charge )
			return 0.0;
			
		switch( $select_charge ){
			
			//calcul basé sur le pourcentage du montant d'achat
			
			case 1 :
			// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT charge_rate FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer le pourcentage à récupérer sur le montant d'achat pour le fournisseur $idsupplier" );

				$charge_rate = $rs->fields( "charge_rate" );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$total_charges = round( $buying_amount * $charge_rate / 100.0, 2 );
			
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						$total_charges = 0.0;
				
				}

				//echo "<br />supplier_charges: [ $idsupplier ] $total_charges";

				return $total_charges;

			//calcul par tranche de poids sans zone départementale
			
			case 2 :
			
				$total_weight = $this->getSupplierWeight( $idsupplier );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT cs.value AS charges
				FROM charge_supplier cs, charge c
				WHERE cs.idsupplier = '$idsupplier'
				AND cs.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight > '$total_weight'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer les frais de ports fournisseur par tranche de poids" );
					
				return $rs->fields( "charges" );
		
			//frais de port proportionnels au poids + charges fixes
			
			/*case 3 :
			
				$weight = $this->getSupplierWeight( $idsupplier );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$query = "SELECT charge_fixed, charge_amount FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer les frais de ports fournisseur par tranche de poids avec charges fixes" );
					
				$charge_fixed = $rs->fields( "charge_fixed" );
				$charge_amount = $rs->fields( "charge_amount" );
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				
				$charges = $charge_fixed + $weight * $charge_amount;

				return $charge_fixed + $weight * $charge_amount;*/
	
			//calcul par zone départementale et par tranche de poids
			
			case 4 :

				$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				
				if( $transit ){ //livraison chez fournisseur interne
				// ------- Mise à jour de l'id gestion des Index  #1161
					$query = "SELECT zipcode FROM supplier WHERE idsupplier = '$internal_supplier'";
					
					$rs = DBUtil::query( $query );
					if( $rs === false || !$rs->RecordCount() )
						die( "Impossible de récupérer le code postal du fournisseur interne" );
					
					$zipcode = $rs->fields( "zipcode" );
					
				}
				else{ //livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers */
					if( $idstate != 1 ) //code postal étranger
						return 0.0;
					
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;
				
				$area = substr( $zipcode, 0, 2 );
				$total_weight = $this->getSupplierWeight( $idsupplier );
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT csr.value
				FROM charge_supplier_region csr, charge c
				WHERE csr.idsupplier = '$idsupplier'
				AND csr.idregion = $area
				AND csr.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight >= '$total_weight'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de poids" );
				
				$charges = $rs->fields( "value" );
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				
				return $charges;
				
			//calcul par région et par tranche de poids
			
			case 5 :
			
				$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				// ------- Mise à jour de l'id gestion des Index  #1161
				if( $transit ){ //livraison chez fournisseur interne
				
					$query = "SELECT zipcode FROM supplier WHERE idsupplier = '$internal_supplier'";
					
					$rs = DBUtil::query( $query );
					if( $rs === false || !$rs->RecordCount() )
						die( "Impossible de récupérer le code postal du fournisseur interne" );
					
					$zipcode = $rs->fields( "zipcode" );
					
				}
				else{ //livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers, ne pas utiliser idstate = 1*/
					if( $idstate != 1 ) //code postal étranger
						return 0.0;
						
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;
				
				$area = substr( $zipcode, 0, 2 );
				$total_weight = $this->getSupplierWeight( $idsupplier );
	// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT csa.value, csa.fixed_rate, csa.value_weight
				FROM area a, area_region ar, charge_supplier_area csa, charge c
				WHERE ar.idregion = $area
				AND ar.idarea = a.idarea
				AND a.idsupplier = '$idsupplier'
				AND a.idarea = csa.idarea
				AND csa.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight >= '$total_weight'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de poids" );
				
				$value 			= $rs->fields( "value" );
				$fixed_rate 	= $rs->fields( "fixed_rate" );
				$value_weight 	= $rs->fields( "value_weight" );
				
				if( $value_weight > 0.0 )
					$charges = $fixed_rate + round( $value_weight * $total_weight, 2 );
				else $charges = $value;
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				
				return $charges;
			
			//calcul par tranche de prix sans zone départementale
			
			case 6 :
			
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT cs.value AS charges
				FROM charge_supplier cs, charge c
				WHERE cs.idsupplier = '$idsupplier'
				AND cs.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer les frais de ports fournisseur par tranche de prix" );
					
				return $rs->fields( "charges" );
			
			//calcul par zone départementale et par tranche de prix
			
			case 7 :

				$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				
				if( $transit ){ //livraison chez fournisseur interne
				// ------- Mise à jour de l'id gestion des Index  #1161
					$query = "SELECT zipcode FROM supplier WHERE idsupplier = '$internal_supplier'";
					
					$rs = DBUtil::query( $query );
					if( $rs === false || !$rs->RecordCount() )
						die( "Impossible de récupérer le code postal du fournisseur interne" );
					
					$zipcode = $rs->fields( "zipcode" );
					
				}
				else{ //livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers */
					if( $idstate != 1 ) //code postal étranger
						return 0.0;
					
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;
				
				$area = substr( $zipcode, 0, 2 );
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT csr.value
				FROM charge_supplier_region csr, charge c
				WHERE csr.idsupplier = '$idsupplier'
				AND csr.idregion = $area
				AND csr.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de prix" );
				
				$charges = $rs->fields( "value" );
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				
				return $charges;
				
			//calcul par région et par tranche de prix
			
			case 8 :
			
				$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
				$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				
				if( $transit ){ //livraison chez fournisseur interne
				// ------- Mise à jour de l'id gestion des Index  #1161
					$query = "SELECT zipcode FROM supplier WHERE idsupplier = '$internal_supplier'";
					
					$rs = DBUtil::query( $query );
					if( $rs === false || !$rs->RecordCount() )
						die( "Impossible de récupérer le code postal du fournisseur interne" );
					
					$zipcode = $rs->fields( "zipcode" );
					
				}
				else{ //livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers */
					if( $idstate != 1 ) //code postal étranger
						return 0.0;
						
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;
				
				$area = substr( $zipcode, 0, 2 );
	// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT csa.value, csa.fixed_rate, csa.value_price
				FROM area a, area_region ar, charge_supplier_area csa, charge c
				WHERE ar.idregion = $area
				AND ar.idarea = a.idarea
				AND a.idsupplier = '$idsupplier'
				AND a.idarea = csa.idarea
				AND csa.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de prix" );
				
				$value 			= $rs->fields( "value" );
				$fixed_rate 	= $rs->fields( "fixed_rate" );
				$value_price 	= $rs->fields( "value_price" );
				
				if( $value_price > 0.0 )
					$charges = $fixed_rate + round( $value_price * $buying_amount, 2 );
				else $charges = $value;
				
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						return 0.0;
				
				}
				
				return $charges;
			
			//toujours franco
			
			case 9 : return 0.0;
			
			//montant fixe par nombre d'articles
			
			case 10 :
				
				$it = $this->items->iterator();
				
				$quantity = 0;
				while( $it->hasNext() ){
					
					$item =& $it->next();
					
					if( $item->get( "idsupplier" ) == $idsupplier )
						$quantity += $item->get( "external_quantity" );
					
				}
				
				if( !$quantity )
					return 0.0;
	
				$rs =& DBUtil::query( "SELECT value FROM charge_supplier_quantity WHERE idsupplier = '$idsupplier' AND quantity <= '$quantity' ORDER BY quantity DESC LIMIT 1" );
				
				return $rs->RecordCount() ? round( $quantity * $rs->fields( "value" ), 2 ) : 0.0;

			//par département et forfait par tranche de prix puis % du montant des achats
			
			case 11 :
					/*$internal_supplier 	= DBUtil::getParameterAdmin( "internal_supplier" );
			$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				
				if( $transit ) //livraison chez fournisseur interne
					$zipcode = DBUtil::getDBValue( "zipcode", "supplier", "idsupplier", $internal_supplier );
				else{ 
					
					//livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers */
				/*	if( $idstate != 1 ) //code postal étranger
						return 0.0;
					
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}
				
				$query = "
				SELECT value, type
				FROM charge_supplier_fixed_price_and_rate
				WHERE idsupplier = '$idsupplier'
				AND maximum <= '" . round( $buying_amount, 0 ) . "'
				AND idregion = '" . substr( $zipcode, 0, 2 ) . "'
				ORDER BY maximum ASC
				LIMIT 1";
				
				$rs =& DBUtil::query( $query );
				//var_dump( $rs);exit;
				if( !$rs->RecordCount() )
					return 0.0;
				
				return $rs->fields( "type" ) == "rate" ? round( $rs->fields( "value" ) * $buying_amount / 100.0, 2 ) : $rs->fields( "value" );*/
				$internal_supplier 	= DBUtil::getParameterAdmin( "internal_supplier" );
				$buying_amount 		= $this->getSupplierBuyingAmount( $idsupplier );
			
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				
				if( $transit ) //livraison chez fournisseur interne
					$zipcode = DBUtil::getDBValue( "zipcode", "supplier", "idsupplier", $internal_supplier );
				else{ 
					
					//livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers */
					if( $idstate != 1 ) //code postal étranger
						return 0.0;
					
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT csa.fixed_rate,csa.value,csa.value_weight FROM charge_supplier csa, charge c WHERE 
				csa.idsupplier = '$idsupplier' 
				AND csa.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'
				LIMIT 1";
				
				$rs = DBUtil::query( $query );
				
				//if( $rs === false || !$rs->RecordCount() )
					//return 0.0; //die( "Impossible de récupérer le pourcentage à récupérer sur le montant d'achat pour le fournisseur $idsupplier" );
				$fixed_rate = $rs->fields( "fixed_rate" );
					$value = $rs->fields( "value" );
					$value_weight = $rs->fields( "value_weight" );
					if( $value_weight > 0.0 )
				return $total_charges = $value;	
				else {
				
				$charge_rate = $fixed_rate;
				if($charge_rate == 0.0){
				// ------- Mise à jour de l'id gestion des Index  #1161
				$querys = "SELECT su.charge_rate FROM supplier su, charge_supplier csa, charge c WHERE 
				su.idsupplier = '$idsupplier'
				AND csa.idsupplier = su.idsupplier 
				AND csa.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'
				LIMIT 1";
				$rss = DBUtil::query( $querys );
				
				//if( $rss === false || !$rss->RecordCount() )
				//	return 0.0; //die( "Impossible de récupérer le pourcentage à récupérer sur le montant d'achat pour le fournisseur $idsupplier" );
				$charge_rate = $rss->fields( "charge_rate" );
				
				}
				
				//$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
				//print $buying_amount;exit;
				$total_charges = round( $buying_amount * $charge_rate / 100.0, 2 );
			//test
				//franco fournisseur (sauf fournisseur interne )
				
				if( !$isInternalSupplier ){
					
					$franco_supplier = $this->getSupplierFranco( $idsupplier );
					
					if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
						$total_charges = 0.0;
				
				}

				//echo "<br />supplier_charges: [ $idsupplier ] $total_charges";

				return $total_charges;
				}
			/*	$query = "
				SELECT value, type
				FROM charge_supplier_fixed_price_and_rate
				WHERE idsupplier = '$idsupplier'
				AND maximum >= '" . round( $buying_amount, 0 ) . "'
				AND idregion = '" . substr( $zipcode, 0, 2 ) . "'
				ORDER BY maximum ASC
				LIMIT 1";
				
				$rs = DBUtil::query( $query );
				//var_dump($query);exit;
				if( !$rs->RecordCount() )
					return 0.0;
				
				return $rs->fields( "type" ) == "rate" ? round( $rs->fields( "value" ) * $buying_amount / 100.0, 2 ) : $rs->fields( "value" );
				*/
			case 12 :
			default : return 0.0;
			
		}

	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si les références d'un fournisseur donné transitent par Lutange ou non
	 * @param int idsupplier le fournisseur
	 * @return bool true si les références du fournisseurs transitent par lutange, sinon false
	 */
	public function isSupplierUsingInternalSupplier( $idsupplier ){
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		
		if( empty( $idsupplier ) || $internal_supplier == $idsupplier )
			return false;
			
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
		
		if( $transit )
			return true;
			
		$useInternalSupplier = false;
		$i = 0;
		while( !$useInternalSupplier && $i < $this->items->size() ){
		
			$hasInternalQuantity = $this->getItemAt( $i)->get( "quantity" ) > $this->getItemAt( $i)->get( "external_quantity" );
			$useInternalSupplier |= $hasInternalQuantity;
				
			$i++;
			
		}
		
		return $useInternalSupplier;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le montant des frais de port supplémentaires Dépôt->Client pour toutes les références d'un fournisseur autre
	 * que le fournisseur interne
	 * Références concernés :
	 * 	- références des fournisseurs ( passage par le dépôt )
	 * 	- références des autres fournisseurs avec stock interne
	 * @param int idsupplier l'identifiant du fournisseur
	 * @param bool $forceAuto force le recalcul
	 * @return float le montant des frais de port fournisseur pour toutes les références d'un fournisseur donné
	 */
	 
	public function getInternalSupplierCharges( $idsupplier, $forceAuto = false ){
				
		if( empty( $idsupplier ) )
			return 0.0;
			
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		$isInternalSupplier = $idsupplier == $internal_supplier;
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
		
		//vérifier s'il s'agit du fournisseur interne
		
		/*if( $isInternalSupplier )
			return 0.0;*/
				
		//frais de port manuels
		
		if( !$forceAuto && !$this->hasAutomaticInternalSupplierCharge( $idsupplier ) ){
			// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "
			SELECT internal_supplier_charges FROM " . $this->TABLE_NAME . "_charges
			WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
			AND idsupplier = '$idsupplier'
			LIMIT 1";
			
			$rs = DBUtil::query( $query );
			if( $rs === false )
				die( "Impossible de récupérer les frais de port pour le fournisseur $idsupplier" );
				
			if( !$rs->RecordCount() )
				return 0.0;
				
			return $rs->fields( "internal_supplier_charges" );
			
		}
		
		//frais de port auto
		
		//récupérer la méthode de calcul pour le fournisseur interne
		
		$select_charge = $this->getSupplierChargeSelect( $internal_supplier );
		
		if( !$select_charge )
			return 0.0;
			
		//vérifier si les références du fournisseur transitent par lutange ( fournisseur ou si stock interne )
		
		if( !$this->isSupplierUsingInternalSupplier( $idsupplier ) )
			return 0.0;
			
		//récupérer les références concernées par le port fournisseur interne
		
		$i = 0;
		$buying_amount = 0.0;
		$total_weight = 0.0;
		while( $i < $this->items->size() ){
		
			$ref_idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
			
			if( $ref_idsupplier == $idsupplier ){
				
				if( $transit )
						$quantity = $this->getItemAt( $i)->get( "quantity" );
				else	$quantity = $this->getItemAt( $i)->get( "quantity" ) - $this->getItemAt( $i)->get( "external_quantity" );
				
				$buying_amount += $this->getItemAt( $i)->get( "unit_cost_amount" ) * $quantity;
				$total_weight += $this->getItemAt( $i)->get( "weight" ) * $quantity;	
				
			}
			
			$i++;
			
		}
		
		//calcul des frais de port fournisseur interne

		switch( $select_charge ){
		
			//calcul basé sur le pourcentage du montant d'achat
			
			case 1 :
			// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT charge_rate FROM supplier WHERE idsupplier = '$internal_supplier' LIMIT 1";
				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer le pourcentage à récupérer sur le montant d'achat pour le fournisseur interne" );

				$charge_rate = $rs->fields( "charge_rate" );
				$total_charges = round( $buying_amount * $charge_rate / 100.0, 2 );
			
				//note : pas de franco appliqué pour le fournisseur interne
				
				return $total_charges;

			//calcul par tranche de poids sans zone départementale
			
			case 2 :

				//note : pas de franco appliqué pour le fournisseur interne
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT cs.value AS charges
				FROM charge_supplier cs, charge c
				WHERE cs.idsupplier = '$internal_supplier'
				AND cs.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight > '$total_weight'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer les frais de ports fournisseur par tranche de poids" );
					
				return $rs->fields( "charges" );
		
			//frais de port proportionnels au poids + charges fixes
			
			/*case 3 :
			
				$query = "SELECT charge_fixed, charge_amount FROM supplier WHERE idsupplier = $internal_supplier LIMIT 1";
				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer les frais de ports fournisseur par tranche de poids avec charges fixes" );
					
				$charge_fixed = $rs->fields( "charge_fixed" );
				$charge_amount = $rs->fields( "charge_amount" );
				
				//note : pas de franco appliqué pour le fournisseur interne

				$charges = $charge_fixed + $total_weight * $charge_amount;

				return $charge_fixed + $total_weight * $charge_amount;*/
	
			//calcul par zone départementale et par tranche de poids
			
			case 4 :
			
				$idstate = $this->forwardingAddress->getIdState();
				
				/* TODO : PAs de frais de port calculables pour pays étrangers */
				if( $idstate != 1 )
					return 0.0;
						
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;

				$area = substr( $zipcode, 0, 2 );
				// ------- Mise à jour de l'id gestion des Index  #1161			
				$query = "
				SELECT csr.value
				FROM charge_supplier_region csr, charge c
				WHERE csr.idsupplier = '$internal_supplier'
				AND csr.idregion = $area
				AND csr.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight >= '$total_weight'";

				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de poids" );

				return $rs->fields( "value" );
			
			//calcul par zone départementale et par tranche de poids
			
			case 5 :
			
				$idstate = $this->forwardingAddress->getIdState();
				
				/* TODO : PAs de frais de port calculables pour pays étrangers */
				if( $idstate != 1 )
					return 0.0;
						
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;

				$area = substr( $zipcode, 0, 2 );
				// ------- Mise à jour de l'id gestion des Index  #1161			
				$query = "SELECT csa.value, csa.fixed_rate, csa.value_weight
				FROM area a, area_region ar, charge_supplier_area csa, charge c
				WHERE ar.idregion = $area
				AND ar.idarea = a.idarea
				AND a.idsupplier = '$idsupplier'
				AND a.idarea = csa.idarea
				AND csa.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight >= '$total_weight'";

				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de poids" );

				$value = $rs->fields( "value" );
				$fixed_rate = $rs->fields( "fixed_rate" );
				$value_weight = $rs->fields( "value_weight" );
				
				if( $value_weight > 0.0 )
					return $fixed_rate + round( $value_weight * $total_weight, 2 );
				
				return $rs->fields( "value" );
		
			//calcul par tranche de prix sans zone départementale
			
			case 6 :

				//note : pas de franco appliqué pour le fournisseur interne
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT cs.value AS charges
				FROM charge_supplier cs, charge c
				WHERE cs.idsupplier = '$internal_supplier'
				AND cs.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";
				
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de récupérer les frais de ports fournisseur par tranche de prix" );
					
				return $rs->fields( "charges" );

			case 7 :
			
				$idstate = $this->forwardingAddress->getIdState();
				
				/* TODO : PAs de frais de port calculables pour pays étrangers */
				if( $idstate != 1 )
					return 0.0;
						
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;

				$area = substr( $zipcode, 0, 2 );
					// ------- Mise à jour de l'id gestion des Index  #1161		
				$query = "
				SELECT csr.value
				FROM charge_supplier_region csr, charge c
				WHERE csr.idsupplier = '$internal_supplier'
				AND csr.idregion = $area
				AND csr.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";

				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de prix" );

				return $rs->fields( "value" );
			
			//calcul par zone départementale et par tranche de prix
			
			case 8 :
			
				$idstate = $this->forwardingAddress->getIdState();
				
				/* TODO : PAs de frais de port calculables pour pays étrangers */
				if( $idstate != 1 )
					return 0.0;
						
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return 0.0;
// ------- Mise à jour de l'id gestion des Index  #1161
				$area = substr( $zipcode, 0, 2 );
							
				$query = "SELECT csa.value, csa.fixed_rate, csa.value_price
				FROM area a, area_region ar, charge_supplier_area csa, charge c
				WHERE ar.idregion = $area
				AND ar.idarea = a.idarea
				AND a.idsupplier = '$idsupplier'
				AND a.idarea = csa.idarea
				AND csa.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";

				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return 0.0; //die( "Impossible de calculer les frais de port fournisseur par zone départementale et tranche de prix" );

				$value = $rs->fields( "value" );
				$fixed_rate = $rs->fields( "fixed_rate" );
				$value_price = $rs->fields( "value_price" );
				
				if( $value_price > 0.0 )
					return $fixed_rate + round( $value_price * $buying_amount, 2 );
				
				return $rs->fields( "value" );
			
			//toujours franco
			
			case 9 : return 0.0;
			
			//montant fixe par nombre d'articles
			
			case 10 :
				
				$it = $this->items->iterator();
				
				$quantity = 0;
				while( $it->hasNext() ){
					
					$item =& $it->next();
					
					if( $item->get( "idsupplier" ) == $idsupplier )
						$quantity += $item->get( "external_quantity" );
				
				}
				
				if( !$quantity );
					return 0.0;
	
				$rs =& DBUtil::query( "SELECT value FROM charge_supplier_quantity WHERE idsupplier = '$idsupplier' AND quantity <= '$quantity' ORDER BY quantity DESC LIMI 1" );
				
				return $rs->RecordCount() ? round( $quantity * $rs->fields( "value" ), 2 ) : 0.0;
			
				//par département et forfait par tranche de prix puis % du montant des achats
			
			case 11 :
				
				$internal_supplier 	= DBUtil::getParameterAdmin( "internal_supplier" );
				$buying_amount 		= $this->getSupplierBuyingAmount( $idsupplier );
				
				$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
				$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				
				if( $transit ) //livraison chez fournisseur interne
					$zipcode = DBUtil::getDBValue( "zipcode", "supplier", "idsupplier", $internal_supplier );
				else{ 
					
					//livraison chez le client

					$idstate = $this->forwardingAddress->getIdState();
				
					/* TODO : PAs de frais de port calculables pour pays étrangers */
					if( $idstate != 1 ) //code postal étranger
						return 0.0;
					
					$zipcode = $this->forwardingAddress->getZipcode();
					
				}
				
				$query = "
				SELECT value, type
				FROM charge_supplier_fixed_price_and_rate
				WHERE idsupplier = '$idsupplier'
				AND maximum >= '" . round( $buying_amount, 0 ) . "'
				AND idregion = '" . substr( $zipcode, 0, 2 ) . "'
				ORDER BY maximum ASC
				LIMIT 1";
				
				$rs =& DBUtil::query( $query );
				
				if( !$rs->RecordCount() )
					return 0.0;
				
				return $rs->fields( "type" ) == "rate" ? round( $rs->fields( "value" ) * $buying_amount / 100.0, 2 ) : $rs->fields( "value" );

			case 12 :
			default : return 0.0;
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @param int $idsupplier
	 * @return int l'identifiant du transporteur
	 */
	public function getSupplierCarrier( $idsupplier ){
		
		$query = "
		SELECT idcarrier FROM " . $this->TABLE_NAME . "_charges
		WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
		AND idsupplier = '" .  $idsupplier  . "'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( !$rs->RecordCount() )
			return 0;
			
		return $rs->fields( "idcarrier" );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Calcul les frais de port fournisseur pour tous les articles
	 * @return float le montant des frais de port fournisseur pour tous les articles
	 */
	 
	public function getSupplierTotalCharges(){
		
		//récupérer les fournisseurs distincts
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		
		$idsuppliers = array();
		$i = 0;
		while( $i < $this->getItemCount() ){
			
			$idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
			
			if( !in_array( $idsupplier, $idsuppliers ) )
				$idsuppliers[] = $idsupplier;
			
			$i++;
			
		}

		$supplier_charges = 0.0;	
		$i = 0;
		while( $i < count( $idsuppliers ) ){
		
			$idsupplier = $idsuppliers[ $i ];
			
			$supplier_charges += $this->getSupplierCharges( $idsupplier );
			if( $idsupplier != $internal_supplier )
				$supplier_charges += $this->getInternalSupplierCharges( $idsupplier );
			
			$i++;
			
		}

		return $supplier_charges;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le mode de calcul des frais de ports fournisseur pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return int le mode de calcul des frais de ports fournisseur pour un fournisseur donné
	 */
	 
	private function getSupplierChargeSelect( $idsupplier ){
			// ------- Mise à jour de l'id gestion des Index  #1161	
		$query = "SELECT select_charge FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
		$rs = DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			return 0;
		
		//echo "<br />select_charge: [ $idsupplier ] " . $rs->fields( "select_charge" );
		
		return $rs->fields( "select_charge" );
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne l'identifiant du pays pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return int l'identifiant du pays pour un fournisseur donné
	 */
	 
	public function getSupplierIdState( $idsupplier ){
	// ------- Mise à jour de l'id gestion des Index  #1161			
		$query = "SELECT idstate FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
		$rs = DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le pays du fournisseur $idsupplier" );
		
		//echo "<br />idstate: [ $idsupplier ] " . $rs->fields( "idstate" );
		
		return $rs->fields( "idstate" );
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne l'identifiant du pays pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return int
	 */
	 
	public function getSupplierTransit( $idsupplier ){
			// ------- Mise à jour de l'id gestion des Index  #1161	
		$query = "SELECT transit FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
		$rs = DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de vérifier si le fournisseur '$idsupplier' transite par le fournisseur interne" );
			
		return $rs->fields( "transit" );
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le franco pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float le franco pour un fournisseur donné
	 */
	 
	public function getSupplierFranco( $idsupplier ){
				// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "SELECT franco_supplier FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
		$rs = DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le mode de calcul des frais de port pour le fournisseur $idsupplier" );
		
		return ( float )$rs->fields( "franco_supplier" );
			
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le montant d'achat HT total des articles pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float
	 */
	 
	public function getSupplierBuyingAmount( $idsupplier ){
		
		//fournisseur interne
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		
		//$isInternalSupplier =$idsupplier = $internal_supplier;
		if($idsupplier ==$internal_supplier)
		$isInternalSupplier =true;
				else
		$isInternalSupplier =false;
		$i = 0;
		$buying_amount = 0.0;
		while( $i < $this->getItemCount() ){
			
			$ref_idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
			$unit_cost =  $this->getItemAt( $i)->get( "unit_cost_amount" );
			$quatity = $this->getItemAt( $i)->get( "quantity" );
			$external_quantity = $this->getItemAt( $i)->get( "external_quantity" );

			if( $ref_idsupplier == $idsupplier ){
				
				/*if( $isInternalSupplier )
						$buying_amount += round( $unit_cost, 2 ) * $quatity;
				else 	$buying_amount += round( $unit_cost, 2 ) * $external_quantity;				*/
				$buying_amount += round( $unit_cost, 2 ) * $quatity;

			}

			$i++;
			
		}
//print $buying_amount; exit;
		return $buying_amount;
				
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le montant d'achat HT total des articles qui transitent par lutange pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float
	 */
	 
	public function getInternalSupplierBuyingAmount( $idsupplier ){
		
		if( empty( $idsupplier ) )
			return 0.0;
			
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		//$isInternalSupplier = $idsupplier == $internal_supplier;
		if($idsupplier ==$internal_supplier)
		$isInternalSupplier =true;
				else
		$isInternalSupplier =false;
		
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
		
		if( $isInternalSupplier )
			return 0.0;
			
		$i = 0;
		$buying_amount = 0.0;
		while( $i < $this->getItemCount() ){
			
			$ref_idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
			
			
			if( $transit )
				$quantity = $this->getItemAt( $i)->get( "quantity" );
			else $quantity = $this->getItemAt( $i)->get( "quantity" ) - $this->getItemAt( $i)->get( "external_quantity" );
			
			if( $ref_idsupplier == $idsupplier )
				$buying_amount += round( $this->getItemAt( $i)->get( "unit_cost_amount" ), 2 ) * $quantity;

			$i++;
			
		}

		return $buying_amount;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le montant HT total des articles pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float
	 */
	 
	public function getSupplierOrderAmount( $idsupplier ){
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		//$isInternalSupplier = $idsupplier == $internal_supplier;
		if($idsupplier ==$internal_supplier)
		$isInternalSupplier =true;
				else
		$isInternalSupplier =false;
				
		$orderAmount = 0.0;
		$i = 0;
		while( $i < $this->getItemCount() ){
			
			$ref_idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
	
			if( $ref_idsupplier == $idsupplier ){
			
				if( $isInternalSupplier )
						$orderAmount += round( $this->getItemAt( $i )->get( "discount_price" ), 2 ) * $this->getItemAt( $i)->get( "quantity" );
				else 	$orderAmount += round( $this->getItemAt( $i )->get( "discount_price" ), 2 ) * $this->getItemAt( $i)->get( "external_quantity" );	
			
			}

			$i++;
			
		}
		
		return $orderAmount;
		
	}

	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le montant d'achat HT total des articles qui transitent par le fournisseur interne pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float
	 */
	 
	public function getInternalSupplierOrderAmount( $idsupplier ){
		
		if( empty( $idsupplier ) )
			return 0.0;
			
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		//$isInternalSupplier = $idsupplier == $internal_supplier;
		if($idsupplier ==$internal_supplier)
		$isInternalSupplier =true;
				else
		$isInternalSupplier =false;
		
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
		
		if( $isInternalSupplier )
			return 0.0;
			
		$i = 0;
		$order_amount = 0.0;
		while( $i < $this->getItemCount() ){
			
			$ref_idsupplier = $this->getItemAt( $i)->get("idsupplier" );

			if( $transit )
					$quantity = $this->getItemAt( $i)->get("quantity" );
			else 	$quantity = $this->getItemAt( $i)->get("quantity" ) - $this->getItemAt( $i)->get("external_quantity" );
				
			if( $ref_idsupplier == $idsupplier )
				$order_amount += round( $this->getItemAt( $i )->get( "discount_price" ), 2 ) * $quantity;

			$i++;
			
		}

		return $order_amount;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le poids total des articles pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float
	 */
	 
	public function getSupplierWeight( $idsupplier ){
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		//$isInternalSupplier = $idsupplier == $internal_supplier;
		if($idsupplier ==$internal_supplier)
		$isInternalSupplier =true;
				else
		$isInternalSupplier =false;
		
		
		$weight = 0.0;
		$i = 0;
		while( $i < $this->getItemCount() ){
			
			$ref_idsupplier = $this->getItemAt( $i)->get( "idsupplier" );

			if( $ref_idsupplier == $idsupplier ){
				
				if( $isInternalSupplier )
						$weight += $this->getItemAt( $i)->get( "weight" ) * $this->getItemAt( $i)->get( "quantity" );
				else 	$weight += $this->getItemAt( $i)->get( "weight" ) * $this->getItemAt( $i)->get( "external_quantity" );	
			
			}
			
			$i++;
			
		}
		
		return $weight;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le poids total des articles des articles qui transitent par lutange pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return float
	 */
	 
	public function getInternalSupplierWeight( $idsupplier ){
		
		if( empty( $idsupplier ) )
			return 0.0;
			
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		//$isInternalSupplier = $idsupplier == $internal_supplier;
		if($idsupplier ==$internal_supplier)
		$isInternalSupplier =true;
				else
		$isInternalSupplier =false;
		
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
		
		if( $isInternalSupplier )
			return 0.0;
			
		$i = 0;
		$weight = 0.0;
		while( $i < $this->getItemCount() ){
			
			$ref_idsupplier = $this->getItemAt( $i)->get("idsupplier" );
			if( $transit )
					$quantity = $this->getItemAt( $i)->get("quantity" );
			else 	$quantity = $this->getItemAt( $i)->get("quantity" ) - $this->getItemAt( $i)->get("external_quantity" );
			
			if( $ref_idsupplier == $idsupplier )
				$weight += $this->getItemAt( $i)->get("weight" ) * $quantity;

			$i++;
			
		}

		return $weight;
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le pourcentage à appliquer sur le montant des achats pour un fournisseur donné
	 * @param int $idsupplier l'identifiant du fournisseur
	 * @return float
	 */
	public function getSupplierChargeRate( $idsupplier ){
			// ------- Mise à jour de l'id gestion des Index  #1161	
		$query = "SELECT charge_rate FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer pourcentage à appliquer pour les frais de port fournisseur" );

		if( !$rs->RecordCount() )
			return 0.0;
			
		return $rs->fields( "charge_rate" );
		
	}
	
	//------------------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur du champs de comptabilité charge_carrier ( frais de port pour tous les fournisseurs sauf interne )
	 * @param int idsupplier le fournisseur de la commande
	 * @return float
	 */
	 
	protected function getCarrierCharges(){
		
		$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
		
		//récupérer les fournisseurs
		
		$idsuppliers = array();
		$i = 0;
		while( $i < $this->getItemCount() ){
			
			$idsupplier = $this->getItemAt( $i)->get( "idsupplier" );
			
			if( !empty( $idsupplier ) 
				&& !in_array( $idsupplier, $idsuppliers ) 
				&& $idsupplier != $internal_supplier )
				$idsuppliers[] = $idsupplier;
				
			$i++;
			
		}
		
		$i = 0;
		$supplier_charges = 0.0;
		while( $i < count( $idsuppliers ) ){
			
			$idsupplier = $idsuppliers[ $i ];

			$supplier_charges += $this->getSupplierCharges( $idsupplier );
			
			$i++;
			
		}
		
		return $supplier_charges;
		
	}
	
	//-----------------------------------------------------------------------------------------------
	/**
	 * Sauvegarde les frais de port fournisseur dans la base de données
	 * @return void
	 */
	protected function UpdateSupplierCharges(){
		
		//frais de port fournisseurs auto ANCIENNE VERSION
		
		/*if( $this->recordSet[ "charge_supplier_auto" ] == 1 ){
	
			$ttc = $this->getTotalATI();
			$supplier_charges = $this->getSupplierTotalCharges();
			$supplier_charge_rate = $supplier_charges * 100.0 / $ttc;
	
			$this->recordSet["charge_supplier"] = $supplier_charges;
			$this->recordSet["charge_carrier"] = 	$this->getCarrierCharges();
			$this->recordSet["charge_supplier_rate"] = $supplier_charge_rate;

		}*/
			
		//récupérer les différents fournisseurs
		
		$idsuppliers = $this->getDistinctIdSuppliers();
		
		//suppression des frais de port pour les fournisseurs non utilisés ( références supprimées )
		
		$this->deleteUselessSupplierCharges();
			
		//enregistrer les frais de port
		
		$total_charges = 0.0;
		$total_internal_supplier_charges = 0.0;
		
		foreach( $idsuppliers as $idsupplier ){
			
			//frais de port déjà enregistrés ?
			
			$exists = $this->isSupplierChargeRecorded( $idsupplier );
			
			//calcul auto ou manuel?
			
			$charges_auto = $this->hasAutomaticSupplierCharge( $idsupplier) ? 1 : 0;
			$internal_supplier_charges_auto = $this->hasAutomaticInternalSupplierCharge( $idsupplier) ? 1 : 0;

			//enregistrement
			
			if( $charges_auto || !isset( $_POST[ "supplier_charges_$idsupplier" ] ) )
				$charges = $this->getSupplierCharges( $idsupplier );
			else $charges =  strtolower( $_POST[ "supplier_charges_$idsupplier" ] ) == "franco" ? 0.0 : Util::text2num( $_POST[ "supplier_charges_$idsupplier" ] );
			
			if( $internal_supplier_charges_auto || !isset( $_POST[ "internal_supplier_charges_$idsupplier" ] ) )
				$internal_supplier_charges = $this->getInternalSupplierCharges( $idsupplier );
			else $internal_supplier_charges = strtolower( $_POST[ "internal_supplier_charges_$idsupplier" ] ) == "franco" ? 0.0 :  Util::text2num( $_POST[ "internal_supplier_charges_$idsupplier" ] );

			if( $exists ){
			// ------- Mise à jour de l'id gestion des Index  #1161	
				$idcarrier = isset( $_POST[ "idcarrier_$idsupplier" ] ) ? intval( $_POST[ "idcarrier_$idsupplier" ] ) : 0;
				$query = "
				UPDATE " . $this->TABLE_NAME . "_charges
				SET charges = '$charges',
				internal_supplier_charges = $internal_supplier_charges,
				idcarrier = '$idcarrier',
				charges_auto = '$charges_auto',
				internal_supplier_charges_auto = '$internal_supplier_charges_auto'
				WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
				AND idsupplier = '$idsupplier'
				LIMIT 1";
				
			}
			else $query = "
				INSERT INTO " . $this->TABLE_NAME . "_charges(
					`" . $this->PRIMARY_KEY_NAME . "`,
					idsupplier,
					charges,
					internal_supplier_charges,
					charges_auto,
					internal_supplier_charges_auto )
				VALUES (
					'" . $this->getId() . "',
					'$idsupplier',
					'$charges',
					'$internal_supplier_charges',
					'$charges_auto',
					'$internal_supplier_charges_auto' )";
					
			$rs = DBUtil::query( $query );
			
			if( $rs === false )
				die( "Impossible de mettre à jour les frais de port pour le fournisseur $idsupplier" );
		
			//report cde client, cde fournisseur
			
			if( $this instanceof Estimate ){
				
				if( $idorder = DBUtil::getDBValue( "idorder", "order", "idestimate", $this->getId() ) ){
					
					DBUtil::query( "UPDATE order_charges SET idcarrier = '" . intval( $_POST[ "idcarrier_$idsupplier" ] ) . "' WHERE idorder = '$idorder' AND idsupplier = '$idsupplier' LIMIT 1" );
					
					$rs = DBUtil::query( "SELECT idbl_delivery FROM bl_delivery WHERE idorder = '$idorder' AND idsupplier = '$idsupplier' LIMIT 1" );
					
					if( $rs->RecordCount() )
						DBUtil::query( "UPDATE carriage SET idcarrier = '" . intval( $_POST[ "idcarrier_$idsupplier" ] ) . "' WHERE idcarriage = '" . $rs->fields( "idbl_delivery" ) . "' LIMIT 1" );
					
				}
				
			}
			
			if( $this instanceof Order && isset( $_POST[ "idcarrier_$idsupplier" ] ) ){
	
				DBUtil::query( "UPDATE order_charges SET idcarrier = '" . intval( $_POST[ "idcarrier_$idsupplier" ] ) . "' WHERE idorder = '" . $this->getId() . "' AND idsupplier = '$idsupplier' LIMIT 1" );
				
				$rs = DBUtil::query( "SELECT idbl_delivery FROM bl_delivery WHERE idorder = '" . $this->getId() . "' AND idsupplier = '$idsupplier' LIMIT 1" );
					
				if( $rs->RecordCount() )
					DBUtil::query( "UPDATE carriage SET idcarrier = '" . intval( $_POST[ "idcarrier_$idsupplier" ] ) . "' WHERE idcarriage = '" . $rs->fields( "idbl_delivery" ) . "' LIMIT 1" );
						
			}
			
			//calcul des totaux
			
			$total_charges += $charges;
			$total_internal_supplier_charges += $internal_supplier_charges;
			
		} //foreach( $idsuppliers as $idsupplier )
		
		$ht = $this->getTotalET();
		$supplier_charge_rate = $ht > 0.0 ? ( $total_charges + $total_internal_supplier_charges ) * 100.0 / $ht : 0.0;

		$this->recordSet["charge_supplier"] = $total_charges + $total_internal_supplier_charges;
		$this->recordSet["charge_supplier_rate"] = $supplier_charge_rate;
		$this->recordSet["charge_carrier"] = $total_charges; //tous les fournisseurs sauf interne
			
	}

	//-----------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si les frais de port doivent être calculés automatiquement pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return bool true si les frais de port fournisseur sont calculés automatiquement, sinon false
	 */
	 
	public function hasAutomaticSupplierCharge( $idsupplier ){
			
		$exists = $this->isSupplierChargeRecorded( $idsupplier );
		
		if( !$exists ) //nouveau fournisseur utilisé
			return true;
			
		if( isset( $_POST[ "SupplierChargesAuto_$idsupplier" ] ) 
			&& $_POST[ "SupplierChargesAuto_$idsupplier" ] == 0 ){
			
			//mise à jour des frais de port manuellement
			
			return 0;
			
		}
		else if( ( isset( $_POST[ "SupplierChargesAuto_$idsupplier" ] ) 
			&& $_POST[ "SupplierChargesAuto_$idsupplier" ] == 1 )
			|| !$exists ){
			
			//mise à jour des frais de port automatiquement demandée ou premier enregistrement
			
			return 1;
			
		}
		else{ //chargement devis/commande
			// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "SELECT charges_auto FROM " . $this->TABLE_NAME . "_charges
			WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
			AND idsupplier = '$idsupplier'
			LIMIT 1";
			
			$rs = DBUtil::query( $query );
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de vérifier si les frais de port doivent calculés automatiquement pour le fournisseur $idsupplier" );
			
			return $rs->fields( "charges_auto" ) == 1 ? true : false;
				
		}
		
	}
	
	//-----------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si les frais de port du fournisseur interne doivent être calculés automatiquement pour un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return bool true si les frais de port fournisseur sont calculés automatiquement, sinon false
	 */
	 
	public function hasAutomaticInternalSupplierCharge( $idsupplier ){
			
		$exists = $this->isSupplierChargeRecorded( $idsupplier );
		
		if( isset( $_POST[ "InternalSupplierChargesAuto_$idsupplier" ] ) 
			&& $_POST[ "InternalSupplierChargesAuto_$idsupplier" ] == 0 ){
			
			//mise à jour des frais de port manuellement
			
			return 0;
			
		}
		else if( ( isset( $_POST[ "InternalSupplierChargesAuto_$idsupplier" ] ) 
			&& $_POST[ "InternalSupplierChargesAuto_$idsupplier" ] == 1 )
			|| !$exists ){
			
			//mise à jour des frais de port automatiquement demandée ou premier enregistrement
			
			return 1;
			
		}
		else if( $exists ){ //chargement devis/commande
			// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "SELECT internal_supplier_charges_auto FROM " . $this->TABLE_NAME . "_charges
			WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
			AND idsupplier = '$idsupplier'
			LIMIT 1";
			
			$rs = DBUtil::query( $query );
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de vérifier si les frais de port doivent calculés automatiquement pour le fournisseur interne" );
			
			return $rs->fields( "internal_supplier_charges_auto" ) == 1 ? true : false;
				
		}
		else return true; //nouveau fournisseur
		
	}
	
	//-----------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si un enregistrement existe pour les frais de port d'un fournisseur donné
	 * @param int idsupplier le fournisseur
	 * @return bool true si un enregistrement existe pour le fournisseur sinon false
	 */
	 
	private function isSupplierChargeRecorded( $idsupplier ){
				
		//vérifier si un enregistrement existe
		// ------- Mise à jour de l'id gestion des Index  #1161	
		$query = "
		SELECT COUNT(*) AS `exists` 
		FROM " . $this->TABLE_NAME . "_charges
		WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
		AND idsupplier = '$idsupplier'";
		
		$rs = DBUtil::query( $query );
		if( $rs === false || !$rs->RecordCount() || $rs->fields( "exists" ) == null )
			die( "Impossible de vérifier si les frais de port pour le fournisseur $idsupplier ont déjà été enregistré ou non" );
			
		return $rs->fields( "exists" ) == 0 ? false : true;
			
	}
	
	//-----------------------------------------------------------------------------------------------
	
	/**
	 * Suppression des frais de port pour les fournisseurs qui ne sont plus utilisés
	 * @return void
	 */	 
	private function deleteUselessSupplierCharges(){
			
		$idsuppliers = $this->getDistinctIdSuppliers();
		
		if( !count( $idsuppliers ) )
			return;
			
		//suppression des fournisseurs non utilisés ( références supprimées )
		//ne pas supprimer tous les fournisseurs car impossible de retrouver les frais de port saisis manuellement
		
		$query = "
		DELETE FROM " . $this->TABLE_NAME . "_charges 
		WHERE `" . $this->PRIMARY_KEY_NAME . "` = '" . $this->getId() . "'
		AND idsupplier NOT IN( " . implode( ",", $idsuppliers ) . " )";

		$rs = DBUtil::query( $query );
		if( $rs === false )
			die( "Impossible de supprimer les frais de ports inutiles" );
			
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Test la validité des données nécéssaires au calcul des frais de port
	 * @return string un éventuel message d'avertissement si des données manquent, sinon vide
	 */
	public function getSupplierError( $idsupplier ){
				
		$help = "Merci de bien vouloir saisir un montant manuellement";
		
		if( empty( $idsupplier ) )
			return "Le fournisseur n'a pas été défini pour ces références<br />$help";
			
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $idsupplier );
		if( empty( $idstate ) )
			return "Le pays n'a pas été défini pour ce fournisseur<br />$help";
			
		$charge_select = $this->getSupplierChargeSelect( $idsupplier );
		if( empty( $charge_select ) )
			return "Aucune méthode n'a été définie pour le calcul des frais de port achat de ce fournisseur. $help<br /><br />";
			
		$internalSupplier = DBUtil::getParameterAdmin( "internal_supplier" );
		$isInternalSupplier = $idsupplier == $internalSupplier;
		
		$total_weight = $this->getSupplierWeight( $idsupplier );
		if( empty( $total_weight ) )
			return "Le poids total des articles n'a pas pu être calculé pour ce fournisseur";
			
		$buying_amount = $this->getSupplierBuyingAmount( $idsupplier );
		if( empty( $buying_amount ) )
			return "Le montant d'achat n'a pas pu être calculé pour ce fournisseur";
		
		if( !$isInternalSupplier ){
					
			$franco_supplier = $this->getSupplierFranco( $idsupplier );
			
			if( $franco_supplier != 0 && $franco_supplier <= $buying_amount )
				return "";
		
		}
							
		switch( $charge_select ){
			
			//pourcentage du montant d'achat
			
			case 1 :
// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT charge_rate FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return "Le pourcentage à appliquer au montant d'achat n'a pas été défini pour déterminer les frais de port de ce fournisseur<br />$help";

				break;
				
			//calcul par tranche de poids sans zone départementale
			
			case 2 :
// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT cs.value AS charges
				FROM charge_supplier cs, charge c
				WHERE cs.idsupplier = '$idsupplier'
				AND cs.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight > '$total_weight'";
					
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return "Les frais de port de ce fournisseur sont calculés par tranche de poids mais aucun montant n'a été défini pour cette tranche de poids<br />$help";
			
				break;
				
			//frais de port proportionnels au poids + charges fixes
			
			/*case 3 :
			
				$total_weight = $this->getSupplierWeight( $idsupplier );
				if( empty( $total_weight ) )
					return "Le poids total des articles n'a pas pu être calculé pour ce fournisseur";
					
				$query = "SELECT charge_fixed, charge_amount FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
				$rs = DBUtil::query( $query );
				
				if( $rs === false || !$rs->RecordCount() )
					return "Les frais de port sont calculés par rapport au poids avec un montant fixe pour ce fournisseur mais aucune value n'a été définie pour ce fournisseur<br />$help";

				break;*/
				
			//par zone départementale et tranche de poids
			
			case 4 :
			
				$total_weight = $this->getSupplierWeight( $idsupplier );
				if( empty( $total_weight ) )
					return "Le poids total des articles n'a pas pu être calculé pour ce fournisseur";
				
				$idstate = $this->forwardingAddress->getIdState();
				if( $idstate != 1 )
					return "Impossible de calculer les frais de ports de ce fournisseur car l'adresse de livraison n'est pas en France";
		
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return "Les frais de ports de ce fournisseur sont fonctions de la zone départementale mais le format du code postal client ( '$zipcode' ) est incorrect";
				
				$area = substr( $zipcode, 0, 2 );
							
				$query = "
				SELECT csr.value
				FROM charge_supplier_region csr, charge c
				WHERE csr.idsupplier = '$idsupplier'
				AND csr.idregion = $area
				AND csr.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight >= '$total_weight'";

				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return "Impossible de calculer les frais de port fournisseur par zone départementale et par tranche de poids pour les paramètres suivant:
					<br />Identifiant fournisseur : $idsupplier
					<br />Département : $area
					<br />Poids : $total_weight
					<br />$help";
				
				break;
			
			//par région et tranche de poids
				
			case 5 :
			
				$total_weight = $this->getSupplierWeight( $idsupplier );
				if( empty( $total_weight ) )
					return "Le poids total des articles n'a pas pu être calculé pour ce fournisseur";
				
				$idstate = $this->forwardingAddress->getIdState();
				if( $idstate != 1 )
					return "Impossible de calculer les frais de ports de ce fournisseur car l'adresse de livraison n'est pas en France";
						
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return "Les frais de ports de ce fournisseur sont fonctions de la zone départementale mais le format du code postal client ( '$zipcode' ) est incorrect";
				
				$area = substr( $zipcode, 0, 2 );
// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT csa.value, csa.fixed_rate, csa.value_weight
				FROM area a, area_region ar, charge_supplier_area csa, charge c
				WHERE ar.idregion = $area
				AND ar.idarea = a.idarea
				AND a.idsupplier = '$idsupplier'
				AND a.idarea = csa.idarea
				AND csa.idcharge = c.idcharge
				AND c.min_weight <= '$total_weight' 
				AND c.max_weight >= '$total_weight'";

				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return "Impossible de calculer les frais de port fournisseur par zone départementale et par tranche de poids pour les paramètres suivant:
					<br />Identifiant fournisseur : $idsupplier
					<br />Département : $area
					<br />Poids : $total_weight
					<br />$help";
				
				break;

			//calcul par tranche de prix sans zone départementale
			
			case 6 :
// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "
				SELECT cs.value AS charges
				FROM charge_supplier cs, charge c
				WHERE cs.idsupplier = '$idsupplier'
				AND cs.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";
					
				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return "Les frais de port de ce fournisseur sont calculés par tranche de prix mais aucun montant n'a été défini pour cette tranche de prix<br />$help";
			
				break;
				
			//par zone départementale et tranche de prix
			
			case 7 :
			
				$idstate = $this->forwardingAddress->getIdState();
				if( $idstate != 1 )
					return "Impossible de calculer les frais de ports de ce fournisseur car l'adresse de livraison n'est pas en France";
		
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return "Les frais de ports de ce fournisseur sont fonctions de la zone départementale mais le format du code postal client ( '$zipcode' ) est incorrect";
				
				$area = substr( $zipcode, 0, 2 );
					// ------- Mise à jour de l'id gestion des Index  #1161		
				$query = "
				SELECT csr.value
				FROM charge_supplier_region csr, charge c
				WHERE csr.idsupplier = '$idsupplier'
				AND csr.idregion = $area
				AND csr.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";

				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return "Impossible de calculer les frais de port fournisseur par zone départementale et par tranche de prix pour les paramètres suivant:
					<br />Identifiant fournisseur : $idsupplier
					<br />Département : $area
					<br />Montant achats : $buying_amount
					<br />$help";
				
				break;
			
			//par région et tranche de poids
				
			case 8 :
		
				$idstate = $this->forwardingAddress->getIdState();
				
				if( $idstate != 1 )
					return "Impossible de calculer les frais de ports de ce fournisseur car l'adresse de livraison n'est pas en France";
						
				$zipcode = $this->forwardingAddress->getZipcode();
				
				if( !is_numeric( $zipcode ) || strlen( strval( $zipcode ) ) < 2 )
					return "Les frais de ports de ce fournisseur sont fonctions de la zone départementale mais le format du code postal client ( '$zipcode' ) est incorrect";
				
				$area = substr( $zipcode, 0, 2 );
// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "SELECT csa.value, csa.fixed_rate, csa.value_price
				FROM area a, area_region ar, charge_supplier_area csa, charge c
				WHERE ar.idregion = $area
				AND ar.idarea = a.idarea
				AND a.idsupplier = '$idsupplier'
				AND a.idarea = csa.idarea
				AND csa.idcharge = c.idcharge
				AND c.min_price <= '$buying_amount' 
				AND c.max_price >= '$buying_amount'";

				$rs = DBUtil::query( $query );
				if( $rs === false || !$rs->RecordCount() )
					return "Impossible de calculer les frais de port fournisseur par zone départementale et par tranche de prix pour les paramètres suivant:
					<br />Identifiant fournisseur : $idsupplier
					<br />Département : $area
					<br />Montant achats : $buying_amount
					<br />$help";
				
				break;
			
			case 9 : 
			case 10 :
			case 11 :
			case 12 :
				return ""; 
			
			default : return "La méthode de calcul définie pour le calcul des frais de port de ce fournisseur est inconnue<br />$help";
			
		}
		
		return "";

	}

	//----------------------------------------------------------------------------
 
	/**
	 * Retourne le nombre de fournisseurs
	 * Si un des fournisseur transite par le fournisseur interne, ce dernier est également pris en compte
	 * @return int
	 */
	 
	public function getSupplierCount(){
		
		$idsuppliers 			= array();
		$useInternalSupplier 	= false;		
		$internal_supplier 		= DBUtil::getParameterAdmin( "internal_supplier" );
		
		$supplierCount = 0;
		$i = 0;
		while( $i < $this->getItemCount() ){
			
			$idsupplier = $this->getItemAt( $i)->get("idsupplier" );
			
			if( !empty( $idsupplier ) ){
				
				$transit 				= DBUtil::getDBValue( "transit", "supplier", "idsupplier", $idsupplier );
				$hasInternalQuantity 	= $this->getItemAt( $i)->get( "quantity" ) > $this->getItemAt( $i)->get( "external_quantity" );
				
				if( !in_array( $idsupplier, $idsuppliers ) ){
	
					$idsuppliers[] = $idsupplier;
					$supplierCount++;
					
				}
				else if( ( $transit || $hasInternalQuantity ) && !in_array( $internal_supplier, $idsuppliers ) ){ //fournisseur interne
					
					$idsuppliers[] = $internal_supplier;
					$supplierCount++;
					
				}
			
			}
			
			$i++;
			
		}
		
		return $supplierCount;
		
	}
	
	//----------------------------------------------------------------------------
 
	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>