<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion de formulaires pour diter une table (stdform)
 */
 
include_once("adm_form.php");
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

class ADMEDIT extends ADMFORM
{
	var $current_page;
	var $enctype_param = ''; // may be : 'enctype="multipart/form-data"';
	var $location =''; // rpertoire pour les uploads
	
	/**
	 * Contructeur
	 * @param ADMTABLE $tableobj L'object de la table
	 */
	function __construct(ADMTABLE &$tableobj) {
		global $ScriptName; // Pour pouvoir changer la valeur en amont (popup)
		$this->table = &$tableobj;
		$this->current_page = $ScriptName;
		}
	
	/**
	 * Initialise l'enctype d'un formulaire
	 *  utiliser pour faire des tlchargements
	 * @param string location rpertoire de destination
	 * @param $e la valeur de l'attribut 'enctype' pour le formulaire, 'multipart/form-data' par dfaut
	 * @return void
	 */
	public function set_enctype($location='',$e='multipart/form-data') {
		if($e) $this->enctype_param = 'enctype="'.$e.'"';
		else $this->enctype_param = '';
		$this->location = $location;
	}
	/**
	 * Affichage du formulaire d'dition
	 * @param array $k Tableau avec les paramtres pour la recherhce de l'enregsitrement' ( optionnel )
	 * @param string $Str_FormName Le nom du formulaire ( optionnel )
	 * @return void
	 */
	public function display_edit_form($k = false , $Str_FormName = 'frm', $search = false)
	{
		global $GLOBAL_START_URL;
		
		$Str_ActionName = $this->current_page;
		$cell_near_cell_number=2;
		// in $Array_FieldsDisplayed the fields that can be displayed for editing
		$Array_FieldsDisplayed = array();
		$Str_FieldsDisplayed = '';
		$Str_KeyID="WHERE ";
		$Array_LocalVars = array();
		
		$this->BeginPostForm($Str_ActionName,$Str_FormName,'',$this->enctype_param);
		
		if($k) //si modification d'un enreg.
		{
			$Array_RecordID= unserialize(base64_decode($k[0]));
			foreach($k as $v)
			{
				$x= unserialize(base64_decode($v)) ;// print_r( $x);
				$kk = base64_encode(serialize($x));
				$this->InputButton('hidden', "k[]" , $kk ); // identifiant d'enregistrement  modifier
			}
		
			//get the data 
			while(list ($key, $val) = each ($Array_RecordID))
			{
				$Array_LocalVars[] = $key;
			}
			//print_r($Array_LocalVars);
			for(; list($i, $ii) = each($Array_LocalVars);) //browsing key names from passed array
			{
				$Str_KeyID.=$ii ." = '". $Array_RecordID[$ii] ."' AND "; //build up select condition
				echo "\n <input type=hidden name=id_index".$i."_name value=\"$ii\">"; // for continuacy
				echo "\n <input type=hidden name=id_index".$i."_value value=\"". $Array_RecordID[$ii] ."\">";
				$v = $i;
			}
		
			echo "\n <input type=hidden name=id_index_number value=".++$v.">";
		
			$Str_KeyID = substr($Str_KeyID, 0, strlen($Str_KeyID)-4); //drop last AND
			//get the record
			$Array_FieldsDisplayed = $this->table->LoadRecord($Str_KeyID); //print_r($Array_FieldsDisplayed);
			$title = "title_modify";
		}
		else //nouvel enreg.
		{
			if(isset($_GET['idcategory'])) $idc = $_GET['idcategory'];	else $idc = 0 ; 
			$Array_FieldsDisplayed = $this->table->MakeEmptyRecord($idc);  //print_r($Array_FieldsDisplayed);
			$title = "title_register";
		}
	
		$n = count($Array_FieldsDisplayed);
		$Array_Uploads = array(); // champs tlchargement
		if($n) // one row to display
		{
			
            //$Array_FieldsDisplayed = Util::arrayMapRecursive("Util::checkEncoding", $Array_FieldsDisplayed);
			if($search) {
				showDebug($search,'stdformbase.php => $search','code');
				echo "		".'<p><a href="javascript:search()" class="Liens">'.Dictionnary::translate("search_help").'</a></p>'."\n";
			}

			//champs cachés
			
			for ( $ii = 0; $ii < $n; $ii++ ){
				
				if( $Array_FieldsDisplayed[$ii]['help'] == 'readonly')			
					echo "<input type=\"hidden\" name=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" value=\"".htmlspecialchars($Array_FieldsDisplayed[$ii]['value'])."\" size=\" ".$Array_FieldsDisplayed[$ii]['size']."\" />";
					
			}
	
			//champs editables
	
			?>
			<style type="text/css">
			<!--
			div.subContent table.dataTable span.mceEditor table.mceLayout td{
				
				border-width:1px;
				padding:0px;
				
			}


			div.subContent table.dataTable span.mceEditor table.mceLayout tr.mceFirst .mceToolbar td{
				
				border-style:none;
				
			}
			-->
			</style>
			<table style="width:100%;">
			<tr>
				<td>
					<div class="subTitleContainer" style="margin-bottom:5px;">
				    	<p class="subTitle"><?php echo Dictionnary::translate($title) ?></p>
				   	</div>
				</td>
			<tr>
				<td>
				<table class="dataTable" style="margin-bottom:3px;">
			<?php
			
			for ($ii=0,$jj=0;$ii<$n;$ii++){

				$mandatory = 0;
				if($this->table->isRequired($Array_FieldsDisplayed[$ii]['fieldname'])) $mandatory = 1;

				if( $Array_FieldsDisplayed[$ii]['help'] != 'readonly' ) {
				
					if( $Array_FieldsDisplayed[$ii]['help'] == 'textarea' || $Array_FieldsDisplayed[$ii]['help'] == 'wysiwyg' )
						continue;
					
					if (($jj % $cell_near_cell_number)==0)  
						echo "\n  <tr>"; //Pass to new line in <table>
				
					$jj++;
			
					$class = $mandatory ? " class=\"filledCell\"" : "";
					echo "
					<th class=\"filledCell\" style=\"padding-left:4px";
					echo "\" style=\"text-align:right;\" $class>";
					echo Dictionnary::translate($Array_FieldsDisplayed[$ii]['export_name']);
					if($mandatory) echo "<span class=\"asterix orangeText\">*</span>";
					echo "</th><td style=\"text-align:left\">";


					//echo ($mandatory) ? "mandatory" : "";
					
					if( $Array_FieldsDisplayed[$ii]['help'] == 'password') {

							echo "
						<input type=\"password\" name=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" value=\"\" size=".$Array_FieldsDisplayed[$ii]['size']." />";
						
					}
					elseif( $Array_FieldsDisplayed[$ii]['help'] == 'users') {
					
						?>
						<script type="text/javascript">
						/* <![CDATA[ */
				
						function usersDialog<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>(){

							$("#UsersDialog_<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>").dialog({
		        
								modal: false,	
								title: "Liste des contacts internes",
								width: 500,
								close: function(event, ui) { 
								
									$("#UsersDialog_<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>").dialog( "destroy" );
									
								}
							
							});
				
							var users = $( "#<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>" ).val().split( "," );
							
							var i = 0;
							while( i < users.length ){
							
								$( "#UsersDialog_<?php echo $Array_FieldsDisplayed[$ii]['fieldname']; ?>" ).find( "input[value=" + users[ i ] + "]" ).attr( "checked", true );
								
								i++;
								
							}
							
						}
				
						function updateUserList( elementId ){
						
							var users = "";
							$( "#UsersDialog_<?php echo $Array_FieldsDisplayed[$ii]['fieldname']; ?>" ).find( "input[checked=true]" ).each( function( i ){
							
								if( users.length )
									users += ",";
									
								users += $( this ).val();
								
							});
							
							$( "#<?php echo $Array_FieldsDisplayed[$ii]['fieldname']; ?>" ).val( users );
							
						}
					
						/* ]]> */
						</script>
						<div id="UsersDialog_<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>" style="display:none;">
							<ul style="list-style-type:none; display:inline; float:left;">
							<?php
							
								$rs =& DBUtil::query( "SELECT login FROM `user` ORDER BY login ASC" );
								
								$i = 0;
								while( !$rs->EOF() ){
									
									if( $i && !( $i % 10 ) ){
										
										?>
										</ul>
										<ul style="list-style-type:none; display:inline; float:left;">
										<?php
										
									}
									
									?>
									<li>
										<input type="checkbox" value="<?php echo htmlentities( $rs->fields( "login" ) ); ?>" onclick="updateUserList('<?php echo $Array_FieldsDisplayed[$ii]['fieldname'];?>');" /> 
										<a href="#" onclick="$( this ).prev().attr( 'checked', !$( this ).prev().attr( 'checked' ) ); updateUserList('<?php echo $Array_FieldsDisplayed[$ii]['fieldname'];?>'); return false;">
											<?php echo htmlentities( $rs->fields( "login" ) ); ?>
										</a>
									</li>
									<?php
									
									$rs->MoveNext();
									$i++;
									
								}
								
							?>
							</ul>
							<p style="text-align:right; clear:both;">
						<input class="blueButton" type="button" value="Fermer" onclick="$('#UsersDialog_<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>').dialog( 'destroy' );" />
							</p>
						</div>
						<div>
							<input class="textInput" id="<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>" type="text" name="<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>" value="<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['value'] ); ?>" style="width:200px;" readonly="readonly" />
							<a href="#" title="Modifier la liste des contacts" onclick="usersDialog<?php echo htmlentities( $Array_FieldsDisplayed[$ii]['fieldname'] ); ?>(); return false;">
								<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/flexigrid_button_headings.png" alt"" />
							</a>
						</div>
						<?php
			
					}
					else if ( $Array_FieldsDisplayed[$ii]["modify_select_table"]=='' ) { // do not select from another table
				
			   			if( $Array_FieldsDisplayed[$ii]['help'] == 'date') {
			   		
					   		$date = $Array_FieldsDisplayed[$ii]['value'];
					   		
					   		if( preg_match( "/^[0-9]{4}-[0-9]{2}-[0-9]{2}\$/", $date ) ){
					   		
					   			list( $year, $month, $day ) = explode( "-", $date );
					   			//$date = "$day-$month-$year";
					   			$date = "$year-$month-$day";
							
					   		}
					   		else if( preg_match( "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}\$/", $date ) ){
					   		
					   			list( $date, $time  ) = explode( " ", $date );
					   			list( $year, $month, $day ) = explode( "-", $date );
					   			//$date = "$day-$month-$year";
					   			$date = "$year-$month-$day";
								
					   		}
					   		else if( preg_match( "/^[0-9]{2}-[0-9]{2}-[0-9]{4}\$/", $date ) ){
					   			
					   			$date = $date;
					   			
					   		}
					   		else $date ="";
					   		
							echo "
							<input type=\"text\" class=\"calendarInput\" name=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" id=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" value=\"$date\" />";
							DHTMLCalendar::calendar( $Array_FieldsDisplayed[$ii][ 'fieldname' ] );
						
						} elseif( $Array_FieldsDisplayed[$ii]['help'] == 'cpopup') {
					
							$can = DBUtil::getDBValue('name_1','category',"idcategory", $Array_FieldsDisplayed[$ii]['value']);
							echo "
							<input type=\"text\" class=\"textInput\" name=\"" .$Array_FieldsDisplayed[$ii]['fieldname']."name\" value=\"". $can ."\" readonly >
							<input type=\"hidden\" name=\"" .$Array_FieldsDisplayed[$ii]['fieldname']."\" value=\"". $Array_FieldsDisplayed[$ii]['value'] ."\" >
							<img type=\"image\" src=\"$GLOBAL_START_URL/images/back_office/content/popup.jpg\" name=\"category_sel\" value=\" ? \" onclick=\"javascript:window.open('$GLOBAL_START_URL/formbase/category_select.php?target=".$Array_FieldsDisplayed[$ii]['fieldname']."&targetform=$Str_FormName&actualvalue=". $Array_FieldsDisplayed[$ii]['value'] ."','', 'height=120,width=110,resize,scrollbars=1');\" alt=\"Popup\"> \n";
				
						} elseif( strlen( $Array_FieldsDisplayed[$ii]['help'] ) > 4 && substr( $Array_FieldsDisplayed[$ii][ 'help' ], 0, 4 ) == 'enum') {
					
							$pattern = "/[']{1}([^']+)[']{1}/i";
							$matches = array();
							preg_match_all( $pattern, $Array_FieldsDisplayed[$ii][ 'help' ], $matches );
								
							if( !count( $matches ) || !count( $matches[ 1 ] ) )
								trigger_error( "Erreur de syntaxe pour la description du champ '" . $Array_FieldsDisplayed[$ii][ 'fieldname' ] . "'", E_USER_ERROR );
						
							echo "<select name=\"" . $Array_FieldsDisplayed[$ii][ 'fieldname' ] . "\">";
							
							foreach( $matches[ 1 ] as $match ){
								
								$selected = $Array_FieldsDisplayed[$ii]['value'] == $match ? " selected=\"selected\"" : "";
								echo "<option value=\"" . htmlentities( $match ) . "\"$selected>" . htmlentities( Dictionnary::translate( $match ) ) . "</option>";
								
							}
						
							echo "</select>";
						
						}
					 	elseif($Array_FieldsDisplayed[$ii]['help'] == 'upload') {
						
							$value = $Array_FieldsDisplayed[ $ii ][ "value" ];
							$fieldname = $Array_FieldsDisplayed[ $ii ][ "fieldname" ];
							
							$this->displayUpload( $fieldname, $value );
						
					
						}
						else if( $Array_FieldsDisplayed[$ii]['help'] != 'textarea' )
							echo "<input type=\"text\" class=\"textInput\" name=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" id=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" value=\"". htmlspecialchars($Array_FieldsDisplayed[$ii]['value']) ."\" size=".$Array_FieldsDisplayed[$ii]['size']."> ";
                            //echo "<input type=\"text\" class=\"textInput\" name=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" id=\"".$Array_FieldsDisplayed[$ii]['fieldname']."\" value=\"". Util::doNothing($Array_FieldsDisplayed[$ii]['value']) ."\" size=".$Array_FieldsDisplayed[$ii]['size']."> ";

					} else if( $Array_FieldsDisplayed[$ii]['help'] != 'textarea' ){
				
						$xx= $Array_FieldsDisplayed[$ii]['fieldname'] ;
						
						if($xx=='idcategory' && isset($_POST[$xx]) ) 
							$Array_FieldsDisplayed[$ii]['value']= $_POST[$xx] ;	
						
						$this->SelectField($Array_FieldsDisplayed[$ii],'modify');
					
					} 
				
					echo "
					</td>";
					
					if  ( ( $jj) % $cell_near_cell_number ==0 ) 
						echo "</tr>";
			
				}//END if $Array_FieldsDisplayed[$ii]['help'] != 'readonly'  
			
				if( strlen($Array_FieldsDisplayed[$ii]['help']) > 10 ) {
						
						?>
						<?php echo toolTipBox( $Array_FieldsDisplayed[$ii]['help'], 200 ) ?>
						<?php
						
				}
			
			}//end for ligne 150
			
			$ii = ($jj % $cell_near_cell_number);
			if  ( $ii  !=0 ) { 
				
				$ii=($cell_near_cell_number-$ii)*2;  
				echo "\n  <td colspan=\"$ii\" >&nbsp;</td></tr>";
				
			}

			//champs textarea
			
			for ( $ii = 0, $jj = 0; $ii < $n; $ii++ ){

				if( $Array_FieldsDisplayed[ $ii ][ "help" ] == "textarea" ){

					$hasTextareas = true;
					
					?>
					<tr>
						<th class="filledCell" style="padding-left:4px;"><?php echo Dictionnary::translate( $Array_FieldsDisplayed[ $ii ][ "fieldname" ] ) ?></th>
						<td colspan="<?php $colspan = $cell_near_cell_number * 2 - 1; echo $colspan; ?>">
							<textarea name="<?php echo $Array_FieldsDisplayed[ $ii ][ "fieldname" ] ?>" style="width:100%; height:140px;"><?php echo htmlspecialchars( $Array_FieldsDisplayed[ $ii ][ "value" ] ) ?></textarea>
						</td>
					<tr>
					<?php
							
				}
				
			}

			//champs wysiwyg
			
			$wysiwygAlreadyInitialized = false;
			for ( $ii = 0, $jj = 0; $ii < $n; $ii++ ){

				if( $Array_FieldsDisplayed[ $ii ][ "help" ] == "wysiwyg" ){

					?>
					<tr>
						<th class="filledCell"  style="padding-left:4px;">
							<?php echo Dictionnary::translate( $Array_FieldsDisplayed[ $ii ][ "fieldname" ] ) ?>
						</th>
						<td colspan="<?php $colspan = $cell_near_cell_number * 2 - 1; echo $colspan; ?>">
						<?php
							
							//intialisation de tinyMCE
					
							if( !$wysiwygAlreadyInitialized ){
								
								global $GLOBAL_START_PATH;
								
								include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );
								
								WYSIWYGEditor::init( $useAdvancedTheme = false );
								
								$wysiwyg = new WYSIWYGEditor( $Array_FieldsDisplayed[ $ii ][ "fieldname" ] );
								$wysiwyg->setHTML( $Array_FieldsDisplayed[ $ii ][ "value" ] );
								$wysiwyg->setDimension( 0, 150 );
								$wysiwyg->display();
						
								$wysiwygAlreadyInitialized = true;
						
							}
					
						?>
						</td>
					</tr>
					<?php
					
				}
				
			}
	
			echo " </table>";
			?></td></tr></table><?php

			//boutons de formulaire
	
			?>
			<p style="text-align:right;">
			
			<?php if($k){ 

					?>
					<input type="submit" class="blueButton" style="margin-right:10px;" name="update" value="<?php echo Dictionnary::translate("btn_update") ?>"
					<?php 
					
			}else{
				?><input type="submit" class="blueButton" name="append" value="Créer" /><?php
			}
			
			?>
			</p>
			<?php
		
			$this->EndForm();
		}// one row to display
			//	return $Int_NumRecords;
	}
	
	//---------------------------------------------------------------------------------------------------
	
	/**
	 * Affiche un champ de slection de fichier
	 * @param string $fieldname le nom du champ dans le formulaire
	 * @param string $value l'URI du fichier par rapport au rpertoire '$GLOBAL_START_PATH/www/'
	 * @return void
	 */
	//public function displayUpload( $fieldname, $value ){
	public function displayUpload( $fieldname, $value ){
		
		global $GLOBAL_START_URL;
		
		if( !empty( $value ) ){
			
			//vrifier s'il s'agit d'une image ou d'un document autre
			
			$image_types = array( "jpg", "jpeg", "gif", "png", "bmp" );
			
			$i = 0;
			$isImage = false;
			while( $i < count( $image_types ) && !$isImage ){
			
				$type = $image_types[ $i ];
				$pattern = "/^.*\.$type\$^/";
				
				if( preg_match( $pattern, $value ) )
					$isImage = true;
				
				$i++;
				
			}
			
			//afficher le lien vers le fichier ou l'image
			
			if( $isImage ){
				
				?>
				<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $value ?>" alt="" />
				<br />
				<?php
				
			}
			else{
				
				$url = "$GLOBAL_START_URL/www/$value";
				
				?>
				<a href="<?php echo $url ?>" onclick="window.open(this.href); return false;"><?php echo htmlentities( $url ) ?></a>
				<?php	
				
			}
			
		}
		
		?>
		<input type="file" name="upload_<?php echo $fieldname ?>" size="15" />
		<input type="hidden" name="upload_dir" value="<?php echo $this->location ?>" size=20 >
		<?php
	
	}
	
	//---------------------------------------------------------------------------------------------------
	
}//EOC
?>
