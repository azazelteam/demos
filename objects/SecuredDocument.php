<?php 

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object droits accès aux documents back-office
 */
 
interface SecuredDocument {

	//----------------------------------------------------------------------------
	/**
	 * Retourne true si l'utilisateur est autorisé à consulter l'objet
	 * @return bool
	 */
    public function allowRead();
    
    //----------------------------------------------------------------------------
    /**
	 * Retourne true si l'utilisateur est autorisé à modifier l'objet
	 * @return bool
	 */
    public function allowEdit();
    
    //----------------------------------------------------------------------------
	/**
	 * Retourne true si le commercial enregistré à le droit de créer un document pour un client donné
	 * Bien entendu, la méthode retourne false si aucun commercial n'est enregistré
	 * @static
	 * @access public
	 * @param int $idbuyer le client
	 * @return bool
	 */
	public static function allowCreationForCustomer( $idbuyer );
	
	//----------------------------------------------------------------------------
	
}

?>