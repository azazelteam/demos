<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object echange de produit dans le cas d'un retour
 */

include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/Credit.php" );
include_once( dirname( __FILE__ ) . "/Order.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
 //------
final class Exchange{
	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var int $idexchange n° d'échange
	 */
	private $idexchange;
	/**
	 * @var $credit Credit avoir
	 */
	private $credit;
	/**
	 * @var $order Order commande
	 */
	private $order;
	
	//----------------------------------------------------------------------------
	//members
	
	public function __construct( $idexchange ){
		
		$this->idexchange =  $idexchange ;
		
		$rs =& DBUtil::query( "SELECT * FROM exchanges WHERE idexchange = '{$this->idexchange}' LIMIT 1" );
		
		if( !$rs->RecordCount() ){
		
			trigger_error( "Impossible de créer l'échange", E_USER_ERROR );
			exit();
				
		}
		
		$this->credit 	= new Credit( $rs->fields( "idcredit" ) );
		$this->order 	= new Order( $rs->fields( "idorder" ) );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne le n° d'échange
	 * @return int
	 */
	public function getIdExchange(){ return $this->idexchange; }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne une référence vers l'avoir constituant l'échange
	 * @return Credit
	 */
	public function &getCredit(){ return $this->credit; }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne une référence vers la commande constituant l'échange
	 * @return Order
	 */
	public function &getOrder(){ return $this->order; }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne le n° de litige dont l'échange est une issue
	 * @return int
	 */
	public function getIdLitigation(){ return $this->credit->get( "idlitigation" ); }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé un nouvel échange à partir d'un avoir donné et d'une commande donnée
	 * @static
	 * @param idcredit int le numéro d'avoir
	 * @param idorder int le numéro de commande
	 * @return Exchange le nouvel échange
	 */
	 public static function create( $idcredit, $idorder ){
	// ------- Mise à jour de l'id gestion des Index  #1161
	 $table = 'exchanges';
		$idexchange = TradeFactory::Indexations($table);
	 
	 	DBUtil::query( "INSERT INTO `exchanges` (idexchange, idcredit, idorder ) VALUES( '$idexchange','$idcredit', '$idorder' )" ) || trigger_error( "Impossible de créer l'échange", E_USER_ERROR );
	 	
	 	return new Exchange( DBUtil::getInsertID() );
	 	
	 }
	 
	//----------------------------------------------------------------------------
	
	public static function delete( $idexchange ){
		
		$rs =& DBUtil::query( "SELECT idcredit, idorder FROM exchanges WHERE idexchange = '" .  $idexchange . "' LIMIT 1" );
		
		if( !$rs->RecordCount() )
			return;
			
		Credit::delete( $rs->fields( "idcredit" ) );
		Order::delete( $rs->fields( "idorder" ) );
		
		DBUtil::query( "DELETE FROM exchanges WHERE idexchange = '" .  $idexchange . "' LIMIT 1" );
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>