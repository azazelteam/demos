<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object contacts clients
 */

include_once( dirname( __FILE__ ) . "/DBObject.php" );
include_once( dirname( __FILE__ ) . "/Contact.php" );


class CustomerContact extends DBObject implements Contact{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idbuyer
	 * @param int $idcontact
	 */
	public function __construct( $idbuyer, $idcontact ){
		
		parent::__construct( "contact", false, "idbuyer", $idbuyer, "idcontact", $idcontact );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
	public function getId(){ return $this->recordSet[ "idcontact" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @param int $idbuyer
	 * @return CustomerContact
	 */
	public static function create( $idbuyer ){
		// ------- Mise à jour de l'id gestion des Index  #1161
		$rs =& DBUtil::query( "SELECT MAX( idcontact ) + 1 AS idcontact FROM contact WHERE idbuyer = '" . $idbuyer  . "'" );
		
		$idcontact = $rs->fields( "idcontact" ) === NULL ? 0 : $rs->fields( "idcontact" );
	
		$query = "
		INSERT INTO `contact` ( 
			`idbuyer`, 
			`idcontact`
		) VALUES ( 
			'" . $idbuyer  . "', 
			'$idcontact'
		)";
		//----------
		//print $query ;exit;
		if( DBUtil::query( $query ) === false ){
			
			trigger_error( "Impossible de créer le nouveau contact", E_USER_ERROR );
			exit();
			
		}
		
		return new CustomerContact( $idbuyer, $idcontact );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
	public function setAccount( $login, $password = false ){
		
		if( $password === false )
			$password = Util::getRandomPassword();

		$query = "
		REPLACE INTO user_account( idbuyer, idcontact, login, password, username, lastupdate ) 
		VALUES(
			'" . $this->recordSet[ "idcontact" ] . "',
			'" . $this->recordSet[ "idcontact" ] . "',
			" . DBUtil::quote( $login ) . ",
			MD5( " . DBUtil::quote( $password ) . " ),
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			NOW()
		)";
		
		DBUtil::query( $query );

	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* Contact implementation */
	
	public function getGender()						{ return $this->recordSet[ "title" ]; }
	public function getFirstName()					{ return $this->recordSet[ "firstname" ]; }
	public function getLastName() 					{ return $this->recordSet[ "lastname" ]; }
	public function getFaxnumber() 					{ return $this->recordSet[ "faxnumber" ]; }
	public function getPhonenumber() 				{ return $this->recordSet[ "phonenumber" ]; }
	public function getGSM() 						{ return $this->recordSet[ "gsm" ]; }
	public function getEmail() 						{ return $this->recordSet[ "mail" ]; }
	public function getComment() 					{ return $this->recordSet[ "comment" ]; }
	public function getDepartment()					{ return $this->recordSet[ "iddepartment" ]; }
	public function getFunction() 					{ return $this->recordSet[ "idfunction" ]; }	
	public function getBirthDate()					{ return $this->recordSet[ "birthday" ]; }
	public function getOptin()					{ return $this->recordSet[ "optin" ]; }
	public function getAddress() 					{ /* @todo */ return ""; }
	public function getAddress2() 					{ /* @todo */ return ""; }
	public function getZipcode()					{ /* @todo */ return 0; }
	public function getCity()						{ /* @todo */ return ""; }
	public function getState()						{ /* @todo */ return 0; }
	
	public function allowDelete()					{ /* @todo */ return false; }
	public function isMainContact()					{ return $this->recordSet[ "idcontact" ] == 0; /*@todo pas de valeur 0 pour la clé I*/ }
	
	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>