<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object export Factures CSV
 */

define( "DEBUG_CSVInvoiceExport", 0 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( "$GLOBAL_START_PATH/objects/Invoice.php" );

class CSVInvoiceExport
{

	//--------------------------------------------------------------------------------------------------
	
	private $con;
	private $separator;
	private $text_delimiter;
	private $breakline;
	private $metadata;
	private $data;
	private $idinvoices;
	
	private $output_type;
	private $mime_type;
	private $filename;
	private $errors;
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Constructeur
	 * @param string $filename le nom du fichier de résultat
	 * @param string $output_type le type de fichier de résultat
	 * @param string $separator le séparateur
	 * @param string $text_delimiter l'indicateur de texte
	 * @param string $breakline l'indicateur de saut de ligne
	 */
	function __construct( $filename, $output_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\n" ){
	
		$this->separator 		= $separator;
		$this->text_delimiter 	= "\"";
		$this->breakline 		= $breakline;
		$this->mime_type 		= "";
		$this->filename 		= $filename;
		$this->export_directory = "";
		$this->metadata = array();
		$this->data = array();
		$this->idinvoices = array();
		$this->errors = array();
		
		switch( $output_type ){
		
			case "csv" :

				$this->output_type = $output_type;
				$this->mime_type = "text/x-csv";

				break;
			
			//case "xls" @todo export xls:
			//case "xml" @todo export xml:
				
			default : die( "Unknown output mode '$output_type'" );
		
		}
	
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le tableau des factures
	 * @param array $idinvoices le tableau des factures
	 * @return void
	 */
	public function setInvoices( $idinvoices ){
	
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>setInvoices( $idinvoices )</u>" );
			
		if( is_array( $idinvoices ) )
			$this->idinvoices = $idinvoices;

		if( DEBUG_CSVInvoiceExport )
			print_r( $idinvoices );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Ajoute une facture à la fin du tableau
	 * @param int $idinvoice l'id de la facture à ajouter
	 * @return void
	 */
	private function addInvoice( $idinvoice ){
	
		$this->idinvoices[] = $idinvoice;
			
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Exporte les données
	 * @return int le nombre de lignes exportées
	 */
	public function export(){

		if( $this->setData() === false )
			return 0;

		$this->output();
		
		return count( $this->data );
		
	}

	//--------------------------------------------------------------------------------------------------

	/**
	 * Exécute une requête SQL
	 * @param string $query la requête à éxécuter
	 * @return mixed la l'objet resource mysql ou false en cas d'échec
	 */
	private function query( $query ){

		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>query()</u><br />$query" );
			
		$result =& DBUtil::query( $query );
		
		if( $result === false )
			$this->errors[] = "SQL Error : $query"; 
		
		return $result;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur
	 * @param string $separator le séparateur
	 * @return void
	 */
	public function setSeparator( $separator ){
	
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>setSeparator( $separator )</u>" );
			
		$this->separator = $separator;
		
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le séparateur de texte
	 * @param string $text_delimiter le séparateur
	 * @return void
	 */
	public function setTextDelimiter( $text_delimiter ){
	
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>setTextDelimiter( $text_delimiter )</u>" );
			
		$this->text_delimiter = $text_delimiter;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Initialise le caractère de saut de ligne
	 * @param string $breakline le caractère de saut de ligne
	 * @return void
	 */	
	public function setBreakline( $breakline ){
	
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>setBreakline( $breakline )</u>" );
			
		$this->breakline = $breakline;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Formatte les données
	 * @return void
	 */
	protected function setData(){
		
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>setData()</u>" );
		
		$lang = User::getInstance()->getLang();
		
		$this->data = array();
		
		$this->data[ 0 ] = array(
		
			"Numéro de Facture ou Avoir",
			"Numéro du client facturé",
			"Montant à payer TTC",
			"Type de la pièce (facture/avoir)",
			"Date de la pièce",
			"Échéance de la pièce",
			"Mode de règlement",
			"Devise",
			"Numéro de commande du client",
			"Référence complémentaire",
			"Raison sociale du client facturé",
			"Enseigne",
			"Sigle",
			"Adresse 1",
			"Adresse 2",
			"Code Postal",
			"Ville",
			"SIREN ou COFACE",
			"NIC",
			"Nom du responsable",
			"Téléphone",
			"RIB",
			"Compte comptable",
			"LCR Magnétique N/A O/N",
			"Nom de la banque du tiré",
			"Code pays facturé"
			
		);
		
		$i = 0;
		while( $i < count( $this->idinvoices ) ){
		
			$idinvoice = $this->idinvoices[ $i ];
			
			$query = "
			SELECT bb.idbilling_buyer,
				bb.idbuyer,
				bb.total_amount,
				bb.DateHeure,
				bb.deliv_payment,
				bb.idpayment,
				1 AS idcurrency,
				bb.idorder,
				b.company,
				b.adress,
				REPLACE( b.adress_2, '\r\n', ' - ' ) AS adress_2,
				b.zipcode,
				b.city,
				b.siren,
				b.siret,
				b.naf,
				b.idstate,
				t.title$lang AS title,
				c.lastname,
				c.phonenumber
			FROM billing_buyer bb, buyer b, contact c, title t
			WHERE bb.idbilling_buyer = '$idinvoice'
			AND bb.idbuyer = b.idbuyer
			AND b.idbuyer = c.idbuyer
			AND c.idcontact = bb.idcontact
			AND bb.factor IS NOT NULL
			AND bb.factor <> ''
			AND bb.factor <> 0
			AND c.title = t.idtitle
			AND bb.factor_send_date = '0000-00-00'
			AND bb.status != 'Paid'";
			
			/*
			 * Maintenant que l'affectation des factures au factor est censée être correcte,
			 * il n'y a aucune raison de refaire le test sur le montant étant donné que ce dernier est fait avant
			
			$amountFilters = $this->getAmountFilters();
			
			foreach( $amountFilters as $amountFilter )
				$query .= " AND $amountFilter";
			
			*/
						
			$result = $this->query( $query );
			
			$Invoice = new Invoice( $idinvoice );
			
			$solde = $Invoice->getBalanceOutstanding();
			
			if( $result->RecordCount() && $solde >= DBUtil::getParameterAdmin( "factor_minbuying" ) && ( $solde <= DBUtil::getParameterAdmin( "factor_maxbuying" ) || DBUtil::getParameterAdmin( "factor_maxbuying" ) == 0 ) ){
			
				$invoice = (object)$result->fields;
				
				//date facturation
				
				list( $date, $time ) = explode( " ", $invoice->DateHeure );
				list( $year, $month, $day ) = explode( "-", $date );
				//$year = substr( $year, 2, 2 );
				$DateHeure = "$day/$month/$year";
				
				//date échéance
				
				$tmp = explode( "-", $invoice->deliv_payment );
				if( count( $tmp ) === 3 ){
				
					list( $year, $month, $day ) = explode( "-", $invoice->deliv_payment );
					//$year = substr( $year, 2, 2 );
					$deliv_payment = "$day/$month/$year";
				
				}
				else $deliv_payment = $invoice->deliv_payment;
				
				//siren
	
				if( empty( $invoice->siren ) && !empty( $invoice->siret ) && strlen( $invoice->siret >= 9 ) )
					$invoice->siren = substr( $invoice->siret, 0, 9 );
	
				//type de pièce
				
				$type = "facture"; //@todo:avoirs?
				
				$row = array(
				
					$invoice->idbilling_buyer,
					$invoice->idbuyer,
					number_format( $Invoice->getBalanceOutstanding(), 2, ",", " " ),
					$type,
					$DateHeure,
					$deliv_payment,
					$invoice->idpayment,
					$invoice->idcurrency,
					$invoice->idorder,
					"",
					$invoice->company,
					"",
					"",
					$invoice->adress,
					$invoice->adress_2,
					$invoice->zipcode,
					$invoice->city,
					$invoice->siren,
					$invoice->naf,
					$invoice->title . " " . $invoice->lastname,
					$invoice->phonenumber,
					"",
					"",
					"",
					"",
					$invoice->idstate
					
				);
				
				$this->data[] = $row;
			
			}
			
			$i++;
			
		}
		
		if( DEBUG_CSVInvoiceExport )
			print_r( $this->data );
			
	}

	//--------------------------------------------------------------------------------------------------
	/**
	 * Retourne les conditions pour les montants
	 * @return array $filters les conditions
	 */
	private function getAmountFilters(){
	
		$filters = array();
		
		$query = "
		SELECT paramvalue
		FROM parameter_admin
		WHERE idparameter LIKE 'factor_minbuying'
		LIMIT 1";
		
		$result = $this->query( $query );
		
		if( $result === false )	
			die( "Impossible de récupérer le montant TTC minimal pour les factures à traiter" );
			
		if( ! $result->RecordCount() )
			return $filters;
			
		$parameter = (object)$result->fields;
		
		if( $parameter->paramvalue != "" ){
		
			$factor_minbuying = $parameter->paramvalue;
			$filters[] = "bb.total_amount > '$factor_minbuying'";
			
		}
		
		$query = "
		SELECT paramvalue
		FROM parameter_admin
		WHERE idparameter LIKE 'factor_maxbuying'
		LIMIT 1";
		
		$result = $this->query( $query );
		
		if( $result === false )	
			die( "Impossible de récupérer le montant TTC maximal pour les factures à traiter" );
			
		if( ! $result->RecordCount() )
			return $filters;

		$parameter = (object)$result->fields;
		
		if( $parameter->paramvalue != "" ){
		
			$factor_maxbuying = $parameter->paramvalue;
			$filters[] = "bb.total_amount < '$factor_maxbuying'";
			
		}
		
		return $filters;
				
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Génère le fichier
	 * @return void
	 */
	private function output(){

		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>output()</u>" );
		
		if( headers_sent() ){
		
			$this->errors[] = "Headers already sent";
			$this->abort();
			
			return;
			
		}

		
		header("Content-Type: {$this->mime_type}");
		header('Expires: 0');
		//header( "Content-disposition: attachment; filename={$this->filename}.{$this->output_type}" );
		header('Content-Disposition: attachement; filename="'.$this->filename.'.'.$this->output_type.'"');
		header("Pragma: no-cache");
			
		$rowCount 		= count( $this->data );
		$columnCount 	= count( $this->data[ 0 ] );
		
		$content = "";
		
		$row = 0;
		while( $row < $rowCount ){

			if( $row )
				$content .= chr(13).chr(10);
					
			$column = 0;
			while( $column < $columnCount ){
			
				if( $column )
					$content .= $this->separator;

				$value = $this->data[ $row ][ $column ];

				if( $this->text_delimiter == "\"" )
					$value = str_replace( "\"", "\"\"", $value );
				else $value = $value;
				
				$content .= $this->text_delimiter;
				$content .= $value;
				$content .= $this->text_delimiter;
								
				$column++;
				
			}
			
			$row++;
			
		}
		
		echo $content.chr(13).chr(10);
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Affiche les erreurs
	 */
	private function report(){
			
		echo implode( "<br />", $this->errors );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Arrete l'import et affiche les erreurs
	 * @return void
	 **/
	private function abort(){
		
		if( DEBUG_CSVInvoiceExport )
			$this->debug( "<u>abort()</u>" );

		if( DEBUG_CSVInvoiceExport )
			$this->report();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Récupère les erreurs
	 * @return string les erreurs
	 */
	private function getErrors(){
	
		return $this->errors;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	function debug( $string ){
	
		echo "<br />$string";
		flush();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
}