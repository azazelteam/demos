<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object import Client et Contacts CSV
 */

set_time_limit( 0 );
ini_set( "memory_limit", "128M" );

define( "DEBUG_CSVCUSTOMERIMPORT", 0  );
 
class CSVCustomerImport{

	//--------------------------------------------------------------------------------------------------
	
	private $con;
	private $input_file;
	
	private $input_type;
	private $separator;
	private $text_delimiter;
	private $breakline;
	
	private $date_format;
	
	private $metadata;
	private $headers;
	private $data;
	
	private $allowUpdate;
	private $allowDelete;
	
	private $currentLine;
	private $insertCount;
	private $updateCount;
	private $deleteCount;
	
	private $errors;
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $input_type le type de fichier à importer : csv, xml, xls, ... ( csv par défaut )
	 * @param string $separator le séparateur de champs utilisé ( ; par défaut )
	 * @param string $text_delimiter le délimiteur de textes utilisé ( " par défaut )
	 * @param string $breakline le retour chariot utilisé ( \n par défaut ) 
	 */
	
	function __construct( $input_type = "csv", $separator = ";", $text_delimiter = "\"", $breakline = "\n" ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>CSVCustomerImport( $input_type, $separator, $text_delimiter, $breakline )</u>" );
			
		$this->separator 		= $separator;
		$this->text_delimiter 	= "\"";
		$this->breakline 		= $breakline;
		
		$this->date_format = "FR"; 		//@todo
		
		$this->metadata 	= array();
		$this->data 		= array();
		$this->headers 		= array();
		
		$this->allowDelete = false;
		$this->allowUpdate = false;
		
		$this->currentLine = 0;
		$this->insertCount = 0;
		$this->updateCount = 0;
		$this->deleteCount = 0;
	
		$this->errors = array();
		
		switch( $input_type ){
		
			case "csv" :
			//case "xls" @todo export xls:
			//case "xml" @todo export xml:
			
				$this->input_type = $input_type;
				
				break;
				
			default : die( "Unknown input type '$input_type'" );
		
		}
		
		if( $this->connect() === false )
			die( "Connection failed" );

	}
	
	//--------------------------------------------------------------------------------------------------

	/**
	 * Exécute l'import
	 * @param string $path le chemin du fichier à importer
	 * @return bool true en cas de succès, sinon false
	 */
	
	public function import( $path ){
		
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>import( $path )</u>" );
			
		if( !$this->openFile( $path ) )
			return false;
		
		if( !$this->parseHeaders() ) 
			return false;
		
		$this->currentLine = 2;
		while( $data = fgetcsv( $this->input_file, 8100, $this->separator, $this->text_delimiter ) ){
		
			$this->importLine( $data );
			
			$this->currentLine++;
			
		}
		
		$this->closeFile();

		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ouvre le fichier à importer en lecture seule
	 * @param string $path le chemin du fichier à importer
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function openFile( $path ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>openFile( $path )</u>" );
			
		if( !file_exists( $path ) ){
		
			$this->errors[] = "Le fichier $path n'existe pas";
			$this->abort();
			
			return false;
			
		}
		
		$fp = fopen( $path, "r" );
		
		if( !is_resource( $fp ) ){
		
			$this->errors[] = "Impossible d'ouvrir le fichier $path en lecture";
			$this->abort();
			
			return false;
			
		}
		
		$this->input_file =& $fp;
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Ferme le fichier d'import
	 * @return void
	 */
	
	private function closeFile(){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>closeFile()</u>" );
			
		if( is_resource( $this->input_file ) )
			fclose( $this->input_file );
			
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/*
	 * Analyse les en-têtes du fichier d'import
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function parseHeaders(){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>parseHeaders()</u>" );
			
		//lecture de la première ligne du fichier
		
		$data = fgetcsv( $this->input_file, 8100, $this->separator, $this->text_delimiter );

		//lecture impossible
		
		if( $data === false ){
		
			$this->errors[] = "Le fichier est vide";
			$this->abort();
			
			return false;
			
		}
		
		//première ligne vide ou colonne action absente
		
		if( $data[ 0 ] = null || $data[ 0 ] != "action" ){
			print_r( $data );
			$this->errors[] = "La colonne 'action' est absente";
			$this->abort();
			
			return false;
			
		}
		
		$this->currentLine = 1;
		
		//aucune en-tête
		
		if( count( $data ) < 2 ){
		
			$this->errors[] = "Le fichier ne contient aucune ligne à importer";
			$this->abort();
			
			return false;
			
		}
		
		$this->headers[] = array();
		
		//récupérer les en-têtes
	
		$i = 1;
		while( $i < count( $data ) ){
		
			$heading = $data[ $i ];
			
			//nom de colonne vide
			
			if( empty( $heading ) ){
			
				$this->errors[] = "La colonne n°$i n'a pas de nom";
				$this->abort();
				
				return false;
			
			}
			
			if( !isset( $this->headings[ $heading ] ) ){
			
				if( !$this->setColumnMetadata( $heading ) ){
				
					$this->abort();
					
					return false;
					
				}
				
				$this->headers[ $heading ][ "columnIndex" ] = $i;
					
			}
			
			$i++;
			
		}
		
		if( $this->primaryKeysAvailable( $data ) ){
		
			$this->allowUpdate = true;
			$this->allowDelete = true;

		}
		
		if( DEBUG_CSVCUSTOMERIMPORT ){
			
			$this->debug( "<u>headers</u>" );
			print_r( $this->headers );
			
		}
		
		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie la présence des clés primaires dans les en-têtes du fichier d'import
	 * @param array $headers les en-têtes du fichier d'import
	 * @return bool true si toutes les clés primaires sont présentes, sinon false
	 */
	
	private function primaryKeysAvailable( $headers ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>primaryKeysAvailable( $headers )</u>" );

		reset( $this->headers );
		
		$primaryKeys = array();
		foreach( $this->headers as $export_name => $metadata ){
		
			if( $export_name != "action" && $metadata[ "key" ] == "PRI" )
				$primaryKeys[] = $metadata[ "fieldname" ];
				
		}
		
		return in_array( "idbuyer", $primaryKeys ) && in_array( "idcontact", $primaryKeys );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/*function setPrimaryKeysMetadata(){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>setPrimaryKeysMetadata()</u>" );
			
		$query = "
		SELECT tablename,
			fieldname,
			type,
			size,
			export_name,
			'' AS `default`,
			0 AS primaryKey,
			0 AS columnIndex
		FROM desc_field
		WHERE ( tablename LIKE 'buyer' AND fieldname LIKE 'idbuyer' )
		OR ( tablename LIKE 'contact' AND fieldname LIKE 'idcontact' )";
		
		$result =& $this->query( $query );

		//ambiguité sur le nom de la colonne
		
		if( @mysqli_num_rows( $result ) != 2 )
			die( "Primary keys not found in table `desc_field`" );

		//colonne valide
		
		while( $metadata = @mysql_fetch_assoc( $result ) ){
		
			//nom d'export non défini, la colonne est exportée avec son véritable nom
			
			if( empty( $metadata[ "export_name" ] ) )
				$metadata[ "export_name" ] = $metadata[ "fieldname" ];

			//récupérer les métadonnées
			
			$query = "SHOW COLUMNS FROM `" . $metadata[ "tablename" ] . "` LIKE '" . $metadata[ "fieldname" ] . "'";
			
			$result2 = $this->query( $query );
			
			if( !mysqli_num_rows( $result2 ) )
				die( "Unknown column " . $metadata[ "fieldname" ] . " IN " . $metadata[ "tablename" ] );
			
			$mysqlMetadata = mysql_fetch_assoc( $result2 );
			
			$metadata[ "key" ] 			= $mysqlMetadata[ "Key" ];
			$metadata[ "nullable" ] 	= $mysqlMetadata[ "Null" ] == "YES" ? true : false;
			$metadata[ "default" ] 		= $mysqlMetadata[ "Default" ];
			$metadata[ "extra" ] 		= $mysqlMetadata[ "Extra" ];
			$metadata[ "dbType" ] 		= $mysqlMetadata[ "Type" ];
		
			$this->headers[ $metadata[ "export_name"] ] = $metadata;

		}
		
		return true;
		
	}*/
	
	/*
	 * Récupère les métadonnées d'une colonne du fichier d'import
	 * @param string $heading le nom de la colonne
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function setColumnMetadata( $heading ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>setColumnMetadata( $heading )</u>" );
			
		//récupérer les données dans des_field
			
		$query = "
		SELECT tablename,
			fieldname,
			type,
			size,
			export_name,
			export_info_column
		FROM desc_field
		WHERE tablename IN( 'buyer', 'contact' )
		AND (
			export_name LIKE '" . @Util::html_escape( $heading ) . "'
			OR ( export_name LIKE '' AND fieldname LIKE '" . @Util::html_escape( $heading ) . "' )
		)";

		$result =& $this->query( $query );

		//ambiguité sur le nom de la colonne
		
		if( @mysqli_num_rows( $result ) > 1 
			//&& mysql_result( $result, 0, "fieldname" ) != "idbuyer" 
			//&& mysql_result( $result, 0, "fieldname" ) != "idcontact" ){ 
		    && Util::mysqli_result( $result, 0, 1 ) != "idbuyer" 
			&& Util::mysqli_result( $result, 0, 1 ) != "idcontact" ){ 

			$this->errors[] = "Le nom de la colonne '$heading' est ambigü";

			return false;

		}
		
		if( !@mysqli_num_rows( $result ) ){ //colonne inconnue
		
			$this->errors[] = "Nom de colonne inconnu '$heading'";
			
			return false;
	
		}
		
		//colonne valide
		
		$metadata = @mysqli_fetch_assoc( $result );
		
		//nom d'export non défini, la colonne est exportée avec son véritable nom
		
		if( empty( $metadata[ "export_name" ] ) )
			$metadata[ "export_name" ] = $metadata[ "fieldname" ];
			
		//colonne non typée
		
		if( empty( $metadata[ "type" ] ) ){ 
		
			$this->errors[] = "Aucun type n'a été défini pour la colonne '$heading'";

			return false;
			
		}
		
		//taille de colonne non définie
		
		//@todo : gérer les tailles des champs : Attention il me semble que la taille renseignée dans desc_field sert plutôt à dimensionner les champs des formulaires de l'administration et ne correspond pas à la taille des colonnes dans la base
		
		/*if( empty( $metadata[ "size" ] ) ){ 
		
			$this->errors[] = "No size defined for column '$heading'";
			
			return false;
		
		}*/
		
		//récupérer les métadonnées
		
		$query = "SHOW COLUMNS FROM `" . $metadata[ "tablename" ] . "` LIKE '" . $metadata[ "fieldname" ] . "'";
		
		$result = $this->query( $query );
		
		if( !mysqli_num_rows( $result ) )
			trigger_error( "Unknown column " . $metadata[ "fieldname" ] . " IN " . $metadata[ "tablename" ], E_USER_ERROR );
		
		$mysqlMetadata = mysqli_fetch_assoc( $result );
		
		$metadata[ "key" ] 		= $mysqlMetadata[ "Key" ];
		$metadata[ "nullable" ] = $mysqlMetadata[ "Null" ] == "YES" ? true : false;
		$metadata[ "default" ] 	= $mysqlMetadata[ "Default" ];
		$metadata[ "extra" ] 	= $mysqlMetadata[ "Extra" ];
		$metadata[ "dbType" ] 	= $mysqlMetadata[ "Type" ];
		
		//@todo : utiliser les métadonnées et non plus la table desc_field
		
		$this->headers[ $heading ] = $metadata;

		return true;
			
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Connexion à la base de données
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function connect(){
	
		global 	$GLOBAL_DB_HOST,
				$GLOBAL_DB_NAME,
				$GLOBAL_DB_USER,
				$GLOBAL_DB_PASS;
				
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>connect( $GLOBAL_DB_HOST, $GLOBAL_DB_NAME, $GLOBAL_DB_USER )</u>" );

		if( !isset( $GLOBAL_DB_HOST )
			|| !isset( $GLOBAL_DB_NAME )
			|| !isset( $GLOBAL_DB_USER )
			|| !isset( $GLOBAL_DB_PASS ) )
			die( "Connection parameters not found" );
		
		$this->con = @mysqli_connect( $GLOBAL_DB_HOST, $GLOBAL_DB_USER, $GLOBAL_DB_PASS );
		
		if( !is_resource( $this->con ) ){ 
		
			$this->errors[] = "Echec de la connexion à l'hôte '$GLOBAL_DB_HOST'"; 
			
			return false; 
		
		}

		$ret = @mysqli_select_db( $this->con,$GLOBAL_DB_NAME );
		
		if( $ret === false ) {
		
			$this->errors[] = "Impossible de se connecter à la base de données '$GLOBAL_DB_NAME' : " . @mysqli_error($this->con); 
			return false; 
		
		}

		return true;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Déconnexion de la base de données
	 * @return void
	 */
	
	private function disconnect(){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>disconnect()</u>" );
			
		/*if( is_resource( $this->con ) )
			@mysql_close( $this->con );*/
		
	}

	//--------------------------------------------------------------------------------------------------

	/**
	 * Exécute une requête SQL
	 * @param string $query la requête à éxécuter
	 * @return mixed la l'objet resource mysql ou false en cas d'échec
	 */
	
	private function &query( $query ){

		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>query()</u><br />$query" );
			
		$result = @mysqli_query($this->con, $query  );
		
		if( $result === false )
			$this->errors[] = "SQL Error : $query ( " . @mysqli_error($this->con) . " )"; 

		return $result;
		
	}
	
	//--------------------------------------------------------------------------------------------------
			
	/**
	 * Spécifie le format de date utilisé pour l'import
	 * @param string $date_format le format de date souhaité ( 'FR' ou 'EN' )
	 * @return void
	 */
	
	public function setDateFormat( $date_format ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>setDateFormat( $format )</u>" );
			
		$this->date_format = $date_format;
		
	}
	
	//--------------------------------------------------------------------------------------------------

	/**
	 * Spécifie le séparateur de champ à utiliser pour l'import
	 * @param string $separator le séparateur de champ à utiliser pour l'import
	 * @return void
	 */
	
	public function setSeparator( $separator ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>setSeparator( $separator )</u>" );
			
		$this->separator = $separator;
		
	}

	//--------------------------------------------------------------------------------------------------

	/**
	 * Spécifie le délimiteur de champs textes à utiliser pour l'import
	 * @param string $text_delimiter le délimiteur de champs textes à utiliser pour l'import
	 * @return void
	 */
	
	public function setTextDelimiter( $text_delimiter ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>setTextDelimiter( $text_delimiter )</u>" );
			
		$this->text_delimiter = $text_delimiter;
		
	}
	
	//--------------------------------------------------------------------------------------------------

	/**
	 * Spécifie le retour chariot à utiliser pour l'import
	 * @param string $breakline le retour chariot à utiliser pour l'import
	 * @return void
	 */
	
	public function setBreakline( $breakline ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>setBreakline( $breakline )</u>" );
			
		$this->breakline = $breakline;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Importe une ligne du fichier d'import
	 * @param array $data la ligne à importer
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function importLine( &$data ){

		switch( $data[ 0 ] ){
		
			case "0" : 
			case "création" : 		return $this->insertLine( $data );
			case "1" : 
			case "mise à jour" : 	return $this->updateLine( $data );
			case "2" : 
			case "suppression" : 	return $this->deleteLine( $data );
			
			default : 
			
				$this->errors[] = "'action' inconnue à la ligne n° {$this->currentLine}";
				
				return false;
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Insère une ligne du fichier d'import
	 * @param array $data la ligne à importer
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function insertLine( &$data ){

		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>insertLine( &$data )</u>" );

		//valeurs à récupérer dans d'autres tables
		
		$data = $this->getForeignValues( $data );
			
		//insert client

		$insertCustomer = true; //créer le client + le contact
		
		$idbuyerIndex 	= $this->getColumnIndex( "idbuyer" );
		$idbuyer 		= $idbuyerIndex !== false ? $data[ $idbuyerIndex ] : false;

		if( $idbuyer === false || empty( $idbuyer ) )
			$idbuyer = $this->getNewPrimaryKeyValue( "buyer", "idbuyer" );
		else{
		
			$query = "SELECT 1 FROM buyer WHERE idbuyer = '$idbuyer' LIMIT 1";
			
			$result = $this->query( $query );
			
			if( mysqli_num_rows( $result ) )
				$insertCustomer = false; //créer uniquement le contact
			
		}
		
		$buyerColumns 	=& $this->getTableColumnNames( "buyer", true );
		
		if( $insertCustomer && count( $buyerColumns ) ){
		
			$buyerExportNames 	=& $this->getTableExportNames( "buyer", true );
			$buyerValues 		=& $this->getFormatedValues( $data, $buyerExportNames );
			
			$query = "
			INSERT INTO buyer ( idbuyer, `" . implode( "`,`", $buyerColumns ) . "` )
			VALUES( '$idbuyer'";
		
			foreach( $buyerValues as $buyerValue ){
			
				$query .= ",";
				$query .= $buyerValue === NULL ? "NULL" : "'$buyerValue'";
				
			}
			
			$query .= " )";
			
			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
			
				$this->insertCount += @mysqli_affected_rows( $this->con );
			
			}
			
		}
		
		//insert contact
		
		$idcontactIndex = $this->getColumnIndex( "idcontact" );
		$idcontact 		= $idcontactIndex !== false ? $data[ $idcontactIndex ] : false;

		if( $idcontact === false || $idcontact == "" ){
			
			$result = $this->query( "SELECT MAX( idcontact ) + 1 AS idcontact FROM contact WHERE idbuyer = '$idbuyer'" );
			if( Util::mysqli_result( $result, 0, 0 ) == NULL )
			//if( mysql_result( $result, 0, "idcontact" ) == NULL )
				$idcontact = 0;
			else $idcontact = Util::mysqli_result( $result, 0, 0 );
			
		}
		else{
		
			$query = "SELECT 1 FROM contact WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1";
			
			$result = $this->query( $query );
			
			if( mysqli_num_rows( $result ) ){
			
				$this->errors[] = "Echec lors de l'insert de la ligne n° {$this->currentLine} : ce contact existe déjà ( contact n° $idcontact )";
				
				return;
				
			}
			
		}
			
		$contactColumns =& $this->getTableColumnNames( "contact", true );
		
		if( count( $contactColumns ) ){

			$contactExportNames =& $this->getTableExportNames( "contact", true );
			$contactValues 		=& $this->getFormatedValues( $data, $contactExportNames );
			
			$query = "
			INSERT INTO contact( idbuyer, idcontact, `" . implode( "`,`", $contactColumns ) . "` )
			VALUES( '$idbuyer', '$idcontact'";
		
			foreach( $contactValues as $contactValue ){
			
				$query .= ",";
				$query .= $contactValue === NULL ? "NULL" : "'$contactValue'";
				
			}
			
			$query .= ")";
			
			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
				
				$this->insertCount += @mysqli_affected_rows( $this->con );
			
			}
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Met à jour une ligne du fichier d'import
	 * @param array $data la ligne à importer
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function updateLine( $data ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>updateLine( &$data )</u>" );

		if( !$this->allowUpdate ){
		
			$this->errors[] = "n° de compte ou n° de contact absent à la linge n° {$this->currentLine}";
			 
			return;
			
		}
		
		//valeurs à récupérer dans d'autres tables
		
		$data = $this->getForeignValues( $data );
		
		//update buyer
		
		$idbuyerIndex 	= $this->getColumnIndex( "idbuyer" );
		$idbuyer 		= $data[ $idbuyerIndex ];
		$buyerColumns 	=& $this->getTableColumnNames( "buyer", true );

		if( count( $buyerColumns ) ){
		
			$buyerExportNames 	=& $this->getTableExportNames( "buyer", true );
			$buyerValues 		=& $this->getFormatedValues( $data, $buyerExportNames );
			
			$query = "UPDATE buyer SET ";
			
			$i = 0;
			$columnCount = count( $buyerColumns );
			while( $i < $columnCount ){
				
				if( $i )
					$query .= ", ";
					
				$query .= "`" . $buyerColumns[ $i ] . "` = ";
				$query .= $buyerValues[ $i ] === NULL ? "NULL" : "'" . $buyerValues[ $i ] . "'";
				
				$i++;
				
			}
			
			$query .= " WHERE idbuyer = '$idbuyer' LIMIT 1";

			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
				
				$this->updateCount += @mysqli_affected_rows( $this->con );
			
			}
			
		}
		
		//update contact
		
		$idcontactIndex = $this->getColumnIndex( "idcontact" );
		$idcontact 		= $data[ $idcontactIndex ];
		$contactColumns =& $this->getTableColumnNames( "contact", true );
		
		if( count( $contactColumns ) ){
		
			$contactExportNames =& $this->getTableExportNames( "contact", true );
			$contactValues 		=& $this->getFormatedValues( $data, $contactExportNames );
			
			$query = "UPDATE contact SET ";
			
			$i = 0;
			$columnCount = count( $contactColumns );
			while( $i < $columnCount ){
				
				if( $i )
					$query .= ", ";
					
				$query .= "`" . $contactColumns[ $i ] . "` = ";
				$query .= $contactValues[ $i ] === NULL ? "NULL" : "'" . $contactValues[ $i ] . "'";
				
				$i++;
				
			}
			
			$query .= " WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1";
		
			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
			
				$this->updateCount += @mysqli_affected_rows( $this->con );
				
			}
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Supprime une ligne du fichier d'import
	 * @param array $data la ligne à importer
	 * @return bool true en cas de succès, sinon false
	 */
	
	private function deleteLine( $data ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>deleteLine( &$data )</u>" );

		if( !$this->allowDelete ){
		
			$this->errors[] = "N° de compte ou de contact absent à la ligne n° {$this->currentLine}";
			 
			return;
			
		}
		
		$idbuyerIndex 	= $this->getColumnIndex( "idbuyer" );
		$idbuyer 		= $data[ $idbuyerIndex ];
		$idcontactIndex = $this->getColumnIndex( "idcontact" );
		$idcontact 		= $data[ $idcontactIndex ];
		
		if( $idcontact == 0 ){
		
			$query = "DELETE FROM `contact` WHERE idbuyer = '$idbuyer'";
			
			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
				
				$this->deleteCount += @mysqli_affected_rows( $this->con );
			
			}
			
			$query = "DELETE FROM `buyer` WHERE idbuyer = '$idbuyer' LIMIT 1";
	
			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
				
				$this->deleteCount += @mysqli_affected_rows( $this->con );
				
			}
			
		}
		else{
		
			$query = "DELETE FROM `contact` WHERE idbuyer = '$idbuyer' AND idcontact = '$idcontact' LIMIT 1";
			
			if( !isset( $_GET[ "dev" ] ) ){
			
				$result =& $this->query( $query );
			
				$this->deleteCount += @mysqli_affected_rows( $this->con );
				
			}
			
		}
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne l'index de la colonne $column dans le fichier d'import
	 * @param string $column le nom de la colonne recherchée
	 * @return mixed l'index de la colonne $column si elle existe, sinon false
	 */
	
	private function getColumnIndex( $column ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>getColumnIndex( $column )</u>" );
			
		reset( $this->headers );

		foreach( $this->headers as $export_name => $metadata ){
		
			if( $export_name != "action" && $metadata[ "fieldname" ] == $column )
				return $metadata[ "columnIndex" ];
				
		}

		return false;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/*
	 * Recherche les noms de toutes les colonnes qui concernent la table $table dans le fichier d'import
	 * @param string $table le nom de la table
	 * @param bool $ignorePrimaryKeys permet d'ignorer les colonnes qui sont des clés primaires
	 * @return array un tableau indicé contenant les noms de toutes les colonnes qui concernent la table $table dans le fichier d'import
	 */
	
	private function &getTableColumnNames( $table, $ignorePrimaryKeys = false ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>getTableColumnNames( $table, $ignorePrimaryKeys )</u>" );
			
		$columns = array();
		
		reset( $this->headers );
		foreach( $this->headers as $export_name => $metadata ){
		
			if( $export_name != "action" && $metadata[ "tablename" ] == $table ){
			
				if( $ignorePrimaryKeys === false || !$metadata[ "key" ] == "PRI" )
					$columns[] = $metadata[ "fieldname" ];
				
			}
				
		}
		
		return $columns;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Recherche les noms d'export de toutes les colonnes qui concernent la table $table dans le fichier d'import
	 * @param string $table le nom de la table
	 * @param bool $ignorePrimaryKeys permet d'ignorer les colonnes qui sont des clés primaires
	 * @return array un tableau indicé contenant les noms d'export de toutes les colonnes qui concernent la table $table dans le fichier d'import
	 */
	
	private function &getTableExportNames( $table, $ignorePrimaryKeys = false ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>getTableExportNames( $table, $ignorePrimaryKeys )</u>" );
			
		$export_names = array();
		
		reset( $this->headers );
		foreach( $this->headers as $export_name => $metadata ){
		
			if( $export_name != "action" && $metadata[ "tablename" ] == $table ){
			
				if( !$ignorePrimaryKeys || !$metadata[ "key" ] == "PRI" )
					$export_names[] = $export_name;
				
			}
				
		}
		
		return $export_names;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne une clé primaire disponible pour une table
	 * @param string $table le nom de la table
	 * @param strin $key le nom de la clé primaire
	 * @return int l'identifiant libre
	 */
	private function getNewPrimaryKeyValue( $table, $key ){
	
		$query = "SELECT MAX( `$key` ) + 1 AS newid FROM `$table`";
		
		$result = $this->query( $query );
		
		return  Util::mysqli_result( $result, 0, 0 );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Remplacement des valeurs qui font références à d'autres tables que buyer et contact
	 * ex : remplacer 'France' par '1' ( idstate = 1 dans la table state )
	 * @param $data la ligne CSV à traiter
	 * @return array la ligne CSV traitée
	 */
	private function getForeignValues( $data ){

		$i = 0;
		foreach( $this->headers as $export_name => $metadata ){
		
			if( $i && strlen( $metadata[ "export_info_column" ] ) ){
				
				list( $foreignTable, $foreignColumn, $foreignKey, $useless ) = explode( ":", $metadata[ "export_info_column" ] );
				
				$query = "
				SELECT `$foreignKey` AS `value`
				FROM `$foreignTable`
				WHERE `$foreignColumn` = '" . Util::html_escape( $data[ $metadata[ "columnIndex" ] ] ) . "'
				LIMIT 1";

				$result = $this->query( $query );
				
				$data[ $metadata[ "columnIndex" ] ] = mysqli_num_rows( $result ) ?  Util::mysqli_result( $result, 0) : $metadata[ "default" ];
				
			}
			
			$i++;
			
		}
	
		return $data;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Formate toutes les valeurs de la ligne courante
	 * @param array $$data la ligne courante
	 * @param array $export_names les noms des colonnes dont on veut formater les valeurs
	 * @return array un tableau indicé contenant les toutes les valeurs des colonnes $export_names formatées
	 */
	
	private function &getFormatedValues( &$data, &$export_names ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>getFormatedValues( &$data, &$export_names )</u>" );
			
		$values = array();
		foreach( $export_names as $export_name ){
		
			$columnIndex 	= $this->headers[ $export_name ][ "columnIndex" ];
			$default 		= $this->headers[ $export_name ][ "default" ];
			$nullable 		= $this->headers[ $export_name ][ "nullable" ];
			
			$value = $data[ $columnIndex ];

			//$size = $this->headers[ $export_name ][ "size" ];
			//@todo : gérer la taille des champs
			
			$type = $this->headers[ $export_name ][ "type" ];
			
			switch( $type ){ //char, int, dec, date, time

				case "dec" :	$values[] = $this->formatDecimal( $value, $default, $nullable  ); 	break;
				case "date" :	$values[] = $this->formatDate( $value, $default, $nullable ); 		break;
				case "int" :	$values[] = $this->formatInteger( $value, $default, $nullable ); 	break;
				case "time" :	$values[] = $this->formatTime( $value, $default, $nullable ); 		break;
				case "char" : 	$values[] = $this->formatString( $value, $default, $nullable ); 	break;
				
				default : die( "Uknown type '$type' in table desc_field" );
				
			}
			
		}
		
		return $values;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Formate une valeur décimale
	 * @param string $value la valeur à formater
	 * @return float la valeur décimale
	 */
	
	private function formatDecimal( $value, $default, $nullable ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>formatDecimal( $value )</u>" );
			
		if( empty( $value ) ){
			
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid decimal format at line {$this->currentLine} : $value";
			
			return 0.0;
			
		}
		
		$value = str_replace( " ", "", $value );
		$value = str_replace( ",", ".", $value );
		
		if( preg_match( "/^[^0-9\.-]\$/", $value ) === false ){
		
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid decimal format at line {$this->currentLine} : $value";
			
			return 0.0;
			
		}
		
		return $value;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Formate un entier
	 * @param string $value la valeur à formater
	 * @return int la valeur entière
	 */
	
	private function formatInteger( $value, $default, $nullable ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>formatInteger( $value )</u>" );
			
		if( $value == "" ){
		
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid integer format at line {$this->currentLine} : $value";
			
			return 0;
			
		}
			
		if( !preg_match( "/^[^-0123456789]+\$/", $value ) ){
		
			$this->error[] = "Invalid integer format at line {$this->currentLine} : $value";
			$integer = ereg_replace( "^[^-0123456789]\$", "", $value );
			
			if( $integer == "" ){
			
				if( $default )
					return $default;
				
				if( $nullable )
					return NULL;

				$this->error[] = "Invalid integer format at line {$this->currentLine} : $value";
			
				return 0;
			
			}
				
		}
		
		return $value;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Formate une valeur de type TIME ( MySQL )
	 * @param string $value la valeur à formater
	 * @return string la valeur TIME
	 */
	
	private function formatTime( $value, $default, $nullable ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>formatTime( $value )</u>" );
			
		if( empty( $value ) ){
		
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid time format at line {$this->currentLine} : $value";
			
			return "00:00:00";
			
		}
			
		if( preg_match( "/^[0-9]{2}-[0-9]{2}-[0-9]{2}\$/", $value ) === false ){
		
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid time format at line {$this->currentLine} : $value";
			
			return "00:00:00";
			
		}
			
		return $value;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Formate une valeur DATE ou DATETIME ( MySQL )
	 * @param string $value la valeur à formater
	 * @return string la valeur DATE ou DATETIME
	 */
	
	private function formatDate( $value, $default, $nullable ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>formatDate( $value )</u>" );
			
		//champ vide
		
		if( empty( $value ) ){
		
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid date format at line {$this->currentLine} : $value";
			
			return "";

		}
		
		//vérification s'il s'agit d'un format date ou datetime
		
		$date = $value;
		$time = "";
		
		$tokens = explode( " ", $value );
		
		if( count( $tokens ) == 2 )
			list( $date, $time ) = $tokens;
		$time = "";
		
		//vérification du format de l'heure
		
		if( !empty( $time ) && preg_match( "/^[0-9]{2}:[0-9]{2}:[0-9]{2}\$/", $time ) === false )
			$time = "00:00:00";
			
		//vérification du format de date
		
		$regs = array();
		
		if( preg_match( "/^([0-9]{4})[^0-9]{1}([0-9]{2})[^0-9]{1}([0-9]{2})\$/", $date, $regs ) ) //YYYY-mm-dd
			$date = $regs[ 1 ] . "-" . $regs[ 2 ] . "-" . $regs[ 3 ];
		else if( preg_match( "/^([0-9]{2})[^0-9]{1}([0-9]{2})[^0-9]{1}([0-9]{4})\$/", $date, $regs ) ) //dd-mm-YYYY
			$date = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
		else if( preg_match( "/^([0-9]{2})[^0-9]{1}([0-9]{2})[^0-9]{1}([0-9]{2})\$/", $date, $regs ) ){ //dd-mm-YY (FR) ou mm-dd-YY (EN)
		
			$lastcentury = intval( substr( date( "Y" ), 0, 2 ) );
			
			if( $this->date_format == "FR" )
				$date = sprintf( "%04d-%02d-%02d", $lastcentury * 100 + $regs[ 3 ], $regs[ 2 ], $regs[ 1 ] );
			else $date = sprintf( "%04d-%02d-%02d", $lastcentury * 100 + $regs[ 3 ], $regs[ 1 ], $regs[ 2 ] );
			
		} 
		else{
		
			if( $default )
				return $default;
			
			if( $nullable )
				return NULL;
				
			$this->error[] = "Invalid date format at line {$this->currentLine} : $value";
			
			$date = "0000-00-00";
			
		}

		return empty( $time ) ? $date : "$date $time";
		
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Formate une valeur VARCHAR ( MySQL )
	 * @param string $value la valeur à formater
	 * @return string la valeur VARCHAR
	 */
	
	private function formatString( $value, $default, $nullable ){
	
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>formatString( $value )</u>" );

		if( empty( $value ) && $nullable )
			return NULL;
			
		return @Util::html_escape( $value );
		
	}

	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Rapporte toutes les erreurs survenues durant l'import ( écriture sur la sortie standard )
	 * @return void
	 */
	
	public function report(){
			
		echo implode( "<br />", $this->errors );
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Annule l'import
	 * @return void
	 */
	
	private function abort(){
		
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->debug( "<u>abort()</u>" );
			
		$this->disconnect();
		$this->closeFile();
		
		if( DEBUG_CSVCUSTOMERIMPORT )
			$this->report();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
	/**
	 * Retourne les erreurs survenues durant l'import
	 * @return array les erreurs survenues durant l'import
	 */
	
	public function getErrors(){
	
		return $this->errors;
		
	}
	
	//--------------------------------------------------------------------------------------------------
	/**
	 * Retourne le nombre d'erreurs
	 * @return int le nombre d'erreurs
	 */
	public function getErrorCount(){ 		return count( $this->errors ); 	}
	
	/**
	 * Retourne le nombre de lignes parcourues
	 * @return int le nombre de lignes
	 */
	public function getParsedLineCount(){ 	return $this->currentLine - 1; 	}
	
	/**
	 * Retourne le nombre de lignes insérées
	 * @return int le nombre de lignes
	 */
	public function getInsertCount(){ 		return $this->insertCount; 		}
	
	/**
	 * Retourne le nombre de lignes modifiées
	 * @return int le nombre de ligne
	 */
	public function getUpdateCount(){ 		return $this->updateCount; 		}
	
	/**
	 * Retourne le nombre de lignes supprimées
	 * @return int le nombre de ligne
	 */
	public function getDeleteCount(){ 		return $this->deleteCount; 		}

	//--------------------------------------------------------------------------------------------------
	
	function debug( $string ){
	
		echo "<br />$string";
		flush();
		
	}
	
	//--------------------------------------------------------------------------------------------------
	
}