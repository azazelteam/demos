<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des couleurs
 */

include_once("$GLOBAL_START_PATH/objects/adm_table.php");

class ADMCOLOR extends ADMTABLE
{
var $DEBUG = false;
var $id_color = 0;
var $id_supplier = 0;
var $colorinfo = false;
var $n = 0 ;

/**
 * constructeur
 * @param int $id l'identifiant de la couleur ( optionnel )
 */
function __construct($id=0)
{
    parent::__construct('color');
	//ADMTABLE::ADMTABLE('color');
	$this->id_color = $id;
	$this->MakeEmptyRecord();
	if($id>0) $this->LoadRecord('WHERE `idcolor`='.$this->id_color);
}

/**
 * Récupère les informations sur les couleurs disponibles pour un fournisseur donné
 * @param int $id_supplier l'identifiant du fournisseur
 * @return void
 */
public function LoadColorsForSupplier($id_supplier)
{
	$this->colorinfo = DBUtil::getConnection()->GetAll('SELECT * FROM `color`  WHERE `idsupplier`='.$id_supplier.' ORDER BY `name`');
	if($this->DEBUG) echo 'DEBUG LoadColorsForSupplier : SELECT * FROM color,color_product  WHERE  color.idcolor=color_product.idcolor AND idsupplier='.$id_supplier,' DEBUG';
	$this->n =  count($this->colorinfo);
}

/**
 * Récupère les informations sur les couleurs disponibles pour un produit donné
 * @param int $idproduct l'identifiant du produit
 * @return void
 */
public function LoadColorsForProduct($idproduct)
{
	$ids=array();
	for($i=0;$i<$this->n;$i++) {
		$ids[$i] = $this->colorinfo[$i]['idcolor'];
	}
	$rs = DBUtil::getConnection()->Execute('SELECT * FROM `color_product` WHERE `idproduct`=' .$idproduct.' ORDER BY `display`');
	while (!$rs->EOF) {
		$idc = $rs->fields['idcolor'];
		$idx = array_search($idc, $ids);
		if( $idx !== false ) {
			$this->colorinfo[$idx] = array_merge ($this->colorinfo[$idx], $rs->fields);
		}
		//for($i=0;$i<$this->n;$i++) {if($idc == $ids[$i] ) $idc = $this->colorinfo[$i]['idcolor'];} 
		$rs->MoveNext();
		}
}
/**
 * Enregistre les informations sur les couleurs disponibles pour un produit donné
 * @param int $idproduct l'identifiant du produit
 * @return void
 */
public function UpdateColorsForProduct($idproduct)
{
	$rs = DBUtil::query('DELETE FROM `color_product` WHERE `idproduct`=' .$idproduct);
	$data = DBUtil::query('SELECT MAX(`idcolor_product`) AS `lastid` FROM `color_product`');
	$lastid = $data->fields("lastid");;
	$newid = $lastid+1;

	if(isset($_REQUEST['select'])){
		
		$countColors = count($_REQUEST['idcolor']);
		
		
		/*foreach($_REQUEST['select'] as $v) {
			$n = count($_REQUEST['idcolor']);
			$s = -1; 
			for($i=0;$i<$n;$i++) {
				if($_REQUEST['idcolor'][$i] == $v) $s = $i;
			}
			if($s > -1) {
				$lastid++;
				$Arr_ValuesInsert = array($lastid);
				$Arr_ValuesInsert[]= 0+$idproduct;
				$Arr_ValuesInsert[]= 0+$v; //idcolor
				$Arr_ValuesInsert[]= 0+$_REQUEST['display'][$s];
				$Arr_ValuesInsert[] = "'".addslashes(stripslashes($_REQUEST['delay'][$s]))."'";
				$Arr_ValuesInsert[] = "'".addslashes(stripslashes($_REQUEST['percent'][$s]))."'";
				$Arr_ValuesInsert[] = "'".addslashes(stripslashes($_REQUEST['price'][$s]))."'";
				$Arr_ValuesInsert[] = "'".date("Y-m-d H:i:s")."'";
				$Arr_ValuesInsert[] = "'".User::getInstance()->get( "login" )."'";
				$Str_ValuesInsert = implode(', ',$Arr_ValuesInsert);
				$sql = 'INSERT INTO `color_product` (`idcolor_product`,`idproduct`,`idcolor`,`display`,`delay`,`percent`,`price`,`lastupdate`,`username`) VALUES(';
				$sql .= $Str_ValuesInsert.')' ;
				$rs = DBUtil::getConnection()->Execute($sql);
			}
		}*/

		for($i=0 ; $i<$countColors ; $i++){
			$idcolor_product	=	$newid;
			$idproduct			=	$idproduct;
			$idcolor			=	$_REQUEST['idcolor'][$i];
			$display			=	$_REQUEST['display'][$i];
			$delay				=	$_REQUEST['delay'][$i];
			$percent			=	$_REQUEST['percent'][$i];
			$price				=	$_REQUEST['price'][$i];
			$lastupdate			=	date("Y-m-d H:i:s");
			$username			=	User::getInstance()->get( "login" );
			
			if( in_array( $idcolor, $_REQUEST[ "select" ] ) ){
				
				$sql = "INSERT INTO `color_product` (`idcolor_product`,`idproduct`,`idcolor`,`display`,`delay`,`percent`,`price`,`lastupdate`,`username`) VALUES(";
				$sql .= "'$idcolor_product','$idproduct','$idcolor','$display','$delay','$percent','$price','$lastupdate','$username')" ;
				$rs = DBUtil::getConnection()->Execute($sql);
				$newid++;
				
			}
			
		}
	}
}

/**
 * Vérifie si une couleur donnée est sélectionnée ou non
 * @param int $i l'identifiant de la couleur
 * @return string la chaîne 'checked="checked"' si la couleur est sélectionnée, sinon ''
 */
public function IsSelected($i)
{
	if(isset($this->colorinfo[$i]['idcolor_product'])) return 'checked="checked"';//$this->colorinfo[$i][$s];
	return '';
}

/**
 * Retourne une information donnée sur la $i-ième couleur
 * @param int $i l'index de la couleur
 * @param int $s l'information recherchée
 */
public function GetColorInfo($i,$s)
{
	if(isset($this->colorinfo[$i][$s])) return $this->colorinfo[$i][$s];
	else return '';
}

}//EOC

?>
