<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object tissu pour une référence
*/
 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/catalog/SaleableOption.php" );

		
class Cloth implements SaleableOption{

	//----------------------------------------------------------------------------
	
	private $idproduct;
	private $idcloth;
	
	private $idtissus_product;
	private $price;
	private $rate;
	private $delay;
	
	private $name;
	private $link_tissu;
	private $image;
		
	//----------------------------------------------------------------------------
	
	function __construct( $idproduct, $idcloth ){
		
		$this->idproduct 	= $idproduct;
		$this->idcloth 		= $idcloth;
		
		$this->init();
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float
	 */
	public function getAmount(){
		
		return $this->price > 0.0 ? $this->price : false;
		
	}
	
	/* ------------------------------------------------------------------------------------------ */
	/**
	 * @access public
	 * @return float
	 */
	public function getRate(){
		
		return $this->rate > 0.0 ? $this->rate : false;
		
	}
	
	//----------------------------------------------------------------------------
	
	private function init(){
	
		

		$query = "
		SELECT t.name, 
			t.link_tissus,
			t.image,
			tp.idtissus_product,
			tp.idproduct,
			tp.price,
			tp.rate,
			tp.delay
		FROM tissus t, tissus_product tp
		WHERE t.idtissus = '{$this->idcloth}'
		AND t.idtissus = tp.idtissus
		AND tp.idproduct = '{$this->idproduct}'
		LIMIT 1";

		$rs =& DBUtil::query( $query );
		
		if( $rs === false  )
			die( "Impossible d'appliquer le tissu n°{$this->idcloth}" );
		
		if( !$rs->RecordCount() )
			return;
			
		$this->idtissus_product = $rs->fields( "idtissus_product" );
		$this->price 			= $rs->fields( "price" );
		$this->rate 			= $rs->fields( "rate" );
		$this->delay 			= $rs->fields( "delay" );
	
		$this->name 			= $rs->fields( "name" );
		$this->link_tissu 		= $rs->fields( "link_tissu" );
		$this->image 			= $rs->fields( "image" );
			
	}
	
	//----------------------------------------------------------------------------

	public function getId(){ return $this->idcloth; }
	public function getName(){ return $this->name; }
	public function getImageURI(){ return $this->image; }
	
	//----------------------------------------------------------------------------
	
	/**
	 * @todo : deprecated : temporaire pour pouvoir instancier un objet ARTICLE ( => revoir le constructeur de l'objet ARTICLE )
	 * @return un tableau bizarre pour pouvoir instancier un objet ARTICLE avec un tissu
	 */
	 
	public function getClothInfos(){
		
		$cloth = array(
		
			"idtissus_product" 	=> $this->idtissus_product,
			"idtissus" 			=> $this->idcloth,
			"idproduct" 		=> $this->idproduct,
			"price" 			=> $this->price,
			"rate" 				=> $this->rate,
			"delay" 			=> $this->delay,
			"name" 				=> $this->name,
			"link_tissus" 		=> $this->link_tissus,
			"image" 			=> $this->image

		);

		return array( $this->idcloth => $cloth );
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * magique!
	 */

	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
}

?>