<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object ligne article
 */
 
include_once( dirname( __FILE__ ) . "/DBObject.php" );


		
class DeliveryNoteItem extends DBObject{

	//----------------------------------------------------------------------------
	/**
	 * @var DeliveryNote $deliveryNote
	 * @access protected
	 */
	protected $deliveryNote;
	
	//----------------------------------------------------------------------------
	/**
	 * Crée une nouvelle ligne article pour le devis
	 * @param Estimate $estimate le devis
	 * @param int $idrow le numéro de la ligne article ( doit commencer à 1 pour les ERP )
	 */
	public function __construct( DeliveryNote $deliveryNote, $idrow ){

		DBObject::__construct( "bl_delivery_row", false, "idbl_delivery", $deliveryNote->getId(), "idrow", intval( $idrow ) );

		$this->deliveryNote =& $deliveryNote;

	}

	//----------------------------------------------------------------------------
	
	/**
	 * Clone une ligne article donnée
	 * @param DeliveryNoteItem la ligne article à cloner
	 * @return void
	 */
	public function cloneItem( DeliveryNoteItem &$item ){
		
		$idrow = $this->recordSet[ "idrow" ];
		
		$this->recordSet = $item->recordSet;

		$this->recordSet[ "idbl_delivery" ] 	= $this->deliveryNote->getId();
		$this->recordSet[ "idrow" ] 			= $idrow;
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>