<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object règlements fournissseurs
 */

include_once( dirname( __FILE__ ) . "/classes.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );
include_once( dirname( __FILE__ ) . "/Salesman.php" );

 
class RegulationsSupplier{

	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var int $idregulations_supplier le numéro de paiement en franglish
	 */
    private $idregulations_supplier;
    
    //----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int $idregulations_supplier le numéro de paiement
	 */
	function __construct( $idregulations_supplier ){
		
		$this->idregulations_supplier 	= $idregulations_supplier;

		$this->fields 				= array();
		
		//données de la base
		
		$this->load();
		$this->salesman = new Salesman( $this->fields[ "iduser" ] );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'un attribut donné de la facture fournisseur
	 * @param string $key l'attribut de la facture fournisseur dont on souhaite obtenir la valeur
	 * @return mixed la valeur de l'attribut
	 */
	 
	public final function get( $key ){ 
		
		return $this->fields[ $key ]; 
		
	}
	
	//----------------------------------------------------------------------------
		
	/**
	 * Affecte une valeur à une propriété de la facture fournisseur donnée
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à affecter
	 * @return void
	 */
	 	 
	public final function set( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Charge les données depuis la base de données
	 */
	public function load(){
		
		$query = "
		SELECT * FROM `regulations_supplier`
		WHERE `idregulations_supplier` = " .$this->idregulations_supplier."
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$this->fields = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Créé un nouveau paiement vierge pour une ou plusieurs factures données
	 * @static
	 * @param array $billings les numéros de factures concernés par le paiement
	 * @return regulationssupplier un nouveau paiement
	 */
	public static function &createRegulationsSupplier( $billings , $idsupplier ){
	 	
		/* tempo debug */
		
		if( !is_array( $billings ) )
			$billings = explode(",",$billings);
			
		foreach( $billings as $invoice )
			if( floatval( $invoice[ "amount" ] ) == 0.0 )
				trigger_error( "BUG CREATION REGLEMENT FOURNISSEUR POUR 0 EURO", E_USER_ERROR );
		
		/* fin debug */
		
	 	$iduser = User::getInstance()->getId();
	 	$login = User::getInstance()->get( "login" );
	 	
	 	//chercher un identifiant libre
	 	$q = "SELECT MAX(idregulations_supplier) as id FROM regulations_supplier";
	 	$r =& DBUtil::query( $q );
	 	
	 	if( $r === false )
	 		die( "Impossible de récupérer un identifiant pour le paiement" );
	 		
	 	$id = $r->fields("id") + 1;
	 	
	 	//réserver le numero de paiement	 	
	 	$query = "
		INSERT INTO `regulations_supplier` ( 
			idregulations_supplier,
			idsupplier, 
			iduser,
			DateHeure
		) VALUES ( 
			'$id',
			'$idsupplier',
			'$iduser',
			NOW() 
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false )
			die( "Impossible de créer le nouveau paiement" );
		
		//enregistrer le numéro de paiement pour les factures concernées
		if(!is_array($billings))
			$bil = explode(",",$billings);
		else
			$bil = $billings;
		
		foreach( $bil as $invoice ){

			$idbilling_supplier = $invoice[ "idbilling" ];
			$amount 			= $invoice[ "amount" ];			
			$rebate 			= isset( $invoice[ "rebate_amount" ] ) ? $invoice[ "rebate_amount" ] : 0.0;
			
			$query = "INSERT INTO billing_regulations_supplier (
							billing_supplier,
							idregulations_supplier,
							idsupplier,
							amount,
							rebate_amount,
							username,
							lastupdate
					 ) VALUES (
							'$idbilling_supplier',
							$id,
							$idsupplier,
							'$amount',
							'$rebate',
							'$login',
							NOW()
					 )";
			
			$ret =& DBUtil::query( $query );
		
			if( $ret === false )
				die( "Impossible d'enregistrer le paiement pour la facture $idbilling_supplier" );
					
		}
			
		//créer le paiement
		
		$regulationssupplier = new RegulationsSupplier( $id );
	 	
	 	return $regulationssupplier;
	 	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la liste des factures concernées par le règlement
	 * @return array $billings, les factures correspondantes au règlement
	 */
	 
	 public function getBillings(){
	 	
	 	$q = "SELECT billing_supplier FROM billing_regulations_supplier WHERE idregulations_supplier=".$this->idregulations_supplier."";
	 	$r =& DBUtil::query( $q );
	 	
	 	return $r;
	 }
	
	//----------------------------------------------------------------------------
	
	/**
	 * Sauvegarde l'objet dans la table
	 * @access public
	 * @return void
	 */
	 
	public function update(){
		
		$username = User::getInstance()->get( "login" );
		$lastupdate = date( "Y-m-d H:i:s" );
		
		$this->set( "username" , $username );
		$this->set( "lastupdate" , $lastupdate );
		
		//mise à jour

		$updatableFields = array();
		foreach( $this->fields as $fieldname => $value ){
			
			if( $fieldname != "idregulations_supplier" )
				$updatableFields[] = $fieldname;
			
		}
		
		$query = "
		UPDATE `regulations_supplier`
		SET ";
		
		$i = 0;
		foreach( $updatableFields as $fieldname ){
		
			if( $i )
				$query .= ", ";
					
			$query .= "`$fieldname` = '" . Util::html_escape( $this->fields[ $fieldname ] ) . "'";
		
			$i++;
			
		}
		$query.=" WHERE `idregulations_supplier` = '".$this->idregulations_supplier."'";
		
		DBUtil::query( $query );
		
	 }
	 
	 //----------------------------------------------------------------------------
	
	/**
	 * Enregistre un règlement pour une facture donnée
	 * @param SupplierInvoice la facture
	 * @param float $amount le montant réglé
	 */
	public static function createSupplierPayment( SupplierInvoice &$supplierinvoice, $idpayment, $amount , $acompte = false, $payment_date, $deliv_payment="", $comment = "" ){
		
		$regulationssupplier =& RegulationsSupplier::createRegulationsSupplier( $supplierinvoice->get( "billing_supplier" ) , $supplierinvoice->get( "idsupplier" ));
		
		if($acompte)
			$regulationssupplier->set( "from_down_payment" , $acompte ? "1" : "0" );
		
			
		$regulationssupplier->set( "comment" , $comment	);
		$regulationssupplier->set( "payment_date" , $payment_date	);
		$regulationssupplier->set( "deliv_payment" , $deliv_payment	);
		$regulationssupplier->set( "idpayment" , 		$idpayment ); 							
		$regulationssupplier->set( "amount" , $amount );
		$regulationssupplier->set( "accept" , 		1 );
	
		//On sauvegarde
		$regulationssupplier->update();
		
		//On met à jour la table billing_regulations_supplier
		
		RegulationsSupplier::updateBillingRegulationsSupplier( $supplierinvoice->get( "billing_supplier" ) , $supplierinvoice->get( "idsupplier" ), $regulationssupplier->get("idregulations_supplier"), $amount);
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Met à jour le montant de la table billing_regulations_supplier
	 * @param string $billing_supplier le numéro de la facture fournisseur
	 * @param int $idsupplier le numéro de fournisseur
	 * @param int $idregulations_supplier le numéro du règlement
	 * @param float $amount le montant du règlement
	 */
	 
	public static function updateBillingRegulationsSupplier( $billing_supplier, $idsupplier, $idregulations_supplier, $amount ){
		
		
		$query = "UPDATE billing_regulations_supplier
				SET amount = '" . floatval( $amount ) . "'
				WHERE idregulations_supplier = $idregulations_supplier
				AND billing_supplier = '" . Util::html_escape( $billing_supplier ) . "'
				AND idsupplier = $idsupplier";

		DBUtil::query( $query );
		
	
	}
	
	//----------------------------------------------------------------------------
	
}
?>