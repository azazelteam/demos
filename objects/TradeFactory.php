<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object création devis à partir du panier
 */
 

include_once( dirname( __FILE__ ) . "/../config/init.php" );


abstract class TradeFactory{
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un devis à partir du panier du front office
	 * @static
	 * @access public
	 * @return Estimate le nouveau devis
	 */
	public static function createEstimateFromBasket(){
		
		include_once( dirname( __FILE__ ) . "/Basket.php" );
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
		
		$idbuyer 	= Session::getInstance()->getCustomer()->getId();
		$idcontact 	= Session::getInstance()->getCustomer()->getContact()->getId();
				
		$estimate = self::createEstimate( $idbuyer, $idcontact );
		
		$estimate->setSalesman( Basket::getInstance()->getSalesman() );

		$estimate->set( "idpayment", 			DBUtil::getParameter( "default_idpayment" ) );
		$estimate->set( "status", 				Estimate::$STATUS_TODO );
		$estimate->set( "total_discount" , 		Basket::getInstance()->getDiscountRate() );
		$estimate->set( "total_discount_amount",Basket::getInstance()->getDiscountAmount() );
		$estimate->set( "type_estimate" , 		1 );
		$estimate->set( "total_charge_auto" , 	1 );

		//ajout des articles
				
		$it = Basket::getInstance()->getItems()->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			
			$estimate->addArticle( $item->getArticle(), $item->getQuantity() );
			
		}
		
		//pas de frais de port si total à 0 ( pour la forme )
		
		if( $estimate->get( "total_amount_ht" ) == 0.0 ){
			
			$estimate->set( "charge_vat_rate", Basket::getInstance()->getVATRate() );
			$estimate->set( "charge_vat", 		0.0 );
			$estimate->set( "total_charge", 	0.0 );
			$estimate->set( "total_charge_ht", 0.0 );
			
		}
		
		/* délai de paiement : 'à la commande' forcé si total HT < montant paramétré 'auto_estimate' */

		$auto_estimate = DBUtil::getParameter( "auto_estimate" );
		
		if( $auto_estimate > 0.0 && $estimate->getTotalET() > 0.0 && $estimate->getTotalET() <= $auto_estimate )
				$estimate->set( "idpayment_delay", 1 );
		else 	$estimate->set( "idpayment_delay", DBUtil::getParameter( "default_idpayment_delay" ) );
		
		//assurance crédit
		
		$estimate->insure();
		
		//sauvegarde
		
		$estimate->save();
	
		//destruction du panier
		
		Basket::getInstance()->destroy();
		
		return $estimate;
			
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une commande à partir du panier du front office
	 * @static
	 * @access public
	 * @return Order la nouvelle commande
	 */
	public static function createOrderFromBasket(){
	
		
		include_once( dirname( __FILE__ ) . "/Basket.php" );
		include_once( dirname( __FILE__ ) . "/../objects/Basket.php" );
		$idbuyer 	= Session::getInstance()->getCustomer()->getId();
		$idcontact 	= Session::getInstance()->getCustomer()->getContact()->getId();
		//print $code;exit;
		$order = self::createOrder( $idbuyer, $idcontact );
		
		//	print 'ok1'; exit;
		$order->setSalesman( Basket::getInstance()->getSalesman() );

		$order->set( "idpayment", 				DBUtil::getParameter( "default_idpayment" ) );
		$order->set( "total_discount" , 		Basket::getInstance()->getDiscountRate() );
		$order->set( "total_discount_amount", 	Basket::getInstance()->getDiscountAmount() );
		$order->set( "idpayment_delay", 		DBUtil::getParameter( "default_idpayment_delay" ) );
		$order->set( "total_charge_auto" , 		1 );

		$reduce = 0.00;
		if( isset( $_SESSION[ "voucher" ] ) ){
			$code = urldecode($_SESSION[ "voucher" ]);
			if(Basket::getInstance()->setUseVoucher( $code )){
				$reduce = Basket::getInstance()->Getreduction( stripslashes( $code  ) );

				if( B2B_STRATEGY ){
					$reduceht = 	$reduce;
					$reducettc =  $reduce +($reduce * $order->getVATRate()) / 100;

				}else	{
					$reducettc =  $reduce;
					$reduceht = 	$reduce /1.196;
				}

				$net = Basket::getInstance()->getNetBill() - $reducettc ;
				$net_ht= $net - ( ($net * $order->getVATRate())/ 100);

				$articlesET = 0.0;
				$it = Basket::getInstance()->getItems()->iterator();
				
				while( $it->hasNext() )
					$articlesET += $it->next()->getTotalET();
					
				$chargesET 	= Basket::getInstance()->getChargesET();
				$chargesATI = Basket::getInstance()->getChargesATI();
				$netapayer_ht = ($articlesET + $chargesET ) - $reduceht ;

				if($net < 0.00){
					$net = 0.00;
				}
				$order->set( "voucher_code" , 		$code );

				$order->set( "reduction" , 			$reduce );
				$order->set( "amount_reduce" , 		$net );
				$order->set( "total_amount_ht" , 	$netapayer_ht );
				$order->set( "amount_reduce_ht" , 	$netapayer_ht );
				$order->set( "total_amount" , 		$net );

				//bon de réduction
				$order->addArticlevoucher( );
			}
		}
		else{
			$articlesET = 0.0;
			$articlesATI = 0.0;
			$it = Basket::getInstance()->getItems()->iterator();
			while( $it->hasNext() )
				$articlesATI += $it->next()->getTotalATI();
			$chargesATI = Basket::getInstance()->getChargesATI();

			$net = ($articlesATI + $chargesATI );
			$order->set( "total_amount" , 		$net );

		}

		//ajout des articles
		
		$it = Basket::getInstance()->getItems()->iterator();
		
		while( $it->hasNext() ){
			
			$item =& $it->next();
			
			$order->addArticle( $item->getArticle(), $item->getQuantity() );	
			
		}
			
		//pas de frais de port si total à 0 ( pour la forme )
		
		if( $order->get( "total_amount_ht" ) == 0.0 ){
			
			$order->set( "charge_vat_rate", 	Basket::getInstance()->getVATRate() );
			$order->set( "charge_vat", 		0.0 );
			$order->set( "total_charge", 	0.0 );
			$order->set( "total_charge_ht", 0.0 );
			
		}else{
			$order->set( "total_charge", 	Basket::getInstance()->getChargesATI() );
			$order->set( "total_charge_ht", Basket::getInstance()->getChargesET());
		}
	
		//assurance crédit
		
		$order->insure();
		
		//sauvegarde
		
		$order->save();
	
		//destruction du panier
		
		//Basket::getInstance()->destroy();
		
		return $order;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Convertit un devis en commande
	 * @static
	 * @access public
	 * @param Estimate $estimate le devis à commander
	 * @param string $status le status de la commande
	 * @return Order la commande créée
	 */
	public static function createOrderFromEstimate( Estimate &$estimate, $status ){
		
		global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
		include_once( dirname( __FILE__ ) . "/Order.php" );
		include_once( dirname( __FILE__ ) . "/Session.php" );
		
		$rs = DBUtil::query( "SELECT idorder FROM `order` WHERE idestimate = '" . $estimate->getId() . "' AND `status` NOT LIKE '" . Order::$STATUS_CANCELLED . "' LIMIT 1" );
		
		if( $rs->RecordCount() )
			return new Order( $rs->fields( "idorder" ) );
	
		$idbuyer 	= $estimate->get( "idbuyer" );
		$idcontact 	= $estimate->get( "idcontact" );
		
		$order = self::createOrder( $idbuyer, $idcontact );
		
		$order->setSalesman( $estimate->getSalesman() );
		
		//lignes articles
		
		$iterator = $estimate->getItems()->iterator();
		
		while( $iterator->hasNext() ){
			
			$estimateItem =& $iterator->next();
			
			$order->addEstimateItem( $estimateItem );
		
			//destockage
			//@todo stock dispo pas vérifié
			
			if( $status == Order::$STATUS_ORDERED ){
				
				$query = "
				UPDATE detail
				SET stock_level = GREATEST( 0, stock_level - " . $estimateItem->get( "quantity" ) . " )
				WHERE idarticle = '" . $estimateItem->get( "idarticle" ) . "'
				AND stock_level <> -1
				LIMIT 1";
				
				DBUtil::query( $query );

			}
			
			//enregistrement dans les stats
			
			$idarticle = $estimateItem->get( "idarticle" );
			$quantity = $estimateItem->get( "quantity" );
			$reference = $estimateItem->get( "reference" );
			
			//@todo : idcategory ne devrait pas être récupéré dans la BDD car la référence peut avoir été supprimée
			//mais si on stock le champ idcategory et que la catégorie a été supprimée c'est le même topo
			// => Il y a une erreur de conception quelque part
				
			$rs =& DBUtil::query( "SELECT p.idcategory FROM product p, detail d WHERE d.idproduct = p.idproduct AND d.idarticle = '$idarticle'" );
			
			$idcategory = $rs->fields( "idcategory" );
						
		}
     	
		//fichiers joints
      	
        self::estimateAttachmentToOrderAttachment( $estimate, $order );
        
		//assurance crédit
		
		$order->insure( $estimate->get( "insurance" ) == 1 );
		
        //propriétés de la commande
            
		$order->set( "idestimate", 				$estimate->getId() );
		$order->set( "estimate_date", 			$estimate->get( "DateHeure" ) );
		$order->set( "iddelivery", 				$estimate->get( "iddelivery" ) );
		$order->set( "idbilling_adress", 		$estimate->get( "idbilling_adress" ) );
		$order->set( "total_amount", 			$estimate->get( "total_amount" ) );
		$order->set( "total_amount_ht", 		$estimate->get( "total_amount_ht" ) );
		$order->set( "total_charge", 			$estimate->get( "total_charge" ) );
		$order->set( "total_charge_ht", 		$estimate->get( "total_charge_ht" ) );
		$order->set( "ecotaxe_code", 			$estimate->get( "ecotaxe_code" ) );
		$order->set( "ecotaxe_amount", 			$estimate->get( "ecotaxe_amount" ) );
		$order->set( "charge_free", 			$estimate->get( "charge_free" ) );
		$order->set( "idpayment", 				$estimate->get( "idpayment" ) );
		$order->set( "idpayment_delay", 		$estimate->get( "idpayment_delay" ) );
		$order->set( "title_offer", 			$estimate->get( "title_offer" ) );
		$order->set( "status", 					$status );
		$order->set( "comment", 				$estimate->get( "comment" ) );
		$order->set( "commercial_comment",		$estimate->get( "commercial_comment" ) );
		$order->set( "no_tax_export", 			$estimate->get( "no_tax_export" ) );
		$order->set( "charge_vat_rate", 		$estimate->get( "charge_vat_rate" ) );
		$order->set( "charge_vat", 				$estimate->get( "charge_vat" ) );
		$order->set( "billing_amount", 			$estimate->get( "billing_amount" ) );
		$order->set( "billing_rate", 			$estimate->get( "billing_rate" ) );
		$order->set( "charge_supplier", 		$estimate->get( "charge_supplier" ) );
		$order->set( "charge_supplier_rate", 	$estimate->get( "charge_supplier_rate" ) );
		$order->set( "total_discount", 			$estimate->get( "total_discount" ) );
		$order->set( "total_discount_amount", 	$estimate->get( "total_discount_amount" ) );
		$order->set( "charge_supplier_auto", 	$estimate->get( "charge_supplier_auto" ) );
		$order->set( "conf_order_date", 		$status == Order::$STATUS_ORDERED ? date( "Y-m-d H:i:s" ) : "0000-00-00 00:00:00" );
		//On met total_charge_auto à 0 car en cas de changement de port par défaut les calculs sont faux
		$order->set( "total_charge_auto", 		0 );
		$order->set( "n_order", 				$estimate->get( "n_order" ) );
		$order->set( "remote_creation_date", 	$estimate->get( "remote_creation_date" ) );
		$order->set( "pro_forma", 				0 );
		$order->set( "global_delay", 			$estimate->get( "global_delay" ) );
        $order->set( "balance_date", 			$estimate->get( "balance_date" ) );
        $order->set( "paid",		 			$estimate->get( "paid" ) );
		$order->set( "rebate_type", 			$estimate->get( "rebate_type" ) ); 
        $order->set( "rebate_value", 			$estimate->get( "rebate_value" ) );
        $order->set( "credit_amount", 			$estimate->get( "credit_amount" ) );
        
        // Acompte
		$order->set( "down_payment_type",		$estimate->get( "down_payment_type" ) );
		$order->set( "down_payment_value", 		$estimate->get( "down_payment_value" ) );
		$order->set( "down_payment_idpayment", $estimate->get( "down_payment_idpayment" ) );
		$order->set( "down_payment_comment", 	$estimate->get( "down_payment_comment" ) );
		$order->set( "down_payment_date", 		$estimate->get( "down_payment_date" ) );
		$order->set( "down_payment_idregulation",$estimate->get( "down_payment_idregulation" ) );
		$order->set( "down_payment_idbank", 	$estimate->get( "down_payment_idbank" ) );
		$order->set( "down_payment_piece", 		$estimate->get( "down_payment_piece" ) );
		
		// Paiement comptant
        $order->set( "balance_amount", 			$estimate->get( "balance_amount" ) );
        $order->set( "cash_payment", 			$estimate->get( "cash_payment" ) );
        $order->set( "payment_comment", 		$estimate->get( "payment_comment" ) ); 
        $order->set( "payment_idpayment", 		$estimate->get( "payment_idpayment" ) ); 
        $order->set( "cash_payment_idregulation",$estimate->get( "cash_payment_idregulation" ) );
        $order->set( "cash_payment_idbank", 	$estimate->get( "cash_payment_idbank" ) );
        $order->set( "cash_payment_piece", 		$estimate->get( "cash_payment_piece" ) );
        
        //Assurance crédit
        //Ca s'appelle factor mais c'est l'assurance crédit. Et oui, c'est beau !
        //On ne copie pas depuis le devis étant donné que c'est info est rarement renseignée dans le devis et est parfois fausse
        //Elle est par exemple fausse quand le client n'est passé à l'assurance qu'une fois le devis créé
        
		$total_amount		= $estimate->get( "total_amount" );
		$buyerFactor		= $estimate->getCustomer()->get( "factor" );
		$buyerFactorStatus	= $estimate->getCustomer()->get( "factor_status" );
		$factor_minbuying	= DBUtil::getParameterAdmin( "factor_minbuying" );
		$factor_maxbuying	= DBUtil::getParameterAdmin( "factor_maxbuying" );
		
		$allowFactor = true;
		$allowFactor &= !empty( $buyerFactor ); 												//à condition qu'il y ait un factor
		$allowFactor &= ( $buyerFactorStatus == "Accepted" ); 									//à condition que le factor soit accepté
		$allowFactor &= ( empty( $factor_minbuying ) || $total_amount >= $factor_minbuying ); 	//à condition que le montant soit au dessus du montant minimum autorisé si ce dernier existe
		$allowFactor &= ( empty( $factor_maxbuying ) || $total_amount <= $factor_maxbuying ); 	//à condition que le montant soit en dessous du montant maximum autorisé si ce dernier existe
		
        if( $allowFactor )
        		$order->set( "factor", $estimate->getCustomer()->get( "factor" ) );
        else 	$order->set( "factor", 0 );

        //date de paiement
        
        $delaipaye = $estimate->getCustomer()->get( "deliv_payment" ) * 3600 * 24; // delai en secondes 

		if( $estimate->getCustomer()->get( "cond_payment" ) == '1' ) //@todo : comprends pas... => ne pas se baser sur la valeur d'une clé primaire pour faire un algo!!! 
				$echeancepaye = $delaipaye + mktime(0,0,0,date('m')+1,1,date('Y')) - 2 ; // delai + debut du mois suivant à 0h - 2 secondes
		else 	$echeancepaye  = time() + $delaipaye ; //date de paiement à le seconde près!
		
		$ADatePaye =getdate($echeancepaye);	
		$DatePaye = sprintf("%04d-%02d-%02d",$ADatePaye['year'], $ADatePaye['mon'], $ADatePaye['mday']) ;
		
        $order->set( "deliv_payment", $DatePaye );
        
        $order->insure();
        //sauvegarde
		
		$order->save();
		
		//frais de port fournisseur
		
		self::estimateChargesToOrderCharges( $estimate->getId(), $order->getId() );

		//On met à jour le devis
		
		$estimate->set( "status", Estimate::$STATUS_ORDERED );
		$estimate->set( "total_charge_auto" , 0 );
		
		$estimate->save();

	 	return $order;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé les commandes fournisseur pour une commande donnée
	 * @static
	 * @access public
	 * @param Order $order la commande
	 * @return void
	 */
	public static function createSupplierOrders( Order $order ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		if( DBUtil::query( "SELECT idorder_supplier FROM order_supplier WHERE idorder = '" . $order->getId() . "' LIMIT 1" )->RecordCount() )
			return;
		
	    $useInternalSupplier 	= false;
	    $useExternalSuppliers 	= false;
	    
	    $it = $order->getItems()->iterator();
	    while( $it->hasNext() ){
	    
	    	$orderItem = $it->next();
	    	
	    	$useInternalSupplier |= $orderItem->get( "quantity" ) > $orderItem->get( "external_quantity" );
	    	$useExternalSuppliers |= $orderItem->get( "external_quantity" ) > 0;
	    	
	    }
	    
	    //créer la commande fournisseur interne
	    
	    if( $useInternalSupplier )
			self::createInternalSupplierOrder( $order );
		
		//créer les commandes fournisseur externes
			
		if( $useExternalSuppliers )
			self::createExternalSupplierOrders( $order );
		
		//créer un bon de livraison pour chaque petite commande fournisseur générée
		
		$query = "
		SELECT idorder_supplier
		FROM order_supplier
		WHERE idorder = '" . $order->getId() . "'";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){
			
			$idorder_supplier = $rs->fields( "idorder_supplier" );
			
			self::createDeliveryNoteFromSupplierOrder( new SupplierOrder( $idorder_supplier ) );
		
			$rs->MoveNext();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créer la commande fournisseur interne pour une commande donnée
	 * @access private
	 * @static
	 * @param Order $order la commande
	 * @return void
	 */
	private static function createInternalSupplierOrder( Order $order ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		$internalSupplierId = DBUtil::getParameterAdmin( "internal_supplier" );
		
		if( !$internalSupplierId ){
			
			trigger_error( "Impossible de trouver le fournisseur interne", E_USER_ERROR );
			exit();
			
		}
		
		$supplierOrder = self::createSupplierOrderFromOrder( $order, $internalSupplierId );
			
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
				
			//ne traiter que le stock interne
			
			if( $orderItem->get( "quantity") > $orderItem->get( "external_quantity") ){
				
				$internal_quantity 	= $orderItem->get( "quantity") - $orderItem->get( "external_quantity");
	
				$supplierOrder->addOrderItem( $orderItem );
				
			}

		}

		//propriétés de la commande fournisseur
		
		$supplierOrder->set( "iddelivery", 			$order->get( "iddelivery" ) );
		$supplierOrder->set( "no_tax_export", 		$order->get( "no_tax_export" ) );
		$supplierOrder->set( "charge", 				$order->get( "charge_supplier" ) );
		$supplierOrder->set( "charge_rate", 		$order->get( "charge_supplier_rate" ) );
		$supplierOrder->set( "total_charge_ht", 	$order->get( "total_charge_ht" ) );
		$supplierOrder->set( "total_charge", 		$order->get( "total_charge" ) );
		$supplierOrder->set( "charge_vat", 			$order->get( "charge_vat" ) );
		$supplierOrder->set( "charge_vat_rate", 	$order->get( "charge_vat_rate" ) );
		$supplierOrder->set( "stock", 				SupplierOrder::$STOCK_TYPE_INTERNAL );
		$supplierOrder->set( "idpayment", 			DBUtil::getDBValue( "idpaiement", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) ) ); 		//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->set( "idpayment_delay", 	DBUtil::getDBValue( "pay_delay", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) ) ); 	//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->set( "charge_carrier", 		0.0 );
		$supplierOrder->set( "charge_carrier_cost", 0.0 );
		$supplierOrder->set( "charge_carrier_sell", 0.0 );
		$supplierOrder->set( "delivery_mail_auto",	1 );
		
		//sauvegarde
		
		$supplierOrder->save();

	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé la commande fournisseur externe pour une commande donnée et un fournisseur donné
	 * @param Order la commande
	 * @param int $idsupplier l'identifiant du fournisseur
	 * @static
	 * @access private
	 * @return void
	 */
	 private static function createExternalSupplierOrder( Order $order, $idsupplier ){
	 	
	 	include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
	 	
		$supplierOrder = self::createSupplierOrderFromOrder( $order, $idsupplier );
				
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem = $it->next();
				
			//ne traiter que le stock externe
			
			if( $orderItem->get( "idsupplier" ) == $idsupplier && $orderItem->get( "external_quantity") ){
				
				$supplierOrder->addOrderItem( $orderItem );
				
			}

		}
		
		//propriétés de la commande fournisseur
		
		$supplierOrder->set( "iddelivery", 		$order->get( "iddelivery" ) );
		$supplierOrder->set( "stock", 			SupplierOrder::$STOCK_TYPE_EXTERNAL );
		$supplierOrder->set( "idpayment", 		DBUtil::getDBValue( "idpaiement", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) ) ); 		//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->set( "idpayment_delay", DBUtil::getDBValue( "pay_delay", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) ) ); 	//@todo english or français, you have to choisir s'il vous please!
		$supplierOrder->set( "billing_rate", 	DBUtil::getDBValue( "billing_rate", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) ) );
		$supplierOrder->set( "billing_amount", 	round( $supplierOrder->getTotalET() * $supplierOrder->get( "billing_rate" ) / 100.0, 2 ) );
		
		//livraison
		
		$default_deliv = DBUtil::getDBValue( "default_deliv", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) );
		$iddelivery = $default_deliv ? $default_deliv : $order->get( "iddelivery" );
		
		$supplierOrder->set( "iddelivery", 		$iddelivery );
		
		//frais de port
		
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) );
		$idstate = DBUtil::getDBValue( "idstate", "supplier", "idsupplier", $supplierOrder->get( "idsupplier" ) );
		$bvat	 = DBUtil::getDBValue( "bvat", "state", "idstate", $idstate );
		$rs 	 = DBUtil::query( "SELECT * FROM `order_charges` WHERE idorder = '" . $order->getId() . "' AND idsupplier = '" . $supplierOrder->get( "idsupplier" ) . "' LIMIT 1" );

		//champs de compta navision
		
		$charge_carrier 		= $transit ? $rs->fields( "charges" ) + $rs->fields( "internal_supplier_charges" ) : 0.0;
		$charge_carrier_cost 	= $transit ? $rs->fields( "charges" ) : 0.0;
		$charge_carrier_sell 	= $transit ? $rs->fields("internal_supplier_charges") : 0.0;

		$total_charge_ht = $rs->fields( "charges" );
		$total_charge 	 = $bvat ? $rs->fields( "charges" ) * ( 1 + $order->get( "charge_vat_rate" ) / 100.0 ) : $rs->fields( "charges" );
		$charge 		 = $bvat ? $rs->fields( "charges" ) * ( 1 + $order->get( "charge_vat_rate" ) / 100.0 ) : $rs->fields( "charges" );
		$charge_rate 	 = $order->getNetBill() > 0.0 ? $charge * 100.0 / $order->getNetBill() : 0.0;

		$supplierOrder->set( "charge_vat_rate", 	$order->get( "charge_vat_rate" ) );
		$supplierOrder->set( "charge_vat", 			$order->get( "charge_vat" ) );
		$supplierOrder->set( "charge", 				$charge );
		$supplierOrder->set( "charge_rate", 		$charge_rate );
		$supplierOrder->set( "charge_carrier", 		$charge_carrier );
		$supplierOrder->set( "total_charge_ht", 	$total_charge_ht );
		$supplierOrder->set( "total_charge", 		$total_charge );
		$supplierOrder->set( "charge_carrier_cost", $charge_carrier_cost );
		$supplierOrder->set( "charge_carrier_sell", $charge_carrier_sell );
		
		//sauvegarde

		$supplierOrder->save();

	 }
	 
	//----------------------------------------------------------------------------
	/**
	 * Créé les commandes fournisseur externes pour une commande donnée
	 * @param Order la commande
	 * @static
	 * @access private
	 * @return void
	 */
	private static function createExternalSupplierOrders( Order $order ){
	
		//fournisseurs distincts
		
		$internalSupplierId = DBUtil::getParameterAdmin( "internal_supplier" );
		$externalSupplierIds = array();
		
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			if( $orderItem->get( "idsupplier" ) != $internalSupplierId && $orderItem->get( "external_quantity" ) > 0 )
				array_push( $externalSupplierIds, $orderItem->get( "idsupplier" ) );

		}
		
		$externalSupplierIds = array_unique( $externalSupplierIds );
		
		if( !count( $externalSupplierIds ) )
			return;

		//créer les commandes fournisseur
		
		foreach( $externalSupplierIds as $idsupplier )
			self::createExternalSupplierOrder( $order, $idsupplier );
  
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un bon de livraison pour une commande fournisseur donnée
	 * @static
	 * @access public
	 * @todo objectiser la création
	 * @param SupplierOrder $supplierOrder la commande fournisseur
	 * @return DeliveryNote un bon de livraison
	 */
	
	public static function createDeliveryNoteFromSupplierOrder( SupplierOrder &$supplierOrder ){
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		/*if( DBUtil::query( "SELECT idbl_delivery FROM `bl_delivery` WHERE idorder_supplier = '" . $supplierOrder->getId() . "' LIMIT 1" )->RecordCount() )
			return;*/
			
		//récupérer un nouveau numéro de BL disponible

	$table = 'bl_delivery';
		$idbl_delivery = self::Indexations($table);

		
		if( $idbl_delivery == null )
			$idbl_delivery = 1;
			
		
	 	//création

	 	$query = "
		INSERT INTO `bl_delivery` ( 
			idbl_delivery,
			idorder,
			idorder_supplier,
			iduser,
			idbuyer,
			idcontact,
			idsupplier,
			DateHeure,
			date_send,
			availability_date,
			dispatch_date,
			iddelivery,
			username,
			lastupdate
		) VALUES ( 
			'$idbl_delivery',
			'" . $supplierOrder->get( "idorder" ) . "',
			'" . $supplierOrder->getId() . "',
			'" . User::getInstance()->getId() . "',
			'" . $supplierOrder->get( "idbuyer" ) . "',
			'" . $supplierOrder->get( "idcontact" ) . "',
			'" . $supplierOrder->get( "idsupplier" ) . "',
			NOW(),
			'0000-00-00',
			'" . $supplierOrder->get( "availability_date" ) . "',
			'" . $supplierOrder->get( "dispatch_date" ) . "',
			'" . $supplierOrder->get( "iddelivery" ) . "',
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			NOW()

		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
				
			trigger_error( "Impossible de créer le nouveau bon de livraison", E_USER_ERROR );
			exit();
					
		}
	
		//lignes article
		
		$iterator = $supplierOrder->getItems()->iterator();
		
		while( $iterator->hasNext() ){
			
			$supplierOrderItem =& $iterator->next();	
		
			$designation = $supplierOrderItem->get( "designation" );
			$extraFields = array( "site", "span", "store" );
			
			$rs =& DBUtil::query( "SELECT `" . implode( "`,`", $extraFields ) . "` FROM detail WHERE idarticle = '" . $supplierOrderItem->get( "idarticle" ) . "' LIMIT 1" );

			foreach( $extraFields as $extraField ){
				
				if( strlen( $rs->fields( $extraField ) ) )
					$designation .= "<br /><b>" . Dictionnary::translate( $extraField ) . "</b> : " . $rs->fields( $extraField );
			
			}
		
			$data = array( 
			
				"idbl_delivery" 	=> $idbl_delivery,
				"idrow" 			=> $supplierOrderItem->get( "idrow" ),
				"idorder_row" 		=> $supplierOrderItem->get( "idorder_row" ),
				"idarticle" 		=> $supplierOrderItem->get( "idarticle" ),
				"reference" 		=> $supplierOrderItem->get( "reference" ),
				"ref_supplier" 		=> $supplierOrderItem->get( "ref_supplier" ),
				"quantity" 			=> $supplierOrderItem->get( "quantity" ),
				"unit" 				=> $supplierOrderItem->get( "unit" ),
				"lot" 				=> $supplierOrderItem->get( "lot" ),
				"designation" 		=> $designation,
				"summary" 			=> $supplierOrderItem->get( "summary" ),
				"width" 			=> $supplierOrderItem->get( "width" ),
				"length" 			=> $supplierOrderItem->get( "length" ),
				"area" 				=> $supplierOrderItem->get( "area" ),
				"comment" 			=> $supplierOrderItem->get( "comment" ),
				"availability_date" => $supplierOrderItem->get( "availability_date" ),
				"useimg" 			=> $supplierOrderItem->get( "useimg" )
	
			);
			
			foreach( $data as $field => $value )
				$data[ $field ] = DBUtil::quote( $value );
				
			$query = "
			INSERT INTO bl_delivery_row( `" . implode( "`,`", array_keys( $data ) ) . "` )
			VALUES( " . implode( ",", array_values( $data ) ) . " )";

			$ret =& DBUtil::query( $query );
			
			if( $ret === false ){
				
				trigger_error( "Impossible de créer les lignes article du BL n°$idbl_delivery", E_USER_ERROR );
				exit();
					
			}
			
		}

		/* transport */
		
		$rs = DBUtil::query( "
	 	SELECT oc.idcarrier
	 	FROM order_supplier os, order_charges oc
	 	WHERE os.idorder_supplier = '". $supplierOrder->getId() . "'
	 	AND oc.idorder = os.idorder
	 	AND oc.idsupplier = os.idsupplier
	 	LIMIT 1" );
		
		$idcarrier = $rs->RecordCount() ? $rs->fields( "idcarrier" ) : 0;
		
	 	$query = "
	 	INSERT INTO carriage( idcarriage, idsupplier_factory, iddelivery, idcarrier )
	 	VALUES( '$idbl_delivery', '0', '" . $supplierOrder->get( "iddelivery" ) . "', '$idcarrier' )";
	 	
	 	DBUtil::query( $query );

		return new DeliveryNote( $idbl_delivery );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Met à jour les BL d'une commande fournisseur
	 * @static
	 * @access public
	 * @param SupplierOrder $supplierOrder la commande fournisseur
	 * @return void
	 */
	public static function synchronizeDeliveryNoteFromSupplierOrder( SupplierOrder &$supplierOrder ){
		
		include_once( dirname( __FILE__ ) . "/DeliveryNote.php" );
		
		if( !$supplierOrder->get( "internal_supply" ) )
			return;
			
		$idorder_supplier 	= $supplierOrder->get( "idorder_supplier" );
	 	$idbl_delivery 		= DBUtil::getDBValue( "idbl_delivery", "bl_delivery", "idorder_supplier", $idorder_supplier );
		$deliveryNote 		= new DeliveryNote( $idbl_delivery );
		
		$deliveryNote->removeItems();
		$deliveryNote->save();
		
		//données à synchroniser dans les lignes article
		
		$supplierOrderIterator = $supplierOrder->getItems()->iterator();
		
		while( $supplierOrderIterator->hasNext() ){
			
			$supplierOrderItem =& $supplierOrderIterator->next();	

			$deliveryNote->addSupplierOrderItem( $supplierOrderItem );

		}
		
		//données à synchroniser dans la table principale
		
		$deliveryNote->set( "availability_date", 	$supplierOrder->get( "availability_date" ) );
		$deliveryNote->set( "dispatch_date", 		$supplierOrder->get( "dispatch_date" ) );
		$deliveryNote->set( "iddelivery", 			$supplierOrder->get( "iddelivery" ) );
		
		$deliveryNote->save();
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande vierge pour un client donné
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Order la nouvelle commande
	 */
public static function createOrder( $idbuyer, $idcontact ){
		include_once( dirname( __FILE__ ) . "/Order.php" );
        
        $iduser 	= User::getInstance()->getId() ? User::getInstance()->getId() : DBUtil::getParameter( "contact_commercial" );
        //62. Gestion commercial unique pour la version opn source
         //$contact_commercial_file = dirname( __FILE__ ) . "../akilae_pro/commercial.php";
         
         //Si fichier présent -> gestion multi-commerciaux, 
         // si fichier absent -> commercial = contact_commercial
         /*if( ! file_exists($contact_commercial_file)) $iduser = DBUtil::getParameter( "contact_commercial" );
         else include($contact_commercial_file);*/
	 
	
		 
	  $table = 'order'; 
	 $idorder = self::Indexations($table); //time(); rova: risque de doublons. si géré en base, causera perte de donné
	 $idSource = 0;

	 if(isset($_SESSION['utm_source'])){
	 	$utm_source =$_SESSION["utm_source"];
	 	$rs = DBUtil::query( "SELECT idsource FROM `source` where source_code = '$utm_source'" );

	 	$idSource = ($rs->fields("idsource")!=null)?$rs->fields("idsource"):0;
	 }
	 
	 
	 	$query = "
		INSERT INTO `order` ( idorder,
			iduser, 
			idbuyer, 
			idcontact,
			DateHeure,
			idsource 
			
		) VALUES ( 
		'$idorder',
			'$iduser', 
			'$idbuyer', 
			'$idcontact',
			NOW(),".
			$idSource."
			
		)";
	
	if(DBUtil::query( "SELECT idorder FROM `order` where idorder = '$idorder'" )->RecordCount() == 0){
		$ret = DBUtil::query( $query );
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande", E_USER_ERROR );
			exit();
			
		}
	}
	 
	 
	 
	 

		$order = new Order( $idorder );
	//	print $idorder.'ok2';exit;
		//assigner le client à la commande
		
		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
		$order->setCustomer( new Customer( $idbuyer, $idcontact ) );
	 	
	 	$order->set( "DateHeure", 			date( "Y-m-d H:i:s" ) );
	 	$order->set( "idpayment", 			DBUtil::getParameter( "default_idpayment" ) );
	 	$order->set( "idpayment_delay", 	DBUtil::getParameter( "default_idpayment_delay" ) );
		$order->set( "paid",				0 );
		$order->set( "status",				Order::$STATUS_TODO );
		$order->set( "rebate_type", 		"rate" );
		$order->set( "rebate_value", 		max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $idbuyer ) ) );
		
		$order->insure();
		
	 	$order->save();
	 	
	 	//les prospect deviennent des clients
	 	
	 	DBUtil::query( "UPDATE buyer SET contact = 0 WHERE idbuyer ='" . $order->get( "idbuyer" ) . "' AND contact = 1 LIMIT 1" );
	 	
	 	return $order;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande fournisseur vierge pour une commande donnée
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param Order $order la commande
	 * @return SupplierOrder la nouvelle commande
	 */
	private static function createSupplierOrderFromOrder( Order $order, $idsupplier ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		

			// ------- Mise à jour de l'id gestion des Index  #1161
		$table = 'order_supplier';
		$idorder_supplier = self::Indexations($table);
	
		
		if( $idorder_supplier == null )
			$idorder_supplier = 1;
		
	
	 	//réserver le numero de commande fournisseur
	 	
	 	$query = "
		INSERT INTO `order_supplier` ( 
			idorder_supplier, 
			idsupplier,
			idorder,
			iduser,
			idbuyer,
			idcontact
		) VALUES ( 
			'$idorder_supplier', 
			'$idsupplier',
			'" . $order->getId() . "',
			'" . User::getInstance()->getId() . "',
			'" . $order->getCustomer()->getId() . "',
			'" . $order->getCustomer()->getContact()->getId() . "'
		)";
		
		$ret = DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande fournisseur", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$supplierOrder = new SupplierOrder( $idorder_supplier );

	 	$supplierOrder->set( "DateHeure", 			date( "Y-m-d H:i:s" ) );
		$supplierOrder->set( "status",				SupplierOrder::$STATUS_STANDBY );
		$supplierOrder->set( "delivery_mail_auto", 	1 );
		$supplierOrder->set( "idpayment", 			$supplierOrder->getSupplier()->get( "idpaiement" ) );
		$supplierOrder->set( "idpayment_delay", 	$supplierOrder->getSupplier()->get( "pay_delay" ) );
		
	 	$supplierOrder->save();
	 	
	 	return $supplierOrder;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un nouveau devis vierge pour un client donné
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact ( optionnel, 0 par défaut )
     * @param int $idcopy le numéro du dévis source pour copie( optionnel, 0 par défaut )
	 * @return Estimate un nouveau devis
	 */
	/*public static function createEstimate( $idbuyer, $idcontact = 0, $idracine = 0 ){
		
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
	
    	
        //$table = 'estimate';
        //$idestimate = self::Indexations($table);
		
         $table = 'estimate';
          $idestimate = self::Indexations($table,$idracine );
		var_dump($idestimate);
       
	
		
	   $iduser 	= User::getInstance()->getId() ? User::getInstance()->getId() : DBUtil::getParameter( "contact_commercial" );
	   /*
        //62. Gestion commercial unique pour la version opn source
         //$contact_commercial_file = dirname( __FILE__ ) . "../akilae_pro/commercial.php";
         
         //Si fichier présent -> gestion multi-commerciaux, 
         // si fichier absent -> commercial = contact_commercial
         /*if( ! file_exists($contact_commercial_file)) $iduser = DBUtil::getParameter( "contact_commercial" );
         else include($contact_commercial_file); */
	 	
		
		
		
		
		public static function createEstimate( $idbuyer, $idcontact = 0, $idestimateSource = '' ){
		
		include_once( dirname( __FILE__ ) . "/Estimate.php" );
	
    	if( $idestimateSource == ''){
          $table = 'estimate';
          $idestimate = self::Indexations($table);
		}
        else{
            
           $indice = 0;
           $idsource  = explode('-',$idestimateSource)[0];
           $query = " SELECT idestimate FROM estimate WHERE idestimate LIKE  ".DBUtil::quote($idsource.'-%') ;  
		   	
            $rs =& DBUtil::query( $query);

    		while( !$rs->EOF() ){
            
           
                $subIndex =  intval(explode('-',$rs->fields('idestimate'))[1]);
                $indice = $subIndex >= $indice ? $subIndex : $indice;
             	$rs->MoveNext();
            }
            
            $idestimate = $idsource.'-'.($indice+1);
        }
		
		 	$iduser 	= User::getInstance()->getId() ? User::getInstance()->getId() : DBUtil::getParameter( "contact_commercial" );
		
	 	$query = "
		INSERT INTO `estimate` ( 
			idestimate, 
			iduser, 
			idbuyer, 
			idcontact,
			DateHeure
		) VALUES ( 
			'$idestimate', 
			'$iduser', 
			'$idbuyer', 
			'$idcontact',
			NOW()
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande", E_USER_ERROR );
			exit();
			
		}
		
		//créer la commande
		
		$estimate = new Estimate( $idestimate );
		
		//assigner le client à la commande
		
		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
		$estimate->setCustomer( new Customer( $idbuyer, $idcontact ) );
	 	
	 	$estimate->set( "DateHeure", 		date( "Y-m-d H:i:s" ) );
	 	$estimate->set( "idpayment", 		DBUtil::getParameter( "default_idpayment" ) );
	 	$estimate->set( "idpayment_delay", DBUtil::getParameter( "default_idpayment_delay" ) );
		$estimate->set( "status", 			Estimate::$STATUS_IN_PROGRESS );
		$estimate->set( "rebate_type", 	"rate" );
		$estimate->set( "rebate_value", 	max( DBUtil::getParameterAdmin( "rebate_rate" ), DBUtil::getDBValue( "rebate_rate", "buyer", "idbuyer", $idbuyer ) ) );
		
	 	$estimate->save();
	 	
	 	return $estimate;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle facture pour un client et une commande donnés
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param int $idorder le numéro de commande
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Order la nouvelle commande
	 */
	private static function createInvoice( $idorder, $idbuyer, $idcontact ){
		
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		

		// ------- Mise à jour de l'id gestion des Index  #1161		
		$table = 'billing_buyer';
		$idbilling_buyer = self::Indexations($table);
	
		if( $idbilling_buyer == null )
			$idbilling_buyer = 1;
		

$date = date("Y-m-d H:i:s");
	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `billing_buyer` ( 
			idbilling_buyer,
			idorder, 
			iduser, 
			idbuyer, 
			idcontact,
			DateHeure,
			`status`
		) VALUES ( 
			'$idbilling_buyer',
			'$idorder', 
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact',
			'$date',
			'" . Invoice::$STATUS_INVOICED . "'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle facture", E_USER_ERROR );
			exit();
			
		}
		
		//créer la facture
		
		$invoice = new Invoice( $idbilling_buyer );

	 	return $invoice;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un nouvel avoir vierge
	 * En attendant l'implémentation de la surcharge pour les constructeur en PHP...
	 * @static
	 * @access private
	 * @param int $idbilling_buyer le numéro de facture à partir duquel on créé l'avoir
	 * @param int $idbuyer le numéro du client
	 * @param int $idcontact le numéro du contact
	 * @return Credit un nouveau lavoir
	 */
	private static function createCredit( $idbilling_buyer, $idbuyer, $idcontact ){
		

$table = 'credits';
		$idcredit = self::Indexations($table);
		// ------- Mise à jour de l'id gestion des Index  #1161	
		if( $idcredit == null )
			$idcredit = 1;


	 	//réserver le numero de commande
	 	
	 	$query = "
		INSERT INTO `credits` ( 
			idcredit,
			idbilling_buyer,
			iduser,
			idbuyer,
			idcontact,
			DateHeure
		) VALUES ( 
			'$idcredit',
			'$idbilling_buyer',
			'" . User::getInstance()->getId() . "', 
			'$idbuyer', 
			'$idcontact',
			NOW()
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer le nouvel avoir", E_USER_ERROR );
			exit();
			
		}
		
		//créer le zavoir
		
		$credit = new Credit( $idcredit );

	 	return $credit;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Transfert les frais de port fournisseur du devis à la commande
	 * @access private
	 * @param int $idestimate le numéro du devis
	 * @param int $idorder le numéro de la commande
	 * @return void
	 */
	private static function estimateChargesToOrderCharges( $idestimate, $idorder ){
		
		assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
	
		DBUtil::query( "DELETE FROM order_charges WHERE idorder = '$idorder'" );
		
		$rs =& DBUtil::query( "SELECT * FROM estimate_charges WHERE idestimate = '$idestimate'" );
	
		while( !$rs->EOF() ){

			$query = "
			INSERT INTO order_charges(
				idorder,
				idsupplier,
				charges,
				internal_supplier_charges,
				idcarrier,
				charges_auto,
				internal_supplier_charges_auto
			) VALUES (
				'$idorder',
				'" . $rs->fields( "idsupplier" ) . "',
				'" . $rs->fields( "charges" ) . "',
				'" . $rs->fields( "internal_supplier_charges" ) . "',
				'" . $rs->fields( "idcarrier" ) . "',
				'" . $rs->fields( "charges_auto" ) . "',
				'" . $rs->fields( "internal_supplier_charges_auto" ) . "'
			)";
			
			DBUtil::query( $query );
	
			$rs->MoveNext();
			
		}
		
	}

	/*************************************************  fin : gestion des indexations  ****************************************************************/
	/***********************************************************************************************************************************************/
	//----------------------------------------------------------------------------
	/**
	 * Transfert des pièces jointe du devis à la commande
	 * @access private
	 * @param int $estimate
	 * @param int $order
	 * @return void
	 */
	private static function estimateAttachmentToOrderAttachment( Estimate &$estimate, Order &$order ){

		global $GLOBAL_START_PATH;

		$attachments = array( 
		
			"estimate_doc" 		=> "order_doc", 
			"offer_signed" 		=> "offer_signed", 
			"supplier_offer" 	=> "supplier_offer",
			"supplier_doc" 		=> "supplier_doc" 
		
		);
		
		foreach( $attachments as $estimateAttachment => $orderAttachment ){
			
			if( strlen( $estimate->get( $estimateAttachment ) ) ){
				
				if( !file_exists( "$GLOBAL_START_PATH/data/order/" . date( "Y" ) ) ){
					
					@mkdir( "$GLOBAL_START_PATH/data/order/" . date( "Y" ) );
					@chmod( "$GLOBAL_START_PATH/data/order/" . date( "Y" ), 0755 );
					
				}
				
				$filename 	= substr( $estimate->get( $estimateAttachment ), strrpos( $estimate->get( $estimateAttachment ), "/" ) + 1 );
				$source 	= "$GLOBAL_START_PATH/data/estimate/" . $estimate->get( $estimateAttachment );
				$dest 		= "$GLOBAL_START_PATH/data/order/" . date( "Y" ) . "/$filename";
				
				 //if( @copy( $source, $dest ) === false || @chmod( $dest, 0755 ) === false )
	        		//trigger_error( "Echec lors de la copie de la pièce jointe lors de la confirmation du devis n°" . $estimate->getId(), E_USER_ERROR );
	     	
		      	$order->set( $orderAttachment, date( "Y" ) . "/$filename" );
		      	
			}

		}
		// ------- Mise à jour de l'id gestion des Index  #1161
		//documents
		$docs = &DBUtil::query( "SELECT * FROM estimate_row_product_document WHERE idestimate = '" . $estimate->getId()."'" );
		if( $docs->RecordCount() ){
			while( !$docs->EOF() ){
				
				DBUtil::query( "INSERT INTO order_row_product_document VALUES( " . $order->getId() . " , " . $docs->fields( 'unique_id' ) . " )" );			
				
				$docs->MoveNext();
			}
		}		
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture à partir de tous les BLs qui n'ont pas été facturés pour une commande donnée
	 * @static
	 * @param Order $order la commande à facturer
	 * @return void
	 */
	public static function createInvoiceFromOrder( Order $order ){
	print "ok";
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		$idorder = $order->getId();
	
		//récupérer toutes les lignes articles de la commande qui n'ont pas encore été facturée ( cf. facturation partielle )
		
		$idorder_rows 		= array();
		$idbl_deliveries 	= array();
		// ------- Mise à jour de l'id gestion des Index  #1161
		$query = "
		SELECT blrow.idbl_delivery, blrow.idorder_row
		FROM bl_delivery bl, bl_delivery_row blrow, order_supplier os
		WHERE bl.idbl_delivery = blrow.idbl_delivery
		AND bl.idorder = '$idorder'
		AND bl.idorder_supplier = os.idorder_supplier
		AND os.internal_supply = 0
		AND bl.idbilling_buyer = 0";

		$rs = DBUtil::query( $query );

		while( !$rs->EOF() ){
		
			$idorder_rows[] = $rs->fields( "idorder_row" );
			
			if( !in_array( $rs->fields( "idbl_delivery" ), $idbl_deliveries ) )
				$idbl_deliveries[] = $rs->fields( "idbl_delivery" );
			
			$rs->MoveNext();
			
		}		
			
		//si rien à facturer
		
		if( !count( $idorder_rows ) ){
				
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}
	
		//créer la facture
		
		$idbuyer 	= $order->get( "idbuyer" );
		$idcontact 	= $order->get( "idcontact" );

		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer
		
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem = $it->next();
			
			if( in_array( $orderItem->get( "idrow" ), $idorder_rows ) )
				$invoice->addOrderItem( $orderItem );
				
		}
		
		//calcul des frais port qui n'ont pas encore été facturés ( cf. facturation partielle )
		
		$total_charge_ht 	= $order->get( "total_charge_ht" );
		$total_charge 		= $order->get( "total_charge" );
		$charge_vat 		= $order->get( "charge_vat" );
		
		$query = "
		SELECT SUM( total_charge ) AS total_charge,
		SUM( total_charge_ht ) AS total_charge_ht,
		SUM( charge_vat ) AS charge_vat
		FROM billing_buyer
		WHERE idorder = '$idorder'
		GROUP BY idorder";
	
		$invoicesRS = DBUtil::query( $query );
		$invoiceCount = $invoicesRS->RecordCount();
		$isPartialInvoice = $invoiceCount > 1;
		
		while( !$invoicesRS->EOF() ){
		
			$total_charge 				-= $invoicesRS->fields( "total_charge" );
			$total_charge_ht 			-= $invoicesRS->fields( "total_charge_ht" );
			$charge_vat 				-= $invoicesRS->fields( "charge_vat" );

			$invoicesRS->MoveNext();
				
		}
		
		$invoice->set( "total_charge", 	$total_charge );
		$invoice->set( "total_charge_ht", 	$total_charge_ht );
		$invoice->set( "charge_vat", 		$total_charge - $total_charge_ht );
		$invoice->set( "charge_vat_rate", 	$order->get( "charge_vat_rate" ) ); //ne correspond pas vraiment si facturation partielle
		
		//appliquer les pourcentages de remise, d'acompte et de remise pour paiement comptant
		
		if( !$isPartialInvoice ){
			
			//remise sur facture
			
			$invoice->set( "total_discount" , $order->get( "total_discount" ) );
			$invoice->set( "total_discount_amount" , $order->get( "total_discount_amount" ) );
			
			//remise pour paiement comptant
			
			$invoice->set( "billing_rate" , $order->get( "billing_rate" ) );
			$invoice->set( "billing_amount" , $order->get( "billing_amount" ) );
			
			//acompte
			
			$invoice->set( "down_payment_type" , $order->get( "down_payment_type" ) );
			$invoice->set( "down_payment_value" , $order->get( "down_payment_value" ) );

		}
		else{ //on est en train de facturer partiellement le reste de la commande
			
			$articlesET = 0.0;
			$it = $invoice->getItems()->iterator();
			while( $it->hasNext() )
				$articlesET += $it->next()->getTotalET();
				
			$total_discount_rate = $order->get( "total_discount" );
			$total_discount_amount = $articlesET * $total_discount_rate / 100.0;
			$invoice->set( "total_discount_amount" , round( $total_discount_amount, 2 ) );
			$invoice->set( "total_discount" , $total_discount_rate );
			
			$billing_rate = $order->get( "billing_rate" );
			$ht = $articlesET - $invoice->get( "total_discount_amount" );
			$total_charge_ht = $invoice->get( "total_charge_ht" );
			$billing_amount = 0.0 + $billing_rate * ( $ht + $total_charge_ht ) / 100.0;
			$invoice->set( "billing_amount" , $billing_amount );
			
			//acompte
			
			$invoice->set( "down_payment_type" , $order->get( "down_payment_type" ) );
			$invoice->set( "down_payment_value" , $order->get( "down_payment_value" ) );
			
		}
		
		//propriétés de la facture héritées de la commande

		include_once( dirname( __FILE__ ) . "/Payment.php" );
		
		$invoice->set( "iderp", 				$order->get( "iderp" ) );
		$invoice->set( "idbuyer_erp", 			$order->get( "idbuyer_erp" ) );
		$invoice->set( "idbilling_adress_erp", $order->get( "idbilling_adress_erp" ) );
		$invoice->set( "idpayment", 			$order->get( "idpayment" ) );
		$invoice->set( "idpayment_delay", 		$order->get( "idpayment_delay" ) );
		$invoice->set( "send_by", 				$order->get( "send_by" ) );
		$invoice->set( "n_order", 				$order->get( "n_order" ) );
		
		$invoice->set( "total_charge_ht", 		$order->get( "total_charge_ht" ) );
		$invoice->set( "total_charge", 				$order->get( "total_charge" ) );
		
		$invoice->set( "title_offer", 				$order->get( "title_offer" ) );
		
		$invoice->set( "ecotaxe_code", 			$order->get( "ecotaxe_code" ) );
		$invoice->set( "ecotaxe_amount", 		$order->get( "ecotaxe_amount" ) );
		$invoice->set( "remote_creation_date", $order->get( "remote_creation_date" ) );
		$invoice->set( "factor", 				$order->get( "factor" ) );
		$invoice->set( "svg_mail", 				$order->get( "svg_mail" ) );
		$invoice->set( "svg_fax", 				$order->get( "svg_fax" ) );
		$invoice->set( "pro_forma", 			$order->get( "pro_forma" ) );
		$invoice->set( "global_delay", 			$order->get( "global_delay" ) );
		$invoice->set( "no_tax_export", 		$order->get( "no_tax_export" ) );
		$invoice->set( "deliv_payment", 		Payment::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->get( "idpayment_delay" ) ) ); //? @todo
		$invoice->set( "credit_amount", 		$order->get( "credit_amount" ) );
		$invoice->set( "charge_auto", 			0 );
		$invoice->set( "total_charge_auto", 	0 );
		
		//adresses de livraison et de facturation

		$invoice->setInvoiceAddress( $order->getInvoiceAddress() );
		$invoice->setForwardingAddress( $order->getForwardingAddress() );

		//escompte
		
		if( $order->get( "rebate_value" ) > 0.0 ){
			
			switch( $order->get( "rebate_type" ) ){
				
				case "rate" :
				
					$invoice->set( "rebate_type", 	"rate" ); 
	        		$invoice->set( "rebate_value", $order->get( "rebate_value" ) );
	        
					break;
					
				case "amount" :
				
					if( $isPartialInvoice ){
						
						$invoice->save();
	
						$rebate_rate 	= $order->getTotalATI() > 0.0 ? $order->get( "rebate_value" ) * 100.0 / $order->getTotalATI() : 0.0;
						$rebate_value 	= $invoice->getTotalATI() * $rebate_rate / 100.0;
						
						$invoice->set( "rebate_type", "amount" );
						$invoice->set( "rebate_value", $rebate_value );
					
					}
					else{
						
						$invoice->set( "rebate_type", "amount" ); 
	        			$invoice->set( "rebate_value", $order->get( "rebate_value" ) );
	        		
					}
					
					break;			
				
			}

		}
		
		//mettre à jour le numéro de facture dans les BLs concernés : @todo : la jointure est faite dans le mauvais sens!
		
		$idbilling_buyer = $invoice->getId();
		
		foreach( $idbl_deliveries as $idbl_delivery )
			DBUtil::query( "UPDATE bl_delivery SET idbilling_buyer = '$idbilling_buyer' WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );

		/* enregistrement des paiements */
		
		self::registerInvoicePayments( $invoice );
		
		//assurance crédit
		
		$invoice->insure( $order->get( "insurance" ) == 1 );
		
		/* sauvegarde */
		
		$invoice->save();

		$order->set( "total_charge_auto" , 0 );
		$order->save();
			//--------------------------------------------maj des tva table : billing buyer---------------------------------------------------------------------------------------------------
	include_once( dirname( __FILE__ ) . "/../config/init.php" );
		$generatedInvoices = array();
	
		$query = "SELECT idbilling_buyer FROM billing_buyer WHERE idorder = '$idorder'";
			$rs =& DBUtil::query( $query );
			
			while( !$rs->EOF() ){
				$generatedInvoices[] = $rs->fields( "idbilling_buyer" );
				$idbilling_buyer = $rs->fields( "idbilling_buyer" );
				// ------- Mise à jour de l'id gestion des Index  #1161
				$qs = " SELECT order_row.idorder, order_row.quantity, order_row.discount_price, order_row.vat_rate
						FROM   `order_row`
						WHERE
					
					
						 order_row.idorder = '$idorder'
						";
				$rs1 =& DBUtil::query( $qs );
				$total_vat_2 = $total_amount_2 = $total_vat_1 = $total_amount_1 = 0.00;
				while( !$rs1->EOF() ){
				
					$vat_rate = $rs1->fields( "vat_rate" );
					$quantity = $rs1->fields( "quantity" );
					$price = $rs1->fields( "discount_price" );
					$idorder =  $rs1->fields( "idorder" );
					//var_dump($price);
					if( B2B_STRATEGY ){
						switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_2 = round ($total_vat_2, 2 );
								$total_amount_2 = $total_amount_2 + $quantity*$price ;
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + $quantity*$price ;
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + $quantity*$price ;
							break;
							default:					
							break;
						}
					}	
					else{
						switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 ) / 1.055;
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 ) / 1.055;
								$total_vat_2 = round ($total_vat_2, 2 );
								//$total_amount_2 =  ($total_amount_2 + $quantity*$price ) /1.055 ;
								$total_amount_2 = $total_amount_2 + ( $quantity*$price ) /1.055 ;
								$total_amount_2 = round ($total_amount_2, 2 );
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 ) / 1.20;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ($quantity*$price) / 1.20 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 ) / 1.196;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ($quantity*$price) / 1.196 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							default:					
							break;
						}
					}
					$rs1->MoveNext();
				}
				
				$qr1 = " SELECT *
						FROM billing_buyer
						WHERE 					
						idbilling_buyer = '$idbilling_buyer'
						 ";
						
				$res1 =& DBUtil::query( $qr1 );
				
				$disc = $res1->fields( "total_discount" );
				
				$total_amount_1 = $total_amount_1 - $total_amount_1 * $disc / 100;
				$total_amount_2 = $total_amount_2 - $total_amount_2 * $disc / 100;
				$total_vat_1 = $total_vat_1 - $total_vat_1 * $disc / 100;
				$total_vat_2 = $total_vat_2 - $total_vat_2 * $disc / 100;
				
				
				$q = "UPDATE `billing_buyer` 
					  SET 
						`total_amount_1` = '$total_amount_1',
						`total_amount_2` = '$total_amount_2',
						`total_vat_1` = '$total_vat_1',
						`total_vat_2` = '$total_vat_2' 
						WHERE `billing_buyer`.`idbilling_buyer` = '$idbilling_buyer' ";
			//	echo $q ;
				DBUtil::query( $q );
				
				$rs->MoveNext();
			}
		
		//-----------------------------------------------------------------------------------------------------------------------------------
		/* sauvegarde */
		
		$invoice->save();

		$order->set( "total_charge_auto" , 0 );
		$order->save();
		return $invoice;
	
	}

	//----------------------------------------------------------------------------
	/**
	 * @todo gestion des règlements multiples
	 */
	private function registerInvoicePayments( Invoice &$invoice ){

		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$unpaidAmount = $invoice->getNetBill();
		$idbilling = $invoice->get('idbilling_buyer');
		
		if( $unpaidAmount == 0.0 )
			return;
			
		/* Règlement par chèque cadeau */
		
		$rs =& DBUtil::query( "SELECT idtoken FROM gift_token WHERE idorder = '" . $invoice->get( "idorder" ) . "' LIMIT 1" );
		
		if( $rs->RecordCount() ){
			
			include_once( dirname( __FILE__ ) . "/GiftToken.php" );
			
			$query = "
			SELECT gtm.value 
			FROM gift_token_model gtm, gift_token gt 
			WHERE gt.idtoken = '" . $rs->fields( "idtoken" ) . "'
			AND gtm.type LIKE '" . GiftToken::$TYPE_AMOUNT . "' 
			AND gt.idtoken_model = gtm.idmodel
			LIMIT 1";
		
			$rs =& DBUtil::query( $query );
			$gift_token_amount = $rs->RecordCount() ? min( $rs->fields( "value" ), $unpaidAmount ) : 0.0;
			
			if( $gift_token_amount > 0.0 ){
				
				include_once( dirname( __FILE__ ) . "/Payment.php" );
				
				if( $gift_token_amount < $invoice->getNetBill() )
						RegulationsBuyer::createPartialPayment( $invoice, Payment::$PAYMENT_MOD_GIFT_TOKEN, $gift_token_amount );
				else 	RegulationsBuyer::createPayment( $invoice );
				
				$unpaidAmount -= $gift_token_amount;
				
				$invoice->set( "balance_amount", 	$invoice->get( "balance_amount" ) + $gift_token_amount );
				$invoice->set( "balance_date", 	date( "Y-m-d" ) );
				$invoice->set( "status", $unpaidAmount > 0.0 ? Invoice::$STATUS_INVOICED : Invoice::$STATUS_PAID );
				$invoice->save();
			
			}
			
		}
	
		if( $unpaidAmount == 0.0 )
			return;
			
		/* Règlement par bon d'achat ( avoir ) */
		
		$rs =& DBUtil::query( "SELECT credit_amount FROM `order` WHERE idorder = '" . $invoice->get( "idorder" ) . "' LIMIT 1" );
		
		if( $rs->RecordCount() ){

			$credit_balance = DBUtil::getDBValue( "credit_balance", "buyer", "idbuyer", $invoice->get( "idbuyer" ) );
			$credit_amount 	= min( $rs->fields( "credit_amount" ), $unpaidAmount, $credit_balance );
	
			if( $credit_amount > 0.0 ){

				$query = "
				UPDATE buyer 
				SET credit_balance = GREATEST( 0.0, credit_balance - $credit_amount ) 
				WHERE idbuyer = '" . $invoice->get( "idbuyer" ) . "' 
				AND credit_balance >= '$credit_amount'
				LIMIT 1";

				DBUtil::query( $query );
				
				if( !DBUtil::getAffectedRows() ){
					
					trigger_error( "transaction non autorisée", E_USER_ERROR );
					exit();
					
				}

				$unpaidAmount -= $credit_amount;
				
				$invoice->set( "balance_amount", 	$invoice->get( "balance_amount" ) + $credit_amount );
				$invoice->set( "balance_date", 	date( "Y-m-d" ) );
				$invoice->set( "status", $unpaidAmount > 0.0 ? Invoice::$STATUS_INVOICED : Invoice::$STATUS_PAID );
				$invoice->save();
			
			}
			
		}
	
		if( $unpaidAmount == 0.0 )
			return;
		
		//création du reglement pour l'acompte si acompte tt seul. bisou
		$down_payment_amount = $invoice->getDownPaymentAmount();

		if( $down_payment_amount > 0 ){
			$rs =& DBUtil::query( "SELECT down_payment_idpayment,down_payment_idregulation, down_payment_date, down_payment_comment FROM `order` WHERE idorder = '" . $invoice->get( "idorder" ) . "' LIMIT 1" );
			
			$dateAcompte = $rs->fields("down_payment_date");
 			$idDownPayment = $rs->fields("down_payment_idpayment");
 			$commentAcompte = $rs->fields("down_payment_comment");
 			
			$idregulationToAdd = $rs->fields("down_payment_idregulation");
			
 			// comme le règlement est créé à l'avance, on fait juste le lien dans la table billing_regulations_buyer
			//RegulationsBuyer::createPartialPayment( $invoice, $idDownPayment , $down_payment_amount, 1 ,$dateAcompte,$commentAcompte);
			if( $idregulationToAdd == 0 ){
				$idregulationToAdd = self::createMyRegulation($invoice->get( "idorder" ),"order",$invoice->get( "idbuyer" ),$down_payment_amount,$idDownPayment,$dateAcompte,0,"aucune info","créé automatiquement cause ancienne commande",1);
			}
				// ------- Mise à jour de l'id gestion des Index  #1161
			DBUtil::query("INSERT INTO billing_regulations_buyer VALUES('$idbilling', '$idregulationToAdd',$down_payment_amount, 0.0, " . DBUtil::quote( User::getInstance()->get( "login" ) ) . ", NOW())");
		
		}
		
		
		/*
		 * paiement direct à la commande ( CB, fianet, ... )
		 * Note : dans le cas d'un paiement direct lors du processus de commande en ligne, le champ cash_payment est renseigné à 1
		 * et le montant réglé est stocké dans balance_amount.
		 * Attention : on ne gère pas ici de paiement partiel! Le montant qui a été réglé par CB correspond à la total de la commande ou 
		 * au total auquel ont été déduits le montant des avoirs et des chèques cadeaux utilisés
		 */

		$rs =& DBUtil::query( "SELECT cash_payment , cash_payment_idregulation  FROM `order` WHERE idorder = '" . $invoice->get( "idorder" ) . "' LIMIT 1" );
		
		if( $rs->RecordCount() && $rs->fields( "cash_payment" ) == '1' ){
			$invoice->set( "balance_amount", 	$invoice->getNetBill() );
			$invoice->set( "balance_date", 	date( "Y-m-d" ) );
			
			$idregulationToAdd = $rs->fields( "cash_payment_idregulation" );
			
			if( $idregulationToAdd == 0 ){
				//pour les anciennes on créé le reglement...
				$idregulationToAdd = self::createMyRegulation($invoice->get( "idorder" ),"order",$invoice->get( "idbuyer" ),$invoice->getNetBill(),$invoice->get( "idpayment" ),date( "Y-m-d" ),1,"aucune info","règlement créé automatiquement cause ancienne commande");
			}
			
			if( $unpaidAmount < $invoice->getNetBill() ){
				//RegulationsBuyer::createPartialPayment( $invoice, $invoice->get( "idpayment" ), $unpaidAmount );
				$myAmountC = $unpaidAmount;
			}else{ 	
				//RegulationsBuyer::createPayment( $invoice );
				$myAmountC = $invoice->getNetBill();
			}
			
			DBUtil::query("INSERT INTO billing_regulations_buyer VALUES($idbilling, '$idregulationToAdd',$myAmountC, 0.0, " . DBUtil::quote( User::getInstance()->get( "login" ) ) . ", NOW())");
			
			$invoice->set( "status", Invoice::$STATUS_PAID );
			$invoice->save();
			
			return;
			
		}
			
		/* modes paiement différés */

		$invoice->set( "status", Invoice::$STATUS_INVOICED );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture partielle à partir d'une commande et d'un numéro de BL donnés
	 * @static
	 * @param Order $order la commande à facturer partiellement
	 * @param int $idbl_delivery le numéro BL contenant les articles à facturer
	 * @param float $total_charge_ht le montant des frais de port HT répartis dans cette facture
	 * @return void
	 */
	public static function createInvoiceFromDeliveryNote( Order &$order, $idbl_delivery, $total_charge_ht = 0.0 ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$idorder = $order->getId();
	
		/* ne pas refacturer */
		
		if( DBUtil::query( "SELECT idbilling_buyer FROM bl_delivery WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" )->fields( "idbilling_buyer" ) )
			return;
			
		//récupérer les lignes article à facturer
		
		$idorder_rows = array();
		$rs =& DBUtil::query( "SELECT idorder_row FROM bl_delivery_row WHERE idbl_delivery = '$idbl_delivery'" );

		while( !$rs->EOF() ){
		
			$idorder_rows[] = $rs->fields( "idorder_row" );
			
			$rs->MoveNext();
			
		}		
		
		//si rien à facturer
		
		if( !count( $idorder_rows ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}

		//créer la facture
		
		$idbuyer 	= $order->get( "idbuyer" );
		$idcontact 	= $order->get( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer

		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			if( in_array( $orderItem->get( "idrow" ), $idorder_rows ) )
				$invoice->addOrderItem( $orderItem );
			
		}
		
		//appliquer les frais de port
		
		$charge_vat_rate 	= $invoice->getVATRate() / 100.0;
		$total_charge 		= $total_charge_ht * ( 1 + $charge_vat_rate );
		
		$invoice->set( "total_charge_ht", $total_charge_ht );
		$invoice->set( "total_charge", $total_charge );
		$invoice->set( "charge_vat", $total_charge - $total_charge_ht );
		$invoice->set( "charge_vat_rate", $charge_vat_rate );
		
		//appliquer les pourcentages de remise, d'acompte et de remise pour paiement comptant
		
		$articlesET = 0.0;
		$it = $invoice->getItems()->iterator();
		while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
				
		$total_discount_rate = $order->get( "total_discount" );
		$total_discount_amount = $articlesET * $total_discount_rate / 100.0;
		$invoice->set( "total_discount_amount" , round( $total_discount_amount, 2 ) );
		$invoice->set( "total_discount" , $total_discount_rate );
		
		$billing_rate = $order->get( "billing_rate" );
		$ht = $articlesET - $invoice->get( "total_discount_amount" );
		$total_charge_ht = $invoice->get( "total_charge_ht" );
		$billing_amount = 0.0 + $billing_rate * ( $ht + $total_charge_ht ) / 100.0;
		$invoice->set( "billing_amount" , $billing_amount );
		
		$invoice->set( "down_payment_type" , $order->get( "down_payment_type" ) );
		$invoice->set( "down_payment_value" , $order->get( "down_payment_value" ) );

		//propriétés de la facture héritées de la commande

		include_once( dirname( __FILE__ ) . "/Payment.php" );
		
		$invoice->set( "iderp", 				$order->get( "iderp" ) );
		$invoice->set( "idbuyer_erp", 			$order->get( "idbuyer_erp" ) );
		$invoice->set( "idbilling_adress_erp", 	$order->get( "idbilling_adress_erp" ) );
		$invoice->set( "idpayment", 			$order->get( "idpayment" ) );
		$invoice->set( "idpayment_delay", 		$order->get( "idpayment_delay" ) );
		$invoice->set( "send_by", 				$order->get( "send_by" ) );
		$invoice->set( "n_order", 				$order->get( "n_order" ) );
		$invoice->set( "remote_creation_date", 	$order->get( "remote_creation_date" ) );
		$invoice->set( "factor", 				$order->get( "factor" ) );
		$invoice->set( "svg_mail", 				$order->get( "svg_mail" ) );
		$invoice->set( "svg_fax", 				$order->get( "svg_fax" ) );
		$invoice->set( "pro_forma", 			$order->get( "pro_forma" ) );
		$invoice->set( "global_delay", 			$order->get( "global_delay" ) );
		$invoice->set( "deliv_payment", 		Payment::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->get( "idpayment_delay" ) ) );
		
		if( $order->get( "cash_payment" ) ){
			
			$invoice->set( "status", Invoice::$STATUS_PAID );
			$invoice->set( "balance_amount", $invoice->get( "total_amount_ht" ) );
			$invoice->set( "balance_date", $order->get( "balance_date" ) );

		}
		else{
			
			$invoice->set( "status", Invoice::$STATUS_INVOICED );
			$invoice->set( "balance_amount", 0.0 );
			$invoice->set( "balance_date", "0000-00-00" );
			
		}
		
		$invoice->set( "charge_auto", 0 );
		$invoice->set( "total_charge_auto", 0 );
		
		
		//adresses de livraison et de facturation
		
		$idbilling_adress 	= $order->get( "idbilling_adress" );
		$iddelivery 		= $order->get( "iddelivery" );
		$idbuyer 			= $order->get( "idbuyer" );
		
		if( $idbilling_adress )
				$invoice->setInvoiceAddress( new InvoiceAddress( $idbilling_adress ) );
		else 	$invoice->setInvoiceAddress( $order->getCustomer()->getAddress() );
		
		if( $iddelivery )
				$invoice->setForwardingAddress( new ForwardingAddress( $iddelivery ) );
		else 	$invoice->setForwardingAddress( $order->getCustomer()->getAddress() );

		//escompte
		
		$invoice->save();
		$isPartialInvoice = Invoice::isPartialInvoice( $invoice->getId() );
		
		if( $order->get( "rebate_value" ) > 0.0 ){
			
			switch( $order->get( "rebate_type" ) ){
				
				case "rate" :
				
					$invoice->set( "rebate_type", 	"rate" ); 
	        		$invoice->set( "rebate_value", $order->get( "rebate_value" ) );
	        
					break;
					
				case "amount" :
				
					if( $isPartialInvoice ){
	
						$rebate_rate 	= $order->getTotalATI() > 0.0 ? $order->get( "rebate_value" ) * 100.0 / $order->getTotalATI() : 0.0;
						$rebate_value 	= $invoice->getTotalATI() * $rebate_rate / 100.0;
						
						$invoice->set( "rebate_type", "amount" );
						$invoice->set( "rebate_value", $rebate_value );
					
					}
					else{
						
						$invoice->set( "rebate_type", "amount" ); 
	        			$invoice->set( "rebate_value", $order->get( "rebate_value" ) );
	        		
					}
					
					break;			
				
			}

		}
		
		//assurance crédit
		
		$invoice->insure( $order->get( "insurance" ) == 1 );
		
		//sauvegarde
		
		$invoice->save();
		
		//mettre à jour le numéro de facture dans le BL concerné
		
		$idbilling_buyer = $invoice->getId();
		
		DBUtil::query( "UPDATE bl_delivery SET idbilling_buyer = '$idbilling_buyer' WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
		
		//paiement direct
		
		if( $order->get( "cash_payment" ) )
			RegulationsBuyer::createPayment( $invoice );
			
		return $invoice;
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une facture partielle à partir d'une commande et d'une liste de numéros de BL donnée
	 * @static
	 * @param Order $order la commande à facturer partiellement
	 * @param array $deliveryNotes un tableau indicé contenant les numéros BL à facturer
	 * @param float $total_charge_ht le montant des frais de port HT répartis dans cette facture
	 * @return void
	 */
	public static function createInvoiceFromDeliveryNotes( Order &$order, $deliveryNotes, $total_charge_ht = 0.0 ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$idorder = $order->getId();
	
		/* ne pas refacturer */
		
		$tmp = array();
		$i = 0;
		while( $i < count( $deliveryNotes ) ){
	
			if( DBUtil::query( "SELECT idbilling_buyer FROM bl_delivery WHERE idbl_delivery = '" . $deliveryNotes[ $i ] . "' LIMIT 1" )->fields( "idbilling_buyer" ) == 0 )
				$tmp[] = $deliveryNotes[ $i ];
		
			$i++;
			
		}
		
		if( !count( $tmp ) )
			return;
			
		$deliveryNotes = $tmp;
		
		//récupérer les lignes article à facturer
		
		$idorder_rows = array();
		
		foreach( $deliveryNotes as $idbl_delivery ){
			
			$rs =& DBUtil::query( "SELECT idorder_row FROM bl_delivery_row WHERE idbl_delivery = '$idbl_delivery'" );
	
			while( !$rs->EOF() ){
			
				$idorder_rows[] = $rs->fields( "idorder_row" );
				
				$rs->MoveNext();
				
			}		
		
		}
		
		//si rien à facturer
		
		if( !count( $idorder_rows ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}

		//créer la facture
		
		$idbuyer 	= $order->get( "idbuyer" );
		$idcontact 	= $order->get( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer

		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();
			
			if( in_array( $orderItem->get( "idrow" ), $idorder_rows ) )
				$invoice->addOrderItem( $orderItem );
			
		}
		
		//appliquer les frais de port
		
		$charge_vat_rate 	= $invoice->getVATRate() / 100.0;
		$total_charge 		= $total_charge_ht * ( 1 + $charge_vat_rate );
		
		$invoice->set( "total_charge_ht", 	$total_charge_ht );
		$invoice->set( "total_charge", 	$total_charge );
		$invoice->set( "charge_vat", 		$total_charge - $total_charge_ht );
		$invoice->set( "charge_vat_rate", 	$charge_vat_rate );
		
		//appliquer les pourcentages de remise, d'acompte et de remise pour paiement comptant
		
		$articlesET = 0.0;
		$it = $invoice->getItems()->iterator();
		while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
			
		$total_discount_rate = $order->get( "total_discount" );
		$total_discount_amount = $articlesET * $total_discount_rate / 100.0;
		$invoice->set( "total_discount_amount" , round( $total_discount_amount, 2 ) );
		$invoice->set( "total_discount" , $total_discount_rate );
		
		$billing_rate = $order->get( "billing_rate" );
		$ht = $articlesET - $invoice->get( "total_discount_amount" );
		$total_charge_ht = $invoice->get( "total_charge_ht" );
		$billing_amount = 0.0 + $billing_rate * ( $ht + $total_charge_ht ) / 100.0;
		$invoice->set( "billing_amount" , $billing_amount );
		
		$invoice->set( "down_payment_type" , $order->get( "down_payment_type" ) );
		$invoice->set( "down_payment_value" , $order->get( "down_payment_value" ) );

		//propriétés de la facture héritées de la commande

		include_once( dirname( __FILE__ ) . "/Payment.php" );
		
		$invoice->set( "iderp", 				$order->get( "iderp" ) );
		$invoice->set( "idbuyer_erp", 			$order->get( "idbuyer_erp" ) );
		$invoice->set( "idbilling_adress_erp", $order->get( "idbilling_adress_erp" ) );
		$invoice->set( "idpayment", 			$order->get( "idpayment" ) );
		$invoice->set( "idpayment_delay", 		$order->get( "idpayment_delay" ) );
		$invoice->set( "send_by", 				$order->get( "send_by" ) );
		$invoice->set( "n_order", 				$order->get( "n_order" ) );
		$invoice->set( "remote_creation_date", $order->get( "remote_creation_date" ) );
		$invoice->set( "factor", 				$order->get( "factor" ) );
		$invoice->set( "svg_mail", 			$order->get( "svg_mail" ) );
		$invoice->set( "svg_fax", 				$order->get( "svg_fax" ) );
		$invoice->set( "pro_forma", 			$order->get( "pro_forma" ) );
		$invoice->set( "global_delay", 		$order->get( "global_delay" ) );
		$invoice->set( "deliv_payment", 		Payment::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->get( "idpayment_delay" ) ) );
		
		if( $order->get( "cash_payment" ) ){
			
			$invoice->set( "status", Invoice::$STATUS_PAID );
			$invoice->set( "balance_amount", $invoice->get( "total_amount_ht" ) );
			$invoice->set( "balance_date", $order->get( "balance_date" ) );

		}
		else{
			
			$invoice->set( "status", Invoice::$STATUS_INVOICED );
			$invoice->set( "balance_amount", 0.0 );
			$invoice->set( "balance_date", "0000-00-00" );
			
		}
		
		$invoice->set( "charge_auto", 0 );
		$invoice->set( "total_charge_auto", 0 );
		
		
		//adresses de livraison et de facturation
		
		$idbilling_adress 	= $order->get( "idbilling_adress" );
		$iddelivery 		= $order->get( "iddelivery" );
		$idbuyer 			= $order->get( "idbuyer" );
		
		if( $idbilling_adress )
				$invoice->setInvoiceAddress( new InvoiceAddress( $idbilling_adress ) );
		else 	$invoice->setInvoiceAddress( $order->getCustomer()->getAddress() );
		
		if( $iddelivery )
				$invoice->setForwardingAddress( new ForwardingAddress( $iddelivery ) );
		else 	$invoice->setForwardingAddress( $order->getCustomer()->getAddress() );

		//escompte
		
		$invoice->save();
		$isPartialInvoice = Invoice::isPartialInvoice( $invoice->getId() );
		
		if( $order->get( "rebate_value" ) > 0.0 ){
			
			switch( $order->get( "rebate_type" ) ){
				
				case "rate" :
				
					$invoice->set( "rebate_type", 	"rate" ); 
	        		$invoice->set( "rebate_value", $order->get( "rebate_value" ) );
	        
					break;
					
				case "amount" :
				
					if( $isPartialInvoice ){
	
						$rebate_rate 	= $order->getTotalATI() > 0.0 ? $order->get( "rebate_value" ) * 100.0 / $order->getTotalATI() : 0.0;
						$rebate_value 	= $invoice->getTotalATI() * $rebate_rate / 100.0;
						
						$invoice->set( "rebate_type", "amount" );
						$invoice->set( "rebate_value", $rebate_value );
					
					}
					else{
						
						$invoice->set( "rebate_type", "amount" ); 
	        			$invoice->set( "rebate_value", $order->get( "rebate_value" ) );
	        		
					}
					
					break;			
				
			}

		}
		
		//assurance crédit
		
		$invoice->insure( $order->get( "insurance" ) == 1 );
		
		//sauvegarde
		
		$invoice->save();
		
		//mettre à jour le numéro de facture dans le BL concerné
		
		$idbilling_buyer = $invoice->getId();
		
		foreach( $deliveryNotes as $idbl_delivery )
			DBUtil::query( "UPDATE bl_delivery SET idbilling_buyer = '$idbilling_buyer' WHERE idbl_delivery = '$idbl_delivery' LIMIT 1" );
		
		//paiement direct
		
		if( $order->get( "cash_payment" ) )
			RegulationsBuyer::createPayment( $invoice );
			
		return $invoice;
	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un avoir depuis une facture
	 * @param Invoice $invoice la facture à partir de laquelle on souhaite créer l'avoir
	 * @return Credit le nouvel avoir
	 */
	public static function createCreditFromInvoice( Invoice &$invoice ){
	
		$idbuyer 	= $invoice->get( "idbuyer" );
		$idcontact 	= $invoice->get( "idcontact" );
		
		//créer un avoir vierge
		
		$credit = self::createCredit( $invoice->getId(), $idbuyer, $idcontact );
		
		//lignes article
		
		$it = $invoice->getItems()->iterator();
		while( $it->hasNext() )
			$credit->addInvoiceItem( $it->next() );
		
		//TVA
		
		$invoice_total_amount_ht 	= $invoice->getTotalET();
		$invoice_total_amount 		= $invoice->getTotalATI();
		$vat_rate 					= $invoice_total_amount_ht > 0.0 ? ( $invoice_total_amount / $invoice_total_amount_ht - 1 ) * 100.0 : 0.0;
		
		$credit->set( "vat_rate", round( $vat_rate, 2 ) );

		//objet du navoir
		
		list( $date, $time ) = explode( " ", $invoice->get( "DateHeure" ) );
		list( $year, $month, $day ) = explode( "-", $date );
									
		$credit->set( "object", "ANNULATION TOTALE DE LA FACTURE N°" . $credit->get( "idbilling_buyer" ) . " DU $day/$month/$year" );
		
		$credit->set( "credited_charges", $invoice->get( "total_charge_ht" ) ); //port recrédité
		$credit->set( "total_charge_ht", 0.0 ); //port refacturé
		
		//remise sur facture
		//@todo : écarts de plusieurs cents possibles car le type de remise à conserver ( montant ou % ) est inconnu
		if( $invoice->get( "total_discount" ) > 0.0 ){
			
			$it = $credit->getItems()->iterator();
			while( $it->hasNext() ){
			
				$creditItem =& $it->next();
				$creditItem->set( "discount_rate", $invoice->get( "total_discount" ) );
				
				$discount_price = $creditItem->get( "unit_price" ) * ( 1 - $creditItem->get( "discount_rate" ) / 100.0 );
				$creditItem->set( "discount_price", round( $discount_price, 2 ) );
			
			}

		}

		/* escompte sur facture */
		
		if( $invoice->get( "rebate_value" ) > 0.0 ){
			
			$rebate_rate = $invoice->get( "rebate_type" ) == "rate" ? $invoice->get( "rebate_value" ) : $invoice->get( "rebate_value" ) * 100.0 / $invoice->getTotalATI();

			$it = $credit->getItems()->iterator();
			while( $it->hasNext() ){
			
				$creditItem =& $it->next();
				
				$discount_price = $creditItem->get( "discount_price" ) * ( 1 - $rebate_rate / 100.0 );
				$discount_rate 	= $discount_price * 100.0 / $creditItem->get( "unit_price" );

				$creditItem->set( "discount_rate", $discount_rate );
				$creditItem->set( "discount_price", $discount_price );
			
			}
			
		}
		
		//sauvegarde
		
		$credit->save();
		
		return $credit;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé un avoir depuis un litige. L'avoir contiendra uniquement la ligne article litigieuse de la facture
	 * @param Litigation $litigation le litige à partir de laquelle on souhaite créer l'avoir
	 * @return Credit le nouvel avoir
	 */
	public static function createLitigationCredit( Litigation $litigation ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		$invoice = new Invoice( $litigation->getSale()->get( "idbilling_buyer" ) );

		//créer un avoir vierge
		
		$credit = self::createCredit( $invoice->getId(), $invoice->get( "idbuyer" ), $invoice->get( "idcontact" ) );
		
		$credit->set( "idlitigation", $litigation->getIdLitigation() );
		
		//ligne article

		$credit->addInvoiceItem( $litigation->getItem() );

		//TVA
		
		$invoice_total_amount_ht 	= $invoice->getTotalET();
		$invoice_total_amount 		= $invoice->getNetBill();
		if($invoice_total_amount_ht >1)
		$vat_rate 					= ( $invoice_total_amount / $invoice_total_amount_ht - 1 ) * 100.0;
		else
		$vat_rate 					= 0.0;
		
		$credit->set( "vat_rate", round( $vat_rate, 2 ) );

		//objet du navoir
		
		list( $date, $time ) = explode( " ", $invoice->get( "DateHeure" ) );
		list( $year, $month, $day ) = explode( "-", $date );
						
		$credit->set( "object", "ANNULATION PARTIELLE DE LA FACTURE N°" . $credit->get( "idbilling_buyer" ) . " DU $day/$month/$year" );
		
		$credit->set( "credited_charges", $invoice->get( "total_charge_ht" ) ); //port recrédité
		//$credit->set( "total_charge_ht", 566666 ); //port refacturé
		$credit->set( "total_charge_ht", 0.0 );
		
		//remise sur facture
		/* @todo : écarts de plusieurs cents possibles car le type de remise à conserver ( montant ou % ) est inconnu
		if( $invoice->get( "total_discount" ) > 0.0 ){
			
			$it = $credit->getItems()->iterator();
			while( $it->hasNext() ){
			
				$creditItem =& $it->next();
				$creditItem->set( "discount_rate", $invoice->get( "total_discount" ) );
				
				$discount_price = $creditItem->get( "unit_price" ) * ( 1 - $creditItem->get( "discount_rate" ) / 100.0 );
				$creditItem->set( "discount_price", round( $discount_price, 2 ) );
			
			}
		
		}
		*/
		
		//sauvegarde
		
		$credit->save();
		
		return $credit;
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une nouvelle commande fournisseur de réapprovisionnement
	 * @static
	 * @access private
	 * @return SupplierOrder la nouvelle commande
	 */
	 
	public static function createSupplierOrder( $idsupplier, $idbuyer, $idcontact = 0 ){
		
		include_once( dirname( __FILE__ ) . "/SupplierOrder.php" );
		
		//récupérer un nouveau numéro de commande disponible


$table = 'order_supplier';
			$idorder_supplier = self::Indexations($table);
		
		
		if( $idorder_supplier == null )
			$idorder_supplier = 1;



	 	//réserver le numero de commande fournisseur
	 	
	 	$query = "
		INSERT INTO `order_supplier` ( 
			idorder_supplier, 
			idsupplier,
			idorder,
			iduser,
			idbuyer,
			idcontact
		) VALUES ( 
			'$idorder_supplier', 
			'$idsupplier',
			NULL,
			'" . User::getInstance()->getId() . "',
			'$idbuyer',
			'$idcontact'
		)";
		
		$ret =& DBUtil::query( $query );
		
		if( $ret === false ){
			
			trigger_error( "Impossible de créer la nouvelle commande fournisseur", E_USER_ERROR );
			exit();
			
		}
			
		//créer la commande
		
		$supplierOrder = new SupplierOrder( $idorder_supplier );

		include_once( dirname( __FILE__ ) . "/Customer.php" );
		
	 	$supplierOrder->set( "DateHeure", 		date( "Y-m-d H:i:s" ) );
		$supplierOrder->set( "status",			SupplierOrder::$STATUS_STANDBY );
		$supplierOrder->set( "idpayment", 		$supplierOrder->getSupplier()->get( "idpaiement" ) );
		$supplierOrder->set( "idpayment_delay", $supplierOrder->getSupplier()->get( "pay_delay" ) );
		
		//Commentaire par défaut
		
		$comment =	"Merci de nous communiquer notre commande et le délai prévisionnel.";
		$comment .= "\nDès que la marchandise sera prête, nous vous communiquerons le bon de livraison sur demande.";
		$comment .= "\nMerci d'avance pour votre collaboration.";
		
		$supplierOrder->set( "comment", $comment );
		
	 	$supplierOrder->save();

	 	return $supplierOrder;
	 	
	}
	
	//----------------------------------------------------------------------------
	/**
	 * Créé une commande de chèque-cadeau
	 * @todo adapter le logiciel pour ce genre de service
	 * @static
	 * @access public
	 * @param GiftToken $giftToken le chèque-cadeau
	 * @return Order la nouvelle commande
	 */
	public static function createOrderFromGiftToken( GiftToken &$giftToken, $idbuyer, $idcontact = 0 ){
	
		include_once( dirname( __FILE__ ) . "/Basket.php" );
		include_once( dirname( __FILE__ ) . "/GiftToken.php" );
		
		assert_options( ASSERT_ACTIVE, 1 );
		assert_options( ASSERT_WARNING, 0 );
		assert_options( ASSERT_BAIL, 1 );
		assert_options( ASSERT_QUIET_EVAL, 1 );
		assert_options( ASSERT_CALLBACK, "assert_callback" );
		
		$type = DBUtil::getDBValue( "type", "gift_token_model", "idmodel", $giftToken->get( "idtoken_model" ) );
	
		assert( "strcmp( \"$type\", \"" . GiftToken::$TYPE_AMOUNT . "\" ) === 0" );

		$amount = round( DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $giftToken->get( "idtoken_model" ) ), 2 );

		$order = self::createOrder( $idbuyer, $idcontact );
				
		$order->setSalesman( Basket::getInstance()->getSalesman() );

		include_once( dirname( __FILE__ ) . "/Payment.php" );
		
		$order->set( "idpayment", 				Payment::$PAYMENT_MOD_DEBIT_CARD );
		$order->set( "status", 				Order::$STATUS_PAID );
		$order->set( "comment", 				"" );
		$order->set( "deliv_payment" , 		date( "Y-m-d H:i:s" ) );
		$order->set( "down_payment_type" ,		"amount" );
		$order->set( "down_payment_value" , 	0.0 );
		$order->set( "billing_rate" , 			0.0 );
		$order->set( "billing_amount" , 		0.0 );
		$order->set( "total_discount" , 		0.0 );
		$order->set( "total_discount_amount", 	0.0 );
		$order->set( "idpayment_delay", 		1 ); //@todo sécuriser
		$order->set( "paid", 					0 );
		$order->set( "cash_payment", 			0 );
		$order->set( "balance_date", 			"0000-00-00" );
		$order->set( "factor" , 				0 );
		$order->set( "total_charge_auto" , 	0 );
		$order->set( "charge_vat_rate", 		0.0 );
		$order->set( "charge_vat", 			0.0 );
		$order->set( "total_charge", 			0.0 );
		$order->set( "total_charge_ht", 		0.0 );
		
		$order->setForwardingAddress( $order->getCustomer()->getAddress() );
		$order->setInvoiceAddress( $order->getCustomer()->getAddress() );
		
		//sauvegarde
		
		$order->save(); //bidouille
		
		//ajout du chèque-cadeau de façon brutale et non sécurisée :

		$fields = array(
		
			"idorder"			=> $order->getId(),
			"idrow"				=> 1,
			"quantity"			=> 1,
			"external_quantity" => 0,
			"unit_price" 		=> $amount,
			"discount_price" 	=> $amount,
			"unit" 				=> DBUtil::getDefaultValue( "unit", "detail" ),
			"lot"				=> DBUtil::getDefaultValue( "unit", "detail" ),
			"designation" 		=> "Chèque-cadeau",
			"summary"			=> "Chèque-cadeau",
			"delivdelay"        => 0,
			"usepdf" 			=> 0,
			"useimg"			=> 0,
			"lastupdate" 		=> date( "Y-m-d H:i:s" ),
			"username" 			=> User::getInstance()->get( "login" )
			
		);
		
		foreach( $fields as $key => $value )
			$fields[ $key ] = DBUtil::quote( $value );
			
		$query = "
		INSERT INTO `order_row` ( `" . implode( "`,`", array_keys( $fields ) ) . "` )
		VALUES( " . implode( ",", array_values( $fields ) ) . " )";
		
		$rs =& DBUtil::query( $query );
		
		$order = new Order( $order->getId() ); //bidouille
		
		//assurance crédit
		
		$order->insure();
		
		$order->save(); //bidouille
		
		return $order;
		
	}

	//----------------------------------------------------------------------------
	/**
	 * Créé une facture pour un chèque cadeau
	 * @todo adapter le logiciel pour ce genre de service
	 * @static
	 * @param Order $order la commande à facturer
	 * @return void
	 */
	public static function createInvoiceFromGiftTokenOrder( Order $order ){
	
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		include_once( dirname( __FILE__ ) . "/RegulationsBuyer.php" );
		
		$idorder = $order->getId();
		
		//si rien à facturer
		
		if( !count( $order->getItemCount() ) ){
			
			trigger_error( "Impossible de récupérer les lignes articles à facturer pour la commande n°$idorder", E_USER_ERROR );
			exit();
				
		}
		
		//créer la facture
		
		$idbuyer 	= $order->get( "idbuyer" );
		$idcontact 	= $order->get( "idcontact" );
		
		$invoice = self::createInvoice( $idorder, $idbuyer, $idcontact );

		//ajout des lignes article à facturer
		
		$it = $order->getItems()->iterator();
		while( $it->hasNext() ){
			
			$orderItem =& $it->next();

			$invoice->addOrderItem( $orderItem );
				
		}

		$invoice->set( "total_charge", 			0.0 );
		$invoice->set( "total_charge_ht", 			0.0 );
		$invoice->set( "charge_vat", 				0.0 );
		$invoice->set( "charge_vat_rate", 			0.0 );
		$invoice->set( "total_discount" , 			0.0 );
		$invoice->set( "total_discount_amount", 	0.0 );
		$invoice->set( "billing_rate" , 			0.0 );
		$invoice->set( "billing_amount" , 			0.0 );
		$invoice->set( "down_payment_type" , 		"amount" );
		$invoice->set( "down_payment_value" , 		0.0 );
		$invoice->set( "charge_auto", 				0 );
		$invoice->set( "total_charge_auto", 		0 );
		
		//propriétés de la facture héritées de la commande

		include_once( dirname( __FILE__ ) . "/Payment.php" );
		
		$invoice->set( "iderp", 				$order->get( "iderp" ) );
		$invoice->set( "idbuyer_erp", 			$order->get( "idbuyer_erp" ) );
		$invoice->set( "idbilling_adress_erp", $order->get( "idbilling_adress_erp" ) );
		$invoice->set( "idpayment", 			$order->get( "idpayment" ) );
		$invoice->set( "idpayment_delay", 		$order->get( "idpayment_delay" ) );
		$invoice->set( "send_by", 				$order->get( "send_by" ) );
		$invoice->set( "n_order", 				$order->get( "n_order" ) );
		$invoice->set( "factor", 				$order->get( "factor" ) );
		$invoice->set( "svg_mail", 			$order->get( "svg_mail" ) );
		$invoice->set( "svg_fax", 				$order->get( "svg_fax" ) );
		$invoice->set( "pro_forma", 			$order->get( "pro_forma" ) );
		$invoice->set( "global_delay", 		$order->get( "global_delay" ) );
		$invoice->set( "deliv_payment", 		Payment::getPaymentDelayDate( date( "Y-m-d H:i:s" ), $order->get( "idpayment_delay" ) ) );
		$invoice->set( "status", 				Invoice::$STATUS_PAID );
		$invoice->set( "balance_amount", 		$order->getNetBill() );
		$invoice->set( "balance_date", 		$order->get( "balance_date" ) );
		$invoice->set( "rebate_type", 			$order->get( "rebate_type" ) );
		$invoice->set( "rebate_value", 		$order->get( "rebate_value" ) );

		//adresses de livraison et de facturation
		
		$invoice->setInvoiceAddress( $order->getCustomer()->getAddress() );
		$invoice->setForwardingAddress( $order->getCustomer()->getAddress() );

		//assurance crédit
		
		$invoice->insure( $order->get( "insurance" ) == 1 );
		
		//sauvegarde
		
		$invoice->save();

		//paiement direct
		
		RegulationsBuyer::createPayment( $invoice );
		
		return $invoice;
	
	}
	
	//----------------------------------------------------------------------------
//   gestion des indexations
public static function Indexations(&$table, $n_index_racine = 0){

//on récupère les données 
if( $n_index_racine == 0){
    $query = "
    			SELECT  *
    			FROM code_indexation
    			WHERE `table` = '$table'";// AND n_subindexation IS NULL
    			//";
    	$rs =& DBUtil::query( $query );
    	$code_indexation = $rs->fields( "code_indexation" );
    	$n_indexation = $rs->fields( "n_indexation" );
    	$new_index = $n_indexation + 1;
    	
    //Mettre à jour code_indexation
    
    $q = "
    			UPDATE code_indexation
    			SET n_indexation = '$new_index'
    			WHERE `table` = '$table'
    			";
    	$rsq =& DBUtil::query( $q );
    	
    // return le dernier incrément + code_indexation si existe
    if(isset($code_indexation))
    $indexation = $code_indexation.''.$n_indexation;
    else
    $indexation = $n_indexation;
}
else{ // Duplicata ou copy
    
     $query = "
    			SELECT  coalesce(n_subindexation,0) + 1 as new_subindex
    			FROM code_indexation
    			WHERE `table` = '$table' AND  n_indexation =".$n_index_racine;
  			
              
    	$rs =& DBUtil::query( $query );    	
        $new_subindex = $rs->fields( "new_subindex" );
        
        $query = "
    			SELECT  *
    			FROM code_indexation
    			WHERE `table` = '$table' AND n_subindexation IS NULL";
          
         $rs =& DBUtil::query( $query );       
   	    $code_indexation = $rs->fields( "code_indexation" );
    
        
         //Mettre à jour la table code_indexation
         if( $new_subindex > 1) {   
            $q = "
        			UPDATE code_indexation
        			SET n_subindexation = '$new_subindex'
        			WHERE `table` = '$table' AND  n_indexation =".$n_index_racine;
        }
        else{
            
            $begin_date = date('Y-m-d');
            $username = User::getInstance()->get( "login" );
            $lastupdate = date('Y-m-d');
            
            $q = "
        			INSERT INTO code_indexation(`table`, n_indexation, n_subindexation, code_indexation, begin_date ,username ,lastupdate )
        			VALUES('$table' ,$n_index_racine ,1, '$code_indexation','$begin_date','$username','$lastupdate')";                    
        			
        }		
        	
        $rsq =& DBUtil::query( $q );
       
        
         if(isset($code_indexation))
            $indexation = $code_indexation.''.$n_index_racine.'-'.$new_subindex;
        else
            $indexation = $n_index_racine.'-'.$new_subindex;
    	
}
return $indexation;

}

 
	//fonction temporaire 
	function createMyRegulation($iddocument,$typeDoc,$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,$fromDP=0){
		
				
				$table= 'regulations_buyer';
	 $maxNumRegul = self::Indexations( $table );
				
		$insertReg=			
		"INSERT INTO regulations_buyer (
			idregulations_buyer,
			idbuyer,
			iduser,
			amount,
			idbank,
			piece_number,
			comment,
			idpayment,
			payment_date,
			accept,
			lastupdate,
			username,
			from_down_payment
		) VALUES (
			'$maxNumRegul',
			'$idbuyer',
			'" . User::getInstance()->getId() . "',
			$amount,
			$idbank,
			'".addslashes($piece)."',
			'".addslashes($comment)."',
			$idpayment,
			'$payment_date',
			1,
			NOW(),
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . ",
			$fromDP
		)";
	
		DButil::query($insertReg);
		
		$setline = "";
		
		//maj devis ou commande
		if($fromDP){
			$setline = "down_payment_idregulation = '$maxNumRegul', 
						down_payment_idbank = '$idbank', 
						down_payment_comment = '".addslashes($comment)."', 
						down_payment_piece = '".addslashes($piece)."', 
						down_payment_idpayment = '$idpayment',
						down_payment_date = '$payment_date'";
		}else{
			$setline = "cash_payment_idregulation= '$maxNumRegul',
						cash_payment_idbank = '$idbank', 
						payment_comment = '".addslashes($comment)."', 
						cash_payment_piece = '".addslashes($piece)."', 
						payment_idpayment = '$idpayment',
						balance_date = '$payment_date'";
		}
		$query = "UPDATE `$typeDoc` SET $setline where id$typeDoc = '$iddocument'";	
		DButil::query($query);
		
		// si c'est une commande on doit aussi transferer au devis
		if($typeDoc=='order'){
		// ------- Mise à jour de l'id gestion des Index  #1161
			$rsidestimate = DBUtil::query("SELECT idestimate FROM `order` WHERE idorder='$iddocument' LIMIT 1");
			
			if($rsidestimate->RecordCount() && $rsidestimate->fields('idestimate')!=0 && $rsidestimate->fields('idestimate')!='' ){
				$idestimateassocie = $rsidestimate->fields('idestimate');
				$query = "UPDATE `estimate` SET $setline where idestimate = '$idestimateassocie'";
				DButil::query($query);
			}
		}
		
		// de même si le devis a une commande ce qui a mon avis est rare et stupide de permettre de passer en commande
		if($typeDoc=='estimate'){
			$rsido = DBUtil::query("SELECT idorder FROM `order` WHERE idestimate='$iddocument' LIMIT 1");
			// ------- Mise à jour de l'id gestion des Index  #1161
			if($rsido->RecordCount() && $rsido->fields('idorder')!='' && $rsidestimate->fields('idorder')!=0 ){
				$idorderassocie = $rsido->fields('idorder');
				// ------- Mise à jour de l'id gestion des Index  #1161
				$query = "UPDATE `order` SET $setline where idorder = '$idorderassocie'";
				DButil::query($query);
			}
		}
		
		return $maxNumRegul;
	}
}
?>
