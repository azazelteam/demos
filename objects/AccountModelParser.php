<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des modèles d'écritures comptables
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );

class AccountModelParser{
	
	private $dateFormat		= "d-m-Y";
	private $relationTypes	= array();
	private $recordTypes	= array(
		"billing_buyer"			=> "Facture clt",
		"regulations_buyer"		=> "Règlement clt",
		"billing_supplier"		=> "Facture frs",
		"regulations_supplier"	=> "Règlement frs",
		"credits"				=> "Avoir clt",
		"credit_supplier"		=> "Avoir frs"
	);
	
	/**
	 * Données membres
	 */
	
	private $model;			//Modèle d'écritures
	private $count;			//Row count
	private $index;			//Current row index
	private $field;			//Current field
	private $string;		//Current string
	private $parsedArray;
	
	/**
	 * Contructeur
	 */
	function __construct( $model, $count ){
		
		$this->model		= $model;
		$this->count		= $count;
		$this->index		= 0;
		$this->field		= null;
		$this->string		= null;
		
		$this->parsedArray	= array(
			
			array(
				
				"account"			=> "",
				"record"			=> "",
				"record_type"		=> "",
				"third_party"		=> "",
				"third_party_type"	=> "",
				"label"				=> "",
				"date"				=> "",
				"payment_date"		=> "",
				"payment"			=> "",
				"amount"			=> "",
				"amount_type"		=> ""
				
			)
			
		);
		
		//Type de tiers
		
		$rs =& DBUtil::query( "SELECT relation_type FROM account_plan WHERE relation_type != '' GROUP BY relation_type ORDER BY COUNT( relation_type ) DESC" );
		
		while( !$rs->EOF ){
			
			$this->relationTypes[] = $rs->fields( "relation_type" );
			
			$rs->MoveNext();
			
		}
		
	}
	
	/**
	 * Getter
	 */
	
	public function get( $key ){ return $this->parsedArray[ $this->index ][ $key ]; }
	
	/**
	 * Itérateur
	 * @param void
	 * @return int $this->index l'indice de la ligne courante
	 */
	public function moveNext(){
		
		if( $this->index < $this->count - 1 ){
			
			$this->index++;
			
			$this->parsedArray[ $this->index ] = array(
				
				"account"			=> "",
				"record"			=> "",
				"record_type"		=> "",
				"third_party"		=> "",
				"third_party_type"	=> "",
				"label"				=> "",
				"date"				=> "",
				"payment_date"		=> "",
				"payment"			=> "",
				"amount"			=> "",
				"amount_type"		=> ""
				
			);
			
		}
		
		return $this->index;
		
	}
	
	/**
	 * Sauvegarde la valeur actuelle
	 * @param void
	 * @return void
	 */
	public function save(){
		
		$this->parsedArray[ $this->index ][ $this->field ] = $this->string;
		
	}
	
	/**
	 * Sauvegarde la valeur actuelle
	 * @param void
	 * @return string $error les messages d'erreur
	 */
	public function insert(){
		
		global $GLOBAL_START_PATH;
		
		include_once( "$GLOBAL_START_PATH/objects/AccountPlan.php" );
		
		$error = array();
		
		$query = "SELECT diary_code FROM accounts_models, diary_code WHERE accounts_models.iddiary_code = diary_code.iddiary_code AND idmodel = $this->model";
		
		$rs = DBUtil::query( $query );
		
		$action			= "from_model";
		$account_plan_1	= $rs->fields( "diary_code" );
		$account_plan_2	= "D";
		
		
		$query = "SELECT * FROM accounts_models_row WHERE idmodel = $this->model ORDER BY idrow ASC";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			trigger_error( "Impossible de récupérer les modèles d'écritures", E_USER_ERROR );
		$this->count = $rs->RecordCount();
		$i = 0;
		while( !$rs->EOF ){
			
			$idaccount_plan[ $i ]	= $this->parseOutputAccount( $rs->fields( "idaccount_plan" ) );
			$idrelation[ $i ]		= $this->parseOutputThirdParty( $rs->fields( "idrelation" ) );
			$relation_type[ $i ]	= $this->parseOutputThirdPartyType( $rs->fields( "relation_type" ) );
			$idaccount_record[ $i ]	= $this->parseOutputRecord( $rs->fields( "idrecord" ) );
			$record_type[ $i ]		= $this->parseOutputRecordType( $rs->fields( "record_type" ) );
			$date[ $i ]				= Util::dateFormatUs( $this->parseOutputDate( $rs->fields( "date" ) ) );
			$amount[ $i ]			= $this->parseOutputAmount( $rs->fields( "amount" ), $rs->fields( "amount_type" ) );
			$amount_type[ $i ]		= $this->parseOutputAmount( $rs->fields( "amount" ), $rs->fields( "amount_type" ), "debit" ) == $amount[ $i ] ? "debit" : "credit";
			$payment[ $i ]			= Util::doNothing($this->parseOutputPayment( $rs->fields( "payment" ) ));
			$payment_date[ $i ]		= Util::dateFormatUs( $this->parseOutputPaymentDate( $rs->fields( "payment_date" ) ) );
			$designation[ $i ]		= $this->parseOutputLabel( $rs->fields( "label" ) ); //Ne pas mettre avant les autres traitements car le libellé peut dépendre des valeurs des champs précédents
			$code_eu[ $i ]			= DBUtil::getDBValue( "accounting_plan_eu", "accounting_plan", "idaccounting_plan", $idaccount_plan[ $i ] );
			$code_vat[ $i ]			= "";
			$account_plan_3[ $i ]	= $amount_type[ $i ] == "debit" ? "D" : "C";
			$account_plan_4[ $i ]	= DBUtil::getDBValue( "forward_carry", "accounting_plan", "idaccounting_plan", $idaccount_plan[ $i ] );
			
			if( !DBUtil::getDBValue( "idaccounting_plan", "accounting_plan", "idaccounting_plan", $idaccount_plan[ $i ] ) )
				$error[] = "Le numéro de compte '" . $idaccount_plan[ $i ] . "' n'existe pas";
			
			/*if( empty( $idrelation[ $i ] ) )
				$error[] = "Aucun numéro de compte tiers spécifié";
			
			if( !in_array( $relation_type[ $i ], $this->relationTypes ) )
				$error[] = "Type de tiers inconnu";
			
			if( empty( $idaccount_record[ $i ] ) )
				$error[] = "Aucun numéro de pièce spécifié";
			
			if( !array_key_exists( $record_type[ $i ], $this->recordTypes ) )
				$error[] = "Type de pièce inconnu";*/
			
			if( empty( $date[ $i ] ) )
				$error[] = "Aucune date spécifiée";
			
			if( !preg_match( "/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $date[ $i ] ) )
				$error[] = "Format de date erronné";
			
			if( $date == "0000-00-00" )
				$error[] = "Aucune date d'écriture spécifiée";
			
			//if( $amount[ $i ] == 0 || !is_float( $amount[ $i ] ) )
			if( !is_float( $amount[ $i ] ) )
				$error[] = "Montant incorrect";
			
			if( !in_array( $amount_type[ $i ], array( "debit", "credit" ) ) )
				$error[] = "Type de montant (débit/crédit) inconnu";
			
			if( !empty( $payment_date[ $i ] ) && !preg_match( "/[0-9]{4}-[0-9]{2}-[0-9]{2}/", $payment_date[ $i ] ) )
				$error[] = "Format de date de paiement erronné";
			
			if( empty( $designation[ $i ] ) )
				$error[] = "Aucun libellé spécifié";
			
			$rs->MoveNext();
			$this->moveNext();
			$i++;
			
		}
//	print_r($payment);
		if( !count( $error ) ){
			
			for( $i = 0 ; $i < $this->count ; $i++ ) {
				if( $amount[ $i ] != 0 ) {
					AccountPlan::insertRow( $idaccount_plan[ $i ], $idrelation[ $i ], $relation_type[ $i ], $idaccount_record[ $i ], $record_type[ $i ], $date[ $i ], $amount[ $i ], $amount_type[ $i ], $designation[ $i ], $code_eu[ $i ], $code_vat[ $i ], $payment[ $i ], $payment_date[ $i ], $account_plan_1, $account_plan_2, $account_plan_3[ $i ], $account_plan_4[ $i ], $action );
				}
            }
			
		}
		
		return $error;
		
	}
	
	/**
	 * Parseur pour le tag %today%
	 * @param void
	 * @return void
	 */
	private function todayParser(){
		
		$this->string = str_replace( "%today%", $_POST[ $this->field . "_" . $this->index ], $this->string );
		
	}
	
	/****************** Parseur pour la saisie ********************/
	
	/**
	 * Parseur par défaut appliqué à toutes les expressions
	 * @param void
	 * @return void
	 */
	private function defaultInputParser(){
		
		global $GLOBAL_START_URL;
		$this->string = str_replace( "%copy%", "<img src=\"$GLOBAL_START_URL/images/back_office/content/copy.png\" alt=\"copie\" />", $this->string );

	}
	
	/**
	 * Parseur pour le tag %manual% en champ texte
	 * @param string $options les options XHTML à rajouter au champ texte
	 * @return void
	 */
	private function manualTextInputParser( $options = "" ){
		
		$this->string = str_replace( "%manual%", "<input type=\"text\" name=\"" . $this->field . "_" . $this->index . "\" id=\"" . $this->field . "_" . $this->index . "\" class=\"textInput model_$this->field model_" . $this->field . "_$this->index\"$options />", $this->string );
		
	}
	
	/**
	 * Parseur pour le tag %manual% en champ texte pour les montants
	 * @param string $options les options XHTML à rajouter au champ texte
	 * @return void
	 */
	private function manualAmountTextInputParser( $type, $options = "" ){
		
		$this->field = "amount_$type";
		
		$this->manualTextInputParser( $options );
		
		$this->field = "amount";
		
	}
	
	/**
	 * Parseur pour le tag %manual% en datepicker
	 * @param string $callback la fonction de callback du calendrier
	 * @return void
	 */
	private function manualDateInputParser( $callback = "" ){
		
		if( strstr( "%manual%", $this->string ) ){
			
			$this->string = "<input type=\"text\" name=\"" . $this->field . "_" . $this->index . "\" id=\"" . $this->field . "_" . $this->index . "\" class=\"calendarInput model_$this->field\" />";
			$this->string .= DHTMLCalendar::calendar( $this->field . "_" . $this->index, false, $callback );
			
		}
		
	}
	
	/**
	 * Parseur du numéro de compte
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputAccount( $string ){
		
		$this->field = "account";
		$this->string = $string;
		
		$this->defaultInputParser();
		$matches = array();
		
		if( preg_match( "/([0-9]+)%/", $string, $matches ) ){
			
			$this->string = "<select name=\"" . $this->field . "_" . $this->index . "\">";
			$this->string .= "<option value=\"\">-</option>";
			
			$rs =& DBUtil::query( "SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_parent = '" . $matches[ 1 ] . "' ORDER BY idaccounting_plan ASC" );
			
			while( !$rs->EOF ){
				
				$this->string .= "<option value=\"" . $rs->fields( "idaccounting_plan" ) . "\">" . $rs->fields( "idaccounting_plan" ) . "</option>";
				
				$rs->MoveNext();
				
			}
			
			$this->string .= "</select>";
			
		}
		
		if( $string == "%manual%" ){
			
			$lang = User::getInstance()->getLang();
			
		    $this->string = "<select name=\"" . $this->field . "_" . $this->index . "\" id=\"" . $this->field . "_" . $this->index . "\" class=\"fullWidth\">";
			$this->string .= "<option value=\"\"></option>";
			
			$rs =& DBUtil::query( "SELECT idaccounting_plan, accounting_plan$lang AS description, accounting_plan_parent FROM accounting_plan ORDER BY idaccounting_plan ASC" );
			
			while( !$rs->EOF() ){
				
				$rs2 =& DBUtil::query( "SELECT COUNT( * ) AS children FROM accounting_plan WHERE accounting_plan_parent = '" . $rs->fields( "idaccounting_plan" ) . "'" );
				
				$bold = $rs->fields( "accounting_plan_parent" ) == 0 ? " style=\"font-weight:bold;\"" : "";
				$idaccounting_plan = $rs2->fields( "children" ) > 0 ? "" : $rs->fields( "idaccounting_plan" );
				
				$this->string .= "<option value=\"$idaccounting_plan\"$bold>" . $rs->fields( "idaccounting_plan" ) . " - " . $rs->fields( "description" ) . "</option>\n";
				
				$rs->MoveNext();
				
			}
			
			$this->string .= "</select>";
			
		}
		
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de date
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputDate( $string ){
		
		$this->field = "date";
		$this->string = $string;
		
		$this->defaultInputParser();
		$this->manualDateInputParser();
		$this->todayParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de numéro de pièce
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputRecord( $string ){
		
		$this->field = "record";
		$this->string = $string;
		
		$this->defaultInputParser();
		$this->manualTextInputParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de type de pièce
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputRecordType( $string ){
		
		$this->field = "record_type";
		$this->string = $string;
		
		$this->defaultInputParser();
		
		if( strstr( "%manual%", $string ) ){
			
			$this->string = "<select name=\"" . $this->field . "_" . $this->index . "\">";
			$this->string .= "<option value=\"\">-</option>";
			
			foreach( $this->recordTypes as $value => $name )
				$this->string .= "<option value=\"$value\">$name</option>";
			
			$this->string .= "</select>";
			
		}
		elseif( in_array( $string, $this->relationTypes ) ){
			
			//@todo: prendre la valeur du tableau $this->relationTypes
			$this->string = Dictionnary::translate( "account_model_$string" );
			
		}
		
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de numéro de tiers
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputThirdParty( $string ){
		
		$this->field = "third_party";
		$this->string = $string;
		
		$this->defaultInputParser();
		$this->manualTextInputParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de type de tiers
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputThirdPartyType( $string ){
		
		$this->field = "third_party_type";
		$this->string = $string;
		
		$this->defaultInputParser();
		
		if( strstr( "%manual%", $string ) ){
			
			$this->string = "<select name=\"" . $this->field . "_" . $this->index . "\">";
			$this->string .= "<option value=\"\">-</option>";
			
			foreach( $this->relationTypes as $record )
				$this->string .= "<option value=\"$record\">" . Dictionnary::translate( "account_model_$record" ) . "</option>";
			
			$this->string .= "</select>";
			
		}
		elseif( in_array( $string, $this->relationTypes ) ){
			
			$this->string = Dictionnary::translate( "account_model_$string" );
			
		}
		
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de mode de paiement
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputPayment( $string ){
		
		$lang = User::getInstance()->getLang();
		
		$this->field = "payment";
		$this->string = $string;
		
		$this->defaultInputParser();
		if( strstr( "%manual%", $string ) ){
			
			$this->string = "<select name=\"" . $this->field . "_" . $this->index . "\">";
			$this->string .= "<option value=\"\">-</option>";
			
			$rs =& DBUtil::query( "SELECT name$lang AS name FROM payment ORDER BY name$lang" );
			
			while( !$rs->EOF ){
				
				$this->string .= "<option value=\"" . $rs->fields( "name" ) . "\">" . $rs->fields( "name" ) . "</option>";
				
				$rs->MoveNext();
				
			}
			
			$this->string .= "</select>";
			
		}
		
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de date de paiement
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputPaymentDate( $string ){
		
		$this->field = "payment_date";
		$this->string = $string;
		
		$this->defaultInputParser();
		$this->manualDateInputParser();
		$this->todayParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de montant
	 * @param string $string la chaine à parser
	 * @param string $modelType le type de montant (débit/crédit) du modèle stocké dans la base
	 * @param string $columnType le type de montant (débit/crédit) de la colonne
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputAmount( $string, $modelType, $columnType ){
		
		if( $modelType != $columnType && $modelType != "" )
			return;
		
		$this->field = "amount_type";
		$this->string = $modelType;
		$this->save();
		
		$this->field = "amount";
		$this->string = $string;
		
		$this->defaultInputParser();
		
		if( strstr( "%manual%", $string ) )
			$this->manualAmountTextInputParser( $columnType );
		
		if( strstr( "%balance%", $string ) )
			$this->string = "<img src=\"$GLOBAL_START_URL/images/back_office/content/balance.png\" alt=\"balance\" />";
		
		if( preg_match( '/\$\([0-9%.*\/+-]*\)/', $string ) )
			$this->string = "<img src=\"$GLOBAL_START_URL/images/back_office/content/calculator.png\" alt=\"calcul\" />";
		
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de libellé
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseInputLabel( $string ){
		
		$this->field = "label";
		$this->string = $string;
		
		$this->defaultInputParser();
		$this->manualTextInputParser();
		
		//Remplacement des clés
		foreach( array_keys( $this->parsedArray[ $this->index ] ) as $key ){
			
			$text = Dictionnary::translate( "account_model_" . $key );
			$key_img_src = "$GLOBAL_START_URL/administration/images/tag.php?label=" . urlencode( htmlentities( $text ) );
			$img = "<img src=\"$key_img_src\" alt=\"$text\" />";
			
			$this->string = str_replace( "%$key%", $img, $this->string );
			
		}
		
		$this->save();
		
		return $this->string;
		
	}
	
	/****************** Parseur pour l'enregistrement ********************/
	
	/**
	 * Parseur par défaut appliqué à toutes les expressions
	 * @param void
	 * @return void
	 */
	private function defaultOutputParser(){
		
		global $GLOBAL_START_URL;
		if( strstr( "%copy%", $this->string ) ){
			$this->string = $this->parsedArray[ $this->index - 1 ][ $this->field ];
        }elseif( strstr( "%manual%", $this->string ) && isset( $_POST[ $this->field . "_" . $this->index ] ) ){
			$this->string = $_POST[ $this->field . "_" . $this->index ];
        }elseif(isset($_POST[ $this->field . "_" . $this->index ])){
            $this->string =  $_POST[ $this->field . "_" . $this->index ];
        }

	}
	
	/**
	 * Parseur du numéro de compte
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputAccount( $string ){
		
		$this->field = "account";
		$this->string = $string;
		
		$this->defaultOutputParser();
		
		$matches = array();
		
		if( preg_match( "/([0-9]+)%/", $string ) && isset( $_POST[ $this->field . "_" . $this->index ] ) )
			$this->string = $_POST[ $this->field . "_" . $this->index ];
		
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de date
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputDate( $string ){
		
		$this->field = "date";
		$this->string = $string;
		
		$this->defaultOutputParser();
		$this->todayParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de numéro de pièce
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputRecord( $string ){
		
		$this->field = "record";
		$this->string = $string;
		
		$this->defaultOutputParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de type de pièce
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputRecordType( $string ){
		
		$this->field = "record_type";
		$this->string = $string;
		
		$this->defaultOutputParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de numéro de tiers
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputThirdParty( $string ){
		
		$this->field = "third_party";
		$this->string = $string;
		
		$this->defaultOutputParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de type de tiers
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputThirdPartyType( $string ){
		
		$this->field = "third_party_type";
		$this->string = $string;
		
		$this->defaultOutputParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de mode de paiement
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputPayment( $string ){
		$lang = User::getInstance()->getLang();
		$this->field = "payment";
		$this->string = $string;
		$this->defaultOutputParser();
        $this->save();

		return $this->string;
		
	}
	
	/**
	 * Parseur de date de paiement
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputPaymentDate( $string ){
		
		$this->field = "payment_date";
		$this->string = $string;
		
		$this->defaultOutputParser();
		$this->todayParser();
		$this->save();
		
		return $this->string;
		
	}
	
	/**
	 * Parseur de montant
	 * @param string $string la chaine à parser
	 * @param string $modelType le type de montant (débit/crédit) du modèle stocké dans la base
	 * @param string $columnType le type de montant (débit/crédit) de la colonne
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputAmount( $string, $modelType, $columnType = "" ){

		$this->parsedArray[ $this->index ][ "amount" ] = 0.0;
		$this->parsedArray[ $this->index ][ "amount_type" ] = $modelType;
		
		if( strstr( "%copy%", $string ) ){
			
			$this->parsedArray[ $this->index ][ "amount" ] = $this->parsedArray[ $this->index - 1 ][ "amount" ];
			$this->parsedArray[ $this->index ][ "amount_type" ] = $this->parsedArray[ $this->index - 1 ][ "amount_type" ];
			
		}
		elseif( strstr( "%manual%", $string ) ){
			
			if( isset( $_POST[ "amount_debit_" . $this->index ] ) && !empty( $_POST[ "amount_debit_" . $this->index ] ) ){
				
				$this->parsedArray[ $this->index ][ "amount" ] = floatval( $_POST[ "amount_debit_" . $this->index ] );
				$this->parsedArray[ $this->index ][ "amount_type" ] = "debit";
				
			}
			elseif( isset( $_POST[ "amount_credit_" . $this->index ] ) && !empty( $_POST[ "amount_credit_" . $this->index ] ) ){
				
				$this->parsedArray[ $this->index ][ "amount" ] = floatval( $_POST[ "amount_credit_" . $this->index ] );
				$this->parsedArray[ $this->index ][ "amount_type" ] = "credit";
				
			}
			
		}
		elseif( strstr( "%balance%", $string ) ){
			
			$balance = 0;
			
			for( $i = 0 ; $i < $this->index ; $i++ ){
				
				$amount = $this->parsedArray[ $i ][ "amount" ];
				$modelType = $this->parsedArray[ $i ][ "amount_type" ];
				
				if( in_array( $modelType, array( "debit", "credit" ) ) )
					$balance += $modelType == "debit" ? $amount : -$amount;
				
			}
			
			$this->parsedArray[ $this->index ][ "amount" ] = abs( $balance );
			$this->parsedArray[ $this->index ][ "amount_type" ] = $balance > 0 ? "credit" : "debit";
			
		}
		elseif( preg_match( '/\$\([0-9%.*\/+-]*\)/', $string ) ){
			
			$string = substr( $string, 2, strlen( $string ) - 3 );
			
			$string = preg_replace( "/%([0-9]+)%/", '$this->parsedArray[ $1 - 1 ][ "amount" ]', $string );
			
			eval( '$amount = round(' . $string . ', 2 );' );
			
			$this->parsedArray[ $this->index ][ "amount" ] = $amount;
			
		}
		else{
			
			$this->parsedArray[ $this->index ][ "amount" ] = floatval( $string );
			$this->parsedArray[ $this->index ][ "amount_type" ] = $modelType;
			
		}
		
		if( $this->parsedArray[ $this->index ][ "amount_type" ] == $columnType || $columnType == "" )
			return $this->parsedArray[ $this->index ][ "amount" ];
		
		return;
		
	}
	
	/**
	 * Parseur de libellé
	 * @param string $string la chaine à parser
	 * @return string $this->string la chaine parsée
	 */
	public function parseOutputLabel( $string ){
		
		$this->field = "label";
		$this->string = $string;
		
		$this->defaultOutputParser();
		
		//Remplacement des clés
		
		foreach( array_keys( $this->parsedArray[ $this->index ] ) as $key )
			$this->string = str_replace( "%$key%", $this->parsedArray[ $this->index ][ $key ], $this->string );
		
		$this->save();
		
		return $this->string;
		
	}
	
	/****************** Parseur pour la modification ********************/
	
	/**
	 * Parseur par défaut appliqué à toutes les expressions
	 * @param void
	 * @return void
	 */
	private function defaultEditionParser( $string ){
		
		global $GLOBAL_START_URL;
		
		//@todo: images dans label
		
		$string = str_replace( "%today%", '<img src="' . $GLOBAL_START_URL . '/images/back_office/content/calendar.jpg" alt="Date" title="Date du jour" class="draggable todayImage" tag="%today%" />', $string );
		$string = str_replace( "%copy%", '<img src="' . $GLOBAL_START_URL . '/images/back_office/content/copy.png" alt="Copier" title="Copier ligne précédente" class="draggable copyImage" tag="%copy%" />', $string );
		$string = str_replace( "%manual%", '<img src="' . $GLOBAL_START_URL . '/images/back_office/content/manual.png" alt="Manuel" title="Saisie manuelle" class="draggable manualImage" tag="%manual%" />', $string );
		$string = str_replace( "%balance%", '<img src="' . $GLOBAL_START_URL . '/images/back_office/content/balance.png" alt="Balance" title="Balance" class="draggable balanceImage" tag="%balance%" />', $string );
		
		if( preg_match( '/\$\([0-9%.*\/+-]*\)/', $string ) )
			$string = '<img src="' . $GLOBAL_START_URL . '/images/back_office/content/calculator.png" alt="Calcul" title="Calcul" class="draggable calculateImage" tag="%calculate%" />';
		
		return $string;
		
	}
	
	public function parseAccountEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parseRecordEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parseRecordTypeEdition( $string ){
		
		return Dictionnary::translate( $this->defaultEditionParser( $string ) );
		
	}
	
	public function parseThirdPartyEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parseThirdPartyTypeEdition( $string ){
		
		return Dictionnary::translate( $this->defaultEditionParser( $string ) );
		
	}
	
	public function parseLabelEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parseDateEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parsePaymentDateEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parsePaymentEdition( $string ){
		
		return $this->defaultEditionParser( $string );
		
	}
	
	public function parseDebitEdition( $amount, $type ){
		
		if( $type == "debit" || $type == "" )
			return $this->defaultEditionParser( $amount );
		
	}
	
	public function parseCreditEdition( $amount, $type ){
		
		if( $type == "credit" || $type == "" )
			return $this->defaultEditionParser( $amount );
		
	}
	
}

?>