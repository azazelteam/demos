<?php

 /*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object droits d'accès au back-office
 */

include_once( dirname( __FILE__ ) . "/DBUtil.php" );


/**
 * Autorise ou non l'accès aux outils d'administration
 */
class UserAccessControl{
	
	//----------------------------------------------------------------------------
	
	private function __construct(){} //static keyword not implemented for classes
	
	//----------------------------------------------------------------------------
	
	public static function allowAccess( $iduser, $idmenu ){
		
		//tempo
		//Il faudrait que tous les fichiers (y compris les popups) aient une entrée dans les tables menu et access_rights, ce qui n'est pas le cas
		return true;
		
		//vérification du login
		
		if( !$iduser || !$idmenu )
			return false;

		//vérifier les droits d'accès à l'outil
		
		return self::allowUserAccess( $iduser, $idmenu ) && self::allowRemoteAddrAccess( $idmenu );
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Vérifie si un utilisateur donné à le droit d'accéder à un outil donné
	 * @param int $iduser l'identifiant de l'utilisateur
	 * @param int $idmenu l'identifiant de l'outil
	 * @access private
	 * @static
	 * @return true si l'utilisateur à accès à l'outil, sinon false
	 */
	private static function allowUserAccess( $iduser, $idmenu ){

		$groups = array();

		// gathering user groups
		$rs =& DBUtil::query( "SELECT idgroup FROM user_group WHERE iduser = $iduser GROUP BY idgroup" );

		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de vérifier les droits d'accès. Utilisateur inconnu." );

		while( !$rs->EOF() ) {
			$groups[] = $rs->fields( "idgroup" );
			$rs->MoveNext();
		}

		// getting user rights
		$query = "SELECT idmenu, ExpDIEdR FROM access_rights WHERE idgroup IN (".implode( ", ", $groups ).") AND idmenu = $idmenu ORDER BY ExpDIEdR DESC LIMIT 1";
		$rs =& DBUtil::query( $query );

		if( $rs === false )
			die( "Impossible de vérifier les droits d'accès" );

		if( $rs->fields( "ExpDIEdR" ) >= 1 )
			return true;
		else
			return false;

	}

	//---------------------------------------------------------------------------------------
	
	/**
	 * Vérifie l'adresse IP de l'utilisateur est autorisée à accéder à un outil donné
	 * @param int $idmenu l'identifiant de l'outil
	 * @access private
	 * @static
	 * @return true si l'adresse IP est autorisée, sinon false
	 */
	private static function allowRemoteAddrAccess( $idmenu ){
	
		$rs =& DBUtil::query( "SELECT * FROM menu_access WHERE idmenu = '$idmenu'" );
		
		if( $rs === false )
			die( "Impossible de vérifier les droits d'accès" );
			
		if( !$rs->RecordCount() )
			return true;
		
		$allowAccess = false;
		while( !$allowAccess && !$rs->EOF() ){
			
			$remote_addr_start 	= $rs->fields( "remote_addr_start" );
			$remote_addr_end 	= $rs->fields( "remote_addr_end" );
			
			$allowAccess |= UserAccessControl::isRemoteAddrInRange(  $remote_addr_start, $remote_addr_end );
			
			$rs->MoveNext();
			
		}
		
		return $allowAccess;
		
	}
	
	//---------------------------------------------------------------------------------------
	
	/**
	 * @access private
	 * @static
	 */
	private static function isRemoteAddrInRange(  $remote_addr_start, $remote_addr_end ){
	
		$bytePattern 	= "/[0-9]{1,3}/";
		
		$startBytes		= explode( ".", $remote_addr_start );
		$endBytes		= explode( ".", $remote_addr_end );
		$remoteBytes 	= explode( ".", $_SERVER[ "REMOTE_ADDR" ] );
			
		//format des plages IP saisies
		
		if( count( $startBytes ) != 4 || ( $remote_addr_end !== null && count( $endBytes ) != 4 )  )
			return true;
		
		foreach( $startBytes as $startByte ){
		
			if( $startByte != "*" && !preg_match( $bytePattern, $startByte ) )
				return true;
			
		}
		
		foreach( $endBytes as $endByte ){
		
			if( $remote_addr_end !== null && $endByte != "*" && !preg_match( $bytePattern, $endByte ) )
				return true;
			
		}
		
		//vérifier l'accès
		
		$i = 0;
		while( $i < 4 ){
		
			$remoteByte 	= $remoteBytes[ $i ];
			$startByte 		= $startBytes[ $i ];
			$endByte 		= $remote_addr_end === null ? null : $endBytes[ $i ];
			
			if( $remote_addr_end === null ){
			
				if( $startByte != "*" && $startByte != $remoteByte )
					return false;
					
			}
			else{
			
				if( ( $startByte != "*" || $endByte != "*" ) 
					&& ( $remoteByte < $startByte || $remoteByte > $endByte ) )
					return false;
					
			}
			
			$i++;
			
		}
		
		return true;
		
	}
	
	//----------------------------------------------------------------------------
	
}

?>
