<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object gestion des tables de la base de données
 */

include_once('adm_table.php');
include_once('adm_category.php');

/**
 * Gestion des articles (références d'un produit)
 * 
 * @package    	Administration
 * @uses 		class ADMTABLE
 * @uses 		class ADMPRODUCT
 * @var $productobj Object produit
 */
class ADMARTICLE extends ADMTABLE
{
	/* defined by parent class 
	var $tablename;
	var $tabledesc = false;
	var $design=false;
	var $Array_DisplayFields;
	var $current_record = false ;
	*/
	//var $Array_DisplayFields = false;
	//var $productobj;
	var $intitile_values;

/**
 * Contructeur
 * @param string $tablename Nom de la table
 */	
	function __construct( $tblname)
	{
		ADMTABLE::ADMTABLE($tblname);
		$this->MakeEmptyRecord();
		//$this->productobj = new ADMPRODUCT('product');
		 $this->intitile_values = array();
		$this->DEBUG = 'code';
	}

	/**
	 * ADMARTICLE::store_intitule()	Gère les champs intitulés supplémentaires x_param...
	 * Copie du champ x_parm_value... si non vide dans  parm_value...
	 * @return void
	 */
	public function store_intitule()
	{
		foreach ($_POST as $k=>$v) {
			if( preg_match("/parm_value_([0-9]*)/",$k,$regs) ) {
				$i=$regs[1];
				$this->intitile_values[$i]=$v;
			}
		}
	}

/**
 * Enregistrement des intitulés d'une référence
 * @param string $ref la référence
 * @return void
 */
	public function save_intitule( $ref)
	{
		$ref = $this->current_record['reference'];
		$idp = $this->current_record['idproduct'];
		$query="SELECT DISTINCT `idcategory`, `idfamily`, `family_display`, `idintitule_title`, `title_display`, `cotation`, `level`, `dyn_text` FROM `intitule_link` WHERE `idproduct`=$idp";
		$rs = DBUtil::getConnection()->Execute($query);
		//if($rs->EOF) trigger_error("ADMARTICLE::save_intitule ",E_USER_ERROR);
		while(!$rs->EOF) {
			$idt = $rs->fields['idintitule_title'];
			if( isset($this->intitile_values[$idt])) $v=trim($this->intitile_values[$idt]);
			else { $v=''; echo "Erreur : intitulé manquant ($idt)."; }
			$query = "SELECT `idintitule_value` FROM `intitule_link` WHERE `idintitule_title`= $idt AND `reference`='$ref'";
			$rsb = DBUtil::getConnection()->Execute($query);
			if($rsb->EOF) {
				$idv = $this->add_intitule_value($v);
				$query='SELECT `idarticle` FROM detail WHERE `reference`='.DBUtil::getConnection()->qstr($ref);
				$rsa = DBUtil::getConnection()->Execute($query);
				$query='INSERT INTO `intitule_link` (`idarticle`, `idproduct`, `idcategory`, `idfamily`, `family_display`, ' .
						'`idintitule_title`, `title_display`, `idintitule_value`, `reference`, `cotation`, `level`, `dyn_text`) ' .
						'VALUES (' . $rsa->fields['idarticle'].', '.$idp.', '.$rs->fields['idcategory'].', '.$rs->fields['idfamily'].', '.
						$rs->fields['family_display'].', '.$idt.', '.$rs->fields['title_display'].', '.$idv.', '.
						DBUtil::getConnection()->qstr($ref).', '.DBUtil::getConnection()->qstr($rs->fields['cotation']).', '.$rs->fields['level'].', '.$rs->fields['dyn_text'].')' ;
				DBUtil::getConnection()->Execute($query);
			} else {
				$id = $this->add_intitule_value($v);
				$oldid = $rsb->fields['idintitule_value'];
				$query = "UPDATE `intitule_link` SET `idintitule_value`=$id WHERE `idintitule_title`= $idt AND `reference`='$ref'";
				if($id != $oldid) DBUtil::getConnection()->Execute($query);
			}
			$rs->MoveNext();
		}

	}

/**
 * Recherche ou ajout d'un enregistrement dans la table intitule_value
 * @param string $v la valeur de l'intitulé
 * @return int id de la valeur
 */
public function add_intitule_value( $v) {
	if($v=='') return 0;
	$query = "SELECT idintitule_value FROM intitule_value WHERE intitule_value_1=".DBUtil::getConnection()->qstr($v,get_magic_quotes_gpc());
	$rsc = DBUtil::getConnection()->Execute($query);
	if($rsc->EOF) {
		$query = "SELECT MAX(idintitule_value) as idmax FROM intitule_value";
		$rsd = DBUtil::getConnection()->Execute($query);
		$newid = 1 + $rsd->fields['idmax'];
		$query = "INSERT INTO intitule_value SET idintitule_value=$newid, intitule_value_1=".DBUtil::getConnection()->qstr($v,get_magic_quotes_gpc());
		DBUtil::getConnection()->Execute($query); //TODO ajout username et date
	} else {
		$newid = $rsc->fields['idintitule_value'];
	}
	return $newid;
}

	/**
	 * Suppression d'un article
	 * @param string $Str_CodedCondition clause sql WHERE
	 * @return void
	 */	
	public function DeleteArticle( $Str_CodedCondition)
	{
		$Str_Condition = trim($this->SerialDecode($Str_CodedCondition));
		if($Str_Condition == '') die("Error DeleteArticle");
		DBUtil::getConnection()->Execute("DELETE FROM detail $Str_Condition");
		DBUtil::getConnection()->Execute("DELETE FROM intitule_link $Str_Condition");
	}
	
	/**
	 * Retourne la categorie d'un produit
	 * @param int $IdProduct Id du produit
	 * @return int
	 */ 
	function LoadCategoryFromProduct( $IdProduct)
	{
		$data = DBUtil::getConnection()->GetRow("select distinct(idcategory) from product where idproduct='$IdProduct'");
		if($data) return $data["idcategory"];
		return 0;
	}
	
	/**
	 * Recherche les noms des intitulés
	 * @param int $IntIDProduct id du produit 
	 * @param string $Str_Ref
	 * @return array
	 */
	public function GetSimplerProductIntitulesNames( $Int_IDProduct, $Str_Ref)
	{
		$Arr_Final=array();
	// get title names
/*		$Str_FieldList = "parm_name_1, parm_name_2, parm_name_3, parm_name_4, parm_name_5, parm_name_6, parm_name_7, parm_name_8, parm_name_9, parm_name_10, parm_name_11, parm_name_12, parm_name_13, parm_name_14, parm_name_15," .
				"parm_name_16, parm_name_17, parm_name_18, parm_name_19, parm_name_20, parm_name_21, parm_name_22, parm_name_23, parm_name_24, parm_name_25, parm_name_26, parm_name_27, parm_name_28, parm_name_29, parm_name_30";
		$Str_KeyID  = "WHERE idproduct='$Int_IDProduct' "; // SQL Condition
*/		
		$SQL_select = 'SELECT DISTINCT t.intitule_title_1 as it ,l.idintitule_title as idt,l.idfamily as idf, f.intitule_family_1 as `if`,' .
			' l.title_display as dt, l.family_display as df' .
			' FROM intitule_link l,intitule_title t,intitule_family f WHERE l.idproduct = ' . $Int_IDProduct .
			' AND l.idintitule_title = t.idintitule_title' .
			' AND f.idintitule_family = l.idfamily' .
			' ORDER BY l.title_display';
		$res= DBUtil::getConnection()->Execute($SQL_select);
		while(!$res->EOF) {
			$idf = $res->fields['idf'];
			$idt = $res->fields['idt'];
			$Arr_Final[$idf][$idt]= array(0 => $res->fields['if'],1 => $res->fields['it'],2 => '');
			$res->MoveNext();
		}
		if($Str_Ref == '') return $Arr_Final;
		
		$SQL_select = 'SELECT l.idarticle,l.idintitule_title as idt,l.idfamily as idf,v.intitule_value_1 as iv' .
			' FROM intitule_link l, intitule_value v WHERE l.reference = ' . DBUtil::getConnection()->qstr($Str_Ref)  .
			' AND l.idintitule_value = v.idintitule_value' .
			' ORDER BY l.reference,l.title_display';
		$res= DBUtil::getConnection()->Execute($SQL_select);
		while(!$res->EOF) {
			$idf = $res->fields['idf'];
			$idt = $res->fields['idt'];
			$Arr_Final[$idf][$idt][2]= $res->fields['iv'];
			$res->MoveNext();
		}	

		return $Arr_Final;
	}
	
	/**
	 * Recherche les langues dans le praramètres
	 * @todo cette fonction existe auusi dans ADMPROD - voir à changer ça
	 * @return array un tableau indicé contenant toutes les langues autorisées
	 */
	public function AllowedLanguages()
	{
		$Arr_Result=array();
		for ( $i=1 ; $i<=5; $i++ )
			{
				$data = DBUtil::getConnection()->GetRow("select paramvalue from parameter_admin WHERE idparameter='lang$i' ") ;
				if ( $data ) $Arr_Result[] = $data['paramvalue'];
			}
		return $Arr_Result;
	}

	/**
	 * Retourne la valeur du champ $Field
	 * @param string $Field le nom du champ
	 * @return mixed la valeur de la propriété $Field
	 */
	public function get( $Field )
	{
		$n = count($this->Array_DisplayFields);
		for ($i = 0 ; $i < $n ; $i ++) {
			if ( $this->Array_DisplayFields[$i]['fieldname'] == $Field ) {
				return $this->Array_DisplayFields[$i]['value'];
			}
		}
		return "Error $Field";
	}
	
	/**
	 * Place une valeur dans un champ
	 * @param string $Field Nom du champ
	 * @param string $Value Valeur du champ
	 * @return void
	 */
	public function set( $Field, $Value )
	{
	$n=count($this->Array_DisplayFields); //$n = $this->GetFieldsForEditing();
	for($i=0;$i<$n;$i++) if( $this->Array_DisplayFields[$i]['fieldname'] == $Field ) { $this->Array_DisplayFields[$i]['value']=$Value; break; }
	//echo "SET $i $n SET";
	}
	
	/**
	 * Affiche une liste déroulante avec les inttulés
	 * @param string $Field Nom du champ
	 * @param int $key Le numéro de l'intitulé
	 * @param string $Select La valeur sélectionnée
	 * @param bool $addempty si true, ajout d'un élément vide
	 * @return int 1 si liste, 0 si champ texte
	 */
	public function SelectIntituleField( $Field, $key, $Select='', $addempty=true)
	{
		$Query="select idintitule_title from intitule_title where intitule_title_1='".mysqli_escape_string($Field)."'";
		$isel=true;
		if( empty($Select) ) $isel=false;
		$res = DBUtil::getConnection()->Execute($Query);
		if($res->RecordCount()>0)
		{
			$Answer=$res->fields['idintitule_title'];
					$res2 = DBUtil::getConnection()->Execute("select v.intitule_value_1 from intitule_title_value tv  ,intitule_value v where  tv.idintitule_title='$Answer' and tv.idintitule_value = v.idintitule_value order by v.intitule_value_1");
					$n=$res2->RecordCount();
					if($n>0)
					{
						echo "<select name=\"parm_value_$key\">\n";
						if($addempty) echo "<option value=\"\"></option>\n";
						for($i=0 ; $i < $n ; $i++)
						{
							$Value=$res2->fields['intitule_value_1'];
							if($Select==$Value)
								{
								echo "<option selected value=\"$Value\">$Value</option>\n";
								$isel=false;
								}
							else
								echo "<option value=\"$Value\">$Value</option>\n";
							$res2->MoveNext();
						}
						if($addempty && $isel) echo "<option selected value=\"$Select\">$Select</option>\n";
						echo "</select>\n";
						return(1);
					}
		}
		echo "<input type='text' name='parm_value_$key' value=\"$Select\" size='30'>\n";
		return(0);
	}
	
	/**
	 * Affiche un menu de sélection du masque détail
	 * @param string $Select le masque présélectionné ( optionnel )
	 * @return void
	 */
	public function SelectMaskDetail( $Select="" )
	{
		$FileArray=file("../../catalog/product_display_detail.php");
		echo "<select name=productmask>\n";
		if($Select==-1)
			$Selected="selected"; 
		else
			$Selected="";
		echo "<option $Selected value=-1>Aucun</option>\n";
		if($Select=="")
			$Selected="selected"; 
		else
			$Selected="";
		echo "<option $Selected value=\"\">0</option>\n";
		for($i=0 ; $i < count($FileArray) ; $i++)
		{
			if(strstr($FileArray[$i],"function DisplayDetail"))
			{	
				$ExpF=explode("_",$FileArray[$i]);
				$ExpMask=explode("(",$ExpF[1]);
				if($ExpMask[0]!="")
				{
					if($Select==$ExpMask[0])
						$Selected="selected";
					else
						$Selected="";
					echo "<option $Selected value=\"$ExpMask[0]\">$ExpMask[0]</option>\n";
				}
			}
		}
		echo "</select>";				
	}
	
	/**
	 * Génère la clauuse sql WHERE
	 * @param int $IdCategory L'id de la catégorie
	 * @param mixed $IdSupplier L'id du fournisseur (facultatif)
	 * @return string
	 */
	public function FindCatProductQuery( $IdCategory,$IdSupplier="")
	{
		$Result="where idcategory='$IdCategory'";
		if($IdSupplier!="") $Result .= " and idsupplier='$IdSupplier'";
		return $Result;
	}

	
	/**
	 * Affiche un 'input' pour un formulaire, la valuer est traduite
	 * @param string $Str_ButtonType Le type du bouton
	 * @param string $Str_ButtonName Le nom du bouton
	 * @param string $Str_ButtonValue La valeur du bouton
	 * @return void
	 */
	public function InputButton( $Str_ButtonType, $Str_ButtonName, $Str_ButtonValue)
	{
	echo '<input type="',$Str_ButtonType,'"  SIZE="8" name="',$Str_ButtonName,'" value="', Dictionnary::translate($Str_ButtonValue),"\">\n";
	}

	/**
	* Récupère les valeurs par défaut de la table desc_field
	* @return void
	*/
	function set_defaults() {
		$n = count($this->design);
		for($i = 0 ; $i < $n ; $i++) {

			$f = $this->design[$i]["fieldname"];
			if(isset($_POST[$f]))
				$this->current_record[$f] = $this->design[$i]["default_value"];
		}
	}
	
	function set_record(array $ArrayRecord,$EditOnly=true)
	{
		showDebug($ArrayRecord,"ADMTABLE::set_record($ArrayRecord,$EditOnly)".' => $ArrayRecord',$this->DEBUG);
		//showDebug($this->design,"ADMTABLE::set_record($ArrayRecord,$EditOnly)".' => $this->design',$this->DEBUG);
		//showDebug($this->current_record,"ADMTABLE::set_record($ArrayRecord,$EditOnly)".' => $this->current_record',$this->DEBUG);
		$n = count($this->design);
		for ( $i=0 ; $i<$n ; $i++ )
		{
			
			$f = $this->design[$i]["fieldname"];
			if ( isset($ArrayRecord[$f]) )
				$this->current_record[$f] = $ArrayRecord[$f];
		}
		showDebug($this->current_record,"ADMTABLE::set_record($ArrayRecord,$EditOnly)".' => $this->current_record',$this->DEBUG);
	}
	
	//--------------------------------------------------------------------------------------
	
	/**
	 * Recherche les champs disponibles pour l'éditon
	 * @return int le nombre de champs trouvés 
	 */
	function GetFieldsForEditing()
	{
	$this->Array_DisplayFields = array();// in $this->Array_DisplayFields the fields

	$n = count($this->design);

	if ($n < 1) Error(" ".Dictionnary::translate("cell_desk_field")." ", 001);// At least one row to display
		$ii = 0;
		for($i = 0 ; $i < $n ; $i++) {
			if(!$this->design[$i]['display_avalaible']) continue;

			$this->Array_DisplayFields[$ii]["fieldname"]=	$this->design[$i]["fieldname"];
			$this->Array_DisplayFields[$ii]["type"]=	$this->design[$i]["type"];
			$this->Array_DisplayFields[$ii]["size"]=	$this->design[$i]["size"];
			$this->Array_DisplayFields[$ii]["help"]=	$this->design[$i]["help"];
			$this->Array_DisplayFields[$ii]["value"]=	$this->design[$i]["default_value"];
			$Str_SelectTableField = $this->design[$i]["modify_select_table"]; // table and field names separated by ':'
			if($Str_SelectTableField) {
				$Arr_SelectTableField = explode(":",$Str_SelectTableField);
				$this->Array_DisplayFields[$ii]["modify_select_table"] = $Arr_SelectTableField[0];
				$this->Array_DisplayFields[$ii]["modify_select_orig_field"] = $Arr_SelectTableField[1];
				$this->Array_DisplayFields[$ii]["modify_select_final_field"] = $Arr_SelectTableField[2];
			}
			else $this->Array_DisplayFields[$ii]["modify_select_table"]='';
			//echo "III $i $ii ".$this->design[$i]["fieldname"]."/".$this->Array_DisplayFields[$ii]["fieldname"]."III";
			$ii++;
		}
		return $ii;
	}
	
	//-----------------------------------------------------------------------------------------------------------
	
	/**
	 * Vérifie si un champ est éditable ou non
	 * @param string $field le om du champ
	 * @return bool true si le champ est éditable, sinon false
	 */
	protected function isEditable( $field ){
	
		return true;
		
	}
}//EOC
?>
