<?php
die("ok");
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object import-Export d'un fichier csv
 */
 
class CsvEngine{
	
	private $dataindir = '/tmp/';
	private $dataoutdir = '/tmp/';
	private $mime_type = "application/octet-stream"; // default mime type
	private $default_date_fr = TRUE;
	private $db; // db connection
	private $result = FALSE; // query result
	private $result2 = FALSE; // 2nd query result
	private $use_filter = TRUE; // utilisation du filtre pour l'export
	private $aErrors;
	private $tables;
	private $exports;
	private $infos = array(); // info column data
	private $exp_intit = 0;
	private $fp; // file handle for excel
	
	private $desc_fields=array();
	private $csv_fields=array(); //fields in csv to care about...

	private $csv_file=''; //file to open
	private $csv_data; //data in file
	private $csv_array = array(); //array of data in file
	private $csv_sel_fields = array(); //selection of fields 
	private $csv_data_all = array(); //all the data
	private $csv_data_keyed = array(); //all the data
	private $tmp_array=array();
	
	
	private $mysql_array=array();
	private $table; //table to insert into
	private $mysql_link; //database
	private $instructions=array(); //sql and default settings
	private $instruct_functions = array(); //function calls
	private $debug=0; // show debug information - step by step messages
	private $return_debug=0; // return debug information instead of echoing it
	private $vals_per_insert=100; //number of values to add per insert statement of SQL
	private $format='win'; // 'linux';
	private $linebreak_char="\r\n" ; //"\n";
	private $delimit_char = ";";
	private $export_filter =''; //sql conditions
	private $headerset = TRUE;
	private $dateformat = 'fr'; 
	private $numformat = 'fr' ;
	private $idreplace = FALSE;
	
	private $detail_filter = "";
	private $product_filter = "";
	private $detail_orderby = "";
	private $product_orderby = "";
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 */
	function CsvEngine(){
		
		$this->aErrors = array();
		
	}

	//---------------------------------------------------------------------------------
	public function getDescFields(){ return $this->desc_fields; }
	public function getCSVFields(){ return $this->csv_fields; }

	/**
	 * Connexion ` la base de donnies
	 * @return bool true mjme si ga marche pas
	 */
	public function connect(){
		
		global $GLOBAL_DB_HOST,$GLOBAL_DB_NAME,$GLOBAL_DB_USER,$GLOBAL_DB_PASS;
		$this->db = mysqli_connect($GLOBAL_DB_HOST,$GLOBAL_DB_USER,$GLOBAL_DB_PASS);
		if(!$this->db) { $this->aErrors[] = "Impossible de se connecter ` " . $GLOBAL_DB_HOST ; return FALSE; }
		// Silection de la base de donnies db
		$db_selected = mysqli_select_db($this->db,$GLOBAL_DB_NAME );
		if (!$db_selected) {
			$this->aErrors[] = "Impossible d'utiliser la base $GLOBAL_DB_NAME : " . mysqli_error($this->db); return FALSE; }
		
		//recherche des paramhtres
		$n = $this->select("SELECT paramvalue FROM parameter_admin WHERE idparameter = 'admin_email'");
		if($n) { $row =  $this->getrow(); $this->msgdest = $row['paramvalue']; }
		else { $this->aErrors[] = "Paramhtre 'admin_email' manquant dans 'parameter_admin'."; return FALSE; }
		$this->free_result();
		$n = $this->select("SELECT paramvalue FROM parameter_admin WHERE idparameter = 'export_dir'");
		if($n) { $row =  $this->getrow(); $this->dataoutdir = $row['paramvalue']; }
		else { $this->aErrors[] = "Paramhtre 'export_dir' manquant dans 'parameter_admin'."; return FALSE; }
		$this->free_result();
		$n = $this->select("SELECT paramvalue FROM parameter_admin WHERE idparameter = 'import_dir'");
		if($n) { $row =  $this->getrow(); $this->dataindir = $row['paramvalue']; }
		else { $this->aErrors[] = "Paramhtre 'import_dir' manquant dans 'parameter_admin'."; return FALSE; }
		$this->free_result();
		
		return TRUE;
		
	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Difinit la la connexion ` utiliser
	 * @param resource $db une resource MySQL
	 * @return void
	 */
	public function setdb( resource &$db){
		$this->db = &$db;
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Diconnexion de la base de donnies
	 * @return void
	 */
	public function disconnect(){
		mysqli_close($this->db);
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Exicute une requjte SQL
	 * @param string $query la requjte ` ixicuter
	 * @return le nombre de lignes de risultats
	 */
	private function select($query){
		$this->result = mysqli_query($this->db,$query);
		if(!$this->result) { $this->aErrors[] = "Erreur SQL : $query - " . mysqli_error($this->db); return 0; }
		 return mysqli_num_rows($this->result);
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Crii un tableau associatif en utilisant mysql_fetch_assoc() ` partir de la dernihre requjte ixicutie
	 * @return array
	 */
	private function getrow(){
		return mysqli_fetch_assoc($this->result);
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Libhre des resources
	 * @return voids
	 */
	private function free_result(){
		@mysqli_free_result($this->result);
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Exicute une requjte SQL
	 * @param string $query la requjte SQL
	 * @return lel nombre de lignes de risultats
	 */
	private function select2($query){
		$this->result2 = mysqli_query($this->db,$query);
		if(!$this->result2) { $this->aErrors[] = "Erreur SQL : $query - " . mysqli_error($this->db); return 0; }
		 return mysqli_num_rows($this->result2);
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Crii un tableau associatif en utilisant mysql_fetch_assoc() ` partir de la dernihre requjte ixicutie
	 * @return array
	 */
	private function getrow2(){
		return mysqli_fetch_assoc($this->result2);
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Libhre des resources
	 * @return void
	 */
	private function free_result2(){
		@mysqli_free_result($this->result2);
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Exicute une requjte SQL
	 * @param string $query une requjte SQL
	 * @return int le nombre d'enregistrements modifiis
	 */
	private function update($query){ 
	 
	 	if( !mysqli_query($this->db,$query) ) { $this->aErrors[] = "Erreur SQL : $query - " . mysqli_error($this->db); return 0; }
	 	return mysqli_affected_rows($this->db);
	}

	//---------------------------------------------------------------------------------
	
	//CSV FUNCTIONS
	
	/**
	 * Difinit le caracthre de dilimitation ` utiliser pour le fichier CSV
	 * @param string $ch le caracthre de dilimitation ` utiliser
	 * @return void
	 */
	public function set_delimit_char($ch){
		$this->delimit_char = $ch;
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit la table ` utiliser
	 * @param string $table
	 * @return void
	 */
	public function set_table($t){
		$this->table = $t;
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit le nombre d'intitulis maximal ` exporter
	 * @param int $n le nombre d'intitulis maximal ` exporter
	 * @return void
	 */
	public function set_exp_intit($n){
		$this->exp_intit = 0 + $n;
	}
		
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit le format ` utiliser pour le fichier CSV ( linux ou windows )
	 * @param string format ( 'linux' ou 'win' )
	 * @return void
	 */
	public function set_format($format){
		
		$this->format = $format;
		if($format == 'win') $this->linebreak_char = "\r\n";
		else $this->linebreak_char = "\n";
		
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit le format de date ` utiliser
	 * @param string format le format de date ` utiliser ( ex : 'fr' )
	 * @return void
	 */
	public function set_date_format($format){
		$this->dateformat = $format;
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit le format ` utiliser pour les valeurs numiriques
	 * @param string format ( ex : 'fr' )
	 * @return void
	 */
	public function set_num_format($format){
		$this->numformat = $format;
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit s'il faut ou non afficher les en-tjtes dans le fichier CSV
	 * @param bool $b
	 * @return void
	 */
	public function set_header($b=TRUE){
		$this->headerset = $b;
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * ???
	 * @param bool $b
	 * @return void
	 */
	public function set_idreplace($b=TRUE){
		$this->idreplace = $b;
	}
	
	//---------------------------------------------------------------------------------
			
	/**
	 * Supprime les espaces blancs au dibut et ` la fin d'une valeur donnie
	 * @param string $t la valeur
	 * @return string la valeur $t sans les espaces blancs de dibut ou de fin
	 */
	private function newid($t){
		if($t == 0 ) return '';
		return trim($t);
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ricuphre la difinition de la table utilisie depuis la table desc_table
	 * @return void
	 */
	private function get_sql_tables(){
		
		$table=$this->table;
		$query="select tablename,tablekeys from desc_table where tablename='$table'";
		$n = $this->select($query);
		$this->tables = array();
		for($i=0;$i<$n;$i++)
			{
			$this->tables[] =  $this->getrow();
			}
		$this->free_result();
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ricuphre les informations sur toutes les colonnes de la table utilisie pour l'import/export
	 * @deprecated $fsel
	 * @return void
	 */
	public function get_sql_fields($fsel=0){
		
		$this->get_sql_tables();
		//$query="select tablename,fieldname,type,export_name,mandatory,order_view,modify_select_table,export_info_column from desc_field where tablename='{$this->table}' and export_avalaible>=$fsel order by order_view,export_name";
		$query="
		SELECT 
			tablename, 
			fieldname,  
			type,  
			export_name,  
			mandatory,  
			order_view, 
			modify_select_table, 
			export_info_column  
		FROM desc_field 
		WHERE tablename='{$this->table}' 
		AND export_avalaible = 1 
		ORDER BY order_view,export_name";
		
		// echo $query;
		$n = $this->select($query);
		$this->csv_fields=array();
		
		$this->desc_fields=array();
		for($i=0;$i<$n;$i++)
			{
			$this->desc_fields[$i] = $row =  $this->getrow();
			$this->csv_fields[$i]=$row['export_name'];
			//$this->sqlxxxx_fields[$i]=$row['tablename'].'.'.$row['fieldname'];
			$this->desc_fields[$i]['fieldformat']= $this->get_format($row['type']);
			}
		$this->free_result();
	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Retourne une valeur numiriques pour le $i-hme champ ( ex : le numiro du pays correspondant ` un nom de pays donni )
	 * @param int $i le $i-ihme champ
	 * @param string $str le texte que l'on souhaite convertir ( ex : 'france' )
	 * @param int $res la valeur retournie par difaut si aucun risultat n'est trouvi
	 * @return string
	 */
	private function translate2num($i,$str,$res=0){
		
		$table = $this->desc_fields[$i]['trans_table'];
		$id = $this->desc_fields[$i]['trans_id'];
		$alias = $this->desc_fields[$i]['trans_text'];
		//list($table,$id,$alias) = explode(';',$tableparam);
		$query="select $id as id from $table where $alias='$str'";
		$n = $this->select($query);
		if($n>0)
			{
			$row =  $this->getrow();
			$res=$row['id'];
			}
			else $this->Error('Dans ' . $this->csv_file . ' la valeur  : '. $str . " n'existe pas dans la table $table." );
		$this->free_result();
		
		return $res;
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Retourne un texte pour le $i-hme champ ( ex : le nom du pays correspondant ` un numiro de pays donni )
	 * @param int $i le $i-ihme champ
	 * @param int $num la valeur numirique que l'on souhaite convertir ( ex : le numiro de pays '1' )
	 * @param string $res la valeur retournie par difaut si aucun risultat n'est trouvi
	 * @return string
	 */
	private function translate2text($i,$num,$res=''){
		
		$Answer= $this->desc_fields[$i]['modify_select_table'];
		if(!empty($Answer))
		{
			list($table,$alias,$id)=explode(":",$Answer);
		}
		$query="select $alias as alias from $table where $id='$num'";
		$n = $this->select2($query);
		if($n>0)
		{
			$row =  $this->getrow2();
			$res='"'.$row['alias'].'"';
		}
		else $this->Error('Dans ' . $this->csv_file . ' la valeur  : '. $num . " n'existe pas dans la table $table." );
		
		$this->free_result2();
		return $res;
		
	}

	//---------------------------------------------------------------------------------
	
	//get the tables
	/**
	 * Ricuphre les noms des tables ` utiliser
	 * @return int le nombre de tables utilisies
	 */ 
	private function get_tables(){
		$query="select tablename from desc_table order by export_name";
		$n = $this->select($query);
		$this->exports=array();
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$this->exports[$i]=$row['tablename'];
			}
		$this->free_result();
		return $n;
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ginhre du code HTML
	 * @return void
	 */
	public function list_tables(){
		$n = $this->get_tables();
		for($i=0;$i<$n;$i++)
		{
		echo '<option value="',$this->exports[$i],'">',$this->exports[$i],"</option>\n";
		}
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit une condition SQL ` utiliser pour l'export
	 * @param string $b la condition SQL
	 * @return void
	 */
	public function set_sqlfilter($b=''){
		$this->export_filter = $b;
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Retourne les conditions utilisies pour les requjtes SQL
	 * @deprecated $b
	 */
	public function get_sqlfilter($b=''){
		return $this->export_filter;
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit le nom de fichier cvs
	 * @param string $f le nom de fichier cvs (optionnel)
	 * @return void
	 */
	public function set_csv($f='data.csv'){
		
		$this->csv_file = $f;
		if( is_file($f) )  
			return TRUE;
		return FALSE;
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit les champ ` exporter 
	 * @param array $v un tableau associatif
	 * @return void
	 */
	public function set_filter($v){
		$this->csv_sel_fields = $v; 
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit les champ ` exporter pour la table detail
	 * @param array $filter un tableau associatif
	 * @return void
	 */
	public function set_detail_filter( $filter ) { $this->detail_filter = $filter; }
	/**
	 * Difinit les champ ` exporter pour la table product
	 * @param array $filter un tableau associatif
	 * @return void
	 */
	public function set_product_filter( $filter ) { $this->product_filter = $filter; }
	/**
	 * Difinit la fagon de trier les risultats pour la table detail
	 * @param string $orderby
	 * @return void
	 */
	public function set_detail_orderby( $orderby )	{ $this->detail_orderby = $orderby; }
	/**
	 * Difinit la fagon de trier les risultats pour la table product
	 * @param string $orderby
	 * @return void
	 */
	 public function set_product_orderby( $orderby ){ $this->product_orderby = $orderby; }

	//---------------------------------------------------------------------------------
	
	/**
	 * Difinit les champs ` conserver pour l'export
	 * @return void
	 */
	private function do_filter(){
		
		if( empty($this->csv_file) ) return; 
		if( empty($this->csv_sel_fields) ) return; 
		$n = count($this->desc_fields);
		for($i=0;$i<$n;$i++)
			{
				$x = $this->desc_fields[$i]['fieldname'];
				if(array_search($x, $this->csv_sel_fields) === FALSE) $this->desc_fields[$i]['fieldformat'] = -1;
			}
			//echo '<pre>';  print_r($this->desc_fields); echo '<pre>';
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Retourne le nom d'export d'une champ donni
	 * @param string $Str_FieldName le nom du champ
	 * @param $fl example : commercial:firstname:iduser:salesperson
	 */
	public function info_field($Str_FieldName,$fl) { //example : commercial:firstname:iduser:salesperson name
		$a = explode(':',$fl);
		if( count($a) != 4 ) { echo ' bad export_info_column ',$Str_FieldName,'/',$fl; return '';}
		$table = $a[0];
		$field = $a[1];
		$id = $a[2];
		$export_name = $a[3];
		$query="SELECT `$id` as `i`,`$field` as `f` FROM `$table`";// echo " $query ";
		$n = $this->select2($query);
		$this->infos[$Str_FieldName]=array();
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow2();
			$x = $row['i'];
			$this->infos[$Str_FieldName][$x]=$row['f'];
			}
		$this->free_result2();
		return $export_name;	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ginhre le fichier d'export CSV
	 * @return string $msg un message de rapport
	 */
	public function sql2csv(){
		
		$this->do_filter(); $msg='';
		$this->setMimeType( 'text/x-csv' );
		if( is_file($this->csv_file ) )  unlink($this->csv_file);
		$handle = fopen($this->csv_file, 'wb');
		if(!$handle) return $this->Error('Unable to create output file: '.$this->csv_file);
		$m = count($this->desc_fields);
		$fieldlist='';$virg='';
		 for($j=0;$j<$m;$j++) 
			{
			if( $this->desc_fields[$j]['fieldformat'] >= 0 )
				{
				$fieldlist .= $virg . "`" . $this->desc_fields[$j]['fieldname'] . "`" ;
				$virg=',';
				} 
			}
		echo '<pre>'; print_r($this->desc_fields);echo '</pre>';
		if( $this->headerset ) 
		 { $virg='';
		 for($j=0;$j<$m;$j++) 
			{
			if( $this->desc_fields[$j]['fieldformat'] >= 0 )
				{
					$f = $this->desc_fields[$j]['export_name'];
					fwrite($handle, $virg.$f);
					$virg=$this->delimit_char;
					if( $this->desc_fields[$j]['export_info_column'] ) {
						fwrite($handle,$virg.$this->info_field($this->desc_fields[$j]['fieldname'],$this->desc_fields[$j]['export_info_column']));	
					}
				} 
			}

		 fwrite($handle, $this->linebreak_char);
		 }
		
		$buffer=array(); 
		//la table primaire...
		$table = $this->tables[0]['tablename'];
		if( empty($table) ) 
		{
		$n = 0;
		$this->Error('Pour ' . $this->csv_file . ' il manque un enregistrement dans la table erp_link.' );
		}
		else
		{
		$query= "SELECT $table.* FROM $table " . $this->export_filter ;
		
		$n = $this->select($query); //echo $query;
		}
		/////if( $n > 10 ) $n=10;
		////echo "$query n=$n m=$m<br />\n"; flush();///////////////////
		  
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow(); $buffer[$i]=array(); ////echo "$i <br />\n"; flush();
			for($j=0;$j<$m;$j++)
				{
				if( $table == $this->desc_fields[$j]['tablename'] && $this->desc_fields[$j]['fieldformat'] >= 0 )
					{
					$f = $this->desc_fields[$j]['fieldname'];
					$erpfield = $this->desc_fields[$j]['export_name'];
					//$v = $this->csv_field($row[$f],$j);
					$buffer[$i][$erpfield] = &$row[$f]; //$v;
					}
				}
			}
		$this->free_result();
		
		//les autres tables...
		$nbt = count($this->tables);
		////echo "nb tables $nbt <br />\n"; flush();///////////////////
		for($nt=1;$nt<$nbt;$nt++)
			{
			$table = $this->tables[$nt]['tablename'];
			  for($i=0;$i<$n;$i++)
			  {
			  $tblk = $this->tables[$nt]['tablekeys']; $erpk = $this->tables[$nt]['selectkeys'];
			  $query= "SELECT $table.* FROM $table WHERE  $tblk ='".$buffer[$i][$erpk]."'" ;
			  $nnn = $this->select($query);// echo $query;
			  if($nnn == 1 )
			    {
			    $row =  $this->getrow();
			    for($j=0;$j<$m;$j++)
				{
				if( $table == $this->desc_fields[$j]['tablename'] && $this->desc_fields[$j]['fieldformat'] >= 0 )
					{
					$f = $this->desc_fields[$j]['fieldname'];
					$erpfield = $this->desc_fields[$j]['export_name'];
					//$v = $this->csv_field($row[$f],$j);
					$buffer[$i][$erpfield] = &$row[$f]; //$v;
					}
				}
			    }
			  $this->free_result();	
			  }  
			}
		
		for($i=0;$i<$n;$i++)
			{
			$row =  $buffer[$i];
			$virg='';
			for($j=0;$j<$m;$j++)
				{
				if( $this->desc_fields[$j]['fieldformat'] >= 0 )
					{
					$erpfield = $this->desc_fields[$j]['export_name'];
					if(isset($buffer[$i][$erpfield])) 
						{
						$v = $this->csv_field($buffer[$i][$erpfield],$j);
						fwrite($handle, $virg.$v);
							if( $this->desc_fields[$j]['export_info_column'] ) {
								$x = $this->desc_fields[$j]['fieldname'];
								if( isset($this->infos[$x][$v])) fwrite($handle, $virg.$this->infos[$x][$v]);
								else fwrite($handle, $virg);
							}
						}
					else fwrite($handle, $virg);
					$virg=$this->delimit_char;
					}
				}
			fwrite($handle, $this->linebreak_char);
			}
		fclose($handle);
		chmod($this->csv_file, 0666);
		unset($buffer);
		$msg .= "EXPORT to <a href=\"download.php?file={$this->csv_file}\" target=\"_blank\">{$this->csv_file}</a> : $n rows.<br />\n";
		return $msg;
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ginhre un fichier d'export CSV pour les produits
	 * @return void
	 */
	public function prod2csv(){
		
		if($this->table != 'product') die('prod2csv is for product only!');

		$this->do_filter(); $msg='';
		$prod_desc_fields = $this->desc_fields;
		$prod_csv_fields = $this->csv_fields;
		$art_desc_fields=array();
		
		$this->mime_type = 'text/x-csv';
		if( is_file($this->csv_file ) )  unlink($this->csv_file);
		$handle = fopen($this->csv_file, 'wb');
		if(!$handle) return $this->Error('Unable to create output file: '.$this->csv_file);
		$m = count($this->desc_fields);
		
		
			$this->table='detail';
			$this->get_sql_fields(1);
			$this->do_filter();
			$n=count($this->csv_fields);
			for($i=0;$i<$n;$i++) {
				if(!in_array($this->csv_fields[$i],$prod_csv_fields)) $art_desc_fields[]=$this->desc_fields[$i];
			}
			$mm=count($art_desc_fields);
		
		$this->desc_fields = &$prod_desc_fields;
		
		if( $this->headerset ) 
		 { $virg='';
		 for($j=0;$j<$m;$j++) 
			{
			if( $prod_desc_fields[$j]['fieldformat'] >= 0 )
				{
				$f = $prod_desc_fields[$j]['export_name'];
				fwrite($handle, $virg.$f);
				$virg=$this->delimit_char;
					if( $prod_desc_fields[$j]['export_info_column'] ) {
						fwrite($handle,$virg.$this->info_field($prod_desc_fields[$j]['fieldname'],$prod_desc_fields[$j]['export_info_column']));	
					}
				} 
			}
		 for($j=0;$j<$mm;$j++) 
			{
			if( $art_desc_fields[$j]['fieldformat'] >= 0 )
				{
				$f = $art_desc_fields[$j]['export_name'];
				fwrite($handle, $virg.$f);
				$virg=$this->delimit_char;
					if( $art_desc_fields[$j]['export_info_column'] ) {
						fwrite($handle,$virg.$this->info_field($art_desc_fields[$j]['fieldname'],$art_desc_fields[$j]['export_info_column']));	
					}
				} 
			}
		 for($j=1;$j<=$this->exp_intit;$j++) 
		 	fwrite($handle, $virg.'titre_intitule_'.$j.$virg.'val_intitule_'.$j.$virg.'cotation_'.$j);
		 	
		// fwrite($handle,$virg."image");
		// fwrite($handle,$virg."url");
		 fwrite($handle, $this->linebreak_char);
		 }
		
		$buffer=array(); 

		//-------------------------------------------------------------------------------------

		//la table primaire...
		$table = 'product, detail';
		$query= "SELECT product.* FROM product, detail";
		
		$and = " WHERE ";
		if( !empty( $this->export_filter ) ){
			
			$query .= " " . $this->export_filter . " AND";
			
		}
		else $query .= " WHERE";
		
		$query .= " product.idproduct <> 0 AND detail.idproduct = product.idproduct";
		
		if( !empty( $this->product_filter ) )
			$query .= " AND " . $this->product_filter;
		
		if( !empty( $this->detail_filter ) )
			$query .= " AND " . $this->detail_filter;
		
		$query .= " GROUP BY product.idproduct";
		
		if( !empty( $this->product_orderby ) || !empty( $this->product_orderby ) )
			$query .= " ORDER BY ";
		
		if( !empty( $this->product_orderby ) )	
			$query .= $this->product_orderby;
			
		if( !empty( $this->detail_orderby ) ){
			
			if( !empty( $this->product_orderby ) )
				$query .= ", ";
				
		$query .= $this->detail_orderby;
	
	}

		//-------------------------------------------------------------------------------------
		
		$n = $this->select($query); //echo $query;
		 
		for($i=0;$i<$n;$i++){
			
			$row =  $this->getrow(); 
			$buffer[$i]=array();
			
			for($j=0;$j<$m;$j++){
				
				if( $this->desc_fields[$j]['fieldformat'] >= 0 ){
					
					$f = $this->desc_fields[$j]['fieldname'];
					$erpfield = $this->desc_fields[$j]['export_name'];
					$buffer[$i][$erpfield] = $this->csv_field($row[$f],$j);
					
				}
					
			}
			
		}
		
		$idproductExportName = false;
		$i = 0;
		while( $idproductExportName === false && $i < count( $this->desc_fields ) ){
		
			if( $this->desc_fields[ $i ][ "fieldname" ] == "idproduct" )
				$idproductExportName = $this->desc_fields[ $i ][ "export_name" ];
				
			$i++;
			
		}
		
		if( $idproductExportName === false )
			die( "Cli primaire absente : idproduct" );
			
		$this->free_result();
		
		//la table detail
			$table = 'detail';
			$this->desc_fields = &$art_desc_fields;
		for($i=0;$i<$n;$i++) {
			 
			  $query= "SELECT * FROM `detail` WHERE `idproduct` ='".$buffer[$i][ $idproductExportName ]."'" ;
			 	
			  if( !empty( $this->detail_filter ) )
			  	$query .= " AND {$this->detail_filter}";
			  
			  if( !empty( $this->detail_orderby ) )
			  	$query .= " ORDER BY {$this->detail_orderby}";
			  	//die( $query ); 
			  $nnn = $this->select($query);// echo $query;
			  if($nnn== 0 ) {
			  	$virg='';
			  	for($j=0;$j<$m;$j++) {
			  		if( $prod_desc_fields[$j]['fieldformat'] < 0 ) continue;
			  		$erpfield = $prod_desc_fields[$j]['export_name'];
			  		$v = $buffer[$i][$erpfield];
			  		fwrite($handle, $virg.$v);
			  		$virg=$this->delimit_char;
			  		if( $prod_desc_fields[$j]['export_info_column'] ) {
								$x = $prod_desc_fields[$j]['fieldname'];
								if( isset($this->infos[$x][$v])) fwrite($handle, $virg.'"'.$this->infos[$x][$v].'"');
								else fwrite($handle, $virg);
							}
			  		}
	
			  	fwrite($handle, $this->linebreak_char);
			  }
			  for($k=0;$k<$nnn;$k++) {
			    $row =  $this->getrow();
			    $virg='';
			  	for($j=0;$j<$m;$j++) {
			  		if( $prod_desc_fields[$j]['fieldformat'] < 0 ) continue;
			  		$erpfield = $prod_desc_fields[$j]['export_name'];
			  		$v = $buffer[$i][$erpfield];
			  		fwrite($handle, $virg.$v);
			  		$virg=$this->delimit_char;
			  		if( $prod_desc_fields[$j]['export_info_column'] ) {
								$x = $prod_desc_fields[$j]['fieldname'];
								if( isset($this->infos[$x][$v])) fwrite($handle, $virg.'"'.$this->infos[$x][$v].'"');
								else fwrite($handle, $virg);
							}
			  		}
		
			    for($j=0;$j<$mm;$j++)
				{
				if( $art_desc_fields[$j]['fieldformat'] >= 0 )
					{
					$f = $art_desc_fields[$j]['fieldname'];
					$erpfield = $art_desc_fields[$j]['export_name'];
					$v = $this->csv_field($row[$f],$j);
					fwrite($handle, $virg.$v);
					if( $art_desc_fields[$j]['export_info_column'] ) {
								$x = $art_desc_fields[$j]['fieldname'];
								if( isset($this->infos[$x][$v])) fwrite($handle, $virg.'"'.$this->infos[$x][$v].'"');
								else fwrite($handle, $virg);
							}
					}
			  	}
			  
			  if( $this->exp_intit > 0 ) {	
			  $query2= "SELECT `intitule_title`.`intitule_title_1`,`intitule_value`.`intitule_value_1`,`intitule_link`.`cotation` FROM `intitule_link`,`intitule_title`,`intitule_value` WHERE `idarticle` ='".$row['idarticle']."'".
			  			" AND `intitule_link`.`idintitule_title`=`intitule_title`.`idintitule_title` AND  `intitule_link`.`idintitule_value`=`intitule_value`.`idintitule_value`" .
			  			" ORDER BY family_display,title_display" ;
			  $nint = min($this->select2($query2),$this->exp_intit);
			  for($l=0;$l<$nint;$l++) {
					$row2 =  $this->getrow2();
					fwrite($handle, $virg.'"'.str_replace('"','""',$row2['intitule_title_1']).'"');
					fwrite($handle, $virg.'"'.str_replace('"','""',$row2['intitule_value_1']).'"'  );
					fwrite($handle, $virg.'"'.str_replace('"','""',$row2['cotation']).'"'  );
			  }
			  $this->free_result2();
			  }	
			  		
				//  fwrite($handle, $virg . URLFactory::getHostURL() . URLFactory::getProductImageURI( $buffer[$i][ $idproductExportName ] ) );
				//  fwrite($handle, $virg . URLFactory::getProductURL( $buffer[$i][ $idproductExportName ] ) );
				  
				fwrite($handle, $this->linebreak_char);
				
			   }
			  $this->free_result();	
		}  
		
		fclose($handle);
		chmod($this->csv_file, 0666);
		unset($buffer);
		$msg .= "EXPORT to <a href=\"download.php?file={$this->csv_file}\" target=\"_blank\">{$this->csv_file}</a> : $n rows.<br />\n";
		return $msg;
	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Riorganise les tables de transposition en fonction des entetes du fichier csv
	 * @param array $data un tableau retourni par la commande fgetcsv
	 * @return void
	 */
	private function translate_table($data){
		
		$num = count($data);
		//$sqlfields = array();
		$sqltrans = array();
		for ($c=0; $c < $num; $c++)
			{
			$i = array_search($data[$c],$this->csv_fields);
			if($i === FALSE)
				{
				$sqltrans[$c] = $this->desc_fields[$i];
				$sqltrans[$c]['fieldformat']=-1;
				$sqltrans[$c]['export_name']='_IGNORE_';
				//$sqlfields[$c]='_IGNORE_';
				//$sqlformats[$c]=-1;
				} 
			else
				{
				//$sqlfields[$c] = $this->desc_fields[$i]['fieldname'];
				$sqltrans[$c] = $this->desc_fields[$i];
				}
			}
		$this->csv_fields = $data;
		$this->desc_fields = $sqltrans;
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Importe le fichier CSV dans la base de donnies
	 * @return mixed true en cas de succhs, sinon un message d'erreur
	 */
	public function csv2sql(){
		
		$this->mime_type = 'text/x-sql';
		$row = 1;
		$handle = fopen($this->csv_file, 'rb');
		if(!$handle) return $this->Error('Unable to open intput file: '.$this->csv_file);
		
		if (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE) {
		$num = count($data);
		if( $num == 1 && $data[0]==NULL ) continue;
		$matches=0;
		$badfields = array();
		//verification des entetes
		for ($c=0; $c < $num; $c++) {
			 if( in_array($data[$c],$this->csv_fields ) ) $matches++;
			 else $badfields[]=$data[$c];
			}
		if( $matches == $num ) 
			{
			//echo "header present!  ";
			$this->translate_table($data);
			}
			else if ( $matches > 0 )
			{
			//echo "header NO CORRECT!  "; echo implode(', ',$badfields);
			fclose($handle);
			return $this->Error('Dans ' . $this->csv_file . ' certains champs ne correspondent pas : '. implode(', ',$badfields) );
			}
			else if ( $num != count($this->desc_fields) )
			{
			//echo "FIELD COUNT NO CORRECT!  "; echo implode(', ',$badfields);
			fclose($handle);
			return $this->Error('Dans ' . $this->csv_file . ' le nombre de champs ne correspondent pas : '. implode(', ',$badfields) );
			}
			else
			{
			fclose($handle);
			$handle = fopen($this->csv_file, 'rb');
			}
		}
		 
		$out = fopen($this->csv_file .'.sql', 'wb');
		if(!$out) return $this->Error('Unable to open output file: '.$this->csv_file.'.sql');
		while (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE) {
		    $num = count($data);
		    if( $num == 1 && $data[0]==NULL ) continue;
		    //echo "<p> $num fields in line $row: <br /></p>\n";
		    $row++; $sep= '';
		    fwrite($out,"REPLACE $this->table SET ");
		    for ($c=0; $c < $num; $c++) {
		    if( $this->desc_fields[$c]['fieldformat'] >= 0 )
		    	{ 
		    	fwrite($out, $sep . $this->desc_fields[$c]['fieldname'] . "=" . $this->sql_field($data[$c],$c) );
		    	$sep=",";
			}
		     }    
		    fwrite($out,';'.$this->linebreak_char); //echo "\n";
		}
		fclose($handle);
		fclose($out);
		chmod($this->csv_file.'.sql', 0666);
		return true;
		
	}

	//---------------------------------------------------------------------------------

	/**
	 * Exicute l'import
	 * @return string un message de rapport
	 */
	public function csvimport(){
	
		$ups = 0 ; $ins = 0; $msg='';
		$this->mime_type = 'text/x-sql';
		$rows = 1;
		$handle = fopen($this->csv_file, 'rb');
		if(!$handle) return $this->Error('Unable to open intput file: '.$this->csv_file);
		
		if (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE) {
		$num = count($data);
		if( $num == 1 && $data[0]==NULL ) return $this->Error('Dans ' . $this->csv_file . ' 1hre ligne vide.');
		$matches=0;
		$badfields = array();
		//verification des entetes
		for ($c=0; $c < $num; $c++) {
			 if( in_array($data[$c],$this->csv_fields ) ) $matches++;
			 else $badfields[]=$data[$c];
			}
		
		// Virification des champs obligatoires
		$numf = count($this->desc_fields); 
		$badfields = array();$matches=0;
		for ($c=0; $c < $numf; $c++) {
			if( $this->desc_fields[$c]['mandatory'] ) 
			{
			if( in_array($this->csv_fields[$c],$data) ) $matches++;
			else $badfields[]=$this->csv_fields[$c];
			}
		}
		
		if( count($badfields) > 0 ) return $this->Error('Dans ' . $this->csv_file . ' des champs obligatoires manquent : '. implode(', ',$badfields) );
		}
		
		$this->translate_table($data);
		$nbtables=count($this->tables);
		
		for($nt=0;$nt<$nbtables;$nt++)
		{
		$ups = 0 ; $ins = 0;
		$table = $this->tables[$nt]['tablename'];
		if( $this->tables[$nt]['import_action'] == 'IGNORE' ) continue;
		if( $this->tables[$nt]['empty_on_import'] == 1 ) $this->update('TRUNCATE '.$table);
		if($nt)	{
			fclose($handle); $rows = 1;
			$handle = fopen($this->csv_file, 'rb');
			$data = fgetcsv($handle, 8100, $this->delimit_char);
			//for ($c=0; $c < $num; $c++)  if( $this->desc_fields[$c]['fieldname'] == $this->tables[$nt]['tablekeys'] ) { $tblidx=$c; break; }
			}
		for ($c=0; $c < $num; $c++)  if( $this->desc_fields[$c]['fieldname'] == $this->tables[$nt]['tablekeys'] ) { $tblidx=$c; break; }
		 $fiderp=-1; $foldid=-1; $ffield = '';
		    if( $table == 'buyer' || $table == 'delivery' || $table == 'real_product' || $table == 'product' || $table == 'estimate' || $table == 'order')
		    	{
			$ffield = 'id' . $table ;
			if($table == 'order') $ffield='idorder';
			for ($c=0; $c < $num; $c++)
				{
				if( $this->desc_fields[$c]['fieldname'] == 'iderp' ) $fiderp = $c;
				if( $this->desc_fields[$c]['fieldname'] == $ffield ) $foldid = $c; 
				}
			}
		
		 while (($data = fgetcsv($handle, 8100, $this->delimit_char)) !== FALSE)
		 {
		    $num = count($data);
		    if( $num == 1 && $data[0]==NULL ) continue;
		    //echo "<p> $num fields in line $rows: <br /></p>\n";
		    
		    
		 if( $foldid > -1 &&  $fiderp > -1 &&  $data[$foldid] <= 0 ) //get old id to prevent autoincrement on replace
		 	{
			$query = "SELECT `$ffield`  as oldid FROM `$table`  WHERE iderp='" . $data[$fiderp] . "'" ;
			$n = $this->select($query);
			if($n) { $row =  $this->getrow(); $id=$row['oldid']; $data[$foldid]=$id; }
			$this->free_result();
			//echo "status = $n $query<br />\n";
			}  
		    
		    $rows++; $sep= ''; $oprt = 0;
		    if( $this->tables[$nt]['import_action'] == 'UPDATE' )
		    {
		     if( $this->sql_field($data[$tblidx],$tblidx)  == 'NULL' ) $n=0;
		     else
		     {
		     $query = "SELECT ". $this->tables[$nt]['tablekeys'] . " FROM $table WHERE ". $this->tables[$nt]['tablekeys'] . "=" . $this->sql_field($data[$tblidx],$tblidx). " LIMIT 1";// echo "$query <br />\n";
		     $n = $this->select($query);
		     $this->free_result();
		     }
		     if($n) { $query = "UPDATE `$table` SET "; $oprt = 1; }
		     else { $query = "REPLACE $table SET "; $oprt = 0; } // else { $query = "REPLACE $table SET "; $oprt = 0; }
		    }
		    else $query = "REPLACE $table SET ";
		    if($nt) { $query .=   "`" .$this->tables[$nt]['tablekeys'] . "`" . "=" . $this->sql_field($data[$tblidx],$tblidx) ; $sep=","; }
		    for ($c=0; $c < $num; $c++) {
		    if( $this->desc_fields[$c]['fieldformat'] >= 0 && $this->desc_fields[$c]['tablename']==$table && $data[$c] !='' )
		    	{
		    	$query .=  $sep . "`" . $this->desc_fields[$c]['fieldname'] . "`" . "=" . $this->sql_field($data[$c],$c) ;
		    	$sep=",";
			}    
		    }
		    if( $oprt == 1 ) 
		    	{
			$query .= " WHERE ". $this->tables[$nt]['tablekeys'] . "='" . $this->sql_field($data[$tblidx],$tblidx). "'";
			//echo $query;
			}
		    $n = $this->update($query);
		  /////  echo "status = $n $query<br />\n";
		   if($n == 2 )$ups++;
		   elseif($n == 1 && $oprt == 1 )$ups++;
		   elseif($n == 1 && $oprt == 0 )$ins++;
		 }
		 if($nt==0) $msg .= "READ $rows lines from $this->csv_file.<br />\n";
		 $msg .= "IMPORT in table $table : $ups rows updated , $ins rows inserted.<br />\n";
		}
		fclose($handle);
		//////print_r($this->desc_fields);
		return $msg ;
	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Met ` jour les clis primaires
	 * @return void
	 */
	public function update_indexes(){
		
		//DELIVERY-----BUYER
		$n = $this->select('SELECT iderp,idbuyer_erp FROM delivery WHERE idbuyer=0 OR idbuyer is NULL');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$idbuyer_erp = $row['idbuyer_erp'];
			$iderp=$row['iderp'];
			if( $idbuyer_erp != '0')
			 {
			 $nn = $this->select2("SELECT idbuyer FROM buyer WHERE iderp='$idbuyer_erp'");
			 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'acheteur iderp='$idbuyer_erp' dans delivery iderp='$iderp'"; 
			 else
				{
				$row2 =  $this->getrow2();
				$idbuyer = $row2['idbuyer'];
				$this->update("UPDATE delivery SET idbuyer='$idbuyer' WHERE iderp='$iderp'");
				//echo "UPDATE delivery SET idbuyer=$idbuyer WHERE iderp=$iderp";
				}
			$this->free_result2();
			 }
			}
		$this->free_result();
		//ORDER----BUYER
		$n = $this->select('SELECT iderp,idbuyer_erp FROM `order` WHERE idbuyer=0 OR idbuyer is NULL');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$iderp = $row['iderp'];
			$idbuyer_erp = $row['idbuyer_erp'];
			if( $idbuyer_erp != '')
			 {
			 $nn = $this->select2("SELECT idbuyer FROM buyer WHERE iderp='$idbuyer_erp'");
			 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'acheteur iderp='$idbuyer_erp' dans order iderp='$iderp'"; 
			 else
				{
				$row2 =  $this->getrow2();
				$idbuyer = $row2['idbuyer'];
				$this->update("UPDATE `order` SET idbuyer='$idbuyer' WHERE iderp='$iderp'");
				//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
				}
			 $this->free_result2();
			 }
			}
		$this->free_result();
		//ORDER-----DELIVERY
		$n = $this->select('SELECT iderp,iddelivery_erp FROM `order` WHERE iddelivery_erp>0 AND iddelivery=0');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$iderp = $row['iderp'];
			$iddeliveryr_erp = $row['iddelivery_erp'];
			if( $iddeliveryr_erp != '')
			 {
			 $nn = $this->select2("SELECT iddelivery FROM delivery WHERE iderp='$iddeliveryr_erp'");
			 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'adresse iderp='$iddeliveryr_erp' dans order iderp='$iderp'"; 
			 else
				{
				$row2 =  $this->getrow2();
				$iddelivery = $row2['iddelivery'];
				$this->update("UPDATE `order` SET iddelivery='$iddelivery' WHERE iderp='$iderp'");
				//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
				}
			 $this->free_result2();
			 }
			}
		$this->free_result();
		
		
		//ORDER_ROW-----ORDER
		$n = $this->select('SELECT distinct idorder_erp FROM order_row WHERE idorder=0 or idorder is NULL');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$iderp = $row['idorder_erp'];
			if( $iderp != '')
			 {
			 $nn = $this->select2("SELECT idorder FROM `order` WHERE iderp='$iderp'");
			 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver la commande iderp='$iderp'"; 
			 else
				{
				$row2 =  $this->getrow2();
				$idorder = $row2['idorder'];
				$this->update("UPDATE order_row SET idorder='$idorder' WHERE idorder_erp='$iderp'");
				}
			 $this->free_result2();
			 }
			}
		$this->free_result();
		
		
		
		
		//ESTIMATE----BUYER
		$n = $this->select('SELECT iderp,idbuyer_erp FROM estimate WHERE idbuyer=0 OR idbuyer is NULL');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$iderp = $row['iderp'];
			$idbuyer_erp = $row['idbuyer_erp'];
			if( $idbuyer_erp != 0)
			 {
			 $nn = $this->select2("SELECT idbuyer FROM buyer WHERE iderp='$idbuyer_erp'");
			 if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'acheteur iderp='$idbuyer_erp' dans estimate iderp='$iderp'"; 
			 else
				{
				$row2 =  $this->getrow2();
				$idbuyer = $row2['idbuyer'];
				$this->update("UPDATE estimate SET idbuyer='$idbuyer' WHERE iderp='$iderp'");
				//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
				}
			 $this->free_result2();
			 }
			}
		$this->free_result();
		
		//ESTIMATE-----DELIVERY
		$n = $this->select('SELECT iderp,iddelivery_erp FROM estimate WHERE iddelivery_erp>0 AND iddelivery=0');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$iderp = $row['iderp'];
			$iddeliveryr_erp = $row['iddelivery_erp'];
			$nn = $this->select2("SELECT iddelivery FROM delivery WHERE iderp='$iddeliveryr_erp'");
			if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver l'adresse iderp='$iddeliveryr_erp' dans estimate iderp='$iderp'"; 
			else
				{
				$row2 =  $this->getrow2();
				$iddelivery = $row2['iddelivery'];
				$this->update("UPDATE estimate SET iddelivery='$iddelivery' WHERE iderp='$iderp'");
				//echo "UPDATE `order` SET idbuyer=$idbuyer WHERE iderp='$iderp'";
				}
			$this->free_result2();
			}
		$this->free_result();
		
		//ESTIMATE_ROW-----ESTIMATE
		$n = $this->select('SELECT distinct idestimate_erp FROM estimate_row WHERE idestimate=0 or idestimate is NULL');
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			$iderp = $row['idestimate_erp'];
			$nn = $this->select2("SELECT idestimate FROM estimate WHERE iderp='$iderp'");
			if($nn <= 0 ) $this->aErrors[] = "Impossible de trouver la commande iderp='$iderp' dans estimate_row idestimate_erp='".$row['idestimate_erp']."'"; 
			else
				{
				$row2 =  $this->getrow2();
				$idoder = $row2['idorder'];
				$this->update("UPDATE estimate_row SET idestimate='$idoder' WHERE idestimate_erp='$iderp'");
				}
			$this->free_result2();
			}
		$this->free_result();

	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Retourne un identifiant numirique correspondant ` un format donni
	 * @param string un format donni
	 * @return int
	 */
	public function get_format($v){
		
		//int(11) varchar(200) decimal(11,2)
		if( strpos($v,'char') !== FALSE )  return 0;
		if( strpos($v,'int') !== FALSE )  return 1;
		if( strpos($v,'dec') !== FALSE && $this->numformat=='fr') return 2;
		if( strpos($v,'dec') !== FALSE )  return 5;
		if( strpos($v,'float') !== FALSE )  return 2;
		if( strpos($v,'date') !== FALSE )  return 7;
		if( strpos($v,'text') !== FALSE )  return 0;
		if( strpos($v,'autoincrement') !== FALSE )  return 20;
		if( strpos($v,'ignore') !== FALSE )  return -1;
		return 0;
		
	}

//---------------------------------------------------------------------------------

	/**
	 * Formate une valeur donnie pour MySQL
	 * @param string $text la valeur du $i-ihme champ
	 * @param int $i l'index du champ dont est issue la valeur
	 * @return mixed
	 */
	private function sql_field($text,$i){
		
		$text = str_replace("\\",'',$text);
		 switch($this->desc_fields[$i]['fieldformat']) {
		 	//case 0 : return trim($text);
			case 1 : 
			
				if( !empty($this->desc_fields[$i]['trans_table']) ) 
					$text = $this->translate2num($i,$text);
					
				$text = preg_replace("([^-0123456789])", '', $text);
				if( $text == '' ) 
					return 'NULL';
				
				 break;
				 
			case 2 : 
			
				$text = str_replace(',', '.',$text);
				$text = preg_replace("([^-0123456789.])", '', $text);
				
				return $text;
				
			case 3 : 
					
				$text = number_format($text,2,',',' '); //Frangais
				break;
					
			case 4 : 
			
				$text = number_format($text); //Anglais
				break;
				
			case 5 : 
			
				$text = number_format($text, 2, '.', ''); //Math
				break;
				
			case 7 : 
			
				$text = $this->sql_date_format($text); //date
				break;
				
			case 20 : return $this->newid($text);
			
			}
		return "'" . mysqli_escape_string($text) . "'"; ///////str_replace("'","\\'",$text);
	
	}

//---------------------------------------------------------------------------------
	
	/**
	 * Formate une valeur donnie pour l'export au format MS EXCEL
	 * @param string $text la valeur du $i-ihme champ
	 * @param int $i l'index du champ dont est issue la valeur
	 * @return mixed
	 */
	private function excel_field($text,$i){
		
		 switch($this->desc_fields[$i]['fieldformat']) {
		 	case 0 : return trim($text);
			case 1 : return preg_replace("([^-0123456789])", '', $text);
			case 2 : return number_format($text,2,',',' '); //Frangais
			case 3 : $text = str_replace(',', '.',$text);
				$text = preg_replace("([^-0123456789.])", '', $text);
				break;
			case 4 : return number_format($text); //Anglais
			case 5 : return number_format($text, 2, '.', ''); //Math
			case 7 : return $this->csv_date_format($text,$i); //date
			case 20 : return preg_replace("([^-0123456789])", '', $text);//$this->newid();
			}
		return $text;
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Formate une valeur donnie pour l'export au format CSV
	 * @param string $text la valeur du $i-ihme champ
	 * @param int $i l'index du champ dont est issue la valeur
	 * @return mixed
	 */
	public function csv_field($text,$i){
		
		$text = str_replace("\\",'',$text);
		 switch($this->desc_fields[$i]['fieldformat']) {
		 	case 0 : return '"'.str_replace('"','""',trim($text)).'"';
			case 1 : if( $this->idreplace && !empty($this->desc_fields[$i]['modify_select_table']) ) return $this->translate2text($i,$text,$text); 
				 return preg_replace("([^-0123456789])", '', $text);
			case 2 : 
			
				if( $this->delimit_char == ',' ) 
					return '"'.number_format($text,2,'.','').'"';
				return number_format($text,2,',',' '); //Frangais
			case 3 : $text = str_replace(',', '.',$text);
				$text = preg_replace("([^-0123456789.])", '', $text);
				break;
			case 4 : return number_format($text); //Anglais
			case 5 : 
				return number_format($text, 2, '.', ''); //Math
			case 7 : return $this->csv_date_format($text,$i); //date
			case 20 : return preg_replace("([^-0123456789])", '', $text);//$this->newid();
			}
		return '"'.str_replace('"','""',$text).'"';
	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Convertit une date donnie au format SQL
	 * @param string $d une date
	 * @return string une date au format Mysql ( YYYY-mm-dd )
	 */
	private function sql_date_format($d){
		
		if (preg_match ("([0-9]{1,4})[^0-9]([0-9]{1,2})[^0-9]([0-9]{1,4})", $d, $regs)) {
		   // echo "$regs[3].$regs[2].$regs[1]";
		    if( $regs[3] > 1900 ) return sprintf("%d-%02d-%02d",$regs[3],$regs[2],$regs[1]);
		    if( $regs[1] > 1900 ) return sprintf("%d-%02d-%02d",$regs[1],$regs[2],$regs[3]); // 
		    if( strpos($d,'/') !== FALSE ) return sprintf("%d-%02d-%02d",$regs[3]+2000,$regs[2],$regs[1]); // french format
		    if( $this->default_date_fr ) return sprintf("%d-%02d-%02d",$regs[3]+2000,$regs[2],$regs[1]); // french format
		    else return sprintf("%4d-%02d-%02d",$regs[3]+2000,$regs[1],$regs[2]);// english format
		} else {
			return $this->Error('Dans ' . $this->csv_file . ' Format de date invalide : '. $d );
		    //echo "Format de date invalide : $d"; 
		}
		//return $d;
	
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Convertit une date en fonction de la langue utilisie
	 * @param string $d la date ` formater
	 * @param string $i 
	 * @return string une date formatie dans la langue actuelle
	 */
	private function csv_date_format($d,$i){
		
		if (preg_match ("([0-9]{1,4})[^0-9]([0-9]{1,2})[^0-9]([0-9]{1,4})", $d, $regs)) {
		   // echo "$regs[3].$regs[2].$regs[1]";
		    if( $regs[1] < 1900 ) $regs[1]+=2000;
			if($this->dateformat=='fr') return sprintf("%02d/%02d/%d",$regs[3],$regs[2],$regs[1]);
		    if($this->dateformat=='en') return sprintf("%02d/%02d/%d",$regs[2],$regs[3],$regs[1]+2000);
			return sprintf("%d-%02d-%02d",$regs[1],$regs[2],$regs[3]);
		} else {
		    // echo "Format de date invalide : $d"; 
		   $this->Error('Dans (' . $this->table . ')['.$this->desc_fields[$i]['fieldname'].'] Format de date invalide : '. $d );
		}
		return $d;
	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Remplace les virgules, apostrophes et quotes... par d'autres codes
	 * @param string $text le texte ` traiter
	 * @return string
	 */
	private function clean_file($text){
	
		$rows = explode($this->linebreak_char,$text);
		$return = '';
		while(list($key,$val)=each($rows)){
		//replace commas apostrophies and quotes.... these are flipped back to propper values in the create_sql function	
			$val = preg_replace("/\,\"(.*)([\,])(.*)\"\,/", ',"\\1&comma;\\3",',  $val, -1);
			$val = str_replace("\"\"", "&quote;", $val);
			$val = str_replace("'", "&appos;", $val);
			$return .= $val."\n";
		}
		return($return);
	
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ginhre un fichier Excell
	 * @return void
	 */
	function ExcelOpen($file){
$header = <<<EOH
<html xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel"
xmlns="http://www.w3.org/TR/REC-html40">

<head>
<meta http-equiv=Content-Type content="text/html; charset=us-ascii">
<meta name=ProgId content=Excel.Sheet>
<!--[if gte mso 9]><xml>
 <o:DocumentProperties>
  <o:LastAuthor>Interact</o:LastAuthor>
  <o:Created>2005-07-14T08:47:23Z</o:Created>
  <o:LastSaved>2005-07-14T08:59:23Z</o:LastSaved>
  <o:Version>10.2625</o:Version>
 </o:DocumentProperties>
 <o:OfficeDocumentSettings>
  <o:DownloadComponents/>
 </o:OfficeDocumentSettings>
</xml><![endif]-->
<style>
<!--table
	{mso-displayed-decimal-separator:"\,";
	mso-displayed-thousand-separator:"\ ";}
@page
	{margin:1.0in .75in 1.0in .75in;
	mso-header-margin:.5in;
	mso-footer-margin:.5in;}
tr
	{mso-height-source:auto;}
col
	{mso-width-source:auto;}
br
	{mso-data-placement:same-cell;}
.style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:windowtext;
	font-size:10.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Arial;
	mso-generic-font-family:auto;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:solid grey 1pt;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
.xl24
	{mso-style-parent:style0;
	white-space:normal;}
.xl25
        {mso-style-parent:style0;
        mso-number-format:General;
        text-align:right;
        white-space:normal;}
.xl26
        {mso-style-parent:style0;
        font-weight:700;
        font-family:Arial, sans-serif;
        mso-font-charset:0;
        text-align:center;
        background:#DDDDDD;
        mso-pattern:auto none;
        white-space:normal;}
.xl27
        {mso-style-parent:style0;
        mso-number-format:"\#\,\#\#0\.00\\ \0022\20AC\0022";}
.xl28
        {mso-style-parent:style0;
        mso-number-format:"d\/m\/yyyy";}
.xl29
        {mso-style-parent:style0;
        mso-number-format:Fixed;
        text-align:right;
        white-space:normal;}
-->
</style>
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>
   <x:ExcelWorksheet>
	<x:Name>feuille 1</x:Name>
	<x:WorksheetOptions>
	 <x:Selected/>
	 <x:ProtectContents>False</x:ProtectContents>
	 <x:ProtectObjects>False</x:ProtectObjects>
	 <x:ProtectScenarios>False</x:ProtectScenarios>
	</x:WorksheetOptions>
   </x:ExcelWorksheet>
  </x:ExcelWorksheets>
  <x:WindowHeight>10005</x:WindowHeight>
  <x:WindowWidth>10005</x:WindowWidth>
  <x:WindowTopX>120</x:WindowTopX>
  <x:WindowTopY>135</x:WindowTopY>
  <x:ProtectStructure>False</x:ProtectStructure>
  <x:ProtectWindows>False</x:ProtectWindows>
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>

<body link=blue vlink=purple>
<table x:str border=0 cellpadding=0 cellspacing=0 style='border-collapse: collapse;'>
EOH;

		if( is_file($file ) )  unlink($file);
		$this->fp=fopen($file,'wb');
		if(!$this->fp) return $this->Error('Unable to create output file: '.$file);
		fwrite($this->fp,$header);
		
		return $header;
		
	}

	//---------------------------------------------------------------------------------
	
	/**
	 * Ferme le fichier excell
	 * @return void
	 */
	private function ExcelClose(){
		
		fwrite($this->fp,"</table>\n</body>\n</html>\n");
		fclose($this->fp);
	
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Ecrit une ligne dans le fichier Excell
	 * @param string $line_arr la ligne ` icrire
	 * @return void
	 */
	private function writeExcelRow($line_arr){
		
		fwrite($this->fp,"<tr>");
		foreach($line_arr as $col)
			fwrite($this->fp,"<td class=xl24 width=120 >$col</td>");
		fwrite($this->fp,"</tr>");
	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Ginhre un fichier Excell
	 * @return void
	 */
	public function excel(){
		
		$this->do_filter();
		$this->mime_type = 'application/msexcel';
		$this->ExcelOpen($this->csv_file); //$handle = fopen($this->csv_file, 'wb');
		$m = count($this->desc_fields);
		$fieldlist='';$virg='';
		 for($j=0;$j<$m;$j++) 
			{
			if( $this->desc_fields[$j]['fieldformat'] >= 0 )
				{
				$fieldlist .= $virg  . $this->desc_fields[$j]['fieldname'] ;
				$virg=',';
				} 
			}
		$query= "SELECT * FROM " . $this->tables[0]['tablename'] .' '. $this->export_filter ;
		$n = $this->select($query);
		
		
		for($j=0;$j<$m;$j++) 
			if( $this->desc_fields[$j]['fieldformat'] >= 0 ) 
				fwrite($this->fp,"<col width=138 style='mso-width-source:userset;mso-width-alt:5046;width:104pt'>\n");
		
		
		fwrite($this->fp,"<tr>\n");
		for($j=0;$j<$m;$j++) if( $this->desc_fields[$j]['fieldformat'] >= 0 ) fwrite($this->fp,"<td class=xl26 width=120>".$this->desc_fields[$j]['export_name']."</td>\n");
		fwrite($this->fp,"</tr>\n");
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();
			fwrite($this->fp,"<tr>\n");
			for($j=0;$j<$m;$j++)
				{
				if( $this->desc_fields[$j]['fieldformat'] >= 0 )
					{
					$f = $this->desc_fields[$j]['fieldname'];
					//$v = $this->excel_field($row[$f],$j);
					$v = str_replace('>','&gt;',str_replace('<','&lt;',$this->excel_field($row[$f],$j)));
					if($this->desc_fields[$j]['fieldformat'] == 2 )  fwrite($this->fp,"<td class=xl29 align=right x:num=\"".$row[$f]."\" width=64 >$v</td>\n");
					else if($this->desc_fields[$j]['fieldformat'] == 1 )  fwrite($this->fp,"<td class=xl25 align=right width=64 x:num>$v</td>\n");
					else if($this->desc_fields[$j]['fieldformat'] == 7 )  
						{
						$t = floor( mktime(7, 0, 0, substr($row[$f],5,2),substr($row[$f],8,2),substr($row[$f],0,4)) / 86400 )+25569;
						fwrite($this->fp,"<td class=xl28 align=right  width=64 x:num=\"$t\" >$v</td>\n");
						}
					else fwrite($this->fp,"<td class=xl24 width=150 >$v</td>\n");
					}
				}
			fwrite($this->fp,"</tr>\n");
			}
			$this->ExcelClose();

	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Exporte les statistiques sur les modifications des donnies au format csv
	 * @return void
	 */
	public function exportstat(){
	
		$this->mime_type = 'text/x-csv';
		$csv__fields = array('idsession','DateHeure','action','quantity','idcategory','url','parmsearch','reference','username');
		$sql__fields = array('idsession','DateHeure','action','quantity','idcategory','url','parmsearch','reference','username');
		$sql__formats = array(0,7,0,1,1,0,0,0,0);
		for($i=0;$i<9;$i++) { $this->desc_fields[$i]['fieldformat']=0;$this->desc_fields[$i]['trans_table']=''; }
		$this->desc_fields[1]['fieldformat']=7;$this->desc_fields[3]['fieldformat']=1;$this->desc_fields[4]['fieldformat']=1;
		$m = count($csv__fields);
		$avant_yier = mktime(0, 0, 0, date('m'), date('d')-2, date('Y'));
		$yier =  mktime(0, 0, 0, date('m'), date('d')-1, date('Y'));
		if( $this->csv_file == '' ) $this->csv_file = $this->dataoutdir .'stat_'. date("d-m",$avant_yier) . date("_d-m-Y",$yier) .  '.csv';
		else $this->csv_file .= date("d-m",$avant_yier) . date("_d-m-Y",$yier) .  '.csv';
		if( is_file($this->csv_file ) )  unlink($this->csv_file);
		$handle = fopen($this->csv_file, 'wb');
		if(!$handle) return $this->Error('Unable to create output file: '.$this->csv_file);
		$avant_yier = mktime(0, 0, 0, date('m'), date('d')-2, date('Y'));
		$yier =  mktime(0, 0, 0, date('m'), date('d')-1, date('Y'));
		$this->table = 'stat'. date('m',$avant_yier);
		$n = $this->select('SELECT * FROM '.$this->table." WHERE DateHeure like '". date("Y-m-d",$avant_yier) . "%'" );
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();$virg='';
			for($j=0;$j<$m;$j++)
				{
				if( $sql__formats[$j] >= 0 )
					{
					$f = $sql__fields[$j];
					$v = $this->csv_field($row[$f],$j);
					fwrite($handle,$virg.$v);
					$virg=$this->delimit_char;
					}
				}
			fwrite($handle, $this->linebreak_char);
			}
		$this->free_result();
		$this->table = 'stat'. date('m',$yier);
		$n = $this->select('SELECT * FROM '.$this->table." WHERE DateHeure like '". date("Y-m-d",$yier) . "%'" );
		for($i=0;$i<$n;$i++)
			{
			$row =  $this->getrow();$virg=0;
			for($j=0;$j<$m;$j++)
				{
				if( $sql__formats[$j] >= 0 )
					{
					$f = $sql__fields[$j];
					$v = $this->csv_field($row[$f],$j);
					if($virg) fwrite($handle,$this->delimit_char.$v);
					$virg=1;
					}
				}
			fwrite($handle, $this->linebreak_char);
			}
		$this->free_result();
		fclose($handle);
		/*
		idsession varchar(50) NOT NULL default '',
		  DateHeure varchar(10) NOT NULL default '',
		  action varchar(50) NOT NULL default '',
		  quantity int(11) NOT NULL default '0',
		  idcategory int(11) NOT NULL default '0',
		  url varchar(50) NOT NULL default '',
		  parmsearch varchar(50) NOT NULL default '',
		  reference varchar(50) NOT NULL default '',
		  username varchar(10) NOT NULL default ''
		  'idsession','DateHeure','action','quantity','idcategory','url','parmsearch','reference','username'
		*/
		
	}

	//---------------------------------------------------------------------------------

	/**
	 * Envoit les donnies
	 * @param string $name
	 * @param string $dest
	 * @return void
	 */
	public function Output($name='',$dest=''){
	
		//Output to some destination
		$dest=strtoupper($dest);
		if($dest=='')
		{
			if($name=='')
			{
				$name='feuille.xls';
				$dest='I';
			}
			else
				$dest='F';
		}
		switch($dest)
		{
			case 'I':
				//Send to standard output
				//ob_clean();
				if(ob_get_contents())
					return $this->Error('Some data has already been output, can\'t send file');
				if(php_sapi_name()!='cli')
				{
				//We send to a browser
				///////header('Content-Type: application/msexcel');
				header('Content-Type: '.$this->mime_type);
				if(headers_sent())
					return $this->Error('Some data has already been output to browser, can\'t send file');
				/////////header('Content-Length: '.strlen($this->buffer));
				header('Content-disposition: inline; filename="'.$name.'"');
				}
				//echo $this->buffer;
				readfile($this->csv_file); exit();
				break;
			case 'D':
				//Download file
				ob_clean();
				if(ob_get_contents())
					return $this->Error('Some data has already been output, can\'t send file');
				//if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
				//	header('Content-Type: application/force-download');
				//else
				//	header('Content-Type: application/octet-stream');
				header('Content-Type: '.$this->mime_type);
				if(headers_sent())
					return $this->Error('Some data has already been output to browser, can\'t send file');
				///////header('Content-Length: '.strlen($this->buffer));
				header('Content-disposition: attachment; filename="'.$name.'"');
				readfile($this->csv_file); exit();//echo $this->buffer;
				break;
			case 'Z':
				//Download Zip file
				ob_clean();
				if(ob_get_contents())
					return $this->Error('Some data has already been output, can\'t send file');
				if(isset($_SERVER['HTTP_USER_AGENT']) && strpos($_SERVER['HTTP_USER_AGENT'],'MSIE'))
					header('Content-Type: application/force-download');
				else
					header('Content-Type: application/octet-stream');
				if(headers_sent())
					return $this->Error('Some data has already been output to browser, can\'t send file');
				///////header('Content-Length: '.strlen($this->buffer));
				header('Content-disposition: attachment; filename="'.$name.'.gz"');
				$data = implode('', file($this->csv_file));
				echo gzencode($data, 9);exit();
				//readfile($this->csv_file); //echo $this->buffer;
				break;
			case 'F':
				ob_clean();
				//Save to local file
				$f=fopen($name,'wb');
				if( is_file($name) )  unlink($name);
				if(!$f) return $this->Error('Unable to create output file: '.$name);
				$data = implode('', file($this->csv_file));
				fwrite($f,$data,strlen($data));
				fclose($f);
				break;
			case 'S':
				//Return as a string
				readfile($this->csv_file); //return $this->buffer;
			default:
				return $this->Error('Incorrect output destination: '.$dest);
		}
		return '';

	}
	
	//---------------------------------------------------------------------------------

	//Error handler
	/**
	 * Ajoute un message d'erreur ` la liste
	 * @param string $msg le message d'erreur
	 * @return bool false dans tous les cas
	 */
	public function Error($msg){
		
		$this->aErrors[] = $msg;
		return FALSE;
		
	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Retourn true s'il y a eu des erreurs durant l'import/export, sinon false
	 * @return bool
	 */
	public function iserror(){
		
	 return count($this->aErrors);
	 
	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Ricuphre la liste des erreurs survenues durant l'import/export
	 * @return array un tableau indici
	 */
	public function geterrors(){
	 return $this->aErrors;
	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Affiche la liste des erreurs survenues durant l'import/export
	 * @return void
	 */
	public function report(){
		
	 $n = count($this->aErrors);
	 for($i=0;$i<$n;$i++) echo "<b>" . $this->aErrors[$i],"</b><br />\n";
	}
	
	//---------------------------------------------------------------------------------

	/**
	 * Difinit le type MIME ` utiliser
	 * @param string $mime_type le type MIME ` utiliser
	 * @return void
	 */
	public function setMimeType( $mime_type ){
		
		$this->mime_type = $mime_type;
		
	}
	
	//---------------------------------------------------------------------------------
	
	/**
	 * Affiche le contenu d'un fichier CSV avec quelques informations supplimentaires
	 * @return void
	 */
	public function dump(){
		
		$row = 1;
		$handle = fopen($this->csv_file, 'rb');
		echo "Fichier : $this->csv_file\n";
		echo "---------------------------------------\n";
		while (($data = fgetcsv($handle, 8100, ",")) !== FALSE) {
		    $num = count($data);
		    echo "<p> $num fields in line $row: <br /></p>\n";
		    $row++;
		    echo "---------------------------------------\n";
		    for ($c=0; $c < $num; $c++) {
		        echo $data[$c] . "\n";
		    }
		}
		
		fclose($handle);
		
	}
	
	//---------------------------------------------------------------------------------


}//EOC
?>
