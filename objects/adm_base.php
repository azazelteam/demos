<?php 
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Objet gestion des tables de la base de données
 * Gestion de l'accès à la base de données et des messages multilangues
  */
class ADMBASE
{

	protected $LingualNumber = '1';
	protected $DEBUG = 'none';
	
	/**
	 * Constructeur
	 */
	function __construct(){
	}
	
	/**
	 * Définit la langue à utiliser
	 * @param int $l le numéro de la langue à utiliser
	 * @return void
	 */
	public function setlang( $l ) {$this->LingualNumber=$l;}

	/**
	 * Retourne la langue utilisée
	 * @return string le numéro de la langue préfixé par un underscore
	 */
	public function getlang() { return '_'.$this->LingualNumber;}
		
	/**
	 * Retourne un tableaun indicé avec les noms de toutes les langues
	 * @return array
	 */
	public function AllowedLanguages()
	{
		$Arr_Result=array();
		for ( $i=1 ; $i<=5; $i++ )
			{
				$data = DBUtil::getConnection()->GetRow("select paramvalue from parameter_admin WHERE idparameter='lang$i' ") ;
				if ( $data ) $Arr_Result[] = $data['paramvalue'];
			}
		return $Arr_Result;
	}

	/**
	 * Retourne le résultat d'une requête sql sous forme de tableau
	 * @param $Str_NameTable le nom de la table
	 * @param $Str_FieldName le nom de la colonne
	 * @param $Str_Condition la condition
	 * @return array un tableau associatif
	 */
	public function GetAllTable( $Str_NameTable, $Str_FieldName='*', $Str_Condition='')
	{
		$SQL_Query = "SELECT $Str_FieldName FROM $Str_NameTable $Str_Condition";
		$data = DBUtil::getConnection()->GetAll($SQL_Query);//print_r($data);
		return $data;
	}
		
	/**
	 * Retourne un paramètre admin
	 * @param string $ParmName le nom du paramètre
	 * @return mixed la valeur du paramètre ou 0
	 */
	public function GetParameterAdmin( $ParmName )
	{
		return DBUtil::getDBValue( "paramvalue", "parameter_admin", "idparameter", $ParmName );
	}
	
	/**
	 * Retourne un paramètre catalogue
	 * @param string $ParmName le nom du paramètre à récupérer
	 * @return mixed la valeur du paramètre ou 0
	 */
	public function GetParameter($ParmName)
	{
		return DBUtil::getDBValue( "paramvalue", "parameter_cat", "idparameter", $ParmName );
	}
	
	/**
	 * Sérialise et encode une variable en base 64
	 * @param mixed $Mixed_var la variable à encoder
	 * @return la variable $Mixed_var encodée en base64
	 */
	public function SerialEncode($Mixed_var)
	{
		return base64_encode(serialize($Mixed_var));
	}
	
	/**
	 * Décode et désérialise une chaine
	 * @param string $Str_string la chaîne à décoder et désérialiser
	 * @return string la variable $Str_string décodée et désérialisée
	 */
	public function SerialDecode($Str_String)
	{
		return unserialize(base64_decode($Str_String));
	}
	
	/**
	 * Décode et désérialise le premier élément d'un tableau qui est lui même un tableau
	 * @param $k ???
	 * @return $k décodée et désérialisée
	 */
	function CreateModifyPattern($k)
	{
		$vec = $this->SerialDecode($k[0]);
		return $vec;
	}
	
}//EOC
?>