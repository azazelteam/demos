<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object contact client
*/

interface Contact{
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* getters */
	
	/**
	 * @return int
	 */
	public function getId();
	/**
	 * @return int ( cf. `title`.idtitle` )
	 */
	public function getGender();
	/**
	 * @return string
	 */
	public function getFirstName();
	/**
	 * @return string
	 */
	public function getLastName();
	/**
	 * @return string
	 */
	public function getFaxnumber();
	/**
	 * @return string
	 */
	public function getPhonenumber();
	/**
	 * @return string
	 */
	public function getGSM();
	/**
	 * @return string
	 */
	public function getEmail();
	/**
	 * @return string
	 */
	public function getComment();
	/**
	 * @return int ( cf. `department`.iddepartment` )
	 */
	public function getDepartment();
	/**
	 * @return int ( cf. `function`.idfunction` )
	 */
	public function getFunction();
	/**
	 * @return string
	public function getAddress();
	 * @return string
	public function getAddress2();
	/**
	 * @return int
	public function getZipcode();
	/**
	 * @return string
	public function getCity();
	/**
	 * @return int ( cf. `state`.idstate` )
	public function getState();
	/**
	 * @return bool
	 */
	public function allowDelete();
	/**
	 * @return bool
	 */
	public function isMainContact();

}

?>