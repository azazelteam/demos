<?

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object codes NAF
 */
 

include_once( dirname( __FILE__ ) . "/../config/init.php" );

class NafExplorer {
	
	private $currentLevel;
	private $childrens;
	
	public function __construct( $current = 0 , $fullArbo = false ){
		
		$this->currentLevel = $current;
		if( $fullArbo !== false )
			$this->createFullChildrens();
		else
			$this->createChildrens();
			
	}
	
	public function get(){
		
		return $this->childrens;
		
	}
	
	/**
	 * Création des codes Naf Enfants du Naf courant
	 * @return array();
	 */
	private function createChildrens(){
		
		$nafChilds =& DBUtil::query( "SELECT * FROM `naf` WHERE naf_parent = '" . $this->currentLevel . "'" );
		
		if( !$nafChilds->RecordCount() )
			return;
		
		while( !$nafChilds->EOF() ){
			
			$this->childrens[ $nafChilds->fields( 'idnaf' ) ] = $nafChilds->fields( 'naf_comment' );
			
			$nafChilds->MoveNext();
		}
		
	}
	
	/**
	 * Création récursivement de tous les Naf Enfants
	 * @return array();
	 */
	private function createFullChildrens(){
		
		$nafChilds =& DBUtil::query( "SELECT * FROM `naf` WHERE naf_parent = '" . $this->currentLevel . "'" );
		
		if( !$nafChilds->RecordCount() )
			return;
			
		while( !$nafChilds->EOF() ){
			
			$this->childrens[ $nafChilds->fields( 'idnaf' ) ][ 'name' ] = $nafChilds->fields( 'naf_comment' );
			$newChild = new NafExplorer( $nafChilds->fields( 'idnaf' ) , true );
			$this->childrens[ $nafChilds->fields( 'idnaf' ) ][ 'childs' ] = $newChild->get();
			
			$nafChilds->MoveNext();
		}
	
	}
	
	public function getHTML( $aCallback = 'return false;' , $href = '#' ){
		
		$html = $this->createNafMenu( $this->childrens , '' , $aCallback , $href );
		
		return $html;
	
	}

	private function createNafMenu( $nafTable , $html = '' , $aCallback = 'return false;' , $href = '#'){
				
		if( count( $nafTable ) && is_array( $nafTable ) ){
		
			foreach( $nafTable as $idnaf => $values ){
				
				$parsedCallback = str_replace( '{idnaf}' , $idnaf , $aCallback );
				
				$html .= "<ul>\n";
				
				$html .= "	<li><a href=\"$href\" onclick=\"$parsedCallback\">" . $idnaf . " : " . $values[ 'name' ] . "</a></li>\n";
					
				$html .= $this->createNafMenu( $values[ 'childs' ] );
				
		
				$html .= "</ul>\n";
		
			}
		
		}
		
		return $html;
	}
	
	public static function getNafHtmlSelect( $nafParent = 0 , $idHtml = "nafSelect" , $class , $callback = "" ){
		
		$naf = new NafExplorer( $nafParent );
		
		$nafarray = $naf->get();
		$select = "";
		
		if( count( $nafarray ) ){
			$select = "<select name='$idHtml' id='$idHtml' class='$class' onchange=\"".$callback."\">";
			$select .= "<option value=''>Tous</option>";
			$i = 1;
			foreach( $nafarray as $idnaf => $values ){
				
				$select .= "<option value='$idnaf'>$i - " . $values . "</option>";
				$i++;
			}
			
			$select .= "</select>"; 
		}
		
		return $select;	
	}
	
}