<?

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Classe permettant d'exporter au format xls partir de données 
 * de type sql ou php array()en tableau html.
 * Fait à partir d'une astuce qui consiste à un fichier excel 
 * avec une simple page .html qu'on renomme en .xls.
 **************************************************************/
 
define( "DEBUG_HTML", 0 );

class HTML_XLS {
	
	private $trtArray;
	private $fileName;
	private $htmlCode;
	private $privateCSS;
	private $fileTitle;
	
	
	/**
	 * Constructeur
	 * @param array() $arrayFields sql_recordset a traiter
	 * @param char $fileN nom du fichier
	 */
	function __construct( $arrayFields , $fileN = "" , $title = "" ){
		
		if( $arrayFields === false ){
			return false;
		}
		
		$this->trtArray = $arrayFields;		
		$this->htmlCode = "";
		$this->privateCSS = "";
		$this->createFileName($fileN);
		if($title)
			$this->fileTitle = $title;
		else
			$this->fileTitle = $this->fileName;
	}

	/**
	 * Fonction qui génère le code de la page html
	 */
	private function generateHTML(){
		
		global $GLOBAL_START_URL,$GLOBAL_START_PATH;
		
		$cssLink = "$GLOBAL_START_URL/css/back_office/export/default_export.css";
		if( $this->privateCSS != "" ){
			$cssPath = "$GLOBAL_START_PATH/css/back_office/export/".$this->privateCSS;
			if( file_exists($cssPath) )
				$cssLink = "$GLOBAL_START_URL/css/back_office/export/".$this->privateCSS;
		}
		
		$myhtml = "<html xmlns:o=\"urn:schemas-microsoft-com:office:office\"
					xmlns:x=\"urn:schemas-microsoft-com:office:excel\"
					xmlns=\"http://www.w3.org/TR/REC-html40\">\n";
		$myhtml .= "<head>\n
					<meta http-equiv=Content-Type content=\"text/html; charset=UTF-8\">
					<meta name=ProgId content=Excel.Sheet>
					<meta name=Generator content=\"Microsoft Excel 11\">
					";
		$myhtml .= "	<link rel='stylesheet' type='text/css' href='$cssLink' />\n";
		
		if(is_array($this->trtArray)){
			$ncols = count($this->trtArray);
		}else{
			$ncols = $this->trtArray->FieldCount();
		}
		
		if($ncols > 4){
			$myhtml .= "<style>
				@page{
					mso-page-orientation:landscape;
				}
			</style>";
		}
		$myhtml .="
					<!--[if gte mso 9]><xml>
					 <x:ExcelWorkbook>
					  <x:ExcelWorksheets>
					   <x:ExcelWorksheet>
					    <x:Name>".$this->fileTitle."</x:Name>
					    <x:WorksheetOptions>
						 <x:DoNotDisplayGridlines/>
					     <x:DefaultColWidth>10</x:DefaultColWidth>
					     <x:Print>
					      <x:ValidPrinterInfo/>
					      <x:PaperSizeIndex>9</x:PaperSizeIndex>
					      <x:HorizontalResolution>600</x:HorizontalResolution>
					      <x:VerticalResolution>0</x:VerticalResolution>
					      <x:HeaderPicture>
					       <x:Location>Left</x:Location>
					       <x:Source>http://www.akilae-saas.fr/images/front_office/global/logo.png</x:Source>
					       <x:Height>24</x:Height>
					       <x:Width>84</x:Width>
					       <x:LockAspectRatio/>
					       <x:ColorType>Automatic</x:ColorType>
					      </x:HeaderPicture>
					     </x:Print>
					     <x:PageBreakZoom>60</x:PageBreakZoom>
					     <x:Selected/>
					     <x:Panes>
					      <x:Pane>
					       <x:Number>3</x:Number>
					       <x:ActiveRow>4</x:ActiveRow>
					       <x:ActiveCol>3</x:ActiveCol>
					      </x:Pane>
					     </x:Panes>
					     <x:ProtectContents>False</x:ProtectContents>
					     <x:ProtectObjects>False</x:ProtectObjects>
					     <x:ProtectScenarios>False</x:ProtectScenarios>
					    </x:WorksheetOptions>
					   </x:ExcelWorksheet>
					  </x:ExcelWorksheets>
					  <x:WindowHeight>12270</x:WindowHeight>
					  <x:WindowWidth>15195</x:WindowWidth>
					  <x:WindowTopX>480</x:WindowTopX>
					  <x:WindowTopY>60</x:WindowTopY>
					  <x:ProtectStructure>False</x:ProtectStructure>
					  <x:ProtectWindows>False</x:ProtectWindows>
					 </x:ExcelWorkbook>
					 <x:ExcelName>
					  <x:Name>Print_Titles</x:Name>
					  <x:SheetIndex>1</x:SheetIndex>
					  <x:Formula>='".$this->fileTitle."'!$1:$2</x:Formula>
					 </x:ExcelName>
					</xml><![endif]--><!--[if gte mso 9]><xml>
					 <o:shapedefaults v:ext=\"edit\" spidmax=\"1025\"/>
					</xml><![endif]-->

					";
		$myhtml .= "</head>\n";
		$myhtml .= "<body>\n";
		$myhtml .= "	<table class='table'>\n";
		$myhtml .= "		<tr style=\"border:none;\">\n";
		
		$myhtml .= "			<td colspan=\"".$ncols."\" style=\"font-size:12.0pt;\" class=\"pageTitle\">".$this->fileTitle."</td>";

		$myhtml .= "		</tr>\n";
		if(is_array($this->trtArray)){
			// on a entré un tableau php
			
			$nbrows = 0;
			
			// entetes
			$myhtml .= "		<tr>\n";
			$cpt=0;
			foreach( $this->trtArray as $colName => $values ){
				if($cpt==0)
					$myhtml .= "			<th class=\"theadLefter\">".htmlentities($colName)."</th>\n";
				else if($cpt==$ncols-1)
					$myhtml .= "			<th class=\"theadRighter\">".htmlentities($colName)."</th>\n";
				else
					$myhtml .= "			<th class=\"theadNormal\">".htmlentities($colName)."</th>\n";
				
				$cpt++;
				$nbrows = count($values);
			}
			$myhtml .= "		</tr>\n";
			
			
			for($i=0;$i<$nbrows;$i++){
				$myhtml .= "		<tr>\n";
				$a = 0;
				foreach( $this->trtArray as $colName => $values){
					$value = $values[$i];
					$style="";
					
					if(eregi("¤",$value) && strlen($value)<11 )
						$style = "text-align:right;mso-number-format:<<CHR(34)>>_-* \#\,\#\#0\.00\\ \0022¤\0022_-\;\\-* \#\,\#\#0\.00\\ \0022¤\0022_-\;_-* \0022-\0022??\\ \0022¤\0022_-\;_-\@_-\<<CHR(34)>>;";
					else
						$style = "text-align:right;mso-number-format:<<CHR(34)>>_-* \#\,\#\#0\.00\\ \0022¤\0022_-\;\\-* \#\,\#\#0\.00\\ \0022¤\0022_-\;_-* \0022-\0022??\\ \0022¤\0022_-\;_-\@_-\<<CHR(34)>>;";
						
					if($i==$nbrows-1){
						if($a==0)
							$myhtml .= "			<td class=\"tableLastLineLeft\" style=\"".$style."\">";
						else if($a == $ncols-1)
							$myhtml .= "			<td class=\"tableLastLineRight\" style=\"".$style."\">";
						else
							$myhtml .= "			<td class=\"tableLastLineNormal\" style=\"".$style."\">";					
					}else{
						if($a==0)
							$myhtml .= "			<td class=\"lefterCol\" style=\"".$style."\">";
						else if($a == $ncols-1)
							$myhtml .= "			<td class=\"righterCol\" style=\"".$style."\">";
						else
							$myhtml .= "			<td class=\"normalCell\" style=\"".$style."\">";					
					}
					
					if(eregi("¤",$value) && strlen($value)<11 )
						$myhtml .= $value;
					else
						$myhtml .= htmlentities($value);
					
					$myhtml .= "</td>\n";
					$a++;
				}
				$myhtml .= "		</tr>\n";
			}		
				
		}else{
			// on a entré un recordset
			// il suffit ici de transformer en tableau html le recordset				
			$ncols = $this->trtArray->FieldCount();
	
			$myhtml .= "		<tr>\n";
			for ($i=0; $i < $ncols; $i++) {
				$field = $this->trtArray->FetchField($i);
				$fieldName = $field->name;
				$desc_field_verif = DBUtil::query("SELECT export_name FROM desc_field WHERE fieldname=\"$fieldName\" LIMIT 1");
				$fieldNameFinal = $desc_field_verif->RecordCount() ? $desc_field_verif->fields('export_name') : $fieldName ;

				if($i==0)
					$myhtml .= "			<th class=\"theadLefter\">".htmlentities($fieldNameFinal)."</th>\n";
				else if($i==$ncols-1)
					$myhtml .= "			<th class=\"theadRighter\">".htmlentities($fieldNameFinal)."</th>\n";
				else
					$myhtml .= "			<th class=\"theadNormal\">".htmlentities($fieldNameFinal)."</th>\n";

			}
			$myhtml .= "		</tr>\n";
	
			$this->trtArray->MoveFirst();
			$numoffset = isset($this->trtArray->fields[0]) || isset($this->trtArray->fields[1]) || isset($this->trtArray->fields[2]);
			while(!$this->trtArray->EOF) {
				$myhtml .= "		<tr>\n";
				
				for ($i=0; $i < $ncols; $i++) {
					if ($i===0) 
						$val = ($numoffset) ? $this->trtArray->fields[0] : reset($this->trtArray->fields);
					else 
						$val = ($numoffset) ? $this->trtArray->fields[$i] : next($this->trtArray->fields);
					
					$key = $this->trtArray->_currentkey;
					$row = $this->trtArray->_currentRow;
					if($row == $this->trtArray->RecordCount()-1){
						if( $i == 0 )
							$myhtml .= "			<td class=\"tableLastLineLeft\">";
						else if( $i == $ncols-1 )
							$myhtml .= "			<td class=\"tableLastLineRight\">";
						else
							$myhtml .= "			<td class=\"tableLastLineNormal\">";					
					}else{
						if( $i == 0 )
							$myhtml .= "			<td class=\"lefterCol\">";
						else if( $i == $ncols-1 )
							$myhtml .= "			<td class=\"righterCol\">";
						else
							$myhtml .= "			<td class=\"normalCell\">";					
					}
						
					$myhtml .= htmlentities($val)."</td>\n";
					
				}
				
				$myhtml .= "		</tr>\n";
				
				$this->trtArray->MoveNext();
			}
		}
		
		$myhtml .= "	</table>\n";
		
		$myhtml .= "</body>\n</html>";
		$this->htmlCode = $myhtml;
	}

	/**
	 * Fonction qui créée le fichier xls
	 */
	public function output(){
		$this->generateHTML();	

		if( DEBUG_HTML )
			die($this->htmlCode);

		// headers pour la création du fichier
		header("Content-Type: application/vnd.ms-excel");
		header("Content-Disposition: attachment; filename=".$this->fileName);
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Expires: 0");

		echo $this->htmlCode;
		
	}

	/**
	 * Fonction pour affecter une feuille de style pour la page en cours (html)
	 * @param char $css la feuille de style à utiliser qui est dans /css/back_office/export/
	 */
	public function setCSS( $css ){
		$this->privateCSS = $css;
	}

	/**
	 * Fonction qui reformate le nom du fichier pour qu'il soit correctement écrit.
	 * @param char $filename nom du fichier
	 */
	private function createFileName( $filename ){
		
		if($filename=="")
			$filename = date('d-m-Y');
			
		$filename = str_replace('&','and',html_entity_decode(strtolower($filename)));
		$filename = preg_replace("/[^a-z0-9A-Z-]/", "-", strtolower($filename)).".xls";
		
		$this->fileName = $filename;
		
	}
	
} //EOC
 
?>