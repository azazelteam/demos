<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object clients
 */
 
include_once( dirname( __FILE__ ) . "/DBUtil.php" );
include_once( dirname( __FILE__ ) . "/User.php" );
include_once( dirname( __FILE__ ) . "/Util.php" );
include_once( dirname( __FILE__ ) . "/CustomerAddress.php" );
include_once( dirname( __FILE__ ) . "/util/ArrayList.php" );
include_once( dirname( __FILE__ ) . "/CustomerContact.php" );
include_once( dirname( __FILE__ ) . "/DBObject.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/TradeFactory.php" );
 //------
class Customer extends DBObject{

	/* ---------------------------------------------------------------------------------------------------- */
	
	/**
	 * @var CustomerAddress $address
	 */
	private $address;
	
	/**
	 * @var ArrayList $contacts
	 */
	private $contacts;
	
	/**
	 * @var CustomerContact $currentContact
	 */
	private $currentContact;
	public $user;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Constructeur
	 * Créé l'object client dont le numéro est donné
	 * Pour créer une nouveau client, utilisez Customer::create()
	 * @param int $idbuyer le numéro du client
	 */
	function __construct( $idbuyer, $idcontact = 0 ){
		
		parent::__construct( "buyer", false, "idbuyer",  $idbuyer  );

		$this->contacts 	= new ArrayList( "CustomerContact" );
		$this->setContacts();
		
		$this->currentContact =& $this->contacts->get( $idcontact );
		//var_dump(  $this->currentContact);exit;
		if($this->currentContact)
		$idcurrentContact = $this->currentContact->getId();
		else
			$idcurrentContact = 0;
		
		$this->address = new CustomerAddress( $this->recordSet[ "idbuyer" ], $idcurrentContact);
		$this->user = $this->getUser();		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Charge les informations du contact
	 * @return void
	 */
	private function setContacts(){

		$query = "
		SELECT * FROM `contact` 
		WHERE `idbuyer` = '" . $this->getId() . "'
		ORDER BY idcontact ASC";
		
		$rs =& DBUtil::query( $query );
		
		while( !$rs->EOF() ){

			$this->contacts->add( new CustomerContact( $this->getId(), $rs->fields( "idcontact" ) ) );
			$rs->MoveNext();
			
		}
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @static
	 * @return Customer
	 */
	public static function create(){

		//Création du client
// ------- Mise à jour de l'id gestion des Index  #1161

		$table = 'buyer';
		$idbuyer = TradeFactory::Indexations($table);

	//--------
	
		$query = "INSERT INTO `buyer` (
		`idbuyer`,
			`iduser`, 
			`create_date`, 
			`lastupdate`, 
			`username`
		 ) VALUES ( 
		   '".$idbuyer."',
			" . DBUtil::quote( DBUtil::getParameter( "contact_commercial" ) ) . ", 
			NOW(), 
			NOW(), 
			" . DBUtil::quote( User::getInstance()->get( "login" ) ) . " 
		)";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false){
			
			trigger_error( "Impossible de créer le nouveau client", E_USER_ERROR );
			exit();
			
		}
		// ------- Mise à jour de l'id gestion des Index  #1161
	///	$idbuyer = DBUtil::getInsertId();
//-----
		$contact = CustomerContact::create( $idbuyer );

		return new Customer( $idbuyer, $contact->getId() );
		
	}
	public function getUser(){
		$users = array();
	
	$query = "SELECT iduser, login, job, firstname, lastname, gender FROM `user` WHERE iduser = ".$this->recordSet[ "iduser" ];
	
	$rs = DBUtil::query( $query );
	
	if( $rs === false ) return [];

		if( $files = glob( dirname( __FILE__ ) . "/../images/users/" . $rs->fields( "iduser" ) . "{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) )
				$photo = "/images/users/" . basename( $files[ 0 ] );
		else 	$photo = intval( $rs->fields( "gender" ) ) < 2 ? "/images/back_office/content/no-pict-m.jpg" : "/images/back_office/content/no-pict-g.jpg";

		$user = array(
		
			"iduser" 	=> $rs->fields( "iduser" ),
			"lastname" 	=> $rs->fields( "lastname" ),
			"firstname" => $rs->fields( "firstname" ),
			"login" 	=> $rs->fields( "login" ),
			"job" 		=> $rs->fields( "job" ),
			"iduser" 	=> $rs->fields( "iduser" ),
			"photo" 	=> $photo
			
		);
	
	return $user;
	}
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'identifiant du client
	 * @return int
	 */
	public function getId(){ return $this->recordSet[ "idbuyer" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return ArrayList
	 */
	public function &getContacts(){ return $this->contacts; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @return CustomerContact
	 */
	public function &getContact(){ return $this->currentContact; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'adresse du client
	 * Note : non éditable ( @todo )
	 * @return CustomerAddress l'adresse du client
	 */
	public function getAddress(){ return $this->address; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @see objects/DBObject#save()
	 */
	public function save(){

		parent::save();

		$it = $this->contacts->iterator();
		
		while( $it->hasNext() )
			$it->next()->save();

		//@todo supprimer lorsque CustomerAddress implements EditableAddress ( sortir les données de la table buyer )
		$this->address = new CustomerAddress( $this->recordSet[ "idbuyer" ], $this->currentContact->getId() );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Créer un compte front office
	 * @param string $password
	 * @return bool
	 */
	public function createAccount( $password = false ){
		
		if( $password == false || !strlen( preg_replace( "/[^a-zA-Z0-9]+/", "", $password ) ) )
			$password = Util::getRandomPassword();

		$query = "
		REPLACE INTO user_account( idbuyer, idcontact, login, password )
		VALUES(
			'" . $this->recordSet[ "idbuyer" ] . "',
			'" . $this->currentContact->get( "idcontact" ) . "',
			'" . $this->currentContact->get( "mail" ) . "',
			" . DBUtil::quote( md5( $password ) ) . "
		)";
		
		return DBUtil::query( $query ) !== false;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * abonne le client à la newsletter du site
	 * @return bool true en cas de succès, sinon false
	 */
	public function subscribeNewsletter(){
		
		include_once( "Validate.php" );
		include_once( "Validate/FR.php" );
		
		if( !Validate::email( $this->currentContact->get( "mail" ) ) )
			return false;
			
		$query = "
		INSERT IGNORE INTO newsletter_subscriptions ( email, gender, lastname, firstname, company, address, address2, zipcode, city, phonenumber )
		VALUES(
			" . DBUtil::quote( $this->currentContact->get( "mail" ) ) . ",
			'" .  $this->currentContact->get( "title" )  . "',
			" . DBUtil::quote( $this->currentContact->get( "lastname" ) ) . ",
			" . DBUtil::quote( $this->currentContact->get( "firstname" ) ) . ",
			" . DBUtil::quote( $this->get( "company" ) ) . ",
			" . DBUtil::quote( $this->get( "adress" ) ) . ",
			" . DBUtil::quote( $this->get( "adress_2" ) ) . ",
			" . DBUtil::quote( $this->get( "zipcode" ) ) . ",
			" . DBUtil::quote( $this->get( "city" ) ) . ",
			" . DBUtil::quote( $this->currentContact->get( "mail" ) ) . "
		)";
		
		DBUtil::query( $query );
	
		return DBUtil::getAffectedRows() == 1;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	
	/**
	 * Indique si le client à été refusé au factor
	 * @static
	 * @param int $idbuyer l'id du client
	 * @return bool true si le client a déjà été réfusé, false sinon
	 */
	public static function isRefusedByFactor( $idbuyer ){
		
		$query = "
		SELECT COUNT(*) AS refusalCount
		FROM `billing_buyer`
		WHERE idbuyer = '$idbuyer'
		AND factor_refusal_date != '0000-00-00'";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de contrôler les refus du factor du client" );
		
		return $rs->fields( "refusalCount" ) > 0;
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * Retourne l'encours assurance en HT
	 * @return float
	 */
	public function getInsuranceOutstandingCreditsET(){

		/* assurance crédit utilisée */
		
		$idcredit_insurance = DBUtil::getParameterAdmin( "idcredit_insurance" );
		
		if( !$idcredit_insurance )
			return 0.0;

		$rs =& DBUtil::query( "SELECT * FROM credit_insurance WHERE idcredit_insurance = '$idcredit_insurance' LIMIT 1" );
		
		if( !$rs->RecordCount() )
			return 0.0;

		$insurance = ( object )$rs->fields;
		
		/* montant pour lequel est assuré le client */
		
		if( $this->get( "insurance_amount" ) == 0.0 || $this->get( "insurance_status" ) != "Accepted" )
			return 0.0;
				
		$outstandingCredits = 0.0;
		
		/* commande confirmées non facturées */
		
		include_once( dirname( __FILE__ ) . "/Order.php" );
		
		$query = "
		SELECT bl.idorder, SUM( bl.idbilling_buyer )
		FROM `order` o, bl_delivery bl
		WHERE bl.idorder = o.idorder
		AND o.`status` IN( '" . Order::$STATUS_ORDERED . "', '" . Order::$STATUS_TO_PAY . "' )
		AND o.cash_payment <> 1
		AND o.insurance = TRUE
		AND o.idbuyer = '" . $this->getId() . "'
		GROUP BY bl.idorder
		HAVING SUM( idbilling_buyer ) = 0";

		$rs =& DBUtil::query( $query );
		
		include_once( dirname( __FILE__ ) . "/Order.php" );
		
		while( !$rs->EOF() ){
			
			$order = new Order( $rs->fields( "idorder" ) );
		
			$netBillET 	= round( $order->getNetBill() / ( 1.0 + $order->getVATRate() / 100.0 ), 2 );
			
			if( $netBillET >= $insurance->minimum && $netBillET <= $insurance->maximum  )
				$outstandingCredits += $netBillET;
			
			$rs->MoveNext();
			
		}
			
		/* commande confirmées facturées et non payées */

		$query = "
		SELECT DISTINCT( idorder ) 
		FROM billing_buyer 
		WHERE insurance = TRUE
		AND idbuyer = '" . $this->getId() . "' 
		AND `status` LIKE 'Invoiced'";

		$rs =& DBUtil::query( $query );
		
		include_once( dirname( __FILE__ ) . "/Order.php" );
		include_once( dirname( __FILE__ ) . "/Invoice.php" );
		
		while( !$rs->EOF() ){
			
			$order = new Order( $rs->fields( "idorder" ) );

			$netBillET 	= round( $order->getNetBill() / ( 1.0 + $order->getVATRate() / 100.0 ), 2 );
			
			$outstandingCredits += $netBillET;
			
			$rs2 =& DBUtil::query( "SELECT idbilling_buyer FROM billing_buyer WHERE idorder = '" . $rs->fields( "idorder" ) . "'" );
			
			while( !$rs2->EOF() ){
				
				$invoice = new Invoice( $rs2->fields( "idbilling_buyer" ) );

				$netBillET 	= round( $invoice->getNetBill() / ( 1.0 + $order->getVATRate() / 100.0 ), 2 );
				
				if( $netBillET >= $insurance->minimum && $netBillET <= $insurance->maximum  )
					$outstandingCredits -= max( 0.0, $netBillET - $invoice->getBalanceOutstanding() / ( 1.0 + $invoice->getVATRate() / 100.0 ) );
				
				$rs2->MoveNext();
				
			}

			$rs->MoveNext();
			
		}

		$outstandingCredits = min( $this->get( "insurance_amount" ), 	$outstandingCredits );
		$outstandingCredits = min( $insurance->maximum, 				$outstandingCredits );
	
		return round( $outstandingCredits, 2 );
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */

}

?>