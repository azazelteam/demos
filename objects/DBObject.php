<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object abstraction pour les interactions avec la base de données
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );

class DBObject{
	
	//----------------------------------------------------------------------------
	
	protected $tableName;
	protected $secureDataMembers;
	protected $primaryKeys;
	protected $recordSet;
	//protected $metadata;
	
	//----------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param string $tablename Le nom de la table
	 * @param bool $secureDataMembers force l'écriture de getters et de setters dans les classes filles
	 */
	public function __construct( $tableName, $secureDataMembers = true ){
		
		$this->tableName 			= "";
		$this->secureDataMembers 	= $secureDataMembers == true;
		$this->recordSet 			= array();
		$this->primaryKeys 			= array();
		//$this->metadata 			= array();
		
		/* parse arguments */
		
		$argCount = func_num_args();
		
		if( !$argCount || $argCount < 4 || $argCount % 2 ){
		
			trigger_error( "Invalid usage of class DBObject", E_USER_ERROR );
			die();
				
		}
		
		/* table */
		
		$this->setTable( $tableName );

		/* keys */
		
		$i = 2;
		while( $i < $argCount ){

			$column = func_get_arg( $i );
			$value 	= func_get_arg( $i + 1 );
			$this->setPrimaryKey( $column, $value );
			
			$i += 2;
			
		}

		$this->loadRecordSet();

	}

	//----------------------------------------------------------------------------
	
	public final function get( $fieldName ){

		if( $this->secureDataMembers || !array_key_exists( $fieldName, $this->recordSet ) )
			return null;
			switch($fieldName){
				case 'description_1':
					$return = $this->recordSet[ $fieldName ];
				break;
				case 'description_plus_1':
					$return = $this->recordSet[ $fieldName ];
				break;
				case 'actuality_1':
					$return = $this->recordSet[ $fieldName ];
				break;
				default:
                    $return = $this->recordSet[ $fieldName ];
				break;
			}
		return $return;
			
	}
	
	//----------------------------------------------------------------------------
	
	public final function set( $fieldName, $value ){

		if( $this->secureDataMembers || !array_key_exists( $fieldName, $this->recordSet ) )
			return false;
			
		$this->recordSet[ $fieldName ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	private final function setTable( $tableName ){
		
		/*$rs =& DBUtil::query( "SHOW TABLES LIKE '" . Util::html_escape( $tableName ) . "'" );
		
		if( !$rs->RecordCount() ){
		
			trigger_error( "Table `$tableName` does not exist", E_USER_ERROR );
			die();
				
		}*/
		
		$this->tableName = $tableName;
		
		//$this->setMetaData();
		
	}
	
	//----------------------------------------------------------------------------
	
	private final function setPrimaryKey( $primaryKeyName, $primaryKeyValue ){
		
		/*$rs =& DBUtil::query( "SHOW COLUMNS FROM `" . Util::html_escape( $this->tableName ) . "` LIKE '" . Util::html_escape( $primaryKeyName ) . "'" );
		
		if( !$rs->RecordCount() ){
		
			trigger_error( "Column `$this->tableName`.``$tableName`` does not exist", E_USER_ERROR );
			die();
				
		}*/
		
		$this->primaryKeys[ $primaryKeyName ] = $primaryKeyValue;
		
	}
	
	//----------------------------------------------------------------------------
	
	/*private final function setMetaData(){
		
		$this->metadata[ "COLUMNS" ] 	= array();
		$this->metadata[ "KEYS" ] 		= array();
		
		/* columns */
		/*
		$rs =& DBUtil::query( "SHOW COLUMNS FROM `{$this->tableName}`" );

		while( !$rs->EOF() ){
			
			$this->metadata[ "COLUMNS" ][] = $rs->fields;
		
			$rs->MoveNext();
			
		}
		
		/* keys */
		/*
		$rs =& DBUtil::query( "SHOW KEYS FROM `{$this->tableName}" );
		
		while( !$rs->EOF() ){
			
			$this->metadata[ "KEYS" ][] = $rs->fields;
		
			$rs->MoveNext();
			
		}
		
	}*/
	
	//----------------------------------------------------------------------------
	
	//private final function showKeys(){ return $this->metaData[ "KEYS" ]; }
	
	//----------------------------------------------------------------------------
	
	//private final function showColumns(){ return $this->metaData[ "COLUMNS" ]; }
	
	//----------------------------------------------------------------------------
	
	private final function loadRecordSet(){
		
		$query = "
		SELECT *
		FROM `{$this->tableName}`
		WHERE ";
		
		$p = 0;
		foreach( $this->primaryKeys as $primakeyKeyName => $primaryKeyValue ){
			
			if( $p )
				$query .= " AND";
				
			$query .= " `$primakeyKeyName` = " . DBUtil::quote( $primaryKeyValue );
			$p++;
				
		}
		
		$query .= " LIMIT 1";
		
		$rs =& DBUtil::query( $query );

		if( $rs === false || !$rs->RecordCount() ){

			//trigger_error( "Loading recordset failed $query", E_USER_ERROR );
			//die();
			return ;
		}
		
		$this->recordSet = $rs->fields;
		
	}
	
	//----------------------------------------------------------------------------
	
	public function save(){
		
		$query = "SELECT 1 FROM `{$this->tableName}` WHERE ";
		
		/*keys*/
		
		$p = 0;
		foreach( $this->primaryKeys as $primakeyKeyName => $primaryKeyValue ){
			
			if( $p )
				$query .= " AND";
				
			$query .= " `$primakeyKeyName` = " . DBUtil::quote( $primaryKeyValue );
			$p++;
				
		}

		$query .= " LIMIT 1";

		if( DBUtil::query( $query )->RecordCount() )
				$this->update();
		else 	$this->insert();
		
	}
	
	//----------------------------------------------------------------------------
	
	protected final function insert(){
		
		$query = "
		INSERT INTO `{$this->tableName}` (";
		
		/*columns*/
		
		$p = 0;
		foreach( $this->recordSet as $fieldName => $value ){
				
			if( $p )
				$query .= ",";
			
			$query .= "`$fieldName`";
		
			$p++;
						
		}
		
		/*keys*/
		
		$query .= " ) VALUES ( ";
		
		$p = 0;
		foreach( $this->recordSet as $fieldName => $value ){
				
			if( $p )
				$query .= ",";
			
			$query .= DBUtil::quote( $value );
		
			$p++;
						
		}
		
		$query .= " )";
		
		$rs =& DBUtil::query( $query );

		if( $rs === false ){
			
			trigger_error( "Insert into table failed", E_USER_ERROR );
			die();
			
		}

	}
	
	//----------------------------------------------------------------------------
	
	protected final function update(){
		
		$query = "
		UPDATE `{$this->tableName}`
		SET ";
		
		/*columns*/
		
		$p = 0;
		foreach( $this->recordSet as $fieldName => $value ){
				
			if( !isset( $this->primaryKeys[ $fieldName ] ) ){
				
				if( $p )
					$query .= ",";
				
				$query .= "`$fieldName` = " . DBUtil::quote( $value );
			
				$p++;
				
			}
						
		}
		
		/*keys*/
		
		$query .= " WHERE ";
		
		$p = 0;
		foreach( $this->primaryKeys as $primakeyKeyName => $primaryKeyValue ){
			
			if( $p )
				$query .= " AND";
				
			$query .= " `$primakeyKeyName` = " . DBUtil::quote( $primaryKeyValue );
			$p++;
				
		}
		
		$query .= " LIMIT 1";
		
		$rs =& DBUtil::query( $query );

		if( $rs === false ){
			
			trigger_error( "Updating table failed", E_USER_ERROR );
			die();
			
		}
		
	}
	
	//----------------------------------------------------------------------------
	
	public function saveXML(){
		
		$document = new DomDocument( "1.0", "utf-8" );
		
		$root = $document->createElement( Util::doNothing( "object" ) );
		
		foreach( $this->recordSet as $fieldname => $value ){
			
			$element 	= $document->createElement( Util::doNothing( $fieldname ) );
			$attribute = $document->createAttribute( Util::doNothing( "value" ) ); 
			$attribute->appendChild( $document->createTextNode( Util::doNothing( htmlentities( $value ) ) ) );
			$element->appendChild( $attribute );
			
			$root->appendChild( $element );
			
		}

		$document->appendChild( $root );

		return $document->saveXML();

	}
	
	//----------------------------------------------------------------------------
	
}

?>