<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object noeuds ....?
 */

class Node {
    
    private $html;
    
    private $root;
    
/*
 * Constructeur
 */
	public function __construct( $root, $node_name ){
  	 	global $GLOBAL_START_URL;
  	 	
        $img_url = $GLOBAL_START_URL ."/images/back_office/layout/icons-menu-star.png"; //$root[ "name" ]
        $this->html = "<ul id=\"" . $node_name . "Menu\">\n<li><a href=\"#\" id=\"" . $node_name . "Root\" class=\"bookmarkMenuRoot\" style=\"margin-top: 1px; padding: 0px;\"><img src='".$img_url."' alt='".$root[ "name" ]."' /></a>";
        $this->createNode( $root );
        $this->html .= "</li></ul>\n"; 

    }

/*
 * Génération de l'arborescence en HTML
 */
    public function createNode( &$node ){
		
		global $GLOBAL_START_URL;
		
		// ouverture d'un sous-menu
        $this->html .= "<ul class=\"bookmarkMenu\">\n";

		// initialisation du compteur
        $i = 0;
        
        // tant que compteur inférieur au nb d'enfants du noeud                                                                 
        while( $i < count( $node[ "children" ] ) ){                             
			
            // on crée un noeud avec chaque enfant et l'id compteur
            $child = &$node[ "children" ][ $i ];                                
			
			if( $child[ "type" ] == "external" )
				$url = $child[ "url" ];
			elseif( $child[ "type" ] == "local" )
				$url = "$GLOBAL_START_URL/www" . $child[ "url" ];
			
            // écriture du nom du noeud sur lequel on se trouve
            $this->html .= "<li>";
            if( !empty( $url ) ){                                     
            	$this->html .= "<a href=\"$url\" class=\"bookmarkMenu\" style=\"color: #FFFFFF;\" onclick=\"window.open(this.href); return false;\">" . $child[ "name" ] . "</a>";          		
            }else{
				$this->html .= "<a href=\"#\" class=\"bookmarkMenu\" style=\"color: #FFFFFF;\">".$child[ "name" ]."</a>";
            }
            // si ce noeud a des enfants, on rappele la fonction
            if( count( $child[ "children" ] ) ) {
                $this->createNode( $child ); 
            }
			
			// fermeture du noeud sur lequel on se trouve
            $this->html .= "</li>\n";

            $i++;

        }
		// fermeture d'un sous-menu
        $this->html .= "</ul>";

    }

/*
 * Accesseur : retourne le html généré
 */
  	public function getHtml(){

        return $this->html;
    }
}

?>
