<?php
/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object menu de navigation CSS
 */

define ( "DEBUG_CSSMENU", 0 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/Session.php" );

class CSSMenu{
	
	//-------------------------------------------------------------------------------------------
	
	//static $UNORDERED_LIST = 0;
	//static $ORDERED_LIST = 1;

	//-------------------------------------------------------------------------------------------

	private $root;
	private $depth;
	private $html;
	private $staticItems;
	private $catalog_right;
	private $categoryCount;
	private $showHomeItem;
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Constructeur
	 * @param int depth facultatif, permet d'afficher les sous-catégories sur 'depth' niveaux
	 * @param int idCategory facultatif, permet de construire le menu à partir de la catégorie spécifiée
	 */
	function __construct( $depth = -1, $idCategory = 0 ){
	
		$this->depth = $depth;
		$this->html ="";
		$this->catalog_right = 0;
		$this->showHomeItem = false;
		
		$this->root = array( 
			"id" 			=> $idCategory, 
			"name" 			=> "ROOT",
			"children" 		=> array()
		);
	
		$this->categoryCount = $idCategory ? 1 : 0;
		$this->staticItems = array( "labels" => array(), "hrefs" => array(), "styles" => array() , "ids" => array());
		
		$this->setCatalogRight();				
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Afficher l'élément 'Accueil' en premier ou non
	 * @param bool $showHomeItem
	 * @return void
	 */
	public function showHomeItem( $showHomeItem = true ){
	
		$this->showHomeItem = $showHomeItem;
			
	}
	
	//-------------------------------------------------------------------------------------------
	/**
	 * Construit le menu
	 * @return void
	 */
	public function buildMenu(){
		
		
			
		$start_time = microtime();
		
		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );
		
		$elapsed_time = ceil( ( microtime() - $start_time ) / 1000 );
		
		$this->createNode( $this->root );	
	
		if( DEBUG_CSSMENU ){
			
			$query = "SELECT COUNT(*) AS total FROM product WHERE  available = 1 AND catalog_right <= " . $this->catalog_right;
			$db =& DBUtil::getConnection();
			$rs = $db->Execute( $query );
			$totalProducts = $rs->fields( "total" );

			$query = "SELECT COUNT(*) AS total FROM category WHERE available = 1 AND catalog_right <= " . $this->catalog_right;
			$rs = $db->Execute( $query );
			$totalCategories = $rs->fields( "total" );
		
			echo "<p><u>buildMap()</u></p>";
			echo "<p>elapsed time : " . $elapsed_time . " ms</p>";
			echo "<p>mapped categories : " . $this->categoryCount . "</p>";
			
		}
		
	}
	
	public function buildImageMenu(){
		
		
			
		$start_time = microtime();
		
		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );
		
		$elapsed_time = ceil( ( microtime() - $start_time ) / 1000 );
		
		$this->createImageNode( $this->root );	
	
		if( DEBUG_CSSMENU ){
			
			$query = "SELECT COUNT(*) AS total FROM product WHERE  available = 1 AND catalog_right <= " . $this->catalog_right;
			$db =& DBUtil::getConnection();
			$rs = $db->Execute( $query );
			$totalProducts = $rs->fields( "total" );

			$query = "SELECT COUNT(*) AS total FROM category WHERE available = 1 AND catalog_right <= " . $this->catalog_right;
			$rs = $db->Execute( $query );
			$totalCategories = $rs->fields( "total" );
		
			echo "<p><u>buildMap()</u></p>";
			echo "<p>elapsed time : " . $elapsed_time . " ms</p>";
			echo "<p>mapped categories : " . $this->categoryCount . "</p>";
			
		}
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Récupère les catégories filles
	 * @param array $parent les catégories mères
	 * @param int $depth le niveau
	 * @return void
	 */
	private function setCategoryChildren( &$parent, $depth ){
			
		
		
		$lang = Session::getInstance()->getLang();
		
		$query = "
		SELECT cat.idcategory AS id, cat.name$lang AS name
		FROM category cat, category_link catlink
		WHERE catlink.idcategorychild = cat.idcategory
		AND cat.available = 1
		AND catlink.idcategoryparent = " . $parent[ "id" ] . "
		AND cat.catalog_right <= " . $this->catalog_right . "
		ORDER BY catlink.displayorder";
		
		$db =& DBUtil::getConnection();
		$rs = $db->Execute( $query );
		$this->categoryCount += $rs->RecordCount();
		
		if( DEBUG_CSSMENU ){
			
			echo "<p><u>setCategoryChildren()</u></p>";
			echo "<p>@param parent: [" . $parent[ "id" ] . "]" . $parent[ "name" ] . "</p>";
			echo "<p>@param depth: " . $depth . "</p>";
			echo "<p>query : " . $query . "</p>";
			echo "<p>results : " . $rs->RecordCount() . "</p>";
			
			
		}
		
		while( !$rs->EOF() ){
			
			$category  = array( 
				"id" 			=> $rs->fields( "id" ), 
				"name" 			=> $rs->fields( "name" ),
				"children" 		=> array()
			);
			
			$pos = array_push( $parent[ "children" ], $category );
			$child = &$parent[ "children" ][ $pos - 1 ];
			
			if( $this->depth == -1 )
				$this->setCategoryChildren( $child, $depth );
			else if( $depth > 1 )
				$this->setCategoryChildren( $child, $depth - 1 );
			
			$rs->MoveNext();
		
		}
			
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Génère les noeux de l'arbre pour chaque catégorie
	 * @param array $parent les catégories mère
	 * @return void
	 */
	private function createNode( &$parent ){
		
		global $GLOBAL_START_URL;
		
			
		$spanstart = "";
		$spanend = "";
		
		if( $parent[ "name" ] == "ROOT" ){

			$spanstart = "<span>";
			$spanend = "</span>";
			$imgHouse = "Accueil";			
			$this->html .= "<ul id=\"mainFrontMenu\">";

			if( $this->showHomeItem)
				$this->html .= "<li id='accueil'><a href=\"$GLOBAL_START_URL\">".$spanstart.$imgHouse.$spanend."</a></li>";

		}else{
			
			 	$this->html .= "<ul>";
			 	
		}

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){
			
			$child = &$parent[ "children" ][ $i ];
			
			/*if( $parent[ "name" ] == "ROOT" ){
				
				$index = $this->getFirstChildIndex( $child );
				
				if( $index > ceil( ( $this->getFirstChildCount() + count( $this->staticItems ) ) / 2 ) )
					$cssClass = "FirstChildRight";
				else $cssClass = "FirstChildLeft";	
				
				if( $index > 0 || $this->showHomeItem )
					$cssClass .= " hasDelimiter";		
				
			}
			else{
				*/
				if( count( $child[ "children" ] ) && $parent[ "name" ] != "ROOT")
					$cssClass = "ParentNode";
				else $cssClass = "";
			
			/*}*/
			
			$this->html .= "<li id='ID".$child[ "id" ]."' class='$cssClass'>";
	
			$href = URLFactory::getCategoryURL( $child[ "id" ] );
			
			/*if( $parent[ "name" ] == "ROOT" && $this->getFirstChildIndex( $child ) > 0 )
				$this->html .= "<div class=\"CSSMenuItemDelimiter\"></div>";*/
				
			$this->html .= "<a href=\"$href\">$spanstart";
			
			$menu_images=$this->getImageMenu($child[ "id" ]);
			
			if ($menu_images->fields('image_menu')!=""){
				$image1=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image1.=$menu_images->fields("image_menu");
				
				$image2=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image2.=$menu_images->fields("image_menu_roll");
				
				$enfant=$child[ 'name' ];
				//$this->html .= "<img style='border:none;' src='$image1' alt='$enfant' onMouseOver=\"this.src='$image2'\" onMouseOut=\"this.src='$image1'\">";
				$this->html .= "<br>".$child[ "name" ];
			}else{
				$this->html .= $child[ "name" ];
			}
			
			$this->html .= "$spanend</a>";
				
			if( count( $child[ "children" ] ) )
				$this->createNode( $child );
						
			$this->html .= "</li>";
			
			
			
			$i++;
			
		}
		
		//add static items
		
		if( $parent[ "name" ] == "ROOT" ){
			
			$k = 0;
			while( $k < count( $this->staticItems[ "labels" ] ) ){
				
				$index = $this->getFirstChildCount();
				
				/*if( $index > ceil( ( $this->getFirstChildCount() + count( $this->staticItems ) ) / 2 ) )
					$cssClass = "FirstChildRight";
				else $cssClass = "FirstChildLeft";
				
				if( $index > 0 )
					$cssClass .= " hasDelimiter";*/
					
				$label = $this->staticItems[ "labels" ][ $k ];
				$href = $this->staticItems[ "hrefs" ][ $k ];
				$style = $this->staticItems[ "styles" ][ $k ];
				$id = $this->staticItems[ "ids" ][ $k ];
				
				$this->html .= "<li id=\"$id\"><a href=\"$href\"";
				
				if( !empty( $style ) )
					$this->html .= " style=\"$style\"";
					
				$this->html .= ">".$spanstart.$label.$spanend."</a></li>";
				
				$k++;
				
			}
			
		}
		
		$this->html .= "</ul>";
		
	}


	private function createImageNode( &$parent ){
		
		global $GLOBAL_START_URL;

			

		if( $parent[ "name" ] == "ROOT" ){

			$this->html .= "<ul id=\"mainFrontMenu\">";

			if( $this->showHomeItem )
				$this->html .= "<li><a href=\"$GLOBAL_START_URL\">Accueil</a></li>";

		}else{
			
			 	$this->html .= "<ul>";
			 	
		}

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){
			
			$child = &$parent[ "children" ][ $i ];
			
			/*if( $parent[ "name" ] == "ROOT" ){
				
				$index = $this->getFirstChildIndex( $child );
				
				if( $index > ceil( ( $this->getFirstChildCount() + count( $this->staticItems ) ) / 2 ) )
					$cssClass = "FirstChildRight";
				else $cssClass = "FirstChildLeft";	
				
				if( $index > 0 || $this->showHomeItem )
					$cssClass .= " hasDelimiter";		
				
			}
			else{
				*/
				if( count( $child[ "children" ] ) && $parent[ "name" ] != "ROOT")
					$cssClass = "ParentNode";
				else $cssClass = "";
			
			/*}*/
			
			$this->html .= "<li id='ID".$child[ "id" ]."' class='$cssClass'>";

			$href = URLFactory::getCategoryURL( $child[ "id" ] );
			
			/*if( $parent[ "name" ] == "ROOT" && $this->getFirstChildIndex( $child ) > 0 )
				$this->html .= "<div class=\"CSSMenuItemDelimiter\"></div>";*/
				
			$this->html .= "<a href=\"$href\">";
			
			$menu_images=$this->getImageMenu($child[ "id" ]);
			
			if ($menu_images->fields('image_menu')!=""){
				$image1=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image1.=$menu_images->fields("image_menu");
				
				$image2=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image2.=$menu_images->fields("image_menu_roll");
				
				$enfant=$child[ 'name' ];
				$this->html .= "<img style='border:none;' src='$image1' alt='$enfant' />";
				//$this->html .= "<br>".$child[ "name" ];
			}else{
				$this->html .= $child[ "name" ];
			}
			
			$this->html .= "</a>";
				
			if( count( $child[ "children" ] ) )
				$this->createImageNode( $child );
						
			$this->html .= "</li>";
			
			
			
			$i++;
			
		}
		
		//add static items
		
		if( $parent[ "name" ] == "ROOT" ){
			
			$k = 0;
			while( $k < count( $this->staticItems[ "labels" ] ) ){
				
				$index = $this->getFirstChildCount();
				
				/*if( $index > ceil( ( $this->getFirstChildCount() + count( $this->staticItems ) ) / 2 ) )
					$cssClass = "FirstChildRight";
				else $cssClass = "FirstChildLeft";
				
				if( $index > 0 )
					$cssClass .= " hasDelimiter";*/
					
				$label = $this->staticItems[ "labels" ][ $k ];
				$href = $this->staticItems[ "hrefs" ][ $k ];
				$style = $this->staticItems[ "styles" ][ $k ];
				$id = $this->staticItems[ "ids" ][ $k ];
				
				$this->html .= "<li id=\"$id\"><a href=\"$href\"";
				
				if( !empty( $style ) )
					$this->html .= " style=\"$style\"";
					
				$this->html .= ">$label</a></li>";
				
				$k++;
				
			}
			
		}
		
		$this->html .= "</ul>";
		
	}
	//-------------------------------------------------------------------------------------------
	/**
	 * Retourne le chemin des images pour le menu
	 * si le menu est désiré en vignettes
	 * @return array(image_menu, image_menu_roll)
	 */
	public function getImageMenu($idCategory){
		
		
		
		$query = "SELECT image_menu, image_menu_roll, imagedirectory";
		$query .= " FROM category";
		$query .= " WHERE idcategory = " . $idCategory;

		$db =& DBUtil::getConnection();
		$rs = $db->Execute( $query );
		
		$imagesPath=$rs;
				
		return $imagesPath;
	}

	/**
	 * Retourne le code HTML du menu
	 * @return string le code html du menu
	 */ 
	public function getHTML(){
			
		return str_replace("&","&amp;",$this->html);
		
	}
	
	//-------------------------------------------------------------------------------------------

	/**
	 * Retourne le nom du menu
	 * @return string le nom du menu
	 */
	public function getName(){
		
		return $this->root[ "name" ];
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Récupère le nom de la catégorie
	 * @param int $idCategory l'id de la catégorie
	 * @return void
	 */
	private function setName( $idCategory ){
		
		
		
		if( !$idCategory )
			$this->root[ "name" ] = "";
		else{
			
			$lang = Session::getInstance()->getLang();
			
			$query = "SELECT name$lang AS name";
			$query .= " FROM category";
			$query .= " WHERE idcategory = " . $idCategory;
			$query .= " AND catalog_right <= " . $this->catalog_right;
	
			$db =& DBUtil::getConnection();
			$rs = $db->Execute( $query );
			
			$this->root[ "name" ] = $rs->RecordCount() ? $rs->fields( "name" ) : "";
			
		}
		
		if( DEBUG_CSSMENU ){
			
				echo "<p><u>setName()</u></p>";
				echo "<p>@param : $idCategory</p>";
				echo "<p>query : " . $query . "</p>";
				echo "<p>result : " . $this->root["name"] . "</p>";
				
		}
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Affecte les droits des utilisateurs
	 * @return void
	 */
	public function setCatalogRight(){
		
		
		
		$buyer = new BUYER();
		
		$buyer->GetBuyerInfo();
		$this->catalog_right = $buyer->getValue( "catalog_right" );
		
		if( DEBUG_CSSMENU ){
			
			echo "<p><u>setCatalogRight()</u></p>";
			echo "<p>result : " . $this->catalog_right . "</p>";
				
		}
			
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne l'index des premiers fils
	 * @return int $child le fils
	 * @return int l'index
	 */
	private function getFirstChildIndex( $child ){
	
		$i = 0;
		while( $i < count( $this->root[ "children" ] ) ){
			
			if( $this->root[ "children" ][ $i ][ "name" ] === $child[ "name" ] )
				return $i;
				
			$i++;
			
		}
		
		return false;	
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Retourne le nombre de fils du premier niveau
	 * @return int le nombre de fils du premier niveau
	 */
	private function getFirstChildCount(){
	
		return count( $this->root[ "children" ] );
		
	}
	
	//-------------------------------------------------------------------------------------------
	
	/**
	 * Permet d'ajouter un menu static
	 * @param string $label le nom du menu
	 * @param string $href le lien
	 * @param string $style le css
	 * @return void
	 */
	public function addStaticItem( $label, $href, $style = "" , $id = ""){
		
		$this->staticItems[ "labels" ][ count( $this->staticItems[ "labels" ] )] = $label;
		$this->staticItems[ "hrefs" ][ count( $this->staticItems[ "hrefs" ] ) ] = $href;
		$this->staticItems[ "styles" ][ count( $this->staticItems[ "styles" ] ) ] = $style;
		$this->staticItems[ "ids" ][ count( $this->staticItems[ "ids" ] ) ] = $id;

	}
	
	//-------------------------------------------------------------------------------------------
	
	public function buildMenuCP(){
		
		
			
		$start_time = microtime();
		
		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );
		
		$elapsed_time = ceil( ( microtime() - $start_time ) / 1000 );
		
		$this->createNodeCP( $this->root );	
	
		if( DEBUG_CSSMENU ){
			
			$query = "SELECT COUNT(*) AS total FROM product WHERE  available = 1 AND catalog_right <= " . $this->catalog_right;
			$db =& DBUtil::getConnection();
			$rs = $db->Execute( $query );
			$totalProducts = $rs->fields( "total" );

			$query = "SELECT COUNT(*) AS total FROM category WHERE available = 1 AND catalog_right <= " . $this->catalog_right;
			$rs = $db->Execute( $query );
			$totalCategories = $rs->fields( "total" );
		
			echo "<p><u>buildMap()</u></p>";
			echo "<p>elapsed time : " . $elapsed_time . " ms</p>";
			echo "<p>mapped categories : " . $this->categoryCount . "</p>";
			
		}
		
	}


	private function createNodeCP( &$parent ){
		
		global $GLOBAL_START_URL;
			
			

		if( $parent[ "name" ] == "ROOT" ){

			$this->html .= "<ul id=\"mainFrontMenu\">";

			if( $this->showHomeItem )
				$this->html .= "<li><a href=\"$GLOBAL_START_URL\">Accueil</a></li>";

		}else $this->html .= "<ul>";

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){
			
			$child = &$parent[ "children" ][ $i ];
			
			if( count( $child[ "children" ] ) && $parent[ "name" ] != "ROOT")
				$cssClass = "ParentNode";
			else $cssClass = "";
			
			
			$this->html .= "<li id=\'ID".$child[ "id" ]."\' class=\'$cssClass\'>";
			
			$href = URLFactory::getCategoryURL( $child[ "id" ] );
				
			$this->html .= "<a href=\"#\" onClick=\"document.formcats.IdCateg.value=".$child[ "id" ].";document.formcats.submit();\">";
			
			$menu_images=$this->getImageMenu($child[ "id" ]);
			
			if ($menu_images->fields("image_menu")!=""){
				$image1=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image1.=$menu_images->fields("image_menu");
				
				$image2=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image2.=$menu_images->fields("image_menu_roll");
				
				$enfant=$child[ "name" ];
				$this->html .= "<img style=\'border:none;\' src=\'$image1\' alt=\'$enfant\' onMouseOver=\"this.src=\'$image2\'\" onMouseOut=\"this.src=\'$image1\'\">";
				$this->html .= "<br>".$child[ "name" ];
			}else{
				$this->html .= $child[ "name" ];
			}
			
			$this->html .= "</a>";
				
			if( count( $child[ "children" ] ) )
				$this->createNodeCP( $child );
						
			$this->html .= "</li>";
			
			
			
			$i++;
			
		}
		
		//add static items
		
		if( $parent[ "name" ] == "ROOT" ){
			
			$k = 0;
			while( $k < count( $this->staticItems[ "labels" ] ) ){
				
				$index = $this->getFirstChildCount();
				
				/*if( $index > ceil( ( $this->getFirstChildCount() + count( $this->staticItems ) ) / 2 ) )
					$cssClass = "FirstChildRight";
				else $cssClass = "FirstChildLeft";
				
				if( $index > 0 )
					$cssClass .= " hasDelimiter";*/
					
				$label = $this->staticItems[ "labels" ][ $k ];
				$href = $this->staticItems[ "hrefs" ][ $k ];
				$style = $this->staticItems[ "styles" ][ $k ];
				$id = $this->staticItems[ "ids" ][ $k ];
				
				$this->html .= "<li id=\"$id\"><a href=\"$href\"";
				
				if( !empty( $style ) )
					$this->html .= " style=\"$style\"";
					
				$this->html .= ">$label</a></li>";
				
				$k++;
				
			}
			
		}
		
		$this->html .= "</ul>";
		
	}


	public function buildImageMenuCP(){
		
		
			
		$start_time = microtime();
		
		if( $this->depth || $this->depth == -1 )
			$this->setCategoryChildren( $this->root, $this->depth );
		
		$elapsed_time = ceil( ( microtime() - $start_time ) / 1000 );
		
		$this->createImageNodeCP( $this->root );	
	
		if( DEBUG_CSSMENU ){
			
			$query = "SELECT COUNT(*) AS total FROM product WHERE  available = 1 AND catalog_right <= " . $this->catalog_right;
			$db =& DBUtil::getConnection();
			$rs = $db->Execute( $query );
			$totalProducts = $rs->fields( "total" );

			$query = "SELECT COUNT(*) AS total FROM category WHERE available = 1 AND catalog_right <= " . $this->catalog_right;
			$rs = $db->Execute( $query );
			$totalCategories = $rs->fields( "total" );
		
			echo "<p><u>buildMap()</u></p>";
			echo "<p>elapsed time : " . $elapsed_time . " ms</p>";
			echo "<p>mapped categories : " . $this->categoryCount . "</p>";
			
		}
		
	}
		
	private function createImageNodeCP( &$parent ){
		
		global $GLOBAL_START_URL;

			

		if( $parent[ "name" ] == "ROOT" ){

			$this->html .= "<ul id=\"mainFrontMenu\">";

			if( $this->showHomeItem )
				$this->html .= "<li><a href=\"$GLOBAL_START_URL\">Accueil</a></li>";

		}else{
			
			 	$this->html .= "<ul>";
			 	
		}

		$i = 0;
		while( $i < count( $parent[ "children" ] ) ){
			
			$child = &$parent[ "children" ][ $i ];
			
			if( count( $child[ "children" ] ) && $parent[ "name" ] != "ROOT")
				$cssClass = "ParentNode";
			else $cssClass = "";
			
			$this->html .= "<li id='ID".$child[ "id" ]."' class='$cssClass'>";
			
			$href = URLFactory::getCategoryURL( $child[ "id" ] );
			
			$this->html .= "<a href=\"#\" onClick=\"document.formcats.IdCateg.value=".$child[ "id" ].";document.formcats.submit();\">";
			
			$menu_images=$this->getImageMenu($child[ "id" ]);
			
			if ($menu_images->fields('image_menu')!=""){
				$image1=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image1.=$menu_images->fields("image_menu");
				
				$image2=$GLOBAL_START_URL."/www".$menu_images->fields("imagedirectory")."/";
				$image2.=$menu_images->fields("image_menu_roll");
				
				$enfant=$child[ 'name' ];
				$this->html .= "<img style='border:none;' src='$image1' alt='$enfant' />";

			}else{
				$this->html .= $child[ "name" ];
			}
			
			$this->html .= "</a>";
				
			if( count( $child[ "children" ] ) )
				$this->createImageNodeCP( $child );
						
			$this->html .= "</li>";
			
			
			
			$i++;
			
		}
		
		//add static items
		
		if( $parent[ "name" ] == "ROOT" ){
			
			$k = 0;
			while( $k < count( $this->staticItems[ "labels" ] ) ){
				
				$index = $this->getFirstChildCount();
									
				$label = $this->staticItems[ "labels" ][ $k ];
				$href = $this->staticItems[ "hrefs" ][ $k ];
				$style = $this->staticItems[ "styles" ][ $k ];
				$id = $this->staticItems[ "ids" ][ $k ];
				
				$this->html .= "<li id=\"$id\"><a href=\"$href\"";
				
				if( !empty( $style ) )
					$this->html .= " style=\"$style\"";
					
				$this->html .= ">$label</a></li>";
				
				$k++;
				
			}
			
		}
		
		$this->html .= "</ul>";
		
	}
	//-------------------------------------------------------------------------------------------	
}

?>