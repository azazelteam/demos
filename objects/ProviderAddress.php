<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object adresses prestataires
 */

include_once( dirname( __FILE__ ) . "/Address.php" );


final class ProviderAddress implements Address{

	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @var int $idprovider l'identifiant du prestataire
	 * @access private
	 */
	private $idprovider;
	/**
	 * @var array $recordSet
	 * @access private
	 */
	private $recordSet;
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @param int $idprovider
	 */
	function __construct( $idprovider ){
		
		$this->idprovider 	= intval( $idprovider );
		$this->recordSet 	= array();
		
		$this->load();
		
	}
	
	/* ---------------------------------------------------------------------------------------------------- */
	/* Address interface */
	
	public function getCompany(){ 		return $this->recordSet[ "company" ]; }
	public function getFirstName(){ 	return $this->recordSet[ "firstname" ]; }
	public function getLastName(){ 		return $this->recordSet[ "lastname" ]; }
	public function getGender(){ 		return $this->recordSet[ "idtitle" ]; }
	public function getPhonenumber(){ 	return $this->recordSet[ "phonenumber" ]; }
	public function getFaxnumber(){ 	return $this->recordSet[ "faxnumber" ]; }
	public function getGSM(){ 			return $this->recordSet[ "gsm" ]; }
	public function getEmail(){ 		return $this->recordSet[ "email" ]; }
	public function getAddress(){ 		return $this->recordSet[ "address" ]; }
	public function getAddress2(){ 		return $this->recordSet[ "address2" ]; }
	public function getZipcode(){ 		return $this->recordSet[ "zipcode" ]; }
	public function getCity(){ 			return $this->recordSet[ "city" ]; }
	public function getIdState(){ 		return $this->recordSet[ "idstate" ]; }
	
	/* ---------------------------------------------------------------------------------------------------- */
	/**
	 * @access private
	 * @return void
	 */
	protected function load(){
		
		$query = "
		SELECT p.name AS company,
			p.address,
			p.address2 ,
			p.city,
			p.zipcode,
			p.idstate,
			pc.phonenumber,
			pc.faxnumber,
			pc.gsm,
			pc.email,
			pc.firstname,
			pc.lastname,
			pc.idtitle
		FROM provider p, 
		provider_contact pc
		WHERE p.idprovider = '{$this->idprovider}'
		AND pc.idprovider = p.idprovider
		AND pc.idcontact = p.main_contact
		LIMIT 1";
		
		$this->recordSet = DBUtil::query( $query )->fields;
		
	}

	/* ---------------------------------------------------------------------------------------------------- */
	
}

?>