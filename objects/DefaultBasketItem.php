<?php

/*
 * Package objects
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Object modèle de ligne article pour le panier
*/
 
include_once( dirname( __FILE__ ) . "/Color.php" );
include_once( dirname( __FILE__ ) . "/Cloth.php" );


abstract class DefaultBasketItem{
 	
	//----------------------------------------------------------------------------
	//data members
	
	/**
	 * @var Basket $owner le panier propriétaire de la ligne article
	 * @access protected
	 */
    protected $owner;
    /**
     * @var array $fields tableau associatif regroupant les propriétés de l'article array( $key => $value )
     * @access protected
     */
	protected $fields;
	/**
	 * @var int $idarticle l'identifiant de l'article
	 * @access protected
	 */
	protected $idarticle;
	/**
	 * @var Color $color la couleur appliquée à l'article
	 * @access private
	 */
	private $color;
	/**
	 * @var Cloth $cloth le tissu appliqué à l'article
	 * @access private
	 */
	private $cloth;
	
	//----------------------------------------------------------------------------
	//abstract members
	
	/**
	 * Définit le numéro de l'article et ses propriétés
	 * @abstract
	 * @param int $idarticle le numéro de l'article
	 * @return void
	 */
	public abstract function setArticle( $idarticle );
	/**
	 * Recalcule les prix de l'article
	 * @access protected
	 * @abstract
	 * @return void
	 */
	protected abstract function recalculateAmounts();
	/**
	 * Modifie la quantité de l'article
	 * @abstract
	 * @access public
	 * @param int $quantity la quantité souhaitée
	 * @return void
	 */
	public abstract function setQuantity( $quantity );
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne la valeur d'une propriété donnée de l'article
	 * @param string $key la propriété souhaitée
	 * @return mixed la valeur de la propriété $key
	 */
	public function get( $key ){
	
		return $this->fields[ $key ];
			
	}

	//----------------------------------------------------------------------------
	
	/**
	 * Définit la valeur d'une propriété donnée de l'article
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à donner à la propriété $key
	 * @return void
	 */
	public function set( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * @return Basket
	 */
	public function &getOwner(){ return $this->owner; }

	//----------------------------------------------------------------------------
	
	/**
	 * Retourne une référence vers la couleur utilisée
	 * @return Color la couleur utilisée si elle existe, sinon NULL
	 */
	public function &getColor() {
		
		return $this->color;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Définit une couleur à utiliser
	 * @param Color $color la couleur à utiliser
	 * @return void
	 */
	public function setColor( Color &$color ) {
	
		$this->color =& $color;
		
		$reference_base = DBUtil::getDBValue( "reference", "detail", "idarticle", $this->get( "idarticle" ) );
		$summary 		= DBUtil::getDBValue( "summary_1", "detail", "idarticle", $this->get( "idarticle" ) );
		$designation 	= DBUtil::getDBValue( "designation_1", "detail", "idarticle", $this->get( "idarticle" ) );
	
		$reference 		= $reference_base . "_" . $color->getCode();
		$colorName 		= "Coloris: " . $color->getName();
		
		$this->setValue( "reference", $reference );
		$this->setValue( "reference_base", $reference_base );
		$this->setValue( "idcolor", $color->getId() );
		$this->setValue( "designation", "<b>$summary</b><br />$colorName<br />$designation" );

		$this->recalculateAmounts();
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne une référence vers le tissu utilisé
	 * @return Cloth le tissu utilisé si il existe, sinon NULL
	 */
	public function &getCloth() {
		
		return $this->cloth;
		
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Définit un tissu à utiliser
	 * @param Cloth $cloth le tissu à utiliser
	 * @return void
	 */
	public function setCloth( Cloth &$cloth ){
		
		$this->cloth =& $cloth;
		
		$summary 		= DBUtil::getDBValue( "summary_1", "detail", "idarticle", $this->get( "idarticle" ) );
		$designation 	= DBUtil::getDBValue( "designation_1", "detail", "idarticle", $this->get( "idarticle" ) );
		$clothName 		= "Tissu: " . $cloth->getName();

		$this->setValue( "idcloth", $cloth->getId() );
		$this->setValue( "designation", "<b>$summary</b><br />$clothName<br />$designation" );

		$this->recalculateAmounts();

	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le total HT
	 */
	public function getTotalET(){
		
		return round( $this->fields[ "discount_price" ] * $this->fields[ "quantity" ], 2 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le total TTC
	 */
	public function getTotalATI(){
		
		return $this->fields[ "discount_price" ] * $this->fields[ "quantity" ] * ( 1.0 + $this->getVATRate() / 100.0 );
		
	}
	
	//----------------------------------------------------------------------------
	/**
	 * @return float le taux de TVA ( ex : 19.6 )
	 */
	public function getVATRate(){ 
		
		if( $this->owner->getVATRate() == 0.0 )
			return 0.0;
			
		return $this->fields[ "vat_rate" ]; 
	
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * Retourne true si le franco est autorisé pour cet article
	 * @return bool
	 */
	public function hasFrancoAvailable(){
		
		return DBUtil::getDBValue( "franco", "detail", "idarticle", $this->idarticle ) == 1;
		
	}
	
	//----------------------------------------------------------------------------
	
	public function debug(){
		
		echo "<p><u>idarticle = {$this->idarticle}</u></p>";
		
		print_r( $this->fields );
			
	}
	
	//----------------------------------------------------------------------------

	/**
	 * magique!
	 */
	function __autoload( $classname ){
		
		if( file_exists( dirname( __FILE__ ) . "/{$classname}.php" ) )
			include_once( dirname( __FILE__ ) . "/{$classname}.php" );
			
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * @deprecated préférer DefaultBasketItem::get( $key )
	 * Retourne la valeur d'une propriété donnée de l'article
	 * @param string $key la propriété souhaitée
	 * @return mixed la valeur de la propriété $key
	 */
	public function getValue( $key ){
	
		return $this->fields[ $key ];
			
	}
	
	//----------------------------------------------------------------------------
	
	/**
	 * @deprecated préférer DefaultBasketItem::set( $key, $value )
	 * Définit la valeur d'une propriété donnée de l'article
	 * @param string $key la propriété à modifier
	 * @param mixed $value la valeur à donner à la propriété $key
	 * @return void
	 */
	public function setValue( $key, $value ){
		
		$this->fields[ $key ] = $value;
		
	}
	
	//----------------------------------------------------------------------------
		
}
 
?>