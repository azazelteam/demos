<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures fournisseurs
 */
 
//------------------------------------------------------------------------------------------------
include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../objects/DHTMLCalendar.php" );

$Title = "Recherche de facture / avoir fournisseur";

//------------------------------------------------------------------------------------------------

if( isset( $_GET[ 'delete' ] ) && $_GET[ 'delete' ] == 'supplier_invoice' ){
	
	include_once( dirname( __FILE__ ) . "/../objects/SupplierInvoice.php" );
	
	if( !isset( $_POST[ 'supplierInvoice' ] ) )
		exit( " Aucune facture " );
		
	if( !isset( $_POST[ 'idsupplier' ] ) )
		exit( " Aucun fournisseur " );
			
	$supplier_invoice = $_POST[ 'supplierInvoice' ];
	$idsupplier = $_POST[ 'idsupplier' ];
	
	SupplierInvoice::delete( $supplier_invoice , $idsupplier );
	
	exit( '1' );
	
}

//------------------------------------------------------------------------------------------------

if( isset( $_GET[ 'delete' ] ) && $_GET[ 'delete' ] == 'supplier_credit' ){
	include_once( dirname( __FILE__ ) . "/../objects/SupplierCredit.php" );
	
	if( !isset( $_POST[ 'supplierCredit' ] ) )
		exit( " Aucun avoir " );
		
	if( !isset( $_POST[ 'idsupplier' ] ) )
		exit( " Aucun fournisseur " );
	
	$supplier_credit = $_POST[ 'supplierCredit' ];
	$idsupplier = $_POST[ 'idsupplier' ];
	
	SupplierCredit::delete( $supplier_credit , $idsupplier );
	
	exit( '1' );
}

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

if( isset( $_GET[ "export" ] ) ){
	
	$exportableArray =& getExportableArray();
	exportArray( $exportableArray );
	
	exit();
	
}

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?><div id="globalMainContent"><?php

displayForm();


?></div><!-- globalMainContent --><?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "Date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier				= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
	$total_amount			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) ? $_POST[ "idbl_delivery" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$status					= isset( $_POST[ "status" ] ) ? $_POST[ "status" ] : "";
	
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
				
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}


		function goForm(){
			var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};
		 
		    // bind to the form's submit event 
		    $('#searchForm').ajaxSubmit(options); 

			
				

		}
		
		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			
			$('#searchForm').ajaxForm( options );
						
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
		}

		/* ----------------------------------------------------------------------------------- */
		
		function removeSupplierInvoice( supplierInvoice , idsupplier , id ){

			var datas = "idsupplier=" + idsupplier + "&supplierInvoice=" + supplierInvoice;

			if( !confirm( 'Supprimer la facture fournisseur ?' ) )
				return false;

			var myreturn = false;
			
			$.ajax({
			 	
				url: "<?php echo HTTP_HOST; ?>/accounting/search_supplier_invoices.php?delete=supplier_invoice",
				type: "POST",
				async: true,
				data: datas,
				error: function(){ alert( "Impossible de supprimer la facture !" ); },
			 	success: function( responseText ){
			 		if( responseText == "1" ){
			 			$.growlUI( 'Suppression effectuée' , '' );
			 			$( '#' + id ).remove();
			 		}else{
						alert( "Impossible de supprimer la facture !" + responseText );
				 	}
				}

			});

			
		}

		/* ----------------------------------------------------------------------------------- */
		
		function removeSupplierCredit( supplierCredit , idsupplier , id ){
			
			var datas = "idsupplier=" + idsupplier + "&supplierCredit=" + supplierCredit;

			if( !confirm( "Supprimer l'avoir fournisseur ?" ) )
				return false;

			var myreturn = false;
			
			$.ajax({
			 	
				url: "<?php echo HTTP_HOST; ?>/accounting/search_supplier_invoices.php?delete=supplier_credit",
				type: "POST",
				async: true,
				data: datas,
				error: function(){ alert( "Impossible de supprimer l'avoir !" ); },
			 	success: function( responseText ){
			 		if( responseText == "1" ){
			 			$.growlUI( 'Suppression effectuée' , '' );
			 			$( '#' + id ).remove();
			 		}else{
						alert( "Impossible de supprimer l'avoir !" + responseText );
				 	}
				}

			});

		}

		/* ----------------------------------------------------------------------------------- */
		
	// -->
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
    	<div class="mainContent">
        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
            <div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            <form action="<?php echo $GLOBAL_START_URL ?>/accounting/search_supplier_invoices.php" method="post" name="searchForm" id="searchForm">
	  			<input type="hidden" name="search" value="1" />
            	<div class="headTitle">
                    <p class="title">Rechercher une facture/avoir fournisseur</p>
                    <div class="rightContainer">
                    	<input type="radio" name="date_type" value="Date"<?php if( $date_type == 'Date') echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Date de facture/avoir
                    	<input type="radio" name="date_type" value="creation_date"<?php if( $date_type == 'creation_date') echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Date d'enregistrement
                        <input type="radio" name="date_type" value="deliv_payment"<?php if( $date_type == 'deliv_payment') echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Date d'échéance
                    </div>
                </div>
                <div class="subContent">
                		<!-- tableau choix de date -->
                        <div class="tableContainer">
                            <table class="dataTable dateChoiceTable">
                                <tr>	
                                    <td><input type="radio" name="interval_select" id="interval_select_since" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Depuis</td>
                                    <td>
                                        <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							            	<option value="0"<?php if($minus_date_select==0 ) echo " selected=\"selected\""; ?>>Aujourd'hui</option>
							              	<option value="-1"<?php if($minus_date_select==-1 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_yesterday")  ?></option>
						              		<option value="-2"<?php if($minus_date_select==-2) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2days")  ?></option>
							              	<option value="-7"<?php if($minus_date_select==-7) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1week") ?></option>
							              	<option value="-14"<?php if($minus_date_select==-14) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2weeks") ?></option>
							              	<option value="-30"<?php if($minus_date_select==-30) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1month") ?></option>
							              	<option value="-60"<?php if($minus_date_select==-60) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2months") ?></option>
							              	<option value="-90"<?php if($minus_date_select==-90) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_3months") ?></option>
							              	<option value="-180"<?php if($minus_date_select==-180) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_6months") ?></option>
							              	<option value="-365"<?php if($minus_date_select==-365) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1year") ?></option>
							              	<option value="-730"<?php if($minus_date_select==-730) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
							              	<option value="-1095"<?php if($minus_date_select==-1095) echo " selected=\"selected\""; ?>>Moins de 3 ans</option>
							              	<option value="all"<?php if($minus_date_select=="all") echo " selected=\"selected\""; ?>>Toutes</option>
							            </select>
                                    </td>
                                    <td><input type="radio" name="interval_select" id="interval_select_month" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> class="VCenteredWithText" />Pour le mois de</td>
                                    <td>
                                        <?php FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                    </td>
                                </tr>
                                <tr>	
                                    <td>
                                        <input type="radio" name="interval_select" id="interval_select_between" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> class="VCenteredWithText" />Entre le
                                    </td>
                                    <td>
										<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
						 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                    </td>
                                    <td>et le</td>
                                    <td>
						 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4" class="tableSeparator"></td>
                                </tr>
                                <tr class="filledRow">
                                    <th>Facture n°</th>
                                    <td><input type="text" size="10" name="billing_supplier"  value="<?php echo $billing_supplier ?>" class="textInput" /></td>
                                    <th>Fournisseur</th>
                                    <td><?php DrawSupplierList( $idsupplier ); ?></td>
                                </tr>
                                <tr class="filledRow">
                                    <th>BL n°</th>
                                    <td><input type="text" size="10" name="idbl_delivery"  value="<?php echo $idbl_delivery ?>" class="textInput" /></td>
                                    <th>Cde fournisseur n°</th>
                                    <td><input type="text" size="10" name="idorder_supplier"  value="<?php echo $idorder_supplier ?>" class="textInput" /></td>
                                </tr>
                                 <tr class="filledRow">
                                    <th>Montant TTC</th>
                                    <td><input type="text" size="10" name="total_amount"  value="<?php echo $total_amount ?>" class="textInput" /></td>
                                    <th>Factures comptabilisables</th>
                                    <td>
                                    	<input type="radio" name="editable" value="-1"<?php if( !isset( $_POST[ "editable" ] ) || $_POST[ "editable" ] == -1 ) echo " checked=\"checked\""; ?> /> sans préférence
                                    	<input type="radio" name="editable" value="0"<?php if( isset( $_POST[ "editable" ] ) && $_POST[ "editable" ] == 0 ) echo " checked=\"checked\""; ?> /> non
                                    	<input type="radio" name="editable" value="1"<?php if( isset( $_POST[ "editable" ] ) && $_POST[ "editable" ] == 1 ) echo " checked=\"checked\""; ?> /> oui
									</td>
                                </tr>
                                <tr class="filledRow">
                                	<th>Statut</th>
                                	<td>
                                		<select name="status">
                                			<option value=''></option>
                                			<option value='Paid'>Payé / Utilisé</option>
                                			<option value='StandBy'>En attente de paiement / Non Utilisé</option>
                                		</select>
                                	</td>
                                	<th>Filtrer</th>
                                	<td><input type="checkbox" name="withoutCredits" value="1"> Ne pas afficher les avoirs</td>
                                </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton" style="text-align: center;" value="Rechercher" />
                        </div>
                </div>
                </form>
            </div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
	<div class="clear"></div>
	<div id="SearchResults"></div>
<?php 

}

/////-----------------------

function search(){

	global $hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH, $ScriptName;

	$now = getdate();

	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
	$total_amount			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) ? $_POST[ "idbl_delivery" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$status					= isset( $_POST[ "status" ] ) ? $_POST[ "status" ] : "";
	
	$SQL_Condition			= "";
	$SQL_ConditionAvoir		= "";
	
	switch ($interval_select){
			    
		case 1:
			//min
			if( $minus_date_select == "all" )
				$Min_Time = "0000-00-00";
			else
				$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >= '$Min_Time' AND `billing_supplier`.$date_type <= '$Now_Time' ";
			$SQL_ConditionAvoir = "cs.Date >= '$Min_Time' AND cs.Date <= '$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >= '$Min_Time' AND `billing_supplier`.$date_type < '$Max_Time' ";
			$SQL_ConditionAvoir  = "cs.Date >= '$Min_Time' AND cs.Date < '$Max_Time' ";
			
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`billing_supplier`.$date_type >= '$Min_Time' AND `billing_supplier`.$date_type <= '$Max_Time' ";
	     	$SQL_ConditionAvoir  = "cs.Date >= '$Min_Time' AND cs.Date <= '$Max_Time' ";
	     	break;
			
	}
	
	if( !$hasAdminPrivileges ){
		$SQL_Condition .= " AND `billing_supplier`.iduser = '$iduser'";
		$SQL_ConditionAvoir .= " AND cs.iduser = '$iduser'";
	}
					
	if ( !empty($idsupplier) ){
		$idsupplier = FProcessString($idsupplier);
		$SQL_Condition .= " AND billing_supplier.idsupplier =$idsupplier";
		$SQL_ConditionAvoir .= " AND cs.idsupplier =$idsupplier";
	}
	
	if ( !empty( $billing_supplier ) ){
		$SQL_Condition .= " AND billing_supplier.billing_supplier LIKE '%$billing_supplier%'";
		$SQL_ConditionAvoir .= " AND ( cs.billing_supplier LIKE '%$billing_supplier%' OR cs.idcredit_supplier LIKE '%$billing_supplier%' ) ";
	}
	
	if ( !empty($total_amount) ){
		$SQL_Condition .= " AND billing_supplier.total_amount = $total_amount";
		$SQL_ConditionAvoir .= " AND cs.amount = $total_amount";
	}
	
	if( !empty($idbl_delivery) ){
		$SQL_Condition .= " AND bl_delivery.idbl_delivery = $idbl_delivery";
	}

	if( !empty($idorder_supplier) ){
		$SQL_Condition .= " AND order_supplier.idorder_supplier = $idorder_supplier";
	}
	
	if( isset( $_POST[ "editable" ] ) && $_POST[ "editable" ] != -1 )
		$SQL_Condition .= " AND billing_supplier.editable = '" . intval( $_POST[ "editable" ] ) . "'";
	
	$havingavoir = '';
	
	if( !empty($status) ){
		$SQL_Condition .= " AND billing_supplier.`status` = '$status'";
		
		if( $status == 'Paid' )
			$havingavoir .= " HAVING amountUsed > 0";
		else if( $status == 'StandBy' )
			$havingavoir .= " HAVING amountUsed = 0";
	}
				
	
	
?>
<script type="text/javascript" language="javascript">
<!--
			
	function registerInvoice(idbilling,idsupplier){
				
		var invoicehref = 'modify_supplier_invoices.php?supplier='+idsupplier+'&billing='+idbilling;
					
		postop = (self.screen.height-550)/2;
		posleft = (self.screen.width-800)/2;

		window.open(invoicehref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=500,height=200,resizable,scrollbars=yes,status=0');
		
	}
	
// -->
</script>

<form action="<?php echo $ScriptName ?>" method="post" name="invoicetopay" id="invoicetopay">
	<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
	<input type="hidden" name="search" value="1" />
	<?php

	$SQL_Condition	.= " AND billing_supplier.iduser = user.iduser
	AND billing_supplier.idsupplier = supplier.idsupplier
	AND billing_supplier.proforma = 0";
	
	$SQL_Query = "
	SELECT billing_supplier.billing_supplier, 
		billing_supplier.idsupplier,
		billing_supplier.editable,
		billing_supplier.status,
		supplier.name,
		billing_supplier.Date,
		billing_supplier.creation_date, 
		billing_supplier.deliv_payment, 
		user.initial,
		billing_supplier.total_amount,
		billing_supplier.rebate_type,
		billing_supplier.rebate_value,
		billing_supplier.billing_supplier_doc
	FROM user, supplier, billing_supplier
	LEFT JOIN billing_supplier_row bsr ON ( bsr.billing_supplier = billing_supplier.billing_supplier AND bsr.idsupplier = billing_supplier.idsupplier )
	LEFT JOIN bl_delivery ON ( bl_delivery.idbl_delivery = bsr.idbl_delivery AND bl_delivery.idsupplier = bsr.idsupplier )
	LEFT JOIN order_supplier ON order_supplier.idorder_supplier = bl_delivery.idorder_supplier
	WHERE $SQL_Condition
	GROUP BY billing_supplier.billing_supplier, billing_supplier.idsupplier";
		
	if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
			$SQL_Query .= " ORDER BY " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ;
	else 	$SQL_Query .= " ORDER by billing_supplier.billing_supplier ASC, billing_supplier.idsupplier ASC";

	$rs = DBUtil::query($SQL_Query);
	
	// on sort les avoirs 
	if( isset( $_POST['withoutCredits'] ) && $_POST['withoutCredits'] == 1 ){
		$rsAvoir = false;
		$rsAvoirCount = 0;
	}else{
		$SQL_QueryAvoirs = "SELECT u.initial , s.name , cs.* , ifnull( SUM(csr.amount), 0 ) as amountUsed, cs.credit_supplier_doc
							FROM user u , supplier s , credit_supplier cs
							LEFT JOIN credit_supplier_regulation csr ON csr.idcredit_supplier = cs.idcredit_supplier
							WHERE $SQL_ConditionAvoir
							AND cs.iduser = u.iduser
							AND cs.idsupplier = s.idsupplier
							GROUP BY cs.idcredit_supplier $havingavoir";
		
		$rsAvoir = DBUtil::query($SQL_QueryAvoirs);
		
		$rsAvoirCount = $rsAvoir->RecordCount();
	}
	
	if ($rs===false)
		die("Impossible de récupérer les factures fournisseurs");
	
	$rsCount = $rs->RecordCount();
	
	if ( $rsCount > 0 || $rsAvoirCount > 0 ){
			?>
			<div class="mainContent fullWidthContent">
            	<div class="topRight"></div><div class="topLeft"></div>
                <div class="content">
                	<div class="headTitle">
                        <p class="title"><?php echo $rsCount + $rsAvoirCount ?> facture(s)/avoir(s) trouvée(s)</p>
                        <?php
							$exportURL = "$GLOBAL_START_URL/accounting/search_supplier_invoices.php?export";
							
							$Enow = getdate();
								
							$Edate_type 			= "&amp;date_type=";
							$Eminus_date_select 	= "&amp;minus_date_select=";
							$Einterval_select 		= "&amp;interval_select=";
							$Eidsupplier 			= "&amp;idsupplier=";
							$Ebilling_supplier		= "&amp;billing_supplier=";
							$Etotal_amount			= "&amp;total_amount=";
							$Ebt_start				= "&amp;bt_start=";
							$Ebt_stop				= "&amp;bt_stop=";
		 	
							$Edate_type 			.= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
							$Eminus_date_select 	.= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
							$Einterval_select 		.= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
							$Eidsupplier 			.= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
							$Ebilling_supplier		.= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
							$Etotal_amount			.= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
							$Ebt_start				.= $_POST[ "bt_start" ];
							$Ebt_stop				.= $_POST[ "bt_stop" ];
							

							
							$exportURLElements = $Edate_type.$Eminus_date_select.$Einterval_select.$Eidsupplier.$Ebilling_supplier.$Etotal_amount.$Ebt_start.$Ebt_stop;
								
							if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) )
								$exportURLElements .= "&amp;sortby=" . htmlentities( $_GET[ "sortby" ] ) . "&amp;sens=" . htmlentities( $_GET[ "sens" ] );							
							
							
							
							
						?>
						<div class="rightContainer">
							<a href="<?php echo $exportURL.$exportURLElements ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
						</div>
                    </div>
                    <div class="subContent">
						<div class="tableHeaderLeft">
							<a href="#" class="goUpOrDown">
								Aller en bas de page
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
							</a>
						</div>
						<div class="tableHeaderRight">
						</div>
                    	<!-- tableau résultats recherche -->
                        <div class="resultTableContainer clear">
                        	<table class="dataTable resultTable">
                            	<thead>
							       	<tr>
							       		<th>Com.</th>
							       		<th>Facture/Avoir</th>
							       		<th>Fournisseur</th>
							       		<th>Statut</th>
							       		<th>Date de facture/avoir</th>
							       		<th>Date enregistrement</th>
							       		<th>Date d'échéance</th>
								      	<th>Total TTC</th>
								      	<th>Solde à payer</th>
								      	<th>Cde. fournisseur n°</th>
								      	<th>BL n°</th>
								      	<th></th>
								      	<!--<th></th>-->
							       	</tr>
									<tr>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'user.initial', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'user.initial', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.billing_supplier', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.billing_supplier', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'supplier.name', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'supplier.name', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.status', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.status', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.Date', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.Date', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.creation_date', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.creation_date', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.deliv_payment', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.deliv_payment', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
										<!--<th class="noTopBorder"></th>-->
									</tr>
						       	</thead>
						       	<tbody>
<?php
								
								$totaliseTTC = 0;
								$totaliseSolde = 0;
								
								for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
									
									if( $rs->fields( 'billing_supplier_doc' ) && file_exists( DOCUMENT_ROOT . '/data/billing_supplier/' . $rs->fields( 'billing_supplier_doc' ) ) )
										$billing_supplier	= "<a href='".HTTP_HOST."/data/billing_supplier/".$rs->fields( "billing_supplier_doc" )."' onclick='window.open(this.href);return false;'>".$rs->fields( "billing_supplier" )."</a>";
									else
										$billing_supplier	= $rs->fields( "billing_supplier" );
									
									$initials			= $rs->fields( "initial" );
									$Date				= $rs->fields( "Date" );
									$creation_date		= $rs->fields( "creation_date" );
									$deliv_payment		= $rs->fields( "deliv_payment" );
									$supplier_name		= $rs->fields( "name" );
									$idsupplier			= $rs->fields( "idsupplier" );
									$rebate 			= 0;
									if( $rs->fields( "rebate_value" ) > 0)
										$rebate 		= $rs->fields( "rebate_type" ) == "amount" ? $rs->fields( "rebate_value" ) : $rs->fields('total_amount') * $rs->fields( "rebate_value" ) / 100.0;
									
									$total_amount		= $rs->fields( "total_amount" ) - $rebate;
									$totaliseTTC		+= $total_amount;
									$status				= $rs->fields( "status" );
									$bls				= SupplierInvoice::getInvoicedDeliveryNotes( $rs->fields( "billing_supplier" ), $idsupplier );
									$os					= SupplierInvoice::getSupplierInvoiceOS( $rs->fields( "billing_supplier" ), $idsupplier );
									$soulde				= SupplierInvoice::getBalanceOutstanding( $rs->fields( "billing_supplier" ), $idsupplier );
									$totaliseSolde		+= $soulde;
									
									?>
							        <tr id="billing_<?php echo $i?>" class="blackText"<?php if( $rs->fields( "editable" ) ) echo " style=\"background-color:#F3F3F3;\""; ?>>
							        	<td class="lefterCol"><?php echo $initials ?></td> 
							        	<td><?php echo $billing_supplier ?></td>
							        	<td><a class="grasBack" href="/product_management/enterprise/supplier.php?idsupplier=<?php echo $idsupplier ?>" onclick="window.open(this.href);return false;"><?php echo $supplier_name ?></a></td>
							        	<td><?php echo Dictionnary::translate( $status ) ?></td>	
										<td><?php echo Util::dateFormatEu( $Date, false, "-" ) ?></td>
										<td><?php echo Util::dateFormatEu( $creation_date, false, "-" ) ?></td>
										<td><?php echo Util::dateFormatEu( $deliv_payment, false, "-" ) ?></td>
										<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
										<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat($soulde)?></td>
										<td>
										<?php
										
										if( is_array( $os ) ){
											
											for( $j = 0 ; $j < count( $os ) ; $j++ ){
												
												if( $j )
													echo " - ";
												
												echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_supplier_order.php?idorder=$os[$j]\" onclick=\"window.open(this.href);return false;\">$os[$j]</a>";
													
											}
											
										}
										
										?>
										</td>
										<td>
										<?php
										
										if( is_array( $bls ) ){
											
											for( $k = 0 ; $k < count( $bls ) ; $k++ ){
												
												if( $k )
													echo " - ";
												
												echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$bls[$k]\" onclick=\"window.open(this.href);return false;\">$bls[$k]</a>";
													
											}
											
										}
										
										?>
										</td>
										<td class="righterCol">
										<?php
										
											if( $rs->fields( "editable" ) ){
												
												?>
												<a title="Modifier la facture" href="<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=<?php echo urlencode( $rs->fields( "billing_supplier" ) ); ?>&amp;idsupplier=<?php echo $rs->fields( "idsupplier" ); ?>" onclick="window.open(this.href); return false;">
													<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/flexigrid_button_headings.png" alt="Modifier" />
												</a>
												<?php
													
											}
											
										?>
										</td>
										<!--<td class="righterCol">
											
											<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/help.png" onclick="registerInvoice('<?=$billing_supplier?>',<?=$idsupplier?>)"/>
											
											<?php if( $status == SupplierInvoice::$STATUS_STANDBY ){ ?>
											<a href="#" onclick="removeSupplierInvoice( '<?php echo $rs->fields( "billing_supplier" ); ?>' , <?php echo $idsupplier ?> , 'billing_<?php echo $i ?>' ) ; return false;" style="float:left;">
												<img src="/images/back_office/content/corbeille.jpg" alt="Supprimer" />
											</a>
											<?php } ?>
										</td>-->
									</tr>
									<?php
									$rs->MoveNext();			
								
								}
								
								if( $rsAvoir !== false){
									for( $i = 0 ; $i < $rsAvoir->RecordCount() ; $i++ ){
																				
										if( $rsAvoir->fields( 'credit_supplier_doc' ) && file_exists( DOCUMENT_ROOT . '/data/credit_supplier/' . $rsAvoir->fields( 'credit_supplier_doc' ) ) )
											$billing_supplier	= "<a href='".HTTP_HOST."/data/credit_supplier/".$rsAvoir->fields( "credit_supplier_doc" )."' onclick='window.open(this.href);return false;'>".$rsAvoir->fields( "idcredit_supplier" )."</a>";
										else
											$billing_supplier	= $rsAvoir->fields( "idcredit_supplier" );
										
										
										$initials			= $rsAvoir->fields( "initial" );
										$Date				= $rsAvoir->fields( "Date" );
										$creation_date		= "-";
										$deliv_payment		= "-";
										$supplier_name		= $rsAvoir->fields( "name" );
										$idsupplier			= $rsAvoir->fields( "idsupplier" );
										$rebate 			= 0;									
										$total_amount		= $rsAvoir->fields( "amount" );
										$totaliseTTC		-= $total_amount;
										$status				= '';
										$soldeAvoir			= $total_amount - DBUtil::query( "SELECT ifnull( sum(amount) , 0) as used FROM credit_supplier_regulation WHERE idcredit_supplier='" . $rsAvoir->fields( "idcredit_supplier" ) . "'")->fields( 'used' );
										$totaliseSolde		-= $soldeAvoir;
																		
										if( $soldeAvoir == 0 )
											$status = 'Utilisé';
										else if( $soldeAvoir == $total_amount )
											$status = 'Non utilisé';
										else
											$status = 'Partiellement utilisé';
										
	
										?>
								        <tr id="credit_<?php echo $i?>"class="blackText" style="color:#ff0000;">
								        	<td class="lefterCol"><?php echo $initials ?></td> 
								        	<td><?php echo $billing_supplier ?></td>
								        	<td><a class="grasBack" style="color:#ff0000;" href="/product_management/enterprise/supplier.php?idsupplier=<?php echo $idsupplier ?>" onclick="window.open(this.href);return false;"><?php echo $supplier_name ?></a></td>
								        	<td><?php echo Dictionnary::translate( $status ) ?></td>	
											<td><?php echo Util::dateFormatEu( $Date, false, "-" ) ?></td>
											<td><?php echo Util::dateFormatEu( $creation_date, false, "-" ) ?></td>
											<td><?php echo Util::dateFormatEu( $deliv_payment, false, "-" ) ?></td>
											<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
											<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $soldeAvoir ) ?></td>
											<td></td>
											<td></td>
											<td class="righterCol"></td>
											<!--<td class="righterCol">
												<?php if( $status == 'Non utilisé' ){ ?>
												<a href="#" onclick="removeSupplierCredit( '<?php echo $rsAvoir->fields( "idcredit_supplier" ); ?>' , <?php echo $idsupplier ?> , 'credit_<?php echo $i ?>' ); return false;" style="float:left;">
													<img src="/images/back_office/content/corbeille.jpg" alt="Supprimer" />
												</a>
												<?php } ?>
											</td>-->
										</tr>								
										<?php
										$rsAvoir->MoveNext();			
									
									}
								}
			
							?>
				       	<tr>
				       		<th colspan="7" style="border:none;"></th>
					      	<th class="totalAmount" style="white-space:nowrap;"><?php echo Util::priceFormat( $totaliseTTC ) ?></th>
					      	<th class="totalAmountSolde" style="white-space:nowrap;"><?php echo Util::priceFormat( $totaliseSolde ) ?></th>
					      	<th colspan="2" style="border:none;"></th>
				       	</tr>
					</tbody>
	            </table>
                </div>
                <a href="#" class="goUpOrDown">
                	Aller en haut de page
                    <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
			<?		
	}else{ //pas de résultat
		?>
		<div class="mainContent fullWidthContent">
        	<div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<div class="headTitle">
                    <p class="title">
                    	Aucune facture fournisseur n'a été trouvé !
                    </p>
					<div class="rightContainer">
					
					</div>
                </div>
       		</div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div> <!-- mainContent fullWidthContent -->
		
		
		
		<!--<p class="msg" style="text-align:center"><?php  echo Dictionnary::translate("no_result"); ?></p>-->
		<?php
	}
		
		?>
	</form>
	<?php

}


//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------

function getBLTotalAmountHT($idbl_delivery){
	
	$bdd = DBUtil::getConnection();
	
	$Query = "SELECT osr.discount_price, osr.quantity FROM order_supplier os";	
}

//--------------------------------------------------------------------------------

function &getExportableArray(){

	global $hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH, $ScriptName;

	$now = getdate();
	
	//die(print_r($_GET));
	
	$date_type 				= isset( $_GET[ "date_type" ] ) ? $_GET[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_GET[ "minus_date_select" ] ) ? $_GET[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_GET[ "interval_select" ] ) ? $_GET[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_GET[ "idsupplier" ] ) ? $_GET[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_GET[ "billing_supplier" ] ) ? $_GET[ "billing_supplier" ] : "";
	$total_amount			= isset( $_GET[ "total_amount" ] ) ? $_GET[ "total_amount" ] : "";
		
	switch ($interval_select){
			    
		case 1:
		 	//min |2000-12-18 15:25:53|
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_GET['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_GET[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_GET[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !$hasAdminPrivileges )
		$SQL_Condition .= " AND `billing_supplier`.iduser = '$iduser'";
						
	if ( !empty($idsupplier) ){
			$idsupplier = FProcessString($idsupplier);
			$SQL_Condition .= " AND billing_supplier.idsupplier =$idsupplier";
	}
	
	if ( !empty($billing_supplier) ){
			$SQL_Condition .= " AND billing_supplier.billing_supplier ='$billing_supplier'";
	}
	
	if ( !empty($total_amount) ){
			$SQL_Condition .= " AND billing_supplier.total_amount = $total_amount";
	}
	
	$SQL_Condition	.= " AND billing_supplier.iduser = user.iduser
	AND billing_supplier.idsupplier = supplier.idsupplier";
	
	$query = "
	SELECT billing_supplier.billing_supplier, 
		billing_supplier.idsupplier,
		billing_supplier.status,
		supplier.name,
		billing_supplier.Date,
		billing_supplier.creation_date, 
		billing_supplier.deliv_payment, 
		user.initial,
		billing_supplier.total_amount
	FROM billing_supplier, user, supplier
	WHERE $SQL_Condition
	ORDER by billing_supplier.billing_supplier ASC
	";
	
	if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
		$SQL_Query .= "
		ORDER BY " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ;	
	
	$rs =& DBUtil::query( $query );
	
	$data = array();
	
	while( !$rs->EOF() ){
		
		$billing_supplier	= $rs->fields( "billing_supplier" );
		$initials			= $rs->fields( "initial" );
		$deliv_payment		= $rs->fields( "deliv_payment" );
		$supplier_name		= $rs->fields( "name" );
		$idsupplier			= $rs->fields( "idsupplier" );
		$total_amount		= $rs->fields( "total_amount" );
		$status				= $rs->fields( "status" );
		$bls				= SupplierInvoice::getInvoicedDeliveryNotes( $billing_supplier, $idsupplier );
		$os					= SupplierInvoice::getSupplierInvoiceOS( $billing_supplier, $idsupplier );
	
		if( $deliv_payment == "0000-00-00" )
			$deliv_payment = "";
		else
			$deliv_payment = usDate2eu( $deliv_payment );
			
		$Date = usDate2eu( $rs->fields( "Date" ) );
		$creation_date = usDate2eu( $rs->fields( "creation_date" ) );
	  	
	  	$txtos="";
	  	if(is_array($os)){
			for($j=0;$j<count($os);$j++){
				if($j)
					$txtos .= "-";
					
				$txtos .= $os[$j];	
			}	
		}

	  	
	  	$txtBls = "";
		if(is_array($bls)){
			for($k=0;$k<count($bls);$k++){
				if($k)
					$txtBls .= "-";
					
				$txtBls .= $bls[$k];	
			}	
		}


	   	$data[ "Commercial" ][]				= $initials;
		$data[ "Facture n°" ][] 			= $billing_supplier;
		$data[ "Fournisseur" ][] 			= $supplier_name;
		$data[ "Statut" ][] 				= Dictionnary::translate( $status );
		$data[ "Date de facture" ][] 		= $Date;
		$data[ "Date enregistrement" ][] 	= $creation_date;
		$data[ "Date d'échéance" ][] 		= $deliv_payment;
	  	$data[ "Total TTC" ][] 				= Util::numberFormat( $total_amount );
	  	$data[ "Commande fournisseur n°" ][]= $txtos;
	  	$data[ "BL n°" ][] 					= $txtBls;	
	  	
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------

function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray,"factures_fournisseur_" . date( "Ymd" ) );
	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------
?>