<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement des factures de service
 */

include_once( "../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/objects/Supplier.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );

//------------------------------------------------------------------------------------------
/* traitement ajax 2m le retour */

if( isset( $_POST[ "registerInvoice" ] ) && $_POST[ "registerInvoice" ] == "1" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" ); 
	
	$result = registerInvoice();
	
	exit($result);
		
}


include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

displayInvoice();

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );


//---------------------------------------------------------------------------------------------------------------------
function displayInvoice(){
	
	global $GLOBAL_START_URL;

	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var optionsInvoices = {
			 	
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postInvoiceSubmitCallback  // post-submit callback 
  				
			};
			
			$('#frm').ajaxForm( optionsInvoices );
		});

		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
				
				message: "Traitement en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
				
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postInvoiceSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' ){
				
				alert( "Impossible d'effectuer le traitement" );
				return;
				
			}
			
			if( responseText == "0" ){
				$.growlUI( '', 'Facture enregistrée' );
			}else{
				$.growlUI( '', responseText );
			}
		}
		
		
	/* ]]> */
	</script>
	<form id="frm" method="POST" action="<?=$GLOBAL_START_URL ?>/accounting/register_service_invoice.php">
		<input type="hidden" name="registerInvoice" value="1" />
		<div id="globalMainContent" style="width:50%;">
        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
			<div class="mainContent fullWidthContent">
				<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title">Enregistrement factures autres achats</p>
					</div>
					<div class="subContent" id="container">
						<div class="resultTableContainer" style="margin-top:20px;">
							<table class="dataTable">
								<tr>
									<th class="filledCell">N° de facture <span class="asterix">*</span></th>
									<td><input type="text" name="billing_supplier" class="textInput" /></td>
								</tr>
								<tr>
									<th class="filledCell">Fournisseur <span class="asterix">*</span></th>
									<td>
										<?
													
										XHTMLFactory::createSelectElement( "supplier", "idsupplier", "name", "name", $selected, "0", "", $attributes = "id=\"idsupplier\" name=\"idsupplier\" onchange=\"selectSupplier( this.options[ this.selectedIndex ].value ); checkIfRefExist();\" onkeyup=\"this.onchange();\"",false, false,"AND supplier.idsupplier_type <> 1" );
										
										?>
									</td>
								</tr>
								<tr>
									<th class="filledCell">Date de facture <span class="asterix">*</span></th>
									<td>
										<input type="text" name="Datef" id="Datef" class="calendarInput" value="<?php echo date( "d-m-Y" ); ?>" />
										<?php DHTMLCalendar::calendar( "Datef" ); ?>
									</td>
								</tr>
								<tr>
									<th class="filledCell">Mode de paiement <span class="asterix">*</span></th>
									<td><?=DrawLift_modepayment(3)?></td>
								</tr>
								<tr>
									<th class="filledCell">Délai de paiement <span class="asterix">*</span></th>
									<td>
										<?php 
										XHTMLFactory::createSelectElement( 
				
													"payment_delay", 
													"idpayment_delay", 
													"payment_delay" . User::getInstance()->getLang(), 
													"idpayment_delay", 
													1,
													false,
													"",
													"name=\"idpayment_delay\" id=\"idpayment_delay\""
													
										);?>
									</td>
								</tr>
								<tr>
									<th class="filledCell">Date d'échéance <span class="asterix">*</span></th>
									<td>
										<input type="text" name="deliv_payment" id="deliv_payment" class="calendarInput" value="" />
										<?php DHTMLCalendar::calendar( "deliv_payment" ); ?>
									</td>
								</tr>
								<tr>
									<th class="filledCell">Montant TTC <span class="asterix">*</span></th>
									<td><input type="text" id="total_amount_ttc" name="total_amount_ttc" class="textInput price" style="width:30%;" value="<?php echo number_format( round( $totalATI, 2 ), 2, ",", "" ); ?>" /> &euro;</td>
								</tr>
								<tr>
									<th class="filledCell">Taux TVA <span class="asterix">*</span></th>
									<td>
									<?php 
										XHTMLFactory::createSelectElement( 
				
													"tva", 
													"tva", 
													"tva", 
													"idtva", 
													19.60,
													0,
													"Pas de TVA",
													"name=\"tva\" id=\"tva\""
													
										);?>
									
									 %</td>
								</tr>
								<tr>
									<th class="filledCell">Escompte</th>
									<td><input type="text" name="rebate_rate" class="textInput percentage" style="width:30%;" value="" /> %</td>
								</tr>
								<tr>
									<th class="filledCell">Facture de service</th>
									<td><input type="checkbox" name="service" class="textInput" value="1" style="width:1%;" /></td>
								</tr>
							</table>
						</div>
						<div style="float:left; margin-top:10px;">Les champs marqués par <span class="asterix">*</span> sont obligatoires</div>
						<br style="clear:both;" />
						<div class="submitButtonContainer">
							<input type="submit" name="register" value="Enregistrer" class="blueButton" />
						</div>
					</div>
				</div>
				<div class="bottomRight"></div><div class="bottomLeft"></div>
			</div>
		</div>
	</form>	
	<?
}


//------------------------------------------------------------------------------------------------

function registerInvoice(){
	

	//Test des champs postés
	$fields = array ( "billing_supplier" , "idsupplier", "Datef" ,  "deliv_payment" , "total_amount_ttc"  );
	$fields_name = array ( "N° de fature" , "Fournisseur", "Date de facture" , "Date d'échéance", "Montant TTC" );
	
	for ($i=0;$i<count($fields);$i++){
		if( empty( $_POST[ $fields[$i] ] ) ){
				return "Le champ \"$fields_name[$i]\" doit être rempli";	
		}
	}

	//Vérification du numéro de facture fournisseur pour voir si il n'existe pas
	$billing_supplier = $_POST["billing_supplier"];
	$idsupplier = $_POST[ "idsupplier" ];
	
	$query = "SELECT billing_supplier FROM billing_supplier WHERE billing_supplier='$billing_supplier' AND idsupplier=$idsupplier";
	$rs = DBUtil::query($query);
	
	if($rs === false)
		trigger_error( "Impossible de vérifier le numéro de facture fournisseur", E_USER_ERROR ) && exit();
		
	if($rs->RecordCount()>0)
		return "Ce numéro de facture fournisseur existe déjà";

	
	//Tous les champs sont remplis, test des champs numériques
	if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "total_amount_ttc" ] ) ) 
		return "Le champ montant TTC doit être un nombre";
	
	if( !empty( $_POST[ "rebate_rate" ] ) && !preg_match( "/^[0-9\., ]+\$/", $_POST[ "rebate_rate" ] ) )
			return "Le champ taux d'escompte doit être un nombre";

	//Toutes les données sont ok, on peut traiter l'enregistrement de la facture
	
	$billing_supplier = $_POST[ "billing_supplier" ];
	$Date = $_POST[ "Datef" ];
	$total_amount_ttc = Util::text2num( $_POST[ "total_amount_ttc" ] );
	$total_charge_ht = Util::text2num( $_POST[ "total_charge_ht" ] );
	$idpayment = $_POST[ "idpayment" ];
	$idpayment_delay = $_POST[ "idpayment_delay"];
	$creation_date = date("Y-m-d");
	$rebate_rate 	= Util::text2num( $_POST[ "rebate_rate" ] );
	$vat_rate = $_POST[ "tva" ];
	
	if( isset($_POST["service"]) && $_POST["service"]== 1 ){
		$service = 1;
	}else{
		$service = 0;
	}
	
	//On créé une nouvelle facture fournisseur
	$supplierinvoice = &SupplierInvoice::create( $billing_supplier , $idsupplier, 0 );
	
	//On calcule la date d'échéance
	$deliv_payment = $_POST[ "deliv_payment" ];
	
	//On affecte les valeurs
	$supplierinvoice->set( "Date" , Util::dateFormatUs($Date) );
	$supplierinvoice->set( "deliv_payment" , Util::dateFormatUs($deliv_payment) );
	$supplierinvoice->set( "idpayment_delay" , $idpayment_delay );
	$supplierinvoice->set( "idpayment" , $idpayment );
	$supplierinvoice->set( "total_amount" , $total_amount_ttc );
	$supplierinvoice->set( "total_charge_ht" , $total_charge_ht );
	$supplierinvoice->set( "creation_date" , $creation_date );
	$supplierinvoice->set( "service" , $service );
	$supplierinvoice->set( "vat_rate", $vat_rate );
	
	
	//On sauvegarde
	$supplierinvoice->save();
			
	$supplierinvoice->set( "rebate_type", "rate" );
	$supplierinvoice->set( "rebate_value", $rebate_rate );
			
	
	//On sauvegarde
	$supplierinvoice->save();
	
	return "0";  
	
}  	

?>