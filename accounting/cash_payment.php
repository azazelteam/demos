﻿<?php

include_once( "../objects/classes.php" );

//------------------------------------------------------------------------------------------------
//export

if (isset ($_GET["export"]) && isset ($_GET["req"])) {

	$req = $_GET["req"];
	$exportableArray = & getExportableArray($req);
	exportArray($exportableArray);

	exit ();

}

//------------------------------------------------------------------------------------------------

$Title = "Paiements comptant à facturer";

//include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$db =& DBUtil::getConnection();


//------------------------------------------------------------------------------------------------

?>
<script type="text/javascript" language="javascript">
	function SortResult(sby,ord){
		document.adm_fact.action="<?php echo $ScriptName ?>?search=1&sortby="+sby+"&sens="+ord;
		document.adm_fact.submit();
	}
</script>

<form action="<?php echo $ScriptName ?>" method="post" name="adm_fact" id="adm_fact">
	<div class="content">
		<?php
		//getEstimateCashPayment(); 
		getOrderCashPayment();
		?>
	</div>
	<input type="hidden" name="search" value="1" />
</form>



<?
	
//--------------------------------------------------------------------------------------------------

//include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//Paiements comptants devis--------------------------------------------------------------------------------

function getEstimateCashPayment (){

	global $GLOBAL_START_URL, $ScriptName, $GLOBAL_DB_PASS, $GLOBAL_START_PATH;
	
	$lang = "_1";
	
	$query = "
	SELECT e.idestimate,
		e.idbuyer,
		e.balance_date,
		p.name$lang as paymentname,
		e.payment_comment,
		e.total_amount,
		e.DateHeure,
		b.company
	FROM estimate e, payment p, buyer b
	WHERE e.cash_payment = 1
	AND e.factor=0
	AND e.paid=1
	AND e.idpayment_delay=1
	AND e.payment_idpayment = p.idpayment
	AND e.idbuyer = b.idbuyer
	AND e.idestimate NOT IN (SELECT idestimate FROM `order`)
	";
	
	if((isset($_GET['cashpaymentorder']))){
		$orderby	= "ORDER BY ".base64_decode( $_GET['cashpaymentorder'] );
	}else{
		$orderby	= "ORDER BY balance_date DESC";
	}
	$query .=$orderby;
	
	$rs=DBUtil::query($query);
	
	if($rs===false)
		die("Impossible de récupérer les paiements comptants des devis");
	
	
	if($rs->recordCount()>0){
	?>
	
	<div class="headTitle">
        <p class="title">Paiements comptants devis à facturer</p>
		<div class="rightContainer"></div>
    </div>
    <div class="subContent">
		<div class="tableHeaderLeft">
			<!--<a href="#" class="goUpOrDown">
				Aller en bas de page
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
			</a>-->
		</div>
		<div class="clear"></div>
		<!-- tableau résultats recherche -->
        <?php 
						
		include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");

		$encryptedQuery = URLFactory::base64url_encode(Encryptor::encrypt($query, $GLOBAL_DB_PASS));
						
		$exportURL = "$GLOBAL_START_URL/accounting/cash_payment.php?export&amp;estimate&amp;req=$encryptedQuery";
		
		?>
            	<div class="subTitleContainer" style="margin:10px 0px;">
					<div class="tableHeaderRight">
						<a href="<?php echo $exportURL ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
					</div>
				</div>
				<!-- Début des boucles -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
                                <th>Devis n°</th>
                                <th>Date de devis</th> 
                                <th>Client n°</th>
                                <th>Raison sociale</th>
                                <th>Date de règlement</th>
                                <th>Mode de règlement</th>
                                <th>Commentaire</th>
                                <th>Montant TTC</th>
                            </tr>
                            <!-- petites flèches de tri -->
                            <tr>
	                            	<th class="noTopBorder">
	                                	<a href="javascript:SortResult( 1,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 1,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 7,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 7,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 2,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 2,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 8,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 8,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 3,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 3,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 4,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 4,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 5,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 5,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult( 6,'ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult( 6,'DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                        	</tr>
                        </thead>
                        <tbody>
                        <?php
						
						$tot=0;
						$total_net_sum = 0.0;
						
						while( !$rs->EOF() ){
							
							$idestimate			= $rs->fields("idestimate");
							$idbuyer 			= $rs->fields("idbuyer");
							$balance_date		= $rs->fields("balance_date");
							$paymentname		= $rs->fields("paymentname");
							$comment 			= $rs->fields("payment_comment");
							$total_amount		= $rs->fields("total_amount");
							$company			= $rs->fields("company");
							$DateHeure			= $rs->fields("DateHeure");
								
							$buyerHREF 		= "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer";
							$estimateHREF 	= "$GLOBAL_START_URL/sales_force/com_admin_devis.php?IdEstimate=$idestimate";
							
							$tot+=$total_amount;
								
							?>
	                             <tr class="orangeText">
	                            	<td class="lefterCol"><a href="<?php echo $estimateHREF; ?>"><?php echo $idestimate ?></a></td>
	                                <td><?php echo usDate2eu( substr($DateHeure,0,10 )) ?></td>
		                            <td><a href="<?php echo $buyerHREF; ?>"><?php echo $idbuyer ?></a></td>
		                            <td><a href="<?php echo $buyerHREF; ?>"><?php echo $company ?></a></td>
	                                <td><?php echo usDate2eu( $balance_date ) ?></td>
	                                <td><?php echo $paymentname ?></td>
	                                <td><?php echo $comment ?></td>
	                                <td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat($total_amount) ?></td>
	                            </tr>
	                  	  	<?
	                    
	                    	$rs->MoveNext();
	                    
	                    }?>
						<tr>
							<td colspan="7" style="border: none;"></td>
							<td class="lefterCol righterCol" style="text-align:right; white-space:nowrap;"><b><?php echo Util::priceFormat($tot) ?></b></td>
						</tr>
                        </tbody>
                    </table>
                </div>
                <!--<a href="#" class="goUpOrDown">
                	Aller en haut de page
                    <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>-->
           	</div>
           	<?php

	}else{			?>
		<div class="headTitle">
        <p class="title">Paiements comptants devis à facturer</p>
		<div class="rightContainer"></div>
    	</div>
		<p style="text-align:center; margin-top:20px;" class="msg">Aucun paiement comptant trouvé</p>
		<?php
     }			

}

//Paiements comptants commande--------------------------------------------------------------------------------

function getOrderCashPayment (){

	global $GLOBAL_START_URL, $ScriptName, $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
	
	$lang = "_1";
	
	$query = "
	SELECT DISTINCT( bl.idorder ),
		o.idbuyer,
		o.balance_date,
		p.name$lang as paymentname,
		o.payment_comment,
		o.total_amount,
		o.DateHeure,
		o.factor,
		b.company
	FROM `order` o, bl_delivery bl, order_supplier os, payment p, buyer b
	WHERE o.cash_payment = 1
	-- AND o.factor=0
	-- AND o.paid=1
	-- AND o.idpayment_delay=1
	AND o.idorder = bl.idorder
	AND o.payment_idpayment = p.idpayment
	AND b.idbuyer = o.idbuyer
	AND bl.idbilling_buyer = 0
	AND bl.idorder_supplier = os.idorder_supplier
	AND os.idorder <> 0
	AND os.internal_supply = 0
	GROUP BY bl.idorder";
	
	if((isset($_GET['cashpaymentorder']))){
		$orderby	= " ORDER BY ".base64_decode( $_GET['cashpaymentorder'] );
	}else{
		$orderby	= " ORDER BY balance_date DESC";
	}
	$query .=$orderby;
	
	$rso =& DBUtil::query($query);
	
	if($rso===false)
		die("Impossible de récupérer les paiements comptants des commandes");
	
	//@todo tempo debug
	$nbFact = 0;
	$bl_total_amount = 0;
	while( !$rso->EOF() ){
		
		if( $rso->fields( "factor" ) )
			trigger_error( "Commande Client n° " . $rso->fields( "idorder" ) . " payée comptant remise au factor", E_USER_ERROR );
		
		$bl_total_amount += $rso->fields("total_amount");
		$nbFact++;
		$rso->MoveNext();
		
	}
	
	$rso->MoveFirst();
	
	if($rso->recordCount()>0){
	?>
	
		
			<!-- tableau résultats recherche -->
	        <?php 
			
			include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");

			$encryptedQuery = URLFactory::base64url_encode(Encryptor::encrypt($query, $GLOBAL_DB_PASS));
						
			$exportURL = "$GLOBAL_START_URL/accounting/cash_payment.php?export&amp;order&amp;req=$encryptedQuery";
			
			
					?>
					
					
					<p><?php echo $nbFact ?> paiement(s) comptant reçu(s) pour un montant total de <strong><?php echo Util::priceFormat($bl_total_amount) ?></strong></p><br/>
	
					
	                <div class="tableContainer clear">
	                	<table class="dataTable devisTable">
	                    	<thead>
	                            <tr>
	                                <th style="border-bottom-style:none; vertical-align:top;">Cde. n°</th>
	                                <th style="border-bottom-style:none; vertical-align:top;">Date de commande</th>
	                                <th style="border-bottom-style:none; vertical-align:top;">Client n°</th>
	                                <th style="border-bottom-style:none; vertical-align:top;">Raison sociale</th>
	                                <th style="border-bottom-style:none; vertical-align:top;">Date de règlement</th>
	                                <th style="border-bottom-style:none; vertical-align:top;">Mode de règlement</th>
	                                <th style="border-bottom-style:none; vertical-align:top;">Montant TTC</th>
								 	<th style="border-bottom-style:none; vertical-align:top;">Commande fournisseur n°</th>
								 	<th style="border-bottom-style:none; vertical-align:top;">Fournisseur</th>
								 	<th style="border-bottom-style:none; vertical-align:top;">BL n°</th>
								 	<th style="border-bottom-style:none; vertical-align:top;">Date d'éxpéd.<br />prévue</th	>
								    <th style="border-bottom-style:none; vertical-align:top;">Date d'éxpéd.<br />Réalisée</th>
								    <th style="border-bottom-style:none; vertical-align:top;">Facturer</th>
								    <th style="border-bottom-style:none; vertical-align:top;">Facturer partiellement</th>
								    <th style="border-bottom-style:none; vertical-align:top;">Port HT</th>
	                            </tr>
	                            <!-- petites flèches de tri -->
								<tr>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.idorder ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.idorder DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.DateHeure ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.DateHeure DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "b.idbuyer ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "b.idbuyer DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "b.company ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "b.company DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.balance_date ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.balance_date DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "paymentname ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "paymentname DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.total_amount ASC" ) ?>"><img src="/images/back_office/content/asc.png" onmouseover="this.src='/images/back_office/content/asc_hover.png';" onmouseout="this.src='/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
										<a href="/accounting/com_admin_search_bl.php?tab=1&amp;cashpaymentorder=<?php echo base64_encode( "o.total_amount DESC" ) ?>"><img src="/images/back_office/content/desc.png" onmouseover="this.src='/images/back_office/content/desc_hover.png';" onmouseout="this.src='/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
									</th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
								</tr>
	                        </thead>
	                        <tbody>
	                        <?php
							
							$tot=0;
							$total_net_sum = 0.0;
							
							while( !$rso->EOF() ){
								
								$idorder			= $rso->fields("idorder");
								$idbuyer 			= $rso->fields("idbuyer");
								$balance_date		= $rso->fields("balance_date");
								$paymentname		= $rso->fields("paymentname");
								$comment 			= $rso->fields("payment_comment");
								$total_amount		= $rso->fields("total_amount");
								$company			= $rso->fields("company");
								$DateHeure			= $rso->fields("DateHeure");
									
								$buyerHREF 		= "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer";
								$orderHREF 		= "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$idorder";
								
								$tot+=$total_amount;
									
								
								//récupérer les BLs et tout le tsouin-tsouin
								$query = "
								SELECT bl.idbl_delivery,
									bl.idorder_supplier, 
									bl.idsupplier,
									bl.DateHeure, 
									bl.idbuyer, 
									bl.idsupplier,
									bl.dispatch_date,
									bl.idbilling_buyer,
									u.initial,
									s.name AS supplierName,
									c.lastname,
									c.firstname,
									c.title AS idtitle,
									b.contact,
									b.company,
									b.siren,
									o.total_amount,
									o.total_charge_ht,
									o.factor,
									o.idbilling_adress,
									os.availability_date
								FROM bl_delivery bl, user u, buyer b, contact c, supplier s, `order` o, order_supplier os
								WHERE o.idorder = '$idorder'
								AND bl.idorder = o.idorder
								AND bl.idorder_supplier = os.idorder_supplier
								AND os.idorder <> 0
								AND os.internal_supply = 0
								AND bl.iduser = u.iduser
								AND bl.idsupplier = s.idsupplier
								AND bl.idbuyer = b.idbuyer
								AND c.idbuyer = bl.idbuyer
								AND c.idcontact = bl.idcontact
								ORDER BY idbilling_buyer ASC"; //Attention : NE PAS MODIFIER LA CLAUSE ORDER BY RISQUE DE VERGLAS
								
								$rs =& DBUtil::query( $query );
								
								if( !$rs->RecordCount() ){
									
									trigger_error( "Des données sont manquantes pour la facturation de la commande n°$idorder", E_USER_ERROR );
									return;
										
								}
								
								$blCount = $rs->RecordCount();		//nombre de BLs
								$invoiceCount = 0;					//nombre de BLs facturés
								$nonInvoicedBls = array();			//liste des BLs non facturés
								$partiallyInvoicedCount = 0;		//nombre de BLs déjà facturés partiellement
								$invoiced_charges = 0.0;			//frais de port déjà facturés
								
								//frais de port restant à facturer
								$order_charges = $rs->fields( "total_charge_ht" );
								$non_invoiced_charges = max( $order_charges - $invoiced_charges, 0.0 );
								
								
								$allowGlobalInvoicing = true;
								
								while( !$rs->EOF() ){
									
									if( $rs->fields( "idbilling_buyer" ) != 0 )
										$invoiceCount++;
									
									if( !$rs->fields( "idbilling_buyer" ) )
										$nonInvoicedBls[] = $rs->fields( "idbl_delivery" );
									
									if( $rs->fields( "idbilling_buyer" ) )
										$partiallyInvoicedCount++;
									
									if( $rs->fields( "idbilling_buyer" ) != 0 )
										$invoiced_charges += DBUtil::getDBValue( "total_charge_ht", "billing_buyer", "idbilling_buyer", $rs->fields( "idbilling_buyer" ) );
							
									//vérifier si on autorise la facturation globale ou non
									//si au moins une date d'expédition n'est pas renseignée, on bloquera la facturation globale		
									if( $rs->fields( "dispatch_date" ) == "0000-00-00" && $allowGlobalInvoicing)
										$allowGlobalInvoicing = false;
										
									$rs->MoveNext();
									
								}
								
								$rs->MoveFirst();
							
							
								//nombre de BLs facturables globalement
								$nonInvoicedCount = $blCount - $partiallyInvoicedCount;
								$globallyInvoiceableCount = $blCount == 1 || $nonInvoicedCount > 1 ? $nonInvoicedCount : 0;
								
								//nombre de BLs facturables partiellement
								$partiallyInvoiceableCount = $nonInvoicedCount > 1 ? $blCount - $partiallyInvoicedCount : 0;
								
								//afficher les données
								$rowspanStr 		= $blCount > 1 ? " rowspan=\"$blCount\"" : "";
								
								$init 				= $rs->fields( "initial" );
								$isContact 			= $rs->fields( "contact" ) == 1;
								$total_amount 		= $rs->fields( "total_amount" );
								$total_charge_ht 	= $rs->fields( "total_charge_ht" );
								$factor 			= $rs->fields( "factor" );
								$contact 			= $rs->fields( "contact" );
								$company 			= $rs->fields( "company" );
								$idtitle 			= $rs->fields( "idtitle" );
								$title 				= empty( $idtitle ) ? "" : DBUtil::getDBValue( "title$lang", "title", "idtitle", $idtitle );
								$lastname 			= $rs->fields( "lastname" );
								$firstname 			= $rs->fields( "firstname" );
								$idbuyer 			= $rs->fields( "idbuyer" );
								$siren 				= $rs->fields( "siren" );
								$buyerName 			= strlen( $company ) ? $company : "$title $lastname";
								
								$orderHREF 			= "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$idorder";
								$buyerHREF 			= "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer&amp;type=";
								$buyerHREF 			.= $isContact ? "contact" : "buyer";
								
								$cashPayment		= DBUtil::getDBValue( "balance_date", "order", "idorder", $idorder ) == "0000-00-00" ? false : true;
								
								$chargesDispatched 	= false;
								
								//Pays de facturation
								
								if( $rs->fields( "idbilling_adress" ) == 0 ){
									$billingCountry = DBUtil::query( "SELECT name$lang AS country 
																	  FROM buyer b, state s 
																	  WHERE b.idbuyer = '$idbuyer' 
																	  AND b.idstate = s.idstate" )->fields( "country" );
								}else{
									$billingCountry = DBUtil::query( "SELECT name$lang AS country 
																	  FROM billing_adress b, state s 
																	  WHERE b.idbilling_adress = " . $rs->fields( "idbilling_adress" ) . " 
																	  AND b.idstate = s.idstate" )->fields( "country" );
								}
										
								?>
		                             <tr>
		                            	<td<?php echo $rowspanStr ?> class="lefterCol"><a href="<?php echo $orderHREF; ?>"><?php echo $idorder ?></a></td>
		                                <td<?php echo $rowspanStr ?> ><?php echo usDate2eu( substr($DateHeure,0,10 )) ?></td>
		                                <td<?php echo $rowspanStr ?> ><a href="<?php echo $buyerHREF; ?>"><?php echo $idbuyer ?></a></td>
		                                <td<?php echo $rowspanStr ?> ><a href="<?php echo $buyerHREF; ?>"><?php echo $buyerName ?></a></td>
		                                <td<?php echo $rowspanStr ?> ><?php echo usDate2eu( $balance_date ) ?></td>
		                                <td<?php echo $rowspanStr ?> ><?php echo $paymentname ?></td>
		                                <!--<td<?php echo $rowspanStr ?> ><?php echo $comment ?></td>-->
		                                <td<?php echo $rowspanStr ?> style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat($total_amount) ?></td>
		                                <!--
		                                <td<?php echo $rowspanStr ?>>
										<?php 
											
											if( $rs->fields( "factor" ) ){
												
												?>
												<a href="#" title="Cliquez pour modifier" onclick="updateFactor( <?php echo $idorder; ?>, this.parentNode ); return false;" style="color:#FF0000; font-weight:bold;">Oui</a>
												<?php
												
											}
											else echo "Non"; 
											
										?>
										</td>
										-->
										<?
										$p = 0;
										while( !$rs->EOF() ){
											
											$idbl_delivery 		= $rs->fields( "idbl_delivery" );
											$idorder_supplier 	= $rs->fields( "idorder_supplier" );
											$idsupplier 		= $rs->fields( "idsupplier" );
											$availability_date 	= $rs->fields( "availability_date" );
											$idsupplier 		= $rs->fields( "idsupplier" );
											$dispatch_date 		= $rs->fields( "dispatch_date" ) == "0000-00-00" ? "" : $rs->fields( "dispatch_date" );
											$supplierName 		= $rs->fields( "supplierName" );
											$idbilling_buyer	= $rs->fields( "idbilling_buyer" );
											$blHREF 			= "$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl_delivery";
											
											$allowPartialInvoicing = $blCount > 1 && $dispatch_date != "";
											
											if( $p )
												echo "<tr>";
												
											?>											
											<td>
												<a href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier; ?>" class="lien" onclick="window.open(this.href); return false;"><?php echo $idorder_supplier; ?></a>
											</td>
									      	<td><?php echo $supplierName ?></td>
											<td><a href="<?php echo $blHREF ?>" onclick="window.open(this.href); return false;"><?php echo $idbl_delivery ?></a></td>	
											<td><?php echo $availability_date != "0000-00-00" ? usDate2eu( $availability_date ) : "-" ?></td>
											<td><?php echo usDate2eu( $dispatch_date )?></td>
											<?php
											
												//BLs facturable globalement
												
												if( !$p && $globallyInvoiceableCount ){
													
													$rowspanStr = $globallyInvoiceableCount > 1 ? " rowspan=\"$globallyInvoiceableCount\"" : "";
													$disabled 	= $allowGlobalInvoicing ? "" : " disabled=\"disabled\"";
													
													if( $allowGlobalInvoicing && $partiallyInvoiceableCount ){
														
														$onclick = "if( this.checked ) uncheck( new Array( 'idbl_delivery_" . implode( "', 'idbl_delivery_", $nonInvoicedBls ) . "' ) );";
														
														foreach( $nonInvoicedBls as $nonInvoicedBl )
															$onclick .= " displayChargeEditor( $idorder, $nonInvoicedBl, false );";
														
														$onclick .= " checkChargeSum( $idorder );";
														
													}
													else $onclick = "";
													
													?>
													<td class="<?php echo empty( $rowspanStr ) ? "" : " lefterCol righterCol" ?>" <?php echo $rowspanStr ?> >
														<input type="checkbox"<?php echo $disabled ?> name="idorder[]" id="idorder_<?php echo $idorder ?>" value="<?php echo $idorder ?>" onclick="<?php echo $onclick ?>" />
													</td>
													<?php
											
												}
												else if( $p > $globallyInvoiceableCount - 1 ){ //le BL n'est facturable que partiellement ( cad que c'est le dernier BL à ne pas avoir été facturé partiellement )
													
													?>
													<td></td>
													<?php
													
												}
												
												//BLs facturable partiellement
												
												?>
												<td>
												<?php
										
													if( !$idbilling_buyer && $blCount > 1 ){
														
														$disabled 	= $allowPartialInvoicing ? "" : " disabled=\"disabled\"";
														
														if( $allowPartialInvoicing && $globallyInvoiceableCount ){
															
															$onclick = "if( this.checked ){ uncheck( new Array( 'idorder_$idorder' ) );";
															
															if( $total_charge_ht > 0.0 )
																$onclick .= " displayChargeEditor( $idorder, $idbl_delivery, true );";
															
															$onclick .= " }";
															
															if( $total_charge_ht > 0.0 )
																$onclick .=" else displayChargeEditor( $idorder, $idbl_delivery, false ); checkChargeSum( $idorder );";
														
														}
														else $onclick = "";
														
														?>
														<input type="checkbox"<?php echo $disabled ?> name="idbl_delivery[]" id="idbl_delivery_<?php echo $idbl_delivery ?>" value="<?php echo $idbl_delivery ?>" onclick="<?php echo $onclick ?>" />
														<input type="hidden" id="order_delivery_notes_<?php echo $idorder ?>" value="<?php echo $idbl_delivery ?>" />
														<?php
														
													}
													else if( $blCount > 1 ){ //le BL est déjà facturé partiellement, on affiche une case cochée pour info
														
														?>
														<input type="checkbox" disabled="disabled" checked="checked" onclick="return false;" />
														<?php
														
													}
													
												?>
												</td>
												
												<td id="ChargeCell_<?php echo $idbl_delivery ?>" class="righterCol" style="text-align:right; white-space:nowrap;">
												<?php
												
													//si aucun frais de port à facturifier
										
													if( $total_charge_ht == 0.0 ){ //s'il n'y a pas de frais de port à facturifier
											
														echo Dictionnary::translate( "gest_com_franco" );
														
														?>
														<input type="hidden" name="total_charge_ht_<?php echo $idbl_delivery ?>" value="0.0" />
														<?php
														
													}
													else if( !$idbilling_buyer && $blCount > 1 && ( $invoiceCount == $blCount - 1 ) ){
														
														//on impose les frais de port dans le cas où il y a plusieurs BLs et que tous ont été facturés sauf celui-ci
									
														echo Util::priceFormat( $non_invoiced_charges );
														
														?>
														<input type="hidden" name="total_charge_ht_<?php echo $idbl_delivery ?>" value="<?php echo round( $non_invoiced_charges, 2 ) ?>" />
														<?php
														
													}
													else if( $idbilling_buyer ){ //si le BL est déjà facturé => on affiche les frais de port facturés
														
														$invoiced_charges = DBUtil::getDBValue( "total_charge_ht", "billing_buyer", "idbilling_buyer", $idbilling_buyer );
														
														echo Util::priceFormat( $invoiced_charges );
														
														?>
														<input type="hidden" name="total_charge_ht_<?php echo $idbl_delivery ?>" value="<?php echo round( $non_invoiced_charges, 2 ) ?>" />
														<?php
														
													}
													else if( $blCount == 1 ){ //s'il n'y a qu'un seul BL, il récupèrera tous les frais de port de la commande
														
														echo $total_charge_ht > 0.0 ? Util::priceFormat( $total_charge_ht ) : Dictionnary::translate( "gest_com_franco" );
														
														?>
														<input type="hidden" name="total_charge_ht_<?php echo $idbl_delivery ?>" value="<?php echo round( $total_charge_ht, 2 ) ?>" />
														<?php
														
													}
													//sinon cette cellule reste vide pour la création dynamique d'elements INPUT avec javascript
													
												?>
												</td>
											</tr>
											<?php
												
											$rs->MoveNext();
											$p++;
											
										}
		                                
		                                ?>
		                                
		                                <!--
		                                <td  <?php echo $rowspanStr ?> class="righterCol"><input type="checkbox" id="orders2billing[]" name="orders2billing"></td>
		                            	-->
		                            
		                  	  	<?
		                    
		                    	$rso->MoveNext();
		                    
		                    }?>
							<tr>
								<td colspan="6" style="border: none;"></td>
								<td class="lefterCol righterCol" style="text-align:right; white-space:nowrap;"><b><?php echo Util::priceFormat($tot) ?></b></td>
								<td colspan="8" style="border: none;"></td>
							</tr>
	                        </tbody>
	                    </table>
	                </div>
					<div class="submitButtonContainer" style="margin-top:15px;">
						<input type="submit" name="InvoiceButton" value="Facturer" onclick="return confirm('Générer les factures pour les BL sélectionnés ?');" class="blueButton floatright" />
					</div>
           	<?php
	}else{
       ?>
       
        <h1 class="titleSearch">
		<span class="textTitle">Paiements comptants commandes à facturer</span>
		<div class="spacer"></div>
		</h1>
		
        <div class="blocResult"><div style="margin:5px;">
			<p style="text-align:center; margin-top:20px;" class="msg">Aucun paiement comptant trouvé</p>
		</div></div>
		<?php
    }			

}

//--------------------------------------------------------------------------------

function & getExportableArray($req) {

	global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;

	include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");

	$SQL_Query = Encryptor::decrypt(URLFactory::base64url_decode($_GET["req"]), $GLOBAL_DB_PASS);
	
	if(isset($_GET["estimate"])){
	
		$data = array (
		
			"Devis n°" => array (),
			"Client n°" => array (),
			"Date de paiement" => array (),
			"Mode de paiement" => array (),
			"Commentaire" => array (),
			"Montant" => array (),
			
		);
	
	}elseif(isset($_GET["order"])){
		
		$data = array (
		
			"Commande n°" => array (),
			"Date de commande" => array (),
			"Client n°" => array (),
			"Raison sociale" => array (),
			"Date de paiement" => array (),
			"Mode de paiement" => array (),
			"Commentaire" => array (),
			"Montant" => array (),
			
		);
		
	}

	$rs = & DBUtil::query($SQL_Query);

	while (!$rs->EOF()) {
		
		if(isset($_GET["estimate"])){
			
			$data["Devis n°"][] = $rs->fields("idestimate");
		
		}elseif(isset($_GET["order"])){
			
			$data["Commande n°"][] = $rs->fields("idorder");
		
		}
		
		$data["Date de commande"][] = usDate2eu( substr($rs->fields("DateHeure"),0,10 ));							
		$data["Client n°"][] = $rs->fields("idbuyer");
		$data["Raison sociale"][] = $rs->fields("company");
		$data["Date de paiement"][] = $rs->fields("balance_date");
		$data["Mode de paiement"][] = $rs->fields("paymentname");
		$data["Commentaire"][] = $rs->fields("payment_comment");
		$data["Montant"][] = str_replace(".",",",$rs->fields("total_amount"));
	
		$rs->MoveNext();

	}

	return $data;

}

//--------------------------------------------------------------------------------

function exportArray(& $exportableArray) {

	global $GLOBAL_START_PATH;

	include_once ("$GLOBAL_START_PATH/objects/CSVExportArray.php");

	$cvsExportArray = new CSVExportArray($exportableArray, "Paiements_comptant");
	$cvsExportArray->export();

}
?>
