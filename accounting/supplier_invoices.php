<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Factures fournisseurs
 */
 


include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

if( !User::getInstance()->getId() )
	exit();

//----------------------------------------------------------------------------
/* logo fournisseur */

if( isset( $_GET[ "logo" ] ) && isset( $_GET[ "idsupplier" ] ) ){

	createSupplierLogo(  $_GET[ "idsupplier" ]  );
	exit();
		
}

//----------------------------------------------------------------------------
/* facturation partielle */

if( isset( $_REQUEST[ "delivery_notes" ] ) && isset( $_REQUEST[ "idsupplier" ] ) ){
	
	$deliveryNotes = explode( ",", $_REQUEST[ "delivery_notes" ] );
	
	$pattern = "F" . $_REQUEST[ "idsupplier" ] . "-" . date( "d-m" );
	
	$exists = true;
	$i = 1;
	while( $exists ){
		
		$billing_supplier = "$pattern-" . sprintf( "%2d", $i );
		$rs =& DBUtil::query( "SELECT billing_supplier FROM billing_supplier WHERE billing_supplier = '" . Util::html_escape( $billing_supplier ) . "' AND idsupplier = '" .  $_REQUEST[ "idsupplier" ]  . "' LIMIT 1" );
		
		$exists = $rs->RecordCount() != 0;
		$i++;
		
	}
	
	$supplierInvoice = SupplierInvoice::create( $billing_supplier,  $_REQUEST[ "idsupplier" ] , $deliveryNotes );
	
	$supplierInvoice->set( "editable", 1 );
	$supplierInvoice->set( "Date", date( "Y-m-d" ) );
	$supplierInvoice->set( "deliv_payment", date( "Y-m-d" ) );
	
	$supplierInvoice->save();
	
	$supplierInvoice->duplicate();
	
	header( "Location: $GLOBAL_START_URL/accounting/supplier_invoices.php?billing_supplier=" . urlencode( $billing_supplier ) . "&idsupplier=" . $_REQUEST[ "idsupplier" ] );
	
	exit();
	
}

//----------------------------------------------------------------------------
/* vérouillage de la facture fournisseur */

if( isset( $_REQUEST[ "lock" ] ) && isset( $_GET[ "billing_supplier" ] ) && isset( $_GET[ "idsupplier" ] ) ){


	$regs = array();
	
	if( preg_match( "/^([0-9]{2}).([0-9]{2}).([0-9]{4})\$/i", $_POST[ "Date" ], $regs ) === false )
		exit( "Le format de la date de facture est incorrect" );
		
	$Date = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
	
	if( preg_match( "/^([0-9]{2}).([0-9]{2}).([0-9]{4})\$/i", $_POST[ "deliv_payment" ], $regs ) === false )
		exit( "Le format de la date d'échéance est incorrect" );
		
	$deliv_payment = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
	
	$supplierInvoice = new SupplierInvoice( $_GET[ "billing_supplier" ], $_REQUEST[ "idsupplier" ] );
	
	$supplierInvoice->set( "comment", 			stripslashes( $_POST[ "comment" ] ) );
	$supplierInvoice->set( "Date", 				$Date );
	$supplierInvoice->set( "deliv_payment", 	$deliv_payment );
	$supplierInvoice->set( "idpayment", 		intval( $_POST[ "idpayment" ] ) );
	$supplierInvoice->set( "idpayment_delay", 	intval( $_POST[ "idpayment_delay" ] ) );
	$supplierInvoice->set( "total_amount_ht", 	Util::text2num( $_POST[ "total_amount_ht" ] ) + Util::text2num( $_POST[ "total_charge_ht" ] ) );
	$supplierInvoice->set( "total_charge_ht", 	Util::text2num( $_POST[ "total_charge_ht" ] ) );
	$supplierInvoice->set( "total_amount", 		Util::text2num( $_POST[ "total_amount" ] ) );
	$supplierInvoice->set( "sundry_expenses", 	Util::text2num( $_POST[ "sundry_expenses" ] ) );
	$supplierInvoice->set( "extra_taxes", 		Util::text2num( $_POST[ "extra_taxes" ] ) );
	
	$supplierInvoice->save();


	DBUtil::query( "UPDATE billing_supplier SET editable = 0 WHERE billing_supplier = '" . Util::html_escape( $_REQUEST[ "billing_supplier" ] ) . "' AND idsupplier = '" .  $_REQUEST[ "idsupplier" ]  . "' LIMIT 1" );
	
	exit( "1" );
	
}

//----------------------------------------------------------------------------
/* mise à jour de la facture fournisseur */

if( isset( $_GET[ "update" ] ) && isset( $_GET[ "billing_supplier" ] ) && isset( $_GET[ "idsupplier" ] ) ){

	$regs = array();
	
	if( preg_match( "/^([0-9]{2}).([0-9]{2}).([0-9]{4})\$/i", $_POST[ "Date" ], $regs ) === false )
		exit( "Le format de la date de facture est incorrect" );
		
	$Date = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
	
	if( preg_match( "/^([0-9]{2}).([0-9]{2}).([0-9]{4})\$/i", $_POST[ "deliv_payment" ], $regs ) === false )
		exit( "Le format de la date d'échéance est incorrect" );
		
	$deliv_payment = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
	
	$supplierInvoice = new SupplierInvoice( $_GET[ "billing_supplier" ], $_REQUEST[ "idsupplier" ] );
	
	$supplierInvoice->set( "comment", 			stripslashes( $_POST[ "comment" ] ) );
	$supplierInvoice->set( "Date", 				$Date );
	$supplierInvoice->set( "deliv_payment", 	$deliv_payment );
	$supplierInvoice->set( "idpayment", 		intval( $_POST[ "idpayment" ] ) );
	$supplierInvoice->set( "idpayment_delay", 	intval( $_POST[ "idpayment_delay" ] ) );
	$supplierInvoice->set( "total_amount_ht", 	Util::text2num( $_POST[ "total_amount_ht" ] ) + Util::text2num( $_POST[ "total_charge_ht" ] ) );
	$supplierInvoice->set( "total_charge_ht", 	Util::text2num( $_POST[ "total_charge_ht" ] ) );
	$supplierInvoice->set( "total_amount", 		Util::text2num( $_POST[ "total_amount" ] ) );
	$supplierInvoice->set( "sundry_expenses", 	Util::text2num( $_POST[ "sundry_expenses" ] ) );
	$supplierInvoice->set( "extra_taxes", 		Util::text2num( $_POST[ "extra_taxes" ] ) );
	
	$supplierInvoice->save();
	
	/* renommage de la facture */
	
	if( isset( $_POST[ "billing_supplier" ] ) 
		&& $_POST[ "billing_supplier" ] != $_GET[ "billing_supplier" ] 
		&& strlen( $_POST[ "billing_supplier" ] )
		&& $supplierInvoice->renameTo( stripslashes( $_POST[ "billing_supplier" ] ) ) )
			exit( "SUPPLIER_INVOICE_ID_CHANGED" );

	exit( "1" );
	
}

//----------------------------------------------------------------------------
/* sauvegarde d'un transfert */

if( isset( $_REQUEST[ "transfert" ] ) ){

	$source = new SupplierInvoice( $_REQUEST[ "source" ], 	$_REQUEST[ "idsupplier" ] );
	$target = new SupplierInvoice( $_REQUEST[ "target" ], 	$_REQUEST[ "idsupplier" ] );
		
	$i = 0;
	$done = false;
	while( !$done && $i < $source->getItems()->size() ){
		
		if( $source->getItems()->get( $i )->get( "idrow" ) == intval( $_REQUEST[ "idrow" ] ) 
			&& $source->getItems()->get( $i )->get( "idbl_delivery" ) ==  $_REQUEST[ "idbl_delivery" ]  ){
				
			$clone = clone $source->getItems()->get( $i );
			$clone->set( "quantity", intval( $_REQUEST[ "quantity" ] ) );	
			
			$target->addItem( $clone );
			
			$source->getItems()->get( $i )->set( "quantity", max( 0, $source->getItems()->get( $i )->get( "quantity" ) - intval( $_REQUEST[ "quantity" ] ) ) );
			
			$done = true;
			
		}

		$i++;
		
	}

	$target->save();
	$source->save();
	
	exit( "1" );

}

//----------------------------------------------------------------------------
/* création d'une nouvelle facture */

if( isset( $_REQUEST[ "create" ] ) && isset( $_REQUEST[ "billing_supplier" ] ) && isset( $_REQUEST[ "idsupplier" ] ) ){
	

	$supplierInvoice = new SupplierInvoice( $_REQUEST[ "billing_supplier" ], $_REQUEST[ "idsupplier" ] );
	$supplierInvoice->duplicate();
	
	//@todo

	header( "Location: $GLOBAL_START_URL/accounting/supplier_invoices.php?billing_supplier=" . urlencode( $_REQUEST[ "billing_supplier" ] ) . "&idsupplier=" . $_REQUEST[ "idsupplier" ] );

	exit();
	
}

//----------------------------------------------------------------------------
/* suppression d'une facture fournisseur : transfert du contenu dans la facture éditée */

if( isset( $_REQUEST[ "dropped" ] ) &&  isset( $_REQUEST[ "idsupplier" ] ) && isset( $_REQUEST[ "billing_supplier" ] ) ){

	$droppedSupplierInvoice = new SupplierInvoice( $_REQUEST[ "dropped" ], intval( $_REQUEST[ "idsupplier" ] ) );
	$supplierInvoice 		= new SupplierInvoice( $_REQUEST[ "billing_supplier" ], intval( $_REQUEST[ "idsupplier" ] ) );
	
	$i = 0;
	while( $i < $droppedSupplierInvoice->getItems()->size() ){
	
		$supplierInvoice->addItem( $droppedSupplierInvoice->getItems()->get( $i ) );
			
		$i++;
		
	}

	$supplierInvoice->save();
	
	SupplierInvoice::delete( $droppedSupplierInvoice->get( "billing_supplier" ), $droppedSupplierInvoice->get( "idsupplier" ) );
	
	header( "Location: $GLOBAL_START_URL/accounting/supplier_invoices.php?billing_supplier=" . urlencode( $_REQUEST[ "billing_supplier" ] ) . "&idsupplier=" . $_REQUEST[ "idsupplier" ] );

	exit();

}

//----------------------------------------------------------------------------

if( !count( getEditableSupplierInvoices() ) ){
	
	header( "Location: $GLOBAL_START_URL/accounting/received_supplier_invoices.php" );
	exit();
		
}

//----------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<style type="text/css">
	
	div#global div#globalMainContent{width: 1100px; margin-left: auto; margin-right: auto; }
	div#global div#globalMainContent div.mainContent{ width: auto; }
	
	/* supplier invoice */
	
	div.supplierInvoice{
		border:1px solid #44474E;
		padding-right:2px;
		width:500px;
	}
	
	div.header{
		width:500px; 
		border:1px solid #E7E7E7;
		background-color:#F7F7F7;
		padding:5px 0px;
		height:auto;
		overflow:hidden;
	}
	
	div.header input.supplierInvoiceID{
		background-color:#FFFFFF;
		color:#333333;
		border:1px solid #E7E7E7; 
		margin-right:4px;
		text-align:right;
		width:140px;
		font-size:10px;
		font-weight:bold;
	}
	
	div.header img.logo{
		margin:0px 4px;
		z-index:0;
	}
	
	div.header div.supplierAddress{
		margin: 5px 10px;
		font-style:italic;
	}
	
	div.body{
		width:500px; 
		height:auto; 
		border:1px solid #E7E7E7;
		background-color:#F7F7F7;
		margin:0px 0px 5px 0px;
		padding:0px;
	}
	
	div.itemsFooter{
		margin:0px;
		padding:0px;
		width:500px;
	}
	
	div.body div.itemHeaders{
		margin:4px;	
	}
	
	div.body div.itemHeader{
		 float:left; 
		 text-align:center; 
		 font-weight:bold;
		 margin-left:1px;
		 padding:0px;
		 background-color:#84878E;
		 color:#FFFFFF;
	}
	
	div.amountsHeaders{
		margin:4px;	
	}
	
	div.amountsHeaders div.amountHeader{
		float:left; 
		 text-align:center; 
		 font-weight:bold;
		 margin-left:1px;
		 padding:0px;
		 background-color:#84878E;
		 color:#FFFFFF;
	}
	
	div.amountsValues{
		margin:4px;	
		background-color:#FFFFFF;
		border:1px solid #E7E7E7;
	}
	
	div.amountsValues div.amountValue{
		float:left; 
		text-align:center; 
		margin-left:1px;
		padding:4px 0px;
		margin:0px;
	}
	
	div.amountValue input.nonEditableAmount{
		background-color:transparent; 
		border-style:none;
		margin-right:4px;
		text-align:right;
		width:65px;
		font-size:10px;
	}
	
	div.amountValue input.editableAmount{
		background-color:transparent; 
		border:1px solid #E7E7E7; 
		margin-right:4px;
		text-align:right;
		width:65px;
		font-size:10px;
	}
	
	div.comment{
		width:492px;
		height:auto; 
		background-color:#FFFFFF;
		margin:4px;
		padding:0px;
	}
	
	div.comment textarea{
		width:492px; 
		margin:0px;
		height:75px;
		border:1px dashed #848484;
	}
	
	input.date{
		background-color:transparent; 
		border:1px solid #E7E7E7; 
		margin-right:4px;
		width:65px;
		font-size:10px;
	}
	
	/* sortables */
	
	ul.items{
		list-style-type:none;
		width:492px;
		margin:4px;
		padding:0px;
	}
	
	ul.items li.item{
		margin-top:4px;
		padding:0px;
		border:1px solid #D2D2D2;
		background-color:#FFFFFF;
	}
		
	ul.items li.item div.itemValue{
		float:left; 
		margin:4px;
		padding:0px;
	}
	
	div.numeric{
		text-align:right;
	}
	
	ul.items li.item input.quantity{
		background-color:transparent;
		border-style:none;
		width:30px;
		margin:0px;
		padding:0px;
	}
	
	ul.items li.item:hover{
		background-color:#F7F7F7;
		cursor:move;
	}
	
	/* QuantityDialog */
	
	#QuantityDialog .quantity{
		background-color:transparent;
		border-style:groove;
		width:50px;
		font-size:32px;
		font-weight:bold;
		text-align:center;
		border-style:none;
		color:#B4B4B4;
		vertical-align:middle;
	}
	
	#QuantityDialog #transferThumb{
		border:1px solid #D2D2D2;	
	}
	
	/* tabs */
	
	.ui-tabs .ui-tabs-nav li{
		/*width:90px;*/
	}
	
</style>
<script type="text/javascript">
/* <![CDATA[ */

	var editableSupplierInvoices = new Array( <?php echo "\"" . implode( "\",\"", getEditableSupplierInvoices() ) . "\""; ?> );
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function getSupplierInvoiceID( billing_supplier ){
	
		for( var i = 0; i < editableSupplierInvoices.length; i++ )
			if( editableSupplierInvoices[ i ] == billing_supplier )
				return new String( i );
				
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	$(document).ready(function(){

		/* sortables */
	
		$(".item").draggable({
			revert: true
		});
		
		/* droppables */
		
		$(".items").droppable({
		
			accept: ".item",
			tolerance: 'touch',
			drop: function(event, ui) {

				if( $( this ).attr( "id" ) == ui.helper.parent().attr( "id" ) )
					return;
					
				var quantity = ui.helper.find( ".quantity" ).val();
				
				ui.helper.fadeOut(700);
				
				//if( quantity > 1 )
				confirmTransfer( $( this ), ui.helper );
				
			}
			
		}).disableSelection();
		
		/* slider */
		
		$( "#quantitySlider" ).slider({
	
			value:0,
			min: 0,
			max: 0,
			step: 1,
			slide: function(event, ui) {
			
				$( "#sourceQuantity" ).val( $( this ).slider( 'option', 'max' ) - ui.value );
				$( "#targetQuantity" ).val( ui.value );
				
			}
				
		});

	});

	/* --------------------------------------------------------------------------------------------------------------- */
	
	function createSupplierInvoice(){
	
		document.location = '<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?create&billing_supplier=<?php echo urlencode( $_REQUEST[ "billing_supplier" ] ); ?>&idsupplier=<?php echo $_REQUEST[ "idsupplier" ]; ?>';

	}

	/* --------------------------------------------------------------------------------------------------------------- */
	
	function dropSupplierInvoice( billing_supplier ){

		$( "#SupplierInvoicePanel" + getSupplierInvoiceID( billing_supplier ) ).animate( { opacity: 0 }, 250 ).css( "display", "none" );
		document.location = '<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=<?php echo urlencode( $_REQUEST[ "billing_supplier" ] ); ?>&idsupplier=<?php echo $_REQUEST[ "idsupplier" ]; ?>&dropped=' + escape( billing_supplier );
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function confirmTransfer( $target, $helper ){

		var reverse 			= $target.attr( "id" ) == "items" + "<?php echo getSupplierInvoiceID( $_REQUEST[ "billing_supplier" ] ); ?>";

		var helperQuantity 		= parseInt( $helper.find( ".quantity" ).val() );
		var targetQuantity 		= parseInt( getTransferTarget( $target, $helper ).find( ".quantity" ).val() );
		var availableQuantity 	= new String( helperQuantity + targetQuantity );
		
		var $source = $helper.parent();
		
		$( "#quantitySlider" ).slider( "option", "max", availableQuantity );
		$( "#quantitySlider" ).slider( "option", "value", reverse ? 0 : availableQuantity );
		
		$( "#sourceQuantity" ).val( reverse ? availableQuantity : 0 );
		$( "#targetQuantity" ).val( reverse ? 0 : availableQuantity );
		$( "#transferThumb" ).attr( "src", $helper.find( ".referenceThumb" ).attr( "src" ) );
			
		$("#QuantityDialog").dialog({
	        
	        modal: true,	
			title: "Quantité",
			width: 300,
			close: function(event, ui) { 
			
				$("#QuantityDialog").dialog( 'destroy' );
				$helper.fadeIn();
				
			}
		
		});
		
		document.getElementById( "CancelButton" ).onclick = function( event ){
		
			$( '#QuantityDialog' ).dialog( 'destroy' );
			$helper.fadeIn(700);
			
		}
		
		document.getElementById( "ValidateButton" ).onclick = function( event ){
		
			$( '#QuantityDialog' ).dialog( 'destroy' );
			
			var quantity = parseInt( reverse ? $( "#sourceQuantity" ).val() : $( "#targetQuantity" ).val() );
			
			if( quantity > 0 )
				transfer( $helper, $target, quantity );
			else $helper.fadeIn(700);
	
		}
		
	}

	/* --------------------------------------------------------------------------------------------------------------- */

	function transfer( $helper, $target, quantity ){
	
		var reverse = $target.attr( "id" ) == "items" + "<?php echo getSupplierInvoiceID( $_REQUEST[ "billing_supplier" ] ); ?>";

		var availableQuantity 	= parseInt( $helper.find( ".quantity" ).val() );
		var targetQuantity 		= parseInt( $( "#targetQuantity" ).val() );
		var sourceQuantity 		= parseInt( $( "#sourceQuantity" ).val() );

		var $item = getTransferTarget( $target, $helper );
		
		$item.find( ".quantity" ).val( reverse ? sourceQuantity : targetQuantity );

		$item.appendTo( $target );

		$item.css( "left", "0px" );
		$item.css( "top", "0px" );
		$item.parent().css( "height", "auto" );
		
		$item.fadeIn( 700 );

		$item.draggable({
			revert: true
		});
		
		saveTransfer( $helper.parent(), $target, $helper.find( ".idbl_delivery" ).val(), $helper.attr( "id" ), reverse ? availableQuantity - targetQuantity : availableQuantity - sourceQuantity );
		
		if( ( !reverse && sourceQuantity > 0 ) || ( reverse && targetQuantity > 0 ) ){ /* transfert partiel */
			
			$helper.find( ".quantity" ).val( reverse ? targetQuantity : sourceQuantity );
			$helper.fadeIn(700);
			
		}
		else{ /* transfert total */

			$helper.parent().css( "height", $helper.parent().find( "li" ).size() > 1 ? "auto" : "70px" );
			$helper.remove();
		
		}

		updateTotalET( editableSupplierInvoices[ $helper.parent().attr( "id" ).replace( new RegExp( "(items)", "g" ), "" ) ] );
		updateTotalET( editableSupplierInvoices[ $target.attr( "id" ).replace( new RegExp( "(items)", "g" ), "" ) ] );
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function saveTransfer( $source, $target, idbl_delivery, idrow, quantity ){

		var reg 	= new RegExp( "(items)", "g" );
		
		var sourceID = quantity > 0 ? editableSupplierInvoices[ $source.attr( "id" ).replace( reg, "" ) ] : editableSupplierInvoices[ $target.attr( "id" ).replace( reg, "" ) ];
		var targetID = quantity > 0 ? editableSupplierInvoices[ $target.attr( "id" ).replace( reg, "" ) ] : editableSupplierInvoices[ $source.attr( "id" ).replace( reg, "" ) ];
		
		var data 	= "idrow=" + idrow;
		data 	+= "&idbl_delivery=" + idbl_delivery;
		data 	+= "&source=" + sourceID;
		data 	+= "&target=" + targetID;
		data 	+= "&quantity=" + Math.abs( quantity );
		
		$.ajax({
			 	
			url: "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?transfert&idsupplier=<?php echo intval( $_REQUEST[ "idsupplier" ] ); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ 
			
				alert( "Impossible de sauvegarder les modifications" ); 
				resetEditor(); 
				
			},
		 	success: function( responseText ){

				if( responseText != "1" ){
					
					alert( "Impossible de sauvegarder les modifications : " + responseText ); 
					resetEditor();
					return;
					
				}
				
				/*$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );*/
        		
        		updateTotalET( sourceID );
        		updateTotalET( targetID );
        		
        		save( sourceID );
        		save( targetID );
        				
			}

		});
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	/* creates target or returns available target */
	
	function getTransferTarget( $target, $helper ){

		$target = $target.find( "#" + $helper.attr( "id" ) );

		if( $target.size() && $target.find( ".idbl_delivery" ).val() == $helper.find( ".idbl_delivery" ).val() )
			return $target;
		
		var $clone = $helper.clone();
		
		$clone.find( ".quantity" ).val( 0 );

		return $clone;
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function transferQuantity( quantity ){
	
		if( quantity > 0 && parseInt( $( "#sourceQuantity" ).val() ) < quantity )
			return;
		
		if( quantity < 0 && parseInt( $( "#targetQuantity" ).val() ) < Math.abs( quantity ) )
			return;
		
		$( "#sourceQuantity" ).val( parseInt( $( "#sourceQuantity" ).val() ) - quantity );
		$( "#targetQuantity" ).val( parseInt( $( "#targetQuantity" ).val() ) + quantity );
		$( "#quantitySlider" ).slider( "value", parseInt( $( "#targetQuantity" ).val() ) );
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function resetEditor(){

		document.location = '<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=<?php echo urlencode( $_REQUEST[ "billing_supplier" ] ); ?>&idsupplier=<?php echo $_REQUEST[ "idsupplier" ]; ?>';
				
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function displayComments( billing_supplier ){
	
		var $comment = $( "#CommentDiv" + getSupplierInvoiceID( billing_supplier ) ); 
		var $saveButton = $( "#SaveComment" + getSupplierInvoiceID( billing_supplier ) );
		
		if( $comment.css( "display" ) == "block" ){ 
		
			$comment.fadeOut( 700 ); 
			$saveButton.fadeOut( 700 );
			
		}else{
		
			$comment.fadeIn(700); 
			$saveButton.fadeIn(700); 
		
		}
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function save( billing_supplier ){
	
		var identifier = getSupplierInvoiceID( billing_supplier );
	
		var data = "comment=" 			+ escape( $( "#comment" 			+ identifier ).val() );
		data += "&Date=" 				+ escape( $( "#Date" 				+ identifier ).val() );
		data += "&deliv_payment=" 		+ escape( $( "#deliv_payment" 		+ identifier ).val() );
		data += "&idpayment=" 			+ escape( $( "#idpayment" 			+ identifier ).val() );
		data += "&idpayment_delay=" 	+ escape( $( "#idpayment_delay" 	+ identifier ).val() );
		data += "&billing_supplier=" 	+ escape( $( "#billing_supplier" 	+ identifier ).val() );
		data += "&total_amount_ht=" 	+ escape( $( "#total_amount_ht" 	+ identifier ).val() );
		data += "&total_charge_ht=" 	+ escape( $( "#total_charge_ht" 	+ identifier ).val() );
		data += "&total_amount=" 		+ escape( $( "#total_amount" 		+ identifier ).val() );
		data += "&sundry_expenses=" 	+ escape( $( "#sundry_expenses" 	+ identifier ).val() );
		data += "&extra_taxes=" 		+ escape( $( "#extra_taxes" 		+ identifier ).val() );
		
		$.ajax({
			 	
			url: "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?update&billing_supplier=" + escape( billing_supplier ) + "&idsupplier=<?php echo intval( $_REQUEST[ "idsupplier" ] ); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ 
			
				alert( "Impossible de sauvegarder les modifications" ); 
				resetEditor(); 
				
			},
		 	success: function( responseText ){

				if( responseText != "1" && responseText != "SUPPLIER_INVOICE_ID_CHANGED" ){
					
					alert( "Impossible de sauvegarder les modifications : " + responseText ); 
					resetEditor();
					return;
					
				}
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );
        		
        		if( responseText == "SUPPLIER_INVOICE_ID_CHANGED" ){
        		
        			if( billing_supplier == '<?php echo $_REQUEST[ "billing_supplier" ]; ?>' )
        				document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=" + escape( $( "#billing_supplier" + identifier ).val() ) + "&idsupplier=<?php echo intval( $_REQUEST[ "idsupplier" ] ); ?>";
        			else document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=<?php echo urlencode( $_REQUEST[ "billing_supplier" ] ); ?>&idsupplier=<?php echo intval( $_REQUEST[ "idsupplier" ] ); ?>";
        		
        		}
        			
			}

		});
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function updateTotalET( billing_supplier ){

		var identifier = getSupplierInvoiceID( billing_supplier );
		
		var totalET = 0.0;
		
		$( "#items" + identifier ).find( ".item" ).each( function( i ){
		
			var discount_price = parseFloat( $( this ).find( ".discount_price" ).html().replace( new RegExp( ",", "g" ), "." ) );
			
			var value = discount_price * parseInt( $( this ).find( ".quantity" ).val() );

			$( this ).find( ".value" ).html( value.toFixed( 2 ) );
			
			totalET += parseFloat( value.toFixed( 2 ) );
			
		} );

		var sundry_expenses = parseFloat( $( "#sundry_expenses" + identifier ).val().replace( new RegExp( ",", "g" ), "." ) );
		var extra_taxes 	= parseFloat( $( "#extra_taxes" + identifier ).val().replace( new RegExp( ",", "g" ), "." ) );

		totalET += sundry_expenses + extra_taxes;
		
		$( "#total_amount_ht" + identifier ).val( totalET.toFixed( 2 ) );

		updateTotalATI( billing_supplier );
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function updateTotalATI( billing_supplier ){

		var identifier = getSupplierInvoiceID( billing_supplier );

		var totalET = parseFloat( $( "#total_amount_ht" + identifier ).val().replace( new RegExp( ",", "g" ), "." ) );
		var chargesET 	= parseFloat( $( "#total_charge_ht" + identifier ).val().replace( new RegExp( ",", "g" ), "." ) );

		var vat_rate 	= parseFloat( $( "#vat_rate" + identifier ).val() );
		var totalATI 	= ( totalET + chargesET ) * ( 1.0 + vat_rate / 100.0 );
		var vat 		= totalATI - totalET - chargesET;
		
		$( "#VAT" + identifier ).val( vat.toFixed( 2 ) );
		$( "#total_amount" + identifier ).val( totalATI.toFixed( 2 ) );
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
	function lockSupplierInvoice( billing_supplier ){
	
		if( !confirm( "Etes-vous certains de vouloir vérouiller cette facture fournisseur" ) )
			return;
		var identifier = getSupplierInvoiceID( billing_supplier );
	
		var data = "comment=" 			+ escape( $( "#comment" 			+ identifier ).val() );
		data += "&Date=" 				+ escape( $( "#Date" 				+ identifier ).val() );
		data += "&deliv_payment=" 		+ escape( $( "#deliv_payment" 		+ identifier ).val() );
		data += "&idpayment=" 			+ escape( $( "#idpayment" 			+ identifier ).val() );
		data += "&idpayment_delay=" 	+ escape( $( "#idpayment_delay" 	+ identifier ).val() );
		data += "&billing_supplier=" 	+ escape( $( "#billing_supplier" 	+ identifier ).val() );
		data += "&total_amount_ht=" 	+ escape( $( "#total_amount_ht" 	+ identifier ).val() );
		data += "&total_charge_ht=" 	+ escape( $( "#total_charge_ht" 	+ identifier ).val() );
		data += "&total_amount=" 		+ escape( $( "#total_amount" 		+ identifier ).val() );
		data += "&sundry_expenses=" 	+ escape( $( "#sundry_expenses" 	+ identifier ).val() );
		data += "&extra_taxes=" 		+ escape( $( "#extra_taxes" 		+ identifier ).val() );
		
			
		$.ajax({
			 	
			url: "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?lock&billing_supplier=" + escape( billing_supplier ) + "&idsupplier=<?php echo intval( $_REQUEST[ "idsupplier" ] ); ?>",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ 
			
				alert( "Impossible de vérouiller la commande" ); 
				
			},
		 	success: function( responseText ){

				if( responseText != "1" && responseText != "0" ){
					
					alert( "Impossible de vérouiller la commande : " + responseText ); 
					return;
					
				}
				
				$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
	        	$.growlUI( '', 'Modifications enregistrées' );
        		
        		var done = false;
        		for( var i = 0; i < editableSupplierInvoices.length && !done; i++ ){
        		
        			if( editableSupplierInvoices[ i ] != billing_supplier ){
        			
        				billing_supplier = editableSupplierInvoices[ i ];
        				done = true;
        			
        			}
        			
        			document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=" + billing_supplier + "&idsupplier=<?php echo $_REQUEST[ "idsupplier" ] ?>";
        		
        		}
        			
			}

		});
		
	}
	
	/* --------------------------------------------------------------------------------------------------------------- */
	
/* ]]> */
</script>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div>
		<div class="topLeft"></div>
	    <div class="content">
	    	<div class="headTitle">
				<p class="title">Facturation Fournisseur</p>
            	<div class="rightContainer">
            		<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_order.php?IdOrder=<?php echo urlencode( $_REQUEST[ "billing_supplier" ] ); ?>">
	            		Facture Fournisseur N° 
	            	</a>
	            	<?php echo listSupplierInvoices( $_REQUEST[ "billing_supplier" ] ); ?>
            	</div>
            	<br style="clear:both;" />
        	</div>
        	<div class="subcontent" style="margin-top:10px;">
		        <?php displayQuantityDialog(); ?>
		        <div style="float:left; width:500px; height:100%; margin-top:20px; padding-right:10px; border-right:2px dashed #D2D2D2;">
		        <?php
					
					displaySupplierInvoice( $_REQUEST[ "billing_supplier" ], true );
					
					?>
		        </div>
		        <div style="float:right; width:500px; margin-left:5px;">
					<?php displayTabs( $_REQUEST[ "billing_supplier" ] ); ?>
				</div>
				<br style="clear:both;" />
			</div><!-- subContent -->
		</div><!-- content -->
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div><!-- mainContent -->
</div><!-- globalMainContent -->
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
//-----------------------------------------------------------------------------------

function displaySupplierInvoice( $billing_supplier, $editable = false ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	$supplierInvoice = new SupplierInvoice( $billing_supplier, $_REQUEST[ "idsupplier" ] );

	if( !$supplierInvoice->get( "editable" ) ){
		
		?>
		<p style="text-align:center;">La facture <b><?php echo htmlentities( $supplierInvoice->get( "billing_supplier" ) ); ?></b> n'est pas modifiable</p>
		<?php
		
		return;
		
	}
	
	?>
	<div class="supplierInvoice">
		<div class="header">
			<div style="width:50%; float:left;">
				<img class="logo" src="supplier_invoices.php?logo&amp;idsupplier=<?php echo intval( $_REQUEST[ "idsupplier" ] ); ?>" alt="" style="opacity:0.6;" onclick="this.src=this.src+'&1';" />
				<div class="supplierAddress">
					<p><?php echo htmlentities( $supplierInvoice->getSupplier()->getAddress()->getAddress() ); ?></p>
					<p>
						<?php echo htmlentities( $supplierInvoice->getSupplier()->getAddress()->getZipcode() ); ?>
						&nbsp;
						<?php echo htmlentities( $supplierInvoice->getSupplier()->getAddress()->getCity() ); ?>
						&nbsp;
						<?php echo htmlentities( DBUtil::getDBValue( "name_1", "state", "idstate", $supplierInvoice->getSupplier()->getAddress()->getIdState() ) ); ?></p>
					</p>
					<p>Tél. : <?php echo Util::phoneNumberFormat( $supplierInvoice->getSupplier()->getContact()->get( "phonenumber" ) ); ?> &nbsp; Fax : <?php echo Util::phoneNumberFormat( $supplierInvoice->getSupplier()->getContact()->get( "faxnumber" ) ); ?></p>
					<p>Email : <a href="mailto:<?php echo $supplierInvoice->getSupplier()->getContact()->get( "email" ); ?>"><?php echo htmlentities( $supplierInvoice->getSupplier()->getContact()->get( "email" ) ); ?></a></p>
				</div>
			</div>
			<div style="width:50%; float:right; clear:right;">
				<div style="float:right; margin:4px;">
					<img title="Vérouiller cette Facture Fournisseur" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_lock.png" style="cursor:pointer; opacity:0.7;" onclick="lockSupplierInvoice('<?php echo $supplierInvoice->get( "billing_supplier" ); ?>'); return false;" />
					<?php
					
						if( $editable ){
							
							?>
							<img title="Créer une facture fournisseur" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_create2.png" style="cursor:pointer; opacity:0.8;" onclick="createSupplierInvoice();" />
							<?php
							
						}
						else{
							
							?>
							<img title="Supprimer cette facture fournisseur" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_delete.png" alt="" style="cursor:pointer;" onclick="dropSupplierInvoice( '<?php echo $supplierInvoice->get( "billing_supplier" ); ?>' );" />
							<?php
							
						}
					
				?>
				</div>
				<p style="text-align:left; font-weight:bold;">
					FACTURE FOURNISSEUR
					<br />N° 
					<input type="text" class="supplierInvoiceID" id="billing_supplier<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" value="<?php echo htmlentities( $supplierInvoice->get( "billing_supplier" ) ); ?>" />
				</p>
			</div>
			<br style="clear:both;" />
		</div>
		<div style="float:left; width:250px; text-align:center;">
			<div style="margin:4px;">
				<i>Date de facture</i> 
				<input id="Date<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" type="text" size="10" class="date" value="<?php echo str_replace( "/", "-", Util::dateFormatEu( $supplierInvoice->get( "Date" ) ) ); ?>" />
				<?php DHTMLCalendar::calendar( "Date" . getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ) ); ?>
			</div>
		</div>
		<div style="float:left; width:250px; text-align:center;">
			<div style="width:26px; float:right; margin-top:4px;">
				<a title="Sauvegarder les informations" href="#" onclick="save( '<?php echo $supplierInvoice->get( "billing_supplier" ); ?>' ); return false;">
					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" />
				</a>
			</div>
			<div style="margin:4px;">
				<i>Date d'échéance</i> 
				<input id="deliv_payment<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" type="text" size="10" class="date" value="<?php echo str_replace( "/", "-", Util::dateFormatEu( $supplierInvoice->get( "deliv_payment" ) ) ); ?>" />
				<?php DHTMLCalendar::calendar( "deliv_payment" . getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ) ); ?>
			</div>	
		</div>
		<br style="clear:both;" />
		<div class="body">
			<div class="itemHeaders">
				<div class="itemHeader" style="width:76px;">Photo</div>
				<div class="itemHeader" style="width:180px;">Référence / Désignation</div>
				<div class="itemHeader" style="width:70px;">Montant</div>
				<div class="itemHeader" style="width:70px;">Qté</div>
				<div class="itemHeader" style="width:90px;">Valeur</div>
			</div>
			<br style="clear:both;" />
			<ul class="items" id="items<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>"<?php if( !$supplierInvoice->getItems()->size() ) echo " style=\"height:70px;\""; ?>>
			<?php

				$totalET = 0.0;
				$i = 0;
				while( $i < $supplierInvoice->getItems()->size() ){
					
					$item =& $supplierInvoice->getItems()->get( $i );
					$deliveryNoteItem = ( object )DBUtil::query( "SELECT * FROM bl_delivery_row WHERE idbl_delivery = '" . $item->get( "idbl_delivery" ) ."' AND idrow = '" . $item->get( "idrow" ) . "' LIMIT 1" )->fields;
					
					$deliveryPrice = ( object )DBUtil::query( "SELECT * FROM order_supplier_row WHERE idarticle = '" . $deliveryNoteItem->idarticle ."' AND ref_supplier = '" . $deliveryNoteItem->ref_supplier ."' AND idrow = '" . $item->get( "idrow" ) . "' LIMIT 1" )->fields;
					
					?>
					<li class="item" id="<?php echo $item->get( "idrow" ); ?>">
						<input type="hidden" class="idbl_delivery" value="<?php echo $item->get( "idbl_delivery" ); ?>" />
						<div class="itemValue" style="width:70px;">
							<img class="referenceThumb" src="<?php echo URLFactory::getReferenceImageURI( $deliveryNoteItem->idarticle, URLFactory::$IMAGE_ICON_SIZE ); ?>&amp;w=70&amp;h=70" alt="" />
						</div>
						<div class="itemValue" style="width:170px;">
							Réf. :<?php echo htmlentities( $deliveryNoteItem->reference ); ?><br />
							Réf. fournisseur : <?php echo htmlentities( $deliveryNoteItem->ref_supplier ); ?>
							<br />
							<?php echo $deliveryNoteItem->designation; ?>
						</div>
						<div class="itemValue numeric" style="width:60px;">
							<span class="discount_price"><?php echo number_format( $deliveryPrice->unit_price, 2, ",", "" ); ?></span> &euro;
						</div>
						<div class="itemValue numeric" style="width:60px;">
							x <input class="quantity" type="text" value="<?php echo $item->get( "quantity" ); ?>" readonly="readonly" />
						</div>
						<div class="itemValue numeric" style="width:80px;">
							<span class="value"><?php echo number_format( $item->get( "quantity" ) * $deliveryPrice->unit_price, 2, ",", "" ); ?></span> &euro;
						</div>
						<br style="clear:both;" />
					</li>
					<?php
			
					$totalET += $item->get( "quantity" ) * $deliveryPrice->unit_price;
					$i++;
					
				}
				
			?>
			</ul>	
			<div class="amountsHeaders">
				<div class="amountHeader" style="width:244px;">&nbsp;</div>
				<div class="amountHeader" style="width:122px;">Frais divers HT</div>
				<div class="amountHeader" style="width:122px;">Taxes diverses HT</div>
			</div>
			<br style="clear:both;" />
			<div class="amountsValues">
				<div class="amountValue" style="width:244px;"></div>
				<div class="amountValue" style="width:122px;">
					<input id="sundry_expenses<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" class="editableAmount" type="text" value="<?php echo number_format( $supplierInvoice->get( "sundry_expenses" ), 2 ); ?>" onkeyup="updateTotalET('<?php echo $supplierInvoice->get( "billing_supplier" ); ?>');" /> &euro;
				</div>
				<div class="amountValue" style="width:122px;">
					<input id="extra_taxes<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" class="editableAmount" type="text" value="<?php echo number_format( $supplierInvoice->get( "extra_taxes" ), 2 ); ?>" onkeyup="updateTotalET('<?php echo $supplierInvoice->get( "billing_supplier" ); ?>');" /> &euro;
				</div>
				<br style="clear:both;" />
			</div>
			<div class="amountsHeaders">
				<div class="amountHeader" style="width:122px;">Total HT</div>
				<div class="amountHeader" style="width:122px;">Port HT</div>
				<div class="amountHeader" style="width:122px;">TVA</div>
				<div class="amountHeader" style="width:122px;">Total TTC</div>
			</div>
			<br style="clear:both;" />
			<div class="amountsValues">
				<div class="amountValue" style="width:122px;">
					<input id="total_amount_ht<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" class="editableAmount" type="text" value="<?php echo number_format( $supplierInvoice->get( "total_amount_ht" ) - $supplierInvoice->get( "total_charge_ht" ), 2, ",", "" ); ?>" onkeyup="updateTotalATI('<?php echo $supplierInvoice->get( "billing_supplier" ); ?>');" /> &euro;
				</div>
				<div class="amountValue" style="width:122px;">
					<input id="total_charge_ht<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" class="editableAmount" type="text" value="<?php echo number_format( $supplierInvoice->get( "total_charge_ht" ), 2, ",", "" ); ?>" onkeyup="updateTotalATI('<?php echo $supplierInvoice->get( "billing_supplier" ); ?>');" /> &euro;
				</div>
				<div class="amountValue" style="width:122px;">
					<input type="text" id="VAT<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>"class="nonEditableAmount" value="<?php echo number_format( $supplierInvoice->get( "total_amount" ) - $supplierInvoice->get( "total_amount_ht" ), 2, ",", "" ); ?>" /> &euro;
					<input type="hidden" id="vat_rate<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" value="<?php echo $supplierInvoice->getSupplier()->getVATRate(); ?>" />
				</div>
				<div class="amountValue" style="width:122px;">
					<input type="text" id="total_amount<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" class="editableAmount" type="text" value="<?php echo number_format( $supplierInvoice->get( "total_amount" ), 2, ",", "" ); ?>" /> &euro;
				</div>
				<br style="clear:both;" />
			</div>
		</div>
		<div class="" style="border-top:2px dotted #E7E7E7; border-bottom:2px dotted #E7E7E7; margin:10px 0px; padding:10px 0px;">
			<div style="float:left; width:230px; text-align:center;">
				<div style="margin:4px;">
					<i>Mode de paiement</i>
					<?php 
					
						XHTMLFactory::createSelectElement( 
							"payment", 
							"idpayment", 
							"name" . User::getInstance()->getLang(), 
							"name" . User::getInstance()->getLang(), 
							$supplierInvoice->get( "idpayment" ), 
							false, "", 
							"id=\"idpayment" . getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ) . "\" style=\"width:120px;\"" 
						); 
				?>
				</div>
			</div>
			<div style="width:26px; float:right; margin-top:4px;">
				<a title="Sauvegarder les informations" href="#" onclick="save( '<?php echo $supplierInvoice->get( "billing_supplier" ); ?>' ); return false;">
					<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" />
				</a>
			</div>
			<div style="float:right; width:240px; text-align:center;">
				<div style="margin:4px;">
					<i>Délai de paiement</i>
					<?php 
					
						XHTMLFactory::createSelectElement( 
							"payment_delay", 
							"idpayment_delay", 
							"payment_delay" . User::getInstance()->getLang(), 
							"payment_delay" . User::getInstance()->getLang(), 
							$supplierInvoice->get( "idpayment_delay" ), 
							false, "", 
							"id=\"idpayment_delay" . getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) )  . "\" style=\"width:120px;\""
							
						); 
							
				?>
				</div>
			</div>
			<br style="clear:both;" />
		</div>
		<div class="comment" style="text-align:right;">
			<a title="Afficher ou Masquer les commentaires" style="margin:4px;" class="orangeText" href="#" onclick="displayComments( '<?php echo $supplierInvoice->get( "billing_supplier" ); ?>' ); return false;">
				<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" /> Commentaires
			</a>
			<a href="#" onclick="save( '<?php echo $supplierInvoice->get( "billing_supplier" ); ?>' ); return false;" title="Sauvegarder les commentaires">
				<img id="SaveComment<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="display:<?php echo strlen( $supplierInvoice->get( "comment" ) ) ? "inline" : "none"; ?>;" />
			</a>
			<div id="CommentDiv<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>" style="display:<?php echo strlen( $supplierInvoice->get( "comment" ) ) ? "block" : "none"; ?>;">
				<textarea id="comment<?php echo getSupplierInvoiceID(  $supplierInvoice->get( "billing_supplier" ) ); ?>"><?php echo strip_tags( $supplierInvoice->get( "comment" ) ); ?></textarea>
			</div>
		</div>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------------

function listSupplierInvoices( $billing_supplier = false ){
	
	global $GLOBAL_START_URL;
	
	$editableSupplierInvoices = getEditableSupplierInvoices();

	if( $editableSupplierInvoices === false )
		return;
		
	if( count( $editableSupplierInvoices ) == 1 ){
	
		echo $editableSupplierInvoices[ 0 ];
		return;
		
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function selectSupplierInvoice( billing_supplier ){
		
			document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?billing_supplier=" + escape( billing_supplier ) + "&idsupplier=<?php echo $_REQUEST[ "idsupplier" ]; ?>";
		
		}
	
	/* ]]> */
	</script>
	<select id="supplierInvoiceList" onchange="selectSupplierInvoice( this.options[ this.selectedIndex ].value );">
	<?php
	
		foreach( $editableSupplierInvoices as $editableSupplierInvoice ){
		
			$selected = $editableSupplierInvoice == $billing_supplier ? " selected=\"selected\"" : "";
			
			?>
			<option<?php echo $selected; ?> value="<?php echo $editableSupplierInvoice; ?>"><?php echo $editableSupplierInvoice; ?></option>
			<?php
			
		}
	
	?>
	</select>
	<?php
	
}

//-----------------------------------------------------------------------------------

function displayTabs( $editedSupplierInvoice ){

	$supplierInvoices = getEditableSupplierInvoices();
	
	if( $supplierInvoices === false )
		return;
		
	?>
	<script type="text/javascript">
	/* <![CDATA[ */

	/* ------------------------------------------------------------------------------- */
	
	$( document ).ready( function(){ 

		/* onglets */
		
		$(document).ready(function(){
		
			$('#supplierInvoiceTabs').tabs({ fxFade: true, fxSpeed: 'fast', selected: 0 });
		
		});

	} );
	
	/* ------------------------------------------------------------------------------- */
	
	/* ]]> */
	</script>
	<div id="supplierInvoiceTabs">
		<ul>
		<?php

			foreach( $supplierInvoices as $billing_supplier ){
				
				if( $billing_supplier != $editedSupplierInvoice ){
					
					?>
					<li>
						<a title="Visualiser le contenu" href="#SupplierInvoicePanel<?php echo getSupplierInvoiceID(  $billing_supplier ); ?>">FACTURE N° <?php echo htmlentities( $billing_supplier ); ?></a>
					</li>
					<?php
				
				}
				
			}
			
			
		?>
		</ul>
		<?php
		
		foreach( $supplierInvoices as $billing_supplier ){
			
			if( $billing_supplier != $editedSupplierInvoice  ){
				
				?>
				<div id="SupplierInvoicePanel<?php echo getSupplierInvoiceID(  $billing_supplier ); ?>"><?php displaySupplierInvoice( $billing_supplier ); ?></div>
				<?php
				
			}
			
		}
			
	?>
	</div>
	<?php
	
		
}

//-----------------------------------------------------------------------------------

function displayQuantityDialog(){
	
	global $GLOBAL_START_URL;

	?>
	<div id="QuantityDialog" style="display:none;">     
		<p style="text-align:center; height:70px; vertical-align:middle;">
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/prev.png" alt="" style="cursor:pointer;" onclick="transferQuantity(-1);" />
			<input id="sourceQuantity" class="quantity" type="text" value="0" readonly="readonly" onfocus="this.blur();" />
			<img id="transferThumb" src="" alt="" />
			<input id="targetQuantity" class="quantity" type="text" value="0" readonly="readonly" onfocus="this.blur();" />
			<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/next.png" alt="" style="cursor:pointer;" onclick="transferQuantity(+1);" />
		</p>
		<p>
			<div id="quantitySlider"></div>
		</p>
		<div></div>
    	<p style="text-align:right;">
    		<input type="button" value="Annuler" id="CancelButton" class="blueButton" onclick="$( '#QuantityDialog' ).dialog( 'destroy' );" />
    		<input type="button" value="Valider" id="ValidateButton" class="blueButton" onclick="$( '#QuantityDialog' ).dialog( 'destroy' );" />
    	</p>
    </div>
    <?php
	        
}

//-----------------------------------------------------------------------------------

function getEditableSupplierInvoices(){

	$editableSupplierInvoices = array();
	
	$query = "
	SELECT billing_supplier
	FROM billing_supplier
	WHERE editable = TRUE
	AND idsupplier = '" . ( $_REQUEST[ "idsupplier" ] ) . "'
	ORDER BY billing_supplier ASC";
	
	$rs =& DBUtil::query( $query );
			
	while( !$rs->EOF() ){
		
		$editableSupplierInvoices[] = $rs->fields( "billing_supplier" );
		
		$rs->MoveNext();
		
	}
	
	return $editableSupplierInvoices;

}

//-----------------------------------------------------------------------------------

function createSupplierLogo( $idsupplier ){
	
	global $GLOBAL_START_PATH;
	
	$supplierName = DBUtil::getDBValue( "name","supplier", "idsupplier", intval( $idsupplier ) );
	
	/*if( strlen( $supplierName ) > 8 )
		$supplierName = substr( $supplierName, 0, 8 ) . "...";*/
		
	$fontsize 	= 50;
	//$fontfile 	= "$GLOBAL_START_PATH/fonts/NONSTOP_.TTF";
	$fontfile 	= "$GLOBAL_START_PATH/fonts/" . getRandomFont();
	
	$bbox = imagettfbbox( $fontsize, 0, $fontfile, $supplierName );
	/*
	0 	Coin inférieur gauche, abscisse
	1 	Coin inférieur gauche, ordonnée
	2 	Coin inférieur droit, abscisse
	3 	Coin inférieur droit, ordonnée
	4 	Coin supérieur droit, abscisse
	5 	Coin supérieur droit, ordonnée
	6 	Coin supérieur gauche, abscisse
	7 	Coin supérieur gauche, ordonnée
	 */
	$imageWidth 	= $bbox[ 2 ] - $bbox[ 0 ] + 40;
	$imageHeight 	= $bbox[ 1 ] - $bbox[ 7 ] + 40;
	
	$image = imagecreatetruecolor( $imageWidth, $imageHeight );

	$backgroundColor = imagecolorallocate( $image, 247, 247, 247 );
	imagefill( $image, 0, 0, $backgroundColor );

	$red = imagecolorallocate( $image, 255, 0, 0 );
	imagettftext ( $image, $fontsize, 0, 20, $imageHeight - 20, $red, $fontfile, substr( $supplierName, 0, 1 ) );
	
	$bbox = imagettfbbox( $fontsize, 0, $fontfile, substr( $supplierName, 0, 1 )  );
	$offset = $bbox[ 2 ] - $bbox[ 0 ] + 20;
	
	$grey = imagecolorallocate( $image, 68, 71, 78 );
	imagettftext ( $image, $fontsize, 0, $offset, $imageHeight - 20, $grey, $fontfile, substr( $supplierName, 1 ) );
	
	//----
	
	$outputHeight 	= 50;
	$outputWidth 	= 220;
	
	$inputWidth 	= imageSX( $image );
	$inputHeight 	= imageSY( $image );
	
	$outputImage = ImageCreateTrueColor( $outputWidth, $outputHeight );
	imagecopyresampled( $outputImage, $image, 0, 0, 0, 0, $outputWidth, $outputHeight, $inputWidth, $inputHeight ); 
		
	//----

	header ( "Content-type: image/png" );
	
	imagepng( $outputImage );
	
	imagedestroy( $image );
	imagedestroy( $outputImage );
	
}

//----------------------------------------------------------------------------

function getSupplierInvoiceID( $billing_supplier ){

	return strval( array_search( $billing_supplier, getEditableSupplierInvoices() ) );
	
	//return ereg_replace( "[^0-9a-zA-Z]", "_", $text );
	
}

//----------------------------------------------------------------------------

function getRandomFont(){
	
	global $GLOBAL_START_PATH;
	
	$dir = opendir( "$GLOBAL_START_PATH/fonts/" );
	
	$fonts = array();
	while( $file = readdir( $dir ) ){
		
		if( eregi( "[^\.]+\.ttf", $file ) )
			array_push( $fonts, $file );
			
	}
	
	closedir( $dir );
	
	return $fonts[ rand( 0, count( $fonts ) - 1 ) ];
	
}

//----------------------------------------------------------------------------

?>