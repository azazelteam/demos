<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des avoirs clients
 */

include_once( "../objects/classes.php" );
include_once( "../catalog/drawlift.php" );

//------------------------------------------------------------------------------------
//numéro d'avoir absent

if( !isset( $_REQUEST[ "idcredit" ] ) && !isset( $_GET[ "idbilling_buyer" ] ) )
	die( "Paramètre absent" );

//------------------------------------------------------------------------------------
//suppression d'un avoir

if( isset( $_GET[ "delete" ] ) && isset( $_GET[ "idcredit" ] ) ){

	$idcredit 	= intval( $_GET[ "idcredit" ] );
	$idbuyer 	= DBUtil::getDBValue( "idbuyer", "credits", "idcredit", $idcredit );
	
	//vérifier les droits de création du commercial
	
	if( !Credit::allowCreationForCustomer( $idbuyer ) )
		die( Dictionnary::translate( "gest_com_access_denied" ) );

	Credit::delete( $idcredit );
	
	header( "Location: $GLOBAL_START_URL/accounting/credit_search.php" );
	
	exit();

}

//numéro d'avoir inexistant

if( isset( $_REQUEST[ "idcredit" ] ) ){
	
	$idcredit = intval( $_REQUEST[ "idcredit" ] );
	
	$rs =& DBUtil::query( "SELECT COUNT(*) AS `exists` FROM credits WHERE idcredit = '$idcredit' LIMIT 1" );
	
	if( $rs === false )
		trigger_error( "Impossible d'afficher la page", E_USER_ERROR );
	
	if( !$rs->fields( "exists" ) ){
		
		include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
		
		?>
		<div id="globalMainContent">
			<div class="mainContent fullWidthContent">
				<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title" style="color:red;">
							<?php echo str_replace( "%idcredit", $idcredit, Dictionnary::translate( "gest_com_credit_doesnt_exist" ) ) ?>
						</p>
						<div class="rightContainer"></div>
					</div>
				</div>
				<div class="bottomRight"></div><div class="bottomLeft"></div>
			</div> <!-- mainContent fullWidthContent -->
		</div> <!-- globalMainContent -->

		<?php
		
		include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
		exit();
		
	}
	
}

//------------------------------------------------------------------------------------
//création d'un nouvel avoir

if( isset( $_GET[ "idbilling_buyer" ] ) ){
	
	$idbilling_buyer 	= intval( $_GET[ "idbilling_buyer" ] );
	$idbuyer 			= DBUtil::getDBValue( "idbuyer", "billing_buyer", "idbilling_buyer", $idbilling_buyer );
	
	//vérifier les droits de création du commercial
	
	if( !Credit::allowCreationForCustomer( $idbuyer ) )
		die( Dictionnary::translate( "gest_com_access_denied" ) );
	
	$credit 	=TradeFactory::createCreditFromInvoice( new Invoice( $idbilling_buyer ) );
	$idcredit 	= $credit->getId();
	
	header( "Location: $GLOBAL_START_URL/accounting/credit.php?idcredit=$idcredit" );
	
	exit();

}

//------------------------------------------------------------------------------------

$idcredit = intval( $_REQUEST[ "idcredit" ] );
$credit 	= new Credit( $idcredit );
$invoice 	= new Invoice( $credit->get( "idbilling_buyer" ) );

//------------------------------------------------------------------------------------

//vérification des droits de consultation

$iduser = User::getInstance()->getId();

if( !$credit->allowRead() )
	die( Dictionnary::translate( "gest_com_access_denied" ) );

//------------------------------------------------------------------------------------

//ajout d'articles depuis la facture ( chtite popup )

if( isset( $_GET[ "idbilling_buyer_row" ] ) && isset( $_GET[ "quantity" ] ) ){
	
	$idbilling_buyer_row = intval( $_GET[ "idbilling_buyer_row" ] );

	//quantité facturée
	
	$idbilling_buyer = $credit->get( "idbilling_buyer" );
	
	$query = "
	SELECT quantity
	FROM billing_buyer_row
	WHERE idbilling_buyer = '$idbilling_buyer'
	AND idrow = '$idbilling_buyer_row'
	LIMIT 1";

	$rs =& DBUtil::query( $query );
	
	$invoicedQuantity = $rs->fields( "quantity" );

	//ajout à la ligne article correspondante si elle exsite déjà
	
	$it = $credit->getItems()->iterator();
	$done = false;
	
	while( $it->hasNext() && !$done ){
		
		$creditItem =& $it->next();
		
		if( $creditItem->get( "idbilling_buyer_row" ) == $idbilling_buyer_row ){
			
			$quantity = min( intval( $_GET[ "quantity" ] ) + $creditItem->get( "quantity" ), $invoicedQuantity );
			$creditItem->set( "quantity", $quantity );
			$done = true;
			
			$credit->save();
			
		}
		
	}

	//création d'une nouvelle ligne article
	
	if( !$done ){
		
		$it = $invoice->getItems()->iterator();
		$done = false;
		
		while( $it->hasNext() && !$done ){
			
			$invoiceItem =& $it->next();
			
			if( $invoiceItem->get( "idrow" ) == $idbilling_buyer_row ){
				
				$tmpInvoiceItem = $invoiceItem; //ne pas modifier la facture
				$tmpInvoiceItem->set( "quantity", min( $tmpInvoiceItem->get( "quantity" ), intval( $_GET[ "quantity" ] ) ) );
		
				$credit->addInvoiceItem( $tmpInvoiceItem );
				$done = true;
				
				$credit->save();
				
			}
			
		}
		
	}
	
}

//------------------------------------------------------------------------------------

//traitement du formulaire

if( isset( $_POST[ "ModifyCredit" ] ) ){
	
	include_once( "$GLOBAL_START_PATH/accounting/credit_parseform.php" );
	ModifyCredit( $credit );
		
}

//------------------------------------------------------------------------------------

$company = $credit->getCustomer()->get("company");
$Title = "Avoir n° $idcredit - $company";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript" language="javascript">
<!--

// PopUp pour les infos des remises au volume
function PopupVolume(ref,buy)
{
	postop = (self.screen.height-150)/2;
	posleft = (self.screen.width-300)/2;

	window.open('volume_price.php?r='+ref+'&b='+buy, '_blank', 'top=' + postop + ',left=' + posleft + ',width=300,height=150,resizable,scrollbars=yes,status=0');
	
}

// PopUp pour les infos du stock
function PopupStock(ref)
{
	postop = (self.screen.height-400)/2;
	posleft = (self.screen.width-400)/2;

	window.open('popup_stock.htm.php?r='+ref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=400,resizable,scrollbars=yes,status=0');
	
}

function isNumeric( text ){

	var validChars = '0123456789';
	var char;
	
	var i = 0;
	while( i < text.length ){
   
   		char = text.charAt( i );
   		
   		if ( validChars.indexOf( char ) == -1 ) 
   			return false;
   			
		i++;
   	
	}
   	
   	return true;

}

// -->
</script>
<?php 

//---------------------------------------------------------------------------------------------
	
$disabled = $credit->get( "editable" ) ? "" : " disabled=\"disabled\""; 

?>
<div id="globalMainContent">
	<form action="credit.php?idcredit=<?php echo $idcredit ?>" method="post" name="frm" enctype="multipart/form-data">
		<input type="hidden" name="ModifyCredit" id="ModifyCredit" value="1" />
		<input type="hidden" name="idcredit" value="<?php echo $idcredit ?>">
		<?php
	
			include( "$GLOBAL_START_PATH/templates/accounting/credit/buyer.htm.php" );
			include( "$GLOBAL_START_PATH/templates/accounting/credit/credit.htm.php" );
			include( "$GLOBAL_START_PATH/templates/accounting/credit/amounts.htm.php" );
			include( "$GLOBAL_START_PATH/templates/accounting/credit/comments.htm.php" );
			
			if( $credit->get( "editable" ) ){
				
				?>
				<div class="fullWidthContent submitZone">
					<div style="float:right;">
						<input type="submit" class="blueButton" value="<?php echo Dictionnary::translate( "gest_com_save" ); ?>" <?php echo $disabled ?>/>
						<input type="submit" name="Validate" class="blueButton" style="margin-left:5px;" value="<?php echo Dictionnary::translate( "gest_com_validate" ); ?>" onclick="return confirm( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "gest_com_validate_credit" ) ) ?>' );" <?php echo $disabled ?>/>
						<?php
						
							/* ne pas autoriser la suppression d'un avoir lié à un remboursement */
							
							if( !DBUtil::query( "SELECT idrepayment FROM repayments WHERE idcredit = '" . $credit->get( "idcredit" ) . "' LIMIT 1" )->RecordCount() ){
								
								?>
								<input type="button" class="blueButton" style="margin-left:5px;" value="<?php echo Dictionnary::translate( "gest_com_delete" ); ?>" onclick="if( confirm( '<?php echo str_replace( "'", "\'", Dictionnary::translate( "gest_com_confirm_delete_credit" ) ) ?>' ) ) document.location = '<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $credit->getId() ?>&delete'; else return false;" <?php echo $disabled ?>/>
								<?php
							
							}
							
						?>
					</div>
				</div>
				<?php
				
			}
			
		?>
		<div class="clear"></div>
	</form>
</div> <!-- globalMainContent -->
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function anchor( $name, $hasIndex = false ){

	if( $hasIndex ){
		
		foreach( $_POST as $key => $value ){
		
			$pos = strpos( $key, $name );	
			if( $pos !== false && !$pos ){
				
				echo "<a name=\"lastaction\"></a>";
				return;	
				
			}
			
		}
		
	}
	else if( isset( $_POST[ $name ] ) || isset( $_POST[ $name . "_x" ] ) )	
		echo "<a name=\"lastaction\"></a>";
	
}

//----------------------------------------------------------------------------

?>