<?php

include_once( "../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

//------------------------------------------------------------------------------------------

$Title = "Sortir une situation";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

$search = "";
$resultCount = 0;

if( isset( $_POST[ "search" ] ) )
	$search = $_POST[ "search" ];

if( isset( $_GET[ "search" ] ) )
	$search = $_GET[ "search" ];
      

?>
<script type="text/javascript" language="javascript">
<!--
	
	function SortResult(sby,ord,rq){
		
		document.adm_estim.action="<?php echo $ScriptName ?>?search=1&sortby="+sby+"&sens="+ord+"&q="+rq;
		document.adm_estim.submit();
		
	}
	
	function selectMonth(){ document.getElementById( "interval_select_month" ).checked=true; }
	
	function selectBetween(){ document.getElementById( "interval_select_between" ).checked=true; }
	
// -->
</script>

<style type="text/css">
	.slideUp{
		background:url(<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-reduire.jpg) no-repeat;
		width:60px;
		height:12px;
		float:right;
	}
	
	.slideDown{
		background:url(<?php echo $GLOBAL_START_URL ?>/images/back_office/content/icons-agrandir.jpg) no-repeat;
		width:60px;
		height:12px;
		float:right;
	}
</style>

<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form action="<?php echo $ScriptName ?>" method="post" name="adm_estim" id="adm_estim">
		<input type="hidden" name="search" value="1" />
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle">
					<p class="title">Sortir une situation</p>
				</div>
				<div class="subContent">
					<!-- tableau choix de date -->
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell"><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2" checked="checked" />Pour le mois de</th>
								<td colspan="3"><?php FStatisticsGenerateDateHTMLSelect( "selectMonth()","width:20%;" ) ?></td>
							</tr>
							<tr>	
								<th class="filledCell" style="width:30%"><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> Entre le</th>
								<td style="width:25%">
									<?php $date = isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : ""; ?>
									<input type="text" name="bt_start" id="bt_start" class="calendarInput" value="<?php echo $date ?>" />
									<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
								</td>
								<th class="filledCell" style="width:20%"> et le</th>
								<td style="width:25%">
									<?php $date = isset( $_POST[ "bt_end" ] ) ? $_POST[ "bt_end" ] : ""; ?>
									<input type="text" name="bt_end" id="bt_end" class="calendarInput" value="<?php echo $date ?>" />
									<?php echo DHTMLCalendar::calendar( "bt_end", true, "selectBetween" ) ?>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
                            <tr>
                                <th class="filledCell">Facture client n°</th>
                                <td colspan="3"><input type="text" name="idbilling_buyer" class="textInput" value="<?php echo isset( $_POST[ "idbilling_buyer" ] ) ? $_POST[ "idbilling_buyer" ] : "" ?>" /></td>
                            </tr>
                        </table>
                    </div>
					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" value="Rechercher" />
					</div>
				</div>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
	</form>
<?php 

$error_sql = true;

if ( $search != ""){
	
	$now = getdate();
	$interval_select = $_POST[ "interval_select" ];
	$SQL_Condition = "";
	$SQL_ConditionCredit = "";
	$SQL_ConditionCreditFs = "";
	
	switch( $interval_select ){
		
			
		case 2:
			$yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now["year"]--;
			
			$Min_Time = date( "Y-m-d", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $yearly_month_select, 1, $now[ "year" ] ) );
			$Max_Time = date( "Y-m-d", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $yearly_month_select + 1, 1, $now[ "year" ] ) );
			
			$SQL_Condition .= " AND bl.dispatch_date >= '$Min_Time' AND bl.dispatch_date < '$Max_Time'";
			$SQL_ConditionCredit .= " c.DateHeure >= '$Min_Time' AND c.DateHeure < '$Max_Time'";
			$SQL_ConditionCreditFs .= " c.Date >= '$Min_Time' AND c.Date < '$Max_Time'";
			
			break;
			
		case 3:

			$sp_bt_start = $_POST[ "bt_start" ];
			$sp_bt_end = $_POST[ "bt_end" ];

			$Min_Time = date( "Y-m-d", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], substr( $sp_bt_start, 3, 2 ), substr( $sp_bt_start, 0, 2 ), substr( $sp_bt_start, 6, 4 ) ) );
			$Max_Time = date( "Y-m-d", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], substr( $sp_bt_end, 3, 2 ), substr( $sp_bt_end, 0, 2 ) + 1, substr( $sp_bt_end, 6, 4 ) ) );
			
			$SQL_Condition .= " AND bl.dispatch_date >= '$Min_Time' AND bl.dispatch_date < '$Max_Time'";
			$SQL_ConditionCredit .=" c.DateHeure >= '$Min_Time' AND c.DateHeure < '$Max_Time'";
			$SQL_ConditionCreditFs .=" c.Date >= '$Min_Time' AND c.Date < '$Max_Time'";
			
			break;

			
	}
	
	//Numéro de facture fournisseur	
	if ( !empty( $_POST[ "billing_supplier" ] ) ){

		$SQL_Condition .= " AND bb.idbilling_buyer = '".$_POST[ "idbilling_buyer" ]."'";

	}
	
	$SQL_Condition .= "AND bl.idbilling_buyer = bb.idbilling_buyer";
	
	getInvoices($Min_Time, $Max_Time,$SQL_Condition,$SQL_ConditionCredit,$SQL_ConditionCreditFs);
	getNotInvoicedOrders();
	getLatestBillingsBuyer($Min_Time,$Max_Time);
	
}

?>
</div>
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//--------------------------------------------------------------------------------------------------

function getInvoices($min,$max,$SQL_Condition,$SQL_ConditionCredit,$SQL_ConditionCreditFs){
	
	global $GLOBAL_START_URL, $billing_supplier;
	
	
	$orderby2 = "ORDER BY bb.idbilling_buyer ASC";
	$orderby1 = "ORDER BY s.name ASC";
	
	/**
	 * factures clients
	 */
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) && isset( $_GET[ "q" ] ) ){
		
		if($_GET[ "q" ] == 1){
			$orderby1 = "ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
		}
		
		if($_GET[ "q" ] == 2){
			$orderby2 = "ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
		}
	
	}
	$queryb = "SELECT DISTINCT(bb.idbilling_buyer), b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht, bb.total_amount
			FROM billing_buyer bb, buyer b, bl_delivery bl
			WHERE bb.idbuyer = b.idbuyer
			$SQL_Condition
			$orderby2";
	$rsb = DBUtil::query($queryb);
	
	if( $rsb === false )
		die("Impossible de récupérer les factures clients");
		
	$bcount = $rsb->RecordCount();
	
	/**
	 * factures fournisseur
	 */
	
	$querys = "SELECT DISTINCT(bsr.billing_supplier), s.name, bs.Date, bs.total_amount_ht, bs.total_amount,s.idsupplier
			FROM billing_buyer bb, bl_delivery bl, billing_supplier bs, billing_supplier_row bsr, supplier s
			WHERE bl.dispatch_date >= '$min' AND bl.dispatch_date <= '$max'
			AND bs.billing_supplier = bsr.billing_supplier
			AND bs.idsupplier = bsr.idsupplier
			AND bsr.idbl_delivery = bl.idbl_delivery
			AND bsr.idsupplier = bl.idsupplier
			AND bb.idbilling_buyer = bl.idbilling_buyer
			AND s.idsupplier = bs.idsupplier
			GROUP BY bsr.billing_supplier, bsr.idsupplier
			$orderby1";
	$rss = DBUtil::query($querys);
	
	if( $rss === false )
		die("Impossible de récupérer les factures fournisseurs");
	
	$bscount = $rss->RecordCount();
	
	$cpt = max($bcount,$bscount);
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$(document).ready(function(){
			$('.supplierInvoiceCell').hover(
				function(){$(this).find('div.supplierInvoiceTip:hidden').show();},
				function(){$(this).find('div.supplierInvoiceTip:visible').hide();}
			);
			
			$('#billingsTableRows').hide();
			$('#billingSupplierNonParvenues').children('thead').hide();
			$('#billingSupplierNonParvenues').children('tbody').hide();
			
		});
					
	/* ]]> */
	</script>
	
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Arrêté de compte (Cut-off) - Base d'affectation des comptes : dates de livraisons fournisseurs</p>
				<a href="#" class="slideDown" onclick="if( $(this).attr('class') == 'slideDown'){$(this).switchClass('slideUp','slideDown');}else{$(this).switchClass('slideDown','slideUp');} $('#billingsTableRows').toggle();return false;"></a>
			</div>
			<div class="subContent">
				<div class="resultTableContainer clear">
					<table class="dataTable resultTable">
						<thead>
							<tr>
							<th colspan="6" style="border-right:2px solid #0000CC;">FACTURES CLIENTS</th>
							<th colspan="5">FACTURES FOURNISSEURS</th>
							</tr>
						</thead>
						<tbody id="billingsTableRows" style="display:none;">
							<tr>
								<th style="background:#EAEAEA;text-align:center;">Client</th>
								<th style="background:#EAEAEA;text-align:center;">N° Client</th>
								<th style="background:#EAEAEA;text-align:center;">N° Facture</th>
								<th style="background:#EAEAEA;text-align:center;">Date facture</th>
								<th style="background:#EAEAEA;text-align:center;">Montant HT</th>
								<th style="background:#EAEAEA;text-align:center;border-right:2px solid #0000CC;">Montant TTC</th>
								<th style="background:#EAEAEA;text-align:center;">Fournisseur</th>
								<th style="background:#EAEAEA;text-align:center;">N° Facture</th>
								<th style="background:#EAEAEA;text-align:center;">Date facture</th>
								<th style="background:#EAEAEA;text-align:center;">Montant HT</th>
								<th style="background:#EAEAEA;text-align:center;">Montant TTC</th>
							</tr>
							<tr>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('b.company','ASC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('b.company','DESC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('b.idbuyer','ASC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('b.idbuyer','DESC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bb.idbilling_buyer','ASC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bb.idbilling_buyer','DESC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bb.DateHeure','ASC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bb.DateHeure','DESC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bb.total_amount_ht','ASC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bb.total_amount_ht','DESC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;border-right:2px solid #0000CC;">
									<a href="javascript:SortResult('bb.total_amount','ASC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bb.total_amount','DESC',2)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('s.name','ASC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('s.name','DESC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bs.billing_supplier','ASC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bs.billing_supplier','DESC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bs.Date','ASC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bs.Date','DESC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bs.total_amount_ht','ASC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bs.total_amount_ht','DESC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder" style="background:#EAEAEA;text-align:center;">
									<a href="javascript:SortResult('bs.total_amount','ASC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bs.total_amount','DESC',1)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
							</tr>					
						<?php
						
						//Totaux premier tableau
						//frs
						$bstotalHT = 0;
						$bstotalTTC = 0;
						//clts
						$bbtotalHT = 0;
						$bbtotalTTC = 0;
						
						
						//Tableau analytique
						//clts
						
						
						//frs
						$totalHTperiode = 0;
						$totalTTCperiode = 0;
						$totalHTavperiode = 0;
						$totalTTCavperiode = 0;
						$totalHTapperiode = 0;
						$totalTTCapperiode = 0;
						$billing_supplier_amount_sum = 0;
						$billing_supplier_amount_sum_av = 0;
						$billing_supplier_amount_sum_ap = 0;
						
						//marge
						$rough_stroke_amount = 0;
						$billing_buyer_margin_sum = 0;
						$billing_supplier_margin_sum = 0;
						
								
						
						$billing_supplier = array();
						$billing_buyer = array();
						
						for ($i = 0; $i < $cpt; $i++) {
				
							$name = $rss->Fields("name");
							$bs = $rss->Fields("billing_supplier");
							$date = $rss->Fields("Date");
							$bsht = $rss->Fields("total_amount_ht");
							$bsttc = $rss->Fields("total_amount");
							$company = $rsb->Fields("company");
							$idbuyer = $rsb->Fields("idbuyer");
							$idbilling_buyer = $rsb->Fields("idbilling_buyer");
							$DateHeure = $rsb->Fields("DateHeure");
							$bbht = $rsb->Fields("total_amount_ht");
							$bbttc = $rsb->Fields("total_amount");
							
							if($idbilling_buyer>0)
								$billing_buyer[] = $idbilling_buyer;
								
							if( strcmp($date, $max)>0 || strcmp($date, $min ) < 0 ){
								
								if(strcmp($date,$max) > 0) 
									$class = "style=color:#FF0000;font-weight:bold;";
								
								if(strcmp($date,$min) < 0)
									$class = "style=color:#0000FF;font-weight:bold;";
								
								if(strcmp($date,$min) < 0 ) {
									$totalHTavperiode += $bsht;
									$totalTTCavperiode += $bsttc;
								}
								
								if( strcmp($date,$max) > 0 ) {
									$totalHTapperiode += $bsht;
									$totalTTCapperiode += $bsttc;
								}
								
							}else{
								$class="";
								$totalHTperiode += $bsht;
								$totalTTCperiode += $bsttc;
							}
							
							if($bs!=0 OR $bs!='')
								$billing_supplier[] = $bs;
							
							//marge brute
							$billing_supplier_amount = getBillingSupplierAmount ( $bs );
							$billing_supplier_margin_sum += $billing_supplier_amount;
							
							$billing_buyer_margin = getBillingBuyerAmount ( $idbilling_buyer );
							$billing_buyer_margin_sum += $billing_buyer_margin;
							
							if($date < $max && $date >= $min ){
								$billing_supplier_amount = getBillingSupplierAmount ( $bs );
								$billing_supplier_amount_sum += $billing_supplier_amount;
							}
							
							if($date < $min ){
								$billing_supplier_amount = getBillingSupplierAmount ( $bs );
								$billing_supplier_amount_sum_av += $billing_supplier_amount;
							}
							
							if($date > $max ){
								$billing_supplier_amount = getBillingSupplierAmount ( $bs );
								$billing_supplier_amount_sum_ap += $billing_supplier_amount;
							}
							
							?>
							<tr valign="top" class="blackText">
								<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idbuyer ?>" onclick="window.open(this.href);return false;"><?php echo $company ?></a></td>
								<td><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idbuyer ?>" onclick="window.open(this.href);return false;"><?php echo $idbuyer ?></a></td>
								<td><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $idbilling_buyer ?>" onclick="window.open(this.href);return false;"><?php echo $idbilling_buyer ?></a></td>
								<td><?php echo usDate2eu(substr( $DateHeure, 0, 10 ) ) ?></td>
								<td><?if($bbht>0) echo Util::priceFormat($bbht) ?></td>
								<td class="righterCol" style="border-right:2px solid #0000CC;"><?if($bbttc>0) echo Util::priceFormat($bbttc) ?></td>    
								<td class="lefterCol"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$rss->Fields("idsupplier")?>" onclick="window.open(this.href);return false;"><?php echo $name ?></a></td>
								<td class="supplierInvoiceCell">
								<?php
								
									if( strlen( $bs ) ){
										
										echo $bs;
										?>
										<img style="margin-top:-3px;" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/help.png" alt="" />
										<?php
										
										supplierInvoiceTip( $bs );
										
									}
									
								?>
								</td>
								<td <?=$class?>><?php echo usDate2eu($date) ?></td>
								<td><?if($bsht>0) echo Util::priceFormat($bsht) ?></td>
								<td class="righterCol"><?if($bsttc>0) echo Util::priceFormat($bsttc) ?></td>   
							</tr>
							<?php
							
							$bstotalHT += $bsht;
							$bstotalTTC += $bsttc;
							$bbtotalHT += $bbht;
							$bbtotalTTC += $bbttc;
							
							$rss->MoveNext();
							$rsb->MoveNext();
				
						}
						
						
						?>
						</tbody>
						<tfoot>
							<tr valign="top" class="blackText">
								<td colspan="4" class="lefterCol"><b>Total Factures clients</b></td>    
								<td><b><?php echo Util::priceFormat($bbtotalHT) ?> HT</b></td>
								<td class="righterCol" style="border-right:2px solid #0000CC;"><b><?php echo Util::priceFormat($bbtotalTTC) ?> TTC</b></td>
								<td colspan="3" class="lefterCol"><b>Total Factures fournisseurs</b></td>    
								<td><b><?php echo Util::priceFormat($bstotalHT) ?> HT</b></td>
								<td class="righterCol"><b><?php echo Util::priceFormat($bstotalTTC) ?> TTC</b></td>     
							</tr>
						</tfoot>
					</table>
				</div>
<?php

// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	/**
	 * commandes fournisseurs non facturées pour les factures clients émises
	 */
	
	$orderby3="ORDER BY s.name ASC";
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) && isset( $_GET[ "q" ] ) ){
		
		if($_GET[ "q" ] == 3){
			$orderby3 = "ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
		}
	}
		
	if( sizeof($billing_buyer) > 0 ){
		
		$query = "SELECT DISTINCT(bl.idorder_supplier), s.name, os.total_amount_ht, os.total_amount, os.DateHeure,s.idsupplier
				FROM order_supplier os, supplier s, bl_delivery bl
				LEFT JOIN billing_supplier_row bsr ON ( bsr.idbl_delivery = bl.idbl_delivery AND bsr.idsupplier = bl.idsupplier )
				WHERE bsr.idbl_delivery IS NULL
				AND bl.idbilling_buyer IN (
				";
				
				for($i=0;$i<sizeof($billing_buyer);$i++){
					
					if($i)
						$query .= ",";
					
					$query .= $billing_buyer[$i];
					
				}
		$query .=")
		AND bl.idorder_supplier = os.idorder_supplier
		AND os.idsupplier = s.idsupplier
		$orderby3";
		
		$rs = DBUtil::query($query);
		
		if( $rs === false )
			die ("Impossible de récupérer les commandes fournisseurs non facturées");
			
		if($rs->RecordCount()>0){?>
				<div class="headTitle" style="margin-top:15px;">
					<p class="title">Factures fournisseurs non parvenues à ce jour</p>
					<a href="#" class="slideDown" onclick="if( $(this).attr('class') == 'slideDown'){$(this).switchClass('slideUp','slideDown');}else{$(this).switchClass('slideDown','slideUp');} $('#billingSupplierNonParvenues').children('thead').toggle();$('#billingSupplierNonParvenues').children('tbody').toggle(); return false;"></a>
				</div>
				<div class="resultTableContainer clear">
					<table class="dataTable resultTable" id="billingSupplierNonParvenues">
						<thead style="display:none;">
							<tr>
								<th>Fournisseur</th>
								<th>N° Commande fournisseur</th>
								<th>Date commande</th>
								<th>Montant HT</th>
								<th>Montant TTC</th>
								<th>HT sans port</th>
							</tr>
							<tr>
								<th class="noTopBorder">
									<a href="javascript:SortResult('s.name','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('s.name','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="javascript:SortResult('bl.idorder_supplier','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('bl.idorder_supplier','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="javascript:SortResult('os.DateHeure','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('os.DateHeure','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="javascript:SortResult('os.total_amount_ht','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('os.total_amount_ht','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="javascript:SortResult('os.total_amount','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('os.total_amount','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="javascript:SortResult('os.total_amount','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="javascript:SortResult('os.total_amount','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
							</tr>
						</thead>
						<tbody style="display:none;">
					<?php
					$totalHT = 0;
					$totalTTC = 0;
					$OsMarginAmount = 0;
					
					for ($j = 0; $j < $rs->RecordCount(); $j++) {
			
						$name = $rs->Fields("name");
						$idorder_supplier = $rs->Fields("idorder_supplier");
						$date = $rs->Fields("DateHeure");
						$ht = $rs->Fields("total_amount_ht");
						$ttc = $rs->Fields("total_amount");
						
						$OsAmount = getOrderSupplierAmount ( $idorder_supplier );
						$OsMarginAmount += $OsAmount ;	
						?>
							<tr valign="top" class="blackText">
								<td class="lefterCol"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$rs->Fields("idsupplier")?>" onclick="window.open(this.href);return false;"><?php echo $name ?></a></td>
								<td><a href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier ?>"><?php echo $idorder_supplier ?></a></td>
								<td><?php echo usDate2eu(substr( $date, 0, 10 )) ?></td>
								<td><?php echo Util::priceFormat($ht) ?></td>
								<td><?php echo Util::priceFormat($ttc) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat($OsAmount) ?></td>
							</tr>
						<?php
						
						$totalHT += $ht;
						$totalTTC += $ttc;
						
						$rs->MoveNext();
						
					}
					?>
						</tbody>
						<tfoot>
							<tr valign="top" class="blackText">
								<td colspan="3" class="lefterCol"><b>Total Commandes fournisseurs</b></td>    
								<td style="color:#FF0000;font-weight:bold;" ><b><?php echo Util::priceFormat($totalHT) ?> HT</b></td>
								<td><b><?php echo Util::priceFormat($totalTTC) ?> TTC</b></td>
								<td class="righterCol"><b><?php echo Util::priceFormat($OsMarginAmount) ?> HT-Port</b></td>  
							</tr>
						</tfoot>
					</table>
				</div>
					
		<?php }
	}
	
							//Marge brute
							$rough_stroke_amount = 0;
							$rough_stroke = 0;
							
							if( $billing_buyer_margin_sum != 0 ){
								$rough_stroke_amount = $billing_buyer_margin_sum - ( $billing_supplier_margin_sum + $OsMarginAmount );
								$rough_stroke = $rough_stroke_amount / $billing_buyer_margin_sum * 100;
							}
							
// -----------------------------------------------------------------------------------------------------------------------------------------------------------
	?>
				<div class="headTitle" style="margin-top:15px;">
					<p class="title">Synthèse</p>
				</div>
				
				<div class="resultTableContainer clear">
					<?
						// somme des avoirs clients pour la période
						$totalAvoirsClt = 0;
						
						$queryCred = " SELECT
						DISTINCT( c.idcredit ),
						c.idbuyer,
						c.iduser,
						c.idbilling_buyer,
						c.DateHeure AS credit_creation_date,
						c.editable,
						ROUND( COALESCE( SUM( credit_rows.discount_price * credit_rows.quantity ), 0.0 ) - c.total_charge_ht + c.credited_charges, 2 ) AS total_amount_et,
						ROUND( COALESCE( SUM( credit_rows.discount_price * credit_rows.quantity * ( 1.0 + credit_rows.vat_rate / 100.0 ) ), 0.0 ) + (  c.credited_charges - c.total_charge_ht ) * ( 1.0 + c.vat_rate / 100.0 ), 2 ) AS total_amount_at
						FROM credits c LEFT JOIN credit_rows ON c.idcredit = credit_rows.idcredit
						WHERE $SQL_ConditionCredit
						AND c.editable = 0
						GROUP BY c.idcredit";
						
						$recreds =& DBUtil::query( $queryCred );
						
						while( !$recreds->EOF() ){
							$totalAvoirsClt += $recreds->fields( 'total_amount_at' );
							
							$recreds->MoveNext();
						}
						
						// somme des avoirs fournisseur pour la période
						$totalAvoirsFs =  DBUtil::query( "SELECT ifnull( SUM(amount) , 0 ) as sumAmounts FROM credit_supplier c WHERE $SQL_ConditionCreditFs" )->fields( 'sumAmounts' );
					?>
					<table class="dataTable resultTable" style="margin-top:15px;">
						<thead>
							<tr>
								<th colspan="2">Avoirs de la période</th>
							</tr>
							<tr>
								<th style="width:50%">Avoirs clients</th>
								<th style="width:50%">Avoirs fournisseurs</th>
							</tr>
						</thead>
						<tbody>
							<tr valign="top" class="blackText">
								<td class="lefterCol"><?php echo Util::priceFormat($totalAvoirsClt) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat($totalAvoirsFs) ?></td>
							</tr>
						</tbody>
					</table>
				</div>				
				
				<div class="resultTableContainer clear">
					<table class="dataTable resultTable" style="margin-top:15px;">
						<thead>
							<tr>
								<th colspan="2">Factures clients de la période</th>
							</tr>
							<tr>
								<th style="width:50%">Période HT avec port</th>
								<th style="width:50%">Période HT sans port</th>
							</tr>
						</thead>
						<tbody>
							<tr valign="top" class="blackText">
								<td class="lefterCol"><?php echo Util::priceFormat($bbtotalHT) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat($billing_buyer_margin_sum) ?></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="resultTableContainer clear">
					<table class="dataTable resultTable" style="margin-top:15px;">
						<thead>
							<tr>
								<th colspan="8">Factures fournisseurs</th>
							</tr>
							<tr>
								<th colspan="4">HT avec port</th>
								<th colspan="4">HT sans port</th>
							</tr>
							<tr>
								<th>Factures<br/>de la période</th>
								<th>CCA<br/>Charges constatées d'avance</th>
								<th>FNP<br/>Factures Fourn. à parvenir</th>
								<th>Total</th>
								<th>Factures<br/>de la période</th>
								<th>CCA<br/>Charges constatées d'avance</th>
								<th>FNP<br/>Factures Fourn. à parvenir</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr valign="top" class="blackText">
								<td class="lefterCol"><?php echo Util::priceFormat($totalHTperiode) ?></td>
								<td style="color:#0000FF;font-weight:bold;"><?php echo Util::priceFormat($totalHTavperiode) ?></td>
								<td style="color:#FF0000;font-weight:bold;"><?php echo Util::priceFormat($totalHTapperiode+$totalHT) ?></td>
								<td><?php echo Util::priceFormat($totalHTperiode+$totalHTavperiode+$totalHTapperiode+$totalHT) ?></td>
								<td><?php echo Util::priceFormat($billing_supplier_amount_sum) ?></td>
								<td style="color:#0000FF;font-weight:bold;"><?php echo Util::priceFormat($billing_supplier_amount_sum_av) ?></td>
								<td style="color:#FF0000;font-weight:bold;"><?php echo Util::priceFormat($billing_supplier_amount_sum_ap+$OsMarginAmount) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat($billing_supplier_amount_sum+$billing_supplier_amount_sum_av+$billing_supplier_amount_sum_ap+$OsMarginAmount) ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div class="resultTableContainer clear">
					<table class="dataTable resultTable" style="margin-top:15px;">
						<thead>
							<tr>
								<th colspan="2">Marge brute période</th>
							</tr>
							<tr>
								<th style="width:50%">%</th>
								<th style="width:50%">Montant</th>
							</tr>
						</thead>
						<tbody>
							<tr valign="top" class="blackText">
								<td class="lefterCol"><?php echo Util::rateFormat($rough_stroke) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat($rough_stroke_amount) ?></td>
							</tr>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
<?php
	
}

//------------------------------------------------------------------------------------------

function getNotInvoicedOrders(){

	global $GLOBAL_START_URL, $billing_supplier;
	
	/**
	 * commandes clients non facturées pour les factures fournisseurs réceptionnées
	 */
	
	$orderby3="ORDER BY b.company ASC";
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) && isset( $_GET[ "q" ] ) ){
		
		if($_GET[ "q" ] == 3){
			$orderby3 = "ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
		}
	}
		
	if( sizeof($billing_supplier) > 0 ){
		
		$query = "SELECT DISTINCT(bl.idorder), b.company, o.total_amount_ht, o.total_amount, o.conf_order_date
				FROM `order` o, buyer b, bl_delivery bl
				LEFT JOIN billing_supplier_row bsr ON( bsr.idbl_delivery = bl.idbl_delivery AND bsr.idsupplier = bl.idsupplier )
				WHERE bsr.idbl_delivery IS NULL
				AND bsr.billing_supplier IN (
				";
				
				for($i=0;$i<sizeof($billing_supplier);$i++){
					
					if($i)
						$query .= ",";
					
					$query .= "'$billing_supplier[$i]'";
					
				}
		$query .=")
		AND bl.idorder = o.idorder
		AND b.idbuyer = bl.idbuyer
		AND o.status LIKE 'Ordered'
		$orderby3";
		
		$rs = DBUtil::query($query);
		
		if( $rs === false )
			die ("Impossible de récupérer les commandes clients non facturées");
			
		if($rs->RecordCount()>0){?>
			<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title">Commandes clients non facturées / Factures fournisseurs réceptionnées</p>
					</div>
					<div class="subContent">
						<div class="tableHeaderLeft"></div>
						<div class="tableHeaderRight"></div>	
						<div class="resultTableContainer clear">
							<table class="dataTable resultTable">
								<thead>
								<tr>
									<th>Client</th>
									<th>N° Commande Client</th>
									<th>Date de confirmation de commande</th>
									<th>Montant HT</th>
									<th>Montant TTC</th>
								</tr>
								<tr>
									<th class="noTopBorder">
										<a href="javascript:SortResult('s.name','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('s.name','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('bl.idorder_supplier','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('bl.idorder_supplier','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('os.DateHeure','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('os.DateHeure','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('os.total_amount_ht','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('os.total_amount_ht','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('os.total_amount','ASC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('os.total_amount','DESC',3)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$totalHT = 0;
							$totalTTC = 0;
							
							for ($j = 0; $j < $rs->RecordCount(); $j++) {
					
								$company = $rs->Fields("company");
								$idorder = $rs->Fields("idorder");
								$date = $rs->Fields("conf_order_date");
								$ht = $rs->Fields("total_amount_ht");
								$ttc = $rs->Fields("total_amount");
									
								?>
								<tr valign="top" class="blackText">
									<td class="lefterCol"><?php echo $company ?></td>    
									<td><a href="<?=$GLOBAL_START_URL?>/sales_force/com_admin_order.php?IdOrder=<?php echo $idorder ?>"><?php echo $idorder ?></a></td>
									<td><?php echo usDate2eu(substr( $date, 0, 10 )) ?></td>
									<td><?php echo Util::priceFormat($ht) ?></td>
									<td class="righterCol"><?php echo Util::priceFormat($ttc) ?></td> 
								</tr>
								<?php
								
								$totalHT += $ht;
								$totalTTC += $ttc;
								
								$rs->MoveNext();
								
							}
							?>
								<tr valign="top" class="blackText">
									<td colspan="3" class="lefterCol"><b>Total Commandes clients</b></td>    
									<td><b><?php echo Util::priceFormat($totalHT) ?></b></td>
									<td class="righterCol"><b><?php echo Util::priceFormat($totalTTC) ?></b></td>  
								</tr>
							</tbody>
						</table>
					</div>
					</div>
						</div>
						<div class="bottomRight"></div><div class="bottomLeft"></div>
						</div>	
		<?php }
	}
}

//------------------------------------------------------------------------------------------
function getLatestBillingsBuyer($min, $max){

	global $GLOBAL_START_URL, $billing_supplier;
	
	/**
	 * factures clients facturées hors période pour les factures fournisseurs émises
	 */
	
	$orderby4="ORDER BY b.company ASC";
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) && isset( $_GET[ "q" ] ) ){
		
		if($_GET[ "q" ] == 4){
			$orderby3 = "ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
		}
	}
		
	if( sizeof($billing_supplier) > 0 ){
		
		$query = "SELECT DISTINCT(bl.idbilling_buyer), b.company, bb.total_amount_ht, bb.total_amount, bb.DateHeure
				FROM bl_delivery bl, billing_buyer bb, buyer b, billing_supplier_row bsr
				WHERE bb.idbuyer = b.idbuyer
				AND bsr.idbl_delivery = bl.idbl_delivery
				AND bsr.idsupplier = bl.idsupplier
				AND bsr.billing_supplier IN (
				";
				
				for($i=0;$i<sizeof($billing_supplier);$i++){
					
					if($i)
						$query .= ",";
					
					$query .= "'.$billing_supplier[$i].'";
					
				}
		$query .=")
		AND bl.idbilling_buyer = bb.idbilling_buyer
		AND ( bb.DateHeure < '$min' OR bb.DateHeure > '$max' )
		GROUP BY bsr.billing_supplier, bsr.idsupplier
		$orderby4";
		
		$rs = DBUtil::query($query);
		
		if( $rs === false )
			die ("Impossible de récupérer les factures clients facturées hors période");
			
		if($rs->RecordCount()>0){?>
			<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title">Factures clients facturées hors période / Factures fournisseurs réceptionnées</p>
					</div>
					<div class="subContent">
						<div class="tableHeaderLeft"></div>
						<div class="tableHeaderRight"></div>	
						<div class="resultTableContainer clear">
							<table class="dataTable resultTable">
								<thead>
								<tr>
									<th>Client</th>
									<th>N° Facture client</th>
									<th>Date facture</th>
									<th>Montant HT</th>
									<th>Montant TTC</th>
								</tr>
								<tr>
									<th class="noTopBorder">
										<a href="javascript:SortResult('s.name','ASC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('s.name','DESC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('bl.billing_supplier','ASC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('bl.billing_supplier','DESC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('bs.Date','ASC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('bs.Date','DESC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('bs.total_amount_ht','ASC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('bs.total_amount_ht','DESC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="javascript:SortResult('bs.total_amount','ASC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="javascript:SortResult('bs.total_amount','DESC',4)"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
								</tr>
							</thead>
							<tbody>
							<?php
							$totalHT = 0;
							$totalTTC = 0;
							
							for ($j = 0; $j < $rs->RecordCount(); $j++) {
					
								$company = $rs->Fields("company");
								$idbilling_buyer = $rs->Fields("idbilling_buyer");
								$date = $rs->Fields("DateHeure");
								$ht = $rs->Fields("total_amount_ht");
								$ttc = $rs->Fields("total_amount");
									
								?>
								<tr valign="top" class="blackText">
									<td class="lefterCol"><?php echo $company ?></td>
									<td><?php echo $idbilling_buyer ?></td>
									<td><?php echo usDate2eu( $date ) ?></td>
									<td><?php echo Util::priceFormat($ht) ?></td>
									<td class="righterCol"><?php echo Util::priceFormat($ttc) ?></td>
								</tr>
								<?php
								
								$totalHT += $ht;
								$totalTTC += $ttc;
								
								$rs->MoveNext();
								
							}
							?>
								<tr valign="top" class="blackText">
									<td colspan="3" class="lefterCol"><b>Total Factures clients</b></td>    
									<td><b><?php echo Util::priceFormat($totalHT) ?></b></td>
									<td class="righterCol"><b><?php echo Util::priceFormat($totalTTC) ?></b></td>
								</tr>
							</tbody>
						</table>
					</div>
					</div>
						</div>
						<div class="bottomRight"></div><div class="bottomLeft"></div>
						</div>	
		<?php }
	}
}

//------------------------------------------------------------------------------------------
function getBillingSupplierAmount ( $bs ){
	
	if( empty( $bs ) )
		return 0.00;
	
	$query = "
	SELECT SUM( osr.discount_price * osr.quantity ) AS discount_price, os.total_discount
	FROM bl_delivery bl, bl_delivery_row blr, order_supplier_row osr, order_supplier os, billing_supplier_row bsr
	WHERE bsr.billing_supplier = '$bs'
	AND bsr.idbl_delivery = blr.idbl_delivery
	AND bsr.idrow = blr.idrow
	AND blr.idbl_delivery = bl.idbl_delivery
	AND bl.idorder_supplier = os.idorder_supplier
	AND os.idorder_supplier = osr.idorder_supplier
	AND osr.idorder_row = blr.idorder_row
	GROUP BY bsr.billing_supplier, bsr.idsupplier";

	$rs =& DBUtil::query( $query );
		
	if( $rs === false )
		die("Impossible de récupérer le montant des lignes articles pour la facture fournisseur $bs");
		
	return $rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100);
	
}

//------------------------------------------------------------------------------------------
function getBillingBuyerAmount ( $bb ){
	
	if( empty( $bb ) )
		return 0.00;
		
	$query = "SELECT SUM( bbr.discount_price * bbr.quantity ) AS discount_price, bb.total_discount
		FROM billing_buyer_row bbr, billing_buyer bb
		WHERE bbr.idbilling_buyer = $bb
		AND bb.idbilling_buyer = $bb
		GROUP BY bb.idbilling_buyer";
		
	$rs =& DBUtil::query( $query );
		
	if( $rs === false )
		die("Impossible de récupérer le montant des lignes articles pour la facture client $bb");
			
	return $rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100);
	
}
//------------------------------------------------------------------------------------------
function getOrderSupplierAmount ( $os ){
	
	if( empty( $os ) )
		return 0.00;
		
	$query = "SELECT SUM( osr.discount_price * osr.quantity ) AS discount_price, os.total_discount
		FROM order_supplier_row osr, order_supplier os
		WHERE osr.idorder_supplier = $os
		AND os.idorder_supplier = osr.idorder_supplier
		GROUP BY os.idorder_supplier";
		
	$rs =& DBUtil::query( $query );
		
	if( $rs === false )
		die("Impossible de récupérer le montant des lignes articles pour la commande fournisseur $os");
			
	return $rs->fields( "discount_price" )*(1-$rs->fields( "total_discount" )/100);
	
}

//------------------------------------------------------------------------------------------
function supplierInvoiceTip( $billing_supplier ){
	
	$query = "
	SELECT s.name
	FROM supplier s, billing_supplier bs
	WHERE bs.billing_supplier = '" . Util::html_escape( $billing_supplier ) . "'
	AND bs.idsupplier = s.idsupplier
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	?>
	<div id="SupplierInvoiceTip<?php echo $billing_supplier; ?>" class="supplierInvoiceTip" style="background-color:#FFFFFF; border:5px solid #EB6A0A; border-radius:5px; display:none; margin-left:-820px; margin-top:6px; padding:10px; position:absolute; width:800px; -moz-border-radius:5px; -webkit-border-radius:5px;">
		<div class="headTitle" style="margin-bottom:10px;">
			<p class="title">Facture Fournisseur <?php echo htmlentities( $billing_supplier ); ?> - <?php echo htmlentities( $rs->fields( "name" ) ); ?></p>
		</div>
		<?php
	
	/* factures client */
	
	$query = "
	SELECT GROUP_CONCAT( DISTINCT bl.idbilling_buyer ) AS idbilling_buyer, GROUP_CONCAT( DISTINCT bl.idorder ) AS idorder
	FROM bl_delivery bl, billing_supplier_row bsr
	WHERE bsr.billing_supplier = '" . Util::html_escape( $billing_supplier ) . "'
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	GROUP BY bsr.billing_supplier, bsr.idsupplier";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucune facture client</p>
		<?php
		
	}
	else listInvoices( explode( ",", $rs->fields( "idbilling_buyer" ) ) );
	
	/* autres factures fournisseur */

	$query = "
	SELECT DISTINCT( bsr.billing_supplier )
	FROM bl_delivery bl
	LEFT JOIN billing_supplier_row bsr ON ( bsr.idbl_delivery = bl.idbl_delivery AND  bsr.idsupplier = bl.idsupplier )
	WHERE bsr.idbl_delivery IS NOT NULL
	AND bsr.billing_supplier <> '" . Util::html_escape( $billing_supplier ) . "'
	AND bl.idorder IN( " . $rs->fields( "idorder" ) . " )
	GROUP BY bsr.billing_supplier, bsr.idsupplier";
	
	$rs =& DBUtil::query( $query );
	
	$invoices = array();
	
	while( !$rs->EOF() ){
		
		array_push( $invoices, $rs->fields( "billing_supplier" ) );
		$rs->MoveNext();
		
	}
	
	if( count( $invoices ) )
		listSupplierInvoices( $invoices );

	?>
	</div>
	<?php
	
}

//------------------------------------------------------------------------------------------

function listInvoices( array $invoices ){
	
	?>
	<div class="subTitleContainer">
		<p class="subTitle">Factures client</p>
	</div>
	<table class="dataTable resultTable">
		<thead>
			<tr>
				<th>Facture n°</th>
				<th>Client n°</th>
				<th>Nom / Raison sociale</th>
				<th>Code postal</th>
				<th>Date de création</th>
				<th>Date d'échéance</th>
				<th>Statut</th>
				<th>Client n°</th>
				<th>Total HT</th>
				<th>Total TTC</th>
				</tr>
			</thead>
			<tbody>
			<?php

				$total_amount_htSum = 0.0;
				$total_amountSum	= 0.0;
				
				foreach( $invoices as $invoice ){
					
					$rs =& DBUtil::query( "
					SELECT billing_buyer.idbilling_buyer,
						buyer.idbuyer,
						buyer.company,
						buyer.zipcode,
						billing_buyer.total_amount,
						billing_buyer.total_amount_ht,
						billing_buyer.status,
						billing_buyer.DateHeure,
						billing_buyer.deliv_payment
					FROM billing_buyer, buyer
					WHERE buyer.idbuyer = billing_buyer.idbuyer
					AND billing_buyer.idbilling_buyer = $invoice" );
					
					if( $rs === false )
						trigger_error( "Impossible de récupérer la liste des factures", E_USER_ERROR );
	
					?>
					<tr>
						<td class="lefterCol" style="background-color:#FFFFFF;"><b><?php echo $rs->fields( "idbilling_buyer" ) ?></b></td>
						<td style="background-color:#FFFFFF;"><?php echo $rs->fields( "idbuyer" ) ?></td>
						<td style="background-color:#FFFFFF;"><?php echo htmlentities( $rs->fields( "company" ) ) ?></td>
						<td style="background-color:#FFFFFF;"><?php echo $rs->fields( "zipcode" ) ?></td>
						<td style="background-color:#FFFFFF;"><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ), false ) ?></td>
						<td style="background-color:#FFFFFF;"><?php echo Util::dateFormatEu( $rs->fields( "deliv_payment" ) ) ?></td>
						<td style="background-color:#FFFFFF;"><?php echo Dictionnary::translate( $rs->fields( "status" ) ) ?></td>
						<td style="background-color:#FFFFFF;"><?php echo $rs->fields( "idbuyer" ) ?></td>
						<td style="background-color:#FFFFFF; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
						<td class="righterCol" style="background-color:#FFFFFF; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
					</tr>
					<?php
					
					$total_amount_htSum += round( $rs->fields( "total_amount_ht" ), 2 );
					$total_amountSum 	+= round( $rs->fields( "total_amount" ), 2 );
				}

			?>
			<tr>
				<td class="lefterCol" style="background-color:#FFFFFF;" colspan="8"></td>
				<td style="background-color:#FFFFFF; text-align:right;"><b><?php echo Util::priceFormat( $total_amount_htSum ) ?></b></td>
				<td class="righterCol" style="background-color:#FFFFFF; text-align:right;"><b><?php echo Util::priceFormat( $total_amountSum ) ?></b></td>
			</tr>
		</tbody>
	</table>
	<?php
	
}

//------------------------------------------------------------------------------------------

function listSupplierInvoices( array $invoices ){
	
	?>
	<div class="subTitleContainer" style="margin-top:10px;">
		<p class="subTitle">Autres Factures fournisseur issues des mêmes commandes client</p>
	</div>
	<table class="dataTable resultTable">
		<thead>
			<tr>
				<th>Cdes Client n°</th>
				<th>Client n°</th>
				<th>Cdes Fournisseur n°</th>
				<th>Total Lignes<br />Article HT</th>
				<th>Facture Fournisseur n°</th>
				<th>Fournisseur</th>
				<th>Date de facturation</th>
				<th>Port HT</th>
				<th>Total HT</th>
				<th>Total TTC</th>
			</tr>
		</thead>
		<tbody>
		<?php

			$total_amount_htSum = 0.0;
			$total_amountSum	= 0.0;
			$total_charge_htSum = 0.0;
			$itemsETSum 		= 0.0;
			
			foreach( $invoices as $billing_supplier ){
				
				$query = "
				SELECT bs.*, s.name, 
				GROUP_CONCAT( DISTINCT bl.idorder ) AS idorder, 
				GROUP_CONCAT( DISTINCT bl.idorder_supplier ) AS idorder_supplier,
				GROUP_CONCAT( DISTINCT bl.idbuyer ) AS idbuyer
				FROM billing_supplier bs, billing_supplier_row bsr, supplier s, bl_delivery bl
				WHERE bs.billing_supplier = '$billing_supplier'
				AND bs.billing_supplier = bsr.billing_supplier
				AND bs.idsupplier = bsr.idsupplier
				AND bsr.idbl_delivery = bl.idbl_delivery
				AND bsr.idsupplier = bl.idsupplier
				AND s.idsupplier = bsr.idsupplier
				GROUP BY bsr.billing_supplier, bsr.idsupplier";

				$rs =& DBUtil::query( $query );
				
				if( $rs === false )
					trigger_error( "Impossible de récupérer la liste des factures", E_USER_ERROR );

				?>
				<tr>
					<td class="lefterCol" style="background-color:#FFFFFF;"><?php echo str_replace( ",", "<br />", $rs->fields( "idorder" ) ) ?></td>
					<td style="background-color:#FFFFFF;"><?php echo str_replace( ",", "<br />", $rs->fields( "idbuyer" ) ) ?></td>
					<td style="background-color:#FFFFFF;"><?php echo str_replace( ",", "<br />", $rs->fields( "idorder_supplier" ) ) ?></td>
					<td style="background-color:#FFFFFF; text-align:right;">
					<?php
					
						$supplierOrders = explode( ",", $rs->fields( "idorder_supplier" ) );
						
						$linebreak = "";
						foreach( $supplierOrders as $idorder_supplier ){
							
							$itemsET = getSupplierOrderItemsSum( $idorder_supplier );
							$itemsETSum += $itemsET;
							
							echo $linebreak . Util::priceFormat( $itemsET );
							$linebreak = "<br />";
							
						}
						
					?>
					</td>
					<td style="background-color:#FFFFFF;"><b><?php echo $rs->fields( "billing_supplier" ) ?></b></td>
					<td style="background-color:#FFFFFF;"><?php echo htmlentities( $rs->fields( "name" ) ) ?></td>
					<td style="background-color:#FFFFFF;"><?php echo Util::dateFormatEu( $rs->fields( "Date" ) ) ?></td>
					<td style="background-color:#FFFFFF; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_charge_ht" ) ) ?></td>
					<td style="background-color:#FFFFFF; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
					<td class="righterCol" style="background-color:#FFFFFF; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
				</tr>
				<?php
				
				$total_amount_htSum += round( $rs->fields( "total_amount_ht" ), 2 );
				$total_amountSum 	+= round( $rs->fields( "total_amount" ), 2 );
				$total_charge_htSum += round( $rs->fields( "total_charge_ht" ), 2 );
				
			}

			?>
			<tr>
				<td class="lefterCol" style="background-color:#FFFFFF;" colspan="3"></td>
				<td style="background-color:#FFFFFF; text-align:right;"><b><?php echo Util::priceFormat( $itemsETSum ) ?></b></td>
				<td style="background-color:#FFFFFF; text-align:right;" colspan="3"></td>
				<td style="background-color:#FFFFFF; text-align:right;"><b><?php echo Util::priceFormat( $total_charge_htSum ) ?></b></td>
				<td style="background-color:#FFFFFF; text-align:right;"><b><?php echo Util::priceFormat( $total_amount_htSum ) ?></b></td>
				<td class="righterCol" style="background-color:#FFFFFF; text-align:right;"><b><?php echo Util::priceFormat( $total_amountSum ) ?></b></td>
			</tr>
		</tbody>
	</table>
	<?php
	
}

//------------------------------------------------------------------------------------------

function getSupplierOrderItemsSum( $idorder_supplier ){
	
	$query = "
	SELECT SUM( quantity * ROUND( discount_price, 2 ) ) AS `sum`
	FROM order_supplier_row
	WHERE idorder_supplier = '$idorder_supplier'";
	
	$rs =& DBUtil::query( $query );
	
	return $rs->fields( "sum" );
	
}

//------------------------------------------------------------------------------------------

?>