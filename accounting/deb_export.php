<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Export DEB
 */
 

define( "FILLER", " " );
define( "INTRODUCTION", 1 );
define( "EXPEDITION", 2 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );

if( !isset( $_POST[ "declare" ] ) || !count( $_POST[ "declare" ] ) )
	exit();

/* ---------------------------------------------------------------------------------------------------------------------------- */
/* premier enregistrement */

$pOffset = 1;

//1 type de fichier
append( 1, 8, "INTRACOM" );

//9 N° d'habilité
append( 9, 4, DBUtil::getParameterAdmin( "NUMERO_D_HABILITE_DEB" ) );

//13 période de référence - mois
list( $year, $month ) = explode( "-", $_POST[ "month" ] );
append( 13, 2, $month );

//15 flux : 1 pour INTRODUCTION ou IMPORTATION, 2 pour EXPEDITION ou EXPORTATION, 3 pour fichier mixte
append( 15, 1, "3" );

//16 N° de séquence ( nombre d'envois pour une même déclaration )
append( 16, 1, "1" );

//17 FILLER
append( 17, 7, filler( 7 ) );

//24 date de construction du fichier
append( 24, 4, date( "my" ) );

//28 siren ou siret
append( 28, 14, str_replace( " ", "", DBUtil::getParameterAdmin( "ad_siret" ) ) );

//42 N° télécopie cadré à gauche
$fax = str_replace( " ", "", DBUtil::getParameter( "ad_fax" ) );
append( 42, 20, $fax . filler( 20 - strlen( $fax ) ) );

//62 N° téléphone cadré à gauche
$phone = str_replace( " ", "", DBUtil::getParameter( "ad_tel" ) );
append( 62, 20, $fax . filler( 20 - strlen( $phone ) ) );

//82 nom du correspondant
$name = substr( DBUtil::getParameterAdmin( "ad_responsable" ), 0, 20 );
append( 82, 20, $name . filler( 20 - strlen( $name ) ) );

//102 filler
append( 102, 35, filler( 35 ) );

/* ---------------------------------------------------------------------------------------------------------------------------- */
/* enregistrements */

$lineNumber = 1;

foreach( $_POST[ "declare" ] as $value ){

	echo "\n";

	$pOffset = 1;
	
	list( $billing_supplier, $idsupplier, $idbl_delivery, $idrow ) = explode( ",", $value );
	
	$query = "
	SELECT IF( bl.iddelivery, delivery.zipcode, b.zipcode ) AS zipcode,
		IF( bl.iddelivery, delivery.idstate, b.idstate ) AS destinationIdState,
		s.idstate AS sourceIdState,
		osr.quantity,
		GREATEST( 1.0, osr.quantity * osr.discount_price ) AS totalET,
		osr.quantity * osr.weight AS totalWeight,
		d.code_customhouse,
		bl.dispatch_date,
		b.tva AS customerVATCode
	FROM detail d, supplier s, state st, order_supplier_row osr, order_supplier os, buyer b, bl_delivery_row blr, bl_delivery bl
	LEFT JOIN delivery ON delivery.iddelivery = bl.iddelivery
	WHERE bl.idbl_delivery = '$idbl_delivery'
	AND blr.idbl_delivery = bl.idbl_delivery
	AND blr.idrow = '$idrow'
	AND b.idbuyer = bl.idbuyer
	AND b.idcustomer_profile > 1
	AND bl.idsupplier = s.idsupplier
	AND osr.idorder_supplier = bl.idorder_supplier
	AND osr.idarticle = blr.idarticle
	AND os.idorder_supplier = osr.idorder_supplier
	AND d.idarticle = osr.idarticle
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	/* vérifications */
	
	if( !$rs->RecordCount() )
		trigger_error( "Enregistrement non exportable ( BL n° $idbl_delivery, ligne n° $idrow )" . $rs->fields( "idorder_supplier" ), E_USER_ERROR ) && exit();
		
	if( !strlen( $rs->fields( "code_customhouse" ) ) )
		trigger_error( "Tous les codes douane n'ont pas été renseignés dans la commande fournisseur n° " . $rs->fields( "idorder_supplier" ), E_USER_ERROR ) && exit();
		
	if( $rs->fields( "dispatch_date" ) == "0000-00-00" )
		trigger_error( "La date d'expédition de la marchandise n'est pas renseignée dans la commande fournisseur n° " . $rs->fields( "idorder_supplier" ), E_USER_ERROR ) && exit();
	
	if( 1 == $rs->fields( "sourceIdState" ) && $rs->fields( "sourceIdState" ) == $rs->fields( "destinationIdState" ) )
		trigger_error( "L'enregistrement ne correspond pas à une importation ou une exportation", E_USER_ERROR ) && exit();

	if( strlen( $rs->fields( "code_customhouse" ) ) != 8 )
		trigger_error( "Le code douane renseigné ne respecte la nommenclature NC8", E_USER_ERROR ) && exit();

	if( $rs->fields( "destinationIdState" ) == 1 && $rs->fields( "sourceIdState" ) != 1 ) //INTRODUCTION
		appendLine( $rs->fields, $lineNumber, INTRODUCTION );
	else if( $rs->fields( "destinationIdState" ) != 1 && $rs->fields( "sourceIdState" ) == 1 ) //EXPEDITION
		appendLine( $rs->fields, $lineNumber, EXPEDITION );
	else if( $rs->fields( "destinationIdState" ) != 1 && $rs->fields( "sourceIdState" ) != 1 ){ //INTRODUCTION + EXPEDITION
		
		appendLine( $rs->fields, $lineNumber, INTRODUCTION );
		$pOffset = 1;
		appendLine( $rs->fields, $lineNumber, EXPEDITION );

	}
	else trigger_error( "Seules les introductions et les expéditions peuvent être exportées", E_USER_ERROR ) && exit();
	
	$lineNumber++;
	
}

/* ---------------------------------------------------------------------------------------------------------------------------- */
/**
 * @param array $data
 * @param int $lineNumber
 * @param int $code INTRODUCTION ou EXPEDITION
 * @return void
 */
function appendLine( $data, $lineNumber, $code ){
	
	//1 filler
	append( 1, 4, filler( 4 ) );
	
	//5 code flux ( 1 : INTRODUCTION, 2 : EXPEDITION )
	append( 5, 1, $code );
	
	//6 indicateur de monnaie de déclaration
	append( 6, 1, "E" );
	
	//7 N° déclaration cadrée à droite, zero filled
	list( $year, $month ) = explode( "-", $_POST[ "month" ] );
	append( 7, 6, "{$year}{$month}" );
	
	//13 numéro de ligne cadré à droite, zero filled
	append( 13, 6, sprintf( "%06d", $lineNumber ) );
	
	//19 filler
	append( 19, 4, filler( 4 ) );
	
	//23 useless
	append( 23, 2, filler( 2 ) );
	
	//25 siren de l'entreprise
	append( 25, 9, substr( str_replace( " ", "", DBUtil::getParameterAdmin( "ad_siret" ) ), 0, 9 ) );
	
	//34 filler
	append( 34, 5, filler( 5 ) );
	
	//39 département
	append( 39, 2, filler( 2 ) );
	
	//41 mode de transport à la frontière
	append( 41, 1, filler( 1 ) );
	
	//42 filler
	append( 42, 1, filler( 1 ) );

	//43 pays de provenance
	if( $code == INTRODUCTION )
		append( 43, 2, substr( DBUtil::getDBValue( "iso_code", "state", "idstate", $data[ "sourceIdState" ] ), 0, 2 ) );
	else append( 43, 2, substr( DBUtil::getDBValue( "iso_code", "state", "idstate", 1 ), 0, 2 ) );
	
	//45 nature de la transaction ( 11 = acquisition intracommunautaire taxable en France )
	append( 45, 2, "11" );
	
	//47 valeur fiscale
	append( 47, 11, sprintf( "%-11d", round( $data[ "totalET" ], 0 ) ) );
	
	//58 conditions de livraison
	append( 58, 4, filler( 4 ) );
	
	//62 régime
	if( $code == INTRODUCTION )
			append( 62, 2, "11" ); //11 : acquisition intracommunautaire taxable en France
	else 	append( 62, 2, "21" ); //21 : Livraison exonérée et transfert
	
	//64 niveau d'obligation : 2
	append( 64, 1, "2" );
	
	//65 nommenclature
	append( 65, 8, sprintf( "%+8s", $data[ "code_customhouse" ] ) );
	
	//73 développement statistique NPG
	append( 73, 1, "0" );

	//74 masse nette
	append( 74, 10, sprintf( "%-10d", $data[ "totalWeight" ] > 0.5 ? round( $data[ "totalWeight" ], 0 ) : 0 ) );
	
	//84 valeur statistique de l'article : facultatif sauf exception
	append( 84, 11, filler( 11 ) );
	
	//95 unités supplémentaires
	append( 95, 10, sprintf( "%-10d", $data[ "quantity" ] ) );
	
	//105 filler
	append( 105, 1, filler( 1 ) );
	
	//106 pays d'origine à l'introduction ou de destination finale à l'expédition
	if( $code == INTRODUCTION )
			append( 106, 2, substr( DBUtil::getDBValue( "iso_code", "state", "idstate", $data[ "sourceIdState" ] ), 0, 2 ) );
	else 	append( 106, 2, substr( DBUtil::getDBValue( "iso_code", "state", "idstate", $data[ "destinationIdState" ] ), 0, 2 ) );

	//108 code TVA du partenaire étranger, uniquement à l'expédition
	if( $code == EXPEDITION ){

		$tva = substr( str_replace( " ", "", $data[ "customerVATCode" ] ), 0, 15 );
		append( 108, 15, $tva . filler( 15 - strlen( $tva ) ) );
			
	}
	else append( 108, 15, filler( 15 ) );

	//123 filler
	append( 123, 1, filler( 1 ) );
	
	//124 période de référence ( unique pour tout le document )
	list( $year, $month ) = explode( "-", $_POST[ "month" ] );
	append( 124, 6, $month . $year );
	
	//130 filler
	append( 130, 7, filler( 7 ) );
	
}

/* ------------------------------------------------------------------------------------------- */
/**
 * @param $offset, 1 based
 * @param $length
 * @param $text
 * @return void
 */
function append( $offset, $length, $text ){

	global $pOffset;
	
	if( $pOffset != $offset )
		trigger_error( "Format de fichier non valide à l'offset $offset", E_USER_ERROR ) && exit();
	
	if( strlen( $text ) != $length )
		trigger_error( "Enregistrement non valide à l'offset $offset : $text ( taille attendue : $length )", E_USER_ERROR ) && exit();

	echo $text;

	$pOffset += $length;
	
}

/* ------------------------------------------------------------------------------------------- */

function filler( $count ){ 
	
	$filler = "";
	
	$i = 0;
	while( $i < $count ){

		$filler .= FILLER;
		$i++;
		
	}
	
	return $filler;
	
}

/* ------------------------------------------------------------------------------------------- */

?>