<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Script de mise à jour des factures clients avec règlmeents enregistrés en avance
 */



include_once( "../objects/classes.php" );


//Sélection des règlements non validés

$query ="SELECT idregulations_buyer
		FROM regulations_buyer
		WHERE accept=0
		AND payment_date <= NOW()";
$rs = DBUtil::query($query);

if( $rs === false )
	die ( "Impossible de récupérer les paiements clients non validés" );

$update_regulations = 0;
$update_billing = 0;
	
for ($i=0;$i<$rs->RecordCount();$i++) {

	//On met à jour les règlements en question
	$idregulations_buyer = $rs->fields( "idregulations_buyer" );
	
	$query = "UPDATE regulations_buyer
			SET accept = 1
			WHERE idregulations_buyer = $idregulations_buyer";
	$rsupd = DBUtil::query($query);
	
	if( $rsupd === false )
		die ( "Impossible de mettre à jour les paiements clients non validés" );
			
	$update_regulations++;
	
	//On met à jour le statut de la facture à payé si tous les règlements sont validés
	
	//1. On récupère le ou les numéros de factures pour ce règlement
	
	$query = "SELECT idbilling_buyer
			FROM billing_regulations_buyer
			WHERE idregulations_buyer = $idregulations_buyer";
	$rsbil = DBUtil::query($query);	
	
	if( $rsbil === false )
		die ( "Impossible de récupérer la ou les factures pour le paiement" );
		
	for ($j=0;$j<$rsbil->RecordCount();$j++) {
		
		//2. On récupère les règlements de chaque facture
		
		$idbilling_buyer = $rsbil->fields("idbilling_buyer");
		
		$query = "SELECT idregulations_buyer
				FROM billing_regulations_buyer
				WHERE idbilling_buyer = $idbilling_buyer";
		$rsregul = DBUtil::query($query);
		
		if( $rsregul === false )
			die ( "Impossible de récupérer les règlements pour la facture $idbilling_buyer" );	
		
		$regulations = $rsregul->RecordCount();
		
		//3. On récupère tous les règlements acceptés de chaque facture
		
		$query = "SELECT brb.idregulations_buyer
				FROM billing_regulations_buyer brb, regulations_buyer rb
				WHERE brb.idbilling_buyer = $idbilling_buyer
				AND rb.accept=1
				AND brb.idregulations_buyer = rb.idregulations_buyer";
		$rsaccept = DBUtil::query($query);
		
		if( $rsaccept === false )
			die ( "Impossible de récupérer les règlements acceptés pour la facture $idbilling_buyer" );	
		
		$accept = $rsaccept->RecordCount();
		
		//4. Si le nombre de règlements total correspond au nombre de règlements acceptés pour cette facture, on passe le statut en payé
		
		if( $accept==$regulations ){
		
			$status = Invoice::$STATUS_PAID;
			
			$query = "UPDATE billing_buyer
					SET status = '$status'
					WHERE idbilling_buyer = $idbilling_buyer";
			$rsupd = DBUtil::query($query);
	
			if( $rsupd === false )
				die ( "Impossible de mettre à jour la facture $idbilling_buyer" );
			
			$update_billing++;	
		}
		
		$rsbil->MoveNext();
	} 
	
	
	
	$rs->MoveNext();
}

echo "<b>Résultat du traitement : </b><br /><br />";
echo "<b>Nombre de paiements mis à jour : </b>".$update_regulations."<br /><br />";
echo "<b>Nombre de factures mises à jour : </b>".$update_billing."<br /><br />";

?>