<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Règlements partiels en retard
 */

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type: text/html; charset=utf-8");

//------------------------------------------------------------------------------------------------
include_once("../objects/classes.php");
include_once("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
include_once("$GLOBAL_START_PATH/objects/SupplierInvoice.php");

$Title = "Règlements partiels en retard";

include_once("$GLOBAL_START_PATH/catalog/admin_func.inc.php");
include_once("$GLOBAL_START_PATH/catalog/drawlift.php");

$con = DBUtil::getConnection();


include("$GLOBAL_START_PATH/templates/back_office/head.php");

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/ui/i18n/ui.datepicker-fr.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/ui/blockUI.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css"/>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css"/>




<?php



/*if (isset($_POST['form_submit'])) {
 if( empty( $_POST[ "date" ] ) )
		exit( "Vous devez saisir un nom pour le modèle" );
}*/
?>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="mainContent fullWidthContent">


   
<div class="topRight"></div>
<div class="topLeft"></div>	
<h1><?php echo $Title; ?></h1>  
<hr>
	<?
$con = DBUtil::getConnection();
$SQL_Query = "SELECT B.idbuyer, B.company  , C.lastname , C.firstname, amount, RB.payment_date ,P.name_1 payment_mode
		FROM `regulations_buyer` RB,buyer B, contact C, payment P
		WHERE RB.accept=0 and payment_date >'". date('Y-m-d') ."'
		AND C.idbuyer = B.idbuyer AND  RB.idbuyer = B.idbuyer
		AND P.idpayment = RB.idpayment
		GROUP BY RB.idpayment
		";
	//echo $SQL_Query;			
	
	$rs = DButil::query($SQL_Query);
	$html = $htmls = '';
	if( $rs->RecordCount() == 0 ){
		echo "<br /><div style='margin-left:10px;'>Aucun paiement en retard n'a été trouvé.</div>";
	}
	while (!$rs->EOF()) {
		
		$html = '<tr>' ;
		$html .= '<td>'. $rs->fields("idbuyer") . '</td>' ;
		$html .= '<td>'. $rs->fields("lastname").' '. $rs->fields("firstname").'<br />'. $rs->fields("company") . '</td>';
		$html .= '<td>'. $rs->fields("amount") . '</td>' ;
		$html .= '<td>'. $rs->fields("payment_date") . '</td>' ;
		$html .= '<td>'. $rs->fields("payment_mode") . '</td>' ;
		$htmls .= $html;
		
	$rs->MoveNext();	
		
	}
	if(!empty($htmls)){
		$output = '<table style="padding:5px;" class="dataTable resultTable  tablesorter">
					<thead>
					<tr>
						<th>N° client</th>
						<th>Nom</th>
						<th>Montant TTC</th>
						<th>Date de paiement</th>
						<th>Mode de paiment</th>
				 </tr>
				 </thead>
				'.$htmls.'</table>';
	}
	
	?>
	<div style="margin:20px">

		<?php echo $output;?>
	</div>
<div class="bottomRight"></div>
<div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent --><!--GlobalMainContent-->

</div>
<?
include("$GLOBAL_START_PATH/templates/back_office/foot.php");
?>
