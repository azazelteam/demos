<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Tableau de relances clients
 */
 
//------------------------------------------------------------------------------------------------

ini_set( "memory_limit", "64M" ); //@todo script à optimiser

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
include_once ("$GLOBAL_START_PATH/objects/Invoice.php");

$Title = Dictionnary::translate( "gest_com_invoices_notpayed" );

//------------------------------------------------------------------------------------------------
//export

if (isset ($_GET["export"]) && isset ($_GET["req"])) {

	$req = $_GET["req"];
	$exportableArray = & getExportableArray($req);
	exportArray($exportableArray);

	exit ();

}

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$lastupdate = date( "Y-m-d H:i:s" );

//------------------------------------------------------------------------------------------------

$resultPerPage = 30;
$maxSearchResults = 1000;

$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

//------------------------------------------------------------------------------------------------

$globalTotal = array();
$globalTotal[ "count" ] = array( 0, 0, 0, 0, 0, 0, 0, 0 );
$globalTotal[ "amount" ] = array( 0, 0, 0, 0, 0, 0, 0, 0 );
$globalTotal[ "toPay" ] = array( 0, 0, 0, 0, 0, 0, 0, 0 );

/**
 * relaunch_type :
 * 1 : mail
 * 2 : fax
 * 3 : les deux
 */

if( ( isset( $_POST[ "UpdateRows" ] ) || isset( $_POST[ "UpdateRows_y" ] ) )
	&& isset( $_POST[ "OrderSupplierRows" ] ) )
	updateLoads();

if ( isset( $_POST[ "RelaunchButton_1" ] ) || isset( $_POST[ "RelaunchButton_1_y" ] ) ){
		
		if ( isset( $_POST[ "relaunch_1" ] )){
			$list = $_POST[ "relaunch_1" ];
			
			for($nb =0; $nb < count( $_POST[ "relaunch_1" ] ); $nb++){
				
				$idbilling_buyer = $list[$nb];
				$relaunch_date_1 = date("Y-m-d");
			
				//On envoie la relance nv1 par mail
				SendRelaunchMail($idbilling_buyer,1);
			
				//On met à jour la date d'envoi
				
				$q = "
				UPDATE `billing_buyer` 
				SET relaunch_date_1='$relaunch_date_1', relaunch_level=1, relaunch_type_1=1, relaunch_iduser=$iduser, relaunch_lastupdate='$lastupdate'
				WHERE idbilling_buyer = '$idbilling_buyer' 
				LIMIT 1";
				
				$r =& DBUtil::query($q);
				
				if($r===false)
					die("Impossible de mettre à jour la date de relance niveau 1");
			}
		}
		
		if ( isset( $_POST[ "relaunch_1fax" ] )){
			$list = $_POST[ "relaunch_1fax" ];
			
			for($nb =0; $nb < count( $_POST[ "relaunch_1fax" ] ); $nb++){
				
				$idbilling_buyer = $list[$nb];
				$relaunch_date_1 = date("Y-m-d");
			
				//On envoie la relance nv1 par mail
				SendRelaunchFax($idbilling_buyer,1);
			
				//On met à jour la date d'envoi
				
				$q2 = "
				UPDATE `billing_buyer` 
				SET relaunch_date_1='$relaunch_date_1', relaunch_level=1, relaunch_type_1=2, relaunch_iduser=$iduser, relaunch_lastupdate='$lastupdate'
				WHERE idbilling_buyer = '$idbilling_buyer' 
				LIMIT 1";
				
				$r2 =& DBUtil::query($q2);
				
				if($r2===false)
					die("Impossible de mettre à jour la date de relance niveau 1");
			}
		}
		
}

if ( isset( $_POST[ "RelaunchButton_2" ] ) || isset( $_POST[ "RelaunchButton_2_y" ] ) ){
		
		if ( isset( $_POST[ "relaunch_2" ] )){
			$list = $_POST[ "relaunch_2" ];
			
			for($nb =0; $nb < count( $_POST[ "relaunch_2" ] ); $nb++){
				
				$idbilling_buyer = $list[$nb];
				$relaunch_date_2 = date("Y-m-d");
			
				//On envoie la relance nv2 par mail
				SendRelaunchMail($idbilling_buyer,2);
			
				//On met à jour la date d'envoi
				
				$q = "
				UPDATE `billing_buyer` 
				SET relaunch_date_2='$relaunch_date_2', relaunch_level=2, relaunch_type_2=1, relaunch_iduser=$iduser, relaunch_lastupdate='$lastupdate'
				WHERE idbilling_buyer = '$idbilling_buyer' 
				LIMIT 1";
				
				$r =& DBUtil::query($q);
				
				if($r===false)
					die("Impossible de mettre à jour la date de relance niveau 2");
			}
		}
		
		if ( isset( $_POST[ "relaunch_2fax" ] )){
			$list = $_POST[ "relaunch_2fax" ];
			
			for($nb =0; $nb < count( $_POST[ "relaunch_2fax" ] ); $nb++){
				
				$idbilling_buyer = $list[$nb];
				$relaunch_date_2 = date("Y-m-d");
			
				//On envoie la relance nv2 par mail
				SendRelaunchFax($idbilling_buyer,2);
			
				//On met à jour la date d'envoi
				
				$q1 = "
				UPDATE `billing_buyer` 
				SET relaunch_date_2='$relaunch_date_2', relaunch_level=2, relaunch_type_2=2, relaunch_iduser=$iduser, relaunch_lastupdate='$lastupdate'
				WHERE idbilling_buyer = '$idbilling_buyer' 
				LIMIT 1";
				
				$r1 =& DBUtil::query($q1);
				
				if($r1===false)
					die("Impossible de mettre à jour la date de relance niveau 2");
			}
		}
}

if ( isset( $_POST[ "RelaunchButton_3" ] ) || isset( $_POST[ "RelaunchButton_3_y" ] ) ){
		
		if ( isset( $_POST[ "relaunch_3" ] )){
			$list = $_POST[ "relaunch_3" ];
			
			for($nb =0; $nb < count( $_POST[ "relaunch_3" ] ); $nb++){
				
				$idbilling_buyer = $list[$nb];
				$relaunch_date_3 = date("Y-m-d");
			
				//On envoie la relance nv2 par mail
				SendRelaunchMail($idbilling_buyer,3);
			
				//On met à jour la date d'envoi
				$q = "
				UPDATE `billing_buyer` 
				SET relaunch_date_3='$relaunch_date_3', relaunch_level=3, relaunch_type_3=1, relaunch_iduser=$iduser, relaunch_lastupdate='$lastupdate'
				WHERE idbilling_buyer = '$idbilling_buyer' 
				LIMIT 1";
				
				$r =& DBUtil::query($q);
				
				if($r===false)
					die("Impossible de mettre à jour la date de relance niveau 3");
			}
		}
		if ( isset( $_POST[ "relaunch_3fax" ] )){
			$list = $_POST[ "relaunch_3fax" ];
			
			for($nb =0; $nb < count( $_POST[ "relaunch_3fax" ] ); $nb++){
				
				$idbilling_buyer = $list[$nb];
				$relaunch_date_3 = date("Y-m-d");
			
				//On envoie la relance nv2 par mail
				SendRelaunchFax($idbilling_buyer,3);
			
				//On met à jour la date d'envoi
				$q = "
				UPDATE `billing_buyer` 
				SET relaunch_date_3='$relaunch_date_3', relaunch_level=3, relaunch_type_3=2, relaunch_iduser=$iduser, relaunch_lastupdate='$lastupdate'
				WHERE idbilling_buyer = '$idbilling_buyer' 
				LIMIT 1";
				
				$r =& DBUtil::query($q);
				
				if($r===false)
					die("Impossible de mettre à jour la date de relance niveau 3");
			}
		}
		
}

?>
<script type="text/javascript">
<!--
	function checkuncheck(id,type){
		
		if (type=='mail'){
			
			var couscous=document.getElementById('mail'+id);
			
			if(couscous.checked==true)
				document.getElementById('fax'+id).checked=false;
			
		}
		
		if(type=='fax'){
			
			var couscous=document.getElementById('fax'+id);
			
			if(couscous.checked==true)
				document.getElementById('mail'+id).checked=false;
			
		}
		
	}
	
	function SortResult(sby,ord){
		
		<?if(isset( $_GET[ "dev" ] )){?>
			document.billing.action="<?php echo $ScriptName ?>?dev&search=1&sortby="+sby+"&sens="+ord;
		<?php } else{?>
			document.billing.action="<?php echo $ScriptName ?>?search=1&sortby="+sby+"&sens="+ord;
		<?php } ?>
		document.billing.submit();
		
	}
	
	function popupcomment(idbilling){
		
		postop = (self.screen.height-700)/2;
		posleft = (self.screen.width-800)/2;
		
		window.open('billing_relaunch_comment.php?idbilling_buyer='+idbilling, '_blank', 'top=' + postop + ',left=' + posleft + ',width=800,height=700,resizable,scrollbars=yes,status=0');
		
	}
	
-->
</script>
<style type="text/css">
<!--
table.resultTable tr.todayLines td{
	background-color:#FFC864;
}
-->
</style>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	
<?php

displayForm();

?><form name="billing"  action="invoices_follow-up.php"  method="post"><?


$sql_cond = "";
if( isset( $_REQUEST[ "search" ] ) ){
	$sql_cond = search();
}
$countToday = 0;
?>
<div >
<!-- Onglets a l'echalotte -->
<div class="mainContent fullWidthContent">
	<div class="topRight"></div><div class="topLeft"></div>
    <div class="content" id="container" >
	
		<ul class="menu" style="margin-bottom:10px;">
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Today" id="Tdy">Relances du jour</a></div>
			</li>
			
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#toCall" id="totel">A rappeler</a></div>
			</li>
			
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Niveau0" id="Nv0">Non relancées</a></div>
			</li>
			
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Niveau1" id="Nv1">Niveau 1</a></div>
			</li>
			
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Niveau2" id="Nv2">Niveau 2</a></div>
			</li>
			
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Niveau3" id="Nv3">Niveau 3</a></div>
			</li>
			
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Niveau4" id="Litigations">Contentieux</a></div>
			</li>

			<li class="item">
				<div class="right"></div><div class="right"></div>
				<div class="centered"><a href="#toCome" id="tocum">Règlements à parvenir</a></div>
			</li>

		</ul>

		<?
		//faites
		displayTodayActions( $sql_cond );
		
		//A rappeler
		displayInvoicesToCall();

		//relances
		displayInvoicesNvX( 0 , $sql_cond);
		displayInvoicesNvX( 1 , $sql_cond);
		displayInvoicesNvX( 2 , $sql_cond);
		displayInvoicesNvX( 3 , $sql_cond);
		displayInvoicesNvX( 4 , $sql_cond , 1);
		
		//paiements à venir
		displayInvoicesRegulationToCome();
		?>
		</form>
	</div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->


<script type="text/javascript">
/* <![CDATA[ */

	$( document ).ready( function(){
		
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
		
	});

/* ]]> */
</script>
<?php

displayRecap();

?>
</div>
	<script type='application/javascript'>
		document.getElementById('Tdy').innerHTML = document.getElementById('Tdy').innerHTML + ' (<?=$countToday?>)';
		document.getElementById('Nv0').innerHTML = document.getElementById('Nv0').innerHTML + ' (<?=$globalTotal[ "count" ][ 1 ]?>)';
		document.getElementById('Nv1').innerHTML = document.getElementById('Nv1').innerHTML + ' (<?=$globalTotal[ "count" ][ 2 ]?>)';
		document.getElementById('Nv2').innerHTML = document.getElementById('Nv2').innerHTML + ' (<?=$globalTotal[ "count" ][ 3 ]?>)';
		document.getElementById('Nv3').innerHTML = document.getElementById('Nv3').innerHTML + ' (<?=$globalTotal[ "count" ][ 4 ]?>)';
		document.getElementById('Litigations').innerHTML = document.getElementById('Litigations').innerHTML + ' (<?=$globalTotal[ "count" ][ 5 ]?>)';
		document.getElementById('tocum').innerHTML = document.getElementById('tocum').innerHTML + ' (<?=$globalTotal[ "count" ][ 6 ]?>)';
		document.getElementById('totel').innerHTML = document.getElementById('totel').innerHTML + ' (<?=$globalTotal[ "count" ][ 7 ]?>)';
	</script>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	global $GLOBAL_START_PATH;

	
	$idbuyer 				= isset( $_POST[ "idbuyer" ] ) ? $_POST[ "idbuyer" ] : "";
	$company 				= isset( $_POST[ "company" ] ) ? stripslashes( $_POST[ "company" ] ) : "";
	$idbilling_buyer 		= isset( $_POST[ "idbilling_buyer" ] ) ? $_POST[ "idbilling_buyer" ] : "";
	$total_amount 			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
	$dev 					= isset( $_GET[ "dev" ] ) ? $_GET[ "dev" ] : "";
	$factor_pay 		= isset( $_POST[ "factor_pay" ] ) ? $_POST[ "factor_pay" ] : "";
	?>

	<div class="mainContent">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content" >
        	<div class="headTitle">
                <p class="title">Recherche de factures en retard de paiement</p>
            </div>
            <div class="subContent">
            	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/invoices_follow-up.php<?if(isset( $_GET[ "dev" ] )){?>?dev<?php } ?>" method="post" name="searchForm">
	  				<input type="hidden" name="search" value="1" />
            		<!-- tableau de recherche -->
                    <div class="tableContainer">
                        <table class="dataTable">
                            <tr class="filledRow">
                                <th>Client n°</th>
                                <td>
                                	<input type="text" name="idbuyer" id="idbuyer" value="<?php echo $idbuyer ?>" class="textInput" />
                                	<?php
                                	
                                	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
                                	AutoCompletor::completeFromDBPattern( "idbuyer", "buyer", "idbuyer", "{company}", 15 );
                                	
                                ?>
                                </td>
                                <th>Facture n°</th>
                                <td><input type="text" name="idbilling_buyer" value="<?php echo $idbilling_buyer ?>" class="textInput" /></td>
                            </tr>
                            <tr>
                                <th>Nom / Raison sociale</th>
                                <td><input type="text" name="company" id="company" value="<?php echo $company ?>" class="textInput" />
                                <?php 
                                
                                	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
                                	AutoCompletor::completeFromDBPattern( "company", "buyer", "company", "Client n°{idbuyer}", 15 ); 
                                
									?>
                                </td>
                                <th>Total facturé TTC</th>
                                <td><input type="text" name="total_amount"  value="<?php echo $total_amount ?>" class="textInput" /></td>
                            </tr>
                           <tr>
                            	<th>Exclure les factures avec règlement factor</th>
                                <td>
                                	<select name="factor_pay">
                                		<option value="1" <?php if($factor_pay=="1") echo "selected";?>>Oui</option>
                                		<option value="0"<?php if($factor_pay=="0") echo "selected";?>>Non</option>
                                	</select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="submitButtonContainer">
                    	<input type="submit" class="blueButton" value="Rechercher" />
                    </div>
            	</form>
            </div>
            <div class="subContent">

            </div>
        </div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos</p>
            </div>
            <div class="content" style="padding:5px;">
            	Si le client dispose d'un comptable, les emails et fax de relance lui seront envoyés en priorité
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>
	<?php 

}

//----------------------------------------------------------------------------------------------------

function displayRecap(){
	
	global $globalTotal;
	
	if( $globalTotal[ "count" ][ 0 ] == 0 )
		return;
	
?>

	
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content" >
			<div class="headTitle">
				<p class="title">Récapitulatif</p>
			</div>
			<div class="subContent">
				<div class="resultTableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th style="border-width:0px; background-color:rgb(255, 255, 255); width:12%;"></th>
								<th style="width:13%;">Non traitées</th>
								<th style="width:13%;">Relance niveau 1</th>
								<th style="width:13%;">Relance niveau 2</th>
								<th style="width:13%;">Relance niveau 3</th>
								<th style="width:13%;">Litigieuses</th>
								<th style="width:13%;">A parvenir</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<th class="lefterCol">Nombre</th>
								<td><?php echo $globalTotal[ "count" ][ 1 ] == "0" ? "-" : $globalTotal[ "count" ][ 1 ] ?></td>
								<td><?php echo $globalTotal[ "count" ][ 2 ] == "0" ? "-" : $globalTotal[ "count" ][ 2 ] ?></td>
								<td><?php echo $globalTotal[ "count" ][ 3 ] == "0" ? "-" : $globalTotal[ "count" ][ 3 ] ?></td>
								<td><?php echo $globalTotal[ "count" ][ 4 ] == "0" ? "-" : $globalTotal[ "count" ][ 4 ] ?></td>
								<td><?php echo $globalTotal[ "count" ][ 5 ] == "0" ? "-" : $globalTotal[ "count" ][ 5 ] ?></td>
								<td><?php echo $globalTotal[ "count" ][ 6 ] == "0" ? "-" : $globalTotal[ "count" ][ 6 ] ?></td>
								<td class="righterCol"><?php echo $globalTotal[ "count" ][ 0 ] ?></td>
							</tr>
							<tr>
								<th class="lefterCol">Total TTC factures</th>
								<td><?php echo $globalTotal[ "count" ][ 1 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "amount" ][ 1 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 2 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "amount" ][ 2 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 3 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "amount" ][ 3 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 4 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "amount" ][ 4 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 5 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "amount" ][ 5 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 6 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "amount" ][ 6 ] ) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat( $globalTotal[ "amount" ][ 0 ] ) ?></td>
							</tr>
							<tr>
								<th class="lefterCol">Total solde à payer</th>
								<td><?php echo $globalTotal[ "count" ][ 1 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "toPay" ][ 1 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 2 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "toPay" ][ 2 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 3 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "toPay" ][ 3 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 4 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "toPay" ][ 4 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 5 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "toPay" ][ 5 ] ) ?></td>
								<td><?php echo $globalTotal[ "count" ][ 6 ] == "0" ? "-" : Util::priceFormat( $globalTotal[ "toPay" ][ 6 ] ) ?></td>
								<td class="righterCol"><?php echo Util::priceFormat( $globalTotal[ "toPay" ][ 0 ] ) ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
<?php
	
}

//------------------------------------------------------------------------------------------------

function search(){
	
		$SQL_Condition = "";
		
		$idbuyer 				= isset( $_POST[ "idbuyer" ] ) ? $_POST[ "idbuyer" ] : "";
		$company 				= isset( $_POST[ "company" ] ) ? stripslashes( $_POST[ "company" ] ) : "";
		$idbilling_buyer 		= isset( $_POST[ "idbilling_buyer" ] ) ? $_POST[ "idbilling_buyer" ] : "";
		$total_amount 			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
		
							
		if ( !empty($idbuyer) ){
				$idbuyer = FProcessString($idbuyer);
				$SQL_Condition .= " AND bb.idbuyer =$idbuyer";
		}
		
		if ( !empty($idbilling_buyer) ){
				$idbilling_buyer = FProcessString( $idbilling_buyer );
				$SQL_Condition .= " AND bb.idbilling_buyer IN( $idbilling_buyer )";
		}
			
		if( !empty( $company ) )
			$SQL_Condition .= " AND b.company LIKE '%$company%'";
			
		if( !empty( $total_amount ) )
			$SQL_Condition .= " 
			AND ROUND( bb.total_amount, 2 ) = '" . Util::text2num( $total_amount ) . "'";
		
		return $SQL_Condition;

}

//------------------------------------------------------------------------------------------------

function FProcessString($str)
{
	return "'". str_replace(",","','",$str) ."'";
}

//------------------------------------------------------------------------------------------------

function displayTodayActions($sql_cond=""){
	
	global	$GLOBAL_START_URL,
			$resultPerPage,
			$globalTotal,
			$GLOBAL_DB_PASS,
			$countToday,
			$GLOBAL_START_PATH;
			
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	$iduser = User::getInstance()->getId();
	
	?>
	<?php
	
	$search=1;
	$SQL_Condition="";
	if((isset($_GET['sortby'])) AND (isset($_GET['sortby']))){
		$SQL_Condition	.= " ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
	}else{
		$SQL_Condition	.= " ORDER BY bb.deliv_payment ASC";
	}
	
	//factures dont la date d'échéance est dépassée
	$minbuying=DBUtil::getParameterAdmin('factor_minbuying');
	$factor_condition="";
	if(isset($_POST['factor_pay'])&&($_POST['factor_pay']=="1"))
	{
	$factor_payment=DBUtil::getParameterAdmin('factor_payment');

	$factor_condition=$factor_payment?"AND bb.idpayment <> $factor_payment":"";
	}
	
	$date = date( "Y-m-d" );
	
	$query = "
	SELECT bb.idbilling_buyer,
		bb.idorder,
		bb.idbuyer,
		bb.idcontact,
		o.conf_order_date,
		bb.total_amount, 
		bb.total_amount_ht,
		bb.deliv_payment,
		bb.factor,
		bb.factor_refusal_date,
		bb.factor_send_date,
		u1.initial AS comRelaunch,
		u2.initial AS comOrder,
		b.company,
		b.accountant,
		b.accountant_phonenumber,
		b.accountant_faxnumber,
		cont.title,
		cont.lastname,
		cont.phonenumber,
		bb.DateHeure,
		b.contact,
		bb.relaunch_phone_date1,
		bb.relaunch_phone_date2,
		bb.relaunch_phone_date3,
		bb.relaunch_phone_date4,
		bb.relaunch_phone_recall,
		bb.relaunch_phone_time,
		bb.relaunch_phone_hour,
		GREATEST(bb.relaunch_phone_date1,
		bb.relaunch_phone_date2,
		bb.relaunch_phone_date3,
		bb.relaunch_phone_date4)as relaunch_tri,
		bb.relaunch_paid_InProgress,
		bb.relaunch_litigation,
		bb.relaunch_comment,
		u3.color
	FROM billing_buyer bb,
		`order` o,
		user u1,
		user u2,
		user u3,
		buyer b,
		contact cont
	WHERE bb.deliv_payment <> '0000-00-00'
	AND bb.deliv_payment <> ''
	AND bb.deliv_payment <= '".$date."'
	AND bb.idpayment = o.idpayment ".
	$factor_condition."
	AND bb.relaunch_lastupdate LIKE '".$date."'
	AND bb.idorder = o.idorder
	AND bb.idbuyer = b.idbuyer
	AND bb.idcontact = cont.idcontact
	AND bb.idbuyer = cont.idbuyer";
		
	
		/**
		 * ATTENTION LE PARAM DEV EST TEMPORAIRE
		 * POUR LAISSER LE TEMPS A JF DE REFAIRE TOUS LES REGLEMENTS
		 * ANTERIEURS
		 * 
		 * BISOUS LES AMIS
		 * */
		//if( isset( $_GET[ "dev" ] ) )
			$query .= " AND bb.status NOT LIKE '" . Invoice::$STATUS_PAID . "'";	
		/*else $query .= "
		AND ( 	
			bb.status NOT IN ( '" . Invoice::$STATUS_PAID . "' )
			OR (
					bb.status = 'Paid'
					AND ROUND(bb.total_amount - bb.credit_amount - (	SELECT SUM(rb.amount)
					FROM billing_regulations_buyer brb, regulations_buyer rb
					WHERE rb.idregulations_buyer = brb.idregulations_buyer
					AND rb.accept=1
					AND brb.idbilling_buyer = bb.idbilling_buyer
				),2 ) > 0
			) 
		)";*/
			
		$query .= "
	AND bb.relaunch_iduser = u1.iduser
	AND o.iduser = u2.iduser
	AND bb.idbuyer = b.idbuyer
	AND bb.relaunch_iduser = u3.iduser";
	
	if( !$hasAdminPrivileges )
		$query .= "
		AND ( bb.iduser = '$iduser' OR b.iduser = '$iduser' )";

	$query .= $sql_cond;
	$query .= $SQL_Condition;
	$rs =& DBUtil::query( $query );
	
	include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");

	$encryptedQuery = URLFactory::base64url_encode(Encryptor::encrypt($query, $GLOBAL_DB_PASS));
		
	if( $rs === false )
		die( "Impossible de trouver les factures relancées aujourd'hui" );
	
	//Encore un truc pourri pour masquer ce qui ne va pas. Tant qu'on n'aura pas des statuts fiables (et donc que la requête pourra être uniquement basée sur les statuts), on laisse ça et on n'utilise pas $rs->RecordCount()
	//C'est moche mais c'est la vie
	
	$resultCount = 0;
	while( !$rs->EOF ){
		
		$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		
		if( round( $Invoice->getBalanceOutstanding(), 2 ) != 0 )
			$resultCount++;
		
		$rs->MoveNext();
		
	}
	$countToday = $rs->RecordCount();
	if( !$rs->RecordCount() ){
	
		?>
		
			<div id="Today">
	        	<div class="headTitle">
	                <p class="title" style="color: #008000;">Aucune facture relancée aujourd'hui</p>
					 <div class="rightContainer"></div> 
	            </div>
			</div>
		<?php
			
		return;
		
	}else{
	
		?>
		<div id="Today">
        	<div class="headTitle">
                <p class="title"><?php echo $resultCount ?> facture<?php echo $resultCount == 1 ? "" : "s" ?> relancée<?php echo $resultCount == 1 ? "" : "s" ?> aujourd'hui</p>
				 <div class="rightContainer">
						<a href="<?=$GLOBAL_START_URL?>/accounting/invoices_follow-up.php?export&amp;req=<?=$encryptedQuery?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
					</div> 
            </div>
			<div class="subContent">
				<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
                                <th>Resp. Rel.</th>
                                <th>Com cde</th>
                                <th>Client n°</th>
                                <th>Facture n°</th>
                                <th>Raison sociale</th>
                                <th>Règlement 1</th>
                                <th>Date de facturation</th>
                                <th>Date d'échéance</th>
                                <th>Retard (jours)</th>
                                <th>Total TTC facture</th>
                                <th>Solde à payer</th>
                                <th>Date relance tél.</th>
                                <th>A relancer par tél.</th>
                                <th>Fiche suivi</th>
                                <th>Aperçu mail</th>
                            </tr>
                            <!-- petites flèches de tri -->
                            <tr>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u1.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u1.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u2.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u2.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbilling_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbilling_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('b.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('b.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.factor','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.factor','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.DateHeure','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.DateHeure','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.total_amount','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.total_amount','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('relaunch_tri','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('relaunch_tri','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                             	</th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_phone_recall','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_phone_recall','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                                <th class="noTopBorder"></th>
                        	</tr>
                        </thead>
                        <tbody>
                        <?php
						
						$order_total_amount_ht = 0.0;
						$order_total_amount = 0.0;
						$order_total_toPay = 0.0;
						
						$rs->MoveFirst();
						
						while( !$rs->EOF() ){
							
							$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
							
							$netToPay = round( $Invoice->getBalanceOutstanding(), 2 );
							
							//On n'affiche pas les factures qui sont passées au travers des mailles du filet et qui pourtant ont un solde nul
							if( $netToPay != 0 ){
								
								$idbuyer 			= $rs->fields( "idbuyer" );
								$idcontact 			= $rs->fields( "idcontact" );
								$idorder 			= $rs->fields( "idorder" );
								list( $conf_order_date, $null )	= explode( " ", $rs->fields( "conf_order_date" ) );
								$deliv_payment		= $rs->fields("deliv_payment");
								$total_amount_ht 	= $rs->fields( "total_amount_ht" );
								$total_amount_ttc 	= $Invoice->getNetBill();
								$mail				= checkBuyerMail($idbuyer,$idcontact);
								$fax				= checkBuyerFax($idbuyer,$idcontact);
								$idbilling_buyer	= $rs->fields( "idbilling_buyer" );
								$iscontact 			= $rs->fields( "contact" ) == 1;
								$factor				= $rs->fields( "factor" );
								$company			= $rs->fields( "company" );
								$lastname			= $rs->fields( "lastname" );
								$title 				= getTitle($rs->fields( "title" ));
								$phonenumber  		= HumanReadablePhone($rs->fields( "phonenumber" ));
								$accountant				= $rs->fields( "accountant" );
								$accountant_phonenumber	= $rs->fields( "accountant_phonenumber" );
								$accountant_faxnumber	= $rs->fields( "accountant_faxnumber" );
								$DateHeure			= $rs->fields( "DateHeure" );
								list($Date, $Heure) = explode(" ",$DateHeure);
								$invoice_date		= $Date;
								$retard = round((strtotime(date("Y-m-d")) - strtotime($deliv_payment))/(60*60*24)-1);
								
								if($factor==0 || $factor==''){
									$factor=Dictionnary::translate( "no" );
								}else{
									$factor=Dictionnary::translate( "yes" );
								}
								
								$order_total_amount_ht += $total_amount_ht;
								$order_total_amount += $total_amount_ttc;
								$order_total_toPay += $netToPay;
								$type = $iscontact ? "contact" : "buyer";
								$buyerHREF = "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer&amp;type=$type";
								$orderHREF = "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$idorder";
								$invoiceHREF = "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer";
								
								$rpd1 = $rs->fields( "relaunch_phone_date1" );
								$rpd2 = $rs->fields( "relaunch_phone_date2" );
								$rpd3 = $rs->fields( "relaunch_phone_date3" );
								$rpd4 = $rs->fields( "relaunch_phone_date4" );
								$rpd = '0000-00-00';
								
								if($rpd4!='0000-00-00')
									$rpd = $rpd4;
								else if($rpd3!='0000-00-00')
									$rpd = $rpd3;
								else if($rpd2!='0000-00-00')
									$rpd = $rpd2;
								else if($rpd1!='0000-00-00')
									$rpd = $rpd1;
								
								if($rpd=='0000-00-00')
									$rpd='';
								
								$rpr = $rs->fields( "relaunch_phone_recall" );
								
								if($rpr=='0000-00-00')
									$rpr='';
								else
									$rpr = $rpr;
								
								$rpt = $rs->fields( "relaunch_phone_time" );
								
								switch( $rpt ){
									case "Après-Midi": $time=" PM";break;
									case "Matin": $time=" AM";break;
									case "Indifférent":	$time='';break;
									default : $time='';
								}
			
								$rph = $rs->fields( "relaunch_phone_hour" );
								
								$paidInProgress = $rs->fields( "relaunch_paid_InProgress" );
								$relaunch_litigation = $rs->fields( "relaunch_litigation" );
								$relaunch_comment = $rs->fields( "relaunch_comment");
								
								//couleur par commercial
								
								$colorclass='';
								$color = $rs->fields("color");
				
								if( $color != '' )
									$colorclass=" style=\"color:$color;\"";	
								
							?>
                             <script type="text/javascript">
							/* <![CDATA[ */
							    $(document).ready(function(){
							        $("#helpBuyerNotPaidInvoicesNotPaid_<?=$idbilling_buyer;?>_today").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Nom du contact :</b> <?php echo "$title $lastname" ?><?php if( $phonenumber != "" ){ ?> <br /><b>Téléphone :</b> <?php echo $phonenumber ?><?php } ?><?php if( $accountant != "" ){ ?> <br /><b>Comptable :</b> <?php echo $accountant ?><?php } ?><?php if( $accountant_phonenumber != "" ){ ?> <br /><b>Tél. comptable :</b> <?php echo $accountant_phonenumber ?><?php } ?><?php if( $accountant_faxnumber != "" ){ ?> <br /><b>Fax comptable :</b> <?php echo $accountant_faxnumber ?><?php } ?>',
							            position: {
							                corner: {
							                    target: "rightMiddle",
							                    tooltip: "leftMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							    
							     $(document).ready(function(){
							        $("#helpComment_<?=$idbilling_buyer;?>_today").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Commentaires :</b><br /> <?php echo str_replace("\r\n","",nl2br($relaunch_comment)) ?>',
							            position: {
							                corner: {
							                    target: "leftMiddle",
							                    tooltip: "rightMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							/* ]]> */
							</script>
                            <?php $name_payment = getPayment( $idpayment ); ?>
                            <tr>
                            	<td class="lefterCol"<?=$colorclass?>><?php echo $rs->fields( "comRelaunch" ) ?></td>
                            	<td><?php echo $rs->fields( "comOrder" ) ?></td>
								<td><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"><?php echo $idbuyer ?></a></td>
								<td class="grasBack"><a href="<?php echo $invoiceHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbilling_buyer ?></a></td>
								<td class="grasBack"><div id="helpBuyerNotPaidInvoicesNotPaid_<?=$idbilling_buyer?>_today"><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $company ?></a><br /><?php echo $lastname ?></div></td>
								<!--<td><a href="<?php echo $buyerHREF ?>#factor" onclick="window.open(this.href); return false;"><?php echo $factor ?></a></td>-->
								<td><?php echo $name_payment ?></td>
								<td><?php echo Util::dateFormatEu( $invoice_date, false, "-", true ) ?></td>
								<td><?php echo Util::dateFormatEu( $deliv_payment, false, "-", true ) ?></td>
								<td><?php echo $retard ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $Invoice->getTotalATI() ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $netToPay ) ?></td>
								<td><?php echo Util::dateFormatEu( $rpd, false, "-", true ) ?></td>
								<td><?php echo Util::dateFormatEu( $rpr, false, "-", true ) ?><?=$time."<br />".$rph?></td>
								<td><div id="helpComment_<?=$idbilling_buyer?>_today">
									<a href="#" onclick="popupcomment('<?=$idbilling_buyer?>'); return false;">Voir</a></div>
									<?php if( $rs->fields( "factor_refusal_date" ) != "0000-00-00" ){ ?>
										<br /><span style="color:#FF0000;">Débité factor</span>
									<?php } ?>
									<?php if( $rs->fields( "factor_refusal_date" ) == "0000-00-00" && $rs->fields( "factor_send_date" ) != "0000-00-00" ){ ?>
										<br /><span style="color:#FF0000;">Financé par le factor</span>
									<?php } ?>
									<?php
									
									$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1" );
									
									if( $rsLitigation->RecordCount() ){
										
										?><br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open(this.href); return false;" style="color:#EB6A0A;">Litige</a><?php
											
									}
									
									
									if($paidInProgress){
										echo"<br /><span style=\"color:#FF0000;\">Paiement en cours</span>";
									}
									
									if($relaunch_litigation){
										echo"<br /><span style=\"color:#FF0000;\">Paiement litigieux</span>";
									}
									?>
								</td>
								<td class="righterCol">
									<a href="<?php echo $GLOBAL_START_URL ?>/accounting/transmit_relaunch.php?idbilling_buyer=<?php echo $idbilling_buyer ?>&amp;level=1">Mail</a><br />
									<!--<a href="<?php echo $GLOBAL_START_URL ?>/accounting/relaunch_rtf_letter.php?id=<?php echo $idbilling_buyer ?>">Word</a>-->
								</td>
                            </tr>
                        <?php 
								
							}
							
							$rs->MoveNext();
							
						}
					
						
						?>
                            <tr>
								<th style="border-style:none;" colspan="9"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_amount ) ?></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_toPay ) ?></th>
								<th style="border-style:none;" colspan="6"></th>
							</tr>
                        </tbody>
                    </table>
                </div>
           	</div>
		</div>
<?php 
	}
	
	
}

//--------------------------------------------------------------------------------------------------
function checkBuyerMail($idbuyer,$idcontact){
	
	//On contrôle que le client possède un mail pour afficher la checkbox d'envoi par mail
	$q = "SELECT c.mail FROM contact c WHERE c.idcontact=$idcontact AND c.idbuyer='$idbuyer'";
	$r =& DBUtil::query( $q );
	
	if( $r === false )
		die( "Impossible de récupérer l'email du client" );
	
	$mail = $r->fields("mail");
	
	if(!empty($mail)){
		return $mail;
	}else{
		return false;
	}
}

//--------------------------------------------------------------------------------------------------
function checkBuyerFax($idbuyer,$idcontact){
	
	//On contrôle que le client possède un fax pour afficher la checkbox d'envoi par fax
	$q = "SELECT c.faxnumber FROM contact c WHERE c.idcontact=$idcontact AND c.idbuyer='$idbuyer'";
	$r =& DBUtil::query( $q );
	
	if( $r === false )
		die( "Impossible de récupérer le fax du client" );
	
	$fax = $r->fields("faxnumber");
	
	if(!empty($fax)){
		return $fax;
	}else{
		$q2 = "SELECT fax_standard FROM buyer WHERE idbuyer='$idbuyer'";
		$r2 =& DBUtil::query( $q2 );
		if( $r2 === false ){
			die( "Impossible de récupérer le fax du client" );
		}
		$fax = $r2->fields("fax_standard");
		if(!empty($fax)){
			return $fax;
		}else{
			return false;
		}
	}
}
//--------------------------------------------------------------------------------------------------

function isBuyerExport( $idorder ){
	
	$query = "SELECT idbuyer, idbilling_adress FROM `order` WHERE idorder='$idorder'";
	
	$rs =& DBUtil::query( $query );
	if( $rs === false )
		die( Dictionnary::translate("Impossible de récupérer l'adresse de facturation") );		
		
	$idbilling 	= $rs->fields("idbilling_adress");
	$idbuyer 	= $rs->fields("idbuyer");
	
	//Pays B@O
	$state_bao=strtolower(DBUtil::getParameterAdmin("ad_state"));
		
	if($idbilling>0){
		//Adresse de facturation différente
		$query = "SELECT idstate FROM billing_adress WHERE idbilling_adress='$idbilling'";
	
		$rs =& DBUtil::query( $query );
		if( $rs === false )
			die( Dictionnary::translate("Impossible de récupérer le pays de l'adresse de facturation") );
			
		$idstate = $rs->fields("idstate");
		
		//Pays du client
		$query = "SELECT name_1 as pays FROM state WHERE idstate=$idstate";
	
		$rs =& DBUtil::query( $query );
		if( $rs === false )
			die( Dictionnary::translate("Impossible de récupérer le pays de du client") );
		
		$state = strtolower($rs->fields("pays"));
		
		if($state!=$state_bao)
			$export='Oui';
		else
			$export='Non';
		
		
	}else{
		//Adresse de facturation du client
		$query = "SELECT idstate FROM buyer WHERE idbuyer='$idbuyer'";
	
		$rs =& DBUtil::query( $query );
		if( $rs === false )
			die( Dictionnary::translate("Impossible de récupérer le pays du client") );
		
		$idstate = $rs->fields("idstate");
		
		//Pays du client
		$query = "SELECT name_1 as pays FROM state WHERE idstate=$idstate";
	
		$rs =& DBUtil::query( $query );
		if( $rs === false )
			die( Dictionnary::translate("Impossible de récupérer le pays de du client") );
		
		$state = strtolower($rs->fields("pays"));
		
		if($state!=$state_bao)
			$export='Oui';
		else
			$export='Non';
	}
	
	return $export;
	
}
//------------------------------------------------------------------------------------------------

function getPayment( $idpayment ){
	
	$query = "SELECT name_1 FROM payment WHERE idpayment = $idpayment";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le mode de paiement" );
		
	return $rs->fields( "name_1" );
	
}

//-------------------------------------------------------------------------------------------------
function SendRelaunch( $idbilling_buyer, $niveau, $mail = true ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php" );
	
	$DEBUG = "";
	
	$query = "
		SELECT bb.idbuyer,
			bb.idcontact,
			bb.factor_refusal_date,
			b.accountant,
			b.accountant_email,
			b.accountant_faxnumber,
			c.firstname,
			c.lastname,
			c.mail,
			c.faxnumber,
			u.firstname AS commercialFirstname,
			u.lastname AS commercialLastname,
			u.email
		FROM billing_buyer bb,
			buyer b,
			contact c,
			`order` o,
			user u
		WHERE bb.idbilling_buyer = '$idbilling_buyer'
			AND bb.idbuyer = b.idbuyer
			AND bb.idbuyer = c.idbuyer
			AND bb.idcontact = c.idcontact
			AND bb.idorder = o.idorder
			AND o.iduser = u.iduser";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les informations de la facture" );
	
	$idbuyer			= $rs->fields( "idbuyer" );
	$idcontact			= $rs->fields( "idcontact" );
	$firstname			= $rs->fields( "firstname" );
	$lastname			= $rs->fields( "lastname" );
	$email				= $rs->fields( "mail" );
	$faxnumber			= $rs->fields( "faxnumber" );
	$accountant_name	= $rs->fields( "accountant" );
	$accountant_email	= $rs->fields( "accountant_email" );
	$accountant_fax		= $rs->fields( "accountant_faxnumber" );
	$salesman_firstname	= $rs->fields( "commercialFirstname" );
	$salesman_lastname	= $rs->fields( "commercialLastname" );
	$salesman_email		= $rs->fields( "email" );
	$factor_refusal		= $rs->fields( "factor_refusal_date" );
	$hasAccountant = $accountant_email == "" ? false : true;
	
	//Débit factor
	$isDebited = $factor_refusal == "0000-00-00" ? false : true;
	
	//-----------------------------------------------------------------------------------------
	
	//Chargement du modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( User::getInstance()->getLang(), 1 ) ); //@todo : imposer la langue du destinataire
	
	if( $isDebited ){
		
		switch( $niveau ){
			case "1":		$editor->setTemplate( "INVOICE_RELAUNCH_FACTOR_1" );break;
			case "2":		$editor->setTemplate( "INVOICE_RELAUNCH_FACTOR_2" );break;
			case "3":		$editor->setTemplate( "INVOICE_RELAUNCH_FACTOR_3" );break;
		}
		
	}else{
		
		switch( $niveau ){
			case "1":		$editor->setTemplate( "INVOICE_RELAUNCH_1" );break;
			case "2":		$editor->setTemplate( "INVOICE_RELAUNCH_2" );break;
			case "3":		$editor->setTemplate( "INVOICE_RELAUNCH_3" );break;
		}
		
	}
	
	$editor->setUseInvoiceTags( $idbilling_buyer );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $idbuyer, $idcontact );
	$editor->setUseCommercialTags( User::getInstance()->getId() );
	
	$Message = $editor->unTagBody();
	
	//-----------------------------------------------------------------------------------------
	//@todo
	/*$CommercialName = DBUtil::getParameterAdmin( "b_contact_firstname" ).' '.DBUtil::getParameterAdmin( "b_contact_lastname" );
	$CommercialEmail = DBUtil::getParameterAdmin( "b_contact_email" );
	$CommercialPhone = DBUtil::getParameterAdmin( "b_contact_tel" );		
	*/
	
	
	$AdminName = DBUtil::getParameterAdmin( "ad_name" );
	
	$mailer = new htmlMimeMail();
	
	if( $mail == false ){
		
		$smtp_server	= DBUtil::getParameterAdmin( "fax_smtp_server" );
		$smtp_sender	= DBUtil::getParameterAdmin( "fax_smtp_sender" );
		$smtp_auth		= DBUtil::getParameterAdmin( "fax_smtp_auth" );
		$smtp_user		= DBUtil::getParameterAdmin( "fax_smtp_user" );
		$smtp_password	= DBUtil::getParameterAdmin( "fax_smtp_password" );
		$smtp_recipient	= DBUtil::getParameterAdmin( "fax_smtp_recipient" );
		$AdminName		= DBUtil::getParameterAdmin( "ad_name" );
		
		$mailer->setSMTPParams( $smtp_server, 25, $smtp_sender, $smtp_auth, $smtp_user, $smtp_password );
		
	}
	
	if( $mail == true )
		$Subject = $editor->unTagSubject();
	elseif( !empty( $accountant_fax ) )
		$Subject = getFaxNumber( $accountant_fax );
	else
		$Subject = getFaxNumber( checkBuyerFax( $idbuyer, $idcontact ) );
	
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );
	
	//-----------------------------------------------------------------------------------------------
	
	/*
	 * Sélection des destinataires et des expéditeurs
	 * 
	 * Relances niveau 1, 2, 3 et relance factor niveau 1
	 * 		Envoi au comptable si existant avec copie au client
	 * 		Envoi au client sinon
	 * 		Expédié par le commercial
	 * Relance factor niveau 2
	 * 		Envoi au comptable si existant avec copie au client
	 * 		Envoi au client sinon
	 * 		Expédié par le service comptabilité
	 * Relance factor niveau 3
	 * 		Envoi au service contentieux
	 * 		Expédié par ????? //@todo
	 */
	//email commercial commande
	$queryc = "SELECT email, firstname, lastname
			FROM user u, `order` o, billing_buyer bb
			WHERE bb.idbilling_buyer = '$idbilling_buyer'
			AND bb.idorder = o.idorder
			AND o.iduser = u.iduser";
	$rsc =& DBUtil::query( $queryc );
	
	if($rsc===false)
		die ("Impossible de récupérer le mail du commercial de la commande");
	
	$email_business = $rsc->fields("email");
	$firstname_business = $rsc->fields("firstname");
	$lastname_business = $rsc->fields("lastname");
	
	//Destinataires
	if( $niveau == 3 && $isDebited ){ //Relance factor niveau 3
		//@todo
		//$receipt = array( "\"Service contentieux\" <$email_contentieux>" );
		
		if( $hasAccountant ){
			
			$receipt = array( $accountant_email );

			
		}else{
			
			$receipt = array( $email );
			
		}
		
	}else{
		
		if( $hasAccountant ){
			
			$receipt = array( $accountant_email );
			
		}else{
			
			$receipt = array( $email );
			
		}
		
	}
	
	//Expéditeur //@todo
	if($mail == true ){
		if( $niveau == 3 && $isDebited ) //Relance factor niveau 3
			$mailer->setFrom( "\"$AdminName - $salesman_firstname $salesman_lastname\" <$salesman_email>" );
		elseif( $niveau == 2 && $isDebited ) //Relance factor niveau 2
			$mailer->setFrom( "\"$AdminName - $salesman_firstname $salesman_lastname\" <$salesman_email>" );
		else
			$mailer->setFrom( "\"$AdminName - $salesman_firstname $salesman_lastname\" <$salesman_email>" );
	}else{
		$mailer->setFrom('"'.$AdminName.'" <'.$smtp_user.'>');
	}
		
	//-----------------------------------------------------------------------------------------------
	/*
	//Ajout du pdf
	$pdf_name = "Facture_$idbilling_buyer.pdf";
	
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
	$odf = new ODFInvoice( $idbilling_buyer );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment( $pdfData, $pdf_name, "application/pdf" );
	*/
	//-----------------------------------------------------------------------------------------------
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setBcc( $salesman_email );
	
	$mail_exepedition = DBUtil::getParameterAdmin( "mail_relaunch_regulation" );
	
	if ( $mail_exepedition != "" )
		$email_business = $mail_exepedition;
	
	$mailer->setBcc( $email_business );
	
	if( $mail == true )
	
		$result = $mailer->send( $receipt );
	else
		$result = $mailer->send( array( "$smtp_recipient" ), "smtp" );
	
	if( !$result )
		trigger_error( "Impossible d'envoyer la relance de facture à l'adresse suivante : $smtp_recipient", E_USER_ERROR );
	
	//Sauvegarde du message envoyé pour suivi
	//updateSending( $idbilling_buyer, $niveau, $Message, $Subject );
}

//------------------------------------------------------------------------------------------------------------------------

function SendRelaunchMail( $idbilling_buyer, $niveau ){
	
	SendRelaunch( $idbilling_buyer, $niveau, true );
	
}

//------------------------------------------------------------------------------------------------------------------------

function SendRelaunchFax( $idbilling_buyer, $niveau ){

	SendRelaunch( $idbilling_buyer, $niveau, false );
	
}

//------------------------------------------------------------------------------------------------------------------------

function HumanReadablePhone($tel){
	//On regarde la longeueur de la cheine pour parcourir
	$lg=strlen($tel);

	$telwcs='';
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($tel,$i,1);
	
		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if($i==0){
			if(($tocheck=='+')OR(is_numeric($tocheck))){
				$telwcs.=$tocheck;
			}
		}else{
			//On ne garde que les chiffres
			if(is_numeric($tocheck)){
				$telwcs.=$tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg=strlen($telwcs);

	$telok ='';

	
	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck=substr($telwcs,0,1);

	if($tocheck=='+'){
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,3);
		$telok.=$tocheck." ";
		$cpt=3;
	}else{
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,2);
		$telok.=$tocheck." ";
		$cpt=2;
	}

	//On traite le reste de la chaine
	for($i=$cpt;$i<$lg;$i+=2){
		if($i==$cpt){
			$tocheck=substr($telwcs,$i,2);
		}else{
			$tocheck=substr($telwcs,$i,2);
		}
		$telok.=$tocheck." ";
	}	

	//On enlève l'espace de fin
	$telok=trim($telok);

	return $telok;
}    

//--------------------------------------------------------------------------------

function getFaxNumber($fax){
		
	$lg = strlen($fax);
	$faxok='';
	
	//On enleve les espaces et les caractères qui ne sont pas des chiffres
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($fax,$i,1);
	
		//On ne garde que les chiffres
		if(is_numeric($tocheck)){
			$faxok.=$tocheck;
		}
	}
	
	return $faxok;
	
}

//--------------------------------------------------------------------------------

function getTitle($idtitle){
	
	$lang = User::getInstance()->getLang();
	
	if(!$idtitle)
		return;
		
	$query = "SELECT title$lang as title FROM title WHERE idtitle='$idtitle'";
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le titre du client" );
		
	return $rs->fields( "title" );
	
}

//--------------------------------------------------------------------------------

function & getExportableArray($req) {

	global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;

	include_once ("$GLOBAL_START_PATH/objects/Encryptor.php");

	$SQL_Query = Encryptor::decrypt(URLFactory::base64url_decode($_GET["req"]), $GLOBAL_DB_PASS);

	$data = array (

		"Com" => array (),
		"Client n°" => array (),
		"Facture n°" => array (),
		"Raison sociale" => array (),
		"Contact (nom)" => array (),
		"Téléphone" => array (),
		"Factor" => array (),
		"Date de facturation" => array (),
		"Date d'échéance" => array (),
		"Retard (jours)" => array (),
		"Total TTC Facture" => array (),
		"Relance tél." => array (),
		"A relancer par tél." => array (),
		
	);

	$rs = & DBUtil::query($SQL_Query);

	while (!$rs->EOF()) {

		$idbuyer 			= $rs->fields( "idbuyer" );
		$idcontact 			= $rs->fields( "idcontact" );
		$idorder 			= $rs->fields( "idorder" );
		list( $conf_order_date, $null )	= explode( " ", $rs->fields( "conf_order_date" ) );
		$deliv_payment		= $rs->fields("deliv_payment");
		$total_amount_ht 	= $rs->fields( "total_amount_ht" );
		$total_amount_ttc 	= $rs->fields( "total_amount" );
		$initials 			= $rs->fields( "initial" );
		$mail				= checkBuyerMail($idbuyer,$idcontact);
		$fax				= checkBuyerFax($idbuyer,$idcontact);
		$idbilling_buyer	= $rs->fields( "idbilling_buyer" );
		$iscontact 			= $rs->fields( "contact" ) == 1;
		$factor				= $rs->fields( "factor" );
		$company			= $rs->fields( "company" );
		$lastname			= $rs->fields( "lastname" );
		$title 				= getTitle($rs->fields( "title" ));
		$phonenumber  		= HumanReadablePhone($rs->fields( "phonenumber" ));
		$DateHeure			= $rs->fields( "DateHeure" );
		list($Date, $Heure) = explode(" ",$DateHeure);
		$invoice_date		= $Date;
		$retard = round((strtotime(date("Y-m-d")) - strtotime($deliv_payment))/(60*60*24)-1);
		
		if($factor==0 || $factor==''){
			$factor=Dictionnary::translate( "no" );
		}else{
			$factor=Dictionnary::translate( "yes" );
		}
		
		
		$rpd1 = $rs->fields( "relaunch_phone_date1" );
		$rpd2 = $rs->fields( "relaunch_phone_date2" );
		$rpd3 = $rs->fields( "relaunch_phone_date3" );
		$rpd4 = $rs->fields( "relaunch_phone_date4" );
		$rpd = '0000-00-00';
		
		if($rpd4!='0000-00-00')
			$rpd = usDate2eu($rpd4);
		else if($rpd3!='0000-00-00')
			$rpd = usDate2eu($rpd3);
		else if($rpd2!='0000-00-00')
			$rpd = usDate2eu($rpd2);
		else if($rpd1!='0000-00-00')
			$rpd = usDate2eu($rpd1);
		
		if($rpd=='0000-00-00')
			$rpd='';
		
		$rpr = $rs->fields( "relaunch_phone_recall" );
		
		if($rpr=='0000-00-00')
			$rpr='';
		else
			$rpr = usDate2eu($rpr);


		$data["Com"][] = $initials;
		$data["Client n°"][] = $idbuyer;
		$data["Facture n°"][] = $idbilling_buyer;
		$data["Raison sociale"][] = $company;
		$data["Contact (nom)"][] = "$title $lastname";
		$data["Téléphone"][] = $phonenumber;
		$data["Factor"][] = $factor;
		$data["Date de facturation"][] = $invoice_date;
		$data["Date d'échéance"][] = $deliv_payment;
		$data["Retard (jours)"][] = $retard;
		$data["Total TTC Facture"][] = str_replace(".",",",$total_amount_ttc);
		$data["Relance tél."][] = $rpd;
		$data["A relancer par tél."][] = $rpr;
		
		$rs->MoveNext();

	}

	return $data;

}

//--------------------------------------------------------------------------------
function displayInvoicesNvX( $niveau = 0 , $sql_cond = "" , $litige = 0 ){

	global	$GLOBAL_START_URL,
			$resultPerPage,
			$globalTotal;
			
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	$iduser = User::getInstance()->getId();
	
	$order_total_amount = 0;
	$order_total_toPay = 0;
	$resultCount = 0;
	
	$search=1;
	$SQL_Condition="";
	if( isset($_GET['sortby']) AND $_GET['sortby']!="" ){
		$SQL_Condition	.= " ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
	}else{
		$SQL_Condition	.= " ORDER BY bb.deliv_payment ASC";
	}
	
	$minbuying=DBUtil::getParameterAdmin('factor_minbuying');
	$factor_condition="";
	if(isset($_POST['factor_pay'])&&($_POST['factor_pay']=="1"))
	{
	$factor_payment=DBUtil::getParameterAdmin('factor_payment');
	$factor_condition=$factor_payment?"AND bb.idpayment <> $factor_payment":"";
	}

	//factures dont la date d'échéance est dépassée
	
	$date = date( "Y-m-d" );
	
	$relaunchCondition = " AND bb.deliv_payment < NOW() ";
	
	switch ($niveau) {
		case 0:    
			$relaunchCondition .= "	AND bb.deliv_payment <> '0000-00-00'
									AND bb.deliv_payment <> ''
									AND bb.deliv_payment <= '".$date."'
									AND bb.relaunch_date_1 = '0000-00-00'";
			break;
		
		case 1:    
			$relaunchCondition .= "	AND bb.relaunch_date_1 <> '0000-00-00'
									AND bb.relaunch_date_2 = '0000-00-00'";
			break;
			
		case 2:
			$relaunchCondition .= "	AND bb.relaunch_date_1 <> '0000-00-00'
									AND bb.relaunch_date_2 <> '0000-00-00'
									AND bb.relaunch_date_3 = '0000-00-00'";
			break;
			
		case 3:
			$relaunchCondition .= "	AND bb.relaunch_date_1 <> '0000-00-00'
									AND bb.relaunch_date_2 <> '0000-00-00'
									AND bb.relaunch_date_3 <> '0000-00-00'";
			break;
			
	}
	
	$query = "
	SELECT bb.idbilling_buyer,
		bb.idorder,
		bb.idbuyer,
		bb.idcontact,
		o.conf_order_date,
		bb.total_amount,
		bb.total_amount_ht,
		bb.deliv_payment,
		bb.factor,
		bb.factor_refusal_date,
		bb.factor_send_date,
		bb.relaunch_date_1,
		u1.initial AS comRelaunch,
		u2.initial AS comOrder,
		b.company,
		b.accountant,
		b.accountant_phonenumber,
		b.accountant_faxnumber,cont.title, 
		cont.lastname, 
		cont.phonenumber, 
		bb.DateHeure, 
		b.contact,
		bb.relaunch_phone_date1,
		bb.relaunch_phone_date2,
		bb.relaunch_phone_date3,
		bb.relaunch_phone_date4,
		bb.relaunch_phone_recall,
		bb.relaunch_phone_time,
		bb.relaunch_phone_hour,
		bb.litigious,
		bb.litigation_date,
		bb.litigation_comment,
		bb.idpayment,
		GREATEST(	bb.relaunch_phone_date1,
					bb.relaunch_phone_date2,
					bb.relaunch_phone_date3,
					bb.relaunch_phone_date4) as relaunch_tri,
		bb.relaunch_paid_InProgress,
		bb.relaunch_litigation,
		bb.relaunch_comment,
		bb.relaunch_lastupdate,
		u1.color
	FROM `order` o, user u2, buyer b, contact cont, billing_buyer bb
	LEFT JOIN user u1 ON u1.iduser = bb.relaunch_iduser
	WHERE bb.relaunch_paid_InProgress = 0
	AND bb.status NOT LIKE '" . Invoice::$STATUS_PAID . "'
	$relaunchCondition 
	AND bb.idorder = o.idorder 
	AND bb.idbuyer = b.idbuyer ".
	$factor_condition."
	AND bb.idcontact = cont.idcontact
	AND bb.idbuyer = cont.idbuyer
	AND o.iduser = u2.iduser
	AND bb.idbuyer = b.idbuyer";
	
	if($litige == 0){
		$query .= " AND bb.litigious = $litige ";
	}else{
		$query .= " AND bb.litigious >= $litige ";
	}
	
	
		/**
		 * ATTENTION LE PARAM DEV EST TEMPORAIRE
		 * POUR LAISSER LE TEMPS A JF DE REFAIRE TOUS LES REGLEMENTS
		 * ANTERIEURS
		 * 
		 * BISOUS LES AMIS
		 * */
	//if( isset( $_GET[ "dev" ] ) )
		$query .= " AND bb.status NOT LIKE '" . Invoice::$STATUS_PAID . "'";	
	/*else $query .= "
	AND ( 	bb.status NOT IN ( '" . Invoice::$STATUS_PAID . "' )
		OR (
			bb.status = 'Paid'
			AND ROUND(bb.total_amount - bb.credit_amount - (	
				SELECT SUM(rb.amount)
				FROM billing_regulations_buyer brb, regulations_buyer rb
				WHERE rb.idregulations_buyer = brb.idregulations_buyer
				AND rb.accept=1
				AND brb.idbilling_buyer = bb.idbilling_buyer
			),2) > 0
		) 
	)";*/

	/*if($minbuying<>"" && $minbuying<>0){
		$query.= "
		AND bb.total_amount<=$minbuying";
	}*/
	
	if( !$hasAdminPrivileges )
		$query .= "
		AND ( b.iduser = '$iduser' OR bb.iduser = '$iduser' )";
	
	$query .= $sql_cond;	
	$query .= " ".$SQL_Condition;
	
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		die( "Impossible de trouver les factures en retard de paiement" );
	
	//Encore un truc pourri pour masquer ce qui ne va pas. Tant qu'on n'aura pas des statuts fiables (et donc que la requête pourra être uniquement basée sur les statuts), on laisse ça et on n'utilise pas $rs->RecordCount()
	//C'est moche mais c'est la vie
	$resultCount = $rs->RecordCount();
	$totToPay = 0 ;
	while( !$rs->EOF() ){
		$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		$totToPay += round( $Invoice->getBalanceOutstanding(), 2 );
		$rs->MoveNext();
	}
	$rs->MoveFirst();
	
	/*while( !$rs->EOF ){
		
		//$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		
		//if( round( $Invoice->getBalanceOutstanding(), 2 ) != 0 )
			$resultCount++;
		
		$rs->MoveNext();
		
	}*/
	
	if( !$resultCount ){
	
		?>
			<div id="Niveau<?=$niveau?>">
	        	<div class="headTitle">
	                <p class="title">Aucune facture </p>
					<div class="rightContainer"></div>
	            </div>
			</div>
		<?php
			
		return;
		
	}else{
	
		?>
			<div id="Niveau<?=$niveau?>">
	        	<div class="headTitle">
	                <a name="nv1"></a><p class="title">
	                	<?php if($niveau == 0){?>
	                		<?php echo $resultCount ?> facture<?php echo $resultCount == 1 ? "" : "s" ?> en retard de paiement non relancée<?php echo $resultCount == 1 ? "" : "s" ?> - Montant <?php echo Util::priceFormat($totToPay) ?>
	                	<?php }else{ ?>
	                		<?if($niveau != 4){?>
			                	<?php echo $resultCount ?> facture<?php echo $resultCount == 1 ? "" : "s" ?> relancée<?php echo $resultCount == 1 ? "" : "s" ?>
			                	<?php if($niveau<3){ ?>
			                	 	de niveau <?=$niveau?>  - Montant <?php echo Util::priceFormat($totToPay) ?>
			                	<?php }
									if($niveau==3){ ?>
			                		par courrier  - Montant <?php echo Util::priceFormat($totToPay) ?>
			                	<?php } ?>
		                	<?php }?>
		                <?php } ?>
		                <?
		                if($niveau == 4){?>
		                	<?php echo $resultCount ?> facture<?php echo $resultCount == 1 ? "" : "s" ?> litigieuse<?php echo $resultCount == 1 ? "" : "s" ?> - Montant <?php echo Util::priceFormat($totToPay) ?>
		                <?php } 
		                ?>
		                </p>
					<div class="rightContainer"></div>
	            </div>
	   			<div class="subContent">
				<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
  						        <th>Resp. Rel.</th>
                        		<th>Com cde</th>
      			                <th>Client n°</th>
            		            <th>Facture n°</th>
                   			    <th>Raison sociale</th>
     		                    <th>Règlement</th>
           		            	<th>Date de facturation</th>
               		            <th>Date d'échéance</th>
                   		        <th>Retard (jours)</th>
                       		    <th>Total TTC facture</th>
                                <th>Solde à payer</th>

                            	<?
								switch ($niveau) {

									case 0:
                                		?>
                                		<th>Relancer niv. 1<br />email</th>
                                		<th>Relancer niv. 1<br />fax</th>
                            			<?
                            			break;
                            			
									case 1:    
										?>
                                		<th>Date relance niv. 1</th>
                                		<th>Jours depuis relance niv. 1</th>
                                		<th>Relancer niv. 2<br />email</th>
                                		<th>Relancer niv. 2<br />fax</th>
 										<?
 										break;
 									
 									case 2:	
 										?>
		                                <th>Date relance niv. 2</th>
		                                <th>Jours depuis relance niv. 2</th>
		                                <th>Relancer niv. 3<br />email</th>
		                                <th>Relancer niv. 3<br />fax</th>
		                    			<?
		                    			break;
		                    		
		                    		case 3:	
		                    			?>
		                                <th>Date relance niv. 3</th>
		                                <th>Jours depuis relance niv. 3</th>
		                                <th>Recommandé</th>
                                		<?
                                		break;
                                		
                                	case 4:
 										?>
		                                <th>Date constat litige</th>
		                                <th>Commentaire litige</th>
		                    			<?
		                    			break;
								}
								
								if($niveau != 4){                                
	                            	?>
	                        		<th>Date relance tél.</th>
	                        		<th>A relancer par tél.</th>
	                        		<th>Fiche suivi</th>
	                        		<?php if($niveau==0 || $niveau==1 || $niveau==2){?>
	                        			<th>Aperçu mail</th>
	                        		<?php }
								}?>
                            
                            </tr>
							<!-- petites flèches de tri -->
                            <tr>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u1.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u1.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u2.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u2.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbilling_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbilling_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('b.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('b.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.factor','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.factor','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.DateHeure','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.DateHeure','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.total_amount','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.total_amount','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <?php
                                if($niveau != 4){
	                                if($niveau > 0){ ?>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('bb.relaunch_date_<?=$niveau?>','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('bb.relaunch_date_<?=$niveau?>','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('bb.relaunch_date_<?=$niveau?>','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                	<a href="javascript:SortResult('bb.relaunch_date_<?=$niveau?>','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
	                                <?php } ?>
	                                <th class="noTopBorder"></th>
	                                <?php if($niveau != 3){ ?>
	                                	<th class="noTopBorder"></th>
	                                	<th class="noTopBorder"></th>
	                                <?php }else{ ?>
	                                	<th class="noTopBorder"></th>
	                                <?php } ?>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult('relaunch_tri','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                	<a href="javascript:SortResult('relaunch_tri','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult('bb.relaunch_phone_recall','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult('bb.relaunch_phone_recall','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
	                                <th class="noTopBorder"></th>
	                        		<?php if($niveau==0 || $niveau==1 || $niveau==2){?>
	                                	<th class="noTopBorder"></th>
	                        		<?php }
                                }else{
                                	?>
                                	<th class="noTopBorder"></th>
	                                <th class="noTopBorder">
	                                	<a href="javascript:SortResult('bb.litigation_date','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
	                                    <a href="javascript:SortResult('bb.litigation_date','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
	                                </th>
                                	<th class="noTopBorder"></th>
	                              	<?
                                }
                        		?>
                        	</tr>
                            </thead>
							<?php
							
							$order_total_amount_ht = 0.0;
							$order_total_amount = 0.0;
							$order_total_toPay = 0.0;
							
							$rs->MoveFirst();
							
							while( !$rs->EOF() ){
								
								$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
								
								$netToPay = round( $Invoice->getBalanceOutstanding(), 2 );
								
								//On n'affiche pas les factures qui sont passées au travers des mailles du filet et qui pourtant ont un solde nul
								//if( $netToPay != 0 ){
									
									$idbuyer 				= $rs->fields( "idbuyer" );
									$idcontact 				= $rs->fields( "idcontact" );
									$idorder 				= $rs->fields( "idorder" );
									list( $conf_order_date, $null )	= explode( " ", $rs->fields( "conf_order_date" ) );
									$deliv_payment			= $rs->fields("deliv_payment");
									$idpayment				= getPayment( $rs->fields( "idpayment" ) );
									$total_amount_ht 		= $rs->fields( "total_amount_ht" );
									$total_amount_ttc 		= $rs->fields( "total_amount" );
									$idbilling_buyer		= $rs->fields( "idbilling_buyer" );
									$relaunch_date_1		= $rs->fields("relaunch_date_1");
									$mail					= checkBuyerMail($idbuyer,$idcontact);
									$fax					= checkBuyerFax($idbuyer,$idcontact);
									$iscontact 				= $rs->fields( "contact" ) == 1;
									$factor					= $rs->fields( "factor" );
									$company				= $rs->fields( "company" );
									$lastname				= $rs->fields( "lastname" );
									$title 					= getTitle($rs->fields( "title" ));
									$phonenumber  			= HumanReadablePhone($rs->fields( "phonenumber" ));
									$accountant				= $rs->fields( "accountant" );
									$accountant_phonenumber	= $rs->fields( "accountant_phonenumber" );
									$accountant_faxnumber	= $rs->fields( "accountant_faxnumber" );
									$DateHeure				= $rs->fields( "DateHeure" );
									$litigation_date		= $rs->fields( "litigation_date" );
									$litigation_comment		= $rs->fields( "litigation_comment" );
									list($Date, $Heure) 	= explode(" ",$DateHeure);
									$invoice_date			= $Date;
									$retard = round((strtotime(date("Y-m-d")) - strtotime($deliv_payment))/(60*60*24)-1);
									$rel1 = round((strtotime(date("Y-m-d")) - strtotime($relaunch_date_1))/(60*60*24)-1); 
									
									if($factor==0 || $factor==''){
										$factor=Dictionnary::translate( "no" );
									}else{
										$factor=Dictionnary::translate( "yes" );
									}
									$order_total_amount_ht += $total_amount_ht;
									$order_total_amount += $total_amount_ttc;
									$order_total_toPay += $netToPay;
									$type = $iscontact ? "contact" : "buyer";
									$buyerHREF = "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer&amp;type=$type";
									$orderHREF = "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$idorder";
									$invoiceHREF = "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer";
									
									$rpd1 = $rs->fields( "relaunch_phone_date1" );
									$rpd2 = $rs->fields( "relaunch_phone_date2" );
									$rpd3 = $rs->fields( "relaunch_phone_date3" );
									$rpd4 = $rs->fields( "relaunch_phone_date4" );
									$rpd = '0000-00-00';
									
									if($rpd4!='0000-00-00')
										$rpd = $rpd4;
									else if($rpd3!='0000-00-00')
										$rpd = $rpd3;
									else if($rpd2!='0000-00-00')
										$rpd = $rpd2;
									else if($rpd1!='0000-00-00')
										$rpd = $rpd1;
									
									if($rpd=='0000-00-00')
										$rpd='';
									
									$class = '';
									if( substr($rs->fields( "relaunch_lastupdate" ),0,10) == date("Y-m-d") ){
										$class = " class=\"todayLines\"";
									}
									
									$rpr = $rs->fields( "relaunch_phone_recall" );
									
									if($rpr=='0000-00-00')
										$rpr='';
									else
										$rpr = $rpr;
										
									$rpt = $rs->fields( "relaunch_phone_time" );
								
									switch( $rpt ){
										case "Après-Midi": $time=" PM";break;
										case "Matin": $time=" AM";break;
										case "Indifférent":	$time='';break;
										default : $time='';
									}
				
									$rph = $rs->fields( "relaunch_phone_hour" );
									
									$paidInProgress = $rs->fields( "relaunch_paid_InProgress" );
									$relaunch_litigation = $rs->fields( "relaunch_litigation" );
									$relaunch_comment = $rs->fields( "relaunch_comment");
									
									//couleur par commercial
								
									$colorclass='';
									$color = $rs->fields("color");
				
									if( $color != '' )
										$colorclass=" style=\"color:$color;\"";	
									
								?>
                             <script type="text/javascript">
							/* <![CDATA[ */
							    $(document).ready(function(){
							        $("#helpBuyerNotPaidInvoices1_<?=$idbilling_buyer;?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Nom du contact :</b> <?php echo "$title $lastname" ?><?php if( $phonenumber != "" ){ ?> <br /><b>Téléphone :</b> <?php echo $phonenumber ?><?php } ?><?php if( $accountant != "" ){ ?> <br /><b>Comptable :</b> <?php echo $accountant ?><?php } ?><?php if( $accountant_phonenumber != "" ){ ?> <br /><b>Tél. comptable :</b> <?php echo $accountant_phonenumber ?><?php } ?><?php if( $accountant_faxnumber != "" ){ ?> <br /><b>Fax comptable :</b> <?php echo $accountant_faxnumber ?><?php } ?>',
							            position: {
							                corner: {
							                    target: "rightMiddle",
							                    tooltip: "leftMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							    
							     $(document).ready(function(){
							        $("#helpComment_<?=$idbilling_buyer;?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Commentaires :</b><br /> <?php echo str_replace("\r\n","",stripslashes(nl2br($relaunch_comment))) ?>',
							            position: {
							                corner: {
							                    target: "leftMiddle",
							                    tooltip: "rightMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							/* ]]> */
							</script>
							<tr<?=$class?>>
								<td class="lefterCol"<?=$colorclass?>><?php echo $rs->fields( "comRelaunch" ) ?></td>
								<td><?php echo $rs->fields( "comOrder" ) ?></td>
								<td><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbuyer ?></a></td>
								<td class="grasBack"><a href="<?php echo $invoiceHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbilling_buyer ?></a></td>
								<td class="grasBack"><div id="helpBuyerNotPaidInvoices<?=$niveau+1?>_<?=$idbilling_buyer?>"><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $company ?></a><br /><?php echo $lastname ?></div></td>
							<!--	<td><a href="<?php echo $buyerHREF ?>#factor" onclick="window.open(this.href); return false;"><?php echo $factor ?></a></td> -->
								<td><?php echo $idpayment ?></td>
								<td><?php echo Util::dateFormatEu( $invoice_date, false, "-", true ) ?></td>
								<td><?php echo Util::dateFormatEu( $deliv_payment, false, "-", true ) ?></td>
								<td><?php echo $retard ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_ttc ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $netToPay ) ?></td>
								
								<?php
								if($niveau != 4){
									if($niveau > 0){ ?>
										<td><?php echo Util::dateFormatEu( $relaunch_date_1, false, "-", true ) ?></td>
										<td style="text-align:center; vertical-align:top;"><?php echo Util::dateFormatEu( $rel1, false, "-", true ) ?></td>
									<?php } ?>
									
									<?php if($niveau != 3){ ?>
										<td style="text-align:center;">
											<?php if($mail!==false){?><input type="checkbox" id="mail<?=$niveau+1?>_<?php echo $idbilling_buyer ?>" name="relaunch_<?=$niveau+1?>[]" value="<?=$idbilling_buyer?>" onclick="javascript:checkuncheck('<?=$niveau+1?>_<?php echo $idbilling_buyer ?>','mail');"/><?php }else{ ?>NC<?php } ?>
										</td>
										<td style="text-align:center;">
											<?php if($fax!==false){?><input type="checkbox" id="fax<?=$niveau+1?>_<?php echo $idbilling_buyer ?>" name="relaunch_<?=$niveau+1?>fax[]" value="<?php echo $idbilling_buyer ?>" onclick="javascript:checkuncheck('<?=$niveau+1?>_<?php echo $idbilling_buyer ?>','fax');"/><?php }else{ ?>NC<?php } ?>
										</td>
										<td style="text-align:center; vertical-align:top;"><?php echo Util::dateFormatEu( $rpd, false, "-", true ) ?></td>
									<?php }else{ ?>
										<td><a href="<?=$GLOBAL_START_URL?>/accounting/get_invoice_letter.php?idbilling=<?=$idbilling_buyer?>" />Word</a></td>
										<td style="text-align:center; vertical-align:top;"><?php echo Util::dateFormatEu( $rpd, false, "-", true ) ?></td>
									<?php } ?>
									<td style="text-align:center; vertical-align:top;"><?php echo Util::dateFormatEu( $rpr, false, "-", true ) ?><?=$time."<br />".$rph?></td>
									<td style="text-align:center; vertical-align:top;">
										<div id="helpComment_<?=$idbilling_buyer?>"><a href="#" onclick="popupcomment('<?=$idbilling_buyer?>'); return false;">Voir</a></div>
										<?php if( $rs->fields( "factor_refusal_date" ) != "0000-00-00" ){ ?><br /><span style="color:#FF0000;">Débité factor</span><?php } ?>
										<?php if( $rs->fields( "factor_refusal_date" ) == "0000-00-00" && $rs->fields( "factor_send_date" ) != "0000-00-00" ){ ?>
											<br /><span style="color:#FF0000;">Financé par le factor</span>
										<?php } ?>
										<?php
										
										$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1" );
										
										if( $rsLitigation->RecordCount() ){
											
											?><br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open(this.href); return false;" style="color:#EB6A0A;">Litige</a><?php
												
										}
																				
										if($relaunch_litigation){
											echo"<br /><span style=\"color:#FF0000;\">Paiement litigieux</span>";
										}
										
										?>
									</td>
									<?php if($niveau==0 || $niveau==1 || $niveau==2){?>
										<td class="righterCol"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/transmit_relaunch.php?idbilling_buyer=<?=$idbilling_buyer?>&amp;level=<?=$niveau+1?>">Mail</a></td>
									<?php }
								}else{
									?>
									<td style="text-align:center;"><?php echo Util::dateFormatEu( $litigation_date ) ?></td>
									<td class="righterCol"><?=stripslashes($litigation_comment)?></td>
									<?	
								}?>
							</tr>
						<?php 
								
							//}
							
							$rs->MoveNext();
							
						}
						switch($niveau){
							case 0 :
								$globalTotal[ "count" ][ 0 ] += $resultCount;
								$globalTotal[ "count" ][ 1 ] = $resultCount;
								$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
								$globalTotal[ "amount" ][ 1 ] = $order_total_amount;
								$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
								$globalTotal[ "toPay" ][ 1 ] = $order_total_toPay;
								break;
		
							case 1:
								$globalTotal[ "count" ][ 0 ] += $resultCount;
								$globalTotal[ "count" ][ 2 ] = $resultCount;
								$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
								$globalTotal[ "amount" ][ 2 ] = $order_total_amount;
								$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
								$globalTotal[ "toPay" ][ 2 ] = $order_total_toPay;
								break;
							
							case 2:	
								$globalTotal[ "count" ][ 0 ] += $resultCount;
								$globalTotal[ "count" ][ 3 ] = $resultCount;
								$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
								$globalTotal[ "amount" ][ 3 ] = $order_total_amount;
								$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
								$globalTotal[ "toPay" ][ 3 ] = $order_total_toPay;
								break;
							
							case 3:	
								$globalTotal[ "count" ][ 0 ] += $resultCount;
								$globalTotal[ "count" ][ 4 ] = $resultCount;
								$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
								$globalTotal[ "amount" ][ 4 ] = $order_total_amount;
								$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
								$globalTotal[ "toPay" ][ 4 ] = $order_total_toPay;
								break;
								
							case 4:
								$globalTotal[ "count" ][ 0 ] += $resultCount;
								$globalTotal[ "count" ][ 5 ] = $resultCount;
								$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
								$globalTotal[ "amount" ][ 5 ] = $order_total_amount;
								$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
								$globalTotal[ "toPay" ][ 5 ] = $order_total_toPay;
								break;
						}
						?>
							<tr>
								<th style="border-style:none;" colspan="9"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_amount ) ?></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_toPay ) ?></th>
								<th style="border-style:none;" colspan="8"></th>
							</tr>
						</tbody>
                        </table>
                        </div>
                        	<?if($niveau<3){?>
                           		<div class="submitButtonContainer">
                            		<input type="submit" name="RelaunchButton_<?=$niveau+1?>" value="Relance niveau <?=$niveau+1?>" onclick="javascript:return confirm('Relancer les factures sélectionnées ?');" class="blueButton" />
                       			</div>
	                       	<?php } ?>
                       	</div>
					</div>
	<?php 	
	}
	

}

//--------------------------------------------------------------------------------
function displayInvoicesToCall( $sql_cond = "" ){
	global	$GLOBAL_START_URL,
			$resultPerPage,
			$globalTotal;
			
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	$iduser = User::getInstance()->getId();
	
	$order_total_amount = 0;
	$order_total_toPay = 0;
	$resultCount = 0;
	
	$search=1;
	$SQL_Condition="";
	if( isset($_GET['sortby']) AND $_GET['sortby']!="" ){
		$SQL_Condition	.= " ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
	}else{
		$SQL_Condition	.= " ORDER BY bb.deliv_payment ASC";
	}
	
	$minbuying=DBUtil::getParameterAdmin('factor_minbuying');
	$factor_condition="";
	if(isset($_POST['factor_pay'])&&($_POST['factor_pay']=="1"))
	{
	$factor_payment=DBUtil::getParameterAdmin('factor_payment');
	$factor_condition=$factor_payment?"AND bb.idpayment <> $factor_payment":"";
	}
	//factures dont la date d'échéance est dépassée
	
	$date = date( "Y-m-d" );
	
	$relaunchCondition = " AND bb.deliv_payment < NOW() ";
		
	$query = "
	SELECT bb.idbilling_buyer,
		bb.idorder,
		bb.idbuyer,
		bb.idcontact,
		o.conf_order_date,
		bb.total_amount,
		bb.total_amount_ht,
		bb.deliv_payment,
		bb.factor,
		bb.factor_refusal_date,
		bb.factor_send_date,
		bb.relaunch_date_1,
		bb.relaunch_date_2,
		bb.relaunch_date_3,
		u1.initial AS comRelaunch,
		u2.initial AS comOrder,
		b.company,
		b.accountant,
		b.accountant_phonenumber,
		b.accountant_faxnumber,cont.title, 
		cont.lastname, 
		cont.phonenumber, 
		bb.DateHeure, 
		b.contact,
		bb.relaunch_phone_date1,
		bb.relaunch_phone_date2,
		bb.relaunch_phone_date3,
		bb.relaunch_phone_date4,
		bb.relaunch_phone_recall,
		bb.relaunch_phone_time,
		bb.relaunch_phone_hour,
		bb.litigious,
		bb.litigation_date,
		bb.litigation_comment,
		bb.relaunch_paid_InProgress,
		bb.relaunch_paid_InProgress_date,
		GREATEST(	bb.relaunch_phone_date1,
					bb.relaunch_phone_date2,
					bb.relaunch_phone_date3,
					bb.relaunch_phone_date4) as relaunch_tri,
		bb.relaunch_paid_InProgress,
		bb.relaunch_litigation,
		bb.relaunch_comment,
		bb.relaunch_lastupdate,
		u1.color
	FROM `order` o, user u2, buyer b, contact cont, billing_buyer bb
	LEFT JOIN user u1 ON u1.iduser = bb.relaunch_iduser
	WHERE bb.relaunch_paid_InProgress = 0
	AND relaunch_phone_recall <= NOW()
	AND bb.status NOT LIKE '" . Invoice::$STATUS_PAID . "'
	$relaunchCondition 
	AND bb.idorder = o.idorder ".
	$factor_condition."
	AND bb.idbuyer = b.idbuyer
	AND bb.idcontact = cont.idcontact
	AND bb.idbuyer = cont.idbuyer
	AND o.iduser = u2.iduser
	AND bb.idbuyer = b.idbuyer";
	
	$query .= " AND bb.litigious = 0 ";
	
	if( !$hasAdminPrivileges )
		$query .= "
		AND ( b.iduser = '$iduser' OR bb.iduser = '$iduser' )";
	
	$query .= $sql_cond;	
	$query .= "	HAVING relaunch_tri < relaunch_phone_recall";
	$query .= " ".$SQL_Condition;
	
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		die( "Impossible de trouver les factures en retard de paiement" );
	
	//Encore un truc pourri pour masquer ce qui ne va pas. Tant qu'on n'aura pas des statuts fiables (et donc que la requête pourra être uniquement basée sur les statuts), on laisse ça et on n'utilise pas $rs->RecordCount()
	//C'est moche mais c'est la vie
	$resultCount = $rs->RecordCount();
	$totToPay = 0 ;
	while( !$rs->EOF() ){
		$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		$totToPay += round( $Invoice->getBalanceOutstanding(), 2 );
		$rs->MoveNext();
	}
	$rs->MoveFirst();
	
	if( !$resultCount ){
	
		?>
			<div id="toCall">
	        	<div class="headTitle">
	                <p class="title">Aucune facture </p>
					<div class="rightContainer"></div>
	            </div>
			</div>
		<?php
			
		return;
		
	}else{
	
		?>
			<div id="toCall">
	        	<div class="headTitle">
	                <a name="toC"></a>
	                <p class="title">
	                	<?php echo $resultCount ?> facture<?php echo $resultCount == 1 ? "" : "s" ?> à appeler
		            </p>
					<div class="rightContainer"></div>
	            </div>
	   			<div class="subContent">
				<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
  						        <th>Resp. Rel.</th>
                        		<th>Com cde</th>
      			                <th>Client n°</th>
            		            <th>Facture n°</th>
                   			    <th>Raison sociale</th>
     		                    <th>Factor</th>
           		            	<th>Date de facturation</th>
               		            <th>Date d'échéance</th>
                   		        <th>Retard (jours)</th>
                       		    <th>Total TTC facture</th>
                                <th>Solde à payer</th>
                           		<th>Date relance niv. 1</th>
                                <th>Date relance niv. 2</th>
                                <th>Date relance niv. 3</th>
                        		<th>Date relance tél.</th>
	                        	<th>Fiche suivi</th>
                            </tr>
							<!-- petites flèches de tri -->
                            <tr>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u1.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u1.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u2.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u2.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbilling_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbilling_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('b.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('b.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.factor','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.factor','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.DateHeure','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.DateHeure','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.total_amount','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.total_amount','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_date_1','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_date_1','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_date_2','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_date_2','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_date_3','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_date_3','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_tri','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_tri','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                        	</tr>
                            </thead>
							<?php
							
							$order_total_amount_ht = 0.0;
							$order_total_amount = 0.0;
							$order_total_toPay = 0.0;
							
							$rs->MoveFirst();
							
							while( !$rs->EOF() ){
								
								$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
								
								$netToPay = round( $Invoice->getBalanceOutstanding(), 2 );
								
								//On n'affiche pas les factures qui sont passées au travers des mailles du filet et qui pourtant ont un solde nul
								//if( $netToPay != 0 ){
									
									$idbuyer 				= $rs->fields( "idbuyer" );
									$idcontact 				= $rs->fields( "idcontact" );
									$idorder 				= $rs->fields( "idorder" );
									list( $conf_order_date, $null )	= explode( " ", $rs->fields( "conf_order_date" ) );
									$deliv_payment			= $rs->fields("deliv_payment");
									$total_amount_ht 		= $rs->fields( "total_amount_ht" );
									$total_amount_ttc 		= $rs->fields( "total_amount" );
									$idbilling_buyer		= $rs->fields( "idbilling_buyer" );
									$relaunch_date_1		= $rs->fields("relaunch_date_1") && $rs->fields("relaunch_date_1")!="0000-00-00" ? $rs->fields("relaunch_date_1") : "-";
									$relaunch_date_2		= $rs->fields("relaunch_date_2") && $rs->fields("relaunch_date_2")!="0000-00-00" ? $rs->fields("relaunch_date_2") : "-";
									$relaunch_date_3		= $rs->fields("relaunch_date_3") && $rs->fields("relaunch_date_3")!="0000-00-00" ? $rs->fields("relaunch_date_3") : "-";
									$mail					= checkBuyerMail($idbuyer,$idcontact);
									$fax					= checkBuyerFax($idbuyer,$idcontact);
									$iscontact 				= $rs->fields( "contact" ) == 1;
									$factor					= $rs->fields( "factor" );
									$company				= $rs->fields( "company" );
									$lastname				= $rs->fields( "lastname" );
									$title 					= getTitle($rs->fields( "title" ));
									$phonenumber  			= HumanReadablePhone($rs->fields( "phonenumber" ));
									$accountant				= $rs->fields( "accountant" );
									$accountant_phonenumber	= $rs->fields( "accountant_phonenumber" );
									$accountant_faxnumber	= $rs->fields( "accountant_faxnumber" );
									$DateHeure				= $rs->fields( "DateHeure" );
									$litigation_date		= $rs->fields( "litigation_date" );
									$litigation_comment		= $rs->fields( "litigation_comment" );
									list($Date, $Heure) 	= explode(" ",$DateHeure);
									$invoice_date			= $Date;
									$retard = round((strtotime(date("Y-m-d")) - strtotime($deliv_payment))/(60*60*24)-1);
									$rel1 = round((strtotime(date("Y-m-d")) - strtotime($relaunch_date_1))/(60*60*24)-1); 
									
									if($factor==0 || $factor==''){
										$factor=Dictionnary::translate( "no" );
									}else{
										$factor=Dictionnary::translate( "yes" );
									}
									$order_total_amount_ht += $total_amount_ht;
									$order_total_amount += $total_amount_ttc;
									$order_total_toPay += $netToPay;
									$type = $iscontact ? "contact" : "buyer";
									$buyerHREF = "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer&amp;type=$type";
									$orderHREF = "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$idorder";
									$invoiceHREF = "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer";
									
									$rpd1 = $rs->fields( "relaunch_phone_date1" );
									$rpd2 = $rs->fields( "relaunch_phone_date2" );
									$rpd3 = $rs->fields( "relaunch_phone_date3" );
									$rpd4 = $rs->fields( "relaunch_phone_date4" );
									$rpd = '0000-00-00';
									
									if($rpd4!='0000-00-00')
										$rpd = $rpd4;
									else if($rpd3!='0000-00-00')
										$rpd = $rpd3;
									else if($rpd2!='0000-00-00')
										$rpd = $rpd2;
									else if($rpd1!='0000-00-00')
										$rpd = $rpd1;
									
									if($rpd=='0000-00-00')
										$rpd='';
									
									$class = '';
									if( substr($rs->fields( "relaunch_lastupdate" ),0,10) == date("Y-m-d") ){
										$class = " class=\"todayLines\"";
									}
									
									$rpr = $rs->fields( "relaunch_phone_recall" );
									
									if($rpr=='0000-00-00')
										$rpr='';
									else
										$rpr = $rpr;
										
									$rpt = $rs->fields( "relaunch_phone_time" );
								
									switch( $rpt ){
										case "Après-Midi": $time=" PM";break;
										case "Matin": $time=" AM";break;
										case "Indifférent":	$time='';break;
										default : $time='';
									}
				
									$rph = $rs->fields( "relaunch_phone_hour" );
									
									$paidInProgress = $rs->fields( "relaunch_paid_InProgress" );
									$relaunch_litigation = $rs->fields( "relaunch_litigation" );
									$relaunch_comment = $rs->fields( "relaunch_comment");
									
									//couleur par commercial
								
									$colorclass='';
									$color = $rs->fields("color");
				
									if( $color != '' )
										$colorclass=" style=\"color:$color;\"";	
									
								?>
                             <script type="text/javascript">
							/* <![CDATA[ */
							    $(document).ready(function(){
							        $("#helpBuyerNotPaidInvoices1_<?=$idbilling_buyer;?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Nom du contact :</b> <?php echo "$title $lastname" ?><?php if( $phonenumber != "" ){ ?> <br /><b>Téléphone :</b> <?php echo $phonenumber ?><?php } ?><?php if( $accountant != "" ){ ?> <br /><b>Comptable :</b> <?php echo $accountant ?><?php } ?><?php if( $accountant_phonenumber != "" ){ ?> <br /><b>Tél. comptable :</b> <?php echo $accountant_phonenumber ?><?php } ?><?php if( $accountant_faxnumber != "" ){ ?> <br /><b>Fax comptable :</b> <?php echo $accountant_faxnumber ?><?php } ?>',
							            position: {
							                corner: {
							                    target: "rightMiddle",
							                    tooltip: "leftMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							    
							     $(document).ready(function(){
							        $("#helpComment_<?=$idbilling_buyer;?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Commentaires :</b><br /> <?php echo str_replace("\r\n","",stripslashes(nl2br($relaunch_comment))) ?>',
							            position: {
							                corner: {
							                    target: "leftMiddle",
							                    tooltip: "rightMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							/* ]]> */
							</script>
							<tr<?=$class?>>
								<td class="lefterCol"<?=$colorclass?>><?php echo $rs->fields( "comRelaunch" ) ?></td>
								<td><?php echo $rs->fields( "comOrder" ) ?></td>
								<td><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbuyer ?></a></td>
								<td class="grasBack"><a href="<?php echo $invoiceHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbilling_buyer ?></a></td>
								<td class="grasBack"><div id="helpBuyerNotPaidInvoices<?=$niveau+1?>_<?=$idbilling_buyer?>"><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $company ?></a><br /><?php echo $lastname ?></div></td>
								<td><a href="<?php echo $buyerHREF ?>#factor" onclick="window.open(this.href); return false;"><?php echo $factor ?></a></td>
								<td><?php echo Util::dateFormatEu( $invoice_date, false, "-", true ) ?></td>
								<td><?php echo Util::dateFormatEu( $deliv_payment, false, "-", true ) ?></td>
								<td><?php echo $retard ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_ttc ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $netToPay ) ?></td>
								<td ><?php echo Util::dateFormatEu( $relaunch_date_1, false, "-", true ) ?></td>
								<td ><?php echo Util::dateFormatEu( $relaunch_date_2, false, "-", true ) ?></td>
								<td ><?php echo Util::dateFormatEu( $relaunch_date_3, false, "-", true ) ?></td>
								<td ><?php echo Util::dateFormatEu( $rpd, false, "-", true ) ?></td>
								<td class="righterCol">
									<div id="helpComment_<?=$idbilling_buyer?>"><a href="#" onclick="popupcomment('<?=$idbilling_buyer?>'); return false;">Voir</a></div>
									<?php if( $rs->fields( "factor_refusal_date" ) != "0000-00-00" ){ ?><br /><span style="color:#FF0000;">Débité factor</span><?php } ?>
									<?php if( $rs->fields( "factor_refusal_date" ) == "0000-00-00" && $rs->fields( "factor_send_date" ) != "0000-00-00" ){ ?>
										<br /><span style="color:#FF0000;">Financé par le factor</span>
									<?php } ?>
									<?php
									
									$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1" );
									
									if( $rsLitigation->RecordCount() ){
										
										?><br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open(this.href); return false;" style="color:#EB6A0A;">Litige</a><?php
											
									}
																		
									if($relaunch_litigation){
										echo"<br /><span style=\"color:#FF0000;\">Paiement litigieux</span>";
									}
									
									?>
								</td>
							</tr>
						<?php 
								
							//}
							
							$rs->MoveNext();
							
						}
						$globalTotal[ "count" ][ 0 ] += $resultCount;
						$globalTotal[ "count" ][ 7 ] = $resultCount;
						$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
						$globalTotal[ "amount" ][ 7 ] = $order_total_amount;
						$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
						$globalTotal[ "toPay" ][ 7 ] = $order_total_toPay;
						?>
							<tr>
								<th style="border-style:none;" colspan="9"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_amount ) ?></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_toPay ) ?></th>
								<th style="border-style:none;" colspan="8"></th>
							</tr>
						</tbody>
                        </table>
                        </div>
                       	</div>
					</div>
	<?php 	
	}
}
//--------------------------------------------------------------------------------
function displayInvoicesRegulationToCome( $sql_cond = "" ){
	global	$GLOBAL_START_URL,
			$resultPerPage,
			$globalTotal;
			
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	$iduser = User::getInstance()->getId();
	
	$order_total_amount = 0;
	$order_total_toPay = 0;
	$resultCount = 0;
	
	$search=1;
	$SQL_Condition="";
	if( isset($_GET['sortby']) AND $_GET['sortby']!="" ){
		$SQL_Condition	.= " ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
	}else{
		$SQL_Condition	.= " ORDER BY bb.deliv_payment ASC";
	}
	
	$minbuying=DBUtil::getParameterAdmin('factor_minbuying');
	$factor_condition="";
	if(isset($_POST['factor_pay'])&&($_POST['factor_pay']=="1"))
	{
	$factor_payment=DBUtil::getParameterAdmin('factor_payment');
	$factor_condition=$factor_payment?"AND bb.idpayment <> $factor_payment":"";
	}
	//factures dont la date d'échéance est dépassée
	
	$date = date( "Y-m-d" );
	
	$relaunchCondition = " AND bb.deliv_payment < NOW() ";
		
	$query = "
	SELECT bb.idbilling_buyer,
		bb.idorder,
		bb.idbuyer,
		bb.idcontact,
		o.conf_order_date,
		bb.total_amount,
		bb.total_amount_ht,
		bb.deliv_payment,
		bb.factor,
		bb.factor_refusal_date,
		bb.factor_send_date,
		bb.relaunch_date_1,
		bb.relaunch_date_2,
		bb.relaunch_date_3,
		u1.initial AS comRelaunch,
		u2.initial AS comOrder,
		b.company,
		b.accountant,
		b.accountant_phonenumber,
		b.accountant_faxnumber,cont.title, 
		cont.lastname, 
		cont.phonenumber, 
		bb.DateHeure, 
		b.contact,
		bb.relaunch_phone_date1,
		bb.relaunch_phone_date2,
		bb.relaunch_phone_date3,
		bb.relaunch_phone_date4,
		bb.relaunch_phone_recall,
		bb.relaunch_phone_time,
		bb.relaunch_phone_hour,
		bb.litigious,
		bb.litigation_date,
		bb.litigation_comment,
		bb.relaunch_paid_InProgress,
		bb.relaunch_paid_InProgress_date,
		GREATEST(	bb.relaunch_phone_date1,
					bb.relaunch_phone_date2,
					bb.relaunch_phone_date3,
					bb.relaunch_phone_date4) as relaunch_tri,
		bb.relaunch_paid_InProgress,
		bb.relaunch_litigation,
		bb.relaunch_comment,
		bb.relaunch_lastupdate,
		u1.color
	FROM `order` o, user u2, buyer b, contact cont, billing_buyer bb
	LEFT JOIN user u1 ON u1.iduser = bb.relaunch_iduser
	WHERE bb.relaunch_paid_InProgress = 1
	AND bb.status NOT LIKE '" . Invoice::$STATUS_PAID . "'
	$relaunchCondition 
	AND bb.idorder = o.idorder 
	AND bb.idbuyer = b.idbuyer
	AND bb.idcontact = cont.idcontact
	AND bb.idbuyer = cont.idbuyer
	".$factor_condition."
	AND o.iduser = u2.iduser
	AND bb.idbuyer = b.idbuyer";
	
	$query .= " AND bb.litigious = 0 ";
	
	if( !$hasAdminPrivileges )
		$query .= "
		AND ( b.iduser = '$iduser' OR bb.iduser = '$iduser' )";
	
	$query .= $sql_cond;	
	$query .= " ".$SQL_Condition;
	
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		die( "Impossible de trouver les factures en retard de paiement" );
	
	//Encore un truc pourri pour masquer ce qui ne va pas. Tant qu'on n'aura pas des statuts fiables (et donc que la requête pourra être uniquement basée sur les statuts), on laisse ça et on n'utilise pas $rs->RecordCount()
	//C'est moche mais c'est la vie
	$resultCount = $rs->RecordCount();
	$totToPay = 0 ;
	while( !$rs->EOF() ){
		$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		$totToPay += round( $Invoice->getBalanceOutstanding(), 2 );
		$rs->MoveNext();
	}
	$rs->MoveFirst();
	
	if( !$resultCount ){
	
		?>
			<div id="toCome">
	        	<div class="headTitle">
	                <p class="title">Aucune facture </p>
					<div class="rightContainer"></div>
	            </div>
			</div>
		<?php
			
		return;
		
	}else{
	
		?>
			<div id="toCome">
	        	<div class="headTitle">
	                <a name="toC"></a>
	                <p class="title">
	                	<?php echo $resultCount ?> facture<?php echo $resultCount == 1 ? "" : "s" ?> qui vont nous parvenir
		            </p>
					<div class="rightContainer"></div>
	            </div>
	   			<div class="subContent">
				<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
  						        <th>Resp. Rel.</th>
                        		<th>Com cde</th>
      			                <th>Client n°</th>
            		            <th>Facture n°</th>
                   			    <th>Raison sociale</th>
     		                    <th>Factor</th>
           		            	<th>Date de facturation</th>
               		            <th>Date d'échéance</th>
                   		        <th>Retard (jours)</th>
                       		    <th>Total TTC facture</th>
                                <th>Solde à payer</th>
                           		<th>Date relance niv. 1</th>
                                <th>Date relance niv. 2</th>
                                <th>Date relance niv. 3</th>
                        		<th>Date relance tél.</th>
	                        	<th>Fiche suivi</th>
                            </tr>
							<!-- petites flèches de tri -->
                            <tr>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u1.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u1.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('u2.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('u2.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.idbilling_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.idbilling_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('b.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('b.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.factor','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.factor','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.DateHeure','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.DateHeure','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.total_amount','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.total_amount','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_date_1','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_date_1','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_date_2','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_date_2','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_date_3','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_date_3','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="javascript:SortResult('bb.relaunch_tri','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                    <a href="javascript:SortResult('bb.relaunch_tri','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                        	</tr>
                            </thead>
							<?php
							
							$order_total_amount_ht = 0.0;
							$order_total_amount = 0.0;
							$order_total_toPay = 0.0;
							
							$rs->MoveFirst();
							
							while( !$rs->EOF() ){
								
								$Invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
								
								$netToPay = round( $Invoice->getBalanceOutstanding(), 2 );
								
								//On n'affiche pas les factures qui sont passées au travers des mailles du filet et qui pourtant ont un solde nul
								//if( $netToPay != 0 ){
									
									$idbuyer 				= $rs->fields( "idbuyer" );
									$idcontact 				= $rs->fields( "idcontact" );
									$idorder 				= $rs->fields( "idorder" );
									list( $conf_order_date, $null )	= explode( " ", $rs->fields( "conf_order_date" ) );
									$deliv_payment			= $rs->fields("deliv_payment");
									$total_amount_ht 		= $rs->fields( "total_amount_ht" );
									$total_amount_ttc 		= $rs->fields( "total_amount" );
									$idbilling_buyer		= $rs->fields( "idbilling_buyer" );
									$relaunch_date_1		= $rs->fields("relaunch_date_1") && $rs->fields("relaunch_date_1")!="0000-00-00" ? $rs->fields("relaunch_date_1") : "-";
									$relaunch_date_2		= $rs->fields("relaunch_date_2") && $rs->fields("relaunch_date_2")!="0000-00-00" ? $rs->fields("relaunch_date_2") : "-";
									$relaunch_date_3		= $rs->fields("relaunch_date_3") && $rs->fields("relaunch_date_3")!="0000-00-00" ? $rs->fields("relaunch_date_3") : "-";
									$mail					= checkBuyerMail($idbuyer,$idcontact);
									$fax					= checkBuyerFax($idbuyer,$idcontact);
									$iscontact 				= $rs->fields( "contact" ) == 1;
									$factor					= $rs->fields( "factor" );
									$company				= $rs->fields( "company" );
									$lastname				= $rs->fields( "lastname" );
									$title 					= getTitle($rs->fields( "title" ));
									$phonenumber  			= HumanReadablePhone($rs->fields( "phonenumber" ));
									$accountant				= $rs->fields( "accountant" );
									$accountant_phonenumber	= $rs->fields( "accountant_phonenumber" );
									$accountant_faxnumber	= $rs->fields( "accountant_faxnumber" );
									$DateHeure				= $rs->fields( "DateHeure" );
									$litigation_date		= $rs->fields( "litigation_date" );
									$litigation_comment		= $rs->fields( "litigation_comment" );
									list($Date, $Heure) 	= explode(" ",$DateHeure);
									$invoice_date			= $Date;
									$retard = round((strtotime(date("Y-m-d")) - strtotime($deliv_payment))/(60*60*24)-1);
									$rel1 = round((strtotime(date("Y-m-d")) - strtotime($relaunch_date_1))/(60*60*24)-1); 
									
									if($factor==0 || $factor==''){
										$factor=Dictionnary::translate( "no" );
									}else{
										$factor=Dictionnary::translate( "yes" );
									}
									$order_total_amount_ht += $total_amount_ht;
									$order_total_amount += $total_amount_ttc;
									$order_total_toPay += $netToPay;
									$type = $iscontact ? "contact" : "buyer";
									$buyerHREF = "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer&amp;type=$type";
									$orderHREF = "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$idorder";
									$invoiceHREF = "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer";
									
									$rpd1 = $rs->fields( "relaunch_phone_date1" );
									$rpd2 = $rs->fields( "relaunch_phone_date2" );
									$rpd3 = $rs->fields( "relaunch_phone_date3" );
									$rpd4 = $rs->fields( "relaunch_phone_date4" );
									$rpd = '0000-00-00';
									
									if($rpd4!='0000-00-00')
										$rpd = $rpd4;
									else if($rpd3!='0000-00-00')
										$rpd = $rpd3;
									else if($rpd2!='0000-00-00')
										$rpd = $rpd2;
									else if($rpd1!='0000-00-00')
										$rpd = $rpd1;
									
									if($rpd=='0000-00-00')
										$rpd='';
									
									$class = '';
									if( substr($rs->fields( "relaunch_lastupdate" ),0,10) == date("Y-m-d") ){
										$class = " class=\"todayLines\"";
									}
									
									$rpr = $rs->fields( "relaunch_phone_recall" );
									
									if($rpr=='0000-00-00')
										$rpr='';
									else
										$rpr = $rpr;
										
									$rpt = $rs->fields( "relaunch_phone_time" );
								
									switch( $rpt ){
										case "Après-Midi": $time=" PM";break;
										case "Matin": $time=" AM";break;
										case "Indifférent":	$time='';break;
										default : $time='';
									}
				
									$rph = $rs->fields( "relaunch_phone_hour" );
									
									$paidInProgress = $rs->fields( "relaunch_paid_InProgress" );
									$relaunch_litigation = $rs->fields( "relaunch_litigation" );
									$relaunch_comment = $rs->fields( "relaunch_comment");
									
									//couleur par commercial
								
									$colorclass='';
									$color = $rs->fields("color");
				
									if( $color != '' )
										$colorclass=" style=\"color:$color;\"";	
									
								?>
                             <script type="text/javascript">
							/* <![CDATA[ */
							    $(document).ready(function(){
							        $("#helpBuyerNotPaidInvoices1_<?=$idbilling_buyer;?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Nom du contact :</b> <?php echo "$title $lastname" ?><?php if( $phonenumber != "" ){ ?> <br /><b>Téléphone :</b> <?php echo $phonenumber ?><?php } ?><?php if( $accountant != "" ){ ?> <br /><b>Comptable :</b> <?php echo $accountant ?><?php } ?><?php if( $accountant_phonenumber != "" ){ ?> <br /><b>Tél. comptable :</b> <?php echo $accountant_phonenumber ?><?php } ?><?php if( $accountant_faxnumber != "" ){ ?> <br /><b>Fax comptable :</b> <?php echo $accountant_faxnumber ?><?php } ?>',
							            position: {
							                corner: {
							                    target: "rightMiddle",
							                    tooltip: "leftMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							    
							     $(document).ready(function(){
							        $("#helpComment_<?=$idbilling_buyer;?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Commentaires :</b><br /> <?php echo str_replace("\r\n","",stripslashes(nl2br($relaunch_comment))) ?>',
							            position: {
							                corner: {
							                    target: "leftMiddle",
							                    tooltip: "rightMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							/* ]]> */
							</script>
							<tr<?=$class?>>
								<td class="lefterCol"<?=$colorclass?>><?php echo $rs->fields( "comRelaunch" ) ?></td>
								<td><?php echo $rs->fields( "comOrder" ) ?></td>
								<td><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbuyer ?></a></td>
								<td class="grasBack"><a href="<?php echo $invoiceHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $idbilling_buyer ?></a></td>
								<td class="grasBack"><div id="helpBuyerNotPaidInvoices<?=$niveau+1?>_<?=$idbilling_buyer?>"><a href="<?php echo $buyerHREF ?>" onclick="window.open(this.href); return false;"<?=$colorclass?>><?php echo $company ?></a><br /><?php echo $lastname ?></div></td>
								<td><a href="<?php echo $buyerHREF ?>#factor" onclick="window.open(this.href); return false;"><?php echo $factor ?></a></td>
								<td><?php echo Util::dateFormatEu( $invoice_date, false, "-", true ) ?></td>
								<td><?php echo Util::dateFormatEu( $deliv_payment, false, "-", true ) ?></td>
								<td><?php echo $retard ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_ttc ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $netToPay ) ?></td>
								<td ><?php echo Util::dateFormatEu( $relaunch_date_1, false, "-", true ) ?></td>
								<td ><?php echo Util::dateFormatEu( $relaunch_date_2, false, "-", true ) ?></td>
								<td ><?php echo Util::dateFormatEu( $relaunch_date_3, false, "-", true ) ?></td>
								<td ><?php echo Util::dateFormatEu( $rpd, false, "-", true ) ?></td>
								<td class="righterCol">
									<div id="helpComment_<?=$idbilling_buyer?>"><a href="#" onclick="popupcomment('<?=$idbilling_buyer?>'); return false;">Voir</a></div>
									<?php if( $rs->fields( "factor_refusal_date" ) != "0000-00-00" ){ ?><br /><span style="color:#FF0000;">Débité factor</span><?php } ?>
									<?php if( $rs->fields( "factor_refusal_date" ) == "0000-00-00" && $rs->fields( "factor_send_date" ) != "0000-00-00" ){ ?>
										<br /><span style="color:#FF0000;">Financé par le factor</span>
									<?php } ?>
									<?php
									
									$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1" );
									
									if( $rsLitigation->RecordCount() ){
										
										?><br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open(this.href); return false;" style="color:#EB6A0A;">Litige</a><?php
											
									}
																		
									if($relaunch_litigation){
										echo"<br /><span style=\"color:#FF0000;\">Paiement litigieux</span>";
									}
									
									?>
								</td>
							</tr>
						<?php 
								
							//}
							
							$rs->MoveNext();
							
						}
						$globalTotal[ "count" ][ 0 ] += $resultCount;
						$globalTotal[ "count" ][ 6 ] = $resultCount;
						$globalTotal[ "amount" ][ 0 ] += $order_total_amount;
						$globalTotal[ "amount" ][ 6 ] = $order_total_amount;
						$globalTotal[ "toPay" ][ 0 ] += $order_total_toPay;
						$globalTotal[ "toPay" ][ 6 ] = $order_total_toPay;
						?>
							<tr>
								<th style="border-style:none;" colspan="9"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_amount ) ?></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $order_total_toPay ) ?></th>
								<th style="border-style:none;" colspan="8"></th>
							</tr>
						</tbody>
                        </table>
                        </div>
                       	</div>
					</div>
	<?php 	
	}
}
//--------------------------------------------------------------------------------

function exportArray(& $exportableArray) {

	global $GLOBAL_START_PATH;

	include_once ("$GLOBAL_START_PATH/objects/CSVExportArray.php");

	$cvsExportArray = new CSVExportArray($exportableArray, "Retard_reglements");
	$cvsExportArray->export();

}

//--------------------------------------------------------------------------------
function updateSending( $idbilling_buyer, $niveau, $Message, $Subject ){
	
	//premier id de libre
	$query = "SELECT MAX(idrelaunch_sending) AS id FROM relaunch_sending";
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le premier enregistrement disponible" );
		
	$idrelaunch_sending = $rs->fields("id");
	
	if($idrelaunch_sending==0)
		$idrelaunch_sending=1;
		
	//On met à jour la base
	
	$qsvg = "INSERT INTO relaunch_sending (
			idrelaunch_sending,
			idbilling_buyer,
			level,
			subject,
			message)
			VALUES (
			'$idrelaunch_sending',
			'$idbilling_buyer',
			'$niveau',
			'".Util::html_escape($Subject)."',
			'".Util::html_escape($Message)."'
			)";
	$rssvg = DBUtil::query( $qsvg );
	
	if( $rssvg === false )
		die( "Impossible de sauvegarder les données envoyées" );
		
}
?>
