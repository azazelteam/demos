<?php
 /**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Ecritures comptables du factor
 */
 
include_once( "../objects/classes.php" );
include_once( "../script/global.fct.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------
/* enregistrement de la date de paiement - ajax */

if( isset( $_REQUEST[ "pay" ] ) && isset( $_REQUEST[ "idregulations_buyer" ] ) ){

	$rs =& DBUtil::query( "UPDATE regulations_buyer SET bank_date = '" . $_REQUEST[ "value_date" ] . "' WHERE idregulations_buyer = '" . intval( $_REQUEST[ "idregulations_buyer" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "ERREUR" : date( "d-m-Y" ) );
	
}

//------------------------------------------------------------------------------------------
/* définancememnt - ajax */

if( isset( $_REQUEST[ "unfund" ] ) && isset( $_REQUEST[ "idbilling_buyer" ] ) ){

	$rs =& DBUtil::query( "UPDATE billing_buyer SET factor_refusal_date = NOW() WHERE idbilling_buyer = '" . intval( $_REQUEST[ "idbilling_buyer" ] ) . "' LIMIT 1" );
	
	exit( $rs === false ? "ERREUR" : date( "d-m-Y" ) );
	
}

//------------------------------------------------------------------------------------------------
/* Import relevé Crédit Mutuel Factor */

if( isset( $_FILES[ "importFactor" ] ) ){
	
	$importsFactor =& importFactor();
	
	?>
	<div class="subTitleContainer">
		<p class="subTitle">Résultat de l'import de l'extrait du factor</p>
	</div>
	<div class="subContent">
	<?php
		
		if( count( $importsFactor ) == 0 )
			echo "Aucune nouvelle ligne importée";
		elseif( count( $importsFactor ) == 1 )
			echo "1 nouvelle ligne importée";
		else
			echo count( $importsFactor ) . " nouvelles lignes importées";
		
		if( count( $importsFactor ) ){
			
		?>
		<div class="tableContainer resultTableContainer">
			<table class="dataTable resultTable">
				<thead>
					<tr>
						<th>Client n°</th>
						<th>Nom / Raison sociale</th>
						<th>Siren</th>
						<th>Code postal</th>
						<th>Ville</th>
						<th>Opération</th>
						<th>Débit</th>
						<th>Crédit</th>
						<th>Codification<br />du compte<br />concerné<br />par la<br />contrepartie<br />de l'écriture</th>
						<th>Codification<br />du compte<br />de l'écriture</th>
					</tr>
				</thead>
				<?php foreach( $importsFactor as $import ){ ?>
					<tr>
						<td class="lefterCol grasBack">
						<?php
						
							if( $import[ "idbuyer" ] ){
								
								?>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $import[ "idbuyer" ] ?>" onclick="window.open( this.href ); return false;">
									<?php echo $import[ "idbuyer" ] ?>
								</a>
								<?php
								
							}
							
						?>
						</td>
						<td class="grasBack">
							<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $import[ "idbuyer" ] ?>" onclick="window.open( this.href ); return false;">
								<?php echo $import[ "buyer_name" ] ?></td>
							</a>
						<td><?php echo $import[ "buyer_siren" ] ?></td>
						<td><?php echo $import[ "buyer_zipcode" ] ?></td>
						<td><?php echo ucwords( strtolower( $import[ "buyer_city" ] ) ) ?></td>
						<td><?php echo $import[ "heading" ] ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo $import[ "credit" ] > 0.0 ? "-" . Util::priceFormat( $import[ "credit" ] ) : "" ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo $import[ "debit" ] > 0.0 ? "+" . Util::priceFormat( $import[ "debit" ] ) : "" ?></td>
						<td style="white-space:nowrap;"><?php echo $import[ "codification" ] ?></td>
						<td class="righterCol" style="white-space:nowrap;"><?php echo $import[ "codification2" ] ?></td>
					</tr>
				<?php } ?>
				<tbody>
				</tbody>
			</table>
		</div>
		<?php
			
		}
		
	?>
	</div>
	<?php
	
	exit();
	
}

//------------------------------------------------------------------------------------------

$Title = "Liste écritures comptables";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent" style="width:90%;">
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Relevés Factor CCV</p>
			</div>
			<div class="subContent" id="container">
				<ul class="menu">
					<li class="item">
						<a href="#search">Recherche</a>
					</li>
					<li class="item">
						<a href="#factor">Import écritures factor</a>
					</li>
				</ul>
				<div id="search"><?php displaySearchForm(); ?></div>
				<div id="factor"><?php displayFactorImport(); ?></div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<style type="text/css">
/* <![CDATA[ */
	
	.ui-tabs .ui-tabs-nav{
		width:auto;
		float:right;
		margin-top:-27px;
		*margin-top:-20px;
	}

/* ]]> */
</style>
<script type="text/javascript">
/* <![CDATA[ */
	
	//onglets
	
	$(document).ready(function(){
		
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' })
		
	});
	
	$(document).ready(function(){
	
		$('.invoiceRow').hover(
			function(){$(this).find('div.invoiceTip:hidden').show();},
			function(){$(this).find('div.invoiceTip:visible').hide();}
		);
	});
	
/* ]]> */
</script>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displaySearchForm(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	$formFields = array(
		
		$fieldName = "date_type"	=> "value_date",
		"interval_select"			=> 2,
		"minus_date_select"			=> $defaultValue = "-365",
		"name"						=> "",
		"idbuyer"					=> "",
		"buyer_zipcode"				=> "",
		"heading"					=> "",
		"amount"					=> "",
		"page"						=> 1,
		"orderByFactor"				=> "entry_date DESC",
		"hiddenFactor"				=> 1,
		"idbilling_buyer" 			=> ""
		
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};
			
			$('#SearchForm').ajaxForm( options );
			$('#FactorForm').ajaxForm({ beforeSubmit: preSubmitImportCallback, success: factorPostUpload });
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitImportCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Téléchargement de l'extrait en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function factorPostUpload( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible de charger le fichier' );
				return;
				
			}
			
			$('#FactorUploadResults').html( responseText );
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function orderByFactor( str ){
			
			document.getElementById( 'SearchForm' ).elements[ 'page' ].value = '1';
			document.getElementById( 'SearchForm' ).elements[ 'orderByFactor' ].value = str;
			
			$('#SearchForm').ajaxSubmit( { 
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postSubmitCallback  // post-submit callback 
			} ); 
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectSince(){ document.getElementById( "interval_select_since" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectMonth(){ document.getElementById( "interval_select_month" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectBetween( cal ){ document.getElementById( "interval_select_between" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectDay( cal ){ document.getElementById( "day_select" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
		<div class="subTitleContainer" style="padding-top:10px;">
			<p class="subTitle">Rechercher une écriture</p>
		</div>
		<input type="hidden" name="search" value="1" />
		<input type="hidden" name="page" value="<?php echo $postData[ "page" ] ?>" />
		<input type="hidden" name="orderByFactor" value="<?php echo $postData[ "orderByFactor" ] ?>" />
		<input type="hidden" name="hiddenFactor" id="hiddenfactor" value="<?php echo $postData[ "hiddenFactor" ] ?>" />
		<br style="clear:both;" />
		<div class="tableContainer" style="margin-top:0;">
			<table class="dataTable">
				<tr>	
					<th style="width:25%" class="filledCell">
						<input type="radio" name="interval_select" id="interval_select_since" class="VCenteredWithText" value="1"<?php if( $postData[ "interval_select" ] == 1 ) echo " checked=\"checked\""; ?> /> 
						Depuis
					</td>
					<td style="width:25%">
						<select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							<option value="0" <?php if( $postData[ "minus_date_select" ] == 0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ?></option>
							<option value="-1" <?php if( $postData[ "minus_date_select" ] == -1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ?></option>
							<option value="-2" <?php if( $postData[ "minus_date_select" ] == -2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ?></option>
							<option value="-7" <?php if( $postData[ "minus_date_select" ] == -7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ?></option>
							<option value="-14" <?php if( $postData[ "minus_date_select" ] == -14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ?></option>
							<option value="-30" <?php if( $postData[ "minus_date_select" ] == -30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ?></option>
							<option value="-60" <?php if( $postData[ "minus_date_select" ] == -60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ?></option>
							<option value="-90" <?php if( $postData[ "minus_date_select" ] == -90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ?></option>
							<option value="-180" <?php if( $postData[ "minus_date_select" ] == -180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ?></option>
							<option value="-365" <?php if( $postData[ "minus_date_select" ] == -365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ?></option>
							<option value="-730" <?php if( $postData[ "minus_date_select" ] == -730) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2year") ?></option>
							<option value="-1825" <?php if( $postData[ "minus_date_select" ] == -1825) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_5year") ?></option>
						</select>
					</td>
					<th style="width:20%" class="filledCell">
						<input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( $postData[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> 
						Pour le mois de
					</td>
					<td style="width:30%">
						<?php FStatisticsGenerateDateHTMLSelect( "selectMonth()" ); ?>
					</td>
				</tr>
				<tr>
					<th class="filledCell"><input type="radio" name="interval_select" id="day_select" class="VCenteredWithText" value="4"<?php if( $postData[ "interval_select" ] == "4" ) echo " checked=\"checked\""; ?> /> Le</th>
					<td><?php $date = isset( $_POST[ "day" ] ) ? $_POST[ "day" ] : date( "d-m-Y" ); ?><input type="text" name="day" id="day" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "day", true, "selectDay" ) ?></td>
					<th class="filledCell"><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( $postData[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> Entre le</td>
					<td><?php $date = isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : date( "d-m-Y", mktime( 0, 0, 0, 1, 1, date( "Y" ) ) ); ?><input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_start", true, "selectBetween" ) ?><span style="margin:0 10px;">et le</span><?php $date = isset( $_POST[ "sp_bt_end" ] ) ? $_POST[ "sp_bt_end" ] : date( "d-m-Y", mktime( 0, 0, 0, 1, 0, date( "Y" ) + 1 ) ); ?><input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_end", true, "selectBetween" ) ?></td>
				</tr>
				<tr>
					<td colspan="4" class="tableSeparator"></td>
				</tr>
				<tr>
					<th class="filledCell">Nom / raison sociale</th>
					<td><input type="text" name="name" class="textInput" value="<?php echo $postData[ "name" ] ?>" /></td>
					<th class="filledCell">Montant</th>
					<td><input type="text" name="amount" class="textInput" value="<?php echo $postData[ "amount" ] ?>" /></td>
				</tr>
				<tr>
					<th>Client n°</th>
					<td><input type="text" name="idbuyer" class="textInput" value="<?php echo $postData[ "idbuyer" ] ?>" /></td>
					<th>Code postal</th>
					<td><input type="text" name="buyer_zipcode" class="textInput" value="<?php echo $postData[ "buyer_zipcode" ] ?>" /></td>
				</tr>
				<tr>
					<th>Type d'opération</th>
					<td>
						<select name="heading_list" class="fullWidth">
							<option value="">-</option>
							<?php
							
							$rs =& DBUtil::query( "SELECT heading FROM bookkeeping_entries WHERE heading NOT LIKE 'REM CHQ%' GROUP BY heading HAVING COUNT( heading ) > 3 ORDER BY heading ASC" );
							
							if( $rs === false )
								trigger_error( "Impossible de récupérer la liste des opérations" );
							
							while( !$rs->EOF ){
								
								?>
								<option><?php echo $rs->fields( "heading" ) ?></option>
								<?php
								
								$rs->MoveNext();
								
							}
							
							?>
						</select>
					</td>
					<th>N° Facture</td>
					<td><input type="text" name="idbilling_buyer" class="textInput" value="<?php echo $postData[ "idbilling_buyer" ] ?>" /></td>
				</tr>
			</table>
		</div>
		<div class="submitButtonContainer">
			<input type="submit" class="blueButton" value="Rechercher" />
		</div>
	</form>
	<div id="SearchResults"></div>
<?php
	
}

//----------------------------------------------------------------------------------------------------

function displayFactorImport(){
	
	?>
	<form method="post" enctype="multipart/form-data" action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" id="FactorForm">
		<div class="subTitleContainer" style="padding-top:10px;">
			<p class="subTitle">Import des écritures du factor</p>
		</div>
		<div style="padding:10px 0;"><input type="file" name="importFactor" accept="*.csv" /></div>
		<div class="submitButtonContainer"><input type="submit" value="Importer" class="blueButton" /></div>
		<div id="FactorUploadResults"></div>
	</form>
					
	<?php
	
}

//----------------------------------------------------------------------------------------------------

function search(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	$maxSearchResults = 1000;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	$where = "1";
	
	//post data
	
	//nom client / intitulé
	
	if( isset( $_POST[ "name" ] ) && strlen( $_POST[ "name" ] ) )
		$where .= " AND buyer_name LIKE '%" . Util::html_escape( $_POST[ "name" ] ) . "%'";
	
	//montant
	
	if( isset( $_POST[ "amount" ] ) && !empty( $_POST[ "amount" ] ) )
		$where .= " AND ( credit = '" . Util::html_escape( Util::text2num( $_POST[ "amount" ] ) ) . "' OR debit = '" . Util::html_escape( Util::text2num( $_POST[ "amount" ] ) ) . "' )";
	
	//idbuyer
	
	if( isset( $_POST[ "idbuyer" ] ) && intval( $_POST[ "idbuyer" ] ) )
		$where .= " AND idbuyer = " . intval( $_POST[ "idbuyer" ] );
	
	//n° facture
	
	if( isset( $_POST[ "idbilling_buyer" ] ) && intval( $_POST[ "idbilling_buyer" ] ) )
		$where .= " AND bb.idbilling_buyer = " . intval( $_POST[ "idbilling_buyer" ] );
		
	//code postal
	
	if( isset( $_POST[ "buyer_zipcode" ] ) && !empty( $_POST[ "buyer_zipcode" ] ) )
		$where .= " AND buyer_zipcode LIKE '%" . Util::html_escape( $_POST[ "buyer_zipcode" ] ) . "%'";
	
	//opération
	
	if( isset( $_POST[ "heading_list" ] ) && !empty( $_POST[ "heading_list" ] ) )
		$where .= " AND heading LIKE '" . Util::html_escape( $_POST[ "heading_list" ] ) . "'";
	
	//date
	
 	$now = getdate();
	
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	
	switch( $interval_select ){
		
		case 1:
			
			$Min_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
			$Now_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + 1,$now["year"])) ;
			$where  .= " AND entry_date >= '$Min_Time' AND entry_date < '$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now[ "year" ]--;
			
		 	//min
			$Min_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select + 1, 1,$now["year"])) ;
			
			//sql_condition
			$where .= " AND entry_date >= '$Min_Time' AND entry_date < '$Max_Time'";
		 	break;
			
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "sp_bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "sp_bt_end" ] );
		 	
			$where .= " AND entry_date >= '$Min_Time' AND entry_date <= '$Max_Time 23:59:59'";
			
		 	break;
			
		case 4:
		 	
		 	$date = euDate2us( $_POST[ "day" ] );
		 	
			$where .= " AND entry_date = '$date'";
			
		 	break;
			
	}
	
	//tri
	
	$orderBy = isset( $_POST[ "orderByFactor" ] ) ? Util::html_escape( stripslashes( $_POST[ "orderByFactor" ] ) ) : " entry_date ASC";
	
	//nombre de pages
	
	$query = "
	SELECT COUNT( * ) AS resultCount
	FROM bookkeeping_entries be
	LEFT JOIN billing_buyer bb ON bb.idbilling_buyer = be.idbilling_buyer
	WHERE $where";
	
	$rs =& DBUtil::query( $query );
	
	$resultCount = $rs->fields( "resultCount" );
	
	if( $resultCount > $maxSearchResults ){ 
		
		?>
		<div class="headTitle">
			<p class="title" style="margin-bottom:15px;">Plus de <?php echo $maxSearchResults ?> écritures ont été trouvées</p>
			<p class="title" style="margin-bottom:15px;">Merci de bien vouloir affiner votre recherche</p>
		</div>
		<?php
		
		return;
		
	}
	
	if( !$resultCount ){
		
		?>
		<div class="headTitle">
			<p class="title" style="margin-bottom:15px;">Aucune opération trouvée</p>
		</div>
		<?php
		
		return;
			
	}
	
	$query = "
	SELECT be.*, bb.DateHeure, bb.deliv_payment, bb.factor_refusal_date
	FROM bookkeeping_entries be
	LEFT JOIN billing_buyer bb ON bb.idbilling_buyer = be.idbilling_buyer
	WHERE $where
	ORDER BY $orderBy";

	$rs =& DBUtil::query( $query );
					
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* -------------------------------------------------------------------------------- */
		
		function pay( idregulations_buyer, value_date ){

			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/bookkeeping_entries.php?pay&idregulations_buyer=" + idregulations_buyer + "&value_date=" + value_date,
				async: true,
			 	success: function( responseText ){

					$.growlUI( '', 'Modifications enregistrées' );
					$( "#bank_date" + idregulations_buyer ).html( responseText );
	
				}

			});
			
		}
		
		/* -------------------------------------------------------------------------------- */
		
		function unfund( idbilling_buyer ){
		
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/bookkeeping_entries.php?unfund&idbilling_buyer=" + idbilling_buyer,
				async: true,
			 	success: function( responseText ){
			 			
					$.growlUI( '', 'Modifications enregistrées' );
					$( "#factor_refusal_date" + idbilling_buyer ).html( responseText );
	
				}

			});
			
		}
		
		/* -------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div class="headTitle">
		<p class="title" style="margin-bottom:15px;"><?php echo $resultCount ?> écriture(s) trouvée(s)</p>
	</div>
	<div class="resultTableContainer" id="factorResultContainer">
		<table class="dataTable resultTable clear">
			<thead>
				<tr>
					<th>Client n°</th>
					<th>Nom / Raison sociale</th>
					<th>Code postal</th>
					<th>Date<br />d'écriture<br />factor</th>
					<th>Date<br />facture</th>
					<th>Date<br />d'échéance</th>
					<th>Intitulé opération</th>
					<th>Compte</th>
					<th>Débit</th>
					<th>Crédit</th>
					<th>Facture n°</th>
					<th>Codification<br />du compte<br />concerné<br />par la<br />contrepartie<br />de l'écriture</th>
					<th>Codification<br />du compte<br />de l'écriture</th>
					<th>Règlements</th>
					<th>Mode de<br />règlement</th>
					<th>Date de<br />règlement</th>
					<th>Date de<br />définancement</th>
				</tr>
				 <!-- petites flèches de tri -->
				<tr>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('idbuyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('idbuyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('buyer_name ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('buyer_name DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('buyer_zipcode ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('buyer_zipcode DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('entry_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('entry_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('DateHeure ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('DateHeure DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('deliv_payment ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('deliv_payment DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('heading ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('heading DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder"></th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('credit ASC, debit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('credit DESC, debit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('debit ASC, credit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('debit DESC, credit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder"></th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('codification ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('codification DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="#" onclick="orderByFactor('codification2 ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="#" onclick="orderByFactor('codification2 DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder"></th>
					<th class="noTopBorder"></th>
					<th class="noTopBorder"></th>
					<th class="noTopBorder"></th>
				</tr>
			</thead>
			<tbody>
			<?php
				
				$row = 0;
				while( !$rs->EOF ){
								
					?>
					<tr>
						<td class="lefterCol grasBack">
						<?php 
						
							if( $rs->fields( "idbuyer" ) ){
								
								?>
								<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ) ?>" onclick="window.open( this.href ); return false;">
									<?php echo $rs->fields( "idbuyer" ); ?>
								</a>
								<?php
								
							}
							
						?>										
						</td>
						<td class="grasBack"><?php echo htmlentities( $rs->fields( "buyer_name" ) ) ?></td>
						<td><?php echo $rs->fields( "buyer_zipcode" ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "entry_date" ), false ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ), false ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "deliv_payment" ), false ) ?></td>
						<td><?php echo htmlentities( $rs->fields( "heading" ) ); ?></td>
						<td><?php echo getHeadingAccount( $rs->fields( "heading" ) ); ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "credit" ) > 0.0 ? "- " . Util::priceFormat( $rs->fields( "credit" ) ) : "" ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "debit" ) > 0.0 ? "+ " . Util::priceFormat( $rs->fields( "debit" ) ) : "" ?></td>
						<td class="invoiceRow">
						<?php 
						
							if( $rs->fields( "idbilling_buyer" ) ){
								
								?>
								<a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $rs->fields( "idbilling_buyer" ) ?>" onclick="window.open(this.href); return false;">
									<?php echo $rs->fields( "idbilling_buyer" ); ?>
								</a>
								<?php
								
								invoiceTip( array( $rs->fields( "idbilling_buyer" ) ) );
								
							}
							else if( $rs->fields( "idbuyer" ) ){
								
								$invoices = searchCustomerInvoice( $rs->fields( "idbuyer" ), max( $rs->fields( "debit" ), $rs->fields( "credit" ) ) );
							
								foreach( $invoices as $invoice ){
									
									?>
									<p style="margin:0px;">
										<a style="color:#FF0000;" href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $invoice ?>" onclick="window.open(this.href); return false;"><?php echo $invoice ?></a>
									</p>
									<?php
		
								}
								
								invoiceTip( $invoices );
								
							}
							
						?>
						</td>
						<td><?php echo $rs->fields( "codification" ); ?></td>
						<td><?php echo $rs->fields( "codification2" ); ?></td>
						<td style="white-space:nowrap; text-align:right;">
						<?php
						
							/* règlements */
							
							if( $rs->fields( "idbilling_buyer" ) && allowPayment( $rs->fields( "heading" ) ) ){

								$query = "
								SELECT rb.idregulations_buyer, 
									rb.amount, 
									p.name_1 AS payment, 
									rb.bank_date
								FROM regulations_buyer rb, billing_regulations_buyer brb, payment p
								WHERE brb.idbilling_buyer = '" . $rs->fields( "idbilling_buyer" ) . "'
								AND brb.idregulations_buyer = rb.idregulations_buyer
								AND rb.idpayment = p.idpayment";

								$rsPayment =& DBUtil::query( $query );
								
								while( !$rsPayment->EOF() ){
									
									?>
									<p><?php echo Util::priceFormat( $rsPayment->fields( "amount" ) ); ?></p>
									<?php
									
									$rsPayment->MoveNext();
									
								}
								
							}
								
						?>
						</td>
						<td style="white-space:nowrap;">
						<?php
						
							/* modes de règlement */
							
							if( $rs->fields( "idbilling_buyer" ) && allowPayment( $rs->fields( "heading" ) ) ){
								
								$rsPayment->MoveFirst();
								
								while( !$rsPayment->EOF() ){
									
									?>
									<p><?php echo htmlentities( $rsPayment->fields( "payment" ) ); ?></p>
									<?php
									
									$rsPayment->MoveNext();
									
								}
								
							}
									
						?>
						</td>
						<td>
						<?php
							
							/* dates de règlement */
							
							if( $rs->fields( "idbilling_buyer" ) && allowPayment( $rs->fields( "heading" ) ) ){
								
								$rsPayment->MoveFirst();
									
								while( !$rsPayment->EOF() ){
									
									if( $rsPayment->fields( "bank_date" ) == "0000-00-00" ){
												
										?>
										<p id="bank_date<?php echo $rsPayment->fields( "idregulations_buyer"); ?>"><a href="#" class="blueLink" onclick="pay( <?php echo $rsPayment->fields( "idregulations_buyer"); ?>, '<?php echo $rs->fields( "value_date" ); ?>' ); return false;">Payé</a></p>
										<?php
											
									}
									else echo "<p>" . usDate2eu( $rsPayment->fields( "bank_date" ) ) . "</p>";
									
									$rsPayment->MoveNext();
									
								}
		
							}
							
						?>
						</td>
						<td class="righterCol">
						<?php
							
							/* dates de définancement */
							
							if( $rs->fields( "idbilling_buyer" ) && allowPayment( $rs->fields( "heading" ) ) ){
				
								if( $rs->fields( "factor_refusal_date" ) == NULL || $rs->fields( "factor_refusal_date" ) == "0000-00-00" ){
								
									?>
									<p id="factor_refusal_date<?php echo $rs->fields( "idbilling_buyer" ); ?>">
										<a href="#" class="blueLink" onclick="unfund( <?php echo $rs->fields( "idbilling_buyer" ); ?> ); return false;">Définancé</a>
									</p>
									<?php
			
								}
								else echo "<p>" . usDate2eu( $rs->fields( "factor_refusal_date" ) ) . "</p>";
							
							}
							
						?>
						</td>
					</tr>
					<?php
					
					$rs->MoveNext();
					
				}
				
			?>
			</tbody>
		</table>
		<script type="text/javascript">
		<!--
			
			$(document).ready(function(){
				$('.invoiceRow').hover(
					function(){$(this).find('div.invoiceTip:hidden').show();},
					function(){$(this).find('div.invoiceTip:visible').hide();}
				);
			});
			
		-->
		</script>
	</div>
	<?php 

	
}

//-------------------------------------------------------------------------------------

//Fonction spécifique à l'import des extraits du factor Crédit Mutuel Factor

function &importFactor(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
	
	if( !is_readable( $_FILES[ "importFactor" ][ "tmp_name" ] ) )
		trigger_error( "Impossible de lire le fichier d'import", E_USER_ERROR );

	$file = @fopen( $_FILES[ "importFactor" ][ "tmp_name" ], "r" );
	
	if( !$file ){
		
		trigger_error( "Impossible de lire le fichier d'import", E_USER_ERROR );
		return;
		
	}

	$import = array();
	
	fgetcsv( $file, 0, ";" );
	while( ( $line = fgetcsv( $file, 0, ";" ) ) !== FALSE )
		insertLine( $import, $line );
	
	fclose( $file );

	return $import;
	
}

//-------------------------------------------------------------------------------------

function insertLine( &$import, $line ){
	/*
	0	Code vendeur	
	1	Nom vendeur	
	2	Date de réception	
	3	Heure de réception	
	4	Date comptable de léécriture	
	5	Libellé condensé	
	6	Montant du débit	
	7	Montant du crédit	
	8	Date de la valeur de léopération	
	9	Solde progressif	
	10	Sens du solde progressif	
	11	Données de léacheteur concerné par léopération	( SOCIETE MANUEST/88170 CHATENOIS )
	12	Complément sur léacheteur concerné par léopération	
	13	Codification du compte concerné par la contrepartie de léécriture	
	14	Abrégé de la devise de léécriture	
	15	Libellé du produit de léécriture	
	16	Codification du compte de léécriture																																																																																																																																																																																																																																															
	*/

	$record = array(
	
		"import_date"		=> date( "Y-m-d H:i:s" ),
		"entry_date"		=> factorDate2MySQL( $line[ 4 ] ),
		"value_date" 		=> factorDate2MySQL( $line[ 8 ] ),
		"heading" 			=> $line[ 5 ],
		"idbilling_buyer" 	=> 0,
		"debit" 			=> Util::text2num( $line[ 6 ] ),
		"credit" 			=> Util::text2num( $line[ 7 ] ) ,
		"balance"			=> Util::text2num( $line[ 9 ] ),
		"idbuyer" 			=> 0,
		"buyer_name"		=> "",
		"buyer_address"		=> "",
		"buyer_zipcode"		=> "",
		"buyer_city"		=> "",
		"buyer_siren"		=> "",
		"codification" 		=> trim( strtoupper( $line[ 13 ] ) ),
		"codification2" 	=> trim( strtoupper( $line[ 16 ] ) )
		
	);

	setCustomerData( $line, $record ); //compléter les infos client
	
	$query = "
	INSERT IGNORE INTO bookkeeping_entries( `" . implode( "`,`", array_keys( $record ) ) . "`
	) VALUES(";
	
	$i = 0;
	foreach( $record as $key => $value ){
	
		if( $i )
			$query .= ",";
				
		$query .= "'" . Util::html_escape( $value ) . "'";
		
		$i++;
		
	}
	
	$query .= ")";
	
	$ret = DBUtil::query( $query );

	if( $ret !== false )
		$import[] = $record;
		
}

//-------------------------------------------------------------------------------------

function factorDate2MySQL( $euDate ){
	
	$res = @explode( "/", $euDate );
	
	if( count( $res ) != 3 )
		return $euDate;
	
	@list( $day, $month, $year ) = $res;
		
	$usDate = "$year-$month-$day";
	
	return $usDate;
	
}

//-------------------------------------------------------------------------------------

function setContactData( $idbuyer, $idcontact, &$record ){
	
	$query = "
	SELECT b.company, b.adress, b.adress_2, b.zipcode, b.city, c.*, t.title_1 AS gender
	FROM buyer b, contact c, title t
	WHERE c.idbuyer = '$idbuyer' 
	AND c.idcontact = '$idcontact' 
	AND c.idbuyer = b.idbuyer 
	AND c.title = t.idtitle
	LIMIT 1";
	
	$rs = DBUtil::query( $query );
			
	if( $rs->RecordCount() ){
		
		$title = strlen( $rs->fields( "gender" ) ) ? $rs->fields( "gender" ) . ". " : "";
		
		$record[ "idbuyer" ] 		= $idbuyer;
		$record[ "buyer_name" ] 	= strlen( $rs->fields( "company" ) ) ? $rs->fields( "company" ) : $title . $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
		$record[ "buyer_address" ] 	= $rs->fields( "adress" ) . "\n" . $rs->fields( "adress_2" );
		$record[ "buyer_zipcode" ] 	= $rs->fields( "zipcode" );
		$record[ "buyer_city" ] 	= $rs->fields( "city" );

	}
		
}

//-------------------------------------------------------------------------------------

function setInvoiceAddressData( $idbilling_adress, &$record ){

	$query = "
	SELECT ba.*, t.title_1 AS gender
	FROM billing_adress ba, title t
	WHERE ba.idbilling_adress = '$idbilling_adress' 
	AND ba.title = t.idtitle
	LIMIT 1";
	
	$rs = DBUtil::query( $query );
			
	if( $rs->RecordCount() ){
		
		$title = strlen( $rs->fields( "gender" ) ) ? $rs->fields( "gender" ) . ". " : "";
		
		$record[ "idbuyer" ] 		= $rs->fields( "idbuyer" );
		$record[ "buyer_name" ] 	= $title . $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
		$record[ "buyer_address" ] 	= $rs->fields( "adress" ) . "\n" . $rs->fields( "adress_2" );
		$record[ "buyer_zipcode" ] 	= $rs->fields( "zipcode" );
		$record[ "buyer_city" ] 	= $rs->fields( "city" );

	}
		
}

//-------------------------------------------------------------------------------------

function setInvoiceData( $idbilling_buyer, &$record ){
	
	$query = "
	SELECT bb.idbuyer, bb.idbilling_adress, c.*, b.siren
	FROM billing_buyer bb, buyer b, contact c
	WHERE bb.idbilling_buyer = '$idbilling_buyer'
	AND bb.idbuyer = b.idbuyer
	AND bb.idbuyer = c.idbuyer
	AND bb.idcontact = c.idcontact
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() ){
		
		$record[ "idbuyer" ] 			= $rs->fields( "idbuyer" );
		$record[ "buyer_siren" ] 		= $rs->fields( "siren" );
		$record[ "idbilling_buyer" ] 	= $idbilling_buyer;

		$idbilling_adress = $rs->fields( "idbilling_adres" );
		$idbuyer = $rs->fields( "idbuyer" );
		$idcontact = $rs->fields( "idcontact" );
		
		//coordonnées de l'adresse de facturation si elle existe, sinon celle du client
		
		if( $rs->fields( "idbilling_adress" ) )
			setInvoiceAddressData( $rs->fields( "idbilling_adress" ), $record );
		else setContactData( $idbuyer, $idcontact, $record );

		
	}
		
}

//-------------------------------------------------------------------------------------

function setCustomerData( &$line,&$record ){

	/* données depuis la facture */
	
	$regs = array();
	
	if( eregi( "FACTURE ([0-9]+)", $line[ 12 ], $regs ) !== FALSE 
		|| eregi( "FACT ([0-9]+)", $line[ 12 ], $regs )
		|| eregi( "FNA N°([0-9]+)", $line[ 12 ], $regs ) ){

		setInvoiceData( $regs[ 1 ], $record );
		return;
		
	}
	
	if( eregi( "AVOIR ([0-9]+)", $line[ 12 ], $regs ) !== FALSE ){

		$idbilling_buyer = DBUtil::getDBValue( "idbilling_buyer", "credits", "idcredit", $regs[ 1 ] );
		setInvoiceData( $regs[ 1 ], $record );
		
		return;
		
	}
	
	if( eregi( "Client ([0-9]+)", $line[ 12 ], $regs ) !== FALSE ){
		
		$record[ "idbuyer" ] = $regs[ 1 ];
		
		$query = "SELECT idbuyer, company, adress, adress_2, zipcode, city, siren FROM buyer WHERE idbuyer = '" . $regs[ 1 ] . "' LIMIT 1";
		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() ){
			
			$record[ "buyer_name" ] = $rs->fields( "company" );
			$record[ "buyer_address" ] = $rs->fields( "adress" ) . "\n" . $rs->fields( "adress_2" );
			$record[ "buyer_zipcode" ] = $rs->fields( "zipcode" );
			$record[ "buyer_city" ] = $rs->fields( "city" );
			$record[ "buyer_siren" ] = $rs->fields( "siren" );
			
			return;
			
		}
		
	}
	
	if( eregi( "Siret: ([0-9 ]+)", $line[ 12 ], $regs ) !== FALSE ){
		
		$query = "
		SELECT idbuyer, company, adress, adress_2, zipcode, city, siren
		FROM buyer 
		WHERE REPLACE( siret, ' ', '' ) = '" . str_replace( " ", "", $regs[ 1 ] ) . "' 
		OR REPLACE( siren, ' ', '' ) = '" . substr( str_replace( " ", "", $regs[ 1 ] ), 0, 9 ) . "' 
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() ){
			
			$record[ "idbuyer" ] 		= $rs->fields( "idbuyer" );
			$record[ "buyer_name" ] 	= $rs->fields( "company" );
			$record[ "buyer_address" ] 	= $rs->fields( "adress" ) . "\n" . $rs->fields( "adress_2" );
			$record[ "buyer_zipcode" ] 	= $rs->fields( "zipcode" );
			$record[ "buyer_city" ] 	= $rs->fields( "city" );
			$record[ "buyer_siren" ] 	= $rs->fields( "siren" );
			
			return;
			
		}
		
	}
	
	if( eregi( "([^/]+)/([0-9]{5})(.+)", $line[ 11 ], $regs ) !== FALSE ){
			
		$company 	= str_replace( " ", "%", trim( $regs[ 1 ] ) );
		$zipcode 	= trim( $regs[ 2 ] );
		$city 		= trim( $regs[ 3 ] );	

		while( strpos( $company, "%%" ) !== false )
			$company = str_replace( "%%", "%", $company );
		
		//recherche sur les 3 critères
		
		$query = "
		SELECT idbuyer
		FROM buyer
		WHERE company LIKE '%" . Util::html_escape( $company ) . "%'
		AND zipcode = '" . Util::html_escape( $zipcode ) . "'
		AND city = '" . Util::html_escape( $city ) . "'
		LIMIT 2";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() == 1 ){
			
			setContactData( $rs->fields( "idbuyer" ), 0, $record );
			return;
			
		}

		//recherche sur nom société de plus en plus petit, gnhiiihihihi ^^
		
		do{
			
			$query = "
			SELECT idbuyer
			FROM buyer
			WHERE company LIKE '%" . Util::html_escape( $company ) . " %'
			LIMIT 2";
	
			$rs =& DBUtil::query( $query );
			
			if( $rs->RecordCount() == 1 ){
				
				setContactData( $rs->fields( "idbuyer" ), 0, $record );
				return;
				
			}
			else if( $rs->RecordCount() > 1 )
				return; //pas la peine d'aller plus loin
		
		}
		while( strpos( $company, "%" ) !== false && $company = substr( $company, 0, strrpos( $company, "%" ) ) );
		
		//recherche sur code postal : j'ai de la chance
		
		$query = "
		SELECT idbuyer
		FROM buyer
		WHERE zipcode = '" . Util::html_escape( $zipcode ) . "'
		AND city = '" . Util::html_escape( $city ) . "'
		LIMIT 2";

		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() == 1 ){
			
			setContactData( $rs->fields( "idbuyer" ), 0, $record );	
			return;
			
		}
		
	}

	if( eregi( "([^/]+)/([0-9]{5})", $line[ 11 ], $regs ) !== FALSE ){
			
		$company 	= str_replace( " ", "%", trim( $regs[ 1 ] ) );
		$zipcode 	= trim( $regs[ 2 ] );

		while( strpos( $company, "%%" ) !== false )
			$company = str_replace( "%%", "%", $company );
		
		//recherche sur les 2 critères
		
		$query = "
		SELECT idbuyer
		FROM buyer
		WHERE company LIKE '%" . Util::html_escape( $company ) . " %'
		AND zipcode = '" . Util::html_escape( $zipcode ) . "'
		LIMIT 2";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() == 1 ){
			
			setContactData( $rs->fields( "idbuyer" ), 0, $record );
			return;
			
		}

		//recherche sur nom société de plus en plus petit, gnhiiihihihi ^^
		
		do{
			
			$query = "
			SELECT idbuyer
			FROM buyer
			WHERE company LIKE '%" . Util::html_escape( $company ) . " %'
			LIMIT 2";
	
			$rs =& DBUtil::query( $query );
			
			if( $rs->RecordCount() == 1 ){
				
				setContactData( $rs->fields( "idbuyer" ), 0, $record );
				return;
				
			}
			else if( $rs->RecordCount() > 1 )
				return; //pas la peine d'aller plus loin
		
		}
		while( strpos( $company, "%" ) !== false && $company = substr( $company, 0, strrpos( $company, "%" ) ) );
		
		//recherche sur code postal : j'ai de la chance
		
		$query = "
		SELECT idbuyer
		FROM buyer
		WHERE zipcode = '" . Util::html_escape( $zipcode ) . "'
		LIMIT 2";

		$rs =& DBUtil::query( $query );
		
		if( $rs->RecordCount() == 1 ){
			
			setContactData( $rs->fields( "idbuyer" ), 0, $record );	
			return;
			
		}
		
	}
	
}

//-------------------------------------------------------------------------------------

function searchCustomerInvoice( $idbuyer, $amount ){
	
	$invoices = array();
	
	$query = "
	SELECT idbilling_buyer, ROUND( total_amount, 2 ) AS total_amount,
	- ABS( ROUND( total_amount, 2 ) - '$amount' ) AS relevance
	FROM billing_buyer
	WHERE idbuyer = '$idbuyer'
	ORDER BY relevance ASC, DateHeure DESC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return $invoices;
	
	//montant exact pour une facture donnée
	
	while( !$rs->EOF() ){
		
		$invoices[] = $rs->fields( "idbilling_buyer" );
			
		$rs->MoveNext();
			
	}
	
	return $invoices;
	
}

//-------------------------------------------------------------------------------------

function invoiceTip( $invoices ){
	
	$query = "
	SELECT bb.idbilling_buyer,
	b.idbuyer,
	b.company,
	b.zipcode,
	ROUND( bb.total_amount, 2 ) AS total_amount,
	ROUND( bb.total_amount_ht, 2 ) AS total_amount_ht,
	bb.status,
	bb.DateHeure,
	bb.deliv_payment
	FROM billing_buyer bb, buyer b
	WHERE bb.idbilling_buyer IN ( '" . implode( "','", $invoices ) . "' )
	AND b.idbuyer = bb.idbuyer
	ORDER BY total_amount DESC, DateHeure DESC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return;

	?>
	<div class="invoiceTip" style="background-color:#FFFFFF; border:5px solid #EB6A0A; border-radius:5px; display:none; margin-left:-500px; margin-top:6px; padding:3px; position:absolute; width:800px; -moz-border-radius:5px; -webkit-border-radius:5px;">
		<table class="dataTable resultTable">
			<thead>
				<tr>
					<th>Facture n°</th>
					<th>Client n°</th>
					<th>Nom / Raison sociale</th>
					<th>Code postal</th>
					<th>Date de création</th>
					<th>Date d'échéance</th>
					<th>Statut</th>
					<th>Total HT</th>
					<th>Total TTC</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				$total_amount 		= 0.0;
				$total_amount_ht 	= 0.0;
				
				while( !$rs->EOF() ){
					
					?>
					<tr>
						<td class="lefterCol"><?php echo $rs->fields( "idbilling_buyer" ) ?></td>
						<td><?php echo $rs->fields( "idbuyer" ) ?></td>
						<td><?php echo htmlentities( $rs->fields( "company" ) ) ?></td>
						<td><?php echo $rs->fields( "zipcode" ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "DateHeure" ), false ) ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "deliv_payment" ) ) ?></td>
						<td><?php echo Dictionnary::translate( $rs->fields( "status" ) ) ?></td>
						<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
						<td  style="text-align:right;" class="righterCol"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
					</tr>
					<?php
					
					$total_amount 		+= $rs->fields( "total_amount" );
					$total_amount_ht 	+= $rs->fields( "total_amount_ht" );
					
					$rs->MoveNext();
					
				}
				
				?>
				<tr>
					<td colspan="7" style="text-align:right;" class="lefterCol"><b>Total</b></td>
					<td style="text-align:right;"><b><?php echo Util::priceFormat( $total_amount_ht ) ?></b></td>
					<td style="text-align:right;" class="righterCol"><b><?php echo Util::priceFormat( $total_amount ) ?></b></td>
				</tr>
			</tbody>
		</table>
	</div>
	<?php
	
}

//-------------------------------------------------------------------------------------

function allowPayment( $heading ){
	
	return in_array( $heading, array(
	
		/*Virement en votre faveur
		Achat de Factures No 1780091
		Ajustement de Réserve
		C.S.F. sur Achat
		Commission sur Achat
		Réajustement FDG (Retrait)
		Ristourne sur C.S.F
		T.V.A. sur Ristourne C.S.F
		TVA sur Achat
		Virement Fonds De Garantie S/Achat
		Virement Réserve sur Achat
		Facturation Pack Télématique
		TVA sur Comm. Prestation de Service
		Régularisation Encaissem. Virement
		Achat de Factures No ...
		VIREMENT EXPRESS EN VOTRE FAVEUR
		A-Facture en Litige sans Solution
		Achat de Factures No 1830096
		Virement en valeur compensée
		Achat de Factures No 1840097
		Remboursement Crédit à L' Acheteur*/
		"A-Facture Impayée charge Vendeur",
		"Virement Encaissé",
		"Encaissement Chèque Hors Place",
		"Encaissement S/Facture non Achetée",
		"Encaissement Chèque sur Place",
		"A-Régul. sur Encaissement Acheteur",
		"Encaissement LCR à Vue France",
		"Facture en Litige sans Solution",
		"Effet échu",
		"Règlement Direct",
		"Facture en Litige Risque Opérationl",
		"Facture Impayée charge Vendeur"

	) );
	
}

//-------------------------------------------------------------------------------------

function getHeadingAccount( $heading ){
	
	static $accounts = array();
	
	if( !count( $accounts ) ){

		$rs =& DBUtil::query( "SELECT * FROM heading_accounts" );
		
		while( !$rs->EOF() ){
			
			$accounts[ $rs->fields( "heading" ) ] = $rs->fields( "account" );
			$rs->MoveNext();
			
		}

	}
	
	foreach( $accounts as $accountHeading => $account ){
		
		$pattern = str_replace( "%", ".*", $accountHeading );
		
		if( preg_match( "/^$pattern\$/", $heading ) !== false )
			return $account;
			
	}
	
	return "";
	
}

//-------------------------------------------------------------------------------------

?>