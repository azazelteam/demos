<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche des avoirs clients
 */
 

//------------------------------------------------------------------------------------------------

$Title = "Rechercher un avoir";

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

//--------------------------------------------------------------------------------------------------
 
if( isset( $_GET[ "export" ] ) ){

	exportArray( getExportableArray( base64_decode( $_GET[ "export" ] ) ) );

	exit();
	
}

//------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$con =& DBUtil::getConnection();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

$resultPerPage = 50000;
$maxSearchResults = 1000;

$postData = array(
	
	"date_type" 			=> isset( $_POST[ "date_type" ] ) ? 		stripslashes( $_POST[ "date_type" ] ) : 				"credit",
	"minus_date_select" 	=> isset( $_POST[ "minus_date_select" ] ) ? stripslashes( $_POST[ "minus_date_select" ] ) : 		"-365",
	"interval_select" 		=> isset( $_POST[ "interval_select" ] ) ? 	stripslashes( $_POST[ "interval_select" ] ) : 			3,
	"idbuyer"				=> isset( $_POST[ "idbuyer" ] ) ? 			stripslashes( $_POST[ "idbuyer" ] ) : 					"",
	"lastname" 				=> isset( $_POST[ "lastname" ] ) ? 			stripslashes( $_POST[ "lastname" ] ) : 					"",
	"company" 				=> isset( $_POST[ "company" ] ) ? 			stripslashes( $_POST[ "company" ] ) : 					"",
	"zipcode" 				=> isset( $_POST[ "zipcode" ] ) ? 			stripslashes( $_POST[ "zipcode" ] ) : 					"",
	"city" 					=> isset( $_POST[ "city" ] ) ? 				stripslashes( $_POST[ "city" ] ) : 						"",
	"faxnumber" 			=> isset( $_POST[ "faxnumber" ] ) ? 		stripslashes( $_POST[ "faxnumber" ] ) : 				"",
	"idsource" 				=> isset( $_POST[ "idsource" ] ) ? 			$_POST[ "idsource" ] : 									array(),
	"iduser" 				=> isset( $_POST[ "iduser" ] ) ? 			stripslashes( $_POST[ "iduser" ] ) : 				"",
	"idbilling_buyer" 		=> isset( $_POST[ "idbilling_buyer" ] ) ? 	stripslashes( $_POST[ "idbilling_buyer" ] ) : 			"",
	"idbl_delivery" 		=> isset( $_POST[ "idbl_delivery" ] ) ? 	stripslashes( $_POST[ "idbl_delivery" ] ) : 			"",
	"idcredit" 				=> isset( $_POST[ "idcredit" ] ) ? 			stripslashes( $_POST[ "idcredit" ] ) : 					"",
	"total_amount_et" 		=> isset( $_POST[ "total_amount_et" ] ) ? 	stripslashes( $_POST[ "total_amount_et" ] ) : 			"",
	"total_amount_ati" 		=> isset( $_POST[ "total_amount_ati" ] ) ? 	stripslashes( $_POST[ "total_amount_ati" ] ): 			"",
	"editable" 				=> isset( $_POST[ "editable" ] ) ? 			$_POST[ "editable" ] : 									""
	
);

?>
<?php

displayForm();

if( isset( $_REQUEST[ "search" ] ) ){
	search();
}
else{
	// Dans le cas ou pas de recherche on ferme la globalmain div
	?>
	</div> <!-- globalMainDiv -->
	<?php
}	

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global 	$GLOBAL_START_URL,
			$postData;

	?>
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}
		
		
	// -->
	</script>
	
<div id="globalMainContent">
        	<div class="mainContent">
                <div class="topRight"></div><div class="topLeft"></div>
                <div class="content">
                	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/credit_search.php" method="post" name="searchForm">
	                	<div class="headTitle">
	                	<a name="topPage"></a>
	                        <p class="title">Rechercher un avoir</p>
	                        <div class="rightContainer">
	                        	<input type="radio" name="date_type" class="VCenteredWithText" value="credit"<?php if( $postData[ "date_type" ]  == "credit" ) echo " checked=\"checked\""; ?> /> Date de création
	                            <input type="radio" name="date_type" class="VCenteredWithText" value="invoice"<?php if( $postData[ "date_type" ] == "invoice") echo " checked=\"checked\""; ?> /> Date de facture
	                        </div>
	                    </div>
	                    <div class="subContent">
                    		<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	  						<input type="hidden" name="search" value="1" />
                    		<!-- tableau choix de date -->
                            <div class="tableContainer">
                                <table class="dataTable dateChoiceTable">
                                    <tr>	
                                        <td><input type="radio" name="interval_select" id="interval_select_since" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> />Depuis</td>
                                        <td>
                                            <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
								            	<option value="0" <?php  if($postData[ "minus_date_select" ] ==0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ;  ?></option>
								              	<option value="-1" <?php  if($postData[ "minus_date_select" ] ==-1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ;  ?></option>
							              		<option value="-2" <?php  if($postData[ "minus_date_select" ] ==-2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ;  ?></option>
								              	<option value="-7" <?php  if($postData[ "minus_date_select" ] ==-7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ; ?></option>
								              	<option value="-14" <?php  if($postData[ "minus_date_select" ] ==-14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ; ?></option>
								              	<option value="-30" <?php  if($postData[ "minus_date_select" ] ==-30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ; ?></option>
								              	<option value="-60" <?php  if($postData[ "minus_date_select" ] ==-60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ; ?></option>
								              	<option value="-90" <?php  if($postData[ "minus_date_select" ] ==-90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ; ?></option>
								              	<option value="-180" <?php  if($postData[ "minus_date_select" ] ==-180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ; ?></option>
								              	<option value="-365" <?php  if($postData[ "minus_date_select" ] ==-365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ; ?></option>
								              	<option value="-730" <?php  if($postData[ "minus_date_select" ] ==-1825) echo "selected=\"selected\""; ?> >Moins de 2 ans</option>
								              	<option value="-1825" <?php  if($postData[ "minus_date_select" ] ==-1825) echo "selected=\"selected\""; ?> >Moins de 5 ans</option>
								            </select>
                                        </td>
                                        <td><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?>/>Pour le mois de</td>
                                        <td>
                                            <?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                        </td>
                                    </tr>
                                    <tr>	
                                        <td>
                                            <input type="radio" name="interval_select" id="interval_select_between"" class="VCenteredWithText" value="3"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?>/>Entre le
                                        </td>
                                        <td>
											<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
							 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
							 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                        </td>
                                        <td>et le</td>
                                        <td>
							 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
							 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td colspan="4" class="tableSeparator"></td>
                                    </tr>
                                    <tr class="filledRow">
                                        <th>N° client</th>
                                        <td><input type="text" class="textInput" name="idbuyer" value="<?php echo $postData[ "idbuyer" ] ?>" /></td>
                                        <th>Nom du contact</th>
                                        <td><input type="text" style="border:1px solid #aaaaaa; " name="lastname" value="<?php echo $postData[ "lastname" ] ?>"/></td>
                                    </tr>
                                    <tr>
                                        <th>Nom / Raison sociale</th>
                                        <td><input type="text" class="textInput" name="company" value="<?php echo $postData[ "company" ] ?>" /></td>
                                        <th>Fax</th>
                                        <td><input type="text" class="textInput" name="faxnumber" value="<?php echo $postData[ "faxnumber" ] ?>"/></td>
                                    </tr>
                                    <tr>
                                        <th>Code postal</th>
                                        <td><input type="text" class="textInput" name="zipcode" value="<?php echo $postData[ "zipcode" ] ?>" /></td>
                                        <th>Ville</th>
                                        <td><input type="text" class="textInput" name="city" value="<?php echo $postData[ "city" ] ?>" /></td>
                                    </tr>
                                    <tr class="filledRow">
                                        <th>Provenance du client</th>
                                        <td>
                                            <?php  
								        	$lang = "_1";
								        	$rssource =& DBUtil::query( "SELECT idsource, source_code, source$lang AS source FROM source ORDER BY source$lang ASC" );
								        	?>
								        	<select name="idsource[]" multiple size="5" class="fullWidth">
								        	<option value=""<?php if( in_array( "", $postData[ "idsource" ] ) ) echo " selected=\"selected\""; ?>>Toutes</option>
								        	<?php 
								        	
												while( !$rssource->EOF() ){
											    
												    ?>
												    <option value="<?=$rssource->Fields( "idsource" );?>" <?php if(in_array( $rssource->Fields('idsource'), $postData[ "idsource" ] )){echo "selected";} ?>><?=$rssource->Fields('source');?></option>	
												    <?php 
												    
												    $rssource->MoveNext();
								
										    	}
										    	
								        	 ?>
                                        </td>
                                        <th>Commercial</th>
                                        <td>
                                            <?php
								        		$hasAdminPrivileges = User::getInstance()->get( "admin" );
								        		if( $hasAdminPrivileges )
								        			XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "lastname", $postData[ "iduser" ], $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
								        		else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
								        	?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Avoir n°</th>
                                        <td>
                                            <input type="text" class="textInput" name="idcredit"  value="<?php echo $postData[ "idcredit" ] ?>"/>
                                        </td>
                                        
                                        <th>Facture n°</th>
                                        <td>
                                            <input type="text" class="textInput"  name="idbilling_buyer"  value="<?php echo $postData[ "idbilling_buyer" ] ?>" />
                                        </td>
                                    </tr>
                                    <tr><td colspan="4" class="tableSeparator"></td></tr>
                                    <tr class="filledRow">
                                        <th>Avancement</th>
                                        <td colspan="3">
                                        <select name="editable" class="fullWidth">
										 	<option value=""></option>
											<option value="1"<?php if( $postData[ "editable" ] ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_credit_editable" ) ; ?></option>
							            	<option value="0"<?php if( $postData[ "editable" ] !== "" && !$postData[ "editable" ] ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate( "gest_com_credit_validated" ) ; ?></option>
						            	</select>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Total HT</th>
                                        <td>
                                           <input type="text" class="textInput" name="total_amount_et" value="<?php echo $postData[ "total_amount_et" ] ?>" />
                                        </td>
                                        <th>Total TTC</th>
                                        <td>
                                            <input type="text"class="textInput" name="total_amount_ati" value="<?php echo $postData[ "total_amount_ati" ] ?>" />
                                        </td>
                                	</tr>
                                </table>
                            </div>
                            <div class="submitButtonContainer">
                            	<input type="submit" class="blueButton" style="text-align:center;" value="Rechercher" />
                            </div>
                    	</div>
                    </form>
                </div>
                <div class="bottomRight"></div><div class="bottomLeft"></div>
            </div>
            <div id="tools" class="rightTools">
            	<div class="toolBox">
                	<div class="header">
                    	<div class="topRight"></div><div class="topLeft"></div>
                        <p class="title">Outils</p>
                    </div>
                    <div class="content"></div>
                    <div class="footer">
                    	<div class="bottomLeft"></div>
                        <div class="bottomMiddle"></div>
                        <div class="bottomRight"></div>
                    </div>
                </div>
                <div class="toolBox">
                	<div class="header">
                    	<div class="topRight"></div><div class="topLeft"></div>
                        <p class="title">Infos Plus</p>
                    </div>
                    <div class="content">
                    </div>
                    <div class="footer">
                    	<div class="bottomLeft"></div>
                        <div class="bottomMiddle"></div>
                        <div class="bottomRight"></div>
                    </div>
                </div>
            </div>

	<?php 

}
//----------------------------------------------------------------------------------------------------

function search(){
	
	global 	$maxSearchResults, 
			$resultPerPage, 
			$hasAdminPrivileges,
			$GLOBAL_START_URL,
			$postData;

 	$now = getdate();

	switch( $postData[ "interval_select" ] ){
		    
	     case 1:
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $postData[ "minus_date_select" ] ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$dateTable = $postData[ "date_type" ] == "credit" ? "credits" : "billing_buyer";
			$SQL_Condition  = "`$dateTable`.DateHeure >='$Min_Time' AND `$dateTable`.DateHeure <='$Now_Time' ";
			
			break;
		
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;

		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$dateTable = $postData[ "date_type" ] == "credit" ? "credits" : "billing_buyer";
			$SQL_Condition  = "`$dateTable`.DateHeure >='$Min_Time' AND `$dateTable`.DateHeure <'$Max_Time' ";
	     	break;
		     
		case 3:
			
			$Min_Time = euDate2us( $_POST[ "bt_start" ] );
			$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
			
			$dateTable = $postData[ "date_type" ] == "credit" ? "credits" : "billing_buyer";
			$SQL_Condition  = "`$dateTable`.DateHeure >= '$Min_Time' AND `$dateTable`.DateHeure <= '$Max_Time' ";
	     	
	     	break;
			
	}

	if( !$hasAdminPrivileges ){
	
		$iduser = User::getInstance()->getId();	
		$SQL_Condition .= " AND ( buyer.iduser = '$iduser' OR credits.iduser = '$iduser' )";
	
	}
		
	if ( !empty($postData[ "idbuyer" ]) ){
		
			$idbuyer = FProcessString($postData[ "idbuyer" ]);
			$SQL_Condition .= " AND credits.idbuyer IN( $idbuyer )";
	
	}

	if ( !empty($postData[ "idcredit" ]) ){
		
			$idcredit = FProcessString($postData[ "idcredit" ]);
			$SQL_Condition .= " AND credits.idcredit IN( $idcredit )";
	
	}
	
	if ( !empty($postData[ "idbilling_buyer" ]) ){
		
			$idbilling_buyer = FProcessString( $postData[ "idbilling_buyer" ] );
			$SQL_Condition .= " AND credits.idbilling_buyer IN( $idbilling_buyer )";
	
	}

	if ( !empty($postData[ "lastname" ]) )
		$SQL_Condition .= " AND contact.lastname LIKE '%" . Util::html_escape( $postData[ "lastname" ] ) . "%'";

	if ( $postData[ "editable" ] !== "" )
		$SQL_Condition .= " AND credits.editable = '" . $postData[ "editable" ] . "'";

	if ( is_array( $postData[ "idsource" ] ) && count( $postData[ "idsource" ] ) ){
		
		$allSelected = false;
		foreach( $postData[ "idsource" ] as $idsource ){
			
			if( $idsource === "" )
				$allSelected = true;
		
		}
		
		if( !$allSelected )
			$SQL_Condition .= " AND buyer.idsource IN( " . implode( ",", $postData[ "idsource" ] ). ")";

	}
	
	if ( !empty($postData[ "iduser" ]) )
		$SQL_Condition .= " AND credits.iduser = '" . $postData[ "iduser" ] ."'";
	
	if( !empty( $postData[ "zipcode" ] ) )
		$SQL_Condition .= " AND buyer.zipcode LIKE '" . Util::html_escape( $postData[ "zipcode" ] ) . "%'";
		
	if( !empty( $postData[ "company" ] ) )
		$SQL_Condition .= " AND buyer.company LIKE '%" . Util::html_escape( $postData[ "company" ] ) . "%'";
	
	if( !empty( $postData[ "city" ] ) )
		$SQL_Condition .= " AND buyer.city LIKE '%" . Util::html_escape( $postData[ "city" ] ) . "%'";
		
	if( !empty( $postData[ "faxnumber" ] ) ){
		
		$faxnumbers = ereg_replace( "[^0-9]*", "", $postData[ "faxnumber" ] );
		$regexp = "";
		
		$i = 0;
		while( $i < strlen( $faxnumbers ) ){
			
			if( $i )
				$regexp .= "[^0-9]*";
				
			$regexp .= $faxnumbers{ $i } . "{1}";
			
			$i++;
			
		}
		
		$SQL_Condition .= " AND contact.faxnumber REGEXP( '^.*$regexp.*\$' )";
	
	}

	//clause HAVING
	
	$having = "";
	if( !empty( $postData[ "total_amount_et" ] ) ){
		
		$total_amount_et = Util::text2num( $postData[ "total_amount_et" ] );
		$having = "HAVING total_amount_et = '$total_amount_et'";
		
	}
	
	if( !empty( $postData[ "total_amount_ati" ] ) ){
		
		if( strlen( $having ) )
			$having .= " AND ";
		else $having .= " HAVING";
		
		$total_amount_ati = Util::text2num( $postData[ "total_amount_ati" ] );
		$having .= " total_amount_ati = '$total_amount_ati'";
		
	}

	$Str_FieldList = "
	DISTINCT( credits.idcredit ),
	credits.idbuyer,
	credits.iduser,
	credits.idbilling_buyer,
	credits.DateHeure AS credit_creation_date,
	credits.editable,
	billing_buyer.DateHeure AS invoice_creation_date,
	contact.lastname,
	contact.title,
	contact.faxnumber,
	contact.firstname,
	buyer.company,
	buyer.idsource,
	buyer.contact,
	buyer.iderp,
	buyer.zipcode,
	user.initial,
	ROUND( COALESCE( SUM( credit_rows.discount_price * credit_rows.quantity ), 0.0 ) - credits.total_charge_ht + credits.credited_charges, 2 ) AS total_amount_et,
	ROUND( COALESCE( SUM( credit_rows.discount_price * credit_rows.quantity * ( 1.0 + credit_rows.vat_rate / 100.0 ) ), 0.0 ) + (  credits.credited_charges - credits.total_charge_ht ) * ( 1.0 + credits.vat_rate / 100.0 ), 2 ) AS total_amount_ati";
	
	$tables = "billing_buyer, buyer, user, contact, credits LEFT JOIN credit_rows ON credits.idcredit = credit_rows.idcredit";
	
	$SQL_Condition .= " 
	AND credits.idbilling_buyer = billing_buyer.idbilling_buyer
	AND credits.iduser = user.iduser
	AND credits.idbuyer = buyer.idbuyer 
	AND credits.idbuyer = contact.idbuyer
	AND credits.idcontact = contact.idcontact";
	
	if( isset( $_REQUEST['sortby'] ) && isset( $_REQUEST['sens'] ) )
		$orderby	= Util::html_escape( stripslashes( $_REQUEST['sortby'] ) )." ".Util::html_escape( stripslashes( $_REQUEST['sens'] ) );
	else $orderby	= "buyer.company ASC";
	
	//recherche
	
	$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
	
	$start = ( $page - 1 ) * $resultPerPage;
	
	$query = "
	SELECT $Str_FieldList 
	FROM $tables 
	WHERE $SQL_Condition 
	GROUP BY credits.idcredit $having
	ORDER BY $orderby 
	LIMIT $start, $resultPerPage";

	$rs =& DBUtil::query( $query );
	
	//nombre de pages
		
	$resultCount = $rs->RecordCount();
	
	if( !$resultCount ){
		
		?>
		<!-- On insere dans la boite a outils de droite, dans la section message -->
		<script type="text/javascript">
		<!--
			document.getElementById("msgInfos").innerHTML = '<?php  echo Dictionnary::translate("no_result"); ?>' ;
		-->
		</script>
		</div> <!-- GlobalMainDiv -->
		<?php
			
		return;
		
	}
	
	if( $resultCount > $maxSearchResults ){
		
		?>
		<!-- On insere dans la boite a outils de droite, dans la section message -->
		<script type="text/javascript">
		<!--
			document.getElementById("msgInfos").innerHTML = 'Plus de  <?php echo $maxSearchResults ?> factures ont &eacute;t&eacute; trouv&eacute;s <br /> Merci de bien vouloir affiner votre recherche' ;
		-->
		</script>
		</div> <!-- GlobalMainDiv -->
		<?php	
	
		return;
		
	}
	
	$pageCount = ceil( $resultCount / $resultPerPage );
	
	?>
	<!-- TABLEAU DE RECHERCHE -->
	<div class="mainContent fullWidthContent">
            	<div class="topRight"></div><div class="topLeft"></div>
                <div class="content">
                	<div class="headTitle">
                        <p class="title">Résultats de la recherche : <?php echo $resultCount ?> avoir(s)</p>
						<div class="rightContainer">
							<a href="<?php echo $GLOBAL_START_URL ?>/accounting/credit_search.php?export=<?php echo urlencode( base64_encode( $query ) ) ?>" class="blackText">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" />
								Export Avoirs
							</a>
							<!--<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_credits.php?query=<?php
							
								global $GLOBAL_DB_PASS, $GLOBAL_START_PATH;
						
								include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
								
								echo URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
 
								?>" class="blackText">
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" />
								Export factor
							</a>-->
						</div>
                    </div>
                    <div class="subContent">
						<div class="tableHeaderLeft">
							<a href="#bottom" class="goUpOrDown">
								Aller en bas de page
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
							</a>
						</div>
                    	<!-- tableau résultats recherche -->
                        <div class="resultTableContainer clear">
                        	<table class="dataTable resultTable">
                            	<thead>
                                    <tr>
	                                    <th>Com.</th>
										<th>Avoir n°</th>
										<th>Facture n°</th>
										<th>Client n°</th>
							  			<th>Code postal</th>
										<th>Raison sociale</th>
										<th>Contact (nom)</th>
										<th>Provenance</th>
							  			<th>Statut</th>
							  			<th>Date de l'avoir</th>
							  			<th>Date de facturation</th>
										<th>Total HT</th>
										<th>Total TTC</th>
                                    </tr>
                                    <!-- petites flèches de tri -->
                                    <tr>
                                    	<th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'user.initial','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'user.initial','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'credits.idcredit','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'credits.idcredit','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'credits.idbilling_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'credits.idbilling_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'credits.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'credits.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'buyer.zipcode','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'buyer.zipcode','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'buyer.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'buyer.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'contact.lastname','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'contact.lastname','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'buyer.idsource','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'buyer.idsource','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'credits.editable','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'credits.editable','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'credit_creation_date','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'credit_creation_date','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'invoice_creation_date','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'invoice_creation_date','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'total_amount_et','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'total_amount_et','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                        <th class="noTopBorder">
                                        	<a href="javascript:SortResult( 'total_amount_ati','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
                                            <a href="javascript:SortResult( 'total_amount_ati','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
                                        </th>
                                	</tr>
                                </thead>
                                <tbody>
                                <?php
							    $con =& DBUtil::getConnection();
								$lang = "_1";
								$creditCount 				= 0;
								$validatedCount 			= 0;
								$totalAmountETSum 			= 0.0;
								$totalAmountATISum 			= 0.0;
								$validatedTotalAmountETSum 	= 0.0;
								$validatedTotalAmountATISum = 0.0;
							    while( !$rs->EOF() ){
									//avoir
									$idcredit 				= $rs->fields( "idcredit" );
									$credit_creation_date 	= $rs->fields( "credit_creation_date" );
									$editable 				= $rs->fields( "editable" );
									$status 				= $editable ? Dictionnary::translate( "gest_com_credit_editable" ) : Dictionnary::translate( "gest_com_credit_validated" );
									$creditHREF 			= "$GLOBAL_START_URL/accounting/credit.php?idcredit=$idcredit";
									$total_amount_et 		= $rs->fields( "total_amount_et" );
									$total_amount_ati 		= $rs->fields( "total_amount_ati" );
									
									//facture
									
									$idbilling_buyer 		= $rs->fields( "idbilling_buyer" );
									$invoice_creation_date 	= $rs->fields( "invoice_creation_date" );
									$invoiceHREF 			= "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer";
									
									//client
									
									$idbuyer 				= $rs->fields( "idbuyer" );
									$zipcode 				= $rs->fields( "zipcode" );
									$company 				= $rs->fields( "company" );
									$lastname 				= $rs->fields( "lastname" );
									$title 					= $rs->fields( "title" );
									$faxnumber 				= $rs->fields( "faxnumber" );
									$firstname 				= $rs->fields( "firstname" );
									$idsource 				= $rs->fields( "idsource" );
									$source 				= DBUtil::getDBValue( "source$lang", "source", "idsource", $idsource );
									$contact 				= $rs->fields( "contact" );
									$isContact 				= $rs->fields( "contact" ) == 1;
									$buyerHREF 				= "$GLOBAL_START_URL/sales_force/contact_buyer.php?type=";
									$buyerHREF 				.= $isContact ? "contact" : "buyer";
									$buyerHREF 				.= "&key=$idbuyer";
									
									//commercial
									
									$init 					= $rs->fields( "initial" );
									?>
                                	<tr class="blackText">
	                                	<td class="lefterCol"><?php echo htmlentities( $init ) ?></td>
								        <td><a href="<?php echo $creditHREF ?>" onclick="window.open(this.href); return false;" class="grasBack"><?php echo $idcredit ?></a></td>
								        <td style="white-space:nowrap;">
								        <?php
								        
								        	/*litige */
								        	
								        	$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1" );
											if( $rsLitigation->RecordCount() ){
												
												?>
												<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="cursor:pointer; vertical-align:middle;" onclick="window.open( '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer' );" />
												<?php
													
											}
											
											?>
								        	<a href="<?php echo $invoiceHREF ?>" onclick="window.open(this.href); return false;"><?php echo $idbilling_buyer ?></a>
								        </td>  
								        <td><a href="<?php echo $buyerHREF ?>"><?php echo $idbuyer ?></a></td>
								        <td><?php echo substr( $zipcode, 0, 2 ) ?></td>
								        <td><a href="<?php echo $buyerHREF ?>" class="grasBack"><?php echo htmlentities( $company ) ?></a></td>
								        <td><a href="<?php echo $buyerHREF ?>"><?php echo htmlentities( $lastname ) ?></a></td>
								  		<td><?php echo htmlentities( $source ) ?></td>
								        <td><?php echo htmlentities( $status ) ?></td>
								        <td><?php echo usDate2eu( substr( $credit_creation_date, 0, 10 ) ) ?></td>
								        <td><?php echo usDate2eu( substr( $invoice_creation_date, 0, 10 ) ) ?></td>
								        <td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_et ) ?></td>
								        <td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_ati ) ?></td>
                                    </tr>
                                    <?php 
		 								// données statistiques--------------------------------------------------------------
										$creditCount		++;
										$totalAmountETSum 	+= $total_amount_et;
										$totalAmountATISum 	+= $total_amount_ati;
										$validatedCount 			+= 1 - $editable;
										$validatedTotalAmountETSum 	+= ( 1 - $editable ) * $total_amount_et;
										$validatedTotalAmountATISum += ( 1 - $editable ) * $total_amount_ati;
										//-------------------------------------------------------------------------------
										$rs->MoveNext();
									}
									?>

                                </tbody>
                            </table>
                        </div>
                        <?php
                        paginate();
                        ?>
                        <a href="#topPage" class="goUpOrDown">
                        	Aller en haut de page
                            <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                        </a>
                   	</div>
           		</div>
                <div class="bottomRight"></div><div class="bottomLeft"></div>
            </div> <!-- mainContent fullWidthContent -->
            
        	<div class="mainContent fullWidthContent">
            	<div class="topRight"></div><div class="topLeft"></div>
                <div class="content">
                	<div class="headTitle">
                        <p class="title">Statistiques sur les avoirs</p>
						<div class="rightContainer"></div>
                    </div>
                    <div class="subContent">
						<div class="tableHeaderLeft">
							<a href="#bottomPage" class="goUpOrDown">
								Aller en bas de page
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
							</a>
						</div>
						<div class="tableHeaderRight">
							<!--<a href="#"><img src="img/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
							<a href="#"><img src="img/content/xls_icon.png" alt="Exporter les donnéés au format XLS" /></a>-->
						</div>
                    	<!-- tableau résultats recherche -->
                        <div class="resultTableContainer clear">
                        	<table class="dataTable resultTable">
                            	<thead>
                                    <tr>
                                        <th>&nbsp;</th>
										<th><?php echo Dictionnary::translate("gest_com_total_amount_et"); ?></th>
										<th><?php echo Dictionnary::translate("gest_com_total_amount_ati"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                	<tr>
                                    	<td class="lefterCol"><?php echo Dictionnary::translate("gest_com_quantity"); ?></td>
										<td><?php echo $creditCount ?></td>
										<td class="righterCol"><?php echo $creditCount ?></td>
                                    </tr>
                                    <tr>
                                    	<td class="lefterCol"><?php echo Dictionnary::translate("gest_com_amount"); ?></td>
										<td><?php echo Util::priceFormat( $totalAmountETSum ) ?></td>
										<td class="righterCol"><?php echo Util::priceFormat( $totalAmountATISum ) ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a name="bottomPage"></a>
                        <a href="#topPage" class="goUpOrDown">
                        	
                        	Aller en haut de page
                            <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                        </a>
                   	</div>
           		</div>
                <div class="bottomRight"></div><div class="bottomLeft"></div>
            </div> <!-- mainContent fullWidthContent -->
            
            
        </div><!-- globalMainContent -->
<?php
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------
 
function &getExportableArray( $query ){
	
	$rs =& DBUtil::query( $query );
	
	$data = array(
	
		Dictionnary::translate( "gest_com_export_idcredit" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_DateHeure" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_idbilling_buyer" ) 	=> array(),
		Dictionnary::translate( "gest_com_export_idbuyer" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_company" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_title" ) 				=> array(),
		Dictionnary::translate( "gest_com_export_firstname" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_lastname" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_source" ) 				=> array(),
		Dictionnary::translate( "gest_com_export_contact" ) 			=> array(),
		Dictionnary::translate( "gest_com_export_total_amount_et" ) 	=> array(),
		Dictionnary::translate( "gest_com_export_total_amount_ati" ) 	=> array(),

	);
	
	$lang = "_1";
	
	while( !$rs->EOF() ){
		
		$data[ Dictionnary::translate( "gest_com_export_idcredit" ) ][]			= $rs->fields( "idcredit" );
		$data[ Dictionnary::translate( "gest_com_export_DateHeure" ) ][] 		= $rs->fields( "credit_creation_date" );
		$data[ Dictionnary::translate( "gest_com_export_idbilling_buyer" ) ][] 	= $rs->fields( "idbilling_buyer" );
		$data[ Dictionnary::translate( "gest_com_export_idbuyer" ) ][] 			= $rs->fields( "idbuyer" );
		$data[ Dictionnary::translate( "gest_com_export_company" ) ][] 			= $rs->fields( "company" );
		$data[ Dictionnary::translate( "gest_com_export_title" ) ][] 			= $rs->fields( "title" );
		$data[ Dictionnary::translate( "gest_com_export_firstname" ) ][] 		= $rs->fields( "firstname" );
		$data[ Dictionnary::translate( "gest_com_export_lastname" ) ][] 		= $rs->fields( "lastname" );
		$idsource 																= $rs->fields( "idsource" );
		$data[ Dictionnary::translate( "gest_com_export_source" ) ][] 			= DBUtil::getDBValue( "source$lang", "source", "idsource", $idsource );
		$contact 																= $rs->fields( "contact" );
		$data[ Dictionnary::translate( "gest_com_export_contact" ) ][] 			= $rs->fields( "contact" ) ? Dictionnary::translate( "gest_com_yes" ) : Dictionnary::translate( "gest_com_no" );
		$data[ Dictionnary::translate( "gest_com_export_total_amount_et" ) ][] 	= $rs->fields( "total_amount_et" );
		$data[ Dictionnary::translate( "gest_com_export_total_amount_ati" ) ][] = $rs->fields( "total_amount_ati" );
		
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------------------------

/**
 * Exporter les données
 */
function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray, "export_avoirs_" . date( "Ymd" ) );
	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------------------------
?>