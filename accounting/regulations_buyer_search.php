<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche des règlements clients
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
include_once( "$GLOBAL_START_PATH/objects/Invoice.php" );

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
				
$hasAdminPrivileges = User::getInstance()->get( "admin" );
$Iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin( "gest_com_use_help" );

$resultPerPage = 50000;
$maxSearchResults = 1000;

if( isset($_GET['deleteRegulation']) ){
	if( isset( $_GET['idRegulation'] ) && $_GET['idRegulation'] != "" ){
		
		$myRegul = $_GET['idRegulation'];
		
		// Supprimer un reglement fournisseur :
		// - supprimer le(s) lien(s) dans billing_regulations_buyer
		// - remettre la(les) facture(s) en 'Invoiced' vu qu'elle est(sont) plus payée(s)
		// - on ne replace pas le 'credit_amount_used' parcequ'il est dans la facture
		// - de même pour l'escompte, il est dans la facture
		// - on déduit le overpayment s'il est dans la tranche et accepté (en gros si la facture est 'Paid' et que l'overpayment est dans la tranche)
		
		// d'abord on va récup les détails du règlement
		$myRegulation = DBUtil::query( "SELECT * FROM regulations_buyer WHERE idregulations_buyer = $myRegul LIMIT 1" );
		
		if( $myRegulation === false || $myRegulation->RecordCount() == 0)
			exit( "Ce règlement n'existe pas" );
			
		$idregulation	= $myRegulation->fields( 'idregulations_buyer' );
		$idbuyer		= $myRegulation->fields( 'idbuyer' );
		$amount 		= $myRegulation->fields( 'amount' );
		$credBalUsed	= $myRegulation->fields( 'credit_balance_used' );
		$rebateAmount	= $myRegulation->fields( 'rebate_amount' );
		$overpayment	= $myRegulation->fields( 'overpayment' );
		
		// on met les factures à 'Invoiced', on vire les liens et le reglement
		$billings = DBUtil::query("	SELECT brb.idbilling_buyer, bb.status
									FROM billing_regulations_buyer brb, billing_buyer bb 
									WHERE brb.idregulations_buyer = $idregulation 
									AND bb.idbilling_buyer = brb.idbilling_buyer");
		
		$solde_tolerance = DBUtil::getParameterAdmin( "solde_tolerance" );
		$max_solde_tolerance = DBUtil::getParameterAdmin( "max_solde_tolerance" );
		
		if($billings->RecordCount() > 0){
			// il faut enlever le crédit client si le reglement était supérieur aux factures
			$rsSumAmounts = DBUtil::query("	SELECT SUM(brb.`amount`) as sumAmounts
											FROM billing_buyer bb, billing_regulations_buyer brb
											WHERE bb.idbilling_buyer = brb.idbilling_buyer
											AND brb.idregulations_buyer = $idregulation");
			
			$sommeFactures = $rsSumAmounts->fields( 'sumAmounts' ) - $rebateAmount;
			
			// trop percu
			if( $amount > $sommeFactures ){
				$newCredSupplier = DBUtil::query( "SELECT credit_balance FROM buyer WHERE idbuyer = $idbuyer" )->fields( 'credit_balance' ) - ( $amount - $sommeFactures );
			
				DBUtil::query( "UPDATE buyer SET `credit_balance` = $newCredSupplier WHERE idbuyer = $idbuyer " );
			}
	
			// avoirs
			if( $credBalUsed > 0 ){
				$newCredSupplier = $credBalUsed + DBUtil::query( "SELECT credit_balance FROM buyer WHERE idbuyer = $idbuyer" )->fields( 'credit_balance' );
			
				DBUtil::query( "UPDATE buyer SET `credit_balance` = $newCredSupplier WHERE idbuyer = $idbuyer " );
			}
						
			// factures
			while(!$billings->EOF){
				$billing2updt  = $billings->fields('idbilling_buyer');
				$billinsStatus = $billings->fields('status');
						
				if( $credBalUsed > 0 ){
			
					//
					DBUtil::query( "UPDATE billing_buyer SET `credit_amount` = 0 WHERE idbilling_buyer = '$billing2updt' " );
					
				}

				DBUtil::query( "UPDATE billing_buyer SET `status` = 'Invoiced' WHERE idbilling_buyer = '$billing2updt' " );
			
				$billings->MoveNext();
			}
				
			// liens
			DBUtil::query("DELETE FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation");
			
			// règlement
			DBUtil::query("DELETE FROM regulations_buyer WHERE idregulations_buyer = $idregulation");
				
		}else{
			$myRegulation = DBUtil::query( "SELECT * FROM regulations_buyer WHERE idregulations_buyer = $myRegul LIMIT 1" );
			$idbuyer		= $myRegulation->fields( 'idbuyer' );
			$amount 		= $myRegulation->fields( 'amount' );
			
			DBUtil::query( "UPDATE buyer SET `credit_balance` = `credit_balance` - $amount WHERE idbuyer = $idbuyer " );
			
			DBUtil::query("DELETE FROM regulations_buyer WHERE idregulations_buyer = $idregulation");
		}
		
		exit();
	}else{
		exit( "Paramètre manquant !" );
	}
}

if( isset( $_REQUEST[ "search" ] ) ){
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
}


$Title = Dictionnary::translate( "search_buyer_payment" );
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
<?php

displayForm();

?>
</div><!-- GlobalMainContent -->
<?php
	
include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() {
			
			var options = {
				
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
				
			};
			
			$('#searchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			$("#selectionAmount").val(0);
			$("#selectionAmountDiv").html("0.00 ¤");
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( 'Une erreur est survenue', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function getCheques(){
			document.getElementById('searchForm').action = "<?php echo $GLOBAL_START_URL ?>/accounting/regulations_checks.php";
			
		   	window.open("<?php echo $GLOBAL_START_URL ?>/accounting/regulations_checks.php", "popup", "height=800,width=840,menubar='no',toolbar='no',location='no',status='no',scrollbars=yes");
		   	
		  	document.getElementById('searchForm').target = "popup";

			document.getElementById('searchForm').submit(); 
			
			document.getElementById('searchForm').action = "<?php echo $GLOBAL_START_URL ?>/accounting/regulations_buyer_search.php";
					
		}
			
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback2( responseText, statusText ){ 
			
			$.unblockUI();
			
			$("#selectionAmount").val(0);
			$("#selectionAmountDiv").html("0.00 ¤");
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( 'Une erreur est survenue', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
		}
		
		/* --------------------------------------------------------------------------------------- */
	/* ]]> */
	</script>
		
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}
		
		function showRow( rowToShow ){
			$( '#tableSearch' ).find( '.searchDoc' ).each( function(){
				$(this).hide();
			});
			
			$( '#' + rowToShow ).show();
		}	
		
	// -->
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
	<a name="topPage"></a>
	<div class="mainContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/regulations_buyer_search.php" method="post" id="searchForm" name="searchForm">
	        	<div class="headTitle">
	                <p class="title">Rechercher un règlement client</p>
	                <div class="rightContainer">
	                	<input type="radio" name="date_type" value="DateHeure" class="VCenteredWithText" />Date d'enregistrement
	                	<input type="radio" name="date_type" value="payment_date" class="VCenteredWithText" checked="checked"/>Date de règlement
	                    <input type="radio" name="date_type" value="deliv_payment" class="VCenteredWithText" />Date d'échéance
	                </div>
	            </div> 
	            <div class="subContent">
					<input type="hidden" name="page" value="" />
					<input type="hidden" name="search" value="1" />
            		<!-- tableau choix de date -->
                    <div class="tableContainer">
                        <table class="dataTable dateChoiceTable" id="tableSearch">
                            <tr>	
                                <td><input type="radio" name="interval_select"  checked="checked" id="interval_select_since" value="1" class="VCenteredWithText" />Depuis</td>
                                <td>
                                    <select name="minus_date_select" onclick="selectSince();" class="fullWidth">
						            	<option value="0">Aujourd'hui</option>
						              	<option value="-1">Hier</option>
					              		<option value="-2">Moins de 2 jours</option>
						              	<option value="-7">Moins de 1 semaine</option>
						              	<option value="-14">Moins de 2 semaines</option>
						              	<option value="-30">Moins de 1 mois</option>
						              	<option value="-60">Moins de 2 mois</option>
						              	<option value="-90">Moins de 3 mois</option>
						              	<option value="-180">Moins de 6 mois</option>
						              	<option value="-365">Moins de 1 an</option>
						              	<option value="-730">Moins de 2 ans</option>
						              	<option value="-1825">Moins de 5 ans</option>
						            </select>
                                </td>
                                <td><input type="radio" name="interval_select" id="interval_select_month" value="2" class="VCenteredWithText" />Pour le mois de</td>
                                <td>
                                    <?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                </td>
                            </tr>
                            <tr>	
                                <td>
                                    <input type="radio" name="interval_select" id="interval_select_between" value="3" class="VCenteredWithText" />Entre le
                               </td>
                                <td>
					 				<input type="text" name="bt_start" id="bt_start" value="" class="calendarInput" />
					 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                </td>
                                <td>et le</td>
                                <td>
					 				<input type="text" name="bt_stop" id="bt_stop" value="" class="calendarInput" />
					 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                </td>
                            </tr>
	                        <tr>
	                        	<td colspan="4" class="tableSeparator"></td>
	                        </tr>
	                        
	                        
                    		<tr class="filledRow">
                    			<th>Document</th>
                    			<td colspan="3">
		                    		<input type="radio" name="documentSearch" value="billing" onclick="showRow( 'TRidbilling_buyer' );" />&nbsp;Facture
		                    		<input type="radio" name="documentSearch" value="order" onclick="showRow( 'TRidorder' );" />&nbsp;Commande
		                    		<input type="radio" name="documentSearch" value="estimate" onclick="showRow( 'TRidestimate' );" />&nbsp;Devis
		                    		<input type="radio" name="documentSearch" value="all" onclick="showRow( 'TRall' );" checked="checked" />&nbsp;Tous
		                    	</td>
	                    	</tr>                            
                            <tr class="filledRow searchDoc" id="TRidbilling_buyer">                          
                                <th>Facture n°</th>
                                <td colspan="3">
                                	<input type="text" class="textInput" name="idbilling_buyer"  value="" />
                                </td>
                            </tr>
                            
                            <tr class="filledRow searchDoc" id="TRidorder" style="display:none;">
                                <th>Commande n°</th>
                                <td colspan="3">
                                	<input type="text" class="textInput" name="idorder"  value="" />
                                </td>
                            </tr>
                            
                            <tr class="filledRow searchDoc" id="TRidestimate" style="display:none;">   
                                <th>Devis n°</th>
                                <td colspan="3">
                                	<input type="text" class="textInput" name="idestimate"  value="" />
                                </td>                               
                            </tr>
                            
                            <tr class="filledRow searchDoc" id="TRall" style="display:none;">   
                                <th colspan="4">Recherche dans tous les documents ( Devis - Commandes - Factures )</th>                            
                            </tr>
                                                  
                            <tr>
                            	<td colspan="4" class="tableSeparator"></td>
                            </tr>
	                        
                            <tr>
                                <th>Total facturé HT</th>
                                <td><input type="text" name="total_amount_ht"  value="" class="textInput" /></td>
                                <th>Total facturé TTC</th>
                                <td><input type="text" name="total_amount"  value="" class="textInput" /></td>
                            </tr>
                            <tr>
                                <th>Mode de règlement</th>
                                <td><?php echo List_payment() ?></td>
                                <th>Type de règlement</th>
                                <td>
                                	<select name="regulationDest">
                                		
                                		<option value="">Tous</option>
                                		<option value="'0'">Direct</option>
                                		<option value="'1'">Factor</option>                                	
                                	</select>
                                </td>
                                
                            </tr>
                             <tr>
	                        	<td colspan="4" class="tableSeparator"></td>
	                        </tr>
                            <tr>
                                <th>Client n°</th>
                                <td><input type="text" name="idbuyer" value="" class="textInput" /></td>
                                <th>Nom / Raison sociale</th>
                                <td><input type="text" name="company" value="" class="textInput" /></td>
                            </tr>
                        </table>
                    </div>
                    <div class="submitButtonContainer">
                    	<input type="submit" class="blueButton" value="Rechercher" />
                    </div>
            	</div>
            </form>
        </div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content" style="padding-top:5px;">
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>
    <div id="SearchResults"></div>
	<?php 
}


/////-----------------------

function search(){

	global	$resultPerPage,
			$hasAdminPrivileges,
			$GLOBAL_START_URL,
			$GLOBAL_START_PATH,
			$ScriptName;
	
	$now = getdate();
	
	$date_type 				= isset( $_POST[ "date_type" ] ) 			? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) 	? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) 		? $_POST[ "interval_select" ] : 1;
	$idbuyer 				= isset( $_POST[ "idbuyer" ] ) 				? $_POST[ "idbuyer" ] : "";
	$company 				= isset( $_POST[ "company" ] ) 				? stripslashes( $_POST[ "company" ] ) : "";
	$faxnumber 				= isset( $_POST[ "faxnumber" ] ) 			? $_POST[ "faxnumber" ] : "";
	$idsource 				= isset( $_POST[ "idsource" ] ) 			? $_POST[ "idsource" ] : "";
	$iduser 				= isset( $_POST[ "iduser" ] ) 				? $_POST[ "iduser" ] : "";
	$idorder 				= isset( $_POST[ "idorder" ] ) 				? $_POST[ "idorder" ] : "";
	$idestimate 			= isset( $_POST[ "idestimate" ] ) 			? $_POST[ "idestimate" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) 	? $_POST[ "idorder_supplier" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) 		? $_POST[ "idbl_delivery" ] : "";
	$idbilling_buyer 		= isset( $_POST[ "idbilling_buyer" ] ) 		? $_POST[ "idbilling_buyer" ] : "";
	$status 				= isset( $_POST[ "status" ] ) 				? stripslashes( $_POST[ "status" ] ) : "";
	$total_amount_ht 		= isset( $_POST[ "total_amount_ht" ] ) 		? $_POST[ "total_amount_ht" ] : "";
	$total_amount 			= isset( $_POST[ "total_amount" ] ) 		? $_POST[ "total_amount" ] : "";
	$idpayment	 			= isset( $_POST[ "idpayment" ] ) 			? $_POST[ "idpayment" ] : 0;
	$regulationDest			= isset( $_POST[ "regulationDest" ] )		? stripslashes($_POST[ "regulationDest" ]) : "";
	
	$Date_ConditionDP = "";
	$Date_ConditionDPE = "";
	$SQL_Condition2 = "";
	$SQL_Condition3 = "1";
	$SQL_Condition5 = "";
	$SQL_Condition6 = "1";
	
	switch ($interval_select){
		
		case 1:
		 	//min |2000-12-18 15:25:53|
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			//$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`regulations_buyer`.$date_type >='$Min_Time'";
			$Date_ConditionDP = "o.down_payment_date >='$Min_Time'";
			$Date_ConditionDPE = "es.down_payment_date >='$Min_Time'";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
			
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`regulations_buyer`.$date_type >='$Min_Time' AND `regulations_buyer`.$date_type <'$Max_Time' ";
			$Date_ConditionDP  = "o.down_payment_date >='$Min_Time' AND o.down_payment_date <'$Max_Time' ";
			$Date_ConditionDPE = "es.down_payment_date >='$Min_Time' AND es.down_payment_date <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
			 
			$SQL_Condition  = "`regulations_buyer`.$date_type >='$Min_Time' AND `regulations_buyer`.$date_type <='$Max_Time 23:59:59' ";
			$Date_ConditionDP  = "o.down_payment_date >='$Min_Time' AND o.down_payment_date <='$Max_Time 23:59:59' ";
			$Date_ConditionDPE = "es.down_payment_date >='$Min_Time' AND es.down_payment_date <='$Max_Time 23:59:59' ";
	     	break;
			
	}
	
	$SQL_Condition4 = $SQL_Condition;

	if(!empty($regulationDest)){
		$SQL_Condition .= " AND regulations_buyer.factor = $regulationDest ";
	}

	$SQL_Condition2.= $Date_ConditionDP;
	$SQL_Condition5.= $Date_ConditionDPE;
	
	if( !User::getInstance()->get( "admin" ) ){
		$SQL_Condition .= " AND ( buyer.iduser = '$iduser' OR `regulations_buyer`.iduser = '$iduser' )";
		$SQL_Condition2.= " AND ( buyer.iduser = '$iduser' OR o.iduser = '$iduser' )";
		$SQL_Condition3.= " AND ( buyer.iduser = '$iduser' OR o.iduser = '$iduser' )";
		$SQL_Condition5.= " AND ( buyer.iduser = '$iduser' OR es.iduser = '$iduser' )";
		$SQL_Condition6.= " AND ( buyer.iduser = '$iduser' OR es.iduser = '$iduser' )";
	}

	if ( !empty($idbuyer) ){
		$idbuyer = FProcessString($idbuyer);
		$SQL_Condition .= " AND buyer.idbuyer =$idbuyer";
		$SQL_Condition2.= " AND buyer.idbuyer =$idbuyer";
		$SQL_Condition3.= " AND buyer.idbuyer =$idbuyer";
		$SQL_Condition5.= " AND buyer.idbuyer =$idbuyer";
		$SQL_Condition6.= " AND buyer.idbuyer =$idbuyer";
		$SQL_Condition4.= " AND `regulations_buyer`.idbuyer =$idbuyer";
	}

	if ( !empty($idbilling_buyer) ){
		$idbilling_buyer = FProcessString( $idbilling_buyer );
		$SQL_Condition .= " AND billing_regulations_buyer.idbilling_buyer IN( $idbilling_buyer )";
	}

	if ( !empty($idorder) ){
		$idorder = $idorder;
		$SQL_Condition .= " AND billing_buyer.idorder IN( $idorder )";
		$SQL_Condition2 .= " AND o.idorder IN( $idorder )";
		$SQL_Condition3 .= " AND o.idorder IN( $idorder )";	
	}
	
	if( !empty($idestimate)){
		$SQL_Condition5 .= " AND es.idestimate IN( $idestimate )";
		$SQL_Condition6 .= " AND es.idestimate IN( $idestimate )";
	}

	if ( !empty($idorder_supplier) ){
		$idorder_supplier = $idorder_supplier;
		$SQL_Condition .= " AND billing_buyer.idorder IN( SELECT idorder FROM order_supplier WHERE idorder_supplier=$idorder_supplier )";
	}

	if( !empty( $company ) ){
		$SQL_Condition  .= " AND (buyer.company LIKE '%$company%' OR contact.lastname LIKE '%$company%')";
		$SQL_Condition2 .= " AND (buyer.company LIKE '%$company%' OR contact.lastname LIKE '%$company%')";
		$SQL_Condition3 .= " AND (buyer.company LIKE '%$company%' OR contact.lastname LIKE '%$company%')";
		$SQL_Condition5 .= " AND (buyer.company LIKE '%$company%' OR contact.lastname LIKE '%$company%')";
		$SQL_Condition6 .= " AND (buyer.company LIKE '%$company%' OR contact.lastname LIKE '%$company%')";
		$SQL_Condition4 .= " AND (buyer.company LIKE '%$company%' OR contact.lastname LIKE '%$company%')";
	}	

	if( !empty( $total_amount_ht ) ){
		$SQL_Condition .= " AND ROUND( `billing_buyer`.total_amount_ht, 2 ) = '" . Util::text2num( $total_amount_ht ) . "'";
	}

	if( !empty( $total_amount ) ){
		$SQL_Condition .= " AND ROUND( `billing_buyer`.total_amount, 2 ) = '" . Util::text2num( $total_amount ) . "'";
	}

	if( !empty( $idpayment ) ){
		$SQL_Condition  .= " AND regulations_buyer.idpayment = $idpayment";
		
		$SQL_Condition2 .= " AND o.down_payment_idpayment = $idpayment";
		$SQL_Condition3 .= " AND o.payment_idpayment = $idpayment";
		
		$SQL_Condition5 .= " AND es.down_payment_idpayment = $idpayment";
		$SQL_Condition6 .= " AND es.payment_idpayment = $idpayment";
		
		$SQL_Condition4 .= " AND regulations_buyer.idpayment = $idpayment";
	}

	$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

	$totalFact = 0;
	$totalTTCFacts = 0;
	?>
	<script type="text/javascript" language="javascript">
	<!--
		
		function popupPartiallyPayment(irs){
			
			postop = (self.screen.height-300)/2;
			posleft = (self.screen.width-400)/2;
			
			window.open('register_partially_payment.php?irs='+irs, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=300,resizable,scrollbars=yes,status=0');
			
		}
		
		function Modify(idrb){
			
			postop = (self.screen.height-300)/2;
			posleft = (self.screen.width-400)/2;
			
			window.open('modify_buyer_payment.php?idrb='+idrb, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=360,resizable,scrollbars=yes,status=0');
			
		}
		
		function removeRow(idrb){
			var ilEstFouAflelou = confirm( "Estes vous sûr de vouloir supprimer ce règlement ? Ceci repassera de même les factures associées en attente s'il y en a" );

			if(ilEstFouAflelou){
				$.ajax({
					type: 'GET',
					url: '<?php echo $GLOBAL_START_URL ?>/accounting/regulations_buyer_search.php',
					data: 'deleteRegulation&idRegulation='+idrb,
					error: function(){ $.growUI('Une erreur est survenue lors de la suppression du règlement',''); },
					success: function( response ){
						if( response.length > 0 ){
							$.growlUI('Une erreur est survenue lors de la suppression du règlement<br/>'+response , '');
						}else{
							if( $('#regulationRow_'+idrb).parent('tbody').children().length == 1 ){
								$('#regulationRow_'+idrb).parent('tbody').parent('table').parent('div').prev('div').remove();
								$('#regulationRow_'+idrb).parent('tbody').parent('table').parent('div').remove();
							}else{
								$('#regulationRow_'+idrb).remove();
							}
							
							$.growlUI( 'Suppression effectuée avec succès' , '');
						}
					}
				});
			}
		
		}
	// -->
	</script>
	
<div class="mainContent fullWidthContent" id="container">
	<div class="topRight"></div><div class="topLeft"></div>
	<ul class="menu" style="margin-bottom:10px;">
		<?php
		if( $_POST[ 'documentSearch' ] == 'billing' || $_POST[ 'documentSearch' ] == 'all' ){ ?>
		<li class="item">
			<div class="right"></div><div class="left"></div>
			<div class="centered"><a id="facturesA" href="#factures">Règlements de factures</a></div>
		</li>
		<?php } 
		
		if( $_POST[ 'documentSearch' ] == 'order' || $_POST[ 'documentSearch' ] == 'all' ){ ?>
		<li class="item">
			<div class="right"></div><div class="left"></div>
			<div class="centered"><a id="down_paymentA" href="#down_payment">Acomptes sur commandes</a></div>
		</li>

		<li class="item">
			<div class="right"></div><div class="left"></div>
			<div class="centered"><a id="cash_paymentA" href="#cash_payment">Paiements comptants sur commandes </a></div>
		</li>
		<?php } 
		
		if( $_POST[ 'documentSearch' ] == 'estimate' || $_POST[ 'documentSearch' ] == 'all' ){ ?>
		<li class="item">
			<div class="right"></div><div class="left"></div>
			<div class="centered"><a id="down_paymentAEs" href="#down_paymentEs">Acomptes sur devis</a></div>
		</li>

		<li class="item">
			<div class="right"></div><div class="left"></div>
			<div class="centered"><a id="cash_paymentAEs" href="#cash_paymentEs">Paiements comptants sur devis </a></div>
		</li>
		<?php } ?>
			
	</ul>
	
	<form action="<?php echo $ScriptName ?>" method="post" name="invoicetopay" id="invoicetopay">
		<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
		<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
		<input type="hidden" name="search" value="1" />
		<?php
		if( $_POST[ 'documentSearch' ] == 'billing' || $_POST[ 'documentSearch' ] == 'all' ){ 
			
			
				$SQL_Condition	.= " AND billing_buyer.iduser = u1.iduser
				AND billing_buyer.idorder = o.idorder
				AND o.iduser = u2.iduser
				AND billing_buyer.idbuyer = buyer.idbuyer
				AND billing_regulations_buyer.idbilling_buyer = billing_buyer.idbilling_buyer
				AND contact.idcontact = billing_buyer.idcontact
				AND contact.idbuyer = billing_buyer.idbuyer
				AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer";
				
				$tables = "billing_buyer, user u1, user u2, buyer, billing_regulations_buyer, `order` o, contact, regulations_buyer";
				
				//nombre de pages
				
				$SQL_Query = "
				SELECT COUNT(*) AS resultCount 
				FROM $tables 
				WHERE $SQL_Condition";	
		
				$rs =& DBUtil::query( $SQL_Query );
				
				if( $rs === false || $rs->fields( "resultCount" ) == null )
					die( "Impossible de récupérer le nombre de pages" );
				
				$resultCount = $rs->fields( "resultCount" );
					
				$pageCount = ceil( $resultCount / $resultPerPage );
				
				//recherche
				
				$start = ( $page - 1 ) * $resultPerPage;
				$lang = User::getInstance()->getLang();
				
				$SQL_Query = "
				SELECT billing_buyer.idbilling_buyer, 
					billing_buyer.idbuyer,
					billing_buyer.DateHeure,
					billing_buyer.status,
					regulations_buyer.payment_date,
					regulations_buyer.deliv_payment,
					regulations_buyer.idpayment,
					regulations_buyer.idregulations_buyer,
					regulations_buyer.credit_balance_used,
					regulations_buyer.bank_date,
					regulations_buyer.overpayment,
					billing_regulations_buyer.amount as regAmount,
					buyer.company,
					buyer.idcustomer_profile,
					u1.initial as comInvoice,
					u2.initial as comOrder,			
					payment.name$lang AS payment,
					billing_buyer.idpayment,
					contact.lastname,
					contact.firstname
				FROM user u1, user u2, buyer, billing_regulations_buyer, billing_buyer, `order` o, contact, regulations_buyer
				LEFT JOIN payment
				ON payment.idpayment = regulations_buyer.idpayment
				WHERE $SQL_Condition
				GROUP BY regulations_buyer.idregulations_buyer
				ORDER BY buyer.company ASC, billing_buyer.idbilling_buyer ASC
				";
		 
				
				if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
					$SQL_Query .= "
					ORDER BY " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ;
					
				$rs =& DBUtil::query( $SQL_Query );
		
				
				$nbrResult = $rs->RecordCount();
		
				//export factor
					?>		
			            <div class="content" id="factures">
		
							<?
				if( $nbrResult == 0 ){
					
					?>
						        <div class="headTitle">
				                    <p class="title">Résultat de la recherche</p>
				                </div>
				                <div class="subContent">
				                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun règlement n'a été trouvée.</span>
				                </div>
		
					<?php	
					
					
				}else{
					
					?>
								<div class="headTitle">
			                    <p class="title">Résultat de la recherche: <?php echo $nbrResult ?> règlements(s)</p>
								<div class="rightContainer">
									<?php
									
									//export factor
									$use_factor_export = DBUtil::getParameterAdmin( "use_factor_export" );
									if( $use_factor_export ){
										
										include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
										include_once( "$GLOBAL_START_PATH/objects/Util.php" );
										
										global $GLOBAL_DB_PASS;
										$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $SQL_Query, $GLOBAL_DB_PASS ) );
									
									?>
										<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_buyer_payment.php?query=<?php echo $encryptedQuery ?>" class="blackText"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /> Export au format CSV</a>
									<?php
									}
									?>
								</div>
			                </div>
			                <div class="subContent">
								<?php
						
								$total_frs = 0;
								$fact = 0;
								
								$last_idbuyer = 0;
								for( $i = 0 ; $i < $nbrResult ; $i++ ){
			
									$status = $rs->fields("status");
									$idrb = $rs->fields("idregulations_buyer");
									$amount = DBUtil::query("SELECT amount FROM regulations_buyer WHERE idregulations_buyer = $idrb")->fields('amount');
									
									//$idbilling_buyer = $rs->fields("idbilling_buyer");
									$comInvoice =  $rs->fields( "comInvoice" );
									$comOrder =  $rs->fields( "comOrder" );
									$deliv_payment = $rs->fields("deliv_payment");
									$payment_date = $rs->fields("payment_date");
									$company = $rs->fields("company");
									$idbuyer = $rs->fields("idbuyer");
									$iscontact = DBUtil::getDBValue( "contact", "buyer", "idbuyer", $idbuyer );
									$rebateUsed = 0.00;
									$dateFacturation = Util::dateFormatEu( $rs->fields("DateHeure"), false );
									$billing_buyer="";
									$credBalUsed = $rs->fields("credit_balance_used");
									$overpayment = $rs->fields("overpayment");
									$billings = DBUtil::query(" SELECT brb.idbilling_buyer, bb.rebate_type, bb.rebate_value, bb.total_amount
																FROM billing_regulations_buyer brb, billing_buyer bb 
																WHERE brb.idregulations_buyer = $idrb
																AND brb.idbilling_buyer = bb.idbilling_buyer");
																
									while(!$billings->EOF()){
										$billing_buyer .= $billings->fields( 'idbilling_buyer' )." - ";
										
										if( $billings->fields( 'rebate_type' ) == 'amount' ){
											$rebateUsed += $billings->fields( 'rebate_value' );
										}else{
											$rebateUsed += ( $billings->fields( 'total_amount' ) * $billings->fields( 'rebate_value' ) ) / 100 ;
										}
										
										$billings->MoveNext();
									}
									$billing_buyer = substr($billing_buyer,0,strlen($billing_buyer)-3);
									
									if($iscontact)
										$type="contact";
									else
										$type="buyer";
									
																
									$idpayment = $rs->fields("idpayment");
									$payment_name = $rs->fields("payment");
											
									if( $deliv_payment == "0000-00-00" )
										$deliv_payment = "";
									else
										$deliv_payment = Util::dateFormatEu( $deliv_payment );
							
									if( $payment_date == "0000-00-00" )
										$payment_date = "";
									else
										$payment_date = Util::dateFormatEu( $payment_date );
									
							
									if( $last_idbuyer !== $idbuyer ){
										
										$last_idbuyer = $idbuyer;
										
										if( $i ){
											
											if( $fact > 1){
										
												?>
										 		<tr>
											 		<th colspan="10" style="border-style:none;"></th>
											 		<th class="totalAmount"><?php echo Util::priceFormat($total_frs);?></th>
										 		</tr>
										 		<?php
										 		
											} 
											
													?>
											 		</tbody>
						            			</table>
								            </div>
									 		<?php
											
											$total_frs = 0;
											$fact = 0;
											
										}
										
										$fullName = $rs->fields("lastname")." ".$rs->fields("firstname");
										$profile = DBUtil::getDBValue( "name", "customer_profile", "idcustomer_profile", $rs->fields("idcustomer_profile") );
										
										?>
								 		<a name="Anchor_<?php echo $idbuyer ?>"></a>
					                	<div class="subTitleContainer" style="margin-top:20px;">
					                        <p class="subTitle">
					                        	<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $type ?>&amp;key=<?php echo $idbuyer ?>" onclick="window.open(this.href);return false;">
					                        		<?php if($company!=""){ echo $company; }else{ echo $profile." : ".$fullName ;} ?>&nbsp;(<?php echo $idbuyer ?>)
					                        	</a>
					                        </p>
					                    </div>
					                    <!-- tableau résultats recherche -->
					                    <div class="resultTableContainer clear">
					                    	<table class="dataTable resultTable">
					                        	<thead>
					                                <tr>
					                                    <th style="width:20px;">Resp.</th>
					                                    <th colspan="2" style="width:80px;">Règlement n°</th>
					                                    <th>Factures n°</th>
					                                    <th style="width:100px;">Date de paiement</th>
					                                    <th style="width:100px;">Mode de paiement</th>
					                                    <th style="width:70px;">Escomptes</th>
					                                    <th style="width:70px;">Crédit utilisé</th>
					                                    <th style="width:70px;">Ecart règlement</th>
					                                    <th style="width:70px;">Trop perçu</th>
					                                    <th style="width:100px;">Total règlé</th>
					                                    <th style="width:20px;"></th>			                                
					                                </tr>
					                            </thead>
					                            <tbody>
					                            <?php
					                            
									} 
									
							?>
					         <tr class="blackText" id="regulationRow_<?=$idrb?>">
					        	<td class="lefterCol"><?php echo $comInvoice ?></td>
					        	<td style="text-align:right;width:50px;">
					        		<?php echo $idrb ?>
					        	</td>
					        	<td style="text-align:left;width:50px;">
									<span onmouseover="document.getElementById('pop<?=$idrb?>').className='tooltiphover';" onmouseout="document.getElementById('pop<?=$idrb?>').className='tooltip';" style="position:relative;float:left;"><img src="<?=$GLOBAL_START_URL?>/images/back_office/content/help.png" alt="voir le détail" class="tooltipa VCenteredWithText" />
										<span id='pop<?=$idrb?>' class='tooltip' style="width:500px;">
											<div class="content" style="padding:5px;">
												<div class="subContent">
													<div class="resultTableContainer">
														<?
														// la on met le détail des factures concernées
														$myfacts = DBUtil::query("SELECT billing_buyer.idbilling_buyer,
																						 billing_buyer.DateHeure,
																						 billing_buyer.idbuyer,
																						 billing_buyer.status,
																						 billing_buyer.deliv_payment,
																						 billing_buyer.total_amount,
																						 billing_regulations_buyer.rebate_amount,
																						 billing_regulations_buyer.amount as registeredAmount,
																						bank.bank_name,
																						rb.comment
																				  	FROM billing_buyer, billing_regulations_buyer, regulations_buyer rb
																					LEFT JOIN bank ON bank.idbank = rb.idbank
																				  	WHERE billing_buyer.idbilling_buyer = billing_regulations_buyer.idbilling_buyer
																					AND rb.idregulations_buyer = billing_regulations_buyer.idregulations_buyer
																				  	AND billing_regulations_buyer.idregulations_buyer = $idrb");
														
														?>
														
					                    				<table class="dataTable resultTable">
								                        	<thead>
								                                <tr>
								                                    <th>Facture</th>
								                                    <th>Date facture</th>
								                                    <th>Date d'échéance</th>
								                                    <th>Statut</th>
								                                    <th>Montant enregistré</th>
								                                    <th>Banque</th>
								                                    <th>Commentaires</th>
								                                </tr>
								                            </thead>
								                            <tbody>
								                            	<?
								                            	$totalAmount = 0;
								                            	$totalRebateJacob = 0;
								                            	
								                            	while(!$myfacts->EOF()){
								                            		$idFact = $myfacts->fields('idbilling_buyer');
								                            		$dateFact = Util::dateFormatEu($myfacts->fields('DateHeure'),false);
								                            		$dateEche = Util::dateFormatEu($myfacts->fields('deliv_payment'));
								                            		$statusFact = Dictionnary::translate( $myfacts->fields('status') );
								                            		
								                            		$totalAmount += $myfacts->fields('total_amount');
								                            		$totalRebateJacob += $myfacts->fields('rebate_amount');
								                            		
								                            		?>
								                            		<tr>
										                            	<td class="lefterCol"><?=$idFact?></td>
										                            	<td><?=$dateFact?></td>
										                            	<td><?=$dateEche?></td>
										                            	<td><?=$statusFact?></td>
										                            	<td><?php echo Util::priceFormat($myfacts->fields('registeredAmount')) ?></td>
										                            	<td><?php if( $myfacts->fields( "bank_name" ) ) echo htmlentities( $myfacts->fields( "bank_name" ) ); ?></td>
									                            		<td class="righterCol"><?php echo htmlentities( $myfacts->fields( "comment" ) ); ?></td>
									                            	</tr>
									                            	<?$myfacts->MoveNext()?>
								                            	<?php } ?>
								                            </tbody>
					                    				</table>
						                    		</div>
												</div>		
											</div>
										</span>
									</span>			        	
					        	</td>
					        	<td><?php echo $billing_buyer ?></td>
					        	<td><?php echo $payment_date ?></td>
					        	<td><?php echo $payment_name ?></td>
					        	<td><?php echo $rebateUsed>0?Util::priceFormat( $rebateUsed ):"-" ?></td>
					        	<td><?php echo $credBalUsed>0?Util::priceFormat( $credBalUsed ):"-" ?></td>
					        	<td><?php echo $overpayment!=0?Util::priceFormat( $overpayment ):"-" ?></td>
					        	<td><?
					        		$mytotal = $amount - $totalAmount - $totalRebateJacob - $credBalUsed - $overpayment;
					        		if(round($mytotal,2) > 0){
					        			echo Util::priceFormat( $mytotal );
					        		}else{
					        			echo '-';
					        		}
					        		?>
					        	</td>
					        	<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $amount ) ?></td>
								<td class="righterCol">
									<?if($rs->fields('bank_date') == '0000-00-00'){?>
										<img src="<?=$GLOBAL_START_URL?>/images/back_office/content/button_delete.png" onclick="removeRow(<?=$idrb?>);"/>
									<?php } ?>
								</td>
					        </tr>
							<?php
							
							$old_supplier_name = $company;
							$old_idbuyer = $idbuyer;
							$total_frs+=$amount;
							$totalTTCFacts+=$amount;
							$fact++;
							$totalFact++;
							$rs->MoveNext();			
							
						}
				
						if( $fact > 1 ){ ?>
							<tr>
								<th colspan="10" style="border-style:none;"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $total_frs ) ?></th>
							</tr>
							<?
						} ?>
				
					</tbody>
		    	</table>
		    </div>
			<?php
			}
			if( $nbrResult > 0 ){
			?>
			
		        <div class="resultTableContainer clear" style="margin-top:20px;float:right;">
		        	<table class="dataTable resultTable" style="width:400px">
		            	<thead>
		                    <tr>
		                    	<th>Nombre de règlements</th>
		                        <th>Total règlements</th>
		                    </tr>
		              	</thead>
		              	<tbody>
		              		<tr>
		              			<td class="lefterCol"><?php echo $totalFact ?></td>
		              			<td class="righterCol"><?php echo Util::priceFormat($totalTTCFacts) ?></td>
		              		</tr>
		              	</tbody>
		             </table>      
		    	</div>
		    	<div class="clear"></div>
		    	</div><?
			}?>
			</div>
		
		<?php } ?>
		
		<?php
		if( $_POST[ 'documentSearch' ] == 'all' || $_POST[ 'documentSearch' ] == 'order' ){ ?>
			
			<!-- ***************************** ACOMPTE **************************** -->
			<div class="content" id="down_payment">
				<?
					$rsDownP = DBUtil::query(	"SELECT bl.idorder,
														o.down_payment_idpayment,
														o.down_payment_comment,
														o.down_payment_date,
														o.idbuyer,
														down_payment_value,
														down_payment_type,
														total_amount,
														buyer.company,
														contact.firstname,
														contact.lastname
												FROM bl_delivery bl, `order` o, buyer, contact
												WHERE $SQL_Condition2 
												AND o.idorder = bl.idorder
												AND o.idbuyer = buyer.idbuyer
												AND o.idcontact = contact.idcontact
												AND contact.idbuyer = buyer.idbuyer
												AND bl.idbilling_buyer = 0
												AND o.down_payment_value > 0
												AND o.down_payment_idregulation > 0");
	
					$compteDP = $rsDownP->RecordCount();
					
					if( $compteDP > 0 && $rsDownP->fields("idorder") ){
						?>
						<div class="headTitle">
		                    <p class="title">Résultat de la recherche: <?php echo $rsDownP->RecordCount() ?> commande(s) avec acompte</p>
		                </div>
		                <div class="subContent">
					        <div class="resultTableContainer clear" style="margin-top:20px;">
					        	<table class="dataTable resultTable" >
					            	<thead>
					                    <tr>
					                    	<th>Client n°</th>
					                    	<th>Client</th>
					                    	<th>N° Commande</th>
					                        <th>Montant acompte TTC</th>
					                        <th>Mode de paiement</th>
					                        <th>Date</th>
					                        <th>TTC à payer</th>
					                        <th>Commentaire</th>
					                    </tr>
					              	</thead>
					              	<tbody>
						            <?
					                while(!$rsDownP->EOF()){ ?>
					                	<?
					                			$down_payment_value = $rsDownP->fields("down_payment_value");
												$dpamount = 0;
												
												if( $rsDownP->fields("down_payment_type") == "rate" )
													$dpamount = round( ( $rsDownP->fields("total_amount") * $down_payment_value ) / 100, 2 );
												else
													$dpamount = $down_payment_value;
												
												$idbuyerDP = $rsDownP->fields("idbuyer");	
												
												$iscontactDP = DBUtil::getDBValue( "contact", "buyer", "idbuyer", $idbuyerDP );
	
												if($iscontactDP)
													$typeDP="contact";
												else
													$typeDP="buyer";
													
												$paymentMode = DBUtil::getDBValue( "name_1", "payment", "idpayment", $rsDownP->fields("down_payment_idpayment") );	
												
												$companyDP = $rsDownP->fields("company");
												
												$fullNameDP = $rsDownP->fields("lastname")." ".$rsDownP->fields("firstname");
					                	?>
					              		<tr>
					              			<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $typeDP ?>&amp;key=<?php echo $idbuyerDP ?>" onclick="window.open(this.href);return false;"><?php echo $idbuyerDP ?></a></td>
					              			<td><?php if($companyDP!=""){ echo $companyDP; }else{ $fullNameDP; } ?></td>
					              			<td><a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $rsDownP->fields("idorder") ?>" onclick="window.open(this.href);return false;"><?php echo $rsDownP->fields("idorder") ?></a></td>
					              			<td><?php echo Util::priceFormat($dpamount) ?></td>
					              			<td><?php echo $paymentMode ?></td>
					              			<td><?php echo Util::dateFormatEu($rsDownP->fields("down_payment_date")) ?></td>
					              			<td><?php echo Util::priceFormat($rsDownP->fields("total_amount") - $dpamount) ?></td>
					              			<td class="righterCol"><?php echo stripslashes($rsDownP->fields("down_payment_comment")) ?></td>
					              			
		                				</tr>	
		                				<?php $rsDownP->MoveNext();
				                	}
					                ?>
					              	</tbody>
					             </table>      
					    	</div>
					    	<div class="clear"></div>
		                </div>
						<?
					}else{
						?>
				        <div class="headTitle">
		                    <p class="title">Résultat de la recherche</p>
		                </div>
		                <div class="subContent">
		                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucune commande avec acompte n'a été trouvée.</span>
		                </div>
						<?
					}
				?>
			
			</div>
				
			<!-- ***************************** PAIEMENT COMPTANT **************************** -->
			<div class="content" id="cash_payment">
				<?
					$rsCashP = DBUtil::query(	"SELECT bl.idorder,
														o.idbuyer,
														o.total_amount,
														o.total_amount_ht,
														buyer.company,
														o.payment_idpayment,
														o.balance_date,
														o.payment_comment,
														o.down_payment_type,
														o.down_payment_value,
														contact.firstname,
														contact.lastname
												FROM bl_delivery bl, `order` o, buyer, contact
												WHERE $SQL_Condition3 
												AND o.idorder = bl.idorder
												AND o.idbuyer = buyer.idbuyer
												AND o.idcontact = contact.idcontact
												AND contact.idbuyer = buyer.idbuyer
												AND bl.idbilling_buyer = 0
												AND o.cash_payment = 1 GROUP BY bl.idorder
												AND o.cash_payment_idregulation > 0");
	
					$compteCP = $rsCashP->RecordCount();
					if( $compteCP > 0 && $rsCashP->fields("idorder") ){
						?>
						<div class="headTitle">
		                    <p class="title">Résultat de la recherche: <?php echo $rsCashP->RecordCount() ?> commande(s) avec paiement comptant</p>
		                </div>
		                <div class="subContent">
					        <div class="resultTableContainer clear" style="margin-top:20px;">
					        	<table class="dataTable resultTable" >
					            	<thead>
					                    <tr>
					                    	<th>Client n°</th>
					                    	<th>Client</th>
					                    	<th>N° Commande</th>
					                        <th>Montant total HT</th>
					                        <th>Acompte TTC</th>
					                        <th>TTC à payer</th>
					                        <th>Mode de paiement</th>
					                        <th>Date</th>
					                        <th>Commentaire</th>
					                    </tr>
					              	</thead>
					              	<tbody>
						            <?
					                while(!$rsCashP->EOF()){ ?>
					                	<?							
					                	
					                			$down_payment_valueCP = $rsCashP->fields("down_payment_value");
												$dpamountCP = 0;
												
												if( $rsCashP->fields("down_payment_type") == "rate" )
													$dpamountCP = round( ( $rsCashP->fields("total_amount") * $down_payment_valueCP ) / 100, 2 );
												else
													$dpamountCP = $down_payment_valueCP;
													
													
												$idbuyerCP = $rsCashP->fields("idbuyer");	
												
												$iscontactCP = DBUtil::getDBValue( "contact", "buyer", "idbuyer", $idbuyerCP );
	
												if($iscontactCP)
													$typeCP="contact";
												else
													$typeCP="buyer";
													
												$paymentModeCp = DBUtil::getDBValue( "name_1", "payment", "idpayment", $rsCashP->fields("payment_idpayment") );	
												
												$companyCP = $rsCashP->fields("company");
												
												$cpamountht = $rsCashP->fields("total_amount_ht");	
												$cpamountttc = $rsCashP->fields("total_amount");	
												
												$fullNameCP = $rsCashP->fields("lastname")." ".$rsCashP->fields("firstname");
					                	?>
					              		<tr>
					              			<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $typeCP ?>&amp;key=<?php echo $idbuyerCP ?>" onclick="window.open(this.href);return false;"><?php echo $idbuyerCP ?></a></td>
					              			<td><?php if($companyCP!=""){ echo $companyCP; }else{ $fullNameCP; } ?></td>
					              			<td><a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $rsCashP->fields("idorder") ?>" onclick="window.open(this.href);return false;"><?php echo $rsCashP->fields("idorder") ?></a></td>
					              			<td><?php echo Util::priceFormat($cpamountht) ?></td>
					              			<td><?php echo Util::priceFormat($dpamountCP) ?></td>
					              			<td><?php echo Util::priceFormat($cpamountttc - $dpamountCP) ?></td>
					              			<td><?php echo $paymentModeCp ?></td>
					              			<td><?php echo Util::dateFormatEu($rsCashP->fields("balance_date")) ?></td>
					              			<td class="righterCol"><?php echo stripslashes($rsCashP->fields("payment_comment")) ?></td>
					              			
		                				</tr>	
		                				<?php $rsCashP->MoveNext();
				                	}
					                ?>
					              	</tbody>
					             </table>      
					    	</div>
					    	<div class="clear"></div>
		                </div>
						<?
					}else{
						?>
				        <div class="headTitle">
		                    <p class="title">Résultat de la recherche</p>
		                </div>
		                <div class="subContent">
		                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucune commande avec paiement comptant n'a été trouvée.</span>
		                </div>
						<?
					}
				?>
			
			</div>		
		<?php }
		
		if( $_POST[ 'documentSearch' ] == 'all' || $_POST[ 'documentSearch' ] == 'estimate' ){ ?>
			<!-- ***************************** ACOMPTE **************************** -->
			<div class="content" id="down_paymentEs">
				<?
					$rsDownP = DBUtil::query(	"SELECT es.idestimate,
														es.down_payment_idpayment,
														es.down_payment_comment,
														es.down_payment_date,
														es.idbuyer,
														down_payment_value,
														down_payment_type,
														total_amount,
														buyer.company,
														contact.firstname,
														contact.lastname
												FROM `estimate` es, buyer, contact
												WHERE $SQL_Condition5 
												AND es.idbuyer = buyer.idbuyer
												AND es.idcontact = contact.idcontact
												AND contact.idbuyer = buyer.idbuyer
												AND es.down_payment_value > 0
												AND es.down_payment_idregulation > 0");
	
					$compteDPes = $rsDownP->RecordCount();
					
					if( $compteDPes > 0 && $rsDownP->fields("idestimate") ){
						?>
						<div class="headTitle">
		                    <p class="title">Résultat de la recherche: <?php echo $rsDownP->RecordCount() ?> devis avec acompte</p>
		                </div>
		                <div class="subContent">
					        <div class="resultTableContainer clear" style="margin-top:20px;">
					        	<table class="dataTable resultTable" >
					            	<thead>
					                    <tr>
					                    	<th>Client n°</th>
					                    	<th>Client</th>
					                    	<th>N° Devis</th>
					                        <th>Montant acompte TTC</th>
					                        <th>Mode de paiement</th>
					                        <th>Date</th>
					                        <th>TTC à payer</th>
					                        <th>Commentaire</th>
					                    </tr>
					              	</thead>
					              	<tbody>
						            <?
					                while(!$rsDownP->EOF()){ ?>
					                	<?
					                			$down_payment_value = $rsDownP->fields("down_payment_value");
												$dpamount = 0;
												
												if( $rsDownP->fields("down_payment_type") == "rate" )
													$dpamount = round( ( $rsDownP->fields("total_amount") * $down_payment_value ) / 100, 2 );
												else
													$dpamount = $down_payment_value;
												
												$idbuyerDP = $rsDownP->fields("idbuyer");	
												
												$iscontactDP = DBUtil::getDBValue( "contact", "buyer", "idbuyer", $idbuyerDP );
	
												if($iscontactDP)
													$typeDP="contact";
												else
													$typeDP="buyer";
													
												$paymentMode = DBUtil::getDBValue( "name_1", "payment", "idpayment", $rsDownP->fields("down_payment_idpayment") );	
												
												$companyDP = $rsDownP->fields("company");
												
												$fullNameDP = $rsDownP->fields("lastname")." ".$rsDownP->fields("firstname");
					                	?>
					              		<tr>
					              			<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $typeDP ?>&amp;key=<?php echo $idbuyerDP ?>" onclick="window.open(this.href);return false;"><?php echo $idbuyerDP ?></a></td>
					              			<td><?php if($companyDP!=""){ echo $companyDP; }else{ $fullNameDP; } ?></td>
					              			<td><a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $rsDownP->fields("idestimate") ?>" onclick="window.open(this.href);return false;"><?php echo $rsDownP->fields("idestimate") ?></a></td>
					              			<td><?php echo Util::priceFormat($dpamount) ?></td>
					              			<td><?php echo $paymentMode ?></td>
					              			<td><?php echo Util::dateFormatEu($rsDownP->fields("down_payment_date")) ?></td>
					              			<td><?php echo Util::priceFormat($rsDownP->fields("total_amount") - $dpamount) ?></td>
					              			<td class="righterCol"><?php echo stripslashes($rsDownP->fields("down_payment_comment")) ?></td>
					              			
		                				</tr>	
		                				<?php $rsDownP->MoveNext();
				                	}
					                ?>
					              	</tbody>
					             </table>      
					    	</div>
					    	<div class="clear"></div>
		                </div>
						<?
					}else{
						?>
				        <div class="headTitle">
		                    <p class="title">Résultat de la recherche</p>
		                </div>
		                <div class="subContent">
		                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun devis avec acompte n'a été trouvée.</span>
		                </div>
						<?
					}
				?>
			
			</div>
				
			<!-- ***************************** PAIEMENT COMPTANT **************************** -->
			<div class="content" id="cash_paymentEs">
				<?
					$rsCashP = DBUtil::query(	"SELECT es.idestimate,
														es.idbuyer,
														es.total_amount,
														es.total_amount_ht,
														buyer.company,
														es.payment_idpayment,
														es.balance_date,
														es.payment_comment,
														es.down_payment_type,
														es.down_payment_value,
														contact.firstname,
														contact.lastname
												FROM `estimate` es, buyer, contact
												WHERE $SQL_Condition6 
												AND es.idbuyer = buyer.idbuyer
												AND es.idcontact = contact.idcontact
												AND contact.idbuyer = buyer.idbuyer
												AND es.cash_payment = 1
												AND es.cash_payment_idregulation > 0");
	
					$compteCPes = $rsCashP->RecordCount();
					if( $compteCPes > 0 && $rsCashP->fields("idestimate") ){
						?>
						<div class="headTitle">
		                    <p class="title">Résultat de la recherche: <?php echo $rsCashP->RecordCount() ?> devis avec paiement comptant</p>
		                </div>
		                <div class="subContent">
					        <div class="resultTableContainer clear" style="margin-top:20px;">
					        	<table class="dataTable resultTable" >
					            	<thead>
					                    <tr>
					                    	<th>Client n°</th>
					                    	<th>Client</th>
					                    	<th>N° Devis</th>
					                        <th>Montant total HT</th>
					                        <th>Acompte TTC</th>
					                        <th>TTC à payer</th>
					                        <th>Mode de paiement</th>
					                        <th>Date</th>
					                        <th>Commentaire</th>
					                    </tr>
					              	</thead>
					              	<tbody>
						            <?
					                while(!$rsCashP->EOF()){ ?>
					                	<?							
					                	
					                			$down_payment_valueCP = $rsCashP->fields("down_payment_value");
												$dpamountCP = 0;
												
												if( $rsCashP->fields("down_payment_type") == "rate" )
													$dpamountCP = round( ( $rsCashP->fields("total_amount") * $down_payment_valueCP ) / 100, 2 );
												else
													$dpamountCP = $down_payment_valueCP;
													
													
												$idbuyerCP = $rsCashP->fields("idbuyer");	
												
												$iscontactCP = DBUtil::getDBValue( "contact", "buyer", "idbuyer", $idbuyerCP );
	
												if($iscontactCP)
													$typeCP="contact";
												else
													$typeCP="buyer";
													
												$paymentModeCp = DBUtil::getDBValue( "name_1", "payment", "idpayment", $rsCashP->fields("payment_idpayment") );	
												
												$companyCP = $rsCashP->fields("company");
												
												$cpamountht = $rsCashP->fields("total_amount_ht");	
												$cpamountttc = $rsCashP->fields("total_amount");	
												
												$fullNameCP = $rsCashP->fields("lastname")." ".$rsCashP->fields("firstname");
					                	?>
					              		<tr>
					              			<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?type=<?php echo $typeCP ?>&amp;key=<?php echo $idbuyerCP ?>" onclick="window.open(this.href);return false;"><?php echo $idbuyerCP ?></a></td>
					              			<td><?php if($companyCP!=""){ echo $companyCP; }else{ $fullNameCP; } ?></td>
					              			<td><a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $rsCashP->fields("idestimate") ?>" onclick="window.open(this.href);return false;"><?php echo $rsCashP->fields("idestimate") ?></a></td>
					              			<td><?php echo Util::priceFormat($cpamountht) ?></td>
					              			<td><?php echo Util::priceFormat($dpamountCP) ?></td>
					              			<td><?php echo Util::priceFormat($cpamountttc - $dpamountCP) ?></td>
					              			<td><?php echo $paymentModeCp ?></td>
					              			<td><?php echo Util::dateFormatEu($rsCashP->fields("balance_date")) ?></td>
					              			<td class="righterCol"><?php echo stripslashes($rsCashP->fields("payment_comment")) ?></td>
					              			
		                				</tr>	
		                				<?php $rsCashP->MoveNext();
				                	}
					                ?>
					              	</tbody>
					             </table>      
					    	</div>
					    	<div class="clear"></div>
		                </div>
						<?
					}else{
						?>
				        <div class="headTitle">
		                    <p class="title">Résultat de la recherche</p>
		                </div>
		                <div class="subContent">
		                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun devis avec paiement comptant n'a été trouvée.</span>
		                </div>
						<?
					}
				?>
			
			</div>		
		<?php } ?>
	</form>

    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->
	
	<script type="text/javascript">
		$(document).ready(function() {
			
			$('#facturesA').append( ' (<?php echo $nbrResult ?>)' );
			
			$('#down_paymentA').append( ' (<?php echo $compteDP ?>)' );

			$('#cash_paymentA').append( ' (<?php echo $compteCP ?>)' );
			
			$('#down_paymentAEs').append( ' (<?php echo $compteDPes ?>)' );

			$('#cash_paymentAEs').append( ' (<?php echo $compteCPes ?>)' );

		});
	
		$(function() {
			
				$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
			
		});
		

		
	</script>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//------------------------------------------------------------------------------------------
function List_payment( $selectedValue = 0 ){
	
	$lang = "_1";

	echo "
	<select name=\"idpayment\" style=\"border: 1px solid #AAAAAA;\">\n
	<option value=\"0\">-</option>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idpayment, name_1 FROM payment ORDER BY name_1 ASC");
	
	if($rs->RecordCount()>0){
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			if( $selectedValue == $rs->Fields( "idpayment" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "idpayment" ) . "\"$selected>" . $rs->Fields( "name_1" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}
?>