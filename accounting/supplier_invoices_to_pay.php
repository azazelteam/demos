<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Factures fournisseurs à payer
 */
 

//------------------------------------------------------------------------------------------------
include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );

$Title = "Factures fournisseurs et proforma à payer";

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

$con = DBUtil::getConnection();
				
$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

$resultPerPage = 50000;
$maxSearchResults = 1000;

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}
?>

<?
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
?>

<div id="globalMainContent">
<?php

displayForm();

?>
</div><!--GlobalMainContent-->
<?php
	
include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "Date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier				= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$idpayment				= isset( $_POST[ "idpayment" ] ) ? $_POST[ "idpayment" ] : "";
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
	$total_amount			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$proforma_supplier		= isset( $_POST[ "proforma_supplier" ] ) ? $_POST[ "proforma_supplier" ] : "";
	
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	
	<script type="text/javascript" language="javascript">
	<!--
	
		function orderBy( order ){
	
			document.getElementById( 'searchForm' ).elements[ 'orderby' ].value = order;

		    $('#searchForm').ajaxSubmit({
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			}); 
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}

		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			
			$('#searchForm').ajaxForm( options );
						
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
		}
		
		function goForm(){
			var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};
		 
		    // bind to the form's submit event 
		    $('#searchForm').ajaxSubmit(options); 

			
				

		}		
		
		function resetTotalAmount(){
			
			$("#selectionAmount").val( 0 );
			$("#selectionAmountDiv").html( "0.00 ¤" );
			
			$(".amountRow").each(function(){
				if($(this).is(".activeAmountRow"))
					$(this).removeClass("activeAmountRow");
			});
			
			$(".cred_amountRow").each(function(){
				if($(this).is(".activeAmountRow"))
					$(this).removeClass("activeAmountRow");
			});
		}
		
	// -->
	</script>
	
	<style type="text/css">
	<!--
		
		.fixme {
			/* Netscape 4, IE 4.x-5.0/Win and other lesser browsers will use this */
		  position: absolute;
		  right: 10px;
		  top: 135px;
		}
		body > div#global > div#globalMainContent > div.fixme {
		  /* used by Opera 5+, Netscape6+/Mozilla, Konqueror, Safari, OmniWeb 4.5+, iCab, ICEbrowser */
		  position: fixed;
		}
		
		tr.activeAmountRow td { background-color:#E0E0E0; }
		
	-->
	</style>
	<!--[if gte IE 5.5]>
	<![if lt IE 7]>
	<style type="text/css">
	
		div.fixme {
		  /* IE5.5+/Win - this is more specific than the IE 5.0 version */
		  right: expression( ( 10 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
		  top: expression( ( 135 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
		}
		
	</style>
	<![endif]>
	<![endif]-->
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css" />
	<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
    	<div class="mainContent noprint">
        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
            <div class="topRight"></div><div class="topLeft"></div>
        	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/supplier_invoices_to_pay.php" method="post" name="searchForm" id="searchForm">
        		<input type="hidden" name="orderby" id="orderby" value="" />
	            <div class="content">
	            	<div class="headTitle">
	                    <p class="title">Recherche de factures et proforma fournisseur</p>
	                    <div class="rightContainer">
	                    	<input type="radio" name="date_type" onchange="document.getElementById('sinceText').innerHTML = this.value == 'Date' ? 'Depuis' : 'Dans';" class="VCenteredWithText" value="Date"<?php if( $date_type == 'Date') echo " checked=\"checked\""; ?> />Date de facture
	                        <input type="radio" name="date_type" onchange="document.getElementById('sinceText').innerHTML = this.value == 'Date' ? 'Depuis' : 'Dans';" class="VCenteredWithText" value="deliv_payment"<?php if( $date_type == 'deliv_payment') echo " checked=\"checked\""; ?> />Date d'échéance
	                    </div>
	                </div>
	                <div class="subContent">
						<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	  					<input type="hidden" name="search" value="1" />
                		<!-- tableau choix de date -->
                        <div class="tableContainer">
                            <table class="dataTable dateChoiceTable">
                                <tr>
                                    <td><input type="radio" name="interval_select" id="interval_select_since" class="VCenteredWithText" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> /><span id="sinceText">Dans</span></td>
                                    <td>
                                        <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							            	<option value="0" <?php  if($minus_date_select==0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ;  ?></option>
							              	<option value="-1" <?php  if($minus_date_select==-1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ;  ?></option>
						              		<option value="-2" <?php  if($minus_date_select==-2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ;  ?></option>
							              	<option value="-7" <?php  if($minus_date_select==-7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ; ?></option>
							              	<option value="-14" <?php  if($minus_date_select==-14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ; ?></option>
							              	<option value="-30" <?php  if($minus_date_select==-30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ; ?></option>
							              	<option value="-60" <?php  if($minus_date_select==-60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ; ?></option>
							              	<option value="-90" <?php  if($minus_date_select==-90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ; ?></option>
							              	<option value="-180" <?php  if($minus_date_select==-180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ; ?></option>
							              	<option value="-365" <?php  if($minus_date_select==-365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ; ?></option>
							              	<option value="-730"<?php if( $minus_date_select == -730 ) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
							              	<option value="-1825"<?php if( $minus_date_select == -1825 ) echo " selected=\"selected\""; ?>>Moins de 5 ans</option>
							            </select>
                                    </td>
                                    <td><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> />Pour le mois de</td>
                                    <td>
                                        <?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                    </td>
                                </tr>
                                <tr>	
                                    <td>
                                        <input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> />Entre le
                                    </td>
                                    <td>
										<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
						 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                    </td>
                                    <td>et le</td>
                                    <td>
						 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4" class="tableSeparator"></td>
                                </tr>
                                <tr class="filledRow">
                                    <th>Facture n°</th>
                                    <td><input type="text" name="billing_supplier" value="<?php echo $billing_supplier ?>" class="textInput" /></td>
                                    <th>Fournisseur</th>
                                    <td><?php DrawSupplierList( $idsupplier ); ?></td>
                                </tr>
                                <tr class="filledRow">
                                	<th>Proforma n°</th>
                                	<td><input type="text" name="proforma_supplier" value="<?php echo $proforma_supplier ?>" class="textInput" /></td>
                                    <th>Cde fourn. n°</th>
                                    <td><input type="text" name="idorder_supplier" value="<?php echo $idorder_supplier ?>" class="textInput" /></td>
                                </tr>
                                 <tr class="filledRow">
                                    <th>Montant TTC</th>
                                    <td><input type="text" name="total_amount" value="<?php echo $total_amount ?>" class="textInput" /></td>
									<th></th>
									<td></td>
                                </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton noprint" value="Rechercher" />
                        </div>
	                </div>
	            </div>
            </form>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools noprint">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
        <div id="SearchResults"></div>
		<div id="tools" class="fixme noprint" style="width:130px;">
			<div class="toolBox">
				<div class="header">
					<div class="topRight"></div><div class="topLeft"></div>
					<p class="title">Montant sélection</p>
				</div>
				<input type="hidden" id="selectionAmount" value="0" />
				<div class="content">
					<div id="selectionAmountDiv" style="font-size:14px; font-weight:bold; padding-top:5px; text-align:center;">0.00 ¤</div>
					<input type="button" value="Effacer" onclick="resetTotalAmount();" class="blueButton noprint" style="display:block; margin:auto; margin-top:3px;" />
				</div>
				<div class="footer">
					<div class="bottomLeft"></div>
					<div class="bottomMiddle" style="width:106px;"></div>
					<div class="bottomRight"></div>
				</div>
			</div>
		</div>
	<?php 
}


/////-----------------------

function search(){

	global $resultPerPage,$hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH, $ScriptName;
	
	$now = getdate();
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "deliv_payment";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
	$total_amount			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$proforma_supplier		= isset( $_POST[ "proforma_supplier" ] ) ? $_POST[ "proforma_supplier" ] : "";
		
	switch ($interval_select){
			    
		case 1:
			
			if( $date_type == "deliv_payment" ){
				
				$Now_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] - $minus_date_select ,$now["year"])) ;
				$Min_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
				
			}else{
				
				$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
				$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
				
			}
				
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
		if( !$hasAdminPrivileges )
			$SQL_Condition .= " AND `billing_supplier`.iduser = '$iduser'";
							
		if ( !empty($idsupplier) ){
				$idsupplier = $idsupplier;
				$SQL_Condition .= " AND billing_supplier.idsupplier = $idsupplier";
		}
		
		if ( !empty($billing_supplier) ){
				$SQL_Condition .= " AND billing_supplier.billing_supplier LIKE '%$billing_supplier%'";
		}
		
		if ( !empty($total_amount) ){
				$SQL_Condition .= " AND billing_supplier.total_amount = $total_amount";
		}
		
		if ( !empty($idorder_supplier) ){
				$SQL_Condition .= " AND bl_delivery.idorder_supplier = $idorder_supplier";
		}
		
		if( !empty($proforma_supplier) ){
			$SQL_Condition .= " AND ( billing_supplier.billing_supplier LIKE '%$proforma_supplier%' )";
		}
		
		$con = DBUtil::getConnection();

		$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

?>
<script type="text/javascript" language="javascript">
<!--
	
	function goToPage( pageNumber ){
	
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function popupPartiallyPayment(irs)
	{
		postop = (self.screen.height-300)/2;
		posleft = (self.screen.width-400)/2;

		window.open('register_partially_payment.php?irs='+irs, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=300,resizable,scrollbars=yes,status=0');
	
	}
	
	function registerPayment(idsupplier,proforma){
		
		payhrefPro="";
		
		if(proforma==true){
			payhrefPro = "proforma&";
			var checkboxes = $('#invoicetopayPro' ).find( '#group_'+idsupplier );
			var checkboxesCredit = $('#invoicetopayPro' ).find('#credgroup_'+idsupplier );
		}else{
			var checkboxes = $('#invoicetopay' ).find( '#group_'+idsupplier );
			var checkboxesCredit = $('#invoicetopay' ).find('#credgroup_'+idsupplier );
		}
	
		var checked = false;
		
		var paymenthref = 'register_supplier_payment.php?'+payhrefPro+'supplier='+idsupplier+'&billings=';
		
		checkboxes.each( function(){ 
		
			if( $(this).attr('checked')==true ){
				
				paymenthref = paymenthref+$(this).val()+',';
				
				checked = true;
			}

		});
		
		var lg = paymenthref.length;
		paymenthref = paymenthref.substring(0,lg-1);
			
		paymenthref = paymenthref + '&credits=';
		
		checkboxesCredit.each( function() { 
		
			if( $(this).attr('checked')==true ){
				
				paymenthref = paymenthref+$(this).val()+',';
				
			}

		});

		var lg2 = paymenthref.length;
		paymenthref = paymenthref.substring(0,lg2-1);
			
				
		if( !checked ){
			alert( 'Aucune facture / proforma sélectionnée !' );
		}else{
			
			postop = (self.screen.height-300)/2;
			posleft = (self.screen.width-400)/2;

			window.open(paymenthref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=950,height=500,resizable,scrollbars=yes,status=0');
		}			
	}
	
	
	
// -->
</script>

<script type="text/javascript">
/* <![CDATA[ */
	
	/* ----------------------------------------------------------------------------------- */
	
	$(document).ready(function() {
		
		$(".amountRow").click(function(){
			
			var value = $(this).attr("id").substring(13);
			var totalAmount = $("#selectionAmount").val();
			
			if($(this).is(".activeAmountRow")){
				
				$(this).removeClass("activeAmountRow");
				
				totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
				
			}
			else{
				
				$(this).addClass("activeAmountRow");
				
				totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
				
			}
			
			$("#selectionAmount").val(totalAmount);
			$("#selectionAmountDiv").html(totalAmount + " ¤");
			
		});
		
		$(".cred_amountRow").click(function(){
			
			var value = $(this).attr("id").substring(18);
			var totalAmount = $("#selectionAmount").val();
			
			if($(this).is(".activeAmountRow")){
				
				$(this).removeClass("activeAmountRow");
				
				totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
				
			}
			else{
				
				$(this).addClass("activeAmountRow");
				
				totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
				
			}
			
			$("#selectionAmount").val(totalAmount);
			$("#selectionAmountDiv").html(totalAmount + " ¤");
			
		});
		
	});
	
	/* ----------------------------------------------------------------------------------- */
	
/* ]]> */
</script>


	<?php

	$SQL_Condition	.= "
	AND billing_supplier.iduser = user.iduser
	AND billing_supplier.status='StandBy'
	AND billing_supplier.proforma = 0
	AND billing_supplier.idsupplier = supplier.idsupplier
	AND billing_supplier.editable = 0";
		
	//recherche
	$SQL_Query = "
	SELECT billing_supplier.billing_supplier, 
		billing_supplier.idsupplier,
		billing_supplier.status,
		supplier.name,
		billing_supplier.deliv_payment, 
		billing_supplier.idpayment,
		payment.name_1 AS payment_name,
		user.initial,
		billing_supplier.total_amount,
		billing_supplier.Date,
		billing_supplier.billing_supplier_doc
	FROM user, supplier, bl_delivery
	RIGHT JOIN billing_supplier_row bsr ON( bsr.idbl_delivery = bl_delivery.idbl_delivery AND bsr.idsupplier = bl_delivery.idsupplier )
	RIGHT JOIN billing_supplier ON( billing_supplier.billing_supplier = bsr.billing_supplier AND billing_supplier.idsupplier = bsr.idsupplier )
	LEFT JOIN payment ON payment.idpayment = billing_supplier.idpayment
	WHERE $SQL_Condition
	GROUP BY billing_supplier.billing_supplier";

	if( isset( $_REQUEST[ "orderby" ] ) && strlen( $_REQUEST[ "orderby" ] ) ){
	$SQL_Query .= "
		ORDER BY supplier.name ASC, " . Util::html_escape( $_REQUEST[ "orderby" ] );
	}else{ $SQL_Query .= " ORDER BY supplier.name ASC, billing_supplier.billing_supplier ASC";}
	
	$rs = DButil::query($SQL_Query);
	$old_supplier_name = "";
	$old_idsupplier = 0;

	$resultCount = $rs->RecordCount();

	$totalToPay = 0.00;	

	$SQL_Query_Proforma = str_replace("billing_supplier.proforma = 0","billing_supplier.proforma = 1",$SQL_Query);
	$rsPro = DButil::query($SQL_Query_Proforma);
	$resultCountProforma = $rsPro->RecordCount();

	?>

	<div class="mainContent fullWidthContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    	<div class="topRight"></div><div class="topLeft"></div>
        <div id="zizi">
			<ul class="menu noprint" style="margin-bottom:10px;">
				<li class="item">
					<div class="right"></div><div class="left"></div>
					<div class="centered"><a id="facture" href="#factures">Factures (<?=$resultCount?>)</a></div>
				</li>
				<li class="item">
					<div class="right"></div><div class="left"></div>
					<div class="centered"><a id="proform" href="#proforma">Factures proforma (<?=$resultCountProforma?>)</a></div>
				</li>
			</ul>
			
            <div class="content" id="factures">
				<form action="<?php echo $ScriptName ?>" method="post" name="invoicetopay" id="invoicetopay">
					<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
					<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
					<input type="hidden" name="search" value="1" />
            	<div class="headTitle">
                    <p class="title" id='titleBillings'>
                    	<?php if( $resultCount == 0 ){ ?>
                    		<span style="font-size: 14px;font-weight: bold; color: #008000;">Aucune facture fournisseur à payer !</span>
                    	<?php }else{ ?>
                    		Résultats de la recherche: <?php echo $resultCount ?> facture(s) fournisseurs à payer - Total TTC à payer : 
                    	<?php } ?>
                    </p>
                </div>
                <div class="subContent">
                	<div class="tableHeaderLeft" style="margin-bottom:5px;"></div>
						<?php
						
						if( $resultCount > 0 ){
							
							$total_frs = 0;
							$fact = 0;
							
							for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
								
								$billing_supplier = $rs->Fields("billing_supplier");
								$initials =  $rs->fields( "initial" );
								$date_facture = $rs->fields("Date");
								$deliv_payment = $rs->fields("deliv_payment");
								$supplier_name = $rs->fields("name");
								$idsupplier = $rs->fields("idsupplier");
								$idpayment = $rs->fields("idpayment");
								$payment_name = $rs->fields("payment_name");
								$total_amount = $rs->fields("total_amount");
								$status = $rs->fields("status");
								
								$soldeToPay = SupplierInvoice::getBalanceOutstanding( $billing_supplier, $idsupplier );
								
								$totalToPay += $soldeToPay;
								
								if( $deliv_payment == "0000-00-00" )
									$deliv_payment = "";
								else
									$deliv_payment = usDate2eu( $deliv_payment );
								
								if( $date_facture == "0000-00-00" )
									$date_facture = "";
								else
									$date_facture = usDate2eu( $date_facture );
									
									
								if( $old_supplier_name!=$supplier_name ){
									
									if($i){

										?></tbody><?
										if($fact>1){?>
											<tfoot>
									 		<tr>
										 		<td colspan="6" style="border:none;"></td>
										 		<td class="lefterCol righterCol" style="font-weight:bold; text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_frs ) ?></td>
										 		<td colspan="2" style="border:none;"></td>
									 		</tr>
									 		</tfoot>
			 							<?php } ?>
			 									
			 								</table>
			 							</div>
							            <div class="submitButtonContainer">
							            	<input type="button" value="Enregistrer le paiement" onclick="registerPayment(<?=$old_idsupplier;?>,false);" class="blueButton noprint" />
							            </div>
			 							<?
										$total_frs = 0;
										$fact = 0;
									}
																
									?>
						 			<a name="Anchor_<?php echo $idsupplier ?>"></a>
				                	<div class="subTitleContainer">
				                        <p class="subTitle"><?php echo $supplier_name ?></p>
				                    </div>
				                	<!-- tableau résultats recherche -->
				                    <div class="tableContainer clear">
				                    	<table class="dataTable resultTable tablesorter">
				                        	<thead>
									 			<tr>
									         		<th style="width:60px;vertical-align:middle;">Com.</th>
									         		<th style="width:100px;vertical-align:middle;">Facture / Avoir</th>
									         		<th style="width:120px;vertical-align:middle;">Cde fourn. n°</th>
									         		<th style="width:120px;vertical-align:middle;">Statut</th>
									         		<th style="width:100px;vertical-align:middle;">Date<br />facture / Avoir</th>
									         		<th style="width:100px;vertical-align:middle;">Date<br />d'échéance</th>
													<th style="width:100px;vertical-align:middle;">Mode de paiement</th>
										        	<th style="width:80px;vertical-align:middle;">Total TTC</th>
										        	<th style="width:80px;vertical-align:middle;">Solde TTC à payer</th>
									        		<th class="noprint" style="width:80px;vertical-align:middle;">Enregistrer / Regrouper</th>
								        		</tr>
							        		</thead>
							        		<tbody>
						        <?php } ?>
						        <tr class="amountRow" id="rowForAmount_<?php echo $soldeToPay ?>">
						        	<td class="lefterCol"><?php echo $initials ?></td> 
						        	<td><?
						        	
									if( $rs->fields( 'billing_supplier_doc' ) && file_exists( DOCUMENT_ROOT . '/data/billing_supplier/' . $rs->fields( 'billing_supplier_doc' ) ) )
										echo "<a href='".HTTP_HOST."/data/billing_supplier/".$rs->fields( "billing_supplier_doc" )."' onclick='window.open(this.href);return false;'>".$billing_supplier."</a>";
									else
										echo $billing_supplier;
						        	
						        	?></td>
						        	<td><?php displayOs( $billing_supplier ) ?></td>
						        	<td><?php echo Dictionnary::translate( $status ) ?></td>	
									<td><?php echo $date_facture ?></td>
									<td><?php echo $deliv_payment ?></td>
									<td><?php echo $payment_name ?></td>									
									<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
									<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $soldeToPay ) ?></td>
									<td class="righterCol noprint"><input type="checkbox" name="group_<?=$idsupplier ?>[]" id="group_<?=$idsupplier ?>" value="<?php echo urlencode($billing_supplier) ?>" /></td>
						        </tr>
								<?php
								
								if($old_supplier_name!=$supplier_name){
								// avoirs ouverts
								$credits = DBUtil::query("	SELECT u.initial,cs.credit_supplier_doc,cs.idcredit_supplier,billing_supplier, Date, cs.amount, ( cs.amount - 
															ifnull(
																(SELECT ifnull( SUM(csr.amount) , COUNT(csr.idcredit_supplier) ) 
																FROM credit_supplier_regulation csr 
																WHERE csr.idcredit_supplier = cs.idcredit_supplier 
																GROUP BY csr.idcredit_supplier)
																,0.00
																) 
															) as dispoAmount
														FROM credit_supplier cs, user u
														WHERE cs.idsupplier = $idsupplier
														AND cs.iduser = u.iduser
														HAVING dispoAmount > 0
														ORDER BY cs.idcredit_supplier ASC");									
								
								while(!$credits->EOF()){
									
									$initials = $credits->fields('initial');
									$credit_supplier = $credits->fields('idcredit_supplier');
									$date = $credits->fields('Date');
									$total_amount = $credits->fields('amount');
									$solde_credit = $credits->fields('dispoAmount');
									$total_frs -= $total_amount;
									?>
									<tr style="color:red;" class="cred_amountRow" id="cred_rowForAmount_<?php echo $solde_credit ?>">
							        	<td class="lefterCol"><?php echo $initials ?></td> 
							        	<td><?
											if( $credits->fields( 'credit_supplier_doc' ) && file_exists( DOCUMENT_ROOT . '/data/credit_supplier/' . $credits->fields( 'credit_supplier_doc' ) ) )
												echo "<a href='".HTTP_HOST."/data/credit_supplier/".$credits->fields( "credit_supplier_doc" )."' onclick='window.open(this.href);return false;'>".$credit_supplier."</a>";
											else
												echo $credit_supplier;
							        		?>
							        	</td>
							        	<td></td>
							        	<td></td>	
										<td><?php echo usDate2eu($date) ?></td>
										<td></td>
										<td></td>
										<td style="text-align:right; white-space:nowrap;">- <?php echo Util::priceFormat( $total_amount ) ?></td>
										<td class="righterCol" style="text-align:right; white-space:nowrap;">- <?php echo Util::priceFormat( $solde_credit ) ?></td>
										<td class="righterCol noprint"><input type="checkbox" name="credgroup_<?=$idsupplier ?>[]" id="credgroup_<?=$idsupplier ?>" value="<?php echo urlencode($credit_supplier) ?>" /></td>
									</tr>
									<?
									$credits->MoveNext();
								}
								$old_supplier_name = $supplier_name;
								$old_idsupplier = $idsupplier;
								}
								$total_frs += $total_amount;
								$fact++;
								
								$rs->MoveNext();
								
							}

							?></tbody><?
							if( $fact > 1 ){
								
								?>
								<tfoot>
								<tr>
									<th colspan="7" style="border-style:none;"></th>
									<th class="totalAmount"><?php echo Util::priceFormat( $total_frs ) ?></th>
									<th colspan="1" style="border-style:none;"></th>
								</tr>
								</tfoot>
							<?php } ?>
									
								</table>
							</div>
						    <div class="submitButtonContainer">
						    	<input type="button" value="Enregistrer le paiement" onclick="registerPayment(<?php echo $idsupplier ?>,false);" class="blueButton noprint" />
						    </div>
							Total à payer : <?php echo Util::priceFormat( $totalToPay ) ?>

							<script type="text/javascript">
								$(document).ready(function() {
									$('#titleBillings').html( $('#titleBillings').html() + " <?php echo Util::priceFormat( $totalToPay ) ?> " );
								});
							</script>

						<?
						}?>
                </div>
                </form>
       		</div>
       		<!-- FACTURES PROFORMA-->
 			<?
	
			
			$old_supplier_name = "";
			$old_idsupplier = 0;
		
			$totalToPay = 0.00;	
			
 ?>      				
            <div class="content" id="proforma">
            	<div class="headTitle">
                    <p class="title" id='titleProforma'>
                    	<?php if( $resultCount == 0 ){ ?>
                    		<span style="font-size: 14px;font-weight: bold; color: #008000;">Aucune facture proforma fournisseur à payer !</span>
                    	<?php }else{ ?>
                    		Résultats de la recherche: <?php echo $resultCountProforma ?> facture(s) proforma fournisseurs à payer - Total TTC à payer : 
                    	<?php } ?>
                    </p>
                </div>
                <div class="subContent">
					<form action="<?php echo $ScriptName ?>" method="post" name="invoicetopayPro" id="invoicetopayPro">
						<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
						<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
						<input type="hidden" name="search" value="1" />
                	<div class="tableHeaderLeft" style="margin-bottom:5px;"></div>
					<?php
					
					if( $resultCountProforma > 0 ){
						
						$total_frs = 0;
						$fact = 0;
						
						for( $i = 0 ; $i < $rsPro->RecordCount() ; $i++ ){
							
							$billing_supplier = $rsPro->Fields("billing_supplier");
							$initials =  $rsPro->fields( "initial" );
							$date_facture = $rsPro->fields("Date");
							$deliv_payment = $rsPro->fields("deliv_payment");
							$supplier_name = $rsPro->fields("name");
							$idsupplier = $rsPro->fields("idsupplier");
							$total_amount = $rsPro->fields("total_amount");
							$status = $rsPro->fields("status");
							
							$soldeToPay = SupplierInvoice::getBalanceOutstanding( $billing_supplier, $idsupplier );
							
							$totalToPay += $soldeToPay;
							
							if( $deliv_payment == "0000-00-00" )
								$deliv_payment = "";
							else
								$deliv_payment = usDate2eu( $deliv_payment );
							
							if( $date_facture == "0000-00-00" )
								$date_facture = "";
							else
								$date_facture = usDate2eu( $date_facture );
					
					
							if( $old_supplier_name!=$supplier_name ){
								if($i){
									?></tbody><?
									if($fact>1){?>
										<tfoot>
								 		<tr>
									 		<td colspan="6" style="border:none;"></td>
									 		<td class="lefterCol righterCol" style="font-weight:bold; text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_frs ) ?></td>
									 		<td colspan="2" style="border:none;"></td>
								 		</tr>
								 		</tfoot>
					 				<?php } ?>
							 				
							 			</table>
							 		</div>
						            <div class="submitButtonContainer">
						            	<input type="button" value="Enregistrer le paiement" onclick="registerPayment(<?=$old_idsupplier;?>,true);" class="blueButton noprint" />
						            </div>
						 		<?
									$total_frs = 0;
									$fact = 0;
								}?>
					 			<a name="Anchor_<?php echo $idsupplier ?>"></a>
			                	<div class="subTitleContainer">
			                        <p class="subTitle"><?php echo $supplier_name ?></p>
			                    </div>
			                    
			                	<!-- tableau résultats recherche -->
			                    <div class="tableContainer clear">
			                    	<table class="dataTable resultTable tablesorter">
			                        	<thead>
								 			<tr>
								         		<th style="width:60px;vertical-align:middle;">Com.</th>
								         		<th style="width:100px;vertical-align:middle;">Proforma</th>
								         		<th style="width:120px;vertical-align:middle;">Cde fourn. n°</th>
								         		<th style="width:120px;vertical-align:middle;">Statut</th>
								         		<th style="width:100px;vertical-align:middle;">Date<br />facture / Avoir</th>
								         		<th style="width:100px;vertical-align:middle;">Date<br />d'échéance</th>
									        	<th style="width:80px;vertical-align:middle;">Total TTC</th>
									        	<th style="width:80px;vertical-align:middle;">Solde TTC à payer </th>
								        		<th class="noprint" style="width:80px;vertical-align:middle;">Enregistrer / Regrouper</th>
							        		</tr>
						        		</thead>
						        		<tbody>
		        		<?php } ?>
				        <tr class="amountRow" id="rowForAmount_<?php echo $soldeToPay ?>">
				        	<td class="lefterCol"><?php echo $initials ?></td> 
				        	<td><?php echo $billing_supplier ?></td>
				        	<td><?php displayOs( $billing_supplier ) ?></td>
				        	<td><?php echo Dictionnary::translate( $status ) ?></td>	
							<td><?php echo $date_facture ?></td>
							<td><?php echo $deliv_payment ?></td>
							<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
							<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $soldeToPay ) ?></td>
							<td class="righterCol noprint"><input type="checkbox" name="group_<?=$idsupplier ?>[]" id="group_<?=$idsupplier ?>" value="<?php echo urlencode($billing_supplier) ?>" /></td>
				        </tr>
						<?php
						
								if($old_supplier_name!=$supplier_name){
								/* avoirs ouverts
								$credits = DBUtil::query("	SELECT u.initial,cs.idcredit_supplier,billing_supplier, Date, cs.amount, ( cs.amount - 
															ifnull(
																(SELECT ifnull( SUM(csr.amount) , COUNT(csr.idcredit_supplier) ) 
																FROM credit_supplier_regulation csr 
																WHERE csr.idcredit_supplier = cs.idcredit_supplier 
																GROUP BY csr.idcredit_supplier)
																,0.00
																) 
															) as dispoAmount
														FROM credit_supplier cs, user u
														WHERE cs.idsupplier = $idsupplier
														AND cs.iduser = u.iduser
														HAVING dispoAmount > 0
														ORDER BY cs.idcredit_supplier ASC");									
								
								while(!$credits->EOF()){
									
									$initials = $credits->fields('initial');
									$credit_supplier = $credits->fields('idcredit_supplier');
									$date = $credits->fields('Date');
									$total_amount = $credits->fields('amount');
									$solde_credit = $credits->fields('dispoAmount');
									$total_frs -= $total_amount;
									?>
									<tr style="color:red;" class="cred_amountRow" id="cred_rowForAmount_<?php echo $solde_credit ?>">
							        	<td class="lefterCol"><?php echo $initials ?></td> 
							        	<td><?php echo $credit_supplier ?></td>
							        	<td></td>
							        	<td></td>	
										<td><?php echo usDate2eu($date) ?></td>
										<td></td>
										<td style="text-align:right; white-space:nowrap;">- <?php echo Util::priceFormat( $total_amount ) ?></td>
										<td class="righterCol" style="text-align:right; white-space:nowrap;">- <?php echo Util::priceFormat( $solde_credit ) ?></td>
										<td class="righterCol"><input type="checkbox" name="credgroup_<?=$idsupplier ?>[]" id="credgroup_<?=$idsupplier ?>" value="<?php echo urlencode($credit_supplier) ?>" /></td>
									</tr>
									<?
									$credits->MoveNext();
								}*/
								$old_supplier_name = $supplier_name;
								$old_idsupplier = $idsupplier;
								}
						$total_frs += $soldeToPay;
						$fact++;
						
						$rsPro->MoveNext();
						
					}
			
					?></tbody><?
					if( $fact > 1 ){?>
						<tfoot>
						<tr>
							<th colspan="8" style="border-style:none;"></th>
							<th class="totalAmount"><?php echo Util::priceFormat( $total_frs ) ?></th>
							<th colspan="1" style="border-style:none;"></th>
						</tr>
						</tfoot>
					<?php } ?>
							
						</table>
					</div>
				    <div class="submitButtonContainer">
				    	<input type="button" value="Enregistrer le paiement" onclick="registerPayment(<?php echo $idsupplier ?>,true);" class="blueButton noprint" />
				    </div>
					Total à payer : <?php echo Util::priceFormat( $totalToPay ) ?>
					<script type="text/javascript">
						$(document).ready(function() {
							$('#titleProforma').html( $('#titleProforma').html() + " <?php echo Util::priceFormat( $totalToPay ) ?> " );
						});
					</script>
					<?php
					
				}?>
				
                </div>
			</form>
       		</div>
   		</div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->

	<script type="text/javascript">
		$(document).ready(function() {
			
				$('#zizi').tabs({ fxFade: true, fxSpeed: 'fast' });
			
		});
	</script>
	
	<script type="text/javascript">
		$(function() {
			$(".tablesorter").tablesorter( {
				sortList:[[1,0]],
		        headers: { 
		            8: { sorter: false }
		        } 
		    });
		}); 
	</script>
<?
}	


//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//-------------------------------------------------------------------------------------

function displayOs( $bs ){
	
	global $GLOBAL_START_URL;
	
	$query = "
	SELECT DISTINCT( bl.idorder_supplier )
	FROM billing_supplier_row bsr, bl_delivery bl
	WHERE bsr.billing_supplier = '$bs'
	AND bsr.idbl_delivery = bl.idbl_delivery
	ORDER BY bl.idorder_supplier ASC";

	$rs = &DBUtil::query( $query );
	
	if( $rs === false )
		die( Dictionnary::translate("Impossible de récupérer les commandes fournisseur de la facture") );
	
	$os = "";
	
	for($i=0;$i<$rs->RecordCount();$i++){
		
		$idos = $rs->fields("idorder_supplier");
		
		if($i)
			$os.=" - ";
		
		$os.="<a href=\"$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=$idos\">$idos</a>";
		
		$rs->MoveNext();
			
	}	
	
	echo $os;
	
}


//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------
function getBLTotalAmountHT($idbl_delivery){
	
	$bdd = DBUtil::getConnection();
	
	$Query = "SELECT osr.discount_price, osr.quantity FROM order_supplier os";	
}

?>