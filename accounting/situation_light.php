<?
include_once( '../config/init.php' );
include_once( '../objects/Order.php' );
include_once( '../objects/SupplierOrder.php' );
include_once( '../objects/Dictionnary.php' );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

// constuction de la condition de la date
if( isset( $_GET[ 'intervalSelect' ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" ); 
		
	if( $_GET[ 'intervalSelect' ] == 'month' ){
		$dateDebut = $_GET[ 'monthSelect' ];
		
		$arrdate = explode( '-' , $dateDebut );

		$months = array (
						1 => 'Janvier',
						2 => 'Février',
						3 => 'Mars',
						4 => 'Avril',
						5 => 'Mai',
						6 => 'Juin',
						7 => 'Juillet',
						8 => 'Août',
						9 => 'Septembre',
						10 => 'Octobre',
						11 => 'Novembre',
						12 => 'Décembre' );
		
		$mois = mktime( 0, 0, 0, $arrdate[ 1 ], 1, $arrdate[ 0 ] );
		
		$nombreDeJours = intval( date( "t" , $mois ) );
		
		// Buyer ---------------------------------------------------------------------------------------------								
		$conditionBuyerBefore = " AND bl.dispatch_date BETWEEN '$dateDebut-01 00:00:00' AND '$dateDebut-$nombreDeJours 23:59:59'
								  AND bb.DateHeure < '$dateDebut-01' ";
							
		$conditionBuyerAfter = " AND bl.dispatch_date BETWEEN '$dateDebut-01 00:00:00' AND '$dateDebut-$nombreDeJours 23:59:59'
								 AND bb.DateHeure > '$dateDebut-$nombreDeJours' ";
		
		// Supplier ------------------------------------------------------------------------------------------
		$conditionSupplierBefore = " AND bs.Date < '$dateDebut-01'
									 AND bl.dispatch_date BETWEEN '$dateDebut-01' AND '$dateDebut-$nombreDeJours' ";
										
		$conditionSupplierAfter = "	AND bs.Date > '$dateDebut-$nombreDeJours'
									AND bl.dispatch_date BETWEEN '$dateDebut-01' AND '$dateDebut-$nombreDeJours' ";

		// Avoirs !!! ----------------------------------------------------------------------------------------
	
		$conditionCreditBuyerBefore = " AND bl.dispatch_date BETWEEN '$dateDebut-01 00:00:00' AND '$dateDebut-$nombreDeJours 23:59:59'
								  		AND cb.DateHeure < '$dateDebut-01' ";
		
		$conditionCreditBuyerAfter = "  AND bl.dispatch_date BETWEEN '$dateDebut-01 00:00:00' AND '$dateDebut-$nombreDeJours 23:59:59'
								 		AND cb.DateHeure > '$dateDebut-$nombreDeJours' ";
		
		$conditionCreditSupplierBefore = "  AND cs.Date < '$dateDebut-01'
											AND bl.dispatch_date BETWEEN '$dateDebut-01' AND '$dateDebut-$nombreDeJours' ";
		
		$conditionCreditSupplierAfter = "	AND cs.Date > '$dateDebut-$nombreDeJours'
											AND bl.dispatch_date BETWEEN '$dateDebut-01' AND '$dateDebut-$nombreDeJours' ";
		
		// Commnades fournisseur -----------------------------------------------------------------------------
		$conditionOrderSupplier = " AND bl.dispatch_date BETWEEN '$dateDebut-01 00:00:00' AND '$dateDebut-$nombreDeJours 23:59:59' ";
		
		// Commandes clients ---------------------------------------------------------------------------------
		$conditionOrderBuyer = " AND bl.dispatch_date BETWEEN '$dateDebut-01 00:00:00' AND '$dateDebut-$nombreDeJours 23:59:59' ";
	
	}
	
	if( $_GET[ 'intervalSelect' ] == 'between' ){
		
		$startDate 	= Util::dateFormatUs( $_GET[ 'startDate' ] );
		$endDate	= Util::dateFormatUs( $_GET[ 'endDate' ] );

		// Buyer ---------------------------------------------------------------------------------------------
		$conditionBuyerBefore = "	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
									AND bb.DateHeure < '$startDate' ";

		$conditionBuyerAfter = "	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
									AND bb.DateHeure > '$endDate' ";

		// Supplier ------------------------------------------------------------------------------------------
		$conditionSupplierBefore = "	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
										AND bs.Date < '$startDate' ";

		$conditionSupplierAfter = "	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
									AND bs.Date > '$endDate' ";												
	
		// Avoirs !!! ----------------------------------------------------------------------------------------
	
		$conditionCreditBuyerBefore = " AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
										AND cb.DateHeure < '$startDate' ";
		
		$conditionCreditBuyerAfter = " 	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
										AND cb.DateHeure > '$endDate' ";
		
		$conditionCreditSupplierBefore = "	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
											AND cs.Date < '$startDate' ";
		
		$conditionCreditSupplierAfter = "	AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate'
											AND cs.Date > '$endDate' ";
		
		// Commnades fournisseur -----------------------------------------------------------------------------
		$conditionOrderSupplier = " AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate' ";
		
		// Commandes clients ---------------------------------------------------------------------------------
		$conditionOrderBuyer = " AND bl.dispatch_date BETWEEN '$startDate' AND '$endDate' ";
		
		
	}
	
}else{

	$yearB = date('Y');

	// Buyer ---------------------------------------------------------------------------------------------	
	$conditionBuyerBefore = "	AND YEAR( bb.DateHeure ) < '$yearB'
								AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	$conditionBuyerAfter = " AND YEAR( bb.DateHeure ) > '$yearB'
							 AND YEAR( bl.dispatch_date ) = '$yearB' ";
							 
	// Supplier ------------------------------------------------------------------------------------------
	$conditionSupplierBefore = " AND YEAR( bs.Date ) < '$yearB'
								 AND YEAR( bl.dispatch_date ) = '$yearB' ";
								 
							 
	$conditionSupplierAfter = " AND YEAR( bs.Date ) > '$yearB'
								AND YEAR( bl.dispatch_date ) = '$yearB' ";
								
								
	// Avoirs !!! ----------------------------------------------------------------------------------------

	$conditionCreditBuyerBefore = "	AND YEAR( cb.DateHeure ) < '$yearB'
									AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	$conditionCreditBuyerAfter = "  AND YEAR( cb.DateHeure ) > '$yearB'
							 		AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	$conditionCreditSupplierBefore = "  AND YEAR( cs.Date ) < '$yearB'
								 		AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	$conditionCreditSupplierAfter = " AND YEAR( cs.Date ) > '$yearB'
									  AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	// Commnades fournisseur -----------------------------------------------------------------------------
	$conditionOrderSupplier = " AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	// Commandes clients ---------------------------------------------------------------------------------
	$conditionOrderBuyer = " AND YEAR( bl.dispatch_date ) = '$yearB' ";
	
	
}

// TITRES ------------------------------------------------------------------------------------------------

$titleBUB = "<strong>Factures clients établies en avance( qui concernaient la période par rapport à la date de livraison ), à déduire du CA période précédente et ajouter à la courante</strong>
			<br />Factures clients à ré-imputer à la période - par rapport à la date de livraison au client
			<br />Pour mémoire les factures fournisseurs sont enregistrées dans la bonne période";
			
$titleBUA = "<strong>Factures clients qui restaient à établir à la fin de la période ( à ajouter au CA de la période )</strong>
			<br />Factures clients à ajouter à la période - par rapport à la date de livraison au client
			<br />Pour mémoire les factures fournisseurs peuvent être enregistrées ou non dans la bonne période";
			
$titleFSB = "<strong>Factures fournisseur non parvenues dans la période ( à ajouter dans les factures fournisseurs de la période )</strong>
			<br />Factures fournisseurs non parvenues et concernant des livraisons faites dans la période ( à ajouter à la période )
			<br />Pour mémoire les factures clients ( concernant ces factures fournisseurs ) peuvent être enregistrées ou non dans la bonne période";
			
$titleFSA = "<strong>Factures fournisseurs parvenues en avance par rapport à la période ( à déduire des factures fournisseurs de la période )</strong>";

$titleCBB = "<strong>Avoirs en avance</strong>";
$titleCBA = "<strong>Avoirs à établir</strong>";
$titleCSB = "<strong>Avoirs non parvenus</strong>";
$titleCBA = "<strong>Avoirs à parvenir</strong>";


// --------------------------------------------------------------------------------------------------------
// factures client avant période
	$buyerQuery = "
	SELECT bl.idbilling_buyer, b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht, bb.total_charge_ht, bs.idsupplier, bl.idorder_supplier, os.dispatch_date, bs.billing_supplier,bs.Date,bs.total_amount_ht as total_amount_htBS,s.name
	FROM billing_supplier bs, billing_supplier_row bsr, supplier s, bl_delivery bl, billing_buyer bb, buyer b, order_supplier os
	WHERE bs.idsupplier = s.idsupplier
	AND bl.idorder_supplier = os.idorder_supplier
	AND bs.billing_supplier = bsr.billing_supplier
	AND bs.idsupplier = bsr.idsupplier
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	AND bl.idbilling_buyer = bb.idbilling_buyer
	AND bb.idbuyer = b.idbuyer
	$conditionBuyerBefore
	AND bs.proforma = 0
	group by bs.billing_supplier
	ORDER BY bb.idbilling_buyer ASC";

	$rs =& DBUtil::query( $buyerQuery );
	
	$arrayBillingsBuyer = array();
	
	// on rassemble les petits s'il faut et on met le tout dans un tableau --> on trie en qq sorte...
	while( !$rs->EOF() ){
		
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'billing_supplier' ][] = $rs->fields( 'billing_supplier' );
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'name' ][] = $rs->fields( 'name' );
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'idsupplier' ][] = $rs->fields( 'idsupplier' );
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'total_amount_ht' ][] = $rs->fields( 'total_amount_htBS' );		
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'dispatch_date' ][] = $rs->fields( 'dispatch_date' );
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'idorder_supplier' ][] = $rs->fields( 'idorder_supplier' );
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'Date' ][] = $rs->fields( 'Date' );
		
		$arrayBillingsBuyer[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'billings_buyer' ][ ] = array( 'idbuyer' => $rs->fields( 'idbuyer' ),
																								'name' => $rs->fields( 'company' ) != "" ? $rs->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT(firstname," ",lastname) as firstAlast FROM contact WHERE idcontact = 0 AND idbuyer = '.$rs->fields( 'idbuyer' ) )->fields('firstAlast'),
																								'DateHeure' => $rs->fields( 'DateHeure' ),
																								'total_amount_ht'=> $rs->fields( 'total_amount_ht' ),
																								'total_charge_ht'=> $rs->fields( 'total_charge_ht' ),
																								'dispatch_date' => $rs->fields( 'dispatch_date' ),
																								'idbilling_buyer' => $rs->fields( 'idbilling_buyer' ) );		

		$rs->MoveNext();
	}

// factures client après période
	$buyerQuery = "
	SELECT bl.idbilling_buyer, b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht, bb.total_charge_ht, bs.idsupplier, bl.idorder_supplier, os.dispatch_date, bs.billing_supplier,bs.Date,bs.total_amount_ht as total_amount_htBS,s.name
	FROM billing_supplier bs, billing_supplier_row bsr, supplier s, bl_delivery bl, billing_buyer bb, buyer b, order_supplier os
	WHERE bs.idsupplier = s.idsupplier
	AND bl.idorder_supplier = os.idorder_supplier
	AND bs.billing_supplier = bsr.billing_supplier
	AND bs.idsupplier = bsr.idsupplier
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	AND bl.idbilling_buyer = bb.idbilling_buyer
	AND bb.idbuyer = b.idbuyer
	$conditionBuyerAfter
	AND bs.proforma = 0
	group by bs.billing_supplier
	ORDER BY bb.idbilling_buyer ASC";

	$rs =& DBUtil::query( $buyerQuery );
	
	$arrayBillingsBuyerAfter = array();
	
	// on rassemble les petits s'il faut et on met le tout dans un tableau --> on trie en qq sorte...
	while( !$rs->EOF() ){
		
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'billing_supplier' ][] = $rs->fields( 'billing_supplier' );
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'name' ][] = $rs->fields( 'name' );
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'idsupplier' ][] = $rs->fields( 'idsupplier' );
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'total_amount_ht' ][] = $rs->fields( 'total_amount_htBS' );
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'dispatch_date' ][] = $rs->fields( 'dispatch_date' );
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'idorder_supplier' ][] = $rs->fields( 'idorder_supplier' );
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'Date' ][] = $rs->fields( 'Date' );
		
		$arrayBillingsBuyerAfter[ strval( $rs->fields( 'idbilling_buyer' ) ) ][ 'billings_buyer' ][ ] = array( 'idbuyer' => $rs->fields( 'idbuyer' ),
																								'name' => $rs->fields( 'company' ) != "" ? $rs->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT(firstname," ",lastname) as firstAlast FROM contact WHERE idcontact = 0 AND idbuyer = '.$rs->fields( 'idbuyer' ) )->fields('firstAlast'),
																								'DateHeure' => $rs->fields( 'DateHeure' ),
																								'total_amount_ht'=> $rs->fields( 'total_amount_ht' ),
																								'total_charge_ht'=> $rs->fields( 'total_charge_ht' ),
																								'dispatch_date' => $rs->fields( 'dispatch_date' ),
																								'idbilling_buyer' => $rs->fields( 'idbilling_buyer' ) );		

		$rs->MoveNext();
	}

	$internalSupplier = DBUtil::getParameterAdmin( 'internal_supplier' );
	$condInternal = $internalSupplier ? ' AND s.idsupplier <> '.$internalSupplier : "";
	
// factures fournisseur avant période
	$supplierQuery = "
	SELECT DISTINCT bb.idbilling_buyer, s.name, s.idsupplier, bs.Date, bs.total_charge_ht as total_charge_htBS, bs.total_amount_ht as total_amount_htBS, bsr.billing_supplier, os.dispatch_date, b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht
	FROM billing_buyer bb, bl_delivery bl, billing_supplier bs, billing_supplier_row bsr, supplier s, buyer b, order_supplier os
	WHERE bs.billing_supplier = bsr.billing_supplier
	AND bb.idbuyer = b.idbuyer
	AND bs.idsupplier = bsr.idsupplier
	AND bl.idorder_supplier = os.idorder_supplier
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	AND bb.idbilling_buyer = bl.idbilling_buyer
	$conditionSupplierBefore
	$condInternal
	AND s.idsupplier = bs.idsupplier
	AND bs.proforma = 0
	ORDER BY bb.idbilling_buyer ASC";
	
	$rss =& DBUtil::query($supplierQuery);
	$arrayBillingsSupplier = array();
	
	// on rassemble les petits s'il faut et on met le tout dans un tableau --> on trie en qq sorte...
	while( !$rss->EOF() ){
		
		$arrayBillingsSupplier[ $rss->fields( 'idbilling_buyer' ) ][ 'DateHeure' ] = $rss->fields( 'DateHeure' );
		$arrayBillingsSupplier[ $rss->fields( 'idbilling_buyer' ) ][ 'company' ] = $rss->fields( 'company' ) != "" ? $rss->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT(firstname," ",lastname) as firstAlast FROM contact WHERE idcontact = 0 AND idbuyer = '.$rss->fields( 'idbuyer' ) )->fields('firstAlast');
		$arrayBillingsSupplier[ $rss->fields( 'idbilling_buyer' ) ][ 'idbuyer' ] = $rss->fields( 'idbuyer' );
		$arrayBillingsSupplier[ $rss->fields( 'idbilling_buyer' ) ][ 'total_amount_ht' ] = $rss->fields( 'total_amount_ht' );
		$arrayBillingsSupplier[ $rss->fields( 'idbilling_buyer' ) ][ 'dispatch_date' ] = $rss->fields( 'dispatch_date' );
		
		$arrayBillingsSupplier[ $rss->fields( 'idbilling_buyer' ) ][ 'billings_supplier' ][ ] = array( 'idsupplier' => $rss->fields( 'idsupplier' ),
																										'name' => $rss->fields( 'name' ),
																										'Date' => $rss->fields( 'Date' ),
																										'total_amount_htBS' => $rss->fields( 'total_amount_htBS' ),
																										'total_charge_htBS' => $rss->fields( 'total_charge_htBS' ),
																										'dispatch_date' => $rss->fields( 'dispatch_date' ),
																										'billing_supplier' => $rss->fields( 'billing_supplier' ) );		

		$rss->MoveNext();
	}

// factures fournisseur après période
	$supplierQuery = "
	SELECT DISTINCT bb.idbilling_buyer, s.name, s.idsupplier, bs.Date, bs.total_charge_ht as total_charge_htBS, bs.total_amount_ht as total_amount_htBS, bsr.billing_supplier, os.dispatch_date, b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht
	FROM billing_buyer bb, bl_delivery bl, billing_supplier bs, billing_supplier_row bsr, supplier s, buyer b, order_supplier os
	WHERE bs.billing_supplier = bsr.billing_supplier
	AND bb.idbuyer = b.idbuyer
	AND bs.idsupplier = bsr.idsupplier
	AND bl.idorder_supplier = os.idorder_supplier
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	AND bb.idbilling_buyer = bl.idbilling_buyer
	$conditionSupplierAfter
	$condInternal
	AND s.idsupplier = bs.idsupplier
	AND bs.proforma = 0
	ORDER BY bb.idbilling_buyer ASC";
	
	$rss =& DBUtil::query($supplierQuery);
	$arrayBillingsSupplierAfter = array();
	
	// on rassemble les petits s'il faut et on met le tout dans un tableau --> on trie en qq sorte...
	while( !$rss->EOF() ){
		
		$arrayBillingsSupplierAfter[ $rss->fields( 'idbilling_buyer' ) ][ 'DateHeure' ] = $rss->fields( 'DateHeure' );
		$arrayBillingsSupplierAfter[ $rss->fields( 'idbilling_buyer' ) ][ 'company' ] = $rss->fields( 'company' ) != "" ? $rss->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT(firstname," ",lastname) as firstAlast FROM contact WHERE idcontact = 0 AND idbuyer = '.$rss->fields( 'idbuyer' ) )->fields('firstAlast');
		$arrayBillingsSupplierAfter[ $rss->fields( 'idbilling_buyer' ) ][ 'idbuyer' ] = $rss->fields( 'idbuyer' );
		$arrayBillingsSupplierAfter[ $rss->fields( 'idbilling_buyer' ) ][ 'total_amount_ht' ] = $rss->fields( 'total_amount_ht' );
		$arrayBillingsSupplierAfter[ $rss->fields( 'idbilling_buyer' ) ][ 'dispatch_date' ] = $rss->fields( 'dispatch_date' );
		
		$arrayBillingsSupplierAfter[ $rss->fields( 'idbilling_buyer' ) ][ 'billings_supplier' ][ ] = array( 'idsupplier' => $rss->fields( 'idsupplier' ),
																										'name' => $rss->fields( 'name' ),
																										'Date' => $rss->fields( 'Date' ),
																										'total_amount_htBS' => $rss->fields( 'total_amount_htBS' ),
																										'total_charge_htBS' => $rss->fields( 'total_charge_htBS' ),
																										'dispatch_date' => $rss->fields( 'dispatch_date' ),
																										'billing_supplier' => $rss->fields( 'billing_supplier' ) );		

		$rss->MoveNext();
	}


// Avoirs clients avant période
	$buyerQuery = "
	SELECT cb.idcredit, bl.idbilling_buyer, bl.dispatch_date ,b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht, bb.total_charge_ht, bl.idorder_supplier,
	cb.total_charge_ht as total_charges_credit_ht, credited_charges, SUM( discount_price ) as total_credit_ht, cb.DateHeure as credit_DateHeure, cb.idcontact
	FROM bl_delivery bl, billing_buyer bb, buyer b, credits cb
	LEFT JOIN credit_rows crr ON crr.idcredit = cb.idcredit
	WHERE bl.idbilling_buyer = bb.idbilling_buyer
	AND cb.idbilling_buyer = bb.idbilling_buyer
	AND bb.idbuyer = b.idbuyer
	AND cb.editable = 0
	$conditionCreditBuyerBefore
	GROUP BY bb.idbilling_buyer
	ORDER BY bb.idbilling_buyer ASC";
	
	$re =& DBUtil::query( $buyerQuery );
	
	$arrayCreditsBuyerBefore = array();
	
	while( !$re->EOF() ){
		
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'idbilling_buyer' ] = $re->fields( 'idcredit' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'credit_DateHeure' ] = $re->fields( 'credit_DateHeure' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'company' ] = $re->fields( 'company' ) != "" ? $re->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT( firstname , \' \' , lastname ) as fullName FROM contact WHERE idbuyer = '.$re->fields( 'idbuyer' ).' AND idcontact = ' . $re->fields( 'idcontact' ) )->fields( 'fullName' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'idbuyer' ] = $re->fields( 'idbuyer' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'bb_DateHeure' ] = $re->fields( 'DateHeure' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'bb_total_amount_ht' ] = $re->fields( 'total_amount_ht' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'bb_total_charge_ht' ] = $re->fields( 'total_charge_ht' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'idorder_supplier' ] = $re->fields( 'idorder_supplier' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'total_charges_credit_ht' ] = $re->fields( 'total_charges_credit_ht' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'total_credit_ht' ] = $re->fields( 'total_credit_ht' );
		$arrayCreditsBuyerBefore[ $re->fields( 'idcredit' ) ][ 'dispatch_date' ] = $re->fields( 'dispatch_date' );
		
		$re->MoveNext();
	}
	
	
// Avoirs clients après période
	$buyerQuery = "
	SELECT cb.idcredit, bl.idbilling_buyer, bl.dispatch_date, b.company, b.idbuyer, bb.DateHeure, bb.total_amount_ht, bb.total_charge_ht, bl.idorder_supplier,
	cb.total_charge_ht as total_charges_credit_ht, credited_charges, SUM( discount_price ) as total_credit_ht, cb.DateHeure as credit_DateHeure , cb.idcontact
	FROM bl_delivery bl, billing_buyer bb, buyer b, credits cb
	LEFT JOIN credit_rows crr ON crr.idcredit = cb.idcredit
	WHERE bl.idbilling_buyer = bb.idbilling_buyer
	AND cb.idbilling_buyer = bb.idbilling_buyer
	AND bb.idbuyer = b.idbuyer
	AND cb.editable = 0
	$conditionCreditBuyerAfter
	GROUP BY bb.idbilling_buyer
	ORDER BY bb.idbilling_buyer ASC";

	$re =& DBUtil::query( $buyerQuery );
		
	$arrayCreditsBuyerAfter = array();

	while( !$re->EOF() ){

		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'idbilling_buyer' ] = $re->fields( 'idcredit' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'credit_DateHeure' ] = $re->fields( 'credit_DateHeure' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'company' ] = $re->fields( 'company' ) != "" ? $re->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT( firstname , \' \' , lastname ) as fullName FROM contact WHERE idbuyer = '.$re->fields( 'idbuyer' ).' AND idcontact = ' . $re->fields( 'idcontact' ) )->fields( 'fullName' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'idbuyer' ] = $re->fields( 'idbuyer' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'bb_DateHeure' ] = $re->fields( 'DateHeure' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'bb_total_amount_ht' ] = $re->fields( 'total_amount_ht' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'bb_total_charge_ht' ] = $re->fields( 'total_charge_ht' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'idorder_supplier' ] = $re->fields( 'idorder_supplier' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'total_charges_credit_ht' ] = $re->fields( 'total_charges_credit_ht' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'total_credit_ht' ] = $re->fields( 'total_credit_ht' );
		$arrayCreditsBuyerAfter[ $re->fields( 'idcredit' ) ][ 'dispatch_date' ] = $re->fields( 'dispatch_date' );
				
		$re->MoveNext();
	}
		
// Avoirs fournisseurs avant période
	$supplierQuery = "
	SELECT cs.idcredit_supplier, cs.amount, s.name, s.idsupplier, bs.Date, bs.total_charge_ht as total_charge_htBS, bs.total_amount_ht as total_amount_htBS,
	bl.dispatch_date, bs.Date, cs.Date as credit_DateHeure, bs.billing_supplier
	FROM bl_delivery bl, billing_supplier bs, billing_supplier_row bsr, supplier s, buyer b, order_supplier os, credit_supplier cs
	WHERE bs.billing_supplier = bsr.billing_supplier
	AND cs.billing_supplier = bs.billing_supplier
	AND cs.idsupplier = bs.idsupplier
	AND bs.idsupplier = bsr.idsupplier
	AND bl.idorder_supplier = os.idorder_supplier
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	$conditionCreditSupplierBefore
	AND s.idsupplier = bs.idsupplier
	AND bs.proforma = 0
	GROUP BY bs.billing_supplier
	ORDER BY bs.billing_supplier ASC";

	$re =& DBUtil::query( $supplierQuery );
		
	$arrayCreditsSupplierBefore = array();
	
	while( !$re->EOF() ){
		
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'billing_supplier' ] = $re->fields( 'billing_supplier' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'credit_DateHeure' ] = $re->fields( 'credit_DateHeure' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'name' ] = $re->fields( 'name' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'idsupplier' ] = $re->fields( 'idsupplier' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'Date' ] = $re->fields( 'Date' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'total_amount_htBS' ] = $re->fields( 'total_amount_htBS' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'total_charge_htBS' ] = $re->fields( 'total_charge_htBS' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'amount' ] = $re->fields( 'amount' );
		$arrayCreditsSupplierBefore[ $re->fields( 'idcredit_supplier' ) ][ 'dispatch_date' ] = $re->fields( 'dispatch_date' );
		
		$re->MoveNext();
	}
		
// Avoirs fournisseurs après période
	$supplierQuery = "
	SELECT cs.idcredit_supplier, cs.amount, s.name, s.idsupplier, bs.Date, bs.total_charge_ht as total_charge_htBS, bs.total_amount_ht as total_amount_htBS,
	bl.dispatch_date, bs.Date, cs.Date as credit_DateHeure, bs.billing_supplier
	FROM bl_delivery bl, billing_supplier bs, billing_supplier_row bsr, supplier s, buyer b, order_supplier os, credit_supplier cs
	WHERE bs.billing_supplier = bsr.billing_supplier
	AND cs.billing_supplier = bs.billing_supplier
	AND cs.idsupplier = bs.idsupplier
	AND bs.idsupplier = bsr.idsupplier
	AND bl.idorder_supplier = os.idorder_supplier
	AND bsr.idbl_delivery = bl.idbl_delivery
	AND bsr.idsupplier = bl.idsupplier
	$conditionCreditSupplierAfter
	AND s.idsupplier = bs.idsupplier
	AND bs.proforma = 0
	GROUP BY bs.billing_supplier
	ORDER BY bs.billing_supplier ASC";

	$re =& DBUtil::query( $supplierQuery );
		
	$arrayCreditsSupplierAfter = array();
	
	while( !$re->EOF() ){
		
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'billing_supplier' ] = $re->fields( 'billing_supplier' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'credit_DateHeure' ] = $re->fields( 'credit_DateHeure' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'name' ] = $re->fields( 'name' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'idsupplier' ] = $re->fields( 'idsupplier' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'Date' ] = $re->fields( 'Date' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'total_amount_htBS' ] = $re->fields( 'total_amount_htBS' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'total_charge_htBS' ] = $re->fields( 'total_charge_htBS' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'amount' ] = $re->fields( 'amount' );
		$arrayCreditsSupplierAfter[ $re->fields( 'idcredit_supplier' ) ][ 'dispatch_date' ] = $re->fields( 'dispatch_date' );
		
		$re->MoveNext();
	}

//Commandes fournisseurs expédiées sans factures fournisseurs
	$queryCdeFs = "
	SELECT o.idorder_supplier, o.`status` , o.DateHeure, o.total_charge_ht, o.total_amount_ht, bl.dispatch_date, bsr.billing_supplier,
	o.idsupplier, s.name
	FROM `order_supplier` o, supplier s,`bl_delivery` bl
	LEFT JOIN billing_supplier_row bsr ON bsr.idbl_delivery = bl.idbl_delivery
	WHERE o.idorder_supplier = bl.idorder_supplier
	$conditionOrderSupplier
	$condInternal
	AND bsr.billing_supplier IS NULL
	AND s.idsupplier = o.idsupplier
	AND o.total_amount_ht > 0
	GROUP BY o.idorder_supplier";

	$resultOrderSupplier =& DBUtil::query( $queryCdeFs );
	
	
//Commandes clients sans factures clients
	$queryCdeClt = "
	SELECT o.idorder, o.`status` , o.DateHeure, o.total_charge_ht, o.total_amount_ht, bl.dispatch_date, o.idbuyer , b.company, o.idcontact, o.Paid
	FROM `order` o, `bl_delivery` bl, `buyer` b
	WHERE o.idorder = bl.idorder
	$conditionOrderBuyer
	AND b.idbuyer = o.idbuyer
	AND bl.idbuyer = o.idbuyer
	AND o.total_amount_ht > 0
	AND bl.idbilling_buyer =0";
	
	$resultOrderBuyer =& DBUtil::query( $queryCdeClt );
	
	
// --------------------------------------------------------------------------------------------------------
			
	$year = date('Y')-1;
	
	if( isset( $_GET[ 'intervalSelect' ] ) ){		
		exit( getTable( $arrayBillingsSupplier , $arrayBillingsBuyer , $arrayBillingsSupplierAfter , $arrayBillingsBuyerAfter , $titleFSB , $titleBUB , $titleFSA , $titleBUA , $arrayCreditsBuyerBefore , $arrayCreditsBuyerAfter , $arrayCreditsSupplierBefore , $arrayCreditsSupplierAfter , $resultOrderSupplier , $resultOrderBuyer ) );
	}
	
	
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
?>

<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<script type="text/javascript">
	
	function selectMonth(){ $( "#intervalSelectMonth" ).attr('checked',true);}
	
	function selectBetween(){ $( "#intervalSelectBetween" ).attr('checked',true); }

	function generateSituation(){
		type = $( "#intervalSelectMonth" ).attr('checked') ? 'month' : $( "#intervalSelectBetween" ).attr('checked') ? 'between' : '';
				
		var data = "intervalSelect=" + type;
		if( type == 'month' )
			data += "&monthSelect=" + $( '#yearly_month_select' ).val();
		else
			data += "&startDate=" + $( '#bt_start' ).val() + "&endDate=" + $( '#bt_end' ).val();
			
		$( "#situation" ).html( '<img src="<?php echo $GLOBAL_START_URL; ?>images/back_office/content/small_black_spinner.gif" alt="" />' );
		
		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL; ?>/accounting/situation_light.php",
			async: true,
			data: data,
		 	success: function( responseText ){
		 		
		 		$( "#situation" ).html( responseText );

			}

		});
		
	}

	
</script>

<div class="centerMax">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="contentResult">
		<h1 class="titleSearch">
			<span class="textTitle">Situations Clients Fournisseurs</span>
			<span class="selectDate"></span>
			<div class="spacer"></div>
		</h1>
		<div class="spacer"></div>
		<!-- Form -->
		<div class="blocSearch">
			<div style="height:60px;padding:5px;">
				<div class="blocMultiple blocMLeft" style="margin-bottom:0;">	
					<input type="radio" name="intervalSelect" id="intervalSelectMonth" checked="checked" value="month" style="float:left;margin-top:10px;margin-right:5px;" />
					<label style="width:20%"><span>Pour le mois</span></label>
					<select name="yearly_month_select" id="yearly_month_select" onchange="selectMonth();" class="fullWidth">
					<?
					$dat = getdate();
					$mon = $dat['mon'] -1;
					$Str_Year = $dat['year'] ;
					$year = $Str_Year;

					$months = array (
									1 => 'Janvier',
									2 => "Février",
									3 => 'Mars',
									4 => 'Avril',
									5 => 'Mai',
									6 => 'Juin',
									7 => 'Juillet',
									8 => "Août",
									9 => 'Septembre',
									10 => 'Octobre',
									11 => 'Novembre',
									12 => "Décembre"
					);
					
					for ( $i = $mon + 12 ; $i > $mon ;  $i-- ){						
						$j = ( ( $i - 1 ) % 12 + 1 );
						if($j == 12 )
							$j = 0; 							
						
						if( $j > $mon )
							$Str_Year = $year-1; 
							
						$j++;
						
						echo "<option value=\"$Str_Year-$j\"";
						echo ">".$months[ $j ] . " " . $Str_Year. "</option>\n";							
					}
					?>
					</select>
					<div class="spacer"></div>
				</div>	
				<div class="blocMultiple blocMRight" style="margin-bottom:0;">
					<input type="radio" name="intervalSelect" id="intervalSelectBetween" value="between" style="float:left;margin-top:10px;margin-right:5px;" />
					<label style="width:35%;"><span>Entre le </span></label>
					<input type="text" name="bt_start" id="bt_start" class="calendarInput" value="" />
					<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
					
					<label class="smallLabel"><span>et le</span></label>
					<input type="text" name="bt_end" id="bt_end" class="calendarInput" value="" />
					<?php echo DHTMLCalendar::calendar( "bt_end", true, "selectBetween" ) ?>
					<div class="spacer"></div>
					
					<input type="button" class="blueButton floatright" style="margin-top:10px;" id="SearchButton" value="Rechercher" onclick="generateSituation();" />
			
				</div>
			</div>
		</div>
		<div class="spacer"></div>
		<!-- Form -->
		
		<div id="situation">
		<?php getTable( $arrayBillingsSupplier , $arrayBillingsBuyer , $arrayBillingsSupplierAfter , $arrayBillingsBuyerAfter , $titleFSB , $titleBUB , $titleFSA , $titleBUA , $arrayCreditsBuyerBefore , $arrayCreditsBuyerAfter , $arrayCreditsSupplierBefore , $arrayCreditsSupplierAfter , $resultOrderSupplier , $resultOrderBuyer )?>
		</div>
		
	</div> <!-- Fin .contentResult -->

</div> <!-- Fin .centerMax -->

<?php

function getTable( $arrayBillingsSupplier , $arrayBillingsBuyer , $arrayBillingsSupplierAfter , $arrayBillingsBuyerAfter , $titleFS = '' , $titleBU = '' , $titleFSA = '' , $titleBUA = '' , $arrayCreditsBuyerBefore , $arrayCreditsBuyerAfter , $arrayCreditsSupplierBefore , $arrayCreditsSupplierAfter , $resultOrderSupplier , $resultOrderBuyer ){
	global $GLOBAL_START_URL;
?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL; ?>/js/jquery/jquery.tablesorter.js"></script>

	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 95%; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
			padding-bottom:15px;
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 

		.hiddenTablePart{
			display:none;
		}
			
	</style>


	<script type="text/javascript">
	/* <![CDATA[ */
		$( document ).ready( function(){ 

			$( "#BuyerSupplierA" ).tablesorter({
		      	
	      		headers: { 
	      			0: {  sorter:'digit' }, 
	      			1: {  sorter:'text' },
	      			2: {  sorter:'digit' },
	      			3: {  sorter:'shortDate' },
	      			4: {  sorter:'shortDate' },
	      			5: {  sorter:'currency' },
	      			6: {  sorter:'currency' },
	      			7: {  sorter:'currency' },
	      			8: {  sorter:'text' },
	      			9: {  sorter:'digit' },
	      			10: {  sorter:'shortDate' },
	      			11: {  sorter:'text' },
	      			12: {  sorter:'currency' }
            	} 
	      	});

			$( "#BuyerSupplierB" ).tablesorter({
		      	
	      		headers: { 
	      			0: {  sorter:'digit' }, 
	      			1: {  sorter:'text' },
	      			2: {  sorter:'digit' },
	      			3: {  sorter:'shortDate' },
	      			4: {  sorter:'shortDate' },
	      			5: {  sorter:'currency' },
	      			6: {  sorter:'currency' },
	      			7: {  sorter:'currency' },
	      			8: {  sorter:'text' },
	      			9: {  sorter:'digit' },
	      			10: {  sorter:'shortDate' },
	      			11: {  sorter:'text' },
	      			12: {  sorter:'currency' }
            	}, sortForce:true
            	
	      	});      	
	      	
			$( "#SupplierBuyerA" ).tablesorter({
		      	
	      		headers: { 
	      			0: {  sorter:'digit' }, 
	      			1: {  sorter:'text' },
	      			2: {  sorter:'text' },
	      			3: {  sorter:'shortDate' },
	      			4: {  sorter:'shortDate' },
	      			5: {  sorter:'currency' },
	      			6: {  sorter:'currency' },
	      			7: {  sorter:'currency' },
	      			8: {  sorter:'digit' },
	      			9: {  sorter:'text' },
	      			10: {  sorter:'digit' },
	      			11: {  sorter:'shortDate' },
	      			12: {  sorter:'currency' }
            	} 
        	
	      	});
	      	
			$( "#SupplierBuyerB" ).tablesorter({
		      	
	      		headers: { 
	      			0: {  sorter:'digit' }, 
	      			1: {  sorter:'text' },
	      			2: {  sorter:'text' },
	      			3: {  sorter:'shortDate' },
	      			4: {  sorter:'shortDate' },
	      			5: {  sorter:'currency' },
	      			6: {  sorter:'currency' },
	      			7: {  sorter:'currency' },
	      			8: {  sorter:'digit' },
	      			9: {  sorter:'text' },
	      			10: {  sorter:'digit' },
	      			11: {  sorter:'shortDate' },
	      			12: {  sorter:'currency' }
            	} 
        	
	      	});
	      		      	
	      	$( '#BuyerSupplierA_A' ).css( 'margin-top' , $( '#BuyerSupplierA' ).height() / 2 + 'px' );
	      	$( '#BuyerSupplierB_A' ).css( 'margin-top' , $( '#BuyerSupplierB' ).height() / 2 + 'px' );
	      	
	      	$( '#SupplierBuyerA_A' ).css( 'margin-top' , $( '#SupplierBuyerA' ).height() / 2 + 'px' );
	      	$( '#SupplierBuyerB_A' ).css( 'margin-top' , $( '#SupplierBuyerB' ).height() / 2 + 'px' );
	      	
		});
		
		function showDetailTable( tableId ){
		
			$( '#' + tableId + ' .hiddenTablePart' ).toggle();
			
		}

	/* ]]> */
	</script>
		
			<div id="container">
				<ul class="menu">
		            <li class="item"><a href="#all"><span>Résultats de la recherche</span></a></li>
		        </ul>
				<div class="spacer"></div>

				<div class="blocResult mainContent withTab">
				<div id="all">
					<div id="cltFs">
						<div class="content" style="margin-top:5px;">
							<span><?=$titleBU?></span>
							<?
							$htbbb = 0;
							$htChargebbb = 0;
							$htfacfsbbb = 0;
									
							if( count( $arrayBillingsBuyer ) > 0){
							?>
							<table class="dataTable resultTable sortable" id="BuyerSupplierA" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° client</th>
										<th>Nom client</th>
										<th>N° Facture <br />client</th>
										<th>Date de <br />Livraison</th>
										<th>Date Facture<br />client</th>
										<th>Montant<br />Marchandise HT</th>
										<th>Montant Port HT</th>
										<th>Montant HT fac.<br />client</th>
										
										<th class="hiddenTablePart" style="border-left:2px solid #CBCBCB;">Fournisseur</th>
										<th class="hiddenTablePart">N° commande<br />fournisseur</th>
										<th class="hiddenTablePart">Date facture<br />fournisseur</th>
										<th class="hiddenTablePart">N° Facture <br />fournisseur</th>
										<th class="hiddenTablePart">Montant HT facture<br />fournisseur</th>

									</tr>
								</thead>
								<tbody>
									<?php

									foreach( $arrayBillingsBuyer as $billing_supplier => $billings_supplier_values ){ ?>
									
										<?
										$i = 1;
										
										$rowspan = "rowspan='".count( $billings_supplier_values[ 'billings_buyer' ] )."'" ;
										
										if( count( $billings_supplier_values[ 'billings_buyer' ] ) > 1 ){
											
											foreach( $billings_supplier_values[ 'billings_buyer' ] as $key => $values ){?>
												
											<tr>
												<?if($i == 1){?>
												<td <?=$rowspan?> class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $values[ 'idbuyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbuyer' ] ?></a></td>
												<td <?=$rowspan?>><?php echo $values[ 'name' ] ?></td>
												<td <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $values[ 'idbilling_buyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbilling_buyer' ] ?></a></td>
												<td <?=$rowspan?>><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ],false ) ?></td>
												<td <?=$rowspan?>><?php echo Util::dateFormatEu( $values[ 'DateHeure' ],false ) ?></td>
												<td <?=$rowspan?> style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] - $values[ 'total_charge_ht' ] ) ?></td>
												<td <?=$rowspan?> style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_ht' ] ) ?></td>
												<td <?=$rowspan?> style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] ) ?></td>
												<?php } ?>
												<td class="hiddenTablePart" style="border-left:2px solid #CBCBCB;"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$billings_supplier_values[ "idsupplier" ][$i-1]?>" onclick="window.open(this.href);return false;"><?php echo $billings_supplier_values[ 'name' ][$i-1] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $billings_supplier_values[ 'idorder_supplier' ][$i-1] ?>"><?php echo $billings_supplier_values[ 'idorder_supplier' ][$i-1] ?></a></td>
												<td class="hiddenTablePart" ><?php echo Util::dateFormatEu( $billings_supplier_values[ 'Date' ][$i-1] ) ?></td>
												<td class="hiddenTablePart" ><?php echo $billings_supplier_values[ 'billing_supplier' ][$i-1] ?></td>
												<td  class="hiddenTablePart" class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $billings_supplier_values[ 'total_amount_ht' ][$i-1] ) ?></td>
												<?php $htbbb += $billings_supplier_values[ 'total_amount_ht' ][$i-1];
												$htChargebbb += $values[ 'total_charge_ht' ];
												?>
											</tr>
											<?	
											$i++;
											}
											
										}else{
											
											?><tr><?
											foreach( $billings_supplier_values[ 'billings_buyer' ] as $key => $values ){?>
												<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $values[ 'idbuyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbuyer' ] ?></a></td>
												<td><?php echo $values[ 'name' ] ?></td>
												<td><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $values[ 'idbilling_buyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbilling_buyer' ] ?></a></td>
												<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ],false ) ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'DateHeure' ],false ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] - $values[ 'total_charge_ht' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_ht' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] ) ?></td>
												<?php $htbbb += $values[ 'total_amount_ht' ];
												$htChargebbb += $values[ 'total_charge_ht' ];?>									
											<?php } ?>
												<td class="hiddenTablePart" style="border-left:2px solid #CBCBCB;"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$billings_supplier_values[ "idsupplier" ][0]?>" onclick="window.open(this.href);return false;"><?php echo $billings_supplier_values[ 'name' ][0] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $billings_supplier_values[ 'idorder_supplier' ][0] ?>"><?php echo $billings_supplier_values[ 'idorder_supplier' ][0] ?></a></td>
												<td class="hiddenTablePart" ><?php echo Util::dateFormatEu( $billings_supplier_values[ 'Date' ][0] ) ?></td>
												<td class="hiddenTablePart" ><?php echo $billings_supplier_values[ 'billing_supplier' ][0] ?></td>
												<td class="hiddenTablePart" class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $billings_supplier_values[ 'total_amount_ht' ][0] ) ?></td>
											</tr><?
											
										}
										
										$htfacfsbbb += $billings_supplier_values[ 'total_amount_ht' ][0];
										
									} ?>
	
								</tbody>
								<tfoot>
									<tr>
										<th colspan=5 style="text-align:left;">Factures client non établies : <?php echo count( $arrayBillingsBuyer ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $htbbb - $htChargebbb ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $htChargebbb ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $htbbb ) ?></th>
										<th class="hiddenTablePart" colspan="4" style="border-left:2px solid #CBCBCB;"></th>
										<th class="hiddenTablePart" style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbbb ) ?></th>
									</tr>
								</tfoot>
							</table>
							<div style="float:right;" id="BuyerSupplierA_A"><a href="#"  onclick="showDetailTable( 'BuyerSupplierA' ); return false;">< ></a></div>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";	
							}
							?>
							
						</div>
						
						<div class="spacer"></div>
						
						
						
						<div class="content" style="margin-top:25px;">
							<span><?=$titleBUA?></span>
							<?
							$htbba = 0;
							$htChargebba = 0;
							$htfacfsbba = 0;

							if( count( $arrayBillingsBuyerAfter ) > 0){
							?>
							<table class="dataTable resultTable sortable" id="BuyerSupplierB" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° client</th>
										<th>Nom client</th>
										<th>N° Facture <br />client</th>
										<th>Date de <br />Livraison</th>
										<th>Date Facture<br />client</th>
										<th>Montant<br />Marchandise HT</th>
										<th>Montant Port HT</th>
										<th>Montant HT fac.<br />client</th>
										<th class="hiddenTablePart" style="border-left:2px solid #CBCBCB;">Fournisseur</th>
										<th class="hiddenTablePart" >N° commande<br />fournisseur</th>
										<th class="hiddenTablePart" >Date facture<br />fournisseur</th>
										<th class="hiddenTablePart" >N° Facture <br />fournisseur</th>
										<th class="hiddenTablePart" >Montant HT facture<br />fournisseur</th>
									</tr>
								</thead>
								<tbody>
									<?php

									foreach( $arrayBillingsBuyerAfter as $billing_supplier => $billings_supplier_values ){ ?>
									
										<?
										$i = 1;
										
										$rowspan = "rowspan='".count( $billings_supplier_values[ 'billings_buyer' ] )."'" ;
										
										if( count( $billings_supplier_values[ 'billings_buyer' ] ) > 1 ){
											
											foreach( $billings_supplier_values[ 'billings_buyer' ] as $key => $values ){?>
												
											<tr>
												<?if($i == 1){?>
												<td <?=$rowspan?> class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $values[ 'idbuyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbuyer' ] ?></a></td>
												<td <?=$rowspan?>><?php echo $values[ 'name' ] ?></td>
												<td <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $values[ 'idbilling_buyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbilling_buyer' ] ?></a></td>
												<td <?=$rowspan?>><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ],false ) ?></td>
												<td <?=$rowspan?>><?php echo Util::dateFormatEu( $values[ 'DateHeure' ],false ) ?></td>
												<td <?=$rowspan?>  style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] - $values[ 'total_charge_ht' ] ) ?></td>
												<td <?=$rowspan?>  style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_ht' ] ) ?></td>
												<td <?=$rowspan?>  style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] ) ?></td>
												<?php } ?>
												<td class="hiddenTablePart"  style="border-left:2px solid #CBCBCB;"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$billings_supplier_values[ "idsupplier" ][$i-1]?>" onclick="window.open(this.href);return false;"><?php echo $billings_supplier_values[ 'name' ][$i-1] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $billings_supplier_values[ 'idorder_supplier' ][$i-1] ?>"><?php echo $billings_supplier_values[ 'idorder_supplier' ][$i-1] ?></a></td>
												<td class="hiddenTablePart" ><?php echo Util::dateFormatEu( $billings_supplier_values[ 'Date' ][$i-1] ) ?></td>
												<td class="hiddenTablePart" ><?php echo $billings_supplier_values[ 'billing_supplier' ][$i-1] ?></td>
												<td class="hiddenTablePart"  class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $billings_supplier_values[ 'total_amount_ht' ][$i-1] ) ?></td>
												<?php $htbba += $billings_supplier_values[ 'total_amount_ht' ][$i-1];
												$htChargebba += $values[ 'total_charge_ht' ];?>
											</tr>
											<?	
											$i++;
											}
											
										}else{
											
											?><tr><?
											foreach( $billings_supplier_values[ 'billings_buyer' ] as $key => $values ){?>
												<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $values[ 'idbuyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbuyer' ] ?></a></td>
												<td><?php echo $values[ 'name' ] ?></td>
												<td><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $values[ 'idbilling_buyer' ] ?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idbilling_buyer' ] ?></a></td>
												<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ],false ) ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'DateHeure' ],false ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] - $values[ 'total_charge_ht' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_ht' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_ht' ] ) ?></td>
												<?php $htbba += $values[ 'total_amount_ht' ];
												$htChargebba += $values[ 'total_charge_ht' ]; ?>									
											<?php } ?>
												<td class="hiddenTablePart"  style="border-left:2px solid #CBCBCB;"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$billings_supplier_values[ "idsupplier" ][0]?>" onclick="window.open(this.href);return false;"><?php echo $billings_supplier_values[ 'name' ][0] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $billings_supplier_values[ 'idorder_supplier' ][0] ?>"><?php echo $billings_supplier_values[ 'idorder_supplier' ][0] ?></a></td>
												<td class="hiddenTablePart" ><?php echo Util::dateFormatEu( $billings_supplier_values[ 'Date' ][0] ) ?></td>
												<td class="hiddenTablePart" ><?php echo $billings_supplier_values[ 'billing_supplier' ][0] ?></td>
												<td class="hiddenTablePart"  class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $billings_supplier_values[ 'total_amount_ht' ][0] ) ?></td>
											</tr><?
											
										}		
																				
										$htfacfsbba += $billings_supplier_values[ 'total_amount_ht' ][0];
									
									} ?>
	
								</tbody>
								<tfoot>
									<tr>
										<th colspan=5 style="text-align:left;">Factures client établies en avance : <?php echo count( $arrayBillingsBuyerAfter ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $htbba - $htChargebba ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $htChargebba ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $htbba ) ?></th>
										<th class="hiddenTablePart" colspan="4" style="border-left:2px solid #CBCBCB;"></th>
										<th class="hiddenTablePart" style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbba ) ?></th>
									</tr>
								</tfoot>
							</table>
							<div style="float:right;" id="BuyerSupplierB_A"><a href="#"  onclick="showDetailTable( 'BuyerSupplierB' ); return false;">< ></a></div>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";	
							}
							?>
							
						</div>
						<div class="spacer"></div>
						
						
							
					</div>
					
					<div id="fsClt">
						<div class="content" style="margin-top:25px;">
							<span><?=$titleFS?></span>
							<?
							$htbsb = 0;
							$htChargebsb = 0;
							$htfacfsbsb = 0;
									
							if( count( $arrayBillingsSupplier ) > 0){
							?>
							<table class="dataTable resultTable sortable" id="SupplierBuyerA" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° fournisseur</th>
										<th>Fournisseur</th>
										<th>N° facture<br />fournisseur</th>
										<th>Date de<br />livraison</th>
										<th>Date facture<br />fournisseur</th>
										<th>Montant<br />Marchandise HT</th>
										<th>Montant Port HT</th>
										<th>Montant HT fact.<br />fournisseur</th>
										<th class="hiddenTablePart" style="border-left:2px solid #CBCBCB;">N° client</th>
										<th class="hiddenTablePart" >Nom</th>
										<th class="hiddenTablePart" >N° facture<br />client</th>
										<th class="hiddenTablePart" >Date facture<br />client</th>
										<th class="hiddenTablePart" >Montant HT fac.<br />client</th>
									</tr>
								</thead>
								<tbody>					
								
									<?

									foreach( $arrayBillingsSupplier as $billing_buyer => $billings_buyer_values ){ ?>
									
										<?
										$i = 1;
										
										$rowspan = "rowspan='".count( $billings_buyer_values[ 'billings_supplier' ] )."'" ;
										
										if( count( $billings_buyer_values[ 'billings_supplier' ] ) > 1 ){
											
											foreach( $billings_buyer_values[ 'billings_supplier' ] as $key => $values ){?>
											<tr>
												<td class="lefterCol"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idsupplier' ] ?></a></td>
												<td><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'name' ] ?></a></td>
												<td><?php echo $values[ 'billing_supplier' ] ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'Date' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] - $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] ) ?></td>
												<?php $htfacfsbsb += $values[ 'total_amount_htBS' ];
												$htChargebsb += $values[ 'total_charge_htBS' ]; ?>
												<?if($i == 1){?>
												<td class="hiddenTablePart" class="lefterCol" style="border-left:2px solid #CBCBCB;" <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'idbuyer' ] ?></a></td>
												<td class="hiddenTablePart" <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'company' ] ?></a></td>
												<td class="hiddenTablePart" <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $billing_buyer ?>" onclick="window.open(this.href);return false;"><?php echo $billing_buyer ?></a></td>
												<td class="hiddenTablePart" <?=$rowspan?>><?php echo substr( Util::dateFormatEu( $billings_buyer_values[ 'DateHeure' ] ) , 0 , 10 ) ?></td>
												<td class="hiddenTablePart" <?=$rowspan?> style="text-align:right;" class="righterCol"><?php echo Util::priceFormat( $billings_buyer_values[ 'total_amount_ht' ] ) ?></td>
												<?php } ?>
											</tr>
											<?	
											$i++;
											}
										}else{
											?><tr><?
											foreach( $billings_buyer_values[ 'billings_supplier' ] as $key => $values ){?>
												<td class="lefterCol"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idsupplier' ] ?></a></td>
												<td><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'name' ] ?></a></td>
												<td><?php echo $values[ 'billing_supplier' ] ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'Date' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] - $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] ) ?></td>
												<?php $htfacfsbsb += $values[ 'total_amount_htBS' ];
												$htChargebsb += $values[ 'total_charge_htBS' ];?>									
											<?php } ?>
												<td class="hiddenTablePart" class="lefterCol" style="border-left:2px solid #CBCBCB;"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'idbuyer' ] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'company' ] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $billing_buyer ?>" onclick="window.open(this.href);return false;"><?php echo $billing_buyer ?></a></td>
												<td class="hiddenTablePart" ><?php echo substr( Util::dateFormatEu( $billings_buyer_values[ 'DateHeure' ] ) , 0 , 10 ) ?></td>
												<td class="hiddenTablePart" style="text-align:right;" class="righterCol"><?php echo Util::priceFormat( $billings_buyer_values[ 'total_amount_ht' ] ) ?></td>
											</tr><?
										}
										
										$htbsb += $billings_buyer_values[ 'total_amount_ht' ];
										
									} ?>
								</tbody>
								<tfoot>
									<th colspan=5 style="text-align:left;">Factures fournisseur non parvenues : <?php echo count( $arrayBillingsSupplier ) ?></th>
									<th style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbsb - $htChargebsb ) ?></th>
									<th style="text-align:right;"><?php echo Util::priceFormat( $htChargebsb ) ?></th>
									<th style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbsb ) ?></th>
									<th class="hiddenTablePart" colspan=4 style="border-left:2px solid #CBCBCB;"></th>
									<th class="hiddenTablePart" style="text-align:right;"><?php echo Util::priceFormat( $htbsb ) ?></th>
								</tfoot>
							</table>
							<div style="float:right;" id="SupplierBuyerA_A"><a href="#" onclick="showDetailTable( 'SupplierBuyerA' ); return false;">< ></a></div>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";
							}
							?>
						</div><div class="spacer"></div>
					
					
					
						<div class="content" style="margin-top:25px;">
							<span><?=$titleFSA?></span>
							<?
							$htbsa = 0;
							$htChargebsa = 0;
							$htfacfsbsa = 0;
									
							if( count( $arrayBillingsSupplierAfter ) > 0){
							?>
							<table class="dataTable resultTable sortable" id="SupplierBuyerB" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° fournisseur</th>
										<th>Fournisseur</th>
										<th>N° facture<br />fournisseur</th>
										<th>Date de<br />livraison</th>
										<th>Date facture<br />fournisseur</th>
										<th>Montant<br />Marchandise HT</th>
										<th>Montant Port HT</th>
										<th>Montant HT fact.<br />fournisseur</th>
										<th class="hiddenTablePart" style="border-left:2px solid #CBCBCB;">N° client</th>
										<th class="hiddenTablePart" >Nom</th>
										<th class="hiddenTablePart" >N° facture<br />client</th>
										<th class="hiddenTablePart" >Date facture<br />client</th>
										<th class="hiddenTablePart" >Montant HT fac.<br />client</th>
									</tr>
								</thead>
								<tbody>					
								
									<?

									foreach( $arrayBillingsSupplierAfter as $billing_buyer => $billings_buyer_values ){ ?>
									
										<?
										$i = 1;
										
										$rowspan = "rowspan='".count( $billings_buyer_values[ 'billings_supplier' ] )."'" ;
										
										if( count( $billings_buyer_values[ 'billings_supplier' ] ) > 1 ){
											
											foreach( $billings_buyer_values[ 'billings_supplier' ] as $key => $values ){?>
											<tr>
												<td class="lefterCol"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idsupplier' ] ?></a></td>
												<td><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'name' ] ?></a></td>
												<td><?php echo $values[ 'billing_supplier' ] ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'Date' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] - $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] ) ?></td>
												<?php $htfacfsbsa += $values[ 'total_amount_htBS' ];
												$htChargebsa += $values[ 'total_charge_htBS' ]; ?>
												<?if($i == 1){?>
												<td class="hiddenTablePart" class="lefterCol" style="border-left:2px solid #CBCBCB;" <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'idbuyer' ] ?></a></td>
												<td class="hiddenTablePart" <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'company' ] ?></a></td>
												<td class="hiddenTablePart" <?=$rowspan?>><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $billing_buyer ?>" onclick="window.open(this.href);return false;"><?php echo $billing_buyer ?></a></td>
												<td class="hiddenTablePart" <?=$rowspan?>><?php echo substr( Util::dateFormatEu( $billings_buyer_values[ 'DateHeure' ] ) , 0 , 10 ) ?></td>
												<td class="hiddenTablePart" <?=$rowspan?> style="text-align:right;" class="righterCol"><?php echo Util::priceFormat( $billings_buyer_values[ 'total_amount_ht' ] ) ?></td>
												<?php } ?>
											</tr>
											<?	
											$i++;
											}
										}else{
											?><tr><?
											foreach( $billings_buyer_values[ 'billings_supplier' ] as $key => $values ){?>
												<td class="lefterCol"><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'idsupplier' ] ?></a></td>
												<td><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$values[ 'idsupplier' ]?>" onclick="window.open(this.href);return false;"><?php echo $values[ 'name' ] ?></a></td>
												<td><?php echo $values[ 'billing_supplier' ] ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
												<td><?php echo Util::dateFormatEu( $values[ 'Date' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] - $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_charge_htBS' ] ) ?></td>
												<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] ) ?></td>
												<?php $htfacfsbsa += $values[ 'total_amount_htBS' ];
												$htChargebsa += $values[ 'total_charge_htBS' ]; ?>									
											<?php } ?>
												<td class="hiddenTablePart" class="lefterCol" style="border-left:2px solid #CBCBCB;"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'idbuyer' ] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $billings_buyer_values[ 'idbuyer' ] ?>" onclick="window.open(this.href); return false;"><?php echo $billings_buyer_values[ 'company' ] ?></a></td>
												<td class="hiddenTablePart" ><a href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $billing_buyer ?>" onclick="window.open(this.href);return false;"><?php echo $billing_buyer ?></a></td>
												<td class="hiddenTablePart" ><?php echo substr( Util::dateFormatEu( $billings_buyer_values[ 'DateHeure' ] ) , 0 , 10 ) ?></td>
												<td class="hiddenTablePart" style="text-align:right;" class="righterCol"><?php echo Util::priceFormat( $billings_buyer_values[ 'total_amount_ht' ] ) ?></td>
											</tr><?
										}
										
										$htbsa += $billings_buyer_values[ 'total_amount_ht' ];
										
									} ?>
								</tbody>
								<tfoot>
									<th colspan=5 style="text-align:left;">Factures fournisseur parvenues en avance : <?php echo count( $arrayBillingsSupplierAfter ) ?></th>
									<th style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbsa - $htChargebsa ) ?></th>
									<th style="text-align:right;"><?php echo Util::priceFormat( $htChargebsa ) ?></th>
									<th style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbsa ) ?></th>
									<th class="hiddenTablePart" colspan=4 style="border-left:2px solid #CBCBCB;"></th>
									<th class="hiddenTablePart" style="text-align:right;"><?php echo Util::priceFormat( $htbsa ) ?></th>
								</tfoot>
							</table>
							<div style="float:right;" id="SupplierBuyerB_A"><a href="#" onclick="showDetailTable( 'SupplierBuyerB' ); return false;">< ></a></div>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";
							}
							?>
						</div><div class="spacer"></div>
						
						<div class="content" style="margin-top:25px;">
							<span>Avoirs envoyés avant période</span>
							<?
									
							if( count( $arrayCreditsBuyerBefore ) > 0 ){
								$sumCBB = 0;
								$sumCBBFacts = 0;
							?>
							<table class="dataTable resultTable sortable" id="CreditBuyerB" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° Avoir</th>
										<th>N° Client</th>
										<th>Client</th>
										<th>Date Avoir</th>
										<th>Montant Avoir HT</th>
										<th>N° Facture</th>
										<th>Date Facture</th>
										<th>Date Livraison</th>
										<th>Montant Facture HT</th>
									</tr>
								</thead>
								<tbody>
									<?
									foreach( $arrayCreditsBuyerBefore as $idcredit => $values ){
									?>
									<tr>
										<td><?php echo $idcredit ?></td>
										<td><?php echo $values[ 'idbuyer' ] ?></td>
										<td><?php echo $values[ 'company' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'credit_DateHeure' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_credit_ht' ] + $values[ 'total_charges_credit_ht' ] ) ?></td>
										<td><?php echo $values[ 'idbilling_buyer' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'bb_DateHeure' ] , false ) ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'bb_total_amount_ht' ] + $values[ 'bb_total_charge_ht' ] ) ?></td>
									</tr>
									<?
										$sumCBB += $values[ 'total_credit_ht' ] + $values[ 'total_charges_credit_ht' ];
										$sumCBBFacts += $values[ 'bb_total_amount_ht' ] + $values[ 'bb_total_charge_ht' ];
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan='4' style="text-align:left;">Nombre d'avoirs : <?php echo count( $arrayCreditsBuyerBefore ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCBB ) ?></th>
										<th colspan='3'></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCBBFacts ) ?></th>
									</tr>
								</tfoot>
							</table>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";
							}
							?>
						</div><div class="spacer"></div>							

						<div class="content" style="margin-top:25px;">
							<span>Avoirs envoyés après période</span>
							<?
									
							if( count( $arrayCreditsBuyerAfter ) > 0 ){
								$sumCBA = 0;
								$sumCBAFacts = 0;
							?>
							<table class="dataTable resultTable sortable" id="CreditBuyerA" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° Avoir</th>
										<th>N° Client</th>
										<th>Client</th>
										<th>Date Avoir</th>
										<th>Montant Avoir HT</th>
										<th>N° Facture</th>
										<th>Date Facture</th>
										<th>Date Livraison</th>
										<th>Montant Facture HT</th>
									</tr>
								</thead>
								<tbody>	
									<?
									foreach( $arrayCreditsBuyerAfter as $idcredit => $values ){
									?>
									<tr>
										<td><?php echo $idcredit ?></td>
										<td><?php echo $values[ 'idbuyer' ] ?></td>
										<td><?php echo $values[ 'company' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'credit_DateHeure' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_credit_ht' ] + $values[ 'total_charges_credit_ht' ] ) ?></td>
										<td><?php echo $values[ 'idbilling_buyer' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'bb_DateHeure' ] , false ) ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'bb_total_amount_ht' ] + $values[ 'bb_total_charge_ht' ] ) ?></td>
									</tr>
									<?
										$sumCBA += $values[ 'total_credit_ht' ] + $values[ 'total_charges_credit_ht' ];
										$sumCBAFacts += $values[ 'bb_total_amount_ht' ] + $values[ 'bb_total_charge_ht' ];
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan='4' style="text-align:left;">Nombre d'avoirs : <?php echo count( $arrayCreditsBuyerAfter ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCBA ) ?></th>
										<th colspan='3'></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCBAFacts ) ?></th>
									</tr>
								</tfoot>
							</table>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";
							}
							?>
						</div><div class="spacer"></div>							

						<div class="content" style="margin-top:25px;">
							<span>Avoirs perçus avant période</span>
							<?
								$sumCSB = 0;
								$sumCSBFacts = 0;
																	
							if( count( $arrayCreditsSupplierBefore ) > 0 ){

							?>
							<table class="dataTable resultTable sortable" id="CreditSupplierB" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° Avoir</th>
										<th>N° Fournisseur</th>
										<th>Fournisseur</th>
										<th>Date Avoir</th>
										<th>Montant Avoir HT</th>
										<th>N° Facture</th>
										<th>Date Facture</th>
										<th>Date Livraison</th>
										<th>Montant Facture HT</th>
									</tr>
								</thead>
								<tbody>	
									<?
									foreach( $arrayCreditsSupplierBefore as $idcredit => $values ){
									?>
									<tr>
										<td><?php echo $idcredit ?></td>
										<td><?php echo $values[ 'idsupplier' ] ?></td>
										<td><?php echo $values[ 'name' ]?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'credit_DateHeure' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'amount' ] ) ?></td>
										<td><?php echo $values[ 'billing_supplier' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'Date' ] ) ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] + $values[ 'total_charge_htBS' ] ) ?></td>
									</tr>
									<?
										$sumCSB += $values[ 'amount' ];
										$sumCSBFacts += $values[ 'total_amount_htBS' ] + $values[ 'total_charge_htBS' ];
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan='4' style="text-align:left;">Nombre d'avoirs : <?php echo count( $arrayCreditsSupplierBefore ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCSB ) ?></th>
										<th colspan='3'></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCSBFacts ) ?></th>
									</tr>
								</tfoot>
							</table>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";
							}
							?>
						</div><div class="spacer"></div>							
						
						<div class="content" style="margin-top:25px;">
							<span>Avoirs percus après période</span>
							<?
								$sumCSA = 0;
								$sumCSAFacts = 0;									
							if( count( $arrayCreditsSupplierAfter ) > 0 ){
							?>
							<table class="dataTable resultTable sortable" id="CreditSupplierA" style="margin-top:10px;width:97%;float:left;">
								<thead>
									<tr>
										<th>N° Avoir</th>
										<th>N° Fournisseur</th>
										<th>Fournisseur</th>
										<th>Date Avoir</th>
										<th>Montant Avoir HT</th>
										<th>N° Facture</th>
										<th>Date Facture</th>
										<th>Date Livraison</th>
										<th>Montant Facture HT</th>
									</tr>
								</thead>
								<tbody>	
									<?
									foreach( $arrayCreditsSupplierAfter as $idcredit => $values ){
									?>
									<tr>
										<td><?php echo $idcredit ?></td>
										<td><?php echo $values[ 'idsupplier' ] ?></td>
										<td><?php echo $values[ 'name' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'credit_DateHeure' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'amount' ] ) ?></td>
										<td><?php echo $values[ 'billing_supplier' ] ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'Date' ] ) ?></td>
										<td><?php echo Util::dateFormatEu( $values[ 'dispatch_date' ] ) ?></td>
										<td style="text-align:right;"><?php echo Util::priceFormat( $values[ 'total_amount_htBS' ] + $values[ 'total_charge_htBS' ] ) ?></td>
									</tr>
									<?
										$sumCSA += $values[ 'amount' ];
										$sumCSAFacts += $values[ 'total_amount_htBS' ] + $values[ 'total_charge_htBS' ];
									}
									?>
								</tbody>
								<tfoot>
									<tr>
										<th colspan='4' style="text-align:left;">Nombre d'avoirs : <?php echo count( $arrayCreditsSupplierAfter ) ?></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCSA ) ?></th>
										<th colspan='3'></th>
										<th style="text-align:right;"><?php echo Util::priceFormat( $sumCSAFacts ) ?></th>
									</tr>
								</tfoot>
							</table>
							<?
							}else{
								echo "<br /><div style='color:#ff0000;'>Aucun résultat</div>";
							}
							?>
						</div><div class="spacer"></div>							

						<div class="content" style="margin-top:25px;">
							<span>Commandes fournisseurs éxpédiées sans factures fournisseurs :</span>
							<?php
							$sumTotalAmountHT = 0;
							$sumTotalChargeHT = 0;
							
							if( $resultOrderSupplier->RecordCount() ){
								?>
								<table class="dataTable resultTable sortable" id="OrderSupplier" style="margin-top:10px;width:97%;float:left;">
									<thead>
										<tr>
											<th>N° Commande fournisseur</th>
											<th>N° Fournisseur</th>
											<th>Fournisseur</th>
											<th>Date Commande</th>
											<th>Date de livraison</th>
											<th>Statut</th>
											<th>Montant Marchandise HT</th>
											<th>Montant HT</th>
											<th>Port HT</th>
										</tr>
									</thead>
									<tbody>
									<?php									
									while( !$resultOrderSupplier->EOF() ){
										$totalAmountHT = $resultOrderSupplier->fields( 'total_amount_ht' );
										$totalChargeHT = $resultOrderSupplier->fields( 'total_charge_ht' );
										
										$sumTotalAmountHT += $totalAmountHT;
										$sumTotalChargeHT += $totalChargeHT;
										
										$statut = DBUtil::getDBValue( 'order_supplier_status_1' , 'order_supplier_status' , 'order_supplier_status' , $resultOrderSupplier->fields( 'status' ) );
										
										?>
										<tr>
											<td><a onclick="window.open(this.href);return false;" href="<?=$GLOBAL_START_URL?>/sales_force/supplier_order.php?IdOrder=<?php echo $resultOrderSupplier->fields( 'idorder_supplier' ) ?>"><?php echo $resultOrderSupplier->fields( 'idorder_supplier' ) ?></a></td>
											<td><a href="<?=$GLOBAL_START_URL?>/product_management/enterprise/supplier.php?idsupplier=<?=$resultOrderSupplier->fields( 'idsupplier' )?>" onclick="window.open(this.href);return false;"><?php echo $resultOrderSupplier->fields( 'idsupplier' ) ?></a>
											<td><?php echo $resultOrderSupplier->fields( 'name' ) ?></td>
											<td><?php echo Util::dateFormatEu( $resultOrderSupplier->fields( 'DateHeure' ) , false ) ?></td>
											<td><?php echo Util::dateFormatEu( $resultOrderSupplier->fields( 'dispatch_date' ) ) ?></td>
											<td><?php echo $statut ?></td>
											<td style="text-align:right;"><?php echo Util::priceFormat( $totalAmountHT - $totalChargeHT ) ?></td>
											<td style="text-align:right;"><?php echo Util::priceFormat( $totalAmountHT ) ?></td>
											<td style="text-align:right;"><?php echo Util::priceFormat( $totalChargeHT ) ?></td>
										</tr>
										<?
										$resultOrderSupplier->MoveNext();
									}
									?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan = 6>Nombre de commandes : <?php echo $resultOrderSupplier->RecordCount() ?></th>
											<th style="text-align:right;"><?php echo Util::priceFormat( $sumTotalAmountHT - $sumTotalChargeHT ); ?></th>
											<th style="text-align:right;"><?php echo Util::priceFormat( $sumTotalAmountHT );?></th>
											<th style="text-align:right;"><?php echo Util::priceFormat( $sumTotalChargeHT ); ?></th>
										</tr>
									</tfoot>
								</table>
								<?php
							}else{
								echo "Aucune commande fournisseur";
							}
							
							?>
						</div>
						
						<div class="spacer"></div>
						
						<div class="content" style="margin-top:25px;">
							<span>Commandes clients éxpédiées sans factures clients :</span>
							<?php
							$sumTotalAmountHT = 0;
							$sumTotalChargeHT = 0;
							
							if( $resultOrderBuyer->RecordCount() ){
								?>
								<table class="dataTable resultTable sortable" id="OrderBuyer" style="margin-top:10px;width:97%;float:left;">
									<thead>
										<tr>
											<th>N° Commande</th>
											<th>N° Client</th>
											<th>Client</th>
											<th>Date Commande</th>
											<th>Date de livraison</th>
											<th>Statut</th>
											<th>Montant Marchandise HT</th>
											<th>Montant HT</th>
											<th>Port HT</th>
										</tr>
									</thead>
									<tbody>
									<?php									
									while( !$resultOrderBuyer->EOF() ){
									
										$totalAmountHT = $resultOrderBuyer->fields( 'total_amount_ht' );
										$totalChargeHT = $resultOrderBuyer->fields( 'total_charge_ht' );
										
										$sumTotalAmountHT += $totalAmountHT;
										$sumTotalChargeHT += $totalChargeHT;
										
										$statut = Dictionnary::translate( $resultOrderBuyer->fields( 'status' ) );
										if( $resultOrderBuyer->fields( 'Paid' ) == 1 ){
											$statut .= "<br />Payé";										
										}
										
										$company = $resultOrderBuyer->fields( 'company' ) != "" ? $resultOrderBuyer->fields( 'company' ) : DBUtil::query( 'SELECT CONCAT( firstname , \' \' , lastname ) as fullName FROM contact WHERE idbuyer = ' . $resultOrderBuyer->fields( 'idbuyer' ) . ' AND idcontact = ' . $resultOrderBuyer->fields( 'idcontact' ) )->fields( 'fullName' );
										?>
										<tr>
											<td>
											<a onclick="window.open(this.href);return false;" href="<?=$GLOBAL_START_URL?>/sales_force/com_admin_order.php?IdOrder=<?php echo $resultOrderBuyer->fields( 'idorder' ) ?>"><?php echo $resultOrderBuyer->fields( 'idorder' ) ?></a></td>
											<td><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $resultOrderBuyer->fields( 'idbuyer' ) ?>" onclick="window.open(this.href); return false;"><?php echo $resultOrderBuyer->fields( 'idbuyer' ) ?></a></td>
											<td><?php echo $company ?></td>
											<td><?php echo Util::dateFormatEu( $resultOrderBuyer->fields( 'DateHeure' ) , false ) ?></td>
											<td><?php echo Util::dateFormatEu( $resultOrderBuyer->fields( 'dispatch_date' ) ) ?></td>
											<td><?php echo $statut ?></td>
											<td style="text-align:right;"><?php echo Util::priceFormat( $totalAmountHT - $totalChargeHT ) ?></td>
											<td style="text-align:right;"><?php echo Util::priceFormat( $totalAmountHT ) ?></td>
											<td style="text-align:right;"><?php echo Util::priceFormat( $totalChargeHT ) ?></td>
										</tr>
										<?
										$resultOrderBuyer->MoveNext();
									}?>
									</tbody>
									<tfoot>
										<tr>
											<th colspan = 6>Nombre de commandes : <?php echo $resultOrderBuyer->RecordCount() ?></th>
											<th style="text-align:right;"><?php echo Util::priceFormat( $sumTotalAmountHT - $sumTotalChargeHT ); ?></th>
											<th style="text-align:right;"><?php echo Util::priceFormat( $sumTotalAmountHT );?></th>
											<th style="text-align:right;"><?php echo Util::priceFormat( $sumTotalChargeHT ); ?></th>
										</tr>
									</tfoot>
								</table>
								<?php
							}else{
								echo "Aucune commande client";
							}
							
							?>
							
						</div>
						<div class="spacer"></div>
							
					</div>
					<div class="spacer"></div>
					
				</div>
				
				<div class="content" style="margin-top:25px;">
					<table class="dataTable resultTable" style="width:50%;">
						<thead>
						<tr>
							<th>Récapitulatif</th>
							<th>Montant<br />Marchandise<br />HT</th>
							<th>Montant Port<br />HT</th>
							<th>Montant Total<br />HT</th>	
						</tr>
						</thead>
						
						<tbody>
						<tr>
							<th>Total factures clients établies en avance</th>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htbbb - $htChargebbb ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htChargebbb ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htbbb ) ?></td>
						</tr>
						<tr>
							<th>Total factures client restant à établir</th>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htbba - $htChargebba ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htChargebba ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htbba ) ?></td>
						</tr>
						<tr>
							<th>Total factures fournisseurs non parvenues</th>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbsb - $htChargebsb ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htChargebsb ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( $htfacfsbsb ) ?></td>
						</tr>
						<tr>
							<th>Total factures fournisseurs parvenues en avance</th>
							<td style="text-align:right;"><?php echo Util::priceFormat( 0 - ( $htfacfsbsa - $htChargebsa ) ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( 0 - $htChargebsa ) ?></td>
							<td style="text-align:right;"><?php echo Util::priceFormat( 0 - $htfacfsbsa ) ?></td>
						</tr>
						</tbody>
						
						<tfoot>
						<tr>
							<th style="font-weight:bold;">Total</th>
							<th style="font-weight:bold;text-align:right;"><?php echo Util::priceFormat( ( $htbba - $htChargebba ) + ( $htbbb - $htChargebbb ) + ( $htfacfsbsb - $htChargebsb ) - ( $htfacfsbsa + $htChargebsa ) ) ?></th>
							<th style="font-weight:bold;text-align:right;"><?php echo Util::priceFormat( $htChargebbb + $htChargebba - $htChargebsa  ) ?></th>
							<th style="font-weight:bold;text-align:right;"><?php echo Util::priceFormat( $htbba + $htbbb + $htfacfsbsb - $htfacfsbsa ) ?></th>
						</tr>
						</tfoot>
					</table>
				</div>
				</div>
			</div>
			<script type="text/javascript">
			/* <![CDATA[ */
				
				//onglets
				
				$(document).ready(function(){
					
						$('#container').tabs({ fxFade: true, fxSpeed: 'fast' })
					
				});
				
			/* ]]> */
			</script>
<?
}
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>