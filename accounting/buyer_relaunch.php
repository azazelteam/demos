<?php

 /**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des relances du client
 */
 

/***************************************
 * Affichage des relances du client    *
 ***************************************/

include_once( "../objects/classes.php" );

//Initialisation des variables

if( isset( $_GET["b"] ) ){
	
	$idbuyer = $_GET[ "b" ];
	
	$req = "
	SELECT o.idorder, 
		o.conf_order_date,  
		o.balance_date,
		bb.relaunch_date_1,  
		bb.relaunch_date_2,  
		bb.relaunch_date_3,  
		bb.status
	FROM `order` o, billing_buyer bb
	WHERE bb.idorder = o.idorder
	AND bb.idbuyer= '$idbuyer'
	AND (
		bb.relaunch_date_1 <> '0000-00-00' 
		OR bb.relaunch_date_2 <> '0000-00-00' 
		OR bb.relaunch_date_3 <> '0000-00-00'
	)";
	
	$res = DBUtil::query($req);
	
	$Title = "Relances du client N° $idbuyer";

}else{
	
	$error = "Paramètre absent, assurez-vous que vous avez suivi la procédure normale pour arriver à cette page.<br />Si c'est le cas, veuillez contacter le <a href=\"mailto:webmaster@akilae-saas.fr\">webmaster</a>";
	
	$Title = "Relances client";
	
}

$banner = "no";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

if( isset( $error ) ){

	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">$error</p>";
	
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}
	
?>
		<style type="text/css">
		<!--
			div#global{
				min-width:0px;
			}
			
			div#global div#globalMainContent{
				padding-right:5px;
				width:750px;
			}
		-->
		</style>
		<div id="globalMainContent">
			<div class="mainContent fullWidthContent">
				<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title">
							Relances client n° <?php echo $idbuyer ?>
						</p>
					</div>
					<div class="subContent">
						<div class="tableContainer">
							<table class="dataTable resultTable">
								<thead>
									<tr>
										<th>Commande n°</th>
										<th>Date de confirmation</th>
										<th>Niveau de relance</th>
										<th>Date de la dernière relance</th>
										<th>Statut</th>
										<th>Date de paiement</th>
									</tr>
								</thead>
								<tbody>
<?php
								
								for( $i = 0 ; $i < $res->RecordCount() ; $i++ ){ 
									
									$idorder			= $res->Fields( "idorder" );
									$conf_order_date	= $res->Fields( "conf_order_date" );
									$relaunch_date_1	= $res->Fields( "relaunch_date_1" );
									$relaunch_date_2	= $res->Fields( "relaunch_date_2" );
									$relaunch_date_3	= $res->Fields( "relaunch_date_3" );
									$balance_date		= $res->Fields( "balance_date" );
									
									if( $balance_date == "0000-00-00" )
										$balance_date = "-";
									
									if( $conf_order_date == "0000-00-00 00:00:00" )
										$conf_order_date = "-";
									
									if( $relaunch_date_1 == "0000-00-00" )
										$relaunch_date_1 = "-";
									
									if( $relaunch_date_2 == "0000-00-00" )
										$relaunch_date_2 = "-";
									
									if( $relaunch_date_3 == "0000-00-00" )
										$relaunch_date_3 = "-";
									
									$status = $res->Fields( "status" );
									
									$relaunch_infos = getRelaunchInfos( $relaunch_date_1, $relaunch_date_2, $relaunch_date_3 );
									
?>
									<tr class="blackText">
										<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $idorder ?>" onclick="window.opener.open(this.href, '_blank'); window.close(); return false;"><?php echo $idorder ?></a></td>
										<td><?php echo usDate2eu( substr( $conf_order_date, 0, 10 ) ) ?></td>
										<td><?php echo $relaunch_infos[ 0 ] ?></td>
										<td><?php echo usDate2eu( $relaunch_infos[ 1 ] ) ?></td>
										<td><?php echo Dictionnary::translate( $status ) ?></td>
										<td class="righterCol"><?php echo usDate2eu( $balance_date ) ?></td>
									</tr>
<?php
									
									$res->MoveNext();
									
								}

?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="bottomRight"></div><div class="bottomLeft"></div>
			</div>
		</div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
 
//---------------------------------------------------------------------------------------------------------

function getRelaunchInfos( $relaunch_date_1, $relaunch_date_2, $relaunch_date_3 ){
	
	if( $relaunch_date_3 != "0000-00-00" ){
		
		$level = 3;
		$last_relaunch = $relaunch_date_3;
		
	}elseif( $relaunch_date_2 != "0000-00-00" ){
		
		$level = 2;
		$last_relaunch = $relaunch_date_2;
		
	}elseif( $relaunch_date_1 != "0000-00-00" ){
		
		$level = 1;
		$last_relaunch = $relaunch_date_1;
		
	}
	
	return array( $level, $last_relaunch );
	
}

//---------------------------------------------------------------------------------------------------------

?>