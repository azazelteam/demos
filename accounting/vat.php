<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Déclaraction de TVA
 */

//------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../config/init.php" );

//------------------------------------------------------------------------------------------------


if( isset( $_POST[ "declare" ] ) ){
	
	if( !is_array( $_POST[ "declare" ] ) )
		$_POST[ "declare" ] = array( $_POST[ "declare" ] );
	
	$date = date( "Y-m-d" );
		
	foreach( $_POST[ "declare" ] as $value ){

		list( $billing_supplier, $idsupplier ) = explode( ",", stripslashes( $value ) );

		$query = "
		UPDATE billing_supplier 
		SET vat_declaration_date = NOW()
		WHERE billing_supplier = " . DBUtil::quote( $billing_supplier ) . "
		AND idsupplier = '$idsupplier'
		LIMIT 1";

		DBUtil::query( $query );
		
	}

}

//------------------------------------------------------------------------------------------------
/* export */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) ){
	
	$req = $_GET[ "req" ];
	$exportableArray =& getExportableArray( $req );
	exportArray( $exportableArray );
	
	exit();
	
}

//------------------------------------------------------------------------------------------

if( isset( $_POST[ "month" ] ) && preg_match( "/[0-9]{4}-[0-9]{2}/", $_POST[ "month" ] ) ){
	
	$year	= substr( $_POST[ "month" ], 0, 4 );
	$month 	= substr( $_POST[ "month" ], 5, 2 );
	
}
else{
	
	$year	= date( "Y" );
	$month	= date( "m" );
	
}

//--------------------------------------------------------------------------------------------------

$Title = "Déclaration de TVA";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------------

$lang = User::getInstance()->getLang();

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript">
<!--
	
	/* ----------------------------------------------------------------------------------- */
	
	$(document).ready(function() { 
		
		$('#tabs').tabs({ fxFade: true, fxSpeed: 'fast' });

	});
	
	/* ----------------------------------------------------------------------------------- */
	
	function showSupplierInfos( idsupplier ){
		
		if( idsupplier == '0' )
			return;
		
		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
	        		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				});
				
				$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');
				
				$('.blockOverlay').attr('title','Cliquer pour sortir').click($.unblockUI);
				
			}
			
		});
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	
-->
</script>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form action="<?php echo $GLOBAL_START_URL; ?>/accounting/vat.php" method="post">
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div>
			<div class="topLeft"></div>
			<div class="content">
	        	<div class="headTitle">
	                <p class="title">Déclaration de TVA</p>
	            </div>
				<div class="subContent">
					<input type="radio" name="declared" value="0"<?php if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] ) echo " checked=\"checked\""; ?> onclick="if( this.checked ) $( '#month' ).attr( 'disabled', true );" /> TVA à déclarer
	                <input type="radio" name="declared" value="1"<?php if( isset( $_POST[ "declared" ] ) && $_POST[ "declared" ] ) echo " checked=\"checked\""; ?> onclick="if( this.checked ) $( '#month' ).attr( 'disabled', false );" /> TVA déclarée pour le mois de
					<select class="fullWidth" name="month" id="month"<?php if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] ) echo " disabled=\"disabled\""; ?>>
					<?php
									
						$month = date( "m" );
						$year = date( "Y" );
	
						for( $i = 0, $j = $month ; $i < 12 ;  $i++, $j-- ){
							
							if( $j == 0 ){
								
								$j = 12;
								$year--;
								
							}
							
							$value = "$year-" . sprintf( "%02d", $j );
						
							$selected = isset( $_POST[ "month" ] ) && $_POST[ "month" ] == $value ? " selected=\"selected\"" : "";
							
							?>
							<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo strftime( "%B", mktime( 0, 0, 0, $j, 1, date( "Y" ) ) ) . " " . $year; ?></option>
							<?php
							
						}
	
					?>
					</select>
	     			<input type="submit" class="blueButton" value="Rechercher" />
	     		</div>
	     	</div>
	     	<div class="bottomRight"></div>
			<div class="bottomLeft"></div>
	     </div>
	     <div class="mainContent fullWidthContent">
			<div class="topRight"></div>
			<div class="topLeft"></div>
			<div class="content">
				<div id="tabs" style="margin-top:20px;">
					<ul class="menu" style="margin-bottom:10px;">
						<li class="item">
			             	<div class="right"></div>
			             	<div class="left"></div>
			                <div class="centered"><a href="#Invoicing">TVA sur facturation</a></div>
						</li>
						<li class="item">
			             	<div class="right"></div>
			             	<div class="left"></div>
			                <div class="centered"><a href="#Regulation">TVA sur règlement</a></div>
						</li>
					</ul>
					<div class="resultTableContainer clear" id="Invoicing">
						<div class="subContent">
							<?php listInvoices(); ?>
			            </div><!-- subContent -->
			            <div id="SearchResults"></div>
					</div><!-- Invoicing -->
					<div class="resultTableContainer clear" id="Regulation">
						<div class="subContent">			
			            	<?php listRegulations(); ?>
			            </div><!-- subContent -->
					</div><!-- Regulation -->
				</div><!-- tabs -->
	        
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
	</form>
</div> <!-- GlobalMainContent -->
<?php

//------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function listInvoices(){
	
	global $month, $year;

	//TVA national
	
	$query = "
	SELECT SUM( amount * ( amount_type = 'debit' ) ) - SUM( amount * ( amount_type = 'credit' ) )  AS amount
	FROM account_plan, billing_supplier bs
	WHERE idaccount_plan IN( SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_use = 5 )
	AND code_eu = 'NATIONAL'
	AND action = 'supplier_invoices'
	AND bs.idsupplier = idrelation";
	
	if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] )
		$query .= " AND bs.vat_declaration_date LIKE '0000-00-00'";
	else $query .= " AND bs.vat_declaration_date LIKE '" . stripslashes( $_POST[ "month" ] ) . "%'";

	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		trigger_error( "Impossible de récupérer le montant HT des factures nationales", E_USER_ERROR );

	$vat = abs( $rs->fields( "amount" ) );
	
	//TTC national
	//HT intra
	//HT export
	
	$query = "
	SELECT SUM( amount * ( code_eu = 'NATIONAL' ) * ( amount_type = 'debit' ) ) - SUM( amount * ( code_eu = 'NATIONAL' ) * ( amount_type = 'credit' ) ) AS national,
		SUM( amount * ( code_eu = 'INTRA' ) * ( amount_type = 'debit' ) ) - SUM( amount * ( code_eu = 'INTRA' ) * ( amount_type = 'credit' ) ) AS intra,
		SUM( amount * ( code_eu = 'EXPORT' ) * ( amount_type = 'debit' ) ) - SUM( amount * ( code_eu = 'EXPORT' ) * ( amount_type = 'credit' ) ) AS export,
		SUM( amount * ( amount_type = 'debit' ) ) - SUM( amount * ( amount_type = 'credit' ) ) AS suma
	FROM account_plan, billing_supplier bs
	WHERE idaccount_plan IN( SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_use = 1 )
	AND action = 'supplier_invoices'
	AND bs.idsupplier = idrelation";
	
	if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] )
		$query .= " AND bs.vat_declaration_date LIKE '0000-00-00'";
	else $query .= " AND bs.vat_declaration_date LIKE '" . stripslashes( $_POST[ "month" ] ) . "%'";

	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		trigger_error( "Impossible de récupérer les montants des factures", E_USER_ERROR );
	
	$nationalATI	= abs( $rs->fields( "national" ) );
	$nationalET		= $nationalATI - $vat;
	$intra			= abs( $rs->fields( "intra" ) );
	$export			= abs( $rs->fields( "export" ) );
	$sum			= abs( $rs->fields( "suma" ) );
	
	?>
	<div class="subContent">
		<div class="tableContainer">
			<table class="dataTable">
				<thead>
					<tr>
						<th class="filledCell" style="text-align:center; width:25%;">France TTC</th>
						<th class="filledCell" style="text-align:center; width:25%;">France HT</th>
						<th class="filledCell" style="text-align:center; width:25%;">Intra-communautaire</th>
						<th class="filledCell" style="text-align:center; width:25%;">Export</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $nationalATI ) ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $nationalET ) ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $intra ) ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $export ) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	
	<?php
	
	if( $intra > 0 )
		getIntraInvoices();
	
}

//------------------------------------------------------------------------------------------------

function listRegulations(){
	return;
	//TVA national
	
	/*$query = "
	SELECT SUM( ap.amount ) AS vat
	FROM regulations_supplier rs, billing_regulations_supplier brs, billing_supplier bs, account_plan ap
	WHERE ap.record_type LIKE 'regulations_supplier'
	AND ap.idrelation = rs.idregulations_supplier
	AND ap.idaccount_plan IN( SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_use = 5 )
	AND rs.idregulations_supplier = brs.idregulations_supplier
	AND brs.billing_supplier = bs.billing_supplier
	AND brs.idsupplier = bs.idsupplier
	AND bs.service = 1";
	
	if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] )
		$query .= " AND bs.vat_declaration_date LIKE '0000-00-00'";
	else $query .= " AND bs.vat_declaration_date LIKE '" . stripslashes( $_POST[ "month" ] ) . "%'";
	
	$rs = DBUtil::query( $query );;
	
	$vat = $rs->fields( "vat" ) == NULL ? 0.0 : $rs->fields( "vat" );
	
	$query = "
	SELECT brs.idregulations_supplier,
		ap.amount,
		ap.code_eu,
		bs.billing_supplier,
		bs.idsupplier,
		rs.credit_balance_used,
		rs.overpayment,
		rs.bank_date,
		p.name_1 AS payment
	FROM regulations_supplier rs, billing_regulations_supplier brs, billing_supplier bs, payment p, account_plan ap
	WHERE ap.record_type LIKE 'regulations_supplier'
	AND ap.idrelation = rs.idregulations_supplier
	AND rs.idregulations_supplier = brs.idregulations_supplier
	AND brs.billing_supplier = bs.billing_supplier
	AND brs.idsupplier = bs.idsupplier
	AND p.idpayment = rs.idpayment
	AND bs.service = 1";

	if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] )
		$query .= " AND bs.vat_declaration_date LIKE '0000-00-00'";
	else $query .= " AND bs.vat_declaration_date LIKE '" . stripslashes( $_POST[ "month" ] ) . "%'";
	
	$query .= "
	ORDER BY bs.billing_supplier ASC, bs.idsupplier ASC";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center;">Aucun règlement trouvé</p>
		<?php
		
		return;
		
	}
	
	$nationalATI	= 0.0;
	$intra			= 0.0;
	$export			= 0.0;
	$sum			= 0.0;
	
	while( !$rs->EOF() ){
		
		switch( $rs->fields( "code_eu" ) ){
			
			case "NATIONAL" : 	$nationalATI 	+= $rs->fields( "amount" ); break;
			case "INTRA" :		$intra 			+= $rs->fields( "amount" ); break;
			case "EXPORT" :		$export 		+= $rs->fields( "amount" ); break;
					
		}
		
		$sum += $rs->fields( "amount" );
		
		$rs->MoveNext();
		
	}
	
	$nationalET	= $nationalATI - $vat;
	
	$rs->MoveFirst();
	
	?>
	<div class="subContent">
		<div class="tableContainer">
			<table class="dataTable">
				<thead>
					<tr>
						<th class="filledCell" style="text-align:center; width:25%;">France TTC</th>
						<th class="filledCell" style="text-align:center; width:25%;">France HT</th>
						<th class="filledCell" style="text-align:center; width:25%;">Intra-communautaire</th>
						<th class="filledCell" style="text-align:center; width:25%;">Export</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $nationalATI ) ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $nationalET ) ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $intra ) ?></td>
						<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $export ) ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>	
	<div class="subContent">
		<div class="tableContainer">
			<table class="dataTable resultTable">
				<thead>
					<tr>
						<th style="width:10px;"></th>
						<th>Facture n°</th>
						<th>Règlements n°</th>
						<th>Date de paiement</th>
						<th>Mode de paiement</th>
						<th>Montant</th>
					</tr>
				</thead>
				<tbody>
				<?php 
				
					$amountSum = 0.0;
					
					while( !$rs->EOF ){ 
						
						?>
						<tr>
							<td class="lefterCol">
							<?php
							
								if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] ){
									
									?>
									<input type="checkbox" name="declare[]" value="<?php echo htmlentities( $rs->fields( "billing_supplier" ) ) . "," . $rs->fields( "idsupplier" ); ?>" />
									<?php
									
								}
							
							?>
							</td>
							<td><?php echo htmlentities( $rs->fields( "billing_supplier" ) );?></td>
							<td><?php echo implode( "<br />", explode( ",", $rs->fields( "idregulations_supplier" ) ) ); ?></td>
							<td><?php echo Util::dateFormatEu( $rs->fields( "bank_date" ) ) ?></td>
							<td><?php echo htmlentities( $rs->fields( "payment" ) ) ?></td>
							<td class="righterCol" style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "amount" ) ); ?></td>
						</tr>
						<?php 
						
						$amountSum += $rs->fields( "amount" );
						
						$rs->MoveNext(); 
						
					} 
				
					?>
					<tr>
						<td colspan="5" style="border-style:none"></td>
						<td class="lefterCol righterCol" style="text-align:right;"><b><?php echo Util::priceFormat( $amountSum ); ?></b></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<?php
	*/
}

//------------------------------------------------------------------------------------------------

function getIntraInvoices(){
	
	global $month, $year;
	
	$lang = User::getInstance()->getLang();

	$query = "
	SELECT billing_supplier.billing_supplier,
		billing_supplier.idsupplier,
		billing_supplier.total_amount,
		billing_supplier.Date AS billing_date,
		billing_supplier.creation_date,
		supplier.name
	FROM account_plan,
		billing_supplier,
		supplier
	WHERE account_plan.idaccount_plan IN( SELECT idaccounting_plan FROM accounting_plan WHERE accounting_plan_use = 1 )
	AND account_plan.action = 'supplier_invoices'
	AND account_plan.record_type = 'billing_supplier'
	AND account_plan.code_eu = 'INTRA'
	AND account_plan.idaccount_record = billing_supplier.billing_supplier
	AND billing_supplier.idsupplier = supplier.idsupplier
	AND account_plan.idaccount_record = billing_supplier.billing_supplier";
	
	if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] )
		$query .= " AND billing_supplier.vat_declaration_date LIKE '0000-00-00'";
	else $query .= " AND billing_supplier.vat_declaration_date LIKE '" . stripslashes( $_POST[ "month" ] ) . "%'";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures intra-communautaires", E_USER_ERROR );

	?>
	<div class="headTitle">
		<p class="title">Factures intra-communautaires</p>
	</div>
	<form action="vat.php" method="post">
		<div class="subContent">
			<div class="tableContainer">
				<table class="dataTable resultTable">
					<thead>
						<tr>
							<th></th>
							<th style="width:20%;">Facture n°</th>
							<th style="width:30%;">Nom</th>
							<th style="width:20%;">Date de facture</th>
							<th style="width:20%;">Date d'enregistrement</th>
							<th style="width:15%;">Montant</th>
						</tr>
					</thead>
					<tbody>
					<?php 
					
						$totalAmountSum = 0.0;
						
						while( !$rs->EOF ){ 
						
						?>
						<tr>
							<td class="lefterCol">
							<?php
							
								if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] ){
									
									?>
									<input type="checkbox" name="declare[]" value="<?php echo htmlentities( $rs->fields( "billing_supplier" ) ) . "," . $rs->fields( "idsupplier" ); ?>" />
									<?php
									
								}
								
							?>
							</td>
							<td><?php echo $rs->fields( "billing_supplier" ) ?></td>
							<td><a href="#" onclick="showSupplier(<?php echo $rs->fields( "idsupplier" ) ?>; return false;"><?php echo $rs->fields( "name" ) ?></a></td>
							<td><?php echo Util::dateFormatEu( $rs->fields( "billing_date" ) ) ?></td>
							<td><?php echo Util::dateFormatEu( $rs->fields( "creation_date" ) ) ?></td>
							<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
						</tr>
						<?php 
						
							$totalAmountSum += $rs->fields( "total_amount" );
							
							$rs->MoveNext(); 
						
						} 
						
						?>
						<tr>
							<td colspan="5" style="border-style:none;"></td>
							<td class="lefterCol righterCol" style="text-align:right;"><b><?php echo Util::priceFormat( $totalAmountSum ); ?></b></td>
						</tr>
					</tbody>
				</table>
				<?php
				
					if( !isset( $_POST[ "declared" ] ) || !$_POST[ "declared" ] ){
						
						?>
						<p style="margin-top:4px;">
							<input type="submit" class="blueButton" value="TVA déclarée" />
						</p>
						<?php
						
					}
					
				?>
			</div>
		</div>
	</form>
	<?php
	
}

//---------------------------------------------------------------------------------------------

function generateMonthsSelect( $onchange = false ){
	
	echo "<select name=\"month\" class=\"fullWidth\">";
	
	$month = date( "m" );
	$year = date( "Y" );
	
	$months = array(
		
		1 => "Janvier",
		2 => "Février",
		3 => "Mars",
		4 => "Avril",
		5 => "Mai",
		6 => "Juin",
		7 => "Juillet",
		8 => "Août",
		9 => "Septembre",
		10 => "Octobre",
		11 => "Novembre",
		12 => "Décembre"
		
	);
	
	for( $i = 0, $j = $month ; $i < 12 ;  $i++, $j-- ){
		
		if( $j == 0 ){
			
			$j = 12;
			$year--;
			
		}
		
		$value = "$year-" . sprintf( "%02d", $j );
		
		echo "<option value=\"$value\"";
		
		if( isset( $_POST[ "month" ] ) && $_POST[ "month" ] == $value )
			echo " selected=\"selected\"";
		
		echo ">".$months[ $j ] . " $year</option>\n";
		
	}
	
	echo "</select>";
	
}

//--------------------------------------------------------------------------------

?>