<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Export avoirs clients
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/CSVCreditExport.php" );
include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );


if( !User::getInstance()->getId() || !isset( $_GET[ "query" ] ) )
	exit();

global $GLOBAL_DB_PASS;

$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "query" ] ), $GLOBAL_DB_PASS );

$rs =& DBUtil::query( $query );

$idcredits = array();
while( !$rs->EOF() ){
	
	$idcredits[] = $rs->fields( "idcredit" );
	
	$rs->MoveNext();
		
}

if( !$rs->RecordCount() )
	exit();

$date = date( "Y_m_d" );
$filename = "export_avoirs_$date";

$csvInvoiceExport = new CSVCreditExport( $filename );
$csvInvoiceExport->setTextDelimiter( "" );
$csvInvoiceExport->setSeparator( ";" );
$csvInvoiceExport->setBreakLine( "\n" );
$csvInvoiceExport->setCredits( $idcredits );
$csvInvoiceExport->export();

?>