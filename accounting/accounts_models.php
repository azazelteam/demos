<?php
// header( "Content-Type: text/html; charset=utf-8" );
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Création de modèles d'écritures comptables
 
 */

//---------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

//---------------------------------------------------------------------
// ajax enregistrement modèle

if( isset( $_POST[ "action" ] ) && $_POST[ "action" ] == "creation" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	create();

	exit();

}

//---------------------------------------------------------------------
// ajax modification modèle

if( isset( $_POST[ "action" ] ) && $_POST[ "action" ] == "modification" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	modify();

	exit();

}

//---------------------------------------------------------------------
// ajax liste déroulante des numéros de compte

if( isset( $_GET[ "getAccountsList" ] ) && isset( $_GET[ "row" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	accountsSelect( $_GET[ "row" ] );

	exit();

}

//---------------------------------------------------------------------
// ajax liste déroulante des types de pièces

if( isset( $_GET[ "getRecordTypeList" ] ) && isset( $_GET[ "row" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	recordTypesSelect( $_GET[ "row" ] );

	exit();

}

//---------------------------------------------------------------------
// ajax liste déroulante des types de tiers

if( isset( $_GET[ "getThirdPartyTypeList" ] ) && isset( $_GET[ "row" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	thirdPartyTypesSelect( $_GET[ "row" ] );

	exit();

}

//---------------------------------------------------------------------
// ajax liste déroulante des modes de paiement

if( isset( $_GET[ "getPaymentList" ] ) && isset( $_GET[ "row" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	PaymentSelect( $_GET[ "row" ] );

	exit();

}

//---------------------------------------------------------------------
// ajax récupération formulaire de création de modèle

if( isset( $_GET[ "getCreationForm" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	getModelForm();

	exit();

}

//---------------------------------------------------------------------
// ajax récupération table de création de modèle

if( isset( $_GET[ "getCreationTable" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	getModelTable();

	exit();

}

//---------------------------------------------------------------------
// ajax récupération formulaire de modification de modèle

if( isset( $_GET[ "getModificationForm" ] ) ){

	include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	if( isset( $_GET[ "idmodel" ] ) )
		getModelForm( $_GET[ "idmodel" ] );
	else
		XHTMLFactory::createSelectElement( "accounts_models", "idmodel", "name", $orderByColumn = false, $selectedValue = false, $nullValue = true, $nullInnerHTML = "-", $attributes = ' onchange="getModificationForm(this.value);"', $returnHtml = false, $ignoreValue = false );

	exit();

}

//---------------------------------------------------------------------
// ajax récupération table de modification de modèle

if( isset( $_GET[ "getModificationTable" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	if( isset( $_GET[ "idmodel" ] ) )
		getModelTable( $_GET[ "idmodel" ] );
	else
		XHTMLFactory::createSelectElement( "accounts_models", "idmodel", "name", $orderByColumn = false, $selectedValue = false, $nullValue = true, $nullInnerHTML = "-", $attributes = ' onchange="getModificationForm(this.value);"', $returnHtml = false, $ignoreValue = false );

	exit();

}

//---------------------------------------------------------------------
// ajax ajout d'une ligne au tableau

if( isset( $_GET[ "newTableRow" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	// header( "Content-Type: text/html; charset=utf-8" );

	getTableRow( $_GET[ "rows" ] + 1 );

	exit();

}

//---------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/objects/AccountModelParser.php" );

//---------------------------------------------------------------------

$Title = "Modèles d'écritures comptables";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//---------------------------------------------------------------------

?>
<style type="text/css">
<!--

	.ui-draggable{
		cursor:move;
	}

	.valueTags{
		padding:1px;
	}

	div.blockUI input.textInput,
	div.blockUI input#txt_account_plan_1{
		border:1px solid #AAAAAA;
		color:#44474E;
		font-family:Arial, Helvetica, sans-serif;
		font-size:11px;
		width:100%;
	}

	div.labelColumn{
		text-align:left;
	}

-->
</style>
<script type="text/javascript">
<!--

	$(function(){

		$('#dialog').draggable({ cancel: 'img' });

		$('.draggable').draggable({
			helper: 'clone',
			cursor: 'move'
		});

		$('#modelValuesForm').ajaxForm({
			success: function( response ){
				if(response != '')
					$.growlUI( '', response );
				else
					$.growlUI( '', 'Enregistrement réussi' );
			}
		});

	});

	/*********************************************************************/
	/* Affiche le formulaire de création de modèle                       */
	/*********************************************************************/

	function getCreationForm(){

		$('#dialog').css('display','block');
		$('#tableContainer').html('');
		$('#action').val('creation');
		$('#rowCount').val(1);

		$.ajax({
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php',
			data: 'getCreationForm',
			error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
			success: function( response ){
				$('#modelMainInfos').html(response);
			}
		});
		
		$.ajax({
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php',
			data: 'getCreationTable',
			error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
			success: function( response ){
				$('#tableContainer').html(response);
			}
		});
		
	}
	
	/*********************************************************************/
	/* Affiche le formulaire de modification de modèle                   */
	/*********************************************************************/
	
	function getModificationForm( idmodel ){
		
		$('#dialog').css('display','block');
		$('#tableContainer').html('');
		$('#action').val('modification');
		$('#rowCount').val(1);
		$('#idmodel').val(idmodel);
		
		var formData = 'getModificationForm';
		var tableData = 'getModificationTable';
		
		if( idmodel != undefined ){
			formData += '&idmodel=' + idmodel;
			tableData += '&idmodel=' + idmodel;
		}
		
		$.ajax({
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php',
			data: formData,
			error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
			success: function( response ){
				$('#modelMainInfos').html(response);
			}
		});
		
		if( idmodel != undefined )
			$.ajax({
				url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php',
				data: tableData,
				error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
				success: function( response ){
					$('#tableContainer').html(response);
				}
			});
		
	}
	
	/*********************************************************************/
	/* Ajouter une ligne au tableau                                      */
	/*********************************************************************/
	
	function addRow(){
		
		$.ajax({
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php',
			data: 'newTableRow&rows=' + $('#rowCount').val(),
			error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
			success: function( response ){
				$('#rowCount').val(parseInt($('#rowCount').val())+1);
				$('#table').children('tbody').append(response);
			}
		});
		
	}
	
-->
</script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/small_black_spinner.gif" id="spinner" style="display:none; margin:auto;" />
<form method="post" action="" id="modelValuesForm" onsubmit="$('.labelEditor').each(function(){ $('#label_'+$(this).parent('td').parent('tr').attr('row')).val($(this).html()); });">
	<input type="hidden" name="action" id="action" value="" />
	<input type="hidden" name="idmodel" id="idmodel" value="" />
	<div id="globalMainContent">
		<div class="mainContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle">
					<p class="title">Modèles d'écritures comptables</p>
				</div>
				<div class="subContent" id="modelMainInfos"></div>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
		<div id="tools" class="rightTools">
			<div class="toolBox">
				<div class="header">
					<div class="topRight"></div><div class="topLeft"></div>
					<p class="title">Infos</p>
				</div>
				<div class="content">
					<div style="padding:6px 6px 0 6px;">
						<a href="#" onclick="getCreationForm(); return false;">Créer un modèle</a><br />
						<a href="#" onclick="getModificationForm(); return false;">Modifier un modèle</a>
					</div>
					<div class="subTitleContainer"></div>
					<div style="padding:6px 6px 0 6px;">
						<a href="#" onclick="$('#dialog').css('display','block'); return false;">Boîte à outils</a>
					</div>
				</div>
				<div class="footer">
					<div class="bottomLeft"></div>
					<div class="bottomMiddle"></div>
					<div class="bottomRight"></div>
				</div>
			</div>
			<div class="toolBox" id="dialog" style="display:none; position:absolute;">
				<div class="header" style="cursor:move;">
					<div class="topRight"></div><div class="topLeft"></div>
					<p class="title">Outils</p>
				</div>
				<div class="content">
					<div style="padding:6px 6px 0 6px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/manual.png" alt="Manuel" title="Saisie manuelle" class="draggable manualImage" tag="%manual%" />
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/copy.png" alt="Copier" title="Copier ligne précédente" class="draggable copyImage" tag="%copy%" />
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.jpg" alt="Date" title="Date du jour" class="draggable todayImage" tag="%today%" />
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calculator.png" alt="Calcul" title="Calcul" class="draggable calculateImage" tag="%calculate%" />
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/balance.png" alt="Balance" title="Balance" class="draggable balanceImage" tag="%balance%" />
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/stop.png" alt="Effacer" title="Effacer" class="draggable deleteImage" tag="delete" style="float:right;" />
					</div>
					<div class="subTitleContainer"></div>
					<div style="padding:6px 6px 0 6px; width:100px;">
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "n° comtpe" ) ) ?>" alt="" title="" id="accountImage" class="valueTags" tag="%account%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "n° pièce" ) ) ?>" alt="" title="" id="recordImage" class="valueTags" tag="%record%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "type pièce" ) ) ?>" alt="" title="" id="recordTypeImage" class="valueTags" tag="%record_type%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "n° tiers" ) ) ?>" alt="" title="" id="thirdPartyImage" class="valueTags" tag="%third_party%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "type tiers" ) ) ?>" alt="" title="" id="thirdPartyTypeImage" class="valueTags" tag="%third_party_type%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "date" ) ) ?>" alt="" title="" id="dateImage" class="valueTags" tag="%date%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "date paiement" ) ) ?>" alt="" title="" id="paymentDateImage" class="valueTags" tag="%payment_date%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "mode paiement" ) ) ?>" alt="" title="" id="paymentImage" class="valueTags" tag="%payment%" /><br />
						<img src="<?php echo $GLOBAL_START_URL ?>/administration/images/tag.php?label=<?php echo urlencode( htmlentities( "montant" ) ) ?>" alt="" title="" id="amountImage" class="valueTags" tag="%amount%" />
					</div>
				</div>
				<div class="footer">
					<div class="bottomLeft"></div>
					<div class="bottomMiddle"></div>
					<div class="bottomRight"></div>
				</div>
			</div>
		</div>
		<div id="tableContainer"></div>
	</div>
</form>
<?php

//---------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//---------------------------------------------------------------------

function getModelForm( $idmodel = false ){

	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );

	$modelValues = array(
		"name"			=> "",
		"iddiary_code"	=> false
	);

	if( $idmodel ){

		$rs =& DBUtil::query( "SELECT * FROM accounts_models WHERE idmodel = $idmodel" );

		if( $rs === false )
			trigger_error( "Impossible de récupérer les informations du modèle", E_USER_ERROR );

		$modelValues = array(
			"name"			=> $rs->fields( "name" ),
			"iddiary_code"	=> $rs->fields( "iddiary_code" )
		);

		$rowCount = & DBUtil::query( "SELECT COUNT( * ) AS rowCount FROM accounts_models_row WHERE idmodel = $idmodel" );

		$rowCount = $rowCount->fields( "rowCount" );

	}

?>
<input type="hidden" name="rowCount" id="rowCount" value="<?php echo isset( $rowCount ) ? $rowCount : 1 ?>" />
<table>
	<tr>
		<td>Nom&nbsp;:</td>
		<td><input type="text" value="<?php echo $modelValues[ "name" ] ?>" name="name" id="name" maxlenght="255" class="textInput" /></td>
	</tr>
	<tr>
		<td>Code journal&nbsp;:</td>
		<td><?php echo XHTMLFactory::createSelectElement( "diary_code", "iddiary_code", "diary_code", $orderByColumn = false, $selectedValue = $modelValues[ "iddiary_code" ], $nullValue = "", $nullInnerHTML = "", $attributes = ' name="iddiary_code" id="iddiary_code"', $returnHtml = false, $ignoreValue = false ) ?></td>
	</tr>
</table>
<?php

}

//---------------------------------------------------------------------

function getModelTable( $idmodel = false ){

?>
<div class="mainContent fullWidthContent">
	<div class="topRight"></div><div class="topLeft"></div>
	<div class="content">
		<div class="headTitle">
			<p class="title">&Eacute;critures comptables</p>
		</div>
		<div class="subContent" id="modelMainInfos">
			<div class="tableContainer">
				<table class="dataTable devisTable" id="table">
					<thead>
						<tr>
							<th style="background-color:transparent; border-style:none; width:16px;"></th>
							<th>Numéro de compte</th>
							<th>Numéro de pièce</th>
							<th>Type de pièce</th>
							<th>Numéro de tiers</th>
							<th>Type de tiers</th>
							<th style="min-width:150px;">Libellé</th>
							<th>Date</th>
							<th>Date de paiement</th>
							<th>Mode de paiement</th>
							<th>Débit</th>
							<th>Crédit</th>
						</tr>
					</thead>
					<tbody>
						<?php

							if( $idmodel === false )
								getTableRow( 1, $idmodel );

							else{

								$rs =& DBUtil::query( "SELECT * FROM accounts_models_row WHERE idmodel = $idmodel ORDER BY idrow ASC" );

								while( !$rs->EOF ){

									getTableRow( $rs->fields( "idrow" ), $idmodel );

									$rs->MoveNext();

								}

							}

						?>
					</tbody>
				</table>
			</div>
			<div class="submitButtonContainer">
				<input type="submit" value="Enregistrer" class="blueButton" />
			</div>
		</div>
	</div>
	<div class="bottomRight"></div><div class="bottomLeft"></div>
</div>
<div id="calculation" style="display:none;">
	<input type="hidden" name="calculationRow" id="calculationRow" />
	<input type="text" name="calculationValue" id="calculationValue" onkeyup="this.value = this.value.replace(/[^0-9/*\-+().%]+/g, '');" class="textInput" />
	<input type="button" value="Enregistrer" onclick="$('#amount_'+$('#calculationRow').val()).val('$('+$(this).parent().children('input:text').val()+')'); $.unblockUI();" class="orangeButton" style="float:right; margin:5px 0;" />
	<input type="button" value="Annuler" onclick="$.unblockUI();" class="blueButton" style="float:right; margin:5px;" />
	<div style="border-top:1px solid #AAAAAA; clear:both; margin-top:35px; padding-top:10px;">
		Pour représenter les valeurs des champs précédents, utiliser la synthaxe <strong>%n%</strong> où <strong>n</strong> est le numéro de la ligne de la valeur.<br style="margin-bottom:5px;" />
		<u>Exemple&nbsp;:</u> <i>%1%*1.196+%2%</i><br />
		Le montant calculé sera égal à 1.196 fois le montant de la première ligne plus le montant de la deuxième
	</div>
</div>
<?php

}

//---------------------------------------------------------------------

function getTableRow( $row = 1, $idmodel = false ){

	global $GLOBAL_START_PATH, $GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/AccountModelParser.php" );

	$modelValues = array();
	$modelInnerHTML = array();

	if( $idmodel !== false ){

		$model = new AccountModelParser( $idmodel, 1 );

		$rs =& DBUtil::query( "SELECT * FROM accounts_models_row WHERE idmodel = $idmodel AND idrow = $row ORDER BY idrow ASC" );

		if( $rs === false )
			trigger_error( "Impossible de récupérer les informations du modèle", E_USER_ERROR );

		$modelValues = array(
			"account"			=> $rs->fields( "idaccount_plan" ),
			"record"			=> $rs->fields( "idrecord" ),
			"record_type"		=> $rs->fields( "record_type" ),
			"third_party"		=> $rs->fields( "idrelation" ),
			"third_party_type"	=> $rs->fields( "relation_type" ),
			"label"				=> $rs->fields( "label" ),
			"date"				=> $rs->fields( "date" ),
			"payment_date"		=> $rs->fields( "payment_date" ),
			"payment"			=> $rs->fields( "payment" ),
			"amount"			=> $rs->fields( "amount" ),
			"amount_type"		=> $rs->fields( "amount_type" )
		);

		$modelInnerHTML = array(
			"account"			=> $model->parseAccountEdition( $rs->fields( "idaccount_plan" ) ),
			"record"			=> $model->parseRecordEdition( $rs->fields( "idrecord" ) ),
			"record_type"		=> $model->parseRecordTypeEdition( $rs->fields( "record_type" ) ),
			"third_party"		=> $model->parseThirdPartyEdition( $rs->fields( "idrelation" ) ),
			"third_party_type"	=> $model->parseThirdPartyTypeEdition( $rs->fields( "relation_type" ) ),
			"label"				=> $model->parseLabelEdition( $rs->fields( "label" ) ),
			"date"				=> $model->parseDateEdition( $rs->fields( "date" ) ),
			"payment_date"		=> $model->parsePaymentDateEdition( $rs->fields( "payment_date" ) ),
			"payment"			=> $model->parsePaymentEdition( $rs->fields( "payment" ) ),
			"debit"				=> $model->parseDebitEdition( $rs->fields( "amount" ), $rs->fields( "amount_type" ) ),
			"credit"			=> $model->parseCreditEdition( $rs->fields( "amount" ), $rs->fields( "amount_type" ) )
		);

	}

?>
						<tr row="<?php echo $row ?>" style="height:32px;">
							<th style="background-color:transparent; border-style:none;">
								<script type="text/javascript">
								<!--

									function dropAmount(event,ui){

										var dragged = ui.draggable;
										var cell = $(this);

										$(cell).css('background-color','transparent');

										var row = $(cell).parent('tr').attr('row');
										var index = $('cell', $(cell).parent('tr')).index(cell);
										var name = $(cell).attr('class').match(/([a-z_]+)Column/)[1];
										var tag = dragged.attr('tag');
										var amount = $('#amount_' + row);
										var amount_type = $('#amount_type_' + row);

										dropDefault(event,ui,$(this));

										if(tag == '%balance%'){
											$(amount).val(tag);
											$(amount_type).val('');
											$(cell).parent('tr').children('.creditColumn').html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/balance.png" alt="Balance" title="Balance" class="calculateImage" />');
											$(cell).parent('tr').children('.debitColumn').html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/balance.png" alt="Balance" title="Balance" class="calculateImage" />');
										}
										else if(tag == 'delete'){
											$(amount).val('');
											$(amount_type).val('');
											$(cell).parent('tr').children('.creditColumn').html('');
											$(cell).parent('tr').children('.debitColumn').html('');
										}
										else{
											$(amount).val(tag);
											$(amount_type).val(name);

											if(name == 'debit')
												$(cell).parent('tr').children('.creditColumn').html('');
											else
												$(cell).parent('tr').children('.debitColumn').html('');
										}

										if(tag == '%calculate%')
											$('.calculateImage').dblclick(function(){
												$('#calculationRow').val(row);

												var value = $('#amount_'+row).val();
												if(value != '%calculate%')
													$('#calculationValue').val(value.substring(2, value.length - 1));
												else
													$('#calculationValue').val('');

												$.blockUI({
													message: $('#calculation'),
													css: {
														'color': '#586065',
														cursor: 'default',
														'font-family': 'Arial, Helvetica, sans-serif',
														'font-size': '12px',
														'font-weight': 'normal',
														'padding': '15px 10px 10px 5px',
														'text-align': 'left',
														'-webkit-border-radius': '10px',
														'-moz-border-radius': '10px'
													},
													fadeIn: 0,
													fadeOut: 700
												});

												$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');
												$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);
											});

									}

									function dropDefault(event,ui,cell){

										var dragged = ui.draggable;
										if(cell == undefined) var cell = $(this);

										$(cell).css('background-color','transparent');

										var row = $(cell).parent('tr').attr('row');
										var index = $('cell', $(cell).parent('tr')).index(cell);
										var name = $(cell).attr('class').match(/([a-z_]+)Column/)[1];
										var tag = dragged.attr('tag');

										if( parseInt(row) != row )		return false;
										if( parseInt(index) != index )	return false;
										if( name == '' )				return false;
										if( tag == undefined )			return false;

										if($(dragged).hasClass('deleteImage')){
											$(cell).html('');
											$('#' + name + '_' + row).val('');
										}
										else{
											$(cell).html(dragged.clone().removeClass('ui-draggable'));
											$('#' + name + '_' + row).val(tag);
										}

									}

									//-------------------------------------------------------------

									$('.accountColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#account_' + row);

										$(cell).html($('#spinner').clone().css('display','block'));
										$(input).val('');

										$.ajax({
											type: 'GET',
											url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php?getAccountsList&row='+row,
											error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
											success: function( response ){
												$(cell).html( response );
											}
										});
									});

									$('.recordColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#record_' + row);

										if($(this).children('input:text').length){
											$(this).html($(this).children('input:text').clone().css('display','inline'));
											$('#record_input_'+row).focus();
										}
										else{
											$(cell).html('<input type="text" name="record_input_'+row+'" id="record_input_'+row+'" value="" onkeyup="$(\'#record_'+row+'\').val(this.value);" onblur="$(this).parent().append(this.value); $(this).css(\'display\',\'none\')" class="textInput" />');
											$('#record_input_'+row).focus();
											$(input).val('');
										}

									});

									$('.record_typeColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#record_type_' + row);

										$(cell).html($('#spinner').clone().css('display','block'));
										$(input).val('');

										$.ajax({
											type: 'GET',
											url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php?getRecordTypeList&row='+row,
											error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
											success: function( response ){
												$(cell).html( response );
											}
										});
									});

									$('.third_partyColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#third_party_' + row);

										if($(this).children('input:text').length){
											$(this).html($(this).children('input:text').clone().css('display','inline'));
											$('#third_party_input_'+row).focus();
										}
										else{
											$(cell).html('<input type="text" name="third_party_input_'+row+'" id="third_party_input_'+row+'" value="" onkeyup="$(\'#third_party_'+row+'\').val(this.value);" onblur="$(this).parent().append(this.value); $(this).css(\'display\',\'none\');" class="textInput" />');
											$('#third_party_input_'+row).focus();
											$(input).val('');
										}

									});

									$('.third_party_typeColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#third_party_type_' + row);

										$(cell).html($('#spinner').clone().css('display','block'));
										$(input).val('');

										$.ajax({
											type: 'GET',
											url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php?getThirdPartyTypeList&row='+row,
											error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
											success: function( response ){
												$(cell).html( response );
											}
										});
									});

									$('.labelColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#label_' + row);

										if($(this).children('.labelEditor').length){
											$(this).html($(this).children('.labelEditor').clone().css('display','block'));
											$(cell).children('.labelEditor').focus();
										}
										else{
											$(cell).html('<div contenteditable="true" style="border:1px solid #AAAAAA; height:auto; min-height:13px; padding:2px;" id="labelEditor_'+row+'" class="labelEditor" onkeyup="$(\'#label_'+row+'\').val(this.innerHTML);"></div>');
											$(cell).children('.labelEditor').focus();
											$(input).val('');
										}

									});

									$('.dateColumn,.payment_dateColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var name = $(cell).attr('class').match(/([a-z_]+)Column/)[1];

										if($(this).hasClass('.dateColumn')){
											$(cell).html('<input type="text" name="date_input_'+row+'" id="date_input_'+row+'" value="'+$(cell).html()+'" class="calendarInput" />');
											var field = $('#date_' + row);
											var input = $('#date_input_' + row);
										}
										else{
											$(cell).html('<input type="text" name="payment_date_input_'+row+'" id="payment_date_input_'+row+'" value="'+$(cell).html()+'" class="calendarInput" />');
											var field = $('#payment_date_' + row);
											var input = $('#payment_date_input_' + row);
										}

										$(input).datepicker({
											changeMonth: true,
											changeYear: true,
											dateFormat: 'dd-mm-yy',
											showAnim: 'fadeIn',
											showOn: 'focus',
											onClose: function(){
												$(cell).html($(this).val());
												$(field).val($(this).val());
											}
										});

										$(input).datepicker('show');

									});

									$('.paymentColumn').dblclick(function(){
										var cell = $(this);
										var row = $(cell).parent('tr').attr('row');
										var input = $('#payment_' + row);

										$(cell).html($('#spinner').clone().css('display','block'));
										$(input).val('');

										$.ajax({
											type: 'GET',
											url: '<?php echo $GLOBAL_START_URL ?>/accounting/accounts_models.php?getPaymentList&row='+row,
											error: function( XMLHttpRequest, textStatus ){ $.growlUI( 'Une erreur est survenue', textStatus ); },
											success: function( response ){
												$(cell).html( response );
											}
										});
									});

									$('.calculateImage').dblclick(function(){
										var row = $(this).parent('td').parent('tr').attr('row');

										$('#calculationRow').val(row);
										var value = $('#amount_'+row).val();

										if(value != '%calculate%')
											$('#calculationValue').val(value.substring(2, value.length - 1));
										else
											$('#calculationValue').val('');

										$.blockUI({
											message: $('#calculation'),
											css: {
												'color': '#586065',
												cursor: 'default',
												'font-family': 'Arial, Helvetica, sans-serif',
												'font-size': '12px',
												'font-weight': 'normal',
												'padding': '15px 10px 10px 5px',
												'text-align': 'left',
												'-webkit-border-radius': '10px',
												'-moz-border-radius': '10px'
											},
											fadeIn: 0,
											fadeOut: 700
										});

										$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');
										$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);
									});

									//-------------------------------------------------------------

									$('.accountColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.accountColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.recordColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.recordColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.record_typeColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.record_typeColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.third_partyColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.third_partyColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.third_party_typeColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.third_party_typeColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.labelColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.labelColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.dateColumn').droppable({
										accept: '.manualImage,.deleteImage,.todayImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.dateColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.todayImage,.copyImage');

									$('.payment_dateColumn').droppable({
										accept: '.manualImage,.deleteImage,.todayImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.payment_dateColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.todayImage,.copyImage');

									$('.paymentColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropDefault
									});

									$('.paymentColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage');

									$('.debitColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropAmount
									});

									$('.debitColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage,.calculateImage,.balanceImage');

									$('.creditColumn').droppable({
										accept: '.manualImage,.deleteImage',
										over: function(){ $(this).css('background-color','#EAEAEA'); },
										out: function(){ $(this).css('background-color','transparent'); },
										drop: dropAmount
									});

									$('.creditColumn:gt(0)').droppable('option','accept','.manualImage,.deleteImage,.copyImage,.calculateImage,.balanceImage');

								-->
								</script>
								<a href="#" onclick="addRow(); return false;" class="addRowLink"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="+" /></a>
								<input type="hidden" name="account_<?php echo $row ?>" id="account_<?php echo $row ?>" value="<?php echo $modelValues[ "account" ] ?>" />
								<input type="hidden" name="record_<?php echo $row ?>" id="record_<?php echo $row ?>" value="<?php echo $modelValues[ "record" ] ?>" />
								<input type="hidden" name="record_type_<?php echo $row ?>" id="record_type_<?php echo $row ?>" value="<?php echo $modelValues[ "record_type" ] ?>" />
								<input type="hidden" name="third_party_<?php echo $row ?>" id="third_party_<?php echo $row ?>" value="<?php echo $modelValues[ "third_party" ] ?>" />
								<input type="hidden" name="third_party_type_<?php echo $row ?>" id="third_party_type_<?php echo $row ?>" value="<?php echo $modelValues[ "third_party_type" ] ?>" />
								<input type="hidden" name="label_<?php echo $row ?>" id="label_<?php echo $row ?>" value="<?php echo htmlentities( $modelValues[ "label" ] ) ?>" />
								<input type="hidden" name="date_<?php echo $row ?>" id="date_<?php echo $row ?>" value="<?php echo $modelValues[ "date" ] ?>" />
								<input type="hidden" name="payment_date_<?php echo $row ?>" id="payment_date_<?php echo $row ?>" value="<?php echo $modelValues[ "payment_date" ] ?>" />
								<input type="hidden" name="payment_<?php echo $row ?>" id="payment_<?php echo $row ?>" value="<?php echo $modelValues[ "payment" ] ?>" />
								<input type="hidden" name="amount_<?php echo $row ?>" id="amount_<?php echo $row ?>" value="<?php echo $modelValues[ "amount" ] ?>" />
								<input type="hidden" name="amount_type_<?php echo $row ?>" id="amount_type_<?php echo $row ?>" value="<?php echo $modelValues[ "amount_type" ] ?>" />
							</th>
							<td class="lefterCol accountColumn"><?php echo $modelInnerHTML[ "account" ] ?></td>
							<td class="recordColumn"><?php echo $modelInnerHTML[ "record" ] ?></td>
							<td class="record_typeColumn"><?php echo $modelInnerHTML[ "record_type" ] ?></td>
							<td class="third_partyColumn"><?php echo $modelInnerHTML[ "third_party" ] ?></td>
							<td class="third_party_typeColumn"><?php echo $modelInnerHTML[ "third_party_type" ] ?></td>
							<td class="labelColumn"><?php echo $modelInnerHTML[ "label" ] ?></td>
							<td class="dateColumn"><?php echo $modelInnerHTML[ "date" ] ?></td>
							<td class="payment_dateColumn"><?php echo $modelInnerHTML[ "payment_date" ] ?></td>
							<td class="paymentColumn"><?php echo $modelInnerHTML[ "payment" ] ?></td>
							<td class="debitColumn"><?php echo $modelInnerHTML[ "debit" ] ?></td>
							<td class="righterCol creditColumn"><?php echo $modelInnerHTML[ "credit" ] ?></td>
						</tr>
<?php

}

//---------------------------------------------------------------------

function accountsSelect( $row = 1, $value = false ){

	$lang = User::getInstance()->getLang();

?>
    <select name="account_plan_select_<?php echo $row ?>" id="account_plan_select_<?php echo $row ?>" onchange="$('#account_<?php echo $row ?>').val(this.value);" onblur="$(this).parent().append(this.value); this.style.display='none';" class="fullWidth">
		<option value=""></option>
		<?php

		$rs =& DBUtil::query( "SELECT idaccounting_plan, accounting_plan$lang AS description, accounting_plan_parent FROM accounting_plan ORDER BY idaccounting_plan ASC" );

		while( !$rs->EOF() ){

			$rs2 =& DBUtil::query( "SELECT COUNT( * ) AS children FROM accounting_plan WHERE accounting_plan_parent = '" . $rs->fields( "idaccounting_plan" ) . "'" );

			$bold = $rs->fields( "accounting_plan_parent" ) == 0 ? " style=\"font-weight:bold;\"" : "";
			$selected = $value == false || $value != $rs->fields( "idaccounting_plan" ) ? "" : ' selected="selected"';
			$idaccounting_plan = $rs2->fields( "children" ) > 0 ? $rs->fields( "idaccounting_plan" ) . "%" : $rs->fields( "idaccounting_plan" );

			echo "    <option value=\"$idaccounting_plan\"$bold$selected>" . $rs->fields( "idaccounting_plan" ) . " - " . $rs->fields( "description" ) . "</option>\n";

			$rs->MoveNext();

		}

		?>
	</select>
<?php

}

//---------------------------------------------------------------------

function recordTypesSelect( $row = 1, $value = false ){

	$lang = User::getInstance()->getLang();

?>
    <select name="record_type_select_<?php echo $row ?>" id="record_type_select_<?php echo $row ?>" onchange="$('#record_type_<?php echo $row ?>').val(this.value);" onblur="$(this).parent().append(this[selectedIndex].innerHTML); this.style.display='none';" class="fullWidth">
		<option value=""></option>
		<option value="billing_buyer">Facture client</option>
		<option value="regulations_buyer">Règlement client</option>
		<option value="billing_supplier">Facture frs</option>
		<option value="regulations_supplier">Règlement frs</option>
		<option value="credits">Avoir client</option>
		<option value="credit_supplier">Avoir frs</option>
	</select>
<?php

}

//---------------------------------------------------------------------

function thirdPartyTypesSelect( $row = 1, $value = false ){

	global $GLOBAL_START_PATH;

	include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );

	$lang = User::getInstance()->getLang();

	$rs =& DBUtil::query( "SELECT relation_type FROM account_plan WHERE relation_type != '' GROUP BY relation_type ORDER BY COUNT( relation_type ) DESC" );

	?><select name="third_party_type_select_<?php echo $row ?>" id="third_party_type_select_<?php echo $row ?>" onchange="$('#third_party_type_<?php echo $row ?>').val(this.value);" onblur="$(this).parent().append(this[selectedIndex].innerHTML); this.style.display='none';" class="fullWidth">
		<option value=""></option>
	<?php

	while( !$rs->EOF ){

?>
		<option value="<?php echo $rs->fields( "relation_type" ) ?>"><?php echo Dictionnary::translate( "account_model_" . $rs->fields( "relation_type" ) ) ?></option>
<?php

		$rs->MoveNext();

	}

	?>
	</select>
	<?php

}

//---------------------------------------------------------------------

function paymentSelect( $row = 1, $value = false ){

	$lang = User::getInstance()->getLang();

	$rs =& DBUtil::query( "SELECT idpayment, name$lang AS name FROM payment ORDER BY display ASC" );

	?><select name="payment_select_<?php echo $row ?>" id="payment_select_<?php echo $row ?>" onchange="$('#payment_<?php echo $row ?>').val(this.value);" onblur="$(this).parent().append(this[selectedIndex].innerHTML); this.style.display='none';" class="fullWidth">
		<option value=""></option>
	<?php

	while( !$rs->EOF ){

?>
		<option value="<?php echo $rs->fields( "idpayment" ) ?>"><?php echo $rs->fields( "name" ) ?></option>
<?php

		$rs->MoveNext();

	}

	?>
	</select>
	<?php

}

//---------------------------------------------------------------------

function create(){

	checkPostData();

	$query = "INSERT INTO accounts_models ( name, iddiary_code ) VALUES ( " . DBUtil::quote( stripslashes( $_POST[ "name" ] ) ) . ", " . DBUtil::quote( stripslashes( $_POST[ "iddiary_code" ] ) ) . " )";
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		exit( "Un problème est survenu lors de la création du modèle" );

	$idmodel = DBUtil::getInsertID();

	for( $i = 1 ; $i <= intval( $_POST[ "rowCount" ] ) ; $i++ ){

		$label = preg_replace( "/<img[^>]*tag=\"(%[a-zA-Z_-]*%)\"[^>]*>/", '$1', stripslashes( $_POST[ "label_$i" ] ) );

		$query = "
			INSERT INTO accounts_models_row (
				idrow,
				idmodel,
				label,
				idaccount_plan,
				idrelation,
				relation_type,
				idrecord,
				record_type,
				date,
				amount,
				amount_type,
				payment,
				payment_date
			) VALUES (
				$i,
				$idmodel,
				" . DBUtil::quote( $label ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "account_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "third_party_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "third_party_type_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "record_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "record_type_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "date_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "amount_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "amount_type_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "payment_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "payment_date_$i" ] ) ) . "
			)";
		$rs =& DBUtil::query( $query );

		if( $rs === false ){

			DBUtil::query( "DELETE FROM accounts_models WHERE idmodel = $idmodel" );
			DBUtil::query( "DELETE FROM accounts_models_row WHERE idmodel = $idmodel" );

			exit( "Un problème est survenu lors de la création du modèle" );

		}

	}

}

//---------------------------------------------------------------------

function modify(){

	checkPostData();

	$idmodel = intval( $_POST[ "idmodel" ] );

	$query = "UPDATE accounts_models SET name = '" . Util::html_escape( $_POST[ "name" ] ) . "', iddiary_code = '" . Util::html_escape( $_POST[ "iddiary_code" ] ) . "' WHERE idmodel = $idmodel";
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		exit( "Un problème est survenu lors de la modification du modèle" );

	for( $i = 1 ; $i <= intval( $_POST[ "rowCount" ] ) ; $i++ ){
		$label = preg_replace( "/<img[^>]*tag=\"(%[a-zA-Z_-]*%)\"[^>]*>/", '$1', stripslashes( Util::doNothing($_POST[ "label_$i" ]) ) );
		/*$query = "
			UPDATE accounts_models_row SET
				idrow = $i,
				idmodel = $idmodel,
				label = '" . Util::html_escape( $label ) . "',
				idaccount_plan = '" . Util::html_escape( stripslashes( $_POST[ "account_$i" ] ) ) . "',
				idrelation = '" . Util::html_escape( stripslashes( $_POST[ "third_party_$i" ] ) ) . "',
				relation_type = '" . Util::html_escape( stripslashes( $_POST[ "third_party_type_$i" ] ) ) . "',
				idrecord = '" . Util::html_escape( stripslashes( $_POST[ "record_$i" ] ) ) . "',
				record_type = '" . Util::html_escape( stripslashes( $_POST[ "record_type_$i" ] ) ) . "',
				date = '" . Util::html_escape( stripslashes( $_POST[ "date_$i" ] ) ) . "',
				amount = '" . Util::html_escape( stripslashes( $_POST[ "amount_$i" ] ) ) . "',
				amount_type = '" . Util::html_escape( stripslashes( $_POST[ "amount_type_$i" ] ) ) . "',
				payment = '" . Util::html_escape( stripslashes( $_POST[ "payment_$i" ] ) ) . "',
				payment_date = '" . Util::html_escape( stripslashes( $_POST[ "payment_date_$i" ] ) ) . "'
			WHERE idmodel = $idmodel
				AND idrow = $i";*/
		$query = "
			REPLACE INTO accounts_models_row (
				idrow,
				idmodel,
				label,
				idaccount_plan,
				idrelation,
				relation_type,
				idrecord,
				record_type,
				date,
				amount,
				amount_type,
				payment,
				payment_date
			) VALUES (
				$i,
				$idmodel,
				" . DBUtil::quote( $label ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "account_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "third_party_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "third_party_type_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "record_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "record_type_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "date_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "amount_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "amount_type_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "payment_$i" ] ) ) . ",
				" . DBUtil::quote( stripslashes( $_POST[ "payment_date_$i" ] ) ) . "
			)";
		$rs =& DBUtil::query( $query );

		if( $rs === false )
			exit( "Un problème est survenu lors de la modification du modèle" );

	}

}

//---------------------------------------------------------------------
function checkPostData(){

	$count = intval( $_POST[ "rowCount" ] );

	if( empty( $_POST[ "name" ] ) )
		exit( "Vous devez saisir un nom pour le modèle" );

	if( empty( $_POST[ "iddiary_code" ] ) )
		exit( "Vous devez saisir un code journal" );

	for( $i = 1 ; $i <= $count ; $i++ ){

		if( empty( $_POST[ "account_$i" ] ) )
			exit( "Le numéro de compte doit être renseigné" );

		/*if( empty( $_POST[ "record_$i" ] ) )
			exit( "Le numéro de pièce doit être renseigné" );*/

		/*if( empty( $_POST[ "record_type_$i" ] ) )
			exit( "Le type de pièce doit être renseigné" );*/

		/*if( empty( $_POST[ "third_party_$i" ] ) )
			exit( "Le numéro de tiers doit être renseigné" );*/

		/*if( empty( $_POST[ "third_party_type_$i" ] ) )
			exit( "Le type de tiers doit être renseigné" );*/

		if( empty( $_POST[ "label_$i" ] ) )
			exit( "Le libellé doit être renseigné" );

		if( empty( $_POST[ "date_$i" ] ) )
			exit( "La date doit être renseignée" );

		if( empty( $_POST[ "amount_$i" ] ) )
			exit( "Le débit/crédit doit être renseigné" );

		if( isset( $_POST[ "payment_date_$i" ] ) && $_POST[ "payment_date_$i" ] == "%copy%" && $_POST[ "payment_date_" . ( $i - 1 ) ] == "" )
			exit( "Vous devez renseigner une valeur à copier pour la date de paiement" );

		if( isset( $_POST[ "payment_$i" ] ) && $_POST[ "payment_$i" ] == "%copy%" && $_POST[ "payment_" . ( $i - 1 ) ] == "" )
			exit( "Vous devez renseigner une valeur à copier pour le mode de paiement" );

		if( $_POST[ "amount_$i" ] == "%calculate%" )
			exit( "Vous n'avez pas sasi le calcul à faire à la ligne $i" );

		if( preg_match( '/\$\([0-9%.*\/+-]*\)/', $_POST[ "amount_$i" ] ) ){

			$matches = array();
			$calculation = substr( $_POST[ "amount_$i" ], 2, strlen( $_POST[ "amount_$i" ] ) - 3 );

			preg_match_all( "/%([0-9]+)%/", $calculation, $matches );

			foreach( $matches[ 1 ] as $match )
				if( $match > $count || $match >= $i )
					exit( "La formule de calcul est fausse.<br />Vous ne pouvez utiliser que les valeurs précédentes la ligne où est fait le calcul." );

			$calculation = preg_replace( "/%[0-9]+%/", rand( 0, 99 ), $calculation );

			eval( '$calculation = ' . "$calculation;" );

			if( !is_int( $calculation ) && !is_float( $calculation ) )
				exit( "La formule de calcul saisie est incorrecte" );

		}

	}

	return true;

}



?>