<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement règlements clients
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/Invoice.php" );
include_once( "$GLOBAL_START_PATH/objects/RegulationsBuyer.php" );
include_once( "$GLOBAL_START_PATH/script/global.fct.php" );

$banner = "no";

//ajax nouvelle ligne de règlement
if( isset( $_GET[ "getRegulationRow" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "idrow" ] ) || !intval( $_GET[ "idrow" ] ) || !isset( $_GET[ "invoices" ] ) )
		exit( "0" );
	
	getRegulationRow( $_GET[ "invoices" ], intval( $_GET[ "idrow" ] ) );
	
	exit();
	
}

//ajax ajout banque
if( isset( $_POST[ "addBankDatas" ] ) && $_POST[ "addBankDatas" ] == 1 ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$bankName = $_POST[ "bank_name" ];
	$bankInitials = $_POST[ "bank_initials" ];
	
	$idsuivant = DBUtil::query( "SELECT MAX( idbank ) + 1 as maxBank FROM bank" )->fields( "maxBank" );
	
	if( $idsuivant == 0 || $idsuivant == "" )
		$idsuivant = 1;
	
	DBUtil::query( "INSERT IGNORE INTO bank VALUES ( $idsuivant, '$bankName', '$bankInitials' )" );
	
	exit( $idsuivant );
	
}

if( isset( $_GET[ "creditBalanceFor" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	echo DBUtil::getDBValue( "credit_balance", "buyer", "idbuyer", intval( $_GET[ "creditBalanceFor" ] ) );
	
	exit();
	
}

if( !isset( $_REQUEST[ "billings" ] ) || ( !isset ( $_REQUEST[ "buyer" ] ) && !isset ( $_REQUEST[ "idgrouping" ] ) ) ){
		
	$mesg = "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";
	
	exit( $mesg );
			
}

//------------------------------------------------------------------------------------------------
//initialisation des variables
//$billings = preg_replace( "/[^0-9,]/", "", $_REQUEST[ "billings" ] );
$billings = $_REQUEST[ "billings" ] ;
$bil = explode( ",", $billings );
$payment_count = isset( $_POST[ "payment_count" ] ) && intval( $_POST[ "payment_count" ] ) ? intval( $_POST[ "payment_count" ] ) : 1;
$idbuyer = isset( $_POST[ "idbuyer" ] ) ? $_POST[ "idbuyer" ] : $_GET[ "buyer" ];
$idpayer = isset( $_POST[ "idgrouping" ] ) && !empty( $_POST[ "idgrouping" ] ) ? $_POST[ "idPayer" ] : $idbuyer;
$credit_balance = DBUtil::getDBValue( "credit_balance", "buyer", "idbuyer", $idpayer );
$title = count( $bil ) > 1 ? "Factures : " . implode( " - ", $bil ) : "Facture : $billings";

$idgrouping = false;

// Added by behzad For mhandling tab2 update

if( isset( $_REQUEST[ "tab2" ] ) && isset( $_REQUEST[ "idpayment" ] )  ){

	$idpayments = preg_replace( "/[^0-9,]/", "", $_REQUEST[ "idpayment" ] );
	$idpayments = explode( ",", $idpayments );
	
	$total_ttcs = preg_replace( "/[^0-9,]/", "", $_REQUEST[ "total_ttc" ] );
	$total_ttcs = explode( ",", $total_ttcs );
	
	$idregulations_buyers = preg_replace( "/[^0-9,]/", "", $_REQUEST[ "idregulations_buyer" ] );
	$idregulations_buyers = explode( ",", $idregulations_buyers );
	
	$tab = array();
	$tab = explode( ",", $_REQUEST[ "idregulations_buyer" ] );
	
	$ttc_tot = array();
	$ttc_tot = explode( ",", $_REQUEST[ "solde" ] );

	for($i =0;$i<count($bil);$i++)
	{
	
		$query = " UPDATE `regulations_buyer` 
			SET `accept` = '1'
			WHERE `idregulations_buyer` = ".$tab[$i]."
			;";
		
		DBUtil::query( $query );
		$query = " UPDATE `billing_regulations_buyer` 
		SET `amount` = '".$ttc_tot[$i]."'
		WHERE `idbilling_buyer` ='".$bil[$i]."'
		AND `idregulations_buyer` =".$tab[$i]."
			;";
				
		//echo $query.'<br/>';
		DBUtil::query( $query );
		$query = "UPDATE `billing_buyer` 
			SET `idpayment` = ". $idpayments[$i] ." 
			WHERE `idbilling_buyer` ='".$bil[$i]."'
			;";
		//echo $query.'<br/>';
		DBUtil::query( $query );
		
		
	}
	exit();
}
////////////////////////////////////////////////////
if( isset( $_REQUEST[ "idgrouping" ] ) && !empty( $_REQUEST[ "idgrouping" ] ) ){
	
	$idgrouping = $_REQUEST[ "idgrouping" ];
	
	$buyersArray = array();
	
	$rsbuyers =& DBUtil::query( "
		SELECT DISTINCT( b.company )
		FROM buyer b, billing_buyer bb
		WHERE bb.idbilling_buyer IN( $billings )
			AND bb.idbuyer = b.idbuyer" );
	
	while( !$rsbuyers->EOF ){
		
		array_push( $buyersArray, $rsbuyers->fields( "company" ) );			
		
		$rsbuyers->MoveNext();
		
	}
	
}

//------------------------------------------------------------------------------------------------
//Traitement du formulaire en cas de paiement par crédit
if(isset( $_REQUEST[ "registerByCredit" ] )){
	
	$payment_count = isset( $_POST[ "payment_count" ] ) ? $_POST[ "payment_count" ] : 1;
	$total_credit_used			= 0;
	
	//On parcourt tous les règlements enregistrés
	for( $i = 1 ; $i <= $payment_count ; $i++ ){
		
		foreach( $bil as $idinvoice ){
			
			$credit_used = isset( $_POST[ "credit_used_${idinvoice}_$i" ] ) && $_POST[ "credit_used_${idinvoice}_$i" ] > 0 ? $_POST[ "credit_used_${idinvoice}_$i" ] : 0.0;
			$total_credit_used += $credit_used;
				
			if( $credit_used > 0 ){
				DBUtil::query( "UPDATE billing_buyer 
								SET credit_amount = credit_amount + $credit_used 
								WHERE idbilling_buyer = '$idinvoice' 
								LIMIT 1" );
				
				$invoicesToUpdate[] = $idinvoice;
			}
			
		}
		
	}
	// ------- Mise à jour de l'id gestion des Index  #1161
	if( $total_credit_used > 0 )
		DBUtil::query( "UPDATE buyer SET credit_balance = credit_balance - $total_credit_used WHERE idbuyer = '$idbuyer'" );
	
	UpdateBillingsStatus( $invoicesToUpdate );
	
	exit();
}

//------------------------------------------------------------------------------------------------
//Traitement du formulaire

if ( isset( $_REQUEST[ "register" ] ) || isset( $_REQUEST[ "register_x" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$invoicesToUpdate = array();
	
	$overpayment_tolerance = DBUtil::getParameterAdmin( "solde_tolerance" );
	$overpayment_max_tolerance = DBUtil::getParameterAdmin( "max_solde_tolerance" );
			
	$idpayer = isset( $_POST[ "idgrouping" ] ) && !empty( $_POST[ "idgrouping" ] ) ? $_POST[ "idPayer" ] : $_POST[ "idbuyer" ];
	
	$credit_balance = DBUtil::getDBValue( "credit_balance", "buyer", "idbuyer", $idpayer );
	
	$payment_count = isset( $_POST[ "payment_count" ] ) ? $_POST[ "payment_count" ] : 1;
	
	//On parcourt tous les règlements enregistrés
	for( $i = 1 ; $i <= $payment_count ; $i++ ){
		
		$total_amount_regulation	= 0;
		$total_credit_used			= 0;
		$total_solde				= 0;
		$accept						= 1; //OSEF ! Normalement, c'est plus utilisé. Je le mets par acquis de bonne conscience parce que ça fait joli mais faut pas l'utiliser, c'est le rapprochement bancaire qui doit être utilisé à la place
		
		$regulationsToCreate = array();
		
		//Date de paiement
		if( !isset( $_POST[ "payment_date_$i" ] ) )
			exit( "Aucune date de paiement renseignée" );
		
		$payment_date = $_POST[ "payment_date_$i" ];
		
		if( $payment_date == "" || strlen( $payment_date ) != 10 || $payment_date == "00-00-0000" )
			exit( "La date de paiement saisie est erronnée !" );
		
		//Date d'échéance
		if( !isset( $_POST[ "deliv_payment_$i" ] ) )
			exit( "Aucune date d'échéance renseignée" );
		
		$deliv_payment = $_POST[ "deliv_payment_$i" ];
		
		//Mode de paiement
		if( !isset( $_POST[ "idpayment_$i" ] ) )
			exit( "Aucun mode de paiement renseigné" );
		
		$idpayment = $_POST[ "idpayment_$i" ];
		
		//Type de paiement
		if( !isset( $_POST[ "FactorOrDirect_$i" ] ) )
			exit( "Aucun compte renseigné" ); 
		
		$factor = $_POST[ "FactorOrDirect_$i" ];
		$accept = $_POST[ "payment_type_$i" ];
		
		//Numéro de pièce
		$piece_number = isset( $_POST[ "piece_number_$i" ] ) ? $_POST[ "piece_number_$i" ] : "";
		
		//Banque
		$idbank = isset( $_POST[ "idbank_$i" ] ) ? $_POST[ "idbank_$i" ] : 0;
		
		//Commentaires
		$comment = "";
		
		if( $piece_number != "" )
			$comment .= "$piece_number ";
		
		if( $idbank != 0 )
			$comment .= DBUtil::getDBValue( "bank_initials", "bank", "idbank", $idbank, true ) . " ";
		
		$comment .= $_POST[ "comment_$i" ];
		
		//On parcourt toutes les factures
		$j = $k = 0;
		foreach( $bil as $idinvoice ){
			
			$invoice = new Invoice( $idinvoice );
			
			$amount = floatval( $_POST[ "amount___${idinvoice}___$i" ] );
			$total_amount_regulation += $amount;
			
			
			//Escompte
					
			if( isset( $_POST[ "rebate_amount_$idinvoice" ] ) ){
				
				//on enlève l'escompte de la facture
				$invoice->set( "rebate_type", 'amount' );
				$invoice->set( "rebate_value", $_POST[ "rebate_amount_$idinvoice" ] );
				
				$rebate = 0.0;
				
				$invoice->save();
				
			}
			
			$invoice = new Invoice( $idinvoice );
			
			$solde = $invoice->getBalanceOutstanding();
			
			$rebate = $invoice->get( "rebate_type" ) == "amount" ? $invoice->get( "rebate_value" ) : $invoice->getTotalATI() * $invoice->get( "rebate_value" ) / 100.0;
			
			$total_solde += $solde;
			
			if( $amount > 0 ){
				
				$regulationsToCreate[ $k ][ "idbilling" ]		= $idinvoice;
				$regulationsToCreate[ $k ][ "amount" ]			= $amount > $solde ? $solde : $amount;
				$regulationsToCreate[ $k ][ "rebate_amount" ]	= $rebate;
				
				$k++;
				
			}
			
			$credit_used = isset( $_POST[ "credit_used_${idinvoice}_$i" ] ) && $_POST[ "credit_used_${idinvoice}_$i" ] > 0 ? $_POST[ "credit_used_${idinvoice}_$i" ] : 0.0;
			$total_credit_used += $credit_used;
			
			if( $credit_used > 0 )
				DBUtil::query( "UPDATE billing_buyer SET credit_amount = credit_amount + $credit_used WHERE idbilling_buyer = '$idinvoice' LIMIT 1" );
			
			if( $amount + $credit_used >= $solde && !in_array( $idinvoice, $invoicesToUpdate ) )
				$invoicesToUpdate[] = $idinvoice;
			
			$j++;
			
		}
		
		//si le tout est dans la tolerance, on passe les factures au statut payé et on onregistre la différence dans le reglement
		//$overpayment_tolerance : tolérance acceptée automatiquement
		//$overpayment_max_tolerance : tolérance acceptée/refusée par l'utilisateur. On sait s'il a acceptée via $_POST[ "maxToleranceAccepted" ]
		
		$maxToleranceAccepted = isset( $_POST[ "maxToleranceAccepted" ] ) ? $_POST[ "maxToleranceAccepted" ] : 0 ;
		
		$final_solde = $total_amount_regulation + $total_credit_used - $total_solde;
		$overpayment = 0;
		
		//si la tolérance est acceptée par l'utilisateur ou qu'on est dans la fourchette automatique
		if( abs( $final_solde ) <= $overpayment_max_tolerance && ( abs( $final_solde ) <= $overpayment_tolerance || $maxToleranceAccepted == 1 ) ){
			
			$overpayment = $final_solde;
			
			foreach( $bil as $idinvoice ){
				
				if( !in_array( $idinvoice, $invoicesToUpdate ) )
					$invoicesToUpdate[] = $idinvoice;
				
			}
			
		}
		//si le client a trop payé et que ce trop payé n'est pas dans la fourchette de tolérance, on crédite son compte client
		//je pense que c'est faux étant donné qu'il doit falloir faire un avoir mais JFC est affirmatif. donc c'est que je dois me tromper
		// ------- Mise à jour de l'id gestion des Index  #1161
		elseif( $final_solde > $overpayment_max_tolerance || $final_solde <= $overpayment_max_tolerance && $final_solde > $overpayment_tolerance && !$maxToleranceAccepted )
			DBUtil::query( "UPDATE buyer SET credit_balance = credit_balance + $final_solde WHERE idbuyer = '$idpayer'");
		// ------- Mise à jour de l'id gestion des Index  #1161
		//on décrémente le crédit client du montant d'avoir utilisé
		if( $total_credit_used > 0 )
			DBUtil::query( "UPDATE buyer SET credit_balance = credit_balance - $total_credit_used WHERE idbuyer = '$idpayer'");
		
		//On crée le règlement pour toutes les factures
		$regulationsBuyer =& RegulationsBuyer::createRegulationsBuyer( $regulationsToCreate, $idpayer );
		
		$regulationsBuyer->set( "amount", $total_amount_regulation );
		$regulationsBuyer->set( "idbank", $idbank );
		$regulationsBuyer->set( "idbuyer", $idpayer );
		$regulationsBuyer->set( "piece_number", $piece_number );
		$regulationsBuyer->set( "comment", $comment );
		$regulationsBuyer->set( "idpayment", $idpayment );
		$regulationsBuyer->set( "payment_date", euDate2us( $payment_date ) );
		$regulationsBuyer->set( "deliv_payment", euDate2us( $deliv_payment ) );
		$regulationsBuyer->set( "accept", $accept );
		$regulationsBuyer->set( "factor", $factor );
		$regulationsBuyer->set( "overpayment", $overpayment );
		$regulationsBuyer->set( "credit_balance_used", $total_credit_used );
		
		$regulationsBuyer->update();
		
		/* rapprochement */
			
		if( isset( $_POST[ "idaccounts_list" ] ) ){
			
			foreach( $_POST[ "idaccounts_list" ] as $idaccounts_list )
				DBUtil::query( "INSERT IGNORE INTO payment_reconciliations VALUES( '$idaccounts_list', '" . $regulationsBuyer->get( "idregulations_buyer" ) . "' )" );
			
		}
		
	}
	
	UpdateBillingsStatus( $invoicesToUpdate );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript">
<!--
	
	function checkForm(){
		
		var error = false;
		var solde_tolerance = <?php echo DBUtil::getParameterAdmin( "solde_tolerance" ) ?>;
		var max_solde_tolerance = <?php echo DBUtil::getParameterAdmin( "max_solde_tolerance" ) ?>;
		
		$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
		$.blockUI.defaults.css.fontSize = '12px';
		$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
		$.blockUI.defaults.growlCSS.color = '#FFFFFF';
		
		<?php if( $idgrouping !== false ){ ?>
		//sélection du payeur
		if( $('#idPayer').val() == "" ){
			$.growlUI( '', "Veuillez sélectionner le client ayant effectué le paiement" );
			return false;
		}
		<?php } ?>
		
		//montants renseignés et non nuls
		//on parcourt toutes les lignes de règlements
		var i = 0;
		$('#regulationsTable').children('tbody').children('tr').each(function(){
			
			//on vérifie les montants affectés
			var amount = $(this).children('.totalRowAmount').children('input').val();
			
			var nameInput = $(this).children('.totalRowAmount').children('input').attr('name');
			
			var arrayKeys = nameInput.split("___");
			
			var key = arrayKeys[1];
			var row = arrayKeys[2];
			
			var mySolde = parseFloat( $( "#invoiceSolde_" + key ).html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
			
			var credUsed = $('#credit_used_'+key+'_1').val();
			
			if(amount == ''){
				$.growlUI( '', "Un montant de règlement n'a pas été saisi il ne sera donc pas enregistré" );
				return false;
			}

			if( amount == 0 && mySolde != 0 && credUsed == 0 && $('#use_credit').attr('checked') == false ){
			
				if( i == 0 ){
					$.growlUI( '', "Vous devez saisir le montant du paiement" );
					error = true;
					return false;
				}

			}
			
			if( isNaN( amount ) ){
				$.growlUI( '', "Le montant doit être un nombre" );
				error = true;
				return false;
			}
			
			//vérification des informations du règlement
			if( $(this).hasClass('regulation') ){
				
				var idpayment = $(this).children('td').children('.idpayment').val();
				if( idpayment == "" || idpayment == 0 ){
					$.growlUI( '', "Veuillez renseigner le mode de paiement" );
					error = true;
					return false;
				}
				
				var payment_date = $(this).children('td').children('.paymentDate').val();
				if( payment_date == "" ){
					$.growlUI( '', "Veuillez renseigner la date de paiement" );
					error = true;
					return false;
				}
				
				var deliv_payment = $(this).children('td').children('.delivPayment').val();
				if( deliv_payment == "" ){
					$.growlUI( '', "Veuillez renseigner la date d'échéance du paiement" );
					error = true;
					return false;
				}
				
			}
			
		});
		
		if( error )
			return false;
		
		//on s'occupe de la tolérance
		
		var total_solde = parseFloat( $('#invoicesTotalSolde').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
		var total_regulations = parseFloat( $('#regulationsTotalSolde').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
		var overpayment = Math.abs( Math.round( ( total_regulations - total_solde ) * 100.0 ) / 100.0 );
		
		if( overpayment <= max_solde_tolerance && overpayment > solde_tolerance ){
			if( Math.round( ( total_regulations - total_solde ) * 100.0 ) / 100.0 > 0){
				$('#popupAcceptToleranceMessage').html("Le trop perçu s'élève à " + overpayment + " ¤, enregister le règlement sans mettre le solde sur le compte client ?");
			}else{
				$('#popupAcceptToleranceMessage').html("La somme des règlements couvre le solde de la facture à " + overpayment + " ¤ près, la considérer comme soldée ?");
			}
			$.blockUI({
				message: $('#popupAcceptTolerance'),
				css: {
					fontFamily: 'arial, "lucida console", sans-serif',
					fontSize: '12px',
					height: '100px',
					width: '400px',
					left: '50%',
					'margin-left': '-200px'
				}
			});
			
			return false;
			
		}
		
		return true;
		
	}
	
	//------------------------------------------------------------------------------------
	
	$(document).ready(function(){
		
		var options = {

			beforeSubmit: checkForm,
			success: function( response ){
				if( response.length == 0 ){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#000';
	        		$.growlUI( '', 'Paiement réalisé avec succès !' );
					
					//on reload la maman et on ferme la popup
					<?php if( isset( $_GET[ "updinvoice" ] ) ){ ?>
					opener.location.href="<?php echo $GLOBAL_START_URL ?>/accounting/modify_invoice.php?IdInvoice=<?php echo $_GET[ "billings" ] ?>";
					<?php }else{ ?>
						window.opener.goForm();       
					<?php } ?>
					setTimeout('self.close();',2000); 
	       		}
	       		else alert( "Impossible d'enregistrer les modifications : " + response );
			}
		}
		
		$('#frm').ajaxForm(options);
		
		var options = {
			beforeSubmit: function(){
				if( $('#bank_name').val() == "" ){
					$.growlUI("Vous devez entrer un nom de banque",'');
					return false;
				}
				
				if( $('#bank_initials').val() == "" ){
					$.growlUI("Vous devez entrer des initiales pour la banque",'');
					return false;
				}
				
				return true;
			},
			success: function( response ){
				if( !isNaN( response ) ){
					refillBankLists( response );
					$.unblockUI();
	       		}
	       		else
				    $.growlUI( response, '' );
	    	}
		}; 
		
		$('#bankForm').ajaxForm(options);
		
	});

	//------------------------------------------------------------------------------------
	// fonction pour poster directement en cas d'overpayment accepté ou pas
	function directPostForm(){
	
		var myoptions = {
			url : "<?php echo $GLOBAL_START_URL ?>/accounting/register_buyer_payment.php?register&billings=<?php echo $billings ?>&amp;buyer=<?php echo $idbuyer ?>",
			success: function( response ){
				if( response.length == 0 ){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#000';
	        		$.growlUI( '', 'Paiement réalisé avec succès !' );
					
					//on reload la maman et on ferme la popup
					<?php if( isset( $_GET[ "updinvoice" ] ) ){ ?>
					opener.location.href="<?php echo $GLOBAL_START_URL ?>/accounting/modify_invoice.php?IdInvoice=<?php echo $_GET[ "billings" ] ?>";
					<?php }else{ ?>
						window.opener.goForm();       
					<?php } ?>

					setTimeout('self.close();',2000); 
	       		}else{
	       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
					$.blockUI.defaults.growlCSS.color = '#FFFFFF';
				    $.growlUI( '', " Impossible d'enregistrer les modifications <br />" + response );
				}
		    }
		};
		
		$('#frm').ajaxSubmit(myoptions);
	}
		
	//------------------------------------------------------------------------------------
	
	function registerByCredit(){
	
		var myoptions = {
			url : "<?php echo $GLOBAL_START_URL ?>/accounting/register_buyer_payment.php?registerByCredit&buyer=<?php echo $idbuyer ?>&amp;billings=<?php echo $billings ?>",
			success: function( response ){
				if( response.length == 0 ){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#000';
	        		$.growlUI( '', "Facture(s) soldée(s) par l'avoir !" );
					
					//on reload la maman et on ferme la popup
					window.opener.goForm();       

					setTimeout('self.close();',2000); 
	       		}else{
	       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
					$.blockUI.defaults.growlCSS.color = '#FFFFFF';
				    $.growlUI( '', " Impossible d'enregistrer les modifications <br />" + response );
				}
		    }
		};
		
		$('#frm').ajaxSubmit(myoptions);
		
	}
	
	//------------------------------------------------------------------------------------
	
	function showRegulations( div ){
		
		$.blockUI({
			message: $('#'+div),
			fadeIn: 700, 
    		fadeOut: 700,
			css: {
				width: '700px',
				top: '0px',
				left: '50%',
				'margin-left': '-350px',
				'margin-top': '50px',
				padding: '5px', 
				cursor: 'help',
				'-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px',
                'background-color': '#FFFFFF',
                'font-size': '11px',
                'font-family': 'Arial, Helvetica, sans-serif',
                'color': '#44474E'
			 }
		}); 
		
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
	}
	
	//------------------------------------------------------------------------------------
	
	function addBank(){ 
		
		$.blockUI({
			message: $('#bankForm'),
			css: {
				fontFamily: 'arial, "lucida console", sans-serif',
				fontSize: '12px',
				height: '100px',
				width: '300px'
			}
		}); 
		
	}
	
	//------------------------------------------------------------------------------------
	
	function refillBankLists( idbank ){
		
		var name = $('#bank_name').val();
		
		$('.bankList').each(function(){
			$(this).append('<option value="' + idbank + '">' + name + "</option>");
		});
		
	}
	
	//------------------------------------------------------------------------------------
	
	function addRegulationRow(){
		
		var table = $('#regulationsTable');
		var idrow = table.children('tbody').children('tr').children('td:last').children('input').attr('id');
		idrow = parseInt( idrow.substr( idrow.indexOf('_',7) + 1 ) ) + 1;
		
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/register_buyer_payment.php',
			data: 'getRegulationRow&invoices=<?php echo $billings ?>&idrow='+idrow,
			error: function(){ $.growUI('Une erreur est survenue lors de l\'ajout d\'une nouvelle ligne de règlement',''); },
			success: function( response ){
				if( response == "0" )
					$.growUI('Une erreur est survenue lors de l\'ajout d\'une nouvelle ligne de règlement','');
				else{
					
					table.children('tbody').append(response);
					if( $('#payment_count').val() == 1 )
						$('#regulationsSubtitle').append('s');
					$('#payment_count').val( parseInt( $('#payment_count').val() ) + 1 );
					
				}
			}
		});
		
	}
	
	//------------------------------------------------------------------------------------
	// Fonction de recalcul des sommes lors d'un changement d'escompte
	
	function updatePaymentSum( rebateValue,type ,idinvoice ){
		
		var invoiceAmount = parseFloat( $( "#invoiceAmount_" + idinvoice ).html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
		
		var rebateAmount = 0.00;
		var rebateRate = 0.00;
		
		if(type=='rate'){
			rebateRate = rebateValue;
			rebateAmount = (invoiceAmount * rebateValue) / 100;
			$( "#rebate_amount_" + idinvoice ).val( Math.round( rebateAmount * 100.0 ) / 100.0 );
		}else{
			rebateAmount = rebateValue;
			rebateRate = ( rebateValue * 100 ) / invoiceAmount;
			$( "#rebate_rate_" + idinvoice ).val( Math.round( rebateRate * 100.0 ) / 100.0 );
		}
		
		
		//solde de la facture
		
		var invoice = parseFloat($( "#invoiceSoldeBase_" + idinvoice ).val());
		invoice -= rebateAmount;
		invoice = Math.round( invoice * 100.0 ) / 100.0;
		
		$( "#invoiceSolde_" + idinvoice ).html( invoice.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		$( "#amount_" + idinvoice + "_1" ).val( invoice.toFixed( 2 ).toString() );

		var total = 0;
		$('#invoicesTable').children('tbody').children('tr').each(function(){
			amount = parseFloat( $(this).children('td.invoiceSolde').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
			if( !isNaN( amount ) )
				total += amount;
		});
		
		total = Math.round( total * 100 ) / 100;
		
		$( "#invoicesTotalSolde" ).html( total.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
			
		useCredit( $('#use_credit').attr('checked') );
		
		recalculateAmounts();
		
	}
	
	//------------------------------------------------------------------------------------
	//met à jour le montant d'avoir disponible quand on change de payeur (groupements)
	
	function reloadCredBal( idbuyer ){
 		
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL ?>/accounting/register_buyer_payment.php",
			cache: false,
			type: "GET",
			data: "creditBalanceFor=" + idbuyer,
			error : function(){ $.growlUI( '','Impossible modifier le crédit disponible' ); },
		 	success: function( response ){
				$('#use_credit').attr('checked',false);
				$('#creditBalance').html( parseFloat( response ).toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
				if( parseFloat( response ) > 0 )
					$('#use_credit').parent().parent('div').css('display','block');
				else
					$('#use_credit').parent().parent('div').css('display','none');
			}
		});
		
	}
	
	//------------------------------------------------------------------------------------
	//recalcule le total des règlements
	
	function recalculateAmounts(){
		
		var total = 0;
		
		$('#regulationsTable').children('tbody').children('tr').each(function(){
			amount = parseFloat( $(this).children('td.totalRowAmount').children('input').val() );
			if( !isNaN( amount ) )
				total += amount;
		});
		
		total = Math.round( total * 100 ) / 100;
		
		$('#regulationsTotalSolde').html( total.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		
		myTotal = total - parseFloat( $('#invoicesTotalSolde').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
		$('#regulationsTotalSoldeOver').html( myTotal.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );

		var st = <?php echo DBUtil::getParameterAdmin( "solde_tolerance" ) ?>;
		var mst = <?php echo DBUtil::getParameterAdmin( "max_solde_tolerance" ) ?>;
				
		if(total!=parseFloat( $('#invoicesTotalSolde').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) ) ){
			$('#regulationsTotalSoldeOver').css('background-color','red');
			
			if( Math.abs( myTotal.toFixed( 2 ) ) <=  st){
				$('#regulationsTotalSoldeOverText').html( "Cet écart est accepté les factures seront soldées" );
			}
			
			if( Math.abs( myTotal.toFixed( 2 ) ) > st && Math.abs( myTotal.toFixed( 2 ) ) <=  mst){	
				$('#regulationsTotalSoldeOverText').html( "Cet écart sera accepté si vous le décidez, les factures seront soldées" );
			}
			
			if( myTotal.toFixed( 2 ) >  mst && myTotal.toFixed( 2 ) > 0 ){	
				$('#regulationsTotalSoldeOverText').html( "Cet écart de paiement soldera les factures et créditera le compte client" );
			}
			
			if( myTotal.toFixed( 2 ) < ((-1)*mst) && myTotal.toFixed( 2 ) < 0){	
				$('#regulationsTotalSoldeOverText').html( "Cet écart de paiement est trop important, les factures ne seront pas soldées" );
			}
			
		}else{
			$('#regulationsTotalSoldeOver').css('background-color','');
			$('#regulationsTotalSoldeOverText').html( "" );
		}		
	}
	
	//------------------------------------------------------------------------------------
	
	//credit_balance : montant crédit avoir disponible affiché en haut de page
	//total_solde : solde total à payer en bas du tableau des factures
	//invoice_solde : solde de chacune des factures parcourues
	//invoice_credit : montant d'avoir utilisé. Inclut les montants d'avoir déjà utilisés et ceux utiliser maintenant
	//credit_used : montant d'avoir utilisé sur la facture maintenant
	
	function useCredit( checked ){
	
		var credit_balance = parseFloat( $( "#creditBalance" ).html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
		var total_solde = parseFloat( $( "#invoicesTotalSolde" ).html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
		
		$('#invoicesTable').children('tbody').children('tr').each(function(){
			
			if( $(this).children('td').length ){
				
				var invoice_solde = parseFloat( $(this).children('.invoiceSolde').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
				var credit_used = parseFloat( $(this).children('.creditAmount').children('input:hidden').val().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
				var invoice_credit = parseFloat( $(this).children('.creditAmount').children('.creditInnerAmount').html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
				
				if( isNaN( invoice_credit ) )
					invoice_credit = 0;
				
				if( checked ){
					
					if( credit_balance > invoice_solde ){
						
						credit_balance -= invoice_solde;
						total_solde -= invoice_solde;
						invoice_credit += invoice_solde;
						credit_used += invoice_solde;
						invoice_solde = 0;
						
					}
					else{
						
						invoice_solde -= credit_balance;
						total_solde -= credit_balance;
						invoice_credit += credit_balance;
						credit_used += credit_balance;
						credit_balance = 0;
						
					}
					
					if( !$(this).children('.creditAmount').hasClass('usedCreditAmount') )
						$(this).children('.creditAmount').toggleClass('usedCreditAmount');
					
				}
				else{
					
					invoice_solde += credit_used;
					total_solde += credit_used;
					invoice_credit -= credit_used;
					credit_balance += credit_used;
					credit_used = 0;
					
					if( $(this).children('.creditAmount').hasClass('usedCreditAmount') )
						$(this).children('.creditAmount').toggleClass('usedCreditAmount');
					
				}
				
				invoice_solde = Math.round( invoice_solde * 100 ) / 100;
				invoice_credit = Math.round( invoice_credit * 100 ) / 100;
				credit_used = Math.round( credit_used * 100 ) / 100;
				
				if( invoice_credit == 0 )
					invoice_credit = '-';
				else
					invoice_credit = invoice_credit.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;";
				
				$(this).children('.invoiceSolde').html( invoice_solde.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
				$(this).children('.creditAmount').children('.creditInnerAmount').html( invoice_credit );
				$(this).children('.creditAmount').children('input:hidden').val( credit_used.toFixed( 2 ).toString() );
				$( "#amount_" + $(this).children('td:first').html() + "_1" ).val( invoice_solde.toFixed( 2 ).toString() );
				
			}
			
		});
		
		total_solde = Math.round( total_solde * 100 ) / 100;
		credit_balance = Math.round( credit_balance * 100 ) / 100;
		
		$('#invoicesTotalSolde').html( total_solde.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		$('#creditBalance').html( credit_balance.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		
		recalculateAmounts();
		
		if(total_solde == 0.00){
			$('#regulationsTotalSoldeOverText').html( "Les factures seront soldées par le crédit fournisseur" );
			$('#registerByCred').css('display','block');
			$('#register').css('display','none');
		}else{
			$('#regulationsTotalSoldeOverText').html( "" );
			$('#registerByCred').css('display','none');
			$('#register').css('display','block');
		}		
	}
	
	//------------------------------------------------------------------------------------
	
-->
</script>
<style type="text/css">
<!--
	
	div#global{
		min-width:0;
		width:auto;
	}
	
	div.mainContent div.content div.subContent table.dataTable td.creditAmount{
		text-align:center;
	}
	
	div.mainContent div.content div.subContent table.dataTable td.usedCreditAmount{
		text-align:right;
	}
	
-->
</style>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div id="popupAcceptTolerance" style="display:none;">
	<div id="popupAcceptToleranceMessage" style="text-align:center;font-weight:bold;margin-top:10px;"></div>
	<div style="text-align:center;margin-top:20px;">
		<input type="button" class="blueButton" style="margin-right:5px;" value="Oui" onclick="$('#maxToleranceAccepted').val('1');directPostForm();$.unblockUI()" />
		<input type="button" class="blueButton" style="margin-left:5px;" value="Non" onclick="$('#maxToleranceAccepted').val('0');directPostForm();$.unblockUI()" />
		<input type="button" class="blueButton" style="background-color:red;margin-left:10px;" Value="Annuler" onclick="$.unblockUI();return false;" />
	</div>
	
</div>

<div id="globalMainContent" style="margin-top:-25px; width:930px;">
	<form id="frm" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/register_buyer_payment.php?billings=<?php echo $billings ?>&amp;buyer=<?php echo $idbuyer ?>">
		<div class="mainContent fullWidthContent" style="width:930px;">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle" style="height:45px; line-height:20px;">
					<p class="title">
						Enregistrement du paiement<br />
						<?php echo $title ?>
					</p>
				</div>
				<div class="subContent">
					<input type="hidden" name="idbuyer" value="<?php echo $idbuyer ?>" />
					<input type="hidden" name="billings" value="<?php echo $billings ?>" />
					<input type="hidden" name="idgrouping" value="<?php echo $idgrouping ?>" />
					<input type="hidden" name="payment_count" id="payment_count" value="1" />
					<input type="hidden" name="maxToleranceAccepted" id="maxToleranceAccepted" value="0" />
					<?php if( $idgrouping == false ){ ?>
						<div style="margin-bottom:10px;">Client n°<?php echo $idbuyer ?> - <?php echo getBuyerName( $idbuyer ) ?></div>
					<?php }else{ ?>
						<div style="font-weight:bold;">Groupement - <?php echo getGroupingName( $idgrouping ) ?></div>
						<?php foreach( $buyersArray as $buyer ){?><div style="margin-left:5px;">- <?php echo $buyer ?></div><?php } ?>
					<?php } ?>
					<?php if( $idgrouping !== false ){ ?>
					<div style="margin:10px 0;">
						Client ayant effectué le règlement <span class="asterix">*</span> :  
						<select name="idPayer" id="idPayer" style="background-color:#CFC3E9;" onchange="if(this.options[this.selectedIndex].value != ''){ reloadCredBal( this.options[this.selectedIndex].value ); }">
							<option value="" selected="selected">Sélection</option>
							<?php
								
								$payers =& DBUtil::query( "SELECT idbuyer, company FROM buyer WHERE idgrouping = $idgrouping ORDER BY company ASC" );
								
								while( !$payers->EOF ){
									
									$idpayerPot = $payers->fields( "idbuyer" );
									$payerPot = $payers->fields( "company" );
									
									?><option value="<?php echo $payers->fields( "idbuyer" ) ?>"><?php echo $payers->fields( "company" ) ?></option>
									<?php
									
									$payers->MoveNext();
									
								}
								
							?>
						</select>
					</div>
					<?php } ?>
					<div style="margin-bottom:10px;height:20px;">
						<div style="font-weight:bold;float:left;height:18px;margin-top:2px;">
							Crédit avoir disponible : <span id="creditBalance"><?php echo Util::priceFormat( $credit_balance ) ?></span>
						</div>
						<div style="float:left;<?php echo $credit_balance > 0 ? "" : "display:none; " ?>margin-left:10px;height:20px;"><label><input type="checkbox" name="use_credit" id="use_credit" onchange="useCredit(this.checked);" style="vertical-align:middle;" /> Utiliser</label></div>
					</div>
					<div class="subTitleContainer" style="margin-left:23px;">
						<p class="subTitle">Informations facture<?php echo strstr( $billings, "," ) ? "s" : "" ?></p>
					</div>
					<?php listInvoices( $billings ); ?>
					<div class="subTitleContainer" style="margin-left:23px;">
						<p id="regulationsSubtitle" class="subTitle">Informations paiement</p>
					</div>
					<?php displayRegulations( $billings ); ?>
					<div class="submitButtonContainer" style="float:right;">
						<input type="submit" id="register" name="register" value="Enregistrer" class="blueButton" />
						<input type="button" onclick="registerByCredit();" id="registerByCred" name="registerByCred" value="Enregistrer" class="blueButton" style="display:none;"/>
					</div>
				</div>
				<div class="clear"></div>
				<?php listReconciliations( $bil ); ?>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
	</form>
	<form id="bankForm" style="display:none;" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/register_buyer_payment.php">
		<div class="tableContainer">
			<input type="hidden" name="addBankDatas" value="1" />
			<table class="dataTable">
				<tr>
					<td class="filledCell" style="text-align:left;">Nom de la banque</td>
					<td class="filledCell" style="text-align:left;"><input class="textInput" style="width:150px;" type="text" id="bank_name" name="bank_name" value="" /></td>
				</tr>
				<tr>
					<td class="filledCell" style="text-align:left;">Initiales de la banque</td>
					<td class="filledCell" style="text-align:left;"><input type="text" class="textInput" style="width:50px;" id="bank_initials" name="bank_initials" value="" /></td>
				</tr>
			</table>
			<input class="orangeButton" style="float:right; margin:10px;" type="submit" value="Ajouter" />
			<input class="blueButton" style="float:right; margin-top:10px;" type="button" onclick="$.unblockUI();" value="Annuler" />
		</div>
	</form>
</div>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------------

function listInvoices( $invoices ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/Invoice.php" );
	
	$invoices = explode( ",", $invoices );
	
?>
<div class="tableContainer">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<table id="invoicesTable" class="dataTable">
		<thead>
			<tr>
				<th class="filledCell" style="text-align:center; width:12%;">Facture n°</th>
				<th class="filledCell" style="text-align:center;">Date facture</th>
				<th class="filledCell" style="text-align:center; width:11%;">TTC facture</th>
				<th class="filledCell" style="text-align:center; width:11%;">Réglé</th>
				<th class="filledCell" style="text-align:center; width:11%;">Acompte</th>
				<th class="filledCell" style="text-align:center; width:15%;">Montant d'avoir utilisé</th>
				<th class="filledCell" style="text-align:center; width:15%;" colspan="2">Escompte</th>
				<th class="filledCell" style="text-align:center; width:65px;">Solde TTC</th>
			</tr>
		</thead>
<?php
	
	$totalSolde = 0.0;
	
	foreach( $invoices as $idinvoice ){
		
		$invoice = new Invoice( $idinvoice );
		
		//règlements effectués
		
		$query = "
		SELECT rb.idregulations_buyer, 
			rb.amount,
			brb.amount as detailledAmount, 
			rb.comment, 
			rb.idpayment, 
			rb.payment_date, 
			rb.deliv_payment, 
			rb.DateHeure, 
			rb.bank_date, 
			rb.accept,
			rb.factor,
			p.name" . User::getInstance()->getLang() . " AS payment
		FROM billing_regulations_buyer brb, regulations_buyer rb, billing_buyer bb, payment p
		WHERE rb.idregulations_buyer = brb.idregulations_buyer
		AND bb.idbilling_buyer = brb.idbilling_buyer
		AND brb.idbilling_buyer = '$idinvoice'
		AND from_down_payment = 0
		AND rb.idpayment = p.idpayment
		ORDER BY idregulations_buyer ASC";
	  	
		$rs =& DBUtil::query( $query );
	  	
		$paid = 0.0;
		
		while( !$rs->EOF() ){
			
			$paid += $rs->fields( "detailledAmount" );
			
			$rs->MoveNext();
			
		}
		
		//acompte
		$down_payment = 0.0;
		if( $invoice->get( "down_payment_value" ) > 0.0 )
			$down_payment = $invoice->get( "down_payment_type" ) == "amount" ? $invoice->get( "down_payment_value" ) : $invoice->getTotalATI() * $invoice->get( "down_payment_value" ) / 100.0;
		
		$solde = $invoice->getBalanceOutstanding();
		$totalSolde += $solde;
		
		$rebate_rate 	= $invoice->get( "rebate_type" ) == "rate" ? $invoice->get( "rebate_value" ) : $invoice->get( "rebate_value" ) * 100.0 / $invoice->getTotalATI();
		$rebate_amount 	= $invoice->get( "rebate_type" ) == "amount" ? $invoice->get( "rebate_value" ) : $invoice->getTotalATI() * $invoice->get( "rebate_value" ) / 100.0;		
		
		?>
			<input type="hidden" id="invoiceSoldeBase_<?php echo $idinvoice ?>" value="<?php echo $solde+$rebate_amount ?>" />
			<tr>
				<td style="text-align:center;"><?php echo $idinvoice ?></td>
				<td style="text-align:center;"><?php echo Util::dateFormatEu( substr( $invoice->get( "DateHeure" ), 0, 10 ) ) ?></td>
				<td style="text-align:right;" id="invoiceAmount_<?=$idinvoice?>"><?php echo Util::priceFormat( $invoice->get( "total_amount" ) ) ?></td>
				<td style="text-align:right;">
				<?php if( $paid > 0 ){ ?>
					<a href="#" class="blueLink" onclick="showRegulations('regle_<?php echo $idinvoice ?>'); return false;"><?php echo Util::priceFormat( $paid ) ?></a>
					<div id="regle_<?php echo $idinvoice ?>" style="display:none;">
						<table style="padding: 0px;border-spacing:0px;border-collapse:collapse;width:100%;">
							<tr style="background-color:#EAEAEA;">
								<th style="width:80px;border:1px solid #CBCBCB;">Num reglement</th>
								<th style="width:85px;border:1px solid #CBCBCB;">Date de paiement</th>
								<th style="width:75px;border:1px solid #CBCBCB;">Montant règlé</th>
								<th style="width:100px;border:1px solid #CBCBCB;">Total du reglement</th>
								<th style="width:100px;border:1px solid #CBCBCB;">Mode de règlement</th>
								<th style="width:50px;border:1px solid #CBCBCB;">Type de paiement</th>
								<th style="border:1px solid #CBCBCB;">Commentaire</th>
							</tr>
						<?php $rs->MoveFirst(); ?>
						<?php while( !$rs->EOF() ){ ?>
							<tr>
								<td style="border:1px solid #CBCBCB;"><?php echo $rs->fields( "idregulations_buyer" ) ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo Util::dateFormatEu( $rs->fields( "payment_date" ) ) ?></td>
								<td style="border:1px solid #CBCBCB; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "detailledAmount" ) ) ?></td>
								<td style="border:1px solid #CBCBCB; text-align:right;"><?php echo Util::priceFormat( $rs->fields("amount" ) ) ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo $rs->fields( "payment" ) ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo $rs->fields( "factor" ) ? "Factor": "Banque" ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo stripslashes( $rs->fields( "comment" ) ) ?></td>
							</tr>
							<?php $rs->MoveNext(); ?>
						 <?php } ?>
						</table>
						<input type="button" value="Fermer" onclick="$.unblockUI();" class="blueButton" style="margin-bottom:5px; margin-top:15px;" />
					</div>
				<?php }else{ ?><?php echo Util::priceFormat( $paid ) ?><?php } ?>
				</td>
				<?php echo $down_payment > 0 ? '<td style="text-align:right;">' . Util::priceFormat( $down_payment ) . "</td>" : '<td style="text-align:center;">-</td>' ?>
				<td class="creditAmount<?php echo $invoice->get( "credit_amount" ) > 0 ? " usedCreditAmount" : "" ?>">
					<span class="creditInnerAmount"><?php echo $invoice->get( "credit_amount" ) > 0 ? Util::priceFormat( $invoice->get( "credit_amount" ) ) : "-" ?></span>
					<input type="hidden" name="credit_used_<?php echo $idinvoice ?>_1" value="0" />
				</td>
				<td style="text-align:center;">
					<!--
					<label><input onclick="updatePaymentSum( <?php echo $rebate_amount ?>, !this.checked, <?php echo $idinvoice ?> );" type="checkbox" checked="checked" name="rebate_<?php echo $idinvoice ?>" value="<?php echo $rebate_rate ?>" style="vertical-align:middle;" />
					-->
					<input type="text" class="textInput" style="width:30px;" id="rebate_rate_<?php echo $idinvoice ?>" name="rebate_rate_<?php echo $idinvoice ?>" value="<?=$rebate_rate?>" onkeyup="updatePaymentSum( this.value, 'rate' , '<?php echo $idinvoice ?>');" /> %
				</td>
				<td style="text-align:center;">
					<input type="text" class="textInput" style="width:35px;" id="rebate_amount_<?php echo $idinvoice ?>" name="rebate_amount_<?php echo $idinvoice ?>" value="<?=$rebate_amount?>" onkeyup="updatePaymentSum( this.value, 'amount' , '<?php echo $idinvoice ?>');" /> &euro;
				</td>
				
				<td id="invoiceSolde_<?php echo $idinvoice ?>" class="invoiceSolde" style="text-align:right;"><?php echo Util::priceFormat( $solde ) ?></td>
			</tr>
			<?php } ?>
		</tbody>
		<tfoot>
			<tr>
				<th colspan="8" id="invoicesTotalSoldeText" style="border-style:none;"></th>
				<th id="invoicesTotalSolde" style="text-align:right;"><?php echo Util::priceFormat( $totalSolde ) ?></th>
			</tr>
		</tfoot>
	</table>
	<input type="hidden" id="invoicesTotalSoldeBase" value="<?php echo $totalSolde ?>" />
</div>
<?php
	
}

//------------------------------------------------------------------------------------------------------

function displayRegulations( $invoices, $row = 1 ){
	
	?>
	<div class="tableContainer">
		<table id="regulationsTable" class="dataTable">
			<thead>
				<tr>
					<th style="border-style:none; width:17px;"></th>
					<th class="filledCell" style="text-align:center;">Date de paiement <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Date d'échéance <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Mode de paiement <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Compte <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Type de paiemen <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Pièce n°</th>
					<th class="filledCell" style="text-align:center;">Emetteur</th>
					<th class="filledCell" style="text-align:center;">Commentaire</th>
					<th class="filledCell" style="text-align:center;">Facture n°</th>
					<th class="filledCell" style="text-align:center; width:65px;">Montant<?php echo strstr( $invoices, "," ) ? " affecté" : "" ?></th>
				</tr>
			</thead>
			<tbody>
				<?php $totalSolde = getRegulationRow( $invoices, $row ); ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="9" style="border-style:none;"></th>
					<th id="regulationsTotalSolde" style="text-align:right;"><?php echo Util::priceFormat( $totalSolde ) ?></th>
				</tr>
				<tr>
					<th id="regulationsTotalSoldeOverText" colspan="9" style="border-style:none;text-align:right;"></th>
					<th id="regulationsTotalSoldeOver" style="text-align:right;">0.00 ¤</th>
				</tr>
			</tfoot>
		</table>
	</div>
	<?php
	
}

//------------------------------------------------------------------------------------------------------

function getRegulationRow( $invoices, $row ){
	
	global $GLOBAL_START_URL;
	$invoices = explode( ",", $invoices );
	
	$rowspan = count( $invoices ) > 1 ? ' rowspan="' . count( $invoices ) . '"' : "";
	
	${"deliv_payment_$row"} = usDate2eu( getInvoiceDelivPayment( $invoices[ 0 ] ) );
	//Added By Behzad
		$idPayment = getInvoiceIdPayment( $invoices[ 0 ] );
	${"payment_date_$row"} = date( "d-m-Y" );
	
	?>
				<tr class="regulation">
					<td<?php echo $rowspan ?> style="border-style:none; text-align:center;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="Ajouter" onclick="addRegulationRow();" /></td>
					<td<?php echo $rowspan ?> style="text-align:center;">
						<input type="text" name="payment_date_<?php echo $row ?>" id="payment_date_<?php echo $row ?>" value="<?php echo ${"payment_date_$row"} ?>" class="calendarInput paymentDate" />
						<?php DHTMLCalendar::calendar( "payment_date_$row", false ); ?>
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;">
						<input type="text" name="deliv_payment_<?php echo $row ?>" id="deliv_payment_<?php echo $row ?>" value="<?php echo ${"deliv_payment_$row"} ?>" class="calendarInput delivPayment" />
						<?php DHTMLCalendar::calendar( "deliv_payment_$row", false ); ?>
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;"><?php echo paymentList( $row, $idPayment ) ?></td>

					<td<?php echo $rowspan ?>>
					<?php
					
						$i = 0;
						$factor = false;
						while( !$factor && $i < count( $invoices ) ){
							
							$factor |= DBUtil::getDBValue( "factor", "billing_buyer", "idbilling_buyer", $invoices[ $i ] );
							$i++;
							
						}
						
						?>
						<label style="white-space:nowrap;"><input type="radio" name="FactorOrDirect_<?php echo $row ?>" value="0"<?php if( !$factor ) echo " checked=\"checked\""; ?> class="factor" style="vertical-align:middle;" />Banque</label><br />
						<!--<label style="white-space:nowrap;"><input type="radio" name="FactorOrDirect_<?php echo $row ?>" value="1"<?php if( $factor ) echo " checked=\"checked\""; ?> class="factor" style="vertical-align:middle;" />Factor</label>-->
					</td>
					
					<td<?php echo $rowspan ?>>
					<?php
						//Added by behzad
						//print_r($invoices);
						$accept = 1;//DBUtil::getDBValue( "accept", "regulations_buyer", "idbilling_buyer", $invoices[ $i ] );
						
						?>
						<label style="white-space:nowrap;"><input type="radio" name="payment_type_<?php echo $row ?>" value="1"<?php if( $accept ) echo " checked=\"checked\""; ?> class="factor" style="vertical-align:middle;" />immédiat</label><br />
						<label style="white-space:nowrap;"><input type="radio" name="payment_type_<?php echo $row ?>" value="0"<?php if( !$accept ) echo " checked=\"checked\""; ?> class="factor" style="vertical-align:middle;" />à venir</label>
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;">
						<input type="text" name="piece_number_<?php echo $row ?>" id="piece_number_<?php echo $row ?>" value="" class="textInput pieceNumber" style="width:90px;" />
					</td>
					<td<?php echo $rowspan ?> style="white-space:nowrap;">
						<select name="idbank_<?php echo $row ?>" id="idbank_<?php echo $row ?>" class="bankList">
							<option value="0">Sélection</option>
							<?php
								
								$banks = DBUtil::query( "SELECT * FROM bank ORDER BY bank_name ASC" );
								
								while( !$banks->EOF ){
									
									$idb = $banks->fields( "idbank" );
									$bname = $banks->fields( "bank_name" );
									
									echo "<option value=\"$idb\">$bname</option>";									
									
									$banks->moveNext();
									
								}
								
							?>
						</select>
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="Ajouter" onclick="addBank();" class="addBankButton" />
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;"><input type="text" name="comment_<?php echo $row ?>" value="" class="textInput commentField" /></td>
					<?php
						
						$i = 0;
						$totalSolde = 0.0;
						foreach( $invoices as $idinvoice ){
							
							if( $row == 1 ){
								
								$invoice = new Invoice( $idinvoice );
								$amount = $invoice->getBalanceOutstanding();
								
							}
							else
								$amount = 0;
							
							$totalSolde += $amount;
							
							if( $i == 0 ){
								?>
					<td style="text-align:center;"><?php echo $idinvoice ?></td>
					<td class="totalRowAmount" style="text-align:right;"><input type="text" id="amount_<?php echo $idinvoice ?>_<?php echo $row ?>" name="amount___<?php echo $idinvoice ?>___<?php echo $row ?>" value="<?php echo $amount ?>" onchange="recalculateAmounts();" onkeyup="recalculateAmounts();" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;</td>
								<?php
							}
							else{
								
							?>
				<tr class="onlyInvoice">
					<td style="text-align:center;"><?php echo $idinvoice ?></td>
					<td class="totalRowAmount" style="text-align:right;"><input type="text" id="amount_<?php echo $idinvoice ?>_<?php echo $row ?>" name="amount___<?php echo $idinvoice ?>___<?php echo $row ?>" value="<?php echo $amount ?>" onchange="recalculateAmounts();" onkeyup="recalculateAmounts();" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;</td>
				</tr>
							<?php
								
							}
							
							$i++;
							
						}
						
					?>
				</tr>
	<?php
	
	return $totalSolde;
	
}

//------------------------------------------------------------------------------------------------------

function getBuyerName( $idbuyer ){
	
	$query = "SELECT company FROM buyer WHERE idbuyer = '$idbuyer' LIMIT 1";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du client" );
	
	if( $rs->fields( "company" ) == "" ){
		
		$query = "SELECT CONCAT( firstname, ' ', lastname ) AS company FROM contact WHERE idbuyer = '$idbuyer' AND idcontact = 0 LIMIT 1";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le nom du client" );
		
	}
	
	return $rs->fields( "company" );
	
}

//------------------------------------------------------------------------------------------------------

function getGroupingName( $idgroup ){
	
	$query = "SELECT grouping FROM grouping WHERE idgrouping = $idgroup LIMIT 1";
	$rs = DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du groupement" );
	
	return $rs->fields( "grouping" );
	
}

//------------------------------------------------------------------------------------------------------

function UpdateBillingsStatus( $bil ){
	
	if( !is_array( $bil ) )
		$billings = explode( "," , $bil );
	else
		$billings = $bil;
	
	foreach( $billings as $billing ){
		
		$query = "UPDATE billing_buyer SET status = 'Paid' WHERE idbilling_buyer = '$billing'";
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de mettre à jour le statut des factures clients" );
		
	}
	
}

//-----------------------------------------------------------------------------------------

function paymentList( $j, $selected_value = "" ){
	
	$lang = User::getInstance()->getLang();
	
	echo "<select name=\"idpayment_$j\" id=\"idpayment_$j\" class=\"idpayment\">";
	
	$rs =& DBUtil::query( "SELECT idpayment, name$lang FROM payment ORDER BY name$lang ASC" );
	
	if( $rs->RecordCount() > 0 ){
		
		include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
		
		for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
			
			$selected = "";
			
			if( $selected_value == "" && $rs->fields( "idpayment" ) == Payment::$PAYMENT_MOD_TRANSFER )
				$selected = " selected=\"selected\"";
			elseif( $rs->fields( "idpayment" ) == $selected_value )
				$selected = " selected=\"selected\"";
			else
				$selected = "";
			
			echo "<option value=\"" . $rs->fields( "idpayment" ) . "\"$selected>" . $rs->fields( "name$lang" ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}
	
    echo "</select>";
	
}

//-----------------------------------------------------------------------------------------------

function getInvoiceDelivPayment( $idbilling_buyer ){
	
	$query = "SELECT deliv_payment FROM billing_buyer WHERE idbilling_buyer = '$idbilling_buyer'";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la date d'échéance de la facture" );
	
	return $rs->fields( "deliv_payment" );
	
}
//----------------------Added By Behzad-------------------------------------------------------------------------

function getInvoiceIdPayment( $idbilling_buyer ){
	
	$query = "SELECT idpayment FROM billing_buyer WHERE idbilling_buyer = '$idbilling_buyer'";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la date d'échéance de la facture" );
	
	return $rs->fields( "idpayment" );
	
}

//----------------------------------------------------------------------------------------------------------
/* rapprochement possibles */

function listReconciliations( array $invoices ){

	global $GLOBAL_START_URL;
	
	$factorEntries = array();
	foreach( $invoices as $idbilling_buyer ){
		
		$invoice = new Invoice( $idbilling_buyer );

		$entries = getFactorEntries( $invoice->get( "idbuyer" ), $invoice->getId(), $invoice->getNetBill() );
	
		foreach( $entries as $idaccounts_list => $reconcilied ){
			
			if( !in_array( $idaccounts_list, array_keys( $factorEntries ) ) )
				$factorEntries[ $idaccounts_list ] 	= $reconcilied;
			else $factorEntries[ $idaccounts_list ] |= $reconcilied;
			
		}
		
	}
	
	if( !count( $factorEntries ) ){
		
		?>
		<p>Aucune écriture correspondante trouvée dans les extraits de la banque</p>
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function checkReconciliation( idaccounts_list ){
		
			var checkbox = document.getElementById( "Reconciliation" + idaccounts_list );
			checkbox.checked = !checkbox.checked;
			checkbox.parentNode.parentNode.style.backgroundColor = checkbox.checked ? '#F5F5F5' : '#FFFFFF';
			checkbox.parentNode.parentNode.style.fontWeight = checkbox.checked ? 'bold' : 'normal';
			
		}
	
	/* ]]> */
	</script>
	<div class="subTitleContainer" style="margin-top:10px;">
		<p class="subTitle">Ecritures possibles pour les factures sélectionnées</p>
	</div>
	<div class="tableContainer resultTableContainer">
		<table class="dataTable resultTable">
			<thead>
				<tr>
					<th colspan="2">Opération</th>
					<th>Date d'opération</th>
					<th>Date de valeur</th>
					<th>Montant</th>
				</tr>
			</thead>
			<tbody>
			<?php
	
				foreach( $factorEntries as $idaccounts_list => $reconcilied ){
					
					$rs =& DBUtil::query( "SELECT * FROM accounts_listing WHERE idaccounts_list = '$idaccounts_list' LIMIT 1" );
					
					?>
					<tr style="cursor:pointer;<?php if( $reconcilied ) echo " font-weight:bold; background-color:#F5F5F5;"; ?>" onclick="checkReconciliation(<?php echo $idaccounts_list; ?>);">
						<td class="lefterCol">
							<input type="checkbox"<?php if( $reconcilied ) echo " disabled=\"disabled\""; ?> id="Reconciliation<?php echo $idaccounts_list; ?>" name="idaccounts_list[]" value="<?php echo $idaccounts_list; ?>"<?php if( $reconcilied ) echo " checked=\"checked\""; ?> />
						</td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "heading" ) ); ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "operation_date" ) ); ?></td>
						<td><?php echo Util::dateFormatEu( $rs->fields( "value_date" ) ); ?></td>
						<td class="righterCol">
						<?php 
						
							echo $rs->fields( "amount_type" ) == "credit" ? "+" : "-"; 
							echo Util::priceFormat( $rs->fields( "amount" ) ); 
							
						?>
						</td>
					</tr>
					<?php
				
				}
				
			?>
			</tbody>
		</table>
	</div>
	<?php
	
}

//----------------------------------------------------------------------------------------------------------
/* rapprochement */

function getFactorEntries( $idbuyer, $idbilling_buyer, $netBill ){
	
	$idaccountsList = array();
	
	/*$select = "
	SELECT idaccounts_list 
	FROM accounts_listing";

	/* recherche par n° compte + montant */
	
	/*$rs =& DBUtil::query( "$select WHERE idbuyer = '$idbuyer' AND amount = '$netBill'" );
	
	while( !$rs->EOF() ){
		
		$idaccountsList[ $rs->fields( "idaccounts_list" ) ] = isReconcilied( $idbilling_buyer, $rs->fields( "idaccounts_list" ) );
		$rs->MoveNext();
		
	}
	
	if( count( $idaccountsList ) )
		return $idaccountsList;
	
	/* recherche par n° compte */
	
	/*$rs =& DBUtil::query( "$select WHERE idbuyer = '$idbuyer'" );

	while( !$rs->EOF() ){
		
		$idaccountsList[ $rs->fields( "idaccounts_list" ) ] = isReconcilied( $idbilling_buyer, $rs->fields( "idaccounts_list" ) );
		$rs->MoveNext();
		
	}*/
	
	return $idaccountsList;
		
}

//----------------------------------------------------------------------------------------------------------

function isReconcilied( $idbilling_buyer, $idaccounts_list ){
	
	$query = "
	SELECT COUNT( * ) > 0 AS isReconcilied
	FROM payment_reconciliations pr, regulations_buyer rb, billing_regulations_buyer brb
	WHERE pr.idaccounts_list = '$idaccounts_list'
	AND pr.idregulations_buyer = rb.idregulations_buyer
	AND rb.idregulations_buyer = brb.idregulations_buyer
	AND brb.idbilling_buyer = '$idbilling_buyer'";
	
	return DBUtil::query( $query )->fields( "isReconcilied" ) == 1;
	
}

//------------------------------------------------------------------------------------------------------
// DEPRECATED - PLUS UTILISE - PAS BEAU
//------------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------------

function getAmountFromBillings( $bil ){
	
	$amount = 0.0;
	
	if(is_array($bil)){
		foreach( $bil as $idbilling_buyer ){
			
			$invoice = new Invoice( $idbilling_buyer );
			$amount += $invoice->getBalanceOutstanding();
			
		}
	}else{
		$invoice = new Invoice( $bil );
		$amount += $invoice->getBalanceOutstanding();
	}
	
	return $amount;
	
}

//------------------------------------------------------------------------------------------------------

function displayPaiementForm( $nb , $billings, $idbuyer , $idgrouping){
	
	global $GLOBAL_START_URL;
	
	$today = date("Y-m-d");
		
	if(isset( $_POST["nb_payment"] ) || $nb==1) {
	
		//Initialisation des variables du formulaire
		
		if(isset( $_POST["nb_payment"] ))
			$nbp = $_POST["nb_payment"];
		else
			$nbp = 1;
			
		for($p=1;$p<=$nbp;$p++){
			
			$bil1 = explode(",",$billings);
			if($nbp==1){
				$deliv_payment_1 = isset($_POST["deliv_payment_1"]) ? $_POST["deliv_payment_1"] : usDate2eu(getInvoiceDelivPayment($bil1[0]));
			}else{
				${"deliv_payment_".$p} = isset($_POST["deliv_payment_$p"]) ? $_POST["deliv_payment_$p"] : '';
			}
			${"payment_date_".$p} = isset($_POST["payment_date_$p"]) ? $_POST["payment_date_$p"] : date("d-m-Y");
			${"idpayment_".$p} = isset($_POST["idpayment_$p"]) ? $_POST["idpayment_$p"] : '';


			$x=0;
			while( $x<count($bil1) ){
				$amount = getAmountFromBillings( $bil1[$x] );
				
				${"amount_".$bil1[$x]."_".$p} = isset($_POST["amount_".$bil1[$x]."_".$p]) ? $_POST["amount_".$bil1[$x]."_".$p] : '';
					
				$x++;
			}
			
			${"comment_".$p} = isset($_POST["comment_".$p]) ? stripslashes($_POST["comment_".$p]) : '';	
			
		}
		
	}
		
		for($i=1;$i<=$nb;$i++){
					
			$bil = explode(",",$billings);
			
			$x=0;
			while( $x<count($bil) ){
				$amount = getAmountFromBillings( $bil[$x] );
				${"amount_".$bil[$x]."_".$i} = round($amount,2);
				
				$x++;
			}
			
		
			if( $nb != 1 ){?>
				<div class="subTitleContainer">
					<p class="subTitle">Paiement n° <?=$i?></p>
				</div>
			<?php } ?>
			<div class="tableContainer">
				<table class="dataTable" style="width:360px;">
					<tr>
						<th class="filledCell" style="width:200px;">Date d'échéance <span class="asterix">*</span></th>
						<td>
							<input type="text" name="deliv_payment_<?=$i?>" id="deliv_payment_<?=$i?>" value="<?=${"deliv_payment_$i"}?>" class="calendarInput" />
							<?php DHTMLCalendar::calendar( "deliv_payment_$i" ); ?>
						</td>
					</tr>
					<tr>
						<th class="filledCell">Date de paiement <span class="asterix">*</span></th>
						<td>
							<input type="text" name="payment_date_<?=$i?>" id="payment_date_<?=$i?>" value="<?=${"payment_date_$i"}?>" class="calendarInput" />
							<?php DHTMLCalendar::calendar( "payment_date_$i" ); ?>
						</td>
					</tr>
					<tr>
						<th class="filledCell">Mode de paiement <span class="asterix">*</span></th>
						<td><?=paymentList($i,${"idpayment_".$i})?></td>
					</tr>
					<tr>
						<th class="filledCell">Type de paiement <span class="asterix">*</span></th>
						<td>
							<input type="radio" name="FactorOrDirect" id="FactorOrDirect" value=0 checked="checked">Banque
							<br />
							<input type="radio" name="FactorOrDirect" id="FactorOrDirect" value=1>Factor
						</td>
					</tr>
					<tr>
						<th class="filledCell">N° pièce <span class="asterix"></span></th>
						<td>
							<input class="textInput" style="width:100px;" type="text" name="piece_number_<?=$i?>" id="piece_number_<?=$i?>" value="" />
						</td>
					</tr>
					<tr>
						<th class="filledCell">Emettreur<span class="asterix"></span></th>
						<td>
							<select name="idbank_<?=$i?>" id="idbank_<?=$i?>">
								<option value="0">Sélection</option>
								<?
								$banks = DBUtil::query("SELECT * FROM bank");
								while(!$banks->EOF()){
									$idb = $banks->fields('idbank');
									$bname = $banks->fields('bank_name');
									echo "<option value='$idb'>$bname</option>";									
									
									$banks->moveNext();
								}
								?>
							</select>
							<img id="addBankButtonOld" src="<?=$GLOBAL_START_URL?>/images/back_office/content/button_create2.png" alt="Ajouter"/>
						</td>
					</tr>
					
				</table>
				<br style="clear:both;" />
				<table class="dataTable">
					<tr>
						<th class="filledCell" style="text-align:center;width:70px;">N° facture</th>
						<th class="filledCell" style="text-align:center;width:70px;">Date facture</th>
						<th class="filledCell" style="text-align:center;width:70px;">TTC facture</th>
						<th class="filledCell" style="text-align:center;width:55px;">Règlé</th>
						<th class="filledCell" style="text-align:center;width:55px;">Acompte</th>
						<th class="filledCell" style="text-align:center;width:50px;">Avoir</th>
						<th class="filledCell" style="text-align:center;width:60px;">Escompte</th>
						<th class="filledCell" style="text-align:center;width:60px;">Solde TTC</th>
						<th class="filledCell" style="text-align:center;width:80px;">Montant payé<span class="asterix">*</span></th>
						<th class="filledCell" style="text-align:center;">Commentaire</th>
					</tr>
					<?
					$b=0;
					$superamount = 0;
					$rebateTotalAmount = 0.0;
					$DPAmount = 0.0;
					$DPTotalAmount = 0.0;
					$champsARecalculer = array();
					
					array_push($champsARecalculer,"credit_balance_used_$i");
					
					while( $b<count($bil) ){
	
						$invoice = new Invoice( $bil[ $b ] );
						
						$idbil = $bil[$b];
						$superamount += $invoice->getBalanceOutstanding();
						
						if( $invoice->get( "rebate_value" ) > 0.0 )
							$rebateTotalAmount += $invoice->get( "rebate_type" ) == "amount" ? $invoice->get( "rebate_value" ) : $invoice->getTotalATI() * $invoice->get( "rebate_value" ) / 100.0;
						
						if( $invoice->get( "down_payment_value" ) > 0.0 ){
							$DPAmount = $invoice->get( "down_payment_type" ) == "amount" ? $invoice->get( "down_payment_value" ) : $invoice->getTotalATI() * $invoice->get( "down_payment_value" ) / 100.0;
							$DPTotalAmount += $DPAmount;
						}

						array_push($champsARecalculer, "amount_".$idbil."_".$i); 
						
						?>
						<tr>
							<td class="filledCell" style="text-align:center;">
								<?=$idbil?>
							</td>
							<td class="filledCell" style="text-align:center;">
								<?php echo usDate2eu(substr($invoice->get("DateHeure"),0,10)) ?>
							</td>
							<td class="filledCell" style="text-align:center;font-weight:bold;">
								<?php echo Util::priceFormat( $invoice->get('total_amount') ) ?>
							</td>
							<td class="filledCell" style="text-align:center;">
								<a href="#" class="blueLink" onclick="suceMaPopup('regle_<?=$bil[ $b ]?>'); return false;">
								<?
								//calcul des reglements parcke ca soule la bite
								
								$query = "
								SELECT rb.idregulations_buyer, 
									rb.amount,
									brb.amount as detailledAmount, 
									rb.comment, 
									rb.idpayment, 
									rb.payment_date, 
									rb.deliv_payment, 
									rb.DateHeure, 
									rb.bank_date, 
									rb.accept,
									rb.factor,
									p.name" . User::getInstance()->getLang() . " AS payment
								FROM billing_regulations_buyer brb, regulations_buyer rb, billing_buyer bb, payment p
								WHERE rb.idregulations_buyer = brb.idregulations_buyer
								AND bb.idbilling_buyer = brb.idbilling_buyer
								AND brb.idbilling_buyer = '".$bil[ $b ]."'
								AND from_down_payment = 0
								AND rb.idpayment = p.idpayment
								ORDER BY idregulations_buyer ASC";
							  
								$reguls = DBUtil::query( $query );
							  
								$sumReguls = 0.0;
								while(!$reguls->EOF()){
									
									$sumReguls+=$reguls->fields('detailledAmount');
									
									$reguls->MoveNext();
								}
								
								echo Util::priceFormat($sumReguls);
								?>	
								</a>
								<div id="regle_<?=$bil[ $b ]?>" style="display:none;">
									<?
									if($sumReguls == 0){
										echo "Aucun règlement à afficher !";
									}else{
										$reguls->MoveFirst();
										?>
										<table style="padding: 0px;border-spacing:0px;border-collapse:collapse;width:100%;">
											<tr style="background-color:#EAEAEA;">
												<th style="width:80px;border:1px solid #CBCBCB;">Num reglement</th>
												<th style="width:85px;border:1px solid #CBCBCB;">Date de paiement</th>
												<th style="width:75px;border:1px solid #CBCBCB;">Montant règlé</th>
												<th style="width:100px;border:1px solid #CBCBCB;">Total du reglement</th>
												<th style="width:100px;border:1px solid #CBCBCB;">Mode de règlement</th>
												<th style="width:50px;border:1px solid #CBCBCB;">Factor</th>
												<th style="border:1px solid #CBCBCB;">Commentaire</th>
											</tr>
											<?php while(!$reguls->EOF()){ ?>
													<tr>
														<td style="border:1px solid #CBCBCB;"><?php echo $reguls->fields("idregulations_buyer") ?></td>
														<td style="border:1px solid #CBCBCB;"><?php echo usDate2eu($reguls->fields("payment_date")) ?></td>
														<td style="border:1px solid #CBCBCB;"><?php echo Util::priceFormat($reguls->fields("detailledAmount")) ?></td>
														<td style="border:1px solid #CBCBCB;"><?php echo Util::priceFormat($reguls->fields("amount")) ?></td>
														<td style="border:1px solid #CBCBCB;"><?php echo $reguls->fields("payment") ?></td>
														<td style="border:1px solid #CBCBCB;">
														<?
														if($reguls->fields("factor"))
															echo "Oui";
														else
															echo "Non";
														?>
														</td>
														<td style="border:1px solid #CBCBCB;"><?php echo stripslashes($reguls->fields("comment")) ?></td>
													</tr>
											
												<?php $reguls->MoveNext(); ?>
											<?php } ?>
										</table>
										<?
									}
									?>
									<br />
									<input type="button" class="blueButton" value="Fermer" onclick="$.unblockUI();" />
								</div>
								
							</td>
							<td class="filledCell" style="text-align:center;">
								<?
									if($DPAmount>0)
										echo Util::priceFormat($DPAmount);
								?>
							</td>
							<td class="filledCell" style="text-align:center;">
								<?
									$CredAmount = $invoice->get("credit_amount");
									if($CredAmount>0)
										echo Util::priceFormat($CredAmount);
								?>
							</td>
							<td class="filledCell" style="text-align:center; white-space:nowrap;">
							<?php
	
								if( $invoice->getTotalATI() == 0.0 )
			                		$rebate_rate = 0.0;
			                	else $rebate_rate 	= $invoice->get( "rebate_type" ) == "rate" ? $invoice->get( "rebate_value" ) : $invoice->get( "rebate_value" ) * 100.0 / $invoice->getTotalATI();
			                	$rebate_amount 	= $invoice->get( "rebate_type" ) == "amount" ? $invoice->get( "rebate_value" ) : $invoice->getTotalATI() * $invoice->get( "rebate_value" ) / 100.0;
							
								if( $rebate_rate > 0.0 && $i == 1){
									
									?>
									<input onclick="updatePaymentSum( <?php echo $rebate_amount; ?>, !this.checked,<?=$idbil?>,<?=$i?>,<?=count($bil)?> ); setRebateUsage( this.checked, <?php echo $invoice->getId(); ?> );" type="checkbox" checked="checked" name="rebate_<?php echo $invoice->getId(); ?>" value="<?php echo $rebate_rate; ?>" /> <?php echo Util::rateFormat( $rebate_rate ); ?>
									<?php
									
								}
								else echo "-";
								
							?>
							</td>
							<?
							
							if($nb > 1){?>
								<td class="filledCell" id="recalculatedSolde_<?=$idbil?>_<?=$i?>" style="text-align:center;"><?=Util::priceFormat(${"amount_".$idbil."_".$i} ,2)?></td>
								<td style="text-align:center;">
									<input type="text" id="amount_<?php echo $idbil ?>_<?php echo $i ?>" name="amount_<?php echo $idbil ?>_<?php echo $i ?>" value="0.00" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;
								</td>
								<?if( $b==0 ){?>
									<td rowspan="<?=count($bil)?>">
										<input type="text" name="comment_<?=$i?>" value="" class="textInput" />
									</td>
								<?php } ?>
							<?php } else{?>
								<td class="filledCell" id="recalculatedSolde_<?=$idbil?>_<?=$i?>" style="text-align:center;"><?=Util::priceFormat(${"amount_".$idbil."_".$i} ,2)?></td>
								<td style="text-align:center;">
									<input type="text" id="amount_<?php echo $idbil ?>_<?php echo $i ?>" name="amount_<?php echo $idbil ?>_<?php echo $i ?>" value="<?=round(${"amount_".$idbil."_".$i},2) ;?>" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;
								</td>
								<?if( $b==0 ){?>
									<td rowspan="<?=count($bil)?>">
										<input type="text" name="comment_<?=$i?>" value="<?=${"comment_".$i}?>" class="textInput" />
									</td>
								<?php } ?>
							<?php } ?>
						</tr>
						<?
						$b++;
					}?>
					<tr>
						<th colspan="8">Total factures</th>
						<th style="text-align:center;"><div id="recalculatedTotal_<?=$i?>" ><?php echo str_replace( chr( 160 ), "", Util::priceFormat( $superamount ) ) ?></th>
						<td></td>
					</tr>
					<tr id="credBalTr_<?=$i?>">
						<?if($idgrouping !== false){?>
							<th colspan="7">Crédit client disponible</th>
							<th style="text-align:center;"><?=Util::priceFormat('0.00')?></th>
							<td style="text-align:center;"><?=Util::priceFormat('0.00')?></td>
							<td></td>
						<?php } else{
							getCredBalTrOf($idbuyer, $i);
						}?>
					</tr>
					<tr>
						<th colspan="8">Total reglements</th>
						<th style="text-align:center;"><div id="calculatedAmounts_<?=$i?>"><?php echo str_replace( chr( 160 ), "", Util::priceFormat($superamount ) ) ?></div></th>
						<?
						$stringRecalculate = implode(",",$champsARecalculer);
						?>
						<script type='application/javascript'>
							var stringRecalculate = "<?=$stringRecalculate?>";
						</script>
						<td style="text-align:center;"></td>
					</tr>
				</table>
			</div>
			<div class="submitButtonContainer">
				<input type="button" name="recalculate" value="Recalculer" class="blueButton" onclick="recalculateAmounts('calculatedAmounts_<?=$i?>','<?=$stringRecalculate?>');" />
			</div>
			<div class="submitButtonContainer" style="margin-top:5px;">
				<input type="submit" name="register" value="Enregistrer" class="blueButton"/>
			</div>
			
			<?php

				listReconciliations( $bil );
			
		 }
	
}

//------------------------------------------------------------------------------------------------------

function getCredBalTrOf( $idpayer, $i ){
	// ------- Mise à jour de l'id gestion des Index  #1161
	$argent = DBUtil::query("SELECT credit_balance FROM buyer WHERE idbuyer = '$idpayer'");
	//on ne construit pas le <tr> entier juste son contenu...
	if($argent->RecordCount() > 0 && $argent->fields('credit_balance') > 0 ){
		$argentDispo = $argent->fields('credit_balance');
		?>
						<th colspan="7">Crédit client disponible</th>
						<th style="text-align:center;">
							<div id="credit_balance_touse_<?=$i?>"><?=Util::priceFormat($argentDispo)?></div>
						</th>
						<td style="text-align:center;">
							<input type="text" class="textInput" style="text-align:right; width:50px;" id="credit_balance_used_<?=$i?>" name="credit_balance_used_<?=$i?>" value="0.00" />&nbsp;&euro;
						</td>
						<td></td>		
		<?
	}else{
		?>
						<th colspan="7">Crédit client disponible</th>
						<th style="text-align:center;">
							<div><?=Util::priceFormat('0.00')?></div>
						</th>
						<td style="text-align:center;">
							<div><?=Util::priceFormat('0.00')?></div>
						</td>
						<td></td>		
		<?
	}
}

//------------------------------------------------------------------------------------------------------

function displaySelectForm( $billings, $idbuyer , $idgrouping ){
	
	global $GLOBAL_START_URL;
	
	?>
	<?if($idgrouping){?>
		<form name="frmm" method="POST" action="<?=$GLOBAL_START_URL?>/accounting/register_buyer_payment.php?idgrouping=<?=$idgrouping?>&billings=<?=$billings?>&amp;buyer=<?=$idbuyer?>">
	<?php } else{?>
		<form name="frmm" method="POST" action="<?=$GLOBAL_START_URL?>/accounting/register_buyer_payment.php?billings=<?=$billings?>&amp;buyer=<?=$idbuyer?>">
	<?php } ?>
		<div class="tableContainer">
			<table class="dataTable" style="width:360px;">
				<tr>
					<th class="filledCell" style="width:200px;"><b>Nombre de paiements partiels</b></th>
					<td><input type="text" name="nb_payment" class="textInput" style="width:60px;margin-right:6px;"/><input type="submit" name="validate" value="Valider" class="blueButton" /></td>
				</tr>
			</table>
		</div>
			
	</form>
	<?php
	
}

//------------------------------------------------------------------------------------------------------

?>