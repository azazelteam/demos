<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Factures clients non payées regroupées par période
 */

include_once( "../objects/classes.php" );

//------------------------------------------------------------------------------------------------
//export

if( isset( $_GET[ "export" ] ) ){
	
	$exportableArray =& getExportableArray();
	exportArray( $exportableArray );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------

$Title = Dictionnary::translate( "gest_com_invoice_stand_by" );

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$db =& DBUtil::getConnection();


//------------------------------------------------------------------------------------------------

?>
<script type="text/javascript" language="javascript">
	function SortResult(sby,ord){
		document.adm_fact.action="<?php echo $ScriptName ?>?search=1&sortby="+sby+"&sens="+ord;
		document.adm_fact.submit();
	}
</script>

<form action="<?php echo $ScriptName ?>" method="post" name="adm_fact" id="adm_fact">
<div id="globalMainContent">
    <div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Factures client non échues</p>
				<?php
					
					$exportURL = "$GLOBAL_START_URL/accounting/com_admin_list_notpayed.php?export";
					if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) )
						$exportURL .= "&amp;sortby=" . htmlentities( $_GET[ "sortby" ] ) . "&amp;sens=" . htmlentities( $_GET[ "sens" ] );
					
				?>
				<div class="rightContainer">
					<a href="<?php echo $exportURL ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
				</div>
            </div>
            <div class="subContent">
            	<!-- tableau résultats recherche -->
                <?php 
				$totalInvoiceCount = 0;
				$superTotalHT = 0;
				$superTotalNetTTC = 0;
				
				$allInvoices = getInvoiceByIdPayment();
					
				if( sizeof( $allInvoices ) ){
					
					foreach($allInvoices as $monthInvoice => $values){
					
						$invoices = $values;
						?>
						<!-- Début des boucles -->
		                <div class="resultTableContainer clear" style="margin-top:20px;">
		                	<span style="font-size:14px;font-weight:bold;float:left;">
		                		<?
		                		$monthInvoice2 = getMonthYearName($monthInvoice);
		                		if($monthInvoice2 != "00 0000"){?>
		                			Mois de <?php echo $monthInvoice2 ?>
		                		<?php } else{?>
		                			Echéance non renseignée
		                		<?php } ?>
		                	</span>
		                	<?
		                	
							$exportURLBis = "$GLOBAL_START_URL/accounting/com_admin_list_notpayed.php?export";
							if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) )
								$exportURLBis .= "&amp;month=". htmlentities( $monthInvoice ) ."&amp;sortby=" . htmlentities( $_GET[ "sortby" ] ) . "&amp;sens=" . htmlentities( $_GET[ "sens" ] );
		                	else
								$exportURLBis .= "&amp;month=". htmlentities( $monthInvoice );
		                	?>
		                	<span style="font-size:14px;font-weight:bold;float:right;">
								<a href="<?php echo $exportURLBis ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
		                	</span>
		                	<div class="clear"></div>
		                	<table class="dataTable resultTable">
		                    	<thead>
		                            <tr>
		                                <th>Client n°</th>
		                                <th>Nom / Raison sociale</th>
		                                <th>Facture n°</th>
		                                <th>Factor</th>
		                                <th>Date de création</th>
		                                <th>Date d'échéance</th>
		                                <th>Total facturé HT</th>
		                                <th>Net à payer TTC</th>
		                            </tr>
		                            <!-- petites flèches de tri -->
		                            <tr>
		                            	<th class="noTopBorder">
		                                	<a href="javascript:SortResult('billing_buyer.idbuyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('billing_buyer.idbuyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('buyer.company','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('buyer.company','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('billing_buyer.idbilling_buyer','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('billing_buyer.idbilling_buyer','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('billing_buyer.factor','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('billing_buyer.factor','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('billing_buyer.DateHeure','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('billing_buyer.DateHeure','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('billing_buyer.deliv_payment','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('billing_buyer.deliv_payment','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('billing_buyer.total_amount_ht','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('billing_buyer.total_amount_ht','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                                <th class="noTopBorder">
		                                	<a href="javascript:SortResult('net_sum','ASC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
		                                    <a href="javascript:SortResult('net_sum','DESC')"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
		                                </th>
		                        	</tr>
		                        </thead>
		                        <tbody>
		                        <?php
						
								$tot=0;
								$total_net_sum = 0.0;
								foreach( $invoices as $invoice ){
									$totalInvoiceCount ++;
									
									$idbuyer 			= $invoice[ "idbuyer" ];
									$idbilling_buyer 	= $invoice[ "idbilling_buyer" ];
									$isContact 			= $invoice[ "contact" ];
									$tot += $invoice[ "total_amount_ht" ];
									$superTotalHT += $invoice[ "total_amount_ht" ];
									
									$total_net_sum += $invoice[ "net_sum" ];
									$superTotalNetTTC += $invoice[ "net_sum" ];
									$buyerHREF 	= "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer&amp;type=";
									$buyerHREF 	.= $isContact ? "contact" : "buyer";
									$invoiceHREF 	= "$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer";
									list( $date, $time ) = explode( " ", $invoice[ "DateHeure" ] );
									?>
		                            <tr>
		                            	<td class="lefterCol"><a href="<?php echo $buyerHREF; ?>"><?php echo $invoice[ "idbuyer" ] ?></a></td>
		                                <td><a href="<?php echo $buyerHREF; ?>"><?php echo $invoice[ "company" ] ?></a></td>
		                                <td><a href="<?php echo $invoiceHREF; ?>"><?php echo $invoice[ "idbilling_buyer" ] ?></a></td>
		                                <td><?php echo ucfirst( $invoice[ "factor" ] ) ?></td>
		                                <td><?php echo usDate2eu( $date ) ?></td>
		                                <td><?php echo usDate2eu( $invoice[ "deliv_payment" ] ) ?></td>
		                                <td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $invoice[ "total_amount_ht" ] ) ?></td>
		                                <td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $invoice[ "net_sum" ] ) ?></td>
		                            </tr>
		                            <?php
		                        }
		                        ?>
								<tr>
									<th colspan="6" style="border-style:none;"></th>
									<th class="totalAmount"><?php echo Util::priceFormat($tot) ?></th>
									<th class="totalAmount"><?php echo Util::priceFormat( $total_net_sum ) ?></th>
								</tr>
		                        </tbody>
		                    </table>
		                </div><?
                	}?>
	                <div class="resultTableContainer clear" style="margin-top:20px;float:right;">
	                	<table class="dataTable resultTable" style="width:400px">
	                    	<thead>
	                            <tr>
	                            	<th>Nombre de factures</th>
	                                <th>Total facturé HT</th>
	                                <th>Net à payer TTC</th>
	                            </tr>
	                      	</thead>
	                      	<tbody>
	                      		<tr>
	                      			<td class="lefterCol"><?php echo $totalInvoiceCount ?></td>
	                      			<td><?php echo Util::priceFormat($superTotalHT) ?></td>
	                      			<td class="righterCol"><?php echo Util::priceFormat($superTotalNetTTC) ?></td>
	                      		</tr>
	                      	</tbody>
	                     </table>      
	            	</div>
	            	<div class="clear"></div><?
				} ?>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
</div><!-- globalMainContent -->
<?php if( !$totalInvoiceCount ){ ?>
<p style="text-align:center; margin-top:20px;" class="msg"><?php echo Dictionnary::translate( "gest_com_no_invoice_found" ) ?></p>
<?php }else{ ?>
<input type="hidden" name="search" value="1" />
<?php } ?>
</form>
<?php
	
//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//Factures par mode de paiement--------------------------------------------------------------------------------

function getInvoiceByIdPayment (){

	$db =& DBUtil::getConnection();
	/*
	$query = "
	SELECT billing_buyer.idbuyer,
		buyer.company,
		buyer.contact,
		billing_buyer.idbilling_buyer, 
		billing_buyer.factor, 
		billing_buyer.DateHeure,
		billing_buyer.deliv_payment,
		billing_buyer.total_amount_ht,
		ROUND( billing_buyer.total_amount - billing_buyer.credit_amount, 2 ) AS net_sum
	FROM billing_buyer, buyer
	WHERE billing_buyer.idbuyer=buyer.idbuyer
	AND `status` NOT IN ( '". Invoice::$STATUS_PAID . "' )
	AND (billing_buyer.deliv_payment >= CURDATE( ) OR billing_buyer.deliv_payment = '0000-00-00')";
	
	if((isset($_GET['sortby'])) AND (isset($_GET['sortby']))){
		$orderby	= "ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
	}else{
		$orderby	= "ORDER BY deliv_payment DESC";
	}
	$query .=$orderby;
	
	$rs=$db->Execute($query);
	
	if($rs===false)
		die("Impossible de récupérer les factures en attente pour le mode de paiement '$idpayment'");
	*/
	$invoices = array();
	
	$i = 0;
	
	// extraction des mois et année de l'échéance pour trier
	$queryTri="
	SELECT DISTINCT MONTH(billing_buyer.deliv_payment) as month, YEAR(billing_buyer.deliv_payment) as year
	FROM billing_buyer, buyer
	WHERE billing_buyer.idbuyer=buyer.idbuyer
	AND `status` NOT IN ( '". Invoice::$STATUS_PAID . "' )
	AND (billing_buyer.deliv_payment >= CURDATE( ) OR billing_buyer.deliv_payment = '0000-00-00')
	ORDER BY year DESC, month DESC";	
	
	$rstri = DBUtil::query($queryTri);
	
	$monthyInvoices = array();
	
	while( !$rstri->EOF() ){
		
		$betweenMin = $rstri->fields("year")."-".$rstri->fields("month")."-01";
		$betweenMax = $rstri->fields("year")."-".$rstri->fields("month")."-31";
						
		$query = "
		SELECT billing_buyer.idbuyer,
			buyer.company,
			buyer.contact,
			billing_buyer.idbilling_buyer, 
			billing_buyer.factor, 
			billing_buyer.DateHeure,
			billing_buyer.deliv_payment,
			billing_buyer.total_amount_ht,
			ROUND( billing_buyer.total_amount - billing_buyer.credit_amount, 2 ) AS net_sum
		FROM billing_buyer, buyer
		WHERE billing_buyer.idbuyer=buyer.idbuyer
		AND `status` NOT IN ( '". Invoice::$STATUS_PAID . "' )";
		
		if($rstri->fields("year") == "0"){
			$query.=" AND billing_buyer.deliv_payment = '0000-00-00' ";
		}else{
			$query.=" AND billing_buyer.deliv_payment BETWEEN '$betweenMin' AND '$betweenMax' ";
		}
		
		if((isset($_GET['sortby'])) AND (isset($_GET['sortby']))){
			$orderby	= "ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
		}else{
			$orderby	= "ORDER BY deliv_payment DESC";
		}
		$query .=$orderby;
		
		$rs=DBUtil::query($query);

		if($rstri->fields("year") == "0")
			$betweenMin = "0000-00-00";
		
		$month = $betweenMin;
		
		$invoices = array();			
		$i = 0;
		
		while( !$rs->EOF() ){
	
			$factor = $rs->fields( "factor" );
			
			if( DBUtil::getDBValue( "factor_refusal_date", "billing_buyer", "idbilling_buyer", $rs->fields( "idbilling_buyer" ) ) != "0000-00-00" )
				$factor = "<span style=\"color:#FF0000;\">Définancé</span>";
			elseif( DBUtil::getDBValue( "factor_send_date", "billing_buyer", "idbilling_buyer", $rs->fields( "idbilling_buyer" ) ) != "0000-00-00" )
				$factor = "<span style=\"color:#EB6A0A;\">Financé</span>";
			
			$invoices[ $i ][ "idbuyer" ] 			= $rs->fields("idbuyer");
			$invoices[ $i ][ "idbilling_buyer" ] 	= $rs->fields("idbilling_buyer");
			$invoices[ $i ][ "total_amount_ht" ] 	= $rs->fields("total_amount_ht");
			$invoices[ $i ][ "company" ] 			= $rs->fields("company");
			$invoices[ $i ][ "contact" ] 			= $rs->fields("contact");
			$invoices[ $i ][ "DateHeure" ] 			= $rs->fields("DateHeure");
			$invoices[ $i ][ "deliv_payment" ] 		= $rs->fields("deliv_payment");
			$invoices[ $i ][ "net_sum" ] 			= $rs->fields("net_sum");
			$invoices[ $i ][ "factor" ] 			= $factor == null || empty( $factor ) ? "Non" : "Oui";
			
			$rs->MoveNext();
			$i++;
			
		}
		
		$monthyInvoices[$month] = $invoices;
		
		$rstri->MoveNext();
	}
	
	/*
	while( !$rs->EOF() ){

		$factor 								= $rs->fields( "factor" );
		$invoices[ $i ][ "idbuyer" ] 			= $rs->fields("idbuyer");
		$invoices[ $i ][ "idbilling_buyer" ] 	= $rs->fields("idbilling_buyer");;
		$invoices[ $i ][ "total_amount_ht" ] 	= $rs->fields("total_amount_ht");
		$invoices[ $i ][ "company" ] 			= $rs->fields("company");
		$invoices[ $i ][ "contact" ] 			= $rs->fields("contact");
		$invoices[ $i ][ "DateHeure" ] 			= $rs->fields("DateHeure");
		$invoices[ $i ][ "deliv_payment" ] 		= $rs->fields("deliv_payment");
		$invoices[ $i ][ "net_sum" ] 			= $rs->fields("net_sum");
		$invoices[ $i ][ "factor" ] 			= $factor == null || empty( $factor ) ? Dictionnary::translate( "gest_com_no" ) : Dictionnary::translate( "gest_com_yes" );
		
		$rs->MoveNext();
		$i++;
		
	}
	
	return $invoices;
	*/
	
	 ksort( $monthyInvoices );
	 
	return $monthyInvoices;
}

//--------------------------------------------------------------------------------

function &getExportableArray(){
	
	$query = "
	SELECT billing_buyer.idbuyer,
		buyer.company,
		billing_buyer.idbilling_buyer, 
		billing_buyer.factor, 
		billing_buyer.DateHeure,
		billing_buyer.total_amount_ht,
		billing_buyer.deliv_payment,
		ROUND( billing_buyer.total_amount - billing_buyer.credit_amount, 2 ) AS net_sum
	FROM billing_buyer, buyer
	WHERE billing_buyer.idbuyer=buyer.idbuyer
	AND `status` NOT IN ( '". Invoice::$STATUS_PAID . "' )";
		
	if(isset( $_GET[ "month" ] )){

		$res = @explode( "-", $_GET[ "month" ] );
	
		@list( $year ,$month , $day) = $res;
		
		if($year!="0000" && $month != "00"){
			$betweenMin = $year."-".$month."-01";
			$betweenMax = $year."-".$month."-31";
			
			$query.=" AND billing_buyer.deliv_payment BETWEEN '$betweenMin' AND '$betweenMax' ";
		}else{
			$query.=" AND billing_buyer.deliv_payment = '0000-00-00' ";
		}
	}else{
		$query.="AND (billing_buyer.deliv_payment >= CURDATE( ) OR billing_buyer.deliv_payment = '0000-00-00')";
	}
		
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sortby" ] ) )
		$query .= " ORDER BY " . Util::html_escape( $_GET[ "sortby" ] ) . " " . Util::html_escape( $_GET[ "sens" ] );
	else $query .= " ORDER BY DateHeure DESC";
	
	
	$data = array(
	
		Dictionnary::translate( "idbuyer" ) 							=> array(),
		Dictionnary::translate( "gest_com_buyer_company" ) 				=> array(),
		Dictionnary::translate( "gest_com_idbilling" ) 					=> array(),
		Dictionnary::translate( "factor" ) 								=> array(),
		Dictionnary::translate( "gest_com_DateHeure" ) 					=> array(),
		"Date d'échéance"							 					=> array(),
		Dictionnary::translate( "gest_com_invoice_total_amount_ht" ) 	=> array(),
		Dictionnary::translate( "gest_com_invoice_total_amount_TTC" ) 	=> array()
		

	);
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$factor = $rs->fields( "factor" );

		$data[ Dictionnary::translate( "idbuyer" ) ][]								= $rs->fields( "idbuyer" );
		$data[ Dictionnary::translate( "gest_com_buyer_company" ) ][] 				= $rs->fields( "company" );
		$data[ Dictionnary::translate( "gest_com_idbilling" ) ][] 					= $rs->fields( "idbilling_buyer" );
		$data[ Dictionnary::translate( "factor" ) ][] 								= $factor == null || empty( $factor ) ? Dictionnary::translate( "gest_com_no" ) : Dictionnary::translate( "gest_com_yes" );
		$data[ Dictionnary::translate( "gest_com_DateHeure" ) ][] 					= Util::dateFormatEu( substr( $rs->fields( "DateHeure" ), 0, 10 ) );
		$data[ "Date d'échéance"][] 												= Util::dateFormatEu(  $rs->fields( "deliv_payment" ) );
		$data[ Dictionnary::translate( "gest_com_invoice_total_amount_ht" ) ][] 	= Util::numberFormat( $rs->fields( "total_amount_ht" ) );
		$data[ Dictionnary::translate( "gest_com_invoice_total_amount_TTC" ) ][]	= Util::numberFormat($rs->fields("net_sum"));
		
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------

function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray,Dictionnary::translate( "gest_com_unpayed" ) . "_" . date( "Ymd" ) );
	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------

 function getMonthYearName($date){
 	
	$res = @explode( "-", $date );
	
	if( count( $res ) != 3 )
		return "0000 00";
	
	@list( $year, $month, $day ) = $res;
	
	if($year!="0000" && $month != "00"){
 		setlocale( LC_TIME, "fr_FR.utf8" );
	
		$timestamp = mktime( 0, 0, 0, $month, $day, $year );
		$month 		= strftime("%B", $timestamp );
		$year 		= strftime("%Y", $timestamp );
	}else{
		$month 		= "00";
		$year 		= "0000";
	}
		
 	return $month." ".$year;
 }
 
//--------------------------------------------------------------------------------
?>