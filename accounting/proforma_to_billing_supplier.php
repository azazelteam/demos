<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion factures proforma fournisseurs
 */


include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

//------------------------------------------------------------------------------------------
/* traitement ajax 2m le retour */

if( isset( $_POST[ "registerInvoice" ] ) && $_POST[ "registerInvoice" ] == "1" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" ); 
	
	$result = registerInvoice();
	
	exit($result);
		
}


if( isset( $_GET['getNewBilling'] ) && $_GET['getNewBilling'] > 0 && isset( $_GET['ids'] ) && $_GET['ids'] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" ); 
		
	displayTable( $_GET['getNewBilling'] , $_GET['ids'] );
	exit();
	
}


$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
if( !isset( $_GET[ "bs" ] ) || !isset ( $_GET[ "ids" ] ) ){
	
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}

$bs = $_GET["bs"];
$billings = explode( ',' , $bs );
$ids = $_GET["ids"];


?>
<div id="globalMainContent" style="width:770px;">
	<div style="width:380px;float:left;">
	<?
	$grosTotalAmountTTC = 0.00;
	for( $i = 0 ; $i < count( $billings ) ; $i++ ){
		$bs = $billings[ $i ];
		
		$supplierinvoice = new SupplierInvoice ( $bs , $ids );
	
		displayInvoicesInfos();
		
		$grosTotalAmountTTC += $supplierinvoice->get("total_amount"); 
		
	}
	?>
	</div>
	<div class="mainContent" style="float:right; width:380px;">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Enregistrement facture <?=getSupplierName($ids)?></p>
			</div>
			<div class="subContent">
				<?php displayInvoice(); ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
	<?
include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );


function displayInvoicesInfos(){
	
	global $bs, $ids, $GLOBAL_START_URL, $supplierinvoice;

	$bls = SupplierInvoice::getInvoicedDeliveryNotes( $bs , $ids );
	
	if( count( $bls )>1 ){
	
		$title = "Bons de livraisons concernés : ";
	
		for($i=0;$i<count($bls);$i++){
			if($i)
				$title.="-";
		
			$title.=$bls[$i];	
		}	
	}else{
		$title = "Bon de livraison concerné : ".$bls[0];
	}
	
	$os = SupplierInvoice::getSupplierInvoiceOS ( $bs , $ids );
	
	if( count( $os )>1 ){
	
		$title2 = "Commandes fournisseur concernées : ";
	
		for($i=0;$i<count($os);$i++){
			if($i)
				$title2.="-";
		
			$title2.=$os[$i];	
		}	
	}else{
		$title2 = "Commande fournisseur concernée : ".$os[0];
	}
	
	?>
	
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	
	<script type="text/javascript">
	/* <![CDATA[ */
				
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
			 	dataType:  'xml',
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postSubmitCallback  // post-submit callback 
				
			};
			
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
				
				message: "Traitement en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
				
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseXML, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' ){
				
				alert( "Impossible d'effectuer le traitement" );
				return;
				
			}
			
			$.growlUI( '', 'Mise à jour effectuée avec succès' );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function priceFormat( value ){
		
			var ret = parseFloat( value ).toFixed( 2 );
			var reg = new RegExp( "(\\.)", "g" );
			ret = ret.replace( reg, "," );
			
			return ret + ' &euro;';
			
		}
		
		/* ----------------------------------------------------------------------------------- */
				
	/* ]]> */
	</script>
	
	<style type="text/css">
		<!--
		div#global{
			min-width:0;
			width:auto;
		}
		-->
	</style>
	
	<div class="mainContent" style="width:380px;margin-bottom:10px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Facture proforma <?=getSupplierName($ids)?> n° <?=$bs?></p>
			</div>
			<div class="subContent">
				<div><?=$title2?><br/><?=$title?></div>
				<div class="resultTableContainer" style="margin-top:20px;">
					<table class="dataTable">
						<tr>
							<th class="filledCell">N° de facture proforma Fournisseur</th>
								<td colspan="2"><?php echo $supplierinvoice->get("billing_supplier") ?></td>
							</tr>
							<tr>
								<th class="filledCell">Date de facture proforma</th>
								<td colspan="2"><?php echo usDate2eu( $supplierinvoice->get("Date") ) ?></td>
							</tr>
							<tr>
								<th class="filledCell">Date d'échéance</th>
								<td colspan="2"><?php echo usDate2eu( $supplierinvoice->get("deliv_payment") ) ?></td>
							</tr>
							<tr>
								<th class="filledCell">Mode de paiement</th>
								<td colspan="2"><?=DrawLift_modepayment( $supplierinvoice->get("idpayment"), false)?></td>
							</tr>
							<tr>
								<th class="filledCell">Délai de paiement</th>
								<td colspan="2"><?php echo DrawPaymentDelayList( $supplierinvoice->get("idpayment_delay"), false )?></td>
							</tr>
							<tr>
								<th class="filledCell">Montant TTC</th>
								<td colspan="2"><?php echo Util::priceFormat( $supplierinvoice->get("total_amount") ) ?></td>
							</tr>
							<?
							if( $supplierinvoice->get("rebate_value") > 0) {
							?>
							<tr>
								<th class="filledCell">Escompte</th>
								<td><?php echo Util::rateFormat( SupplierInvoice::getRebateRate( $supplierinvoice->get("billing_supplier"), $supplierinvoice->get("idsupplier") ) ) ?></td>
								<td><?php echo Util::priceFormat( SupplierInvoice::getRebateAmount( $supplierinvoice->get("billing_supplier"), $supplierinvoice->get("idsupplier") ) ) ?></td>
							</tr>
							<?php } ?>
							<tr>
								<th class="filledCell">Frais de port HT</th>
								<td colspan="2"><?php echo Util::priceFormat( $supplierinvoice->get("total_charge_ht") ) ?></td>
							</tr>
							<?
							if( $supplierinvoice->get("down_payment_value") > 0) {
							?>
								<tr>
									<th class="filledCell">Acompte</th>
									<td><?php echo Util::rateFormat( SupplierInvoice::getDownPaymentRate( $supplierinvoice->get("billing_supplier"),$supplierinvoice->get("idsupplier") ) ) ?></td>
									<td><?php echo Util::priceFormat( SupplierInvoice::getDownPaymentAmount( $supplierinvoice->get("billing_supplier"),$supplierinvoice->get("idsupplier") ) ) ?></td>
							</tr>
							<?php } ?>
					</table>
				</div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<div class="mainContent" style="width:380px;margin-bottom:10px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Paiements enregistrés</p>
			</div>
			<div class="subContent">
				<?
				$reguls = DBUtil::query( "SELECT rs.payment_date,rs.idpayment,rs.amount FROM regulations_supplier rs, billing_regulations_supplier brs WHERE brs.idsupplier = $ids AND brs.billing_supplier LIKE '$bs' AND rs.idsupplier = brs.idsupplier AND brs.idregulations_supplier = rs.idregulations_supplier" );
				
				if( $reguls->RecordCount() ){
				?>
				<div class="resultTableContainer" style="margin-top:10px;style="text-align:center;"">
					<table class="dataTable">
						<tr>
							<th class="filledCell" style="text-align:center;">Date</th>
							<th class="filledCell" style="text-align:center;">Mode</th>
							<th class="filledCell" style="text-align:center;">Montant</th>
						</tr>
						<?
						while( !$reguls->EOF() ){
						?>
							<tr>
								<td style="text-align:center;"><?php echo Util::dateFormat( $reguls->fields('payment_date') , 'd-m-Y' ) ?></td>
								<td style="text-align:center;"><?php echo DBUtil::getDBValue( "name_1" , "payment" , "idpayment" , $reguls->fields('idpayment') ) ?></td>
								<td style="text-align:center;"><?php echo Util::priceFormat( $reguls->fields('amount') ) ?></td>
							</tr>						
						<?
							$reguls->MoveNext();
						}
						?>
					</table>
				</div>
				<?php } else{?>
					Aucun paiement
				<?php } ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<?
}

//---------------------------------------------------------------------------------------------------------------------
function displayInvoice(){
	
	global $bs , $billings , $ids , $GLOBAL_START_URL;
	
	$total_rebate_amount= 0;
	$total_rebate_rate	= 0;
	$total_amount_ttc 	= 0;
	$total_charge_ht	= 0;
	$total_dp_amount	= 0;
	$total_dp_rate		= 0;
	
	// on créé les montants à mettre dans la facture
	for( $i = 0 ; $i < count( $billings ) ; $i++ ){
		$billing = new SupplierInvoice ( $billings[ $i ] , $ids );
		
		$total_rebate_amount+= ( SupplierInvoice::getRebateRate( $billing->get("billing_supplier"),$billing->get("idsupplier") ) * $billing->get( "total_amount" ) ) / 100 ;
		$total_amount_ttc 	+= $billing->get( "total_amount" );
		$total_charge_ht	+= $billing->get( "total_charge_ht" );
		$total_dp_amount	+= SupplierInvoice::getDownPaymentAmount( $billing->get("billing_supplier") , $billing->get("idsupplier") );
		
	}
	
	if( $total_rebate_amount > 0 ){
		$total_rebate_rate = round( $total_rebate_amount / $total_amount_ttc * 100, 2 );
	}
	
	if( $total_dp_amount > 0 ){
		$total_dp_rate = round( $total_dp_amount / $total_amount_ttc , 2 );
	}
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var optionsInvoices = {
			 	
				beforeSubmit:  preInvoiceSubmitCallback,  // pre-submit callback 
				success:	   postInvoiceSubmitCallback  // post-submit callback 
  				
			};
			
			$('#frm').ajaxForm( optionsInvoices );
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function postInvoiceSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' ){
			
				$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
				$.growlUI( '', "Impossible d'effectuer le traitement" );
												
			}
			
			if( responseText == "0" ){
				$.growlUI( '', 'Facture proforma enregistrée en facture(s)' );
				window.opener.goForm();
				setTimeout('self.close();',2000); 
			}else{
				$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
				$.growlUI( '', responseText );
			}
		}

		/* ----------------------------------------------------------------------------------- */
		
		function preInvoiceSubmitCallback( formData, jqForm, options ){
			
			errorDeliv = false;
			$( '#tablesBillings .deliv_payments' ).each(
				function(i){
					if( $(this).val().length < 10 || $(this).val() == '0000-00-00' )
						errorDeliv = true;
				}
			);
			
			if( errorDeliv ){
				$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
				$.growlUI( '', "Veuillez saisir une date d'échéance" );
				
				return false;
			}
			
			errorDatef = false;
			$( '#tablesBillings .Datefs' ).each(
				function(){
					if( $(this).val().length < 10 || $(this).val() == '0000-00-00' )
						errorDatef = true;
				}
			);
			
			if( errorDatef ){
				$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
				$.growlUI( '', 'Veuillez saisir une date de facture' );
				
				return false;
			}
			
			//sommeTTCs = 0;
		
			//$( '#tablesBillings' ).find( '.totalTTC' ).each(
			//	function(){
			//		sommeTTCs += parseFloat( $(this).val() );
			//	}
			//);
			
			//alert( sommeTTCs );
			
			//return false;
			$.blockUI({
				message: "Traitement en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700	
			}); 

			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function addBilling( id ){
		
			$.ajax({
				type: 'GET',
				url: "<?php echo $GLOBAL_START_URL; ?>/accounting/proforma_to_billing_supplier.php?ids=<?=$ids?>&getNewBilling=" + id,
				async: true,
				error: function(){ alert( 'Une couille inatendue est survenue!' ) },
			 	success: function( responseText ){
	
					$('#tablesBillings').append( responseText );
					
				}
	
			});
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<style type="text/css">
	
		div.mainContent div.content div.subContent table.dataTable input.price,
		div.mainContent div.content div.subContent table.dataTable input.percentage{ text-align:right; width:50px; }
			
	</style>

	<form id="frm" method="POST" action="<?=$GLOBAL_START_URL ?>/accounting/proforma_to_billing_supplier.php?ids=<?=$ids?>&amp;bs=<?php echo implode( ',' , array_map( 'urlencode' , $billings ) )?>">					
		<input type="hidden" name="registerInvoice" value="1" />
		<input type="hidden" name="billingCount" id="billingCount" value="1" />
		
		<div id="tablesBillings">
		<?php
		displayTable( 0 , $ids );
		?>
		</div>
		<div class="submitButtonContainer" style="margin-top:10px;">
			<!--
			<input type="button" name="addBillingButton" value="Ajouter une facture" class="blueButton" onclick="addBilling( $('#billingCount').val() ); $('#billingCount').val( parseInt( $('#billingCount').val() ) + 1 );" />
			-->
			<input type="submit" name="register" value="Enregistrer" class="blueButton" />
		</div>
		<div style="float:left; margin-top:10px;">Les champs marqués par <span class="asterix">*</span> sont obligatoires</div>
		<div class="clear"></div>
	</form>
	<?
}

//--------------------------------------------------------------------------------------------------

function displayTable( $a , $ids ){
	global $bs , $billings , $GLOBAL_START_URL , $grosTotalAmountTTC;
	
	?> 
	<div class="resultTableContainer" style="margin-top:20px;">
		<table class="dataTable">
			<tr>
				<th class="filledCell">N° de facture Fournisseur définitive <span class="asterix">*</span></th>
				<td colspan="2"><input type="text" name="billing_supplier_<?php echo $a; ?>" class="textInput" /></td>
			</tr>
			<tr>
				<th class="filledCell">Date de facture <span class="asterix">*</span></th>
				<td colspan="2">
					<input type="text" name="Datef_<?php echo $a; ?>" id="Datef_<?php echo $a; ?>" class="calendarInput Datefs" value="" />
					<?php DHTMLCalendar::calendar( "Datef_$a" ); ?>
				</td>
			</tr>
			<tr>
				<th class="filledCell">Date d'échéance <span class="asterix">*</span></th>
				<td colspan="2">
					<input type="text" name="deliv_payment_<?php echo $a; ?>" id="deliv_payment_<?php echo $a; ?>" class="calendarInput deliv_payments" value="" />
					<?php DHTMLCalendar::calendar( "deliv_payment_$a" ); ?>
				</td>
			</tr>
			<tr>
				<th class="filledCell">Mode de paiement <span class="asterix">*</span></th>
				<td colspan="2"><?php
				$idpaiement = getSupplierIdPayment($ids);
				//DrawLift_modepayment($idpaiement);
				$table = "payment";
				$columnAsValue = "idpayment";
				$columnAsInnerHTML = "name_1";
				$orderByColumn = false;
				$selectedValue = $idpaiement;
				$nullValue = false;
				$nullInnerHTML = "";
				$attributes = "name='idpayment_$a'"; 
				$returnHtml = false; 
				$ignoreValue = false; 
				$SQL_Condition = "";
				
				XHTMLFactory::createSelectElement( $table, $columnAsValue, $columnAsInnerHTML, $orderByColumn, $selectedValue, $nullValue, $nullInnerHTML, $attributes, $returnHtml, $ignoreValue, $SQL_Condition );
				?></td>
			</tr>
			<tr>
				<th class="filledCell">Délai de paiement <span class="asterix">*</span></th>
				<td colspan="2"><?php
				$pay_delay = getSupplierPaymentDelay($ids);						
				//DrawPaymentDelayList($pay_delay)
				$table = "payment_delay";
				$columnAsValue = "idpayment_delay";
				$columnAsInnerHTML = "payment_delay_1";
				$orderByColumn = false;
				$selectedValue = $pay_delay;
				$nullValue = false;
				$nullInnerHTML = "";
				$attributes = "name='idpayment_delay_$a'"; 
				$returnHtml = false; 
				$ignoreValue = false; 
				$SQL_Condition = "";
				
				XHTMLFactory::createSelectElement( $table, $columnAsValue, $columnAsInnerHTML, $orderByColumn, $selectedValue, $nullValue, $nullInnerHTML, $attributes, $returnHtml, $ignoreValue, $SQL_Condition );
				?></td>
			</tr>
			<tr>
				<th class="filledCell">Montant TTC <span class="asterix">*</span></th>
				<td colspan="2"><input type="text" id="total_amount_ttc_<?php echo $a; ?>" name="total_amount_ttc_<?php echo $a; ?>" class="textInput price totalTTC" value="<?=$grosTotalAmountTTC?>" /> &euro;</td>
			</tr>
			<tr>
				<th class="filledCell">Escompte</th>
				<td colspan="2"><input type="text" name="rebate_rate_<?php echo $a; ?>" class="textInput percentage" value="" /> %</td>
			</tr>
			<tr>
				<th class="filledCell">Frais de port HT <span class="asterix">*</span></th>
				<td colspan="2"><input type="text" id="total_charge_ht_<?php echo $a; ?>" name="total_charge_ht_<?php echo $a; ?>" class="textInput price" value="" />  &euro;</td>
			</tr>
			<tr>
				<th class="filledCell">Acompte</th>
				<td><input disabled="disabled" type="text" name="down_payment_rate_<?php echo $a; ?>" class="textInput percentage" value="" /> %</td>
				<td><input disabled="disabled" type="text" name="down_payment_amount_<?php echo $a; ?>" class="textInput price" value="" /> &euro;</td>
			</tr>
			<tr>
				<th class="filledCell">Facture document</th>
				<td colspan="2"><input type="file" name="billing_supplier_doc" class="calendarInput"/></td>
			</tr>
		</table>
	</div><?
}

//---------------------------------------------------------------------------------------------------
function displayAmounts(){
	
	$bl = $_GET["bl"];
	$bl = preg_replace('`([^0-9,]+)`iS','',$bl);
	$bls = explode(",",$bl);

	/**
	 * Commandes fournisseurs
	 */
	
	$in='';
	
	for($i=0;$i<count($bls);$i++){
		
		$idbl_delivery = $bls[$i];
		
		if(!$i)
			$in .= $idbl_delivery;
			
		$in .= ",".$idbl_delivery;
	
	}
	
	$query = "SELECT DISTINCT(idorder_supplier)
				FROM bl_delivery
				WHERE idbl_delivery IN ($in)";
	
	$rs = DBUtil::query($query);
	
	if( $rs === false )
		die("Impossible de récupérer les commandes fournisseurs");
	
	$totalHTToInvoice = 0;
	$totalTTCToInvoice = 0;
	$chargesET = 0.0;
	
	for($i=0;$i<$rs->RecordCount();$i++){
		
		$idorder_supplier = $rs->fields("idorder_supplier");
		
		$SupplierOrder = new SupplierOrder( $idorder_supplier );
		
		$totalHTToInvoice += round( $SupplierOrder->getTotalET(), 2 );
		$totalTTCToInvoice += round($SupplierOrder->getTotalATI(), 2 );
		$chargesET += round( $SupplierOrder->getChargesET(), 2 );
		
		$j = 0;
		while( $j < $SupplierOrder->getItemCount() ){
		
			echo"<totalrow".$j.$idorder_supplier."><![CDATA[".round($SupplierOrder->getItemAt($j)->get("quantity")*$SupplierOrder->getItemAt($j)->get("discount_price"), 2)."]]></totalrow".$j.$idorder_supplier.">";
		
			$j++;
		}
		
		echo"<totalosht$idorder_supplier><![CDATA[".round($SupplierOrder->getTotalET(),2)."]]></totalosht$idorder_supplier>";
		echo"<totalosttc$idorder_supplier><![CDATA[".round($SupplierOrder->getTotalATI(),2)."]]></totalosttc$idorder_supplier>";
	
		$rs->MoveNext();
	}
	
	echo"<totalht><![CDATA[$totalHTToInvoice]]></totalht>";
	echo"<totalttc><![CDATA[$totalTTCToInvoice]]></totalttc>";
	echo"<chargeset><![CDATA[" . Util::numberFormat( $chargesET, 2 ) . "]]></chargeset>";
		
}

function registerInvoice(){
	
	$bs = $_GET["bs"];
	$billings = explode( ',' , $bs );
	$ids = $_GET["ids"];
	$idsupplier = $ids;
	
	//------------------------------------------------------------------------------------------------
	//Traitement du formulaire
	
	$error = 0;
	$Msgerror = '';
	
		
	//Test des champs postés
	$fields = array ( "billing_supplier" , "Datef" , "deliv_payment" , "total_amount_ttc", "total_charge_ht"  );
	
	$nbBils = $_POST['billingCount'];
	
	for( $a = 0 ; $a < $nbBils ; $a++ ){
		for ( $i = 0 ; $i < count( $fields ) ; $i++ ){
			if( empty( $_POST[ $fields[$i].'_'.$a ] ) && $fields[ $i ].'_'.$a != 'total_charge_ht' ){
				$error = $fields[$i].'_'.$a;
			}
		}
	}
	
	if ( $error != 0 ){
		exit("Tous les champs doivent être remplis $error");
	}else{
		for( $a = 0 ; $a < $nbBils ; $a++ ){
			//Vérification du numéro de facture fournisseur pour voir si il n'existe pas
			$billing_supplier = $_POST["billing_supplier_$a"];
			
			$query = "SELECT billing_supplier FROM billing_supplier WHERE billing_supplier='$billing_supplier' AND idsupplier=$ids";
			$rs = DBUtil::query($query);
			
			if($rs === false)
				die("Impossible de vérifier le numéro de facture fournisseur" );
				
			if($rs->RecordCount()>0){
				exit( "Ce numéro de facture fournisseur existe déjà" );
				
			}else{
				//Tous les champs sont remplis, test des champs numériques
				if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "total_amount_ttc_$a" ] ) ) {
					$error = 1;
					exit( "Le champ montant TTC doit être un nombre" );
							
				}else{
					if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "total_charge_ht_$a" ] ) ) {
						$error = 1;
						exit( "Le champ frais de port HT doit être un nombre" );
						
					}else{
						if( !empty( $_POST[ "rebate_rate_$a" ] ) ){
							if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "rebate_rate_$a" ] ) ) {
								$error = 1;
								exit( "Le champ taux d'escompte doit être un nombre" );
								
							}
						}else{
							if( !empty( $_POST[ "rebate_amount_$a" ] ) ){
								if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "rebate_amount_$a" ] ) ) {
									$error = 1;
									exit( "Le champ montant d'escompte doit être un nombre" );
									
								}
							}
						}
					}
				}
			}
		}
	}
	
	if( $error == 0 ) {
		
		for( $a = 0 ; $a < $nbBils ; $a++ ){
			//Toutes les données sont ok, on peut traiter l'enregistrement de la facture, on enregistre pas si tt les form ne sont pas ok
			$down_payment_value = 0.00;
			
			$status = 'Paid';
			$billing_supplier = $_POST[ "billing_supplier_$a" ];
			
			for( $i = 0 ; $i < count( $billings ) ; $i++ ){
				
				$bs = $billings[ $i ];
				$bls = SupplierInvoice::getInvoicedDeliveryNotes( $bs , $ids );
				$proformainvoice = new SupplierInvoice ( $bs , $ids );
				
				$down_payment_value += SupplierInvoice::getDownPaymentAmount( $bs , $ids );
				
				if( $proformainvoice->get("status") == 'StandBy' ){
					$status = 'StandBy';
				}
				
				DBUtil::query( "INSERT INTO billing_proforma VALUES( '$billing_supplier' , '$bs' )" );
				
			}
			
			$Date 				= $_POST[ "Datef_$a" ];
			$total_amount_ttc 	= Util::text2num( $_POST[ "total_amount_ttc_$a" ] );
			$total_charge_ht 	= Util::text2num( $_POST[ "total_charge_ht_$a" ] );
			$idpayment 			= $_POST[ "idpayment_$a" ];
			$idpayment_delay 	= $_POST[ "idpayment_delay_$a"];
			$creation_date 		= date("Y-m-d");
			$rebate_rate 		= Util::text2num( $_POST[ "rebate_rate_$a" ] );
			
			//On créé une nouvelle facture fournisseur
			$supplierinvoice = SupplierInvoice::create( $billing_supplier , $ids , $bls );
			
			//On calcule la date d'échéance
			$deliv_payment = $_POST[ "deliv_payment_$a" ];
			
			//On affecte les valeurs
			$supplierinvoice->set( "Date" , euDate2us($Date) );
			$supplierinvoice->set( "deliv_payment" , euDate2us($deliv_payment) );
			$supplierinvoice->set( "idpayment_delay" , $idpayment_delay );
			$supplierinvoice->set( "idpayment" , $idpayment );
			$supplierinvoice->set( "total_amount" , $total_amount_ttc );
			$supplierinvoice->set( "total_charge_ht" , $total_charge_ht );
			$supplierinvoice->set( "creation_date" , $creation_date );
			$supplierinvoice->set( "proforma_id" , $bs );	
			$supplierinvoice->set( "down_payment_type" , "amount" );
			$supplierinvoice->set( "down_payment_value" , $down_payment_value );
			$supplierinvoice->set( "status" , $status );

			//PJs
			if( isset( $_FILES[ "billing_supplier_doc" ] ) && is_uploaded_file( $_FILES[ "billing_supplier_doc" ][ "tmp_name" ] ) ){
		
				$YearFile = date("Y");
				$DateFile = date("d-m");
					
				// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
				// pas besoin d'uploader 15 fois
			
				$fieldname = "billing_supplier_doc";
			
				$target_dir = DOCUMENT_ROOT . "/data/billing_supplier/$YearFile/$idsupplier/";
				$prefixe = "Fact";
				
				$name_file = $_FILES[ $fieldname ][ 'name' ];
				$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
				
				$extension = substr( $name_file , strrpos( $name_file , "." ) );
				
				if(($extension!='.doc')&&($extension!='.jpg')&&($extension!='.xls')&&($extension!='.pdf')&&($extension!='.DOC')&&($extension!='.JPG')&&($extension!='.XLS')&&($extension!='.PDF')){
		   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./supplier_order.php?IdOrder=".$IdOrder."\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
					return false;
				}
				
				// on copie le fichier dans le dossier de destination
		    	$dest = $target_dir.$prefixe."_".$DateFile."_".$name_file;
				$newdoc = $YearFile."/".$idsupplier."/".$prefixe."_".$DateFile."_".$name_file;
			
				//billing_supplier existe
				//rep année	
				
				if( !file_exists( DOCUMENT_ROOT . '/data/billing_supplier/'.$YearFile ) )
					if( mkdir( DOCUMENT_ROOT.'/data/billing_supplier/'.$YearFile ) === false )
						die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
					
				//if( chmod( DOCUMENT_ROOT.'/data/billing_supplier/'.$YearFile, 0755 ) === false )
					//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
					
				if( !file_exists( DOCUMENT_ROOT.'/data/billing_supplier/'.$YearFile.'/'.$idsupplier ) )
					if( mkdir( DOCUMENT_ROOT.'/data/billing_supplier/'.$YearFile.'/'.$idsupplier ) === false )
						die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
					
				//if( chmod( DOCUMENT_ROOT.'/data/billing_supplier/'.$YearFile.'/'.$idsupplier , 0755 ) === false )
					//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
		    	
		    	if( !move_uploaded_file($tmp_file, $dest) )
		    	{
		        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
		    	}
			
				//if( chmod( $dest, 0755 ) === false )
					//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
						
				$supplierinvoice->set( $fieldname , $newdoc );		
			}			
			
			//On sauvegarde
			$supplierinvoice->save();
			
			//on dit a la commande fournisseur que la facture est réceptionnée. bisou
			$ordersSupplier = &SupplierInvoice::getSupplierInvoiceOS( $billing_supplier , $ids );
			$countOS = count($ordersSupplier);
			
			if($countOS){
				$osToInit = implode( "," , $ordersSupplier );
				
				DBUtil::query("UPDATE order_supplier SET status='invoiced' WHERE idorder_supplier IN($osToInit)");
			}
			
			$supplierinvoice->set( "rebate_type", "rate" );
			$supplierinvoice->set( "rebate_value", $rebate_rate );
				
			//On sauvegarde
			$supplierinvoice->save();	

			//transfert des règlements enregistrés si y'en a
			//SupplierInvoice::forwardRegulationsSupplier( $bs, $billing_supplier, $ids );
			// eh ben non on transfert plus et toc ! ;-)	
		}
	}
	
	
		
	return "0";

}   
	
  	

//------------------------------------------------------------------------------------------------------
function getSupplierName( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du fournisseur" );
		
	return $rs->fields( "name" );
	
}

//------------------------------------------------------------------------------------------------------
function getSupplierPaymentDelay( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT pay_delay FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le délai de paiement du fournisseur" );
		
	return $rs->fields( "pay_delay" );
	
}

//------------------------------------------------------------------------------------------------------
function getSupplierIdPayment( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT idpaiement FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le mode de paiement du fournisseur" );
		
	return $rs->fields( "idpaiement" );
	
}
?>