﻿<?php
/*
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Rapprochements bancaires
 */

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type: text/html; charset=UTF-8");

//------------------------------------------------------------------------------------------------
include_once("../objects/classes.php");
include_once("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
include_once("$GLOBAL_START_PATH/objects/SupplierInvoice.php");

$Title = "Factures fournisseurs et proforma à payer";

include_once("$GLOBAL_START_PATH/catalog/admin_func.inc.php");
include_once("$GLOBAL_START_PATH/catalog/drawlift.php");

$con = DBUtil::getConnection();

$hasAdminPrivileges = User::getInstance()->get("admin");
$iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

$resultPerPage = 50000;
$maxSearchResults = 1000;

function getMatchAccountListingAndAccountPlanRecord()
{
    $SQL_Condition = "
        ap.amount = al.amount
        AND ap.account_plan_1 = 'BQ1'
		AND ap.idaccount_plan = '51211000'
        AND al.bank_dating IS NULL
        AND ap.idaccounts_listing IS NULL";
		
		if (isset($_POST["bankcompany"]) && ($_POST["bankcompany"]!=0) )
		{
			$code_bank = $_POST["bankcompany"];
			 $SQL_Condition .= " AND al.code_bank = '". $code_bank ."'";	
		
		}
    //recherche
    $SQL_Query = "
	SELECT al.heading, al.operation_date, al.value_date,al.code_bank, ap.designation, ap.date, ap.amount,ap.idrow,al.idaccounts_list,ap.`amount_type`
	FROM account_plan as ap, accounts_listing as al
	WHERE $SQL_Condition
	ORDER BY al.buyer_name ASC, ap.amount ASC";
	//echo "tab 1 Query ".$SQL_Query;
    $rs = DButil::query($SQL_Query);
    return $rs;
}

if (isset($_POST["idaccounts_list"]) && isset($_POST["bank_match"])) {
        // print_r($_POST["idaccounts_list"]);
    $error = array();
    foreach ($_POST["idaccounts_list"] as $key => $value) {
        $split = explode(":", $value);
        $idaccounts_list = $split[0];
        $idrow = $split[1];
       $rs = DButil::query("  SELECT operation_date,amount
						FROM `accounts_listing`
						WHERE `idaccounts_list` =' " . Util::html_escape($idaccounts_list) . "'");
			
			$current = $rs->fetchRow();
			$value_date = $current['operation_date'];
			$amount = $current['amount'];
		$rs = DButil::query("UPDATE accounts_listing SET bank_dating='" . Util::html_escape(date('Y-m-d H:m:s')) . "' WHERE idaccounts_list='" . Util::html_escape($idaccounts_list) . "'");
        if ($rs === false) {
            $error[] = "Cannot update accounts_listing with id " . $idaccounts_list . "\n";
        }
		$rs = DButil::query("  SELECT idaccount_record,date,idrelation,record_type
						FROM `account_plan`
						WHERE `idrow` ='" . Util::html_escape($idrow) . "'");
			$current = $rs->fetchRow();
			$idaccount_record = $current['idaccount_record'];
			$dates = $current['date'];
			$idrelation = $current['idrelation'];
			$record_type = $current['record_type'];
            $q = "UPDATE account_plan SET idaccounts_listing='" . Util::html_escape($idaccounts_list) . "'
				, date = '". $value_date . "'	
				WHERE  amount='". $amount . "' AND idrow='" . Util::html_escape($idrow) . "' AND idaccount_plan = '51211000'
				 ";
				  if ($idaccount_record) {
				  $qd = "UPDATE account_plan SET 
				 date = '". $value_date . "'	
				WHERE  idaccount_record='". $idaccount_record . "'  AND record_type='". $record_type . "'
				 ";
				 }
				 else {
				   $qd = "UPDATE account_plan SET 
				 date = '". $value_date . "'	
				WHERE amount='". $amount . "' AND date='". $dates . "' AND idrelation='". $idrelation . "'
				 ";
				 }
        $rs = DButil::query($q);
	    $rs = DButil::query($qd);
        if ($rs === false) {
            $error[] = "Cannot update account plan with id " . $idrow . "\n";
        }
    }
    if (count($error) == 0) {
        echo json_encode(array("error" => "0", "des" => ""));
    } else {
        echo json_encode(array("error" => "1", "des" => $error));
    }
    exit();
}
if (isset($_POST["accounts_listing"]) && isset($_POST["bank_match_list_selected"])) {
   //   print_r('xxxxxx');
    $error = array();
    $total = 0;
    foreach ($_POST["accounts_listing"] as $key => $value) {
        $split = explode(":", $value);
        $idrow = $split[0];
        $idaccounts_list = $split[1];
        $rs = DButil::query("SELECT amount FROM account_plan WHERE idrow='" . $idrow . "'");
        $rs = $rs->fetchRow();
        $total += $rs['amount'];
    }
    $accounts_listing_idrow = explode(":", $value);
    $accounts_listing_idrow = $accounts_listing_idrow[1];
    $accounts_listing = DButil::query("
    SELECT amount
	FROM accounts_listing
	WHERE idaccounts_list='" . Util::html_escape($accounts_listing_idrow) . "'");
    $accounts_listing = $accounts_listing->fetchRow();
	
	$total = number_format($total,2,'.',''); 
	$accounts_listing['amount'] = number_format($accounts_listing['amount'],2,'.',''); 
	
    /*if ($total == $accounts_listing['amount']) {*/
        foreach ($_POST["accounts_listing"] as $key => $value) {
            $split = explode(":", $value);
            $idrow = $split[0];
            $idaccounts_list = $split[1];
            $rs = DButil::query("UPDATE accounts_listing SET bank_dating='" . Util::html_escape(date('Y-m-d H:m:s')) . "' WHERE idaccounts_list='" . Util::html_escape($idaccounts_list) . "'");
            if ($rs === false) {
                $error[] = "Cannot update accounts listing with id " . $idaccounts_list . "\n";
            }
			
			
			
			$rs = DButil::query("  SELECT idaccount_record,date,record_type,idrelation,amount
						FROM `account_plan`
						WHERE `idrow` ='" . Util::html_escape($idrow) . "'");
			$current = $rs->fetchRow();
			$idaccount_record = $current['idaccount_record'];
			$dates = $current['date'];
			$record_type = $current['record_type'];
			$idrelation = $current['idrelation'];
			$amounts = $current['amount'];
			$rss = DButil::query("  SELECT value_date
						FROM `accounts_listing`
						WHERE `idaccounts_list` =' " . Util::html_escape($idaccounts_list) . "'");
			
			$currents = $rss->fetchRow();
			$value_date = $currents['value_date'];
			
			
			 $q = "UPDATE account_plan SET idaccounts_listing='" . Util::html_escape($idaccounts_list) . "'
				, date = '". $value_date . "'	
				WHERE  idrow='" . Util::html_escape($idrow) . "' AND idaccount_plan = '51211000'
				 ";
				   if ($idaccount_record) {
				     $qd = "UPDATE account_plan SET 
				 date = '". $value_date . "'	
				WHERE idaccount_record='". $idaccount_record . "'  AND record_type='". $record_type . "' ";
				 } else {
				   $qd = "UPDATE account_plan SET 
				 date = '". $value_date . "'	
				WHERE amount='". $amounts . "' AND date='". $dates . "' AND idrelation='". $idrelation . "'
				 ";
				
				 }
        $rs = DButil::query($q);
	    $rs = DButil::query($qd);
			
			//	die($q);		
            if ($rs === false) {
                $error[] = "Cannot update account plan with id " . $idrow . "\n";
            }
        }
    /*} else {
        $error[] = "Les la somme des montants ne correspond pas au montant total sélectionné précédemment.\n";
    }*/
    if (count($error) == 0) {
        echo json_encode(array("error" => "0", "des" => ""));
    } else {
        echo json_encode(array("error" => "1", "des" => $error));
    }
    exit();
}
if (isset($_POST["idaccounts_list"]) && isset($_POST["bank_match_list"])) {
    $error = array();
    $data = array();
    $SQL_Query = "
	SELECT *
	FROM account_plan
	WHERE account_plan_1= 'BQ1' AND idaccount_plan = '51211000' AND idaccounts_listing IS NULL
	ORDER BY amount ASC";
	

    $rs = DButil::query($SQL_Query);

    if ($rs === false) {
        $error[] = "Cannot retrieve account list";
    }

    $SQL_Query = "
	SELECT amount,amount_type
	FROM accounts_listing
	WHERE idaccounts_list='" . Util::html_escape($_POST["idaccounts_list"]) . "'";
	//echo $SQL_Query;
    $accounts_listing = DButil::query($SQL_Query);
	if ($accounts_listing === false) {
        $error[] = "Selected accounts listing not found.";
    }
    $accounts_listing = $accounts_listing->fetchRow();
    while (!$rs->EOF()) {
        $current = $rs->fetchRow();
		
		$current["amount"] = $current["amount_type"] == 'credit' ? -1 * $current["amount"] : $current["amount"];
		
        $current["accounts_listing_id"] = $_POST["idaccounts_list"];
		
		//Apply to Account Plan Total
	   $current["accounts_listing_amount"] = $accounts_listing["amount_type"] == 'debit' ? -1 * $accounts_listing["amount"] : $accounts_listing["amount"];
       
	    $current["designation"] = Util::doNothing($current["designation"]);
		$current["payment"] = Util::doNothing($current["payment"]);
	    $data[] = $current;
    }

    if (count($error) == 0) {
        echo json_encode(array("error" => "0", "des" => "", "data" => $data));
    } else {
        echo json_encode(array("error" => "1", "des" => $error));
    }
    exit();
}

include("$GLOBAL_START_PATH/templates/back_office/head.php");
?>

<div id="globalMainContent">
<?php
search();
    ?>
</div><!--GlobalMainContent-->
<?php

include("$GLOBAL_START_PATH/templates/back_office/foot.php");

/////-----------------------

function search()
{

    global $resultPerPage, $hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH, $ScriptName;

    $page = isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;

    ?>
<style type="text/css">
    .apColumn {
        background: #e6e6fa;
    }

    .alColumn {
        background: #98fb98;
    }
</style>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css"/>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
<script type="text/javascript" language="javascript">
    function goToPage(pageNumber) {
        document.forms.bank_match.elements[ 'page' ].value = pageNumber;
        document.forms.bank_match.submit();
    }
</script>

<script type="text/javascript">
    $(document).ready(function() {

        var options = {

            beforeSubmit:    preSubmitCallback,  // pre-submit callback
            success:        postSubmitCallback  // post-submit callback

        };
        var options_2 = {

            beforeSubmit:    preSubmitCallback,  // pre-submit callback
            success:        listPostSubmitCallback  // post-submit callback

        };
        var options_3 = {

            beforeSubmit:    preSubmitCallback,  // pre-submit callback
            success:        SelectedAccountPlantsPostSubmitCallback  // post-submit callback

        };
        $('#bank_match').ajaxForm(options);
        $('#bank_match_list').ajaxForm(options_2);
        $('#account_listing_form').ajaxForm(options_3);
        $('#zizi').tabs({ fxFade: true, fxSpeed: 'fast' });
		
		if ($('#account_listing_form') != 'undefined') {
			$('#account_listing .submitButtonContainer input[type="submit"]').click( function() {
				if ( !confirm("La somme des montants sélectionnés ne correspond pas au montant attendu. Voulez-vous enregistrer malgré tout ?") )
					return false;
			});
		}
    });

    function preSubmitCallback(formData, jqForm, options) {

        $.blockUI({

            message: "Recherche en cours",
            css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
            fadeIn: 0,
            fadeOut: 700

        });

        $('.blockOverlay').attr('title', 'Click to unblock').click($.unblockUI);

    }

    function postSubmitCallback(responseText, statusText) {

        $.unblockUI();

        if (statusText != 'success') {

            $.growlUI('', 'Impossible d\'effectuer la recherche');
            return;

        }

        var result = $.parseJSON(responseText, true);
        if (result.error == 0) {
            document.location.replace(location.protocol+"//"+location.host+location.pathname+"#factures");
            document.location.reload();
        } else {
            $.growlUI('', result.des);
        }
    }
    function SelectedAccountPlantsPostSubmitCallback(responseText, statusText) {

        $.unblockUI();

        if (statusText != 'success') {

            $.growlUI('', 'Impossible d\'effectuer la recherche');
            return;

        }

        var result = $.parseJSON(responseText, true);
        if (result.error == 0) {
            document.location.replace(location.protocol+"//"+location.host+location.pathname+"#lists");
            document.location.reload();
        } else {
            $.growlUI('', result.des);
        }
    }
    function listPostSubmitCallback(responseText, statusText) {

        $.unblockUI();

        if (statusText != 'success') {

            $.growlUI('', 'Impossible d\'effectuer la recherche');
            return;

        }

        var result = $.parseJSON(responseText, true);
        if (result.error != 'undefined' && result.error == 0) {
            $("#account_listing_table tbody").empty();
            $("#account_listing_table tbody").html("");
            var trs = "";
            $.each(result.data, function(index, item) {
                trs += ("<tr class='amountRow'><td class='lefterCol'>" + item.amount + "</td><td class='apColumn'>" + item.date + "</td><td class='apColumn'>" + item.designation + "</td><td class='apColumn'>" + item.payment + "</td><td><input type='checkbox' name='accounts_listing[]' onclick='updateAccountListTotalAmount(" + item.accounts_listing_amount + ")' value='" + item.idrow + ':' + item.accounts_listing_id + "'/><input type='hidden' name='ammount' value='" + item.amount + "' /> </td></tr>");
            });
            $("#account_listing_table tbody").html(trs);
            $("#account_listing").dialog({
                width: 700,
                height: 400,
                autoOpen: true,
                title: ""
            });
            $("#account_listing").dialog("open");
            $("#account_listing").dialog("option", "title", "");
            $("#account_listing tfoot").html('<tr><td class="totalAmount" colspan="3">Account Plan Total:  ' + result.data[0].accounts_listing_amount + ' &euro;</td><td colspan="4" class="amount"></td><td style="border-style:none;"></td></tr></tfoot>');
            $("#account_listing_table").tablesorter({
                sortList:[[1,0]]/*,
                headers: {
					0: { sorter: false },
                    2: { sorter: false },
                    4: { sorter: false }
                }*/
            });
        } else {
            $.growlUI('', result.des);
        }
    }
    function updateAccountListTotalAmount(account_plan_amount) {
        var amount = 0;
//        console.log(account_plan_amount);
        jQuery("#account_listing input:checked").each(function(index, item) {
            amount = parseFloat(amount) + parseFloat(jQuery(item).next().val());
            amount = parseFloat(amount.toFixed(2));
        });
//        console.log(amount);
        if (parseFloat(amount) == parseFloat(account_plan_amount)) {
            $("#account_listing tfoot .amount").css({
                background: "green",
                color: "white"
            }).html(amount + ' &euro;');
            /*
			//$("#account_listing .submitButtonContainer").html('<input type="submit" value="Enregistrer le paiement" class="blueButton noprint"/>');
			//$('#account_listing .submitButtonContainer input[type="submit"]').removeAttr('disabled');
			*/
        } else {
            $("#account_listing tfoot .amount").css({
                background: "red",
                color: "black"
            }).html(amount + ' &euro;');
            /*
			//$("#account_listing .submitButtonContainer").html('<input type="button" value="Enregistrer le paiement" class="blueButton noprint"/>');
			$('#account_listing .submitButtonContainer input[type="submit"]').attr('disabled', 'disabled');
			*/
        }
        $("#account_listing").dialog("option", "title",  amount + ' / ' + account_plan_amount + ' &euro;');
    }
</script>


<?php
    $rs = getMatchAccountListingAndAccountPlanRecord();//tab1
    $resultCount = $rs->RecordCount();
	
	$where = "";
	
	if (isset($_POST["bankcompany"]) && ($_POST["bankcompany"]!=0) )
		{
			$code_bank = $_POST["bankcompany"];
			 $where .= " AND code_bank = '". $code_bank ."'";	
		
		}
	
	
	
    $SQL_Query = "
    SELECT *
    FROM accounts_listing
    WHERE bank_dating IS NULL
	$where
    ORDER BY amount ASC";
	// echo "tab 2 Query:<br/>".$SQL_Query ;

			

    $list = DButil::query($SQL_Query);
    $listResultCount = $list->RecordCount();
    ?>
<div class="mainContent fullWidthContent">
    <div class="topRight"></div>
    <div class="topLeft"></div>
    <div id="zizi">
        <ul class="menu noprint" style="margin-bottom:10px;">
            <li class="item">
                <div class="right"></div>
                <div class="left"></div>
                <div class="centered"><a id="facture" href="#factures">Montants identiques (<?=$resultCount?>)</a>
                </div>
            </li>
            <li class="item">
                <div class="right"></div>
                <div class="left"></div>
                <div class="centered"><a id="list" href="#lists">Somme des montants (<?=$listResultCount?>)</a></div>
            </li>
        </ul>
<!--tab1-->


		<div align="right">
        <table>
        	<tr>
        		<td><h3>Affichage par banque:</h3></td>
        
        		<td>
        
        			<form method="post" enctype="multipart/form-data" action="<?php echo $_SERVER[ "PHP_SELF" ] ?>">
       
       
        				<select name="bankcompany" id="bankcompany">
                           	<option value="0">--Sélectionner une banque--</option>
								<?php
					
									$q1 = "SELECT idbank_company, name
											FROM bank_company";
													
									$rs1 = & DBUtil::query( $q1 );
										
									while(!$rs1->EOF())
									{
										$idbank_company = $rs1->fields("idbank_company");
										$name = $rs1->fields("name");
											
										if ($idbank_company == $_POST["bankcompany"])
											echo '<option value="' . $idbank_company . '" selected="selected">' . $name . '</option>';
											
										else
											echo '<option value="' . $idbank_company . '">' . $name . '</option>';
											
										$rs1->MoveNext();

									}

								?>
						</select>
				</td>
				               
				<td>                                
        			<div class="submitButtonContainer" style="float:right"><input type="submit" value="Rechercher" class="blueButton" /></div>
				</td>
			</tr>
		</table>
        			</form>
       
        </div>

		<br /><br />
        <div class="content" id="factures">
            <form action="<?php echo $ScriptName ?>" method="post" name="bank_match" id="bank_match">
                <input type="hidden" name="page"
                       value="<?php echo isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1 ?>"/>
                <input type="hidden" name="bank_match"/>

                <div class="headTitle">
                    <p class="title" id="titleBillings_1">
                        <?php if ($resultCount == 0) { ?>
                        <span style="font-size: 14px;font-weight: bold; color: #008000;">Aucune facture fournisseur à payer !</span>
                        <?php } else { ?>
                        Résultats de la recherche: <?php echo $resultCount ?> facture(s) fournisseurs à payer - Total TTC à
                        payer :
                        <?php } ?>
                    </p>
                </div>
                <div class="subContent">
                    <div class="tableHeaderLeft" style="margin-bottom:5px;"></div>
                    <div class="tableContainer clear">
                        <table class="dataTable resultTable tablesorter" id="account_table">
                            <thead>
                            <tr>
                            	<th style="vertical-align:middle;" class="sortable">Code banque</th>
                                <th style="vertical-align:middle;width:70px;" class="sortable">Montant</th>
                                <th style="vertical-align:middle;" class="sortable">Intitulé</th>
                                <th style="vertical-align:middle;" class="sortable">Date création</th>
                                <th style="vertical-align:middle;" class="sortable">Date opération</th>
                                <th style="vertical-align:middle;" class="sortable">Date valeur</th>
                                <th style="vertical-align:middle;" class="sortable">Intitulé</th>
                                <th style="vertical-align:middle;width:30px;"></th>
                            </tr>
                            </thead>
                            <tbody>
<?php
                 if ($resultCount > 0) {

while (!$rs->EOF()) {

	/*$counter = 0;
	
	$counter++;*/
		$amount = $rs->fields( "amount_type" ) == 'credit' ? -1 * $rs->fields( "amount" ) : $rs->fields( "amount" );
	/*	$date_creation = $date_record_temp =  $rs->fields("date");
		
			 $rs->MoveNext();
			 if(!$rs->EOF){
				 $amount_next = $rs->fields( "amount_type" ) == 'credit' ? -1 * $rs->fields( "amount" ) : $rs->fields( "amount" );
				 if($amount_next == $amount )
				 {
					 $date_creation .= '<br />'.$rs->fields("date");
				 }else{
					$rs->Move($counter-1);
				 }
			 }
			 else{
				$rs->Move($counter-1);
			 }
		*/
		
		$qr1 = "SELECT code 
				FROM bank_company
				WHERE idbank_company ='". $rs->fields("code_bank") ."'
				";
			
		$rs1 =& DBUtil::query( $qr1 );
			
		$code_bank = $rs1->fields("code");
		$heading = $rs->fields("heading");
		$date_creation = $rs->fields("date");
        $operation_date = $rs->fields("operation_date");
        $value_date = $rs->fields("value_date");
        $desc = $rs->fields("designation");
        $idrow = $rs->fields("idrow");
        $idaccounts_list = $rs->fields("idaccounts_list");
        ?>
    <tr class="amountRow">
    	<td class="apColumn"><?php echo ($code_bank !="") ? $code_bank : "-" ?></td>
        <td class="lefterCol"><?php echo $amount ?></td>
        <td class="apColumn"><?php echo $desc ?></td>
        <td class="apColumn"><?php echo $date_creation ?></td>
        <td class="alColumn"><?php echo $operation_date ?></td>
        <td class="alColumn"><?php echo $value_date ?></td>
        <td class="alColumn"><?php echo $heading ?></td>
        <td><input type="checkbox" name="idaccounts_list[]" value="<?php echo $idaccounts_list . ":" . $idrow ?>"/>
        </td>
    </tr>
        <?php
			
			
                                                $rs->MoveNext();
    }
}

    ?>
                            </tbody>
                            <!--<tfoot>
                <tr>
                    <td class="totalAmount"><?/*= Util::priceFormat($total_frs) */?></td>
                    <td style="border-style:none;"></td>
                    <td style="border-style:none;"></td>
                </tr>
                </tfoot>-->
                        </table>
                    </div>
                    <div class="submitButtonContainer">
                        <input type="submit" value="Enregistrer le paiement" class="blueButton noprint"/>
                    </div>
                    <!--                        Total à payer : --><?php //= Util::priceFormat($total_frs) ?>

                </div>
            </form>
        </div>
<!--tab2-->
        <div class="content" id="lists">
            <form action="<?php echo $ScriptName ?>" method="post" name="bank_match_list" id="bank_match_list">
                <input type="hidden" name="page"
                       value="<?php echo isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1 ?>"/>
                <input type="hidden" name="bank_match_list"/>
                <div class="headTitle">
                    <p class="title" id='titleBillings_2'>
                        <?php if ($listResultCount == 0) { ?>
                        <span style="font-size: 14px;font-weight: bold; color: #008000;">Aucune facture fournisseur à payer !</span>
                        <?php } else { ?>
                        Résultats de la recherche: <?php echo $listResultCount ?> facture(s) fournisseurs à payer - Total TTC à
                        payer :
                        <?php } ?>
                    </p>
                </div>
            
                
                <div class="subContent">
                    <div class="tableHeaderLeft" style="margin-bottom:5px;"></div>
                    <div class="tableContainer clear">
                        <table class="dataTable resultTable tablesorter" id="account_table">
                            <thead>
                            <tr>
                            	 <th style="vertical-align:middle;" class="sortable">Code banque</th>
                                <th style="vertical-align:middle;width:70px;" class="sortable">Montant</th>
                                <th style="vertical-align:middle;" class="sortable">Date opétation</th>
                                <th style="vertical-align:middle;" class="sortable">Date valeur</th>
                                <th style="vertical-align:middle;" class="sortable">Intitulé</th>
                                <th style="vertical-align:middle;width:30px;"></th>
                            </tr>
                            </thead>
                            <tbody>
<?php
	$sum_amount=0;
    if ($listResultCount > 0) {

        while (!$list->EOF()) {
			$amount = $list->fields( "amount_type" ) == 'debit' ? -1 * $list->fields( "amount" ) : $list->fields( "amount" );
			$sum_amount += $amount;
            
			$operation_date = $list->fields("operation_date");
            $value_date = $list->fields("value_date");
            $desc = $list->fields("heading");
            $idaccounts_list = $list->fields("idaccounts_list");
			
			$qr1 = "SELECT code 
					FROM bank_company
					WHERE idbank_company ='". $list->fields("code_bank") ."'
					";
			
			$rs1 =& DBUtil::query( $qr1 );
			
			$code_bank = $rs1->fields("code");
			
            ?>
        <tr class="amountRow">
       		<td class="alColumn"><?php echo ($code_bank !="") ? $code_bank : "-" ?></td>
            <td class="lefterCol"><?php echo $amount ?></td>
            <td class="alColumn"><?php echo $operation_date ?></td>
            <td class="alColumn"><?php echo $value_date ?></td>
            <td class="alColumn"><?php echo $desc ?></td>
            <td><input type="radio" name="idaccounts_list" value="<?php echo $idaccounts_list ?>"/>
            </td>
        </tr>
            <?php
                                        $list->MoveNext();
        }
    }
    ?>
                            </tbody>
                            <!--<tfoot>
                <tr>
                    <td class="totalAmount"><?/*= Util::priceFormat($total_frs) */?></td>
                    <td style="border-style:none;"></td>
                    <td style="border-style:none;"></td>
                </tr>
                </tfoot>-->
				<tr><td  style="background-color:#F1F1F1"  class="grasBack">Total </td><td style="background-color:#F1F1F1" colspan="4" class="grasBack"><?=$sum_amount; ?> </td></tr>	
					   </table>
                    </div>
                    <div class="submitButtonContainer">
                        <input type="submit" value="Enregistrer le paiement" class="blueButton noprint"/>
                    </div>
                    <!--                                    Account Plan List : --><?//= Util::priceFormat($total_frs) ?>
                    <!--                                    Total à payer : --><?//= Util::priceFormat($total_frs) ?>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="bottomRight"></div>
<div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->
<div id="account_listing" style="display:none;">
    <form action="<?php echo $ScriptName ?>" method="post" name="bank_match" id="account_listing_form">
        <input type="hidden" name="page"
               value="<?php echo isset($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1 ?>"/>
        <input type="hidden" name="bank_match_list_selected"/>
        <table class="dataTable resultTable tablesorter" id="account_listing_table">
            <thead>
            <tr>
                <th style="vertical-align:middle;width:70px" class="sortable">Montant</th>
                <th style="vertical-align:middle;width:70px" class="sortable">Date création</th>
                <th style="vertical-align:middle;" class="sortable">Intitulé</th>
                <th style="vertical-align:middle;" class="sortable">Mode de paiement</th>
                <th style="vertical-align:middle;width:30px;"></th>
            </tr>
            </thead>
            <tbody></tbody>
            <tfoot></tfoot>
        </table>
        <div class="submitButtonContainer">
            <input type="submit" value="Enregistrer le paiement" class="blueButton noprint"/>
        </div>
    </form>
</div>
 <script type="text/javascript">
    $(function() {
        try {
            $(".tablesorter").not("#account_listing_table").tablesorter({
                sortList:[
                    [1,0]
                ],
                headers: {
                    8: { sorter: false }
                }
            });
        } catch(e) {
        }
    });
	 $(function() {
        try {
            $(".tablesorter").not("#account_table").tablesorter({
                sortList:[
                    [1,0]
                ],
                headers: {
                    8: { sorter: false }
                }
            });
        } catch(e) {
        }
    });
</script>
<?
}
 
//--------------------------------------------------------------------------------------------------

function paginate()
{

    global $page, $pageCount, $GLOBAL_START_URL;

    if ($pageCount < 2) return;

    ?>
<p style="text-align:center;">
    <b>Pages</b>&nbsp;
    <?php

    /*if( $page > 1 ){

        $prev = $page - 1;

        ?>
        <a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
        <?php

    }*/

    $i = 1;
    while ($i < $page) {

        ?>
        <a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
        <?php

        $i++;

    }

    echo "&nbsp;<b>$page</b>&nbsp;";

    $i = $page + 1;
    while ($i < $pageCount + 1) {

        ?>
        <a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
        <?php

        $i++;

    }

    /*if( $page < $pageCount ){

         $next = $page + 1;

         ?>
         <a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
         <?php

     }*/

    ?>
</p>
    <?php

}

//--------------------------------------------------------------------------------------------------
function getBLTotalAmountHT($idbl_delivery)
{

    $bdd = DBUtil::getConnection();

    $Query = "SELECT osr.discount_price, osr.quantity FROM order_supplier os";
}

?>