<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures clients
 */

//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "../catalog/admin_func.inc.php" );
include_once( "../script/global.fct.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------------
//génération du PDF

if( isset( $_GET[ "output" ] ) && isset( $_GET[ "idinvoices" ] ) ){
	
	$invoices = explode( "-", $_GET[ "idinvoices" ] );
	
	if( count( $invoices ) ){
		
		outputInvoices( $invoices );
		exit();
		
	}
	else trigger_error( "URL malformée", E_USER_ERROR );
	
}

// -----------------------------------------------------------------------------------------------



// changement brutal du statut de la facture
if(isset($_POST['updateNotPaid']) && count($_POST['updateNotPaid']) > 0){
	
	$uslogin = User::getInstance()->get('login');
	
	if( $uslogin != 'sile')
		die("Vous n'estes pas autorisé à effectuer cette opération");
		
	$trtIdBillingTable = $_POST['updateNotPaid'];
	
	for($i=0 ; $i<count($trtIdBillingTable) ; $i++){
		
		$trtIdBilling = $trtIdBillingTable[$i];
		// ------- Mise à jour de l'id gestion des Index  #1161
		//on prend les reglements pour les virés
		$rsreguls = DBUtil::query("SELECT brb.idregulations_buyer
								   FROM billing_regulations_buyer brb, regulations_buyer rb
								   WHERE brb.idbilling_buyer = '$trtIdBilling'
								   AND rb.from_down_payment = 0
								   AND brb.idregulations_buyer = rb.idregulations_buyer");
	
		while(!$rsreguls->EOF()){
			$trtIdregulation = $rsreguls->fields('idregulations_buyer');
			
			//allé hop on gicle tous les reglements qui ne sont pas acompte
			$rsBillings=DBUtil::query("SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer=$trtIdregulation");
				
			while(!$rsBillings->EOF()){
				$billingToFuck = $rsBillings->fields("idbilling_buyer");
				// ------- Mise à jour de l'id gestion des Index  #1161
				// MAJ de la facture
				$rupdateBilling = DBUtil::query("UPDATE billing_buyer SET `status`='Invoiced' WHERE idbilling_buyer = '$billingToFuck'");			
				
				// suppression du lien vers le reglement billing_regulations_buyer
				$rdelRegulationBuyer = DBUtil::query("DELETE FROM billing_regulations_buyer WHERE idbilling_buyer = '$billingToFuck' AND idregulations_buyer = $trtIdregulation");			
				
				$rsBillings->MoveNext();
			}
					
			// maintenant on supprime le règlement lui même
			$rdelRegulation = DBUtil::query("DELETE FROM regulations_buyer WHERE idregulations_buyer = $trtIdregulation");
			
			$rsreguls->MoveNext();
		}
		// ------- Mise à jour de l'id gestion des Index  #1161
		// MAJ de la facture
		$rupdateBilling = DBUtil::query("UPDATE billing_buyer SET `status`='Invoiced' WHERE idbilling_buyer = '$trtIdBilling'");			
	}
}

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );
//------------------------------------------------------------------------------------------------

$Title = Dictionnary::translate("tlt_10");

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
	
	/* ----------------------------------------------------------------------------------- */
	
	$(document).ready(function() {
		
		var options = {
			
			beforeSubmit:	preSubmitCallback,  // pre-submit callback 
			success:		postSubmitCallback  // post-submit callback
			
		};
		
		$('#SearchForm').ajaxForm( options );
		
	});
	
	/* ----------------------------------------------------------------------------------- */
	
	function preSubmitCallback( formData, jqForm, options ){
		
		$.blockUI({

			message: "Recherche en cours",
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
			fadeIn: 0, 
			fadeOut: 700
				
		}); 
		
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	
	function postSubmitCallback( responseText, statusText ){ 
		
		$.unblockUI();
		
		$("#selectionAmount").val(0);
		$("#selectionAmountDiv").html("0.00 ¤");
		
		if( statusText != 'success' || responseText == '0' ){
			
			$.growlUI( 'Une erreur est survenue', 'Impossible d\'effectuer la recherche' );
			return;
			
		}
		
		$('#SearchResults').html( responseText );
		
	}
	
	/* --------------------------------------------------------------------------------------- */
	
	function orderBy( str ){
		
		document.getElementById( 'SearchForm' ).elements[ 'orderBy' ].value = str;
		
		$('#SearchForm').ajaxSubmit( { 
			
			beforeSubmit:  preSubmitCallback,  // pre-submit callback 
			success:	   postSubmitCallback  // post-submit callback
			
		} ); 
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	
	function selectSince(){ document.getElementById( "interval_select_since" ).checked = true; }
	
	/* ----------------------------------------------------------------------------------- */
	
	function selectMonth(){ document.getElementById( "interval_select_month" ).checked = true; }
	
	/* ----------------------------------------------------------------------------------- */
	
	function selectBetween( cal ){ document.getElementById( "interval_select_between" ).checked = true; }
	
	/* ----------------------------------------------------------------------------------- */
	
	function resetTotalAmount(){
		
		$("#selectionAmount").val( 0 );
		$("#selectionAmountDiv").html( "0.00 ¤" );
		
		$(".amountRow").each(function(){
			if($(this).is(".activeAmountRow"))
				$(this).removeClass("activeAmountRow");
		});
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	
/* ]]> */
</script>
<style type="text/css">
<!--
	
	.fixme {
		/* Netscape 4, IE 4.x-5.0/Win and other lesser browsers will use this */
	  position: absolute;
	  right: 10px;
	  top: 135px;
	}
	body > div#global > div#globalMainContent > div.fixme {
	  /* used by Opera 5+, Netscape6+/Mozilla, Konqueror, Safari, OmniWeb 4.5+, iCab, ICEbrowser */
	  position: fixed;
	}
	
	tr.activeAmountRow td { background-color:#E0E0E0; }
	
-->
</style>
<!--[if gte IE 5.5]>
<![if lt IE 7]>
<style type="text/css">

	div.fixme {
	  /* IE5.5+/Win - this is more specific than the IE 5.0 version */
	  right: expression( ( 10 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
	  top: expression( ( 135 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
	}
	
</style>
<![endif]>
<![endif]-->
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/invoice_search.php" method="post" id="SearchForm">
		<?php displayForm(); ?>
	</form>
	<div id="tools" class="rightTools">
		<div class="toolBox">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Infos</p>
			</div>
			<div class="content" style="padding:3px; text-align:justify;">Pour calculer la somme de plusieurs factures, cliquez sur chacune d'entre elles. La somme de leurs montants sera affichée dans le cadre de droite.</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
	</div>
	<div id="SearchResults"></div>
	<div id="tools" class="fixme" style="width:130px;">
		<div class="toolBox">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Montant sélection</p>
			</div>
			<input type="hidden" id="selectionAmount" value="0" />
			<div class="content">
				<div id="selectionAmountDiv" style="font-size:14px; font-weight:bold; padding-top:5px; text-align:center;">0.00 ¤</div>
				<input type="button" value="Effacer" onclick="resetTotalAmount();" class="blueButton" style="display:block; margin:auto; margin-top:3px;" />
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle" style="width:106px;"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
	</div>
</div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global	$GLOBAL_START_URL;
	
	$formFields = array(
		
		$fieldName =
		"date_type"					=> "DateHeure",
		"interval_select"			=> 1,
		"minus_date_select"			=> "-365",
		"idbuyer"					=> "",
		"lastname"					=> "",
		"company"					=> "",
		"zipcode"					=> "",
		"city"						=> "",
		"faxnumber"					=> "",
		"idsource"					=> "",
		"iduser"					=> "",
		"idbl_delivery"				=> "",
		"idorder"					=> "",
		"idbilling_buyer"			=> "",
		"status"					=> "",
		"factor"					=> -1,
		"total_amount_ht"			=> "",
		"total_amount"				=> "",
		"idpayment"					=> 0,
		"orderBy"					=> "buyer.company ASC"
		
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
?>
			<div class="mainContent">
				<div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title"><?php echo Dictionnary::translate("tlt_11");?></p>
						<?php if( B2B_STRATEGY ){ ?>
						<div class="rightContainer">
							<label><input type="radio" name="date_type" class="VCenteredWithText" value="DateHeure"<?php	if( $postData[ "date_type" ] == "DateHeure" ) echo " checked=\"checked\""; ?> /> Date de création</label>
							<label><input type="radio" name="date_type" class="VCenteredWithText" value="balance_date"<?php	if( $postData[ "date_type" ] == "balance_date" ) echo " checked=\"checked\""; ?> /> Date d'échéance</label>
						</div>
						<?php } ?>
					</div>
					<div class="subContent">
						<input type="hidden" name="search" value="1" />
						<input type="hidden" name="orderBy" value="<?php echo $postData[ "orderBy" ] ?>" />
						<?php
														
							//if( B2B_STRATEGY )
								 include( "B2B/invoice_search_form.php" );
							//else include( "B2C/invoice_search_form.php" );
							
						?>
						<div class="submitButtonContainer">
							<input type="submit" value="Rechercher" class="blueButton" />
						</div>
					</div>
				</div>
				<div class="bottomRight"></div><div class="bottomLeft"></div>
			</div>
<?php
	
}
//----------------------------------------------------------------------------------------------------

function search(){
	
	global 	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	$lang = User::getInstance()->getLang();
	$iduser = User::getInstance()->getId();
	
	$resultPerPage = 50000;
	$maxSearchResults = 1000;
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	$date_type = isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	
	$select = "
	DISTINCT( billing_buyer.idbilling_buyer ),
	billing_buyer.factor,
	billing_buyer.insurance,
	billing_buyer.charge_vat,
	billing_buyer.idorder,
	billing_buyer.idbuyer,
	billing_buyer.$date_type,
	billing_buyer.status,
	billing_buyer.idpayment,
	billing_buyer.total_amount_ht,
	billing_buyer.total_amount,
	billing_buyer.total_charge,
	billing_buyer.total_charge_ht,
	billing_buyer.billing_amount,
	billing_buyer.deliv_payment,
	billing_buyer.idbilling_adress,
	billing_buyer.relaunch_comment,
	billing_buyer.net_margin_amount,
	billing_buyer.net_margin_rate,
	billing_buyer.gross_margin_amount,
	billing_buyer.gross_margin_rate,
	bl_delivery.idbl_delivery,
	contact.lastname,
	contact.title,
	contact.faxnumber,
	contact.firstname,
	buyer.company,
	buyer.idsource,
	buyer.contact,
	buyer.iderp,
	buyer.zipcode,
	buyer.idstate,
	user.initial";
	
	$tables = "buyer,
		user,
		contact,
		billing_buyer
		LEFT JOIN bl_delivery
			ON billing_buyer.idbilling_buyer = bl_delivery.idbilling_buyer";
	
	$where = "
	billing_buyer.iduser = user.iduser
	AND billing_buyer.idbuyer = buyer.idbuyer
	AND billing_buyer.idbuyer = contact.idbuyer
	AND billing_buyer.idcontact = contact.idcontact";
	
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	if( !$hasAdminPrivileges )
		$where .= " AND ( buyer.iduser = '$iduser' OR `billing_buyer`.iduser = '$iduser' )";
	
	//post data
	
	//integers
	
	$integers = array( 
		
		"buyer.idbuyer", 
		"buyer.idsource", 
		"buyer.idgrouping",
		"billing_buyer.idorder",
		"billing_buyer.idbilling_buyer",
		"billing_buyer.idpayment",
		"billing_buyer.iduser",
		"bl_delivery.idbl_delivery"
		
	);

	foreach( $integers as $integer ){
		
		list( $table, $column ) = explode( ".", $integer );
		
		if( isset( $_POST[ $column ] ) &&  $_POST[ $column ]  )
		{// ------- Mise à jour de l'id gestion des Index  #1161
		    if($_POST[ $column ]!='Tous')
					$where .= " AND `$table`.`$column` = '" .  $_POST[ $column ] . "'";
			}
		
	}
	
	//strings
	
	$strings = array(
		
		"contact.lastname",
		"buyer.company",
		"buyer.city",
		"buyer.zipcode"
		
	);
	
	foreach( $strings as $string ){
		
		list( $table, $column ) = explode( ".", $string );
		
		if( isset( $_POST[ $column ] ) && strlen( $_POST[ $column ] ) )
			$where .= " AND `$table`.`$column` LIKE '%" . Util::html_escape( $_POST[ $column ] ) . "%'";
		
	}
	
	//numbers ( phone, fax, ... )
	
	$numbers = array( "contact.faxnumber" );
	
	foreach( $numbers as $number ){
		
		list( $table, $column ) = explode( ".", $number );
		
		if( isset( $_POST[ $column ] ) && !empty( $_POST[ $column ] ) ){
			
			$num = ereg_replace( "[^0-9]*", "", $_POST[ $column ] );
			$regexp = "";
			$i = 0;
			while( $i < strlen( $num ) ){
				
				if( $i )
					$regexp .= "[^0-9]*";
					
				$regexp .= substr( $num, $i, 1 ) . "{1}";
				
				$i++;
				
			}
			
			$where .= " AND `$table`.`$column` REGEXP '^.*$regexp.*\$'";
		
		}
		
	}
	
	//statut
	
	if ( isset( $_POST[ "status" ] ) && !empty( $_POST[ "status" ] ) ){
		
		$status = FProcessString( $_POST[ "status" ] );
		$where .= " AND billing_buyer.status = $status";
		
	}
	
	//factor
	
	if( isset( $_POST[ "factor" ] ) && $_POST[ "factor" ] == 1 )
		$where .= " AND billing_buyer.factor IS NOT NULL AND billing_buyer.factor <> 0";
	elseif( isset( $_POST[ "factor" ] ) && $_POST[ "factor" ] == 0 )
		$where .= " AND ( billing_buyer.factor IS NULL OR billing_buyer.factor LIKE '0' )";
	
	//montants
	
	if( isset( $_POST[ "total_amount_ht" ] ) && !empty( $_POST[ "total_amount_ht" ] ) )
		$where .= " AND ROUND( `billing_buyer`.total_amount_ht, 2 ) = '" . Util::text2num( $_POST[ "total_amount_ht" ] ) . "'";
	
	if( isset( $_POST[ "total_amount" ] ) && !empty( $_POST[ "total_amount" ] ) )
		$where .= " AND ROUND( `billing_buyer`.total_amount, 2 ) = '" . Util::text2num( $_POST[ "total_amount" ] ) . "'";
	
 	$now = getdate();
	
	$minus_date_select 	= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 	= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;

	switch( $interval_select ){
		    
	     case 1:
			$Min_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $now[ "mon" ], $now[ "mday" ] + $minus_date_select, $now[ "year" ] ) );
			$Now_Time = date( "Y-m-d 23:59:59", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $now[ "mon" ], $now[ "mday" ], $now[ "year" ] ) );
			$where .= " AND billing_buyer.$date_type >= '$Min_Time' AND billing_buyer.$date_type <= '$Now_Time' ";
			
			break;
			
		case 2:
			$yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now[ "year" ]--;
			
		 	$Min_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $yearly_month_select, 1, $now[ "year" ] ) );
		 	$Max_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $yearly_month_select + 1, 1, $now[ "year" ] ) );
			$where .= " AND billing_buyer.$date_type >= '$Min_Time' AND billing_buyer.$date_type < '$Max_Time' ";
	     	
	     	break;
			
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	$where .= " AND billing_buyer.$date_type >= '$Min_Time' AND billing_buyer.$date_type <= '$Max_Time 23:59:59'";
			
	     	break;
			
	}
	
	$groupBy = "billing_buyer.idbilling_buyer";
	
	//tri

	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderBy" ] ) );
	
	//recherche
	
	$query = "
	SELECT $select 
	FROM $tables
	WHERE $where 
	GROUP BY $groupBy
	ORDER BY $orderBy";
	
	$rs =& DBUtil::query( $query );
		
	$resultCount = $rs->RecordCount();
	
	if( !$resultCount ){
		
		?>
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="subContent" style="color:#FF0000; font-size:12px; font-weight:bold; text-align:center;">
					<?php echo Dictionnary::translate("tlt_12");?>
				</div>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
		<?php
		
		return;
		
	}
	
	if( $resultCount > $maxSearchResults ){
		
		?>
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="subContent" style="color:#FF0000; font-size:12px; font-weight:bold; text-align:center;">
					Plus de <?php echo $maxSearchResults ?> factures ont été trouvés<br /> Merci de bien vouloir affiner votre recherche
				</div>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
		<?php	
		
		return;
		
	}
	
	//export factor
	$use_factor_export = DBUtil::getParameterAdmin( "use_factor_export" );
	
	?>
        <div class="mainContent fullWidthContent">
	    	<div class="topRight"></div><div class="topLeft"></div>
	        <div class="content">
	        	<div class="headTitle">
	                <p class="title">Résultats de la recherche : <?php echo $resultCount == 1 ? "1 facture" : "$resultCount factures" ?></p>
					<div class="rightContainer">
					<?php
						
						global $GLOBAL_DB_PASS;
						
						include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
						
						$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
						
						?>
							<!--<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_tva.php?query=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif"/></a>
							<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_tva.php?query=<?php echo $encryptedQuery ?>">Export Factures</a>
						<?php if( $use_factor_export && B2B_STRATEGY ){ ?>
							<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_invoice.php?query=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif"/></a>
							<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_invoice.php?query=<?php echo $encryptedQuery ?>">Export Factor</a>-->
							<?php 
							
						}
	
                		if( $rs->RecordCount() ){
                			
                			$invoices = array();
	                		while( !$rs->EOF() ){
	                			
	                			array_push( $invoices, $rs->fields( "idbilling_buyer" ) );
	                			$rs->MoveNext();
	                			
	                		}
	                		
	                		$rs->MoveFirst();
		                		
                			?>
		                	<a href="invoice_search.php?output&amp;idinvoices=<?php echo implode( "-", $invoices ); ?>" onclick="window.open(this.href); return false;" title="Imprimer les factures trouvées">
		                		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/print.gif" alt="<?php echo Dictionnary::translate( "print" ) ?>" style="border-style:none; vertical-align:middle; margin-left:5px;" />
		                	</a>
		                	<?php
		                	
                		}
	                		
	               	 	?>
					</div>
	            </div>
	            <div class="subContent">
				<?php
					
					
					//if( B2B_STRATEGY )
						 include( "B2B/invoice_search_results.php" );
					//else include( "B2C/invoice_search_results.php" );
					
				?>
	           	</div>
	   		</div>
	        <div class="bottomRight"></div><div class="bottomLeft"></div>
	    </div>
<?php	
}

//--------------------------------------------------------------------------------------------------

function outputInvoices( $generatedInvoices ){

	global $GLOBAL_START_PATH;
		
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFMultiplePDF.php" );
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
	
	$outputFileName = "factures.pdf";
	
	$pdf_list = array();
	foreach( $generatedInvoices as $idinvoice ){
		
		$pdf_list[] = new ODFInvoice( $idinvoice );				
		
	}
	
	$pdfM = new ODFMultiplePDF( $pdf_list , $outputFileName , "I" , false );
	
}

//--------------------------------------------------------------------------------------------------
 
?>
