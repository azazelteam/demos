<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des services
 */

include_once( dirname( __FILE__ ) . "/../../config/init.php" );
include_once( dirname( __FILE__ ) . "/../../objects/Provider.php" );

/* -------------------------------------------------------------------------------------------------- */
/* création d'un prestataire - ajax */

if( isset( $_REQUEST[ "create" ] ) ){ 
	
	header( "Location: " . URLFactory::getHostURL() . "/accounting/provider/provider.php?edit&idprovider=" . Provider::create()->getId() );
	exit();
	
}

/* -------------------------------------------------------------------------------------------------- */
/* Modification d'un prestataire  - ajax */

if( isset( $_REQUEST[ "update" ] ) && isset( $_REQUEST[ "idprovider" ] ) ) { 

	$provider = new Provider( intval( $_REQUEST["idprovider"] ) );
	
	$ret = updateProvider( $provider );
	$ret &= updateProviderContacts( $provider->getContacts() );

	exit( $ret ? "1" : "0" );
	
}

/* -------------------------------------------------------------------------------------------------- */
/* lecture des données d'un prestataire - ajax */

if( isset( $_REQUEST[ "edit" ] ) && isset( $_REQUEST[ "idprovider" ] ) ){ 
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	
	include( dirname( __FILE__ ) . "/../../templates/accounting/provider/provider_edit.php" );
	
	exit();
	
}
else if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "idprovider" ] ) ){ /* suppression d'un prestataire */
	
	Provider::delete( intval( $_REQUEST[ "idprovider" ] ) );
	
}
else if( isset( $_REQUEST[ "list" ] ) ){ /* liste des prestataires - ajax */
	
	listProviders();
	exit();
	
}

/* -------------------------------------------------------------------------------------------------- */

/* liste des prestataires */
	
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="top"></div>
<div id="ProviderFrame">
<?php 

	if( isset( $_GET[ "idprovider" ] ) ){
		
		$provider = new Provider( intval( $_REQUEST[ "idprovider" ] ) );
	
		include( dirname( __FILE__ ) . "/../../templates/accounting/provider/provider_edit.php" );
	
	}
	else listProviders(); 
	
?>
</div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

/* -------------------------------------------------------------------------------------------------- */

function listProviders(){
	
	?>
	<link rel="stylesheet" type="text/css" href="/css/back_office/form_default.css" />
	<script type="text/javascript" src="/js/jquery/ui/uploadify-2.1.0/swfobject.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function createProvider(){

			$.ajax({
			 	
				url: "/accounting/provider/provider.php?create",
				async: true,
				type: "GET",
				data: "",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le prestataire" ); },
			 	success: function( responseText ){
	
					$( "#ProviderEdit" ).html( $( responseText ).html() );
					
				}
	
			});
			
		}

		/* ---------------------------------------------------------------------------------------------------------------- */
		
		function editProvider( idprovider ){

			$.ajax({
			 	
				url: "/accounting/provider/provider.php?edit&idprovider=" + idprovider,
				async: true,
				type: "GET",
				data: "",
				contentType : "application/x-www-form-urlencoded; charset=UTF-8",
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les informations du prestataire" ); },
			 	success: function( responseText ){
	
					$( "#ProviderEdit" ).html( $( responseText ).html() );
					
				}
	
			});

		}

		/* ---------------------------------------------------------------------------------------------------------------- */
				
	 	function filterProviderList(){
	
	 	 	var searchString = $( "#search" ).val().toLowerCase();

	 	 	$( ".listSupplier li" ).css( "display", "none" );
	 	 	
		 	$( ".listSupplier li" ).each( function( i ){

		 		if( $( this ).hasClass( "letter" ) )
		 			return;

	 	 		if( !searchString.length || $( this ).text().toLowerCase().indexOf( searchString ) != -1 ){
		 	 		
	 	 	 			$( this ).css( "display", "block" );
						$( this ).parent().find( ".letter" ).css( "display", "block" );
						
	 	 		}
	 	 		else	$( this ).css( "display", "none" );
	 	 	 		
	 	 	});
		 	
		}

	 	/* ---------------------------------------------------------------------------------------------------------------- */
		
	 	function getAccountNumber( obj ){

			$( "#ServiceAccountNumber" ).val( obj.info.split( ":" )[ 0 ].replace( new RegExp( " ", "g" ), "" ) );
			
		}
		
	 	/* ---------------------------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div class="centerMax provider">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="blocLeftSupplier" id="ProviderList">
			<div class="blocLeftSupplier" id="SupplierList">
				<input type="text" id="search" onkeyup="filterProviderList();" />
				<div class="spacer"></div>
				<ul class="addSupplier">
					<li><a href="#" onclick="createProvider();">Créer un prestataire</a></li>
				</ul>
				<div id="updList">
				<?php echo providerIndex(); ?>
				</div>
				<div class="spacer"></div>
			</div>
		</div>
		<div id="ProviderEdit">
			<div class="blocRightSupplier">
				<div class="contentResult">
					<!-- Titre -->
					<h1 class="titleSearch">
						<span class="textTitle">Liste des prestataires de service</span>
						<div class="spacer"></div>
					</h1>
					
					<div class="blocResult">
						<div class="marginLeft floatleft">
						<p>Sélectionner un fournisseur grâce au menu ci-contre.</p>
						</div>
						<div class="spacer"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<style type="text/css">

		#as_ServiceAccountLabel ul{ width:400px;}
		#as_ServiceAccountLabel .as_header,
		#as_ServiceAccountLabel .as_footer{ width:388px;}
		
	</style>
	<div id="ServiceDialog" style="display:none;">
	<?php
	
		include_once( dirname( __FILE__ ) . "/../../objects/AutoCompletor.php" );
		AutoCompletor::completeFromDBPattern( "ServiceAccountLabel", "accounting_plan", "accounting_plan_1", "{idaccounting_plan} : {accounting_plan_1}", 15, "getAccountNumber" );
			
	?>
	<div class="blocMultiple" style="width:100%;">
		<label style="width:50%;"><span><strong>N° de compte :</strong></span></label>
		<input class="textidInput" type="text" id="ServiceAccountNumber" value="" />
		<div class="spacer"></div>
		<label style="width:50%;"><span><strong>Nom du compte :</strong></span></label>
		<input class="textidInput" type="text" id="ServiceAccountLabel" value=""></span>
		<div class="spacer"></div>
		<label style="width:50%;"><span><strong>Intitulé du service:</strong></span></label>
		<input class="textidInput" type="text" id="ServiceLabel" value="" />
		<!--<div class="spacer"></div>
		<label style="width:50%;"><span><strong>Montant HT :</strong></span></label>
		<input class="textidInput" type="text" id="ServicePrice" value="" /> ¤
		<label style="width:50%;"><span><strong>TVA :</strong></span></label> %-->
		<?php 
		
			include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
			XHTMLFactory::createSelectElement( "tva", "tva", "tva", "tva", 1, 0, "-", "id=\"ServiceVATRate\" style=\"width:80px !important;\"" );
		
		?>
		<div class="spacer"></div>
		<p style="text-align:right; margin:15px 0px;">
			<input type="button" class="blueButton" value="Annuler" onclick="$('#ServiceDialog').dialog( 'destroy' );" />
			<input type="button" class="blueButton" value="Sauvegarder" onclick="createService();" />
		</p>
	</div>
</div>
<div id="AccountDialog" style="display:none;">
	<div class="blocMultiple" style="width:100%;">
			<label style="width:50%;"><span><strong>N° de compte :</strong></span></label>
			<input class="textidInput" type="text" id="AccountNumber" value="" />
			<div class="spacer"></div>
			<label style="width:50%;"><span><strong>Nom du compte :</strong></span></label>
			<input class="textidInput" type="text" id="AccountLabel" value="" />
		<div class="spacer"></div>
		<p style="text-align:right; margin:15px 0px;">
			<input type="button" class="blueButton" value="Annuler" onclick="$('#AccountDialog').dialog( 'destroy' );" />
			<input type="button" class="blueButton" value="Sauvegarder" onclick="createAccount();" />
		</p>
	</div>
</div>
	<?php
	
}

/* -------------------------------------------------------------------------------------------------- */

function updateProvider( Provider &$provider ){
	
	$_POST =& Util::arrayMapRecursive( "stripslashes", $_POST );
	//$_POST =& Util::arrayMapRecursive( "Util::doNothing", $_POST );
	
	/* champs éditables */
	
	$fields = array(
	
	    "name" 						=> "string",
		"erp_name"					=> "string",
		"naf" 						=> "string",
		"siret" 					=> "string",
		"idpayment" 				=> "integer",
		"idpayment_delay" 			=> "integer",
		"comment" 					=> "string",
		"website" 					=> "string",
	    "address"					=> "string",
	    "address2" 					=> "string",
	    "zipcode" 					=> "string",
	    "city" 						=> "string",
	    "idstate" 					=> "integer",
		"manager_idtitle"			=> "integer",
		"manager_firstname"			=> "string",
		"manager_lastname"			=> "string",
		"manager_birthday"			=> "date",
		"manager_place_of_birth"	=> "string",
		"manager_phonenumber"		=> "string",
		"manager_gsm"				=> "string",
		"manager_email"				=> "string",
		"share_capital"				=> "integer",
		"legal_form"				=> "string",
		"creation_date"				=> "date",
		"institution_count"			=> "integer",
		"bank_name"					=> "string",
		"accountant_idtitle" 		=> "integer",
		"accountant_lastname" 		=> "string",
		"accountant_firstname" 		=> "string",
		"accountant_phonenumber"	=> "string",
		"accountant_faxnumber" 		=> "string",
		"bic_bank_name" 			=> "string",
		"bic_bank_code" 			=> "string",
		"bic_branch_code" 			=> "string",
		"bic_account_number" 		=> "string",
		"bic_check_digits" 			=> "string",
		"bic_banking_domiciliation" => "string",
		"IBAN"						=> "string"
		 
	 );
	
	foreach( $fields as $field => $type ){
		
		if( isset( $_POST[ $field ] ) ){
		
			switch(	$type ){

				case "integer" :
					
					$_POST[ $field ] = intval( $_POST[ $field ] );
					break;
					
				case "float" :
					
					$_POST[ $field ] = Util::text2num( $_POST[ $field ] );
					break;
				
				case "date" :
					
					$matches = array();
					if( preg_match( "/([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})/", $_POST[ $field ], $matches ) )
						$_POST[ $field ] = $matches[ 3 ] . "-" . $matches[ 2 ] . "-" . $matches[ 1 ];
					
					break;
					
				case "string" :
				default :
					
					break; /* @todo */
						
			}
			
			$provider->set( $field, $_POST[ $field ] );
			
		}
		
	}

	$provider->save();

	return true;
	
}

/* -------------------------------------------------------------------------------------------------- */

function updateProviderContacts( ArrayList &$contacts ){
	
	$_POST =& Util::arrayMapRecursive( "stripslashes", $_POST );
	//$_POST =& Util::arrayMapRecursive( "Util::doNothing", $_POST );
	
	/* champs éditables */
	
	$fields = array(
	
	    "idtitle" 		=> "int",
		"firstname" 	=> "string",
		"lastname" 		=> "string",
		"phonenumber" 	=> "string",
		"gsm" 			=> "string",
		"faxnumber" 	=> "string",
		"email" 		=> "string",
		"comment" 		=> "string",
	    "iddepartment"	=> "int",
	    "idfunction" 	=> "int"
 
	 );
	
	foreach( $fields as $field => $type ){
		
		if( isset( $_POST[ "contacts" ][ $field ] ) ){
		
			switch(	$type ){

				case "integer" :
					
					$_POST[ "contacts" ][ $field ] = intval( $_POST[ "contacts" ][ $field ] );
					break;
					
				case "float" :
					
					$_POST[ "contacts" ][ $field ] = Util::text2num( $_POST[ "contacts" ][ $field ] );
					break;
				
				case "date" :
				case "string" :
				default :
					
					break; /* @todo */
						
			}
		
			$it = $contacts->iterator();
			$i = 0;
			while( $it->hasNext() )
				$it->next()->set( $field, $_POST[ "contacts" ][ $field ][ $i++ ] );

			
		}
		
	}

	$it = $contacts->iterator();
	
	while( $it->hasNext() )
		$it->next()->save();

	return true;
		
}

/* -------------------------------------------------------------------------------------------------- */

function providerIndex(){
	
	$query = "
	SELECT DISTINCT UPPER( SUBSTR( name, 1, 1 ) ) AS firstchar,
		GROUP_CONCAT( idprovider ORDER BY name ) AS identifiers,
		GROUP_CONCAT( name ORDER BY name ) AS names,
		COUNT( idprovider ) AS `count`
	FROM provider
	GROUP BY SUBSTR( name, 1, 1 )
	ORDER BY name ASC";
	
	$rs =& DBUtil::query( $query );
	$providerCount = 0;

	while( !$rs->EOF() ){
		
		?>
		<ul class="listSupplier" id="Char<?php echo $rs->fields( "firstchar" ); ?>">
			<?php if($rs->fields( "firstchar" )) { ?>
				<li class="letter"><?php echo $rs->fields( "firstchar" ); ?></li>
			<?php } 
		
			$names 			= explode( ",", $rs->fields( "names" ) );
			$identifiers 	= explode( ",", $rs->fields( "identifiers" ) );
			
			$p = 0;
			while( $p < count( $identifiers ) ){
				
				?>
				<li onclick="editProvider(<?php echo $identifiers[ $p ]; ?>);">
					<?php echo strlen( $names[ $p ] ) ? ucfirst( strtolower( htmlentities( $names[ $p ] ) ) ) : "<i>Nouveau prestataire</i>"; ?>
				</li>
				<?php
				
				$p++;

			}

		?>
		</ul>
		<?php

		$providerCount += $rs->fields( "count" );
		$rs->MoveNext();
		
	}
	
}

/* -------------------------------------------------------------------------------------------------- */

?>