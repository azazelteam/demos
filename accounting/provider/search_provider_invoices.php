<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures services
 */

//------------------------------------------------------------------------------------------------

include_once( "../../config/init.php" );
include_once( "../../objects/DHTMLCalendar.php" );
include_once( "../../catalog/admin_func.inc.php" );
include_once( "../../script/global.fct.php" );

$Title = "Recherche de factures de service";

//------------------------------------------------------------------------------------------------

if( isset( $_GET[ 'delete' ] ) && $_GET[ 'delete' ] == 'provider_invoice' ){
	
	include_once( dirname( __FILE__ ) . "/../../objects/ProviderInvoice.php" );
	
	if( !isset( $_POST[ 'providerInvoice' ] ) )
		exit( " Aucune facture " );
		
	if( !isset( $_POST[ 'idprovider' ] ) )
		exit( " Aucun prestataire " );
			
	$provider_invoice = urldecode( $_POST[ 'providerInvoice' ] );
	$idprovider = $_POST[ 'idprovider' ];
	
	ProviderInvoice::delete( $provider_invoice , $idprovider );
	
	exit( '1' );
	
}

//------------------------------------------------------------------------------------------------

if( isset( $_POST[ "search" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit();
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------------

include( "../../templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<?php displayForm(); ?>
</div><!-- globalMainContent -->
<?php

include( "../../templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){

	?>
	<script type="text/javascript" src="/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="/js/form.lib.js"></script>
	<script type="text/javascript" src="/js/ajax.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */

		/* ----------------------------------------------------------------------------------- */

		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			$( "#SearchForm" ).ajaxForm( options );

		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );

		}

		/* ----------------------------------------------------------------------------------- */
		
		function removeProviderInvoice( providerInvoice , idprovider , id ){

			var datas = "idprovider=" + idprovider + "&providerInvoice=" + providerInvoice;

			if( !confirm( 'Supprimer la facture prestataire ?' ) )
				return false;

			var myreturn = false;
			
			$.ajax({
			 	
				url: "<?php echo HTTP_HOST; ?>/accounting/provider/search_provider_invoices.php?delete=provider_invoice",
				type: "POST",
				async: true,
				data: datas,
				error: function(){ alert( "Impossible de supprimer la facture !" ); },
			 	success: function( responseText ){
			 		if( responseText == "1" ){
			 			$.growlUI( 'Suppression effectuée' , '' );
			 			$( '#' + id ).remove();
			 		}else{
						alert( "Impossible de supprimer la facture !" + responseText );
				 	}
				}

			});

			
		}

		/* ----------------------------------------------------------------------------------- */		
		
	/* ]]> */
	</script>
    <div class="mainContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="topRight"></div>
		<div class="topLeft"></div>
		<div class="content">
            <form action="/accounting/provider/search_provider_invoices.php" method="post" id="SearchForm">
  			<input type="hidden" name="search" value="1" />
            <div class="headTitle">
                    <p class="title">Rechercher une facture de service</p>
                    <div class="rightContainer">
                    <input type="radio" name="date_type" value="invoice_date" checked="checked" class="VCenteredWithText" /> Date de facture
                    <input type="radio" name="date_type" value="creation_date" class="VCenteredWithText" /> Date d'enregistrement
                        <input type="radio" name="date_type" value="deliv_payment" class="VCenteredWithText" /> Date d'échéance
                    </div>
                </div>
                <div class="subContent">
                	<!-- tableau choix de date -->
					<div class="tableContainer">
                    	<table class="dataTable dateChoiceTable">
							<tr>	
                            	<th class="filledCell">
                            		<input type="radio" name="interval_select" id="interval_select_since" value="1" checked="checked" class="VCenteredWithText"/> Depuis
                            	</th>
                                <td>
									<select name="minus_date_select" onchange="$( '#interval_select_since' ).attr('checked','checked');" class="fullWidth">
						            	<option value="0">Aujourd'hui</option>
						              	<option value="-1"><?php echo Dictionnary::translate("order_yesterday")  ?></option>
					              		<option value="-2"><?php echo Dictionnary::translate("order_2days")  ?></option>
						              	<option value="-7"><?php echo Dictionnary::translate("order_1week") ?></option>
						              	<option value="-14"><?php echo Dictionnary::translate("order_2weeks") ?></option>
						              	<option value="-30"><?php echo Dictionnary::translate("order_1month") ?></option>
						              	<option value="-60"><?php echo Dictionnary::translate("order_2months") ?></option>
						              	<option value="-90"><?php echo Dictionnary::translate("order_3months") ?></option>
						              	<option value="-180"><?php echo Dictionnary::translate("order_6months") ?></option>
						              	<option value="-365" selected="selected"><?php echo Dictionnary::translate("order_1year") ?></option>
						              	<option value="-730">Moins de 2 ans</option>
						              	<option value="-1095">Moins de 3 ans</option>
						              	<option value="all">Toutes</option>
				            		</select>
								</td>
								<th class="filledCell">
									<input type="radio" name="interval_select" id="interval_select_month" value="2" class="VCenteredWithText" /> Pour le mois de
								</th>
								<td><?php FStatisticsGenerateDateHTMLSelect( "$( '#interval_select_month' ).attr('checked','checked');" ); ?></td>
							</tr>
							<tr>	
								<th class="filledCell">
								    <input type="radio" name="interval_select" id="interval_select_between" value="3" class="VCenteredWithText" /> Entre le
								</th>
								<td>
					 				<input type="text" name="bt_start" id="bt_start" value="<?php echo date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ) ?>" class="calendarInput" />
					 				<?php DHTMLCalendar::calendar( "bt_start", true, "$( '#interval_select_between' ).attr('checked','checked');" ); ?>
								</td>
								<th class="filledCell">et le</th>
								<td>
					 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo date( "d-m-Y" ) ?>" class="calendarInput" />
					 				<?php DHTMLCalendar::calendar( "bt_stop", true, "$( '#interval_select_between' ).attr('checked','checked');" ); ?>
								</td>
							</tr>
                            <tr>
                                <td colspan="4" class="tableSeparator"></td>
                           	</tr>
                            <tr>
                            	<th class="filledCell">Facture n°</th>
                                <td><input type="text" size="10" name="idprovider_invoice"  value="" class="textInput" /></td>
                                <th class="filledCell">Prestataire</th>
                                <td>
                                <?php
                                    
                                    include_once( "../../objects/XHTMLFactory.php" );
                                    XHTMLFactory::createSelectElement( "provider", "idprovider", "name", "name", false, 0, "Tous", "name=\"idprovider\"" );
                                    
                                ?>
                               	</td>
                            </tr>
                            <tr>
								<th class="filledCell">Montant HT</th>
                                <td><input type="text" size="10" name="totalET"  value="" class="textInput" /></td>
                                <th class="filledCell">Montant TTC</th>
                                <td><input type="text" size="10" name="totalATI"  value="" class="textInput" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton" style="text-align: center;" value="Rechercher" />
                    </div>
                </div>
                </form>
            </div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
	<div class="clear"></div>
	<div id="SearchResults"></div>
	<?php 

}

//------------------------------------------------------------------------------------------------

function search(){

	$now = getdate();

	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "invoice_date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
			
	switch ($interval_select){
			    
		case 1:
			//min
			if( $minus_date_select == "all" )
					$Min_Time = "0000-00-00";
			else 	$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$where  = "`provider_invoice`.$date_type >='$Min_Time' AND `provider_invoice`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$where  = "`provider_invoice`.$date_type >='$Min_Time' AND `provider_invoice`.$date_type <'$Max_Time' ";
	     	
			break;
			     
		case 3:
		default :
				
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$where  = "`provider_invoice`.$date_type >='$Min_Time' AND `provider_invoice`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !User::getInstance()->get( "admin" ) )
		$where .= " AND `provider_invoice`.iduser = '" . User::getInstance()->getId() . "'";
	else if( isset( $_POST[ "iduser" ] ) && intval( $_POST[ "iduser" ] ) )
		$where .= " AND provider_invoice.iduser = '" . intval( $_POST[ "iduser" ] ) . "'";
							
	if( isset( $_POST[ "idprovider" ] ) && intval( $_POST[ "idprovider" ] ) )
		$where .= " AND provider_invoice.idprovider = '" . intval( $_POST[ "idprovider" ] ) . "'";

	if ( isset( $_POST[ "idprovider_invoice" ] ) && strlen( $_POST[ "idprovider_invoice" ] ) )
		$where .= " AND provider_invoice.idprovider_invoice LIKE '%" . $_POST[ "idprovider_invoice" ] . "%'";

	$query = "
	SELECT provider_invoice.*, `user`.initial, provider.name, 
	COALESCE( SUM( provider_invoice_item.price * provider_invoice_item.quantity ), 0.0 ) AS totalET
	FROM `user`, provider , provider_invoice
	LEFT JOIN provider_invoice_item ON( 
		provider_invoice_item.idprovider_invoice = provider_invoice.idprovider_invoice
		AND provider_invoice_item.idprovider = provider_invoice.idprovider
	)
	WHERE $where
	AND `user`.iduser = provider_invoice.iduser
	AND provider.idprovider = provider_invoice.idprovider
	
	GROUP BY CONCAT( provider_invoice.idprovider_invoice, '_', provider_invoice.idprovider )";
	
	if( isset( $_POST[ "totalET" ] ) && strlen( $_POST[ "totalET" ] ) 
		|| isset( $_POST[ "totalATI" ] ) && strlen( $_POST[ "totalATI" ] ) )
		$query .= " HAVING 1";
		
	if( isset( $_POST[ "totalET" ] ) && strlen( $_POST[ "totalET" ] ) )
		$query .= " AND SUM( provider_invoice_item.price * provider_invoice_item.quantity ) = '" . Util::text2num( $_POST[ "totalET" ] ) . "'";

	if( isset( $_POST[ "totalATI" ] ) && strlen( $_POST[ "totalATI" ] ) )
		$query .= " AND SUM( provider_invoice_item.price * provider_invoice_item.quantity ) + provider_invoice.vat_amount = '" . Util::text2num( $_POST[ "totalATI" ] ) . "'";
		
	$query .= "
	ORDER BY $date_type DESC";

	$rs = DBUtil::query( $query );

	if( !$rs->RecordCount() ){
		
		?>
		<div class="mainContent fullWidthContent">
        	<div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<div class="headTitle">
                    <p class="title">
                    	Aucune facture n'a été trouvé !
                    </p>
					<div class="rightContainer">
					
					</div>
                </div>
       		</div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div> <!-- mainContent fullWidthContent -->
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ 
			
			$( "#ResultTable" ).tablesorter({
		      	
		      	headers: { 
		
					0: {  sorter:'text' },
					1: (  sorter:'text' ),
					2: {  sorter:'text' },
					3: {  sorter:'text' },
					4: {  sorter:'date' },
					5: {  sorter:'date' },
					6: {  sorter:'date' },
					7: {  sorter:'currency' },
					8: {  sorter:'currency' },
					9: {  sorter:'currency' }
		      		
				} 
					        
			});

		});

	/* ]]> */
	</script>
	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    padding-left: 20px; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
		    padding-bottom:15px;
		
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 
		
	</style>
	<div class="mainContent fullWidthContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="topRight"></div>
		<div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
        		<p class="title"><?php echo $rs->RecordCount(); ?> facture(s) trouvée(s)</p>
				<div class="rightContainer"></div>
			</div>
			<div class="subContent">
				<div class="tableHeaderLeft">
					<a href="#" class="goUpOrDown">
						Aller en bas de page
						<img src="/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a>
				</div>
				<div class="tableHeaderRight"></div>
				<!-- tableau résultats recherche -->
				<div class="resultTableContainer clear">
					<table class="dataTable resultTable sortable" id="ResultTable">
						<thead>
					       	<tr>
					       		<th>Com.</th>
					       		<th>Doc.</th>
					       		<th>Facture n°</th>
					       		<th>Prestataire</th>
					       		<th>Date de facture</th>
					       		<th>Date enregistrement</th>
					       		<th>Date d'échéance</th>
					       		<th>Total HT</th>
						      	<th>Total TTC</th>
						      	<th>Solde à payer</th>
						      <!--	<th></th> -->
					       	</tr>
				       	</thead>
				       	<tbody>
						<?php

							$totalETSum 	= 0.0;
							$totalATISum		= 0.0;
							$balanceOutstandingSum 	= 0.0;
							$i=0;
							while( !$rs->EOF() ){		
															
								?>
						        <tr id="invoice_<?php echo $i; ?>" class="blackText">
						        	<td class="lefterCol"><?php echo $rs->fields( "initial" ); ?></td>
						        	<td><?php 
						        		if( $rs->fields( 'provider_invoice_doc' ) && file_exists( DOCUMENT_ROOT . "/data/provider_invoice/" . $rs->fields( 'provider_invoice_doc' ) ) )
											echo "<a href='" . HTTP_HOST . "/data/provider_invoice/" . $rs->fields( 'provider_invoice_doc' ) . "' style='font-weight:bold;color:#000;' onclick='window.open(this.href);return false;'>Lien</a>";
						        		?>
						        	</td>
						        	<td>
						        		<a class="grasBack" href="/accounting/provider/provider_invoice.php?idprovider_invoice=<?php echo urlencode( $rs->fields( "idprovider_invoice" ) ); ?>&idprovider=<?php echo $rs->fields( "idprovider" ); ?>" onclick="window.open(this.href); return false;">
											<?php echo htmlentities( $rs->fields( "idprovider_invoice" ) ); ?>
										</a>
									</td>
						        	<td>
						        		<a class="grasBack" href="/accounting/provider/provider.php?idprovider=<?php echo $rs->fields( "idprovider" ); ?>" onclick="window.open(this.href); return false;">
						        			<?php echo htmlentities( $rs->fields( "name" ) ); ?>
											(<?php echo htmlentities( $rs->fields( "provider_specific" ) ); ?>)
						        		</a>
						        	</td>
									<td><?php echo Util::dateFormatEu( $rs->fields( "invoice_date" ) ); ?></td>
									<td><?php echo Util::dateFormatEu( substr( $rs->fields( "creation_date" ), 0, 10 ) ); ?></td>
									<td><?php echo Util::dateFormatEu( $rs->fields( "deliv_payment" ) ); ?></td>
									<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "totalET" ) ); ?></td>
									<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "totalET" ) + $rs->fields( "vat_amount" ) ); ?></td>
									<td class="righterCol" style="text-align:right; white-space:nowrap;">
									<?php 
									
										$query = "
										SELECT COALESCE( SUM( amount ), 0.0 ) AS paidAmount
										FROM billing_regulations_provider 
										WHERE idprovider_invoice = '" . $rs->fields( "idprovider_invoice" ) . "'  
										AND idprovider = '" . $rs->fields( "idprovider" ) . "' 
										GROUP BY CONCAT( idprovider_invoice, '_', idprovider )";
										
										$rs2 = DBUtil::query( $query );
										
										$paidAmount = $rs2->RecordCount() ? $rs2->fields( "paidAmount" ) : 0.0;
										
										$balanceOutstanding = $rs->fields( "totalET" );
										$balanceOutstanding += $rs->fields( "vat_amount" );
										$balanceOutstanding -= $rs->fields( "rebate_amount" );
										$balanceOutstanding -= $rs->fields( "down_payment_amount" );
										$balanceOutstanding -= $paidAmount;
										
										echo $balanceOutstanding != 0.0 ? Util::priceFormat( $balanceOutstanding ) : "-"; 
										
									?>
									
									
									
									</td>
									<!--<td>
										<?php if( $balanceOutstanding != 0.0 ){ ?>
										<a href="#" onclick="removeProviderInvoice( '<?php echo urlencode( $rs->fields( "idprovider_invoice" ) ); ?>' , <?php echo $rs->fields( "idprovider" ); ?> , 'invoice_<?php echo $i ?>' ) ; return false;" style="float:left;">
											<img src="/images/back_office/content/corbeille.jpg" alt="Supprimer" />
										</a>
										<?php } ?>
									</td>-->
								</tr>
								<?php
								
									$totalETSum 			+= $rs->fields( "totalET" );
									$totalATISum 			+= $rs->fields( "totalET" ) + $rs->fields( "vat_amount" );
									$balanceOutstandingSum 	+= $balanceOutstanding;
									
									$i++;
									$rs->MoveNext();
							
							}
							
						?>
						</tbody>
				       	<tr>
				       		<td colspan="7" class="lefterCol"></td>
					      	<td style="text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $totalETSum ); ?></td>
					      	<td style="text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $totalATISum ); ?></td>
					      	<td class="righterCol" colspan="2" style="text-align:right; font-weight:bold;"><?php echo Util::priceFormat( $balanceOutstandingSum ); ?></td>
				       	</tr>
	            	</table>
                </div>
                <a href="#" class="goUpOrDown">
                	Aller en haut de page
                    <img src="/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>
           	</div>
   		</div>
        <div class="bottomRight"></div>
        <div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
	<?php	

}

//--------------------------------------------------------------------------------

?>