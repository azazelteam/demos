<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Factures de service à payer
 */

//------------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../../config/init.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

//------------------------------------------------------------------------------------------------
/*
if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}*/

//------------------------------------------------------------------------------------------------

$Title = "Factures de service à payer";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<?php displayForm(); ?>
</div><!--globalMainContent-->
<?php
	
include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
		
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "invoice_date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	
	?>
	<script type="text/javascript" src="/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="/js/form.lib.js"></script>
	<script type="text/javascript" language="javascript">
	/* <![ CDATA[ */
	
		/* --------------------------------------------------------------------------------------------- */
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}

		/* --------------------------------------------------------------------------------------------- */
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}

		/* --------------------------------------------------------------------------------------------- */
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}

		/* --------------------------------------------------------------------------------------------- */
		//bug avec ajaxForm + dialog
		/*$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			
			$('#searchForm').ajaxForm( options );
						
		});*/
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				alert( 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
		}	
		
		/* ----------------------------------------------------------------------------------- */
		
		function sumSelection(){

			var sum = 0.0;
			$( ".activeAmountRow" ).each( function( i ){
				sum += parseFloat( $( this ).find( "td:last" ).prev().html().replace( new RegExp( ",", "g" ), "." ).replace( new RegExp( " ", "g" ), "" ) );
			});

			$( "#selectionAmountDiv" ).html( sum.toFixed( 2 ) + " ¤" );
			
		}

		/* ----------------------------------------------------------------------------------- */
		
		function resetSelection(){

			$( ".amountRow" ).toggleClass( "activeAmountRow", false );
			$( "input:checked" ).attr( "checked", false );
			$( "#selectionAmountDiv" ).html( "0.00 ¤" );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<style type="text/css">
	<!--
		
		.fixme {
			/* Netscape 4, IE 4.x-5.0/Win and other lesser browsers will use this */
		  position: absolute;
		  right: 10px;
		  top: 135px;
		}
		body > div#global > div#globalMainContent > div.fixme {
		  /* used by Opera 5+, Netscape6+/Mozilla, Konqueror, Safari, OmniWeb 4.5+, iCab, ICEbrowser */
		  position: fixed;
		}
		
		tr.activeAmountRow td { background-color:#E0E0E0; }
		
	-->
	</style>
	<!--[if gte IE 5.5]>
	<![if lt IE 7]>
	<style type="text/css">
	
		div.fixme {
		  /* IE5.5+/Win - this is more specific than the IE 5.0 version */
		  right: expression( ( 10 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
		  top: expression( ( 135 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
		}
		
	</style>
	<![endif]>
	<![endif]-->
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<link rel="stylesheet" type="text/css" href="/css/back_office/tableSorter.css" />
	<script type="text/javascript" language="javascript" src="/js/ajax.lib.js"></script>
    	<div class="mainContent noprint">
      <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
            <div class="topRight"></div><div class="topLeft"></div>
        	<form action="/accounting/provider/provider_invoices_to_pay.php" method="post" name="searchForm" id="searchForm">
        		<input type="hidden" name="orderby" id="orderby" value="" />
	            <div class="content">
	            	<div class="headTitle">
	                    <p class="title">Recherche de factures de service</p>
	                    <div class="rightContainer">
	                    	<input type="radio" name="date_type" onchange="document.getElementById('sinceText').innerHTML = this.value == 'invoice_date' ? 'Depuis' : 'Dans';" class="VCenteredWithText" value="invoice_date"<?php if( $date_type == 'invoice_date') echo " checked=\"checked\""; ?> /> Date de facture
	                        <input type="radio" name="date_type" onchange="document.getElementById('sinceText').innerHTML = this.value == 'invoice_date' ? 'Depuis' : 'Dans';" class="VCenteredWithText" value="deliv_payment"<?php if( $date_type == 'deliv_payment') echo " checked=\"checked\""; ?> /> Date d'échéance
	                    </div>
	                </div>
	                <div class="subContent">
	  					<input type="hidden" name="search" value="1" />
                		<!-- tableau choix de date -->
                        <div class="tableContainer">
                            <table class="dataTable dateChoiceTable">
                                <tr>
                                    <td><input type="radio" name="interval_select" id="interval_select_since" class="VCenteredWithText" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> /> <span id="sinceText">Dans</span></td>
                                    <td>
                                        <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							            	<option value="0" <?php  if($minus_date_select==0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ;  ?></option>
							              	<option value="-1" <?php  if($minus_date_select==-1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ;  ?></option>
						              		<option value="-2" <?php  if($minus_date_select==-2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ;  ?></option>
							              	<option value="-7" <?php  if($minus_date_select==-7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ; ?></option>
							              	<option value="-14" <?php  if($minus_date_select==-14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ; ?></option>
							              	<option value="-30" <?php  if($minus_date_select==-30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ; ?></option>
							              	<option value="-60" <?php  if($minus_date_select==-60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ; ?></option>
							              	<option value="-90" <?php  if($minus_date_select==-90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ; ?></option>
							              	<option value="-180" <?php  if($minus_date_select==-180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ; ?></option>
							              	<option value="-365" <?php  if($minus_date_select==-365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ; ?></option>
							              	<option value="-730"<?php if( $minus_date_select == -730 ) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
							              	<option value="-1825"<?php if( $minus_date_select == -1825 ) echo " selected=\"selected\""; ?>>Moins de 5 ans</option>
							            </select>
                                    </td>
                                    <td><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> Pour le mois de</td>
                                    <td>
                                        <?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                    </td>
                                </tr>
                                <tr>	
                                    <td>
                                        <input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> Entre le
                                    </td>
                                    <td>
										<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
						 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
						 				<?php DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ); ?>
                                    </td>
                                    <td>et le</td>
                                    <td>
						 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
						 				<?php DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ); ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4" class="tableSeparator"></td>
                                </tr>
                                <tr class="filledRow">
                                    <th>Facture n°</th>
                                    <td><input type="text" name="idprovider_invoice" value="<?php if( isset( $_POST[ "idprovider_invoice" ] ) ) echo htmlentities( stripslashes( $_POST[ "idprovider_invoice" ] ) ); ?>" class="textInput" /></td>
                                    <th>Prestataire</th>
                                    <td>
                                    <?php 
                                    
                                    	include_once( dirname( __FILE__ ) . "/../../objects/XHTMLFactory.php" );
                                    	
                                    	$idprovider = isset( $_POST[ "idprovider" ] ) ? intval( $_POST[ "idprovider" ] ) : 0;
                                    	XHTMLFactory::createSelectElement( "provider", "idprovider", "name", "name", $idprovider, 0, "Tous", "name=\"idprovider\"" );
                                    	
                                    ?>
                                    </td>
                                </tr>
                                 <tr class="filledRow">
                                    <th>Montant TTC</th>
                                    <td><input type="text" name="total_amount" value="<?php if( isset( $_POST[ "total_amount" ] ) ) echo htmlentities( stripslashes( $_POST[ "total_amount" ] ) ); ?>" class="textInput" /></td>
									<th></th>
									<td></td>
                                </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton noprint" value="Rechercher" />
                        </div>
	                </div>
	            </div>
            </form>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools noprint">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
        <div id="SearchResults">
        <?php if( isset( $_POST[ "search" ] ) ) search(); ?>
        </div>
		<div id="tools" class="fixme noprint" style="width:130px;">
			<div class="toolBox">
				<div class="header">
					<div class="topRight"></div><div class="topLeft"></div>
					<p class="title">Montant sélection</p>
				</div>
				<input type="hidden" id="selectionAmount" value="0" />
				<div class="content">
					<div id="selectionAmountDiv" style="font-size:14px; font-weight:bold; padding-top:5px; text-align:center;">0.00 ¤</div>
					<input type="button" value="Effacer" onclick="resetSelection();" class="blueButton noprint" style="display:block; margin:auto; margin-top:3px;" />
				</div>
				<div class="footer">
					<div class="bottomLeft"></div>
					<div class="bottomMiddle" style="width:106px;"></div>
					<div class="bottomRight"></div>
				</div>
			</div>
		</div>
		<?php 
}


/////-----------------------

function search(){
	
	$now = getdate();
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	
	switch ($interval_select){
			    
		case 1:
			
			if( $_POST[ "date_type" ] == "deliv_payment" ){
				
				$Now_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] - $minus_date_select ,$now["year"])) ;
				$Min_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
				
			}else{
				
				$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
				$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
				
			}
				
			//sql_condition
			$where  = "pi." . $_POST[ "date_type" ] . " >='$Min_Time' AND pi." . $_POST[ "date_type" ] . " <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$where  = "pi." . $_POST[ "date_type" ] . " >='$Min_Time' AND pi." . $_POST[ "date_type" ] . " <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
			include_once( dirname( __FILE__ ) . "/../../script/global.fct.php" );
			
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$where  = "pi." . $_POST[ "date_type" ] . " >='$Min_Time' AND pi." . $_POST[ "date_type" ] . " <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !User::getInstance()->get( "admin" ) )
		$where .= " AND pi.iduser = '" . User::getInstance()->getId() . "'";
						
	if( isset( $_POST[ "idprovider" ] ) && intval( $_POST[ "idprovider" ] ) )
		$where .= " AND pi.idprovider = '" . intval( $_POST[ "idprovider" ] ) . "'";

	if( isset( $_POST[ "idprovider_invoice" ] ) && strlen( $_POST[ "idprovider_invoice" ] ) )
		$where .= " AND pi.idprovider_invoice LIKE " . DBUtil::quote( "%" . $_POST[ "idprovider_invoice" ] . "%" );

	$query = "
	SELECT
			pi.idprovider,
			pi.idprovider_invoice,
			pi.invoice_date,
			pi.deliv_payment,
			pi.provider_specific,
			pi.idpayment,
			pi.status,
			payment.name_1 AS payment_name,
			SUM( pii.price * pii.quantity ) AS total_amount_ht,
			SUM( pii.price * pii.quantity ) + pi.vat_amount AS total_amount,
			SUM( pi.total_amount_ht + pi.vat_amount ) AS total_amountSum,
			IFNULL(
				SUM( pii.price * pii.quantity ) + pi.vat_amount - pi.rebate_amount 	- pi.down_payment_amount -
				(
					SELECT SUM( amount ) AS total_payed
					FROM billing_regulations_provider
					WHERE billing_regulations_provider.idprovider_invoice = pii.idprovider_invoice
					AND `idprovider` = pii.idprovider
					GROUP BY billing_regulations_provider.idprovider_invoice
				),
				(
					SELECT SUM( pii.price * pii.quantity ) + pi.vat_amount AS total_amount
				)
			) AS balanceOutstanding,
			u.initial, p.name AS providerName
			FROM provider_invoice pi
			JOIN provider_invoice_item pii ON (pii.idprovider_invoice = pi.idprovider_invoice AND pii.idprovider = pi.idprovider)
			JOIN provider p ON p.idprovider = pi.idprovider
			LEFT JOIN payment ON payment.idpayment = pi.idpayment
			JOIN user u ON pi.iduser = u.iduser
			AND pi.status <> 1
			WHERE $where
			GROUP BY CONCAT( pi.idprovider_invoice, '|', pi.idprovider )
			HAVING balanceOutstanding > 0.0 OR balanceOutstanding IS NULL
			
	";

	if( isset( $_POST[ "total_amount_ht" ] ) && Util::text2num( $_POST[ "total_amount_ht" ] ) > 0.0 )
		$query .= " AND total_amount_ht = '" . Util::text2num( $_POST[ "total_amount_ht" ] ) . "'";
		
	if( isset( $_POST[ "total_amount" ] ) && Util::text2num( $_POST[ "total_amount" ] ) > 0.0 )
		$query .= " AND total_amount = '" . Util::text2num( $_POST[ "total_amount" ] ) . "'";
		
	$query .= "
	ORDER BY p.name ASC, pi.idprovider_invoice ASC";

	$rs =& DButil::query( $query );

	if( !$rs->RecordCount() ){
		
		?>
		<div class="mainContent fullWidthContent">
	    	<div class="topRight"></div>
	    	<div class="topLeft"></div>
            <div class="content">
            	<div class="headTitle">
                    <p class="title" id='titleBillings'>
                    	<span style="font-size: 14px;font-weight: bold; color: #008000;">Aucune facture à payer !</span>
                    </p>
				</div>
			</div>
			<div class="bottomRight"></div>
			<div class="bottomLeft"></div>
		</div>
		<?php
			
		return;
		
	}
		
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ---------------------------------------------------------------------------------------------- */
		/* tri résultats */
		
		$( document ).ready( function(){
			
			$(".tablesorter").tablesorter({
				
				sortList:[[1,0]],
		        headers: { 
		            6: { sorter: false }
		        } 
	        
		    });

			$( "input[type=checkbox]" ).click( function(){
				
				$( this ).parent().parent().toggleClass( "activeAmountRow", $( this ).attr( "checked" ) );
				sumSelection();
				
			});

			$( "input[type=checkbox]" ).parent().siblings().click( function(){
				
				$( this ).parent().toggleClass( "activeAmountRow" );
				sumSelection();
				
			});

			$( ".amountRow" ).css( "cursor", "pointer" );
			
		});

		/* ---------------------------------------------------------------------------------------------- */
		/* paiement */
		
		function paymentDialog( idprovider ){

			var invoices = new Array();

			if( !$( "input:checked[type=checkbox][id^='" + idprovider + "_']" ).size() ){

				alert( "Vous devez sélectionner au moins une facture à payer" );
				return;

			}
			
			$( "input:checked[type=checkbox][id^='" + idprovider + "_']" ).each( function( i ){ 

				invoices.push( escape( $( this ).val() ) ); 

			});

			var data = "idprovider=" + idprovider + "&invoices=" + invoices.join( "," );
			
			$.ajax({
			 	
				url: "/accounting/provider/register_provider_payment.php",
				async: false,
				type: "GET",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le dialogue" ); },
			 	success: function( responseText ){

					$( "#PaymentDialog" ).html( responseText );
					$( "#PaymentDialog" ).dialog({

						modal: true,	
						title: "Saisir un règlement",
						close: function(event, ui) { 
						
							$("#PaymentDialog").dialog( "destroy" );
							$("#PaymentDialog").html( "" );
							
						},
						width: 1024
						
					});
					
				}

			});
			
		}

		/* ---------------------------------------------------------------------------------------------- */

	/* ]]> */
	</script>
	<div id="PaymentDialog" style="display:none;"></div>
	<div class="mainContent fullWidthContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    	<div class="topRight"></div>
    	<div class="topLeft"></div>
            <div class="content">
            <div class="headTitle">
                    <p class="title" id='titleBillings'>
                    Résultats de la recherche: <?php echo $rs->RecordCount() ?> facture(s) à payer - Total TTC à payer : <?php
                    
                    	$balanceOutstandingSum = 0.0;
                    	while( !$rs->EOF() ){

                    		$balanceOutstandingSum += $rs->fields( "balanceOutstanding" );
                    		$rs->MoveNext();
                    		
                    	}
                    	
                    	$rs->MoveFirst();
                    	
                    	echo Util::priceFormat( $balanceOutstandingSum );
                    	
                    ?>
                    </p>
                </div>
                <div class="subContent">
                <div class="tableHeaderLeft" style="margin-bottom:5px;"></div>
				<?php

					while( !$rs->EOF() ){

						$balanceOutstandingSum = 0.0;
						$total_amount = 0.0;
						
						?>
						<a name="Anchor_<?php echo $rs->fields( "idprovider" ); ?>"></a>
	                	<div class="subTitleContainer">
	                        <p class="subTitle"><?php echo htmlentities( $rs->fields( "providerName" ) ); ?></p>
	                    </div>
	                	<!-- tableau résultats recherche -->
	                    <div class="tableContainer clear">
	                    	<table class="dataTable resultTable tablesorter">
	                        	<thead>
						 			<tr>
						         		<th style="width:60px;vertical-align:middle;">Com.</th>
						         		<th style="width:100px;vertical-align:middle;">Facture</th>
						         		<th style="width:100px;vertical-align:middle;">Date<br />facture</th>
						         		<th style="width:100px;vertical-align:middle;">Date<br />d'échéance</th>
							         	<th style="width:100px;vertical-align:middle;">Mode règlement</th>									
							        	<th style="width:80px;vertical-align:middle;">Total TTC</th>
							        	<th style="width:80px;vertical-align:middle;">Solde TTC</th>
						        		<th class="noprint" style="width:80px;vertical-align:middle;">Enregistrer / Regrouper</th>
					        		</tr>
				        		</thead>
				        		<tbody>
				        		<?php
				        		
									$idprovider = $rs->fields( "idprovider" );
									$i = 0;
									while( !$rs->EOF() && $rs->fields( "idprovider" ) == $idprovider ){
										
										?>
					        			<tr id="InvoiceRow<?php echo $rs->fields( "idprovider" ); ?>_<?php echo htmlentities( $rs->fields( "idprovider_invoice" ) ); ?>" class="amountRow">
								        	<td class="lefterCol"><?php echo $rs->fields( "initial" ); ?></td> 
								        	<td>
								        		<a href="/accounting/provider/provider_invoice.php?idprovider_invoice=<?php echo urlencode( $rs->fields( "idprovider_invoice" ) ); ?>&idprovider=<?php echo $rs->fields( "idprovider" ); ?>" onclick="window.open(this.href); return false;">
									        		<?php echo htmlentities( $rs->fields( "idprovider_invoice" ) ); ?>
									        	</a>
												<b><?php echo htmlentities( $rs->fields( "provider_specific" ) ); ?></b>
								        	</td>
											<td><?php echo Util::dateFormat( $rs->fields( "invoice_date" ), "d/m/Y" ); ?></td>
											<td><?php if( $rs->fields( "deliv_payment" ) != "0000-00-00" ) echo Util::dateFormat( $rs->fields( "deliv_payment" ), "d/m/Y" ); ?></td>
											<td class="lefterCol"><?php echo $rs->fields( "payment_name" ); ?></td> 
											<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ); ?></td>
											<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "balanceOutstanding" ) ) ?></td>
											<td class="righterCol noprint">
												<input type="checkbox" name="" id="<?php echo $rs->fields( "idprovider" ) . "_" . $i; ?>" value="<?php echo htmlentities( $rs->fields( "idprovider_invoice" ) ); ?>" />
											</td>
								        </tr>
								        <?php
			
								        $balanceOutstandingSum += $rs->fields( "balanceOutstanding" );
								        $total_amount += $rs->fields( "total_amount" );
								        $idprovider = $rs->fields( "idprovider" );
								        
								        $rs->MoveNext();
								        
								        $i++;
								        
									}
									
									if( $i )
										$rs->Move( $rs->_currentRow - 1 );
									
								?>
				        		</tbody>
								<tfoot>
							 		<tr>
								 		<td colspan="5" style="border:none;"></td>
								 		<td class="lefterCol righterCol" style="font-weight:bold; text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
								 		<td class="lefterCol righterCol" style="font-weight:bold; text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $balanceOutstandingSum ) ?></td>
								 		<td style="border:none;"></td>
							 		</tr>
						 		</tfoot>
 							</table>
 						</div><!-- tableContainer -->
			            <div class="submitButtonContainer">
			            	<input type="button" value="Enregistrer le paiement" onclick="paymentDialog( <?php echo $rs->fields( "idprovider" ); ?> );" class="blueButton noprint" />
			            </div>
			            <?php
						
						$balanceOutstandingSumTotal += $balanceOutstandingSum;
							
			           	if( !$rs->EOF() )
							$rs->MoveNext();
													
					}

				?>
				<p>Total à payer : <?php echo Util::priceFormat( $balanceOutstandingSumTotal ) ?></p>
			</div><!-- subContent -->
       	</div><!-- content -->
       	<div class="bottomRight"></div>
    	<div class="bottomLeft"></div>
	</div><!-- mainContent -->
	<?php
		
}	

//-------------------------------------------------------------------------------------

?>