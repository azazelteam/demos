<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Eléments de facturation clients
 */


function ModifyInvoice( &$invoice ){
 	
 	//---------------------------------------------------------------------------------------
 	/* escompte */
	/*dépendances : 
	 		- ajout/suppression d'articles
	 		- modification des prix de vente article
	  		- modification de l'adress de livraison ( TVA )
	 => traitement à faire avant les autres
	 */
	if( isset( $_POST[ "rebate_rate" ] ) && isset( $_POST[ "rebate_amount" ] ) ){

		$rebate_rate 	= Util::text2num( $_POST[ "rebate_rate" ] );
		$rebate_amount 	= Util::text2num( $_POST[ "rebate_amount" ] );

		if( $invoice->get( "rebate_type" ) == "rate" && $rebate_rate != $invoice->get( "rebate_value" ) )
				$invoice->set( "rebate_value", $rebate_rate );
		elseif( $invoice->get( "rebate_type" ) == "rate" && $rebate_amount != round( $invoice->getTotalATI() * $invoice->get( "rebate_value" ) / 100.0, 2 ) ){
				
			$invoice->set( "rebate_type", "amount" );
			$invoice->set( "rebate_value", $rebate_amount );
				
		}
		elseif( $invoice->get( "rebate_type" ) == "amount" && $rebate_amount != $invoice->get( "rebate_value" ) )
				$invoice->set( "rebate_value", $rebate_amount );
		elseif( $invoice->get( "rebate_type" ) == "amount" &&  $rebate_rate != round( $invoice->getTotalATI() > 0.0 ? $invoice->get( "rebate_value" ) * 100.0 / $invoice->getTotalATI() : 0.0, 2 ) ){
				
			$invoice->set( "rebate_type", "rate" );
			$invoice->set( "rebate_value", $rebate_rate );
				
		}
	
	}
	
 	//---------------------------------------------------------------------------------------
 	//champs entiers à traiter
 	
 	$integers = array( 

		"idpayment_delay",
		"idpayment"
		
	);
	
	//champs décimaux à traiter
	
 	$floats = array(
 	
 		"balance_amount"
 		
 	);
 	
 	//champs dates à traiter
 	
 	$dates = array(
 	
 		"deliv_payment",
 		"remote_creation_date",
 		"balance_date"
 		
 	);
 	
 	//champs textes à traiter
 	
 	$texts = array( 
	
		"billing_comment", 
		"factor",
		"n_order" 
		
	);
	
	//---------------------------------------------------------------------------------------
 	//integers
 	
 	foreach( $integers as $integer ){
 	
 		if( isset( $_POST[ $integer ] ) )
 			$invoice->set( $integer, intval( $_POST[ $integer ] ) );
 	
 	}
 	
 	//---------------------------------------------------------------------------------------
 	//floats
 	
 	foreach( $floats as $float ){
 	
 		if( isset( $_POST[ $float ] ) )
 			$invoice->set( $float, Util::text2num( $_POST[ $float ] ) );
 	
 	}
 	
 	//---------------------------------------------------------------------------------------
 	//dates
 	
 	foreach( $dates as $date ){
 		
	 	if( isset( $_POST[ $date ] ) ){
			
			
			if( empty( $_POST[ $date ] ) )
				$invoice->set( $date, "0000-00-00" );
			else{
				
				$tmp = explode( "-", $_POST[ $date ] );
				
				if( count( $tmp ) == 3 ){
						
					list( $day, $month, $year ) = $tmp;
					
					if( checkdate( $month, $day, $year ) )
						$invoice->set( $date, "$year-$month-$day" );
						
				}
				
			}
			
		}
	
 	}
 	
 	//---------------------------------------------------------------------------------------
 	//texts
 	
 	foreach( $texts as $text ){
 		
 		if( isset( $_POST[ $text ] ) )
 			$invoice->set( $text, stripslashes( $_POST[ $text ] ) );
 				
 	}
 	
 	//---------------------------------------------------------------------------------------
 	//changement de commercial

	if( isset( $_POST[ "ComChanged" ] ) || isset( $_POST[ "ComChanged_x" ] ) ){
		
		include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
		$invoice->setSalesman( new Salesman( intval( $_POST[ "iduser" ] ) ) );
		
	}

	//---------------------------------------------------------------------------------------
	//statut payé
	
	if( isset( $_POST[ "Billing2Pay" ] ) || isset( $_POST[ "Billing2Pay_y" ] ) ){
		$invoice->set( "status", Invoice::$STATUS_PAID );

		$idbuyer = $_POST[ "idbuyer" ];
		$payment_date = date("Y-m-d");
		$deliv_payment =  $_POST[ "deliv_payment" ];
		$comments = $_POST[ "billing_comment" ];
		
		$amount = $_POST[ "amount" ] ;
		
		$idpayment = $_POST[ "idpayment" ];
		
		$accept = 1;

		$billings = $_POST['IdOrder'];
		
		//On créé une nouvelle facture
		$regulationsbuyer = &RegulationsBuyer::createRegulationsBuyer( $billings , $idbuyer );
		
		//On affecte les valeurs
		$regulationsbuyer->set( "payment_date" , $payment_date );
		$regulationsbuyer->set( "deliv_payment" , euDate2us($deliv_payment) );
		$regulationsbuyer->set( "idpayment" , $idpayment );
		$regulationsbuyer->set( "amount" , $amount );
		$regulationsbuyer->set( "accept" , $accept );
		$regulationsbuyer->set( "comment" , $comments );
		
		//On sauvegarde
		$regulationsbuyer->update();
		
	}
	//---------------------------------------------------------------------------------------
	
	//adresses de facturation
	//@todo : un seul formulaire dans l'édition du devis puis supprimer le test if( isset( $_POST[ "ModifyInvoice" ] ) )
	if( isset( $_POST[ "updateAddresses" ] ) )
		updateAddresses( $invoice );
	
	//---------------------------------------------------------------------------------------
	//avoir
	if( isset( $_POST[ "credit_amount" ] ) ){
		
		$credit_amount_post =  Util::text2num($_POST[ "credit_amount" ]);
		$credit_amount =  $invoice->get("credit_amount");
		$credit_balance = $invoice->getCustomer()->get("credit_balance");
		
		$credit_balance+=$credit_amount;
		
		//Total à payer
		$total_amount = round($invoice->getNetBill(),2);
			
		if($credit_amount_post>$total_amount)
			$credit_amount_post=$total_amount;
			
		if($credit_amount_post>$credit_balance){
			
			$credit_amount = $credit_balance;
		
		}else{
		
			$credit_amount = $credit_amount_post;
				
		}
			
		$invoice->set("credit_amount",$credit_amount);
		
		$credit_balance-=$credit_amount;
		
		//On débite le credit du client
		$idbuyer = $invoice->get("idbuyer");
		
		$Query = "UPDATE buyer SET credit_balance=$credit_balance WHERE idbuyer='$idbuyer'";

		$rs = & DBUtil::query( $Query );
		
		if($rs===false)
			die("Impossible de débiter le solde du client");
		
		/* mise à jour commande client et devis */
		
		if( $invoice->get( "idorder" ) ){
			
			DBUtil::query( "UPDATE `order` SET credit_amount = '$credit_amount' WHERE idorder = '" . $invoice->get( "idorder" ) . "' LIMIT 1" );	
			
			$rs =& DBUtil::query( "SELECT idestimate FROM `order` WHERE idorder = '" . $invoice->get( "idorder" ) . "' LIMIT 1" );
			
			if( $rs->recordCount() )
				DBUtil::query( "UPDATE estimate SET credit_amount = '$credit_amount' WHERE idestimate = '" . $rs->fields( "idestimate" ) . "' LIMIT 1" );
				
		}
		
		//Payé ?
		
		if( $invoice->getBalanceOutstanding() == 0.0 )
			$invoice->set( "status", Invoice::$STATUS_PAID );
		
		
	}
	
	
	//---------------------------------------------------------------------------------------
 	//sauvegarde
 	
	$invoice->save();
		
}
 
//--------------------------------------------------------------------------------------------

function updateAddresses( Invoice &$invoice ){
	
	//adresse de facturation
	
	if( isset( $_POST[ "invoice_address_checkbox" ] ) && !empty( $_POST[ "invoice_address" ][ "idbilling_adress" ] ) ) 
		$invoice->setInvoiceAddress( new InvoiceAddress( intval( $_POST[ "invoice_address" ][ "idbilling_adress" ] ) ) );
	else $invoice->setInvoiceAddress( $invoice->getCustomer()->getAddress() );
	
}

//--------------------------------------------------------------------------------------------

?>