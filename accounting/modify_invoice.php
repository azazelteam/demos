<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Modification des règlements et de la facture clients
 */
 
$DEBUG = "log";

include_once( "../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/Customer.php" );
include_once( "$GLOBAL_START_PATH/objects/GroupingCatalog.php" );

include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

$banner = "no";

$uslogin = User::getInstance()->get('login');

// changement brutal du statut de la facture
if(isset($_POST['killBilling']) && $_POST['killBilling']==1){
	$trtIdBilling 			= $_GET['IdInvoice'];

	//on prend les reglements pour les virés
	$rsreguls = DBUtil::query("SELECT brb.idregulations_buyer
							   FROM billing_regulations_buyer brb, regulations_buyer rb
							   WHERE brb.idbilling_buyer = $trtIdBilling
							   AND rb.from_down_payment = 0
							   AND brb.idregulations_buyer = rb.idregulations_buyer");

	while(!$rsreguls->EOF()){
		$trtIdregulation = $rsreguls->fields('idregulations_buyer');
		
		//allé hop on gicle tous les reglements qui ne sont pas acompte
		$rsBillings=DBUtil::query("SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer=$trtIdregulation");
			
		while(!$rsBillings->EOF()){
			$billingToFuck = $rsBillings->fields("idbilling_buyer");
			
			// MAJ de la facture
			$rupdateBilling = DBUtil::query("UPDATE billing_buyer SET `status`='Invoiced' WHERE idbilling_buyer = $billingToFuck");			
			
			// suppression du lien vers le reglement billing_regulations_buyer
			$rdelRegulationBuyer = DBUtil::query("DELETE FROM billing_regulations_buyer WHERE idbilling_buyer = $billingToFuck AND idregulations_buyer = $trtIdregulation");			
			
			$rsBillings->MoveNext();
		}
				
		// maintenant on supprime le règlement lui même
		$rdelRegulation = DBUtil::query("DELETE FROM regulations_buyer WHERE idregulations_buyer = $trtIdregulation");
		
		$rsreguls->MoveNext();
	}
}

// traitement des règlements
if(isset($_POST['trtRegulations']) && $_POST['trtRegulations'] == 1){

	$trtIdBilling 			= $_GET['IdInvoice'];
	$trtIdregulation 		= $_POST['idRegulation'];
	$trtRegulationDate 		= euDate2us($_POST['regulationDate']);
	$trtRegulationAmount 	= $_POST['amountRegulation'];
	$trtRegulationIdPayment = $_POST['idpayment'];
	$trtFromDownPayment 	= $_POST['fromDownPayment'];
	$trtFactor 				= $_POST['factor'];
	$trtComment 			= $_POST['regulationComment'];

	if(isset($_POST['delRegulation_x'])){
		// dja faut changer le statut de la facture, ben vi si on jette un reglement, elle est automatiquement non soldée donc 'Invoiced'
		// mtn faut guetter si le reglement sélectionné concerne plusieurs factures et les virer aussi
		// put1 ca va chier des bulles

		$rsBillings=DBUtil::query("SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer=$trtIdregulation");
			
		while(!$rsBillings->EOF()){
			$billingToFuck = $rsBillings->fields("idbilling_buyer");
			
			// suppression du lien vers le reglement billing_regulations_buyer
			$rdelRegulationBuyer = DBUtil::query("DELETE FROM billing_regulations_buyer WHERE idbilling_buyer = $billingToFuck AND idregulations_buyer = $trtIdregulation");			

			// MAJ de la facture		
			$invoice = new Invoice( $trtIdBilling  );
			$toupet = $invoice->getBalanceOutstanding();
			if($toupet == 0){
				$rupdateBilling = DBUtil::query("UPDATE billing_buyer SET `status`='Paid' WHERE idbilling_buyer = $billingToFuck");			
			}else{
				$rupdateBilling = DBUtil::query("UPDATE billing_buyer SET `status`='Invoiced' WHERE idbilling_buyer = $billingToFuck");			
			}
			
			$rsBillings->MoveNext();
		}
				
		// maintenant on supprime le règlement lui même
		$rdelRegulation = DBUtil::query("DELETE FROM regulations_buyer WHERE idregulations_buyer = $trtIdregulation");			
		
		//si acompte , le supprimer de billing_buyer
		if($trtFromDownPayment==1){
			$query = "UPDATE billing_buyer SET down_payment_type='amount',down_payment_value = 0 WHERE idbilling_buyer = $trtIdBilling";
			$rs = DBUtil::query( $query );
		}
		
	}
		
	if(isset($_POST['updateRegulation_x'])){
		//on met a jour les  tables billing_regulations_buyer et regulations_buyer
		
		$amountToUse = 0.0;
		if($trtFromDownPayment == 0){
			$amountToUse = $trtRegulationAmount;
		}
		
		$rsupdBRB = DBUtil::query("UPDATE billing_regulations_buyer
								   SET amount = $amountToUse ,
								   username = '".User::getInstance()->get('login')."' ,
								   lastupdate = NOW()
								   WHERE idregulations_buyer = $trtIdregulation
								   AND idbilling_buyer = $trtIdBilling");
								   
		$rsupdRB = DBUtil::query("UPDATE regulations_buyer 
								  SET amount = $trtRegulationAmount,
								  comment = \"$trtComment\",
								  idpayment = $trtRegulationIdPayment,
								  payment_date = '$trtRegulationDate',
								  from_down_payment = $trtFromDownPayment,
								  factor = $trtFactor,
								  lastupdate = NOW(),
								  username = '".User::getInstance()->get('login')."'
								  WHERE idregulations_buyer = $trtIdregulation");	

	
		//si c'est un accompte il faut modif la table billing_buyer aussi
		if($trtFromDownPayment==1){
			$query = "UPDATE billing_buyer SET down_payment_type='amount',down_payment_value = $trtRegulationAmount WHERE idbilling_buyer = $trtIdBilling";
			$rs = DBUtil::query( $query );
		}

		$invoice = new Invoice( $trtIdBilling  );
		$toupet = $invoice->getBalanceOutstanding();
					
		//accept
		$auj = date("d-m-Y");
		
		if( $trtRegulationDate != "" && $trtRegulationDate != "0000-00-00" && $toupet <= 0  ) {
			$paid = 1;
		}else{
			$paid = 0;
		}
		
		if( $paid==1 ){
			$query = "UPDATE billing_buyer SET status='Paid' WHERE idbilling_buyer = $trtIdBilling";
			$rs = DBUtil::query( $query );
		}else{
			$query = "UPDATE billing_buyer SET status='Invoiced' WHERE idbilling_buyer = $trtIdBilling";
			$rs = DBUtil::query( $query );
		}
		
				
					
	}


}

if( !isset( $_REQUEST[ "IdInvoice" ] ) )
	die( "Paramètre absent" );
	
//verification si c'est possible d'afficher quelque chose
if( isset( $_REQUEST['IdInvoice'])){

	//compte pour voir si ya une cde de ce num
	$idordera=$_REQUEST['IdInvoice'] ;
	$reqverif="SELECT COUNT(idbilling_buyer) as comptage FROM `billing_buyer` WHERE idbilling_buyer=$idordera";
	
	$baseverif = DBUtil::getConnection();
	$resverif = $baseverif->Execute( $reqverif );

	if($resverif->fields("comptage")==0){
		$html="<div style='	text-align:center; color:#FF0000; font-size:18px; font-weight:bold;'>".Dictionnary::translate("this_number_dont_exist")."</div>";
		include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
		die( $html );
	}
}

//Adresse de facturation
$b_company_required = DBUtil::getParameterAdmin("b_company_required");
$b_company_required = $b_company_required ? "<span class=\"asterix\">*</span>" : "";

$b_title_required = DBUtil::getParameterAdmin("b_title_required");
$b_title_required = $b_title_required ? "<span class=\"asterix\">*</span>" : "";

$b_lastname_required = DBUtil::getParameterAdmin("b_lastname_required");
$b_lastname_required = $b_lastname_required ? "<span class=\"asterix\">*</span>" : "";

$b_firstname_required = DBUtil::getParameterAdmin("b_firstname_required");
$b_firstname_required = $b_firstname_required ? "<span class=\"asterix\">*</span>" : "";

$b_address_required = DBUtil::getParameterAdmin("b_address_required");
$b_address_required = $b_address_required ? "<span class=\"asterix\">*</span>" : "";

$b_address2_required = DBUtil::getParameterAdmin("b_address2_required");
$b_address2_required = $b_address2_required ? "<span class=\"asterix\">*</span>" : "";

$b_zipcode_required = DBUtil::getParameterAdmin("b_zipcode_required");
$b_zipcode_required = $b_zipcode_required ? "<span class=\"asterix\">*</span>" : "";

$b_city_required = DBUtil::getParameterAdmin("b_city_required");
$b_city_required = $b_city_required ? "<span class=\"asterix\">*</span>" : "";

$b_state_required = DBUtil::getParameterAdmin("b_state_required");
$b_state_required = $b_state_required ? "<span class=\"asterix\">*</span>" : "";

$b_phonenumber_required = DBUtil::getParameterAdmin("b_phonenumber_required");
$b_phonenumber_required = $b_phonenumber_required ? "<span class=\"asterix\">*</span>" : "";

$b_faxnumber_required = DBUtil::getParameterAdmin("b_faxnumber_required");
$b_faxnumber_required = $b_faxnumber_required ? "<span class=\"asterix\">*</span>" : "";

//Délai général commande
$global_deliv = DBUtil::getParameterAdmin("view_delay_delivery");

//---------------------------------------------------------------------------------------------

/**
 * Chargement de la facture
 */

$IdOrder = intval( $_REQUEST[ "IdInvoice" ] );

$Order = new Invoice( $IdOrder );

//------------------------------------------------------------------------------------

//vérification des droits de consultation

$iduser = User::getInstance()->getId();

if( !$Order->allowRead() )
	die( Dictionnary::translate( "gest_com_access_denied" ) );

//------------------------------------------------------------------------------------
//traitement du formulaire

if( isset( $_POST[ "ModifyInvoice" ] ) ){
	
	include_once( "$GLOBAL_START_PATH/accounting/invoice_parseform.php" );
	ModifyInvoice( $Order );
		
}

//------------------------------------------------------------------------------------
$Str_status = $Order->get('status');

include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );

if( $Str_status == "Invoiced" && Payment::checkRegulations( $IdOrder ) )
	$status_plus = "<br />Paiement réceptionné";
elseif( DBUtil::getDBValue( "factor_refusal_date", "billing_buyer", "idbilling_buyer", $IdOrder ) != "0000-00-00" )
	$status_plus = "<br />Définancée par le factor";
elseif( $Str_status == "Invoiced" && DBUtil::getDBValue( "factor_send_date", "billing_buyer", "idbilling_buyer", $IdOrder ) != "0000-00-00" )
	$status_plus = "<br />Financée par le factor";
else
	$status_plus = "";
		
$company = $Order->getCustomer()->get("company");

$Title = 'Facture n°'.$IdOrder.' - '.$company;

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$relaunched = DBUtil::query(

	"SELECT COUNT(*) AS relaunchCount
	FROM `billing_buyer`
	WHERE idbuyer = '" . $Order->getCustomer()->getId() . "'
	AND (
		relaunch_date_1 <> '0000-00-00'
		OR relaunch_date_2 <> '0000-00-00'
		OR relaunch_date_3 <> '0000-00-00'
	)"

)->fields( "relaunchCount" ) > 0;

/**
 * Affichage du formulaire d'edition
 */
 
?>
<script type="text/javascript" language="javascript">
<!--

// PopUp pour les infos des remises au volume
function PopupVolume(ref,buy)
{
	postop = (self.screen.height-150)/2;
	posleft = (self.screen.width-300)/2;

	window.open('volume_price.php?r='+ref+'&b='+buy, '_blank', 'top=' + postop + ',left=' + posleft + ',width=300,height=150,resizable,scrollbars=yes,status=0');
	
}

// PopUp pour les infos du stock
function PopupStock(ref)
{
	postop = (self.screen.height-400)/2;
	posleft = (self.screen.width-400)/2;

	window.open('popup_stock.htm.php?r='+ref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=400,resizable,scrollbars=yes,status=0');
	
}

function isNumeric( text ){

	var validChars = '0123456789';
	var char;
	
	var i = 0;
	while( i < text.length ){
   
   		char = text.charAt( i );
   		
   		if ( validChars.indexOf( char ) == -1 ) 
   			return false;
   			
		i++;
   	
	}
   	
   	return true;

}

// -->
</script>
<div id="globalMainContent">
<br />
<?php 
//---------------------------------------------------------------------------------------------

$mail_buyer = $Order->getCustomer()->getContact()->get( "mail" );

if(!empty( $mail_buyer ) ){
	$vide=0;
}else{
	$vide=1;
}	
?>
<a name="topPage"></a>

<script type="text/javascript">
/* <![CDATA[ */

	// PopUp pour voir l'historique d'un client
	function seeBuyerHisto(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-1010)/2;
		window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/buyer_history.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=1010,height=600,resizable,scrollbars=yes,status=0');
		
	}

	// PopUp pour voir les relances du client
	function seeRelaunch(){
		
		postop = (self.screen.height-600)/2;
		posleft = (self.screen.width-750)/2;
		window.open('<?php echo $GLOBAL_START_URL ?>/accounting/buyer_relaunch.php?b=<?php echo $Order->getCustomer()->get('idbuyer'); ?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=780,height=600,resizable,scrollbars=yes,status=0');
		
	}

/* ]]> */
</script>
<?php anchor( "Order2Billing", true );?>
<div class="mainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    <div class="topRight"></div>
    <div class="topLeft"></div>
    <div class="content">
    	<div class="headTitle">
            <p class="title">
            <?php 
            
            	/* litige */
	    		
		    	$rsLitigation =& DBUtil::query( "SELECT COUNT(*) AS hasLitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '" . $Order->get("idbilling_buyer" ) . "'" );
		    	
		    	if( $rsLitigation->fields( "hasLitigation" ) ){
		    		
		    		?>
		    		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="vertical-align:middle;" />
		    		<span style="color:#EB6A0A;">Facture litigieuse n°<?php echo $Order->getId(); ?></span>
		    		<?php
		    	
		    	}
		    	else{
		    		
		            echo $Str_status == "Ordered" ? "Confirmation de commande client n°" : "Facture n°";
		            echo " " . $IdOrder;
            
		    	}
		    	
            ?>
            </p>
            <div class="rightContainer date"><?php echo humanReadableDatetime( $Order->get( "DateHeure" ) ) ?></div>
      		<div class="clear"></div>
        </div><!-- headTitle -->
        <div class="subHeadTitle">
        	<div class="subHeadTitleElementRight">
                <b><?php echo Dictionnary::translate( "salesman"); ?></b>
            	<?php 
	            	
	            	/*commercial affaire*/
	            	
            		if( User::getInstance()->get("admin") ){
            			
            			include_once( dirname( __FILE__ ) . "/../script/user_list.php" );
						userList( true, $Order->get( "iduser" ) );

	            		//DrawLift_commercial("user",$Order->get( "iduser" ) );

	            		?>
	            		<input type="image" name="ComChanged" id="ComChanged" alt="Modifier" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" style="vertical-align:middle; margin-left:5px;" />
	            		<?php
	            		
            		}
	            	else echo htmlentities( Util::getDBValue( "firstname", "user", "iduser", $Order->get( "iduser" ) ) . " " . Util::getDBValue( "lastname", "user", "iduser", $Order->get( "iduser" ) ) );
	            	
	        ?>
            </div>
            <p class="subHeadTitleElementLeft" style="margin-top:5px;"><b><?php echo Dictionnary::translate( "salesman_buyer"); ?> : </b><?php echo DBUtil::getDBValue( "firstname", "user", "iduser", $Order->getCustomer()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $Order->getCustomer()->get( "iduser" ) ) ?></p>
			<span style="color:red;font-weight:bold;float:left;">Attention le passage d'une facture au statut 'Facturé' ou 'Financé' détruira tous les règlements liés sauf l'accompte"</span>
			<div class="clear" style="float:left; margin-top:5px;">STATUT DE LA FACTURE :</div>
			<div style="float:left; font-weight:bold; margin: 5px 0 20px 5px;">
			<?
			if($Str_status == 'Paid'){
				?>
				<form action="modify_invoice.php?IdInvoice=<?php echo $IdOrder ?>" method="post" name='frmStatus' enctype="multipart/form-data">
					<input type="hidden" name="killBilling" value="1">
					<select name='strStatus'>
						<option value='Paid' selected='selected'><?php echo Dictionnary::translate( $Str_status ) ?></option>
						<option value='Invoiced'><?php echo Dictionnary::translate( 'Invoiced' ) ?></option>
					</select>
					<input type="submit" name='changeStatus' class='blueButton' value="OK" onclick="return confirm('Estes vous sur de cette modification ? Tous les règlements sauf l\'accompte seront supprimés !');">
				</form>
				<?
			}else{
				echo Dictionnary::translate( $Str_status ) ;
			}
			?>
				<?php echo $status_plus ?>
			</div>
    	</div><!-- subHeadTitle -->
       <div class="subContent">
       		<!-- Coordonnées société -->
        	<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate("gest_com_company_info") ; ?></p>
			</div><!-- subTitleContainer -->
	        <div class="tableContainer">
            	<table class="dataTable">
                    <tr class="filledRow">
                        <th><?php echo Dictionnary::translate( $Order->getCustomer()->get( "contact" ) ? "idcontact" : "idbuyer" ); ?></th>
                        <td colspan="2"><?php echo $Order->getCustomer()->get( "idbuyer" );  ?> <a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/contact_buyer.php?type=<?php echo $Order->getCustomer()->get( "contact" ) ? "contact" : "buyer" ?>&amp;key=<?php echo $Order->getCustomer()->get( "idbuyer" );  ?>&amp;IdOrder=<?php echo $IdOrder ?>" onclick="window.open(this.href); return false;" ><?php echo Dictionnary::translate( $Order->getCustomer()->get( "contact" ) ? "gest_com_edit_contact" : "gest_com_edit_buyer" ) ; ?></a>&nbsp;&nbsp;&nbsp;<?php if($relaunched){?><b><a href="#" class="blueLink" onclick=""seeRelaunch(); return false;">!! Attention client relancé !!</a><?php } ?></td>
                        <td colspan="4"><a href="#" class="blueLink" onclick="seeBuyerHisto(); return false;"><?php echo Dictionnary::translate( $Order->getCustomer()->get( "contact" ) ? "gest_com_view_contact_story" : "gest_com_view_buyer_story" ) ; ?></a></td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("company_social"); ?></th>
                        <td colspan="2"><?php echo $Order->getCustomer()->get('company'); ?></td>
                        <th colspan="2"><?php echo str_replace("<br />","",Dictionnary::translate( $Order->getCustomer()->get( "contact" ) ? "contact_source" : "buyer_source" ) ) ?></th>
                        <td colspan="2"><?php echo DBUtil::getDBValue( "source_1", "source", "idsource", $Order->getCustomer()->get( "idsource" ) ); ?></td>
                    </tr>
                     <tr>
                        <th><?php echo Dictionnary::translate("adress"); ?></th>
                        <td colspan="6"><?php echo $Order->getCustomer()->get('adress'); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("adress_2"); ?></th>
              			<td colspan="6"><?php $adr2= $Order->getCustomer()->get('adress_2');if(empty($adr2)){echo "&nbsp;";}else{ echo $adr2;} ?></td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("zipcode"); ?></th>
                        <td><?php echo $Order->getCustomer()->get('zipcode'); ?></td>
                        <th><?php echo Dictionnary::translate("city"); ?></th>
                        <td colspan="2"><?php echo $Order->getCustomer()->get('city'); ?></td>
                        <th><?php echo Dictionnary::translate("state"); ?></th>
                        <td><?php echo DBUtil::getDBValue( "name_1", "state", "idstate", $Order->getCustomer()->get( "idstate" ) ); ?></td>
                    </tr>
                    <tr class="filledRow">
                        <th><?php echo Dictionnary::translate( "lastname" ); ?></th>
                        <td colspan="2"><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->get( "title" ) );  ?> <?php echo $Order->getCustomer()->getContact()->get( "lastname" ); ?></td>
                        <th colspan="2"><?php echo Dictionnary::translate( "firstname" ); ?></th>
                        <td colspan="2"><?php echo $Order->getCustomer()->getContact()->get( "firstname" ); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("phone"); ?></th>
                        <td colspan="2"><?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "phonenumber" )); ?></td>
                        <th colspan="2"><?php echo Dictionnary::translate("fax"); ?></th>
                        <td colspan="2"><?php echo Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "faxnumber" )); ?></td>
                    </tr>
                    <tr>
                        <th><?php echo Dictionnary::translate("email"); ?></th>
                        <td colspan="2"><a class="blueLink" href="mailto:<?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?>"><?php echo $Order->getCustomer()->getContact()->get( "mail" ); ?></a></td>
                        <th colspan="2"><?php echo Dictionnary::translate("gsm"); ?></th>
                        <td colspan="2"><?=Util::phonenumberFormat($Order->getCustomer()->getContact()->get( "gsm" )); ?></td>
                    </tr>
                    <tr>
                        <th>Statut demande assurance</th>
                        <td colspan="2">Encours garanti par Assurance Crédit</td>
                        <?php
                        	switch ( $Order->getCustomer()->get('insurance_status') ){
                        		case 'Accepted':
									$customer = new Customer( $Order->get( "idbuyer" ), $Order->get( "idcontact" ) );
									$outstandingCredits = $customer->getInsuranceOutstandingCreditsET();
									
									?>
									 <th colspan="2">Encours garanti par Assurance Crédit</th>
		                       		 <td colspan="2"><?php echo Util::priceFormat( $outstandingCredits ); ?></td>
									<?php                        			
                        			
                        			break;
                        		case 'Refused':
									?>
									<th colspan="2">Date de refus</th>
									<td colspan="2"><?php echo usDate2eu($Order->getCustomer()->get('insurance_refusal_date')); ?></td>
									<?php
                        			break;

                        		case 'Terminated':
									?>
									<th colspan="2">Date de résiliation</th>
									<td colspan="2"><?php echo usDate2eu($Order->getCustomer()->get('insurance_termination_date')); ?></td>
									<?php
                        			break;

                        		case 'Deleted':
									?>
									<th colspan="2">Date de refus</th>
									<td colspan="2"><?php echo usDate2eu($Order->getCustomer()->get('insurance_deletion_date')); ?></td>
									<?php
                        			break;
                        			                        			
                        		default :
									?>
									<th colspan="2"></th>
		                        	<td colspan="2"></td>
		                        	<?php                        			
                        			break;
                        		
                        	}
						
					?>
                    </tr>
               </table>
			</div><!-- tableContainer -->
		</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight">
	</div><div class="bottomLeft"></div>
</div><!-- mainContent -->
<div id="tools" class="rightTools">
<?php
    
    $rsLitigation =& DBUtil::query( "SELECT lb.idlitigation, l.editable FROM litigations_billing_buyer lb, litigations l WHERE lb.idlitigation = l.idlitigation AND lb.idbilling_buyer = '" . $Order->get("idbilling_buyer" ) . "'" );
		    	
	if( $rsLitigation->RecordCount() ){
		
		?>
		<div class="toolBox">
	    	<div class="header">
	        	<div class="topRight"></div><div class="topLeft"></div>
	            <p class="title">Litiges ouverts</p>
	        </div>
	        <div class="content">
	        	<p>
		            <?php
		            
		            	while( !$rsLitigation->EOF() ){
		
							?>
								<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" onclick="window.open( this.href ); return false;">
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="vertical-align:middle;" />
									Litige N° <?php echo $rsLitigation->fields( "idlitigation" ) ?> ( <?php echo $rsLitigation->fields( "editable" ) ? "en cours" : "résolu"; ?> )
								</a><br />
							<?php
						
							$rsLitigation->MoveNext();
							
		            	}
		            	
		            ?>
				</p>
	        </div>
	        <div class="footer">
	        	<div class="bottomLeft"></div>
	            <div class="bottomMiddle"></div>
	            <div class="bottomRight"></div>
	        </div>
	    </div><!-- toolBox -->
		<?php

	}
	
	/* avoirs client*/
	
	?>
    <div class="toolBox">
    	<div class="header">
        	<div class="topRight"></div><div class="topLeft"></div>
            <p class="title">Avoirs</p>
        </div>
        <div class="content">

        	<?php
        	
        		$rsCredits =& DBUtil::query( "SELECT idcredit FROM `credits` WHERE idbilling_buyer='" . $Order->get("idbilling_buyer") ."'" );
        	
        		if( $rsCredits->RecordCount() ){
        			
        			?>
		        	<div class="subTitleContainer" style="margin-top:0;">
		            	<p class="subTitle">Liste des avoirs client créés</p>
		            </div>

		        	<ul style="margin-left:10px;">
		        	<?php
		        		
		        			while( !$rsCredits->EOF() ){
		        				
		        				?>
		        				<li>
		        					<a class="normalLink" style="display:block;" href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $rsCredits->fields( "idcredit" ) ?>">Avoir Client n°<?php echo $rsCredits->fields( "idcredit" ) ?></a>
		        				</li>
		        				<?php
		        				
		        				$rsCredits->MoveNext();
		        				
		        			}
		        			
		        		?>
		        		</ul>

		        	<?php
		        	
        		}
        		
        	?>
			<p>
				<div class="subTitleContainer" style="padding-top:0;">
		           	<p class="subTitle">Création</p>
		        </div>
				<a class="normalLink" style="display:block;margin-left:5px;" href="<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idbilling_buyer=<?php echo $Order->getId() ?>" taget="_blank">Créer un avoir client</a>
			</p>
        </div><!-- content -->
        <div class="footer">
        	<div class="bottomLeft"></div>
            <div class="bottomMiddle"></div>
            <div class="bottomRight"></div>
        </div>
    </div><!-- toolBox -->
    <div class="toolBox">
    	<div class="header">
        	<div class="topRight"></div>
        	<div class="topLeft"></div>
            <p class="title">Visualiser et imprimer</p>
        </div>
        <div class="content">
		<?php
		
			if( !strlen( $Order->getCustomer()->getContact()->get( "mail" ) ) ){
						
				?>
				<div class="subTitleContainer" style="margin-bottom:5px; margin-top:0px;">
					<p class="subTitleWarning"><?php  echo Dictionnary::translate("gest_com_buyer_without_mail_order") ; ?></p>
				</div>
				<?php
				
			}
			?>
			<p><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $Order->getId() ?>" class="normalLink">Facture PDF n°<?php echo $Order->getId() ?></a></p>
        </div><!-- content -->
        <div class="footer">
        	<div class="bottomLeft"></div>
            <div class="bottomMiddle"></div>
            <div class="bottomRight"></div>
        </div>
    </div><!-- toolBox -->
<?php
	
	$customerGrouping = DBUtil::getDBValue( "idgrouping", "buyer", "idbuyer", $Order->getCustomer()->get( "idbuyer" ) );
	
	if( $customerGrouping ){
		
		$grouping = new GroupingCatalog();
		
		//@todo: Bidouille qui consiste à changer l'idgrouping du groupement à la volée pour charger les infos
		// Le problème vient du fait qu'on ne peut instancier un groupement que depuis le Front-Office
		$grouping->getGroupingInfos( $customerGrouping );
		
		$groupingName = $grouping->getGrouping( "grouping" );
		$groupingLogo = $grouping->getGrouping( "grouping_logo" );
							
?>
	<div class="toolBox">
		<div class="header">
			<div class="topRight"></div><div class="topLeft"></div>
			<p class="title">Groupement</p>
		</div>
		<div class="content">
			<div class="subTitleContainer" style="margin-top:0px;">
				<p class="subTitle"><?php echo $groupingName ?></p>
			</div>
			<div style="margin-top:10px; text-align:center;">
				<img src="<?php echo $GLOBAL_START_URL ?>/www/<?php echo $groupingLogo ?>" />
			</div>
		</div>
		<div class="footer">
			<div class="bottomLeft"></div>
			<div class="bottomMiddle"></div>
			<div class="bottomRight"></div>
		</div>
	</div>
	<?php } ?>
</div><!-- tools -->
<?

//---------------------------------------------------------------------------------------------


$hasLot = false;
$i = 0;
while( !$hasLot && $i < $Order->getItemCount() ){
	
	if( $Order->getItemAt( $i)->get( "lot" ) > 1 )
		$hasLot = true;
		
	$i++;
	
}
	
?>
<br />
<form action="modify_invoice.php?IdInvoice=<?php echo $IdOrder ?>" method="post" name='frm' enctype="multipart/form-data">
	<input type="hidden" name="sendmail" id="sendmail" value="0" />
	<input type="hidden" name="sendmailcancel" id="sendmailcancel" value="0" />
	<input type="hidden" name="sendmailpaiement" id="sendmailpaiement" value="0" />
	<input type="hidden" name="ModifyInvoice" id="ModifyInvoice" value="1" />
	<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>" />
	<input type="hidden" name="terminate" value="0" />
	<input type="hidden" name="DdeProForma" value="0" />
	<input type="hidden" name="idbuyer" value="<?php echo $Order->get( "idbuyer" ) ?>" />
<?php 

//---------------------------------------------------------------------------------------------
	
	$articlesET = 0.0;
	$it = $Order->getItems()->iterator();
	while( $it->hasNext() )
		$articlesET += $it->next()->getTotalET();
	
	$total_discount_rate = 0.0 + $Order->get( "total_discount" );
	$total_discount_amount = 0.0 + $Order->get( "total_discount_amount" );
	
	$ttc = $Order->getTotalATI() - $Order->getChargesATI() - $Order->get( "billing_amount" );
	$ht = $articlesET - $Order->get( "total_discount_amount" ); //HT sans frais de port
	
	$total_charge_ht = $Order->get( "total_charge_ht" );
	$total_estimate_ht = $ht + $total_charge_ht;
	
	$billing_rate = $Order->get( "billing_rate" );
	$billing_amount = $Order->get( "billing_amount" );
	$total_ht_wb = $total_estimate_ht - $billing_amount;
	
	$down_payment_rate = $Order->getDownPaymentRate();
	$down_payment_amount = $Order->getDownPaymentAmount();
	
	$total_ttc_wb = $Order->getTotalATI();
	$total_ttc_wbi = $Order->getNetBill();
	
	$Factor = $Order->get( "factor" );
	
	//Marge nette facture
	
	$Marge_Nette 		= $Order->get( "net_margin_rate" );;
	$Marge_Nette_Amount = $Order->get( "net_margin_amount" );;


//Infos commande
?>
<script type="text/javascript">
/* <![CDATA[ */

	function showSupplierInfos( idsupplier){
					
		if( idsupplier == '0' )
			return;

		$.ajax({
		 	
			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){
		 		
				$.blockUI({
					
					message: msg,
					fadeIn: 700, 
            		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px', 
						cursor: 'help',
						'-webkit-border-radius': '10px', 
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }
					 
				}); 
				
				$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
			}

		});
		
	}
	
/* ]]> */
</script>
<a name="references"></a>
<input type="hidden" id="adminScript" value="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" />
<div class="mainContent fullWidthContent">
	<div class="topRight"></div>
	<div class="topLeft"></div>
	<div class="content">
		<div class="subContent">
			<script type='application/javascript'>
				function showDesc(){
					var madiv = document.getElementById("descDiv") ;
					if(madiv.style.display == 'none'){
						madiv.style.display = 'block';
					}else{
						madiv.style.display = 'none';
					}
					
				}
			</script>
        	<div class="subTitleContainer">
                <p class="subTitle"><a href="javascript:showDesc();">Descriptifs de la facture</a></p>
			</div><!-- subTitleContainer -->
			<div id="descDiv" style="display:none;">
			<?php
			
				anchor( "UpdateRefDiscountRate_", true );
				anchor( "UpdateOrderPriceWD_", true );
				anchor( "UpdateUnitCostRate_", true );
				anchor( "UpdateUnitCostAmount_", true );
				anchor( "UpdateDesignation_", true );
				anchor( "UpdateSupplierCharges_", true );
				anchor( "UpdateInternalSupplierCharges_", true ); 
				anchor( "UpdRow_", true );
			
				/*lignes articles*/ 
				
				$it = $Order->getItems()->iterator();
				
				while( $it->hasNext() ){
				
					$item =& $it->next();
					
					?>
					<div class="leftContainer" style="width:845px; margin-bottom:10px;">
					<?php
						
					displayItem( $Order, $item );
				
					?>
					</div>
					<div class="rightContainer" style="width:110px;">
						<?php displayItemMargins( $item ); ?>
					</div>
					<div class="clear"></div>
					<?php
					
				}
				
			?>
			</div>
		</div><!-- subContent -->
	</div><!-- content -->
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->
<?php

//-------------------------------------------------------------------------------------------------------------------

function displayItem( Invoice $invoice, InvoiceItem &$item ){
	
	global $GLOBAL_START_URL;
	
	?>
	<div class="tableContainer" style="margin-bottom:5px;">
		<table class="dataTable devisTable">
		    <tr>
		    	<th style="width:75px;">Photo</th>
		    	<th>Réf. & Désignation courte</th>
		    	<th>Quantité</th>
		    	<th>Prix unitaire</th>
		    	<th>Remise<br />Client %</th>
		    	<th>Prix unitaire<br />Vente Net</th>
		    	<th>Prix vente<br />Total</th>
		    </tr>
			<tr>
		    	<td rowspan="5" style="vertical-align:top;">
				<?php 
				
					/*options */ 			displayOptionLink( $item ); 
					/*produits similaires*/	displaySimilarLink( $item );
					/*image*/				displayReferenceThumb( $item );
					/*promotion @todo : n'existe pas*/
					/*destockage @todo : n'existe pas*/
					/*prix au volume*/		displayVolPriceLink( $item );
					
				?>
		    	</td>
		    	<td rowspan="2" style="text-align:left;"><?php displayReferenceInfos( $item ); ?></td>
				<td rowspan="3">
				<?php 
				
					/*popup stock*/ 	displayStockPopup( $item );
					/*minimum de cde*/	checkMinOrder( $item );
					/*quantité*/		echo $item->get( "quantity" );
					/*quantité par lot*/ 
					
					if( $item->get( "lot" ) > 1 ){
							
						?>
						<p><?php echo $item->get("lot" ) . " / " . htmlentities( DBUtil::getDBValue( "unit" . "_1", "unit", "idunit", $item->get( "unit" ) ) ); ?></p>
						<?php
					}
					
				?>
				</td>
				<td><?php  /*Prix unitaire*/ echo Util::priceFormat( $item->get( "unit_price" ) ); ?></td>
				<td>
				<?php 
				
					/*remise client %*/ 
				
					if( DBUtil::getParameterAdmin( "view_ref_discount_add" ) == 0 )
						echo Util::rateFormat( $item->get( "ref_discount" ) );
					else{
				
						?>
						<p><b>Base</b></p>
						<p><?php echo Util::rateFormat( $item->get( "ref_discount_base" ) ) ?></p>
						<p><b>Complémentaire</b></p>
						<p><?php echo Util::rateFormat( $item->get( "ref_discount_add" ) ) ?></p>
						<?php 
						
					}
				
				?>
				</td>
				<td><?php /*prix unitaire vente net*/	echo Util::priceFormat( $item->get( "discount_price" ) ); ?></td>
				<td><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "discount_price" ) ) ?></td>
			</tr>
			<tr>
				<th>Prix unitaire<br />Tarif fournisseur</th>
		    	<th>Remise<br />Fournisseur %</th>
		    	<th>Prix unitaire<br />Achat net</th>
		    	<th>Prix achat<br />Total</th>
		    </tr>
			<tr>
				<td rowspan="3" style="width:210px; height:110px;">
					<div style="width:210px; height:110px; border-style:none; text-align:left;"><?php echo stripDesignationTags( $item ) ?></div>
				</td>
				<td><?php echo Util::priceFormat( $item->get( "unit_cost" ) ) ?></td>
				<td><?php echo Util::rateFormat( $item->get( "unit_cost_rate" ) ) ?></td>
				<td><?php echo Util::priceFormat( $item->get( "unit_cost_amount" )) ?></td>
				<td><?php echo Util::priceFormat( $item->get( "quantity" ) * $item->get( "unit_cost_amount" ) ) ?></td>
			</tr>
			<tr>
				<th>Stock<br />interne</th>
				<th>Stock<br />externe</th>
				<th>Délai<br />d'éxpédition</th>
				<th>Poids unitaire</th>
				<th>Poids total</th>
			</tr>
			<tr>
				<td><?php
				
					$external_quantity = DBUtil::query( "SELECT external_quantity FROM `order_row` WHERE idorder = '" . $invoice->get( "idorder" ) . "' AND idrow = '" . $item->get( "idorder_row" ) . "' LIMIT 1" )->fields( "external_quantity" );
					echo intval( $item->get( "quantity" ) - $external_quantity );
					
				?></td>
				<td><?php /*quantité externe @todo : n'existe pas*/ echo $external_quantity ?></td>
				<td>
				<?php 
				
					/*délai de livraison @todo : n'existe pas*/
				
					$lang = User::getInstance()->getLang();
					$deliv_delay = DBUtil::query( "SELECT d.delay$lang AS delay FROM delay d, `order_row` orow WHERE orow.idorder = '" . $invoice->get( "idorder" ) . "' AND orow.idrow = '" . $item->get( "idorder_row" ) . "' AND orow.delivdelay = d.iddelay LIMIT 1" )->fields( "delay" );
					echo htmlentities( $deliv_delay );
					
				?></td>
				<td><?php echo Util::numberFormat( $item->get( "weight" ) ) . " kg"; ?></td>
				<td><?php echo Util::numberFormat( $item->get( "quantity" ) * $item->get( "weight" ) ) . " kg"; ?></td>
			</tr>
		</table>
	</div><!-- tableContainer -->
	<div class="leftContainer">
    </div>
    <div class="rightContainer">
        <input type="button" onclick="window.open('<?php echo "$GLOBAL_START_URL/statistics/statistics.php?cat=product&reference=" . urlencode( $item->get( "reference" ) ) . "&amp;period&amp;StatStartDate=" . urlencode( date( "d/m/Y", mktime( 0, 0, 0, date( "m" ) - 6, date( "d" ), date( "Y" ) ) ) ) . "&amp;StatEndDate=" . urlencode( date( "d/m/Y" ) ) ?>' , '_blank');" class="blueButton" value="Statistiques" style="margin-left:5px; margin-right:5px;"/>
    	<input type="button" value="Ouvrir un litige" class="orangeButton" onclick="document.location = '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idbilling_buyer=<?php echo $invoice->get( "idbilling_buyer" ) ?>&idrow=<?php echo $item->get( "idrow" ) ?>';" />
    </div>
    <div class="clear"></div>
    <?php

}

//-----------------------------------------------------------------------------------------------------
/**
 * options
 */
function displayOptionLink( &$item ){
			
	if( DBUtil::query( "SELECT COUNT( * ) AS `hasOption` FROM associat_product WHERE idproduct='" . DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) ). "'" )->fields( "hasOption" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="optionssel(<?php echo $item->get( "idarticle" ) ?>); return false;" class="orangeText"><b>Options</b></a>
		</p>
		<?php 
		
	}
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * produits similaires
 */
function displaySimilarLink( &$item ){
	
	//produits similaires
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle",  $item->get( "idarticle" ) );
	if( DBUtil::query( "SELECT COUNT( * ) AS `hasSimilar` FROM product WHERE idproduct='$idproduct' AND ( idproduct_similar_1 > 0 OR idproduct_similar_2 > 0 OR idproduct_similar_3 > 0 )" )->fields( "hasSimilar" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="similarsel(<?php echo $item->get( "idarticle" ) ?>); return false;" class="orangeText"><b>Similaires</b></a>
		</p>
		<?php 
		
	}
    
}

//-----------------------------------------------------------------------------------------------------
/**
 * image référence + lien catalogue
 */
function displayReferenceThumb( &$item ){
	
	global $GLOBAL_START_URL;

	echo "<p style=\"text-align:center;\">";
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	
	if( $idproduct ){ 
		
		?>
		<a href="<?php echo URLFactory::getProductURL( $idproduct ) ?>" onclick="window.open(this.href); return false;">
		<?php 
		
	}
	
	?>
	<img src="<?php echo URLFactory::getReferenceImageURI( $item->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE ) ?>" alt="icone" style="border-style:none;" />
	<?php 
	
	if( $idproduct ){ 
		
		?>
		</a>
		<?php 
				
	}
	
	echo "</p>";
	
}

//-----------------------------------------------------------------------------------------------------
/**
 * prix au volumne
 */
function displayVolPriceLink( &$item ){
	
	global $GLOBAL_START_URL;
	
	$idbuyer_family = DBUtil::getDBValue( "idbuyer_family", "buyer", "idbuyer", $invoice->get( "idbuyer" ) );
	
	$query = "
	SELECT COUNT( rate ) AS `count`
	FROM multiprice_family 
	WHERE idbuyer_family = '$idbuyer_family' 
	AND reference = '" . $item->get( "reference" ) . "' 
	AND begin_date <= NOW() 
	AND end_date >= NOW()";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->fields( "count" ) > 0 ){
		
		?>
		<p style="text-align:center;">
			<a href="#" onclick="PopupVolume( '<?php echo $item->get( "reference" ) ?>','<?php echo $idbuyer_family ?>');return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/www/img/volume.gif" alt="Prix au volume" style="border-style:none;" />
			</a>
		</p>
		<?php
		
	}
		
}

//-----------------------------------------------------------------------------------------------------

function displayReferenceInfos( &$item ){
		
	global $GLOBAL_START_URL;
	
	/*référence avant substitution @todo : n'existe pas*/
	
	/*référence catalogue*/
	
	?>
	<p>Référence catalogue : <?php echo $item->get( "reference" ) ?></p>
	 <?php
	 
	 /*référence et infos fournisseur*/
	 
	 if( $item->get( "idsupplier" ) ){
		
	 	$ref_supplier = DBUtil::getDBValue( "ref_supplier", "detail", "idarticle", $item->get( "idarticle" ) );
	 	
		?>
		<p>
			<span class="lightGrayText">Réf. fournisseur : <?php echo $ref_supplier ?></span>
			&nbsp;
			<a href="#" onclick="showSupplierInfos( <?php echo $item->get( "idsupplier" ) ?> ); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="" style="border-style:none; vertical-align:text-bottom;" />
			</a>
		</p>
		<?php 

	 }
	
	/*référence alternative @todo : n'existe pas*/
	
	/*lien fiche PDF*/
	
	 $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $item->get( "idarticle" ) );
	 
	if( $idproduct ){
		
		$doc = DBUtil::getDBValue( "doc", "detail", "idarticle", $item->get( "idarticle" ) );
		$href = strlen( $doc ) ? "{$GLOBAL_START_URL}{$doc}" : "$GLOBAL_START_URL/catalog/pdf_product.php?idproduct=$idproduct";
		
		?>
		<p>
			<a class="blueLink" href="<?php echo $href ?>" onclick="window.open(this.href); return false;">
				<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/pdf.gif" style="border-style:none;" alt="" />&nbsp;Voir la fiche Pdf
			</a>
		</p>
		<?php
			
	}
								 	
}

//-----------------------------------------------------------------------------------------------------

function displayStockPopup( &$item ){
	
	global $GLOBAL_START_URL;
	
	$stock_level = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $item->get( "idarticle" ) );
	if( $stock_level == -1 )
		return;
			
	?>
	<p style="text-align:center; margin:5px;">
		<a href="#" onclick="popupStock( '<?php echo $item->get( "reference" ) ?>' );return false;">
			<img src="<?php echo $GLOBAL_START_URL ?>/www/img/stock.jpg" alt="Informations sur le stock" style="border-style:none;" />
		</a>
	</p>
	<?php
			
}

//-----------------------------------------------------------------------------------------------------

function checkMinOrder( &$item ){
	
	$min_cde= DBUtil::getDBValue( "min_cde", "detail", "idarticle", $item->get( "idarticle" ) );
	if( $min_cde < 2 )
		return;
				
	?>
	<p style="text-align:center; margin:5px;">
		<span class="Marge">Qté min : <?php echo $min_cde ?></span>
	</p>
	<?php
		
}	

//-----------------------------------------------------------------------------------------------------
/**
 * @todo :o/
 */
function stripDesignationTags( &$item ){

	$designation = $item->get( "designation" );
	$designation = str_replace("\n",'', $designation);
	$designation = str_replace("\r",'', $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = str_replace('<br />',"\n", $designation);
	$designation = strip_tags($designation);
	
	return $designation;
		
}

//-----------------------------------------------------------------------------------------------------

function displayItemMargins( &$item ){
	
	?>
    <div class="tableContainer">
        <table class="dataTable devisTable summaryTable">
            <tr>
                <th colspan="2">Marge Brute Article  /  PV<br/>Hors Port</th>
            </tr>
            <tr>
                <td colspan="2">
				<?php
			
					/*marge brute HT*/
					
					$mb =  $item->get( "quantity" ) * ( $item->get( "discount_price" ) - $item->get( "unit_cost_amount" ) );
					echo Util::priceFormat( $mb );
					
				?>
				</td>
            </tr>
            <tr>
                <th>MB %</th>
                <th>Coeff.</th>
            </tr>
            <tr>
                <td style="width:50%">
                <?php
                
                	/*marge finale*/
				
					$mbf = $item->get( "discount_price" ) > 0.0 ? ( 1 - $item->get( "unit_cost_amount" ) / $item->get( "discount_price" ) ) * 100.0 : 0.0;
					echo Util::rateFormat( $mbf );
				
				?>
                </td>
				<td style="width:50%">
				<?php 
			
					/*coefficient de vente brut*/
					
					$rough_sale_coeff = $item->get( "unit_cost" ) > 0.0 ? $item->get( "unit_price" ) / $item->get( "unit_cost" ) : 0.0; 
					echo Util::numberFormat(  $rough_sale_coeff );
					
				?>
				</td>
            </tr>
        </table>
	</div>
	<?php
	       
}

//-----------------------------------------------------------------------------------------------------

?>
<?
//---------------------------------------------------------------------------------------------
?>
<br />
<?php

//---------------------------------------------------------------------------------------------

?>
<?php 

anchor( "UpdateTotalDiscountRate" );
anchor( "UpdateTotalDiscountAmount" );
anchor( "UpdateBillingRate" );
anchor( "UpdateBillingAmount" ); 
anchor( "UpdateInstallmentRate" ); 
anchor( "UpdateInstallmentAmount" ); 
anchor( "UpdateTotalChargeHT" ); 
anchor( "UpdateChargeSupplierRate" );
anchor( "UpdateChargeSupplierAmount" );
anchor( "UpdatePaymentDelay" );
anchor( "ApplySupplierCharges" );

?>
<div class="mainContent fullWidthContent">
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Montants et tarifications de la facture</p>
				<div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
            </div>
             <div class="leftContainer" style="margin-top:10px;width:89%;">
				<table class="dataTable devisTable">
                    <tr>
                    	<th>Prix vente<br />HT brut</th>
                        <th colspan="2">Remise supplémentaire<br />sur facture</th>
						<th>Total HT net</th>
						<th colspan="2">Port vente refacturé</th>
						<th>Total HT<br />avec port vente</th>
						<th colspan="2">Remise pour<br />paiement comptant</th>
						<th>Total HT</th>
						<th>Taux TVA</th>
						<th>Montant TVA</th>
						<th>Total TTC</th>
					</tr>
                    <tr>
                    <?php
                    
                    	$articlesET = 0.0;
						$it = $Order->getItems()->iterator();
						while( $it->hasNext() )
							$articlesET += $it->next()->getTotalET();
		
						?>
						<td rowspan="3"><?php echo Util::priceFormat( $articlesET ); ?></td>
						<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat(  $Order->get( "total_discount" ) ) ?> %</td>
						<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat( $Order->get( "total_discount_amount" ) ) ?> &euro;</td>
						<td rowspan="3"><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) ?></td>
						<td colspan="2"><?php echo $Order->get( "total_charge_auto" ) ? "Coût paramétré" : "Coût calculé"; ?></td>
						<td rowspan="3"><b><?php echo Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) + $Order->getChargesET() ) ?></b></td>
						<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat(  $Order->get( "billing_rate" ) ) ?> %</td>
						<td rowspan="3" style="white-space:nowrap;"><?php echo Util::numberFormat( $Order->get( "billing_amount" ) ) ?> &euro;</td>
						<td rowspan="3"><b><?php echo Util::priceFormat( $Order->getTotalET() ) ?></b></td>
						<td rowspan="3"><?php echo Util::rateFormat( $Order->getVATRate() ) ?></td>
						<td rowspan="3"><?php echo Util::priceFormat( ( $Order->getTotalET() ) * $Order->getVATRate() / 100.0 ); ?></td>
						<td rowspan="3"><b><?php echo Util::priceFormat( $Order->getTotalATI() ); ?></b></td>
					</tr>
					<tr>
						<th>Poids</th>
						<th>Port HT</th>
					</tr>
					<tr>
						<td><?php echo Util::numberFormat( $Order->getWeight() ) ; ?>&nbsp;kg</td>
						<td style="white-space:nowrap;"><?php echo $Order->get( "total_charge_ht" ) > 0.0 ? Util::priceFormat( $Order->get( "total_charge_ht" ) ) : "Franco"; ?></td>
					<tr>
            	</table>
 			</div><!-- leftContainer -->
 			<div class="rightContainer" style="width:95px; margin-top:10px;">
		    	<div class="tableContainer" style="margin-top:0px;">
		            <table class="dataTable devisTable summaryTable">
		                <tr>
		                    <th colspan="2">Marge nette après <br /> Port Achat et Vente</th>
		                </tr>
		                <tr>
		                    <td colspan="2"><?php echo Util::priceFormat( $Order->get( "net_margin_amount" ) ); ?></td>
		                </tr>
                        <tr>
                            <th>%</th>
                            <th>Coeff.</th>
                        </tr>
		                <tr>
		                    <td style="width:50%"><?php echo Util::rateFormat( $Order->get( "net_margin_rate" ) ) ?></td>
							<td style="width:50%"><?php echo Util::priceFormat( $Order->get( "net_margin_amount" ) ) ?></td>
		                </tr>
		            </table>
		    	</div><!-- tableContainer -->
    		</div><!-- rightContainer -->
    		<div class="leftContainer" style="margin-top:5px;">
	        	<input type="submit" class="blueButton" name="SaveOrder" value="Recalculer" />
	        </div>
	        <div class="clear"></div>
       	</div><!-- subContent -->
	</div><!-- content -->
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->
<?
//---------------------------------------------------------------------------------------------

?>
<br />
<?php 

//---------------------------------------------------------------------------------------------

//Infos paiement
?>

<?php

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

anchor( "UpdateIdPayment" );
anchor( "UpdatePaymentDel" );
anchor( "UpdateFactor" );
anchor( "UpdateNOrder" );
anchor( "UpdateBalanceAmount" );
anchor( "UpdateBalanceDate" );
anchor( "UpdateBillingComment" );

//----------------------------------------------------------------------------
//partie éditable si la commande n'a pas été facturée, payée ou annulée

$payment_disabled = "";

$no_tax_export = $Order->get( "no_tax_export" );

//----------------------------------------------------------------------------

?>
<div class="mainContent fullWidthContent">
	<div class="topRight"></div><div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Paiement et échéance</p>
                <div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
			</div>
			<div class="leftContainer" style="margin-top:10px;width:89%;">
				<table class="dataTable devisTable">
                    <tr>
                    	<th colspan="2" style="width:25%;">Mode de règlement</th>
						<th colspan="2" style="width:25%;">Délai de paiement</th>
						<th style="width:25%;">Assurance</th>
						<th style="width:25%;">Echéance de règlement</th>
                    </tr>
                	<tr>
                		<td colspan="2">
						<?php 
						
							DrawLift_modepayment( $Order->get( "idpayment" ) ? $Order->get( "idpayment" ) : 4 );
							if( $Order->get( "idpayment" ) == 1 ){ 
								?>
								<br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/pay_secure.php?IdOrder=<?php echo $IdOrder ?>" onclick="window.open(this.href); return false;">Lien vers le paiement sécurisé</a>
								<?php 
								
							} 
							
							?>
							<input type="hidden" id="UpdateIdPayment" name="UpdateIdPayment" value="ok" />
						</td>
                        <td colspan="2">
                    	<?php 
                    	 
							$idpayment_delay = $Order->get( "idpayment_delay" );
							if( empty( $idpayment_delay ) )
								$idpayment_delay = DBUtil::getParameter( "default_idpayment_delay" );
								
							DrawPaymentDelayList( $idpayment_delay );
							
						?>
                        </td>
                        <td>
                        	<input<?php if( !$Order->get( "insurance" ) ) echo " checked=\"checked\""; ?> type="radio" name="insurance" value="1"<?php echo $payment_disabled ?> /> oui
							<input<?php if( !$Order->get( "insurance" ) ) echo " checked=\"checked\""; ?> type="radio" name="insurance" value="0"<?php echo $payment_disabled ?> /> non
                        </td>
                        <td>
                            <?php 
							
								//Echeance
								
								$echeance = $Order->get( "deliv_payment" );
					
								if( $echeance == "0000-00-00" ){ //aucune date saisie
								
									$echeance = "";
									$startdate = date( "Y-m-d" );
									
								}
								else $startdate = $echeance;
								
							?>
							<input class="calendarInput" type="text" name="deliv_payment" id="deliv_payment" value="<?php echo usDate2eu( $echeance ); ?>" readonly="readonly" />
							<?php DHTMLCalendar::calendar( "deliv_payment" ) ?>
                        </td>
					</tr>
				</table>
				<table class="dataTable devisTable" style="margin-top:10px;">
					<tr>
						<th colspan="2" style="width:25%">Acompte</th>
						<th colspan="2" style="width:25%">Escompte</th>
						<th colspan="2" style="width:25%">Avoirs</th>
						<th style="width:25%">Facturation HT</th>
					</tr>
					<tr>
                        <td style="white-space:nowrap;"><?php echo Util::numberFormat( $Order->getDownPaymentRate() ) ?> %</td>
						<td style="white-space:nowrap;"><?php echo Util::numberFormat( $Order->getDownPaymentAmount() ) ?> &euro;</td>
						<?php
						
							if( $Order->getTotalATI() == 0.0 )
		                		$rebate_rate = 0.0;
		                	else $rebate_rate 	= $Order->get( "rebate_type" ) == "rate" 	? $Order->get( "rebate_value" ) : $Order->get( "rebate_value" ) * 100.0 / $Order->getTotalATI();
		                	$rebate_amount 	= $Order->get( "rebate_type" ) == "amount" ? $Order->get( "rebate_value" ) : $Order->getTotalATI() * $Order->get( "rebate_value" ) / 100.0;
  	
						?>
						<td><input<?php echo $payment_disabled ?> type="text" name="rebate_rate" class="textInput percentage" value="<?php echo Util::numberFormat( $rebate_rate ); ?>" /> %</td>
						<td><input<?php echo $payment_disabled ?> type="text" name="rebate_amount" class="textInput price" value="<?php echo Util::numberFormat( $rebate_amount ); ?>" /> &euro;</td>
						<td>
                        <?php
                        
                        		$credit_balance = $Order->getCustomer()->get( "credit_balance" );
                        		echo "dispo. : " . Util::priceFormat( $credit_balance );
                        		
                        ?>
                        </td>
                        <td>
                        	<input type="text" class="textInput" style="width:30px;" name="credit_amount" value="<?=Util::numberFormat($Order->get("credit_amount"));?>" />  &euro;
                        </td>
                        <td><?php echo $Order->get( "no_tax_export" ) ? "Oui" : "Non"; ?></td>
					</tr>
				</table>
			</div>
			<div class="rightContainer" style="width:95px; margin-top:10px;">
		    	<div class="tableContainer" style="margin-top:0px;">
		    	<?php
		    	
		    		if( $Order->getTotalATI() == 0.0 )
                		$rebate_rate = 0.0;
                	else $rebate_rate 	= $Order->get( "rebate_type" ) == "rate" 	? $Order->get( "rebate_value" ) : $Order->get( "rebate_value" ) * 100.0 / $Order->getTotalATI();
                	$rebate_amount 	= $Order->get( "rebate_type" ) == "amount" ? $Order->get( "rebate_value" ) : $Order->getTotalATI() * $Order->get( "rebate_value" ) / 100.0;
  	
                    ?>
		            <table class="dataTable devisTable summaryTable">
		            	<tr>
		            		<th colspan="2">Escompte</th>
		            	</tr>
		            	<tr>
		            		<td><?php echo Util::rateFormat( $rebate_rate ); ?></td>
		            		<td><?php echo Util::priceFormat( $rebate_amount ); ?></td>
		            	</tr>
		            	<tr>
		            		<th colspan="2">Solde à payer</th>
		            	</tr>
		            	<tr>
		            		<td colspan="2"><?php echo Util::priceFormat( $Order->getBalanceOutstanding() ); ?></td>
		            	</tr>
		          	</table>
				</div>
		    </div>
		    <br style="clear:both;" />
			<div class="leftContainer" style="width:89%;">
                <table class="dataTable devisTable" style="margin-top:10px; width:100%;">
	                <tr>
	                    <th colspan="2" style="width:20%;">Paiement comptant</th>
	                    <th colspan="2" style="width:20%;">Mode de règlement</th>
	                    <th colspan="2" style="width:20%;">Commentaire</th>
	                    <th style="width:20%;">Vos références de commande</th>
	                    <th style="width:20%;">Date de création interne</th>
	                </tr>
	                <tr>
	                	<td style="white-space:nowrap;" colspan="2">
	                	<?php 
	                	
	                		if( $Order->get( "idorder" ) && DBUtil::getDBValue( "cash_payment", "order", "idorder", $Order->get( "idorder" ) ) ){
	                			
	                			?>
	                			<input type="checkbox" checked="checked" readonly="readonly" /> <?php echo usDate2eu( $Order->get( "balance_date" ) ) ?>
	                			<?php
	                				
	                		} 
	                		else echo "non";
	                		
	                	?>
	                	</td>

	                	<td style="white-space:nowrap;" colspan="2">
							<?php echo htmlentities( DBUtil::getDBValue( "name" . User::getInstance()->getLang(), "payment", "idpayment", $Order->get( "idpayment" ) ) ); ?>
						</td>
						<td style="white-space:nowrap;" colspan="2">
						<?php
							
							if( $Order->get( "idorder" ) ){
								
								$payment_comment = DBUtil::getDBValue( "payment_comment", "order", "idorder", $Order->get( "idorder" ) );
								echo strlen( $payment_comment ) ? htmlentities( $payment_comment ) : "-";
								
							}
							else echo "-";
							
						?>
						</td>
						<td> 
							<input type="text" name="n_order" style="width:100px;" value="<?php if( intval( $Order->get( "n_order" ) )) echo $Order->get( "n_order" ); ?>" class="textInput" />
							<input type="image" name="UpdateNOrder" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="OK" style="vertical-align:top;" />
						</td>
						<td>
                    	<?php 
                    	
							if( $Order->get( "remote_creation_date" ) != "0000-00-00" )
								$remote_creation_date = usDate2eu( $Order->get( "remote_creation_date" ) );
							else $remote_creation_date = "";
							$startdate = empty( $remote_creation_date ) ? date( "Y-m-d" ) : $Order->get( "remote_creation_date" );
							
							?>
							<input class="calendarInput" type="text" name="remote_creation_date" id="remote_creation_date" value="<?php echo $remote_creation_date ?>" />
							<?php DHTMLCalendar::calendar( "remote_creation_date" ); ?>
                        </td>
	                </tr>
				</table>
			</div>
			<br style="clear:both;" />
			<div class="leftContainer">
				<input type="submit" style="float:left;margin-top:5px;" class="blueButton" value="Recalculer" name="SaveOrder" />
       		</div>
			<div class="clear"></div>
		</div>
	</div>
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div>
<?
//---------------------------------------------------------------------------------------------

//Commentaires
?>

<?php anchor( "UpdateComment" ); ?>
<div class="mainContent fullWidthContent">
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Commentaires</p>
				<div class="rightContainer"></div><!-- correction d'un bug IE -->
			</div>
	        <div class="clear" style="margin-top:10px;">
	        	<input type="text" class="textInput" style="width:100%;" name="billing_comment" value="<?php echo $Order->get( "billing_comment" ) ?>" />
	        </div> 
	        <div class="leftContainer">
		        <input type="submit" name="" class="blueButton" value="Modifier" style="margin-top:5px;" />
		    </div> 
		    <div class="clear"></div> 
		</div><!-- subContent -->
	</div><!-- content -->
    <div class="bottomRight"></div>
    <div class="bottomLeft"></div>
</div><!-- mainContent -->         

<?
//---------------------------------------------------------------------------------------------
?>
	<div class="fullWidthContent submitZone" style="margin-top:0;">
		<div style="float:right;">
			<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>">
			<input onclick="return confirm('Estes vous sur des modifications apportées?');" type="submit" class="blueButton" style="background-color:red;text-align:center; cursor:pointer;" value="<?php echo Dictionnary::translate("gest_com_save"); ?>" name="SaveOrder"/>
		</div>
	</div>
</form>
<a name="bottomPage"></a>

<?
//---------------------------------------------------------------------------------------------

//Paiements
?>

<!-- ************************************* ( o ) ( o ) résumé des paiements ( o ) ( o ) *********************************** -->

<div class="mainContent fullWidthContent">
	<div class="topRight"></div>
	<div class="topLeft"></div>
    <div class="content">
    	<div class="subContent">
        	<div class="subTitleContainer">
                <p class="subTitle">Paiements enregistrés pour cette facture</p>
				<div class="rightContainer">&nbsp;</div> <!-- correction d'un bug IE -->
            </div><!-- subTitleContainer -->
            <?php
            
            include_once( dirname( __FILE__ ) . "/../objects/RegulationsBuyer.php" );
            
            $regulations = RegulationsBuyer::getRegulations($Order->get("idbilling_buyer"));
            
            if(count($regulations)){
            ?>
			 <div class="tableContainer">
				<span style="font-weight:bold;color:red;font-size:14px;margin-top:5px;">
					Attention si vous supprimez un règlement et qu'il concerne d'autres factures, 
					celles-ci seront modifiées de la sorte à ce que vous y rentriez a nouveau le règlement. Elles changeront de statut et ne seront pas soldées.<br />
					Il faudra les recréer si besoin depuis l'écran d'enregistrement des paiements.<br />
					Les paiements groupés ne sont pas modifiables (suppression obligatoire).					
				</span>
				<table class="dataTable devisTable" style="margin-top:10px;">
					<tr>
						<th style="width:100px;">Num reglement</th>
						<th style="width:100px;">Date de paiement</th>
						<th style="width:100px;">Montant règlé</th>
						<th style="width:100px;">Total du règlement</th>
						<th style="width:100px;">Mode de règlement</th>
						<th style="width:50px;">Acompte</th>
						<th style="width:50px;">Type paiement</th>
						<th>Commentaire</th>
						<th>Modifier</th>
						<th>Suppr</th>
					</tr>
					<? for( $i=0 ; $i<count($regulations) ; $i++){ ?>
						<?
						//verif s'il y a plusieur factures pr ce reglement
						$qverifBillings = DBUtil::query("SELECT count(idbilling_buyer) as compteBils FROM billing_regulations_buyer WHERE idregulations_buyer = ".$regulations[$i]["idregulations_buyer"]);
						
						$disableMe="";
						if($qverifBillings!==false && $qverifBillings->fields("compteBils") > 1)
							$disableMe = " disabled=\"disabled\" ";						
						?>
						<form action="modify_invoice.php?IdInvoice=<?php echo $IdOrder ?>" method="post" name='frm' enctype="multipart/form-data">
							<input type="hidden" name="idRegulation" value="<?php echo $regulations[$i]["idregulations_buyer"] ?>">
							<input type="hidden" name="trtRegulations" value="1">
							<tr>
								<td><?php echo $regulations[$i]["idregulations_buyer"] ?></td>
								<td>
									<input <?=$disableMe?> class="calendarInput" type="text" name="regulationDate" id="regulationDate_<?php echo $regulations[$i]["idregulations_buyer"] ?>" value="<?php echo usDate2eu($regulations[$i]["payment_date"]) ?>" />
									<?php DHTMLCalendar::calendar( "regulationDate_".$regulations[$i]["idregulations_buyer"] ); ?>
								</td>
								<td>
									<?php echo Util::priceFormat($regulations[$i]["detailledAmount"]) ?>
								</td>
								<td><input <?=$disableMe?> type="text" class="textInput"style="width:40px;" name="amountRegulation" value="<?php echo $regulations[$i]["amount"] ?> ">&nbsp;&euro;</td>
								<td>
									<? DrawLift_modepayment( $regulations[$i]["idpayment"] ); ?>
								</td>
								<td>
									<select <?=$disableMe?> name="fromDownPayment">
										<option value="1" <? if($regulations[$i]["from_down_payment"]==1){ echo "selected=\"selected\""; }?>>Oui</option>
										<option value="0" <? if($regulations[$i]["from_down_payment"]==0){ echo "selected=\"selected\""; }?>>Non</option>
									</select>
								</td>
								<td>
									<select <?=$disableMe?> name="factor">
										<option value="1" <? if($regulations[$i]["factor"]==1){ echo "selected=\"selected\""; }?> >Factor</option>
										<option value="0" <? if($regulations[$i]["factor"]==0){ echo "selected=\"selected\""; }?> >Banque</option>
									</select>
								</td>
								<td>
									<input <?=$disableMe?> type="text" name="regulationComment" class="textInput" style="width:200px;" value="<?php echo stripslashes($regulations[$i]["comment"]) ?>">
								</td>
								<td><input <?=$disableMe?> type="image" name="updateRegulation" onclick="return confirm('Estes vous sur de votre modification?');" src="<?=$GLOBAL_START_URL?>/images/back_office/content/rightArrow.png"></td>
								<td><input type="image" name="delRegulation" onclick="return confirm('Estes vous sur de vouloir supprimer ce règlement?');" src="<?=$GLOBAL_START_URL?>/images/back_office/content/corbeille.jpg"></td>
							</tr>
						</form>
					<? } ?>
				</table>
			</div>
			<? }else{ ?>
					Aucun paiement enregistré pour cette facture
			<? } ?>
			<script type='application/javascript'>
				postop = (self.screen.height-450)/2;
				posleft = (self.screen.width-450)/2;
			</script>
			<input type="button" class="blueButton" name="createRegualtion" value="Créer un règlement" onclick="window.open('register_buyer_payment.php?updinvoice&buyer=<?=$Order->get("idbuyer")?>&billings=<?=$Order->get("idbilling_buyer")?>', '_blank', 'top=' + postop + ',left=' + posleft + ',width=850,height=500,resizable,scrollbars=yes,status=0');">
			<br style="clear:both;" />
		</div><!-- subContent -->
   	</div><!-- content -->
   	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div><!-- mainContent -->


</div><!-- GlobalMainContent --> 

<?php

//------------------------------------------------------------------------------------------------

function anchor( $name, $hasIndex = false ){

	if( $hasIndex ){
		
		foreach( $_POST as $key => $value ){
		
			$pos = strpos( $key, $name );	
			if( $pos !== false && !$pos ){
				
				echo "<a name=\"lastaction\"></a>";
				return;	
				
			}
			
		}
		
	}
	else if( isset( $_POST[ $name ] ) || isset( $_POST[ $name . "_x" ] ) )	
		echo "<a name=\"lastaction\"></a>";
	
}

//------------------------------------------------------------------------------------------------

function IsVolumeDiscounted($ref){
	
	global  $Order;
	
	$bdd=DBUtil::getConnection();
	
	$buyerfam = $Order->getCustomer()->get('idcustomer_profile');
	$today = date('Y-m-d');
	
	//On cherche dans la base de données si cette référence possède actuellement une remise au volume
	$Query="SELECT rate FROM multiprice_family WHERE idcustomer_profile='$buyerfam' and reference='$ref' and begin_date<='$today' and end_date>='$today'";
	$rs = $bdd->Execute($Query);
	
	if($rs->RecordCount()>0){
		//Il y a des remises au volume en ce moment pour cette référence
		return true;
	}else{
		return false;
	}
}

//------------------------------------------------------------------------------------------------
function uploadPJ($id){
	
	global 
			$GLOBAL_START_PATH,$Order;
		
	$YearFile = date("Y");
	$DateFile = date("d-m");
	
	$newdoc='vide';
	$filestoupload=array();
	
	//On remplit le tableau avec les fichiers uploadés
	if( isset( $_FILES[ "order_doc" ] ) && is_uploaded_file( $_FILES[ "order_doc" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_doc","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"PJ");
	}
	if( isset( $_FILES[ "offer_signed" ] ) && is_uploaded_file( $_FILES[ "offer_signed" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"offer_signed","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"OS");
	}
	if( isset( $_FILES[ "supplier_offer" ] ) && is_uploaded_file( $_FILES[ "supplier_offer" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"supplier_offer","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"SO");
	}
	if( isset( $_FILES[ "supplier_doc" ] ) && is_uploaded_file( $_FILES[ "supplier_doc" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"supplier_doc","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"SD");
	}
	
	
	//On parcours le tableau
	foreach($filestoupload as $upload){
		$fieldname = $upload["fieldname"];
	
		$target_dir = $upload["target"];
		$prefixe = $upload["prefixe"];
		
		$name_file = $_FILES[$fieldname]['name'];
		$tmp_file = $_FILES[$fieldname]['tmp_name'];
		
		$extension = substr($name_file, strrpos($name_file,"."));
	
		if(($extension!='.doc')&&($extension!='.jpg')&&($extension!='.xls')&&($extension!='.pdf')){
   			die("Le format de la documentation doit être .doc, .xls, .pdf, .jpg");
			return false;
		}
	
		// on copie le fichier dans le dossier de destination
    	$dest=$target_dir.$prefixe."_".$DateFile."_".$id.$extension;
		$newdoc= $YearFile."/".$prefixe."_".$DateFile."_".$id.$extension;
	
	
		if( !file_exists( $target_dir ) )
			//if( mkdir( $target_dir ) === false )
			//	die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $target_dir, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
	
		//On update dans la base le nom du fichier
		$requeteaexecuter = "UPDATE `order` SET $fieldname = '$newdoc' WHERE idorder = $id LIMIT 1";
		
		$connexionalabasededonnees = &DBUtil::getConnection();
		$resultatdelarequete = $connexionalabasededonnees->Execute( $requeteaexecuter );
	
		if( $resultatdelarequete === false )
			die( Dictionnary::translate("gest_com_impossible_join_link") );
		
		$Order->set( $fieldname, $newdoc );	
	
	}
	
}

//---------------------------------------------------------------------------------------------

/**
 * Recherche les options associées à un article
 *
 */
 
function GetOptions($idart){

	//On récupère l'IdProduct de l'article
	$Query = "SELECT idproduct FROM detail WHERE idarticle=$idart";
	$rsp =& DBUtil::query($Query);
	
	$IdProduct = $rsp->fields("idproduct");
	
	$SQL_Query = "select associat_product from associat_product where idproduct='$IdProduct' order by displayorder";
	$rs =& DBUtil::query($SQL_Query);
	
	if($rs->RecordCount()>0){
		return true;
	}else{
		return false;
	}
}

//---------------------------------------------------------------------------------------------

/**
 * Recherche les produits similaires
 *
 */
function GetSimilar($idart){

	//On récupère l'IdProduct de l'article
	$Query = "SELECT idproduct FROM detail WHERE idarticle=$idart";
	$rsp =& DBUtil::query($Query);
	
	$IdProduct = $rsp->fields("idproduct");

	$SQL_Query = "select idproduct_similar_1, idproduct_similar_2, idproduct_similar_3 from product where idproduct='$IdProduct'";
	$rs =& DBUtil::query($SQL_Query);
	
	
	$sim1 = $rs->fields("idproduct_similar_1");
	$sim2 = $rs->fields("idproduct_similar_2");
	$sim3 = $rs->fields("idproduct_similar_3");
	
	
	if(($sim1>0) || ($sim2>0) || ($sim3>0)){
		return true;
	}else{
		return false;
	}

}

//------------------------------------------------------------------------------------------------

/**
 * Retourne la socité du fournisseur en fontion d'un idsupplier
 */

function GetSupName($idsup){
	
	//On récupère l'IdProduct de l'article
	$Query = "SELECT name FROM supplier WHERE idsupplier=$idsup";
	$rs =& DBUtil::query($Query);
	
	$Name = $rs->fields("name");
	
	return $Name;
	
}

//----------------------------------------------------------------------------

function getPaymentTxt( $idpayment ){
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = User::getInstance()->getLang();
	$db = DBUtil::getConnection();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}


?>