<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement des factures proforma fournisseurs
 */

//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

$con = DBUtil::getConnection();

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

$resultPerPage = 50000;

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	search();
	
	exit();
		
}

$Title = "Enregistrement factures proforma fournisseurs";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
<?php

displayForm();

?>
</div> <!-- globalMainContent -->
<?php
	
include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier				= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) ? $_POST[ "idbl_delivery" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$idorder				= isset( $_POST[ "idorder" ] ) ? $_POST[ "idorder" ] : "";
	
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() {
			
			var options = {
				
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
				
			};
			
			$('#searchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			$("#selectionAmount").val(0);
			$("#selectionAmountDiv").html("0.00 ¤");
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( 'Une erreur est survenue', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function orderBy( str ){
			
			document.getElementById( 'searchForm' ).elements[ 'orderBy' ].value = str;
			
			$('#SearchForm').ajaxSubmit( { 
				
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postSubmitCallback  // post-submit callback
				
			} ); 
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectSince(){ document.getElementById( "interval_select_since" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectMonth(){ document.getElementById( "interval_select_month" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectBetween( cal ){ document.getElementById( "interval_select_between" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function goForm(){
		
			var options = {
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  			};
	 
    		// bind to the form's submit event 
    		$('#searchForm').ajaxSubmit(options); 

		}	
				
	/* ]]> */
	</script>
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}
		
	// -->
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
    	<div class="mainContent">
        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
            <div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_proforma.php" method="post" id="searchForm" name="searchForm">
	            	<div class="headTitle">
	                    <p class="title">Rechercher une commande fournisseur / un BL.</p>
	                    <div class="rightContainer">
	                    	<input type="radio" name="date_type" value="DateHeure"<?php if( $date_type == 'DateHeure') echo " checked=\"checked\""; ?> class="VCenteredWithText" />Date de création
	                        <input type="radio" name="date_type" value="dispatch_date"<?php if( $date_type == 'dispatch_date') echo " checked=\"checked\""; ?> class="VCenteredWithText" />Date de livraison
	                    </div>
	                </div>
	                <div class="subContent">
						<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
						<input type="hidden" name="search" value="1" />
                		<!-- tableau choix de date -->
                        <div class="tableContainer">
                            <table class="dataTable dateChoiceTable">
                                <tr>	
                                    <td><input type="radio" name="interval_select" id="interval_select_since" class="VCenteredWithText" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> />Depuis</td>
                                    <td>
                                        <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							            	<option value="0" <?php  if($minus_date_select==0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ;  ?></option>
							              	<option value="-1" <?php  if($minus_date_select==-1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ;  ?></option>
						              		<option value="-2" <?php  if($minus_date_select==-2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ;  ?></option>
							              	<option value="-7" <?php  if($minus_date_select==-7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ; ?></option>
							              	<option value="-14" <?php  if($minus_date_select==-14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ; ?></option>
							              	<option value="-30" <?php  if($minus_date_select==-30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ; ?></option>
							              	<option value="-60" <?php  if($minus_date_select==-60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ; ?></option>
							              	<option value="-90" <?php  if($minus_date_select==-90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ; ?></option>
							              	<option value="-180" <?php  if($minus_date_select==-180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ; ?></option>
							              	<option value="-365" <?php  if($minus_date_select==-365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ; ?></option>
							              	<option value="-730"<?php  if($minus_date_select==-730) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
							              	<option value="-1825"<?php  if($minus_date_select==-1825) echo " selected=\"selected\""; ?>>Moins de 5 ans</option>
							            </select>
                                    </td>
                                    <td><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> />Pour le mois de</td>
                                    <td>
                                        <?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                    </td>
                                </tr>
                                <tr>	
                                    <td><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?>/>Entre le</td>
                                    <td>
										<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
						 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                    </td>
                                    <td>et le</td>
                                    <td>
						 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4" class="tableSeparator"></td>
                                </tr>
                                <tr class="filledRow">
                                    <th>BL n°</th>
                                    <td colspan="3"><input class="textInput" type="text" name="idbl_delivery"  value="<?php echo $idbl_delivery ?>" /></td>
                                </tr>
                                <tr>
                                    <th>Commande fournisseur n°</th>
                                    <td><input type="text" name="idorder_supplier" value="<?php echo $idorder_supplier ?>" class="textInput" /></td>
                                    <th>Commande client n°</th>
                                    <td><input type="text" name="idorder" value="<?php echo $idorder ?>" class="textInput" /></td>
                                </tr>
                                <tr>
                                    <th>Fournisseur</th>
                                    <td colspan="3"><?php DrawSupplierList( $idsupplier ); ?></td>
                                </tr>
                                
                            </table>
                        </div><!-- tableContainer -->
                        <div class="submitButtonContainer">
                        	<input style="text-align: center;" type="submit" class="blueButton" value="Rechercher" />
                        </div>
                   </div><!-- subContent -->
                </form>
			</div><!-- content -->
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div><!-- mainContent -->
        <div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
		<div id="SearchResults"></div>
	<?php 

}

/////-----------------------

function search(){

	global $resultPerPage,$hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH;

	$now = getdate();

	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) ? $_POST[ "idbl_delivery" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$idorder				= isset( $_POST[ "idorder" ] ) ? $_POST[ "idorder" ] : "";
		
	switch ($interval_select){
			    
		case 1:
		 	//min |2000-12-18 15:25:53|
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`bl_delivery`.$date_type >='$Min_Time' AND `bl_delivery`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`bl_delivery`.$date_type >='$Min_Time' AND `bl_delivery`.$date_type <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`bl_delivery`.$date_type >= '$Min_Time' AND `bl_delivery`.$date_type <= '$Max_Time' ";
	     	
	     	break;
			
	}
	
		if( !$hasAdminPrivileges )
			$SQL_Condition .= " AND ( buyer.iduser = '$iduser' OR `bl_delivery`.iduser = '$iduser' )";
							
		if ( !empty($idsupplier) ){
				$idsupplier = FProcessString($idsupplier);
				$SQL_Condition .= " AND bl_delivery.idsupplier =$idsupplier";
		}
		
		if ( !empty($idbl_delivery) ){
				$idbl_delivery = FProcessString($idbl_delivery);
				$SQL_Condition .= " AND bl_delivery.idbl_delivery =$idbl_delivery";
		}
		
		if ( !empty($idorder_supplier) ){
				$idorder_supplier = FProcessString($idorder_supplier);
				$SQL_Condition .= " AND bl_delivery.idorder_supplier=$idorder_supplier";
		}
		
		if ( !empty($idorder) ){
				$idorder = FProcessString($idorder);
				$SQL_Condition .= " AND bl_delivery.idorder=$idorder";
		}
		
$con = DBUtil::getConnection();

$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
	
?>
<script type="text/javascript" language="javascript">
<!--
	
	function goToPage( pageNumber ){
	
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function registerInvoice(idsupplier){
		
		var checkboxes = document.forms.bltoinvoice.elements[ 'group_'+idsupplier ];
		
		var checked = false;
		var i = 0;
		
		var invoicehref = 'register_supplier_invoices.php?proforma&supplier='+idsupplier+'&bl=';
		
		if( !checkboxes.length ){
			
			if(checkboxes.checked){
				
				invoicehref = invoicehref+checkboxes.value+',';
				
				checked = true;
			}
			
		}
		
		while (i<checkboxes.length){ 
		
			if( checkboxes[ i ].checked ){
				
				invoicehref = invoicehref+checkboxes[ i ].value+',';
				
				checked = true;
			}
			
			i++;
		}
		
		if( !checked ){
			alert( 'Veuillez sélectionner au minimum un bon de livraison pour enregistrer sa facture' );
		}else{
			var lg = invoicehref.length;
			invoicehref=invoicehref.substring(0,lg-1);
			
			postop = (self.screen.height-550)/2;
			posleft = (self.screen.width-800)/2;

			window.open(invoicehref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=800,height=550,resizable,scrollbars=yes,status=0');
		}			
	}
// -->
</script>
<form action="" method="post" name="bltoinvoice" id="bltoinvoice">
<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
<input type="hidden" name="search" value="1" />
<?php

	//nombre de pages

	//@todo : pas de cohérence entre order_supplier.dispatch_date et bl_delivery_dispatch_date => supprimer les données dupliquées

	$SQL_Query = "
	SELECT COUNT(*) AS resultCount 
	FROM user, supplier, buyer, order_supplier, bl_delivery
	LEFT JOIN billing_supplier_row bsr ON ( bsr.idbl_delivery = bl_delivery.idbl_delivery AND bsr.idsupplier = bl_delivery.idsupplier )
	WHERE $SQL_Condition
	AND bsr.idbl_delivery IS NULL
	AND bl_delivery.iduser = user.iduser
	AND bl_delivery.idbuyer = buyer.idbuyer
	AND bl_delivery.idsupplier = supplier.idsupplier
	AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier";	

	$rs = $con->Execute($SQL_Query);
	
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );

	$pageCount = ceil( $resultCount / $resultPerPage );
	
	//recherche
	
	$start = ( $page - 1 ) * $resultPerPage;
	
	$SQL_Query = "
	SELECT GROUP_CONCAT( bl_delivery.idbl_delivery ) AS idbl_delivery,
		GROUP_CONCAT( bl_delivery.dispatch_date ) AS bl_dispatch_date,
		GROUP_CONCAT( bl_delivery.idbilling_buyer = 0 AND bl_delivery.date_send = '0000-00-00' AND date_delivery = '0000-00-00' ) AS deliveryNoteEditable,
		order_supplier.idorder_supplier,  
		order_supplier.idsupplier,
		order_supplier.confirmation_date,
		order_supplier.DateHeure, 
		order_supplier.total_amount_ht,
		order_supplier.total_amount,
		supplier.name,
		user.initial
	FROM user, supplier, buyer, order_supplier, bl_delivery
	LEFT JOIN billing_supplier_row bsr ON ( bsr.idbl_delivery = bl_delivery.idbl_delivery AND bsr.idsupplier = bl_delivery.idsupplier )
	WHERE $SQL_Condition
	AND bsr.idbl_delivery IS NULL
	AND bl_delivery.iduser = user.iduser
	AND bl_delivery.idbuyer = buyer.idbuyer
	AND bl_delivery.idsupplier = supplier.idsupplier
	AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier
	GROUP BY order_supplier.idorder_supplier
	ORDER BY supplier.name ASC, order_supplier.idorder_supplier ASC, bl_delivery.idbl_delivery ASC";
	
	$rs = $con->Execute($SQL_Query);
	
	$totalTTC = 0.0;
	while( !$rs->EOF() ){
		
		$totalTTC += $rs->fields("total_amount");
		$rs->MoveNext();
		
	}
	
	$rs->MoveFirst() ;
		
	?>
	<div class="mainContent fullWidthContent">
        <div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            <div class="headTitle">
                    <p class="title">
                    <?php
                    if($resultCount == 0){
                    	?>
                    	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun bon de livraison !</span>
                    <?php
                    }
                    else{
                    	?>
                    	Résultats de la recherche: <?php echo $resultCount ?> bon(s) de livraison - Montant TTC <?php echo Util::priceFormat( $totalTTC ) ?>
                    	<?php
                    }
                    ?>
                    </p>
                </div>
                <div class="subContent">
				<?php
			
			$last_idsupplier = false;
			
			while( !$rs->EOF() ){
				
				//nouveau fournisseur
							
				if( !$last_idsupplier || $rs->fields( "idsupplier" ) != $last_idsupplier ){
		
					$totalET 	= 0.0;
					$totalATI 	= 0.0;
					
					?>
					<a name="Anchor_<?php $rs->fields( "idsupplier" ) ?>"></a>
	                    <div class="subTitleContainer">
	                    <p class="subTitle"><?php echo $rs->fields( "name" ) ?></p>
	                    </div>
	                    <div class="tableContainer clear">
	                    <table class="dataTable resultTable">
	                        <thead>
				         		<tr>
					         		<th>Com.</th>
					         		<th>Notre N° Cde fourniss.</th>
					         		<th>Notre Date Cde</th>
					         		<th>Notre N° BL</th>
					         		<th>Modifier<br />la livraison</th>
					         		<th>Date Expédition Réelle</th>
						        	<th>Total HT</th>
						        	<th>Total TTC</th>
					        		<th>Regrouper / Regrouper</th>
				        		</tr>
			        		</thead>
			        		<tbody>
			        		<?php
	        			
				}
	
				//bons de livraison
							
				$deliveryNoteCount		= count( explode( ",", $rs->fields( "idbl_delivery" ) ) );		
				$idbl_delivery			= explode( ",", $rs->fields( "idbl_delivery" ) );
				$deliveryNoteEditable 	= explode( ",", $rs->fields( "deliveryNoteEditable" ) );
				$bl_dispatch_date		= explode( ",", $rs->fields( "bl_dispatch_date" ) );
				
				$rowspan = $deliveryNoteCount > 1 ? " rowspan=\"$deliveryNoteCount\"" : "";
				  	
				$i = 0;
				while( $i < $deliveryNoteCount ){
	
					?>
					<tr>
					<?php
					
						if( !$i ){
							
							?>
							<td<?php echo $rowspan; ?> class="lefterCol"><?php echo $rs->fields( "initial" ); ?></td> 
				        	<td<?php echo $rowspan; ?>><a href="<?php echo "$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=" . $rs->fields( "idorder_supplier" ) ?>" onclick="window.open(this.href); return false;"><?php echo $rs->fields( "idorder_supplier" ) ?></a></td>
				        	<td<?php echo $rowspan; ?>><?php echo Util::dateFormatEu(substr( $rs->fields("DateHeure"), 0, 10 ) ) ?></td>
						   	<?php
		        			
						}
					
						?>
						<td><a href="<?php echo "$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=" . $idbl_delivery[ $i ] ?>" onclick="window.open(this.href); return false;"><?php echo $idbl_delivery[ $i ] ?></a></td>
						<td>
						<?php
						
							if( $deliveryNoteEditable[ $i ] ){
	        			
								?>
								<a class="blueLink" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/delivery_notes.php?idorder_supplier=<?php echo $rs->fields( "idorder_supplier" ); ?>&amp;idbl_delivery=<?php echo $idbl_delivery[ $i ]; ?>" onclick="window.open(this.href); return false;">Modifier</a>
								<?php
								
							}
							else echo "-";
					
						?>
						</td>
						<td>
						<?php
						
							if( $rs->fields( "os_dispatch_date" ) == "0000-00-00" && $bl_dispatch_date[ $i ] == "0000-00-00" )
									$dispatch_date = "-";
							else if( $bl_dispatch_date[ $i ] != "0000-00-00" )
									$dispatch_date = Util::dateFormatEu( $bl_dispatch_date[ $i ] );
							else 	$dispatch_date = Util::dateFormatEu( $rs->fields("os_dispatch_date") );
				
							echo $dispatch_date;
				
						?>
						</td>
						<?php
						
							if( !$i ){
								
								?>
								<td<?php echo $rowspan; ?> style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
								<td<?php echo $rowspan; ?> style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
								<?php
				
							}
						
						?>
						<td class="righterCol">
							<input type="checkbox" name="group_<?php echo $rs->fields("idsupplier") ?>[]" id="group_<?php echo $rs->fields("idsupplier") ?>" value="<?php echo $idbl_delivery[ $i ] ?>" />
						</td>
					</tr>
					<?php
					
					$i++;
					
				}

				$last_idsupplier = $rs->fields( "idsupplier" );
							
				$totalET 	+= $rs->fields( "total_amount_ht" );
				$totalATI 	+= $rs->fields( "total_amount" );
				
	        	$rs->MoveNext();
	   
				//fin fournisseur
				
				if( $rs->EOF() || $last_idsupplier != $rs->fields( "idsupplier" ) ){
			
							?>
					 		<tr>
						 		<th colspan="6" style="border-style:none;"></th>
						 		<th class="totalAmount"><?php echo Util::priceFormat( $totalET ) ?></th>
						 		<th class="totalAmount"><?php echo Util::priceFormat( $totalATI ) ?></th>
						 		<th style="border-style:none;"></th>
					 		</tr>
					 		</tbody>
				 		</table>
			 		</div>
		            <div class="submitButtonContainer">
		            	<input type="button" value="Enregistrer la facture proforma" onclick="registerInvoice(<?php echo $last_idsupplier ?>);" class="blueButton" />
				    </div>
					<?php
				
				}

			}
			
		/*
		$idbl_delivery = $rs->Fields("idbl_delivery");
		$idorder_supplier = $rs->Fields("idorder_supplier");
		$idbl = $rs->fields( "idbl_delivery" );
		$initials =  $rs->fields( "initial" );
		$os_confirmation_date = $rs->fields("confirmation_date");
		$supplier_name = $rs->fields("name");
		$idsupplier = $rs->fields("idsupplier");
		$DateHeure = $rs->fields("DateHeure");
		$total_amount_ht = $rs->fields("total_amount_ht");
		$total_amount = $rs->fields("total_amount");
				
	
				
		$blHREF = "$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=$idbl";
		$osHREF = "$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=$idorder_supplier"; 

		
		if( $old_supplier_name!=$supplier_name ){
			if($i){
			if($fact>1){
			?>
	 		<tr>
		 		<th colspan="5" style="border-style:none;"></th>
		 		<th class="totalAmount"><?php echo Util::priceFormat( $total_frs_ht ) ?></th>
		 		<th class="totalAmount"><?php echo Util::priceFormat( $total_frs_ttc ) ?></th>
		 		<th style="border-style:none;"></th>
	 		</tr>
			<?php } ?>
			</tbody>
	 		</table>
	 		</div>
            <div class="submitButtonContainer">
            	<input type="button" value="Enregistrer la facture proforma" onclick="registerInvoice(<?=$old_idsupplier;?>);" class="blueButton" />
		    </div>
	 		<?
			$total_frs_ht = 0;
			$total_frs_ttc = 0;
			$fact = 0;
			}
			?>
	 			<a name="Anchor_<?php echo $idsupplier ?>"></a>
                    <div class="subTitleContainer">
                            <p class="subTitle"><?php echo $supplier_name ?></p>
                        </div>
                    <!-- tableau résultats recherche -->
                        <div class="tableContainer clear">
                        <table class="dataTable resultTable">
                            <thead>
			         		<tr>
				         		<th>Com.</th>
				         		<th>Notre N° Cde fourniss.</th>
				         		<th>Notre Date Cde</th>
				         		<th>Notre N° BL</th>
				         		<th>Date de Confirmation</th>
					        	<th>Total HT</th>
					        	<th>Total TTC</th>
				        		<th>Enregistrer / Regrouper</th>
			        		</tr>
		        		</thead>
		        		<tbody>
        <?php
		} 
        ?>
        <tr>
        	<td class="lefterCol"><?php echo $initials ?></td>
        	<td><a href="<?php echo $osHREF ?>" onclick="window.open(this.href); return false;"><?php echo $idorder_supplier ?></a></td>
        	<td><?php echo usDate2eu(substr( $DateHeure, 0, 10 ) ) ?></td>
        	<td><a href="<?php echo $blHREF ?>" onclick="window.open(this.href); return false;"><?php echo $idbl ?></a></td>
			<td><?php echo usDate2eu($os_confirmation_date) ?></td>
			<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount_ht ) ?></td>
			<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
			<td class="righterCol"><input type="checkbox" name="group_<?=$idsupplier ?>[]" id="group_<?=$idsupplier ?>" value="<?php echo $idbl ?>" /></td>
        </tr>
		
		<?
		
		$old_supplier_name = $supplier_name;
		$old_idsupplier = $idsupplier;
		$total_frs_ht+=$total_amount_ht;
		$total_frs_ttc+=$total_amount;
		$fact++;
		$rs->MoveNext();			
	
	}*/
	/*if($fact>1){
		
	?>
	<tr>
		<th colspan="5" style="border-style:none;"></th>
		<th class="totalAmount"><?php echo Util::priceFormat( $total_frs_ht ) ?></th>
		<th class="totalAmount"><?php echo Util::priceFormat( $total_frs_ttc ) ?></th>
		<th style="border-style:none;"></th>
	</tr>
	<?php } ?>
</tbody>
</table>
</div>
    <div class="submitButtonContainer">
    <input type="button" value="Enregistrer la facture proforma" onclick="registerInvoice(<?php echo $idsupplier ?>);" class="blueButton" />
    </div>
*/ ?>
                </div>
       		</div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div> <!-- mainContent fullWidthContent -->
	</form>
	<?php
	
}
//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------
function getBLTotalAmountHT($idbl_delivery){
	
	$bdd = DBUtil::getConnection();
	
	$Query = "SELECT osr.discount_price, osr.quantity FROM order_supplier os";	
}
	
?>