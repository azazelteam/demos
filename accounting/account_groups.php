<?php
/*
 * Package comptabilit�
 * Created on 01 Juillet 2016
 * @author jfCorb�
 * Regroupement des r�partions des �critures comptables  et r�sultat net
 */

header("Cache-Control: no-cache, must-revalidate");
header("Pragma: no-cache");
header("Content-Type: text/html; charset=utf-8");

//------------------------------------------------------------------------------------------------
include_once("../objects/classes.php");
include_once("$GLOBAL_START_PATH/objects/DHTMLCalendar.php");
include_once("$GLOBAL_START_PATH/objects/SupplierInvoice.php");
include_once( "../config/init.php" );
$Title = "R�sultat net";



$con = DBUtil::getConnection();
///////////////////////////////////////////////////////////////////////////
/* 
* Tooltip Content show 
* By behzad
*/
if( isset( $_GET[ 'showTooltip' ] ) && $_GET[ 'item' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( showTooltip( $_GET[ 'item' ] ) );

}

include("$GLOBAL_START_PATH/templates/back_office/head.php");

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/ui/i18n/ui.datepicker-fr.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/ui/blockUI.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css"/>
<script type="text/javascript" language="javascript" src="<?php echo $GLOBAL_START_URL ?>/js/ajax.lib.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css"/>
		<script type="text/javascript">

			
			var minDate = new Array();
				var maxDate = new Array();
			
			function changedate( fiscal_year ){
                       //$('#search_start').datepicker('option','minDate','2009-10-11');
						
						var parts = fiscal_year.split('to');
					$('#search_start').datepicker('setDate',parts[0]);
                    $('#search_end').datepicker('setDate',parts[1]);	
					$('#search_start').val(parts[0]);
                    $('#search_end').val(parts[1]);

				}	
		$(document).ready(function() {
		
		
			
			changedate($("#fiscal_year").val());
				
				
			//Handler of tooltip By behzad
	        $(".fournisseurHelp").each( function(){
		        $(this).qtip({
					show: {
						solo: true,
						when: {
							event: 'mouseover',
							
						}
					},
					hide:{
						fixed: true // Make it fixed so it can be hovered over
					 },
		            adjust: {
		                mouse: true,
		                screen: true
		            },
		            content: { url: '<?php echo HTTP_HOST; ?>/accounting/account_groups.php?showTooltip&item=' + $(this).attr( 'rel' ) },
		            position: {
		                corner: {
		                    target: "rightMiddle",
		                    tooltip: "leftMiddle"
		                }
		            },
		            style: {
		                background: "#FFFFFF",
						
						border: {
		                    color: "#EB6A0A",
		                    radius: 5
		                },
		                fontFamily: "Arial,Helvetica,sans-serif",
		                fontSize: "13px",
		                tip: true,
		                width: 650,
						
		            }
	        	});
	        	
	        });
			//end of Tooltip

		});

				
				/* ----------------------------------------------------------------------------------- */
			
		</script>

<?php 
	$query = "SELECT name,date_format(start_date,'%d-%m-%Y') start_date FROM `fiscal_years` ORDER BY `fiscal_years`.`start_date` ASC";
	$fiscal_years_resource = DBUtil::query( $query );
	$fiscal_years =array();
	while( !$fiscal_years_resource->EOF() ){
		$fiscal_years_resource->fields( "start_date" );
		$fiscal_years[] = array('start_date'=>$fiscal_years_resource->fields( "start_date" ), 'name'=>$fiscal_years_resource->fields( "name" ));
		$fiscal_years_resource->MoveNext();
	}
	$options = '';
	for($k = 0;$k<count($fiscal_years);$k++){
		if($k+1 == count($fiscal_years))
			$end_of_period = date('d-m-Y');
		else
			$end_of_period = date('d-m-Y',strtotime($fiscal_years[$k+1]['start_date'])-1);
		
		$selected = ($_REQUEST['fiscal_years'] == $fiscal_years[$k]['start_date'].'to'.$end_of_period) ? 'selected="selected"':'';
		$options[] = '<option '.$selected.'  value="'.$fiscal_years[$k]['start_date'].'to'.$end_of_period.'">'.$fiscal_years[$k]['name'].'</option>';
	}
	krsort($options);
	$options = implode('',$options);
?>
	<div id="globalMainContent">
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div>
			<div class="topLeft"></div>
			<div class="content">
	        <form method="post">	
				<div class="headTitle">
	                <p class="title">Recherche dans les r�sultats net</p>
					<div class="rightContainer">
						Exercice comptable : 
						<select name="fiscal_years" id="fiscal_year" onchange="changedate(this.value); return false;">
							<?php echo $options;?>
						</select>
					</div>	
	            </div>										
				<div class="subContent" >
			<input type="hidden" name="search_start" id="search_start" class="calendarInput" value="<?php echo $_REQUEST['search_start'] ?>" />
														
			<input type="hidden" name="search_end" id="search_end" class="calendarInput" value="<?php echo $_REQUEST['search_end']?>" />
						Date d�but : <?php echo $_REQUEST['search_start'] ?> / Date fin : <?php echo $_REQUEST['search_end']?>
												
					<!--<table class="dataTable">
												<tr>
													<th style="width:20%" class="filledCell">Entre le</th>
													<td style="width:20%">
														<input type="text" name="search_start" id="search_start" class="calendarInput" value="<?php echo $_REQUEST['search_start'] ?>" />
														<?php echo DHTMLCalendar::calendar( "search_start" ) ?>
													</td>
													<th style="width:20%" class="filledCell">et le</th>
													<td style="width:40%">
														<?php $date = date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ); ?>
														<input type="text" name="search_end" id="search_end" class="calendarInput" value="<?php echo $_REQUEST['search_end']?>" />
														<?php echo DHTMLCalendar::calendar( "search_end" ) ?>
													</td>
												</tr>
					</table>-->
					<br />
					<div class="submitButtonContainer">
						<input type='submit' value='Rechercher' class="blueButton"/>
					</div>
				
				</div>
			</div>
			</form>
	     	<div class="bottomRight"></div>
			<div class="bottomLeft"></div>
	     </div>

<?php 
	showResultTable(); 
?>
</div><!--GlobalMainContent-->


<?php

include("$GLOBAL_START_PATH/templates/back_office/foot.php");

//--------------------------------------------------------------------------------
function date_convertor_to_db($date){

	if(empty($date))
		return $date;

	$date_arr = explode('-', $date);
	return $date_arr[2].'-'.$date_arr[1].'-'.$date_arr[0];
	
}

function showResultTable()
{
	$critera ='';
	if(isset($_REQUEST['search_start'])){
		// list($fiscal_years_start, $fiscal_years_end) = explode('to',$_REQUEST['fiscal_years']);
		$critera = "AND date >= '".date_convertor_to_db($_REQUEST['search_start'])."' ";
		
	}
	if(isset($_REQUEST['search_end'])){
		// list($fiscal_years_start, $fiscal_years_end) = explode('to',$_REQUEST['fiscal_years']);
		$critera .= "AND date <= '".date_convertor_to_db($_REQUEST['search_end'])."' ";
		
	}
	if ($critera =='') return '';
	$query = "SELECT `accounting_grouping`,idaccounting_grouping FROM `accounting_grouping` ORDER BY accounting_grouping.display";
	$rs1 = DBUtil::query( $query );
	while( !$rs1->EOF() ){
		$header[$rs1->fields( "idaccounting_grouping")] = $rs1->fields( "accounting_grouping" );
		$rs1->MoveNext();
	}
	
	/* before edit 6 task 1.a we show last year  as codes below:
		$last_year =  (date('Y',time())-1) . '-'.date('m',time()).'-00';
		$critera  =" AND date > '$last_year' "
	
	*/
	$query = "
		SELECT
			DATE_FORMAT( date, '%Y-%m' ) AS date_mois,
			accounting_grouping, 
			SUM( account_plan.amount * CASE account_plan.amount_type WHEN 'debit' THEN -1 ELSE 1 END ) AS sum_amount,
			accounting_grouping.view
		FROM
			accounting_grouping,
			accounting_plan,
			account_plan 
		WHERE
			accounting_plan.idaccounting_grouping = accounting_grouping.`idaccounting_grouping` 
			AND account_plan.amortizement < 1
			AND account_plan.idaccount_plan = accounting_plan.idaccounting_plan 
			$critera
		GROUP BY
			`accounting_plan`.`idaccounting_grouping`,
			DATE_FORMAT( date, '%Y%m' ) 
		ORDER BY
			date desc,
			`accounting_plan`.`idaccounting_grouping`";
	// echo $query;
	$rs = DBUtil::query( $query );
	$result_array = '';			
	while( !$rs->EOF() ){
		$result_array[$rs->fields( "date_mois" )][$rs->fields( "accounting_grouping" )] = array('sum_amount'=>$rs->fields( "sum_amount" ),
																									  'view'=>$rs->fields( "view" )) ;
	$rs->MoveNext();
	}

    ?>
<style type="text/css">

</style>


<div class="mainContent fullWidthContent"  >
    <div class="topRight"></div>
    <div class="topLeft"></div> 
	<h1></h1> 
    <div class="content">
        <div class="headTitle">
            <p class="title">R�sultats net</p>
            <div class="rightContainer"></div>	
        </div>										
        <div class="subContent" >
            <table class="dataTable resultTable tablesorter" style="margin: 0 auto; width: 95%;">
                <thead>
                <tr> 
                	<th style="vertical-align:middle;"> </th>
            <?php
                foreach($header as $accounting_grouping_name){
                    $column_sum[$accounting_grouping_name] = 0;//initial 0 for sum of column
                    ?>
                                        <th style="vertical-align:middle;"><?php echo $accounting_grouping_name ?></th>
            <?php }?>
                	<th style="vertical-align:middle;">R�sultat</th>
                </tr>
                </thead>
                <tbody>

<?php
//ALTER TABLE  `accounting_grouping` ADD  `view` BOOL NOT NULL DEFAULT  '0'
// print_r($result_array);
// print_r($header);

	foreach($result_array as $date_mois=>$accounting_grouping){
		echo '<tr class="amountRow">';
		echo '<td>'.$date_mois.'</td>';
		$total=0;//every row sum
		//$column = 0;
		foreach($header as $the_key => $accounting_grouping_name){
				//print_r($accounting_grouping);
				
				if(@isset($result_array[$date_mois][$accounting_grouping_name]))
				{
					//$amount_type = $accounting_grouping[$accounting_grouping_name]['amount_type'];
					$sum_amount = $accounting_grouping[$accounting_grouping_name]['sum_amount'];
					$htm = $sum_amount/*.$accounting_grouping_name*/;
					$total += $htm;
					
					$column_sum[$accounting_grouping_name] += $htm; //calculating column sum
					
				}else{
					$htm = '0';//blank Items
				}
				//print_r($result_array[$date_mois]);
				//Tootip
					if($result_array[$date_mois][$accounting_grouping_name]['view']=='1'){
						$tooltip_html = "<img src='". HTTP_HOST ."/images/back_office/content/help.png' 
											rel=".$date_mois .'hash'.$the_key." class='fournisseurHelp' />";
					}else{
						$tooltip_html ='';
					}
				//End of tooltip	
				echo '<td>'.$htm.$tooltip_html.'</td>';//for R�sultat row sum
			}
		echo '<td>'.round($total,2).'</td>';//for R�sultat row sum
		echo '</tr>';
	}
	$sumcolumns_of_sum_rows = array_sum($column_sum);
	$column_sum =  implode('</td><td>',$column_sum);
	echo "<tr><td>R�sultat</td><td>$column_sum</td><td>$sumcolumns_of_sum_rows</td></tr>";
	
	?>
            	</tbody>
            </table>
	    </div>
    </div>
	<div class="bottomRight"></div>
	<div class="bottomLeft"></div>
</div>


</div> <!-- mainContent fullWidthContent -->
<?php
}

//--------------------------------------------------------------------------------------------------
function showTooltip($hash){
	list($date,$accounting_grouping) = explode('hash', $hash);
	$date_range_begin = $date.'-01';
	$date_array = explode('-', $date);
	$month_tmp = $date_array[1]+1;
	
	if($month_tmp<10){
		$month = '0'.$month_tmp;
	}else{
		$month = $month_tmp;
	}
	$date_range_end = $date_array[0].'-'.$month.'-01';
	$tooltip_max_rows = DBUtil::getParameterAdmin( "tooltip_max_rows" )+1;
	$query = "
		SELECT
			account_plan.*
		FROM
			accounting_grouping,
			accounting_plan,
			account_plan 
		WHERE
			accounting_plan.idaccounting_grouping = accounting_grouping.`idaccounting_grouping` 
			AND accounting_grouping.idaccounting_grouping = '$accounting_grouping'
			AND account_plan.amortizement < 1
			AND account_plan.idaccount_plan = accounting_plan.idaccounting_plan 
			AND date >='$date_range_begin'
			AND date <'$date_range_end' 
			LIMIT $tooltip_max_rows
		";
	// echo $query;
	//echo $tooltip_max_rows;
	$rs = DBUtil::query( $query );
	$rows ='';
	$counter = 1;
	while( !$rs->EOF() ){
		$amount = $rs->fields( "amount_type" ) == 'debit' ? -1 * $rs->fields( "amount" ) : $rs->fields( "amount" );
		$rows .= "<tr>
			<td>".$rs->fields("idaccount_plan")."</td>
			<td>".$rs->fields("date")."</td>
			<td>".$amount."</td>
			<td>".$rs->fields("designation")."</td>
			</tr>";
		$rs->MoveNext();
		$tooltip_max_rows;
		if(++$counter == $tooltip_max_rows){
			$rows .= "<tr><td colspan=\"4\">
						<img src='". HTTP_HOST ."/images/back_office/content/icons-warning.png'/>
						 Le nombre de lignes � afficher d�passe la limite autoris�e par les param�tres.
					  </td></tr>";
			break;
		}
	}
	
	
	$tooltip_content = "
	<strong>Commande fournisseur n�$idbl_delivery</strong>
	<div class='content' style='padding:5px;'>
		<div class='subContent'>
			<div class='resultTableContainer'>
				<table class='dataTable resultTable' >
					<thead>
						<tr>
							<th>Ecriture</th>
							<th>Date</th>
							<th>Montant </th>
							<th width='50%'>Designation </th>
						</tr>
					</thead>
					<tbody>
							$rows
					</tbody>
				</table>
			</div>
		</div>
	</div>
	";
	
	return $tooltip_content;
}
?>
