<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Lien vers les différents paiements SSL
 */
 
include ("../objects/classes.php");

//---------------------------------------------------------------------------------------
// Vérifiaction du login 
//---------------------------------------------------------------------------------------

//connexion
if (isset ($_POST["Login"]) && isset ($_POST["Password"]))
	User::getInstance()->login(stripslashes($_POST["Login"]), md5(stripslashes($_POST["Password"])));

if (!User::getInstance()->getId()) {

	$banner = "no";

	include_once ("$GLOBAL_START_PATH/templates/back_office/head.php");

	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate("expired_session") . "</p>";

	include_once ("$GLOBAL_START_PATH/templates/back_office/foot.php");

	exit ();

}

if ( !isset( $_GET[ "IdOrder" ] ) &&  !isset( $_GET[ "IdEstimate" ] ) ) {

	die("<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate("missing_parameter") . "</p>");

}

if( isset( $_GET[ "IdOrder" ] ) ){

	$IdOrder =  $_GET["IdOrder"];
	$Order = new Order( $IdOrder );

}

if( isset( $_GET[ "IdEstimate" ] ) ){

	$IdEstimate =  $_GET["IdEstimate"];
	$Estimate = new Estimate( $IdEstimate );
	
	//On confirme la commande puis on traite
	$Estimate->set( "status", Estimate::$STATUS_ORDERED );
	
	$Estimate->save();
	
	$Order = TradeFactory::createOrderFromEstimate( $Estimate , Order::$STATUS_ORDERED );

	//générer les commandes fournisseur
	TradeFactory::createSupplierOrders( $Order );

	$IdOrder = $Order->getId();

}

$_SESSION[ "idorder_secure" ] = $IdOrder;

$sEmail = $Order->getCustomer()->getContact()->getEmail();
$sDate = date("d/m/Y:H:i:s");

$Montant = round($Order->get('total_amount'), 2);
$Reference_Cde = urlencode($IdOrder);

$tpeVer = DBUtil::getParameterAdmin( "CMCIC_version" );

switch ( $tpeVer ){
	case "3.0":
		include_once( dirname( __FILE__ ) . "/../objects/PaySecureV3.php" );			
		$pay_secure = new PaySecureV3();			
		$pay_secure->CreerFormulaireHmac( $sEmail , $Montant , $Reference_Cde , $sDate );			
		break;
		
	case "1.2open":
		include_once( dirname( __FILE__ ) . "/../objects/PaySecure.php" );			
		$pay_secure = new PaySecure();				
		$pay_secure->CreerFormulaireHmac( $Montant , $Reference_Cde );  			
		break;
	
	default :
		die( " TPE introuvable : La version n'est pas renseignée dans le paramètrage " );
		break;

}

exit ();
?>