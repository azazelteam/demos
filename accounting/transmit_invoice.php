<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi par mail d'une facture client
 */
 

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );
//------------------------------------------------------------------------------------------------

$Title = "E-mail d'accompagnement - " . getCompany();

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
include_once($GLOBAL_START_PATH.'/catalog/drawlift.php');
//------------------------------------------------------------------------------------------------

include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
$DEBUG = '';

//--------------------------------------------------------------------------------------------

if( empty( $_GET[ "IdInvoice" ] ) )
	die( Dictionnary::translate("parameter_absent") );



if( isset( $_GET[ "IdInvoice" ] ) ){
	
	$IdInvoice = intval($_GET[ "IdInvoice" ]);
	
	$Invoice = new Invoice( $IdInvoice );
	showDebug( $IdInvoice, "Chargement facture n°", "code" );
	
}
	
//--------------------------------------------------------------------------------------------
?>
<div id="globalMainContent">
<?php
if( isset( $_POST[ "send" ] ) || isset( $_POST[ "send_x" ] ) )
	sendMail();
	
if( isset( $_POST[ "save" ] ) || isset( $_POST[ "save_x" ] ) )
	saveMail($_POST[ "message" ]);

displayMailForm();

?>
</div>
<?php

//--------------------------------------------------------------------------------------------

function displayMailForm(){
	
	global 	$GLOBAL_START_URL, 
			$GLOBAL_START_PATH, 
			$IdInvoice,
			$Invoice;
	
	$AdminName= DBUtil::getParameterAdmin( "ad_name" );

	$iduser = User::getInstance()->getId();
	
	$svg_mail = $Invoice->get( "svg_mail" );
	
	//-----------------------------------------------------------------------------------------
				
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( $Invoice->getCustomer()->get( "id_lang" ) ); //@todo : imposer la langue du destinataire

	$editor->setTemplate( "ORDER_INVOICE" );
	
	$editor->setUseInvoiceTags( $IdInvoice );
	$editor->setUseOrderTags( $Invoice->get( "idorder" ) );
	
	
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $Invoice->get( "idbuyer" ),$Invoice->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	if( isset( $_POST[ "message" ] ) )
		$Message = stripslashes( $_POST[ "message" ] );
	else if( !empty( $svg_mail ) )
		$Message = $svg_mail;
	else $Message = $editor->unTagBody();
	
	if( isset( $_POST[ "subject" ] ) )
		$Subject = stripslashes( $_POST[ "subject" ] );
	else $Subject = $editor->unTagSubject();
				
	//-----------------------------------------------------------------------------------------
				
	?>
	<script type="text/javascript" language="javascript">
	function GeneratePopupToPrint() {
		w=window.open("",'popup','width=400,height=200,toolbar=no,scrollbars=no,resizable=yes');	
		w.document.write("<TITLE>Email personnalisé : Impression</TITLE>");
		w.document.write("<BODY onload=\"window.print()\"> <b>Sujet :</b>  "+document.forms[0].elements["subject"].value+"<br /><br />");
		w.document.write("<b>Message :</b> "+document.forms[0].elements["message"].value);
		w.document.write("</BODY>");
		w.close();
	}
	</script>
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form action="transmit_invoice.php?IdInvoice=<?php echo $IdInvoice ?>" method="post" enctype="multipart/form-data">
		
		
<a name="topPage"></a>
	<div class="mainContent fullWidthContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
        	<div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<div class="headTitle">
                    <p class="title">Envoi de facture par mail</p>
					<div class="rightContainer"></div>
                </div>
                <div class="subContent">
					<div class="tableHeaderLeft">
						<a href="#bottomPage" class="goUpOrDown">
							Aller en bas de page
							<img src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
						</a>
					</div>
					<div class="tableHeaderRight">
						<input type="button" value="Facture PDF n°<?php echo $IdInvoice ?>" class="blueButton" style="text-align:center; cursor:pointer;" onClick="window.open('<?=$GLOBAL_START_URL?>/catalog/pdf_invoice.php?idinvoice=<?php echo $IdInvoice ?>','_blank');"/>
					</div>
                	<!-- tableau résultats recherche -->
                    <div class="resultTableContainer clear">
                    	<table class="dataTable resultTable" style="border:none; border-style:none;">
                    		<tbody>
		
								<tr>
									<td style="width:auto; border:0;">
										<div style="margin-left:auto; margin-right:auto; border:0;">
											<?php
											
											$idbuyer = $Invoice->getCustomer()->getId();
											$contacts = $Invoice->getCustomer()->getContacts();
											
											if($contacts->size()>0){
												?>
												<b>Destinataire(s) :</b><br/>
												<div style="width:100%; border: 1px solid #CBCBCB;">
												<table border="0" style="border:0; border-style:none;">
													<tr>
													<?php 
													
													$it = $contacts->iterator();
													$i = 0;
													while( $it->hasNext() ){
														
														$contact = $it->next();
														
														?>
														<td style="border:none; font-weight:bold;"><?php echo $contact->get("firstname") . " " . $contact->get("lastname"); ?></td>
														<td style="border:none;" width="15" align="center">A <br /><input type="checkbox" name="a_<?=$i?>" value="<?php echo $contact->get("mail"); ?>"></td>
														<td style="border:none;" width="15" align="center">Cci <br /><input type="checkbox" name="cci_<?=$i?>" value="<?php echo $contact->get("mail"); ?>"></td>
														<td style="border:none;" width="15">&nbsp;</td>
														<?php
														
														$i++;
														
													}
													?>
													</tr>
												</table>
												</div>
											<?php } ?>
											<br/>
											<b><?php echo Dictionnary::translate("object") ; ?></b>
											<br />
											<input type="text" name="subject" value="<?php echo $Subject ?>" class="textInput" />
											<p style="margin-top:15px; width:100%; text-align:center;">
												<b><?php echo Dictionnary::translate("message") ; ?></b>
												<br />
												<?php WYSIWYGEditor::createEditor( "message", $Message, "style=\"width:500px; height:450px;\"" ); ?>
											</p>
											<p style="margin-top:20px; text-align:center;">
												<font color="#cc3333"><?php echo Dictionnary::translate("gest_com_modify_title_text") ; ?>.</font>
											</p>
											<p style="margin-top:20px; text-align:center;">
												<input type="submit" name="send" class="blueButton" style="text-align:center;cursor:pointer;" value="<?php echo Dictionnary::translate("Envoi mail + facture") ; ?>" />
												<input type="submit" name="save" class="blueButton" style="text-align:center;cursor:pointer;"  value="<?php echo Dictionnary::translate("register") ; ?>" />
												<input type="button" name="print" class="blueButton" style="text-align:center;cursor:pointer;"  onclick="javascript:GeneratePopupToPrint()" value="<?php echo Dictionnary::translate("print") ; ?>" />
												<input type="button" class="blueButton" style="text-align:center;cursor:pointer;"  value="<?php echo Dictionnary::translate( "back" ) ?>" onclick="document.location='<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $IdInvoice; ?>';" />
											</p>
										</div>
									</td>
									
									
									<!--<td rowspan="3" style="vertical-align:top; width:200px; padding-left:15px; border:0;">
									
										
									</td>-->
								
								
								
								</tr>
							</tbody>
                </table>
            </div>
            <a href="#topPage" class="goUpOrDown">
            	Aller en haut de page
                <img src="<?php echo $GLOBAL_START_URL?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
            </a>
       	</div>
	</div>
    <div class="bottomRight"></div><div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->
	<a name="bottomPage"></a>

	</form>

	<?php
	
}

//--------------------------------------------------------------------------------------------

function saveMail($msg){
	
	global 	$IdInvoice,
			$Invoice;
	
	$bdd = DBUtil::getConnection();
	
	if( isset( $_POST[ "send" ] ) || isset( $_POST[ "send_x" ] ) ){
		$Invoice->set( "svg_mail" , $msg );
		$Invoice->set( "send_by" , "mail" );
	}else{
		$Invoice->set( "svg_mail" , $msg );
	}
	$Invoice->save();

}
//--------------------------------------------------------------------------------------------

function sendMail(){

	global 	$GLOBAL_START_PATH,
			$GLOBAL_START_URL,
			$IdInvoice,
			$Invoice;
			
	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$AdminName= DBUtil::getParameterAdmin( "ad_name" );
	$AdminEmail = DBUtil::getParameterAdmin( "ad_mail" );
	
	$receipt = array();
	
	$BuyerName = $Invoice->getCustomer()->getContact()->get( "firstname" ).' '.$Invoice->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $Invoice->getCustomer()->getContact()->get( "mail" );
	
	$receipt[] = $BuyerEmail;
		
	$iduser = User::getInstance()->getId();
	
	if( !empty($iduser)  && $iduser == DBUtil::getParameter('contact_commercial') ) {
		
		$CommercialName= User::getInstance()->get('firstname').' '.User::getInstance()->get('lastname');
		$CommercialEmail = User::getInstance()->get('email');
		$CommercialPhone = User::getInstance()->get('phonenumber');		
	}
	else {
	
		$CommercialName= DBUtil::getParameterAdmin('ad_name');
		$CommercialEmail = DBUtil::getParameterAdmin('ad_mail');
		
	}
	
	$Subject = stripslashes( $_POST[ "subject" ] );
	$Message = stripslashes( $_POST[ "message" ] );
	
	$mailer =new htmlMimeMail();
	
	$mailer->setFrom( '"'.$CommercialName.'" <'.$CommercialEmail.'>');
	$mailer->setHtml( $Message );
	$mailer->setSubject( $Subject );
	
	//ne pas envoyer ici au commercial à caude de l'accusé de réception caché dans le mail 
	
	//Gestion des contacts
	
	$it = $Invoice->getCustomer()->getContacts()->iterator();
	
	$i = 0;
	while( $it ->hasNext() ){
		
		$contact = $it->next();
		
		$contLastName = $contact->get("lastname");
		$contFirstName = $contact->get("firstname");
		$contName = $contFirstName.' '.$contLastName;
		$idcontact = $contact->get("idcontact");
		
		if( (isset($_POST["a_$i"])) && (isset($_POST["cci_$i"]))){
			$a=$_POST["a_$i"];
			$receipt[] = $a;
			//saveContactsSend( $Invoice, $contact->getId(), 'A' );
		}else if(isset($_POST["a_$i"])){
			$a=$_POST["a_$i"];
			$receipt[] = $a;
			//saveContactsSend( $Invoice, $contact->getId(), 'A' );
		}else if(isset($_POST["cci_$i"])){
			$cci=$_POST["cci_$i"];
			$mailer->setBcc($cci);
			//saveContactsSend( $Invoice, $contact->getId(), 'CCi' );
		}

		$i++;
		
	}
	
	if( file_exists( "$GLOBAL_START_PATH/www/cgv.pdf" ) ){
		ob_start();
		readfile("$GLOBAL_START_PATH/www/cgv.pdf");
		$pdf_cgv = ob_get_contents();
		ob_end_clean();
		$mailer->addAttachment($pdf_cgv,'cgv.pdf','application/pdf');
	}
	

	//ajout du PDF
	
	$pdf_name = "Facture_$IdInvoice.pdf";

	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
	$odf = new ODFInvoice( $IdInvoice );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment($pdfData,$pdf_name,'application/pdf');
	
	$result = $mailer->send($receipt);
	
	if(!$result)
		die("Problème d'envoi");
		
	saveMail( $Message );
	

	//if( $mailer->is_errors() )  
		//die( $mailer->get_errors() );
		
	//-------------------------------------------------------------------------------------------------------
	
	//copie au commercial
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) ){
		
		$Subject = stripslashes( $_POST[ "subject" ] );
		$Message = stripslashes( $_POST[ "message" ] );
		
		$mailer = new htmlMimeMail();
		
	
		$mailer->setFrom( '"'.$AdminName.'" <'.$AdminEmail.'>');
		$mailer->setHtml( $Message );
		$mailer->setSubject( "(" . Dictionnary::translate( "copy" ) . ") $Subject" );
	
		if( file_exists( "$GLOBAL_START_PATH/www/cgv.pdf" ) ){
			ob_start();
			readfile("$GLOBAL_START_PATH/www/cgv.pdf");
			$pdf_cgv = ob_get_contents();
			ob_end_clean();
			$mailer->addAttachment($pdf_cgv,'cgv.pdf','application/pdf');
		}
		
		//ajout du PDF
		
		$mailer->addAttachment( $pdfData,$pdf_name,'application/pdf' );	
		
		
		$result = $mailer->send(array($CommercialEmail));
		
		if(!$result)
			die("Problème d'envoi");
			
	}
		
	?>
	<div class="mainContent fullWidthContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
        <div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
                <div class="subContent">
				<div><?php echo Dictionnary::translate("La facture a été envoyée par Email"); ?></div>
				<br /><br />
				<p stlyle="text-align:center; margin:20px; font-size:14px;"><?php echo Dictionnary::translate("gest_com_mail_sent") ; ?> <?php echo $Invoice->getCustomer()->getContact()->get( "mail" ); ?></p>
				<p stlyle="text-align:center; margin:20px; font-size:14px;"><?php echo Dictionnary::translate("gest_com_copy_sent") ; ?> <?php echo $CommercialEmail; ?></p>
				<p style="text-align:center; margin:20px;">
					<input type="button" value="Retour" onclick="document.location='com_admin_invoice.php?IdInvoice=<?php echo $IdInvoice ?>';" class="blueButton" />
				</p>
			</div>
		</div>
    	<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div> <!-- mainContent fullWidthContent -->
		<?php
	
}

//--------------------------------------------------------------------------------------------

function HumanReadablePhone($tel){
	//On regarde la longeueur de la cheine pour parcourir
	$lg=strlen($tel);

	$telwcs='';
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($tel,$i,1);
	
		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if($i==0){
			if(($tocheck=='+')OR(is_numeric($tocheck))){
				$telwcs.=$tocheck;
			}
		}else{
			//On ne garde que les chiffres
			if(is_numeric($tocheck)){
				$telwcs.=$tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg=strlen($telwcs);

	$telok ='';

	
	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck=substr($telwcs,0,1);

	if($tocheck=='+'){
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,3);
		$telok.=$tocheck." ";
		$cpt=3;
	}else{
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,2);
		$telok.=$tocheck." ";
		$cpt=2;
	}

	//On traite le reste de la chaine
	for($i=$cpt;$i<$lg;$i+=2){
		if($i==$cpt){
			$tocheck=substr($telwcs,$i,2);
		}else{
			$tocheck=substr($telwcs,$i,2);
		}
		$telok.=$tocheck." ";
	}	

	//On enlève l'espace de fin
	$telok=trim($telok);

	return $telok;
}    

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-------------------------------------------------------------------------------------------

function getCompany(){
	
	$db =& DBUtil::getConnection();
	
	if( !isset( $_GET[ "IdEstimate" ] ) )
		return "";
		
	$idestimate =  $_GET[ "IdEstimate" ];

	$query = "
	SELECT b.company
	FROM buyer b, estimate e
	WHERE e.idestimate = '$idestimate'
	AND b.idbuyer = e.idbuyer
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )	
		die( "Impossible de récupérer le nom de la société" );
		
	if( !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "company" );
	
}

//-------------------------------------------------------------------------------------------

?>