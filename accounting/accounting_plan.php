<?php
/*
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire pour gérer le plan comptable
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_accounting_plan";
    $Str_TableName="accounting_plan";
    $KeyName = 'idaccounting_plan';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idaccounting_plan";
	$ArrayFieldsColumns2Show[1]="accounting_plan_1";

// show the page 
	include "../formbase/stdformbase.php";
	
?>