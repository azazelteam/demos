<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement factures fournisseurs
 */

//------------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/DHTMLCalendar.php" );
include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
include_once( dirname( __FILE__ ) . "/../catalog/admin_func.inc.php" );
include_once( dirname( __FILE__ ) . "/../catalog/drawlift.php" );
include_once( dirname( __FILE__ ) . "/../objects/SupplierInvoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/SupplierOrder.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "update" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
		
	error_reporting( 0 );
	ModifyDate();
	
	exit();
	
}
//------------------------------------------------------------------------------------------
/* 
* Tooltip Content show 
* By behzad
*/
if( isset( $_GET[ 'showTooltip' ] ) && $_GET[ 'idBlDelivery' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( showTooltip( $_GET[ 'idBlDelivery' ] ) );

}
//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$foundSupplierOrders = search();
	
	if( isset( $_REQUEST[ "proposal" ] ) && $_REQUEST[ "proposal" ] == "1" )
		searchMore( $foundSupplierOrders );

	exit();
		
}

// -----------------------------------------------------------------------------------------------

$Title = "Enregistrement factures fournisseurs";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
<?php

displayForm();

?>
</div> <!-- globalMainContent -->
<?php
	
include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier				= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) ? $_POST[ "idbl_delivery" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$idorder				= isset( $_POST[ "idorder" ] ) ? $_POST[ "idorder" ] : "";
	$orderBy				= isset( $_POST[ "orderBy" ] ) ? $_POST[ "orderBy" ] : "";
	
	?>
	
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() {
			
			var options = {
				
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
				
			};
			
			$('#searchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			$("#selectionAmount").val(0);
			$("#selectionAmountDiv").html("0.00 ¤");
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( 'Une erreur est survenue', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			$('#proposal').val('0');
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function orderBy( str ){
			
			document.getElementById( 'searchForm' ).elements[ 'orderBy' ].value = str;
			
			$('#SearchForm').ajaxSubmit( { 
				
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postSubmitCallback  // post-submit callback
				
			} ); 
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectSince(){ document.getElementById( "interval_select_since" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectMonth(){ document.getElementById( "interval_select_month" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectBetween( cal ){ document.getElementById( "interval_select_between" ).checked = true; }
		
		/* ----------------------------------------------------------------------------------- */
		
		function goForm(){
		
			var options = {
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  			};
	 
    		// bind to the form's submit event 
    		$('#searchForm').ajaxSubmit(options); 

		}	
				
	/* ]]> */
	</script>
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		
	// -->
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
    	<div class="mainContent">
        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
        
            <div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<form action="/accounting/received_supplier_invoices.php" method="post" id="searchForm" name="searchForm">
            		<input type="hidden" name="proposal" id="proposal" value="0" />
	            	<div class="headTitle">
	                    <p class="title">Enregistrer une facture fournisseur / Commande / BL</p>
	                    <div class="rightContainer">
	                    	<input type="radio" name="date_type" value="DateHeure"<?php if( !isset( $_POST[ "date_type" ] ) || $_POST[ "date_type" ] == "DateHeure" ) echo " checked=\"checked\""; ?> class="VCenteredWithText" />Date de création
	                        <input type="radio" name="date_type" value="dispatch_date"<?php if( isset( $_POST[ "date_type" ] ) && $_POST[ "date_type" ] == 'dispatch_date') echo " checked=\"checked\""; ?> class="VCenteredWithText" />Date de livraison
	                    </div>
	                </div>
	                <div class="subContent">
						<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
						<input type="hidden" name="search" value="1" />
						<input type="hidden" name="orderBy" value="<?php echo $orderBy ?>" />
                		<!-- tableau choix de date -->
                        <div class="tableContainer">
                            <table class="dataTable dateChoiceTable">
                                <tr>	
                                    <td><input type="radio" name="interval_select" id="interval_select_since" class="VCenteredWithText" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> />Depuis</td>
                                    <td>
                                        <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							            	<option value="0" <?php  if($minus_date_select==0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ;  ?></option>
							              	<option value="-1" <?php  if($minus_date_select==-1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ;  ?></option>
						              		<option value="-2" <?php  if($minus_date_select==-2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ;  ?></option>
							              	<option value="-7" <?php  if($minus_date_select==-7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ; ?></option>
							              	<option value="-14" <?php  if($minus_date_select==-14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ; ?></option>
							              	<option value="-30" <?php  if($minus_date_select==-30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ; ?></option>
							              	<option value="-60" <?php  if($minus_date_select==-60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ; ?></option>
							              	<option value="-90" <?php  if($minus_date_select==-90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ; ?></option>
							              	<option value="-180" <?php  if($minus_date_select==-180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ; ?></option>
							              	<option value="-365" <?php  if($minus_date_select==-365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ; ?></option>
							              	<option value="-730"<?php  if($minus_date_select==-730) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
							              	<option value="-1825"<?php  if($minus_date_select==-1825) echo " selected=\"selected\""; ?>>Moins de 5 ans</option>
							            </select>
                                    </td>
                                    <td><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> />Pour le mois de</td>
                                    <td>
                                        <?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                    </td>
                                </tr>
                                <tr>	
                                    <td><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?>/>Entre le</td>
                                    <td>
										<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
						 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                    </td>
                                    <td>et le</td>
                                    <td>
						 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4" class="tableSeparator"></td>
                                </tr>
                                <tr>
                                    <th>Fournisseur</th>
                                    <td><?php DrawSupplierList( $idsupplier ); ?></td>
                                    <th>BL n°</th>
                                    <td>
                                    	<input class="textInput" type="text" name="idbl_delivery"  value="<?php echo $idbl_delivery ?>" />
                                    </td>
                                </tr>
                                <tr>
                                    <th>Commande fournisseur n°</th>
                                    <td><input type="text" name="idorder_supplier" value="<?php echo $idorder_supplier ?>" class="textInput" /></td>
                                    <th>Commande client n°</th>
                                    <td><input type="text" name="idorder" value="<?php echo $idorder ?>" class="textInput" /></td>
                                </tr>
                                <tr>
                                	<td style="border-style:none;" colspan="4">
                                		<!-- n'afficher QUE la marchandise expédiée -->
                                		<input type="checkbox" name="use_delivery_date" value="1" checked="checked" /> N'afficher que les commandes dont la marchandise a été expédiée
                                	</td>
                                </tr>                            
                            </table>
                        </div><!-- tableContainer -->
                        <div class="submitButtonContainer">
                        	<input style="text-align: center;" type="submit" class="blueButton" value="Rechercher" />
                        </div>
                   </div><!-- subContent -->
                </form>
			</div><!-- content -->
            <div class="bottomRight"></div><div class="bottomLeft"></div>
		</div><!-- mainContent -->
		<div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
    	<div id="SearchResults"></div>
	<?php 

}

/////-----------------------

function search(){

	global $resultPerPage,$hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH;

	$supplierOrders = array();	
	
	$now = getdate();

	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$idbl_delivery 			= isset( $_POST[ "idbl_delivery" ] ) ? $_POST[ "idbl_delivery" ] : "";
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) ? $_POST[ "idorder_supplier" ] : "";
	$idorder				= isset( $_POST[ "idorder" ] ) ? $_POST[ "idorder" ] : "";
	
	$SQL_ConditionAvoir		= "1";
	
	switch ($interval_select){
			    
		case 1:
		 	//min |2000-12-18 15:25:53|
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`bl_delivery`.$date_type >='$Min_Time' AND `bl_delivery`.$date_type <='$Now_Time' ";
			$SQL_ConditionAvoir .= " AND `bl_delivery`.$date_type >='$Min_Time' AND `bl_delivery`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`bl_delivery`.$date_type >='$Min_Time' AND `bl_delivery`.$date_type <'$Max_Time' ";
			$SQL_ConditionAvoir .= " AND `bl_delivery`.$date_type >='$Min_Time' AND `bl_delivery`.$date_type <'$Max_Time' ";
			
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`bl_delivery`.$date_type >= '$Min_Time' AND `bl_delivery`.$date_type <= '$Max_Time' ";
	     	$SQL_ConditionAvoir .= " AND `bl_delivery`.$date_type >= '$Min_Time' AND `bl_delivery`.$date_type <= '$Max_Time' ";
	     	
	     	break;
			
	}
	
		if( !$hasAdminPrivileges )
			$SQL_Condition .= " AND ( buyer.iduser = '$iduser' OR `bl_delivery`.iduser = '$iduser' )";
							
		if ( !empty($idsupplier) ){
				
				$SQL_Condition .= " AND bl_delivery.idsupplier = '$idsupplier'";
				$SQL_ConditionAvoir	.= " AND bs.idsupplier = '$idsupplier' ";
		}
		
		if ( !empty($idbl_delivery) ){
				
				$SQL_Condition .= " AND bl_delivery.idbl_delivery = '$idbl_delivery'";
		}
		
		if ( !empty($idorder_supplier) ){
			
				$SQL_Condition .= " AND bl_delivery.idorder_supplier= '$idorder_supplier'";
		}
		
		if ( !empty($idorder) ){
				
				$SQL_Condition .= " AND bl_delivery.idorder= '$idorder'";
		}
		
$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
	
?>
<script type="text/javascript" language="javascript">
<!--
	
	function goToPage( pageNumber ){
	
		document.forms.adm_estim.elements[ 'page' ].value = pageNumber;
		document.forms.adm_estim.submit();
		
	}
	
	function registerInvoice(idsupplier){
		
		var checkboxes = document.forms.bltoinvoice.elements[ 'group_'+idsupplier ];
		
		var checked = false;
		var i = 0;
		
		var invoicehref = 'register_supplier_invoices.php?supplier='+idsupplier+'&bl=';
		
		if( !checkboxes.length ){
			
			if(checkboxes.checked){
				
				invoicehref = invoicehref+checkboxes.value+',';
				
				checked = true;
			}
			
		}
		
		while (i<checkboxes.length){ 
		
			if( checkboxes[ i ].checked ){
				
				invoicehref = invoicehref+checkboxes[ i ].value+',';
				
				checked = true;
			}
			
			i++;
		}
		
		// on regarde les boutons radio mtn
		var radios = document.forms.bltoinvoice.elements[ 'supplier_'+idsupplier ];
		
		var i = 0;
	
		if( !radios.length ){
			
			if(radios.checked){
				
				invoicehref = invoicehref+radios.value+',';
				
				checked = true;
			}
			
		}
		
		while (i<radios.length){ 
		
			if( radios[ i ].checked ){
				
				invoicehref = invoicehref+radios[ i ].value+',';
				
				checked = true;
			}
			
			i++;
		}
		
		if( !checked ){
			alert( 'Veuillez sélectionner au minimum un bon de livraison pour enregistrer sa facture' );
		}else{
			var lg = invoicehref.length;
			invoicehref=invoicehref.substring(0,lg-1);
			
			postop = (self.screen.height-550)/2;
			posleft = (self.screen.width-800)/2;

			window.open(invoicehref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=800,height=550,resizable,scrollbars=yes,status=0');
		}			
	}
	
	/* -------------------------------------------------------------------------------------------------- */
	
	function registerPartialInvoices( idsupplier ){
	
		var deliveryNotes = "";
		
		$( "#factures" ).find( "input[type=checkbox][checked=true]" ).each( function( i ){
		
			if( $( this ).attr( "name" ) == "group_" + idsupplier + "[]" ){
			
				if( deliveryNotes.length )
					deliveryNotes += ",";
					
				deliveryNotes += $( this ).val();
			
			}
			
		});
		
		$( "#factures" ).find( "input[type=radio][checked=true]" ).each( function( i ){
		
			if( $( this ).attr( "name" ) == "supplier_" + idsupplier + "[]" ){
			
				if( deliveryNotes.length )
					deliveryNotes += ",";
					
				deliveryNotes += $( this ).val();
			
			}
			
		});
			
		if( !deliveryNotes.length )
			return;
		
		document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?delivery_notes=" + deliveryNotes + "&idsupplier=" + idsupplier;
		
	}
	
	/* -------------------------------------------------------------------------------------------------- */
	
	function registerPartialInvoicesProforma( idsupplier ){
	
		var deliveryNotes = "";
		
		$( "#proforma" ).find( "input[type=checkbox][checked=true]" ).each( function( i ){
		
			if( $( this ).attr( "name" ) == "groupPro_" + idsupplier + "[]" ){
			
				if( deliveryNotes.length )
					deliveryNotes += ",";
					
				deliveryNotes += $( "#supplierProT_" + idsupplier ).val();
			
			}
			
		});
		
		$( "#proforma" ).find( "input[type=radio][checked=true]" ).each( function( i ){
		
			if( $( this ).attr( "name" ) == "supplierPro_" + idsupplier + "[]" ){
			
				if( deliveryNotes.length )
					deliveryNotes += ",";
					
				deliveryNotes += $( "#supplierProT_" + idsupplier ).val();
			
			}
			
		});
			
		if( !deliveryNotes.length )
			return;
		
		document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_invoices.php?delivery_notes=" + deliveryNotes + "&idsupplier=" + idsupplier;
		
	}
	
	/* -------------------------------------------------------------------------------------------------- */
	
	function proformaToInvoice ( idsupplier ){
		
		var prohref = 'proforma_to_billing_supplier.php?ids=' + idsupplier + '&bs=';
		
		postop = (self.screen.height-550)/2;
		posleft = (self.screen.width-800)/2;
		
		var checkboxes = document.forms.bltoinvoice.elements[ 'groupPro_' + idsupplier ];
		var checked = false;
		var i = 0;
				
		if( !checkboxes.length ){
			
			if( checkboxes.checked ){
				
				prohref = prohref + checkboxes.value+',';
				
				checked = true;
			}
			
		}
		
		while ( i <checkboxes.length ){ 
		
			if( checkboxes[ i ].checked ){
				
				prohref = prohref + checkboxes[ i ].value + ',';
				
				checked = true;
			}
			
			i++;
		}
		
		// on regarde les boutons radio mtn
		var radios = document.forms.bltoinvoice.elements[ 'supplierPro_' + idsupplier ];
		
		var i = 0;
	
		if( !radios.length ){
			
			if( radios.checked ){
				
				prohref = prohref + radios.value + ',';
				
				checked = true;
			}
			
		}
		
		while ( i < radios.length ){ 
		
			if( radios[ i ].checked ){
				
				prohref = prohref + radios[ i ].value + ',';
				
				checked = true;
			}
			
			i++;
		}
		
		if( !checked ){
			alert( 'Veuillez sélectionner au minimum un bon de livraison pour enregistrer sa facture' );
		}else{
			var lg = prohref.length;
			prohref = prohref.substring( 0 , lg - 1 );
									
			window.open( prohref , '_blank' , 'top=' + postop + ',left=' + posleft + ' , width=800 , height=550 , resizable , scrollbars = yes , status = 0');
		
		}
	}	

	function SetDate( element ){

		var idorders = element.id.substring( element.id.lastIndexOf( '_', element.id.length ) + 1, element.id.length );
		
		var availibility = $( "#availability_date_" + idorders ).attr( "value" );
		var dispatch = $( "#dispatch_date_" + idorders ).attr( "value" );
		
		var postData = "action=update&idos=" + idorders + "&av=" + availibility + "&disp=" + dispatch
		$.ajax({

			url: "<?php echo $GLOBAL_START_URL; ?>/accounting/received_supplier_invoices.php",
			async: true,
			cache: false,
			type: "GET",
			data: postData,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la date" ); },
		 	success: function( responseText ){

				if( responseText == 'wrong disp date format' )
					alert( "Format de date d'expédition incorrect !" );
				else if( responseText == 'wrong av date format' )
					alert( "Format de date de mise à disposition incorrect !" );
        		else if( responseText.length ){
        		
        			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( '', 'Modifications enregistrées' );

        			$( "#cellstatus_" + idorders ).html( responseText );
        			$( "#cellstatus_" + idorders ).attr( "class", "" );
        			$( "#celldelete_" + idorders ).html( "" );
        			
        		}	
					
			}

		});
		
		
	}

	/* -------------------------------------------------------------------------------------------------- */

// -->
</script>
<form action="" method="post" name="bltoinvoice" id="bltoinvoice">
<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
<input type="hidden" name="search" value="1" />
<?php

	$internal_supplier = DBUtil::getParameterAdmin( "internal_supplier" );
	
	$query = "
	SELECT GROUP_CONCAT( bl_delivery.idbl_delivery ) AS idbl_delivery,
		GROUP_CONCAT( bl_delivery.dispatch_date ) AS bl_dispatch_date,
		GROUP_CONCAT( bl_delivery.idbilling_buyer = 0 AND bl_delivery.date_send = '0000-00-00' AND date_delivery = '0000-00-00' ) AS deliveryNoteEditable,
		order_supplier.idorder_supplier,  
		order_supplier.idsupplier,
		supplier.name,
		order_supplier.dispatch_date AS os_dispatch_date,
		order_supplier.DateHeure, 
		user.initial,
		order_supplier.total_amount_ht,
		order_supplier.total_amount,
		order_supplier.rebate_type,
		order_supplier.rebate_value,
		bl_delivery.idbuyer,
		buyer.company,
		contact.lastname,
		contact.idbuyer
		
	FROM user, supplier, buyer, order_supplier, contact, bl_delivery
	LEFT JOIN billing_supplier_row ON ( billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery AND billing_supplier_row.idsupplier = bl_delivery.idsupplier )
	WHERE $SQL_Condition
	AND supplier.idsupplier = order_supplier.idsupplier
	AND billing_supplier_row.idbl_delivery IS NULL
	AND bl_delivery.iduser = user.iduser
	AND bl_delivery.idbuyer = buyer.idbuyer
	";
	if( isset( $_REQUEST[ "use_delivery_date" ] ) )
		$query .= "
		AND ( bl_delivery.dispatch_date<>'0000-00-00' OR  order_supplier.dispatch_date<>'0000-00-00' )";
	$query .= "
	AND bl_delivery.idsupplier = supplier.idsupplier
	AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier
	AND order_supplier.idsupplier <> '$internal_supplier'
	AND order_supplier.total_amount_ht <> 0
	AND order_supplier.idorder_supplier_parent = 0
	AND order_supplier.status NOT IN( 'cancelled', 'standby' )
	AND bl_delivery.idbuyer = contact.idbuyer
	GROUP BY order_supplier.idorder_supplier
	ORDER BY supplier.name ASC, order_supplier.idorder_supplier ASC, bl_delivery.idbl_delivery ASC";
	
	/*
	$query .= "
	AND bl_delivery.idsupplier = supplier.idsupplier
	AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier
	AND order_supplier.idsupplier <> '$internal_supplier'
	AND order_supplier.total_amount_ht <> 0
	AND order_supplier.idorder_supplier_parent IS NOT NULL
	AND order_supplier.status NOT IN( 'cancelled', 'standby' )
	GROUP BY order_supplier.idorder_supplier
	ORDER BY supplier.name ASC, order_supplier.idorder_supplier ASC, bl_delivery.idbl_delivery ASC";
	*/
	//echo $query;
	$rs = DBUtil::query($query);

	$totalTTC = 0.0;
	while( !$rs->EOF() ){
		
		$totalTTC += $rs->fields("total_amount");
		$rs->MoveNext();
		
	}

	$rs->MoveFirst() ;
	
	$resultCount = $rs->RecordCount();
	
	?>
	<div class="mainContent fullWidthContent">
        <div class="topRight"></div><div class="topLeft"></div>
            <div id="zizi">
			<ul class="menu" style="margin-bottom:10px;">
				<li class="item">
					<div class="right"></div><div class="left"></div>
					<div class="centered"><a id="blA" href="#factures">Commandes à facturer</a></div>
				</li>
				<li class="item">
					<div class="right"></div><div class="left"></div>
					<div class="centered"><a id="proformaA" href="#proforma">Factures proforma reçues</a></div>
				</li>
			</ul>
			
            <div class="content" id="factures">
            	<div class="headTitle">
                    <p class="title">
                    <?php
                    
                    	if($resultCount == 0){
                    		
                    		?>
                    		<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucune commande à facturer !</span>
                    		<?php
                    		
                    	}
                    	else{
                    		
                    		?>
                    		Résultats de la recherche: <?php echo $resultCount ?> commandes - Montant TTC <?php echo Util::priceFormat( $totalTTC ) ?> 
                    		<?php
                    		
                    	}
                    	
                    	?>
                   	 </p>
                </div>
                <div class="subContent">
				<?php
				
					$last_idsupplier = false;
	
					while( !$rs->EOF() ){
						
						array_push( $supplierOrders, $rs->fields( "idorder_supplier" ) );
					
						//nouveau fournisseur
						
						if( !$last_idsupplier || $rs->fields( "idsupplier" ) != $last_idsupplier ){
				
							$totalET 	= 0.0;
							$totalATI 	= 0.0;
							$totalReb	= 0.0;
							$totalNet	= 0.0;
							
							?>
							<a name="Anchor_<?php $rs->fields( "idsupplier" ) ?>"></a>
		                    <div class="subTitleContainer">
		                    	<p class="subTitle"><?php echo $rs->fields( "name" ) ?></p>
		                    </div>
		                    <div class="tableContainer clear">
		                    	<table class="dataTable resultTable">
		                        	<thead>
						         		<tr>
							         		<th>Com.</th>
							         		<th>Cde<br />fournisseur</th>
							         		<th>Date Cde</th>
							         		<th>N° BL</th>
                                            <th>N° de client</th>
                                            <th>Nom du client</th>
							         		<th>Date Expédition<br />Réelle</th>
								        	<th>Total HT</th>
								        	<th>Total TTC</th>
								        	<!--
								        	<th>Escompte</th>
								        	<th>Net à payer</th>
								        	-->
							        		<th>Enregistrer</th>
							        		<th>Regrouper</th>
						        		</tr>
					        		</thead>
					        		<tbody>
					        		<?php
			        			
						}
		
						//bons de livraison
						
						//$deliveryNoteCount		= (count( explode( ",", $rs->fields( "idbl_delivery" ) ) )/ 2) ;		
						$deliveryNoteCount		= 1 ;	
						$idbl_delivery			= explode( ",", $rs->fields( "idbl_delivery" ) );
						$deliveryNoteEditable 	= explode( ",", $rs->fields( "deliveryNoteEditable" ) );
						$bl_dispatch_date		= explode( ",", $rs->fields( "bl_dispatch_date" ) );
					
						$rowspan = $deliveryNoteCount > 1 ? " rowspan=\"$deliveryNoteCount\"" : "";

						$rebate_value	= $rs->fields( "rebate_value" );
						$rebate_type	= $rs->fields( "rebate_type" );
						
						if( $rs->fields( "total_amount" ) > 0.0 ){
							
							$rebate_rate	= $rebate_type == 'rate' 	? $rebate_value : round( $rebate_value / $rs->fields( "total_amount" ) * 100 , 2 ) ;
							$rebate_amount	= $rebate_type == 'amount'	? $rebate_value : round( $rs->fields( "total_amount" ) * $rebate_value / 100 , 2 ) ;

						}
						else $rebate_rate = $rebate_amount = 0.0;
						
						$total_net_amount = $rs->fields( "total_amount" ) - $rebate_amount;

						  	
						$i = 0;
						while( $i < $deliveryNoteCount ){

							?>
							<tr>
							<?php
							
								if( !$i ){
									
									?>
									<td<?php echo $rowspan; ?> class="lefterCol"><?php echo $rs->fields( "initial" ); ?></td> 
						        	<td<?php echo $rowspan; ?>><a href="<?php echo "$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=" . $rs->fields( "idorder_supplier" ) ?>" onclick="window.open(this.href); return false;"><?php echo $rs->fields( "idorder_supplier" ) ?></a></td>
						        	<td<?php echo $rowspan; ?>><?php echo Util::dateFormatEu(substr( $rs->fields("DateHeure"), 0, 10 ) ) ?></td>
								   	<?php
				        			
								}
							
								?>
								<td>
									<a href="<?php echo "$GLOBAL_START_URL/catalog/pdf_delivery.php?iddelivery=" . $idbl_delivery[ $i ] ?>" onclick="window.open(this.href); return false;"><?php echo $idbl_delivery[ $i ] ?></a>
									<img src="<?php echo HTTP_HOST;?>/images/back_office/content/help.png" alt="Commande fournisseur <?php echo $idbl_delivery[ $i ] ?>, chargement en cours..." rel="<?php echo $idbl_delivery[ $i ] ?>" class="fournisseurHelp" />
								</td>
                            <?php    
							if( !$i ){
							?>
                            <td<?php echo $rowspan; ?>><?php echo $rs->fields("idbuyer") ?></td>
							 <td<?php echo $rowspan; ?>><?php if($rs->fields("company")){ ?><b><?php echo $rs->fields("company") ?></b> /<?php } else {?> <?php } ?><br><?php echo $rs->fields("lastname") ?></td>
                         <!--  <td<?php echo $rowspan; ?>><?php if($rs->fields("company")){ ?><?php echo $rs->fields("company") ?> /<?php } else {?><p>"rien" /</p><?php } ?><br><?php echo $rs->fields("lastname") ?></td>-->
                              <?php      
							  }
							  ?>
								<td>
								<?php
									$idosD = $rs->fields( "idorder_supplier" );
									if( $rs->fields( "os_dispatch_date" ) == "0000-00-00" && $bl_dispatch_date[ $i ] == "0000-00-00" )
											$dispatch_date = "";
									else if( $bl_dispatch_date[ $i ] != "0000-00-00" )
											$dispatch_date = usDate2eu( $bl_dispatch_date[ $i ] );
									else 	$dispatch_date = usDate2eu( $rs->fields("os_dispatch_date") );
									?>
									<input size="10" onkeyup="if( !this.value.length  || this.value.length == 10 ) SetDate( this );" type="text" name="dispatch_date_<?php echo $idosD ?>" id="dispatch_date_<?php echo $idosD ?>" value="<?php echo $dispatch_date ?>" class="calendarInput" />
									<?php DHTMLCalendar::calendar( "dispatch_date_$idosD", true, "SetDate" );
						
								?>
								</td>
								<?php
								
									if( !$i ){
										
										?>
										<td<?php echo $rowspan; ?> style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ) ?></td>
										<td<?php echo $rowspan; ?> style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
										<?php
						
									}
								
								?>
								<!--
								<td<?php //echo $rowspan; ?> style="text-align:right; white-space:nowrap;"><?php echo $rebate_type == 'rate' ? Util::rateFormat( $rebate_value ) : Util::priceFormat( $rebate_value ) ?></td>
								<td<?php //echo $rowspan; ?> style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_net_amount ) ?></td>
								-->
								<td><input type="radio" name="supplier_<?php echo $rs->fields("idsupplier") ?>[]" id="supplier_<?php echo $rs->fields("idsupplier") ?>" value="<?php echo $idbl_delivery[ $i ] ?>" onclick="$('input[type=\'checkbox\']').attr('checked',false);" /></td>
								<td class="righterCol"><input type="checkbox" onclick="$('input[type=\'radio\']').attr('checked',false);" name="group_<?php echo $rs->fields("idsupplier") ?>[]" id="group_<?php echo $rs->fields("idsupplier") ?>" value="<?php echo $idbl_delivery[ $i ] ?>" /></td>
							</tr>
							<?php
							
							$i++;
							
						}

						$last_idsupplier = $rs->fields( "idsupplier" );
						
						$totalET 	+= $rs->fields( "total_amount_ht" );
						$totalReb	+= $rebate_amount;
						$totalATI 	+= $rs->fields( "total_amount" );
						$totalNet	+= $total_net_amount;
						
	        			$rs->MoveNext();
		   	
						//fin fournisseur
						
						if( $rs->EOF() || $last_idsupplier != $rs->fields( "idsupplier" ) ){
					
									?>
							 		<tr>
								 		<th colspan="5" style="border-style:none;"></th>
								 		<th class="totalAmount"><?php echo Util::priceFormat( $totalET ) ?></th>
								 		<th class="totalAmount"><?php echo Util::priceFormat( $totalATI ) ?></th>
								 		<th colspan="4" style="border-style:none;"></th>
							 		</tr>
							 		</tbody>
						 		</table>
					 		</div>
				            <div class="submitButtonContainer">
				            	<input type="button" value="Facturer partiellement" onclick="registerPartialInvoices(<?="'".$last_idsupplier ."'";?>);" class="blueButton" />
				            	<input type="button" value="Enregistrer la facture" onclick="registerInvoice(<?="'" . $last_idsupplier. "'";?>);" class="blueButton" />
						    </div>
							<?php
							
						}
						
	        		}
	
					paginate();
						
				?>
			</div>
		</div>

			<!-- ***************************** PROFORMA FOURNISSEUR **************************** -->
					<div class="content" id="proforma">
					<?php
							
						$rsPro = DBUtil::query("SELECT bs.billing_supplier, bs.Date, bs.status, bs.total_amount, s.name, bs.idsupplier ,GROUP_CONCAT( bsr.idbl_delivery ) AS idbl_delivery
												FROM supplier s, billing_supplier bs 
												LEFT JOIN billing_proforma bp ON bs.billing_supplier = bp.proforma_id
												LEFT JOIN billing_supplier_row bsr ON ( bsr.billing_supplier = bs.billing_supplier AND bsr.idsupplier = bs.idsupplier )
												LEFT JOIN bl_delivery_row blr ON( blr.idbl_delivery = bsr.idbl_delivery AND blr.idrow = bsr.idrow )
												LEFT JOIN bl_delivery ON bl_delivery.idbl_delivery = blr.idbl_delivery
												WHERE bp.proforma_id IS NULL
												AND $SQL_ConditionAvoir
												AND bs.idsupplier = s.idsupplier
												AND bs.proforma =1
												GROUP BY bs.billing_supplier, bs.idsupplier");
		
						$comptePro = $rsPro->RecordCount();
						$sumSoldes = 0;
						$sumAmounts = 0;
						
						if( $comptePro > 0 && $rsPro->fields("billing_supplier") ){
							$arrayBySupplier = array();
							
							while( !$rsPro->EOF() ){
								$idsupplier = $rsPro->fields("idsupplier");
								$billing_supplier = $rsPro->fields("billing_supplier");
								
								$arrayBySupplier[ $idsupplier ][ "name" ] = $rsPro->fields("name");
								$arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "Date" ] = $rsPro->fields( "Date" );
				                $arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "status" ] = $rsPro->fields( "status" );
				                $arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "total_amount" ] = $rsPro->fields( "total_amount" );
				                $arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "bl" ] = $rsPro->fields( "idbl_delivery" );
				           		$arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "solde" ] = SupplierInvoice::getBalanceOutstanding( $billing_supplier , $idsupplier );
								$sumAmounts += $arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "total_amount" ];
				           		$sumSoldes += $arrayBySupplier[ $idsupplier ][ "billings" ][ $billing_supplier ][ "solde" ];
								
								$rsPro->MoveNext();
							}
							
							?>
					<div class="headTitle">
	                    <p class="title">Résultat de la recherche: <?php echo $rsPro->RecordCount() ?> facture(s) proforma fournisseur reçues <?php echo Util::priceFormat( $sumAmounts ); ?>, reste à payer : <?php echo Util::priceFormat( $sumSoldes );?></p>
	                </div>
	                <div class="subContent">
	                	<?php foreach( $arrayBySupplier as $idsupplier => $supplierValues ){?>
	                    <div class="subTitleContainer">
	                    	<p class="subTitle"><?php echo $supplierValues[ 'name' ] ?></p>
	                    </div>
				        <div class="resultTableContainer clear">
				        	<table class="dataTable resultTable" >
				            	<thead>
				                    <tr>
				                    	<th>N° Proforma</th>
				                        <th>Date proforma</th>
				                        <th>Total TTC</th>
				                        <th>Solde à payer</th>
				                        <th>Enregistrer</th>
				                        <th>Regrouper</th>
				                    </tr>
				              	</thead>
				              	<tbody>
					            <?
				                foreach( $supplierValues[ 'billings' ] as $billing_supplier => $billingValues ){ ?>
				                	<?							
				                			
				                			$Date = $billingValues[ 'Date' ];
				                			$status = $billingValues[ 'status' ];
				                			$total_amount = $billingValues[ 'total_amount' ];
				           					$solde = $billingValues[ 'solde' ];
				                	?>
				              		<tr>
				              			<td class="lefterCol"><?=$billing_supplier?></td>
				              			<td><?php echo Util::dateFormatEu( $Date ) ?></td>
				              			<td><?php echo Util::priceFormat( $total_amount ) ?></td>
				              			<td><?php echo Util::priceFormat( $solde ) ?></td>
				              			<td><!--
				              			<input type="hidden" value="<?=$billingValues[ 'bl' ]?>" name="supplierProT_<?php echo $idsupplier ?>[]" id="supplierProT_<?php echo $idsupplier ?>">
				              			-->
				              			<input type="radio" name="supplierPro_<?php echo $idsupplier ?>[]" id="supplierPro_<?php echo $idsupplier ?>" value="<?php echo $billing_supplier ?>" onclick="$('input[type=\'checkbox\']').attr('checked',false);" /></td>
				              			<td><input type="checkbox" onclick="$('input[type=\'radio\']').attr('checked',false);" name="groupPro_<?php echo $idsupplier ?>[]" id="groupPro_<?php echo $idsupplier ?>" value="<?php echo $billing_supplier ?>" /></td>
				              			<!--
				              			<td class="righterCol"><a href="#" onclick="proformaToInvoice('<?=urlencode($billing_supplier)?>','<?=$idsupplier?>')">Enregistrer la facture définitive</a></td>
				              			-->
	                				</tr>
	                				<?
			                	}
				                ?>
				              	</tbody>
				             </table>      
				    	</div>
			            <div class="submitButtonContainer">
			            	<!--<input type="button" value="Facturer partiellement" onclick="registerPartialInvoicesProforma(<?=$idsupplier;?>);" class="blueButton" />-->
			            	<input type="button" value="Enregistrer la facture" onclick="proformaToInvoice(<?php echo "'". $idsupplier . "'" ?>);" class="blueButton" />
					    </div>
				    	<div class="clear"></div>
				    	<?php } ?>
	                </div>
					<?
				}else{
					?>
			        <div class="headTitle">
	                    <p class="title">Résultat de la recherche</p>
	                </div>
	                <div class="subContent">
	                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucune facture proforma fournisseur reçue trouvée</span>
	                </div>
					<?
				}
			?>
		
			</div>		
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div> <!-- mainContent fullWidthContent -->
        <script type="text/javascript">
		$(document).ready(function() {
			document.getElementById('blA').innerHTML = 'Cdes à facturer (<?php echo $resultCount ?>)';

			document.getElementById('proformaA').innerHTML = 'Factures proforma reçues (<?php echo $comptePro ?>)';
			//Handler of tooltip By behzad
	        $(".fournisseurHelp").each( function(){
		        $(this).qtip({
		            adjust: {
		                mouse: true,
		                screen: true
		            },
		            content: { url: '<?php echo HTTP_HOST; ?>/accounting/received_supplier_invoices.php?showTooltip&idBlDelivery=' + $(this).attr( 'rel' ) },
		            position: {
		                corner: {
		                    target: "rightMiddle",
		                    tooltip: "leftMiddle"
		                }
		            },
		            style: {
		                background: "#FFFFFF",
		                border: {
		                    color: "#EB6A0A",
		                    radius: 5
		                },
		                fontFamily: "Arial,Helvetica,sans-serif",
		                fontSize: "13px",
		                tip: true,
		                width: 600
		            }
	        	});
	        	
	        });
			//end of Tooltip

		});
	
		$(function() {
			
				$('#zizi').tabs({ fxFade: true, fxSpeed: 'fast' });
			
		});
		

		
	</script>
	</form>
	<?php
	
		if( isset( $_REQUEST[ "proposal" ] ) && !$_REQUEST[ "proposal" ] ){
			
			?>
			<div class="mainContent fullWidthContent">
				<div class="topRight"></div>
				<div class="topLeft"></div>
				<div class="content">
					<div class="headTitle">
						<p class="title">
							<a class="blueLink" href="#" onclick="$('#proposal').val('1'); $('#searchForm').ajaxSubmit( { beforeSubmit:	preSubmitCallback, success: postSubmitCallback } ); return false;">Afficher les suggestions</a>
						</p> &nbsp;&nbsp;
					</div>
				</div>
				<div class="bottomRight"></div>
				<div class="bottomLeft"></div>
			</div>
			<?php
	
		}
		
	return $supplierOrders;
	
}

//-------------------------------------------------------------------------------------

function searchMore( array $supplierOrders ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH,$use_help;

	if( isset( $_POST[ "idorder" ] ) )				$idorder = $_POST[ "idorder" ];						else $idorder = "";
	if( isset( $_POST[ "n_order_supplier" ] ) )		$n_order_supplier = $_POST[ "n_order_supplier" ];	else $n_order_supplier = "";
	if( isset( $_POST[ "idbl_delivery" ] ) )		$idbl_delivery = $_POST[ "idbl_delivery" ];			else $idbl_delivery = "";
	if( isset( $_POST[ "idsupplier" ] ) )			$idsupplier = $_POST[ "idsupplier" ];				else $idsupplier = "";
	if( isset( $_POST[ "idbuyer" ] ) )				$idbuyer = $_POST[ "idbuyer" ];						else $idbuyer = "";
	if( isset( $_POST[ "idorder_supplier" ] ) )		$idorder_supplier = $_POST[ "idorder_supplier" ];	else $idorder_supplier = "";
	if( isset( $_POST[ "lastname" ] ) )				$lastname = $_POST[ "lastname" ];					else $lastname = "";
	if( isset( $_POST[ "status" ] ) )				$status = $_POST[ "status" ];						else $status = "";
	if( isset( $_POST[ "minus_date_select" ] ) )	$minus_date_select = $_POST[ "minus_date_select" ];	else $minus_date_select = "-365";
	if( isset( $_POST[ "company" ] ) )				$company = $_POST[ "company" ];						else $company = "";

	$now = getdate();
	$interval_select = isset( $_POST[ "interval_select" ] ) ? intval( $_POST[ "interval_select" ] ) : 1;
	$minus_date_select = $_POST[ "minus_date_select" ];
	
	switch( $interval_select ){
		
		case 1:
			$Min_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $now[ "mon" ], $now[ "mday" ] + $minus_date_select, $now[ "year" ] ) );
			$Now_Time = date( "Y-m-d 23:59:59" );
			
			$SQL_Condition = "order_supplier.`" . $_POST[ "date_type" ] . "` >= '$Min_Time' AND order_supplier.`" . $_POST[ "date_type" ] . "` <= '$Now_Time'";
			
			break;
			
		case 2:
		    $yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now[ "year" ]--;
			
			$Min_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $yearly_month_select, 1, $now[ "year" ] ) );
			$Max_Time = date( "Y-m-d 00:00:00", mktime( $now[ "hours" ], $now[ "minutes" ], $now[ "seconds" ], $yearly_month_select + 1, 1, $now[ "year" ] ) );
			
			$SQL_Condition = "order_supplier.`" . $_POST[ "date_type" ] . "` >= '$Min_Time' AND order_supplier.`" . $_POST[ "date_type" ] . "` <= '$Max_Time' ";
			
			break;
			
		case 3:
			
			$Min_Time = Util::dateFormatEu( $_POST[ "bt_start" ] );
		    $Max_Time = Util::dateFormatEu( $_POST[ "bt_end" ] ) . " 23:59:59";
			
			$SQL_Condition = "order_supplier.`" . $_POST[ "date_type" ] . "` >= '$Min_Time' AND order_supplier.`" . $_POST[ "date_type" ] . "` <= '$Max_Time' ";
			
			break;
			
	     case 4:
			
			//min
		    $delivery_start = $_POST[ "delivery_start" ];
		    $delivery_end = $_POST[ "delivery_end" ];
			
			$SQL_Condition = "bl_delivery.dispatch_date >= '$Min_Time' AND bl_delivery.dispatch_date <= '$Max_Time' ";
        	
			break;
			
	}
	
	if( !empty( $idorder ) ){
		
		$idorder = FProcessString( $idorder );
		$SQL_Condition .= " AND order_supplier.idorder IN( $idorder )";
	
	}
	
	if( !empty( $idbuyer ) ){
		
		$idbuyer = FProcessString( $idbuyer );
		$SQL_Condition .= " AND order_supplier.idbuyer IN( $idbuyer )";
	
	}
	
	if( !empty( $idsupplier ) ){
		
		$idsupplier = FProcessString( $idsupplier );
		$SQL_Condition .= " AND bl_delivery.idsupplier IN( $idsupplier )";
	
	}

	if( !empty( $idorder_supplier ) ){
		
		$idorder = FProcessString( $idorder_supplier );
		$SQL_Condition .= " AND order_supplier.idorder_supplier IN( $idorder_supplier )";
	
	}
	
	if( !empty( $n_order_supplier) ){
		
		$n_order_supplier = FProcessString( $n_order_supplier );
		$SQL_Condition .= " AND order_supplier.n_order_supplier IN( $n_order_supplier )";
	
	}
	
	if( !empty( $idbl_delivery ) ){
		
		$idbl_delivery = FProcessString( $idbl_delivery );
		$SQL_Condition .= " AND bl_delivery.idbl_delivery IN( $idbl_delivery )";
	
	}
	
	if( isset($_POST[ "status" ]) && strlen( $_POST[ "status" ] ) ){
		
		$status = FProcessString( $status );
		$SQL_Condition .= " AND order_supplier.status LIKE '" . Util::html_escape( stripslashes( $_POST[ "status" ] ) ) . "'";
	
	}
	
	if( !empty( $company ) ){
		
		$SQL_Condition .= " AND buyer.company LIKE '%$company%'";
	
	}
	
	$lang = User::getInstance()->getLang();
	
	/*$SQL_Condition	.= "
	AND order_supplier.idsupplier=supplier.idsupplier 
	AND order_supplier.status = order_supplier_status.order_supplier_status
	AND order_supplier.idbuyer = buyer.idbuyer
	AND order_supplier.iduser = user.iduser
	AND order_supplier.idorder_supplier = bl_delivery.idorder_supplier
	AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier";*/
	
	if( count( $supplierOrders ) )
		$SQL_Condition	.= " AND order_supplier.idorder_supplier NOT IN ( " . implode( ",", $supplierOrders ) . " )";
	
	//Droits commercial
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	if( !$hasAdminPrivileges ){
		
		$iduser = User::getInstance()->getId();
		$SQL_Condition .= " AND ( buyer.iduser = '$iduser' OR order_supplier.iduser = '$iduser' )";

	} 
	
	if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sortby" ] ) )
		$orderby = " ORDER BY " . $_GET[ "sortby" ] . " " . $_GET[ "sens" ];
	else
		$orderby = " ORDER BY name ASC";

	//Nombre de pages
	
	/*$query = "
	SELECT COUNT(*) AS resultCount 
	FROM bl_delivery, supplier, order_supplier_status, buyer, user, order_supplier
	LEFT JOIN `order` ON `order`.idorder = order_supplier.idorder 
	WHERE $SQL_Condition";	*/
	
	$query = "
	SELECT COUNT(*) AS resultCount 
	FROM user, supplier, buyer, order_supplier, bl_delivery
	LEFT JOIN billing_supplier_row ON ( billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery AND billing_supplier_row.idsupplier = bl_delivery.idsupplier )
	WHERE $SQL_Condition
	AND billing_supplier_row.idbl_delivery IS NULL
	AND bl_delivery.iduser = user.iduser
	AND bl_delivery.idbuyer = buyer.idbuyer
	AND bl_delivery.idsupplier = supplier.idsupplier
	AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier
	AND order_supplier.idsupplier <> '$internal_supplier'
	-- AND order_supplier.total_amount_ht > 0.0";
	
	$rs = DBUtil::query( $query );
	
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );
	
	if( $resultCount){
				
		/*$query = "
		SELECT order_supplier.idorder_supplier, 
			order_supplier.idorder,
			order_supplier.n_order_supplier, 
			order_supplier_status.order_supplier_status, 
			order_supplier_status.order_supplier_status$lang, 
			order_supplier.DateHeure, 
			order_supplier.total_amount_ht,  
			supplier.name, 
			order_supplier.seen, 
			order_supplier.idbuyer, 
			order_supplier.iddelivery, 
			user.initial,
			order_supplier.idsupplier,
			bl_delivery.availability_date,
			bl_delivery.dispatch_date,
			bl_delivery.idbl_delivery,
			`order`.status
		FROM bl_delivery, supplier, order_supplier_status, buyer, user, order_supplier
		LEFT JOIN `order` ON `order`.idorder = order_supplier.idorder 
		WHERE $SQL_Condition 
		$orderby";*/

		$query = "
		SELECT bl_delivery.idbl_delivery,
			bl_delivery.idorder_supplier,  
			bl_delivery.idsupplier,
			bl_delivery.dispatch_date AS bl_dispatch_date,
			bl_delivery.iddelivery,
			bl_delivery.availability_date,
			bl_delivery.dispatch_date,
			( bl_delivery.idbilling_buyer = 0 AND bl_delivery.date_send = '0000-00-00' AND date_delivery = '0000-00-00' ) AS deliveryNoteEditable,
			bl_delivery.idorder,
			supplier.name,
			order_supplier.dispatch_date AS os_dispatch_date,
			order_supplier.status,
			order_supplier.idbuyer,
			order_supplier.DateHeure, 
			order_supplier.n_order_supplier,
			user.initial,
			order_supplier.total_amount_ht,
			order_supplier.total_amount
		FROM user, supplier, buyer, order_supplier, bl_delivery
		LEFT JOIN billing_supplier_row ON ( billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery AND billing_supplier_row.idsupplier = bl_delivery.idsupplier )
		WHERE $SQL_Condition
		AND billing_supplier_row.idbl_delivery IS NULL
		AND bl_delivery.iduser = user.iduser
		AND bl_delivery.idbuyer = buyer.idbuyer
		AND bl_delivery.idsupplier = supplier.idsupplier
		AND bl_delivery.idorder_supplier = order_supplier.idorder_supplier
		AND order_supplier.idsupplier <> '$internal_supplier'
		-- AND order_supplier.total_amount_ht > 0.0
		ORDER BY supplier.name ASC, bl_delivery.idorder_supplier ASC";
	
		$rs = DBUtil::query( $query );
		
		global $GLOBAL_START_PATH, $GLOBAL_DB_PASS;
			
		include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
			
		$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
		
		if( $rs->RecordCount() > 0 ){
			
			?>
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function updateRemoteNumber( idorder_supplier, n_order_supplier ){
				
					var data = "idorder_supplier=" + idorder_supplier + "&n_order_supplier=" + n_order_supplier;
					
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL; ?>/sales_force/supplier_order_search.php?update=n_order_supplier",
						async: true,
						type: "POST",
						data: data,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ 
						
							alert( "Impossible de sauvegarder les modifications" ); 
							
						},
					 	success: function( responseText ){
			
							if( responseText != "1" ){
								
								alert( "Impossible de sauvegarder les modifications : " + responseText );
								return;
								
							}
							
							$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
							$.blockUI.defaults.css.fontSize = '12px';
				        	$.growlUI( '', 'Modifications enregistrées' );
				        	
		        					
						}

					});
		
				}

				function SetDate( element ){

					var idorders = element.id.substring( element.id.lastIndexOf( '_', element.id.length ) + 1, element.id.length );
					
					var availibility = $( "#availability_date_" + idorders ).attr( "value" );
					var dispatch = $( "#dispatch_date_" + idorders ).attr( "value" );
					
					var postData = "action=update&idos=" + idorders + "&av=" + availibility + "&disp=" + dispatch
					$.ajax({

						url: "<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order_search.php",
						async: true,
						cache: false,
						type: "GET",
						data: postData,
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible modifier la date" ); },
					 	success: function( responseText ){

							if( responseText == 'wrong disp date format' )
								alert( "Format de date d'expédition incorrect !" );
							else if( responseText == 'wrong av date format' )
								alert( "Format de date de mise à disposition incorrect !" );
			        		else if( responseText.length ){
			        		
	
			        			$( "#cellstatus_" + idorders ).html( responseText );
			        			$( "#cellstatus_" + idorders ).attr( "class", "" );
			        			$( "#celldelete_" + idorders ).html( "" );
			        			
					        	$('#searchForm').ajaxSubmit( { 
					
									beforeSubmit:  preSubmitCallback2,  // pre-submit callback 
									success:	   postSubmitCallback  // post-submit callback
									
								} ); 
			        			
			        			
			        		}	
								
						}
		
					});
					
					
				}
				
				function preSubmitCallback2( formData, jqForm, options ){
					
					$.blockUI({
			
						message: "Modifications enregistrées <br/> Mise à jour de la recherche",
						css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
						fadeIn: 0, 
						fadeOut: 700
							
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
				
				function visibility(thingId, txtAff, txtMasque) {
					var targetElement; 
					var targetElementLink;
					targetElement = document.getElementById(thingId);
					targetElementLink = document.getElementById(thingId+'Link');
					if (targetElement.style.display == "none") {
						targetElement.style.display = "";
						targetElementLink.innerHTML = txtMasque;
					} else {
						targetElement.style.display = "none";
						targetElementLink.innerHTML = txtAff;
					}
				}

			/* ]]> */
			</script>
					<div class="mainContent fullWidthContent">
						<div class="topRight"></div><div class="topLeft"></div>
						<div class="content">
							<div class="headTitle">
								<p class="title">Suggestions : <?php echo $rs->RecordCount() ?> commande(s) fournisseur</p> &nbsp;&nbsp;
			            		<a href="#" id="suggestDivLink" onclick="visibility('suggestDiv','[Afficher]','[Masquer]');return false;" class="blueLink">
	            					[Masquer]
	            				</a>
							</div>
							<div class="subContent" id="suggestDiv" style="display:block;">
								<div class="tableHeaderLeft"></div>
								<div class="tableHeaderRight"></div>
								<!-- tableau résultats recherche -->
								<div class="resultTableContainer clear">
									<table class="dataTable resultTable">
										<thead>
											<tr>
												<th style="width:4%;">Com</th>
												<th style="width:4%;">Suppr.</th>
												<th style="width:8%;">Commande fournisseur n°</th>
												<th style="width:7%;">Commande fournisseur interne n°</th>
												<th style="width:7%;">Commande client n°</th>
												<th style="width:22%;">Fournisseur</th>
												<th style="width:8%;">Statut</th>
												<th style="width:8%;">Date commande</th>
												<th style="width:8%;">Total HT</th>
												<th style="width:11%;">Date mise à disposition</th>
												<th style="width:11%;">Date expédition<br />Enlèvement prévu</th>
											</tr>
										</thead>
										<tbody>
<?php
			
			//Récupérer les traductions des status
			$strstatus = array();
			$db = &DBUtil::getConnection();
			
			$query = "SELECT order_supplier_status, order_supplier_status_1 FROM order_supplier_status";
			$rs2 = $db->Execute( $query );
			
			if( $rs2 === false )
				die( Dictionnary::translate( "gest_com_impossible_recover_status_list" ) );
			
			while( !$rs2->EOF() ){
				
				$strstatus[ $rs2->fields( "order_supplier_status" ) ] = $rs2->fields( "order_supplier_status_1" );
				
				$rs2->MoveNext();
				
			}
			
			for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
				
				$idorder			= $rs->Fields( "idorder" );
				$idorder_supplier	= $rs->Fields( "idorder_supplier" );
				$n_order_supplier	= $rs->Fields( "n_order_supplier" );
				$DateHeure			= $rs->Fields( "DateHeure" );
				$status				= $strstatus[ $rs->Fields( "status" ) ];
				$total_amount_ht	= $rs->Fields( "total_amount_ht" );
				$suppliername		= $rs->fields( "name" );
				$idbuyer			= $rs->fields( "idbuyer" );
				$initials			= $rs->fields( "initial" );
				$Iddelivery			= $rs->fields( "iddelivery" );
				$status_order		= $rs->fields( "status" );
				$oss				= $rs->Fields( "status" );
				$ids				= $rs->Fields( "idsupplier" );
				$availability_date	= $rs->fields( "availability_date" );
				
				if( $availability_date == "0000-00-00" )
					$availability_date = "";
				
				$dispatch_date = $rs->fields( "dispatch_date" );
				
				if( $dispatch_date == "0000-00-00" )
					$dispatch_date = "";
				
				if( $oss == "standby" )
					$class = " class=\"orangeText\"";
				else
					$class= "";
				
				$bls = SupplierOrder::getBLs( $idorder_supplier );
				$blCount = count( $bls );
				
				?>
				<tr class="blackText">
					<td class="lefterCol"><?php echo $initials ?></td>    
					<td id="celldelete_<?php echo $idorder_supplier ?>"><?php if( $oss == "standby" || $oss == "received" ){ ?><input type="image" name="DeleteOrder" id="DeleteOrder" src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/corbeille.jpg
					" alt="Supprimer cette commande fournisseur" onclick="return confirmDeleteOrder( <?php echo "'".$idorder_supplier."'" ?>);" /><?php } ?></td>
					<td valign="middle" style="white-space: nowrap;">
						<div style="margin-left:auto; margin-right:auto; text-align:center; width:55px;">
							<?php echo GenerateHTMLForToolTipBox( $idorder_supplier, $suppliername ) ?>
							<a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier ?>" onclick="window.open(this.href); return false;"><?php echo $idorder_supplier ?></a>
						</div>
					</td>
					<td style="white-space:nowrap;">
						<input id="n_order_supplier_<?php echo $idorder_supplier; ?>" type="text" class="textInput" style="width:70px;" value="<?php echo htmlentities( $rs->fields( "n_order_supplier" ) ); ?>" onkeyup="" />
						<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/button_save.png" style="cursor:pointer;" onclick="updateRemoteNumber(<?php echo $idorder_supplier; ?>, $('#n_order_supplier_<?php echo $idorder_supplier; ?>').val() );" />
					</td>
					<td>
						<?php if( $idorder ){ ?>
							<?php echo DrawOrderToolTipBox( $idorder_supplier ) ?>
							<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $idorder ?>" onclick="window.open(this.href); return false;"><?php echo $idorder ?></a>
						<?php } ?>
					</td>
					<td>
						<?php echo GenerateHTMLForToolTipBoxTel( $idorder_supplier ) ?>
						<?php if( $use_help ){ ?><?php echo $suppliername ?><?php }else{ ?><a class="grasBack" href="/product_management/enterprise/supplier.php?idsupplier=<?php echo $ids ?>" onclick="window.open(this.href); return false;"><?php echo $suppliername ?></a><?php } ?></td>
					</td>
					<td id="cellstatus_<?php echo $idorder_supplier ?>"<?php echo $class ?>><?php echo $status ?></td>
					<td style="white-space:nowrap;"><?php echo Util::dateFormatEu( substr( $DateHeure, 0, 10 ) ) ?></td>	
					<td style="text-align:right; white-space:nowrap;">
						<?php echo Util::priceFormat( $total_amount_ht ) ?><br />
						<?php echo getOrderSupplierTotalWeight( $idorder_supplier ) ?> kg
					</td>
					<td style="white-space:nowrap;">
						<?php if( $blCount > 1 ){ ?>
							<?php echo Dictionnary::translate( "gest_com_partial_deliveries" ) ?>
						<?php }else{ ?>
							<input size="10" onkeyup="if( !this.value.length || this.value.length == 10 ) SetDate( this );" type="text" name="availability_date_<?php echo $idorder_supplier ?>" id="availability_date_<?php echo $idorder_supplier ?>" value="<?php echo Util::dateFormatEu( $availability_date ) ?>" class="calendarInput" />
							<?php echo DHTMLCalendar::calendar( "availability_date_$idorder_supplier", true, "SetDate" ) ?>
						<?php } ?>
					</td>
					<td class="righterCol" style="white-space:nowrap;">
						<?php if( $blCount > 1 ){ ?>
							<?php echo Dictionnary::translate( "gest_com_partial_deliveries" ) ?>
						<?php }else{ ?>
							<input size="10" onkeyup="if( !this.value.length  || this.value.length == 10 ) SetDate( this );" type="text" name="dispatch_date_<?php echo $idorder_supplier ?>" id="dispatch_date_<?php echo $idorder_supplier ?>" value="<?php echo Util::dateFormatEu( $dispatch_date ) ?>" class="calendarInput" />
							<?php echo DHTMLCalendar::calendar( "dispatch_date_$idorder_supplier", true, "SetDate" ) ?>
						<?php } ?>
					</td>
				</tr>
				<?php
				
				$rs->MoveNext();
				
			}
			
			?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="bottomRight"></div><div class="bottomLeft"></div>
			</div>
			<?php
			
		}
	}
}

//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$query = "SELECT name FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
	$rs = DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function GenerateHTMLForToolTipBox( $idorder_supplier, $suppliername ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT b.idbuyer, b.company 
	FROM buyer b, order_supplier os 
	WHERE os.idbuyer = b.idbuyer 
	AND os.idorder_supplier = '$idorder_supplier' 
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom de la société pour la commande '$idorder_supplier'" );
		
	$company = $rs->fields( "company" );
	$idbuyer = $rs->fields( "idbuyer" );
	
	//TODO : jointure entre order_supplier_row et order_row approximative
	
	$query = "
	SELECT DISTINCT( osr.idrow ), osr.reference,osr.ref_supplier, osr.quantity, osr.discount_price, osr.summary, orow.discount_price AS pv, orow.quantity AS qte 
	FROM order_supplier_row osr, order_row orow, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND osr.idorder_supplier = os.idorder_supplier
	AND orow.idorder = os.idorder
	AND orow.reference = osr.reference";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les infos des lignes de commande" );
	
	$html = "<span onmouseover=\"document.getElementById('pop$idorder_supplier').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idorder_supplier').className='tooltip';\" style=\"position:relative;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/help.png\" alt=\"voir le détail\" class=\"tooltipa VCenteredWithText\" />";
	
	$html .= "<span id='pop$idorder_supplier' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
	<div class=\"headTitle\">
		<p class=\"title\">Client n° $idbuyer - $company</p>
		<div class=\"rightContainer\"></div>
	</div>
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	if( $rs->RecordCount() ){
		
		$html .= "<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
			<thead>
			<tr>
				<th>".Dictionnary::translate('reference')."</th>
				<th>".Dictionnary::translate('gest_com_ref_supplier')."</th>
				<th>".Dictionnary::translate('product')."</th>
				<th>".Dictionnary::translate('supplier')."</td>
				<th>".Dictionnary::translate('gest_com_quantity')."</th>
				<th>".Dictionnary::translate('gest_com_PHAUHT')."</th>
				<th>".Dictionnary::translate('gest_com_buying_MTHAHT')."</th>
				<th>".Dictionnary::translate('gest_com_MTVTHT')."</th>
				<th>".Dictionnary::translate('gest_com_rough_strock')."</th>
				<th>".Dictionnary::translate('gest_com_rough_strock')." %</th>
			</tr>
			</thead>
			<tbody>\n";
		
		while( !$rs->EOF() ){
			
			$TitrePdt = $rs->fields('summary');
			$MTHA = $rs->fields('discount_price')*$rs->fields('quantity');
			$MTVT = $rs->fields('pv')*$rs->fields('qte');
			$MB = $MTVT-$MTHA;
			if($MTHA>0 AND $MTVT>0){
				$M=(1-($MTHA/$MTVT))*100;
			}else{
				$M=0;
			}
		
			$html.= "
			<tr>
				<td class=\"lefterCol\">" . $rs->fields('reference') . "</td>
				<td>" . $rs->fields('ref_supplier') . "</td>
				<td style=\"white-space:nowrap;\">$TitrePdt</td>
				<td style=\"white-space:nowrap;\">$suppliername</td>
				<td>" . $rs->fields('quantity') . "</td>
				<td>" . Util::numberFormat($rs->fields('discount_price')) . "</td>
				<td>" . Util::numberFormat($MTHA) . "</td>
				<td>" . Util::numberFormat($MTVT) . "</td>
				<td>" . Util::numberFormat($MB) . "</td>
				<td class=\"righterCol\">" . Util::numberFormat($M) . "</td>
			</tr>";
			
			$rs->MoveNext();
			
		}
		
		$html.= "	</tbody>
	</table>\n";
		
	}else{
		
		$html .= "<div style=\"font-size: 12px; white-space:nowrap;\">Cette commande ne comporte aucun article.</div>";
		
	}
	
	$html .= "</div>
</div>
</div>
</span>
</span>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function DrawToolTipBox( $idorder_supplier, $suppliername ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT b.idbuyer, b.company 
	FROM buyer b, order_supplier os 
	WHERE os.idbuyer = b.idbuyer 
	AND os.idorder_supplier = '$idorder_supplier' 
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom de la société pour la commande '$idorder_supplier'" );
		
	$company = $rs->fields( "company" );
	$idbuyer = $rs->fields( "idbuyer" );
	
	//TODO : jointure entre order_supplier_row et order_row approximative
	
	$query = "
	SELECT DISTINCT( osr.idrow ), osr.reference, osr.ref_supplier, osr.quantity, osr.discount_price, osr.summary, orow.discount_price AS pv, orow.quantity AS qte 
	FROM order_supplier_row osr, order_row orow, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND osr.idorder_supplier = os.idorder_supplier
	AND orow.idorder = os.idorder
	AND orow.reference = osr.reference";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les infos des lignes de commande" );
	
	$html="
	<div id='pop$idorder_supplier' class='tooltip'>
	<table border='0' cellspacing='0' align='center'>
			<tr>
			<td colspan=\"10\" style=\"text-align:center;\"><b>" . Dictionnary::translate( "gest_com_search_idbuyer" ) ." ".$idbuyer."</b> - ".$company."</td>
			</tr>
			<tr>
			<td colspan=\"10\">&nbsp;</td>
			</tr>
			<tr>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('reference')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_ref_supplier')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('product')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('supplier')."</td>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_quantity')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_PHAUHT')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_buying_MTHAHT')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_MTVTHT')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_rough_strock')."</th>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('gest_com_rough_strock')." %</th>
			</tr>";
	
	while( !$rs->EOF() ){
		
		$TitrePdt = $rs->fields('summary');
		$MTHA = $rs->fields('discount_price')*$rs->fields('quantity');
		$MTVT = $rs->fields('pv')*$rs->fields('qte');
		$MB = $MTVT-$MTHA;
		if($MTHA>0 AND $MTVT>0){
			$M=(1-($MTHA/$MTVT))*100;
		}else{
			$M=0;
		}
	
		$html.= "
		<tr>
			<td style=\"text-align:center;\">" . $rs->fields('reference') . "</td>
			<td style=\"text-align:center;\">" . $rs->fields('ref_supplier') . "</td>
			<td  style=\"text-align:center;\"  style=\"white-space:nowrap;\">$TitrePdt</td>
			<td  style=\"text-align:center;\"  style=\"white-space:nowrap;\">$suppliername</td>
			<td style=\"text-align:center;\">" . $rs->fields('quantity') . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($rs->fields('discount_price')) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($MTHA) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($MTVT) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($MB) . "</td>
			<td style=\"text-align:center;\">" . Util::numberFormat($M) . "</td>
		</tr>";
		
		$rs->MoveNext();
		
	}
	
	$html.= "</table>
	</div>";
	
	echo $html;
	
}

//--------------------------------------------------------------------------------------------------

function GenerateHTMLForToolTipBoxTel( $idorder_supplier ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT sc.phonenumber 
	FROM supplier s, supplier_contact sc, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le numéro de téléphone du fournisseur" );
		
	$tel = $rs->fields( "phonenumber" );
	
	$html = "<span onmouseover=\"document.getElementById('poptel$idorder_supplier').className='tooltiphover';\" onmouseout=\"document.getElementById('poptel$idorder_supplier').className='tooltip';\" style=\"position:relative;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/help.png\" alt=\"voir le détail\" class=\"tooltipa\" />";
	
	$html .= "<span id='poptel$idorder_supplier' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	
	$html .= "	<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
		<thead>
		<tr>
			<th>".Dictionnary::translate('phone')."</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class=\"lefterCol righterCol\">$tel</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	</span>
	</span>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function DrawToolTipBoxTel( $idorder_supplier ){
	
	global 
			$GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();

	$query = "
	SELECT sc.phonenumber 
	FROM supplier s, supplier_contact sc, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";

	$rs = $db->Execute( $query );
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le numéro de téléphone du fournisseur" );
		
	$tel = $rs->fields( "phonenumber" );
	
	$html="
	<div id='poptel$idorder_supplier' class='tooltip'>
	<table border='0' cellspacing='0' align='center'>
		<tr>
			<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">".Dictionnary::translate('phone')."</th>
		</tr>
		<tr>
			<td style=\"text-align:center;\"  style=\"white-space:nowrap;\">" . $tel . "</td>
		</tr>
	</table>
	</div>
	";
	
	echo $html;
	
}
//--------------------------------------------------------------------------------------------------

function GenerateHTMLForOrderToolTipBox( $idorders ){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT o.status, c.idbuyer, c.lastname, c.firstname, c.phonenumber " .
			"FROM contact c, buyer b,`order` o, order_supplier os " .
			"WHERE os.idorder_supplier=$idorders " .
			"AND o.idorder = os.idorder " .
			"AND o.idbuyer = c.idbuyer " .
			"AND o.idcontact = c.idcontact";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_recover_information_order") );				
	
	$html="<div onMouseOver=\"document.getElementById('popord$idorders').className='tooltipordhover';\" onMouseOut=\"document.getElementById('popord$idorders').className='tooltipord';\" class='tooltiporda'><img class=\"helpicon\" src=\"$GLOBAL_START_URL/images/back_office/content/icon_help.gif\" alt=\"\" border=\"0\" />";
	
	$html.="<div id='popord$idorders' class='tooltipord'><table border='0' cellspacing='0' align='center'><tr><th>&nbsp;".Dictionnary::translate('status')."&nbsp;</th><th>&nbsp;".Dictionnary::translate('idbuyer')."&nbsp;</th><th>&nbsp;".Dictionnary::translate('lastname')."&nbsp;</th><th>&nbsp;".Dictionnary::translate('firstname')."&nbsp;</td><th>&nbsp;".Dictionnary::translate('phonenumber')."&nbsp;</th></tr>";
	
	$idbuyer = $rs->fields('idbuyer');
	$lastname = $rs->fields('lastname');	
	$firstname = $rs->fields('firstname');
	$phonenumber = $rs->fields('phonenumber');
	$status = Dictionnary::translate($rs->fields('status'));
	
	$html.= "<tr><td>";
	$html.= $status;
	$html.= "</td><td>";
	$html.= $idbuyer;
	$html.= "</td><td style=\"white-space:nowrap;\">";
	$html.= $lastname;
	$html.= "</td><td style=\"white-space:nowrap;\">";
	$html.= $firstname;
	$html.= "</td><td>";
	$html.= $phonenumber;
	$html.= "</td></tr>";
	
	
	$html.="</table></div></div>";
	
	return $html;
	
}

//--------------------------------------------------------------------------------------------------

function DrawOrderToolTipBox( $idorders ){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT o.status,c.idbuyer, c.lastname, c.firstname, c.phonenumber " .
			"FROM contact c, buyer b,`order` o, order_supplier os " .
			"WHERE os.idorder_supplier=$idorders " .
			"AND o.idorder = os.idorder " .
			"AND o.idbuyer = c.idbuyer " .
			"AND o.idcontact = c.idcontact";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_recover_information_order") );				
	
	$idbuyer = $rs->fields('idbuyer');
	$lastname = $rs->fields('lastname');	
	$firstname = $rs->fields('firstname');
	$phonenumber = $rs->fields('phonenumber');
	$status = Dictionnary::translate($rs->fields('status'));
	
	$html = "<span onmouseover=\"document.getElementById('popord$idorders').className='tooltiphover';\" onmouseout=\"document.getElementById('popord$idorders').className='tooltip';\" style=\"position:relative;\"><img src=\"$GLOBAL_START_URL/images/back_office/content/help.png\" alt=\"voir le détail\" class=\"tooltipa\" />";
	
	$html .= "<span id='popord$idorders' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">
<div class=\"resultTableContainer\">";
	
	
	$html .= "	<table class=\"dataTable resultTable\" style=\"white-space:nowrap;\">
		<thead>
		<tr>
			<th>Statut</th>
			<th>Client n°</th>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Téléphone</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td class=\"lefterCol\">$status</td>
			<td>$idbuyer</td>
			<td>$lastname</td>
			<td>$firstname</td>
			<td class=\"righterCol\">$phonenumber</td>
		</tr>
		</tbody>
	</table>
	</div>
	</div>
	</div>
	</span>
	</span>";
	
	return $html;
	
}
//--------------------------------------------------------------------------------------------------

function getOrderStatus( $idorder ){
	
	
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT status FROM `order` WHERE idorder = '$idorder' LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le status de la commande '$idorder'" );
	
	return $rs->fields( "status" );
	
}

//--------------------------------------------------------------------------------------------------

function getOrderSupplierTotalWeight( $idorder_supplier ){

	
	
	$db =& DBUtil::getConnection();
	
	$query = "
	SELECT SUM( weight ) AS weight
	FROM order_supplier_row
	WHERE idorder_supplier = '$idorder_supplier'";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le poids total pour la commande fournisseur '$idorder_supplier'<br />$query" );

	if( !$rs->RecordCount() || !$rs->fields( "weight" ) )
		return 0.0;
		
	return $rs->fields( "weight" );
	
}

//--------------------------------------------------------------------------------------------------

function ModifyDate(){
	global $GLOBAL_START_PATH;
	
	
	$idorders = $_GET[ "idos" ];
	$availability_date = usDate2eu( DBUtil::query( "SELECT availability_date FROM bl_delivery WHERE idorder_supplier = " . $idorders )->fields( 'availability_date' ) );
	$dispatch_date = $_GET[ "disp" ];
	
	$Order = new SupplierOrder( $idorders );
	
	$regs = array();
	$pattern = "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/";
	
	if( preg_match( $pattern, $availability_date, $regs ) !== false ){
		
		$day = $regs[ 1 ];
		$month = $regs[ 2 ];
		$year = $regs[ 3 ];
		
		$availability_date = $year."-".$month."-".$day;
		
	}elseif( !empty( $availability_date ) ){
		
		echo "wrong av date format";
		return;
		
	}
	
	$regs = array();
	$pattern = "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/";
	
	if( !empty( $dispatch_date ) && preg_match( $pattern, $dispatch_date, $regs ) !== false ){
		
		$day = $regs[ 1 ];
		$month = $regs[ 2 ];
		$year = $regs[ 3 ];
		
		$dispatch_date = $year."-".$month."-".$day;
		
	}elseif( !empty( $dispatch_date ) ){
		
		echo "wrong disp date format";
		return;
		
	}
	
	//status

	$old_status = $Order->get('status');
	if( !empty( $dispatch_date ) ){
		
		$status = "dispatch";
		
		if( empty( $availability_date ) )
			$availability_date = $dispatch_date;
			
	}
	else if( !empty( $availability_date ) )
		$status = "confirmed";
	else $status = in_array( $old_status, array( "dispatch", "confirmed" ) ) ? "standby" : $old_status;
	
	$Order->set( 'status' , $status );
			
	DBUtil::query( "
		UPDATE bl_delivery 
		SET availability_date= '$availability_date', 
		dispatch_date = '$dispatch_date' 
		WHERE idorder_supplier = '$idorders'
		LIMIT 1"
	);
	
	$Order->set( 'availability_date' , $availability_date );
	$Order->set( 'dispatch_date' , $dispatch_date );
	
	
	switch( $status ){
		
		case "dispatch" :
			echo "Expédiée";
			break;
		case "confirmed" : 	echo "Confirmée"; 	break;
		case "standby" : 	echo "<span class=\"orangeText\">En attente</span>"; 	break;
		
	}
	
	$Order->save();
	
}
 
//--------------------------------------------------------------------------------------------------
function showTooltip($idbl_delivery){
	$query = "SELECT reference,`ref_supplier`,`quantity`,`designation` 
		FROM `bl_delivery_row` 
		WHERE idbl_delivery = '$idbl_delivery' ";
	
	$rs = DBUtil::query( $query );
	$rows ='';
	while( !$rs->EOF() ){
		
		$rows .= "<tr>
			<td>".$rs->fields("reference")."</td>
			<td>".$rs->fields("ref_supplier")."</td>
			<td>".$rs->fields("quantity")."</td>
			<td>".$rs->fields("designation")."</td>
			</tr>";
		$rs->MoveNext();
		
	}
	
	
	$tooltip_content = "
	<strong>Commande fournisseur n°$idbl_delivery</strong>
	<div class='content' style='padding:5px;'>
		<div class='subContent'>
			<div class='resultTableContainer'>
				<table class='dataTable resultTable' >
					<thead>
						<tr>
							<th>Référence</th>
							<th>Réf. fournisseur</th>
							<th>Quantité</th>
							<th width='50%'>Désignation</th>
						</tr>
					</thead>
					<tbody>
							$rows
					</tbody>
				</table>
			</div>
		</div>
	</div>
	";
	
	return $tooltip_content;
}
?>