<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Export TVA
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );

//---------------------------------------------------------------------------------------
// Vérifiaction du login 
//---------------------------------------------------------------------------------------

//connexion
if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) )
	User::getInstance()->login( stripslashes( $_POST[ "Login" ] ), md5( stripslashes( $_POST[ "Password" ] ) ) );
	
if( !User::getInstance()->getId() ){
	
	$banner = "no";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "expired_session" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

if( !isset( $_GET[ "query" ] ) ){

	$banner = "no";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}
	
global $GLOBAL_DB_PASS;
$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "query" ] ), $GLOBAL_DB_PASS );

$rs =& DBUtil::query( $query );

$lang = "_1";

$exportableArray = array();
$headers = array(

        "N° facture",
        "N° client",
        "Date de facturation",
        "Total H.T.",
        "Montant T.V.A.",
        "Total T.T.C.",
        "CP",
        "Pays",
        "Port vente H.T."
);

foreach( $headers as $header ){
        $exportableArray[ $header ] = array();
    }

while( !$rs->EOF() ){

	if( $rs->fields( "idbilling_adress" ) )
    	$zipcode = DBUtil::getDBValue( "zipcode", "billing_adress", "idbilling_adress", $rs->fields( "idbilling_adress" ) );
  	else $zipcode = $rs->fields( "zipcode" );

  	$DateHeure = $rs->fields( "DateHeure" );
  	list( $date, $heure ) = explode( " ", $DateHeure );
    list( $year, $month, $day ) = explode( "-", $date );
	
	$idb = $rs->fields( "idbilling_buyer" );
	
	//requete montants
	$Q = "SELECT bbr.idbilling_buyer, SUM( bbr.discount_price * bbr.quantity ) AS row_ht,
	SUM( bbr.discount_price * bbr.quantity * ( 1.0 + bbr.vat_rate / 100.0 ) ) AS row_ttc,
	bb.total_discount, bb.total_charge_ht, bb.billing_amount, bb.idorder, bb.idbuyer
	FROM billing_buyer bb, billing_buyer_row bbr
	WHERE bb.idbilling_buyer = $idb
	AND bbr.idbilling_buyer = bb.idbilling_buyer
	GROUP BY bbr.idbilling_buyer
	";

	$r = DBUtil::query( $Q );
	if($r->RecordCount()>0){
	$row_ht = $r->fields("row_ht");
	$row_ttc = $r->fields("row_ttc");
	$total_discount = $r->fields("total_discount");
	$billing_amount = $r->fields("billing_amount");
	$total_charge_ht = $r->fields("total_charge_ht");
	$idorder = $r->fields("idorder");
	$idbuyer = $r->fields("idbuyer");    
	
	$total_ht = $row_ht;
	//tva
	//On regarde si il y a une adresse de livraison dans la commande
	$rdeliv = "SELECT iddelivery FROM `order` WHERE idorder=$idorder";
	$rsdeliv =  DBUtil::query($rdeliv);
	
	$iddelivery = $rsdeliv->fields("iddelivery");
	
	if($iddelivery){
		
		$rtva = "SELECT d.idstate, s.bvat FROM delivery d, state s
				WHERE d.iddelivery=$iddelivery
				AND d.idstate = s.idstate";
		$rstva = & DBUtil::query( $rtva );
		
	}else{
		
		$rtva = "SELECT b.idstate, s.bvat FROM buyer b, state s
				WHERE b.idbuyer=$idbuyer
				AND b.idstate = s.idstate";
		$rstva = & DBUtil::query( $rtva );
	
	}
	
	$bvat = $rstva->fields("bvat");
	
	if($bvat){
		
		$total_ttc = $row_ttc;
		$vat_rate = DBUtil::getParameter( "vatdefault" );
	
	}else{
		
		$total_ttc = $row_ht;
		$vat_rate = 0.0;
	
	}
	

	if( $total_discount > 0.0 ) 
		$total_ht = $total_ht * ( 1.0 - $total_discount / 100.0 );
	
	$total_ht+=$total_charge_ht;
	$total_ht-=$billing_amount;
	
	if( $total_discount > 0.0 ) 
		$total_ttc = $total_ttc * ( 1.0 - $total_discount / 100.0 );
		
	$total_ttc+=$total_charge_ht * ( 1.0 + $vat_rate / 100.0 );
	$total_ttc-=$billing_amount * ( 1.0 + $vat_rate / 100.0 );;
	
 	$tva = $total_ttc - $total_ht;

 	$exportableArray[ "N° facture" ][] = $rs->fields( "idbilling_buyer" );
 	$exportableArray[ "N° client" ][] = $rs->fields( "idbuyer" );
 	$exportableArray[ "Date de facturation" ][] = "$day/$month/$year";
 	$exportableArray[ "Total H.T." ][] = str_replace( ".", ",",round($total_ht,2));
    $exportableArray[ "Montant T.V.A." ][] = str_replace( ".", ",", round( $tva, 2 ) );
   	$exportableArray[ "Total T.T.C." ][] = str_replace( ".", ",",round($total_ttc,2));
    $exportableArray[ "CP" ][] = $zipcode;
 	
 	if( $rs->fields( "idbilling_adress" ) )
    	$idstate = DBUtil::getDBValue( "idstate", "billing_adress", "idbilling_adress", $rs->fields( "idbilling_adress" ) );
  	else $idstate = $rs->fields( "idstate" );
  	
  	$state = DBUtil::getDBValue( "name$lang", "state", "idstate", $idstate );
  	
 	$exportableArray[ "Pays" ][] = $state;
   
   	$exportableArray[ "Port vente H.T." ][] = $rs->fields( "total_charge_ht" );
}
   	$rs->MoveNext();

}

exportArray(&$exportableArray);

//-------------------------------------------------------------------------------


function exportArray(&$exportableArray){

        global $GLOBAL_START_PATH;

        include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );

        $cvsExportArray = new CSVExportArray( $exportableArray, "factures" );
        $cvsExportArray->export();

}

//-------------------------------------------------------------------------------


?>
