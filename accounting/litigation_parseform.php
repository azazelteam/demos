<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonctions importantes de la gestion litige clients
 */
 
//-----------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "../objects/Litigation.php" );
/*
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header( "charset=utf-8" );*/

//-----------------------------------------------------------------------------------

if( !isset( $_POST[ "idlitigation" ] ) || !isset( $_POST[ "typeLitigation" ] ) )
	exit( "0" );

$_POST = Util::arrayMapRecursive( "Util::doNothing", Util::arrayMapRecursive( "stripslashes", $_POST ) );

$litigation = new Litigation( $_POST[ "idlitigation" ]  , $_POST[ "typeLitigation" ] );

//-----------------------------------------------------------------------------------
/* mise à jour du litige */

if( isset( $_POST[ "idlitigation_motive" ] ) ){
	
	if( !isset($_POST[ "idlitigation_motive" ]) ){
	
		$query = "
		INSERT IGNORE INTO litigation_motives ( motive, username, lastupdate ) 
		VALUES( '" . Util::html_escape( $_POST[ "idlitigation_motive" ] ) . "', '" . Util::html_escape( User::getInstance()->get( "login" ) ) . "', NOW() )";
			
		DBUtil::query( $query );
		
		$idlitigation_motive = DBUtil::getInsertID();
		
	}
	else $idlitigation_motive = $_POST[ "idlitigation_motive" ] ;
	
	$litigation->setIdLitigationMotive( $idlitigation_motive );

}

if( isset( $_POST[ "wayout_require_goods_delivery" ] ) )
	$litigation->setWayoutRequireGoodsDelivery(  $_POST[ "wayout_require_goods_delivery" ] == 1 );
	
if( isset( $_POST[ "goods_delivery_location" ] ) )
	$litigation->setGoodsDeliveryLocation( stripslashes( $_POST[ "goods_delivery_location" ] ) );

if( isset( $_POST[ "iduser" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	$litigation->setSalesman( new Salesman(  $_POST[ "iduser" ] ) );
	
}

if( isset( $_POST[ "quantity" ] ) )
	$litigation->setQuantity( intval( $_POST[ "quantity" ] ) );

if( isset( $_POST[ "comment" ] ) )
	$litigation->setComment( stripslashes( $_POST[ "comment" ] ) );
	
if( isset( $_POST[ "management_fees" ] ) )
	$litigation->setManagementFees( Util::text2num( stripslashes( $_POST[ "management_fees" ] ) ) );

//-----------------------------------------------------------------------------------
/* issues au litige */

/* effectuer un remboursement */

if( isset( $_POST[ "idpayment" ] ) && isset( $_POST[ "create_repayment" ] )  && isset( $_POST[ "idbuyer" ] ) && $_POST[ "create_repayment" ] && isset( $_POST[ "repaymentAmount" ] ) && floatval( $_POST[ "repaymentAmount" ] ) > 0.0 )
$litigation->createRepayment( Util::text2num( $_POST[ "repaymentAmount" ] ),  $_POST[ "idpayment" ] , $_POST[ "idbuyer" ]   );

/* annuler un remboursement */

if( isset( $_POST[ "drop_repayment" ] ) &&  $_POST[ "drop_repayment" ] )
	$litigation->dropRepayment(  $_POST[ "drop_repayment" ] , isset( $_POST[ "drop_credit" ] )  );
	
/* procéder à un échange */
	
if( isset( $_POST[ "exchange" ] ) )
	$litigation->createExchange();

/* annuler un échange */

if( isset( $_POST[ "drop_exchange" ] ) &&  $_POST[ "drop_exchange" ]  )
	$litigation->dropExchange(  $_POST[ "drop_exchange" ] , isset( $_POST[ "drop_credit" ] ) );

/* créer un avoir */

if( isset( $_POST[ "credit" ] ) )
	$litigation->createCredit();
	
/* supprimer un avoir */

if( isset( $_POST[ "drop_credit" ] ) ){

	include_once( "$GLOBAL_START_PATH/objects/Credit.php" );
		
	Credit::delete(  $_POST[ "drop_credit" ]  );
	
}

//-----------------------------------------------------------------------------------

$litigation->save();

header( "Content-Type: text/xml;charset=utf-8" );

echo $litigation->toXML();

//-----------------------------------------------------------------------------------
	
?>