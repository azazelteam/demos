<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Export règlements clients
 */
 
include_once( "../objects/classes.php" );
include_once( "../objects/Encryptor.php" );

//---------------------------------------------------------------------------------------
// Vérifiaction du login 
//---------------------------------------------------------------------------------------

//connexion
if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) )
	User::getInstance()->login( stripslashes( $_POST[ "Login" ] ), md5( stripslashes( $_POST[ "Password" ] ) ) );
	
if( !User::getInstance()->getId() ){
	
	$banner = "no";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "expired_session" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

if( !isset( $_GET[ "query" ] ) ){
	
	die( "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>" );

}

$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "query" ] ), $GLOBAL_DB_PASS );

$rs =& DBUtil::query( $query );


$exportableArray = array();
$headers = array(

        "Date de facturation",
		"N° facture",        
		"N° client",
        "Raison sociale",
        "Type de paiement",
        "Débit",
        "Total par client",
        "Date d'échéance",
        "Date de paiement"
);

foreach( $headers as $header )
        $exportableArray[ $header ] = array();

$total = 0;
$i=0;

while( !$rs->EOF() ){

	$DateHeure = $rs->fields( "DateHeure" );
  	list( $date, $heure ) = explode( " ", $DateHeure );
    list( $year, $month, $day ) = explode( "-", $date );
	$DateHeure = "$day/$month/$year";
	
	$deliv_payment =  $rs->fields( "deliv_payment" );
	list( $year, $month, $day ) = explode( "-", $deliv_payment );
	if($deliv_payment!='0000-00-00'){
		$deliv_payment = "$day/$month/$year";
	}else{
		$deliv_payment = '';
	}
	
	$payment_date =  $rs->fields( "payment_date" );
	list( $year, $month, $day ) = explode( "-", $payment_date );
	if($payment_date!='0000-00-00'){
		$payment_date = "$day/$month/$year";
	}else{
		$payment_date = '';
	}
	
 	$exportableArray[ "Date de facturation" ][] = $DateHeure;
 	$exportableArray[ "N° facture" ][] = $rs->fields( "idbilling_buyer" );
 	$exportableArray[ "N° client" ][] = $rs->fields( "idbuyer" );
 	$exportableArray[ "Raison sociale" ][] = $rs->fields( "company" );
 	
 	if($rs->fields( "payment" ) != null){//$rs->fields( "payment" ) != null
 		$exportableArray[ "Type de paiement" ][] = $rs->fields( "payment" ) ;
 	}
 	else{
 		$exportableArray[ "Type de paiement" ][] = '' ;
 	} 	
 	 	 	
   	$exportableArray[ "Débit" ][] = str_replace( ".", ",", round( $rs->fields( "amount" ), 2 ) );
    
    $idbuyer = $rs->fields( "idbuyer" );
    $total+=$rs->fields( "amount" );
   
    $rs->MoveNext();
    $idnextbuyer =  $rs->fields( "idbuyer" );
    
    if($idbuyer!=$idnextbuyer){
    	$exportableArray[ "Total par client" ][] = str_replace( ".", ",", round( $total, 2 ));
   		$total = 0;
   	}else{
    	$exportableArray[ "Total par client" ][] = '';
    }
    
    $rs->Move($i);
     
    $exportableArray[ "Date d'échéance" ][] = $deliv_payment;
    $exportableArray[ "Date de paiement" ][] = $payment_date;
 	
   	$rs->MoveNext();
	$i++;
}

exportArray( &$exportableArray );

//-------------------------------------------------------------------------------


function exportArray( &$exportableArray ){

        global $GLOBAL_START_PATH;

        include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );

        $cvsExportArray = new CSVExportArray( $exportableArray, "règlements_clients" );
        $cvsExportArray->export();

}

//-------------------------------------------------------------------------------


?>
