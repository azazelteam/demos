<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Lignes factures clients
 */

include_once( "../objects/classes.php" );

//----------------------------------------------------------------------------

if( !isset( $_GET[ "idcredit" ] ) ){
	
	$error = Dictionnary::translate( "missing_parameter" );
	
	$Title = "Références facture";

}else{

	$idcredit = intval( $_GET[ "idcredit" ] );
	$idbilling_buyer = DBUtil::getDBValue( "idbilling_buyer", "credits", "idcredit", $idcredit );

	$Title = "Références de la facture n°$idbilling_buyer";

}

$banner = "no";

$js = array();
//$js[] = "select_reference.js"; NAN ya du php dedans, marche pô

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

if( isset( $error ) ){

	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">$error</p>";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}

//----------------------------------------------------------------------------

?>
<script type="text/javascript">
<!--
	
	function selectReference( idbilling_buyer_row ){
	
		var quantity = document.getElementById( 'quantity_' + idbilling_buyer_row ).value;
		
		var location = '<?php echo $GLOBAL_START_URL ?>/accounting/credit.php?idcredit=<?php echo $idcredit ?>';
		location += '&idbilling_buyer_row=' + idbilling_buyer_row;
		location += '&quantity=' + quantity;
	
		window.opener.location = location;
		
		window.close();
	
	}
	
-->
</script>
<style type="text/css">
<!--
div#global{
	min-width:0;
	width:auto;
}
-->
</style>
<div id="globalMainContent" style="width:740px;">
<?php

listReferences( getAvailableReferences() );

?>
</div>
<?php

//-------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-------------------------------------------------------------------------------------------------------

/**
 * @return array( idbilling_buyer_row => available_quantity )
 */
function getAvailableReferences(){

	$idcredit = intval( $_GET[ "idcredit" ] );
	$references = array();
	
	//références de la facture
	
	$query = "
	SELECT bbr.idrow, bbr.quantity
	FROM billing_buyer_row bbr, billing_buyer bb, credits c
	WHERE c.idcredit = '$idcredit'
	AND c.idbilling_buyer = bb.idbilling_buyer
	AND bb.idbilling_buyer = bbr.idbilling_buyer
	ORDER BY bbr.idrow ASC";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$references[ $rs->fields( "idrow" ) ] = $rs->fields( "quantity" );
		$rs->MoveNext();
			
	}

	//soustraire les quantités déjà ajoutées dans l'avoir
	
	$query = "
	SELECT idbilling_buyer_row, quantity
	FROM credit_rows
	WHERE idcredit = '$idcredit'
	ORDER BY idbilling_buyer_row ASC";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$references[ $rs->fields( "idbilling_buyer_row" ) ] -= $rs->fields( "quantity" );
		$rs->MoveNext();
			
	}
	
	$availableReferences = array();
	foreach( $references as $idbilling_buyer_row => $quantity ){
		
		if( $quantity > 0 )
			$availableReferences[ $idbilling_buyer_row ] = $quantity;
			
	}

	return $availableReferences;
	
}

//-------------------------------------------------------------------------------------------------------

function listReferences( &$availableReferences ){
	
	global 	$GLOBAL_START_URL,
			$SSEResult;
	
	$idcredit = intval( $_GET[ "idcredit" ] );
	$lang = "_1";
	
	?>
	<div class="mainContent fullWidthContent" style="width:740px;">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Références disponibles</p>
				<div class="rightContainer"></div>
			</div>
			<div class="subContent">
				<div class="tableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th><?php echo Dictionnary::translate("gest_com_image") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_reference") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_designation") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_supplier") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_discount_price") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_unit_cost") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_quantity") ; ?></th>
								<th><?php echo Dictionnary::translate("gest_com_add") ; ?></th>
							</tr>
						</thead>
						<tbody>
		<?php 
		
		foreach( $availableReferences as $idbilling_buyer_row => $available_quantity ){
			
			$query = "
			SELECT bbr.idrow,
				bbr.idarticle,
				bbr.reference,
				bbr.designation,
				bbr.idsupplier,
				bbr.discount_price,
				bbr.unit_cost,
				'$available_quantity' AS available_quantity
			FROM billing_buyer_row bbr, credits c
			WHERE c.idcredit = '$idcredit'
				AND bbr.idbilling_buyer = c.idbilling_buyer
				AND bbr.idrow = '$idbilling_buyer_row'
			LIMIT 1";
			
			$rs =& DBUtil::query( $query );
		
			?>
							<tr>
								<td class="lefterCol">
									<img src="<?php echo URLFactory::getReferenceImageURI( $rs->fields( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE ) ?>" alt="" />
								</td>
								<td><?php echo $rs->fields( "reference" ) ?></td>
								<td><?php echo $rs->fields( "designation" ) ?></td>
								<td><?php echo getSupplierName( $rs->fields( "idsupplier" ) ) ?></td>
								<td><?php echo Util::priceFormat( $rs->fields( "discount_price" ) ) ?></td>
								<td><?php echo Util::priceFormat( $rs->fields( "unit_cost" ) ) ?></td>
								<td>
									<input type="text" id="quantity_<?php echo $rs->fields( "idrow" ) ?>" value="<?php echo $rs->fields( "available_quantity" ) ?>" onkeypress="return forceUnsignedIntegerValue( event );" class="textInput" style="width:70px;" />
								</td>
								<td class="righterCol">
									<a href="#" onclick="selectReference( <?php echo $rs->fields( "idrow" ) ?> ); return false;">
										<img src="<?php echo $GLOBAL_START_URL ?>/www/icones/ajout_devis.gif" alt="" style="border-style:none;" />
									</a>
								</td>
							</tr>
			<?php
			
		}
 
	?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<?php
	
}

//-------------------------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$db = &DBUtil::getConnection();
	$lang = User::getInstance()->getLang();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier ";
	
	$rs = $db->Execute( $query );
	if( $rs === false )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );

	return $rs->Fields('name');
	
}
//-----------------------------------------------------------------------------------------------------------
function getDelay($iddelay){
	
	
	
	$db =& DBUtil::getConnection();
	$lang = User::getInstance()->getLang();
	
	$rs = $db->Execute("SELECT delay$lang as d FROM delay WHERE iddelay='".$iddelay."'");
	
	if($rs === false ){
		die ("Impossible de récupérér le délai de la référence");
	}
	
	if($rs->RecordCount()>0){
		return $rs->fields("d");
	}else{
		return "";
	}
}

?>