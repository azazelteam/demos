﻿<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * gestion des relances clients
 */

//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/script/global.fct.php" );

$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

if( !isset( $_REQUEST[ "idbilling_buyer" ] ) || empty( $_REQUEST[ "idbilling_buyer" ] ) ){
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}

$idbilling_buyer = $_REQUEST["idbilling_buyer"];

$Invoice = new Invoice ($idbilling_buyer);

$db =& DBUtil::getConnection();

//Commercial commande
$reqcom = "SELECT u.firstname, u.lastname
			FROM user u, `order` o, billing_buyer bb
			WHERE u.iduser = o.iduser
			AND o.idorder = bb.idorder
			AND bb.idbilling_buyer = $idbilling_buyer";
$rscom = & DBUtil::query( $reqcom );

if($rscom===false)
	die("Impossible de récupérer le commercial de la commande");
	
$commercial_name = $rscom->fields("firstname")." ".$rscom->fields("lastname");


if(isset($_POST["Modify"])){
	
	$idbuyer = $Invoice->get("idbuyer");
	
	$rpd1 = euDate2us($_POST["relaunch_phone_date1"]);
	$rpd2 = euDate2us($_POST["relaunch_phone_date2"]);
	$rpd3 = euDate2us($_POST["relaunch_phone_date3"]);
	$rpd4 = euDate2us($_POST["relaunch_phone_date4"]);
	$rpr = euDate2us($_POST["relaunch_phone_recall"]);
	$rpt = $_POST["relaunch_phone_time"];
	$rph = $_POST["relaunch_phone_hour"];
	$rc = $_POST["relaunch_comment"];
	$accountant = $_POST["accountant"];
	$accountant_phonenumber = $_POST["accountant_phonenumber"];
	$accountant_faxnumber = $_POST["accountant_faxnumber"];
	$accountant_email = $_POST["accountant_email"];
	$rr1 =  $_POST["relaunch_result_1"];
	$rr2 =  $_POST["relaunch_result_2"];
	$rr3 =  $_POST["relaunch_result_3"];
	$rr4 =  $_POST["relaunch_result_4"];
	$rrd1 =  euDate2us($_POST["relaunch_result_date1"]);
	$rrd2 =  euDate2us($_POST["relaunch_result_date2"]);
	$rrd3 =  euDate2us($_POST["relaunch_result_date3"]);
	$rrd4 =  euDate2us($_POST["relaunch_result_date4"]);
	$paid_InProgress = isset($_POST["paid_InProgress"])?1:0;
	if($paid_InProgress){
		$paid_InProgress_date = isset($_POST["paid_InProgress_date"]) && $_POST["paid_InProgress_date"]!="" && $_POST["paid_InProgress_date"]!="00-00-0000" ?euDate2us($_POST["paid_InProgress_date"]):'0000-00-00';
	}else{
		$paid_InProgress_date = "0000-00-00";
	}
	$litigation = isset($_POST["litigation"])?1:0;
	
	$Invoice->set("relaunch_phone_date1",$rpd1);
	$Invoice->set("relaunch_phone_date2",$rpd2);
	$Invoice->set("relaunch_phone_date3",$rpd3);
	$Invoice->set("relaunch_phone_date4",$rpd4);
	$Invoice->set("relaunch_phone_recall",$rpr);
	$Invoice->set("relaunch_phone_time",$rpt);
	$Invoice->set("relaunch_phone_hour",$rph);
	$Invoice->set("relaunch_comment",$rc);
	$Invoice->set("relaunch_result_1",$rr1);
	$Invoice->set("relaunch_result_2",$rr2);
	$Invoice->set("relaunch_result_3",$rr3);
	$Invoice->set("relaunch_result_4",$rr4);
	$Invoice->set("relaunch_result_date1",$rrd1);
	$Invoice->set("relaunch_result_date2",$rrd2);
	$Invoice->set("relaunch_result_date3",$rrd3);
	$Invoice->set("relaunch_result_date4",$rrd4);
	$Invoice->set("relaunch_paid_InProgress",$paid_InProgress);
	$Invoice->set("relaunch_paid_InProgress_date",$paid_InProgress_date);
	$Invoice->set("relaunch_litigation",$litigation);
	$Invoice->set("relaunch_iduser",User::getInstance()->getId());
	$Invoice->set("relaunch_lastupdate",  date( "Y-m-d H:i:s" ));
	
	$Invoice->save();
		
	$query = "UPDATE buyer SET accountant='$accountant', accountant_phonenumber='$accountant_phonenumber', accountant_faxnumber='$accountant_faxnumber', accountant_email='$accountant_email'
				WHERE idbuyer=$idbuyer";
	$rs = $db->Execute($query);

	if($rs===false)
		die("Impossible de mettre à jour les informations du client");
		
	?><script type="text/javascript">
			<!--
			opener.location.reload();
			-->
		</script>
<?php

}

$query = "SELECT relaunch_phone_date1, relaunch_phone_date2, relaunch_phone_date3, relaunch_phone_date4, relaunch_phone_recall, relaunch_phone_time, relaunch_phone_hour, relaunch_comment,
		relaunch_result_1,relaunch_result_2,relaunch_result_3,relaunch_result_4
		FROM billing_buyer WHERE idbilling_buyer=$idbilling_buyer";
$rs = $db->Execute($query);

if($rs===false)
	die("Impossible de récupérer les informations de relance téléphonique");
	
$rpd1 = $rs->fields("relaunch_phone_date1");
$rpd2 = $rs->fields("relaunch_phone_date2");
$rpd3 = $rs->fields("relaunch_phone_date3");
$rpd4 = $rs->fields("relaunch_phone_date4");
$rpr = $rs->fields("relaunch_phone_recall");
$rpt = $rs->fields("relaunch_phone_time");
$rph = $rs->fields("relaunch_phone_hour");
$rc = $rs->fields("relaunch_comment");

if( $rpd1 == "0000-00-00" ){ //aucune date saisie
					
	$rpd1 = "";
	$startdate = date( "Y-m-d" );
					
}
else $startdate = $rpd1;

if( $rpd2 == "0000-00-00" ){ //aucune date saisie
					
	$rpd2 = "";
	$startdate = date( "Y-m-d" );
					
}
else $startdate = $rpd2;

if( $rpd3 == "0000-00-00" ){ //aucune date saisie
					
	$rpd3 = "";
	$startdate = date( "Y-m-d" );
					
}
else $startdate = $rpd3;

if( $rpd4 == "0000-00-00" ){ //aucune date saisie
					
	$rpd4 = "";
	$startdate = date( "Y-m-d" );
					
}
else $startdate = $rpd4;

if( $rpr == "0000-00-00" ){ //aucune date saisie
					
	$rpr = "";
	$startdater = date( "Y-m-d" );
					
}
else $startdater = $rpr;

list($Date, $Heure) = explode(" ",$Invoice->get("DateHeure"));
$invoice_date = $Date;

$accountant= isset( $_POST[ "accountant" ] ) ? $_POST[ "accountant" ] : $Invoice->getCustomer()->get("accountant");
$accountant_phonenumber= isset( $_POST[ "accountant_phonenumber" ] ) ? $_POST[ "accountant_phonenumber" ] : $Invoice->getCustomer()->get("accountant_phonenumber");
$accountant_faxnumber= isset( $_POST[ "accountant_faxnumber" ] ) ? $_POST[ "accountant_faxnumber" ] : $Invoice->getCustomer()->get("accountant_faxnumber");
$accountant_email= isset( $_POST[ "accountant_email" ] ) ? $_POST[ "accountant_email" ] : $Invoice->getCustomer()->get("accountant_email");

$rr1 = $Invoice->get("relaunch_result_1");
$rr2 = $Invoice->get("relaunch_result_2");
$rr3 = $Invoice->get("relaunch_result_3");
$rr4 = $Invoice->get("relaunch_result_4");

$rrd1 = $Invoice->get("relaunch_result_date1");
$rrd2 = $Invoice->get("relaunch_result_date2");
$rrd3 = $Invoice->get("relaunch_result_date3");
$rrd4 = $Invoice->get("relaunch_result_date4");

$doc=0;
$doc1=0;
$doc2=0;
$doc3=0;

if( $rrd1 == "0000-00-00" ){ //aucune date saisie
					
	$rrd1 = "";
	$startdate_rrd1 = date( "Y-m-d" );
					
}
else $startdate_rrd1 = $rrd1;

if( $rrd2 == "0000-00-00" ){ //aucune date saisie
					
	$rrd2 = "";
	$startdate_rrd2 = date( "Y-m-d" );
					
}
else $startdate_rrd2 = $rrd2;

if( $rrd3 == "0000-00-00" ){ //aucune date saisie
					
	$rrd3 = "";
	$startdate_rrd3 = date( "Y-m-d" );
					
}
else $startdate_rrd3 = $rrd3;

if( $rrd4 == "0000-00-00" ){ //aucune date saisie
					
	$rrd4 = "";
	$startdate_rrd4 = date( "Y-m-d" );
					
}
else $startdate_rrd4 = $rrd4;

$paid_InProgress = $Invoice->get("relaunch_paid_InProgress");
$paid_InProgress_date = usDate2eu($Invoice->get("relaunch_paid_InProgress_date"));

$litigation = $Invoice->get("relaunch_litigation");

$iduser = $Invoice->get("relaunch_iduser");

$relaunch_lastupdate = $Invoice->get("relaunch_lastupdate");

?>
<style type="text/css">
<!--
div#global{
	min-width:0px;
	width:auto;
}
-->
</style>
<script type="text/javascript">
			<!--
			
			function popupcomment(idbilling){
		
				postop = (self.screen.height-700)/2;
				posleft = (self.screen.width-800)/2;
		
				window.open('printable_billing_relaunch_comment.php?idbilling_buyer='+idbilling, '_blank', 'top=' + postop + ',left=' + posleft + ',width=800,height=700,resizable,scrollbars=yes,status=0');
		
			}
			-->
		</script>
<div id="globalMainContent" style="width:770px;">
	<form name="frm" method="post" action="<?=$GLOBAL_START_URL?>/accounting/billing_relaunch_comment.php">
		<input type="hidden" name="idbilling_buyer" value="<?=$idbilling_buyer?>" />
		<input type="hidden" name="Modify" value="1" />
		<?php if( isset( $_POST[ "Modify" ] ) ){ ?><div style="color:#FF0000; font-size:14px; font-weight:bold; text-align:center;">Les modifications ont été enregistrées</div><?php } ?>
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle">
					<p class="title">
						Fiche de suivi des relances
					</p>
					<div class="rightContainer">
					</div>
				</div>
				<div class="subContent">
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell">Nom / Raison sociale</th>
								<td><a href="#" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Invoice->get("idbuyer"); ?>'); return false;"><?php echo $Invoice->getCustomer()->get("company");?></a></td>
								<th class="filledCell">Client n°</th>
								<td><a href="#" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $Invoice->get("idbuyer"); ?>'); return false;"><?php echo $Invoice->get("idbuyer");?></a></td>
							</tr>
							<tr>
								<th class="filledCell">Nom</th>
								<td><?php echo DBUtil::getDBValue( "title_1", "title", "idtitle", $Invoice->getCustomer()->getContact()->get( "title" ) )." ".$Invoice->getCustomer()->getContact()->get( "lastname" )." ".$Invoice->getCustomer()->getContact()->get( "firstname" );?></td>
								<th class="filledCell">Tél.</th>
								<td><?php echo HumanReadablePhone($Invoice->getCustomer()->getContact()->get( "phonenumber" ));?></td>
							</tr>
							<tr>
								<th class="filledCell">Siren</th>
								<td><?php echo substr($Invoice->getCustomer()->get("siret"),0,9);?></td>
								<th class="filledCell">Code postal</th>
								<td><?php echo $Invoice->getCustomer()->get("zipcode");?></td>
							</tr>
							<tr>
								<th class="filledCell">Nom comptable</th>
								<td><input type="text" name="accountant" value="<?=$accountant?>" class="textInput" /></td>
								<th class="filledCell">Tél. comptable</th>
								<td><input type="text" name="accountant_phonenumber" value="<?=$accountant_phonenumber?>" class="textInput" /></td>
							</tr>
							<tr>
								<th class="filledCell">Fax comptable</th>
								<td><input type="text" name="accountant_faxnumber" value="<?=$accountant_faxnumber?>" class="textInput" /></td>
								<th class="filledCell">Email comptable </th>
								<td><input type="text" name="accountant_email" value="<?=$accountant_email?>" class="textInput" /></td>
							</tr>
							<tr>
								<th class="filledCell">Facture n°</th>
								<td><a href="#" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $idbilling_buyer ?>'); return false;"><?php echo $idbilling_buyer ?></a></td>
								<th class="filledCell">Date facture</th>
								<td><?=usDate2eu($invoice_date)?></td>
							</tr>
							<tr>
								<th class="filledCell">Montant TTC</th>
								<td><?php echo Util::priceFormat($Invoice->get("total_amount"));?></td>
								<th class="filledCell">Echéance</th>
								<td><?=usDate2eu($Invoice->get("deliv_payment"))?></td>
							</tr>
							<tr>
								<th class="filledCell">Commercial facture</th>
								<td><?php echo $Invoice->getSalesman()->get("firstname")." ".$Invoice->getSalesman()->get("lastname")?></td>
								<th class="filledCell">Commercial affaire</th>
								<td><?=$commercial_name?></td>
							</tr>
							<tr>
							<th class="filledCell" colspan="2">Dernière mise à jour</th>
							<td colspan="2">
							<?if( $iduser == 0 ){?>
								
								Relance non traitée
							
							<?php } else{
								
								//Infos commercial
								$requser = "SELECT u.firstname, u.lastname
								FROM user u
								WHERE u.iduser = $iduser";
								$rsuser = & DBUtil::query( $requser );

								if( $rsuser === false )
									die("Impossible de récupérer le commercial de la relance");
	
								echo $rsuser->fields("firstname")." ".$rsuser->fields("lastname")." le ".humanReadableDatetime($relaunch_lastupdate);	
							}?>
							</td>
							</tr>
						</table>
						<br />
						<b>Le règlement va nous parvenir</b> <input type="checkbox" name="paid_InProgress" value="1" class="textInput" onclick="if(this.checked){$('#paid_InProgress_date').datepicker( 'enable' );}else{$('#paid_InProgress_date').datepicker( 'disable' );}" <?if($paid_InProgress){echo"checked=\"checked\"";}?> />
						<input type="text" name="paid_InProgress_date" id="paid_InProgress_date" value="<?php echo usDate2eu( $paid_InProgress_date ); ?>" class="calendarInput" <?if(!$paid_InProgress){echo"disabled=\"disabled\"";}?>/>
						<?php echo DHTMLCalendar::calendar( "paid_InProgress_date" ) ?>
						<br /><br />
						<b>Litige par rapport au règlement</b> <input type="checkbox" name="litigation" value="1" class="textInput" <?if($litigation){echo"checked=\"checked\"";}?> />
					</div>
					<?php
					
					if($Invoice->get("relaunch_type_1")==1 || $Invoice->get("relaunch_type_2")==1 || $Invoice->get("relaunch_type_3")==1){
						
						$doc=1;
					
					?>
					<div class="subTitleContainer">
						<p class="subTitle">Relances mail</p>
					</div>
					<?php
						
						if( $Invoice->get( "relaunch_date_1" ) != "0000-00-00" && $Invoice->get( "relaunch_type_1" ) == 1 ){ ?>
							<br/>Niveau 1 : le <?php echo usDate2eu( $Invoice->get( "relaunch_date_1" ) ) ?> (<a href="<?php echo $GLOBAL_START_URL ?>/accounting/relaunch_preview.php?idbilling_buyer=<?php echo $idbilling_buyer ?>&amp;level=1&amp;source=comment">mail</a>)<br />
							<?php
							
							$doc1 = 1;
						}
						
						if( $Invoice->get( "relaunch_date_2" ) != "0000-00-00" && $Invoice->get( "relaunch_type_2" ) == 1 ){ ?>
							<br/>Niveau 2 : le <?php echo usDate2eu( $Invoice->get( "relaunch_date_2" ) ) ?> (<a href="<?php echo $GLOBAL_START_URL ?>/accounting/relaunch_preview.php?idbilling_buyer=<?php echo $idbilling_buyer ?>&amp;level=2&amp;source=comment">mail</a>)<br />
							<?php
							
							$doc2 = 1;
						}
						
						if( $Invoice->get( "relaunch_date_3" ) != "0000-00-00" && $Invoice->get( "relaunch_type_3" ) == 1 ){?>
							<br/>Niveau 3 : le <?php echo usDate2eu( $Invoice->get( "relaunch_date_3" ) ) ?> (<a href="<?php echo $GLOBAL_START_URL ?>/accounting/relaunch_preview.php?idbilling_buyer=<?php echo $idbilling_buyer ?>&amp;level=3&amp;source=comment">mail</a>)<br />
							<?php
							
							$doc3 = 1;
						}
						
					}
					
					if( $Invoice->get( "relaunch_type_1" ) == 2 || $Invoice->get( "relaunch_type_2" ) == 2 || $Invoice->get( "relaunch_type_3" ) == 2 ){
						$doc = 1;
					
					?>
					<div class="subTitleContainer">
						<p class="subTitle">Relances fax</p>
					</div>
					<?php
						
						if(($Invoice->get("relaunch_date_1")!='0000-00-00') && ($Invoice->get("relaunch_type_1")==2)){?>
							<br/>Niv. 1 : le <?=usDate2eu($Invoice->get("relaunch_date_1")) ?><br />
							<?php
							
							$doc1 = 1;
						}
						if(($Invoice->get("relaunch_date_2")!='0000-00-00') && ($Invoice->get("relaunch_type_2")==2)){?>
							<br/>Niv. 2 : le <?=usDate2eu($Invoice->get("relaunch_date_2")) ?><br />
							<?php
							
							$doc2 = 1;
						}
						if(($Invoice->get("relaunch_date_3")!='0000-00-00') && ($Invoice->get("relaunch_type_3")==2)){?>
							<br/>Niv. 3 : le <?=usDate2eu($Invoice->get("relaunch_date_3")) ?><br />
							<?php
							
							$doc3 = 1;
						}
						
					}
					
					if($doc){
					?>
					<br />
					<div class="subTitleContainer">
						<p class="subTitle">Documents</p>
					</div>
					<?php if( $doc1 ){ ?><br />Voir<br /><?php } ?>
					<?php if( $doc2 ){ ?><br />Voir<br /><?php } ?>
					<?php if( $doc3 ){ ?><br />Voir<br /><?php } ?>
					<?php } ?>
					<br />
					<div class="subTitleContainer">
						<p class="subTitle">Relances téléphoniques</p>
					</div>
					<div style="margin:5px; text-align:center;">
						<b>A relancer le </b>
						<input type="text" name="relaunch_phone_recall" id="relaunch_phone_recall" value="<?php echo usDate2eu( $rpr ); ?>" class="calendarInput" />
						<?php echo DHTMLCalendar::calendar( "relaunch_phone_recall" ) ?>
						<?php timeList( $rpt ); ?>
						<b> Heure </b><input type="text" name="relaunch_phone_hour" id="relaunch_phone_hour" value="<?php echo ( $rph ); ?>" class="textInput" />
					</div>
					<div class="tableContainer">
						<table class="dataTable">
							<thead>
								<tr>
									<th class="filledCell">Date</th>
									<th class="filledCell">Réponse client</th>
									<th class="filledCell">Le</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>
										<input type="text" name="relaunch_phone_date1" id="relaunch_phone_date1" value="<?php echo usDate2eu( $rpd1 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_phone_date1" ) ?>
									</td>
									<td><?=Result_List(1,$Invoice->get("relaunch_result_1"))?></td>
									<td>
										<input type="text" name="relaunch_result_date1" id="relaunch_result_date1" value="<?php echo usDate2eu( $rrd1 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_result_date1" ) ?>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" name="relaunch_phone_date2" id="relaunch_phone_date2" value="<?php echo usDate2eu( $rpd2 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_phone_date2" ) ?>
									</td>
									<td><?=Result_List(2,$Invoice->get("relaunch_result_2"))?></td>
									<td>
										<input type="text" name="relaunch_result_date2" id="relaunch_result_date2"value="<?php echo usDate2eu( $rrd2 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_result_date2" ) ?>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" name="relaunch_phone_date3" id="relaunch_phone_date3" value="<?php echo usDate2eu( $rpd3 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_phone_date3" ) ?>
									</td>
									<td><?=Result_List(3,$Invoice->get("relaunch_result_3"))?></td>
									<td>
										<input type="text" name="relaunch_result_date3" id="relaunch_result_date3" value="<?php echo usDate2eu( $rrd3 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_result_date3" ) ?>
									</td>
								</tr>
								<tr>
									<td>
										<input type="text" name="relaunch_phone_date4" id="relaunch_phone_date4" value="<?php echo usDate2eu( $rpd4 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_phone_date4" ) ?>
									</td>
									<td><?=Result_List(4,$Invoice->get("relaunch_result_4"))?></td>
									<td>
										<input type="text" name="relaunch_result_date4" id="relaunch_result_date4" value="<?php echo usDate2eu( $rrd4 ); ?>" class="calendarInput" />
										<?php echo DHTMLCalendar::calendar( "relaunch_result_date4" ) ?>
									</td>
								</tr>
								<tr>
									<th>Commentaire</th>
									<td colspan="2"><textarea name="relaunch_comment" cols="50" rows="10" style="width:100%;"><?=stripslashes($rc)?></textarea></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="button" value="Transmettre la facture" class="blueButton" onclick="document.location='<?=$GLOBAL_START_URL?>/accounting/transmit_relaunch.php?idbilling_buyer=<?=$idbilling_buyer?>&amp;level=1&amp;reminder=1'; return false;" />
						<input type="button" value="Enregistrer les modifications" class="blueButton" onclick="document.frm.submit(); return false;" />
						<input type="button" value="Imprimer" class="blueButton" onclick="popupcomment('<?=$idbilling_buyer?>'); return false;" />
						<input type="button" value="Visualiser la facture" class="blueButton" onclick="window.open('<?=$GLOBAL_START_URL?>/catalog/pdf_invoice.php?idinvoice=<?=$idbilling_buyer?>'); return false;" />
					</div>
				</div>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
	</form>
</div>

<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//--------------------------------------------------------------------

function timeList($selected=""){
	
	$times = array('Indifférent','Matin','Après-Midi');
	
	?>
	<select name="relaunch_phone_time">
	<?
	for($i=0;$i<count($times);$i++){
		
		$sel='';
		
		if($selected==$times[$i])
			$sel = "selected=\"selected\"";
		
		?>
		<option value="<?=$times[$i]?>" <?=$sel?>><?=$times[$i]?></option>
		<?
	
	}?>
	</select>
	<?	
}

//------------------------------------------------------------------------------------------------------------------------
function HumanReadablePhone($tel){
	//On regarde la longeueur de la cheine pour parcourir
	$lg=strlen($tel);

	$telwcs='';
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($tel,$i,1);
	
		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if($i==0){
			if(($tocheck=='+')OR(is_numeric($tocheck))){
				$telwcs.=$tocheck;
			}
		}else{
			//On ne garde que les chiffres
			if(is_numeric($tocheck)){
				$telwcs.=$tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg=strlen($telwcs);

	$telok ='';

	
	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck=substr($telwcs,0,1);

	if($tocheck=='+'){
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,3);
		$telok.=$tocheck." ";
		$cpt=3;
	}else{
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,2);
		$telok.=$tocheck." ";
		$cpt=2;
	}

	//On traite le reste de la chaine
	for($i=$cpt;$i<$lg;$i+=2){
		if($i==$cpt){
			$tocheck=substr($telwcs,$i,2);
		}else{
			$tocheck=substr($telwcs,$i,2);
		}
		$telok.=$tocheck." ";
	}	

	//On enlève l'espace de fin
	$telok=trim($telok);

	return $telok;
}

//-----------------------------------------------------------------------------------------------

function Result_List( $id, $selected = "" ){
	
?>
<select name="relaunch_result_<?=$id?>">
	<option value="0" <?if($selected==0 || $selected==''){echo"selected=\"selected\"";}?>>Sélectionner</option>
	<optgroup label="Absence">
		<option value="1" <?if($selected==1){echo"selected=\"selected\"";}?>>Ne réponds pas</option>
		<option value="2" <?if($selected==2){echo"selected=\"selected\"";}?>>Absent</option>
		<option value="3" <?if($selected==3){echo"selected=\"selected\"";}?>>Message sur boîte vocale</option>
		<option value="4" <?if($selected==4){echo"selected=\"selected\"";}?>>Rappeler la comptable</option>
		<option value="16" <?if($selected==16){echo"selected=\"selected\"";}?>>Va rappeler</option>
	</optgroup>
	<optgroup label="Problème">
		<option value="5" <?if($selected==5){echo"selected=\"selected\"";}?>>Rappeler on va vérifier</option>
		<option value="6" <?if($selected==6){echo"selected=\"selected\"";}?>>Renvoyer un duplicata</option>
		<option value="7" <?if($selected==7){echo"selected=\"selected\"";}?>>Pas reçu de facture</option>
		<option value="8" <?if($selected==8){echo"selected=\"selected\"";}?>>Litige</option>
	</optgroup>
	<optgroup label="Déjà réglé">
		<option value="9" <?if($selected==9){echo"selected=\"selected\"";}?>>Traite renvoyée au factor</option>
		<option value="10" <?if($selected==10){echo"selected=\"selected\"";}?>>Traite renvoyée à nous</option>
		<option value="11" <?if($selected==11){echo"selected=\"selected\"";}?>>Virement effectué au factor</option>
		<option value="12" <?if($selected==12){echo"selected=\"selected\"";}?>>Virement effectué à nous</option>
		<option value="13" <?if($selected==13){echo"selected=\"selected\"";}?>>Chèque envoyé au factor</option>
		<option value="14" <?if($selected==14){echo"selected=\"selected\"";}?>>Chèque envoyé à nous</option>
		<option value="15" <?if($selected==15){echo"selected=\"selected\"";}?>>Déjà débité</option>
	</optgroup>
</select>
<?php
	
}

//-----------------------------------------------------------------------------------------------

function drawTime( $id, $date = 0 ){
	
	$hour = date( "H" );
	$minutes = date( "i" );
	
?>
										<select name="relaunch_phone_hour<?php echo $id ?>">
											<option value="00"<?php if( $hour == 0 ){ echo " selected=\"selected\""; } ?>>00</option>
											<option value="01"<?php if( $hour == 1 ){ echo " selected=\"selected\""; } ?>>01</option>
											<option value="02"<?php if( $hour == 2 ){ echo " selected=\"selected\""; } ?>>02</option>
											<option value="03"<?php if( $hour == 3 ){ echo " selected=\"selected\""; } ?>>03</option>
											<option value="04"<?php if( $hour == 4 ){ echo " selected=\"selected\""; } ?>>04</option>
											<option value="05"<?php if( $hour == 5 ){ echo " selected=\"selected\""; } ?>>05</option>
											<option value="06"<?php if( $hour == 6 ){ echo " selected=\"selected\""; } ?>>06</option>
											<option value="07"<?php if( $hour == 7 ){ echo " selected=\"selected\""; } ?>>07</option>
											<option value="08"<?php if( $hour == 8 ){ echo " selected=\"selected\""; } ?>>08</option>
											<option value="09"<?php if( $hour == 9 ){ echo " selected=\"selected\""; } ?>>09</option>
											<option value="10"<?php if( $hour == 10 ){ echo " selected=\"selected\""; } ?>>10</option>
											<option value="11"<?php if( $hour == 11 ){ echo " selected=\"selected\""; } ?>>11</option>
											<option value="12"<?php if( $hour == 12 ){ echo " selected=\"selected\""; } ?>>12</option>
											<option value="13"<?php if( $hour == 13 ){ echo " selected=\"selected\""; } ?>>13</option>
											<option value="14"<?php if( $hour == 14 ){ echo " selected=\"selected\""; } ?>>14</option>
											<option value="15"<?php if( $hour == 15 ){ echo " selected=\"selected\""; } ?>>15</option>
											<option value="16"<?php if( $hour == 16 ){ echo " selected=\"selected\""; } ?>>16</option>
											<option value="17"<?php if( $hour == 17 ){ echo " selected=\"selected\""; } ?>>17</option>
											<option value="18"<?php if( $hour == 18 ){ echo " selected=\"selected\""; } ?>>18</option>
											<option value="19"<?php if( $hour == 19 ){ echo " selected=\"selected\""; } ?>>19</option>
											<option value="20"<?php if( $hour == 20 ){ echo " selected=\"selected\""; } ?>>20</option>
											<option value="21"<?php if( $hour == 21 ){ echo " selected=\"selected\""; } ?>>21</option>
											<option value="22"<?php if( $hour == 22 ){ echo " selected=\"selected\""; } ?>>22</option>
											<option value="23"<?php if( $hour == 23 ){ echo " selected=\"selected\""; } ?>>23</option>
										</select> : 
										<select name="relaunch_phone_minutes<?php echo $id ?>">
											<option value="00"<?php if( $minutes >= 0 && $minutes < 5 ){ echo " selected=\"selected\""; } ?>>00</option>
											<option value="05"<?php if( $minutes >= 5 && $minutes < 10 ){ echo " selected=\"selected\""; } ?>>05</option>
											<option value="10"<?php if( $minutes >= 10 && $minutes < 15 ){ echo " selected=\"selected\""; } ?>>10</option>
											<option value="15"<?php if( $minutes >= 15 && $minutes < 20 ){ echo " selected=\"selected\""; } ?>>15</option>
											<option value="20"<?php if( $minutes >= 20 && $minutes < 25 ){ echo " selected=\"selected\""; } ?>>20</option>
											<option value="25"<?php if( $minutes >= 25 && $minutes < 30 ){ echo " selected=\"selected\""; } ?>>25</option>
											<option value="30"<?php if( $minutes >= 30 && $minutes < 35 ){ echo " selected=\"selected\""; } ?>>30</option>
											<option value="35"<?php if( $minutes >= 35 && $minutes < 40 ){ echo " selected=\"selected\""; } ?>>35</option>
											<option value="40"<?php if( $minutes >= 40 && $minutes < 45 ){ echo " selected=\"selected\""; } ?>>40</option>
											<option value="45"<?php if( $minutes >= 45 && $minutes < 50 ){ echo " selected=\"selected\""; } ?>>45</option>
											<option value="50"<?php if( $minutes >= 50 && $minutes < 55 ){ echo " selected=\"selected\""; } ?>>50</option>
											<option value="55"<?php if( $minutes >= 55 && $minutes < 60 ){ echo " selected=\"selected\""; } ?>>55</option>
										</select>
<?php
	
}

//-----------------------------------------------------------------------------------------------

?>