<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Remboursements clients
 */

//------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------------

if( isset( $_REQUEST[ "repay" ] ) && isset( $_REQUEST[ "idrepayment" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$query = "
	SELECT b.idbuyer, b.credit_balance, r.amount
	FROM buyer b, credits c, billing_buyer bb, repayments r
	WHERE r.idrepayment = '" . intval( $_REQUEST[ "idrepayment" ] ) . "'
	AND r.idcredit = c.idcredit
	AND c.idbilling_buyer = bb.idbilling_buyer
	AND bb.idbuyer = b.idbuyer
	AND b.credit_balance >= r.amount
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		exit( "0" );
	
	$tmp = explode( "-", $_POST[ "payment_date" ] );
	
	if( count( $tmp ) != 3 )
		exit( "0" );
		
	list( $day, $month, $year ) = $tmp;
	
	$query = "
	UPDATE buyer 
	SET credit_balance = GREATEST( 0.0, credit_balance - " . $rs->fields( "amount" ) . " )
	WHERE idbuyer = '" . $rs->fields( "idbuyer" ) . "'
	AND credit_balance >= " . $rs->fields( "amount" ) . "
	LIMIT 1";

	$ret = 	DBUtil::query( $query ) !== false;
	$ret &= DBUtil::query( "UPDATE repayments SET payment_date = '$year-$month-$day 00:00:00' WHERE idrepayment = '" . $_REQUEST[ "idrepayment" ] . "' LIMIT 1" ) !== false;
	
	exit( $ret ? "1" : "0" );
	
}

//------------------------------------------------------------------------------------------------

$Title = "Rechercher un remboursements";

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );

include_once( $GLOBAL_START_PATH . "/catalog/admin_func.inc.php" );
include_once( $GLOBAL_START_PATH . "/catalog/drawlift.php" );

//------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<a name="top"></a>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent">
        <div class="topRight"></div>
        <div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Recherche de remboursements</p>
            </div>
            <div class="subContent">
            	<?php displayForm(); ?>
			</div>
		</div>
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div>
	<div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div>
            	<div class="topLeft"></div>
                <p class="title">Outils</p>
           	</div>
            <div class="content"></div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div>
            	<div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            	<div id="msgInfos" style="padding:10px 4px 4px 10px; font-weight:bold; color:#FF0000; text-align:center;"></div>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>
    <div id="SearchResults"></div>
</div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$formFields = array(
	
		$fieldName = "minus_date_select" => $defaultValue = "-90",
		"interval_select" 			=> 3,
		"idbuyer" 					=> "",
		"lastname" 					=> "",
		"company" 					=> "",
		"faxnumber" 				=> "",
		"zipcode" 					=> "",
		"city" 						=> "",
		"idpayment" 				=> "",
		"iduser" 					=> "",
		"idbilling_buyer" 			=> "",

		"page" 						=> 1,
		"orderBy" 					=> "repayments.creation_date DESC"
		
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;

	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback 
  
		    };
    
    		$('#SearchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
        
        function preSubmitCallback( formData, jqForm, options ){
        	
        	$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
            	fadeOut: 700
            		
			}); 
        	
        	$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
        	
        }
        
        /* ----------------------------------------------------------------------------------- */
        
        function postSubmitCallback( responseText, statusText ){ 
    
    		$.unblockUI();
    		
    		if( statusText != 'success' || responseText == '0' ){
    			
    			alert( "Impossible d'effectuer la recherche" );
    			return;
    			
    		}

        	$( '#SearchResults' ).html( responseText );
        	
        }
	
		/* ----------------------------------------------------------------------------------- */
		
		function repay( idrepayment ){

			var reg = new RegExp( "[0-9]{2}-[0-9]{2}-[0-9]{4}", "g" );

			if( !reg.test( $( "#payment_date" + idrepayment ).val() ) ){

				alert( "Le format de la date de remboursement est incorrect" );
				return false;
				
			}
			
			var data = 'repay=1&idrepayment=' + idrepayment + '&payment_date=' + $( "#payment_date" + idrepayment ).val();
			
			$.ajax({
			 	
				url: "<?php echo $_SERVER[ "PHP_SELF" ] ?>",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( onErrorMessage ); },
			 	success: function( responseText ){

					if( responseText == "0" )
						alert( "Impossible de valider le remboursement" );
					else{
							
						$('#SearchForm').ajaxSubmit( {
					
					        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
					        success:       postSubmitCallback  // post-submit callback 
			    		
			    		} );
			    		
					}
	    		
				}

			});
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function orderBy( str ){

			document.getElementById( 'SearchForm' ).elements[ 'page' ].value = '1';
			document.getElementById( 'SearchForm' ).elements[ 'orderBy' ].value = str;
			
			$('#SearchForm').ajaxSubmit( { 
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback 
    		} ); 
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function goToPage( pageNumber ){
		
			document.getElementById( 'SearchForm' ).elements[ 'page' ].value = pageNumber;
			document.getElementById( 'SearchForm' ).submit();
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectBetween(){
			
			document.getElementById( "interval_select_between" ).checked=true;
			return true;
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
		<input type="hidden" name="search" value="1" />
		<input type="hidden" name="page" value="<?php echo $postData[ "page" ] ?>" />
		<input type="hidden" name="orderBy" value="<?php echo $postData[ "orderBy" ] ?>" />
		<div class="tableContainer">
			<p><b>Date d'enregistrement</b></p>
            <table class="dataTable" style="margin-top:4px;">
                <tr>	
                    <th style="width:20%" class="filledCell">
                    	<input type="radio" name="interval_select" id="interval_select_relance" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1 || !isset( $_POST[ "interval_select" ] ) ) echo " checked=\"checked\""; ?> /> 
                    	Depuis
                    </th>
                    <td style="width:30%">
                    	<select name="minus_date_select" class="fullWidth">
							<option value="0" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == 0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ?></option>
							<option value="-1" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ?></option>
							<option value="-2" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ?></option>
							<option value="-7" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ?></option>
							<option value="-14" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ?></option>
							<option value="-30" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ?></option>
							<option value="-60" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ?></option>
							<option value="-90" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ?></option>
							<option value="-180" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ?></option>
							<option value="-365" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ?></option>
							<option value="-730" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -730) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2year") ?></option>
							<option value="-1825" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1825) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_5year") ?></option>
						</select>
                    </td>
                    <th style="width:20%" class="filledCell">
                    	<input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> 
                    	Pour le mois de
                    </th>
                    <td style="width:30%">
                    	<?php FStatisticsGenerateDateHTMLSelect( "selectMonth()" ); ?>
                    </td>
                </tr>
                <tr>	
                    <th class="filledCell"><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> /> Entre le</th>
                    <td><?php $date = isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : ""; ?><input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_start", true, "selectBetween" ) ?></td>
                    <th class="filledCell"> et le</th>
                    <td><?php $date = isset( $_POST[ "sp_bt_end" ] ) ? $_POST[ "sp_bt_end" ] : ""; ?><input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_end", true, "selectBetween" ) ?></td>
                </tr>
            </table>
		</div>
		<div class="tableContainer">
            <table class="dataTable">
                <tr class="filledRow">
                    <th style="width:20%">Client n°</th>
                    <td style="width:30%"><input type="text" name="idbuyer" class="textInput" value="<?php echo $postData[ "idbuyer" ] ?>" /></td>
                    <th style="width:20%">Nom du contact</th>
                    <td style="width:30%">
                    	<input type="text" name="lastname" id="lastname" class="textInput" value="<?php echo htmlentities( $postData[ "lastname" ] ) ?>" />
                    	<?php AutoCompletor::completeFromDB( "lastname", "contact", "lastname", 15 ); ?>
                    </td>
                </tr>
                <tr>
                    <th>Nom / Raison sociale</th>
                    <td>
                    	<input type="text" name="company" id="company" class="textInput" value="<?php echo htmlentities( $postData[ "company" ] ) ?>" />
                    	<?php AutoCompletor::completeFromDB( "company", "buyer", "company", 15 ); ?>
                    </td>
                    <th>Fax</th>
                    <td><input type="text" name="faxnumber" class="textInput" value="<?php echo htmlentities( $postData[ "faxnumber" ] ) ?>" /></td>
                </tr>
                <tr>
                    <th>Code postal</th>
                    <td><input type="text" name="zipcode" class="textInput" value="<?php echo htmlentities( $postData[ "zipcode" ] ) ?>" /></td>
                    <th>Ville</th>
                    <td>
                    	<input type="text" name="city" id="city" class="textInput" value="<?php echo htmlentities( $postData[ "city" ] ) ?>" />
                    	<?php AutoCompletor::completeFromDB( "city", "buyer", "city", 15 ); ?>
                    </td>
                </tr>
                <tr>
                	<td colspan="4" class="tableSeparator"></td>
                </tr>
                <tr class="filledRow">
                    <th>Facture n°</th>
                    <td><input type="text" name="idbilling_buyer" class="textInput" value="<?php echo $postData[ "idbilling_buyer" ] ?>" /></td>
                   	<th>Commercial</th>
					<td>
					<?php
								
						if( User::getInstance()->get( "admin" ) ){

							include_once( dirname( __FILE__ ) . "/../script/user_list.php" );
							userList( true, $postData[ "iduser" ] );
							//DrawLift_commercial( "user", $postData[ "iduser" ], "ORDER BY firstname ASC" );
							
						}
						else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
																	
					?>
					</td>
				</tr>
				<tr>
					<th class="filledCell">Mode de remboursement</th>
					<td>
						<?php XHTMLFactory::createSelectElement( "payment", "idpayment", "name" . User::getInstance()->getLang(), "name" . User::getInstance()->getLang(), $postData[ "idpayment" ], "", "Tous", "name=\"idpayment\"" ); ?>
					</td>
					<th class="filledCell">Statut</th>
					<td>
						<select name="repaid">
							<option value="">Tous</option>
							<option value="1">Effectué</option>
							<option value="0">A effectuer</option>
						</select>
					</td>
				</tr>
			</table>
		</div>
		<div class="submitButtonContainer">
	    	<input type="submit" class="blueButton" value="Rechercher" />
	    </div>
	</form>
	<?php

}
//----------------------------------------------------------------------------------------------------

function search(){
	
	global 	$GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
	
	$resultPerPage 		= 50000;
	$maxSearchResults 	= 1000;

	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	$select = "
	repayments.idrepayment,
	repayments.idpayment,
	repayments.amount,
	SUBSTRING( repayments.creation_date, 1, 10 ) AS creation_date,
	SUBSTRING( repayments.payment_date, 1, 10 ) AS payment_date,
	repayments.payment_date <> '0000-00-00 00:00:00' AS repaid,
	`user`.initial,
	billing_buyer.idbilling_buyer,
	credits.idlitigation,
	credits.idcredit,
	buyer.idbuyer,
	buyer.zipcode,
	buyer.company,
	contact.lastname,
	contact.title,
	payment.name" . User::getInstance()->getLang() . ",
	buyer.credit_balance";
	
	$tables = "repayments, payment, credits, `user`, billing_buyer, buyer, contact";
	
	$where = "
	repayments.idcredit = credits.idcredit
	AND repayments.idpayment = payment.idpayment
	AND credits.iduser = `user`.iduser
	AND credits.idbilling_buyer = billing_buyer.idbilling_buyer
	AND billing_buyer.idcontact = contact.idcontact
	AND billing_buyer.idbuyer = contact.idbuyer
	AND billing_buyer.idbuyer = buyer.idbuyer";
	
	//integers
	
	$integers = array( 

		"buyer.idbuyer", 
		"billing_buyer.idbilling_buyer",
		"payment.idpayment",
		"user.iduser"
		
	);

	foreach( $integers as $integer ){
		
		list( $table, $column ) = explode( ".", $integer );
		
		if( isset( $_POST[ $column ] ) && intval( $_POST[ $column ] ) )
			$where .= " AND `$table`.`$column` = " . intval( $_POST[ $column ] );
		
	}
	
	//strings
	
	$strings = array( "contact.lastname", "buyer.company", "buyer.city", "buyer.zipcode" );
	
	foreach( $strings as $string ){
		
		list( $table, $column ) = explode( ".", $string );
		
		if( isset( $_POST[ $column ] ) && strlen( $_POST[ $column ] ) )
			$where .= " AND `$table`.`$column` LIKE '%" . Util::html_escape( $_POST[ $column ] ) . "%'";
		
	}
	
	//numbers ( phone, fax, ... )
	
	$numbers = array( "contact.faxnumber" );
	
	foreach( $numbers as $number ){
		
		list( $table, $column ) = explode( ".", $number );
		
		if( isset( $_POST[ $column ] ) && !empty( $_POST[ $column ] ) ){
			
			$num = ereg_replace( "[^0-9]*", "", $_POST[ $column ] );
			$regexp = "";
			$i = 0;
			while( $i < strlen( $num ) ){
				
				if( $i )
					$regexp .= "[^0-9]*";
					
				$regexp .= substr( $num, $i, 1 ) . "{1}";
				
				$i++;
				
			}
			
			$where .= " AND `$table`.`$column` REGEXP '^.*$regexp.*\$'";
		
		}
		
	}

	//creation_date
	
 	$now = getdate();

	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;

	switch ($interval_select){
		    
		case 1:

			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			$where  .= " AND `repayments`.creation_date >='$Min_Time' AND `repayments`.creation_date <='$Now_Time' ";
			
			break;
		
		case 2:
			
			$yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now[ "year" ]--;

		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$where .= " AND `repayments`.creation_date >='$Min_Time' AND `repayments`.creation_date <'$Max_Time' ";
	     	break;
		     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "sp_bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "sp_bt_end" ] );
		 	
			$where .= " AND `repayments`.creation_date >= '$Min_Time' AND `repayments`.creation_date <= '$Max_Time' ";
			
	     	break;
		
	}

	//statut
	
	if( isset( $_POST[ "repaid" ] ) ){
	
		switch( $_POST[ "repaid" ] ){
		
			case "0" : 	$where .= " AND `repayments`.payment_date = '0000-00-00 00:00:00'"; break;
			case "1" : 	$where .= " AND `repayments`.payment_date <> '0000-00-00 00:00:00'"; break;
			case "" : 	
			default : break;
		}
			
	}
	
	//tri

	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderBy" ] ) );
	
	//nombre de pages

	$query = "
	SELECT COUNT( DISTINCT( repayments.idrepayment ) ) AS resultCount
	FROM $tables
	WHERE $where";
	
	$rs =& DBUtil::query( $query );
		
	$resultCount = $rs->fields( "resultCount" );
	
	?>
    <div class="mainContent fullWidthContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="topRight"></div>
		<div class="topLeft"></div>
	    <div class="content">
	    	<div class="headTitle">
	            <p class="title">Résultats de la recherche</p>
				<div class="rightContainer"></div>
	        </div>
	        <div class="subContent">
	        <?php
	        
				if( !$resultCount ){
					
					echo "La recherche n'a donné résultat";
					
									?>
									</div><!-- subContent -->
						</div><!-- content -->
				    	<div class="bottomRight"></div>
				    	<div class="bottomLeft"></div>
					</div><!-- mainContent -->
					<?php
	
					return;
					
				}

				if( $resultCount > $maxSearchResults ){
					
					//On insere dans la boite a outils de droite, dans la section message
					
										?>
										Plus de  <?php echo $maxSearchResults ?> litiges ont &eacute;t&eacute; trouv&eacute;s <br /> Merci de bien vouloir affiner votre recherche
									</div><!-- subContent -->
						</div><!-- content -->
				    	<div class="bottomRight"></div>
				    	<div class="bottomLeft"></div>
					</div><!-- mainContent -->
					<?php	
				
					return;
					
				}
	
				$pageCount = ceil( $resultCount / $resultPerPage );
				
				//recherche
				
				$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
				
				$start = ( $page - 1 ) * $resultPerPage;
				
				$query = "
				SELECT $select
				FROM $tables
				WHERE $where 
				ORDER BY $orderBy 
				LIMIT $start, $resultPerPage";

				$rs =& DBUtil::query( $query );

				?>
				<p class="tableHeaderLeft">
					<a href="#bas" class="goUpOrDown">
						Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a>
				</p>
				<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
                            	<th class="lefterCol">Com</th>
                            	<th>Facture n°</th>
                            	<th>Voir le litige</th>
                            	<th>N° Avoir</th>
                            	<th>Client n°</th>
                            	<th>Code postal</th>
                            	<th>Raison sociale</th>
                            	<th>Contact (nom)</th>
                            	<th>Date de création</th>
                            	<th>Montant</th>
                            	<th>Date de remboursement</th>
                            	<th class="righterCol"></th>
							</tr>
							 <!-- petites flèches de tri -->
                            <tr>
                            	<th class="noTopBorder">
                                	<a href="#" onclick="orderBy('user.initial ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('user.initial DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('credits.idbilling_buyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('credits.idbilling_buyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('credits.idcredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('credits.idcredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('contact.idbuyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('contact.idbuyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('buyer.zipcode ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('buyer.zipcode DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('buyer.company ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('buyer.company DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('contact.lastname ASC, contact.firstname ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('contact.lastname DESC, contact.firstname DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('repayments.creation_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('repayments.creation_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                 <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('repayments.amount ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('repayments.amount DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder">
                                	<a href="#" onclick="orderBy('repayments.payment_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
                                    <a href="#" onclick="orderBy('repayments.payment_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
                                </th>
                                <th class="noTopBorder"></th>
							</tr>
						</thead>
						<tbody>
						<?php
						
							while( !$rs->EOF() ){
								
								?>
								<tr>
									<td class="lefterCol"><?php echo $rs->fields( "initial" ) ?></td>
	                            	<td>
	                            		<a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?php echo $rs->fields( "idbilling_buyer" ) ?>" onclick="window.open( this.href ); return false;">
	                            			<?php echo $rs->fields( "idbilling_buyer" ) ?>
	                            		</a>
	                            	</td>
	                            	<td>
	                            		<a href="<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rs->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" target="_blank">
	                            			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="" />
	                            		</a>
	                            	</td>
	                            	<td>
	                            		<a href="<?php echo "/catalog/pdf_credit.php?idcredit=" . $rs->fields( "idcredit" ); ?>" onclick="window.open( this.href ); return false;">
	                            			<?php echo $rs->fields( "idbuyer" ) ?>
	                            		</a>
	                            	</td>
	                            	<td>
	                            		<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ) ?>" onclick="window.open( this.href ); return false;">
	                            			<?php echo $rs->fields( "idbuyer" ) ?>
	                            		</a>
	                            	</td>
	                            	<td><?php echo $rs->fields( "zipcode" ) ?></td>
	                            	<td><?php echo htmlentities( $rs->fields( "company" ) ) ?></td>
	                            	<td><?php echo DBUtil::getDBValue( "title$lang", "title", "idtitle", $rs->fields( "title" ) ) ?> <?php echo htmlentities( $rs->fields( "lastname" ) ) ?></td>
	                            	<td><?php echo $rs->fields( "creation_date" ) == "0000-00-00" ? "-" : Util::dateFormatEu( $rs->fields( "creation_date" ) ) ?></td>
	                            	<td style="text-align:right;"><?php echo Util::priceFormat( $rs->fields( "amount" ) ) ?></td>
	                            	<td>
									<input type="text" size="10" class="calendarInput" name="payment_date<?php echo $rs->fields( "idrepayment" ); ?>" id="payment_date<?php echo $rs->fields( "idrepayment" ); ?>" value="" />
	                            			<?php
	                            			
	                            			include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	                            			
	                            			DHTMLCalendar::calendar( "payment_date" . $rs->fields( "idrepayment" ) );
											?>
									
	                            	<?php /*
	                            	
	                            		if( !$rs->fields( "repaid" ) && $rs->fields( "credit_balance" ) >= $rs->fields( "amount" ) ){
	                            		
	                            			?>
	                            			<input type="text" size="10" class="calendarInput" name="payment_date<?php echo $rs->fields( "idrepayment" ); ?>" id="payment_date<?php echo $rs->fields( "idrepayment" ); ?>" value="" />
	                            			<?php
	                            			
	                            			include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	                            			
	                            			DHTMLCalendar::calendar( "payment_date" . $rs->fields( "idrepayment" ) );
	                            			
	                            		}
	                            		else if( $rs->fields( "payment_date" ) != "0000-00-00" )
	                            			echo Util::dateFormatEu( $rs->fields( "payment_date" ) );
	                            		else echo "-";
	                            		
	                            	*/?>
	                            	</td>
										<td>
	                            			<input type="button" class="blueButton" name="" value="Valider" onclick="repay(<?php echo $rs->fields( "idrepayment" ) ?>);" />
	                            												
	                            	<?php /*
	                            	
	                            		if( !$rs->fields( "repaid" ) && $rs->fields( "credit_balance" ) >= $rs->fields( "amount" ) ){
	                            			
	                            			?>
	                            			<input type="button" class="blueButton" name="" value="Valider" onclick="repay(<?php echo $rs->fields( "idrepayment" ) ?>);" />
	                            			<?php
	                            			
	                            		}
	                            		elseif( !$rs->fields( "repaid" ) ){
	                            			
	                            			?>
	                            			<p style="color:#FF0000; font-weight:bold;">solde crédité insuffisant</p>
	                            			<?php
	                            		
	                            		}
	                            		else echo "-";
	                            		*/
	                            	?>
	                            	</td>
								</tr>
								<?php
								
								$rs->MoveNext();
								
							}
							
						?>
						</tbody>
					</table>
					<p class="tableHeaderLeft">
			            <a href="#top" class="goUpOrDown">
			            	Aller en haut de page
			                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
			            </a>
					</p>
				</div><!-- resultTableContainer -->
			</div><!-- subContent -->
		</div><!-- content -->
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div><!-- mainContent -->
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

?>