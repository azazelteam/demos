﻿<?php
/*
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Balance des tiers
 */


//--------------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
include_once( "$GLOBAL_START_PATH/objects/AccountPlan.php" );
include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
include_once( "$GLOBAL_START_PATH/objects/URLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) && $_POST[ "search" ] == "1" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( ( !isset( $_POST[ "name" ] ) || empty( $_POST[ "name" ] ) ) && isset( $_POST[ "searchFor" ] ) && in_array( "customer", $_POST[ "searchFor" ] ) )
		searchCustomerResults();
	
	if( ( !isset( $_POST[ "idbuyer" ] ) || empty( $_POST[ "idbuyer" ] ) ) && ( !isset( $_POST[ "company" ] ) || empty( $_POST[ "company" ] ) ) && isset( $_POST[ "searchFor" ] ) && in_array( "supplier", $_POST[ "searchFor" ] ) )
		searchSupplierResults();
	
	//Added BY Behzad
	if( isset( $_POST[ "searchFor" ] ) && in_array( "provider", $_POST[ "searchFor" ] ) )
		searchProviderResults();
	/////////////////
	
	exit();
	
}

//------------------------------------------------------------------------------------------------
/* export clients */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) && isset( $_GET[ "customer" ] ) ){
	
	$req = $_GET[ "req" ];
	$exportableArray =& getCustomerExportableArray( $req );
	exportArray( $exportableArray );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------
/* export fournisseurs */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) && isset( $_GET[ "supplier" ] ) ){
	
	$req = $_GET[ "req" ];
	$exportableArray =& getSupplierExportableArray( $req );
	exportArray( $exportableArray );
	
	exit();
	
}

//--------------------------------------------------------------------------------------------------
/* export Provider  */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) && isset( $_GET["provider" ] ) ){
	
	$req = $_GET[ "req" ];
	$exportableArray =& getProviderExportableArray( $req );
	exportArray( $exportableArray );
	
	exit();
	
}
//--------------------------------------------------------------------------------------------------

$Title = "Balance des tiers";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------------

$lang = User::getInstance()->getLang();

?>
			<script type="text/javascript">
			function changeSupplier( element ){ $('#idsupplier').val( element.info ); }
			function changeProvider( element ){ $('#provider_search').val( element.info ); }

			
			</script>
<div id="globalMainContent">
	<div class="mainContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<?php searchForm(); ?>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<div id="tools" class="rightTools">
		<div class="toolBox">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Date d'exercices</p>
			</div>
			<div class="content" style="padding:5px;">
				<div id="fiscalYearsList"><?php AccountPlan::displayFiscalYears(); ?></div>
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
	</div>
	<div id="SearchResults"></div>
</div> <!-- GlobalMainContent -->
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function searchForm(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
	$maxDate = DBUtil::query( "SELECT MAX( start_date ) AS maxDate FROM fiscal_years" )->fields( "maxDate" );
	
	$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime( 0, 0, 0, 1, 1, date( "Y" ) ) ),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ),
		"searchFor"			=> array( "customer", "supplier", "provider" ),
		"idbuyer" 			=> "",
		"company"		 	=> "",
		"name"			 	=> "",
		"orderByCustomer"	=> "buyer.company ASC",
		"orderBySupplier"	=> "supplier.name ASC",
		"orderByProvider"	=> "provider.name ASC"
		
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
?>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
			<script type="text/javascript">
			
			/* <![CDATA[ */
				
				/* ----------------------------------------------------------------------------------- */
				
				$(document).ready(function() { 
					
					 var options = {
					 	
						beforeSubmit:  preSubmitCallback,  // pre-submit callback 
						success:	   postSubmitCallback  // post-submit callback 
		  				
					};
					

					$('#SearchForm').ajaxForm( options );
					
				});
				
				/* ----------------------------------------------------------------------------------- */
				
				function preSubmitCallback( formData, jqForm, options ){
					
					document.getElementById( 'SearchForm' ).elements[ 'search' ].value = '1';
					document.getElementById( 'SearchForm' ).elements[ 'export' ].value = '0';
					
					$.blockUI({
						
						message: "Recherche en cours",
						css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
						fadeIn: 0, 
						fadeOut: 700
						
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function postSubmitCallback( responseText, statusText ){ 
					
					$.unblockUI();
					
					if( statusText != 'success' || responseText == '0' ){
						
						alert( "Impossible d'effectuer la recherche" );
						return;
						
					}
					
					$('#SearchResults').html( responseText );
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function orderByCustomer( str ){
					
					document.getElementById( 'SearchForm' ).elements[ 'orderByCustomer' ].value = str;
					
					$('#SearchForm').ajaxSubmit({ 
						
						beforeSubmit:  preSubmitCallback,	//pre-submit callback
						success:	   postSubmitCallback	//post-submit callback
						
					});
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function orderBySupplier( str ){
					
					document.getElementById( 'SearchForm' ).elements[ 'orderBySupplier' ].value = str;
					
					$('#SearchForm').ajaxSubmit({ 
						
						beforeSubmit:  preSubmitCallback,	//pre-submit callback
						success:	   postSubmitCallback	//post-submit callback
						
					});
					
				}
				/* ----------------------------------------------------------------------------------- */
				
				function orderByProvider( str ){
					
					document.getElementById( 'SearchForm' ).elements[ 'orderByProvider' ].value = str;
					
					$('#SearchForm').ajaxSubmit({ 
						
						beforeSubmit:  preSubmitCallback,	//pre-submit callback
						success:	   postSubmitCallback	//post-submit callback
						
					});
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function toggleSubAccounts( main_account ){
					
					if( $( ".subAccount_" + main_account ).css( "display" ) == "none" ){
						
						$( ".subAccount_" + main_account ).show();
						$( "#account_" + main_account ).addClass( "openedAccount" );
						
					}else{
						
						$( ".subAccount_" + main_account ).hide();
						$( "#account_" + main_account ).removeClass( "openedAccount" );
						
					}
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function showSupplierInfos( idsupplier){
					
					if( idsupplier == '0' )
						return;
					
					$.ajax({
					 	
						url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
						async: false,
					 	success: function(msg){
					 		
							$.blockUI({
								
								message: msg,
								fadeIn: 700, 
			            		fadeOut: 700,
								css: {
									width: '700px',
									top: '0px',
									left: '50%',
									'margin-left': '-350px',
									'margin-top': '50px',
									padding: '5px', 
									cursor: 'help',
									'-webkit-border-radius': '10px', 
					                '-moz-border-radius': '10px',
					                'background-color': '#FFFFFF',
					                'font-size': '11px',
					                'font-family': 'Arial, Helvetica, sans-serif',
					                'color': '#44474E'
								 }
								 
							}); 
							
							$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
							
						}
						
					});
					
				}
				//Added by behzad
				function showProviderInfos( idprovider){
					
					if( idprovider == '0' )
						return;
					
					$.ajax({
					 	
						url: "<?php echo $GLOBAL_START_URL ?>/provider/infos.php?idprovider=" + idprovider,
						async: false,
					 	success: function(msg){
					 		
							$.blockUI({
								
								message: msg,
								fadeIn: 700, 
			            		fadeOut: 700,
								css: {
									width: '700px',
									top: '0px',
									left: '50%',
									'margin-left': '-350px',
									'margin-top': '50px',
									padding: '5px', 
									cursor: 'help',
									'-webkit-border-radius': '10px', 
					                '-moz-border-radius': '10px',
					                'background-color': '#FFFFFF',
					                'font-size': '11px',
					                'font-family': 'Arial, Helvetica, sans-serif',
					                'color': '#44474E'
								 }
								 
							}); 
							
							$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
							
						}
						
					});
					
				}
				/* ----------------------------------------------------------------------------------- */
				
			/* ]]> */
			</script>
			<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				
			/* ]]> */
			</style>
			<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
				<div class="headTitle">
					<p class="title">Balance des tiers</p>
					<div class="rightContainer">
						<label style="vertical-align:top;"><input type="checkbox" name="searchFor[]" id="searchForCustomer" value="customer" style="vertical-align:middle;"<?php if( in_array( "customer", $postData[ "searchFor" ] ) ) echo " checked=\"checked\""; ?> /> Clients</label>
						<label style="vertical-align:top;"><input type="checkbox" name="searchFor[]" id="searchForSupplier" value="supplier" style="vertical-align:middle;"<?php if( in_array( "supplier", $postData[ "searchFor" ] ) ) echo " checked=\"checked\""; ?> /> Fournisseurs</label>
						<label style="vertical-align:top;"><input type="checkbox" name="searchFor[]" id="searchForProvider" value="provider" style="vertical-align:middle;"<?php if( in_array( "provider", $postData[ "searchFor" ] ) ) echo " checked=\"checked\""; ?> /> Fournisseurs de service</label>
					</div>
				</div>
				<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />
					<input type="hidden" name="orderByCustomer" value="<?php echo $postData[ "orderByCustomer" ] ?>" />
					<input type="hidden" name="orderBySupplier" value="<?php echo $postData[ "orderBySupplier" ] ?>" />
					<input type="hidden" name="orderByProvider" value="<?php echo $postData[ "orderByProvider" ] ?>" />
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell">Entre le</th>
								<td>
									<input type="text" name="start_date" id="start_date" class="calendarInput" value="<?php echo $postData[ "start_date" ] ?>" />
									<?php echo DHTMLCalendar::calendar( "start_date" ) ?>
									<!--<input type="hidden" name="start_date" id="start_date" class="calendarInput" value="<?php echo $postData[ "start_date" ] ?>" />-->
								</td>
								<th class="filledCell">et le</th>
								<td>
									<input type="text" name="end_date" id="end_date" class="calendarInput" value="<?php echo $postData[ "end_date" ] ?>" />
									<?php echo DHTMLCalendar::calendar( "end_date" ) ?>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell">Client n°</th>
								<td><input type="text" name="idbuyer" class="textInput" value="<?php echo $postData[ "idbuyer" ] ?>" onkeyup="if(this.value!='')document.getElementById('searchForCustomer').checked=true;" onkeypress="if(this.value!='')document.getElementById('searchForCustomer').checked=true;" /></td>
								<th class="filledCell">Fournisseur</th>
								<td>
									<input type="text" name="name" class="textInput" value="<?php echo $postData[ "name" ] ?>" id="supplier_search" class="textInput" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" />
									<!--<input type="hidden" name="idsupplier" id="idsupplier" />-->
									<?php AutoCompletor::completeFromFile( "supplier_search", "$GLOBAL_START_URL/xml/autocomplete/supplier.php", 5, "changeSupplier" ); ?>
								</td>
							</tr>
							<tr>
								<th class="filledCell">Nom / Raison sociale</th>
								<td><input type="text" name="company" class="textInput" value="<?php echo $postData[ "company" ] ?>" onkeyup="if(this.value!='')document.getElementById('searchForCustomer').checked=true;" onkeypress="if(this.value!='')document.getElementById('searchForCustomer').checked=true;" /></td>
								<th class="filledCell">Fournisseur de service</th>
								<td colspan="2">
									<input type="text" name="idprovider"  id="idprovider"  value="<?php echo $postData[ "idprovider" ] ?>" class="textInput" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" />
									<input type="hidden" name="provider_search" id="provider_search" />
									<?php AutoCompletor::completeFromFile( "idprovider", "$GLOBAL_START_URL/xml/autocomplete/provider.php", 5, "changeProvider" ); ?>
								</td>
							</tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" value="Rechercher" />
					</div>
				</div>
				<input type="hidden" name="detail" id="detail" value="1" />
			</form>
<?php
	
}

//------------------------------------------------------------------------------------------------

function searchCustomerResults(){
	
	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$maxSearchResults = 2500;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	//dates
	
	$maxDate = DBUtil::query( "SELECT MAX( start_date ) AS maxDate FROM fiscal_years" )->fields( "maxDate" );
	
	//@todo: ces dates ne sont pas utilisées. Voir comment les utiliser
 	$start_date = euDate2us( $_POST[ "start_date" ] );
 	$end_date = euDate2us( $_POST[ "end_date" ] );
	
	$select = "
	account_plan.idrelation,
	buyer.company,
	SUM( ( date < '$maxDate' ) * ( account_plan.amount_type = 'credit' ) * account_plan.amount ) AS previousCredit,
	SUM( ( date < '$maxDate' ) * ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS previousDebit,
	SUM( ( date >= '$maxDate' ) * ( account_plan.amount_type = 'credit' ) * account_plan.amount ) AS currentCredit,
	SUM( ( date >= '$maxDate' ) * ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS currentDebit,
	SUM( ( account_plan.amount_type = 'credit' ) * account_plan.amount - ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS balance";
	
	$tables = "account_plan, buyer";
	
	$where = "account_plan.relation_type = 'buyer'
	AND account_plan.idrelation = buyer.idbuyer
	AND (   ( SUBSTRING( account_plan.idaccount_plan, 1, 3 ) = '411') OR (SUBSTRING( account_plan.idaccount_plan, 1, 3 ) = '416')       )
	AND action NOT LIKE '%forward_carry'";
	
	//post data
	
	//nom client
	
	if( isset( $_POST[ "company" ] ) && strlen( $_POST[ "company" ] ) )
		$where .= "\n\tAND buyer.company LIKE '%" . Util::html_escape( $_POST[ "company" ] ) . "%'";
	
	//numéro de client
	
	if( isset( $_POST[ "idbuyer" ] ) && intval( $_POST[ "idbuyer" ] ) )
		$where .= "\n\tAND account_plan.idrelation = " . intval( $_POST[ "idbuyer" ] );
	
	//tri
	
	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderByCustomer" ] ) );
	
	//requete
	
	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		GROUP BY account_plan.idrelation
		HAVING balance != 0
		ORDER BY $orderBy";
	
	$rs =& DBUtil::query( $query );
	
	$resultCount = $rs->RecordCount();
	
?>
	 <div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php
				
				if( $resultCount > 0 ){
					
					$totalDebit = 0;
					$totalCredit = 0;
					$totalCurrentDebit = 0;
					$totalCurrentCredit = 0;
					$totalPreviousDebit = 0;
					$totalPreviousCredit = 0;
					
					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
					
					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
					
					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/balance.php?export&amp;customer&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php } ?>
				<p class="title">Balance des tiers clients</p>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ ?>Aucune ligne trouvée
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> lignes ont été trouvées<br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/balance.php?export&amp;customer&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th rowspan="2" style="width:10%;">Client n°</th>
								<th rowspan="2" style="width:30%;">Nom du client</th>
								<th colspan="2" class="filledCell">Mouvements au <?php echo Util::dateFormatEu(  DateUtil::getPreviousDay( $maxDate ) ) ?></th>
								<th colspan="2" class="filledCell">Mouvements</th>
								<th colspan="2" class="filledCell">Soldes cumulés</th>
							</tr>
							<tr>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
							</tr>
							<tr>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('buyer.idbuyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('buyer.idbuyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('buyer.company ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('buyer.company DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('previousDebit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('previousDebit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('previousCredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('previousCredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('currentDebit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('currentDebit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('currentCredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('currentCredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('balance DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('balance ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByCustomer('balance ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByCustomer('balance DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php while( !$rs->EOF ){ ?>
							<tr>
								<td class="lefterCol"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idrelation" ) ?>" onclick="window.open(this.href); return false;"><?php echo $rs->fields( "idrelation" ) ?></a></td>
								<td class="grasBack"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idrelation" ) ?>" onclick="window.open(this.href); return false;"><?php echo $rs->fields( "company" ) == "" ? AccountPlan::getCustomerName( $rs->fields( "idrelation" ) ) : $rs->fields( "company" ) ?></a></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "previousDebit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "previousDebit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "previousCredit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "previousCredit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "currentDebit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "currentDebit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "currentCredit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "currentCredit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "balance" ) < 0 ? Util::priceFormat( abs( $rs->fields( "balance" ) ) ) : "" ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "balance" ) > 0 ? Util::priceFormat( $rs->fields( "balance" ) ) : "" ?></td>
							</tr>
							<?php
								
								if( $rs->fields( "balance" ) < 0 )
									$totalDebit				-= $rs->fields( "balance" );
								else
									$totalCredit			+= $rs->fields( "balance" );
								
								$totalCurrentDebit		+= $rs->fields( "currentDebit" );
								$totalCurrentCredit		+= $rs->fields( "currentCredit" );
								$totalPreviousDebit		+= $rs->fields( "previousDebit" );
								$totalPreviousCredit	+= $rs->fields( "previousCredit" );
								
								$rs->MoveNext();
								
							}
							
							?>
							<tr>
								<th colspan="2" style="border-style:none;"></th>
								<th class="totalAmount"><?php echo $totalPreviousDebit == 0 ? "" : Util::priceFormat( $totalPreviousDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalPreviousCredit == 0 ? "" : Util::priceFormat( $totalPreviousCredit ) ?></th>
								<th class="totalAmount"><?php echo $totalCurrentDebit == 0 ? "" : Util::priceFormat( $totalCurrentDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalCurrentCredit == 0 ? "" : Util::priceFormat( $totalCurrentCredit ) ?></th>
								<th class="totalAmount"><?php echo $totalDebit == 0 ? "" : Util::priceFormat( $totalDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalCredit == 0 ? "" : Util::priceFormat( $totalCredit ) ?></th>
							</tr>
						</tbody>
					</table>
				</div><?php } ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
<?php
	
}

//------------------------------------------------------------------------------------------------

function searchSupplierResults(){
	
	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$maxSearchResults = 2500;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	//dates
	
	$maxDate = DBUtil::query( "SELECT MAX( start_date ) AS maxDate FROM fiscal_years" )->fields( "maxDate" );
	
	//@todo: ces dates ne sont pas utilisées. Voir comment les utiliser
 	$start_date = euDate2us( $_POST[ "start_date" ] );
 	$end_date = euDate2us( $_POST[ "end_date" ] );
	
	$select = "
	account_plan.idrelation,
	supplier.name,
	SUM( ( date < '$maxDate' ) * ( account_plan.amount_type = 'credit' ) * account_plan.amount ) AS previousCredit,
	SUM( ( date < '$maxDate' ) * ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS previousDebit,
	SUM( ( date >= '$maxDate' ) * ( account_plan.amount_type = 'credit' ) * account_plan.amount ) AS currentCredit,
	SUM( ( date >= '$maxDate' ) * ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS currentDebit,
	SUM( ( account_plan.amount_type = 'credit' ) * account_plan.amount - ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS balance";
	
	$tables = "account_plan, supplier";
	
	$where = "account_plan.relation_type = 'supplier'
	AND account_plan.idrelation = supplier.idsupplier
	AND SUBSTRING( account_plan.idaccount_plan, 1, 3 ) = '401'
	AND action NOT LIKE '%forward_carry'";
	
	//post data
	
	//nom fournisseur
	
	if( isset( $_POST[ "name" ] ) && strlen( $_POST[ "name" ] ) )
		$where .= "\n\tAND supplier.name LIKE '%" . Util::html_escape( $_POST[ "name" ] ) . "%'";
	
	//tri
	
	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderBySupplier" ] ) );
	
	//requete
	
	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		GROUP BY account_plan.idrelation
		HAVING balance != 0
		ORDER BY $orderBy";
	
	$rs =& DBUtil::query( $query );
	
	$resultCount = $rs->RecordCount();
	
?>
	 <div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php
				
				if( $resultCount > 0 ){
					
					$totalDebit = 0;
					$totalCredit = 0;
					$totalCurrentDebit = 0;
					$totalCurrentCredit = 0;
					$totalPreviousDebit = 0;
					$totalPreviousCredit = 0;
					
					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
					
					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
					
					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/balance.php?export&amp;supplier&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php } ?>
				<p class="title">Balance des tiers fournisseurs</p>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ ?>Aucune ligne trouvée
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> lignes ont été trouvées<br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/balance.php?export&amp;supplier&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th rowspan="2" style="width:40%;">Nom du fournisseur</th>
								<th colspan="2" class="filledCell">Mouvements au <?php echo Util::dateFormatEu(  DateUtil::getPreviousDay( $maxDate ) ) ?></th>
								<th colspan="2" class="filledCell">Mouvements</th>
								<th colspan="2" class="filledCell">Soldes cumulés</th>
							</tr>
							<tr>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
							</tr>
							<tr>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('supplier.name ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('supplier.name DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('previousDebit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('previousDebit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('previousCredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('previousCredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('currentDebit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('currentDebit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('currentCredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('currentCredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('balance DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('balance ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderBySupplier('balance ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBySupplier('balance DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php while( !$rs->EOF ){ ?>
							<tr>
								<td class="lefterCol grasBack"><a href="#" onclick="showSupplierInfos(<?php echo $rs->fields( "idrelation" ) ?>); return false;"><?php echo $rs->fields( "name" ) ?></a></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "previousDebit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "previousDebit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "previousCredit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "previousCredit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "currentDebit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "currentDebit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "currentCredit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "currentCredit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "balance" ) < 0 ? Util::priceFormat( abs( $rs->fields( "balance" ) ) ) : "" ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "balance" ) > 0 ? Util::priceFormat( $rs->fields( "balance" ) ) : "" ?></td>
							</tr>
							<?php
								
								if( $rs->fields( "balance" ) < 0 )
									$totalDebit			-= $rs->fields( "balance" );
								else
									$totalCredit		+= $rs->fields( "balance" );
								
								$totalCurrentDebit		+= $rs->fields( "currentDebit" );
								$totalCurrentCredit		+= $rs->fields( "currentCredit" );
								$totalPreviousDebit		+= $rs->fields( "previousDebit" );
								$totalPreviousCredit	+= $rs->fields( "previousCredit" );
								
								$rs->MoveNext();
								
							}
							
							?>
							<tr>
								<th style="border-style:none;"></th>
								<th class="totalAmount"><?php echo $totalPreviousDebit == 0 ? "" : Util::priceFormat( $totalPreviousDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalPreviousCredit == 0 ? "" : Util::priceFormat( $totalPreviousCredit ) ?></th>
								<th class="totalAmount"><?php echo $totalCurrentDebit == 0 ? "" : Util::priceFormat( $totalCurrentDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalCurrentCredit == 0 ? "" : Util::priceFormat( $totalCurrentCredit ) ?></th>
								<th class="totalAmount"><?php echo $totalDebit == 0 ? "" : Util::priceFormat( $totalDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalCredit == 0 ? "" : Util::priceFormat( $totalCredit ) ?></th>
							</tr>
						</tbody>
					</table>
				</div><?php } ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
<?php
	
}


//------------------------------------------------------------------------------------------------
// Added By Behzad
function searchProviderResults(){
	
	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$maxSearchResults = 2500;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	//dates
	
	$maxDate = DBUtil::query( "SELECT MAX( start_date ) AS maxDate FROM fiscal_years" )->fields( "maxDate" );
	
	//@todo: ces dates ne sont pas utilisées. Voir comment les utiliser
 	$start_date = euDate2us( $_POST[ "start_date" ] );
 	$end_date = euDate2us( $_POST[ "end_date" ] );
	
	$select = "
	account_plan.idrelation,
	provider.name,
	SUM( ( date < '$maxDate' ) * ( account_plan.amount_type = 'credit' ) * account_plan.amount ) AS previousCredit,
	SUM( ( date < '$maxDate' ) * ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS previousDebit,
	SUM( ( date >= '$maxDate' ) * ( account_plan.amount_type = 'credit' ) * account_plan.amount ) AS currentCredit,
	SUM( ( date >= '$maxDate' ) * ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS currentDebit,
	SUM( ( account_plan.amount_type = 'credit' ) * account_plan.amount - ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS balance";
	
	$tables = "account_plan, provider";
	
	$where = "account_plan.relation_type = 'provider'
	AND account_plan.idrelation = provider.idprovider
	AND SUBSTRING( account_plan.idaccount_plan, 1, 3 ) = '401'
	AND action NOT LIKE '%forward_carry'";
	
	//post data
	
	//nom fournisseur
	
	if( isset( $_POST[ "idprovider" ] ) && strlen( $_POST[ "idprovider" ] ) )
		$where .= "\n\tAND provider.name LIKE '%" . Util::html_escape( $_POST[ "idprovider" ] ) . "%'";
	
	//tri
	
	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderByProvider" ] ) );
	
	//requete
	
	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		GROUP BY account_plan.idrelation
		HAVING balance != 0
		ORDER BY $orderBy";
	//echo $query;
	$rs =& DBUtil::query( $query );
	
	$resultCount = $rs->RecordCount();
	
?>
	 <div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php
				
				if( $resultCount > 0 ){
					
					$totalDebit = 0;
					$totalCredit = 0;
					$totalCurrentDebit = 0;
					$totalCurrentCredit = 0;
					$totalPreviousDebit = 0;
					$totalPreviousCredit = 0;
					
					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
					
					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
					
					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/balance.php?export&amp;provider&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php } ?>
				<p class="title">Balance des tiers fournisseurs de service</p>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ ?>Aucune ligne trouvée
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> lignes ont été trouvées<br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/balance.php?export&amp;provider&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th rowspan="2" style="width:40%;">Nom du fournisseur</th>
								<th colspan="2" class="filledCell">Mouvements au <?php echo Util::dateFormatEu(  DateUtil::getPreviousDay( $maxDate ) ) ?></th>
								<th colspan="2" class="filledCell">Mouvements</th>
								<th colspan="2" class="filledCell">Soldes cumulés</th>
							</tr>
							<tr>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
								<th style="width:10%;">Débit</th>
								<th style="width:10%;">Crédit</th>
							</tr>
							<tr>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('provider.name ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('provider.name DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('previousDebit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('previousDebit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('previousCredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('previousCredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('currentDebit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('currentDebit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('currentCredit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('currentCredit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('balance DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('balance ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder">
									<a href="#" onclick="orderByProvider('balance ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderByProvider('balance DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php while( !$rs->EOF ){ ?>
							<tr>
								<td class="lefterCol grasBack"><a href="#" onclick="showProviderInfos(<?php echo $rs->fields( "idrelation" ) ?>); return false;"><?php echo $rs->fields( "name" ) ?></a></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "previousDebit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "previousDebit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "previousCredit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "previousCredit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "currentDebit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "currentDebit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "currentCredit" ) == 0 ? "" : Util::priceFormat( $rs->fields( "currentCredit" ) ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "balance" ) < 0 ? Util::priceFormat( abs( $rs->fields( "balance" ) ) ) : "" ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "balance" ) > 0 ? Util::priceFormat( $rs->fields( "balance" ) ) : "" ?></td>
							</tr>
							<?php
								
								if( $rs->fields( "balance" ) < 0 )
									$totalDebit			-= $rs->fields( "balance" );
								else
									$totalCredit		+= $rs->fields( "balance" );
								
								$totalCurrentDebit		+= $rs->fields( "currentDebit" );
								$totalCurrentCredit		+= $rs->fields( "currentCredit" );
								$totalPreviousDebit		+= $rs->fields( "previousDebit" );
								$totalPreviousCredit	+= $rs->fields( "previousCredit" );
								
								$rs->MoveNext();
								
							}
							
							?>
							<tr>
								<th style="border-style:none;"></th>
								<th class="totalAmount"><?php echo $totalPreviousDebit == 0 ? "" : Util::priceFormat( $totalPreviousDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalPreviousCredit == 0 ? "" : Util::priceFormat( $totalPreviousCredit ) ?></th>
								<th class="totalAmount"><?php echo $totalCurrentDebit == 0 ? "" : Util::priceFormat( $totalCurrentDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalCurrentCredit == 0 ? "" : Util::priceFormat( $totalCurrentCredit ) ?></th>
								<th class="totalAmount"><?php echo $totalDebit == 0 ? "" : Util::priceFormat( $totalDebit ) ?></th>
								<th class="totalAmount"><?php echo $totalCredit == 0 ? "" : Util::priceFormat( $totalCredit ) ?></th>
							</tr>
						</tbody>
					</table>
				</div><?php } ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
<?php
	
}
//---------------------------------------------------------------------------------------------

function getCustomerExportableArray( $req ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "req" ] ), $GLOBAL_DB_PASS );
	
	$data = array(
		
		"id"		=> array(),
		"name"		=> array(),
		"debit"		=> array(),
		"credit"	=> array()
		
	);
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$data[ "id" ][]		= $rs->fields( "idrelation" );
		$data[ "name" ][]	= $rs->fields( "company" );
		$data[ "debit" ][]	= $rs->fields( "balance" ) < 0 ? abs( $rs->fields( "balance" ) ) : "";
		$data[ "credit" ][]	= $rs->fields( "balance" ) > 0 ? abs( $rs->fields( "balance" ) ) : "";
		
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//---------------------------------------------------------------------------------------------

function getSupplierExportableArray( $req ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "req" ] ), $GLOBAL_DB_PASS );
	echo $query;
	$data = array(
		
		"id"		=> array(),
		"name"		=> array(),
		"debit"		=> array(),
		"credit"	=> array()
		
	);
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		$data[ "id" ][]		= $rs->fields( "idrelation" );
		$data[ "name" ][]	= $rs->fields( "name" );
		$data[ "debit" ][]	= $rs->fields( "balance" ) < 0 ? abs( $rs->fields( "balance" ) ) : "";
		$data[ "credit" ][]	= $rs->fields( "balance" ) > 0 ? abs( $rs->fields( "balance" ) ) : "";
		
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------


function getProviderExportableArray( $req ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "req" ] ), $GLOBAL_DB_PASS );
	
	$data = array(
		
		"id"		=> array(),
		"name"		=> array(),
		"debit"		=> array(),
		"credit"	=> array()
		
	);
	
	$rs =& DBUtil::query( $query );
	while( !$rs->EOF() ){
		
		$data[ "id" ][]		= $rs->fields( "idrelation" );
		$data[ "name" ][]	= $rs->fields( "name" );
		$data[ "debit" ][]	= $rs->fields( "balance" ) < 0 ? abs( $rs->fields( "balance" ) ) : "";
		$data[ "credit" ][]	= $rs->fields( "balance" ) > 0 ? abs( $rs->fields( "balance" ) ) : "";
		
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------


function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray, "balance_tiers", "csv", ";", "", "\r\n" );
	//$cvsExportArray->hideHeaders();
	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------

?>