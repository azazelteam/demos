<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Reglement fournisseur partiel
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/RegulationsSupplier.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
if( !isset( $_GET[ "irs" ] ) || empty( $_GET[ "irs" ] ) ){
	
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

$idregulations_supplier = $_GET["irs"];

$today = date("Y-m-d");

$regulations = new RegulationsSupplier( $idregulations_supplier );

//Numéro(s) de facture(s) concernée(s) par le règlement
$bil = $regulations->getBillings();

$billing_supplier = '';
$billings = array();

for($i=0;$i<$bil->RecordCount();$i++){

	if($i)
		$billing_supplier .= "-";
	
	$billing_supplier .= $bil->fields("billing_supplier");
	$billings[] = $bil->fields("billing_supplier");
}

$title = "Facture : <b>".$billing_supplier."</b>";
 
//------------------------------------------------------------------------------------------------
//Traitement du formulaire

$error = 0;
$Msgerror = '';

if ( isset($_POST[ "register" ]) || isset($_POST[ "register_x" ]) ){
		
	$fields = array();
	
	$fields[] = "deliv_payment" ;
	$fields[] = "payment_date" ;
	
	//Test des champs postés
	
	for ($i=0;$i<count($fields);$i++){
		//Ne pas tester le dernier champ qui est calculé
		if( empty( $_POST[ $fields[$i] ] ) ){
			echo $_POST[ $fields[$i]];
			$error = 1;
		}
	}
	
	if ( $error == 1 ){
		$Msgerror = "Tous les champs doivent être remplis";	
	}
	
	if( $error == 0 ) {
		
		//Toutes les données sont ok, on peut traiter l'enregistrement du paiement
		
		$payment_date = $_POST[ "payment_date" ];
		$deliv_payment =  $_POST[ "deliv_payment" ];
		$idpayment = $_POST[ "idpayment" ];
			
		//accept
		$auj = date("d-m-Y");
			
		if(	$auj >= $payment_date) {
			$accept = 1;
		}else{
			$accept = 0;
		}
		
		//On affecte les valeurs
		$regulations->set( "payment_date" , euDate2us($payment_date) );
		$regulations->set( "deliv_payment" , euDate2us($deliv_payment) );
		$regulations->set( "idpayment" , $idpayment );
		$regulations->set( "accept" , $accept );
			
		//On sauvegarde
		$regulations->update();
			
		UpdateBillingsStatus( $billings );
		
	
	?>
		<script type="text/javascript" language="javascript">
			<!--
			alert ("Paiement enregistré avec succès !");
			opener.document.location = '<?=$GLOBAL_START_URL?>/accounting/supplier_invoices_to_pay.php';
			window.close();
			-->
		</script>
	<?php  
	}
}  

?>
<script type="text/javascript" language="javascript">
<!--
function testform(){
	
	if( frm.deliv_payment.value == "" ) {
		alert("Vous devez saisir une date d'échéance pour le règlement !");
		return false;
	}
	
	if( frm.payment_date.value == "" ) {
		alert("Vous devez saisir une date de paiement pour le règlement !");
		return false;
	}
	
	return true;
}
// -->
</script>
<style type="text/css">
<!--
div#global{
	min-width:0;
	width:auto;
}
-->
</style>
<div id="globalMainContent" style="width:380px;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent fullWidthContent" style="width:380px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Enregistrement du paiement</p>
			</div>
			<div class="subContent">
				<div><?php echo $title ?></div>
				<?php if( $error == 1 ){ ?><br /><span class="msg"><?=$Msgerror?></span><?php } ?>
				<?php displayPaiementForm( $regulations ); ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------------
function getSupplierName( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du fournisseur" );
		
	return $rs->fields( "name" );
	
}


//------------------------------------------------------------------------------------------------------
function getAmountFromBillings( $bil ){
	
	$db = &DBUtil::getConnection();
	
	$total_amount=0;
	
	if( count($bil) == 1 ){
		
		$billing_supplier = $bil[0];
		
		//1 seule facture sélectionnée on récupère le montant TTC
		$query = "SELECT total_amount FROM billing_supplier WHERE billing_supplier = '$billing_supplier'";
		$rs = $db->Execute( $query );
		
		if( $rs === false || !$rs->RecordCount() )
			die( "Impossible de récupérer le total de la facture" );
			
		$total_amount = $rs->fields("total_amount");
		
	}else{
		
		for($i=0;$i<count($bil);$i++){
			
			$billing_supplier = $bil[$i];
			
			$query = "SELECT total_amount FROM billing_supplier WHERE billing_supplier = '$billing_supplier'";
			$rs = $db->Execute( $query );
		
			if( $rs === false || !$rs->RecordCount() )
				die( "Impossible de récupérer le total des factures" );
				
			$total_amount+=$rs->fields("total_amount");
		}
	}	
	
	return $total_amount;
	
}

//------------------------------------------------------------------------------------------------------
function UpdateBillingsStatus( $bil ){
	
	$db = &DBUtil::getConnection();
	
	for($i=0;$i<count($bil);$i++){
	
		$billing_supplier = $bil[$i];
			
		//On sélectionne les paiements de la facture
		$Query = "SELECT rs.accept FROM regulations_supplier rs, billing_regulations_supplier bsr WHERE bsr.billing_supplier = '$billing_supplier'
				AND bsr.idregulations_supplier = rs.idregulations_supplier";
		$rs = $db->Execute( $Query );
			
		if( $rs === false )
			die( "Impossible de récupérer le status des règlements" );
	
		
		if($rs->RecordCount()>0){
			
			$paid = 1;
			
			for($i=0;$i<$rs->RecordCount();$i++){
				
				$accept = $rs->fields("accept");
				
				if(!$accept)
					$paid = 0 ;
			}	
			
			if($paid) {
				
				//On met à jour la facture
				$query = "UPDATE billing_supplier SET status='Paid' WHERE billing_supplier = '$billing_supplier'";
				$rs = $db->Execute( $query );
		
				if( $rs === false )
					die( "Impossible de mettre à jour le statut des factures fournisseurs" );
			
			}
		}
	}	
}

//------------------------------------------------------------------------------------------------------
function displayPaiementForm( RegulationsSupplier &$regulations ){
	
	global $GLOBAL_START_URL;
	
	$today = date("Y-m-d");
	
	$idregulations_supplier = $regulations->get("idregulations_supplier");
	$deliv_payment = isset($_POST["deliv_payment"]) ? $_POST["deliv_payment"] : usDate2Eu($regulations->get("deliv_payment"));
	$payment_date = isset($_POST["payment_date"]) ? $_POST["payment_date"] : usDate2Eu($regulations->get("payment_date"));
	$amount = isset($_POST["amount"]) ? $_POST["amount"] : $regulations->get("amount");
	$idpayment = isset($_POST["idpayment"]) ? $_POST["idpayment"] : $regulations->get("idpayment");
	
	if( $payment_date == '00-00-0000')
			$payment_date = '';
	
	if( $deliv_payment == '00-00-0000')
			$deliv_payment = '';	
			
?>
				<form name="frm" method="POST" action="<?=$GLOBAL_START_URL?>/accounting/register_partially_payment.php?irs=<?=$idregulations_supplier?>" onsubmit="return testform();">
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th>Date d'échéance <span class="asterix">*</span></th>
								<td>
									<input type="text" name="deliv_payment" id="deliv_payment" value="<?=$deliv_payment?>" class="calendarInput" />
									<?php DHTMLCalendar::calendar( "deliv_payment" ); ?>
								</td>
							</tr>
							<tr>
								<th>Date de paiement <span class="asterix">*</span></th>
								<td>
									<input type="text" name="payment_date" id="payment_date" value="<?=$payment_date?>" class="calendarInput" />
									<?php DHTMLCalendar::calendar( "payment_date" ); ?>
								</td>
							</tr>
							<tr>
								<th>Mode de paiement <span class="asterix">*</span></th>
								<td><?=paymentList($idpayment)?></td>
							</tr>
							<tr>
								<th>Montant TTC <span class="asterix">*</span></th>
								<td><input type="text" name="amount" value="<?=$amount;?>" disabled="disabled" class="textInput" /></td>
							</tr>
						</table>
					</div>
					<div style="margin-top:-10px;">Les champs marqués par <span class="asterix">*</span> sont obligatoires</div>
					<div class="submitButtonContainer">
						<input type="submit" name="register" value="Enregistrer" class="blueButton" />
					</div>
				</form>
<?php
	
}

//-----------------------------------------------------------------------------------------
function paymentList( $selected_value ){
	
	$lang = "_1";

	echo "
	<select name=\"idpayment\">\n";
	
	$rs =& DBUtil::query("SELECT idpayment, name_1 FROM payment ORDER BY name_1 ASC");
	
	if($rs->RecordCount()>0){
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			$selected = '';
			
			if( $rs->Fields( "idpayment" ) == $selected_value )
				$selected = " selected=\"selected\"";
			else $selected = "";
		
			
			echo "
			<option value=\"" . $rs->Fields( "idpayment" ) . "\"$selected>" . $rs->Fields( "name_1" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}
?>