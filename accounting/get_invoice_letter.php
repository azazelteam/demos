<?php 

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Courrier mise en demeure client
 */
 
include_once('../objects/classes.php');

if( !isset($_GET["idbilling"]) || empty( $_GET["idbilling"] ) )
	die( "Paramètre absent" );

$idbilling_buyer = $_GET["idbilling"];

$bdd =& DBUtil::getConnection();

//fichier modèle
$doc = $GLOBAL_START_PATH."/templates/accounting/word/mise_en_demeure.rtf";

if (file_exists($doc)) {
	
	//On ouvre le fichier
	$datas = fread(fopen($doc, "r"), filesize($doc));
	
	//On récupère les infos de la facture
	$query = "SELECT idbilling_buyer, DateHeure, deliv_payment, total_amount
			FROM billing_buyer
			WHERE idbilling_buyer=$idbilling_buyer";
	$rs = $bdd->Execute($query);
	
	if( $rs===false )
		die ( "Impossible de récupérer les informations du courier de relance" );
			
	//On initialise les variables à changer dans le template
	
	//Général
	//Date du document
	$date = date("Y-m-d");
	$creation_Date = @explode( "-", $date );
	
	@list( $year, $month, $day ) = $creation_Date;
	
	setlocale( LC_TIME, "fr_FR.utf8" );
	
	$timestamp = mktime( 0, 0, 0, $month, $day, $year );
	$strday 	= strftime("%A", $timestamp );
	$day 		= strftime("%d", $timestamp );
	$month 		= strftime("%B", $timestamp );
	$year 		= strftime("%Y", $timestamp );
	
	$today = $day." ".$month." ".$year;
	
	//Date de la facture
	$date = $rs->fields("DateHeure");
	@list($datedev,$heuredev)= @explode(" ", $date );
	$creation_Date = @explode( "-", $datedev );
	
	
	@list( $year, $month, $day ) = $creation_Date;
	
	setlocale( LC_TIME, "fr_FR.utf8" );
	
	$timestamp = mktime( 0, 0, 0, $month, $day, $year );
	$strday 	= strftime("%A", $timestamp );
	$day 		= strftime("%d", $timestamp );
	$month 		= strftime("%B", $timestamp );
	$year 		= strftime("%Y", $timestamp );
	
	$billing_date = $day." ".$month." ".$year;

	//Date d'échéance
	
	$date = $rs->fields("deliv_payment");
	$creation_Date = @explode( "-", $date );
	
	
	@list( $year, $month, $day ) = $creation_Date;
	
	setlocale( LC_TIME, "fr_FR.utf8" );
	
	$timestamp = mktime( 0, 0, 0, $month, $day, $year );
	$strday 	= strftime("%A", $timestamp );
	$day 		= strftime("%d", $timestamp );
	$month 		= strftime("%B", $timestamp );
	$year 		= strftime("%Y", $timestamp );
	
	$deliv_payment = $day." ".$month." ".$year;
	
	//TTC
	
	$ttc = round($rs->fields("total_amount"),2);
	
	//On recherche et on remplace
	$to_replace = array("Idbilling_buyer",
	"Now_Date",
	"DateHeure",
	"Deliv_payment",
	"Montant");
	
	$replace = array($idbilling_buyer,
	$today,
	$billing_date,
	$deliv_payment,
	$ttc);
	
	$newdatas = str_replace($to_replace, $replace, $datas);
	
	header("content-type: application/msword");
	header("content-disposition: .doc; filename=mise_en_demeure_" . $idbilling_buyer . ".doc");
	
	echo $newdatas;
		
}

?>