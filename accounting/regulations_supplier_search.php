<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche des règlements fournisseurs
 */

include_once( dirname( __FILE__ ) . '/../config/init.php' );
include_once( dirname( __FILE__ ) . '/../objects/XHTMLFactory.php' );
include_once( dirname( __FILE__ ) . '/../objects/DHTMLCalendar.php' );
include_once( dirname( __FILE__ ) . '/../objects/Dictionnary.php' );
include_once( dirname( __FILE__ ) . '/../objects/SupplierInvoice.php' );
include_once( dirname( __FILE__ ) . '/../script/global.fct.php' );
include_once( dirname( __FILE__ ) . '/../catalog/admin_func.inc.php' );

$Title = "Rechercher un règlement fournisseur";
				
$hasAdminPrivileges = User::getInstance()->get( "admin" );
$Iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin( "gest_com_use_help" );


if( isset( $_GET[ 'getContactsTable' ] ) && isset( $_GET[ 'idsupplier' ] ) &&  $_GET[ 'idsupplier' ] > 0 ){
	$idsuppl = $_GET[ "idsupplier" ];
	
	$sup = new Supplier( intval( $idsuppl ) );
	$it = $sup->getContacts()->iterator();
	getContactsTable( $it );

	exit();
}

if( isset( $_POST[ 'sendSupplierArray' ] ) && $_POST[ 'sendSupplierArray' ] == '1' ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
			
	$unserializedArray = unserialize( base64_decode( $_POST[ 'serializedArrayRegulations' ] ) );

	$idsupplier = $_POST[ 'idsupplier' ];
	$dest = $_POST[ 'checkSupplierContact' ];
	$response = sendRegulationsDetails( $dest , $unserializedArray ,$idsupplier );
	
	exit( $response );
}

if( isset($_GET['deleteRegulation']) ){
	if( isset( $_GET['idRegulation'] ) && $_GET['idRegulation'] != "" ){
		
		$myRegul = $_GET['idRegulation'];
		
		// Supprimer un reglement fournisseur :
		// - supprimer le(s) lien(s) dans billing_regulations_supplier
		// - remettre la(les) facture(s) en 'StandBy' vu qu'elle est(sont) plus payée(s)
		// - on ne replace pas le 'credit_amount_used' parcequ'il est dans la facture
		// - de même pour l'escompte, il est dans la facture
		// - on déduit le overpayment s'il est dans la tranche et accepté (en gros si la facture est 'Paid' et que l'overpayment est dans la tranche)
		// - on remet les avoirs à dispo
		
		// d'abord on va récup les détails du règlement
		$myRegulation = DBUtil::query( 
		
			"SELECT rs.*, brs.rebate_amount
			FROM regulations_supplier rs, billing_regulations_supplier brs
			WHERE rs.idregulations_supplier = $myRegul 
			AND rs.idregulations_supplier = brs.idregulations_supplier
			LIMIT 1" 
		
		);
		
		if( $myRegulation === false || $myRegulation->RecordCount() == 0)
			exit( "Ce règlement n'existe pas" );
			
		$idregulation	= $myRegulation->fields( 'idregulations_supplier' );
		$idsupplier 	= $myRegulation->fields( 'idsupplier' );
		$amount 		= $myRegulation->fields( 'amount' );
		$credBalUsed	= $myRegulation->fields( 'credit_balance_used' );
		$rebateAmount	= $myRegulation->fields( 'rebate_amount' );
		$overpayment	= $myRegulation->fields( 'overpayment' );
		
		// on met les factures à 'StandBy', on vire les liens et le reglement
		$billings = DBUtil::query( "SELECT brs.billing_supplier, bs.status
									FROM billing_regulations_supplier brs, billing_supplier bs 
									WHERE brs.idregulations_supplier = $idregulation 
									AND brs.idsupplier = $idsupplier
									AND bs.billing_supplier = brs.billing_supplier" );
		
		$solde_tolerance = DBUtil::getParameterAdmin( "solde_tolerance_supplier" );
		$max_solde_tolerance = DBUtil::getParameterAdmin( "max_solde_tolerance_supplier" );
		
		if($billings->RecordCount() > 0){
			// il faut enlever le crédit fournisseur si le reglement était supérieur aux factures
			$rsSumAmounts = DBUtil::query( "	SELECT SUM(brs.`amount`) as sumAmounts
											FROM billing_supplier bs, billing_regulations_supplier brs
											WHERE bs.billing_supplier = brs.billing_supplier
											AND brs.idregulations_supplier = $idregulation
											AND bs.idsupplier = $idsupplier" );
			
			$sommeFactures = $rsSumAmounts->fields( 'sumAmounts' ) - $rebateAmount;
			
			// trop payé
			if( $amount > $sommeFactures ){
				$newCredSupplier = DBUtil::query( "SELECT credit_balance FROM supplier WHERE idsupplier = $idsupplier" )->fields( 'credit_balance' ) - ( $amount - $sommeFactures );
			
				DBUtil::query( "UPDATE supplier SET `credit_balance` = $newCredSupplier WHERE idsupplier = $idsupplier " );
			}
	
			//  trop payé utilisé
			if( $credBalUsed > 0 ){
				$newCredSupplier = $credBalUsed + DBUtil::query( "SELECT credit_balance FROM supplier WHERE idsupplier = $idsupplier" )->fields( 'credit_balance' );
			
				DBUtil::query( "UPDATE supplier SET `credit_balance` = $newCredSupplier WHERE idsupplier = $idsupplier " );
			}
			
			//avoirs
			if( $idregulation > 0)
				DBUtil::query("DELETE FROM credit_supplier_regulation WHERE idusage = $idregulation");		
			
			// factures
			while(!$billings->EOF){
				$billing2updt  = $billings->fields( 'billing_supplier' );
				$billinsStatus = $billings->fields( 'status' );
						
				if( $credBalUsed > 0 ){
			
					//
					DBUtil::query( "UPDATE billing_supplier SET `credit_amount` = 0 WHERE billing_supplier = '$billing2updt' " );
					
				}

				DBUtil::query( "UPDATE billing_supplier SET `status` = 'StandBy' WHERE billing_supplier = '$billing2updt' " );
			
				$billings->MoveNext();
			}
				
			// liens
			DBUtil::query( "DELETE FROM billing_regulations_supplier WHERE idregulations_supplier = $idregulation AND idsupplier = $idsupplier" );
			
			// règlement
			DBUtil::query( "DELETE FROM regulations_supplier WHERE idregulations_supplier = $idregulation AND idsupplier = $idsupplier" );
				
		}else{
			exit( "Règlement sans factures, impossible de le supprimer" );
		}
		
		exit();
	}else{
		exit( "Paramètre manquant !" );
	}
}

if(isset($_POST['validOrders'])){
	
	// on créé les règlements puis on confirme la commande normal koi
	// cmok
	
	$myOrdersDP = $_POST["ordersForDp"] ;

	$i=0;
	
	while( $i<count($myOrdersDP) ){
		$orderDP = $myOrdersDP[$i];

		$orderRs = DBUtil::query( "SELECT idorder_supplier,
										 idsupplier,
										 total_amount,
										 down_payment_type, 
										 down_payment_value, 
										 down_payment_comment, 
										 down_payment_date, 
										 down_payment_idpayment 
								  FROM order_supplier
								  WHERE idorder_supplier = $orderDP 
								  LIMIT 1" );
				
		$idsupplier 	= $orderRs->fields( 'idsupplier' );
		$amount			= $orderRs->fields( 'down_payment_type' ) == 'amount'?$orderRs->fields( 'down_payment_value' ):round($orderRs->fields( 'total_amount' ) * $orderRs->fields( 'down_payment_value' ) / 100 , 2);
		$comment  		= $orderRs->fields( 'down_payment_comment' );
		$idpayment  	= $orderRs->fields( 'down_payment_idpayment' );
		$payment_date  	= $orderRs->fields( 'down_payment_date' );
			
		//on créé le vrai règlement et on dit à la commande qu'elle est confirmé en lui passant l'idregulations_buyer
		
		$maxNumRegul = DBUtil::query( "SELECT (MAX(idregulations_supplier) + 1) as newId FROM regulations_supplier" )->fields( 'newId' );
		
		$username = User::getInstance()->get( 'login' );
		$iduser = User::getInstance()->getId();	

		$insertReg=			
		"INSERT INTO regulations_supplier (
			idregulations_supplier,
			idsupplier,
			iduser,
			amount,
			comment,
			idpayment,
			payment_date,
			deliv_payment,
			DateHeure,
			from_down_payment,
			accept,
			lastupdate,
			username
		) VALUES (
			$maxNumRegul,
			$idsupplier,
			$iduser,
			'$amount',
			'$comment',
			$idpayment,
			'$payment_date',
			'$payment_date',
			NOW(),
			1,
			1,
			NOW(),
			'$username'
		)";
		
		DButil::query($insertReg);
		
		// puis on change le statut de la commande à confirmé
		// cmok
		DBUtil::query( " UPDATE order_supplier SET `status` = 'confirmed', down_payment_idregulation = $maxNumRegul WHERE idorder_supplier = $orderDP " );
		
		
		$i++;
	}

	
	exit();
}

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />


<div class="centerMax">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div id="tools" class="rightTools">
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
	Accès rapide</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
	Quelques chiffres...</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
</div>	
<?php
	displayForm();
?>

</div>


<?php
	
include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
		
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked = true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked = true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked = true;
			
			return true;
			
		}
		
		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			
			$( '#searchForm' ).ajaxForm( options );
						
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData , jqForm , options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px' , cursor: 'pointer' , 'font-weight': 'bold' , 'font-family': 'Arial , Helvetica , sans-serif' , 'color': '#586065' , '-webkit-border-radius': '10px' , '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$( '.blockOverlay' ).attr( 'title' , 'Click to unblock' ).click( $.unblockUI );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText , statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$( '#SearchResults' ).html( responseText );
			
		}		
		
	// -->
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
	<a name="topPage"></a>
	
	<div class="contentDyn">
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<form action="<?php echo $GLOBAL_START_URL ?>/accounting/regulations_supplier_search.php" method="post" name="searchForm" id="searchForm">
		<input type="hidden" name="page" value="1" />
		<input type="hidden" name="search" value="1" />        	
		
		<!-- Titre -->
		<h1 class="titleSearch">
		<span class="textTitle">Rechercher un règlement fournisseur</span>
		<span class="selectDate">
			<input type="radio" name="date_type" value="payment_date"  checked="checked" class="VCenteredWithText" checked="checked" /> Date de règlement&nbsp;&nbsp;
		    <input type="radio" name="date_type" value="deliv_payment" class="VCenteredWithText" /> Date d'échéance          
		</span>
		<div class="spacer"></div>
		</h1>
		
		<div class="blocSearch"><div style="margin:5px;">
			<!-- Choix par date -->
			<div class="blocMultiple blocMLeft">
				<h2>&#0155; Par date :</h2>
				
				<label><span><input type="radio" name="interval_select" id="interval_select_since" value="1" checked="checked" class="VCenteredWithText" />
				Depuis</label></span>
	            <select name="minus_date_select" onclick="selectSince();" class="fullWidth">
	            	<option value="0">Aujourd'hui</option>
	              	<option value="-1">Hier</option>
	          		<option value="-2">Moins de 2 jours</option>
	              	<option value="-7">Moins de 1 semaine</option>
	              	<option value="-14">Moins de 2 semaines</option>
	              	<option value="-30">Moins de 1 mois</option>
	              	<option value="-60">Moins de 2 mois</option>
	              	<option value="-90">Moins de 3 mois</option>
	              	<option value="-180">Moins de 6 mois</option>
	              	<option value="-365">Moins de 1 an</option>
	              	<option value="-730">Moins de 2 ans</option>
	              	<option value="-1825">Moins de 5 ans</option>
	            </select>
				<div class="spacer"></div>
				
				<label><span><input type="radio" name="interval_select" id="interval_select_month" value="2" class="VCenteredWithText" />
				Pour le mois de</span></label>
				<?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
				<div class="spacer"></div>
				
				<label><span><input type="radio" name="interval_select" id="interval_select_between" value="3" class="VCenteredWithText" />
				Entre le</span></label>
				<?php $lastYearDate = date( "d-m-Y", mktime( 0 , 0 , 0 , date( "m" ) , date( "d" ) , date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
				<input type="text" name="bt_start" id="bt_start" value="<?php echo $lastYearDate ?>" class="calendarInput" />
				<?php echo DHTMLCalendar::calendar( "bt_start" , true , "selectBetween" ) ?>
				
				<label class="smallLabel"><span>et le</span></label>
				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo date( "d-m-Y" ) ?>" class="calendarInput" />
				<?php echo DHTMLCalendar::calendar( "bt_stop" , true , "selectBetween" ) ?>
				<div class="spacer"></div>
				
				
				<label><span>N° facture fsr</span></label>
				<input type="text" name="facture_fournisseur" id="facture_fournisseur" class="textInput" value="<?php echo isset( $_POST[ "facture_fournisseur" ] ) ? $_POST[ "facture_fournisseur" ] : "" ?>" />
				<?php
                    //Added By Behzad                    	
					include_once( dirname( __FILE__ ) . "/../objects/AutoCompletor.php" );
					AutoCompletor::completeFromDBPattern( "facture_fournisseur", "billing_regulations_supplier", "billing_supplier", "Montant :  {amount}", 15 );

				?>
				<div class="spacer"></div>
	        </div>   
	        
	        <!-- Choix par informations sur le devis -->
	        <div class="blocMultiple blocMRight">
	        	<h2>&#0155; Par informations sur la facture ou proforma :</h2>                             
	                               
	            <label><span>Facture proforma n°</span></label>
	            <input type="text" maxlenght="10" class="textmoitieInput" name="billing_supplier" value="" />
	            
	            <label class="smallLabelMoitie"><span>Cde frs n°</span></label>
	            <input type="text" name="idorder_supplier" value="" class="textmoitieInput" />
	            <div class="spacer"></div>
	            
	            <label><span>Fournisseur</span></label>
	            <?php XHTMLFactory::createSelectElement( 'supplier' , 'idsupplier' , 'name' , 'name' , '' , '' , '' , ' name="idsupplier" id="idsupplier" ' ); ?>
	            <div class="spacer"></div>
	            
	            <label><span>Total facturé HT</span></label>
	            <input type="text" name="total_amount_ht"  value="" class="textmoitieInput" />
	            
	            <label class="smallLabelMoitie"><span>Total facturé TTC</span></label>
	            <input type="text" name="total_amount"  value="" class="textmoitieInput" />
	            <div class="spacer"></div>
	            
	            <label><span>Mode de règlement</span></label>
	            <?php XHTMLFactory::createSelectElement( 'payment' , 'idpayment' , 'name_1' , 'idpayment' , '' , '' , '' , ' name="idpayment" ' ); ?>
	        	<div class="spacer"></div>
	        </div>  
	        <div class="spacer"></div>
	        <input type="submit" class="inputSearch" value="Rechercher" />
	        <div class="spacer"></div>
	        
	 	</div></div>   
	 	</form>
 		<div class="spacer"></div>
 	
 	</div>
 	<div class="spacer"></div>
 	
 	<div id="SearchResults"></div>  
        

	<?php 
}


/////-----------------------

function search(){

	global	$resultPerPage,
			$hasAdminPrivileges,
			$GLOBAL_START_URL,
			$GLOBAL_START_PATH,
			$ScriptName;
	
	$now = getdate();
	
	$date_type 				= isset( $_POST[ "date_type" ] ) 			? $_POST[ "date_type" ] : "payment_date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) 	? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) 		? $_POST[ "interval_select" ] : 1;
	$idorder_supplier		= isset( $_POST[ "idorder_supplier" ] ) 	? $_POST[ "idorder_supplier" ] : "";
	$total_amount_ht 		= isset( $_POST[ "total_amount_ht" ] ) 		? $_POST[ "total_amount_ht" ] : "";
	$total_amount 			= isset( $_POST[ "total_amount" ] ) 		? $_POST[ "total_amount" ] : "";
	$idpayment	 			= isset( $_POST[ "idpayment" ] ) 			? $_POST[ "idpayment" ] : 0;
	$idsupplier	 			= isset( $_POST[ "idsupplier" ] ) 			? $_POST[ "idsupplier" ] : 0;
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) 	? $_POST[ "billing_supplier" ] : "";
	$facture_fournisseur	= isset( $_POST[ "facture_fournisseur" ] ) 	? $_POST[ "facture_fournisseur" ] : 0;

	$SQL_Condition2 = "";
	$sqlCondAvoir	= "";
	switch ($interval_select){
		
		case 1:
		 	//min |2000-12-18 15:25:53|
			$Min_Time = date( "Y-m-d 00:00:00", mktime( $now["hours"] , $now["minutes"] , $now["seconds"] , $now["mon"] , $now["mday"] + $minus_date_select , $now["year"] ) ) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime( $now["hours"] , $now["minutes"] , $now["seconds"] , $now["mon"] , $now["mday"] , $now["year"] ) ) ;
			//sql_condition
			$SQL_Condition  = "`regulations_supplier`.$date_type >='$Min_Time' AND `regulations_supplier`.$date_type <='$Now_Time 23:59:59' ";
			$sqlCondAvoir = "`credit_supplier_regulation`.date >='$Min_Time' AND `credit_supplier_regulation`.date <='$Now_Time 23:59:59' ";

			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if( $yearly_month_select > $now["mon"] )
				$now["year"] --;
			
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00" , mktime( $now["hours"] ,$now["minutes"] , $now["seconds"] , $yearly_month_select , 1 , $now["year"]) ) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00" , mktime( $now["hours"] ,$now["minutes"] , $now["seconds"] , $yearly_month_select + 1 , 1 , $now["year"] ) ) ;
		
			//sql_condition
			$SQL_Condition  = "`regulations_supplier`.$date_type >='$Min_Time' AND `regulations_supplier`.$date_type <'$Max_Time' ";
			$sqlCondAvoir = "`credit_supplier_regulation`.date >='$Min_Time' AND `credit_supplier_regulation`.date <='$Now_Time 23:59:59' ";
									
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
			 
			$SQL_Condition  = "`regulations_supplier`.$date_type >='$Min_Time' AND `regulations_supplier`.$date_type <='$Max_Time 23:59:59' ";
			$sqlCondAvoir = "`credit_supplier_regulation`.date >='$Min_Time' AND `credit_supplier_regulation`.date <='$Now_Time 23:59:59' ";
			
	     	break;
			
	}

	
	
	if( !$hasAdminPrivileges ){
		$SQL_Condition .= " AND ( `regulations_supplier`.iduser = '$iduser' )";
	}				
	if ( !empty( $idsupplier ) ){
		$SQL_Condition .= " AND billing_supplier.idsupplier =$idsupplier";
		
		$SQL_Condition2 .= " AND order_supplier.idsupplier = $idsupplier";
		
		$sqlCondAvoir .= " AND billing_supplier.idsupplier =$idsupplier";
	}
	
	if ( !empty( $billing_supplier ) ){
		$SQL_Condition .= " AND billing_regulations_supplier.billing_supplier LIKE '%$billing_supplier%'";	
		$sqlCondAvoir .= " AND ( credit_supplier_regulation.billing_supplier LIKE '%$billing_supplier%' OR credit_supplier_regulation.idcredit_supplier LIKE '%$billing_supplier%' )";
	}
	if ( !empty( $facture_fournisseur ) ){
		$SQL_Condition .= " AND billing_regulations_supplier.billing_supplier = '$facture_fournisseur'";	
		
	}
	if ( !empty( $idorder_supplier ) ){
		$idorder_supplier = FProcessString( $idorder_supplier );
		$SQL_Condition .= " AND bl_delivery.idorder_supplier = $idorder_supplier";
	}
		
	if( !empty( $total_amount_ht ) ){
		$SQL_Condition .= " AND ROUND(`billing_supplier`.total_amount_ht,2) = '" . Util::text2num( $total_amount_ht ) . "'";
		$SQL_Condition2 .= " AND ROUND(`order_supplier`.total_amount_ht,2) = '" . Util::text2num( $total_amount_ht ) . "'";
	}
		
	if( !empty( $total_amount ) ){
		$SQL_Condition .= " AND ROUND(`billing_supplier`.total_amount,2) = '" . Util::text2num( $total_amount ) . "'";
		$SQL_Condition2 .= " AND ROUND(`order_supplier`.total_amount,2) = '" . Util::text2num( $total_amount ) . "'";
	}
	
	if( !empty( $idpayment ) ){
		$SQL_Condition .= " AND regulations_supplier.idpayment = $idpayment";
		$SQL_Condition2 .= " AND `order_supplier`.down_payment_idpayment = $idpayment";
	}
	
	$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

	$totalFact = 0;
	$totalTTCFacts = 0;
	?>
	<script type="text/javascript" language="javascript">
		<!--
			
			function popupPartiallyPayment( irs ){
				
				postop = (self.screen.height-300) / 2;
				posleft = (self.screen.width-400) / 2;
				
				window.open( 'register_partially_payment.php?irs='+irs, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=300,resizable,scrollbars=yes,status=0' );
				
			}
			
			function Modify( idrb ){
				
				postop = (self.screen.height-300) / 2;
				posleft = (self.screen.width-400) / 2;
				
				window.open( 'modify_buyer_payment.php?idrb='+idrb, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=360,resizable,scrollbars=yes,status=0' );
				
			}
			
			function removeRow( idrb ){
				var ilEstFouAflelou = confirm( "Estes vous sûr de vouloir supprimer ce règlement ? Ceci repassera de même les factures associées en attente " );
	
				if(ilEstFouAflelou){
					$.ajax({
						type: 'GET',
						url: '<?php echo $GLOBAL_START_URL ?>/accounting/regulations_supplier_search.php',
						data: 'deleteRegulation&idRegulation=' + idrb,
						error: function(){ $.growUI( 'Une erreur est survenue lors de la suppression du règlement' , '' ); },
						success: function( response ){
							if( response.length > 0 ){
								$.growlUI( 'Une erreur est survenue lors de la suppression du règlement<br/>'+response , '' );
							}else{
								if( $( '#regulationRow_'+idrb).parent( 'tbody' ).children().length == 1 ){
									$( '#regulationRow_'+idrb).parent( 'tbody' ).parent( 'table' ).parent( 'div' ).prev( 'div' ).remove();
									$( '#regulationRow_'+idrb).parent( 'tbody' ).parent( 'table' ).parent( 'div' ).remove();
								}else{
									$( '#regulationRow_'+idrb).remove();
								}
								
								$.growlUI( 'Suppression effectuée avec succès' , '' );
							}
						}
					});
				}
			
			}
			
			function displayFournisseurSend(){
										
					$( "#mailFournisseurForm" ).dialog({
						width: 1000,
						height: 'auto',
						position: ['middle',20],
						modal: true,
						resizable:false,
						title: 'Envoyer le récapitulatif des règlements',
						close:function(event, ui) { $('#mailFournisseurForm').dialog( 'destroy' ); }
					});
					
					$( '#serializedArrayRegulations' ).val( $( '#serializedArray' ).val() );
					
			}
			
		// -->
		
	</script>


<div class="contentResult" id="container">

	<div id="mailFournisseurForm" style="display:none;">
		<?
		if( isset( $_POST[ "idsupplier" ] ) && $_POST[ "idsupplier" ] > 0 ){
			$idsuppl = $_POST[ "idsupplier" ];
			
			$sup = new Supplier( intval( $idsuppl ) );
			$it = $sup->getContacts()->iterator();
			
			?>
			<p style="width:100%;"><strong>Sélectionnez un ou plusieurs destinataires :</strong></p>
			<script type="text/javascript">
			/* <![CDATA[ */
		
			    /* -------------------------------------------------------------------------- */
				
				function createContact(){
		
					$.ajax({
					 	
						url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $sup->getId(); ?>&create=contact",
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de créer le contact" ); },
					 	success: function( responseText ){
			
							$( "#ContactEditorContainer" ).html( responseText );
						
						}
			
					});
					
				}
		
				/* -------------------------------------------------------------------------- */
				
				function deleteContact( idcontact ){
		
					$.ajax({
					 	
						url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $sup->getId(); ?>&delete=contact&idcontact=" + idcontact,
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de supprimer le contact" ); },
					 	success: function( responseText ){
			
							$( "#ContactEditorContainer" ).html( responseText );
						
						}
			
					});
		
				}
		
				/* -------------------------------------------------------------------------- */
				
				function duplicateContact( idcontact ){
		
					$.ajax({
					 	
						url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $sup->getId(); ?>&duplicate=contact&idcontact=" + idcontact,
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de dupliquer le contact" ); },
					 	success: function( responseText ){
			
							$( "#ContactEditorContainer" ).html( responseText );
						
						}
			
					});
		
				}
		
				/* -------------------------------------------------------------------------- */
				
				function setMainContact( idcontact ){
		
					$.ajax({
					 	
						url: "/templates/product_management/supplier_edit.php?idsupplier=<?php echo $sup->getId(); ?>&main_contact=" + idcontact,
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de définir le contact principal" ); },
					 	success: function( responseText ){
			
							$( "#ContactEditorContainer" ).html( responseText );
						
						}
			
					});
		
				}
		
				/* ---------------------------------------------------------------------------------------------------------------- */
			
				$(function() {
			
			    	$('#SupplierForm').ajaxForm( {
					 	
				        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				        success:       postSubmitCallback  // post-submit callback      
			  
				    } );
				    
			    	$('#SupplierFormSend').ajaxForm( {
					 	
				        beforeSubmit:  preSubmitCallbackSend,  // pre-submit callback 
				        success:       postSubmitCallbackSend  // post-submit callback      
			  
				    } );
			    	
				});
				
				/* ---------------------------------------------------------------------------------------------------------------- */
				
				function preSubmitCallbackSend( formData, jqForm, options ){
									
					countChecked = $( '.checkSuppliersSend input:checked').length;
					
					if( countChecked == 0 )
						return false;
					
				}

				/* ---------------------------------------------------------------------------------------------------------------- */
				
				function postSubmitCallbackSend( responseText, statusText ){
				
					if( responseText != "1" ){
			
						alert( "Impossible d'envoyer les informations au fournisseur : " + responseText );
						return;
						
					}
					
					/* destruction du popup */
					
					$('#mailFournisseurForm').dialog( 'destroy' );
					
					/* confirmation */
										
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( "", "Les informations ont bien été envoyées au fournisseur" );
			    	
				}				
			
				/* ---------------------------------------------------------------------------------------------------------------- */
				
				function preSubmitCallback( formData, jqForm, options ){
				}
			
				/* ---------------------------------------------------------------------------------------------------------------- */
				
				function postSubmitCallback( responseText, statusText ){ 
			
					if( responseText != "1" ){
			
						alert( "Impossible de sauvegarder les informations : " + responseText );
						return;
						
					}
				
					/* confirmation */
					
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
			    	$.growlUI( "", "Modifications enregistrées" );
			    	
			    	refreshTable();
				}
				
				/* ---------------------------------------------------------------------------------------------------------------- */
				
				function refreshTable(){
					$.ajax({
					 	
						url: "/accounting/regulations_supplier_search.php?getContactsTable&idsupplier=<?php echo $sup->getId(); ?>",
						async: true,
						type: "GET",
						data: "",
						error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( "Impossible de récupérer les contacts !!!" ); },
					 	success: function( responseText ){
			
							$( "#contactsTable" ).html( responseText );
						
						}
			
					});
				}
				
			/* ]]> */
			</script>		
            <div class="resultTableContainer clear" id="contactsTable">
            	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/regulations_supplier_search.php" method="post" id="SupplierFormSend" enctype="multipart/form-data">
	            	<?php getContactsTable( $it ); ?>
	            	
					<div class="spacer"></div>
					<input type="hidden" name="serializedArrayRegulations" id="serializedArrayRegulations" value="" />
					<input type="hidden" name="sendSupplierArray" value="1" />
					<input type="hidden" name="idsupplier" value="<?php echo $idsuppl; ?>" />
					<input type="submit" name="Send" class="blueButton floatleft" value="Envoyer" style="margin:2px 0 0;" />
				</form>
				<a class="blueButton floatright" href='#' onclick="$('#contactsManagement').slideToggle(); return false;" style="margin:2px 0 0; float:right;">
				Ajouter / Modifier un contact du fournisseur</a>
			</div>
			<div class="spacer"></div>
			
			<div style="display:none;" id="contactsManagement">
				<form action="<?php echo $GLOBAL_START_URL ?>/product_management/enterprise/supplier.php?update&amp;idsupplier=<?php echo $sup->getId(); ?>" method="post" id="SupplierForm" enctype="multipart/form-data">
					<input type="hidden" name="update" value="1" />
					<input type="hidden" name="code_supplier" value="<?php echo strlen( $sup->get( "code_supplier" ) ) ? $sup->get( "code_supplier" ) : "F" . $sup->getId(); /* @todo : DROP */ ?>" />
					<input id="users" class="textInput inputSupplier" type="hidden" readonly="readonly" value="<?php echo $sup->get("contacts");  ?>" name="users" />
					
					<div id="ContactEditorContainer"> 
					<?
						include( dirname( __FILE__ ) . "/../lib/contact_editor.php" );
						displayContactEditor( $sup->getContacts(), 0, "setMainContact", "createContact", "deleteContact", "duplicateContact" );
					?>
					</div>
					<div class="spacer"></div>	
					
					<input type="submit" name="Modify" class="inputSave" value="Sauvegarder" />
					<div class="spacer"></div>	
				</form>
			</div>
		<?	
		}?>
	</div>
	
	<h1 class="titleSearch">
		<span class="textTitle">Résultat de la recherche</span>
		<div class="spacer"></div>
	</h1>	
	<div class="spacer"></div>

	<div id="containerEstimate">

		<ul class="menu">
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#Regulations"><span id="reg">Règlements</span></a></div>
			</li>
			<li class="item">
				<div class="right"></div><div class="left"></div>
				<div class="centered"><a href="#DownPayments"><span id="dps">Acomptes à valider</span></a></div>
			</li>
		</ul>
		
		<div class="blocSearch withTab"><div class="blocMarge">
		<div id="Regulations">
			<form action="<?php echo $ScriptName ?>" method="post" name="invoicetopay" id="invoicetopay">
				<input type="hidden" name="page" value="1" />
				<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
				<input type="hidden" name="search" value="1" />
				
				
				<?php
				
				$arrayRegulations = array();
				
				$SQL_Condition	.= " AND regulations_supplier.iduser = u.iduser
				AND regulations_supplier.idsupplier = supplier.idsupplier
				AND billing_regulations_supplier.billing_supplier = billing_supplier.billing_supplier
				AND billing_regulations_supplier.idsupplier = billing_supplier.idsupplier
				AND billing_regulations_supplier.idregulations_supplier = regulations_supplier.idregulations_supplier";
					
											
				//recherche				
				$lang = User::getInstance()->getLang();
				
				$SQL_Query = "
				SELECT billing_supplier.billing_supplier, 
					supplier.name,
					billing_supplier.Date,
					billing_supplier.idsupplier,
					billing_supplier.status,
					regulations_supplier.payment_date,
					regulations_supplier.deliv_payment,
					regulations_supplier.idpayment,
					regulations_supplier.idregulations_supplier,
					regulations_supplier.credit_balance_used,
					regulations_supplier.bank_date,
					u.initial,
					payment.name$lang AS payment,
					billing_supplier.idpayment,
					regulations_supplier.amount,
					regulations_supplier.overpayment,
					'' as idcredit_supplier
				FROM user u, supplier, billing_regulations_supplier, billing_supplier
				LEFT JOIN billing_supplier_row ON ( billing_supplier_row.billing_supplier = billing_supplier.billing_supplier AND billing_supplier_row.idsupplier = billing_supplier.idsupplier )
				LEFT JOIN bl_delivery ON ( bl_delivery.idbl_delivery = billing_supplier_row.idbl_delivery AND bl_delivery.idsupplier = billing_supplier_row.idsupplier ), 
				regulations_supplier
				LEFT JOIN payment ON payment.idpayment = regulations_supplier.idpayment
				WHERE $SQL_Condition
				GROUP BY regulations_supplier.idregulations_supplier
				ORDER BY supplier.name ASC";
			//	echo $SQL_Query;
				$SQL_QueryCredits ="
				SELECT billing_supplier.billing_supplier, 
					supplier.name, 
					billing_supplier.Date, 
					billing_supplier.idsupplier, 
					billing_supplier.status,
					credit_supplier_regulation.date, 
					billing_supplier.idpayment, 
					credit_supplier_regulation.amount,
					credit_supplier_regulation.idcredit_supplier
				FROM billing_supplier, credit_supplier_regulation, supplier
				WHERE $sqlCondAvoir
				AND billing_supplier.billing_supplier = credit_supplier_regulation.billing_supplier
				AND billing_supplier.idsupplier = supplier.idsupplier
				AND idusage = 0
				GROUP BY credit_supplier_regulation.idcredit_supplier
				";
				
		   	  	// On remplit le tableau avec les factures + avoirs 	  	
		   	  	fillArrayRegulations( $arrayRegulations , $SQL_Query );

		   	  	// On remplit le tableau avec les avoirs tt seuls comme on peut pas les récup
		   	  	fillArrayCreditsOnly( $arrayRegulations , $SQL_QueryCredits );

				// Une fois la misère terminée on peut travailler un peut enfin
				$nbrResult = 0;
				foreach($arrayRegulations as $idsupplier => $Regulations ){
					$nbrResult += count( $Regulations );
				}
				?>		
				
				
			    <div id="factures">
					<?if( $nbrResult == 0 ){?>
				    	<p style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun règlement de facture fournisseur n'a été trouvé.</p>
					
					<?php } else{?>
			        	<p><strong>Résultat de la recherche: <?php echo $nbrResult ?> règlement(s)</strong></p>

						<?php
									
						//export factor
						$use_factor_export = DBUtil::getParameterAdmin( "use_factor_export" );
						if( $use_factor_export ){
							
							include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
							include_once( "$GLOBAL_START_PATH/objects/Util.php" );
							
							global $GLOBAL_DB_PASS;
							$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $SQL_Query , $GLOBAL_DB_PASS ) );
						
						?>
							<a href="<?php echo $GLOBAL_START_URL ?>/accounting/export_supplier_payment.php?query=<?php echo $encryptedQuery ?>" class="blackText"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /> Export au format CSV</a>
						<?php } ?>

						<?php
							
						$total_frs = 0;
						$fact = 0;
						$totalTTCFacts = 0;
												
						foreach($arrayRegulations as $idsupplier => $Regulations ){
							
							$supplier = DBUtil::query("SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1")->fields( 'name' );
						
						?>
	                	<div class="subTitleContainer" style="margin-top:20px;">
	                        <p class="subTitle">
	                        	<?=$supplier?>
	                        </p>
	                    </div>
						                    
		                <!-- tableau résultats recherche -->
		                <div class="resultTableContainer clear">
		                	<table class="dataTable resultTable">
		                    	<thead>
		                            <tr>
		                                <th style="width:90px;">Factures ou proformas n°</th>
		                                <th style="width:90px;">Total TTC<br />factures</th>
		                                <th style="width:100px;">Mode de paiement</th>
		                                <th style="width:70px;">Escompte</th>
		                                <th style="width:70px;">Avoirs<br/>utilisés</th>
		                                <th style="width:70px;">Ecart règlement accepté</th>
		                                <th style="width:80px;">Total réglé</th>
		                                <th style="width:70px;">Trop payé</th>
		                                <th style="width:100px;">Date de paiement</th>
		                              </tr>
		                        </thead>
		                        <tbody>
							<?
							
							$supplierTotalAmount = 0.00 ;
							foreach($Regulations as $key => $values ){
								
								$idrs 				= $values[ "idregulations_supplier" ];
								$idcs				= $values[ "idcredit_supplier" ];
								$billing_supplier 	= "";
								$credBalUsed 		= $values[ "credit_balance_used" ];
								$rebateUsed 		= 0.00;
								$com 				= $values[ "initial" ];								
								$deliv_payment 		= $values[ "deliv_payment" ]	==	"0000-00-00"	?	""	:	usDate2eu( $values[ "deliv_payment" ] );						
								$payment_date 		= $values[ "payment_date" ] 	==	"0000-00-00"	?	""	:	usDate2eu( $values[ "payment_date" ] );
								$overpayment 		= $values[ "overpayment" ];
								$dateFacturation 	= usDate2eu( $values[ "Date" ] );
								$status	 			= $values[ "status" ];
								$idpayment 			= $values[ "idpayment" ];
								$payment_name 		= $values[ "payment" ];
								$billings 			= $values[ "billing_supplier" ];
								$credits			= $values[ "credit_supplier" ];
							?>
						        <tr class="blackText" id="regulationRow_<?=$idrs?>">
						        	<td class="lefterCol" style="">
						        		<ul style="width:90px;list-style:none;margin:0;padding:0;">
						        		<?
						        			foreach( $billings as $idbil => $billing ){
						        				?>
						        				<li style="width:90px;line-height:20px;"><?php echo $idbil ?></li>
						        				<?
						        			}
						        			foreach( $credits as $idcred => $credit ){
						        				?>
						        				<li style="width:90px;line-height:20px;color:red;"><?php echo $idcred ?></li>
						        				<?
						        			}
						        			
						        		?>
						        	</td>
						        	<td>
						        		<ul style="width:90px;list-style:none;margin:0;padding:0;">
						        		<?
						        			$totalAmount = 0.00;
						        			foreach( $billings as $idbil => $billing ){
						        				?>
						        				<li style="width:90px;line-height:20px;"><?php echo Util::priceFormat($billings[ $idbil ][ 'amount' ]) ?></li>
						        				<?
						        				$totalAmount += $billing[ 'amount' ];
						        			}
						        			$creditSupplierTotal = 0.00;
						        			foreach( $credits as $idcred => $credit ){
						        				?>
						        				<li style="width:90px;line-height:20px;color:red;">- <?php echo Util::priceFormat($credits[ $idcred ][ 'amount' ]) ?></li>
						        				<?
						        				$creditSupplierTotal += $credit[ 'amount' ];
						        			}
						        		?>
						        	</td>
						        	<td>
						        		<ul style="width:100px;list-style:none;margin:0;padding:0;">
						        		<?
						        			foreach( $billings as $idbil => $billing ){
						        				$pname=DBUtil::query( "SELECT name_1 FROM payment p, billing_supplier bs WHERE bs.billing_supplier = '$idbil' AND p.idpayment = bs.idpayment LIMIT 1" )->fields( 'name_1' );
						        				
						        				?>
						        				<li style="width:100px;line-height:20px;"><?php echo $pname ?></li>
						        				<?
						        			}
						        			foreach( $credits as $idcred => $credit ){
						        				?>
						        				<li style="width:100px;line-height:20px;color:red;">-</li>
						        				<?
						        			}
						        		?>
						        	</td>
						        	<td>
						        		<ul style="width:70px;list-style:none;margin:0;padding:0;">
						        		<?
						        			$totalRebate = 0.00;
						        			foreach( $billings as $idbil => $billing ){
						        				
						        				$rebate = $billing[ 'rebate' ] ;
						        				?>
						        				<li style="width:70px;line-height:20px;"><?php echo $rebate==0 ? "-" : Util::priceFormat($rebate) ?></li>
						        				<?
						        				$totalRebate += $rebate;
						        			}
						        			foreach( $credits as $idcred => $credit ){
						        				?>
						        				<li style="width:70px;line-height:20px;color:red;">-</li>
						        				<?
						        			}
						        		?>
						        	</td>
						        	<td><?php echo $creditSupplierTotal > 0 ? Util::priceFormat( $creditSupplierTotal ) : "-" ?></td>
						        	<td><?php echo $overpayment != 0 ? Util::priceFormat( $overpayment ) : "-" ?></td>
						        	<?
						        	$amount = $values[ 'regulation_amount' ];
						        	$supplierTotalAmount += $amount;
						        	?>
						        	<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $amount ) ?></td>
						        	<td><?
							        	
						        		$mytotal = $amount - $totalAmount - $totalRebate - $credBalUsed - $overpayment;
						        		if( $mytotal > 0 ){
						        			echo Util::priceFormat( $mytotal );
						        		}else{
						        			echo '-';
						        		}

						        		?>
						        	</td>
						        	<td><?php echo $payment_date ?></td>
									<!--<td class="righterCol">
										<?
										if( $values[ 'bank_date' ] == '0000-00-00' ){?>
											<img src="<?=$GLOBAL_START_URL?>/images/back_office/content/button_delete.png" onclick="removeRow(<?=$idrs?>);"/>
										<?php }
										?>
									</td>-->
						        </tr>
									<?php
									
								$fact++;
								$totalFact++;
								
							}
							$totalTTCFacts += $supplierTotalAmount;
							if( $fact > 1 ){ ?>
							<tr>
								<th colspan="6" style="border-style:none;"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $supplierTotalAmount ) ?></th>
								<th colspan="3" style="border-style:none;"></th>
							</tr>
							<?php } ?>
			
						</tbody>
			    	</table>
			    </div>
		    		<?php }
		    		
				}
				
				if( $nbrResult > 0 ){
				?>
				
			        <div class="resultTableContainer clear" style="margin-top:20px;float:right;">
			        	<table class="dataTable resultTable" style="width:400px">
			            	<thead>
			                    <tr>
			                    	<th>Nombre de règlements</th>
			                        <th>Total règlements</th>
			                    </tr>
			              	</thead>
			              	<tbody>
			              		<tr>
			              			<td class="lefterCol"><?php echo $totalFact ?></td>
			              			<td class="righterCol"><?php echo Util::priceFormat($totalTTCFacts) ?></td>
			              		</tr>
			              	</tbody>
			             </table>      
			    	</div>
			    	<div class="clear"></div>
			    	</div><?
				}?>
				</div>
			</form>
			<?
			if( isset( $_POST[ "idsupplier" ] ) && $_POST[ "idsupplier" ] > 0 ){
				?>
				<div class="clear"></div>
				<div style="text-align:right;margin-right:13px;margin-top:10px;">
					<input type="hidden" name="serializedArray" id="serializedArray" value="<?php echo base64_encode( serialize( $arrayRegulations ) ) ?>">
					<input type="button" name="sendSupplier" value="Envoyer au fournisseur" class="blueButton" onclick="displayFournisseurSend();return false;">
				</div>
				<div class="clear"></div>
				<?
			}
			?>
		</div>

<?php /*********************************** ACOMPTES ***************************************************/ ?>
		<div id="DownPayments">
			<?
			$totalReg = 0;
			$totalRegAmount = 0;
			
			$rsDownPayments = DBUtil::query( "SELECT `order_supplier`.*
											 FROM `order_supplier`, supplier
											 WHERE `order_supplier`.idsupplier = supplier.idsupplier
											 AND `order_supplier`.`status` <> 'confirmed' 
											 AND `order_supplier`.`status` <> 'dispatch'
											 AND `order_supplier`.`status` <> 'invoiced'
											 AND `order_supplier`.down_payment_value > 0
											 AND `order_supplier`.down_payment_idregulation = 0
											 $SQL_Condition2
											 ORDER BY idsupplier ASC" );
			
			$totalReg = $rsDownPayments->RecordCount();
			
			?>
			<form action="<?php echo $ScriptName ?>" method="post" name="dpToValidate" id="dpToValidate">
				<div class="content" id="orders">
					<?php if( $totalReg == 0 ){ ?>
				        <div class="subContent">
				        	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun acompte fournisseur n'a été trouvé.</span>
				        </div>
					<?php }else{ ?>
						<div class="headTitle">
	                    	<p class="title">Résultat de la recherche: <?php echo $nbrResult ?> règlement(s)</p>
	                    </div>
			            <div class="subContent">
		                    <div class="resultTableContainer clear">
		                    	<table class="dataTable resultTable">
		                        	<thead>
		                                <tr>
		                                    <th style="width:20px;">Resp.</th>
		                                    <th style="width:100px;">Fournisseur</th>
		                                    <th style="width:80px;">Cde n°</th>
		                                    <th style="width:70px;">Montant cde</th>
		                                    <th style="width:100px;">Date acompte</th>
		                                    <th style="width:100px;">Mode de paiement</th>
		                                    <th colspan='2'>Acompte</th>
		                                    <th>Commentaire</th>
		                                    <th style="width:20px;">Valider</th>
		                                </tr>
		                            </thead>
		                            <tbody>
		                            	<?
		                            	while(!$rsDownPayments->EOF()){
		                            		$idsupplier 		= $rsDownPayments->fields( 'idsupplier' );
		                            		$idordersupplier 	= $rsDownPayments->fields( 'idorder_supplier' );
		                            		$resp				= DBUtil::getDBValue( "initial","user","iduser",$rsDownPayments->fields( 'iduser' ));
		                            		$totalcde			= $rsDownPayments->fields( 'total_amount' );
		                            		$dateDp				= Util::dateFormatEu($rsDownPayments->fields( 'down_payment_date' ));
		                            		$modeDp    			= DBUtil::getDBValue( "name_1","payment","idpayment",$rsDownPayments->fields( 'down_payment_idpayment' ));
		                            		$dptype				= $rsDownPayments->fields( 'down_payment_type' );
		                            		$dpvalue			= $rsDownPayments->fields( 'down_payment_value' );
		                            		$dprate				= $dptype == 'rate'? $dpvalue : round( ( $dpvalue * 100 ) / $totalcde , 2);
		                            		$dpamount			= $dptype == 'amount'? $dpvalue : round( ($totalcde*$dpvalue)/100 , 2);
		                            		$dpcomment			= $rsDownPayments->fields( 'down_payment_comment' );
		                            		$totalRegAmounts    += $dpamount;
		                            		?>
		                            		
		                            		<tr id="orderRow_<?=$idordersupplier?>">
		                            			<td class="lefterCol"><?=$resp?></td>
		                            			<td><?=$idsupplier?></td>
		                            			<td><?=$idordersupplier?></td>
		                            			<td><?=Util::priceFormat($totalcde)?></td>
		                            			<td><?=$dateDp?></td>
		                            			<td><?=$modeDp?></td>
		                            			<td style="width:70px;"><?=Util::rateFormat($dprate)?></td>
		                            			<td style="width:70px;"><?=Util::priceFormat($dpamount)?></td>
		                            			<td class="righterCol"><?=$dpcomment?></td>
		                            			<td class="righterCol">
		                            				<input type="checkbox" name="ordersForDp[]" value="<?=$idordersupplier?>">
		                            			</td>
		                            		</tr>
		                            		<?
		                            		$rsDownPayments->MoveNext();
		                            	}?>
		                            </tbody>
		                        </table>
		                        <div class="submitButtonContainer" style="margin-top:10px;">
		                        	<input type="submit" name="validOrders" value="Valider" class="blueButton">
		                        </div>
		                    </div>			            
			            </div>
					<?
					}
					if( $totalReg > 0 ){
					?>
					
				        <div class="resultTableContainer clear" style="margin-top:20px;float:right;">
				        	<table class="dataTable resultTable" style="width:400px">
				            	<thead>
				                    <tr>
				                    	<th>Nombre d'acomptes</th>
				                        <th>Total acomptes</th>
				                    </tr>
				              	</thead>
				              	<tbody>
				              		<tr>
				              			<td class="lefterCol"><?php echo $totalReg ?></td>
				              			<td class="righterCol"><?php echo Util::priceFormat($totalRegAmounts) ?></td>
				              		</tr>
				              	</tbody>
				             </table>      
				    	</div>
				    	<div class="clear"></div>
				    	<?
					}?>
				</div>
			</form>
		</div>
	</div>
	<div class="spacer"></div>
	
	</div></div>
	
	
</div> <!-- mainContent fullWidthContent -->

	<script type='application/javascript'>
		$(document).ready(function() {
			$( '#reg' ).html( $( '#reg' ).html() + ' (<?php echo $totalFact ?>)' );

			$( '#dps' ).html( $( '#dps' ).html() + ' (<?php echo $totalReg ?>)' );

		});
	
		$(function() {
			
				$( '#containerEstimate' ).tabs({ fxFade: true , fxSpeed: 'fast' });
			
		});
		
		$(document).ready(function() {
			
			var options = {
				
				beforeSubmit:	preSubmitCallbackDP,  // pre-submit callback 
				success:		postSubmitCallbackDP  // post-submit callback
				
			};
			
			$( '#dpToValidate' ).ajaxForm( options );
			
		});
		
		function preSubmitCallbackDP( formData , jqForm , options ){
						
			if($( '#dpToValidate :checkbox' ).fieldValue().length == 0){
				alert( "Aucune case n'a été cochée !" );
				return false;
			}
			
			if(!confirm( 'Estes vous sur de vouloir valider ces acomptes, des règlements seront créés ?' ) )
				return false;

		}
		
		function postSubmitCallbackDP( responseText , statusText ){
						
			if( statusText != 'success' || responseText != '' ){
				
				$.growlUI( 'Une erreur est survenue' , 'Impossible d\'effectuer l\'enregistrement! <br/>'+responseText );
				return;
				
			}else{
							
				$.growlUI( '', 'Enregistrement effectué avec succès' );
				
				var orders =  $( '#dpToValidate :checkbox' ).fieldValue();
					
				for(i=0;i<orders.length;i++){
				
					if( $( '#orderRow_'+orders[i]).parent( 'tbody' ).children().length == 1 ){
						$( '#orderRow_'+orders[i]).parent( 'tbody' ).parent( 'table' ).parent( 'div' ).prev( 'div' ).remove();
						$( '#orderRow_'+orders[i]).parent( 'tbody' ).parent( 'table' ).parent( 'div' ).remove();
					}else{
						$( '#orderRow_'+orders[i]).remove();
					}
					
				}
			
				return;
			
			}
			
		}
	</script>
		
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}?>
	</p>
	<?php
	
}

//------------------------------------------------------------------------------------------

function fillArrayRegulations( &$arrayRegulations , $query ){
	
	$rs = DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		$idsupplier= $rs->fields( "idsupplier" );
		
		if( !isset($arrayRegulations[ $idsupplier ]) )
			$arrayRegulations[ $idsupplier ] = array();
		
		$i = count($arrayRegulations[ $idsupplier ]);
		
		$arrayRegulations[ $idsupplier ][ $i ][ "idregulations_supplier" ] 	= $rs->fields( "idregulations_supplier" );
		$arrayRegulations[ $idsupplier ][ $i ][ "idcredit_supplier" ]		= $rs->fields( "idcredit_supplier" );
		$arrayRegulations[ $idsupplier ][ $i ][ "credit_balance_used" ] 	= $rs->fields( "credit_balance_used" );
		$arrayRegulations[ $idsupplier ][ $i ][ "initial" ] 				= $rs->fields( "initial" );
		$arrayRegulations[ $idsupplier ][ $i ][ "deliv_payment" ] 			= $rs->fields( "deliv_payment" );
		$arrayRegulations[ $idsupplier ][ $i ][ "payment_date" ] 			= $rs->fields( "payment_date" );
		$arrayRegulations[ $idsupplier ][ $i ][ "name" ] 					= $rs->fields( "name" );
		$arrayRegulations[ $idsupplier ][ $i ][ "overpayment" ] 			= $rs->fields( "overpayment" );
		$arrayRegulations[ $idsupplier ][ $i ][ "Date" ] 					= usDate2eu( $rs->fields( "Date" ) );									
		$arrayRegulations[ $idsupplier ][ $i ][ "status" ]					= $rs->fields( "status" );
		$arrayRegulations[ $idsupplier ][ $i ][ "idpayment" ] 				= $rs->fields( "idpayment" );
		$arrayRegulations[ $idsupplier ][ $i ][ "payment" ] 				= $rs->fields( "payment" );
		$arrayRegulations[ $idsupplier ][ $i ][ "bank_date" ] 				= $rs->fields( "bank_date" );	
		$arrayRegulations[ $idsupplier ][ $i ][ "regulation_amount" ]		= $rs->fields( "amount" );
		
		$idrs = $rs->fields( "idregulations_supplier" );
		
		$billings = DBUtil::query( "SELECT brs.billing_supplier, bs.rebate_type, bs.rebate_value, bs.total_amount
									FROM billing_regulations_supplier brs, billing_supplier bs 
									WHERE brs.idregulations_supplier = $idrs 
									AND bs.idsupplier = $idsupplier
									AND brs.billing_supplier = bs.billing_supplier" );

		$a = 0;
		$bils = array();
		$creds = array();
		
		while( !$billings->EOF() ){

			$bils[ $billings->fields( 'billing_supplier' ) ][ 'amount' ] = $billings->fields( 'total_amount' );
			
			$rebateUsed = 0.00;						
			if( $billings->fields( 'rebate_type' ) == 'amount' ){
				$rebateUsed += $billings->fields( 'rebate_value' );
			}else{
				$rebateUsed += ( $billings->fields( 'total_amount' ) * $billings->fields( 'rebate_value' ) ) / 100 ;
			}
			$bils[ $billings->fields( 'billing_supplier' ) ][ 'rebate' ] = $rebateUsed;
			
			$billings->MoveNext();
		}

		$creditsSup = DBUtil::query( "SELECT idcredit_supplier, SUM(amount) as amount FROM credit_supplier_regulation WHERE idusage = $idrs GROUP BY idcredit_supplier" );
		while( !$creditsSup->EOF() ){
			$creds[ $creditsSup->fields( 'idcredit_supplier' ) ][ 'amount' ] = $creditsSup->fields( 'amount' );
			$creditsSup->MoveNext();
		}
		
		$amount = DBUtil::query( "SELECT amount FROM regulations_supplier WHERE idregulations_supplier = $idrs" )->fields( 'amount' );
		
		$arrayRegulations[ $idsupplier ][ $i ][ "billing_supplier" ] = $bils;
		$arrayRegulations[ $idsupplier ][ $i ][ "credit_supplier" ] = $creds;
		
		$rs->MoveNext();
	
	}
}

//------------------------------------------------------------------------------------------

function fillArrayCreditsOnly( &$arrayRegulations , $query ){

	$rs = DBUtil::query( $query );
					
	while( !$rs->EOF() ){

		$idsupplier= $rs->fields( "idsupplier" );
		$i = count($arrayRegulations[ $idsupplier ]);
		$arrayRegulations[ $idsupplier ][ $i ][ "idregulations_supplier" ] 	= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "idcredit_supplier" ]		= $rs->fields( "idcredit_supplier" );
		$arrayRegulations[ $idsupplier ][ $i ][ "credit_balance_used" ] 	= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "initial" ] 				= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "deliv_payment" ] 			= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "payment_date" ] 			= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "name" ] 					= $rs->fields( "name" );
		$arrayRegulations[ $idsupplier ][ $i ][ "overpayment" ] 			= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "Date" ] 					= usDate2eu( $rs->fields( "Date" ) );									
		$arrayRegulations[ $idsupplier ][ $i ][ "status" ]					= $rs->fields( "status" );
		$arrayRegulations[ $idsupplier ][ $i ][ "idpayment" ] 				= $rs->fields( "idpayment" );
		$arrayRegulations[ $idsupplier ][ $i ][ "payment" ] 				= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "bank_date" ] 				= '';
		$arrayRegulations[ $idsupplier ][ $i ][ "regulation_amount" ]		= 0.00;

		$idcs = $rs->fields( "idcredit_supplier" );
		$billings = DBUtil::query( "SELECT csr.billing_supplier, bs.rebate_type, bs.rebate_value, bs.total_amount
									FROM credit_supplier_regulation csr, billing_supplier bs 
									WHERE csr.idcredit_supplier = '$idcs'
									AND bs.idsupplier = $idsupplier
									AND csr.idusage = 0
									AND csr.billing_supplier = bs.billing_supplier" );
											
		$bils = array();
		
		while( !$billings->EOF() ){

			$bils[ $billings->fields( 'billing_supplier' ) ][ 'amount' ] = $billings->fields( 'total_amount' );
			
			$rebateUsed = 0.00;						
			if( $billings->fields( 'rebate_type' ) == 'amount' ){
				$rebateUsed += $billings->fields( 'rebate_value' );
			}else{
				$rebateUsed += ( $billings->fields( 'total_amount' ) * $billings->fields( 'rebate_value' ) ) / 100 ;
			}
			$bils[ $billings->fields( 'billing_supplier' ) ][ 'rebate' ] = $rebateUsed;
			
			$billings->MoveNext();
		}
		
		$arrayRegulations[ $idsupplier ][ $i ][ "billing_supplier" ] = $bils;
		$arrayRegulations[ $idsupplier ][ $i ][ "credit_supplier" ][ $rs->fields( 'idcredit_supplier' ) ][ 'amount' ] = $rs->fields( "amount" );
				
		$rs->MoveNext();
	
	}
		
}

//------------------------------------------------------------------------------------------
// fonction qui sort la liste des contacts
// param ; supplier contacts iterator
function getContactsTable( $it ){
	?>
	<table class="dataTable resultTable">
		<tr>
			<th>Nom</th>
			<th>Prénom</th>
			<th>Email</th>
			<th>Sélection</th>
		</tr>
		<?
		while( $it->hasNext() ){

			$contact =& $it->next();
			?>
			<tr>
				<td><?php echo htmlentities( $contact->getLastName() ); ?></td>
				<td><?php echo htmlentities( $contact->getFirstName() ); ?></td>
				<td><?php echo $contact->getEmail(); ?></td>
				<td class="checkSuppliersSend"><?php if($contact->getEmail()){ ?><input type="checkbox" value="<?php echo $contact->getId(); ?>" name="checkSupplierContact[]"><?php } ?></td>
			</tr><?
		}?>

	</table>
	<?
}

//------------------------------------------------------------------------------------------	
function sendRegulationsDetails( $destination , $arrayRegulations , $idsupplier ){
	global $GLOBAL_START_URL , $GLOBAL_START_PATH;
	
	$nbrResult = 0;
	foreach($arrayRegulations as $idsupplier => $Regulations ){
		$nbrResult += count( $Regulations );
	}
	
	if( $nbrResult == 0 )
		return "Il n'y a aucun règlement à envoyer";

	$fact = 0;

	foreach($arrayRegulations as $idsupplier => $Regulations ){
		
		$supplier = DBUtil::query("SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1")->fields( 'name' );
		
		$totalTTCFacts = 0;
		
		$htmlMail = "<div>$supplier</div>
	                    
	    <!-- tableau résultats recherche -->
	    <div>
	    	<table>
	        	<thead>
	                <tr>
	                    <th style=\"width:90px;\">Factures ou proformas n°</th>
	                    <th style=\"width:90px;\">Total TTC<br />factures</th>
	                    <th style=\"width:100px;\">Mode de paiement</th>
	                    <th style=\"width:70px;\">Escompte</th>
	                    <th style=\"width:70px;\">Avoirs<br/>utilisés</th>
	                    <th style=\"width:70px;\">Ecart règlement accepté</th>
	                    <th style=\"width:80px;\">Total réglé</th>
	                    <th style=\"width:70px;\">Trop payé</th>
	                    <th style=\"width:100px;\">Date de paiement</th>
	                    <th style=\"width:20px;\"></th>
	                </tr>
	            </thead>
	            <tbody>";
				
				$supplierTotalAmount = 0.00 ;
				foreach($Regulations as $key => $values ){
					
					$idrs 				= $values[ "idregulations_supplier" ];
					$idcs				= $values[ "idcredit_supplier" ];
					$billing_supplier 	= "";
					$credBalUsed 		= $values[ "credit_balance_used" ];
					$rebateUsed 		= 0.00;
					$com 				= $values[ "initial" ];								
					$deliv_payment 		= $values[ "deliv_payment" ]	==	"0000-00-00"	?	""	:	usDate2eu( $values[ "deliv_payment" ] );						
					$payment_date 		= $values[ "payment_date" ] 	==	"0000-00-00"	?	""	:	usDate2eu( $values[ "payment_date" ] );
					$overpayment 		= $values[ "overpayment" ];
					$dateFacturation 	= usDate2eu( $values[ "Date" ] );
					$status	 			= $values[ "status" ];
					$idpayment 			= $values[ "idpayment" ];
					$payment_name 		= $values[ "payment" ];
					$billings 			= $values[ "billing_supplier" ];
					$credits			= $values[ "credit_supplier" ];
					
					$htmlMail .= "
			        <tr id=\"regulationRow_$idrs\">
			        	<td style=\"\">
			        		<ul style=\"width:90px;list-style:none;margin:0;padding:0;\">";
			        		
			        			foreach( $billings as $idbil => $billing ){
			        				
			        				$htmlMail .= "<li style=\"width:90px;line-height:20px;\">$idbil</li>";
			        				
			        			}
			        			foreach( $credits as $idcred => $credit ){
			        				
			        				$htmlMail .= "<li style=\"width:90px;line-height:20px;color:red;\">$idcred</li>";
			        				
			        			}
			        			
			        	$htmlMail .= "
			        	</td>
			        	<td>
			        		<ul style=\"width:90px;list-style:none;margin:0;padding:0;\">";
			        		
			        			$totalAmount = 0.00;
			        			foreach( $billings as $idbil => $billing ){
			        				
			        				$htmlMail .= "<li style=\"width:90px;line-height:20px;\">" . Util::priceFormat($billings[ $idbil ][ 'amount' ]) . "</li>";
			        				
			        				$totalAmount += $billing[ 'amount' ];
			        			}
			        			$creditSupplierTotal = 0.00;
			        			foreach( $credits as $idcred => $credit ){
			        				
			        				$htmlMail .= "<li style=\"width:90px;line-height:20px;color:red;\">- " . Util::priceFormat($credits[ $idcred ][ 'amount' ]) . "</li>";
			        				
			        				$creditSupplierTotal += $credit[ 'amount' ];
			        			}
			       		$htmlMail .= "
			        	</td>
			        	<td>
			        		<ul style=\"width:100px;list-style:none;margin:0;padding:0;\">";
			        		
			        			foreach( $billings as $idbil => $billing ){
			        				$pname=DBUtil::query( "SELECT name_1 FROM payment p, billing_supplier bs WHERE bs.billing_supplier = '$idbil' AND p.idpayment = bs.idpayment LIMIT 1" )->fields( 'name_1' );
			        				
			        				$htmlMail .= "<li style=\"width:100px;line-height:20px;\">$pname</li>";
			        				
			        			}
			        			foreach( $credits as $idcred => $credit ){
			        				
			        				$htmlMail .= "<li style=\"width:100px;line-height:20px;color:red;\">-</li>";
			        				
			        			}
			        	$htmlMail .= "
			        	</td>
			        	<td>
			        		<ul style=\"width:70px;list-style:none;margin:0;padding:0;\">";
			        		
			        			$totalRebate = 0.00;
			        			foreach( $billings as $idbil => $billing ){
			        				
			        				$rebate = $billing[ 'rebate' ] ;
			        				
			        				$htmlMail .= "<li style=\"width:70px;line-height:20px;\">" . $rebate==0 ? "-" : Util::priceFormat($rebate) . "</li>";
			        				
			        				$totalRebate += $rebate;
			        			}
			        			foreach( $credits as $idcred => $credit ){
			        				
			        				$htmlMail .= "<li style=\"width:70px;line-height:20px;color:red;\">-</li>";
			        				
			        			}
			        	$htmlMail .= "
			        	</td>
			        	<td>" . $creditSupplierTotal > 0 ? Util::priceFormat( $creditSupplierTotal ) : "-" . "</td>
			        	<td>" . $overpayment != 0 ? Util::priceFormat( $overpayment ) : "-" . "</td>";
			        	
			        	$amount = $values[ 'regulation_amount' ];
			        	$supplierTotalAmount += $amount;
			        	
			        	$htmlMail .="
			        	<td style=\"text-align:right; white-space:nowrap;\">" . Util::priceFormat( $amount ) . "</td>
			        	<td>";
				        	
			        		$mytotal = $amount - $totalAmount - $totalRebate - $credBalUsed - $overpayment;
			        		if( $mytotal > 0 ){
			        			$htmlMail .= Util::priceFormat( $mytotal );
			        		}else{
			        			$htmlMail .= '-';
			        		}
		
			        	$htmlMail .= "
			        	</td>
			        	<td>$payment_date</td>
			        </tr>";
						
					$fact++;
					
				}
					$totalTTCFacts += $supplierTotalAmount;
					if( $fact > 1 ){
					$htmlMail .= "
					<tr>
						<th colspan=\"6\" style=\"border-style:none;\"></th>
						<th>" . Util::priceFormat( $supplierTotalAmount ) . "</th>
						<th colspan=\"2\" style=\"border-style:none;\"></th>
					</tr>";
					}
				$htmlMail .= "
				</tbody>
			</table>
		</div>";
	}
		
	// on charge les contacts
	$arrayAdresses = array();
	for( $i = 0 ; $i < count( $destination ) ; $i++ ){
		$idcontact = $destination[ $i ];
		$sup = new Supplier( intval( $idsupplier ) , $idcontact );
		
		$contactEmail = $sup->getContact()->getEmail();
		
		array_push( $arrayAdresses , $contactEmail );
	}

	include_once ("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$mailer = new htmlMimeMail();
	
	$AdminName = DBUtil::getParameterAdmin("ad_name");
	$AdminEmail = DBUtil::getParameterAdmin("ad_mail");
	
	$iduser = User::getInstance()->getId();

	if (!empty ($iduser)) {

		$CommercialName = User::getInstance()->get('firstname') . ' ' . User::getInstance()->get('lastname');
		$CommercialEmail = User::getInstance()->get('email');
		$CommercialPhone = User::getInstance()->get('phonenumber');
	} else {

		$CommercialName = DBUtil::getParameterAdmin('ad_name');
		$CommercialEmail = DBUtil::getParameterAdmin('ad_mail');

	}	
	
	$mailer->setFrom('"' . $AdminName . ' - ' . $CommercialName . '" <' . $CommercialEmail . '>');
	
	$mailer->setHtml( $htmlMail );
	$mailer->setSubject( "détail des règlements demandés" );
	$result = $mailer->send( $arrayAdresses , 'mail' );

	return "1";
}
?>