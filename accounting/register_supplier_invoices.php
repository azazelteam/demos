<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement des factures fournisseurs
 */
 
include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );

//------------------------------------------------------------------------------------------
/* calcul date d'échéance */

if( isset( $_GET[ "idpayment_delay" ] ) ){
	
	if( preg_match( "/[0-9]{2}-[0-9]{2}-[0-9]{4}/", $_GET[ "date" ] ) ){
		
		list( $day, $month, $year ) = explode( "-", $_GET[ "date" ] );
		$date = "$year-$month-$day";
			
	}
	else $date = date( "Y-m-d" );
	
	include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
	
	$date = Payment::getPaymentDelayDate( $date, intval( $_GET[ "idpayment_delay" ] ) );
	
	if( !strlen( $date ) )
		exit();
	
	list( $year, $month, $day ) = explode( "-", $date );
	
	exit( "$day-$month-$year" );
	
}

//------------------------------------------------------------------------------------------
/* traitement ajax */

if( isset( $_POST[ "idorder_supplier" ] ) && $_POST[ "idorder_supplier" ] > "0" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-type: text/xml; charset=UTF-8" ); 
	
	updateSupplierOrder();
	
	echo "<root>";
	
	displayAmounts();
	
	echo "</root>";
	
	exit();
		
}

//------------------------------------------------------------------------------------------
/* traitement ajax 2m le retour */

if( isset( $_POST[ "registerInvoice" ] ) && $_POST[ "registerInvoice" ] == "1" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" ); 
	
	$_POST =& Util::arrayMapRecursive( "Util::doNothing", $_POST );
	
	$result = registerInvoice();
	
	exit($result);
		
}

$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
if( !isset( $_GET[ "bl" ] ) || !isset ( $_GET[ "supplier" ] ) ){
	
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}

$bl = $_GET["bl"];
$temp_bls = explode(',',$bl);
$temp_bl = "";
for ( $ii=0;$ii<count($temp_bls);$ii++){
	if(!$ii)
		$temp_bl .= "'".$temp_bls[$ii]."'";
	else
		$temp_bl .= "'".$temp_bls[$ii]."',";
}
// Correction: point 17 Regrouper factures fournisseurs
// un message d'erreurs s'affichent. 
$temp_bl = $bl;
// $bl = preg_replace('`([^0-9,]+)`iS','',$bl);

$idsupplier = $_GET["supplier"];

$today = date("Y-m-d");

$bls = explode(",",$bl);

//$amounts = getAmountsFromBls( $bls );

if( count( $bls )>1 ){
	$title = "Bons de livraisons concernés : ";
	
	for($i=0;$i<count($bls);$i++){
		if($i)
			$title.="-";
		
		$title.=$bls[$i];	
	}	
}else{
	$title = "Bon de livraison concerné : ".$bls[0];
}


displayOrdersSuppliersInfos();

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------------
function getSupplierName( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du fournisseur" );
		
	return $rs->fields( "name" );
	
}

//------------------------------------------------------------------------------------------------------
function getSupplierPaymentDelay( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT pay_delay FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le délai de paiement du fournisseur" );
		
	return $rs->fields( "pay_delay" );
	
}


//---------------------------------------------------------------------------------------------------------------------
function displayOrdersSuppliersInfos(){
	
	global $temp_bl, $bls, $idsupplier, $GLOBAL_START_URL;
	
	if( count( $bls )>1 ){
		$title = "Bons de livraisons concernés : ";
	
		for($i=0;$i<count($bls);$i++){
			if($i)
				$title.="-";
		
			$title.=$bls[$i];	
		}	
	}else{
		$title = "Bon de livraison concerné : ".$bls[0];
	}
	

	/**
	 * Commandes fournisseurs
	 */
	
	$in='';
	
	for($i=0;$i<count($bls);$i++){
		
		$idbl_delivery = $bls[$i];
		
		if(!$i)
			$in .= "'".$idbl_delivery."'";
			
		$in .= ",'".$idbl_delivery."'";
	
	}
	
	$query = "SELECT DISTINCT(idorder_supplier)
				FROM bl_delivery
				WHERE idbl_delivery IN ($in)";
	$rs = DBUtil::query($query);
	
	if( $rs === false )
		die("Impossible de récupérer les commandes fournisseurs");
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
			/* <![CDATA[ */
				
				/* ----------------------------------------------------------------------------------- */
				
				$(document).ready(function() { 
					
					 var options = {
					 	
					 	dataType:  'xml',
						beforeSubmit:  preSubmitCallback,  // pre-submit callback 
						success:	   postSubmitCallback  // post-submit callback 
		  				
					};
					
					<?
					for($os=0;$os<$rs->RecordCount();$os++){?>
						$('#frm<?=$rs->fields("idorder_supplier")?>').ajaxForm( options );
						<?
						$rs->MoveNext();
					}?>
				});
				
				/* ----------------------------------------------------------------------------------- */
				
				function preSubmitCallback( formData, jqForm, options ){
					
					
					$.blockUI({
						
						message: "Traitement en cours",
						css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
						fadeIn: 0, 
						fadeOut: 700
						
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function postSubmitCallback( responseXML, statusText ){ 
					
					$.unblockUI();
					
					if( statusText != 'success' ){
						
						alert( "Impossible d'effectuer le traitement" );
						return;
						
					}
					
					<?
					$rs->MoveFirst();
					
					for($os=0;$os<$rs->RecordCount();$os++){
						
						$idorder_supplier = $rs->fields("idorder_supplier");
						
						$SupplierOrder = new SupplierOrder( $idorder_supplier );
	
						$i = 0;
						while( $i < $SupplierOrder->getItemCount() ){?>
		
							var totalrow<?=$i.$idorder_supplier?> = $(responseXML).find('totalrow<?=$i.$idorder_supplier?>').text();
                          	
                    		document.getElementById("totalrow<?=$i.$idorder_supplier?>").innerHTML = priceFormat( totalrow<?=$i.$idorder_supplier?> );
                    
                    		<?php
							$i++;
						}
						?>
						var totalosht<?=$idorder_supplier?> = $(responseXML).find('totalosht<?=$idorder_supplier?>').text();
						document.getElementById("totalosht<?=$idorder_supplier?>").innerHTML = priceFormat( totalosht<?=$idorder_supplier?>);

						var totalosttc<?=$idorder_supplier?> = $(responseXML).find('totalosttc<?=$idorder_supplier?>').text();
						
						document.getElementById("totalosttc<?=$idorder_supplier?>").innerHTML = priceFormat( totalosttc<?=$idorder_supplier?> );
						
						<?
						
						$rs->MoveNext();
						
					}?>
					
					var totalht = $(responseXML).find('totalht').text();
					document.getElementById("totalht").innerHTML = priceFormat( totalht );
					
					var totalttc = $(responseXML).find('totalttc').text();
					document.getElementById("totalttc").innerHTML = priceFormat( totalttc );
	
					var reg = new RegExp( "(\\.)", "g" );
					
					$( "#total_amount_ttc" ).val( totalttc.replace( reg, "," ) );
					
					$( "#chargesET" ).html( priceFormat( $(responseXML).find('chargeset').text() ) );
					$( "#total_charge_ht" ).val( $(responseXML).find('chargeset').text().replace( reg, "," ) );
					$( "#tva" ).html( priceFormat( parseFloat( $( responseXML ).find('totalttc').text() ) - parseFloat( $(responseXML).find('totalht').text() ) ) );
					
					$.growlUI( '', 'Mise à jour effectuée avec succès' );
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function priceFormat( value ){
				
					var ret = parseFloat( value ).toFixed( 2 );
					var reg = new RegExp( "(\\.)", "g" );
					ret = ret.replace( reg, "," );
					
					return ret + ' &euro;';
					
				}
			
        	 function recalculatetotal(){
		 	var totalht = $( "input[name='total_ht']" ).val( );
var totalttc = $( "input[name='total_amount_ttc']" ).val( );

			
		
if( totalttc){
				var vat = totalttc - totalht;
		
		
	$( "#total_vat" ).html( vat.toFixed( 2 ) );
	}
	else {
	vat = 0.00;
	$( "#total_vat" ).html( vat.toFixed( 2 )  );
	}
		 
			document.getElementById("totalosht<?=$idorder_supplier?>").innerHTML = priceFormat( totalht );
	document.getElementById("totalosttc<?=$idorder_supplier?>").innerHTML = priceFormat( totalttc );
	document.getElementById("tva").innerHTML = priceFormat( vat );
	document.getElementById("total_vatt").value = vat;
		document.getElementById("totalht").innerHTML = priceFormat( totalht );
		document.getElementById("totalttc").innerHTML = priceFormat( totalttc );
	

		
			
		}
				/* ----------------------------------------------------------------------------------- */
				
	/* ]]> */
	</script>
	<style type="text/css">
			<!--
			div#global{
				min-width:0;
				width:auto;
			}

			-->
			</style>
	<div id="globalMainContent" style="width:770px;">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent" style="float:right; width:380px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Montants</p>
			</div>
			<div class="subContent">
				<div class="resultTableContainer clear" style="margin-top:20px;">
					<table class="dataTable resultTable">
						<tr>
							<th class="filledCell">Total HT</th>
							<td class="righterCol" id="totalht"></td>
						</tr>
						<tr>
							<th class="filledCell">Port HT</th>
							<td class="righterCol" id="chargesET"></td>
						</tr>
						<tr>
							<th class="filledCell">TVA</th>
							<td class="righterCol" id="tva"></td>
						</tr>
						<tr>
							<th class="filledCell">Total TTC</th>
							<td class="righterCol" id="totalttc"></td>
						</tr>
					</table>
				</div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<div class="mainContent" style="width:380px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Commandes <?=getSupplierName($idsupplier)?></p>
			</div>
			<?
				$query = "SELECT reference,`ref_supplier`,`quantity`,`designation` 
				FROM `bl_delivery_row` 
				WHERE idbl_delivery IN ($temp_bl)";
			$result = DBUtil::query( $query );
			$bl_rows = '';
			while( !$result->EOF() ){
				
				$bl_rows .= "<tr>
					<td>".$result->fields("reference")."</td>
					<td>".$result->fields("ref_supplier")."</td>
					<td>".$result->fields("quantity")."</td>
					<td>".$result->fields("designation")."</td>
					</tr>";
				$result->MoveNext();
				
			}
			
			?>
            <div class='subContent'>
                <div class='resultTableContainer'>
                    <table class='dataTable resultTable' >
                        <thead>
                            <tr>
                                <th colspan="4"><?=$title?></th>
                            </tr>
                            <tr>
                                <th>Référence</th>
                                <th>Réf. fsr</th>
                                <th>Quantité</th>
                                <th width='100px'>Désignation</th>
                            </tr>
                        </thead>
                        <tbody>
                                <?=$bl_rows?>
                        </tbody>
                    </table>
                </div>
            </div>
			<div class="subContent">
				<!--<div><?=$title?></div>-->
					
	
	<?
	
	$totalHTToInvoice = 0;
	$totalTTCToInvoice = 0;
	$chargesET = 0.0;
	
	$rs->MoveFirst();
	
	for($i=0;$i<$rs->RecordCount();$i++){
		
		$idorder_supplier = $rs->fields("idorder_supplier");
		
		$SupplierOrder = new SupplierOrder( $idorder_supplier );
		
		$totalHTToInvoice += $SupplierOrder->getTotalET();
		$totalTTCToInvoice += $SupplierOrder->getTotalATI();
		$chargesET += $SupplierOrder->getChargesET();
		
		?>
		<div class="resultTableContainer clear" style="margin-top:20px;">
			<form name="frm<?=$idorder_supplier?>" id="frm<?=$idorder_supplier?>" enctype="multipart/form-data" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_invoices.php?bl=<?=$bl?>&amp;supplier=<?=$idsupplier?>">
			<input type="hidden" name="idorder_supplier" value="<?=$idorder_supplier?>" />
			<table class="dataTable resultTable">
				<thead>
					<tr>
						<th colspan="5">Commande fournisseur n° <?=$idorder_supplier?></th>
					</tr>
					<tr>
						<th>N/Réf.</th>
						<th>Réf. fsr</th>
						<th>Quantité</th>
						<th>Prix net HT</th>
						<th>Total HT</th>
					</tr>	
				</thead>
				<tbody>
				<?for ( $k = 0; $k < $SupplierOrder->getItemCount(); $k++ ) {?>
					<tr>
						<td class="lefterCol"><?=$SupplierOrder->getItemAt($k)->get('reference');?></td>
						<td><?=$SupplierOrder->getItemAt($k)->get('ref_supplier');?></td>
						<td><?=$SupplierOrder->getItemAt($k)->get('quantity');?></td>
						<td><input type="text" name="discount_price_<?=$k?>" value="<?=Util::numberFormat($SupplierOrder->getItemAt($k)->get('discount_price'));?>" class="textInput" style="width:60px;" />
						<input type="hidden" name="old_discount_price_<?=$k?>" value="<?=Util::numberFormat($SupplierOrder->getItemAt($k)->get('discount_price'));?>" />
						</td>
						<td class="righterCol" id="totalrow<?=$k.$idorder_supplier?>"><?=Util::priceFormat($SupplierOrder->getItemAt($k)->get('quantity')* $SupplierOrder->getItemAt($k)->get('discount_price'));?></td>
					</tr>
				<?php } ?>
				<tr>
					<td colspan="4" class="lefterCol">Coût port vente fournisseur HT</td>
					<td class="righterCol"><input type="text" name="total_charge_ht" value="<?=Util::numberFormat($SupplierOrder->get('total_charge_ht'))?>" class="textInput" style="width:60px;" /></td>
				</tr>
				<tr>
					<td colspan="4" class="lefterCol">N° commande fournisseur interne</td>
					<td class="righterCol"><input type="text" name="n_order_supplier" value="<?=$SupplierOrder->get('n_order_supplier')?>" class="textInput" style="width:60px;" /></td>
				</tr>
				<tr>
					<td colspan="2" class="lefterCol">Frais divers HT</td>
					<td class="righterCol"><input type="text" name="other_charges" class="textInput" style="width:60px;" value="<?=Util::numberFormat($SupplierOrder->get("other_charges" ))?>" /></td>
					<td class="lefterCol">Taxes diverses HT</td>
					<td class="righterCol"><input type="text" name="other_taxes" class="textInput" style="width:60px;" value="<?=Util::numberFormat($SupplierOrder->get("other_taxes" ))?>" /></td>
				</tr>
				<tr>
					<th class="filledCell" colspan="2">Total HT</td>
					<td class="righterCol" id="totalosht<?=$idorder_supplier?>"><?=Util::priceFormat($SupplierOrder->getTotalET())?></td>
					<th class="filledCell">Total TTC</td>
					<td class="righterCol" id="totalosttc<?=$idorder_supplier?>"><?=Util::priceFormat($SupplierOrder->getTotalATI())?></td>
				</tr>
				</tbody>
			</table>
		<div class="submitButtonContainer" style="margin-top:10px;">
			<input type="submit" name="UpdateButton" value="Modifier" class="blueButton" />
		</div>
		</form>
		</div>
	<?
		$rs->MoveNext();
	}?>
	</div>
	<script type="text/javascript">
	document.getElementById("totalttc").innerHTML = '<?=Util::priceFormat($totalTTCToInvoice)?>';
	document.getElementById("totalht").innerHTML = '<?=Util::priceFormat($totalHTToInvoice)?>';
	$( "#chargesET" ).html( "<?php echo Util::priceFormat( $chargesET ); ?>" );
	document.getElementById("tva").innerHTML = '<?=Util::priceFormat($totalTTCToInvoice-$totalHTToInvoice)?>';
		
	</script>
	</div>
	<div class="bottomRight"></div><div class="bottomLeft"></div>
						</div>	
	<div class="mainContent" style="float:right; width:380px;margin-top:20px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Enregistrement facture <?if(isset($_GET["proforma"])){?>proforma <?php } ?><?=getSupplierName($idsupplier)?></p>
			</div>
			<div class="subContent">
				<?displayInvoice();?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<?
}

//---------------------------------------------------------------------------------------------------------------------
function displayInvoice(){
	
	global $temp_bl, $title, $GLOBAL_START_URL, $bl, $idsupplier;
	

	$totalATI 	= 0.0;
	$totalET	= 0.0;
	$chargesET 	= 0.0;
$tva 	= 0.0;
	$rs =& DBUtil::query( "SELECT DISTINCT( idorder_supplier ) FROM bl_delivery WHERE idbl_delivery IN( " . $temp_bl . " )" );
	
	/*$down_payment_amount 	= 0.0;
	$down_payment_date 		= "0000-00-00";
	$down_payment_idpayment = 0;
	$down_payment_comment 	= "";*/
	
	while( !$rs->EOF() ){
		
		$supplierOrder = new SupplierOrder( $rs->fields( "idorder_supplier" ) );
$totalET 				+= round( $supplierOrder->getTotalET(), 2 );
		$totalATI 				+= round( $supplierOrder->getTotalATI(), 2 );
		$chargesET 				+= $supplierOrder->getChargesET();
		$tva = $totalATI - $totalET;
		//$down_payment_amount 	+= $supplierOrder->getDownPaymentAmount();
		//$down_payment_date 		= max( $down_payment_date, $supplierOrder->get( "down_payment_date" ) );
		//$down_payment_idpayment = max( $down_payment_idpayment, $supplierOrder->get( "down_payment_idpayment" ) );
		
		/*if( strlen( $supplierOrder->get( "down_payment_comment" ) ) )
			$down_payment_comment .= strlen( $down_payment_comment ) ? "\n" . $supplierOrder->get( "down_payment_comment" ) : $supplierOrder->get( "down_payment_comment" );*/
		
		//règlements par acompte déjà enregistrés
		
		/*$query = "
		SELECT SUM( rs.amount ) AS amount
		FROM regulations_supplier rs, billing_regulations_supplier brs, billing_supplier_row bsr, order_supplier os, bl_delivery bl
		WHERE os.idorder_supplier = '" . $supplierOrder->getId() . "'
		AND bl.idorder_supplier = os.idorder_supplier
		AND bsr.idbl_delivery = bl.idbl_delivery
		AND bsr.idsupplier = bl.idsupplier
		AND brs.billing_supplier = bsr.billing_supplier
		AND brs.idsupplier = bsr.idsupplier
		AND rs.idregulations_supplier = brs.idregulations_supplier
		AND rs.from_down_payment = 1";
		
		if( ( $amount = DBUtil::query( $query )->fields( "amount" ) ) != NULL )
			$down_payment_amount -= max( $down_payment_amount, $amount );*/

		$rs->MoveNext();
			
	}

	?>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var optionsInvoices = {
			 	
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postInvoiceSubmitCallback  // post-submit callback 
  				
			};
			
			$('#frm').ajaxForm( optionsInvoices );
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function postInvoiceSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' ){
				
				alert( "Impossible d'effectuer le traitement" );
				return;
				
			}
			
			if( responseText == "0" ){
				<?if(isset($_GET["proforma"])){?>
					$.growlUI( '', 'Facture proforma enregistrée' );
				<?php } else{?>
					$.growlUI( '', 'Facture enregistrée' );
				<?php } ?>
				window.opener.goForm();
				setTimeout('self.close();',2000); 
			}else{
				$.growlUI( '', responseText );
			}
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function getPaymentDate(){

			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_invoices.php?idpayment_delay=" + $( "#idpayment_delay" ).val() + "&date=" + $( "#Datef" ).val(),
				async: true,
			 	success: function( responseText ){

					if( responseText.length )
						$( "#deliv_payment" ).val( responseText );
	
				}

			});
			
		}
		
		$(document).ready( function(){ getPaymentDate() } );
		
	/* ]]> */
	</script>
	<style type="text/css">
	
		div.mainContent div.content div.subContent table.dataTable input.price,
		div.mainContent div.content div.subContent table.dataTable input.percentage{ text-align:right; width:50px; }
			
	</style>
	<div><?=$title?></div>
	<?php
	$n_temp_bl = str_replace("'", "", $temp_bl);
	?>
				<form enctype="multipart/form-data" id="frm" method="post" action="
				<?if(isset($_GET["proforma"])){?>
					<?=$GLOBAL_START_URL ?>/accounting/register_supplier_invoices.php?proforma&bl=<?=$n_temp_bl?>&amp;supplier=<?=$idsupplier?>">
				<?php } else{?>
					<?=$GLOBAL_START_URL ?>/accounting/register_supplier_invoices.php?bl=<?=$n_temp_bl?>&amp;supplier=<?=$idsupplier?>">
				<?php } ?>
				<input type="hidden" name="registerInvoice" value="1" />
					<input type="hidden" name="idsupplier" value="<?php echo $idsupplier ?>" />
					<input type="hidden" name="bl" value="<?php echo $n_temp_bl ?>" />
					<input type="hidden" name="Date" id="Date" value="<?php echo date( "d-m-Y" ) ?>" />
                       <input type="hidden" id="total_vatt" name="total_vatt" class="textInput price" value="<?php echo number_format( round( $tva, 2 ), 2, ",", "" ); ?>" />
					<div class="resultTableContainer" style="margin-top:20px;">
						<table class="dataTable">
							<tr>
								<th class="filledCell" style="width:30%;">
								<?if(isset($_GET["proforma"])){?>
									N° de facture proforma Fournisseur 
								<?php } else{?>
									N° de facture Fournisseur 
								<?php } ?>
								<span class="asterix">*</span></th>
								<td><input type="text" name="billing_supplier" class="textInput" /></td>
							</tr>
							<tr>
								<th class="filledCell">Date de facture <span class="asterix">*</span></th>
								<td>
									<input type="text" name="Datef" id="Datef" class="calendarInput" value="<?php echo date( "d-m-Y" ); ?>" />
									<?php DHTMLCalendar::calendar( "Datef" ); ?>
								</td>
							</tr>
							<tr>
							<?php 
								$rs_1 = DBUtil::query( "SELECT `idpaiement`  FROM `supplier` WHERE `idsupplier` = '$idsupplier' limit 1 " );
								$idpaiement =  $rs_1->fields( "idpaiement" );
							?>
								<th class="filledCell">Mode de paiement <span class="asterix">*</span></th>
								<td><?=DrawLift_modepayment($idpaiement)?></td>
							</tr>
							<tr>
								<th class="filledCell">Délai de paiement <span class="asterix">*</span></th>
								<td>
								<?php
									
									global $GLOBAL_START_PATH;
									
									include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );
									
									$pay_delay = getSupplierPaymentDelay($idsupplier);		
									
									XHTMLFactory::createSelectElement( 

										"payment_delay", 
										"idpayment_delay", 
										"payment_delay" . User::getInstance()->getLang(), 
										"idpayment_delay", 
										$pay_delay,
										false,
										"",
										"name=\"idpayment_delay\" id=\"idpayment_delay\" onchange=\"getPaymentDate();\" onkeyup=\"this.onchange();\""
										
									);

								?>
								</td>
							</tr>
							<tr>
								<th class="filledCell">Date d'échéance <span class="asterix">*</span></th>
								<td>
									<input type="text" name="deliv_payment" id="deliv_payment" class="calendarInput" value="" />
									<?php DHTMLCalendar::calendar( "deliv_payment" ); ?>
								</td>
							</tr>
                              <tr>
								<th class="filledCell">Montant HT <span class="asterix">*</span></th>
								<td>  <input type="text" id="total_ht" name="total_ht" class="textInput price" value="<?php echo number_format( round( $totalET, 2 ), 2, ".", "" ); ?>" onchange="recalculatetotal();" onkeyup="this.onchange(event);"  /> &euro;</td>
							</tr>
                            <tr>
								<th class="filledCell">TVA <span class="asterix">*</span></th>
								<td id="total_vat"><?php echo number_format( round( $tva, 2 ), 2, ",", "" ); ?></td>
							</tr>
							<tr>
							<tr>
								<th class="filledCell">Montant TTC <span class="asterix">*</span></th>
								<td><input type="text" id="total_amount_ttc" name="total_amount_ttc" class="textInput price" value="<?php echo number_format( round( $totalATI, 2 ), 2, ".", "" ); ?>"  onchange="recalculatetotal();" onkeyup="this.onchange(event);" /> &euro;</td>
							</tr>
							<tr>
								<th class="filledCell">Escompte</th>
								<td><input type="text" name="rebate_rate" class="textInput percentage" value="0" /> %</td>
							</tr>
							<tr>
								<th class="filledCell">Frais de port HT <span class="asterix">*</span></th>
								<td><input type="text" id="total_charge_ht" name="total_charge_ht" class="textInput price" value="<?php echo number_format( $chargesET, 2, ",", "" ); ?>" /> &euro;</td>
							</tr>
							<tr>
								<th class="filledCell">Facture</th>
								<td>
									<input type="file" class="textInput" name="billing_supplier_doc" />
								</td>
							</tr>
						</table>
					</div>
					<?php
					/*
					<div class="resultTableContainer" style="margin-top:20px;">
						<table class="dataTable">
							<tr>
								<th class="filledCell">Acompte</th>
								<td colspan="2"><input type="text" name="down_payment_amount" class="textInput percentage" value="<?php echo number_format( $down_payment_amount, 2, ",", "" ); ?>" /> &euro;</td>
							</tr>
							<tr>
								<th class="filledCell">Date de règlement</th>
								<td colspan="2">
									<input type="text" name="down_payment_date" id="down_payment_date" class="calendarInput" value="<?php if( $down_payment_date != "0000-00-00" ) echo str_replace( "/", "-", Util::dateFormatEu( $down_payment_date ) ); ?>" />
									<?php DHTMLCalendar::calendar( "down_payment_date" ); ?>
								</td>
							</tr>
							<tr>
								<th class="filledCell">Mode de règlement</th>
								<td colspan="2">
									<?php
									
										include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );
										
										XHTMLFactory::createSelectElement( 

											"payment", 
											"idpayment", 
											"name" . User::getInstance()->getLang(), 
											"name" . User::getInstance()->getLang(),
											$down_payment_idpayment,
											false,
											"",
											"name=\"down_payment_idpayment\""
											
										);
									
								?>
								</td>
							</tr>
							<tr>
								<th class="filledCell">Commentaires</th>
								<td colspan="2">
									<textarea name="down_payment_comment" style="width:100%;"><?php echo htmlentities( $down_payment_comment ); ?></textarea>
								</td>
							</tr>
						</table>
					</div>
					*/ ?>
					<div style="float:left; margin-top:10px;">Les champs marqués par <span class="asterix">*</span> sont obligatoires</div>
					<br style="clear:both;" />
					<div class="submitButtonContainer">
						<input type="submit" name="register" value="Enregistrer" class="blueButton" />
					</div>
				</form>	
	<?
}

//---------------------------------------------------------------------------------------------------------------------
function updateSupplierOrder(){
	
	$idorder_supplier = $_POST["idorder_supplier"];
	
	$SupplierOrder = new SupplierOrder( $idorder_supplier );
	
		
		
	$i = 0;
	while( $i < $SupplierOrder->getItemCount() ){
			
		if( isset( $_POST[ "quantity_$i" ] ) )
			$SupplierOrder->getItemAt( $i )->set( "quantity", Util::text2num( $_POST[ "quantity_$i" ] ));
		
		if( $_POST[ "discount_price_$i" ]!=$_POST[ "old_discount_price_$i" ] ){
	
			$unit_price = $SupplierOrder->getItemAt($i)->get("unit_price");
			$ref_discount_price = Util::text2num( $_POST[ "discount_price_$i" ] );
				
			$ref_discount_rate = ( 1.0 - $ref_discount_price / $unit_price ) * 100.0;
			
			$SupplierOrder->getItemAt( $i )->set( "ref_discount" , $ref_discount_rate );
			$SupplierOrder->getItemAt( $i )->set( "discount_price" , $ref_discount_price );
					
			updateEstimateOrderAmounts($i, $SupplierOrder);
		
		}
		
		$i++;
	}
	
			
	//Coût port achat transporteur HT
		
	if( isset( $_POST[ "internal_supplier_charges" ] ) ){
		
		$internal_supplier_charges = Util::text2num( $_POST[ "internal_supplier_charges" ] );
		
		/* mise à jour dans la commande */
		
		if( $SupplierOrder->get( "idorder" ) ){
		
			$query = "
			UPDATE order_charges 
			SET internal_supplier_charges = '$internal_supplier_charges',
			internal_supplier_charges_auto = 0
			WHERE idorder = '" . $SupplierOrder->get( "idorder" ) . "' 
			AND idsupplier = '" . $SupplierOrder->get( "idsupplier" ) . "' 
			LIMIT 1";

			DBUtil::query( $query );
			
		}

		/* mise à jour dans le devis */
		
		$rs =& DBUtil::query( "SELECT idestimate FROM `order` WHERE idorder = '" . $SupplierOrder->get( "idorder" ) . "' LIMIT 1" );;
		
		if( $rs->RecordCount() ){
			
			$query = "
			UPDATE estimate_charges 
			SET internal_supplier_charges = '$internal_supplier_charges',
			internal_supplier_charges_auto = 0
			WHERE idestimate = '" . $rs->fields( "idestimate" ) . "' 
			AND idsupplier = '" . $SupplierOrder->get( "idsupplier" ) . "' 
			LIMIT 1";

			DBUtil::query( $query );
			
		}

	}
	
	//Coût port vente fournisseur HT
	
	if( isset( $_POST[ "total_charge_ht" ] ) ){
		
		$charges = Util::text2num( $_POST[ "total_charge_ht" ] );
		
		/* mise à jour dans la commande */
		
		if( $SupplierOrder->get( "idorder" ) ){
		
			$query = "
			UPDATE order_charges 
			SET charges = '$charges',
			charges_auto = 0
			WHERE idorder = '" . $SupplierOrder->get( "idorder" ) . "' 
			AND idsupplier = '" . $SupplierOrder->get( "idsupplier" ) . "' 
			LIMIT 1";

			DBUtil::query( $query );

		}

		/* mise à jour dans le devis */
		
		$rs =& DBUtil::query( "SELECT idestimate FROM `order` WHERE idorder = '" . $SupplierOrder->get( "idorder" ) . "' LIMIT 1" );;
		
		if( $rs->RecordCount() ){
			
			$query = "
			UPDATE estimate_charges 
			SET charges = '$charges',
			charges_auto = 0
			WHERE idestimate = '" . $rs->fields( "idestimate" ) . "' 
			AND idsupplier = '" . $SupplierOrder->get( "idsupplier" ) . "' 
			LIMIT 1";

			DBUtil::query( $query );
			
		}
		
		$SupplierOrder->set( "total_charge_ht", $charges );
		
	}

	/* champs de compta navision */
		
	if( $SupplierOrder->get( "idsupplier" ) != DBUtil::getParameterAdmin( "internal_supplier" ) ){
		
		$query = "
		SELECT *
		FROM order_charges
		WHERE idorder = '" . $SupplierOrder->get( "idorder" ) . "' 
		AND idsupplier = '" . $SupplierOrder->get( "idsupplier" ) . "' 
		LIMIT 1";
			
		$rs =& DBUtil::query( $query );
		
		$transit = DBUtil::getDBValue( "transit", "supplier", "idsupplier", $SupplierOrder->get( "idsupplier" ) );

		$SupplierOrder->set( "charge_carrier_cost", $transit && $rs->RecordCount() ? $rs->fields( "charges" ) : 0.0 );
		$SupplierOrder->set( "charge_carrier_sell", $transit && $rs->RecordCount() ? $rs->fields( "internal_supplier_charges" ) : 0.0 );
		$SupplierOrder->set( "charge_carrier", $transit && $rs->RecordCount() ? $rs->fields( "charges" ) + $rs->fields( "internal_supplier_charges" ) : 0.0 );

	}
	
	if( isset( $_POST[ "other_charges" ] ) ){
			
		$other_charges = Util::text2num( $_POST[ "other_charges" ] );
		$SupplierOrder->set( "other_charges", $other_charges );
		
	}
		
	if( isset( $_POST[ "other_taxes" ] ) ){
			
		$other_taxes = Util::text2num( $_POST[ "other_taxes" ] );
		$SupplierOrder->set( "other_taxes", $other_taxes );
		
	}
	
	//Numéro de commande fournisseur interne	
	if(isset($_POST["n_order_supplier"])){
		$SupplierOrder->set( "n_order_supplier" , stripslashes( $_POST[ "n_order_supplier" ] ));
	}
		
	$SupplierOrder->save();
	
}

//---------------------------------------------------------------------------------------------------

function updateEstimateOrderAmounts($row, $supplierOrder){
	
	
	
	$db = &DBUtil::getConnection();
	
	$idarticle = $supplierOrder->getItemAt($row)->get("idarticle");
	$unit_cost = $supplierOrder->getItemAt($row)->get("unit_price");
	$unit_cost_rate = $supplierOrder->getItemAt($row)->get("ref_discount");
	$unit_cost_amount = $supplierOrder->getItemAt($row)->get("discount_price");
	$idorder = $supplierOrder->get("idorder");
	
	//Mise à jour de la commande
	$qcde = "UPDATE order_row SET unit_cost=$unit_cost, unit_cost_rate=$unit_cost_rate, unit_cost_amount=$unit_cost_amount WHERE idorder=$idorder AND idarticle=$idarticle";
	$rcde = $db->Execute( $qcde );
	
	if($rcde===false)
		die("Impossible de mettre à jour la ligne article de la commande");
		
		
	//Devis ???
	
	$q = "SELECT idestimate FROM `order` WHERE idorder=$idorder";
	$r = $db->Execute( $q );
		
	if(!$r)
		die("Impossible de récupérer le numéro de devis");
		
	$idestimate = $r->fields("idestimate");
		
	//////DEVIS///////
	if($idestimate>0){
		
		//Mise à jour de la commande
		$qest = "UPDATE estimate_row SET unit_cost=$unit_cost, unit_cost_rate=$unit_cost_rate, unit_cost_amount=$unit_cost_amount WHERE idestimate=$idestimate AND idarticle=$idarticle";
		$rest = $db->Execute( $qest );
	
		if($rest===false)
			die("Impossible de mettre à jour la ligne article du devis");
		
	}
}


//---------------------------------------------------------------------------------------------------
function displayAmounts(){
	
	$bl = $_GET["bl"];
	// $bl = preg_replace('`([^0-9,]+)`iS','',$bl);
	$bls = explode(",",$bl);

	/**
	 * Commandes fournisseurs
	 */
	
	$in='';
	
	for($i=0;$i<count($bls);$i++){
		
		$idbl_delivery = $bls[$i];
		
		if(!$i)
			$in .= "'".$idbl_delivery."'";
			
		$in .= ",'".$idbl_delivery."'";
	
	}
	
	$query = "SELECT DISTINCT(idorder_supplier)
				FROM bl_delivery
				WHERE idbl_delivery IN ($in)";
	
	$rs = DBUtil::query($query);
	
	if( $rs === false )
		die("Impossible de récupérer les commandes fournisseurs");
	
	$totalHTToInvoice = 0;
	$totalTTCToInvoice = 0;
	$chargesET = 0.0;
	
	for($i=0;$i<$rs->RecordCount();$i++){
		
		$idorder_supplier = $rs->fields("idorder_supplier");
		
		$SupplierOrder = new SupplierOrder( $idorder_supplier );
		
		$totalHTToInvoice += round( $SupplierOrder->getTotalET(), 2 );
		$totalTTCToInvoice += round($SupplierOrder->getTotalATI(), 2 );
		$chargesET += round( $SupplierOrder->getChargesET(), 2 );
		
		$j = 0;
		while( $j < $SupplierOrder->getItemCount() ){
		
			echo"<totalrow".$j.$idorder_supplier."><![CDATA[".round($SupplierOrder->getItemAt($j)->get("quantity")*$SupplierOrder->getItemAt($j)->get("discount_price"), 2)."]]></totalrow".$j.$idorder_supplier.">";
		
			$j++;
		}
		
		echo"<totalosht$idorder_supplier><![CDATA[".round($SupplierOrder->getTotalET(),2)."]]></totalosht$idorder_supplier>";
		echo"<totalosttc$idorder_supplier><![CDATA[".round($SupplierOrder->getTotalATI(),2)."]]></totalosttc$idorder_supplier>";
	
		$rs->MoveNext();
	}
	
	echo"<totalht><![CDATA[$totalHTToInvoice]]></totalht>";
	echo"<totalttc><![CDATA[$totalTTCToInvoice]]></totalttc>";
	echo"<chargeset><![CDATA[" . Util::numberFormat( $chargesET, 2 ) . "]]></chargeset>";
		
}

//------------------------------------------------------------------------------------------------

function registerInvoice(){
	
	global $GLOBAL_START_PATH;
	
	$bl = $_GET["bl"];
	// $bl = preg_replace('`([^0-9,]+)`iS','',$bl);
	$bls = explode(",",$bl);

	//Test des champs postés
	$fields = array ( "billing_supplier" , "Datef" , "deliv_payment" , "total_amount_ttc", "total_ht", "total_charge_ht"  );
	
	for ($i=0;$i<count($fields);$i++){
		if( empty( $_POST[ $fields[$i] ] ) ){
			if($fields[$i]!='total_charge_ht')
				return "Tous les champs doivent être remplis";	
		}
	}

	//Vérification du numéro de facture fournisseur pour voir si il n'existe pas
	$billing_supplier = $_POST["billing_supplier"];
	$idsupplier = $_POST[ "idsupplier" ];
	
	$query = "SELECT billing_supplier FROM billing_supplier WHERE billing_supplier='$billing_supplier' AND idsupplier=$idsupplier";
	$rs = DBUtil::query($query);
	
	if($rs === false)
		trigger_error( "Impossible de vérifier le numéro de facture fournisseur", E_USER_ERROR ) && exit();
		
	if($rs->RecordCount()>0)
		return "Ce numéro de facture fournisseur existe déjà";

	
	//Tous les champs sont remplis, test des champs numériques
	if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "total_amount_ttc" ] ) ) 
		return "Le champ montant TTC doit être un nombre";
	
	if( !preg_match( "/^[0-9\., ]+\$/", $_POST[ "total_charge_ht" ] ) ) 
		return "Le champ frais de port HT doit être un nombre";
	
	if( !empty( $_POST[ "rebate_rate" ] ) && !preg_match( "/^[0-9\., ]+\$/", $_POST[ "rebate_rate" ] ) )
			return "Le champ taux d'escompte doit être un nombre";
	
	if( !empty( $_POST[ "rebate_amount" ] ) && !preg_match( "/^[0-9\., ]+\$/", $_POST[ "rebate_amount" ] ) )
			return "Le champ montant d'escompte doit être un nombre";

	//Toutes les données sont ok, on peut traiter l'enregistrement de la facture
	
	$billing_supplier = $_POST[ "billing_supplier" ];
	$Date = $_POST[ "Datef" ];
	$total_amount_ttc = Util::text2num( $_POST[ "total_amount_ttc" ] );
	$total_ht = Util::text2num( $_POST[ "total_ht" ] );
	$total_vat = Util::text2num( $_POST[ "total_vatt" ] );
	$total_charge_ht = Util::text2num( $_POST[ "total_charge_ht" ] );
	$idpayment = $_POST[ "idpayment" ];
	$idpayment_delay = $_POST[ "idpayment_delay"];
	$creation_date = date("Y-m-d");
	$rebate_rate 	= Util::text2num( $_POST[ "rebate_rate" ] );
	
	//On créé une nouvelle facture fournisseur
	$supplierinvoice = SupplierInvoice::create( $billing_supplier , $idsupplier, $bls, $total_ht, $total_vat );
	
	//On calcule la date d'échéance
	$deliv_payment = $_POST[ "deliv_payment" ];
	
	//On affecte les valeurs
	$supplierinvoice->set( "Date" , Util::dateFormatUs($Date) );
	$supplierinvoice->set( "deliv_payment" , Util::dateFormatUs($deliv_payment) );
	$supplierinvoice->set( "idpayment_delay" , $idpayment_delay );
	$supplierinvoice->set( "idpayment" , $idpayment );
	$supplierinvoice->set( "total_amount" , $total_amount_ttc );
	$supplierinvoice->set( "total_amount_ht" , $total_ht );
		$supplierinvoice->set( "total_vat" , $total_vat );
	$supplierinvoice->set( "total_charge_ht" , $total_charge_ht );
	$supplierinvoice->set( "creation_date" , $creation_date );
	
	
	// return "0";
	/* acompte */
	
	$myOrders = array();
	for( $yu = 0 ; $yu < count($bls) ; $yu++ ){
		$idbl = $bls[$yu];
		$myOrder = DBUtil::query("SELECT idorder_supplier FROM bl_delivery WHERE idbl_delivery = '$idbl' LIMIT 1")->fields('idorder_supplier');
		
		if(!in_array($myOrder, $myOrders))
			array_push($myOrders, $myOrder);
	}
	
	//PJs
	if( isset( $_FILES[ "billing_supplier_doc" ] ) && is_uploaded_file( $_FILES[ "billing_supplier_doc" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "billing_supplier_doc";
	
		$target_dir = "$GLOBAL_START_PATH/data/billing_supplier/$YearFile/$idsupplier/";
		$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.doc')&&($extension!='.jpg')&&($extension!='.xls')&&($extension!='.pdf')&&($extension!='.DOC')&&($extension!='.JPG')&&($extension!='.XLS')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./supplier_order.php?IdOrder=".$IdOrder."\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$prefixe."_".$DateFile."_".$name_file;
		$newdoc = $YearFile."/".$idsupplier."/".$prefixe."_".$DateFile."_".$name_file;
	
		//billing_supplier existe
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/billing_supplier/'.$YearFile ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/billing_supplier/'.$YearFile ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $GLOBAL_START_PATH.'/data/billing_supplier/'.$YearFile, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		if( !file_exists( $GLOBAL_START_PATH.'/data/billing_supplier/'.$YearFile.'/'.$idsupplier ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/billing_supplier/'.$YearFile.'/'.$idsupplier ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $GLOBAL_START_PATH.'/data/billing_supplier/'.$YearFile.'/'.$idsupplier , 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
    	
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
		$supplierinvoice->set( $fieldname , $newdoc );		
	}
				
	$username = User::getInstance()->get( "login" );
	$iduser = User::getInstance()->getId();	
	
	if( count($myOrders) > 0 ){
		// de mes commandes jvai traiter les acomptes
		for( $a = 0 ; $a < count($myOrders) ; $a++){
			$idorder = $myOrders[$a];
			$rso = DBUtil::query("SELECT down_payment_idregulation, down_payment_value, down_payment_type, down_payment_idpayment FROM order_supplier WHERE idorder_supplier = '$idorder' LIMIT 1");
		
			$value 	= $rso->fields('down_payment_value');
			$type 	= $rso->fields('down_payment_type');
			$idreg	= $rso->fields('down_payment_idregulation');
			
			if( $idreg > 0 ){
				// on transfert le règlement
				$amount = DBUtil::query("SELECT amount FROM regulations_supplier WHERE idregulations_supplier = $idreg LIMIT 1")->fields('amount');
									
				DBUtil::query("INSERT INTO billing_regulations_supplier (billing_supplier, idregulations_supplier, idsupplier, amount, lastupdate, username )VALUES( '$billing_supplier', $idreg, $idsupplier, $amount, NOW(),'$username' )");
							
			}else if( $value > 0 ){
				// sinon on créé tout et on tr dans la commande le numéro
				
				
				// ------- Mise à jour de l'id gestion des Index  #1161
				//$maxNumRegul = DBUtil::query("SELECT (MAX(idregulations_supplier) + 1) as newId FROM regulations_supplier")->fields('newId');
				
				include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php" );
				$table = 'regulations_buyer';
				$maxNumRegul = TradeFactory::Indexations($table);
				
				$amount			= $rso->fields('down_payment_type') == 'amount'?$rso->fields('down_payment_value'):round($rso->fields('total_amount') * $rso->fields('down_payment_value') / 100 , 2);
				$comment  		= $rso->fields('down_payment_comment');
				$idpayment  	= $rso->fields('down_payment_idpayment');
				$payment_date  	= $rso->fields('down_payment_date');
				
				$insertReg=				
				"INSERT INTO regulations_supplier (
					idregulations_supplier,
					idsupplier,
					iduser,
					amount,
					comment,
					idpayment,
					payment_date,
					deliv_payment,
					DateHeure,
					from_down_payment,
					accept,
					lastupdate,
					username
				) VALUES (
					'$maxNumRegul',
					$idsupplier,
					$iduser,
					'$amount',
					'$comment',
					$idpayment,
					'$payment_date',
					'$payment_date',
					NOW(),
					1,
					1,
					NOW(),
					'$username'
				)";
				
				DBUtil::query($insertReg);
				
				DBUtil::query("INSERT INTO billing_regulations_supplier (billing_supplier, idregulations_supplier, idsupplier, amount, lastupekorys.fr, Blocage factures frs, username )VALUES( '$billing_supplier', $idreg, $idsupplier, $amount, NOW(),'$username' )");
				
			}
			
		}
	}
	
	if(isset($_GET["proforma"]))
		$supplierinvoice->set( "proforma" , 1 );	
	
	//On sauvegarde
	$supplierinvoice->save();
	if(!isset($_GET["proforma"])){
		//on dit a la commande fournisseur que la facture est réceptionnée. bisou
		$ordersSupplier = &SupplierInvoice::getSupplierInvoiceOS( $billing_supplier,$idsupplier );
		$countOS = count($ordersSupplier);
		if($countOS){
			foreach ($ordersSupplier as $key => $value) {
				$ordersSupplier[$key] = "'".$value."'";
			}
			$osToInit = implode( "," , $ordersSupplier );
		
			DBUtil::query("UPDATE order_supplier SET status='invoiced' WHERE idorder_supplier IN($osToInit)");
		}
	}
			
	$supplierinvoice->set( "rebate_type", "rate" );
	$supplierinvoice->set( "rebate_value", $rebate_rate );
			
	
	//On sauvegarde
	$supplierinvoice->save();
		
	return "0";  
	
}

?>