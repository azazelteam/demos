<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche des litiges clients
 */

//------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------
/* récupération commentaires ajax */

if( isset( $_GET[ "comment" ] ) && isset( $_GET[ "idlitigation" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$query = "
	SELECT rts.comment
	FROM litigations l, 
		billing_buyer_row bbr,
		bl_delivery bl,
		bl_delivery_row blr, 
		order_supplier_row osr,
		return_to_sender rts
	WHERE l.idlitigation = '" . intval( $_GET[ "idlitigation" ] ) . "'
	AND bbr.idbilling_buyer = l.idbilling_buyer
	AND bbr.idrow = l.idrow
	AND bl.idbilling_buyer = bbr.idbilling_buyer
	AND bl.idbl_delivery = blr.idbl_delivery
	AND blr.idorder_row = bbr.idorder_row
	AND osr.idorder_supplier = bl.idorder_supplier
	AND osr.idorder_row = blr.idorder_row
	AND rts.idorder_supplier = osr.idorder_supplier
	AND rts.idrow = osr.idrow
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	echo $rs->RecordCount() ?  htmlentities( $rs->fields( "comment" ) ) : "0";
	
	exit();
		
}

//------------------------------------------------------------------------------------------------

$Title = "Rechercher un litige";

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
include_once( "$GLOBAL_START_PATH/objects/Litigation.php" );
include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );

include_once( $GLOBAL_START_PATH . "/catalog/admin_func.inc.php" );
include_once( $GLOBAL_START_PATH . "/catalog/drawlift.php" );

//------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="centerMax">
	<div id="tools" class="rightTools">
		<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
		Accès rapide</h2>
		<div class="spacer"></div>
		
		<div class="blocSearch">
			&nbsp;
		</div>
		
		<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
		Quelques chiffres...</h2>
		<div class="spacer"></div>
		
		<div class="blocSearch">
			&nbsp;
		</div>
	</div>	
	<div class="contentDyn">
		<a name="top"></a>
		<!-- Titre -->
		<h1 class="titleSearch">
		<span class="textTitle">Recherche de litiges</span>
		<div class="spacer"></div>
		</h1>	
		
		<div class="blocSearch"><div style="margin:5px;">
			<?php displayForm(); ?>
			<div class="spacer"></div>   
		</div></div>
	</div>
	<div class="spacer"></div>
	
	<div class="contentResult">	
		<div id="SearchResults"></div>
	</div>
	<div class="spacer"></div>   
	
</div>       		      	
<div class="spacer"></div>      


<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$formFields = array(
	
		$fieldName = "minus_date_select" => $defaultValue = "-365",
		"interval_select" 			=> 3,
		"idbuyer" 					=> "",
		"lastname" 					=> "",
		"company" 					=> "",
		"faxnumber" 				=> "",
		"zipcode" 					=> "",
		"city" 						=> "",
		"idsource" 					=> "",
		"iduser" 					=> "",
		"idbilling_buyer" 			=> "",
		"idorder" 					=> "",
		"idlitigation_motive" 		=> "",
		/*"goods_delivery_location" 	=> "",*/
		"page" 						=> 1,
		"orderBy" 					=> "l.creation_date DESC"
		
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;

	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback 
  
		    };
    
    		$('#SearchForm').ajaxForm( options );
    		$('#SearchForm').ajaxSubmit( options );
    		
		});
		
		/* ----------------------------------------------------------------------------------- */
        
        function preSubmitCallback( formData, jqForm, options ){
        	
        	$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
            	fadeOut: 700
            		
			}); 
        	
        	$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
        	
        }
        
        /* ----------------------------------------------------------------------------------- */
        
        function postSubmitCallback( responseText, statusText ){ 
    
    		$.unblockUI();
    		
    		if( statusText != 'success' || responseText == '0' ){
    			
    			alert( "Impossible d'effectuer la recherche" );
    			return;
    			
    		}

        	document.getElementById( 'SearchResults' ).innerHTML = responseText;

        	createTabs();
        	
        }

		/* --------------------------------------------------------------------------------------- */
		
		function orderBy( str ){

			document.getElementById( 'SearchForm' ).elements[ 'page' ].value = '1';
			document.getElementById( 'SearchForm' ).elements[ 'orderBy' ].value = str;
			
			$('#SearchForm').ajaxSubmit( { 
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback 
    		} ); 
			
		}
		
		/* --------------------------------------------------------------------------------------- */
		
		function goToPage( pageNumber ){
		
			document.getElementById( 'SearchForm' ).elements[ 'page' ].value = pageNumber;
			document.getElementById( 'SearchForm' ).submit();
			
		}

		/* ----------------------------------------------------------------------------------- */
		
		function showSupplierInfos( idsupplier ){
					
			if( idsupplier == '0' )
				return;
	
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
				async: false,
			 	success: function(msg){
			 		
					$.blockUI({
						
						message: msg,
						fadeIn: 700, 
	            		fadeOut: 700,
						css: {
							width: '700px',
							top: '0px',
							left: '50%',
							'margin-left': '-350px',
							'margin-top': '50px',
							padding: '5px', 
							cursor: 'help',
							'-webkit-border-radius': '10px', 
			                '-moz-border-radius': '10px',
			                'background-color': '#FFFFFF',
			                'font-size': '11px',
			                'font-family': 'Arial, Helvetica, sans-serif',
			                'color': '#44474E'
						 }
						 
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
	
			});
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function selectBetween(cal){
			
			document.getElementById( "interval_select_between" ).checked=true;
			cal.hide();
			return true;
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function viewComment( idlitigation ){

			$.ajax({
		 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/litigation_search.php?comment&idlitigation=" + idlitigation,
				async: true,
			 	success: function( msg ){
			 		
			 		if( msg == '0' ){
			 			
			 			alert( 'Impossible de récupérer les commentaires du magazinier' );
			 			return;
			 			
			 		}
			 		
			 		document.getElementById( 'CommentTextArea' ).value = msg;
			 		
			 		$.blockUI({
	
						message: $( '#CommentDiv' ),
						css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
						fadeIn: 0, 
		            	fadeOut: 700
		            		
					}); 
		        	
		        	$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
	
			});
			
			

		}

		/* ----------------------------------------------------------------------------------- */
		
		function createTabs(){
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' })
		}
		
		/* ----------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div id="CommentDiv" style="display:none;">
		<p class="subTitle" style="text-align:left; margin:0px;">Commentaires du magazinier</p>
		<textarea id="CommentTextArea" style="width:100%; height:80px; border:1px solid #E7E7E7;" readonly="readonly"></textarea>
		<p style="text-align:center; margin-bottom:0px;">
			<input type="button" name="" class="blueButton" value="Fermer" onclick="$.unblockUI();" />
		</p>
	</div>
	
	<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
		<input type="hidden" name="search" value="1" />
		<input type="hidden" name="page" value="<?php echo $postData[ "page" ] ?>" />
		<input type="hidden" name="orderBy" value="<?php echo $postData[ "orderBy" ] ?>" />
		
		
		<!-- Choix par date -->
		<div class="blocMultiple blocMLeft">
			<h2>&#0155; Par date :</h2>
			
			<label><span><input type="radio" name="interval_select" id="interval_select_relance" class="VCenteredWithText" value="1"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == 1 || !isset( $_POST[ "interval_select" ] ) ) echo " checked=\"checked\""; ?> />
			&nbsp;Depuis</span></label>

        	<select name="minus_date_select" class="fullWidth">
				<option value="0" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == 0 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_today") ?></option>
				<option value="-1" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1 ) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_yesterday") ?></option>
				<option value="-2" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -2) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2days") ?></option>
				<option value="-7" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -7) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1week") ?></option>
				<option value="-14" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -14) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2weeks") ?></option>
				<option value="-30" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -30) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1month") ?></option>
				<option value="-60" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -60) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2months") ?></option>
				<option value="-90" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -90) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_3months") ?></option>
				<option value="-180" <?php if( !isset( $_POST[ "minus_date_select" ] ) || $_POST[ "minus_date_select" ] == -180) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_6months") ?></option>
				<option value="-365" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -365) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_1year") ?></option>
				<option value="-730" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -730) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_2year") ?></option>
				<option value="-1825" <?php if( isset( $_POST[ "minus_date_select" ] ) && $_POST[ "minus_date_select" ] == -1825) echo "selected=\"selected\""; ?> > <?php echo Dictionnary::translate("order_5year") ?></option>
			</select>
			<div class="spacer"></div>
			
			<label><span><input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> /> 
            &nbsp;Pour le mois de</span></label>
            <?php FStatisticsGenerateDateHTMLSelect( "selectMonth()" ); ?>
            <div class="spacer"></div>
            
            <label><span><input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> />
            &nbsp;Entre le</span></label>
            <?php $date = isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : ""; ?>
            <input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php echo $date ?>" />
            <?php echo DHTMLCalendar::calendar( "sp_bt_start", true, "selectBetween" ) ?>
            
            <label class="smallLabel"><span>et le</span></label>
            <?php $date = isset( $_POST[ "sp_bt_end" ] ) ? $_POST[ "sp_bt_end" ] : ""; ?>
            <input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php echo $date ?>" />
            <?php echo DHTMLCalendar::calendar( "sp_bt_end", true, "selectBetween" ) ?></td>
			<div class="spacer"></div>

        </div> 
        
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Par informations sur le litige :</h2>
        	
        	<label><span><strong>Facture n°</strong></span></label>
			<input type="text" name="idbilling_buyer" class="textidInput" value="<?php echo $postData[ "idbilling_buyer" ] ?>" />
			<label class="smallLabel"><span><strong>Cde n°</strong></span></label>
			<input type="text" name="idorder" class="textidInput" value="<?php echo $postData[ "idorder" ] ?>" />
			<div class="spacer"></div> 
			
			<label><span><strong>Motif de litige</strong></span></label>
			<?php XHTMLFactory::createSelectElement( "litigation_motives", "idlitigation_motive", "motive", "motive", $postData[ "idlitigation_motive" ], "", "Tous", "name=\"idlitigation_motive\"" ); ?>
            <div class="spacer"></div>
        </div>
        <div class="spacer"></div>  
        	
        <div class="blocMultiple blocMLeft">
        	<h2>&#0155; Par client :</h2>
        	
        	<label><span><strong>Raison sociale</strong></span></label>
        	<input type="text" name="company" id="company" class="textInput" value="<?php echo htmlentities( $postData[ "company" ] ) ?>" />
            <?php AutoCompletor::completeFromDB( "company", "buyer", "company", 15 ); ?>
        	<div class="spacer"></div>
        	
        	<label><span><strong>Nom du contact</strong></span></label>
			<input type="text" name="lastname" id="lastname" class="textInput" value="<?php echo htmlentities( $postData[ "lastname" ] ) ?>" />
            <?php AutoCompletor::completeFromDB( "lastname", "contact", "lastname", 15 ); ?>
			<div class="spacer"></div>
			
			<label><span><strong>N° client</strong></span></label>
			<input type="text" name="idbuyer" class="textInput" value="<?php echo $postData[ "idbuyer" ] ?>" />
			<div class="spacer"></div>
			
			<label><span>Code postal</span></label>
        	<input type="text" name="zipcode" class="numberInput" value="<?php echo htmlentities( $postData[ "zipcode" ] ) ?>" />
        	
            <label class="smallLabel"><span>Ville</span></label>
            <input type="text" name="city" id="city" class="mediumInput" value="<?php echo htmlentities( $postData[ "city" ] ) ?>" />
            <?php AutoCompletor::completeFromDB( "city", "buyer", "city", 15 ); ?>
            <div class="spacer"></div>  
            
            <!-- <label><span>Fax</span></label>
            <input type="text" name="faxnumber" class="textInput" value="<?php echo htmlentities( $postData[ "faxnumber" ] ) ?>" />
            <div class="spacer"></div>  -->
		</div>
		
		<!-- ??? -->
        <div class="blocMultiple blocMRight">
        	<h2>&#0155; Autres critères :</h2>
        	
        	<label><span><strong>Commercial</strong></span></label>
			<?php				
				if( User::getInstance()->get( "admin" ) ){
					include_once( dirname( __FILE__ ) . "/../script/user_list.php" );
					userList( true, $postData[ "iduser" ] );				
				}
				else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";											
			?>
			<div class="spacer"></div>  
		
			<label><span><strong>Provenance</strong></span></label>
			<?php XHTMLFactory::createSelectElement( "source", "idsource", "source" . User::getInstance()->getLang(), "source" . User::getInstance()->getLang(), $postData[ "idsource" ], "", "Toutes", "name=\"idsource\"" ); ?>
			<div class="spacer"></div>  

		</div>
		<div class="spacer"></div>  
		
	    <input type="submit" class="inputSearch" value="Rechercher" />

	</form>
	<?php

}
//----------------------------------------------------------------------------------------------------

function search(){
	
	global 	$GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
	
	$resultPerPage 		= 50000;
	$maxSearchResults 	= 1000;

	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	$select = "
	DISTINCT( l.idlitigation ),
	l.*,
	lb.idbilling_buyer,
	SUBSTRING( l.creation_date, 1, 10 ) AS creation_date,
	lm.motive,
	u.initial,
	c.idbuyer,
	c.idcontact,
	c.firstname,
	c.lastname,
	c.title,
	b.company,
	b.zipcode,
	b.city,
	b.idsource,
	CONCAT( t.title_1, ' ', c.firstname, ' ', c.lastname ) AS contact,
	s.name$lang AS stateName,
	so.source$lang,
	bb.idorder";

	$selectOrder = "
	DISTINCT( l.idlitigation ),
	l.*,
	lo.idorder,
	SUBSTRING( l.creation_date, 1, 10 ) AS creation_date,
	lm.motive,
	u.initial,
	c.idbuyer,
	c.idcontact,
	c.firstname,
	c.lastname,
	c.title,
	b.company,
	b.zipcode,
	b.city,
	b.idsource,
	CONCAT( t.title_1, ' ', c.firstname, ' ', c.lastname ) AS contact,
	s.name$lang AS stateName,
	so.source$lang";	
	
	$tables = "
	litigations l 
	LEFT JOIN litigation_motives lm ON l.idlitigation_motive = lm.idlitigation_motive, 
	billing_buyer bb, litigations_billing_buyer lb , `order` o , state s , source so , user u , buyer b , contact c
	LEFT JOIN title t ON t.idtitle = c.title";
	
	$tablesOrder = "
	litigations l 
	LEFT JOIN litigation_motives lm ON l.idlitigation_motive = lm.idlitigation_motive, 
	litigations_order lo , `order` o , state s , source so , user u , buyer b , contact c
	LEFT JOIN title t ON t.idtitle = c.title";	
	
	$where = "
	l.iduser = u.iduser
	AND lb.idbilling_buyer = bb.idbilling_buyer
	AND lb.idlitigation = l.idlitigation
	AND bb.idorder = o.idorder
	AND c.idbuyer = bb.idbuyer
	AND c.idcontact = bb.idcontact
	AND b.idbuyer = bb.idbuyer
	AND s.idstate = b.idstate
	AND so.idsource = b.idsource
	AND t.idtitle = c.title";

	$whereOrder = "
	l.iduser = u.iduser
	AND lo.idlitigation = l.idlitigation
	AND lo.idorder = o.idorder
	AND c.idbuyer = o.idbuyer
	AND c.idcontact = o.idcontact
	AND b.idbuyer = o.idbuyer
	AND s.idstate = b.idstate
	AND so.idsource = b.idsource
	AND t.idtitle = c.title";
	
	//post data
	
	//integers
	
	$integers = array( 

		"b.idbuyer", 
		"b.idsource", 
		"l.iduser",
		"lb.idbilling_buyer",
		"l.idlitigation_motive",
		"bb.idorder"
		
	);

	$integersOrder = array( 

		"b.idbuyer", 
		"b.idsource", 
		"l.iduser",
		"l.idlitigation_motive",
		"lo.idorder"
		
	);	
	
	foreach( $integers as $integer ){
		
		list( $table, $column ) = explode( ".", $integer );
		
		if( isset( $_POST[ $column ] ) && intval( $_POST[ $column ] ) )
			$where .= " AND `$table`.`$column` = " . intval( $_POST[ $column ] );
		
	}
	
	foreach( $integersOrder as $integer ){
		
		list( $table, $column ) = explode( ".", $integer );
		
		if( isset( $_POST[ $column ] ) && intval( $_POST[ $column ] ) )
			$whereOrder .= " AND `$table`.`$column` = " . intval( $_POST[ $column ] );
		
	}
	
	
	//strings
	
	$strings = array( "c.lastname", "b.company", "b.city", "b.zipcode" );
	
	foreach( $strings as $string ){
		
		list( $table, $column ) = explode( ".", $string );
		
		if( isset( $_POST[ $column ] ) && strlen( $_POST[ $column ] ) ){
			$where .= " AND `$table`.`$column` LIKE '%" . Util::html_escape( $_POST[ $column ] ) . "%'";
			$whereOrder .= " AND `$table`.`$column` LIKE '%" . Util::html_escape( $_POST[ $column ] ) . "%'";
		}
		
	}
	
	
	//numbers ( phone, fax, ... )
	
	$numbers = array( "c.faxnumber" );
	
	foreach( $numbers as $number ){
		
		list( $table, $column ) = explode( ".", $number );
		
		if( isset( $_POST[ $column ] ) && !empty( $_POST[ $column ] ) ){
			
			$num = ereg_replace( "[^0-9]*", "", $_POST[ $column ] );
			$regexp = "";
			$i = 0;
			while( $i < strlen( $num ) ){
				
				if( $i )
					$regexp .= "[^0-9]*";
					
				$regexp .= substr( $num, $i, 1 ) . "{1}";
				
				$i++;
				
			}
			
			$where .= " AND `$table`.`$column` REGEXP '^.*$regexp.*\$'";
			$whereOrder .= " AND `$table`.`$column` REGEXP '^.*$regexp.*\$'";
		}
		
	}

	//creation_date
	
 	$now = getdate();

	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;

	switch ($interval_select){
		    
		case 1:

			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			$where  .= " AND l.creation_date >='$Min_Time' AND l.creation_date <='$Now_Time' ";
			
			break;
		
		case 2:
			
			$yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now[ "year" ]--;

		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$where .= " AND l.creation_date >='$Min_Time' AND l.creation_date <'$Max_Time' ";
	     	break;
		     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "sp_bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "sp_bt_end" ] );
		 	
			$where .= " AND l.creation_date >= '$Min_Time' AND l.creation_date <= '$Max_Time' ";
			
	     	break;
		
	}

	$groupBy = "l.idlitigation";
	
	//tri

	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderBy" ] ) );
	
	//nombre de pages

	$query = "
	SELECT COUNT( DISTINCT( l.idlitigation ) ) AS resultCount
	FROM $tables
	WHERE $where";
	
	$rs =& DBUtil::query( $query );
		
	$resultCount = $rs->fields( "resultCount" );
	
	$query = "
	SELECT COUNT( DISTINCT( l.idlitigation ) ) AS resultCount
	FROM $tablesOrder
	WHERE $whereOrder";
	
	$rs =& DBUtil::query( $query );
	
	$resultCountOrders = $rs->fields( "resultCount" );
	
	?>
	
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Résultats de la recherche</span>
	<span class="selectDate">
		<a href="#bas" class="goUpOrDown">
			Aller en bas de page
			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
		</a>
	</span>
	<div class="spacer"></div>
	</h1>

	<div id="container">
		<ul class="menu">
            <li class="item"><a href="#billing_buyer_tab"><span>Litiges sur factures ( <?php echo $resultCount; ?> )</span></a></li>
            <li class="item"><a href="#order_tab"><span>Litiges sur commandes ( <?php echo $resultCountOrders; ?> )</span></a></li>
        </ul>
		<div class="spacer"></div>
					
	    <div class="blocResult mainContent withTab">
	    	<div id="billing_buyer_tab" style="margin:5px;">
		        <?php if( !$resultCount ){
					echo "<p>La recherche n'a donné résultat</p>";
					return; 
				}
		
				if( $resultCount > $maxSearchResults ){ ?>
					
					<p>Plus de  <?php echo $maxSearchResults ?> litiges ont &eacute;t&eacute; trouv&eacute;s <br />
					Merci de bien vouloir affiner votre recherche</p>
		
				<?php	
					
					return;
						
				}
		
				$pageCount = ceil( $resultCount / $resultPerPage );
				
				//recherche
				
				$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
				
				$start = ( $page - 1 ) * $resultPerPage;
				
				$query = "
				SELECT $select
				FROM $tables
				WHERE $where 
				GROUP BY $groupBy
				ORDER BY $orderBy 
				LIMIT $start, $resultPerPage";
			
				$rs =& DBUtil::query( $query );
		
				?>
				<!-- tableau résultats recherche -->
		    	<table class="dataTable resultTable">
		        	<thead>
		                <tr>
		                	<th class="lefterCol">Com</th>
		                	<th>Litige N°</th>
		                	<th>Avancement</th>
		                	<th>Cde n°</th>
		                	<th>Facture n°</th>
		                	<th>Client n°</th>
		                	<th>Nom / Raison sociale</th>
		                	<th>Date de création</th>
		                	<th>Motif</th>
		                	<?php /*<th class="righterCol">Retour marchandise</th>*/?>
						</tr>
						 <!-- petites flèches de tri -->
		                <tr>
		                	<th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('u.initial ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('u.initial DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.idlitigation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.idlitigation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.editable ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.editable DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                     <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('o.idorder ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('o.idorder DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>                 
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.idbilling_buyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.idbilling_buyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('c.idbuyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('c.idbuyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('b.company ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('b.company DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.creation_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.creation_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('lm.motive ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('lm.motive DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder"></th>
						</tr>
					</thead>
					<tbody>
					<?php
					
						while( !$rs->EOF() ){
							
							?>
							<tr>
								<td class="lefterCol"><?php echo $rs->fields( "initial" ) ?></td>
		                    	<td>
		                    		<a class="blueLink" href="/accounting/litigation.php?idlitigation=<?php echo $rs->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" target="_blank">
		                    			<?php echo $rs->fields( "idlitigation" ); ?>
		                    		</a>
		                    	</td>
		                    	<td><?php echo $rs->fields( "editable" ) ? "En cours" : "Résolu"; ?></td>
		                    	<td>
		                    		<a href="/sales_force/com_admin_order.php?IdOrder=<?php echo $rs->fields( "idorder" ) ?>" onclick="window.open( this.href ); return false;">
		                    			<?php echo $rs->fields( "idorder" ) ?>
		                    		</a>
		                    	</td>
		                    	<td>
		                    		<a href="/accounting/com_admin_invoice.php?IdInvoice=<?php echo $rs->fields( "idbilling_buyer" ) ?>" onclick="window.open( this.href ); return false;">
		                    			<?php echo $rs->fields( "idbilling_buyer" ) ?>
		                    		</a>
		                    	</td>
		                    	<td>
		                    		<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ) ?>" onclick="window.open( this.href ); return false;">
		                        		<?php echo $rs->fields( "idbuyer" ) ?>
		                        	</a>
		                    	</td>
		                    	<td style="text-align:left;">
		                    		<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ) ?>" onclick="window.open( this.href ); return false;">
		                        	<?php 
		                        	
		                        		if( strlen( $rs->fields( "company" ) ) )
											echo htmlentities( $rs->fields( "company" ) );
		                        		else echo htmlentities( $rs->fields( "contact" ) );
		                        		
		                        	?>
		                        	</a>
		                    	</td>
		                    	<td><?php echo Util::dateFormatEu( $rs->fields( "creation_date" ) ) ?></td>
		                    	<td style="text-align:left;" class="righterCol"><?php echo htmlentities( $rs->fields( "motive" ) ) ?></td>
							</tr>
							<?php
							
							$rs->MoveNext();
							
						}
						
					?>
					</tbody>
				</table>
				<script type="text/javascript">
				/* <![CDATA[ */
					document.getElementById("msgInfos").innerHTML = '<?php echo $rs->RecordCount() ?> litige(s) trouvé(s)' ;
				/* ]]> */
				</script>
				<div class="spacer"></div>
					
	    	</div>
	    	
	    	<!-- Litiges sur commandes -->
	    	<div id="order_tab" style="margin:5px;">
		        <?php if( !$resultCountOrders ){
					echo "<p>La recherche n'a donné résultat</p>";
					return; 
				}
		
				if( $resultCountOrders > $maxSearchResults ){ ?>
					
					<p>Plus de  <?php echo $maxSearchResults ?> litiges ont &eacute;t&eacute; trouv&eacute;s <br /> 
					Merci de bien vouloir affiner votre recherche</p>
		
				<?php	
					
					return;
						
				}
		
				$pageCount = ceil( $resultCountOrders / $resultPerPage );
				
				//recherche
				
				$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
				
				$start = ( $page - 1 ) * $resultPerPage;
				
				$query = "
				SELECT $selectOrder
				FROM $tablesOrder
				WHERE $whereOrder 
				GROUP BY $groupBy
				ORDER BY $orderBy 
				LIMIT $start, $resultPerPage";
			
				$rs =& DBUtil::query( $query );
		
				?>
				<!-- tableau résultats recherche -->
		    	<table class="dataTable resultTable">
		        	<thead>
		                <tr>
		                	<th class="lefterCol">Com</th>
		                	<th>Litige N°</th>
		                	<th>Avancement</th>
		                	<th>Cde n°</th>
		                	<th>Client n°</th>
		                	<th>Nom / Raison sociale</th>
		                	<th>Date de création</th>
		                	<th>Motif</th>
		                	<?php /*<th class="righterCol">Retour marchandise</th>*/?>
						</tr>
						 <!-- petites flèches de tri -->
		                <tr>
		                	<th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('u.initial ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('u.initial DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.idlitigation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.idlitigation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.editable ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.editable DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                     <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('o.idorder ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('o.idorder DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>                 
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('c.idbuyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('c.idbuyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('b.company ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('b.company DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('l.creation_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('l.creation_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder">
		                    	<a href="#" onclick="orderBy('lm.motive ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="tri ascendant" /></a>
		                        <a href="#" onclick="orderBy('lm.motive DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="tri descendant" /></a>
		                    </th>
		                    <th class="noTopBorder"></th>
						</tr>
					</thead>
					<tbody>
					<?php
					
						while( !$rs->EOF() ){
							
							?>
							<tr>
								<td class="lefterCol"><?php echo $rs->fields( "initial" ) ?></td>
		                    	<td>
		                    		<a class="blueLink" href="/accounting/litigation.php?idlitigation=<?php echo $rs->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer" target="_blank">
		                    			<?php echo $rs->fields( "idlitigation" ); ?>
		                    		</a>
		                    	</td>
		                    	<td><?php echo $rs->fields( "editable" ) ? "En cours" : "Résolu"; ?></td>
		                    	<td>
		                    		<a href="/sales_force/com_admin_order.php?IdOrder=<?php echo $rs->fields( "idorder" ) ?>" onclick="window.open( this.href ); return false;">
		                    			<?php echo $rs->fields( "idorder" ) ?>
		                    		</a>
		                    	</td>
		                    	<td>
		                    		<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ) ?>" onclick="window.open( this.href ); return false;">
		                        		<?php echo $rs->fields( "idbuyer" ) ?>
		                        	</a>
		                    	</td>
		                    	<td style="text-align:left;">
		                    		<a href="/sales_force/contact_buyer.php?key=<?php echo $rs->fields( "idbuyer" ) ?>" onclick="window.open( this.href ); return false;">
		                        	<?php 
		                        	
		                        		if( strlen( $rs->fields( "company" ) ) )
											echo htmlentities( $rs->fields( "company" ) );
		                        		else echo htmlentities( $rs->fields( "contact" ) );
		                        		
		                        	?>
		                        	</a>
		                    	</td>
		                    	<td><?php echo Util::dateFormatEu( $rs->fields( "creation_date" ) ) ?></td>
		                    	<td style="text-align:left;" class="righterCol"><?php echo htmlentities( $rs->fields( "motive" ) ) ?></td>
							</tr>
							<?php
							
							$rs->MoveNext();
							
						}
						
					?>
					</tbody>
				</table>
				<script type="text/javascript">
				/* <![CDATA[ */
					document.getElementById("msgInfos").innerHTML = '<?php echo $rs->RecordCount() ?> litige(s) trouvé(s)' ;
				/* ]]> */
				</script>
				<div class="spacer"></div>
					
	    	</div>	    	
	    	
	    </div>
	</div>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

?>