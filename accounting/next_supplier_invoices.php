<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures fournisseurs
 */
 

//------------------------------------------------------------------------------------------------

include_once ("../objects/classes.php");

$Title = Dictionnary::translate("next_supplier_invoices");

include ("$GLOBAL_START_PATH/templates/back_office/head.php");

$hasAdminPrivileges = User::getInstance()->get("admin");
$iduser = User::getInstance()->getId();
$db = & DBUtil::getConnection();

//------------------------------------------------------------------------------------------------

displaySearchResultsByMonth();

include ("$GLOBAL_START_PATH/templates/back_office/foot.php");
//------------------------------------------------------------------------------------------------

function displaySearchResultsByMonth() {

	global  $GLOBAL_START_URL, $resultPerPage;

	$db = & DBUtil::getConnection();
	$lang = User::getInstance()->getLang();

	$hasAdminPrivileges = User::getInstance()->get("admin");
	$iduser = User::getInstance()->getId();
	$period = DBUtil::getParameterAdmin("next_invoices_period");
	$use_help = DBUtil::getParameterAdmin('gest_com_use_help');
	$page = isset ($_REQUEST["page"]) ? intval($_REQUEST["page"]) : 1;
	$start = ($page -1) * $resultPerPage;

	//Année en cours
	$year = date("Y");
	//Mois en cours
	$month = date("m");

	$ht = 0;
	$ttc = 0;
	for ($i = 0; $i < $period; $i++) {

		$tmp = array ();
		$idorders = array ();

		if ($month == 13) {
			$month = 1;
			$year = $year +1;
		}
		if ($month < 10) {
			if ($month {
				0 }
			!= "0")
			$month = '0' .
			$month;
		}

		$SQL_Condition = "order_supplier.status='confirmed' ";
		$SQL_Condition .= "AND (order_supplier.availability_date LIKE '$year-$month-%'";
		$SQL_Condition .= " OR bl_delivery.dispatch_date LIKE '$year-$month-%')";

		//recherche
		$SQL_Query = "
				SELECT order_supplier.idorder_supplier, 
				order_supplier.idorder, 
				order_supplier_status.order_supplier_status, 
				order_supplier_status.order_supplier_status$lang, 
				order_supplier.DateHeure, 
				order_supplier.total_amount_ht,
				order_supplier.total_amount,  
				supplier.name, 
				order_supplier.seen, 
				order_supplier.idbuyer, 
				order_supplier.iddelivery, 
				user.initial,
				order_supplier.idsupplier,
				bl_delivery.availability_date,
				bl_delivery.dispatch_date,
				`order`.status,
				order_supplier.idpayment,
				order_supplier.idpayment_delay
				FROM order_supplier, supplier, order_supplier_status, buyer, user, `order`, bl_delivery
				WHERE $SQL_Condition";
		$SQL_Query .= "
					AND order_supplier.status = order_supplier_status.order_supplier_status
					AND order_supplier.idsupplier=supplier.idsupplier 
					AND order_supplier.idbuyer = buyer.idbuyer
					AND user.iduser = buyer.iduser
					AND `order`.idorder = order_supplier.idorder 
					AND order_supplier.idorder_supplier = bl_delivery.idorder_supplier
				ORDER BY bl_delivery.availability_date ASC";

		//die ($SQL_Query);
		$rs = $db->Execute($SQL_Query);

		if ($rs === false)
			die("Impossible d'effectuer la recherche");
?>

		<div id="globalMainContent">
            	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
                <div class="mainContent fullWidthContent">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
                    	<div class="headTitle">
                            <p class="title">
                            <a name="topPage"></a>
                            	<?php

		if (!$rs->RecordCount()) {
?>
		                    		<span style="font-size: 14px;font-weight: bold; color: #008000;"> Aucune facture trouvée pour le mois de <?=GetMonthName($month);?> <?=$year;?></span>
		                    	<?php

		} else {
			$resultCount = $rs->RecordCount();
?>
		                    		Résultats de la recherche: <?php echo $resultCount . "&nbsp;" . Dictionnary::translate( "gest_com_invoices_found" ) ?> pour le mois de <?=GetMonthName($month);?> <?=$year;?>
		                    		<?php

		}
?>
                            </p>
							<div class="rightContainer"></div>
                        </div>
                        
               		</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
                </div> <!-- mainContent fullWidthContent -->

		<?php

		if (!$rs->RecordCount()) {
?>
			</div> <!-- GlobalMainContent -->
			<?php

			return;
		}
?>
		
		
		<div class="mainContent fullWidthContent">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <div class="content">
                        <div class="subContent">
							<div class="tableHeaderLeft">
								<a href="#bottomPage" class="goUpOrDown">
									Aller en bas de page
									<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
								</a>
							</div>
                        	<!-- tableau résultats recherche -->
                            <div class="resultTableContainer clear">
                            	<table class="dataTable resultTable">
                                	<thead>
		
		         <tr>
		         	  <th><?php echo Dictionnary::translate("com") ; ?></th>
		         	  <th>Commande n°</th>
		          	  <th><?php echo Dictionnary::translate("supplier"); ?></th>
			          <th>Montant HT</th>
			          <th>Montant TTC</th>
			          <th><?php echo Dictionnary::translate("cond_payment") ; ?></th>
		        	  <th><?php echo Dictionnary::translate("date_disposition") ; ?></th>
		         </tr>
            </thead>
	        <?php


		//récupérer les traductions des status

		$strstatus = array ();
		$db = & DBUtil::getConnection();

		$query = "SELECT order_supplier_status, order_supplier_status$lang FROM order_supplier_status";
		$rs2 = $db->Execute($query);

		if ($rs2 === false)
			die(Dictionnary::translate("gest_com_impossible_recover_status_list"));

		while (!$rs2->EOF()) {

			$strstatus[$rs2->fields("order_supplier_status")] = $rs2->fields("order_supplier_status$lang");

			$rs2->MoveNext();

		}

		for ($i = 0; $i < $rs->RecordCount(); $i++) {

			$idorder = $rs->Fields("idorder");
			$bls = SupplierOrder::getBLs($rs->Fields("idorder_supplier"));
			$idorder_supplier = $rs->Fields("idorder_supplier");
			$DateHeure = $rs->Fields("DateHeure");
			$status = $rs->Fields("order_supplier_status$lang");
			$total_amount_ht = $rs->Fields("total_amount_ht");
			$total_amount = $rs->Fields("total_amount");
			$suppliername = $rs->fields("name");
			$idbuyer = $rs->fields("idbuyer");
			$initials = $rs->fields("initial");
			$Iddelivery = $rs->fields("iddelivery");
			$status_order = $rs->fields("status");
			$oss = $rs->Fields("order_supplier_status");
			$ids = $rs->Fields("idsupplier");

			$idpayment = $rs->fields("idpayment");
			$payment = GetPayment($idpayment);
			$idpayment_delay = $rs->fields("idpayment_delay");
			$payment_delay = GetPaymentDelay($idpayment_delay);

			$ht += $total_amount_ht;
			$ttc += $total_amount;

			$av_date = $rs->fields("availability_date");
			if ($av_date == '0000-00-00') {
				$av_date = "";
			} else {
				$av_date = usDate2Eu($av_date);
			}
			$style = "";
			if ($oss == 'standby')
				$style = 'style="color:#0036FF;"';
?>
		        <tr>
		        	<td class="lefterCol"><?php echo $initials ?></td>
		        	<td><?php if($use_help){ GenerateHTMLForToolTipBox( $idorder_supplier, $suppliername ); ?><a href="supplier_order.php?IdOrder=<?php  echo $idorder_supplier; ?>" onclick="window.open(this.href); return false;"><?php  echo $idorder_supplier; ?></a><?php }else{?><div class='tooltipanousehelp'><?php DrawToolTipBox($idorder_supplier,$suppliername);?><a href="supplier_order.php?IdOrder=<?=$idorder_supplier?>" class="tooltipanousehelp" onclick="window.open(this.href); return false;" onmouseover="document.getElementById('pop<?=$idorder_supplier;?>').className='tooltiphovernousehelp'" onMouseOut="document.getElementById('pop<?=$idorder_supplier;?>').className='tooltipnousehelp'"><?=$idorder_supplier;?></a></div><?php } ?></td>
		          	<td><?php if($use_help){GenerateHTMLForToolTipBoxTel($idorder_supplier);?><?php  echo $suppliername; ?><?php }else{?><div class='tooltipanousehelp'><?php DrawToolTipBoxTel($idorder_supplier);?><a href="/product_management/enterprise/supplier.php?idsupplier=<?=$ids?>" class="tooltipanousehelp" style="z-index:10;" onclick="window.open(this.href); return false;" onmouseover="document.getElementById('poptel<?=$idorder_supplier;?>').className='tooltiphovernousehelp'" onMouseOut="document.getElementById('poptel<?=$idorder_supplier;?>').className='tooltipnousehelp'"><?=$suppliername; ?></a></div><?php } ?></td>
					<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat($total_amount_ht);?></td>
					<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat($total_amount);?></td>
					<td><?=$payment?><br /><?=$payment_delay?></td>
					<td class="righterCol"><?=$av_date;?></td>
		        </tr>
		        <?php


			$rs->MoveNext();

		}
?>
		  	<tr>
		        <td colspan="3" style="border: none;">&nbsp;</td>
		        <td class="lefterCol righterCol" style="text-align:right; white-space:nowrap; vertical-align:top;font-weight:bold;"><?php echo Util::priceFormat($ht);?></td>
		        <td class="lefterCol righterCol" style="text-align:right; white-space:nowrap; vertical-align:top;font-weight:bold;"><?php echo Util::priceFormat($ttc);?></td>
		        <td colspan="2" style="border:none;">&nbsp;</td>
		    </tr>
		</tbody>
                                </table>
                            </div>
                            <a name="bottomPage"></a>
                            <a href="#topPage" class="goUpOrDown">
                            	Aller en haut de page
                                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                            </a>
                       	</div>
               		</div>
                    <div class="bottomRight"></div><div class="bottomLeft"></div>
                </div> <!-- mainContent fullWidthContent -->
            </div><!-- globalMainContent -->
		
		<?php

		$month++;
	}
	//paginate( $page, $pageCount );

}

//------------------------------------------------------------------------------------------------

function paginate($page, $pageCount) {

	global $GLOBAL_START_URL;

	if ($pageCount < 2)
		return;

	$pattern = "&nbsp;<a href=\"#\" onclick=\"var form = document.forms.SearchForm; form.elements[ 'page' ] = %page; \">%page</a>&nbsp;";
?>
	<p style="text-align:center; margin:20px;">
	<?php


	if ($page > 1) {

		$prev = $page -1;
?>
		&nbsp;<a href="#" onclick="var form = document.forms.SearchForm; form.elements[ 'page' ] = <?php echo $prev ?>; form.submit();"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/btn_previous.jpg" alt="" style="border-style:none; text-align:text-bottom;" /></a>&nbsp;
		<?php


	}

	$i = 1;
	while ($i < $page) {

		echo str_replace("%page", $i, $pattern);
		$i++;

	}

	echo "<b>$page</b>";

	$i = $page +1;
	while ($i <= $pageCount) {

		echo str_replace("%page", $i, $pattern);
		$i++;

	}

	if ($page < $pageCount) {

		$next = $page +1;
?>
		&nbsp;<a href="#" onclick="var form = document.forms.SearchForm; form.elements[ 'page' ] = <?php echo $next ?>; form.submit();"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/btn_next.jpg" alt="" style="border-style:none; text-align:text-bottom;" /></a>&nbsp;
		<?php


	}
?>
	</p>
	<?php


}

//------------------------------------------------------------------------------------------------

function listCommercials($selection = 0) {

	

	$db = & DBUtil::getConnection();

	$query = "SELECT iduser, lastname, firstname FROM user ORDER BY lastname ASC";

	$rs = $db->Execute($query);

	if ($rs === false)
		die("Impossible de récupérer la liste des commerciaux");
?>
	<select name="iduser">
		<option value="0">-</option>
		<?php


	while (!$rs->EOF()) {

		$commercial = htmlentities(ucfirst(strtolower($rs->fields("lastname"))));
		$commercial .= "&nbsp;";
		$commercial .= htmlentities(ucfirst(strtolower($rs->fields("firstname"))));

		$iduser = $rs->fields("iduser");
		$selected = $selection == $iduser ? " selected=\"selected\"" : "";
?>
			<option value="<?php echo $iduser ?>"<?php echo $selected ?>><?php echo $commercial ?></option>
			<?php


		$rs->MoveNext();

	}
?>
	</select>
	<?php


}

//------------------------------------------------------------------------------------------------

function listSources($selection = 0) {

	

	$db = & DBUtil::getConnection();
	$lang = User::getInstance()->getLang();

	$query = "SELECT idsource, source$lang FROM source ORDER BY source$lang ASC";

	$rs = $db->Execute($query);

	if ($rs === false)
		die("Impossible de récupérer la liste des provenances client");
?>
	<select name="idsource">
		<option value="0">-</option>
		<?php


	while (!$rs->EOF()) {

		$source = htmlentities($rs->fields("source$lang"));
		$idsource = $rs->fields("idsource");
		$selected = $selection == $idsource ? " selected=\"selected\"" : "";
?>
			<option value="<?php echo $idsource ?>"<?php echo $selected ?>><?php echo $source ?></option>
			<?php


		$rs->MoveNext();

	}
?>
	</select>
	<?php


}

//------------------------------------------------------------------------------------------------

function listSupplierOrders($idorder, & $order_supplier_total_amount_ht, $supplierOrderCount) {

	global  $GLOBAL_START_URL;

	$db = & DBUtil::getConnection();

	$query = "
		SELECT 
			bl.dispatch_date, 
			os.idorder_supplier, 
			os.total_amount_ht,
			os.billing_amount,
			s.name
		FROM order_supplier os, supplier s, bl_delivery bl
		WHERE os.idorder = '$idorder'
		AND os.idsupplier = s.idsupplier
		AND bl.idorder_supplier = os.idorder_supplier";

	$idinternal_supplier = DBUtil::getParameterAdmin("internal_supplier");

	if (!empty ($idinternal_supplier))
		$query .= "
				AND os.idsupplier <> '$idinternal_supplier'";

	$query .= "
		ORDER BY idorder_supplier ASC";

	$rs = $db->Execute($query);

	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer les infos des commandes fournisseur");

	$i = 0;

	//On contrôle que le client possède un mail pour afficher la checkbox d'envoi par mail
	$q = "SELECT c.mail FROM contact c,`order` o WHERE o.idorder=$idorder AND o.idbuyer=c.idbuyer AND o.idcontact=c.idcontact";
	$r = $db->Execute($q);

	if ($r === false)
		die("Impossible de récupérer l'email du client");

	$mail = $r->fields("mail");

	while (!$rs->EOF()) {

		$idorder_supplier = $rs->fields("idorder_supplier");
		$supplierOrderHREF = "$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=$idorder_supplier";
		$total_amount_ht = $rs->fields("total_amount_ht");
		$billing_amount = $rs->fields("billing_amount");
		$total_amount_ht_wob = $total_amount_ht - $billing_amount;
		$order_supplier_total_amount_ht += $total_amount_ht_wob;
		$name = $rs->fields("name");

		if ($i) {
?>
			<tr>
			<?php


		}
?>
			<td style="text-align:center;">
				<a href="<?php echo $supplierOrderHREF ?>" onclick="window.open(this.href); return false;"><?php echo $idorder_supplier ?></a>
			</td>
			<td style="text-align:center;"><?php echo htmlentities( $name ) ?></td>
			<td style="text-align:center;"><?php echo usDate2eu( $rs->fields( "dispatch_date" ) ) ?></td>
			<!--<td style="text-align:center;"><?php echo Util::priceFormat( $total_amount_ht_wob ) ?></td>-->
		</tr>
		<?php


		$rs->MoveNext();

		$i++;

	}

}

//------------------------------------------------------------------------------------------------

function getSupplierOrderCount($idorder) {

	

	$db = & DBUtil::getConnection();

	$query = "SELECT COUNT(*) AS supplierCount FROM order_supplier WHERE idorder = '$idorder'";

	$idinternal_supplier = DBUtil::getParameterAdmin("internal_supplier");

	if (!empty ($idinternal_supplier))
		$query .= "
				AND idsupplier <> '$idinternal_supplier'";

	$rs = $db->Execute($query);

	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer le nombre de commandes fournisseur pour la commande '$idorder'");

	return $rs->fields("supplierCount");

}
//--------------------------------------------------------------------------------------------------

function getComInitials($idbuyer) {

	

	$db = & DBUtil::getConnection();

	$query = "SELECT u.firstname, u.lastname FROM user u, buyer b WHERE b.idbuyer = $idbuyer AND b.iduser = u.iduser";

	$rs = $db->Execute($query);
	if ($rs === false)
		die(Dictionnary::translate("gest_com_impossible_recover_in_commercial"));

	$initials = strtoupper(substr($rs->fields("firstname"), 0, 1));
	$initials .= strtoupper(substr($rs->fields("lastname"), 0, 1));

	return $initials;

}

//--------------------------------------------------------------------------------------------------

function isBuyerExport($idorder) {

	

	$db = & DBUtil::getConnection();

	$query = "SELECT idbuyer, idbilling_adress FROM `order` WHERE idorder='$idorder'";

	$rs = $db->Execute($query);
	if ($rs === false)
		die(Dictionnary::translate("Impossible de récupérer l'adresse de facturation"));

	$idbilling = $rs->fields("idbilling_adress");
	$idbuyer = $rs->fields("idbuyer");

	//Pays B@O
	$state_bao = strtolower(DBUtil::getParameterAdmin("ad_state"));

	if ($idbilling > 0) {
		//Adresse de facturation différente
		$query = "SELECT idstate FROM billing_adress WHERE idbilling_adress=$idbilling";

		$rs = $db->Execute($query);
		if ($rs === false)
			die(Dictionnary::translate("Impossible de récupérer le pays de l'adresse de facturation"));

		$idstate = $rs->fields("idstate");

		//Pays du client
		$query = "SELECT name_1 as pays FROM state WHERE idstate=$idstate";

		$rs = $db->Execute($query);
		if ($rs === false)
			die(Dictionnary::translate("Impossible de récupérer le pays de du client"));

		$state = strtolower($rs->fields("pays"));

		if ($state != $state_bao)
			$export = 'Oui';
		else
			$export = 'Non';

	} else {
		//Adresse de facturation du client
		$query = "SELECT idstate FROM buyer WHERE idbuyer=$idbuyer";

		$rs = $db->Execute($query);
		if ($rs === false)
			die(Dictionnary::translate("Impossible de récupérer le pays du client"));

		$idstate = $rs->fields("idstate");

		//Pays du client
		$query = "SELECT name_1 as pays FROM state WHERE idstate=$idstate";

		$rs = $db->Execute($query);
		if ($rs === false)
			die(Dictionnary::translate("Impossible de récupérer le pays de du client"));

		$state = strtolower($rs->fields("pays"));

		if ($state != $state_bao)
			$export = 'Oui';
		else
			$export = 'Non';
	}

	return $export;

}
//------------------------------------------------------------------------------------------------

function getPayment($idpayment) {

	

	if (!empty ($idpayment)) {
		$db = & DBUtil::getConnection();

		$query = "SELECT name_1 FROM payment WHERE idpayment = $idpayment";

		$rs = $db->Execute($query);

		if ($rs === false || !$rs->RecordCount())
			die("Impossible de récupérer le mode de paiement");

		return $rs->fields("name_1");
	} else {
		return "-";
	}
}

//-------------------------------------------------------------------------------------------------
function SendInvoiceMail($IdOrder) {

	global $GLOBAL_START_PATH;

	include_once ("$GLOBAL_START_PATH/objects/mailerobject.php");

	$DEBUG = '';
	$Order = new Order($IdOrder);

	$AdminName = DBUtil::getParameterAdmin("ad_name");
	$AdminEmail = DBUtil::getParameterAdmin("ad_mail");

	$BuyerName = $Order->getCustomer()->getContact()->get( "firstname" ) . ' ' . $Order->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );

	$iduser = User::getInstance()->getId();

	$CommercialName = DBUtil::getParameterAdmin("b_contact_firstname") . ' ' . DBUtil::getParameterAdmin("b_contact_lastname");
	$CommercialEmail = DBUtil::getParameterAdmin("b_contact_email");
	$CommercialPhone = DBUtil::getParameterAdmin("b_contact_tel");

	$mailer = new EVM_Mailer();

	$mailer->set_message_type("html");
	$mailer->set_sender(DBUtil::getParameterAdmin("ad_name") . " - " . $CommercialName, $CommercialEmail);
	$mailer->add_recipient($BuyerName, $BuyerEmail);

	//-----------------------------------------------------------------------------------------

	//charger le modèle de mail

	include_once ("$GLOBAL_START_PATH/objects/MailTemplateEditor.php");

	$editor = new MailTemplateEditor();

	$editor->setLanguage($Order->getCustomer()->get( "id_lang" )); //@todo : imposer la langue du destinataire
	$editor->setTemplate("ORDER_INVOICE");

	$editor->setUseOrderTags($Order->get("idorder"));
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags($Order->get("idbuyer"), $Order->get("idcontact"));
	$editor->setUseCommercialTags($iduser);

	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();

	//-----------------------------------------------------------------------------------------

	$mailer->set_subject($Subject);
	$mailer->set_message($Message);

	//-----------------------------------------------------------------------------------------------

	//ajout du pdf

	$invoices = Order::getInvoices( $Order->get( "idorder" ) );

	foreach( $invoices as $invoice ){
		
		$pdf_name = "Facture_$invoice.pdf";
		
		include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
		$odf = new ODFInvoice( $invoice );
		$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
		
		$mailer->add_pdf($pdf_name, $pdfData);

	}

	$mailer->send();

	if ($mailer->is_errors()) {
		die($mailer->get_errors());
	} else {
		//On enregistre ds la base la date d'envoi
		$db = DBUtil::getConnection();

		$today = date("Y-m-d");

		reset($invoices);

		foreach ($invoices as $invoice) {

			$q = "UPDATE `billin_buyer` SET billing_send_date = NOW() WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1";
			$r = $db->Execute($q);

			if ($r === false)
				die("Impossible de mettre à jour la date d'envoi de la facture");

		}

	}
}

//------------------------------------------------------------------------------------------------------------------------
function HumanReadablePhone($tel) {
	//On regarde la longeueur de la cheine pour parcourir
	$lg = strlen($tel);

	$telwcs = '';
	for ($i = 0; $i < $lg; $i++) {
		//On extrait caractère par caractère
		$tocheck = substr($tel, $i, 1);

		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if ($i == 0) {
			if (($tocheck == '+') OR (is_numeric($tocheck))) {
				$telwcs .= $tocheck;
			}
		} else {
			//On ne garde que les chiffres
			if (is_numeric($tocheck)) {
				$telwcs .= $tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg = strlen($telwcs);

	$telok = '';

	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck = substr($telwcs, 0, 1);

	if ($tocheck == '+') {
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck = substr($telwcs, 0, 3);
		$telok .= $tocheck . " ";
		$cpt = 3;
	} else {
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck = substr($telwcs, 0, 2);
		$telok .= $tocheck . " ";
		$cpt = 2;
	}

	//On traite le reste de la chaine
	for ($i = $cpt; $i < $lg; $i += 2) {
		if ($i == $cpt) {
			$tocheck = substr($telwcs, $i, 2);
		} else {
			$tocheck = substr($telwcs, $i, 2);
		}
		$telok .= $tocheck . " ";
	}

	//On enlève l'espace de fin
	$telok = trim($telok);

	return $telok;
}

//------------------------------------------------------------------------------------------------
function numero_semaine($jour, $mois, $annee) {
	/* 
	 * Norme ISO-8601: 
	 * - La semaine 1 de toute année est celle qui contient le 4 janvier ou que la semaine 1 de toute année est celle qui contient le 1er jeudi de janvier. 
	 * - La majorité des années ont 52 semaines mais les années qui commence un jeudi et les années bissextiles commençant un mercredi en possède 53. 
	 * - Le 1er jour de la semaine est le Lundi 
	 */

	// Définition du Jeudi de la semaine 
	if (date("w", mktime(12, 0, 0, $mois, $jour, $annee)) == 0) // Dimanche 
		$jeudiSemaine = mktime(12, 0, 0, $mois, $jour, $annee) - 3 * 24 * 60 * 60;
	else
		if (date("w", mktime(12, 0, 0, $mois, $jour, $annee)) < 4) // du Lundi au Mercredi 
			$jeudiSemaine = mktime(12, 0, 0, $mois, $jour, $annee) + (4 - date("w", mktime(12, 0, 0, $mois, $jour, $annee))) * 24 * 60 * 60;
		else
			if (date("w", mktime(12, 0, 0, $mois, $jour, $annee)) > 4) // du Vendredi au Samedi 
				$jeudiSemaine = mktime(12, 0, 0, $mois, $jour, $annee) - (date("w", mktime(12, 0, 0, $mois, $jour, $annee)) - 4) * 24 * 60 * 60;
			else // Jeudi 
				$jeudiSemaine = mktime(12, 0, 0, $mois, $jour, $annee);

	// Définition du premier Jeudi de l'année 
	if (date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine))) == 0) // Dimanche 
		{
		$premierJeudiAnnee = mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine)) + 4 * 24 * 60 * 60;
	} else
		if (date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine))) < 4) // du Lundi au Mercredi 
			{
			$premierJeudiAnnee = mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine)) + (4 - date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine)))) * 24 * 60 * 60;
		} else
			if (date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine))) > 4) // du Vendredi au Samedi 
				{
				$premierJeudiAnnee = mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine)) + (7 - (date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine))) - 4)) * 24 * 60 * 60;
			} else // Jeudi 
				{
				$premierJeudiAnnee = mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine));
			}

	// Définition du numéro de semaine: nb de jours entre "premier Jeudi de l'année" et "Jeudi de la semaine"; 
	$numeroSemaine = ((date("z", mktime(12, 0, 0, date("m", $jeudiSemaine), date("d", $jeudiSemaine), date("Y", $jeudiSemaine))) - date("z", mktime(12, 0, 0, date("m", $premierJeudiAnnee), date("d", $premierJeudiAnnee), date("Y", $premierJeudiAnnee)))) / 7) + 1;

	// Cas particulier de la semaine 53 
	if ($numeroSemaine == 53) {
		// Les années qui commence un Jeudi et les années bissextiles commençant un Mercredi en possède 53 
		if (date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine))) == 4 || (date("w", mktime(12, 0, 0, 1, 1, date("Y", $jeudiSemaine))) == 3 && date("z", mktime(12, 0, 0, 12, 31, date("Y", $jeudiSemaine))) == 365)) {
			$numeroSemaine = 53;
		} else {
			$numeroSemaine = 1;
		}
	}

	return sprintf("%02d", $numeroSemaine);
}
//-------------------------------------------------------------------------------------------------------

function get_lundi_dimanche_from_week($week, $year) {
	if (strftime("%W", mktime(0, 0, 0, 01, 01, $year)) == 1)
		$mon_mktime = mktime(0, 0, 0, 01, (01 + (($week -1) * 7)), $year);
	else
		$mon_mktime = mktime(0, 0, 0, 01, (01 + (($week) * 7)), $year);

	if (date("w", $mon_mktime) > 1)
		$decalage = ((date("w", $mon_mktime) - 1) * 60 * 60 * 24);

	$lundi = $mon_mktime - $decalage;
	$dimanche = $lundi + (6 * 60 * 60 * 24);

	return array (
		date("D - d/m/Y", $lundi),
		date("D - d/m/Y", $dimanche)
	);
}

//---------------------------------------------------------------------------------------------------------

function GetMonthName($month) {

	switch ($month) {
		case "01" :
			return "Janvier";
		case "02" :
			return "Février";
		case "03" :
			return "Mars";
		case "04" :
			return "Avril";
		case "05" :
			return "Mai";
		case "06" :
			return "Juin";
		case "07" :
			return "Juillet";
		case "08" :
			return "Aout";
		case "09" :
			return "Septembre";
		case "10" :
			return "Octobre";
		case "11" :
			return "Novembre";
		case "12" :
			return "Décembre";
	}
}
//--------------------------------------------------------------------------------------------------

function getPaymentDelay($idpayment_delay) {

	

	$db = & DBUtil::getConnection();
	$lang = User::getInstance()->getLang();

	$query = "SELECT payment_delay$lang as pay FROM payment_delay WHERE idpayment_delay = '$idpayment_delay' LIMIT 1";

	$rs = $db->Execute($query);

	if ($rs === false)
		die("Impossible de récupérer le délai de paiement");

	return $rs->fields("pay");

}
//--------------------------------------------------------------------------------------------------

function DrawToolTipBox($idorder_supplier, $suppliername) {

	global  $GLOBAL_START_URL;

	$db = & DBUtil::getConnection();

	$query = "
		SELECT b.idbuyer, b.company 
		FROM buyer b, order_supplier os 
		WHERE os.idbuyer = b.idbuyer 
		AND os.idorder_supplier = '$idorder_supplier' 
		LIMIT 1";

	$rs = $db->Execute($query);
	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer le nom de la société pour la commande '$idorder_supplier'");

	$company = $rs->fields("company");
	$idbuyer = $rs->fields("idbuyer");

	//TODO : jointure entre order_supplier_row et order_row approximative

	$query = "
		SELECT DISTINCT( osr.idrow ), osr.reference, osr.quantity, osr.discount_price, osr.summary, orow.discount_price AS pv, orow.quantity AS qte 
		FROM order_supplier_row osr, order_row orow, order_supplier os
		WHERE os.idorder_supplier = '$idorder_supplier'
		AND osr.idorder_supplier = os.idorder_supplier
		AND orow.idorder = os.idorder
		AND orow.reference = osr.reference";

	$rs = $db->Execute($query);

	if ($rs === false)
		die("Impossible de récupérer les infos des lignes de commande");

	$html = "
		<div id='pop$idorder_supplier' class='tooltip'>
		<table border='0' cellspacing='0' align='center'>
				<tr>
				<td colspan=\"9\" style=\"text-align:center;\"><b>" . Dictionnary::translate("gest_com_search_idbuyer") . " " . $idbuyer . "</b> - " . $company . "</td>
				</tr>
				<tr>
				<td colspan=\"9\">&nbsp;</td>
				</tr>
				<tr>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('reference') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('product') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('supplier') . "</td>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('gest_com_quantity') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('gest_com_PHAUHT') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('gest_com_buying_MTHAHT') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('gest_com_MTVTHT') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('gest_com_rough_strock') . "</th>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('gest_com_rough_strock') . " %</th>
				</tr>";

	while (!$rs->EOF()) {

		$TitrePdt = $rs->fields('summary');
		$MTHA = $rs->fields('discount_price') * $rs->fields('quantity');
		$MTVT = $rs->fields('pv') * $rs->fields('qte');
		$MB = $MTVT - $MTHA;
		if ($MTHA > 0 AND $MTVT > 0) {
			$M = (1 - ($MTHA / $MTVT)) * 100;
		} else {
			$M = 0;
		}

		$html .= "
				<tr>
					<td style=\"text-align:center;\">" . $rs->fields('reference') . "</td>
					<td  style=\"text-align:center;\" nowrap=\"true\">$TitrePdt</td>
					<td  style=\"text-align:center;\" nowrap=\"true\">$suppliername</td>
					<td style=\"text-align:center;\">" . $rs->fields('quantity') . "</td>
					<td style=\"text-align:center;\">" . Util::numberFormat($rs->fields('discount_price')) . "</td>
					<td style=\"text-align:center;\">" . Util::numberFormat($MTHA) . "</td>
					<td style=\"text-align:center;\">" . Util::numberFormat($MTVT) . "</td>
					<td style=\"text-align:center;\">" . Util::numberFormat($MB) . "</td>
					<td style=\"text-align:center;\">" . Util::numberFormat($M) . "</td>
				</tr>";

		$rs->MoveNext();

	}

	$html .= "</table>
		</div>";

	echo $html;

}

//--------------------------------------------------------------------------------------------------

function GenerateHTMLForToolTipBoxTel($idorder_supplier) {

	global  $GLOBAL_START_URL;

	$db = & DBUtil::getConnection();

	$query = "
	SELECT sc.phonenumber 
	FROM supplier s, supplier_contact sc, order_supplier os
	WHERE os.idorder_supplier = $idorder_supplier 
	AND s.idsupplier = os.idsupplier
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";

	$rs = $db->Execute($query);
	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer le numéro de téléphone du fournisseur");

	$tel = $rs->fields("phonenumber");

	$html = "
		<div onmouseover=\"document.getElementById('poptel$idorder_supplier').className='tooltiphover';\" onMouseOut=\"document.getElementById('poptel$idorder_supplier').className='tooltip';\" class='tooltipa'><img class=\"helpicon\" src=\"$GLOBAL_START_URL/images/back_office/content/icon_help.gif\" alt=\"\" border=\"0\" />
		<div id='poptel$idorder_supplier' class='tooltip'>
		<table border='0' cellspacing='0' align='center'>
			<tr>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('phone') . "</th>
			</tr>
			<tr>
				<td style=\"text-align:center;\" nowrap>" . $tel . "</td>
			</tr>
		</table>
		</div>
		</div>";

	return $html;

}

//--------------------------------------------------------------------------------------------------

function DrawToolTipBoxTel($idorder_supplier) {

	global  $GLOBAL_START_URL;

	$db = & DBUtil::getConnection();

	$query = "
	SELECT sc.phonenumber 
	FROM supplier s, supplier_contact sc, order_supplier os
	WHERE os.idorder_supplier = '$idorder_supplier'
	AND s.idsupplier = os.idsupplier
	AND sc.idsupplier = s.idsupplier
	AND sc.idcontact = s.main_contact
	LIMIT 1";

	$rs = $db->Execute($query);
	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer le numéro de téléphone du fournisseur");

	$tel = $rs->fields("phonenumber");

	$html = "
		<div id='poptel$idorder_supplier' class='tooltip'>
		<table border='0' cellspacing='0' align='center'>
			<tr>
				<th style=\"text-align:center; vertical-align:middle; font-weight:bold;\">" . Dictionnary::translate('phone') . "</th>
			</tr>
			<tr>
				<td style=\"text-align:center;\" nowrap>" . $tel . "</td>
			</tr>
		</table>
		</div>
		";

	echo $html;

}
//--------------------------------------------------------------------------------------------------

function GenerateHTMLForOrderToolTipBox($idorders) {

	global  $GLOBAL_START_URL;

	$db = & DBUtil::getConnection();

	$query = "SELECT o.status, c.idbuyer, c.lastname, c.firstname, c.phonenumber " .
	"FROM contact c, buyer b,`order` o, order_supplier os " .
	"WHERE os.idorder_supplier='$idorders' " .
	"AND o.idorder = os.idorder " .
	"AND o.idbuyer = c.idbuyer " .
	"AND o.idcontact = c.idcontact";

	$rs = $db->Execute($query);

	if ($rs === false)
		die(Dictionnary::translate("gest_com_impossible_recover_information_order"));

	$html = "<div onMouseOver=\"document.getElementById('popord$idorders').className='tooltipordhover';\" onMouseOut=\"document.getElementById('popord$idorders').className='tooltipord';\" class='tooltiporda'><img class=\"helpicon\" src=\"$GLOBAL_START_URL/images/back_office/content/icon_help.gif\" alt=\"\" border=\"0\" />";

	$html .= "<div id='popord$idorders' class='tooltipord'><table border='0' cellspacing='0' align='center'><tr><th>&nbsp;" . Dictionnary::translate('status') . "&nbsp;</th><th>&nbsp;" . Dictionnary::translate('idbuyer') . "&nbsp;</th><th>&nbsp;" . Dictionnary::translate('lastname') . "&nbsp;</th><th>&nbsp;" . Dictionnary::translate('firstname') . "&nbsp;</td><th>&nbsp;" . Dictionnary::translate('phonenumber') . "&nbsp;</th></tr>";

	$idbuyer = $rs->fields('idbuyer');
	$lastname = $rs->fields('lastname');
	$firstname = $rs->fields('firstname');
	$phonenumber = $rs->fields('phonenumber');
	$status = Dictionnary::translate($rs->fields('status'));

	$html .= "<tr><td>";
	$html .= $status;
	$html .= "</td><td>";
	$html .= $idbuyer;
	$html .= "</td><td nowrap>";
	$html .= $lastname;
	$html .= "</td><tdnowrap>";
	$html .= $firstname;
	$html .= "</td><td>";
	$html .= $phonenumber;
	$html .= "</td></tr>";

	$html .= "</table></div></div>";

	return $html;

}

//--------------------------------------------------------------------------------------------------

function DrawOrderToolTipBox($idorders) {

	global  $GLOBAL_START_URL;

	$db = & DBUtil::getConnection();

	$query = "SELECT o.status,c.idbuyer, c.lastname, c.firstname, c.phonenumber " .
	"FROM contact c, buyer b,`order` o, order_supplier os " .
	"WHERE os.idorder_supplier=$idorders " .
	"AND o.idorder = os.idorder " .
	"AND o.idbuyer = c.idbuyer " .
	"AND o.idcontact = c.idcontact";

	$rs = $db->Execute($query);

	if ($rs === false)
		die(Dictionnary::translate("gest_com_impossible_recover_information_order"));

	$html = "<div id='popord$idorders' class='tooltipord'><table border='0' cellspacing='0' align='center'><tr><th>&nbsp;" . Dictionnary::translate('status') . "&nbsp;</th><th>&nbsp;" . Dictionnary::translate('idbuyer') . "&nbsp;</th><th>&nbsp;" . Dictionnary::translate('lastname') . "&nbsp;</th><th>&nbsp;" . Dictionnary::translate('firstname') . "&nbsp;</td><th>&nbsp;" . Dictionnary::translate('phonenumber') . "&nbsp;</th></tr>";

	$idbuyer = $rs->fields('idbuyer');
	$lastname = $rs->fields('lastname');
	$firstname = $rs->fields('firstname');
	$phonenumber = $rs->fields('phonenumber');
	$status = Dictionnary::translate($rs->fields('status'));

	$html .= "<tr><td>";
	$html .= $status;
	$html .= "</td><td>";
	$html .= $idbuyer;
	$html .= "</td><td nowrap>";
	$html .= $lastname;
	$html .= "</td><tdnowrap>";
	$html .= $firstname;
	$html .= "</td><td>";
	$html .= $phonenumber;
	$html .= "</td></tr>";

	$html .= "</table></div>";

	echo $html;

}
//--------------------------------------------------------------------------------------------------

function getOrderStatus($idorder) {

	

	$db = & DBUtil::getConnection();

	$query = "SELECT status FROM `order` WHERE idorder = '$idorder' LIMIT 1";

	$rs = $db->Execute($query);

	if ($rs === false)
		die("Impossible de récupérer le status de la commande '$idorder'");

	return $rs->fields("status");

}
?>