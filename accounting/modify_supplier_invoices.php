<?
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Modification des règlements et de la facture fournisseurs
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );

$banner = "no";

if(isset($_POST['update']) && $_POST['update']==1 ){
	$ht = $_POST['mtht'];
	$ttc = $_POST['mtttc'];
	
	$bil = $_POST['billing_supplier'];
	$supplier = $_POST['idsupplier'];
	
	DBUtil::query("UPDATE billing_supplier SET total_amount_ht = $ht , total_amount = $ttc WHERE billing_supplier = '$bil' ");
	
	exit();
}

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$invoice = new SupplierInvoice( $_GET['billing'] , $_GET['supplier'] );
?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">

		$(document).ready(function() { 
			
			 var options = {
				success:	   postSubmitCallback  // post-submit callback 	
			};

			$('#invoiceForm').ajaxForm( options );

		});
		
		function postSubmitCallback( responseText, statusText ){
			if(responseText.length == 0){ 
				$.growlUI( '', "Montants enregistrés" );
				window.opener.goForm();
			}
		}
	</script>
	
	<style type="text/css">
<!--
	
	div#global{
		min-width:0;
		width:auto;
	}
	
	div.mainContent div.content div.subContent table.dataTable td.creditAmount{
		text-align:center;
	}
	
	div.mainContent div.content div.subContent table.dataTable td.usedCreditAmount{
		text-align:right;
	}
	
-->
</style>

<div id="globalMainContent" style="margin-top:-25px; width:480px;">	
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>	
    <form id="invoiceForm" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/modify_supplier_invoices.php">
		<div class="mainContent fullWidthContent" style="width:480px;">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="tableContainer">
					<input type="hidden" name="update" value="1" />
					<input type="hidden" name="idsupplier" value="<?=$_GET['supplier']?>" />
					<input type="hidden" name="billing_supplier" value="<?=$_GET['billing']?>" />
					<table class="dataTable">
						<tr>
							<td class="filledCell" style="text-align:left;">Montant HT</td>
							<td class="filledCell" style="text-align:left;"><input class="textInput" style="width:50px;" type="text" id="mtht" name="mtht" value="<?=$invoice->get("total_amount_ht")?>" />&nbsp;&euro;</td>
						</tr>
						<tr>
							<td class="filledCell" style="text-align:left;">Montant TTC</td>
							<td class="filledCell" style="text-align:left;"><input type="text" class="textInput" style="width:50px;" id="mtttc" name="mtttc" value="<?=$invoice->get("total_amount")?>" />&nbsp;&euro;</td>
						</tr>
					</table>
					<input class="orangeButton" style="float:right; margin:10px;" type="submit" value="Ajouter" />
					<input class="blueButton" style="float:right; margin-top:10px;" type="button" onclick="$.unblockUI();" value="Annuler" />
				</div>
			</div>
			<div class="clear"></div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
	</form>
</div>
<?
include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
?>