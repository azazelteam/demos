<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi par mail d'une relance facture client
 */
 

$DEBUG = "";

ini_set( "memory_limit", "64M" );

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php" );

include_once( "$GLOBAL_START_PATH/objects/Invoice.php" );
include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );

include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

/* Changement de modèle de mail ajax */

if( isset( $_GET[ "template" ] ) && !empty( $_GET[ "template" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "idinvoice" ] ) || !intval( $_GET[ "idinvoice" ] ) )
		exit( "0" );
	
	$idinvoice = intval( $_GET[ "idinvoice" ] );
	$Invoice = new Invoice( $idinvoice );
	
	//charger le modèle de mail
	
	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( $Invoice->getCustomer()->get( "id_lang" ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( $_GET[ "template" ] );
	$editor->setUseInvoiceTags( $idinvoice );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $Invoice->get( "idbuyer" ), $Invoice->get( "idcontact" ) );
	$editor->setUseCommercialTags( User::getInstance()->getId() );
	
	$Message = $editor->unTagBody();
	//$Subject = $editor->unTagSubject();
	
	exit( $Message );
	
}

//------------------------------------------------------------------------------------------------

$Title = "Mail de relance envoyé";

$reminder = false;

if(isset($_GET["reminder"]) && !empty($_GET["reminder"]) ){
	$banner = "no";
}

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------

if( empty( $_GET[ "idbilling_buyer" ] ) || empty( $_GET[ "level" ] ) )
	die( Dictionnary::translate("parameter_absent") );

$idbilling_buyer = $_GET[ "idbilling_buyer" ];
$level = $_GET[ "level" ];

$Invoice = new Invoice (  $idbilling_buyer );	

//--------------------------------------------------------------------------------------------

if(isset($_GET["reminder"]) && !empty($_GET["reminder"]) ){
	$reminder = true;
	?>
	<script type="text/javascript">
	window.resizeBy(220,80);
	</script>
	<?
}?>
<script type="text/javascript">
<!--
	
	function changeTemplate( identifier ){
		
		$.blockUI({ message:'Chargement du modèle de mail', 
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }
		});
		
		$.ajax({
			type:		"GET",
			url:		"<?php echo $GLOBAL_START_URL ?>/accounting/transmit_relaunch.php?idinvoice=<?php echo $idbilling_buyer ?>&template=" + identifier,
			success:	function( responseText ){
				tinyMCE.activeEditor.setContent( responseText );
				$.unblockUI();
			}
		});
		
	}
	
-->
</script>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form id="mailForm" action="transmit_relaunch.php?idbilling_buyer=<?php echo $idbilling_buyer ?>&amp;level=<?php echo $level ?><?if($reminder){echo"&amp;reminder=1";}?>" method="post" enctype="multipart/form-data">
		<div class="mainContent" style="width:700px; margin-left:30px;">

<?

if( isset( $_POST[ "send" ] ) || isset( $_POST[ "send_x" ] ) ){
	if( sendMail() ){
		?>	
		<script type="text/javascript">
			$(document).ready(function(){
			$.blockUI({ message:'Relance envoyée par email', 
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }
			});
			$('.blockOverlay').attr('title','Cliquez pour quitter').click($.unblockUI);
		});
		</script>
		<?	
	}else{
		?>	
		<script type="text/javascript">
			$(document).ready(function(){
			$.blockUI({ message:'Problème lors de l'envoi de la relance par email', 
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }
			});
			$('.blockOverlay').attr('title','Cliquez pour quitter').click($.unblockUI);
		});
		</script>
		<?	
		
	}
}

if( isset( $_POST[ "sendFax" ] ) || isset( $_POST[ "sendFax_x" ] ) ){
	if( sendFax() ){
		?>	
		<script type="text/javascript">
			$(document).ready(function(){
			$.blockUI({ message:'Relance envoyée par fax', 
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }
			});
			$('.blockOverlay').attr('title','Cliquez pour quitter').click($.unblockUI);
		});
		</script>
		<?	
	}else{
		?>	
		<script type="text/javascript">
			$(document).ready(function(){
			$.blockUI({ message:'Problème lors de l'envoi de la relance par fax', 
			css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }
			});
			$('.blockOverlay').attr('title','Cliquez pour quitter').click($.unblockUI);
		});
		</script>
		<?	
		
	}
}

displayMailForm();

?>

	    </form>
    </div>
</div>
<?

//--------------------------------------------------------------------------------------------

function displayMailForm(){
	
	global 	$GLOBAL_START_URL, 
			$GLOBAL_START_PATH, 
			$idbilling_buyer,
			$level,
			$Invoice,
			$reminder;
	
	$AdminName= DBUtil::getParameterAdmin( "ad_name" );
	
	$iduser = User::getInstance()->getId();
	
	
	//-----------------------------------------------------------------------------------------
				
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( User::getInstance()->getLang2(), 1 ) ); //@todo : imposer la langue du destinataire
	
	//Débité factor ?
	
	$is_debited = false;
	
	if($Invoice->get("factor_refusal_date") != '0000-00-00')
		$is_debited = true;
	
	if( $is_debited ){
		
		switch( $level ){
			case "1":	$templateIdentifier = "INVOICE_RELAUNCH_FACTOR_1";break;
			case "2":	$templateIdentifier = "INVOICE_RELAUNCH_FACTOR_2";break;
			case "3":	$templateIdentifier = "INVOICE_RELAUNCH_FACTOR_3";break;
		}
		
	}else{
		
		switch( $level ){
			case "1":	$templateIdentifier = "INVOICE_RELAUNCH_1";break;
			case "2":	$templateIdentifier = "INVOICE_RELAUNCH_2";break;
			case "3":	$templateIdentifier = "INVOICE_RELAUNCH_3";break;
		}
		
	}
	
	if($reminder)
		$templateIdentifier = "INVOICE_RELAUNCH_REMINDER";
	
	$editor->setTemplate( $templateIdentifier );
	
	$editor->setUseInvoiceTags( $idbilling_buyer );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $Invoice->get("idbuyer") , $Invoice->get("idcontact") );
	$editor->setUseCommercialTags( User::getInstance()->getId() );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
	
	$faxnumber = $Invoice->getCustomer()->getContact()->get( "faxnumber" );
	$accountant_faxnumber = $Invoice->getCustomer()->get("accountant_faxnumber");
	
	if( isset( $_POST[ "message" ] ) )
		$Message = stripslashes( $_POST[ "message" ] );
	
	//-----------------------------------------------------------------------------------------
				
	?>
	<script type="text/javascript" language="javascript">
	<!--
		
		function GeneratePopupToPrint() {
			w=window.open("",'popup','width=800,height=600,toolbar=no,scrollbars=yes,resizable=yes');
			w.document.write("<html>\n");
			w.document.write("<head>\n");
			w.document.write("<title>Email personnalisé : Visualisation</title>\n");
			w.document.write("</head>\n");
			w.document.write("<body>\n");
			w.document.write("<b>Sujet :</b>  "+document.getElementById("mailForm").elements["subject"].value+"<br /><br />\n");
			w.document.write("<b>Message :</b> "+document.getElementById("mailForm").elements["message"].value);
			w.document.write("<br /><br />");
			w.document.write("<center><input type='button' onclick='window.print()' value='Imprimer'></center>");
			w.document.write("\n</body>\n");
			w.document.write("</html>");
			w.document.close();
		}
		
	-->
	</script>
	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Email d'accompagnement</p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
				<?php
					
					if( !empty( $faxnumber ) || !empty( $accountant_faxnumber ) ){
						
						if( !empty( $accountant_faxnumber ) )
							$infofax = getFaxNumber( $Invoice->getCustomer()->get( "accountant_faxnumber" ) );
						elseif( !empty( $faxnumber ) )
							$infofax = getFaxNumber( $Invoice->getCustomer()->getContact()->get( "faxnumber" ) ); 
						
				?>
					<p style="margin-top:15px;">
						<b>Numéro de fax du destinataire</b> (ex : 0388332074 (France) - 0033388332074 (étranger)) : <input type="text" name="subjectfax" value="<?php echo $infofax ?>" class="textInput" />
						<br />
						<?php if( !empty( $faxnumber ) ){ ?><b>Numéro de fax fiche client (pour rappel) : <?php echo $Invoice->getCustomer()->getContact()->get( "faxnumber" ) ?></b><br /><?php } ?>
						<?php if( !empty( $accountant_faxnumber ) ){ ?><b>Numéro de fax comptable (pour rappel) : <?php echo $Invoice->getCustomer()->get( "accountant_faxnumber" ) ?></b><?php } ?>
					</p>
				<?php } ?>
				<p style="margin-top:15px;">
					<b>Sujet</b><br />
					<input type="text" name="subject" value="<?php echo $Subject ?>" style="width:99%;" class="textInput" />
				</p>
				<p style="margin-top:15px;">
					<b>Modèle</b><br />
					<select name="mailTemplate" style="width:99%;" onchange="changeTemplate(this.value);">
					<?php
						
						$lang = User::getInstance()->getLang();
						
						$rs =& DBUtil::query( "SELECT identifier, name$lang AS name FROM mail_template WHERE identifier LIKE 'INVOICE_RELAUNCH%' ORDER BY name ASC" );
						
						while( !$rs->EOF ){
							
						?>
						<option value="<?php echo $rs->fields( "identifier" ) ?>"<?php if( $rs->fields( "identifier" ) == $templateIdentifier ){ ?> selected="selected"<?php } ?>><?php echo $rs->fields( "name" ) ?></option>
						<?php
							
							$rs->MoveNext();
							
						}
						
					?>
					</select>
				</p>
				<p style="margin-top:15px;">
					<b>Message</b>
					<br />
					<?php WYSIWYGEditor::createEditor( "message", $Message, "style=\"width:600px; height:450px;\"" ); ?>
				</p>
				<p style="color:#cc3333; margin-top:20px; text-align:center;">Vous pouvez personnaliser l'objet et le texte du mail d'accompagnement.</p>
				<p style="margin-top:20px; text-align:center;">
					<?php if( !$reminder ){ ?>
						<input type="submit" class="blueButton" name="send" value="Envoyer mail de relance + facture" />
						<?php if( !empty( $faxnumber ) || !empty( $accountant_faxnumber ) ){ ?><input type="submit" class="blueButton" name="sendFax" value="Envoyer fax de relance + facture" /><?php } ?>
						<input type="button" class="blueButton" name="view" value="Visualiser" onclick="GeneratePopupToPrint();" />
						<br /><br />
						<input type="button" value="Visualiser la facture" class="blueButton" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?=$idbilling_buyer?>'); return false;" />
						<input type="button" class="blueButton" value="Retour aux factures en retard de paiement" onclick="document.location='<?php echo $GLOBAL_START_URL ?>/accounting/invoices_follow-up.php'" style="text-align: center;" />
					<?php }else{ ?>
						<input type="submit" class="blueButton" name="send" value="Envoyer mail + facture" />
						<?php if( !empty( $faxnumber ) || !empty( $accountant_faxnumber ) ){ ?><input type="submit" class="blueButton" name="sendFax" value="Envoyer fax + facture" /><?php } ?>
						<input type="button" class="blueButton" name="view" value="Visualiser" onclick="GeneratePopupToPrint();" />
						<br /><br />
						<input type="button" value="Visualiser la facture" class="blueButton" onclick="window.open('<?php echo $GLOBAL_START_URL ?>/catalog/pdf_invoice.php?idinvoice=<?=$idbilling_buyer?>'); return false;" />
						<input type="button" class="blueButton" value="Retour au suivi" onclick="document.location='<?php echo $GLOBAL_START_URL ?>/accounting/billing_relaunch_comment.php?idbilling_buyer=<?=$idbilling_buyer?>'" style="text-align: center;" />
						
					<?php } ?>
				</p>
			</div>
   	    </div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<div id="tools" class="rightTools">
    	 <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Destinataires</p>
            </div>
            <div class="content">
				<table border="0">
					<tr>
						<th>Destinataires</th>
						<th>A</th>
						<th>Cci</th>
					</tr>
					<?php if( $Invoice->getCustomer()->get( "accountant_email" ) != "" ){ ?>
						<tr>
							<td><?php echo $Invoice->getCustomer()->get( "accountant" ) ?> <b>(comptable)</b></td>
							<td align="center"><input type="checkbox" disabled="disabled" checked="checked" /></td>
							<td align="center"><input type="checkbox" disabled="disabled" /></td>
						</tr>
					<?php }else{ ?>
						<tr>
							<td><?php echo $Invoice->getCustomer()->getContact()->get( "firstname" ) . " " . $Invoice->getCustomer()->getContact()->get( "lastname" ) ?> <b>(client)</b></td>
							<td align="center"><input type="checkbox" disabled="disabled" checked="checked" /></td>
							<td align="center"><input type="checkbox" disabled="disabled" /></td>
						</tr>
					<?php } ?>
				</table>
			</div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>

<?php
	
}

//-------------------------------------------------------------------------------------------------
function sendMail(){
	
	global	$GLOBAL_START_PATH,
			$Invoice,
			$idbilling_buyer,
			$level;
	
	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$iduser = User::getInstance()->getId();
	$lastupdate = date( "Y-m-d H:i:s" );

	$DEBUG = "";
	
	//email commercial commande
	$queryc = "SELECT email
			FROM user u, `order` o, billing_buyer bb
			WHERE bb.idbilling_buyer = $idbilling_buyer
			AND bb.idorder = o.idorder
			AND o.iduser = u.iduser";
	$rsc =& DBUtil::query( $queryc );
	
	if($rsc===false)
		die ("Impossible de récupérer le mail du commercial de la commande");
	
	$email_business = $rsc->fields("email");
	
	$AdminName= DBUtil::getParameterAdmin( "ad_name" );
	$AdminEmail = DBUtil::getParameterAdmin( "ad_mail" );

	$CommercialName= User::getInstance()->get( "firstname" ).' '.User::getInstance()->get( "lastname" );
	$CommercialEmail = User::getInstance()->get( "email" );		

	$mailer = new htmlMimeMail();
	
	$AdminName=DBUtil::getParameterAdmin( "ad_name" );
	$mailer->setFrom( '"'.$AdminName.' - '.$CommercialName.'" <'.$CommercialEmail.'>'); 
	
	//-----------------------------------------------------------------------------------------
	
	$Subject = stripslashes( $_POST[ "subject" ] );
	$Message = stripslashes( $_POST[ "message" ] );
	
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );

	//-----------------------------------------------------------------------------------------------

	//ajout du pdf

	$pdf_name = "Facture_$idbilling_buyer.pdf";

	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
	$odf = new ODFInvoice( $idbilling_buyer );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment( $pdfData, $pdf_name, 'application/pdf' );
	
	if($Invoice->getCustomer()->get("accountant_email")!='') {
		$expa = $Invoice->getCustomer()->get("accountant_email");
		//$mailer->setBcc($Invoice->getCustomer()->getContact()->get( "mail" ));
	}else{
		$expa = $Invoice->getCustomer()->getContact()->get( "mail" );
	}
	
	//copie au commercial de la commande et de la facture
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setBcc( $CommercialEmail );
		
	$mailer->setBcc( $email_business );
	
	$result = $mailer->send( array( "$expa" ) );
	
	if( !$result )
		return false;
	
	//On met à jour la facture
				
	$Invoice->set( "relaunch_date_$level" , date("Y-m-d") );
	$Invoice->set( "relaunch_level" , $level );
	$Invoice->set( "relaunch_type_$level" , 1 );
	$Invoice->set( "relaunch_iduser" , $iduser );
	$Invoice->set( "relaunch_lastupdate" , $lastupdate );
	
	$Invoice->save();
	
	return true;
}

//------------------------------------------------------------------------------------------------------------------------

function SendFax(){
	
	global $GLOBAL_START_PATH, $Invoice, $idbilling_buyer, $level;
	
	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$iduser = User::getInstance()->getId();
	$lastupdate = date( "Y-m-d H:i:s" );

	$DEBUG = '';
	
	//email commercial commande
	$queryc = "SELECT email
			FROM user u, `order` o, billing_buyer bb
			WHERE bb.idbilling_buyer = $idbilling_buyer
			AND bb.idorder = o.idorder
			AND o.iduser = u.iduser";
	$rsc =& DBUtil::query( $queryc );
	
	if($rsc===false)
		die ("Impossible de récupérer le mail du commercial de la commande");
	
	$email_business = $rsc->fields("email");
	
	$AdminName= DBUtil::getParameterAdmin( "ad_name" );
	$AdminEmail = DBUtil::getParameterAdmin( "ad_mail" );

	$BuyerEmail = $Invoice->getCustomer()->getContact()->get( "mail" );
		
	$CommercialEmail = User::getInstance()->get( "email" );		

	$mailer = new htmlMimeMail();

	$smtp_server = DBUtil::getParameterAdmin('fax_smtp_server');
	$smtp_sender = DBUtil::getParameterAdmin('fax_smtp_sender');
	$smtp_auth = DBUtil::getParameterAdmin('fax_smtp_auth');
	$smtp_user = DBUtil::getParameterAdmin('fax_smtp_user');
	$smtp_password = DBUtil::getParameterAdmin('fax_smtp_password');
	$smtp_recipient = DBUtil::getParameterAdmin('fax_smtp_recipient');
	$AdminName= DBUtil::getParameterAdmin( "ad_name" );
	
	$mailer->setSMTPParams($smtp_server, 25, $smtp_sender,$smtp_auth,$smtp_user,$smtp_password);
	
	$mailer->setFrom('"'.$AdminName.'" <'.$smtp_user.'>');


	//-----------------------------------------------------------------------------------------
	

	$Subject = stripslashes( $_POST[ "subjectfax" ] );
	$Message = stripslashes( $_POST[ "message" ] );
				
	//-----------------------------------------------------------------------------------------
	
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );


	//-----------------------------------------------------------------------------------------------
	
	//ajout du pdf

	$pdf_name = "Facture_$idbilling_buyer.pdf";
	
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
	$odf = new ODFInvoice( $idbilling_buyer );
	$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
	
	$mailer->addAttachment( $pdfData, $pdf_name, 'application/pdf' );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setBcc( $CommercialEmail );
	
	$mailer->setBcc( $email_business );
	
	$result = $mailer->send(array("$smtp_recipient"),'smtp');
	
	if( !$result )  
		return false;	
	
	
	//On met à jour la facture
				
	$Invoice->set( "relaunch_date_$level" , date("Y-m-d") );
	$Invoice->set( "relaunch_level" , $level );
	$Invoice->set( "relaunch_type_$level" , 2 );
	$Invoice->set( "relaunch_iduser" , $iduser );
	$Invoice->set( "relaunch_lastupdate" , $lastupdate );
	
	$Invoice->save();
	
	return true;
}

//--------------------------------------------------------------------------------------------

function HumanReadablePhone($tel){
	//On regarde la longeueur de la cheine pour parcourir
	$lg=strlen($tel);

	$telwcs='';
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($tel,$i,1);
	
		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if($i==0){
			if(($tocheck=='+')OR(is_numeric($tocheck))){
				$telwcs.=$tocheck;
			}
		}else{
			//On ne garde que les chiffres
			if(is_numeric($tocheck)){
				$telwcs.=$tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg=strlen($telwcs);

	$telok ='';

	
	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck=substr($telwcs,0,1);

	if($tocheck=='+'){
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,3);
		$telok.=$tocheck." ";
		$cpt=3;
	}else{
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,2);
		$telok.=$tocheck." ";
		$cpt=2;
	}

	//On traite le reste de la chaine
	for($i=$cpt;$i<$lg;$i+=2){
		if($i==$cpt){
			$tocheck=substr($telwcs,$i,2);
		}else{
			$tocheck=substr($telwcs,$i,2);
		}
		$telok.=$tocheck." ";
	}	

	//On enlève l'espace de fin
	$telok=trim($telok);

	return $telok;
}    

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//--------------------------------------------------------------------------------

function getFaxNumber($fax){
		
	$lg = strlen($fax);
	$faxok='';
	
	//On enleve les espaces et les caractères qui ne sont pas des chiffres
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($fax,$i,1);
	
		//On ne garde que les chiffres
		if(is_numeric($tocheck)){
			$faxok.=$tocheck;
		}
	}
	
	return $faxok;
	
}

//--------------------------------------------------------------------------------------------------
function checkBuyerFax($idbuyer,$idcontact){
	
	//On contrôle que le client possède un fax pour afficher la checkbox d'envoi par fax
	$q = "SELECT c.faxnumber FROM contact c WHERE c.idcontact=$idcontact AND c.idbuyer=$idbuyer";
	$r =& DBUtil::query( $q );
	
	if( $r === false )
		die( "Impossible de récupérer le fax du client" );
	
	$fax = $r->fields("faxnumber");
	
	if(!empty($fax)){
		return $fax;
	}else{
		$q2 = "SELECT fax_standard FROM buyer WHERE idbuyer=$idbuyer";
		$r2 =& DBUtil::query( $q2 );
		if( $r2 === false ){
			die( "Impossible de récupérer le fax du client" );
		}
		$fax = $r2->fields("fax_standard");
		if(!empty($fax)){
			return $fax;
		}else{
			return false;
		}
	}
}

//--------------------------------------------------------------------------------
function updateSending( $idbilling_buyer, $niveau, $Message, $Subject ){
	
	//premier id de libre
	$query = "SELECT MAX(idrelaunch_sending) AS id FROM relaunch_sending";
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le premier enregistrement disponible" );
		
	$idrelaunch_sending = $rs->fields("id");
	
	if($idrelaunch_sending==0)
		$idrelaunch_sending=1;
		
	//On met à jour la base
	
	$qsvg = "INSERT INTO relaunch_sending (
			idrelaunch_sending,
			idbilling_buyer,
			level,
			subject,
			message)
			VALUES (
			'$idrelaunch_sending',
			'$idbilling_buyer',
			'$niveau',
			'".Util::html_escape($Subject)."',
			'".Util::html_escape($Message)."'
			)";
	$rssvg = DBUtil::query( $qsvg );
	
	if( $rssvg === false )
		die( "Impossible de sauvegarder les données envoyées" );
		
}
?>