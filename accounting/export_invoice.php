<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Export factures clients
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/CSVInvoiceExport.php" );
include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );

//---------------------------------------------------------------------------------------
// Vérifiaction du login 
//---------------------------------------------------------------------------------------

//connexion
if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) )
	User::getInstance()->login( stripslashes( $_POST[ "Login" ] ), md5( stripslashes( $_POST[ "Password" ] ) ) );
	
if( !User::getInstance()->getId() ){
	
	$banner = "no";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "expired_session" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

if( !isset( $_GET[ "query" ] ) ){

	die( "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>" );

}

global $GLOBAL_DB_PASS;
$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "query" ] ), $GLOBAL_DB_PASS );

$rs =& DBUtil::query( $query );

if( $rs === false )
	die( "Impossible de récupérer la liste des factures à traiter" );

$idinvoices = array();
while( !$rs->EOF() ){
	
	$idinvoices[] = $rs->fields( "idbilling_buyer" );
	
	$rs->MoveNext();
		
}

if( !$rs->RecordCount() ){
	
	?>
	<p style="text-align:center;">
		Aucune facture trouvée
	</p>
	<p style="text-align:center;">
		<input type="button" value="Fermer" onclick="window.close();" />
	</p>
	<?php
	
	exit();
		
}

$date = date( "Y_m_d" );
$filename = "export_factures_$date";

$csvInvoiceExport = new CSVInvoiceExport( $filename );
$csvInvoiceExport->setTextDelimiter( "" );
$csvInvoiceExport->setSeparator( ";" );
$csvInvoiceExport->setBreakLine( "\n" );
$csvInvoiceExport->setInvoices( $idinvoices );
$csvInvoiceExport->export();

?>