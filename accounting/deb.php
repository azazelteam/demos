<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion DEB fournisseurs
 */

$Title = "DEB";

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );

//------------------------------------------------------------------------------------------------
/* transmission par voie informatique des informations statistiques concernant le format DEB ( it rocks! ) */

if( isset( $_REQUEST[ "export" ] ) == $_REQUEST[ "export" ] = "deb" ){
	
	list( $year, $month ) = explode( "-", $_POST[ "month" ] );

	//header( "Content-type: application/octetstream" );
	header( "Content-Disposition: attachment; filename=\"deb_{$month}_{$year}.txt\"");
	
	include( dirname( __FILE__ ) . "/deb_export.php" );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------
/* déclaration effectuée */

if( isset( $_REQUEST[ "declare" ] ) && isset( $_REQUEST[ "month" ] ) )
	DBUtil::query( "INSERT IGNORE INTO deb_declaration( `date` ) VALUES( " . DBUtil::quote( $_REQUEST[ "month" ] ) . " )" );

//------------------------------------------------------------------------------------------------
/**
 * récupérer les données à afficher ou à exporter
 */

if( isset( $_REQUEST[ "month" ] ) )
		list( $year, $month ) = explode( "-", $_REQUEST[ "month" ] );
else 	list( $year, $month ) = explode( "-", date( "Y-m" ) );

$data = getData( $year, $month );

//------------------------------------------------------------------------------------------------
/**
 * exporter les données
 */

if( isset( $_GET[ "export" ] ) && $_GET[ "export" ] == "csv" ){

	exportArray( getExportableArray( $data ) );

	exit();
	
}

//------------------------------------------------------------------------------------------------
/**
 * Afficher les données
 */

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<form method="post" action="deb.php?export=deb" enctype="multipart/form-data" name="DEBForm" id="DEBForm">
<?php

displayForm( $data );
displayData( $data );

?>
</form>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------
/**
 * Exporter les données
 */
 
function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray, "export_deb_" . date( "Ymd" ) . ".csv" );
	$cvsExportArray->export();
	
}

//------------------------------------------------------------------------------------------------
/**
 * Afficher le formulaire
 */
 
function displayForm( array &$data ){
	
	global $GLOBAL_START_URL;

	?>
	<div id="globalMainContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="mainContent">
	        <div class="topRight"></div>
	        <div class="topLeft"></div>
	        <div class="content">
	        	<div class="headTitle">
	                <p class="title">Déclaration d'échange de biens</p>
	                <div class="rightContainer"></div>
	            </div>
	            <div class="subContent">
	            	<input type="hidden" name="search" value="1" />
	            	<!-- tableau choix de date -->
                    <p style="text-align:right;">
                        Afficher la marchandise à déclarer pour le mois de 
						<select id="month" name="month" onchange="document.location='<?php echo URLFactory::getHostURL(); ?>/accounting/deb.php?month=' + escape( this.options[ this.selectedIndex ].value );">
						<?php
						
							$i = 0;
							while( $i < 48 ){
					
								list( $year, $month ) = explode( "-", date( "Y-m", mktime( 0, 0, 0, date( "m" ) - $i, 1, date( "Y" ) ) ) );
								
								$selected = isset( $_REQUEST[ "month" ] ) && $_REQUEST[ "month" ] == "$year-$month" ? " selected=\"selected\"" : "";
								$style = DBUtil::query( "SELECT date FROM deb_declaration WHERE date LIKE '$year-$month' LIMIT 1" )->RecordCount() ? "text-decoration:line-through;" : "text-decoration:normal;";
								
								?>
								<option style="<?php echo $style; ?>"<?php echo $selected; ?> value="<?php echo "$year-$month"; ?>" style="font-weight:bold;">
									<?php echo strftime( "%B", mktime( 0, 0, 0, $month, 1, $year ) ) . " $year"; ?>
								</option>
								<?php
								
								$i++;
								
							}
							
						?>
						</select>
					</p>
					<?php 
					
						/* recherche d'erreurs */
					
						$error = false;
						
						/* code douane non renseigné */
						
						$i = 0;
						while( $i < count( $data[ "code_customhouse" ] ) ){

							if( !$data[ "code_customhouse" ][ $i ] ){

								?>
								<p>
									<a href="<?php echo URLFactory::getHostURL(); ?>/product_management/catalog/detail.php?idarticle=<?php echo $data[ "idarticle" ][ $i ]; ?>" onclick="window.open(this.href); return false;" style="color:#FF0000;">
										Le code douane n'a pas été renseigné pour la référence <?php echo $data[ "reference" ][ $i ]; ?>. 
										Cliquez ici pour corriger le problème.
									</a>
								</p>
								<?php
							
								$error = true;
								
							}

							$i++;
							
						}
						
						/* code douane mal renseigné */
						
						$i = 0;
						while( $i < count( $data[ "code_customhouse" ] ) ){

							if( strlen( $data[ "code_customhouse" ][ $i ] ) && strlen( $data[ "code_customhouse" ][ $i ] ) != 8 ){

								?>
								<p>
									<a href="<?php echo URLFactory::getHostURL(); ?>/product_management/catalog/detail.php?idarticle=<?php echo $data[ "idarticle" ][ $i ]; ?>" onclick="window.open(this.href); return false;" style="color:#FF0000;">
										Le code douane <i><?php echo $data[ "code_customhouse" ][ $i ]; ?></i> ne respecte pas la norme NC8. 
										Cliquez ici pour corriger le problème.
									</a>
								</p>
								<?php
							
								$error = true;
								
							}
							
							$i++;
							
						}
						
						/* date d'expédition non renseignée */
						
						$i = 0;
						while( $i < count( $data[ "code_customhouse" ] ) ){

							if( !strlen( $data[ "dispatch_date" ][ $i ] ) || $data[ "dispatch_date" ][ $i ] == "0000-00-00" ){

								?>
								<p>
									<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/supplier_order.php?IdOrder=<?php echo $data[ "idorder_supplier" ][ $i ]; ?>" onclick="window.open(this.href); return false;" style="color:#FF0000;">
										La date d'expédition n'est pas renseignée dans la Cde fournisseur n° <?php echo $data[ "idorder_supplier" ][ $i ]; ?>. 
										Cliquez ici pour corriger le problème.
									</a>
								</p>
								<?php
							
								$error = true;
								
							}

							$i++;
							
						}
						
						/* code TVA du client non ou mal renseigné */
						
						$i = 0;
						while( $i < count( $data[ "code_customhouse" ] ) ){
			
							if( $data[ "sourceIdState" ][ $i ] == 1 && !strlen( trim( $data[ "customerVATCode" ][ $i ] ) ) ){
	
								?>
								<p>
									<a href="<?php echo URLFactory::getHostURL(); ?>/sales_force/contact_buyer.php?key=<?php echo $data[ "idbuyer" ][ $i ]; ?>" onclick="window.open(this.href); return false;" style="color:#FF0000;">
										Le code TVA du partenaire étranger est mal ou non renseigné. 
										Cliquez ici pour corriger le problème.
									</a>
								</p>
								<?php
							
								$error = true;
								
							}
				
							$i++;
		
						}
						
						/* autoriser la validation */
						
						$currentDate = isset( $_REQUEST[ "month" ] ) ? $_REQUEST[ "month" ] : date( "Y-m" );
						$rs =& DBUtil::query( "SELECT date FROM deb_declaration WHERE date LIKE '$currentDate' LIMIT 1" );
						
						if( $currentDate != date( "Y-m" ) && $rs->RecordCount() ){
								
							?>
							<div style="color:#0000FF; text-align:right;">DEB effectuée</div>
							<?php
	
						}	
						else if( !$error ){
	
							?>
							<p style="text-align:right; margin-top:4px;">
								<input id="DeclareButton" type="button" class="blueButton" value="DEB effectuée" onclick="document.location='<?php echo URLFactory::getHostURL() . "/accounting/deb.php?declare&month="; ?>' + $( '#month' ).val();" />
							</p>
							<?php
				
						}

					?>
	            </div><!-- subContent -->
	        </div><!-- content -->
        	<div class="bottomRight"></div>
        	<div class="bottomLeft"></div>
    	</div> <!-- mainContent -->
	    <div id="tools" class="rightTools">
	    	<div class="toolBox">
	        	<div class="header">
	            	<div class="topRight"></div><div class="topLeft"></div>
	                <p class="title">Outils</p>
	            </div>
	            <div class="content" style="padding:10px 10px 0 10px;">
		            <?php
		            /*<a href="#" onclick="document.getElementById('DEBForm').action='deb.php?export=csv'; document.getElementById('DEBForm').submit(); return false;" style="text-decoration:none;">
						<img src="/images/back_office/content/csv_icon.gif" alt="" style="border-style:none; vertical-align:middle;" />
						Export CSV
					</a>*/
		            
		            if( !$error ){
		            	
			            ?>
						<a href="#" onclick="$('#DEBForm').submit();" style="text-decoration:none;">
							<img src="/images/back_office/content/csv_icon.gif" alt="" style="border-style:none; vertical-align:middle;" />
							Export DEB
						</a>
						<?php
						
		            }
		            
		        ?>
	            </div>
	            <div class="footer">
	            	<div class="bottomLeft"></div>
	                <div class="bottomMiddle"></div>
	                <div class="bottomRight"></div>
	            </div>
	        </div>
	    </div> <!-- tools -->
	<?php
	
}

//------------------------------------------------------------------------------------------------
/**
 * Afficher les résultats de la recherche
 */
 
function displayData( &$data ){

	global $GLOBAL_START_URL;
	
	setlocale( LC_TIME, "fr_FR.utf8" );
	
	$codeCustomhouseCount = count( $data[ "code_customhouse" ] );
	
	$totalET						= 0.0;
	$totalWeight					= 0.0;
	$quantity						= 0.0;
	$totalETPerCodeCustomhouse		= 0.0;
	$totalWeigthPerCodeCustomhouse	= 0.0;
	$quantityPerCodeCustomhouse		= 0;
	$totalInvoicedET 				= 0.0;
	$totalChargesET					= 0.0;
	
	$i = 0;
	$lastInvoice = false;
	
	while( $i < $codeCustomhouseCount ){
		
		//date de facturation
		
		list( $year, $month, $day ) = explode( "-", $data[ "Date" ][ $i ] );
		$euDate = "$day-$month-$year";
		
		//totaux pour chaque code douane
		
		$totalETPerCodeCustomhouse		+= $data[ "discount_price" ][ $i ] * $data[ "quantity" ][ $i ];
		$totalWeigthPerCodeCustomhouse	+= $data[ "weight" ][ $i ] * $data[ "quantity" ][ $i ];
		$quantityPerCodeCustomhouse		+= $data[ "quantity" ][ $i ];
		
		//nouveau fournisseur
		if( !$i || $data[ "idsupplier" ][ $i ] != $data[ "idsupplier" ][ $i -1 ] ){
			
			?>
		     <div class="mainContent fullWidthContent">
		    	<div class="topRight"></div><div class="topLeft"></div>
		        <div class="content">
		        	<div class="headTitle">
		                <p class="title">
		                	Fournisseur : <?php echo htmlentities( $data[ "supplierName" ][ $i ] ) ?>
		                </p>
						<div class="rightContainer"></div>
		            </div>
		            <div class="subContent">
						<div class="tableHeaderLeft">
							
						</div>
						<div class="tableHeaderRight">
							
						</div>
		            	<!-- tableau résultats recherche -->
		                <div class="resultTableContainer clear">
		                	<table class="dataTable resultTable">
	    					<?php
	    	
		}
		
		//nouveau code douane ou nouveau fournisseur
		if( !$i || $data[ "code_customhouse" ][ $i ] != $data[ "code_customhouse" ][ $i -1 ] || $data[ "idsupplier" ][ $i ] != $data[ "idsupplier" ][ $i -1 ] ){
			
			?>   	
        	<thead>
                <tr>
					<?php 
					
					if( !$data[ "code_customhouse" ][ $i ] ){ 
						
						?>
						<th colspan="10" style="font-weight:bold;text-align:left;">
							<a href="<?php echo URLFactory::getHostURL(); ?>/product_management/catalog/detail.php?idarticle=<?php echo $data[ "idarticle" ][ $i ]; ?>" onclick="window.open(this.href); return false;" style="color:#FF0000;">
								Code douane non renseigné
							</a>
						</th>
						<?php 
						
					} 
					else if( strlen( $data[ "code_customhouse" ][ $i ] ) != 8 ){ 
						
						?>
						<th colspan="10" style="font-weight:bold;text-align:left;">
							<a name="CodeCustomhouseError">
							<a href="<?php echo URLFactory::getHostURL(); ?>/product_management/catalog/detail.php?idarticle=<?php echo $data[ "idarticle" ][ $i ]; ?>" onclick="window.open(this.href); return false;" style="color:#FF0000;">
								Code douane non valide : <?php echo $data[ "code_customhouse" ][ $i ]; ?>
							</a>
						</th>
						<?php 
						
					} 
					else if( $data[ "code_customhouse" ][ $i ] ){ 
					
						?>
						<th colspan="10" style="font-weight:bold;text-align:left;">
							Code douane <?php echo " : <b>" . $data[ "code_customhouse" ][ $i ] . " </b>( " . DBUtil::getDBValue( "code_customhouse_text", "code_customhouse", "code_customhouse", $data[ "code_customhouse" ][ $i ] ) . " )"; ?> 
						</th>
						<?php 
					
					}
					
				?>
				</tr>
               	<tr>
					<th>Pays</th>
					<th>Facture fournisseur n°</th>
					<th>Date facture fournisseur</th>
					<th>Date enregistrement</th>
					<th>Commande client n°</th>
					<th>Référence</th>
					<th>Référence fournisseur</th>
					<th>Poids</th>
					<th>Quantité</th>
					<th>Prix de vente HT</th>
				</tr> 
            </thead>
			<?php
			
		}
			
		?>
        <tbody>
        	<tr class="blackText">
				<td class="lefterCol">
				<?php 
			
					if( $data[ "code_customhouse" ][ $i ] ){
						
						?>
						<input type="hidden" name="declare[]" value="<?php echo htmlentities( $data[ "billing_supplier" ][ $i ] ); ?>,<?php echo $data[ "idsupplier" ][ $i ]; ?>,<?php echo $data[ "idbl_delivery" ][ $i ]; ?>,<?php echo $data[ "idrow" ][ $i ]; ?>" />
						<?php
							
					}
					
					echo htmlentities( $data[ "supplierStateName" ][ $i ] );
					
				?>
				</td>
				<td><?php echo $data[ "billing_supplier" ][ $i ] ?></a></td>
				<td><?php echo $euDate ?></td>
				<td><?php echo usDate2eu($data[ "creation_date" ][ $i ]) ?></td>
				<td>
					<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $data[ "idorder" ][ $i ] ?>" onclick="window.open(this.href); return false;"><?php echo $data[ "idorder" ][ $i ] ?>
				</td>
				<td><?php echo htmlentities( $data[ "reference" ][ $i ] ) ?></td>
				<td><?php echo htmlentities( $data[ "ref_supplier" ][ $i ] ) ?></td>
				<td style="text-align:right; white-space:nowrap;"><?php echo $data[ "weight" ][ $i ] *  $data[ "quantity" ][ $i ] ?> kg</td>
				<td><?php echo $data[ "quantity" ][ $i ] ?></td>
				<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $data[ "quantity" ][ $i ] * $data[ "discount_price" ][ $i ] )?></td>
			</tr>
			<?php
			
			//fin de code douane
			
			if( $i ==  $codeCustomhouseCount -1 || $data[ "code_customhouse" ][ $i ] != $data[ "code_customhouse" ][ $i + 1 ] ){
				
				?>
				<tr>
					<td colspan="7" style="border-style:none; background-color:#FFFFFF;"></td>
					<th style="text-align:right; white-space:nowrap;"><?php echo $totalWeigthPerCodeCustomhouse ?> kg</th>
					<th style="text-align:center;"><?php echo $quantityPerCodeCustomhouse ?></th>
					<th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $totalETPerCodeCustomhouse ) ?></th>
				</tr>
				<?php
				
				$totalETPerCodeCustomhouse 		= 0.0;
				$totalWeigthPerCodeCustomhouse 	= 0.0;
				$quantityPerCodeCustomhouse 	= 0;
				
			}

			//fin de fournisseur
			
			if( $i ==  $codeCustomhouseCount -1 || $data[ "idsupplier" ][ $i ] != $data[ "idsupplier" ][ $i + 1 ] ){
						
						?>
						</tbody>
						</table>
						</div>
		           	</div>
		   		</div>
		        <div class="bottomRight"></div><div class="bottomLeft"></div>
				<?php
			
			}
			
			//totaux pour tous les fournisseurs
			
			$totalET 			+= $data[ "discount_price" ][ $i ] * $data[ "quantity" ][ $i ];
			$totalWeight 		+= $data[ "weight" ][ $i ] * $data[ "quantity" ][ $i ];
			$quantity 			+= $data[ "quantity" ][ $i ];

			$lastInvoice = $data[ "billing_supplier" ][ $i ];
			
			$i++;
			
			if( $i > $codeCustomhouseCount || ( $i && $data[ "billing_supplier" ][ $i ] != $data[ "billing_supplier" ][ $i -1 ] ) ){
				
				$totalInvoicedET 	+= $data[ "total_amount_ht" ][ $i - 1 ];
				$totalChargesET 	+= $data[ "total_charge_ht" ][ $i - 1 ];

			}
			
		?>			
    	</div><!-- mainContent fullWidthContent -->
		<?php
	
	}
	
	// Ligne de statistiques et d'exports
	
			?>
	        <div class="mainContent fullWidthContent">
	    	<div class="topRight"></div><div class="topLeft"></div>
	        <div class="content">
	        	<div class="headTitle">
	                <p class="title">Totaux </p>
					<div class="rightContainer"></div>
	            </div>
	            <div class="subContent">
					<div class="tableHeaderLeft">
						
					</div>
					<div class="tableHeaderRight"></div>
	            	<!-- tableau résultats recherche -->
	                <div class="resultTableContainer clear">
	                	<table class="dataTable resultTable">
	                    	<tbody>
	                            <tr>
									<th>Poids total : <?php echo $totalWeight ?> kg</th>
									<th>Quantité : <?php echo $quantity ?></th>
									<th>Total produit HT : <?php echo Util::priceFormat( $totalET ) ?></th>
									<th>Total facture HT : <?php echo Util::priceFormat( $totalInvoicedET ) ?></th>
									<th>Total port HT : <?php echo Util::priceFormat( $totalChargesET ) ?></th>
								</tr>
	                        </tbody>
	                    </table>
	                </div>
	                
	           	</div>
	   		</div>
	        <div class="bottomRight"></div><div class="bottomLeft"></div>
	    </div> <!-- mainContent fullWidthContent des totaux-->
	</div> <!-- GlobalMainContent -->
	<?php
}

//------------------------------------------------------------------------------------------------
/**
 * Recherche
 */
 
function &getData( $year, $month ){
	
	$lang				= User::getInstance()->getLang();
	$default_idstate	= DBUtil::getParameter( "default_idstate" );
	
	$start	= "$year-$month-01";
	$end	= date( "Y-m-d", mktime( 0, 0, 0, $month + 1, 1, $year ) );
	
	$query = "
	SELECT DISTINCT bs.idsupplier,
		s.name AS supplierName,
		d.code_customhouse,
		bs.Date,
		bs.creation_date,
		bs.billing_supplier,
		bs.total_amount_ht,
		bs.total_charge_ht,
		bsr.idrow,
		bl.idbl_delivery,
		bl.idorder,
		blr.idarticle,
		blr.reference, 
		blr.ref_supplier,
		blr.quantity,
		osr.weight,
		ROUND( osr.discount_price * ( 1.0 - os.billing_rate / 100.0 ), 2 ) AS discount_price,
		bl.dispatch_date,
		os.idorder_supplier,
		supplierState.name$lang AS supplierStateName,
		IF( bl.iddelivery, delivery.idstate, b.idstate ) AS destinationIdState,
		s.idstate AS sourceIdState,
		b.tva AS customerVATCode,
		b.idbuyer
	FROM billing_supplier bs,
		supplier s,
		state supplierState,
		state destinationState,
		bl_delivery bl LEFT JOIN delivery ON delivery.iddelivery = bl.iddelivery,
		bl_delivery_row blr,
		billing_supplier_row bsr,
		order_supplier_row osr,
		order_supplier os,
		detail d,
		buyer b
	WHERE s.idsupplier = bs.idsupplier
	AND s.idstate = supplierState.idstate
	AND IF( bl.iddelivery, delivery.idstate, b.idstate ) = destinationState.idstate
	AND bl.idbl_delivery = blr.idbl_delivery
	AND bsr.billing_supplier = bs.billing_supplier
	AND bsr.idsupplier = bs.idsupplier
	AND bsr.idbl_delivery = blr.idbl_delivery
	AND bsr.idrow = blr.idrow
	AND blr.idarticle = d.idarticle
	AND osr.idorder_supplier = bl.idorder_supplier
	AND osr.idarticle = blr.idarticle
	AND os.idorder_supplier = osr.idorder_supplier
	AND bs.editable = 0
	AND bs.proforma = 0
	AND bs.Date >= '$start' AND bs.Date < '$end'
	AND b.idbuyer = bl.idbuyer
	AND b.idcustomer_profile > 1
	-- importation ou exportation intracommunataire :
	AND( ( s.idstate = 1 AND IF( bl.iddelivery, delivery.idstate, b.idstate ) <> 1 ) OR ( s.idstate <> 1 AND IF( bl.iddelivery, delivery.idstate, b.idstate ) = 1 ) )
	AND supplierState.code_eu IN( 'INTRA', 'NATIONAL' ) 
	AND destinationState.code_eu IN( 'INTRA', 'NATIONAL' )
	AND ( supplierState.code_eu <> 'NATIONAL' OR destinationState.code_eu <> 'NATIONAL' )
	-- 
	ORDER BY s.name ASC, d.code_customhouse ASC, bs.Date ASC, s.idstate ASC, bs.billing_supplier ASC, osr.reference ASC";

	$rs =& DBUtil::query( $query );

	$data = array(
	
		"idsupplier" 			=> array(),
		"supplierName" 			=> array(),
		"code_customhouse"		=> array(),
		"billing_supplier" 		=> array(),
		"idbl_delivery" 		=> array(),
		"idrow" 				=> array(),
		"total_amount_ht" 		=> array(),
		"total_charge_ht" 		=> array(),
		"Date" 					=> array(),
		"creation_date"			=> array(),
		"idorder" 				=> array(),
		"idarticle" 			=> array(),
		"reference"				=> array(),
		"ref_supplier"			=> array(),
		"quantity"				=> array(),
		"weight" 				=> array(),
		"discount_price"		=> array(),
		"supplierStateName" 	=> array(),
		"customerVATCode"		=> array(),
		"sourceIdState" 		=> array(),
		"destinationIdState" 	=> array(),
		"idbuyer" 				=> array(),
		"dispatch_date" 		=> array(),
		"idorder_supplier" 		=> array()
	
	);
	
	while( !$rs->EOF() ){
		
		$keys = array_keys( $data );
		
		foreach( $keys as $key )
				$data[ $key ][] = $rs->fields( $key );		
	
		$rs->MoveNext();
		
	}

	return $data;
	
}

//------------------------------------------------------------------------------------------------
/**
 * Données exportables
 */
 
function &getExportableArray( $data ){

	$exportableArray = array(
	
		Dictionnary::translate( "idsupplier" ) 						=> array(),
		Dictionnary::translate( "code_customhouse" ) 				=> array(),
		Dictionnary::translate( "idstate" ) 						=> array(),
		Dictionnary::translate( "gest_com_billing_supplier" ) 		=> array(),
		Dictionnary::translate( "gest_com_supplier_invoice_date" ) 	=> array(),
		Dictionnary::translate( "gest_com_idorder" ) 				=> array(),
		Dictionnary::translate( "reference" ) 						=> array(),
		Dictionnary::translate( "gest_com_ref_supplier" ) 			=> array(),
		Dictionnary::translate( "weight" ) 							=> array(),
		Dictionnary::translate( "quantity" ) 						=> array(),
		Dictionnary::translate( "sellingcost" ) 					=> array()

	);
	
	$lang = "_1";
	
	$i = 0;
	$keys = array_keys( $data );
	while( $i < count( $data[ $keys[ 0 ] ] ) ){
		
		$exportableArray[ Dictionnary::translate( "idsupplier" ) ][]						= $data[ "supplierName" ][ $i ];
		$exportableArray[ Dictionnary::translate( "code_customhouse" ) ][] 					= $data[ "code_customhouse" ][ $i ];
		$exportableArray[ Dictionnary::translate( "idstate" ) ][] 							= $data[ "supplierStateName" ][ $i ];
		$exportableArray[ Dictionnary::translate( "gest_com_billing_supplier" ) ][] 		= $data[ "billing_supplier" ][ $i ];
		$exportableArray[ Dictionnary::translate( "gest_com_supplier_invoice_date" ) ][] 	= Util::dateFormatEu( $data[ "Date" ][ $i ] );
		$exportableArray[ Dictionnary::translate( "gest_com_idorder" ) ][] 					= $data[ "idorder" ][ $i ];
		$exportableArray[ Dictionnary::translate( "reference" ) ][] 						= $data[ "reference" ][ $i ];
		$exportableArray[ Dictionnary::translate( "gest_com_ref_supplier" ) ][] 			= $data[ "ref_supplier" ][ $i ];
		$exportableArray[ Dictionnary::translate( "weight" ) ][] 							= Util::numberFormat( $data[ "weight" ][ $i ] );
		$exportableArray[ Dictionnary::translate( "quantity" ) ][] 							= $data[ "quantity" ][ $i ];
		$exportableArray[ Dictionnary::translate( "sellingcost" ) ][] 						= Util::numberFormat( $data[ "discount_price" ][ $i ] );
		
		$i++;
		
	}
	
	return $exportableArray;
	
}

//------------------------------------------------------------------------------------------------

?>