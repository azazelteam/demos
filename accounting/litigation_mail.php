<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Mails litiges clients
 */
	
//----------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/Litigation.php" );
include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );

//----------------------------------------------------------------------------
/* prérequis */

if( !isset( $_GET[ "mail" ] ) ){
	
	trigger_error( "paramètre absent", E_USER_ERROR );
	exit();
	
}

if( isset( $_REQUEST[ "idlitigation" ] ) && isset( $_REQUEST[ "typeLitigation" ] ) )
	$litigation = new Litigation(  $_REQUEST[ "idlitigation" ]  , $_REQUEST[ "typeLitigation" ] );
else{
	
	trigger_error( "paramètre absent", E_USER_ERROR );
	exit();
	
}

//----------------------------------------------------------------------------
/* modèle de courriel */

$mailStruct = ( object )array();

$mailStruct->attachments 	= array();
$mailStruct->recipients 	= array();

$editor 	= new MailTemplateEditor();

$editor->setLanguage( substr( User::getInstance()->getLang(), 1 ));

switch( $_REQUEST[ "mail" ] ){
	
	/* ouverture */
	
	case "creation" :
		switch( $litigation->getType() ){
			case Litigation::$TYPE_BILLING_BUYER:
			$editor->setTemplate( MailTemplateEditor::$LITIGATION );
			break;
			
			case Litigation::$TYPE_ORDER:
			$editor->setTemplate( MailTemplateEditor::$LITIGATION_ORDER );
			break;
		}
		
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( $litigation->getCustomer()->get( "idbuyer" ), $litigation->getCustomer()->getContact()->get( "idcontact" ) );
		$editor->setUseCommercialTags( $litigation->getSalesman()->get( "iduser" ) );
		$editor->setUseLitigationTags( $litigation->getIdLitigation() , $litigation->getType() );

		break;
	
	/* remboursement */
	
	case "repayment" :
	
		$editor->setTemplate( MailTemplateEditor::$REPAYMENT );
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( $litigation->getCustomer()->get( "idbuyer" ), $litigation->getCustomer()->getContact()->get( "idcontact" ) );
		$editor->setUseCommercialTags( $litigation->getSalesman()->get( "iduser" ) );
		$editor->setUseLitigationTags( $litigation->getIdLitigation() , $litigation->getType() );
		$editor->setUseRepaymentTags(  $_GET[ "idrepayment" ]  );

		break;
		
	/* accord de retour marchandise */
	
	case "return_to_sender" :
	
		$editor->setTemplate( MailTemplateEditor::$RETURN_TO_SENDER );
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( $litigation->getCustomer()->get( "idbuyer" ), $litigation->getCustomer()->getContact()->get( "idcontact" ) );
		$editor->setUseCommercialTags( $litigation->getSalesman()->get( "iduser" ) );
		$editor->setUseLitigationTags( $litigation->getIdLitigation() , $litigation->getType() );

		/* pièce jointe */
	
		switch( $litigation->getGoodsDeliveryLocation() ){
		
			case Litigation::$GOODS_DELIVERY_LOCATION_LOCAL :
			case Litigation::$GOODS_DELIVERY_LOCATION_BOTH :
			
				$mailStruct->attachments[] =  ( object )array(

					"label" 	=> "PDF Accord de retour",
					"filename" 	=> "accord_retour.pdf",
					"url" 		=> "$GLOBAL_START_URL/accounting/litigation.php?output_delivery_note&idorder_supplier=" . $litigation->getReturnToSender()->get( "idorder_supplier" ) . "&idrow=" . $litigation->getReturnToSender()->get( "idrow" ) . "&location=" . Litigation::$GOODS_DELIVERY_LOCATION_LOCAL,
					"mime_type" => "application/pdf"
					
				);
				
				break;
	
			case Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER :
	
				$mailStruct->attachments[] =  ( object )array(

					"label" 	=> "PDF Accord de retour",
					"filename" 	=> "accord_retour.pdf",
					"url" 		=> "$GLOBAL_START_URL/accounting/litigation.php?output_delivery_note&idorder_supplier=" . $litigation->getReturnToSender()->get( "idorder_supplier" ) . "&idrow=" . $litigation->getReturnToSender()->get( "idrow" ) . "&location=" . Litigation::$GOODS_DELIVERY_LOCATION_SUPPLIER,
					"mime_type" => "application/pdf"
					
				);

				break;

			default : trigger_error( "destination de la marchandise inconnue", E_USER_ERROR ); exit();
			
		}
		
		break;
	
	/* accusé de réception de la marchandise retournée */
	
	case "return_to_sender_confirmation" :
	
		$editor->setTemplate( MailTemplateEditor::$RETURN_TO_SENDER_CONFIRMATION );
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseBuyerTags( $litigation->getCustomer()->get( "idbuyer" ), $litigation->getCustomer()->getContact()->get( "idcontact" ) );
		$editor->setUseCommercialTags( $litigation->getSalesman()->get( "iduser" ) );
		$editor->setUseReturnToSenderTags( $litigation->getReturnToSender()->get( "idorder_supplier" ), $litigation->getReturnToSender()->get( "idrow" ) );
		$editor->setUseLitigationTags( $litigation->getIdLitigation() , $litigation->getType() );

		break;
		
			
	default :
	
		trigger_error( "bad parameter", E_USER_ERROR );
		exit();
			
}

$mailStruct->subject 			= isset( $_POST[ "subject" ] ) ? stripslashes( $_POST[ "subject" ] ) 	: $editor->unTagSubject();
$mailStruct->body 				= isset( $_POST[ "body" ] ) ? stripslashes( $_POST[ "body" ] ) 			: $editor->unTagBody();

$mailStruct->recipients[] = $litigation->getCustomer()->getContact()->get( "mail" );

if( DBUtil::getParameterAdmin( "cc_salesman" ) )
	$mailStruct->recipients[] = $litigation->getSalesman()->get( "email" );

$mailStruct->sender = ( object )array(

	"name" 	=> DBUtil::getParameterAdmin( "ad_name" ),
	"email" => DBUtil::getParameterAdmin( "ad_mail" )
	
);
		
//----------------------------------------------------------------------------

/**
 * affichage de la page
 */

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent">
	    <div class="topRight"></div>
	    <div class="topLeft"></div>
		<div class="content" style="margin-bottom:25px;">
			<div class="headTitle">
				<p class="title">N° Litige : <?php echo $litigation->getIdLitigation(); ?></p>
				<div class="rightContainer date"><?php echo humanReadableDatetime( $litigation->getCreationDate( "creation_date" ) ); ?></div>
	  			<div class="clear"></div>      	
			</div><!-- headTitle -->
	        <div class="subHeadTitle">
	            <div class="subHeadTitleElementRight">
	                <b>Commercial Affaire : </b>
	            	<?php echo htmlentities( DBUtil::getDBValue( "firstname", "user", "iduser", $litigation->getSalesman()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $litigation->getSalesman()->get( "iduser" ) ) ); ?>
	            </div><!-- subHeadTitleElementRight -->
	            <p class="subHeadTitleElementLeft" style="margin-top:5px;">
	            	<b>Commercial client : </b><?php echo DBUtil::getDBValue( "firstname", "user", "iduser", $litigation->getCustomer()->get( "iduser" ) ) . " " . DBUtil::getDBValue( "lastname", "user", "iduser", $litigation->getCustomer()->get( "iduser" ) ); ?>
	            </p>
	            <div class="clear" style="margin-top:5px;">
	            	<p>STATUT DU LITIGE : <?php echo $litigation->isEditable() ? "En cours" : "Validé"; ?></p>
				</div>
				<div class="clear" style="margin-top:3px; margin-bottom:30px; margin-bottom:30px;">
					<p>
					<?php
					switch ( $litigation->getType() ){
						case Litigation::$TYPE_BILLING_BUYER :
							?>
							N° FACTURE LITIGIEUSE : <a class="blueLink" href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $litigation->getIdInvoice() ?>" onclick="window.open( this.href ); return false;">
								<?php echo $litigation->getIdInvoice() ?>
							</a>
							<?php 
							break;
						
						case Litigation::$TYPE_ORDER :
							?>
							N° COMMANDE LITIGIEUSE : <a class="blueLink" href="<?php echo $GLOBAL_START_URL ?>/sales_force/com_admin_order.php?IdOrder=<?php echo $litigation->getIdOrder() ?>" onclick="window.open( this.href ); return false;">
								<?php echo $litigation->getIdOrder() ?>
							</a>
							<?php 
							break;
						default:
							echo "rien du tout";
							break;
					}
					?>	
					</p>
					<p style="margin-top:10px;">
	           			<a href="<?php echo $GLOBAL_START_URL; ?>/accounting/litigation.php?idlitigation=<?php echo $litigation->getIdLitigation(); ?>&typeLitigation=<?php echo $litigation->getType(); ?>" class="blueLink">Retour au litige
	           				<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/rightArrow.png" alt="Retour au litige" />
	           			</a>
	           		</p>
				</div>
	    	</div><!-- subHeadTitle -->
			<div class="subContent">
	        	<div class="subTitleContainer">
	                <p class="subTitle">Email d'accompagnement</p>
				</div><!-- subTitleContainer -->
				<?php

					if( isset( $_POST[ "send" ] ) ){ /* envoi du courriel */
						
						$ret = send( $litigation, $mailStruct );
						
						if( !$ret )
							displayForm( $litigation, $mailStruct );

					}
					else displayForm( $litigation, $mailStruct ); 
					
				?>
				<div class="clear"></div>
			</div><!-- subContent -->
		</div><!-- content -->
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div><!-- mainContent -->
	<div id="toolBoxes">
		<?php include( "$GLOBAL_START_PATH/templates/accounting/litigation/toolboxes.php" ); ?>
	</div>
</div><!-- globalMainContent -->
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------------

function displayForm( &$litigation, &$mailStruct ){

	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/WYSIWYGEditor.php" );
	
	?>
	<form id="LitigationForm" method="post" action="<?php echo $GLOBAL_START_URL . $_SERVER[ "REQUEST_URI" ]; ?>">
		<input type="hidden" name="send" value="1" />
		<input type="hidden" name="idlitigation" value="<?php echo $litigation->getIdLitigation(); ?>" />	
		<div style="margin-top:15px;">
		<?php
		
			/* pièces jointes */

			if( count( $mailStruct->attachments ) ){
			
				?>
				<p style="margin:10px 0px 5px 0px;">
					<b>Pièces jointes : </b>
					<?php

						$i = 0;
						while( $i < count( $mailStruct->attachments ) ){
							
							if( $i )
								echo " - ";
								
							?>
							<a href="<?php echo $mailStruct->attachments[ $i ]->url; ?>" onclick="window.open( this.href ); return false;">
								<img src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/pdf.gif" alt="" />
								<?php echo htmlentities( $mailStruct->attachments[ $i ]->label ); ?>
							</a>
							<?php
							
							$i++;
							
						}
				
					?>
					</p>
					<?php
					
				}

			?>
			<p style="font-weight:bold; margin:10px 0px 5px 0px;">Objet :</p>
			<p style="padding-right:10px;">
				<input type="text" class="textInput" value="<?php echo $mailStruct->subject ?>" name="subject" style="width:90%;" />
			</p>
			<p style="font-weight:bold; margin:10px 0px 5px 0px;">Message :</p>
			<?php WYSIWYGEditor::createEditor( "body", $mailStruct->body, "style=\"width:600px; height:500px;\"" ); ?>
		</div>
		<p style="text-align:center; margin:15px;">
			<input type="submit" class="blueButton" name="" value="Envoyer" />
		</p>
	</form>
	<?php
	
}

//-----------------------------------------------------------------------------------

/**
 * envoi par courriel
 */
 
function send( &$litigation, &$mailStruct ){

	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$mailer = new htmlMimeMail();
	
	$mailer->setHtml( $mailStruct->body );
	$mailer->setSubject( $mailStruct->subject );
	$mailer->setFrom( "\"" . $mailStruct->sender->name ."\" <" . $mailStruct->sender->email . ">" );
	
	/* attachments */
	
	$i = 0;
	while( $i < count( $mailStruct->attachments ) ){
		
		$data = file_get_contents( $mailStruct->attachments[ $i ]->url );

		$mailer->addAttachment( $data, $mailStruct->attachments[ $i ]->filename, $mailStruct->attachments[ $i ]->mime_type );
			
		$i++;
		
	}
	
	$ret = $mailer->send( $mailStruct->recipients );
	
	if( !$ret ){
	
		?>	
		<p class="msg" style="text-align:center;">Une erreur inattendue est survenue lors de l'envoi du courriel</p>
		<?php
		
	}
	else{
		
		?>
		<p style="text-align:center; margin-top:15px;">
			Le courriel a bien été envoyé aux destinataires suivants :
			<ul style="list-style-type:none; clear:left; margin-top:15px; text-align:center;">
			<?php
			
				$i = 0;
				while( $i < count( $mailStruct->recipients ) ){
					
					?>
					<li>
						<a href="mailto:<?php echo htmlentities( $mailStruct->recipients[ $i ] ); ?>" class="blueLink"><?php echo htmlentities( $mailStruct->recipients[ $i ] ); ?></a>
					</li>
					<?php
					
					$i++;
					
				}
				
			?>
			</ul>
		</p>
		<p style="text-align:center; margin-top:15px;">
			<input type="button" class="blueButton" name="" value="Retour au litige" onclick="document.location='<?php echo $GLOBAL_START_URL; ?>/accounting/litigation.php?idlitigation=<?php echo $litigation->getIdLitigation(); ?>&typeLitigation=<?php echo $litigation->getType(); ?>';" />
		</p>
		<?php
			
	}
	
	return $ret;
		
}

//-----------------------------------------------------------------------------------

?>