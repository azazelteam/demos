<?php
 /**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Sélection des chèques pour le dépot bancaire
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );

if( !isset($_POST['regulationsToUpdt']) ){
	// On sélectionne les chèques qui n'ont pas été déposés, en gros les règlements qui ne sont pas dans deposit
	// et qui ont le mode de paiement par chèque et qui ne sont pas au factor
	
	$regulationsToDeposit = DBUtil::query("SELECT rb.idregulations_buyer, rb.amount, rb.payment_date, rb.piece_number, rb.comment, rb.idbuyer, b.company, bank.bank_initials
											FROM buyer b, regulations_buyer rb 
											LEFT JOIN bank ON bank.idbank = rb.idbank
											WHERE rb.iddeposit = 0
											AND rb.idpayment = 2
											AND rb.factor = 0
											AND b.idbuyer = rb.idbuyer
											ORDER BY amount ASC");
											
	$regs_to_show = "";

}else{
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$regs_to_show = implode( "," , $_POST['regulationsToUpdt'] );
	
	$regulationsToDeposit = DBUtil::query("SELECT rb.idregulations_buyer, rb.amount, rb.payment_date, rb.piece_number, rb.comment, rb.idbuyer, b.company, bank.bank_initials
											FROM buyer b, regulations_buyer rb 
											LEFT JOIN bank ON bank.idbank = rb.idbank
											WHERE rb.iddeposit = 0
											AND rb.idpayment = 2
											AND rb.factor = 0
											AND rb.idregulations_buyer IN( $regs_to_show )
											AND b.idbuyer = rb.idbuyer
											ORDER BY amount ASC");
	
	// calcul de l'id à mettre									
	$idDeposit = DBUtil::query("SELECT ifnull(MAX(iddeposit),0) + 1 as newDeposit FROM regulations_buyer LIMIT 1")->fields('newDeposit');
	$iduserDeposit = User::getInstance()->getId();
	// update
	DBUtil::query("UPDATE regulations_buyer SET iddeposit = $idDeposit, date_deposit = NOW(), iduser_deposit = $iduserDeposit WHERE idregulations_buyer IN ( $regs_to_show )");
	
}

$totalDeposit = 0;

if( !isset($_POST['regulationsToUpdt']) ){
	$Title = ucfirst("remise de chèques");
	
	include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	?>
	
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />
	
	<script type="text/javascript">
		function resetTotalAmount(){
			
			$("#selectionAmount").val( 0 );
			$("#selectionAmountDiv").html( "0.00 ¤" );
			
			$(".amountRow").each(function(){
				if($(this).is(".activeAmountRow"))
					$(this).removeClass("activeAmountRow");
			});
			
		}
	</script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() {
			
			$(".amountRow").click(function(){
				
				var value = $(this).find( ".myAmount" ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' );
				var totalAmount = $("#selectionAmount").val();
				
				if($(this).is(".activeAmountRow")){
					
					$(this).removeClass("activeAmountRow");
					
					totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
					
				}
				else{
					
					$(this).addClass("activeAmountRow");
					
					totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
					
				}
				
				$("#selectionAmount").val(totalAmount);
				$("#selectionAmountDiv").html(totalAmount + " ¤");
				
			});
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function checkAll( checked ){
			if( checked ){
				$("#tableDesposit").find(':checkbox').each( function(){ if( $(this).attr('id') != 'checkAllCB' ){ $(this).attr('checked',true) } } );
			}else{
				$("#tableDesposit").find(':checkbox').each( function(){ if( $(this).attr('id') != 'checkAllCB' ){ $(this).attr('checked',false) } } );
			}
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function generateDeposit(){
			var checkCount = false;
			$(".resultTable").find(':checkbox').each( function(){ if ($(this).attr('checked') == true && $(this).attr('id') != 'checkAll' ){ checkCount = true; } });
			
			if(!checkCount){
	   			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
				$.blockUI.defaults.css.fontSize = '12px';
				$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
				$.blockUI.defaults.growlCSS.color = '#FFFFFF';
			    $.growlUI( '', "Veuillez sélectionner des chèques à déposer" );
			    
			    return false;
			}

			var myoptions = {
				success: function( response ){
					if( response.length == 0 ){
		       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
						$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
						$.blockUI.defaults.growlCSS.color = '#FFFFFF';
					    $.growlUI( '', " Impossible d'effectuer le traitement les modifications <br />" + response );
		       		}else{
		       			$("#centerMaxPrint").html( response );
		       			var reg = new RegExp("[,]+", "g");
		       			var rowsToKill = $("#regs_to_show").val().split( reg );

		       			for( i=0 ; i < rowsToKill.length ; i++ ){
		       				$( "#regulationRow_"+rowsToKill[i] ).remove();
    					}
    					
    					window.print();
    					
			   			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
						$.blockUI.defaults.css.fontSize = '12px';
						$.blockUI.defaults.growlCSS.backgroundColor = '#000';
						$.blockUI.defaults.growlCSS.color = '#fff';
					    $.growlUI( '', "Remise effectuée avec succès" );
					}
			    }
			};

			$('#deposit').ajaxSubmit(myoptions);
		}
		
		/* ----------------------------------------------------------------------------------- */
				
	/* ]]> */
	</script>
	<style type="text/css">
	<!--
		
		.fixme {
			/* Netscape 4, IE 4.x-5.0/Win and other lesser browsers will use this */
		  position: absolute;
		  right: 10px;
		  top: 135px;
		}
		body > div#global > div#globalMainContent > div.fixme {
		  /* used by Opera 5+, Netscape6+/Mozilla, Konqueror, Safari, OmniWeb 4.5+, iCab, ICEbrowser */
		  position: fixed;
		}
		
		tr.activeAmountRow td { background-color:#E0E0E0; }
		
	-->
	</style>
	<!--[if gte IE 5.5]>
		<![if lt IE 7]>
			<style type="text/css">
			
				div.fixme {
				  /* IE5.5+/Win - this is more specific than the IE 5.0 version */
				  right: expression( ( 10 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
				  top: expression( ( 135 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
				}
				
			</style>
		<![endif]>
	<![endif]-->
<?php } ?>
	
<div class="centerMax <?php echo !isset($_POST['regulationsToUpdt']) ? "noprint" : "" ?>">
	
	<?if(!isset($_POST['regulationsToUpdt'])){?>

	    <div class="contentDyn">
			<h1 class="titleSearch noprint">
				<span class="textTitle">Remise de chèques : <?php echo $regulationsToDeposit->RecordCount() ?> chèques non déposés</span>
				<span class="textDate">
					<span class="noprint" style="font-size:14px;font-weight:bold;float:right;margin-top:5px;margin-right:5px;">
						<?php echo date("d/m/Y")?>
					</span>
					<div class="spacer"></div>
				</span>
				<div class="spacer"></div>
			</h1>
			
			<div id="container">
				<div class="blocResult mainContent" style="margin:0;">
					<div class="content">
						<form id="deposit" name="deposit" method="post" action="<?php echo $GLOBAL_START_URL?>/accounting/check_deposit.php">
							<table class="dataTable resultTable" id="tableDesposit">
								<thead>
								<tr>
									<th style="width:180px">Documents</th>
									<th style="width:80px;">N°Client</th>
									<th>Nom client</th>
									<th style="width:100px">Montant<br />enregistré</th>
									<th style="width:155px">Date<br />d'enregistrement</th>
									<th style="width:80px;">N° chèque</th>
									<th style="width:70px;">Banque</th>
									<th style="width:30px;">Tout<br /><input type="checkbox" name="checkAllCB" id="checkAllCB" onclick="checkAll( this.checked );" /></th>
								</tr>
								</thead>
								<tbody>
								<?while(!$regulationsToDeposit->EOF()){?>
									<?
									$idregulation 	= $regulationsToDeposit->fields('idregulations_buyer');
									$amount			= $regulationsToDeposit->fields('amount');
									$date_payment	= Util::dateFormatEu($regulationsToDeposit->fields('payment_date'));
									$comment		= htmlentities($regulationsToDeposit->fields('comment'),ENT_COMPAT,'UTF-8') ;
									$piece_number	= $regulationsToDeposit->fields('piece_number');
									$bankInitials	= $regulationsToDeposit->fields('bank_initials');
									$idbuyer		= $regulationsToDeposit->fields('idbuyer');
									$buyerName		= $regulationsToDeposit->fields('company') == "" ? DBUtil::query("SELECT CONCAT(lastname,' ',firstname) as buyerName FROM contact WHERE idbuyer = '$idbuyer' AND idcontact = 0 LIMIT 1")->fields('buyerName'): $regulationsToDeposit->fields('company') ;
									
									$billings 		= DBUtil::query("SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation");
									$ordersDP		= DBUtil::query("SELECT idorder 
																	FROM `order` 
																	WHERE down_payment_idregulation = $idregulation 
																	AND `order`.down_payment_idregulation NOT IN( SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation )");
									$ordersCP		= DBUtil::query("SELECT idorder 
																	FROM `order` 
																	WHERE cash_payment_idregulation = $idregulation
																	AND `order`.cash_payment_idregulation NOT IN( SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation )");
									$billings 		= DBUtil::query("SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation");
									$estimatesDP	= DBUtil::query("SELECT idestimate 
																	FROM `estimate` 
																	WHERE down_payment_idregulation = $idregulation 
																	AND `estimate`.down_payment_idregulation NOT IN( SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation )");
									$estimatesCP	= DBUtil::query("SELECT idestimate 
																	FROM `estimate` 
																	WHERE cash_payment_idregulation = $idregulation
																	AND `estimate`.cash_payment_idregulation NOT IN( SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation )");
									
									
									$totalDeposit	+= $amount;
									?>
									<tr id="regulationRow_<?php echo $idregulation ?>" class="amountRow">
										<td class="lefterCol">
											<?
											$bils = array();
											$ords = array();
											$esti = array();
											
											if($billings->RecordCount()){
												?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Factures : </span><?
												while(!$billings->EOF()){
													array_push( $bils , $billings->fields('idbilling_buyer') );					
													
													$billings->MoveNext();
												}
												
												?><?php echo implode( ' - ' , $bils ) ?></p><?
											}
											
											if($ordersDP->RecordCount() || $ordersCP->RecordCount()){
												?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Commandes : </span><?
												if($ordersDP->RecordCount()){
													while(!$ordersDP->EOF()){
														array_push( $ords , $ordersDP->fields('idorder') );					
														
														$ordersDP->MoveNext();
													}
												}
												if($ordersCP->RecordCount()){
													while(!$ordersCP->EOF()){
														array_push( $ords , $ordersCP->fields('idorder') );					
														
														$ordersCP->MoveNext();
													}
												}
												
												?><?php echo implode( ' - ' , $ords ) ?></p><?
											}
											
											if($estimatesDP->RecordCount() || $estimatesCP->RecordCount()){
												?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Devis : </span><?
												if($estimatesDP->RecordCount()){
													while(!$estimatesDP->EOF()){
														array_push( $esti , $estimatesDP->fields('idestimate') );					
														
														$estimatesDP->MoveNext();
													}
												}
												if($estimatesCP->RecordCount()){
													while(!$estimatesCP->EOF()){
														array_push( $esti , $estimatesCP->fields('idestimate') );					
														
														$estimatesCP->MoveNext();
													}
												}
												
												?><?php echo implode( ' - ' , $esti ) ?></p><?
											}
											
											if( !count($ords) && !count( $bils ) && !count( $esti )){
												?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Paiement non rattaché</span></p><?									
											}
											?>
										</td>
										<td><?php echo $idbuyer ?></td>
										<td><?php echo $buyerName ?></td>
										<td class="myAmount"><?php echo Util::priceFormat($amount) ?></td>
										<td><?php echo $date_payment ?></td>
										<td><?php echo $piece_number ?></td>
										<td><?php echo $bankInitials ?></td>
										<td class="righterCol checkBox"><input type="checkbox" name="regulationsToUpdt[]" value="<?php echo $idregulation ?>" /></td>
									</tr>
										
									<?
									$regulationsToDeposit->MoveNext();
								}?>
								</tbody>
								<tfoot>
									<tr>
										<th style="border:none;" colspan="3"></th>
										<th style="text-align:center;"><?=Util::priceFormat($totalDeposit)?></th>
										<th style="border:none;text-align:right;margin:0;padding:0;" colspan="3"></th>
									</tr>
								</tfoot>		
							</table>
						</form>
						<div style="text-align:right;width:100%;margin-top:10px;margin-bottom:10px;">
							<input type="button" value="Créer une remise" onclick="generateDeposit();" name="createDeposit" class="blueButton" />
						</div>
					</div>
				</div>
			</div><!-- container -->
		</div><!-- contentDyn -->
		<div class="rightTools noprint">
			<div class="spacer"></div>
			<div class="toolBox">
			
				<h2>
					<img alt="star" src="<?=$GLOBAL_START_URL?>/images/back_office/layout/icons-menu-calculette.png"/>
					&nbsp;Montant sélection
				</h2>
				<div class="spacer"></div>
				<input type="hidden" id="selectionAmount" value="0" />
				<div class="blocSearch">
					<div id="selectionAmountDiv" style="font-size:14px; font-weight:bold; padding-top:5px; text-align:center;">0.00 ¤</div>
					<input type="button" value="Effacer" onclick="resetTotalAmount();" class="blueButton noprint" style="display:block; margin:auto; margin-top:3px;" />
				</div>
			</div>	
		</div>
	
	<?php } else{?>
		<input type="hidden" name="regs_to_show" id="regs_to_show" value="<?php echo $regs_to_show ?>" />
	    <div class="contentDyn">
			<h1 class="titleSearch" style="width:950px;">
				<span class="textTitle">Remise de chèques N°<?php echo $idDeposit ?> : <?php echo $regulationsToDeposit->RecordCount() ?> chèques</span>
				<div class="spacer"></div>
				<div style="width:50%;float:left;font-size:14px;margin-top:10px;margin-left:5px;">
            		Bénéficiaire : <?=DBUtil::getParameterAdmin("ad_name") ;?><br />
            		Le <?=date('d/m/Y')?><br />
            		RIB : <?=DBUtil::getParameterAdmin("ad_bank1") ;?>&nbsp;<?=DBUtil::getParameterAdmin("ad_bank2") ;?>&nbsp;<?=DBUtil::getParameterAdmin("ad_bank3") ;?>&nbsp;<?=DBUtil::getParameterAdmin("ad_bank4") ;?>
				</div>
				<div style="float:right;margin-right:150px;font-size:14px;margin-top:10px;">
					Signature :
				</div>
				<div class="spacer"></div>
			</h1>
			
			<div id="container">
				<div class="blocResult mainContent" style="margin:0;">
					<div class="content">
						<table class="dataTable resultTable">
							<thead>
							<tr>
								<th style="width:180px">Documents</th>
								<th style="width:80px;">N°Client</th>
								<th>Nom client</th>
								<th style="width:100px">Montant<br />enregistré</th>
								<th style="width:155px">Date<br />d'enregistrement</th>
								<th style="width:80px;">N° chèque</th>
								<th style="width:70px;">Banque</th>
							</tr>
							</thead>
							<tbody>
							<?while(!$regulationsToDeposit->EOF()){?>
								<?
								$idregulation 	= $regulationsToDeposit->fields('idregulations_buyer');
								$amount			= $regulationsToDeposit->fields('amount');
								$date_payment	= Util::dateFormatEu($regulationsToDeposit->fields('payment_date'));
								$comment		= htmlentities($regulationsToDeposit->fields('comment'),ENT_COMPAT,'UTF-8')	;
								$piece_number	= $regulationsToDeposit->fields('piece_number');				
								$bankInitials	= $regulationsToDeposit->fields('bank_initials');
								$idbuyer		= $regulationsToDeposit->fields('idbuyer');
								$buyerName		= $regulationsToDeposit->fields('company') == "" ? DBUtil::query("SELECT CONCAT(lastname,' ',firstname) as buyerName FROM contact WHERE idbuyer = $idbuyer AND idcontact = 0 LIMIT 1")->fields('buyerName'): $regulationsToDeposit->fields('company') ;

								$billings 		= DBUtil::query("SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation");
								$ordersDP		= DBUtil::query("SELECT idorder 
																FROM `order` 
																WHERE down_payment_idregulation = $idregulation 
																AND `order`.down_payment_idregulation NOT IN( SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation )");
								$ordersCP		= DBUtil::query("SELECT idorder 
																FROM `order` 
																WHERE cash_payment_idregulation = $idregulation
																AND `order`.cash_payment_idregulation NOT IN( SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idregulations_buyer = $idregulation )");
								
								$totalDeposit	+= $amount;
								?>
								<tr>
									<td class="lefterCol">
										<?
										$bils = array();
										$ords = array();
										
										if($billings->RecordCount()){
											?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Factures : </span><?
											while(!$billings->EOF()){
												array_push( $bils , $billings->fields('idbilling_buyer') );					
												
												$billings->MoveNext();
											}
											
											?><?php echo implode( ' - ' , $bils ) ?></p><?
										}
										
										if($ordersDP->RecordCount() || $ordersCP->RecordCount()){
											?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Commandes : </span><?
											if($ordersDP->RecordCount()){
												while(!$ordersDP->EOF()){
													array_push( $ords , $ordersDP->fields('idorder') );					
													
													$ordersDP->MoveNext();
												}
											}
											if($ordersCP->RecordCount()){
												while(!$ordersCP->EOF()){
													array_push( $ords , $ordersCP->fields('idorder') );					
													
													$ordersCP->MoveNext();
												}
											}
											
											?><?php echo implode( ' - ' , $ords ) ?></p><?
										}
										
										if( !count($ords) && !count( $bils ) ){
											?><p style="text-align:left;"><span style="font-weight:bold;text-align:left;">Paiement non rattaché</span></p><?									
										}
										?>
									</td>
									<td><?php echo $idbuyer ?></td>
									<td><?php echo $buyerName ?></td>
									<td class="myAmount"><?php echo Util::priceFormat($amount) ?></td>
									<td><?php echo $date_payment ?></td>
									<td><?php echo $piece_number ?></td>
									<td class="righterCol"><?php echo $bankInitials ?></td>
								</tr>
									
								<?
								$regulationsToDeposit->MoveNext();
							}?>
								<tr>
									<th style="border:none;" colspan="3"></th>
									<th style="text-align:center;"><?=Util::priceFormat($totalDeposit)?></th>
									<th style="border:none;" colspan="3"></th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div><!-- container -->
		</div><!-- contentDyn -->
	<?php } ?>
</div><!-- centerMax -->

<div id="centerMaxPrint" class="noshow"></div>

<?
if(!isset($_POST['regulationsToUpdt'])){
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
}
?>
