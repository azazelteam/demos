<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Echéanciers règlements fournisseurs
 */

//------------------------------------------------------------------------------------------------
/* enregistrement des factures sélectionnées */
if(isset($_GET['UpdateInvoices']) && isset($_GET['invoices']) && strlen($_GET['invoices']) > 0){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	include_once( dirname( __FILE__ ) . "/../config/init.php" );
	include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
	
	
	// on récapitule les factures à mettre à jour
	$billingsSupplier = explode( ',' , $_GET['invoices'] );	
		
	for( $i = 0 ; $i < count($billingsSupplier) ; $i++ ){
		// on créé le règlement par rapport au solde
		// si solde == 0 c'est que la facture est payée
		$billingId = $billingsSupplier[$i];
		$idsupplier = DBUtil::query("SELECT idsupplier FROM billing_supplier WHERE billing_supplier = '$billingId'")->fields('idsupplier');
		
		$solde = SupplierInvoice::getBalanceOutstanding( $billingId , $idsupplier );
		
		$supplierInvoice = new SupplierInvoice($billingId , $idsupplier);
		
		if($solde){
			$rebate 		= 0 ;
			$login			= 'régulAuto';		
			$iduser			= User::getInstance()->getId();
			$comment		= 'Régul auto';
			$payment_date	= $supplierInvoice->get('deliv_payment');
			$deliv_payment	= $supplierInvoice->get('deliv_payment');
			$idpayment		= $supplierInvoice->get('idpayment');
			
			$id = DBUtil::query( "SELECT MAX(idregulations_supplier)+1 as newId FROM regulations_supplier" )->fields('newId');
			
		 	$query = "
			INSERT INTO `regulations_supplier` ( 
				idregulations_supplier,
				idsupplier, 
				iduser,
				comment,
				payment_date,
				deliv_payment,
				idpayment,
				amount,
				accept,
				DateHeure
			) VALUES ( 
				'$id',
				'$idsupplier',
				'$iduser',
				'$comment',
				'$payment_date',
				'$deliv_payment',
				$idpayment,
				$solde,
				1,
				NOW()
			)";
					
			$ret1 = DBUtil::query( $query );
					
			$query = "INSERT INTO billing_regulations_supplier (
							billing_supplier,
							idregulations_supplier,
							idsupplier,
							amount,
							rebate_amount,
							username,
							lastupdate
					 ) VALUES (
							'$billingId',
							$id,
							$idsupplier,
							'$solde',
							'$rebate',
							'$login',
							NOW()
					 )";
			
			$ret = DBUtil::query( $query );
		}
		
		DBUtil::query("UPDATE billing_supplier SET status='Paid' WHERE billing_supplier = '$billingId'");
	}
	
	exit();
	
}



//------------------------------------------------------------------------------------------------
/* recherche retards de paiement - ajax */

if( isset( $_REQUEST[ "search" ] ) ){

	include_once( dirname( __FILE__ ) . "/../config/init.php" );
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$idsupplier = isset( $_REQUEST[ "idsupplier" ] ) ? intval( $_REQUEST[ "idsupplier" ] ) : false;

	$regs = array();

	if( isset( $_REQUEST[ "deliv_payment_start" ] ) && preg_match( "/^([0-9]{2}).([0-9]{2}).([0-9]{4})\$/", $_REQUEST[ "deliv_payment_start" ], $regs ) !== false )
		$end = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
	else $end = false;

	if( isset( $_REQUEST[ "deliv_payment_end" ] ) && preg_match( "/^([0-9]{2}).([0-9]{2}).([0-9]{4})\$/", $_REQUEST[ "deliv_payment_end" ], $regs ) !== false )
		$start = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
	else $start 	= date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) - 1, date( "Y" ) ) );
	
	listDelayedPayments( $idsupplier, $start, $end );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );

//------------------------------------------------------------------------------------------------
//export

if( isset($_POST[ "export" ]) ){
	$request = "";
	if( isset($_POST[ "request" ]) )
		$request = base64_decode( $_POST[ "request" ] );
	
	if( isset($_POST[ "fields" ]) && count($_POST[ "fields" ]) == 0 )
		exit();
	
	$selectedFields = $_POST[ "fields" ];
		
	$exportableArray =& getExportableArray( $request , $selectedFields );
	
	include_once( "$GLOBAL_START_PATH/objects/HTML_XLS_export.php" );
	
	$pageTitle = "";
	if(isset($_POST['pageTitle']))
		$pageTitle = $_POST['pageTitle'];
		
	$export = new HTML_XLS( $exportableArray , "Echeancier_fournisseur_" . date( "Ymd" ) , $pageTitle );
	$export->output();
	
	//exportArray( $exportableArray );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------

$Title = ucfirst("échéancier fournisseur");

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();
$db =& DBUtil::getConnection();

$totalInvoiceCount = 0;
$superTotalHT = 0;
$superTotalNetTTC = 0;

$allInvoices = getInvoiceByIdPayment();

//------------------------------------------------------------------------------------------------

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<script type="text/javascript">
	function resetTotalAmount(){
		
		$("#selectionAmount").val( 0 );
		$("#selectionAmountDiv").html( "0.00 ¤" );
		
		$(".amountRow").each(function(){
			if($(this).is(".activeAmountRow"))
				$(this).removeClass("activeAmountRow");
		});
		
	}
</script>
<script type="text/javascript">
/* <![CDATA[ */
	
	/* ----------------------------------------------------------------------------------- */
	
	$(document).ready(function() {
		
		$(".amountRow").click(function(){
			
			var value = $(this).attr("id").substring(13);
			var totalAmount = $("#selectionAmount").val();
			
			if($(this).is(".activeAmountRow")){
				
				$(this).removeClass("activeAmountRow");
				
				totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
				
			}
			else{
				
				$(this).addClass("activeAmountRow");
				
				totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
				
			}
			
			$("#selectionAmount").val(totalAmount);
			$("#selectionAmountDiv").html(totalAmount + " ¤");
			
		});
		
	});
	
	/* ----------------------------------------------------------------------------------- */
	
/* ]]> */
</script>
<style type="text/css">
<!--
	
	.fixme {
		/* Netscape 4, IE 4.x-5.0/Win and other lesser browsers will use this */
	  position: absolute;
	  right: 10px;
	  top: 135px;
	}
	body > div#global > div#globalMainContent > div.fixme {
	  /* used by Opera 5+, Netscape6+/Mozilla, Konqueror, Safari, OmniWeb 4.5+, iCab, ICEbrowser */
	  position: fixed;
	}
	
	tr.activeAmountRow td { background-color:#E0E0E0; }
	
-->
</style>
<!--[if gte IE 5.5]>
	<![if lt IE 7]>
		<style type="text/css">
		
			div.fixme {
			  /* IE5.5+/Win - this is more specific than the IE 5.0 version */
			  right: expression( ( 10 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
			  top: expression( ( 135 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
			}
			
		</style>
	<![endif]>
<![endif]-->
<style type="text/css">
	.none{background-color:#F4F4F4;}
</style>

<form action="<?php echo $ScriptName ?>" method="post" name="adm_fact" id="adm_fact">
<div class="centerMax">
	<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    <div class="contentDyn">
		<h1 class="titleSearch noprint">
			<span class="textTitle"><span style="text-transform:capitalize;">échéancier</span> fournisseur</span>
			<span class="textDate">
				<span class="noprint" style="font-size:14px;font-weight:bold;float:right;">
					<a href="#" onclick="showExportDialog('<?php echo base64_encode( $exportURLBis ); ?>');return false;">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
					<a href="#" onclick="window.print();return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/print.gif" alt="Imprimer la sélection" /></a>&nbsp;
				</span>
				<div class="spacer"></div>
			</span>
			<div class="spacer"></div>
		</h1>
		
					
	
		<div id="container">
			<ul class="menu noprint">
				<li class="item">
					<a href="#DelayedPayments" onclick="fillTable_perso();"><span>Antérieur à <?php echo getMonthYearName( date( "Y-m-d" ) ); ?></span></a>
				</li>
				<?php foreach($allInvoices as $monthInvoice => $values){ ?>					
				<li class="item">
					<?php
					$monthInvoice2 = getMonthYearName($monthInvoice);
					if($monthInvoice2 != "00 0000"){?>
								<a onclick="fillTable_<?=str_replace("-","_",$monthInvoice)?>();" id="facture" href="#month_<?=str_replace(" ","_",$monthInvoice)?>"><span><?php echo ucfirst($monthInvoice2) ?></span></a>
					<?php 
					}else{?>
								<a onclick="fillTable_<?=str_replace("-","_",$monthInvoice)?>();" id="facture" href="#month_<?=str_replace(" ","_",$monthInvoice)?>"><span>Echéance non renseignée</span></a>
					<?php 
					}?>
				</li>
    			<?php } ?>
			</ul>
			
			<div class="blocResult mainContent" style="margin:0;"><div class="content">
				<?php foreach($allInvoices as $monthInvoice => $values){
				 	
					$totalInvoiceCountMonth = 0;
					$superTotalHTMonth = 0;
					$superTotalNetTTCMonth = 0;
							
				 	$suppliers = $values;
				 	$monthInvoice2 = getMonthYearName($monthInvoice); ?>   	
					
					
					<?php if( sizeof( $allInvoices ) ){ ?>
					<!-- Début des boucles -->
					<div class="subContent" id="month_<?=str_replace(" ","_",$monthInvoice)?>">
					<?php
							                	
					$exportURLBis = "";
					$betweenMin = $monthInvoice;
					$betweenMax = substr($monthInvoice,0,7).'-31';
					if($monthInvoice2 != "00 0000")
						$exportURLBis .= " AND billing_supplier.deliv_payment BETWEEN '$betweenMin' AND '$betweenMax' ";
					else $exportURLBis .= " AND billing_supplier.deliv_payment = '0000-00-00' ";?>
				
					
										
					<?php foreach($suppliers as $supplier => $invoices){
						$supplier_name = DBUtil::query("SELECT name FROM supplier WHERE idsupplier = $supplier")->fields('name'); ?>
						
			        	<span style="font-size:14px;font-weight:bold;float:left;">
			        		<?php echo $supplier_name ?>
			        	</span>
			        	<div class="spacer"></div>
						<table class="dataTable resultTable tablesorter" style="">
			            	<thead>
			                    <tr>
			                        <th style="width:16%;">Facture n°</th>
			                        <th style="width:18%;">Date de création</th>
			                        <th style="width:18%;">Date d'échéance</th>
			                        <th style="width:16%;">Mode de règlement</th>
			                        <th style="width:16%;">Total facturé HT</th>
			                        <th style="width:16%;">Net à payer</th>
			                    </tr>
			                </thead>
					        <tbody>
					    	<?php
					
							$tot=0;
							$total_net_sum = 0.0;
							foreach( $invoices as $billing_supplier =>$invoice ){
								
								$totalInvoiceCount ++;
								$totalInvoiceCountMonth ++;
								
								$billing_supplier 	= $billing_supplier;
								$tot += $invoice[ "total_amount_ht" ];
								$superTotalHT += $invoice[ "total_amount_ht" ];
								$superTotalHTMonth += $invoice[ "total_amount_ht" ];
								
								$total_net_sum += $invoice[ "net_sum" ];
								$superTotalNetTTC += $invoice[ "net_sum" ];
								$superTotalNetTTCMonth += $invoice[ "net_sum" ];
								
								list( $date, $time ) = explode( " ", $invoice[ "Date" ] ); ?>
					                        <tr class="amountRow" id="rowForAmount_<?php echo $invoice[ "net_sum" ] ?>">
					                            <td class="lefterCol"><?php echo $billing_supplier ?></td>
					                            <td><?php echo usDate2eu( $date ) ?></td>
					                            <td><?php echo usDate2eu( $invoice[ "deliv_payment" ] ) ?></td>
					                            <td><?php echo $invoice[ "payment_mode" ] ?></td>
					                            <td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $invoice[ "total_amount_ht" ] ) ?></td>
					                            <td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $invoice[ "net_sum" ] ) ?></td>
					                        </tr>
								<?php
								                            
							} ?>
							</tbody>
			                <tfoot>
								<tr>
									<th colspan="4" class="none" style="border-style:none;"></th>
									<th class="totalAmount"><?php echo Util::priceFormat($tot) ?></th>
									<th class="totalAmount"><?php echo Util::priceFormat( $total_net_sum ) ?></th>
								</tr>
			                </tfoot>
			            </table>
					<?php } ?>
				        	
				    	<div class="spacer"></div>
				    	<script type="text/javascript">
				    		function fillTable_<?=str_replace("-","_",$monthInvoice)?>(){
				    			$("#recapTotalInvoices").html('<?=$totalInvoiceCountMonth?>');
				    			$("#recapTotalHT").html('<?=Util::priceFormat($superTotalHTMonth)?>');
				    			$("#recapTotalTTC").html('<?=Util::priceFormat($superTotalNetTTCMonth)?>');
				    		}
				    	</script>
					</div> <!-- subContent -->
					<?php } ?>
				
			<?php } ?>
					
					
				<div class="subContent" id="DelayedPayments">
				<?php 
				
					$start 	= date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), 0 , date( "Y" ) ) );
					$end 	= false;
					
					displayDelayedPaymentsForm( $start, $end );
					
					?>
					<div id="DelayedPaymentList"><?php listDelayedPayments( false, $start, $end ); ?></div>
				</div>
					
					
			</div></div>
		</div> <!-- container -->
	
	
    </div> <!-- contentDyn -->
	<div class="rightTools noprint">
		<h2>
			<img alt="star" src="<?=$GLOBAL_START_URL?>/images/back_office/layout/icons-menu-chiffre.png"/>
			&nbsp;Quelques chiffres...
		</h2>
		<div class="spacer"></div>
		<div class="blocSearch" >
			<div style="margin:5px;">
				<span style="font-weight:bold;">Total dû aux fournisseurs</span>
				<table class="dataTable resultTable" style="margin:5px 0;">
	                <tr>
	                	<th class="lefterCol">Nombre</th>
	                	<td class="righterCol"><?php echo $totalInvoiceCount ?></td>
	                </tr>
	                <tr>
	                    <th class="lefterCol">Tot. facturé HT</th>
	                    <td class="righterCol"><?php echo Util::priceFormat($superTotalHT) ?></td>
	                </tr>
	                <tr>
	                    <th class="lefterCol">Tot. à payer TTC</th>
	                    <td class="righterCol"><?php echo Util::priceFormat($superTotalNetTTC) ?></td>
	                </tr>
	            </table>
				<div class="spacer"></div><br/>

				<span style="font-weight:bold;">Total période sélectionnée</span>
				<table class="dataTable resultTable" style="margin:5px 0;">
	                <tr>
	                	<th class="lefterCol">Nombre</th>
	                	<td class="righterCol" id="recapTotalInvoices"></td>
	                </tr>
	                <tr>
	                    <th class="lefterCol">Tot. facturé HT</th>
	                    <td class="righterCol" id="recapTotalHT"></td>
	                </tr>
	                <tr>
	                    <th class="lefterCol">Tot. à payer TTC</th>
	                    <td class="righterCol" id="recapTotalTTC"></td>
	                </tr>
	            </table>
			</div>
		</div>
		<div class="spacer"></div>
		<div class="toolBox">
		
			<h2>
				<img alt="star" src="<?=$GLOBAL_START_URL?>/images/back_office/layout/icons-menu-calculette.png"/>
				&nbsp;Montant sélection
			</h2>
			<div class="spacer"></div>
			<input type="hidden" id="selectionAmount" value="0" />
			<div class="blocSearch">
				<div id="selectionAmountDiv" style="font-size:14px; font-weight:bold; padding-top:5px; text-align:center;">0.00 ¤</div>
				<input type="button" value="Effacer" onclick="resetTotalAmount();" class="blueButton noprint" style="display:block; margin:auto; margin-top:3px;" />
			</div>
		</div>	
			
	</div>
	<div class="spacer"></div>
</div><!-- centerMax -->
<script type="text/javascript">
	$(document).ready(function() {
		
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
			fillTable_perso();
			
	});

	$(function() {
		$(".tablesorter").tablesorter( {sortList:[[5,0]]} );
	}); 
	
	function showExportDialog( base64query ){
	
		$( "#ExportDialog" ).dialog({
        	
        	open: function(event, ui) { $( '#request' ).val( base64query ); },
			title: "Export de données",
			width:  500,
			modal: true,
			close: function(event, ui) { $("#ExportDialog").dialog( 'destroy' ); }
		
		});
		
	}
</script>
<?php 

	if( !$totalInvoiceCount ){
		
		?>
		<p style="text-align:center; margin-top:20px;" class="msg"><?php echo Dictionnary::translate( "gest_com_no_invoice_found" ) ?></p>
		<?php 
		
	}
	else{ 
		
		?>
		<input type="hidden" name="search" value="1" />
		<?php 
		
	} 
	
?>
</form>
<div id="ExportDialog" style="display:none;" class="noprint">
	<span style="font-weight:bold;">Sélectionnez les champs désirés</span><br />
	<form name="exportForm" id="exportForm" action="<?=$GLOBAL_START_URL?>/accounting/supplier_due_date_file.php" method="POST">
		<input type="hidden" value="" name="request" id="request" />
		<input type="hidden" value="1" name="export" />
		<?
			$fields = getExportableFields();
			foreach( $fields as $fieldId => $fieldName ){			
				?>
				<br /><input type="checkbox" name="fields[]" value="<?=$fieldId?>" checked="checked">&nbsp;<?=$fieldName?>
				<?
				
			}
		?>
		<br />
		<div style="width:100%;margin-top:20px;">
			<span style="font-weight:bold;">Titre de la page</span><br />
			<input type="text" name="pageTitle" value="" style="width:470px;" />
		</div>
		<div style="width:100%;margin-top:20px;">
			<input type="button" value="Annuler" class="blueButton" style="float:right;background:#ff0000;margin-left:10px;" onclick="$('#ExportDialog').dialog('destroy');" />
			<input type="submit" value="Exporter" name="sumy" class="blueButton" style="float:right;margin-left:10px;" />
		</div>
	</form>
</div>
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//Factures par mode de paiement--------------------------------------------------------------------------------

function getInvoiceByIdPayment (){
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
	$invoices = array();
	
	$i = 0;
	
	// extraction des mois et année de l'échéance pour trier
	$queryTri="
	SELECT DISTINCT MONTH(billing_supplier.deliv_payment) as month, YEAR(billing_supplier.deliv_payment) as year
	FROM billing_supplier, supplier
	WHERE billing_supplier.idsupplier=supplier.idsupplier
	AND `status` NOT IN ( '". SupplierInvoice::$STATUS_PAID . "' )
	AND (billing_supplier.deliv_payment >= NOW() OR billing_supplier.deliv_payment = '0000-00-00')
	ORDER BY year DESC, month DESC";	

	$rstri = DBUtil::query($queryTri);
	
	$monthyInvoices = array();
	
	while( !$rstri->EOF() ){
		
		$betweenMin = $rstri->fields("year")."-".$rstri->fields("month")."-01";
		$betweenMax = $rstri->fields("year")."-".$rstri->fields("month")."-31";
						
		$query = "
		SELECT billing_supplier.idsupplier,
			   supplier.name as company,
			   billing_supplier.billing_supplier, 
			   billing_supplier.Date,
			   billing_supplier.deliv_payment,
			   billing_supplier.total_amount_ht,
 			   billing_supplier.idpayment,
			   billing_supplier.editable,
			   ROUND( billing_supplier.total_amount - billing_supplier.credit_amount, 2 ) AS net_sum
		FROM billing_supplier, supplier
		WHERE billing_supplier.idsupplier=supplier.idsupplier
		AND proforma = 0
		AND NOT billing_supplier.editable = 0
		AND `status` NOT IN ( '". SupplierInvoice::$STATUS_PAID . "' )";
		
		if($rstri->fields("year") == "0"){
			$query.=" AND billing_supplier.deliv_payment = '0000-00-00' ";
		}else{
			$query.=" AND billing_supplier.deliv_payment BETWEEN '$betweenMin' AND '$betweenMax' ";
		}
		
		if((isset($_GET['sortby'])) AND (isset($_GET['sortby']))){
			$orderby	= "ORDER BY ".$_GET['sortby']." ".$_GET['sens'];
		}else{
			$orderby	= "ORDER BY deliv_payment DESC";
		}
		$query .=$orderby;
		
		$rs=DBUtil::query($query);
		
		if($rstri->fields("year") == "0")
			$betweenMin = "0000-00-00";
			
		$month = $betweenMin;
		
		$invoices = array();			

		while( !$rs->EOF() ){
			
			$idpayment = $rs->fields( "idpayment" );
			$mode_paiement = DBUtil::query( "SELECT name_1 FROM payment WHERE idpayment = $idpayment LIMIT 1" )->fields('name_1');
			
			$invoices[ $rs->fields("idsupplier") ][$rs->fields("billing_supplier")][ "total_amount_ht" ]	= $rs->fields("total_amount_ht");
			$invoices[ $rs->fields("idsupplier") ][$rs->fields("billing_supplier")][ "Date" ] 				= $rs->fields("Date");
			$invoices[ $rs->fields("idsupplier") ][$rs->fields("billing_supplier")][ "deliv_payment" ] 		= $rs->fields("deliv_payment");
			$invoices[ $rs->fields("idsupplier") ][$rs->fields("billing_supplier")][ "payment_mode" ] 		= $mode_paiement;
			$invoices[ $rs->fields("idsupplier") ][$rs->fields("billing_supplier")][ "net_sum" ]			= SupplierInvoice::getBalanceOutstanding( $rs->fields("billing_supplier") , $rs->fields("idsupplier") );
			
			$rs->MoveNext();
		}
		
		$monthyInvoices[$month] = $invoices;
		
		$rstri->MoveNext();
	}
		
	ksort( $monthyInvoices );

	return $monthyInvoices;
}

//--------------------------------------------------------------------------------

function getExportableFields(){

	$data = array();

	$data[ "idsupplier" ]		= Dictionnary::translate( "idsupplier" );
	$data[ "company" ] 			= Dictionnary::translate( "gest_com_supplier_company" );
	$data[ "billing_supplier" ]	= Dictionnary::translate( "gest_com_idbilling" );
	$data[ "Date" ]				= Dictionnary::translate( "gest_com_date" );
	$data[ "deliv_payment" ]	= "Date d'échéance";
	$data[ "total_amount_ht" ] 	= Dictionnary::translate( "gest_com_invoice_total_amount_ht" );
	$data[ "net_sum" ]			= "Net à payer";
		
	return $data;	
}

//--------------------------------------------------------------------------------

function &getExportableArray( $andstate = "" , $selectedFields ){
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
		
	$query = "	SELECT billing_supplier.idsupplier,
					   supplier.name as company,
					   billing_supplier.billing_supplier, 
					   billing_supplier.Date,
					   billing_supplier.deliv_payment,
					   billing_supplier.total_amount_ht,
					   ROUND( billing_supplier.total_amount - billing_supplier.credit_amount, 2 ) AS net_sum
				FROM billing_supplier, supplier
				WHERE billing_supplier.idsupplier=supplier.idsupplier
				AND proforma = 0
				AND `status` NOT IN ( '". SupplierInvoice::$STATUS_PAID . "' )
				$andstate";
	
	$query .= " ORDER BY deliv_payment DESC";
	
	$data = array();
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		if(in_array("idsupplier",$selectedFields))
			$data[ Dictionnary::translate( "idsupplier" ) ][]							= $rs->fields( "idsupplier" );
			
		if(in_array("company",$selectedFields))	
			$data[ Dictionnary::translate( "gest_com_supplier_company" ) ][] 			= $rs->fields( "company" );
			
		if(in_array("billing_supplier",$selectedFields))	
			$data[ Dictionnary::translate( "gest_com_idbilling" ) ][] 					= $rs->fields( "billing_supplier" );
			
		if(in_array("Date",$selectedFields))	
			$data[ Dictionnary::translate( "gest_com_date" ) ][] 						= Util::dateFormatEu( substr( $rs->fields( "Date" ), 0, 10 ) );
			
		if(in_array("deliv_payment",$selectedFields))	
			$data[ "Date d'échéance"][] 												= Util::dateFormatEu(  $rs->fields( "deliv_payment" ) );

		if(in_array("total_amount_ht",$selectedFields))	
			$data[ Dictionnary::translate( "gest_com_invoice_total_amount_ht" ) ][] 	= number_format( $rs->fields( "total_amount_ht" ) , 2 , "," , "" )."¤";
			
		if(in_array("net_sum",$selectedFields))	
			$data[ "Net à payer" ][]													= number_format( SupplierInvoice::getBalanceOutstanding( $rs->fields("billing_supplier") , $rs->fields("idsupplier") ) , 2 , "," , "" )."¤";

		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------

function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray,"Echeancier_fournisseur_" . date( "Ymd" ) );
	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------

 function getMonthYearName($date){
 	
	$res = @explode( "-", $date );
	
	if( count( $res ) != 3 )
		return "0000 00";
	
	@list( $year, $month, $day ) = $res;
	
	if($year!="0000" && $month != "00"){
 		setlocale( LC_TIME, "fr_FR.utf8" );
	
		$timestamp = mktime( 0, 0, 0, $month, $day, $year );
		$month 		= strftime("%B", $timestamp );
		$year 		= strftime("%Y", $timestamp );
	}else{
		$month 		= "00";
		$year 		= "0000";
	}
		
 	return $month." ".$year;
 }
 
//--------------------------------------------------------------------------------

function listDelayedPayments( $idsupplier, $start, $end = false ){ 

	global $GLOBAL_START_URL, $totalInvoiceCount,$superTotalHT,$superTotalNetTTC;
	
	include_once( dirname( __FILE__ ) . "/../objects/SupplierInvoice.php" );
	include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
	
	
	$query = "
	SELECT bs.idsupplier,
	   s.name as supplierName,
	   bs.billing_supplier, 
	   bs.Date,
	   bs.deliv_payment,
	   bs.total_amount,
	   bs.total_amount_ht,
	   bs.idpayment,
	   ROUND( bs.total_amount - bs.credit_amount, 2 ) AS net
	FROM billing_supplier bs, supplier s
	WHERE bs.idsupplier = s.idsupplier
	AND proforma = 0
	AND NOT (bs.deliv_payment = '0000-00-00')
	AND NOT (bs.total_amount = 0)
	AND bs.`status` NOT IN ( ". DBUtil::quote( SupplierInvoice::$STATUS_PAID ) . " )";
	
	if( $idsupplier )
		$query .= " AND s.idsupplier = '$idsupplier'";
		
	if( $end )
		$query .= " AND bs.deliv_payment >= '$end'";
		
	$query .= "
	AND bs.deliv_payment <= '$start'
	ORDER BY s.idsupplier, bs.deliv_payment ASC";

	$rs =& DBUtil::query( $query );

	if( !$rs->RecordCount() ){
		
		?>
		<p style="text-align:center; margin:20px;">Aucun retard de paiement</p>
		<script type="text/javascript">
			function fillTable_perso(){
				$("#recapTotalInvoices").html('0');
				$("#recapTotalHT").html('0&nbsp;&euro;');
				$("#recapTotalTTC").html('0&nbsp;&euro;');
			}
		</script>
		
		<?php
		
		return;
			
	}
	
	/* export */

	$condition = " AND billing_supplier.deliv_payment <= '$start'";
	
	if( $end )
		$condition .= " AND billing_supplier.deliv_payment >= '$end'";

	if( $idsupplier )
		$condition .= " AND billing_supplier.idsupplier = '$idsupplier'";
	
	if(isset( $_REQUEST[ "search" ] )){		
	?>
		<script type="text/javascript">
		/* <![CDATA[ */
			
			/* ----------------------------------------------------------------------------------- */
			
			function rowClick(obj) {
				
				var value = $(obj).attr("id").substring(13);
				var totalAmount = $("#selectionAmount").val();
				
				if($(obj).is(".activeAmountRow")){
					
					$(obj).removeClass("activeAmountRow");
					
					totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
					
				}
				else{
					
					$(obj).addClass("activeAmountRow");
					
					totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
					
				}
				
				$("#selectionAmount").val(totalAmount);
				$("#selectionAmountDiv").html(totalAmount + " ¤");
					
				
			}
			
			/* ----------------------------------------------------------------------------------- */
			

			
		/* ]]> */
		</script>
	<?php } ?>
	<script type="text/javascript">
		function upgradeRegulations( button ){

			var factures = new Array();
			button.parents('table').find(':checkbox').each( function(){ if( $(this).attr('checked') == true ){ factures.push( $(this).val() ); } } );
			
			var facturesTxt = factures.join(",");
			
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/supplier_due_date_file.php?UpdateInvoices&invoices=" + facturesTxt,
				async: true,
				type: "GET",
				error: function( XMLHttpRequest, textStatus, errorThrown ){
					
					$.growlUI( '', "Problème lors de l'enregistrement" );
					
				},
			 	success: function( responseText ){
					
					if( responseText.length == 0 ){
						$.growlUI( '', "Factures traitées" );
						button.parents('table').find(':checkbox').each( function(){ if( $(this).attr('checked') == true ){ $(this).parents('tr').remove(); } } );
						
					}else{
						$.growlUI( '', "Erreur" );
					}
				}
				
			});
		}
	</script>
	<?php
	
	/* résultats */
	
	$last_idsupplier = false;

	$litletotalInvoiceCount 	= 0;
	$litleTotalHT		= 0;
	$litleTotalNetTTC	= 0;
	
	while( !$rs->EOF() ){
		
		$idpayment = $rs->fields( "idpayment" );
		$mode_paiement = DBUtil::query( "SELECT name_1 FROM payment WHERE idpayment = $idpayment LIMIT 1" )->fields('name_1');
		
		if( $last_idsupplier != $rs->fields( "idsupplier" ) ){
			
			?>
			<div class="spacer"></div>
			
			<p style="font-size:14px;font-weight:bold;float:left;"><?php echo htmlentities( $rs->fields( "supplierName" ) ); ?></p>
			<div class="spacer"></div>
			<table class="dataTable resultTable tablesorter" style="">
				<thead>
					<tr>
						<th style="width:16%;">Facture n°</th>
						<th style="width:18%;">Date de création</th>
						<th style="width:18%;">Date d'échéance</th>
						<th style="width:16%;">Mode de règlement</th>
						<th style="width:16%;">Total facturé TTC</th>
						<th style="width:16%;">Net à payer</th>
					</tr>
				</thead>
				<tbody>
				<?php
							
				$total_amount_htSum = 0.0;
				$netSum 			= 0.0;

			}
			$net = SupplierInvoice::getBalanceOutstanding( $rs->fields("billing_supplier") , $rs->fields("idsupplier") );
		?>
		
		<tr class="amountRow" id="rowForAmount_<?php echo $rs->fields( "net" ) ?>" <?php echo isset( $_REQUEST[ "search" ] )? 'onclick="rowClick(this);"' :"" ?>>
			<td class="lefterCol"><?php echo htmlentities( $rs->fields( "billing_supplier" ) ); ?></td>
			<td><?php echo usDate2eu( $rs->fields( "Date" ) ); ?></td>
			<td><?php echo usDate2eu( $rs->fields( "deliv_payment" ) ); ?></td>
			<td><?php echo $mode_paiement ?></td>
			<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount" ) ); ?></td>
			<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $net ); ?></td>
		</tr>
		<?php
	
		$totalInvoiceCount++;
		$litletotalInvoiceCount++;
		
		$litleTotalHT		+= $rs->fields( "total_amount_ht" );
		$superTotalHT		+= $rs->fields( "total_amount_ht" );
		
		$litleTotalNetTTC	+= $net;
		$superTotalNetTTC	+= $net;
		
		$last_idsupplier 	= $rs->fields( "idsupplier" );	
		
		$total_amount_htSum += $rs->fields( "total_amount_ht" );
		$netSum	 			+= $net;
		
		$rs->MoveNext();
		
		if( $rs->EOF() || $rs->fields( "idsupplier" ) != $last_idsupplier ){
			
				?>
				</tbody>
				<tfoot>
					<tr>
						<th colspan="5" class="none" style="border-style:none;"></th>
						<!--<th class="totalAmount"><?php echo Util::priceFormat( $total_amount_htSum ); ?></th>-->
						<th class="totalAmount"><?php echo Util::priceFormat( $netSum ); ?></th>
					</tr>
				</tfoot>
			</table>
			<?php
		
		}

	}
	?>

	<script type="text/javascript">
		function fillTable_perso(){
			$("#recapTotalInvoices").html('<?=$litletotalInvoiceCount?>');
			$("#recapTotalHT").html('<?=Util::priceFormat($litleTotalHT)?>');
			$("#recapTotalTTC").html('<?=Util::priceFormat($litleTotalNetTTC)?>');
		}
	</script>
	<div class="spacer"></div>
	<?
}

//--------------------------------------------------------------------------------

function displayDelayedPaymentsForm( $start, $end ){
	
	global $GLOBAL_START_URL;
	
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function searchDelayedPayments(){

			var data = "search=1&deliv_payment_start=" + escape( $( "#deliv_payment_start" ).val() );
			data += "&deliv_payment_end=" + escape( $( "#deliv_payment_end" ).val() );
			data += "&idsupplier=" + escape( $( "#idsupplier" ).val() );

			$( "#DelayedPaymentList" ).html( '<img src="<?php echo $GLOBAL_START_URL; ?>images/back_office/content/small_black_spinner.gif" alt="" />' );
			
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL; ?>/accounting/supplier_due_date_file.php",
				async: true,
				data: data,
			 	success: function( responseText ){
			 		
			 		$( "#DelayedPaymentList" ).html( responseText );
					$(".tablesorter").tablesorter();
					fillTable_perso();
				}
	
			});
			
		}
		
		
				
	/* ]]> */
	</script>
	
	<div class="blocMultiple blocMLeft" style="margin-bottom:0;">	
		<label><span>Fournisseur</span></label>
		<?php
		
			include_once( dirname( __FILE__ ) . "/../objects/XHTMLFactory.php" );
			
			XHTMLFactory::createSelectElement( 
			
				"supplier", 
				"idsupplier", 
				"name",
				"name",
				isset( $_REQUEST[ "idsupplier" ] ) ? intval( $_REQUEST[ "idsupplier" ] ) : false,
				0,
				"Tous",
				"id=\"idsupplier\""
				
			);
			
		?>
		<div class="spacer"></div>
	</div>
	
	<div class="blocMultiple blocMRight" style="margin-bottom:0;">
		<label style="width:35%;"><span>Date d'échéance entre</span></label>
		<input type="text" class="calendarInput " id="deliv_payment_start" style="float:left;" value="<?php if( isset( $_REQUEST[ "deliv_paymnet_start" ] ) ) echo stripslashes( $_REQUEST[ "deliv_payment_start" ] ); ?>" />
		
		<label class="smallLabel"><span>et</span></label>
		<input type="text" class="calendarInput" id="deliv_payment_end" style="float:left;" value="<?php if( isset( $_REQUEST[ "deliv_payment_end" ] ) ) echo stripslashes( $_REQUEST[ "deliv_payment_end" ] ); ?>" />
		<div class="spacer"></div>
		
		
		<input type="button" class="blueButton floatright" style="margin-top:10px;" id="SearchButton" value="Rechercher" onclick="searchDelayedPayments();" />

	</div>
	<div class="spacer"></div>

	<?php
	
		include_once( dirname( __FILE__ ) . "/../objects/DHTMLCalendar.php" );
		
		DHTMLCalendar::calendar( "deliv_payment_start" );
		DHTMLCalendar::calendar( "deliv_payment_end" );
	
}

//--------------------------------------------------------------------------------

?>