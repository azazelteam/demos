<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Futures factures clients
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/SupplierOrder.php" );

//------------------------------------------------------------------------------------------
/* Traitement Ajax de l'enregistrement des dates */

if( isset( $_GET[ "idbl_delivery" ] ) || isset( $_GET[ "date" ] ) ){
	
	if( isset( $_GET[ "idbl_delivery" ] ) && isset( $_GET[ "date" ] ) ){
		
		include_once( "$GLOBAL_START_PATH/script/global.fct.php" );
		
		$date = euDate2us( $_GET[ "date" ] );
		
		if( !preg_match( "/([0-9]{4})-([0-9]{2})-([0-9]{2})/", $date ) )
			exit( "1" );
		
		$query = "UPDATE bl_delivery SET dispatch_date = '$date' WHERE idbl_delivery = " . $_GET[ "idbl_delivery" ];
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			exit( "2" );
		
		$idorder_supplier = DBUtil::getDBValue( "idorder_supplier", "bl_delivery", "idbl_delivery", $_GET[ "idbl_delivery" ] );
		
		$query = "SELECT MAX( dispatch_date ) AS dispatch_date FROM bl_delivery WHERE idorder_supplier = $idorder_supplier";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			exit( "3" );
		
		$query = "UPDATE order_supplier SET dispatch_date = '" . $rs->fields( "dispatch_date" ) . "' WHERE idorder_supplier = $idorder_supplier";
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			exit( "4" );
		
		exit( "0" );
		
	}else{
		
		exit( "5" );
		
	}
	
}

//------------------------------------------------------------------------------------------

$Title = "Futures factures clients";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------

if ((isset ($_POST["UpdateRows"]) || isset ($_POST["UpdateRows_y"])) && isset ($_POST["OrderSupplierRows"]))
	updateLoads();
?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.tablesorter.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/tableSorter.css" />+

<div id="globalMainContent">
<?php

displaySearchResultsByMonth();
?>
</div>
<script type="text/javascript">
	$(function() {
		$(".tablesorter").tablesorter( );
	}); 
</script>
<?php


include_once ("$GLOBAL_START_PATH/templates/back_office/foot.php");

//------------------------------------------------------------------------------------------------

function displaySearchResultsByMonth() {

	global	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	
	$lang = User::getInstance()->getLang();

	$period = DBUtil::getParameterAdmin("next_invoices_period"); //nombre de mois
	$idinternal_supplier = DBUtil::getParameterAdmin("internal_supplier");

	$month = date("m");
	$year = date("Y");

	$globalCount = 0;
	$globalAmount = 0;
	
	?>
		
		<div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Commandes en attente de livraison à facturer</p>
            </div>
            <?php summary(); ?>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    	</div> <!-- mainContent fullWidthContent -->

		<?php
	
	
	
	
	
	//$d == -1 correspond aux mois avant le mois en cours
	for ($d = -1; $d < $period; $d++) {

		//next month
		$next_month = mktime(0, 0, 0, date("m") + $d, date("d"), date("Y"));

		$dd = date("m:Y", $next_month);

		//mois
		$month_dd = substr($dd, 0, 2);
		$year_dd = substr($dd, 3, 4);

		$query = "
		SELECT bl.availability_date,
			bl.idbl_delivery,
			bl.idorder_supplier,
			bl.idorder,
			o.idbuyer,
			c.firstname,
			c.lastname,
			c.title,
			b.company,
			b.contact,
			b.insurance_status,
			b.insurance_amount,
			b.insurance,
			o.idpayment,
			o.factor,
			o.total_amount,
			u1.initial AS comFact,
			u2.initial AS comOrder,
			p.name_1 AS payment,
			os.idsupplier,
			os.DateHeure,
			bl.dispatch_date,
			os.idorder_supplier, 
			os.total_amount_ht,
			os.status,
			os.billing_amount,
			sc.phonenumber AS supplierPhoneNumber,
			s.name AS supplierName,
			s.idsupplier
		FROM bl_delivery bl LEFT JOIN bl_delivery_row blr ON blr.idbl_delivery = bl.idbl_delivery, 
			contact c, 
			buyer b, 
			`order` o, 
			user u1,
			user u2,
			payment p,
			order_supplier os,
			supplier s
			LEFT JOIN supplier_contact sc ON ( sc.idsupplier = s.idsupplier AND sc.idcontact = s.main_contact )
		WHERE blr.idbl_delivery IS NOT NULL -- BL vides suite à des cdes fournisseur de port
			AND bl.idbilling_buyer = 0
			AND bl.idbuyer = c.idbuyer
			AND c.idcontact = 0
			AND b.idbuyer = o.idbuyer
			AND o.idorder = bl.idorder
			AND u1.iduser = bl.iduser
			AND u2.iduser = o.iduser
			AND o.idpayment = p.idpayment
			AND bl.idorder_supplier = os.idorder_supplier
			AND os.idsupplier = s.idsupplier
			AND os.status <> 'cancelled'
			AND NOT os.total_amount_ht = 0
			AND os.idsupplier <> '$idinternal_supplier'";

		if ( !User::getInstance()->get( "admin" ) )
			$query .= " AND ( bl.iduser = '" . User::getInstance()->getId() . "' OR b.iduser = '" . User::getInstance()->getId() . "' )";

		$query .= " GROUP BY bl.idbl_delivery";
		
		$rs = DBUtil::query($query);

		$result = array ();
		$bl_total_amount = 0.0;
		$idorder_supplier = 0;
		$i = 0;
		$factor_minimum = DBUtil::getParameterAdmin( "factor_minbuying" );
		
		while (!$rs->EOF()) {
$rowspanStr =  false;
			//Récupération de la date de mise à disposition (prévisionnelle ou non)
			if ($rs->fields("availability_date") != "0000-00-00") { //Date de mise à disposition renseignée

				$availability_date = $rs->fields("availability_date");
				$real_availability_date = true;

			} else { //Date prévisionnelle calculée

				$availability_date = SupplierOrder::getProvisionalAvailabilityDate($rs->fields("idorder_supplier"));
				$real_availability_date = false;

				//Si aucune date n'est renvoyée (délai non renseigné), on ajoute 3 semaines à la date de création (21 jours)
				if ($availability_date === false)
					$availability_date = date("Y-m-d", mktime(0, 0, 0, substr($rs->fields("DateHeure"), 5, 2), substr($rs->fields("DateHeure"), 8, 2) + 21, substr($rs->fields("DateHeure"), 0, 4)));

			}
			//Si la date de mise à disposition est dans la tranche à afficher
			//On remplit le tableau

			//Toutes les factures avant le mois en cours
			//strcmp( $availability_date, date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), 1, date( "Y" ) ) ) ) < 0

			//Factures par mois
			//strcmp( $availability_date, date( "Y-m-d", mktime( 0, 0, 0, date( "m" ) + $d, 1, date( "Y" ) ) ) ) < 0 && strcmp( $availability_date, date( "Y-m-d", mktime( 0, 0, 0, date( "m" ) + $d, 1, date( "Y" ) ) ) ) >= 0

			if ((strcmp($availability_date, date("Y-m-d", mktime(0, 0, 0, date("m"), 1, date("Y")))) < 0 && $d == -1) || (strcmp($availability_date, date("Y-m-d", mktime(0, 0, 0, date("m") + $d +1, 1, date("Y")))) < 0 && strcmp($availability_date, date("Y-m-d", mktime(0, 0, 0, date("m") + $d, 1, date("Y")))) >= 0 && $d >= 0)) {

				$result[ $i ][ "idbuyer" ]				= $rs->fields( "idbuyer" );
				$result[ $i ][ "company" ]				= $rs->fields( "company" );
				$result[ $i ][ "isContact" ]			= $rs->fields( "contact" );
				$result[ $i ][ "idorder" ]				= $rs->fields( "idorder" );
				$result[ $i ][ "idsupplier" ]			= $rs->fields( "idsupplier" );
				$result[ $i ][ "supplierPhoneNumber" ]	= $rs->fields( "supplierPhoneNumber" );
				$result[ $i ][ "idorder_supplier" ]		= $rs->fields( "idorder_supplier" );
				$result[ $i ][ "comFact" ]				= $rs->fields( "comFact" );
				$result[ $i ][ "comOrder" ]				= $rs->fields( "comOrder" );
				$result[ $i ][ "insurance" ]				= $rs->fields( "insurance" ) ? "Oui" : "Non";
				$result[ $i ][ "total_amount" ]			= $rs->fields( "total_amount" );
				$result[ $i ][ "idpayment" ]			= $rs->fields( "idpayment" );
				$result[ $i ][ "payment" ]				= $rs->fields( "payment" );
				$result[ $i ][ "idbl_delivery" ]		= $rs->fields( "idbl_delivery" );
				$result[ $i ][ "idorder_supplier" ]		= $rs->fields( "idorder_supplier" );
				$result[ $i ][ "supplierName" ]			= $rs->fields( "supplierName" );
				$result[ $i ][ "dispatch_date" ]		= $rs->fields( "dispatch_date" );
				$result[ $i ][ "supplierOrderHREF" ]	= "$GLOBAL_START_URL/sales_force/supplier_order.php?IdOrder=" . $result[ $i ][ "idorder_supplier" ];
				$result[ $i ][ "availability_date" ]	= $availability_date;
				$result[ $i ][ "dateColor" ]			= $real_availability_date === true ? " orangeText" : " blueText";
				
				$result[ $i ][ "insurance_amount" ] = "N/D";
				if( $rs->fields( "insurance" ) )
					$result[ $i ][ "insurance_amount" ] = Util::priceFormat( $rs->fields( "insurance_amount" ));
				else{
					switch( $rs->fields( "insurance_status" ) ){
						case "Refused":
							$result[ $i ][ "insurance_amount" ] = "<span style=\"color:#FF0000;\">Refusé</span>";
							break;
						
						case "Deleted":
							$result[ $i ][ "insurance_amount" ] = "<span style=\"color:#FF0000;\">Supprimé</span>";
							break;
						
						case "Terminated":
							$result[ $i ][ "insurance_amount" ] = "<span style=\"color:#FF0000;\">Résilé</span>";
							break;
					}
				}

				$result[ $i ][ "export" ] = isBuyerExport($result[ $i ][ "idorder" ]);

				
				// Remettre le meme code qu'en bas popur afficher le montant total aussi en haut du  tableau
				
					      
				            
				
				
				//Montant commandé HT
				
				if( $result[ $i ][ "idorder_supplier" ] != $idorder_supplier){
					
					
					$bl_total_amount += round( $rs->fields( "total_amount" ), 2 );
					
					$idorder_supplier = $result[ $i ][ "idorder_supplier" ];
					
				}

				$result[$i]["buyerHREF"] = "$GLOBAL_START_URL/sales_force/contact_buyer.php?key=" . $result[$i]["idbuyer"] . "&amp;type=";
				$result[$i]["buyerHREF"] .= $result[$i]["isContact"] ? "contact" : "buyer";
				$result[$i]["orderHREF"] = "$GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=" . $result[$i]["idorder"];

				$i++;

			}

			$rs->MoveNext();

		}


/**************************/

if( count( $result ) ){
     $bl_total_amount=0;
	   $i = 0;
	   $idorder_supplier = 0;
						while( $i < count( $result ) )
						{
							$order = $result[ $i ];
							$deliveryNoteCount = 1;
							while( $i + $deliveryNoteCount < count( $result ) && $result[ $i  ][ "idorder" ] == $result[ $i + $deliveryNoteCount ][ "idorder" ] )
								$deliveryNoteCount++;	
							
						
								
								if( $order[ "idorder_supplier" ] != $idorder_supplier)
								
								{
					
					
								$bl_total_amount += round($order["total_amount"], 2 );
								
								$idorder_supplier = $order[ "idorder_supplier" ];
								
							}
				$i += $deliveryNoteCount;
				
					}			


}


/**************************/

		if( !count( $result ) ){
?>
	<div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $d < 0 ? "Mois précédent" : ucfirst( strftime("%B", mktime( 0, 0, 0, $month_dd, 1, date( "Y" ) ) ) ) . " " . $year_dd ?> : aucune commande trouvée</p>
            </div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
<?php
			
		}else{
			
			include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
			
			$idorder_supplier = 0;
			
			$globalCount += count( $result );
			$globalAmount += $bl_total_amount;
			
?>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		function updateDispatchDate( calendarField ){
			
			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
			$.blockUI.defaults.css.fontSize = '12px';
			
			var idbl_delivery = calendarField.id.substring( 9 );
			var date = calendarField.value;
			
			if( date == "" )
				return false;
			
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/provisionnal_next_invoices.php?idbl_delivery=" + idbl_delivery + "&date=" + date,
				async: true,
				type: "GET",
				error: function( XMLHttpRequest, textStatus, errorThrown ){
					
					$.growlUI( '', "Problème lors de l'enregistrement" );
					
				},
			 	success: function( responseText ){
					
					if( responseText == "0" )
						$.growlUI( '', 'Modification enregistrée' );
					else if( responseText == "1" )
						$.growlUI( '', "Problème lors de l'enregistrement<br />Le format de la date n'est pas correct" );
					else if( responseText == "2" || responseText == "3" || responseText == "4" )
						$.growlUI( '', "Problème lors de l'enregistrement de la modification dans la base de données" );
					else
						$.growlUI( '', "Problème lors de l'enregistrement" );
					
				}
				
			});
			
		}
		
		/* -------------------------------------------------------------------------------------------- */
		
		function orderBy( index, asc ){
		
			document.location = "<?php echo $GLOBAL_START_URL; ?>/accounting/provisionnal_next_invoices.php?orderby=" + index + "&" + asc;
		}
		
		/* -------------------------------------------------------------------------------------------- */
		
	/* ]]> */
	</script>
	<div class="mainContent fullWidthContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $d < 0 ? "Mois précédent" : ucfirst( strftime("%B", mktime( 0, 0, 0, $month_dd, 1, date( "Y" ) ) ) ) . " " . $year_dd ?> : <?php echo count( $result ) ?> commande<?php echo count( $result ) > 1 ? "s" : "" ?> non livrée<?php echo count( $result ) > 1 ? "s" : "" ?> - non facturée<?php echo count( $result ) > 1 ? "s" : "" ?> - Montant Total TTC: <?php echo Util::priceFormat( $bl_total_amount ) ?></p>
            	<div class="rightContainer"><span class="orangeText">Date renseignée</span> - <span class="blueText">Date prévisionnelle</span></div>
            </div>
            <div class="subContent">
            	<!-- tableau résultats recherche -->
                <div class="resultTableContainer clear">
                	<table class="dataTable resultTable">
                    	<thead>
                            <tr>
                                <th>Resp. fact.</th>
                                <th>Com.</th>
								<th>Client n°</th>
								<th>Raison sociale</th>
								<th>Assur.</th>
								<th>Montant<br />assuré</th>
								<th>Export</th>
								<th>BL n°</th>
								<th>Mode de paiement</th>
								<th>Total TTC commande</th>
								<th>Cde Four. n°</th>
								<th>Fournisseur</th>
								<th>Date de départ théorique</th>
								<th>Date de livraison<br />confirmée</th>
                            </tr>
   						</thead>
                        <tbody>
						<?php 
						
						$i = 0;
						while( $i < count( $result ) ){
						
							$order = $result[ $i ];
							
							$deliveryNoteCount = 1;
							while( $i + $deliveryNoteCount < count( $result ) && $result[ $i  ][ "idorder" ] == $result[ $i + $deliveryNoteCount ][ "idorder" ] )
								$deliveryNoteCount++;

							?>
				             <script type="text/javascript">
							/* <![CDATA[ */
							    $(document).ready(function(){
							        $("#supplier_<?php echo $order[ "idbl_delivery" ] ?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Téléphone :</b> <?php echo $order[ "supplierPhoneNumber" ] ?>',
							            position: {
							                corner: {
							                    target: "rightMiddle",
							                    tooltip: "leftMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							/* ]]> */
							</script>
							<?php
							
								$deliveryNotes = array( $order[ "idbl_delivery" ] );
								$p = 1;
				       			while( $p < $deliveryNoteCount ){
						
				       				array_push( $deliveryNotes, $result[ $i + $p ][ "idbl_delivery" ] );
				            		
				            		$p++;
				            		
				            	} 
				            	
								/* autres BL */

								$query = "
								SELECT bl.idbl_delivery, 
									bl.idbilling_buyer <> 0 AS invoiced,
									bl.idorder_supplier,
									p.name$lang AS payment,
									o.total_amount,
									s.name AS supplierName,
									bl.availability_date,
									bl.dispatch_date
								FROM bl_delivery bl, payment p, `order` o, order_supplier os, supplier s
								WHERE os.idorder = '" . $order[ "idorder" ] . "'
								-- AND os.idorder_supplier <> '" . $order[ "idorder_supplier" ] . "'
								AND os.idorder = o.idorder
								AND bl.idorder_supplier = os.idorder_supplier
								AND o.idpayment = p.idpayment
								AND os.idsupplier = s.idsupplier
								AND os.idsupplier <> '$idinternal_supplier'";
								
								if( count( $deliveryNotes ) )
									$query .= "	AND bl.idbl_delivery NOT IN( '" . implode( "','", $deliveryNotes ) . "' )";
								
								$query .= " ORDER BY bl.availability_date ASC";
								
								$otherDeliveryNotesRS =& DBUtil::query( $query );
								
								$rowspan = $otherDeliveryNotesRS->RecordCount();
								
								$rowspan = $deliveryNoteCount + $otherDeliveryNotesRS->RecordCount();
								
								$rowspanStr = $rowspan > 1 ? " rowspan=\"$rowspan\"" : "";
								
							?>
							<tr>
				            	<td<?php echo $rowspanStr; ?> class="lefterCol"><?php echo $order[ "comFact" ] ?></td>
				            	<td<?php echo $rowspanStr; ?>><?php echo $order[ "comOrder" ] ?></td>
				                <td<?php echo $rowspanStr; ?>><a href="<?php echo $order[ "buyerHREF" ] ?>" onclick="window.open( this.href ); return false;"><?php echo $order[ "idbuyer" ] ?></a></td>
				                <td<?php echo $rowspanStr; ?>><a href="<?php echo $order[ "buyerHREF" ] ?>" onclick="window.open( this.href ); return false;"><?php echo $order[ "company" ] ?></a></td>
				                <td<?php echo $rowspanStr; ?>><a href="<?php echo $order[ "buyerHREF" ] ?>#insurance" onclick="window.open( this.href ); return false;"><?php echo $order[ "insurance" ] ?></a></td>
				                <td<?php echo $rowspanStr; ?>><?php echo $order[ "insurance_amount" ] ?></td>
				                <td<?php echo $rowspanStr; ?>><?php echo $order[ "export" ] ?></td>
				                <td><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_delivery.php?iddelivery=<?php echo $order[ "idbl_delivery" ] ?>" onclick="window.open(this.href); return false;"><?php echo $order[ "idbl_delivery" ] ?></a></td>
				                <td<?php echo $rowspanStr; ?>><?php echo $order[ "payment" ] ?></td>
				                <td<?php echo $rowspanStr; ?> style="text-align:right;"><?php echo Util::priceFormat( $order[ "total_amount" ] ) ?></td>
				                <td><a href="<?php echo $order[ "supplierOrderHREF" ] ?>" onclick="window.open(this.href); return false;"><?php echo $order[ "idorder_supplier" ] ?></a></td>
				                <td id="supplier_<?php echo $order[ "idbl_delivery" ] ?>"><?php echo htmlentities( ucwords( strtolower( $order[ "supplierName" ] ) ) ) ?></td>
				                <td class="<?php echo $order[ "dateColor" ] ?>"><?php echo usDate2eu( $order[ "availability_date" ] ) ?></td>
				                <td class="righterCol" style="white-space:nowrap;">
				                	<input type="text" name="calendar_<?php echo $order[ "idbl_delivery" ] ?>" id="calendar_<?php echo $order[ "idbl_delivery" ] ?>" value="<?php echo $order[ "dispatch_date" ] == "0000-00-00" ? "" : usDate2eu( $order[ "dispatch_date" ] ) ?>" class="calendarInput" />
				                	<?php echo DHTMLCalendar::calendar( "calendar_" . $order[ "idbl_delivery" ], true, "updateDispatchDate" ) ?>
				                </td>
				            </tr>
				       		<?php
				       		
				       			/* BL de la même période */
				       			
								$p = 1;
				       			while( $p < $deliveryNoteCount ){
				       				
				       				$order = $result[ $i + $p ];
				       				
				       				?> 
									<tr style="border-style:none;">
						            	<td><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_delivery.php?iddelivery=<?php echo $order[ "idbl_delivery" ] ?>" onclick="window.open(this.href); return false;"><?php echo $order[ "idbl_delivery" ] ?></a></td>
						                <td><a href="<?php echo $order[ "supplierOrderHREF" ] ?>" onclick="window.open(this.href); return false;"><?php echo $order[ "idorder_supplier" ] ?></a></td>
				               			<td id="supplier_<?php echo $order[ "idbl_delivery" ] ?>"><?php echo htmlentities( ucwords( strtolower( $order[ "supplierName" ] ) ) ) ?></td>
						                <td class="<?php echo $order[ "dateColor" ] ?>"><?php echo usDate2eu( $order[ "availability_date" ] ) ?></td>
						                <td style="white-space:nowrap;">
						                	<input type="text" name="calendar_<?php echo $order[ "idbl_delivery" ] ?>" id="calendar_<?php echo $order[ "idbl_delivery" ] ?>" value="<?php echo $order[ "dispatch_date" ] == "0000-00-00" ? "" : usDate2eu( $order[ "dispatch_date" ] ) ?>" class="calendarInput" />
						                	<?php echo DHTMLCalendar::calendar( "calendar_" . $order[ "idbl_delivery" ], true, "updateDispatchDate" ) ?>
						                </td>
						            </tr>
				            		<?php 
	
				            		$p++;
				            		
				            	} 

								$i += $deliveryNoteCount;
			
								/* autres BL */
								
								while( !$otherDeliveryNotesRS->EOF() ){
									
									?>
									<tr style="border-top-style:none; border-bottom-style:none;">
										<td><a style="color:#A1A1A1;" href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_delivery.php?iddelivery=<?php echo $otherDeliveryNotesRS->fields( "idbl_delivery" ); ?>" onclick="window.open(this.href); return false;"><?php echo $otherDeliveryNotesRS->fields( "idbl_delivery" ); ?></a></td>
						                <td><a style="color:#A1A1A1;" href="<?php echo $GLOBAL_START_URL; ?>/sales_force/supplier_order.php?IdOrder=<?php echo $otherDeliveryNotesRS->fields( "idorder_supplier" ); ?>" onclick="window.open(this.href); return false;"><?php echo $otherDeliveryNotesRS->fields( "idorder_supplier" ); ?></a></td>
				               			<td style="color:#A1A1A1;"><?php echo htmlentities( ucwords( strtolower( $otherDeliveryNotesRS->fields( "supplierName" ) ) ) ) ?></td>
						                <td style="color:#A1A1A1;"><?php echo usDate2eu( $otherDeliveryNotesRS->fields( "availability_date" ) ) ?></td>
						                <td style="white-space:nowrap; color:#A1A1A1; text-align:left;">
						                	<?php echo $otherDeliveryNotesRS->fields( "dispatch_date" ) == "0000-00-00" ? "" : usDate2eu( $otherDeliveryNotesRS->fields( "dispatch_date" ) ) ?>
						                </td>
									</tr>
									<?php
									
									$otherDeliveryNotesRS->MoveNext();
										
								}
								
							} 
							
							?>
						</tbody>
						<tfoot>
							<tr>
								<th colspan="9" style="border-style:none"></th>
								<th class="totalAmount"><?php echo Util::priceFormat( $bl_total_amount ) ?></th>
								<th colspan="4" style="border-style:none"></th>
							</tr>
						</tfoot>
			        </table>
				</div><!-- resultTableContainer clear -->
			</div>
	   	</div>
	    <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
	<?php

		}
	}
	
	?>
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Récapitulatif</p>
			</div>
			<div class="subContent">

				<div class="resultTableContainer clear">
					<table class="dataTable resultTable" style="width:50%;">
						<thead>
							<tr>
								<th>Nombre de factures</th>
								<th>Total TTC à facturer</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="lefterCol"><?php echo $globalCount ?></td>
								<td class="righterCol"><?php echo Util::priceFormat( $globalAmount ) ?></td>
							</tr>
						</tbody>
					</table>
				</div><!-- resultTableContainer clear -->
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<?php

}

//--------------------------------------------------------------------------------------------------

function isBuyerExport($idorder) {

	$query = "SELECT idbuyer, idbilling_adress FROM `order` WHERE idorder=$idorder";

	$rs = DBUtil::query($query);
	if ($rs === false)
		die(Dictionnary::translate("Impossible de récupérer l'adresse de facturation"));

	$idbilling = $rs->fields("idbilling_adress");
	$idbuyer = $rs->fields("idbuyer");

	//Pays B@O
	$state_bao = strtolower(DBUtil::getParameterAdmin("ad_state"));

	if ($idbilling > 0) {
		//Adresse de facturation différente
		$query = "SELECT idstate FROM billing_adress WHERE idbilling_adress=$idbilling";

		$rs = DBUtil::query($query);

		if ($rs === false)
			die(Dictionnary::translate("Impossible de récupérer le pays de l'adresse de facturation"));

		if ($rs->RecordCount() > 0) {
			$idstate = $rs->fields("idstate");
			//Pays du client
			$query = "SELECT name_1 as pays FROM state WHERE idstate=$idstate";

			$rs = DBUtil::query($query);
			if ($rs === false)
				die(Dictionnary::translate("Impossible de récupérer le pays de ladr de fact du client pour la commande $idorder"));

			$state = strtolower($rs->fields("pays"));
			if ($state != $state_bao)
				$export = 'Oui';
			else
				$export = 'Non';

		} else {
			$export = "?";
		}

	} else {
		//Adresse de facturation du client
		$query = "SELECT idstate FROM buyer WHERE idbuyer=$idbuyer";

		$rs = DBUtil::query($query);

		if ($rs === false)
			die(Dictionnary::translate("Impossible de récupérer le pays du client"));

		$idstate = $rs->fields("idstate");

		//Pays du client
		$query = "SELECT name_1 as paysb FROM state WHERE idstate=$idstate";

		$rs =& DBUtil::query($query);
		if ($rs === false)
			trigger_error( "Impossible de récupérer le pays de du client pour la commande $idorder", E_USER_ERROR );

		$state = strtolower($rs->fields("paysb"));

		if ($state != $state_bao)
			$export = 'Oui';
		else
			$export = 'Non';
	}

	return $export;

}
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------

function summary(){
	
	$internal_supplier 	= DBUtil::getParameterAdmin( "internal_supplier" );
	$factor_minimum		= DBUtil::getParameterAdmin( "factor_minbuying" );
	
	/*$query = "
	SELECT COUNT( o.idorder ) AS orderCount,
		SUM( ROUND( o.total_amount, 2 ) ) AS totalATI,
		SUM( ( o.factor <> 0 ) * ( o.factor REGEXP( '[0-9]*' ) ) * ( b.factor_status = 'Accepted' ) * ( ROUND( o.total_amount, 2 ) >= '$factor_minimum' ) ) AS factorCount,
		SUM( ( o.factor <> 0 ) * ( o.factor REGEXP( '[0-9]*' ) ) * ( b.factor_status = 'Accepted' ) * ( ROUND( o.total_amount, 2 ) >= '$factor_minimum' ) * ROUND( o.total_amount, 2 ) ) AS totalFactorATI,
		SUM( ( ( o.factor = 0 ) + ( o.factor NOT REGEXP( '[0-9]*' ) ) + ( b.factor_status <> 'Accepted' ) + ( ROUND( o.total_amount, 2 ) < '$factor_minimum' ) ) <> 0 ) AS nonFactorCount,
		SUM( ( ( ( o.factor = 0 ) + ( o.factor NOT REGEXP( '[0-9]*' ) ) + ( b.factor_status <> 'Accepted' ) + ( ROUND( o.total_amount, 2 ) < '$factor_minimum' ) ) <> 0 ) * ROUND( o.total_amount, 2 ) ) AS totalNonFactorATI,
		SUBSTR( bl.availability_date, 1, 7 ) AS `date`
	FROM bl_delivery bl, `order` o, order_supplier os, buyer b
	WHERE bl.idbilling_buyer = 0
	AND bl.idorder_supplier = os.idorder_supplier
	AND os.idsupplier <> '$internal_supplier'
	AND os.idorder = o.idorder
	AND o.idbuyer = b.idbuyer
	AND bl.availability_date = ( SELECT MAX( bl2.availability_date ) FROM bl_delivery bl2 WHERE bl2.idorder_supplier = bl.idorder_supplier )
	GROUP BY `date`
	ORDER BY `date` DESC";*/

	$query = "
	SELECT DISTINCT( o.idorder ),
		ROUND( o.total_amount, 2 ) AS totalATI,
		COALESCE( ( o.factor <> 0 ) * ( o.factor REGEXP( '[0-9]*' ) ) * ( b.factor_status = 'Accepted' ) * ( ROUND( o.total_amount, 2 ) >= '$factor_minimum' ), FALSE ) AS factor,
		COALESCE( ( o.factor <> 0 ) * ( o.factor REGEXP( '[0-9]*' ) ) * ( b.factor_status = 'Accepted' ) * ( ROUND( o.total_amount, 2 ) >= '$factor_minimum' ) * ROUND( o.total_amount, 2 ), 0.0 ) AS factorATI,
		COALESCE( ( ( o.factor = 0 ) + ( o.factor NOT REGEXP( '[0-9]*' ) ) + ( b.factor_status <> 'Accepted' ) + ( ROUND( o.total_amount, 2 ) < '$factor_minimum' ) ) <> 0, FALSE ) AS nonFactor,
		COALESCE( ( ( ( o.factor = 0 ) + ( o.factor NOT REGEXP( '[0-9]*' ) ) + ( b.factor_status <> 'Accepted' ) + ( ROUND( o.total_amount, 2 ) < '$factor_minimum' ) ) <> 0 ) * ROUND( o.total_amount, 2 ), 0.0 ) AS nonFactorATI,
		SUBSTR( MAX( bl.availability_date ), 1, 7 ) AS `date`
	FROM bl_delivery bl, `order` o, order_supplier os, buyer b
	WHERE bl.idbilling_buyer = 0
	AND bl.idorder_supplier = os.idorder_supplier
	AND os.idsupplier <> '$internal_supplier'
	AND os.idorder = o.idorder
	AND o.idbuyer = b.idbuyer
	AND bl.availability_date = ( SELECT MAX( bl2.availability_date ) FROM bl_delivery bl2 WHERE bl2.idorder_supplier = bl.idorder_supplier )
	GROUP BY o.idorder
	ORDER BY SUBSTR( MAX( bl.availability_date ), 1, 7 ) DESC";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return;
		
	?>
	<div class="subContent">
	<div class="resultTableContainer clear" style="padding-top:5px;">
		<table class="dataTable resultTable tablesorter">
	    	<thead>
	    		<tr>
	                <th style="vertical-align:top; border-right:1px solid #CBCBCB;">Mois</th>
	                <th style="vertical-align:top;">Nombre Total</th>
	                <th style="vertical-align:top; border-right:1px solid #CBCBCB;">Montant Total<br />TTC</th>
	                <th style="vertical-align:top;">Factures<br />cédées<br />au Factor</th>
	                <th style="vertical-align:top; border-right:1px solid #CBCBCB;">Montant Total<br />TTC</th>
	                <th style="vertical-align:top;">Factures<br />non cédées<br />au Factor</th>
	                <th style="vertical-align:top;">Montant Total<br />TTC</th>
				</tr>
			</thead>
			<tbody>
			<?php
			
				setLocale( LC_TIME, "fr_FR.utf8" );

				$lastDate 			= false;
				$orderCount 		= 0;
				$totalATI 			= 0.0;
				$factorCount 		= 0;
				$totalFactorATI 	= 0.0;
				$nonFactorCount 	= 0;
				$totalNonFactorATI 	= 0.0;
						
				while( !$rs->EOF() ){
					
					$orderCount			++;
					$totalATI 			+= $rs->fields( "totalATI" );
					$factorCount 		+= $rs->fields( "factor" );
					$totalFactorATI 	+= $rs->fields( "factorATI" );
					$nonFactorCount 	+= $rs->fields( "nonFactor" );
					$totalNonFactorATI 	+= $rs->fields( "nonFactorATI" );
					$lastDate 			= $rs->fields( "date" );
					
					$rs->MoveNext();
					
					if( $rs->EOF() || $lastDate != $rs->fields( "date" ) ){
					
						?>
						<tr>
			                <td style="text-align:right; border-right:1px solid #CBCBCB;" class="lefterCol">
			                <?php 
			                
			                	if( $lastDate == "0000-00" )
			                		echo "Date de départ<br /> théorique inconnue";
			                	else echo $month = ucfirst( strftime("%B %G", mktime( 0, 0, 0, substr( $lastDate, 5 ), 1, substr( $lastDate, 0, 4 ) ) ) ); 
			                	
			                ?>
			                </td>
			                <td style="text-align:right;"><?php echo $orderCount; ?></td>
			                <td style="text-align:right; border-right:1px solid #CBCBCB;"><?php echo Util::priceFormat( $totalATI ); ?></td>
			                <td style="text-align:right;"><?php echo $factorCount; ?></td>
			                <td style="text-align:right; border-right:1px solid #CBCBCB;"><?php echo Util::priceFormat( $totalFactorATI ); ?></td>
			                <td style="text-align:right;"><?php echo $nonFactorCount; ?></td>
			                <td style="text-align:right;" class="righterCol"><?php echo Util::priceFormat( $totalNonFactorATI ); ?></td>
						</tr>
						<?php
						
						$orderCount 		= 0;
						$totalATI 			= 0.0;
						$factorCount 		= 0;
						$totalFactorATI 	= 0.0;
						$nonFactorCount 	= 0;
						$totalNonFactorATI 	= 0.0;
						
					}
						
				}
				
			?>
			</tbody>
		</table>
	</div>
	</div>
	<?php
	            
}

//---------------------------------------------------------------------------------------------------------

?>