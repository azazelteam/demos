<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures services
 */

//------------------------------------------------------------------------------------------------

include_once( "../config/init.php" );
include_once( "../objects/DHTMLCalendar.php" );
include_once( "../objects/ProviderInvoice.php" );
include_once( "../catalog/admin_func.inc.php" );
include_once( "../script/global.fct.php" );

$Title = "Recherche de factures de service";

//------------------------------------------------------------------------------------------------

if( isset( $_POST[ "search" ] ) ){
	
	if( !User::getInstance()->getId() )
		exit();
		
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------------

include( "../templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<?php displayForm(); ?>
</div><!-- globalMainContent -->
<?php

include( "../templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){

	?>
	<script type="text/javascript" src="/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="/js/form.lib.js"></script>
	<script type="text/javascript" src="/js/ajax.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */

		/* ----------------------------------------------------------------------------------- */

		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			$( "#SearchForm" ).ajaxForm( options );

		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );

		}

		/* ----------------------------------------------------------------------------------- */
			
	/* ]]> */
	</script>
    <div class="mainContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
		<div class="topRight"></div>
		<div class="topLeft"></div>
		<div class="content">
            <form action="/accounting/search_provider_invoices.php" method="post" id="SearchForm">
  			<input type="hidden" name="search" value="1" />
            <div class="headTitle">
                    <p class="title">Rechercher une facture de service</p>
                    <div class="rightContainer">
                    <input type="radio" name="date_type" value="invoice_date" checked="checked" class="VCenteredWithText" /> Date de facture
                    <input type="radio" name="date_type" value="creation_date" class="VCenteredWithText" /> Date d'enregistrement
                        <input type="radio" name="date_type" value="deliv_payment" class="VCenteredWithText" /> Date d'échéance
                    </div>
                </div>
                <div class="subContent">
                	<!-- tableau choix de date -->
					<div class="tableContainer">
                    	<table class="dataTable dateChoiceTable">
							<tr>	
                            	<th class="filledCell">
                            		<input type="radio" name="interval_select" id="interval_select_since" value="1" checked="checked" class="VCenteredWithText"/> Depuis
                            	</th>
                                <td>
									<select name="minus_date_select" onchange="$( '#interval_select_since' ).attr('checked','checked');" class="fullWidth">
						            	<option value="0">Aujourd'hui</option>
						              	<option value="-1"><?php echo Dictionnary::translate("order_yesterday")  ?></option>
					              		<option value="-2"><?php echo Dictionnary::translate("order_2days")  ?></option>
						              	<option value="-7"><?php echo Dictionnary::translate("order_1week") ?></option>
						              	<option value="-14"><?php echo Dictionnary::translate("order_2weeks") ?></option>
						              	<option value="-30"><?php echo Dictionnary::translate("order_1month") ?></option>
						              	<option value="-60"><?php echo Dictionnary::translate("order_2months") ?></option>
						              	<option value="-90"><?php echo Dictionnary::translate("order_3months") ?></option>
						              	<option value="-180"><?php echo Dictionnary::translate("order_6months") ?></option>
						              	<option value="-365" selected="selected"><?php echo Dictionnary::translate("order_1year") ?></option>
						              	<option value="-730">Moins de 2 ans</option>
						              	<option value="-1095">Moins de 3 ans</option>
						              	<option value="all">Toutes</option>
				            		</select>
								</td>
								<th class="filledCell">
									<input type="radio" name="interval_select" id="interval_select_month" value="2" class="VCenteredWithText" /> Pour le mois de
								</th>
								<td><?php FStatisticsGenerateDateHTMLSelect( "$( '#interval_select_month' ).attr('checked','checked');" ); ?></td>
							</tr>
							<tr>	
								<th class="filledCell">
								    <input type="radio" name="interval_select" id="interval_select_between" value="3" class="VCenteredWithText" /> Entre le
								</th>
								<td>
					 				<input type="text" name="bt_start" id="bt_start" value="<?php echo date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ) ?>" class="calendarInput" />
					 				<?php DHTMLCalendar::calendar( "bt_start", true, "$( '#interval_select_between' ).attr('checked','checked');" ); ?>
								</td>
								<th class="filledCell">et le</th>
								<td>
					 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo date( "d-m-Y" ) ?>" class="calendarInput" />
					 				<?php DHTMLCalendar::calendar( "bt_stop", true, "$( '#interval_select_between' ).attr('checked','checked');" ); ?>
								</td>
							</tr>
                            <tr>
                                <td colspan="4" class="tableSeparator"></td>
                           	</tr>
                            <tr>
                            	<th class="filledCell">Facture n°</th>
                                <td><input type="text" size="10" name="idprovider_invoice"  value="" class="textInput" /></td>
                                <th class="filledCell">Prestataire</th>
                                <td>
                                <?php
                                    
                                    include_once( "../objects/XHTMLFactory.php" );
                                    XHTMLFactory::createSelectElement( "provider", "idprovider", "name", "name", false, 0, "Tous", "name=\"idprovider\"" );
                                    
                                ?>
                               	</td>
                            </tr>
                            <tr>
								<th class="filledCell">Montant HT</th>
                                <td><input type="text" size="10" name="total_amount_ht"  value="" class="textInput" /></td>
                                <th class="filledCell">Montant TTC</th>
                                <td><input type="text" size="10" name="total_amount"  value="" class="textInput" /></td>
                                </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton" style="text-align: center;" value="Rechercher" />
                    </div>
                </div>
                </form>
            </div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
	<div class="clear"></div>
	<div id="SearchResults"></div>
	<?php 

}

//------------------------------------------------------------------------------------------------

function search(){

	$now = getdate();

	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "invoice_date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
			
	switch ($interval_select){
			    
		case 1:
			//min
			if( $minus_date_select == "all" )
					$Min_Time = "0000-00-00";
			else 	$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$where  = "`provider_invoice`.$date_type >='$Min_Time' AND `provider_invoice`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$where  = "`provider_invoice`.$date_type >='$Min_Time' AND `provider_invoice`.$date_type <'$Max_Time' ";
	     	
			break;
			     
		case 3:
		default :
				
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$where  = "`provider_invoice`.$date_type >='$Min_Time' AND `provider_invoice`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !User::getInstance()->getValue( "admin" ) )
		$where .= " AND `provider_invoice`.iduser = '" . User::getInstance()->getId() . "'";
	else if( isset( $_POST[ "iduser" ] ) && intval( $_POST[ "iduser" ] ) )
		$where .= " AND provider_invoice.iduser = '" . intval( $_POST[ "iduser" ] ) . "'";
							
	if( isset( $_POST[ "idprovider" ] ) && intval( $_POST[ "idprovider" ] ) )
		$where .= " AND provider_invoice.idprovider = '" . intval( $_POST[ "idprovider" ] ) . "'";

	if ( isset( $_POST[ "idprovider_invoice" ] ) && strlen( $_POST[ "idprovider_invoice" ] ) )
		$where .= " AND provider_invoice.idprovider_invoice LIKE '%" . $_POST[ "idprovider_invoice" ] . "%'";

	if( isset( $_POST[ "total_amount_ht" ] ) && strlen( $_POST[ "total_amount_ht" ] ) )
		$where .= " AND provider_invoice.total_amount_ht = '" . Util::text2num( $_POST[ "total_amount_ht" ] ) . "'";
		
	if( isset( $_POST[ "total_amount" ] ) && strlen( $_POST[ "total_amount" ] ) )
		$where .= " AND provider_invoice.total_amount_ht + provider_invoice.vat_amount = '" . Util::text2num( $_POST[ "total_amount" ] ) . "'";
	
	$query = "
	SELECT provider_invoice.*, `user`.initial, provider.name
	FROM `user`, provider, provider_invoice
	WHERE $where
	AND `user`.iduser = provider_invoice.iduser
	AND provider.idprovider = provider_invoice.idprovider";

	$rs = DBUtil::query( $query );

	if( !$rs->RecordCount() ){
		
		?>
		<div class="mainContent fullWidthContent">
        	<div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            	<div class="headTitle">
                    <p class="title">
                    	Aucune facture n'a été trouvé !
                    </p>
					<div class="rightContainer">
					
					</div>
                </div>
       		</div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div> <!-- mainContent fullWidthContent -->
		<?php
		
		return;
		
	}
	
	?>
	<script type="text/javascript" src="/js/jquery/jquery.tablesorter.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		$( document ).ready( function(){ 
			
			$( "#ResultTable" ).tablesorter({
		      	
		      	headers: { 
		
					0: {  sorter:'text' },
					1: {  sorter:'text' },
					2: {  sorter:'text' },
					3: {  sorter:'date' },
					4: {  sorter:'date' },
					5: {  sorter:'date' },
					6: {  sorter:'currency' },
					7: {  sorter:'currency' },
					8: {  sorter:'currency' }
		      		
				} 
					        
			});

		});

	/* ]]> */
	</script>
	<style type="text/css">

		table.sortable th.header{ 
		
		    background-image: url(/images/small_sort.gif);     
		    cursor: pointer; 
		    font-weight: bold; 
		    background-repeat: no-repeat; 
		    background-position: 50% 85%; 
		    padding-left: 20px; 
		    border-right: 1px solid #dad9c7; 
		    margin-left: -1px; 
		    padding-bottom:15px;
		
		}
		
		table.sortable th.headerSortUp{
		 
		    background-image: url(/images/small_asc.gif); 
	
		} 
	
		table.sortable th.headerSortDown{ 
		
		    background-image: url(/images/small_desc.gif); 
		
		} 
		
	</style>
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div>
		<div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
        		<p class="title"><?php echo $rs->RecordCount(); ?> facture(s) trouvée(s)</p>
				<div class="rightContainer"></div>
			</div>
			<div class="subContent">
				<div class="tableHeaderLeft">
					<a href="#" class="goUpOrDown">
						Aller en bas de page
						<img src="/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a>
				</div>
				<div class="tableHeaderRight"></div>
				<!-- tableau résultats recherche -->
				<div class="resultTableContainer clear">
					<table class="dataTable resultTable sortable" id="ResultTable">
						<thead>
					       	<tr>
					       		<th>Com.</th>
					       		<th>Facture n°</th>
					       		<th>Prestataire</th>
					       		<th>Date de facture</th>
					       		<th>Date enregistrement</th>
					       		<th>Date d'échéance</th>
					       		<th>Total HT</th>
						      	<th>Total TTC</th>
						      	<th>Solde à payer</th>
					       	</tr>
				       	</thead>
				       	<tbody>
						<?php

							$total_amount_htSum = 0.0;
							$total_amountSum	= 0.0;
							
							while( !$rs->EOF() ){		
															
								?>
						        <tr class="blackText">
						        	<td class="lefterCol"><?php echo $rs->fields( "initial" ); ?></td> 
						        	<td>
						        		<a class="grasBack" href="/accounting/provider/provider_invoice.php?idprovider_invoice=<?php echo urlencode( $rs->fields( "idprovider_invoice" ) ); ?>&idprovider=<?php echo $rs->fields( "idprovider" ); ?>" onclick="window.open(this.href); return false;">
											<?php echo htmlentities( $rs->fields( "idprovider_invoice" ) ); ?>
										</a>
									</td>
						        	<td>
						        		<a class="grasBack" href="/accounting/provider/provider.php?idprovider=<?php echo $rs->fields( "idprovider" ); ?>" onclick="window.open(this.href); return false;">
						        			<?php echo htmlentities( $rs->fields( "name" ) ); ?>
						        		</a>
						        	</td>
									<td><?php echo Util::dateFormat( $rs->fields( "invoice_date" ) ); ?></td>
									<td><?php echo Util::dateFormat( substr( $rs->fields( "creation_date" ), 0, 10 ) ); ?></td>
									<td><?php echo Util::dateFormat( $rs->fields( "deliv_payment" ) ); ?></td>
									<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) ); ?></td>
									<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $rs->fields( "total_amount_ht" ) + $rs->fields( "vat_amount" ) ); ?></td>
									<td class="righterCol"></td>
								</tr>
								<?php
								
									$total_amount_htSum += $rs->fields( "total_amount_ht" );
									$total_amountSum 	+= $rs->fields( "total_amount_ht" ) + $rs->fields( "vat_amount" );
									
									$rs->MoveNext();			
							
							}
							
						?>
						</tbody>
				       	<tr>
				       		<th colspan="6" style="border:none;"></th>
					      	<th class="totalAmount"><?php echo Util::priceFormat( $total_amount_htSum ); ?></th>
					      	<th class="totalAmount"><?php echo Util::priceFormat( $total_amountSum ); ?></th>
					      	<th style="border:none;"></th>
				       	</tr>
	            	</table>
                </div>
                <a href="#" class="goUpOrDown">
                	Aller en haut de page
                    <img src="/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>
           	</div>
   		</div>
        <div class="bottomRight"></div>
        <div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
	<?php	

}

//--------------------------------------------------------------------------------

?>