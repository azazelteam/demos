<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement des avoirs fournisseurs
 */


include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierCredit.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

//------------------------------------------------------------------------------------------
/* traitement ajax 2m le retour */


$bs = $_GET["bs"];
$ids = $_GET["ids"];

$supplierinvoice = new SupplierInvoice ( $bs , $ids );

if( isset( $_POST[ "registerInvoice" ] ) && $_POST[ "registerInvoice" ] == "1" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" ); 
	
	$result = registerCredit();
	
	exit($result);
		
}

$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
if( !isset( $_GET[ "bs" ] ) || !isset ( $_GET[ "ids" ] ) ){
	
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}

displayInvoicesInfos();

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );


function displayInvoicesInfos(){
	
	global $bs, $ids, $GLOBAL_START_URL, $supplierinvoice;

	$bls = SupplierInvoice::getInvoicedDeliveryNotes( $bs , $ids );
	
	if( count( $bls )>1 ){
	
		$title = "Bons de livraisons concernés : ";
	
		for($i=0;$i<count($bls);$i++){
			if($i)
				$title.="-";
		
			$title.=$bls[$i];	
		}	
	}else{
		$title = "Bon de livraison concerné : ".$bls[0];
	}
	
	$os = SupplierInvoice::getSupplierInvoiceOS ( $bs , $ids );
	
	if( count( $os )>1 ){
	
		$title2 = "Commandes fournisseur concernées : ";
	
		for($i=0;$i<count($os);$i++){
			if($i)
				$title2.="-";
		
			$title2.=$os[$i];	
		}	
	}else{
		$title2 = "Commande fournisseur concernée : ".$os[0];
	}
	
	?>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
			/* <![CDATA[ */
				
				/* ----------------------------------------------------------------------------------- */
				
				$(document).ready(function() { 
					
					 var options = {
					 	
					 	dataType:  'xml',
						beforeSubmit:  preSubmitCallback,  // pre-submit callback 
						success:	   postSubmitCallback  // post-submit callback 
		  				
					};
					
					
				});
				
				/* ----------------------------------------------------------------------------------- */
				
				function preSubmitCallback( formData, jqForm, options ){
					
					
					$.blockUI({
						
						message: "Traitement en cours",
						css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
						fadeIn: 0, 
						fadeOut: 700
						
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function postSubmitCallback( responseXML, statusText ){ 
					
					$.unblockUI();
					
					if( statusText != 'success' ){
						
						alert( "Impossible d'effectuer le traitement" );
						return;
						
					}
					
					$.growlUI( '', 'Mise à jour effectuée avec succès' );
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function priceFormat( value ){
				
					var ret = parseFloat( value ).toFixed( 2 );
					var reg = new RegExp( "(\\.)", "g" );
					ret = ret.replace( reg, "," );
					
					return ret + ' &euro;';
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
	/* ]]> */
	</script>
	<style type="text/css">
			<!--
			div#global{
				min-width:0;
				width:auto;
			}

			-->
			</style>
	<div id="globalMainContent" style="width:770px;">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent" style="width:380px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Facture <?=getSupplierName($ids)?> n° <?=$bs?></p>
			</div>
			<div class="subContent">
			<div><?=$title2?><br/><?=$title?></div>
				<div class="resultTableContainer" style="margin-top:20px;">
					<table class="dataTable">
						<tr>
							<th class="filledCell">N° de facture fournisseur</th>
								<td colspan="2"><?php echo $supplierinvoice->get("billing_supplier") ?></td>
							</tr>
							<tr>
								<th class="filledCell">Date de facture</th>
								<td colspan="2"><?php echo usDate2eu( $supplierinvoice->get("Date") ) ?></td>
							</tr>
							<tr>
								<th class="filledCell">Date d'échéance</th>
								<td colspan="2"><?php echo usDate2eu( $supplierinvoice->get("deliv_payment") ) ?></td>
							</tr>
							<tr>
								<th class="filledCell">Mode de paiement</th>
								<td colspan="2"><?=DrawLift_modepayment( $supplierinvoice->get("idpayment"), false)?></td>
							</tr>
							<tr>
								<th class="filledCell">Délai de paiement</th>
								<td colspan="2"><?php echo DrawPaymentDelayList( $supplierinvoice->get("idpayment_delay"), false )?></td>
							</tr>
							<tr>
								<th class="filledCell">Montant TTC</th>
								<td colspan="2"><?php echo Util::priceFormat( $supplierinvoice->get("total_amount") ) ?></td>
							</tr>
							<?
							if( $supplierinvoice->get("rebate_value") > 0) {
							?>
							<tr>
								<th class="filledCell">Escompte</th>
								<td><?php echo Util::rateFormat( SupplierInvoice::getRebateRate( $supplierinvoice->get("billing_supplier"), $supplierinvoice->get("idsupplier") ) ) ?></td>
								<td><?php echo Util::priceFormat( SupplierInvoice::getRebateAmount( $supplierinvoice->get("billing_supplier"), $supplierinvoice->get("idsupplier") ) ) ?></td>
							</tr>
							<?php } ?>
							<tr>
								<th class="filledCell">Frais de port HT</th>
								<td colspan="2"><?php echo Util::priceFormat( $supplierinvoice->get("total_charge_ht") ) ?></td>
							</tr>
							<?
							if( $supplierinvoice->get("down_payment_value") > 0) {
							?>
								<tr>
									<th class="filledCell">Acompte</th>
									<td><?php echo Util::rateFormat( SupplierInvoice::getDownPaymentRate( $supplierinvoice->get("billing_supplier"),$supplierinvoice->get("idsupplier") ) ) ?></td>
									<td><?php echo Util::priceFormat( SupplierInvoice::getDownPaymentAmount( $supplierinvoice->get("billing_supplier"),$supplierinvoice->get("idsupplier") ) ) ?></td>
							</tr>
							<?php } ?>
						</table>
					</div>
				</form>	
	</div>
	</div>
	<div class="bottomRight"></div><div class="bottomLeft"></div>
						</div>	
	<div class="mainContent" style="float:right; width:380px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Enregistrement avoir <?=getSupplierName($ids)?></p>
			</div>
			<div class="subContent">
				<?displayCredit();?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<?
}

//---------------------------------------------------------------------------------------------------------------------
function displayCredit(){
	
	global $bs, $ids, $supplierinvoice, $GLOBAL_START_URL;
		
	?>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var optionsInvoices = {
			 	
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postInvoiceSubmitCallback  // post-submit callback 
  				
			};
			
			$('#frm').ajaxForm( optionsInvoices );
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function postInvoiceSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' ){
				
				alert( "Impossible d'effectuer le traitement" );
				return;
				
			}
			
			if( responseText == "0" ){
				$.growlUI( '', 'Avoir enregistré' );
				window.opener.goForm();
				setTimeout('self.close();',2000); 
			}else
				$.growlUI( '', responseText );
			
		}
	/* ]]> */
	</script>
	<style type="text/css">
	
		div.mainContent div.content div.subContent table.dataTable input.price,
		div.mainContent div.content div.subContent table.dataTable input.percentage{ text-align:right; width:50px; }
			
	</style>
		<form id="frm" method="POST" action="<?=$GLOBAL_START_URL ?>/accounting/register_supplier_credit.php?bs=<?=$bs?>&amp;ids=<?=$ids?>">					
			<input type="hidden" name="registerInvoice" value="1" />
			<div class="resultTableContainer" style="margin-top:20px;">
				<table class="dataTable">
					<tr>
						<th class="filledCell">N° avoir <span class="asterix">*</span></th>
						<td colspan="2"><input type="text" name="idcredit_supplier" class="textInput" /></td>
					</tr>
					<tr>
						<th class="filledCell">Date de l'avoir <span class="asterix">*</span></th>
						<td colspan="2">
							<input type="text" name="Date" id="Date" class="calendarInput" value="" />
							<?php DHTMLCalendar::calendar( "Date" ); ?>
						</td>
					</tr>
                    
                    <tr>
						<th class="filledCell">Montant HT <span class="asterix">*</span></th>
						<td colspan="2"><input type="text" id="total_amount_ht" name="total_amount_ht" class="textInput price" value="" /> &euro;</td>
					</tr>
                    
                    
					<tr>
						<th class="filledCell">Montant TTC <span class="asterix">*</span></th>
						<td colspan="2"><input type="text" id="amount" name="amount" class="textInput price" value="" /> &euro;</td>
					</tr>
					<tr>
						<th class="filledCell">Objet de l'avoir</th>
						<td colspan="2"><textarea name="object" style="width:200px;height:100px;"><?php
							
							if (strlen($supplierinvoice->get("credit_request_object"))>0)
								echo stripslashes($supplierinvoice->get("credit_request_object"));
							
						?></textarea></td>
					</tr>
					<tr>
						<th class="filledCell">Avoir document</th>
						<td colspan="2">
							<input type="file" class="calendarInput" name="credit_supplier_doc" />
						</td>
					</tr>
					</table>
					</div>
					<div style="float:left; margin-top:10px;">Les champs marqués par <span class="asterix">*</span> sont obligatoires</div>
					<div class="submitButtonContainer">
						<input type="submit" name="register" value="Enregistrer" class="blueButton" />
					</div>
				</form>	
	<?
}




function registerCredit(){
	global $supplierinvoice;
	
	$bs = $_GET["bs"];
	$ids = $_GET["ids"];
	
	//------------------------------------------------------------------------------------------------
	//Traitement du formulaire
	
	$error = 0;
	$Msgerror = '';
	
		
	//Test des champs postés
	$fields = array ( "idcredit_supplier" , "Date" , "amount"  );
	
	for ($i=0;$i<count($fields);$i++){
		if( empty( $_POST[ $fields[$i] ] ) ){
				$error = 1;
		}
	}
	
	if ( $error == 1 ){
		$Msgerror = "Tous les champs doivent être remplis";	
		return $Msgerror;
	}else{
		//Vérification du numéro de l'avoir fournisseur pour voir si il n'existe pas
		$idcredit_supplier = $_POST["idcredit_supplier"];
		
		$query = "SELECT idcredit_supplier FROM credit_supplier WHERE idcredit_supplier='$idcredit_supplier' AND idsupplier=$ids";
		$rs = DBUtil::query($query);
		
		if($rs === false)
			die("Impossible de vérifier le numéro de l'avoir fournisseur");
			
		if($rs->RecordCount()>0){
			$Msgerror = "Ce numéro d'avoir fournisseur existe déjà";
			return $Msgerror;
		}else{
			//Tous les champs sont remplis, test des champs numériques
			if( !preg_match( "/^[0-9\., ]+\$/i", $_POST[ "total_amount_ht" ] ) ) {
				$error = 1;
				$Msgerror = "Le champ montant HT doit être un nombre";
				return $Msgerror;		
			}
			else if( !preg_match( "/^[0-9\., ]+\$/i", $_POST[ "amount" ] ) ) {
				$error = 1;
				$Msgerror = "Le champ montant TTC doit être un nombre";
				return $Msgerror;		
			}
		}
	}
	
	if( $error == 0 ) {
		
		//Toutes les données sont ok, on peut traiter l'enregistrement de l'avoir

		$Date = $_POST[ "Date" ];
		
		$amount = Util::text2num( $_POST[ "amount" ] );
		$total_amount_ht = Util::text2num( $_POST[ "total_amount_ht" ] );
		$total_amount = Util::text2num( $_POST[ "amount" ] );
		$object = $_POST[ "object" ];

		//On créé un nouvel avoir fournisseur
		$creditsupplier = &SupplierCredit::createSupplierCredit( $idcredit_supplier , $bs, $ids );	
		
		//PJs
		if( isset( $_FILES[ "credit_supplier_doc" ] ) && is_uploaded_file( $_FILES[ "credit_supplier_doc" ][ "tmp_name" ] ) ){
	
			$YearFile = date("Y");
			$DateFile = date("d-m");
				
			// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
			// pas besoin d'uploader 15 fois
		
			$fieldname = "credit_supplier_doc";
		
			$target_dir = DOCUMENT_ROOT . "/data/credit_supplier/$YearFile/$idsupplier/";
			$prefixe = "Avoir";
			
			$name_file = $_FILES[ $fieldname ][ 'name' ];
			$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
			
			$extension = substr( $name_file , strrpos( $name_file , "." ) );
			
			if( ( $extension != '.doc' ) && ( $extension != '.jpg' ) && ( $extension != '.xls' ) && ( $extension != '.pdf' ) && ( $extension != '.DOC' ) && ( $extension != '.JPG' ) && ( $extension != '.XLS' ) && ( $extension != '.PDF' ) ){
	   			exit( "Format de fichier invalide" );
			}
			
			// on copie le fichier dans le dossier de destination
	    	$dest = $target_dir . $prefixe . "_" . $DateFile . "_" . $name_file;
			$newdoc = $YearFile . "/" . $idsupplier . "/" . $prefixe . "_" . $DateFile . "_" . $name_file;
		
			//billing_supplier existe
			//rep année	
			
			if( !file_exists( DOCUMENT_ROOT . '/data/credit_supplier/' . $YearFile ) )
				if( mkdir( DOCUMENT_ROOT . '/data/credit_supplier/' . $YearFile ) === false )
					exit( "Impossible de créer le répertoire '/credit_supplier/$YearFile'" );
				
			//if( chmod( DOCUMENT_ROOT . '/data/credit_supplier/' . $YearFile , 0755 ) === false )
				//exit( "Impossible de modifier les droits du répertoire '/credit_supplier/$YearFile'" );	
				
			if( !file_exists( DOCUMENT_ROOT . '/data/credit_supplier/' . $YearFile . '/' . $idsupplier ) )
				if( mkdir( DOCUMENT_ROOT . '/data/credit_supplier/' . $YearFile . '/' . $idsupplier ) === false )
					exit( "Impossible de créer le répertoire '/credit_supplier/$YearFile/$idsupplier'" );
				
			//if( chmod( DOCUMENT_ROOT . '/data/credit_supplier/' . $YearFile . '/' . $idsupplier , 0755 ) === false )
				//exit( "Impossible de modifier les droits du répertoire '/credit_supplier/$YearFile/$idsupplier'" );	
	    	
	    	if( !move_uploaded_file( $tmp_file , $dest ) )
	        	exit( "Impossible de déplacer le fichier uploadé vers sa destination $dest" );
		
			//if( chmod( $dest , 0755 ) === false )
				//exit( "Impossible de modifier les droits du fichier $dest" );	

			$creditsupplier->set( $fieldname , $newdoc );
		}
				
		//On affecte les valeurs
		$creditsupplier->set( "Date" , Util::dateFormat($Date, "Y-m-d") );
		$creditsupplier->set( "total_amount_ht" , $total_amount_ht );
		$creditsupplier->set( "total_amount" , $total_amount );
		$creditsupplier->set( "amount" , $amount );
		$creditsupplier->set( "object" , $object );
		
		//On sauvegarde
		$creditsupplier->save();
		
		return "0";
	
	}   

}
  	

//------------------------------------------------------------------------------------------------------
function getSupplierName( $idsupplier ){
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du fournisseur" );
		
	return $rs->fields( "name" );
	
}

?>