<?php
 /**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonctions utiles lors de la facturation client
 */

$DEBUG = "log";

include_once( "../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/Customer.php" );
include_once( "$GLOBAL_START_PATH/objects/GroupingCatalog.php" );

include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

/* identification requise */

if( !User::getInstance()->getId() ){
	
	include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	exit();
	
}

if( !isset( $_REQUEST[ "IdInvoice" ] ) )
	die( "Paramètre absent" );
	
//verification si c'est possible d'afficher quelque chose
if( isset( $_REQUEST['IdInvoice'])){
// ------- Mise à jour de l'id gestion des Index  #1161
	//compte pour voir si ya une cde de ce num
	$idordera=$_REQUEST['IdInvoice'] ;
	$reqverif="SELECT COUNT(idbilling_buyer) as comptage FROM `billing_buyer` WHERE idbilling_buyer='$idordera'";
	
	$baseverif = DBUtil::getConnection();
	$resverif = $baseverif->Execute( $reqverif );

	if($resverif->fields("comptage")==0){
		$html="<div style='	text-align:center; color:#FF0000; font-size:18px; font-weight:bold;'>".Dictionnary::translate("this_number_dont_exist")."</div>";
		include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
		die( $html );
	}
}

//Adresse de facturation
$b_company_required = DBUtil::getParameterAdmin("b_company_required");
$b_company_required = $b_company_required ? "<span class=\"asterix\">*</span>" : "";

$b_title_required = DBUtil::getParameterAdmin("b_title_required");
$b_title_required = $b_title_required ? "<span class=\"asterix\">*</span>" : "";

$b_lastname_required = DBUtil::getParameterAdmin("b_lastname_required");
$b_lastname_required = $b_lastname_required ? "<span class=\"asterix\">*</span>" : "";

$b_firstname_required = DBUtil::getParameterAdmin("b_firstname_required");
$b_firstname_required = $b_firstname_required ? "<span class=\"asterix\">*</span>" : "";

$b_address_required = DBUtil::getParameterAdmin("b_address_required");
$b_address_required = $b_address_required ? "<span class=\"asterix\">*</span>" : "";

$b_address2_required = DBUtil::getParameterAdmin("b_address2_required");
$b_address2_required = $b_address2_required ? "<span class=\"asterix\">*</span>" : "";

$b_zipcode_required = DBUtil::getParameterAdmin("b_zipcode_required");
$b_zipcode_required = $b_zipcode_required ? "<span class=\"asterix\">*</span>" : "";

$b_city_required = DBUtil::getParameterAdmin("b_city_required");
$b_city_required = $b_city_required ? "<span class=\"asterix\">*</span>" : "";

$b_state_required = DBUtil::getParameterAdmin("b_state_required");
$b_state_required = $b_state_required ? "<span class=\"asterix\">*</span>" : "";

$b_phonenumber_required = DBUtil::getParameterAdmin("b_phonenumber_required");
$b_phonenumber_required = $b_phonenumber_required ? "<span class=\"asterix\">*</span>" : "";

$b_faxnumber_required = DBUtil::getParameterAdmin("b_faxnumber_required");
$b_faxnumber_required = $b_faxnumber_required ? "<span class=\"asterix\">*</span>" : "";

//Délai général commande
$global_deliv = DBUtil::getParameterAdmin("view_delay_delivery");

//---------------------------------------------------------------------------------------------
$IdOrder =  $_REQUEST[ "IdInvoice" ];

/**
 * Modif facture impayable
 */
 // ------- Mise à jour de l'id gestion des Index  #1161
if( isset($_POST[ "update_litigious" ]) && $_POST[ "update_litigious" ]==1 ){
	//maj de la facture
	$dateImpaya = Util::dateFormatUs($_POST[ "litigation_date" ], false);
	$commentImpaya = $_POST[ "litigation_comment" ];
	DBUtil::query("UPDATE billing_buyer 
					SET litigious=1,
					litigation_date = '$dateImpaya',
					litigation_comment = '$commentImpaya'
					WHERE idbilling_buyer ='".$_GET['IdInvoice']."'");
}

//------------------------------Update invoice title------------------------------------------

if (isset($_POST["title_offer"]))
{

$qr = "
			UPDATE `billing_buyer`
			set title_offer = '". $_POST["title_offer"] ."'
			WHERE idbilling_buyer ='". $_GET['IdInvoice'] ."'
			 ";

	$res =& DBUtil::query( $qr );
}
//----------------------------------------------------------------------------------------------

/**
 * Chargement de la facture
 */

$Order = new Invoice( $IdOrder );

//------------------------------------------------------------------------------------

//vérification des droits de consultation

$iduser = User::getInstance()->getId();

if( !$Order->allowRead() )
	die( Dictionnary::translate( "gest_com_access_denied" ) );

//------------------------------------------------------------------------------------
//traitement du formulaire

if( isset( $_POST[ "ModifyInvoice" ] ) ){
	
	include_once( "$GLOBAL_START_PATH/accounting/invoice_parseform.php" );
	ModifyInvoice( $Order );
		
}

//------------------------------------------------------------------------------------
$Str_status = $Order->get('status');

include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );

if( $Str_status == "Invoiced" && Payment::checkRegulations( $IdOrder ) )
	$status_plus = "<br />Paiement réceptionné";
elseif( DBUtil::getDBValue( "factor_refusal_date", "billing_buyer", "idbilling_buyer", $IdOrder ) != "0000-00-00" )
	$status_plus = "<br />Définancée par le factor";
elseif( $Str_status == "Invoiced" && DBUtil::getDBValue( "factor_send_date", "billing_buyer", "idbilling_buyer", $IdOrder ) != "0000-00-00" )
	$status_plus = "<br />Financée par le factor";
else
	$status_plus = "";
		
$company = $Order->getCustomer()->get("company");

$Title = 'Facture n°'.$IdOrder.' - '.$company;

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$relaunched = DBUtil::query(

	"SELECT COUNT(*) AS relaunchCount
	FROM `billing_buyer`
	WHERE idbuyer = '" . $Order->getCustomer()->getId() . "'
	AND (
		relaunch_date_1 <> '0000-00-00'
		OR relaunch_date_2 <> '0000-00-00'
		OR relaunch_date_3 <> '0000-00-00'
	)"

)->fields( "relaunchCount" ) > 0;

/**
 * Affichage du formulaire d'edition
 */
 
?>
<script type="text/javascript" language="javascript">
<!--

// PopUp pour les infos des remises au volume
function PopupVolume(ref,buy)
{
	postop = (self.screen.height-150)/2;
	posleft = (self.screen.width-300)/2;

	window.open('volume_price.php?r='+ref+'&b='+buy, '_blank', 'top=' + postop + ',left=' + posleft + ',width=300,height=150,resizable,scrollbars=yes,status=0');
	
}

// PopUp pour les infos du stock
function PopupStock(ref)
{
	postop = (self.screen.height-400)/2;
	posleft = (self.screen.width-400)/2;

	window.open('popup_stock.htm.php?r='+ref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=400,height=400,resizable,scrollbars=yes,status=0');
	
}

function isNumeric( text ){

	var validChars = '0123456789';
	var char;
	
	var i = 0;
	while( i < text.length ){
   
   		char = text.charAt( i );
   		
   		if ( validChars.indexOf( char ) == -1 ) 
   			return false;
   			
		i++;
   	
	}
   	
   	return true;

}

// -->
</script>
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<div class="centerMax">
<?php 
//---------------------------------------------------------------------------------------------

$mail_buyer = $Order->getCustomer()->getContact()->get( "mail" );

if(!empty( $mail_buyer ) ){
	$vide=0;
}else{
	$vide=1;
}	
?>
<a name="topPage"></a>
<?php
//Buyer

/**
 * Selection du mode d'affichage :
 * 	- B2B
 * 	- B2C
 * */
/*if( B2B_STRATEGY )*/
	 include( "$GLOBAL_START_PATH/templates/accounting/invoice/buyer.htm.php" );
//else include( "$GLOBAL_START_PATH/templates/accounting/invoice/B2C/buyer_b2c.htm.php" );

//---------------------------------------------------------------------------------------------


$hasLot = false;
$i = 0;
while( !$hasLot && $i < $Order->getItemCount() ){
	
	if( $Order->getItemAt( $i)->get( "lot" ) > 1 )
		$hasLot = true;
		
	$i++;
	
}
	
?>

<form action="com_admin_invoice.php?IdInvoice=<?php echo $IdOrder ?>" method="post" name='frm' enctype="multipart/form-data">
	<input type="hidden" name="sendmail" id="sendmail" value="0" />
	<input type="hidden" name="sendmailcancel" id="sendmailcancel" value="0" />
	<input type="hidden" name="sendmailpaiement" id="sendmailpaiement" value="0" />
	<input type="hidden" name="ModifyInvoice" id="ModifyInvoice" value="1" />
	<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>" />
	<input type="hidden" name="terminate" value="0" />
	<input type="hidden" name="DdeProForma" value="0" />
	<input type="hidden" name="idbuyer" value="<?php echo $Order->get( "idbuyer" ) ?>" />
<?php 

//---------------------------------------------------------------------------------------------
	
	$total_discount_rate = 0.0 + $Order->get( "total_discount" );
	$total_discount_amount = 0.0 + $Order->get( "total_discount_amount" );
	
	$ttc = $Order->getTotalATI() - $Order->getChargesATI() - $Order->get( "billing_amount" );
	
	//HT sans frais de port
	$ht = 0.0;
	$it = $Order->getItems()->iterator();
	while( $it->hasNext() )
		$ht += $it->next()->getTotalET();
	$ht -= $Order->get( "total_discount_amount" );
		
	$total_charge_ht = $Order->get( "total_charge_ht" );
	$total_estimate_ht = $ht + $total_charge_ht;
	
	$billing_rate = $Order->get( "billing_rate" );
	$billing_amount = $Order->get( "billing_amount" );
	$total_ht_wb = $total_estimate_ht - $billing_amount;
	
	$down_payment_rate = $Order->getDownPaymentRate();
	$down_payment_amount = $Order->getDownPaymentAmount();
	
	$total_ttc_wb = $Order->getTotalATI();
	$total_ttc_wbi = $Order->getNetBill();
	
	$Factor = $Order->get( "factor" );
	
	//Marge nette facture
		
	$Marge_Nette 		= $Order->get( "net_margin_rate" );;
	$Marge_Nette_Amount = $Order->get( "net_margin_amount" );;


//Infos commande

//if( B2B_STRATEGY ){
	
	include( "$GLOBAL_START_PATH/templates/accounting/invoice/invoice.htm.php" );
	include( "$GLOBAL_START_PATH/templates/accounting/invoice/amounts.htm.php" );
	include( "$GLOBAL_START_PATH/templates/accounting/invoice/pay.htm.php" );
	
/*}
else{

	include( "$GLOBAL_START_PATH/templates/accounting/invoice/B2C/invoice_b2b.htm.php" );
	include( "$GLOBAL_START_PATH/templates/accounting/invoice/B2C/amounts_b2c.htm.php" );
	include( "$GLOBAL_START_PATH/templates/accounting/invoice/B2C/pay_b2c.htm.php" );
	
}*/

//---------------------------------------------------------------------------------------------

//Paiements
include( "$GLOBAL_START_PATH/templates/accounting/invoice/regulation.php" );


//---------------------------------------------------------------------------------------------

	?>
	<div class="fullWidthContent submitZone">
		<div style="float:right;">
			<input type="hidden" name="IdOrder" value="<?php echo $IdOrder ?>">
			<!--
			<?php
		
				if( $Str_status == "Invoiced" || $Order->getBalanceOutstanding() > 0 ){?>
					<input type="submit" name="Billing2Pay" class="blueButton" style="text-align:center; cursor:pointer; margin-top:10px;" value="Créér le paiement" onclick="javascript:return return confirm('<?php  echo Dictionnary::translate("gest_com_confirm_payement"); ?> ?');" />
				<?php }
			
			?>
			-->
			<input type="submit" class="blueButton" style="text-align:center; cursor:pointer; margin:0 10px 0 0;" value="<?php echo Dictionnary::translate("gest_com_save"); ?>" name="SaveOrder"/>
		</div>
	</div>
</form>
<a name="bottomPage"></a>
</div><!-- GlobalMainContent --> 

<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
//------------------------------------------------------------------------------------------------

function anchor( $name, $hasIndex = false ){

	if( $hasIndex ){
		
		foreach( $_POST as $key => $value ){
		
			$pos = strpos( $key, $name );	
			if( $pos !== false && !$pos ){
				
				echo "<a name=\"lastaction\"></a>";
				return;	
				
			}
			
		}
		
	}
	else if( isset( $_POST[ $name ] ) || isset( $_POST[ $name . "_x" ] ) )	
		echo "<a name=\"lastaction\"></a>";
	
}

//------------------------------------------------------------------------------------------------

function IsVolumeDiscounted($ref){
	
	global $Order;
	
	$bdd=DBUtil::getConnection();
	
	$buyerfam = $Order->getCustomer()->get('idcustomer_profile');
	$today = date('Y-m-d');
	
	//On cherche dans la base de données si cette référence possède actuellement une remise au volume
	$Query="SELECT rate FROM multiprice_family WHERE idcustomer_profile='$buyerfam' and reference='$ref' and begin_date<='$today' and end_date>='$today'";
	$rs = $bdd->Execute($Query);
	
	if($rs->RecordCount()>0){
		//Il y a des remises au volume en ce moment pour cette référence
		return true;
	}else{
		return false;
	}
}

//------------------------------------------------------------------------------------------------
function uploadPJ($id){
	
	global 
			$GLOBAL_START_PATH,$Order;
		
	$YearFile = date("Y");
	$DateFile = date("d-m");
	
	$newdoc='vide';
	$filestoupload=array();
	
	//On remplit le tableau avec les fichiers uploadés
	if( isset( $_FILES[ "order_doc" ] ) && is_uploaded_file( $_FILES[ "order_doc" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"order_doc","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"PJ");
	}
	if( isset( $_FILES[ "offer_signed" ] ) && is_uploaded_file( $_FILES[ "offer_signed" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"offer_signed","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"OS");
	}
	if( isset( $_FILES[ "supplier_offer" ] ) && is_uploaded_file( $_FILES[ "supplier_offer" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"supplier_offer","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"SO");
	}
	if( isset( $_FILES[ "supplier_doc" ] ) && is_uploaded_file( $_FILES[ "supplier_doc" ][ "tmp_name" ] ) ){
		$filestoupload[]=array("fieldname"=>"supplier_doc","target"=>"$GLOBAL_START_PATH/data/order/$YearFile/","prefixe"=>"SD");
	}
	
	
	//On parcours le tableau
	foreach($filestoupload as $upload){
		$fieldname = $upload["fieldname"];
	
		$target_dir = $upload["target"];
		$prefixe = $upload["prefixe"];
		
		$name_file = $_FILES[$fieldname]['name'];
		$tmp_file = $_FILES[$fieldname]['tmp_name'];
		
		$extension = substr($name_file, strrpos($name_file,"."));
	
		if(($extension!='.doc')&&($extension!='.jpg')&&($extension!='.xls')&&($extension!='.pdf')){
   			die("Le format de la documentation doit être .doc, .xls, .pdf, .jpg");
			return false;
		}
	
		// on copie le fichier dans le dossier de destination
    	$dest=$target_dir.$prefixe."_".$DateFile."_".$id.$extension;
		$newdoc= $YearFile."/".$prefixe."_".$DateFile."_".$id.$extension;
	
	
		if( !file_exists( $target_dir ) )
			if( mkdir( $target_dir ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $target_dir, 0755 ) === false )
		//	die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
	
		//On update dans la base le nom du fichier
		$requeteaexecuter = "UPDATE `order` SET $fieldname = '$newdoc' WHERE idorder = $id LIMIT 1";
		
		$connexionalabasededonnees = &DBUtil::getConnection();
		$resultatdelarequete = $connexionalabasededonnees->Execute( $requeteaexecuter );
	
		if( $resultatdelarequete === false )
			die( Dictionnary::translate("gest_com_impossible_join_link") );
		
		$Order->set( $fieldname, $newdoc );	
	
	}
	
}

//---------------------------------------------------------------------------------------------

/**
 * Recherche les options associées à un article
 *
 */
 
function GetOptions($idart){

	//On récupère l'IdProduct de l'article
	$Query = "SELECT idproduct FROM detail WHERE idarticle=$idart";
	$rsp =& DBUtil::query($Query);
	
	$IdProduct = $rsp->fields("idproduct");
	
	$SQL_Query = "select associat_product from associat_product where idproduct='$IdProduct' order by displayorder";
	$rs =& DBUtil::query($SQL_Query);
	
	if($rs->RecordCount()>0){
		return true;
	}else{
		return false;
	}
}

//---------------------------------------------------------------------------------------------

/**
 * Recherche les produits similaires
 *
 */
function GetSimilar($idart){

	//On récupère l'IdProduct de l'article
	$Query = "SELECT idproduct FROM detail WHERE idarticle=$idart";
	$rsp =& DBUtil::query($Query);
	
	$IdProduct = $rsp->fields("idproduct");

	$SQL_Query = "select idproduct_similar_1, idproduct_similar_2, idproduct_similar_3 from product where idproduct='$IdProduct'";
	$rs =& DBUtil::query($SQL_Query);
	
	
	$sim1 = $rs->fields("idproduct_similar_1");
	$sim2 = $rs->fields("idproduct_similar_2");
	$sim3 = $rs->fields("idproduct_similar_3");
	
	
	if(($sim1>0) || ($sim2>0) || ($sim3>0)){
		return true;
	}else{
		return false;
	}

}

//------------------------------------------------------------------------------------------------

/**
 * Retourne la socité du fournisseur en fontion d'un idsupplier
 */

function GetSupName($idsup){
	
	//On récupère l'IdProduct de l'article
	$Query = "SELECT name FROM supplier WHERE idsupplier=$idsup";
	$rs =& DBUtil::query($Query);
	
	$Name = $rs->fields("name");
	
	return $Name;
	
}

//----------------------------------------------------------------------------

function getPaymentTxt( $idpayment ){
	
	if( empty( $idpayment ) )
		return "";
		
	$lang = User::getInstance()->getLang();
	$db = DBUtil::getConnection();
	
	$query = "SELECT name$lang AS name FROM payment WHERE idpayment = $idpayment LIMIT 1";
	$rs = $db->Execute( $query );

	if( $rs === false || !$rs->RecordCount() )
		return "";
		
	return $rs->fields( "name" );
	
}
?>