﻿<?php

/**
 * Package comptabilité
 * Etats des comptes
 * Cherche les informations sur un client ou montant dans la liste des opérations du factor et les relevés de comptes
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire pour gérer le plan comptable
 */

include_once( "../objects/classes.php" );
include_once( "../script/global.fct.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	//searchBankAccount();
	searchBankAccountversion2();//Since astelos edit 5 google doc by behzad
	exit();
		
}

//------------------------------------------------------------------------------------------------
/* Import relevé de compte Crédit Mutuel */

if( isset( $_FILES[ "importBank" ] ) ){
	
	$importsBank =& importBank();
	
	$imported_count =0;
	foreach( $importsBank as $eacht ){ 
		if($eacht[ "is_import" ] == 'Oui')
			$imported_count++;
	}
	
	
?>
				<div class="subTitleContainer">
					<p class="subTitle">Résultat de l'import de l'extrait bancaire</p>
				</div>
				<div class="subContent">
				<?php
					
					if( $imported_count == 0 )
						echo "Aucune nouvelle ligne importée";
					elseif( $imported_count == 1 )
						echo "1 nouvelle ligne importée";
					else
						echo $imported_count . " nouvelles lignes importées";
					
					if( count( $importsBank ) ){
						
					?>
					<div class="tableContainer resultTableContainer">
						<table class="dataTable resultTable">
							<thead>
								<tr>
                                	<th>Code banque</th>
									<th>Opération</th>
									<th>Date de l'opération</th>
									<th>Date de valeur</th>
									<th>Montant</th>
									<th>Solde</th>
									<th>Ajoutée</th>
								</tr>
							</thead>
							<?php foreach( $importsBank as $import ){ 
							
							$qr1 = "SELECT code 
									FROM bank_company
									WHERE idbank_company =". $import[ "code_bank" ] ."
									";
			
							$rs1 =& DBUtil::query( $qr1 );
			
							$code_bank = $rs1->fields("code");
			
							?>
								<tr>
                                	<td class="lefterCol"><?php echo $code_bank  ?></td>
									<td class="lefterCol"><?php echo $import[ "heading" ] ?></td>
									<td><?php echo Util::dateFormatEu( $import[ "operation_date" ] ) ?></td>
									<td><?php echo Util::dateFormatEu( $import[ "value_date" ] ) ?></td>
									<td style="text-align:right; white-space:nowrap;"><?php echo $import[ "amount_type" ] == "credit" ?  ( $import[ "amount" ] ) : ( $import[ "amount" ] ) ?> ¤</td>
									<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo ( $import[ "balance" ] ) ?> ¤</td>
									<td class="lefterCol"><?php echo $import[ "is_import" ] ?></td>
								</tr>
							<?php } ?>
							<tbody>
							</tbody>
						</table>
					</div>
					<?php
						
					}
					
				?>
				</div>
<?php
	
	exit();
	
}

//------------------------------------------------------------------------------------------

$Title = "Liste des extraits de compte";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent" style="width:90%;">
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Relevés Banque Crédit Mutuel</p>
			</div>
			<div class="subContent" id="container">
				<ul class="menu">
					<li class="item">
						<a href="#search">Recherche</a>
					</li>
					<li class="item">
						<a href="#bank">Import extrait banque</a>
					</li>
				</ul>
				<div id="search"><?php displaySearchForm(); ?></div>
				<div id="bank"><?php displayBankImport(); ?></div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<style type="text/css">
/* <![CDATA[ */
	
	.ui-tabs .ui-tabs-nav{
		width:auto;
		float:right;
		margin-top:-27px;
		*margin-top:-20px;
	}

/* ]]> */
</style>
<script type="text/javascript">
/* <![CDATA[ */
	
	//onglets
	
	$(document).ready(function(){
		
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' })
		
	});
	
/* ]]> */
</script>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displaySearchForm(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	$formFields = array(
		
		$fieldName = "date_type"	=> "value_date",
		"interval_select"			=> 2,
		"minus_date_select"			=> $defaultValue = "-365",
		"name"						=> "",
		"heading"					=> "",
		"amount"					=> "",
		"page"						=> 1,
		"orderByBank"				=> "value_date DESC"
		
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
?>
				<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
				<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
				<script type="text/javascript">
				/* <![CDATA[ */
					
					/* ----------------------------------------------------------------------------------- */
					
					$(document).ready(function() { 
						
						 var options = {
						 	
							beforeSubmit:	preSubmitCallback,  // pre-submit callback 
							success:		postSubmitCallback  // post-submit callback
			  				
						};
						
						$('#SearchForm').ajaxForm( options );
						$('#BankForm').ajaxForm({ beforeSubmit: preSubmitImportCallback, success: bankPostUpload });
						
					});
					
					/* ----------------------------------------------------------------------------------- */
					
					function preSubmitCallback( formData, jqForm, options ){
						
						
						$.blockUI({
				
							message: "Recherche en cours",
							css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
							fadeIn: 0, 
							fadeOut: 700
								
						}); 
						
						$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
						
					}
					
					/* ----------------------------------------------------------------------------------- */
					
					function preSubmitImportCallback( formData, jqForm, options ){
						
						
						$.blockUI({
				
							message: "Téléchargement de l'extrait en cours",
							css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
							fadeIn: 0, 
							fadeOut: 700
								
						}); 
						
						$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
						
					}
					
					/* ----------------------------------------------------------------------------------- */
					
					function postSubmitCallback( responseText, statusText ){ 
						
						$.unblockUI();
						
						if( statusText != 'success' || responseText == '0' ){
							
							$.growlUI( '', 'Impossible d\'effectuer la recherche' );
							return;
							
						}
						
						$('#SearchResults').html( responseText );
						
					}
					
					/* ----------------------------------------------------------------------------------- */
					
					function bankPostUpload( responseText, statusText ){ 
						
						$.unblockUI();
						
						if( statusText != 'success' || responseText == '0' ){
							
							$.growlUI( '', 'Impossible de charger le fichier' );
							return;
							
						}
						
						$('#BankUploadResults').html( responseText );
						
					}
					
					/* --------------------------------------------------------------------------------------- */
					
					function orderByBank( str ){
						
						document.getElementById( 'SearchForm' ).elements[ 'page' ].value = '1';
						document.getElementById( 'SearchForm' ).elements[ 'orderByBank' ].value = str;
						
						$('#SearchForm').ajaxSubmit( { 
							beforeSubmit:  preSubmitCallback,  // pre-submit callback 
							success:	   postSubmitCallback  // post-submit callback 
						} ); 
						
					}
					
					 
					
					function selectSince(){ document.getElementById( "interval_select_since" ).checked = true; }
					
					
					function selectMonth(){ document.getElementById( "interval_select_month" ).checked = true; }
					
					
					function selectBetween( cal ){ document.getElementById( "interval_select_between" ).checked = true; }
					
					function selectDay( cal ){ document.getElementById( "day_select" ).checked = true; }
					
					
					
				/* ]]> */
				</script>
				<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
					<div class="subTitleContainer" style="padding-top:10px;">
						<p class="subTitle">Rechercher dans les extraits de compte</p>
					</div>
					<br style="clear:both;" />
					<input type="hidden" name="search" value="1" />
					<input type="hidden" name="page" value="<?php echo $postData[ "page" ] ?>" />
					<input type="hidden" name="orderByBank" value="<?php echo $postData[ "orderByBank" ] ?>" />
					<div class="tableContainer" style="margin-top:0;">
						<table class="dataTable">
							<tr>
								<th class="filledCell">Date de valeur</td>
								<td><span style="margin:0 10px 0 0;">du</span><?php $date = isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : date( "d-m-Y", mktime( 0, 0, 0, 1, 1, date( "Y" ) ) ); ?><input type="text" name="sp_bt_start" id="sp_bt_start" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_start", true ) ?><span style="margin:0 10px;">au</span><?php $date = isset( $_POST[ "sp_bt_end" ] ) ? $_POST[ "sp_bt_end" ] : date( "d-m-Y", mktime( 0, 0, 0, 1, 0, date( "Y" ) + 1 ) ); ?><input type="text" name="sp_bt_end" id="sp_bt_end" class="calendarInput" value="<?php echo $date ?>" /> <?php echo DHTMLCalendar::calendar( "sp_bt_end", true ) ?></td>
							</tr>
							<tr>
								<th class="filledCell">Montant banque</td>
								<td><input type="text" name="montant_banque" value="<?php isset($_POST[ "montant_banque" ])?$_POST[ "montant_banque" ]:''; ?>" />  </td>
							</tr>
							<tr>
								<th class="filledCell">Montant écriture</td>
								<td><input type="text" name="montant_ecriture" value="<?php echo isset($_POST[ "montant_ecriture" ])?$_POST[ "montant_ecriture" ]:''; ?>" />  </td>
							</tr>
							<tr>
                            <th class="filledCell">Banque</th>
								<td>
                                
                                
                                <select name="bankcompany" id="bankcompany">
                                	<option value="0">--Sélectionner une banque--</option>
									<?php
					
										$q1 = "SELECT idbank_company, name
													FROM bank_company";
													
										$rs1 = & DBUtil::query( $q1 );
										
										while(!$rs1->EOF())
										{
											$idbank_company = $rs1->fields("idbank_company");
											$name = $rs1->fields("name");
											

											echo '<option value="' . $idbank_company . '">' . $name . '</option>';
											
											$rs1->MoveNext();

										}

									?>
								</select>
                                
                                </td>
                                </tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" value="Rechercher" />
					</div>
				</form>
				<div id="SearchResults"></div>
<?php
	
}

//----------------------------------------------------------------------------------------------------

function displayBankImport(){
	
?>
					<form method="post" enctype="multipart/form-data" action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" id="BankForm">
						<div class="subTitleContainer" style="padding-top:10px;">
						  <p class="subTitle">Import de l'extrait de la banque</p>
						</div>
                        
                        <br style="clear:both;" />
                        
                        <div>
                                               
                        	<table class="dataTable" style="width:26%; text-align:center;">
								<tr>
								<th class="filledCell">Banque</th>
								<td>
                                
                                
                                <select name="bankcompany" id="bankcompany">
                                	<option value="0">--Sélectionner une banque--</option>
									<?php
										$q1 = "SELECT idbank_company, name
													FROM bank_company";
													
										$rs1 = & DBUtil::query( $q1 );
											
										
										
										while(!$rs1->EOF())
										{
											$idbank_company = $rs1->fields("idbank_company");
											$name = $rs1->fields("name");
											

											echo '<option value="' . $idbank_company . '">' . $name . '</option>';
											
											$rs1->MoveNext();

										}

									?>
								</select>
                                
                                </td>
								</tr>
							
							</table>
                        
                        </div>
                        <br style="clear:both;" />
						<div style="padding:5px 0;"><input type="file" name="importBank" accept="*.csv" /></div>
						<div class="submitButtonContainer"><input type="submit" value="Importer" class="blueButton" /></div>
						<div id="BankUploadResults"></div>
					</form>
				
<?php
	
}

function searchBankAccountversion2(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	$maxSearchResults = 1000;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	//print_r($_POST);
	
	$where = $from = '';
	//Montant banque
	
	if( isset( $_POST[ "montant_banque" ] ) && !empty( $_POST[ "montant_banque" ] ) )
		$where .= " AND accounts_listing.amount = '" . Util::html_escape( $_POST[ "montant_banque" ] ) . "'";
	
	
	//Montant écriture
	
	if( isset( $_POST[ "montant_ecriture" ] ) && !empty( $_POST[ "montant_ecriture" ] ) )
		$where .= " AND account_plan.amount = '" . Util::html_escape( $_POST[ "montant_ecriture" ] ) . "'";
	
	
	//$sp_bt_start 		= isset( $_POST[ "sp_bt_start" ] ) ? $_POST[ "sp_bt_start" ] : "-365";
	if( isset( $_POST[ "sp_bt_start" ] ) && !empty( $_POST[ "sp_bt_start" ] ) )
	{
		$Min_Time = euDate2us( $_POST[ "sp_bt_start" ] );
		$where .= " AND value_date >= '$Min_Time' ";
	}	
	
	if( isset( $_POST[ "sp_bt_end" ] ) && !empty( $_POST[ "sp_bt_end" ] ) )
	{
		$Max_Time = euDate2us( $_POST[ "sp_bt_end" ] );
		$where .= " AND value_date <= '$Max_Time 23:59:59'";
	}		
	if(isset($_POST[ "orderByBank" ]))
		$orderBy = Util::html_escape( stripslashes( $_POST[ "orderByBank" ])) ;
	else
		$orderBy ='value_date DESC';
	
	
	//Recherche par banque
	
	if( isset( $_POST[ "bankcompany" ] ) && !empty( $_POST[ "bankcompany" ] ) )
		$where .= " AND accounts_listing.code_bank = '" . Util::html_escape( $_POST[ "bankcompany" ] ) . "'";
	
	
	$query = "
		SELECT account_plan.idrow, account_plan.`amount`, account_plan.idaccounts_listing, account_plan.idaccount_record, account_plan.`amount_type`,
		accounts_listing.`amount` accounts_listing_amount, accounts_listing.code_bank, accounts_listing.`amount_type` accounts_listing_amount_type,
		`designation`, `relation_type`, `idrelation`, `relation_type`, value_date
		FROM `account_plan`, accounts_listing 
		WHERE account_plan.idaccounts_listing = accounts_listing.idaccounts_list
		$where
		ORDER BY $orderBy";
	
	//echo $query;
	$rs =& DBUtil::query( $query );
	
	$resultCount = $rs->_numOfRows;
	
	?>

	<div class="headTitle" style="margin-top:15px;">
				
				<p class="title" style="margin-bottom:15px;">
					<?php if( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> opérations ont été trouvées
					<?php }elseif( $resultCount == 1 ){ ?>1 opération trouvée
					<?php }elseif( $resultCount > 1 ){ ?><?php echo $resultCount ?> opérations trouvées
					<?php }else{ ?>Aucune opération trouvée<?php } ?>
				</p>
			</div>
			<div class="resultTableContainer" id="bankResultContainer">
				<table class="dataTable resultTable clear">
					<thead>
						<tr>
                        	<th>Code_banque</th>
							<th>Montant banque</th>
							<th>Montant écriture</th>
							<th>Intitulé</th>
							<th>Type</th>
                            <th>n° de règlement </th>
                            <th>n° ligne banque</th>
							<th>N° compte</th>
							<th>Nom du compte</th>
							<th>Date valeur</th>
                            <th>Annuler</th>
							
						</tr>
						 <!-- petites flèches de tri -->
						<tr>
                        	<th class="noTopBorder">
								<a href="#" onclick="orderByBank('designation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('designation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder"></th>
							<th class="noTopBorder">
								<a href="#" onclick="orderByBank('designation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('designation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="#" onclick="orderByBank('relation_type ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('relation_type DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder"></th>
                            <th class="noTopBorder">
								<a href="#" onclick="orderByBank('designation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('designation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
                            <th class="noTopBorder">
								<a href="#" onclick="orderByBank('designation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('designation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder"></th>
							<th class="noTopBorder">
								<a href="#" onclick="orderByBank('value_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('value_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
                            <th class="noTopBorder"></th>
                            <th class="noTopBorder"></th>
						</tr>
					</thead>
					<tbody>
					<?php 
						$sum_amount = 0;
						$sum_amount_accounts_listing = 0;
						while( !$rs->EOF ){ 
							
							//$customers 	= getCustomers( $rs->fields( "heading" ), $rs->fields( "amount" ) );
							//$matches 	= getInvoices( $customers, $rs->fields( "amount" ), $rs->fields( "value_date" ) );
							
							$qr1 = "SELECT code 
									FROM bank_company
									WHERE idbank_company ='". $rs->fields( "code_bank" ) ."'
									";
			
							$rs1 =& DBUtil::query( $qr1 );
			
							$code_bank = $rs1->fields("code");
							
							
							?>
							<tr id="ligne_<?php echo $rs->fields( "idrow" )  ;?>">
                            	<td><?php echo ($rs->fields( "code_bank" ) !="")? $code_bank : "-" ?></td>
								<td class="lefterCol grasBack">
								<?php 
									$accounts_listing_amount = $rs->fields( "accounts_listing_amount_type" ) == 'debit' ? -1 * $rs->fields( "accounts_listing_amount" ) : $rs->fields( "accounts_listing_amount" );
									echo number_format($accounts_listing_amount,2);
									$sum_amount_accounts_listing += $sum_amount_accounts_listing;
								?>
								</td>
								<td class="lefterCol grasBack">
								<?php 
									$amount = $rs->fields( "amount_type" ) == 'debit' ? -1 * $rs->fields( "amount" ) : $rs->fields( "amount" );
									echo number_format($amount,2);
									$sum_amount += $amount;
								?>
								</td>
							
								
								<td ><?php echo $rs->fields( "designation" )  ;?></td>
								<td ><?php echo $rs->fields( "relation_type" ) ; ?></td>
                                <td ><?php echo $rs->fields( "idaccount_record" ) ; ?></td>
                                <td ><?php echo $rs->fields( "idaccounts_listing" ) ; ?></td>
								<td ><?php echo $rs->fields( "idrelation" )  ;?></td>
								
								<td >
									<?php
									switch($rs->fields( "relation_type" )){
										case 'buyer':
										$query = "
												SELECT company, lastname, firstname
												FROM `contact` C, buyer B	
												WHERE C.idbuyer = B.idbuyer
												AND idcontact = 0
												AND B.`idbuyer` =".$rs->fields( "idrelation" );
											 // echo $query;
											$rss =& DBUtil::query( $query );
											if ($rss->fields( "company" ) != '')
												$rss->fields( "company" ).', ';
											echo $rss->fields( "lastname" ).' '.$rss->fields( "firstname" );
										break;
										case 'supplier':
											$query = "SELECT name FROM `supplier` WHERE `idsupplier` =".$rs->fields( "idrelation" );
											// echo $query;
											$rss =& DBUtil::query( $query );
											echo $rss->fields( "name" );

										break;
										case 'provider':
											$query = "SELECT name FROM `provider` WHERE `idprovider` =".$rs->fields( "idrelation" );
											// echo $query;
											$rss =& DBUtil::query( $query );
											echo $rss->fields( "name" );

										break;
										}?>
								</td>
								<td ><?php echo $rs->fields( "value_date" )  ;?></td>
   								
                                <?php $chaine="";
								
								$qr1 = "
												SELECT idrow
												FROM `account_plan`	
												WHERE idaccounts_listing =". $rs->fields('idaccounts_listing')."
												";
											 // echo $query;
											$rs1 =& DBUtil::query( $qr1 );
								
								while( !$rs1->EOF ){ 
								$chaine .= $rs1->fields('idrow'). ",";
								$rs1->MoveNext();
							
								}
								
								?>
								
                                
                                <td ><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/ico_red_cross.png" onclick="javascript:AnnulerRapprochement(<?php echo $rs->fields('idrow') ?>,<?php echo $rs->fields('idaccounts_listing') ?>,'<?php echo $chaine ?>')" /></td>
							</tr>
							<?php
							
							$rs->MoveNext();
							
						}
						
					?>
					<tr>
						<td  style="background-color:#F1F1F1"  class="grasBack">Total: <?=$sum_amount_accounts_listing; ?> </td>
						<td  style="background-color:#F1F1F1"  class="grasBack">Total: <?=$sum_amount; ?> </td></tr>
					</tbody>
				</table>
			</div>
            
            <script type='application/javascript'>		
		function AnnulerRapprochement(idrow,idaccounts_listing,chaine)
			{
			var tableau=chaine.split(",");
			
			var location = '<?php echo $GLOBAL_START_URL ?>/accounting/update_listing_bank.php?idrow=' + idrow +' &idaccounts_listing=' + idaccounts_listing;		
			
		var xhr_object = null;
		if(window.XMLHttpRequest) // Firefox
		   xhr_object = new XMLHttpRequest();
		else if(window.ActiveXObject) // Internet Explorer
		   xhr_object = new ActiveXObject("Microsoft.XMLHTTP");
		else { // XMLHttpRequest non supporté par le navigateur
		   alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");
		   return;
		}
	
		xhr_object.onreadystatechange = function()
		{
		if(xhr_object.readyState == 4)
		{
		     // ON CONTROLE LE STATUS (ERREUR 404, ETC)
			if(xhr_object.status == 200)
			
		{
			for(j=0;j< tableau.length-1;j++)
			{
				document.getElementById("ligne_" + tableau[j]).style.display="none";
			}
			
			
			
				
			}else
				return ;
			}
		}; 
		
		xhr_object.open("GET", location, true);
		xhr_object.send(null);
						
						
						
				//	}
			
			
			
			}
		
			
			</script>
            
			<?php 
			
		
	
}
//----------------------------------------------------------------------------------------------------
/*
	//Deprecated Since astelos edit 5 google doc by behzad and replace by searchBankAccountversion2() 8/8/2011
function searchBankAccount(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	$maxSearchResults = 1000;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	$select = "
	operation_date,
	value_date,
	heading,
	heading_2,
	amount,
	amount_type,
	balance,
	action";
	
	$tables = "accounts_listing";
	
	$where = "action = 'bank'";
	
	//post data
	
	//nom client / intitulé
	
	if( isset( $_POST[ "name" ] ) && strlen( $_POST[ "name" ] ) ){
		
		$where .= " AND ( heading LIKE '%" . Util::html_escape( $_POST[ "name" ] ) . "%'";
		$where .= " OR heading_2 LIKE '%" . Util::html_escape( $_POST[ "name" ] ) . "%' )";
		
	}
	
	//montant
	
	if( isset( $_POST[ "amount" ] ) && !empty( $_POST[ "amount" ] ) )
		$where .= " AND amount = '" . Util::html_escape( $_POST[ "amount" ] ) . "'";
	
	//opération
	
	if( isset( $_POST[ "heading_list" ] ) && !empty( $_POST[ "heading_list" ] ) )
		$where .= " AND heading LIKE '" . Util::html_escape( $_POST[ "heading_list" ] ) . "%'";
	
	//date
	
 	$now = getdate();
	
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	
	switch( $interval_select ){
		
		case 1:
			
			$Min_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
			$Now_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + 1,$now["year"])) ;
			$where  .= " AND value_date >= '$Min_Time' AND value_date < '$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST[ "yearly_month_select" ];
			
			if( $yearly_month_select > $now[ "mon" ] )
				$now[ "year" ]--;
			
		 	//min
			$Min_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select + 1, 1,$now["year"])) ;
			
			//sql_condition
			$where .= " AND value_date >= '$Min_Time' AND value_date < '$Max_Time'";
		 	break;
			
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "sp_bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "sp_bt_end" ] );
		 	
			$where .= " AND value_date >= '$Min_Time' AND value_date <= '$Max_Time 23:59:59'";
			
		 	break;
			
		case 4:
		 	
		 	$date = euDate2us( $_POST[ "day" ] );
		 	
			$where .= " AND value_date = '$date'";
			
		 	break;
			
	}
	
	//tri
	
	$orderBy = " action ASC," . Util::html_escape( stripslashes( $_POST[ "orderByBank" ] ) );
	
	//nombre de pages
	
	$query = "
	SELECT COUNT( * ) AS resultCount
	FROM $tables
	WHERE $where";

	$rs =& DBUtil::query( $query );
	
	$resultCount = $rs->fields( "resultCount" );
	
	?>
	<script type="text/javascript">
	
	
		$(document).ready(function(){
			$('.invoiceCell').hover(
				function(){$(this).find('div.invoiceTip:hidden').show();},
				function(){$(this).find('div.invoiceTip:visible').hide();}
			);
		});
	
	
	</script>
	<div class="headTitle" style="margin-top:15px;">
				<?php
				
				if( $resultCount > 0 ){
			
					$query = "
						SELECT $select
						FROM $tables
						WHERE $where
						ORDER BY $orderBy";
					
					$rs =& DBUtil::query( $query );
					
				?>
				<?php } ?>
				<p class="title" style="margin-bottom:15px;">
					<?php if( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> opérations ont été trouvées
					<?php }elseif( $resultCount == 1 ){ ?>1 opération trouvée
					<?php }elseif( $resultCount > 1 ){ ?><?php echo $resultCount ?> opérations trouvées
					<?php }else{ ?>Aucune opération trouvée<?php } ?>
				</p>
			</div>
			<?php if( $resultCount > $maxSearchResults ){ ?>Merci de bien vouloir affiner votre recherche
			<?php }elseif( $resultCount > 0 && $resultCount < $maxSearchResults ){ ?><div class="resultTableContainer" id="bankResultContainer">
				<table class="dataTable resultTable clear">
					<thead>
						<tr>
							<th>Nom / Raison sociale</th>
							<th>Date de l'opération</th>
							<th>Date de valeur</th>
							<th>Intitulé opération</th>
							<th>Client n°</th>
							<th>Montant</th>
							<th>Factures</th>
						</tr>
						 <!-- petites flèches de tri -->
						<tr>
							<th class="noTopBorder"></th>
							<th class="noTopBorder"></th>
							<th class="noTopBorder"></th>
							<th class="noTopBorder"></th>
							<th class="noTopBorder">
								<a href="#" onclick="orderByBank('operation_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('operation_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="#" onclick="orderByBank('value_date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('value_date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder">
								<a href="#" onclick="orderByBank('heading ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
								<a href="#" onclick="orderByBank('heading DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
							</th>
							<th class="noTopBorder"></th>
						</tr>
					</thead>
					<tbody>
					<?php 

						while( !$rs->EOF ){ 
							
							$customers 	= getCustomers( $rs->fields( "heading" ), $rs->fields( "amount" ) );
							$matches 	= getInvoices( $customers, $rs->fields( "amount" ), $rs->fields( "value_date" ) );
							
							?>
							<tr>
								<td class="lefterCol grasBack">
								<?php 
								
									$i = 0;
									foreach( $matches as $idbuyer => $data ){
										
										if( $i ) 
											echo "<br />";
											
										echo "<a href=\"$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer\" onclick=\"window.open(this.href); return false;\">" . $data[ "company" ] . "</a>";
										$i++;
										
									}
									
								?>
								</td>
							
								
								<td><?php echo Util::dateFormatEu( $rs->fields( "operation_date" ) ) ?></td>
								<td><?php echo Util::dateFormatEu( $rs->fields( "value_date" ) ) ?></td>
								<td class="grasBack"><?php echo $rs->fields( "heading" ) ?><?php echo $rs->fields( "heading_2" ) == "" ? "" : "<br />" . $rs->fields( "heading_2" ) ?></td>
								<td class="grasBack"><?php 
								
									$i = 0;
									foreach( $matches as $idbuyer => $data ){
										
										if( $i ) 
											echo "<br />";
											
										echo "<a href=\"$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idbuyer\" onclick=\"window.open(this.href); return false;\">$idbuyer</a>";
										$i++;
										
									}
									
								?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $rs->fields( "amount_type" ) == "credit" ? "" : "-" ?> <?php echo Util::priceFormat( $rs->fields( "amount" ) ) ?></td>
								<td class="invoiceCell"><?php 
								
									$customersInvoices = array();
									$i = 0;
									foreach( $matches as $idbuyer => $data ){
										
										$invoices = explode( ",", $data[ "idbilling_buyer" ] );
										$customersInvoices = array_merge( $customersInvoices, $invoices );
										
										foreach( $invoices as $idbilling_buyer ){
											
											if( $i ) 
												echo "<br />";
												
											echo "<a href=\"$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=$idbilling_buyer\" onclick=\"window.open(this.href); return false;\">$idbilling_buyer</a>";

											$i++;
										}
										
									}
									
									if( count( $customersInvoices ) )
										invoiceTips( $customersInvoices );
									
								?></td>
								
							</tr>
							<?php
							
							$rs->MoveNext();
							
						}
						
					?>
					</tbody>
				</table>
			</div>
			<?php 
			
		} 
	
}
//Deprecated Since astelos edit 5 google doc by behzad 
*/
//-------------------------------------------------------------------------------------

//Fonction spécifique à l'import des extraits bancaires Crédit Mutuel 

function &importBank(){
	
	global $GLOBAL_START_PATH;

	if( !is_readable( $_FILES[ "importBank" ][ "tmp_name" ] ) )
		trigger_error( "Impossible de lire le fichier d'import", E_USER_ERROR );

	$file = @fopen( $_FILES[ "importBank" ][ "tmp_name" ], "r" );
	
		
	if( !$file ){
		
		trigger_error( "Impossible de lire le fichier d'import", E_USER_ERROR );
		return;
		
	}
	
	//Comptage du nombre de lignes
	$line_count = count( file( $_FILES[ "importBank" ][ "tmp_name" ] ) );
	
	$import = array();
	
	if(isset($_POST["bankcompany"]) && $_POST["bankcompany"]==3 )
	{
		
		//print $_POST["bankcompany"];
		
		
		$chaine = "";
		for($i=0;$i<($line_count);$i++)
	
		{

		
		// On recupere toute la ligne
		$uneLigne = (fgets($file));
		//On met dans un tableau les differentes valeurs trouvés (ici séparées par un ';')
		$tableauValeurs = explode(';', $uneLigne);
		
		
			for($j=0;$j<count($tableauValeurs);$j++)
			{
			$chaine.= $tableauValeurs[$j];
			$chaine .= ";";
				
			}
		$chaine.= "<br>";
		}
		
		
		$chaine = explode("\n",$chaine);  // On divise selon les lignes
		unset($chaine[0]); // On enlève la première
		unset($chaine[1]);

		$chaine = array_values($chaine);
		
		
		for($k=0;$k<count($chaine)-4;$k++)
		{
			
			
			$line = explode(';', $chaine[$k]);
			
			
			$date_tmp = explode('/', $line[1]);
			
			$date = $date_tmp[2] . "-" . $date_tmp[1] . "-" . strip_tags($date_tmp[0]);
			
			$ref = str_replace("=","",$line[2]);
			$ref = str_replace('"','',$ref);
			
			
			$import[ $k ][ "heading" ]			= $ref. "/" .$line[ 3 ];
			$import[ $k ][ "operation_date" ]	= $date;
			$import[ $k ][ "value_date" ]		= $date;
			
			$import[ $k ][ "import_date" ]		= date( "Y-m-d" );
			$import[ $k ][ "action" ]			= "bank";
			$import[ $k ][ "is_import" ]			= "Non";
			
			$debit = $line[ 4 ];
		
			if( !empty( $debit ) )
				{
					
					$import[ $k ][ "amount" ]		= abs( floatval( $line[ 4 ] ) );
					$import[ $k ][ "amount_type" ]	= "debit";
					$import[ $k ][ "balance" ]			= - $line[ 4 ];
					
				}
			else
				{	
					
					$import[ $k ][ "amount" ]		= abs( floatval( $line[ 5 ] ) );
					$import[ $k ][ "amount_type" ]	= "credit";
					$import[ $k ][ "balance" ]			= $line[ 5 ];
				}	
			
			$import[ $k ][ "code_bank" ] = $_POST["bankcompany"];
			
			$query = "SELECT * FROM accounts_listing WHERE 
				heading = '" . Util::html_escape( $import[ $k ][ "heading" ] ) . "'
				AND value_date = '" . $import[ $k ][ "value_date" ] . "'
				AND amount = '" . $import[ $k ][ "amount" ] . "'
				AND amount_type = '" . $import[ $k ][ "amount_type" ] . "'
				AND code_bank = '" . $import[ $k ][ "code_bank" ] . "'
				AND action = '" . $import[ $k ][ "action" ] . "'";
				
			//echo $query;
			$rs =& DBUtil::query( $query );
			
			if( !$rs->RecordCount() )
			{
		
				$import[ $k ][ "is_import" ] = "Oui";
			
				$query = "INSERT INTO accounts_listing (
					heading,
					value_date,
					operation_date,
					amount,
					amount_type,
					balance,
					import_date,
					code_bank,
					action
				) VALUES (
					'" . Util::html_escape( $import[ $k ][ "heading" ] ) . "',
					'" . $import[ $k ][ "value_date" ] . "',
					'" . $import[ $k ][ "operation_date" ] . "',
					'" . $import[ $k ][ "amount" ] . "',
					'" . $import[ $k ][ "amount_type" ] . "',
					'" . $import[ $k ][ "balance" ] . "',
					'" . $import[ $k ][ "import_date" ] . "',
					'" . $import[ $k ][ "code_bank" ] . "',
					'" . $import[ $k ][ "action" ] . "'
				)";
			
				$rs =& DBUtil::query( $query );
			
				if( $rs === false )
					trigger_error( "Impossible d'enregistrer les données dans la base" );
			
			}
				else
					array_splice( $import, $j, 1 );
		
		}

	
	}//Fin relevé Natixis
	
	else if(isset($_POST["bankcompany"]) && $_POST["bankcompany"]==2 )
	{
				
		$chaine = "";
		for($i=0;$i<($line_count);$i++)
	
		{

		
		// On recupere toute la ligne
		$uneLigne = (fgets($file));
		//On met dans un tableau les differentes valeurs trouvés (ici séparées par un ';')
		$tableauValeurs = explode(';', $uneLigne);
		
		
			for($j=0;$j<count($tableauValeurs);$j++)
			{
			$chaine.= $tableauValeurs[$j];
			$chaine .= ";";
				
			}
		$chaine.= "<br>";
		}
		
		
		$chaine = explode("\n",$chaine);  // On divise selon les lignes
		unset($chaine[0]); // On enlève la première
		unset($chaine[1]);
		unset($chaine[2]);
		unset($chaine[3]);
		unset($chaine[4]);

		$chaine = array_values($chaine);
		
		for($k=0;$k<count($chaine)-1;$k++)
		{
		
		$line = explode(';', $chaine[$k]);
		
		$date_tmp = explode('/', $line[1]);
			
			$date = $date_tmp[2] . "-" . $date_tmp[1] . "-" . strip_tags($date_tmp[0]);
			

			$import[ $k ][ "heading" ]			= $line[ 3 ];
			$import[ $k ][ "operation_date" ]	= $date;
			$import[ $k ][ "value_date" ]		= $date;
			
			$import[ $k ][ "import_date" ]		= date( "Y-m-d" );
			$import[ $k ][ "action" ]			= "bank";
			$import[ $k ][ "is_import" ]			= "Non";
			
			$debit = $line[ 4 ];
		
			if( !empty( $debit ) )
				{
					
					$import[ $k ][ "amount" ]		= $line[ 4 ];
					$import[ $k ][ "amount_type" ]	= "debit";
					$import[ $k ][ "balance" ]		= $line[ 4 ];

				}
			else
				{	
					
					$import[ $k ][ "amount" ]		= $line[ 5 ];
					$import[ $k ][ "amount_type" ]	= "credit";
					$import[ $k ][ "balance" ]		= $line[ 5 ];
				}	
			
			$import[ $k ][ "code_bank" ] = $_POST["bankcompany"];
			
			
			
			
			$query = "SELECT * FROM accounts_listing WHERE 
				heading = '" . Util::html_escape( $import[ $k ][ "heading" ] ) . "'
				AND value_date = '" . $import[ $k ][ "value_date" ] . "'
				AND amount = '" . $import[ $k ][ "amount" ] . "'
				AND amount_type = '" . $import[ $k ][ "amount_type" ] . "'
				AND code_bank = '" . $import[ $k ][ "code_bank" ] . "'
				AND action = '" . $import[ $k ][ "action" ] . "'";
				
			//echo $query;
			$rs =& DBUtil::query( $query );
			
			if( !$rs->RecordCount() )
			{
		
				$import[ $k ][ "is_import" ] = "Oui";
			
				$query = "INSERT INTO accounts_listing (
					heading,
					value_date,
					operation_date,
					amount,
					amount_type,
					balance,
					import_date,
					code_bank,
					action
				) VALUES (
					'" . Util::html_escape( $import[ $k ][ "heading" ] ) . "',
					'" . $import[ $k ][ "value_date" ] . "',
					'" . $import[ $k ][ "operation_date" ] . "',
					'" . $import[ $k ][ "amount" ] . "',
					'" . $import[ $k ][ "amount_type" ] . "',
					'" . $import[ $k ][ "balance" ] . "',
					'" . $import[ $k ][ "import_date" ] . "',
					'" . $import[ $k ][ "code_bank" ] . "',
					'" . $import[ $k ][ "action" ] . "'
				)";
			
				$rs =& DBUtil::query( $query );
			
				if( $rs === false )
					trigger_error( "Impossible d'enregistrer les données dans la base" );
			
			}
				else
					array_splice( $import, $j, 1 );
		
		}
	
	
	}//Fin relevé CE
	

	else
	{

	fgetcsv( $file, 0, ";" );
	$j = 0;
	while( ( $line = fgetcsv( $file, 0, "," ) ) !== FALSE ){

		$import[ $j ][ "heading" ]			= $line[ 4 ];
		$import[ $j ][ "operation_date" ]	= bankDate2MySQL( $line[ 0 ], "/" );
		$import[ $j ][ "value_date" ]		= bankDate2MySQL( $line[ 1 ], "/" );
		$import[ $j ][ "balance" ]			= $line[ 5 ];
		$import[ $j ][ "import_date" ]		= date( "Y-m-d" );
		$import[ $j ][ "action" ]			= "bank";
		$import[ $j ][ "is_import" ]			= "Non";
		
		$debit = $line[ 2 ];
		
		if( !empty( $debit ) ){
			
			$import[ $j ][ "amount" ]		= abs( floatval( $line[ 2 ] ) );
			$import[ $j ][ "amount_type" ]	= "debit";
			
		}else{	
			
			$import[ $j ][ "amount" ]		= abs( floatval( $line[ 3 ] ) );
			$import[ $j ][ "amount_type" ]	= "credit";
			
		}
		
		//Si la ligne a déjà été importée, on ne la réimporte pas
		//On ne peut pas être certain à 100%
		//Il y a un tout petit pouillème de chance pour qu'on ait deux lignes exactement identiques
		// mais la probabilité est plus que faible
		//Une erreur humaine est beaucoup plus probable
		
		//AND operation_date = '" . $import[ $j ][ "operation_date" ] . "'
		//AND balance = '" . $import[ $j ][ "balance" ] . "'
		$query = "SELECT * FROM accounts_listing WHERE 
			heading = '" . Util::html_escape( $import[ $j ][ "heading" ] ) . "'
			AND value_date = '" . $import[ $j ][ "value_date" ] . "'
			AND amount = '" . $import[ $j ][ "amount" ] . "'
			AND amount_type = '" . $import[ $j ][ "amount_type" ] . "'
			AND action = '" . $import[ $j ][ "action" ] . "'";
		//echo $query;
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			trigger_error( "Impossible de vérifier l'existance des données dans la base" );
		
		if( !$rs->RecordCount() ){
		
			$import[ $j ][ "is_import" ] = "Oui";
			
			$query = "INSERT INTO accounts_listing (
				heading,
				value_date,
				operation_date,
				amount,
				amount_type,
				balance,
				import_date,
				action
			) VALUES (
				'" . Util::html_escape( $import[ $j ][ "heading" ] ) . "',
				'" . $import[ $j ][ "value_date" ] . "',
				'" . $import[ $j ][ "operation_date" ] . "',
				'" . $import[ $j ][ "amount" ] . "',
				'" . $import[ $j ][ "amount_type" ] . "',
				'" . $import[ $j ][ "balance" ] . "',
				'" . $import[ $j ][ "import_date" ] . "',
				'" . $import[ $j ][ "action" ] . "'
			)";
			
			$rs =& DBUtil::query( $query );
			
			if( $rs === false )
				trigger_error( "Impossible d'enregistrer les données dans la base" );
			
			$j++;
			
		}
		else
			array_splice( $import, $j, 1 );
		
		$j++;
		
	}
	}
	
	return $import;
	
}

//-------------------------------------------------------------------------------------

function bankDate2MySQL( $euDate ){
	
	$res = @explode( "/", $euDate );
	
	if( count( $res ) != 3 )
		return $euDate;
	
	@list( $month, $day, $year ) = $res;
		
	$usDate = "$year-$month-$day";
	
	return $usDate;
	
}

//-------------------------------------------------------------------------------------

function getCustomers( $heading, $amount ){
	
	$customers = array();
	
	/* recherche du compte client */
	
	$company = false;
	
	if( ( $pos = strpos( $heading, "VIR " ) ) !== false )
		$company = trim( substr( $heading, $pos + 4 ) );
		
	else if( ( $pos = strpos( $heading, "PRLV " ) ) !== false )
		$company = trim( substr( $heading, $pos + 5 ) );
	
	//recherche sur nom société de plus en plus petit, gnhiiihihihi ^^

	if( $company !== false ){
		
		do{

			$company = str_replace( " ", "%", $company );
			
			$rs =& DBUtil::query( "SELECT idbuyer FROM buyer WHERE company LIKE '%" . Util::html_escape( $company ) . "%' LIMIT 2" );
			
			while( !$rs->EOF() ){
				
				array_push( $customers, $rs->fields( "idbuyer" ) );
				
				$rs->MoveNext();

			}
			
			if( count( $customers ) )
				return $customers;
				
		}
		while( $company !== false && strpos( $company, "%" ) !== false && $company = substr( $company, 0, strrpos( $company, "%" ) ) );
		
	}

	$query = "
	SELECT bb.idbuyer 
	FROM billing_buyer bb
	WHERE bb.total_amount = '$amount'";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		array_push( $customers, $rs->fields( "idbuyer" ) );
		
		$rs->MoveNext();

	}

	if( count( $customers ) )
		return $customers;
		
	/* recherche dans les règlements */
	
	$query = "
	SELECT idbuyer
	FROM regulations_buyer
	WHERE amount = '$amount'";
	
	$rs =& DBUtil::query( $query );
	
	while( !$rs->EOF() ){
		
		array_push( $customers, $rs->fields( "idbuyer" ) );
		
		$rs->MoveNext();

	}
	
	return $customers;
	
}

//-------------------------------------------------------------------------------------

function getInvoices( array $customers, $amount, $value_date ){
	
	$results = array();
	
	/* recherche avec montant */
	
	$query = "
	SELECT GROUP_CONCAT( bb.idbilling_buyer ) AS idbilling_buyer, 
		GROUP_CONCAT( bb.deliv_payment ) AS deliv_payment,
		GROUP_CONCAT( bb.DateHeure ) AS DateHeure,
		b.company, 
		b.zipcode,
		b.idbuyer
	FROM billing_buyer bb, buyer b
	WHERE (
		ROUND( bb.total_amount, 2 ) = '$amount'
		OR ROUND(
			 ROUND( bb.total_amount, 2 ) 
			- ( bb.rebate_type = 'amount' ) *  bb.rebate_value
			- (  bb.rebate_type = 'rate' ) *  ROUND( bb.total_amount, 2 ) *  bb.rebate_value / 100.0
			- (  bb.down_payment_type = 'amount' ) *  bb.down_payment_value
			- (  bb.down_payment_type = 'rate' ) *  ROUND( bb.total_amount, 2 ) *  bb.down_payment_value / 100.0
			-  bb.credit_amount
		, 2 ) = '$amount'
		OR( bb.down_payment_type = 'rate' AND ROUND( bb.total_amount, 2 ) *  bb.down_payment_value / 100.0 = '$amount' )
		OR( bb.down_payment_type = 'amount' AND bb.down_payment_value = '$amount' )
	)
	AND bb.idbuyer = b.idbuyer";
	
	if( count( $customers ) )
		$query .= " AND b.idbuyer IN( '" . implode( "','", $customers ) . "' )";
		
	$query .= "
	GROUP BY b.idbuyer
	ORDER BY bb.idbuyer ASC, bb.idbilling_buyer ASC";

	$rs =& DBUtil::query( $query );

	while( !$rs->EOF() ){
		
		$results[ strval( $rs->fields( "idbuyer" ) ) ] = $rs->fields;
		
		$rs->MoveNext();
			
	}
	
	if( count( $results ) )
		return $results;
	
	/* recherche sans montant */
	
	if( !count( $customers ) )
		return $results;
		
	$query = "
	SELECT GROUP_CONCAT( bb.idbilling_buyer ) AS idbilling_buyer, 
		GROUP_CONCAT( bb.deliv_payment ) AS deliv_payment,
		GROUP_CONCAT( bb.DateHeure ) AS DateHeure,
		b.company, 
		b.zipcode,
		b.idbuyer
	FROM billing_buyer bb, buyer b
	WHERE bb.idbuyer = b.idbuyer
	AND b.idbuyer IN( '" . implode( "','", $customers ) . "' )
	GROUP BY b.idbuyer
	ORDER BY bb.idbuyer ASC, bb.idbilling_buyer ASC";

	$rs =& DBUtil::query( $query );

	while( !$rs->EOF() ){
		
		$results[ strval( $rs->fields( "idbuyer" ) ) ] = $rs->fields;
		
		$rs->MoveNext();
			
	}

	return $results;
			
}
//-------------------------------------------------------------------------------------

function invoiceTips( array $invoices ){

	?>
	<div class="invoiceTip" style="background-color:#FFFFFF; border:5px solid #EB6A0A; border-radius:5px; display:none; margin-left:-820px; margin-top:6px; padding:3px; position:absolute; width:800px; -moz-border-radius:5px; -webkit-border-radius:5px;">
		<table class="dataTable resultTable">
			<thead>
				<tr>
					<th>Facture n°</th>
					<th>Nom / Raison sociale</th>
					<th>Code postal</th>
					<th>Date de création</th>
					<th>Date d'échéance</th>
						<th>Statut</th>
						<th>Client n°</th>
						<th>Total HT</th>
						<th>Total TTC</th>
					</tr>
				</thead>
				<tbody>
				<?php
	
					foreach( $invoices as $invoice ){
						
						$rsInvoices =& DBUtil::query( "
						SELECT billing_buyer.idbilling_buyer,
							buyer.idbuyer,
							buyer.company,
							buyer.zipcode,
							billing_buyer.total_amount,
							billing_buyer.total_amount_ht,
							billing_buyer.status,
							billing_buyer.DateHeure,
							billing_buyer.deliv_payment
						FROM billing_buyer, buyer
						WHERE buyer.idbuyer = billing_buyer.idbuyer
							AND billing_buyer.idbilling_buyer = $invoice" );
						
						if( $rsInvoices === false )
							trigger_error( "Impossible de récupérer la liste des factures", E_USER_ERROR );
		
						?>
						<tr>
							<td class="lefterCol" style="background-color:#FFFFFF;"><?php echo $rsInvoices->fields( "idbilling_buyer" ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo htmlentities( $rsInvoices->fields( "company" ) ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo $rsInvoices->fields( "zipcode" ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo Util::dateFormatEu( $rsInvoices->fields( "DateHeure" ), false ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo Util::dateFormatEu( $rsInvoices->fields( "deliv_payment" ) ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo Dictionnary::translate( $rsInvoices->fields( "status" ) ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo $rsInvoices->fields( "idbuyer" ) ?></td>
							<td style="background-color:#FFFFFF;"><?php echo Util::priceFormat( $rsInvoices->fields( "total_amount_ht" ) ) ?></td>
							<td class="righterCol" style="background-color:#FFFFFF;"><?php echo Util::priceFormat( $rsInvoices->fields( "total_amount" ) ) ?></td>
						</tr>
						<?php
						
					}
	
				?>
				</tbody>
			</table>
		</div>
		<?php
		
}

//-------------------------------------------------------------------------------------

?>