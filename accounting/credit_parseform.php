<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Script permettant de traiter le formulaire des avoirs
 */



function ModifyCredit(Credit & $credit) {

	//commercial
	
	if( isset( $_POST["object"] ) )
		$credit->set( "object", stripslashes( $_POST[ "object" ] ) );
		
	//commercial
	
	if( isset( $_POST["iduser"] ) && intval( $_POST["iduser"] ) ){

		include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
		$credit->setSalesman( new Salesman( intval( $_POST["iduser"] ) ) );

	}
	
	//port refacturé
	
	if( isset( $_POST["total_charge_ht" ] ) )
		$credit->set( "total_charge_ht", max( Util::text2num( $_POST[ "total_charge_ht" ] ), 0.0 ) );
		
	//port recrédité
	
	if( isset( $_POST["credited_charges" ] ) )
		$credit->set( "credited_charges", min( max( Util::text2num( $_POST[ "credited_charges" ] ), 0.0 ), $credit->getInvoice()->get( "total_charge_ht" ) ) );
	
	//lignes articles
	
	$it = $credit->getItems()->iterator();
	while( $it->hasNext() ){
		
		$creditItem =& $it->next();
		
		$idrow = $creditItem->get( "idrow" );
		
		//quantité
		
		if( isset( $_POST[ "quantity_$idrow" ] ) ){
			
			$quantity = abs( intval( $_POST[ "quantity_$idrow" ] ) );
			
			$idbilling_buyer 		= $credit->get( "idbilling_buyer" );
			$idbilling_buyer_row 	= $creditItem->get( "idbilling_buyer_row" );
			
			//quantité facturée
			
			$query = "
			SELECT quantity 
			FROM billing_buyer_row 
			WHERE idbilling_buyer = '$idbilling_buyer'
			AND idrow = '$idbilling_buyer_row'
			LIMIT 1";
			
			$rs =& DBUtil::query( $query );

			$invoiceQuantity = $rs->fields( "quantity" );
			
			//quantité déjà avoirisée
			
			$query = "
			SELECT quantity
			FROM credit_rows 
			WHERE idbilling_buyer_row = '$idbilling_buyer_row'";
			
			$rs =& DBUtil::query( $query );

			$creditQuantity = $rs->fields( "quantity" );
			
			//quantité dispounible
			
			if( $quantity > $invoiceQuantity )
				$quantity = $invoiceQuantity;
			
			$creditItem->set( "quantity", $quantity );
			
		}
	
		//prix de vente
		
		if( isset( $_POST[ "unit_price_$idrow" ] ) )
			$creditItem->set( "unit_price", Util::text2num( $_POST[ "unit_price_$idrow" ] ) );
		
		//prix de vente remisé
		
		if( isset( $_POST[ "UpdateDiscountRate_$idrow" ] ) ){
			
			$creditItem->set( "discount_rate", round( Util::text2num( $_POST[ "discount_rate_$idrow" ] ), 2 ) );
			
			$discount_price = $creditItem->get( "unit_price" ) * ( 1 - $creditItem->get( "discount_rate" ) / 100.0 );
			$creditItem->set( "discount_price", round( $discount_price, 2 ) );
			
		}
		else if( isset( $_POST[ "UpdateDiscountPrice_$idrow" ] ) ){
			
			$creditItem->set( "discount_price", round( Util::text2num( $_POST[ "discount_price_$idrow" ] ), 2 ) );
			
			$discount_rate = ( $creditItem->get( "discount_price" ) / $creditItem->get( "unit_price" ) - 1.0 ) * 100.0;
			$creditItem->set( "discount_rate", round( $discount_rate, 2 ) );
			
		}

		//désignation
		if( isset( $_POST[ "designation_$idrow" ] ) )
			$creditItem->set( "designation", nl2br( $_POST[ "designation_$idrow" ] ) );
		
	}

	//remise globale : ATTENTION : se répercute sur toutes les lignes articles
	
	if( isset( $_POST[ "UpdateDiscountRate" ] ) ){
		
		$discount_rate 	= Util::text2num( $_POST[ "discount_rate" ] );
		
		//lignes article
		
		$it = $credit->getItems()->iterator();
		while( $it->hasNext() ){
			
			$creditItem =& $it->next();
			
			$item_discount_price = $creditItem->get( "unit_price" ) * ( 1 - $discount_rate / 100.0 );
			
			$creditItem->set( "discount_rate", $discount_rate );
			$creditItem->set( "discount_price", $item_discount_price );
			
		}

	}
	else if( isset( $_POST[ "UpdateDiscountAmount" ] ) ){
		
		$totalETWithoutDiscount = 0.0;
	
		$it = $credit->getItems()->iterator();
		while( $it->hasNext() ){
		
			$item =& $it->next();
			$totalETWithoutDiscount += $item->get( "unit_price" ) * $item->get( "quantity" );
				
		}

		$totalETWithoutDiscount += round( $credit->get( "total_charge_ht" ), 2 );
		$totalETWithoutDiscount -= round( $credit->get( "total_charge_ht" ), 2 );
			
		$discount_amount 	= Util::text2num( $_POST[ "discount_amount" ] );
		$discount_rate 		= $discount_amount * 100.0 / $totalETWithoutDiscount;
	
		//lignes article
		
		$it = $credit->getItems()->iterator();
		while( $it->hasNext() ){
			
			$creditItem =& $it->next();
			
			$item_discount_price = $creditItem->get( "unit_price" ) * ( 1 - $discount_rate / 100.0 );
			
			$creditItem->set( "discount_rate", $discount_rate );
			$creditItem->set( "discount_price", $item_discount_price );
			
		}
		
	}
	
	//commentaire
	
	if( isset( $_POST[ "comment" ] ) )
		$credit->set( "comment", stripslashes( $_POST[ "comment" ] ) );
	//HT TTC
	
		if( isset( $_POST[ "total_amount_ht" ] ) )
		$credit->set( "total_amount_ht", stripslashes( $_POST[ "total_amount_ht" ] ) );
			if( isset( $_POST[ "total_amount" ] ) )
		$credit->set( "total_amount", stripslashes( $_POST[ "total_amount" ] ) );
	//suppression d'un seul article possible => ne pas modifier à cause du réassignement automatique des idrow après une suppression
	
	$it = $credit->getItems()->iterator();
	$i = 0;
	while( $it->hasNext() ){
		
		$creditItem =& $it->next();
		$idrow = $creditItem->get( "idrow" );
		
		if( isset( $_POST[ "delete_$idrow" ] ) && $_POST[ "delete_$idrow" ] || !$creditItem->get( "quantity" ) ){
			
			$credit->removeItemAt( $i );
			$credit->save();
			
			return;
			
		}
		
		$i++;
		
	}
	
	//validation de l'avoir
	
	if( isset( $_POST[ "Validate" ] ) ){
		
		$credit->set( "editable", 0 );
	
		//créditer le compte du client
		
		$idbuyer 		= $credit->get( "idbuyer" );
		$totalAmountATI = $credit->getTotalATI();
		
		$query = "
		UPDATE buyer
		SET credit_balance = credit_balance + '$totalAmountATI'
		WHERE idbuyer = '$idbuyer'
		LIMIT 1";
		
		DBUtil::query( $query );
		
	}
	
	$credit->save();

	/* bloquer la revalidation du formulaire avec la touche F5 */
	
	global $GLOBAL_START_URL;
	
	header( "Location: $GLOBAL_START_URL/accounting/credit.php?idcredit=" . $credit->get( "idcredit" ) );
	
}

?>
