<?php /**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Resultat recherche factures clients B2C
 */
 ?>
					
					<script type="text/javascript">
					/* <![CDATA[ */
						
						/* ----------------------------------------------------------------------------------- */
						
						$(document).ready(function() {
							
							$(".amountRow").click(function(){
								
								var value = $(this).attr("id").substring(13);
								var totalAmount = $("#selectionAmount").val();
								
								if($(this).is(".activeAmountRow")){
									
									$(this).removeClass("activeAmountRow");
									
									totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
									
								}
								else{
									
									$(this).addClass("activeAmountRow");
									
									totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
									
								}
								
								$("#selectionAmount").val(totalAmount);
								$("#selectionAmountDiv").html(totalAmount + " ¤");
								
							});
							
						});
						
						/* ----------------------------------------------------------------------------------- */
						
					/* ]]> */
					</script>
					<?
					$allowModifyInvoice	= DBUtil::getParameterAdmin("allowModifyInvoices") ;
					?>
					<div class="resultTableContainer clear">
                    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
						<table class="dataTable resultTable">
							<thead>
								<tr>
									<th>Com.</th>
									<th>Facture n°</th>
									<th>Client n°</th>
									<th>Code postal</th>
									<th>Nom</th>
									<th>Statut</th>
									<th>Date</th>
									<th>Total HT</th>
									<th>Total TTC</th>
									<th>BL n°</th>
									<th>Avoirs</th>
									<th>Infos</th>
									<?if($allowModifyInvoice){?>
										<th>Modifier</th>
										<th>Impayée</th>
									<?php } ?>
								</tr>
								<!-- petites flèches de tri -->
								<tr>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('user.initial ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('user.initial DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('billing_buyer.idbilling_buyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('billing_buyer.idbilling_buyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('billing_buyer.idbuyer ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('billing_buyer.idbuyer DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('buyer.zipcode ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('buyer.zipcode DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('contact.lastname ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('contact.lastname DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('billing_buyer.status ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('billing_buyer.status DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('billing_buyer.DateHeure ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('billing_buyer.DateHeure DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('billing_buyer.total_amount_ht ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('billing_buyer.total_amount_ht DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('billing_buyer.total_amount ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('billing_buyer.total_amount DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder">
										<a href="#" onclick="orderBy('bl_delivery.idbl_delivery ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
										<a href="#" onclick="orderBy('bl_delivery.idbl_delivery DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
									</th>
									<th class="noTopBorder"></th>
									<th class="noTopBorder"></th>
									<?if($allowModifyInvoice){?>
										<th class="noTopBorder"></th>
										<th class="noTopBorder"></th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
							<?php
								
								$lang = User::getInstance()->getLang();
								
								$invoiceCount 		= 0;
								$paidCount 			= 0;
								$paidAmount			= 0.0;
								$invoicedCount 		= 0;
								$invoicedAmount 	= 0.0;
								$totalAmount	 	= 0.0;
								$totalAmountHT	 	= 0.0;
								$roughStrokeAmount 	= 0.0;
								$netMarginAmount 	= 0.0;
								
								while( !$rs->EOF() ){
							
									$idbilling_buyer 	= $rs->fields( "idbilling_buyer" );
									$idorder 			= $rs->fields( "idorder" );
									$charge_vat 		= $rs->fields( "charge_vat" );
									$date 				= $rs->fields( $date_type ) == "0000-00-00" ? "-" : $rs->fields( $date_type );
									$deliv_payment		= $rs->fields( "deliv_payment" ) == "0000-00-00" ? "-" : $rs->fields( "deliv_payment" );
									$idbuyer 			= $rs->fields( "idbuyer" );
									$idbl_delivery	 	= $rs->fields( "idbl_delivery" ) == null ? "" : $rs->fields( "idbl_delivery" );
									$status 			= $rs->fields( "status" );
									
									if( $status == "Invoiced" ){
										
										include_once( dirname( __FILE__ ) . "/../../objects/Payment.php" );
										
										if( Payment::checkRegulations( $idbilling_buyer ) )
											$status_plus = "<br />Paiement réceptionné";
										else
											$status_plus = "";
										
									}else{
										
										$status_plus = "";
										
									}
									
									$idpayment 			= $rs->fields( "idpayment" );
									$total_amount_ht 	= $rs->fields( "total_amount_ht" );
									$total_amount 		= $rs->fields( "total_amount" );
									$total_charge 		= $rs->fields( "total_charge" );
									$total_charge_ht 	= $rs->fields( "total_charge_ht" );
									$billing_amount 	= $rs->fields( "billing_amount" );
									$lastname 			= $rs->fields( "lastname" );
									$title 				= $rs->fields( "title" );
									$faxnumber 			= $rs->fields( "faxnumber" );
									$firstname 			= $rs->fields( "firstname" );
									$company 			= $rs->fields( "company" );
									$factor 			= $rs->fields( "factor" ) == null || !$rs->fields( "factor" ) ? ucfirst( Dictionnary::translate( "gest_com_no" ) ) : ucfirst( Dictionnary::translate( "gest_com_yes" ) );
									$contact 			= $rs->fields( "contact" );
									$iderp 				= $rs->fields( "iderp" );
									$zipcode 			= $rs->fields( "zipcode" );
									$init 				= $rs->fields( "initial" );
									
									if( empty( $company ) )
										$company = $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
									
								?>
								<tr class="amountRow" id="rowForAmount_<?php echo $total_amount ?>">
									<td class="lefterCol"><?php echo $init ?></td>
									<td style="white-space:nowrap;">
										<a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $idbilling_buyer ?>" onclick="window.open(this.href); return false;">
								   			<?php echo $idbilling_buyer ?>
										</a>
									</td>
									<td>
										<?
										$hasLitigGraves = DBUtil::query("SELECT count(idbilling_buyer) as compte FROM billing_buyer WHERE litigious = 1 AND idbuyer = $idbuyer")->fields('compte');
										if($hasLitigGraves){
											?><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/litigation.png" alt="Litige" style="vertical-align:middle;" />
										<?php } ?>
										<a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idbuyer ?>">
									 		<?php echo $iderp == null ? $idbuyer : "$iderp / $idbuyer" ?>
							  			</a>
									</td>
									<td>
										<?php echo substr( $zipcode, 0, 2 ) ?>
									</td>
									<td>
										<a class="grasBack" href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idbuyer ?>"><?php echo htmlentities( $company ) ?></a>
									</td>
									<td style="width:8%"><?php echo Dictionnary::translate( $status ); ?> <?php echo $status_plus ?></td>
									<td style="width:8%"><?php echo usDate2eu( substr( $date, 0, 10 ) ); ?></td>
									<td style="text-align:right; white-space:nowrap; width:6%"><?php echo Util::priceFormat( $total_amount_ht ) ?></td>
									<td style="text-align:right; white-space:nowrap; width:6%"><?php echo Util::priceFormat( $total_amount ) ?></td>
									<td>
										<ul style="list-style-type:none; margin:0px; padding:0px;">
											<?php
												$deliveryNotesRS =& DBUtil::query( "SELECT idbl_delivery FROM bl_delivery WHERE idbilling_buyer = '$idbilling_buyer'" );
												while( !$deliveryNotesRS->EOF() ){
													$idbl_delivery = $deliveryNotesRS->fields( "idbl_delivery" );
													?>
													<li style="padding:0px; margin:0px;"><a href="<?php echo $GLOBAL_START_URL ?>/catalog/pdf_delivery.php?iddelivery=<?php echo $idbl_delivery ?>" onclick="window.open(this.href); return false;"><?php echo $idbl_delivery;?></a></li>
													<?php
													$deliveryNotesRS->MoveNext();
												}
											?>
										</ul>
									</td>
									<td>
										<ul style="list-style-type:none; margin:0px; padding:0px;">
											<?php
												$creditsRS =& DBUtil::query( "SELECT idcredit FROM credits WHERE idbilling_buyer = '$idbilling_buyer'" );
												while( !$creditsRS->EOF() ){
													$idcredit = $creditsRS->fields( "idcredit" );
													?>
													<li style="padding:0px; margin:0px;"><a href="<?php echo "/catalog/pdf_credit.php?idcredit=$idcredit"; ?>" onclick="window.open(this.href); return false;"><?php echo $idcredit ?></a></li>
													<?php
													$creditsRS->MoveNext();
												}
											?>
										</ul>
									</td>
									<td class="righterCol" style="white-space:nowrap;">
									<?php
										
										$rsLitigation =& DBUtil::query( "SELECT idlitigation FROM litigations_billing_buyer WHERE idbilling_buyer = '$idbilling_buyer' LIMIT 1" );
										if( $rsLitigation->RecordCount() ){
											
											?>
											<a href="#" onclick="window.open( '<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $rsLitigation->fields( "idlitigation" ) ?>&typeLitigation=billing_buyer' ); return false;" style="color:#EB6A0A;">Litige n°<?php echo $rsLitigation->fields( "idlitigation" ) ?></a>
											<?php
											
										}
										
									?>
									</td>
									<?if($allowModifyInvoice){?>
										<td class="righterCol">
			                        		<a href="<?php echo $GLOBAL_START_URL ?>/accounting/modify_invoice.php?IdInvoice=<?php echo $idbilling_buyer ?>" target="_blank">
			                        			<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="" />
			                        		</a>
			                        	</td>
			                        	<td class="righterCol"> 
			                        		<?
			                        		if($status == 'Paid'){
			                        			?>
			                        			<input type="checkbox" name="updateNotPaid[]" value="<?php echo $idbilling_buyer ?>">
			                        			<?
			                        		}
			                        		?>
			                        	</td>
		                        	<?php } ?>
								</tr>
								<?php
									
									// données statistiques--------------------------------------------------------------
									$invoiceCount++;
									$totalAmountHT += $total_amount_ht;
									$totalAmount += $total_amount;
									
									$netMarginAmount += $rs->fields( "net_margin_amount" );
									if( $status == Invoice::$STATUS_PAID ){
										$paidCount++;
										$paidAmount += $total_amount;
									}
									else if( $status == Invoice::$STATUS_INVOICED ){
										$invoicedCount++;
										$invoicedAmount += $total_amount;
									}
									
									$rs->MoveNext();
									
								}
								
								?>

								<tr>
									<th colspan="9" style="border-style:none;"></th>
									<th class="totalAmount"><?php echo Util::priceFormat( $totalAmountHT ) ?></th>
									<th class="totalAmount"><?php echo Util::priceFormat( $totalAmount ) ?></th>
									<?if($allowModifyInvoice){?>
										<th colspan="4" style="border-style:none;"></th>
										<th><input type="submit" class='blueButton' value='ok' name="updateBils" onclick="return confirm('Attention les règlements des factures sélectionnées seront supprimés sauf l\'acompte!');"></th>
									<?php } else{?>
										<th colspan="3" style="border-style:none;"></th>
									<?php } ?>
								</tr>
							</tbody>
						</table>
					</div>
	           	</div>
	   		</div>
	        <div class="bottomRight"></div><div class="bottomLeft"></div>
	    </div>
		<!-- Tableau de Stats -->
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle">
					<p class="title">Statistiques factures</p>
					<div class="rightContainer"></div>
				</div>
				<div class="subContent">
					<!-- tableau résultats recherche -->
					<div class="resultTableContainer clear">
						<table class="dataTable resultTable">
							<thead>
								<tr>
									<th>Statut</th>
									<th>En attente de paiement</th>
									<th>Total payé TTC</th>
									<th>Total facturé TTC</th>
									<th>Marge nette</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="width:20%" class="lefterCol"><?php echo Dictionnary::translate("gest_com_quantity"); ?></td>
									<td style="width:20%"><?php echo $invoicedCount ?></td>
									<td style="width:20%"><?php echo $paidCount ?></td>
									<td style="width:20%"><?php echo $invoiceCount ?></td>
									<?php $netMarginRate = $totalAmount > 0.0 ? $netMarginAmount / $totalAmount * 100 : 0.0; ?>
									<td style="width:20%" class="righterCol"><?php echo Util::rateFormat( $netMarginRate ); ?></td>
								</tr>
								<tr>
									<td class="lefterCol"><?php echo Dictionnary::translate("gest_com_amount"); ?></td>
									<td><?php echo Util::priceFormat( $invoicedAmount ); ?></td>
									<td><?php echo Util::priceFormat( $paidAmount ); ?></td>
									<td><?php echo Util::priceFormat( $totalAmount ); ?></td>
									<td class="righterCol"><?php echo Util::priceFormat( $netMarginAmount ); ?></td>
								</tr>
							</tbody>
						</table>
					</div>