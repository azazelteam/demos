<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement des règlements clients
 */
 
//------------------------------------------------------------------------------------------------
include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
// ------- Mise à jour de l'id gestion des Index  #1161
include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php" );
$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->get( "iduser" );

$resultPerPage = 50000;
$maxSearchResults = 1000;

$use_help = DBUtil::getParameterAdmin( "gest_com_use_help" ); 
//------------------------------------------------------------------------------------------
/* recherche ajax */
/*
if( isset($_POST["registerEstimates"])){
	$estimateRowsToHideArray = array();
	//---------------------------------------------------------------------------------
	// Devis acomptes
	$myEstimatesDP = isset($_POST["estimatesToCheckForDP"])?$_POST["estimatesToCheckForDP"]:array();
	$i=0;
	
	if( count($myEstimatesDP)>0 ){
		while( $i<count($myEstimatesDP) ){
			$estimate 		= $myEstimatesDP[$i];
			
			$payment_date 	= euDate2us($_POST["es_down_payment_date_$estimate"]);
			$idbank			= $_POST["es_down_payment_idbank_$estimate"];
			$piece			= $_POST["es_down_payment_piece_$estimate"];
			$comment		= $_POST["es_down_payment_comment_$estimate"];
			$idpayment		= $_POST["es_down_payment_idpayment_$estimate"];
			$amount			= $_POST["es_down_payment_amount_$estimate"];
			$idbuyer		= $_POST["es_down_payment_idbuyer_$estimate"];
			
			if( $payment_date!="" && $payment_date!="0000-00-00" && $idbank!=0 && $piece!="" && $idpayment!=0 && $amount!="" && $idbuyer!=""){
				//on créé le vrai règlement et on dit au devis qu'il est confirmé en lui passant l'idregulations_buyer
				createMyRegulation($estimate,"estimate",$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,1);

				array_push( $estimateRowsToHideArray , $estimate );
			}
			
			$i++;
		}
	}
	
	
	//--------------------------------------------------------------------------------
	//Devis cash
	$myEstimatesCP = isset($_POST["estimatesToCheckForCP"])?$_POST["estimatesToCheckForCP"]:array();
	$i=0;
	
	if(count($myEstimatesCP)>0){
		while( $i<count($myEstimatesCP) ){
			$estimate 		= $myEstimatesCP[$i];
			
			$payment_date 	= euDate2us($_POST["es_cash_payment_date_$estimate"]);
			$idbank			= $_POST["es_cash_payment_idbank_$estimate"];
			$piece			= $_POST["es_cash_payment_piece_$estimate"];
			$comment		= $_POST["es_cash_payment_comment_$estimate"];
			$idpayment		= $_POST["es_cash_payment_idpayment_$estimate"];
			$amount			= $_POST["es_cash_payment_amount_$estimate"];
			$idbuyer		= $_POST["es_cash_payment_idbuyer_$estimate"];
			
			if( $payment_date!="" && $payment_date!="0000-00-00" && $idbank!=0 && $piece!="" && $idpayment!=0 && $amount!="" && $idbuyer!=""){
				//on créé le vrai règlement et on dit au devis qu'il est confirmé en lui passant l'idregulations_buyer
				createMyRegulation($estimate,"estimate",$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment);

				array_push( $estimateRowsToHideArray , $estimate );
			}
			
			$i++;
		}
	}
	
	exit( implode(',',$estimateRowsToHideArray) );
}
*/

if(isset($_POST["registerOrders"])){
	$orderRowsToHideArray=array();
	
	
	//---------------------------------------------------------------------------------
	// Commandes acomptes
	$myOrdersDP = isset($_POST["ordersToCheckForDP"])?$_POST["ordersToCheckForDP"]:array();

	$i=0;
	
	if(count($myOrdersDP)>0){
		while( $i<count($myOrdersDP) ){
			$order	 		= $myOrdersDP[$i];
			
			$payment_date 	= euDate2us($_POST["o_down_payment_date_$order"]);
			$idbank			= $_POST["o_down_payment_idbank_$order"];
			$piece			= $_POST["o_down_payment_piece_$order"];
			$comment		= $_POST["o_down_payment_comment_$order"];
			$idpayment		= $_POST["o_down_payment_idpayment_$order"];
			$amount			= $_POST["o_down_payment_amount_$order"];
			$idbuyer		= $_POST["o_down_payment_idbuyer_$order"];
			
			if( $payment_date!="" && $payment_date!="0000-00-00"  && $idpayment!=0 && $amount!="" && $idbuyer!=""){
				//on créé le vrai règlement et on dit au devis qu'il est confirmé en lui passant l'idregulations_buyer
				createMyRegulation($order,"order",$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,1);

				array_push( $orderRowsToHideArray , $order );
			}
			
			$i++;
		}
	}
	
	
	//--------------------------------------------------------------------------------
	//Commandes cash	
	$myOrdersCP = isset($_POST["ordersToCheckForCP"])?$_POST["ordersToCheckForCP"]:array();

	$i=0;
	
	if(count($myOrdersCP)>0){
		while( $i<count($myOrdersCP) ){
			$order	 		= $myOrdersCP[$i];
			
			$payment_date 	= euDate2us($_POST["o_cash_payment_date_$order"]);
			$idbank			= $_POST["o_cash_payment_idbank_$order"];
			$piece			= $_POST["o_cash_payment_piece_$order"];
			$comment		= $_POST["o_cash_payment_comment_$order"];
			$idpayment		= $_POST["o_cash_payment_idpayment_$order"];
			$amount			= $_POST["o_cash_payment_amount_$order"];
			$idbuyer		= $_POST["o_cash_payment_idbuyer_$order"];
			
			if( $payment_date!="" && $payment_date!="0000-00-00" && $idpayment!=0 && $amount!="" && $idbuyer!=""){
				//on créé le vrai règlement et on dit au devis qu'il est confirmé en lui passant l'idregulations_buyer
				createMyRegulation($order,"order",$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment);

				array_push( $orderRowsToHideArray , $order );
			}
			
			$i++;
		}
	}
	
	
	//--------------------------------------------------------------------------------
	
	exit( implode(',',$orderRowsToHideArray) );
}


if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	search();
	
	exit();
		
}


$Title = "Enregistrement des paiements client";

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------



?>

<div id="globalMainContent">
<?php

	displayForm();
		
	?>
	
</div> <!-- GlobalMainContent -->
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------------------

function paymentList1( $j, $selected_value = "" ){
	
	$lang = User::getInstance()->getLang();
	
	echo "<select name=\"idpayment_$j\" id=\"idpayment_$j\" class=\"idpayment\">";
	
	$rs =& DBUtil::query( "SELECT idpayment, name$lang FROM payment ORDER BY name$lang ASC" );
	
	if( $rs->RecordCount() > 0 ){
		
		for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
			
			$selected = "";
			
			include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
			
			if( $selected_value == "" && $rs->fields( "idpayment" ) == Payment::$PAYMENT_MOD_TRANSFER )
				$selected = " selected=\"selected\"";
			elseif( $rs->fields( "idpayment" ) == $selected_value )
				$selected = " selected=\"selected\"";
			else
				$selected = "";
			
			echo "<option value=\"" . $rs->fields( "idpayment" ) . "\"$selected>" . $rs->fields( "name$lang" ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}
	
    echo "</select>";
	
}
//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$date_type 			= isset( $_POST[ "date_type" ] ) 		 ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 	= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 	= isset( $_POST[ "interval_select" ] ) 	 ? $_POST[ "interval_select" ] : 3;
	$idbuyer 			= isset( $_POST[ "idbuyer" ] ) 			 ? $_POST[ "idbuyer" ] : "";
	$lastname 			= isset( $_POST[ "lastname" ] ) 		 ? stripslashes( $_POST[ "lastname" ] ) : "";
	$company 			= isset( $_POST[ "company" ] ) 			 ? stripslashes( $_POST[ "company" ] ) : "";
	$zipcode 			= isset( $_POST[ "zipcode" ] ) 			 ? $_POST[ "zipcode" ] : "";
	$faxnumber 			= isset( $_POST[ "faxnumber" ] ) 		 ? $_POST[ "faxnumber" ] : "";
	$idsource 			= isset( $_POST[ "idsource" ] ) 		 ? $_POST[ "idsource" ] : "";
	$iduser 			= isset( $_POST[ "iduser" ] ) 			 ? $_POST[ "iduser" ] : "";
	$idorder 			= isset( $_POST[ "idorder" ] ) 			 ? $_POST[ "idorder" ] : "";
	$idbl_delivery 		= isset( $_POST[ "idbl_delivery" ] ) 	 ? $_POST[ "idbl_delivery" ] : "";
	$idbilling_buyer 	= isset( $_POST[ "idbilling_buyer" ] ) 	 ? $_POST[ "idbilling_buyer" ] : "";
	$status 			= isset( $_POST[ "status" ] ) 			 ? stripslashes( $_POST[ "status" ] ) : "";
	$total_amount_ht 	= isset( $_POST[ "total_amount_ht" ] )	 ? $_POST[ "total_amount_ht" ] : "";
	$total_amount 		= isset( $_POST[ "total_amount" ] ) 	 ? $_POST[ "total_amount" ] : "";
	$city		 		= isset( $_POST[ "city" ] ) 			 ? $_POST[ "city" ] : "";
	$siret				= isset( $_POST[ "siret" ] )			 ? $_POST[ "siret" ] : "";
	?>
	
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
		
			 var options = {
			 	
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  				
			};

			
			$('#SearchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
			
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				$.growlUI( '', 'Impossible d\'effectuer la recherche' );
				return;
				
			}
			
			$('#SearchResults').html( responseText );
			
			selectLastActiveTab();
			
		}
		

		/* ----------------------------------------------------------------------------------- */
		
		function selectLastActiveTab() {}
		
		/* ----------------------------------------------------------------------------------- */

		
		function goForm(tab){
			
			var options = {				
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
			};				

			if (tab !== undefined) {	
				$("#SearchForm input[name=targetTab]").val(tab); //Select the 2nd tab (reglements)
			}
		 
		    // bind to the form's submit event 
		    $('#SearchForm').ajaxSubmit(options); 

		}
		
		
	/* ]]> */
	</script>
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}
		
		function buyerOtherPayment(){
			paymenthref='<?=$GLOBAL_START_URL?>/accounting/register_buyer_other_payment.php';
		
			postop = (self.screen.height-450)/2;
			posleft = (self.screen.width-450)/2;
	
			window.open(paymenthref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=950,height=500,resizable,scrollbars=yes,status=0');
		}		
	// -->
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
	<a name="topPage"></a>
	<div class="mainContent">
    <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
        <div class="topRight"></div><div class="topLeft"></div>        
        <div class="content">
        	<form action="<?php echo $GLOBAL_START_URL ?>/accounting/payment_registration.php" method="post" name="searchForm" id="SearchForm">
            	<div class="headTitle">
                    <p class="title">Rechercher de commandes / factures</p>
                    <div class="rightContainer">
                    	<input type="radio" name="date_type" class="VCenteredWithText" value="DateHeure"<?php if( $date_type == 'DateHeure') echo " checked=\"checked\""; ?> />Date de création
                        <input type="radio" name="date_type" class="VCenteredWithText" value="balance_date"<?php if( $date_type == 'balance_date') echo " checked=\"checked\""; ?>/>Date d'échéance
                    </div>
                </div>
            	<div class="subContent">
					<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
					<input type="hidden" name="search" value="1" />
					<input type="hidden" name="letter" id="letter" value="" />
	                <input type="hidden" name="targetTab" />
                	<!-- tableau de recherche -->
                    <div class="tableContainer">
                    	
                    	<script type="text/javascript">
                    		function showRow( rowToShow ){
                    			$( '#tableSearch' ).find( '.searchDoc' ).each( function(){
                    				$(this).hide();
                    			});
                    			
                    			$( '#' + rowToShow ).show();
                    		}
                    	</script>
                    	                    	
                        <table class="dataTable" id="tableSearch">
                        
                    		<tr class="filledRow">
                    			<th>Document</th>
                    			<td colspan="3">
		                    		<input type="radio" name="documentSearch" value="billing" onclick="showRow( 'TRidbilling_buyer' );" checked="checked">&nbsp;Facture
		                    		<input type="radio" name="documentSearch" value="order" onclick="showRow( 'TRidorder' );">&nbsp;Commande
		                    		<?php /*<input type="radio" name="documentSearch" value="estimate" onclick="showRow( 'TRidestimate' );">&nbsp;Devis*/ ?>
		                    	</td>
	                    	</tr>
	                    	
                            <tr>
                            	<td colspan="4" class="tableSeparator"></td>
                            </tr>
                            
                            <tr class="filledRow searchDoc" id="TRidbilling_buyer">                          
                                <th>Facture n°</th>
                                <td colspan="3">
                                	<input type="text" class="textInput" name="idbilling_buyer"  value="" />
                                </td>
                            </tr>
                            
                            <tr class="filledRow searchDoc" id="TRidorder" style="display:none;">
                                <th>Commande n°</th>
                                <td colspan="3">
                                	<input type="text" class="textInput" name="idorder"  value="" />
                                </td>
                            </tr>
                            <?php
                            /*
                            <tr class="filledRow searchDoc" id="TRidestimate" style="display:none;">   
                                <th>Devis n°</th>
                                <td colspan="3">
                                	<input type="text" class="textInput" name="idestimate"  value="" />
                                </td>                               
                            </tr>
                            */ 
                            ?>
                            <tr>
                            	<td colspan="4" class="tableSeparator"></td>
                            </tr>
                            <tr>
                                <th><input type="radio" name="interval_select" id="interval_select_since" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> <?php echo Dictionnary::translate("order_since") ; ?></th>
                                <td>
                                    <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
						            	<option value="0"<?php if( $minus_date_select == 0 ) echo " selected=\"selected\""; ?>>Aujourd'hui</option>
						              	<option value="-1"<?php if( $minus_date_select == -1 ) echo " selected=\"selected\""; ?>>Hier</option>
					              		<option value="-2"<?php if( $minus_date_select == -2 ) echo " selected=\"selected\""; ?>>Moins de 2 jours</option>
						              	<option value="-7"<?php if( $minus_date_select == -7 ) echo " selected=\"selected\""; ?>>Moins de 1 semaine</option>
						              	<option value="-14"<?php if( $minus_date_select == -14 ) echo " selected=\"selected\""; ?>>Moins de 2 semaines</option>
						              	<option value="-30"<?php if( $minus_date_select == -30 ) echo " selected=\"selected\""; ?>>Moins de 1 mois</option>
						              	<option value="-60"<?php if( $minus_date_select == -60 ) echo " selected=\"selected\""; ?>>Moins de 2 mois</option>
						              	<option value="-90"<?php if( $minus_date_select == -90 ) echo " selected=\"selected\""; ?>>Moins de 3 mois</option>
						              	<option value="-180"<?php if( $minus_date_select == -180 ) echo " selected=\"selected\""; ?>>Moins de 6 mois</option>
						              	<option value="-365"<?php if( $minus_date_select == -365 ) echo " selected=\"selected\""; ?>>Moins de 1 an</option>
						              	<option value="-730"<?php if( $minus_date_select == -730 ) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
						              	<option value="-1825"<?php if( $minus_date_select == -1825 ) echo " selected=\"selected\""; ?>>Moins de 5 ans</option>
						            </select>
                                </td>
                                <th><input type="radio" name="interval_select" id="interval_select_month" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> <?php echo Dictionnary::translate("order_month") ; ?></th>
                                <td>
									<?php FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
								</td>
                            </tr>
                            <tr>
                                <th><input type="radio" name="interval_select" id="interval_select_between" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> style="vertical-align:bottom;" /> <?php echo Dictionnary::translate("order_between") ; ?></th>
                                <td>
									<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
					 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
					 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                </td>
                                <th>et le</th>
                                <td>
					 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
					 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                </td>
                            </tr>
                            <tr><td colspan="4" class="tableSeparator"></td></tr>
                            <tr class="filledRow">
                                <th>Client n°</th>
                                <td>
                                   <input type="text" name="idbuyer" value="<?php echo $idbuyer ?>" class="textInput" />
                                </td>
                                <th>Nom / Raison sociale</th>
                                <td>
                                    <input type="text" name="company" value="<?php echo $company ?>" class="textInput" />
                                </td>
                            </tr>
                            <tr class="filledRow">
                                <th>Nom du contact<br /><span style="font-weight:normal;">(sauf règlements partiels)</span></th>
                                <td>
                                   <input type="text" name="lastname" value="<?php echo $lastname ?>" class="textInput" />
                                </td>
                                <th>Groupement</th>
                                <td>
                                	<?php XHTMLFactory::createSelectElement( "grouping", "idgrouping", "grouping", "grouping", isset( $_POST[ "idgrouping" ] ) ? $_POST[ "idgrouping" ] : false, "", "-", "name=\"idgrouping\"" ); ?></td>
                                </td>
                            </tr>
                            <tr class="filledRow">
                                <th>Code postal</th>
                                <td>
                                   <input type="text" name="zipcode" value="<?php echo $zipcode ?>" class="textInput" />
                                </td>
                                <th>Ville</th>
                                <td>
                                    <input type="text" name="city" value="<?php echo $city ?>" class="textInput" />
                                </td>
                            </tr>
                            <tr>
                                <th>Total facturé HT<br /><span style="font-weight:normal;">(sauf règlements partiels)</span></th>
                                <td>
                                    <input type="text" name="total_amount_ht"  value="<?php echo $total_amount_ht ?>" class="textInput" />
                                </td>
                                <th>Total facturé / réglé TTC</th>
                                <td>
                                   <input type="text" name="total_amount"  value="<?php echo $total_amount ?>" class="textInput" />
                                </td>
                            </tr>
                            <tr>
                            	<td colspan="4" class="tableSeparator"></td>
                            </tr>
                            
                        </table>
                    </div><!-- tableContainer -->
                    <div class="submitButtonContainer">
                    	<input type="submit" class="blueButton" style="text-align: center;" value="Rechercher" />
                    </div>
            	</div><!-- subContent -->
            </form>
        </div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content">
            	<?
            	// OH MON DIEU QU'EST CE QUE C'EST QUE CA ?!?!?!? CA VA PAS NON ?! -->
            	//<a href='#' onclick="buyerOtherPayment();return false;" style="margin-left:5px;">Enregistrer un règlement supplémentaire</a>
            	// Fonction servant à remplire le credit_balance d'un buyer ^^ , on aura tout vu...
            	?>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>
    <div id="SearchResults"></div>
<?php 
	
}

function search(){
	
	global	$resultPerPage,
			$hasAdminPrivileges,
			$GLOBAL_START_URL;
	
	
	$now = getdate();
	
	$date_type 				= isset( $_POST[ "date_type" ] )			? $_POST[ "date_type" ]					: "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] )	? $_POST[ "minus_date_select" ]			: "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] )		? $_POST[ "interval_select" ]			: 1;
	$idbuyer 				= isset( $_POST[ "idbuyer" ] )				? $_POST[ "idbuyer" ]					: "";
	$company 				= isset( $_POST[ "company" ] )				? stripslashes( $_POST[ "company" ] )	: "";
	$total_amount_ht		= isset( $_POST[ "total_amount_ht" ] )		? $_POST[ "total_amount_ht" ]			: "";
	$total_amount			= isset( $_POST[ "total_amount" ] )			? $_POST[ "total_amount" ]				: "";
	$city		 			= isset( $_POST[ "city" ] )					? $_POST[ "city" ]						: "";
	$siret					= isset( $_POST[ "siret" ] )				? $_POST[ "siret" ]						: "";
	$lastname 				= isset( $_POST[ "lastname" ] )				? stripslashes( $_POST[ "lastname" ] )	: "";
	$zipcode 				= isset( $_POST[ "zipcode" ] )				? $_POST[ "zipcode" ]					: "";
	$letter					= isset( $_POST[ "letter" ] )				? $_POST[ "letter" ]					: "";
	$idgrouping				= isset( $_POST[ "idgrouping" ] )			? $_POST[ "idgrouping" ]				: "";


	?>
	
			<script type="text/javascript">
			<!--
				
				function popupPartiallyPayment( irb ){
					
					postop = (self.screen.height-450)/2;
					posleft = (self.screen.width-450)/2;
					
					window.open('register_partially_buyer_payment.php?irb='+irb, '_blank', 'top=' + postop + ',left=' + posleft + ',width=450,height=450,resizable,scrollbars=yes,status=0');
					
				}
				
				function registerPayment(idbuyer,idgroup){
					
					if(idgroup == 0){
						var checkboxes = document.forms.invoicetopay.elements[ 'group_'+idbuyer ];
					}else{
						var checkboxes = document.forms.invoicetopay.elements[ 'group_'+idgroup ];
					}
					
					var checked = false;
					var i = 0;
					
					if(idgroup==0){
						var paymenthref = 'register_buyer_payment.php?buyer='+idbuyer+'&billings=';
					}else{
						var paymenthref = 'register_buyer_payment.php?idgrouping='+idgroup+'&buyer='+idbuyer+'&billings=';
					}
					
					if( !checkboxes.length ){
						
						if(checkboxes.checked){
							
							paymenthref = paymenthref+checkboxes.value+',';
							
							checked = true;
						}
						
					}
					
					while (i<checkboxes.length){ 
					
						if( checkboxes[ i ].checked ){
							
							paymenthref = paymenthref+checkboxes[ i ].value+',';
							
							checked = true;
						}
						
						i++;
					}
					
					if( !checked ){
						alert( 'Veuillez sélectionner au minimum une facture pour enregistrer son règlement' );
					}else{
						var lg = paymenthref.length;
						paymenthref=paymenthref.substring(0,lg-1);
						
						postop = (self.screen.height-450)/2;
						posleft = (self.screen.width-450)/2;
			
						window.open(paymenthref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=950,height=500,resizable,scrollbars=yes,status=0');
					}			
				}
						
				var optionsPartialPayment = {
				 	
					beforeSubmit:	preSubmitCallbackPartialPayment,  // pre-submit callback 
					success:		postSubmitCallbackPartialPayment  // post-submit callback
						
				};
							
				$('#partially').ajaxForm( optionsPartialPayment );
			
			
				function preSubmitCallbackPartialPayment( formData, jqForm, options ){
					
					var checkboxes = document.getElementById('partially').elements[ 'partialPayments[]' ];
				
					var checked = false;
					var i = 0;
			
					while ( i < checkboxes.length){ 
					
						if( checkboxes[ i ].checked ){
												
							checked = true;
						}
						
						i++;
					}
				
					if( !checked ){
					
						$.blockUI({
				
							message: "Aucune case n'a été cochée !",
							css: { padding: '15px', cursor: 'pointer', 'background-color':'#F00', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
							fadeIn: 0, 
							fadeOut: 700
								
						});
						setTimeout('$.unblockUI()',1000);
						
						return false;
					}else{
					
						$.blockUI({
				
							message: "Enregistrement en cours",
							css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
							fadeIn: 0, 
							fadeOut: 700
								
						});
				
						$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					}			
								
					
						
				}
					
				/* ----------------------------------------------------------------------------------- */
			
				function postSubmitCallbackPartialPayment( responseText, statusText ){ 
					
					$.unblockUI();
					
					if( statusText != 'success' || responseText == '0' ){
						
						$.growlUI( '', responseText );
						return;
						
					}else{
						$.growlUI( '', 'Enregistrement effectué avec succès' );
						
						goForm();
								
						return;
					}
					
				}		
			
				/* ----------------------------------------------------------------------------------- */
				
				function acceptValidation( myForm ){
					
						if(myForm=='registerEstimate'){
							var myoptions = {
								success:		alerteMoiLaVeriteEstimate  // post-submit callback
							};
						}
						
						if(myForm=='registerOrder'){	
							var myoptions = {
								success:		alerteMoiLaVeriteOrder  // post-submit callback
							};				
						}
					 
					 	if( myForm=='registerEstimate' || myForm=='registerOrder' ){
						    // bind to the form's submit event 
						    $('#'+myForm).ajaxSubmit(myoptions);
						}else{
							$.growlUI( '', 'Formulaire inconnu !' );
						}
					
				}
				
				function alerteMoiLaVeriteEstimate( responseText, statusText ){
							
					if(responseText.length > 0 && responseText.indexOf('Error')==-1){
						var myRowsToDelete = responseText.split(',');
						
						for(i=0 ; i<myRowsToDelete.length ; i++){
						
							idestimate = myRowsToDelete[i];
							$('#estimateRow_'+idestimate).remove();
						}
						
						$.growlUI( '', 'Traitement effectué avec succès' );
					
					}else{
						if(responseText.length == 0){
							$.growlUI( '', 'Aucun devis traité, verifiez que vous avez bien rempli les champs obligatoires' );
						}
					}
					
					
					return;
				}
				
				function alerteMoiLaVeriteOrder( responseText, statusText ){
					
					if(responseText.length > 0 && responseText.indexOf('Error')==-1){
						var myRowsToDelete = responseText.split(',');
						
						for(i=0 ; i<myRowsToDelete.length ; i++){
							idorder = myRowsToDelete[i];
							$('#orderRow_'+idorder).remove();
						}
						
						$.growlUI( '', 'Traitement effectué avec succès' );
					}else{
						if(responseText.length == 0){
							$.growlUI( '', 'Aucune commande traitée, verifiez que vous avez bien rempli les champs obligatoires' );
						}
					}
						
					return;
				}

				// Tab 2 Added by Behzad
				function registerPaymentTab2(idbuyer,idgroup){
					
					if(idgroup == 0){
						var checkboxes = document.forms.invoicetopaytab2.elements[ 'group_'+idbuyer ];
						var idpayment = document.forms.invoicetopaytab2.elements[ 'idpayment_'+idbuyer ];
						var solde = document.forms.invoicetopaytab2.elements[ 'solde_'+idbuyer ];
						// console.log(solde);
						var idregulations_buyer = document.forms.invoicetopaytab2.elements[ 'idregulations_buyer_'+idbuyer ];
					}else{
						var checkboxes = document.forms.invoicetopaytab2.elements[ 'group_'+idgroup ];
						var idpayment = document.forms.invoicetopaytab2.elements[ 'idpayment_'+idgroup ];
						var solde = document.forms.invoicetopaytab2.elements[ 'solde_'+idgroup ];
						var idregulations_buyer = document.forms.invoicetopaytab2.elements[ 'idregulations_buyer_'+idgroup ];
					}
					var checked = false;
					var i = 0;
					var idpaymenthref = '';
					var soldehref = '';
					var idregulations_buyerhref = '';
					
					if(idgroup==0){
						var paymenthref = 'register_buyer_payment.php?tab2=1&buyer='+idbuyer+'&billings=';
					}else{
						var paymenthref = 'register_buyer_payment.php?tab2=1&idgrouping='+idgroup+'&buyer='+idbuyer+'&billings=';
					}
					
					if( !checkboxes.length ){
						
						if(checkboxes.checked){
							
							paymenthref = paymenthref+checkboxes.value+',';
							idpaymenthref = idpaymenthref+idpayment.value+',';//for sending select/options values
							soldehref = soldehref+solde.value+',';//for sending select/options values
							idregulations_buyerhref = idregulations_buyerhref+idregulations_buyer.value+',';//for sending select/options values
							
							checked = true;
							
						}
						
					}
					
					while (i<checkboxes.length){ 
					
						if( checkboxes[ i ].checked ){
							
							paymenthref = paymenthref+checkboxes[ i ].value+',';
							idpaymenthref = idpaymenthref+idpayment[ i ].value+',';
							soldehref = soldehref+solde[ i ].value+',';
							idregulations_buyerhref = idregulations_buyerhref+idregulations_buyer[ i ].value+',';
							checked = true;
						}
						
						i++;
					}
					
					if( !checked ){
						alert( 'Veuillez sélectionner au minimum une facture pour enregistrer son règlement' );
					}else{
						var lg = paymenthref.length;
						paymenthref=paymenthref.substring(0,lg-1);//for removing last comma from end of string','
						//console.log(idregulations_buyerhref);
												
						var lg_idpayment = idpaymenthref.length;
						idpaymenthref=idpaymenthref.substring(0,lg_idpayment-1);
						
						var lg_solde = soldehref.length;
						soldehref=soldehref.substring(0,lg_solde-1);
						
						var lg_idregulations_buyer = idregulations_buyerhref.length;
						idregulations_buyerhref=idregulations_buyerhref.substring(0,lg_idregulations_buyer-1);
						
						
						paymenthref =  paymenthref+'&idpayment='+idpaymenthref;
						paymenthref =  paymenthref+'&solde='+soldehref;
						paymenthref =  paymenthref+'&idregulations_buyer='+idregulations_buyerhref;
						
						postop = (self.screen.height-450)/2;
						posleft = (self.screen.width-450)/2;
						$.ajax({
					 		url: paymenthref,
						});
						
						goForm(1); // Ajax Submit the form and autoselect the 2nd tab (reglements)
						//$("#SearchForm").submit(); // Auto-submit the search form to refresh the page with new data
						$.growlUI('Règlement(s) modifié(s) avec succès.');
						
						//DIVs
						//setTimeout('$("div#factures").addClass("ui-tabs-hide");$("div#reglements").removeClass("ui-tabs-hide");',4000);
							
						// Li active Tab
						//setTimeout('$("ul.ui-tabs-nav li").removeClass("ui-tabs-selected");$("li#reglements_li").addClass("ui-tabs-selected");',4000);
						
						//Show message
						//setTimeout("$.growlUI('Règlement(s) modifié(s) avec succès.')",5000);
						

						
						//window.open(paymenthref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=950,height=500,resizable,scrollbars=yes,status=0');
					}			
				}
				
		/* ----------------------------------------------------------------------------------- */
				
			// -->
			</script>
			
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div id="container">
			<ul class="menu">
				<?php
				switch( $_POST[ 'documentSearch' ] ){
					case 'billing':
						?>
						<li class="item">
							<div class="right"></div><div class="left"></div>
							<div class="centered"><a href="#factures" id="factureMenu_">Factures</a></div>
							
						</li>
						<li class="item">
							<div class="right"></div><div class="left"></div>
							<div class="centered"><a href="#reglements" id="reglements_">Règlements partiels</a></div>
							
						</li><?php
						break;
					
					case 'estimate':
						?>
						<li class="item">
							<div class="right"></div><div class="left"></div>
							<div class="centered"><a href="#estimate" id="PDevisMenu">Paiements sur Devis</a></div>
						</li><?php
						break;
					
					case 'order':
						?>
						<li class="item">
							<div class="right"></div><div class="left"></div>
							<div class="centered"><a href="#commandes" id="PCdeMenu">Paiements sur Commandes</a></div>
						</li><?php
						break;
				}?>
			</ul>
	<?
	
	switch( $_POST[ 'documentSearch' ] ){
		
		case 'billing':
	
			$idbilling_buyer 		= isset( $_POST[ "idbilling_buyer" ] )		? $_POST[ "idbilling_buyer" ]			: "";
			
			if(isset( $_POST[ "letter" ] ) &&  $_POST[ "letter" ]=="Tous"){
				$letter="";
			}
			
			switch( $interval_select ){
				
				case 1:
				 	//min |2000-12-18 15:25:53|
					$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
				 	//max
					$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
					//sql_condition
					$SQL_Condition  = "`billing_buyer`.$date_type >='$Min_Time' AND `billing_buyer`.$date_type <='$Now_Time' ";
					
					break;
					
				case 2:
					
					$yearly_month_select = $_POST['yearly_month_select'];
					
					if ($yearly_month_select > $now["mon"])
						$now["year"] --;
			
				 	//min
					$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
				 	//max
					$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
				
					//sql_condition
					$SQL_Condition  = "`billing_buyer`.$date_type >='$Min_Time' AND `billing_buyer`.$date_type <'$Max_Time' ";
			     	break;
					     
				case 3:
				 	
				 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
				 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
				 	
					$SQL_Condition  = "`billing_buyer`.$date_type >= '$Min_Time' AND `billing_buyer`.$date_type <= '$Max_Time' ";
					
			     	break;
					
			}
			
			if( !$hasAdminPrivileges )
				$SQL_Condition .= " AND ( buyer.iduser = '$iduser' OR `billing_buyer`.iduser = '$iduser' )";
								
			if ( !empty($idbuyer) ){
			
			//		$idbuyer = FProcessString($idbuyer);
					$SQL_Condition .= " AND buyer.idbuyer ='$idbuyer'";
			}
			
			if ( !empty($idbilling_buyer) ){
					//$idbilling_buyer = FProcessString( $idbilling_buyer );
					$SQL_Condition .= " AND billing_buyer.idbilling_buyer IN( '$idbilling_buyer' )";
			}
			
			if( !empty( $company ) )
				$SQL_Condition .= " AND buyer.company LIKE '%$company%'";
			
			if( !empty( $letter ) ){
				if($letter == "0-9"){
					$SQL_Condition .= " AND (buyer.company LIKE '0%' 
											OR buyer.company LIKE '1%' 
											OR buyer.company LIKE '2%' 
											OR buyer.company LIKE '3%' 
											OR buyer.company LIKE '4%' 
											OR buyer.company LIKE '5%' 
											OR buyer.company LIKE '6%' 
											OR buyer.company LIKE '7%' 
											OR buyer.company LIKE '8%' 
											OR buyer.company LIKE '9%')";
				}else{	
					$SQL_Condition .= " AND buyer.company LIKE '$letter%'";
				}
			}
			
			if( !empty( $zipcode ) )
				$SQL_Condition .= " AND buyer.zipcode LIKE '%$zipcode%'";
				
			if( !empty( $city ) )
				$SQL_Condition .= " AND buyer.city LIKE '%$city%'";
			
			if( !empty( $siret ) )
				$SQL_Condition .= " AND buyer.siret LIKE '%$siret%'";
				
			if( !empty( $lastname ) )
				$SQL_Condition .= " AND contact.lastname LIKE '%$lastname%'";
			
			if( !empty( $total_amount_ht ) )
				$SQL_Condition .= " 
				AND ROUND( `billing_buyer`.total_amount_ht, 2 ) = '" . Util::text2num( $total_amount_ht ) . "'";
				
			if( !empty( $total_amount ) )
				$SQL_Condition .= " 
				AND ROUND( `billing_buyer`.total_amount, 2 ) = '" . Util::text2num( $total_amount ) . "'";
			
			if( !empty( $idgrouping ) )
				$SQL_Condition .= " AND buyer.idgrouping = $idgrouping ";
			
			$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
		
			$SQL_Condition	.= "
			AND billing_buyer.iduser = user.iduser 
			AND billing_buyer.status='Invoiced'";
			/*"AND (
					billing_buyer.status='Invoiced'
					OR (
						billing_buyer.status = 'Paid'
						AND ROUND( (billing_buyer.total_amount - billing_buyer.credit_amount - (SELECT ( SUM(rb.amount) + SUM(rb.credit_balance_used) )
																								FROM billing_regulations_buyer brb, regulations_buyer rb
																								WHERE rb.idregulations_buyer = brb.idregulations_buyer
																								AND brb.idbilling_buyer = billing_buyer.idbilling_buyer) )
								 ,2) > 0.00
						) 
				)";*/
			
			$SQL_Condition	.= " AND billing_buyer.idbuyer = buyer.idbuyer
			AND billing_buyer.idcontact = contact.idcontact
			AND contact.idbuyer = billing_buyer.idbuyer
			AND payment.idpayment = billing_buyer.idpayment and billing_buyer.total_amount > 0
			";
			
			$tables = "billing_buyer, user, buyer, contact, payment";
			
			
			// bon les conneries ca suffit maintnant
			// on va ranger le rs ds un joli tableau php, tu va voir c simple comme tout maintentant
			
			$SQL_Query = "	SELECT billing_buyer.idbilling_buyer, 
							billing_buyer.idbuyer,
							buyer.company,
							buyer.contact,
							buyer.siret,
							buyer.city,
							buyer.zipcode,
							buyer.idgrouping,
							contact.lastname,
							contact.firstname,
							billing_buyer.deliv_payment, 
							user.initial,
							billing_buyer.total_amount,
							billing_buyer.credit_amount,
							billing_buyer.factor,
							payment.idpayment as idpayments,
							payment.name_1
							FROM $tables 
							WHERE $SQL_Condition";
			
			
			if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
				$SQL_Query .= "
				ORDER BY idgrouping ASC , " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ." ";
			else
				$SQL_Query .= " ORDER BY billing_buyer.idbilling_buyer ASC, idgrouping ASC ";
			
			
			$rs =& DBUtil::query( $SQL_Query );
			
			$fields = array();
			$compteBils=0;
			
			while(!$rs->EOF){
				
				$idgrouping = strval( $rs->fields("idgrouping") );
				$idbuyer 	= strval( $rs->fields("idbuyer") );
				
				$fields[$idgrouping][$idbuyer][$compteBils]["idbilling_buyer"]	= $rs->fields("idbilling_buyer");
				$fields[$idgrouping][$idbuyer][$compteBils]["idbuyer"]			= $idbuyer;
				$fields[$idgrouping][$idbuyer][$compteBils]["company"] 			= $rs->fields("company");
				$fields[$idgrouping][$idbuyer][$compteBils]["contact"] 			= $rs->fields("contact");
				$fields[$idgrouping][$idbuyer][$compteBils]["siret"] 			= $rs->fields("siret");
				$fields[$idgrouping][$idbuyer][$compteBils]["city"] 			= $rs->fields("city");
				$fields[$idgrouping][$idbuyer][$compteBils]["zipcode"] 			= $rs->fields("zipcode");
				$fields[$idgrouping][$idbuyer][$compteBils]["lastname"]			= $rs->fields("lastname");
				$fields[$idgrouping][$idbuyer][$compteBils]["firstname"] 		= $rs->fields("firstname");
				$fields[$idgrouping][$idbuyer][$compteBils]["deliv_payment"] 	= $rs->fields("deliv_payment");
				$fields[$idgrouping][$idbuyer][$compteBils]["initial"] 			= $rs->fields("initial");
				$fields[$idgrouping][$idbuyer][$compteBils]["total_amount"] 	= $rs->fields("total_amount");
				$fields[$idgrouping][$idbuyer][$compteBils]["credit_amount"] 	= $rs->fields("credit_amount");
				$fields[$idgrouping][$idbuyer][$compteBils]["factor"] 			= $rs->fields("factor");
				$fields[$idgrouping][$idbuyer][$compteBils]["idpayments"]		= $rs->fields("idpayments");
				$fields[$idgrouping][$idbuyer][$compteBils]["name_1"]			= $rs->fields("name_1");
				$rs->MoveNext();
				$compteBils++;
			}
						$rs->MoveFirst();
			if( $rs->RecordCount() > 0 ){
				
				// Traitement pour l'ordre alpha
				if($letter==""){
					$alphaList = array();
				}else{
					$alphaList = array("Tous");
				}
				
				if($letter==""){
					while(!$rs->EOF()){
						if($rs->fields('company')!="")
							$letter = strtoupper(substr($rs->fields('company'),0,1));
						else
							$letter = strtoupper(substr($rs->fields('lastname'),0,1));
							
						if(is_numeric($letter))
							$letter = "0-9";
							
						if(!in_array($letter,$alphaList)){
							array_push($alphaList,$letter);
						}
		
						$rs->MoveNext();
					}
				}
				// retri dans l'ordre alpha parceque les particuliers ont pas de company donc déja pas sortis lors de la requete
				usort($alphaList, "strcasecmp");
			}
			
			//For tab2 Added by behzad
			$SQL_Query_Second_Tab = "	SELECT
											regulations_buyer.idregulations_buyer,
											billing_buyer.idpayment,
											billing_buyer.idbilling_buyer,
											regulations_buyer.amount,
											billing_buyer.idbuyer,
											buyer.company,
											buyer.contact,
											buyer.siret,
											buyer.city,
											buyer.zipcode,
											buyer.idgrouping,
											contact.lastname,
											contact.firstname,
											billing_buyer.deliv_payment, 
											user.initial,
											billing_buyer.total_amount,
											billing_buyer.credit_amount,
											billing_buyer.factor
											
										FROM $tables ,regulations_buyer, billing_regulations_buyer
										WHERE $SQL_Condition
										AND regulations_buyer.idbuyer = billing_buyer.idbuyer
										AND regulations_buyer.accept = 1
												
AND billing_regulations_buyer.idbilling_buyer = billing_buyer.idbilling_buyer
AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer";

//	echo $SQL_Query_Second_Tab ;
			//echo $SQL_Query_Second_Tab;
			if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
				$SQL_Query_Second_Tab .= "
				ORDER BY idgrouping ASC , " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ." ";
			else
				$SQL_Query_Second_Tab .= " ORDER BY billing_buyer.idbilling_buyer ASC, idgrouping ASC ";
			
			//echo $SQL_Query_Second_Tab;
			$rs ='';
			$rs =& DBUtil::query( $SQL_Query_Second_Tab );
			
			$fields_tab2 = array();
			$compteBils_Tab2=0;
			
			while(!$rs->EOF){
				
				$idgrouping = strval( $rs->fields("idgrouping") );
				$idbuyer 	= strval( $rs->fields("idbuyer") );
				
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["idbilling_buyer"]	= $rs->fields("idbilling_buyer");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["idregulations_buyer"]	= $rs->fields("idregulations_buyer");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["idpayment"]			= $rs->fields("idpayment");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["idbuyer"]			= $idbuyer;
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["company"] 			= $rs->fields("company");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["contact"] 			= $rs->fields("contact");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["siret"] 			= $rs->fields("siret");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["city"] 			= $rs->fields("city");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["zipcode"] 			= $rs->fields("zipcode");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["lastname"]			= $rs->fields("lastname");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["firstname"] 		= $rs->fields("firstname");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["deliv_payment"] 	= $rs->fields("deliv_payment");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["initial"] 			= $rs->fields("initial");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["total_amount"] 	= $rs->fields("total_amount");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["credit_amount"] 	= $rs->fields("credit_amount");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["factor"] 			= $rs->fields("factor");
				$fields_tab2[$idgrouping][$idbuyer][$compteBils_Tab2]["amount"] 			= $rs->fields("amount");
						
				$rs->MoveNext();
				$compteBils_Tab2++;
			}
			
			$rs->MoveFirst();
			if( $rs->RecordCount() > 0 ){
				
				// Traitement pour l'ordre alpha
				if($letter==""){
					$alphaList = array();
				}else{
					$alphaList = array("Tous");
				}
				
				if($letter==""){
					while(!$rs->EOF()){
						if($rs->fields('company')!="")
							$letter = strtoupper(substr($rs->fields('company'),0,1));
						else
							$letter = strtoupper(substr($rs->fields('lastname'),0,1));
							
						if(is_numeric($letter))
							$letter = "0-9";
							
						if(!in_array($letter,$alphaList)){
							array_push($alphaList,$letter);
						}
		
						$rs->MoveNext();
					}
				}
				// retri dans l'ordre alpha parceque les particuliers ont pas de company donc déja pas sortis lors de la requete
				usort($alphaList, "strcasecmp");
			}
			
			?>

			<style>
				 input.total{text-align: right; width: 50px;}
			</style>
			<script type="text/javascript">
				$(document).ready(function() { 
					$('#factureMenu_').append( " ("+<?=$compteBils?>+")" );
				
					$('#reglements_').append( " ("+<?=$compteBils_Tab2?>+")" );
				});
				
				function selectLastActiveTab() {
					<?php
					$targetTab = intval($_POST['targetTab']);
					if ($targetTab >= 0) {
					?>
						// Select the previously selected tab
						$('#container').tabs('select', <?php echo $targetTab ?>);
					<?php
					}
					?>
				}
			</script>
			<div class="content" style="margin-top:10px;" id="factures">
				<!--tab1-->
				<form action="<?php echo basename( $_SERVER[ "PHP_SELF" ] ) ?>" method="post" name="invoicetopay" id="invoicetopay">
					<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
					<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
					<input type="hidden" name="search" value="1" />				
		        	<div class="headTitle">
		                <p class="title">
		                	<?php if( $compteBils == 0 ){ ?>
		                			<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucune facture n'a été trouvée.</span>
		                	<?php }else{ ?>
		                		Résultats de la recherche: <?php echo $compteBils ?> facture(s)
		                	<?php } ?>
		                </p>
	
		            </div><?
		            if($compteBils>0){?>
						<?php
	
						$total_buyer = 0;
						$fact = 0;			
						$totalNetToPay 	= 0;
		// Nota comme il n'y a pas de groupement 0, tout ce qui n'est pas 'groupingé' est dedans
						// Sinon envoie la sauce Jean-Pierre
						foreach( $fields as $idgrouping => $grouping ){
							?><div class="subContent"><?
							// titre du groupement
							$groupingName = "" ;
							if($idgrouping > 0){
								$groupingQ = DBUtil::query("SELECT * FROM grouping WHERE idgrouping = $idgrouping");
								
								if($groupingQ->RecordCount() > 0){
									$groupingName = $groupingQ->fields('grouping');						
								}
								
								?>
								<div class="subTitleContainer">
									<p class="subTitle"> GROUPEMENT : <?=$groupingName?></p>
								</div>
								<?
							}
							$buyerCount = 0;
							foreach( $grouping as $idBuyerGroup => $buyerGroup ){
								
								// buyers par groupement, cadre groupement créé, on met les buyers et leurs factures
	
								$total_buyer = 0;
								$totalNetToPay  = 0;
								
					
							$company = DBUtil::query("SELECT company FROM buyer WHERE idbuyer = '$idBuyerGroup'")->fields('company');					
							
									
								if($company == ''){
								
									$buyerfullnameRS = DBUtil::query("	SELECT c.firstname, c.lastname 
																		FROM buyer b, contact c 
																		WHERE b.idbuyer = '$idBuyerGroup'
																		AND c.idcontact = 0
																		AND b.idbuyer = c.idbuyer");
																		
									$buyerfullname = $buyerfullnameRS->fields('lastname')." ".$buyerfullnameRS->fields('firstname');
								}
								
								if($idgrouping == 0){
									?>
								 	<a name="Anchor_<?php echo $idBuyerGroup ?>"></a>
									<div class="subTitleContainer">
										<p class="subTitle"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idBuyerGroup ?>" class="blackText"><?php if(!$company){ echo $buyerfullname; }else{ echo $company;} ?> - <?php echo $idBuyerGroup ?></a></p>
									</div>
						            <div class="resultTableContainer clear">
						            	<table class="dataTable resultTable">
						                	<thead>
						                        <tr>
						                            <th style="width:10%;">Com.</th>
						                            <th style="width:10%;">Client n°</th>
						                            <th style="width:10%;">Facture n°</th>
						                            <th style="width:15%;">Date d'échéance</th>
                                                    <th style="width:15%;">Mode de règlement</th>
						                            <th style="width:10%;">Factor</th>
						                            <th style="width:15%;">Total TTC</th>
						                            <th style="width:10%;">Escompte</th>
						                            <th style="width:15%;">Solde à payer</th>
						                            <th style="width:15%;"><?php echo count( $buyerGroup ) == 1 ? "Enregistrer" : "Enregistrer / Regouper" ?></th>
						                        </tr>
						                    </thead>
						                    <tbody>									
								<?php } else{?>
								
					        		<!-- tableau résultats recherche -->
						            <?if($buyerCount == 0){?>
							            <div class="resultTableContainer clear">
							            	<table class="dataTable resultTable">
							                	<thead>
							                        <tr>
						                        		<th>Acheteur</th>
							                            <th>Com.</th>
							                            <th>Client n°</th>
							                            <th>Facture n°</th>
							                            <th>Date d'échéance</th>
                                                        <th>Mode de règlement</th>
							                            <th>Factor</th>
							                            <th>Total TTC</th>
							                            <th>Escompte</th>
							                            <th>Solde à payer</th>
							                            <th>Enregistrer / Regouper</th>
							                        </tr>
							                    </thead>
							                    <tbody><?
						            }
					            }
					                   
					            $invoicesCount = count( $buyerGroup );
							    
							    include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
							    
							    $billCount = 0;
								foreach( $buyerGroup as $indice => $useless ){
									//affichage des factures
									
									$Invoice = new Invoice( $fields[$idgrouping][$idBuyerGroup][$indice][ "idbilling_buyer" ] );
									
									$netToPay		= $Invoice->getTotalATI();
									$billing_buyer	= $fields[$idgrouping][$idBuyerGroup][$indice][ "idbilling_buyer" ];
									$initials		= $fields[$idgrouping][$idBuyerGroup][$indice][ "initial" ];
									$deliv_payment	= $fields[$idgrouping][$idBuyerGroup][$indice][ "deliv_payment" ];
									$company		= $fields[$idgrouping][$idBuyerGroup][$indice][ "company" ];
									$buyerfullname	= $fields[$idgrouping][$idBuyerGroup][$indice][ "lastname" ].' '.$fields[$idgrouping][$idBuyerGroup][$indice][ "firstname" ];
									$idbuyer		= $fields[$idgrouping][$idBuyerGroup][$indice][ "idbuyer" ];
									$total_amount	= $fields[$idgrouping][$idBuyerGroup][$indice][ "total_amount" ];
									$credit_amount	= $fields[$idgrouping][$idBuyerGroup][$indice][ "credit_amount" ];
									$total_invoice	= $netToPay;
									$soldeToPay		= $Invoice->getBalanceOutstanding();
									$isContact		= $fields[$idgrouping][$idBuyerGroup][$indice][ "contact" ] == 1;
									$factor			= $fields[$idgrouping][$idBuyerGroup][$indice][ "factor" ] > 0 ? "Oui" : "Non";
									$idpayments      = $fields[$idgrouping][$idBuyerGroup][$indice][ "idpayments" ];
				                    $name_1         = $fields[$idgrouping][$idBuyerGroup][$indice][ "name_1" ];
						
									if( DBUtil::getDBValue( "factor_refusal_date", "billing_buyer", "idbilling_buyer", $billing_buyer ) != "0000-00-00" )
										$factor = "<span style=\"color:#FF0000;\">Définancé</span>";
									elseif( DBUtil::getDBValue( "factor_send_date", "billing_buyer", "idbilling_buyer", $billing_buyer ) != "0000-00-00" )
										$factor = "<span style=\"color:#EB6A0A;\">Financé</span>";
									
									include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
										
									if( $deliv_payment == "0000-00-00" )
										$deliv_payment = "";
									else
										$deliv_payment = usDate2eu( $deliv_payment );
									
									if( $Invoice->getTotalATI() == 0.0 )
					            		$rebate_rate = 0.0;
					            	else $rebate_rate 	= $Invoice->get( "rebate_type" ) == "rate" 	? $Invoice->get( "rebate_value" ) : $Invoice->get( "rebate_value" ) * 100.0 / $Invoice->getTotalATI();
				                																	
			        				?>
							        <tr class="blackText">
							        	<?
							        	if($idgrouping > 0  && $billCount == 0){
							        		?><td class="lefterCol" rowspan="<?=$invoicesCount?>">
							        			<a href="<?=$GLOBAL_START_URL?>/sales_force/contact_buyer.php?key=<?=$idbuyer?>" class="blackText"><?=$company?></a>
							        		</td><?
							        	}?>
							        	<td class="lefterCol"><?php echo $initials ?></td>
	
							        	<td>
							        		<a href="/sales_force/contact_buyer.php?key=<?php echo $idbuyer; ?>" onclick="window.open( this.href ); return false;">
							        			<?php echo $idbuyer; ?>
							        		</a>
							        	</td>
							        	
							        	<td><a href="<?=$GLOBAL_START_URL?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $billing_buyer ?>"><?php echo $billing_buyer ?></a></td>	
										<td><?php echo $deliv_payment ?></td>
                                        
											<td class="lefterCol"><?php echo $name_1 ?></td>
										
										<td><?php echo $factor ?></td>
                                       
										<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_invoice ) ?></td>
										<td style="text-align:right;"><?php if( $rebate_rate > 0.0 ) echo Util::rateFormat( $rebate_rate ); ?></td>
										<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $soldeToPay ) ?></td>
										<?if($idgrouping == 0){?>
											<td class="righterCol"><input type="checkbox" name="group_<?=$idbuyer ?>[]" id="group_<?=$idbuyer ?>" value="<?php echo $billing_buyer ?>" /></td>
							        	<?php } else{?>
											<td class="righterCol"><input type="checkbox" name="group_<?=$idgrouping ?>[]" id="group_<?=$idgrouping ?>" value="<?php echo $billing_buyer ?>" /></td>
							        	<?php } ?>
							        </tr>
									<?php
									
									//$total_buyer+=$total_amount;
									$total_buyer += $soldeToPay;
									$totalNetToPay  += $netToPay;
									
									$billCount++;
								}// fin facture
								
								if($idgrouping == 0){
									?>
											<tr>
										 		<th colspan="8" style="border-style:none;"></th>
										 		<th class="totalAmount"><?php echo Util::priceFormat( $total_buyer ) ?></th>
										 		<!-- <th class="totalAmount"><?php echo Util::priceFormat( $totalNetToPay ) ?></th> -->
										 		<th colspan="3" style="border-style:none; background-color:#FFFFFF;">&nbsp;</th>
											 </tr>
										</tbody>
					                </table>
				                </div>
					            <div class="submitButtonContainer">
					            	<input type="button" value="Enregistrer le paiement" onclick="registerPayment(<?php echo "'" . $idbuyer . "'" ?>,0);" class="blueButton" style="margin-top:5px;" />
					            </div>													
									<?
								}
								
								$buyerCount++;
							}// fin buyer
																	            
							if($idgrouping > 0){
								?>
											</tbody>
						                </table>
						            </div>
						            <div class="submitButtonContainer">
						            	<input type="button" value="Enregistrer le paiement" onclick="registerPayment(<?php echo "'" . $idbuyer . "'" ?>,<?=$idgrouping?>);" class="blueButton" style="margin-top:5px;" />
						            </div>
								<?
							}
							
							?></div><?php
						}//fin grouping
						
					}
				?>
				</form>
			</div>
			
			<div class="content ui-tabs-hide" style="margin-top:10px;" id="reglements">
				<!--tab2-->
				<form action="<?php echo basename( $_SERVER[ "PHP_SELF" ] ) ?>" method="post" name="invoicetopaytab2" id="invoicetopaytab2">
					<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
					<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
					<input type="hidden" name="search" value="1" />				
		        	<div class="headTitle">
		                <p class="title">
		                	<?php if( $compteBils_Tab2 == 0 ){ ?>
		                			<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucune facture n'a été trouvée.</span>
		                	<?php }else{ ?>
		                		Résultats de la recherche: <?php echo $compteBils_Tab2 ?> facture(s)
		                	<?php } ?>
		                </p>
	
		            </div><?
		            if($compteBils_Tab2>0){?>
						<?php
	
						$total_buyer = 0;
						$fact = 0;			
						$totalNetToPay 	= 0;			
			
						// Nota comme il n'y a pas de groupement 0, tout ce qui n'est pas 'groupingé' est dedans
						// Sinon envoie la sauce Jean-Pierre
						foreach( $fields_tab2 as $idgrouping => $grouping ){
							?><div class="subContent"><?
							// titre du groupement
							$groupingName = "" ;
							if($idgrouping > 0){
								$groupingQ = DBUtil::query("SELECT * FROM grouping WHERE idgrouping = $idgrouping");
								
								if($groupingQ->RecordCount() > 0){
									$groupingName = $groupingQ->fields('grouping');						
								}
								
								?>
								<div class="subTitleContainer">
									<p class="subTitle"> GROUPEMENT : <?=$groupingName?></p>
								</div>
								<?
							}
							$buyerCount = 0;
							foreach( $grouping as $idBuyerGroup => $buyerGroup ){
								
								// buyers par groupement, cadre groupement créé, on met les buyers et leurs factures
	
								$total_buyer = 0;
								$totalNetToPay  = 0;
								
								$company = DBUtil::query("SELECT company FROM buyer WHERE idbuyer = '$idBuyerGroup'")->fields('company');
								if($company == ''){
									$buyerfullnameRS = DBUtil::query("	SELECT c.firstname, c.lastname 
																		FROM buyer b, contact c 
																		WHERE b.idbuyer = '$idBuyerGroup'
																		AND c.idcontact = 0
																		AND b.idbuyer = c.idbuyer");
																		
									$buyerfullname = $buyerfullnameRS->fields('lastname')." ".$buyerfullnameRS->fields('firstname');
								}
								
								if($idgrouping == 0){
									?>
								 	<a name="Anchor_<?php echo $idBuyerGroup ?>"></a>
									<div class="subTitleContainer">
										<p class="subTitle"><a href="<?php echo $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?php echo $idBuyerGroup ?>" class="blackText"><?php if(!$company){ echo $buyerfullname; }else{ echo $company;} ?> - <?php echo $idBuyerGroup ?></a></p>
									</div>
						            <div class="resultTableContainer clear">
						            	<table class="dataTable resultTable">
						                	<thead>
						                        <tr>
						                            <th style="width:5%;">Com.</th>
						                            
						                            <th style="width:10%;">Client n°</th>
						                            <th style="width:10%;">Facture n°</th>
						                            <th style="width:15%;">Date d'échéance</th>
						                            <th style="width:10%;">Factor</th>
						                            <th style="width:10%;">Mode de paiement</th>
													<th style="width:5%;">Total TTC</th>
						                          
						                            <th style="width:15%;">Solde à payer</th>
						                            <th style="width:15%;"><?php echo count( $buyerGroup ) == 1 ? "Enregistrer" : "Enregistrer / Regouper" ?></th>
						                        </tr>
						                    </thead>
						                    <tbody>									
								<?php } else{?>
								
					        		<!-- tableau résultats recherche -->
						            <?if($buyerCount == 0){?>
							            <div class="resultTableContainer clear">
							            	<table class="dataTable resultTable">
							                	<thead>
							                        <tr>
						                        		<th>Acheteur</th>
							                            <th>Com.</th>
							                            <th>Client n°</th>
							                            <th>Facture n°</th>
							                            <th>Date d'échéance</th>
							                            <th>Factor</th>
							                            <th>Mode de paiement</th>
														<th>Total TTC</th>
							                            <th>Solde à payer</th>
							                            <th>Enregistrer / Regouper</th>
							                        </tr>
							                    </thead>
							                    <tbody><?
						            }
					            }
					                   
					            $invoicesCount = count( $buyerGroup );
							    
							    include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
							    $billCount = 0;
								foreach( $buyerGroup as $indice => $useless ){
									//affichage des factures
									
									$Invoice = new Invoice( $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "idbilling_buyer" ] );
									
									$netToPay		= $Invoice->getTotalATI();
									$billing_buyer	= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "idbilling_buyer" ];
									$idpayment	   = $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "idpayment" ];
									$idregulations_buyer	   = $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "idregulations_buyer" ];
									$initials		= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "initial" ];
									$deliv_payment	= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "deliv_payment" ];
									$company		= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "company" ];
									$buyerfullname	= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "lastname" ].' '.$fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "firstname" ];
									$idbuyer		= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "idbuyer" ];
									$total_amount	= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "total_amount" ];
									$credit_amount	= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "credit_amount" ];
									$total_invoice	= $netToPay;
									$soldeToPay		= $Invoice->getBalanceOutstanding();
									$isContact		= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "contact" ] == 1;
									$factor			= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "factor" ] > 0 ? "Oui" : "Non";
									$amount			= $fields_tab2[$idgrouping][$idBuyerGroup][$indice][ "amount" ] ;
					
									if( DBUtil::getDBValue( "factor_refusal_date", "billing_buyer", "idbilling_buyer", $billing_buyer ) != "0000-00-00" )
										$factor = "<span style=\"color:#FF0000;\">Définancé</span>";
									elseif( DBUtil::getDBValue( "factor_send_date", "billing_buyer", "idbilling_buyer", $billing_buyer ) != "0000-00-00" )
										$factor = "<span style=\"color:#EB6A0A;\">Financé</span>";
									
									include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
										
									if( $deliv_payment == "0000-00-00" )
										$deliv_payment = "";
									else
										$deliv_payment = usDate2eu( $deliv_payment );
									
									if( $Invoice->getTotalATI() == 0.0 )
					            		$rebate_rate = 0.0;
					            	else $rebate_rate 	= $Invoice->get( "rebate_type" ) == "rate" 	? $Invoice->get( "rebate_value" ) : $Invoice->get( "rebate_value" ) * 100.0 / $Invoice->getTotalATI();
				                																	
			        				?>
							        <tr class="blackText">
							        	<?
							        	if($idgrouping > 0  && $billCount == 0){
							        		?><td class="lefterCol" rowspan="<?=$invoicesCount?>">
							        			<a href="<?=$GLOBAL_START_URL?>/sales_force/contact_buyer.php?key=<?=$idbuyer?>" class="blackText"><?=$company?></a>
							        		</td><?
							        	}?>
							        	<td class="lefterCol"><?php echo $initials ?></td>
							        	
	
							        	<td>
							        		<a href="/sales_force/contact_buyer.php?key=<?php echo $idbuyer; ?>" onclick="window.open( this.href ); return false;">
							        			<?php echo $idbuyer; ?>
							        		</a>
							        	</td>
							        	
							        	<td><a href="<?=$GLOBAL_START_URL?>/accounting/com_admin_invoice.php?IdInvoice=<?php echo $billing_buyer ?>"><?php echo $billing_buyer ?></a></td>	
										<td><?php echo $deliv_payment ?></td>
										<td><?php echo $factor ?></td>
										
										<?if($idgrouping == 0){?>
											<td class="lefterCol"><?=paymentList1( $idbuyer, $idpayment);?></td>
										<?php } else{?>
											<td class="lefterCol"><?=paymentList1( $idgrouping, $idpayment);?></td>
										<?php } ?>
										
										<td style="text-align:right; white-space:nowrap;">
											<?if($idgrouping == 0){?>
												<?=$total_invoice?>&euro; 
											<?php } else{?>
												<?=$total_invoice?> &euro;
											<?php } ?>
										</td>
										<td style="text-align:right; white-space:nowrap;">
											<input type="hidden" name="idregulations_buyer_<?=$idbuyer ?>" value="<?="'".$idregulations_buyer."'"?>" />
											<input class="total" type="text" name="solde_<?=$idbuyer ?>" value="<?=$amount?>" />&euro;
											</td>
										<?if($idgrouping == 0){?>
											<td class="righterCol"><input type="checkbox" name="group_<?=$idbuyer ?>[]" id="group_<?=$idbuyer ?>" value="<?php echo $billing_buyer ?>" /></td>
							        	<?php } else{?>
											<td class="righterCol"><input type="checkbox" name="group_<?=$idgrouping ?>[]" id="group_<?=$idgrouping ?>" value="<?php echo $billing_buyer ?>" /></td>
							        	<?php } ?>
							        </tr>
									<?php
									
									$total_buyer+=$total_amount;
									$totalNetToPay  += $netToPay;
									
									$billCount++;
								}// fin facture
								
								if($idgrouping == 0){
									?>
											<tr>
										 		<th colspan="6" style="border-style:none;"></th>
										 		<th class="totalAmount"><?php echo Util::priceFormat( $total_buyer ) ?></th>
										 		<!-- <th class="totalAmount"><?php echo Util::priceFormat( $totalNetToPay ) ?></th> -->
										 		<th colspan="3" style="border-style:none; background-color:#FFFFFF;">&nbsp;</th>
											 </tr>
										</tbody>
					                </table>
				                </div>
					            <div class="submitButtonContainer">
					            	<input type="button" value="Enregistrer le paiement"  onclick="registerPaymentTab2(<?php echo "'".$idbuyer."'" ?>,0);" class="blueButton" style="margin-top:5px;" />
					            </div>													
									<?
								}
								
								$buyerCount++;
							}// fin buyer
																	            
							if($idgrouping > 0){
								?>
											</tbody>
						                </table>
						            </div>
						            <div class="submitButtonContainer">
						            	<input type="button" value="Enregistrer le paiement" onclick="registerPaymentTab2(<?php echo "'" . $idbuyer . "'" ?>,<?=$idgrouping?>);" class="blueButton" style="margin-top:5px;" />
						            </div>
								<?
							}
							
							?></div><?php
						}//fin grouping
						
					}
				?>
				</form>
			</div>
			<?php
			break;
		
		/*case "estimate":
			/**************************************************************************************************************************
			*                                 PAIEMENTS SUR DEVIS (accomptes & paiements comptants)                                   *
			**************************************************************************************************************************/
			/*
			// on recherche les paiements comptants et acomptes faits sur un devis
		
			$dpCond="";
			$cpCond="";
			$idestimate 		= isset( $_POST[ "idestimate" ] )		? $_POST[ "idestimate" ]			: "";
			
			
			if( !empty($idestimate) ){
				$dpCond .= " AND es.idestimate = $idestimate ";
			}
			
			if(!empty($idbuyer)){
				$dpCond .= " AND b.idbuyer = $idbuyer";
			}
			
			if(!empty($company)){
				$dpCond .= " AND b.company LIKE '%$company%'";
			}
			
			if(!empty($city)){
				$dpCond .= " AND b.city LIKE '%$city%'";
			}
				
			if(!empty($siret)){
				$dpCond .= " AND b.siret LIKE '%$siret%'";
			}
				
			if(!empty($zipcode)){
				$dpCond .= " AND b.zipcode LIKE '%$zipcode%'";
			}
			
			switch( $interval_select ){
				
				case 1:
				 	//min |2000-12-18 15:25:53|
					$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
				 	//max
					$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
					//sql_condition
					$dpCond .= " AND es.$date_type >='$Min_Time' AND es.$date_type <='$Now_Time' ";
					
					break;
					
				case 2:
					
					$yearly_month_select = $_POST['yearly_month_select'];
					
					if ($yearly_month_select > $now["mon"])
						$now["year"] --;
			
				 	//min
					$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
				 	//max
					$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
				
					//sql_condition
					$dpCond .= " AND es.$date_type >='$Min_Time' AND es.$date_type <'$Max_Time' ";
			     	break;
					     
				case 3:
				 	
				 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
				 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
				 	
					$dpCond .= " AND es.$date_type >= '$Min_Time' AND es.$date_type <= '$Max_Time' ";
					
			     	break;
					
			}
			
			$cpCond=$dpCond;
			
			$query = "
			SELECT es.idestimate,
				es.down_payment_type, 
				es.down_payment_value, 
				es.down_payment_idpayment, 
				es.down_payment_comment,
				es.down_payment_idregulation,
				es.down_payment_idbank,
				es.down_payment_piece,
				es.down_payment_date,
				es.total_amount,
				es.DateHeure,
				b.company,
				b.idbuyer,
				c.lastname,
				c.firstname
			FROM estimate es, buyer b, contact c
			WHERE down_payment_value > 0.00 
			AND down_payment_idregulation = 0
			$dpCond
			AND (es.status <> 'Refused' AND es.status <> 'Ordered' AND es.status <> 'Refused')
			AND b.idbuyer = es.idbuyer
			AND c.idbuyer = b.idbuyer";
			
			if( isset( $_POST[ "idbilling_buyer" ] ) && intval( $_POST[ "idbilling_buyer" ] ) )
				$query .= " AND FALSE";
				
			$query .= "
			GROUP BY es.idestimate
			ORDER BY b.company ASC";
							
			$dpRS = DBUtil::query( $query );
			
			
			$rsBanks = DBUtil::query("SELECT * FROM bank");
			$rsTypesReg = DBUtil::query("SELECT * FROM payment");
			
			?>
				<form action="payment_registration.php" method="post" name="registerEstimate" id="registerEstimate">
				<input type="hidden" name="registerEstimates" value="1">
				<div class="content" style="margin-top:10px;" id="estimate">
						
					<?
					if($dpRS->RecordCount()==0 || $dpRS === false ){?>
						<div class="headTitle">
							<p class="title">
			                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun acompte.</span>
			                </p>
						</div>
						<?
					}else{?>
						<div class="headTitle">
							<p class="title">
								<?php echo $dpRS->RecordCount() ?> acompte(s) trouvé(s)
							</p>
						</div>
						<div class="subContent">
								<!-- tableau résultats recherche -->
			                    <div class="resultTableContainer clear">
			                    	<table class="dataTable resultTable" id="estimateDPTable">
			                        	<thead>
					                        <tr>
					                            <th style="width:50px;">Devis n°</th>
					                            <th style="width:50px;">N° client</th>
					                            <th style="width:150px;">Client</th>
					                            <th style="width:60px;">Montant</th>
					                            <th style="width:70px;">Mode de paiement</th>
					                            <th style="width:100px;">Date</th>
					                            <th style="width:80px;">Banque émètrice</th>
					                            <th style="width:80px;">N° pièce</th>
					                            <th>Commentaire</th>
					                            <th>Traiter</th>
					                        </tr>
			                            </thead>
			                            <tbody>
			                            	<?
			                            	while(!$dpRS->EOF()){
			                            		
												$company 					= $dpRS->fields('company')==""?$dpRS->fields('firstname')." ".$dpRS->fields('lastname'):$dpRS->fields('company');
												$idbouyer 					= $dpRS->fields('idbuyer');
												$idestimate 				= $dpRS->fields('idestimate');
												$date_estimate 				= usDate2eu(substr($dpRS->fields('DateHeure'),0,10));
												$down_payment_type 			= $dpRS->fields('down_payment_type');
												$down_payment_value			= $dpRS->fields('down_payment_value');
												$estimateAmount				= $dpRS->fields('total_amount');
												$down_payment_idpayment		= $dpRS->fields('down_payment_idpayment');
												$down_payment_comment		= $dpRS->fields('down_payment_comment');
												$down_payment_idregulation	= $dpRS->fields('down_payment_idregulation');
												$down_payment_idbank		= $dpRS->fields('down_payment_idbank');
												$down_payment_piece			= $dpRS->fields('down_payment_piece');
												$down_payment_date			= usDate2eu($dpRS->fields('down_payment_date'));
												
				                            	if($down_payment_type=="amount"){
				                            		$down_payment_amount = $down_payment_value;
				                            	}else{
				                            		$down_payment_amount = round( $estimateAmount * $down_payment_value / 100, 2 );
				                            	}
													                            		
			                            		?>
			                            		<input type="hidden" name="es_down_payment_idbuyer_<?=$idestimate?>" value="<?=$idbouyer?>" />
			                            		<input type="hidden" name="es_down_payment_amount_<?=$idestimate?>" value="<?=$down_payment_amount?>" />
			                            		
			                            		<tr id="estimateRow_<?=$idestimate?>">
						                            <td class="lefterCol">
						                            	<a class="grasBack" href="<?$GLOBAL_START_URL?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $idestimate ?>" onclick="window.open(this.href); return false;"><?php echo $idestimate ?></a>
						                            </td>
						                            <td><?=$idbouyer?></td>
						                            <td><?=$company?></td>
						                            <td><?=Util::priceFormat($down_payment_amount);?></td>
						                            <td>
														<select name="es_down_payment_idpayment_<?=$idestimate?>" id="es_down_payment_idpayment_<?=$idestimate?>">
															<option value="0">Sélection</option>
															<?
		
															$rsTypesReg->MoveFirst();
															
															while(!$rsTypesReg->EOF()){
																$id = $rsTypesReg->fields('idpayment');
																$name = $rsTypesReg->fields('name_1');
																if($down_payment_idpayment==$id)
																	echo "<option value='$id' selected>$name</option>";									
																else
																	echo "<option value='$id'>$name</option>";
																
																$rsTypesReg->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
														<input type="text" name="es_down_payment_date_<?=$idestimate?>" id="es_down_payment_date_<?=$idestimate?>" value="<?php echo $down_payment_date ?>" readonly="readonly" class="calendarInput" />
														<?php DHTMLCalendar::calendar( "es_down_payment_date_$idestimate" ); ?>
						                            </td>
						                            <td>			                            	
														<select name="es_down_payment_idbank_<?=$idestimate?>" id="es_down_payment_idbank_<?=$idestimate?>">
															<option value="0">Sélection</option>
															<?
		
															$rsBanks->MoveFirst();
															
															while(!$rsBanks->EOF()){
																$idbcp = $rsBanks->fields('idbank');
																$bname = $rsBanks->fields('bank_name');
																if($down_payment_idbank==$idbcp)
																	echo "<option value='$idbcp' selected>$bname</option>";									
																else
																	echo "<option value='$idbcp'>$bname</option>";
																
																$rsBanks->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
						                            	<input type="text" style="width:75px;" class="textInput" id="es_down_payment_piece_<?=$idestimate?>" name="es_down_payment_piece_<?=$idestimate?>" value="<?=$down_payment_piece?>"/>
						                            </td>
						                            <td class="righterCol">
						                            	<input type="text" class="textInput" id="es_down_payment_comment_<?=$idestimate?>" name="es_down_payment_comment_<?=$idestimate?>" value="<?=$down_payment_comment?>"/>
						                            </td>
						                            <td>
						                            	<input type="checkbox" name="estimatesToCheckForDP[]" value="<?=$idestimate?>" />
						                            </td>
						                        </tr>
						                        
						                        <?$dpRS->MoveNext();?>
					                        <?php } ?>
			                            </tbody>
									</table>
								</div>
						</div><?
					}
		
				$query = "
				SELECT es.idestimate,
					es.payment_idpayment, 
					es.payment_comment,
					es.cash_payment_idregulation,
					es.cash_payment_idbank,
					es.cash_payment_piece,
					es.balance_date,
					es.total_amount,
					es.rebate_type,
					es.rebate_value,
					es.DateHeure,
					b.company,
					b.idbuyer,
					c.lastname,
					c.firstname
				FROM estimate es, buyer b, contact c
				WHERE cash_payment_idregulation = 0
				AND cash_payment = 1
				$cpCond
				AND (es.status <> 'Refused' AND es.status <> 'Ordered' AND es.status <> 'Refused')
				AND b.idbuyer = es.idbuyer
				AND c.idbuyer = b.idbuyer";
				
				if( isset( $_POST[ "idbilling_buyer" ] ) && intval( $_POST[ "idbilling_buyer" ] ) )
					$query .= " AND FALSE";
						
				$query .= "
				GROUP BY es.idestimate
				ORDER BY b.company ASC";
									
					$cpRS = DBUtil::query( $query );
					
					if($cpRS->RecordCount()==0 || $cpRS === false ){?>
						<div class="headTitle" style="margin-top:20px;">
							<p class="title">
			                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun paiement comptant.</span>
			                </p>
						</div>
						<?
					}else{?>
						<div class="headTitle"  style="margin-top:20px;">
							<p class="title">
								<?php echo $cpRS->RecordCount() ?> paiement(s) comptant(s) trouvé(s)
							</p>
						</div>
						<div class="subContent">
								<!-- tableau résultats recherche -->
			                    <div class="resultTableContainer clear">
			                    	<table class="dataTable resultTable" id="estimateCPTable">
			                        	<thead>
					                        <tr>
					                            <th style="width:50px;">Devis n°</th>
					                            <th style="width:50px;">N° client</th>
					                            <th style="width:150px;">Client</th>
					                            <th style="width:60px;">Montant</th>
					                            <th style="width:70px;">Mode de paiement</th>
					                            <th style="width:100px;">Date</th>
					                            <th style="width:80px;">Banque émètrice</th>
					                            <th style="width:80px;">N° pièce</th>
					                            <th>Commentaire</th>
					                            <th>Traiter</th>
					                        </tr>
			                            </thead>
			                            <tbody>
			                            	<?
			                            	while(!$cpRS->EOF()){
		
												$company 					= $cpRS->fields('company')==""?$cpRS->fields('firstname')." ".$cpRS->fields('lastname'):$cpRS->fields('company');
												$idbouyer 					= $cpRS->fields('idbuyer');
												$idestimate 				= $cpRS->fields('idestimate');
												$date_estimate 				= usDate2eu(substr($cpRS->fields('DateHeure'),0,10));
												$cash_payment_idpayment		= $cpRS->fields('payment_idpayment');
												$cash_payment_comment		= $cpRS->fields('payment_comment');
												$cash_payment_idregulation	= $cpRS->fields('cash_payment_idregulation');
												$cash_payment_idbank		= $cpRS->fields('cash_payment_idbank');
												$cash_payment_piece			= $cpRS->fields('cash_payment_piece');
												$cash_payment_date			= usDate2eu($cpRS->fields('balance_date'));
												
												$estimateRebateType			= $cpRS->fields('rebate_type');
												$estimateRebateValue		= $cpRS->fields('rebate_value');
												if($estimateRebateValue > 0){
													
													if($estimateRebateType=="rate"){
														$estimateAmount		= round($cpRS->fields('total_amount') - ( ($cpRS->fields('total_amount') * $estimateRebateValue)/100 ),2);
													}else{
														$estimateAmount		= $cpRS->fields('total_amount') - $estimateRebateValue;
													}
													
												}else{
													$estimateAmount			= $cpRS->fields('total_amount');
												}
			                            		?>
			                            		<input type="hidden" name="es_cash_payment_idbuyer_<?=$idestimate?>" value="<?=$idbouyer?>" />
			                            		<input type="hidden" name="es_cash_payment_amount_<?=$idestimate?>" value="<?=$estimateAmount?>" />
			                            		
			                            		<tr id="estimateRow_<?=$idestimate?>">
						                            <td class="lefterCol">
						                            	<a class="grasBack" href="<?$GLOBAL_START_URL?>/sales_force/com_admin_devis.php?IdEstimate=<?php echo $idestimate ?>" onclick="window.open(this.href); return false;"><?php echo $idestimate ?></a>
													</td>
						                            <td><?=$idbouyer?></td>
						                            <td><?=$company?></td>
						                            <td><?=Util::priceFormat($estimateAmount)?></td>
						                            <td>
														<select name="es_cash_payment_idpayment_<?=$idestimate?>" id="es_cash_payment_idpayment_<?=$idestimate?>">
															<option value="0">Sélection</option>
															<?
		
															$rsTypesReg->MoveFirst();
															
															while(!$rsTypesReg->EOF()){
																$id = $rsTypesReg->fields('idpayment');
																$name = $rsTypesReg->fields('name_1');
																if($cash_payment_idpayment==$id)
																	echo "<option value='$id' selected>$name</option>";									
																else
																	echo "<option value='$id'>$name</option>";
																
																$rsTypesReg->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
														<input type="text" name="es_cash_payment_date_<?=$idestimate?>" id="es_cash_payment_date_<?=$idestimate?>" value="<?php echo $cash_payment_date ?>" readonly="readonly" class="calendarInput" />
														<?php DHTMLCalendar::calendar( "es_cash_payment_date_$idestimate" ); ?>
						                            </td>
						                            <td>
														<select name="es_cash_payment_idbank_<?=$idestimate?>" id="es_cash_payment_idbank_<?=$idestimate?>">
															<option value="0">Sélection</option>
															<?
		
															$rsBanks->MoveFirst();
															
															while(!$rsBanks->EOF()){
																$idbcp = $rsBanks->fields('idbank');
																$bname = $rsBanks->fields('bank_name');
																if($cash_payment_idbank==$idbcp)
																	echo "<option value='$idbcp' selected>$bname</option>";									
																else
																	echo "<option value='$idbcp'>$bname</option>";
																
																$rsBanks->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
						                            	<input type="text" style="width:75px;" class="textInput" id="es_cash_payment_piece_<?=$idestimate?>" name="es_cash_payment_piece_<?=$idestimate?>" value="<?=$cash_payment_piece?>"/>
						                            </td>
						                            <td class="righterCol">
						                            	<input type="text" class="textInput" id="es_cash_payment_comment_<?=$idestimate?>" name="es_cash_payment_comment_<?=$idestimate?>" value="<?=$cash_payment_comment?>"/>
						                            </td>
						                            <td>
						                            	<input type="checkbox" name="estimatesToCheckForCP[]" value="<?=$idestimate?>" />
						                            </td>
						                        </tr>
						                        
						                        <?$cpRS->MoveNext();?>
					                        <?php } ?>
			                            </tbody>
									</table>
								</div>
						</div>
					<?
					}
					?>
					<script type="text/javascript">
						$(document).ready(function() { 
							document.getElementById('PDevisMenu').innerHTML = document.getElementById('PDevisMenu').innerHTML+" ("+<?=$dpRS->RecordCount()+$cpRS->RecordCount()?>+")";
						});
					</script>
					<?if($dpRS->RecordCount()+$cpRS->RecordCount() > 0){?>
						<div style="float:right;margin-top:10px;">
							<input type="button" class="blueButton" value="Enregistrer les paiements" onclick="acceptValidation('registerEstimate');">
						</div>
					<?php } ?>
					<div class="clear"></div>
				</div>
				</form>
			<?
			break;*/
		
		case "order":?>
			<?php
			/**************************************************************************************************************************
			*                                PAIEMENTS SUR COMMANDES (accomptes & paiements comptants)                                *
			**************************************************************************************************************************/
			
			// on recherche les paiements comptants et acomptes faits sur une commande
		
			$dpCond="";
			$cpCond="";

			$idorder 		= isset( $_POST[ "idorder" ] )		? $_POST[ "idorder" ]			: "";

			if( !empty($idorder) ){
				$dpCond .= " AND o.idorder = '$idorder' ";
			}
		
			if(!empty($idbuyer)){
				$dpCond .= " AND b.idbuyer = '$idbuyer'";
			}
			
			if(!empty($company)){
				$dpCond .= " AND b.company LIKE '%$company%'";
			}
			
			if(!empty($city)){
				$dpCond .= " AND b.city LIKE '%$city%'";
			}
				
			if(!empty($siret)){
				$dpCond .= " AND b.siret LIKE '%$siret%'";
			}	
			if(!empty($zipcode)){
				$dpCond .= " AND b.zipcode LIKE '%$zipcode%'";
			}
			
			if( isset( $_POST[ "idbilling_buyer" ] ) && intval( $_POST[ "idbilling_buyer" ] ) )
				$dpCond .= " AND FALSE";
					
			switch( $interval_select ){
				
				case 1:
				 	//min |2000-12-18 15:25:53|
					$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
				 	//max
					$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
					//sql_condition
					$dpCond .= " AND o.$date_type >='$Min_Time' AND o.$date_type <='$Now_Time' ";
					
					break;
					
				case 2:
					
					$yearly_month_select = $_POST['yearly_month_select'];
					
					if ($yearly_month_select > $now["mon"])
						$now["year"] --;
			
				 	//min
					$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
				 	//max
					$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
				
					//sql_condition
					$dpCond .= " AND o.$date_type >='$Min_Time' AND o.$date_type <'$Max_Time' ";
			     	break;
					     
				case 3:
				 	
				 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
				 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
				 	
					$dpCond .= " AND o.$date_type >= '$Min_Time' AND o.$date_type <= '$Max_Time' ";
					
			     	break;
					
			}
			
			$cpCond=$dpCond;
			
			$odpRS = DBUtil::query("SELECT o.idorder,
									o.down_payment_type, 
									o.down_payment_value, 
									o.down_payment_idpayment, 
									o.down_payment_comment,
									o.down_payment_idregulation,
									o.down_payment_idbank,
									o.down_payment_piece,
									o.down_payment_date,
									o.total_amount,
									o.DateHeure,
									b.company,
									b.idbuyer,
									c.lastname,
									c.firstname
							FROM `order` o, buyer b, contact c
							WHERE o.down_payment_value > 0.00 
							AND o.down_payment_idregulation = '0'
							$dpCond
							AND (o.status <> 'Cancelled' AND o.status <> 'delivered' AND o.status <> 'picking')
							AND conf_order_date = '0000-00-00 00:00:00'
							AND b.idbuyer = o.idbuyer
							AND c.idbuyer = b.idbuyer
							GROUP BY o.idorder
							ORDER BY b.company ASC");
			
			
			$rsBanks = DBUtil::query("SELECT * FROM bank");
			$rsTypesReg = DBUtil::query("SELECT * FROM payment");
			
			?>
				<form action="payment_registration.php" method="post" name="registerOrder" id="registerOrder">
				<input type="hidden" name="registerOrders" value="1" />		
				<div class="content" style="margin-top:10px;" id="commandes">
					<?
					if($odpRS->RecordCount()==0 || $odpRS === false ){?>
						<div class="headTitle">
							<p class="title">
			                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun acompte.</span>
			                </p>
						</div>
						<?
					}else{?>
						<div class="headTitle">
							<p class="title">
								<?php echo $odpRS->RecordCount() ?> acompte(s) trouvé(s)
							</p>
						</div>
		
						<div class="subContent">
								<!-- tableau résultats recherche -->
			                    <div class="resultTableContainer clear">
			                    	<table class="dataTable resultTable" id="orderDPTable">
			                        	<thead>
					                        <tr>
					                            <th style="width:50px;">Commande n°</th>
					                            <th style="width:50px;">N° client</th>
					                            <th style="width:150px;">Client</th>
					                            <th style="width:60px;">Montant</th>
					                            <th style="width:70px;">Mode de paiement</th>
					                            <th style="width:100px;">Date</th>
					                            <th style="width:80px;">Banque émètrice</th>
					                            <th style="width:80px;">N° pièce</th>
					                            <th>Commentaire</th>
					                            <th>Traiter</th>
					                        </tr>
			                            </thead>
			                            <tbody>
			                            	<?
			                            	while(!$odpRS->EOF()){
			                            		
												$company 					= $odpRS->fields('company')==""?$odpRS->fields('firstname')." ".$odpRS->fields('lastname'):$odpRS->fields('company');
												$idbouyer 					= $odpRS->fields('idbuyer');
												$idorder	 				= $odpRS->fields('idorder');
												$date_order 				= usDate2eu(substr($odpRS->fields('DateHeure'),0,10));
												$down_payment_type 			= $odpRS->fields('down_payment_type');
												$down_payment_value			= $odpRS->fields('down_payment_value');
												$orderAmount				= $odpRS->fields('total_amount');
												$down_payment_idpayment		= $odpRS->fields('down_payment_idpayment');
												$down_payment_comment		= $odpRS->fields('down_payment_comment');
												$down_payment_idregulation	= $odpRS->fields('down_payment_idregulation');
												$down_payment_idbank		= $odpRS->fields('down_payment_idbank');
												$down_payment_piece			= $odpRS->fields('down_payment_piece');
												$down_payment_date			= usDate2eu($odpRS->fields('down_payment_date'));	      
										
				                            	if($down_payment_type=="amount"){
				                            		$down_payment_amount = $down_payment_value;
				                            	}
                                                else if($down_payment_type=="rate")
				                            		$down_payment_amount = round( $orderAmount * $down_payment_value / 100, 2 );
                                                else{
				                            		$down_payment_amount = round( $orderAmount * $down_payment_value / 100, 2 );
				                            	}
				                            	
			                            		?>
			                            		
			                            		<input type="hidden" name="o_down_payment_idbuyer_<?=$idorder?>" value="<?=$idbouyer?>" />
			                            		<input type="hidden" name="o_down_payment_amount_<?=$idorder?>" value="<?=$down_payment_amount?>" />
			                            		
			                            		<tr id="orderRow_<?=$idorder?>">
						                            <td class="lefterCol">
						                            	<a class="grasBack" href="<?$GLOBAL_START_URL?>/sales_force/com_admin_order.php?IdOrder=<?php echo $idorder ?>" onclick="window.open(this.href); return false;"><?php echo $idorder ?></a>
						                            </td>
						                            <td><?=$idbouyer?></td>
						                            <td><?=$company?></td>
						                            <td><?=Util::priceFormat($down_payment_amount)?></td>
						                            <td>
														<select name="o_down_payment_idpayment_<?=$idorder?>" id="o_down_payment_idpayment_<?=$idorder?>">
															<option value="0">S&eacute;lection</option>
															<?
		
															$rsTypesReg->MoveFirst();
															
															while(!$rsTypesReg->EOF()){
																$id = $rsTypesReg->fields('idpayment');
																$name = $rsTypesReg->fields('name_1');
																if($down_payment_idpayment==$id)
																	echo "<option value='$id' selected>$name</option>";									
																else
																	echo "<option value='$id'>$name</option>";
																
																$rsTypesReg->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
														<input type="text" name="o_down_payment_date_<?=$idorder?>" id="o_down_payment_date_<?=$idorder?>" value="<?php echo $down_payment_date ?>" readonly="readonly" class="calendarInput" />
														<?php DHTMLCalendar::calendar( "o_down_payment_date_$idorder",  true,  false, true ); ?>
						                            </td>
						                            <td>			                            	
														<!--<select name="o_down_payment_idbank_<?=$idorder?>" id="o_down_payment_idbank_<?=$idorder?>">-->
														<select name="o_down_payment_idbank_<?=$idorder?>">
															<option value="0">S&eacute;lection</option>
															<?
		
															$rsBanks->MoveFirst();
															
															while(!$rsBanks->EOF()){
																$idbcp = $rsBanks->fields('idbank');
																$bname = $rsBanks->fields('bank_name');
																if($down_payment_idbank==$idbcp)
																	echo "<option value='$idbcp' selected>$bname</option>";									
																else
																	echo "<option value='$idbcp'>$bname</option>";
																
																$rsBanks->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
						                            	<!--<input type="text" style="width:75px;" class="textInput" id="o_down_payment_piece_<?=$idorder?>" name="o_down_payment_piece_<?=$idorder?>" value="<?=$down_payment_piece?>"/>-->
														<input type="text" style="width:75px;" class="textInput" name="o_down_payment_piece_<?=$idorder?>" value="<?=$down_payment_piece?>"/>
						                            </td>
						                            <td class="righterCol">
						                            	<input type="text" class="textInput" id="o_down_payment_comment_<?=$idorder?>" name="o_down_payment_comment_<?=$idorder?>" value="<?=$down_payment_comment?>"/>
						                            </td>
						                            <td class="righterCol">
						                            	<input type="checkbox" name="ordersToCheckForDP[]" value="<?=$idorder?>" />
						                            </td>
						                        </tr>
						                        
						                        <?$odpRS->MoveNext();?>
					                        <?php } ?>
			                            </tbody>
									</table>
								</div>
						</div><?
					}
			
					$ocpRS = DBUtil::query("SELECT o.idorder,
											o.payment_idpayment, 
											o.payment_comment,
											o.cash_payment_idregulation,
											o.cash_payment_idbank,
											o.cash_payment_piece,
											o.balance_date,
											o.total_amount,
											o.rebate_type,
											o.rebate_value,
											o.DateHeure,
											b.company,
											b.idbuyer,
											c.lastname,
											c.firstname
									FROM `order` o, buyer b, contact c
									WHERE o.cash_payment_idregulation = '0'
									AND o.cash_payment = 1
									$cpCond
									AND (o.status <> 'Cancelled' AND o.status <> 'delivered' AND o.status <> 'picking')
									AND b.idbuyer = o.idbuyer
									AND c.idbuyer = b.idbuyer
									GROUP BY o.idorder
									ORDER BY b.company ASC");
					
					if($ocpRS->RecordCount()==0 || $ocpRS === false ){?>
						<div class="headTitle" style="margin-top:20px;">
							<p class="title">
			                	<span style="font-size: 14px;font-weight: bold; color: #ff0000;">Aucun paiement comptant.</span>
			                </p>
						</div>
						<?
					}else{?>
						<div class="headTitle"  style="margin-top:20px;">
							<p class="title">
								<?php echo $ocpRS->RecordCount() ?> paiement(s) comptant(s) trouvé(s)
							</p>
						</div>
						<div class="subContent">
							
								<!-- tableau résultats recherche -->
			                    <div class="resultTableContainer clear">
			                    	<table class="dataTable resultTable" id="orderCPTable">
			                        	<thead>
					                        <tr>
					                            <th style="width:50px;">Commande n°</th>
					                            <th style="width:50px;">N° client</th>
					                            <th style="width:150px;">Client</th>
					                            <th style="width:60px;">Montant</th>
					                            <th style="width:70px;">Mode de paiement</th>
					                            <th style="width:100px;">Date</th>
					                            <th style="width:80px;">Banque émètrice</th>
					                            <th style="width:80px;">N° pièce</th>
					                            <th>Commentaire</th>
					                            <th>Traiter</th>
					                        </tr>
			                            </thead>
			                            <tbody>
			                            	<?
			                            	while(!$ocpRS->EOF()){
		
												$company 					= $ocpRS->fields('company')==""?$ocpRS->fields('firstname')." ".$ocpRS->fields('lastname'):$ocpRS->fields('company');
												$idbouyer 					= $ocpRS->fields('idbuyer');
												$idorder 					= $ocpRS->fields('idorder');
												$date_estimate 				= usDate2eu(substr($ocpRS->fields('DateHeure'),0,10));
												$cash_payment_idpayment		= $ocpRS->fields('payment_idpayment');
												$cash_payment_comment		= $ocpRS->fields('payment_comment');
												$cash_payment_idregulation	= $ocpRS->fields('cash_payment_idregulation');
												$cash_payment_idbank		= $ocpRS->fields('cash_payment_idbank');
												$cash_payment_piece			= $ocpRS->fields('cash_payment_piece');
												$cash_payment_date			= usDate2eu($ocpRS->fields('balance_date')); 		
		
												$orderRebateType			= $ocpRS->fields('rebate_type');
												$orderRebateValue			= $ocpRS->fields('rebate_value');
												
												if($orderRebateValue > 0){
													
													if($orderRebateType=="rate"){
														$orderAmount		= round($ocpRS->fields('total_amount') - ( ($ocpRS->fields('total_amount') * $orderRebateValue)/100 ),2);
													}else{
														$orderAmount		= $ocpRS->fields('total_amount') - $orderRebateValue;
													}
													
												}else{
													$orderAmount			= $ocpRS->fields('total_amount');
												}
												
			                            		?>
			                            		
			                            		<input type="hidden" name="o_cash_payment_idbuyer_<?=$idorder?>" value="<?=$idbouyer?>" />
			                            		<input type="hidden" name="o_cash_payment_amount_<?=$idorder?>" value="<?=$orderAmount?>" />
			                            		
			                            		<tr id="orderRow_<?=$idorder?>">
						                            <td class="lefterCol">
						                            	<a class="grasBack" href="<?$GLOBAL_START_URL?>/sales_force/com_admin_order.php?IdOrder=<?php echo $idorder ?>" onclick="window.open(this.href); return false;"><?php echo $idorder ?></a>
													</td>
						                            <td><?=$idbouyer?></td>
						                            <td><?=$company?></td>
						                            <td><?=Util::priceFormat($orderAmount)?></td>
						                            <td>
														<select name="o_cash_payment_idpayment_<?=$idorder?>" id="o_cash_payment_idpayment_<?=$idorder?>">
															<option value="0">Sélection</option>
															<?
		
															$rsTypesReg->MoveFirst();
															
															while(!$rsTypesReg->EOF()){
																$id = $rsTypesReg->fields('idpayment');
																$name = $rsTypesReg->fields('name_1');
																if($cash_payment_idpayment==$id)
																	echo "<option value='$id' selected>$name</option>";									
																else
																	echo "<option value='$id'>$name</option>";
																
																$rsTypesReg->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
														<input type="text" name="o_cash_payment_date_<?=$idorder?>" id="o_cash_payment_date_<?=$idorder?>" value="<?php echo $cash_payment_date ?>" readonly="readonly" class="calendarInput" />
														<?php DHTMLCalendar::calendar( "o_cash_payment_date_$idorder",  true,  false, true ); ?>
						                            </td>
						                            <td>
													<select name="o_cash_payment_idbank_<?=$idorder?>">
														<!--<select name="o_cash_payment_idbank_<?=$idorder?>" id="o_cash_payment_idbank_<?=$idorder?>">-->
															<option value="0">Sélection</option>
															<?
		
															$rsBanks->MoveFirst();
															
															while(!$rsBanks->EOF()){
																$idbcp = $rsBanks->fields('idbank');
																$bname = $rsBanks->fields('bank_name');
																if($cash_payment_idbank==$idbcp)
																	echo "<option value='$idbcp' selected>$bname</option>";									
																else
																	echo "<option value='$idbcp'>$bname</option>";
																
																$rsBanks->moveNext();
															}
															?>
														</select>
						                            </td>
						                            <td>
						                            	<input type="text" style="width:75px;" class="textInput" id="o_cash_payment_piece_<?=$idorder?>" name="o_cash_payment_piece_<?=$idorder?>" value="<?=$cash_payment_piece?>"/>
														
						                            </td>
						                            <td class="righterCol">
						                            	<input type="text" class="textInput" id="o_cash_payment_comment_<?=$idorder?>" name="o_cash_payment_comment_<?=$idorder?>" value="<?=$cash_payment_comment?>"/>
						                            </td>
						                            <td class="righterCol">
						                            	<input type="checkbox" name="ordersToCheckForCP[]" value="<?=$idorder?>" />
						                            </td>
						                        </tr>
						                        
						                        <?$ocpRS->MoveNext();?>
					                        <?php } ?>
			                            </tbody>
									</table>
								</div>
						</div>
		
					<?	
					}
					?>
					
					<script type="text/javascript">
						$(document).ready(function() { 
							document.getElementById('PCdeMenu').innerHTML = document.getElementById('PCdeMenu').innerHTML+" ("+<?=$odpRS->RecordCount()+$ocpRS->RecordCount()?>+")";
						});
					</script>
					<?if($odpRS->RecordCount()+$ocpRS->RecordCount()>0){?>
						<div style="float:right;margin-top:10px;">
							<input type="button" class="blueButton" value="Enregistrer les paiements" onclick="acceptValidation('registerOrder');">
							
						</div>
					<?php } ?>
					<div class="clear"></div>
				</div>
				</form>
			<?
			break;
		
		}?>
		</div>
	<div class="bottomRight"></div><div class="bottomLeft"></div>
</div> <!-- mainContent fullWidthContent -->

<script type='application/javascript'>
<!--

	$(function() {
		
			$('#container').tabs({ fxFade: true, fxSpeed: 'fast' });
		
	});
	
	
-->
</script>
<a name="bottomPage"></a>
<?php
	
}

//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = '$idsupplier' LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

function createMyRegulation($iddocument,$typeDoc,$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,$fromDP=0){

	//$maxNumRegul = DBUtil::query("SELECT (IFNULL( MAX(idregulations_buyer) , 0 ) + 1) as newId FROM regulations_buyer")->fields('newId');
		$table = 'regulations_buyer';
		$maxNumRegul = TradeFactory::Indexations($table);
	
	$username = User::getInstance()->get('login');
	$iduser = User::getInstance()->getId();	
	// ------- Mise à jour de l'id gestion des Index  #1161
	$insertReg=			
	"INSERT INTO regulations_buyer (
		idregulations_buyer,
		idbuyer,
		iduser,
		amount,
		idbank,
		piece_number,
		comment,
		idpayment,
		payment_date,
		accept,
		lastupdate,
		username,
		from_down_payment
	) VALUES (
		'$maxNumRegul',
		'$idbuyer',
		$iduser,
		$amount,
		$idbank,
		'".addslashes($piece)."',
		'".addslashes($comment)."',
		$idpayment,
		'$payment_date',
		1,
		NOW(),
		'$username',
		$fromDP
	)";

	DButil::query($insertReg);
	
	$setline = "";
	// ------- Mise à jour de l'id gestion des Index  #1161
	//maj devis ou commande
	if($fromDP){
		$setline = "down_payment_idregulation = '$maxNumRegul', 
					down_payment_idbank = $idbank, 
					down_payment_comment = '".addslashes($comment)."', 
					down_payment_piece = '".addslashes($piece)."', 
					down_payment_idpayment = $idpayment,
					down_payment_date = '$payment_date'";
	}else{
	// ------- Mise à jour de l'id gestion des Index  #1161
		$setline = "cash_payment_idregulation='$maxNumRegul',
					cash_payment_idbank = $idbank, 
					payment_comment = '".addslashes($comment)."', 
					cash_payment_piece = '".addslashes($piece)."', 
					payment_idpayment = $idpayment,
					balance_date = '$payment_date'";
	}
	// ------- Mise à jour de l'id gestion des Index  #1161
	$query = "UPDATE `$typeDoc` SET $setline where id$typeDoc = '$iddocument'";	
	DButil::query($query);
	// ------- Mise à jour de l'id gestion des Index  #1161
	// si c'est une commande on doit aussi transferer au devis
	if($typeDoc=='order'){
		$rsidestimate = DBUtil::query("SELECT idestimate FROM `order` WHERE idorder='$iddocument' LIMIT 1");
		
		if($rsidestimate->RecordCount() && $rsidestimate->fields('idestimate')!=0 && $rsidestimate->fields('idestimate')!='' ){
			$idestimateassocie = $rsidestimate->fields('idestimate');
			// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "UPDATE `estimate` SET $setline where idestimate = '$idestimateassocie'";
			DButil::query($query);
		}
	}
	
	// de même si le devis a une commande ce qui a mon avis est rare et stupide de permettre de passer en commande
	if($typeDoc=='estimate'){
	// ------- Mise à jour de l'id gestion des Index  #1161
		$rsido = DBUtil::query("SELECT idorder FROM `order` WHERE idestimate='$iddocument' LIMIT 1");
		
		if($rsido->RecordCount() && $rsido->fields('idorder')!='' && $rsidestimate->fields('idorder')!=0 ){
			$idorderassocie = $rsido->fields('idorder');
			// ------- Mise à jour de l'id gestion des Index  #1161
			$query = "UPDATE `order` SET $setline where idorder = '$idorderassocie'";
			DButil::query($query);
		}
	}
}

?>