<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Lettres de relance au format RTF (pour Word)
 */
 


include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

if( !isset( $_GET[ "id" ] ) || empty( $_GET[ "id" ] ) )
	die( "Paramètre absent" );

$idbilling_buyer = $_GET[ "id" ];

//Récupération du niveau de relance
$rs =& DBUtil::query( "SELECT relaunch_date_1, relaunch_date_2 FROM billing_buyer WHERE idbilling_buyer = $idbilling_buyer" );

if( $rs === false )
	trigger_error( "Impossible de récupérer le niveau de relance de la facture n°$idbilling_buyer", E_USER_ERROR );

if( $rs->fields( "relaunch_date_2" ) != "0000-00-00" )
	$level = 3;
elseif( $rs->fields( "relaunch_date_1" ) != "0000-00-00" )
	$level = 2;
elseif( $rs->fields( "relaunch_date_1" ) == "0000-00-00" )
	$level = 1;

//Vérification du débit factor
$rs =& DBUtil::query( "SELECT factor_refusal_date FROM billing_buyer WHERE idbilling_buyer = $idbilling_buyer" );

if( $rs === false )
	trigger_error( "Impossible de récupérer l'information de débit factor de la facture n°$idbilling_buyer", E_USER_ERROR );

//Sélection du modèle
if( $rs->fields( "factor_refusal_date" ) == "0000-00-00" ){ //Pas de refus au factor
	
	switch( $level ){
		case "1":	$doc = "$GLOBAL_START_PATH/templates/accounting/word/relance_1.rtf";
		case "2":	$doc = "$GLOBAL_START_PATH/templates/accounting/word/relance_2.rtf";
		case "3":	$doc = "$GLOBAL_START_PATH/templates/accounting/word/relance_3.rtf";
	}
	
}else{ //Refusé au factor
	
	switch( $level ){
		case "1":	$doc = "$GLOBAL_START_PATH/templates/accounting/word/relance_factor_1.rtf";
		case "2":	$doc = "$GLOBAL_START_PATH/templates/accounting/word/relance_factor_2.rtf";
		case "3":	$doc = "$GLOBAL_START_PATH/templates/accounting/word/relance_factor_3.rtf";
	}

}

if( !file_exists( $doc ) )
	trigger_error( "Le modèle de fichier n'existe pas.", E_USER_ERROR );

$datas = fread( fopen( $doc, "r" ), filesize( $doc ) );

$query = "
	SELECT idbilling_buyer,
		DateHeure,
		deliv_payment,
		total_amount
	FROM billing_buyer
	WHERE idbilling_buyer = $idbilling_buyer";

$rs =& DBUtil::query( $query );

if( $rs === false )
	trigger_error( "Impossible de récupérer les informations du courier de relance", E_USER_ERROR );
		
//On initialise les variables à changer dans le template

//Général
//Date du document
$date = date("Y-m-d");
$creation_Date = @explode( "-", $date );

@list( $year, $month, $day ) = $creation_Date;

setlocale( LC_TIME, "fr_FR.utf8" );

$timestamp = mktime( 0, 0, 0, $month, $day, $year );
$strday 	= strftime("%A", $timestamp );
$day 		= strftime("%d", $timestamp );
$month 		= strftime("%B", $timestamp );
$year 		= strftime("%Y", $timestamp );

$today = $day." ".$month." ".$year;

//Date de la facture
$date = $rs->fields("DateHeure");
@list($datedev,$heuredev)= @explode(" ", $date );
$creation_Date = @explode( "-", $datedev );


@list( $year, $month, $day ) = $creation_Date;

setlocale( LC_TIME, "fr_FR.utf8" );

$timestamp = mktime( 0, 0, 0, $month, $day, $year );
$strday 	= strftime("%A", $timestamp );
$day 		= strftime("%d", $timestamp );
$month 		= strftime("%B", $timestamp );
$year 		= strftime("%Y", $timestamp );

$billing_date = $day." ".$month." ".$year;

//Date d'échéance

$date = $rs->fields("deliv_payment");
$creation_Date = @explode( "-", $date );


@list( $year, $month, $day ) = $creation_Date;

setlocale( LC_TIME, "fr_FR.utf8" );

$timestamp = mktime( 0, 0, 0, $month, $day, $year );
$strday 	= strftime("%A", $timestamp );
$day 		= strftime("%d", $timestamp );
$month 		= strftime("%B", $timestamp );
$year 		= strftime("%Y", $timestamp );

$deliv_payment = $day." ".$month." ".$year;

//TTC

$ttc = round($rs->fields("total_amount"),2);

//Remplacement des tags
$to_replace = array(
	'\{Idbilling_buyer\}',
	'\{Now_Date\}',
	'\{DateHeure\}',
	'\{Deliv_payment\}',
	'\{Montant\}'
);

$replace = array(
	$idbilling_buyer,
	$today,
	$billing_date,
	$deliv_payment,
	$ttc
);

$newdatas = str_replace( $to_replace, $replace, $datas );

header( "content-type: application/msword" );
header( "content-disposition: .doc; filename=mise_en_demeure_$idbilling_buyer.doc" );

echo $newdatas;

?>