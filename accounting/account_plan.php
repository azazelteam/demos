<?php
/*
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Génération des écritures comptables
 */

//------------------------------------------------------------------------------------------

set_time_limit( 180 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( "$GLOBAL_START_PATH/objects/AccountPlan.php" );
include_once( "$GLOBAL_START_PATH/objects/Util.php" );
include_once( "$GLOBAL_START_PATH/objects/URLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) && $_POST[ "search" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	searchResults();

	exit();

}

//------------------------------------------------------------------------------------------
/* chargement ajax du moteur de recherche */

if( isset( $_GET[ "searchEngine" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	searchForm();

	exit();

}

//------------------------------------------------------------------------------------------
/* modification ajax d'un lettrage */

//Retourne 0 si le lettrage a été modifié avec succès
//Retourne 1 si le lettrage n'a pas été modifié
//Retourne le message d'erreur à afficher en cas d'erreur

if( isset( $_GET[ "setNewLettering" ] ) && $_GET[ "setNewLettering" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	setNewLettering();

}

//------------------------------------------------------------------------------------------
/* récupération ajax de la liste des lettrages d'un tiers */

if( isset( $_GET[ "getLetteringList" ] ) && $_GET[ "getLetteringList" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	getLetteringList();

	exit();

}

//------------------------------------------------------------------------------------------
/* récupération de la liste des lettrages non balancés */

if( isset( $_GET[ "checkLettering" ] ) && $_GET[ "checkLettering" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	checkLetteringContent();

	exit();

}

//------------------------------------------------------------------------------------------
/* ajustement d'un lettrage non balancé */

if( isset( $_GET[ "adjust" ] ) && $_GET[ "adjust" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	adjustLettering();

	exit( "0" );

}

//------------------------------------------------------------------------------------------
/* lancement du lettrage automatique */

if( isset( $_GET[ "automaticLettering" ] ) && $_GET[ "automaticLettering" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$start = microtime( true );

	customerLettering();
	supplierLettering();

	$execution_time = round( microtime( true ) - $start, 1 );

	exit( "Lettrage effectué avec succès en $execution_time secondes" );

}

//------------------------------------------------------------------------------------------
/* enregistrement ajax de nouvelle écriture */

if( isset( $_POST[ "addEntry" ] ) && $_POST[ "addEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	entryEdition( "create" );

}

//------------------------------------------------------------------------------------------
/* enregistrement ajax de nouvelles écritures à partir d'un modèle */

if( isset( $_POST[ "addModelEntry" ] ) && $_POST[ "addModelEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	addModelEntry();

	exit( "0" );

}

//------------------------------------------------------------------------------------------
/* modification ajax d'une écriture */

if( isset( $_POST[ "editEntry" ] ) && $_POST[ "editEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	entryEdition( "edit" );

}

//------------------------------------------------------------------------------------------
/* suppression ajax d'une écriture */

if( isset( $_POST[ "dropEntry" ] ) && $_POST[ "dropEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	dropEntry();

	exit();

}

//------------------------------------------------------------------------------------------
/* chargement ajax du contenu du popup pour une création d'écriture */

if( isset( $_GET[ "windowEntry" ] ) && $_GET[ "windowEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	getCreationWindow();

	exit();

}

//------------------------------------------------------------------------------------------
/* chargement ajax du contenu du popup pour une création d'écriture à partir d'un modèle */

if( isset( $_GET[ "windowModelEntry" ] ) && $_GET[ "windowModelEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	getModelWindow();

	exit();

}

//------------------------------------------------------------------------------------------

if( isset( $_GET[ "getModelTable" ] ) && $_GET[ "getModelTable" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	getModelTable( $_GET[ "idmodel" ] );

	exit();

}

//------------------------------------------------------------------------------------------
/* chargement ajax du contenu du popup pour une modification d'écriture */

if( isset( $_GET[ "windowEditionEntry" ] ) && $_GET[ "windowEditionEntry" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	getEditionWindow();

	exit();

}

//------------------------------------------------------------------------------------------
/* ajax d'ajout d'une ligne pour la création de plusieurs écritures */

if( isset( $_GET[ "newCreationRow" ] ) && $_GET[ "newCreationRow" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	if( !isset( $_GET[ "row" ] ) || !intval( $_GET[ "row" ] ) )
		exit();

	getCreationRow( intval( $_GET[ "row" ] ) );

	exit();

}

//------------------------------------------------------------------------------------------------
/* Liste des exports réalisés */

if( isset( $_GET[ "exportsList" ] ) ){

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$directory = "$GLOBAL_START_PATH/data/account_plan/";

	if( !is_dir( $directory ) )
		exit( "Aucun dossier d'export" );

	$matches = array();
	$files = scandir( $directory, 1 );

	$i = 0;
	foreach( $files as $file ){

		if( is_file( $directory . $file ) && preg_match( "plan_comptable_([0-9]{4}-[0-9]{2}-[0-9]{2})_([0-9]{2}:[0-9]{2}:[0-9]{2}).csv", $file, $matches ) ){

			echo "<a href=\"$GLOBAL_START_URL/data/account_plan/" . $matches[ 0 ] . "\">" . Util::dateFormatEu( $matches[ 1 ] ) . " à " . $matches[ 2 ] . "</a><br />";

			$i++;

		}

	}

	if( $i == 0 )
		exit( "Aucun fichier disponible" );

	exit();

}

//------------------------------------------------------------------------------------------------
/* export des dernières écritures */

if( isset( $_GET[ "exportLastRecords" ] ) ){

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );

	$lastExportDate = &getLastExportDate();

	$query = "SELECT * FROM account_plan WHERE creation_date > '$lastExportDate'";
	$query = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );

	$exportableArray =& getExportableArray( $query );
	exportArray( $exportableArray );

	exit();

}

//------------------------------------------------------------------------------------------------
/* export */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) ){

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	$req = $_GET[ "req" ];
	$exportableArray = getExportableArray( $req );

	exportArray( $exportableArray );

	exit();

}

//--------------------------------------------------------------------------------------------------

$Title = "Plan comptable";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------------

$lang = User::getInstance()->getLang();

//---------------------------------------------------------------------------------------------

$generatedRows = 0;

$start = microtime( true );

$startDateTime = date( "Y-m-d H:i:s" );

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
<!--

	$(document).ready(function(){

		$('#loading').hide();


	});

	/* ----------------------------------------------------------------------------------- */
	/* Affiche dans un blockUI le HTML de la fenêtre de création d'une nouvelle écriture   */
	/* ----------------------------------------------------------------------------------- */

	function getNewEntryWindow(){

		$('#newEntry').html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." style="display:block; margin:auto;" />');

		$.blockUI({
			message: $('#newEntry'),
			css: {
				'color': '#586065',
				cursor: 'default',
				'font-family': 'Arial, Helvetica, sans-serif',
				'font-size': '12px',
				'font-weight': 'normal',
				left: ( $(window).width() - 1000 ) / 2 + 'px',
				'padding': '15px 10px 10px 5px',
				'text-align': 'left',
				'top': '100px',
				'width': '970px',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700
		});

		$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?windowEntry=1',
			success: function( response ){ $('#newEntry').html(response); }
		});

		$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);

	}

	/* ----------------------------------------------------------------------------------------------- */
	/* Affiche dans un blockUI le HTML de la fenêtre de création d'une nouvelle écriture sur un modèle */
	/* ----------------------------------------------------------------------------------------------- */

	function getModelEntryWindow(){

		$('#modelEntry').html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." style="display:block; margin:auto;" />');

		$.blockUI({
			message: $('#modelEntry'),
			css: {
				'color': '#586065',
				cursor: 'default',
				'font-family': 'Arial, Helvetica, sans-serif',
				'font-size': '12px',
				'font-weight': 'normal',
				left: ( $(window).width() - 1000 ) / 2 + 'px',
				'padding': '15px 10px 10px 5px',
				'text-align': 'left',
				'top': '100px',
				'width': '970px',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700
		});

		$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?windowModelEntry=1',
			success: function( response ){ $('#modelEntry').html(response); }
		});

		$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);

	}

	/* ----------------------------------------------------------------------------------- */
	/* Ajoute une ligne à la popup de création manuelle des écritures                      */
	/* ----------------------------------------------------------------------------------- */

	function getNewCreationRow( idrow ){

		var row = parseInt( $('#row_count').val() ) + 1;
		$('#row_count').val(row);

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?newCreationRow=1&row=' + row,
			success: function( response ){ $('#newWrittingsTable').children('tbody').append( response ); }
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Affiche dans un blockUI le HTML de la fenêtre de création d'une nouvelle écriture   */
	/* ----------------------------------------------------------------------------------- */

	function getEntryWindow( idrow ){

		$('#newEntry').html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." style="display:block; margin:auto;" />');

		$.blockUI({

			message: $('#newEntry'),
			css: {
				'color': '#586065',
				cursor: 'pointer',
				'font-family': 'Arial, Helvetica, sans-serif',
				'font-size': '12px',
				'font-weight': 'normal',
				left: ( $(window).width() - 465 ) / 2 + 'px',
				padding: '15px',
				'text-align': 'left',
				'top': '150px',
				'width': '465px',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700

		});

		$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

		if( idrow != false )
			idrow = '&idrow=' + idrow;
		else
			idrow = '';

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?windowEditionEntry=1' + idrow,
			success: function( response ){ $('#newEntry').html(response); }
		});

		$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);

	}

	/* ----------------------------------------------------------------------------------- */
	/* Ajout d'une nouvelle écriture manuelle en Ajax                                      */
	/* ----------------------------------------------------------------------------------- */

	function entryEdition( action ){

		var idrow = "";

		if( action != "create" ){
			idrow = '&idrow=' + action;
			action = 'editEntry';
		}

		$("#errorMessages").html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." id="creatingEntrySpinner" style="display:block; margin:auto;" />');

		$.ajax({
			type: 'POST',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php',
			data: action + '=1&account_plan_1=' + $('#account_plan_1').val() + '&idaccount_plan=' + $('#idaccount_plan').val() + '&relation_name=' + $('#relation_name').val() + '&idrelation=' + $('#idrelation').val() + '&idaccount_record=' + $('#idaccount_record').val() + '&value_date=' + $('#value_date').val() + '&designation=' + $('#designation').val() + '&payment=' + $('#payment').val() + '&payment_date=' + $('#payment_date').val() + '&amount=' + $('#amount').val() + '&amount_type=' + $('#amount_type').val() + '&lettering=' + $('#lettering').val() + '&relation_type=' + $('#relation_type').val() + idrow ,
			success: function( response ){

				if( response == 'created' )
					$.growlUI( '&Eacute;criture ajoutée avec succès', '' );

				else if( response == 'edited' )
					$.growlUI( '&Eacute;criture modifiée avec succès', '' );

				else{

					$("#creatingEntrySpinner").remove();
					$("#errorMessages").html('<div style="border:1px solid #AAAAAA; margin: 10px 0;"></div>' + response);

				}

			}
		});

	}
	 function onChange(event){
         var $this = event.currentTarget;
         var value = jQuery($this).val();
         if(!value && value.length <=0){
             jQuery($this).val("");
             jQuery($this).parent().find("input[type='hidden']").val("");
         }
     }
	/* ----------------------------------------------------------------------------------- */
	/* Demande de confirmation de la suppression d'une ligne                               */
	/* ----------------------------------------------------------------------------------- */

	function confirmDropEntry( idrow ){

		$('#toDeleteEntry').val( idrow );

		$.blockUI({

			message: $('#dropEntryConfirmationDiv'),
			css: {
				padding: '15px',
				cursor: 'pointer',
				'font-weight': 'bold',
				'font-family': 'Arial, Helvetica, sans-serif',
				'color': '#586065',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700

		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Suppression manuelle d'une écriture en Ajax                                         */
	/* ----------------------------------------------------------------------------------- */

	function dropEntry(){

		$.blockUI({

			message: "Suppression de l'écriture en cours",
			css: {
				padding: '15px',
				cursor: 'pointer',
				'font-weight': 'bold',
				'font-family': 'Arial, Helvetica, sans-serif',
				'color': '#586065',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700

		});

		var idrow = $('#toDeleteEntry').val();

		$.ajax({
			type: 'POST',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php',
			data: 'dropEntry=1&idrow=' + idrow,
			success: function( response ){

				$('#resultCount').html( $('#resultCount').html() - 1 );

				if( $('#resultRow_'+idrow).prev('tr').hasClass('mainAccount') && !$('#resultRow_'+idrow).next('tr').hasClass('subAccount') )
					$('#resultRow_'+idrow).prev('tr').remove();

				$('#resultRow_'+idrow).remove();

				$.growlUI( '', response );

			}
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Visualisation de l'ensemble des écritures d'une pièce                               */
	/* ----------------------------------------------------------------------------------- */

	function getRecordList( record, record_type ){

		$.ajax({
			type: "POST",
			url: "<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php",
			data: "interval_select=3&search=1&detail=1&record=" + record + "&record_type=" + record_type + "&fiscal_year=" + $('#fiscal_year').val() + "&orderBy=idaccount_plan ASC, idrelation ASC, idaccount_record ASC",
			beforeSend: preSubmitCallback,
			success: postSubmitCallback
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Récupère la liste des lettrages d'un tiers pour afficher un menu déroulant avec ces */
	/* dans la fenêtre d'édition manuelle des écritures                                    */
	/* ----------------------------------------------------------------------------------- */

	function getLetteringList(){

		var idrelation = $('#idrelation').val();
		var relation_name = $('#relation_name').val();

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?getLetteringList=1&idrelation=' + idrelation + '&relation_name=' + relation_name,
			success: function(response){ $('#letteringCell').html(response); }
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Récupère la liste des lettrages d'un tiers pour afficher un menu déroulant avec ces */
	/* dans la fenêtre de création manuelle des écritures                                  */
	/* ----------------------------------------------------------------------------------- */

	function getLetteringListForCreation( field, id ){

		var idrelation = $('#idrelation_'+id).val();
		var relation_name = $(field).parent('div').children('.relation_name').val();
		$(field).parent('div').children('.idrelation').val(idrelation);

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?getLetteringList=1&idrelation=' + idrelation + '&relation_name=' + relation_name,
			success: function(response){ $('#letteringCell_'+id).html(response); }
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Lance le lettrage automatique                                                       */
	/* ----------------------------------------------------------------------------------- */

	function automaticLettering(){

		$.blockUI({

			message: 'Lettrage en cours...<br />Cette opération peut durer plusieurs minutes',
			css: {
				padding: '15px',
				cursor: 'pointer',
				'font-weight': 'bold',
				'font-family': 'Arial, Helvetica, sans-serif',
				'color': '#586065',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700

		});

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?automaticLettering=1',
			success: function( response ){ $.growlUI(response,''); }
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Modification manuelle du lettrage d'une écriture                                    */
	/* ----------------------------------------------------------------------------------- */

	function setNewLettering( value, idrow ){

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?setNewLettering=1&value=' + value + '&idrow=' + idrow,
			success: function( response ){
				if( response == "0" )
					$.growlUI('Lettrage modifié avec succès','');
				else if( response == "1" )
					return true;
				else
					$.growlUI(response,'');
			}
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Affiche un blockUI dans lequel on voit la liste des lettrages non balancés          */
	/* C'est dans ce blocUI qu'on pourra faire les ajustements                             */
	/* ----------------------------------------------------------------------------------- */

	function checkLettering(){

		$('#checkLetteringDivContent').css('height', ( $(window).height() - 120 ) + 'px').html('<div style="font-size:16px; font-weight:bold; margin-top:' + ( $(window).height() - 180 ) / 2 + 'px">Récupération de la liste des lettrages non balancés<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." style="display:block; margin:auto; margin-top:15px;" /></div>');

		$.blockUI({
			message: $('#checkLetteringDiv'),
			css: {
				'color': '#586065',
				cursor: 'default',
				'font-family': 'Arial, Helvetica, sans-serif',
				'font-size': '12px',
				'font-weight': 'normal',
				'height': ( $(window).height() - 120 ) + 'px',
				left: ( $(window).width() - 950 ) / 2 + 'px',
				padding: '15px',
				top: '50px',
				'width': '950px',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700
		});

		$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

		$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?checkLettering=1',
			success: function(response){ $('#checkLetteringDivContent').html(response); }
		});

	}

	/* ----------------------------------------------------------------------------------- */
	/* Ajuste un écart de règlement pour un lettrage                                       */
	/* @param string type le type du tiers                                                 */
	/* @param string idrelation le numéro de tiers                                         */
	/* @param string lettering le lettrage pour lequel il faut faire l'ajustement          */
	/* @param int idrow la ligne du tableau correspondant à l'ajustement                   */
	/* ----------------------------------------------------------------------------------- */

	function adjust( type, idrelation, lettering, idrow ){

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?adjust=1&type=' + type + '&idrelation=' + idrelation + '&lettering=' + lettering,
			success: function( response ){
				if( response == '0' ){
					$('#row_' + idrow ).children(':last-child').html('Ajustement effectué');
					$('#row_' + idrow ).children().css('background-color','#CBCBCB');
				}else
					$.growlUI('',response);
			}
		});

	}

	/* ----------------------------------------------------------------------------------- */

	function getModelTable( idmodel ){

		if( idmodel == '' )
			$('#windowModelTableContainer').html('');
		else{
			$('#idmodel').val(idmodel);
			$.ajax({
				type: 'GET',
				url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?getModelTable=1&idmodel=' + idmodel,
				success: function( response ){
					$('#windowModelTableContainer').html(response);
				}
			});
		}

	}

	/* ----------------------------------------------------------------------------------- */

	function getSearchEngine( type ){

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?searchEngine&' + type,
			success: function( response ){
				$('#SearchResults').html('');
				$('#searchFormContainer').html(response);
			}
		});

	}

	/* ----------------------------------------------------------------------------------- */

	function toggleSubAccounts( main_account ){

		if( $( ".subAccount_" + main_account ).css( "display" ) == "none" ){

			$( ".subAccount_" + main_account ).show();
			$( "#account_" + main_account ).addClass( "openedAccount" );

		}else{

			$( ".subAccount_" + main_account ).hide();
			$( "#account_" + main_account ).removeClass( "openedAccount" );

		}

	}

	/* ----------------------------------------------------------------------------------- */

	function showExports(){

		$('#exportsDiv').html('<div style="font-size:16px; font-weight:bold;">Récupération de la liste des fichiers exportés<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." style="display:block; margin:auto; margin-top:15px;" /></div>');

		$.blockUI({
			message: $('#exportsDiv'),
			fadeIn: 700,
    		fadeOut: 700,
			css: {
				'color': '#586065',
				cursor: 'default',
				'font-family': 'Arial, Helvetica, sans-serif',
				'font-size': '12px',
				'font-weight': 'normal',
				padding: '15px',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			 }
		});

		$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

		$('.blockOverlay').attr('title','Cliquer pour sortir').click($.unblockUI);

		$.ajax({
		 	type: 'GET',
		 	url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?exportsList',
			success: function( response ){ $('#exportsDiv').html( '<div style="font-size:16px; margin-bottom:10px;">Liste des exports réalisés</div>' + response ); }
		});

	}

	/* ----------------------------------------------------------------------------------- */

	function showSupplierInfos( idsupplier ){

		if( idsupplier == '0' )
			return;

		$.ajax({

			url: "<?php echo $GLOBAL_START_URL ?>/supplier/infos.php?idsupplier=" + idsupplier,
			async: false,
		 	success: function(msg){

				$.blockUI({

					message: msg,
					fadeIn: 700,
	        		fadeOut: 700,
					css: {
						width: '700px',
						top: '0px',
						left: '50%',
						'margin-left': '-350px',
						'margin-top': '50px',
						padding: '5px',
						cursor: 'help',
						'-webkit-border-radius': '10px',
		                '-moz-border-radius': '10px',
		                'background-color': '#FFFFFF',
		                'font-size': '11px',
		                'font-family': 'Arial, Helvetica, sans-serif',
		                'color': '#44474E'
					 }

				});

				$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

				$('.blockOverlay').attr('title','Cliquer pour sortir').click($.unblockUI);

			}

		});

	}

	/* ----------------------------------------------------------------------------------- */

-->
</script>
<div id="globalMainContent">
	<form action="#">
		<div id="dropEntryConfirmationDiv" style="display:none;">
		<div style="margin:15px 5px;">Êtes-vous certain de vouloir supprimer cette ligne&nbsp;?</div>
			<input type="hidden" value="" id="toDeleteEntry" />
			<input type="button" value="Annuler" class="orangeButton" onclick="$.unblockUI();" />
			<input type="button" value="Continuer" class="blueButton" onclick="dropEntry();" />
		</div>
	</form>
	<div id="loading" style="color:#44474E; font-size:13px; font-weight:bold; line-height:40px; padding:20px; text-align:center;">
		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." /><br />
		Génération des écritures en cours...
	</div>
<?php

if( strlen( ob_get_contents() ) )
	ob_flush();

//On ne génère pas les écritures si elles ont déjà été générées aujourd'hui
$rs =& DBUtil::query( "SELECT COUNT( * ) AS generate FROM account_plan WHERE creation_date > '" . date( "Y-m-d" ) . "'" );

if( $rs === false )
	trigger_error( "Impossible de vérifier la date de dernière génération des écritures comptables", E_USER_ERROR );

if( $rs->fields( "generate" ) == 0 )
	generate();
//print $generatedRows;
?>
	<div id="exportsDiv" style="display:none;"></div>
	<div id="newEntry" style="display:none;"></div>
	<div id="modelEntry" style="display:none;"></div>
	<div id="checkLetteringDiv" style="display:none;">
		<div id="checkLetteringDivContent" style="overflow:auto;"></div>
	</div>
	<div class="mainContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content" id="searchFormContainer">
			<?php searchForm(); ?>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<div id="tools" class="rightTools" style="position:relative;">
		<div class="toolBox">
			<div class="header">
				<div class="topRight"></div>
				<a href="#" onclick="$('#fiscalYearsToolbox').slideUp(); $('#helpToolbox').slideToggle(); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/toolbox_help.png" alt="Aide" style="float:right;" /></a>
				<a href="#" onclick="$('#helpToolbox').slideUp(); $('#fiscalYearsToolbox').slideToggle(); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/toolbox_list.png" alt="Exercices" style="float:right; margin-right:3px;" /></a>
				<div class="topLeft"></div>
				<p class="title">Outils</p>
			</div>
			<div class="content" style="line-height:15px;">
				<div style=" padding:6px 6px 0 6px;">
					<a href="#" onclick="getNewEntryWindow(); return false;"><?php echo Dictionnary::translate("tlt_23") ?></a><br />
					<a href="#" onclick="getModelEntryWindow(); return false;"><?php echo Dictionnary::translate("tlt_24") ?></a><br />
					<!--<a href="#" onclick="automaticLettering(); return false;">Lettrage automatique</a><br />-->
					<a href="#" onclick="checkLettering(); return false;"><?php echo Dictionnary::translate("tlt_25") ?></a>
				</div>
				<!--<div class="subTitleContainer" style="margin-top:0;">
					<p class="subTitle"><?php echo Dictionnary::translate("tlt_26") ?></p>
				</div>
				<div style=" padding:6px 6px 0 6px;">
					<a href="#" onclick="getSearchEngine('general'); return false;"><?php echo Dictionnary::translate("tlt_27") ?></a><br />
					<a href="#" onclick="getSearchEngine('thirdparty'); return false;"><?php echo Dictionnary::translate("tlt_28") ?></a>
				</div>
				<div class="subTitleContainer" style="margin-top:0;">
					<p class="subTitle"><?php echo Dictionnary::translate("tlt_29") ?></p>
				</div>
				<div style=" padding:6px 6px 0 6px;">
					<a href="#" onclick="showExports(); return false;"><?php echo Dictionnary::translate("tlt_30") ?></a><br />
					<a href="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?exportLastRecords"><?php echo Dictionnary::translate("tlt_31") ?></a>
				</div>-->
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<div id="fiscalYearsToolbox" class="toolBox" style="display:none; position:absolute;">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Exercices comptables</p>
			</div>
			<div class="content" style="padding:5px;">
				<div id="fiscalYearsList"><?php AccountPlan::displayFiscalYears(); ?></div>
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<div id="helpToolbox" class="toolBox" style="display:none; position:absolute;">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Aide</p>
			</div>
			<div class="content" style="padding:5px;">
				Les écritures ne sont générées qu'une fois par jour en début de journée. Seules les pièces antérieures à la date du jour sont concernées.
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<?php if( isset( $generatedRows ) && $generatedRows > 0 ){ $end = microtime( true ); ?>
		<div class="toolBox">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Infos</p>
			</div>
			<div class="content">
				<div style="padding:10px 5px 0 5px; text-align:center;">
					<span style="font-weight:bold;"><?php echo $generatedRows ?></span> lignes générées en<br />
					<span style="font-weight:bold;"><?php echo round( microtime( true ) - $start, 1 ) ?></span> secondes<br />
					<a href="#" onclick="displayDetail(); return false;">Voir le détail</a>
				</div>
				<script type="text/javascript">
				<!--

					function displayDetail(){

						$.ajax({
							type: "POST",
							url: "<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php",
							data: "interval_select=2&start=<?php echo $startDateTime ?>&end=<?php echo date( "Y-m-d H:i:s" ) ?>&search=1&detail=1&orderBy=idaccount_plan ASC, idrelation ASC, idaccount_record ASC",
							beforeSend: preSubmitCallback,
							success: postSubmitCallback
						});
						
					}
					
				-->
				</script>
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div id="SearchResults"></div>
</div> <!-- GlobalMainContent -->
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function searchForm(){

	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
	include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

	$formFields = array(

		$fieldName = "idbuyer" 			=> "",
		"idsupplier"			 	=> "",
		"idcustomer_invoice" 			=> "",
		"idcredit"		 		=> "",
		"idsupplier_invoice"	 		=> "",
		"idprovider_invoice"	 		=> "",
		"amount"	 			=> "",
		"amount_bottom"				=> "",
		"amount_top"			 	=> "",
		"lettering"				=> "",
		"piece"					=> "",
		"orderBy"				=> "idrelation ASC"

	);

	$postData = array();

	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;

?>
			<style type="text/css">
			<!--

				.mainAccount:hover{
					background-color:#F5F5F5;
				}

				.openedAccount{
					background-color:#F5F5F5;
				}

				.openedAccount:hover{
					background-color:#F5F5F5;
				}

				th.noTopBorder{
					border-top-style:none;
				}

				td.modifiableLetterCell{
					cursor:pointer;
				}

				div.blockUI input.textInput,
				div.blockUI input#txt_account_plan_1{
					border:1px solid #AAAAAA;
					color:#44474E;
					font-family:Arial, Helvetica, sans-serif;
					font-size:11px;
					width:100%;
				}

				#ui-datepicker-div,
				.ui-datepicker{ z-index:2000; }

			-->
			</style>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
			<script type="text/javascript">
			<!--

				/* ----------------------------------------------------------------------------------- */

				$(document).ready(function() {

					 var options = {

						beforeSubmit:  preSubmitCallback,  // pre-submit callback
						success:	   postSubmitCallback  // post-submit callback

					};

					$('#SearchForm').ajaxForm( options );

				});

				/* ----------------------------------------------------------------------------------- */

				function preSubmitCallback( formData, jqForm, options ){

					document.getElementById( 'SearchForm' ).elements[ 'search' ].value = '1';
					document.getElementById( 'SearchForm' ).elements[ 'export' ].value = '0';

					$.blockUI({

						message: "Recherche en cours",
						css: {
							padding: '15px',
							cursor: 'pointer',
							'font-weight': 'bold',
							'font-family': 'Arial, Helvetica, sans-serif',
							'color': '#586065',
							'-webkit-border-radius': '10px',
							'-moz-border-radius': '10px'
						},
						fadeIn: 0,
						fadeOut: 700

					});

				}


				/* ----------------------------------------------------------------------------------- */

				function postSubmitCallback( responseText, statusText ){

					$.unblockUI();

					if( statusText != 'success' || responseText == '0' ){

						alert( "Impossible d'effectuer la recherche" );
						return;

					}

					$('#SearchResults').html( responseText );

				}

				/* --------------------------------------------------------------------------------------- */

				function orderBy( str ){

					document.getElementById( 'SearchForm' ).elements[ 'orderBy' ].value = str;

					$('#SearchForm').ajaxSubmit( {

						beforeSubmit:  preSubmitCallback,  // pre-submit callback
						success:	   postSubmitCallback  // post-submit callback

					} );

				}

				/* ----------------------------------------------------------------------------------- */

				function changeRelation( element ){ $('#idrelation').val( element.info ); }

				/* ----------------------------------------------------------------------------------- */

				function changeSupplier( element ){ $('#idsupplier').val( element.info ); }

				/* ----------------------------------------------------------------------------------- */

                /* ----------------------------------------------------------------------------------- */

				function changeProvider( element ){ $('#idprovider').val( element.info ); }

				/* ----------------------------------------------------------------------------------- */

				function changeDatepickerEdges( fiscal_year ){
                        $('#search_start').datepicker('option','minDate',minDate[ fiscal_year ]);
                        $('#search_end').datepicker('option','maxDate',maxDate[ fiscal_year ]);

                        $('#search_start').datepicker('setDate',minDate[ fiscal_year ]);
                        $('#search_end').datepicker('setDate',maxDate[ fiscal_year ]);
				}

				/* ----------------------------------------------------------------------------------- */

				var minDate = new Array();
				var maxDate = new Array();

<?php

					$rs =& DBUtil::query( "SELECT idfiscal_year, start_date FROM fiscal_years ORDER BY start_date ASC" );

					if( $rs === false )
						trigger_error( "Impossible de récupérer la liste des exercices comptables", E_USER_ERROR );

					while( !$rs->EOF ){

						$start = $rs->fields( "start_date" );
						$id = $rs->fields( "idfiscal_year" );

						$rs->MoveNext();

						if( $rs->EOF )
							$end = date( "Y-m-d" );
						else
							$end = DateUtil::getPreviousDay( $rs->fields( "start_date" ) );

						list( $year, $month, $day ) = explode( "-", $start );
						echo "minDate[ $id ] = new Date($year," . ( $month - 1 ) . ",$day);\n";

						list( $year, $month, $day ) = explode( "-", $end );
						echo "maxDate[ $id ] = new Date($year," . ( $month - 1 ) . "," . intval( $day ) . ");\n";

					}

?>

				/* ----------------------------------------------------------------------------------- */

			-->
			</script>
			<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
				<div class="headTitle">
					<p class="title"><?php echo Dictionnary::translate("tlt_32") ?></p>
					<div class="rightContainer">
						Exercice comptable :
						<select name="fiscal_year" id="fiscal_year" onchange="changeDatepickerEdges(this.value); return false;">
							<?php

								$rs =& DBUtil::query( "SELECT idfiscal_year, name, start_date FROM fiscal_years ORDER BY start_date DESC" );

								if( $rs === false )
									trigger_error( "Impossible de récupérer la liste des exercices comptables", E_USER_ERROR );

								$start_date = Util::dateFormatEu( $rs->fields( "start_date" ), "d/m/Y" );
								$fiscal_year = $rs->fields( "idfiscal_year" );

								while( !$rs->EOF ){

									echo '<option value="' . $rs->fields( "idfiscal_year" ) . '">' . $rs->fields( "name" ) . '</option>';

									$rs->MoveNext();

								}

							?>
						</select>
					</div>
				</div>
				<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />
					<input type="hidden" name="orderBy" value="<?php echo $postData[ "orderBy" ] ?>" />
					<input type="hidden" name="interval_select" id="interval_select" value="1" />
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th style="width:20%" class="filledCell"><?php echo Dictionnary::translate("tlt_13") ?></th>
								<td style="width:20%">
									<input type="text" name="search_start" id="search_start" class="calendarInput" value="<?php echo $start_date ?>" />
									<?php echo DHTMLCalendar::calendar( "search_start" ) ?>
								</td>
								<th style="width:20%" class="filledCell"><?php echo Dictionnary::translate("tlt_14") ?></th>
								<td style="width:40%">
									<?php $date = date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ); ?>
									<input type="text" name="search_end" id="search_end" class="calendarInput" value="<?php echo $date ?>" />
									<?php echo DHTMLCalendar::calendar( "search_end" ) ?>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_33") ?></th>
								<td><input type="text" name="idbuyer" class="textInput" onchange="onChange(event)" value="<?php echo $postData[ "idbuyer" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_34") ?></th>
								<td>
									<input type="text" name="supplier_search" onchange="onChange(event)" id="supplier_search" class="textInput" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" />
									<input type="hidden" name="idsupplier" id="idsupplier" />
									<?php AutoCompletor::completeFromFile( "supplier_search", "$GLOBAL_START_URL/xml/autocomplete/supplier.php", 5, "changeSupplier" ); ?>
								</td>
							</tr>
                            <tr>
								<th class="filledCell" colspan="2"></th>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_35") ?></th>
								<td>
									<input type="text" name="provider_search" onchange="onChange(event)" id="provider_search" class="textInput" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" />
									<input type="hidden" name="idprovider" id="idprovider" />
									<?php AutoCompletor::completeFromFile( "provider_search", "$GLOBAL_START_URL/xml/autocomplete/provider.php", 5, "changeProvider" ); ?>
								</td>
							</tr>
							<?php if( !isset( $_GET[ "thirdparty" ] ) ){ ?>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_36") ?></th>
								<td><input type="text" name="idcustomer_invoice" class="textInput" value="<?php echo $postData[ "idcustomer_invoice" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_37") ?></th>
								<td><input type="text" name="idcredit" class="textInput" value="<?php echo $postData[ "idcredit" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_38") ?></th>
								<td><input type="text" name="idsupplier_invoice" class="textInput" value="<?php echo $postData[ "idsupplier_invoice" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_39") ?></th>
								<td><input type="text" name="idprovider_invoice" class="textInput" value="" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_40") ?></th>
								<td style="white-space:nowrap;"><input type="text" name="amount_bottom" class="textInput" value="<?php echo $postData[ "amount_bottom" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" style="text-align:right; width:50px;" /> ¤</td>
								<th class="filledCell"> <?php echo Dictionnary::translate("tlt_41") ?> </th>
								<td style="white-space:nowrap;"><input type="text" name="amount_top" class="textInput" value="<?php echo $postData[ "amount_top" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" style="text-align:right; width:50px;" /> ¤</td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_42") ?></th>
								<td style="white-space:nowrap;"><input type="text" name="amount" class="textInput" value="<?php echo $postData[ "amount" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" style="text-align:right; width:50px;" /> ¤</td>
								<td colspan="2"></td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_43") ?></th>
								<td><input type="text" name="lettering" class="textInput" value="<?php echo $postData[ "lettering" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_44") ?></th>
								<td><?php accountsSelect( true ); ?></td>
							</tr>
							<?php }else{ ?>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_43") ?></th>
								<td><input type="text" name="lettering" class="textInput" value="<?php echo $postData[ "lettering" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>

								<th class="filledCell"><?php echo Dictionnary::translate("tlt_45") ?></th>
								<td><input type="text" name="piece" class="textInput" value="<?php echo $postData[ "piece" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" /></td>


								<td colspan="2"></td>
							</tr>
							<?php } ?>
                            
                            <?php if( isset( $_GET[ "thirdparty" ] ) ){ ?>
                            
                            <tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
                            <tr>
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_42") ?></th>
								<td style="white-space:nowrap;"><input type="text" name="amount" class="textInput" value="<?php echo $postData[ "amount" ] ?>" onkeyup="if(this.value!='')document.getElementById('detail').checked=true;" onkeydown="if(this.value!='')document.getElementById('detail').checked=true;" style="text-align:right; width:50px;" /> ¤</td>
								<td colspan="2"></td>
							</tr>
                            
                            
                            
                            <?php } ?>
						</table>
					</div>
					<?php if( isset( $_GET[ "thirdparty" ] ) ){ ?>
					<input type="hidden" name="detail" id="detail" value="1" />
					<input type="hidden" name="thirdparty" value="1" />
					<?php }else{ ?>
					<label style="float:left;"><input type="checkbox" name="detail" id="detail" value="1" style="vertical-align:middle;" /> <?php echo Dictionnary::translate("tlt_58") ?></label>
					<?php } ?>
					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" value="Rechercher" />
					</div>
				</div>
			</form>
			<script type="text/javascript">
			<!--

				$(document).ready(function(){
			        $('#search_start').datepicker('option','minDate',minDate[ <?php echo $fiscal_year ?> ]);
				});
				
			-->
			</script>
<?php

}

//------------------------------------------------------------------------------------------------

function searchResults(){

	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	$maxSearchResults = 2500;

	$lang = User::getInstance()->getLang();

	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );

	if( isset( $_POST[ "detail" ] ) && $_POST[ "detail" ] == "1" ){

		$select = "
		idrow,
		idaccount_plan,
		idrelation,
		relation_type,
		idaccount_record,
		record_type,
		date,
		designation,
		account_plan_1,
		account_plan_2,
		account_plan_3,
		account_plan_4,
		lettering,
		action,
		( amount_type = 'credit' ) * amount AS credit,
		( amount_type = 'debit' ) * amount AS debit,
		( amount_type = 'credit' ) * amount - ( amount_type = 'debit' ) * amount AS amount,
		accounting_plan_parent,
		accounting_plan$lang AS description";

	}else{

		$select = "
		idaccount_plan,
		designation,
		SUM( ( amount_type = 'credit' ) * amount ) AS credit,
		SUM( ( amount_type = 'debit' ) * amount ) AS debit,
		SUM( ( amount_type = 'credit' ) * amount - ( amount_type = 'debit' ) * amount ) AS amount,
		accounting_plan_parent,
		accounting_plan$lang AS description";

	}

	$tables = "accounting_plan, account_plan";

	$where = getSearchWhereCondition();

	//tri

	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderBy" ] ) );

	//requete

	if( isset( $_POST[ "detail" ] ) && $_POST[ "detail" ] == "1" ){

		$query = "
			SELECT $select
			FROM $tables
			WHERE $where 
			ORDER BY idaccount_plan ASC, $orderBy";

	}else{

		$query = "
			SELECT $select
			FROM $tables
			WHERE $where 
			GROUP BY idaccount_plan
			ORDER BY idaccount_plan ASC, $orderBy";

	}

	$rs =& DBUtil::query( $query );

	$resultCount = $rs->RecordCount();

	?>
	<script type="text/javascript">
	<!--

		$(document).ready(function(){

			$(".record_help").qtip({
				adjust: {
					mouse: true,
					screen: true
				},
				content: 'AC : avoir client<br />FC : facture client<br />FF : facture fournisseur<br />RC : règlement client',
				position: {
					corner: {
						target: "rightMiddle",
						tooltip: "leftMiddle"
					}
				},
				style: {
					background: "#FFFFFF",
					border: {
						color: "#EB6A0A",
						radius: 5
					},
					fontFamily: "Arial,Helvetica,sans-serif",
					fontSize: "13px",
					tip: true
				}
			});

			$('.modifiableLetterCell').click(function(){

				var lettering = $(this).html();
				var idrow = $(this).attr('id').substring(7);

				if( !$(this).hasClass('activeCell') ){
					$(this).addClass('activeCell');
					$(this).html('<input type="text" value="' + lettering + '" id="field_' + idrow + '" class="textInput" style="text-align:center; text-transform:uppercase; width:30px;" onkeyup="setNewLettering(this.value,' + idrow + ');" onblur="$(this).parent().removeClass(\'activeCell\').html($(this).val());" />');
					$('#field_'+idrow).focus();
				}

			});

		});
function afficher(id){
alert(id);
 document.getElementById('pop'+id).className='tooltiphover';
 }
	-->
	</script>
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php

				if( $resultCount > 0 ){

					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );

					$query = "
						SELECT *
						FROM $tables
						WHERE $where
						ORDER BY idaccount_plan ASC, date ASC";

					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );

					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?export&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php } ?>
				<p class="title">
					&Eacute;critures comptables
					<?php if( $resultCount == 1 ){ ?> : <span id="resultCount">1</span> résultat trouvé<?php } ?>
					<?php if( $resultCount > 1 && $resultCount <= $maxSearchResults ){ ?> : <span id="resultCount"><?php echo $resultCount ?></span> résultats trouvés<?php } ?>
				</p>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ ?>Aucune écriture trouvée
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> écritures ont été trouvées <br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?export&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable">
					<?php if( isset( $_POST[ "detail" ] ) && $_POST[ "detail" ] == "1" ){ ?>
						<thead>
							<tr>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_46") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_47") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_48") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_49") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_50") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_51") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_52") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_53") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_54") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_55") ?></th>
								<th class="filledCell" style="border-bottom-style:none; text-align:center;"><?php echo Dictionnary::translate("tlt_56") ?></th>
                                	
							</tr>
							<tr>
								<th class="noTopBorder filledCell"></th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('account_plan_1 ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('account_plan_1 DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell"></th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('designation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('designation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('idaccount_record ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('idaccount_record DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('idrelation ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('idrelation DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('date ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('date DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('lettering ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('lettering DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('debit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('debit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('credit ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('credit DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
								<th class="noTopBorder filledCell">
									<a href="#" onclick="orderBy('amount ASC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
									<a href="#" onclick="orderBy('amount DESC'); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
								</th>
                             
							</tr>
						</thead>
					<?php }else{ ?>
						<thead>
							<tr>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_57") ?></th>
								<th class="filledCell" style="text-align:center; width:40%;"><?php echo Dictionnary::translate("tlt_49") ?></th>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_54") ?></th>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_55") ?></th>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_56") ?></th>
							</tr>
						</thead>
					<?php } ?>
						<tbody>
							<?php

							//construction du tableau de résultats
							//on fait ça pour pouvoir avoir les montants des parents en haut

							$results = array();
							$parent = 0;
							$i = -1;
							$j = 0;

							if( isset( $_POST[ "detail" ] ) && $_POST[ "detail" ] == "1" ){

								while( !$rs->EOF() ){

									if( $parent != AccountPlan::getLastParent( $rs->fields( "idaccount_plan" ) ) ){

										$parent = AccountPlan::getLastParent( $rs->fields( "idaccount_plan" ) );

										$i++;

										$rsparent =& DBUtil::query( "SELECT idaccounting_plan, accounting_plan$lang AS description FROM accounting_plan WHERE idaccounting_plan = '$parent'" );

										$results[ $i ] = array(
											"idaccount_plan"	=> $parent,
											"description"		=> $rsparent->fields( "description" ),
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);

										$j = 0;

									}

									$results[ $i ][ "childrens" ][ $j ] = array(
										"idrow"				=> $rs->fields( "idrow" ),
										"idaccount_plan"	=> $rs->fields( "idaccount_plan" ),
										"description"		=> $rs->fields( "designation" ),
										"idaccount_record"	=> "",
										"record"			=> $rs->fields( "idaccount_record" ),
										"record_type"		=> $rs->fields( "record_type" ),
										"third_party"		=> "",
										"date"				=> $rs->fields( "date" ),
										"account_plan_1"	=> $rs->fields( "account_plan_1" ),
										"account_plan_2"	=> $rs->fields( "account_plan_2" ),
										"account_plan_3"	=> $rs->fields( "account_plan_3" ),
										"account_plan_4"	=> $rs->fields( "account_plan_4" ),
										"lettering"			=> $rs->fields( "lettering" ),
										"debit"				=> $rs->fields( "debit" ),
										"credit"			=> $rs->fields( "credit" ),
										"amount"			=> $rs->fields( "amount" )
									);

									switch( $rs->fields( "relation_type" ) ){
										case "buyer":
											$results[ $i ][ "childrens" ][ $j ][ "third_party" ] = "<a href=\"$GLOBAL_START_URL/sales_force/contact_buyer.php?key=" . $rs->fields( "idrelation" ) . "\" onclick=\"window.open(this.href); return false;\">Client n°" . $rs->fields( "idrelation" ) . "</a>";
											break;
										case "supplier":
											$results[ $i ][ "childrens" ][ $j ][ "third_party" ] = '<a href="#" onclick="showSupplierInfos(' . $rs->fields( "idrelation" ) . '); return false;">' . DBUtil::getDBValue( "name", "supplier", "idsupplier", $rs->fields( "idrelation" ) ) . "</a>";
											break;
										case "others":
											$results[ $i ][ "childrens" ][ $j ][ "third_party" ] = $rs->fields( "idrelation" );
											break;
                                        case "provider":
											$results[ $i ][ "childrens" ][ $j ][ "third_party" ] = DBUtil::getDBValue( "name", "provider", "idprovider", $rs->fields( "idrelation" ) );
											break;
										default:
											$results[ $i ][ "childrens" ][ $j ][ "third_party" ] = "";
											break;
									}

									switch( $rs->fields( "record_type" ) ){
										case "billing_buyer":
											//$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = "<a href=\"$GLOBAL_START_URL/accounting/com_admin_invoice.php?IdInvoice=" . $rs->fields( "idaccount_record" ) . "\" onclick=\"window.open(this.href); return false;\"><strong>" . $rs->fields( "idaccount_record" ) . "</strong> (FC)</a>";
											$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = $rs->fields( "idaccount_record" );
											break;
										case "credits":
											//$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = "<a href=\"$GLOBAL_START_URL/accounting/credit.php?idcredit=" . $rs->fields( "idaccount_record" ) . "\" onclick=\"window.open(this.href); return false;\"><strong>" . $rs->fields( "idaccount_record" ) . "</strong> (AC)</a>";
											$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = "<a href=\"$GLOBAL_START_URL/accounting/credit.php?idcredit=" . $rs->fields( "idaccount_record" ) . "\" onclick=\"window.open(this.href); return false;\">" . $rs->fields( "idaccount_record" ) . "</a>";
											break;
										case "regulations_buyer":
											//$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = "<strong>" . $rs->fields( "idaccount_record" ) . "</strong> (RC)";
											$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = $rs->fields( "idaccount_record" ) . "";
											break;
										case "billing_supplier":
											//$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = "<strong>" . $rs->fields( "idaccount_record" ) . "</strong> (FF)";
											$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = $rs->fields( "idaccount_record" ) . "";
											break;
										default:
											$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ] = $rs->fields( "idaccount_record" );
											break;
									}

									$results[ $i ][ "debit" ]	+= $rs->fields( "debit" );
									$results[ $i ][ "credit" ]	+= $rs->fields( "credit" );
									$results[ $i ][ "amount" ]	+= $rs->fields( "amount" );
$results[ $i ][ "childrens" ][ $j ][ "idaccount_records" ] = $rs->fields( "idaccount_record" );
									$j++;
									$rs->MoveNext();

								}

							}else{

								while( !$rs->EOF() ){

									if( $parent != AccountPlan::getLastParent( $rs->fields( "idaccount_plan" ) ) ){

										$parent = AccountPlan::getLastParent( $rs->fields( "idaccount_plan" ) );

										$i++;

										$rsparent =& DBUtil::query( "SELECT idaccounting_plan, accounting_plan$lang AS description FROM accounting_plan WHERE idaccounting_plan = '$parent'" );

										$results[ $i ] = array(
											"idaccount_plan"	=> $parent,
											"description"		=> $rsparent->fields( "description" ),
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);

										$j = 0;

									}

									$results[ $i ][ "childrens" ][ $j ] = array(
										"idaccount_plan"	=> $rs->fields( "idaccount_plan" ),
										"description"		=> $rs->fields( "description" ),
										"debit"				=> $rs->fields( "debit" ),
										"credit"			=> $rs->fields( "credit" ),
										"amount"			=> $rs->fields( "amount" )
									);

									$results[ $i ][ "childrens" ][ $j ][ "idaccount_plan" ]	= $rs->fields( "idaccount_plan" );
									$results[ $i ][ "childrens" ][ $j ][ "description" ]	= $rs->fields( "description" );
									$results[ $i ][ "childrens" ][ $j ][ "debit" ]			= $rs->fields( "debit" );
									$results[ $i ][ "childrens" ][ $j ][ "credit" ]			= $rs->fields( "credit" );
									$results[ $i ][ "childrens" ][ $j ][ "amount" ]			= $rs->fields( "amount" );

									$results[ $i ][ "debit" ]	+= $rs->fields( "debit" );
									$results[ $i ][ "credit" ]	+= $rs->fields( "credit" );
									$results[ $i ][ "amount" ]	+= $rs->fields( "amount" );






									$j++;
									$rs->MoveNext();

								}

							}

							foreach( $results as $result ){

								if( isset( $_POST[ "detail" ] ) && $_POST[ "detail" ] == "1" ){

							?>
							<tr id="account_<?php echo $result[ "idaccount_plan" ] ?>" class="grasBack mainAccount" onmouseover="this.style.cursor='pointer';" onmouseout="this.style.cursor='default';" onclick="toggleSubAccounts('<?php echo $result[ "idaccount_plan" ] ?>');">
								<th class="lefterCol"></th>
								<th></th>
								<th><?php echo $result[ "idaccount_plan" ] ?></th>
								<th><?php echo stripslashes( $result[ "description" ] ) ?></th>
								<th></th>
								<th></th>
								<th></th>
								<th></th>
								<th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "debit" ] ) ?></th>
								<th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "credit" ] ) ?></th>
								<th class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "amount" ] ) ?></th>
							</tr>
								<?php

								}else{

								?>
							<tr id="account_<?php echo $result[ "idaccount_plan" ] ?>" class="grasBack mainAccount" onmouseover="this.style.cursor='pointer';" onmouseout="this.style.cursor='default';" onclick="toggleSubAccounts('<?php echo $result[ "idaccount_plan" ] ?>');">
								<th class="lefterCol"><?php echo $result[ "idaccount_plan" ] ?></th>
								<th><?php echo stripslashes( $result[ "description" ] ) ?></th>
								<th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "debit" ] ) ?></th>
								<th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "credit" ] ) ?></th>
								<th class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "amount" ] ) ?></th>
							</tr>
								<?php

								}

								foreach( $result[ "childrens" ] as $children ){

									if( isset( $_POST[ "detail" ] ) && $_POST[ "detail" ] == "1" ){
									

//-----------------------------------------------------Recherche fournisseur--------------------------------//
									
if( isset( $_POST[ "idsupplier" ] ) && intval( $_POST[ "idsupplier" ] ) ){
									
$rsr='';$idregulations_buyer='';$reglement_pro='';
								
//if(!empty($children[ "record_type" ] )){


if($children[ "account_plan_1" ] == 'BQ1'){
	$idaccount_record = $children[ "idaccount_records" ];
	$query = "
			SELECT  idsupplier,billing_supplier,idregulations_supplier,amount
			FROM billing_regulations_supplier
			WHERE idregulations_supplier = '$idaccount_record'
			";
	

	$rsr =& DBUtil::query( $query );
	if( $rsr === false )
	$reglement_pro='';



	while( !$rsr->EOF() ){
		$billing_supplier = $rsr->fields( "billing_supplier" );
		$amount = $rsr->fields( "amount" );
	 
	  	$qr1 = "
				SELECT  *
				FROM billing_regulations_supplier
				WHERE billing_supplier = '$billing_supplier'
				";
		
	
		$rs1 =& DBUtil::query( $qr1 );
		
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			$tot_amount .=  $rs1->fields( "amount" );
			$rs1->MoveNext();
		}
		
		
		if ($tot_amount == $amount )
			$reglement_pro = $billing_supplier.'    :'.$amount.' &euro;<br>'.$reglement_pro; 
		else
			$reglement_pro = "blocked";
	
	$rsr->MoveNext();
	}

}

if($children[ "account_plan_1" ] == 'ACH'){
	$idaccount_record = $children[ "idaccount_records" ];
$query = "
			SELECT  idsupplier,billing_supplier,idregulations_supplier,amount
			FROM billing_regulations_supplier
			WHERE billing_supplier = '$idaccount_record'
			";

	$rsr =& DBUtil::query( $query );
	if( $rsr === false )
		$reglement_pro='';
	
	while( !$rsr->EOF() )
	{
		$billing_supplier = $rsr->fields( "billing_supplier" );
		$idregulations_supplier = $rsr->fields( "idregulations_supplier" );
		$amount = $rsr->fields( "amount" );
		
		$qr1 = "
			SELECT  *
			FROM billing_regulations_supplier
			WHERE idregulations_supplier = '$idregulations_supplier'
			";

		$rs1 =& DBUtil::query( $qr1 );
		
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			$tot_amount .=  $rs1->fields( "amount" );
			$rs1->MoveNext();
		}
		
		
		if ($tot_amount == $amount )
			$reglement_pro = $idregulations_supplier.'    :'.$amount.' &euro;<br>'.$reglement_pro; 
		else
			$reglement_pro = "blocked";
		$rsr->MoveNext();
	}

}




								?>
                                 <?php
					//if($reglement_pro){ ?>
                                
							<tr id="resultRow_<?php echo $children[ "idrow" ] ?>" class="subAccount subAccount_<?php echo $result[ "idaccount_plan" ] ?>">
								<td class="lefterCol" style="text-align:center; white-space:nowrap;">
									<div class="bubbleInfo">
										<div class="trigger"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="?" /></div>
										<div class="popup">
											<?php if( !empty( $children[ "record_type" ] ) && !empty( $children[ "record_type" ] ) ){ ?><a href="#" onclick="getRecordList('<?php echo $children[ "record" ] ?>','<?php echo $children[ "record_type" ] ?>'); return false;">Visualiser les écritures de la pièce</a><br /><?php } ?>
											<a href="#" onclick="getEntryWindow(<?php echo $children[ "idrow" ] ?>); return false;">Modifier</a><br />
											<a href="#" onclick="confirmDropEntry(<?php echo $children[ "idrow" ] ?>); return false;">Suppprimer</a>
										</div>
									</div>
									<?php
					//} ?>
								</td>
                                
                                 <?php
					/*if (!$reglement_pro){ ?>
                                
							<tr id="resultRow_<?php echo $children[ "idrow" ] ?>" class="subAccount subAccount_<?php echo $result[ "idaccount_plan" ] ?>">
								<td class="lefterCol" style="text-align:center; white-space:nowrap;">
									
									<?php
					} */?>
								</td>
                                
                                <td style="text-align:center;"><?php echo $children[ "account_plan_1" ] ?></td>
								<td style="text-align:center;"><?php echo $children[ "idaccount_plan" ] ?></td>
								<td style="padding:0 15px;"><?php echo stripslashes( $children[ "description" ] ) ?></td>
								<td  style="padding-left:5px; padding-right:5px; white-space:nowrap;">
                                
                                
                                    <table width="60%" border="0" style="border:none">
                                    <tr style="border:none">
                                    <td style="border:none" width="50%"> <span class="record_help"><?php echo $children[ "idaccount_record" ] ?> </span></td>
                                    <td style="border:none" width="50%" align="right">	<!-----     <div style="margin-left:auto; margin-right:auto; text-align:center; width:55px;">  -->
									<?php
                                    if( isset( $_POST[ "idsupplier" ] ) && intval( $_POST[ "idsupplier" ] ) ){
                                    if($reglement_pro){
                                        $difference = Util::priceFormat($children[ "credit" ] - $children[ "debit" ]);
                                        $lettering = '';
                                        if ($children[ "lettering" ] == '')
                                            $lettering = '-';
                                        else
                                            $lettering = $children[ "lettering" ];
                 ?>
                                        <?php echo GenerateHTMLForProvider( $children[ "idrow" ],$children[ "idaccount_record" ],$children[ "amount" ],$reglement_pro,$difference, $lettering ) ?>
                                      <?php
                                }}
                                
                                    
                                    
                                    
                                //	print $reglement;
                                    else if($reglement_pro){
                
                                        $difference = Util::priceFormat($children[ "credit" ] - $children[ "debit" ]);
                                        $lettering = '';
                                        if ($children[ "lettering" ] == '')
                                            $lettering = '-';
                                        else
                                            $lettering = $children[ "lettering" ];
                
                 ?>
                                        <?php echo GenerateHTMLForToolTipBox( $children[ "idrow" ],$children[ "idaccount_record" ],$children[ "amount" ],$reglement_pro,$difference, $lettering ) ?>
                                      <?php
                                } ?>
                                    </div></td>
                                </tr>
                                </table>

                                </td>
								<td style="text-align:center;"><?php echo $children[ "third_party" ] ?>
                                
                                </td>
								<td style="text-align:center;"><?php echo Util::dateFormatEu( $children[ "date" ] ) ?></td>
								<td class="modifiableLetterCell" id="letter_<?php echo $children[ "idrow" ] ?>" style="text-align:center;"><?php echo $children[ "lettering" ] ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "debit" ] == 0 ? "-" : Util::priceFormat( $children[ "debit" ] ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "credit" ] == 0 ? "-" : Util::priceFormat( $children[ "credit" ] ) ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $children[ "amount" ] == 0 ? "-" : Util::priceFormat( $children[ "amount" ] ) ?></td>
                                	
							</tr>
                                              
								<?php
								
								
								}else{
								
//------------------------------------------------------Fin recherche fournisseur----------------------------------------------------------------
								
								
								$rsr='';$idregulations_buyer='';$reglement='';$reglement_p='';
								
//if(!empty($children[ "record_type" ] )){
if($children[ "idaccount_plan" ] == '41110000' && $children[ "account_plan_1" ] == 'VTE' && $children[ "account_plan_2" ] == 'D' && $children[ "account_plan_3" ] == 'D' && $children[ "account_plan_4" ] == 'A'){
	$idaccount_record = $children[ "idaccount_records" ];
//	print $children[ "idaccount_records" ];exit;
$query = "
			SELECT  idbilling_buyer,idregulations_buyer,amount
			FROM billing_regulations_buyer
			WHERE idbilling_buyer = '$idaccount_record'
			
			";

	$rsr =& DBUtil::query( $query );

//print $rsr->RecordCount();print "<br>";

if( $rsr === false )
	$reglement='';
	
	while( !$rsr->EOF() )
	{
		$idregulations_buyer = $rsr->fields( "idregulations_buyer" );
		$amount = $rsr->fields( "amount" );
		
		if(($rsr->RecordCount() == 1 ))
			$reglement = $idregulations_buyer.': '.$amount.' ¤<br>'.$reglement;
		else
			$reglement = "blocked";
	$rsr->MoveNext();
	}
	
}


if($children[ "idaccount_plan" ] == '41110000' && $children[ "account_plan_1" ] == 'BQ1' && $children[ "account_plan_2" ] == 'D' && $children[ "account_plan_3" ] == 'C' && $children[ "account_plan_4" ] == 'A'){
	$idaccount_record = $children[ "idaccount_records" ];

$query = "
			SELECT  idbilling_buyer,idregulations_buyer,amount
			FROM billing_regulations_buyer
			WHERE idregulations_buyer = '$idaccount_record'
			
			";

	

	$rsr =& DBUtil::query( $query );
if( $rsr === false )
	$reglement='';


	
	while( !$rsr->EOF() )
	{
		$idregulations_buyer = $rsr->fields( "idregulations_buyer" );
		$idbilling_buyer = $rsr->fields( "idbilling_buyer" );
		$amount = $rsr->fields( "amount" );
		
		
		$qr1 = "
			SELECT  *
			FROM billing_regulations_buyer
			WHERE idbilling_buyer = '$idbilling_buyer'
			
			";

		$rs1 =& DBUtil::query( $qr1 );
		 
		 
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			$tot_amount .=  $rs1->fields( "amount" );
			$rs1->MoveNext();
		}
		
		
		if ($tot_amount == $amount )
			$reglement = $idbilling_buyer.': '.$amount.' ¤<br>'.$reglement;
		else
			$reglement ="blocked";
		$rsr->MoveNext();
	}

}

if($children[ "record_type" ] == 'billing_supplier' &&$children[ "idaccount_plan" ] == '40110000' && $children[ "account_plan_1" ] == 'ACH' && $children[ "account_plan_2" ] == 'D' && $children[ "account_plan_3" ] == 'C' && $children[ "account_plan_4" ] == 'A'){

	$idaccount_record = $children[ "idaccount_records" ];
$query = "
			SELECT  idsupplier,idregulations_supplier,amount
			FROM billing_regulations_supplier
			WHERE billing_supplier = '$idaccount_record'
			";

	

	$rsr =& DBUtil::query( $query );
if( $rsr === false )
									$reglement='';

while( !$rsr->EOF() ){
 $idregulations_buyer = $rsr->fields( "idregulations_provider" );
 $amount = $rsr->fields( "amount" );
$reglement = $idregulations_buyer.'('.$amount.')<br>'.$reglement;

$rsr->MoveNext();
}
}

//rechercher de service
if($children[ "record_type" ] == 'provider_invoice' && ($children[ "idaccount_plan" ] == '40210000' || $children[ "idaccount_plan" ] == '40220000' || $children[ "idaccount_plan" ] == '40230000' ) && $children[ "account_plan_1" ] == 'ACH' && $children[ "account_plan_2" ] == 'D' && $children[ "account_plan_3" ] == 'C' && $children[ "account_plan_4" ] == 'A'){

	$idaccount_record = $children[ "idaccount_records" ];
	$query = "
			SELECT  idprovider_invoice,idregulations_provider,amount
			FROM billing_regulations_provider
			WHERE idprovider_invoice = '$idaccount_record'
			";

	

	$rsr =& DBUtil::query( $query );
if( $rsr === false )
									$reglement_p='';

	
	while( !$rsr->EOF() )
	{
		$idprovider_invoice = $rsr->fields( "idprovider_invoice" );
		$iregulations_provider = $rsr->fields( "idregulations_provider" );
		$amount = $rsr->fields( "amount" );
		
		$qr1 = "
			SELECT *
			FROM billing_regulations_provider
			WHERE idregulations_provider = '$iregulations_provider'
			";

		

		$rs1 =& DBUtil::query( $qr1 );
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			$tot_amount .=  $rs1->fields( "amount" );
			$rs1->MoveNext();
		}
		
		
		if ($tot_amount == $amount )
			$reglement_p = $iregulations_provider.': '.$amount.' ¤<br>'.$reglement_p;
		else
			$reglement_p = "blocked";
			
		$rsr->MoveNext();
	}

}


if($children[ "record_type" ] == 'regulations_provider' && ($children[ "idaccount_plan" ] == '40210000' || $children[ "idaccount_plan" ] == '40220000' || $children[ "idaccount_plan" ] == '40230000' ) && $children[ "account_plan_1" ] == 'BQ1' && $children[ "account_plan_2" ] == 'D' && $children[ "account_plan_3" ] == 'D' && $children[ "account_plan_4" ] == 'A'){

	$idaccount_record = $children[ "idaccount_records" ];
	$query = "
			SELECT  idprovider_invoice,idregulations_provider,amount
			FROM billing_regulations_provider
			WHERE idregulations_provider = '$idaccount_record'
			";

	

	$rsr =& DBUtil::query( $query );
if( $rsr === false )
									$reglement_p='';
	
	while( !$rsr->EOF() )
	{
		$idprovider_invoice = $rsr->fields( "idprovider_invoice" );
		$iregulations_provider = $rsr->fields( "idregulations_provider" );
		$amount = $rsr->fields( "amount" );
		
		$qr1 = "
			SELECT  *
			FROM billing_regulations_provider
			WHERE idprovider_invoice = '$idprovider_invoice'
			";

	

		$rs1 =& DBUtil::query( $qr1 );
		
		$tot_amount = 0;
		
		while( !$rs1->EOF() )
		{
			$tot_amount =  $tot_amount + $rs1->fields( "amount" );
			
			$rs1->MoveNext();
		}
		 
		
		if ($tot_amount == $amount )
		
			$reglement_p = $idprovider_invoice.': '.$amount.' ¤<br>'.$reglement_p;
		else
			$reglement_p = "blocked";
			
		$rsr->MoveNext();
	}

}

//fin rechercher de service


								?>
                                 <?php
				//	if($reglement){ ?>
                                
							<tr id="resultRow_<?php echo $children[ "idrow" ] ?>" class="subAccount subAccount_<?php echo $result[ "idaccount_plan" ] ?>">
								<td class="lefterCol" style="text-align:center; white-space:nowrap;">
									<div class="bubbleInfo">
										<div class="trigger"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/help.png" alt="?" /></div>
										<div class="popup">
											<?php if( !empty( $children[ "record_type" ] ) && !empty( $children[ "record_type" ] ) ){ ?><a href="#" onclick="getRecordList('<?php echo $children[ "record" ] ?>','<?php echo $children[ "record_type" ] ?>'); return false;"><?php echo Dictionnary::translate("tlt_59") ?></a><br /><?php } ?>
											<a href="#" onclick="getEntryWindow(<?php echo $children[ "idrow" ] ?>); return false;"><?php echo Dictionnary::translate("tlt_60") ?></a><br />
											<a href="#" onclick="confirmDropEntry(<?php echo $children[ "idrow" ] ?>); return false;"><?php echo Dictionnary::translate("tlt_61") ?></a>
										</div>
									</div>
									<?php
					//} ?>
								</td>
                                
                                 <?php
					/*if (!$reglement){ ?>
                                
							<tr id="resultRow_<?php echo $children[ "idrow" ] ?>" class="subAccount subAccount_<?php echo $result[ "idaccount_plan" ] ?>">
								<td class="lefterCol" style="text-align:center; white-space:nowrap;">
									
									<?php
				*/?>
								</td>
                                
                                <td style="text-align:center;"><?php echo $children[ "account_plan_1" ] ?></td>
								<td style="text-align:center;"><?php echo $children[ "idaccount_plan" ] ?></td>
								<td style="padding:0 15px;"><?php echo stripslashes( $children[ "description" ] ) ?></td>
								<td  style="padding-left:5px; padding-right:5px; white-space:nowrap;">
                                                             
                                <table width="60%" border="0" style="border:none">
  								<tr style="border:none">
    							<td style="border:none" width="50%"><span class="record_help"><?php echo $children[ "idaccount_record" ] ?> </span></td>
    
    
					<!-----     <div style="margin-left:auto; margin-right:auto; text-align:center; width:55px;">  -->
                    <?php
					if( isset( $_POST[ "idsupplier" ] ) && intval( $_POST[ "idsupplier" ] ) ){
					if($reglement){ ?>
					<td style="border:none" width="50%" align="right"><?php echo GenerateHTMLForProvider( $children[ "idrow" ],$children[ "idaccount_record" ],$reglement ) ?></td>	
					  <?php
				}}
				
					
					
					
				//	print $reglement;
					else if($reglement){
?>
<td style="border:none" width="50%" align="right">
<?php

$difference = Util::priceFormat($children[ "credit" ] - $children[ "debit" ]);
						$lettering = '';
						if ($children[ "lettering" ] == '')
							$lettering = '-';
						else
							$lettering = $children[ "lettering" ];




 ?>
						<?php echo GenerateHTMLForToolTipBox( $children[ "idrow" ],$children[ "idaccount_record" ],$children[ "amount" ],$reglement,$difference, $lettering ) ?>
                        
                        </td>
						
					  <?php
				} 

					else if($reglement_p){ 
					
					?> <td style="border:none" width="50%">
                    
                    <?php
                    
                    $difference = Util::priceFormat($children[ "credit" ] - $children[ "debit" ]);
						$lettering = '';
						if ($children[ "lettering" ] == '')
							$lettering = '-';
						else
							$lettering = $children[ "lettering" ];


?>
						<?php echo GenerateHTMLForToolTipBox_provider( $children[ "idrow" ],$children[ "idaccount_record" ],$children[ "amount" ], $reglement_p,$difference, $lettering ) ?></td>
						
					  <?php
				} ?>
					</div>
					</tr>
					</table>
                                </td>
								<td style="text-align:center;"><?php echo $children[ "third_party" ] ?>
                                
                                </td>
								<td style="text-align:center;"><?php echo Util::dateFormatEu( $children[ "date" ] ) ?></td>
								<td class="modifiableLetterCell" id="letter_<?php echo $children[ "idrow" ] ?>" style="text-align:center;"><?php echo $children[ "lettering" ] ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "debit" ] == 0 ? "-" : Util::priceFormat( $children[ "debit" ] ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "credit" ] == 0 ? "-" : Util::priceFormat( $children[ "credit" ] ) ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $children[ "amount" ] == 0 ? "-" : Util::priceFormat( $children[ "amount" ] ) ?></td>
                                	
							</tr>
								
								<?php
								
								
								}
								
								}else{
								
								//----------------------------------------------------------------------------------------------------------------------

									
									
									
									

								?>
							<tr class="subAccount_<?php echo $result[ "idaccount_plan" ] ?>" style="display:none;">
								<td class="lefterCol" style="padding-left:25px;"><?php echo $children[ "idaccount_plan" ] ?></td>
								<td><?php echo stripslashes( $children[ "description" ] ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "debit" ] == 0 ? "-" : Util::priceFormat( $children[ "debit" ] ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "credit" ] == 0 ? "-" : Util::priceFormat( $children[ "credit" ] ) ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $children[ "amount" ] == 0 ? "-" : Util::priceFormat( $children[ "amount" ] ) ?></td>
							</tr>
								<?php

									}

								}

							}

							?>
						</tbody>
					</table>
                  
				</div><?php } ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<script type="text/javascript">
	<!--

		$(function () {
			$('.bubbleInfo').each(function () {
				var distance = 10;
				var time = 250;
				var hideDelay = 500;

				var hideDelayTimer = null;

				var beingShown = false;
				var shown = false;
				var trigger = $('.trigger', this);
				var info = $('.popup', this).css('opacity', 0);

				$([trigger.get(0), info.get(0)]).mouseover(function () {
					if (hideDelayTimer) clearTimeout(hideDelayTimer);
					if (beingShown || shown) {
						// don't trigger the animation again
						return;
					} else {
						//hide other bubbles
						$('.bubbleInfo').each(function () {
							$('.popup', this).css('opacity', 0).css('display','none');
						});

						// reset position of info box
						beingShown = true;

						info.css({
							top: -27,
							right: -240,
							display: 'block'
						}).animate({
							right: '-=' + distance + 'px',
							opacity: 1
						}, time, 'swing', function() {
							beingShown = false;
							shown = true;
						});
					}

					return false;
				}).mouseout(function () {
					if (hideDelayTimer) clearTimeout(hideDelayTimer);
					hideDelayTimer = setTimeout(function () {
						hideDelayTimer = null;
						info.animate({
							right: '+=' + distance + 'px',
							opacity: 0
						}, time, 'swing', function () {
							shown = false;
							info.css('display', 'none');
						});

					}, hideDelay);

					return false;
				});
			});

		});

	-->
	</script>
	<style type="text/css">
	<!--

		.bubbleInfo{
			position:relative;
		}

		/* Bubble pop-up */

		.popup{
			background-image:url('<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bubble.png');
			background-position:top left;
			background-repeat:no-repeat;
			border-collapse:collapse;
			display:none;
			font-size:12px;
			height:80px;
			line-height:18px;
			padding:5px 5px 5px 22px;
			position:absolute;
			text-align:left;
			width:226px;
			z-index:50;
		}

	-->
	</style>
<?php

}
function GenerateHTMLForToolTipBox( $idrow,$idaccount_record=0,$amount=0,$reglements=0,$diff=0,$lettering=''){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	
		
	$qr1 = "
			SELECT  *
			FROM billing_regulations_buyer
			WHERE idbilling_buyer = '$idaccount_record'
			
			";
	$rs1 =& DBUtil::query( $qr1 );
	
	
	if($rs1->RecordCount() > 0 )
	{
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			$tot_amount =  $tot_amount + $rs1->fields( "amount" );
			$rs1->MoveNext();
		}
	$diff = Util::priceFormat(abs($amount) - abs($tot_amount));
	}
	
	$qr2 = "
			SELECT *
			FROM billing_regulations_buyer
			WHERE idregulations_buyer = '$idaccount_record'
			";

	

	$rs2 =& DBUtil::query( $qr2 );
	
	if($rs2->RecordCount() > 0 )
	{	
		$tot_amount = 0;
		
		while( !$rs2->EOF() )
		{
			$tot_amount =  $tot_amount + $rs2->fields( "amount" );
			
			$rs2->MoveNext();
		}
	$diff = Util::priceFormat(abs($amount) - abs($tot_amount));
	}
	
	if ($reglements == "blocked")
	{
		$html = "<img src='".$GLOBAL_START_URL."/images/sens-interdit.png' width='15px' height='15px'>";
	}
	else
	{
	
		$html = "<span onmouseover=\"document.getElementById('pop$idrow').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idrow').className='tooltip';\" style=\"position:relative;  display:block;\"><b style=\"color:#990000;font-size:20px\">+</b>";
		
				
		$html .= "<span id='pop$idrow' class='tooltip'>
	<div class=\"content\" style=\"padding:5px;\">
	<div class=\"subContent\">";
	
			
			$html .= "<div style=\"font-size: 12px; white-space:nowrap;\">". $idaccount_record .': '. $amount . ' ¤<br>' . $reglements;
		
			$html .= "<br>";
			$html .= "Différence:" . $diff  ." <br>   Lettering: <span class='modifiableLetterCell' id='letter_$idrow' style=\"border:1px solid;  margin: 4px; padding: 0 20px 0 17px;\"> ".$lettering . "</span>";
	
		$html .= "
			</div>
			</div>
			</div>
			</span>
			</span>";
	}
	return $html;
	
}
//-----------------------------------------------------------------------------------------------

function GenerateHTMLForToolTipBox_provider( $idrow,$idaccount_record=0,$amount=0,$reglements=0,$diff=0,$lettering=''){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	
	
	$qr1 = "
			SELECT *
			FROM billing_regulations_provider
			WHERE idregulations_provider = '$idaccount_record'
			";

		

		$rs1 =& DBUtil::query( $qr1 );
	
	
	if($rs1->RecordCount() > 0 )
	{
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			
			$tot_amount =  $tot_amount + $rs1->fields( "amount" );
			
			$rs1->MoveNext();
		}
		
	$diff = Util::priceFormat(abs($amount) - abs($tot_amount));
	}
	
	
	
			$qr2 = "
			SELECT  *
			FROM billing_regulations_provider
			WHERE idprovider_invoice = '$idaccount_record'
			";

	

		$rs2 =& DBUtil::query( $qr2 );
	
	if($rs2->RecordCount() > 0 )
	{	
		$tot_amount = 0;
		
		while( !$rs2->EOF() )
		{
			$tot_amount =  $tot_amount + $rs2->fields( "amount" );
			
			$rs2->MoveNext();
		}
	$diff = Util::priceFormat(abs($amount) - abs($tot_amount));
	}
	
	
	
	if ($reglements == "blocked")
	{
		$html = "<img src='".$GLOBAL_START_URL."/images/sens-interdit.png' width='15px' height='15px'>";
	}
	else
	{
		$html = "<span onmouseover=\"document.getElementById('pop$idrow').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idrow').className='tooltip';\" style=\"position:relative; display:block;\"><b style=\"color:#990000;font-size:20px\">+</b>";
		
		
		
		$html .= "<span id='pop$idrow' class='tooltip'>
	<div class=\"content\" style=\"padding:5px;\">
	<div class=\"subContent\">";
	
			
			$html .= "<div style=\"font-size: 12px; white-space:nowrap;\">". $idaccount_record .': '. $amount . ' ¤<br>' . $reglements;
			
			$html .= "<br>";
			$html .= "Différence:" . $diff  ." <br>   Lettering: <span class='modifiableLetterCell' id='letter_$idrow' style=\"border:1px solid;  margin: 4px; padding: 0 20px 0 17px;\"> ".$lettering . "</span>";
		
		$html .= "
			</div>
			</div>
			</div>
			</span>
			</span>";
	}
	
	
	return $html;
	
}
//-----------------------------------------------------------------------------------------------

function GenerateHTMLForProvider( $idrow,$idaccount_record=0,$amount=0,$reglements=0,$diff=0,$lettering='' ){
	
	global $GLOBAL_START_URL;
	
	$db = &DBUtil::getConnection();
	
	$qr1 = "
			SELECT  *
				FROM billing_regulations_supplier
				WHERE billing_supplier = '$idaccount_record'
			";

		$rs1 =& DBUtil::query( $qr1 );
	
	if($rs1->RecordCount() > 0 )
	{
		$tot_amount = 0;
		while( !$rs1->EOF() )
		{
			$tot_amount =  $tot_amount + $rs1->fields( "amount" );
			$rs1->MoveNext();
		}
	$diff = Util::priceFormat(abs($amount) - abs($tot_amount));
	}
	
			$qr2 = "
			SELECT  *
			FROM billing_regulations_supplier
			WHERE idregulations_supplier = '$idaccount_record'
			";

		$rs2 =& DBUtil::query( $qr2 );
	
	if($rs2->RecordCount() > 0 )
	{	
		$tot_amount = 0;
		
		while( !$rs2->EOF() )
		{
			$tot_amount =  $tot_amount + $rs2->fields( "amount" );
			
			$rs2->MoveNext();
		}
	$diff = Util::priceFormat(abs($amount) - abs($tot_amount));
	}
		
	if ($reglements == "blocked")
	{
		$html = "<img src='".$GLOBAL_START_URL."/images/sens-interdit.png' width='15px' height='15px'>";
	}
	else
	{
	
		$html = "<span onmouseover=\"document.getElementById('pop$idrow').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idrow').className='tooltip';\" style=\"position:relative; display:block;\"><b style=\"color:#990000;font-size:20px\">+</b>";
		
		
		
		$html .= "<span id='pop$idrow' class='tooltip'>
	<div class=\"content\" style=\"padding:5px;\">
	<div class=\"subContent\">";
	
			
			$html .= "<div style=\"font-size: 12px; white-space:nowrap;\">". $idaccount_record .': '. $amount . ' ¤<br>' . $reglements;
			
			$html .= "<br>";
			$html .= "Différence:" . $diff  ." <br>   Lettering: <span class='modifiableLetterCell' id='letter_$idrow' style=\"border:1px solid;  margin: 4px; padding: 0 20px 0 17px;\"> ".$lettering . "</span>";
		
		$html .= "
			</div>
			</div>
			</div>
			</span>
			</span>";
	}
	
	return $html;
	
}

//------------------------------------------------------------------------------------------------

function getSearchWhereCondition(){

	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	$maxSearchResults = 2500;

	$lang = User::getInstance()->getLang();

	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );

	$where = "
	accounting_plan.idaccounting_plan = account_plan.idaccount_plan";

	//post data

	//strings

	$strings = array( "contact.lastname", "buyer.company", "account_plan.lettering" );

	foreach( $strings as $string ){

		list( $table, $column ) = explode( ".", $string );

		if( isset( $_POST[ $column ] ) && strlen( $_POST[ $column ] ) )
			$where .= "\n\tAND `$table`.`$column` LIKE '%" . Util::html_escape( $_POST[ $column ] ) . "%'";

	}

	//numéro de compte

	if( isset( $_POST[ "idaccount_plan" ] ) && !empty( $_POST[ "idaccount_plan" ] ) && $_POST[ "idaccount_plan" ] != "all" ){

		$accounts = array( $_POST[ "idaccount_plan" ] );

		//recherche des enfants

		$accounts =& AccountPlan::getAccountChildrens( $accounts );

		$where .= "\n\tAND account_plan.idaccount_plan IN( '" . implode( $accounts, "', '" ) . "' )";

	}

	//montants

	if( isset( $_POST[ "amount" ] ) && !empty( $_POST[ "amount" ] ) )
		$where .= "\n\tAND account_plan.amount = '" . Util::html_escape( $_POST[ "amount" ] ) . "'";

	if( isset( $_POST[ "amount_bottom" ] ) && !empty( $_POST[ "amount_bottom" ] ) )
		$where .= "\n\tAND account_plan.amount >= '" . Util::html_escape( $_POST[ "amount_bottom" ] ) . "'";

	if( isset( $_POST[ "amount_top" ] ) && !empty( $_POST[ "amount_top" ] ) )
		$where .= "\n\tAND account_plan.amount <= '" . Util::html_escape( $_POST[ "amount_top" ] ) . "'";

	//date

	switch( $_POST[ "interval_select" ] ){

		case 1:

			$start = Util::dateFormatus( $_POST[ "search_start" ] );
		 	$end = Util::dateFormatUs( $_POST[ "search_end" ] );

			$where .= "\n\tAND account_plan.date >= '$start'\n\tAND account_plan.date <= '$end'";

		 	break;

		//Affichage des lignes venant d'être générées
		case 2:

		 	$start = $_POST[ "start" ];
		 	$end = $_POST[ "end" ];

			$where .= "\n\tAND account_plan.creation_date >= '$start'\n\tAND account_plan.creation_date <= '$end'";

		 	break;

		//Affichage des lignes d'une pièce comptable
		case 3:

		 	$start = DBUtil::getDBValue( "start_date", "fiscal_years", "idfiscal_year", $_POST[ "fiscal_year" ] );

		 	$rs =& DBUtil::query( "SELECT start_date FROM fiscal_years WHERE start_date > '$start' ORDER BY start_date ASC LIMIT 1" );

		 	if( $rs === false )
		 		trigger_error( "Impossible de récupérer la date de fin de l'exercice comptable", E_USER_ERROR );

		 	$end = $rs->RecordCount() ? $rs->fields( "start_date" ) : date( "Y-m-d" );

			$where .= "\n\tAND account_plan.date >= '$start'\n\tAND account_plan.date < '$end'";

		 	break;

	}

	//Si recherche par numéro de client
	$idbuyer = $_POST[ "idbuyer" ];
	if( isset( $_POST[ "idbuyer" ] ) && ( $_POST[ "idbuyer" ] ) )
		$where .= "\n\tAND account_plan.relation_type = 'buyer'\n\tAND account_plan.idrelation = '$idbuyer' ";

	//Si recherche par numéro de fournisseur
	if( isset( $_POST[ "idsupplier" ] ) && intval( $_POST[ "idsupplier" ] ) )
		$where .= "\n\tAND account_plan.relation_type = 'supplier'\n\tAND account_plan.idrelation = " . intval( $_POST[ "idsupplier" ] );
	//Si recherche par numéro de provider
	if( isset( $_POST[ "idprovider" ] ) && intval( $_POST[ "idprovider" ] ) )
		$where .= "\n\tAND account_plan.relation_type = 'provider'\n\tAND account_plan.idrelation = " . intval( $_POST[ "idprovider" ] );
	//Si recherche par numéro de facture
	if( isset( $_POST[ "idcustomer_invoice" ] ) && intval( $_POST[ "idcustomer_invoice" ] ) )
		$where .= "\n\tAND account_plan.record_type = 'billing_buyer'\n\tAND account_plan.idaccount_record = " . intval( $_POST[ "idcustomer_invoice" ] );

	//Si recherche par numéro d'avoir
	if( isset( $_POST[ "idcredit" ] ) && intval( $_POST[ "idcredit" ] ) )
		$where .= "\n\tAND account_plan.record_type = 'credits'\n\tAND account_plan.idaccount_record = " . intval( $_POST[ "idcredit" ] );

	//Si recherche par numéro de règlement
	if( isset( $_POST[ "idsupplier_invoice" ] ) && !empty( $_POST[ "idsupplier_invoice" ] ) )
		$where .= "\n\tAND account_plan.record_type = 'billing_supplier'\n\tAND account_plan.idaccount_record LIKE '%" . Util::html_escape( $_POST[ "idsupplier_invoice" ] ) . "%'";

	if( isset( $_POST[ "idprovider_invoice" ] ) && !empty( $_POST[ "idprovider_invoice" ] ) )
		$where .= "\n\tAND account_plan.record_type = 'provider_invoice'\n\tAND account_plan.idaccount_record LIKE " . DBUtil::quote( "%" . $_POST[ "idprovider_invoice" ] . "%" );

	//Visualisation des écritures d'une pièce
	if( isset( $_POST[ "record" ] ) && !empty( $_POST[ "record" ] ) && isset( $_POST[ "record_type" ] ) && !empty( $_POST[ "record_type" ] ) )
		$where .= "\n\tAND account_plan.record_type = '" . Util::html_escape( $_POST[ "record_type" ] ) . "'\n\tAND account_plan.idaccount_record = '" . Util::html_escape( $_POST[ "record" ] ) . "'";


	
	if( isset( $_POST[ "piece" ] ) && ( $_POST[ "piece" ] ) )
		$where .= "\n\tAND account_plan.idaccount_record = '" . $_POST[ "piece" ] . "' ";


	//Interrogation de tiers
	if( isset( $_POST[ "thirdparty" ] ) && $_POST[ "thirdparty" ] == "1" ){

		/*$accounts = array( "40", "41" );
		$accounts =& AccountPlan::getAccountChildrens( $accounts );

		$where .= "\n\tAND account_plan.idaccount_plan IN( '" . implode( $accounts, "', '" ) . "' )";*/

	}

	return $where;

}

//---------------------------------------------------------------------------------------------

function generate(){

	//Clients
	generateCustomerInvoices();				//Factures
	generateCustomerRegulations();			//Règlements
	generateCustomerFactorInvoices();		//Factures vendues au factor
	generateCustomerFactorRegulations();	//Règlements reçus par le factor
	generateCustomerFactorRefusal();		//Définancement factor
	generateCustomerPaidFactorRefused();	//Facture définancée par le factor puis payée chez lui
	generateCustomerCredits();				//Avoirs
	generateCustomerUsedCredits();		//Utilisation des avoirs	//Il n'y a aucune écriture à faire dans le cas de l'utilisation d'avoir normalement. Seules les écritures faites pour l'émission d'un avoir sont nécessaires
	generateCustomerRebates();				//Escomptes
	generateCustomerRepayments();			//Remboursements
	generateCustomerAdjustments();			//Ecarts de paiement
	
	//Fournisseurs
	generateSupplierInvoices();				//Factures
	generateSupplierRegulations();			//Règlements
	generateSupplierCredits();				//Avoirs
	generateSupplierRebates();				//Escomptes
	generateSupplierAdjustments();			//Ecarts de paiement


	/* prestataires de services */
	generateProviderInvoices();				//Factures
	generateProviderRegulations();			//Règlements
	generateProviderRebates();			//Escomptes
}

//---------------------------------------------------------------------------------------------

function generateCustomerInvoices(){
	$action = "customer_invoices";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
		billing_buyer.*,
		`order`.iddelivery,
		payment.name$lang AS payment";

	$tables = "
		billing_buyer, `order`, payment";

	$where = "
		billing_buyer.DateHeure < '$today'
		AND billing_buyer.idorder = `order`.idorder
		AND billing_buyer.idpayment = payment.idpayment
		AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		ORDER BY billing_buyer.idbilling_buyer";

	//echo $query;
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

//------------------------------------------------------------------------------------------------------------------------

$idorder = $rs->fields( "idorder" );
	$idbilling_buyer = $rs->fields( "idbilling_buyer" );

				
		$total_discount = $rs->fields( "total_discount" );
	
		
	//----------------------------------------------------------------
	//Ventes
	$total_amount_1 = round( $rs->fields( "total_amount_1" ), 2 );
	$total_amount_1 = ((100 - $total_discount)/100) * $total_amount_1;
	$total_amount_1 = round ($total_amount_1, 2 );
	
	$total_amount_2 = round( $rs->fields( "total_amount_2" ), 2 );
	$total_amount_2 = ((100 - $total_discount)/100) * $total_amount_2;
	$total_amount_2 = round ($total_amount_2, 2 );
	
	$total_amount_3 = round( $rs->fields( "total_amount_3" ), 2 );
	$total_amount_3 = ((100 - $total_discount)/100) * $total_amount_3;
	$total_amount_3 = round ($total_amount_3, 2 );
	
	
	//TVAs
	
	//Le port est ajouté à total_amount_1 dans la table billing_buyer
	$tva_charge =  round( $rs->fields( "total_charge" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 );
	
	$total_vat_1 = round( $rs->fields( "total_vat_1" ), 2 );
	$total_vat_1 = ((100 - $total_discount)/100) * $total_vat_1;
	$total_vat_1 = $total_vat_1 + $tva_charge;
	$total_vat_1 = round ($total_vat_1, 2 );

	$total_vat_2 = round( $rs->fields( "total_vat_2" ), 2 );
	$total_vat_2 = ((100 - $total_discount)/100) * $total_vat_2;
	$total_vat_2 = round ($total_vat_2, 2 );

	$total_vat_3 = round( $rs->fields( "total_vat_3" ), 2 );
	$total_vat_3 = ((100 - $total_discount)/100) * $total_vat_3;
	$total_vat_3 = round ($total_vat_3, 2 );



		if( round( $rs->fields( "total_amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= round( $rs->fields( "total_amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Facture " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "deliv_payment" );
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		if( round( $rs->fields( "total_amount_ht" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 ) > 0 ){

			//707 - Vente
			
			// 70710000
if($total_amount_1 > 0 ){
			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 10, $rs->fields( "no_tax_export" ), 1 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= $total_amount_1;
			$amount_type		= "credit";
			$designation		= "Vente facture 20.00 " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			}
			//70711000
			if($total_amount_2 > 0 ){
			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 10, $rs->fields( "no_tax_export" ), 2 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= $total_amount_2;
			$amount_type		= "credit";
			$designation		= "Vente facture 5.50 " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			}
			//
			if($total_amount_3 > 0 ){
			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 10, $rs->fields( "no_tax_export" ) , 3 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= $total_amount_3;
			$amount_type		= "credit";
			$designation		= "Vente facture 7.00 " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			}

		}

		if( round( $rs->fields( "total_charge_ht" ), 2 ) > 0 ){

			//708 - Port vente

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 11, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= round( $rs->fields( "total_charge_ht" ), 2 );
			$amount_type		= "credit";
			$designation		= "Port vente facture " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}
		
		
		
		if( round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 ) > 0 ){

			//445 - TVA
			// 44566190
if($total_vat_1 > 0 ){
			if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				//$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 5 , 2 , 3 );
				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 3, 2 , 1 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "DateHeure" );
				$amount				=  $total_vat_1;
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA collectée 20.00 ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";
//print $idaccount_plan;exit;
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}
			}
			
			// 44566191
			
			if($total_vat_2 > 0 ){
			if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 3 , 2 , 2 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "DateHeure" );
				$amount				=  $total_vat_2;
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA collectée 5.50 ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";
//print $idaccount_plan;exit;
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}
			}
			// 44571130
			if($total_vat_3 > 0 ){
			if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 3 , 3 , 3 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "DateHeure" );
				$amount				= $total_vat_3;
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA collectée 7.00 ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";
//print $idaccount_plan;exit;
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}
			}
			

}


if( round( $rs->fields( "total_charge" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 ) > 0 ){
$tva_charge =  round( $rs->fields( "total_charge" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 );
			
			
			//708 - TVA port achat
			

		/*	if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 5 , 2 , 1 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "DateHeure" );
				$amount				=  $tva_charge;
				$amount_type		= "credit";
				$designation		= "TVA port collectée 19.60 " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";
//print $idaccount_plan;exit;
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			
			}
			*/
			}
			
		$rs->MoveNext();

	}

	return true;

}


//---------------------------------------------------------------------------------------------

function generateCustomerFactorInvoices(){

	$action = "customer_factor_invoices";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	billing_buyer.idbilling_buyer,
	billing_buyer.total_amount,
	billing_buyer.factor_send_date,
	billing_buyer.deliv_payment,
	billing_buyer.idbuyer,
	billing_buyer.no_tax_export,
	`order`.iddelivery,
	payment.name$lang AS payment";

	$tables = "
	billing_buyer, `order`, payment";

	$where = "
	billing_buyer.factor_send_date < '$today'
	AND billing_buyer.factor_send_date != '0000-00-00'
	AND billing_buyer.idorder = `order`.idorder
	AND billing_buyer.idpayment = payment.idpayment
	AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		GROUP BY billing_buyer.idbilling_buyer
		ORDER BY billing_buyer.idbilling_buyer ASC";


	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements factor à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "total_amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "factor_send_date" );
			$amount				= round( $rs->fields( "total_amount" ), 2 );
			$amount_type		= "credit";
			$designation		= "Remise factor";
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "deliv_payment" );
			$account_plan_1		= "FACT";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//467 - Factor

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 7 );
			$idrelation			= "";
			$relation_type		= "";
			$idaccount_record	= "";
			$record_type		= "";
			$date				= $rs->fields( "factor_send_date" );
			$amount				+= round( $rs->fields( "total_amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Factor CMDP";
			$code_eu			= "";
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "FACT";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerRegulations(){

	$action = "customer_regulations";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );
/*
	$select = "
	regulations_buyer.idregulations_buyer,
	regulations_buyer.payment_date,
	regulations_buyer.idpayment,
	regulations_buyer.idbuyer,
	regulations_buyer.amount,
	billing_buyer.no_tax_export,
	`order`.iddelivery,
	payment.name$lang AS payment";

	$tables = "billing_buyer, billing_regulations_buyer, `order`, payment, regulations_buyer";

	$where = "
	billing_buyer.idbilling_buyer = billing_regulations_buyer.idbilling_buyer
	AND billing_buyer.idorder = `order`.idorder
	AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer
	AND regulations_buyer.idpayment = payment.idpayment
	AND regulations_buyer.payment_date < '$today'
	AND regulations_buyer.payment_date != '0000-00-00'
	AND regulations_buyer.factor = 0
	AND regulations_buyer.accept = 1
	AND regulations_buyer.idregulations_buyer NOT IN ( $notIn )";
*/


	$select = "
	regulations_buyer.idregulations_buyer,
	regulations_buyer.payment_date,
	regulations_buyer.idpayment,
	regulations_buyer.idbuyer,
	regulations_buyer.amount,
	`order`.iddelivery,
	payment.name$lang AS payment";

	$tables = "`order`, payment, regulations_buyer";

	$where = "
	regulations_buyer.idpayment = payment.idpayment
	AND regulations_buyer.payment_date < '$today'
	AND regulations_buyer.payment_date != '0000-00-00'
	AND regulations_buyer.factor = 0
	AND regulations_buyer.accept = 1
	AND regulations_buyer.idregulations_buyer NOT IN ( $notIn )";


	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		GROUP BY regulations_buyer.idregulations_buyer
		ORDER BY regulations_buyer.idregulations_buyer ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idregulations_buyer" );
			$record_type		= "regulations_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "credit";
			$designation		= "Paiement " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "payment_date" );
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//512 - Banque

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 6, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idregulations_buyer" );
			$record_type		= "regulations_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			$designation		= Util::doNothing("Règlement banque ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "payment_date" );
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerFactorRegulations(){

	$action = "customer_factor_regulations";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	regulations_buyer.idregulations_buyer,
	regulations_buyer.payment_date,
	regulations_buyer.amount,
	regulations_buyer.idpayment,
	regulations_buyer.idbuyer,
	billing_buyer.no_tax_export,
	`order`.iddelivery,
	payment.name$lang AS payment";

	$tables = "billing_buyer, billing_regulations_buyer, `order`, payment, regulations_buyer";

	$where = "
	billing_buyer.idbilling_buyer = billing_regulations_buyer.idbilling_buyer
	AND billing_buyer.idorder = `order`.idorder
	AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer
	AND regulations_buyer.idpayment = payment.idpayment
	AND regulations_buyer.payment_date < '$today'
	AND regulations_buyer.payment_date != '0000-00-00'
	AND regulations_buyer.factor = 1
	AND regulations_buyer.idregulations_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		ORDER BY regulations_buyer.idregulations_buyer ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//467 - Factor

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 7 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idregulations_buyer" );
			$record_type		= "regulations_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "credit";
			$designation		= Util::doNothing("Règlement factor ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "payment_date" );
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//511 - Factor

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 4, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idregulations_buyer" );
			$record_type		= "regulations_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			$designation		= Util::doNothing("Règlement factor ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "BQ2";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerFactorRefusal(){

	$action = "customer_factor_refusal";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	billing_buyer.idbilling_buyer,
	billing_buyer.total_amount,
	billing_buyer.factor_refusal_date,
	billing_buyer.deliv_payment,
	billing_buyer.idbuyer,
	billing_buyer.no_tax_export,
	`order`.iddelivery,
	payment.name$lang AS payment";

	$tables = "
	billing_buyer, `order`, payment";

	$where = "
	billing_buyer.factor_refusal_date < '$today'
	AND billing_buyer.factor_refusal_date != '0000-00-00'
	AND billing_buyer.idorder = `order`.idorder
	AND billing_buyer.idpayment = payment.idpayment
	AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		GROUP BY billing_buyer.idbilling_buyer
		ORDER BY billing_buyer.idbilling_buyer ASC";

	//411 - Collectif client

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements factor à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "total_amount" ), 2 ) > 0 ){

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "factor_refusal_date" );
			$amount				= round( $rs->fields( "total_amount" ), 2 );
			$amount_type		= "debit";
			$designation		= Util::doNothing("Facture impayée ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "BQ2";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//511 - Factor

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 4 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "factor_refusal_date" );
			$amount				+= round( $rs->fields( "total_amount" ), 2 );
			$amount_type		= "credit";
			$designation		= Util::doNothing("Client impayé");
			$code_eu			= "";
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "BQ2";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

/*
 * Factures définancées puis payées au factor
 * Ces factures sont donc refinancées
 */

function generateCustomerPaidFactorRefused(){

	$action = "customer_paid_factor_refused";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	billing_buyer.idbilling_buyer,
	billing_buyer.idbuyer,
	billing_buyer.no_tax_export,
	`order`.iddelivery, 
	billing_regulations_buyer.amount,
	regulations_buyer.payment_date";

	$tables = "
	billing_buyer, billing_regulations_buyer, `order`, regulations_buyer";

	$where = "
	billing_buyer.factor_send_date < '$today'
	AND billing_buyer.factor_send_date != '0000-00-00'
	AND billing_buyer.factor_refusal_date != '0000-00-00'
	AND billing_buyer.idbilling_buyer = billing_regulations_buyer.idbilling_buyer
	AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer
	AND billing_buyer.idorder = `order`.idorder
	AND regulations_buyer.payment_date != '0000-00-00'
	AND regulations_buyer.factor = 1
	AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		ORDER BY billing_buyer.idbilling_buyer ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements factor à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "credit";
			$designation		= "paiement factor";
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "FACT";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//511 - Factor

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 4 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= "";
			$record_type		= "";
			$date				= $rs->fields( "payment_date" );
			$amount				+= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Factor CMDP";
			$code_eu			= "";
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "FACT";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerCredits(){

	global $GLOBAL_START_PATH;

	include_once( "$GLOBAL_START_PATH/objects/Credit.php" );

	$action = "customer_credits";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	DISTINCT( credits.idcredit ),
	credits.idbuyer,
	credits.idbilling_buyer,
	credits.DateHeure AS credit_creation_date,
	billing_buyer.DateHeure AS invoice_creation_date,
	billing_buyer.deliv_payment,
	billing_buyer.no_tax_export,
	`order`.iddelivery";

	$tables = "billing_buyer, credits, credit_rows, `order`, buyer";

	$where = "
	credits.DateHeure < '$today'
	AND credits.idbuyer = buyer.idbuyer
	AND credits.editable = 0
	AND credits.idcredit = credit_rows.idcredit
	AND credits.idbilling_buyer = billing_buyer.idbilling_buyer
	AND billing_buyer.idorder = `order`.idorder
	AND credits.idcredit NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		GROUP BY credit_rows.idcredit
		ORDER BY credits.idcredit";
	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des avoirs à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		$credit = new Credit( $rs->fields( "idcredit" ) );

		if( round( $credit->getTotalATI(), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idcredit" );
			$record_type		= "credits";
			$date				= $rs->fields( "credit_creation_date" );
			$amount				= round( $credit->getTotalATI(), 2 );
			$amount_type		= "credit";
			$designation		= "Avoir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		if( round( $credit->getTotalET(), 2 ) > 0 ){

			//707 - Vente
			//70710000
			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 10, $rs->fields( "no_tax_export" ),1 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idcredit" );
			$record_type		= "credits";
			$date				= $rs->fields( "credit_creation_date" );
			$amount				= round( $credit->getTotalET(), 2 );
			$amount_type		= "debit";
			$designation		= "Avoir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		if( round( $credit->getTotalATI() - $credit->getTotalET(), 2 ) > 0 ){

			//445 - TVA
			
			//44571190
			if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 3,$rs->fields( "no_tax_export" ),1  );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idcredit" );
				$record_type		= "credits";
				$date				= $rs->fields( "credit_creation_date" );
				$amount				= round( $credit->getTotalATI() - $credit->getTotalET(), 2 );
				$amount_type		= "debit";
				$designation		= Util::doNothing("TVA déductible avoir ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerUsedCredits(){

	$action = "customer_used_credits";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	billing_buyer.idbilling_buyer,
	billing_buyer.credit_amount,
	billing_buyer.DateHeure,
	billing_buyer.idbuyer,
	billing_buyer.no_tax_export,
	`order`.iddelivery";

	$tables = "
	billing_buyer, `order`";

	$where = "
	billing_buyer.DateHeure < '$today'
	AND billing_buyer.idorder = `order`.idorder
	AND billing_buyer.credit_amount > 0
	AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		ORDER BY billing_buyer.idbilling_buyer";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "credit_amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= round( $rs->fields( "credit_amount" ), 2 );
			$amount_type		= "credit";
			$designation		= "Avoir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$amount = round( round( $rs->fields( "credit_amount" ), 2 ) / ( 1 + DBUtil::getParameter( "vatdefault" ) / 100 ), 2 );

		if( $amount > 0 ){

			//707 - Vente

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 10, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "DateHeure" );
			$amount				= $amount;
			$amount_type		= "debit";
			$designation		= "Avoir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		if( round( $rs->fields( "credit_amount" ), 2 ) - $amount > 0 ){

			//445 - TVA

			if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 3 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "DateHeure" );
				$amount				= round( $rs->fields( "credit_amount" ), 2 ) - $amount;
				$amount_type		= "debit";
				$designation		= "TVA avoir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerRebates(){

	/*
	 * Exemple facture client
	 * 		HT :		100 ¤
	 * 		TTC :		119.6 ¤
	 * 		Escompte :	10 ¤
	 *
	 *          411
	 * ____________________
	 *    119.6  |			//generateCustomerInvoices()
	 *           |  109.6	//generateCustomerRegulations()
	 *           |   10		//generateCustomerRebates()
	 *
	 *          512
	 * ____________________
	 *    109.6  |			//generateCustomerRegulations()
	 *     10    |			//generateCustomerRebates()
	 *           |   10		//generateCustomerRebates()
	 *
	 *          707
	 * ____________________
	 *           |   100	//generateCustomerInvoices()
	 *
	 *         4456
	 * ____________________
	 *           |  19.6	//generateCustomerInvoices()
	 *    1.96   |			//generateCustomerRebates()		//Si étranger => 0
	 *
	 *          665
	 * ____________________
	 *    8.04   |			//generateCustomerRebates()		//Si étranger => 10
	 *
	 * */

	$action = "customer_rebates";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	billing_buyer.idbilling_buyer,
	billing_buyer.total_amount,
	billing_buyer.idbuyer,
	billing_buyer.no_tax_export,
	`order`.iddelivery,
	billing_regulations_buyer.rebate_amount,
	regulations_buyer.payment_date";

	$tables = "
	`order`,
	billing_buyer,
	billing_regulations_buyer,
	regulations_buyer";

	$where = "
	billing_buyer.idbilling_buyer = billing_regulations_buyer.idbilling_buyer
	AND billing_regulations_buyer.idregulations_buyer = regulations_buyer.idregulations_buyer 
	AND billing_buyer.idorder = `order`.idorder
	AND billing_regulations_buyer.rebate_amount > 0
	AND regulations_buyer.payment_date < '$today'
	AND regulations_buyer.payment_date != 0
	AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where 
		ORDER BY billing_buyer.idbilling_buyer ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "rebate_amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= $rs->fields( "rebate_amount" );
			$amount_type		= "credit";
			$designation		= "Escompte TTC " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//512 - Banque

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 6 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "billing_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= $rs->fields( "rebate_amount" );
			$amount_type		= "credit";
			$designation		= "Escompte TTC " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "VTE";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//512 - Banque

			$amount_type		= "debit";
			$account_plan_3		= "D";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//4456 - TVA escompte	//$rebate - $rebate / ( 1 + 0.196 ) si France 0 sinon
			//665  - Escompte HT	//$rebate / ( 1 + 0.196 ) si France $rebate sinon

			if( AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){

				//4456 - TVA escompte

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 3 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "payment_date" );
				$amount				= round( $rs->fields( "rebate_amount" ) - $rs->fields( "rebate_amount" ) / ( 1 + DBUtil::getParameter( "vatdefault" ) / 100 ), 2 );
				$amount_type		= "debit";
				$designation		= "TVA escompte " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

				//665 - Escompte HT

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 9 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "payment_date" );
				$amount				= round( $rs->fields( "rebate_amount" ) / ( 1 + DBUtil::getParameter( "vatdefault" ) / 100 ), 2 );
				$amount_type		= "debit";
				$designation		= "Escompte HT " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "N";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}else{

				//665 - Escompte HT

				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 9 );
				$idrelation			= $rs->fields( "idbuyer" );
				$relation_type		= "buyer";
				$idaccount_record	= $rs->fields( "idbilling_buyer" );
				$record_type		= "billing_buyer";
				$date				= $rs->fields( "payment_date" );
				$amount				= $rs->fields( "rebate_amount" );
				$amount_type		= "debit";
				$designation		= "Escompte " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "VTE";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "N";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerRepayments(){

	$action = "customer_repayments";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	billing_buyer.idbilling_buyer,
	billing_buyer.idbuyer,
	billing_buyer.no_tax_export,
	repayments.amount,
	repayments.payment_date,
	`order`.iddelivery";

	$tables = "billing_buyer, credits, `order`, repayments";

	$where = "
	billing_buyer.DateHeure < '$today'
	AND billing_buyer.idbilling_buyer = credits.idbilling_buyer
	AND billing_buyer.idorder = `order`.idorder
	AND credits.idcredit = repayments.idcredit
	AND repayments.payment_date != '0000-00-00'
	AND billing_buyer.idbilling_buyer NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		ORDER BY billing_buyer.idbilling_buyer";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 2, $rs->fields( "no_tax_export" ) );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "repayment_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Remboursement " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//512 - Banque

			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 6 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= $rs->fields( "idbilling_buyer" );
			$record_type		= "repayment_buyer";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "credit";
			$designation		= "Remboursement " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateCustomerAdjustments( $query = false ){

	$action = "customer_adjustments";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	if( $query === false ){

		$notIn =& AccountPlan::getNotIn( $action );

		$select = "
		regulations_buyer.idregulations_buyer,
		regulations_buyer.payment_date,
		regulations_buyer.idbuyer,
		regulations_buyer.overpayment AS amount,
		state.code_eu";

		$tables = "buyer, regulations_buyer, state";

		$where = "
		regulations_buyer.idbuyer = buyer.idbuyer
		AND buyer.idstate = state.idstate
		AND regulations_buyer.overpayment != 0
		AND regulations_buyer.payment_date < '$today'
		AND regulations_buyer.payment_date != '0000-00-00'
		AND regulations_buyer.factor = 0
		AND regulations_buyer.idregulations_buyer NOT IN( $notIn )";

		$query = "
			SELECT $select
			FROM $tables
			WHERE $where
			ORDER BY regulations_buyer.payment_date";

	}

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des ajustements à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( abs( round( $rs->fields( "amount" ), 2 ) ) > 0 ){

			//411 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 2 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= isset( $rs->fields[ "idregulations_buyer" ] ) ? $rs->fields( "idregulations_buyer" ) : "";
			$record_type		= isset( $rs->fields[ "idregulations_buyer" ] ) ? "regulations_buyer" : "";
			$date				= isset( $rs->fields[ "payment_date" ] ) ? $rs->fields( "payment_date" ) : date( "Y-m-d" );
			$amount				= abs( $rs->fields( "amount" ) );
			$amount_type		= $rs->fields( "amount" ) > 0 ? "debit" : "credit";
			$designation		= Util::doNothing("Ajustement écart ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= $rs->fields( "amount" ) > 0 ? "D" : "C";
			$account_plan_4		= "N";
			$lettering			= isset( $_GET[ "lettering" ] ) ? $_GET[ "lettering" ] : "";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

			//65800000 - Ajustement écarts (écart > 0)
			//75800000 - Ajustement écarts (écart < 0)

			$idaccount_plan		= $rs->fields( "amount" ) > 0 ? AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 25 ) : AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 24 );
			$idrelation			= $rs->fields( "idbuyer" );
			$relation_type		= "buyer";
			$idaccount_record	= isset( $rs->fields[ "idregulations_buyer" ] ) ? $rs->fields( "idregulations_buyer" ) : "";
			$record_type		= isset( $rs->fields[ "idregulations_buyer" ] ) ? "regulations_buyer" : "";
			$date				= isset( $rs->fields[ "payment_date" ] ) ? $rs->fields( "payment_date" ) : date( "Y-m-d" );
			$amount				= abs( $rs->fields( "amount" ) );
			$amount_type		= $rs->fields( "amount" ) > 0 ? "credit" : "debit";
			$designation		= Util::doNothing("Ajustement écart client ") . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$code_eu			= "";
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= $rs->fields( "amount" ) > 0 ? "C" : "D";
			$account_plan_4		= "N";
			$lettering			= "";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------

function generateSupplierInvoices(){

	$action = "supplier_invoices";

	$notIn =& AccountPlan::getNotInCouple( $action );

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$select = "
	billing_supplier.billing_supplier,
	billing_supplier.total_amount,
	billing_supplier.total_amount_ht,
	billing_supplier.total_charge_ht,
	billing_supplier.Date,
	billing_supplier.payment_date,
	billing_supplier.idsupplier,
	payment.name$lang AS payment,
	supplier.name,
	`order`.idbuyer,
	`order`.iddelivery,
	supplier.idaccounting_plan,
	supplier.idsupplier,
	state.code_eu";

	$tables = "
	billing_supplier, billing_supplier_row, bl_delivery, `order`, payment, state, supplier";

 /*   $where = "
	billing_supplier.Date < '$today'
	AND billing_supplier.editable = 0
	AND billing_supplier.proforma = 0
	AND billing_supplier.idsupplier = supplier.idsupplier
	AND billing_supplier.billing_supplier = billing_supplier_row.billing_supplier
	AND billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery
	AND billing_supplier.idpayment = payment.idpayment
	AND bl_delivery.idorder = `order`.idorder
	AND state.idstate = supplier.idstate
	AND ( billing_supplier.billing_supplier, billing_supplier.idsupplier ) NOT IN( $notIn )";
*/


	$where = "
	billing_supplier.Date < '$today'
	AND billing_supplier.editable = 0
	AND billing_supplier.proforma = 0
	AND billing_supplier.idsupplier = supplier.idsupplier
	AND billing_supplier.billing_supplier = billing_supplier_row.billing_supplier
	AND billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery
	AND billing_supplier.idpayment = payment.idpayment
	AND state.idstate = supplier.idstate
	AND ( billing_supplier.billing_supplier, billing_supplier.idsupplier ) NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		GROUP BY billing_supplier_row.billing_supplier
		ORDER BY billing_supplier.billing_supplier";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures fournisseurs à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "total_amount" ), 2 ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 1 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "Date" );
			$amount				= round( $rs->fields( "total_amount" ), 2 );
			$amount_type		= "credit";
			$designation		= "Facture " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "payment_date" );
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}
	
		if( round( $rs->fields( "total_amount_ht" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 ) > 0 ){

			//607 - Achat marchandises
			
			$idaccount_plan		= $rs->fields( "idaccounting_plan" ) ? $rs->fields( "idaccounting_plan" ) : AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 8 );
			//echo "idsupplier: " . $rs->fields( "idsupplier" ) . ", idaccounting_plan: $idaccounting_plan, idaccount_plan: $idaccount_plan<br />";
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "Date" );
			$amount				= round( $rs->fields( "total_amount_ht" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 );
			$amount_type		= "debit";
			$designation		= "Achat facture " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		if( round( $rs->fields( "total_charge_ht" ), 2 ) > 0 ){

			//624 - Acaht port HT

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 14 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "Date" );
			$amount				= round( $rs->fields( "total_charge_ht" ), 2 );
			$amount_type		= "debit";
			$designation		= "Achat port facture " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		//445 - TVA
		if( $amount > 0 && $rs->fields( "code_eu" ) == "NATIONAL" ){

			//44566190 - TVA collectée France

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 5 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "Date" );
			$amount				= round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 );
			$amount_type		= "debit";
			$designation		= Util::doNothing("TVA facture déductible France ") . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}


		//Calcul du montant de la TVA
		//$amount = round( DBUtil::getParameter( "vatdefault" ) / ( 100 + DBUtil::getParameter( "vatdefault" ) ) * round( $rs->fields( "total_amount" ), 2 ), 2 );
		 $amount = round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_charge_ht" ), 2 );

		if( $amount > 0 && $rs->fields( "code_eu" ) == "INTRA" ){

			//44527900 - TVA collectée CEE

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 16 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "Date" );
			$amount				= $amount;
			$amount_type		= "debit";
			$designation		= Util::doNothing("TVA facture déduite CEE ") . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//44526900 - TVA déductible CEE

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 17 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "Date" );
			$amount				= $amount;
			$amount_type		= "credit";
			$designation		= Util::doNothing("TVA facture collectée CEE ") . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateSupplierRegulations(){

	$action = "supplier_regulations";

	$notIn =& AccountPlan::getNotIn( $action );

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$select = "
	DISTINCT( regulations_supplier.idregulations_supplier ),
	regulations_supplier.payment_date,
	regulations_supplier.amount,
	supplier.idsupplier,
	supplier.name,
	bl_delivery.iddelivery,
	bl_delivery.idbuyer,
	payment.name$lang AS payment,
	state.code_eu";

	$tables = "
	payment,
	bl_delivery,
	billing_supplier_row,
	state,
	supplier,
	regulations_supplier
	LEFT JOIN billing_regulations_supplier ON regulations_supplier.idregulations_supplier = billing_regulations_supplier.idregulations_supplier
	LEFT JOIN billing_supplier ON billing_regulations_supplier.billing_supplier = billing_supplier.billing_supplier";

	$where = "
	regulations_supplier.payment_date < '$today'
	AND regulations_supplier.payment_date != '0000-00-00'
	AND billing_supplier.editable = 0
	AND billing_supplier.billing_supplier = billing_supplier_row.billing_supplier
	AND billing_supplier.idsupplier = billing_supplier_row.idsupplier
	AND billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery
	AND regulations_supplier.idpayment = payment.idpayment
	AND supplier.idsupplier = billing_supplier.idsupplier
	AND state.idstate = supplier.idstate
	AND regulations_supplier.idregulations_supplier NOT IN( $notIn )";


	/*
	$select = "
	DISTINCT( regulations_supplier.idregulations_supplier ),
	regulations_supplier.payment_date,
	regulations_supplier.amount,
	supplier.idsupplier,
	supplier.name,
	bl_delivery.iddelivery,
	bl_delivery.idbuyer,
	payment.name$lang AS payment,
	state.code_eu";

	$tables = "
	payment,
	bl_delivery,
	state,
	supplier,
	regulations_supplier";

	$where = "
	regulations_supplier.payment_date < '$today'
	AND regulations_supplier.payment_date != '0000-00-00'
	AND regulations_supplier.idpayment = payment.idpayment
	AND state.idstate = supplier.idstate
	AND regulations_supplier.idregulations_supplier NOT IN( $notIn )";
*/

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		GROUP BY regulations_supplier.idregulations_supplier
		ORDER BY regulations_supplier.idregulations_supplier ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements fournisseurs à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 1 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "idregulations_supplier" );
			$record_type		= "regulations_supplier";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Paiement facture " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= $rs->fields( "payment_date" );
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//512 - Banque

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 6 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "idregulations_supplier" );
			$record_type		= "regulations_supplier";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "credit";
			$designation		= Util::doNothing("Règlement facture ") . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= $rs->fields( "payment" );
			$payment_date		= "";
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateSupplierCredits(){

	global $GLOBAL_START_PATH;

	$action = "supplier_credits";

	$notIn =& AccountPlan::getNotInCouple( $action );

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$select = "
	credit_supplier.idcredit_supplier,
	credit_supplier.idsupplier,
	credit_supplier.amount,
	credit_supplier.Date AS credit_creation_date,
	billing_supplier.Date AS invoice_creation_date,
	billing_supplier.deliv_payment,
	billing_supplier.billing_supplier,
	state.code_eu,
	supplier.name";

	$tables = "billing_supplier, credit_supplier, state, supplier";

	$where = "
	credit_supplier.Date < '$today'
	AND billing_supplier.editable = 0
	AND billing_supplier.billing_supplier = credit_supplier.billing_supplier
	AND credit_supplier.idsupplier = supplier.idsupplier
	AND supplier.idstate = state.idstate
	AND ( credit_supplier.idcredit_supplier, credit_supplier.idsupplier ) NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		ORDER BY credit_supplier.idcredit_supplier";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des avoirs fournisseurs à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 1 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "idcredit_supplier" );
			$record_type		= "credit_supplier";
			$date				= $rs->fields( "credit_creation_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Avoir " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//607 - Achat

			//Si France, on déduit la TVA des achats
			//Sinon, le montant des achats est le montant de l'avoir
			if( $rs->fields( "code_eu" ) == "NATIONAL" )
				$amount = round( round( $rs->fields( "amount" ), 2 ) / ( 1 + DBUtil::getParameter( "vatdefault" ) / 100 ), 2 );
			else
				$amount = round( $rs->fields( "amount" ), 2 );

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 8 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "idcredit_supplier" );
			$record_type		= "credit_supplier";
			$date				= $rs->fields( "credit_creation_date" );
			$amount				= $amount;
			$amount_type		= "credit";
			$designation		= "Achat avoir " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//445 - TVA

			//Calcul du montant de la TVA
			$amount = round( DBUtil::getParameter( "vatdefault" ) / ( 100 + DBUtil::getParameter( "vatdefault" ) ) * round( $rs->fields( "amount" ), 2 ), 2 );

			//44566190 - France
			if( $rs->fields( "code_eu" ) == "NATIONAL" ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 5 );
				$idrelation			= $rs->fields( "idsupplier" );
				$relation_type		= "supplier";
				$idaccount_record	= $rs->fields( "idcredit_supplier" );
				$record_type		= "credit_supplier";
				$date				= $rs->fields( "credit_creation_date" );
				$amount				= $amount;
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA collectée France - Avoir ") . $rs->fields( "name" );
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "ACH";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

			//44527900 & 44526900 - Intra
			if( $rs->fields( "code_eu" ) == "INTRA" ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 16 );
				$idrelation			= $rs->fields( "idsupplier" );
				$relation_type		= "supplier";
				$idaccount_record	= $rs->fields( "idcredit_supplier" );
				$record_type		= "credit_supplier";
				$date				= $rs->fields( "credit_creation_date" );
				$amount				= $amount;
				$amount_type		= "debit";
				$designation		= Util::doNothing("TVA collectée CEE - Avoir ") . $rs->fields( "name" );
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "ACH";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 17 );
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA déduite CEE - Avoir ") . $rs->fields( "name" );
				$account_plan_3		= "C";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateSupplierRebates(){

	global $GLOBAL_START_PATH;

	$action = "supplier_rebates";

	$notIn =& AccountPlan::getNotInCouple( $action );

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$select = "
	billing_supplier.billing_supplier,
	billing_supplier.idsupplier,
	billing_supplier.deliv_payment,
	billing_supplier.billing_supplier,
	billing_regulations_supplier.rebate_amount,
	regulations_supplier.payment_date,
	state.code_eu,
	supplier.name";

	$tables = "
	billing_supplier,
	billing_regulations_supplier,
	regulations_supplier,
	state,
	supplier";

	$where = "
	billing_supplier.idsupplier = supplier.idsupplier
	AND billing_supplier.billing_supplier = billing_regulations_supplier.billing_supplier
	AND billing_regulations_supplier.idregulations_supplier = regulations_supplier.idregulations_supplier 
	AND supplier.idstate = state.idstate
	AND billing_supplier.billing_date < '$today'
	AND billing_supplier.editable = 0
	AND billing_regulations_supplier.rebate_amount > 0
	AND regulations_supplier.payment_date < '$today'
	AND regulations_supplier.payment_date != 0
	AND ( billing_supplier.billing_supplier, billing_supplier.idsupplier ) NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		ORDER BY idsupplier ASC, billing_supplier.billing_supplier ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des escomptes fournisseurs à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "rebate_amount" ), 2 ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 1 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "rebate_amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Escompte TTC " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//765 - Escompte

			//Si France, on déduit la TVA des achats
			//Sinon, le montant des achats est le montant de l'escomtpe
			if( $rs->fields( "code_eu" ) == "NATIONAL" )
				$amount = round( round( $rs->fields( "rebate_amount" ), 2 ) / ( 1 + DBUtil::getParameter( "vatdefault" ) / 100 ), 2 );
			else
				$amount = round( $rs->fields( "rebate_amount" ), 2 );

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 13 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "billing_supplier" );
			$record_type		= "billing_supplier";
			$date				= $rs->fields( "payment_date" );
			$amount				= $amount;
			$amount_type		= "credit";
			$designation		= "Escompte HT " . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//445 - TVA

			//Calcul du montant de la TVA
			$amount = round( DBUtil::getParameter( "vatdefault" ) / ( 100 + DBUtil::getParameter( "vatdefault" ) ) * round( $rs->fields( "rebate_amount" ), 2 ), 2 );

			//44571190 - France
			if( $rs->fields( "code_eu" ) == "NATIONAL" ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 3 );
				$idrelation			= $rs->fields( "idsupplier" );
				$relation_type		= "supplier";
				$idaccount_record	= $rs->fields( "billing_supplier" );
				$record_type		= "billing_supplier";
				$date				= $rs->fields( "payment_date" );
				$amount				= $amount;
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA déductible France - Escompte ") . $rs->fields( "name" );
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "OD";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

			//44527900 & 44526900 - Intra
			if( $rs->fields( "code_eu" ) == "INTRA" ){

				//44527900

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 16 );
				$idrelation			= $rs->fields( "idsupplier" );
				$relation_type		= "supplier";
				$idaccount_record	= $rs->fields( "billing_supplier" );
				$record_type		= "billing_supplier";
				$date				= $rs->fields( "payment_date" );
				$amount				= $amount;
				$amount_type		= "debit";
				$designation		= Util::doNothing("TVA collectée CEE - Escompte ") . $rs->fields( "name" );
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "OD";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

				//44526900

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 17 );
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA déduite CEE - Escomtpe ") . $rs->fields( "name" );
				$account_plan_3		= "C";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateSupplierAdjustments(){

	$action = "supplier_adjustments";

	$today = date( "Y-m-d" );

	$lang = User::getInstance()->getLang();

	$notIn =& AccountPlan::getNotIn( $action );

	$select = "
	regulations_supplier.idregulations_supplier,
	regulations_supplier.payment_date,
	regulations_supplier.idsupplier,
	regulations_supplier.overpayment AS amount,
	supplier.name,
	state.code_eu";

	$tables = "supplier, regulations_supplier, state";

	$where = "
	regulations_supplier.idsupplier = supplier.idsupplier
	AND supplier.idstate = state.idstate
	AND regulations_supplier.overpayment != 0
	AND regulations_supplier.payment_date < '$today'
	AND regulations_supplier.payment_date != '0000-00-00'
	AND regulations_supplier.idregulations_supplier NOT IN( $notIn )";

	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		ORDER BY regulations_supplier.payment_date";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des ajustements à traîter", E_USER_ERROR );

	while( !$rs->EOF() ){

		if( abs( round( $rs->fields( "amount" ), 2 ) ) > 0 ){

			//401 - Collectif client

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 1 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "idregulations_supplier" );
			$record_type		= "regulations_supplier";
			$date				= $rs->fields( "payment_date" );
			$amount				= abs( $rs->fields( "amount" ) );
			$amount_type		= $rs->fields( "amount" ) < 0 ? "debit" : "credit";
			$designation		= Util::doNothing("Ajustement écart ") . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= $rs->fields( "amount" ) < 0 ? "D" : "C";
			$account_plan_4		= "N";
			$lettering			= "";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

			//65800000 - Ajustement écarts (écart > 0)
			//75800000 - Ajustement écarts (écart < 0)

			$idaccount_plan		= $rs->fields( "amount" ) < 0 ? AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 25 ) : AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 24 );
			$idrelation			= $rs->fields( "idsupplier" );
			$relation_type		= "supplier";
			$idaccount_record	= $rs->fields( "idregulations_supplier" );
			$record_type		= "regulations_supplier";
			$date				= $rs->fields( "payment_date" );
			$amount				= abs( $rs->fields( "amount" ) );
			$amount_type		= $rs->fields( "amount" ) < 0 ? "credit" : "debit";
			$designation		= Util::doNothing("Ajustement écart fournisseur ") . $rs->fields( "name" );
			$code_eu			= "";
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= $rs->fields( "amount" ) < 0 ? "C" : "D";
			$account_plan_4		= "N";
			$lettering			= "";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function getExportableArray( $req ){

	global	$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;

	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );

	$query = Encryptor::decrypt( URLFactory::base64url_decode( $req ), $GLOBAL_DB_PASS );

	$rs =& DBUtil::query( "SELECT * FROM account_plan_export ORDER BY display_order ASC" );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des colonnes à exporter", E_USER_ERROR );

	$data = array();

	while( !$rs->EOF ){

		if( $rs->fields( "displayed" ) == 1 )
			$data[ $rs->fields( "field" ) ] = array();

		$rs->MoveNext();

	}

	$rs =& DBUtil::query( $query );


	while( !$rs->EOF()){

		foreach( $data as $key => $value ){

			switch( $key ){
				case "date":
					$data[ "date" ][] = Util::dateFormatEu( $rs->fields( "date" ), "d-m-Y" );
					break;
				case "idrelation":
					$data[ "idrelation" ][] = substr( $rs->fields( "idaccount_plan" ), 0, 3 ) == "411" ? $rs->fields( "idrelation" ) : ""; //On n'exporte le numéro de client seulement pour les comptes 411
					break;
				case "payment_date":
					$data[ "payment_date" ][] = $rs->fields( "payment_date" ) == "0000-00-00" ? "" : Util::dateFormatEu( $rs->fields( "payment_date" ), "d-m-Y" );
					break;
				default:
					$data[ $key ][] = $rs->fields( $key );
					break;
			}

		}

		$rs->MoveNext();

	}
	return $data;
	

}

//--------------------------------------------------------------------------------

function exportArray( &$exportableArray ){

	global $GLOBAL_START_PATH;

	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );

	$filename = "plan_comptable_";
	$filename .= date( "Y-m-d_H:i:s" );

	$cvsExportArray = new CSVExportArray( $exportableArray, $filename, "csv", ";", "", "\r\n" );
	$cvsExportArray->hideHeaders();
	$cvsExportArray->writeOuputFile( "/data/account_plan/" );
	$cvsExportArray->export();

}

//--------------------------------------------------------------------------------

function accountsSelect( $withAll = false, $id = false, $value = false ){

?>
    <select <?php echo $id === false ? "name=\"idaccount_plan\"" : "name=\"$id\" id=\"$id\"" ?> class="fullWidth">
    	<?php if( $withAll ){ ?><option value="all">Tous</option><?php } ?>
		<?php

		$lang = User::getInstance()->getLang();

		$rs =& DBUtil::query( "SELECT idaccounting_plan, accounting_plan$lang AS description, accounting_plan_parent FROM accounting_plan ORDER BY idaccounting_plan ASC" );

		while( !$rs->EOF() ){

			$bold = $rs->fields( "accounting_plan_parent" ) == 0 ? " style=\"font-weight:bold;\"" : "";
			$selected = $value == false || $value != $rs->fields( "idaccounting_plan" ) ? "" : ' selected="selected"';

			echo "    <option value=\"" . $rs->fields( "idaccounting_plan" ) . "\"$bold$selected>" . $rs->fields( "idaccounting_plan" ) . " - " . $rs->fields( "description" ) . "</option>\n";

			$rs->MoveNext();

		}

		?>
	</select>
<?php

}

//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------

/**
 * Récupère la liste des factures associées à une liste de règlements
 * @param array int $regulations la liste des règlements à partir desquels on cherche les factures
 * @param array int &$invoices la référence vers le tableau dans lequel sont enregistrés les numéros de factures
 * @return int $new le nombre de factures ajoutées au tableau
 */
function getCustomerInvoicesFromRegulations( $regulations, $invoices ){

	$new = 0;

	$rs =& DBUtil::query( "SELECT idbilling_buyer FROM billing_regulations_buyer WHERE idregulations_buyer IN( " . implode( ", ", $regulations ) . " )" );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures associées aux règlements", E_USER_ERROR );

	while( !$rs->EOF ){

		if( !in_array( $rs->fields( "idbilling_buyer" ), $invoices ) ){

			$invoices[] = $rs->fields( "idbilling_buyer" );
			$new++;

		}

		$rs->MoveNext();

	}

	return $new;


}

/**
 * Effectue le lettrage automatique des comptes
 * Le lettrage consiste à affecté un code (composé de lettres en général) à un ensemble de règlements/factures liés pour un compte tiers donné
 * On part des règlements puis on remonte aux factures, jusqu'à toutes les factures et règlements liés soient lettrés
 * @param void
 * @return void
 */

function customerLettering(){

	//Sélection de tous les règlements client pas encore lettrés

	$query = "
		SELECT idaccount_record,
			idrelation,
			date
		FROM account_plan
		WHERE record_type = 'regulations_buyer'
			AND editable = 1
			AND lettering = ''
		GROUP BY idaccount_record
		ORDER BY date ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements non lettrés", E_USER_ERROR );

	//Pour chacun de ces règlements, on recherche les factures associées et les règlements associés via d'autres factures

	while( !$rs->EOF ){

		$invoices = array();
		$regulations = array( $rs->fields( "idaccount_record" ) );

		while( getCustomerInvoicesFromRegulations( $regulations, $invoices ) )
			getCustomerRegulationsFromInvoices( $invoices, $regulations );

		//A ce moment, on a l'ensemble des factures et règlements à regrouper dans les tableaux $invoices et $regulations

		//On vérifie le lettrage des pièces trouvées
		//Si elles ne sont pas lettrées, on prend le lettrage suivant
		//Si elles le sont et qu'elles ont toutes le même lettrage, on prend ce dernier
		//Si elles sont lettrées mais avec des lettrages différents, on passe notre route

		$query = "
			SELECT DISTINCT( lettering ) AS lettering
			FROM account_plan
			WHERE lettering != ''
				AND idaccount_plan LIKE '411%'
				AND (
					   record_type = 'regulations_buyer' AND idaccount_record IN( '" . implode( "', '", $regulations ) . "' )
					OR record_type = 'billing_buyer' AND idaccount_record IN( '" . implode( "', '", $invoices ) . "' )
				)";

		$rsLetters =& DBUtil::query( $query );

		if( !$rsLetters->RecordCount() )
			$letter = AccountPlan::getNextLettering( $rs->fields( "idrelation" ), $rs->fields( "date" ) );
		elseif( $rsLetters->RecordCount() == 1 )
			$letter = $rsLetters->fields( "lettering" );

		if( $rsLetters->RecordCount() <= 1 ){

			$query = "
				UPDATE account_plan
				SET lettering = '$letter'
				WHERE idrelation = '" . $rs->fields( "idrelation" ) . "'
					AND idaccount_plan LIKE '411%'
					AND (
						   ( record_type = 'regulations_buyer' AND idaccount_record IN( '" . implode( "', '", $regulations ) . "' ) )
						OR ( record_type = 'billing_buyer' AND idaccount_record IN( '" . implode( "', '", $invoices ) . "' ) )
					)";

			$rsUpdate =& DBUtil::query( $query );

			if( $rsUpdate === false )
				trigger_error( "Impossible de mettre à jour le lettrage des comptes", E_USER_ERROR );

		}

		$rs->MoveNext();

	}

}


//--------------------------------------------------------------------------------

/**
 * Récupère la liste des règlements associés à une liste de factures
 * @param array int $invoices la liste des factures à partir desquels on cherche les règlements
 * @param array int &$regulations la référence vers le tableau dans lequel sont enregistrés les numéros de règlements
 * @return int $new le nombre de règlements ajoutés au tableau
 */
function getCustomerRegulationsFromInvoices( $invoices, $regulations ){

	$new = 0;

	$rs =& DBUtil::query( "SELECT idregulations_buyer FROM billing_regulations_buyer WHERE idbilling_buyer IN( " . implode( ", ", $invoices ) . " )" );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements associés aux factures", E_USER_ERROR );

	while( !$rs->EOF ){

		if( !in_array( $rs->fields( "idregulations_buyer" ), $regulations ) ){

			$regulations[] = $rs->fields( "idregulations_buyer" );
			$new++;

		}

		$rs->MoveNext();

	}

	return $new;

}
//--------------------------------------------------------------------------------

/**
 * Récupère la liste des factures associées à une liste de règlements
 * @param array int $regulations la liste des règlements à partir desquels on cherche les factures
 * @param array int &$invoices la référence vers le tableau dans lequel sont enregistrés les numéros de factures
 * @return int $new le nombre de factures ajoutées au tableau
 */
function getSupplierInvoicesFromRegulations( $regulations, $invoices ){

	$new = 0;

	$rs =& DBUtil::query( "SELECT billing_supplier FROM billing_regulations_supplier WHERE idregulations_supplier IN( " . implode( ", ", $regulations ) . " )" );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des factures associées aux règlements", E_USER_ERROR );

	while( !$rs->EOF ){

		if( !in_array( $rs->fields( "billing_supplier" ), $invoices ) ){

			$invoices[] = $rs->fields( "billing_supplier" );
			$new++;

		}

		$rs->MoveNext();

	}

	return $new;


}
//--------------------------------------------------------------------------------

/**
 * Effectue le lettrage automatique des comptes
 * Le lettrage consiste à affecté un code (composé de lettres en général) à un ensemble de règlements/factures liés pour un compte tiers donné
 * On part des règlements puis on remonte aux factures, jusqu'à toutes les factures et règlements liés soient lettrés
 * @param void
 * @return void
 */

function supplierLettering(){

	//Sélection de tous les règlements fournisseur pas encore lettrés

	$query = "
		SELECT idaccount_record,
			idrelation
		FROM account_plan
		WHERE record_type = 'regulations_supplier'
			AND editable = 1
			AND lettering = ''
		GROUP BY idaccount_record
		ORDER BY date ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements non lettrés", E_USER_ERROR );

	//Pour chacun de ces règlements, on recherche les factures associées et les règlements associés via d'autres factures

	while( !$rs->EOF ){

		$invoices = array();
		$regulations = array( $rs->fields( "idaccount_record" ) );

		while( getSupplierInvoicesFromRegulations( $regulations, $invoices ) )
			getSupplierRegulationsFromInvoices( $invoices, $regulations, $rs->fields( "idrelation" ) );

		//A ce moment, on a l'ensemble des factures et règlements à regrouper dans les tableaux $invoices et $regulations

		//On vérifie le lettrage des pièces trouvées
		//Si elles ne sont pas lettrées, on prend le lettrage suivant
		//Si elles le sont et qu'elles ont toutes le même lettrage, on prend ce dernier
		//Si elles sont lettrées mais avec des lettrages différents, on passe notre route

		$query = "
			SELECT DISTINCT( lettering ) AS lettering
			FROM account_plan
			WHERE lettering != ''
				AND idaccount_plan LIKE '401%'
				AND (
					   record_type = 'regulations_supplier' AND idaccount_record IN( '" . implode( "', '", $regulations ) . "' )
					OR record_type = 'billing_supplier' AND idaccount_record IN( '" . implode( "', '", $invoices ) . "' )
				)";

		$rsLetters =& DBUtil::query( $query );

		if( !$rsLetters->RecordCount() )
			$letter = AccountPlan::getNextLettering( $rsLetters->fields( "idrelation" ), $rs->fields( "date" ), "supplier" );
		elseif( $rsLetters->RecordCount() == 1 )
			$letter = $rsLetters->fields( "lettering" );

		if( $rsLetters->RecordCount() <= 1 ){

			$query = "
				UPDATE account_plan
				SET lettering = '$letter'
				WHERE idrelation = '" . $rs->fields( "idrelation" ) . "'
					AND idaccount_plan LIKE '401%'
					AND (
						   ( record_type = 'regulations_supplier' AND idaccount_record IN( '" . implode( "', '", $regulations ) . "' ) )
						OR ( record_type = 'billing_supplier' AND idaccount_record IN( '" . implode( "', '", $invoices ) . "' ) )
					)";

			$rsUpdate =& DBUtil::query( $query );

			if( $rsUpdate === false )
				trigger_error( "Impossible de mettre à jour le lettrage des comptes", E_USER_ERROR );

		}

		$rs->MoveNext();

	}

}



//--------------------------------------------------------------------------------

/**
 * Récupère la liste des règlements associés à une liste de factures
 * @param array int $invoices la liste des factures à partir desquels on cherche les règlements
 * @param array int &$regulations la référence vers le tableau dans lequel sont enregistrés les numéros de règlements
 * @return int $new le nombre de règlements ajoutés au tableau
 */
function getSupplierRegulationsFromInvoices( $invoices, $regulations, $idsupplier ){

	$new = 0;

	$rs =& DBUtil::query( "SELECT idregulations_supplier FROM billing_regulations_supplier WHERE billing_supplier IN( '" . implode( "', '", $invoices ) . "' ) AND idsupplier = '$idsupplier'" );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des règlements associés aux factures", E_USER_ERROR );

	while( !$rs->EOF ){

		if( !in_array( $rs->fields( "idregulations_supplier" ), $regulations ) ){

			$regulations[] = $rs->fields( "idregulations_supplier" );
			$new++;

		}

		$rs->MoveNext();

	}

	return $new;


}

//--------------------------------------------------------------------------------

/**
 * Vérifie l'intégrité des données de la base avant de lancer la génération du plan comptable
 * @param void
 * @return mixed array la liste des erreurs rencontrées
 */
function checkIntegrity(){

	global	$start_date,
			$end_date;

	$integrity = array();

	//Vérification des données pour les numéros de client dans les factures
	$checkBuyer =& DBUtil::query( "SELECT idbilling_buyer, idbuyer FROM billing_buyer WHERE idbuyer NOT IN( SELECT idbuyer FROM buyer ) AND DateHeure > '$start_date' AND DateHeure < '$end_date'" );
	if( $checkBuyer->RecordCount() ){

		$integrity[ "buyer" ] = array();

		$i = 0;
		while( !$checkBuyer->EOF ){

			$integrity[ "buyer" ][ $i ][ "idbuyer" ] = $checkBuyer->fields( "idbuyer" );
			$integrity[ "buyer" ][ $i ][ "idbilling_buyer" ] = $checkBuyer->fields( "idbilling_buyer" );

			$i++;
			$checkBuyer->MoveNext();

		}

	}

	//Vérification de l'intégrité des données pour les numéros de commande
	$checkOrder =& DBUtil::query( "SELECT idbilling_buyer, idorder FROM billing_buyer WHERE idorder NOT IN( SELECT idorder FROM `order` ) AND DateHeure > '$start_date' AND DateHeure < '$end_date'" );
	if( $checkOrder->RecordCount() ){

		$integrity[ "order" ] = array();

		$i = 0;
		while( !$checkOrder->EOF ){

			$integrity[ "order" ][ $i ][ "idorder" ] = $checkBuyer->fields( "idorder" );
			$integrity[ "order" ][ $i ][ "idbilling_buyer" ] = $checkBuyer->fields( "idbilling_buyer" );

			$i++;
			$checkOrder->MoveNext();

		}

	}

	//Vérification de l'intégrité des données pour les modes de paiement
	$checkPayment =& DBUtil::query( "SELECT idbilling_buyer, idpayment FROM billing_buyer WHERE idpayment NOT IN( SELECT idpayment FROM payment ) AND DateHeure > '$start_date' AND DateHeure < '$end_date'" );
	if( $checkPayment->RecordCount() ){

		$integrity[ "payment" ] = array();

		$i++;
		while( !$checkPayment->EOF ){

			$integrity[ "payment" ][ $i ][ "idpayment" ] = $checkBuyer->fields( "idorder" );
			$integrity[ "payment" ][ $i ][ "idbilling_buyer" ] = $checkBuyer->fields( "idbilling_buyer" );

			$i++;
			$checkPayment->MoveNext();

		}

	}

	$message = false;

	if( count( $integrity ) ){

		foreach( $integrity as $type => $value ){

			switch( $type ){
				case "buyer":
					$message .= "Les clients associés aux factures suivantes n'existent pas.<br />\n";
					foreach( $value as $error )
						$message .= "Facture n°&nbsp;: " . $error[ "idbilling_buyer" ] . " - Client n°&nbsp;: " . $error[ "idbuyer" ] . "<br />\n";
					break;
				case "order":
					$message .= "Les commandes associées aux factures suivantes n'existent pas.<br />\n";
					foreach( $value as $error )
						$message .= "Facture n°&nbsp;: " . $error[ "idbilling_buyer" ] . " - Commande n°&nbsp;: " . $error[ "idorder" ] . "<br />\n";
					break;
				case "payment":
					$message .= "Les modes de paiement associés aux factures suivantes n'existent pas.<br />\n";
					foreach( $value as $error )
						$message .= "Facture n°&nbsp;: " . $error[ "idbilling_buyer" ] . " - Mode de paiement n°&nbsp;: " . $error[ "idpayment" ] . "<br />\n";
					break;
			}

		}

		return $message;

	}

	return $integrity;

}

//--------------------------------------------------------------------------------

function setNewLettering(){

	if( !isset( $_GET[ "value" ] ) || !isset( $_GET[ "idrow" ] ) || !intval( $_GET[ "idrow" ] ) )
		exit( "Paramètres erronés" );

	if( $_GET[ "value" ] == DBUtil::getDBValue( "lettering", "account_plan", "idrow", intval( $_GET[ "idrow" ] ) ) )
		exit( "1" );

$q1 = "SELECT idaccount_plan,idaccount_record,record_type FROM account_plan WHERE idrow = " . intval( $_GET[ "idrow" ] ) . " ";

	$rs1 =& DBUtil::query( $q1 );

$idrecord = $rs1 -> fields ("idaccount_record");
$record_type = $rs1 -> fields ("record_type");
$idaccount_plan = $rs1 -> fields ("idaccount_plan");
	
//------Lettrage provider
if ($record_type == "provider_invoice")
{
$query = "
			SELECT  idprovider_invoice,idregulations_provider
			FROM billing_regulations_provider
			WHERE idprovider_invoice = '$idrecord'
			";

	

	$rsr =& DBUtil::query( $query );


while( !$rsr->EOF() ){
 $idprovider_invoice = $rsr->fields( "idprovider_invoice" );
$iregulations_provider = $rsr->fields( "idregulations_provider" );



$q2 = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idaccount_record = '" . ( $iregulations_provider ) . "' AND idaccount_plan = $idaccount_plan ";

	$rs2 =& DBUtil::query( $q2 );

$rsr->MoveNext();
}
}

if ($record_type == "regulations_provider")
{

$query = "
			SELECT  idprovider_invoice,idregulations_provider
			FROM billing_regulations_provider
			WHERE idregulations_provider = '$idrecord'
			";

	

	$rsr =& DBUtil::query( $query );


while( !$rsr->EOF() ){
 $idprovider_invoice = $rsr->fields( "idprovider_invoice" );
$iregulations_provider = $rsr->fields( "idregulations_provider" );

$q2 = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idaccount_record = '" . ( $idprovider_invoice ) . "' AND idaccount_plan = $idaccount_plan ";
$rs2 =& DBUtil::query( $q2 );


$rsr->MoveNext();
}
}
//------Fin lettrage provider


////------Lettrage buyer
if ($record_type == "regulations_buyer")
{


$query = "
			SELECT  idbilling_buyer,idregulations_buyer
			FROM billing_regulations_buyer
			WHERE idregulations_buyer = '$idrecord'
			
			";


	$rsr =& DBUtil::query( $query );


while( !$rsr->EOF() ){
 $idbilling_buyer = $rsr->fields( "idbilling_buyer" );
$idregulations_buyer = $rsr->fields( "idregulations_buyer" );

$q2 = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idaccount_record = '" . ( $idbilling_buyer ) . "' AND idaccount_plan = $idaccount_plan ";
$rs2 =& DBUtil::query( $q2 );


$rsr->MoveNext();
}

}

if ($record_type == "billing_buyer")
{

	$query = "
			SELECT  idbilling_buyer,idregulations_buyer
			FROM billing_regulations_buyer
			WHERE idbilling_buyer = '$idrecord'
			
			";


	$rsr =& DBUtil::query( $query );


while( !$rsr->EOF() ){
 $idbilling_buyer = $rsr->fields( "idbilling_buyer" );
$idregulations_buyer = $rsr->fields( "idregulations_buyer" );

$q2 = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idaccount_record = '" . ( $idregulations_buyer ) . "' AND idaccount_plan = $idaccount_plan ";

print $q2;

$rs2 =& DBUtil::query( $q2 );

$rsr->MoveNext();

}
}
//------Fin lettrage buyer



//------Lettrage supplier
if ($record_type == "regulations_supplier")
{

	$query = "
			SELECT  billing_supplier,idregulations_supplier
			FROM billing_regulations_supplier
			WHERE idregulations_supplier = '$idrecord'
			
			";


	$rsr =& DBUtil::query( $query );


while( !$rsr->EOF() ){
 $billing_supplier = $rsr->fields( "billing_supplier" );
$idregulations_supplier = $rsr->fields( "idregulations_supplier" );

$q2 = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idaccount_record = '" . ( $billing_supplier ) . "' AND idaccount_plan = $idaccount_plan ";



$rs2 =& DBUtil::query( $q2 );


$rsr->MoveNext();
}

}

if ($record_type == "billing_supplier")
{

	$query = "
			SELECT  billing_supplier,idregulations_supplier
			FROM billing_regulations_supplier
			WHERE billing_supplier = '$idrecord'
			
			";


	$rsr =& DBUtil::query( $query );


while( !$rsr->EOF() ){
 $billing_supplier = $rsr->fields( "billing_supplier" );
$idregulations_supplier = $rsr->fields( "idregulations_supplier" );

$q2 = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idaccount_record = '" . ( $idregulations_supplier ) . "' AND idaccount_plan = $idaccount_plan ";



$rs2 =& DBUtil::query( $q2 );


$rsr->MoveNext();
}

}
//---------Fin lettrage supplier


	$query = "UPDATE account_plan SET lettering = '" . Util::html_escape( strtoupper( $_GET[ "value" ] ) ) . "' WHERE idrow = " . intval( $_GET[ "idrow" ] ) . " LIMIT 1";

	$rs =& DBUtil::query( $query );

	if( $rs === false ){

		trigger_error( "Impossible de mettre à jour le lettrage", E_USER_ERROR );
		exit();

	}

	exit( "0" );

}

//--------------------------------------------------------------------------------

//@todo: fonction plus utilisée pour l'instant. Désactivé car il y avait plusieurs bugs dans les javascripts et parce que les dates n'étaient pas prises en compte
function getLetteringList(){

	//Si client
	if( substr( $_GET[ "idrelation" ], 0, 6 ) == "Client" ){

		$idrelation = intval( substr( $_GET[ "idrelation" ], 6 ) );
		$relation_type = "buyer";

	}
	//Si fournisseur
	elseif( substr( $_GET[ "idrelation" ], 0, 6 ) == "Fourn." ){

		$idrelation = intval( substr( $_GET[ "idrelation" ], 6 ) );
		$relation_type = "supplier";

	}
	else{

		$idrelation = $_GET[ "relation_name" ];
		$relation_type = "others";

	}

	$rs =& DBUtil::query( "
		SELECT DISTINCT( lettering )
		FROM account_plan
		WHERE idrelation = '$idrelation'
			AND relation_type = '$relation_type'
			AND lettering != ''
		ORDER BY LENGTH( lettering ) ASC, lettering ASC" );

	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des lettrages du tiers : " . $_GET[ "idrelation" ], E_USER_ERROR );

?>
<select name="lettering" id="lettering">
	<option value=""></option>
<?php

	$lettering = false;

	while( !$rs->EOF ){

		$lettering = $rs->fields( "lettering" );

?>
	<option value="<?php echo $lettering ?>"><?php echo $lettering ?></option>
<?php

		$rs->MoveNext();

	}

	$lettering = AccountPlan::getNextLettering( $idrelation, $relation_type, $lettering );

?>
	<option value="<?php echo $lettering ?>" style="color:#009900;"><?php echo $lettering ?></option>
</select>
<?php

}

//--------------------------------------------------------------------------------

function addModelEntry(){

	global $GLOBAL_START_PATH;

	include_once( "$GLOBAL_START_PATH/objects/AccountModelParser.php" );

	$model = new AccountModelParser( $_POST[ "idmodel" ], 0 );

	$errors =& $model->insert();

	if( count( $errors ) ){

		foreach( $errors as $error )
			echo "$error<br />";

		exit();

	}

}

//--------------------------------------------------------------------------------

function entryEdition( $type = "create" ){

	if( $type == "create" )
		$row_count = $_POST[ "row_count" ];
	elseif( $type == "edit" )
		$row_count = 1;

	for( $i = 1 ; $i < $row_count + 1 ; $i++ ){

		if( $type == "create" )
			$row = "_$i";
		elseif( $type == "edit" )
			$row = "";

		//On vérifie que toutes les informations obligatoires ont bien été renseignées

		if( !isset( $_POST[ "account_plan_1$row" ] ) || empty( $_POST[ "account_plan_1$row" ] ) )
			exit( "Aucun code journal n'a été renseigné" );

		if( !isset( $_POST[ "idaccount_plan$row" ] ) || empty( $_POST[ "idaccount_plan$row" ] ) )
			exit( "Aucun numéro de compte n'a été renseigné" );

		/*if( !isset( $_POST[ "relation_name$row" ] ) || empty( $_POST[ "relation_name$row" ] ) )
			exit( "Aucun tiers n'a été renseigné" );*/

		/*if( !isset( $_POST[ "idaccount_record$row" ] ) || empty( $_POST[ "idaccount_record$row" ] ) )
			exit( "Aucun numéro de pièce n'a été renseigné" );*/

		if( !isset( $_POST[ "value_date$row" ] ) || empty( $_POST[ "value_date$row" ] ) )
			exit( "Aucune date de valeur n'a été renseignée" );

		if( !isset( $_POST[ "designation$row" ] ) || empty( $_POST[ "designation$row" ] ) )
			exit( "Aucune désignation n'a été renseignée" );

		if( !isset( $_POST[ "amount$row" ] ) || empty( $_POST[ "amount$row" ] ) )
			exit( "Aucun montant n'a été renseigné" );

		if( !isset( $_POST[ "amount_type$row" ] ) || empty( $_POST[ "amount_type$row" ] ) )
			exit( "Le crédit / débit n'a pas été renseigné" );

		//On vérifie que le numéro de compte n'est pas un compte parent

		$rs =& DBUtil::query( "SELECT COUNT( * ) AS children FROM accounting_plan WHERE accounting_plan_parent = '" . Util::html_escape( $_POST[ "idaccount_plan$row" ] ) . "'" );

		if( $rs === false )
			exit( "Impossible de vérifier le numéro de compte " . $_POST[ "idaccount_plan$row" ] );

		if( $rs->fields( "children" ) > 0 )
			exit( "Le numéro de compte sélectionné n'est pas valide" );

		$idaccount_plan = $_POST[ "idaccount_plan$row" ];

		//On vérifie que le numéro de tiers existe, sinon, on prend le nom saisi du tiers

		//Si client
		if( $_POST[ "relation_type" ] == "buyer" ){ //Client

			//$idrelation = intval(  $_POST[ "idrelation$row" ] );
			$idrelation = (  $_POST[ "idrelation$row" ] );
			$relation_type = "buyer";

			$rs =& DBUtil::query( "SELECT idbuyer FROM buyer WHERE idbuyer = '$idrelation'" );

			if( $rs === false )
				exit( "Impossible de vérifier le numéro de tiers : " . $_POST[ "idrelation$row" ] );

			if( !$rs->RecordCount() )
				exit( "Numéro de tiers inconnu : " . $_POST[ "idrelation$row" ] );

		}
		//Si fournisseur
		elseif(  $_POST[ "relation_type" ] == "supplier" ){ //Fournisseur

			$idrelation =   $_POST[ "idrelation$row" ];
			
			//$tab = explode(". ", $idrelation);
			
			//$idsup = $tab[1];
			
			$relation_type = "supplier";

			$rs =& DBUtil::query( "SELECT idsupplier FROM supplier WHERE idsupplier = $idrelation" );

			if( $rs === false )
				exit( "Impossible de vérifier le numéro de tiers : " . $_POST[ "idrelation$row" ] );

			if( !$rs->RecordCount() )
				exit( "Numéro de tiers inconnu : " . $_POST[ "idrelation$row" ] );

		}
		//Si fournisseur de service
		elseif(  $_POST[ "relation_type" ] == "provider" ){ //Fournisseur de service

			//$idrelation = intval(  $_POST[ "idrelation$row" ]  );
			$idrelation = (  $_POST[ "idrelation$row" ]  );
			$relation_type = "provider";

			$rs =& DBUtil::query( "SELECT idprovider FROM provider WHERE idprovider = '$idrelation'" );

			if( $rs === false )
				exit( "Impossible de vérifier le numéro de tiers : " . $_POST[ "idrelation$row" ] );

			if( !$rs->RecordCount() )
				exit( "Numéro de tiers inconnu : " . $_POST[ "idrelation$row" ] );

		}
		else{

			//$idrelation = intval($_POST[ "relation_name$row" ]);
			$idrelation = ($_POST[ "relation_name$row" ]);
			$relation_type = "others";

		}

		$idaccount_record = $_POST[ "idaccount_record$row" ];

		//Vérification du format de date (date de valeur)

		$datePattern = "/^([0-9]{2})-{1}([0-9]{2})-{1}([0-9]{4})\$/";

		if( !preg_match( $datePattern, $_POST[ "value_date$row" ] ) )
			exit( "Format de date de valeur erroné" );

		$date = Util::dateFormatUs( $_POST[ "value_date$row" ] );

		//Montant

		if( !is_numeric( str_replace( ",", ".", $_POST[ "amount$row" ] ) ) )
			exit( "Le montant saisi n'est pas un nombre" );

		$amount = floatval( str_replace( ",", ".", $_POST[ "amount$row" ] ) );

		//Type de montant

		if( $_POST[ "amount_type$row" ] != "credit" && $_POST[ "amount_type$row" ] != "debit" )
			exit( "Le type de montant doit être un crédit ou un débit" );

		$amount_type = $_POST[ "amount_type$row" ];

		//Désignation

		$designation = Util::doNothing( $_POST[ "designation$row" ] );

		//Code EU
		//@todo : Récupérer le code EU à partir de l'adresse du client/fournisseur sélectionné

		$code_eu = "";

		//Code TVA

		$code_vat = "";


		//Mode de paiement

		if( isset( $_POST[ "payment$row" ] ) && !empty( $_POST[ "payment$row" ] ) ){
			$payment =  $_POST[ "payment$row" ];
//			$payment = intval( $_POST[ "payment$row" ] );
        }
		else
			$payment = "";

		//Date de paiement

		if( isset( $_POST[ "payment_date$row" ] ) && !empty( $_POST[ "payment_date$row" ] ) && !preg_match( $datePattern, $_POST[ "payment_date$row" ] ) )
			exit( "Format de date de paiement erroné" );

		if( isset( $_POST[ "payment_date$row" ] ) && !empty( $_POST[ "payment_date$row" ] ) )
			$payment_date = Util::dateFormatUs( $_POST[ "payment_date$row" ] );
		else
			$payment_date = "";

		//Account plan

		$account_plan_1 = strtoupper( $_POST[ "account_plan_1$row" ] );
		$account_plan_2 = "D";
		$account_plan_3 = $amount_type == "credit" ? "C" : "D";
		$account_plan_4 = DBUtil::getDBValue( "forward_carry", "accounting_plan", "idaccounting_plan", $idaccount_plan );

		//Lettrage

		$lettering = isset( $_POST[ "lettering$row" ] ) ? strtoupper( $_POST[ "lettering$row" ] ) : "";

		if( $type == "create" ){

			$action = "manual_entry";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, "", $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

		}
		elseif( $type == "edit" && intval( $_POST[ "idrow" ] ) ){

			//On vérifie que l'écriture n'appartient pas à un exercice clos
			$rs =& DBUtil::query( "SELECT editable FROM account_plan WHERE idrow = " . intval( $_POST[ "idrow" ] ) );

			if( $rs === false )
				exit( '<span style="color:#FF0000;">Impossible de vérifier la date de l\'écriture</span>' );

			if( $rs->fields( "editable" ) == 0 )
				exit( '<span style="color:#FF0000;">Impossible de modifier une ligne d\'un exercice clos</span>' );

			$query = "
			UPDATE account_plan SET
				idaccount_plan = '" . Util::html_escape( $idaccount_plan ) . "',
				idrelation = '" . Util::html_escape( $idrelation ) . "',
				relation_type = '" . Util::html_escape( $relation_type ) . "',
				idaccount_record = '" . Util::html_escape( $idaccount_record ) . "',
				date = '" . Util::html_escape( $date ) . "',
				amount = '" . Util::html_escape( $amount ) . "',
				amount_type = '" . Util::html_escape( $amount_type ) . "',
				designation = '" . Util::html_escape( $designation ) . "',
				payment = '" . Util::html_escape( $payment ) . "',
				payment_date = '" . Util::html_escape( $payment_date ) . "',
				account_plan_1 = '" . Util::html_escape( $account_plan_1 ) . "',
				account_plan_3 = '" . Util::html_escape( $account_plan_3 ) . "',
				lettering = '" . Util::html_escape( $lettering ) . "',
				username = '" . User::getInstance()->get( "login" ) . "',
				lastupdate = '" . date( "Y-m-d H:i:s" ) . "'
			WHERE idrow = " . intval( $_POST[ "idrow" ] ) . "
			LIMIT 1";

			$rs =& DBUtil::query( $query );

			if( $rs === false )
				exit( "Problème lors de la modification des données" );

			exit( "edited" );

		}


	}

	if( $type == "create" )
		exit( "created" );

	exit( "Un problème est survenu lors de l'édition de l'écriture" );

}

//--------------------------------------------------------------------------------

function dropEntry(){

	if( !isset( $_POST[ "idrow" ] ) || empty( $_POST[ "idrow" ] ) || !intval( $_POST[ "idrow" ] ) )
		exit( "Aucune ligne à supprimer spécifiée" );

	//On vérifie que l'écriture n'appartient pas à un exercice clos
	$rs =& DBUtil::query( "SELECT editable FROM account_plan WHERE idrow = " . intval( $_POST[ "idrow" ] ) );

	if( $rs === false )
		exit( '<span style="color:#FF0000;">Impossible de vérifier la date de l\'écriture</span>' );

	if( $rs->fields( "editable" ) == 0 )
		exit( '<span style="color:#FF0000;">Impossible de supprimer une ligne d\'un exercice clos</span>' );

	$rs =& DBUtil::query( "DELETE FROM account_plan WHERE idrow = " . intval( $_POST[ "idrow" ] ) . " LIMIT 1" );

	if( $rs === false )
		exit( '<span style="color:#FF0000;">Impossible de supprimer la ligne sélectionnée</span>' );
	else
		exit( "Ligne supprimée avec succès" );

}

//--------------------------------------------------------------------------------
function date_convertor_to_datepicker($date){

	if(empty($date))
		return $date;

	$date = str_replace("/", "-", $date);
	$date_arr = explode('-', $date);
	return $date_arr[2].'-'.$date_arr[1].'-'.$date_arr[0];
	
}

function getEditionWindow(){

	global $GLOBAL_START_PATH, $GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

	$lang = User::getInstance()->getLang();

	//Default values

	$defaultValues = array(
		"account_plan_1"	=> "OD",
		"idaccount_plan"	=> "",
		"relation_name"		=> "",
		"idrelation"		=> "",
		"idaccount_record"	=> "",
		"value_date"		=> "",
		"designation"		=> "",
		"payment"			=> "",
		"payment_date"		=> "",
		"amount"			=> "",
		"amount_type"		=> "",
		"lettering"			=> ""
	);

	$rs =& DBUtil::query( "SELECT account_plan_1, idaccount_plan, relation_type, idrelation, idaccount_record, date, designation, payment, payment_date, amount, amount_type, lettering FROM account_plan WHERE idrow = " . intval( $_GET[ "idrow" ] ) );

	if( $rs === false )
		trigger_error( "Impossible de récupérer les informations concernant cette écriture", E_USER_ERROR );

	if( $rs->fields( "relation_type" ) == "buyer" ){

		$idrelation = $rs->fields( "idrelation" );
		$relation_name = AccountPlan::getCustomerName( $rs->fields( "idrelation" ) );

	}
	elseif( $rs->fields( "relation_type" ) == "supplier" ){

		$idrelation = $rs->fields( "idrelation" );
		$relation_name = DBUtil::getDBValue( "name", "supplier", "idsupplier", $rs->fields( "idrelation" ) );

	}
	else{

		$idrelation = $rs->fields( "idrelation" );
		$relation_name = $rs->fields( "idrelation" );

	}

	$defaultValues = array(
		"account_plan_1"	=> $rs->fields( "account_plan_1" ),
		"idaccount_plan"	=> $rs->fields( "idaccount_plan" ),
		"relation_name"		=> $relation_name,
		"idrelation"		=> $idrelation,
		"idaccount_record"	=> $rs->fields( "idaccount_record" ),
		"value_date"		=> $rs->fields( "date" ) == "0000-00-00" ? "" : date_convertor_to_datepicker($rs->fields( "date" )) ,
		"designation"		=> $rs->fields( "designation" ),
		"payment"			=> $rs->fields( "payment" ),
		"payment_date"		=> $rs->fields( "payment_date" ) == "0000-00-00" ? "" : date_convertor_to_datepicker($rs->fields( "payment_date" )) ,
		"amount"			=> $rs->fields( "amount" ),
		"amount_type"		=> $rs->fields( "amount_type" ),
		"lettering"			=> $rs->fields( "lettering" ),
		"relation_type"			=> $rs->fields( "relation_type" )
	);

?>
		<div style="font-size:14px; font-weight:bold; margin-bottom:15px; text-align:center;">Modifier une écriture</div>
		<form id="newEntryForm" onsubmit="return false;">
			<div>
				<table>
					<tr>
						<td>Code journal</td>
						<td>
							<select name="account_plan_1" id="account_plan_1">
								<?php

									$rs =& DBUtil::query( "SELECT diary_code FROM diary_code ORDER BY diary_code ASC" );

									if( $rs == false )
										trigger_error( "Impossible de réupérer la liste des codes comptables", E_USER_ERROR );

									while( !$rs->EOF ){

										$selected = $rs->fields( "diary_code" ) == $defaultValues[ "account_plan_1" ] ? ' selected="selected"' : "";

										echo '<option value="' . $rs->fields( "diary_code" ) . "\"$selected>" . $rs->fields( "diary_code" ) . '</option>';

										$rs->MoveNext();

									}

								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Numéro de compte</td>
						<td><?php accountsSelect( $withAll = false, $id = "idaccount_plan", $defaultValues [ "idaccount_plan" ] ); ?></td>
					</tr>
					<tr>
						<td>Type de tiers</td>
						<td>
							<select  name="relation_type" id="relation_type">
								<option <?php echo  $defaultValues [ "relation_type" ] == 'buyer'? 'selected="selected"':''; ?> value="buyer">Client</option>
								<option <?php echo $defaultValues [ "relation_type" ] == 'supplier'?'selected="selected"':''; ?>  value="supplier">Fournisseur</option>
								<option <?php echo $defaultValues [ "relation_type" ] == 'provider'?'selected="selected"':''; ?>  value="provider">Fournisseur de service</option>
								<option <?php echo $defaultValues [ "relation_type" ] == 'others'?'selected="selected"':''; ?>  value="others">Autres</option>
							</select>
							</td>
					</tr>
					<tr>
						<td>Numéro de tiers</td>
						<td>
							<input type="text" name="relation_name" onchange="onChange(event)" id="relation_name" value="<?php echo $defaultValues [ "relation_name" ] ?>" class="textInput" />
							<input type="hidden" name="idrelation" id="idrelation" value="<?php echo $defaultValues [ "idrelation" ] ?>" />
							<?php AutoCompletor::completeFromFile( "relation_name", "$GLOBAL_START_URL/xml/autocomplete/customer_supplier.php", 5, "changeRelation", true ); ?>
						</td>
					</tr>
					<tr>
						<td>Pièce n°</td>
						<td><input type="text" name="idaccount_record" id="idaccount_record" value="<?php echo $defaultValues [ "idaccount_record" ] ?>" class="textInput" /></td>
					</tr>
					<tr>
						<td>Date de valeur</td>
						<td>
							<input type="text" name="value_date" id="value_date" value="<?php echo $defaultValues [ "value_date" ] ?>" class="calendarInput" />
							<?php DHTMLCalendar::calendar( "value_date" ); ?>
						</td>
					</tr>
					<tr>
						<td>Désignation</td>
						<td><input type="text" name="designation" id="designation" value="<?php echo $defaultValues [ "designation" ] ?>" class="textInput" /></td>
					</tr>
					<tr>
						<td>Mode de paiement</td>
						<td>
							<select name="payment" id="payment">
								<option value="">-</option>
								<?php

									$rs =& DBUtil::query( "SELECT idpayment, name$lang AS name FROM payment ORDER BY name$lang ASC" );

									if( $rs == false )
										trigger_error( "Impossible de réupérer la liste des modes de paiement", E_USER_ERROR );

									while( !$rs->EOF ){

										$selected = $rs->fields( "idpayment" ) == $defaultValues[ "idpayment" ] ? ' selected="selected"' : "";

										echo '<option value="' . $rs->fields( "idpayment" ) . "\"$selected>" . $rs->fields( "name" ) . '</option>';

										$rs->MoveNext();

									}

								?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Date de paiement</td>
						<td>
							<input type="text" name="payment_date" id="payment_date" value="<?php echo $defaultValues [ "payment_date" ] ?>" class="calendarInput" />
							<?php DHTMLCalendar::calendar( "payment_date" ); ?>
						</td>
					</tr>
					<tr>
						<td>Montant</td>
						<td><input type="text" name="amount" id="amount" value="<?php echo $defaultValues [ "amount" ] ?>" class="calendarInput" style="text-align:right;" /> ¤</td>
					</tr>
					<tr>
						<td>Crédit / débit</td>
						<td>
							<select name="amount_type" id="amount_type">
								<option value="credit"<?php echo  $defaultValues [ "amount_type" ] == "credit" ? ' selected="selected"' : "" ?>>Crédit</option>
								<option value="debit"<?php echo  $defaultValues [ "amount_type" ] == "debit" ? ' selected="selected"' : "" ?>>Débit</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Lettrage</td>
						<td id="letteringCell"><input type="text" name="lettering" id="lettering" value="<?php echo $defaultValues [ "lettering" ] ?>" class="textInput" style="width:30px;" /></td>
					</tr>
				</table>
			</div>
			<div style="margin-top:5px; text-align:right;">
				<input type="button" value="Annuler" class="blueButton" onclick="$.unblockUI();" />
				<input type="button" value="Modifier" class="orangeButton" onclick="entryEdition('<?php echo intval( $_GET[ "idrow" ] ) ?>');" />
			</div>
			<div id="errorMessages"></div>
		</form>
<?php

}

//--------------------------------------------------------------------------------

function getCreationWindow(){

	global $GLOBAL_START_URL;

?>
		<style type="text/css">
		<!--

			.autosuggest ul{ width:375px; }
			.autosuggest .as_header,
			.autosuggest .as_footer{ width:363px; }

		-->
		</style>
		<script type="text/javascript">
		<!--

			$('#newWrittingsTableContainer').css('max-height', ( $(document).height() - 350 ) + 'px');

			$('#newEntryForm').ajaxForm({
				success: function( response ){

					if( response == 'created' )
						$.growlUI( '&Eacute;criture ajoutée avec succès', '' );

					else if( response == 'edited' )
						$.growlUI( '&Eacute;criture modifiée avec succès', '' );

					else{

						$("#creatingEntrySpinner").remove();
						$("#errorMessages").html('<div style="border:1px solid #AAAAAA; margin: 10px 0;"></div>' + response);

					}

				}
			});

		-->
		</script>
		<div style="font-size:14px; font-weight:bold; margin-bottom:15px; text-align:center;">Ajouter des écritures</div>
		<form id="newEntryForm" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php">
			<input type="hidden" name="addEntry" value="1" />
			<input type="hidden" name="row_count" id="row_count" value="1" />
			<input type="hidden" name="idrelation" id="idrelation" value="" />
			<div id="newWrittingsTableContainer" class="tableContainer" style="overflow:auto; padding-top:1px;">
				<table id="newWrittingsTable" class="dataTable">
					<tbody>
						<tr class="filledRow">
							<td style="background-color:white; border-style:none; width:16px;"></td>
							<th style="text-align:center; width:55px;">Code journal <span class="asterix">*</span></th>
							<th style="text-align:center; width:63px;">Numéro de compte <span class="asterix">*</span></th>
							<th style="text-align:center; width:100px;">Client / fournisseur <span class="asterix">*</span></th>
							<th style="text-align:center; width:50px;">Pièce n° <span class="asterix">*</span></th>
							<th style="text-align:center; width:60px;">Date de valeur <span class="asterix">*</span></th>
							<th style="text-align:center;">Désignation <span class="asterix">*</span></th>
							<th style="text-align:center; width:125px;">Mode de paiement</th>
							<th style="text-align:center; width:60px;">Date de paiement</th>
							<th style="text-align:center; width:62px;">Montant <span class="asterix">*</span></th>
							<th style="text-align:center; width:56px;">Crédit / débit <span class="asterix">*</span></th>
							<th style="text-align:center; width:50px;">Lettrage <span class="asterix">*</span></th>
						</tr>
						<?php getCreationRow(); ?>
					</tbody>
				</table>
			</div>
			<div style="margin-top:10px; text-align:right;">
				<input type="button" value="Annuler" class="blueButton" onclick="$.unblockUI();" />
				<input type="submit" value="Enregistrer" class="orangeButton" />
			</div>
			<div id="errorMessages"></div>
		</form>
<?php

}

//--------------------------------------------------------------------------------

function getModelWindow(){

	global $GLOBAL_START_URL;

?>
		<script type="text/javascript">
		<!--

			$('#newModelEntryForm').ajaxForm({
				beforeSubmit: function(){
					if( $('#model').val() == 0 ){
						$("#errorMessages").html("Vous devez d'abord sélectionner un modèle");
						return false;
					}
				},
				success: function( response ){
					if( response == '0' )
						$.growlUI( '&Eacute;critures ajoutées avec succès', '' );
					else
						$("#errorMessages").html('<div style="border:1px solid #AAAAAA; margin: 10px 0;"></div>' + response);
				}
			});

		-->
		</script>
		<div style="font-size:14px; font-weight:bold; margin-bottom:15px; text-align:center;">Ajouter des écritures</div>
		<form method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php">
			<div style="margin:5px 0 15px 0;">
				Choisir un modèle d'écritures :
				<select name="model" id="model" onchange="getModelTable(this.value);">
					<option value="">-</option>
				<?php

					$rs =& DBUtil::query( "SELECT * FROM accounts_models ORDER BY name DESC" );

					while( !$rs->EOF ){

						echo "<option value=\"" . $rs->fields( "idmodel" ) . "\">" . $rs->fields( "name" ) . "</option>";

						$rs->MoveNext();

					}

				?>
				</select>
			</div>
		</form>
		<form id="newModelEntryForm" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php">
			<input type="hidden" name="addModelEntry" value="1" />
			<input type="hidden" name="idmodel" id="idmodel" value="0" />
			<div class="tableContainer" id="windowModelTableContainer"></div>
			<div style="margin-top:10px; text-align:right;">
				<input type="button" value="Annuler" class="blueButton" onclick="$.unblockUI();" />
				<input type="submit" value="Enregistrer" class="orangeButton" />
			</div>
			<div id="errorMessages"></div>
		</form>
<?php

}

//--------------------------------------------------------------------------------

function getModelTable( $idmodel ){

	global $GLOBAL_START_PATH;

	include_once( "$GLOBAL_START_PATH/objects/AccountModelParser.php" );

	?>
				<script type="text/javascript">
				<!--

					$(".model_amount_debit").keyup(function(){
						if( $(this).val() != '' )
							$(this).parent('td').parent('tr').children().children('.model_amount_credit').val('');
					});

					$(".model_amount_credit").keyup(function(){
						if( $(this).val() != '' )
							$(this).parent('td').parent('tr').children().children('.model_amount_debit').val('');
					});

				-->
				</script>
				<table class="dataTable resultTable">
					<thead>
						<tr>
							<th style="width:80px;">Numéro de compte</th>
							<th style="width:45px;">Numéro de pièce</th>
							<th style="width:95px;">Type de pièce</th>
							<th style="width:45px;">Numéro de tiers</th>
							<th style="width:90px;">Type de tiers</th>
							<th>Libellé</th>
							<th style="width:100px;">Date</th>
							<th>Date de paiement</th>
							<th style="width:125px;">Mode de paiement</th>
							<th style="width:35px;">Débit</th>
							<th style="width:35px;">Crédit</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$query = "SELECT * FROM accounts_models_row WHERE idmodel = $idmodel ORDER BY idrow ASC";

							$rs =& DBUtil::query( $query );


							if( $rs === false )
								trigger_error( "Impossible de récupérer les modèles d'écritures", E_USER_ERROR );

							$model = new AccountModelParser( $idmodel, $rs->RecordCount() );

                            $count = -1;
							while( !$rs->EOF ){
                                $count++;
                                $payment_name = "";
                                if($rs->fields( 'payment' ) == "%manual%"){
                                  $payment_name = $model->parseInputPayment($rs->fields( 'payment' ));
                                }else if($rs->fields( 'payment' ) != ""){
                                    $payment_query = "SELECT name_1 FROM payment where idpayment = ".$rs->fields( 'payment' )." ORDER BY name_1 ASC";
                                    $payment_rs =& DBUtil::query( $payment_query );
                                    if( $payment_rs === false )
                                        trigger_error( "Impossible de récupérer les payment d'écritures", E_USER_ERROR );
                                    $payment_name = '<input type="hidden" name="payment_'. $count .'" value="'.$payment_rs->fields("name_1").'">'.$payment_rs->fields("name_1");
                                }
						?>
						<tr>
							<td class="lefterCol"><?php echo $model->parseInputAccount( $rs->fields( "idaccount_plan" ) ); ?></td>
							<td><?php echo $model->parseInputRecord( $rs->fields( "idrecord" ) ); ?></td>
							<td><?php echo $model->parseInputRecordType( $rs->fields( "record_type" ) ); ?></td>
							<td><?php echo $model->parseInputThirdParty( $rs->fields( "idrelation" ) ); ?></td>
							<td><?php echo $model->parseInputThirdPartyType( $rs->fields( "relation_type" ) ); ?></td>
							<td><?php echo $model->parseInputLabel( $rs->fields( "label" ) ); ?></td>
							<td id="td_date_<?php echo $count ?>"><input style="width: 70px;" id="date_<?php echo $count ?>" name="date_<?php echo $count ?>" type="text" value="<?php echo $model->parseInputDate($rs->fields( 'date' )) ?>"></td>
							<td><?php echo $model->parseInputPaymentDate( $rs->fields( "payment_date" ) ); ?></td>
							<!--<td>
                                <select name="payment_<?/*= $count */?>">
                                <?php
/*                                  $payment_rs->MoveFirst();
                                  while( !$payment_rs->EOF ){
                                      */?>
                                            <option value="<?/*= $payment_rs->fields('name_1') */?>"><?/*= $payment_rs->fields('name_1') */?></option>
                                      <?/*
                                      $payment_rs->MoveNext();
                                  }
                                */?>
                                </select>
                            </td>-->
							<td><?php echo $payment_name ?> </td>
							<td><?php echo $model->parseInputAmount( $rs->fields( "amount" ), $rs->fields( "amount_type" ), "debit" ); ?></td>
							<td class="righterCol"><?php echo $model->parseInputAmount( $rs->fields( "amount" ), $rs->fields( "amount_type" ), "credit" ); ?></td>
						</tr>
						<?php

                               if($rs->fields( "date" ) == "%today%"){
                                    DHTMLCalendar::calendar("date_$count");
                               }else{
                                    echo "<script type='application/javascript'>jQuery('#td_date_".$count . "').html('".$model->parseInputDate($rs->fields( 'date' ))."');</script>";
                               }
								$rs->MoveNext();
								$model->moveNext();

							}

						?>
					</tbody>
				</table>
<?php

}

//--------------------------------------------------------------------------------

function getCreationRow( $row = 1 ){

	global $GLOBAL_START_PATH, $GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

	$lang = User::getInstance()->getLang();

?>
						<tr>
							<td style="background-color:#FFFFFF; border-style:none;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="+" onclick="getNewCreationRow();" style="cursor:pointer;" /></td>
							<td class="lefterCol" style="text-align:center;">
								<select name="account_plan_1_<?php echo $row ?>" id="account_plan_1_<?php echo $row ?>" style="width:50px;">
									<?php

										$rs =& DBUtil::query( "SELECT diary_code FROM diary_code ORDER BY diary_code ASC" );

										if( $rs == false )
											trigger_error( "Impossible de réupérer la liste des codes comptables", E_USER_ERROR );

										while( !$rs->EOF ){

											echo '<option value="' . $rs->fields( "diary_code" ) . "\">" . $rs->fields( "diary_code" ) . '</option>';

											$rs->MoveNext();

										}

									?>
								</select>
							</td>
							<td style="text-align:center;">
                                <?php accountsSelect( false , "idaccount_plan_$row"); ?>
								<!--<div id="idaccountPlanPopupCompletor">
									<input type="text" name="idaccount_plan_<?/*= $row */?>" id="idaccount_plan_<?/*= $row */?>" class="textInput" />
									<?/*= AutoCompletor::completeFromFile( "idaccount_plan_$row", "$GLOBAL_START_URL/xml/autocomplete/accounting_plan.php", 5 ) */?>
								</div>-->
							</td>
							<td style="text-align:center;">
								<div id="relationPopupCompletor">
									<input type="text" name="relation_name_<?php echo $row ?>" onchange="onChange(event)" id="relation_name_<?php echo $row ?>" value="" class="textInput relation_name" />
									<input type="hidden" name="idrelation_<?php echo $row ?>" id="idrelation_<?php echo $row ?>" value="" class="idrelation" />
									<?php AutoCompletor::completeFromFile( "relation_name_$row", "$GLOBAL_START_URL/xml/autocomplete/customer_supplier.php", 5, "changeRelation", true ); ?>
								</div>
							</td>
							<td style="text-align:center;"><input type="text" name="idaccount_record_<?php echo $row ?>" id="idaccount_record_<?php echo $row ?>" value="" class="textInput" /></td>
							<td style="text-align:center;">
								<input type="text" name="value_date_<?php echo $row ?>" id="value_date_<?php echo $row ?>" value="" class="textInput" />
								<?php DHTMLCalendar::calendar( "value_date_$row", false ); ?>
							</td>
							<td style="text-align:center;"><input type="text" name="designation_<?php echo $row ?>" id="designation_<?php echo $row ?>" value="" class="textInput" /></td>
							<td style="text-align:center;">
								<select name="payment_<?php echo $row ?>" id="payment_<?php echo $row ?>">
									<option value="">-</option>
									<?php

										$rs =& DBUtil::query( "SELECT idpayment, name$lang AS name FROM payment ORDER BY name$lang ASC" );

										if( $rs == false )
											trigger_error( "Impossible de réupérer la liste des modes de paiement", E_USER_ERROR );

										while( !$rs->EOF ){

											echo '<option value="' . $rs->fields( "name" ) . "\">" . $rs->fields( "name" ) . '</option>';

											$rs->MoveNext();

										}

									?>
								</select>
							</td>
							<td style="text-align:center;">
								<input type="text" name="payment_date_<?php echo $row ?>" id="payment_date_<?php echo $row ?>" value="" class="textInput" />
								<?php DHTMLCalendar::calendar( "payment_date_$row", false ); ?>
							</td>
							<td style="white-space:nowrap;"><input type="text" name="amount_<?php echo $row ?>" id="amount_<?php echo $row ?>" value="" class="textInput" style="text-align:right; width:50px;" /> ¤</td>
							<td style="text-align:center;">
								<select name="amount_type_<?php echo $row ?>" id="amount_type_<?php echo $row ?>">
									<option value="credit">Crédit</option>
									<option value="debit">Débit</option>
								</select>
							</td>
							<td id="letteringCell_<?php echo $row ?>" class="righterCol" style="text-align:center;"><input type="text" name="lettering_<?php echo $row ?>" id="lettering_<?php echo $row ?>" value="" class="textInput" style="width:30px;" /></td>
						</tr>
<?php

}

//--------------------------------------------------------------------------------

function checkLetteringContent(){

	global $GLOBAL_START_URL;

	//@todo: gérer les autres "fournisseurs"

	$query = "
		SELECT idaccount_plan,
			idrelation,
			relation_type,
			lettering,
			SUM( ( amount_type = 'credit' ) * amount - ( amount_type = 'debit' ) * amount ) AS total,
			ABS( SUM( ( amount_type = 'credit' ) * amount - ( amount_type = 'debit' ) * amount ) ) AS abstotal
		FROM account_plan
		WHERE lettering != ''
			AND relation_type IN( 'buyer', 'supplier', 'provider' )
		GROUP BY relation_type,
			idrelation,
			lettering
		HAVING total != 0
		ORDER BY relation_type ASC,
			abstotal ASC,
			idrelation ASC";

	$rs =& DBUtil::query( $query );

	if( $rs === false ){

		trigger_error( "Impossible de récupérer la liste des lettrages non balancés", E_USER_ERROR );
		exit();

	}

	$titleArray = array( "buyer" => "Clients", "supplier" => "Fournisseurs", "provider" => "Service", "others" => "Autres", "" => "Autres" );

?>
<style type="text/css">
<!--

	table.blockUITable{
		border:1px solid #CBCBCB;
		border-collapse:collapse;
		border-spacing:0;
		margin:1px auto;
		width:99%;
	}

	.blockUITable th,
	.blockUITable td{
		border-bottom:1px solid #CBCBCB;
		border-top:1px solid #CBCBCB;
		padding:3px;
	}

	.blockUITable th{
		background-color:#EAEAEA;
	}

	.blockUITable td{
	}

	.blockUITable td.amount{
		text-align:right;
		white-space:nowrap;
	}

-->
</style>
<div style="font-size:16px; font-weight:bold; margin-bottom:15px;">Ajustement des écarts de lettrage</div>
<?php if( !$rs->RecordCount() ){ ?>
<div style="font-size:14px; font-weight:bold; margin-bottom:15px;">Aucun écart de lettrage constaté</div>
<?php }else{ ?>
<div style="font-size:14px; font-weight:bold; margin-bottom:15px;"><?php echo $titleArray[ $rs->fields( "relation_type" ) ] ?></div>
<table class="blockUITable">
	<thead>
		<th>Tiers n°</th>
		<th>Tiers</th>
		<th>Lettrage</th>
		<th>Balance</th>
		<th style="width:15%;">Ajuster</th>
	</thead>
	<tbody>
<?php

	$i = 0;
	$relation = "";

	while( !$rs->EOF ){

		if( $relation != $rs->fields( "relation_type" ) && $relation != "" ){

			$title = $titleArray[ $rs->fields( "relation_type" ) ];

?>
	</tbody>
</table>
<div style="font-size:14px; font-weight:bold; margin:15px;"><?php echo $title ?></div>
<table class="blockUITable">
	<thead>
		<th>Tiers n°</th>
		<th>Tiers</th>
		<th>Lettrage</th>
		<th>Balance</th>
		<th style="width:15%;">Ajuster</th>
	</thead>
	<tbody>
<?php

		}

		$idrelation = $rs->fields( "idrelation" );
		$name = getRelationName( $rs->fields( "relation_type" ), $idrelation );

		if( $rs->fields( "relation_type" ) == "buyer" ){

			$link = "<a href=\"$GLOBAL_START_URL/sales_force/contact_buyer.php?key=$idrelation\" onclick=\"window.open(this.href); return false;\">";
			$idrelation = $link . $idrelation . "</a>";
			$name = $link . $name . "</a>";

		}

?>
		<tr id="row_<?php echo $i ?>">
			<td><?php echo $idrelation ?></td>
			<td><?php echo $name ?></td>
			<td><?php echo $rs->fields( "lettering" ) ?></td>
			<td class="amount"><?php echo Util::priceFormat( $rs->fields( "total" ) ) ?></td>
			<td style="color:#FF0000; font-weight:bold;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Ajuster" onclick="adjust('<?php echo $rs->fields( "relation_type" ) ?>','<?php echo $rs->fields( "idrelation" ) ?>','<?php echo $rs->fields( "lettering" ) ?>',<?php echo $i ?>)" style="cursor:pointer;" /></td>
		</tr>
<?php

		$relation = $rs->fields( "relation_type" );
		$rs->MoveNext();
		$i++;

	}

?>
	</tbody>
</table>
<?php

	}

}

//--------------------------------------------------------------------------------

function getRelationName( $relation_type, $idrelation ){

	switch( $relation_type ){
		case "buyer":
			$name = AccountPlan::getCustomerName( $idrelation );
			break;
		case "supplier":
			$name = DBUtil::getDBValue( "name", "supplier", "idsupplier", $idrelation );
			break;
		case "provider":
			$name = DBUtil::getDBValue( "name", "provider", "idprovider", $idrelation );
			break;	
		case "others":
			$name = $idrelation;
			break;
		default:
			$name = $idrelation;
			break;
	}

	return $name;

}

//--------------------------------------------------------------------------------

function adjustLettering(){

	//@todo: gérer les autres cas que 'buyer' ou 'supplier'

	/*if( !isset( $_GET[ "type" ] ) || empty( $_GET[ "type" ] ) || $_GET[ "type" ] != "buyer" && $_GET[ "type" ] != "supplier" )
		exit( "Type de tiers inconnu" );

	if( !isset( $_GET[ "idrelation" ] ) || empty( $_GET[ "idrelation" ] ) )
		exit( "Numéro de tiers inconnu" );*/

	if( !isset( $_GET[ "lettering" ] ) || empty( $_GET[ "lettering" ] ) )
		exit( "Lettrage inconnu" );

	$type = $_GET[ "type" ];
	$idrelation = $_GET[ "idrelation" ];
	$lettering = $_GET[ "lettering" ];

	if( $type == "buyer" ){

		$query = "
			SELECT state.code_eu,
				buyer.idbuyer,
				SUM( ( account_plan.amount_type = 'credit' ) * account_plan.amount - ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS amount
			FROM account_plan,
				buyer,
				state
			WHERE account_plan.relation_type = 'buyer'
				AND account_plan.idrelation = '$idrelation'
				AND account_plan.lettering = '$lettering'
				AND account_plan.idrelation = buyer.idbuyer
				AND buyer.idstate = state.idstate
			GROUP BY account_plan.idrelation";

		generateCustomerAdjustments( $query );

	}
	elseif( $type == "supplier" ){

		$query = "
			SELECT supplier.name,
				state.code_eu,
				SUM( ( account_plan.amount_type = 'credit' ) * account_plan.amount - ( account_plan.amount_type = 'debit' ) * account_plan.amount ) AS total
			FROM account_plan,
				buyer,
				state
			WHERE account_plan.relation_type = 'supplier'
				AND account_plan.idrelation = '$idrelation'
				AND account_plan.lettering = '$lettering'
				AND account_plan.idrelation = supplier.idsupplier
				AND supplier.idstate = state.idstate
			GROUP BY account_plan.idrelation";

		$rs =& DBUtil::query( $query );

		if( $rs === false )
			trigger_error( "Impossible de récupérerle montant de l'écart", E_USER_ERROR );

		if( $rs->RecordCount() && abs( $rs->fields( "total" ) ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 1 );
			$idrelation			= $idrelation;
			$relation_type		= "supplier";
			$idaccount_record	= "";
			$record_type		= "";
			$date				= date( "Y-m-d" );
			$amount				= abs( $rs->fields( "total" ) );
			$amount_type		= $rs->fields( "total" ) > 0 ? "debit" : "credit";
			$designation		= Util::doNothing("Ajustement écart ") . $rs->fields( "name" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= $rs->fields( "total" ) > 0 ? "D" : "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

			//65800000 - Ajustement écarts (écart > 0)
			//75800000 - Ajustement écarts (écart < 0)

			$idaccount_plan		= $rs->fields( "total" ) > 0 ? AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 25 ) : AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 24 );
			$idrelation			= $idrelation;
			$relation_type		= "supplier";
			$idaccount_record	= "";
			$record_type		= "";
			$date				= date( "Y-m-d" );
			$amount				= abs( $rs->fields( "total" ) );
			$amount_type		= $rs->fields( "total" ) > 0 ? "credit" : "debit";
			$designation		= Util::doNothing("Ajustement écart fournisseur ") . $rs->fields( "name" );
			$code_eu			= "";
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= $rs->fields( "total" ) > 0 ? "C" : "D";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action, $lettering );

		}

	}

}

//---------------------------------------------------------------------------------------------

function generateProviderInvoices(){

	$action = "provider_invoices";
/*
$query = "
	SELECT pi.idprovider_invoice,
	pi.vat_amount,
	pi.invoice_date,
	pi.idprovider,
	pi.idpayment,
	SUM( pii.price * pii.quantity ) + pi.vat_amount AS totalATI,
	SUM( pii.price * pii.quantity ) AS totalET,
	GROUP_CONCAT( s.idaccounting_plan ) AS accountNumbers,
	GROUP_CONCAT( pii.quantity * pii.price ) AS itemsET,
	p.name AS providerName,
	st.code_eu,
	COALESCE( pm.name_1, '' ) AS paymentName
	FROM provider_invoice_item pii, service s, provider p, state st, provider_invoice pi
	LEFT JOIN payment pm ON pm.idpayment = pi.idpayment
	WHERE pi.invoice_date < NOW()
	AND pii.idprovider = pi.idprovider AND pii.idprovider_invoice = pi.idprovider_invoice
	AND s.idservice = pii.idservice
	AND p.idprovider = pi.idprovider
	AND st.idstate = p.idstate
	AND ( pi.idprovider_invoice, pi.idprovider ) NOT IN( " . AccountPlan::getNotInCouple( $action ) . " )
	GROUP BY CONCAT( pi.idprovider_invoice, '_', pi.idprovider )
	ORDER BY pi.idprovider_invoice, pi.idprovider";
*/
	$query = "
	SELECT pi.idprovider_invoice,
	pi.vat_amount,
	pi.invoice_date,
	pi.idprovider,
	pi.idpayment,
	pi.provider_specific,
	SUM( pii.price * pii.quantity ) + pi.vat_amount AS totalATI,
	SUM( pii.price * pii.quantity ) AS totalET,
	GROUP_CONCAT( s.idaccounting_plan ) AS accountNumbers,
	GROUP_CONCAT( pii.quantity * pii.price ) AS itemsET,
	p.name AS providerName,
	st.code_eu,
	COALESCE( pm.name_1, '' ) AS paymentName
	FROM provider_invoice_item pii, service s, provider p, state st, provider_invoice pi
	LEFT JOIN payment pm ON pm.idpayment = pi.idpayment
	WHERE pi.invoice_date < NOW()
	AND pii.idprovider = pi.idprovider AND pii.idprovider_invoice = pi.idprovider_invoice
	AND s.idservice = pii.idservice
	AND p.idprovider = pi.idprovider
	AND st.idstate = p.idstate
	AND ( pi.idprovider_invoice, pi.idprovider ) NOT IN( " . AccountPlan::getNotInCouple( $action ) . " )
	GROUP BY CONCAT( pi.idprovider_invoice, '_', pi.idprovider )
	ORDER BY pi.idprovider_invoice, pi.idprovider";

	$rs =& DBUtil::query( $query );

	while( !$rs->EOF() ){

		if( $rs->fields( "totalATI" ) > 0.0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 27 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idprovider_invoice" );
			$record_type		= "provider_invoice";
			$date				= $rs->fields( "invoice_date" );
			$amount				= round( $rs->fields( "totalATI" ), 2 );
			$amount_type		= "credit";
			/*$designation		= "Facture " . $rs->fields( "providerName" );*/
			
			if($rs->fields( "provider_specific" ) != NULL){
			$designation		= "Facture " . $rs->fields( "provider_specific" );
			}
			else{
			$designation		= "Facture " . $rs->fields( "providerName" );
			} 
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= $rs->fields( "paymentName" );
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$accountNumbers = explode( ",", $rs->fields( "accountNumbers" ) );
		$itemsET 		= explode( ",", $rs->fields( "itemsET" ) );

		$i = 0;
		while( $i < count( $itemsET ) ){

			//Achats marchandises, port HT, ...

			if( $itemsET[ $i ] > 0.0 ){

				$idaccount_plan		= $accountNumbers[ $i ];
				$idrelation			= $rs->fields( "idprovider" );
				$relation_type		= "provider";
				$idaccount_record	= $rs->fields( "idprovider_invoice" );
				$record_type		= "provider_invoice";
				$date				= $rs->fields( "invoice_date" );
				$amount				= $itemsET[ $i ];
				$amount_type		= "debit";
				/*$designation		= "Achat facture " . $rs->fields( "providerName" ); */
				if($rs->fields( "provider_specific" ) != NULL){
				$designation		= "Achat facture " . $rs->fields( "provider_specific" );
				}
				else{
				$designation		= "Achat facture " . $rs->fields( "providerName" );
				} 
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "ACH";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "N";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

			$i++;

		}

		//445 - TVA
		if( $rs->fields( "vat_amount" ) > 0.0 && $rs->fields( "code_eu" ) == "NATIONAL" ){

			//44566190 - TVA collectée France

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 26 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idprovider_invoice" );
			$record_type		= "provider_invoice";
			$date				= $rs->fields( "invoice_date" );
			$amount				= $rs->fields( "vat_amount" );
			$amount_type		= "debit";
			/*$designation		= "TVA facture déductible France " . $rs->fields( "providerName" );*/
			if($rs->fields( "provider_specific" ) != NULL){
			$designation		= Util::doNothing("TVA facture déductible France ") . $rs->fields( "provider_specific" );
			}
			else{
			$designation		= Util::doNothing("TVA facture déductible France ") . $rs->fields( "providerName" );
			} 		
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}


		//Calcul du montant de la TVA

		$vatdefault = DBUtil::getParameter( "vatdefault" );
		$amount = round( $vatdefault / ( 100.0 + $vatdefault ) * round( $rs->fields( "totalATI" ), 2 ), 2 );

		if( $amount > 0 && $rs->fields( "code_eu" ) == "INTRA" ){

			//44527900 - TVA collectée CEE

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 26 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idprovider_invoice" );
			$record_type		= "provider_invoice";
			$date				= $rs->fields( "invoice_date" );
			$amount				= $amount;
			$amount_type		= "debit";
			$designation		= Util::doNothing("TVA facture déduite CEE ") . $rs->fields( "providerName" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//44526900 - TVA déductible CEE

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 26 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idprovider_invoice" );
			$record_type		= "provider_invoice";
			$date				= $rs->fields( "invoice_date" );
			$amount				= $amount;
			$amount_type		= "credit";
			$designation		= Util::doNothing("TVA facture collectée CEE ") . $rs->fields( "providerName" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "ACH";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateProviderRegulations(){

	$action = "provider_regulations";

	$query = "
	SELECT DISTINCT( rp.idregulations_provider ),
	rp.payment_date,
	rp.amount,
	p.idprovider,
	p.name AS providerName,
	pm.name_1 AS paymentName,
	pi.provider_specific,
	st.code_eu
	FROM payment pm, provider_invoice_item pii, state st, provider p, regulations_provider rp
	LEFT JOIN billing_regulations_provider brp ON rp.idregulations_provider = brp.idregulations_provider
	LEFT JOIN provider_invoice pi ON ( pi.idprovider = brp.idprovider AND pi.idprovider_invoice = brp.idprovider_invoice )
	WHERE rp.payment_date < NOW()
	AND rp.payment_date NOT REGEXP( '^[0-]*\$' )
	AND pii.idprovider_invoice = pi.idprovider_invoice AND pii.idprovider = pi.idprovider
	AND rp.idpayment = pm.idpayment
	AND p.idprovider = pi.idprovider
	AND st.idstate = p.idstate
	AND rp.idregulations_provider NOT IN( " . AccountPlan::getNotIn( $action ) . " )
	GROUP BY rp.idregulations_provider
	ORDER BY rp.idregulations_provider ASC";

	$rs = DBUtil::query( $query );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "amount" ), 2 ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 27 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idregulations_provider" );
			$record_type		= "regulations_provider";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "debit";
			//$designation		= "Paiement facture " . $rs->fields( "providerName" );
			if($rs->fields( "provider_specific" ) != NULL){
			$designation		= "Paiement facture " . $rs->fields( "provider_specific" );
			}
			else{
			$designation		= "Paiement facture " . $rs->fields( "providerName" );
			} 
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= $rs->fields( "paymentName" );
			$payment_date		= $rs->fields( "payment_date" );
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//512 - Banque

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 6 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idregulations_provider" );
			$record_type		= "regulations_provider";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "amount" ), 2 );
			$amount_type		= "credit";
			//$designation		= "Règlement facture " . $rs->fields( "providerName" );
			if($rs->fields( "provider_specific" ) != NULL){
			$designation		= "Paiement facture " . $rs->fields( "provider_specific" );
			}
			else{
			$designation		= "Paiement facture " . $rs->fields( "providerName" );
			} 
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= $rs->fields( "paymentName" );
			$payment_date		= "";
			$account_plan_1		= "BQ1";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

		}

		$rs->MoveNext();

	}

	return true;

}

//---------------------------------------------------------------------------------------------

function generateProviderRebates(){

	$action = "provider_rebates";

	$query = "
	SELECT pi.idprovider_invoice,
	pi.idprovider,
	pi.deliv_payment,
	pi.idprovider_invoice,
	brp.rebate_amount,
	rp.payment_date,
	st.code_eu,
	p.name AS providerName
	FROM provider_invoice pi, billing_regulations_provider brp, regulations_provider rp, state st, provider p
	WHERE pi.idprovider_invoice = brp.idprovider_invoice AND pi.idprovider = brp.idprovider
	AND rp.idregulations_provider = brp.idregulations_provider
	AND p.idstate = st.idstate
	AND pi.invoice_date < NOW()
	AND brp.rebate_amount > 0.0
	AND rp.payment_date < NOW()
	AND rp.payment_date NOT REGEXP( '^[0-]*\$' )
	AND ( pi.idprovider_invoice, pi.idprovider ) NOT IN( " . AccountPlan::getNotInCouple( $action ) . " )
	ORDER BY pi.idprovider ASC, pi.idprovider_invoice ASC";

	$rs =& DBUtil::query( $query );

	while( !$rs->EOF() ){

		if( round( $rs->fields( "rebate_amount" ), 2 ) > 0 ){

			//401 - Collectif fournisseur

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 27 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idprovider_invoice" );
			$record_type		= "provider_invoice";
			$date				= $rs->fields( "payment_date" );
			$amount				= round( $rs->fields( "rebate_amount" ), 2 );
			$amount_type		= "debit";
			$designation		= "Escompte TTC " . $rs->fields( "providerName" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= "D";
			$account_plan_4		= "A";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//765 - Escompte

			//Si France, on déduit la TVA des achats
			//Sinon, le montant des achats est le montant de l'escomtpe
			if( $rs->fields( "code_eu" ) == "NATIONAL" )
				$amount = round( round( $rs->fields( "rebate_amount" ), 2 ) / ( 1 + DBUtil::getParameter( "vatdefault" ) / 100 ), 2 );
			else
				$amount = round( $rs->fields( "rebate_amount" ), 2 );

			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 13 );
			$idrelation			= $rs->fields( "idprovider" );
			$relation_type		= "provider";
			$idaccount_record	= $rs->fields( "idprovider_invoice" );
			$record_type		= "provider_invoice";
			$date				= $rs->fields( "payment_date" );
			$amount				= $amount;
			$amount_type		= "credit";
			$designation		= "Escompte HT " . $rs->fields( "providerName" );
			$code_eu			= $rs->fields( "code_eu" );
			$code_vat			= "";
			$payment			= "";
			$payment_date		= "";
			$account_plan_1		= "OD";
			$account_plan_2		= "D";
			$account_plan_3		= "C";
			$account_plan_4		= "N";

			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			//445 - TVA

			//Calcul du montant de la TVA
			$amount = round( DBUtil::getParameter( "vatdefault" ) / ( 100 + DBUtil::getParameter( "vatdefault" ) ) * round( $rs->fields( "rebate_amount" ), 2 ), 2 );

			//44571190 - France
			if( $rs->fields( "code_eu" ) == "NATIONAL" ){

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 3 );
				$idrelation			= $rs->fields( "idprovider" );
				$relation_type		= "provider";
				$idaccount_record	= $rs->fields( "idprovider_invoice" );
				$record_type		= "provider_invoice";
				$date				= $rs->fields( "payment_date" );
				$amount				= $amount;
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA déductible France - Escompte ") . $rs->fields( "providerName" );
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "OD";
				$account_plan_2		= "D";
				$account_plan_3		= "C";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

			//44527900 & 44526900 - Intra
			if( $rs->fields( "code_eu" ) == "INTRA" ){

				//44527900

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 16 );
				$idrelation			= $rs->fields( "idprovider" );
				$relation_type		= "provider";
				$idaccount_record	= $rs->fields( "idprovider_invoice" );
				$record_type		= "provider_invoice";
				$date				= $rs->fields( "payment_date" );
				$amount				= $amount;
				$amount_type		= "debit";
				$designation		= Util::doNothing("TVA collectée CEE - Escompte ") . $rs->fields( "providerName" );
				$code_eu			= $rs->fields( "code_eu" );
				$code_vat			= "";
				$payment			= "";
				$payment_date		= "";
				$account_plan_1		= "OD";
				$account_plan_2		= "D";
				$account_plan_3		= "D";
				$account_plan_4		= "A";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

				//44526900

				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 17 );
				$amount_type		= "credit";
				$designation		= Util::doNothing("TVA déduite CEE - Escomtpe ") . $rs->fields( "providerName" );
				$account_plan_3		= "C";

				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );

			}

		}

		$rs->MoveNext();

	}

	return true;

}

//--------------------------------------------------------------------------------

function getLastExportDate(){

	global $GLOBAL_START_PATH;

	$directory = "$GLOBAL_START_PATH/data/account_plan/";

	if( !is_dir( $directory ) )
		return false;

	$matches = array();
	$files = scandir( $directory, 1 );

	foreach( $files as $file ){

		if( is_file( $directory . $file ) && preg_match( "/plan_comptable_([0-9]{4}-[0-9]{2}-[0-9]{2})_([0-9]{2}:[0-9]{2}:[0-9]{2}).csv/", $file, $matches ) )
			return $matches[ 1 ] . " " . $matches[ 2 ];

	}

	return false;

}

//--------------------------------------------------------------------------------

?>
