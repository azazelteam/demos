<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Visualisation des relances clients
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );

$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

if( !isset( $_GET[ "idbilling_buyer" ] ) ){

	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
	
}
?>
<style type="text/css">
<!--
	div#global{
		min-width:0px;
	}
	
	div#global div#globalMainContent{
		padding-right:5px;
		width:700px;
	}
-->
</style>
<div id="globalMainContent" style="margin:auto">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent fullWidthContent" style="margin:auto; width:700px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Mail envoyé</p>
                <div class="rightContainer"></div>
            </div>
            <div class="subContent" style="padding:5px;">
<?php


	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( User::getInstance()->getLang(), 1 ) ); //@todo : imposer la langue du destinataire
	
	$niveau = $_GET["level"];
	$idbilling_buyer = $_GET["idbilling_buyer"];
	
	$query = "SELECT idbuyer, idcontact
			FROM billing_buyer
			WHERE idbilling_buyer = $idbilling_buyer";
	$rs = & DBUtil::query( $query );
	
	if($rs===false)
		die("Impossible de récupérer les informations du contact");
		
	$idbuyer = $rs->fields("idbuyer");
	$idcontact = $rs->fields("idcontact");
	
	//Débité factor ?
	$querydf = "SELECT factor_refusal_date
				FROM billing_buyer
				WHERE idbilling_buyer = $idbilling_buyer";
	$rsdf = & DBUtil::query( $querydf );
	
	if( $rsdf === false )
		die( "Impossible de savoir si la facture $idbilling_buyer est débitée par le factor" );
		
	$factor_refusal_date = $rsdf->fields("factor_refusal_date");
	$is_debited = false;
	
	if( $factor_refusal_date != "0000-00-00" )
		$is_debited = true;
	
	if( $is_debited ){
		
		switch( $niveau ){
			case "1":		$editor->setTemplate( "INVOICE_RELAUNCH_FACTOR_1" );break;
			case "2":		$editor->setTemplate( "INVOICE_RELAUNCH_FACTOR_2" );break;
			case "3":		$editor->setTemplate( "INVOICE_RELAUNCH_FACTOR_3" );break;
		}
		
	}else{
		
		switch( $niveau ){
			case "1":		$editor->setTemplate( "INVOICE_RELAUNCH_1" );break;
			case "2":		$editor->setTemplate( "INVOICE_RELAUNCH_2" );break;
			case "3":		$editor->setTemplate( "INVOICE_RELAUNCH_3" );break;
		}
		
	}

	$editor->setUseInvoiceTags( $idbilling_buyer );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $idbuyer, $idcontact );
	$editor->setUseCommercialTags( User::getInstance()->getId() );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();

	?>
				<b>Sujet&nbsp;: </b><?php echo $Subject ?>
				<br />
				<br />
				<b>Pièce jointe&nbsp;: </b><a href="<?=$GLOBAL_START_URL?>/catalog/pdf_invoice.php?idinvoice=<?=$idbilling_buyer?>">PDF facture n°<?php echo $idbilling_buyer ?></a>
				<br />
				<br />
				<b>Message&nbsp;:</b>
				<div style="border:1px solid #AAAAAA; margin:10px 0; padding:5px;"><?php echo $Message ?></div>
				<div class="submitButtonContainer">
					<?php if( isset( $_GET[ "source" ] ) && $_GET[ "source" ] == "comment" ){ ?><input type="button" class="blueButton" value="Retour au suivi des relances" onclick="history.go(-1)" /><?php } ?>
					<input type="button" class="blueButton" name="close" value="Fermer la fenêtre" onclick="window.close();" />
				</div>
			</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>			

<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

?>