<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Reglement client partiel
 */


// traitement Majax pour mettre le accept à 1, en gros pour dire que le règlement est bien arrivé à la maison

include_once( "../objects/classes.php" );

if( !isset( $_POST[ "partialPayments" ] ) ){
		
	exit("Une erreur est survenue lors du traitement !");
			
}

$idregulations_buyer = $_POST[ "partialPayments" ];

$today = date("Y-m-d");

for( $i = 0 ; $i < count($idregulations_buyer) ; $i++ ){
	
	// requequette de mise à jour
	DBUtil::query("UPDATE regulations_buyer SET accept=1 WHERE idregulations_buyer = ".$idregulations_buyer[$i] );
	
	// on matte si la ou les factures pour ce règlement sont payées et on les met à 'Paid' si besoin
	$factures = DBUtil::query("	SELECT brb.idbilling_buyer, bb.status 
								FROM billing_regulations_buyer brb, billing_buyer bb
								WHERE brb.idregulations_buyer = ".$idregulations_buyer[$i]);
	
	if($factures->RecordCount()){
		
		while(!$factures->EOF){
			$invoice = new Invoice($factures->fields('idbilling_buyer'));
			
			$solde = $invoice->getBalanceOutstanding();
		
			if(round($solde,2) == 0){
				$invoice->set( "status", 'Paid' );
				$invoice->save();
			}
		
			$factures->moveNext();
		}
	}
}

exit('success');
?>