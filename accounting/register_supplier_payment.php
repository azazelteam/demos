<?php

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/objects/RegulationsSupplier.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );

$banner = "no";

//ajax nouvelle ligne de règlement
if( isset( $_GET[ "getRegulationRow" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "idrow" ] ) || !intval( $_GET[ "idrow" ] ) || !isset( $_GET[ "invoices" ] ) )
		exit( "0" );
	
	getRegulationRow( $_GET[ "invoices" ], $_GET[ "supplier" ],intval( $_GET[ "idrow" ] ));
	
	exit();
	
}

if( isset( $_GET[ "creditBalanceFor" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	echo DBUtil::getDBValue( "credit_balance", "supplier", "idsupplier", intval( $_GET[ "creditBalanceFor" ] ) );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------
//initialisation des variables

$billings = $_REQUEST[ "billings" ];
$credits = $_REQUEST[ "credits" ];

$creds = strlen($credits) ? explode( ",", $credits ) : array();
$bilou = explode( ",", $billings );
$bil = array();

for($i=0;$i<count($bilou);$i++){
	$bil[] = $bilou[$i];
}

$cred = array();
for($i=0;$i<count($creds);$i++){
	$cred[] = $creds[$i];
}

$payment_count = isset( $_POST[ "payment_count" ] ) && intval( $_POST[ "payment_count" ] ) ? intval( $_POST[ "payment_count" ] ) : 1;
$idsupplier = isset( $_POST[ "supplier" ] ) ? $_POST[ "supplier" ] : $_GET[ "supplier" ];
$credit_balance = DBUtil::getDBValue( "credit_balance", "supplier", "idsupplier", $idsupplier );

if(isset($_GET['proforma']))
	$title = count( $bil ) > 1 ? "Factures proforma : " . implode( " - ", $bil ) : "Facture proforma : $billings";
else
	$title = count( $bil ) > 1 ? "Factures : " . implode( " - ", $bil ) : "Facture : $billings";
	
//------------------------------------------------------------------------------------------------
//Traitement du formulaire en cas de paiement par crédit
if(isset( $_REQUEST[ "registerByCredit" ] )){
	
	$payment_count = isset( $_POST[ "payment_count" ] ) ? $_POST[ "payment_count" ] : 1;
	$total_credit_used			= 0;
	
	//On parcourt tous les règlements enregistrés
	for( $i = 1 ; $i <= $payment_count ; $i++ ){
		
		foreach( $bil as $key => $idinvoice ){
			
			$credit_used = isset( $_POST[ "credit_used_".$key."_$i" ] ) && $_POST[ "credit_used_".$key."_$i" ] > 0 ? $_POST[ "credit_used_".$key."_$i" ] : 0.0;
			$total_credit_used += $credit_used;
				
			if( $credit_used > 0 ){
				DBUtil::query( "UPDATE billing_supplier 
								SET credit_amount = credit_amount + $credit_used 
								WHERE billing_supplier = '$idinvoice' 
								LIMIT 1" );
				
				$invoicesToUpdate[] = $idinvoice;
			}
			
		}
		
	}
	
	if( $total_credit_used > 0 )
		DBUtil::query( "UPDATE supplier SET credit_balance = credit_balance - $total_credit_used WHERE idsupplier = $idsupplier" );
	
	UpdateBillingsStatus( $invoicesToUpdate, $idsupplier );
	
	exit();
}

//------------------------------------------------------------------------------------------------
//Traitement du formulaire
if ( isset( $_REQUEST[ "register" ] ) || isset( $_REQUEST[ "register_x" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	$invoicesToUpdate = array();
	
	$overpayment_tolerance = DBUtil::getParameterAdmin( "solde_tolerance_supplier" );
	$overpayment_max_tolerance = DBUtil::getParameterAdmin( "max_solde_tolerance_supplier" );
			
	$credit_balance = DBUtil::getDBValue( "credit_balance", "supplier", "idsupplier", $idsupplier );
	
	$payment_count = isset( $_POST[ "payment_count" ] ) ? $_POST[ "payment_count" ] : 1;
		
	$total_avoirs = 0;
	
	$creditTable = array();

	foreach( $cred as $key => $idcredit ){
		

		// montant
		$avoirAmount = DBUtil::query("	SELECT amount - ifnull(( SELECT SUM(amount) FROM credit_supplier_regulation WHERE idcredit_supplier = '$idcredit' ),0) as restAmount
										FROM credit_supplier 
										WHERE idcredit_supplier = '$idcredit' 
										LIMIT 1")->fields('restAmount');			
		
		$total_avoirs += $avoirAmount;
		$avoirAmountTotal += $avoirAmount;
		
		$creditTable[ $idcredit ] = $avoirAmount;
				
	}

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! REGLEMENTS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		
	//On parcourt tous les règlements enregistrés
	for( $i = 1 ; $i <= $payment_count ; $i++ ){
		
		$total_amount_regulation	= 0;
		$total_credit_used			= 0;
		$total_solde				= 0;
		$accept						= 1; //OSEF ! Normalement, c'est plus utilisé. Je le mets par acquis de bonne conscience parce que ça fait joli mais faut pas l'utiliser, c'est le rapprochement bancaire qui doit être utilisé à la place
		
		$regulationsToCreate = array();
		
		//Date de paiement
		if( !isset( $_POST[ "payment_date_$i" ] ) )
			exit( "Aucune date de paiement renseignée" );
		
		$payment_date = $_POST[ "payment_date_$i" ];
		
		if( $payment_date == "" || strlen( $payment_date ) != 10 || $payment_date == "00-00-0000" )
			exit( "La date de paiement saisie est erronnée !" );
		
		//Date d'échéance
		if( !isset( $_POST[ "deliv_payment_$i" ] ) )
			exit( "Aucune date d'échéance renseignée" );
		
		$deliv_payment = $_POST[ "deliv_payment_$i" ];
		
		//Mode de paiement
		if( !isset( $_POST[ "idpayment_$i" ] ) )
			exit( "Aucun mode de paiement renseigné" );
		
		$idpayment = $_POST[ "idpayment_$i" ];
						
		//Numéro de pièce
		$piece_number = isset( $_POST[ "piece_number_$i" ] ) ? $_POST[ "piece_number_$i" ] : "";
		
		//Commentaires
		$comment = "";
		
		if( $piece_number != "" )
			$comment .= "$piece_number ";
				
		$comment .= $_POST[ "comment_$i" ];
		
		//On parcourt toutes les factures
		$j = 0;
		$k = 0;
		foreach( $bil as $key => $idinvoice ){
			
			$invoice = new SupplierInvoice( $idinvoice , $idsupplier);
			
			$amount = floatval( $_POST[ "amount_".$key."_$i" ] );
			$total_amount_regulation += $amount;
			
			//Escompte
					
			if( isset( $_POST[ "rebate_amount_$key" ] ) ){
				
				//on enlève l'escompte de la facture
				$invoice->set( "rebate_type", 'amount' );
				$invoice->set( "rebate_value", $_POST[ "rebate_amount_$key" ] );
				
				$rebate = 0.0;
				
				$invoice->save();
				
			}
			
			$invoice = new SupplierInvoice( $idinvoice , $idsupplier);
			
			$solde = SupplierInvoice::getBalanceOutstanding( $idinvoice , $idsupplier );
			
			$rebate = $invoice->get( "rebate_type" ) == "amount" ? $invoice->get( "rebate_value" ) : $invoice->get('total_amount') * $invoice->get( "rebate_value" ) / 100.0;
			
			$total_solde += $solde;
			
			
			// credit fournisseur
			$credit_used = isset( $_POST[ "credit_used_".$key."_$i" ] ) && $_POST[ "credit_used_".$key."_$i" ] > 0 ? $_POST[ "credit_used_".$key."_$i" ] : 0.0;
			$total_credit_used += $credit_used;
			
			if( $credit_used > 0 || $_POST[ "amount_".$key."_$i" ] > 0 || ( isset( $_POST[ "maxToleranceAccepted" ] ) && $_POST[ "maxToleranceAccepted" ] )){
				
				$regulationsToCreate[ $k ][ "idbilling" ] = $idinvoice;
				$regulationsToCreate[ $k ][ "amount" ] = $amount > $solde ? $solde : $amount;
				$regulationsToCreate[ $k ][ "rebate_amount" ]	= $rebate;
				
				$k++;
			}

			if( $credit_used > 0 )
				DBUtil::query( "UPDATE billing_supplier SET credit_amount = credit_amount + $credit_used WHERE billing_supplier = '$idinvoice' LIMIT 1" );
			
			if( $amount + $credit_used >= $solde && !in_array( $idinvoice, $invoicesToUpdate ) )
				$invoicesToUpdate[] = $idinvoice;
						
			$j++;
			
		}
		
		//si le tout est dans la tolerance, on passe les factures au statut payé et on onregistre la différence dans le reglement
		//$overpayment_tolerance : tolérance acceptée automatiquement
		//$overpayment_max_tolerance : tolérance acceptée/refusée par l'utilisateur. On sait s'il a acceptée via $_POST[ "maxToleranceAccepted" ]
		$final_solde = $total_amount_regulation + $total_credit_used - $total_solde + $total_avoirs;
		
		$overpayment = 0;
		$maxToleranceAccepted = isset( $_POST[ "maxToleranceAccepted" ] ) ? $_POST[ "maxToleranceAccepted" ] : 0 ;
		
		//si la tolérance est acceptée par l'utilisateur ou qu'on est dans la fourchette automatique
		if( abs( $final_solde ) <= $overpayment_max_tolerance && ( abs( $final_solde ) <= $overpayment_tolerance || $maxToleranceAccepted == 1 ) ){
			
			$overpayment = $final_solde;
			
			foreach( $bil as $key => $idinvoice ){
				
				if( !in_array( $idinvoice, $invoicesToUpdate ) )
					$invoicesToUpdate[] = $idinvoice;
				
			}
			
		}
		
		$final_solde -= $total_avoirs;
		
		//si le client a trop payé et que ce trop payé n'est pas dans la fourchette de tolérance, on crédite son compte client
		//je pense que c'est faux étant donné qu'il doit falloir faire un avoir mais JFC est affirmatif. donc c'est que je dois me tromper
		if( $final_solde > $overpayment_max_tolerance || $final_solde <= $overpayment_max_tolerance && $final_solde > $overpayment_tolerance && !$maxToleranceAccepted )
			DBUtil::query( "UPDATE supplier SET credit_balance = credit_balance + $final_solde WHERE idsupplier = $idsupplier" );
		
		//on décrémente le crédit fournisseur du montant de trop payé utilisé
		if( $total_credit_used > 0 )
			DBUtil::query( "UPDATE supplier SET credit_balance = credit_balance - $total_credit_used WHERE idsupplier = $idsupplier" );
		
		//On crée le règlement pour toutes les factures
		if(count($regulationsToCreate)){
			$regulationssupplier =& RegulationsSupplier::createRegulationsSupplier( $regulationsToCreate, $idsupplier );
			
			$regulationssupplier->set( "amount", $total_amount_regulation );
			$regulationssupplier->set( "piece_number", $piece_number );
			$regulationssupplier->set( "comment", $comment );
			$regulationssupplier->set( "idpayment", $idpayment );
			$regulationssupplier->set( "payment_date", euDate2us( $payment_date ) );
			$regulationssupplier->set( "deliv_payment", euDate2us( $deliv_payment ) );
			$regulationssupplier->set( "accept", $accept );
			$regulationssupplier->set( "overpayment", $overpayment );
			$regulationssupplier->set( "credit_balance_used", $total_credit_used );
							
			$regulationssupplier->update();
		
			$idregulationssupplier = $regulationssupplier->get('idregulations_supplier');
		}
		
	}

	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! AVOIRS !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	if( count($cred) > 0 ){
		
		$pAvoir = 0;
		$pFacture = 0;
		
		//$idusage = DBUtil::query('SELECT ifnull(MAX(idusage),0)+1 as idusage FROM credit_supplier_regulation LIMIT 1')->fields('idusage');
		if(!$idregulationssupplier)
			$idusage = 0;
		else	
			$idusage = $idregulationssupplier;
		
		while( $pFacture < count( $bilou ) && $pAvoir < count( $creditTable ) ){
			
			$facture = new SupplierInvoice( $bilou[ $pFacture ] , $idsupplier );
			$soldeFacture = SupplierInvoice::getBalanceOutstanding( $bilou[ $pFacture ] , $idsupplier );
			
			$montantsAvoirs = array_values( $creditTable );
			$identifiantsAvoirs = array_keys( $creditTable );
			$idAvoir = $identifiantsAvoirs[ $pAvoir ];
			
			$montantAvoir = $montantsAvoirs[ $pAvoir ];
				
			if( $soldeFacture > 0.0 ){
				
				//utilisation partielle de l'avoir
				$montantUtilise = 0.00;
				if( $soldeFacture < $montantAvoir ){
					
					$montantUtilise = $soldeFacture;
					$creditTable[ $idAvoir ] -= $montantUtilise;
					
				}else{ //utilisation complète de l'avoir pour solder la facture	
		
					$montantUtilise = $montantAvoir;
					$creditTable[ $idAvoir ] = 0.0;
				}
				
				//lier la facture et l'avoir
				if(strlen($idAvoir)>0){
				 $query = "
							INSERT INTO `credit_supplier_regulation` ( 
								idcredit_supplier,
								billing_supplier,
								idusage,
								amount,
								`date`
							) VALUES ( 
								'".$idAvoir."',
								'".$bilou[ $pFacture ]."',
								$idusage,
								'".$montantUtilise."',
								NOW()
							)";
				DBUtil::query( $query );
				}
					
			}
			
			if( $creditTable[ $idAvoir ] == 0.0 )
				$pAvoir++;
				
			if( SupplierInvoice::getBalanceOutstanding( $bilou[ $pFacture ] , $idsupplier ) == 0.0 ){
				
				//solder la facture
				DBUtil::query( "UPDATE billing_supplier SET `status` = 'Paid' WHERE billing_supplier = '".$bilou[ $pFacture ]."' " );
				
				$pFacture++;
				
			}
		}
	}
	
	UpdateBillingsStatus( $invoicesToUpdate, $idsupplier );
	
	if( isset( $_POST[ "send_mail_supplier" ] ) && $_POST[ "send_mail_supplier" ] == "1" && $total_amount_regulation > 0 )	
		sendSupplierEmail( $idsupplier , $bilou , $creds , $total_amount_regulation );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript">
<!--
	
	function checkForm(){
		
		var error = false;
		var solde_tolerance = <?php echo DBUtil::getParameterAdmin( "solde_tolerance_supplier" ) ?>;
		var max_solde_tolerance = <?php echo DBUtil::getParameterAdmin( "max_solde_tolerance_supplier" ) ?>;
				
		$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
		$.blockUI.defaults.css.fontSize = '12px';
		$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
		$.blockUI.defaults.growlCSS.color = '#FFFFFF';
				
		//montants renseignés et non nuls
		//on parcourt toutes les lignes de règlements
		var i = 0;
		var total_regulations = parseFloat( $('#regulationsTotalSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ).replace( ' ', '' ) );
		avoirsCpt = <?=count($creds)?>;
		useAvoirs = false;
		if(avoirsCpt > 0 && total_regulations == 0){
			useAvoirs = confirm("Souhaitez vous utiliser que les avoirs ?");
			
			if(useAvoirs == true)
				error = false;
			else{
				$.growlUI( '', "Vous devez saisir le montant du paiement" );
				return false;
			}	
		}
			
		$('#regulationsTable').children('tbody').children('tr').each(function(){
			
			//on vérifie les montants affectés
			var amount = $(this).children('.totalRowAmount').children('input').val();
			
			var nameInput = $(this).children('.totalRowAmount').children('input').attr('name');
			
			var arrayKeys = nameInput.split("_");
			
			var key = arrayKeys[1];
			var row = arrayKeys[2];
			
			var mySolde = parseFloat( $( "#invoiceSolde_" + key ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
			
			var credUsed = $('#credit_used_'+key+'_1').val();
			
			if(amount == ''){
				$.growlUI( '', "Un montant de règlement n'a pas été saisi il ne sera donc pas enregistré" );
				return false;
			}
							
			if( amount == 0 && mySolde != 0 && avoirsCpt == 0){
			
				if( i == 0 ){
					$.growlUI( '', "Vous devez saisir le montant du paiement" ); 
					error = true;
					return false;
				}

			}
			
			
			if( isNaN( amount ) ){
				$.growlUI( '', "Le montant doit être un nombre" );
				error = true;
				return false;
			}
			
			//vérification des informations du règlement
			if( $(this).hasClass('regulation') ){
				
				var idpayment = $(this).children('td').children('.idpayment').val();
				if( idpayment == "" || idpayment == 0 ){
					$.growlUI( '', "Veuillez renseigner le mode de paiement" );
					error = true;
					return false;
				}
				
				var payment_date = $(this).children('td').children('.paymentDate').val();
				if( payment_date == "" ){
					$.growlUI( '', "Veuillez renseigner la date de paiement" );
					error = true;
					return false;
				}
				
				var deliv_payment = $(this).children('td').children('.delivPayment').val();
				if( deliv_payment == "" ){
					$.growlUI( '', "Veuillez renseigner la date d'échéance du paiement" );
					error = true;
					return false;
				}
				
			}
			
			i++;
		});
		
		if( error )
			return false;
		
		//on s'occupe de la tolérance
		
		var total_solde = parseFloat( $('#invoicesTotalSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		
		var overpayment = Math.abs( Math.round( ( total_regulations - total_solde ) * 100.0 ) / 100.0 );

		if( overpayment <= max_solde_tolerance && overpayment > solde_tolerance && !useAvoirs){
			
			if( Math.round( ( total_regulations - total_solde ) * 100.0 ) / 100.0  > 0){
				$('#popupAcceptToleranceMessage').html("Le trop payé s'élève à " + overpayment + " ¤, enregister le règlement sans mettre le solde sur le compte fournisseur ?");
			}else{
				$('#popupAcceptToleranceMessage').html("La somme des règlements couvre le solde de la facture à " + overpayment + " ¤ près, la considérer comme soldée ?");
			}
			
			$.blockUI({
				message: $('#popupAcceptTolerance'),
				css: {
					fontFamily: 'arial, "lucida console", sans-serif',
					fontSize: '12px',
					height: '100px',
					width: '400px',
					left: '50%',
					'margin-left': '-200px'
				}
			});
			
			return false;
		}
		
		return true;
	}
	
	//------------------------------------------------------------------------------------
	
	$(document).ready(function(){
		
		//Plug-in jQuery URLEncode
		$.extend({
			URLEncode:function(c){
				var o='';
				var x=0;
				c=c.toString();
				var r=/(^[a-zA-Z0-9_.]*)/;
				while(x<c.length){
					var m=r.exec(c.substr(x));
					if(m!=null && m.length>1 && m[1]!=''){
						o+=m[1];
						x+=m[1].length;
					}else{
						if(c[x]==' ')
							o+='+';
						else{
							var d=c.charCodeAt(x);
							var h=d.toString(16);
							o+='%'+(h.length<2?'0':'')+h.toUpperCase();
						}
						x++;
					}
				}
				return o;
			},
			URLDecode:function(s){
				var o=s;
				var binVal,t;
				var r=/(%[^%]{2})/;
				while((m=r.exec(o))!=null && m.length>1 && m[1]!=''){
					b=parseInt(m[1].substr(1),16);
					t=String.fromCharCode(b);
					o=o.replace(m[1],t);
				}
				return o;
			}
		});
		
		var options = {
			beforeSubmit: checkForm,
			success: function( response ){
				if( response.length == 0 ){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#000';
	        		$.growlUI( '', 'Paiement réalisé avec succès !' );
					
					//on reload la maman et on ferme la popup
					window.opener.goForm();       

					setTimeout('self.close();',2000); 
	       		}else{
	       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
					$.blockUI.defaults.growlCSS.color = '#FFFFFF';
				    $.growlUI( '', " Impossible d'enregistrer les modifications <br />" + response );
				}
		    }
		};
	
		$('#frm').ajaxForm(options);	
		
	});
	//------------------------------------------------------------------------------------
	// fonction pour poster directement en cas d'overpayment accepté ou pas
	function directPostForm(){
	
		var myoptions = {
			url : "<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_payment.php?register&supplier=<?php echo $idsupplier ?>&amp;billings=<?php echo $billings ?>",
			success: function( response ){
				if( response.length == 0 ){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#000';
	        		$.growlUI( '', 'Paiement réalisé avec succès !' );
					
					//on reload la maman et on ferme la popup
					window.opener.goForm();       

					setTimeout('self.close();',2000); 
	       		}else{
	       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
					$.blockUI.defaults.growlCSS.color = '#FFFFFF';
				    $.growlUI( '', " Impossible d'enregistrer les modifications <br />" + response );
				}
		    }
		};
		
		$('#frm').ajaxSubmit(myoptions);
	}
	
	//------------------------------------------------------------------------------------
	
	function registerByCredit(){
	
		var myoptions = {
			url : "<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_payment.php?registerByCredit&supplier=<?php echo $idsupplier ?>&amp;billings=<?php echo $billings ?>",
			success: function( response ){
				if( response.length == 0 ){
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#000';
	        		$.growlUI( '', "Facture(s) soldée(s) par le trop payé !" );
					
					//on reload la maman et on ferme la popup
					window.opener.goForm();       

					setTimeout('self.close();',2000); 
	       		}else{
	       			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
					$.blockUI.defaults.growlCSS.backgroundColor = '#FF0000';
					$.blockUI.defaults.growlCSS.color = '#FFFFFF';
				    $.growlUI( '', " Impossible d'enregistrer les modifications <br />" + response );
				}
		    }
		};
		
		$('#frm').ajaxSubmit(myoptions);
		
	}
	
	//------------------------------------------------------------------------------------
	
	function showRegulations( div ){
		
		$.blockUI({
			message: $('#'+div),
			fadeIn: 700, 
    		fadeOut: 700,
			css: {
				width: '700px',
				top: '0px',
				left: '50%',
				'margin-left': '-350px',
				'margin-top': '50px',
				padding: '5px', 
				cursor: 'help',
				'-webkit-border-radius': '10px', 
                '-moz-border-radius': '10px',
                'background-color': '#FFFFFF',
                'font-size': '11px',
                'font-family': 'Arial, Helvetica, sans-serif',
                'color': '#44474E'
			 }
		}); 
		
		$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
				
	}
	
	//------------------------------------------------------------------------------------
	
	function addBank(){ 
		
		$.blockUI({
			message: $('#bankForm'),
			css: {
				fontFamily: 'arial, "lucida console", sans-serif',
				fontSize: '12px',
				height: '100px',
				width: '300px'
			}
		}); 
		
	}
	
	//------------------------------------------------------------------------------------
	
	function addRegulationRow(){
		
		var table = $('#regulationsTable');
		var idrow = table.children('tbody').children('tr').children('td:eq(6)').children('input').attr('id');
		
		idrow = parseInt( idrow.substr( idrow.indexOf('_',7) + 1 ) ) + 1;
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_payment.php',
			data: 'getRegulationRow&idrow='+idrow+'&supplier=<?=$idsupplier?>&invoices=<?php echo $billings ?>',
			error: function( XMLHttpRequest, errorType, exception ){ alert(exception); },
			success: function( response ){
				if( response == "0" )
					$.growlUI('Une erreur est survenue lors de l\'ajout d\'une nouvelle ligne de règlement','');
				else{
					
					table.children('tbody').append(response);
					if( $('#payment_count').val() == 1 )
						$('#regulationsSubtitle').append('s');
					$('#payment_count').val( parseInt( $('#payment_count').val() ) + 1 );
					
				}
			}
		});
		
	}
	
	function delRegulationRow( image ){
		$(image).parent('td').parent('tr').remove();
	}
	
	//------------------------------------------------------------------------------------
	// Fonction de recalcul des sommes lors d'un changement d'escompte
	
	function updatePaymentSum( rebateValue,type ,idinvoice ){
		
		var invoiceAmount = parseFloat( $( "#invoiceAmount_" + idinvoice ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		
		var rebateAmount = 0.00;
		var rebateRate = 0.00;
		
		if(type=='rate'){
			rebateRate = rebateValue;
			rebateAmount = (invoiceAmount * rebateValue) / 100;
			$( "#rebate_amount_" + idinvoice ).val( Math.round( rebateAmount * 100.0 ) / 100.0 );
		}else{
			rebateAmount = rebateValue;
			rebateRate = ( rebateValue * 100 ) / invoiceAmount;
			$( "#rebate_rate_" + idinvoice ).val( Math.round( rebateRate * 100.0 ) / 100.0 );
		}
		
				
		//solde de la facture
		
		var invoice = parseFloat($( "#invoiceSoldeBase_" + idinvoice ).val());
		invoice -= rebateAmount;
		invoice = Math.round( invoice * 100.0 ) / 100.0;
		
		$( "#invoiceSolde_" + idinvoice ).html( invoice.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		$( "#amount_" + idinvoice + "_1" ).val( invoice.toFixed( 2 ).toString() );
		
		var total = 0;
		$('#invoicesTable').children('tbody').children('tr').each(function(){
			amount = parseFloat( $(this).children('td.invoiceSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
			if( !isNaN( amount ) )
				total += amount;
		});
		
		total = Math.round( total * 100 ) / 100;
		
		$( "#invoicesTotalSolde" ).html( total.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		
		recalculateAmounts();
		
	}
	
	//------------------------------------------------------------------------------------
	//met à jour le montant d'avoir disponible quand on change de payeur (groupements)
	
	function reloadCredBal( idsupplier ){
 		
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_payment.php",
			cache: false,
			type: "GET",
			data: "creditBalanceFor=" + idsupplier,
			error : function(){ $.growlUI( '','Impossible modifier le crédit disponible' ); },
		 	success: function( response ){
				useCredit( false );
				$('#use_credit').attr('checked',false);
				$('#creditBalance').html( parseFloat( response ).toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
				if( parseFloat( response ) > 0 )
					$('#use_credit').parent().parent('div').css('display','block');
				else
					$('#use_credit').parent().parent('div').css('display','none');
			}
		});
		
	}
	
	//------------------------------------------------------------------------------------
	//recalcule le total des règlements
	
	function recalculateAmounts(){
				
		var total = 0;
		
		$('#regulationsTable').children('tbody').children('tr').each(function(){
			amount = parseFloat( $(this).children('td.totalRowAmount').children('input').val() );
			if( !isNaN( amount ) )
				total += amount;
		});
		
		total = Math.round( total * 100 ) / 100;
		
		$('#regulationsTotalSolde').html( total.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );

		useCredit( $('#use_credit').attr('checked') );
		

		myTotal = total - parseFloat( $('#invoicesTotalSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) ) + parseFloat( $('#credit_balance_used').val() );	
				
		$('#regulationsTotalSoldeOver').html( myTotal.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );

		var myFinalTotal = parseFloat( $('#credit_balance_used').val() ) + parseFloat( $('#regulationsTotalSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );	
		
		$('#regulationsFinalTotalSolde').html( myFinalTotal.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		
		var st = <?php echo DBUtil::getParameterAdmin( "solde_tolerance_supplier" ) ?>;
		var mst = <?php echo DBUtil::getParameterAdmin( "max_solde_tolerance_supplier" ) ?>; // mst lol petit cochon va toitoine
		//alert('mytotal : '+myTotal+' myfinaltotal : '+myFinalTotal);
		if( myTotal != parseFloat( $('#invoicesTotalSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) ) && myTotal!=0){
			$('#regulationsTotalSoldeOver').css('background-color','red');
			
			if( Math.abs( myTotal.toFixed( 2 ) ) <=  st){
				$('#regulationsTotalSoldeOverText').html( "Cet écart est accepté les factures seront soldées" );
			}
			
			if( Math.abs( myTotal.toFixed( 2 ) ) > st && Math.abs( myTotal.toFixed( 2 ) ) <=  mst ){
				$('#regulationsTotalSoldeOverText').html( "Cet écart sera accepté si vous le décidez, les factures seront soldées" );
			}
			
			if( myTotal.toFixed( 2 ) >  mst && myTotal.toFixed( 2 ) > 0 ){	
				$('#regulationsTotalSoldeOverText').html( "Cet écart de paiement soldera les factures et créditera le compte fournisseur" );
			}
			
			
			if( myTotal.toFixed( 2 ) < ((-1)*mst) && myTotal.toFixed( 2 ) < 0){
				$('#regulationsTotalSoldeOverText').html( "Cet écart de paiement est trop important, les factures ne seront pas soldées" );
			}
			
		}else{
			$('#regulationsTotalSoldeOver').css('background-color','');
			$('#regulationsTotalSoldeOverText').html( "" );
		}		
		
	}
	
	//------------------------------------------------------------------------------------
	
	function useCredit( checked ){
		/*
		var credit_balance = parseFloat( $( "#creditBalance" ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		var total_solde = parseFloat( $( "#invoicesTotalSolde" ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		i=0;
		$('#invoicesTable').children('tbody').children('tr').each(function(){
			
			if( $(this).children('td').length && $(this).children('td').hasClass('creditAmount') ){
				
				var invoice_solde = parseFloat( $(this).children('.invoiceSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
				var credit_used = parseFloat( $(this).children('.creditAmount').children('input:hidden').val().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
				var invoice_credit = parseFloat( $(this).children('.creditAmount').children('.creditInnerAmount').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
				
				if( isNaN( invoice_credit ) )
					invoice_credit = 0;
				
				if( checked ){
					
					if( credit_balance > invoice_solde ){
						
						credit_balance -= invoice_solde;
						total_solde -= invoice_solde;
						invoice_credit += invoice_solde;
						credit_used += invoice_solde;
						invoice_solde = 0;
						
					}
					else{
						
						invoice_solde -= credit_balance;
						total_solde -= credit_balance;
						invoice_credit += credit_balance;
						credit_used += credit_balance;
						credit_balance = 0;
						
					}
					
					if( !$(this).children('.creditAmount').hasClass('usedCreditAmount') )
						$(this).children('.creditAmount').toggleClass('usedCreditAmount');
					
				}
				else{
					
					invoice_solde += credit_used;
					total_solde += credit_used;
					invoice_credit -= credit_used;
					credit_balance += credit_used;
					credit_used = 0;
					
					if( $(this).children('.creditAmount').hasClass('usedCreditAmount') )
						$(this).children('.creditAmount').toggleClass('usedCreditAmount');
					
				}
				
				invoice_solde = Math.round( invoice_solde * 100 ) / 100;
				invoice_credit = Math.round( invoice_credit * 100 ) / 100;
				credit_used = Math.round( credit_used * 100 ) / 100;
				
				if( invoice_credit == 0 )
					invoice_credit = '-';
				else
					invoice_credit = invoice_credit.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;";
				
				$(this).children('.invoiceSolde').html( invoice_solde.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
				$(this).children('.creditAmount').children('.creditInnerAmount').html( invoice_credit );
				$(this).children('.creditAmount').children('input:hidden').val( credit_used.toFixed( 2 ).toString() );
				$( "#amount_" + i + "_1" ).val( invoice_solde.toFixed( 2 ).toString() );
				
				i++;
			}
		});
		
		total_solde = Math.round( total_solde * 100 ) / 100;
		credit_balance = Math.round( credit_balance * 100 ) / 100;
		
		$('#invoicesTotalSolde').html( total_solde.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		$('#creditBalance').html( credit_balance.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		var total_regulations = parseFloat( $('#regulationsTotalSolde').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		recalculateAmounts();
		
		if(total_solde == 0.00){
			$('#regulationsTotalSoldeOverText').html( "Les factures seront soldées par le crédit fournisseur" );
			$('#registerByCred').css('display','block');
			$('#register').css('display','none');
		}else{
			$('#regulationsTotalSoldeOverText').html( "" );
			$('#registerByCred').css('display','none');
			$('#register').css('display','block');
		}
		*/

		var credit_balance = parseFloat( $( "#creditBalance" ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		var total_solde = parseFloat( $( "#invoicesTotalSolde" ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		var regulationsTotalSolde = parseFloat( $( "#regulationsTotalSolde" ).html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
		
		var newCredit = 0.00;
		
		if( checked ){
		
			if( total_solde > 0 && total_solde > regulationsTotalSolde ){
			
				var cbu = total_solde - regulationsTotalSolde ; // cbu -> pas le sandwich mcDo mais Credit Balance Used
				
				if( cbu >= credit_balance){
					cbu = credit_balance;
				}
				var oldCbu = parseFloat($('#credit_balance_used').val());

				newCredit = credit_balance + (oldCbu - cbu);
				
			}else{
			
				alert( "Vous ne pouvez pas utiliser le trop payé !" );
				$('#use_credit').attr('checked', false);
				
				var cbu = 0 ; // la blague est au dessus
				var oldCbu = parseFloat($('#credit_balance_used').val());
				
				newCredit = credit_balance + oldCbu;
				
			}
			
		}else{
			
			var cbu = 0 ; // la blague est au dessus
			var oldCbu = parseFloat($('#credit_balance_used').val());
			
			newCredit = credit_balance + oldCbu;		
			
		}
		
		$('#credit_balance_used').val( cbu );
		$('#creditUsed').html( cbu.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;"  );
		$( "#creditBalance" ).html( newCredit.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
		
	}
	
	//------------------------------------------------------------------------------------
	
-->
</script>
<style type="text/css">
<!--
	
	div#global{
		min-width:0;
		width:auto;
	}
	
	div.mainContent div.content div.subContent table.dataTable td.creditAmount{
		text-align:center;
	}
	
	div.mainContent div.content div.subContent table.dataTable td.usedCreditAmount{
		text-align:right;
	}
	
-->
</style>
<div id="popupAcceptTolerance" style="display:none;">
	<div id="popupAcceptToleranceMessage" style="text-align:center;font-weight:bold;margin-top:10px;"></div>
	<div style="text-align:center;margin-top:20px;">
		<input type="button" class="blueButton" style="margin-right:5px;" value="Oui" onclick="$('#maxToleranceAccepted').val('1');directPostForm();$.unblockUI()" />
		<input type="button" class="blueButton" style="margin-left:5px;" value="Non" onclick="$('#maxToleranceAccepted').val('0');directPostForm();$.unblockUI()" />
		<input type="button" class="blueButton" style="background-color:red;margin-left:10px;" Value="Annuler" onclick="$.unblockUI();return false;" />
	</div>
	
</div>
<div id="globalMainContent" style="margin-top:-25px; width:930px;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form id="frm" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/register_supplier_payment.php?supplier=<?php echo $idsupplier ?>&amp;billings=<?php echo $billings ?>&amp;credits=<?php echo $credits ?>">
		<div class="mainContent fullWidthContent" style="width:930px;">
			<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle" style="height:45px; line-height:20px;">
					<p class="title">
						Enregistrement du paiement<br />
						<?php echo $title ?>
					</p>
				</div>
				<div class="subContent">
					<input type="hidden" name="supplier" value="<?php echo $idsupplier ?>" />
					<input type="hidden" name="billings" value="<?php echo $billings ?>" />
					<input type="hidden" name="credits" value="<?php echo $credits ?>" />
					<input type="hidden" name="send_mail_supplier" id="send_mail_supplier" value="0" />
					<input type="hidden" name="payment_count" id="payment_count" value="1" />
					<input type="hidden" name="maxToleranceAccepted" id="maxToleranceAccepted" value="0" />

					<div style="margin-bottom:10px;">Fournisseur n°<?php echo $idsupplier ?> - <?php echo getsupplierName( $idsupplier ) ?></div>

					<div class="clear"></div>
					<div class="subTitleContainer" style="margin-left:23px;">
						<p class="subTitle">Informations facture<?php echo strstr( $billings, "," ) ? "s" : "" ?><?=isset($_GET['proforma'])?" proforma" : "" ?></p>
					</div>
					<?php $total_solde = listInvoices( $bil , $idsupplier , $cred ); ?>
					<div class="subTitleContainer" style="margin-left:23px;">
						<p id="regulationsSubtitle" class="subTitle">Informations paiement</p>
					</div>
					<?php displayRegulations( $bil , $idsupplier ); ?>
					<div class="submitButtonContainer" style="float:right;">
						<input type="submit" id="register" name="register" value="Enregistrer" class="blueButton" onclick="if( confirm( 'Envoyer un Email au fournisseur ?' )){$('#send_mail_supplier').val('1'); }"/>
						<input type="button" onclick="registerByCredit();" id="registerByCred" name="registerByCred" value="Enregistrer" class="blueButton" style="display:none;"/>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div class="bottomRight"></div><div class="bottomLeft"></div>
		</div>
	</form>
	<?
	if($total_solde<0){
		?>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#dialogErrorAvoir").dialog( 	{ draggable: false,
												  modal: true,
												  resizable: false,
												  close: function(event, ui) { self.close(); }
												} );
			});
		</script>
		<div id="dialogErrorAvoir" title="Avoir trop important" style="display:none;">
			Le montant de l'avoir est supérieur à la somme des soldes des factures.<br /><br />
			L'avoir n'est donc pas utilisable.<br />
			<p style="text-align:center;width:100;margin-top:20px;">
				<input type="button" class="blueButton" style="background-color:red;" value="Quitter" onclick="$('#dialogErrorAvoir').dialog( 'close' );" />
			</p>
		</div>
		<?
	}
	?>
</div>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------------

function listInvoices( $invoices , $idsupplier , $cred ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );
	
	
?>
<div class="tableContainer">
	<table id="invoicesTable" class="dataTable">
		<thead>
			<tr>
				<th class="filledCell" style="text-align:center; width:70px;"><?=isset($_GET['proforma'])?" Proforma" : "Facture" ?> n°</th>
				<th class="filledCell" style="text-align:center; width:70px;">Date facture</th>
				<th class="filledCell" style="text-align:center; width:11%;">TTC facture</th>
				<th class="filledCell" style="text-align:center; width:11%;">Réglé</th>
				<th class="filledCell" style="text-align:center; width:11%;">Acompte</th>
				<!--<th class="filledCell" style="text-align:center; width:15%;">Trop payé utilisé</th>-->
				<th class="filledCell" style="text-align:center; width:15%;" colspan="2">Escompte</th>
				<th class="filledCell" style="text-align:center; width:65px;">Solde TTC</th>
			</tr>
		</thead>
		<tbody>
<?php
	
	$totalSolde = 0.0;
	$avoirTotal = getCreditTotal( $idsupplier , $cred );
	
	foreach( $invoices as $key => $idinvoice ){
		
		$invoice = new SupplierInvoice( $idinvoice , $idsupplier);
		
		//règlements effectués
		
		$query = "
		SELECT rb.idregulations_supplier, 
			rb.amount,
			brb.amount as detailledAmount, 
			rb.comment, 
			rb.idpayment, 
			rb.payment_date, 
			rb.deliv_payment, 
			rb.DateHeure, 
			rb.bank_date, 
			rb.accept,
			p.name" . User::getInstance()->getLang() . " AS payment
		FROM billing_regulations_supplier brb, regulations_supplier rb, billing_supplier bb, payment p
		WHERE rb.idregulations_supplier = brb.idregulations_supplier
		AND bb.billing_supplier = brb.billing_supplier
		AND brb.billing_supplier = '$idinvoice'
		AND from_down_payment = 0
		AND rb.idpayment = p.idpayment
		ORDER BY idregulations_supplier ASC";
	  	
		$rs =& DBUtil::query( $query );
	  	
		$paid = $invoice->get( "total_amount" ) - SupplierInvoice::getBalanceOutstanding( $idinvoice, $idsupplier );
		/*
		while( !$rs->EOF() ){
			
			$paid += $rs->fields( "detailledAmount" );
			
			$rs->MoveNext();
			
		}*/
		
		//acompte
		$down_payment = 0.0;
		if( $invoice->get( "down_payment_value" ) > 0.0 )
			$down_payment = $invoice->get( "down_payment_type" ) == "amount" ? $invoice->get( "down_payment_value" ) : $invoice->get('total_amount') * $invoice->get( "down_payment_value" ) / 100.0;
		
		$solde = SupplierInvoice::getBalanceOutstanding( $idinvoice , $idsupplier );
		$totalSolde += $solde;

					
		$rebate_rate 	= $invoice->get( "rebate_type" ) == "rate" ? $invoice->get( "rebate_value" ) : round( $invoice->get( "rebate_value" ) * 100.0 / $invoice->get('total_amount') ,2 );
	    $rebate_amount 	= $invoice->get( "rebate_type" ) == "amount" ? $invoice->get( "rebate_value" ) : round( $invoice->get('total_amount') * $invoice->get( "rebate_value" ) / 100.0 , 2);

		$soldeBase = $solde + $rebate_amount;
		/*
		if( $avoirTotal >= $soldeBase){
			$avoirAmount = $soldeBase;
		}else{
			$avoirAmount = $avoirTotal;
		}
		
		$avoirTotal -= $avoirAmount;
		*/
		?>
			<tr>
				<td style="text-align:center;"><input type="hidden" id="invoiceSoldeBase_<?php echo $key ?>" value="<?php echo $soldeBase ?>" /><?php echo $idinvoice ?></td>
				<td style="text-align:center;"><?php echo Util::dateFormatEu( substr( $invoice->get( "Date" ), 0, 10 ) ) ?></td>
				<td style="text-align:right;" id="invoiceAmount_<?=$key?>"><?php echo Util::priceFormat( $invoice->get( "total_amount" ) ) ?></td>
				<td style="text-align:right;">
				<?php /*if( $paid > 0 ){ ?>
					<a href="#" class="blueLink" onclick="showRegulations('regle_<?php echo $key ?>'); return false;"><?php echo Util::priceFormat( $paid ) ?></a>
					<div id="regle_<?php echo $key ?>" style="display:none;">
						<table style="padding: 0px;border-spacing:0px;border-collapse:collapse;width:100%;">
							<tr style="background-color:#EAEAEA;">
								<th style="width:80px;border:1px solid #CBCBCB;">Num reglement</th>
								<th style="width:85px;border:1px solid #CBCBCB;">Date de paiement</th>
								<th style="width:75px;border:1px solid #CBCBCB;">Montant règlé</th>
								<th style="width:100px;border:1px solid #CBCBCB;">Total du reglement</th>
								<th style="width:100px;border:1px solid #CBCBCB;">Mode de règlement</th>
								<th style="border:1px solid #CBCBCB;">Commentaire</th>
							</tr>
						<?php $rs->MoveFirst(); ?>
						<?php while( !$rs->EOF() ){ ?>
							<tr>
								<td style="border:1px solid #CBCBCB;"><?php echo $rs->fields( "idregulations_supplier" ) ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo Util::dateFormatEu( $rs->fields( "payment_date" ) ) ?></td>
								<td style="border:1px solid #CBCBCB; text-align:right;"><?php echo Util::priceFormat( $rs->fields( "detailledAmount" ) ) ?></td>
								<td style="border:1px solid #CBCBCB; text-align:right;"><?php echo Util::priceFormat( $rs->fields("amount" ) ) ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo $rs->fields( "payment" ) ?></td>
								<td style="border:1px solid #CBCBCB;"><?php echo stripslashes( $rs->fields( "comment" ) ) ?></td>
							</tr>
							<?php $rs->MoveNext(); ?>
						 <?php } ?>
						</table>
						<input type="button" value="Fermer" onclick="$.unblockUI();" class="blueButton" style="margin-bottom:5px; margin-top:15px;" />
					</div>
				<?php }else{ 
					*/
					?><?php echo Util::priceFormat( $paid ) ?><?php /*}*/ ?>
				</td>
				<?php echo $down_payment > 0 ? '<td style="text-align:right;">' . Util::priceFormat( $down_payment ) . "</td>" : '<td style="text-align:center;">-</td>' ?>
				<!--
				<td class="creditAmount<?php echo $invoice->get( "credit_amount" ) > 0 ? " usedCreditAmount" : "" ?>">
					<span class="creditInnerAmount"><?php echo $invoice->get( "credit_amount" ) > 0 ? Util::priceFormat( $invoice->get( "credit_amount" ) ) : "-" ?></span>
					<input type="hidden" name="credit_used_<?php echo $key ?>_1" value="0" />
				</td>
				-->
				<td style="text-align:center;">
					<!--
					<label><input onclick="updatePaymentSum( <?php echo $rebate_amount ?>, !this.checked, '<?php echo $key ?>' );" type="checkbox" checked="checked" name="rebate_<?php echo $key ?>" value="<?php echo $rebate_rate ?>" style="vertical-align:middle;" />
					<?php echo Util::rateFormat( $rebate_rate ) ?></label>
					<input type="hidden" name="avoirAmount_<?php echo $key ?>" value="<?php echo $avoirAmount ?>" />
					-->
					<input type="text" class="textInput" style="width:30px;" id="rebate_rate_<?php echo $key ?>" name="rebate_rate_<?php echo $key ?>" value="<?=$rebate_rate?>" onkeyup="updatePaymentSum( this.value, 'rate' , '<?php echo $key ?>');" /> %
				</td>
				<td style="text-align:center;">
					<input type="text" class="textInput" style="width:35px;" id="rebate_amount_<?php echo $key ?>" name="rebate_amount_<?php echo $key ?>" value="<?=$rebate_amount?>" onkeyup="updatePaymentSum( this.value, 'amount' , '<?php echo $key ?>');" /> &euro;
				</td>
				
				<td id="invoiceSolde_<?php echo $key ?>" class="invoiceSolde" style="text-align:right;">
					<?php echo Util::priceFormat( $solde ) ?>
				</td>
				
			</tr>
			<?php }
			 
			$totalSolde -= displayCredits( $idsupplier , $cred , $key ); ?>

		</tbody>
		<tfoot>
			<tr>
				<th colspan="7" id="invoicesTotalSoldeText" style="border-style:none;"></th>
				<th id="invoicesTotalSolde" style="text-align:right;"><?php echo Util::priceFormat( $totalSolde ) ?></th>
			</tr>
		</tfoot>
	</table>
	<input type="hidden" id="invoicesTotalSoldeBase" value="<?php echo $totalSolde ?>" />
</div>
<?php
	return $totalSolde;
}

//------------------------------------------------------------------------------------------------------

function displayRegulations( $invoices , $idsupplier , $row = 1 ){
	global $credit_balance;
	?>
	<script type="text/javascript">
		$( document ).ready( function(){ recalculateAmounts(); } );
	</script>
	<div class="tableContainer">
		<table id="regulationsTable" class="dataTable">
			<thead>
				<tr>
					<th class="filledCell" style="text-align:center;">Date de paiement <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Date d'échéance <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Mode de paiement <span class="asterix">*</span></th>
					<th class="filledCell" style="text-align:center;">Pièce n°</th>
					<th class="filledCell" style="text-align:center;">Commentaire</th>
					<th class="filledCell" style="text-align:center;">Facture n°</th>
					<th class="filledCell" style="text-align:center; width:65px;">Montant<?php echo count( $invoices ) > 1 ? " affecté" : "" ?></th>
					<th style="border-style:none; width:17px;"></th>
				</tr>
			</thead>
			<tbody>
				<?php $totalSolde = getRegulationRow( $invoices, $idsupplier, $row ); ?>
			</tbody>
			<tfoot>
				<tr>
					<th colspan="6" style="border-style:none;"></th>
					<th id="regulationsTotalSolde" style="text-align:right;">0,00 ¤</th>
					<th style="border:none;"></th>
				</tr>
				<tr>
					<th colspan="6" style="border-style:none;text-align:right">
						<div style="margin-left:10px;float:right;<?php echo $credit_balance > 0 ? "" : "display:none; " ?>margin-left:10px;height:20px;"><label><input type="checkbox" name="use_credit" id="use_credit" onchange="recalculateAmounts();" style="vertical-align:middle;" /> Utiliser</label></div>
						<div style="font-weight:bold;float:right;height:18px;margin-top:2px;">
								Trop payé disponible : <span id="creditBalance" style="color:red;"><?php echo Util::priceFormat( $credit_balance ) ?></span>
						</div>
					</th>
					<th id="creditUsed" style="text-align:right;">0,00 ¤</th>
					<th style="border:none;"><input type="hidden" value="0" name="credit_balance_used" id="credit_balance_used" /></th>
				</tr>
				<tr>
					<th colspan="6" style="border-style:none;text-align:right;font-weight:bold;">TOTAL</th>
					<th id="regulationsFinalTotalSolde" style="text-align:right;">0,00 ¤</th>
					<th style="border:none;"></th>
				</tr>
				<tr>
					<th id="regulationsTotalSoldeOverText" colspan="6" style="border-style:none;text-align:right;"></th>
					<th id="regulationsTotalSoldeOver" style="text-align:right;">0,00 ¤</th>
					<th style="border:none;"></th>
				</tr>
			</tfoot>
		</table>
	</div>
	<?php
	
}

//------------------------------------------------------------------------------------------------------

function getRegulationRow( $invoices, $idsupplier, $row ){
	
	global $GLOBAL_START_URL, $cred,$bilou;
	
	if( !is_array( $invoices ) )
		$invoices = explode( ',',$invoices );
		
	$rowspan = count( $invoices ) > 1 ? ' rowspan="' . count( $invoices ) . '"' : "";
	
	${"deliv_payment_$row"} = usDate2eu( getInvoiceDelivPayment( $invoices[ 0 ] ) );
	$idPayment = getInvoiceIdPaymentSupplier( $invoices[ 0 ] ) ;
	
	${"payment_date_$row"} = date( "d-m-Y" );
	
	?>
				<tr class="regulation">
					<td<?php echo $rowspan ?> style="text-align:center;">
						<input type="text" name="payment_date_<?php echo $row ?>" id="payment_date_<?php echo $row ?>" value="<?php echo ${"payment_date_$row"} ?>" class="calendarInput paymentDate" />
						<?php DHTMLCalendar::calendar( "payment_date_$row", false ); ?>
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;">
						<input type="text" name="deliv_payment_<?php echo $row ?>" id="deliv_payment_<?php echo $row ?>" value="<?php echo ${"deliv_payment_$row"} ?>" class="calendarInput delivPayment" />
						<?php DHTMLCalendar::calendar( "deliv_payment_$row", false ); ?>
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;"><?php echo paymentList( $row, $idPayment ) ?></td>
					<td<?php echo $rowspan ?> style="text-align:center;">
						<input type="text" name="piece_number_<?php echo $row ?>" id="piece_number_<?php echo $row ?>" value="" class="textInput pieceNumber" style="width:90px;" />
					</td>
					<td<?php echo $rowspan ?> style="text-align:center;"><input type="text" name="comment_<?php echo $row ?>" value="" class="textInput commentField" /></td>
					<?php
													
						$i = 0;
						$totalSolde = 0.0;

						if( count( $cred ) == 0 ){
							foreach( $invoices as $key => $idinvoice ){
								
								if( $row == 1 ){
									
									$invoice = new SupplierInvoice( $idinvoice , $idsupplier);
									$amount = SupplierInvoice::getBalanceOutstanding( $idinvoice , $idsupplier );
									
								}else{
									$amount = 0;
								}
								
								$totalSolde += $amount;
								
								if( $i == 0 ){
									?>
									<td style="text-align:center;"><?php echo $idinvoice ?></td>
									<td class="totalRowAmount" style="text-align:right;"><input type="text" id="amount_<?php echo $key ?>_<?php echo $row ?>" name="amount_<?php echo $key ?>_<?php echo $row ?>" value="<?php echo round( SupplierInvoice::getBalanceOutstanding($idinvoice, $idsupplier), 2 ); ?>" onkeyup="recalculateAmounts();" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;</td>
									<td<?php echo $rowspan ?> style="border-style:none; text-align:center;">
										<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="Ajouter une ligne" onclick="addRegulationRow();" />
										<?php if( $row > 1 ){ ?>
											<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_remove.png" alt="Supprimer la ligne" onclick="delRegulationRow( this );" />
										<?php } ?>
									</td>
									<?php
								}else{	
									?>
									<tr class="onlyInvoice">
										<td style="text-align:center;"><?php echo $idinvoice ?></td>
										<td class="totalRowAmount" style="text-align:right;"><input type="text" id="amount_<?php echo $key ?>_<?php echo $row ?>" name="amount_<?php echo $key ?>_<?php echo $row ?>" value="<?php echo round(SupplierInvoice::getBalanceOutstanding( $idinvoice, $idsupplier ), 2 ); ?>" onkeyup="recalculateAmounts();" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;</td>
									</tr>
									<?php
								}
								
								$i++;
								
							}
						}else{
							
							$pAvoir = 0;
							$pFacture = 0;
							$invoicesBis = array();

							$creditTable = array();
						
							foreach( $cred as $key => $idcredit ){
								
						
								// montant
								$avoirAmount = DBUtil::query("	SELECT amount - ifnull(( SELECT SUM(amount) FROM credit_supplier_regulation WHERE idcredit_supplier = '$idcredit' ),0) as restAmount
																FROM credit_supplier 
																WHERE idcredit_supplier = '$idcredit' 
																LIMIT 1")->fields('restAmount');			

								$creditTable[ $idcredit ] = $avoirAmount;
										
							}
								
							while( $pFacture < count( $bilou ) && $pAvoir < count( $creditTable ) ){
								
								$facture = new SupplierInvoice( $bilou[ $pFacture ] , $idsupplier );
								$soldeFacture = SupplierInvoice::getBalanceOutstanding( $bilou[ $pFacture ] , $idsupplier ) - $invoicesBis[ $pFacture ]['avoir'];
								
								$montantsAvoirs = array_values( $creditTable );
								$identifiantsAvoirs = array_keys( $creditTable );
								$idAvoir = $identifiantsAvoirs[ $pAvoir ];
								
								$montantAvoir = $montantsAvoirs[ $pAvoir ];
									
								if( $soldeFacture > 0.0 ){
									
									//utilisation partielle de l'avoir
									$montantUtilise = 0.00;
									if( $soldeFacture < $montantAvoir ){
										
										$montantUtilise = $soldeFacture;
										$creditTable[ $idAvoir ] -= $montantUtilise;
										
									}else{
							
										$montantUtilise = $montantAvoir;
										$creditTable[ $idAvoir ] = 0.0;
										
									}
									
									//lier la facture et l'avoir
									if( strlen( $idAvoir ) > 0 ){
										$invoicesBis[ $pFacture ]['avoir'] += $montantUtilise;
									}
										
								}

								$newsolde = SupplierInvoice::getBalanceOutstanding( $bilou[ $pFacture ] , $idsupplier ) - $invoicesBis[ $pFacture ]['avoir'];
								
								if( $creditTable[ $idAvoir ] == 0.0 )
									$pAvoir++;
								
								
								if( $newsolde == 0.0 ){
																		
									$pFacture++;
									
								}
								
							}
							
							foreach( $invoices as $key => $idinvoice ){
								$invoice = new SupplierInvoice( $idinvoice , $idsupplier);
								$amount = SupplierInvoice::getBalanceOutstanding( $idinvoice , $idsupplier ) - $invoicesBis[ $key ]['avoir'] ;
								
								$totalSolde += $amount ;
								
								if( $i == 0 ){
									?>
									<td style="text-align:center;"><?php echo $idinvoice ?></td>
									<td class="totalRowAmount" style="text-align:right;"><input type="text" id="amount_<?php echo $key ?>_<?php echo $row ?>" name="amount_<?php echo $key ?>_<?php echo $row ?>" value="<?php echo $amount; ?>" onkeyup="recalculateAmounts();" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;</td>
									<td<?php echo $rowspan ?> style="border-style:none; text-align:center;">
										<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="Ajouter une ligne" onclick="addRegulationRow();" />
										<?php if( $row > 1 ){ ?>
											<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_remove.png" alt="Supprimer la ligne" onclick="delRegulationRow( this );" />
										<?php } ?>
									</td>
									<?php
								}else{	
									?>
									<tr class="onlyInvoice">
										<td style="text-align:center;"><?php echo $idinvoice ?></td>
										<td class="totalRowAmount" style="text-align:right;"><input type="text" id="amount_<?php echo $key ?>_<?php echo $row ?>" name="amount_<?php echo $key ?>_<?php echo $row ?>" value="<?php echo $amount; ?>" onkeyup="recalculateAmounts();" class="textInput" style="text-align:right; width:50px;" />&nbsp&euro;</td>
									</tr>
									<?php
								}
								
								$i++;
								
							}
											
						}
					?>
				</tr>
	<?php
	
	return $totalSolde;
	
}

//------------------------------------------------------------------------------------------------------

function displayCredits( $idsupplier , $credits , $key ){
	
	$creditsList = "\"".implode( "\",\"",$credits)."\"";
	$totalCredits = 0.00;
	
	// afficher les avoirs utilisés pour le fournisseur pour l'utiliser lors du règlement
	$query = "	SELECT cs.idcredit_supplier,billing_supplier, Date, cs.amount, ( cs.amount - 
					ifnull(
						(SELECT ifnull( SUM(csr.amount) , COUNT(csr.amount) ) 
						FROM credit_supplier_regulation csr 
						WHERE csr.idcredit_supplier = cs.idcredit_supplier 
						GROUP BY csr.idcredit_supplier)
						,0.00
						) 
					) as dispoAmount
				FROM credit_supplier cs
				WHERE cs.idsupplier = $idsupplier
				AND cs.idcredit_supplier IN ( $creditsList )
				HAVING dispoAmount > 0
				ORDER BY cs.idcredit_supplier ASC";
							

	$credits = DBUtil::query($query);
	if( $credits->RecordCount() ){
		while(!$credits->EOF()){
			$key++;
			?>
			<tr>
				<td style="text-align:center;"><?=$credits->fields('idcredit_supplier')?></td>
				<td style="text-align:center;"><?=Util::dateFormatEu($credits->fields('Date'));?></td>
				<td style="text-align:right;">- <?=Util::priceFormat($credits->fields('amount'));?></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td id="invoiceSolde_<?php echo $key ?>" class="invoiceSolde avoir" style="text-align:right;">-<?=Util::priceFormat($credits->fields('dispoAmount'));?></td>
			</tr>
			<?
			
			$totalCredits += $credits->fields('dispoAmount');
			$credits->MoveNext();
		}
	}
	
	return $totalCredits;
}

//------------------------------------------------------------------------------------------------------

function getCreditTotal( $idsupplier , $credits  ){
	
	$creditsList = "\"".implode( "\",\"",$credits)."\"";
	$totalCredits = 0.00;
	
	// afficher les avoirs utilisés pour le fournisseur pour l'utiliser lors du règlement
	$query = "	SELECT cs.idcredit_supplier,billing_supplier, Date, cs.amount, ( cs.amount - 
					ifnull(
						(SELECT ifnull( SUM(csr.amount) , COUNT(csr.amount) ) 
						FROM credit_supplier_regulation csr 
						WHERE csr.idcredit_supplier = cs.idcredit_supplier 
						GROUP BY csr.idcredit_supplier)
						,0.00
						) 
					) as dispoAmount
				FROM credit_supplier cs
				WHERE cs.idsupplier = $idsupplier
				AND cs.idcredit_supplier IN ( $creditsList )
				HAVING dispoAmount > 0
				ORDER BY cs.idcredit_supplier ASC";
							

	$credits = DBUtil::query($query);
	if( $credits->RecordCount() ){
		while(!$credits->EOF()){
			
			$totalCredits += $credits->fields('dispoAmount');
			$credits->MoveNext();
		}
	}
	
	return $totalCredits;
}

//------------------------------------------------------------------------------------------------------

function getsupplierName( $idsupplier ){
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du fournisseur" );
		
	return $rs->fields( "name" );
	
}

//------------------------------------------------------------------------------------------------------

function UpdateBillingsStatus( $bil, $idsupplier ){
	
		
	foreach( $bil as $key => $billing ){
		
		$query = "UPDATE billing_supplier SET `status` = 'Paid' WHERE billing_supplier = '$billing' AND idsupplier = $idsupplier";
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de mettre à jour le statut des factures clients" );
		
	}
	
}

//-----------------------------------------------------------------------------------------

function paymentList( $j, $selected_value = "" ){
	
	$lang = User::getInstance()->getLang();
	
	echo "<select name=\"idpayment_$j\" id=\"idpayment_$j\" class=\"idpayment\">";
	
	$rs =& DBUtil::query( "SELECT idpayment, name$lang FROM payment ORDER BY name$lang ASC" );
	
	if( $rs->RecordCount() > 0 ){
		
		for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
			
			$selected = "";
			
			include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
			
			if( $selected_value == "" && $rs->fields( "idpayment" ) == Payment::$PAYMENT_MOD_TRANSFER )
				$selected = " selected=\"selected\"";
			elseif( $rs->fields( "idpayment" ) == $selected_value )
				$selected = " selected=\"selected\"";
			else
				$selected = "";
			
			echo "<option value=\"" . $rs->fields( "idpayment" ) . "\"$selected>" . $rs->fields( "name$lang" ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}
	
    echo "</select>";
	
}

//-----------------------------------------------------------------------------------------------

function getInvoiceDelivPayment( $idbilling_supplier ){
	
	$query = "SELECT deliv_payment FROM billing_supplier WHERE billing_supplier = '$idbilling_supplier'";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la date d'échéance de la facture" );
	
	return $rs->fields( "deliv_payment" );
	
}
//---------------------Adde By Behzad --------------------------------------------------------------

function getInvoiceIdPaymentSupplier( $idbilling_supplier ){
	
	$query = "SELECT idpayment FROM billing_supplier WHERE billing_supplier = '$idbilling_supplier'";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la date d'échéance de la facture" );
	
	return $rs->fields( "idpayment" );
	
}
//-----------------------------------------------------------------------------------------------

function sendSupplierEmail( $idsupplier , $billings , $credits , $totalAmountRegulation ){
	
	global $GLOBAL_START_PATH, $GLOBAL_START_URL;
	
	$AdminName = DBUtil::getParameterAdmin("ad_name");
	$AdminEmail = DBUtil::getParameterAdmin("ad_mail");
	
	$iduser = User::getInstance()->getId();

	if (!empty ($iduser)) {

		$CommercialName = User::getInstance()->get('firstname') . ' ' . User::getInstance()->get('lastname');
		$CommercialEmail = User::getInstance()->get('email');
		$CommercialPhone = User::getInstance()->get('phonenumber');
		
	} else {

		$CommercialName = DBUtil::getParameterAdmin('ad_name');
		$CommercialEmail = DBUtil::getParameterAdmin('ad_mail');

	}
	
	// on sort les infos du contact principal
	$supplierContact = DBUtil::query( 'SELECT s.main_contact, sc.firstname, sc.lastname, sc.email, t.title_1 
										FROM supplier s, supplier_contact sc, title t
										WHERE s.main_contact = sc.idcontact
										AND s.idsupplier = sc.idsupplier
										AND sc.idtitle = t.idtitle
										AND s.idsupplier = ' . $idsupplier .' LIMIT 1');
	
	$arrayAdresses = array();
	
	if( $supplierContact->RecordCount() && $supplierContact->fields( 'email' ) != '' ){
		
		array_push( $arrayAdresses , $supplierContact->fields( 'email' ) );
		$supplierContactGender = $supplierContact->fields( 'title_1' );
		$supplierContactFirstName = $supplierContact->fields( 'firstname' );
		$supplierContactLastname = $supplierContact->fields( 'lastname' );
		
		$creds = "";
		if( count( $credits ) )
			$creds = ' - ' . implode( ' - ' , $credits );
			
		$documents = implode( ' - ' , $billings ) . $creds;
		
		// on evoie le mail au fournisseur qu'il sache qu'on a payé
		$htmlMail = "
			<html>
			<body>
				<p>$supplierContactGender $supplierContactFirstName $supplierContactLastname,</p>
				<p>Nous avons émis en votre faveur un virement sur votre compte en règlement des factures suivantes :</p>
				<p><strong>$documents</strong></p>
	
				<p>Bonne réception</p>
				
				$CommercialName
				
			</body>
			</html>";
		
		
		include_once ( "$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php" );
		
		$mailer = new htmlMimeMail();
			
		$mailer->setFrom('"' . $AdminName . ' - ' . $CommercialName . '" <' . $CommercialEmail . '>');
		
		$mailer->setHtml( $htmlMail );
		$mailer->setSubject( "Un règlement à été enregistré" );
		$result = $mailer->send( $arrayAdresses , 'mail' );
	}
}

?>