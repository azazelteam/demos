<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures clients B2B
 */

global $GLOBAL_START_PATH;
include_once( "$GLOBAL_START_PATH/objects/XHTMLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Invoice.php" );
			
?>
<div class="tableContainer">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<table class="dataTable dateChoiceTable">
		<tr>	
			<td style="width:20%"><input type="radio" name="interval_select" id="interval_select_since" value="1"<?php if( $postData[ "interval_select" ] == 1 ) echo " checked=\"checked\""; ?> class="VCenteredWithText" /> Depuis</td>
			<td style="width:30%">
				<select name="minus_date_select" onchange="selectSince();" class="fullWidth">
					<option value="0"<?php		if( $postData[ "minus_date_select" ] == 0 )		echo " selected=\"selected\""; ?>>Aujourd'hui</option>
				  	<option value="-1"<?php		if( $postData[ "minus_date_select" ] == -1 )	echo " selected=\"selected\""; ?>>Hier</option>
			  		<option value="-2"<?php		if( $postData[ "minus_date_select" ] == -2 )	echo " selected=\"selected\""; ?>>Moins de 2 jours</option>
				  	<option value="-7"<?php		if( $postData[ "minus_date_select" ] == -7 )	echo " selected=\"selected\""; ?>>Moins de 1 semaine</option>
				  	<option value="-14"<?php	if( $postData[ "minus_date_select" ] == -14 )	echo " selected=\"selected\""; ?>>Moins de 2 semaines</option>
				  	<option value="-30"<?php	if( $postData[ "minus_date_select" ] == -30 )	echo " selected=\"selected\""; ?>>Moins de 1 mois</option>
				  	<option value="-60"<?php	if( $postData[ "minus_date_select" ] == -60 )	echo " selected=\"selected\""; ?>>Moins de 2 mois</option>
				  	<option value="-90"<?php	if( $postData[ "minus_date_select" ] == -90 )	echo " selected=\"selected\""; ?>>Moins de 3 mois</option>
				  	<option value="-180"<?php	if( $postData[ "minus_date_select" ] == -180 )	echo " selected=\"selected\""; ?>>Moins de 6 mois</option>
				  	<option value="-365"<?php	if( $postData[ "minus_date_select" ] == -365 )	echo " selected=\"selected\""; ?>>Moins de 1 an</option>
				  	<option value="-1825"<?php	if( $postData[ "minus_date_select" ] == -1825 )	echo " selected=\"selected\""; ?>>Moins de 5 ans</option>
				</select>
			</td>
			<td style="width:20%">
				<input type="radio" name="interval_select" id="interval_select_month" class="VCenteredWithText" value="2"<?php if( $postData[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> />
				Pour le mois de
			</td>
			<td style="width:30%">
				<?php  FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
			</td>
		</tr>
		<tr>	
			<td>
				<input type="radio" name="interval_select" id="interval_select_between" class="VCenteredWithText" value="3"<?php if( $postData[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> />Entre le
			</td>
			<td>
				<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
		   	</td>
			<td>et le</td>
			<td>
 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="tableSeparator"></td>
		</tr>
		<tr class="filledRow">
			<th>Client n°</th>
			<td><input type="text" name="idbuyer" value="<?php echo $postData[ "idbuyer" ] ?>" class="textInput" /></td>
			<th>Nom du contact</th>
			<td><input type="text" name="lastname" value="<?php echo $postData[ "lastname" ] ?>" class="textInput" /></td>
		</tr>
		<tr>
			<th>Nom / Raison sociale</th>
			<td><input type="text" name="company" value="<?php echo $postData[ "company" ] ?>" class="textInput" /></td>
			<th>Fax</th>
			<td><input type="text" name="faxnumber"  value="<?php echo $postData[ "faxnumber" ] ?>" class="textInput" /></td>
		</tr>
		<tr>
			<th>Code postal</th>
			<td><input type="text" name="zipcode" value="<?php echo $postData[ "zipcode" ] ?>" class="textInput" /></td>
			<th>Ville</th>
			<td><input type="text" name="city" value="<?php echo $postData[ "city" ] ?>" class="textInput" /></td>
		</tr>
		<tr class="filledRow">
			<th>Provenance du client</th>
			<td>
				<select name="idsource[]" multiple="multiple" size="5" class="fullWidth">
					<option value="">Toutes</option>
					<?php
						
						$lang = User::getInstance()->getLang();
						$rssource =& DBUtil::query( "SELECT idsource, source_code, source$lang AS source FROM source ORDER BY source$lang ASC" );
				   		
				   		for( $s = 0 ; $s < $rssource->RecordCount() ; $s++ ){
							
							?>
					<option value="<?php echo $rssource->fields( "idsource" ) ?>"<?php if( $postData[ "idsource" ] == $rssource->fields( "idsource" ) ){ echo " selected=\"selected\"";} ?>><?php echo $rssource->fields( "source" )?></option>	
							<?php
							
							$rssource->MoveNext();
							
						}
						
					?>
				</select>
			</td>
			<th>Commercial</th>
			<td>
			<?php
	
				$hasAdminPrivileges = User::getInstance()->get( "admin" );
				if( $hasAdminPrivileges ){
					
					include_once( dirname( __FILE__ ) . "/../../script/user_list.php" );
					userList( true, $postData[ "iduser" ] );
					
					//XHTMLFactory::createSelectElement( "user", "iduser", array( "firstname", "lastname" ), "lastname", $postData[ "iduser" ], $nullValue = 0, $nullInnerHTML = "-", $attributes = " name=\"iduser\"" );
					
				}
				else echo "<b>" . User::getInstance()->get( "firstname" ) . " " . User::getInstance()->get( "lastname" ) . "</b>";
				
			?>
			</td>
		</tr>
		<?php $akilae_grouping = DBUtil::getParameterAdmin("akilae_grouping"); ?>
		<?php if($akilae_grouping==1){ ?>
		<tr>
        	<th>Groupement</th>
            <td colspan="3"><?php XHTMLFactory::createSelectElement( "grouping", "idgrouping", "grouping", "grouping", isset( $_POST[ "idgrouping" ] ) ? $_POST[ "idgrouping" ] : false, "", "-", "name=\"idgrouping\"" ); ?></td>
        </tr>    
		<?php } ?>
		<tr>
			<td colspan="4" class="tableSeparator"></td>
		</tr>
		<tr>
			<th>BL n°</th>
			<td colspan="3">
				<input type="text" name="idbl_delivery" value="<?php echo $postData[ "idbl_delivery" ] ?>" class="textInput" />
			</td>
			
		</tr>
		<tr><td colspan="4" class="tableSeparator"></td></tr>
		<tr class="filledRow">
			<th>Commande n°</th>
			<td><input type="text" name="idorder" value="<?php echo $postData[ "idorder" ] ?>" class="textInput" /></td>
			<!--<th>Facture n°</th>
			<td>
				<input type="text" name="idbilling_buyer" value="<?php echo $postData[ "idbilling_buyer" ] ?>" class="textInput" />
			</td>-->
			<th>Avancement</th>
			<td>
				<select name="status" class="fullWidth">
				 	<option value=""></option>
					<option value="<?php echo Invoice::$STATUS_INVOICED ?>"<?php if( $postData[ "status" ] == Invoice::$STATUS_INVOICED ) echo " selected=\"selected\""; ?>>Facturée</option>
					<option value="<?php echo Invoice::$STATUS_PAID ?>"<?php if( $postData[ "status" ] == Invoice::$STATUS_PAID ) echo " selected=\"selected\""; ?>>Payée</option>
				</select>
			</td>
			
		</tr>
		<tr>
		  	<th>Total facturé HT</th>
			<td><input type="text" name="total_amount_ht" value="<?php echo $postData[ "total_amount_ht" ] ?>" class="textInput" /></td>
			<th>Total facturé TTC</th>
			<td><input type="text" name="total_amount" value="<?php echo $postData[ "total_amount" ] ?>" class="textInput" /></td>
	  	</tr>
	  	<tr>
	  		<th>Mode de paiement</th>
	  		<td colspan="3"><?php XHTMLFactory::createSelectElement( "payment", "idpayment", "name" . User::getInstance()->getLang(), "name" . User::getInstance()->getLang(), $postData[ "idpayment" ], "Tous","", " name=\"idpayment\"" ); ?></td>
	  	</tr>
	</table>
</div>