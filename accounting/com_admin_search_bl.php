<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * BL à facturer pour les clients
 */

//------------------------------------------------------------------------------------------------


$Title = "Liste des Cdes à facturer";

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php" );
include_once( dirname( __FILE__ ) . "/../objects/Order.php" );


//------------------------------------------------------------------------------------------------
/* mise à jour du factor */

if( isset( $_GET[ "update" ] ) && $_GET[ "update" ] == "factor" && isset( $_GET[ "idorder" ] ) ){
	echo "here";
	$idorder = intval( $_GET[ "idorder" ] );
	$orderFactor = DBUtil::query( "SELECT factor FROM `order` WHERE idorder = '$idorder' LIMIT 1" )->fields( "factor" );
	$ret = false;
	
	if( !$orderFactor ){ //@todo tempo à cause de assurance <--> factor
		$idbuyer = DBUtil::query( "SELECT idbuyer FROM `order` WHERE idorder = '$idorder' LIMIT 1" )->fields( "idbuyer" );
		DBUtil::query( "UPDATE `order` SET factor = '$idbuyer' WHERE idorder = '$idorder' LIMIT 1" );
		$ret = DBUtil::getAffectedRows() == 1;
	}
	else if( !$orderFactor ){ //@todo tempo à cause de assurance <--> factor
		DBUtil::query( "UPDATE `order` SET factor = '$buyerFactor' WHERE idorder = '$idorder' LIMIT 1" );
		$ret = DBUtil::getAffectedRows() == 1;
	}
	else if( $orderFactor ){
		DBUtil::query( "UPDATE `order` SET factor = '0' WHERE idorder = '$idorder' LIMIT 1" );
		$ret = DBUtil::getAffectedRows() == 1;
	}
	
	echo $ret ? "1" : "0";
	
	exit();
	
}

//------------------------------------------------------------------------------------------------
//Tooltip Factor
if( isset( $_GET[ 'getTooltipFactor' ] ) && $_GET[ 'getTooltipFactor' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( getTooltipFactor( $_GET[ 'getTooltipFactor' ] ) );

}

//------------------------------------------------------------------------------------------------
//Tooltip Commande
if( isset( $_GET[ 'getTooltipOrder' ] ) && $_GET[ 'getTooltipOrder' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( getTooltipOrder( $_GET[ 'getTooltipOrder' ] ) );

}

//------------------------------------------------------------------------------------------------
//Tooltip Commande Founisseur
if( isset( $_GET[ 'getTooltipOrderSupplier' ] ) && $_GET[ 'getTooltipOrderSupplier' ] > 0 ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	exit( getTooltipOrderSupplier( $_GET[ 'getTooltipOrderSupplier' ] ) );

}

//------------------------------------------------------------------------------------------------
//génération du PDF
if( isset( $_GET[ "output" ] ) && isset( $_GET[ "idinvoices" ] ) ){
	
	$generatedInvoices = explode( "-", $_GET[ "idinvoices" ] );
	if( count( $generatedInvoices ) ){
		outputGeneratedInvoices( $generatedInvoices );
		exit();
	}
	else trigger_error( "URL malformée", E_USER_ERROR );
	
}

//------------------------------------------------------------------------------------------------
//facturation

$generatedInvoices = array();

if ( isset( $_POST[ "generateInvoices" ] ) )
	$generatedInvoices = generateInvoices();

$resultPerPage = 50000;
$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;

//---------------------------------------------------------------------------------------------
//affichage de la page

include_once( dirname( __FILE__ ) . "/../templates/back_office/head.php" );

?>

<link rel="stylesheet" type="text/css" href="<?= HTTP_HOST ?>/css/back_office/form_default.css" />
<script type="text/javascript">
/* <![CDATA[ */

	//onglets
	$(function() {
		$('#container').tabs({ 
			fxFade: true, 
			fxSpeed: 'fast',
			selected: <?php echo isset( $_GET[ "tab" ] ) ? intval( $_GET[ "tab" ] ) : "0"; ?>
		});
	});
/* ]]> */
</script>
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
<div class="centerMax">


	
	
	<div class="contentDyn" style="float:right; margin-right:30px;">
		<!-- Titre -->
		<h1 class="titleSearch" style="margin:15px 0 5px 0 !important; padding:2px 0; width:100%; max-width:1240px; min-width:730px; -webkit-border-radius:5px; border:1px solid #D0D0D0; background:#FFFFFF">
			<span class="textTitle" style="color:#9d0b5d; font-size:17px; padding: 2px 5px; display:block;">Commandes à facturer</span>
			<div class="spacer"></div>
		</h1>
		
		
		
		<div id="container">
			<!-- Onglet -->
			<ul class="menu">
				<li class="item"><a href="#livraisons-partielles"><span>Livraisons partielles</span></a></li>
				<li class="item"><a href="#paiements-comptant"><span>Paiements comptant reçus</span></a></li>
				<?
				if( count( $generatedInvoices ) ){?>
					<li class="item"><a href="#createdInvoices"><span>Factures créées</span></a></li>
				<?}?>
			</ul>
			<script type="text/javascript">
			/* <![CDATA[ */
			
				function goToPage( pageNumber ){
				
					document.forms.DeliveryNoteForm.elements[ 'page' ].value = pageNumber;
					document.forms.DeliveryNoteForm.submit();
					
				}
				
				//-----------------------------------------------------------------------------------------
				
				function uncheck( identifiers ){
				
					var i = 0;
					while( i < identifiers.length ){
					
						var elementID = identifiers[ i ];
						var element = document.getElementById( elementID );
						
						if( element != null )
							element.checked = false;
							
						i++;
						
					}
						
				}
				
				//-----------------------------------------------------------------------------------------
				
				function displayChargeEditor( idorder, idbl_delivery, display ){
				
					if( !display ){
					
						var input = document.getElementById( 'total_charge_ht_' + idbl_delivery );
						
						if( input != null ){
						
							var parent = input.parentNode;
							parent.removeChild( input );
						
						}
						
					}
					else{
					
						var charges = getCharges( idorder, idbl_delivery );
						var input = document.createElement( 'input' );
			
						input.setAttribute( 'id', 'total_charge_ht_' + idbl_delivery ); 
						input.setAttribute( 'name', 'total_charge_ht_' + idbl_delivery ); 
						input.setAttribute( 'type', 'text' );
						input.setAttribute( 'value', charges.toFixed( 2 ) );
						input.setAttribute( 'size', '6' );
						input.onkeypress = function( event ){
						
							var keynum;
							
							if( window.event ) // IE
			  					keynum = window.event.keyCode;
							else if( event.which ) // Netscape/Firefox/Opera
		  						keynum = event.which;
		  					else return true;
		
		  					if( keynum == 8 ) //backspace
		  						return true;
		  							
		  					var keychar = String.fromCharCode( keynum );
		  					
		  					var pattern = '^[0-9,\.]{1}$';
							var reg = new RegExp( pattern, 'g' );
				
							if( !reg.test( keychar ) )
								return false;
		
							return true;
							
						}
						
						input.onchange = function( event ){
						
							checkChargeSum( idorder );
							
						}
						
						var cell = document.getElementById( 'ChargeCell_' + idbl_delivery );
						cell.appendChild( input );
					
					}
					
				}
				
				//-----------------------------------------------------------------------------------------
				
				function getCharges( idorder, idbl_delivery ){
			
					var order_charges 			= document.getElementById( 'order_charges_' + idorder ).value;
					var invoiced_charges 		= document.getElementById( 'invoiced_charges_' + idorder ).value;
					var non_invoiced_charges 	= order_charges - invoiced_charges;
					
					var deliveryNoteList = document.forms.DeliveryNoteForm.elements[ 'order_delivery_notes_' + idorder ];
					var i = 0;
					while( i < deliveryNoteList.length ){
						
						var id = deliveryNoteList[ i ].value;
						var element = document.forms.DeliveryNoteForm.elements[ 'total_charge_ht_' + id ];
			
						if( element != null )
							non_invoiced_charges -= floatValue( element.value );
		
						i++;
						
					}
					
					return non_invoiced_charges < 0.0 ? 0.0 : parseFloat( non_invoiced_charges );
					
				}
				
				//-----------------------------------------------------------------------------------------
				
				function floatValue( text ){
				
					return parseFloat( text.replace( ',', '.' ) );
					
				}
				
				//-----------------------------------------------------------------------------------------
				
				function checkChargeSum( idorder ){
				
					var deliveryNoteList = document.forms.DeliveryNoteForm.elements[ 'order_delivery_notes_' + idorder ];
					
					if( deliveryNoteList == null )
						return true;
						
					var order_charges 			= document.getElementById( 'order_charges_' + idorder ).value;
					var invoiced_charges 		= document.getElementById( 'invoiced_charges_' + idorder ).value;
					var non_invoiced_charges 	= order_charges - invoiced_charges;
			
					var i = 0;
					var blCount = 0;
					var selectedBLCount = 0;
					while( i < deliveryNoteList.length ){
						
						var id = deliveryNoteList[ i ].value;
						var element = document.forms.DeliveryNoteForm.elements[ 'total_charge_ht_' + id ];
			
						if( element != null ){
						
							non_invoiced_charges -= floatValue( element.value );
							selectedBLCount++;
							
						}
						
						i++;
						blCount++;
						
					}
			
					var hasError = false;
					var errorMessage = '';
					
					//si trop de frais de port ont été facturés
					
					if( non_invoiced_charges < 0.0 ){
					
						hasError = true;
						errorMessage = 'Le montant des frais port facturés pour la commande n° ' + idorder + ' ne doit pas excéder ' + order_charges + ' euros';
					
					}
					
					//si tous les BL seront facturés et que tous les frais de port n'auront pas été facturés
				
					if( !hasError && selectedBLCount && blCount == selectedBLCount && non_invoiced_charges > 0.0 ){
					
						hasError = true;
						errorMessage = 'Le montant total des frais port facturés pour la commande n° ' + idorder + ' doit être de ' + order_charges + ' euros';
		
					}
					
					document.getElementById( 'ChargeErrorDiv' ).innerHTML = errorMessage;
					
					document.forms.DeliveryNoteForm.elements[ 'InvoiceButton' ].disabled = hasError ? true : false;
					updateFieldsetStatus( idorder, hasError );
					
					return !hasError;
						
				}
				
				//-----------------------------------------------------------------------------------------
				
				function checkAllChargeSum(){
				
					var idorderList = document.forms.DeliveryNoteForm.elements[ 'idorderList' ];
					
					var i = 0;
					while( i < idorderList.length ){
					
						var idorder = idorderList[ i ].value;
					
						if( !checkChargeSum( idorder ) )
							return false;
		
						i++;
						
					}
		
					return true;
					
				}
				
				//-----------------------------------------------------------------------------------------
				
				function updateFieldsetStatus( idorder, hasError ){
				
					var deliveryNoteList = document.forms.DeliveryNoteForm.elements[ 'order_delivery_notes_' + idorder ];
					
					var i = 0;
					while( i < deliveryNoteList.length ){
						
						var id = deliveryNoteList[ i ].value;
						var element = document.forms.DeliveryNoteForm.elements[ 'total_charge_ht_' + id ];
			
						if( element != null )
						element.style.backgroundColor = hasError ? '#FFDFDF' : '#FFFFFF';
		
						i++;
						
					}
							
				}
				
				//-----------------------------------------------------------------------------------------
				
				function updateFactor( idorder, element ){
					$.ajax({
				 	
						url: "<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?update=factor&idorder=" + idorder,
						async: true,
					 	success: function( msg ){
		
					 		if( msg == '0' ){
					 			
					 			alert( 'Impossible de modifier le factor' );
					 			return;
					 			
					 		}
					 		
					 		element.innerHTML = element.innerHTML == 'Non' ? '<span style=\"color:#f69814\">Oui</span>' : 'Non';
					 		
							$.growlUI( '', 'Factor modifié' );
		
						}
			
					});
				
				}
				
				//-----------------------------------------------------------------------------------------
				
				function resetTotalAmount(){
					
					$("#selectionAmount").val( 0 );
					$("#selectionAmountDiv").html( "0.00 ¤" );
					
					$(".amountRow").each(function(){
						if($(this).is(".activeAmountRow"))
							$(this).removeClass("activeAmountRow");
					});
					
				}
				
				//-----------------------------------------------------------------------------------------
				
				$(document).ready(function() {
					
					$(".amountRow").click(function(){
						
						var value = parseFloat( $(this).children('.amount').html().replace( ',', '.' ).replace( '&nbsp;', '' ).replace( ' ', '' ) );
						var totalAmount = $("#selectionAmount").val();
						
						if($(this).is(".activeAmountRow")){
							
							$(this).removeClass("activeAmountRow");
							
							totalAmount = Math.round( ( parseFloat( totalAmount ) - parseFloat( value ) ) * 100 ) / 100;
							
							idor = $(this).attr('rel');
							$(this).parent( 'tbody' ).children( 'tr[rel='+idor+']' ).each( function(){ $(this).removeClass("activeAmountRow"); });
						}
						else{
							
							$(this).addClass("activeAmountRow");
							
							idor = $(this).attr('rel');
							$(this).parent( 'tbody' ).children( 'tr[rel='+idor+']' ).each( function(){ $(this).addClass("activeAmountRow"); });
							
							totalAmount = Math.round( ( parseFloat( totalAmount ) + parseFloat( value ) ) * 100 ) / 100;
							
						}
						
						$("#selectionAmount").val( totalAmount );
						$("#selectionAmountDiv").html( totalAmount.toFixed( 2 ).toString().replace( ".", "," ) + " &euro;" );
						
					});
					
				});
				
				//-----------------------------------------------------------------------------------------
				
			/* ]]> */
			</script>
			<form action="com_admin_search_bl.php" method="post" name="DeliveryNoteForm" enctype="multipart/form-data">	
			<input type="hidden" name="generateInvoices" value="1" />
			<div class="blocResult" style="margin:0; -moz-border-radius-topleft:0px !important; "><div style="margin:5px;">
			
				<!-- ********************* -->
				<!-- Livraisons partielles -->
				<!-- ********************* -->
				<div id="livraisons-partielles">
					<?php search(); ?>
				</div>
				
				<!-- ************************ -->
				<!-- Paiements comptant reçus -->
				<!-- ************************ -->
				<div id="paiements-comptant">
					<?php include_once( dirname( __FILE__ ) . "/cash_payment.php" ); ?>
				</div>
				
				<?
				if( count( $generatedInvoices ) ) {
					?>
					<!-- ************************ -->
					<!-- Factures créées		  -->
					<!-- ************************ -->
					<div id="createdInvoices">
						<? displayGeneratedInvoices( $generatedInvoices ); ?>
					</div><?
				}
				?>
				<div class="spacer"></div>
			</div></div>
			</form>
		</div>	
		<div class="rightTools">
		<div class="toolBox">
			<h2><img alt="star" src="<?= HTTP_HOST ?>/images/back_office/layout/icons-menu-calculette.png">
			Montant sélection</h2>
			<div class="spacer"></div>
			<input type="hidden" id="selectionAmount" value="0" />
			
			<div class="blocSearch">
				<div id="selectionAmountDiv" style="font-size:14px; font-weight:bold; padding-top:5px; text-align:center;">0.00 ¤</div>
				<input type="button" value="Effacer" onclick="resetTotalAmount();" class="blueButton" style="display:block; margin:auto; margin-top:3px;" />
			</div>
		</div>
	</div>
		<div class="spacer"></div>		
		
	</div>

</div><!-- centerMax -->
<?php



include_once( dirname( __FILE__ ) . "/../templates/back_office/foot.php" );

//---------------------------------------------------------------------------------------------
//recherche

function search(){
	
	global $ScriptName;
	
	$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
	$lang = User::getInstance()->getLang();
	
	//recherche des commandes à facturer
	
	
	// ------- Mise à jour de l'id gestion des Index  #1161	
	$query = "
	SELECT DISTINCT( bl.idorder ),
		u.initial,
		b.company,
		b.siren,
		b.factor,
		c.lastname,
		o.total_amount,
		o.factor,
		o.idbuyer,
		bl.idorder_supplier,
		oss.order_supplier_status$lang,
		s.name,
		os.DateHeure
	FROM bl_delivery bl, user u, buyer b, contact c, `order` o, order_supplier os, order_supplier_status oss, supplier s
	WHERE bl.idbilling_buyer = '0'
	AND bl.dispatch_date NOT IN ( '0000-00-00', '' )
	AND bl.dispatch_date <= NOW()
	AND bl.idorder = o.idorder
	AND o.cash_payment = 0
	AND o.total_amount > 0.0
	AND bl.iduser = u.iduser
	AND bl.idbuyer = b.idbuyer
	AND bl.idbuyer = c.idbuyer
	AND bl.idcontact = c.idcontact
	AND bl.idorder_supplier = os.idorder_supplier
	AND os.status = oss.order_supplier_status
	AND os.idorder <> '0'
	AND os.internal_supply = 0
	AND bl.idsupplier = s.idsupplier";

	//droits commercial
	
	$hasAdminPrivileges = User::getInstance()->get( "admin" );
	if( !$hasAdminPrivileges ){
		
		$iduser = User::getInstance()->getId();

		$query .= "
		AND ( b.iduser = '$iduser' OR bl.iduser = '$iduser' )";

	}

	$orderby = isset( $_GET[ "orderby" ] ) ? Util::html_escape( base64_decode( $_GET[ "orderby" ] ) ) : "bl.idbuyer ASC, bl.dispatch_date DESC";
	
	$query .= "
	GROUP BY bl.idorder
	ORDER BY $orderby";

	$rs =& DBUtil::query( $query );
	$rs_count =& DBUtil::query( $query );
	
	$resultCount = $rs->RecordCount();
	
	if( !$resultCount ){
		
		?>
		<div class="content">
        	<p class="msg" style="text-align:center;"><?php  echo Dictionnary::translate("no_result"); ?></p>
		</div>
		<?php
		
		return;
		
	}
			
	$resultPerPage = 50;
	$pageCount = ceil( $resultCount / $resultPerPage );
	$start = ( $page - 1 ) * $resultPerPage;

	?>
	<style type="text/css">
	/* <![CDATA[ */
		
		.fixme {
			/* Netscape 4, IE 4.x-5.0/Win and other lesser browsers will use this */
		  position: absolute;
		  right: 10px;
		  top: 135px;
		}
		body > div#global > div#globalMainContent > div.fixme {
		  /* used by Opera 5+, Netscape6+/Mozilla, Konqueror, Safari, OmniWeb 4.5+, iCab, ICEbrowser */
		  position: fixed;
		}
		
		tr.activeAmountRow td { background-color:#E0E0E0; }
		
	/* ]]> */
	</style>
	<!--[if gte IE 5.5]>
	<![if lt IE 7]>
	<style type="text/css">
	
		div.fixme {
		  /* IE5.5+/Win - this is more specific than the IE 5.0 version */
		  right: expression( ( 10 + ( ignoreMe2 = document.documentElement.scrollLeft ? document.documentElement.scrollLeft : document.body.scrollLeft ) ) + 'px' );
		  top: expression( ( 135 + ( ignoreMe = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop ) ) + 'px' );
		}
		
	</style>
	<![endif]>
	<![endif]-->
	<input type="hidden" name="generateInvoices" value="1" />
	<input type="hidden" name="page" value="<?= $page ?>" />
	<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
	<input type="hidden" name="search" value="1" />	
	
	
	<?php
	$nbFact = 0;
	$bl_total_amount = 0.0;
	while( !$rs_count->EOF() ){

    	
    	$bl_total_amount += $rs_count->fields( "total_amount" );
    	$nbFact++;
    	$rs_count->MoveNext();
    	
    }
	
	?>
	
	<p><?= $nbFact ?> livraison(s) partielle(s) pour un montant total de <strong><?= Util::priceFormat($bl_total_amount) ?></strong></p><br/>

	<script type="text/javascript">
	/* <![CDATA[ */
      
	    $(document).ready(function(){

	        $(".factorHelp").each( function(){
		        
		        $(this).qtip({
		            adjust: {
		                mouse: true,
		                screen: true
		            },
		            content: { url: '<?php echo HTTP_HOST; ?>/accounting/com_admin_search_bl.php?getTooltipFactor=' + $(this).attr( 'rel' ) },
		            position: {
		                corner: {
		                    target: "rightMiddle",
		                    tooltip: "leftMiddle"
		                }
		            },
		            style: {
		                background: "#FFFFFF",
		                border: {
		                    color: "#EB6A0A",
		                    radius: 5
		                },
		                fontFamily: "Arial,Helvetica,sans-serif",
		                fontSize: "13px",
		                tip: true,
		                width: 'auto'
		            }
	        	});
	        	
	        });

	        $(".orderHelp").each( function(){
		        
		        $(this).qtip({
		            adjust: {
		                mouse: true,
		                screen: true
		            },
		            content: { url: '<?php echo HTTP_HOST; ?>/accounting/com_admin_search_bl.php?getTooltipOrder=' + $(this).attr( 'rel' ) },
		            position: {
		                corner: {
		                    target: "rightMiddle",
		                    tooltip: "leftMiddle"
		                }
		            },
		            style: {
		                background: "#FFFFFF",
		                border: {
		                    color: "#EB6A0A",
		                    radius: 5
		                },
		                fontFamily: "Arial,Helvetica,sans-serif",
		                fontSize: "13px",
		                tip: true,
		                width: 'auto'
		            }
	        	});
	        	
	        });

	        $(".orderSupplierHelp").each( function(){
		        
		        $(this).qtip({
		            adjust: {
		                mouse: true,
		                screen: true
		            },
		            content: { url: '<?php echo HTTP_HOST; ?>/accounting/com_admin_search_bl.php?getTooltipOrderSupplier=' + $(this).attr( 'rel' ) },
		            position: {
		                corner: {
		                    target: "rightMiddle",
		                    tooltip: "leftMiddle"
		                }
		            },
		            style: {
		                background: "#FFFFFF",
		                border: {
		                    color: "#EB6A0A",
		                    radius: 5
		                },
		                fontFamily: "Arial,Helvetica,sans-serif",
		                fontSize: "13px",
		                tip: true,
		                width: 'auto'
		            }
	        	});
	        	
	        });	        	        
	        
	    });
	
	/* ]]> */
	</script>
	
	<div class="tableContainer">
		<table class="dataTable devisTable">
			<thead>
				<tr>
				 	<th style="border-bottom-style:none; vertical-align:top;">Com.</th>
				 	<th style="border-bottom-style:none; vertical-align:top;">Commande n°</th>
				 	<th style="border-bottom-style:none; vertical-align:top;">Client n°</th>
				 	<th style="border-bottom-style:none; vertical-align:top;">Nom</th>
				 	<th style="border-bottom-style:none; vertical-align:top;">Total TTC</th>
				 	
				 	<th style="border-bottom-style:none; vertical-align:top;">Commande<br />fournisseur n°</th>
				 	<th style="border-bottom-style:none; vertical-align:top;">Fournisseur</th>
				 	<!-- <th style="border-bottom-style:none; vertical-align:top;">BL n°</th>
				 	<th style="border-bottom-style:none; vertical-align:top;">Date d'éxpéd.<br />prévue</th> -->
				    <th style="border-bottom-style:none; vertical-align:top;">Date d'éxpéd.<br />Réalisée</th>
				    
				    <th style="border-bottom-style:none; vertical-align:top;">Factor</th>
				    
				    <th style="border-bottom-style:none; vertical-align:top; background:url(../images/back_office/content/bg-important.jpg) repeat-x #ff6b01; color:#fff; ">A facturer</th>
					<th style="border-bottom-style:none; vertical-align:top;">Facturation partielle<br />possible</th>
					<th style="border-bottom-style:none; vertical-align:top;"><?php /* NE PAS COMMENTER CETTE COLONNE SVP */ ?>Port HT</th>
				</tr>
				<!-- petites flèches de tri -->
				<tr>
					<th class="noTopBorder">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "u.initial ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "u.initial DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "o.idorder ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "o.idorder DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "b.idbuyer ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "b.idbuyer DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "b.company ASC, c.lastname ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "b.company DESC, c.lastname DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "o.total_amount ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "o.total_amount DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th style="border-top-style:none;">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "bl.idorder_supplier ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "bl.idorder_supplier DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th style="border-top-style:none;">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "s.name ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "s.name DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<!-- <th style="border-top-style:none;"></th>
					<th style="border-top-style:none;"></th> -->
					<th style="border-top-style:none;">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "bl.dispatch_date ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "bl.dispatch_date DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th class="noTopBorder">
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "o.factor ASC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/asc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/asc.png';" alt="Trier par ordre croissant" /></a>
						<a href="<?= HTTP_HOST ?>/accounting/com_admin_search_bl.php?orderby=<?= base64_encode( "o.factor DESC" ) ?>"><img src="<?= HTTP_HOST ?>/images/back_office/content/desc.png" onmouseover="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?= HTTP_HOST ?>/images/back_office/content/desc.png';" alt="Trier par ordre décroissant" /></a>
					</th>
					<th style="border-top-style:none; background:#ff6b01;"></th>
					<th style="border-top-style:none;"></th>
					<th style="border-top-style:none;"></th>
				</tr>
			</thead>
			<tbody>
				 <?php

			        $bl_total_amount= 0.0;
			        while( !$rs->EOF() ){
			        	$idorder = $rs->fields( "idorder" );
			        	displayOrderRow( $idorder );
			        	$bl_total_amount += $rs->fields( "total_amount" );
			        	$rs->MoveNext();
			        }
			        
			       
					
				?>
				<tr>
					<td colspan="4" style="border-style:none;"></td>
					<td style="font-weight:bold; text-align:right; white-space:nowrap;"><?= Util::priceFormat( $bl_total_amount ) ?> </td>
					<td colspan="9" style="border-style:none;"></td>
				</tr>
			</tbody>
		</table>
	</div><!-- tableContainer -->
	<input type="hidden" name="listpartially" id="listpartially" />
	<div id="ChargeErrorDiv" style="font-weight:bold; color:#FF0000; text-align:center; margin-top:15px;"></div>
	<div class="submitButtonContainer">
		<input type="submit" name="InvoiceButton" value="Facturer" onclick="return checkAllChargeSum() && confirm('Générer les factures pour les BL sélectionnés ?');" class="blueButton floatright" />
	</div>
	<?php 
	
	paginate();

}

//------------------------------------------------------------------------------------------------

/**
 * Afficher les Cdes à facturer pour une commande donnée
 */
function displayOrderRow( $idorder ){
		
	$lang = User::getInstance()->getLang();
	
	//récupérer les BLs et tout le tsouin-tsouin
	// ------- Mise à jour de l'id gestion des Index  #1161
	$query = "
	SELECT bl.idbl_delivery,
		bl.idorder_supplier, 
		bl.idsupplier,
		bl.DateHeure, 
		bl.idbuyer, 
		bl.idsupplier,
		bl.dispatch_date,
		bl.idbilling_buyer,
		u.initial,
		s.name AS supplierName,
		c.lastname,
		c.firstname,
		c.title AS idtitle,
		b.contact,
		b.company,
		b.siren,
		b.factor AS b_factor,
		b.insurance_status,
		o.total_amount,
		o.total_charge_ht,
		o.factor,
		o.idbilling_adress,
		o.cash_payment,
		o.insurance,
		o.idpayment,
		os.availability_date,
		pd.payment_code,
		pd.payment_delay$lang AS payment_delay,
		o.cash_payment_idregulation
	FROM bl_delivery bl, user u, buyer b, contact c, supplier s, `order` o, order_supplier os, payment_delay pd
	WHERE o.idorder = '$idorder'
	AND bl.idorder = o.idorder
	AND o.idpayment_delay = pd.idpayment_delay
	AND bl.idorder_supplier = os.idorder_supplier
	AND bl.iduser = u.iduser
	AND bl.idsupplier = s.idsupplier
	AND bl.idbuyer = b.idbuyer
	AND c.idbuyer = bl.idbuyer
	AND c.idcontact = bl.idcontact
	AND os.idorder <> '0'
	AND os.internal_supply = 0
	ORDER BY idbilling_buyer ASC"; //Attention : NE PAS MODIFIER LA CLAUSE ORDER BY

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() ){
		
		trigger_error( "Des données sont manquantes pour la facturation de la commande n°$idorder", E_USER_ERROR );
		return;
			
	}
	
	$blCount = $rs->RecordCount();		//nombre de BLs
	$invoiceCount = 0;					//nombre de BLs facturés
	$nonInvoicedBls = array();			//liste des BLs non facturés
	$partiallyInvoicedCount = 0;		//nombre de BLs déjà facturés partiellement
	$invoiced_charges = 0.0;			//frais de port déjà facturés
	
	//frais de port restant à facturer
	$order_charges = $rs->fields( "total_charge_ht" );
	$non_invoiced_charges = max( $order_charges - $invoiced_charges, 0.0 );
	
	include_once( dirname( __FILE__ ) . "/../objects/Customer.php" );
	include_once( dirname( __FILE__ ) . "/../objects/Order.php" );

	$customer 	= new Customer( $rs->fields( "idbuyer" ) );
	$order 		= new Order( $idorder );
	
	$allowGlobalInvoicing = true;
	$last_idbilling_buyer = false;
	
	while( !$rs->EOF() ){
		
		if( $rs->fields( "idbilling_buyer" ) != 0 && ( $last_idbilling_buyer === false || $last_idbilling_buyer != $rs->fields( "idbilling_buyer" ) )  ){
			$invoiced_charges += DBUtil::getDBValue( "total_charge_ht", "billing_buyer", "idbilling_buyer", $rs->fields( "idbilling_buyer" ) );
			$invoiceCount++;
		}

		if( !$rs->fields( "idbilling_buyer" ) )
			$nonInvoicedBls[] = $rs->fields( "idbl_delivery" );
		
		if( $rs->fields( "idbilling_buyer" ) )
			$partiallyInvoicedCount++;
	
		//vérifier si on autorise la facturation globale ou non
		//si au moins une date d'expédition n'est pas renseignée, on bloquera la facturation globale		
		if( $rs->fields( "dispatch_date" ) == "0000-00-00" && $allowGlobalInvoicing )
			$allowGlobalInvoicing = false;
		
		$last_idbilling_buyer = $rs->fields( "idbilling_buyer" );
		
		$rs->MoveNext();
		
	}
	
	$rs->MoveFirst();

	//nombre de BLs facturables globalement
	$nonInvoicedCount = $blCount - $partiallyInvoicedCount;
	$globallyInvoiceableCount = $blCount == 1 || $nonInvoicedCount > 1 ? $nonInvoicedCount : 0;
	
	//nombre de BLs facturables partiellement
	$partiallyInvoiceableCount = $nonInvoicedCount > 1 ? $blCount - $partiallyInvoicedCount : 0;
	
	//afficher les données
	$rowspanStr 		= $blCount > 1 ? " rowspan=\"$blCount\"" : "";
	
	$init 				= $rs->fields( "initial" );
	$isContact 			= $rs->fields( "contact" ) == 1;
	$total_amount 		= $rs->fields( "total_amount" );
	$total_charge_ht 	= $rs->fields( "total_charge_ht" );
	
	$insurance 			= $rs->fields( "insurance" );
	$contact 			= $rs->fields( "contact" );
	$company 			= $rs->fields( "company" );
	$b_factor 			= $rs->fields( "b_factor" );
	$siren				= $rs->fields( "siren" );
	$idtitle 			= $rs->fields( "idtitle" );
	$title 				= empty( $idtitle ) ? "" : DBUtil::getDBValue( "title$lang", "title", "idtitle", $idtitle );
	$lastname 			= $rs->fields( "lastname" );
	$firstname 			= $rs->fields( "firstname" );
	$idbuyer 			= $rs->fields( "idbuyer" );
	$siren 				= $rs->fields( "siren" );
	$cash_payment		= $rs->fields( "cash_payment" );
	$cash_payment_idregulation= $rs->fields( "cash_payment_idregulation" );
	$idbilling_adress	= $rs->fields( "idbilling_adress" );
	$payment_code		= $rs->fields( "payment_code" );
	$buyer_insurance	= $rs->fields( "insurance_status" );
	$payment_delay		= $rs->fields( "payment_delay" );
	$buyerName 			= strlen( $company ) ? $company : "$title $lastname";
	$idpayment 			= $rs->fields( "idpayment" );
	
	$orderHREF 			= HTTP_HOST . "/sales_force/com_admin_order.php?IdOrder=$idorder";
	$buyerHREF 			= HTTP_HOST . "/sales_force/contact_buyer.php?key=$idbuyer&amp;type=";
	$buyerHREF 			.= $isContact ? "contact" : "buyer";
	
	$cashPayment		= DBUtil::getDBValue( "balance_date", "order", "idorder", $idorder ) == "0000-00-00" ? false : true;
	
	$chargesDispatched 	= false;
	
	?>

<?php 
$accompte=0;
if($cash_payment_idregulation!="0") 
{

$sql1= "SELECT amount	
	FROM regulations_buyer
	WHERE idregulations_buyer='$cash_payment_idregulation'";

	$rs11 =& DBUtil::query($sql1);
$accompte	= $rs11->fields( "amount" );
}
 
?>
	<tr class="amountRow" rel="<?php echo $idorder ?>">
		<td class="lefterCol"<?= $rowspanStr ?>><?= $init ?></td>
    	<td<?= $rowspanStr ?>>
    		<a href="<?= $orderHREF ?>" onclick="window.open(this.href); return false;"><?= $idorder ?></a>
    		<img src="<?php echo HTTP_HOST;?>/images/back_office/content/help.png" alt="Commande client <?php echo $idorder; ?>, chargement en cours..." rel="<?php echo $idorder; ?>" class="orderHelp"/>
			
			</td>
    		<input type="hidden" id="idorderList" name="idorderList[]" value="<?= $idorder ?>" />
    		<input type="hidden" id="order_charges_<?= $idorder ?>" value="<?= round( $total_charge_ht, 2 ) ?>" />   		
    		<input type="hidden" id="invoiced_charges_<?= $idorder ?>" value="<?= $invoiced_charges ?>" />
		
			
    	</td>
		<td<?= $rowspanStr ?>><a href="<?= $buyerHREF ?>" onclick="window.open(this.href); return false;"><?= $idbuyer ?></a></td>
    	<td<?= $rowspanStr ?>><a href="<?= $buyerHREF ?>" onclick="window.open(this.href); return false;"><?= htmlentities( $buyerName ) ?><?php if($idbilling_adress!="0"){?><span style="color:red">&nbsp;<strong>(F)</strong></span><?php } ?></a>
		<br /><br />
		<span style="color:#f69814">Siren : <?= $siren ?></span>
		</td>
		<td<?= $rowspanStr ?> style="text-align:right; white-space:nowrap;" class="amount"><?= Util::priceFormat( $total_amount ) ?><?php if($accompte!=0){?><br/><span style="color:red"><strong>Acompte:<br /><?= Util::priceFormat( $accompte ) ;?></strong></span><?php }?></td>
		
		<?php 
		$righterCol = empty( $rowspanStr ) ? "" : " righterCol";
		$isFactor = empty( $b_factor ) ? "Non" : "<span style=\"color:#f69814\">Oui</span>";
		$b_factor = '<td class="'.$righterCol.'" style="white-space:nowrap;" '.$rowspanStr.'>
		<a href="#" class="blueLink" title="Cliquez pour modifier" onclick="updateFactor( \''.$idorder.'\', this ); return false;">'. $isFactor .'</a></td>';  
		  ?>
				<?php

			$last_idbilling_buyer = false;
			$p = 0;
			while( !$rs->EOF() ){
				
				$idbl_delivery 		= $rs->fields( "idbl_delivery" );
				$idorder_supplier 	= $rs->fields( "idorder_supplier" );
				$idsupplier 		= $rs->fields( "idsupplier" );
				$availability_date 	= $rs->fields( "availability_date" );
				$idsupplier 		= $rs->fields( "idsupplier" );
				$dispatch_date 		= $rs->fields( "dispatch_date" ) == "0000-00-00" ? "" : $rs->fields( "dispatch_date" );
				$supplierName 		= $rs->fields( "supplierName" );
				$idbilling_buyer	= $rs->fields( "idbilling_buyer" );
				$payment_delay		= $rs->fields( "payment_delay" );
				$blHREF 			= HTTP_HOST . "/catalog/pdf_delivery.php?iddelivery=$idbl_delivery";
				
				$allowPartialInvoicing = $blCount > 1 && $dispatch_date != "";
				
				if( $p )
					echo "<tr rel='$idorder'>";
			
				?>
				<td>
					<a href="<?=HTTP_HOST?>/sales_force/supplier_order.php?IdOrder=<?php echo $idorder_supplier; ?>" class="lien" onclick="window.open(this.href); return false;"><?php echo $idorder_supplier; ?></a>
					<img src="<?php echo HTTP_HOST;?>/images/back_office/content/help.png" alt="Commande fournisseur <?php echo $idorder_supplier; ?>, chargement en cours..." rel="<?php echo $idorder_supplier; ?>" class="orderSupplierHelp"/></td>
				</td>
		      	<td>
		      		<a href="/product_management/enterprise/supplier.php?idsupplier=<?= $idsupplier ?>" onclick="window.open(this.href); return false;"><?= $supplierName ?></a>
				</td>
               
				<!-- <td><a href="<?= $blHREF ?>" onclick="window.open(this.href); return false;"><?= $idbl_delivery ?></a></td> -->
				<!-- <td><?= $availability_date != "0000-00-00" ? Util::dateFormat( $availability_date , 'd-m-Y' ) : "-" ?></td> -->
				<td><?= Util::dateFormat( $dispatch_date , 'd-m-Y' ); ?></td>
				<!--<td><span style="color:#f69814"><strong><? echo $b_factor; ?></span></strong></td>-->
				
				<?php 
					if( $p == 0 ) { echo $b_factor; }
				
				
					//BLs facturable globalement
					
					if( !$p && $globallyInvoiceableCount ){
						
						$rowspanStr = $globallyInvoiceableCount > 1 ? " rowspan=\"$globallyInvoiceableCount\"" : "";
						$disabled 	= $allowGlobalInvoicing ? "" : " disabled=\"disabled\"";
						
						if( $allowGlobalInvoicing && $partiallyInvoiceableCount ){
							// ------- Mise à jour de l'id gestion des Index  #1161
							$onclick = "if( this.checked ) uncheck( new Array( 'idbl_delivery_" . implode( "', 'idbl_delivery_", $nonInvoicedBls ) . "' ) );";
							
							foreach( $nonInvoicedBls as $nonInvoicedBl )
								$onclick .= " displayChargeEditor( '$idorder', $nonInvoicedBl, false );";
							
							$onclick .= " checkChargeSum( '$idorder' );";
							
						}
						else $onclick = "";
						
						?>
						<td <?= empty($disabled) ? "style='background:#ff6b01;'" : "" ?> class="<?= empty( $rowspanStr ) ? "" : " lefterCol righterCol" ?>" <?= $rowspanStr ?> >
							<input type="checkbox"<?= $disabled ?> name="idorder[]" id="idorder_<?= $idorder ?>" value="<?= $idorder ?>" onclick="<?= $onclick ?>" />
							
							
						</td>
						<?php
				
					}
					else if( $p > $globallyInvoiceableCount - 1 ){ //le BL n'est facturable que partiellement ( cad que c'est le dernier BL à ne pas avoir été facturé partiellement )
						
						?>
						
						<td></td>
						<?php
						
					}
					
			//BLs facturable partiellement
			
			?>
			<td>
			<?php
	
				if( !$idbilling_buyer && $blCount > 1 ){
					
					$disabled 	= $allowPartialInvoicing ? "" : " disabled=\"disabled\"";
					
					if( $allowPartialInvoicing && $globallyInvoiceableCount ){
						// ------- Mise à jour de l'id gestion des Index  #1161
						$onclick = "if( this.checked ){ uncheck( new Array( 'idorder_$idorder' ) );";
						// ------- Mise à jour de l'id gestion des Index  #1161
						if( $total_charge_ht > 0.0 )
							$onclick .= " displayChargeEditor( '$idorder', '$idbl_delivery', true );";
						
						$onclick .= " }";
						// ------- Mise à jour de l'id gestion des Index  #1161
						if( $total_charge_ht > 0.0 )
							$onclick .=" else displayChargeEditor( '$idorder', '$idbl_delivery', false ); checkChargeSum( '$idorder' );";
					
					}
					else $onclick = "";
					
					?>
					<input type="checkbox"<?= $disabled ?> name="idbl_delivery[]" id="idbl_delivery_<?= $idbl_delivery ?>" value="<?= $idbl_delivery ?>" onclick="<?= $onclick ?>" />
					<input type="hidden" id="order_delivery_notes_<?= $idorder ?>" value="<?= $idbl_delivery ?>" />
					<?php
					
				}
				else if( $blCount > 1 ){ //le BL est déjà facturé partiellement, on affiche une case cochée pour info
					
					?>
					<input type="checkbox" disabled="disabled" checked="checked" onclick="return false;" />
					<?php
					
				}
			
			?>
			</td>
			<?php
			
				//frais de port
				if( !$idbilling_buyer || $idbilling_buyer != $last_idbilling_buyer ){
					
					?>
					<td id="ChargeCell_<?= $idbl_delivery ?>" class="righterCol" style="text-align:right; white-space:nowrap;"<?php
		
							$rowspan = 0;
							// ------- Mise à jour de l'id gestion des Index  #1161
							while( $idbilling_buyer && !$rs->EOF() && $rs->fields( "idbilling_buyer" ) == '$idbilling_buyer' ){
								
								$rowspan++;
								$rs->MoveNext();
									
							}
						
							$rs->Move( $p );
							
							if( $rowspan > 1 )
								echo " rowspan=\"$rowspan\"";
						
					?>>
					<?php
				
					//si aucun frais de port à facturifier
					if( $total_charge_ht == 0.0 ){ //s'il n'y a pas de frais de port à facturifier
			
						echo Dictionnary::translate( "gest_com_franco" );
						
						?>
						<input type="hidden" name="total_charge_ht_<?= $idbl_delivery ?>" value="0.0" />
						<?php
						
					}
					else if( !$idbilling_buyer && $blCount > 1 && ( $invoiceCount == $blCount - 1 ) ){
						
						//on impose les frais de port dans le cas où il y a plusieurs BLs et que tous ont été facturés sauf celui-ci
						echo Util::priceFormat( $non_invoiced_charges );
						?>
						<input type="hidden" name="total_charge_ht_<?= $idbl_delivery ?>" value="<?= round( $non_invoiced_charges, 2 ) ?>" />
						<?php
						
					}
					else if( $idbilling_buyer && $idbilling_buyer != $last_idbilling_buyer ){ //si le BL est déjà facturé => on affiche les frais de port facturés
						
						// ------- Mise à jour de l'id gestion des Index  #1161
						$invoiced_charges = DBUtil::getDBValue( "total_charge_ht", "billing_buyer", "idbilling_buyer", '$idbilling_buyer' );
						echo Util::priceFormat( $invoiced_charges );
						?>
						<input type="hidden" name="total_charge_ht_<?= $idbl_delivery ?>" value="<?= round( $non_invoiced_charges, 2 ) ?>" />
						<?php
						
					}
					else if( $blCount == 1 ){ //s'il n'y a qu'un seul BL, il récupèrera tous les frais de port de la commande
						
						echo $total_charge_ht > 0.0 ? Util::priceFormat( $total_charge_ht ) : Dictionnary::translate( "gest_com_franco" );
						?>
						<input type="hidden" name="total_charge_ht_<?= $idbl_delivery ?>" value="<?= round( $total_charge_ht, 2 ) ?>" />
						<?php
						
					}
					//sinon cette cellule reste vide pour la création dynamique d'elements INPUT avec javascript
				
				?>
				</td>
				<?php
			
			}
		?>
		</tr>
		<?php
		
		// ------- Mise à jour de l'id gestion des Index  #1161	
		$last_idbilling_buyer = '$idbilling_buyer';
		$rs->MoveNext();
		$p++;
		
	}
			
}

//--------------------------------------------------------------------------------------------------

/**
 * Retourne le montant des frais de port déjà facturés pour un BL donné
 */
function getInvoicedCharges( $idbl_delivery ){
	
	$query = "
	SELECT bb.total_charge_ht
	FROM billing_buyer bb, bl_delivery bl
	WHERE bl.idbl_delivery = '$idbl_delivery'
	AND bl.idbilling_buyer = bb.idbilling_buyer
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return 0.0;
		
	return $rs->fields( "total_charge_ht" );
	
}

//--------------------------------------------------------------------------------------------------

/**
 * Retourne le montant des frais de port client qui n'ont pas encore été facturés pour une commande donnée
 */
function getDispatchableCharges( $idorder ){
	
	//total frais de port pour la commande
	$total_charge_ht = DBUtil::getDBValue( "total_charge_ht", "order", "idorder", $idorder );
	
	//frais de port déjà facturés
	$query = "SELECT SUM( total_charge_ht ) AS invoicedCharges FROM billing_buyer WHERE idorder = '$idorder'";
	$rs =& DBUtil::query( $query );
	$invoicedCharges = $rs->fields( "invoicedCharges" ) == NULL ? 0.0 : $rs->fields( "invoicedCharges" );
	
	//frais de port non facturés
	return $total_charge_ht - $invoicedCharges;
	
}

//--------------------------------------------------------------------------------------------------

//facturation

function generateInvoices(){
	//print_r($_POST);

	$generatedInvoices = array();
	$factor_payment=DBUtil::getParameterAdmin('factor_payment');
	//$total_vat_2 = $total_amount_2 = $total_vat_1 = $total_amount_1 = 0.00;
	//facturations globales 
	//A Facture
	if ( isset( $_POST[ "idorder" ] )){
		
		foreach( $_POST[ "idorder" ] as $idorder ){
			$idbuy = DBUtil::query( "SELECT idbuyer FROM `order` WHERE idorder = '$idorder' LIMIT 1" )->fields( "idbuyer" );
			$factor = DBUtil::query( "SELECT factor FROM `buyer` WHERE idbuyer = '$idbuy' LIMIT 1" )->fields( "factor" );
			$idfactor=$factor?$factor:0;
			$total_vat_2 = $total_amount_2 = $total_vat_1 = $total_amount_1 = 0.00;
			TradeFactory::createInvoiceFromOrder( new Order( $idorder ) );
			//récupérer la liste des factures générées
			$query = "SELECT idbilling_buyer,idpayment FROM billing_buyer WHERE idorder = '$idorder'";
			$rs =& DBUtil::query( $query );
			
			while( !$rs->EOF() ){
				$generatedInvoices[] = $rs->fields( "idbilling_buyer" );
				$idbilling_buyer = $rs->fields( "idbilling_buyer" );
				$idpay = $rs->fields( "idpayment" );
				//$bil_buyer_factor=$factor?$relaunch_payment:$idpay;
				if($factor_payment){
					$bil_buyer_factor=$factor?$factor_payment:$idpay;
				}
				else{
					$bil_buyer_factor=$idpay;
				}
				echo $bil_buyer_factor;
				echo $factor_payment;
				$q = " SELECT order_row.idorder, order_row.quantity, order_row.discount_price, order_row.vat_rate
						FROM   `order_row`
						WHERE
					
					
						 order_row.idorder = '$idorder'
						";
				$rs1 =& DBUtil::query( $q );

				$total_vat_2 = $total_amount_2 = $total_vat_1 = $total_amount_1 = 0.00;
				while( !$rs1->EOF() ){
					$vat_rate = $rs1->fields( "vat_rate" );
					$quantity = $rs1->fields( "quantity" );
					$price = $rs1->fields( "discount_price" );
					$idorder =  $rs1->fields( "idorder" );
					
					if( B2B_STRATEGY ){
						switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_2 = $total_amount_2 + ($quantity*$price) ;
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + ($quantity*$price) ;
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + ($quantity*$price) ;
							break;
							default:					
							break;
						}
					}	
					else{
						switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 ) / 1.055;
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100) / 1.055;
								$total_vat_2 = round ($total_vat_2, 2 );
								//$total_amount_2 =  ($total_amount_2 + $quantity*$price ) /1.055 ;
								$total_amount_2 = $total_amount_2 + ( $quantity*$price ) /1.055 ;
								$total_amount_2 = round ($total_amount_2, 2 );
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100) / 1.196;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ( $quantity*$price) / 1.196 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100) / 1.20;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ( $quantity*$price) / 1.20 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							default:					
							break;
						}
					}
					$rs1->MoveNext();
				}
				$q = "UPDATE `billing_buyer` 
					  SET 
						`total_amount_1` = '$total_amount_1',
						`total_amount_2` = '$total_amount_2',
						`total_vat_1` = '$total_vat_1',
						`total_vat_2` = '$total_vat_2',
						`idpayment`='$bil_buyer_factor',
						`factor`='$idfactor'
						
						WHERE `billing_buyer`.`idbilling_buyer` = '$idbilling_buyer' ";
			//	echo $q ;
				DBUtil::query( $q );
				
				$rs->MoveNext();
			}
		}

	}
	
	//facturations partielles
	//For partial fatcure
	
	if ( isset( $_POST[ "idbl_delivery" ] )){
		
		$sortedDeliveryNotes = getDeliveryNotesSortedByOrder( $_POST[ "idbl_delivery" ] );
		foreach( $sortedDeliveryNotes as $idorder => $deliveryNotes ){
			$idbyer = DBUtil::query( "SELECT idbuyer FROM `order` WHERE idorder = '$idorder' LIMIT 1" )->fields( "idbuyer" );
			$factor1 = DBUtil::query( "SELECT factor FROM `buyer` WHERE idbuyer = '$idbyer' LIMIT 1" )->fields( "factor" );
			$idfactor1=$factor1?$factor1:0;
			$order = new Order( $idorder );
			$total_charge_ht = 0.0;
			$i = 0;
			while( $i < count( $deliveryNotes ) ){
				$total_charge_ht += floatval( $_POST[ "total_charge_ht_" . $deliveryNotes[ $i ] ] );
				$i++;
			}
		//	$idbilling_buyer = $_POST[ "idbl_delivery" ];
			$invoice =TradeFactory::createInvoiceFromDeliveryNotes( $order, $deliveryNotes, $total_charge_ht );
			$generatedInvoices[] = $invoice->getId();
			$idbilling_buyer = $invoice->getId();
			$query1 = "SELECT idpayment FROM billing_buyer WHERE idbilling_buyer = '$idbilling_buyer'";
			$rs2 =& DBUtil::query( $query1 );
			$idpay=$rs2->fields( "idpayment" );
			//$bil_byer_factor=$factor1?$relaunch_payment:$idpay;
			if($factor_payment){
					$bil_byer_factor=$factor1?$factor_payment:$idpay;
				}
				else{
					$bil_byer_factor=$idpay;
				}
	
			 $q = " SELECT order.idorder, bl_delivery_row.quantity, order_row.discount_price, order_row.vat_rate
						FROM billing_buyer, bl_delivery_row, bl_delivery, `order` , `order_row`
						WHERE `billing_buyer`.`idbilling_buyer` = '$idbilling_buyer'
						AND `billing_buyer`.`idbilling_buyer` = `bl_delivery`.`idbilling_buyer`
						AND `bl_delivery_row`.`idbl_delivery` = `bl_delivery`.`idbl_delivery`
						AND order_row.idorder = order.idorder
						AND bl_delivery.idorder = order.idorder
						AND order_row.idrow = bl_delivery_row.idrow ";
				$rs1 =& DBUtil::query( $q );
				/*while( !$rs1->EOF() ){
					$vat_rate = $rs1->fields( "vat_rate" );
					$quantity = $rs1->fields( "quantity" );
					$price = $rs1->fields( "discount_price" );
					$idorder =  $rs1->fields( "idorder" );
					switch($vat_rate){
						case '5.50':
							$total_vat_2 = $total_vat_2 + $quantity*$price*$vat_rate/100;
							$total_amount_2 = $total_amount_2 + $quantity*$price ;
						break;
						case '19.60':
							$total_vat_1 = $total_vat_1 + $quantity*$price*$vat_rate/100;
							$total_amount_1 = $total_amount_1 + $quantity*$price ;
						break;
						default:					
						break;
					}
					
					$rs1->MoveNext();
				}*/
					while( !$rs1->EOF() ){
					$vat_rate = $rs1->fields( "vat_rate" );
					$quantity = $rs1->fields( "quantity" );
					$price = $rs1->fields( "discount_price" );
					$idorder =  $rs1->fields( "idorder" );
					if( B2B_STRATEGY ){
						switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_2 = round ($total_vat_2, 2 );
								$total_amount_2 = $total_amount_2 + ($quantity*$price) ;
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + ($quantity*$price) ;
							break;
							case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 );
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 );
								$total_vat_1 = round ($total_vat_1, 2 );
								$total_amount_1 = $total_amount_1 + ($quantity*$price) ;
							break;
							default:					
							break;
						}
					}	
					else{
						switch($vat_rate){
							case '5.50':
								//$total_vat_2 = round( $total_vat_2 + $quantity*$price*$vat_rate/100, 2 ) / 1.055;
								$total_vat_2 = $total_vat_2 + ( $quantity*$price*$vat_rate/100 ) / 1.055;
								$total_vat_2 = round ($total_vat_2, 2 );
								//$total_amount_2 =  ($total_amount_2 + $quantity*$price ) /1.055 ;
								$total_amount_2 = $total_amount_2 + ( $quantity*$price ) /1.055 ;
								$total_amount_2 = round ($total_amount_2, 2 );
							break;
							case '19.60':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 ) / 1.196;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ($quantity*$price) / 1.196 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
								case '20.00':
								//$total_vat_1 = round( $total_vat_1 + $quantity*$price*$vat_rate/100, 2 ) / 1.196;
								$total_vat_1 = $total_vat_1 + ( $quantity*$price*$vat_rate/100 ) / 1.20;
								$total_vat_1 = round ($total_vat_1, 2 );
								//$total_amount_1 =  ($total_amount_1 + $quantity*$price) / 1.196 ;
								$total_amount_1 = $total_amount_1 + ($quantity*$price) / 1.20 ;
								$total_amount_1 = round ($total_amount_1, 2 );
							break;
							default:					
							break;
						}
					}
					$rs1->MoveNext();
				}
				
				$q = "UPDATE `billing_buyer` 
					  SET 
						`total_amount_1` = '$total_amount_1',
						`total_amount_2` = '$total_amount_2',
						`total_vat_1` = '$total_vat_1',
						`total_vat_2` = '$total_vat_2',
						`idpayment`='$bil_byer_factor',
						`factor`='$idfactor1'
						WHERE `billing_buyer`.`idbilling_buyer` = '$idbilling_buyer' ";
				//echo $q ;
				DBUtil::query( $q );
			
		}
		
	}
	
	
	return $generatedInvoices;
	
}

//--------------------------------------------------------------------------------------------------

function displayGeneratedInvoices( $generatedInvoices ){
	
	?>
				<div style="width: 100%;height:25px;">
				    <div style="float:left;font-size:14px;font-weight:bold;">
						<?=count($generatedInvoices)?>&nbsp;facture<?= count($generatedInvoices)>1 ? "s" : "" ?> créée<?= count($generatedInvoices)>1 ? "s" : "" ?>
					</div>
					<a style="float:right;" href="com_admin_search_bl.php?output&amp;idinvoices=<?= implode( "-", $generatedInvoices ) ?>" onclick="window.open(this.href); return false;"><img src="<?= HTTP_HOST ?>/images/back_office/content/print.gif" alt="<?= Dictionnary::translate( "print" ) ?>" style="border-style:none; vertical-align:middle;" /></a>
				</div>
				<div class="clear"></div>
				<?php
				
					//pas de facture générée, snirf
					if( !count( $generatedInvoices ) ){
						
						?>
						<p class="msg"><?= Dictionnary::translate( "gest_com_no_invoice_generated" ) ?></p>
						<?php
						
						return;
						
					}
		// ------- Mise à jour de l'id gestion des Index  #1161
		$v="'";
		$idsbuyerstmp=implode( "','", $generatedInvoices );
		$concat= $v . $idsbuyerstmp .$v;
		
		
						//liste des factures générées par le générateur de factures
						$query = "
						SELECT bb.idbilling_buyer,
							bb.DateHeure,
							bb.total_amount_ht,
							bb.total_amount,
							bb.idbuyer,
							bb.idorder,
							bb.idcontact,
							b.company,
							c.title,
							c.firstname,
							c.lastname
						FROM billing_buyer bb, buyer b, contact c
						WHERE bb.idbilling_buyer IN( " . $concat  . " )
						AND bb.idbuyer = b.idbuyer
						AND bb.idbuyer = c.idbuyer
						AND bb.idcontact = c.idcontact
						ORDER BY bb.idbilling_buyer ASC";
					
						$rs =& DBUtil::query( $query );
					
					?>
				<div class="tableContainer">
					<table class="dataTable devisTable">
						<thead>
							<tr>
							 	<th><?= Dictionnary::translate( "gest_com_idbilling_buyer" ) ?></th>
							 	<th>Facture PDF</th>
							 	<th><?= Dictionnary::translate( "gest_com_billing_date" ) ?></th>
							 	<th><?= Dictionnary::translate( "idbuyer" ) ?></th>
							 	<th><?= Dictionnary::translate( "lastname" ) ?></th>
							 	<th>Total factues TTC</th>
							 	<th>Total à payer TTc</th>
							</tr>
						</thead>
						<tbody>
						<?php
						
							$total_amount_ttc = 0.0;
							$total_amount_sum = 0.0;
							
							
							while( !$rs->EOF() ){
								
								$idorder1=$rs->fields( "idorder" );
								$sql2="
								select cash_payment_idregulation, idbilling_adress from `order` where idorder='$idorder1'

								";
								$rs22=& DBUtil::query( $sql2 );
								$id_billing_adresse=$rs22->fields( "idbilling_adress" );
								$cash_payment_idreg=$rs22->fields( "cash_payment_idregulation" );
								$accompte1=0;
								if($cash_payment_idreg!="0") 
								{

								$sql11= "SELECT amount	
									FROM regulations_buyer
									WHERE idregulations_buyer='$cash_payment_idreg'";

									$rs1 =& DBUtil::query($sql11);
								$accompte1	= $rs1->fields( "amount" );
								}
								if( !strlen( $rs->fields( "company" ) ) )
										$customer = $rs->fields( "title" ) . ". " . $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
								else 	$customer = $rs->fields( "company" );
								
								$total_amount_ttc += ($rs->fields( "total_amount" )- $accompte1);
								$total_amount_sum += $rs->fields( "total_amount" );
								
								list( $year, $month, $day ) = explode( "-", substr( $rs->fields( "DateHeure" ), 0, 10 ) );
								
								?> 
								
				
								<tr>
									<td class="lefterCol">
										<a href="<?= HTTP_HOST ?>/accounting/com_admin_invoice.php?IdInvoice=<?= $rs->fields( "idbilling_buyer" ) ?>" onclick="window.open(this.href); return false;"><?= $rs->fields( "idbilling_buyer" ) ?></a>
									</td>
									<td>
										<a href="<?= HTTP_HOST ?>/catalog/pdf_invoice.php?idinvoice=<?= $rs->fields( "idbilling_buyer" ) ?>" onclick="window.open(this.href); return false;"><img src="<?= HTTP_HOST ?>/images/back_office/content/pdf.gif" alt="Facture PDF" style="border-style:none;" /></a>
									</td>
									<td><?= "$day-$month-$year" ?></td>
									<td><?= $rs->fields( "idbuyer" ) ?></td>
									<td><?= htmlentities( $customer ) ?><?php if($id_billing_adresse !="0"){?><span style="color:red">&nbsp;<strong>F</strong></span><?php } ?></td>
									<td style="text-align:right;"><?= Util::priceFormat( $rs->fields( "total_amount" ) ) ?></td>
									<td class="righterCol" style="text-align:right;"><?= Util::priceFormat( $rs->fields( "total_amount" ) -$accompte1) ?></td>
								</tr>
								<?php
								
								$rs->MoveNext();
								
							}
						
							?>
							<tr>
								<td colspan="6" class="lefterCol" style="font-weight:bold; text-align:right;"><?= Util::priceFormat( $total_amount_sum ) ?></td>
								<td class="righterCol" style="font-weight:bold; text-align:right;"><?= Util::priceFormat( $total_amount_ttc ) ?></td>
							</tr>
							
						</tbody>
					</table>
				</div>
	<?php
		
}

//--------------------------------------------------------------------------------------------------

function outputGeneratedInvoices( $generatedInvoices ){
	
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFMultiplePDF.php" );
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
		
	$outputFileName = "invoices.pdf";
	
	$pdf_list = array();
	foreach( $generatedInvoices as $idinvoice ){
		$pdf_list[] = new ODFInvoice( $idinvoice );				
	}
		

	$pdfM = new ODFMultiplePDF( $pdf_list , $outputFileName , "I" , false );
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?= $prev ?> ); return false;"><img src="<?= HTTP_HOST ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		?>
		<a href="#" onclick="goToPage( <?= $i ?> ); return false;"><?= $i ?></a>
		<?php
		$i++;
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		?>
		<a href="#" onclick="goToPage( <?= $i ?> ); return false;"><?= $i ?></a>
		<?php
		$i++;
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?= $next ?> ); return false;"><img src="<?= HTTP_HOST ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------

function getDeliveryNotesSortedByOrder( $deliveryNotes ){
	
	$sorted = array();
	foreach( $deliveryNotes as $idbl_delivery ){
		$idorder = DBUtil::getDBValue( "idorder", "bl_delivery", "idbl_delivery", $idbl_delivery );
		if( !isset( $sorted[ strval( $idorder ) ] ) )
			$sorted[ strval( $idorder ) ] = array();
			
		$sorted[ strval( $idorder ) ][] = $idbl_delivery;
	}
	
	return $sorted;
	
}

//--------------------------------------------------------------------------------------------------

function getTooltipFactor( $idorder ){

	$order = new Order( $idorder );
	$customer = $order->getCustomer();
	$idpayment_delay = DBUtil::getDBValue( "idpayment_delay", "payment_delay", "payment_code", "CD" );
	
	$payment_code = false;
	if( $order->get( "idpayment_delay" ) == $idpayment_delay ){
		$payment_code = true;
	}
	
	$insurance_status = "";
	switch( $customer->get( 'insurance_status' ) ){
		
		case "Accepted":
			$insurance_status = '<strong>Client assuré</strong>';
			break;
			
		case "Refused":
			$insurance_status = '<strong style="color:#FF0000;">Client refusé</strong>';
			break;
	
		case "Deleted":
			$insurance_status = '<strong style="color:#FF0000;">Client supprimé</strong>';
			break;
			
		case "Terminated":
			$insurance_status = '<strong style="color:#FF0000;">Client résilié</strong>';
			break;
	
		default:
			$insurance_status = '<strong style="color:#FF0000;">Client non assuré</strong>';
			break;
			
	}
	
	$invoiceAdress = $order->getInvoiceAddress();
	$billingCountry = DBUtil::getDBValue( "name_1", "state", "idstate", $invoiceAdress->getIdState() );
	$customerProfile = DBUtil::getDBValue( "name", "customer_profile", "idcustomer_profile", $customer->get( 'idcustomer_profile' ) );
	
	$tooltip_content = $order->get( 'cach_payment' ) == 1 ? '<strong style="color:#FF0000;">Paiement comptant</strong><br />' : "";
	$tooltip_content .= "<strong>Mode de paiement : </strong>";
	$tooltip_content .= $order->get( 'idpayment' ) > 0 ? DBUtil::query( "SELECT name_1 FROM payment WHERE idpayment = " . $order->get( 'idpayment' ) . " LIMIT 1" )->fields('name_1') . '<br />' : "<strong style='color:#FF0000;text-transform:uppercase;'>non défini</strong><br />";
	$tooltip_content .= $payment_code ? '<strong style="color:#FF0000;">Paiement à la commande</strong><br />' : "";
	$tooltip_content .= "<strong>Délai de paiement : </strong>" . DBUtil::getDBValue( "payment_delay_1", "payment_delay", "idpayment_delay", $order->get( "idpayment_delay" ) ) . "<br />";
	$tooltip_content .= $insurance_status . "<br />";
	$tooltip_content .= $order->get( 'insurance' ) ? '<strong style="color:#FF0000;">Commande non assurée</strong><br />' : "<strong>Commande assurée</strong><br />";
	$tooltip_content .= $order->get( 'total_amount' ) < DBUtil::getParameterAdmin( "factor_minbuying" ) || $order->get( 'total_amount' ) > DBUtil::getParameterAdmin( "factor_maxbuying" ) && DBUtil::getParameterAdmin( "factor_maxbuying" ) != 0 ? '<strong style="color:#FF0000;">Montant non couvert</strong><br />' : "";
	$tooltip_content .= $invoiceAdress->getIdState() == DBUtil::getParameter( "default_idstate" ) ? "<strong>Pays de facturation : </strong>" . stripslashes( $billingCountry ) : '<span style="color:#FF0000; font-weight:bold;"><strong>Pays de facturation : </strong>' . stripslashes( $billingCountry ) . '</span>';
	$tooltip_content .= "<br /><strong>Type d\'acheteur : </strong>" . stripslashes( $customerProfile );
	
	return $tooltip_content;
	
}

//--------------------------------------------------------------------------------------------------

function getTooltipOrder( $idorder ){
	include_once( dirname( __FILE__ ) . "/../objects/Supplier.php" );

	$order = new Order( intval( $idorder ) );
	
	$it = $order->getItems()->iterator();
	
	$itemList = "";
	
	while( $it->hasNext() ){
		
		$item =& $it->next();
		
		$supplier = new Supplier( $item->get( "idsupplier" ) );
		$itemList .= "<tr>";
		$itemList .= "<td class='lefterCol'>" . $item->get( "reference" ) . "</td>" ;
		$itemList .= "<td>" . $item->get( "ref_supplier" ) . "</td>" ;
		$itemList .= "<td>" . $supplier->getName() . "</td>" ;
		$itemList .= "<td>" . stripslashes( $item->get( "summary" ) ) . "</td>" ;
		$itemList .= "<td>" . $item->get( "quantity" ) . "</td>" ;
		$itemList .= "<td class='righterCol'>" . Util::priceFormat( $item->get( "discount_price" ) ) . "</td>" ;
		$itemList .= "</tr>";
		
	}

	$tooltip_content = "
	<strong>Commande client n°$idorder</strong>
	<div class='content' style='padding:5px;'>
		<div class='subContent'>
			<div class='resultTableContainer'>
				<table class='dataTable resultTable' style='white-space:nowrap;'>
					<thead>
						<tr>
							<th>Réf.</th>
							<th>Réf. fourn.</th>
							<th>Fournisseur</th>
							<th>Designation</th>
							<th>Quantité</th>
							<th>Prix vente</th>
						</tr>
					</thead>
					<tbody>
						$itemList
					</tbody>
				</table>
			</div>
		</div>
	</div>
	";
	
	return $tooltip_content;
}

//--------------------------------------------------------------------------------------------------

function getTooltipOrderSupplier( $idorder_supplier ){
	include_once( dirname( __FILE__ ) . "/../objects/SupplierOrder.php" );
	
	$order_supplier = new SupplierOrder( intval( $idorder_supplier ) );
	$supplierName = $order_supplier->getSupplier()->getName();
	
	$it = $order_supplier->getItems()->iterator();
	$itemList = "";
	
	while( $it->hasNext() ){
		
		$item =& $it->next();

		$itemList .= "<tr>";
		$itemList .= "<td class='lefterCol'>" . $item->get( "reference" ) . "</td>" ;
		$itemList .= "<td>" . $item->get( "ref_supplier" ) . "</td>" ;
		$itemList .= "<td>" . stripslashes( $item->get( "summary" ) ) . "</td>" ;
		$itemList .= "<td>" . $item->get( "quantity" ) . "</td>" ;
		$itemList .= "<td class='righterCol'>" . Util::priceFormat( $item->get( "discount_price" ) ) . "</td>" ;
		$itemList .= "</tr>";
		
	}	
	
	$tooltip_content = "<strong>Commande fournisseur " . strtoupper( $supplierName ) . " n°$idorder_supplier</strong>
	<div class='content' style='padding:5px;'>
		<div class='subContent'>
			<div class='resultTableContainer'>
				<table class='dataTable resultTable' style='white-space:nowrap;'>
					<thead>
						<tr>
							<th>Réf.</th>
							<th>Réf. fourn.</th>
							<th>Designation</th>
							<th>Quantité</th>
							<th>Prix achat</th>
						</tr>
					</thead>
					<tbody>
						$itemList
					</tbody>
				</table>
			</div>
		</div>
	</div>	";

	return $tooltip_content;
}

//--------------------------------------------------------------------------------------------------
?>
