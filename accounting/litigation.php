<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des litiges clients
 */
	
//----------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/Litigation.php" );

//----------------------------------------------------------------------------
/*suppression d'un litige*/

if( isset( $_GET[ "idlitigation" ] ) && isset( $_GET[ "delete" ] ) ){
	// ------- Mise à jour de l'id gestion des Index  #1161
	Litigation::delete(  $_GET[ "idlitigation" ] , $_GET[ "type" ] );
	
	header( "Location: $GLOBAL_START_URL/accounting/litigation_search.php" );
	
	exit();
	
}

//----------------------------------------------------------------------------
/*création d'un nouveau litige*/

if( ( isset( $_GET[ "idbilling_buyer" ] ) || isset( $_GET[ "idorder" ] ) ) && isset( $_GET[ "idrow" ] ) ){
	
	if( isset( $_GET[ "idbilling_buyer" ] ) ){
	// ------- Mise à jour de l'id gestion des Index  #1161
		$invoice = new Invoice( $_GET[ "idbilling_buyer" ]  );
		$litigation =& Litigation::create( $invoice->getItemAt( intval( $_GET[ "idrow" ] ) - 1 ) , Litigation::$TYPE_BILLING_BUYER );
		$typeLi = Litigation::$TYPE_BILLING_BUYER;
	}else{// ------- Mise à jour de l'id gestion des Index  #1161
		$order = new Order( $_GET[ "idorder" ]  );
		$litigation =& Litigation::create( $order->getItemAt( intval( $_GET[ "idrow" ] ) - 1 ) , Litigation::$TYPE_ORDER );
		$typeLi = Litigation::$TYPE_ORDER;
	}

	header( "Location: $GLOBAL_START_URL/accounting/litigation.php?idlitigation=" . $litigation->getIdLitigation() ."&typeLitigation=".$typeLi );
	
	exit();
	
}

//----------------------------------------------------------------------------
/*PDF BL de retour*/

if( isset( $_GET[ "output_delivery_note" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../objects/odf/ODFReturnToSender.php" );
	
	$odf = new ODFReturnToSender( intval( $_GET[ "idorder_supplier" ] ), intval( $_GET[ "idrow" ] ), $_GET[ "location" ] );

	$odf->exportPDF( "BL de retour.pdf" );

	exit();

}

//----------------------------------------------------------------------------
/*édition d'un litige existant*/
// ------- Mise à jour de l'id gestion des Index  #1161
if( isset( $_REQUEST[ "idlitigation" ] ) && isset( $_REQUEST[ "typeLitigation" ] )){
	$litigation = new Litigation( $_REQUEST[ "idlitigation" ]  , $_REQUEST[ "typeLitigation" ] );
}else{
	
	trigger_error( "paramètre absent", E_USER_ERROR );
	exit();
	
}


//----------------------------------------------------------------------------
/*validation et vérouillage d'un litige*/

if( isset( $_REQUEST[ "validate" ] ) && isset( $_REQUEST[ "typeLitigation" ] ) ){
	
	$litigation->validate();

	header( "Location: $GLOBAL_START_URL/accounting/litigation.php?idlitigation=" . $litigation->getIdLitigation()."&typeLitigation=".$_REQUEST[ "typeLitigation" ] );
	
	exit();
	
}

//----------------------------------------------------------------------------

$editable = true;

if( $litigation->getGoodsDeliveryLocation() !=  Litigation::$GOODS_DELIVERY_LOCATION_LOCAL
	&& $litigation->getWayoutRequireGoodsDelivery() )
	$editable = false;

//----------------------------------------------------------------------------

/**
 * affichage de la page
 */
 
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
<script type="text/javascript">
/* <![CDATA[ */
	
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
		        beforeSubmit:  preSubmitCallback,  // pre-submit callback 
		        success:       postSubmitCallback  // post-submit callback 
  
		    };
    
    		$('#LitigationForm').ajaxForm( options );
			
		});
        
        /* ----------------------------------------------------------------------------------- */
        
        function preSubmitCallback( formData, jqForm, options ){
        	
        	//var queryString = $.param(formData);
        	//alert('About to submit: \n\n' + queryString); 
        	
        }
        
        /* ----------------------------------------------------------------------------------- */
        
        function postSubmitCallback( responseXML, statusText ){ 

    		if( statusText != 'success' || responseXML == '0' ){
    			
    			alert( 'Impossible de sauvegarder les modifications' + responseXML );
    			return;
    			
    		}

	  		parseXMLResponse( responseXML );
	
			$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
			$.blockUI.defaults.css.fontSize = '12px';
        	$.growlUI( '', 'Modifications enregistrées' );
        	
        	resetRepaymentEditor();
        	updateWayOut();
        	updateParameters();
        	updateToolBoxes();
        	
        }
        
        /* ----------------------------------------------------------------------------------- */
		
		function postData( data, onErrorMessage ){
		
			$.ajax({
			 	
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/litigation_parseform.php",
				async: true,
				type: "POST",
				data: data,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( onErrorMessage ); },
			 	success: function( responseXML ){
   					postSubmitCallback( responseXML, 'success' );
					
				}
	
			});
		
		}
		
        /* ----------------------------------------------------------------------------------- */
        
        function parseXMLResponse( xmlDocument ){
        
        	var updatedQuantity = xmlDocument.getElementsByTagName( 'quantity' ).item( 0 ).getAttribute( 'value' );
        	
        	if( document.getElementById( 'quantity' ).value != updatedQuantity )
        		alert( 'La quantité sélectionnée n\'est pas valide' );
        	
        	document.getElementById( 'quantity' ).value = xmlDocument.getElementsByTagName( 'quantity' ).item( 0 ).getAttribute( 'value' );
			
        }

		/*-------------------------------------------------------------------------*/

		function createRepaymentEditor(){
							
			$.blockUI({ 
	
				message: $('#RepaymentEditor'),
				css: { padding: '15px', cursor: 'pointer', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' }
				
			});
	
		}
		
		/*-------------------------------------------------------------------------*/
		
		function createRepayment(){
		
			var payments 		= document.getElementById( 'idpayment' );
			var idpayment 		= payments.options[ payments.selectedIndex ].value;
			var repaymentAmount = floatValue( document.getElementById( 'repaymentAmount' ).value );
	var idbuyer ='<?php echo $litigation->getCustomer()->get( "idbuyer" );  ?>';
	
			if( repaymentAmount <= 0.0 ){
			
				alert( 'Le montant saisi est invalide' );
				return;
				
			}
			
			postData( "idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&idbuyer=<?php echo $litigation->getCustomer()->get( "idbuyer" );  ?>&create_repayment=1&idpayment=" + idpayment + "&repaymentAmount=" + repaymentAmount + "&idbuyer=" + idbuyer, "Impossible d'enregistrer le remboursement" );

		}

		/* ----------------------------------------------------------------------------------- */
		
		function dropRepayment( idrepayment ){
		
			var dropCreditParameter = '';
			
			if( confirm( "Le remboursement est sur le point d'être supprimé. Voulez-vous également supprimer l'avoir qui lui est associé?" ) )
				dropCreditParameter = '&drop_credit=1';
				
			postData( "idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&drop_repayment=" + idrepayment + dropCreditParameter, "Impossible de supprimer le remboursement" );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function createExchange(){
		
			postData( "idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&exchange=1", "Impossible de procéder à un échange" );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function dropExchange( idexchange ){
		
			var dropCreditParameter = '';
			
			if( confirm( "La commande d'échange est sur le point d'être supprimée. Voulez-vous également supprimer l'avoir qui lui est associé?" ) )
				dropCreditParameter = '&drop_credit=1';
				
			postData( "idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&drop_exchange=" + idexchange + dropCreditParameter, "Impossible d'annuler l'échange" );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function createCredit(){
		
			postData( "idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&credit=1", "Impossible de créer un nouvel avoir" );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function dropCredit( idcredit ){
		
			postData( "idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&drop_credit=" + idcredit , "Impossible de supprimer l'avoir" );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
			
		function floatValue( text ){
		
			return parseFloat( text.replace( ',', '.' ) );
			
		}
		
		/* ----------------------------------------------------------------------------------- */
	    
	    function resetRepaymentEditor(){
	    
	    	document.getElementById( 'idpayment' ).selectedIndex = 0;
	
	    	document.getElementById( 'repaymentAmount' ).value = '0.0';
	    	var vat = <?php echo $litigation->getItem() ? $litigation->getItem()->get( "vat_rate" ) : 0; ?>;
	    	var amount = <?php echo ( $litigation->getItem() ? $litigation->getItem()->get( "discount_price" ) . " * ( 1.0 + vat / 100.0 ) * parseInt( document.getElementById( 'quantity' ).value )" : 0 ) ?>; 
			document.getElementById( 'repaymentAmount' ).value = amount;
			document.getElementById( 'CreateRepayment' ).value = '0';
			document.getElementById( 'idbuyer' ).value = '0';
	    }

        /* ----------------------------------------------------------------------------------- */
        
        function updateWayOut(){
        
       		$.ajax({
		 	
				url: "<?php echo $GLOBAL_START_URL ?>/templates/accounting/litigation/wayout.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&ajax",
				async: true,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( 'Impossible de recharger les issues du litige' ); },
			 	success: function( responseText ){
	
			 		document.getElementById( 'wayout' ).innerHTML = responseText;
					
				}
	
			});
			
       	}
       	
       	/* ----------------------------------------------------------------------------------- */
        
        function updateToolBoxes(){
        
       		$.ajax({
		 	
				url: "<?php echo $GLOBAL_START_URL ?>/templates/accounting/litigation/toolboxes.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&ajax",
				async: true,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ return; },
			 	success: function( responseText ){
	
			 		document.getElementById( 'toolBoxes' ).innerHTML = responseText;
					
				}
	
			});
			
       	}
       	
       	/* ----------------------------------------------------------------------------------- */
        
        function updateParameters(){
        
       		$.ajax({
		 	
				url: "<?php echo $GLOBAL_START_URL ?>/templates/accounting/litigation/litigation.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&ajax",
				async: true,
				error : function( XMLHttpRequest, textStatus, errorThrown ){ return; },
			 	success: function( responseText ){
	
			 		document.getElementById( 'parameters' ).innerHTML = responseText;
					$( '#idlitigation_motive' ).dxCombobox();
					
				}
	
			});
			
       	}
       	
        /* ----------------------------------------------------------------------------------- */
        
        function ignoreDeliveryFailure(){
        
        	postData( 'idlitigation=<?php echo $litigation->getIdLitigation() ?>&wayout_require_goods_delivery=0' );
        	
        }
        
        /* ----------------------------------------------------------------------------------- */
        
	/* ]]> */
</script>
<div id="globalMainContent">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<form id="LitigationForm" method="post" action="<?php echo $GLOBAL_START_URL ?>/accounting/litigation_parseform.php">
		<input type="hidden" name="typeLitigation" value="<?php echo $litigation->getType(); ?>" />
		<input type="hidden" name="idlitigation" value="<?php echo $litigation->getIdLitigation(); ?>" />
		<?php include( "$GLOBAL_START_PATH/templates/accounting/litigation/customer.php" ); ?>
		<div id="toolBoxes">
			<?php include( "$GLOBAL_START_PATH/templates/accounting/litigation/toolboxes.php" ); ?>
		</div>
		<div id="parameters">
			<?php include( "$GLOBAL_START_PATH/templates/accounting/litigation/litigation.php" ); ?>
		</div>
		<?php 
		if( $litigation->getItem() )
			include( "$GLOBAL_START_PATH/templates/accounting/litigation/product.php" );
		?>
		<div id="wayout">
			<?php include( "$GLOBAL_START_PATH/templates/accounting/litigation/wayout.php" ); ?>
		</div>
		<div class="mainContent fullWidthContent">
			<div class="topRight"></div>
			<div class="topLeft"></div>
		    <div class="content">
    			<div class="subContent">
	        		<div class="subTitleContainer">
		                <p class="subTitle">Commentaires</p>
		        	</div><!-- subTitleContainer -->
		        	<textarea<?php if( !$litigation->isEditable() ) echo " disabled=\"disabled\""; ?> name="comment" id="comment" style="width:100%; height:120px; margin:10px 0px 5px 0px; border:1px solid #AAAAAA; color:#44474E;"><?php echo htmlentities( $litigation->getComment() ) ?></textarea>
		        	<?php
		        	
		        		if( $litigation->isEditable() ){
		        			
		        			?>
				        	<div class="leftContainer">
						    	<input class="blueButton" type="submit" name="" value="Enregistrer" />
						    </div>
						    <?php
						    
		        		}
		        		
		        	?>
				    <div class="clear"></div>
		        </div><!-- subContent -->
			</div><!-- content -->
			<div class="bottomRight"></div>
			<div class="bottomLeft"></div>
		</div><!-- mainContent -->
		<div class="submitZone fullWidthContent">
			<div style="float:right;">
			<?php
			
				if( $litigation->isEditable() ){

            		?>
            		<input type="button" name="deleteLitigationButton" value="Supprimer" class="blueButton" onclick="if( !confirm( 'Voulez-vous vraiment supprimer ce litige ainsi que toutes ses issues ( avoirs, remboursements, etc... ) ?' ) ) return false; document.location = 'litigation.php?delete&amp;idlitigation=<?php echo $litigation->getIdLitigation() ?>&type=<?php echo $litigation->getType(); ?>';" />
            		<?php
            		
            			if( !$litigation->getWayoutRequireGoodsDelivery() || $litigation->getGoodsHasBeenDelivered() ){
            		
		            		?>
		            		<input style="margin-left:15px;" type="button" name="validateLitigationButton" value="Litige résolu" class="blueButton" onclick="document.location='<?php echo $GLOBAL_START_URL ?>/accounting/litigation.php?idlitigation=<?php echo $litigation->getIdLitigation() ?>&typeLitigation=<?php echo $litigation->getType(); ?>&validate';" />
		            		<?php
		            		
            			}
            		
            	}
            	
            ?>
			</div>
		</div><!-- submitZone -->
	</form>
</div><!-- globalMainContent -->
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------------

?>