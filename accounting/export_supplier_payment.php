<?php

/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Export règlements fournisseurs
 */

include_once( "../objects/classes.php" );

//---------------------------------------------------------------------------------------
// Vérifiaction du login 
//---------------------------------------------------------------------------------------

//connexion
if( isset( $_POST[ "Login" ] ) && isset( $_POST[ "Password" ] ) )
	User::getInstance()->login( stripslashes( $_POST[ "Login" ] ), md5( stripslashes( $_POST[ "Password" ] ) ) );
	
if( !User::getInstance()->getId() ){
	
	$banner = "no";
	
	include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "expired_session" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

if( isset($_GET['query']) && $_GET['query']!='' ){
	$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "query" ] ), $GLOBAL_DB_PASS );
}else{
	$query = "
	SELECT billing_supplier.billing_supplier,
		billing_supplier.Date, 
		billing_supplier.idsupplier,
		billing_supplier.status,
		regulations_supplier.payment_date,
		regulations_supplier.deliv_payment,
		regulations_supplier.idpayment,
		supplier.name,
		user.initial,
		regulations_supplier.amount
	FROM billing_supplier, user, supplier, billing_regulations_supplier, regulations_supplier
	WHERE $SQL_Condition
	ORDER BY supplier.name ASC, billing_supplier.billing_supplier ASC";
}

$rs =& DBUtil::query( $query );

$exportableArray = array();
$headers = array(

        "Date de facturation",
		"N° facture",       
        "Fournisseur",
        "Débit",
        "Total par fournisseur",
        "Date d'échéance",
        "Date de paiement"
);

foreach( $headers as $header )
        $exportableArray[ $header ] = array();

$total = 0;
$i=0;

while( !$rs->EOF() ){

	$DateHeure = $rs->fields( "Date" );
	if( $DateHeure != "0000-00-00" ){
		
	    list( $year, $month, $day ) = explode( "-", $DateHeure );
		$DateHeure = "$day/$month/$year";
	
	}
	else $DateHeure = "";
	
	$deliv_payment =  $rs->fields( "deliv_payment" );
	if($deliv_payment!='0000-00-00'){
		list( $year, $month, $day ) = explode( "-", $deliv_payment );
		$deliv_payment = "$day/$month/$year";
	}else{
		$deliv_payment = '';
	}
	
	$payment_date =  $rs->fields( "payment_date" );
	list( $year, $month, $day ) = explode( "-", $payment_date );
	if($payment_date!='0000-00-00'){
		$payment_date = "$day/$month/$year";
	}else{
		$payment_date = '';
	}
	
 	$exportableArray[ "Date de facturation" ][] = $DateHeure;
 	$exportableArray[ "N° facture" ][] = $rs->fields( "billing_supplier" );
 	$exportableArray[ "Fournisseur" ][] = $rs->fields( "name" );
   	$exportableArray[ "Débit" ][] = str_replace( ".", ",", round( $rs->fields( "amount" ), 2 ) );
    
    $idsupplier = $rs->fields( "idsupplier" );
    $total+=$rs->fields( "amount" );
   
    $rs->MoveNext();
    $idnextsupplier =  $rs->fields( "idsupplier" );
    
    if($idsupplier!=$idnextsupplier){
    	$exportableArray[ "Total par fournisseur" ][] = str_replace( ".", ",", round( $total, 2 ));
   		$total = 0;
   	}else{
    	$exportableArray[ "Total par fournisseur" ][] = '';
    }
    
    $rs->Move($i);
     
    $exportableArray[ "Date d'échéance" ][] = $deliv_payment;
    $exportableArray[ "Date de paiement" ][] = $payment_date;
 	
   	$rs->MoveNext();
	$i++;
}

exportArray( &$exportableArray );

//-------------------------------------------------------------------------------


function exportArray( &$exportableArray ){

        global $GLOBAL_START_PATH;

        include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );

        $cvsExportArray = new CSVExportArray( $exportableArray, "règlements_fournisseurs" );
        $cvsExportArray->export();

}

//-------------------------------------------------------------------------------


?>