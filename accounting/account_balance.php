﻿<?php
/*
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Relation entre les écritures comptables et bancaires
 */
//------------------------------------------------------------------------------------------

set_time_limit( 180 );

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( "$GLOBAL_START_PATH/objects/AccountPlan.php" );
include_once( "$GLOBAL_START_PATH/objects/Util.php" );
include_once( "$GLOBAL_START_PATH/objects/URLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/Dictionnary.php" );

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) && $_POST[ "search" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	searchResults();

	exit();

}

//------------------------------------------------------------------------------------------
/* chargement ajax du moteur de recherche */

if( isset( $_GET[ "searchEngine" ] ) ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	searchForm();

	exit();

}


//------------------------------------------------------------------------------------------

if( isset( $_GET[ "getModelTable" ] ) && $_GET[ "getModelTable" ] == "1" ){

	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );

	if( !User::getInstance()->getId() )
		exit( "Session expirée" );

	getModelTable( $_GET[ "idmodel" ] );

	exit();

}





//--------------------------------------------------------------------------------------------------

$Title = "Equilibre des Ecritures";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------------

$lang = User::getInstance()->getLang();

//---------------------------------------------------------------------------------------------

$generatedRows = 0;

$start = microtime( true );

$startDateTime = date( "Y-m-d H:i:s" );

//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript">
<!--

	$(document).ready(function(){

		$('#loading').hide();


	});

	/* ----------------------------------------------------------------------------------- */
	/* Affiche dans un blockUI le HTML de la fenêtre de création d'une nouvelle écriture   */
	/* ----------------------------------------------------------------------------------- */

	function getNewEntryWindow(){

		$('#newEntry').html('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." style="display:block; margin:auto;" />');

		$.blockUI({
			message: $('#newEntry'),
			css: {
				'color': '#586065',
				cursor: 'default',
				'font-family': 'Arial, Helvetica, sans-serif',
				'font-size': '12px',
				'font-weight': 'normal',
				left: ( $(window).width() - 1000 ) / 2 + 'px',
				'padding': '15px 10px 10px 5px',
				'text-align': 'left',
				'top': '100px',
				'width': '970px',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0,
			fadeOut: 700
		});

		$('.blockMsg').append('<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/closebox.png" class="blockOverlay" style="position:absolute; top:-15px; right:-15px;" />');

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?windowEntry=1',
			success: function( response ){ $('#newEntry').html(response); }
		});

		$('.blockOverlay').attr('title','Cliquez pour fermer la popup').click($.unblockUI);

	}

	/* ----------------------------------------------------------------------------------- */
	/* Modification manuelle du lettrage d'une écriture                                    */
	/* ----------------------------------------------------------------------------------- */

	function setNewLettering( value, idrow ){

		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?setNewLettering=1&value=' + value + '&idrow=' + idrow,
			success: function( response ){
				if( response == "0" )
					$.growlUI('Lettrage modifié avec succès','');
				else if( response == "1" )
					return true;
				else
					$.growlUI(response,'');
			}
		});

	}


	function getModelTable( idmodel ){

		if( idmodel == '' )
			$('#windowModelTableContainer').html('');
		else{
			$('#idmodel').val(idmodel);
			$.ajax({
				type: 'GET',
				url: '<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?getModelTable=1&idmodel=' + idmodel,
				success: function( response ){
					$('#windowModelTableContainer').html(response);
				}
			});
		}

	}

	/* ----------------------------------------------------------------------------------- */

	

	/* ----------------------------------------------------------------------------------- */

	function toggleSubAccounts( main_account ){

		if( $( ".subAccount_" + main_account ).css( "display" ) == "none" ){

			$( ".subAccount_" + main_account ).show();
			$( "#account_" + main_account ).addClass( "openedAccount" );

		}else{

			$( ".subAccount_" + main_account ).hide();
			$( "#account_" + main_account ).removeClass( "openedAccount" );

		}

	}

	/* ----------------------------------------------------------------------------------- */

	
	/* ----------------------------------------------------------------------------------- */

	

	/* ----------------------------------------------------------------------------------- */

-->
</script>
<div id="globalMainContent">
	<form action="#">
		<div id="dropEntryConfirmationDiv" style="display:none;">
		<div style="margin:15px 5px;">Êtes-vous certain de vouloir supprimer cette ligne&nbsp;?</div>
			<input type="hidden" value="" id="toDeleteEntry" />
			<input type="button" value="Annuler" class="orangeButton" onclick="$.unblockUI();" />
			<input type="button" value="Continuer" class="blueButton" onclick="dropEntry();" />
		</div>
	</form>
	<div id="loading" style="color:#44474E; font-size:13px; font-weight:bold; line-height:40px; padding:20px; text-align:center;">
		<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/loading.gif" alt="Chargement..." /><br />
		Génération des écritures en cours...
	</div>
<?php

if( strlen( ob_get_contents() ) )
	ob_flush();



//print $generatedRows;
?>
	<div id="exportsDiv" style="display:none;"></div>
	<div id="newEntry" style="display:none;"></div>
	<div id="modelEntry" style="display:none;"></div>
	<div id="checkLetteringDiv" style="display:none;">
		<div id="checkLetteringDivContent" style="overflow:auto;"></div>
	</div>
	<div class="mainContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content" id="searchFormContainer">
			<?php searchForm(); ?>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<div id="tools" class="rightTools" style="position:relative;">
		<div class="toolBox">
			
			
			
		</div>
		<div id="fiscalYearsToolbox" class="toolBox" style="display:none; position:absolute;">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Exercices comptables</p>
			</div>
			<div class="content" style="padding:5px;">
				<div id="fiscalYearsList"><?php AccountPlan::displayFiscalYears(); ?></div>
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<div id="helpToolbox" class="toolBox" style="display:none; position:absolute;">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Aide</p>
			</div>
			<div class="content" style="padding:5px;">
				Les écritures ne sont générées qu'une fois par jour en début de journée. Seules les pièces antérieures à la date du jour sont concernées.
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<?php if( isset( $generatedRows ) && $generatedRows > 0 ){ $end = microtime( true ); ?>
		<div class="toolBox">
			<div class="header">
				<div class="topRight"></div><div class="topLeft"></div>
				<p class="title">Infos</p>
			</div>
			<div class="content">
				<div style="padding:10px 5px 0 5px; text-align:center;">
					<span style="font-weight:bold;"><?php echo $generatedRows ?></span> lignes générées en<br />
					<span style="font-weight:bold;"><?php echo round( microtime( true ) - $start, 1 ) ?></span> secondes<br />
					<a href="#" onclick="displayDetail(); return false;">Voir le détail</a>
				</div>
				<script type="text/javascript">
				<!--

					function displayDetail(){

						$.ajax({
							type: "POST",
							url: "<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php",
							data: "interval_select=2&start=<?php echo $startDateTime ?>&end=<?php echo date( "Y-m-d H:i:s" ) ?>&search=1&detail=1&orderBy=idaccount_plan ASC, idrelation ASC, idaccount_record ASC",
							beforeSend: preSubmitCallback,
							success: postSubmitCallback
						});
						
					}
					
				-->
				</script>
			</div>
			<div class="footer">
				<div class="bottomLeft"></div>
				<div class="bottomMiddle"></div>
				<div class="bottomRight"></div>
			</div>
		</div>
		<?php } ?>
	</div>
	<div id="SearchResults"></div>
</div> <!-- GlobalMainContent -->
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function searchForm(){

	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
	include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

	$formFields = array(

		$fieldName = 
		"idaccount_record"		 			=> "",
		"idrelation"			 			=> "",
		"idsupplier"			 			=> "",
		"record_type"						=> "",
		"amount"	 						=> "",
		"lettering"				 			=> "",
		"orderBy"							=> "idrelation ASC"

	);

	$postData = array();

	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;

?>
			<style type="text/css">
			<!--

				.mainAccount:hover{
					background-color:#F5F5F5;
				}

				.openedAccount{
					background-color:#F5F5F5;
				}

				.openedAccount:hover{
					background-color:#F5F5F5;
				}

				th.noTopBorder{
					border-top-style:none;
				}

				td.modifiableLetterCell{
					cursor:pointer;
				}

				div.blockUI input.textInput,
				div.blockUI input#txt_account_plan_1{
					border:1px solid #AAAAAA;
					color:#44474E;
					font-family:Arial, Helvetica, sans-serif;
					font-size:11px;
					width:100%;
				}

				#ui-datepicker-div,
				.ui-datepicker{ z-index:2000; }

			-->
			</style>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
			<script type="text/javascript">
			<!--

				/* ----------------------------------------------------------------------------------- */

				$(document).ready(function() {

					 var options = {

						beforeSubmit:  preSubmitCallback,  // pre-submit callback
						success:	   postSubmitCallback  // post-submit callback

					};

					$('#SearchForm').ajaxForm( options );

				});

				/* ----------------------------------------------------------------------------------- */

				function preSubmitCallback( formData, jqForm, options ){

					document.getElementById( 'SearchForm' ).elements[ 'search' ].value = '1';
					document.getElementById( 'SearchForm' ).elements[ 'export' ].value = '0';

					$.blockUI({

						message: "Recherche en cours",
						css: {
							padding: '15px',
							cursor: 'pointer',
							'font-weight': 'bold',
							'font-family': 'Arial, Helvetica, sans-serif',
							'color': '#586065',
							'-webkit-border-radius': '10px',
							'-moz-border-radius': '10px'
						},
						fadeIn: 0,
						fadeOut: 700

					});

				}


				/* ----------------------------------------------------------------------------------- */

				function postSubmitCallback( responseText, statusText ){

					$.unblockUI();

					if( statusText != 'success' || responseText == '0' ){

						alert( "Impossible d'effectuer la recherche" );
						return;

					}

					$('#SearchResults').html( responseText );

				}

				/* --------------------------------------------------------------------------------------- */

				function orderBy( str ){

					document.getElementById( 'SearchForm' ).elements[ 'orderBy' ].value = str;

					$('#SearchForm').ajaxSubmit( {

						beforeSubmit:  preSubmitCallback,  // pre-submit callback
						success:	   postSubmitCallback  // post-submit callback

					} );

				}

				/* ----------------------------------------------------------------------------------- */

				function changeRelation( element ){ $('#idrelation').val( element.info ); }

				/* ----------------------------------------------------------------------------------- */

				function changeSupplier( element ){ $('#idsupplier').val( element.info ); }

				/* ----------------------------------------------------------------------------------- */

                /* ----------------------------------------------------------------------------------- */

				function changeProvider( element ){ $('#idprovider').val( element.info ); }

				/* ----------------------------------------------------------------------------------- */

				function changeDatepickerEdges( fiscal_year ){
                        $('#search_start').datepicker('option','minDate',minDate[ fiscal_year ]);
                        $('#search_end').datepicker('option','maxDate',maxDate[ fiscal_year ]);

                        $('#search_start').datepicker('setDate',minDate[ fiscal_year ]);
                        $('#search_end').datepicker('setDate',maxDate[ fiscal_year ]);
				}

				/* ----------------------------------------------------------------------------------- */

				var minDate = new Array();
				var maxDate = new Array();

<?php

					$rs =& DBUtil::query( "SELECT idfiscal_year, start_date FROM fiscal_years ORDER BY start_date ASC" );

					if( $rs === false )
						trigger_error( Dictionnary::translate("tlt_2"), E_USER_ERROR );

					while( !$rs->EOF ){

						$start = $rs->fields( "start_date" );
						$id = $rs->fields( "idfiscal_year" );

						$rs->MoveNext();

						if( $rs->EOF )
							$end = date( "Y-m-d" );
						else
							$end = DateUtil::getPreviousDay( $rs->fields( "start_date" ) );

						list( $year, $month, $day ) = explode( "-", $start );
						echo "minDate[ $id ] = new Date($year," . ( $month - 1 ) . ",$day);\n";

						list( $year, $month, $day ) = explode( "-", $end );
						echo "maxDate[ $id ] = new Date($year," . ( $month - 1 ) . "," . intval( $day ) . ");\n";

					}

?>

				/* ----------------------------------------------------------------------------------- */

			-->
			</script>
			<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
				<div class="headTitle">
					<p class="title"><?php echo Dictionnary::translate("tlt_1") ?></p>
					<div class="rightContainer">
						
						
							<?php

								$rs =& DBUtil::query( "SELECT idfiscal_year, name, start_date FROM fiscal_years ORDER BY start_date DESC" );

								if( $rs === false )
									trigger_error( Dictionnary::translate("tlt_2"), E_USER_ERROR );

								$start_date = Util::dateFormatEu( $rs->fields( "start_date" ), "d/m/Y" );
								$fiscal_year = $rs->fields( "idfiscal_year" );

								while( !$rs->EOF ){

									echo '<option value="' . $rs->fields( "idfiscal_year" ) . '">' . $rs->fields( "name" ) . '</option>';

									$rs->MoveNext();

								}

							?>
						
					</div>
				</div>
				<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />
					<input type="hidden" name="orderBy" value="<?php echo $postData[ "orderBy" ] ?>" />
					<input type="hidden" name="interval_select" id="interval_select" value="1" />
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th style="width:20%" class="filledCell"><?php echo Dictionnary::translate("tlt_13") ?></th>
								<td style="width:20%">
									<input type="text" name="search_start" id="search_start" class="calendarInput" value="<?php echo $start_date ?>" />
									<?php echo DHTMLCalendar::calendar( "search_start" ) ?>
								</td>
								<th style="width:20%" class="filledCell"><?php echo Dictionnary::translate("tlt_14") ?></th>
								<td style="width:40%">
									<?php $date = date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ); ?>
									<input type="text" name="search_end" id="search_end" class="calendarInput" value="<?php echo $date ?>" />
									<?php echo DHTMLCalendar::calendar( "search_end" ) ?>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
                            	
								<th class="filledCell"><?php echo Dictionnary::translate("tlt_15") ?></th>
								<td><input type="text" name="idaccount_record" class="textInput" value="<?php echo $postData[ "idaccount_record" ] ?>" /></td>
                                
                                <th class="filledCell"><?php echo Dictionnary::translate("tlt_16") ?></th>
								<td><input type="text" name="record_type" class="textInput" value="<?php echo $postData[ "record_type" ] ?>" /></td>
								
							</tr>
						</table>
					</div>

					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" value="Rechercher" />
					</div>
				</div>
			</form>
			<script type="text/javascript">
			<!--

				$(document).ready(function(){
			        $('#search_start').datepicker('option','minDate',minDate[ <?php echo $fiscal_year ?> ]);
				});
				
			-->
			</script>
<?php

}

//------------------------------------------------------------------------------------------------

function searchResults(){

	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	$maxSearchResults = 2500;

	$lang = User::getInstance()->getLang();

	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );

	
		$select = "
		idaccount_record,
		idrelation,
		record_type,
		SUM( ( amount_type = 'credit' ) * amount ) AS credit,
		SUM( ( amount_type = 'debit' ) * amount ) AS debit,
		SUM( ( amount_type = 'credit' ) * amount - ( amount_type = 'debit' ) * amount ) AS amount";

	

	$tables = "account_plan";

	$where = getSearchWhereCondition();

	//tri

	$orderBy = Util::html_escape( stripslashes( $_POST[ "orderBy" ] ) );

	//requete


		$query = "
			SELECT $select
			FROM $tables
			WHERE $where 
			GROUP BY idaccount_record, record_type
			ORDER BY idaccount_record ASC, record_type";

	$rs =& DBUtil::query( $query );

	$resultCount = $rs->RecordCount();

	?>
	<script type="text/javascript">
	<!--

		$(document).ready(function(){

			$(".record_help").qtip({
				adjust: {
					mouse: true,
					screen: true
				},
				content: 'AC : avoir client<br />FC : facture client<br />FF : facture fournisseur<br />RC : règlement client',
				position: {
					corner: {
						target: "rightMiddle",
						tooltip: "leftMiddle"
					}
				},
				style: {
					background: "#FFFFFF",
					border: {
						color: "#EB6A0A",
						radius: 5
					},
					fontFamily: "Arial,Helvetica,sans-serif",
					fontSize: "13px",
					tip: true
				}
			});

			$('.modifiableLetterCell').click(function(){

				var lettering = $(this).html();
				var idrow = $(this).attr('id').substring(7);

				if( !$(this).hasClass('activeCell') ){
					$(this).addClass('activeCell');
					$(this).html('<input type="text" value="' + lettering + '" id="field_' + idrow + '" class="textInput" style="text-align:center; text-transform:uppercase; width:30px;" onkeyup="setNewLettering(this.value,' + idrow + ');" onblur="$(this).parent().removeClass(\'activeCell\').html($(this).val());" />');
					$('#field_'+idrow).focus();
				}

			});

		});
function afficher(id){
alert(id);
 document.getElementById('pop'+id).className='tooltiphover';
 }
	-->
	</script>
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php

				if( $resultCount > 0 ){

					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );

					$query = "
						SELECT *
						FROM $tables
						WHERE $where
						ORDER BY idaccount_plan ASC, date ASC";

					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );

					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?export&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php } ?>
				<p class="title">
					&Eacute;critures comptables
					<?php if($resultCount <= $maxSearchResults ){ ?> : <span id="resultCount"></span><?php echo Dictionnary::translate("tlt_3"); } ?>
				</p>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ echo Dictionnary::translate("tlt_4"); ?>
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> écritures ont été trouvées <br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/accounting/account_plan.php?export&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable">
					

						<thead>
							<tr>
								<th class="filledCell" style="text-align:center; width:15%;">N° pièce</th>
                                <th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_18") ?></th>
								<th class="filledCell" style="text-align:center; width:40%;"><?php echo Dictionnary::translate("tlt_19") ?></th>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_20") ?></th>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_21") ?></th>
								<th class="filledCell" style="text-align:center; width:15%;"><?php echo Dictionnary::translate("tlt_22") ?></th>
							</tr>
						</thead>
					
						<tbody>
							<?php

							//construction du tableau de résultats
							//on fait ça pour pouvoir avoir les montants des parents en haut

							$results = array();
							$parent = '';
							$i = -1;
							$j = 0;


								while( !$rs->EOF() ){

									/*if( $parent != AccountPlan::getLastParent( $rs->fields( "idaccount_record" ) ) ){

										$parent = AccountPlan::getLastParent( $rs->fields( "idaccount_record" ) );*/

										$i++;

										$rsparent =& DBUtil::query( "SELECT record_type FROM account_plan WHERE idaccount_record = '$parent'" );

										if ( $rs->fields( "record_type" ) == "regulations_supplier" ) 
										{
										
										$results[ $i ] = array(
											"idaccount_record"	=> $rs->fields( "idaccount_record" ),
											"idrelation"		=> $rs->fields( "idrelation" ),
											"record_type"		=> 'Fournisseur',
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);
										
										}
										
										else if ( $rs->fields( "record_type" ) == "regulations_buyer" ) 
										{
										
										$results[ $i ] = array(
											"idaccount_record"	=> $rs->fields( "idaccount_record" ),
											"idrelation"		=> $rs->fields( "idrelation" ),
											"record_type"		=> 'Client',
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);
										
										}
										
										else if ( $rs->fields( "record_type" ) == "billing_buyer" ) 
										{
										
										$results[ $i ] = array(
											"idaccount_record"	=> $rs->fields( "idaccount_record" ),
											"idrelation"		=> $rs->fields( "idrelation" ),
											"record_type"		=> 'Client',
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);
										
										}
										
										else if ( $rs->fields( "record_type" ) == "billing_supplier" ) 
										{
										
										$results[ $i ] = array(
											"idaccount_record"	=> $rs->fields( "idaccount_record" ),
											"idrelation"		=> $rs->fields( "idrelation" ),
											"record_type"		=> 'Fournisseur',
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);
										
										}							
										
										else
										{
										$results[ $i ] = array(
											"idaccount_record"	=> $rs->fields( "idaccount_record" ),
											"idrelation"		=> $rs->fields( "idrelation" ),
											"record_type"		=> $rs->fields( "record_type" ),
											"debit"				=> 0,
											"credit"			=> 0,
											"amount"			=> 0,
											"childrens"			=> array()
										);
										
										}
										

										$j = 0;

									//}

									$results[ $i ][ "childrens" ][ $j ] = array(
										"idaccount_record"	=> $rs->fields( "idaccount_record" ),
										"idrelation"		=> $rs->fields( "idrelation" ),
										"record_type"		=> $rs->fields( "record_type" ),
										"debit"				=> $rs->fields( "debit" ),
										"credit"			=> $rs->fields( "credit" ),
										"amount"			=> $rs->fields( "amount" )
									);

									$results[ $i ][ "childrens" ][ $j ][ "idaccount_record" ]	= $rs->fields( "idaccount_record" );
									$results[ $i ][ "childrens" ][ $j ][ "idrelation" ]	= $rs->fields( "idrelation" );
									$results[ $i ][ "childrens" ][ $j ][ "record_type" ]	= $rs->fields( "record_type" );
									$results[ $i ][ "childrens" ][ $j ][ "debit" ]			= $rs->fields( "debit" );
									$results[ $i ][ "childrens" ][ $j ][ "credit" ]			= $rs->fields( "credit" );
									$results[ $i ][ "childrens" ][ $j ][ "amount" ]			= $rs->fields( "amount" );

									$results[ $i ][ "debit" ]	+= $rs->fields( "debit" );
									$results[ $i ][ "credit" ]	+= $rs->fields( "credit" );
									$results[ $i ][ "amount" ]	+= $rs->fields( "amount" );


									$j++;
									$rs->MoveNext();

								

							}

							foreach( $results as $result ){
								if ( $result[ "amount" ] != 0)
								{
								?>
							
							<tr id="account_<?php echo $result[ "idaccount_record" ] ?>" class="grasBack mainAccount" onmouseover="this.style.cursor='pointer';" onmouseout="this.style.cursor='default';" onclick="toggleSubAccounts('<?php echo $result[ "idaccount_record" ] ?>');">
								<th class="lefterCol"><?php echo $result[ "idaccount_record" ] ?></th>
                                <th class="lefterCol"><?php echo $result[ "idrelation" ] ?></th>
                                <th><?php echo stripslashes( $result[ "record_type" ] ) ?></th>
                                <th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "debit" ] ) ?></th>
								<th style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $result[ "credit" ] ) ?></th>
								<th class="righterCol" style="text-align:right; white-space:nowrap;"><?=Util::priceFormat( $result[ "amount" ] ) ?></th>
							</tr>
								<?php
								}
								foreach( $result[ "childrens" ] as $children ){
								if ( $children[ "amount" ] != 0)
								{
								
								?>
							<tr class="subAccount_<?php echo $result[ "idaccount_record" ] ?>" style="display:none;">
								<td class="lefterCol" style="padding-left:25px;"><?php echo $children[ "idaccount_record" ] ?></td>
                                <td class="lefterCol" style="padding-left:25px;"><?php echo $children[ "idrelation" ] ?></td>
								<td><?php echo stripslashes( $children[ "record_type" ] ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "debit" ] == 0 ? "-" : Util::priceFormat( $children[ "debit" ] ) ?></td>
								<td style="text-align:right; white-space:nowrap;"><?php echo $children[ "credit" ] == 0 ? "-" : Util::priceFormat( $children[ "credit" ] ) ?></td>
								<td class="righterCol" style="text-align:right; white-space:nowrap;"><?php echo $children[ "amount" ] == 0 ? "-" : Util::priceFormat( $children[ "amount" ] ) ?></td>
							</tr>
								<?php
								}
									

								}

							}

							?>
						</tbody>
					</table>
                  
				</div><?php } ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<script type="text/javascript">
	<!--

		$(function () {
			$('.bubbleInfo').each(function () {
				var distance = 10;
				var time = 250;
				var hideDelay = 500;

				var hideDelayTimer = null;

				var beingShown = false;
				var shown = false;
				var trigger = $('.trigger', this);
				var info = $('.popup', this).css('opacity', 0);

				$([trigger.get(0), info.get(0)]).mouseover(function () {
					if (hideDelayTimer) clearTimeout(hideDelayTimer);
					if (beingShown || shown) {
						// don't trigger the animation again
						return;
					} else {
						//hide other bubbles
						$('.bubbleInfo').each(function () {
							$('.popup', this).css('opacity', 0).css('display','none');
						});

						// reset position of info box
						beingShown = true;

						info.css({
							top: -27,
							right: -240,
							display: 'block'
						}).animate({
							right: '-=' + distance + 'px',
							opacity: 1
						}, time, 'swing', function() {
							beingShown = false;
							shown = true;
						});
					}

					return false;
				}).mouseout(function () {
					if (hideDelayTimer) clearTimeout(hideDelayTimer);
					hideDelayTimer = setTimeout(function () {
						hideDelayTimer = null;
						info.animate({
							right: '+=' + distance + 'px',
							opacity: 0
						}, time, 'swing', function () {
							shown = false;
							info.css('display', 'none');
						});

					}, hideDelay);

					return false;
				});
			});

		});

	-->
	</script>
	<style type="text/css">
	<!--

		.bubbleInfo{
			position:relative;
		}

		/* Bubble pop-up */

		.popup{
			background-image:url('<?php echo $GLOBAL_START_URL ?>/images/back_office/content/bubble.png');
			background-position:top left;
			background-repeat:no-repeat;
			border-collapse:collapse;
			display:none;
			font-size:12px;
			height:80px;
			line-height:18px;
			padding:5px 5px 5px 22px;
			position:absolute;
			text-align:left;
			width:226px;
			z-index:50;
		}

	-->
	</style>
<?php

}

//------------------------------------------------------------------------------------------------

function getSearchWhereCondition(){

	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;

	$maxSearchResults = 2500;

	$lang = User::getInstance()->getLang();

	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );

	$where = "
	amount !=0";

	//post data

	//strings

	$strings = array( "contact.lastname", "buyer.company", "account_plan.lettering" );

	foreach( $strings as $string ){

		list( $table, $column ) = explode( ".", $string );

		if( isset( $_POST[ $column ] ) && strlen( $_POST[ $column ] ) )
			$where .= "\n\tAND `$table`.`$column` LIKE '%" . Util::html_escape( $_POST[ $column ] ) . "%'";

	}

	//date

	switch( $_POST[ "interval_select" ] ){

		case 1:

			$start = Util::dateFormatus( $_POST[ "search_start" ] );
		 	$end = Util::dateFormatUs( $_POST[ "search_end" ] );

			$where .= "\n\tAND account_plan.date >= '$start'\n\tAND account_plan.date <= '$end'";

		 	break;

		//Affichage des lignes venant d'être générées
		case 2:

		 	$start = $_POST[ "start" ];
		 	$end = $_POST[ "end" ];

			$where .= "\n\tAND account_plan.creation_date >= '$start'\n\tAND account_plan.creation_date <= '$end'";

		 	break;

		//Affichage des lignes d'une pièce comptable
		case 3:

		 	$start = DBUtil::getDBValue( "start_date", "fiscal_years", "idfiscal_year", $_POST[ "fiscal_year" ] );

		 	$rs =& DBUtil::query( "SELECT start_date FROM fiscal_years WHERE start_date > '$start' ORDER BY start_date ASC LIMIT 1" );

		 	if( $rs === false )
		 		trigger_error( "Impossible de récupérer la date de fin de l'exercice comptable", E_USER_ERROR );

		 	$end = $rs->RecordCount() ? $rs->fields( "start_date" ) : date( "Y-m-d" );

			$where .= "\n\tAND account_plan.date >= '$start'\n\tAND account_plan.date < '$end'";

		 	break;

	}

	//Si recherche par numéro de reglement
	$accountrecordd = $_POST[ "idaccount_record" ];
	if( isset( $_POST[ "idaccount_record" ] ) && ( $_POST[ "idaccount_record" ] ) )
		$where .= "\n\tAND account_plan.idaccount_record = '$accountrecordd' " ;
		
		//Si recherche par numéro de reglement
	$recordtype = $_POST[ "record_type" ];
	if( isset( $_POST[ "record_type" ] ) && ( $_POST[ "record_type" ] ) )
		$where .= "\n\tAND account_plan.record_type = '$recordtype' " ;


	
	return $where;

}

//---------------------------------------------------------------------------------------------

function getModelTable( $idmodel ){

	global $GLOBAL_START_PATH;

	include_once( "$GLOBAL_START_PATH/objects/AccountModelParser.php" );

	?>
				<script type="text/javascript">
				<!--

					$(".model_amount_debit").keyup(function(){
						if( $(this).val() != '' )
							$(this).parent('td').parent('tr').children().children('.model_amount_credit').val('');
					});

					$(".model_amount_credit").keyup(function(){
						if( $(this).val() != '' )
							$(this).parent('td').parent('tr').children().children('.model_amount_debit').val('');
					});

				-->
				</script>
				<table class="dataTable resultTable">
					<thead>
						<tr>
							<th style="width:80px;">Numéro de compte</th>
							<th style="width:45px;">Numéro de pièce</th>
							<th style="width:95px;">Type de pièce</th>
							<th style="width:45px;">Numéro de tiers</th>
							<th style="width:90px;">Type de tiers</th>
							<th>Libellé</th>
							<th style="width:100px;">Date</th>
							<th>Date de paiement</th>
							<th style="width:125px;">Mode de paiement</th>
							<th style="width:35px;">Débit</th>
							<th style="width:35px;">Crédit</th>
						</tr>
					</thead>
					<tbody>
						<?php

							$query = "SELECT * FROM accounts_models_row WHERE idmodel = $idmodel ORDER BY idrow ASC";

							$rs =& DBUtil::query( $query );


							if( $rs === false )
								trigger_error( "Impossible de récupérer les modèles d'écritures", E_USER_ERROR );

							$model = new AccountModelParser( $idmodel, $rs->RecordCount() );

                            $count = -1;
							while( !$rs->EOF ){
                                $count++;
                                $payment_name = "";
                                if($rs->fields( 'payment' ) == "%manual%"){
                                  $payment_name = $model->parseInputPayment($rs->fields( 'payment' ));
                                }else if($rs->fields( 'payment' ) != ""){
                                    $payment_query = "SELECT name_1 FROM payment where idpayment = ".$rs->fields( 'payment' )." ORDER BY name_1 ASC";
                                    $payment_rs =& DBUtil::query( $payment_query );
                                    if( $payment_rs === false )
                                        trigger_error( "Impossible de récupérer les payment d'écritures", E_USER_ERROR );
                                    $payment_name = '<input type="hidden" name="payment_'. $count .'" value="'.$payment_rs->fields("name_1").'">'.$payment_rs->fields("name_1");
                                }
						?>
						<tr>
							<td class="lefterCol"><?php echo $model->parseInputAccount( $rs->fields( "idaccount_plan" ) ); ?></td>
							<td><?php echo $model->parseInputRecord( $rs->fields( "idrecord" ) ); ?></td>
							<td><?php echo $model->parseInputRecordType( $rs->fields( "record_type" ) ); ?></td>
							<td><?php echo $model->parseInputThirdParty( $rs->fields( "idrelation" ) ); ?></td>
							<td><?php echo $model->parseInputThirdPartyType( $rs->fields( "relation_type" ) ); ?></td>
							<td><?php echo $model->parseInputLabel( $rs->fields( "label" ) ); ?></td>
							<td id="td_date_<?php echo $count ?>"><input style="width: 70px;" id="date_<?php echo $count ?>" name="date_<?php echo $count ?>" type="text" value="<?php echo $model->parseInputDate($rs->fields( 'date' )) ?>"></td>
							<td><?php echo $model->parseInputPaymentDate( $rs->fields( "payment_date" ) ); ?></td>
							<!--<td>
                                <select name="payment_<?/*= $count */?>">
                                <?php
/*                                  $payment_rs->MoveFirst();
                                  while( !$payment_rs->EOF ){
                                      */?>
                                            <option value="<?/*= $payment_rs->fields('name_1') */?>"><?/*= $payment_rs->fields('name_1') */?></option>
                                      <?/*
                                      $payment_rs->MoveNext();
                                  }
                                */?>
                                </select>
                            </td>-->
							<td><?php echo $payment_name ?> </td>
							<td><?php echo $model->parseInputAmount( $rs->fields( "amount" ), $rs->fields( "amount_type" ), "debit" ); ?></td>
							<td class="righterCol"><?php echo $model->parseInputAmount( $rs->fields( "amount" ), $rs->fields( "amount_type" ), "credit" ); ?></td>
						</tr>
						<?php

                               if($rs->fields( "date" ) == "%today%"){
                                    DHTMLCalendar::calendar("date_$count");
                               }else{
                                    echo "<script type='application/javascript'>jQuery('#td_date_".$count . "').html('".$model->parseInputDate($rs->fields( 'date' ))."');</script>";
                               }
								$rs->MoveNext();
								$model->moveNext();

							}

						?>
					</tbody>
				</table>
<?php

}

//--------------------------------------------------------------------------------

function getCreationRow( $row = 1 ){

	global $GLOBAL_START_PATH, $GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

	$lang = User::getInstance()->getLang();

?>
						<tr>
							<td style="background-color:#FFFFFF; border-style:none;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/flexigrid_button_create.png" alt="+" onclick="getNewCreationRow();" style="cursor:pointer;" /></td>
							<td class="lefterCol" style="text-align:center;">
								<select name="account_plan_1_<?php echo $row ?>" id="account_plan_1_<?php echo $row ?>" style="width:50px;">
									<?php

										$rs =& DBUtil::query( "SELECT diary_code FROM diary_code ORDER BY diary_code ASC" );

										if( $rs == false )
											trigger_error( "Impossible de réupérer la liste des codes comptables", E_USER_ERROR );

										while( !$rs->EOF ){

											echo '<option value="' . $rs->fields( "diary_code" ) . "\">" . $rs->fields( "diary_code" ) . '</option>';

											$rs->MoveNext();

										}

									?>
								</select>
							</td>
							<td style="text-align:center;">
                                <?php accountsSelect( false , "idaccount_plan_$row"); ?>
							
							</td>
							<td style="text-align:center;">
								<div id="relationPopupCompletor">
									<input type="text" name="relation_name_<?php echo $row ?>" onchange="onChange(event)" id="relation_name_<?php echo $row ?>" value="" class="textInput relation_name" />
									<input type="hidden" name="idrelation_<?php echo $row ?>" id="idrelation_<?php echo $row ?>" value="" class="idrelation" />
									<?php AutoCompletor::completeFromFile( "relation_name_$row", "$GLOBAL_START_URL/xml/autocomplete/customer_supplier.php", 5, "changeRelation", true ); ?>
								</div>
							</td>
							<td style="text-align:center;"><input type="text" name="idaccount_record_<?php echo $row ?>" id="idaccount_record_<?php echo $row ?>" value="" class="textInput" /></td>
							<td style="text-align:center;">
								<input type="text" name="value_date_<?php echo $row ?>" id="value_date_<?php echo $row ?>" value="" class="textInput" />
								<?php DHTMLCalendar::calendar( "value_date_$row", false ); ?>
							</td>
							<td style="text-align:center;"><input type="text" name="designation_<?php echo $row ?>" id="designation_<?php echo $row ?>" value="" class="textInput" /></td>
							<td style="text-align:center;">
								<select name="payment_<?php echo $row ?>" id="payment_<?php echo $row ?>">
									<option value="">-</option>
									<?php

										$rs =& DBUtil::query( "SELECT idpayment, name$lang AS name FROM payment ORDER BY name$lang ASC" );

										if( $rs == false )
											trigger_error( "Impossible de réupérer la liste des modes de paiement", E_USER_ERROR );

										while( !$rs->EOF ){

											echo '<option value="' . $rs->fields( "name" ) . "\">" . $rs->fields( "name" ) . '</option>';

											$rs->MoveNext();

										}

									?>
								</select>
							</td>
							<td style="text-align:center;">
								<input type="text" name="payment_date_<?php echo $row ?>" id="payment_date_<?php echo $row ?>" value="" class="textInput" />
								<?php DHTMLCalendar::calendar( "payment_date_$row", false ); ?>
							</td>
							<td style="white-space:nowrap;"><input type="text" name="amount_<?php echo $row ?>" id="amount_<?php echo $row ?>" value="" class="textInput" style="text-align:right; width:50px;" /> ¤</td>
							<td style="text-align:center;">
								<select name="amount_type_<?php echo $row ?>" id="amount_type_<?php echo $row ?>">
									<option value="credit">Crédit</option>
									<option value="debit">Débit</option>
								</select>
							</td>
							<td id="letteringCell_<?php echo $row ?>" class="righterCol" style="text-align:center;"><input type="text" name="lettering_<?php echo $row ?>" id="lettering_<?php echo $row ?>" value="" class="textInput" style="width:30px;" /></td>
						</tr>
<?php

}

//--------------------------------------------------------------------------------



function getRelationName( $relation_type, $idrelation ){

	switch( $relation_type ){
		case "buyer":
			$name = AccountPlan::getCustomerName( $idrelation );
			break;
		case "supplier":
			$name = DBUtil::getDBValue( "name", "supplier", "idsupplier", $idrelation );
			break;
		case "provider":
			$name = DBUtil::getDBValue( "name", "provider", "idprovider", $idrelation );
			break;	
		case "others":
			$name = $idrelation;
			break;
		default:
			$name = $idrelation;
			break;
	}

	return $name;

}
//--------------------------------------------------------------------------------
?>