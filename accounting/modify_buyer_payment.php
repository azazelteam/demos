<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Modification règlements clients
 */

include_once( "../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

$banner = "no";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );
	
if( !isset( $_GET[ "idrb" ] ) || $_GET[ "idrb" ] < 0 ){
	
	
	echo "<p style=\"color: #FF0000; font-weight: bold; text-align: center;\">" . Dictionnary::translate( "missing_parameter" ) . "</p>";

	include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );
	
	exit();
			
}

$idrb = $_GET[ "idrb" ];

//Facture(s) concernée(s)

$rs =& DBUtil::query("SELECT brb.idbilling_buyer, bb.idbuyer FROM billing_regulations_buyer brb, billing_buyer bb WHERE idregulations_buyer = $idrb AND brb.idbilling_buyer = bb.idbilling_buyer");

if( $rs === false )
	die( "Impossible de récupérer les factures liées au règlement" );

if( $rs->RecordCount() > 0 ){
	
	$idbuyer = $rs->fields( "idbuyer" );
	
	if( $rs->RecordCount() == 1 ){
		
		$title = "Facture concernée : " . $rs->fields( "idbilling_buyer" );
		$bil = array( $rs->fields( "idbilling_buyer" ) );
		
	}else{
		
		$title = "Factures concernées : ";
		
		$bil = array();
		
		for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
			
			if($i)
				$title.="-";
			
			$title.=$rs->fields("idbilling_buyer");
			
			$bil[] = $rs->fields("idbilling_buyer");
		}
	}
		
}else{
	
	die ("Aucune facture correspondante");	
	
}

 //------------------------------------------------------------------------------------------------
//Traitement du formulaire

$error = 0;
$Msgerror = '';

if ( isset($_POST[ "Modify" ]) || isset($_POST[ "Modify_x" ]) ){
	
	$fields = array();
	
	$fields[] = "deliv_payment" ;
	$fields[] = "amount";	
	
	//Test des champs postés
	
	for ($i=0;$i<count($fields);$i++){
		if( empty( $_POST[ $fields[$i] ] ) ){
			$error = 1;
		}
	}
	
	
	if ( $error == 1 ){
		$Msgerror = "Tous les champs doivent être remplis";	
	}else{
		//Tous les champs sont remplis, test des champs numériques
		if( !is_numeric( $_POST[ "amount" ] ) ) {
			$error = 1;
			$Msgerror = "Le champ montant TTC doit être un nombre";		
		}
	}
	
	if( $error == 0 ){
		
		$paid = 1;
		
		$idrb = $_GET[ "idrb" ];
		$idbuyer = $_POST[ "idbuyer" ];
		$deliv_payment =  $_POST[ "deliv_payment" ];
		$payment_date = $_POST[ "payment_date" ];
		$bank_date = $_POST[ "bank_date" ];
		$comments = $_POST[ "comment" ];
		
		$amount = $_POST[ "amount" ];
		
		$idpayment = $_POST[ "idpayment" ];
		
		//accept
		$auj = date("d-m-Y");
		
		if(	$auj >= $payment_date && $payment_date!="" && $payment_date!="0000-00-00") {
			$accept = 1;
		}else{
			$accept = 0;
			$paid = 0;
		}
		
		include_once( "$GLOBAL_START_PATH/objects/RegulationsBuyer.php" );
		
		//On affecte les valeurs
		$regulationsbuyer = new RegulationsBuyer($idrb);
		$regulationsbuyer->set( "payment_date" , euDate2us($payment_date) );
		$regulationsbuyer->set( "deliv_payment" , euDate2us($deliv_payment) );
		$regulationsbuyer->set( "bank_date" , euDate2us($bank_date) );
		$regulationsbuyer->set( "idpayment" , $idpayment );
		$regulationsbuyer->set( "amount" , $amount );
		$regulationsbuyer->set( "accept" , $accept );
		$regulationsbuyer->set( "comment" , $comments );
		
		//On sauvegarde
		$regulationsbuyer->update();
		
		if( $paid )
			UpdateBillingsStatus( $bil );
	?>
		<script type="text/javascript">
			<!--
			alert ("Paiement modifié avec succès !");
			-->
		</script>
	<?
	}   
	
}  

?>
<script type="text/javascript">
<!--

function checkForm(){
	
	if( document.getElementById( "amount" ).value == "" ){
		
		alert( "Vous devez saisir le montant du paiement." );
		
		return false;
		
	}else if( isNaN( document.getElementById( "amount" ).value ) == true ){
		
		alert( "Le montant du paiement doit être un nombre." );
		
		return false;
		
	}else if( document.getElementById( "deliv_payment" ).value == "" ){
		
		alert( "Vous devez saisir une date d'échéance." );
		
		return false;
		
	}
	
	return true;
	
}
// -->
</script>
<style type="text/css">
<!--
div#global{
	min-width:0;
	width:auto;
}
-->
</style>
<div id="globalMainContent" style="width:380px;">
<span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
	<div class="mainContent fullWidthContent" style="width:380px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Modification du paiement de <?php echo getBuyerName( $idbuyer ) ?></p>
			</div>
			<div class="subContent">
				<div><?php echo $title ?></div>
				<?php if( $error == 1 ){ ?><br /><span class="msg"><?php echo $Msgerror ?></span><?php } ?>
				<?php displayPaiementForm( $idrb ); ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//---------------------------------------------------------------------------------------------------

function getBuyerName( $idbuyer ){
	
	$query = "SELECT company FROM buyer WHERE idbuyer = $idbuyer LIMIT 1";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nom du client" );
	
	return $rs->fields( "company" );
	
}

//------------------------------------------------------------------------------------------------------

function UpdateBillingsStatus( $bil ){
	
	$db = &DBUtil::getConnection();
	
	for($i=0;$i<count($bil);$i++){
		
		$billing_buyer = $bil[$i];
		
		$query = "UPDATE billing_buyer SET status='Paid' WHERE idbilling_buyer = $billing_buyer";
		$rs = $db->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de mettre à jour le statut des factures clients" );
		
	}
	
}

//------------------------------------------------------------------------------------------------------

function displayPaiementForm( $idrb ){
	
	global	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/RegulationsBuyer.php" );
	
	$today = date( "Y-m-d" );
	
	$RB = new RegulationsBuyer( $idrb );
	
	$deliv_payment = usDate2eu( $RB->get( "deliv_payment" ) );
	$payment_date = usDate2eu( $RB->get( "payment_date" ) );
	$bank_date = usDate2eu( $RB->get( "bank_date" ) );
	
?>
				<form name="frm" method="POST" action="<?php echo $GLOBAL_START_URL ?>/accounting/modify_buyer_payment.php?idrb=<?php echo $idrb ?>" onsubmit="return checkForm();">
					<input type="hidden" name="idbuyer" value="<?php echo $RB->get( "idbuyer" ) ?>" />
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th>Date d'échéance <span class="asterix">*</span></th>
								<th>
									<input type="text" name="deliv_payment" id="deliv_payment" value="<?php echo $deliv_payment == "00-00-0000" ? "" : $deliv_payment ?>" class="calendarInput" />
									<?php echo DHTMLCalendar::calendar( "deliv_payment" ) ?>
								</th>
							</tr>
							<tr>
								<th>Date de paiement</th>
								<td>
									<input type="text" name="payment_date" id="payment_date" value="<?php echo $payment_date == "00-00-0000" ? "" : $payment_date ?>" class="calendarInput" />
									<?php echo DHTMLCalendar::calendar( "payment_date" ) ?>
								</td>
							</tr>
							<tr>
								<th>Date de rapprochement</th>
								<td>
									<input type="text" name="bank_date" id="bank_date" value="<?php echo $bank_date == "00-00-0000" ? "" : $bank_date ?>" class="calendarInput" />
									<?php echo DHTMLCalendar::calendar( "bank_date" ) ?>
								</td>
							</tr>
							<tr>
								<th>Mode de paiement <span class="asterix">*</span></th>
								<td><?php echo paymentList( $RB->get( "idpayment" ) ) ?></td>
							</tr>
							<tr>
								<th>Montant TTC <span class="asterix">*</span></th>
								<td><input type="text" name="amount" id="amount" value="<?php echo $RB->get( "amount" ) ?>" class="textInput" /></td>
							</tr>
							<tr>
								<th>Commentaire</th>
								<td><input type="text" name="comment" value="<?php echo $RB->get( "comment" ) ?>" class="textInput" /></td>
							</tr>
						</table>
					</div>
					<div style="margin-top:-10px;">Les champs marqués par <span class="asterix">*</span> sont obligatoires</div>
					<div class="submitButtonContainer">
						<input type="submit" name="Modify" value="Modifier" class="blueButton" />
					</div>
				</form>
<?php
	
}

//-----------------------------------------------------------------------------------------

function paymentList( $selected_value ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/RegulationsBuyer.php" );
	
	$lang = "_1";

	echo "
	<select name=\"idpayment\">\n";
	
	$rs =& DBUtil::query("SELECT idpayment, name_1 FROM payment ORDER BY name_1 ASC");
	
	if($rs->RecordCount()>0){
		
		include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			$selected = '';
			
			if( $selected_value=='' ){
				if( $rs->Fields( "idpayment" ) == Payment::$PAYMENT_MOD_TRANSFER ){
					$selected = " selected=\"selected\"";
				}
			}else{
				if( $rs->Fields( "idpayment" ) == $selected_value )
					$selected = " selected=\"selected\"";
				else $selected = "";
			}
			
			echo "
			<option value=\"" . $rs->Fields( "idpayment" ) . "\"$selected>" . $rs->Fields( "name_1" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}

//-----------------------------------------------------------------------------------------------

function getInvoiceDelivPayment( $idbilling_buyer ){
	
	$query = "SELECT deliv_payment FROM billing_buyer WHERE idbilling_buyer = $idbilling_buyer";
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la date d'échéance de la facture" );
		
	return $rs->fields( "deliv_payment" );
	
}

?>