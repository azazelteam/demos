<?php

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

//------------------------------------------------------------------------------------------

$Title = "Listes des reglements";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------------

?>

<div id="globalMainContent">
<?php

displaySearchResults();
?>
</div>

<?php


include_once ("$GLOBAL_START_PATH/templates/back_office/foot.php");

//------------------------------------------------------------------------------------------------

function displaySearchResults() {

	global	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
	

	
	?>
		
		<div class="mainContent fullWidthContent">
    	<div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Liste des r�glements non affect�s � des factures</p>
            </div>
            <?php summary(); ?>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    	</div> <!-- mainContent fullWidthContent -->

		<?php
		
}

//---------------------------------------------------------------------------------------------------------

function summary(){
	global	$GLOBAL_START_URL,
			$GLOBAL_START_PATH;
			
	$query = "
				SELECT *
				FROM `regulations_buyer`
				WHERE `idregulations_buyer` NOT IN (

														SELECT `idregulations_buyer`
														FROM `billing_regulations_buyer`)
			 ";

	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		return;
		
	?>
	<div class="subContent">
	<div class="resultTableContainer clear" style="padding-top:5px;">
		<table class="dataTable resultTable tablesorter">
	    	<thead>
	    		<tr>
	                <th style="vertical-align:top; border-right:1px solid #CBCBCB;">R�glement n�:</th>
	                <th style="vertical-align:top;">Client n�:</th>
	                <th style="vertical-align:top; border-right:1px solid #CBCBCB;">Montant</th>
	                
				</tr>
			</thead>
			<tbody>
			<?php
			
				
						
				while( !$rs->EOF() ){
					
					
					$idregulations_buyer = $rs->fields( "idregulations_buyer" );
					$idbuyer	 		 = $rs->fields( "idbuyer" );
					$amount			 	 = $rs->fields( "amount" );
					
					
					
					
						?>
						<tr>
			                <td style="text-align:center; border-right:1px solid #CBCBCB;" class="lefterCol">
			                <?php 
			                
			                	echo $idregulations_buyer;
			                	
			                ?>
			                </td>
			                <td style="text-align:center;">
                            <a href="<?= $GLOBAL_START_URL ?>/sales_force/contact_buyer.php?key=<?= $idbuyer ?>" onclick="window.open(this.href); return false;">
                            <?php echo $idbuyer; ?></a></td>
			                <td style="text-align:center; border-right:1px solid #CBCBCB;"><?php echo $amount; ?></td>
			                
						</tr>
						<?php
						$rs->MoveNext();
						
						
					}

				
			?>
			</tbody>
		</table>
	</div>
	</div>
	<?php
	            
}

//---------------------------------------------------------------------------------------------------------

?>