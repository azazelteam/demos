<?php
/**
 * Package comptabilité
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Demande avoirs fournisseurs
 */
//------------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );

$hasAdminPrivileges = User::getInstance()->get( "admin" );
$iduser = User::getInstance()->getId();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');

$resultPerPage = 50000;

//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	search();
	
	exit();
		
}

//------------------------------------------------------------------------------------------
/* mise à jour ajax */

if( isset( $_POST[ "demand" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_POST[ "billing_supplier" ] ) )
		exit( "msg1" );
	
	if( !isset( $_POST[ "comment" ] ) )
		exit( "msg5" );
	
	if( !isset( $_POST[ "date" ] ) )
		exit( "msg2" );
	
	if( !preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})/", $_POST[ "date" ], $registers ) )
		exit( "msg3" );
	
	$date = $registers[ 3 ] . "-" . $registers[ 2 ] . "-" . $registers[ 1 ];
	
	$rs =& DBUtil::query( "UPDATE billing_supplier SET credit_request_date = '$date', credit_request_object='" . Util::html_escape($_POST["comment"])."' WHERE billing_supplier = '" . $_POST[ "billing_supplier" ] . "'" );
	if( $rs === false )
		exit( "msg4" );
	
	exit("0");
		
}


$Title = Dictionnary::translate("search_supplier_invoice_title");


//------------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/SupplierInvoice.php" );

$con = DBUtil::getConnection();

$use_help = DBUtil::getParameterAdmin('gest_com_use_help');


$resultPerPage = 50000;
$maxSearchResults = 1000;

if( isset( $_GET[ "export" ] ) ){
	
	$exportableArray =& getExportableArray();
	exportArray( $exportableArray );
	
	exit();
	
}

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?><div id="globalMainContent"><?php

displayForm();


?></div><!-- globalMainContent --><?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function displayForm(){
	
	global $GLOBAL_START_URL;
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "Date";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier				= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
	$total_amount			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
	
	?>
	<script type="text/javascript" language="javascript">
	<!--
	
		function SortResult( sby, ord ){
		
			document.searchForm.action = document.searchForm.action + '?search=1&sortby=' + sby + '&sens=' + ord;
			document.searchForm.submit();
		
		}
		
		function goToPage( pageNumber ){
		
			document.forms.searchForm.elements[ 'page' ].value = pageNumber;
			document.forms.searchForm.submit();
			
		}
		
		function selectSince(){
			
			document.getElementById( "interval_select_since" ).checked=true;
			
		}
		
		function selectMonth(){
			
			document.getElementById( "interval_select_month" ).checked=true;
			
		}
		
		function selectBetween( dateField ){
			
			document.getElementById( "interval_select_between" ).checked=true;
			
			return true;
			
		}
		
	// -->
	</script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
	<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
	<script type="text/javascript">
	/* <![CDATA[ */
	
	/* ----------------------------------------------------------------------------------- */
		
		function goForm(){
		
			var options = {
				beforeSubmit:	preSubmitCallback,  // pre-submit callback 
				success:		postSubmitCallback  // post-submit callback
  			};
	 
    		// bind to the form's submit event 
    		$('#SearchForm').ajaxSubmit(options); 

		}	
		
		/* ----------------------------------------------------------------------------------- */
		
		$(document).ready(function() { 
			
			 var options = {
			 	
				beforeSubmit:  preSubmitCallback,  // pre-submit callback 
				success:	   postSubmitCallback  // post-submit callback 
  
			};
			
			$('#SearchForm').ajaxForm( options );
			
		});
		
		/* ----------------------------------------------------------------------------------- */
		
		function creditRequest( calInput ){
			
			document.getElementById( "requestObjectCal" ).value = calInput.id;
			
			$.blockUI({
				
				message: $("#requestObject"),
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			});
			
		}
		
		function MajaxMe( calInput ){
			
			$.blockUI({
	
				message: "Mise à jour des données",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			});
			
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
			var myincrement = calInput.substring( 5 );
			var billing_supplier = document.getElementById('billing_supplier_'+myincrement).value
			
			$.ajax({
				type: "POST",
				url: "<?php echo $GLOBAL_START_URL ?>/accounting/supplier_credits_requests.php",
				data: "demand=1&billing_supplier=" + billing_supplier + "&date=" + document.getElementById(calInput).value + "&comment=" + escape( document.getElementById('requestObjectComment').value ),
				error: function(){ $.growlUI( '', 'Un problème est survenu lors de la mise à jour des données' ); },
				success: function( responseText ){
					
					if( responseText != "0" ){
						
						$.unblockUI();
						alert( responseText );
						return;
						
					}
					
					$.growlUI( '', 'Les données ont bien été mises à jour' );
					goForm();
				}
			});
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function preSubmitCallback( formData, jqForm, options ){
			
			
			$.blockUI({
	
				message: "Recherche en cours",
				css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
				fadeIn: 0, 
				fadeOut: 700
					
			}); 
			
			$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
			
		}
		
		/* ----------------------------------------------------------------------------------- */
		
		function postSubmitCallback( responseText, statusText ){ 
	
			$.unblockUI();
			
			if( statusText != 'success' || responseText == '0' ){
				
				alert( "Impossible d'effectuer la recherche" );
				return;
				
			}

			$('#SearchResults').html( responseText );
			
		}
		
	/* ]]> */
	</script>
	<script type="text/javascript" language="javascript" src="<?=$GLOBAL_START_URL?>/js/ajax.lib.js"></script>
		<div id="requestObject" style="display:none;">
        <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
			<form id="requestObjectForm" action="" method="post">
				<p class="title">Objet de la demande d'avoir</p>
				<input type="hidden" id="requestObjectCal" />
				<textarea id="requestObjectComment" style="width:350px;height=400px;"></textarea>
				<div class="submitButtonContainer">
					<input type="button" class="blueButton" onclick="MajaxMe(this.form.elements['requestObjectCal'].value);" value="Valider" />
				</div>
			</form>
		</div>
    	<div class="mainContent">
            <div class="topRight"></div><div class="topLeft"></div>
            <div class="content">
            <form action="<?php echo $GLOBAL_START_URL ?>/accounting/supplier_credits_requests.php" method="post" name="searchForm" id="SearchForm">
				<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
	  			<input type="hidden" name="search" value="1" />
            	<div class="headTitle">
                    <p class="title">Rechercher une facture fournisseur</p>
                    <div class="rightContainer">
                    	<input type="radio" name="date_type" value="Date"<?php if( $date_type == 'Date') echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Date de facture
                    	<input type="radio" name="date_type" value="creation_date"<?php if( $date_type == 'creation_date') echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Date d'enregistrement
                        <input type="radio" name="date_type" value="deliv_payment"<?php if( $date_type == 'deliv_payment') echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Date d'échéance
                    </div>
                </div>
                <div class="subContent">
                		<!-- tableau choix de date -->
                        <div class="tableContainer">
                            <table class="dataTable dateChoiceTable">
                                <tr>	
                                    <td><input type="radio" name="interval_select" id="interval_select_since" value="1"<?php if( !isset( $_POST[ "interval_select" ] ) || $_POST[ "interval_select" ] == 1) echo " checked=\"checked\""; ?> class="VCenteredWithText"/>Depuis</td>
                                    <td>
                                        <select name="minus_date_select" onchange="selectSince();" class="fullWidth">
							            	<option value="0"<?php if($minus_date_select==0 ) echo " selected=\"selected\""; ?>>Aujourd'hui</option>
							              	<option value="-1"<?php if($minus_date_select==-1 ) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_yesterday")  ?></option>
						              		<option value="-2"<?php if($minus_date_select==-2) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2days")  ?></option>
							              	<option value="-7"<?php if($minus_date_select==-7) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1week") ?></option>
							              	<option value="-14"<?php if($minus_date_select==-14) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2weeks") ?></option>
							              	<option value="-30"<?php if($minus_date_select==-30) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1month") ?></option>
							              	<option value="-60"<?php if($minus_date_select==-60) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_2months") ?></option>
							              	<option value="-90"<?php if($minus_date_select==-90) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_3months") ?></option>
							              	<option value="-180"<?php if($minus_date_select==-180) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_6months") ?></option>
							              	<option value="-365"<?php if($minus_date_select==-365) echo " selected=\"selected\""; ?>><?php echo Dictionnary::translate("order_1year") ?></option>
							              	<option value="-730"<?php if($minus_date_select==-730) echo " selected=\"selected\""; ?>>Moins de 2 ans</option>
							              	<option value="-1095"<?php if($minus_date_select==-1095) echo " selected=\"selected\""; ?>>Moins de 3 ans</option>
							              	<option value="all"<?php if($minus_date_select=="all") echo " selected=\"selected\""; ?>>Toutes</option>
							            </select>
                                    </td>
                                    <td><input type="radio" name="interval_select" id="interval_select_month" value="2"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "2" ) echo " checked=\"checked\""; ?> class="VCenteredWithText" />Pour le mois de</td>
                                    <td>
                                        <?php FStatisticsGenerateDateHTMLSelect( "selectMonth();" ); ?>
                                    </td>
                                </tr>
                                <tr>	
                                    <td>
                                        <input type="radio" name="interval_select" id="interval_select_between" value="3"<?php if( isset( $_POST[ "interval_select" ] ) && $_POST[ "interval_select" ] == "3" ) echo " checked=\"checked\""; ?> class="VCenteredWithText" />Entre le
                                    </td>
                                    <td>
										<?php $lastYearDate = date( "d-m-Y", mktime( 0, 0, 0, date( "m" ), date( "d" ), date( "Y" ) - 1 ) ); /*Back in time one year*/ ?>
						 				<input type="text" name="bt_start" id="bt_start" value="<?php echo isset( $_POST[ "bt_start" ] ) ? $_POST[ "bt_start" ] : $lastYearDate ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_start", true, "selectBetween" ) ?>
                                    </td>
                                    <td>et le</td>
                                    <td>
						 				<input type="text" name="bt_stop" id="bt_stop" value="<?php echo isset( $_POST[ "bt_stop" ] ) ? $_POST[ "bt_stop" ] : date( "d-m-Y" ) ?>" class="calendarInput" />
						 				<?php echo DHTMLCalendar::calendar( "bt_stop", true, "selectBetween" ) ?>
                                    </td>
                                </tr>
                                <tr>
                                	<td colspan="4" class="tableSeparator"></td>
                                </tr>
                                <tr class="filledRow">
                                    <th>Facture n°</th>
                                    <td><input type="text" size="10" name="billing_supplier"  value="<?php echo $billing_supplier ?>" class="textInput" /></td>
                                    <th>Fournisseur</th>
                                    <td><?php DrawSupplierList( $idsupplier ); ?></td>
                                </tr>
                                 <tr class="filledRow">
                                    <th>Montant TTC</th>
                                    <td><input type="text" size="10" name="total_amount"  value="<?php echo $total_amount ?>" class="textInput" /></td>
                                    <th>&nbsp;</th>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                        <div class="submitButtonContainer">
                        	<input type="submit" class="blueButton" style="text-align: center;" value="Rechercher" />
                        </div>
                </div>
                </form>
            </div>
            <div class="bottomRight"></div><div class="bottomLeft"></div>
        </div>
        <div id="tools" class="rightTools">
        	<div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Outils</p>
                </div>
                <div class="content"></div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
            <div class="toolBox">
            	<div class="header">
                	<div class="topRight"></div><div class="topLeft"></div>
                    <p class="title">Infos Plus</p>
                </div>
                <div class="content">
                </div>
                <div class="footer">
                	<div class="bottomLeft"></div>
                    <div class="bottomMiddle"></div>
                    <div class="bottomRight"></div>
                </div>
            </div>
        </div>
	<div id="SearchResults"></div>
<?php 

}

/////-----------------------

function search(){

	global $resultPerPage, $GLOBAL_START_URL, $GLOBAL_START_PATH, $ScriptName,$hasAdminPrivileges,$iduser;

	$now = getdate();
	
	$date_type 				= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
	$total_amount			= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
		
	switch ($interval_select){
			    
		case 1:
			//min
			if( $minus_date_select == "all" )
				$Min_Time = "0000-00-00";
			else
				$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_POST['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_POST[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_POST[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !$hasAdminPrivileges )
		$SQL_Condition .= " AND `billing_supplier`.iduser = '$iduser'";
						
	if ( !empty($idsupplier) ){
			$idsupplier = FProcessString($idsupplier);
			$SQL_Condition .= " AND billing_supplier.idsupplier =$idsupplier";
	}
	
	if ( !empty($billing_supplier) ){
			$SQL_Condition .= " AND billing_supplier.billing_supplier ='$billing_supplier'";
	}
	
	if ( !empty($total_amount) ){
			$SQL_Condition .= " AND billing_supplier.total_amount = $total_amount";
	}
	
	$con = DBUtil::getConnection();

	$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
	
?>
<script type="text/javascript" language="javascript">
<!--
	
	function registerCredit(billing_supplier,idsupplier){
		
		var credithref = 'register_supplier_credit.php?ids='+idsupplier+'&bs='+billing_supplier;
		
		postop = (self.screen.height-550)/2;
		posleft = (self.screen.width-800)/2;
		
		window.open(credithref, '_blank', 'top=' + postop + ',left=' + posleft + ',width=800,height=550,resizable,scrollbars=yes,status=0');
					
	}
// -->
</script>

<form action="<?php echo $ScriptName ?>" method="post" name="invoicetopay" id="invoicetopay">
<input type="hidden" name="page" value="<?php echo isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 ?>" />
<input type="hidden" name="OrderToDelete" id="OrderToDelete" value="0" />
<input type="hidden" name="search" value="1" />
<?php

	$SQL_Condition	.= " AND billing_supplier.iduser = user.iduser
	AND billing_supplier.idsupplier = supplier.idsupplier";
		
	$tables = "billing_supplier, user, supplier";
	
	//nombre de pages

	$SQL_Query = "
	SELECT COUNT(*) AS resultCount 
	FROM $tables 
	WHERE $SQL_Condition";	

	$rs =& DBUtil::query($SQL_Query);
	
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );

		$pageCount = ceil( $resultCount / $resultPerPage );
		
		//recherche
		
		$start = ( $page - 1 ) * $resultPerPage;

		$SQL_Query = "
		SELECT billing_supplier.billing_supplier, 
			billing_supplier.idsupplier,
			billing_supplier.status,
			supplier.name,
			billing_supplier.Date,
			billing_supplier.creation_date, 
			billing_supplier.deliv_payment, 
			user.initial,
			billing_supplier.total_amount,
			billing_supplier.credit_request_date,
			billing_supplier.credit_request_object,
			GROUP_CONCAT( credit_supplier.Date ) as credit_date,
			GROUP_CONCAT( credit_supplier.idcredit_supplier ) as idcredit_supplier,
			GROUP_CONCAT( credit_supplier.amount ) as amount
		FROM user, supplier, billing_supplier 
		LEFT JOIN credit_supplier ON billing_supplier.billing_supplier = credit_supplier.billing_supplier
		WHERE $SQL_Condition
		GROUP BY billing_supplier.billing_supplier
		";
		
		if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
			$SQL_Query .= "
			ORDER BY " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ;
		else
			$SQL_Query .= "ORDER by billing_supplier.billing_supplier ASC";
		
		$rs =& DBUtil::query($SQL_Query);
		
		if ($rs===false)
			die("Impossible de récupérer les factures fournisseurs");
		
		if ($rs->RecordCount()>0){
			?>
			<div class="mainContent fullWidthContent">
            <span class="showdevinfo"><?php showDevInfo( __FILE__ );  ?></span>
            	<div class="topRight"></div><div class="topLeft"></div>
                <div class="content">
                	<div class="headTitle">
                        <p class="title"><?php echo $resultCount ?> facture(s) trouvée(s)</p>
                        <?php
							$exportURL = "$GLOBAL_START_URL/accounting/search_supplier_invoices.php?export";
							
							$Enow = getdate();
								
							$Edate_type 			= "&amp;date_type=";
							$Eminus_date_select 	= "&amp;minus_date_select=";
							$Einterval_select 		= "&amp;interval_select=";
							$Eidsupplier 			= "&amp;idsupplier=";
							$Ebilling_supplier		= "&amp;billing_supplier=";
							$Etotal_amount			= "&amp;total_amount=";
							$Ebt_start				= "&amp;bt_start=";
							$Ebt_stop				= "&amp;bt_stop=";
		 	
							$Edate_type 			.= isset( $_POST[ "date_type" ] ) ? $_POST[ "date_type" ] : "DateHeure";
							$Eminus_date_select 	.= isset( $_POST[ "minus_date_select" ] ) ? $_POST[ "minus_date_select" ] : "-365";
							$Einterval_select 		.= isset( $_POST[ "interval_select" ] ) ? $_POST[ "interval_select" ] : 1;
							$Eidsupplier 			.= isset( $_POST[ "idsupplier" ] ) ? $_POST[ "idsupplier" ] : "";
							$Ebilling_supplier		.= isset( $_POST[ "billing_supplier" ] ) ? $_POST[ "billing_supplier" ] : "";
							$Etotal_amount			.= isset( $_POST[ "total_amount" ] ) ? $_POST[ "total_amount" ] : "";
							$Ebt_start				.= $_POST[ "bt_start" ];
							$Ebt_stop				.= $_POST[ "bt_stop" ];
							

							
							$exportURLElements = $Edate_type.$Eminus_date_select.$Einterval_select.$Eidsupplier.$Ebilling_supplier.$Etotal_amount.$Ebt_start.$Ebt_stop;
								
							if( isset( $_GET[ "sortby" ] ) && isset( $_GET[ "sens" ] ) )
								$exportURLElements .= "&amp;sortby=" . htmlentities( $_GET[ "sortby" ] ) . "&amp;sens=" . htmlentities( $_GET[ "sens" ] );							
							
							
							
							
						?>
						<div class="rightContainer">
							<a href="<?php echo $exportURL.$exportURLElements ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les donnéés au format CSV" /></a>
						</div>
                    </div>
                    <div class="subContent">
						<div class="tableHeaderLeft">
							<a href="#" class="goUpOrDown">
								Aller en bas de page
								<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
							</a>
						</div>
						<div class="tableHeaderRight">
						</div>
                    	<!-- tableau résultats recherche -->
                        <div class="resultTableContainer clear">
                        	<table class="dataTable resultTable">
                            	<thead>
							       	<tr>
							       		<th>Com.</th>
							       		<th>Facture n°</th>
							       		<th>Fournisseur</th>
							       		<th>Statut</th>
							       		<th>Date de facture</th>
								      	<th>Total TTC</th>
								      	<th>Commande fournisseur n°</th>
								      	<th>Date demande avoir & objet</th>
								      	<th>Avoir n°</th>
								      	<th>Date avoir</th>
								      	<th>Montant avoir TTC</th>
								      	<th></th>
							       	</tr>
									<tr>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'user.initial', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'user.initial', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.billing_supplier', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.billing_supplier', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'supplier.name', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'supplier.name', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.status', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.status', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.Date', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.Date', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder"></th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder">
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'ASC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png';" alt="tri ascendant" /></a>
											<a href="#" onclick="SortResult( 'billing_supplier.total_amount', 'DESC' ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" onmouseover="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc_hover.png';" onmouseout="this.src='<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png';" alt="tri descendant" /></a>
										</th>
										<th class="noTopBorder"></th>
									</tr>
						       	</thead>
						       	<tbody>
<?php
								
								$totaliseTTC = 0;
								
								for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
									
									$billing_supplier		= $rs->fields( "billing_supplier" );
									$initials				= $rs->fields( "initial" );
									$Date					= $rs->fields( "Date" );
									$creation_date			= $rs->fields( "creation_date" );
									$deliv_payment			= $rs->fields( "deliv_payment" );
									$supplier_name			= $rs->fields( "name" );
									$idsupplier				= $rs->fields( "idsupplier" );
									$total_amount			= $rs->fields( "total_amount" );
									$totaliseTTC			+= $total_amount;
									$status					= $rs->fields( "status" );
									$bls					= SupplierInvoice::getInvoicedDeliveryNotes( $billing_supplier, $idsupplier );
									$os						= SupplierInvoice::getSupplierInvoiceOS( $billing_supplier, $idsupplier );
									$credit_request_date 	= $rs->fields( "credit_request_date" );
									$credit_request_object 	= $rs->fields( "credit_request_object" );
									$idcredit 				= $rs->fields( "idcredit_supplier" ) == NULL ? $rs->fields( "idcredit_supplier" ) : explode( ',' , $rs->fields( "idcredit_supplier" ) );
									$credit_date 			= $rs->fields( "credit_date" ) == NULL ? $rs->fields( "credit_date" ) : explode( ',' , $rs->fields( "credit_date" ) );
									$credit_amount 			= $rs->fields ( "amount" ) == NULL ? $rs->fields ( "amount" ) : explode( ',' , $rs->fields( "amount" ) );
							
?>									
	
						  <script type="text/javascript">
							/* <![CDATA[ */
							   
							     $(document).ready(function(){
							        $("#helpObject_<?=$i?>").qtip({
							            adjust: {
							                mouse: true,
							                screen: true
							            },
										content: '<b>Objet de l\'avoir :</b><br /> <?php echo str_replace("\n","",str_replace("\r\n","",nl2br($credit_request_object))) ?>',
							            position: {
							                corner: {
							                    target: "leftMiddle",
							                    tooltip: "rightMiddle"
							                }
							            },
							            style: {
							                background: "#FFFFFF",
							                border: {
							                    color: "#EB6A0A",
							                    radius: 5
							                },
							                fontFamily: "Arial,Helvetica,sans-serif",
							                fontSize: "13px",
							                tip: true
							            }
							        });
							    });
							/* ]]> */
							</script>
				        <tr class="blackText">
				        	<td class="lefterCol"><?php echo $initials ?></td>
				        	<td><?php echo $billing_supplier ?></td>
				        	<td><a class="grasBack" href="/product_management/enterprise/supplier.php?idsupplier=<?php echo $idsupplier ?>"><?php echo $supplier_name ?></a></td>
				        	<td><?php echo Dictionnary::translate( $status ) ?></td>
							<td><?php echo Util::dateFormatEu( $Date, false, "-" ) ?></td>
							<td style="text-align:right; white-space:nowrap;"><?php echo Util::priceFormat( $total_amount ) ?></td>
							<td>
							<?php
							
							if( is_array( $os ) ){
								
								for( $j = 0 ; $j < count( $os ) ; $j++ ){
									
									if( $j )
										echo " - ";
									
									echo "<a href=\"$GLOBAL_START_URL/catalog/pdf_supplier_order.php?idorder=$os[$j]\" target=\"_blank\">$os[$j]</a>";
										
								}
								
							}
							
							?>
							</td>
							<td style="white-space:nowrap;">
							<?
							if(strlen($credit_request_object)>0){?>
								<img id="helpObject_<?=$i ?>" src="<?=$GLOBAL_START_URL?>/images/back_office/content/help.png" alt="Voir l'objet de l'avoir" style="float:right;" />
							<?php } ?>
							<?if($credit_request_date!='0000-00-00'){
								echo Util::dateFormatEu( $credit_request_date, false, "-" );
							}else{?>
								<input type="text" class="calendarInput" id="date_<?php echo $i ?>" />
								<?php echo DHTMLCalendar::calendar( "date_" . $i, true, "creditRequest" ) ?>
								<input type="hidden" id="billing_supplier_<?php echo $i ?>" value="<?php echo $billing_supplier ?>" />
								<input type="hidden" id="idsupplier_<?php echo $i ?>" value="<?php echo $idsupplier ?>" />
							<?php } ?>
						</td>
						<td>
							<?
							if( count( $idcredit ) ){
								echo "<ul style='list-style:none;margin:0;padding:0;'>";
								for( $j = 0 ; $j < count( $idcredit ) ; $j++ )					
									echo "<li style='margin:0;padding:0;'>".$idcredit[$j]."</li>";
								
								echo "</ul>";
							}
							?>
							
						</td>
						<td><?
							if( count( $credit_date ) ){
								echo "<ul style='list-style:none;margin:0;padding:0;'>";
								for( $j = 0 ; $j < count( $credit_date ) ; $j++ )					
									echo "<li style='margin:0;padding:0;'>".$credit_date[$j]."</li>";
								
								echo "</ul>";
							}					
						?></td>
						<td class="righterCol">
							<?
							if( count( $credit_amount ) ){
								echo "<ul style='list-style:none;margin:0;padding:0;'>";
								for( $j = 0 ; $j < count( $credit_date ) ; $j++ )					
									echo "<li style='margin:0;padding:0;'>".Util::priceFormat( $credit_amount[$j] )."</li>";
								
								echo "</ul>";
							}?>
						</td>
						<td>
							<a href="#" onclick="registerCredit('<?=$billing_supplier?>','<?=$idsupplier?>')">Ajouter</a>
						</td>
						</tr>
				<?php
				$rs->MoveNext();			
			
			}?>
				       	<tr>
				       		<th colspan="5" style="border:none;"></th>
					      	<th class="totalAmount"><?php echo Util::priceFormat( $totaliseTTC ) ?></th>
					      	<th style="border:none;"></th>
					      	<th style="border:none;"></th>
				       	</tr>
					</tbody>
	            </table>
                </div>
                <a href="#" class="goUpOrDown">
                	Aller en haut de page
                    <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
                </a>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div> <!-- mainContent fullWidthContent -->
	<?
		}		
		paginate();
		
		?>
	</form>
	<?php

}


//-------------------------------------------------------------------------------------

function getSupplierName( $idsupplier ){
	
	
	
	$db = &DBUtil::getConnection();
	
	$query = "SELECT name FROM supplier WHERE idsupplier = $idsupplier LIMIT 1";
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( Dictionnary::translate("gest_com_impossible_recover_supplier_name") );
		
	return $rs->fields( "name" );
	
}

//--------------------------------------------------------------------------------------------------

function paginate(){

	global 	$page,
			$pageCount,
			$GLOBAL_START_URL;
	
	if( $pageCount < 2 )
		return;
	
	?>
	<p style="text-align:center;">
		<b>Pages</b>&nbsp;
		<?php
	
	/*if( $page > 1 ){
	
		$prev = $page - 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $prev ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/

	$i = 1;
	while( $i < $page ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	echo "&nbsp;<b>$page</b>&nbsp;";
	
	$i = $page + 1;
	while( $i < $pageCount + 1 ){
		
		?>
		<a href="#" onclick="goToPage( <?php echo $i ?> ); return false;"><?php echo $i ?></a>
		<?php
		
		$i++;
		
	}
	
	/*if( $page < $pageCount ){
	
		$next = $page + 1;
		
		?>
		<a href="#" onclick="goToPage( <?php echo $next ?> ); return false;"><img src="<?php echo $GLOBAL_START_URL ?>" alt="" stle="border-style:none;" /></a>
		<?php
			
	}*/
	
	?>
	</p>
	<?php
	
}

//--------------------------------------------------------------------------------------------------
function getBLTotalAmountHT($idbl_delivery){
	
	$bdd = DBUtil::getConnection();
	
	$Query = "SELECT osr.discount_price, osr.quantity FROM order_supplier os";	
}

//--------------------------------------------------------------------------------

function &getExportableArray(){

	global $resultPerPage, $iduser, $hasAdminPrivileges, $GLOBAL_START_URL, $GLOBAL_START_PATH, $ScriptName;

	$now = getdate();
	
	//die(print_r($_GET));
	
	$date_type 				= isset( $_GET[ "date_type" ] ) ? $_GET[ "date_type" ] : "DateHeure";
	$minus_date_select 		= isset( $_GET[ "minus_date_select" ] ) ? $_GET[ "minus_date_select" ] : "-365";
	$interval_select 		= isset( $_GET[ "interval_select" ] ) ? $_GET[ "interval_select" ] : 1;
	$idsupplier 			= isset( $_GET[ "idsupplier" ] ) ? $_GET[ "idsupplier" ] : "";
	$billing_supplier		= isset( $_GET[ "billing_supplier" ] ) ? $_GET[ "billing_supplier" ] : "";
	$total_amount			= isset( $_GET[ "total_amount" ] ) ? $_GET[ "total_amount" ] : "";
		
	switch ($interval_select){
			    
		case 1:
		 	//min |2000-12-18 15:25:53|
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"] + $minus_date_select ,$now["year"])) ;
		 	//max
			$Now_Time = date( "Y-m-d 23:59:59", mktime($now["hours"],$now["minutes"],$now["seconds"],$now["mon"],$now["mday"],$now["year"])) ;
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Now_Time' ";
			
			break;
			
		case 2:
			
			$yearly_month_select = $_GET['yearly_month_select'];
			
			if ($yearly_month_select > $now["mon"])
				$now["year"] --;
	
		 	//min
			$Min_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select, 1,$now["year"])) ;
		 	//max
			$Max_Time = date( "Y-m-d 00:00:00", mktime($now["hours"],$now["minutes"],$now["seconds"], $yearly_month_select+1, 1,$now["year"])) ;
		
			//sql_condition
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <'$Max_Time' ";
	     	break;
			     
		case 3:
		 	
		 	$Min_Time = euDate2us( $_GET[ "bt_start" ] );
		 	$Max_Time = euDate2us( $_GET[ "bt_stop" ] );
		 	
			$SQL_Condition  = "`billing_supplier`.$date_type >='$Min_Time' AND `billing_supplier`.$date_type <='$Max_Time' ";
	     	
	     	break;
			
	}
	
	if( !$hasAdminPrivileges )
		$SQL_Condition .= " AND `billing_supplier`.iduser = '$iduser'";
						
	if ( !empty($idsupplier) ){
			$idsupplier = FProcessString($idsupplier);
			$SQL_Condition .= " AND billing_supplier.idsupplier =$idsupplier";
	}
	
	if ( !empty($billing_supplier) ){
			$SQL_Condition .= " AND billing_supplier.billing_supplier ='$billing_supplier'";
	}
	
	if ( !empty($total_amount) ){
			$SQL_Condition .= " AND billing_supplier.total_amount = $total_amount";
	}
	
	$con = DBUtil::getConnection();

	$page = isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1;
		
				

	$SQL_Condition	.= " AND billing_supplier.iduser = user.iduser
	AND billing_supplier.idsupplier = supplier.idsupplier";
		
	$tables = "billing_supplier, user, supplier";
	
	//nombre de pages

	$SQL_Query = "
	SELECT COUNT(*) AS resultCount 
	FROM $tables 
	WHERE $SQL_Condition";	

	$rs = $con->Execute($SQL_Query);
	
	if( $rs === false || $rs->fields( "resultCount" ) == null )
		die( "Impossible de récupérer le nombre de pages" );
		
	$resultCount = $rs->fields( "resultCount" );

	$pageCount = ceil( $resultCount / $resultPerPage );
	
	//recherche
	
	$start = ( $page - 1 ) * $resultPerPage;

	$query = "
	SELECT billing_supplier.billing_supplier, 
		billing_supplier.idsupplier,
		billing_supplier.status,
		supplier.name,
		billing_supplier.Date,
		billing_supplier.creation_date, 
		billing_supplier.deliv_payment, 
		user.initial,
		billing_supplier.total_amount
	FROM billing_supplier, user, supplier
	WHERE $SQL_Condition
	ORDER by billing_supplier.billing_supplier ASC
	";
	
	if( isset( $_REQUEST[ "sortby" ] )&& isset( $_REQUEST[ "sens" ] ) )
		$SQL_Query .= "
		ORDER BY " . $_REQUEST[ "sortby" ] . " " . $_REQUEST[ "sens" ] ;	
	
	$rs =& DBUtil::query( $query );
	
	$data = array();
	
	while( !$rs->EOF() ){
		
		$billing_supplier	= $rs->fields( "billing_supplier" );
		$initials			= $rs->fields( "initial" );
		$deliv_payment		= $rs->fields( "deliv_payment" );
		$supplier_name		= $rs->fields( "name" );
		$idsupplier			= $rs->fields( "idsupplier" );
		$total_amount		= $rs->fields( "total_amount" );
		$status				= $rs->fields( "status" );
		$bls				= SupplierInvoice::getInvoicedDeliveryNotes( $billing_supplier, $idsupplier );
		$os					= SupplierInvoice::getSupplierInvoiceOS( $billing_supplier, $idsupplier );
	
		if( $deliv_payment == "0000-00-00" )
			$deliv_payment = "";
		else
			$deliv_payment = usDate2eu( $deliv_payment );
			
		$Date = usDate2eu( $rs->fields( "Date" ) );
		$creation_date = usDate2eu( $rs->fields( "creation_date" ) );
	  	
	  	$txtos="";
	  	if(is_array($os)){
			for($j=0;$j<count($os);$j++){
				if($j)
					$txtos .= "-";
					
				$txtos .= $os[$j];	
			}	
		}

	  	
	  	$txtBls = "";
		if(is_array($bls)){
			for($k=0;$k<count($bls);$k++){
				if($k)
					$txtBls .= "-";
					
				$txtBls .= $bls[$k];	
			}	
		}


	   	$data[ "Commercial" ][]				= $initials;
		$data[ "Facture n°" ][] 			= $billing_supplier;
		$data[ "Fournisseur" ][] 			= $supplier_name;
		$data[ "Statut" ][] 				= Dictionnary::translate( $status );
		$data[ "Date de facture" ][] 		= $Date;
		$data[ "Date enregistrement" ][] 	= $creation_date;
		$data[ "Date d'échéance" ][] 		= $deliv_payment;
	  	$data[ "Total TTC" ][] 				= Util::numberFormat( $total_amount );
	  	$data[ "Commande fournisseur n°" ][]= $txtos;
	  	$data[ "BL n°" ][] 					= $txtBls;	
	  	
		$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------

function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportArray.php" );
	
	$cvsExportArray = new CSVExportArray( $exportableArray,"factures_fournisseur_" . date( "Ymd" ) );
	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------
?>