<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Remplacer une valeur par une autre
 */

include_once( "../config/init.php" );
include_once( dirname( __FILE__ ) . "/../catalog/drawlift.php" );
include_once( dirname( __FILE__ ) . "/../objects/XHTMLFactory.php" );


//---------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------


/**
 * Formulaire de recherche
 */
$replace 			= isset( $_POST[ "replace" ] ) && !empty( $_POST[ "replace" ] ) ? intval( $_POST[ "replace" ] ) : "";
$valchoice 			= isset( $_POST[ "valchoice" ] ) && !empty( $_POST[ "valchoice" ] ) ? intval( $_POST[ "valchoice" ] ) : "";

if ( isset( $_POST[ "replace" ] ) && !empty( $_POST[ "replace" ] )){
	$valchoice = $_POST[ "valchoice" ];
	if( isset( $_POST[ "replace" ] ) && !empty( $_POST[ "replace" ] ) )
	$replace = $_POST[ "replace" ];

/* ---------------------------------------------- modèle : Client ---------------------------------------------*/
if($_POST[ "modeles" ] == 1){

	
	$upde = "UPDATE `estimate` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupde = DBUtil::query($upde);
	
	$updc = "UPDATE `order` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdc = DBUtil::query($updc);
	
	$updf = "UPDATE `billing_buyer` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdf = DBUtil::query($updf);
	
	$updb = "UPDATE `bl_delivery` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdb = DBUtil::query($updb);
	
	$updr = "UPDATE `regulations_buyer` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdr = DBUtil::query($updr);
	
	$upda = "UPDATE `credits` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupda = DBUtil::query($upda);
	
	$updd = "UPDATE `delivery` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdd = DBUtil::query($updd);
	
	$updbil = "UPDATE `billing_buyer` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdbil = DBUtil::query($updbil);
	
	$updrep = "UPDATE `repayments` SET idbuyer='".$replace."' WHERE idbuyer = '".$valchoice."'";
	$rsupdrep = DBUtil::query($updrep);
	
	$updac = "UPDATE `account_plan` SET idrelation='".$replace."' WHERE idrelation = '".$valchoice."'";
	$rsupdac = DBUtil::query($updac);
	

	
	}

/*--------------------------------------- modèle : Intitulé ----------------------------------*/	
	if($_POST[ "modeles" ] == 2){
	
	
	$updil = "UPDATE `intitule_link` SET idintitule_title='".$replace."' WHERE idintitule_title = '".$valchoice."'";
	$rsupil = DBUtil::query($updil);
	
	
	}

/*--------------------------------------- modèle : Commercial ----------------------------------*/	
 if($_POST[ "modeles" ] == 5){

	
	$upde = "UPDATE `estimate` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupde = DBUtil::query($upde);
	
	$updc = "UPDATE `order` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdc = DBUtil::query($updc);
	
	$updf = "UPDATE `billing_buyer` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdf = DBUtil::query($updf);
	
	$updb = "UPDATE `bl_delivery` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdb = DBUtil::query($updb);
	
	$updr = "UPDATE `regulations_buyer` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdr = DBUtil::query($updr);
	
	$upda = "UPDATE `credits` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupda = DBUtil::query($upda);
	
	$updbil = "UPDATE `billing_buyer` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdbil = DBUtil::query($updbil);

	$updbu = "UPDATE `buyer` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdbu = DBUtil::query($updbu);
	
	$updco = "UPDATE `contact_follow` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdco = DBUtil::query($updco);

	$updos = "UPDATE `order_supplier` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdos = DBUtil::query($updos);
	
	$updbs = "UPDATE `billing_supplier` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdbs = DBUtil::query($updbs);
	
	$updlit = "UPDATE `litigations` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdlit = DBUtil::query($updlit);
	
	$updlith = "UPDATE `litigation_history` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdlith = DBUtil::query($updlith);
	
	$updregs = "UPDATE `regulations_supplier` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdregs = DBUtil::query($updregs);
	
	$updregp = "UPDATE `regulations_provider` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdregp = DBUtil::query($updregp);	
	
	$updpi = "UPDATE `provider_invoice` SET iduser='".$replace."' WHERE iduser = '".$valchoice."'";
	$rsupdpi = DBUtil::query($updpi);
	
	}
	
}

?>
		
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/queryLoader.css"  type="text/css" />
<script type='text/javascript' src='<?php echo $GLOBAL_START_URL ?>/js/queryLoader.js'></script>
<div class="contentDyn" style="padding:25px ;margin:0px 265px;">

					
	<div class="blocSearch"><div style="margin:25px;">
	<form name="search_form" action="/administration/modele.php" method="post">

		<!-- Left -->
		<div class="blocMultiple blocMLeft" style="padding:25px ;margin:0px 200px;">
			<strong>             Mise à jour effectuée avec succés, <br>
            La valeur n° <?php echo $valchoice ?> a été remplacé par  <?php echo $_POST[ "replace" ] ?></strong>
			
			<div class="spacer"></div>
			
		
		</div>
		
		<!-- Right -->
		<div class="blocMultiple blocMRight">		
			</div>
		<div class="spacer"></div>
        
           <input type="submit" class="inputSearch" value="Retour au sélection" />
        <div class="spacer"></div>     
		
	</div></div>
	<div class="spacer"></div>


	
</div>       		      	
<div class="spacer"></div>  
<?php     


//---------------------------------------------------------------------------------


include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//---------------------------------------------------------------------------------




//---------------------------------------------------------------------------------

?>