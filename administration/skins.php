<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Personnalisation barre haute administration
 */

include_once( "../config/init.php" );

//------------------------------------------------------------------------------------------
/* Traitement Ajax de l'enregistrement des dates */

if( isset( $_GET[ "skin" ] ) ){
	
	$rs =& DBUtil::query( "UPDATE `user` SET skin = '" . $_GET[ "skin" ] . "' WHERE iduser = " . User::getInstance()->getId() );
	
	if( $rs === false )
		exit( "1" );
	
	exit( "0" );
	
}

//------------------------------------------------------------------------------------------
/* Traitement Ajax de l'enregistrement des couleurs */

if( isset( $_GET[ "color" ] ) ){
	
	$rs =& DBUtil::query( "UPDATE `user` SET color = '#" . $_GET[ "color" ] . "' WHERE iduser = " . User::getInstance()->getId() );
	
	if( $rs === false )
		exit( "1" );
	
	exit( "0" );
	
}

//------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$skin = User::getInstance()->get( "skin" );

?>
<script type="text/javascript">
<!--
/* <![CDATA[ */
	
	function setActiveStyleSheet( title ){
		
		var i, a, main;
		
		for( i = 0 ; ( a = document.getElementsByTagName( "link" )[ i ] ); i++ ){
			
			if( a.getAttribute( "rel" ).indexOf( "style" ) != -1 && a.getAttribute( "title" ) ){
				
				if( a.getAttribute( "title" ) == title )
					a.disabled = false;
				else
					a.disabled = true;
				
			}
		}
		
		$.ajax({
			type:	"GET",
			url:	"<?php echo $GLOBAL_START_URL ?>/administration/skins.php?skin=" + title,
			error:	function( XMLHttpRequest, textStatus, errorThrown ){ $.growlUI( '', "Problème lors de l'enregistrement" ); },
		 	success: function( responseText ){
				if( responseText == "0" )
					$.growlUI( '', 'Modification enregistrée' );
				else
					$.growlUI( '', "Problème lors de l'enregistrement" );
			}
		});
		
	}
	
/* ]]> */
-->
</script>
<div id="globalMainContent">
	<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Préférences utilisateurs</p>
			</div>
			<div class="subContent">
				Choix de la charte graphique
				<select onchange="setActiveStyleSheet( this.value );">
					<option value="blue"<?php echo empty( $skin ) || $skin == "blue" ? " selected=\"selected\"" : "" ?>>Bleu</option>
					<option value="green"<?php echo $skin == "green" ? " selected=\"selected\"" : "" ?>>Vert</option>
					<option value="gray"<?php echo $skin == "gray" ? " selected=\"selected\"" : "" ?>>Gris</option>
					<option value="red"<?php echo $skin == "red" ? " selected=\"selected\"" : "" ?>>Rouge</option>
					<option value="orange"<?php echo $skin == "orange" ? " selected=\"selected\"" : "" ?>>Orange</option>
					<option value="pink"<?php echo $skin == "pink" ? " selected=\"selected\"" : "" ?>>Rose</option>
					<option value="yellow"<?php echo $skin == "yellow" ? " selected=\"selected\"" : "" ?>>Jaune</option>
					<option value="purple"<?php echo $skin == "purple" ? " selected=\"selected\"" : "" ?>>Violet</option>
					<option value="hermes"<?php echo $skin == "hermes" ? " selected=\"selected\"" : "" ?>>Hermès</option>
				</select>
				<div style="height:15px;"></div>
				Couleur de l'utilisateur pour les relances
				<style type="text/css">
				<!--
					
					#red, #green, #blue {
						float: left;
						clear: left;
						width: 300px;
						margin: 15px;
					}
					#swatch {
						width: 120px;
						height: 100px;
						margin-top: 18px;
						margin-left: 350px;
						background-image: none;
					}
					#red .ui-slider-range { background: #ef2929; }
					#red .ui-slider-handle { border-color: #ef2929; }
					#green .ui-slider-range { background: #8ae234; }
					#green .ui-slider-handle { border-color: #8ae234; }
					#blue .ui-slider-range { background: #729fcf; }
					#blue .ui-slider-handle { border-color: #729fcf; }
					
				-->
				</style>
				<script type="text/javascript">
				<!--
					
					function hexFromRGB (r, g, b) {
						var hex = [
							r.toString(16),
							g.toString(16),
							b.toString(16)
						];
						$.each(hex, function (nr, val) {
							if (val.length == 1) {
								hex[nr] = '0' + val;
							}
						});
						return hex.join('').toUpperCase();
					}
					
					function refreshSwatch() {
						var red = $("#red").slider("value")
							,green = $("#green").slider("value")
							,blue = $("#blue").slider("value")
							,hex = hexFromRGB(red, green, blue);
						$("#swatch").css("background-color", "#" + hex);
					}
					
					function saveColor(){
						
						var red = $("#red").slider("value")
							,green = $("#green").slider("value")
							,blue = $("#blue").slider("value")
							,hex = hexFromRGB(red, green, blue);
						
						$.ajax({
							type: 'GET',
							url: '<?php echo $GLOBAL_START_URL ?>/administration/skins.php?color=' + hex,
							error:	function( XMLHttpRequest, textStatus, errorThrown ){ $.growlUI( '', "Problème lors de l'enregistrement" ); },
						 	success: function( responseText ){
								if( responseText == "0" )
									$.growlUI( '', 'Modification enregistrée' );
								else
									$.growlUI( '', "Problème lors de l'enregistrement" );
							}
						});
						
					}
					
					$(document).ready(function(){
						$("#red, #green, #blue").slider({
							orientation: 'horizontal',
							range: "min",
							max: 255,
							value: 127,
							slide: refreshSwatch,
							change: refreshSwatch,
							stop: saveColor
						});
						$("#red").slider("value", <?php echo getUserColor( "red" ) ?>);
						$("#green").slider("value", <?php echo getUserColor( "green" ) ?>);
						$("#blue").slider("value", <?php echo getUserColor( "blue" ) ?>);
					});
					
				-->
				</script>
				<div id="red"></div>
				<div id="green"></div>
				<div id="blue"></div>
				<div id="swatch" class="ui-widget-content ui-corner-all"></div>
				<div class="clear"></div>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------

function getUserColor( $color ){
	
	$dbValue = User::getInstance()->get( "color" );
	
	if( $dbValue == "" )
		return "0";
	
	switch( $color ){
		case "red":
			return hexdec( substr( $dbValue, 1, 2 ) );
		case "green":
			return hexdec( substr( $dbValue, 3, 2 ) );
		case "blue":
			return hexdec( substr( $dbValue, 5, 2 ) );
	}
	
}

//------------------------------------------------------------------------------------------

?>