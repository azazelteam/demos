<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Outil de requete dans la base
 */


include_once( "../config/init.php" );
include_once( dirname( __FILE__ ) . "/../catalog/drawlift.php" );
include_once( dirname( __FILE__ ) . "/../objects/XHTMLFactory.php" );

//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------
	
?>
<script language="javascript">
<!--
function getmodele( ){
	if( $( "#modele" ).val() == '' ){
			 document.getElementById('error1').style.visibility = "visible";
		} else {
			var modele = $( "#modele" ).val();
			
			
			document.location = '/administration/transfert.php?modele=' + $( "#modele" ).val();
			<?php
		
			$modele = $_REQUEST[ "modele" ];
		
		?>
	}
	}
	
//-->
</script>
		
		
		
		
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/queryLoader.css"  type="text/css" />
<script type='text/javascript' src='<?php echo $GLOBAL_START_URL ?>/js/queryLoader.js'></script>

<form name="modele_form" action="/administration/transfert.php" method="post">



<div class="centerMax">
<div id="tools" class="rightTools">
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-star.png" alt="star" />&nbsp;
	Accès rapide</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
	<h2>&nbsp;<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/layout/icons-menu-chiffre.png" alt="star" />&nbsp;
	Quelques chiffres...</h2>
	<div class="spacer"></div>
	
	<div class="blocSearch">
		&nbsp;
	</div>
	
</div>	
<div class="contentDyn">
	
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Choisir un modèle de modification</span>
	
	<div class="spacer"></div>
	</h1>
		<div class="blocSearch"><div style="margin:5px;">
	
		<!-- Left -->
		<div class="blocMultiple blocMLeft">
			<div id="tables">
              <div id="error1" style="visibility:hidden">
                        <p class="msgError">Vous devez saisir un modèle.</p>
                       
                        </div>
                         <div class="spacer"></div>
            <label><span><strong>sélection du modèle</strong></span></label>
           
             <select  name="modele" id="modele" onchange="getmodele();" >
                  <option selected="selected" value="" >-----</option>
                  <option<?php if( $_REQUEST['modele'] == $modele) echo " selected=\"selected\""; ?> value="1" >Client</option>
                  <option<?php if( $_REQUEST['modele'] == $modele) echo " selected=\"selected\""; ?> value="2" >Intitulé</option>
		          <option<?php if( $_REQUEST['modele'] == $modele) echo " selected=\"selected\""; ?> value="3" >Commande fournisseur</option>
				  <option<?php if( $_REQUEST['modele'] == $modele) echo " selected=\"selected\""; ?> value="4" >Société service</option>
				  <option<?php if( $_REQUEST['modele'] == $modele) echo " selected=\"selected\""; ?> value="5" >Commercial</option>
            </select>
            
         </div><br><br>
         <div class="spacer"></div>
        
		</div>
		
		
		
		<!-- Right -->
		<div class="blocMultiple blocMRight">		
			</div>
		<div class="spacer"></div>
       
		
	</div></div>
	<div class="spacer"></div>

</form>
	
</div>       		      	
<div class="spacer"></div>       

<?php
//---------------------------------------------------------------------------------


include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//---------------------------------------------------------------------------------


//---------------------------------------------------------------------------------

//---------------------------------------------------------------------------------
?>

 

