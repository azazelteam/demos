<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Génération du sitemap Google merchant
 */

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/GoogleMap.php" );

//--------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div>
        <div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Sitemap généré</p>
                <div class="rightContainer"></div>
            </div>
            <div class="subContent">
				<?php crawl(); ?>
			</div><!-- subContent -->
		</div><!-- content -->
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div><!-- mainContent -->
	<div class="clear"></div>
</div><!-- globalMainContent -->
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//--------------------------------------------------------------------------------------

function crawl(){
 	 
 	 $googlemap = new GoogleMap();
 	 
 	//post data

 	$googlemap->setOutputFile( stripslashes( $_POST[ "outputfile" ] ) );
 	$googlemap->setGetLastModificationMethod( intval( $_POST[ "lastmodification" ] ) );
 	$googlemap->setCategoryChangeFrequency( $_POST[ "categoryChangeFrequency" ] );
 	$googlemap->setProductChangeFrequency( $_POST[ "productChangeFrequency" ] );
 	$googlemap->setPDFChangeFrequency( $_POST[ "pdfChangeFrequency" ] );
 	$googlemap->setStaticPageChangeFrequency( $_POST[ "staticPageChangeFrequency" ] );
 	$googlemap->setIndexPDF( intval( $_POST[ "indexpdf" ] ) ? true : false );
	$googlemap->clearStaticPages();
	
 	$tokens = explode( "\n", stripslashes( $_POST[ "staticpages" ] ) );
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->addStaticPage( trim( $tokens[ $i ] ) );
			
		$i++;
		
	}
	
	$googlemap->clearBlackLists();
	
	$tokens = explode( "\n", stripslashes( $_POST[ "categoryBlackList" ] ) );
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->blackListCategory( intval( trim( $tokens[ $i ] ) ) );
			
		$i++;
		
	}
	
	$tokens = explode( "\n", stripslashes( $_POST[ "productBlackList" ] ) );
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->blackListProduct( intval( trim( $tokens[ $i ] ) ) );
			
		$i++;
		
	}
	
	$tokens = explode( "\n", stripslashes( $_POST[ "pdfBlackList" ] ) );
	$i = 0;
	while( $i < count( $tokens ) ){
	
		if( strlen( trim( $tokens[ $i ] ) ) )
			$googlemap->blackListPDF( intval( trim( $tokens[ $i ] ) ) );
			
		$i++;
		
	}
 	
	//crawl

	if( isset( $_POST[ "saveConfig" ] ) )
		$googlemap->saveConfig();
	
	$googlemap->crawl();
 	
 	global $GLOBAL_START_URL;
 	
 	if( isset( $_POST[ "saveConfig" ] ) ){
 		
 		?>
 		<p style="font-weight:bold;">Votre configuration a bien été enregistrée.</p>
 		<?php
 			
 	}
 	
	?>
	<p>Nombre de liens ajoutés au sitemap : <b><?php echo $googlemap->getChildCount() ?></b></p>
	<p style="margin-top:15px;">Le sitemap ci-dessous a été enregistré à l'emplacement suivant : <a href="<?php echo $GLOBAL_START_URL . stripslashes( $_POST[ "outputfile" ] ) ?>" onclick="window.open(this.href); return false;"><?php echo $GLOBAL_START_URL . stripslashes( $_POST[ "outputfile" ] ) ?></a></p>
	<textarea style="width:100%; height:350px; margin-top:20px; overflow:auto; background:transparent; border:1px solid #E7E7E7;"><?php echo $googlemap->getXMLDocument() ?></textarea>
	<p style="text-align:center; margin-top:15px;">
		<input type="button" name="" value="Retour" class="blueButton"  onclick="history.back();" />
	</p>
	<?php
 	
 }

 //--------------------------------------------------------------------------------------
 
?>