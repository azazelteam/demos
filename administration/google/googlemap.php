<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Génération du sitemap Google
 */
 
 //-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once ( "$GLOBAL_START_PATH/objects/GoogleMap.php" );

$googleMap = new GoogleMap();

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<form action="googlemap_crawl.php" method="post" enctype="multipart/form-data">
	<div id="globalMainContent">
		<div class="mainContent" style="width:700px; margin-left:30px;">
	        <div class="topRight"></div>
	        <div class="topLeft"></div>
	        <div class="content">
	        	<div class="headTitle">
	                <p class="title">Configuration du sitemap</p>
	                <div class="rightContainer"></div>
	            </div>
	            <div class="subContent">
					<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
				    	<table class="dataTable">
			    			<tr>
								<th class="filledCell" style="vertical-align:top; width:50%;">
									Dernier sitemap créé
								</th>
								<td style="vertical-align:top; width:50%;">
								<?php
								
									if( strlen( $googleMap->getOutputFile() ) && file_exists( $GLOBAL_START_PATH . $googleMap->getOutputFile() ) ){
										
										?>
										<p>
											<a href="<?php echo $GLOBAL_START_URL . $googleMap->getOutputFile() ?>" onclick="window.open(this.href); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="aperçu" style="border-style:none; vertical-align:middle; float:right;" /></a>
											<b>Date de création :</b> <?php echo strlen( $googleMap->getLastCreationDate() ) ? Util::dateFormatEu( $googleMap->getLastCreationDate() ) : "inconnue" ?>
										</p>
										<?php
										
									}
									else echo "Vous n'avez créé aucun sitemap";
									
								?>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top; width:50%;">
									Enregistrer le sitemap sous
									<div style="font-weight:normal;">
										Emplacement auquel enregistrer le fichier du sitemap
										<br />exemple : 
										<span style="margin:10px; padding:5px; border:1px solid #A2A2A2; background-color:#FFFFFF; width:50%; float:left;">/sitemap.xml</span>
									</div>
								</th>
								<td style="vertical-align:top; width:50%;"><input class="textInput" type="text" id="outputfile" name="outputfile" value="<?php echo $googleMap->getOutputFile() ?>" /></td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Date de dernière modification
									<div style="font-weight:normal;">
										Signaler à Google quelle est la dernière date de modification de chaque fichier en utilisant la méthode sélectionnée.
									</div>
								</th>
								<td style="vertical-align:top;">
									<select name="lastmodification">
										<option value="<?php echo GoogleMap::$LAST_MOD_CURRENTDATE ?>"<?php if( $googleMap->getGetLastModificationMethod() == GoogleMap::$LAST_MOD_CURRENTDATE ) echo " selected=\"selected\""; ?>>Date du jour</selected>
										<option value="<?php echo GoogleMap::$LAST_MOD_CHECKSUM ?>"<?php if( $googleMap->getGetLastModificationMethod() == GoogleMap::$LAST_MOD_CHECKSUM ) echo " selected=\"selected\""; ?>>Somme de contrôle du fichier</selected>
										<option value="<?php echo GoogleMap::$LAST_MOD_READFROMFILE ?>"<?php if( $googleMap->getGetLastModificationMethod() == GoogleMap::$LAST_MOD_READFROMFILE ) echo " selected=\"selected\""; ?>>Date de dernière modification du fichier</selected>
									</select>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Indexer les PDF
									<div style="font-weight:normal;">
										( ajouter les URL des PDF au sitemap )
									</div>
								</th>
								<td class="textInput" style="vertical-align:top;">
									<input type="radio" name="indexpdf" value="1" <?php if( $googleMap->getIndexPDF() ) echo " checked=\"checked\""; ?> /> oui
									<input type="radio" name="indexpdf" value="0" <?php if( !$googleMap->getIndexPDF() ) echo " checked=\"checked\""; ?> /> non
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Liste des pages statiques à inclure dans le sitemap
									<div style="font-weight:normal;">
										( Insérez un saut de ligne entre chaque page )
										<br />exemple :
										<span style="margin:10px; padding:5px; border:1px solid #A2A2A2; background-color:#FFFFFF; width:50%; float:left;">
											/contact.php
											<br />/promotions.php
											<br />/destockage.html
										</span>
									</div>
								</th>
								<td class="textInput" style="vertical-align:top;">
									<textarea name="staticpages" style="width:100%; height:130px;"><?php
									
										$i = 0;
										while( $i < $googleMap->getStaticPageCount() ){
											
											if( $i )
												echo "\n";
												
											echo $googleMap->getStaticPageAt( $i )->getURI();
											
											$i++;
											
										}
									
									?></textarea>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Fréquence de mise à jour des catégories
									<div style="font-weight:normal;">
										Signaler à Google que les catégories du sitemap sont mises à jour sur le site avec la fréquence sélectionnée
									</div>
								</th>
								<td style="vertical-align:top;">
									<select name="categoryChangeFrequency">
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_ALWAYS ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_ALWAYS ) echo " selected=\"selected\""; ?>>en permanence</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_HOURLY ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_HOURLY ) echo " selected=\"selected\""; ?>>toutes les heures</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_DAILY ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_DAILY ) echo " selected=\"selected\""; ?>>tous les jours</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_WEEKLY ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_WEEKLY ) echo " selected=\"selected\""; ?>>toutes les semaines</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_MONTHLY ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_MONTHLY ) echo " selected=\"selected\""; ?>>tous les mois</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_YEARLY ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_YEARLY ) echo " selected=\"selected\""; ?>>tous les ans</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_NEVER ?>"<?php if( $googleMap->getCategoryChangeFrequency() == GoogleMap::$CHANGE_FREQ_NEVER ) echo " selected=\"selected\""; ?>>jamais</selected>
									</select>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Fréquence de mise à jour des produits
									<div style="font-weight:normal;">
										Signaler à Google que les produits du sitemap sont mis à jour sur le site avec la fréquence sélectionnée
									</div>
								</th>
								<td style="vertical-align:top;">
									<select name="productChangeFrequency">
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_ALWAYS ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_ALWAYS ) echo " selected=\"selected\""; ?>>en permanence</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_HOURLY ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_HOURLY ) echo " selected=\"selected\""; ?>>toutes les heures</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_DAILY ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_DAILY ) echo " selected=\"selected\""; ?>>tous les jours</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_WEEKLY ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_WEEKLY ) echo " selected=\"selected\""; ?>>toutes les semaines</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_MONTHLY ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_MONTHLY ) echo " selected=\"selected\""; ?>>tous les mois</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_YEARLY ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_YEARLY ) echo " selected=\"selected\""; ?>>tous les ans</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_NEVER ?>"<?php if( $googleMap->getProductChangeFrequency() == GoogleMap::$CHANGE_FREQ_NEVER ) echo " selected=\"selected\""; ?>>jamais</selected>
									</select>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Fréquence de mise à jour des PDF
									<div style="font-weight:normal;">
										Signaler à Google que les PDF du sitemap sont mis à jour sur le site à la fréquence sélectionnée
									</div>
								</th>
								<td style="vertical-align:top;">
									<select name="pdfChangeFrequency">
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_ALWAYS ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_ALWAYS ) echo " selected=\"selected\""; ?>>en permanence</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_HOURLY ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_HOURLY ) echo " selected=\"selected\""; ?>>toutes les heures</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_DAILY ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_DAILY ) echo " selected=\"selected\""; ?>>tous les jours</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_WEEKLY ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_WEEKLY ) echo " selected=\"selected\""; ?>>toutes les semaines</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_MONTHLY ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_MONTHLY ) echo " selected=\"selected\""; ?>>tous les mois</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_YEARLY ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_YEARLY ) echo " selected=\"selected\""; ?>>tous les ans</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_NEVER ?>"<?php if( $googleMap->getPDFChangeFrequency() == GoogleMap::$CHANGE_FREQ_NEVER ) echo " selected=\"selected\""; ?>>jamais</selected>
									</select>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Fréquence de mise à jour des pages statiques
									<div style="font-weight:normal;">
										Signaler à Google que les pages statiques du sitemap sont mises à jour sur le site à la fréquence sélectionnée
									</div>
								</th>
								<td style="vertical-align:top;">
									<select name="staticPageChangeFrequency">
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_ALWAYS ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_ALWAYS ) echo " selected=\"selected\""; ?>>en permanence</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_HOURLY ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_HOURLY ) echo " selected=\"selected\""; ?>>toutes les heures</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_DAILY ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_DAILY ) echo " selected=\"selected\""; ?>>tous les jours</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_WEEKLY ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_WEEKLY ) echo " selected=\"selected\""; ?>>toutes les semaines</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_MONTHLY ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_MONTHLY ) echo " selected=\"selected\""; ?>>tous les mois</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_YEARLY ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_YEARLY ) echo " selected=\"selected\""; ?>>tous les ans</selected>
										<option value="<?php echo GoogleMap::$CHANGE_FREQ_NEVER ?>"<?php if( $googleMap->getStaticPageChangeFrequency() == GoogleMap::$CHANGE_FREQ_NEVER ) echo " selected=\"selected\""; ?>>jamais</selected>
									</select>
								</td>
							</tr>
						</table>
					</div><!-- tableContainer -->
				</div><!-- subContent -->
			</div><!-- content -->
			<div class="content" style="margin-top:15px;">
	        	<div class="headTitle">
	                <p class="title">URL à exclure du sitemap</p>
	                <div class="rightContainer"></div>
	            </div>
	            <div class="subContent">
					<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">	
				    	<table class="dataTable">
							<tr>
								<th class="filledCell" style="vertical-align:top; width:50%;">
									Catégories à ignorer
									<div style="font-weight:normal;">
										( Insérez un saut de ligne entre chaque n° de catégorie )
										<br />exemple :
										<span style="margin:10px; padding:5px; border:1px solid #A2A2A2; background-color:#FFFFFF; width:50%; float:left;">
											10
											<br />1001
											<br />1204
										</span>
									</div>
								</th>
								<td class="textInput" style="vertical-align:top; width:50%;">
									<textarea name="categoryBlackList" style="width:100%; height:130px;"><?php
									
										$blackListedCategories = $googleMap->getBlackListedCategories();
										$i = 0;
										while( $i < count( $blackListedCategories ) ){
											
											if( $i )
												echo "\n";
												
											echo $blackListedCategories[ $i ];
											
											$i++;
											
										}
										
									?></textarea>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Produits à ignorer
									<div style="font-weight:normal;">
										( Insérez un saut de ligne entre chaque n° de produit )
										<br />exemple :
										<span style="margin:10px; padding:5px; border:1px solid #A2A2A2; background-color:#FFFFFF; width:50%; float:left;">
											100399
											<br />120346
											<br />121196
										</span>
									</div>
								</th>
								<td class="textInput" style="vertical-align:top;">
									<textarea name="productBlackList" style="width:100%; height:130px;"><?php
									
										$blackListedProducts = $googleMap->getBlackListedProducts();
										$i = 0;
										while( $i < count( $blackListedProducts ) ){
											
											if( $i )
												echo "\n";
												
											echo $blackListedProducts[ $i ];
											
											$i++;
											
										}
										
									?></textarea>
								</td>
							</tr>
							<tr>
								<th class="filledCell" style="vertical-align:top;">
									Fiches PDF à ignorer
									<div style="font-weight:normal;">
										( Insérez un saut de ligne entre chaque n° de produit )
										<br />exemple :
										<span style="margin:10px; padding:5px; border:1px solid #A2A2A2; background-color:#FFFFFF; width:50%; float:left;">
											100399
											<br />120346
											<br />121196
										</span>
									</div>
								</th>
								<td class="textInput" style="vertical-align:top;">
									<textarea name="pdfBlackList" style="width:100%; height:130px;"><?php
									
										$blackListedPDF = $googleMap->getBlackListedPDF();
										$i = 0;
										while( $i < count( $blackListedPDF ) ){
											
											if( $i )
												echo "\n";
												
											echo $blackListedPDF[ $i ];
											
											$i++;
											
										}
										
									?></textarea>
								</td>
							</tr>
						</table>
						<p style="text-align:center; margin-top:20px;">
							<input type="checkbox" name="saveConfig" value="1" checked="checked" /> Sauvegarder la configuration
							<input type="submit" class="blueButton" name="submitForm" value="Générer le Sitemap" />
						</p>
					</div><!-- tableContainer -->
				</div><!-- subContent -->
			</div><!-- content -->
			<div class="bottomRight"></div>
			<div class="bottomLeft"></div>
		</div><!-- mainContent -->
		<div class="clear"></div>
	</div><!-- globalMainContent -->
</form>
<?php 

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>