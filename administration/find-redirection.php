<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Génération des redirections dans le fichier .htaccess
 */

include_once(dirname(__FILE__)."/../config/init.php");
class Redirect
{
	public function generateHtacess()
	{
		$query = "select * from redirection_items where statut=1";
		$rs = & DBUtil::query($query);
		$i = 0;
		$file = "../.htaccess";
		
		$handle = file("../.htaccess");

		$tab1 = array();
		$tab2 = array();
		$i = 0;
		while($i < count($handle) && trim($handle[$i]) !== "########----#########" )
		{
			$i++;
		}
		for ($j = 0; $j < $i+1; $j++)
		{
			$tab1[$j] = $handle[$j];
		}
		
		$k = $i+1;
		while ($k < count($handle) && trim($handle[$k]) !== "########----#########" )
		{
			$k++;
		}
		
		for ($j = $k; $j < count($handle); $j++)
		{
			$tab2[$j-$k] = $handle[$j];
		}
		unset($handle);
		$handle = array();
		for ($j = 0; $j < count($tab1); $j++)
		{
			array_push($handle, $tab1[$j]);
		}

		while (!$rs->EOF())
		{ 	
			$source = $rs->fields("url_source");
			$cible = $rs->fields("url_cible");
			$type = $rs->fields("type");
			$dolarpos = substr($source, -1);
			if ($dolarpos != '$')
				$source = $rs->fields("url_source").'$';
			if ($type == 301)
				$data = "RewriteRule ^".$source." ".$cible."  [L,R=301]\n" ;
			else 
				$data = "RewriteRule ^".$source." ".$cible."  [L,R]\n" ;
	
			array_push($handle, $data);
			$rs->MoveNext();
		}
		
		for ($j = 0; $j < count($tab2); $j++)
		{
			array_push($handle, $tab2[$j]);
		}
		
		$k = 0;
		$f = fopen($file, "w");
		
		while ($k < count($handle))
		{
			fwrite($f, $handle[$k]);
			$k++;
		}
		fclose($f);
	}
}
?>