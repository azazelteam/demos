<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des lignes redirections
 */

 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/find-redirection.php" );

if (isset( $_POST["action"]) &&  ($_POST["action"]=="add" ) )
{
if(!empty($_POST["source"]) &&(!empty($_POST["cible"])) )
{
	$date_add=date("Y-m-d H:i:s");
	if( Session::getInstance()->getCustomer() ) 
	$source=str_replace(" ", "", $_POST["source"]);
	$cible=str_replace(" ", "", $_POST["cible"]);
	//var_dump(trim($source))."/".var_dump(trim($cible));
	$user_id=User::getInstance()->getId();
	$type=(!empty($_POST["type"]))?$_POST["type"]:301;

$query1="select * from redirection_items where url_source='$source' ";
$rs1 = & DBUtil::query( $query1 );
if( !$rs1->RecordCount() ){

	$query = " INSERT INTO `redirection_items`(`url_source`, `url_cible`, `statut`, `type`,`date_add`, `user_id`) VALUES('$source','$cible',1,$type,'$date_add',$user_id)";
	$rs = & DBUtil::query( $query );
}
}
}
	
if( isset( $_REQUEST[ "id_item" ] ) && isset( $_REQUEST[ "validate" ] ) ){

	$ret = DBUtil::query( "UPDATE redirection_items SET statut= '" . intval( $_REQUEST[ "validate" ] ) . "' WHERE id = '" . intval( $_REQUEST[ "id_item" ] ) . "' LIMIT 1" );
	if($ret)
	{
		$redirect=new Redirect();
		$redirect->generateHtacess();
	}
	exit( $ret === false ? "0" : "1" );

}
if( isset( $_REQUEST[ "id_item" ] ) && isset( $_REQUEST[ "delete" ] ) ){

	$ret = DBUtil::query( "DELETE FROM redirection_items WHERE id = '" . intval( $_REQUEST[ "id_item" ] ) . "' LIMIT 1" );
	if($ret)
	{
		$redirect=new Redirect();
		$redirect->generateHtacess();
	}

	exit( $ret === false ? "0" : "1" );

}

/*

if( isset( $_REQUEST[ "id_item" ] ) && isset( $_REQUEST[ "update" ] ) ){


if(!empty($_REQUEST["url_source"]) &&(!empty($_REQUEST["url_cible"])) )
{
	$type=(!empty($_REQUEST["type"]))?$_REQUEST["type"]:301;
	$source=str_replace(" ", "", $_REQUEST["url_source"]);
	$cible=str_replace(" ", "", $_REQUEST["url_cible"]);
	$ret = DBUtil::query( "UPDATE `redirection_items` SET `type`= " .intval( $type ). ",`url_source`='" . $source . "',`url_cible`='" . $cible . "' WHERE `id` = '" . intval( $_REQUEST[ "id_item" ] ) . "' LIMIT 1" );
	exit( $ret === false ? "0" : "1" );

}}*/
?>