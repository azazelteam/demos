<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire scoring client
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_scoring_buyer";
    $Str_TableName="scoring_buyer";
    $KeyName = 'idscoring_buyer';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idscoring_buyer";
	$ArrayFieldsColumns2Show[1]="textscoring_buyer";
	
// show the page 
	include "../../formbase/stdformbase.php";
		
?>