<?php 

/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire stdform gestion des groupements
 */

 //Paramètres de base 
	$Int_LimitP = 5;
	$pagetitle = "title_grouping";
    $Str_TableName="grouping";
  	$KeyName ='idgrouping';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idgrouping";
  	$ArrayFieldsColumns2Show[]="grouping";

 //Chemin pour les téléchargements relatif à $GLOBAL_START_PATH .'/www/'
 $Param_FileLocation = 'img_dir_supplier'; // recherche dans parmeter_admin
 
 //show the page 
 	

include ( "../../formbase/stdformbase.php" );
