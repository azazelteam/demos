<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire gestion catégories fournissseur appels d'offres
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_category_supplier";
    $Str_TableName="category_supplier";
    $KeyName = 'idcategory_supplier';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idcategory_supplier";
	$ArrayFieldsColumns2Show[1]="idcategory";
	$ArrayFieldsColumns2Show[2]="idsupplier";	

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>