<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage gestion commerciale
 */

include_once( "../../objects/classes.php" );
include_once( "../../catalog/drawlift.php" );

//-----------------------------------------------------------------------------

$TiTLTe = Dictionnary::translate( "parameter_admin" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------

$db =& DBUtil::getConnection();
$TableName = 'parameter_admin';

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']) )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	$db->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = $db->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

?>

<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
	            	
							<div class="subTitleContainer">
                <p class="subTitle"><?php  echo Dictionnary::translate('parameter_gest_com') ; ?></p>
				        	</div>		            	

		            	<table class="dataTable">
							<tr>
								<th class="filledCell" style="width:80%;"><?php  echo Dictionnary::translate("estimate_validity") ; ?> :</th>
								<td style="width:20%;">
									<input class="textInput" size=1 name="estimate_validity" value=<?php  echo $Parameter['estimate_validity'] ; ?>><br />
								</td>
							</tr><tr>
								<th class="filledCell"><?php  echo Dictionnary::translate("internal_delivery") ; ?> :</th>
								<td>
									<input class="textInput" size=1 name="internal_delivery" value=<?php  echo $Parameter['internal_delivery'] ; ?>><br />
								</td>
							</tr><tr>
								<th class="filledCell"><?php  echo Dictionnary::translate("internal_supplier") ; ?> :</th>
								<td>
									<input class="textInput" size=1 name="internal_supplier" value=<?php  echo $Parameter['internal_supplier'] ; ?>><br />
								</td>
							</tr>
						</table>
							

						
							<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    
</div>					

<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------

?>