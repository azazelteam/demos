<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire gestion des litiges types
 */
 
//Paramètres de base 
	$Int_LimitP = 5; 
	$pagetitle = "title_litigation";
	$Str_TableName="litigation";
	
// Field columns that appear after searching
  	$ArrayFieldsColumns2Show[0]="idlitigation";
  	$ArrayFieldsColumns2Show[1]="litigation_1";
	
//show the page 
include "../../formbase/stdformbase.php";
?>