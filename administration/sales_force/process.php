<?php

include( dirname( __FILE__ ) . "/../config/init.php" );

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Process Astelos</p>
                <div class="rightContainer">
                </div>
            </div>
           <!-- <div class="subContent">-->
<div style="margin-left:30px;margin-top:30px; font-size: 12px; color: black;">
<style type="text/css">

ul {
 font-family: Arial, sans-serif;
 font-size: 12px;
 color: black;
 background-position: 10px;
 padding-left: 0px;
 margin-left: 0px;
} 

li {
 font-family: Arial, sans-serif;
 font-size: 100%;
 color: black;
 background-position: 0px;
 padding-left: 0px;
 margin-left: 30px;
} 
</style>

<strong><p align="right">Date : 18 Février 2013</p></strong>

<br /><br />
<ul><strong>Fiche Fournisseurs :</strong> 
<li>SAV, Garantie, délai de livraison</li>
<li>Contacts et fonctions</li>
<li>Franco et frais de port</li>
<li>Produit de démo / conseil démo / quotation selon projet (montant / volume ?)</li>
</ul>
<br />

<ul><strong>Fiche clients : </strong> 
<li>Profils : hopitaux, EHPAD, MAS / IME…</li>
<li>Adresse => dater envoi catalogue dans suivi clients</li>
<li>Groupement => si oui rattacher sur existant (création via jcGay) + n° groupement</li>
<li>Nombre de lits</li>
</ul>
<br />

Facultatif : <br />
o	Commentaires clients : projet d’extension, achat chez concurrent / centrale (CAHPP, CACIC…) / référencement (Unadere, Apogées…)<br />
o	Scoring : « 7 – Ne pas recontacter »<br />
Si client en double, informer jfCorbé pour les fusionner 
<br /><br />

<ul><strong>Produits : </strong> 
<li>Enrichissement permanent / modifications simples faites par jcGay /Chef Produit suite remontées commerciaux</li>
<li>Création produit spécifique / option : par le commercial / jcGay avec fiche technique fournisseur sous 24-48h.</li>
<li>Création produit unique sur projet : suite devis en cours sous 2-3j.</li>
<li>Création produits / gamme : fiche post-it (catégorie + nom fournisseur + qté) selon planning chef produit</li>
<li>Fichier liste des produits / gammes créés sur le Z://Produit pour actualisation tarif / newsletter « Nouveauté » / Formation commerciaux</li>
<li>Si besoin produits sans catégorie (décision collégiale de la création d?une nouvelle catégorie)</li>
</ul>
<br />

<ul><strong>Devis : </strong> 
<li>Envoi systématique du catalogue aux nouveaux clients</li>
<li>Renseigner commentaires et relance devis après chaque appel</li>
<li>Si possible, renseigner scoring : </li>
o	Négociation en cours (+date) = planning défini<br />
o	Budget validé (+date) = ok verbal<br />
<li>Si devis avec plusieurs références similaires, mettre la quantité = 1 pour une autre référence (Quantité = 0 pour les autres)</li>
<li>Marge de manœuvre sur marge :</li>
o	Marge moyenne = 27% (25-30%)<br />
o	Si devis 5.000 - 10.000EUR => Marge min. 20-25%<br />
o	Si devis 10.000EUR => Marge min. 15-20%<br />
o	Hors cadre = ok selon cas : nouveau client / dossier stratégique  /démo…<br />
<br />

<ul><strong>Remboursement sécurité sociale : </strong>
<li>Suite à ok verbal du devis par le client, rassembler </li>
o	Ordonnance<br />
o	Attestation de sécurité sociale (à défaut valider prise en charge avec N° Sécu et date de naissance auprès CPAM – 0801 709 0CP)<br />
<li>Confirmation reste à charge par mail puis règlement par CB / chèque / virement</li>
<li>Validation commande puis envoi entente préalable</li>
<li>Télétransmission sous 15j. après envoi entente préalable</li>
<li>Rapprochement règlements suite au bilan menseul jfCorbé</li>
</ul>
<br />

<ul><strong>Produits démo : </strong> 
<li>Uniquement Astelos-Santé</li>
<li>Création d’un devis avec le produit complet (toutes options) - Prix produit = 0</li>
<li>Ajout article 18.0020.01 : port démo selon fournisseur / produit ? retour facturé ou non ? (intégrer frais de démo dans les coûts)</li>
<li>Validation commande suite à signature devis (avec ou sans port)</li>
</ul>
<br />

<ul><strong>Commande client :</strong> 
<li>Validation commande : affecter le devis signé du client à la commande – validation par jcGay</li>
<li>Vérifier adresse facturation</li>
<li>Indiquer N° cde client le cas échéant</li>
</ul>
<br />

<ul><strong>Création commande client : </strong> 
<li>Produits de remplacement</li>
<li>Ajout de services (transport, SAV, ...)</li>
<li>Produits de démo</li>
</ul>
<br />

<ul><strong>Suivi commandes fournisseurs : </strong> 
<li>Si commande > 5.000EUR => Télécharger confirmation commande fournisseurs</li>
<li>Relancer fournisseur si pas de confirmation dans les 48h</li>
<li>Suivi des dates d’expédition 2 fois par semaine (Girad Agédiss = pt hedbo)</li>
<li>Facturation les 15 et 30 du mois => vérifier date d’expédition</li>
</ul>
<br />

<ul><strong>Conditions annulation commande : </strong> 
(Pour les professionnels : aucune)
Pour les particuliers : 
<li>Sous 7 jours à réception de colis</li>
<li>Impossible pour les produits personnalisés</li>
<li>Produit dans l?emballage d?origine</li>
<li>Renvoyer à nos bureaux</li>
SAV / Retour produits, : 
<li>SAV : création suivi avec description litige  - demander photos au client (fonction archivage en cours de développement)</li>
<li>Retour produits : création suivi puis suite à réception retour colis => création litige</li>
</ul>
<br />

<ul><strong>Produits plus en stock après confirmation commande : </strong><br /> 
Proposer un autre produit : 
<li>Si même prix : créer une commande avec 0 TTC + annuler cde frs</li>
<li>Si prix supérieur : créer une commande avec différence de règlement à payer + annuler cde frs</li>
<li>Si prix inférieur : créer une commande avec 0 TTC et avoir du montant à rembourser + annuler cde frs</li>
Remboursement : créer un litige sur facture </li>
</ul>
<br />

<ul><strong>Gestion erreur expédition à partir du stock : </strong><br /> 
<li>Erreur dans la référence expédiée : créer une commande client avec un montant =0€ avec la référence expédiée par erreur (pour historique et déduction stock) </li>
<li>Erreur dans la quantité expédiée : outil Correcions</li>
</ul>
<br />


<ul><strong>Conditions règlements clients :</strong> 
<li>Particuliers : 100% à la commande (sauf montant pris en charge par la Cpam)</li>
<li>Hôpitaux et ehpad : 30 jours net à date de facture</li>
<li>Grand compte : 30 jours net à date de facture</li>
<li>Sociétés diverses : 100% à la commande</li>
<li>Indépendants médicaux (médecins, revendeurs…) : 100% à la commande</li>
<li>Sociétés (labos, pharmacie) : 100% à la commande voir acompte de 30% à la commande</li>
o	Si devis > 2.000EUR et problème avec conditions => exception via jcGay
</ul>
<br />

<ul><strong>Suivi client / CRM : </strong> 
<li>Enregistrer tout aléa (SAV, pour un client)</li>
<li>Créer un seul suivi par problématique (enregistrer l’historique dans le même formulaire avec la date de l’action)</li>
<li>Informer par mail le commercial pour les étapes importantes</li>
</ul>

		<div class="clear"></div>
            	
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

?>