<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */


//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "../../objects/adm_order.php" );
include_once( "../../catalog/drawlift.php" );

$Title = "Gestion des tarifications spécifiques";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" ); 

//-----------------------------------------------------------------------------


//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']) )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_cat SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_cat WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}


//Formulaire
?>
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Spécifications tarification</p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<form action="" method="post">
					<?php echo Dictionnary::translate( "multiprice_all_ref" ) ?> :</td>
					<?php echo DrawLiftParameters( "IdBuyerFamily", "Utiliser", "Ne pas utiliser" ); ?>
					<input type="submit" value="Modifier" name="Modify" class="blueButton" style="float:right;" />
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------

?>