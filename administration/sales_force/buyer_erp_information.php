<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */


include_once( "../../objects/classes.php" );

// paramètres de base 

$Int_LimitP 	= 5; 
$pagetitle 		= Dictionnary::translate( "buyer_erp_information " );
$Str_TableName	= "buyer_erp_information";
$KeyName 		= "idbuyer_erp_information";

// Field columns that appear after searching in ShowRecordsBy	

$lang = User::getInstance()->getLang();

$ArrayFieldsColumns2Show = array();
//$ArrayFieldsColumns2Show[] = "idbuyer_extra_information";
$ArrayFieldsColumns2Show[] = "information$lang";

// show the page 

include( "../../formbase/stdformbase.php" );

?>