<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire seuil de rentabilité
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_threshold_rentability";
    $Str_TableName="threshold_rentability";
    $KeyName = 'idthreshold_rentability';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idthreshold_rentability";
	$ArrayFieldsColumns2Show[1]="date_threshold";
	$ArrayFieldsColumns2Show[2]="amount";	

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>