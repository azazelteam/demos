<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire scoring devis
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_scoring_estimate";
    $Str_TableName="scoring_estimate";
    $KeyName = 'idscoring_estimate';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idscoring_estimate";
	$ArrayFieldsColumns2Show[1]="scoring_name";	

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>