<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Exportation des données au format CSV ou XML
 */

if (file_exists("../../objects/classes.php")) {
	include_once( "../../objects/classes.php" );
}else{
	include_once("../objects/classes.php");
}


include_once( "$GLOBAL_START_PATH/objects/adm_table.php" );

class ExportCSV extends ADMTABLE
{
/*
 * vars from admtable
	var $tablename;
	var $tabledesc = false;
	var $design = false;
	var $Array_DisplayFields = false;
	var $current_record = false ;
 */	
 var $infos = array(); // info column data

	/**
	 * ExportCSV()	Contructeur
	 * @param string $tblname Nom de la table
	 */ 
	function __construct($tblname)
	{
		parent::__construct($tblname);
	}

function ListAvailableFields()
{
	/*foreach($this->design as $k=>$v) {
		if($v['export_avalaible']!=0) $Array_FieldsAvailable[$k]=$v['fieldname'];
	}*/
	
	//tri par nom d'export
	
	$query = "
	SELECT tablename, export_name, export_avalaible FROM desc_table
	FROM desc_table
	WHERE export_avalaible = 1
	ORDER BY export_name ASC";
	
	$rs = DBUtil::getConnection()->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer la liste des tables exportables" );
	
	$Array_FieldsAvailable = array();	
	while( !$rs->EOF() ){
		
		$Array_FieldsAvailable[ $rs->fields( "export_name" ) ] = $rs->fields( "fieldname" );
		
		$rs->MoveNext();
			
	}
	
	 return $Array_FieldsAvailable;
}

function RenameField($Str_FieldName)
{
	$Str_NameTable = $this->tablename;
	$query = "
	SELECT export_name, export_avalaible 
	FROM desc_field 
	WHERE tablename='$Str_NameTable' 
	AND fieldname='$Str_FieldName'";
	
	$res = DBUtil::getConnection()->Execute( $query );
		$n=$res->RecordCount();
if ($n>0) {
                $Local_Availability=$res->fields["export_avalaible"];
                if ($Local_Availability<>"0") {
                      $Local_FieldName=$res->fields["export_name"];
                      if ($Local_FieldName<>"")  
                      	return $Local_FieldName;
                      die( $query );
                      trigger_error("RenameField ". Dictionnary::translate("imp-exp_mesg1"),E_USER_ERROR);
                      
                } else   return "-1"; //Field Not Available at Export
        }
        else {
            trigger_error("RenameField ($Str_FieldName)". Dictionnary::translate("imp-exp_mesg2"),E_USER_ERROR);
        }
}

function info_field($Str_FieldName,$fl) { //example : commercial:firstname:iduser:salesperson name
	$a = explode(':',$fl);
	if( count($a) != 4 ) { echo ' bad export_info_column ',$Str_FieldName,'/',$fl; return '';}
	$table = $a[0];
	$field = $a[1];
	$id = $a[2];
	$export_name = $a[3];
	$query="SELECT `$id` as `i`,`$field` as `f` FROM `$table`";// echo " $query ";
	$res = DBUtil::getConnection()->Execute($query);
	$this->infos[$Str_FieldName]=array();
	$n= $res->RecordCount();
	for($i=0;$i<$n;$i++)
		{
		$x = $res->fields['i'];
		$this->infos[$Str_FieldName][$x]=$res->fields['f'];
		$res->MoveNext();
		}
return $export_name;	
}

/**
 * Vérifie s'il faut exporter une colonne info
 * retorune le nom de la colonne si c'est le cas
 */
function InfoField($Str_FieldName)
{
	$result='';
	$Str_NameTable = $this->tablename;
	$res = DBUtil::getConnection()->Execute("SELECT export_info_column FROM desc_field " .
		"WHERE tablename='$Str_NameTable' AND fieldname='$Str_FieldName'");
		$n=$res->RecordCount();
if ($n>0) {
                $eic=$res->fields["export_info_column"];
                if(!empty($eic)) $result= $this->info_field($Str_FieldName,$eic);
        }
   return $result;
}

	/**
	 * Recherche la plus grande valeur de la clef primaire
	 */
	function LastKeyValue()
	{
	$result='';
	$res = DBUtil::getConnection()->Execute("SELECT  tablekeys FROM desc_table WHERE tablename='".$this->tablename."'");
	if ($res->RecordCount()>0)
		{
		//get records //Goto first table
		$keys = explode(",", $res->fields["tablekeys"] );
		foreach($keys as $k)
			{
			if( strncmp(trim($k),'id',2) == 0 ) 
				{
				 $res = DBUtil::getConnection()->Execute("SELECT max($k) as m FROM `$this->tablename`");
				 $result .= " $k = ". $res->fields["m"] ;
				}
			}
		}
	return $result;
	}


	/**
	 * Retourne le nom d'export de la table
	 */
	function RenameTable($tablename)
	{
	$res = DBUtil::getConnection()->Execute("SELECT export_name, export_avalaible FROM desc_table WHERE tablename='".$tablename."'");
	if ($res->RecordCount()>0)
		{
			$Local_Availability=$res->fields["export_avalaible"];
			if ($Local_Availability<>"0")
				{
					$Local_TableName=$res->fields["export_name"];
					if ($Local_TableName<>"")
						{
							return htmlentities($Local_TableName);
						}
						else
						{
						trigger_error("RenameTable ". Dictionnary::translate("imp-exp_mesg4"),E_USER_ERROR);
						}
				}
				else
				{
					return -1; //Table Not Available at Export
				}
		}
		else
		{
			trigger_error("RenameTable ". Dictionnary::translate("imp-exp_mesg5"),E_USER_ERROR);
		}
	}

	/**
	 * fonction pour obtenir les tables de qu'on peut exporter
	 * @return array liste des tables
	 */
	function ListAvailableTables()
	{
	$res = DBUtil::getConnection()->Execute("SELECT tablename, export_name FROM desc_table WHERE export_avalaible<>'0'  ORDER BY export_name ASC");
	if ($res->RecordCount()>0)
		{
			$Array_TablesAvailable=array();
	    	//get records //Goto first table
		    for ($i=0;$i<$res->RecordCount();$i++)
			    {
					$Array_TablesAvailable[$i]=$res->fields["tablename"];
			    	$res->MoveNext();
			    }
			return $Array_TablesAvailable;
		}
		else
		{
		    trigger_error("ListAvailableTables ". Dictionnary::translate("imp-exp_mesg6"),E_USER_ERROR);
		}
	}


	/**
	 * Crée les données du csv
	 * @return string Liste des champs
	 */
	function CSVPutFields($Str_FieldList, $Str_Condition, $firstline = false)
	{
		$Str_TableName = $this->tablename;
		//$query="SELECT $Str_FieldList FROM `$Str_TableName` $Str_Condition";
		//var_dump($query);
		if ($Str_Condition=="")	{
			$res = DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM `$Str_TableName`");
		}
		else {
			if ( $firstline )
				{
					$Str_BigResult=''; // main result
					$Str_SelFields=''; // selected fields
					//$Arr_Tmp = split(",",$Str_FieldList);
                    $Arr_Tmp = explode(",",$Str_FieldList);
					foreach ($Arr_Tmp as $v)
						{
							$v = str_replace('`','',ltrim($v));
							$Str_SelFields .=";". $this->RenameField($v);
							$x = $this->InfoField($v);
                            
                            $Str_Encodage = mb_detect_encoding($x);  
                             if( $Str_Encodage== 'UTF-8' ){                                        

                                $x =utf8_decode($x);                     

                            }                                 
							if(!empty($x)) $Str_SelFields .=";". $x;
						}
					$Str_BigResult .= substr($Str_SelFields, 1);
					$Str_BigResult .= "\r"."\n"; //new line begin
					//echo '<pre>'; print_r($this->infos); echo '</pre>';
					return $Str_BigResult;
				}
			$res = DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM `$Str_TableName` $Str_Condition");
		//echo"SQL SELECT $Str_FieldList FROM $Str_TableName $Str_Condition SQL<br />";
		}
	$Str_BigResult=''; // main result
	$Str_SelFields=''; // selected fields
	
    if ($res->RecordCount()>0)
		{
			//get records
		    for ($i=0;$i<$res->RecordCount();$i++)
			    {
					$Str_LineResult=''; //line result
					$Tmp_Fields = explode(",", $Str_FieldList); // list of Fields to result
					foreach($Tmp_Fields as $v )
					 	{
						$v = str_replace('`','',$v);
						$x = $res->fields[$v];
						$x = str_replace('"', "'", $x);
	                       
						/* date US -> FR */
						$regs = array();
						//if( preg_match( "/^([0-9]{4})-([0-9]{2})-([0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}\$/", $x, $regs )
							//|| preg_match( "^([0-9]{4})-([0-9]{2})-([0-9]{2})\$/", $x, $regs ) )
                        if( preg_match( "/^([0-9]{4})-([0-9]{2})-([0-9]{2}) [0-9]{2}:[0-9]{2}:[0-9]{2}\$/", $x, $regs )
							|| preg_match( "/^([0-9]{4})-([0-9]{2})-([0-9]{2})\$/", $x, $regs ) )
							$x = $regs[ 3 ] . "/" . $regs[ 2 ] . "/" . $regs[ 1 ];
	
						$Str_LineResult.=';"'.str_replace('\n','',str_replace('\r','',$x)).'"';
						// $Str_LineResult.=';"'.html_entity_decode(str_replace('\n','',str_replace('\r','',$x))).'"';
						
						if( isset($this->infos[$v]) ) {
	
							if( isset($this->infos[$v][$x]) ) 
                            //$Str_LineResult.=';"'.html_entity_decode(str_replace('\n','',str_replace('\r','',$this->infos[$v][$x]))).'"';
                            $Str_LineResult.=';"'.str_replace('\n','',str_replace('\r','',$this->infos[$v][$x])).'"';
							else $Str_LineResult.=';';
							//echo " $v $x "; if( isset($this->infos[$v][$x]) ) echo $this->infos[$v][$x]; else echo 'niet';
						}
						}
					$Str_LineResult=substr($Str_LineResult,1);
					$Str_BigResult.=$Str_LineResult."\r"."\n";//new line begin
			    $res->MoveNext();
			    }
			return $Str_BigResult;
		}
	}

	/**
	 * Export XML
	 */
	function XMLHeadPutFields($Str_FieldList)
	{
		$Str_TableName = $this->tablename;
		$Str_BigResult=''; // main result
		$Str_SelFields=''; // selected fields
		$Str_BigResult=''; // main result
		$Str_BigResult .= "<?php xml version=\"1.0\"?>\n";
		$Str_BigResult .= "<EXPORT_TABLE>\n";
		$Str_BigResult .= " <TABLE_NAME>";
		$Str_BigResult .= $Str_TableName ;
		$Str_BigResult .= "</TABLE_NAME>\n";
		$Str_SelFields=''; // selected fields
		$Arr_SelFields='';
		$Arr_SelFields = split(',' , $Str_FieldList);
		$Str_BigResult .= " <FIELD_NAMES>\n";
		for (; list($i, $ii)=each($Arr_SelFields);)
			{
				$ii = str_replace('`','',ltrim($ii));
				$Str_BigResult .= "  ";  //XML space
				$Str_BigResult .= "<";  //begin begin XML tag
				$Str_BigResult .= str_replace(' ','_',strtoupper($this->RenameField($ii)));
				$Str_BigResult .= ">";  //end begin XML tag
				$Str_BigResult .= $this->RenameField($ii);
				$Str_BigResult .= "</"; //begin end XML tag
				$Str_BigResult .= str_replace(' ','_',strtoupper($this->RenameField($ii)));
				$Str_BigResult .= ">\n";  //end end XML tag , new line
			}
		$Str_BigResult .= " </FIELD_NAMES>\n";
		$Str_BigResult .= " <FIELD_VALUES>\n";
		return $Str_BigResult;
	}
	
	/**
	 * fonction pour obtenir un string avec la fin des champs qu'on va ecrire dans le fichier exportee XML
	 */			
	function XMLEndPutFields()
	{
		$Str_BigResult=''; // main result
		$Str_BigResult .= " </FIELD_VALUES>\n";
		$Str_BigResult .= "</EXPORT_TABLE>\n";
		return $Str_BigResult;
	}
	
	
	function XMLPutFields( $Str_FieldList, $ii, $Str_Condition)
	{
		$Str_TableName = $this->tablename;
	if ($Str_Condition=="")
		{
			$res = DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM `$Str_TableName`");
		}
		else
		{
			$res = DBUtil::getConnection()->Execute("SELECT $Str_FieldList FROM `$Str_TableName` $Str_Condition");
		}
	$Str_BigResult=''; // main result
	if ($res->RecordCount()>0)
		{
			//get records
		    for ($i=0;$i<$res->RecordCount();$i++)
			    {
					$Str_LineResult=''; //line result
					$Tmp_Fields = split(",", $Str_FieldList); // list of Fields to result
					//$Str_BigResult .= "  <FIELD_VALUES_RECORD_NO_$ii>\n";
					$Str_BigResult .= "  <RECORD>\n";
					for (; list($k, $v) = each($Tmp_Fields); )
					 	{
						$v = str_replace('`','',ltrim($v));
						$Str_LineResult.= "   ";  //XML spaces
						$Str_LineResult.="<";  //begin begin XML tag
						$Str_LineResult.= str_replace(' ','_',strtoupper($v));
						$Str_LineResult.=">";  //end begin XML tag
						$Str_LineResult.=$res->fields[$v];
						$Str_LineResult.="</";  //begin end XML tag
						$Str_LineResult.= str_replace(' ','_',strtoupper($v));
						$Str_LineResult.=">\n";  //end end XML tag,  new line
						}
					$Str_BigResult.=$Str_LineResult;
					//$Str_BigResult .= "  </FIELD_VALUES_RECORD_NO_$ii>\n";
					$Str_BigResult .= "  </RECORD>\n";
			    $res->MoveNext();
			    }
			return $Str_BigResult;
		}
	}
	
	
	function GetFieldsSelected($k)
	{
		$Str_SelFields=''; // selected fields for query
		$n= count($k);
		for ($i=0; $i<$n; $i++)
			{
				$Str_SelFields.=',`'.substr($k[$i],1).'`';
			}
		$Str_SelFields=substr($Str_SelFields,1);
		return $Str_SelFields;
	}
	
	
	/**
	 * fonction pour obtenir le nom du fichier à exporter.
	 */
	function FileName($table_name, $Str_ExportFormat) //CSV ou XML
	{
	//	$FileName="FInteractExport_le_".date("_d_m_y_H_i_s").".". strtolower($Str_ExportFormat);
		$FileName=ucfirst($table_name) . date("_d_m_y") ."." . strtolower($Str_ExportFormat);
		return $FileName;
	}
	
	/**
	 * Ecriture d'une ligne dans un fichier csv
	 */
	function WriteLineToCSVFile( $i, $Str_FileName, $Str_SelFields)
	{
		$table_name = $this->tablename;
		$where = $this->getWhereCondition();
		
		$firstline = $i == -1;
        $Str_BigOutput = $this->CSVPutFields($Str_SelFields, "$where LIMIT $i, 1", $firstline );
           
       // $Arr_BigOutput =  explode(';',$Str_BigOutput);        
	   // echo "<br />Pentru i=$i linia este:".$Str_BigOutput;
		if  ($i ==-1 ) //fields line
			{
				if ( !$FileP = fopen("/tmp/$Str_FileName",'w') )
					{
	
						trigger_error("Répertoire /tmp inexistant ou droits d'écriture insuffisants",E_USER_ERROR);
					}
			}
				else //values lines
			{

				if ( !$FileP = fopen("/tmp/$Str_FileName",'a') )
					{
						trigger_error("WriteLineToCSVFile Cannot create file /tmp/$Str_FileName Line $i." .
								"<br /> Please verify write permissions on temp directory",E_USER_ERROR);
					}
			}
          
         //$Str_BigOutput = iconv("UTF-8", "Windows-1252//TRANSLIT", $Str_BigOutput);
         //$encodage = mb_detect_encoding($Str_BigOutput);
		 fputs($FileP, $Str_BigOutput);                   
		 fclose($FileP);
        
	}
	
	/**
	 * Ecriture sur disque
	 */
	function WriteHeadLinesToXMLFile($Str_FileName, $Str_SelFields)
	{
		$Str_BigOutput = $this->XMLHeadPutFields($Str_SelFields);
	//	echo "<br />Pentru i=$i linia este:".$Str_BigOutput;
		if ( !$FileP = fopen("/tmp/$Str_FileName",'w') )
			{
				trigger_error("WriteHeadLinesToXMLFile Cannot create file /tmp/$Str_FileName " .
						"<br /> Please verify write permissions on temp directory",E_USER_ERROR);
			}
		fputs($FileP, $Str_BigOutput);
		fclose($FileP);
		return $Str_FileName;
	}
	function WriteEndLinesToXMLFile( $Str_FileName, $Str_SelFields)
	{
		$Str_BigOutput = $this->XMLEndPutFields();
	//	echo "<br />Pentru i=$i linia este:".$Str_BigOutput;
		if ( !$FileP = fopen("/tmp/$Str_FileName",'a') )
			{
				trigger_error("WriteEndLinesToXMLFile Cannot create file /tmp/$Str_FileName " .
						"<br /> Please verify write permissions on temp directory",E_USER_ERROR);
			}
		fputs($FileP, $Str_BigOutput);
		fclose($FileP);
		return $Str_FileName;
	}
	
	
	function WriteLineToXMLFile( $i,  $Str_FileName, $Str_SelFields)
	{
		$Str_BigOutput = $this->XMLPutFields($Str_SelFields, $i, " LIMIT $i, 1");
	//	echo "<br />Pentru i=$i linia este:".$Str_BigOutput;
		if ( !$FileP = fopen("/tmp/$Str_FileName",'a') )
			{
				trigger_error("trigger_error Cannot create file /tmp/$Str_FileName Line $i." .
								"<br /> Please verify write permissions on temp directory",E_USER_ERROR);
			}
		fputs($FileP, $Str_BigOutput);
		fclose($FileP);
		return $Str_FileName;
	}

	
	/**
	 * Crée un lien vers le fichier csv ou xml
	 */
	function LinkToFile($Str_FileName)
	{
		echo "<center><a class=\"normalLink\" href=\"/administration/import-export/getfile.php?file=/tmp/$Str_FileName\" class=\"lien\">"; //do not open a new window
		echo Dictionnary::translate("imp-exp_file");
	  	echo "</a></center>";
	}
	
	/**
	 * Début de formulaire
	 */
	function BeginPostForm($Str_ActionName)
	{
		echo "<form name=export action=".$Str_ActionName." method=post>\n";
	}
	/**
	 * Fin du formulaire
	 */
	function EndForm()
	{
		echo "</form>\n";
	}
	
	function InputButton($Str_ButtonType, $Str_ButtonName, $Str_ButtonValue, $Str_ButtonAdd = "")
	{
		echo "<input type=\"$Str_ButtonType\"  name=\"$Str_ButtonName\" value=\"$Str_ButtonValue\" class=\"blueButton\"$Str_ButtonAdd />\n";
	}
	
	/**
	 * Crée les boutons radio de choix du format d'export
	 */
	function Format()
	{
		echo "\t\t".'<tr>'."\n";
		echo "\t\t\t".'<th class="filledCell">'. Dictionnary::translate("imp-exp_select_format")."</th>\n";
		echo "\t\t\t".'<td style="text-align:center;">';
		$this->InputButton('radio','export_format','CSV',' checked="checked"');
		echo "&nbsp;CSV</td>\n";
		echo "\t\t\t".'<td style="text-align:center;">';
		$this->InputButton('radio','export_format','XML');
		echo "&nbsp;XML</td>\n";
		echo "\t\t".'</tr>'."\n";
	}
	
	/**
	 * fonction pour generer la selection pour la table selectee pour l'export.
	 * On affiche la traduction de la table, mais on utilise le nom
	 */
	function MetaTable()
	{
?>
		<tr>
			<th class="filledCell"><?php echo Dictionnary::translate('imp-exp_select_table') ?></th>
			<td colspan="2">
				<select name="table_name">
<?php
		$Array_Tables = $this->ListAvailableTables();
		$n = count($Array_Tables);
		for ($i=0;$i<$n;$i++)
			echo "\t\t\t\t\t<option value=\"".$Array_Tables[$i]."\">".$this->RenameTable($Array_Tables[$i])."</option>\n";
?>
				</select>
			</td>
		</tr>
<?php
	}
	
	function Center()
	{
	  	echo "<center>\n";
	}
	

	/**
	 * Affichage de la liste des champs avec case à cocher
	 */
	function FieldsTable()
	{
		?>
		<div class="tableContainer" style="width:300px; padding-top:14px; margin:auto;">
		<?php
		echo '<table class="dataTable">';
		echo '	<tr>';
		echo '		<th class="filledCell" colspan="2">'.Dictionnary::translate("import_select_table").'</th>';
		echo '	</tr>';
		foreach($this->design as $k=>$v) {
		if($v['export_avalaible']!=0) {
			if($v['mandatory']) $x='0'; else $x='_';
			echo '	<tr>';
			echo '		<td width="20px"><input type="checkbox" name="k[]" value="'.$x.$v['fieldname'].'" checked="checked" ';
			if($v['mandatory']) echo 'Onclick="this.checked=1" style="border: 1px solid rgb(255, 0, 0)"';
			echo '></td>';
			if($v['mandatory'])echo '		<td><i>'.$v['export_name'].'</i></td>';
			else echo '		<td>'.$v['export_name'].'</td>';
			echo '	</tr>';
		}
		}
		echo '	</table>';
		?>
		</div>
		<?php
	}
	
	/**
	 * pour obtenir le nombre des enregistrements a exporter
	 */
	function NumRecords()
	{
		
		$where = $this->getWhereCondition();
		$query = "SELECT COUNT(*) as num FROM `".$this->tablename . "`";
		
		if( strlen( $where ) )
			$query .= " $where";
			
		$res = DBUtil::getConnection()->Execute( $query );
		
		if ( $res->RecordCount() )
			return $res->fields['num'];
			
	}
	
	
	/**
	 * fonction pour selectioner certains enregistrements de la table pour l'effacement
	 * @todo à revoir complètement

	function SelectIdTable()
	{
		$Array_Fields = $Export->ListAvailableFields();
	//	echo "<table  border=0 bgcolor=#009999 cellpadding=1 cellspacing=1>\n";
		echo "\n <table border=0  bgcolor=\"#FFFFFF\" width=\"$GLOBAL_Table_Width\" cellpadding=\"1\"  cellspacing=\"1\">";
		echo "<tr><td colspan=2>";
		echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
		echo Dictionnary::translate("export_del_txt1").":</b></font>";
		echo "</td></tr>";
		echo "<tr><td bgcolor='$Result_BackGroundColor'>\n";
		echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
		echo Dictionnary::translate("export_del_txt2").": <SELECT name=\"del_field_select\">\n";
		for ($i=0;$i<sizeof($Array_Fields);$i++)
			{
				echo "\n <OPTION VALUE=\"". $Array_Fields[$i] ."\">". $Array_Fields[$i];
			}
		echo "</SELECT>\n";
		echo "\n <td bgcolor='$Result_BackGroundColor'>\n";
		echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
		echo Dictionnary::translate("export_del_txt3").": ";
		echo "<INPUT TYPE=text name=\"field_min\" size=5>";
		echo " ".Dictionnary::translate("export_del_txt4").": ";
		echo "<INPUT TYPE=text name=\"field_max\" size=5>";
		echo "</td></tr>\n";
		echo "\n <td bgcolor='$Result_BackGroundColor' colspan=2>\n";
		echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
		echo Dictionnary::translate("export_del_txt5").": ";
		echo "<INPUT TYPE=text name=\"field_idx\" size=8>";
		echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
		echo Dictionnary::translate("export_del_txt6")." ";
		echo "</td></tr>\n";
		echo "</table>\n";
	}
		 */
	/**
	 * pour effacer certains enregistrements de la table selectee et pour les visualiser
	 * @todo à revoir complètement

	function ShowAndDeleteFromTable( $del_field_select, $field_min, $field_max, $field_idx)
	{
	$table_name = $this->tablename;
		if ( ($field_min != "") && ($field_max != "") )
			{
	//			echo "<table  border=0 bgcolor=#009999 cellpadding=1 cellspacing=1>\n";
		echo "\n <table border=0  bgcolor=\"#FFFFFF\" width=\"$GLOBAL_Table_Width\" cellpadding=\"1\"  cellspacing=\"1\">";
				echo "<tr><td colspan=2>\n";
		echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
				echo Dictionnary::translate("export_del_txt7")."</b></font>\n";
				echo "</td></tr>\n";
				echo "<tr><td>";
				echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
				echo "Champ <b>$del_field_select \n";
				echo "</td></tr>\n";
				$res = DBUtil::getConnection()->Execute(" SELECT  $del_field_select FROM $table_name WHERE $del_field_select>$field_min AND $del_field_select<$field_max ");
				if ($res->RecordCount()>0)
					{
						//get records
					    for ($i=0;$i<$res->RecordCount();$i++)
						    {
								echo " <tr><td bgcolor='$Result_BackGroundColor'><font color=black face=arial size=-1>".Dictionnary::translate("export_del_txt8").": \n";
								echo $res->fields[$del_field_select];
								echo " </td></tr>\n";
								$res->MoveNext();
							}
					}
				echo "</td></tr>\n";
				echo "</table>\n";
				$res = DBUtil::getConnection()->Execute("DELETE FROM $table_name WHERE $del_field_select>$field_min AND $del_field_select<$field_max");
				if ( !strcmp($table_name ,"detail") )
					{
						$res = DBUtil::getConnection()->Execute("DELETE FROM realproduct WHERE $del_field_select>$field_min AND $del_field_select<$field_max");
					}
			}
				else
			{
	// build SQL Query
				$Str_Condition='';
				$Arr_Tmp = split (",", $field_idx);
				while ( list($k, $v) = each ($Arr_Tmp) )
					{
						$Str_Condition .= " OR ". $del_field_select . "='" . ltrim($v)."'";
					}
				$Str_Condition = " WHERE ". substr($Str_Condition, 4);
	//			echo "<table  border=0 bgcolor=#009999 cellpadding=1 cellspacing=1>\n";
		echo "\n <table border=0  bgcolor=\"#FFFFFF\" width=\"$GLOBAL_Table_Width\" cellpadding=\"1\"  cellspacing=\"1\">";
				echo "<tr><td colspan=2>\n";
				echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
				echo Dictionnary::translate("export_del_txt7")."</b></font>\n";
				echo "</td></tr>\n";
				echo "<tr><td>";
				echo "\n     <font face=\"$GLOBAL_Result_Face\" size=\"$GLOBAL_Result_Size\" color=\"$GLOBAL_Result_Color\">$GLOBAL_Result_Text\n";
				echo Dictionnary::translate("export_del_txt2")." <b>$del_field_select \n";
				echo "</td></tr>\n";
				$res = DBUtil::getConnection()->Execute(" SELECT  $del_field_select FROM $table_name ". $Str_Condition);
				if ($res->RecordCount()>0)
					{
						//get records
					    for ($i=0;$i<$res->RecordCount();$i++)
						    {
								echo " <tr><td bgcolor='$Result_BackGroundColor'><font color=black face=arial size=-1>".Dictionnary::translate("export_del_txt8").": \n";
								echo $res->fields[$del_field_select];
								echo " </td></tr>\n";
								$res->MoveNext();
							}
					}
						else
					{
						echo " <tr><td bgcolor='$Result_BackGroundColor'><font color=black face=arial size=-1>".Dictionnary::translate("export_del_txt8")." : \n";
						echo "".Dictionnary::translate("export_del_txt9")."</font>\n";
						echo " </td></tr>\n";
					}
				echo "</td></tr>\n";
				echo "</table>\n";
				$res = DBUtil::getConnection()->Execute("DELETE FROM $table_name ". $Str_Condition);
				if ( !strcmp($table_name ,"detail") )
					{
						$res = DBUtil::getConnection()->Execute("DELETE FROM realproduct ". $Str_Condition);
					}
			}
	}
		 */
	//-----------------------------------------------------------------------------------------
	
	function getWhereCondition(){
		
		$where = "";
		
		//critères d'exportation
		
		if( isset( $_POST[ "Where" ] ) && !empty( $_POST[ "Where" ] ) )
			$where = stripslashes( $_POST[ "Where" ] );
	
		//critères de tri
		
		if( isset( $_POST[ "OrderBy" ] ) && !empty( $_POST[ "OrderBy" ] ) )
			$where .= " ORDER BY " . stripslashes( $_POST[ "OrderBy" ] );
			
		if( strlen( $where ) )
			return "WHERE $where";
		else return "WHERE 1";
			
	}

	//-----------------------------------------------------------------------------------------
	
}//EOC
?>