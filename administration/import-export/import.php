<?php 

 /**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Importation des données au format CSV
 */

ini_set('display_errors','On');
$DEBUG = 'code';

/**
 * Initialisation de la session
 * Initialisation des variables globales
 * Fonctions générales
 * Appels des bibliothèques communes
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/adm_base.php");
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/sessionobject.php");
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_CODE_PATH/secure.inc.php");
 */

//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

$Title = "Import données";
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" ); 

//-----------------------------------------------------------------------------


showDebug(ini_get('display_errors'),'import.php => display_errors',$DEBUG);
include_once('template_import.php');
include_once($GLOBAL_START_PATH.'/product_management/import-export/import_realproduct.php');
	
// action:
// 0 = insertion
// 1 = modifications dans la table
// 2 = suppresion


?>

	<!-- Traitements spécifiques à la page -->
	<script type="text/javascript">
	<!--
		function check_num(act) {
			//if (document.forms[0].
			document.forms[0].action.value=act;
			document.forms[0].submit();
		}
		function help () {
		<?php 
			$height = 500;
			$width = 700;
			$toolbar = 0;
			$location = 0;
			$directories = 0;
			$status = 0;
			$menubar = 0;
			$scrollbars = 1;
			$resizable = 1;
			echo "window.open('../html_fr/import.htm','hw','width=$width,height=$height,toolbar=$toolbar,location=$location,directories=$directories,status=$status,menubar=$menubar,scrollbars=$scrollbars,resizable=$resizable')";
		?>
		}
	// -->
	</script>
	
	
<?php 
/////////////////////
//  Table selected, 
//  Doing import
/////////////////////
if( isset($_FILES['userfile']['tmp_name']) ) { 
    showDebug($_FILES,'import.php => $_FILES',$DEBUG);
	$userfile = $_FILES['userfile']['tmp_name'];
	$userfile_name = $_FILES['userfile']['name'];
	$type = $_FILES['userfile']['type'];
	$size = $_FILES['userfile']['size'];
	$Arr_filename = explode('.',$userfile_name);
	$ext = array_pop($Arr_filename);
	if (empty($size)) {
		?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">						
<?php
		showDebug($_FILES,"Taille de fichier trop élevée(> ".ini_get('upload_max_filesize').')','user_error');
?>
		</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>		
<?php
	
	} elseif ($ext != 'csv' && $type != 'text/x-comma-separated-values') {
?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">	
					Format de fichier <?php echo $ext; ?> invalide
<?php
	//	showDebug($type,"Format de fichier '$ext' invalide",'user_error');
?>
		</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>		
<?php	

	} elseif ( isset ($_POST['table_name']) && isset ($userfile) ) {
		$table_name = $_POST['table_name'];
		$fieldsep = $_POST['fieldsep'];
		showDebug($table_name,'import.php => $table_name',$DEBUG);
		if ( $table_name == 'real_product' ) { die('not available.');
			$ImportProductCSV = new ImportProductCSV($fieldsep);
			showDebug($userfile,'import.php => $userfile',$DEBUG);
			if (!copy($userfile,"/tmp/$userfile_name"))
			/*
?>
<div class="mainContent" style="width:700px; margin-left:30px;">
	<div id="globalMainContent">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">					
<?php		*/	
				showDebug($userfile,"Erreur chargement de fichier dans /tmp/$userfile_name",'user_error');
/* ?>
		</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>		
<?php	*/		
			else {
				$time_1 = strtotime(date('Y-m-d H:i:s'));
				$resImport = $ImportProductCSV->importCSVData("/tmp/$userfile_name");
				$time_2 = strtotime(date('Y-m-d H:i:s'));
				$time_elapsed = $time_2 - $time_1;
				echo "<br />Temps : $time_elapsed secondes<br /><br />";
				if ( !empty($resImport) ) {
					$ImportProductCSV->showResult($resImport);
					echo '<br />';
					$ImportProductCSV->showErrors();
					echo '<br />';
				}
			}
			
				
		} else {
			
			$importer = new DATA_IMPORT($table_name);
			if (!copy($userfile,"$GLOBAL_START_PATH/tmp/".$userfile_name) ){
				
				?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">				
	<div class="mainContent" style="width:700px; margin-left:30px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">Impossible de t&eacute;l&eacute;charger le fichier</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>
				<?php 
			
			}
			
			if( $importer->ReadVerifyInsert("$GLOBAL_START_PATH/tmp/".$userfile_name) ){
				
				/* ?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">fdgdfg
					<p class="msg"><?php echo Dictionnary::translate("imp-exp_good_import") ?></p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>
				<?php */
				
				//$importer->HRLine(80);
			
			}
			else{
				
				?>
					<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
				        <div class="topRight"></div><div class="topLeft"></div>
				        <div class="content">
							<p class="msg"><?php echo Dictionnary::translate("imp-exp_msg13") ?></p>
							<div class="clear"></div>
				        </div>
					 	<div class="bottomRight"></div><div class="bottomLeft"></div>
				    </div>
				<?php 
			
			}
			
			unlink("$GLOBAL_START_PATH/tmp/".$userfile_name);
			
		}
	}
}
/////////////////////
//  Table not selected, 
//  Doing selection of tables
/////////////////////
?>

<div id="globalMainContent">
	<div class="mainContent" style="width:700px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
        
            	<!-- formulaire -->



<?php 
//echo "\t".'<h1>'.Dictionnary::translate('import_title');
//echo "\t\t".'<a href="javascript: help();"><img src="../images/back_office/content/help.gif" border="0" /></a></h1>'."\n";

// list available tables and select one of them
$importer = new DATA_IMPORT('desc_table');
// get array of export_available <>0 tables from desc_field.
$Array_Tables= $importer->ListAvailableTables();
$importer->BeginPostFileForm($ScriptName);
//selection of table
$importer->MetaTable($Array_Tables);
//selection of file to upload
$importer->ImportSeq(); 
$importer->EndForm(); 
?>

					
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>
<?php 
//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------

?>
