<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Exportation des données au format CSV ou XML
 */

date_default_timezone_set("Europe/Berlin");
ini_set('display_errors','On');
$DEBUG = 'code';

/**
 * Initialisation de la session
 * Initialisation des variables globales
 * Fonctions générales
 * Appels des bibliothèques communes
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/adm_base.php");
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/sessionobject.php");
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_CODE_PATH/secure.inc.php");
 */
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

$Title = "Export";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" ); 

//-----------------------------------------------------------------------------

showDebug(ini_get('display_errors'),'import.php => display_errors',$DEBUG);
include_once('template_export.php');

?>
<div id="globalMainContent">
	<!-- Traitements spécifiques à la page -->
	<script type="text/javascript">
	<!--
		function check_num(act) {
			document.forms[0].action.value=act;
			document.forms[0].submit();
		}
		
		function help () {
			window.open('../html_fr/export.htm','hw','width=700,height=500,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1')
		}
	
	function CheckAll(c) {
		nb=document.forms[0].elements.length;
		if( c == 2 ) {
			for (i=0; i < nb; i++) 
			{
			if(document.forms[0].elements[i].value.indexOf("_") == 0) {
					if(document.forms[0].elements[i].checked) c=0; else c=1;
					document.forms[0].elements[i].checked = c;
				}
			}
			return 
		}
	    for (i=0; i < nb; i++) 
		{
		if(document.forms[0].elements[i].value.indexOf("_") == 0) 
			document.forms[0].elements[i].checked = c;
		}
	}	
		
		
	// -->
	</script>
	<!--
<?php

/////////////////////
//  Table Selected, Fields selected, 
//  Doing Export, link to new window with exported file
/*if ( ( isset($_POST['select_fields_done'] )|| isset($_POST['select_fields_done_x'])  )
	&&  isset($_POST['k'])  
	&&  isset($_POST['table_name'])  
	&&  isset($_POST['export_format']) )
	{
		$table_name = $_POST['table_name'];
		$Export = new ExportCSV($table_name); 
	
		// do export and link to it
		$Int_NumFields = $Export->NumRecords();
		//echo "<br />Int_NumFields=$Int_NumFields";exit;
		if(isset($no_chop_lines))
		if ($no_chop_lines >0 && $no_chop_lines <$Int_NumFields)
			{
				//numéro des enregistrements à exporter			
				$Int_NumFields = $no_chop_lines;
			}
		$Str_SelF = $Export->GetFieldsSelected($_POST['k']);
		 // Generate and Store Export File Name in $Str_FileName
		$Str_FileName = $Export->FileName($_POST['table_name'],$_POST['export_format']);

		if ($_POST['export_format']=="CSV")
			{
				for ($i=-1; $i < $Int_NumFields; $i++) // for -1 write field names
					{
						$Export->WriteLineToCSVFile($i, $Str_FileName, $Str_SelF); 
					}
			}

		if ($_POST['export_format']=="XML")
			{
				$Export->WriteHeadLinesToXMLFile( $Str_FileName, $Str_SelF); // table name, field names
				for ($i=0; $i < $Int_NumFields; $i++)
					{
						$Export->WriteLineToXMLFile( $i, $Str_FileName, $Str_SelF);  // field values
					}
				$Export->WriteEndLinesToXMLFile( $Str_FileName, $Str_SelF);
			}
			?>
		
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo Dictionnary::translate('imp-exp_good_export') ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
						
			<?
		echo "\t".'<br /><span class="msg">'.$Int_NumFields.'&nbsp;'.Dictionnary::translate('imp-exp_ligne')."</span><br />\n";
		$Export->LinkToFile($Str_FileName);
			?>
		<div class="clear"></div>
           	</div>
   		</div>
 <div class="bottomRight"></div><div class="bottomLeft"></div>  	
	</div>
</div>

		<?	
	}*/
	?>	
-->
<?php 
 
/////////////////////
//  Table not selected, 
//  Doing selection of tables

if (!isset($_POST['select_table_done']) && !isset($_POST['select_table_done_x']))
	{
	
	//list available tables and select one of them
	
	$Export = new ExportCSV('desc_table');
	//$Array_Tables=$Export->ListAvailableTables();// print_r($Array_Tables);
	$Export->BeginPostForm($ScriptName);
	
?>
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer"></div>
            </div>
            <div class="subContent">
				<div class="tableContainer" style="width:400px; padding-top:14px; margin:auto;">
					<table class="dataTable">
<?php
					
					$Export->MetaTable();
					$Export->Format(); //CSV ou XML
					
?>
					</table>
				</div>
				<p style="float:right;">
					<input type="submit" class="blueButton" name="select_table_done" value="Exporter" style="margin-top:7px;" /> 
				</p>
			</div>
			<div class="clear"></div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    </form>
<?php
		
	}


////////////////////////////////////
// Resultat pour l'import général
//  Table Selected, Fields selected, 
//  Doing Export, link to new window with exported file

if ( ( isset($_POST['select_fields_done'] )|| isset($_POST['select_fields_done_x'])  )
	&&  isset($_POST['k'])  
	&&  isset($_POST['table_name'])  
	&&  isset($_POST['export_format']) )
	{
		$table_name = $_POST['table_name'];

		$Export = new ExportCSV($table_name); 
	
		// do export and link to it
		$Int_NumFields = $Export->NumRecords();

//		echo "<br />Int_NumFields=$Int_NumFields";
		if(isset($no_chop_lines))
		if ($no_chop_lines >0 && $no_chop_lines <$Int_NumFields)
			{
				//numéro des enregistrements à exporter			
				$Int_NumFields = $no_chop_lines;
			}

		$Str_SelF = $Export->GetFieldsSelected($_POST['k']);
		 // Generate and Store Export File Name in $Str_FileName
		$Str_FileName = $Export->FileName($_POST['table_name'],$_POST['export_format']);

		if ($_POST['export_format']=="CSV")
			{
                $fichier_csv = fopen($Str_FileName, 'w+');
                fprintf($fichier_csv, chr(0xEF).chr(0xBB).chr(0xBF));
				for ($i=-1; $i <$Int_NumFields; $i++) // for -1 write field names
					{	 
						$Export->WriteLineToCSVFile($i, $Str_FileName, $Str_SelF); 
					

					}

			}     

		if ($_POST['export_format']=="XML")
			{
				$Export->WriteHeadLinesToXMLFile( $Str_FileName, $Str_SelF); // table name, field names
				for ($i=0; $i < $Int_NumFields; $i++)
					{
						$Export->WriteLineToXMLFile( $i, $Str_FileName, $Str_SelF);  // field values
					}
				$Export->WriteEndLinesToXMLFile( $Str_FileName, $Str_SelF);
			}
?>
	<div class="mainContent" style="width:700px; margin-left:30px;margin-top:20px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo Dictionnary::translate('imp-exp_good_export') ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<div class="msg"><?php echo $Int_NumFields ?>&nbsp;<?php echo Dictionnary::translate('imp-exp_ligne') ?></div>
				<?php $Export->LinkToFile($Str_FileName); ?>
				<div class="clear"></div>
           	</div>
   		</div>
 		<div class="bottomRight"></div><div class="bottomLeft"></div>  	
	</div>
<?php
		
		$Export->EndForm();
		
	}


/////////////////////
//  Sélection de la table 
//  Doing selection of fields
if ( ( isset($_POST['select_table_done']) || isset($_POST['select_table_done_x']) ) && isset($_POST['table_name']) 
	&&  isset($_POST['export_format']) )
	{
		$Export = new ExportCSV($_POST['table_name']);
		?>
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
	        	<div class="subTitleContainer">
          			<p class="subTitle">Table  : <?php echo $_POST[ "table_name" ] ?></p>
				</div>
            	<div class="tableContainer" style="text-align:center;">
<?php
		
		echo "<br /><span class=\"msg\">";
		echo ' '.Dictionnary::translate('export_info');
		echo "</span><br />";
		$ilastid = $Export->LastKeyValue();
		echo '<br /><br />'."\n";
		if( $ilastid != '' ) echo ' '.Dictionnary::translate('last_id').":<b> $ilastid</b><br /><br />\n";
		//$Array_Fields=$Export->ListAvailableFields();
		$Export->BeginPostForm($ScriptName);
		$Export->FieldsTable();
		if(isset($no_chop_lines))
			$Export->InputButton("hidden", "no_chop_lines" , $no_chop_lines);
		$Export->InputButton("hidden", "table_name" , $_POST['table_name']);
		$Export->InputButton("hidden", "export_format" , $_POST['export_format'] );
		
		echo '<input class="blueButton" style="margin-top:10px;" type="button" name="tous" value="Tout sélectionner"  onClick="CheckAll(1);" /> ';
		echo '<input class="blueButton" style="margin-top:10px;" type="button" name="tous" value="Tout désélectionner"  onClick="CheckAll(0);" /> ';
		echo '<input class="blueButton" style="margin-top:10px;" type="button" name="tous" value="Inverser les sélections"  onClick="CheckAll(2);" /><br />';
		
		displayWhereForm();
		displayOrderByForm();
		
?>
						<input type="submit" class="blueButton" name="select_fields_done" value="Valider" style="float:right; margin-top:7px;" />
		        	</form>
				</div>
				<div class="clear"></div>
    	   	</div>
		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
<?php
		
		$Export->EndForm();  
	}

/*
· sélection dans un ascenceur de la table (données dans table_desc) 
· champ de saisie des id
(idx  idy) : suppression de tous les id entre idx et id 
(idx, idy, idz) : suppression des idx, idy et idz uniquement 
· affichage de toutes les données avant suppression 
*/
//echo "<hr noshade width=\"80%\">\n";
?>

<!--
<?php
		echo Dictionnary::translate('export_del_title');

// delete
if ( isset($select_fat_fields_done) )
	{
		$Export = new ExportCSV($_POST['table_name']);
		$Export->ShowAndDeleteFromTable($del_field_select, $field_min, $field_max, $field_idx);
	}

// select records to delete			
if ( isset($select_fat_table_done) )
	{
		$Export = new ExportCSV($_POST['table_name']);
		$Export->BeginPostForm($ScriptName);
		//$Array_Fields=$Export->ListAvailableFields($con, $table_name);
		$Export->SelectIdTable();
		$Export->InputButton("submit", "select_fat_fields_done" ,"Effacer");
		$Export->InputButton("hidden", "table_name" ,$_POST['table_name']);
		$Export->EndForm();  
	}

// select table to delete records from	
		$Export->BeginPostForm($ScriptName);
		echo Dictionnary::translate("export_del_info")." :\n";
		$Export->MetaTable();
		
?>
-->
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------
	
function displayWhereForm(){
 
 	global  			$GLOBAL_START_URL;
 	
 	//récupérer les champs exportables
 					
	$query = "
	SELECT fieldname, export_name, type, description, size, help
	FROM desc_field
	WHERE tablename LIKE '" . $_POST[ "table_name" ] . "'
	AND export_avalaible = 1
	AND help NOT IN ( 'upload', 'password', 'cpopup' )
	ORDER BY export_name ASC";
	
	$rs = DBUtil::getConnection()->Execute( $query );
	if( $rs === false )
		die( "Impossible de récupérer la liste des champs exportables" );
	
	$fields = array();
	while( !$rs->EOF() ){
		
		$fieldname 		= $rs->fields( "fieldname" );
		
		$maxsize = 25;
		$fields[ $fieldname ] = array(
		
			"export_name" 	=> $rs->fields( "export_name" ),
			"description" 	=> $rs->fields( "description" ),
			"size"			=> $rs->fields( "size" ) > $maxsize ? $maxsize : $rs->fields( "size" ),
			"type"			=> $rs->fields( "type" ),
			"help"			=> $rs->fields( "type" )
			
		);
		
		$rs->MoveNext();
		
	}
	
	?>
	<script type="text/javascript" language="javascript">
	<!--
	
		var fields = new Array();
		
		<?php
		
			foreach( $fields as $fieldname => $fieldinfos ){
			
				echo "\nfields['" . htmlentities( addslashes( $fieldname ) ) . "'] = new Array();";
					
				foreach( $fieldinfos as $key => $value )
					echo "\nfields['" . htmlentities( addslashes( $fieldname ) ) . "']['" .  htmlentities( addslashes( $key ) ) . "'] = '" . htmlentities( addslashes( $value ) ) . "';";
				
			}
			
		?>
		
		fields[ 'username' ] = new Array();
		fields[ 'username' ][ 'export_name' ] = 'Auteur de la derni&egrave; modification';
		fields[ 'username' ][ 'description' ] = 'Auteur de la derni&egrave; modification';
		fields[ 'username' ][ 'size' ] = 25;
		fields[ 'username' ][ 'type' ] = 'user';
		fields[ 'username' ][ 'help' ] = 'valid_user';
		
		fields[ 'lastupdate' ] = new Array();
		fields[ 'lastupdate' ][ 'export_name' ] = 'Date de la derni&egrave; modification';
		fields[ 'lastupdate' ][ 'description' ] = 'Date de la derni&egrave; modification';
		fields[ 'lastupdate' ][ 'size' ] = 25;
		fields[ 'lastupdate' ][ 'type' ] = 'datetime';
		fields[ 'lastupdate' ][ 'help' ] = 'date';
		
		function updateRuleForm(){
		
			var fieldList = document.getElementById( 'FieldList' );
			var selectedField = fieldList.options[ fieldList.selectedIndex ].value;
			
			var type = fields[ selectedField ][ 'type' ];
			var operatorLists = new Array( 'NumericOperators', 'TextOperators', 'DateOperators', 'UserOperators' );
			
			var operatorList = getFieldOperatorListID( selectedField );
			
			var i =0;
			while( i < operatorLists.length ){
			
				var list = operatorLists[ i ];
				if( list == operatorList )
					document.getElementById( list ).style.display = 'block';
				else document.getElementById( list ).style.display = 'none';
				
				document.getElementById( list ).selectedIndex = 0;
				
				i++;
			
			}
			
			displayFieldTools();
			
			var size = fields[ selectedField ][ 'size' ];
			document.getElementById( 'ValueEdit' ).size = size;
			document.getElementById( 'ValueEdit' ).maxlength = size;
			document.getElementById( 'ValueEdit' ).value = '';
			
		}
		
		function createExportRule(){
		
			var fieldList = document.getElementById( 'FieldList' );
			var selectedField = fieldList.options[ fieldList.selectedIndex ].value;
			var selectedFieldStr = fieldList.options[ fieldList.selectedIndex ].innerHTML;
			if( selectedFieldStr == '' ){
			
				alert( 'Vous sélectionner un champ' );
				return;
				
			}
			
			var operatorList = document.getElementById( getFieldOperatorListID( selectedField ) );
			var operator = operatorList.options[ operatorList.selectedIndex ].value;
			var operatorStr = operatorList.options[ operatorList.selectedIndex ].innerHTML;
			if( operatorStr == '' ){
			
				alert( 'Vous devez renseigner le champ \'opérateur\'' );
				return;
				
			}
			
			var value = document.getElementById( 'ValueEdit' ).value;
			if( value == '' ){
			
				alert( 'Vous devez renseigner le champ \'valeur\'' );
				return;
				
			}
			
			if( operator == '%LIKE%' ){
			
				operator = 'LIKE';
				value = '%' + value + '%';
				
			}
			else if( operator == '%NOT LIKE%' ){
			
				operator = 'NOT LIKE';
				value = '%' + value + '%';
				
			}
			else if( operator == 'LIKE%' ){
			
				operator = 'LIKE';
				value = value + '%';
				
			}
			
			var rule = '\`' + selectedField + '\` ' + operator + ' \'' + value + '\'';
			var ruleStr =  selectedFieldStr + ' ' + operatorStr + ' ' + value;
			
			var rules = document.getElementById( 'ExportRules' );
		
			rules.options[ rules.options.length ] = new Option( ruleStr, rule );
			
			/*Correctif décodage des entités HTML*/
			rules.options[ rules.options.length - 1 ].value = rule;
			rules.options[ rules.options.length - 1 ].innerHTML = ruleStr;
			
			updateExportRules();
			
		}
		
		function getFieldOperatorListID( fieldname ){
		
			var fieldList = document.getElementById( 'FieldList' );
			var selectedField = fieldList.options[ fieldList.selectedIndex ].value;
			
			var type = fields[ selectedField ][ 'type' ];
			
			if( type == 'char' || type == 'char or null' )
				return 'TextOperators';
			else if( type == 'int' || type == 'dec' )
				return 'NumericOperators';
			else if( type == 'date' || type == 'datetime' || type == 'time' )
				return 'DateOperators';
			else if( type == 'user' )
				return 'UserOperators';
			
		}
		
		function displayFieldTools( fieldname ){
		
			var fieldList = document.getElementById( 'FieldList' );
			var selectedField = fieldList.options[ fieldList.selectedIndex ].value;
			
			var help = fields[ selectedField ][ 'help' ];
			
			document.getElementById( 'Calendar' ).style.display = help == 'date' ? 'block' : 'none';
			document.getElementById( 'Users' ).style.display = help == 'valid_user' ? 'block' : 'none';
			document.getElementById( 'ValueEdit' ).style.display = ( help == 'valid_user' || help == 'date' ) ? 'none' : 'block';
			
		}

		function deleteSelectedRule(){
		
			var rules = document.getElementById( 'ExportRules' );
			
			rules.options[ rules.selectedIndex ] = null;
		
			updateExportRules();
			
		}

		function updateExportRules(){
		
			var rules = document.getElementById( 'ExportRules' );
			var where = document.getElementById( 'Where' );
			
			where.value = '';
			
			var i = 0;
			while( i < rules.length ){
			
				if( i )
					where.value += ' AND ';
					
				where.value += rules.options[ i ].value;
				
				i++;
				
			}
			
		}
		
		function displayExportRules( display ){
		
			document.getElementById( 'ExportRulesDiv' ).style.display = display ? 'block' : 'none';
			
			if( !display )
				removeAllExportRules();
			
		}
		
		function removeAllExportRules(){
		
			var rules = document.getElementById( 'ExportRules' );
			
			var i = rules.options.length - 1;
			while( i >= 0 ){
			
				rules.options[ i ] = null;
				
				i--;
				
			}
			
			document.getElementById( 'Where' ).value = '';
			
		}
		
		function htmlEntityDecode( string ){
			
			/*bug mozilla?*/
			string = string.replace( '&lt;', '<' );
			string = string.replace( '&gt;', '>' );
			
			return string;
			
		}
		$(function() {
$("#datepicker").datepicker();
$.datepicker.formatDate('yyyy-mm-dd');

});
		function changeCalendar( dateField ){
		 
		 data = document.getElementById( "calendarField" ).value;
				var tab = data.split('-'); 	
					D = tab[0] ;
				
				
				M = tab[1] ;
				Y = tab[2] ;
				var datas = tab[2]+'-'+tab[1]+'-'+tab[0];
			//$.datepicker.formatDate('yy-mm-dd', new Date("data"));
			
			document.getElementById( "ValueEdit" ).value = datas;
			
			return true;
			
		}
		
	// -->
	</script>
	<br>
	<div class="subTitleContainer">
        <p class="subTitle"><input type="checkbox" onclick="displayExportRules( this.checked );" />
		Critères d'export</p>
	</div>
	
	<div id="ExportRulesDiv" style="display:none;">
	<br />
		<table class="dataTable">
			<tr>
				<th class="filledCell" align="center">Champ</th>
				<th class="filledCell" align="center">Opérateur</th>
				<th class="filledCell" align="center">Valeur</th>
				<th  class="filledCell" align="center"></th>
			</tr>
			<tr>
				<td valign="top" align="center">
					<select name="FieldList" id="FieldList" onchange="updateRuleForm();">
						<option></option>
						<?php
							
							$query = "
							SELECT fieldname, export_name, description
							FROM desc_field
							WHERE tablename LIKE '" . $_POST[ "table_name" ] . "'
							AND export_avalaible = 1
							AND help NOT IN ( 'upload', 'password', 'cpopup' )
							ORDER BY order_view ASC, export_name ASC";
							
							$rs = DBUtil::getConnection()->Execute( $query );
							if( $rs === false )
								die( "Impossible de récupérer la liste des champs exportables" );
								
							while( !$rs->EOF() ){
								
								$fieldname 		= $rs->fields( "fieldname" );
								$export_name	= $rs->fields( "export_name" );
								
								?>
								<option value="<?php echo htmlentities( $fieldname ) ?>"><?php echo htmlentities( $export_name ) ?></option>
								<?php
								
								$rs->MoveNext();
								
							}
							
						?>
						<option value="username">Auteur derni&egrave;re modif.</option>
						<option value="lastupdate">Date derni&egrave;re modif.</option>
					</select>
				</td>
				<td valign="top" align="center">
					<select name="NumericOperators" id="NumericOperators" style="display:block;">
						<option></option>
						<option value="=">=</option>
						<option value="&gt;">&gt;</option>
						<option value="&gt;=">&ge;</option>
						<option value="&lt;">&lt;</option>
						<option value="&lt;=">&le;</option>
						<option value="&lt;&gt;">&ne;</option>
					</select>
					<select name="TextOperators" id="TextOperators" style="display:none;">
						<option></option>
						<option value="LIKE">est égale à</option>
						<option value="%LIKE%">contient le texte</option>
						<option value="LIKE%">commence par</option>
						<option value="NOT LIKE">est différent de</option>
						<option value="%NOT LIKE%">ne contient pas le texte</option>
					</select>
					<select name="DateOperators" id="DateOperators" style="display:none;">
						<option></option>
						<option value="=">est égale</option>
						<option value="&lt;&gt;">est différent de</option>
						<option value="&lt;=">est antérieure ou égale à</option>
						<option value="&lt;">est strictement antérieure à</option>
						<option style="font-size:10px;" value="&gt;=">est postérieure ou égale à</option>
						<option style="font-size:10px;" value="&gt;">est strictement postérieure</option>
					</select>
					<select name="UserOperators" id="UserOperators" style="display:none;">
						<option></option>
						<option value="LIKE">=</option>
						<option value="NOT LIKE">&ne;</option>
					</select>
				</td>
				<td valign="top" align="center">
					<input type="text" name="ValueEdit" id="ValueEdit" maxlength="20" class="textInput" value="" />
					<div id="Calendar" style="display:none; white-space:nowrap;">
						<input type="text" id="calendarField" class="calendarInput" />
						<?php DHTMLCalendar::calendar( "calendarField", true, "changeCalendar" ); ?>
					</div>
					<select name="Users" id="Users" onchange="document.getElementById( 'ValueEdit' ).value = this.options[ this.selectedIndex ].value;" style="display:none;">
					<option></option>
					<?php
						
						$query = "
						SELECT iduser, firstname, lastname, login
						FROM user
						ORDER BY lastname ASC";
						
						$rs = DBUtil::getConnection()->Execute( $query );
						
						if( $rs === false )
							die( "Impossible de récupérer la liste des utilisateurs" );
							
						while( !$rs->EOF() ){
						
							$iduser 	= $rs->fields( "iduser" );
							$name 		= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
							$login 		= $rs->fields( "login" );
							
							?>
							<option value="<?php echo $login ?>"><?php echo "$name ($login)" ?></option>
							<?php
							
							$rs->MoveNext();
								
						}
						
						?>
					</select>
				</td>
				<td class="empty" valign="top" align="left">
					<input type="button" name="createExportRuleBtn" id="createExportRuleBtn" onclick="createExportRule();" value="<?php echo Dictionnary::translate( "create_rule" ) ?>" class="blueButton" />
				</td>
			</tr>
			<tr>
				<th colspan="3" valign="top" align="center" class="empty" style="padding:10px 0px 0px 0px; width:100%;">
					<select name="ExportRules" id="ExportRules" multiple="multiple" style="width:100%; height:150px; border:1px solid #949494;">
					</select>
					<input type="hidden" name="Where" id="Where" value="" />
				</th>
				<th valign="top" align="left" class="empty" style="padding:10px 2px 2px 2px;">
					<input type="button" class="blueButton" name="" value="Supprimer la sélection" onclick="deleteSelectedRule(); return false;" style="float:right; margin-top:7px;" />
				</th>
			</tr>
		</table>
	</div>
	<?php
	
}
 
//-----------------------------------------------------------------------------
	
function displayOrderByForm(){
	
	global $GLOBAL_START_URL;
 	
	?>
	<script type="text/javascript" language="javascript">
	<!--
	
		function deleteSelectedSortRule(){
		
			var OrderByRules = document.getElementById( 'OrderByRules' );
			var selectedIndex = OrderByRules.selectedIndex;
			
			OrderByRules.options[ selectedIndex ] = null;
			
			updateOrderByRules();
				
		}
		
		function updateOrderByRules(){
		
			var OrderByRules = document.getElementById( 'OrderByRules' );
			
			var sortStr = '';
			var i = 0;
			while( i < OrderByRules.options.length ){
			
				if( i )
					sortStr += ', ';
				
				sortStr += OrderByRules.options[ i ].value;
				
				i++;
				
			}
			
			document.getElementById( 'OrderBy' ).value = sortStr;
			
		}
		
		function createSortRule(){

			var SortByFieldName = document.getElementById( 'SortByFieldName' );
			
			var sort = SortByFieldName.options[ SortByFieldName.selectedIndex ].value;
			var sortStr = SortByFieldName.options[ SortByFieldName.selectedIndex ].innerHTML;
			
			var SortOption = document.getElementById( 'SortOption' );
			sort += ' ' + SortOption.options[ SortOption.selectedIndex ].value;
			sortStr += ' ' + SortOption.options[ SortOption.selectedIndex ].innerHTML;
			
			var rules = document.getElementById( 'OrderByRules' );
			rules.options[ rules.options.length ] = new Option( htmlEntityDecode( sortStr ), sort );
			
			updateOrderByRules();
			
		}
		
		function MoveSelectionUp(){
		
			var rules = document.getElementById( 'OrderByRules' );
			var selectedIndex = rules.selectedIndex;
			var selectedItem = rules.options[ selectedIndex ];
			
			if( selectedIndex == 0 )
				return;
				
			var previous = rules.options[ selectedIndex - 1 ];
			var html = previous.innerHTML;
			var value = previous.value;
			
			previous.innerHTML = selectedItem.innerHTML;
			previous.value = selectedItem.value;
			
			selectedItem.innerHTML = html;
			selectedItem.value = value;
			
			rules.selectedIndex = selectedIndex - 1;
			
			updateOrderByRules();
			
		}
		
		function MoveSelectionDown(){
		
			var rules = document.getElementById( 'OrderByRules' );
			var selectedIndex = rules.selectedIndex;
			var selectedItem = rules.options[ selectedIndex ];
			
			if( selectedIndex == rules.options.length - 1 )
				return;
				
			var next = rules.options[ selectedIndex + 1 ];
			var html = next.innerHTML;
			var value = next.value;
			
			next.innerHTML = selectedItem.innerHTML;
			next.value = selectedItem.value;
			
			selectedItem.innerHTML = html;
			selectedItem.value = value;
			
			rules.selectedIndex = selectedIndex + 1;
		
			updateOrderByRules();
			
		}
		
		function displayOrderByRules( display ){
		
			document.getElementById( 'OrderByRulesDiv' ).style.display = display ? 'block' : 'none';
			
			if( !display )
				removeAllOrderByRules();
			
		}
		
		function removeAllOrderByRules(){
		
			var rules = document.getElementById( 'OrderByRules' );
			
			var i = rules.options.length - 1;
			while( i >= 0 ){
			
				rules.options[ i ] = null;
				
				i--;
				
			}
			
		}
		
	// -->
	</script>
	
	<br>
	<div class="subTitleContainer">
                <p class="subTitle"><input type="checkbox" onclick="displayOrderByRules( this.checked );" />
		Crit&egrave;res de tri</p>
	</div>
	
	<div id="OrderByRulesDiv" style="display:none;">
	<br />
		<table class="dataTable">
			<tr>
				<th class="filledCell" align="center">Champ</th>
				<th class="filledCell" align="center">Tri par ordre</th>
				<th class="filledCell" align="center"></th>
			</tr>
			<tr>
				<td valign="top" align="center">
					<select name="SortByFieldName" id="SortByFieldName">
						<option></option>
						<?php
						
							$query = "
							SELECT fieldname, export_name
							FROM desc_field
							WHERE tablename LIKE '" . $_POST[ "table_name" ] . "'
							AND export_avalaible = 1
							AND help NOT IN ( 'upload', 'password', 'cpopup' )
							ORDER BY order_view ASC, export_name ASC";
							
							$rs = DBUtil::getConnection()->Execute( $query );
							if( $rs === false )
								die( "Impossible de récupérer la liste des champs exportables" );
								
							while( !$rs->EOF() ){
								
								$fieldname = htmlentities( $rs->fields( "fieldname" ) );
								$export_name = htmlentities( $rs->fields( "export_name" ) );
								
								?>
								<option value="<?php echo $fieldname ?>"><?php echo $export_name ?></option>
								<?php
								
								$rs->MoveNext();
								
							}	
					
						?>
						<option value="username">Auteur derni&egrave;re modif.</option>
						<option value="lastupdate">Date derni&egrave;re modif.</option>
					</select>
				</td>
				<td valign="top" align="center">
					<select name="SortOption" id="SortOption">
						<option></option>
						<option value="ASC">Croissant</option>
						<option value="DESC">Décroissant</option>
					</select>
				</td>
				<td valign="top" align="left" class="empty">
					<input type="button" value="<?php echo Dictionnary::translate( "create_rule" ) ?>" onclick="createSortRule();" class="blueButton" />
				</td>
			</tr>
			<tr>
				<th colspan="2" valign="top" align="center" class="empty" style="padding:10px 0px 0px 0px; width:100%;">
					<select name="OrderByRules" id="OrderByRules" multiple="multiple" style="width:100%; height:150px; border:1px solid #949494;">
					</select>
					<input type="hidden" name="OrderBy" id="OrderBy" value="" />
				</th>
				<th valign="top" align="left" class="empty" style="padding:10px 2px 2px 2px;">
					<a href="#" onclick="deleteSelectedSortRule(); return false;">
					<input type="submit" class="blueButton" value="Supprimer la sélection" style="float:right; margin-top:7px;" />
					</a>
					<br />
					<a href="#" onclick="MoveSelectionUp(); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/asc.png" alt="Monter la s&eacute;lection" style="border-style:none;" /></a>
					<br />
					<a href="#" onclick="MoveSelectionDown(); return false;"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/desc.png" alt="Descendre la s&eacute;lection" style="border-style:none;" /></a>
				</th>
			</tr>
		</table>
	</div>
	<?php
	
}

//-----------------------------------------------------------------------------

?>