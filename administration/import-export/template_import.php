<?php

 /**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Importation des données au format CSV
 */

include_once( dirname( __FILE__ ) . "/../../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/adm_table.php" );


class DATA_IMPORT extends ADMTABLE
{

var $userfile = "";
var $con;
var $table_name;
var $fp;
var $Arr_FieldList_detail;
var $Arr_FieldList_product;
var $Arr_FieldLists;
var $Arr_ExportLists;
var $Arr_ImportLists;
var $Arr_Types;
var $Arr_PresentLists;
var $username =  '';
var $lastupdate ='';

function __construct($tbl)
	{
	parent::__construct($tbl);
	//$this->username = Util::html_escape($_SESSION['__session__']['Login']);
	$this->username = Util::html_escape(DBUtil::getConnection()->connectionId, $_SESSION["AdminLogin"]);
	$this->lastupdate = date('Y-m-d H:i:s'); 
	}
	

// rechreche les champs pour une table dans desc_field 
function RetriveFields($Str_NameTable) 
{
	
	$this->Arr_Types[$Str_NameTable]=array();
	$this->Arr_ExportLists[$Str_NameTable]=array();
	$this->Arr_FieldLists[$Str_NameTable]=array();
	
	$res = DBUtil::query("SELECT fieldname, export_avalaible, type, export_name FROM desc_field WHERE tablename='$Str_NameTable'");
	$n = $res->RecordCount();
	if ($n>0) 
	  {
		for ($i=0;$i<$n;$i++)
		{
		//	$Local_Availability=$res->fields["export_avalaible");
			$Local_FieldName=$res->fields["fieldname"];
	
			if ($Local_FieldName<>"")
				{
					$x = $res->fields["type"];if( $x =='' ) $x='char'; //default to char
					$this->Arr_Types[$Str_NameTable][]=$x;
					$this->Arr_FieldLists[$Str_NameTable][] = $Local_FieldName;
					$this->Arr_ExportLists[$Str_NameTable][] = $res->fields["export_name"];
				}
					else
				{ ?>
	<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
	      <div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<p class="msg">
			<?php
			Error("RetriveFields",Dictionnary::translate("imp-exp_msg7"), 105);
			return false;
			?>
				</p>
					
			<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
				<?php
				}
			$res->MoveNext();
		}
	  }
	else
	  {
	  	?>
	<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
	      <div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<p class="msg">	  	
	<?php
		Error("RetriveFields",Dictionnary::translate("imp-exp_msg7"), 105);
		return false;
	?>
				</p>
			<div class="clear"></div>
	   	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
		<?php
	  }
	  //all records done
	  
	  return true;
	  
}


//	fonction pour obtenir le nom initial du table $Str_NameTable
//			reverse du FExportRenameTable
function RenameTable($Str_NameTable)
{
$res = DBUtil::query("SELECT export_name, import_avalaible FROM desc_table WHERE tablename='$Str_NameTable' ");
if ($res->RecordCount()>0) 
	{
		$Local_Availability=$res->fields["import_avalaible"];
		if ($Local_Availability<>"0")
			{
				$Local_TableName=$res->fields["export_name"];
				if ($Local_TableName<>"") return htmlentities($Local_TableName);
				else	Error("RenameTable", Dictionnary::translate("imp-exp_msg8"), 107);
			}
			else
			{
				return -1; //Table Not Available at Import
			}
	}
	else
	{
			Error("RenameTable", Dictionnary::translate("imp-exp_msg9"), 108);
	}
//all records done
}



//	fonction pour obtenir les tables sur qu'on peut importer
//			retourne un array avec ces tables 
function ListAvailableTables()
{
$res = DBUtil::query("SELECT tablename, export_name FROM desc_table WHERE import_avalaible<>'0' ORDER BY export_name ASC");
if ($res->RecordCount()>0) 
	{	
	$Array_TablesAvailable=array();
	    for ($i=0;$i< $res->RecordCount();$i++)
		{
		$Array_TablesAvailable[$i]=$res->fields["tablename"];
		$res->MoveNext();
		}
		//all records done
	return $Array_TablesAvailable;
	}
	else
	{
			Error("ListAvailableTables", Dictionnary::translate("imp-exp_msg10"), 109);
	}
}


//	fonction HTML qui mis le commencement d'une form
function BeginPostFileForm($Str_ActionName)
{
	echo "\t\t".'<form enctype="multipart/form-data" action="'.$Str_ActionName.'" method="post">'."\n";
}


//	fonction HTML qui mis le fin d'une form
function EndForm()
{
	echo "\t\t".'</form>'."\n";
}



//	fonction pour l'affichage d'un Input button 
//		Param:	$Str_ButtonType : le type du button
//			$Str_ButtonName : le nom du button
//			$Str_ButtonValue : la valeur du button
function InputButton($Str_ButtonType, $Str_ButtonName, $Str_ButtonValue, $Str_ButtonAdd = "")
{
	echo "<input type=\"$Str_ButtonType\"  name=\"$Str_ButtonName\" value=\"$Str_ButtonValue\" $Str_ButtonAdd>\n";
}


//	fonction HTML qui affiche une ligne horizontale
//		Param: $Int_HRPercent = percent
function HRLine($Int_HRPercent = 80)
{
	echo "<hr width=\"$Int_HRPercent%\">\n";
}


//	fonction HTML qui affiche une selection pour le type de import
function Format()
{
	echo "Select export type:\n";
	echo "<br />CSV\n";
	InputButton("radio", "export_format" ,"CSV", "CHECKED");
	echo "<br />XML\n ";
	InputButton("radio", "export_format" ,"XML");
	echo "<br />";
}

//	fonction HTML qui affiche une selection pour la table ou on veut importer
function MetaTable($Array_Tables)
{
?>
<div class="tableContainer" style="width:500px; padding-top:14px; margin:auto;">
 	<table class="dataTable">
		<tr>
		<th class="filledCell"><?php echo Dictionnary::translate("txt_select_table"); ?></th>
		<td width="200">
			<select name="table_name" class="fullWidth">
<?php
		foreach ($Array_Tables as $i=>$table) {
?>
			<option value="<?php echo $table ?>"><?php echo utf8_encode($this->RenameTable($Array_Tables[$i])) ?></option>
<?php
		}
?>
			</select>
		</td>
	</tr>
<?php
}

//	fonction pour faire upload du fichier à importer.
function  ImportSeq()
{
	
	global $GLOBAL_START_URL;
	
	$upload_max_filesize =  ini_get('upload_max_filesize');
	//echo 'upload_max_filesize = '.$upload_max_filesize."<br />";
	if (strrpos($upload_max_filesize,'M') !== false) {
		$upload_max_filesize = (int)$upload_max_filesize * 1048576;
	} elseif (strrpos($upload_max_filesize,'K') !== false) {
		$upload_max_filesize = (int)$upload_max_filesize * 1024;
	} else {
		$upload_max_filesize = (int)$upload_max_filesize;
	}
?>
	<tr>
		<th class="filledCell"><?php echo Dictionnary::translate("imp-exp_txt_upload_file"); echo " (max.: $upload_max_filesize)"; ?></th>
		<td width="200"><input type="hidden" name="MAX_FILE_SIZE" value="<?php echo $upload_max_filesize ?>" /><input type="file" name="userfile" /></td>
	</tr><tr>
		<th class="filledCell">Séparateur (csv) [| si description html]</th>
		<td width="200"><input type="text" name="fieldsep" value=";" class="textInput" /></td>
	</tr>
</table>
<input type="submit" class="blueButton" name="select_file" value="Importer" style="float:right; margin-top:7px;" />
</div>
<?php
}

//	fonction qui fait l'import effectif sur la table $table_name et le fichier a importer $userfile
function ReadVerifyInsert( $filename)
{
	$this->userfile = $filename;
	
	// read file

	if( !$this->Read() ){
		
		trigger_error( "Impossible de lire le fichier d'import", E_USER_ERROR );
		
		return false;
	
	}
	// build field list

	if( !$this->RetriveFields($this->tablename) ){
		
		trigger_error( "Fichier d'import mal formé", E_USER_ERROR );

		return false;
	
	}
		
	if( !$this->BuildFieldList($this->tablename,true) ){
		
		trigger_error( "Echec lors de l'analyse du fichier", E_USER_ERROR );
		
		return false;
	
	}
	if( !$this->Insert($this->tablename) ){
		
		trigger_error( "Impossible de sauvegarder les données", E_USER_ERROR );
		
		return false;
	
	}
    return true;
	
}


//	fonction pour ouvrir le fichier a importer, retourne un array avec le contenu du fichier
function Read()
{
	$userfile = preg_replace("/\\\\\\\\/","\\",$this->userfile); //Win32 upload problem
	
 
	$handle = fopen ($userfile, "rb");
	//$contents = fread ($handle,7);
    $contents = fgetcsv($handle, 4000, ";");    
	fclose ($handle);
    //echo $contents[0]. strlen($contents[0]);
	//if( $contents !='action;' && $contents !='"action'){
	if( strpos(  strtolower( $contents[0]),'action')=== false){	
		//trigger_error("<span class=\"msg\">Le fichier '". $this->userfile."' Ne semble pas être un fichier csv valide.<br /> Le premier mot de la premiere ligne du fichier à importer doit être action. Veuillez verifier vos données avec le bouton: <br /><INPUT TYPE=\"button\" VALUE=\"< Derrière \"   onClick=\"history.back()\"></span>" ,E_USER_ERROR);
		
		?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">

					<p class="msg">
			La premi&egrave;re colonnedu fichier CSV doit &ecirc;tre intitul&eacute;e &quot;<b>action</b>&quot;. 
</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>
		<?php
		
		return false;
		
	}
	
	$handle = fopen ($userfile, "rb");
    $regEx = "^([0-9]{2})/([0-9]{2})/([0-9]{4}) ([0-9]{2}):([0-9]{2}):([0-9]{2})\$^";
    $regEx2="^([0-9]{2})/([0-9]{2})/([0-9]{4}) ([0-9]{2}):([0-9]{2})\$^";
	while (($data = fgetcsv($handle, 4000, ";")) !== FALSE) {
		
		/* dates FR -> US */
		
		$i = 0;
		while( $i < count( $data ) ){
			
			$regs = array();
            $data[ $i ] = str_replace("  "," ",$data[ $i ]);
            if( preg_match( $regEx, $data[ $i ], $regs ) )
				$data[ $i ] = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ]." ". $regs[ 4 ] . ":" . $regs[ 5 ]. ":" . $regs[ 6 ];
            else if( preg_match( $regEx2, $data[ $i ], $regs ) )
				$data[ $i ] = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ]." ". $regs[ 4 ] . ":" . $regs[ 5 ]. ":00";
			else if( preg_match( "^([0-9]{2})/([0-9]{2})/([0-9]{4})\$^", $data[ $i ], $regs ) )
				$data[ $i ] = $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];
			else if( preg_match( "^([0-9]{2})/([0-9]{2})/([0-9]{2})\$^", $data[ $i ], $regs ) )
				$data[ $i ] = substr( date( "Y" ), 0, 2 ) . $regs[ 3 ] . "-" . $regs[ 2 ] . "-" . $regs[ 1 ];

			$i++;
				
		}
		
		$this->fp[] = $data;
    }
	
	fclose ($handle);
	
	///echo "<pre>\n";print_r($this->fp);echo "</pre>\n";
	/*
	if ( $this->fp = @file($userfile) )
		{
		return true;
		}
			else 
		{
		Error("Read", Dictionnary::translate("imp-exp_msg12"). " ". $this->userfile." Veuillez verifier votre donnees avec le bouton: <br /><INPUT TYPE=\"button\" VALUE=\"< Derrière \"   onClick=\"history.back()\">" ,2);
		}
	*/	
	
	return true;
	
}


//	fonction pour obtenir la liste des champs pour l'import (premiere ligne du fichier a importer)
function BuildFieldList($table_name, $warn = false)
{
	$Arr_FirstLine = $this->fp[0];
	//$Arr_FirstLine = explode(";" , $Str_FirstLine) ;
	list($key, $value) = each ( $Arr_FirstLine ); //pass over action name
	//if ( $Arr_FirstLine[0] != "action") 
    if( strpos(strtolower($Arr_FirstLine[0]),'action' )===false)
	{
			return false;
			//echo "W".$Arr_FirstLine[0]."W";
			//trigger_error(" . Veuillez verifier votre fichier et refaire l'import avec le boutton: <br /><INPUT TYPE=\"button\" VALUE=\"< Derrière \"   onClick=\"history.back()\">",E_USER_ERROR);
			
	}
	
	$this->Arr_ImportLists[$table_name]=array();
	$this->Arr_PresentLists[$table_name]=array();
	
	$columnsToIgnore = $this->getColumnsToIgnore();
	
    //var_dump($this->Arr_ExportLists); var_dump($Arr_FirstLine);die;
	$wb= false;
	$i=0;
    
   //var_dump($this->Arr_ExportLists[$table_name]);echo '<br>';
  // var_dump($Arr_FirstLine);
   
	foreach($Arr_FirstLine as $value)
		{  //echo "*$i $value *";
		$value = trim($value); 
			if ($value !='')
			{
				$encoding = mb_detect_encoding(trim($value));
               // echo $encoding.'<br>';
               //echo $encoding.' value:'. $value.'<br>';
                 if( $encoding == 'UTF-8' ){                                         
  
                       $value =utf8_encode($value); 
                       //echo $value.'<br>';                       
                 } 
				$r = array_search($value,$this->Arr_ExportLists[$table_name]);
				if( $r === FALSE )
				{
				$this->Arr_PresentLists[$table_name][] = 0;
				if( $wb && !in_array( $value, $columnsToIgnore ) ) {
					//echo "<pre>"; print_r($this->Arr_ExportLists); echo "</pre>\n";
					//echo "<p class=\"mes_error\">"; // die("! $i $value !");
					//trigger_error(" $value n'est pas dans la table $table_name",E_USER_NOTICE);
					?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">
						Attention : Le champ &quot;<?php echo $value ?>&quot; n'est pas dans la table <?php echo $table_name ?>
					</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>
					<?php
					
					}
				} else {
					if( $value == 'username' OR $value == 'lastupdate' ) {
						//echo "JBDEBUG $value";
					} else {
						$this->Arr_ImportLists[$table_name][$i] = $this->Arr_FieldLists[$table_name][$r];
						$this->Arr_PresentLists[$table_name][$i] = $this->Arr_Types[$table_name][$r];
					}	
				}
			}
		$wb=$warn; $i++;
		}
		// echo "<pre>"; print_r($this->Arr_ImportLists); echo "</pre>\n";
		
		return true;
		
}



// fonction pour conversion
function FormConv($value,$fmt)
{   //echo "[$value/$fmt]";
$value=trim( $value );
if(strstr($fmt,'null') AND $value == '') return '_NULL_';
$fmt = substr($fmt,0,3);
if($fmt == 'dec' OR $fmt == 'int')
	 return str_replace(' ','',str_replace(',','.',$value));

$value=str_replace("\\",'',$value);
$value=str_replace("'","\'",$value);
return $value;
}

// fonction pour faire l'import effectif. On sait deja les champs a inserer ($Str_FieldList) et le contenu du fichier ($fp)
function Insert($table_name)
{
$Vec_FieldList = &$this->Arr_ImportLists[$table_name];
/////////////////
// Initialising

	$Int_TotalProcessed = sizeof($this->fp); //count total actions
	$Int_ModifyProcessed=0; //count modified
	$Int_ModifyIgnored=0; //count modified ignored
	$Int_InsertProcessed=0; //count inserted
	$Int_InsertIgnored=0; //count inserted ignored
	$Int_DeleteProcessed=0; //count deleted
	$Int_DeleteIgnored=0; //count deleted ignored
	$Int_IgnoreProcessed=0; //count ignored


// Set Operation Ids (action column)
// 0 = insertion
// 1 = modifications dans la table
// 2 = suppresion
$Int_InsertId = 0;
$Int_ModifyId = 1;
$Int_DeleteId = 2;

//get key names
//	if ($Int_OperationId == 0 || $Int_OperationId == 2) 
//		{
			$artmp = DBUtil::getConnection()->GetRow( "SELECT tablekeys FROM desc_table WHERE tablename = '$table_name' ORDER BY export_name ASC" );
			if( $artmp === false ){
				
				?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">				
					Impossible de récupérer les clés primaires pour la table <?php echo $table_name ?>
				</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>				
				<?php
				
				return false;
			}
			
		
			$Str_FieldsName='';
			if (!$Str_ModifyKeyFieldNames = $artmp["tablekeys"]) 
			{
				
				?>
<div id="globalMainContent" style="width:100%;margin-bottom:10px;">
	<div class="mainContent" style="width:700px; margin-left:30px;">
   	   <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">
					Cette table n'a pas été configur&eacute;e correctement pour l'import-export
				</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
 </div>								
				<?php
				
				return false;
				
			};
			
			$Str_ModifyKeyFieldNames  = str_replace (" " , "" , $Str_ModifyKeyFieldNames); //chop spaces from Field Names List
			
//		}	


/////////////////
// Processing


//real_product doit être importé par un autre script
	if ($table_name == "real_product") {
		trigger_error("Erreur : pas d'import de produit avec ce script",E_USER_ERROR);
		return false;
	}

// first line (i=0) contains field names
	$i=0; $Finished = false;
	while ( !$Finished )
		{
			
			set_time_limit(600);
			
			$i++;  
			if ( empty($this->fp[$i]) ) break;
			$Finished = false; //continue iterations

			$Arr_Line = $this->fp[$i];
			$n = count($Arr_Line);
			for ( $tmp=0 ; $tmp < $n; $tmp++)
			{					
				if( isset($this->Arr_PresentLists[$table_name][$tmp]) )
				//$Arr_ValuesLine[$tmp] = $this->FormConv($Arr_Line[$tmp],$this->Arr_PresentLists[$table_name][$tmp]);
                
                // Import Csv : 31- Résolution caractère spéciaux
                $StrColumnValue = $this->FormConv($Arr_Line[$tmp],$this->Arr_PresentLists[$table_name][$tmp]);
                $Str_Encodage = mb_detect_encoding($StrColumnValue);
                
                    if( $Str_Encodage== 'UTF-8' ){
                                        
                     $StrColumnValue =utf8_encode($StrColumnValue);
                    
                   }
                  
                   $Arr_ValuesLine[$tmp] = $StrColumnValue;
			}

			if ( empty($this->fp[$i]) ) break;
			reset($Arr_ValuesLine);
	
			list($key, $value) = each ( $Arr_Line ); //get operation id
			$Int_OperationId = substr ($value,0,1); //premier element de la ligne
			$Str_ValuesList='' ;

			foreach($Vec_FieldList as $ii=>$vv) {

				if(isset($Arr_ValuesLine[$ii]) ){
					
					$value = $Arr_ValuesLine[$ii];
                  
					$index = 0;
					$done = false;
					while( !$done && $index < count( $this->design ) ){

						if( $this->design[ $index ][ "fieldname" ] == $vv ){
							
							if( ( $Int_OperationId == $Int_InsertId && $this->design[ $index ][ "append_select_table" ] != "" )
								|| ( $Int_OperationId == $Int_ModifyId && $this->design[ $index ][ "modify_select_table" ] != "" ) ){

								$pattern = $Int_OperationId == $Int_InsertId ? $this->design[ $index ][ "append_select_table" ] : $this->design[ $index ][ "modify_select_table" ];
								list( $table, $column1, $column2 ) = explode( ":", $pattern );
								$value = DBUtil::getDBValue( $column2, $table, $column1, $value );
								
							}
							
							$done = true;
							
						}
						
						$index++;
						
					}
					
				}
				else $value ='';
				
				if($value == '_NULL_') 
					$Str_ValuesList .= "," . "NULL";
				else $Str_ValuesList .= "," . "'".$value."'";
			}

			$Str_ValuesList = substr( $Str_ValuesList , 1 ); //chop first ,
			

			$Str_UpdateString="`username`='".$this->username."' ,`lastupdate`='".$this->lastupdate."'" ;
			$Str_DeleteFind='';

			foreach($Vec_FieldList as $ii=>$vv)
			{
				//if we are not modifying and the current field is not a key field (captured in the WhereCondition)
				if  ( !($Str_ModifyKeyFieldNames && strstr("," . $Str_ModifyKeyFieldNames . ",", "," . $vv .",") ) )
				{
					if(isset($Arr_ValuesLine[$ii])) 
						$Value=trim($Arr_ValuesLine[$ii]);
					else continue;
					
					if($Value == '_NULL_') 
						$Str_UpdateString.= ",`$vv`=NULL";
					else {
						
						$Value = Util::html_escape(str_replace("\\","",$Value));
						$Str_UpdateString.= ",`$vv`='$Value'";
					
					}
				//	if(!isset($Arr_ValuesLine[$ii+1])) echo "<pre>\n";  print_r($Arr_ValuesLine); echo "\n</pre>";
				
				}
				else
				{
					$Str_DeleteFind .= ",".$vv . "=" . "'" . trim($Arr_ValuesLine[$ii]). "'";
				}
				
				if(isset($Arr_ValuesLine[$ii])) 
					$Arr_KeyValues[$vv] = trim($Arr_ValuesLine[$ii]);
				else $Arr_KeyValues[$vv]='';
				
			}
			//$Str_UpdateString = substr ($Str_UpdateString, 1);//chop first ,
			$Str_WhereCond = str_replace(",", " AND ",$Str_UpdateString);
			$Str_DeleteWhereCond = str_replace(",", " AND ", substr($Str_DeleteFind, 1) );

			//echo "<br />Str_UpdateString |$Str_UpdateString|, Str_WhereCond |$Str_WhereCond| ";

		
		if ( $Int_OperationId == $Int_ModifyId ) 
				{
					// modify needed
					$Arr_KeyFieldNames=array();
					$Str_WhereCondition='';
					$Arr_KeyFieldNames = explode("," , $Str_ModifyKeyFieldNames);
					while ( list($keynumber,$fieldname) = each($Arr_KeyFieldNames) ) 
						{
							$Str_WhereCondition .= "AND ". $fieldname . " = '" . $Arr_KeyValues["$fieldname"] ."' ";
						}
					$Str_WhereCondition  = substr($Str_WhereCondition,3); //chop first AND
					$Str_SQLQuery = "UPDATE `$table_name` SET $Str_UpdateString WHERE $Str_WhereCondition ";

					//echo "<br />$Str_SQLQuery";
					
					//$Str_SQLQuery = $this->ConvertStringForInput($Str_SQLQuery);
//	echo "<br />DV : $Str_SQLQuery<br />";
					$res = DBUtil::query($Str_SQLQuery);
					
					if( $res === false ){
						 
						?>
						<p class="alert">
							Impossible d'effectuer les modifications
						</p>
						<?php
						
						showDebug( $Str_SQLQuery, "[ ERROR ]", "code" );
						
						return false;
						
					}
					
					$afr = DBUtil::getConnection()->Affected_Rows();
					
					//$Str_SQLQuery = "SELECT * FROM $table_name WHERE $Str_WhereCondition ";
					//$res = DBUtil::query($Str_SQLQuery);
					if ($afr) 
						{
							$Int_ModifyProcessed+=$afr;
						}
					else
						{
							$Int_ModifyIgnored++;
						}
					//all records done
				}
			if ( $Int_OperationId == $Int_DeleteId ) 
				{
					// delete needed
					// modify needed
					
					$Str_SQLQuery = "DELETE FROM `$table_name` WHERE $Str_DeleteWhereCond"; 
					//$Str_SQLQuery = $this->ConvertStringForInput($Str_SQLQuery);
//	echo "<br />DV : $Str_SQLQuery<br />";
					$res = DBUtil::query($Str_SQLQuery);
					$afr = DBUtil::getConnection()->Affected_Rows();
					
					if( $afr === false ){
							
						?>

	<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;"> class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">					
							Impossible de supprimer les données dans la table <?php echo $table_name ?>
							<?php echo htmlentities( $Str_SQLQuery ) ?>
						</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>							
						<?php
						
					}
					
					if ($res->RecordCount())
						{
							$Int_DeleteProcessed+=$afr;
						}
					else
						{
							$Int_DeleteIgnored++;
						}
				}
			if ( $Int_OperationId == $Int_InsertId ) 
			{
					//already existing record?
					$Str_WhereCondition='';
					$Arr_KeyFieldNames = explode("," , $Str_ModifyKeyFieldNames);
					while ( list($keynumber,$fieldname) = each($Arr_KeyFieldNames) ) 
					{
						
						if( isset( $Arr_KeyValues["$fieldname"] ) )
							$Str_WhereCondition .= "AND `". $fieldname . "` = '" . Util::html_escape($Arr_KeyValues["$fieldname"]) ."' ";
						else{
							
							?>
<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">
								La colonne <?php echo $fieldname ?> est absente dans le fichier
					</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>					
							<?php
							
							return false;
						
						}
						
					}
					$Str_WhereCondition  = substr($Str_WhereCondition,3); //chop first AND
					$Str_Query = " SELECT $Str_ModifyKeyFieldNames FROM `$table_name` WHERE $Str_WhereCondition "; 
					$res = DBUtil::query ($Str_Query);
					
					if( $res === false ){
							
						?>
<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">					
							Impossible de récupérer les données dans la table <?php echo $table_name ?>
				</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>			
						<?php
						
						showDebug( $Str_Query, "[ ERROR ]", "code" );
						
						return false;
						
					}
					
//	echo "<br />DV : $Str_Query<br />";
					if ($res->RecordCount() == 0)
					{
					
					$qr = " SELECT * FROM code_indexation WHERE `table` = '$table_name' ";
					$rsss = DBUtil::query($qr);
						
						
						
						
						
						
						if($rsss == true ){
						
						
					
							if ( $rsss->RecordCount()> 0 )
							{
					
								include_once( dirname( __FILE__ ) . "/../../objects/TradeFactory.php" );
								$id_table = TradeFactory::Indexations( $table_name );

								$nb_char_del=3;
								$Str_ValuesList_Insert=substr($Str_ValuesList,-strlen($Str_ValuesList)+$nb_char_del); 
								// var_dump($Str_ValuesList);
								$Str_FieldList = "`".implode('`, `',$Vec_FieldList)."`"; //implode(",", $Vec_FieldList);
								$Str_FieldList .= ",`username`,`lastupdate`" ;
								$Str_ValuesList_Insert .= ",'".$this->username."' ,'".$this->lastupdate."'" ;
								$Str_SQLQuery = "INSERT IGNORE INTO `$table_name` ($Str_FieldList) VALUES('$id_table', $Str_ValuesList_Insert)";
								// var_dump($Str_SQLQuery);	
								//$Str_SQLQuery = $this->ConvertStringForInput($Str_SQLQuery);
								$res = DBUtil::query($Str_SQLQuery);
								
								
							
							}
							else
							{
					
					
								// update needed
								$Str_FieldList = "`".implode('`, `',$Vec_FieldList)."`"; //implode(",", $Vec_FieldList);
								$Str_FieldList .= ",`username`,`lastupdate`" ;
								$Str_ValuesList .= ",'".$this->username."' ,'".$this->lastupdate."'" ;
								$Str_SQLQuery = "INSERT IGNORE INTO `$table_name` ($Str_FieldList) VALUES($Str_ValuesList)";
								
								//$Str_SQLQuery = $this->ConvertStringForInput($Str_SQLQuery);
								$res = DBUtil::query($Str_SQLQuery);
								}
						
						}
						
						
						
						if($res === false ){
						 
							?>
<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
	      <div class="topRight"></div><div class="topLeft"></div>
				<div class="content">
					<p class="msg">						
								Impossible d'effectuer les modifications
					</p>
				<div class="clear"></div>
           	</div>
	 	<div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>		
							<?php
							
							showDebug( $Str_SQLQuery, "[ ERROR ]", "code" );
							
							return false;
							
						}
						
//	echo "<br />DV : $Str_SQLQuery<br />";
						$Int_InsertProcessed++;
						
					}
					else $Int_InsertIgnored ++;
						
				}
				
			if ($i==$Int_TotalProcessed && ! isset($Bool_Case)) {
				$Finished = true; //next loop, quit iteration
			}
		}
	$Int_TotalProcessed --; //skip fields line
	?>

	<!--<h2>Résultat de l'import</h2>-->
<div id="globalMainContent">
	<div class="mainContent" style="width:700px;margin-bottom:20px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Import réalisé</p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            
          
            	<div class="tableContainer" style="width:200px; padding-top:14px; margin:auto;">
		              	<table class="dataTable">
							<tr>
								<th class="filledCell">
								<?php echo $Int_TotalProcessed; ?>&nbsp;<?php echo Dictionnary::translate("imp-exp_line_made"); ?><br /></th>
								<?php
								$Int_IgnoreProcessed = $Int_TotalProcessed - $Int_InsertProcessed - $Int_DeleteProcessed - $Int_ModifyProcessed;
								?>
							</tr><tr>
								<td>
								<?php echo $Int_DeleteProcessed; ?> 
								<?php echo Dictionnary::translate("imp-exp_line_deleted"); ?>
								</td>
							</tr><tr>
								<td>
								<?php echo $Int_DeleteIgnored; ?> 
								<?php echo Dictionnary::translate("imp-exp_line_delete_ignored"); ?>
								</td>
							</tr><tr>
								<td>
								<?php echo $Int_InsertProcessed; ?>
								<?php echo Dictionnary::translate("imp-exp_line_integreted"); ?>
								</td>
							</tr><tr>
								<td>
								<?php echo $Int_InsertIgnored; ?> 
								<?php echo Dictionnary::translate("imp-exp_line_insert_ignored"); ?>
								</td>
							</tr><tr>
								<td>
								<?php echo $Int_ModifyProcessed; ?> 
								<?php echo Dictionnary::translate("imp-exp_line_modified"); ?>
								</td>
							</tr><tr>
								<td>
								<?php echo $Int_ModifyIgnored; ?> 
								<?php echo Dictionnary::translate("imp-exp_line_modify_ignored"); ?>
								</td>
							</table>
						</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
   </div>
</div>
<?php

	return true;
	
}



// Function a23: LineErrorReport($Arr_FieldList, $Arr_ValuesLine)
//			fonction utilisé pour raporter une erreur si il y sont des differences entre la ligne des champs et les lignes des valeurs
//					Param: $Arr_FieldList = array contenant la liste des champs
//								$Arr_ValuesLine = array contenant la liste des valeurs pour les champs
function LineErrorReport($Arr_FieldList, $Arr_ValuesLine)
{
	$maxf = sizeof($Arr_FieldList);
	$maxv = sizeof($Arr_ValuesLine)-1;
	$max = ($maxf>$maxv)?$maxf:$maxv;
	$font = "<font face=Arial size=-1>";

   	$Str_ErrorMessage.="\n ".Dictionnary::translate("imp-exp_inline")."  ".Dictionnary::translate("imp-exp_file_name")." ".$this->userfile;
	$Str_ErrorMessage.="\n :".Dictionnary::translate("imp-exp_illegal")." ";
	$Str_ErrorMessage.="\n :<br /> hjkhjkh<table border=0  cellpadding=1  cellspacing=1>";
	$Str_ErrorMessage.="\n <br />  <tr> <td bgcolor=yellow > ".Dictionnary::translate("imp-exp_field_list")." </td>";
	$Str_ErrorMessage.="\n <td bgcolor=yellow> ".Dictionnary::translate("imp-exp_register_list")." : </td> </tr>" ;
	for ($j=0; $j<$max; $j++)
		{
			$Str_ErrorMessage.="\n <tr> " ;
			$Str_ErrorMessage.="\n 	<td bgcolor=yellow > " ;
			$Str_ErrorMessage.="\n ";
			if (isset($Arr_FieldList[$j])) 
				{
					$fl = $Arr_FieldList[$j];
				}
					else
				$fl = "<font color=red><b>?!".Dictionnary::translate("import_field_empty")."?!";
			$Str_ErrorMessage.=$fl;
			$Str_ErrorMessage.="\n 	</td> " ;
			$Str_ErrorMessage.="\n 	<td bgcolor=yellow > " ;
			$Str_ErrorMessage.="\n ";
			if (isset($Arr_ValuesLine[$j+1]))
				{
					$vl = $Arr_ValuesLine[$j+1];
				}
					else
				$vl = "<font color=red><b>?!".Dictionnary::translate("import_field_empty")."?!";
			$Str_ErrorMessage.=$vl;
			$Str_ErrorMessage.="\n 	</td> " ;
			$Str_ErrorMessage.="\n </tr> " ;
		}
	$Str_ErrorMessage.="\n <br />  </table>" ;
	
	$Str_ErrorMessage.="\n <br />test1 ".Dictionnary::translate("imp-exp_msg13")."." ;
	$Str_ErrorMessage.="\n <br /> ".Dictionnary::translate("imp-exp_msg14")." : $i" ;

	//echo $Str_ErrorMessage ;
	?>
	<div class="mainContent" style="float:right; margin-right:25px; margin-bottom:15px;">
					      <div class="topRight"></div><div class="topLeft"></div>
        						<div class="content">
    <?php	
	trigger_error("Insert", $Str_ErrorMessage ,E_USER_ERROR);
	?>
					<p class="msg"><?php echo Dictionnary::translate("imp-exp_good_import") ?></p>
						  </div>
				      </div>		
	<?php
}

	//--------------------------------------------------------------------------------------------------
	
	function getColumnsToIgnore(){
	
		$columnsToIgnore = array();
		
		$query = "
		SELECT export_info_column
		FROM desc_field
		WHERE tablename LIKE '" . $this->tablename . "'
		AND export_info_column REGEXP '^[^:]+(:[^:]+){3}\$'";
		
		$rs = DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de récupérer le nom des colonnes à ignorer" );
		
		while( !$rs->EOF() ){
		
			$export_info_column = $rs->fields( "export_info_column" );
			
			list( $tablename, $fieldname, $useless, $alias ) = explode( ":", $export_info_column );

			$columnsToIgnore[] = $alias;
				
			$rs->MoveNext();
				
		}
		
		return $columnsToIgnore;
		
	}

	//--------------------------------------------------------------------------------------------------
	
}//EOC
?>
