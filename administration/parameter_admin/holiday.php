<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire jours fériés
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_admin_holiday";
    $Str_TableName="holidays";
    $KeyName = 'idholiday';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idholiday";
	$ArrayFieldsColumns2Show[]="holiday_1";

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>