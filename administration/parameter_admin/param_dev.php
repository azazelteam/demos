<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage en mode développeur
 */

//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "../../catalog/drawlift.php" );

$Title = Dictionnary::translate( "title_param_admin" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------

$TableName='parameter_admin';

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']))
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?> 
<div id="globalMainContent">
	<div class="mainContent fullWidthContent" style="margin-left:160px; width:680px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
				<form name="myform" method="POST" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th style="padding-left:4px;"><?php echo Dictionnary::translate('display_code') ; ?> :</th>
								<td>
									<?php echo DrawLiftParameters('show_debug','Afficher','Ne pas afficher');?></td>
							</tr>		
						</table>
					</div>
					<div>
						<input type="submit" value="Modifier" name="Modify" class="blueButton" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------

?>