<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage remise première commande
 */

include_once( "../../config/init.php" );

$Title = "Gestion des réductions clients";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//Enregistrement
if( isset( $_POST[ "Modify" ] ) || isset( $_POST[ "Modify_x" ] ) ){
	
	//Type de remise
	DBUtil::query( "UPDATE parameter_admin SET paramvalue = '" . $_POST[ "first_discount_type" ] . "' WHERE idparameter = 'first_discount_type'" );
	DBUtil::query( "UPDATE parameter_admin SET paramvalue = '" . $_POST[ "first_discount_value" ] . "' WHERE idparameter = 'first_discount_value'" );
	DBUtil::query( "UPDATE parameter_admin SET paramvalue = '" . $_POST[ "sponsoring_gift_token_model" ] . "' WHERE idparameter = 'sponsoring_gift_token_model'" );
	
}

//Type de remise
$rs = DBUtil::query( "SELECT * FROM parameter_admin WHERE idparameter = 'first_discount_type'" );

if( $rs === false )
	die( "Impossible de récupérer les valeurs des paramètres" );

$discountType = $rs->fields( "paramvalue" ) != "" ? $rs->fields( "paramvalue" ) : "amount";

if( $discountType == "amount" ){
	
	$innerValue = "Montant de la remise";
	$symbol = "¤";
	
}else{
	
	$innerValue = "Pourcentage de remise";
	$symbol = "%";
	
}

//Valeur de la remise
$rs = DBUtil::query( "SELECT * FROM parameter_admin WHERE idparameter = 'first_discount_value'" );

if( $rs === false )
	die( "Impossible de récupérer les valeurs des paramètres" );

$discountValue = $rs->fields( "paramvalue" ) != "" ? $rs->fields( "paramvalue" ) : "";


//Chèque de parrainage
$rs = DBUtil::query( "SELECT * FROM parameter_admin WHERE idparameter = 'sponsoring_gift_token_model'" );

if( $rs === false )
	die( "Impossible de récupérer les valeurs des paramètres" );

$sponsorshipToken = $rs->fields( "paramvalue" ) != "" ? $rs->fields( "paramvalue" ) : "1";

?> 
<script type="text/javascript">
<!--

function changeDiscountType( element ){
	
	var type = element.options[ element.selectedIndex ].value;
	var value = document.getElementById( "innerDiscountValue" );
	
	if( type == "amount" ){
		
		document.getElementById( "innerDiscountValueType" ).innerHTML = "Montant de la remise";
		document.getElementById( "innerDiscountValueSymbol" ).innerHTML = "¤";
		
	}else{
		
		document.getElementById( "innerDiscountValueType" ).innerHTML = "Pourcentage de remise";
		document.getElementById( "innerDiscountValueSymbol" ).innerHTML = "%";
		
	}
	
}

-->
</script>
<div id="globalMainContent">
	<div class="mainContent">
		<div class="topRight"></div><div class="topLeft"></div>
			<div class="content">
				<div class="headTitle">
					<p class="title"><?php echo $Title ?></p>
				</div>
				<div class="subContent">
					<form method="post" action="<?php echo $GLOBAL_START_URL . $_SERVER[ "PHP_SELF" ] ?>">
						<div class="subTitleContainer" style="margin-top:10px;">
							<p class="subTitle">Remise sur la première commande</p>
						</div>
						<div class="tableContainer">
							<table class="dataTable">
								<tr>
									<th style="width:30%;" class="filledCell">Type de remise</th>
									<td>
										<select name="first_discount_type" onchange="changeDiscountType( this );">
											<option value="amount"<?php echo $discountType == "amount" ? " selected=\"selected\"" : "" ?>>Montant</option>
											<option value="rate"<?php echo $discountType == "rate" ? " selected=\"selected\"" : "" ?>>Pourcentage de réduction</option>
										</select>
									</td>
								</tr>
								<tr>
									<th style="width:30%;" class="filledCell"><span id="innerDiscountValueType"><?php echo $innerValue ?></span></th>
									<td>
										<input name="first_discount_value" value="<?php echo $discountValue ?>" class="textInput" style="text-align:right; width:40px;" />
										<span id="innerDiscountValueSymbol"><?php echo $symbol ?></span>
									</td>
								</tr>
							</table>
						</div>
						
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<?php include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); ?>