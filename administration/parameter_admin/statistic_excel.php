<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage logo dans fichier excel des reporting
 */
	
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

$Title = Dictionnary::translate( "title_statistic_excel" );
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------
$TableName='parameter_admin';

//mise à jour du logo

if( isset( $_FILES[ "UpdateLogo" ] ) && is_uploaded_file( $_FILES[ "UpdateLogo" ][ "tmp_name" ] ) ){

	//vérifier le format de l'image
	
	/*1 = GIF  , 2 = JPG  , 3 = PNG  , 4 = SWF  , 5 = PSD  , 6 = BMP  , 
	7 = TIFF  (Ordre des octets Intel), 8 = TIFF  (Ordre des octets Motorola), 9 = JPC  , 
	10 = JP2  , 11 = JPX  , 12 = JB2  , 13 = SWC  , 14 = IFF*/

	$imageInfos = getImageSize( $_FILES[ "UpdateLogo" ][ "tmp_name" ] );
	if( $imageInfos[ 2 ] != 2 ){
		
		?>
		<br /><span class="msg"><?php Dictionnary::translate("gest_com_bad_format_image") ; ?></span>
		<?php	
		
	}
	
	//copier le fichier téléchargé
	
	$dest = "$GLOBAL_START_PATH/www/img/logo_stat.jpg";
	
	if( file_exists( $dest ) )
		@unlink( $dest );
		
	if( !move_uploaded_file( $_FILES[ "UpdateLogo" ][ "tmp_name" ], $dest ) )
        echo "<span class=\"msg\">". Dictionnary::translate( "upload_error" ) . "</span>";
	else{
		
		@chmod( $dest, 0755 );
		
		$rs = DBUtil::getConnection()->Execute( "UPDATE parameter_admin SET paramvalue = 'www/img/logo_stat.jpg' WHERE idparameter LIKE 'logo_stat' LIMIT 1" );
		if( $rs === false )
			die( "Impossible de mettre à jour le logo pour les stat" );
			
	}
	
}

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']))
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?> 

<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
				<form name="myform" method="POST" action="<?php echo $ScriptName ?>" enctype="multipart/form-data">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
		            	<table class="dataTable">
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate('logo_stat') ; ?> :
								<br><br>
								L'image doit se nommer "logo_stat.jpg"								
								</th>
								<td>
									<?php
										if( file_exists( "$GLOBAL_START_PATH/www/img/logo_stat.jpg" ) ){
									?>
										<img src="<?php echo $GLOBAL_START_URL ?>/www/img/logo_stat.jpg" alt="" />
										<?php
									}
										?>
									<input type="file" name="UpdateLogo" value="" />
								</td>
							</tr><tr>	
								<th class="filledCell"><?php echo Dictionnary::translate('titleXLSstat') ; ?> :</th>
								<td><input class="textInput" type=text size=30 name='titleXLSstat' value="<?php echo $Parameter['titleXLSstat'] ; ?>"></td>
							</tr>
						</table>
<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <!--<div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content"></div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            	<p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
                <div class="subTitleContainer">
                	<p class="subTitle">Commentaire client</p>
                </div>
                <p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>-->
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>
