<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage des couleurs des statistiques
 */

include_once( "../../objects/classes.php" );
include_once( "../../catalog/drawlift.php" );

$Title = "Choix des couleurs pour les graphiques.";
$filename = "$GLOBAL_START_PATH/statistics/colors.init.htm";
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );
echo '<script src="'.$GLOBAL_START_URL.'/js/colorPicker/ifx.js" type="text/javascript"></script>'."\n";
echo '<script src="'.$GLOBAL_START_URL.'/js/colorPicker/idrop.js" type="text/javascript"></script>'."\n";
echo '<script src="'.$GLOBAL_START_URL.'/js/colorPicker/idrag.js" type="text/javascript"></script>'."\n";
echo '<script src="'.$GLOBAL_START_URL.'/js/colorPicker/iutil.js" type="text/javascript"></script>'."\n";
echo '<script src="'.$GLOBAL_START_URL.'/js/colorPicker/islider.js" type="text/javascript"></script>'."\n";
echo '<script src="'.$GLOBAL_START_URL.'/js/colorPicker/color_picker.js" type="text/javascript"></script>'."\n";
if(count($_POST)) {
		
	$filestream = '';
	
	foreach($_POST as $key => $value)
	{
		if(substr($key,0,5) == 'color' && isset($value)) {
			$filestream .= '#'.$value.',';
		}
	}		
	$length = strlen($filestream);
	$filestream = substr($filestream, 0, $length-1);
	if($length > 0)
	{
		$fp = fopen($filename,"w"); // ouverture du fichier en écriture
		fwrite($fp, $filestream);
		fclose($fp);
	}	
}


// Paramétrage couleurs --------------------------------------------------------

	$camcolor = '';
	if (file_exists($filename))
	{
		ob_start();
		readfile ($filename); 
		$filecolors = ob_get_contents();
		ob_end_clean();
    	$camcolor = explode(",",$filecolors);
    }

?> 

<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
				<form name="myform" method="POST" action="" enctype="multipart/form-data">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
		            	<table class="dataTable">
		            		<tr>
								<th style="width: 30px;">Ordre</th>
								<th style="width: 100px;">Aperçu</th>
								<th>Valeur</th>
								<th style="width: 30px;">Ordre</th>
								<th style="width: 100px;">Aperçu</th>
								<th>Valeur</th>
				 			</tr>
<?php
		$middle = count($camcolor)/2;
		
		for($i=0; $i<$middle; $i++)
		{
			$j = $i+$middle;
			echo '<tr>
					<td style="text-align: center">'.($i+1).'</th>
					<td style="background-color: '.$camcolor[$i].'">&nbsp;</td>
					<td>
						<a href="javascript:void(0);" rel="colorpicker&objcode=color'.$i.'&objshow=myshowcolor&showrgb=1" style="text-decoration:none; display: inline;" >
						<div id="myshowcolor">
							<input id="color'.$i.'" name="color'.$i.'" type="text" value="'.substr($camcolor[$i],1).'" readonly style="width: 60px;"/>
						</div>
						</a>
					</td>
					<td style="text-align: center">'.($j+1).'</th> 
					<td style="background-color: '.$camcolor[$j].'">&nbsp;</td>
					<td> 
						<a href="javascript:void(0);" rel="colorpicker&objcode=color'.$j.'&objshow=myshowcolor&showrgb=1" style="text-decoration:none; display: inline;" >
						<div id="myshowcolor">
							<input id="color'.$j.'" name="color'.$j.'" type="text" value="'.substr($camcolor[$j],1).'" readonly style="width: 60px;"/>
						</div>
						</a>
					</td> 
				  </tr>';
		}
?>				   
						</table>
					</div>
					<input type="submit" class="blueButton" name="Modify" value="Enregistrer" style="float:right;" />
				</form>	
			  <div class="clear"></div>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
<script language="Javascript">

	function myokfunc(){
		alert("This is my custom function which is launched after setting the color");
	}

	//init colorpicker:
	$(document).ready(
		function()
		{
			$.ColorPicker.init();
		}
	);

</script>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>
