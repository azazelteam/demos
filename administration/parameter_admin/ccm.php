<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage CCM Credit Mutuel
 */
	
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "../../catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------
$TableName='parameter_cat';

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x'])  )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?> 



<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Paiement sécurisé Crédit Mutuel</p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
		            	<table class="dataTable">
							<tr>
								<th class="filledCell">CMCIC version :</th>
								<td style="text-align:right;"><input class="textInput" type=text size=60 name='CMCIC_version' value="<?php echo $Parameter['CMCIC_version'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('CCM_return_code') ; ?> :</th>
								<td style="text-align:right;"><input class="textInput" type=text size=30 name='CCM_return_code' value="<?php echo $Parameter['CCM_return_code'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('CCM_tracker_mail') ; ?> :</th>
								<td style="text-align:right;"><input class="textInput" type=text size=60 name='CCM_tracker_mail' value="<?php echo $Parameter['CCM_tracker_mail'] ; ?>"></td>
							</tr>
		  				</table>
						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <!--<div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content"></div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            	<p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
                <div class="subTitleContainer">
                	<p class="subTitle">Commentaire client</p>
                </div>
                <p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>-->
</div>


<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>
