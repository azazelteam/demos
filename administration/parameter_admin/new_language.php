<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage des langues
 */

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

/* ---------------------------------------------------------------------------------------------- */
/* enregistrement nouveau commentaire */
function GetImageExtension($imagetype)
     {
       if(empty($imagetype)) return false;
       switch($imagetype)
       {
           case 'image/bmp': return '.bmp';
           case 'image/gif': return '.gif';
           case 'image/jpeg': return '.jpg';
           case 'image/png': return '.png';
           default: return false;
       }
     }

if( isset( $_REQUEST[ "idproduct_comment" ] ) && isset( $_REQUEST[ "validate" ] ) ){

    $ret = DBUtil::query( "UPDATE product_comment SET accept_admin = '" . intval( $_REQUEST[ "validate" ] ) . "' WHERE idproduct_comment = '" . intval( $_REQUEST[ "idproduct_comment" ] ) . "' LIMIT 1" );

    if( isset( $_REQUEST[ "referer" ] ) && $_REQUEST[ "referer" ] == "mail" )//depuis le courriel
        header( "Location: " . URLFactory::getHostURL() . "/product_management/catalog/cat_product.php?idproduct=" . intval( $_REQUEST[ "idproduct" ] ) . "&tab=0" );

    exit( $ret === false ? "0" : "1" );

}
if( isset($_POST['Modify']) )
//Chargement des valeurs
{
    $name=$_POST['name'];
    $designation=$_POST['designation'];

if (!empty($_FILES["flag"]["name"])){
    $temp_name=$_FILES["flag"]["tmp_name"];
    $imgtype=$_FILES["flag"]["type"];
    $ext= GetImageExtension($imgtype);

    $imagename=$designation."-".date("y-m-d").$ext;
    $target_path = "../../images/flags/".$imagename;

if(move_uploaded_file($temp_name, $target_path)) {

   $res = DBUtil::getConnection()->Execute("INSERT INTO language (name,designation,flag)values ('$name','$designation','$imagename')"); 
}else{
   exit("Error While uploading image on the server");
}
}}


   
//$n = $res->RecordCount();


/* ---------------------------------------------------------------------------------------------- */
/* suppression d'un commentaire */

if( isset( $_REQUEST[ "id_lang" ] ) && isset( $_REQUEST[ "delete" ] ) ){
    alert("here");

    $ret = DBUtil::query( "DELETE FROM language WHERE id_lang = '" . intval( $_REQUEST[ "id_lang" ] ) . "' LIMIT 1" );

    exit( $ret === false ? "0" : "1" );

}

if( isset( $_REQUEST[ "idproduct_comment" ] ) && isset( $_REQUEST[ "update" ] ) ){

    $ret = DBUtil::query( "UPDATE product_comment SET product_comment = '" .  $_REQUEST[ "comment" ]  . "' WHERE idproduct_comment = '" . intval( $_REQUEST[ "idproduct_comment" ] ) . "' LIMIT 1" );

    if( isset( $_REQUEST[ "referer" ] ) && $_REQUEST[ "referer" ] == "mail" )//depuis le courriel
        header( "Location: " . URLFactory::getHostURL() . "/product_management/catalog/cat_product.php?idproduct=" . intval( $_REQUEST[ "idproduct" ] ) . "&tab=0" );

    exit( $ret === false ? "0" : "1" );

}
/* ---------------------------------------------------------------------------------------------- */

$rs =& DBUtil::query( "SELECT * FROM language  ORDER BY  id_lang DESC" );

if( !$rs->RecordCount() ){

    ?>
    <p style="text-align:center;">Aucune langue trouvée</p>
    <?php

    exit();

}

?>
<style type="text/css">

    #CustomerComments{

        padding:0px;

    }

    #CustomerComments{

        list-style-type:none;
        margin:10px;

    }

    #CustomerComments li{

        width:70%;
        padding:10px;
        border:1px solid #E7E7E7;
        margin:1px;
        background-color:#FFFFFF;

    }
    #CustomerComments li .recycle{

        position:absolute;
        margin-left:-40px;

    }

    #CustomerComments li .available{ float:right; margin-left:10px; cursor:pointer; }

</style>
<script type="text/javascript">
/* <![CDATA[ */

    function validateComment( idproduct_comment ){

        var validate = $( "#accept_admin" + idproduct_comment ).val() == 1 ? 0 : 1;

        $.ajax({

            url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_comments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&idproduct_comment=" + idproduct_comment + "&validate=" + validate,
            async: true,
            cache: false,
            type: "GET",
            data: "",
            error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
            success: function( responseText ){

                if( responseText != "1" )
                    alert(responseText );
                else{

                    $( "#ValidateIcon" + idproduct_comment ).attr( "src", validate ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif" );
                    $( "#accept_admin" + idproduct_comment ).val( validate );
                    $( "#Comment" + idproduct_comment ).css( "background-color", validate ? "#FFFFFF" : "#FAFAFA" );

                    $.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
                    $.blockUI.defaults.css.fontSize = '12px';
                    $.growlUI( '', 'Modifications enregistrées' );

                }

            }

        });

    }

    function deleteLang(idlang){

        $.ajax({

            url: "<?php echo $GLOBAL_START_URL; ?>/administration/parameter_admin/new_language.php?id_lang=" + idlang + "&delete",
            async: true,
            cache: false,
            type: "GET",
            data: "",
            error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
            success: function( responseText ){

                if( responseText != "1" )
                    alert(responseText );
                else{

                    $( "#lang" + idlang ).remove();

                    $.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
                    $.blockUI.defaults.css.fontSize = '12px';
                    $.growlUI( '', 'Modifications enregistrées' );

                }

            }

        });

    }
function updateComment( idlang){

        var comment = $( "#comment" + idproduct_comment ).text();
        $.ajax({

            url: "<?php echo $GLOBAL_START_URL; ?>/templates/product_management/product_comments.php?idproduct=<?php echo intval( $_GET[ "idproduct" ] ); ?>&idproduct_comment=" + idproduct_comment +"&comment="+comment+ "&update",
            async: true,
            cache: false,
            type: "GET",
            data: "",
            error : function( XMLHttpRequest, textStatus, errorThrown ){ alert( textStatus ); },
            success: function( responseText ){

                if( responseText != "1" )
                    alert(responseText );
                else{

                    //$( "#ValidateIcon" + idproduct_comment ).attr( "src", validate ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif" );
                    //$( "#accept_admin" + idproduct_comment ).val( validate );
                    //$( "#Comment" + idproduct_comment ).css( "background-color", validate ? "#FFFFFF" : "#FAFAFA" );
                //$.('btn_update'+ idproduct_comment).css.display="none";
                    $('#btn_update'+ idproduct_comment).css("display","none");
                    $('#comment'+ idproduct_comment).css("height","");
                    $('#comment'+ idproduct_comment).css("width","");
                    $('#comment'+ idproduct_comment).css("position","");
                    $('#comment'+ idproduct_comment).css("padding","");
                    $.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
                    $.blockUI.defaults.css.fontSize = '12px';
                    $.growlUI( '', 'Modifications enregistrées' );

                }

            }

        });

    }
    function enableEdit(idproduct_comment ){

        //var comment = $( "#comment" + idproduct_comment ).val();
    var editorBtn = document.getElementById('btn_update'+ idproduct_comment);
    var element = document.getElementById('comment'+ idproduct_comment);

        element.contentEditable = 'true';
        //editorBtn.innerHTML = 'Enable Editing';
      document.getElementById('btn_update'+ idproduct_comment).style.display="block";

    element.style.position="relative";
    element.style.height="100px";
    element.style.width="100%";
    element.style.padding="0px";


}
/* ]]> */
</script>
<div id="globalMainContent">
<div class="mainContent borderProduct">
 <div class="subContent">
                <!-- formulaire -->
                <form id="devisSearchForm" name="myform" method="post"  enctype="multipart/form-data" action="<?php echo $ScriptName ?>">
                    <div class="tableContainer" style="width:200px; padding-top:14px; margin:auto;">
                        Ajouter une nouvelle langue:
                                <br />
                        <table class="dataTable">
                            
                                
                            
                           
                            <tr>
                                <th style="width:30%;" class="filledCell">Langue</th>
                                <td style="text-align:right; width:70%;"><input class="textInput" name="name" value="" required></td>
                                

                            </tr>
                             <tr>
                                <th style="width:30%;" class="filledCell">D&eacute;signation</th>
                                <td style="text-align:right; width:70%;"><input class="textInput" name="designation" value="" required></td>
                                

                            </tr>
                             <tr>
                                <th style="width:30%;" class="filledCell">Drapeau</th>
                               <td style="text-align:right; width:70%;"><input class="fileInput" type="file" name="flag" value="" required></td>

                                

                            </tr>
                           
                        </table>
                        <input type="submit" class="blueButton" name="Modify" value="Ajouter" style="float:right; margin-top:7px;" />
                    </div>
                    <div class="clear"></div>
                </form>
            </div>
<!--
<div class="subTitleContainer" style="cursor:move;">
    <p class="subTitle">Langues disponibles</p>
</div>
<div style="padding-left:50px;">



    <ul id="CustomerComments">
    <?php

        while( !$rs->EOF() ){
        $name=$rs->fields( "name" );
        $idlang=$rs->fields( "id_lang" );
        $designation=$rs->fields( "designation" );

        //$rs1 =& DBUtil::query( "SELECT firstname, lastname,company, phonenumber FROM contact c inner join buyer b on c.`idbuyer`=b.`idbuyer`  WHERE c.`idbuyer` = '".$idbuyer."' AND c.`idcontact` = '".$idcontact."' LIMIT 1" );

            ?>
            <li id="lang<?php echo $rs->fields( "id_lang" ); ?>" style="background-color:#FFFFFF">

                <span class="available">
                  <!--  <img id="ValidateIcon<?php echo $rs->fields( "id_lang" ); ?>" onclick="validateComment( <?php echo $rs->fields( "id_lang" ); ?> );" src="<?php echo $GLOBAL_START_URL; ?>/images/<?php echo $rs->fields( "accept_admin" ) ? "visible.gif" : "hidden.gif"; ?>" alt="" />-->
                  <!--  <img id="UpdateIcon" onclick="enableEdit(<?php echo $rs->fields( "idproduct_comment" ); ?>);" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/icons-edit.png" alt="" />-->
               <!-- </span>
                <img class="recycle" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" alt="" style="cursor:pointer;" onclick="deleteLang( <?php echo $rs->fields( "id_lang" ); ?> );" />
                <p>
                        <i><strong><?php echo htmlentities( $rs->fields( "name" ) ) ?></strong><br></i>
                        <i><?php echo $designation  ?></i>
                    </p>
                <div>
                  <!--  
                    <div id="Lang<?php echo $rs->fields( "id_lang" ); ?>" style="cursor:pointer;  overflow:auto;" >
                        <?php echo htmlentities( $rs->fields( "name" ) ); ?>

                    </div>
                    <input type="button" id="btn_update<?php echo $rs->fields( "id_lang" ); ?>" style="display:none;"  onclick="updateComment( <?php echo $rs->fields( "id_lang" ); ?> );" value="Modifier">
                </div>-->
             <!--   <br style="clear:both;" />
            </li>
            <?php

            $rs->MoveNext();

        }

    ?>
    </ul>
</div>
</div>
-->
</div>

<?php
//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>