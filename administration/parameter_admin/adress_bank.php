<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion coordonnées bancaires de la société
 */
	
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
$Title = Dictionnary::translate( "admin_adress_bank" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------
$TableName='parameter_admin';

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x'])  )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = Util::doNothing($res->fields['paramvalue']);
	$res->MoveNext();
}

//Formulaire

?> 

<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
		            	<table class="dataTable">
							<tr>
								<th style="width:20%;" class="filledCell"><?php echo Dictionnary::translate('ad_bank1') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_bank1' value="<?php echo $Parameter['ad_bank1'] ; ?>"></td>
								<th style="width:20%;" class="filledCell"><?php echo Dictionnary::translate('ad_bank2') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_bank2' value="<?php echo $Parameter['ad_bank2'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_bank3') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_bank3' value="<?php echo $Parameter['ad_bank3'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_bank4') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_bank4' value="<?php echo $Parameter['ad_bank4'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_bank5') ; ?> :</th>
								<td colspan="3"><input class="textInput" size=30 name='ad_bank5' value="<?php echo $Parameter['ad_bank5'] ; ?>"></td>
							</tr>
							</table>
							<br />
							<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate('adress_factor') ; ?></p>
				        	</div>
							<table class="dataTable">
							<tr>
								<th style="width:20%;" class="filledCell"><?php echo Dictionnary::translate('ad_bank1') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_factor1' value="<?php echo $Parameter['ad_factor1'] ; ?>"></td>
								<th style="width:20%;" class="filledCell"><?php echo Dictionnary::translate('ad_bank2') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_factor2' value="<?php echo $Parameter['ad_factor2'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_bank3') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_factor3' value="<?php echo $Parameter['ad_factor3'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_bank4') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_factor4' value="<?php echo $Parameter['ad_factor4'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_bank5') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_factor5' value="<?php echo $Parameter['ad_factor5'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('ad_factor_url') ; ?> :</th>
								<td><input class="textInput" size=30 name='ad_factor_url' value="<?php echo $Parameter['ad_factor_url'] ; ?>"></td>
							</tr>
							</table>
							
							<br />
							<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate('adress_payment') ; ?></p>
				        	</div>
							<table class="dataTable">
							<tr>
								<th style="width:20%;" class="filledCell"><?php echo Dictionnary::translate('b_contact_email') ; ?> :</th>
								<td><input class="textInput" size=30 name='b_contact_email' value="<?php echo $Parameter['b_contact_email'] ; ?>"></td>
								<th style="width:20%;" class="filledCell"><?php echo Dictionnary::translate('b_contact_fax') ; ?> :</th>
								<td><input class="textInput" size=30 name='b_contact_fax' value="<?php echo $Parameter['b_contact_fax'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('b_contact_firstname') ; ?> :</th>
								<td><input class="textInput" size=30 name='b_contact_firstname' value="<?php echo $Parameter['b_contact_firstname'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('b_contact_lastname') ; ?> :</th>
								<td><input class="textInput" size=30 name='b_contact_lastname' value="<?php echo $Parameter['b_contact_lastname'] ; ?>"></td>
							</tr>
						</table>
						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <!--<div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content"></div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            	<p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
                <div class="subTitleContainer">
                	<p class="subTitle">Commentaire client</p>
                </div>
                <p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>-->
</div>


<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>
