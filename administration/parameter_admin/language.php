<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage langues
 */

//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

$Title = "Gestion des langues" ;

//-----------------------------------------------------------------------------

$TableName="title_param_admin";

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']))
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}
//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like 'lang%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$IdParameter[$i] = $res->fields['idparameter'];
	$Parameter[$i] = $res->fields['paramvalue'];
	$res->MoveNext();
}
?>


<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer" style="width:200px; padding-top:14px; margin:auto;">
		            	<table class="dataTable">
							<tr>
								<th style="width:30%;" class="filledCell"><?php echo Dictionnary::translate("language") ; ?> 1</th>
								<td style="text-align:right; width:70%;"><input class="textInput" name=<?php echo $IdParameter[0]; ?> value=<?php echo $Parameter[0] ; ?>></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("language") ; ?> 2</th>
								<td style="text-align:right;"><input class="textInput" name=<?php echo $IdParameter[1]; ?> value=<?php echo $Parameter[1] ; ?>></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("language") ; ?> 3</th>
								<td style="text-align:right;"><input class="textInput" name=<?php echo $IdParameter[2]; ?> value=<?php echo $Parameter[2] ; ?>></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("language") ; ?> 4</th>
								<td style="text-align:right;"><input class="textInput" name=<?php echo $IdParameter[3]; ?> value=<?php echo $Parameter[3] ; ?>></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("language") ; ?> 5</th>
								<td style="text-align:right;"><input class="textInput" name=<?php echo $IdParameter[4]; ?> value=<?php echo $Parameter[4] ; ?>></td>
							</tr>
						</table>
						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <!--<div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content"></div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            	<p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
                <div class="subTitleContainer">
                	<p class="subTitle">Commentaire client</p>
                </div>
                <p>
                	QuÕest-ce quÕon mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de lŽgumes, cookies 
                    moelleux ananas-coco. 
                </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>-->
</div>
<?php
//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>

