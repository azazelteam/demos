<?php

 /**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonction qui permet de faire un update sur une table de forme idparameter, paramvalue
 */


$langue=User::getInstance()->getLang();

function FAdminUpdateRecords($TableName,$IdParameter,$Tab)
{
	global $base;
	for($i=0 ; $i < count($IdParameter) ; $i++) {
	$k = $IdParameter[$i];
    $rs = DBUtil::query("UPDATE $TableName SET paramvalue='".$Tab[$k]."' WHERE idparameter='$k'");
	}
}

function FadminLoadValues($TableName,$IdParameter) {
	global $base;
	$result = array();
	for($i=0 ; $i < count($IdParameter) ; $i++) {
	$rs = DBUtil::query("SELECT paramvalue FROM $TableName WHERE idparameter='$IdParameter[$i]'");
	if( !$rs->EOF() ) $result[]=$rs->fields['paramvalue'];
	else $result[]='';
	}
	return $result;
}

function DisplayChargeLift($SelectName,$Select)
{
	if($Select=="buyer")
	{
		$Selected1="selected";
		$Selected2="";
	}
	else
	{
		$Selected1="";
		$Selected2="selected";
	}
	
	echo "<select name=\"$SelectName\">\n";
	echo "<option value=\"buyer\" $Selected1 >buyer</option>\n";
	echo "<option value=\"state\" $Selected2 >state</option>\n";
	echo "</select>";
}

function DisplayChoiceLift($SelectName,$Select)
{
	switch($Select)
	{
		case "price" :	$Selected1="selected";
				$Selected2="";
				$Selected3="";
				$Selected4="";
				$Selected5="";									
				break;
		case "weight":	$Selected1="";
				$Selected2="selected";
				$Selected3="";
				$Selected4="";	
				$Selected5="";										
				break;
		case "area"  :	$Selected1="";
				$Selected2="";
				$Selected3="selected";
				$Selected4="";	
				$Selected5="";									
				break;
		case "department"  :	$Selected1="";
				$Selected2="";
				$Selected3="";
				$Selected4="selected";	
				$Selected5="";								
				break;	
		case "area_weight"  :	$Selected1="";
				$Selected2="";
				$Selected3="";
				$Selected4="";				
				$Selected5="selected";					
				break;											
	}
	echo "<select name=\"$SelectName\">\n";
	echo "<option value=\"price\" $Selected1 >Prix</option>\n";
	echo "<option value=\"weight\" $Selected2 >Poids</option>\n";
	echo "<option value=\"area\" $Selected3 >Zone géographique</option>\n";
	echo "<option value=\"department\" $Selected4 >Département</option>\n";	
	echo "<option value=\"area_weight\" $Selected4 >Zone géographique + Prix/poids</option>\n";		
	echo "</select>\n";
}

function DisplayDeviceLift($SelectName,$Select)
{
	global $langue,$base;
	echo "<select name=\"$SelectName\">\n";
	$rs= DBUtil::query("select idcurrency,name$langue from currency");
	if($rs->RecordCount())
	{

		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$IdCurrency=$rs->fields["idcurrency"];
			$DeviceName=$rs->fields["name$langue"];
			if($IdCurrency==$Select)
				$Selected="selected";
			else
				$Selected="";
			echo "<option value=$IdCurrency $Selected>$IdCurrency - $DeviceName</option>\n";
			$rs->MoveNext();
		}
	}	
	echo "</select>";
}

function DisplayStateLift($SelectName,$Select)
{
	global $langue,$base;
	
	echo "<select name=\"$SelectName\">\n";
	$rs = DBUtil::query("select idstate,name$langue from state");
	if($rs->RecordCount())
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$IdState=$rs->fields["idstate"];
			$StateName=$rs->fields["name$langue"];
			if($StateName==$Select)
				$Selected="selected";
			else
				$Selected="";
			echo "<option value=\"". $StateName."\" $Selected>$IdState - ". $StateName."</option>\n";
			$rs->MoveNext();
		}
	}	
	echo "</select>";
}

function DisplayPaymentdelayLift($SelectName,$Select)
{
	global $langue,$base;
	
	echo "<select name=\"$SelectName\">\n";
	$rs = DBUtil::query("select idpayment_delay,payment_delay$langue from payment_delay");
	if($rs->RecordCount())
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$IdState=$rs->fields["idpayment_delay"];
			$StateName=$rs->fields["payment_delay$langue"];
			if($StateName==$Select)
				$Selected="selected";
			else
				$Selected="";
			echo "<option value=\"". $StateName."\" $Selected>$IdState - ". $StateName."</option>\n";
			$rs->MoveNext();
		}
	}	
	echo "</select>";
}

function DisplayPaymentLift($SelectName,$Select)
{
	global $langue,$base;
	
	echo "<select name=\"$SelectName\">\n";
	$rs = DBUtil::query("select idpayment,name$langue from payment");
	if($rs->RecordCount())
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$IdState=$rs->fields["idpayment"];
			$StateName=$rs->fields["name$langue"];
			if($StateName==$Select)
				$Selected="selected";
			else
				$Selected="";
			echo "<option value=\"". $StateName."\" $Selected>$IdState - ". $StateName."</option>\n";
			$rs->MoveNext();
		}
	}	
	echo "</select>";
}

function DisplayVatLift($SelectName,$Select)
{
	global $langue,$base;
	
	echo "<select name=\"$SelectName\">\n";
	$rs = DBUtil::query("select idtva,tva from tva");
	if($rs->RecordCount())
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$IdVAT=$rs->fields["idtva"];
			$VAT=$rs->fields["tva"];
			if($VAT==$Select)
				$Selected="selected";
			else
				$Selected="";
			echo "<option value=\"$VAT\" $Selected>$IdVAT - $VAT</option>\n";
			$rs->MoveNext();
		}
	}	
	echo "</select>";
}


?>