<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire indexation des documents
 */

// Paramétrage de base 
$Int_LimitP = 5; // Limite de produits a afficher sur la page, au moins 1
$pagetitle = 'code_indexation';
$Str_TableName = 'code_indexation';
$KeyName = 'Idcode_indexation';

// Field columns that appear after searching in ShowRecordsBy	
$ArrayFieldsColumns2Show[0] = 'Idcode_indexation';
$ArrayFieldsColumns2Show[]  = 'table';
  
// Pas d'affichage du bandeau d'en-tête de page si c'est une popup
	include('../../formbase/stdformbase.php');

?>