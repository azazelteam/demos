<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage reporting
 */
	
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "../../catalog/drawlift.php" );

$Title = Dictionnary::translate( "title_admin_statistic" );
include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------
$TableName='parameter_admin';

//mise à jour du logo

if( isset( $_FILES[ "UpdateLogo" ] ) && is_uploaded_file( $_FILES[ "UpdateLogo" ][ "tmp_name" ] ) ){

	//vérifier le format de l'image
	
	/*1 = GIF  , 2 = JPG  , 3 = PNG  , 4 = SWF  , 5 = PSD  , 6 = BMP  , 
	7 = TIFF  (Ordre des octets Intel), 8 = TIFF  (Ordre des octets Motorola), 9 = JPC  , 
	10 = JP2  , 11 = JPX  , 12 = JB2  , 13 = SWC  , 14 = IFF*/

	$imageInfos = getImageSize( $_FILES[ "UpdateLogo" ][ "tmp_name" ] );
	if( $imageInfos[ 2 ] != 2 ){
		
		?>
		<br /><span class="msg"><?php Dictionnary::translate("gest_com_bad_format_image") ; ?></span>
		<?php	
		
	}
	
	//copier le fichier téléchargé
	
	$dest = "$GLOBAL_START_PATH/www/img/logo_stat.jpg";
	
	if( file_exists( $dest ) )
		@unlink( $dest );
		
	if( !move_uploaded_file( $_FILES[ "UpdateLogo" ][ "tmp_name" ], $dest ) )
        echo "<span class=\"msg\">". Dictionnary::translate( "upload_error" ) . "</span>";
	else{
		
		@chmod( $dest, 0755 );
		
		$rs = DBUtil::getConnection()->Execute( "UPDATE parameter_admin SET paramvalue = 'www/img/logo_stat.jpg' WHERE idparameter LIKE 'logo_stat' LIMIT 1" );
		if( $rs === false )
			die( "Impossible de mettre à jour le logo pour les stat" );
			
	}
	
}

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']))
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?> 

<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
				<form name="myform" method="POST" action="<?php echo $ScriptName ?>" enctype="multipart/form-data">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
		            	<table class="dataTable">
							<tr>
								<th style="padding-left:4px;"><?php echo Dictionnary::translate('use_order_supplier') ; ?> :</th>
								<td><?php echo DrawLiftParameters('use_order_supplier','Utiliser','Ne pas utiliser');?></td>
							</tr><tr>
								<th style="padding-left:4px;"><?php echo Dictionnary::translate('use_big_datas') ; ?> :</th>
								<td><?php echo DrawLiftParameters('use_big_datas','Utiliser','Ne pas utiliser');?></td>
							</tr><tr>
								<th style="padding-left:4px;"><?php echo Dictionnary::translate('pie_min_percent') ; ?> :</th>
								<td><input class="textInput" type=text size=4 name='pie_min_percent' value="<?php echo $Parameter['pie_min_percent'] ; ?>"></td>
							</tr>
						</table>
					</div>
					<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right;" />
				</form>
				<div class="clear"></div>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>
