<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire de familles d'utilisateurs
 */

// Paramétrage de base 
$Int_LimitP = 5; // Limite de produits a afficher sur la page, au moins 1
$pagetitle = 'title_catalog_right';
$Str_TableName = 'catalog_right';
$KeyName = 'idcatalog_right';

// Field columns that appear after searching in ShowRecordsBy	
$ArrayFieldsColumns2Show[0] = 'idcatalog_right';
$ArrayFieldsColumns2Show[]  = 'catalog_right_1';
  
// Pas d'affichage du bandeau d'en-tête de page si c'est une popup
	include('../../formbase/stdformbase.php');

?>