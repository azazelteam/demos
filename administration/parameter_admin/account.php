<?php

 /**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des paramètres de la comptabilité
 */
 

//------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../../config/init.php" );
include_once( dirname( __FILE__ ) . "/../../objects/Util.php" );

include_once( dirname( __FILE__ ) . "/../../script/global.fct.php" );

//------------------------------------------------------------------------------------------
/* ajax pour ajouter un code journal */

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "addDiaryCode" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "newCode" ] ) || empty( $_GET[ "newCode" ] ) )
		exit( "Aucun code journal renseigné" );
	
	if( strlen( $_GET[ "newCode" ] ) > 4 )
		exit( "Le code journal ne doit pas faire plus de 4 caractères" );
	
	if( DBUtil::query( "SELECT * FROM diary_code WHERE diary_code = '" . $_GET[ "newCode" ] . "'" )->RecordCount() )
		exit( "Ce code journal existe déjà" );
	
	$rs =& DBUtil::query( "
		INSERT INTO diary_code (
			diary_code,
			lastupdate,
			username
		) VALUES (
			'" . Util::html_escape( $_GET[ "newCode" ] ) . "',
			NOW(),
			'" . User::getInstance()->get( "login" ) . "'
		)" );
	
	if( $rs === false )
		exit( "Impossible d'ajouter le code journal" );
	
	exit( "0" );
	
}

//------------------------------------------------------------------------------------------
/* ajax création d'un nouvel exercice comptable */

if( isset( $_GET[ "newYear" ] ) || isset( $_GET[ "name" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "newYear" ] ) )
		exit( '<strong style="color:#FF0000;">Aucune date de clôture renseignée</strong>' );
	
	if( !isset( $_GET[ "name" ] ) || empty( $_GET[ "name" ] ) || !is_string( $_GET[ "name" ] ) )
		exit( '<strong style="color:#FF0000;">Aucun nom d\'exercice comptable renseigné</strong>' );
	
	if( !preg_match( "/([0-9]{2})-([0-9]{2})-([0-9]{4})/", $_GET[ "newYear" ] ) )
		exit( '<strong style="color:#FF0000;">Format de date erroné</strong>' );
	
	$query = "
		INSERT INTO fiscal_years (
			name,
			start_date,
			closed
		) VALUES (
			'" . Util::html_escape( $_GET[ "name" ] ) . "',
			'" . Util::html_escape( euDate2us( $_GET[ "newYear" ] ) ) . "',
			0
		)";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		exit( '<strong style="color:#FF0000;">Problème survenu lors de la création de l\'exercice</strong>' );
	
	exit( "0" );
	
}

//------------------------------------------------------------------------------------------
/* ajax cloture exercice comptable */

if( isset( $_GET[ "close" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "fiscal_year" ] ) || !intval( $_GET[ "fiscal_year" ] ) )
		exit( "Impossible de trouver l'exercice comptable à clore" );
	
	$time = microtime( true );
	
	$generatedRows = closeFiscalYear( intval( $_GET[ "fiscal_year" ] ) );
	
	$time = microtime( true ) - $time;
	
	exit( "$generatedRows lignes générées en " . round( $time, 2 ) . " secondes" );
	
}

//------------------------------------------------------------------------------------------
/* ajax contenu cloture exercice comptable */

if( isset( $_GET[ "getCloseYearContent" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	getCloseYearContent();
	
	exit();
	
}

//------------------------------------------------------------------------------------------
/* ajax modification de l'ordre des colonnes pour l'export */

if( isset( $_GET[ "sortColumns" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "array" ] ) )
		exit( "Impossible de modifier l'ordre des colonnes" );
	
	$columns = explode( ",", $_GET[ "array" ] );
	
	for( $i = 0 ; $i < count( $columns ) ; $i++ )
		DBUtil::query( "UPDATE account_plan_export SET display_order = " . ( $i + 1 ) . " WHERE idcolumn = " . $columns[ $i ] );
	
	exit();
	
}

//------------------------------------------------------------------------------------------
/* ajax modification de la visibilité des colonnes pour l'export */

if( isset( $_GET[ "columnsVisibility" ] ) ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=utf-8" );
	
	if( !isset( $_GET[ "idrow" ] ) || !intval( $_GET[ "idrow" ] ) )
		exit( "Aucune colone spécifiée" );
	
	if( !isset( $_GET[ "visibility" ] ) )
		exit( "Aucune visibilité spécifiée" );
	
	DBUtil::query( "UPDATE account_plan_export SET displayed = " . intval( $_GET[ "visibility" ] ) . " WHERE idcolumn = " . intval( $_GET[ "idrow" ] ) );
	
	exit();
	
}

//------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/objects/AccountPlan.php" );
include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

//------------------------------------------------------------------------------------------

$Title = "Paramétrage comptabilité";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//------------------------------------------------------------------------------------------
//Modification des paramètres

if( isset( $_POST[ "Modify" ] ) || isset( $_POST[ "Modify_x" ] ) ){
	
	foreach( $_POST as $k => $v )
		DBUtil::query( "UPDATE parameter_admin SET paramvalue = '$v' WHERE idparameter = '$k'" );
	
}

//------------------------------------------------------------------------------------------
//Chargement des valeurs des paramètres

$rs = DBUtil::query( "SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'" );

while( !$rs->EOF ){
	
	$Parameter[ $rs->fields( "idparameter" ) ] = $rs->fields( "paramvalue" );
	
	$rs->MoveNext();
	
}

//------------------------------------------------------------------------------------------
//Récupération de la date de début de l'exercice comptable en cours

$rs =& DBUtil::query( "SELECT MAX( start_date ) AS minDate FROM fiscal_years" );

if( $rs === false )
	trigger_error( "Un problème est survenu lors de la récupération de la date de début de l'année fiscale en cours", E_USER_ERROR );

if( !$rs->RecordCount() )
	$minDate = "2005-01-01";
else
	$minDate = $rs->fields( "minDate" );

list( $year, $month, $day ) = explode( "-", $minDate );

//------------------------------------------------------------------------------------------

?>
<style type="text/css">
<!--
	
	#export-sortable > div{
		clear:left;
		height:30px;
	}
	
	#export-sortable > div img{
		float:left;
		margin:6px 5px 0 5px;
	}
	
	#export-sortable div.sortable-text{
		background-color:white;
		border:1px solid #CBCBCB;
		float:left;
		margin:2px 0;
		padding:5px;
		width:425px;
	}
	
	#export-sortable > div.empty-sortable{
		background-color:#EAEAEA;
		height:30px;
		margin-left:25px;
	}
	
	#export-sortable div.hiddenColumn div{
		opacity:0.5;
	}
	
-->
</style>
<script type="text/javascript">
<!--
	
	/* ----------------------------------------------------------------------------------- */
	
	$(document).ready(function(){
		
		$('#export-sortable').sortable({
			placeholder: 'empty-sortable',
			update: updateColumns
		});
		
		$('div#export-sortable img').click(function(){
			
			var parent = $(this).parent();
			var idrow = parent.attr('id');
			
			if( parent.hasClass('hiddenColumn') ){
				var visibility = 1;
				$(this).attr('src','<?php echo $GLOBAL_START_URL ?>/images/back_office/content/check_icon.png');
				parent.slideUp('normal',moveColumnLast);
			}
			else{
				var visibility = 0;
				$(this).attr('src','<?php echo $GLOBAL_START_URL ?>/images/back_office/content/uncheck_icon.png');
				parent.slideUp('normal',moveColumnLast);
			}
			
			parent.toggleClass('hiddenColumn');
			
			$.ajax({
				type: 'GET',
				url: '<?php echo $GLOBAL_START_URL ?>/administration/parameter_admin/account.php',
				data: 'columnsVisibility&idrow=' + idrow + '&visibility=' + visibility,
				success: function( response ){ if( response != '' ) $.growlUI( '', response ); }
			});
			
		});
		
	});
	
	/* ----------------------------------------------------------------------------------- */
	
	function moveColumnLast(){
		
		var parent = $(this).parent();
		
		if( $(".hiddenColumn").size() ){
			$(this).insertBefore(".hiddenColumn:first").slideDown('normal', function(){
				$('#export-sortable').sortable('refresh');
				updateColumns();
			});
		}
		else{
			$(this).insertAfter(".column:not(.hiddenColumn):last").slideDown('normal', function(){
				$('#export-sortable').sortable('refresh');
				updateColumns();
			});
		}
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	
	function updateColumns(){
		
		var sortedArray = $('#export-sortable').sortable('toArray');
		
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/administration/parameter_admin/account.php',
			data: 'sortColumns&array=' + sortedArray,
			success: function( response ){ if( response != '' ) $.growlUI( '', response ); }
		});
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	
	function addCode(){
		
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/administration/parameter_admin/account.php?action=addDiaryCode&newCode=' + $('#diaryCode').val(),
			success: function(response){
				if( response == "0" ){
					$('#diaryCodes').html( $('#diaryCodes').html() + $('#diaryCode').val() + '<br />' );
					$.growlUI('','Code journal ajouté');
				}else
					$.growlUI('',response);
			}
		});
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	/* Création d'un nouvel exercice comptable                                             */
	/* ----------------------------------------------------------------------------------- */
	
	function createNewYear(){
		
		var name = $('#newYearName').val();
		var date = $('#newYearDate').val();
		
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/administration/parameter_admin/account.php?newYear='+date+'&name='+name,
			error: function(){ $.growlUI( '', 'Un problème est survenu lors de la création du nouvel exercice comptable' ); },
			success: function( response ){
				if( response == "0" )
					$.growlUI( '', 'Nouvel exercice créé avec succès' );
				else
					$.growlUI( '', response );
			}
		});
		
	}
	
	/* ----------------------------------------------------------------------------------- */
	/* Cloture d'un exercice comptable                                                     */
	/* @param string id l'identifiant de l'année fiscale à clore                           */
	/* ----------------------------------------------------------------------------------- */
	
	function closeYear( id ){
		
		$.blockUI({
			message: "Clôture de l'exercice comptable en cours<br />Cela peut prendre plusieurs minutes",
			css: {
				padding: '15px',
				cursor: 'pointer',
				'font-weight': 'bold',
				'font-family': 'Arial, Helvetica, sans-serif',
				'color': '#586065',
				'-webkit-border-radius': '10px',
				'-moz-border-radius': '10px'
			},
			fadeIn: 0, 
			fadeOut: 700
		});
		
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/administration/parameter_admin/account.php?close&fiscal_year=' + id,
			error:	function( XMLHttpRequest, textStatus, errorThrown ){ $.growlUI( '', "Problème lors de la cloture" ); },
		 	success: function( response ){
				$.growlUI( '', response );
			}
		});
		
		$.ajax({
			type: 'GET',
			url: '<?php echo $GLOBAL_START_URL ?>/administration/parameter_admin/account.php?getCloseYearContent',
			error:	function( XMLHttpRequest ){ $.growlUI( '', "Problème lors de la cloture" ); },
		 	success: function( response ){ $('#closeYearContent').html(response); }
		});
		
	}
	
-->
</script>
<div id="globalMainContent">
	<div class="mainContent" style="float:left; width:490px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Clôture d'exercices comptables</p>
			</div>
			<div class="subContent" id="closeYearContent">
				<?php getCloseYearContent(); ?>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<div class="mainContent" style="float:right; width:490px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Nouvel exercice comptable</p>
			</div>
			<div class="subContent">
				<form>
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell">Nom de l'exercice</th>
								<td><input type="text" name="newYearName" id="newYearName" class="textInput" /></td>
							</tr>
							<tr>
								<th class="filledCell">Date de début de l'exercice</th>
								<td>
									<input type="text" id="newYearDate" class="calendarInput" />
									<?php echo DHTMLCalendar::calendar( "newYearDate" ) ?>
									<script type="text/javascript">
									<!--
										
										$(document).ready(function(){
											
											$('#newYearDate').datepicker('option','minDate',new Date(<?php echo $year ?>,<?php echo $month - 1 ?>,<?php echo $day + 1 ?>));
											
										});
										
									-->
									</script>
								</td>
							</tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="button" class="blueButton" value="Créer" onclick="createNewYear();" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<div class="clear"></div>
	
	<div class="mainContent" style="float:left; margin-top:10px; width:490px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Codes journaux</p>
			</div>
			<div class="subContent">
				<form>
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell" style="width:50%;">Codes journaux existants</th>
								<td style="padding:5px; width:50%;" id="diaryCodes">
									<?php
										
										$rs =& DBUtil::query( "SELECT diary_code FROM diary_code ORDER BY diary_code ASC" );
										
										if( $rs == false )
											trigger_error( "Impossible de réupérer la liste des codes comptables", E_USER_ERROR );
										
										while( !$rs->EOF ){
											
											echo $rs->fields( "diary_code" ) . "<br />";
											
											$rs->MoveNext();
											
										}
										
									?>
								</td>
							</tr>
							<tr>
								<th class="filledCell">Nouveau code journal</th>
								<td><input type="text" name="diaryCode" id="diaryCode" maxlength="4" class="textInput" style="width:35px;" /></td>
							</tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="button" class="blueButton" value="Ajouter" onclick="addCode();" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<div class="mainContent" style="float:right; margin-top:10px; width:490px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">&Eacute;carts</p>
			</div>
			<div class="subContent">
				<form method="post" action="<?php echo $ScriptName ?>">
					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell" style="width:65%;">&Eacute;cart accepté lors de l'enregistrement des écarts</th>
								<td style="width:50%;" style="width:35%;"><input class="textInput" name="solde_tolerance" value="<?php echo $Parameter[ "solde_tolerance" ] ?>" style="width:30px;" /> ¤</td>
							</tr><tr>
								<th class="filledCell" style="width:65%;">&Eacute;cart maximum accepté lors de l'enregistrement des écarts</th>
								<td style="width:50%;" style="width:35%;"><input class="textInput" name="max_solde_tolerance" value="<?php echo $Parameter[ "max_solde_tolerance" ] ?>" style="width:30px;" /> ¤</td>
							</tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" name="Modify" value="Modifier" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	
	<div class="clear"></div>
	
	<div class="mainContent" style="float:left; margin-top:10px; width:490px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Exports</p>
			</div>
			<div class="subContent">
				<div id="export-sortable">
				<?php
					
					$rs =& DBUtil::query( "SELECT * FROM account_plan_export ORDER BY displayed DESC, display_order ASC" );
					
					if( $rs === false )
						trigger_error( "Impossible de récupérer les critères d'export", E_USER_ERROR );
					
					while( !$rs->EOF ){
						
						?>
					<div id="<?php echo $rs->fields( "idcolumn" ) ?>" class="column<?php echo $rs->fields( "displayed" ) ? "" : " hiddenColumn" ?>">
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/<?php echo $rs->fields( "displayed" ) ? "" : "un" ?>check_icon.png" alt="" />
						<div class="sortable-text"><?php echo Util::doNothing($rs->fields( "name" )) ?></div>
					</div>
						<?php
						
						$rs->MoveNext();
						
					}
					
				?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
</div>
<?php

//------------------------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------

function closeFiscalYear( $id ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/AccountPlan.php" );
	include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
	
	//Récupération des dates de début et fin de l'exercice comptable
	
	$rs =& DBUtil::query( "SELECT * FROM fiscal_years ORDER BY start_date ASC" );
	
	while( !$rs->EOF ){
		
		if( $rs->fields( "idfiscal_year" ) == $id ){
			
			//L'exercice est déjà clos
			if( $rs->fields( "closed" ) == 1 )
				return;
			
			$start = $rs->fields( "start_date" );
			break;
			
		}
		
		$rs->MoveNext();
		
	}
	
	$rs->MoveNext();
	
	$start_next = $rs->fields( "start_date" );
	
	$generatedRows = 0;
	
	//Report des comptes clients
	
	$action = "customer_forward_carry";
	
	$query = "
	SELECT idaccount_plan,
		idrelation,
		idaccount_record,
		record_type,
		code_eu,
		SUM( ( amount_type = 'credit' ) * amount - ( amount_type = 'debit' ) * amount ) AS balance
	FROM account_plan
	WHERE date >= '$start'
		AND date < '$start_next'
		AND idaccount_plan LIKE '411%'
		AND relation_type = 'buyer'
	GROUP BY idaccount_record
	HAVING balance != 0
	ORDER BY idrelation";
	
	$rs =& DBUtil::query( $query );
	
	$relation_type		= "buyer";
	$account_plan_1		= "VTE";
	$account_plan_2		= "D";
	$account_plan_4		= "A";
	$code_vat			= "";
	$payment			= "";
	$payment_date		= "";
	
	while( !$rs->EOF ){
		
		$idaccount_plan		= $rs->fields( "idaccount_plan" );
		$idrelation			= $rs->fields( "idrelation" );
		$idaccount_record	= $rs->fields( "idaccount_record" );
		$record_type		= $rs->fields( "record_type" );
		$date				= $start_next;
		$amount				= abs( $rs->fields( "balance" ) );
		$designation		= "Report facture $idaccount_record - " . AccountPlan::getCustomerName( $rs->fields( "idrelation" ) );
		$code_eu			= $rs->fields( "code_eu" );
		
		if( $rs->fields( "balance" ) > 0 ){
			
			$amount_type		= "credit";
			$account_plan_3		= "C";
			
		}
		else{
			
			$amount_type		= "debit";
			$account_plan_3		= "D";
			
		}
		
		AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
		
		$rs->MoveNext();
		$generatedRows++;
		
	}
	
	//Report des comptes fournisseurs
	
	$action = "supplier_forward_carry";
	
	$query = "
	SELECT a.idaccount_plan,
		a.idrelation,
		a.idaccount_record,
		a.record_type,
		a.code_eu,
		SUM( ( a.amount_type = 'credit' ) * a.amount - ( a.amount_type = 'debit' ) * a.amount ) AS balance,
		s.name
	FROM account_plan a, supplier s
	WHERE a.idrelation = s.idsupplier
		AND a.date >= '$start'
		AND a.date < '$start_next'
		AND a.idaccount_plan LIKE '401%'
		AND a.relation_type = 'supplier'
	GROUP BY a.idaccount_record
	HAVING balance != 0
	ORDER BY a.idrelation";
	
	$rs =& DBUtil::query( $query );
	
	$relation_type		= "supplier";
	$account_plan_1		= "ACH";
	$account_plan_2		= "D";
	$account_plan_4		= "N";
	$code_vat			= "";
	$payment			= "";
	$payment_date		= "";
	
	while( !$rs->EOF ){
		
		$idaccount_plan		= $rs->fields( "idaccount_plan" );
		$idrelation			= $rs->fields( "idrelation" );
		$idaccount_record	= $rs->fields( "idaccount_record" );
		$record_type		= $rs->fields( "record_type" );
		$date				= $start_next;
		$amount				= abs( $rs->fields( "balance" ) );
		$designation		= "Report facture $idaccount_record - " . $rs->fields( "name" );
		$code_eu			= $rs->fields( "code_eu" );
		
		if( $rs->fields( "balance" ) > 0 ){
			
			$amount_type		= "credit";
			$account_plan_3		= "C";
			
		}
		else{
			
			$amount_type		= "debit";
			$account_plan_3		= "D";
			
		}
		
		AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
		
		$rs->MoveNext();
		$generatedRows++;
		
	}
	
	//Factures clients à établir (to be issued invoices)
	
	$action = "customer_to_be_issued_invoices";
	
	$notIn =& AccountPlan::getNotIn( $action );
	
	$query = "
	SELECT bl_delivery.idbuyer,
		bl_delivery.idbl_delivery,
		bl_delivery.iddelivery,
		`order`.no_tax_export,
		ROUND( SUM( order_row.quantity * order_row.discount_price ) * ( 1 - `order`.total_discount / 100 ), 2 ) AS total_amount_ht,
		ROUND( SUM( order_row.quantity * order_row.discount_price * ( 1 + vat_rate / 100 ) ) * ( 1 - `order`.total_discount / 100 ), 2 ) AS total_amount
	FROM bl_delivery_row,
		order_row,
		`order`,
		bl_delivery LEFT JOIN billing_buyer ON bl_delivery.idbilling_buyer = billing_buyer.idbilling_buyer
	WHERE bl_delivery.idbl_delivery = bl_delivery_row.idbl_delivery
		AND bl_delivery.idorder = order_row.idorder
		AND bl_delivery_row.idorder_row = order_row.idrow
		AND order_row.idorder = `order`.idorder
		AND bl_delivery.dispatch_date < '$start_next'
		AND bl_delivery.dispatch_date != '0000-00-00'
		AND ( bl_delivery.idbilling_buyer = 0 OR billing_buyer.DateHeure >= '$start_next' )
		AND bl_delivery.idbl_delivery NOT IN( $notIn )
	GROUP BY `order`.idorder
	ORDER BY bl_delivery.dispatch_date";
	
	$rs =& DBUtil::query( $query );
	
	$relation_type		= "buyer";
	$account_plan_1		= "OD";
	$account_plan_2		= "D";
	$account_plan_4		= "A";
	$code_vat			= "";
	$payment			= "";
	$payment_date		= "";
	
	while( !$rs->EOF ){
		
		if( $rs->fields( "total_amount" ) > 0 ){
			
			$idrelation			= $rs->fields( "idbuyer" );
			$idaccount_record	= $rs->fields( "idbl_delivery" );
			$record_type		= "bl_delivery";
			$code_eu			= AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) );
			
			/*
			 * 418 - FAE (factures à établir) clients
			 */
			
			//@todo: mettre le bon numéro de plan comptable
			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 18, $rs->fields( "no_tax_export" ) );
			$date				= DateUtil::getPreviousDay( $start_next );
			$amount				= $rs->fields( "total_amount" );
			$designation		= "Facture à établir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$amount_type		= "debit";
			$account_plan_3		= "D";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			//Année n + 1
			
			$date			= $start_next;
			$amount_type	= "credit";
			$account_plan_3	= "C";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			/*
			 * 707 - Vente marchandises HT
			 */
			
			//@todo: mettre le bon numéro de plan comptable
			$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 10, $rs->fields( "no_tax_export" ) );
			$date				= DateUtil::getPreviousDay( $start_next );
			$amount				= $rs->fields( "total_amount_ht" );
			$designation		= "Facture à établir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
			$amount_type		= "credit";
			$account_plan_3		= "C";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			//Année n + 1
			
			$date			= $start_next;
			$amount_type	= "debit";
			$account_plan_3	= "D";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			/*
			 *  - TVA France
			 */
			
			if( $rs->fields( "total_amount" ) - $rs->fields( "total_amount_ht" ) > 0 && AccountPlan::getCodeEUFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ) ) == "NATIONAL" && $rs->fields( "no_tax_export" ) != 1 || $rs->fields( "no_tax_export" ) == 2 ){
				
				//@todo: mettre le bon numéro de plan comptable
				$idaccount_plan		= AccountPlan::getAccountNumberFromDelivery( $rs->fields( "idbuyer" ), $rs->fields( "iddelivery" ), 19, $rs->fields( "no_tax_export" ) );
				$date				= DateUtil::getPreviousDay( $start_next );
				$amount				= $rs->fields( "total_amount" ) - $rs->fields( "total_amount_ht" );
				$designation		= "Facture à établir " . AccountPlan::getCustomerName( $rs->fields( "idbuyer" ) );
				$amount_type		= "credit";
				$account_plan_3		= "C";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
				//Année n + 1
				
				$date			= $start_next;
				$amount_type	= "debit";
				$account_plan_3	= "D";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
			}
			
		}
		
		$rs->MoveNext();
		
	}
	
	//Factures fournisseurs non parvenues (accrued invoices)
	
	$action = "supplier_accrued_invoices";
	
	$notIn =& AccountPlan::getNotIn( $action );
	
	$query = "
	SELECT bl_delivery.idsupplier,
		bl_delivery.idbl_delivery,
		order_supplier.total_amount_ht,
		order_supplier.total_amount,
		state.code_eu,
		supplier.name
	FROM order_supplier,
		state,
		supplier,
		bl_delivery 
		LEFT JOIN billing_supplier_row ON billing_supplier_row.idbl_delivery = bl_delivery.idbl_delivery
		LEFT JOIN billing_supplier ON billing_supplier_row.billing_supplier = billing_supplier.billing_supplier
	WHERE bl_delivery.idorder_supplier = order_supplier.idorder_supplier
		AND bl_delivery.idsupplier = supplier.idsupplier
		AND supplier.idstate = state.idstate
		AND bl_delivery.dispatch_date < '$start_next'
		AND bl_delivery.dispatch_date != '0000-00-00'
		AND ( billing_supplier.billing_supplier = '0' OR billing_supplier.Date >= '$start_next' )
		AND billing_supplier.proforma = 0
		AND bl_delivery.idbl_delivery NOT IN( $notIn )
	GROUP BY billing_supplier_row.billing_supplier, billing_supplier_row.idsupplier
	ORDER BY bl_delivery.dispatch_date";
	
	$rs =& DBUtil::query( $query );
	
	$relation_type		= "supplier";
	$account_plan_1		= "OD";
	$account_plan_2		= "D";
	$account_plan_4		= "A";
	$code_vat			= "";
	$payment			= "";
	$payment_date		= "";
	
	while( !$rs->EOF ){
		
		if( round( $rs->fields( "total_amount" ), 2 ) > 0 ){
			
			$idrelation			= $rs->fields( "idsupplier" );
			$idaccount_record	= $rs->fields( "idbl_delivery" );
			$record_type		= "bl_delivery";
			$code_eu			= $rs->fields( "code_eu" );
			
			/*
			 * 408 - FNP (facture non parvenue) fournisseurs
			 */
			
			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 20 );
			$date				= DateUtil::getPreviousDay( $start_next );
			$amount				= round( $rs->fields( "total_amount" ), 2 );
			$designation		= "Facture non parvenue " . $rs->fields( "name" );
			$amount_type		= "credit";
			$account_plan_3		= "C";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			//Année n + 1
			
			$date			= $start_next;
			$amount_type	= "debit";
			$account_plan_3	= "D";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			/*
			 * 607 - Achat marchandises
			 */
			
			$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 8 );
			$date				= DateUtil::getPreviousDay( $start_next );
			$amount				= round( $rs->fields( "total_amount_ht" ), 2 );
			$designation		= "Facture non parvenue " . $rs->fields( "name" );
			$amount_type		= "debit";
			$account_plan_3		= "D";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			//Année n + 1
			
			$date			= $start_next;
			$amount_type	= "credit";
			$account_plan_3	= "C";
			
			AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
			
			$generatedRows++;
			
			/*
			 * 44586000 - TVA collectée France
			 */
			
			if( $rs->fields( "code_eu" ) == "NATIONAL" && round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 ) > 0 ){
				
				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 21 );
				$date				= DateUtil::getPreviousDay( $start_next );
				$amount				= round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 );
				$designation		= "TVA facture non parvenue " . $rs->fields( "name" );
				$amount_type		= "debit";
				$account_plan_3		= "D";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
				//Année n + 1
				
				$date			= $start_next;
				$amount_type	= "credit";
				$account_plan_3	= "C";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
			}
			
			if( $rs->fields( "code_eu" ) == "INTRA" && round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 ) > 0 ){
				
				/*
				 * 44527800 - TVA collectée CEE
				 */
				
				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 22 );
				$date				= DateUtil::getPreviousDay( $start_next );
				$amount				= round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 );
				$designation		= "TVA collectée CEE facture non parvenue " . $rs->fields( "name" );
				$amount_type		= "debit";
				$account_plan_3		= "D";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
				//Année n + 1
				
				$date			= $start_next;
				$amount_type	= "credit";
				$account_plan_3	= "C";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
				/*
				 * 44526800 - TVA déductible CEE
				 */
				
				$idaccount_plan		= AccountPlan::getAccountNumberFromCodeEU( $rs->fields( "code_eu" ), 23 );
				$date				= DateUtil::getPreviousDay( $start_next );
				$amount				= round( $rs->fields( "total_amount" ), 2 ) - round( $rs->fields( "total_amount_ht" ), 2 );
				$designation		= "TVA déductible CEE facture non parvenue " . $rs->fields( "name" );
				$amount_type		= "debit";
				$account_plan_3		= "D";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
				//Année n + 1
				
				$date			= $start_next;
				$amount_type	= "credit";
				$account_plan_3	= "C";
				
				AccountPlan::insertRow( $idaccount_plan, $idrelation, $relation_type, $idaccount_record, $record_type, $date, $amount, $amount_type, $designation, $code_eu, $code_vat, $payment, $payment_date, $account_plan_1, $account_plan_2, $account_plan_3, $account_plan_4, $action );
				
				$generatedRows++;
				
			}
			
		}
		
		$rs->MoveNext();
		
	}
	
	DBUtil::query( "UPDATE account_plan SET editable = 0 WHERE date >= '$start' AND date < '$start_next'" );
	DBUtil::query( "UPDATE fiscal_years SET closed = 1 WHERE idfiscal_year = $id" );
	
	return $generatedRows;
	
}

//------------------------------------------------------------------------------------------

function getCloseYearContent(){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	$rs =& DBUtil::query( "SELECT idfiscal_year, name, start_date FROM fiscal_years WHERE closed = 0 ORDER BY start_date ASC" );
	
	if( $rs === false )
		trigger_error( "Impossible de récupérer la liste des exercices comptables", E_USER_ERROR );
	
	?>
	<?php if( $rs->RecordCount() <= 1 ){ ?>
		<span style="font-weight:bold;">Aucun exercice à clore</span>
	<?php }else{ ?>
		<div class="tableContainer">
			<table class="dataTable">
				<tr>
					<th colspan="2" class="filledCell" style="text-align:center;">Prochain exercice à clore</th>
				</tr>
				<tr>
					<th class="filledCell" style="width:50%;">Nom de l'exercice</th>
					<td style="width:50%;"><?php echo $rs->fields( "name" ) ?></td>
				</tr>
				<tr>
					<th class="filledCell">Date de début de l'exercice</th>
					<td><?php echo Util::dateFormatEu( $rs->fields( "start_date" ) ) ?></td>
				</tr>
				<?php $id = $rs->fields( "idfiscal_year" ); $rs->MoveNext(); ?>
				<tr>
					<th class="filledCell">Date de fin de l'exercice</th>
					<td><?php echo Util::dateFormatEu( DateUtil::getPreviousDay( $rs->fields( "start_date" ) ) ) ?></td>
				</tr>
			</table>
		</div>
		<div class="submitButtonContainer">
			<form>
				<input type="button" class="blueButton" value="Clore" onclick="closeYear(<?php echo $id ?>);" />
			</form>
		</div>
	<?php } ?>
<?php
	
}

//------------------------------------------------------------------------------------------

?>