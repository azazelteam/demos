<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des addon
 */
 
	
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

$Title = Dictionnary::translate( "title_admin_adress" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------
$TableName='parameter_admin';

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x'])  )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?>
<div id="globalMainContent">
	<div class="mainContent fullWidthContent" style="margin-left:160px; width:680px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title">Gestion add-on</p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer">
		            	<table class="dataTable">
		            		<tr>
								<th class="filledCell" width="100">CRM</th>
								<td style="text-align:right;"><input class="textInput" type="text" name="akilae_crm" value="<?php echo $Parameter['akilae_crm'] ; ?>"></td>
								<th class="filledCell" width="100">Echéancier</th>
								<td style="text-align:right;"><input class="textInput" type="text" name='akilae_deadline' value="<?php echo $Parameter['akilae_deadline'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell">Groupement</th>
								<td style="text-align:right;"><input class="textInput" type="text" name='akilae_grouping' value="<?php echo $Parameter['akilae_grouping'] ; ?>"></td>
								<th class="filledCell">Commercial externe</th>
								<td style="text-align:right;"><input class="textInput" type="text" name='akilae_comext' value="<?php echo $Parameter['akilae_comext'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell">Réservation</th>
								<td style="text-align:right;"><input class="textInput" type="text" name='akilae_reservation' value="<?php echo $Parameter['akilae_reservation'] ; ?>"></td>
							</tr>
						</table>
						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>