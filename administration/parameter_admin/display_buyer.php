<?php
/**
 * Gestion des D�signations
 * 
 * @package    	Administration
 * @author     	Jean Bilwes
 * @author     	Annabelle Desbois
 * @author     	Jean-Fran�ois Corb� <jfcorbe@netlogix.fr>
 * @copyright  	2006 (c) NETLOGIX
 * @version 	CVS: $Id: display.php,v 1.27 2010-06-14 14:23:42 foxdie Exp $
 */

/**
 * Initialisation de la session
 * Initialisation des variables globales
 * Fonctions g�n�rales
 * Appels des biblioth�ques communes
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/adm_base.php");
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_ADMIN_PATH/objects/sessionobject.php");
 * => include_once("$GLOBAL_START_PATH/$GLOBAL_CODE_PATH/secure.inc.php");
 */

//-----------------------------------------------------------------------------



include_once( "../../objects/classes.php" );
$Title = Dictionnary::translate( "title_display" );
include_once( "../../catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );


//-----------------------------------------------------------------------------

 $TableName='parameter_admin';


//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']) )
{
foreach($_POST as $k=>$v) {
	$v = addslashes(stripslashes($v));	
	$Query = "UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'";
	//echo "$Query <br />";
	DBUtil::getConnection()->Execute($Query);
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like 'display%' OR idparameter LIKE 'use_tiny'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?> 
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
		            								<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate('title_display_cat') ; ?></p>
				        	</div>
		            	<table class="dataTable">
							<tr>
							
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info1') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info1','Afficher','Ne pas afficher');?></td>
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info2') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info2','Afficher','Ne pas afficher');?></td>	
						
							</tr><tr>	
							
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info3') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info3','Afficher','Ne pas afficher');?></td>
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info4') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info4','Afficher','Ne pas afficher');?></td>	
						
							</tr><tr>	
							
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info5') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info5','Afficher','Ne pas afficher');?></td>
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info6') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info6','Afficher','Ne pas afficher');?></td>
							
							</tr><tr>	
							
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info7') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info7','Afficher','Ne pas afficher');?></td>
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info8') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info8','Afficher','Ne pas afficher');?></td>
								
							</tr><tr>	
							
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info9') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info9','Afficher','Ne pas afficher');?></td>
								<th class="filledCell" style="width:40%;"><?php echo Dictionnary::translate('buyer_info10') ; ?> :</th>
								<td style="width:10%;"><?php echo DrawLiftParameters('display_buyer_info10','Afficher','Ne pas afficher');?></td>
								
							</tr>
							
							</table>
							

						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
    <!--<div id="tools" class="rightTools">
    	<div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Outils</p>
            </div>
            <div class="content"></div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
        <div class="toolBox">
        	<div class="header">
            	<div class="topRight"></div><div class="topLeft"></div>
                <p class="title">Infos Plus</p>
            </div>
            <div class="content">
            	<p>
                	Qu�est-ce qu�on mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de l�gumes, cookies 
                    moelleux ananas-coco. 
                </p>
                <div class="subTitleContainer">
                	<p class="subTitle">Commentaire client</p>
                </div>
                <p>
                	Qu�est-ce qu�on mange ce soir ? 
                    Cassolettes de crevettes aux trois 
                    poivrons, gratin de l�gumes, cookies 
                    moelleux ananas-coco. 
                </p>
            </div>
            <div class="footer">
            	<div class="bottomLeft"></div>
                <div class="bottomMiddle"></div>
                <div class="bottomRight"></div>
            </div>
        </div>
    </div>-->
</div>

<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------

?>
