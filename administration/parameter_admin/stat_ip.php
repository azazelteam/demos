<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire gestion des IP non utilisées pour les statistiques
 */



//fire User Rights Management
    $Str_TableName="stat_ip";
 
 
// Field columns that appear after searching in ShowRecordsBy	
	 $ArrayFieldsColumns2Show[0]="company";
	 $ArrayFieldsColumns2Show[]="ip";
  	 
//table key field : array with all table keys (used for correct deleting and modifying)
  $Array_KeyNames["idip"]="";
  $KeyName = 'idmenu';
  
//paramètres de base 
	$Int_LimitP = 10;
	$pagetitle = "title_stat_ip";
	
//show the page 
include "../../formbase/stdformbase.php";

?>