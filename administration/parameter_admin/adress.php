<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion coordonnées société
 */
 
	
//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

$Title = Dictionnary::translate( "title_admin_adress" );
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------
$TableName='parameter_admin';

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x'])  )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_admin SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}

//Formulaire

?>
<div id="globalMainContent">
	<div class="mainContent fullWidthContent" style="margin-left:160px; width:680px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer">
		            	<table class="dataTable">
		            		<tr>
								<th class="filledCell" width="100"><?php echo Dictionnary::translate('name') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name="ad_name" value="<?php echo $Parameter['ad_name'] ; ?>"></td>
								<th class="filledCell" width="100"><?php echo Dictionnary::translate('responsable') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_responsable' value="<?php echo $Parameter['ad_responsable'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('adress') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_adress' value="<?php echo $Parameter['ad_adress'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('city') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_city' value="<?php echo $Parameter['ad_city'] ; ?>"></td>
							</tr><tr>	
								<th class="filledCell"><?php echo Dictionnary::translate('zipcode') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_zipcode' value="<?php echo $Parameter['ad_zipcode'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('state') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_state' value="<?php echo $Parameter['ad_state'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('tel') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_tel' value="<?php echo $Parameter['ad_tel'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('fax') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_fax' value="<?php echo $Parameter['ad_fax'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('email') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='admin_email' value="<?php echo $Parameter['admin_email'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('site') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_sitename' value="<?php echo $Parameter['ad_sitename'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('siren') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_siren' value="<?php echo $Parameter['ad_siren'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('siret') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_siret' value="<?php echo $Parameter['ad_siret'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('tva-intra') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_tva-intra' value="<?php echo $Parameter['ad_tva-intra'] ; ?>"></td>
								<th class="filledCell"><?php echo Dictionnary::translate('ape') ; ?></th>
								<td style="text-align:right;"><input class="textInput" type="text" name='ad_ape' value="<?php echo $Parameter['ad_ape'] ; ?>"></td>
							<!--</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate('registered_brand') ; ?></th>
								<td style="text-align:right;" colspan="3"><input class="textInput" type="text" name='ad_registered_brand' value="<?php echo $Parameter['ad_registered_brand'] ; ?>"></td>
							</tr><tr>
								<th class="filledCell">Logo dans mails</th>
								<td style="text-align:right;" colspan="3"><input class="textInput" type="text" name='ad_logo' value="<?php echo $Parameter['ad_logo'] ; ?>"></td>
								-->
							</tr>
						</table>
						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>