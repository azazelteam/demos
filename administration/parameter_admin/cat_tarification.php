<?php 

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrage tarification
 */

//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

$Title = "Gestion des tarifications";
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" ); 

//-----------------------------------------------------------------------------

include_once('../../objects/adm_order.php');

//Enregistrement
if( isset($_POST['Modify']) || isset($_POST['Modify_x']) )
{
foreach($_POST as $k=>$v) {
	//$v = addslashes($v);	
	DBUtil::getConnection()->Execute("UPDATE parameter_cat SET paramvalue='$v' WHERE idparameter= '$k'");
	}
//echo '<pre>'; print_r($_POST);echo '</pre>';
}

//Chargement des valeurs
$res = DBUtil::getConnection()->Execute("SELECT idparameter,paramvalue FROM parameter_cat WHERE idparameter like '%'");
$n = $res->RecordCount();
for($i=0;$i<$n;$i++) {
	$ai = $res->fields['idparameter'];
	$Parameter[$ai] = $res->fields['paramvalue'];
	$res->MoveNext();
}


//Formulaire
?>
<form method='POST' action='<?php echo $ScriptName ?>' name='myform' >
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
 	            	<div class="tableContainer">
		            	<table class="dataTable">
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("multiprice_buyer_qty_max_only") ; ?></th>
								<td style="width:30px;"><input type="text" name="multiprice_buyer_qty_max_only" value=<?php echo $Parameter['multiprice_buyer_qty_max_only'] ; ?> class="textInput" /></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("use_custom_family_rate") ; ?></th>
								<td><input type="text" name="use_custom_family_rate" value=<?php echo $Parameter['use_custom_family_rate'] ; ?> class="textInput" /></td>
							</tr>
							<tr>
								<th class="filledCell"><?php echo Dictionnary::translate("use_multiprice_grouping") ; ?></th>
								<td><input type="text" name="use_multiprice_grouping" value=<?php echo $Parameter['use_multiprice_grouping'] ; ?> class="textInput" /></td>
							</tr>
						</table>
					</div>
					<input type="submit" class="blueButton" name="btn_modify" value="Modifier" style="float:right;" />
					<div class="clear"></div>
            	</div>
   			</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
</form>
<?php 

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" ); 

//-----------------------------------------------------------------------------

?>