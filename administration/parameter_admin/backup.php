<?php

/**
 * Gestion utilisateur des sauvegardes de la BDD
 */

//---------------------------------------------------------------------------------------

include_once( "../../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

$RESULT_PER_PAGE = 10;

//---------------------------------------------------------------------------------------
//type de sauvegardes

if( isset( $_REQUEST[ "type" ] ) ){
	
	switch( $_REQUEST[ "type" ] ){
		
		case "data" :
		
			$type = "data";
			$BACKUP_DIR = "$GLOBAL_START_PATH/backups/data";
			$prefix = $GLOBAL_DB_NAME;
			
			break;
			
		case "statistics" :
		
			$type = "statistics";
			$BACKUP_DIR = "$GLOBAL_START_PATH/backups/stats";
			$prefix = "stats";
			
			break;
		
		default : die( "Unknown type" );
		
	}
	
}else{
	
	$type = "data";
	$prefix = "data";
	$BACKUP_DIR = "$GLOBAL_START_PATH/backups/data";
	
}

//---------------------------------------------------------------------------------------
//upload

if( isset( $_GET[ "file" ] ) ){
		
	uploadFile( $_GET[ "file" ] );
	exit();
		
}

$Title = "Gestion des sauvegardes";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//---------------------------------------------------------------------------------------

?>
<div id="globalMainContent">
	<?php displaySearchBox(); ?>
	<div class="mainContent">
		<div class="topRight"></div>
		<div class="topLeft"></div>
		<?php
		
			if( isset( $_POST[ "Config" ] ) || isset( $_POST[ "Config_x" ] ) )
				updateBackupConfig();
			else if( isset( $_POST[ "DeleteSelection" ] ) || isset( $_POST[ "DeleteSelection_x" ] ) ){
				
				deleteSelection();
				
				listBackups();
				
				?>
				<br style="margin-bottom:10px;" />
				<?php
				
			}else if( isset( $_REQUEST[ "search" ] ) || isset( $_REQUEST[ "search_x" ] ) ){
			
				listBackups();
				
				?>
				<br style="margin-bottom:10px;" />
				<?php
				
			}
		
			displayBackupForm();
			
		?>
		<div class="bottomRight"></div>
		<div class="bottomLeft"></div>
	</div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-------------------------------------------------------------------------------------

function listBackups(){
	
	global 	$BACKUP_DIR,
			$RESULT_PER_PAGE,
			$GLOBAL_START_URL,
			$type;
	
	?>
	<div class="content">
		<div class="headTitle">
			<p class="title">Résultat de la recherche (<?= $type == "data" ? "données" : "statistiques" ?>)</p>
		</div>
		<div class="subContent">
		<?php
	
	//date de début
	
	$start_date = isset( $_REQUEST[ "start_date" ] ) ? $_REQUEST[ "start_date" ] : "";
	
	// Test supprimé suite à un changement dans la date (rentrée dans 3 champs de texte avant, un seul maintenant)
	/*
	 if( $start_date === false ){
		
		?>
		<p style="text-align:center">
			Le format de la date de début sélectionné pour la recherche est incorrect
		</p>
		<?php
		
		return;
		
	}
	*/
	
	//date de fin
	
	$end_date = isset( $_REQUEST[ "end_date" ] ) ? $_REQUEST[ "end_date" ] : "";
	
	// Test supprimé suite à un changement dans la date (rentrée dans 3 champs de texte avant, un seul maintenant)
	/*
	if( $end_date === false ){
		
?>
		<p style="text-align:center">
			Le format de la date de fin sélectionné pour la recherche est incorrect
		</p>
<?php
		
		return;
		
	}
	*/
	
	//pertinence recherche date_end < date_start
	
	if( !empty( $start_date ) && !empty( $end_date ) ){
		
		if( strcmp( $end_date, $start_date ) < 0 ){
			
?>
			<p style="text-align:center">
				La date de début est supérieure à la date de fin
			</p>
<?php
			
			return;
			
		}
		 
	}
	
	//pertinence recherche date_start > date du jour
	
	if( !empty( $start_date ) ){
		
		$curdate = date( "Y-m-d" );
		
		if( strcmp( $curdate, $start_date ) < 0 ){
			
			?>
			<p style="text-align:center">
				La date de début est supérieure à la date du jour
			</p>
			<?php
			
			return;
			
		}
		
	}
	
	//récupérer la liste des fichiers
	
	$files = getBackupFiles( $start_date, $end_date );
	usort( $files, "strcompare" );
	
	$page = isset( $_GET[ "page" ] ) ? intval( $_GET[ "page" ] ) : 1;
	$start = ( $page - 1 ) * $RESULT_PER_PAGE;
	
	if( !count( $files ) ){
		
		?>
		<p>Aucune sauvegarde trouvée</p>
		<?php
		
		return;
		
	}
	
	?>
				<p><?= count( $files ) ?> sauvegardes trouvées</p>
				<script type="text/javascript" language="javascript">
				<!--
				
					function checkAll( status ){
						
						var i = 0;
						var checkboxes = document.forms.search_form.elements[ 'files[]' ];
						var content = document.getElementById('checkall');
						
						if( status )
							content.innerHTML = "décocher";
						else
							content.innerHTML = "cocher";
						
						while( i < checkboxes.length ){
						
							checkboxes[ i ].checked = status;
							
							i++;
							
						}
						
					}
					
				// -->
				</script>
				<form id="search_form" action="backup.php" method="post" enctype="multipart/form-data">
				<input type="hidden" name="type" value="<?= $type ?>" />
				<div class="tableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th style="width:50px;">Suppr.</th>
								<th>Nom du fichier</th>
								<th>Date de création</th>
								<th>Télécharger</th>
							</tr>
						</thead>
						<tbody>
<?php
	
	$i = $start;
	while( $i < count( $files ) && $i < $start + $RESULT_PER_PAGE ){

		$file = $files[ $i ];
		$status = isset( $_POST[ "files" ] ) && in_array( $file, $_POST[ "files" ] ) ? " checked=\"checked\"" : "";
		
?>
							<tr>
								<td class="lefterCol"><input type="checkbox" id="files[]" name="files[]" value="<?= $file ?>" /></td>
								<td><?= $file ?></td>
								<td><?= humanReadableDate( getStrFileDate( $file ) ) ?></td>
								<td class="righterCol">
									<a href="backup.php?type=<?= $type ?>&amp;file=<?= $file ?>" onclick="window.open(this.href); return false;">
										<img src="<?= $GLOBAL_START_URL ?>/images/back_office/content/rightArrow.png" alt="Télécharger" style="border-style:none;" />
									</a>
								</td>
							</tr>
<?php
		
		$i++;
			
	}
	
?>
						</tbody>
					</table>
				</div>
				<?php if( count( $files ) > 1 ){ ?><div style="float:left;"><input type="checkbox" onclick="checkAll( this.checked );" style="vertical-align:bottom;" /> Tout <span id="checkall">cocher</span></div><?php } ?>
				<div class="submitButtonContainer">
					<input type="submit" name="DeleteSelection" value="Supprimer la sélection" onclick="return confirm('Etes-vous certains de vouloir supprimer les fichiers sélectionnés?');" class="blueButton" />
				</div>
				</form>
<?php
	
	displayPageList( count( $files ) );
	
?>
			</div>
		</div>
<?php
	
}

//-------------------------------------------------------------------------------------

function displayPageList( $count ){
	
	global 	$RESULT_PER_PAGE,
			$GLOBAL_START_URL,
			$type;
	
	if( $count <= $RESULT_PER_PAGE )
		return;
		
	$page = isset( $_GET[ "page" ] ) ? intval( $_GET[ "page" ] ) : 1;
	$pageCount = ceil( $count / $RESULT_PER_PAGE );
	
	$start_date = isset( $_REQUEST[ "start_date" ] ) ? $_REQUEST[ "start_date" ] : "";
	$end_date = isset( $_REQUEST[ "end_date" ] ) ? $_REQUEST[ "end_date" ] : "";
	
	$url = "backup.php?search&amp;type=$type&amp;start_date=" . $start_date;
	$url .= "&amp;end_date=" . $end_date;
	
	?>
	<p style="text-align:center">Pages&nbsp;
	<?php
	
	if( $page > 1 ){
		
		$prev = $page - 1;
		
		?>
		<a href="<?= "$url&amp;page=$prev" ?>">
			<img src="<?= $GLOBAL_START_URL ?>/www/icones/previous.png" alt="Page pr&eacute;c&eacute;dente" style="border-style:none; vertical-align:text-bottom;" />
		</a>
		<?php
		
	}
	
	$i = 1;
	while( $i < $page ){
		
		echo "
		&nbsp;<a href=\"$url&amp;page=$i\">$i</a>&nbsp;";
		
		$i++;
		
	}
	
	echo "
	&nbsp;<u>$i</u>&nbsp;";
	
	$i++;
		
	while( $i <= $pageCount ){
		
		echo "
		&nbsp;<a href=\"$url&amp;page=$i\">$i</a>&nbsp;";
		
		$i++;
		
	}
	
	if( $page < $pageCount ){
		
		$next = $page + 1;
		
		?>
		<a href="<?= "$url&amp;page=$next" ?>">
			<img src="<?= $GLOBAL_START_URL ?>/www/icones/next.png" alt="Page pr&eacute;c&eacute;dente" style="border-style:none; vertical-align:text-bottom;" />
		</a>
		<?php
		
	}
	
	?></p><?php
	
}

//-------------------------------------------------------------------------------------

function getStrFileDate( $filename ){
	
	global 	$type,
			$GLOBAL_DB_NAME,
			$prefix;
	
	$filedate = substr( $filename, strlen( "$prefix-" ), 8 );
	$fileyear = substr( $filedate, 0, 4 );
	$filemonth = substr( $filedate, 4, 2 );
	$fileday = substr( $filedate, 6, 2 );

	return date( "$fileyear-$filemonth-$fileday" );
	
}

//-------------------------------------------------------------------------------------

function getBackupFiles( $start_date = "", $end_date = "" ){
	
	global 	$type,
			$BACKUP_DIR,
			$GLOBAL_DB_NAME,
			$prefix;
	
	$pattern = "$BACKUP_DIR/$prefix-*.gz";
	
	$results = array();
	$list = array();
	
	$dir = opendir( $BACKUP_DIR );
	if( !is_resource( $dir ) )
		die( "Impossible d'accéder au répertoire de sauvegarde" );
		
	while( $file = readdir( $dir ) ){
		
		if( !is_dir( $file ) && $file != "." && $file != ".." && $file != ".htaccess" ){
			
			$match = true;
		
			if( !empty( $start_date ) || !empty( $end_date ) ){
				
				$filedate = substr( $file, strlen( "$prefix-" ), 8 );
				$fileyear = substr( $filedate, 0, 4 );
				$filemonth = substr( $filedate, 4, 2 );
				$fileday = substr( $filedate, 6, 2 );
				
				$filetime = strtotime( "$fileyear-$filemonth-$fileday" );
				
				if( !empty( $start_date ) ){
					
					$mintime = strtotime( $start_date );
					$cmp = $filetime - $mintime;
					 
					if( $cmp < 0 )
						$match = false;
						
				}
				
				if( !empty( $end_date ) ){

					$maxtime = strtotime( $end_date );
					$cmp = $maxtime - $filetime;
					
					if( $cmp < 0 )
						$match = false;
						
				}
				
			}
			
			if( $match )
				$results[] = $file;
				
		}
		
	}

	return $results;
	
}

//-------------------------------------------------------------------------------------

function displaySearchBox(){
	
	global	$GLOBAL_START_URL,
			$type;
	
?>
	<form action="backup.php" method="post" name="searchform" enctype="multipart/form-data">
	<div id="tools" class="leftTools">
	    <div class="toolBox">
	    	<div class="header">
	        	<div class="topRight"></div><div class="topLeft"></div>
	            <p class="title">Recherche</p>
	        </div>
	        <div class="content">
	        	<div style="padding:5px;">
					<div>Type de sauvegarde</div>
					<div>
						<select name="type">
							<option value="data"<?php if( $type == "data" ) echo " selected=\"selected\""; ?>>Données</option>
							<option value="statistics"<?php if( $type == "statistics" ) echo " selected=\"selected\""; ?>>Statistiques</option>
						</select>
					</div>
				</div>
				<div class="subTitleContainer" style="margin-top:0px;">
					<p class="subTitle">Sauvegardes éffectuées</p>
				</div>
				<div style="padding:5px;">
					<div>Depuis le</div>
					<div>
						<input type="text" name="start_date" id="start_date" value="<?php if( isset( $_REQUEST[ "start_date" ] ) ) echo $_REQUEST[ "start_date" ]; ?>" class="calendarInput" />
						<?php DHTMLCalendar::calendar( "start_date" ); ?>
					</div>
					<div>Jusqu'au</div>
					<div>
						<input type="text" name="end_date" id="end_date" value="<?php if( isset( $_REQUEST[ "end_date" ] ) ) echo $_REQUEST[ "end_date" ]; ?>" class="calendarInput" />
						<?php DHTMLCalendar::calendar( "end_date" ); ?>
					</div>
				</div>
				<div style="float:right; margin:0px 5px;">
					<input type="hidden" name="search" value="1" />
					<input type="submit" value="Rechercher" class="blueButton" />
				</div>
				<div class="clear"></div>
	        </div>
	        <div class="footer">
	        	<div class="bottomLeft"></div>
	            <div class="bottomMiddle"></div>
	            <div class="bottomRight"></div>
	        </div>
	    </div>
	</div><!-- Fin tools -->
	</form>
<?php
	
}

//-------------------------------------------------------------------------------------

function uploadFile( $file ){
	
	global 	$BACKUP_DIR;
	
	ini_set( "upload_max_filesize", "16M" );
	set_time_limit( 0 );
	
	if( strpos( $file, "/" ) )
		die( "Vilain!" );
	
	$path = "$BACKUP_DIR/$file";
	
	if( !file_exists( $path ) )
		die( "le fichier n'existe pas" );
	
	$fp = fopen( $path, "r" );
	if( !is_resource( $fp ) )
		die( "Impossible d'ouvrir le fichier en lecture seule" );
	
	/*header( "Content-type: application/octet-stream" );
	header( "Content-Disposition: attachment; filename=$file" );*/
	
	header( "Content-type: application/x-gzip" );
	header( "Content-Disposition: attachment; filename=$file" );
	
	while( $data = fread( $fp, 4096 ) )
		echo $data;
	
	fclose( $fp );
	
}

//-------------------------------------------------------------------------------------

function deleteSelection(){
	
	global	$BACKUP_DIR,
			$GLOBAL_START_PATH;
	
	if( !count( $_POST[ "files" ] ) ){
		
?>
		<p style="text-align:center">
			Vous devez sélectionner des fichiers à supprimer
		</p>
<?php
		
		return;
		
	}
	
	$i = 0;
	while( $i < count( $_POST[ "files" ] ) ){
		
		$file = $_POST[ "files" ][ $i ];
		$path = "$BACKUP_DIR/$file";
		
		if( !file_exists( $path ) )
			die( "Le fichier $file est introuvable" );
		
		if( !unlink( $path ) )
			die( "Impossible de supprimer le fichier $file" );
		
		$i++;
		
	}
	
?>
	<hr />
	<p class="msg" style="text-align:center;">
		Les fichiers sélectionnés ont été supprimés
	</p>
	<hr />
<?php
	
}

//-------------------------------------------------------------------------------------

function displayBackupForm(){
	
	global	$GLOBAL_START_URL,
			$type;
	
	$data_backup_frequency = 	DBUtil::getParameterAdmin( "data_backup_frequency" );
	$data_backup_auto = 		DBUtil::getParameterAdmin( "data_backup_auto" );
	$data_backup_availability = DBUtil::getParameterAdmin( "data_backup_availability" );
	
	$statistics_backup_frequency = 	DBUtil::getParameterAdmin( "statistics_backup_frequency" );
	$statistics_backup_auto = 		DBUtil::getParameterAdmin( "statistics_backup_auto" );
	$statistics_backup_availability = DBUtil::getParameterAdmin( "statistics_backup_availability" );
	
	?>
		<div class="content">
			<div class="headTitle">
				<p class="title">Paramétrage des sauvegardes</p>
			</div>
			<div class="subContent">
				<form name="BackupForm" method="post" enctype="multipart/form-data">
					<input type="hidden" name="type" value="<?= $type ?>" />
					<div class="subTitleContainer">
						<p class="subTitle">Sauvegarde de données</p>
					</div>
					<div class="tableContainer">
						<table class="dataTable clientTable" style="margin:5px auto; width:500px;">
							<tr>
								<th style="width:50%;">Sauvegardes automatiques</th>
								<td style="width:50%;">
									<input type="radio" name="data_backup_auto" value="1" style="vertical-align:bottom;"<?php if( $data_backup_auto ) echo " checked=\"checked\""; ?> />&nbsp;Oui&nbsp;
									<input type="radio" name="data_backup_auto" value="0" style="vertical-align:bottom;"<?php if( !$data_backup_auto ) echo " checked=\"checked\""; ?> />&nbsp;Non
								</td>
							</tr>
							<tr>
								<th>Fréquence des sauvegardes</th>
								<td>
									<select name="data_backup_frequency" class="fullWidth">
										<option value="daily"<?php if( $data_backup_frequency == "daily" ) echo " selected=\"selected\""; ?>>Tous les jours</option>
										<option value="weekly"<?php if( $data_backup_frequency == "weekly" ) echo " selected=\"selected\""; ?>>Toutes les semaines</option>
										<option value="monthly"<?php if( $data_backup_frequency == "monthly" ) echo " selected=\"selected\""; ?>>Tous les mois</option>
										<option value="yearly"<?php if( $data_backup_frequency == "yearly" ) echo " selected=\"selected\""; ?>>Tous les ans</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>Durée maximale d'archivage</th>
								<td>
									<select name="data_backup_availability" class="fullWidth">
										<option value="week"<?php if( $data_backup_availability == "week" ) echo " selected=\"selected\""; ?>>Une semaine</option>
										<option value="month"<?php if( $data_backup_availability == "month" ) echo " selected=\"selected\""; ?>>Un mois</option>
										<option value="year"<?php if( $data_backup_availability == "year" ) echo " selected=\"selected\""; ?>>Un an</option>
										<option value="semester"<?php if( $data_backup_availability == "semester" ) echo " selected=\"selected\""; ?>>6 mois</option>
										<option value="forever"<?php if( $data_backup_availability == "forever" ) echo " selected=\"selected\""; ?>>Aucune</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class="subTitleContainer">
						<p class="subTitle">Sauvegarde de statistiques</p>
					</div>
					<div class="tableContainer">
						<table class="dataTable clientTable" style="margin:5px auto; width:500px;">
							<tr>
								<th style="width:50%;">Sauvegardes automatiques</th>
								<td style="width:50%;">
									<input type="radio" name="statistics_backup_auto" value="1" style="vertical-align:bottom;"<?php if( $statistics_backup_auto ) echo " checked=\"checked\""; ?> />&nbsp;Oui&nbsp;
									<input type="radio" name="statistics_backup_auto" value="0" style="vertical-align:bottom;"<?php if( !$statistics_backup_auto ) echo " checked=\"checked\""; ?> />&nbsp;Non
								</td>
							</tr>
							<tr>
								<th>Fréquence des sauvegardes</th>
								<td>
									<select name="statistics_backup_frequency" class="fullWidth">
										<option value="daily"<?php if( $statistics_backup_frequency == "daily" ) echo " selected=\"selected\""; ?>>Tous les jours</option>
										<option value="weekly"<?php if( $statistics_backup_frequency == "weekly" ) echo " selected=\"selected\""; ?>>Toutes les semaines</option>
										<option value="monthly"<?php if( $statistics_backup_frequency == "monthly" ) echo " selected=\"selected\""; ?>>Tous les mois</option>
										<option value="yearly"<?php if( $statistics_backup_frequency == "yearly" ) echo " selected=\"selected\""; ?>>Tous les ans</option>
									</select>
								</td>
							</tr>
							<tr>
								<th>Durée maximale d'archivage</th>
								<td>
									<select name="statistics_backup_availability" class="fullWidth">
										<option value="week"<?php if( $statistics_backup_availability == "week" ) echo " selected=\"selected\""; ?>>Une semaine</option>
										<option value="month"<?php if( $statistics_backup_availability == "month" ) echo " selected=\"selected\""; ?>>Un mois</option>
										<option value="year"<?php if( $statistics_backup_availability == "year" ) echo " selected=\"selected\""; ?>>Un an</option>
										<option value="semester"<?php if( $statistics_backup_availability == "semester" ) echo " selected=\"selected\""; ?>>6 mois</option>
										<option value="forever"<?php if( $statistics_backup_availability == "forever" ) echo " selected=\"selected\""; ?>>Aucune</option>
									</select>
								</td>
							</tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="submit" name="Config" class="blueButton" value="Enregistrer" />
					</div>
				</form>
			</div>
		</div>
<?php
	
}

//-------------------------------------------------------------------------------------

function updateBackupConfig(){
	
	$parameters = array(
	
		"data_backup_frequency" 			=> $_POST[ "data_backup_frequency" ],
		"data_backup_auto" 					=>  $_POST[ "data_backup_auto" ],
		"data_backup_availability" 			=>  $_POST[ "data_backup_availability" ],
		"statistics_backup_frequency" 		=> $_POST[ "statistics_backup_frequency" ],
		"statistics_backup_auto" 			=>  $_POST[ "statistics_backup_auto" ],
		"statistics_backup_availability" 	=>  $_POST[ "statistics_backup_availability" ]
		
	);
	
	foreach( $parameters as $idparameter => $paramvalue ){
		
		$query = "UPDATE parameter_admin SET paramvalue = '$paramvalue' WHERE idparameter = '$idparameter' LIMIT 1";
		
		$rs = DBUtil::getConnection()->Execute( $query );
		
		if( $rs === false )
			die( "Impossible de mettre à jour la configuration des sauvegardes automatiques" );
		
	}
	
?>
	<p style="text-align:center;">
		Vos modifications ont bien été enregistrées
	</p>
<?php
	
}

//-------------------------------------------------------------------------------------

function strcompare( $str1, $str2 ){
	
	$ret = strcmp( $str1, $str2 );
	
	if( $ret != 0 )
		$ret = -$ret;
		
	return $ret;

	
}

//-------------------------------------------------------------------------------------

?>