<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Paramétrages globaux
 */


include "../../objects/classes.php";
include "./admin_cat_dev.inc.php";
include_once( "../../catalog/drawlift.php" );

  $TableName="parameter_cat";
  $IdParameter[0]="Charge";
  $IdParameter[1]="Choice";
  $IdParameter[2]="idcurrency";
  $IdParameter[3]="statedefault";
  $IdParameter[4]="vatdefault";
  $IdParameter[5]="default_idpayment_delay";
  $IdParameter[6]="default_idpayment";
  
$Title = "Paramétrages de base";
include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

if(isset($_POST['Modify']) || isset($_POST['Modify_x']))
{
	//FAdminUpdateRecords($TableName,$IdParameter,$HTTP_POST_VARS);
    FAdminUpdateRecords($TableName,$IdParameter,$_POST);
}
//Chargement des valeurs
$IdParameterValues =  FadminLoadValues($TableName,$IdParameter);

//Formulaire
?> 

<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	<div class="headTitle">
                <p class="title"><?php echo $Title ?></p>
                <div class="rightContainer">
                </div>
            </div>
            <div class="subContent">
            	<!-- formulaire -->
            	<form id="devisSearchForm" name="myform" method="post" action="<?php echo $ScriptName ?>">
	            	<div class="tableContainer" style="width:600px; padding-top:14px; margin:auto;">
	            	<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate('parameter_device') ; ?></p>
				        	</div>
		            	<table class="dataTable">
							<tr>
								<th style="width:400px;" class="filledCell"><?php echo Dictionnary::translate("admin_state") ; ?>:</th>
								<td style="text-align:left;"><?php DisplayStateLift($IdParameter[3],$IdParameterValues[3]); ?></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate("admin_device") ; ?>:</th>
								<td style="text-align:left;"><?php DisplayDeviceLift($IdParameter[2],$IdParameterValues[2]); ?></td>
							</tr>
							</table>
							<br>
							<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate('parameter_payment') ; ?></p>
				        	</div>
		            	<table class="dataTable">
							<tr>
								<th style="width:400px;"class="filledCell"><?php echo Dictionnary::translate('default_idpayment_delay') ; ?> :</th>
								<td style="text-align:left;"><?php DisplayPaymentdelayLift($IdParameter[5],$IdParameterValues[5]); ?></td>
							</tr><tr>								
								<th class="filledCell"><?php echo Dictionnary::translate('default_idpayment') ; ?> :</th>
								<td style="text-align:left;"><?php DisplayPaymentLift($IdParameter[6],$IdParameterValues[6]); ?></td>						
							</tr>
							</table>
							<br>	
							
							 	<div class="subTitleContainer">
                <p class="subTitle"><?php echo Dictionnary::translate('parameter_charge') ; ?></p>
				        	</div>
				        	
		            	<table class="dataTable">
							<tr>
								<th style="width:400px;" class="filledCell"><?php echo Dictionnary::translate("admin_charge") ; ?>:</th>
								<td style="text-align:left;"><?php DisplayChoiceLift($IdParameter[1],$IdParameterValues[1]); ?></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate("admin_charge2") ; ?>:</th>
								<td style="text-align:left;"><?php DisplayChargeLift($IdParameter[0],$IdParameterValues[0]); ?></td>
							</tr><tr>
								<th class="filledCell"><?php echo Dictionnary::translate("admin_vat") ; ?>:</th>
								<td style="text-align:left;"><?php DisplayVatLift($IdParameter[4],$IdParameterValues[4]); ?></td>
							</tr>
						</table>
						<input type="submit" class="blueButton" name="Modify" value="Modifier" style="float:right; margin-top:7px;" />
					</div>
					<div class="clear"></div>
            	</form>
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
</div>
<?php

//-----------------------------------------------------------------------------

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//-----------------------------------------------------------------------------

?>