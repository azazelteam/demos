<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire de gestion des historiques pdf
 */

 //Paramètres de base 
 	$Int_LimitP = 5;
 	$pagetitle = "title_pdf_story";
    $Str_TableName="pdf_story";
    $KeyName ='idpdf_story';

// Field columns that appear after searching in ShowRecordsBy	
  $ArrayFieldsColumns2Show[0]="idpdf_story";
  $ArrayFieldsColumns2Show[1]="name";
  $ArrayFieldsColumns2Show[2]="type";

   //Chemin pour les téléchargements relatif à $GLOBAL_START_PATH .'/www/'
 $Param_FileLocation = 'img_dir_pdf'; // recherche dans parmeter_admin
  
 //show the page 
 include "../../formbase/stdformbase.php";
?>