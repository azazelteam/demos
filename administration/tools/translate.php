<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire de gestion des traductions
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_translation";
    $Str_TableName="translation";
    $KeyName = 'idtext';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idtext";
	$ArrayFieldsColumns2Show[1]="language1";

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>