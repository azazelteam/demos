<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Moteur de recherche affiché dans certains outils stdform
 */

//-----------------------------------------------------------------------------

include_once( "../../objects/classes.php" );

$banner = "no";

$Title = Dictionnary::translate( "admin_search_idproduct_idbuyer" );

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//-----------------------------------------------------------------------------

// FAdministrativeManageUser( $Int_IdTool);
$ScriptName = basename($_SERVER['PHP_SELF']);

//-----------------------------------------------------------------------------

?>
<style type="text/css">
<!--
div#global{
	min-width:0;
	width:auto;
}
-->
</style>
<div id="globalMainContent" style="width:780px;">

<?php

if(isset($_POST['k'])) $k = $_POST['k']; else $k="product";
if(isset($_POST['SearchText'])) $SearchText = $_POST['SearchText']; else $SearchText ='';

if(isset($_POST['Search']) || isset($_POST['Search_x'])){
	
	$Data = array();
	SearchId( $k, $SearchText, $Data );
	DisplayResult( $Data );

}

DisplaySearchForm( $ScriptName, $k, $SearchText );

//-----------------------------------------------------------------------------

?>
</div>
<?php

include_once( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

function DisplaySearchForm( $ScriptName, $k, $SearchText ){
	
	if( $k == "product" ){
		
		$Selected1 = "checked";
		$Selected2 = "";
		
	}elseif( $k == "buyer" ){
		
		$Selected1 = "";
		$Selected2 = "checked";
		
	}
	
?>
	<div class="mainContent fullWidthContent" style="width:780px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Recherche rapide pour trouver l'identifiant</p>
			</div>
			<div class="subContent">
				<form action="<?php echo $ScriptName ?>" method="post">
					<div class="tableContainer"> 
						<table class="dataTable">
							<tr>
								<th colspan="2" class="filledCell" style="text-align:center;">
									<input type="radio" name="k" value="product" <?php echo $Selected1 ?>  style="vertical-align:bottom;" /> Nom du produit
									<input type="radio" name="k" value="buyer" <?php echo $Selected2 ?> style="vertical-align:bottom;" /> Nom l'acheteur
								</th>
							</tr>
							<tr>
								<th>Recherche</th>
								<td><input type="text" name="SearchText" value="<?php echo $SearchText ?>" class="textInput" /></td>
							</tr>
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="submit" name="Search" value="Rechercher" class="blueButton" />
					</div>
				</form>
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
<p>&nbsp;</p>
<?php
	
}

function DisplayResult( $Data ){

?>
	<div class="mainContent fullWidthContent" style="width:780px;">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<p class="title">Résultats</p>
			</div>
			<div class="subContent">
				<?php if( count( $Data ) ){	?>
				<div class="tableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th>Nom</th>
								<th>ID</th>
							</tr>
						</thead>
						<tbody>
						<?php for( $i = 0 ; $i < count( $Data ) ; $i++ ){ ?>			
							<tr>
								<td class="lefterCol"><?php echo $Data[ $i ][ "name" ] ?></td>
								<td class="righterCol"><?php echo $Data[ $i ][ "id" ] ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</div>
				<?php }else{ ?>		
				<div>Aucun resultat</div>
<?php } ?>
<?php
	
}

function SearchId( $k, $SearchText, &$Data ){
	
	if( $k == "product" )
		SearchIdProduct( $SearchText, $Data );
	elseif( $k == "buyer" )
		SearchIdBuyer( $SearchText, $Data );
	
}
//-----------------------------------------------------------------------------

function SearchIdProduct( $SearchText,&$Data ){
	
	$lang =  User::getInstance()->getLang();
	
	$Table="detail d, product p";
	$Key="idproduct";
	$SQL_select = "
	SELECT DISTINCT p.$Key, p.name$lang FROM $Table
	WHERE d.idproduct = p.idproduct
	AND (
		p.name$lang LIKE '%$SearchText%' 
		OR p.description$lang LIKE '%$SearchText%'
		OR d.designation$lang LIKE '%$SearchText%'
	)
	GROUP BY p.$Key, p.name$lang";
				
	$res = DBUtil::getConnection()->Execute($SQL_select);
	if($res->RecordCount())
	{

		for($i=0 ; $i < $res->RecordCount() ; $i++)
		{
			$Data[$i]["id"]=$res->fields[$Key];
			$Data[$i]["name"]=$res->fields["name$lang"];
			$res->MoveNext();
		}
	}
}
//-----------------------------------------------------------------------------

function SearchIdBuyer($SearchText,&$Data){
	
	
	$Table = "buyer";
	$Key = "idbuyer";
	$SQL_select = "SELECT $Key, company FROM $Table WHERE company LIKE '%$SearchText%'";

	$res = DBUtil::getConnection()->Execute($SQL_select);
	if(!empty($res) && $res->RecordCount())
	{
		for($i=0 ; $i < $res->RecordCount() ; $i++)
		{
			$Data[$i]["id"]=$res->fields[$Key];
			$Data[$i]["name"]=$res->fields["company"];
			$res->MoveNext();
		}
	}
}

?>