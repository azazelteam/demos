<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire de gestion des menus
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_menu";
    $Str_TableName="menu";
    $KeyName = 'idmenu';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idmenu";
	$ArrayFieldsColumns2Show[1]="name";
	$ArrayFieldsColumns2Show[2]="parent";
	$ArrayFieldsColumns2Show[3]="type_tools";		

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>