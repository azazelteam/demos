<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire des gestion des fichiers
 */

// Paramétrage de base 
    $Str_TableName="file";
    $Int_LimitP = 5;
	$pagetitle = "title_file";
 
// Field columns that appear after searching in ShowRecordsBy	
	 $ArrayFieldsColumns2Show[0]="idfile";
	 $ArrayFieldsColumns2Show[]="tools";
	 $ArrayFieldsColumns2Show[]="name";
  	 
	
//show the page 
	include "../../formbase/stdformbase.php";

?>