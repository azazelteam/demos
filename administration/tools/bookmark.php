<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire gestion des bookmark
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_admin_bookmark";
    $Str_TableName="bookmark";
    $KeyName = 'idbookmark';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idbookmark";
	$ArrayFieldsColumns2Show[]="bookmark_1";

//Chemin pour les téléchargements relatif à $GLOBAL_START_PATH .'/www/'
 $Param_FileLocation = 'img_dir_documentation'; // recherche dans parmeter_admin

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>