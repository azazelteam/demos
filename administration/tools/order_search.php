﻿<?php

//--------------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../../script/global.fct.php" );
include_once( "$GLOBAL_START_PATH/objects/DateUtil.php" );
include_once( "$GLOBAL_START_PATH/objects/URLFactory.php" );
include_once( "$GLOBAL_START_PATH/objects/AutoCompletor.php" );
include_once( "$GLOBAL_START_PATH/objects/Util.php" );
//------------------------------------------------------------------------------------------
/* recherche ajax */

if( isset( $_POST[ "search" ] ) && $_POST[ "search" ] == "1" ){
	
	header( "Cache-Control: no-cache, must-revalidate" );
	header( "Pragma: no-cache" );
	header( "Content-Type: text/html; charset=ISO-8859-15" );
	
	if( isset( $_POST[ "searchFor" ] ) && in_array( "customer", $_POST[ "searchFor" ] ) )
		searchTaxeResults();
	
	if( isset( $_POST[ "searchFor" ] ) && in_array( "supplier", $_POST[ "searchFor" ] ) )
		searchClientcategResults();

	
	exit();
	
}

//------------------------------------------------------------------------------------------------
/* export taxe */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) && isset( $_GET[ "taxe" ] ) ){
	
	$req = $_GET[ "req" ];
	$exportableArray = getTaxeExportableArray( $req );
	exportArray( $exportableArray );
	
	exit();
	
}

//------------------------------------------------------------------------------------------------
/* export client */

if( isset( $_GET[ "export" ] ) && isset( $_GET[ "req" ] ) && isset( $_GET[ "client" ] ) ){
	
	$req = $_GET[ "req" ];
	$exportableArray = getClientExportableArray( $req );
	exportArray( $exportableArray );
	
	exit();
	
}
//--------------------------------------------------------------------------------------------------

$Title = "Recherche sur les commandes";

include_once( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//--------------------------------------------------------------------------------------------------

$lang = User::getInstance()->getLang();

?>
			<script type="text/javascript">
			function changeSupplier( element ){ $('#idsupplier').val( element.info ); }
			function changeProvider( element ){ $('#provider_search').val( element.info ); }

			
			</script>
<div id="globalMainContent">
	<div class="mainContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<?php searchForm(); ?>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>
	<div id="tools" class="rightTools">
		<div class="toolBox">
			
		</div>
	</div>
	<div id="SearchResults"></div>
</div> <!-- GlobalMainContent -->
<?php

//--------------------------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//------------------------------------------------------------------------------------------------

function searchForm(){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
	$maxDate = DBUtil::query( "SELECT MAX( start_date ) AS maxDate FROM fiscal_years" )->fields( "maxDate" );
	
	$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime( 0, 0, 0, 1, 1, date( "Y" ) ) ),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ),
		"searchFor"			=> array( "customer", "supplier", "provider" ),
		"reference"			=> "",
		"commande"			=> "",
		"idbuyer" 			=> "",
		"idcateg" 			=> ""
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
?>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery.form.js"></script>
			<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/form.lib.js"></script>
			<script type="text/javascript">
			
			/* <![CDATA[ */
				
				/* ----------------------------------------------------------------------------------- */
				
				$(document).ready(function() { 
					
					 var options = {
					 	
						beforeSubmit:  preSubmitCallback,  // pre-submit callback 
						success:	   postSubmitCallback  // post-submit callback 
		  				
					};
					

					$('#SearchForm').ajaxForm( options );
					
				});
				
				/* ----------------------------------------------------------------------------------- */
				
				function preSubmitCallback( formData, jqForm, options ){
					
					document.getElementById( 'SearchForm' ).elements[ 'search' ].value = '1';
					document.getElementById( 'SearchForm' ).elements[ 'export' ].value = '0';
					
					$.blockUI({
						
						message: "Recherche en cours",
						css: { padding: '15px', cursor: 'pointer', 'font-weight': 'bold', 'font-family': 'Arial, Helvetica, sans-serif', 'color': '#586065', '-webkit-border-radius': '10px', '-moz-border-radius': '10px' },
						fadeIn: 0, 
						fadeOut: 700
						
					}); 
					
					$('.blockOverlay').attr('title','Click to unblock').click($.unblockUI);
					
				}
				
				/* ----------------------------------------------------------------------------------- */
				
				function postSubmitCallback( responseText, statusText ){ 
					
					$.unblockUI();
					
					if( statusText != 'success' || responseText == '0' ){
						
						alert( "Impossible d'effectuer la recherche" );
						return;
						
					}
					
					$('#SearchResults').html( responseText );
					
				}



				
				/* ----------------------------------------------------------------------------------- */
				
				function toggleSubAccounts( main_account ){
					
					if( $( ".subAccount_" + main_account ).css( "display" ) == "none" ){
						
						$( ".subAccount_" + main_account ).show();
						$( "#account_" + main_account ).addClass( "openedAccount" );
						
					}else{
						
						$( ".subAccount_" + main_account ).hide();
						$( "#account_" + main_account ).removeClass( "openedAccount" );
						
					}
					
				}
				
			
				/* ----------------------------------------------------------------------------------- */
				
			/* ]]> */
			</script>
			<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				
			/* ]]> */
			</style>
			<form action="<?php echo $_SERVER[ "PHP_SELF" ] ?>" method="post" id="SearchForm">
				<div class="headTitle">
					<p class="title">Recherche sur les commandes</p>
					<div class="rightContainer">
						<label style="vertical-align:top;"><input type="radio" name="searchFor[]" id="searchForTaxe" value="customer" style="vertical-align:middle;"<?php if( in_array( "customer", $postData[ "searchFor" ] ) ) echo " checked=\"checked\""; ?> /> Taxe</label>
						<label style="vertical-align:top;"><input type="radio" name="searchFor[]" id="searchForClient" value="supplier" style="vertical-align:middle;"/> Clients par catégorie</label>
						
					</div>
				</div>
				<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />

					<div class="tableContainer">
						<table class="dataTable">
							<tr>
								<th class="filledCell">Entre le</th>
								<td>
									<input type="text" name="start_date" id="start_date" class="calendarInput" value="<?php echo $postData[ "start_date" ] ?>" />
									<?php echo DHTMLCalendar::calendar( "start_date" ) ?>
									<!--<input type="hidden" name="start_date" id="start_date" class="calendarInput" value="<?php echo $postData[ "start_date" ] ?>" />-->
								</td>
								<th class="filledCell">et le</th>
								<td>
									<input type="text" name="end_date" id="end_date" class="calendarInput" value="<?php echo $postData[ "end_date" ] ?>" />
									<?php echo DHTMLCalendar::calendar( "end_date" ) ?>
								</td>
							</tr>
							<tr>
								<td colspan="4" class="tableSeparator"></td>
							</tr>
							<tr>
								<th class="filledCell"> N° de commande</th>
								<td><input type="text" name="commande" class="textInput" value="<?php echo $postData[ "commande" ] ?>" /></td>
								<th class="filledCell">N° de référence</th>
								<td>
									<input type="text" name="reference" class="textInput" value="<?php echo $postData[ "reference" ] ?>" />
									<!--<input type="hidden" name="idsupplier" id="idsupplier" />-->
									
								</td>
							</tr>
                            
                            <tr>
								<th class="filledCell"> Client n°</th>
								<td><input type="text" name="idbuyer" class="textInput" value="<?php echo $postData[ "idbuyer" ] ?>" /></td>
								
								<th class="filledCell"> catégorie n°</th>
								<td><input type="text" name="idcateg" class="textInput" value="<?php echo $postData[ "idcateg" ] ?>" /></td>

							</tr>
                            
                            
                            
                            
							
						</table>
					</div>
					<div class="submitButtonContainer">
						<input type="submit" class="blueButton" value="Rechercher" />
					</div>
				</div>
				<input type="hidden" name="detail" id="detail" value="1" />
			</form>
<?php
	
}

//------------------------------------------------------------------------------------------------

function searchTaxeResults(){
	
	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$maxSearchResults = 1000;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	
	//Les valeurs saisies dans les champs dates
 	$start_date = euDate2us( $_POST[ "start_date" ] );
 	$end_date = euDate2us( $_POST[ "end_date" ] );
	
	$select = "
	order_row.idorder, order_row.reference, order_row.summary, order_row.quantity, order_row.unit_price, order.DateHeure, ecotaxe_code, taxe_amount
	";
	
	$tables = "order_row, `order`, detail";
	
	//$where = "order_row.idorder = order.idorder AND order_row.reference = detail.reference AND detail.ecotaxe_code != '' ";
	$where = "order_row.idorder = order.idorder AND order_row.reference = detail.reference";
	
	//dates
	$where .= "\n\tAND order.DateHeure >= '$start_date'\n\tAND order.DateHeure <= '$end_date'";
	
	
	//Num de commande
	$ord = $_POST[ "commande" ];
	if( isset( $_POST[ "commande" ] ) &&  $_POST[ "commande" ] )
		
		$where .= "\n\tAND order_row.idorder = '$ord' ";
	
		
	//référence
	$ref = $_POST[ "reference" ];
	if( isset( $_POST[ "reference" ] ) &&  $_POST[ "reference" ] )
		
		$where .= "\n\tAND order_row.reference = '$ref' ";	
	
	
	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		";
	
	$rs =& DBUtil::query( $query );	
	
	$resultCount = $rs->RecordCount();
	
?>
<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php
				
				if( $resultCount > 0 ){
					
					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
					
					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
					
					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/administration/tools/order_search.php?export&amp;taxe&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php } 
				
				if ( $resultCount == 1 ){
				?>
				<p class="title">Taxe mobilier: <?php echo $resultCount ." ". "Résultat trouvé "; ?></p>
                <?php } else {?>
                <p class="title">Taxe mobilier: <?php echo $resultCount ." ". "Résultats trouvés "; ?></p>
                <?php } ?>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ ?>Aucune ligne trouvée
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> lignes ont été trouvées<br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/administration/tools/order_search.php?export&amp;taxe&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th class="filledCell">Commande n°</th>
								<th class="filledCell">Référence</th>
								<th  class="filledCell">Désignation</th>
								<th  class="filledCell">Quantité</th>
                                <th  class="filledCell">Code taxe</th>
                                <th  class="filledCell">Montant taxe</th>
                                <th  class="filledCell">Date</th>
                                
							</tr>
					
							
						</thead>
						<tbody>
							<?php 
							//$i=0;
							$arrayDate=array();
							while( !$rs->EOF ){ 
								if ( $rs->fields( "ecotaxe_code" ) ){
								
								$arrayDate=explode(" ",$rs->fields( "DateHeure" ));
							?>
                                <tr>
                                    <td class="lefterCol"><?php echo $rs->fields( "idorder" ) ?></td>
                                    <td class="lefterCol"><?php echo $rs->fields( "reference" ) ?></td>
                                    <td class="lefterCol"><?php echo $rs->fields( "summary" ) ?></td>
                                    <td class="lefterCol"><?php echo $rs->fields( "quantity" ) ?></td>
                                    <td class="lefterCol"><?php echo $rs->fields( "ecotaxe_code" ) ?></td>
                                    <td class="lefterCol"><?php echo Util::priceFormat ( $rs->fields( "taxe_amount" ) ) ?></td>
                                    <td class="lefterCol"><?php echo  $arrayDate[0] ?></td>
                                    
                                </tr>
								<?php
								//$i++;
								}
							$rs->MoveNext();
								
							}
								?>
						</tbody>
					</table>
				</div><?php } ?>
               <!-- <p class="title" style=" float:right; font-size:12px; font-weight:bold;">(<?php echo " " . $i . " " . "Résultats trouvés" . " ";?>)</p> -->
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>	 
<?php
	
}
//------------------------------------------------------------------------------------------------

function searchClientcategResults(){
	
	global	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$maxSearchResults = 1000;
	
	$lang = User::getInstance()->getLang();
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	
	
	//@todo: ces dates ne sont pas utilisées. Voir comment les utiliser
 	$start_date = euDate2us( $_POST[ "start_date" ] );
 	$end_date = euDate2us( $_POST[ "end_date" ] );
	
	$select = "
	order_row.idorder, order_row.reference, order_row.summary, order_row.quantity, order_row.unit_price,category.idcategory, category.name_1, order.DateHeure, order.idbuyer, buyer.naf
	";
	
	$tables = "order_row, `order`, category, `buyer`";
	
	$where = "order_row.idorder = order.idorder AND order_row.idcategory = category.idcategory AND buyer.idbuyer = order.idbuyer ";
	
	
	//dates
	$where .= "\n\tAND order.DateHeure >= '$start_date'\n\tAND order.DateHeure <= '$end_date'";
	
	
	//Num de commande
	$ord = $_POST[ "commande" ];
	if( isset( $_POST[ "commande" ] ) &&  $_POST[ "commande" ] )
		
		$where .= "\n\tAND order_row.idorder = '$ord' ";
	
	
	
	
	
	//référence
	$ref = $_POST[ "reference" ];
	if( isset( $_POST[ "reference" ] ) &&  $_POST[ "reference" ] )
		
		$where .= "\n\tAND order_row.reference = '$ref' ";
		
		
	//Client
	$buy = $_POST[ "idbuyer" ];
	if( isset( $_POST[ "idbuyer" ] ) &&  $_POST[ "idbuyer" ] )
		
		$where .= "\n\tAND order.idbuyer = '$buy' ";	
		
	//Categorie
	$idcategory = $_POST[ "idcateg" ];
	if( isset( $_POST[ "idcateg" ] ) &&  $_POST[ "idcateg" ] )
		
		$where .= "\n\tAND category.idcategory = '$idcategory' ";	
	
	
	$query = "
		SELECT $select
		FROM $tables
		WHERE $where
		";
	
	$rs =& DBUtil::query( $query );

	$resultCount = $rs->RecordCount();
	
?>
<div class="mainContent fullWidthContent">
		<div class="topRight"></div><div class="topLeft"></div>
		<div class="content">
			<div class="headTitle">
				<?php
				
				if( $resultCount > 0 ){
					
					include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
					
					$encryptedQuery = URLFactory::base64url_encode( Encryptor::encrypt( $query, $GLOBAL_DB_PASS ) );
					
					?>
					<div class="rightContainer"><a href="<?php echo $GLOBAL_START_URL ?>/administration/tools/order_search.php?export&amp;client&amp;req=<?php echo $encryptedQuery ?>"><img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a></div>
				<?php }
				if ( $resultCount == 1 ){
				?>
				<p class="title">Clients par catégorie et code Naf: <?php echo $resultCount ." ". "Résultat trouvé "; ?></p>
                <?php } else {?>
                <p class="title">Clients par catégorie et code Naf: <?php echo $resultCount ." ". "Résultats trouvés "; ?></p>
                <?php } ?>
			</div>
			<div class="subContent">
				<?php if( !$resultCount ){ ?>Aucune ligne trouvée
				<?php }elseif( $resultCount > $maxSearchResults ){ ?>Plus de <?php echo $maxSearchResults ?> lignes ont été trouvées<br />Merci de bien vouloir affiner votre recherche<br /><a href="<?php echo $GLOBAL_START_URL ?>/administration/tools/order_search.php?export&amp;client&amp;req=<?php echo $encryptedQuery ?>">Vous pouvez tout de même exporter les données<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/csv_icon.gif" alt="Exporter les données au format CSV" /></a>
				<?php }else{ ?><div class="resultTableContainer">
					<table class="dataTable resultTable">
						<thead>
							<tr>
								<th class="filledCell">Commande n°</th>
                              					<th class="filledCell">Client n°</th>
                              					<th class="filledCell">Mail</th>
								<th class="filledCell">Référence</th>
								<th  class="filledCell">Désignation</th>
								<th  class="filledCell">Quantité</th>
                                <th  class="filledCell">Catégorie n°</th>
                                <th  class="filledCell">Nom de catégorie</th>
                                <th  class="filledCell">Code Naf</th>
                                <th  class="filledCell">Date</th>
                                
							</tr>
					
							
						</thead>
						<tbody>
							<?php 
							$arrayDate=array();
							//$i=0;
							while( !$rs->EOF ){
							
							//if ( $rs->fields( "naf" ) ){
							 
							 $q1 = "SELECT mail FROM contact WHERE idbuyer = '".$rs->fields( "idbuyer" )."'";
							 $rs1 =& DBUtil::query( $q1 );
							 
							 
							$arrayDate=explode(" ",$rs->fields( "DateHeure" ));
							
							?>
							<tr>
								<td class="lefterCol"><?php echo $rs->fields( "idorder" ) ?></td>
                                <td class="lefterCol"><?php echo $rs->fields( "idbuyer" ) ?></td>
                                <td class="lefterCol"><?php echo $rs1->fields( "mail" ) ?></td>
                                <td class="lefterCol"><?php echo $rs->fields( "reference" ) ?></td>
                                <td class="lefterCol"><?php echo $rs->fields( "summary" ) ?></td>
                                <td class="lefterCol"><?php echo $rs->fields( "quantity" ) ?></td>
                                 <td class="lefterCol"><?php echo $rs->fields( "idcategory" ) ?></td>
                                <td class="lefterCol"><?php echo $rs->fields( "name_1" ) ?></td>
                                <td class="lefterCol"><?php echo $rs->fields( "naf" ) ?></td>
                                <td class="lefterCol"><?php echo $arrayDate[0] ?></td>
								
							</tr>
							<?php
								//$i++;
							//	}
								
								$rs->MoveNext();
								
							}
							
							?>
						</tbody>
					</table>
				</div><?php } ?>
             <!--     <p class="title" style=" float:right; font-size:12px; font-weight:bold;">(<?php echo " " . $i . " " . "Résultats trouvés" . " ";?>)</p> -->
			</div>
		</div>
		<div class="bottomRight"></div><div class="bottomLeft"></div>
	</div>	 
<?php
	
}


//------------------------------------------------------------------------------------------------

function getTaxeExportableArray( $req ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "req" ] ), $GLOBAL_DB_PASS );
	
	$data = array(
		
		"Commande n°"		=> array(),
		"référence"			=> array(),
		"Désignation"		=> array(),
		"quantité"			=> array(),
		"Code taxe"			=> array(),
		"Montant taxe"		=> array(),
		"Date"				=> array()
		
	);
	
	$rs =& DBUtil::query( $query );
	
	$arrayDate=array();
	
	while( !$rs->EOF() ){
		$arrayDate=explode(" ",$rs->fields( "DateHeure" ));
		
		
			$data[ "Commande n°" ][]	= $rs->fields( "idorder" );
			$data[ "référence" ][]		= $rs->fields( "reference" );
			$data[ "Désignation" ][]	= $rs->fields( "summary" );
			$data[ "quantité" ][]		= $rs->fields( "quantity" );
			$data[ "Code taxe" ][]		= $rs->fields( "ecotaxe_code" );
			$data[ "Montant taxe" ][]	= $rs->fields( "taxe_amount" );
			$data[ "Date" ][]			= $arrayDate[0];
			
			$rs->MoveNext();
		
	}
	
	return $data;
	
}

//---------------------------------------------------------------------------------------------

function getClientExportableArray( $req ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$query = Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "req" ] ), $GLOBAL_DB_PASS );
	//echo $query;
	$data = array(
		
		"Commande n°"		=> array(),
		"Client n°"			=> array(),
		"Email"			=> array(),
		"référence"			=> array(),
		"Désignation"		=> array(),
		"quantité"			=> array(),
		"Catégorie n°"		=> array(),
		"Nom de catégorie"	=> array(),
		"Code Naf"			=> array(),
		"Date"				=> array()
		
	);
	
	$rs =& DBUtil::query( $query );
	$arrayDate=array();
	
	while( !$rs->EOF() ){
		$arrayDate=explode(" ",$rs->fields( "DateHeure" ));
		$q1 = "SELECT mail FROM contact WHERE idbuyer = '".$rs->fields( "idbuyer" )."'";
		$rs1 =& DBUtil::query( $q1 );
							 
			$data[ "Commande n°" ][]		= $rs->fields( "idorder" );
			$data[ "Client n°" ][]			= $rs->fields( "idbuyer" );
			$data[ "Email" ][]			= $rs1->fields( "mail" );;
			$data[ "référence" ][]			= $rs->fields( "reference" );
			$data[ "Désignation" ][]		= $rs->fields( "summary" );
			$data[ "quantité" ][]			= $rs->fields( "quantity" );
			$data[ "Catégorie n°" ][]			= $rs->fields( "idcategory" );
			$data[ "Nom de catégorie" ][]			= $rs->fields( "name_1" );
			$data[ "Code Naf" ][]			= $rs->fields( "naf" );
			$data[ "Date" ][]				= $arrayDate[0];
			
			$rs->MoveNext();
		
	}
	
	return $data;
	
}

//--------------------------------------------------------------------------------


function exportArray( &$exportableArray ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/CSVExportOrderArray.php" );
	
	$cvsExportArray = new CSVExportOrderArray( $exportableArray, "Recherche_sur_commande", "csv", ";", "", "\r\n" );

	$cvsExportArray->export();
	
}

//--------------------------------------------------------------------------------

?>
