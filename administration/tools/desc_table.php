<?php 

/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire stdform gestion des tables 
 */

//paramétrages de base 
	$Int_LimitP = 5; 
	$pagetitle = "title_desc_table";
	$Str_TableName="desc_table";
  	$Array_KeyNames["table"]=""; //table key field


// Field columns that appear after searching in ShowRecordsBy	
	$ArrayFieldsColumns2Show[0]="tablename";
 	$ArrayFieldsColumns2Show[1]="description";
 	$ArrayFieldsColumns2Show[2]="export_avalaible";
 	$ArrayFieldsColumns2Show[3]="import_avalaible";
	
//show the page 
include "../../formbase/stdformbase.php"
?>