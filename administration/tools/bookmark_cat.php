<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire de gestion des catégories de bookmark
 */

// paramètres de base 
 	$Int_LimitP = 5; 
	$pagetitle = "gest_com_admin_bookmark_cat";
    $Str_TableName="bookmark_cat";
    $KeyName = 'idbookmark_cat';

// Field columns that appear after searching in ShowRecordsBy	
  	$ArrayFieldsColumns2Show[0]="idbookmark_cat";
	$ArrayFieldsColumns2Show[]="bookmark_cat_1";

// show the page 
	include "../../formbase/stdformbase.php";
	
	
?>
