<?php 

/**
 * Package chef produit
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire stdform gestion des champs 
 */

//paramétrages de base 
	$Int_LimitP = 5; 
	$pagetitle = "title_desc_field";
	$Str_TableName="desc_field";
  	$Array_KeyNames["tablename"]=""; //table key field
  	$Array_KeyNames["fieldname"]=""; //table key fiel


// Field columns that appear after searching in ShowRecordsBy	
	$ArrayFieldsColumns2Show[0]="tablename";
 	$ArrayFieldsColumns2Show[1]="fieldname";
 	$ArrayFieldsColumns2Show[2]="description";

	
//show the page 
include "../../formbase/stdformbase.php"
?>