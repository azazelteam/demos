<?php

$degree=$_GET[ "degree" ];

$Width = 190;
$Height = 93;

$Xcentre = $Width/2-2;
$Ycentre = $Height-15;

$Hfleche = 10;
$Wfleche = 60;
$Lfleche = 60;

$src_cadran = '../../images/widget_kpi_cadran.png';	

$img = imagecreatefrompng($src_cadran);




$white = imagecolorallocate($img, 255, 255, 255);
$black = imagecolorallocate($img, 0, 0, 0);

imageAlphaBlending($img, true);
imageSaveAlpha($img, true);

$x1 = $Xcentre;
$x2 = $x1 + cos(deg2rad($degree))*$Lfleche;
$y1 = $Ycentre; 
$y2 = $y1 - sin(deg2rad($degree))*$Lfleche;


imageline($img, $x1, $y1, $x2, $y2, $black);


Header("Content-type: image/png");

ImagePNG($img);

ImageDestroy($img);


?>