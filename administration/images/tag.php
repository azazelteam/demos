<?php
/*
$label 	= " " . stripslashes( html_entity_decode( $_GET[ "label" ] ) );
$red 	= isset( $_GET[ "r" ] ) ? intval( $_GET[ "r" ] ) : 255;
$green 	= isset( $_GET[ "g" ] ) ? intval( $_GET[ "g" ] ) : 0;
$blue 	= isset( $_GET[ "b" ] ) ? intval( $_GET[ "b" ] ) : 0;

$fontsize 		= 3;

$background_path 	= "tag_bg_pattern.jpg";
$left_path			= "tag_left_pattern.jpg";
$right_path 		= "tag_right_pattern.jpg";

if( !file_exists( $background_path )
	|| !file_exists( $left_path )
	|| !file_exists( $right_path ) )
	exit();
	
$background_infos 	= getimagesize( $background_path );
$left_infos 		= getimagesize( $left_path );
$right_infos 		= getimagesize( $right_path );

$img_background = ImageCreateFromJpeg( $background_path );
$img_left 	= ImageCreateFromJpeg ( $left_path );
$img_right 	= ImageCreateFromJpeg ( $right_path );

$height = $background_infos[ 1 ];
$width 	= $left_infos[ 0 ] + ( strlen( $label ) + 1 ) * imagefontwidth( $fontsize ) + $right_infos[ 0 ];

$img 	= imagecreatetruecolor( $width, $height );
$color 	= ImageColorAllocate ( $img, $red, $green, $blue );

$i = 0;
while( $i < $width ){
	
	imageCopyMerge( $img, $img_background, $i, 0, 0, 0, $width, $height, 100) ;
	$i += $background_infos[ 0 ];
	
}


imageCopyMerge( $img, $img_left, 0, 0, 0, 0, $left_infos[ 0 ], $left_infos[ 1 ], 100 );
imageCopyMerge( $img, $img_right, $width - $right_infos[ 0 ], 0, 0, 0, $right_infos[ 0 ], $right_infos[ 1 ], 100 );

imageString( $img, $fontsize, $left_infos[ 0 ],( $height - imagefontheight( $fontsize ) ) / 2, $label, $color );

header( "Content-type: image/jpeg" );

ImageJPEG( $img,"",100 );

ImageDestroy( $img );
*/

$label 	= " " . stripslashes( html_entity_decode( $_GET[ "label" ] ) );

//background

$bgred 		= isset( $_GET[ "bgr" ] ) ? intval( $_GET[ "bgr" ] ) : 238;
$bggreen 	= isset( $_GET[ "bgg" ] ) ? intval( $_GET[ "bgg" ] ) : 238;
$bgblue 	= isset( $_GET[ "bgb" ] ) ? intval( $_GET[ "bgb" ] ) : 238;

//foreground

$red 	= isset( $_GET[ "r" ] ) ? intval( $_GET[ "r" ] ) : 80;
$green 	= isset( $_GET[ "g" ] ) ? intval( $_GET[ "g" ] ) : 80;
$blue 	= isset( $_GET[ "b" ] ) ? intval( $_GET[ "b" ] ) : 80;


$fontsize 	= 8;

//create image

//$width 		= 5 + ( strlen( $label ) + 1 ) * imagefontwidth( $fontsize ) + 5;
$width 		= strlen( $label ) * (imagefontwidth( $fontsize ) - 2);
$height 	= 15;
$img 		= imagecreatetruecolor( $width, $height );

$bgColor = ImageColorAllocate ( $img, $bgred, $bggreen, $bgblue );
imageFill( $img, 0, 0, $bgColor );

$color 	= ImageColorAllocate ( $img, $red, $green, $blue );
//imageString( $img, 2, 5,( $height - imagefontheight( $fontsize ) ) / 2, $label, $color );
//putenv('GDFONTPATH=' . realpath('.'));
$fontfile = "/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf";
//die($height.",".imagefontheight( $fontsize ).",".( $height - imagefontheight( $fontsize ) ) / 2);
imagettftext ( $img , $fontsize , 0 , 5,( $height + $height / 2) / 2 , $color,  $fontfile , $label ) ;

header( "Content-type: image/jpeg" );

imagejpeg( $img);
imagedestroy( $img );

				
?>