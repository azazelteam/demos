<?php

$label 	= " " . stripslashes(html_entity_decode( $_GET[ "label" ] ));
$red 	= isset( $_GET[ "r" ] ) ? intval( $_GET[ "r" ] ) : 100;
$green 	= isset( $_GET[ "g" ] ) ? intval( $_GET[ "g" ] ) : 100;
$blue 	= isset( $_GET[ "b" ] ) ? intval( $_GET[ "b" ] ) : 100;

$fontsize = 3;

$background_path 	= "default_button_bg.jpg";
$left_path			= "default_button_left.jpg";
$right_path 		= "default_button_right.jpg";

if( !file_exists( $background_path )
	|| !file_exists( $left_path )
	|| !file_exists( $right_path ) )
	exit();
	
$background_infos 	= getimagesize( $background_path );
$left_infos 		= getimagesize( $left_path );
$right_infos 		= getimagesize( $right_path );

$img_background = ImageCreateFromJpeg( $background_path );
$img_left 	= ImageCreateFromJpeg ( $left_path );
$img_right 	= ImageCreateFromJpeg ( $right_path );

$height = $background_infos[ 1 ];
$width 	= $left_infos[ 0 ] + ( strlen( $label ) + 1 ) * imagefontwidth( $fontsize ) + $right_infos[ 0 ];

$img 	= imageCreate( $width, $height );
$color 	= ImageColorAllocate ( $img, $red, $green, $blue );

$i = 0;
while( $i < $width ){
	
	imageCopyMerge( $img, $img_background, $i, 0, 0, 0, $width, $height, 100) ;
	$i += $background_infos[ 0 ];
	
}


imageCopyMerge( $img, $img_left, 0, 0, 0, 0, $left_infos[ 0 ], $left_infos[ 1 ], 100 );
imageCopyMerge( $img, $img_right, $width - $right_infos[ 0 ], 0, 0, 0, $right_infos[ 0 ], $right_infos[ 1 ], 100 );

imageString( $img, $fontsize, $left_infos[ 0 ],( $height - imagefontheight( $fontsize ) ) / 2, $label, $color );

header( "Content-type: image/jpeg" );

ImageJPEG( $img );

ImageDestroy( $img );
		
?>