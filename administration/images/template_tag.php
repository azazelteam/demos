<?php

$label 	= " " . $_GET[ "label" ];

$antialiasing 	= 1; //1 ou -1
$fontsize 		= 9;
$fontfile 		= "../../fonts/entsans.ttf";
$red 			= 255 * $antialiasing;
$green 			= 0 * $antialiasing;
$blue 			= 0 * $antialiasing;

$bbox = imagettfbbox ( $fontsize, 0, $fontfile, $label );

/*
0 	Coin inférieur gauche, abscisse
1 	Coin inférieur gauche, ordonnée
2 	Coin inférieur droit, abscisse
3 	Coin inférieur droit, ordonnée
4 	Coin supérieur droit, abscisse
5 	Coin supérieur droit, ordonnée
6 	Coin supérieur gauche, abscisse
7 	Coin supérieur gauche, ordonnée
*/

//pattern

$pattern_path	= "button_left.jpg";
$pattern_infos 	= getimagesize( $pattern_path );
$pattern_height = $pattern_infos[ 1 ];//$bbox[ 6 ] - $bbox[ 0 ];
$pattern_width 	= $pattern_infos[ 0 ];//$bbox[ 2 ] - $bbox[ 0 ];

$img_pattern 	= ImageCreateFromJpeg ( $pattern_path );


$height = max( $pattern_infos[ 1 ], $pattern_height );
$width 	= $pattern_infos[ 0 ] + ( $bbox[ 2 ] - $bbox[ 0 ] );

$img 		= imageCreate( $width, $height );
$white 		= ImageColorAllocate ( $img, 255, 255, 255 );

imageCopyMerge( $img, $img_pattern, 0, 0, 0, 0, $pattern_width, $pattern_height, 100 );

$fontcolor 	= ImageColorAllocate ( $img, $red, $green, $blue );
imagettftext ( $img, $fontsize, 0, $pattern_width, ( $height / 2 ) + ( $fontsize / 2 ), $fontcolor, $fontfile, $label );

header( "Content-type: image/jpeg" );

ImageJPEG( $img, "", 100 );

ImageDestroy( $img );
		
?>