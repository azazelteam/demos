<?php 

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des redirections
 */

 
date_default_timezone_set("Europe/Paris");
include_once(dirname(__FILE__)."/../config/init.php" );
include_once(dirname(__FILE__)."/find-redirection.php" );
include("$GLOBAL_START_PATH/templates/back_office/head.php");
;?>				
<link rel="stylesheet" type="text/css" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/form_default.css" />

<link rel="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/back_office/queryLoader.css"  type="text/css" />
<script type='text/javascript' src='<?php echo $GLOBAL_START_URL ?>/js/queryLoader.js'></script>

<?php 
if (isset($_POST["action"]) &&  ($_POST["action"] == "add"))
{
	if (!empty($_POST["source"]) && (!empty($_POST["cible"])) && ($_POST["source"] != "/"))
	{
		$date_add = date("Y-m-d H:i:s");
		$source = $_POST['source'];
		if (Session::getInstance()->getCustomer()) 
			$source = str_replace(" ", "", $source);
		$source = str_replace(" ", "", $source);
		$cible = str_replace(" ", "", $_POST["cible"]);
		$host = $_SERVER["HTTP_HOST"];
		if (strpos($host, "www") !== false)
			$host = substr($host, 4);
		$pattern = '/@^(?:http://)?([^/]+)@i/';
		if (strpos($source, $host) !== false)
		{
			$index = substr($source, 0, 1);
			if ($index == "/")
			{
				$source = substr($source, 1);
			}
			$matches_source = preg_match($pattern, $source, $match);
			if ($matches_source)
			{
				$source = substr($source, strlen($match["0"]) + 1);
			}
		}
		else
		{
			$index = substr($source, 0, 1);
			if ($index == "/")
			{
				$source = substr($source, 1);
			}
		}
		if (strpos($cible, $host) !== false)
		{
			$index = substr($cible, 0, 1);
			if ($index == "/")
			{
				$cible = substr($cible, 1);
			}
			$matches_cible = preg_match($pattern, $cible, $match);
			if ($matches_cible)
			{
				$cible = substr($cible, strlen($match["0"]));
			}
		}
		else
		{
			$index = substr($cible, 0, 1);
			if ($index != "/")
			{
				$cible = "/".$cible;
			}
		}
		$user_id = User::getInstance()->getId();
		$type = (!empty($_POST["type"])) ? $_POST["type"] : 301;
		if ($source != substr($cible, 1))
		{
			$rs_items = DBUtil::query("SELECT id, url_source, url_cible FROM `redirection_items` WHERE `statut`=1");
			$sub_cible = substr($cible, 1);
			$items = array(
				"id" => array(),
				"url_source" => array(),
				"url_cible" => array()
			);
			while (!$rs_items->EOF())
			{
				$items["id"][] = $rs_items->fields("id");
				$items["url_source"][] = str_replace("$", "", $rs_items->fields("url_source"));
				$items["url_cible"][] = $rs_items->fields("url_cible");
				$rs_items->MoveNext();
			}
			//Notation commentaire
			//NS:Nouvelle Source, NC:Nouvelle Cible, AS:Ancienne source, AC:Ancienne Cible
			if ( in_array($source, $items["url_source"]) )//NS == AS
			{
				$retour = "L'URL source existe d&eacute;j&agrave;!!";
			}
			else
			{
				if ( in_array($sub_cible, $items["url_cible"]) )//NS != AS && NC == AC
				{
					$rs_insert = DBUtil::query("INSERT INTO `redirection_items`(`url_source`, `url_cible`, `statut`, `type`,`date_add`, `user_id`) VALUES('$source','$cible',1,$type,'$date_add',$user_id)");
				}
				else
				{
					if ( in_array($sub_cible, $items["url_source"]) && in_array('/'.$source, $items["url_cible"]) )//NS != AS && NC != AC && NC == AS && NS == AC
					{
						$retour = "Cette redirection pr&eacute;sente une boucle de redirection!!";
					}
					else if ( in_array($sub_cible, $items["url_source"]) )//NS != AS && NC != AC && NC == AS && NS != AC => insert (NS->AC) 
					{
						$item_keys = array_keys($items["url_source"], $sub_cible);
						$key = $item_keys[0];
						$old_cible = $items["url_cible"][$key];
						$rs_insert = DBUtil::query("INSERT INTO `redirection_items`(`url_source`, `url_cible`, `statut`, `type`,`date_add`, `user_id`) VALUES('$source','$old_cible',1,$type,'$date_add',$user_id)");
					}
					else if ( in_array('/'.$source, $items["url_cible"]) )//NS != AS && NC != AC && NC != AS && NS == AC
					{
						$item_keys = array_keys($items["url_cible"], '/'.$source);
						foreach ( $item_keys as $key )
						{
							$rs_update = DBUtil::query( "UPDATE `redirection_items` SET `type`= ".intval($type).",`url_cible`='".$cible."',`lastupdate`='".$date_add."' WHERE `id` = '".intval($items["id"][$key])."' LIMIT 1" );
						}
						$rs_insert = DBUtil::query("INSERT INTO `redirection_items`(`url_source`, `url_cible`, `statut`, `type`,`date_add`, `user_id`) VALUES('$source','$cible',1,$type,'$date_add',$user_id)");
					}
					else//NS != AS && NC != AC && NC != AS && NS != AC 
					{
						$rs_insert = DBUtil::query("INSERT INTO `redirection_items`(`url_source`, `url_cible`, `statut`, `type`,`date_add`, `user_id`) VALUES('$source','$cible',1,$type,'$date_add',$user_id)");
					}
				}
			}
			if ($rs_insert)
			{
				$redirect = new Redirect();
				$redirect->generateHtacess();
			}
		}
		else $retour = "Cette redirection pr&eacute;sente une boucle de redirection!!";
	}
	else $retour = "Veuillez verifier!!!L'un des champs est vide ou format inacceptable!!";
}
if (isset($_REQUEST["id_item"]) && ($_REQUEST["action"] == "update"))
{
	//var_dump($_REQUEST);exit();
	if (!empty($_REQUEST["url_source"]) && (!empty($_REQUEST["url_cible"])) && ($_POST["url_source"] != "/"))
	{	
		$date_update = date("Y-m-d H:i:s");
		$id = $_REQUEST["id_item"];
		$type1 = (!empty($_REQUEST["type"])) ? $_REQUEST["type"] : 301;
		$url_source = str_replace(" ", "", $_REQUEST["url_source"]);
		$url_cible = str_replace(" ", "", $_REQUEST["url_cible"]);
		$host = $_SERVER["HTTP_HOST"];
		if (strpos($host, "www") !== false)
			$host = substr($host, 4);
		$pattern = '/@^(?:http://)?([^/]+)@i/';
		if (strpos($url_source, $host) !== false)
		{
			$index = substr($url_source, 0, 1);
			if ($index == "/")
			{
				$url_source = substr($url_source, 1);
			}
			$matches_source = preg_match($pattern, $url_source, $match);
			if ($matches_source)
			{
				$url_source = substr($url_source, strlen($match["0"]) + 1);
			}
		}
		else
		{
			$index = substr($url_source, 0, 1);
			if ($index == "/")
			{
				$url_source = substr($url_source, 1);
			}
		}
		if (strpos($url_cible,$host) !== false)
		{
			$index = substr($url_cible, 0, 1);
			if ($index == "/")
			{
				$url_cible = substr($url_cible, 1);
			}
			$matches_cible = preg_match($pattern, $url_cible, $match);
			if ($matches_cible)
			{
				$url_cible = substr($url_cible, strlen($match["0"]));
			}
		}
		else{
			$index = substr($url_cible, 0, 1);
			if ($index != "/")
			{
				$url_cible = "/".$url_cible;
			}
		}
	if ($url_source != substr($url_cible, 1))
	{
		$query1 = "select * from redirection_items where BINARY url_source='$url_source' and id<>$id ";
		$rs1 = & DBUtil::query($query1);
		if (!$rs1->RecordCount())
		{
			$ret = DBUtil::query( "UPDATE `redirection_items` SET `type`= ".intval($type1).",`url_source`='".$url_source."',`url_cible`='".$url_cible."',`lastupdate`='".$date_update."' WHERE `id` = '".intval($_REQUEST["id_item"])."' LIMIT 1" );
		//exit( $ret === false ? "0" : "1" );
			if ($ret)
			{
				$redirect = new Redirect();
				$redirect->generateHtacess();
			}
		}
		else $retour = "Cette redirection exite d&eacute;j&agrave;!!";
	}
	else $retour = "Cette redirection pr&eacute;sente une boucle de redirection!!";
}
else $retour = "Veuillez verifier!!!L'un des champs est vide ou format inacceptable!!";
}
?>
<form name="search_form" action="/administration/manage_redirection.php" method="post">	
<input type="hidden" name="action"  value="add" />
<div class="centerMax">
<div id="tools" class="rightTools">
</div>	
<div class="contentDyn">
	<!-- Titre -->
	<h1 class="titleSearch">
	<span class="textTitle">Ajouter une nouvelle redirection</span>
	<div class="spacer"></div>
	</h1>		
	<div class="blocSearch"><div style="margin:5px;">
		<!-- Left -->
	<center>	<div class="blocMultiple ">
		<div  id="div_retour" style="color:red;" ><?php if (!empty($retour)) echo $retour;?></div>
			<label><span><strong>URL source</strong></span></label>
			<input type="text" name="source" class="textInput" value="" />
			<div class="spacer"></div>
			<label><span>Type</span></label>
			<select name="type">
				<option value="301" >301</option>
				<option value="302" >302</option>
			</select>
			<div class="spacer"></div>
			<label><span>URL cible</span></label>
			<input type="text" name="cible" class="textInput" value="" />
			<div class="spacer"></div>
		<div class="spacer"></div>
        <div class="spacer"></div>
        <input type="submit" class="blueButton" name="valider" style="height:28px;float: right;" value="Ajouter la redirection" />    
        <div class="spacer"></div> 
	</div></center></div>
	<div class="spacer"></div>
</form>
</div> 
</div>       		      	
<div class="spacer"></div>
<?php
$query = "select * from redirection_items";
$rs = & DBUtil::query($query);
if ($rs->RecordCount())
{	
?>
<div class="contentResult" id="ResultBuyer" >
		<a name="top"></a><a name="Results"></a>
			<h1 class="titleSearch">
				<span class="textTitle" id="caSearch">Redirections: </span>
				<span class="selectDate">
					<a href="#bottom" class="goUpOrDown" style="font-size:10px; color:#000000;">
		            	Aller en bas de page
						<img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_down.gif" alt="Aller en bas de page" />
					</a>
				</span>
				<div class="spacer"></div>
			</h1>
			<div class="blocResult mainContent">
			<div class="content">
				<!-- tableau résultats recherche -->
				<table class="dataTable resultTable">
					<thead>
						<tr>
							<?php /*Contact principal ou pas. <th>Com</th> */ ?>
                           <!-- <th>#</th>
                            <th>ID</th>		-->				
							<th>Type</th>
							<th>URL source</th>
							<th>URL cible</th>
							<th>Statut</th>
							<th>Ajout&eacute; par</th>
							<th></th>
							<!--<th>Date d'Ajout</th>
							<th>Dernière modification</th>-->
						</tr>
						<!-- petites flèches de tri -->
					</thead>
					<tbody>
					<?php
					$x = 1;	
					while (!$rs->EOF())
					{ 	$id = $rs->fields("id");
						$type = $rs->fields("type");
						$cible = $rs->fields("url_cible");
						$source = $rs->fields("url_source");
						$statut = $rs->fields("statut");
						$user = $rs->fields("user_id");
						$date_add = $rs->fields("date_add");
						$date_update = $rs->fields("last_update");

						?>
						<tr id="tr<?php echo $id; ?>" style="">
							<input type="hidden" id="statut<?php echo $id; ?>" name="statut<?php echo $id; ?>" value="<?php echo $statut; ?>" />
                  <!--       <td><?php echo $x; ?></td>
                         <td><?php echo $id; ?></td>  -->
						<td><?php echo $type ?></td>
						<td><?php echo $source?></td>
						<td><?php echo $cible ?></td>
						<td id="td<?php echo $id; ?>"> <?php if ($statut == 1) echo "Active"; else echo "Non active"; ?></td>
						<td><?php echo $user ?></td>
						<td><img id="ValidateIcon<?php echo $id; ?>" onclick="validateItem(<?php echo $id; ?> );" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/<?php echo $rs->fields( "statut" ) ? "visible.gif" : "hidden.gif"; ?>" alt="" />
					<img id="UpdateIcon" onclick="enableEdit(<?php echo $id; ?>);" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/icons-edit.png" alt="" />

					<img class="recycle" src="<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/corbeille.jpg" alt="" style="cursor:pointer;" onclick="deleteItem( <?php echo $id; ?> );" />
					</td>
						<!--<td><?php echo $date_add?></td>
						<td><?php echo $date_update ?></td>-->
						</tr>
				<tr id="tr1<?php echo $id; ?>"  style="display:none;">

						<td><?php echo $type ?></td>
						<td  COLSPAN=4 class="blocMultiple ">
							<form name="search_form" action="/administration/manage_redirection.php" method="post">	
						<input type="hidden" name="action"  value="update" />
							<input type="hidden" id="id_item<?php echo $id; ?>" name="id_item" value="<?php echo $id; ?>" />
					<label><span><strong>URL source</strong></span></label>
					<input type="text" style="width: 332px;" name="url_source" id="source<?php echo $id; ?>" class="textInput" value="<?php if(!empty($source)) echo $source; ?>" placeholder="http://" />
					
					<label><span>Type</span></label>
					<select name="type" id="type<?php echo $id; ?>">
						<option value="301" <?php if (!empty($type) && ($type == 301)) echo "selected" ;?>>301</option>
						<option value="302" <?php if (!empty($type) && ($type == 302)) echo "selected" ;?>>302</option>
					</select>
					<div class="spacer"></div>
					<label><span>URL cible</span></label>
					<input type="text" name="url_cible"  style="width: 332px;" id="cible<?php echo $id; ?>"class="textInput" value="<?php if (!empty($cible)) echo $cible; ?>"placeholder="http://" /><br>
					<input type="button"class="blueButton" name="annuler" style="height:28px;float: right;" value="Annuler" onclick="disableEdit(<?php echo $id; ?>);">
					<!--<input type="button"class="blueButton" name="valider" style="height:28px;float: right;" value="Modifier" onclick="updateItem(<?php echo $id; ?>);">-->
					<input type="submit"class="blueButton" name="modifier" style="height:28px;float: right;" value="Modifier" >
				</form>
							</td>
							<td ></td>
							</tr>
						<?php
						$rs->MoveNext();
						$x++;
						}
						?>
				</tbody>
				</table>  
			</div>
			</div>                
	        <div class="spacer"></div>
	           
	        <h1 class="titleSearch">
			<span class="textTitle">&nbsp;</span>
			<span class="selectDate">
				<a href="#top" class="goUpOrDown" style="font-size:10px; color:#000000;">
	            	Aller en haut de page
	                <img src="<?php echo $GLOBAL_START_URL ?>/images/back_office/content/shape_up.gif" alt="Aller en haut de page" />
	            </a>
			</span>
			<div class="spacer"></div>
			</h1>	
</div>

<div class="spacer"></div><br/>
<a name="bottom"></a>	
</div><!-- centerMax -->
</form>
</div>   
<?php }?>
<script type="text/javascript">
/* <![CDATA[ */
	function validateItem(id){
		var validate = $("#statut"+id).val() == 1 ? 0 : 1;
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL; ?>/administration/manage_redirection_items.php?id_item="+id+"&validate="+validate,
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function(XMLHttpRequest, textStatus, errorThrown){ alert(textStatus); },
		 	success: function(responseText){

				if (responseText != "1")
					alert(responseText);
				else
				{
					$("#ValidateIcon"+id).attr("src", validate ? "<?php echo $GLOBAL_START_URL; ?>/images/back_office/content/visible.gif" : "<?php echo $GLOBAL_START_URL; ?>/images/hidden.gif");
					$("#statut"+id).val(validate);
					if (validate == 1)
					{
						$("#td"+id).text("Active");
					}
					else 
					{
						$("#td"+id).text("Non active");
					}
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI('', 'Modifications enregistrées');
				}
			}
		});
	}
	function deleteItem(id){
		$.ajax({
			url: "<?php echo $GLOBAL_START_URL; ?>/administration/manage_redirection_items.php?id_item=" + id +"&delete",
			async: true,
			cache: false,
			type: "GET",
			data: "",
			error : function( XMLHttpRequest, textStatus, errorThrown ){ alert(textStatus); },
		 	success: function(responseText){
				if (responseText != "1")
					alert(responseText);
				else
				{
					$("#tr" + id).remove();
					$.blockUI.defaults.css.fontFamily = 'arial, "lucida console", sans-serif';
					$.blockUI.defaults.css.fontSize = '12px';
	        		$.growlUI('', 'Modifications enregistrées');
				}
			}
		});
	}
	function enableEdit(id)
	{
	  document.getElementById('tr'+id).style.display = "none";
	  document.getElementById('tr1'+id).style.display = "";
	}
	function disableEdit(id)
	{
		document.getElementById('tr'+id).style.display = "";
		document.getElementById('tr1'+id).style.display = "none";
	}
/* ]]> */
</script>
<?php
/****************************************************************************************************************************************/
	include("$GLOBAL_START_PATH/templates/back_office/foot.php");
?>