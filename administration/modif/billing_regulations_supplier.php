<?php
// Paramètres de base 
	$Str_TableName = "billing_regulations_supplier";
	$pagetitle = "title_billing_regulations_supplier";
	$Int_LimitP = 5; // Limite de produits a afficher sur la page, au moins 1
	$KeyName = 'billing_supplier, idregulations_supplier ';

// Field columns that appear after searching in ShowRecordsBy
	$ArrayFieldsColumns2Show[0]="idregulations_supplier";
	$ArrayFieldsColumns2Show[1]="billing_supplier";
	$ArrayFieldsColumns2Show[2]="amount";
	

// Affichage de l'icone d'aide à côté du titre
	//$showHelp = true;
// Affichage lien "Recherche: Id acheteur ?"
	//$search = "search_id";


// show the page 
include "../../formbase/stdformbase.php";
?>
