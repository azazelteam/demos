<?php

include( dirname( __FILE__ ) . "/../../config/init.php" );

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

?>
<div id="globalMainContent">
	<div class="mainContent" style="width:700px; margin-left:30px;">
        <div class="topRight"></div><div class="topLeft"></div>
        <div class="content">
        	
               
                <div class="rightContainer">
                
            </div>
           <!-- <div class="subContent">-->
<div style="margin-left:30px;margin-top:30px; font-size: 12px; color: black;">
<style type="text/css">

ul {
 font-family: Arial, sans-serif;
 font-size: 12px;
 color: black;
 background-position: 10px;
 padding-left: 0px;
 margin-left: 0px;
} 

li {
 font-family: Arial, sans-serif;
 font-size: 100%;
 color: black;
 background-position: 0px;
 padding-left: 0px;
 margin-left: 30px;
} 
</style>


<div class="headTitle">
 <p class="title">Manipulations diverses</p>
</div>

<br />
<ul><strong>Transférer une cde frs d`un fournisseur vers un autre :</strong> 
<li>Modifier le fournisseur : Corrections / Commandes fournisseurs / Entete cde frs </li>
<li>Modifier le fournisseur : Corrections / Bons de livraison / Entete BL </li>
</ul>
<br />

<ul><strong>Transférer un ligne cde frs vers une autre cde frs :</strong> 
<li>Modifier le n° de cde frs : Corrections / Bons de livraison / Lignes BL  </li>
<li>modifier le n° de cde frs : Corrections / Commandes fournisseurs / Lignes cdes frs </li>
</ul>
<br />

<ul><strong>Erreur sur la quantité expédiée par le logisticien :</strong> 
<li>Modifier la quantité : Corrections / Bons de livraison / Lignes BL  </li>
<li>modifier la quantité : Corrections / Commandes fournisseurs / Lignes cdes frs </li>
<li>Modifier le stock dans la fiche produit</li>
</ul>
<br />

<ul><strong>Refacturer une facture frs déjà facturée suite à un avoir</strong>
<li>Demander à ladministrateur de mettre bl_delivery = 0 dans la table billing_supplier_row</li>
</ul>
<br>

<strong>Gestion des cdes frs démo passés en cde frs </strong><br />
Cas où le client garde le produit de démo et quil faut déduire de la nouvelle cde frs et BL
<ul>
<li>Affecter la cde frs démo à la commande client : Corrections / Commandes fournisseurs</li>
<li>Affecter le bl démo à la commande client : Corrections / Bon de livraison / </li>
<li>Modifier la qté de la cde frs : Corrections / Lignes cde frs</li>
<li>Modifier la qté du BL : Corrections / Lignes BL</li>
</ul>
<br />

<ul><strong>Scinder un BL</strong>
<li>Cde frs : statut = "Envoyé par mail" et Date prise en charge= 0000-00-00</li>
<li>BL : n° facture client = 0, date livraison et date envoi = 0000-00-00 (supprimer ces contraintes par dans php7)</li>
<li>Fichier :  templates/sales_force/order_supplier/buyer.htm.php</li>
</ul>
<br>

<ul><strong>Erreur rapprochement bancaire :</strong> 
<li>Supprimer un rapprochement bnacaire : Comptabilite / Comptable / Banque / Rapprochement bancaire -> Recherche</li>
</ul>

<br><br>


<div class="headTitle">
 <p class="title">Blocage enregistrement factures clients et fournisseurs</p>
</div>
<br>



<strong>Conditions pour la facturation client</strong><br>
-> Commercial / Commandes clients / Rechercher une commande
Montant TTC (sans frais de port) > 0.00<br>
-> Comptabilité / Facturation clients / Rechercher une facture
<ul>
<li>Vérifier si la facture nexiste pas 
</ul>
-> Corrections / Bons de livraison / Entete BL : 
<ul>
<li>N° facture client = 0</li>
<li>Toutes les dates remplies</li>
<li>Date d`expédition inférieure à la date d`aujourdhui</li>
<li>Fournisseur identique pour le BL et la commande fournisseur</li>
</ul>
-> Corrections /Commandes fournisseurs / Entete cdes frs : 
<ul>
<li>Toutes les dates remplies</li>
<li>Date d`expédition inférieure à la date d`aujourdhui</li>
<li>Fournisseur identique pour le BL et la commande fournisseur</li>
<li>Le champ n° cde frs parent doit être vide</li>
</ul>
-> Administrateur :
<ul>
<li>Remplacer la valeur "1" par "0" dans le champ cash_payment de la table order</li>
</ul>


<br>

<strong>Conditions pour la facturation fournisseurs</strong><br>
-> Corrections / Commandes fournisseurs / Entete cdes frs :
<ul>
<li>Montant TTC > 0.00</li>
<li>Toutes les dates remplies</li>
<li>N° cde frs parent = 0</li>
</ul>
-> Comptabilité / Factures fournisseurs / Rechercher une facture :
<ul>
<li>Recherche sur n° cde frs si facture partielle existe (icone loupe) . Si oui, à utiliser.</li>
</ul>

<br><br>

<div class="headTitle">
 <p class="title">Erreurs montants factures/réglements - clients/fournisseurs/services</p>
</div>
<br>

<strong>Erreur montant facture client</strong><br>
Si paiement non enregistré :
<ul>
<li>Cliquer sur l`icone poubelle pour supprimer la facture : Comptable / Facturation clients / Recherche une facture</li>
<li>Enregistrer la facture : Comptable / Facturation clients / Enregistrer une facture
<li>Modifier les montants dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers </li>
</ul>
Si paiement enregistré:
<ul>
<li>Modifiers montants : Corrections / Clients / Factures clients</li>
<li>Modifier les montants dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers</li>
</ul>
<br>
<strong>Erreur montant règlement client</strong>
<ul>
<li>Modifier montant : Corrections / Clients / Regl. clients + Regl./factures clients</li>
<li>Si facture passée en statut payé, remettre le statut en "Invoiced" : Corrections / Clients / Factures clients</li>
<li>Modifier les montants dans les écritures : Comptabilité / Comptables / Interrogation des tiers</li>
</ul>
<br>
<strong>Erreur montant facture fournisseur</strong><br>
Si paiement non enregistré :
<ul>
<li>Cliquer sur licone poubelle pour supprimer la facture : Comptable / Facturation fournisseurs / Recherche une facture</li>
<li>Enregistrer la facture : Comptable / Facturation fournisseurs / Enregistrer une facture
<li>Modifier les montants dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers </li>
</ul>
Si paiement enregistré:
<ul>
<li>Modifiers montants : Corrections / Fournisseurs / Factures fournisseurs</li>
<li>Modifier les montants dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers</li>
</ul>
<br>
<strong>Erreur montant règlement fournisseur</strong><br>
<ul>
<li>Modifier montant : Corrections / Fournisseurs / Regl. frs + Regl./factures fournisseurs</li>
<li>Si facture passée en statut payé, remettre le statut en "StandBy" : Corrections / Fournisseurs / Factures fournisseurs</li>
<li>Modifier les montants dans les écritures : Comptabilité / Comptables / Interrogation des tiers</li>
</ul>
<br>
<strong>Erreur montant facture service</strong><br>
Si paiement non enregistré :
<ul>
<li>Cliquer sur l`icone poubelle pour supprimer la facture : Comptable / Facturation services / Recherche une facture</li>
<li>Enregistrer la facture : Comptable / Facturation services / Enregistrer une facture
<li>Modifier les montants dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers </li>
</ul>
Si paiement enregistré:
<ul>
<li>Modifiers montants : Corrections / Services / Factures services</li>
<li>Modifier les montants dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers</li>
</ul>
<br>
<strong>Erreur montant règlement service</strong><br>
<ul>
<li>Modifier montant : Corrections / Services / Regl. frs + Regl./factures services</li>
<li>Modifier les montants dans les écritures : Comptabilité / Comptables / Interrogation des tiers</li>
</ul>
<br><br>

<div class="headTitle">
 <p class="title">Erreurs logiciels</p>
</div>
<br>
<strong>Création règlement client en double</strong>
<ul>
<li>Rechercher n° règlement : Corrections / Clients / Regl. clients</li>
<li>Choisir un n° de règlement</li>
<li>Mettre le montant à 0 du réglement sélectionné : Corrections / Clients / Regl. clients</li>
<li>Mettre le montant à 0 du réglement sélectionné : Corrections / Clients / Regl./facture clients</li>
<li>Modifier les montants 0 du réglement sélectionné dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers </li>
</ul>
<br>
<strong>Création règlement fournisseur en double</strong>
<ul>
<li>Rechercher n° règlement : Corrections / fournisseurs / Regl. frs</li>
<li>Choisir un n° de règlement</li>
<li>Mettre le montant à 0 du réglement sélectionné : Corrections / Fournisseurs / Regl. frs</li>
<li>Mettre le montant à 0 du réglement sélectionné : Corrections / Fournisseurs / Regl./facture frs</li>
<li>Modifier les montants 0 du réglement sélectionné dans les écritures si existants : Comptabilité / Comptables / Interrogation des tiers </li>
</ul>

<br>
<strong>Ecritures banque en double</strong>
<ul>
<li>Mettre les valeurs à "0" pour les écritures en double : Paramétrage / Corrections / Ecritures banques </li>
</ul>

<br><br>

		<div class="clear"></div>
            	
           	</div>
   		</div>
        <div class="bottomRight"></div><div class="bottomLeft"></div>
    </div>
<?php

include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

?>