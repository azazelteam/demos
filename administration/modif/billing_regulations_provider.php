<?php
// Paramètres de base 
	$Str_TableName = "billing_regulations_provider";
	$pagetitle = "title_billing_regulations_provider";
	$Int_LimitP = 5; // Limite de produits a afficher sur la page, au moins 1
	$KeyName = 'idbilling_provider, idregulations_provider ';

// Field columns that appear after searching in ShowRecordsBy
	$ArrayFieldsColumns2Show[0]="idregulations_provider";
	$ArrayFieldsColumns2Show[1]="idprovider_invoice";
	$ArrayFieldsColumns2Show[2]="amount";
	

// Affichage de l'icone d'aide à côté du titre
	//$showHelp = true;
// Affichage lien "Recherche: Id acheteur ?"
	//$search = "search_id";


// show the page 
include "../../formbase/stdformbase.php";
?>
