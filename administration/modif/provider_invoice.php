<?php
// Paramètres de base 
	$Str_TableName = "provider_invoice";
	$pagetitle = "title_provider_invoice";
	$Int_LimitP = 5; // Limite de produits a afficher sur la page, au moins 1
	$KeyName = 'idprovider_invoice';

// Field columns that appear after searching in ShowRecordsBy
	$ArrayFieldsColumns2Show[0]="idprovider_invoice";
	$ArrayFieldsColumns2Show[1]="idprovider";
	$ArrayFieldsColumns2Show[2]="total_amount_ht";
	
// Affichage de l'icone d'aide à côté du titre
	//$showHelp = true;
// Affichage lien "Recherche: Id acheteur ?"
	//$search = "search_id";


// show the page 
include "../../formbase/stdformbase.php";
?>
