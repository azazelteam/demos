<?php
// Paramètres de base 
	$Str_TableName = "billing_regulations_buyer";
	$pagetitle = "title_billing_regulations_buyer";
	$Int_LimitP = 5; // Limite de produits a afficher sur la page, au moins 1
	$KeyName = 'idbilling_buyer, idregulations_buyer ';

// Field columns that appear after searching in ShowRecordsBy
	$ArrayFieldsColumns2Show[0]="idregulations_buyer";
	$ArrayFieldsColumns2Show[1]="idbilling_buyer";
	$ArrayFieldsColumns2Show[2]="amount";
	

// Affichage de l'icone d'aide à côté du titre
	//$showHelp = true;
// Affichage lien "Recherche: Id acheteur ?"
	//$search = "search_id";


// show the page 
include "../../formbase/stdformbase.php";
?>
