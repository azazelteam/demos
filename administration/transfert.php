<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Critères de sélection de requete dans la base pour les modèles
 */
 
include_once( "../config/init.php" );
include_once( dirname( __FILE__ ) . "/../catalog/drawlift.php" );
include_once( dirname( __FILE__ ) . "/../objects/XHTMLFactory.php" );

//---------------------------------------------------------------------------------

include( "$GLOBAL_START_PATH/templates/back_office/head.php" );

//---------------------------------------------------------------------------------


 
$modele=$_REQUEST[ "modele" ];
 $_SESSION[ "modele" ]= $modele;


$replace 			= isset( $_POST[ "replace" ] ) && !empty( $_POST[ "replace" ] ) ? intval( $_POST[ "replace" ] ) : "";
$valchoice 			= isset( $_POST[ "valchoice" ] ) && !empty( $_POST[ "valchoice" ] ) ? intval( $_POST[ "valchoice" ] ) : "";


if( isset( $_POST[ "SSESearch" ] ) || isset( $_POST[ "SSESearch_x" ] ) ){
	if($modele == 1)
	$SSEResult = CreateSSEBuyerResult();
	if($modele == 2)
	$SSEResult = CreateSSEIntituleResult();

	if($modele == 5)
	$SSEResult = CreateSSEUserResult();
	
}

	// On est en B2B
	include( "$GLOBAL_START_PATH/administration/templates/transfert.php" );
//}


//---------------------------------------------------------------------------------


include( "$GLOBAL_START_PATH/templates/back_office/foot.php" );

//---------------------------------------------------------------------------------

//--------------------------------modele 1 : Client--------------------------------

function getEstimates(){
	$rscounte = DBUtil::query( "SELECT * FROM `estimate` WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscounte;
}

function getCmdes(){
	$rscountc = DBUtil::query( "SELECT * FROM `order` WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscountc;
}

function getFacts(){
	$rscountf = DBUtil::query( "SELECT * FROM billing_buyer WHERE idbuyer = '".$_POST['valchoice']."' " );
	return $rscountf;
}

function getBls(){
	$rscountb = DBUtil::query( "SELECT * FROM bl_delivery WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscountb;
}

function getCredits(){
	$rscounta = DBUtil::query( "SELECT * FROM credits WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscounta;
}

function getRegs(){
	$rscountr = DBUtil::query( "SELECT * FROM regulations_buyer WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscountr;
}

function getDelivery(){
	$rscountd = DBUtil::query( "SELECT * FROM delivery WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscountd;
}

function getBilling(){
	$rscountbil = DBUtil::query( "SELECT * FROM billing_adress WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscountbil;
}

function getRepayment(){
	$rscountrep = DBUtil::query( "SELECT * FROM repayments WHERE idbuyer = '".$_POST['valchoice']."' " );
	return  $rscountrep;
}

function getAccount(){
	$rscountac = DBUtil::query( "SELECT * FROM account_plan WHERE idrelation = '".$_POST['valchoice']."' " );
	return  $rscountac;
}


function CreateSSEBuyerResult(){
	
	$rscounte = DBUtil::query( "SELECT * FROM `estimate` WHERE idbuyer = '".$_POST['valchoice']."' " );
	$rscountc = DBUtil::query( "SELECT * FROM `order` WHERE idbuyer = '".$_POST['valchoice']."' " );
	$rscountf = DBUtil::query( "SELECT * FROM billing_buyer WHERE idbuyer = '".$_POST['valchoice']."' " );
	$rscountb = DBUtil::query( "SELECT * FROM bl_delivery WHERE idbuyer = '".$_POST['valchoice']."' " );
	$rscounta = DBUtil::query( "SELECT * FROM credits WHERE idbuyer = '".$_POST['valchoice']."' " );
	$rscountr = DBUtil::query( "SELECT * FROM regulations_buyer WHERE idbuyer = '".$_POST['valchoice']."' " );
	$rscountd = DBUtil::query( "SELECT * FROM delivery WHERE idbuyer = '".$_POST['valchoice']."' " );	
	$rscountbil = DBUtil::query( "SELECT * FROM billing_adress WHERE idbuyer = '".$_POST['valchoice']."' " );	
	$rscountrep = DBUtil::query( "SELECT * FROM repayments WHERE idbuyer = '".$_POST['valchoice']."' " );	
	$rscountac = DBUtil::query( "SELECT * FROM account_plan WHERE idrelation = '".$_POST['valchoice']."' " );	

$counte = $rscounte->RecordCount();
$countc = $rscountc->RecordCount();
$countf = $rscountf->RecordCount() ;
$countb = $rscountb->RecordCount();
$counta = $rscounta->RecordCount();
$countr = $rscountr->RecordCount();
$countd = $rscountd->RecordCount();
$countbil = $rscountbil->RecordCount();
$countrep = $rscountrep->RecordCount();
$countac = $rscountac->RecordCount();

$tot = $counte + $countc + $countf + $countb + $counta + $countr + $countd + $countbil + $countrep + $countac;


	//print $count;exit;
	if($tot > 0)
	return true;
	else
	return false;
	//return $rscount->RecordCount();
	
}

//--------------------------------modele 2 : Intitulé --------------------------------

function getIntitule_link(){
	$rscountil = DBUtil::query( "SELECT * FROM `intitule_link` WHERE idintitule_title = '".$_POST['valchoice']."' " );
	return  $rscountil;
	
}


function CreateSSEIntituleResult(){
	$rscountil = DBUtil::query( "SELECT * FROM intitule_link  WHERE idintitule_title = '".$_POST['valchoice']."' " );
		
$countil = $rscountil->RecordCount();

$tot = $countil;

	//print $count;exit;
	if($tot > 0)
	return true;
	else
	return false;
	//return $rscount->RecordCount();
	
}

//--------------------------------modele 5 : Commercial--------------------------------


function getEstimates5(){
	$rscounte = DBUtil::query( "SELECT * FROM `estimate` WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscounte;
}

function getCmdes5(){
	$rscountc = DBUtil::query( "SELECT * FROM `order` WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountc;
}

function getFacts5(){
	$rscountf = DBUtil::query( "SELECT * FROM billing_buyer WHERE iduser = '".$_POST['valchoice']."' " );
	return $rscountf;
}

function getBls5(){
	$rscountb = DBUtil::query( "SELECT * FROM bl_delivery WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountb;
}

function getCredits5(){
	$rscounta = DBUtil::query( "SELECT * FROM credits WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscounta;
}

function getRegs5(){
	$rscountr = DBUtil::query( "SELECT * FROM regulations_buyer WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountr;
}

function getBilling5(){
	$rscountbil = DBUtil::query( "SELECT * FROM billing_buyer WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountbil;
}

function getBuyer5(){
	$rscountbu = DBUtil::query( "SELECT * FROM buyer WHERE iduser= '".$_POST['valchoice']."' " );
	return  $rscountbu;
}

function getContact_follow5(){
	$rscountco = DBUtil::query( "SELECT * FROM contact_follow WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountco;
}

function getOrder_supplier5(){
	$rscountos = DBUtil::query( "SELECT * FROM order_supplier WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountos;
}

function getBilling_supplier5(){
	$rscountbs = DBUtil::query( "SELECT * FROM billing_supplier WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountbs;
}

function getLitigations5(){
	$rscountlit = DBUtil::query( "SELECT * FROM litigations WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountlit;
}

function getLitigation_history5(){
	$rscountlith = DBUtil::query( "SELECT * FROM litigation_history WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountlith;
}

function getRegulations_provider5(){
	$rscountregp = DBUtil::query( "SELECT * FROM regulations_provider WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountregp;
}

function getRegulations_supplier5(){
	$rscountregs = DBUtil::query( "SELECT * FROM regulations_supplier WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountregs;
}

function getProvider_invoice5(){
	$rscountpi = DBUtil::query( "SELECT * FROM provider_invoice WHERE iduser = '".$_POST['valchoice']."' " );
	return  $rscountpi;
}


function CreateSSEUserResult(){
	
	$rscounte = DBUtil::query( "SELECT * FROM `estimate` WHERE iduser = '".$_POST['valchoice']."' " );
	$rscountc = DBUtil::query( "SELECT * FROM `order` WHERE iduser = '".$_POST['valchoice']."' " );
	$rscountf = DBUtil::query( "SELECT * FROM billing_buyer WHERE iduser = '".$_POST['valchoice']."' " );
	$rscountb = DBUtil::query( "SELECT * FROM bl_delivery WHERE iduser = '".$_POST['valchoice']."' " );
	$rscounta = DBUtil::query( "SELECT * FROM credits WHERE iduser = '".$_POST['valchoice']."' " );
	$rscountr = DBUtil::query( "SELECT * FROM regulations_buyer WHERE iduser = '".$_POST['valchoice']."' " );
	$rscountbil = DBUtil::query( "SELECT * FROM billing_buyer WHERE iduser = '".$_POST['valchoice']."' " );	
	$rscountbu = DBUtil::query( "SELECT * FROM buyer WHERE iduser = '".$_POST['valchoice']."' " );	
	$rscountco = DBUtil::query( "SELECT * FROM contact_follow WHERE iduser = '".$_POST['valchoice']."' " );		
	$rscountos = DBUtil::query( "SELECT * FROM order_supplier WHERE iduser = '".$_POST['valchoice']."' " );	
	$rscountbs = DBUtil::query( "SELECT * FROM billing_supplier WHERE iduser = '".$_POST['valchoice']."' " );		
	$rscountlit = DBUtil::query( "SELECT * FROM litigations WHERE iduser = '".$_POST['valchoice']."' " );		
	$rscountlith = DBUtil::query( "SELECT * FROM litigation_history WHERE iduser = '".$_POST['valchoice']."' " );		
	$rscountregp = DBUtil::query( "SELECT * FROM regulations_provider WHERE iduser = '".$_POST['valchoice']."' " );	
	$rscountregs = DBUtil::query( "SELECT * FROM regulations_supplier WHERE iduser = '".$_POST['valchoice']."' " );	
	$rscountpi = DBUtil::query( "SELECT * FROM provider_invoice WHERE iduser = '".$_POST['valchoice']."' " );	

$counte = $rscounte->RecordCount();
$countc = $rscountc->RecordCount();
$countf = $rscountf->RecordCount() ;
$countb = $rscountb->RecordCount();
$counta = $rscounta->RecordCount();
$countr = $rscountr->RecordCount();
$countbil = $rscountbil->RecordCount();
$countbu = $rscountbu->RecordCount();
$countco = $rscountco->RecordCount();
$countos = $rscountos->RecordCount();
$countbs = $rscountbs->RecordCount();
$countlit = $rscountlit->RecordCount();
$countlith = $rscountlith->RecordCount();
$countregp = $rscountregp->RecordCount();
$countregs = $rscountregs->RecordCount();
$countpi = $rscountpi->RecordCount();

$tot = $counte + $countc + $countf + $countb + $counta + $countbil+ $countr + $countbu + $countco + $countos + $countbs + $countlit + $countlith + $countregp + $countregs + $countpi;


	//print $tot;exit;
	if($tot > 0)
	return true;
	else
	return false;
	//return $rscount->RecordCount();
}	

//-----------------------------------------------------------------------------------------------------------------------------------------------------



//---------------------------------------------------------------------------------
?>