<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage d'une facture de commande dans une fenêtre PopUp
 */

include_once('../objects/classes.php');  
include_once('../objects/Order.php');
include_once('../fichepdf/fiches_pdf.php');

if( isset($_GET['idorder']) ) $idorder = $_GET['idorder'];
if ( empty($idorder) || ( Session::getInstance()->islogin() <= 0 && !User::getInstance()->getId() ) )
{
	//rien à faire
 	header("HTTP/1.0 204 No Response");
 	exit();
}

		$pdf_filename = "Facture_$idorder.pdf";
		if(!isset($_GET['dev'])){
		
			header( "Content-type: application/pdf" );
			header( "Content-disposition: inline; filename=\"$pdf_filename\"" );
			header( "Pragma: no-cache" );
            header( "Cache-Control: must-revalidate, post-check=0, pre-check=0, public" );
            header( "Expires: 0" );
            
		}
		makepdf($pdf_filename,null,null,$idorder);
?>