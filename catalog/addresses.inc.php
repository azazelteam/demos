<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Mise à jour des adresses de livraison/facturation
 */

 showDevInfo( "catalog/addresses.inc.php" );  


include_once( "$GLOBAL_START_PATH/objects/Util.php" );
include_once( dirname( __FILE__ ) . "/../lib/buyer.php" );

/* protection XSS */
$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

$iddelivery = isset( $_REQUEST["delivery_checkbox"] ) ? intval( $_POST["iddelivery"] ) : 0;
$idbilling = isset( $_REQUEST["billing_checkbox"] ) ? intval( $_POST["idbilling"] ) : 0;



//adresse de livraison

if( isset( $_POST["delivery_checkbox"] ) ){
	
	
	//mise à jour de l'adresse sélectionnée

	 if( !empty( $_POST["iddelivery"] ) || $_POST[ "iddelivery"] != "0"){
	 	
	 	if( checkPostData( "delivery" ) ){
		 		
		 	$iddelivery = intval( $_POST["iddelivery"] );
		 	buyer_UpdateDelivery( $iddelivery, $_POST[ "delivery" ] );
		 	
		 	/*echo "<p class=\"confirm\">MISE A JOUR DE L'ADRESSE DE LIVRAISON $iddelivery</p>";*/
		 	
	 	}
	 	/*else echo "<p class=\"alert\">TOUS LES CHAMPS OBLIGATOIRES DE L'ADRESSE DE LIVRAISON DOIVENT ETRE RENSEIGNES</p>";*/
	 	
	 }
	 
	//ajout d'une nouvelle adresse
	
	if( empty( $_POST[ "iddelivery" ] ) || $_POST[ "iddelivery"] == "0" ){
		
		if( checkPostData( "delivery" ) ){
			
		 	$iddelivery = buyer_AddDeliveryAddress( $_POST[ "delivery" ] );
		 	buyer_SetUseDeliveryAddress( $iddelivery );
		 	
		 	//echo "<p class=\"confirm\">AJOUT D'UNE NOUVELLE ADRESSE DE LIVRAISON $iddelivery</p>";
	 	
		}
		/*else echo "<p class=\"alert\">TOUS LES CHAMPS OBLIGATOIRES DE L'ADRESSE DE LIVRAISON DOIVENT ETRE RENSEIGNES</p>";*/
		
	}
	
	//sélection de l'adresse
	
	buyer_SetUseDeliveryAddress( $iddelivery );
	
	/*echo "<p class=\"confirm\">ADRESSE DE LIVRAISON SELECTIONNEE $iddelivery</p>";*/
	
}

//adresse de facturation

if( isset( $_POST["billing_checkbox"] ) ){
	
	//mise à jour de l'adresse sélectionnée

	 if( !empty( $_POST["idbilling"] ) || $_POST[ "idbilling" ] != '0'){
	 	
	 	if( checkPostData( "billing" ) ){

		 	$idbilling = intval( $_POST["idbilling"] );
		 	buyer_UpdateBilling( $idbilling, $_POST[ "billing" ] );
		 	
		 	/*echo "<p class=\"confirm\">MISE A JOUR DE L'ADRESSE DE FACTURATION $idbilling</p>";*/
	 		
	 	}
	 	//else echo "<p class=\"alert\">TOUS LES CHAMPS OBLIGATOIRES DE L'ADRESSE DE FACTURAION DOIVENT ETRE RENSEIGNES</p>";
	 	
	 }
	 
	 
	//ajout d'une nouvelle adresse
	
	if( empty( $_POST[ "idbilling" ] ) || $_POST[ "idbilling" ] == '0'){
		
		if( checkPostData( "billing" ) ){
			
			$idbilling = buyer_AddBillingAddress( $_POST[ "billing" ] );
			buyer_SetUseBillingAddress( $idbilling );
		 	
		 	/*echo "<p class=\"confirm\">AJOUT D'UNE NOUVELLE ADRESSE DE FACTURATION $idbilling</p>";*/
		
		}
		/*else echo "<p class=\"alert\">TOUS LES CHAMPS OBLIGATOIRES DE L'ADRESSE DE FACTURATION DOIVENT ETRE RENSEIGNES</p>";*/
		
	}
	
	//sélection de l'adresse
	
	buyer_SetUseBillingAddress( $idbilling );
	
	/*echo "<p class=\"confirm\">ADRESSE DE FACTURATION SELECTIONNEE $idbilling</p>";*/
	
}

//ne pas utiliser d'adresse de livraison différente

if( !isset( $_POST[ "delivery_checkbox" ] ) )
	$iddelivery = 0;
	
//ne pas utiliser d'adresse de facturation différente

if( !isset( $_POST[ "billing_checkbox" ] ) )
	$idbilling = 0;


buyer_SetUseBillingAddress( $idbilling );
buyer_SetUseDeliveryAddress( $iddelivery );

$deliveryCheckStatus = $iddelivery ? "checked=\"checked\"" : "";
$billingCheckStatus = $idbilling ? "checked=\"checked\"" : "";
	 
//--------------------------------------------------------------------------

function checkPostData( $addr_type ){

	$required = array( 
		"Lastname",
		"Adress",
		"Zipcode",
		"City",
		"Company",
		"Phonenumber"
	);
	
	foreach( $required as $fieldname ){
		
		$field = "$addr_type[$fieldname]";
		if( empty( $_POST[ $addr_type ][ $fieldname ] ) )
			return false;
			
	}
	
	return true;
		
}
//--------------------------------------------------------------------------
	
?>