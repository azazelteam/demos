<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de l'odf adresse de livraison
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFDelivery.php" );


$odf = new ODFDelivery(  $_REQUEST[ "iddelivery" ] );

$odf->exportODT( "Bon de livraison n° " .  $_REQUEST[ "iddelivery" ]  . ".odt" );

?>


