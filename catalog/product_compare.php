<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des produits dans le comparateur
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
header( "Content-Type: text/html; charset=utf-8" );
$lang = "_1";
class IndexController extends PageController{};
$compare_template = new IndexController( "product/compare.tpl" , "Comparatif de produits" );
if( !isset( $_SESSION[ "comproducts" ] ) || isset( $_GET[ "empty" ] ) )
	$_SESSION[ "comproducts" ] = array();

if( isset( $_GET[ "d" ] ) ){
	
	$key = array_search( $_GET[ "d" ], $_SESSION[ "comproducts" ] ) ;
	
	if( $key !== false )
		unset( $_SESSION[ "comproducts" ][ $key ] );
	
}

if( !empty( $_SESSION[ "comproducts" ] ) ) {
	
	$productsAvailable = 1;
	
	$Real_array = $_SESSION[ "comproducts" ];
	$SORT_field = isset( $_GET[ "SORT_field" ] ) ? urldecode( $_GET[ "SORT_field" ] ) : "id";
	$SORT_order = isset( $_GET[ "SORT_order" ] ) ? $_GET[ "SORT_order" ] : "ASC";
	
	$characLabels = array();
	
	//recherche les infos détaillés des produits
	foreach( $Real_array as $i => $idreal ) {
		
		$art = array();
		$Article = null;
		$Article = new DEPRECATEDARTICLE( $art, false );
	
		$Article->SetId( $idreal );
		$Article->Load();
		
		// Tableau associatif pour le tri
		$Articles[ "ARTICLE" ][ $i ]	= $Article;
		$Articles[ "id" ][ $i ]			= $Article->GetId();
		$Articles[ "reference" ][ $i ]	= $Article->get( "reference" );
		$Articles[ "delivdelay" ][ $i ]	= getDelay( $Article->get( "delivdelay" ) );
		$Articles[ "basicprice" ][ $i ]	= $Article->GetBasicPrice();
		$Articles[ "reducerate" ][ $i ]	= $Article->GetReduceRate();
		$Articles[ "wotprice" ][ $i ]	= html_entity_decode($Article->GetWoPrice());
		$Articles[ "weight" ][ $i ]		= $Article->get( "weight", "-" );
		$Articles[ "lot" ][ $i ]		= $Article->get( "lot", "-" );
		$Articles[ "parmFarm" ][ $i ]	= $Article->GetParmFamArr();
		
		$productsAvailable = count( $Real_array );
		
		for( $j=1 ; $j < 31 ; $j++ ){
			
			$characLabel = $Article->get( "parm_name_$j" );
			
			if( !empty( $characLabel ) ) {
				
				$Articles[ $characLabel ][ $i ] = $Article->get( "parm_value_$j" );
				
				if( array_search( $characLabel, $characLabels ) === false )
					$characLabels[] = $characLabel;
			}
			
		}
		
	}
	
	// Remplissage des cases !isset pour le tri
	foreach( $Real_array as $i => $idreal ) {
		
		foreach( $characLabels as $characLabel ) {
			
			if( !isset( $Articles[ $characLabel ][ $i ] ) )
				$Articles[ $characLabel ][ $i ] = "";
			
		}
		
	}
	
	foreach( $characLabels as $characLabel ) {
		
		ksort( $Articles[ $characLabel ] );
		
	}
	
	switch( $SORT_order ) {
	
	case "ASC":
		@array_multisort( $Articles[ $SORT_field ], SORT_ASC, $Articles[ "id" ], $Articles[ "parmFarm" ], $Articles[ "ARTICLE" ] );
		break;
		
	case "DESC":
		@array_multisort( $Articles[$SORT_field], SORT_DESC, $Articles[ "id" ], $Articles[ "parmFarm" ], $Articles[ "ARTICLE" ] );
		break;
		
	}
	
	$AllProductsInfoArray = array();
	
	// Tableau créé dans l'ordre du tri
	foreach( $Articles[ "ARTICLE" ] as $x => $Article ) {
		
		$AllProductsInfoArray[] = $Article;
		
	}

	$characs = array();

	// Construction de $characs dans l'ordre de parmFarm
	foreach( $Articles[ "parmFarm" ] as $i => $parmFarm ) {
		
		$familyLabels = !empty( $parmFarm[ "fam" ][ "label" ] ) ? array_count_values( $parmFarm[ "fam" ][ "label" ] ) : array();
		
		foreach( $familyLabels as $familyLabel => $familyColspan ) {
			
			// Caractéristiques techniques
			foreach( $parmFarm[ "int" ][ $familyLabel ][ "label" ] as $k => $characLabel ) {
				
				$Article = $Articles[ "ARTICLE" ][ $i ]; // ARTICLE Object
				$a = $Article->getArticle(); // Array
				
				//Ne pas utiliser les famille d'intitulés dans le comparatif => tous mettre dans Général :
				$characs[ "Général" ][ $characLabel ][ $i ] = $a[ $familyLabel ][ $characLabel ];
				
			}
			
		}
		
	}
	
	$CompTable = implode( ",", $Real_array );
	
	$hideFamilies = DBUtil::getParameter( "hide_families" );
	
	$products_array = array();
	
	$i = 0;
	foreach( $AllProductsInfoArray as $Article ) {
		
		$id = $Article->GetId();
		
		$wotprice = $Article->GetWoPrice();
		
		if( !$Article->get( "hidecost", 0 ) && $wotprice > 0 )
			$wotprice = Util::priceFormat( $wotprice );
		else
			$wotprice = "";
		
		$products_array[ $i ][ "href" ]			= URLFactory::getProductURL($Article->GetProductId());

		$products_array[ $i ][ "img" ]			= URLFactory::getReferenceImageURI( $Article->get( "idarticle" ), URLFactory::$IMAGE_ICON_SIZE );
		$products_array[ $i ][ "altIMG" ]		= "";
		$products_array[ $i ][ "name" ]			= Util::doNothing($Article->get( "name" , "-" ));
		$products_array[ $i ][ "deleteLink" ]	= "$ScriptName?d=$id";
		$products_array[ $i ][ "idBuyer" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$products_array[ $i ][ "titre" ]		= addslashes( $Article->get( "name" ) );
		$products_array[ $i ][ "productID" ]	= $Article->GetProductId();
		$products_array[ $i ][ "reference" ]	= $Article->GetRef();
		$products_array[ $i ][ "wotprice" ]		= html_entity_decode($wotprice);
		$products_array[ $i ][ "lot" ]			= $Article->get( "lot", "-" );
		$products_array[ $i ][ "unit" ]			= getUnit( $Article->get( "unit", "-" ) );
		$products_array[ $i ][ "delay" ]		= Util::doNothing($delivdelay = getDelay( $Article->get( "delivdelay", "-" ) ));
		$products_array[ $i ][ "basicPrice" ]	= ( Util::numberFormat( $Article->GetReduceRate() ) > 0 ) ? Util::priceFormat( $Article->GetBasicPrice() ) : "-";	
		$products_array[ $i ][ "reduceRate" ]	= ( Util::numberFormat( $Article->GetReduceRate() ) > 0 ) ? Util::rateFormat( $Article->GetReduceRate() ) : "-";
		$products_array[ $i ][ "weight" ]		= $Article->get( "weight", "-" );
		$products_array[ $i ][ "i" ]			= $i;
		$products_array[ $i ][ "idcategory" ]	= $Article->GetIdCategory();
		$products_array[ $i ][ "basic_price" ]	= $Article->GetBasicPrice();		
		$products_array[ $i ][ "min_cde" ]		= $Article->get("min_cde");
		$products_array[ $i ][ "idarticle" ]	= $Article->GetId();
		
		$i++;
		
	}
	
	$promo = 0;
	
	foreach( $AllProductsInfoArray as $Article )
		$promo += $Article->GetReduceRate();
	
	$compare_template->setData( "promo", $promo );
	
	$compare_template->setData( "productsArray", $products_array );
	
	$characId = 0;
	
	//Intitule
	$specifications = array();
	foreach( $characs as $familyLabel=>$characLabels ){
		
		foreach( $characLabels as $characLabel=>$characValues ) {
			
			$specifications[ $characId ][ "specifID" ] = $characId;
			$specifications[ $characId ][ "specifLabel" ] = $characLabel;
			$specifications[ $characId ][ "URLspecifLabel" ] = urlencode( $characLabel );
			
			$specifications[ $characId ][ "productSpecifValues" ] = array();
			
			foreach( $AllProductsInfoArray as $key=>$Article ) {
				
				$specifications[ $characId ][ "productSpecifValues" ][] = empty( $characValues[ $key ] ) ? "-" : $characValues[ $key ];
				
			}
			
			//On regarde si les productSpecifValues sont identiques
			if(count(array_unique($specifications[ $characId ][ "productSpecifValues" ])) > 1) {
				$specifications[ $characId ][ "isDifferentSpecif" ] = 1;
			} else {
				$specifications[ $characId ][ "isDifferentSpecif" ] = 0;	
			}
			
			$characId ++;
			
		}
		
	}
	
	
	$compare_template->setData( "specifications", $specifications );
	
	$sortDown	= "$GLOBAL_START_URL/catalog/product_compare.php?SORT_order=ASC&SORT_field=";
	$sortUp	= "$GLOBAL_START_URL/catalog/product_compare.php?SORT_order=DESC&SORT_field=";
	
	$compare_template->setData( "sortDown", $sortDown );
	$compare_template->setData( "sortUp", $sortUp );
	
	$compare_template->setData( "askUs", Dictionnary::translate( "ask_us" ) );
	
}else{
	
	$productsAvailable = "";
	
}

//----------------------------------------------------------------------------------------------

$compare_template->setData( "productsAvailable", $productsAvailable );
$compare_template->setData( "title", "Comparatif produits " );
$compare_template->setData( "meta_description", "Vous permet de comparer les différents produits proposés sur le site pour afin de mieux choisir celui qui vous convient." );

//----------------------------------------------------------------------------------------------


$compare_template->output();

//----------------------------------------------------------------------------------------------

function getDelay($iddelay){
	
	
	
	$db =& DBUtil::getConnection();
	$lang = "_1";
	
	$rs = $db->Execute( "SELECT delay$lang AS d FROM delay WHERE iddelay = '$iddelay'" );
	
	if( $rs === false )
		die( "Impossible de récupérér le délai de la référence" );
	
	if( $rs->RecordCount() > 0 )
		return $rs->fields( "d" );
	else
		return "";
	
}

//----------------------------------------------------------------------------------------------

function getUnit($idunit){
	
	
	
	$db =& DBUtil::getConnection();
	$lang = "_1";
	
	$rs = $db->Execute( "SELECT unit$lang AS u FROM unit WHERE idunit = '$idunit'" );
	
	if( $rs === false )
		die( "Impossible de récupérér le délai de la référence" );
	
	if( $rs->RecordCount() > 0 )
		return $rs->fields( "u" );
	else
		return "";
	
}

//----------------------------------------------------------------------------------------------

?>