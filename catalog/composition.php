<?php 
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */

include_once( "../objects/classes.php" );  
include_once( "$GLOBAL_START_PATH/objects/Voucher.php" );
include_once( "$GLOBAL_START_PATH/objects/GiftToken.php" );

include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );

class CompositionController extends WebPageController{};

$lang = Session::getInstance()->getlang();

/****************************************************************************************
 * 								CODE POUR LA COMPOSITION								*
 ***************************************************************************************/

// Pour commencer voir si une compo est demandée sinon
// Si une composition est demandée on l'affiche
// Si on a un thème demandé il faut afficher les compositions du theme
// Un peut compliqué mais rien de méchant

$idBuyer = $_SESSION['buyerinfo']['idbuyer'];
if(isset($_REQUEST["idBuyerSelect"]) && $_REQUEST["idBuyerSelect"]!=''){
	$idBuyerLinked = $_REQUEST["idBuyerSelect"];
}else{
	$idBuyerLinked = false;
}

if(isset($_REQUEST["composition"]) && $_REQUEST["composition"]!=""){
	$idcompo = $_REQUEST["composition"];
	
	//jusque la ok mais faut il encore vérifier si la compo existe desfois qu'un petit malin
	//tente un truc de cochon
	
	$verifcompo = DBUtil::query("SELECT idnomenclature FROM nomenclature WHERE idnomenclature=$idcompo");
	
	if($verifcompo->RecordCount() && $verifcompo->fields("idnomenclature")){
		//c'est bien on peut y aller maintenant
		//sortie des produits de la composition	
		
		$composition_page = new CompositionController( "composition.tpl" , "Compositions" );
		
		$composition = DBUtil::query("SELECT idproduct FROM nomenclature_product WHERE idnomenclature=$idcompo");
		
		//donc la on a bien les idproduct de la composition
		//mainenant on récup tout ce qu'il faut pour les masques produit detail_composition.tpl
		
		$i=0;
		$articlesJsDatas = "";
		
		$productsArray = array();
		
		while(!$composition->EOF()){	
			$idproduct = $composition->fields("idproduct");
			
			array_push($productsArray,$idproduct);
			
			
			$composition->MoveNext();
		}
		
		//Allez hop setData et doudidouda
		
		$composition_page->GetProductList("","",$productsArray,"products");
		
		$composition_page->setData('idComposition',$idcompo);
		
		$rscompote = DBUtil::query("SELECT idnomenclature, name, photo, anim FROM nomenclature WHERE idnomenclature=$idcompo");
		
		$compoName = $rscompote->fields("name");
		
		// photo de la composition
		if($rscompote->fields("photo")){
			$compoImageP = str_replace("../",$GLOBAL_START_PATH."/",$rscompote->fields("photo"));
		}else{
			$compoImageP = "default";
		}
		if(file_exists($compoImageP)){
			$compoImage = str_replace("../",$GLOBAL_START_URL."/",$rscompote->fields("photo"));
		}else{
			$compoImage = "";
		}
		
		//Animation de la composition
		if($rscompote->fields("anim")){
			$compoImageA = str_replace("../",$GLOBAL_START_PATH."/",$rscompote->fields("anim"));
		}else{
			$compoImageA = "default";
		}		
		if(file_exists($compoImageA)){
			$compoAnim = str_replace("../",$GLOBAL_START_URL."/",$rscompote->fields("anim"));	
		}else{
			$compoAnim = "";
		}

		$composition_page->setData('compositionName',$compoName);
		$composition_page->setData('compositionPhoto',$compoImage);
		$composition_page->setData('compositionAnimation',$compoAnim);
		
		$template = "composition.tpl";
					
	}else{
		//allez hop cochon retour a l'index
		header('Location: '.$GLOBAL_START_URL.'/index.php');
	}
}else{
		// sinon on affiche toutes les compos. Attention! ça va chier !!!!
		// Attention c'est con mais il faut refaire la même chose pour les partenaires
		// Au final on sort plusieurs tableaux, les compos du client, des utilisateurs et des partenaires
		// Jspr ke t'as pigé ! une fonction qui renvoie tout, c pa la mort!
		// GetCompositions(idbuyer,statut);
		// "idbuyer = 0","" 					= compositions client (MIM par exemple, compo sans idbuyer et sans statut)
		// "idbuyer > 0","catalog_right = 7"  	= compositions partenaires (l'idbuyer on s'en fou mais il faut le catalog_right 7)
		// "idbuyer > 0","catalog_right < 7"  	= compositions utilisateurs (idbuyer > 0)
		// "",""								= toutes les compositions
		// facile non ???

		$composition_page = new CompositionController( "composition_multi.tpl" , "Compositions" );
		
		if(isset($_REQUEST["type"]) && $_REQUEST["type"]=='client'){
			$compositionsClt = GetCompositions("idbuyer = 0", "");
			$composition_page->setData('Compositions',$compositionsClt);
		}else if(isset($_REQUEST["type"]) && $_REQUEST["type"]=='partenaires'){
			$compositionsPart = GetCompositions("idbuyer > 0", "catalog_right = 7");
			$composition_page->setData('Compositions',$compositionsPart);
		}else if(isset($_REQUEST["type"]) && $_REQUEST["type"]=='users'){
			$compositionsUsers = GetCompositions("idbuyer > 0", "catalog_right < 7");
			$composition_page->setData('Compositions',$compositionsUsers);
		}else if($idBuyerLinked){
			$compositionsUsers = GetCompositions("idbuyer = $idBuyerLinked","");
			$composition_page->setData('Compositions',$compositionsUsers);
		}else{
			$compositionsAll = GetCompositions();
			$composition_page->setData('Compositions',$compositionsAll);
		}

		$template = "composition_multi.tpl";
		$composition_page->setData('MultiCompositions',"ok");
}

$composition_page->setData('CurrentPageNumberC',"");
$composition_page->setData('CurrentPageNumberP',"");

//------------------------------initialisation du thème----------------------------------

	$idtopic = false;
	if(isset($_REQUEST["idtopic"])){
		$idtopic = $_REQUEST["idtopic"];
		
		//verif si thème existe si passé par url
		$rstopic = DBUtil::query("SELECT idtopic, name$lang as name, description$lang as description, image FROM topic WHERE idtopic = $idtopic");
		if($rstopic->RecordCount() > 0 && $rstopic->fields("idtopic")){
			$idtopic = $rstopic->fields("idtopic");
			$composition_page->setData('themeId',$rstopic->fields("idtopic"));
			$composition_page->setData('themeName',$rstopic->fields("name"));
			$composition_page->setData('themeDescription',$rstopic->fields("description"));
			$composition_page->setData('themeImage',$rstopic->fields("image"));
		}else{
			$idtopic =  false ;
			$composition_page->setData('themeId',"");
			$composition_page->setData('themeName',"");
			$composition_page->setData('themeDescription',"");
			$composition_page->setData('themeImage',"");
		}	
	}else if(isset($_REQUEST["composition"]) && $_REQUEST["composition"]!=""){
			$idcompo = $_REQUEST["composition"];
			$verifcompo = DBUtil::query("SELECT idnomenclature FROM nomenclature WHERE idnomenclature=$idcompo");
			
			if($verifcompo->RecordCount() && $verifcompo->fields("idnomenclature")){
				$rstopicVerif = DBUtil::query("SELECT idtopic FROM nomenclature WHERE idnomenclature=$idcompo");
				if($rstopicVerif->RecordCount() > 0 && $rstopicVerif->fields("idtopic")){
					$idtopic = $rstopicVerif->fields("idtopic");
					$rstopic = DBUtil::query("SELECT idtopic, name$lang as name, description$lang as description, image FROM topic WHERE idtopic = $idtopic");
					$composition_page->setData('themeId',$rstopic->fields("idtopic"));
					$composition_page->setData('themeName',$rstopic->fields("name"));
					$composition_page->setData('themeDescription',$rstopic->fields("description"));
					$composition_page->setData('themeImage',$rstopic->fields("image"));
				}else{
					$idtopic =  false ;
					$composition_page->setData('themeId',"");
					$composition_page->setData('themeName',"");
					$composition_page->setData('themeDescription',"");
					$composition_page->setData('themeImage',"");
				}	
			}
	}else{
		$composition_page->setData('themeId',"");
		$composition_page->setData('themeName',"");
		$composition_page->setData('themeDescription',"");
		$composition_page->setData('themeImage',"");
	}

//-----------------------------création du menu des thèmes------------------------------
	
	//pour les users on fait pas une liste désfois qu'il y en aie 100000 ça chierait completement
	
	$menuThemesClient = array();
	$menuThemesPartenaires = array();

	$menuThemesClient = GetMenuThemes("idbuyer = 0","");
	//$menuThemesPartenaires = GetMenuThemes("idbuyer > 0","catalog_right = 7");
	
	// il faut rajouter le nom des compos du theme défois que....
	// vision globale a 360° qui voit loin dans le futur parfait
	// dans	lequel les bases de données seraient une sorte de rongeur
	// qui mettent tout dans le même endroit du terrier, qui pueraient des
	// aisselles et de la gueule
	for($mouloud = 0; $mouloud < count($menuThemesClient) ; $mouloud++){
		$menuThemesClient[$mouloud]["compos"] = GetCompositions("idbuyer = 0", "" , $menuThemesClient[$mouloud]["idtheme"]);

	}		
	$composition_page->setData('menuSelfThemes',$menuThemesClient);
	// Fin du délire je vais me pendre maintenant
	
	
	$menuthemes = array();
	
	$requequette = "SELECT b.idbuyer, company FROM buyer b, nomenclature n
					WHERE n.idbuyer=b.idbuyer
					AND b.catalog_right=7
					AND b.idbuyer>0
					GROUP BY b.idbuyer";
	
	$rsthemes = DBUtil::query($requequette);
	$countthemes = 0;
	
	while(!$rsthemes->EOF()){
		$menuthemes[$countthemes]["idbuyer"] = $rsthemes->fields("idbuyer");
		$menuthemes[$countthemes]["company"] = $rsthemes->fields("company");
		
		if(($idBuyerLinked && $idBuyerLinked==$rsthemes->fields("idbuyer"))){
			$menuthemes[$countthemes]["selected"] = "ok";
		}else{
			$menuthemes[$countthemes]["selected"] = "";
		}
		
		$menuthemes[$countthemes]["href"] = $GLOBAL_START_URL."/catalog/composition.php?idBuyerSelect=".$rsthemes->fields("idbuyer");
		
		$rsthemes->MoveNext();
		$countthemes++;
	}
	
	$menuThemesPartenaires = $menuthemes;
	$composition_page->setData('menuParteraires',$menuThemesPartenaires);


//-----------------------------------------------------------------------------------


if(!$template){
	header('Location: '.$GLOBAL_START_URL.'/index.php');
}

$composition_page->GetCategoryLinks();
$composition_page->GetBrands();
$composition_page->GetTrades();

$composition_page->output();


//--------------------FONCTIONS DE LA MORT QUI TUENT----------------------------

//--------------------------------------------------------------------------------------------------------------------------------------
//fonction qui renvoie un menu pour les thèmes sous conditions
function GetMenuThemes($condBuyer = false, $condCatalogRight = false){
	global $idtopic, $GLOBAL_START_URL,$GLOBAL_START_PATH,$lang;
	
	$menuthemes = array();
	
	if($condBuyer && $condCatalogRight){
		$from=" FROM topic t, nomenclature n, buyer b ";
	}else{
		$from=" FROM topic t, nomenclature n ";
	}
	
	if($condBuyer && $condCatalogRight){
		$where=" WHERE b.idbuyer=n.idbuyer AND t.idtopic=n.idtopic AND n.$condBuyer ";
	}else{
		$where=" WHERE t.idtopic=n.idtopic AND n.idbuyer=0";
	}
	
	$requequette = "SELECT t.idtopic, t.name$lang as name 
					$from
					$where
					GROUP BY t.idtopic";
	
	if($condCatalogRight){
		$conditionCatalog = " AND b.$condCatalogRight ";
		$requequette = $requequette.$conditionCatalog;
	}
	
	$rsthemes = DBUtil::query($requequette);
	$countthemes = 0;
	
	while(!$rsthemes->EOF()){
		$menuthemes[$countthemes]["idtheme"] = $rsthemes->fields("idtopic");
		$menuthemes[$countthemes]["theme"] = $rsthemes->fields("name");
		
		if($idtopic && $idtopic==$rsthemes->fields("idtopic")){
			$menuthemes[$countthemes]["selected"] = "ok";
		}else{
			$menuthemes[$countthemes]["selected"] = "";
		}
		
		$menuthemes[$countthemes]["href"] = $GLOBAL_START_URL."/catalog/composition.php?idtopic=".$rsthemes->fields("idtopic");
		
		$rsthemes->MoveNext();
		$countthemes++;
	}
	
	return $menuthemes;
}	


//----------------------------------------------------------------------------------------------------------------------------------------
// fonction qui renvoie les compositions de qui tu veux de quel theme tu veux
// EXEMPLES :
// "idbuyer = 0","" 					= compositions client (MIM par exemple, compo sans idbuyer et sans statut)
// "idbuyer > 0","catalog_right = 7"  	= compositions partenaires (l'idbuyer on s'en fou mais il faut le catalog_right 7)
// "idbuyer > 0","catalog_right < 7"  	= compositions utilisateurs (idbuyer > 0)
// "",""								= toutes les compositions
// pour un theme donné il faut juste rajouter un "idtopic"
// on sfait une touffe ?
function GetCompositions($condBuyer = false, $condCatalogRight = false , $theme = false){
	global $GLOBAL_START_PATH,$GLOBAL_START_URL,$idtopic;
	
	$andy = "";
	if($theme){
		$andy=" AND idtopic=$andy ";
	}
	
	if($condBuyer && $condCatalogRight){
		$from=" FROM nomenclature , buyer ";
		$where=" WHERE buyer.idbuyer = nomenclature.idbuyer AND nomenclature.$condBuyer ";
	}else{
		$from=" FROM nomenclature ";
		if($condBuyer)
			$where=" WHERE nomenclature.$condBuyer ";
		else
			$where ="";
	}
		
	$couscous = "SELECT idnomenclature
				$from
				$where
				$andy
				";
	
	if($condCatalogRight){
		$conditionCatalog = " AND buyer.$condCatalogRight ";
		$couscous = $couscous.$conditionCatalog;
	}
	
	if($idtopic && $condBuyer){
		$couscous = $couscous." AND idtopic=$idtopic";
	}else{
		if($idtopic)
			$couscous = $couscous." WHERE idtopic=$idtopic";			
	}
	
	$rscomposition=DBUtil::query($couscous);
	
	$Compositions=array();
	$compo=0;
	$productsArray=array();
	while(!$rscomposition->EOF()){
		$idcompo = $rscomposition->fields("idnomenclature");
		
		//jusque la ok mais faut il encore vérifier si la compo existe desfois qu'un petit malin
		//tente un truc de cochon
		
		$verifcompo = DBUtil::query("SELECT idnomenclature FROM nomenclature WHERE idnomenclature=$idcompo");
		
		if($verifcompo->RecordCount() && $verifcompo->fields("idnomenclature")){
			//c'est bien on peut y aller maintenant
			//sortie des produits de la composition	
			
			$composition = DBUtil::query("SELECT idproduct FROM nomenclature_product WHERE idnomenclature=$idcompo");
			
			//donc la on a bien les idproduct de la composition
			//mainenant on récup tout ce qu'il faut pour les masques produit detail_composition.tpl
			
			$i=0;
			while(!$composition->EOF()){
				$idproduct = $composition->fields("idproduct");
				$productsArray[$i]['idproduct']=$idproduct;
					
				$Product = new Product($idproduct);
	
				$productName = $Product->GetName();
				$productsArray[$i]['productName']=$productName;
						
				$productIcon = $Product->GetIcon();
				$productsArray[$i]['productIcon']=$productIcon;
				
				$productURL = URLFactory::getProductURL( $idproduct, $productName );
				$productsArray[$i]['productURL']=$productURL;
				
				$hasPromo = $Product->hasPromo();
				if( $hasPromo ){
					$productsArray[$i]['hasPromo']=$hasPromo;
					
					$debutDate = $Product->GetBeginPromoDate();
					$finDate = $Product->GetEndPromoDate();
					
					$jourDebut = substr($debutDate, 8, 2);
					$moisDebut = substr($debutDate, 5, 2);
					$anneeDebut = substr($debutDate, 0, 4);
					
					$jourFin = substr($finDate, 8, 2);
					$moisFin = substr($finDate, 5, 2);
					$anneeFin = substr($finDate, 0, 4);
					
					$productsArray[$i]['jourDebut']=$jourDebut;
					$productsArray[$i]['moisDebut']=$moisDebut;
					$productsArray[$i]['anneeDebut']=$anneeDebut;
					
					$productsArray[$i]['jourFin']=$jourFin;
					$productsArray[$i]['moisFin']=$moisFin;
					$productsArray[$i]['anneeFin']=$anneeFin;
					
					
				}else{
					$productsArray[$i]['hasPromo']="";
				}
	
				/****************************************************
				 *	    				 PRIX						*
				 ****************************************************/	
			
				$pxmin = Product::getLowerPrice( $idproduct );
				$idartmin = Product::getLowerPriceIdArticle( $idproduct );
			
				if( $idartmin && $pxmin !== false ){
					$productsArray[$i]['hasprice']='ok';
					$productsArray[$i]['pxmin']=Util::priceFormat( $pxmin );
					
					include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
					
					$article = new CArticle( $idartmin );
					$productsArray[$i]['pxminTTC']=Util::priceFormat( $article->getPriceET( Session::getInstance()->getIdBuyer() ) );
					
					if($hasPromo){
						$promoRate=Util::numberFormat($article->GetPromoRate(), 0);
						if($promoRate!=0){
							$productsArray[$i]['promoRate'] = $promoRate;
						}else{
							$productsArray[$i]['promoRate']="";
						}	
					}else{
						$productsArray[$i]['promoRate']="";
					}
				}else{
					$productsArray[$i]['promoRate']="";
					$productsArray[$i]['pxminTTC']="";
					$productsArray[$i]['hasprice']="";
				}
				
				//attention que pour MIM -> suppliercost est en fait son px TTC d'origine, nimporte quoi je sais
				if( $idartmin && $pxmin !== false ){
					$realprice =  $article->getvalue("sellingcos");
					$productsArray[$i]['oldTTC']=Util::priceFormat($realprice);
				}else{
					$productsArray[$i]['oldTTC']="";
				}
							
				$i++;
				$composition->MoveNext();
			}
			
			//Allez hop tableau et doudidouda
			$Compositions[$compo]['products']=$productsArray;
			$Compositions[$compo]['idComposition']=$idcompo;
			
			$rscompote = DBUtil::query("SELECT idnomenclature, name, photo, anim FROM nomenclature WHERE idnomenclature=$idcompo");
			
			$compoName = $rscompote->fields("name");
			// photo de la composition
			if($rscompote->fields("photo")){
				$compoImageP = str_replace("../",$GLOBAL_START_PATH."/",$rscompote->fields("photo"));
			}else{
				$compoImageP = "default";
			}
			if(file_exists($compoImageP)){
				$compoImage = str_replace("../",$GLOBAL_START_URL."/",$rscompote->fields("photo"));
			}else{
				$compoImage = "";
			}
			
			//Animation de la composition
			if($rscompote->fields("anim")){
				$compoImageA = str_replace("../",$GLOBAL_START_PATH."/",$rscompote->fields("anim"));
			}else{
				$compoImageA = "default";
			}		
			if(file_exists($compoImageA)){
				$compoAnim = str_replace("../",$GLOBAL_START_URL."/",$rscompote->fields("anim"));	
			}else{
				$compoAnim = "";
			}
			
			$Compositions[$compo]['compositionName']=$compoName;
			$Compositions[$compo]['compositionPhoto']=$compoImage;
			$Compositions[$compo]['compositionAnimation']=$compoAnim;
			$Compositions[$compo]['href']=$GLOBAL_START_URL."/catalog/composition.php?composition=".$rscompote->fields("idnomenclature");
		
			$compo++;
		}
		$rscomposition->MoveNext();
	}
	return $Compositions;
}

function affVisionImg( $visionImages ){

	if ( count( $visionImages ) < 2 ) 
		return;
	
	?>
	<script type="text/javascript" language="javascript" src="/js/visioimg.js"></script>
	<script type="text/javascript" language="javascript">
	<!--
		var bauto = 0;
		var dossier = '/images/products/';
		var numero = 1;
		var nom = new objet ( '<?php echo implode( "', '", $visionImages ) ?>' );
		
	// -->
	</script>
	<table id="visioImg" cellspacing="4">
		<tr>
			<td><img class="link" src="/www/icones/previous.png" title="Précédente" width="16" height="16" onclick="precedente(); return false;" style="cursor:pointer;" /></td>
			<td><img class="link" src="/www/icones/next.png" title="Suivante" width="16" height="16" onclick="suivante(); return false;" style="cursor:pointer;" /></td>
			<td><img class="link" src="/www/icones/player_fwd.png" title="Auto" width="16" height="16" name="automat" onclick="auto('/www/icones/'); return false;" style="cursor:pointer;" /></td>
			<td>Voir autres photos</td>
		</tr>
	</table>
	<?php
	
		
}

//----------------------------------------------------------------------------
function getDelay($iddelay){
	
	
	
	$db =& DBUtil::getConnection();
	$lang = Session::getInstance()->getLang();
	
	$rs = $db->Execute("SELECT delay$lang as d FROM delay WHERE iddelay='".$iddelay."'");
	
	if($rs === false ){
		die ("Impossible de récupérér le délai de la référence");
	}
	
	if($rs->RecordCount()>0){
		return $rs->fields("d");
	}else{
		return "";
	}
}	

//-----------------------------------------------------------------------------
//Fonction pour récupérer les produits similaires

function getProductSimilars( $idproduct ){

	global  $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	$db =& DBUtil::getConnection();
	$lang = Session::getInstance()->getLang();
	
	$query = "
	SELECT idproduct_similar_1, idproduct_similar_2, idproduct_similar_3
	FROM product
	WHERE idproduct = '$idproduct'
	LIMIT 1";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer les produits similaires" );
		
	$similars = array();
	$idproduct_similars = array();
		
	for( $i = 1; $i < 4 ; $i++ ){
	
		$idproduct_similar = $rs->fields( "idproduct_similar_$i" );
		
		if( $idproduct_similar && !in_array( $idproduct_similar, $idproduct_similars ) )
			$idproduct_similars[] = $idproduct_similar;

	}
		
	if( count( $idproduct_similars ) ){
	
		$comma_separated = implode( "','", $idproduct_similars );

		if( isset( $_SESSION[ "buyerinfo" ] ) && isset( $_SESSION[ "buyerinfo" ][ "catalog_right" ] ) )
			$catalog_right = $_SESSION[ "buyerinfo" ][ "catalog_right" ];
		else $catalog_right = 0;

		$gestStock=DBUtil::getParameter("use_stock_levels");
		if($gestStock==0){
			$ANDLINES.=" AND d.stock_level<>0 ";
		}
		$query = "
		SELECT DISTINCT( p.idproduct ), name_1 AS name
		FROM product p, detail d 
		WHERE p.idproduct != '$idproduct'
		AND p.idproduct = d.idproduct 
		$ANDLINES
		AND p.available = 1 
		AND p.catalog_right <= '$catalog_right' 
		AND ( p.idproduct_similar_1 IN ( '$comma_separated' )
			OR p.idproduct_similar_2 IN ( '$comma_separated' )
			OR p.idproduct_similar_3 IN ( '$comma_separated' )
		)";
		

		$rs2 = $db->Execute( $query );
		while( !$rs2->EOF() ){

			$iconURL = URLFactory::getProductImageURI( $rs2->fields( "idproduct" ), URLFactory::$IMAGE_ICON_SIZE );
			$productURL=URLFactory::getProductURL( $rs2->fields( "idproduct" ), $rs2->fields( "name" ) );
			
			
			//et hop on emballe le tout et on a un joli tableau
			$similars[ $rs2->fields( "idproduct" ) ]["name"] = $rs2->fields( "name" );
			$similars[ $rs2->fields( "idproduct" ) ]["icone"]= $iconURL;
			$similars[ $rs2->fields( "idproduct" ) ]["productURL"]= $productURL;
			$rs2->MoveNext();
			
		}
			
	}
	
	return $similars;
	
}


//---------------------------------------------------------------------------------------------

function setReferencesJsData(& $details) {

	global $IdProduct, $IdCategory, $ScriptName;

	$ReferencesJsData="<script type=\"text/javascript\" language=\"javascript\">";

	foreach ($details["id"] as $row => $idarticle) {

		$ReferencesJsData.="articles[ '$idarticle' ] = new Array();";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'reference' ] = '".jsFormat( $details[ "reference" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'weight' ] = '".jsFormat( $details[ "weight" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'unit' ] = '".jsFormat( $details[ "unit" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'lot' ] = '". jsFormat( $details[ "lot" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'delay' ] = '".jsFormat( $details[ "delay" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'promo' ] = '".jsFormat( $details[ "promo" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'hidecost' ] = '". jsFormat( $details[ "hidecost" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'hasPromo' ] = '". jsFormat( $details[ "hasPromo" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'promo_rate' ] = '". jsFormat( $details[ "promo_rate" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'promo_price' ] = '". jsFormat( $details[ "promo_price" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'isDestocking' ] = '". jsFormat( $details[ "isDestocking" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'destocking_price' ] = '". jsFormat( $details[ "destocking_price" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'destocking_rate' ] = '". jsFormat( $details[ "destocking_rate" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'stock_level' ] = '". jsFormat( $details[ "stock_level" ][ $row ] )."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'wtprice' ] = '". str_replace(" &curren;","",jsFormat($details[ "wtprice" ][ $row ] ))."';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'IdProduct' ] = '$IdProduct ';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'IdCategory' ] = ' $IdCategory ';";
		$ReferencesJsData.="articles[ '$idarticle' ][ 'ScriptName' ] = '". jsFormat( $ScriptName ) ."';";
	}

	foreach ($details["hiddenFields"] as $row => $hiddenFields) {

		$idarticle = $details["id"][$row];

		foreach ($hiddenFields as $fieldname => $value) {
			if(htmlentities($fieldname)!='delay'){
				$ReferencesJsData.="articles[ '$idarticle' ][ '". htmlentities($fieldname) ."' ] = '". html_entity_decode(jsFormat( $value) ) ."';";
			}
		}

	}

	$ReferencesJsData.="</script>";

	return $ReferencesJsData;
}

//---------------------------------------------------------------------------------------------------
//Fabrication du tableau des couleurs en JS
function setColorsJsData() {
	global $Product;

	$colors = $Product->GetColors();
	if (!count($colors))
		return;
	
	$ColorsJsData="<script type=\"text/javascript\" language=\"javascript\">";

	$ColorsJsData.="var productColors = new Array();";

	foreach ($colors as $idcolor => $color) {
		$ColorsJsData.="productColors[ '$idcolor' ] = new Array();";

		foreach ($color as $key => $value) {
				$ColorsJsData.="productColors[ ' $idcolor ' ][ '". jsFormat( $key ) ."' ] = ' ".jsFormat( $value )." ';";
		}
	}
	$ColorsJsData.="</script>";
	
	return $ColorsJsData;
}

function jsFormat($string) {

	return str_replace("'", "\\'", htmlentities($string));

}

//---------------------------------------------------------------------------------------------------
function setHeadingValues(& $headings, & $details, $tableId) {
	global $charte_product;
	
	if (!count($headings) || !count($details["reference"]))
		return;
	
	$headingsArray=array();
	
	$i=0;
	$headingcount=0;
	foreach ($headings as $heading) {
			
		$idarticles = $details["id"];

		$valueCount = 0;
		$firstPassage = true;
		$headingValues=array();
		$j=0;
		$continue="nok";
		foreach ($idarticles as $row => $idarticle) {
			$tableamoi=$details['hiddenFields'];
			$ref="";
			//die(print_r($details['hiddenFields']));
			
			foreach($details['hiddenFields'] as $hiddenfield){
				if($hiddenfield['IdRealProduct']==$idarticle){
					if(!empty($hiddenfield['prixtotalremise'])){
						$continue="ok";
					}
				}
			}
			if( $continue=="ok" ) {
			
				$headingValue = getArticleHeadingValue($idarticle, $heading, $ref);
				if ($headingValue !== false) {
	
					$headingValues[$j]["idarticle"]=$idarticle;			
					$headingValues[$j]["headingValue"]=htmlentities( $headingValue );
					
					if($details['stock_level'][$row]==-1){
						$headingValues[$j]["stock_level_0"]="";
						$headingValues[$j]["stock_level_M1"]="OK";
						$headingValues[$j]["stock_level_1"]="";
					}else if($details['stock_level'][$row]==0){
						$headingValues[$j]["stock_level_0"]="OK";
						$headingValues[$j]["stock_level_M1"]="";
						$headingValues[$j]["stock_level_1"]="";
					}else if($details['stock_level'][$row]>0){
						$headingValues[$j]["stock_level_0"]="";
						$headingValues[$j]["stock_level_M1"]="";
						$headingValues[$j]["stock_level_1"]="OK";
					}
					$valueCount++;
	
				}
				$continue="nok";
				$j++;
			}
		}
		if($valueCount>0){
			if($ref=="ok"){
				$heading="Référence";
			}
			$headingsArray[$i]["heading"]=htmlentities( $heading );
			$headingsArray[$i]["headingValues"]=$headingValues;		
			//die(print_r($headingValues));
		}
		$i++;
	}
	 
	return $headingsArray;
}

//---------------------------------------------------------------------------------------------

function getArticleHeadingValue($idarticle, $heading) {

	

	$db = & DBUtil::getConnection();
	$lang = Session::getInstance()->getLang();

	if($heading!="Référence"){
		$query = "
		SELECT iv.intitule_value$lang AS value, il.reference AS ref
		FROM intitule_link il, intitule_value iv, intitule_title it
		WHERE il.idarticle = '$idarticle'
		AND il.idintitule_title = it.idintitule_title
		AND it.intitule_title$lang LIKE '$heading'
		AND il.idintitule_value = iv.idintitule_value
		LIMIT 1";
	
		$rs = $db->Execute($query);
	
		if($rs === false)
			return false;
	
		if (!$rs->RecordCount()){
			$query = "SELECT reference AS value FROM detail
					  WHERE idarticle = '$idarticle'
					  LIMIT 1 ;";
			
			$rs = $db->Execute($query);
	
			if (!$rs->RecordCount()){
				return false;}
		}
	}else{
		$query = "SELECT idarticle, reference AS value FROM intitule_link
				WHERE idarticle = '$idarticle'
				LIMIT 1 ;";
	
		$rs = $db->Execute($query);
	
		if ($rs === false)
			die("Impossible de récupérer la valeur de l'intitulé '$heading' pour l'article '$idarticle'");
	
		if (!$rs->RecordCount())
			return false;
	}

	return $rs->fields("value");

}

//---------------------------------------------------------------------------------------------
function listProductColors($tableId) {

	global $Product, $GLOBAL_START_URL, $ParameterArray, $ScriptName, $Arr_Couleurs;

	$colors = $Product->GetColors();

	if (!count($colors))
		return;
		
	$colorsTable=array();
	$i=0;
	foreach ($colors as $idcolor => $color) {
		$colorsTable[$i]["idcolor"]=$color["idcolor"];
		$colorsTable[$i]["code_color"]=$color["code_color"];
		$colorsTable[$i]["image"]=$color["image"];
		$popupStr = addslashes($color["name"]);

		if ($color["price"] > 0){
			$popupStr .= "<br />Supplément : " . Util::priceFormat($color["price"]);
		}else{
			if ($color["percent"] > 0){
				$popupStr .= "<br />Supplément : " . Util::rateFormat($color["percent"]);
			}elseif ($color["percent"] < 0) {
				$popupStr .= "<br />Réduction : " . Util::rateFormat($color["percent"]);
			}
		}
		$popupStr .= "<br />D&eacute;lai : " . $color["delay"];
		
		$colorsTable[$i]["textColor"]=$popupStr;
		
		
		if (in_array($idcolor, $Arr_Couleurs)) {
			$checked = "'checked'";
		} else {
			$checked = '';
		}
		$colorsTable[$i]["check"]=$checked;
		$colorsTable[$i]["hiddenFields"]="";
		foreach ($ParameterArray as $ParmName => $ParmValue) {
			$colorsTable[$i]["hiddenFields"].="<input type=\"hidden\" name=\"$ParmName\" value=\"$ParmValue\" />";
		}
		
		$i++;
	}


	return $colorsTable;
}

?>