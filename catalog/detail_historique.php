<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Détail historique clients des achats pour un groupement
 */


include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class GroupController extends PageController {}
$controller = new GroupController( "/account/detail_historique.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
	
} else {
	$admin_order->setData('isLogin', "");
}

$detail ="";

$statStartDate = $_SESSION["start"];
$statEndDate = $_SESSION["end"];
$idbuyer = $_GET["buyer"];
$grouping = $_SESSION["grouping"];


$q2 = " SELECT  
				gs.idorder,
					gs.conf_order_date,
					gs.idcategory,
					gs.quantity,
					c.name_1,
					gs.reference,
					d.summary_1,
					gs.discount_price
			FROM 	grouping_story gs,
					category c,
					detail d
			WHERE	gs.idbuyer = '$idbuyer'
			AND		gs.idcategory = c.idcategory
			AND		gs.reference = d.reference
			AND 	(gs.conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' )
			AND 	gs.idgrouping = $grouping
			ORDER  BY gs.conf_order_date ASC
		";
			//print $q2;						 
			$rs2 = & DBUtil::query( $q2 );
	
		$q1 = "SELECT company
				FROM buyer
				WHERE idbuyer = '$idbuyer'";
									 
			$rs1 = & DBUtil::query( $q1 );
			if ($rs1->fields("company") !="")
			$company = $rs1->fields("company");
			else
			$company = "-";
	
		$detail.= "<h2>Historique du client: ". $idbuyer ." / ". $company . "</h2>";
		$detail.= "<br>";
		$detail .= "<div style='font-size: 12px;'>";
		$detail.= "<table class='resultTable' border='1' style='width: 100%; border-collapse: collapse; border-color: #BBBBBB;' cellspacing='1'>
				 <tr>
				 <th>
				 N. cde
				 </th>
				 <th>
				 Date cde
				 </th>
				 <th>
				 Nom categorie
				 </th>
				 <th>
				 Reference
				 </th>
				 <th>
				 Nom
				 </th>
				 <th>Qte</th>
				 <th>
				 Prix HT
				 </th>
				 </tr>";
				 
				 while( !$rs2->EOF ){


				 
				 $detail .="
				 <tr>
				  <td><span style='font-size:10px;'>
				 ".$rs2->fields("idorder")."
				 </span></td>
				 <td><span style='font-size:10px;'>
				 ".$rs2->fields("conf_order_date")."
				 </span></td>
				 <td><span style='font-size:10px;'>
				  ".$rs2->fields("name_1")."
				 </span></td><span style='font-size:10px;'>
				 <td align=left><span style='font-size:10px;'>
				 ".$rs2->fields("reference")."
				 </span></td>
				 <td><span style='font-size:10px;'>
				 ".$rs2->fields("summary_1")."
				 </span></td>
				 <td><span style='font-size:10px;'>
				 ".$rs2->fields("quantity")."
				 </span></td>
				 <td><span style='font-size:10px;'>
				 ".$rs2->fields("discount_price")*$rs2->fields("quantity")."
				 </span></td>
				 </tr>";
				 
				 $rs2->MoveNext();
	}
				 
				 
	$detail.="			 </table><br><br>
				
				";
		
		
		
		$detail .= "<br>";
		
	
	$detail .= "
</div>

</span>";





$exportCSV = array();


$arraytitle = array("N. client","company","N. de commande","Date confir. cde","N. category","Nom category","Reference","Designation","Qte","Prix HT");

$exportCSV[] = $arraytitle;

$rs2 = & DBUtil::query( $q2 );


				 while( !$rs2->EOF ){

				$q1 = "SELECT company
				FROM buyer
				WHERE idbuyer = '$idbuyer'";
									 
			$rs1 = & DBUtil::query( $q1 );
			if ($rs1->fields("company") !="")
			$company = $rs1->fields("company");
			else
			$company = "-";
				 
				
				$arraydata = array($idbuyer,
									$company,
									$rs2->fields("idorder"),
									$rs2->fields("conf_order_date"),
									$rs2->fields("idcategory"),
				 					$rs2->fields("name_1"),
									$rs2->fields("reference"),
									$rs2->fields("summary_1"),
									$rs2->fields("quantity"),
									$rs2->fields("discount_price")*$rs2->fields("quantity"));
				 $exportCSV[] = $arraydata;
				 
				 $rs2->MoveNext();
	}







include_once('../statistics/objects/tableStats.php');


$detail .=LinkExportHistofront($exportCSV,'',0,Date("YmdHms")."-HistogrammeCAL3",'turnover');









$controller->setData( "detail", Util::doNothing($detail) ); //Affichage



/* On ferme l'envoi de données */
$controller->output();
?>
