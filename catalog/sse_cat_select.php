<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Arborescence catégorie pour le moteur de recherche
 */
 
include_once('../objects/classes.php');

/**
 * Affichage arbo
 */
function ArboCatSel($IdCategory=0,$Langue,$First=0,$Level,$PIdCategory,$Allparents=FALSE)
{

global $langue, $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	$Base = &DBUtil::getConnection();
	//Racine=Categorie 0 !
	$i=0;
	$openlevels = explode(",",$Level);
	if($First == 0)
	{
	if($PIdCategory > 0) 
		{
		if( !in_array($PIdCategory,$openlevels, FALSE) ) { $openlevels[] = $PIdCategory; $Level=implode(",",$openlevels); }
		}
	else if($PIdCategory < 0)
		{
		$closelevels=array(abs($PIdCategory)) ; $openlevels= array_diff($openlevels,$closelevels); 
		$Level=implode(",",$openlevels);
		}
	}
	
	if($Allparents === FALSE)
	{
		$Allparents = array();
		//if( isset($_SESSION['buyerinfo']['catalog_right']) )
		//	$SQL_select = "SELECT DISTINCT `cl`.`idcategoryparent` FROM `category_link` `cl`, `category` `c`" .
		//			" WHERE `cl`.`idcategoryparent` = `c`.`idcategory`" .
		//			" AND `c`.`catalog_right` <= '{$_SESSION['buyerinfo']['catalog_right']}'";
		//	else 
			$SQL_select = "SELECT DISTINCT `idcategoryparent` FROM `category_link`";
		
		$rs =& DBUtil::query($SQL_select);
		//trigger_error(showDebug($SQL_select,'ArboCatSel() => $SQL_select','sql'),E_USER_NOTICE);
		if( empty($rs) ) trigger_error(showDebug($SQL_select,'ArboCatSel() => $SQL_select','log'),E_USER_ERROR);
		$nn= $rs->RecordCount();
		for($ii=0;$ii<$nn;$ii++)
		{
		$Allparents[$ii] = 0 + $rs->Fields('idcategoryparent');
		$rs->MoveNext();
		}
	}
	
	if( isset($_SESSION['buyerinfo']['catalog_right']) ) 
	$query = "select `idcategorychild` from `category_link`,`category` where `idcategoryparent`='$IdCategory' AND `category_link`.`idcategorychild` = `category`.`idcategory` AND `category`.`catalog_right` <= '{$_SESSION['buyerinfo']['catalog_right']}'";
	else
	$query = "select `idcategorychild` from `category_link` where `idcategoryparent`='$IdCategory'";
	//echo " $query ";
	$rs= DBUtil::getConnection()->Execute($query);
	$n=$rs->RecordCount();
	if($n)
	{
		while($i < $n)
		{
			$IdCategoryChild=$rs->Fields("idcategorychild");
			
			if($IdCategory!=$IdCategoryChild)	
			{	
			//if($Level) echo "<div style=\"visibility: hidden;\">\n";
			//echo "<img src=../img/expand_category.jpg border=0  alt=\"Ouvrir cette catégorie\">\n";
				$rs1 = DBUtil::getConnection()->Execute("select name$Langue from category where idcategory='$IdCategoryChild'");
				$mt = $First * 15;
				if($First > 0 ) $cstyle = "opened_level"; else $cstyle = "active_level";
				if( !in_array($IdCategoryChild,$openlevels, FALSE) ) 
				{	
					echo "<table style=\"margin-left:".$mt."px;\" width=\"250\" border=\"0\"><tr height=\"20\">";
					if( in_array(0+$IdCategoryChild,$Allparents, FALSE) )
					{
					echo "<td width=\"40\" align=\"left\"><a href=\"$GLOBAL_START_URL/catalog/sse_cat_select.php?Opens=$Level&PIdCategory=$IdCategoryChild\" class=\"opened_level\"><img src=../www/icones/cat_close.jpg border=0></a></td>";
					echo '<td width="210" valign="top"><a href="javascript:setsel(', $IdCategoryChild, ",'", $rs1->Fields("name$Langue") , "');\" class=opened_level >";					
					echo $rs1->Fields("name$Langue");
					echo "</a></td>";
					}
					else 
					{
					echo '<td width="40" align="left"><a href="javascript:setsel(', $IdCategoryChild, ",'", $rs1->Fields("name$Langue") , "');\" class=\"opened_level\" ><img src=../www/icones/cat_open.jpg border=0></a></td>";
					echo '<td width="210" valign="top"><a href="javascript:setsel(', $IdCategoryChild, ",'", $rs1->Fields("name$Langue") , "');\" class=\"opened_level\" >";
					echo $rs1->Fields("name$Langue");
					echo "</a></td>";
					}
					//echo "</td>";	
				}
				else
				{
					echo "<table style=\"margin-left:".$mt."px;\" width=\"250\" border=\"0\"><tr height=\"20\">";
					echo "<td width=\"40\" align=\"left\"><a href=\"$GLOBAL_START_URL/catalog/sse_cat_select.php?Opens=$Level&PIdCategory=-$IdCategoryChild\" class=\"active_level\"><img src=../www/icones/cat_open.jpg border=0></a>\n</td>";
					echo '<td width="210" valign="top"><a href="javascript:setsel(', $IdCategoryChild, ",'", $rs1->Fields("name$Langue") , "');\" class=\"active_level\" >";
					echo $rs1->Fields("name$Langue");
					echo "</a></td>";
					//echo "</b>";
					//echo "</td>";
				}

				echo "</tr></table>\n";

				if( in_array($IdCategoryChild,$openlevels, FALSE) )
					ArboCatSel($IdCategoryChild,$Langue,$First+1,$Level,$PIdCategory,$Allparents);
			}
			$rs->MoveNext();
			$i++;
		}
		
	}

}

include($GLOBAL_START_PATH.'/catalog/include/sse_cat_select.htm.php');
?>