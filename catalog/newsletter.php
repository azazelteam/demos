<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Formulaire inscription à la newsletter
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );

class NewsController extends PageController{};
$newsletter = new NewsController( "newsletter.tpl" , "Newsletter" );

$ScriptName = "newsletter.php";

$newsletter->setData( "ScriptName", basename( $_SERVER[ "PHP_SELF" ] ) );
$noNewsletter = "";

$newsletter->setData( "noNewsletter", $noNewsletter );


//----------------------------------------------------------------------------------------------
/* souscription */

$newsletter->setData( "errorString", 	"" );
$newsletter->setData( "subscription", 	!isset( $_REQUEST[ "unsubscribe" ] ) );
$newsletter->setData( "unsubscription", isset( $_REQUEST[ "unsubscribe" ] ) );
$newsletter->setData( "subscribed", 	false );
$newsletter->setData( "unsubscribed", 	false );
$newsletter->setData( "action", 		$GLOBAL_START_URL . $_SERVER[ "REQUEST_URI" ] );

/* inscription ou désinscription */

$ret = false;
if( isset( $_POST[ "email" ] ) ){
	
	if( isset( $_REQUEST[ "unsubscribe" ] ) )
		$newsletter->setData( "unsubscribed", 	$ret = unsubscribe( $newsletter ) );
	else $newsletter->setData( "subscribed", 	$ret = subscribe( $newsletter ) );
	
}

$newsletter->setData( "display_form", !$ret );

//----------------------------------------------------------------------------------------------

/* préparation du formulaire de souscription */

include_once( "$GLOBAL_START_PATH/objects/Util.php" );

$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

/* éléments flexy */

$newsletter->createElement( "email", 		isset( $_REQUEST[ "email" ] ) ? $_REQUEST[ "email" ] : "" );
$newsletter->createElement( "gender", 		isset( $_POST[ "gender" ] ) ? intval( $_POST[ "gender" ] ) : 0 )->setOptions( getGenders() );
$newsletter->createElement( "lastname", 	isset( $_POST[ "lastname" ] ) ? $_POST[ "lastname" ] : "" );
$newsletter->createElement( "firstname", 	isset( $_POST[ "firstname" ] ) ? $_POST[ "firstname" ] : "" );
$newsletter->createElement( "company", 		isset( $_POST[ "company" ] ) ? $_POST[ "company" ] : "" );
$newsletter->createElement( "address", 		isset( $_POST[ "address" ] ) ? $_POST[ "address" ] : "" );
$newsletter->createElement( "address2", 	isset( $_POST[ "address2" ] ) ? $_POST[ "address2" ] : "" );
$newsletter->createElement( "zipcode", 		isset( $_POST[ "zipcode" ] ) ? $_POST[ "zipcode" ] : "" );
$newsletter->createElement( "city", 		isset( $_POST[ "city" ] ) ? preg_replace( "[^0-9]*", "", $_POST[ "city" ] ) : "" );
$newsletter->createElement( "phonenumber", 	isset( $_POST[ "phonenumber" ] ) ? $_POST[ "phonenumber" ] : "" );

//----------------------------------------------------------------------------------------------
	
$newsletter->output();

//----------------------------------------------------------------------------------------------
/* souscription */

function subscribe( &$controller ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

	/* PEAR packages */
	
	//include_once( "Validate.php" );
	//include_once( "Validate/FR.php" );
	
	/*if( !Validate::email( $_POST[ "email" ] ) ){ //email invalide
	
		$controller->setData( "errorString", "Le format de l'adresse email est incorrect" );
		return false;
		
	}*/
	
	/* utilisateur déjà inscrit ? */
	$db_context = DBUtil::getConnection()->connectionId;
	$query = "
	SELECT COUNT(*) AS alreadyRegistered 
	FROM newsletter_subscriptions 
	WHERE email LIKE '" . Util::html_escape($db_context, $_POST[ "email" ] ) . "'";
	
	if( DBUtil::query( $query )->fields( "alreadyRegistered" ) > 0 ){
		
		$controller->setData( "errorString", "Vous êtes déjà abonné à la newsletter" );
		return false;
		
	}
	
	$gender 		= isset( $_POST[ "gender" ] ) ? intval( $_POST[ "gender" ] ) : 0;
	$lastname 		= isset( $_POST[ "lastname" ] ) ? $_POST[ "lastname" ] : "";
	$firstname 		= isset( $_POST[ "firstname" ] ) ? $_POST[ "firstname" ] : "";
	$company 		= isset( $_POST[ "company" ] ) ? $_POST[ "company" ] : "";
	$address 		= isset( $_POST[ "address" ] ) ? $_POST[ "address" ] : "";
	$address2 		= isset( $_POST[ "address2" ] ) ? $_POST[ "address2" ] : "";
	$zipcode 		= isset( $_POST[ "zipcode" ] ) ? preg_replace( "[^0-9]*", "", $_POST[ "zipcode" ] ) : 0;
	$city 			= isset( $_POST[ "city" ] ) ? $_POST[ "city" ] : "";
	$phonenumber 	= isset( $_POST[ "phonenumber" ] ) ? $_POST[ "phonenumber" ] : "";
	
	$query = "
	INSERT INTO newsletter_subscriptions(
		email,
		gender,
		lastname,
		firstname,
		company,
		address,
		address2,
		zipcode,
		city,
		phonenumber
	) VALUES (
		'" . Util::html_escape($db_context, $_REQUEST[ "email" ] ) . "',
		'" . intval( $gender ) . "',
		'" . Util::html_escape($db_context, $lastname ) . "',
		'" . Util::html_escape($db_context, $firstname ) . "',
		'" . Util::html_escape($db_context, $company ) . "',
		'" . Util::html_escape($db_context, $address ) . "',
		'" . Util::html_escape($db_context, $address2 ) . "',
		'" . preg_replace( "[^0-9]*", "", $zipcode ) . "',
		'" . Util::html_escape($db_context, $city ) . "',
		'" . Util::html_escape($db_context, $phonenumber ) . "'
	)";
	
	DBUtil::query( $query );
	
	return true;

}

//----------------------------------------------------------------------------------------------
/* désinscription */

function unsubscribe( &$controller ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	$_POST = Util::arrayMapRecursive( "stripslashes", $_POST );
	$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

	/* PEAR packages */
	
	include_once( "Validate.php" );
	include_once( "Validate/FR.php" );
	
	if( !Validate::email( $_POST[ "email" ] ) ){ //email invalide
	
		$controller->setData( "errorString", "Le format de l'adresse email est incorrect" );
		return false;
		
	}
	
	/* utilisateur déjà inscrit ? */
	$db_context = DBUtil::getConnection()->connectionId;
	$query = "
	SELECT COUNT(*) AS alreadyRegistered 
	FROM newsletter_subscriptions 
	WHERE email LIKE '" . Util::html_escape($db_context, $_POST[ "email" ] ) . "'";
	
	$query2 = "
	SELECT COUNT(*) AS alreadyRegistered 
	FROM buyer b, contact c
	WHERE c.mail LIKE '" . Util::html_escape($db_context, $_POST[ "email" ] ) . "'
	AND c.idbuyer = b.idbuyer
	AND b.newsletter = 1";
	
	if( DBUtil::query( $query )->fields( "alreadyRegistered" ) == 0 && DBUtil::query( $query2 )->fields( "alreadyRegistered" ) == 0 ){
		
		$controller->setData( "errorString", "Cette adresse email ne fait pas partie des abonnés à la newsletter" );
		return false;
		
	}
	
	$email = preg_replace( "[%#]+", "", $_REQUEST[ "email" ] );
	$query = "DELETE FROM newsletter_subscriptions WHERE email = '" . Util::html_escape($db_context, $email ) . "' LIMIT 1";

	DBUtil::query( $query );
	
	$query = "
	UPDATE buyer b, contact c
	SET b.newsletter = 0
	WHERE c.mail LIKE '" . Util::html_escape($db_context, $_POST[ "email" ] ) . "'
	AND c.idbuyer = b.idbuyer
	AND b.newsletter = 1";
	
	DBUtil::query( $query );
	
	return true;

}

//----------------------------------------------------------------------------------------------

function getGenders(){

	$lang 		= "_1";
	$genders 	= array();
	
	$rs =& DBUtil::query( "SELECT idtitle, title$lang FROM title ORDER BY idtitle ASC" );
	while( !$rs->EOF() ){
		
		$genders[ $rs->fields( "idtitle" ) ] = $rs->fields( "title$lang" );
		
		$rs->MoveNext();
			
	}
	
	return $genders;
	
}

//----------------------------------------------------------------------------------------------

?>