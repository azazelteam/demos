<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi mail au client lors de sa création
 */

include_once ("../config/init.php");
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );
require_once ("HTML/Template/Flexy.php");
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/FlexyController.php" );

$lang = "_1";

$DEBUG = "none";

class ContactController extends PageController{};
$contact_template = new ContactController( "contact.tpl" , "Contactez-nous" );
$contact_template->setData( "title", Dictionnary::translate("contact_title"));
$contact_template->setData( "meta_description",  Dictionnary::translate("contact_description") );

$ScriptName = "contact.php";

$mandatoryError = "";
$sendError = "";
$displayForm = "";
$successfullMail = "";
$recipient = "";
$recipientName = "";
$id_cat="";
$name_cat =  "";
if(isset ($_GET["id_cat"]))
$id_cat=$_GET["id_cat"];
else if(isset ($_POST["idcat"]))
$id_cat=$_POST["idcat"];

if(isset ($_REQUEST["name_cat"]))
$name_cat = $_REQUEST["name_cat"];

$options["templateDir"][] = "$GLOBAL_START_PATH/templates/catalog/";
//$options[ "compileDir" ] 		= "$GLOBAL_START_PATH/catalog/templates/";
$options[ "compileDir" ] 		= "$GLOBAL_START_PATH/templates/catalog/";
//if (isset ($_GET["id_cat"])) echo $_GET["id_cat"]; 
if (isset ($_GET["email"])) {
	$_POST = Util::arrayMapRecursive('Util::doNothing', $_POST);
	if (empty ($_POST["lastname"]) || empty ($_POST["email"]) || empty ($_POST["message"])) {

		$mandatoryError = 1;
		$displayForm = 1;

	} else {
		include_once ("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
		if (isset ($_POST['dest'])) { 
			$recipient = $_POST['dest'];
			$recipientName = $_POST['recipient'];
		}else{
			header( "Location: contact.php" );
		}
		
		$mail_first = new HTML_Template_Flexy( $options );
		$mail_first->setData("lastname",$_POST["lastname"]);
		$mail_first->setData("firstname",$_POST["firstname"]);
		$mail_first->setData("company",$_POST["company"]);
		$mail_first->setData("start_url",$GLOBAL_START_URL);
		$mail_first->setData("phonenumber",hrPhoneNumber($_POST["phonenumber"]));
		$mail_first->setData("faxnumber",hrPhoneNumber($_POST["faxnumber"]));
		$mail_first->setData("ourLogo",DBUtil::getParameterAdmin("ad_logo"));
		$mail_first->setData("subject",$_POST["subject"]);
		$mail_first->setData("message",$_POST["message"]);
		$mail_first->setData("email",$_POST["email"]);
		$mail_first->setData("id_cat",$id_cat);
		$mail_first->setData("name_cat",$name_cat);
		
		$mail_first->compile("contact_mail_seller.tpl");
		$mailContent = $mail_first->toString();
		
		//envoi du mail		
		$email1 = new htmlMimeMail();
		
		$fullname = $_POST["firstname"]." ".$_POST["lastname"];
		$mailReceiver = $_POST['email'];
		$email1->setFrom("\"$fullname\" <$mailReceiver>");
		$email1->setSubject("Contact client");
		$email1->setHtml(stripslashes($mailContent));

		$recipients = array (); //tableau qui contient les déstinataires du message
		array_push($recipients, $recipient); //on ajoute le destinataire à la liste		

		$res1 = $email1->send($recipients);

		if (!$res1) {

			$sendError = 1;
			$displayForm = 1;

		} else {

			$successfullMail = 1;

		}

		//*********************envoi d'une copie ********************************
		$mail_copy = new HTML_Template_Flexy($options);

		$days = array (
			"Dimanche",
			"Lundi",
			"Mardi",
			"Mercredi",
			"Jeudi",
			"Vendredi",
			"Samedi"
		);
		$strday = $days[date("w")];
		$curday = date("d");
		$months = array (
			"janvier",
			"février",
			"mars",
			"avril",
			"mai",
			"juin",
			"juillet",
			"août",
			"septembre",
			"octobre",
			"novembre",
			"décembre"
		);
		$curmonth = $months[date("m") - 1];
		$strdate = "$strday $curday $curmonth " . date("Y") . "&nbsp;&agrave; " . date("H:m");
		$AdminLogo = DBUtil::getParameterAdmin("ad_logo");

		$BuyerTitle = $_POST['titre'];
		$BuyerLastname = $_POST['lastname'];
		$BuyerFirstname = $_POST['firstname'];
		$BuyerEmail = $_POST["email"];

		$query = "SELECT `phonenumber`, `faxnumber` FROM `user` WHERE `email` LIKE '$recipient'";
		$rs = DBUtil::query($query);

		if (isset ($rs->fields["phonenumber"]))
			$phonenumber = $rs->fields["phonenumber"];
		else
			$phonenumber = "";

		if (isset ($rs->fields["faxnumber"]))
			$faxnumber = $rs->fields["faxnumber"];
		else
			$faxnumber = "";

		$mail_copy->setData("logo", "http://" . $_SERVER["HTTP_HOST"] . $AdminLogo);
		$mail_copy->setData("recipientName", $recipientName);
		$mail_copy->setData("recipient", $recipient);
		$mail_copy->setData("strdate", $strdate);
		$mail_copy->setData("phonenumber", hrPhoneNumber($phonenumber));
		$mail_copy->setData("faxnumber", hrPhoneNumber($faxnumber));
		$mail_copy->setData("title",$_POST['titre']);
		$mail_copy->setData("lastname",$_POST['lastname']);
		$mail_copy->setData("firstname",$_POST['firstname']);
		$mail_copy->setData("buyer",$_POST["titre"] . ". " .$_POST["firstname"] . " " .$_POST["lastname"]);
		$mail_copy->setData("id_cat",$id_cat);
		$mail_copy->setData("name_cat",$name_cat);
		$mail_copy->compile("contact_mail.tpl");
		$bccData = $mail_copy->toString();

		$email2 = new htmlMimeMail();
		
		$email2->setSubject("Confirmation de votre message sur " . DBUtil::getParameterAdmin("ad_name"));
		$email2->setFrom("\"$recipientName\" <$recipient>");
		$email2->setHtml(stripslashes($bccData));

		$mailReceiver = array ();
		array_push($mailReceiver, $_POST['email']);

		$res2 = $email2->send($mailReceiver);

		if (!$res2) {

			$sendError = 1;
			$displayForm = 1;
			$successfullMail = 0;

		} else {

			$successfullMail = 1;
		}

	}

}

$contact_template->setData("successfullMail", $successfullMail);
$contact_template->setData("recipient", $recipient);
$contact_template->setData("id_cat",$id_cat);
$contact_template->setData("name_cat",$name_cat);
$buyerTitle		= Session::getInstance()->getCustomer() ? DBUtil::getDBValue( "title_1", "title", "idtitle", Session::getInstance()->getCustomer()->getAddress()->getGender() ) : "";
$buyerPhone		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getPhonenumber() : "";
$buyerFax		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getFaxnumber() : "";
$buyerCompany	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getCompany() : "";

$contact_template->setData("buyerTitle", $buyerTitle);
$contact_template->setData("buyerPhonenumber", $buyerPhone);
$contact_template->setData("buyerFaxnumber", $buyerFax);
$contact_template->setData("buyerCompany", $buyerCompany);

$contact_template->setData("recipientName", $recipientName);
$contact_template->setData("sendError", $sendError);
$contact_template->setData("mandatoryError", $mandatoryError);
$contact_template->setData("displayForm", $displayForm);

$contact_template->output();


function hrPhoneNumber($phonenumber) {

	if (strlen($phonenumber) != 10)
		return $phonenumber;

	$hrphonenumber = "";
	$i = 0;

	while ($i < strlen($phonenumber) / 2) {

		$hrphonenumber .= substr($phonenumber, $i * 2, 2) . "&nbsp;";

		$i++;

	}

	return $hrphonenumber;

}
?>