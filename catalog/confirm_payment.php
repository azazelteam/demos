<?php 

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Confirmation paiement
 */
 
include_once('../objects/classes.php');  

// Feuille de styles spécifique à la page

$HTML_link['stylesheet'][2] = $GLOBAL_START_URL.'/css/catalog/droite.css';

$HTML_title = 'Confirmation de commande';
$HTML_description = 'Site de vente de matériel de manutention';

// Liste des <meta ...> à afficher si définis
$HTML_meta = array(
		'description'	=> $HTML_description,
		'keywords'		=> 'manutention, matériel',
		'author'		=> 'AKILAE',
		'DC.Title'		=> $HTML_title,
		'DC.Type'		=> 'texte',
		'DC.Format'		=> 'text/html',
		'DC.Language'	=> 'fr',
		'DC.Description'=> $HTML_description
		);

//header de la page
include("$GLOBAL_START_PATH/www/head.htm.php");

//template du centre de la page
$maintemplate="$GLOBAL_START_PATH/catalog/include/confirm_payment.htm.php";

//barre de gauche à utiliser
$leftcolumn="$GLOBAL_START_PATH/www/barre_g.php";

//barre de droite à utiliser
//$rightcolumn="$GLOBAL_START_PATH/www/barre_d3.php";

//utlilisation du module de rcherche
$searchmodule=true;

//page
include("$GLOBAL_START_PATH/www/main.htm.php");

$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_estimate_conversion_id = DBUtil::getParameterAdmin( "google_estimate_conversion_id" );
$ScriptNem="finish.php";
include_once("$GLOBAL_START_PATH/catalog/google_pageview_conversion.php");

//fermeture de la page
include_once("$GLOBAL_START_PATH/www/footer.php");

?>