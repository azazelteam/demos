<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage de la 3ème étape de la commande
 * Récapitulatif avant validation finale
 */

include_once('../objects/classes.php');  
include_once('drawlift.php');
include_once( "$GLOBAL_START_PATH/objects/GiftToken.php" );
include_once( "$GLOBAL_START_PATH/objects/Voucher.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
//die;
//----------------------------------------------------------------------------
Basket::getInstance()->removeArticle0Quantity();

	$it = Basket::getInstance()->getItems()->iterator();
	$index = 0;

	while ($it->hasNext()) {
		$item = $it->next();
		if($item->getQuantity() == 0){
			Basket::getInstance()->removeItemAt($index);
			
		}
		$index++;
	}
$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

//var_dump($_REQUEST);die;
//----------------------------------------------------------------------------

class OrderController extends PageController{};
$order_tpl = new OrderController( "order.tpl" , 'Confirmation et paiement' );
// Si l'utilisateur n'est pas identifié
if( Session::getInstance()->getCustomer() === NULL ) 
{
	// Retour à la première étape (panier)
	header('Location: register.php');
}
//Si panier vide, retour au panier
if (!Basket::getInstance()->getItemCount())
	header('Location: basket.php');
// Livraison et facturation
if ((isset($_GET['step']) && $_GET['step'] == 'delivery') || (!isset($_POST['step'])) || $_POST['step'] != '1')
{
	$order_tpl->setData('delivery', "1");
	$order_tpl->setData('title', 'Livraison et facturation');
}
else
	$order_tpl->setData('title', 'Confirmation');

$order_tpl->setData('hideBottomBar', 1 );//bottom bar
$lang = "_1";
$isTemplate = true;
//----------------------------------------------------------------------------
//-------------------account-------------------//

$order_tpl->setData('isLogin', "ok");

//----------------------------------------------------------------------------

//tracker
$effiliation = DBUtil::getParameter('use_effiliation_tag');

if($effiliation){
	
	$articlesET = 0.0;
	$it = Basket::getInstance()->getItems()->iterator();
	while( $it->hasNext() )
		$articlesET += $it->next()->getTotalET();

	$order_tpl->setData('tracker','ok');
	$order_tpl->setData('tracker_buyer',Session::getInstance()->getCustomer()->getContact()->getEmail());
	$order_tpl->setData('tracker_ht',round($articlesET - Basket::getInstance()->getDiscountAmount(),2));
}

if(isset($_POST["idpayment_pre"])){
	$idpayment=$_POST["idpayment_pre"];
}else{
	$idpayment=1;
}

if(isset($_POST["n_order"])){
	$n_order=$_POST["n_order"];
}else{
	$n_order='';
}

if(isset($_POST["Comment_pre"])){
	$comment=$_POST["Comment_pre"];
}else{
	$comment='';
}
/**
 * Enregistrement des adresses de livraison et de facturation
 * Mise à jour du basket
 */
include_once('addresses.inc.php');
include_once( dirname( __FILE__ ) . "/../lib/buyer.php" );

	
//order.tpl

$order_tpl->setData('comment',$comment);
if( isset( $_POST["delivery_checkbox"] ) )
	$order_tpl->setData('iddelivery', $iddelivery);
else
	$order_tpl->setData('iddelivery', buyer_GetDeliveryAddress());
if( isset( $_POST["billing_checkbox"] ) )
	$order_tpl->setData('idbilling', $idbilling);
else
	$order_tpl->setData('idbilling',buyer_GetBillingAddress());
$order_tpl->setData('SPPrice',Basket::getInstance()->getTotalATI() );

$order_tpl->setData('is_estimate',0);

//basket_amounts

$order_tpl->setData('estimate',false);

$articlesET = 0.0;
$it = Basket::getInstance()->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();

$articlesATI = 0.0;
$it = Basket::getInstance()->getItems()->iterator();
while( $it->hasNext() )
	$articlesATI += $it->next()->getTotalATI();
	
//----------------------------------------------------------------------------
/*
 * Réduction automatique à la première commande
 * Traité comme une remise commerciale ( Basket::fields[ "total_discount" ] et Basket::fields[ "total_discount_amount" ] )
 */

$idbuyer 					= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
$firstOrder 				= $idbuyer && DBUtil::query( "SELECT COUNT(*) AS `count` FROM `order` WHERE idbuyer = '$idbuyer' AND `status` NOT LIKE 'Cancelled'" )->fields( "count" ) == 0;

$order_tpl->setData( "first_order", $firstOrder );

//----------------------------------------------------------------------------
/*
 * Chèque-cadeau
 * Considéré comme un mode de règlement donc l'utilisation du chèque-cadeau ne modifie pas les montants du panier
 */

$gift_token_amount 		= 0.0;
$gift_token_serial 		= isset( $_REQUEST[ "gift_token_serial" ] ) ?stripslashes( $_REQUEST[ "gift_token_serial" ] ) : "";
$isAvailableGiftToken 	= GiftToken::isAvailableGiftToken( $gift_token_serial );

$order_tpl->setData( "gift_token_serial", 			$gift_token_serial );
$order_tpl->setData( "gift_token_error", 			strlen( $gift_token_serial ) && !$isAvailableGiftToken ? Dictionnary::translate( "gift_token_unavailable" ) : ""  );
$order_tpl->setData( "display_gift_token_serial", 	isset( $_REQUEST[ "gift_token_serial" ] ) );

$order_tpl->setData( "gift_token_amount", 			0.0 );

//utilisation du chèque-cadeau

if( $gift_token_serial && $isAvailableGiftToken ){
		
	$query = "
	SELECT gtm.value 
	FROM gift_token_model gtm, gift_token gt 
	WHERE gt.serial = '" . Util::html_escape( $gift_token_serial ) . "'
	AND gtm.type LIKE '" . GiftToken::$TYPE_AMOUNT . "' 
	AND gt.idtoken_model = gtm.idmodel
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );

	$gift_token_amount = $rs->RecordCount() ? round( $rs->fields( "value" ), 2 ) : 0.0;
	
	$order_tpl->setData( "gift_token_amount", Util::numberFormat( $gift_token_amount ) );
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/*
 * Bon de réduction
 */

//include_once( dirname( __FILE__ ) . "/../objects/Voucher.php" );

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

 * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction * 
 
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */
include_once( dirname( __FILE__ ) . "/../objects/Voucher.php" );
if( isset( $_REQUEST[ "voucher_code" ] ) ){ //bon de réduction
	
	$BuyingScript .= isset( $_REQUEST[ "gift_token_serial" ] ) ? "&" : "?";	
	$BuyingScript .= "voucher_code=" . urlencode( stripslashes( $_REQUEST[ "voucher_code" ] ) );

}


if( isset( $_REQUEST[ "voucher_code" ])||isset( $_SESSION[ "voucher" ])){

if( isset( $_REQUEST[ "voucher_code" ]))
$votchecode=$_REQUEST[ "voucher_code" ];
else
 $votchecode=$_SESSION[ "voucher" ];
//print $votchecode;exit;
 $vouchers = false;
//--------- vérifier si le bon de réduction exist --------------//
	if( DBUtil::query( "SELECT 1 FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) ) . " LIMIT 1" )->RecordCount() )
	 {
	 $order_tpl->setData( "voucher", true );
	 $vouchers = true;
	 $_SESSION[ "voucher" ] =  $votchecode;
	 }
	else
	$order_tpl->setData( "voucher", false );
	//-------------- vérifier les dates début et de fin -------------//
	$dates = date("Y-m-d");
	
	$voucherdates = false;
	if( DBUtil::query( 'SELECT 1 FROM discount_voucher WHERE  date_start < "'.$dates.'" AND date_end > "'.$dates.'" AND code = ' . DBUtil::quote( stripslashes( $votchecode ) ) . " LIMIT 1" )->RecordCount() )
	{
	 $order_tpl->setData( "voucherdate", true );
	 $voucherdates = true;
	 }
	else
	$order_tpl->setData( "voucherdate", false );
	//-------------------------vérifier si le bon de réduction n'est pas utiliser par le mme client -------------//
	
	if( isset( $_SESSION[ "idbuyer" ]))
$idbuyer = urldecode($_SESSION[ "idbuyer" ]);
	
	$req = "SELECT voucher_code FROM `order` WHERE voucher_code = ".DBUtil::quote( stripslashes( $votchecode ) )." AND idbuyer = " . DBUtil::quote( stripslashes( $idbuyer ) )." ";
	$res = DBUtil::query($req);

	$voucher_code = $res->fields("voucher_code");
	$order_tpl->setData( "idbuyer"  ,$idbuyer );
	
if ( $res->RecordCount() > 0 ) {
	$order_tpl->setData( "voucherexist", false );
	}
	else {
	$order_tpl->setData( "voucherexist", true );
	}
	
	
	//---------------- vérifier si montant de la commande > amount_min --------------//

	  $voucheramountmins = false;
	$req = "SELECT amount_min FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) )." ";
	$res = DBUtil::query($req);

	$amount_min = $res->fields("amount_min");
	$order_tpl->setData( "amount_min"  ,$amount_min );
	
	if( $amount_min > 0 ){
			$order_amount = 0.0;
			//$it = $this->items->iterator();
			$it = Basket::getInstance()->getItems()->iterator();
			while( $it->hasNext() ){
				
				if( B2B_STRATEGY )
						$order_amount += $it->next()->getTotalET();
				else 	$order_amount += $it->next()->getTotalATI();
				
			}
	$order_tpl->setData( "order_amount"  ,$order_amount );
	
			if( $amount_min < $order_amount ){
			 $order_tpl->setData( "voucheramountmin", true );
			 $voucheramountmins = true;
			}else {	$order_tpl->setData( "voucheramountmin", false ); }
		
		}
		else  { $order_tpl->setData( "voucheramountmin", true );
		$voucheramountmins = true;
		 }

//--------------------------------------------------------Cohérence avec produits et catégories---------------------//
		
		$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) )." ";
	$ress = DBUtil::query($reqs);
$idcategory = $ress->fields("idcategory");
	$order_tpl->setData( "idcategory"  ,$idcategory );
	$idproduct = $ress->fields("idproduct");
	$order_tpl->setData( "idproduct"  ,$idproduct );
	$type = $ress->fields("type");
	$order_tpl->setData( "type"  ,$type );		
	$reference = $ress->fields("reference");
	$order_tpl->setData( "reference"  ,$reference );	
	
	$reqs = "SELECT reference,sellingcost FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$sellingcost = $ress->fields("sellingcost");
	$order_tpl->setData( "sellingcost"  ,$sellingcost );	
	$exist = 0; $exists = 0;
	$order_amount = 0.0;
	$order_tpl->setData( "voucherproduct", false );
	$order_tpl->setData( "voucherpromo", true );
	for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
	
	$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
	//$references = $it->next()->getReference();
	$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
	$rs = DBUtil::query($rq);
	$references = $rs->fields("reference");
	$dates = date("Y-m-d");
	$date_promo=date("Y-m-d H:i:s");
	$q = "SELECT reference FROM promo WHERE reference = '".$references."' and  begin_date < '".$date_promo."' AND end_date > '".$date_promo."'  ";
	$rs = DBUtil::query($q);
	$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	$tab1=array();
	$idcatparent =$idcategorys;
	while($idcatparent!=0){
	array_push($tab1,$idcatparent);
	$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
	$rs2 = DBUtil::query($q2);
	$idcatparent =$rs2->fields("idcategoryparent") ;
	}
	if ( $rs->RecordCount() > 0 ) {
	$ref = $rs->fields("reference");
	$order_tpl->setData( "references", $ref );
	
	$order_tpl->setData( "voucherpromo", false );
	}
	else {
	
		$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	
	
	$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
	$rs = DBUtil::query($q);

	$idcategoryparent = $rs->fields("idcategoryparent");
	
	if( B2B_STRATEGY )
			$amountht = Basket::getInstance()->getItemAt( $i)->getTotalET();
		else 	 
		$amountttc = Basket::getInstance()->getItemAt( $i)->getTotalATI(); 
if( $idproduct > 0  ){
	

	if($idproduct ==  $idprod)
	{
	if($type == "rate"){
			if( B2B_STRATEGY )
				 $order_amount =  $amountttc;		
				else	
			$order_amount = $amountht;
	
		$reduction = (($order_amount * $sellingcost)/100);
		$order_tpl->setData( "reduction"  , Util::priceFormat($reduction) );
	}
	if($type == "amount"){
			if( B2B_STRATEGY )
				$reduction =  $sellingcost;	
				else
			$reduction =  $sellingcost / 1.196;	
	
		
		$order_tpl->setData( "reduction"  , Util::priceFormat($reduction) );
	}
	 $order_tpl->setData( "voucherproduct", true );
	}	
 }
	else  { //$order_tpl->setData( "voucherproduct", true );
	
		if( $idcategory > 0 ){
		
		//if( $idcategory ==  $idcategoryparent ){
	if(in_array($idcategory,$tab1)){	
		if($type == "rate"){
			if( B2B_STRATEGY )
					 $order_amount +=  $amountttc;	
				else	
			$order_amount += $amountht;
		$reduction = (($order_amount * $sellingcost)/100);
		$order_tpl->setData( "reduction"  , Util::priceFormat($reduction) );
	}
		if($type == "amount"){
			if( B2B_STRATEGY )
			$reduction =  $sellingcost;	
				else	
				$reduction =  $sellingcost / 1.196;	
		$order_tpl->setData( "reduction"  , Util::priceFormat($reduction) );
	}
	 $order_tpl->setData( "voucherproduct", true );
	 
	 }
	 
	 }
		else  {// modif ched 22-02
		
		if($type == "rate"){
			if( B2B_STRATEGY )
					 $order_amount +=  $amountttc;	
				else	
			$order_amount += $amountht;
		$reduction = (($order_amount * $sellingcost)/100);
		$order_tpl->setData( "reduction"  , Util::priceFormat($reduction) );
	}
		if($type == "amount"){
			if( B2B_STRATEGY )
			$reduction =  $sellingcost;	
				else	
				$reduction =  $sellingcost / 1.196;	
		$order_tpl->setData( "reduction"  , Util::priceFormat($reduction) );
	}// fin de modif
		 $order_tpl->setData( "voucherproduct", true );
		 
		 
		  }
		}
	
}	
}
	
$order_tpl->setData( "voucher_code", isset( $votchecode ) ? strip_tags( stripslashes(  $votchecode ) ) : false );

if(Basket::getInstance()->setUseVoucher( $votchecode )){
$order_tpl->setData( "vouchervalider", true );
}else  { $order_tpl->setData( "vouchervalider", false ); }
		
/* Afficher ligne bon de reduction */
if(Basket::getInstance()->setUseVoucher( $votchecode )){

	$req = "SELECT reference FROM discount_voucher WHERE code = '" .  stripslashes( $votchecode )."' ";
	$res = DBUtil::query($req);

	$reference = $res->fields("reference");
	
	$reqs = "SELECT * FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$idarticle = $ress->fields("idarticle");
	
	$order_tpl->setData( "reference", $reference );
	$summary = $ress->fields("summary_1");
	$order_tpl->setData( "summary", $summary );
$order_tpl->setData( "code", $votchecode );


/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

 * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction * 
 
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

		$reduce = 0.0;
	
	
	$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) )." ";
	$ress = DBUtil::query($reqs);
$idcategory = $ress->fields("idcategory");
	
	$idproduct = $ress->fields("idproduct");

	$type = $ress->fields("type");
		
	$reference = $ress->fields("reference");
		
	
	$reqs = "SELECT reference,sellingcost,summary_1 FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$sellingcost = $ress->fields("sellingcost");
	
	$exist = 0; $exists = 0;
	$order_amount = 0.0;
	
	for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
	
	$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
	
	$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
	$rs = DBUtil::query($rq);
	$references = $rs->fields("reference");
	$dates = date("Y-m-d");
	$date_promo=date("Y-m-d H:i:s");
	$q = "SELECT reference FROM promo WHERE reference = '".$references."' and  begin_date < '".$date_promo."' AND end_date > '".$date_promo."'  ";
	$rs = DBUtil::query($q);
	$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	$tab1=array();
	$idcatparent =$idcategorys;
	while($idcatparent!=0){
	array_push($tab1,$idcatparent);
	$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
	$rs2 = DBUtil::query($q2);
	$idcatparent =$rs2->fields("idcategoryparent") ;
	}
	if ( !$rs->RecordCount() ) {
	
	
		$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	
	
	$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
	$rs = DBUtil::query($q);

	$idcategoryparent = $rs->fields("idcategoryparent");
	
	if( B2B_STRATEGY )
			$amountht = Basket::getInstance()->getItemAt( $i)->getTotalET();
		else 	 
		$amountttc = Basket::getInstance()->getItemAt( $i)->getTotalATI(); 

if( $idproduct > 0  ){
	

	if($idproduct ==  $idprod)
	{
	if($type == "rate"){
			if( B2B_STRATEGY )
						$order_amount = $amountht;
				else	
			 $order_amount =  $amountttc;
	
		 $reduce = (($order_amount * $sellingcost)/100);
	
	}
	if($type == "amount"){
			if( B2B_STRATEGY )
				$reduce =  $sellingcost / 1.196;		
				else	
			$reduce =  $sellingcost;
	
		
		
	}
	
	}	
 }
	else  { 
	
		if( $idcategory > 0 ){
		
		//if( $idcategory ==  $idcategoryparent ){
			if(in_array($idcategory,$tab1)){
						if($type == "rate"){
							if( B2B_STRATEGY )
										$order_amount += $amountht;
								else	
							 $order_amount +=  $amountttc;
						 $reduce = (($order_amount * $sellingcost)/100);
						
					}
						if($type == "amount"){
							if( B2B_STRATEGY )
								$reduce +=  $sellingcost / 1.196;		
								else	
							$reduce +=  $sellingcost;
						
						
					}

	 
	 }
	 
	 }
	 else  // modif ched 22-02
	 {
	 	if($type == "rate"){
							if( B2B_STRATEGY )
										$order_amount += $amountht;
								else	
							 $order_amount +=  $amountttc;
						 $reduce = (($order_amount * $sellingcost)/100);
						
					}
						if($type == "amount"){
							if( B2B_STRATEGY )
								$reduce +=  $sellingcost / 1.196;		
								else	
							$reduce +=  $sellingcost;
						
						
					}
	 }// fin de modif
		
		}
	
}	
}
	
	}
	}
	// $order_tpl->setData( "reduction"  , $reduce );
/*
if( isset( $_REQUEST[ "voucher_code" ] ) )
	if( DBUtil::query( "SELECT 1 FROM vouchers WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) ) . " LIMIT 1" )->RecordCount() )
 		Basket::getInstance()->setUseVoucher( new Voucher( DBUtil::getDBValue( "idvoucher", "vouchers", "code", stripslashes( $_REQUEST[ "voucher_code" ] ) ) ) );
	
if( $voucher = Basket::getInstance()->getVoucher() )
	$order_tpl->setData( "voucher", ( object )array(
	
		"code" 				=> $voucher->get( "code" ),
		"creation_date" 	=> Util::dateFormat( $voucher->get( "creation_date" ), "d/m/Y" ),
		"expiration_date" 	=> Util::dateFormat( $voucher->get( "expiration_date" ), "d/m/Y" ),
		"rate" 				=> Util::rateFormat( $voucher->get( "rate" ) ),
		"min_order_amount" 	=> Util::priceFormat( $voucher->get( "min_order_amount" ) )
	
	));
else $order_tpl->setData( "voucher", false );
*/
$order_tpl->setData( "voucherexist", true );
if( isset( $_SESSION[ "voucher" ] ) ){
if( isset( $_SESSION[ "idbuyer" ]) &&  isset( $_SESSION[ "voucher" ] )){
$idbuyer = urldecode($_SESSION[ "idbuyer" ]);
	
	$req = "SELECT voucher_code FROM `order` WHERE voucher_code = ".DBUtil::quote( stripslashes( $_SESSION[ "voucher" ] ) )." AND idbuyer = " . DBUtil::quote( stripslashes( $idbuyer ) )." ";
	$res = DBUtil::query($req);

	$voucher_code = $res->fields("voucher_code");
	
if ( $res->RecordCount() > 0 ) {
	$order_tpl->setData( "voucherexist", false );
	}
	
	
}
// $reduce = 0.00;
$code = urldecode($_SESSION[ "voucher" ]);
 //print  $code;exit;
if(Basket::getInstance()->setUseVoucher( stripslashes( $code  ) ) ){
//print  $code;exit;
$req = "SELECT reference FROM discount_voucher WHERE code = '" .  stripslashes( $code )."' ";
	$res = DBUtil::query($req);

	$reference = $res->fields("reference");
	
	$reqs = "SELECT * FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$idarticle = $ress->fields("idarticle");
	
	$order_tpl->setData( "reference", $reference );
	$summary = $ress->fields("summary_1");
	$order_tpl->setData( "summary", $summary );
$order_tpl->setData( "code", $code );


$order_tpl->setData( "vouche", true );

// $reduce = Basket::getInstance()->Getreduction( $code );

 $order_tpl->setData( "voucher_code", $code );
// $order_tpl->setData( "reduction", $reduce );
}

	
}

//----------------------------------------------------------------------------
/* montants */

$articlesET = 0.0;
$it = Basket::getInstance()->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();
	
	$chargesET 	= Basket::getInstance()->getChargesET();
	$chargesATI = Basket::getInstance()->getChargesATI();
	
	
	if( B2B_STRATEGY ){
			$reduceht = 	$reduce;
			$reducettc =  $reduce +($reduce * Basket::getInstance()->getVATRate()) / 100;	
				}else	{
			$reducettc =  $reduce;
			$reduceht = 	$reduce /1.196;
	}

$netapayer =  max( Basket::getInstance()->getNetBill() - $gift_token_amount, 0.0 );
$netapayer_ht =  $articlesET - Basket::getInstance()->getDiscountAmount();
//print $netapayer_ht;exit;
//$netapayers = $netapayer - $reduce;
//$netapayer_ht = $netapayers - ( ($netapayers * 16.39) / 100);
/*
if( B2B_STRATEGY ){
			$netapayers = ($articlesATI + $chargesATI ) - $reducettc;
			$netapayer_ht = $articlesET - Basket::getInstance()->getDiscountAmount();
				}else	{
			$netapayers = ($articlesATI + $chargesATI )- $reducettc ;
			$netapayer_ht =$articlesET - Basket::getInstance()->getDiscountAmount();
	}
*/
if( B2B_STRATEGY ){
			$netapayers = ($articlesATI + $chargesATI ) - $reducettc;
			$netapayer_ht = ($articlesET + $chargesET ) - $reduceht ;	
				}else	{
			$netapayers = ($articlesATI + $chargesATI )- $reducettc;
			$netapayer_ht = ($articlesATI + $chargesATI )- $reduceht ;
	}


//----------------------------------------------------------------------------
/* montants panier récupéré après abandon s'il y a réduction
*/
if(isset($_SESSION[ "id_cart" ]))
{
	
	$reduction_val = DBUtil::getDBValue('reduction', 'cart', 'id_cart', $_SESSION[ "id_cart" ]);
	if($reduction_val && (float)$reduction_val != (float)0)
	{
		$basket_items = array();
		for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++)
			$basket_items[] = Basket::getInstance()->getItemAt( $i)->getArticle()->getId();
			
		$ci = DBUtil::query("SELECT id_article FROM `cart_detail` WHERE `id_cart` = ".$_SESSION[ 'id_cart' ]);
		$cart_items = array();
		$cartInBasket = 0;
		while(!$ci->EOF)
		{
			if (in_array($ci->fields["id_article"], $basket_items))
				$cartInBasket += 1;
			
			$ci->MoveNext();
		}
		if($cartInBasket != $ci->RecordCount())
		{
			$reduction_type = DBUtil::getDBValue('reduction_type', 'cart', 'id_cart', $_SESSION[ "id_cart" ]);
			if ($reduction_type == 0)
				$reduction = $reduction_val;//Réduction calculée par montant
			elseif ($reduction_type == 1)
				$reduction = $articlesET * $reduction_val / 100;//Réduction calculée par pourcentage
			
			if( B2B_STRATEGY ){			
				$netapayer_ht = $articlesET + $chargesET - $reduction;
				$netapayers = $articlesATI + $chargesATI - $reduction * (1 + Basket::getInstance()->getVATRate() / 100) ;
			}
			else
			{
				$netapayer_ht = $articlesATI + $chargesATI - $reduction;
				$netapayers = $articlesATI + $chargesATI - $reduction * (1 + Basket::getInstance()->getVATRate() / 100) ;
			}
			$order_tpl->setData( "reduction"  , $reduction);
			$order_tpl->setData('SPPrice', Util::numberFormat($netapayers));
		}
		else
			DBUtil::getConnection()->Execute("UPDATE cart SET `reduction` = 0.0 WHERE `id_cart`=".$_SESSION[ "id_cart" ]);
		
	}
}



//$it = Basket::getInstance()->getItems()->iterator();
//while( $it->hasNext() )
	//$articlesET += $it->next()->getTotalET();

// $netapayer = max( Basket::getInstance()->getNetBill() - $gift_token_amount, 0.0 );
 //$netapayers = $netapayer - $reduce;
// $netapayer_ht = $netapayers - ( ($netapayers * 16.39) / 100);
$order_tpl->setData( "remise_rate", 				Util::rateFormat( Basket::getInstance()->getDiscountRate() ) );
$order_tpl->setData( "total_discount", 				Util::rateFormat( Basket::getInstance()->getDiscountRate() ) );
$order_tpl->setData( "discount", 					Util::priceFormat( $netapayers ) );
$order_tpl->setData( "total_discount_amount",		Util::priceFormat( $netapayers ) );
$order_tpl->setData( "total_amount_ht", 			Util::priceFormat( $articlesET - Basket::getInstance()->getDiscountAmount() ) );
$order_tpl->setData( "total_amount_ht_wb", 			Util::priceFormat( Basket::getInstance()->getTotalET() ) );
$order_tpl->setData( "total_amount_ttc_wbi", 		Util::priceFormat( Basket::getInstance()->getNetBill() ) );
$order_tpl->setData( "total_amount_ht_avant_remise",Util::priceFormat( $articlesET ) );
$order_tpl->setData( "remise", 						Basket::getInstance()->getDiscountRate() > 0.0 );
$order_tpl->setData( "net_to_pays", 					Util::priceFormat(  $netapayers ) );
$order_tpl->setData( "net_to_pay", 					Util::priceFormat( $netapayer ) );
$order_tpl->setData( "reduction", 					Util::priceFormat( $reduce ) );
$order_tpl->setData( "reduceht", 					Util::numberFormat( $reduceht ) );
$order_tpl->setData( "reducettc", 					Util::numberFormat( $reducettc ) );
$order_tpl->setData( "B2B", 					B2B_STRATEGY );
$order_tpl->setData( "net_to_pay_ht", 				Util::priceFormat($netapayer_ht));


/* frais de port */

if( $articlesET == 0.0 ){ //ne pas afficher les frais de port si prix cachés

	$order_tpl->setData( "total_charge_ht", "-" );
	$order_tpl->setData( "total_charge_ttc", "-" );
	
}
else{
	
	$chargesET 	= Basket::getInstance()->getChargesET();
	$chargesATI = Basket::getInstance()->getChargesATI();
	
	$order_tpl->setData( "total_charge_ht", $chargesET > 0.0 ? Util::priceFormat( $chargesET ) : "FRANCO" ); 
	$order_tpl->setData( "total_charge_ttc", $chargesATI > 0.0 ? Util::priceFormat( $chargesATI ) : "FRANCO" ); 
	
	$order_tpl->setData( "franco", $chargesET == 0.0 );
	
}

//----------------------------------------------------------------------------
		
//articles_read

$hasLot = false;
$i = 0;
$count = Basket::getInstance()->getItemCount();
while( $i < $count && !$hasLot ){
	
	$hasLot = Basket::getInstance()->getItemAt( $i)->getArticle()->get( "lot" ) > 1;
	$i++;
	
}

if($hasLot)
	$colspan=10;
else
	$colspan=9;

$order_tpl->setData('colspan',$colspan);
$order_tpl->setData('haslot',$hasLot);

$basket_article = array();
$HasDiscount = '';

/* includes produits complémentaires */
include_once( dirname( __FILE__ ) . "/../objects/catalog/CProduct.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CProductProxy.php" );
$complements = array();
/*/*/

for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++)
{
	
	$idarticle = Basket::getInstance()->getItemAt($i)->getArticle()->get("idarticle");

	$stock_level = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
	//$quantity = Basket::getInstance()->getItemAt($i)->getQuantity();
	$quantity = Basket::getInstance()->getItemFromIdArticle($idarticle)->getQuantity();
	
	$basket_article[$i]["i"] = $i;
	$basket_article[$i]["icon"] = URLFactory::getReferenceImageURI( Basket::getInstance()->getItems()->get( $i )->getArticle()->get( "idarticle" ) );
	$basket_article[$i]["idarticle"] = $idarticle;
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $idarticle );
	$basket_article[$i]["idproduct"] = $idproduct;
	$basket_article[$i]["productname"] = DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct );
	$basket_article[$i]["productURL"] = URLFactory::getProductURL( $idproduct );
	
	$basket_article[$i]["reference"] = Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "reference" );
	$basket_article[$i]["stock_level"] = $stock_level;
	$basket_article[$i]["quantity"] = $quantity;
	
	$rk="SELECT iv.intitule_value_1 as value FROM intitule_value iv,intitule_link il WHERE il.idintitule_value=iv.idintitule_value AND il.idarticle=$idarticle LIMIT 1";
	$rc=DBUtil::query($rk);

	$valeurTaille=$rc->fields("value");
	$basket_article[$i]["taille"] = $valeurTaille;
	$basket_article[$i]["summary_1"] = Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "summary_1" );
	$basket_article[$i]["designation_1"] = Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "designation_1" );
	$basket_article[$i]["designation"] = '<b>'.Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "summary_1" ).'</b><br />'.Basket::getInstance()->getItemAt($i)->getArticle()->get( "designation_1" );
	$basket_article[$i]["delay"] = Util::checkEncoding(Util::getDelay(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "delivdelay" )));
	$basket_article[$i]["discount_price"] = Util::priceFormat( Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "discount_price" ) );
	$basket_article[$i]["lot"] = Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "lot" );
	$basket_article[$i]["unit"] = Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "unit" );
	$basket_article[$i]["total_price"] = Util::priceFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getTotalET() );
	$basket_article[$i]["unit_price"] = Util::priceFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "unit_price" ));
	$basket_article[$i]["puht"] = Util::priceFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->getPriceEt());
	$basket_article[$i]["ref_discount"] = Util::rateFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "ref_discount" ));
	$basket_article[$i]["discount_price"] = Util::priceFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->get( "discount_price" )); 
	$basket_article[$i]["total_priceTTC"] = Util::priceFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getTotalATI() );
	//$basket_article[$i]["unit_price_ati"] = Util::priceFormat( Basket::getInstance()->getItemAt( $i )->getArticle()->get( "unit_price" ) * ( 1.0 + Basket::getInstance()->getItemAt( $i )->getArticle()->get( "vat_rate" ) / 100.0 ) );

	$basket_article[$i]["unit_price_ati"] = Util::priceFormat(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->getPriceATI());
	//$basket_article[$i]["unit_price_ati"] = Util::priceFormat( Basket::getInstance()->getItemAt( $i )->getArticle()->getPriceATI("unit_price"));

	//var_dump($basket_article[$i]["unit_price_ati"]);
   // var_dump(Basket::getInstance()->getItemAt( $i )->getArticle()->get( "vat_rate"));
	//var_dump(Util::priceFormat(Basket::getInstance()->getItemAt( $i )->getArticle()->get( "lot" )));
	//var_dump(Basket::getInstance()->getItemAt( $i )->getArticle()->get( "vat_rate" ));
	//var_dump(Basket::getInstance()->getItemAt($i)->getArticle()->get( "lot" ));

//	var_dump(Basket::getInstance()->getItemFromIdArticle($idarticle)->getArticle()->getPriceATI());

	$basket_article[$i]["discount_price_ati"] = Util::priceFormat( Basket::getInstance()->getItemAt( $i )->getArticle()->get( "unit_price" ) * ( 1.0 + Basket::getInstance()->getItemAt( $i )->getArticle()->get( "vat_rate" ) / 100.0 ) * ( 1.0 - Basket::getInstance()->getItemAt( $i )->getArticle()->get( "ref_discount" ) / 100.0 ) ); 
	$basket_article[$i]["total_discount_price_ati"] = Util::priceFormat( Basket::getInstance()->getItemAt( $i )->getQuantity() * Basket::getInstance()->getItemAt( $i )->getArticle()->get( "unit_price" ) * ( 1.0 + Basket::getInstance()->getItemAt( $i )->getArticle()->get( "vat_rate" ) / 100.0 ) * ( 1.0 - Basket::getInstance()->getItemAt( $i )->getArticle()->get( "ref_discount" ) / 100.0 ) );
		
	if(Basket::getInstance()->getItemAt($i)->getArticle()->get( "discount_price" ) != Basket::getInstance()->getItemAt($i)->getArticle()->get( "unit_price" )){
		$HasDiscount='ok';
	}
	
	/* produits complémentaires */
	$basket_product_proxy = new CProductProxy( new CProduct( intval( $idproduct ) ) );
	$complements_parts = $basket_product_proxy->getComplements();
	if (count($complements_parts))
		foreach ($complements_parts as $complements_part)
			array_push($complements, $complements_part);
	/*/*/
	
}

/* produits complémentaires */
if (count($complements))
	$order_tpl->setData('complements',$complements);
if (count($complements) > 2)
	$order_tpl->setData('slide_auto', true);
else
	$order_tpl->setData('slide_auto', false);
/*/*/

$order_tpl->setData('basket_articles',$basket_article);
$order_tpl->setData('hasdiscount',$HasDiscount);
	
//----------------------------------------------------------------------------

$order_tpl->setData( "use_cb", (Basket::getInstance()->getNetBill() - $gift_token_amount) - $reduce > 0.0 );

//mode de paiement

if( (Basket::getInstance()->getNetBill() - $gift_token_amount) - $reduce <= 0.0 ){ 

	//net à payer = 0.0
	
	/**
	 * @todo le chèque-cadeau ne peut pas être utilisé comme mode de paiement ( pas d'entrée dans la table payment ) 
	 * car si le montant du chèque ne couvre pas le net à payer il doit pouvoir être combiné avec un autre mode 
	 * de paiement complémentaire et on ne gère pas plusieurs mode de paiement différents pour une même commande 
	 * dans le front office.
	 */
	 
	$radio = array();
			
}
else{
	
	//pas de chèque-cadeau utilisé ou le montant du chèque ne couvre pas le net à payer
	
	$amount_payment = DBUtil::getParameter('amount_payment');

	$articlesATI = 0.0;
	$it = Basket::getInstance()->getItems()->iterator();
	while( $it->hasNext() )
		$articlesATI += $it->next()->getTotalATI();
	
	$ttc = $articlesATI;
	
	
	//Gestion des modes de paiement
	
	if (isset($_POST["idpayment_pre"]))
	{
		$order_tpl->setData('idpayment', $idpayment);
	}
	else
	{
		//if($ttc<=$amount_payment){
			$query = "SELECT idpayment, name$lang FROM payment WHERE use_web=1 AND payment_code NOT LIKE 'GIFT_TOKEN' AND payment_code NOT LIKE 'CB'";
		//}else{
			//$query = "SELECT idpayment, name$lang FROM payment WHERE payment_code NOT LIKE 'GIFT_TOKEN'";
		//}
		
		$rs = DBUtil::query( $query );
		
		$i = 0;
		$radio = array();
			
		while( !$rs->EOF() ){
		
			$idpayment = $rs->fields["idpayment"];
			$payment_name = Util::doNothing( $rs->fields["name$lang"] );
			switch ($idpayment)
			{
				case '21':
					$radio[$i]["image"] = '<div>Payment s&eacute;curis&eacute; avec Paybox.</div><img class="pay_paybox" src="/templates/catalog/images/paybox.jpg" alt="Paybox" />';
					break;
				case '2' :
					$radio[$i]["image"] = '<div>Validation de la commande &agrave; encaissement du ch&egrave;que.</div><img class="pay_check" src="/templates/catalog/images/check.png" alt="Chèque" />';
					break;
				case '3' :
					$radio[$i]["image"] = '<div>Validation de la commande &agrave; r&eacute;ception du virement.<br>D&eacute;bit imm&eacute;diat.</div><img class="pay_cm" src="/templates/catalog/images/credit_mutuel.png" alt="Virement" />';
					break;
			}
			$radio[$i]["idpayment"] = $idpayment;
			$radio[$i]["payment_name"] = $payment_name;
				
			$rs->MoveNext();
			$i++;
				
		}
		$order_tpl->createElement( 'idpayment' , 1 );

		//fianet
		$use_fianet_cb = DBUtil::getParameter('use_fianet_cb') && (Basket::getInstance()->getNetBill() - $gift_token_amount) - $reduce > 0.0;
		$use_fianet_credit = DBUtil::getParameter('use_fianet_credit') && (Basket::getInstance()->getNetBill() - $gift_token_amount) - $reduce > 0.0;
		
		//contrôle de la disponibilité du service
		if( $use_fianet_cb || $use_fianet_credit ){
			
			$document= new DOMDocument();
			
			if( !$document->load( 'http://www.receiveandpay.com/wallet/engine/available.cgi' ) )
				$status='KO';

			//status
			$status = $document->getElementsByTagName( "online" )->item(0)->attributes->getNamedItem( "status" )->nodeValue;
		}
		
		if(	$use_fianet_cb && ( $status=='OK' || $status=='KC' ) )
			$order_tpl->setData('use_fianet_cb','ok');
		else
			$order_tpl->setData('use_fianet_cb','');
			
		if(	$use_fianet_credit && $status=='OK' )
			$order_tpl->setData('use_fianet_credit','ok');
		else
			$order_tpl->setData('use_fianet_credit','');
	}
}	
if (!isset($_POST["idpayment"]))
	$order_tpl->setData('radio',$radio);

//----------------------------------------------------------------------------
//adresses_edit.tpl
//----------------------------------------------------------------------------

if( !isset( $disabled ) )
		$disabled = "";

$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
$where = "WHERE `idbuyer`='$idbuyer'";	

$addressFields = array( 
	"adress", 
	"adress_2", 
	"zipcode", 
	"city", 
	"idstate", 
	"lastname",
	"firstname",
	"company",
	"title",
	"phonenumber",
	"faxnumber"
);

$DeliveryJsData = setDeliveryListJSData();
$order_tpl->setData('setDeliveryListJSData',$DeliveryJsData);

$BillingJsData = setBillingListJSData();
$order_tpl->setData('setBillingListJSData',$BillingJsData);

if( !isset( $_POST["delivery_checkbox"] ) )
{
	$order_tpl->createElement( 'delivery_checkbox' );
	if($deliveryCheckStatus!=""){
		$order_tpl->setElementAttribute( 'delivery_checkbox' , 'checked' , 'checked');
	}
}

if( !isset( $_POST["billing_checkbox"] ) )
{
	$order_tpl->createElement( 'billing_checkbox' );
	if($billingCheckStatus!=""){
		$order_tpl->setElementAttribute( 'billing_checkbox' , 'checked' , 'checked');
	}
}


$drawliftdelivery = DrawDeliveryLift($iddelivery,$where,true,true);
$order_tpl->setData('drawliftdelivery',$drawliftdelivery);

$drawliftbilling = DrawBillingLift($idbilling,$where,true,true);
$order_tpl->setData('drawliftbilling',$drawliftbilling);



if( !Session::getInstance()->getCustomer() ){
	
	//delivery
	$order_tpl->setData('d_company',"");
	$order_tpl->setData('deliveryTitleList',deliveryTitleList());
	$order_tpl->setData('d_lastname',"");
	$order_tpl->setData('d_firstname',"");
	$order_tpl->setData('d_adress',"");
	$order_tpl->setData('d_adress_2',"");
	$order_tpl->setData('d_zipcode',"");
	$order_tpl->setData('d_city',"");
	$order_tpl->setData('d_phonenumber',"");
	$order_tpl->setData('d_faxnumber',"");
	$order_tpl->setData('drawliftd_state',DrawLift("state",1,'',true,true,'delivery[idstate]'));

	//billing
	$order_tpl->setData('b_company',"");
	$order_tpl->setData('billingTitleList',billingTitleList());
	$order_tpl->setData('b_lastname',"");
	$order_tpl->setData('b_firstname',"");
	$order_tpl->setData('b_adress',"");
	$order_tpl->setData('b_adress_2',"");
	$order_tpl->setData('b_zipcode',"");
	$order_tpl->setData('b_city',"");
	$order_tpl->setData('b_phonenumber',"");
	$order_tpl->setData('b_faxnumber',"");
	$order_tpl->setData('drawliftd_state',DrawLift("state",1,'',true,true,'delivery[idstate]'));/**/
	
	//addresses_read.tpl
	$order_tpl->setData('iddelivery',0);
	$order_tpl->setData('idbilling',0);
	$order_tpl->setData('d_buyer_company',"");
	$order_tpl->setData('d_buyer_title',Util::getTitle(0));
	$order_tpl->setData('d_buyer_firstname',"");
	$order_tpl->setData('d_buyer_lastname',"");
	$order_tpl->setData('d_buyer_adress',"");
	$order_tpl->setData('d_buyer_adress_2',"");
	$order_tpl->setData('d_buyer_zipcode',"");
	$order_tpl->setData('d_buyer_city',"");
	$order_tpl->setData('b_buyer_company',"");
	$order_tpl->setData('b_buyer_title',"");
	$order_tpl->setData('b_buyer_firstname',"");
	$order_tpl->setData('b_buyer_lastname',"");
	$order_tpl->setData('b_buyer_adress',"");
	$order_tpl->setData('b_buyer_adress_2',"");
	$order_tpl->setData('b_buyer_zipcode',"");
	$order_tpl->setData('b_buyer_city',"");

}
else{
	
	include_once( dirname( __FILE__ ) . "/../objects/ForwardingAddress.php" );
	
	$address = buyer_GetDeliveryAddress() ? new ForwardingAddress( intval( $_SESSION[ "iddelivery" ] ) ) : Session::getInstance()->getCustomer()->getAddress();
	
	//delivery
	$order_tpl->setData('d_company', $address->getCompany());
	$order_tpl->setData('deliveryTitleList',deliveryTitleList());
	$order_tpl->setData('d_lastname',$address->getLastName());
	$order_tpl->setData('d_firstname',$address->getFirstName());
	$order_tpl->setData('d_adress',$address->getAddress());
	$order_tpl->setData('d_adress_2',$address->getAddress2());
	$order_tpl->setData('d_zipcode',$address->getZipcode());
	$order_tpl->setData('d_city',$address->getCity());
	$order_tpl->setData('d_phonenumber',$address->getPhonenumber());
	$order_tpl->setData('d_faxnumber',$address->getFaxnumber());
	$order_tpl->setData('drawliftd_state',DrawLift("state",$address->getIdState(),'',true,true,'delivery[idstate]', 'javascript:checkIdstate();'));
	//addresses_read.tpl
	if( isset( $_POST["delivery_checkbox"] ) )
		$order_tpl->setData('iddelivery', $iddelivery);
	else
		$order_tpl->setData('iddelivery', buyer_GetDeliveryAddress());
	$order_tpl->setData('d_buyer_company',$address->getCompany());
	$order_tpl->setData('d_buyer_title',DBUtil::getDBValue( "title_1", "title", "idtitle", $address->getGender() ) );
	$order_tpl->setData('d_buyer_firstname',$address->getFirstName());
	$order_tpl->setData('d_buyer_lastname',$address->getLastName());
	$order_tpl->setData('d_buyer_adress',$address->getAddress());
	$adr2=$address->getAddress2();
	if(!empty($adr2)){$order_tpl->setData('d_buyer_adress_2',$adr2."<br />");}else{$order_tpl->setData('d_buyer_adress_2','');}
	$order_tpl->setData('d_buyer_zipcode',$address->getZipcode());
	$order_tpl->setData('d_buyer_city',$address->getCity());

	$address = buyer_GetBillingAddress() ? new InvoiceAddress( intval( $_SESSION[ "idbilling_adress" ] ) ) : Session::getInstance()->getCustomer()->getAddress();
	
	//billing
	$order_tpl->setData('b_company', Util::checkEncoding($address->getCompany()));
	$order_tpl->setData('billingTitleList',billingTitleList());
	$order_tpl->setData('b_lastname',Util::checkEncoding($address->getLastName()));
	$order_tpl->setData('b_firstname',Util::checkEncoding($address->getFirstName()));
	$order_tpl->setData('b_adress',Util::checkEncoding($address->getAddress()));
	$order_tpl->setData('b_adress_2',Util::checkEncoding($address->getAddress2()));
	$order_tpl->setData('b_zipcode',$address->getZipcode());
	$order_tpl->setData('b_city',Util::checkEncoding($address->getCity()));
	$order_tpl->setData('b_phonenumber',$address->getPhonenumber());
	$order_tpl->setData('b_faxnumber',$address->getFaxnumber());
	$order_tpl->setData('drawliftb_state',DrawLift("state",$address->getIdState(),'',true,true,'billing[idstate]'));
	//addresses_read.tpl
	$order_tpl->setData('idbilling',buyer_GetBillingAddress());
	$order_tpl->setData('b_buyer_company',Util::checkEncoding($address->getCompany()));
	$order_tpl->setData('b_buyer_title',DBUtil::getDBValue( "title_1", "title", "idtitle", $address->getGender() ) );
	$order_tpl->setData('b_buyer_firstname',Util::checkEncoding($address->getFirstName()));
	$order_tpl->setData('b_buyer_lastname',Util::checkEncoding($address->getLastName()));
	$order_tpl->setData('b_buyer_adress',Util::checkEncoding($address->getAddress()));
	$adr2=Util::checkEncoding($address->getAddress2());
	if(!empty($adr2)){$order_tpl->setData('b_buyer_adress_2',$adr2."<br />");}else{$order_tpl->setData('b_buyer_adress_2','');}
	$order_tpl->setData('b_buyer_zipcode',$address->getZipcode());
	$order_tpl->setData('b_buyer_city',Util::checkEncoding($address->getCity()));
	
}

//----------------------------------------------------------------------------

$order_tpl->setData('buyer_company',Session::getInstance()->getCustomer() ? Util::checkEncoding(Session::getInstance()->getCustomer()->getAddress()->getCompany()) : "" );
$order_tpl->setData('buyer_title',Session::getInstance()->getCustomer() ? DBUtil::getDBValue( "title_1", "title", "idtitle", Session::getInstance()->getCustomer()->getContact()->getGender() ) : "");
$order_tpl->setData('buyer_firstname',Session::getInstance()->getCustomer() ? Util::checkEncoding(Session::getInstance()->getCustomer()->getContact()->getFirstName()) : "");
$order_tpl->setData('buyer_lastname',Session::getInstance()->getCustomer() ? Util::checkEncoding(Session::getInstance()->getCustomer()->getContact()->getLastName()) : "");
$order_tpl->setData('buyer_adress',Util::checkEncoding(Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getAddress() : ""));
$adr2=Session::getInstance()->getCustomer() ? Util::checkEncoding(Session::getInstance()->getCustomer()->getAddress()->getAddress2()) : "";
if(!empty($adr2)){$order_tpl->setData('buyer_adress_2',$adr2);}else{$order_tpl->setData('buyer_adress_2','');}
$order_tpl->setData('buyer_zipcode',Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getZipcode() : "");
$order_tpl->setData('buyer_city',Session::getInstance()->getCustomer() ? Util::checkEncoding(Session::getInstance()->getCustomer()->getAddress()->getCity()) : "");
$order_tpl->setData('buyer_idstate',Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getIdstate() : "");
$codepostal = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getZipcode() : "";
$codepostal = substr($codepostal,0,2); 
$order_tpl->setData('codepostal',$codepostal);
//buyer_read.tpl
if( $ScriptName != "order_secure.php"){
	$order_tpl->setData('order_secure',0);
}else{
	$order_tpl->setData('order_secure',1);
}

//cart & cart_detail
if ( Session::getInstance()->getCustomer() && isset($_POST['step']) && $_POST['step'] == '1' && $idpayment == '1' && !isset($_SESSION[ "id_cart" ]))
{
	$current_session_id = session_id();
	$id_buyer = Session::getInstance()->getCustomer()->getId();
	$current_basket = Basket::getInstance();
	$cart_id = $current_basket->checkSessionId($current_session_id, $id_buyer);
	if ($cart_id)
		$current_basket->updateCart($cart_id);
	else
		$current_basket->addCart($current_session_id, $id_buyer);
}
//mise à jour cart après modification du panier
if(isset($_SESSION[ "id_cart" ]))
	Basket::getInstance()->updateCart($_SESSION[ "id_cart" ]);

ob_start();
// var_dump(session_id());
// var_dump($cart_items);
// $vd = ob_get_contents();
ob_end_clean();
$order_tpl->setData('var_dump', $vd);

allowOrder( $order_tpl );

$order_tpl->output();
die();
//---------------------------------------------------------------------------------------------
/**
 * Interdire la commande pour la Corse, les DOM-TOMs
 * car le port ne peut pas être calculé automatiquement
 */
function allowOrder( &$controller ){
	
	$controller->setData( "allowOrder", true );
	$controller->setData( "orderRestrictionPromo", false );
	$controller->setData( "orderRestrictionState", false );
				
	global $GLOBAL_START_PATH;
	
	//promo ou destockage
	
	$it = Basket::getInstance()->getItems()->iterator();
	
	while( $it->hasNext() ){
		
		$item =& $it->next();
		
		if( /*$item->getArticle()->get( "hasPromo" ) 
			|| $item->getArticle()->get( "isDestocking" ) 
			|| Product::getReferenceAttribute( $item->getArticle()->get( "reference" ), "type" ) == "destocking" 
			|| */$item->getArticle()->get( "sellingcost" ) == 0.0 
			|| $item->getArticle()->get( "hidecost" ) == 1){
				
				$controller->setData( "allowOrder", false );
				$controller->setData( "orderRestrictionPromo", true );
				
				return;
		
			}
				
	}
	
	if( !Session::getInstance()->getCustomer() )
		return;
		
	$customer = Session::getInstance()->getCustomer();

	//strangers
	
	if( $customer->get( "idstate" ) != 1 || $customer->get( "idstate" ) != 68 ){
		
		$controller->setData( "allowOrder", false );
		$controller->setData( "orderRestrictionState", true );
		
		return;
		
	}

	//adresse de livraison
	
	if( isset( $_POST[ "delivery_checkbox" ] ) && intval( $_POST[ "iddelivery" ] ) ){
		
		include_once( "$GLOBAL_START_PATH/objects/ForwardingAddress.php" );
		$forwardingAddress = new ForwardingAddress( intval( $_POST[ "iddelivery" ] ) );
		
		$zipcode = $forwardingAddress->getZipcode();
		$idstate = $forwardingAddress->getIdState();
		
	}
	else{
		
		$zipcode = $customer->get( "zipcode" );
		$idstate = $customer->get( "idstate" );
		
	}
	
	//corse
	
	if( strlen( $zipcode ) > 2 && substr( $zipcode, 0, 2 ) == "20" ){
		
		$controller->setData( "allowOrder", false );
		$controller->setData( "orderRestrictionState", true );
		
		return;
		
	}
	
	//dom-toms
	
	if( $idstate == 44 ){
		
		$controller->setData( "allowOrder", false );
		$controller->setData( "orderRestrictionState", false );
		
		return;

	}

}

//---------------------------------------------------------------------------------------------

function setDeliveryListJSData(){

	global $addressFields;	
	
	$DeliveryJsData="<script type=\"text/javascript\">";
	
	$i = 0;
	while( $i < buyer_GetDeliveryAddressesCount() ){
			
		$delivery = buyer_GetDeliveryAddressAt( $i );
		$idDelivery = $delivery[ "id" ];
			

		$DeliveryJsData.=" var deliveryAddresses$idDelivery= {};";

		$k = 0;
		while( $k < count( $addressFields ) ){
				
			$field = $addressFields[ $k ];
				
			$DeliveryJsData.="deliveryAddresses$idDelivery.$field = '" . addslashes( $delivery[ $field ] ) . "';";
				
			$k++;
				
		}
			
		$i++;
			
	}
	$DeliveryJsData.="</script>";
	
	return $DeliveryJsData;
}

//--------------------------------
function setBillingListJSData(){

	global $addressFields;	
	
	$BillingJsData="<script type=\"text/javascript\">";
	
	$i = 0;
	while( $i < buyer_GetBillingAddressesCount() ){
			
		$billing = buyer_GetBillingAddressAt( $i );
		$idBilling = $billing[ "id" ];
			
		$BillingJsData.="billingAddresses$idBilling = {};";

		$k = 0;
		while( $k < count( $addressFields ) ){
				
			$field = $addressFields[ $k ];
				
			$BillingJsData.="billingAddresses$idBilling.$field = '" . addslashes( $billing[ $field ] ) . "';";
				
			$k++;
				
		}
			
		$i++;
			
	}
	$BillingJsData.="</script>";
	
	return $BillingJsData;
}

//--------------------------------------
function deliveryTitleList(){

	$iddelivery = buyer_GetDeliveryAddress();
	$idtitle = $iddelivery ? DBUtil::getDBValue( "title", "delivery", "iddelivery", $iddelivery ) : 0;
	
	$list="<select name=\"delivery[Title]\" id=\"delivery[title]\" class=\"AuthInput\">";
	$list.="<option value=\"1\"";
	if($idtitle==1){
		$list.="selected=\"selected\"";
	}
	$list.=">Mr</option>";
	$list.="<option value=\"2\"";
	if($idtitle==2){
		$list.="selected=\"selected\"";
	} 
	$list.=">Mme</option>";
	$list.="<option value=\"3\"";
	if($idtitle==3){
		$list.="selected=\"selected\"";
	} 
	$list.=">Mlle</option></select>";
				
	return $list;
	
}

//--------------------------------------
function billingTitleList(){
	
	$idbilling_adress = buyer_GetDeliveryAddress();
	$idtitle = $idbilling_adress ? DBUtil::getDBValue( "title", "delivery", "iddelivery", $idbilling_adress ) : 0;
	
	$list="<select name=\"billing[Title]\" id=\"billing[title]\" class=\"AuthInput\">";
	$list.="<option value=\"1\"";
	if($idtitle==1){
		$list.="selected=\"selected\"";
	}
	$list.=">Mr</option>";
	$list.="<option value=\"2\"";
	if($idtitle==2){
		$list.="selected=\"selected\"";
	} 
	$list.=">Mme</option>";
	$list.="<option value=\"3\"";
	if($idtitle==3){
		$list.="selected=\"selected\"";
	} 
	$list.=">Mlle</option></select>";
				
	return $list;
	
}

?>