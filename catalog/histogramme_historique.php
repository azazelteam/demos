<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Génération des histogrammes
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include ( dirname( __FILE__ ) . "/../jpgraph/jpgraph.php");
include ( dirname( __FILE__ ). "/../jpgraph/jpgraph_bar.php");

$tableaucategorie = array();

$date = array();
$tabledate = array();
$tableprice = array();
// **********************************************
// Données dans ta base de données
// *************************************************
$a="2014-01-01";
$b="2014-12-31";
$g="1";
 $statStartDate = $a;
 $statEndDate = $b;
 $idgrouping = $g;
 
$arrCA=array();
$ListBuyer =array();
$searchDateParam = "period";
$datesearch		= Date("Y");
 
 $query = "SELECT conf_order_date AS DateH, idbuyer, SUM(discount_price) AS Price_total , discount_price
				FROM grouping_story
				WHERE (conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' ) 
				and idgrouping = $idgrouping 
				GROUP BY conf_order_date, idbuyer
				ORDER BY conf_order_date ASC
				";
	//print $query;								 
$rs = & DBUtil::query( $query );



		$n = $rs->RecordCount();
		
		for($i=0;$i<$n;$i++){
			if (!in_array($rs->fields("idbuyer"), $ListBuyer))
			array_push( $ListBuyer, $rs->fields("idbuyer") );
	
	
			$rs->Movenext();
		}




$ListBuyer = array_unique($ListBuyer);


$rs = & DBUtil::query( $query );
		for($i=0;$i<$n;$i++){
		
			$arrCA[$i]['date'] = $rs->fields("DateH");
			
			for ($k=0;$k<count($ListBuyer);$k++){
			$buyer = $ListBuyer[$k];
				
			
			
			$querytest = "SELECT idbuyer
				FROM grouping_story
				WHERE 
				discount_price ='". $rs->fields("discount_price")."'
				AND conf_order_date ='" .$rs->fields("DateH")."'
				
				";
			
			$rstest = & DBUtil::query( $querytest );
			
			$buyername = $rstest->fields("idbuyer");
			
			if ($buyer == $buyername)
			$arrCA[$i][$buyer] = $rs->fields("Price_total");
			else
			$arrCA[$i][$buyer] = 0;
			
			}
			
			
			$rs->Movenext();
		}


for ($i=0;$i<count($arrCA);$i++)
{

	$tableaucategorie[$i] = $arrCA[$i]['date'];
	
	for ($k=0;$k<count($ListBuyer);$k++){
	$buyer = $ListBuyer[$k];
	
	$nom = "tableauouvert";
	$tableauouvert[$k][$i] = $arrCA[$i][$buyer];
	

}
}
 
 
//tableau de couleurs du graphe

$colors= array("#FF6A2D","#FFFF66","#90EE90","#99CCFF","#FF99FF","#FF9933","#F6E433","#CAFF70","#66FFFF","#E066FF","#FF4040",
"#EEC900","#E4F192","#54FF9F","#AB82FF","#FF5536","#9999FF","#FFE4B5","#66FF66","#FF7F24");


 
// *******************
// Création du graphique
// *******************
 
// Construction du conteneur
// Spécification largeur et hauteur
$graph = new Graph(1000,500);
 
// Réprésentation linéaire
$graph->SetScale("textlin");
 
// Ajouter une ombre au conteneur
//$graph->SetShadow();
 
// Fixer les marges
$graph->img->SetMargin(60,30,25,140);
 
// Chaque histogramme sera placé dans un tableau commun 
$aGroupBarPlot = array();
//Histo 1

for ($k=0;$k<count($ListBuyer);$k++){
$nom = "bplot".$k;



$$nom = new BarPlot($tableauouvert[$k]);
$aGroupBarPlot[] = $$nom;

}

 
//Objet qui regroupe les histogrammes
$gbarplot = new GroupBarPlot($aGroupBarPlot);
 
// Spécification des couleurs des barres

for ($k=0;$k<count($ListBuyer);$k++)
{
$nom = "bplot".$k;
$color = $colors[$k];
$$nom->SetFillColor($color);
}
 
// Afficher les valeurs pour chaque barre
$bplot0->value->Show();
// Fixer l'aspect de la police
$bplot0->value->SetFont(FF_ARIAL,FS_NORMAL,9);
// Modifier le rendu de chaque valeur
$bplot0->value->SetFormat('%d');
 
// Le titre
$graph->title->Set("Graphique 'HISTOGRAMME' : Historique du groupement");
$graph->title->SetFont(FF_FONT1,FS_BOLD);
 
// Titre pour l'axe horizontal(axe x) et vertical (axe y)
//$graph->xaxis->title->Set("Années");
//$graph->yaxis->title->Set("Nombre de ventes");
 
$graph->yaxis->title->SetFont(FF_FONT1,FS_BOLD);
$graph->xaxis->title->SetFont(FF_FONT1,FS_BOLD);
 
// Légende pour l'axe horizontal
$graph->xaxis->SetTickLabels($tableaucategorie);
$graph->xaxis->SetFont(FF_ARIAL,FS_NORMAL,8); 
$graph->xaxis->SetLabelAngle(50);
 
// Ajouter au graphique les 2 histos
$graph->Add($gbarplot);
 
// Afficher
$graph->Stroke();
 
?>
