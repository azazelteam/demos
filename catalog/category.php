<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonctions principales pour l'affichage des catégories
 */
 

include_once('../objects/classes.php');
include_once('navigation.php');

//-----------------------------------------------------------
/** Inistialisation du template pour la charte categorie défaut
 * centre du template
 */
require_once 'HTML/Template/Flexy.php';
require_once 'PEAR.php';

$options['templateDir'][] = "$GLOBAL_START_PATH/templates/catalog/";

$charte_category = new HTML_Template_Flexy($options);

$baseURL=$GLOBAL_START_URL;
$charte_category->setData('baseURL',$baseURL);


if( Basket::getInstance()->getItemCount()> 0 ){
	$nbbasket=Basket::getInstance()->getItemCount();
}else{
	$nbbasket="";
}

//montant du panier
$charte_category->setData('total_amount_ht_wb',Util::priceFormat( Basket::getInstance()->getTotalET()));	

$charte_category->setData('nbBasketHead',$nbbasket);	
//---------------------------------------------//
//---------------Google analytics--------------//
//---------------------------------------------//

$uacct = DBUtil::getParameterAdmin( "google_analytics" );

if( $uacct )
	$charte_category->setData( "google_analytics", true );
else
	$charte_category->setData( "google_analytics", null );

$charte_category->setData( "uacct", $uacct );

//------------------------------------------------------------

$Category = new CATEGORY($IdCategory);
//Session::getInstance()->statistique('view',$Category);

// Récupération des paramètres
$NbProductsByPages  = DBUtil::getParameter("nb_products_by_pages");
$NbColumnProducts   = DBUtil::getParameter("nb_column_products");

$ParameterOutputArray["IdCategory"] = $IdCategory;

$NbCategoriesByPages = $Category->GetNbCategoriesByPages();
$NbColumnCategories  = $Category->GetNbColumnCategorie();

// si les params dans table category sont nulls alors on prend les valeurs par defaut dans les paramètres globaux
if($NbCategoriesByPages<=0) $NbCategoriesByPages = DBUtil::getParameter("nb_categories_by_pages");
if( $NbColumnCategories<=0) $NbColumnCategories  = DBUtil::getParameter("nb_column_categories");

// Calcul de la limite de catégories à afficher et récupération des informations
$LimiteMaxC = $Category->CalculateLimiteToDisplay($NbCategoriesByPages,$CurrentPageNumberC);
$ParameterOutputArray["LimiteMaxC"] = $LimiteMaxC;

// Calcul du nombre de sous-catégories
$NbAllCats = $Category->GetNbSubCategory();

if( empty($NbAllCats) ) {
	// Calcul du nombre de produits lorsqu'il n'y a pas de sous-catégories
	$NbAllProducts = $Category->GetNbProductsInCategory();
}

// Définition des paramètres à passer dans les liens vers les pages suivantes
$ParameterOutputArray["IdCategory"] = $IdCategory;
$ParameterOutputArray["FromScript"]	= $ScriptName;
$ParameterOutputArray["nb_column_categories"]	= $NbColumnCategories;
$ParameterOutputArray["nb_categories_by_pages"]	= $NbCategoriesByPages;
$CurrentPageNumber1 = $CurrentPageNumberP;
$CurrentPageNumber2 = $CurrentPageNumberC;
$ParameterOutputArray["CurrentPageNumberP"]	= $CurrentPageNumber1;
$ParameterOutputArray["CurrentPageNumberC"]	= $CurrentPageNumber2;

// Page précédente
if( !empty($NbAllCats) )		$ParameterOutputArray["CurrentPageNumberC"]	= $CurrentPageNumberC-1; 
$LinkToPreviousPage = CreateLinkToPage('category.php',$ParameterOutputArray);

// Page suivante
if( !empty($NbAllCats) )		$ParameterOutputArray["CurrentPageNumberC"]	= $CurrentPageNumberC+1;
$LinkToNextPage = CreateLinkToPage('category.php',$ParameterOutputArray);

$ParameterOutputArray["CurrentPageNumberC"]	= $CurrentPageNumber2;

$Chartec="$GLOBAL_START_PATH/templates/catalog/chartec" . $Category->GetCharteCategory() . ".htm.php";


if( $Category->GetHTML() !='' ) {

	header("Location: ".$Category->GetHTML());
	exit;
}


if( !empty($NbAllCats) ){
	 $maintemplate=$Chartec;
	 
	 /**
	 * Détection du navigateur (PEAR)
	 */
	//require_once('Net/UserAgent/Detect.php');
	
	$DEBUG = 'none';
	
	// Liste des catégories
	include_once($GLOBAL_START_PATH.'/catalog/drawlift.php');
	
	// Feuilles de styles spécifique à la page
	
	$HTML_link['stylesheet'][3] = "$GLOBAL_START_URL/css/catalog/product.css";
	$HTML_link['stylesheet'][4] = "$GLOBAL_START_URL/css/catalog/category.css";
	$HTML_link['stylesheet'][5] = "$GLOBAL_START_URL/css/catalog/monthpromo.css";
	$HTML_link['stylesheet'][6] = "$GLOBAL_START_URL/css/catalog/droite.css";
	$HTML_link['stylesheet'][7] = "$GLOBAL_START_URL/css/catalog/footer.css";
	
	$IdCategory = $Category->getid();
	if( !empty($IdCategory)){
		$HTML_title = $Category->getname();
	}else{
		$HTML_title = $Category->getname(); 
	}
	
	$infoplus=$Category->getinfo_plus();
	$DEBUG = 'none';
	
	//barre de gauche à utiliser
	$leftcolumn="$GLOBAL_START_PATH/www/barre_g_chartec.php";
	$leftcolumn="$GLOBAL_START_PATH/www/barre_g0.php";
	
	//barre de droite à utiliser
	$rightcolumn="$GLOBAL_START_PATH/www/barre_d_chartec.php";
	$rightcolumn="$GLOBAL_START_PATH/www/barre_d0.php";
	
	//utlilisation du module de rcherche
	$searchmodule=true;
	
	
	$categoryNews = $Category->GetActu();
	$categoryDescription = $Category->GetDesc();

	if( $Category->getParent() ){

		$categoryPath = $Category->getPath();
		$charte_category->setData('categoryPath',$categoryPath);
	}else{
		$charte_category->setData('categoryPath','');
	}
	
	$categoryName=$Category->getname();
	$charte_category->setData('categoryName',$categoryName);
	
	if( $Category->GetList() ){
		$hrefListProducts = URLFactory::getProductListURL( $Category->getid(), $Category->getname() );
		$charte_category->setData('hrefListProducts',$hrefListProducts);
	}else{
		$charte_category->setData('hrefListProducts','');
	}
	
	if( strlen( $categoryNews ) ){
		$charte_category->setData('categoryNews',$categoryNews);
	}else{
		$charte_category->setData('categoryNews','');
	}
	
	if( strlen( $categoryDescription ) ){
		$charte_category->setData('categoryDescription',$categoryDescription);
	}else{
		$charte_category->setData('categoryDescription','');
	}
	
	$SearchCategory = $Category->GetSearch();
	if( $SearchCategory > 0 ) {
		$CharteDescSearchEngine = 1;
		$charte_category->setData('SearchCategory',$SearchCategory);
	}else{
		$charte_category->setData('SearchCategory','');
	}
	
	//création de tableaux pour l'affichage des catégories
	if( !isset( $SSEResult ) || !count( $SSEResult ) ){
		$CategoryInfoArray = $Category->getSubCategories();
		$charte_category->setData('SearchResults','');

		/*$ImagesArray[]=array();
		$TitleArray[]=array();
		$NbProdsArray[]=array();
		for ($Compteur = 0; $Compteur < count($CategoryInfoArray); $Compteur=$Compteur+$NbColumnCategories){
			$ImagesArray[]=DrawImageRow($Compteur, $CategoryInfoArray,$NbColumnCategories,$ScriptName,$ParameterOutputArray);
			$TitleArray[]=DrawTitleRow($Compteur, $CategoryInfoArray,$NbColumnCategories,$ScriptName,$ParameterOutputArray);
			$NbProdsArray[]=DrawNbProdRow($Compteur, $CategoryInfoArray,$NbColumnCategories,$ScriptName,$ParameterOutputArray);
		}*/
		
		
		$i = 0;
		$column = 0;
		$row = 0;
		$listFields = array();

		foreach( $CategoryInfoArray as $SSCat){
		    $CategoryInfoArray[$i]["nbprods"]= NbRefsCalculate($CategoryInfoArray[$i]["idcategory"]);
		    $CategoryInfoArray[$i]["imageurl"]= URLFactory::getCategoryImageURI( $CategoryInfoArray[$i]["idcategory"] );
		    $CategoryInfoArray[$i]["category_url"]= URLFactory::getCategoryURL($CategoryInfoArray[$i]["idcategory"],$CategoryInfoArray[$i][ "name" ] );
			$i++;
		}
		
		$i = 0;
		foreach( $CategoryInfoArray as $SSCat){
			//die($CategoryInfoArray[$i]["idcategory"]."idc");
		    $CategoryInfoArray[$i]["nbprods"]= NbRefsCalculate($CategoryInfoArray[$i]["idcategory"]);
		    $listFields[ $row ][ $column ] = $SSCat;
		    $column++;	    
		    if( $column > 0 && $column % $NbColumnCategories == 0 ){	    
		        $row++;    
		        $column = 0;	        
		    }	    
		    $i++;	    
		}	
		$charte_category->setData('listFields',$listFields);
	}
	

	
	$desc_plus = $Category->GetDescPlus();
	if( strlen( $desc_plus ) ){
		$charte_category->setData('desc_plus',$desc_plus);
	}
	
	/********************
	 * LAST CONSULTATION* 
	 * ******************/
	$limit = 7;
	if(isset ($_COOKIE["FrontOfficeNetlogix"])){
		$myCookie = $_COOKIE["FrontOfficeNetlogix"];
	}else{
		$myCookie="";
	}
	$myDate = date("Y") . date("m");
	$myTable = "stat" . $myDate;

	$db = DBUTIL::getConnection();

	$query = "SELECT  DISTINCT(idproduct) from " . $myTable . " where idsession = \"" . $myCookie . "\" and idproduct <> 0 order by DateHeure Desc limit " . $limit . "";

	$rs = $db->Execute($query);
	$rsTxt = $rs;

	// Nouvelle dimension des images
	$n_largeur = 60;
	$n_hauteur = 60;
	$lang = Session::getInstance()->getLang();
	
	$productHistory = array();
	while (!$rs->EOF) {

		// Requete pour recupérer le chemin complet de l'image
		$query2 = "SELECT idproduct, name$lang from product where idproduct = " . $rs->fields('idproduct') . "";
		$rs2 = $db->Execute($query2);
		//On construit le chmin de l'image

		$productHistory[] = array(
		
			"image" 		=> URLFactory::getProductImageURI( $rs2->fields( "idproduct" ) ),
			"name" 			=> $rs2->fields( "name$lang" ),
			"idproduct" 	=> $rs2->fields( "idproduct" )
			
		);
		
		$rs->MoveNext();
	}

	$charte_category->setData('products',$productHistory);
	$charte_category->setData('n_largeur',$n_largeur);
	$charte_category->setData('n_hauteur',$n_hauteur);

	$charte_category->setData('n_hauteur',$n_hauteur);
	//header de la page
	//include("$GLOBAL_START_PATH/www/head.htm.php");
	
	include_once( "$GLOBAL_START_PATH/catalog/init_head.php" );
	
	$head = new HTML_Template_Flexy($options);
	$title=$HTML_title;
	$head->setData('title',$title);
	$head->setData('baseURL',$baseURL);
	$head->setData('meta_description',$meta_desc);
	$head->setData('meta_keywords',$meta_keywords);
	$head->setData('meta_author',$meta_author);
	
	$head->compile("head.tpl");
	$head->output();
		
	//page
	unset($maintemplate);
	include("$GLOBAL_START_PATH/www/main.htm.php");
	
	$charte_category->compile("/chartec_default.tpl");
	$charte_category->output();
	
	//fermeture de la page
//	include_once("$GLOBAL_START_PATH/refeo.php");
//	echo "jhgqhjqgfhgsdhfhffhsdf";
	
	//fermeture de la page
	include_once("$GLOBAL_START_PATH/www/footer.php");
		
}else{
	header( "Location: ../index.php" );
}


function NbRefsCalculate($idcat){
	
	
	$bdd=&DBUtil::getConnection();
	//Droits du buyer
	$right = isset( $_SESSION[ "buyerinfo" ][ "catalog_right" ] ) ? Session::getInstance()->getDeprecatedBuyer()->GetValue('catalog_right') : 0;

	$qc = "SELECT d.reference, p.idproduct FROM product p, detail d WHERE p.idcategory IN ('$idcat') AND p.catalog_right<=$right AND p.available=1 AND p.idproduct = d.idproduct AND d.stock_level<>0";
	$rc = $bdd->Execute($qc);
	
	$nbprod=$rc->RecordCount();
	
	return $nbprod;
			
}

function FormatImageURL($directory, $name){
	global  $GLOBAL_START_URL;
	
	$imgpath=$GLOBAL_START_URL."/www".$directory."/".$name;
	
	return $imgpath;
}


?>