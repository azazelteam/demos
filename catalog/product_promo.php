<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Liste des produits en promotion
 */

include_once('../objects/classes.php');
include_once('../objects/catalpromoobject.php');
include_once('navigation.php');
 
$IdCategory = 0;
if( isset($_GET['IdCategory']) ) $IdCategory = $_GET['IdCategory'];
elseif( isset($_POST['IdCategory']) ) $IdCategory = $_POST['IdCategory'];

$Category = new CATALPROMO($IdCategory);
//Session::getInstance()->statistique('view',$Category);

	$NbAllProducts = $Category->GetNBProduct();

	// Définition des paramètres à passer dans les liens vers les pages suivantes
	//$ParameterOutputArray["IdCategory"] = $IdCategory;
	$ParameterOutputArray["FromScript"]	= $ScriptName;
	$CurrentPageNumber1 = $CurrentPageNumberP;
	$CurrentPageNumber2 = $CurrentPageNumberC;
	$ParameterOutputArray["CurrentPageNumberP"]	= $CurrentPageNumber1;
	$ParameterOutputArray["CurrentPageNumberC"]	= $CurrentPageNumber2;
	//$ParameterOutputArray["Promo"] = $Promo;

	// Feuille de styles spécifique à la page
	$HTML_link['stylesheet'][3] = "$GLOBAL_START_URL/css/catalog/product.css";
	$HTML_link['stylesheet'][4] = "$GLOBAL_START_URL/css/catalog/category.css";
	$HTML_link['stylesheet'][] = "$GLOBAL_START_URL/css/catalog/product_one.css";
	
	// JavaScripts
	$path = '../js/';
	$HTML_head[$path][3] = 'displayquantityprices.js';
	$HTML_head[$path][4] = 'focus.js';
	
	$HTML_title = 'Promotions';
	
	//header de la page
	include("$GLOBAL_START_PATH/www/head.htm.php");
	
	//template du centre de la page
	$maintemplate="$GLOBAL_START_PATH/catalog/include/chartep_promo.htm.php";
	
	//barre de gauche à utiliser
	$leftcolumn="$GLOBAL_START_PATH/www/barre_g.php";
	
	//barre de droite à utiliser
	//$rightcolumn="$GLOBAL_START_PATH/www/barre_d3.php";
	
	//utlilisation du module de rcherche
	$searchmodule=true;
	
	//page
	include("$GLOBAL_START_PATH/www/main.htm.php");
	
	//fermeture de la page
	include_once("$GLOBAL_START_PATH/www/footer.php");
	?>