<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Ajout direct des références à la commande
 */

function dobasket($IdRealProduct,$Qtt)
{
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	Basket::getInstance()->addArticle( new CArticle( $IdRealProduct ),$Qtt);
	
}


function VerifRef($qte, $ref, &$notrefs,&$notqtts)
{

$Base = &DBUtil::getConnection();
$Query = "
SELECT idarticle,stock_level
FROM product p,detail d
WHERE d.reference = '" . trim($ref) . "' 
AND p.available = 1 
AND p.idproduct = d.idproduct";

$rs= DBUtil::query ($Query);
$tcount = $rs->RecordCount() ;
//echo $Query;
if(  $tcount == 1 ) 
	{
	$stock = $rs->Fields("stock_level");
	$IdRealProduct = $rs->Fields("idarticle");

	if( dobasket($IdRealProduct,$qte) == 0  ) return $qte ;
	//echo "commande ok $qte  $ref <br />";
	}

	$notrefs[] = $ref;
	$notqtts[] = $qte;

return 0;
}

function RechRef(&$notrefs,&$notqtts)
{	
global $GLOBAL_START_PATH,$GLOBAL_START_URL;

	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	$Base = DBUtil::getConnection();
	$Langue = "_1";
	// on cherche les paramètres pour l'affiche, définir d'autres paramètres éventuellement...
	$NbColumnCategories  = DBUtil::getParameter("nb_column_categories");
	// Calcul de la limite de catégories à afficher et récupération des informations
	// max 50 produits
////CalculateLimiteToDisplay(100,1, $LimiteMinC, $LimiteMaxC);
	$tcount = 0;
	$LimiteMinC = 0;
	$LimiteMaxC = 100;
	$nbnr = count($notrefs);
for($j=0; $j < $nbnr; $j++)
{
	$aref = $notrefs[$j];
    $skey = str_replace(' ', '%',trim($aref));

	$Query = "
	SELECT d.idarticle, 
		p.idproduct, 
		p.name$Langue as name,
		d.designation$Langue as desi,
		p.description$Langue as descrip ,
		reference 
	FROM product p, detail d 
	WHERE d.reference LIKE '%$skey%' 
	AND ( d.stock_level <> 0 ) 
	AND p.available = 1  AND p.idproduct = d.idproduct 
	ORDER BY d.reference LIMIT 0,50";

	$rs=& DBUtil::query($Query);
	$tcount = $rs->RecordCount() ;
	
	//if( $fin > 5.0 ) break;
	
 if ($tcount>0)
	{

include_once($GLOBAL_START_PATH.'/catalog/order_direct_result.htm.php');

}

}
}

?>