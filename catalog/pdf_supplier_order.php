<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf commandes fournisseurs
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFSupplierOrder.php" );


$odf = new ODFSupplierOrder(  $_REQUEST[ "idorder" ]);

$odf->exportPDF( "Commande fournisseur n° " .  $_REQUEST[ "idorder" ]  . ".pdf" );

?>