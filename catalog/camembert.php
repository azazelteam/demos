<?php 

 /**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Classe de création de camembert statistiques 3D
 */
 

class Camembert
{
	
	var $fontSize;
	var $heightImg;
	var $widthImg;
	var $bigDiam;
	var $heightEllipseTxt;
	var $widthEllipseTxt;
	var $datas;
	var $nomImg;
	var $legend;
	var $percent;
	var $minPercent;
	var $text;
 	var $idGraph;
 	var $graphClass;
 	var $pieOptions;


	
/**
 * Constructeur
 * 
 * @param mixed $tabval Tableau des valeurs du diagramme
 * @param string $name Nom du fichier image de sortie
 * @param string $idGraph Id du graphe, et du div conteneur
 * @param string $graphClass classe CSS pour limiter le décalage des étiquettes: "pieChart" pour le premier graphe d'une page, "pieChart2" pour les autres
 * @param int $fontsize Taille de la police
 * @param int $hImg Hauteur de l'image
 * @param int $wImg Largeur de l'image
 * @param int $bigDiam Taille du grand diametre
 * @param int $heightEllipseTxt Hauteur de l'ellipse d'affichage du texte
 * @param int $widthEllipseTxt Largeur de l'ellipse d'affichage du texte
 * @return void
 */ 
	function __constructor($tabval,$name,$idGraph,$graphClass,$fontsize=3,$hImg=250,$wImg=570,$bigDiam=130,$heightEllipseTxt=112,$widthEllipseTxt=158){
		$this->datas = $tabval;
		$this->idGraph = $idGraph;
		$this->graphClass = $graphClass;
		$this->fontSize = $fontsize;
		$this->heightImg = $hImg; 
		$this->widthImg = $wImg;
		$this->bigDiam = $bigDiam;
		$this->heightEllipseTxt = $heightEllipseTxt;
		$this->widthEllipseTxt = $widthEllipseTxt;
		$this->nomImg = $name;
		$this->legend = 1;
		$this->text = 1;
		$this->percent = 1;
		$this->minPercent = DBUtil::getParameterAdmin("pie_min_percent");
		$this->pieOptions = ''; 
	}

/**
 * Masque la légende
 * 
 * @return void
 */	
	function hideLegend(){
		$this->legend = 0;
	}
	
/**
 * Masque les pourcentages
 * 
 * @return void
 */
	function hidePercent(){
		$this->percent = 0;
	}
	
/**
 * Masque le texte
 * 
 * @return void
 */
	function hideText(){
		$this->text = 0;
	}
	
/**
 * Récupérer les couleurs du camembert dans le fichier
 * 
 * @return string
 */
 	function getColors()
	{
		global $camcolor;
				
		$colors = 'colors: [';
		$moreThanOne = false;
		
		foreach($camcolor as $color)
		{
			if($moreThanOne) $colors.=',';
			else $moreThanOne = true;
			$colors .= '"#'.$color.'"';
		}
		
		$colors .= '],'; 
		return $colors;
	}	
	
/**
 * Défini les options du camembert
 * 
 * @return void
 */
	function setOptions()
	{
		echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8" />';
		$this->pieOptions = '{'.$this->getColors().'
					pie: {
					show: true, 
					pieStrokeLineWidth: 0, 
					pieStrokeColor: \'#FFF\', 
					//pieChartRadius: 150, 			// by default it calculated by 
					//centerOffsetTop:30,
					//centerOffsetLeft:30, 			// if auto and legend position is "nw" then centerOffsetLeft is equal a width of legend.
					showLabel: true,				//use ".pieLabel div" to format looks of labels
					//labelOffsetFactor: 5/6, 		// part of radius (default 5/6)
					labelOffset: 0,        		// offset in pixels if > 0 then labelOffsetFactor is ignored
					labelBackgroundOpacity: 1, 		// default is 0.85
					labelFormatter: function(serie){// default formatter is "serie.label"
					//return serie.label;
					//return serie.data;
					//return serie.label+\'<br/>\'+Math.round(serie.percent)+\'%\';
					return serie.label+\' (\'+Math.round(serie.percent)+\'%)\';
					}
				},
				legend: {show: false}
				})
				});
				</script>'; 
	}
	
/**
 * Conversion d'une couleur Hexa en RVB
 * 
 * @param mixed $hexa Valeur de couleur hexadecimal à convertir
 * @return mixed $tab Tableau des valeurs RVB
 */ 
	function convertHexaEnRVB($hexa){
		for ($i=0; $i<3; $i++){
			$tab[$i]=hexdec(substr($hexa, (2 * $i)+1,2));
		}
		return $tab;
	}

/**
 * Création et affichage du camembert
 * 
 * @return void
 */ 	
	function Show()
	{	

  	 	$this->setOptions();
  	 	
  	 	
  	 	echo '<div class="'.$this->graphClass.'" id="'.$this->idGraph.'"></div>'; 
  	 	
  	 	echo '<script language="Javascript">'."\n".'$(function (){'."\n"; 
		
		$DataSet = '$.plot($("#'.$this->idGraph.'"), [';   
		
		$total = 0;
		for($i=0;$i<count($this->datas);$i++){ 
			$total += $this->datas[$i][0]; 
		}
		$others = 0;
		$firstseted = false;
		for($i=0;$i<count($this->datas);$i++){
			// Affiche les valeur individuellement si > 6%
			if( abs(($this->datas[$i][0]/$total))>$this->minPercent && $this->datas[$i][0]>0 )
			{			
				if($firstseted) $DataSet .= ', ';
					
				$DataSet .= '{label: "'.$this->datas[$i][2].'", data: '.$this->datas[$i][0].'}';
				$firstseted = true; 		
			}
			// Sinon regroupe dans une valeur "others"
			else if($this->datas[$i][0]>0)
			{
				$others += $this->datas[$i][0]; 
			}	
		}
		if($others > 0)
		{
			if($firstseted == true) $DataSet .= ',';
			$DataSet .= ' {label: "autres", data: '.$others.'}';
		}	  
		$DataSet .= '],'."\n"; 
		
		// Ecriture du code js de génération du graphe
		echo $DataSet;
		echo $this->pieOptions;
  	 	
  	 	// ancienne partie (conservée pour export provisoirement
  	 	$arr=$this->datas;				// données à afficher
   		$size=$this->fontSize;          /* taille de la police, largeur du caractère */
   		$ifw=imagefontwidth($size);							
	
   		$w=$this->widthImg;             /* largeur de l'image */
   		$h=$this->heightImg;            /* hauteur de l'image */
   		$a=$this->bigDiam;              /* grand axe du camembert */
   		$b=$a/2;                  		/* petit axe du camembert */
   		$d=$a/3;                  		/* "épaisseur" du camembert */
   		$cx=$w/2-1;               		/* abscisse du "centre" du camembert */
   		$cy=($h-$d)/2;            		/* ordonnée du "centre" du camembert */
	
   		$A=$this->widthEllipseTxt;      /* grand axe de l'ellipse "englobante" */
   		$B=$this->heightEllipseTxt;     /* petit axe de l'ellipse "englobante" */
   		$oy=-$d/2;                		/* du "centre" du camembert à celui de l'ellipse "englobante"*/

   		$img=imagecreate($w,$h);  
   		
   		$bgcolor=imagecolorallocate($img,0xFF,0xFF,0xFF);
   		// Couleur de fond transparente
   		//$bgcolor=imagecolorallocate($img,0xCD,0xCD,0xCD);	
   		//imagecolortransparent($img,$bgcolor); 
   		
   		$black=imagecolorallocate($img,0,0,0);
   		$gray=imagecolorallocate($img,110,110,110);
        /* calcule la somme des données */
   		for ($i=$sum=0,$n=count($arr);$i<$n;$i++){
   			if($arr[$i][0]>=0){ $sum+=$arr[$i][0]; } else { $arr[$i][0]=0; }
   		}	
	
   if($sum!=0){

   		for ($i=$v[0]=0,$x[0]=$cx+$a,$y[0]=$cy,$doit=true;$i<$n;$i++) {														
     		$t = $this->convertHexaEnRVB($arr[$i][1]);
                             /* détermine les "vraies" couleurs */
      		$color[$i]=imagecolorallocate($img,$t[0],$t[1],$t[2]);
//      		echo ("color[i]=($img,$t[0],$t[1],$t[2])<br>");
                             /* calcule l'angle des différents "secteurs" */
      		$v[$i+1]=$v[$i]+round($arr[$i][0]*360/$sum);	
														
      if ($doit) {           /* détermine les couleurs "ombrées" */
         $shade[$i]=imagecolorallocate($img,max(0,$t[0]-50),max(0,$t[1]-50),max(0,$t[2]-50)); 
														
         if ($v[$i+1]<180) { /* calcule les coordonnées des différents parallélogrammes */
            $x[$i+1]=$cx+$a*cos($v[$i+1]*M_PI/180);		
            $y[$i+1]=$cy+$b*sin($v[$i+1]*M_PI/180);	
         }										
         else { 
            $m=$i+1;
            $x[$m]=$cx-$a;   /* c'est comme si on remplaçait $v[$i+1] par 180° */
            $y[$m]=$cy;	
            $doit=false;     /* indique qu'il est inutile de continuer! */
         }
      }
   }
	

   /* dessine la "base" du camembert */
   for ($i=0;$i<$m;$i++) imagefilledarc($img,$cx,$cy+$d,2*$a,2*$b,$v[$i],$v[$i+1],$shade[$i],IMG_ARC_PIE);
	
   /* dessine la partie "verticale" du camembert */														
   for ($i=0;$i<$m;$i++) {						
      $area=array($x[$i],$y[$i]+$d,$x[$i],$y[$i],$x[$i+1],$y[$i+1],$x[$i+1],$y[$i+1]+$d);
      imagefilledpolygon($img,$area,4,$shade[$i]);			
   }

   /* dessine le dessus du camembert */
   for ($i=0;$i<$n;$i++){
   		if($v[$i]!=$v[$i+1])
   		{
	   		imagefilledarc($img,$cx,$cy,2*$a,2*$b,$v[$i],$v[$i+1],$color[$i],IMG_ARC_PIE);
	   	}
	}
   if($this->legend == 1){
   	
   /*imageellipse($img,$cx,$cy-$oy,2*$A,2*$B,$black);	// dessine l'ellipse "englobante" 
	
   /* dessine les "flêches" et met en place le texte */
   for ($i=0,$AA=$A*$A,$BB=$B*$B;$i<$n;$i++) if ($arr[$i][0]) {
      $phi=($v[$i+1]+$v[$i])/2;       
      /* intersection des "flêches" avec l'ellipse "englobante" */
      $px=$a*3*cos($phi*M_PI/180)/4;		
      $py=$b*3*sin($phi*M_PI/180)/4;		
      /* équation du 2ème degré avec 2 racines réelles et distinctes */	
      $U=$AA*$py*$py+$BB*$px*$px;         
      $V=$AA*$oy*$px*$py;						
      $W=$AA*$px*$px*($oy*$oy-$BB);	
      /* calcule le pourcentage à afficher */
      $value=number_format(100*$arr[$i][0]/$sum,2,",","")."%";
       
    if((100*$arr[$i][0]/$sum)>2){
           /* écrit le texte à droite */	
      if ($phi<91 || $phi>270) {          
         $root=(-$V+sqrt($V*$V-$U*$W))/$U;
         imageline($img,$px+$cx,$py+$cy,$qx=$root+$cx,$qy=$root*$py/$px+$cy,$black);
         imageline($img,$qx,$qy,$qx+10,$qy,$black);		
		
         if($this->text==1){
         	$textsize = $ifw*strlen($arr[$i][2]); 
         	imagestring($img,$size,$qx+14,$qy-8,$arr[$i][2],$gray);
         } else { $textsize = 0; }         
         if($this->percent==1) imagestring($img,$size,$qx+14+$textsize,$qy-8,' '.$value,$black);
      }
      else {/* écrit le texte à gauche */
         $root=(-$V-sqrt($V*$V-$U*$W))/$U;
         imageline($img,$px+$cx,$py+$cy,$qx=$root+$cx,$qy=$root*$py/$px+$cy,$black);
         imageline($img,$qx,$qy,$qx-10,$qy,$black);		
			 		
         if($this->text==1)	imagestring($img,$size,$qx-27-$ifw*strlen($value)-$ifw*strlen($arr[$i][2]),$qy-8,$arr[$i][2],$gray);
         if($this->percent==1) imagestring($img,$size,$qx-25-$ifw*strlen($value),$qy-8,' '.$value,$black);
      }
     }
    }
   }
 

   imagepng($img,$this->nomImg.".png");
   imagedestroy($img);
   //echo '<img src="'.$this->nomImg.'.png" >';
   chmod($this->nomImg.'.png',0755);
	} 
	
	}
	
}


// Exemple Configuration du camembert ##############################################################################

/*$arr=array(
//        donnée   couleur    légende 
   array(    40,  0xFF99CC,   "Bennes"),
   array(    80,  0xFF9900,   "Pelles"),
   array(   160,  0xFFCB03,   "Chariots"),
   array(   160,  0x99CC00,   "Ventes"),
   array(   200,  0x339966,   "Charges"),
   array(   240,  0x33CCCC,   "Rentrée"),
   array(   280,  0x0091C3,   "Fourniture"),
   array(   340,  0x969696,   "Manutention"),
   array(   520,  0x942696,   "Manugyjhj"),
   array(   440,  0x96def6,   "uotuiyntion")
);

$cam = new Camembert($arr,"temp/".Date("YmdHms")."-Camembert","Graph","pieChart",3,250,600,120,102,138);
$cam->Show(); //Affichage du Camembert*/
                     
?>
