<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Confirmation commande
 */


include( "./deprecations.php" );

include_once( dirname( __FILE__ ) . "/../config/init.php" );  
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );  
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/OrderProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/Order.php" );
if(isset($_SESSION["idorder_secure"]) && (intval($_SESSION["idorder_secure"])>0)){
	
	$idorder = $_SESSION["idorder_secure"];
	$Order = new Order( $idorder );
	
}else{
	
	header('Location: '.$GLOBAL_START_URL.'/index.php');
	exit();
	
}


//----------------------------------------------------------------------------------------------

class OrderController extends PageController{};
$confirm_order = new OrderController( "confirm_order.tpl" );

//----------------------------------------------------------------------------------------------
/* proxy commande */
if( $Order->get( "idpayment" ) == 1 ){

$confirm_order->setData( "mode_payment_used","Carte bancaire");
	}

$confirm_order->setData( "order", new OrderProxy( $Order ) );

//----------------------------------------------------------------------------------------------

if( Session::getInstance()->getCustomer() ) {
	$confirm_order->setData('isLogin', "ok");
	
} else {
	$confirm_order->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$confirm_order->setData('com_step_empty', "ok");
	} else {
		$confirm_order->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$confirm_order->setData('issetPostLoginAndNotLogged', "ok");
		$confirm_order->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$confirm_order->setData('issetPostLoginAndNotLogged', "");
	}
	$confirm_order->setData('scriptName', $ScriptName);
	$confirm_order->setData('translateLogin',Dictionnary::translate('login'));
	$confirm_order->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$confirm_order->setData('img', "acceder_compte.png");
		$confirm_order->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$confirm_order->setData('img', "poursuivre.png");
		$confirm_order->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

$confirm_order->setData('idorder',$idorder);
//articles_read
$hasLot = false;
$i = 0;
$count = $Order->getItemCount();
while( $i < $count && !$hasLot ){
	
	$hasLot = $Order->getItemAt( $i)->get( "lot" ) > 1;
	$i++;
	
}

if($hasLot)
	$colspan=10;
else
	$colspan=9;

$confirm_order->setData('colspan',$colspan);
$confirm_order->setData('haslot',$hasLot);

$HasDiscount = '';


for ($i=0 ; $i < $Order->getItemCount() ; $i++)
{
	 
	$idarticle = $Order->getItemAt($i)->get("idarticle");
	$stock_level = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
	$quantity = $Order->getItemAt( $i)->get( "quantity" );
	
	if($Order->getItemAt( $i)->get( "discount_price" ) != $Order->getItemAt( $i)->get( "unit_price" )){
		$HasDiscount='ok';
	}	
			
}

$confirm_order->setData('hasdiscount',$HasDiscount);
//basket_amounts
//Remise commercial

$confirm_order->setData('estimate', true );

//buyer_read.tpl

if( $ScriptName != "order_secure.php"){
	$confirm_order->setData('order_secure',0);
}else{
	$confirm_order->setData('order_secure',1);
}


$confirm_order->setData('buyer_zipcode',Session::getInstance()->getCustomer()->get("zipcode"));
$confirm_order->setData('buyer_city',Session::getInstance()->getCustomer()->get("city"));

//addresses_read.tpl

$confirm_order->setData('iddelivery',$iddelivery); 	//initialisation ???
$confirm_order->setData('idbilling',$idbilling);	//initialisation ???

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/AddressProxy.php" );

$confirm_order->setData('forwardingAddress', new AddressProxy( $Order->getForwardingAddress() ) );
$confirm_order->setData('invoiceAddress', new AddressProxy( $Order->getInvoiceAddress() ) );

/* ------------------------------------------------------------------------------------------------------------- */
/*
 * Bon de réduction
 */

include_once( dirname( __FILE__ ) . "/../objects/Voucher.php" );

if( isset( $_REQUEST[ "voucher_code" ] ) )
	Basket::getInstance()->setUseVoucher( new Voucher( DBUtil::getDBValue( "idvoucher", "vouchers", "code", stripslashes( $_REQUEST[ "voucher_code" ] ) ) ) );
	
if( $voucher = Basket::getInstance()->getVoucher() )
	$confirm_order->setData( "voucher", ( object )array(
	
		"code" 				=> $voucher->get( "code" ),
		"creation_date" 	=> Util::dateFormat( $voucher->get( "creation_date" ), "d/m/Y" ),
		"expiration_date" 	=> Util::dateFormat( $voucher->get( "expiration_date" ), "d/m/Y" ),
		"rate" 				=> Util::rateFormat( $voucher->get( "rate" ) ),
		"min_order_amount" 	=> Util::priceFormat( $voucher->get( "min_order_amount" ) )
	
	));
else $confirm_order->setData( "voucher", false );	

$confirm_order->setData( "voucher_code", isset( $_REQUEST[ "voucher_code" ] ) ? strip_tags( stripslashes( $_REQUEST[ "voucher_code" ] ) ) : false );

//----------------------------------------------------------------------------
/* chèque cadeau */

$gift_token = $Order->getGiftToken();

$confirm_order->setData( "gift_token_serial", 			$gift_token == false ? "" : $gift_token->get( "serial" ) );
$confirm_order->setData( "gift_token_error", 			""  );
$confirm_order->setData( "display_gift_token_serial", 	false );

$gift_token_amount = $gift_token == false ? 0.0 : DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $gift_token->get( "idtoken_model" ) );
$confirm_order->setData( "gift_token_amount", $gift_token_amount );

//----------------------------------------------------------------------------
/* montants */

$articlesET = 0.0;
$it = $Order->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();
;


$confirm_order->setData( "remise_rate", 					Util::rateFormat( $Order->get( "total_discount" ) ) );
$confirm_order->setData( "total_discount", 					Util::rateFormat( $Order->get( "total_discount" ) ) );
$confirm_order->setData( "discount", 						Util::priceFormat( $Order->get( "total_discount_amount" ) ) );
$confirm_order->setData( "total_discount_amount",			Util::priceFormat( $Order->get( "total_discount_amount" ) ) );
$confirm_order->setData( "total_amount_ht", 				Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) );
$confirm_order->setData( "total_amount_ht_wb", 				Util::priceFormat( $Order->getTotalET() ) );
$confirm_order->setData( "total_amount_ttc_wbi", 			Util::priceFormat( $Order->getNetBill() ) );
$confirm_order->setData( "total_amount_ht_avant_remise", 	Util::priceFormat( $articlesET ) );
$confirm_order->setData( "remise", 							$Order->get( "total_discount" ) > 0.0 );
$confirm_order->setData( "net_to_pay", 						Util::numberFormat( max( $Order->getNetBill() - $gift_token_amount, 0.0 ) ) );

$confirm_order->setData('net_to_pay_ht',Util::priceFormat($Order->get( "total_amount" )));
/* frais de port */

if( $articlesET == 0.0 ){ //ne pas afficher les frais de port si prix cachés

	$confirm_order->setData( "total_charge_ht", "-" );
	$confirm_order->setData( "total_charge_ttc", "-" );
	
}
else{
	
	$chargesET 	= $Order->getChargesET();
	$chargesATI = $Order->getChargesATI();
	$confirm_order->setData( "total_charge_ht", $chargesET > 0.0 ? Util::priceFormat( $chargesET ) : "FRANCO" ); 
	$confirm_order->setData( "total_charge_ttc", $chargesATI > 0.0 ? Util::priceFormat( $chargesATI ) : "FRANCO" ); 
	$confirm_order->setData( "franco", $chargesET == 0.0 );
}

//----------------------------------------------------------------------------
	
//mode de paiement

$query = "SELECT idpayment, name_1 FROM payment WHERE use_web=1";

$rs = DBUtil::query( $query );

$i = 0;

$radio = array();

while( !$rs->EOF() ){

	$idpayment = $rs->fields["idpayment"];
	$payment_name = htmlentities( $rs->fields["name_1"] );
	$radio[$i]["idpayment"] = $idpayment;
	$radio[$i]["payment_name"] = $payment_name;
	$rs->MoveNext();
	$i++;
		
}

$confirm_order->setData('radio',$radio);


$paid = $Order->get("paid");
$status = $Order->get("status");
	
if($paid)
	$confirm_order->setData('paid','ok');
else
	$confirm_order->setData('paid','');
	
$confirm_order->createElement('idpayment',$Order->get('idpayment'));	

//tracker
$effiliation = DBUtil::getParameter('use_effiliation_tag');

if($effiliation){
	if($paid){
		
		$articlesET = 0.0;
		$it = $Order->getItems()->iterator();
		while( $it->hasNext() )
			$articlesET += $it->next()->getTotalET();
	
		$confirm_order->setData('tracker','ok');
		$confirm_order->setData('tracker_idorder',$Order->getId());
		$confirm_order->setData('tracker_ht',round($articlesET - $Order->get( "total_discount_amount" ),2));
		
	}
}

$confirm_order->setData('displayfoot','ok');
$confirm_order->setData('displayhead','');

$confirm_order->setData('basic_payment','');

//------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/OrderProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CustomerProxy.php" );

$confirm_order->setData( "order", new OrderProxy( $Order ) );
// commenté suite à un bug
//$confirm_order->setData( "customer", new CustomerProxy( $Order->getCustomer() ) );

/*$tab="";
for ($i=0 ; $i < $Order->getItemCount() ; $i++)
{
	 
	$idarticle = $Order->getItemAt($i)->get("idarticle");

$qqs = "SELECT p.`idcategory`,p.`idproduct` FROM detail d inner join product p on d.`idproduct`=p.`idproduct` WHERE d.`idarticle` = '".$idarticle."' ";
$results= DBUtil::query($qqs);

	$tab.= $results->fields("idcategory").":";
	$tab.= $results->fields("idproduct").";";			
}*/
//------------------------------------------------------------------------------------------
//$confirm_order->setData('articles',$tab);

$basket_article = array();
/*/*/

for ($i=0 ; $i < $Order->getItemCount() ; $i++)
{
	$basket_article[$i] = array();
	$idarticle = $Order->getItemAt($i)->get("idarticle");


	$stock_level = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
	//$quantity = Basket::getInstance()->getItemAt($i)->getQuantity();
	$quantity = $Order->getItemAt($i)->getQuantity();
	
	$basket_article[$i]["i"] = $i;
	$basket_article[$i]["icon"] = URLFactory::getReferenceImageURI( $Order->getItemAt($i)->get( "idarticle" ) );
	$basket_article[$i]["idarticle"] = $idarticle;
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $idarticle );
	$basket_article[$i]["idproduct"] = $idproduct;
	$basket_article[$i]["productname"] = DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct );
	$basket_article[$i]["productURL"] = URLFactory::getProductURL( $idproduct );
	
	$basket_article[$i]["reference"] = $Order->getItemAt($i)->get( "reference" );
	$basket_article[$i]["stock_level"] = $stock_level;
	$basket_article[$i]["quantity"] = $quantity;
	
	$rk="SELECT iv.intitule_value_1 as value FROM intitule_value iv,intitule_link il WHERE il.idintitule_value=iv.idintitule_value AND il.idarticle=$idarticle LIMIT 1";
	$rc=DBUtil::query($rk);

	$valeurTaille=$rc->fields("value");
	$basket_article[$i]["taille"] = $valeurTaille;
	$basket_article[$i]["summary_1"] = $Order->getItemAt($i)->get( "summary_1" );
	$basket_article[$i]["designation_1"] = $Order->getItemAt($i)->get( "designation_1" );
	$basket_article[$i]["designation"] = '<b>'.$Order->getItemAt($i)->get( "summary_1" ).'</b><br />'.$Order->getItemAt($i)->get( "designation_1" );
	$basket_article[$i]["delay"] = Util::checkEncoding(Util::getDelay($Order->getItemAt($i)->get( "delivdelay" )));
	$basket_article[$i]["discount_price"] = Util::priceFormat( $Order->getItemAt($i)->get( "discount_price" ) );
	$basket_article[$i]["lot"] = $Order->getItemAt($i)->get( "lot" );
	$basket_article[$i]["unit"] = $Order->getItemAt($i)->get( "unit" );
	$basket_article[$i]["total_price"] = Util::priceFormat($Order->getItemAt($i)->getTotalET() );
	$basket_article[$i]["unit_price"] = Util::priceFormat($Order->getItemAt($i)->get( "unit_price" ));
	$basket_article[$i]["puht"] = Util::priceFormat($Order->getItemAt($i)->getPriceEt());
	$basket_article[$i]["ref_discount"] = Util::rateFormat($Order->getItemAt($i)->get( "ref_discount" ));
	$basket_article[$i]["discount_price"] = Util::priceFormat($Order->getItemAt($i)->get( "discount_price" )); 
	$basket_article[$i]["total_priceTTC"] = Util::priceFormat($Order->getItemAt($i)->getTotalATI() );
	$basket_article[$i]["unit_price_ati"] = Util::priceFormat($Order->getItemAt($i)->getPriceATI());
	$basket_article[$i]["discount_price_ati"] = Util::priceFormat( $Order->getItemAt($i)->get( "unit_price" ) * ( 1.0 + $Order->getItemAt($i)->get( "vat_rate" ) / 100.0 ) * ( 1.0 - $Order->getItemAt($i)->get( "ref_discount" ) / 100.0 ) ); 
	$basket_article[$i]["total_discount_price_ati"] = Util::priceFormat( $Order->getItemAt($i)->getQuantity() 
		* $Order->getItemAt($i)->get( "unit_price" ) 
		* ( 1.0 + $Order->getItemAt($i)->get( "vat_rate" ) / 100.0 ) 
		* ( 1.0 - $Order->getItemAt($i)->get( "ref_discount" ) / 100.0 ) );
	if($Order->getItemAt($i)->get( "discount_price" ) != $Order->getItemAt($i)->get( "unit_price" )){
		$HasDiscount='ok';
	}
}

$confirm_order->setData('basket_articles',$basket_article);


$confirm_order->output();

?>