<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet devis client sur le front-office 
 */
 
 
include_once('../objects/classes.php');  
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");

class AdminOrderController extends PageController{};
$admin_estimate = new AdminOrderController( "admin_estimate.tpl" );

$lang = "_1";

/**************************************************
 *********************ACCOUNT**********************
 **************************************************/
if( Session::getInstance()->getCustomer() ) {
	$admin_estimate->setData('isLogin', "ok");

	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	
	$admin_estimate->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$admin_estimate->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$admin_estimate->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$admin_estimate->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$admin_estimate->setData( "contact_lastname", $salesman->get("lastname") );
	$admin_estimate->setData( "contact_firstname", $salesman->get("firstname") );
	$admin_estimate->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$admin_estimate->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$admin_estimate->setData( "contact_email", $salesman->get("email") );
	
	$admin_estimate->setData( "isEstimate", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$admin_estimate->setData( "contact_grouping", $contact_grouping );
	
} else {
	$admin_estimate->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$admin_estimate->setData('com_step_empty', "ok");
	} else {
		$admin_estimate->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$admin_estimate->setData('issetPostLoginAndNotLogged', "ok");
		$admin_estimate->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$admin_estimate->setData('issetPostLoginAndNotLogged', "");
	}
	$admin_estimate->setData('scriptName', $ScriptName);
	$admin_estimate->setData('translateLogin',Dictionnary::translate('login'));
	$admin_estimate->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$admin_estimate->setData('img', "acceder_compte.png");
		$admin_estimate->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$admin_estimate->setData('img', "poursuivre.png");
		$admin_estimate->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

/**************************************************
 ****************ADMIN_ORDER.TPL*******************
 **************************************************/

$FormName = 'adm_estim';


/**************************************************
 ***************ADMIN_SEARCH.TPL*******************
 **************************************************/

$Arr_LastYear = getdate(mktime(0,0,0,date('m'),date('d'),date('Y'))); // back in time one year
$Arr_Today = getdate(); // today

$admin_estimate->setData('ScriptName',$ScriptName); 
$admin_estimate->setData('FormName',$FormName);
 
// Si l'utilisateur est identifié
if( Session::getInstance()->getCustomer() ) 
{
	if( isset($_POST['search']) ) $search = isset($_POST['search']) ? $_POST['search'] : '';
	
	$TableNameParent = 'order';
	$TableNameChild = 'order_row';
	$FieldId = 'idorder';
	$admin_estimate->setData('IdBuyer',Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0);
	
}

//Liste déroulante depuis
$select_from ='<select name="minus_date_select" class="AuthInput">
			<option value="-2">'.Dictionnary::translate("order_2days").'
					<option value="-7">'.Dictionnary::translate("order_1week").'
					<option value="-14">'.Dictionnary::translate("order_2weeks").'
					<option value="-30">'.Dictionnary::translate("order_1month").'
					<option value="-365">'.Dictionnary::translate("order_1year").'
				</select>';

$admin_estimate->setData('select_from',$select_from);

// Liste déroulante affichant les 12 derniers mois par ordre décroissant
$dat = getdate();
$mon = $dat['mon'] - 1;
$year = $dat['year'] ;
$months = array (
				1 => 'Janvier',
				2 => 'Février',
				3 => 'Mars',
				4 => 'Avril',
				5 => 'Mai',
				6 => 'Juin',
				7 => 'Juillet',
				8 => 'Août',
				9 => 'Septembre',
				10 => 'Octobre',
				11 => 'Novembre',
				12 => 'Décembre'
				);
				
$select_month='<select name="yearly_month_select" class="AuthInput">';

for ( $i = $mon + 12 ; $i > $mon ;  $i--){ //parse one year
	$j = ( ( ( $i-1) % 12 + 1 )  ); // the month
	if ($j == 12) {
		$year--;
		$j = 0;
	}
	
	$select_month.='<option value="'.++$j.'">'.$months[$j].' '.$year.'</option>';
	
	$j++;
}
$select_month.='</select>';

$admin_estimate->setData('select_month',$select_month);

$admin_estimate->setData('LastYearmday',$Arr_LastYear['mday']);
$admin_estimate->setData('LastYearmon',$Arr_LastYear['mon']);
$admin_estimate->setData('LastYearyear',$Arr_LastYear['year']);

$admin_estimate->setData('Todaymday',$Arr_Today['mday']);
$admin_estimate->setData('Todaymon',$Arr_Today['mon']);
$admin_estimate->setData('Todayyear',$Arr_Today['year']);

$admin_estimate->setData('adm_estim',1);

$admin_estimate->setData('NoResults',1);

if( !empty( $_REQUEST["search"]) || !empty( $_REQUEST["search_x"] ) || isset( $_REQUEST[ "all" ] ) ){
	
	if( isset( $_REQUEST[ "all" ] ) ){
		
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$query = "
		
		SELECT idestimate FROM estimate
		WHERE idbuyer = '$idbuyer'
		AND `status` IN ( 'ToDo', 'InProgress', 'Send', 'Ordered' )
		ORDER BY DateHeure DESC";
		
		/*$query = "
		
		SELECT idestimate FROM estimate
		WHERE idbuyer = '$idbuyer'
		AND ( valid_until = '0000-00-00' OR valid_until >= NOW() )
		AND `status` IN ( 'ToDo', 'InProgress', 'Send', 'Ordered' )
		ORDER BY DateHeure DESC";
*/
		
		$rs =& DBUtil::query( $query );
				
	}
	else include($GLOBAL_START_PATH.'/catalog/admin_search.inc.php');
	
	if( $rs->RecordCount() )
		$admin_estimate->setData('NoResults',$rs->RecordCount() ?  false : true);

	$array_estimate = array();
	
	$mois = array("","janvier","f&eacute;vrier","mars","avril","mai","juin","juillet","ao&ucirc;t","septembre","octobre","novembre","d&eacute;cembre");
	
	
	for($i=0;$i<$rs->RecordCount();$i++){
	
		$estimate = new Estimate( $rs->fields( "idestimate" ) );

		$array_estimate[$i]["i"] 			= $i;
		$array_estimate[$i]["idestimate"] 	= $estimate->getId();
		$array_estimate[$i]["idbuyer"] 		= $estimate->get( "idbuyer" );
		$array_estimate[$i]["iddelivery"] 	= $estimate->get( "iddelivery" );
		
		list($datedev,$heure)=explode(" ", $estimate->get('DateHeure') );
		list($y,$m,$d)=explode("-",$datedev);
		$dateorder = $d." ".$mois[intval($m)]." ".$y;
		
		$array_estimate[$i]["DateHeure"] 	= $dateorder;
		
		$Str_Date_Valid = $estimate->get('valid_until');
		if($Str_Date_Valid=='0000-00-00')
			$Str_Date_Valid='-';
		else
			$Str_Date_Valid=usDate2eu($estimate->get('valid_until'));
		$array_estimate[$i]["valid_until"] = $Str_Date_Valid;
			
		$status = $estimate->get('status');
		if($status=="ToDo")
			$status ="InProgress";
		$array_estimate[$i]["status"] = Util::doNothing(Dictionnary::translate( $status ));
		$array_estimate[$i]["total_amount_ht"] =  $estimate->getTotalET() > 0.0 ? Util::priceFormat($estimate->getTotalET()) : "-";
		$array_estimate[$i]["total_amount"] =  $estimate->getTotalATI() > 0.0 ? Util::priceFormat($estimate->getTotalATI()) : "-";
		
		$color = false;
		if($status == "InProgress") {
			$color = true;
		}
		$array_estimate[$i]["color"] 	= $color;
		
		$rs->MoveNext();
		
	}
	
	$admin_estimate->setData('array_estimate',$array_estimate);
}	
$admin_estimate->output();
?>
