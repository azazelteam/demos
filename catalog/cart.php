<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Ajout ou suppression références au panier
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketProxy.php" );

$_POST = Util::arrayMapRecursive( "strip_tags", 	$_POST );
$_POST = Util::arrayMapRecursive( "stripslahes", 	$_POST );

/* ------------------------------------------------------------------------------------------------------------------ */
/**
 * ajout d'un ou plusieurs articles au panier
 * 		$_REQUEST[ "idarticle" ] 	int ou array<int>
 * 		$_REQUEST[ "quantity" ] 	int ou array<int>, optionnel
 * 		$_REQUEST[ "idcolor" ] 		int ou array<int>, optionnel
 * 		$_REQUEST[ "idcloth" ] 		int ou array<int>, optionnel
 */
if( isset( $_REQUEST[ "idarticle" ] ) ){
	
	if( !is_array( $_REQUEST[ "idarticle" ] ) ){ /* ajout d'une seule référence */
	
		addBasketItem( 
			
			intval( $_REQUEST[ "idarticle" ] ), 
			isset( $_REQUEST[ "quantity" ] ) ? intval( $_REQUEST[ "quantity" ] ) : 1,
			isset( $_REQUEST[ "idcolor" ] ) ? intval( $_REQUEST[ "idcolor" ] ) 	: 0,
			isset( $_REQUEST[ "idcloth" ] ) ? intval( $_REQUEST[ "idcloth" ] ) 	: 0
			
		);
	
	}
	else{  /* ajout de plusieurs références */
		
		$i = 0;
		while( $i < count( $_REQUEST[ "idarticle" ] ) ){
		
			addBasketItem(
			
				intval( $_REQUEST[ "idarticle" ][ $i ] ), 
				isset( $_REQUEST[ "quantity" ] ) ? intval( $_REQUEST[ "quantity" ][ $i ] ) 	: 1,
				isset( $_REQUEST[ "idcolor" ] ) ? intval( $_REQUEST[ "idcolor" ][ $i ] ) 	: 0,
				isset( $_REQUEST[ "idcloth" ] ) ? intval( $_REQUEST[ "idcloth" ][ $i ] ) 	: 0
				
			);

			$i++;
		
		}
	
	}

	Basket::getInstance()->save();

}

/* ------------------------------------------------------------------------------------------------------------------ */
/**
 * suppression d'une référence du panier : méthode GET, POST ou COOKIE avec les paramètres suivants :
 * 		$_REQUEST[ "delete" ] 	void
 * 		$_REQUEST[ "index" ] 	int
 */
if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "index" ] ) ){
	
	Basket::getInstance()->removeItemAt( intval( $_REQUEST[ "index" ] ) );

	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/**
 * Vider le panier
 * 		$_REQUEST[ "reset" ] void
 */
if( isset( $_REQUEST[ "reset" ] ) )
	Basket::getInstance()->removeItems();

/* ------------------------------------------------------------------------------------------------------------- */
/**
 * Modifier les quantités
 * 		$_REQUEST[ "update" ] 	void
 * 		$_REQUEST[ "quantity" ] array<int>
 */
if( isset( $_POST[ "update" ] ) ){
	
	$index = 0;
	foreach( $_POST[ "quantity" ] as $quantity ){
		
		$quantity = max( 1, Basket::getInstance()->getItems()->get( $index )->getArticle()->get( "min_cde" ), intval( $quantity ) );
		
		Basket::getInstance()->getItemAt( $index )->setQuantity( intval( $quantity ) );

		$index++;
		
	}

}

/* ------------------------------------------------------------------------------------------------------------- */

Basket::getInstance()->save();

class CartController extends PageController{};
$controller = new CartController( "cart.tpl" );

$basketController->setData( "category", 		new CCategoryProxy( new CCategory( 0 ) ) );
$basketController->setData( "auto_estimate", 	DBUtil::getParameter( "auto_estimate" ) );

$basketProxy = new BasketProxy();

$basketController->setData( "basket", 		$basketProxy );
$basketController->setData( "basketItems", 	$basketProxy->getItems() );

/* ------------------------------------------------------------------------------------------------------------------ */

function addBasketItem( $idarticle, $quantity = 1, $idcolor = 0, $idcloth = 0 ){
	
	if( !$quantity )
		return;
		
	$it = Basket::getInstance()->getItems()->iterator();
	
	while( $it->hasNext() ){
	
		$item =& $it->next();
		
		if( $item->getArticle()->getId() == $idarticle 
			&& ( ( !$idcolor && $item->getColor() ) == NULL || ( $idcolor && $item->getColor() != NULL && $item->getColor()->getId() == $idcolor ) )
			&& ( ( !$idcloth && $item->getCloth() ) == NULL || ( $idcloth && $item->getCloth() != NULL && $item->getCloth()->getId() == $idcloth ) ) ){
				
			$item->setQuantity( $item->getQuantity() + $quantity );

			return;
				
		}
		
	}
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	
	Basket::getInstance()->addArticle( new CArticle( $idarticle, $idcolor, $idcloth ), $quantity );
	
}

/* ------------------------------------------------------------------------------------------------------------------ */

?>