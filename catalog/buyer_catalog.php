<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Catalogue groupement client (liste personnelle)
 */
 

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/catalbuyerobject.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php" );

include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

class BuyerCatalogController extends PageController{};

$chartep_buyer = new BuyerCatalogController( "buyer_catalog.tpl" );

//$GroupingCatalog = new GroupingCatalog();
//$catalog = new CATALBUYER();

$chartep_buyer->output();

?>