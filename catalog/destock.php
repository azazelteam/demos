<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des destockages en cours
 */
 


include_once('../config/init.php');
include_once('../script/global.fct.php');
include_once( "$GLOBAL_START_PATH/objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CCategoryProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );

class DestockController extends PageController{};
$destocking_template = new DestockController( "destock.tpl" );

$destocking_template->setData( "category", new CCategoryProxy( new CCategory( 0 ) ) );

$lang = "_1";

$ScriptName = "destock.php";

$query1 = "SELECT d.iddestocking, d.rate, d.reference, dc.destocking_cat$lang FROM destocking d, destocking_cat dc WHERE begin_date<=NOW() AND end_date>=NOW() AND d.iddestocking_cat = dc.iddestocking_cat ORDER BY d.lastupdate DESC ";
$rs1 = DBUtil::query( $query1 );

if( $rs1 === false )
	$error_msg = "Impossible de récupérer les destockages actuelles";
else
	$error_msg = "";

$destocking_template->setData( "error_msg", $error_msg );

$descriptions = array();

for( $i = 0 ; $i < 6 ; $i++ ){

	if( Dictionnary::translate( "description_destocking" . ( $i + 1 ) ) != "description_destocking" . ( $i + 1 ) )
		$descriptions[ $i ] = Dictionnary::translate( "description_destocking" . ( $i + 1 ) ) . " ";
	else
		$descriptions[ $i ] = "";

}

$destocking_template->setData( "descriptions", $descriptions );

$promotions = array();

for( $i = 1 ; $i <= $rs1->RecordCount() ; $i++ ){

	$reference = $rs1->fields( "reference" );
	$remise = $rs1->fields( "rate" );
	
	// Récupération des infos
	$query = "SELECT detail.sellingcost,
			product.idcategory,
			detail.summary_1,
			detail.weight,
			detail.reference,
			detail.idproduct,
			detail.idarticle,
			detail.designation_1,
			detail.stock_level,
			delay.delay_1
			FROM detail
			INNER JOIN product ON product.idproduct=detail.idproduct
			INNER JOIN delay ON detail.delivdelay = delay.iddelay
			WHERE detail.reference='$reference' LIMIT 1";
			
	$rs 		 = DBUtil::query( $query );
	$ref 		 = $rs->fields( "reference" );
	$title 		 = $rs->fields( "summary_1" );
	$tempdesign  = explode( "<br />", $rs->fields( "designation_1" ) );
	
	if( count( $tempdesign ) > 1 ){
		
		$promotions[ $i ][ "designation" ] = $tempdesign[ 1 ];
		
			if( count( $tempdesign ) > 2 ){
				$promotions[ $i ][ "designation" ] = "";
				for($u=2 ; $u<count($tempdesign) ; $u++){
					$promotions[ $i ][ "designation" ] .= "<br />" . $tempdesign[ $u ];
				}
			}
	
	}
	else $promotions[ $i ][ "designation" ] = "";
	
	if( count( $tempdesign ) <= 1 ){
		$tempdesign  = explode( "<p>", $rs->fields( "designation_1" ) );
		
		if( count( $tempdesign ) > 1 ){
			
			$promotions[ $i ][ "designation" ] = $tempdesign[ 1 ];
			
			if( count( $tempdesign ) > 2 ){
				$promotions[ $i ][ "designation" ] = "";
				for($u=2 ; $u<count($tempdesign) ; $u++){
					$promotions[ $i ][ "designation" ] .= "<p>" . $tempdesign[ $u ];
				}
			}
		}
		else $promotions[ $i ][ "designation" ] = "";
	}
	
	
	$idcategory	= $rs->fields( "idcategory" );		
	$idproduct	= $rs->fields( "idproduct" );
	$price		= $rs->fields( "sellingcost" );
	$idarticle	= $rs->fields( "idarticle" );
	$delay		= $rs->fields( "delay_1" );
	$stock_level= $rs->fields( "stock_level" );
	$newprice	= number_format( $price * ( (100 - $remise ) / 100 ), 2, ",", "" );
	$tabprice	= explode( ",", $newprice );
	
	$promotions[ $i ][ "tabprice_0" ]	= $tabprice[ 0 ];
	$promotions[ $i ][ "tabprice_1" ]	= $tabprice[ 1 ];
	$promotions[ $i ][ "newprice" ]		= Util::numberFormat($newprice,2);
	$promotions[ $i ][ "remise" ]		= round( $remise );
	$promotions[ $i ][ "product_url" ]	= $productURL = URLFactory::getProductURL( $idproduct, $title );
	//$promotions[ $i ][ "product_url" ]	= URLFactory::getProductURL( $idarticle );
	$promotions[ $i ][ "category_url" ]	= URLFactory::getCategoryURL( $idcategory );
	
	$imageURL = URLFactory::getReferenceImageURI( $idarticle, URLFactory::$IMAGE_ICON_SIZE );

	$promotions[ $i ][ "image" ]		= $imageURL;
	$promotions[ $i ][ "title" ]		= $title;
	$promotions[ $i ][ "ref" ]			= $ref;
	$promotions[ $i ][ "price" ]		= $price;
	$promotions[ $i ][ "delay" ]		= $delay;
	$promotions[ $i ][ "idarticle" ]	= $idarticle;
	$promotions[ $i ][ "reference" ]	= $reference;
	$promotions[ $i ][ "idproduct" ]	= $idproduct;
	
	if($stock_level > 0){
		$promotions[ $i ][ "have_stock" ] = 'ok';
		$promotions[ $i ][ "stock_level" ]	= $stock_level;
	}else{
		$promotions[ $i ][ "have_stock" ] = '';
		$promotions[ $i ][ "stock_level" ]	= "";
	}
	
	if( !isset( $color ) )
		$color = "red";
	
	if( $color == "red" )
		$promotions[ $i ][ "color" ] = "red";
	else
		$promotions[ $i ][ "color" ] = "";

	if( ( $i % 2 ) == 0 ){

		$promotions[ $i ][ "switch" ] = 1;

		if( $color == "yellow" ){

			$color = "red";

		} else {

			$color = "yellow";

		}

	} else {

		$promotions[ $i ][ "switch" ] = "";

	}

	$rs1->MoveNext();

}

$destocking_template->setData( "promotions", $promotions );


$destocking_template->output();

?>
