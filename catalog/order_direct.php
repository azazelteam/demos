<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Ajout direct des références à la commande
 */

include( "./deprecations.php" );

include_once ( dirname( __FILE__ ) . "/../config/init.php");
include_once ( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

class OrderDirectController extends PageController{};
$order_direct = new OrderDirectController( "order_direct.tpl" );

$lang = "_1";
$Db = DBUtil::getConnection();

$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get("catalog_right") : 0;

if (isset ($IdCategory) AND $IdCategory > 0) {
	$bdd = DBUtil::getConnection();
	$query = "SELECT name_1 FROM category WHERE idcategory='$IdCategory'";
	$rs = $bdd->Execute($query);

	$cat_name = $rs->fields("name_1");
	$first = $cat_name {
		0 };
	$voyelles = "aeiouyAEIOUY";
	$isvoy = strpos($voyelles, $first);

	if ($isvoy === false) {
		$appart_catalog = "Le catalogue de " . $cat_name;
		$order_direct->setData('appart_catalog', $appart_catalog);
	} else {
		$appart_catalog = "Le catalogue d'" . $cat_name;
		$order_direct->setData('appart_catalog', $appart_catalog);
	}
} else {
	$order_direct->setData('appart_catalog', "Equipement de manutention et matériel de stockage");
}

	
/* --- Commande express ----- */

$refs_not_found = false;
$list_articles_not_found = array ();
$list_refs_hide_prices = array ();

$hasrefwoprice = false;

$nb_lignes = 10;

$order_direct->setData('nb_lignes', $nb_lignes);

$nb_ref_not_found = 0;

$articles = array ();

for ($i = 0; $i < $nb_lignes; $i++) {

	$articles[$i]["reference"] = "";
	$articles[$i]["quantity"] = "";
	$articles[$i]["i"] = $i;
}

if (isset ($_POST['validated'])) {

	for ($i = 0; $i < $nb_lignes; $i++) {
		$articles[$i]["reference"] = $_POST['ref_' . $i];
		$articles[$i]["quantity"] = $_POST['qtt_' . $i];
	}

	$notrefs = array (); //numéros qui ne sont pas des références
	$refwoprice = array (); //références dont le prix est caché

	$n = 0;

	for ($i = 0; $i < 10; $i++) {

		//si on a une quantite d'indiqué et une référence d'indiqué
		if ($articles[$i]['quantity'] > 0 && trim($articles[$i]['reference']) != '') {

			//vérifier si le prix est dispo (hidecost == 0 )
			$query = "SELECT hidecost FROM detail WHERE reference LIKE '" . $articles[$i]['reference'] . "' LIMIT 1";

			$base = & DBUtil::getConnection();
			$rs = $base->Execute($query);

			if ($rs->RecordCount()) {
				$hidecost = $rs->fields("hidecost");
				if ($hidecost != 0) {
					array_push($list_refs_hide_prices, $articles[$i]['reference']);
					$hasrefwoprice = true;
					
				}
			}
			else{
				$hidecost = 0;				
			}

			//si on n'a pas de prix caché
			if (!$hidecost > 0) {
				// enregistrement de la commande, les références non trouvées vont dans $notrefs
				$n += VerifRef($articles[$i]['quantity'], $articles[$i]['reference']);

				if ($n == 0) { //si on a pas réussi à ajouter au panier
					array_push($list_articles_not_found, $articles[$i]);
					$refs_not_found = true;

				} else { //on a réussit à l'ajouter
					$articles[$i]["reference"] = "";
					$articles[$i]["quantity"] = "";
				}

			}
		}
		
			
	}
$url_redirect = $GLOBAL_START_URL."/catalog/basket.php";
			header( "Location:$url_redirect");
		exit;
}
	
$order_direct->setData('hasrefwoprice', $hasrefwoprice);
$order_direct->setData('list_refs_hide_prices', $list_refs_hide_prices);

$order_direct->setData('refs_not_found', $refs_not_found);
$order_direct->setData('list_articles_not_found', $list_articles_not_found);
$order_direct->setData('nb_refs_not_found', $refs_not_found);

$order_direct->setData('articles', $articles);

$ad_mail = DBUtil::getParameterAdmin("ad_mail");
$order_direct->setData('ad_mail', $ad_mail);

$ad_tel = DBUtil::getParameterAdmin("ad_tel");
$order_direct->setData('ad_tel', $ad_tel);
$order_direct->setData('title', 'Commande directe');

$order_direct->output();



function dobasket($IdRealProduct, $Qtt) {
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	Basket::getInstance()->addArticle( new CArticle( $IdRealProduct ), $Qtt);

}

function VerifRef($qte, $ref) {

	
	$Base = & DBUtil::getConnection();
	$Query = "
												SELECT idarticle,stock_level
												FROM product p,detail d
												WHERE d.reference = '" . trim($ref) . "' 
												AND p.available = 1 
												AND p.idproduct = d.idproduct";

	$rs = $Base->Execute($Query);
	$tcount = $rs->RecordCount();

	if ($tcount == 1) {
		$stock = $rs->Fields("stock_level");
		$IdRealProduct = $rs->Fields("idarticle");

		if (dobasket($IdRealProduct, $qte) == 0)
			return $qte;
	}

	return 0;
}
?>