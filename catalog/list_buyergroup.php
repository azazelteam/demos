<?php 

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Nombre de devis et commande clients pour le groupement
 */

include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");

//Affichage du template
class AccountController extends WebPageController{};
$account_page = new AccountController( "list_buyergroup.tpl" );

$idUser = Session::getInstance()->islogin();

if( $idUser != 0 ) {
	$GroupingCatalog = new GroupingCatalog();
	
	$account_page->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$account_page->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$account_page->setData('issetPostLoginAndNotLogged', "ok");
	
	//Liste des revendeurs
	$list_buyergroup_prospect = getCustomers($GroupingCatalog->getGrouping( "idgrouping" ), "1");
	$list_buyergroup_client = getCustomers($GroupingCatalog->getGrouping( "idgrouping" ), "0");
	
	$account_page->setData('list_buyergroup_prospect', $list_buyergroup_prospect[2]);
	$account_page->setData('list_buyergroup_client', $list_buyergroup_client[2]);
	
	$account_page->setData('nb_estimate_prospect', $list_buyergroup_prospect[0]);
	$account_page->setData('nb_estimate_client', $list_buyergroup_client[0]);
	
	$account_page->setData('nb_order_prospect', $list_buyergroup_prospect[1]);
	$account_page->setData('nb_order_client', $list_buyergroup_client[1]);
	
	$account_page->setData('nb_magasin_prospect', $list_buyergroup_prospect[3]);
	$account_page->setData('nb_magasin_client', $list_buyergroup_client[3]);
	
}
$account_page->output();


//Retourne la liste des membres du groupe &group
function getCustomers($idgrouping, $type){
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT * FROM buyer b, user u, contact cont " .
			"WHERE b.iduser = u.iduser " .
			"AND cont.idbuyer = b.idbuyer " .
			"AND b.idgrouping = '$idgrouping' " .
			"AND b.contact = '$type' " .
			"ORDER BY b.company";
	
	$rs = $db->Execute( $query );
	
	$TabCustomers = array();
	
	$i = 0;
	$sumEstimate = 0;
	$sumOrder = 0;
	while( !$rs->EOF() ){
		$TabCustomers[$i]["idbuyer"] = $rs->fields( "idbuyer" );
		$TabCustomers[$i]["company"] = $rs->fields( "company" );
		$TabCustomers[$i]["zipcode"] = $rs->fields( "zipcode" );
		$TabCustomers[$i]["statut"] = $rs->fields( "contact" ) == "1" ? "Prospect" : "Client";
		$TabCustomers[$i]["nbestimate"] = getEstimateCount($rs->fields( "idbuyer" ));
		$TabCustomers[$i]["nborder"] = getOrderCount($rs->fields( "idbuyer" ));
		$TabCustomers[$i]["name"] = $rs->fields( "lastname"). " " . $rs->fields( "firstname");
		$color = ($i % 2 != 0) ? "F9F9F9" : "FFFFFF";
		$TabCustomers[$i]["color"] = $color;
		
		$sumEstimate += getEstimateCount($rs->fields( "idbuyer" ));
		$sumOrder += getOrderCount($rs->fields( "idbuyer" ));
		
		$i++;
		$rs->MoveNext();
	}
	
	return array($sumEstimate, $sumOrder, $TabCustomers, $i);
	
}

//Retourne le nombre de devis
function getEstimateCount( $idbuyer ){
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT COUNT(*) AS estimateCount FROM estimate";
	
	if($idbuyer) {
		$query .= " WHERE idbuyer = '$idbuyer'";
	}
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nombre de devis pour le client '$idbuyer'" );
		
	return $rs->fields( "estimateCount" );
		
}

//Retourne le nombre de commande
function getOrderCount( $idbuyer ){
	
	$db =& DBUtil::getConnection();
	
	$query = "SELECT COUNT(*) AS orderCount FROM `order`";
	
	if($idbuyer) {
		$query .= " WHERE idbuyer = '$idbuyer'";
	}
	
	$rs = $db->Execute( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		die( "Impossible de récupérer le nombre de cdes pour le client '$idbuyer'" );
		
	return $rs->fields( "orderCount" );
	
}
 
?>