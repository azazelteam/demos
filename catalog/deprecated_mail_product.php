<?php
include_once (dirname(__FILE__) . "/../objects/classes.php");
include_once ("$GLOBAL_START_PATH/objects/mailerobject.php");
include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );
class MailProductController extends WebPageController{};
$mail_product = new MailProductController( "mail_product.tpl" );
$mail_product->setData( "title", Dictionnary::translate("mail_product_title"));
$mail_product->setData( "meta_description",  Dictionnary::translate("mail_product_description") );
//mails
include_once ("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");

$base = & DBUtil::getConnection();
$lang = "_1";

$idproduct = intval($_REQUEST["idproduct"]);

$mail_product->setData("thumbURL", URLFactory::getHostURL() . URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE ) );

//récupérer la miniature et le nom du produit

$lang = "_1";
$query = "SELECT name$lang AS name FROM product WHERE idproduct = '$idproduct' LIMIT 1";

$rs = & DBUtil::query($query);

$productName = $rs->fields("name");

//----------------------------------------------------------------------------------------------

if (!isset ($_REQUEST["idproduct"]))
	die("Paramètre absent");

if (Session::getInstance()->getCustomer()) {

	$idbuyer = Session::getInstance()->getCustomer()->getId();

	$idproduct = intval($_REQUEST["idproduct"]);
	$product = new PRODUCT($idproduct);

	//infos visiteur
	$buyer = array ();

	$buyer["title"] = deprecated_listTitles(Session::getInstance()->getCustomer()->getContact()->getGender());
	$buyer["lastname"] = Session::getInstance()->getCustomer()->getContact()->getLastName();
	$buyer["email"] = Session::getInstance()->getCustomer()->getContact()->getEmail();
	$buyer["phonenumber"] = Session::getInstance()->getCustomer()->getContact()->getPhonenumber();
	$buyer["faxnumber"] = Session::getInstance()->getCustomer()->getContact()->getFaxnumber();
	$buyer["company"] = Session::getInstance()->getCustomer()->getAddress()->getCompany();

} else {

	//infos visiteur
	$buyer = array ();

	$buyer["title"] = deprecated_listTitles();
	$buyer["lastname"] = null;
	$buyer["email"] = null;
	$buyer["phonenumber"] = null;
	$buyer["faxnumber"] = null;
	$buyer["company"] = null;

}
if ( isset($_POST) ) $_POST = Util::arrayMapRecursive('Util::doNothing', $_POST);
foreach ($buyer as $fieldname => $value) {

	if (isset ($_POST[$fieldname]))
		$buyer[$fieldname] = stripslashes($_POST[$fieldname]);

}

$mail_product->setData("buyer", $buyer);

//infos commercial
$iduser = Basket::getInstance()->getSalesman()->getId();


$rs = & DBUtil::query("SELECT iduser, gender, firstname, lastname, phonenumber, faxnumber, email FROM `user` WHERE iduser = '$iduser' LIMIT 1");

$user = & $rs->fields;
$user["company"] = DBUtil::getParameterAdmin("ad_name");

//infos produit
$productName = deprecated_getProductName(intval($_REQUEST["idproduct"]));

//infos locales
$admin = array ();

$admin["ape"] = DBUtil::getParameterAdmin("ad_ape");
$admin["city"] = DBUtil::getParameterAdmin("ad_city");
$admin["siret"] = DBUtil::getParameterAdmin("ad_siret");
$admin["email"] = DBUtil::getParameterAdmin("ad_mail");
$admin["company"] = DBUtil::getParameterAdmin("ad_name");
$admin["address"] = DBUtil::getParameterAdmin("ad_adress");
$admin["zipcode"] = DBUtil::getParameterAdmin("ad_zipcode");
$admin["faxnumber"] = DBUtil::getParameterAdmin("ad_fax");
$admin["tva_intra"] = DBUtil::getParameterAdmin("ad_tva");
$admin["phonenumber"] = DBUtil::getParameterAdmin("ad_phone");

$mail_product->setData('admin_infos', $admin);

//----------------------------------------------------------------------------------------------

$ret = null;
$sent = null;
$display_form = null;

if (isset ($_POST["send"]) || isset ($_POST["send_y"])) {

	$ret = deprecated_sendMail();
	$sent = 1;

}

if ((isset ($_POST["send"]) || isset ($_POST["send_y"])) && !$ret || !(isset ($_POST["send"]) || isset ($_POST["send_y"]))) {

	$display_form = 1;
}
$mail_product->setData("ret", $ret);
$mail_product->setData("sent", $sent);
$mail_product->setData("displayForm", $display_form);
$mail_product->setData("idProduct", $_REQUEST["idproduct"]);
$mail_product->setData("idcat", $_REQUEST["id_cat"]);

//----------------------------------------------------------------------------------------------

$idproduct = intval($_REQUEST["idproduct"]);
$productName = Util::doNothing(deprecated_getProductName($idproduct));

$subject = isset ($_POST["subject"]) ? stripslashes($_POST["subject"]) : "Demande d'information produit concernant '$productName'";

$mail_product->setData("subject", $subject);
$mail_product->setData("productName", $productName);

//----------------------------------------------------------------------------------------------

//Traductions

$translations = array ();

$translations["email"] = Dictionnary::translate("email");
$translations["gender"] = Dictionnary::translate("title");
$translations["company"] = Dictionnary::translate("company");
$translations["lastname"] = Dictionnary::translate("lastname");
$translations["faxnumber"] = Dictionnary::translate("faxnumber");
$translations["pageTitle"] = Dictionnary::translate("mail_product");
$translations["phonenumber"] = Util::doNothing(Dictionnary::translate("phonenumber"));

$mail_product->setData("translations", $translations);

//----------------------------------------------------------------------------------------------

$strdate = humanReadableDate2(date("Y-m-d"));
$mail_product->setData("strdate", $strdate);

//----------------------------------------------------------------------------------------------

$mail_product->output();

//------------------------------------------------------------------------------------------------------------

function deprecated_getCommercialMessage() {

	global  $buyer, $user, $admin, $GLOBAL_START_URL;

	$base = & DBUtil::getConnection();
	$lang = "_1";

	$idproduct = intval($_REQUEST["idproduct"]);

	//récupérer la miniature et le nom du produit

	$lang = "_1";
	$query = "SELECT name$lang AS name
				FROM product
				WHERE idproduct = '$idproduct'
				LIMIT 1";

	$rs = & DBUtil::query($query);

	if ($rs === false)
		die("Impossible de récupérer les informations du produit");

	
	$thumbURL = URLFactory::getHostURL() . URLFactory::getProductImageURI( $idproduct, URLFactory::$IMAGE_ICON_SIZE );
	$productName = $rs->fields("name");

	ob_start();
?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title></title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta http-equiv="Content-Language" content="fr" />
		<link rel="stylesheet" type="text/css" rev="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/catalog/global.css" />
		<link rel="stylesheet" type="text/css" rev="stylesheet" href="<?php echo $GLOBAL_START_URL ?>/css/catalog/mail_product.css" />
	</head>
	<body>
	
		<p class="title" style="width:600px; margin-left:auto; margin-right:auto; text-align:left;">Demande d'information Produit</p>
		<p class="subtitle" style="width:600px; margin-left:auto; margin-right:auto;">Cette demande concerne :</p>	
		<table border="0" cellspacing="0" cellpadding="2" align="center" style="margin-bottom:10px; width:600px;">
			<tr>
				<td valign"top" align="center" width="75">
					<img src="<?php echo $thumbURL ?>" alt="" />
				</td>
				<td valign"top" align="left">
					<?php echo $productName ?>
				</td>
			</tr>
		</table>
		<p style="width:600px; margin-left:auto; margin-right:auto; margin-top:10px;">
			<img src="<?php echo $GLOBAL_START_URL ?>/www/img/barrejaune.gif" height="1" width="600" alt="" />
		</p>
		<p class="subtitle" style="width:600px; margin-left:auto; margin-right:auto; text-align:left;">Coordonnées du visiteur</p>
		<table cellpadding="3" cellspacing="0" border = "1" style="border-collapse:collapse; width:600px;" align="center" class="BorderedTable">
			<tr> 
				<td class="label">
					<?php echo Dictionnary::translate( "title" ) ?>
				</th>
				<td class="value">
					<?php echo $buyer[ "title" ] ?>
				</td>
				<td class="label">
					<?php echo Dictionnary::translate( "lastname" ) ?>&nbsp;<span class="asterix">*</span>
				</td>
				<td class="value">
					<?php echo stripslashes( $buyer[ "lastname" ] )  ?>
				</td>
		    </tr>
		    <tr>		
		    	<td class="label">
		    		<?php echo Dictionnary::translate( "email" ) ?>&nbsp;<span class="asterix">*</span>
				</td>
				<td class="value">
					<a href="mailto:<?php echo $buyer[ "title" ] ?>"><?php echo $buyer[ "email" ] ?></a>
				</td>
				<td class="label">
					<?php echo Dictionnary::translate( "company" ) ?>
				</th>
				<td class="value">
					<?php echo stripslashes( $buyer[ "company" ] ) ?>
				</td>
		    </tr>
		    <tr>
		 		<td class="label">
		 			<?php echo Dictionnary::translate( "phonenumber" ) ?>
		 		</td>		
				<td class="value">
					<?php echo $buyer[ "phonenumber" ] ?>
				</td>
				<td class="label">
					<?php echo Dictionnary::translate( "faxnumber" ) ?>
				</td>
				<td class="value">
					<?php echo  $buyer[ "faxnumber" ] ?>
				</td>
			</tr>
		</table>
		<p class="subtitle" style="width:600px; margin-left:auto; margin-right:auto; text-align:left;">Demande du visiteur</p>
		<table border="1" cellspacing="0" cellpadding="3" align="center" style="width:600px;border-collapse:collapse;" class="BorderedTable">
			<tr>
				<td class="label" style="width:200px;">Sujet: </td>
				<td class="value" style="width:400px;">
					<?php echo stripslashes( $_POST[ "subject" ] ) ?>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="label" style="width:600px;">
					Message :
				</td>
			</tr>
			<tr>
				<td colspan="2" class="value" style="width:600px;">
					<?php echo stripslashes( nl2br( $_POST[ "message" ] ) ) ?>				
				</td>
			</tr>					
		</table>
	</body>
	</html>
	<?php


	$message = ob_get_contents();

	ob_end_clean();

	return $message;

}
//------------------------------------------------------------------------------------------------------------

function deprecated_misEnFormeDuMessage($texte, $nbCar) {

	$lg = strlen($texte);
	$message = '';
	$nb = $nbCar;
	$i = 0;

	while ($lg > $nb) {
		if ($i = 0) {
			$txt = substr($texte, 0, $nb);
		} else {
			$txt = substr($texte, 0 + $nbCar, $nb);
		}
		$pos = strrpos($txt, " ");
		$txt = substr($txt, 0 + $nbCar, $pos);
		$txt .= "<br />";
		$message .= $txt;
		$nb = $nb + $nbCar;
	}

	//Dernier tronçon du message

	return $message;
}
//------------------------------------------------------------------------------------------------------------

function deprecated_sendMail() {

	global	$GLOBAL_START_PATH,
			$buyer,
			$user,
			$admin;
	
	//-----------------------------------------------------------------------------------------

	$idproduct = intval( $_REQUEST[ "idproduct" ] );
	$productName = deprecated_getProductName( $idproduct );
	
	isset( $_POST[ "title" ] ) ? $title = $_POST[ "title" ] : "";
	isset( $_POST[ "lastname" ] ) ? $name = $_POST[ "lastname" ] : "";
	isset( $_POST[ "email" ] ) ? $email = $_POST[ "email" ] : "";
	isset( $_POST[ "company" ] ) ? $company = $_POST[ "company" ] : "";
	isset( $_POST[ "phonenumber" ] ) ? $phone = $_POST[ "phonenumber" ] : "";
	isset( $_POST[ "faxnumber" ] ) ? $fax = $_POST[ "faxnumber" ] : "";
	
	//charger le modèle de mail
	
	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( User::getInstance()->getLang(), 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( "PRODUCT_INFORMATION" );
	
	$editor->setUseProductTags( $idproduct );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerProductInfoTags( $title, $name, $email, $company, $phone, $fax );
	$editor->setUseCommercialTags( Basket::getInstance()->getSalesman()->getId() );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
	
	//envoi du mail au visiteur
	
	$mailer = new htmlMimeMail();
	$mailer->setFrom( $user[ "firstname" ] . " " . $user[ "lastname" ] . "<" . $user[ "email" ] . ">" );
	
	$mailer->setSubject( $Subject );
	$mailer->setHtml( $Message );
	
	$receivers = array ();
	array_push( $receivers, $buyer[ "email" ] );
	$ret = $mailer->send( $receivers );
	
	if( !$ret ){
		
		echo $mailer->get_errors();
		
		return false;
		
	}
	
	//envoi du mail au commercial
	
	$message = deprecated_getCommercialMessage();
	$subject = stripslashes( $_POST[ "subject" ] );
	
	$mailer = new htmlMimeMail();
	
	$mailer->setFrom( $buyer[ "lastname" ] . "<" . $buyer[ "email" ] . ">" );
	$mailer->setSubject( $subject );
	$mailer->setHtml( $message );

	$receivers = array ();
	array_push( $receivers, $user[ "email" ] );
	

	/*var_dump($receivers);
	var_dump($user[ "email" ]);
	exit();
	$rec2 = array(0 => 'contact@.....com');*/
	$ret = $mailer->send( $receivers );

	if( !$ret ){
		
		echo $mailer->get_errors();
		
		return false;
		
	}
	
	return true;
	
}

//------------------------------------------------------------------------------------------------------------

function deprecated_getProductName() {

	

	$idproduct = intval($_REQUEST["idproduct"]);

	$db = & DBUtil::getConnection();
	$lang = "_1";

	$query = "SELECT name$lang FROM product WHERE idproduct = '$idproduct' LIMIT 1";

	$rs = $db->Execute($query);

	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer le nom du produit");

	return $rs->fields("name$lang");

}

//------------------------------------------------------------------------------------------------------------

function deprecated_listTitles($selection = "") {

	

	$html = "";

	$db = & DBUtil::getConnection();
	$lang = "_1";

	$query = "SELECT title$lang FROM title ORDER BY idtitle ASC";

	$rs = $db->Execute($query);

	if ($rs === false || !$rs->RecordCount())
		die("Impossible de récupérer la liste des civilités");

	$html .= "<select name=\"title\">\n";

	while (!$rs->EOF()) {

		$title = $rs->fields("title$lang");
		$selected = $title == $selection ? " selected=\"selected\"" : "";

		$html .= "<option value=\"$title\"$selected>$title</option>\n";

		$rs->MoveNext();

	}

	$html .= "</select>\n";

	return $html;

}

//------------------------------------------------------------------------------------------------------------
?>