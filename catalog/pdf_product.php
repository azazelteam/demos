<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf fiches produits 
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFProduct.php" );

$odf = new ODFProduct( intval( $_REQUEST[ "idproduct" ] ) );
$odf->exportPDF( "fiche produit n° " . intval( $_REQUEST[ "idproduct" ] ) . ".pdf" );

?>