<?php

/** 
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage de la 3ème étape du devis
 * Récapitulatif avant validation finale
*/

include_once('../objects/classes.php');  


// Si l'utilisateur n'est pas identifié
/*if( Session::getInstance()->islogin() <= 0 ) 
{
	// Retour à la première étape (panier)
	header('Location: estimate_basket.php');
}*/

$HTML_title = 'Devis (validation)';
//$HTML_description = 'Site de vente de matériel de manutention';

// Liste des <meta ...> à afficher si définis
$HTML_meta = array(
		'description'	=> $HTML_description,
		'keywords'		=> 'manutention, matériel',
		'author'		=> 'Akilaé',
		'DC.Title'		=> $HTML_title,
		'DC.Type'		=> 'texte',
		'DC.Format'		=> 'text/html',
		'DC.Language'	=> 'fr',
		'DC.Description'=> $HTML_description
		);

//header de la page
include("$GLOBAL_START_PATH/www/head.htm.php");

//template du centre de la page
$maintemplate="$GLOBAL_START_PATH/catalog/include/estimate_order.htm.php";

//barre de gauche à utiliser
$leftcolumn="$GLOBAL_START_PATH/www/barre_g.php";

//barre de droite à utiliser
//$rightcolumn="$GLOBAL_START_PATH/www/barre_d3.php";

//utlilisation du module de rcherche
$searchmodule=true;


///////////////////////////////////////////////////////////////////
/**
 * Début des appels dynamiques (contenu central)
 */
///////////////////////////////////////////////////////////////////

	include_once('drawlift.php');

	$Buyer = &Session::getInstance()->getDeprecatedBuyer() ;
	
	if( isset( $_POST[ "IdDelivery" ] ) )
		$Buyer->SetUseDeliveryAddress( intval( $_POST[ "IdDelivery" ] ) );
		
	if( isset( $_POST[ "IdBilling" ] ) )	
		$Buyer->SetUseBillingAddress( intval( $_POST[ "IdBilling" ] ) );
	
	/**
	 * Enregistrement des adresses de livraison et de facturation
	 * Mise à jour du basket
	 */
	include_once('addresses.inc.php');
		


//page
include("$GLOBAL_START_PATH/www/main.htm.php");

//fermeture de la page
include_once("$GLOBAL_START_PATH/www/footer.php");
?>