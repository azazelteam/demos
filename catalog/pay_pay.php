<?php

include_once( "../objects/classes.php" );

//----------------------------------------------------------------------------

if( !Session::getInstance()->getCustomer()
	|| !DBUtil::query( "SELECT idorder FROM `order` WHERE idorder = '" . intval( $_GET[ "idorder" ] ) . "' AND idbuyer = '" . Session::getInstance()->getCustomer()->getId() . "' LIMIT 1" )->RecordCount() ){
	
	header( "Location: $GLOBAL_START_URL" );
	exit();

}

$order = new Order( intval( $_GET[ "idorder" ] ) );

/* montant impayé */

$netBill = $order->getNetBill();

$rs =& DBUtil::query( "SELECT idbilling_buyer FROM bl_delivery WHERE idorder = '" . $order->getId() . "'" );
if( !$rs->RecordCount() ){ /* paiement intégral */
	
	$gift_token = $order->getGiftToken();
	$gift_token_amount = $gift_token == false ? 0.0 : DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $gift_token->get( "idtoken_model" ) );
	$amount = max( 0.0, round( $order->getNetBill() - $gift_token_amount, 2 ) );
	
}
else{ /* reste à payer */
	
	$amount = 0.0;
	while( !$rs->EOF() ){
		
		$invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		$amount += max( 0.0, $invoice->getBalanceOutstanding() );
		
		$rs->MoveNext();
		
	}
	
}

if( $amount == 0.0 ){
	
	?>
	<p>Votre commande n° <?php echo $order->getId(); ?> a déjà été réglée.</p>
	<p>Si vous n'êtes pas automatiquement redirigé vers <a href="<?php echo $GLOBAL_START_URL; ?>/catalog/account.php"><b>votre compte</b></a>, veuillez cliquer <a href="<?php echo $GLOBAL_START_URL; ?>/catalog/account.php"><b>ici</b></a></p>
	<script type="text/javascript">
	/* <![CDATA[ */
	
		function redirect( url ){ window.location = url; }
		setTimeout( 'redirect( "<?php echo $GLOBAL_START_URL; ?>/catalog/account.php" )', 5000 );
	
	/* ]]> */
	</script>
	<?php
	
	exit();
	
}

$_SESSION[ "idorder_secure" ] = $order->getId();

include( "$GLOBAL_START_PATH/catalog/pay_secure.php" );

//----------------------------------------------------------------------------
	
?>