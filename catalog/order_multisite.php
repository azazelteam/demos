<?php


/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Commandes réalisées à partir de site satellites
 * Si le fichier de configuration /config/multisite.xml existe, alors le site est un site satellite. Dans le cas contraire, il s'agit du site principal
 * La structure du ficher de configuration multisite est la suivante :
 * 
 * 		<?xml version="1.0" encoding="utf-8"?>
 * 			<config>
 * 				<idsource value="19" />
 * 				<host value="nom du site" />
 * 			</config>
 */

include_once( "Validate.php" ); 	//classe PEAR
include_once( "Validate/FR.php" );	//classe PEAR

include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ )  . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ )  . "/../objects/Customer.php" );
include_once( dirname( __FILE__ )  . "/../objects/Order.php" );
include_once( dirname( __FILE__ )  . "/../objects/TradeFactory.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );
include_once( dirname( __FILE__ ) . "/../objects/mailerobject.php" );

/* --------------------------------------------------------------------------------------------------------------- */
/* commande - site principal */

if( isset( $_POST[ "order" ] ) ){

	if( !count( $_POST[ "ref_supplier" ] ) )
		cancel();
		
	User::fakeAuthentification();

	/* protection XSS */
	$_POST = Util::arrayMapRecursive( "stripslashes", 	$_POST );
	$_POST = Util::arrayMapRecursive( "strip_tags", 	$_POST );
	$_POST = Util::arrayMapRecursive( "Util::doNothing", 	$_POST );
	$_POST = Util::arrayMapRecursive( "trim", 			$_POST );
	
	$customer = createCustomer();
	
	if( $customer === false )
		cancel();
		
	$order = createOrder( $customer );
	
	if( $order === false )
		cancel();
		
	mailOrder( $order, $customer );
	
	/* retour sur le site satellite */
	
	$regs = array();
	
	if( preg_match( "/(http:\/\/[^\/]+).*/", $_SERVER[ "HTTP_REFERER" ], $regs ) )
		header( "Location: " . $regs[ 1 ] . "/catalog/order_multisite.php?confirm&idorder=" . $order->getId() );

	exit( '<script type="text/javascript"> history.back(); </script>' );
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/* vérification du n° de siret */

if( isset( $_REQUEST[ "check_siret" ] ) && isset( $_REQUEST[ "siret" ] ) ){
	
	//site satellite
	
	if( $config = getMultisiteConfig() )
		exit( file_get_contents( "http://{$config->host}/catalog/order_multisite.php?check_siret&siret=" . urlencode( stripslashes( $_REQUEST[ "siret" ] ) ) ) );
	
	//site principal
	
	exit( Validate_FR::SIRET( $_REQUEST[ "siret" ] ) ? "1" : "0" );

}

/* --------------------------------------------------------------------------------------------------------------- */
/* identification - site satellite */

if( isset( $_REQUEST[ "identification" ] ) ){

	if( !isset( $_REQUEST[ "login" ] )
		|| !isset( $_REQUEST[ "password" ] ) )
		exit( "0" );

	//site satellite
	
	if( $config = getMultisiteConfig() ){
		
		header( "Cache-Control: no-cache, must-revalidate" );
		header( "Pragma: no-cache" );
		header( "Content-Type: text/xml;charset=utf-8" );
	
		exit( file_get_contents( "http://{$config->host}/catalog/order_multisite.php?identification&login=" . urlencode( stripslashes( $_REQUEST[ "login" ] ) ) . "&password=" . urlencode( stripslashes( $_REQUEST[ "password" ] ) ) ) );
	
	}
	
	//site principal

	$query = "
	SELECT b.idbuyer,
		b.company,
		b.siret, b.naf,
		b.adress AS address, b.adress_2 AS address2, b.zipcode, b.city,
		c.firstname, c.lastname, 
		c.phonenumber, c.faxnumber, c.gsm
	FROM buyer b, user_account ua, contact c
	WHERE ua.login = " . DBUtil::quote( stripslashes( $_REQUEST[ "login" ] ) ) . "
	AND ua.password = MD5( " . DBUtil::quote( stripslashes( $_REQUEST[ "password" ] ) ) . " )
	AND c.idbuyer = ua.idbuyer
	AND c.idcontact = ua.idcontact
	AND b.idbuyer = c.idbuyer
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		exit( "0" );

	$document = new DomDocument( "1.0", "utf-8" );

	$customer = $document->createElement( Util::doNothing( "customer" ) );
	
	foreach( $rs->fields as $key => $value ){
		
		$element = $document->createElement( Util::doNothing( $key ) );
		$element->appendChild( $document->createCDATASection( Util::doNothing( $value ) ) );
		$customer->appendChild( $element );
		
	}

	$document->appendChild( $customer );

	exit( $document->saveXML() );

}

/* --------------------------------------------------------------------------------------------------------------- */
/* mot de passe oublié */

if( isset( $_REQUEST[ "new_password" ] ) && isset( $_REQUEST[ "login" ] ) ){

	//site satellite
	
	if( $config = getMultisiteConfig() )
		exit( file_get_contents( "http://{$config->host}/catalog/order_multisite.php?new_password&login=" . urlencode( stripslashes( $_REQUEST[ "login" ] ) ) ) );
	
	//site principal
	
	$query = "
	SELECT ua.idbuyer, ua.idcontact, c.mail 
	FROM user_account ua, contact c
	WHERE ua.login = " . DBUtil::quote( stripslashes( $_REQUEST[ "login" ] ) ) . " 
	AND c.idbuyer = ua.idbuyer
	AND c.idcontact = ua.idcontact
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( !$rs->RecordCount() )
		exit( "0" );
	
	$password = Util::getRandomPassword();
	
	DBUtil::query( "UPDATE user_account SET password = MD5( " . DBUtil::quote( $password ) . " ) WHERE idbuyer = '" . $rs->fields( "idbuyer" ) . "' AND idcontact = '" . $rs->fields( "idcontact" ) . "' LIMIT 1" );

	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( 1 );
	$editor->setTemplate( MailTemplateEditor::$BUYER_FORGOTTEN_PASSWORD );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $rs->fields( "idbuyer" ), $rs->fields( "idcontact" ) );
	$editor->setUseBuyerPassword( $password );
	$editor->setUseCommercialTags( DBUtil::getParameter( "contact_commercial" ) );

	$mailer = new EVM_Mailer();

	$mailer->set_subject( $editor->unTagSubject() );
	$mailer->set_sender( DBUtil::getParameterAdmin( "ad_sitename" ), DBUtil::getParameterAdmin( "ad_mail" ) );
	$mailer->add_recipient( stripslashes( $_REQUEST[ "login" ] ), $rs->fields( "mail" ) );
	$mailer->set_message( $editor->unTagBody() );
	$mailer->set_message_type( "html" );
	  
	exit( $mailer->send() ? "1" : "0" );
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/* vérifier la disponibilité d'un identifiant pour la création d'un compte */

if( isset( $_REQUEST[ "login_available" ] ) && isset( $_REQUEST[ "login" ] ) ){
	
	//site satellite
	
	if( $config = getMultisiteConfig() )
		exit( file_get_contents( "http://{$config->host}/catalog/order_multisite.php?login_available&login=" . urlencode( stripslashes( $_REQUEST[ "login" ] ) ) ) );
	
	//site principal
	
	if( !Validate::email( stripslashes( $_REQUEST[ "login" ] ) ) )
		exit( "0" );
		
	if( DBUtil::query( "SELECT login FROM user_account WHERE login = " . DBUtil::quote( stripslashes( $_REQUEST[ "login" ] ) ) . " LIMIT 1" )->RecordCount() )
		exit( "0" );
		
	exit( "1" );

}

/* --------------------------------------------------------------------------------------------------------------- */
/* panier vide */

if( !Basket::getInstance()->getItemCount() ){
	
	header( "Location: /catalog/basket.php" );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/* affichage de la page - site satellite uniquement */

if( ( $config = getMultisiteConfig() ) == NULL )
	exit();
	
$controller = new PageController( "order_multisite.tpl" );

$controller->setData( "host", 		$config->host );
$controller->setData( "idsource", 	$config->idsource );
$controller->createElement( "idsource", $config->idsource );

/* confirmation de commande */

if( isset( $_GET[ "confirm" ] ) && isset( $_GET[ "idorder" ] ) ){
	
	Basket::getInstance()->destroy();
	$controller->setData( "idorder", intval( $_GET[ "idorder" ] ) );
	
}

$controller->output();

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Création du prospect - site principal uniquement
 * @return Customer ou false
 */
function createCustomer(){
	
	if( $config = getMultisiteConfig() )
		return false;
	
	if( !isset( $_POST[ "login" ] ) 
		|| !strlen( $_POST[ "login" ] )
		|| !isset( $_POST[ "password" ] ) 
		|| !strlen( preg_replace( "/[^a-zA-Z0-9]+/", "", $_POST[ "password" ] ) ) )
	return false;
		
	//compte existant
	
	$query = "
	SELECT idbuyer, idcontact 
	FROM user_account
	WHERE login = " . DBUtil::quote( $_POST[ "login" ] ) . " 
	AND password = MD5(" . DBUtil::quote( $_POST[ "password" ] ) . ")
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs->RecordCount() )
		return new Customer( $rs->fields( "idbuyer" ), $rs->fields( "idcontact" ) );

	//nouveau compte
	
	$customer = Customer::create();

	$customer->set( "company", 						isset( $_POST[ "company" ] ) ? strtoupper( $_POST[ "company" ] ) : "" );
	$customer->set( "adress", 						isset( $_POST[ "address" ] ) ? $_POST[ "address" ] : "" );
	$customer->set( "adress_2", 					isset( $_POST[ "address2" ] ) ? $_POST[ "address2" ] : "");
	$customer->set( "zipcode", 						isset( $_POST[ "zipcode" ] ) ? preg_replace( "/[^0-9]+/", "", $_POST[ "zipcode" ] ) : "" );
	$customer->set( "city", 						isset( $_POST[ "city" ] ) ? strtoupper( $_POST[ "city" ] ) : "" );
	$customer->set( "siret", 						isset( $_POST[ "siret" ] ) ? preg_replace( "/[^0-9]+/", "", $_POST[ "siret" ] ) : "" );
	$customer->set( "naf", 							isset( $_POST[ "naf" ] ) ? strtoupper( $_POST[ "naf" ] ) : "" );
	$customer->set( "accept_condition", 			isset( $_POST[ "accept_condition" ] ) ? 1 : 0 );
	
	$customer->getContact()->set( "title", 			isset( $_POST[ "company" ] ) ? intval( $_POST[ "title" ] ) : 0 );
	$customer->getContact()->set( "firstname", 		isset( $_POST[ "firstname" ] ) ? ucfirst( $_POST[ "firstname" ] ) : "" );
	$customer->getContact()->set( "lastname", 		isset( $_POST[ "lastname" ] ) ? ucfirst( $_POST[ "lastname" ] ) : "" );
	$customer->getContact()->set( "mail", 			isset( $_POST[ "login" ] ) ? $_POST[ "login" ] : "" );
	$customer->getContact()->set( "phonenumber", 	isset( $_POST[ "phonenumber" ] ) ? $_POST[ "phonenumber" ] : "" );
	$customer->getContact()->set( "faxnumber", 		isset( $_POST[ "faxnumber" ] ) ? $_POST[ "faxnumber" ] : "" );
	
	$customer->createAccount( preg_replace( "/[^a-zA-Z0-9]+/", "", $_POST[ "password" ] ) );
	
	if( isset( $_POST[ "newsletter" ] ) )
		$customer->subscribeNewsletter();
		
	$customer->save();
	
	return $customer;
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Création de la commande - site principal uniquement
 * @param Customer $customer
 * @return Order
 */
function createOrder( Customer $customer ){
	
	if( $config = getMultisiteConfig() )
		return false;
		
	/* création devis */
	
	$order = TradeFactory::createOrder( $customer->getId(), $customer->getContact()->getId() );
	
	$order->set( "status", 		Order::$STATUS_TODO );
	$order->set( "idsource", 	intval( $_POST[ "idsource" ] ) );
	$order->set( "idpayment", 	intval( $_POST[ "idpayment" ] ) );
	$order->set( "comment", 	isset( $_POST[ "comment" ] ) ? $_POST[ "comment" ] : "" );
	
	/* référence unique */
	
	if( !is_array( $_POST[ "ref_supplier" ] ) ) $_POST[ "ref_supplier" ] 	= array( $_POST[ "ref_supplier" ] );
	if( !is_array( $_POST[ "quantity" ] ) ) 	$_POST[ "quantity" ] 		= array( $_POST[ "quantity" ] );
	if( !is_array( $_POST[ "idcolor" ] ) ) 		$_POST[ "idcolor" ] 		= array( $_POST[ "idcolor" ] );
	if( !is_array( $_POST[ "idcloth" ] ) ) 		$_POST[ "idcloth" ] 		= array( $_POST[ "idcloth" ] );
	
	/* ajout références */
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	
	$i = 0;
	while( $i < count( $_POST[ "ref_supplier" ] ) ){

		$idarticle = DBUtil::getDBValue( "idarticle", "detail", "ref_supplier", $_POST[ "ref_supplier" ][ $i ] );
		
		if( $idarticle === false )
			trigger_error( "La référence fournisseur '" . $_POST[ "ref_supplier" ][ $i ] . "' n'existe plus", E_USER_ERROR );
		else $order->addArticle( new CArticle(
		
				$idarticle, 
				isset( $_POST[ "idcolor" ] ) ? intval( $_POST[ "idcolor" ][ $i ] ) : 0,
				isset( $_POST[ "idcloth" ] ) ? intval( $_POST[ "idcloth" ][ $i ] ) : 0
				
			), 
			max( 1, intval( $_POST[ "quantity" ][ $i ] ) )
		
		);
		
		$i++;
		
	}
	
	/* prix du site satellite */
	
	$database = DBUtil::getDBValue( "database", "multisite", "idsource", intval( $_POST[ "idsource" ] ) );
	
	if( !$database ){

		trigger_error( "source inconnu", E_USER_ERROR );
		exit();
		
	}
	
	$it = $order->getItems()->iterator();
	
	while( $it->hasNext() ){
		
		$item =& $it->next();
		
		$query = "
		SELECT sellingcost
		FROM `$database`.detail
		WHERE ref_supplier = '" . $item->get( "ref_supplier" ) . "'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$item->set( "discount_price", 	$rs->fields( "sellingcost" ) );
		$item->set( "unit_price", 		$rs->fields( "sellingcost" ) );
		
	}
	
	/* adresse de livraison */

	if( isset( $_POST[ "delivery_checkbox" ] ) ){
		
		$forwardingAddress = ForwardingAddress::create( $customer->getId() );
		
		$forwardingAddress->setFirstName( $_POST[ "delivery" ][ "Firstname" ] );
		$forwardingAddress->setLastName( $_POST[ "delivery" ][ "Lastname" ] );
		$forwardingAddress->setPhonenumber( $_POST[ "delivery" ][ "Phonenumber" ] );
		$forwardingAddress->setFaxnumber( 	$_POST[ "delivery" ][ "Faxnumber" ] );
		$forwardingAddress->setCompany( $_POST[ "delivery" ][ "Company" ] );
		$forwardingAddress->setAddress( $_POST[ "delivery" ][ "Adress" ] );
		$forwardingAddress->setAddress2( $_POST[ "delivery" ][ "Adress_2" ] );
		$forwardingAddress->setZipcode( $_POST[ "delivery" ][ "Zipcode" ] );
		$forwardingAddress->setCity( $_POST[ "delivery" ][ "City" ] );
		
		$forwardingAddress->save();
		
		$order->setForwardingAddress( $forwardingAddress );
		
	}
	
	
	/* adresse de facturation */
	
	if( isset( $_POST[ "billing_checkbox" ] ) ){

		$invoiceAddress = InvoiceAddress::create( $customer->getId() );
		
		$invoiceAddress->setFirstName(	$_POST[ "billing" ][ "Firstname" ] );
		$invoiceAddress->setLastName( $_POST[ "billing" ][ "Lastname" ] );
		$invoiceAddress->setPhonenumber( $_POST[ "billing" ][ "Phonenumber" ] );
		$invoiceAddress->setFaxnumber(	$_POST[ "billing" ][ "Faxnumber" ] );
		$invoiceAddress->setCompany( $_POST[ "billing" ][ "Company" ] );
		$invoiceAddress->setAddress( $_POST[ "billing" ][ "Adress" ] );
		$invoiceAddress->setAddress2( $_POST[ "billing" ][ "Adress_2" ] );
		$invoiceAddress->setZipcode( $_POST[ "billing" ][ "Zipcode" ] );
		$invoiceAddress->setCity( $_POST[ "billing" ][ "City" ] );
		
		$invoiceAddress->save();
		
		$order->setInvoiceAddress( $invoiceAddress );
		
	}
	
	$order->insure();
	$order->factorize();
	
	$order->save();
	
	return $order;
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Envoi du mail de ocnfirmation - site principal uniquement
 * @param Order $order
 * @param Customer $customer
 */
function mailOrder( Order $order, Customer $customer ){

	if( $config = getMultisiteConfig() )
		return false;
		
	$editor = new MailTemplateEditor();
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( MailTemplateEditor::$ORDER_NOTIFICATION_1 );
	
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseOrderTags( $order->getId() );
	$editor->setUseBuyerTags( $order->get( "idbuyer" ), $order->get( "idcontact" ) );
	$editor->setUseCommercialTags( User::getInstance()->getId() );

	$mailer = new htmlMimeMail();
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . "\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setCc( User::getInstance()->get( "email" ) );
	
	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSubject( $editor->unTagSubject() );

	if( file_exists( dirname( __FILE__ ) . "/../www/cgv.pdf" ) )
		$mailer->addAttachment( file_get_contents( dirname( __FILE__ ) . "/../www/cgv.pdf" ), "cgv.pdf", "application/pdf" );

	$ret = $mailer->send( array( $customer->getContact()->get( "mail" ) ) );

	if( $ret )
		trigger_error( "Impossible d'envoyer la confirmation de commande par courriel", E_USER_ERROR );
		
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Annulation du processus de commande et retour au site satellite - site principal uniquement
 */
function cancel(){
	
	$regs = array();
	
	if( preg_match( "/(http:\/\/[^\/]+).*/", $_SERVER[ "HTTP_REFERER" ], $regs ) )
		header( "Location: " . $regs[ 1 ] . "/catalog/order_multisite.php" );

	exit( '<script type="text/javascript"> history.back(); </script>' );
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * @return stdclass
 * 	$stdclass::idsource 	( idsource du satellite dans la BDD du site principal )
 * 	$stdclass::host 		( site principal )
 */
function getMultisiteConfig(){
	
	static $config = NULL;
	
	if( $config )
		return $config;
		
	$configPath = dirname( __FILE__ ) . "/../config/multisite.xml";

	if( !file_exists( $configPath ) )
		return NULL;
	
	$document= new DOMDocument();
		
	if( !$document->load( $configPath ) ){
		
		trigger_error( "Impossible de lire le fichier de configuration multisite", E_USER_ERROR );
		return NULL;

	}
	
	$config = new stdclass;
	
	$config->idsource 	= $document->getElementsByTagName( "idsource" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
	$config->host 		= $document->getElementsByTagName( "host" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
	
	return $config;
	
}

/* --------------------------------------------------------------------------------------------------------------- */

?>