<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Liste des produits par métier
 * Chargé du traitement des stats pour l'affichage des catégories stars
 * Fonction chargé de retournée les produits les plus visités 
 * Params   : $limit --> noimbre de produits à retourné
 * Params 2 : $month --> le nombre de mois sur lequel s'effectue la recherche des produits les plus visités
 */

include_once("$GLOBAL_START_PATH/objects/classes.php");

function getStarsProducts($limit, $month){
	
	$myYear = date(Y);
	//$myMonth = (date(m) -1) ;
	//$mySecondMonth = $myMonth - 1;
	$myTable = "stat".$myYear.$month;
			
	$db =& DBUtil::getConnection();
	//$query = "SELECT DISTINCT(idproduct) from ".$myTable."where idproduct <> 0 GROUP BY idproduct ORDER BY COUNT(idproduct) DESC LIMIT=".$limit."";
	//$query = "SELECT DISTINCT(P.name_1), P.idproduct, P.description_1 FROM ".$myTable." S, product P WHERE S.idproduct <> 0 AND S.idproduct = P.idproduct GROUP BY S.idproduct ORDER BY COUNT(S.idproduct) DESC LIMIT ".$limit."";
	$query ="SELECT DISTINCT(C.name_1), C.idcategory , C.description_1 FROM ".$myTable." S, category C WHERE S.idcategory<> 0 AND S.idcategory= C.idcategory GROUP BY S.idcategory ORDER BY COUNT(S.idcategory) DESC LIMIT ".$limit."";	
	$rs = $db->Execute($query);
			
	return $rs;
}
?>