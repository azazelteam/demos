<?php 
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage de la 2ème étape du devis
 * Page d'enregistrement de l'acheteur dans le devis
 */

include_once('../objects/classes.php');  

$HTML_title = 'Devis (adresse de livraison)';

//Session::getInstance()->addstat( "estimate_validate", "", 0, 0, 0 );
//Session::getInstance()->UpdateStatUser();

include_once($GLOBAL_START_PATH.'/catalog/drawlift.php');

$error_msg = "";

//Création d'un nouveau compte


//header de la page
include("$GLOBAL_START_PATH/www/head.htm.php");

//template du centre de la page
$maintemplate="$GLOBAL_START_PATH/catalog/include/estimate_buying.htm.php";

//barre de gauche à utiliser
$leftcolumn="$GLOBAL_START_PATH/www/barre_g.php";

//barre de droite à utiliser
//$rightcolumn="$GLOBAL_START_PATH/www/barre_d3.php";

//utlilisation du module de rcherche
$searchmodule=true;

if( isset( $_POST[ "Register" ] ) ){ 
		
	//vérifier le format des données
	
	$_POST[ "phonenumber" ] 	= str_replace( " ", "", $_POST[ "phonenumber" ] );
	$_POST[ "faxnumber" ] 		= str_replace( " ", "", $_POST[ "faxnumber" ] );
	$_POST[ "zipcode" ] 			= str_replace( " ", "", $_POST[ "zipcode" ] );
	
	include_once( "Validate.php" ); //classe PEAR
	
	if( empty( $_POST[ "email" ] ) || !Validate::email( $_POST[ "email" ] ) ) 
		$error_msg = Dictionnary::translate( "pear_validator_email_error" );
	else if( empty( $_POST[ "company" ] ) )
		$error_msg = Dictionnary::translate( "register_company_required" );
	else if( empty( $_POST[ "lastname" ] ) )
		$error_msg = Dictionnary::translate( "register_lastname_required" );
	else if( empty( $_POST[ "city" ] ) )
		$error_msg = Dictionnary::translate( "register_city_required" );
	/*else if( empty( $_POST[ "phonenumber" ] ) || !Validate::string( $_POST[ "phonenumber" ], $options = array( "format" => VALIDATE_NUM ) ) ) 
		$error_msg = Dictionnary::translate( "pear_validator_phonenumber_error" );
	else if( !Validate::string( $_POST[ "faxnumber" ], $options = array( "format" => VALIDATE_NUM ) ) ) 
		$error_msg = Dictionnary::translate( "pear_validator_faxnumber_error" );
	else if( empty( $_POST[ "zipcode" ] ) || !Validate::string( $_POST[ "zipcode" ], $options = array( "format" => VALIDATE_NUM, "min_length" => 5, "max_length" => 5 ) ) ) 
		$error_msg = Dictionnary::translate( "pear_validator_zipcode_error" );*/
			
	$validatePostData = empty( $error_msg );
	
	if( $validatePostData ){ //enregistrer le nouvel acheteur
	
		if( registerUser( $error_msg ) )
			header( "Location: $GLOBAL_START_URL/catalog/estimate_order.php" );
			
	}
	
}
else{ //Acheteur enregistré

	if( Session::getInstance()->islogin() ) 
		header( "Location: $GLOBAL_START_URL/catalog/estimate_order.php" );
	
}
//page
include("$GLOBAL_START_PATH/www/main.htm.php");

//fermeture de la page
include_once("$GLOBAL_START_PATH/www/footer.php");

?>