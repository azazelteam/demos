<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Script d'appel du paiement sécurisé pour le SSL
 */

// * Historique *****************************

// * Modification ***************************


function VerifySSL($SSL)
{
	$Result=0;
	
	if($SSL["cardnumber"]!="" && $SSL["exp_date_card"]!="" && $SSL["name_card"]!="")
		$Result=1;
	return $Result;
}

function DisplaySSLForm($ScriptName,$SSL)
{
	global  $Comment,$Payment, $GLOBAL_START_URL;
	
	echo '<form action="' . $GLOBAL_START_URL  . '/catalog/'. $ScriptName .'" method="post">';
	echo "<table>\n";
	echo "<tr><td>Numero de carte</td><td><input type=text name=SSL[cardnumber] value=\"".$SSL["cardnumber"]."\"></td></tr>\n";
	echo "<tr><td>Date d'expiration</td><td><input type=text name=SSL[exp_date_card] value=\"".$SSL["exp_date_card"]."\"></td></tr>\n";
	echo "<tr><td>Nom</td><td><input type=text name=SSL[name_card] value=\"".$SSL["name_card"]."\"></td></tr>\n";
	echo "</table>\n";
	echo "<input type=hidden name=Payment value=\"$Payment\">\n";
	echo "<input type=hidden name=Comment value=\"$Comment\">\n";
	echo "<input type=submit name=Confirm value=\"Confirmer\">\n";
	echo "</form>\n"; 
}

function UpdateSSL($IdDoc,$SSL)
{
	
	$Base = &DBUtil::getConnection();
	$rs=& DBUtil::query("select idorder from 'order' where idorder='$IdDoc'");
	if($rs->RecordCount())
	{	
		DBUtil::query("update 'order' set cardnumber='".$SSL["cardnumber"]."', exp_date_card='".$SSL["exp_date_card"]."', name_card='".$SSL["name_card"]."' where idorder='$IdDoc'"); 
	}
}

?>