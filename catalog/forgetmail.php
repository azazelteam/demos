<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Oubli du mot de passe par le client : envoi mail 
 */
 
include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once('../script/global.fct.php');

$Email = "";
$message = "";
$send = "";


if( isset( $_POST[ "Email" ] ) ){
	
	$Email = $_POST[ "Email" ];
	
	$idbuyer = getIdBuyer( stripslashes( $_POST[ "Email" ] ) );
	
	if( $idbuyer === false || $idbuyer == ''){
		
		 $message = "<strong>Cette adresse email n'est pas enregistr&eacute;e.<br/>Vous pouvez l'utiliser pour cr&eacute;er un nouveau compte.</strong>";
	
	}else{
		
		if( sendNewPassword( $idbuyer, stripslashes( $_POST[ "Email" ] ) ) ){
			
			$send = 1;
			$message = "<strong>Votre identifiant et votre mot de passe vous ont &eacute;t&eacute; envoy&eacute;s par email.</strong>";
			
		}else{
			
			$message = "<strong>L'envoi de l'email a &eacute;chou&eacute;, veuillez nous contacter au ".DBUtil::getParameterAdmin( "ad_tel" ).".</strong>";
			
		}
		
	}
	
}

echo $message;

//-----------------------------------------------------------------------------------------

function getIdBuyer( $email ){

	/*
	 * @todo : la colonne contact.mail devrait être UNIQUE !!!
	 */

	if( empty( $email ) )
		return false;
		

	$SQL_Query2 = "SELECT idbuyer FROM user_account WHERE login LIKE '" . Util::html_escape( $email ) . "' LIMIT 1";
	$rs2 =& DBUtil::query( $SQL_Query2 );
	
	
	if( $rs2 === false  )
		die( "Impossible de récupérer le propriétaire de l'adresse email '$email'" );
		
	if( !$rs2->RecordCount() )
		return false;
	
	$idbuyer = "";
	
	if( $rs2->RecordCount() > 0 )
		$idbuyer =  $rs2->fields( "idbuyer" );
		
	return $idbuyer;
	
}

//-----------------------------------------------------------------------------------------

function sendNewPassword( $idbuyer, $email ){

	global  $GLOBAL_START_PATH;
		
	//récupérer les infos client
	
	$query = "
	SELECT c.idcontact, ua.login
	FROM contact c, user_account ua
	WHERE c.idbuyer = '$idbuyer'
	AND ua.login LIKE '" . Util::html_escape( $email ) . "'
	AND ua.idbuyer = c.idbuyer
	AND ua.idcontact = c.idcontact
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		return false;
	
	$idcontact 		= $rs->fields( "idcontact" );
	$login 			= $rs->fields( "login" );
	
	$iduser 	= DBUtil::getParameter( "contact_commercial" );
	$password 		= Util::getRandomPassword( 6 );
	
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( "BUYER_FORGOTTEN_PASSWORD" );
	
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $idbuyer, $idcontact );
	$editor->setUseBuyerPassword( $password );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();

	//mettre à jour le mot de passe
	
	$md5 = md5( $password );
	
	$query = "
	UPDATE user_account 
	SET password = '$md5'
	WHERE idbuyer = '$idbuyer'
	AND idcontact = '$idcontact'
	LIMIT 1";

	$rs = & DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de mettre à jour le mot de passe" );

	if( DBUtil::getConnection()->Affected_Rows() != 1 )
		return false;
	
	//envoyer l'email
	
	include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );
	
	$mailer = new EVM_Mailer();

	$mailer->set_subject( $Subject );
	$mailer->set_sender( DBUtil::getParameterAdmin( "ad_sitename" ), DBUtil::getParameterAdmin( "ad_mail" ) );
	$mailer->add_recipient( $login, $email );
	$mailer->set_message( $Message );
	$mailer->set_message_type( "html" );
	  
	return $mailer->send();
  		
}

//-----------------------------------------------------------------------------------------

?>