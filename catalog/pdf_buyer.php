<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf liste des références 
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Encryptor.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFBuyer.php" );

if( isset( $_SESSION['QueryPdfBuyer'] ) ){
	$query = Encryptor::decrypt( $_SESSION['QueryPdfBuyer'] , 'multiSearch' );
	if( isset( $_GET[ 'buyers' ] ) && $_GET[ 'buyers' ] != "" )
		$query = str_replace( "-- BUYERS" , " AND b.idbuyer IN( '" . $_GET[ 'buyers' ] . "') " , $query );

}else
		die( "Un paramètre est manquant !" );

$title = isset( $_GET[ 'title' ] ) ? $_GET[ 'title' ] : '';
		
$odf = new ODFBuyer( $query , $title );
$odf->exportPDF( "Liste des références.pdf" );

?>