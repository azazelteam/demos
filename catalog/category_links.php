<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Génération de l'arborescence des catégories
 */

function category_links($index,$idcat=""){
	
	$lang = "_1";
	
	//Préparation pour afficher toute la liste des sous catagories des categories
	$reqmenuextra=	"SELECT `idcategorychild` as idcategory, name$lang as name
					FROM `category_link`, category
					WHERE idcategorychild=idcategory
					AND `idcategoryparent`=0 AND catalog_right <= 0 ";	
	
	$rsmenuextra =& DBUtil::query($reqmenuextra);
	
	$arraycategories=array();
	$sile=0;
	while(!$rsmenuextra->EOF()){
		
		
		$urlCat=URLFactory::getCategoryURL($rsmenuextra->fields("idcategory"),$rsmenuextra->fields("name") );
		
		$arraycategories[$sile] = array(
		
			"idcategory" 	=> $rsmenuextra->fields("idcategory"),
			"name" 			=> $rsmenuextra->fields("name"),
			"url" 			=> $urlCat
			
		);
	
		$idcatsile=$rsmenuextra->fields("idcategory");
		
		$reqmenuextra2=	"SELECT `idcategorychild` as idcategory, name$lang as name
						FROM `category_link`, category
						WHERE idcategorychild=idcategory
						AND `idcategoryparent`=$idcatsile AND catalog_right <= 0 ";
						
		$rsmenuextra2 =& DBUtil::query($reqmenuextra2);
		
		$arraysscategories=array();
		
		$sile2=0;
		while(!$rsmenuextra2->EOF()){
			$urlCat2=URLFactory::getCategoryURL($rsmenuextra2->fields("idcategory"),$rsmenuextra2->fields("name") );
			$arraysscategories[$sile2] = array(
			
				"idcategory" 	=> $rsmenuextra2->fields("idcategory"),
				"name" 			=> $rsmenuextra2->fields("name"),
				"url" 			=> $urlCat2,
				"end"			=> ""
			);
			
			$idcatsileX = $rsmenuextra2->fields("idcategory");
			
			$reqmenuextraX=	"SELECT `idcategorychild` as idcategory, name$lang as name
							FROM `category_link`, category
							WHERE idcategorychild=idcategory
							AND `idcategoryparent`=$idcatsileX AND catalog_right <= 0 ";
						
			$rsmenuextraX =& DBUtil::query($reqmenuextra2);
			
			$arrayssscategories=array();
			$sileX=0;
			while(!$rsmenuextraX->EOF()){
				$urlCatX=URLFactory::getCategoryURL($rsmenuextraX->fields("idcategory"),$rsmenuextraX->fields("name") );
				$arrayssscategories[$sileX] = array(
				
					"idcategory" 	=> $rsmenuextraX->fields("idcategory"),
					"name" 			=> $rsmenuextraX->fields("name"),
					"url" 			=> $urlCatX,
					"end"			=> ""
				);
				$sileX++;
				$rsmenuextraX->MoveNext();
			}
			$sile2++;
			$rsmenuextra2->MoveNext();
		}
		if($rsmenuextra2->RecordCount())
			$arraysscategories[$sile2-1]["end"] ="ok";
		
		$arraycategories[$sile]["sscat"]=$arraysscategories;
		$sile++;
		$rsmenuextra->MoveNext();
	}
	
	return $arraycategories;
}
?>