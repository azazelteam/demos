<?php

/** 
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Devis réalisés à partir de site satellites
 * Si le fichier de configuration /config/multisite.xml existe, alors le site est un site satellite. Dans le cas contraire, il s'agit du site principal
 * La structure du ficher de configuration multisite est la suivante :
 * 
 * 		<?xml version="1.0" encoding="utf-8"?>
 * 			<config>
 * 				<idsource value="19" />
 * 				<host value="nom du site" />
 * 			</config>
 * 
 * Attention : l'utilisateur MySQL du site principal doit avoir accès aux BDD des sites satellites
 */

include_once( "Validate.php" ); //classe PEAR

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Estimate.php" );
include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );
include_once( dirname( __FILE__ ) . "/../objects/Customer.php" );
include_once( dirname( __FILE__ )  . "/../objects/flexy/PageController.php" );

/* --------------------------------------------------------------------------------------------------------------- */
/* devis - site principal */

if( isset( $_POST[ "estimate" ] ) ){

	if( !count( $_POST[ "ref_supplier" ] ) )
		cancel();
		
	User::fakeAuthentification();

	/* protection XSS */
	$_POST = Util::arrayMapRecursive( "stripslashes", 	$_POST );
	$_POST = Util::arrayMapRecursive( "strip_tags", 	$_POST );
	$_POST = Util::arrayMapRecursive( "Util::doNothing", 	$_POST );
	$_POST = Util::arrayMapRecursive( "trim", 			$_POST );
	
	$customer = createCustomer();	
	$estimate = createEstimate( $customer );
	
	if( $estimate === false )
		cancel();
		
	mailEstimate( $estimate, $customer );
	
	/* retour sur le site satellite */
	
	$regs = array();
	
	if( preg_match( "/(http:\/\/[^\/]+).*/", $_SERVER[ "HTTP_REFERER" ], $regs ) )
		header( "Location: " . $regs[ 1 ] . "/catalog/estimate_multisite.php?confirm&idestimate=" . $estimate->getId() );

	exit( '<script type="text/javascript"> history.back(); </script>' );
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/* panier vide */

if( !Basket::getInstance()->getItemCount() ){
	
	header( "Location: /catalog/basket.php" );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/* affichage de la page */

if( ( $config = getMultisiteConfig() ) == NULL )
	exit();
	
$controller = new PageController( "estimate_multisite.tpl" );

$controller->setData( "host", 		$config->host );
$controller->setData( "idsource", 	$config->idsource );
$controller->createElement( "idsource", $config->idsource );

if( isset( $_GET[ "confirm" ] ) && isset( $_GET[ "idestimate" ] ) ){
	
	Basket::getInstance()->destroy();

	$controller->setData( "idestimate", intval( $_GET[ "idestimate" ] ) );
	
}

$controller->output();

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Création du prospect - site principal uniquement
 * @return Customer ou false
 */
function createCustomer(){
	
	if( $config = getMultisiteConfig() )
		return false;
		
	$rs =& DBUtil::query( "SELECT idbuyer, idcontact FROM contact WHERE mail = " . DBUtil::quote( $_POST[ "email" ] ) . " LIMIT 1" );
	
	if( $rs->RecordCount() )
		return new Customer( $rs->fields( "idbuyer" ), $rs->fields( "idcontact" ) );

	$customer = Customer::create();
	
	$customer->set( "company", 						strtoupper( $_POST[ "company" ] ) );
	$customer->set( "zipcode", 						preg_replace( "/[^0-9]+/", "", $_POST[ "zipcode" ] ) );
	$customer->set( "city", 						strtoupper( $_POST[ "city" ] ) );
	$customer->set( "idsource", 					intval( $_POST[ "idsource" ] ) );
	$customer->getContact()->set( "title", 			intval( $_POST[ "title" ] ) );
	$customer->getContact()->set( "firstname", 		ucfirst( $_POST[ "firstname" ] ) );
	$customer->getContact()->set( "lastname", 		ucfirst( $_POST[ "lastname" ] ) );
	$customer->getContact()->set( "mail", 			$_POST[ "email" ] );
	$customer->getContact()->set( "phonenumber", 	$_POST[ "phonenumber" ] );
	$customer->getContact()->set( "faxnumber", 		$_POST[ "faxnumber" ] );
	
	$customer->save();
	
	return $customer;
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Création du devis - site principal uniquement
 * @param Customer $customer
 * @return Estimate
 */
function createEstimate( Customer $customer ){
	
	if( $config = getMultisiteConfig() )
		return false;
		
	/* création devis */
	
	$estimate =TradeFactory::createEstimate( $customer->getId(), $customer->getContact()->getId() );
	
	$estimate->set( "status", Estimate::$STATUS_TODO );
	$estimate->set( "idsource", intval( $_POST[ "idsource" ] ) );
	$estimate->set( "comment", $_POST[ "message" ] );
	
	/* référence unique */
	
	if( !is_array( $_POST[ "ref_supplier" ] ) ) $_POST[ "ref_supplier" ] 	= array( $_POST[ "ref_supplier" ] );
	if( !is_array( $_POST[ "quantity" ] ) ) 	$_POST[ "quantity" ] 		= array( $_POST[ "quantity" ] );
	if( !is_array( $_POST[ "idcolor" ] ) ) 		$_POST[ "idcolor" ] 		= array( $_POST[ "idcolor" ] );
	if( !is_array( $_POST[ "idcloth" ] ) ) 		$_POST[ "idcloth" ] 		= array( $_POST[ "idcloth" ] );
	
	/* ajout références */

	$i = 0;
	while( $i < count( $_POST[ "ref_supplier" ] ) ){

		$idarticle = DBUtil::getDBValue( "idarticle", "detail", "ref_supplier", $_POST[ "ref_supplier" ][ $i ] );
		
		if( $idarticle === false )
			trigger_error( "La référence fournisseur '" . $_POST[ "ref_supplier" ][ $i ] . "' n'existe plus", E_USER_ERROR );
		else{

			include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
			
			$article = new CArticle( 
			
				$idarticle, 
				isset( $_POST[ "idcolor" ] ) ? intval( $_POST[ "idcolor" ][ $i ] ) : 0,
				isset( $_POST[ "idcloth" ] ) ? intval( $_POST[ "idcloth" ][ $i ] ) : 0
				
			);

			$estimate->addArticle( $article, max( 1, intval( $_POST[ "quantity" ][ $i ] ) ) ); 
		
		}
		
		$i++;
		
	}
	
	/* prix du site satellite */
	
	$database = DBUtil::getDBValue( "database", "multisite", "idsource", intval( $_POST[ "idsource" ] ) );
	
	if( !$database ){

		trigger_error( "source inconnu", E_USER_ERROR );
		exit();
		
	}
	
	$it = $estimate->getItems()->iterator();
	
	while( $it->hasNext() ){
		
		$item =& $it->next();
		
		$query = "
		SELECT sellingcost
		FROM `$database`.detail
		WHERE ref_supplier = '" . $item->get( "ref_supplier" ) . "'
		LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		$item->set( "discount_price", $rs->fields( "sellingcost" ) );
		$item->set( "unit_price", $rs->fields( "sellingcost" ) );
		
	}
	

	$estimate->insure();
	$estimate->save();
	
	return $estimate;
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Envoi du mail de ocnfirmation - site principal uniquement
 * @param Estimate $estimate
 * @param Customer $customer
 */
function mailEstimate( Estimate $estimate, Customer $customer ){

	if( $config = getMultisiteConfig() )
		return false;
		
	$editor = new MailTemplateEditor();
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( MailTemplateEditor::$ESTIMATE_NOTIFICATION );
	
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseEstimateTags( $estimate->getId() );
	$editor->setUseBuyerTags( $estimate->get( "idbuyer" ), $estimate->get( "idcontact" ) );
	$editor->setUseCommercialTags( User::getInstance()->getId() );

	$mailer = new htmlMimeMail();
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . "\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setCc( User::getInstance()->get( "email" ) );
	
	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSubject( $editor->unTagSubject() );

	$ret = $mailer->send( array( $customer->getContact()->get( "mail" ) ) );

	if( $ret )
		trigger_error( "Impossible d'envoyer la confirmation de demande de devis par courriel", E_USER_ERROR );
		
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * Annulation du processus de devis et retour au site satellite - site principal uniquement
 */
function cancel(){
	
	$regs = array();
	
	if( preg_match( "/(http:\/\/[^\/]+).*/", $_SERVER[ "HTTP_REFERER" ], $regs ) )
		header( "Location: " . $regs[ 1 ] . "/catalog/estimate_multisite.php" );

	exit( '<script type="text/javascript"> history.back(); </script>' );
	
}

/* --------------------------------------------------------------------------------------------------------------- */
/**
 * @return stdclass
 * 	$stdclass::idsource 	( idsource du satellite dans la BDD du site principal )
 * 	$stdclass::host 		( site principal )
 */
function getMultisiteConfig(){
	
	static $config = NULL;
	
	if( $config )
		return $config;
		
	$configPath = dirname( __FILE__ ) . "/../config/multisite.xml";

	if( !file_exists( $configPath ) )
		return NULL;
	
	$document= new DOMDocument();
		
	if( !$document->load( $configPath ) ){
		
		trigger_error( "Impossible de lire le fichier de configuration multisite", E_USER_ERROR );
		return NULL;

	}
	
	$config = new stdclass;
	
	$config->idsource 	= $document->getElementsByTagName( "idsource" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
	$config->host 		= $document->getElementsByTagName( "host" )->item(0)->attributes->getNamedItem( "value" )->nodeValue;
	
	return $config;
	
}

/* --------------------------------------------------------------------------------------------------------------- */

?>