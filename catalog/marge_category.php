<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Rpartition des produits / Catégories pour un groupement
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");

header( "Content-Type: text/html; charset=utf-8" );
class GroupingController extends PageController {}
$controller = new GroupingController( "/account/marge_category.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
		
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
	
	
} else {
	$admin_order->setData('isLogin', "");
}


//Géneration automatiques des données a partir des commandes confirmées

	
$username = DBUtil::getDBValue( "login", "user", "iduser", 15 );

		$q1 = "SELECT 
				 o.idbuyer,
				 b.idgrouping,
				 o.conf_order_date,
				 o.idorder,
				 orow.reference,
				 orow.quantity,
				 orow.discount_price,
				 orow.idcategory
			
		
			FROM `order` o
 				 LEFT JOIN `order_row` orow ON o.idorder=orow.idorder
 				 LEFT JOIN buyer b ON b.idbuyer=o.idbuyer 
   			WHERE o.status = 'Ordered' and b.idgrouping != 0";
   			
   		$rs1 = &DBUtil::query( $q1 );
   							 
		while( !$rs1->EOF() )
		{
			
			$idbuyer = $rs1->fields('idbuyer');
			$idgrouping = $rs1->fields('idgrouping');
			
			$date = explode(" ", $rs1->fields('conf_order_date'));
			$conf_order_date = $date[0];
			
			$idorder = $rs1->fields('idorder');
			$reference = $rs1->fields('reference');	
			$quantity = $rs1->fields('quantity');
			$discount_price = $rs1->fields('discount_price');
			$idcategory = $rs1->fields('idcategory');
			
			
			//Vérifications si l'element existe déja dans la table
			$q2 = "SELECT *			
		
			FROM grouping_story

   			WHERE   idbuyer = '$idbuyer'
				and idgrouping = $idgrouping
				and conf_order_date = '$conf_order_date'
				and idorder = '$idorder'
				and reference = '$reference'
				and quantity = $quantity
				and discount_price = $discount_price
				and idcategory = $idcategory   			
   			";
   			
   		$rs2 = &DBUtil::query( $q2 );
			
			if (($idcategory > 0) and ($rs2->RecordCount() == 0 ) ) {
			$qinsert = "INSERT IGNORE INTO grouping_story (
				idbuyer,
				idgrouping,
				conf_order_date,
				idorder,
				reference,
				quantity,
				discount_price,
				idcategory,
				lastupdate,
				username
				 )
				VALUES(
				'$idbuyer',
				$idgrouping,
				'$conf_order_date',
				'$idorder',
				'$reference',
				$quantity,
				$discount_price,
				$idcategory,
				now(),
				'$username'
				)";
			
		$rsinsert =& DBUtil::query( $qinsert );
			}	
						
			$rs1->MoveNext();
			
	        }

//Fin géneration automatiques des données


$result = "";


$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime( 0, 0, 0, 1, 1, date( "Y" ) ) ),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ),
		"idbuyer" 			=> "",
		"grouping" 			=> ""
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
	{
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
	}
?>
		
			
			<!--catalog/marge_category.php-->
			
			<?php
			
			$form = "";
			$form .= '<form action="/catalog/marge_category.php" method="post" id="SearchForm">
				
				<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />
					<div>
						<div class="tableContainer">
							<table class="dataTable" width="100%">';
								
			$form .='<tr>
						<th class="filledCell">Entre le</th>
							<td>
								<input type="text" name="start_date" id="start_date" class="calendarInput" value='. $postData[ "start_date" ] .' />';
								
			$form .= '<a href="javascript:calendar(start_date)" id="test_a" ></a>';
					$displayIcon = true; $calledFunction = false;
									
			$form .= '</td>';
							
			$form .='<th class="filledCell">Et le</th>
					<td>
						<input type="text" name="end_date" id="end_date" class="calendarInput" value='. $postData[ "end_date" ] .' />';
						
			$form .= '<a href="javascript:calendar(end_date)" id="test_b" ></a>';
								
			$form .= '</table>
					</div>
					<div class="submitButtonContainer" style="float:right;padding-right:200px;margin-bottom:10px;">
						<input type="submit" class="blueButton"  id="idRechercher" value="Rechercher" />
					</div>
				</div>
				<input type="hidden" name="detail" id="detail" value="1" />
			</form>';

			$controller->setData( "form",$form );


//***************************************************************************************//
//***********************************Calcul de la marge**********************************//
//***************************************************************************************//

function calcmarge($idparentcat,$idtopcat,$arrCA,&$CAqtty, $datesearch){
	
	global $GLOBAL_START_PATH, $searchDateParam;
	$db = &DBUtil::getConnection();
	global $GLOBAL_START_PATH;
	
	if(isset($_POST["start_date"]))
		$start = $_POST["start_date"];
	else
		$start = "01-01-2014";
		
		$start = explode("-", $start);
		
		$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
		
		if(isset($_POST["end_date"]))
		$end = $_POST["end_date"];
	else
		$end ="31-12-2014";
		
		$end = explode("-", $end);
		
		$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
		
	$statStartDate = $start_date;
	$statEndDate = $end_date;
	
	include_once($GLOBAL_START_PATH.'/statistics/objects/getfunctions.inc.php');
	$datesearch		= Date("Y");
	
 	$paramDate = " ( grouping_story.conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' ) ";
	
	$queryRecursif = "SELECT idcategorychild FROM category_link WHERE category_link.idcategoryparent = '$idtopcat' ";
	$rsRecc	= $db->Execute( $queryRecursif );
	
	$where = "";
	$GroupingCatalog = new GroupingCatalog();
		
		if  ($GroupingCatalog->getGrouping( "idgrouping" ))
		{
		
		$where = " and idgrouping = ". $GroupingCatalog->getGrouping( "idgrouping" );
		}
	

	
	
	
	
	$queryDP = "SELECT grouping_story.idcategory, conf_order_date AS DateH, " .
			   " grouping_story.discount_price , grouping_story.quantity FROM grouping_story" .
			   " INNER JOIN `category` ON category.idcategory=grouping_story.idcategory" .
			   " LEFT JOIN `category_link` ON category_link.idcategorychild = category.idcategory " .
			   " WHERE category.idcategory='$idtopcat' AND $paramDate $where " .
			   " ORDER BY conf_order_date ";
   	// var_dump($queryDP);
	$rsDP = & DBUtil::query( $queryDP );
	$nDP= $rsDP->RecordCount();
	
	for($iDP=0;$iDP<$nDP;$iDP++){
		$arrCA += ( $rsDP->Fields('discount_price')*$rsDP->Fields('quantity'));
		$CAqtty += $rsDP->Fields('quantity');
		$rsDP->Movenext();
	}
	
	$nRecc= $rsRecc->RecordCount();
		for($i=0;$i<$nRecc;$i++){
			$arrCA += calcmarge($idparentcat, $rsRecc->Fields('idcategorychild') ,0, $CAqtty, $datesearch);
			$rsRecc->MoveNext();	
		}
	
	
	return $arrCA;
	
}


//-----------------------------------------------------------------------------------------------
// Calcul au niveau demandé
//-----------------------------------------------------------------------------------------------
?>



	

	<?php



 include_once($GLOBAL_START_PATH . '/catalog/camembert.php');
 include_once($GLOBAL_START_PATH . '/statistics/objects/init_arr_stat.php');
 include_once($GLOBAL_START_PATH . '/statistics/objects/tableStats.php');
 if(isset($_GET['idcategory'])){ $idcatsort = $_GET['idcategory']; } else {  $idcatsort = 0; }
 $idcategoryParent = DBUtil::query( "SELECT idcategoryparent FROM category_link WHERE idcategorychild = '$idcatsort' LIMIT 1" )->fields( "idcategoryparent" );
 $datesearch		= Date("Y");
 $arrCA  = array(); 
 $arrCO  = array(); 
 $arrM   = array(); 
 $arrVCA = array(); 
 $arrVCO = array();	
 $arrVM  = array();
 $lang   = "_1";
	
 //Liste des categories au premier niveau
 $queryDP = "SELECT name$lang AS name, idcategorychild FROM category_link " .
		    "LEFT JOIN `category` ON category_link.idcategorychild = category.idcategory " .
 			"WHERE category_link.idcategoryparent = '$idcatsort' "; 

 $rsDP = & DBUtil::query( $queryDP );
		$nDP= $rsDP->RecordCount();
		$valDP = 0; $CAqtty = array(); $COqtty = array();
		for($iDP=0;$iDP<$nDP;$iDP++){
			$CAqtty[$rsDP->Fields('name')]=0;
			$COqtty[$rsDP->Fields('name')]=0;
			$arrCA[$rsDP->Fields('name')]=calcmarge($rsDP->Fields('idcategorychild'),$rsDP->Fields('idcategorychild'),0,$CAqtty[$rsDP->Fields('name')],$datesearch);
			//$arrCO[$rsDP->Fields('name')]=calcCoutHT($rsDP->Fields('idcategorychild'),$rsDP->Fields('idcategorychild'),0,$COqtty[$rsDP->Fields('name')],$datesearch, $datesize);
			$rsDP->Movenext();
		}

	//produits de destockage et spécifiques
	
	if( !$idcatsort ){
		
		$name = Dictionnary::translate( "stat_destocking_n_specific" );

		$CAqtty[ $name ] 	= 0;
		$COqtty[ $name ] 	= 0;
		
		
	}
	$chaine = "#FF6A2D,#FFFF66,#90EE90,#99CCFF,#FF99FF,#FF9933,#F6E433,#CAFF70,#66FFFF,#E066FF,#FF4040,#EEC900,#E4F192,#54FF9F,#AB82FF,#FF5536,#9999FF,#FFE4B5,#66FF66,#FF7F24";
	$camcolor   = $histocolor = preg_split("/,?#/",$chaine,-1,PREG_SPLIT_NO_EMPTY);	
	// Affichage du diagamme ----
	$i = 0;

	foreach($arrCA as $x1 => $y1){	
		$i++; 
		$arrVCA[] = array($arrCA[$x1],  
					      $camcolor[$i%20],  
						  $x1, 
						  $CAqtty[$x1]); 	
	}
	
		
	rsort($arrVCA);
	
	
	
	
	if( $idcatsort ){
		
		
		$result .='<p style="text-align:right; margin:10px; font-size:12px;">
			<a href="marge_category.php" class="blueLink">Accès catégorie mère</a>
		</p>';
		

	} 
	
	if($nDP!=0){
	//Camembert(tabDonnées, chemin_de_l'image_temporaire, fontsize, hauteurIMG, largeurIMG, Gd diametre, hauteurellipseTxt, largeurEllipseTxt')
	$camCA = new Camembert($arrVCA,"temp/".Date("YmdHms")."-CamembertCA","graphCA","pieChart",1,250,575,130,112,158);
	$camCA->hideText();
	$camCA->Show(); //Affichage du Camembert
	
	$text = array(Util::doNothing("Prix remisé HT ")."(&euro;)",Util::doNothing("Qté"));
	
	$result .= '<div align="center"><img src="temp/'. Date("YmdHms") .'-CamembertCA.png" border="0" alt=""/></div>';
	//$result .= '<br><br>';
	
	//Affichage du tableau
	
	
	if(isset($_POST["start_date"]))
		$start = $_POST["start_date"];
	else
		$start = "01-01-2014";
		
		$start = explode("-", $start);
		
		$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
		
		if(isset($_POST["end_date"]))
		$end = $_POST["end_date"];
	else
		$end ="31-12-2014";
		
		$end = explode("-", $end);
		
		$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
		
	$statStartDate = $start_date;
	$statEndDate = $end_date;
	
	
	$GroupingCatalog = new GroupingCatalog();
		
		if  ($GroupingCatalog->getGrouping( "idgrouping" ))
		{
		
		$grouping = $GroupingCatalog->getGrouping( "idgrouping" );
		}
	
	
	$_SESSION["start"] = $statStartDate;
			$_SESSION["end"] = $statEndDate;
			$_SESSION["grouping"] = $grouping;
	
	
	$arr = $arrVCA;
	$width = 550;
	$numeric=1;
	$graphname= Date("YmdHms")."-CamembertCA";
	$showTotal=1;
	$idhelp="turnover2";
	
	include_once($GLOBAL_START_PATH . '/statistics/objects/statsFunctions.php');
	
	global $statPageTitle, $GLOBAL_START_URL, $camcolor;
	$lang = "_1"; 
	$pieMinPercent = DBUtil::getParameterAdmin("pie_min_percent");
	$idarrurl = str_replace(" ","",substr(microtime(),-19));
	$exportArray[0][0]='Intitulé';
	$result .= '<script language="javascript">function OuvrirPopup(page,nom,option) { window.open(page,nom,option); }</script>';
	$result .= '<div align="center"><table class="resultTable" style="text-align: left; width: '.$width.'px; border: 1px solid black;" cellspacing="0">'."\n";
	$result .= '<tr style="background-color: #cccccc; color: black; text-align: center; font-weight: bold; "><th colspan="2">Intitulé</th>';
	for($n=0;$n<count($text);$n++){ 
		$result .= '<th>'.Util::doNothing($text[$n]).'&nbsp;</th>';
		$exportArray[0][$n+1]=$text[$n];
	}
	$result .= '</tr>'."\n";
	
	// echo "arr[1]"; var_dump($arr);echo " arr[2]: ";
	// Affichage du tableau --
	$linecolor="#e4e4e4";
	$sum = 0;
	for($n=0;$n<count($arr);$n++){
		$sum += $arr[$n][0];
	}
	$othersColorIndice = 0;
	for($n=0;$n<count($arr)-1;$n++){
		if($arr[$n][0]/$sum > $pieMinPercent)	{
			$arr[$n][1] = $camcolor[$n%20];
		} else if($othersColorIndice == 0) {
			$othersColorIndice = $n%20;
			$arr[$n][1] = $camcolor[$othersColorIndice]; 
		} else {
			$arr[$n][1] = $camcolor[$othersColorIndice]; 
		}
		
	} 
	$total = 0;
	for($i=0;$i<count($arr);$i++){
		if($arr[$i][0]!=0)
		{
			if($linecolor=="#EEEEEE"){ $linecolor="#e4e4e4"; } else { $linecolor="#EEEEEE"; }
			if($numeric==1){ $val = number_format($arr[$i][0], 2, '.', ' '); } else { $val = $arr[$i][0]; }
			if($sum!=0){ $percent = number_format(($arr[$i][0])*100/$sum, 2, '.', ' '); } else { $percent = 0; }
			$t = convertHexaEnRVB($arr[$i][1]);
			if($numeric==1){ $total = number_format($sum, 2, '.', ' '); } else { $total = $sum; }
			
			//Mise en forme export
			$exportArray[$i+1]=array($arr[$i][2],$val);
			
			//Affichage
			$result .= '<tr><td align="center" style="border-right: 2px solid white; width: 30px; background: '.$linecolor.';">'.$percent.'%  </td>'."\n".
				 '<td style="border-right: 2px solid white; background: '.$linecolor.';">&nbsp;&nbsp;' ;
			
			$idcategory = SearchIdCat($arr[$i][2]);
			
			if( $idcategory && !isLastCat( $idcategory ) )	
				$result .= '<a href="?&amp;idcategory='.SearchIdCat($arr[$i][2]).'&categoryname='.Util::doNothing($arr[$i][2]).'">';
			//else echo "<a href=\"statistics.php?cat=margeproduct&amp;idcategory=" . SearchIdCat( $arr[ $i ][ 2 ] ) . "\">";
				
			$result .= Util::doNothing($arr[$i][2]);
			

			
			$result .= '</a><a href="'.$GLOBAL_START_URL.'/catalog/detail_marge.php?&amp;idcategory='.$idcategory.'&namecateg='.Util::doNothing($arr[$i][2]).'"><img src="'.$GLOBAL_START_URL.'/images/show.gif"></a>
			
			
			</td>';
		 //<img src="'.$GLOBAL_START_URL.'/images/show.gif">
			$result .= 	'<td style="border-right: 2px solid white; text-align: right; padding-right: 15px; background: '.$linecolor.';" nowrap>' .
					'<b>'.$val.'</b></td>'."\n";

			for($n=3;$n<count($arr[$i]);$n++){ 
					$result .= '<td style="border-right: 2px solid white; text-align: center; background: '.$linecolor.';"><b>'.$arr[$i][$n].'</b></td>'."\n";
					$exportArray[$i+1][]=$arr[$i][$n];
			}
			$result .= '</tr>'."\n";
		}	
	}
	
	
	$arraycat = array();
	// var_dump($arr);echo " arraycat: ";
	for($n=0;$n<count($arr);$n++){
		if($arr[$n][0] > 0)
		{
		$qr = "SELECT idcategory from category where name_1 = '".addslashes($arr[$n][2])."'";
		$rs = & DBUtil::query( $qr );
	
		$idcateg = $rs->fields("idcategory");
				
		array_push($arraycat, $idcateg); 
		}
	}
	// var_dump($arraycat);

	
	$where = "";
	$GroupingCatalog = new GroupingCatalog();
		
		if  ($GroupingCatalog->getGrouping( "idgrouping" ))
		{
		
		$grouping = $GroupingCatalog->getGrouping( "idgrouping" );
		
			
		$where = " and gs.idgrouping = ". $grouping;
		}
	
		
	$export = array();
	
	$arraytitle = array("N. client","Ehpad","N. cde","Date cde","N. categorie","Categorie","Reference","Designation","Qte","Prix HT");

$export[] = $arraytitle;
	
	
	$statStartDate = $_SESSION["start"];
$statEndDate = $_SESSION["end"];


$lastcateg = array();
for($i=0;$i<count($arraycat);$i++)
{
//print $arraycat[$i];

	$idcat = $arraycat[$i];

	$q3 = "SELECT `idcategorychild`
			FROM `category_link`
			WHERE `idcategoryparent` =$idcat";

	$rs3 = & DBUtil::query( $q3 );
	
	if ($rs3->RecordCount()==0)
		{
		array_push($lastcateg, $idcat);
		}
	
	else{
	while( !$rs3->EOF )
	{
		$idcat = $rs3->fields("idcategorychild");
		
		$q4 = "SELECT `idcategorychild`
			FROM `category_link`
			WHERE `idcategoryparent` =$idcat";
		$rs4 = & DBUtil::query( $q4 );
		
		//print $idcat."/". $rs4->RecordCount();
		///print "<br>";
		
		
		if ($rs4->RecordCount()==0)
		{
		array_push($lastcateg, $idcat);
		}
		else
		{
		$q5 = "SELECT `idcategorychild`
			FROM `category_link`
			WHERE `idcategoryparent` =$idcat";
		$rs5 = & DBUtil::query( $q5 );
		while( !$rs5->EOF )
		{
		$idcategg = $rs5->fields("idcategorychild");
		array_push($lastcateg, $idcategg);
		$rs5->MoveNext();	
		}
		}
		
							
		$rs3->MoveNext();
	}	 	
	}
	

}





for($i=0;$i<count($lastcateg);$i++)
{

	$idcat = $lastcateg[$i];	

	$q4= " SELECT gs.idbuyer,
				b.company,
				gs.idorder,
				gs.conf_order_date,	 
				gs.idcategory,
				gs.quantity,
				c.name_1,
				gs.reference,	
				d.summary_1,	 
				gs.discount_price
		
		FROM    grouping_story gs,
				buyer b,
				category c,
				detail d
					
		WHERE 	gs.idcategory = $idcat
				AND	b.idbuyer = gs.idbuyer
				AND c.idcategory = gs.idcategory
				AND d.reference = gs.reference 
				AND (gs.conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' )
				".$where ."
				ORDER  BY gs.conf_order_date ASC
				
				
				";
		$rs4 = & DBUtil::query( $q4 );
				
		while( !$rs4->EOF )
			{
			//print $rs4->fields("idcategory");	
							
				$arraydata = array( $rs4->fields("idbuyer"),
				 					(mb_detect_encoding($rs4->fields("company"),mb_detect_order(),true) == 'UTF-8') ? Util::doNothing($rs4->fields("company")) : $rs4->fields("company"),
				 					$rs4->fields("idorder"),
				 					$rs4->fields("conf_order_date"),
				 					$rs4->fields("idcategory"),
				 					$rs4->fields("name_1"),
				 					$rs4->fields("reference"),
									$rs4->fields("summary_1"),
									$rs4->fields("quantity"),
									($rs4->fields("discount_price")*$rs4->fields("quantity"))
									);
									
				 $export[] = $arraydata;
				
				$rs4->MoveNext();
			}	 






}

		$result .= '<tr style="text-align: center; height: 15px;"><th colspan="2">&nbsp;</th><th style="text-align: right; padding-right: 15px; border-top: 1px solid gray;"><b>'.$total.'</b></th><th style="border-top: 1px solid gray;"></th></tr>'."\n";
		$result .= '<tr><th colspan="10" style="padding: 5px; text-align: center; ">'."\n".
		/*'<a style="text-decoration: none;" href="#" onClick="document.forms.Stat_exportxls_'.$idarrurl.'.submit();"><img src="'.$GLOBAL_START_URL.'/statistics/img/export_xls.gif" border=0 align="top"> XLS</a> - ' ."\n".*/
		'<a style="text-decoration: none;" href="#" onClick="document.forms.Stat_exportcsv_'.$idarrurl.'.submit();"><img src="'.$GLOBAL_START_URL.'/statistics/img/export_csv.gif" border=0 align="top"> Donnees CSV</a> - ' ."\n".
		'<a style="text-decoration: none;" href="#" onClick="window.print();"><img src="'.$GLOBAL_START_URL.'/statistics/img/impr.gif" height="18" border=0 align="top"> '.Dictionnary::translate('stat_print').'</a> - ' ."\n".
		'<a style="text-decoration: none;" href="#" onClick="javascript:OuvrirPopup(\'help.php?id='.$idhelp.'\', \'help\', \'top=\'+((screen.height/2)-250)+\',left=\'+((screen.width/2)-350)+\',resizable=yes, location=no, width=700, height=500, menubar=no, status=no, scrollbars=yes, menubar=no\'); return false;"><img src="'.$GLOBAL_START_URL.'/statistics/img/help.gif" height="18" border=0 align="top"></a></th></tr>'."\n";
	$result .= '</table></div><br />'."\n"."\n";
	
	$arrurl = base64_encode(serialize($export));
	$result .= '<form method="post" id="Stat_export'.$idarrurl.'" name="Stat_exportcsv_'.$idarrurl.'" action="'.$GLOBAL_START_URL.'/statistics/export_csv.php?type=cam"><input type="hidden" name="Stat_export_csv" value="'.$arrurl.'" /></form>'."\n";
	$result .= '<form method="post" id="Stat_export'.$idarrurl.'" name="Stat_exportxls_'.$idarrurl.'" action="'.$GLOBAL_START_URL.'/statistics/export_xls.php?type=cam&title='.stripslashes($statPageTitle).'&showtotal='.$showTotal.'&name='.$graphname.'"><input type="hidden" name="Stat_export_xls" value="'.$arrurl.'" /></form>'."\n";
	
	
	
	} else
 	{ $result .= '<br /><br />'.$ERROR_MSG; } 







$controller->setData('result', $result);



/* On ferme l'envoi de données */
$controller->output();

?>
	<script type="text/javascript">



		/* <![CDATA[ */
		function calendar(param){
		$(document).ready(function() { 
		
			$(param).datepicker({
				buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
				buttonImageOnly: true,
				changeMonth: true,
				changeYear: true,
				dateFormat: 'dd-mm-yy',
				showAnim: 'fadeIn',
				showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
				onClose: function(){
					<?php echo $calledFunction ?>( this );
				}
				<?php } ?>
			});
							
		});
		}
		/* ]]> */
		</script>
<script type="text/javascript">
$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_a').click();
});

$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_b').click();
});

</script>