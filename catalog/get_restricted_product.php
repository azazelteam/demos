<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ...?
 */
 
//------------------------------------------------------------------------

include_once( "../objects/classes.php" );

$idbuyer = Session::getInstance()->getIdBuyer();
$useRestrictions = isset( $_GET[ "rexel" ] ) && $_GET[ "rexel" ] == md5( "rexel" ) ? false : true;

if( $useRestrictions && (
		!$idbuyer 
		|| !isset( $_GET[ "idrestricted_product" ] )
		|| !intval( $_GET[ "idrestricted_product" ] )
		|| !isset( $_GET[ "file" ] ) 
		|| !allowRestrictedProduct( $idbuyer, intval( $_GET[ "idrestricted_product" ] ) )
	) )
	header( "Location; $GLOBAL_START_URL" );
else 	getRestrictedProduct( intval( $_GET[ "idrestricted_product" ] ), $_GET[ "file" ] );

//------------------------------------------------------------------------

function allowRestrictedProduct( $idbuyer, $idrestricted_product ){

	$query = "
	SELECT restriction_fieldname, restriction_value
	FROM restricted_products
	WHERE idrestricted_product = '$idrestricted_product'
	LIMIT 1";

	$rs =& DBUtil::query( $query );	
	
	if( $rs === false || !$rs->RecordCount() )
		return false;
	
	$restriction_fieldname 	= $rs->fields( "restriction_fieldname" );
	$restriction_value 		= $rs->fields( "restriction_value" );
	
	$query = "
	SELECT COUNT(*) AS granted
	FROM buyer
	WHERE idbuyer = '$idbuyer'
	AND `$restriction_fieldname` = '" . Util::html_escape( $restriction_value ) . "'";
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		return false;
		
	return $rs->fields( "granted" ) == 1;
	
}

//------------------------------------------------------------------------

function getRestrictedProduct( $idrestricted_product, $file ){
	
	global $GLOBAL_START_PATH;
	
	$allowedFiles = array( "image", "sheet", "plan" );
	
	if( !in_array( $file, $allowedFiles ) )
		return;
	
	$rs =& DBUtil::query( "SELECT `$file` AS `file` FROM restricted_products WHERE idrestricted_product = '$idrestricted_product' LIMIT 1" );
	
	if( !$rs->RecordCount() )
		return;
		
	$file = $rs->fields( "file" );
	$path = "$GLOBAL_START_PATH/www/restricted_products/$file";
	
	if( !file_exists( $path ) )
		return;
	
	header( "Content-Type: " . getMimeType( $path ) );
	header( "Content-Disposition: inline; filename=$file" );
	//header( "Content-Transfer-Encoding: binary" );

	readfile( $path );
	
}

//------------------------------------------------------------------------

function getMimeType( $path ){
	
	if( !file_exists( $path ) )
		return "application/octet-stream";

    /*
    doesn't work properly!!! :o(((((((((
    $fi = new finfo(FILEINFO_MIME);
    $mime_type = $fi->buffer(file_get_contents($path));
    */

    /*
    doesn't work properly!!! :o(((((((((
    return shell_exec( "file -ib $path" );
    */

    $pos = strrpos( $path, "." );

    if( $pos === false )
		return "application/octet-stream";

    $file_ext = substr( $path, $pos + 1 );

    if( !strlen( $file_ext ) )
		return "application/octet-stream";
	
	$mime_types = array(

		"bz2" 	=> "application/x-bzip",
		"gz" 	=> "application/x-gzip",
		"tar" 	=> "application/x-tar",
		"zip" 	=> "application/zip",
		"asf" 	=> "video/x-ms-asf",
		"asx" 	=> "video/x-ms-asf",
		"avi" 	=> "video/avi",
		"mpg" 	=> "video/mpeg",
		"mpeg" 	=> "video/mpeg",
		"wmv" 	=> "video/x-ms-wmv",
		"wmx" 	=> "video/x-ms-wmx",
		"xml" 	=> "text/xml",
		"xsl" 	=> "text/xsl",
		"doc" 	=> "application/msword",
		"rtf" 	=> "application/msword",
		"xls" 	=> "application/excel",
		"pps" 	=> "application/vnd.ms-powerpoint",
		"ppt" 	=> "application/vnd.ms-powerpoint",
		"pdf" 	=> "application/pdf",
		"ai" 	=> "application/postscript",
		"eps" 	=> "application/postscript",
		"psd" 	=> "image/psd",
		"swf" 	=> "application/x-shockwave-flash",
		"rm" 	=> "application/vnd.rn-realmedia",
		"rv" 	=> "video/vnd.rn-realvideo",
		"exe" 	=> "application/x-msdownload",
		"pls" 	=> "audio/scpls",
		"m3u" 	=> "audio/x-mpegurl",
		"png" 	=> "image/png",
		"gif" 	=> "image/gif",
		"jpg"	=> "image/jpeg",
		"jpeg" 	=> "image/jpeg",
		"bmp" 	=> "image/bmp",
		"tif" 	=> "image/tiff",
		"txt" 	=> "text/plain",
		"htm" 	=> "text/html",
		"html" 	=> "text/html",
		"css" 	=> "text/css"
		
	);
	
	foreach( $mime_types as $ext => $mime_type )
		if( strtolower( $file_ext ) == $ext )
			return $mime_type;
			
	return "application/octet-stream";

}

//------------------------------------------------------------------------

?>