<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de l'odf facture
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );

$odf = new ODFInvoice( intval( $_REQUEST[ "idinvoice" ] ) );
$odf->exportODT( "Facture n° " . intval( $_REQUEST[ "idinvoice" ] ) . ".odt" );

?>