<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Gestion des bons de réduction (ancienne version)
 */

include( "./deprecations.php" );

include_once( dirname( __FILE__ ) . "/../config/init.php" );  
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/GiftToken.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );

//----------------------------------------------------------------------------

$errorString = ""; //erreur lors du traitement du formulaire

if( isset( $_POST[ "createGiftToken" ] ) && parsePostData() ){
	
	$giftToken =& createGiftToken();
	$order =& orderGiftToken( $giftToken );
	
	displayPaymentForm( $order, $giftToken->getId() );
	
}
else if( isset( $_GET[ "paid" ] ) ){

	//invoiceGiftToken();	=> déjà fait dans order_secure.php utilisé par la banque
	
	sendGiftToken();
	confirmGiftTokenPayment();
	
}
else{
	
	if( isset( $_GET[ "cancel" ] ) ){
		
		$errorString = Dictionnary::translate( "payment_not_validated" );
		cancelGiftToken();
		
	}
	
	displayForm();

}

//----------------------------------------------------------------------------
//affichage de la page

function displayForm(){
	
	global $errorString;
	
	$availableGiftTokens 	= getAvailableGiftTokens();
	$defaultGiftToken 		= key( $availableGiftTokens );

	class GiftTokenController extends PageController{};

	$controller = new GiftTokenController( "gift_token.tpl" );
	
	$controller->createElement( "sender_name", 					isset( $_POST[ "sender_name" ] ) ? stripslashes( $_POST[ "sender_name" ] ) : "" );
	$controller->createElement( "target_name", 					isset( $_POST[ "target_name" ] ) ? stripslashes( $_POST[ "target_name" ] ) : "" );
	$controller->createElement( "target_email", 				isset( $_POST[ "target_email" ] ) ? stripslashes( $_POST[ "target_email" ] ) : "" );
	$controller->createElement( "target_email_confirmation",	"" );
	$controller->createElement( "gift_token_message", 			isset( $_POST[ "gift_token_message" ] ) ? stripslashes( $_POST[ "gift_token_message" ] ) : "Votre message..." );
	$controller->createElement( "gift_token_amount", 			isset( $_POST[ "gift_token_amount" ] ) ? intval( $_POST[ "gift_token_amount" ] ) : $defaultGiftToken )->setOptions( $availableGiftTokens );

	$controller->setData( "errorString", $errorString );
	
	$controller->output();

}

//----------------------------------------------------------------------------

/**
 * Créer le chèque-cadeau
 */
function &createGiftToken(){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/GiftToken.php" );
	
	$idmodel 	= intval( $_POST[ "gift_token_amount" ] );
	$idbuyer 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer() : 0;
	$idcontact 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getId() : 0;
	
	$giftToken 	=& GiftToken::create( $idmodel, $idbuyer, $idcontact );

	return $giftToken;
	
}

//----------------------------------------------------------------------------

/**
 * Envoyer le chèque-cadeau au destinataire
 */
function sendGiftToken(){

	global 	$GLOBAL_START_URL,
			$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
			
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$params = unserialize( Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "params" ] ), $GLOBAL_DB_PASS ) );
	
	$idorder 			= $params[ "idorder" ];
	$idtoken 			= $params[ "idtoken" ];
	$sender_name 		= $params[ "sender_name" ];
	$target_name 		= $params[ "target_name" ];
	$target_email 		= $params[ "target_email" ];
	$gift_token_amount 	= $params[ "gift_token_amount" ];
	$gift_token_message = $params[ "gift_token_message" ];
	
	$giftToken = new GiftToken( $idtoken );
	
	class GiftTokenMailController extends PageController{};

	$controller = new GiftTokenMailController( "gift_token_mail.tpl" );
	
	$controller->setData( "serial", 			$giftToken->get( "serial" ) );
	$controller->setData( "gift_token_amount", 	Util::priceFormat( DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $params[ "gift_token_amount" ] ) ) );
	$controller->setData( "gift_token_message", $gift_token_message );
	$controller->setData( "sender_name",		$sender_name );
	$controller->setData( "sitename",			DBUtil::getParameterAdmin( "ad_sitename" ) );

	if( ob_get_contents() )
		ob_end_clean();
	
	//envois du mail au destinataire
	
	ob_start();
	
	$controller->output();

	$message = ob_get_contents();
	
	ob_end_clean();

	$mailer = new htmlMimeMail();
	
	$mailer->setFrom( "\"$sender_name\" <$sender_email>" ); 
	$mailer->setSubject( Dictionnary::translate( "your_gift_token" ) );
	$mailer->setHtml( $message );
	
	$ret = $mailer->send( array( $target_email ) );

	if( !$ret ){
		
		$phonenumber = DBUtil::getDBValue( "phonenumber", "user", "iduser", DBUtil::getParameter( "contact_commercial" ) );
		$message = Dictionnary::translate( "gift_token_send_failed" );
		$message .= "<br />" . str_replace( "%phonenumber", $phonenumber, Dictionnary::translate( "commercial_assistance" ) );
	
		trigger_error( $message, E_USER_ERROR );
	
	}
	
}

//----------------------------------------------------------------------------

function confirmGiftTokenPayment(){
	
	global 	$GLOBAL_START_URL,
			$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
			
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$params = unserialize( Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "params" ] ), $GLOBAL_DB_PASS ) );
	
	$idorder 			= $params[ "idorder" ];
	$idtoken 			= $params[ "idtoken" ];
	$sender_name 		= $params[ "sender_name" ];
	$target_name 		= $params[ "target_name" ];
	$target_email 		= $params[ "target_email" ];
	$gift_token_amount 	= $params[ "gift_token_amount" ];
	$gift_token_message = $params[ "gift_token_message" ];
	
	//confirmation
	
	class GiftTokenController extends PageController{};

	$controller = new GiftTokenController( "gift_token_payment.tpl" );
	
	$controller->setData( "sender_name", 		$sender_name );
	$controller->setData( "sender_email", 		Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getEmail() : "" );
	$controller->setData( "target_name", 		$target_name );
	$controller->setData( "target_email", 		$target_email );
	$controller->setData( "gift_token_message", $gift_token_message );
	$controller->setData( "gift_token_amount", 	$gift_token_amount );
	$controller->setData( "gift_token_paid", 	true );
	$controller->setData( "idorder", 			$idorder );
	$controller->setData( "gift_token_serial", 	DBUtil::getDBValue( "serial", "gift_token", "idtoken", $idtoken ) );
	
	$controller->output();
	
}

//----------------------------------------------------------------------------

function &orderGiftToken( &$giftToken ){
	
	global $GLOBAL_START_PATH;
	
	$idbuyer 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$idcontact 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getId() : 0;
	$idmodel 	= intval( $_POST[ "gift_token_amount" ] );

	User::fakeAuthentification();
	$order 		=TradeFactory::createOrderFromGiftToken( $giftToken, $idbuyer, $idcontact );
	
	return $order;

}

//----------------------------------------------------------------------------

function displayPaymentForm( &$order, $idtoken ){
	
	global 	$GLOBAL_DB_PASS,
			$GLOBAL_START_PATH, 
			$GLOBAL_START_URL;
	
	$availableGiftTokens = getAvailableGiftTokens();
	
	class GiftTokenController extends PageController{};

	$controller = new GiftTokenController( "gift_token_payment.tpl" );
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$params = Encryptor::encrypt( serialize( array(
	
		"idtoken" 				=> $idtoken,
		"idorder" 				=> $order->getId(),
		"sender_name" 			=> stripslashes( $_POST[ "sender_name" ] ),
		"target_name" 			=> stripslashes( $_POST[ "target_name" ] ),
		"target_email" 			=> stripslashes( $_POST[ "target_email" ] ),
		"gift_token_amount" 	=> stripslashes( $_POST[ "gift_token_amount" ] ),
		"gift_token_message" 	=> stripslashes( $_POST[ "gift_token_message" ] )
	
	) ), $GLOBAL_DB_PASS );
	
	$controller->createElement( "sender_name", 			stripslashes( $_POST[ "sender_name" ] ) );
	$controller->createElement( "target_name", 			stripslashes( $_POST[ "target_name" ] ) );
	$controller->createElement( "target_email", 		stripslashes( $_POST[ "target_email" ] ) );
	$controller->createElement( "gift_token_message", 	stripslashes( $_POST[ "gift_token_message" ] ) );
	$controller->createElement( "gift_token_amount", 	$availableGiftTokens[ intval( $_POST[ "gift_token_amount" ] ) ] )->setOptions( $availableGiftTokens );
	$controller->setData( "gift_token_paid", 			false );
	$controller->setData( "cancel_parameters", 			HtmlEncode( $params ) );
	
	//formulaire de paiement

    $formData = createPaymentForm( $order, $idtoken );
    $controller->setData( "paymentForm", $formData );
       
	$controller->output();

}

//----------------------------------------------------------------------------

/**
 * Traitement du formulaire de création du chèque-cadeau
 */
function parsePostData(){
	
	global $errorString;
	
	//champs obligatoires
	
	$requiredFields = array( 
	
		"sender_name",
		"target_name",
		"target_email",
		"target_email_confirmation",
		"gift_token_amount",
		"gift_token_message"
		
	);

	foreach( $requiredFields as $requiredField ){
			
		if( !strlen( $_POST[ $requiredField ] ) ){
			
			$errorString = str_replace( "%fieldname", Dictionnary::translate( $requiredField ), Dictionnary::translate( "missing_required_field" ) );
			
			return false;
			
		}
		
	}
	
	//confirmation de l'email
	
	if( strcmp( $_POST[ "target_email" ], $_POST[ "target_email_confirmation" ] ) !== 0 ){
		
		$errorString = Dictionnary::translate( "bad_target_mail_confirmation" );
			
		return false;
		
	}
	
	//format de l'email du destinataire
	
	if( !preg_match( "/[^@]+@[^\.]+\..+/", stripslashes( $_POST[ "target_email" ] ) ) ){
		
		$errorString = Dictionnary::translate( "bad_target_mail_format" );
			
		return false;
		
	}
	
	//cgv
	
	if( !isset( $_POST[ "cgv" ] ) ){
		
		$errorString = Dictionnary::translate( "cvg_not_read" );
			
		return false;
		
	}
	
	return true;
	
}

//----------------------------------------------------------------------------

/**
 * Liste des chèquescadeaux disponibles
 * @return array un tableau indéxé de la forme array( gift_token_model.idmodel => gift_token_model.name )
 */
function getAvailableGiftTokens(){
	
	$query = "
	SELECT idmodel,
		value,
		name,
		image
	FROM gift_token_model
	WHERE type LIKE 'amount'
	AND start_date <= NOW()
	AND end_date >= NOW()
	ORDER BY value ASC";
	
	$rs =& DBUtil::query( $query );
	
	$giftTokens = array();
	while( !$rs->EOF() ){
		
		$giftTokens[ $rs->fields( "idmodel" ) ] = $rs->fields( "name" );
		
		$rs->MoveNext();
			
	}
	
	return $giftTokens;
	
}

//----------------------------------------------------------------------------

/**
 * Annulation du chèque-cadeau, de la commande et de la facture
 */
function cancelGiftToken(){
	
	global 	$GLOBAL_DB_PASS,
			$errorString;
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$errorString 	= Dictionnary::translate( "gift_token_cancelled" );
	$params 		= unserialize( Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "params" ] ), $GLOBAL_DB_PASS ) );
	
	DBUtil::query( "DELETE FROM gift_token WHERE idorder = '" . $params[ "idorder" ] . "' LIMIT 1" );
	Order::delete( $params[ "idorder" ] );
	Invoice::delete( DBUtil::getDBValue( "idbilling_buyer", "billing_buyer", "idorder", $params[ "idorder" ] ) );
	
}

//----------------------------------------------------------------------------

/**
 * Facturer le chèque-cadeau
 */
function &invoiceGiftToken(){

	global 	$GLOBAL_START_URL,
			$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;

	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$params = unserialize( Encryptor::decrypt( URLFactory::base64url_decode( $_GET[ "params" ] ), $GLOBAL_DB_PASS ) );
	
	//ne pas facturer 2 fois
	
	$rs =& DBUtil::query( "SELECT COUNT( idbilling_buyer ) AS alreadyInvoiced FROM billing_buyer WHERE idorder = '" . $params[ "idorder" ] ."'" );
	
	if( $rs->fields( "alreadyInvoiced" ) ){
		
		header( "Location: $GLOBAL_START_URL/catalog/gift_token.php" );
		exit();
		
	}
	
	User::fakeAuthentification();
	$order 		= new Order( $params[ "idorder" ] );
	$invoice 	=TradeFactory::createInvoiceFromGiftTokenOrder( $order );
	
	//sauvegarder le paiement dans la commande
	
	include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
	
	$order->set( "idpayment", 		Payment::$PAYMENT_MOD_DEBIT_CARD );
	$order->set( "status", 		Order::$STATUS_PAID );
	$order->set( "paid", 			1 );
	$order->set( "cash_payment", 	1 );
	$order->set( "balance_date", 	date( "Y-m-d" ) );
	$order->save();

	return $invoice;
		
}

//----------------------------------------------------------------------------

/**
 * Créer le formulaire de paiement
 */
function createPaymentForm( &$order, $idtoken ){

	global 	$GLOBAL_START_URL, 
			$GLOBAL_START_PATH,
			$GLOBAL_DB_PASS;
	
	$availableGiftTokens = getAvailableGiftTokens();
	
	include_once( "$GLOBAL_START_PATH/objects/Encryptor.php" );
	
	$params = Encryptor::encrypt( serialize( array(
	
		"idtoken" 				=> $idtoken,
		"idorder" 				=> $order->getId(),
		"sender_name" 			=> stripslashes( $_POST[ "sender_name" ] ),
		"target_name" 			=> stripslashes( $_POST[ "target_name" ] ),
		"target_email" 			=> stripslashes( $_POST[ "target_email" ] ),
		"gift_token_amount" 	=> stripslashes( $_POST[ "gift_token_amount" ] ),
		"gift_token_message" 	=> stripslashes( $_POST[ "gift_token_message" ] )
	
	) ), $GLOBAL_DB_PASS );
	
	include_once( dirname( __FILE__ ) . "/../objects/PaySecure.php" );
	
	$pay_secure = new PaySecure();	
	
	$pay_secure->forceConfig( "retourok" , "$GLOBAL_START_URL/catalog/gift_token.php?paid&params=" . URLFactory::base64url_encode( $params ) );
	$pay_secure->forceConfig( "retourko" , "$GLOBAL_START_URL/catalog/gift_token.php?cancel&params=" . URLFactory::base64url_encode( $params ) );
	$pay_secure->forceConfig( "submit"   , "Régler ma commande" );
	
	$pay_secure->CreerFormulaireHmac( round( $order->getNetBill(), 2 ) , $order->getId() ); 

}
?>