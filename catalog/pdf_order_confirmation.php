<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf commandes confirmées 
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFOrder.php" );

$odf = new ODFOrder(  $_REQUEST[ "idorder" ]  );

$odf->exportPDF( "Confirmation de commande n° " .  $_REQUEST[ "idorder" ]  . ".pdf" );

?>