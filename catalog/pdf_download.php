<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonction ajout fiche produit en PDF
 */
 

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/URLFactory.php" );
include_once( dirname( __FILE__ ) . "/../objects/URLFactory.php" );

include_once( "Validate.php" );
include_once( "Validate/FR.php" );

$_REQUEST =& Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
	
/* champs de saisie obligatoires */

$postData = array(

	"gender" 	=> isset( $_REQUEST[ "gender_pdf" ] ) 		? intval( $_REQUEST[ "gender_pdf" ] ) 	: 0,
	"lastname"	=> isset( $_REQUEST[ "lastname_pdf" ] ) 	? $_REQUEST[ "lastname_pdf" ] 			: "",
	"firstname"	=> isset( $_REQUEST[ "firstname_pdf" ] ) 	? $_REQUEST[ "firstname_pdf" ] 		: "",
	"company"	=> isset( $_REQUEST[ "company_pdf" ] ) 		? $_REQUEST[ "company_pdf" ] 			: "",
	"email"		=> isset( $_REQUEST[ "email_pdf" ] ) 		? $_REQUEST[ "email_pdf" ] 			: "",
	"zipcode"	=> isset( $_REQUEST[ "zipcode_pdf" ] ) 		? $_REQUEST[ "zipcode_pdf" ] 			: ""
	
);

if( !Validate::email( $_REQUEST[ "email_pdf" ] ) )
	exit( "Le format de l'adresse email n'est pas valide" );
	
/* stat + téléchargement */

DBUtil::query( "INSERT INTO pdf_downloads ( idproduct, datetime, `" . implode( "`,`", array_keys( $postData ) ) . "` ) VALUES( '" . intval( $_REQUEST[ "idproduct" ] ) . "', NOW(), '" . implode( "','", array_values( $postData ) ) . "' )" );

$ret = mailPDF( $_REQUEST[ "email_pdf" ], intval( $_REQUEST[ "idproduct" ] ) );

exit( $ret ? "1" : "0" );

/* ------------------------------------------------------------------------------------------------------------------ */

function mailPDF( $email, $idproduct ){
	
	global $GLOBAL_START_PATH;
	
	include_once( "$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php" );
	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor 	= new MailTemplateEditor();
	$mailer 	= new htmlMimeMail();
	
	$editor->setLanguage( substr( "_1", 1 ) );
	$editor->setTemplate( MailTemplateEditor::$PDF_DOWNLOAD );
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseProductTags( $idproduct );

	$mailer->setHtml( $editor->unTagBody() );

	$mailer->setSubject( $editor->unTagSubject() );
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) ."\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );

	$mailer->addAttachment( file_get_contents( URLFactory::getPDFURL( $idproduct) ), "Notre fiche technique N° $idproduct.pdf", "application/pdf" );
	
	return $mailer->send( array( $email ) );
	
}

/* ------------------------------------------------------------------------------------------------------------------ */

?>