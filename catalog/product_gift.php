<?php

class ProductGift{
	
	protected $_basketItem;
	protected $_basket;
	protected $_product_gift;
	protected $_articleInCart;
	protected $_articleAutom;
	protected $_b;

	public function __construct(&$basketItem, &$basket){
		$this->_basketItem = $basketItem;
		$this->_product_gift = array();
		$this->_articleInCart = array();
		$this->_articleAutom = array();
		$this->_basket = $basket;
		$this->initArray();
		$this->sortItem();
		$this->articleAddAutom();

		$this->removeAutomAdd();

		
		$basketItem = $this->_basketItem;
		$basket = $this->_basket;
		$this->addProductGift();
		
	}

	public function sortItem(){
		usort($this->_basketItem, array($this, "cmpArt"));

	}

	public function isInBasket($id) {
	   foreach ($_SESSION[ "basket_items" ] as $val) {

	       		if($val['idarticle'] == $id) return true;
	   }
	   return false;
	}

	//point 1

	public function articleAddAutom(){
		$allAutom = $this->_basket->getProductAutom();
		//var_dump($_SESSION["RM_BASKET"]);die;
		foreach($allAutom as $val){
			$it = DBUtil::query("SELECT idarticle, idproduct FROM `detail` WHERE `reference` = '".$val['reference']."'");
			
				while (!$it->EOF())
				{

					$idarticle = $it->fields['idarticle'];
					if(!$this->isInBasket($idarticle) && !in_array($idarticle, $_SESSION["RM_BASKET"])){
						$this->_articleAutom[] = $idarticle;
						$this->_basket->addArticle(new CArticle($idarticle),0);
						$this->_basketItem[] = new BasketItemProxy(new BasketItem(new CArticle($idarticle),0),true);

					}else if(in_array($idarticle, $_SESSION["RM_BASKET"]) && $k=array_search($idarticle, $_SESSION["RM_BASKET"])  ){
						unset($this->_basketItem[$k]);

						
					}
					
					$it->MoveNext();		
				}
		}

	}

	public function getArticleAutom(){
		return $this->_articleAutom;
	}

	public function cmpArt($a, $b){
		//echo '$a->isGift ' . $a->isGift;
		//echo '$b->isGift ' . $b->isGift;
		return $b->isGift ? -1 : 1;
	}

	//point 2
	public function removeAutomAdd(){
		foreach($this->_basketItem as $index=>$val){
			if($val->getQuantity() == 0){
				unset($array[$index]);
			}
		}
	}

	public function addProductGift(){
		$allGift = $this->_basket->getProductGift();


		foreach($allGift as $key=>$val){

			if($this->_basket->getTotalET() > intval($val['amount_min']) ){

				$this->_product_gift[$val['amount_min']][] = $val['reference'];

				$it = DBUtil::query("SELECT idarticle, idproduct FROM `detail` WHERE `reference` = '".$val['reference']."'");


				while (!$it->EOF())
				{
					$idarticle = $it->fields['idarticle'];
					$this->addBasketItemGift( intval( $idarticle ));
					$it->MoveNext();		
				}
			}
		}
	}

	public function addBasketItemGift($idarticle, $quantity = 1, $idcolor = 0, $idcloth = 0){
		if( !$quantity && $quantity < 1 || isset($this->_b[$idarticle]))
		return;
		$this->_articleInCart[] = $idarticle;
		return ;
	}

	private function initArray(){
		$b=[];
		foreach($_SESSION[ "basket_items" ] as $s){
			$b[$s['idarticle']] = 0;
		}
		$this->_b=$b;
	}

	public function getArticleGiftInCart(){
		return $this->_articleInCart;
	}
}

?>