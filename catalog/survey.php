<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Questionnaire satisfcation client
 */
 
include( "./deprecations.php" );

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

class IndexController extends PageController{};
$survey_template = new IndexController( "survey.tpl" );

$ScriptName = "survey.php";

//---------------------------------------------//
//-----------------traductions-----------------//
//---------------------------------------------//

$translations = array();
$translations[ "survey_title" ]			= Dictionnary::translate( "survey_title" );
$translations[ "survey_man" ]			= Dictionnary::translate( "survey_man" );
$translations[ "satisfaction_level_1" ]	= Dictionnary::translate( "satisfaction_level_1" );
$translations[ "satisfaction_level_2" ]	= Dictionnary::translate( "satisfaction_level_2" );
$translations[ "satisfaction_level_3" ]	= Dictionnary::translate( "satisfaction_level_3" );
$translations[ "satisfaction_level_4" ]	= Dictionnary::translate( "satisfaction_level_4" );
$translations[ "satisfaction_level_5" ]	= Dictionnary::translate( "satisfaction_level_5" );
$translations[ "survey_email" ]			= Dictionnary::translate( "survey_email" );
$translations[ "survey_comment_title" ]	= Dictionnary::translate( "survey_comment_title" );
$survey_template->setData( "translations", $translations );

//----------------------------------------------------------------------------------------------

if( isset( $_GET[ "action" ] ) && $_GET[ "action" ] == "post" ){

	if( !isset( $_POST[ "idsatisfaction_ask" ] ) && !isset( $_POST[ "reply" ] ) ){

		$register_error = "Paramètre absent";

	}else{

		if( count( $_POST[ "idsatisfaction_ask" ] ) != count( $_POST[ "idsatisfaction_ask" ] ) ){

			$register_error = "Données du formulaire incorrectes";

		}else{
		
			$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
			$date = $lastupdate = date( "Y-m-d H:i:s" );
			$username = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getEmail() : "";
			
			$i = 0;
			while( $i < count( $_POST[ "idsatisfaction_ask" ] ) ){
				
				$idsatisfaction_ask = $_POST[ "idsatisfaction_ask" ][ $i ];
				$note = $_POST[ "reply" ][ $i ];
				
				$query = "INSERT INTO satisfaction_answer(
					idsatisfaction_ask,
					idbuyer,
					note,
					date,
					username,
					lastupdate )
				VALUES(
					'$idsatisfaction_ask',
					'$idbuyer',
					'$note',
					'$date',
					'$username',
					'$lastupdate' )";
				
				$rs = DBUtil::query( $query );
			
				if( $rs === false ){
					
					$register_error = "Impossible d'enregistrer le formulaire";
				
				}
				
				$i++;
		
			}
		
		}
	
	}
	
	$survey_template->setData( "register", "ok" );
	
	if( isset( $register_error ) )
		$survey_template->setData( "register_error", "ok" );
	else
		$survey_template->setData( "register_error", "" );

}else{

	$query = "SELECT idsatisfaction_ask AS id, satisfaction_ask_1 AS question FROM satisfaction_ask WHERE display = 1 ORDER BY display_order ASC";
	$rs = DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer le questionnaire" );
	
	if( !$rs->RecordCount() )
		return array();
			
	$survey = array();
	$i = 0;
	while( !$rs->EOF() ){
		
		$survey[ $i ][ "offset" ]				= $i;
		$survey[ $i ][ "id" ]					= $rs->fields( "id" );
		$survey[ $i ][ "question" ]				= $rs->fields( "question" );
		
		$rs->MoveNext();
		$i++;
		
	}

	$survey_template->setData( "survey_lines", $survey );
	$survey_template->setData( "register", "" );

}

//---------------------------------------------//
//---------------------mail--------------------//
//---------------------------------------------//

if( isset( $_POST[ "comment" ] ) && !empty( $_POST[ "comment" ] ) ){

	$adminName = DBUtil::getParameterAdmin( "ad_name" );
	$adminEmail = DBUtil::getParameterAdmin( "ad_mail" );

	if( Session::getInstance()->getCustomer() ){

		$senderEmail = Session::getInstance()->getCustomer()->getContact()->getEmail();
		$senderName = DBUtil::getDBValue( "title_1", "title", "idtitle", Session::getInstance()->getCustomer()->getContact()->getGenders() ) . " " . Session::getInstance()->getCustomer()->getContact()->getFirstName() . " " . Session::getInstance()->getCustomer()->getContact()->getLastName();

	}else if( isset( $_POST[ "email" ] ) && !empty( $_POST[ "email" ] ) ){

		$senderEmail = $_POST[ "email" ];
		$senderName = "Inconnu";
	
	}else{

		$senderEmail = DBUtil::getParameterAdmin('ad_mail');
		$senderName= DBUtil::getParameterAdmin('ad_name');
	
	}
	
	$subject = Dictionnary::translate( "survey_mail_subject" );	
	require_once( "HTML/Template/Flexy.php" );
	$options['templateDir'][] = "$GLOBAL_START_PATH/templates/catalog/";
	ob_start();

	$survey_mail = new HTML_Template_Flexy( $options );

	$survey_mail->setData( "surveyHTML", stripslashes( $_POST[ "surveyHTML" ] ) );
	$survey_mail->setData( "comment", stripslashes( $_POST[ "comment" ] ) );
	$survey_mail->setData( "server_host", $_SERVER[ "HTTP_HOST" ] );
	$survey_mail->setData( "ad_logo", DBUtil::getParameterAdmin( "ad_logo" ) );
	$survey_mail->setData( "date", humanReadableDatetime( date( "Y-m-d H:i:s" ) ) );
	$survey_mail->setData( "ad_sitename", DBUtil::getParameterAdmin( "ad_sitename" ) );
	
	//---------------------------------------------//
	//-----------------traductions-----------------//
	//---------------------------------------------//
	
	$translations = array();
	$translations[ "survey_title" ]			= Dictionnary::translate( "survey_title" );
	$translations[ "survey_notification" ]	= Dictionnary::translate( "survey_notification" );
	$translations[ "survey_result" ]		= Dictionnary::translate( "survey_result" );
	$translations[ "survey_user_comment" ]	= Dictionnary::translate( "survey_user_comment" );
	$survey_mail->setData( "translations", $translations );
	
	$survey_mail->compile( "mail_survey.tpl" );
	$survey_mail->output();

	$data = ob_get_contents();
	ob_end_clean();

	include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );
	
	$mailer = new EVM_Mailer();
	
	$mailer = new EVM_Mailer();
	$mailer->set_message_type( "html" );
	$mailer->set_sender( $senderName, $senderEmail );
	$mailer->add_recipient( $adminName, $adminEmail );
	$mailer->set_message($data);
	$mailer->set_subject( $subject );
	$mailer->send();

	$survey_template->setData( "report", "ok" );
	
}else{
	
	$survey_template->setData( "report", "" );
	
}


$survey_template->output();
?>