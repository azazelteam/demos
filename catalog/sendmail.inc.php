<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi de mail de confirmation (register, order)
*/

include_once('../objects/mailerobject.php');

/**
 * Envoi de mail de confirmation
 * 
 * @param string $include Chemin vers le fichier contenant le texte de l'e-mail
 * @param array $pdf Nom de fichier + données du fichier pdf à attacher
 */
function SendMail($include, $attachments = array( "name" => array(), "path" => array(), "mimetype" => array() ), $pdf = array( "name" => array(), "data" =>array() ) )
{
	
	global 
			$Order,
			$errorMsg,
			$GLOBAL_START_URL,
			$GLOBAL_START_PATH,
			$ScriptName,
			$UserInfos;
			
	$DEBUG = 'none';
	
	$AdminName= DBUtil::getParameterAdmin('ad_name');
	$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
	$AdminSite= DBUtil::getParameterAdmin('ad_site');
	$AdminLogo= DBUtil::getParameterAdmin('ad_logo');	
	// Infos du commercial par défaut (dans parameter_cat)
	$iduser = User::getInstance()->getId();
	if( !empty($iduser)  && $iduser == DBUtil::getParameter('contact_commercial') ) {
		$CommercialName= User::getInstance()->get('firstname').' '.User::getInstance()->get('lastname');
		$CommercialEmail = User::getInstance()->get('email');
		$CommercialPhone = User::getInstance()->get('phonenumber');		
	}
	else {
		$CommercialName= DBUtil::getParameterAdmin('ad_name');
		$CommercialEmail = DBUtil::getParameterAdmin('ad_mail');
	}

	// Variables à revaloriser dans le fichier $include
	$BuyerName = '';
	$BuyerEmail = '';
	$Type = 'html'; // Format 'html' ou 'text'
	$Subject = '';
	$Message = '';
	include($include);
	
	
	// Envoi du mail
	$EVM_Mailer = new EVM_Mailer();
	$EVM_Mailer->set_message_type($Type);
	$EVM_Mailer->set_sender( DBUtil::getParameterAdmin( "ad_name" ), $AdminEmail);
	$EVM_Mailer->set_subject($Subject);
	$EVM_Mailer->add_recipient($BuyerName,$BuyerEmail);
	//die("subject : ".$Subject." name : ".$BuyerName." mail : ".$BuyerEmail);
	//$EVM_Mailer->add_recipient($CommercialName,$CommercialEmail);
	//$EVM_Mailer->add_BCC($CommercialName,$CommercialEmail);
	$EVM_Mailer->add_CC( $CommercialName,$CommercialEmail );

	$EVM_Mailer->set_message($Message);
	
	//attachments
	$i = 0;
	while( $i < count( $attachments[ "name" ] ) ){
	
		$EVM_Mailer->add_attachment( 
			$attachments[ "name" ][ $i ], 
			$attachments[ "path" ][ $i ], 
			$attachments[ "mimetype" ][ $i ] 
		);
		
		$i++;
			
	}
	
	//pdf
	$i = 0;
	while( $i < count( $pdf[ "name" ] ) ){
	
		$EVM_Mailer->add_pdf( 
			$pdf[ "name" ][ $i ], 
			$pdf[ "data" ][ $i ]
		);
		
		$i++;
			
	}
	
	showDebug($EVM_Mailer,'sendmail.inc.php => $EVM_Mailer','none');
	$EVM_Mailer->send();
	if( $EVM_Mailer->is_errors() ) {
		$errorMsg[] = array('var'=>$EVM_Mailer->get_errors(),'user'=>Dictionnary::translate('error'),'log'=>$ScriptName.' (sendmail.inc.php) => $EVM_Mailer->get_errors()');
	}
	showDebug($EVM_Mailer,'sendmail.inc.php => $EVM_Mailer',$DEBUG);
	//$Message = wordwrap(str_replace('\n','\r\n',$Message),70);
	//$res = mail("$BuyerName <$BuyerEmail>",$Subject,$Message);
	//showDebug($res,'sendmail.inc.php => '."mail('$BuyerName <$BuyerEmail>','$Subject','$Message')",'log');
}
?>