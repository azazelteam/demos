<?php 
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ...?
 */
 
include( "./deprecations.php" );

include_once( dirname( __FILE__ ) . "/../config/init.php" );  
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

class PaySecureController extends PageController{};
$pay_secure = new PaySecureController( "pay_secure.tpl" );

$lang = "_1";

if( !isset( $_SESSION[ "idorder_secure" ] ) || !intval( $_SESSION[ "idorder_secure" ] ) ){
	
	header( "Location: $GLOBAL_START_URL" );
	exit();
	
}

$idorder = intval($_SESSION["idorder_secure"]);

$Order = new Order( $idorder );

//Tracker
$CCM_tracker_mail = DBUtil::getParameterAdmin('CCM_tracker_mail');
$SiteName 	= DBUtil::getParameterAdmin('ad_name');
$headers = 'From: AKILAE <webmaster@akilae-saas.fr>' . "\r\n";
// $headers .= 'Bcc: webmaster@akilae-saas.fr' . "\r\n";

//---------------------------------------------//
//-------------------account-------------------//
if( !Session::getInstance()->getCustomer() ) {
	
	$com_step2 = true;
	if(empty($com_step2)) {
		$pay_secure->setData('com_step_empty', "ok");
	} else {
		$pay_secure->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) ) {
		$pay_secure->setData('issetPostLoginAndNotLogged', "ok");
		$pay_secure->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$pay_secure->setData('issetPostLoginAndNotLogged', "");
	}
	$pay_secure->setData('scriptName', $ScriptName);
	$pay_secure->setData('translateLogin',Dictionnary::translate('login'));
	$pay_secure->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$pay_secure->setData('img', "acceder_compte.png");
		$pay_secure->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$pay_secure->setData('img', "poursuivre.png");
		$pay_secure->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

//----------------------------------------------------------------------------

$rs =& DBUtil::query( "SELECT idbilling_buyer FROM bl_delivery WHERE idorder = '" . $Order->getId() . "'" );
if( !$rs->RecordCount() ){ /* paiement intégral */
	
	$gift_token = $Order->getGiftToken();
	$gift_token_amount = $gift_token == false ? 0.0 : DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $gift_token->get( "idtoken_model" ) );
	$Montant = max( 0.0, round( $Order->getNetBill() - $gift_token_amount, 2 ) );
	
}
else{ /* reste à payer */
	
	$Montant = 0.0;
	while( !$rs->EOF() ){
		
		$invoice = new Invoice( $rs->fields( "idbilling_buyer" ) );
		$Montant += max( 0.0, $invoice->getBalanceOutstanding() );
		
		$rs->MoveNext();
		
	}
	
}

$Reference_Cde = urlencode($idorder);

$sEmail = $Order->getCustomer()->getContact()->getEmail();
$sDate = date("d/m/Y:H:i:s");

$tpeVer = DBUtil::getParameterAdmin( "CMCIC_version" );

switch ( $tpeVer ){
	case "3.0":
		include_once( dirname( __FILE__ ) . "/../objects/PaySecureV3.php" );			
		$pay_secure = new PaySecureV3();			
		$pay_secure->CreerFormulaireHmac( $sEmail , $Montant , $Reference_Cde , $sDate );			
		break;
		
	case "1.2open":
		include_once( dirname( __FILE__ ) . "/../objects/PaySecure.php" );			
		$pay_secure = new PaySecure();				
		$pay_secure->CreerFormulaireHmac( $Montant , $Reference_Cde );  			
		break;
	
	default :
		die( " TPE introuvable : La version n'est pas renseignée dans le paramètrage " );
		break;

}

//google
$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_conversion_id = DBUtil::getParameterAdmin( "google_order_conversion_id" );
$google_conversion_label = DBUtil::getParameterAdmin( "google_conversion_label_order" );
$ScriptNem = "finish.php";


if( $ScriptName == $ScriptNem && $google_conversion_id && $google_conversion_label && $UseGoogleConversion ){
	$pay_secure->setData('google',1);
	$pay_secure->setData('google_conversion_id',$google_conversion_id); 
	$pay_secure->setData('google_conversion_label',$google_conversion_label); 
}else{
	$pay_secure->setData('google',0);
}
	
$pay_secure->output();

?>
