<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des détails du produit (fiche produit)
 *  * Positionnement masque n°2
 * Lignes = Caractéristiques techniques (parm_*)
 * Colonnes = Références
 */
 

include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );


	$DEBUG = 'none';
	$default = '-';
	$familyLabels = array();
	$RefTable = array(
					'id'			=> array(),
					'checked'		=> array(),
					'summary'		=> array(),
					'reference'		=> array(),
					'weight'		=> array(),
					'lot'			=> array(),
					'unit'			=> array(),
					'delay'			=> array(),
					'parm'			=> array(),
					'cot'			=> array(),
					'reducerate'	=> array(),
					'basicprice'	=> array(),
					'promo'			=> array(),
					'woprice'		=> array(),
					'wtprice'		=> array(),
					'stock_level'	=> array(),
					'rowspan'		=> array(),
					'hiddenFields'	=> array(),
					'hidecost'		=> array()
					);
	foreach( $Articles as $id => $Article ){
					
			$a = null;
			$a = $Article->getArticle(); //@todo bof!

			$RefTable['id'][] = $id;
			$RefTable['checked'][] = $Article->IsSelected();
			$RefTable['summary'][] = $Article->get('summary','&nbsp;');
			$RefTable['reference'][] = $reference = $Article->GetVisualRef();
			$RefTable['weight'][] = getValueOrDefaultIfEmpty($Article->GetWeight(),$default);
			$RefTable['delay'][] = Util::getDelay($Article->GetDelivDelay());
			$RefTable['lot'][] = $Article->get('lot',$default);
			$RefTable['unit'][] = $Article->get('unit',$default);
			$RefTable['hidecost'][] = $Article->get('hidecost',0);
			$RefTable['stock_level'][] = $Article->GetStock();
			
			if(isset($a['fam']['label']))
				$familyLabels = array_count_values($a['fam']['label']);
			else $familyLabels = array();

			$rspan = 0;
			if($hideFamilies OR $Article->get('hide_family')) { $rspan ++;
				for( $j=1 ; $j < 100 ; $j++ )
				{
					$characLabel = $Article->get("parm_name_$j");
					$characValue = $Article->get("parm_value_$j");
					if(!empty($characLabel)) { $rspan ++;
						$RefTable['parm']['noFam'][$characLabel][] = $characValue;
						$RefTable['cot'][$characLabel] = !empty($a[$characLabel]) ? $a[$characLabel] : '&nbsp;';
					} else break;	
				}
			} else {
				foreach( $familyLabels as $familyLabel => $familyColspan ) {
					$rspan ++;
					// Caractéristiques techniques
					foreach( $a['int'][$familyLabel]['label'] as $k => $characLabel ) {
						$rspan ++;
						$characValue = !empty($a[$familyLabel][$characLabel]) ? $a[$familyLabel][$characLabel] : $default;
						$RefTable['parm'][$familyLabel][$characLabel][] = $characValue;
						$RefTable['cot'][$characLabel] = !empty($a[$characLabel]) ? $a[$characLabel] : '&nbsp;';
					}
				}
			}
	
			$reducerate = $Article->GetReduceRate();
			$basicprice = $Article->GetBasicPrice();
			
			if($Article->hasMultiPriceRange()){
				$RefTable['hasMultiPriceRange'][]="ok";
			}else{
				$RefTable['hasMultiPriceRange'][]="";
			}
			
			$arrayMultiprice = $Article->getMultiPrices();
			$RefTable['Multiprice'][]=$arrayMultiprice;
			
			$woprice = $Article->GetWoPrice();
			$wtprice = $Article->GetWtPrice();
			
			if($Article->IsReduceRate()){
				$RefTable['reducerate'][] = !empty($reducerate) ? Util::rateFormat($reducerate) : $default;
			}else{
				$RefTable['reducerate'][] = !empty($reducerate) ? Util::priceFormat($reducerate) : $default;
			}
			
	
			$RefTable['basicprice'][] = !empty($basicprice) ? Util::priceFormat($basicprice) : $default;
			
			if( Product::isPromo( $reference ) ){
				
				$RefTable['promo'][] = 'promo';
				$RefTable['hasPromo'][] = true;
				$RefTable['promo_price'][] = Util::priceFormat( $woprice );
				$RefTable['promo_rate'][] = Util::numberFormat($reducerate );
				
			}
			else{
				
				$RefTable['promo'][] = "";
				$RefTable['hasPromo'][] = false;
				$RefTable['promo_price'][] = "-";
				$RefTable['promo_rate'][] = "-";
				
			}
			
			if( Product::isDestocking( $reference ) ){
				
				$RefTable['isDestocking'][] = true;
				$rate = Product::getDestockingRate( $reference );
				$RefTable['destocking_rate'][] = $rate > 0.0 ? Util::rateFormat( $rate ) : "-";
				$RefTable['destocking_price'][] = Util::priceFormat( $basicprice - ($basicprice * $rate / 100.0 ));
				
			}
			else{
				
				$RefTable['isDestocking'][] = false;
				$RefTable['destocking_price'][] = "-";
				$RefTable['destocking_rate'][] = "-";
				
			}
			
			/*if( !empty($woprice) ) {
				$DisplayQuantityPrices = '';
				//if( Session::getInstance()->islogin() ) {
					$QuantityPrices = $Product->GetQuantityPrices($id);

					if ( !empty($QuantityPrices) ) {
						$DisplayQuantityPrices = '&nbsp;'.DisplayQuantityPrices($QuantityPrices);
					}
				//}
			}*/
			//$RefTable['woprice'][] = !empty($woprice) ? Util::priceFormat($woprice).$DisplayQuantityPrices : 'Nous consulter';
			$RefTable['woprice'][] = !empty($woprice) ? Util::priceFormat($woprice) : 'Nous consulter';
			$RefTable['wtprice'][] = !empty($woprice) ? Util::priceFormat($wtprice) : $default;
			$RefTable['rowspan'][] = !empty($woprice) ? '' : ' rowspan="2"';
			// attention $Article->article['idreal_product'] peut être différent de $Article->GetId() si couleur ou tissu
	
			$RefTable['hiddenFields'][] = array(
												'IdRealProduct'		=>	$Article->get('idarticle' ), /////GetId(),
												'reftotal'			=>	$Article->GetRef().$Article->getRefExtra(),
												'prixtotal'			=>	$Article->GetBasicPrice(),
												'prixtotalremise'	=>	Util::numberFormat( $Article->GetWoPrice() ),
												'caract'			=>	$Article->get('designation'),
												'r'					=>	$Article->GetReduceRate(),
												'poids'				=>	$Article->GetWeight(),
												'delay'				=>  $Article->GetDelivDelay(),
												'min_cde'			=>  $Article->get('min_cde','1'),
												'idtissus'			=>  $Article->GetIdTissus(),
												'idcolor'			=>  $Article->GetIdColor(),
												'option_price'		=>  $Article->GetOptionPrice()
												);
			
		}
			
	// Suppression des characs sans valeur pour toutes les references
	foreach( $RefTable['parm'] as $familyLabel => $characs ) {
		foreach( $characs as $characLabel => $characValues ) {
			if( count($characValues) > 1) {
				$array_unique = array_unique($characValues);

				if( count($array_unique) == 1 && $array_unique[0] == $default) {
					unset($RefTable['parm'][$familyLabel][$characLabel]);
					$rspan --;
				}
			}
		}
	}

?>