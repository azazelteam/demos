<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Catalogue client personnalisé
 */

// include necessary files
include_once '../objects/classes.php';


$IdBuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;

if(Session::getInstance()->getCustomer()){

	//Si l'ajout est une référence
	if( isset($_REQUEST['Ref'])){
		$Reference = $_REQUEST['Ref'];
		$Query = "SELECT * FROM buyer_catalog WHERE idbuyer='$IdBuyer' AND idproduct='$IdProduct' AND reference='$Reference'";
		$rs = DBUtil::query($Query);
	
		if( $rs->RecordCount() <= 0 ) {
			$Query = "INSERT INTO buyer_catalog (idbuyer,idproduct,reference) VALUES('$IdBuyer','$IdProduct','$Reference')";
			DBUtil::query($Query); //echo " $Query <br />\n";
		}
	}else{
		if( isset($_GET['IdProduct'])){
			$IdProduct = $_GET['IdProduct'];
		}else{
			header("HTTP/1.0 204 No Response");
		}
		//L'ajout est un produit, donc on met toutes les références	
		//On sélectionne toutes les références du produit
		$reqallref="SELECT reference FROM detail WHERE idproduct=$IdProduct";
		$rsallref=DBUtil::query($reqallref);

		while(!$rsallref->EOF()){
			$Reference = $rsallref->fields('reference');
			$Query = "SELECT count(*) as comptage FROM buyer_catalog WHERE idbuyer='$IdBuyer' AND idproduct='$IdProduct' AND reference='$Reference'";

			$rs = DBUtil::query($Query);
					
			if( $rs->fields("comptage") <= 0) {
				$Query = "INSERT INTO buyer_catalog (idbuyer,idproduct,reference) VALUES('$IdBuyer','$IdProduct','$Reference')";
				DBUtil::query($Query); //echo " $Query <br />\n";
			}
			$rsallref->MoveNext();
		}
	}
}
header("HTTP/1.0 204 No Response");
?>