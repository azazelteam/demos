<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet client pour afficher les factures
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class InvoiceController extends PageController {}
$controller = new InvoiceController( "/account/admin_invoice.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
	$controller->setData( "isInvoice", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
} else {
	$admin_order->setData('isLogin', "");
}

$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
/* On recherche les factures */
$query = "SELECT idbilling_buyer FROM `billing_buyer`
WHERE idbuyer = '$idbuyer'
ORDER BY DateHeure DESC";

$rs =& DBUtil::query( $query );

$array_invoice = array();
$mois = array("","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre");


/* On vérifie qu'il y existe au moins une facture... */
if( $rs->RecordCount() ){
	$controller->setData('NoResults',0);
} else {
	$controller->setData('NoResults',1);
	
}

/* On parcours les factures... */
for($i=0; $i<$rs->RecordCount(); $i++){
			
	$invoice = new Invoice($rs->fields( "idbilling_buyer" ));
	
	$array_invoice[$i]["idinvoice"] = ( $rs->fields( "idbilling_buyer" ) );
	
	$bl = array();
	$query = "SELECT idbl_delivery FROM bl_delivery WHERE idbilling_buyer=".intval( $rs->fields( "idbilling_buyer" ) );
	$rsbl=& DBUtil::query($query);
	
	for($j=0; $j<$rsbl->RecordCount(); $j++){
		$bl[$j] = intval( $rsbl->fields("idbl_delivery") );
		$rsbl->MoveNext();
	}	
	
	$array_invoice[$i]["bls"] = $bl;	
	
	list($datedev,$heure)=explode(" ", $invoice->get("DateHeure") );
	list($y,$m,$d)=explode("-",$datedev);
	$dateinvoice = $d." ".$mois[intval($m)]." ".$y;
	
	$array_invoice[$i]["DateHeure"] = Util::doNothing($dateinvoice);
	$array_invoice[$i]["status"] = Util::doNothing(Dictionnary::translate( $invoice->get( "status" ) ));
	
	$array_invoice[$i]["total_amount_ht"] = Util::priceFormat($invoice->getNetBill());
	
	list($y,$m,$d)=explode("-",$invoice->get("deliv_payment"));
	$deliv = $d." ".$mois[intval($m)]." ".$y;
	
	$array_invoice[$i]["echeance"] = $deliv;
	$rs->MoveNext();
	
}

/* On passe le tableau des factures au tpl */
$controller->setData('array_invoice',$array_invoice);


/* On ferme l'envoi de données */
$controller->output();

?>
