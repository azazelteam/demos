<?php

 /**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Adresse de livraison et facturation dans le panier
 */
 
include_once('../objects/classes.php');  

require_once("HTML/Template/Flexy.php");
require_once("HTML/Template/Flexy/Element.php");

include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/objects/Util.php" );

//----------------------------------------------------------------------------

$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

//----------------------------------------------------------------------------

$lang = "_1";

//page

$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
$iddelivery = isset( $_REQUEST["delivery_checkbox"] ) ? intval( $_POST["iddelivery"] ) : 0;
$idbilling = isset( $_REQUEST["billing_checkbox"] ) ? intval( $_POST["idbilling"] ) : 0;


/**
 * Enregistrement des adresses de livraison et de facturation
 * Mise à jour du basket
 */


//On traite le formulaire
if( isset( $_POST["delivery_checkbox"] ) ) {
	
	
	if( $_POST[ "delivery" ][ "Title" ] == 'Mr' || $_POST[ "delivery" ][ "Title" ] == 'Mme' || $_POST[ "delivery" ][ "Title" ] == 'Mlle'  ) {
	 $_POST[ "delivery" ][ "Title" ] = '';	
	}	
	if( $_POST[ "delivery" ][ "Lastname" ] == 'Nom'  ) {
	 $_POST[ "delivery" ][ "Lastname" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Firstname" ] == 'Prénom'  ) {
	 $_POST[ "delivery" ][ "Firstname" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Phonenumber" ] == 'TÃ©lÃ©phone'  ) {
	 $_POST[ "delivery" ][ "Phonenumber" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Faxnumber" ] == 'Fax'  ) {
	 $_POST[ "delivery" ][ "Faxnumber" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Company" ] == 'Entreprise'  ) {
	 $_POST[ "delivery" ][ "Company" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Adress" ] == 'Adresse'  ) {
	 $_POST[ "delivery" ][ "Adress" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Adress_2" ] == 'Adresse complÃ©mentaire'  ) {
	 $_POST[ "delivery" ][ "Adress_2" ] = '';	
	}
	if( $_POST[ "delivery" ][ "Zipcode" ] == 'Code postal'  ) {
	 $_POST[ "delivery" ][ "Zipcode" ] = '';	
	}
	if( $_POST[ "delivery" ][ "City" ] == 'Ville'  ) {
	 $_POST[ "delivery" ][ "City" ] = '';	
	}
	
}
if( isset( $_POST["billing_checkbox"] ) ) {
	if( $_POST[ "billing" ][ "Title" ] == 'Mr' || $_POST[ "billing" ][ "Title" ] == 'Mme' || $_POST[ "billing" ][ "Title" ] == 'Mlle'  ) {
	 $_POST[ "billing" ][ "Title" ] = '';		
	}	
	if( $_POST[ "billing" ][ "Lastname" ] == 'Nom'  ) {
	 $_POST[ "billing" ][ "Lastname" ] = '';	
	}
	if( $_POST[ "billing" ][ "Firstname" ] == 'Prénom'  ) {
	 $_POST[ "billing" ][ "Firstname" ] = '';	
	}
	if( $_POST[ "billing" ][ "Phonenumber" ] == 'TÃ©lÃ©phone'  ) {
	 $_POST[ "billing" ][ "Phonenumber" ] = '';	
	}
	if( $_POST[ "billing" ][ "Faxnumber" ] == 'Fax'  ) {
	 $_POST[ "billing" ][ "Faxnumber" ] = '';	
	}
	if( $_POST[ "billing" ][ "Company" ] == 'Entreprise'  ) {
	 $_POST[ "billing" ][ "Company" ] = '';	
	}
	if( $_POST[ "billing" ][ "Adress" ] == 'Adresse'  ) {
	 $_POST[ "billing" ][ "Adress" ] = '';	
	}
	if( $_POST[ "billing" ][ "Adress_2" ] == 'Adresse complÃ©mentaire'  ) {
	 $_POST[ "billing" ][ "Adress_2" ] = '';	
	}
	if( $_POST[ "billing" ][ "Zipcode" ] == 'Code postal'  ) {
	 $_POST[ "billing" ][ "Zipcode" ] = '';	
	}
	if( $_POST[ "billing" ][ "City" ] == 'Ville'  ) {
	 $_POST[ "billing" ][ "City" ] = '';	
	}
}

//print_r($_POST);

include_once( dirname( __FILE__ ) . "/../lib/buyer.php" );

//Adresse de livraison
if( isset( $_POST["delivery_checkbox"] ) ) {
	
	if( empty( $_POST[ "delivery" ][ "Lastname" ] )) {
		echo Util::doNothing(Dictionnary::translate( "register_lastname_required" ));
	} 
	else if( empty( $_POST[ "delivery" ][ "Adress" ] )){
		echo Util::doNothing(Dictionnary::translate( "register_address_required" ));
	}
	else if( empty( $_POST[ "delivery" ][ "City" ] )){
		echo "Merci de bien vouloir renseigner le champ 'Ville'";
	}	
	else if( empty( $_POST[ "delivery" ][ "Zipcode" ] )){
		echo "Merci de bien vouloir renseigner le champ 'Code postal'";
	}
	else {
		if( !empty( $_POST["iddelivery"] ) ) {
			$iddelivery = intval( $_POST["iddelivery"] );
		 	buyer_UpdateDelivery( $iddelivery, $_POST[ "delivery" ] );
		}
		if( empty( $_POST[ "iddelivery" ] ) ) {
			buyer_AddDeliveryAddress( $_POST[ "delivery" ] );
		}
		
		buyer_SetUseDeliveryAddress( $iddelivery );
		echo Util::doNothing("Adresse de livraison enregistrée.");			
	}		
}

echo "&";

//adresse de facturation
if( isset( $_POST["billing_checkbox"] ) ) {
	if( empty( $_POST[ "billing" ][ "Lastname" ] )) {
		echo Util::doNothing(Dictionnary::translate( "register_lastname_required" ));
	} 
	else if( empty( $_POST[ "billing" ][ "Adress" ] )){
		echo Util::doNothing(Dictionnary::translate( "register_address_required" ));
	}
	else if( empty( $_POST[ "billing" ][ "City" ] )){
		echo "Merci de bien vouloir renseigner le champ 'Ville'";
	}	
	else if( empty( $_POST[ "billing" ][ "Zipcode" ] )){
		echo "Merci de bien vouloir renseigner le champ 'Code postal'";
	}
	else {
		if( !empty( $_POST["idbilling"] ) ) {
			$idbilling = intval( $_POST["idbilling"] );
		 	buyer_UpdateBilling( $idbilling, $_POST[ "billing" ] );
		}
		if( empty( $_POST[ "idbilling" ] ) ) {
			$idbilling = buyer_AddBillingAddress( $_POST[ "billing" ] );
		}
		
		buyer_SetUseBillingAddress( $idbilling );
		echo Util::doNothing("Adresse de facturation enregistrée.");			
	}	 	
}

echo "&".$idbilling."&".$iddelivery;

$deliveryCheckStatus = $iddelivery ? "checked=\"checked\"" : "";
$billingCheckStatus = $idbilling ? "checked=\"checked\"" : "";

exit();	

?>