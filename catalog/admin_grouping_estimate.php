<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet client pour afficher les devis du groupement
 */

include_once('../objects/classes.php');  
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");

class AdminOrderController extends PageController{};
$admin_estimate = new AdminOrderController( "admin_grouping_estimate.tpl" );

$lang = "_1";

/**************************************************
 *********************ACCOUNT**********************
 **************************************************/
if( Session::getInstance()->getCustomer() ) {
	$admin_estimate->setData('isLogin', "ok");

	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	
	$admin_estimate->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$admin_estimate->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$admin_estimate->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$admin_estimate->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$admin_estimate->setData( "contact_lastname", $salesman->get("lastname") );
	$admin_estimate->setData( "contact_firstname", $salesman->get("firstname") );
	$admin_estimate->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$admin_estimate->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$admin_estimate->setData( "contact_email", $salesman->get("email") );
	
	$admin_estimate->setData( "isEstimate", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$admin_estimate->setData( "contact_grouping", $contact_grouping );
	
} else {
	$admin_estimate->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$admin_estimate->setData('com_step_empty', "ok");
	} else {
		$admin_estimate->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$admin_estimate->setData('issetPostLoginAndNotLogged', "ok");
		$admin_estimate->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$admin_estimate->setData('issetPostLoginAndNotLogged', "");
	}
	$admin_estimate->setData('scriptName', $ScriptName);
	$admin_estimate->setData('translateLogin',Dictionnary::translate('login'));
	$admin_estimate->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$admin_estimate->setData('img', "acceder_compte.png");
		$admin_estimate->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$admin_estimate->setData('img', "poursuivre.png");
		$admin_estimate->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

/**************************************************
 ****************ADMIN_ORDER.TPL*******************
 **************************************************/

$FormName = 'adm_estim';


/**************************************************
 ***************ADMIN_SEARCH.TPL*******************
 **************************************************/

$Arr_LastYear = getdate(mktime(0,0,0,date('m'),date('d'),date('Y'))); // back in time one year
$Arr_Today = getdate(); // today

$admin_estimate->setData('ScriptName',$ScriptName); 
$admin_estimate->setData('FormName',$FormName);
 
// Si l'utilisateur est identifié
if( Session::getInstance()->getCustomer() ) 
{
	if( isset($_POST['search']) ) $search = isset($_POST['search']) ? $_POST['search'] : '';
	
	$TableNameParent = 'order';
	$TableNameChild = 'order_row';
	$FieldId = 'idorder';
	$admin_estimate->setData('IdBuyer',Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0);
	
}

//Liste déroulante depuis
$select_from ='<select name="minus_date_select" class="AuthInput">
			<option value="-2">'.Dictionnary::translate("order_2days").'
					<option value="-7">'.Dictionnary::translate("order_1week").'
					<option value="-14">'.Dictionnary::translate("order_2weeks").'
					<option value="-30">'.Dictionnary::translate("order_1month").'
					<option value="-365">'.Dictionnary::translate("order_1year").'
				</select>';

$admin_estimate->setData('select_from',$select_from);

// Liste déroulante affichant les 12 derniers mois par ordre décroissant
$dat = getdate();
$mon = $dat['mon'] - 1;
$year = $dat['year'] ;
$months = array (
				1 => 'Janvier',
				2 => 'F&eacute;vrier',
				3 => 'Mars',
				4 => 'Avril',
				5 => 'Mai',
				6 => 'Juin',
				7 => 'Juillet',
				8 => 'Ao&ucirc;t',
				9 => 'Septembre',
				10 => 'Octobre',
				11 => 'Novembre',
				12 => 'D&eacute;cembre'
				);
				
$select_month='<select name="yearly_month_select" class="AuthInput">';

for ( $i = $mon + 12 ; $i > $mon ;  $i--){ //parse one year
	$j = ( ( ( $i-1) % 12 + 1 )  ); // the month
	if ($j == 12) {
		$year--;
		$j = 0;
	}
	
	$select_month.='<option value="'.++$j.'">'.$months[$j].' '.$year.'</option>';
	
	$j++;
}
$select_month.='</select>';

$admin_estimate->setData('select_month',$select_month);

$admin_estimate->setData('LastYearmday',$Arr_LastYear['mday']);
$admin_estimate->setData('LastYearmon',$Arr_LastYear['mon']);
$admin_estimate->setData('LastYearyear',$Arr_LastYear['year']);

$admin_estimate->setData('Todaymday',$Arr_Today['mday']);
$admin_estimate->setData('Todaymon',$Arr_Today['mon']);
$admin_estimate->setData('Todayyear',$Arr_Today['year']);

$admin_estimate->setData('adm_estim',1);

$admin_estimate->setData('NoResults',1);


//-----------------------------------------------------formulaire de recherche-------------------------------------//

global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
	$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime() - 30*3600*24),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( ) )
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
	{
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
	}
?>
		
			
			
			<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
			/* ]]> */
			</style>
			<?php
			$form = "";
			
			$form .= '<form action="/catalog/admin_grouping_estimate.php?all" method="post" id="SearchForm">
				
					<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />
						<div>
						<div class="tableContainer">
							<table class="dataTable" width="100%">';
								
			$form .='<tr>
						<th class="filledCell">Entre le</th>
							<td>
								<input type="text" name="start_date" id="start_date" class="calendarInput" value='. $postData[ "start_date" ] .' />';
								
			$form .= '<a href="javascript:calendar(start_date)" id="test_a" ></a>';
					$displayIcon = true; $calledFunction = false;
									
			$form .= '</td>';
							
			$form .='<th class="filledCell">Et le</th>
					<td>
						<input type="text" name="end_date" id="end_date" class="calendarInput" value='. $postData[ "end_date" ] .' />';
						
			$form .= '<a href="javascript:calendar(end_date)" id="test_b" ></a>';
								
			$form .= '</table>
					</div>
					<div class="submitButtonContainer" style="float:right;padding-right:200px;margin-bottom:10px;">
						<input type="submit" class="blueButton"  id="idRechercher" value="Rechercher" />
					</div>
				</div>
				<input type="hidden" name="detail" id="detail" value="1" />
			</form>';

			$admin_estimate->setData( "form",$form );

//-----------------------------------------------------formulaire de recherche-------------------------------------//



if( !empty( $_REQUEST["search"]) || !empty( $_REQUEST["search_x"] ) || isset( $_REQUEST[ "all" ] ) ){
	
	if( isset( $_REQUEST[ "all" ] ) ){
		
		$query ="";
		if(isset($_POST["list_buyer"]) && !empty($_POST["list_buyer"]))
		{
		
		$nb_buyer = count($_POST["list_buyer"]);
		
			if($nb_buyer == 1)
			{
				$query.= "AND b.idbuyer = '". $_POST["list_buyer"][0] ."'";
			
			}
			else
			{
			
				$query.= "AND ( b.idbuyer = '". $_POST["list_buyer"][0] ."'";
			
					for ($i=1;$i<count($_POST["list_buyer"])-1;$i++)
					{
					$query.= " OR b.idbuyer = '". $_POST["list_buyer"][$i] ."'";
				
					}
			
			$query.= " OR b.idbuyer = '". $_POST["list_buyer"][count($_POST["list_buyer"])-1] ."')";
				
			}
		
		
		}
		
		//---------------------------------------Recherche sur la date-----------------------------------//
		if (isset ($_POST["start_date"]))
		$start = $_POST["start_date"];
		else
		$start =$postData[ "start_date" ];
		
		$start = explode("-", $start);
		
		$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
		if(isset($_POST["end_date"]))
		$end = $_POST["end_date"];
		else
		$end =$postData[ "end_date" ];
		
		$end = explode("-", $end);
		
		$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
		
		
		$query .= " AND (es.DateHeure BETWEEN '$start_date' AND '$end_date' ) ";
		
		
		
		$idgrouping = $GroupingCatalog->getGrouping( "idgrouping" )? $GroupingCatalog->getGrouping( "idgrouping" ) : 0;
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$query = "
		SELECT es.idestimate, b.company,
		b.idbuyer
		FROM estimate es,
			 buyer b
		WHERE es.idbuyer = b.idbuyer
		AND b.idgrouping = $idgrouping
		AND es.`status` IN ( 'ToDo', 'InProgress', 'Send', 'Ordered' )
		".$query."
		ORDER BY es.DateHeure DESC";

		$rs =& DBUtil::query( $query );
				
	}
	else include($GLOBAL_START_PATH.'/catalog/admin_search.inc.php');
	
	if( $rs->RecordCount() )
		$admin_estimate->setData('NoResults',$rs->RecordCount() ?  false : true);

	$array_estimate = array();
	
	$mois = array("","janvier","f&eacute;vrier","mars","avril","mai","juin","juillet","ao&ucirc;t","septembre","octobre","novembre","d&eacute;cembre");
	
	
	for($i=0;$i<$rs->RecordCount();$i++){
	
		$estimate = new Estimate( $rs->fields( "idestimate" ) );

		$array_estimate[$i]["i"] 			= $i;
		$array_estimate[$i]["idestimate"] 	= $estimate->getId();
		$array_estimate[$i]["idbuyer"] 		= $estimate->get( "idbuyer" );
		$array_estimate[$i]["company"] = GenerateHTMLForToolTipBox($estimate->getId(), Util::doNothing($rs->fields("company")));
		$array_estimate[$i]["iddelivery"] 	= $estimate->get( "iddelivery" );
		
		list($datedev,$heure)=explode(" ", $estimate->get('DateHeure') );
		list($y,$m,$d)=explode("-",$datedev);
		$dateorder = $d." ".$mois[intval($m)]." ".$y;
		
		$array_estimate[$i]["DateHeure"] 	= GenerateHTMLForToolTipBox($estimate->getId(),$dateorder);
		
		$Str_Date_Valid = $estimate->get('valid_until');
		if($Str_Date_Valid=='0000-00-00')
			$Str_Date_Valid='-';
		else
			$Str_Date_Valid=usDate2eu($estimate->get('valid_until'));
		$array_estimate[$i]["valid_until"] = $Str_Date_Valid;
			
		$status = $estimate->get('status');
		if($status=="ToDo")
			$status ="InProgress";
		$array_estimate[$i]["status"] = Util::doNothing(GenerateHTMLForToolTipBox($estimate->getId(),Dictionnary::translate( $status ) ));
		$array_estimate[$i]["total_amount_ht"] = GenerateHTMLForToolTipBox($estimate->getId(), $estimate->getTotalET() > 0.0 ? Util::priceFormat($estimate->getTotalET()) : "-");
		$array_estimate[$i]["total_amount"] =  GenerateHTMLForToolTipBox($estimate->getId(),$estimate->getTotalATI() > 0.0 ? Util::priceFormat($estimate->getTotalATI()) : "-");
		
		$color = false;
		if($status == "InProgress") {
			$color = true;
		}
		$array_estimate[$i]["color"] 	= $color;
		
		$rs->MoveNext();
		
	}
	
	$admin_estimate->setData('array_estimate',$array_estimate);
		
}	
$admin_estimate->output();



function GenerateHTMLForToolTipBox($idestimate, $value=0){
	
	global $GLOBAL_START_URL;
	
	$query = " SELECT idarticle,
				reference,
				summary,
				delivdelay,
				quantity,
				unit_price,
				ref_discount,
				discount_price
				FROM  `estimate_row` 
				WHERE
				idestimate = '". $idestimate ."'
				
	
			";	
	
	$rs = & DBUtil::query( $query );
	
		
	$html = ' <link type="text/css" rel="stylesheet" href="'.$GLOBAL_START_URL.'/css/catalog/contents.css" />';
	$html .= "<span onmouseover=\"document.getElementById('pop$idestimate').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idestimate').className='tooltip';\" style=\"position:relative; display:block;\">" .$value." ";
	
		
	$html .= "<span id='pop$idestimate' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">";

		
		$html .= "<div style=\"font-size: 10px;\">";
		$html.= "<table>
				<tr>
					<th><span style=\"font-size: 12px;\">Icone</th>
					<th><span style=\"font-size: 12px;\">R&eacute;f&eacute;rence</th>
					<th><span style=\"font-size: 12px;\">D&eacute;signation</th>
					<th><span style=\"font-size: 12px;\">D&eacute;lai de livraison</th>
					<th><span style=\"font-size: 12px;\">Prix TTC</th>
					<th><span style=\"font-size: 12px;\">Remise</th>	
					<th><span style=\"font-size: 12px;\">Prix remis&eacute; TTC</th>
					<th><span style=\"font-size: 12px;\">Quantit&eacute;</th>
					<th><span style=\"font-size: 12px;\">Prix total TTC</th>

				</tr>";
				
				
				 while( !$rs->EOF ){
				$Icon = URLFactory::getReferenceImageURI( $rs->fields("idarticle") );;
				$reference = $rs->fields("reference");
				$summary = $rs->fields("summary");
				$delivdelay = DBUtil::getDBValue( "delay_1", "delay", "iddelay", $rs->fields("delivdelay") );
				$unit_price = Util::priceFormat($rs->fields("unit_price"));
				$quantity = $rs->fields("quantity");
				$ref_discount = ($rs->fields("ref_discount") > 0) ? Util::rateFormat($rs->fields("ref_discount")) : "-" ;
				$discount_price = Util::priceFormat($rs->fields("discount_price"));
				$total = Util::priceFormat($discount_price * $quantity);

				 
				 $html .="
				 <tr>
				 <td>
				 <img src='". $Icon ."' width='40' alt='Icone'>
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $reference ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$summary."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $delivdelay ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $unit_price ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $ref_discount ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$discount_price."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$quantity."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $total ."
				 </td>
				 </tr>";
				 
				 $rs->MoveNext();
	}
				 
		 
				 
	$html.="			 </table>
				
				";
		
		
		
		$html .= "<br>";
		
	
	$html .= "
</div>
</div>
</div>
</span>
</span>";
	
	return $html;
}

?>
<script type="text/javascript">
$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_a').click();
});

$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_b').click();
});

</script>
<script type="text/javascript">

/* <![CDATA[ */
function calendar(param){
$(document).ready(function() { 

	$(param).datepicker({
		buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		showAnim: 'fadeIn',
		showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
		onClose: function(){
			<?php echo $calledFunction ?>( this );
		}
		<?php } ?>
	});
	
	
		
});
}
/* ]]> */
</script>