<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf commande client
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Order.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFOrder.php" );

$odf = new ODFOrder( intval( $_REQUEST[ "idorder" ] ) );

$status = DBUtil::getDBValue( "status", "order", "idorder", intval( $_REQUEST[ "idorder" ] ) );

switch( $status ){

	case Order::$STATUS_DEMAND_IN_PROGRESS:
	case Order::$STATUS_TODO:
	case Order::$STATUS_SENT:
	case Order::$STATUS_IN_PROGRESS:
		
		$odf->exportPDF( "Bon de commande n° " . intval( $_REQUEST[ "idorder" ] ) . ".pdf" );
		break;

	case Order::$STATUS_DELIVERED:
	case Order::$STATUS_ORDERED:
	case Order::$STATUS_PICKING:
	case Order::$STATUS_PAID:
 	case Order::$STATUS_TO_PAY:
	case Order::$STATUS_INVOICED:
	case Order::$STATUS_CANCELLED:
	case Order::$STATUS_REFUSED:
	default:
		
		$odf->exportPDF( "Confirmation de commande n° " . intval( $_REQUEST[ "idorder" ] ) . ".pdf" );
		break;
		
}

?>