<?php

 /**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage des infos du client
 */

include_once( "../objects/classes.php" );  

require_once( "HTML/Template/Flexy.php" );
require_once( "HTML/Template/Flexy/Element.php" );

$isTemplate = true;

include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
include_once( "$GLOBAL_START_PATH/catalog/addresses.inc.php" );

include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");
header( "Content-Type: text/html; charset=utf-8" );
class BuyerInfosController extends PageController{};
$buyer_infos = new BuyerInfosController( "buyer_infos.tpl" , "Informations Client" );

$ScriptName = 'buyer_infos.php';

//---------------------------------------------//
//-------------------account-------------------//
//---------------------------------------------//
if( Session::getInstance()->getCustomer() ) {
//if( Session::getInstance()->islogin() > 0 ) {

	$buyer_infos->setData('isLogin', "ok");
	
	$GroupingCatalog = new GroupingCatalog();
	
	$buyer_infos->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	
	$buyer_infos->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$buyer_infos->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$buyer_infos->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	
	$iduser  = DBUtil::getParameter('contact_commercial');
	$salesman = new Salesman( $iduser );

	$buyer_infos->setData( "contact_lastname", $salesman->get("lastname") );
	$buyer_infos->setData( "contact_firstname", $salesman->get("firstname") );
	$buyer_infos->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$buyer_infos->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$buyer_infos->setData( "contact_email", $salesman->get("email") );
	
	$buyer_infos->setData( "isBuyerInfos", true );
	
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$buyer_infos->setData( "contact_grouping", $contact_grouping );
	
} else {

	$buyer_infos->setData('isLogin', "");
	$FromScript = $ScriptName;

}

$buyer_infos->setData('scriptname', $ScriptName);
$buyer_infos->setData('fromscript', $FromScript);

$buyer_infos->setData('translations',$translations);


//récupérer les champs de la table contact
$idbuyer = Session::getInstance()->getCustomer()->getId();

$q = "
		SELECT title,lastname,firstname,mail,phonenumber,gsm,faxnumber,iddepartment
		FROM contact 
		WHERE idbuyer = '$idbuyer' 
		 
		";
	
		$rs = &DBUtil::query( $q );
	$title =  $rs->fields( "title" );	
	$lastname = $rs->fields( "lastname" );
	$firstname = $rs->fields( "firstname" );
	$mail = $rs->fields( "mail" );
	$phonenumber = $rs->fields( "phonenumber" );
	$gsm = $rs->fields( "gsm" );
	$faxnumber = $rs->fields( "faxnumber" );
	$department = $rs->fields( "iddepartment" );
	
	

//---------------------------------------------//
//-----------------Buyer infos-----------------//
//---------------------------------------------//

$buyerInfos = array( 
	
	"company" 			=> "",
	"idactivity" 		=> "",
	"adress" 			=> "",
	"adress_2"	 		=> "",
	"zipcode" 			=> "",
	"city" 				=> "",
	"idstate" 			=> "",
	"phone_standard"	=> "",
	"idmanpower" 		=> "",
	"tva" 				=> "",
	"siret" 			=> "",
	"siren"				=> "",
	"naf"				=> "",
	"logo"				=> "",
	"buyer_info1"		=> "",
	"buyer_info2"		=> "",
	"buyer_info3"		=> "",
	"buyer_info4"		=> "",
	"buyer_info5"		=> "",
	"buyer_info6"		=> "",
	"buyer_info7"		=> "",
	"buyer_info8"		=> "",	
	"buyer_info9"		=> "",
	"buyer_info10"		=> "",	
	"idcustomer_profile"=> "",
	// "profile"			=> ""
	
);

//---------------------------------------------//
//-----------------Contact infos-----------------//
//---------------------------------------------//


$contactInfos = array( 
	"title"				=> "",
	"lastname" 			=> "",
	"firstname" 		=> "",
	"email" 			=> "",
	"phonenumber" 		=> "",
	"gsm" 				=> "",
	"faxnumber"		 	=> "",
	"iddepartment"		=> ""
);	
$contactInfos["title"] = $title;
$contactInfos["lastname"] = Util::checkEncoding($lastname);
$contactInfos["firstname"] = Util::checkEncoding($firstname);
$contactInfos["email"] = $mail;
$contactInfos["phonenumber"] = $phonenumber;
$contactInfos["gsm"] = $gsm;
$contactInfos["faxnumber"] = $faxnumber;
$contactInfos["iddepartment"] = $department;

$_POST 	= Util::arrayMapRecursive( "Util::doNothing", $_POST );
foreach( $buyerInfos as $field => $value )
	$buyerInfos[ $field ] = isset( $_POST[ $field ] ) ? stripslashes( $_POST[ $field ] ) : Session::getInstance()->getCustomer()->get( $field );

include_once( dirname( __FILE__ ) . "/../objects/XHTMLFactory.php" );

$contactInfos[ "title" ] = XHTMLFactory::createRadioGroup( "title", "idtitle", "title_1", "idtitle", $contactInfos[ "title" ], "name='title'", true );

$contactInfos[ "department" ] = DrawLift_2( "department", $contactInfos[ "iddepartment" ] );
$buyerInfos[ "activity" ] = DrawLift_3( "activity", $buyerInfos[ "idactivity" ] );
$buyerInfos[ "state" ] = DrawLift( "state", $buyerInfos[ "idstate" ] );
$buyerInfos[ "manpower" ] = DrawLift_5( $buyerInfos[ "idmanpower" ] );

$buyerInfos[ "isBuyer" ] = ($buyerInfos[ "idcustomer_profile" ] == 1 ) ? true : false;
$buyerInfos[ "profile" ] = profileSelectField(stripslashes( $_POST[ "profile" ] ));


$buyer_infos->setData('contactInfos',$contactInfos);
$buyer_infos->setData('buyer_infos',$buyerInfos);
$buyer_infos->setData('showRIBAttachment',showRIBAttachment('$idbuyer'));


if( Session::getInstance()->getCustomer() ) {

	/**
	 * Enregistrement des adresses de livraison et de facturation
	 * Mise à jour du basket
	 */
	
	//Traitement buyer
	if(isset($_POST['user_changed'])){
		
		$validatePostData = empty( $err_msg );
		
		//validation des données
	
		include_once( "Validate.php" ); //classe PEAR
		include_once( "Validate/FR.php" ); //classe PEAR
		
		//champs sans espaces
		$_POST[ "lastname" ] 	= str_replace( " ", "", $_POST[ "lastname" ] );
		$_POST[ "phonenumber" ] 	= str_replace( " ", "", $_POST[ "phonenumber" ] );
		$_POST[ "faxnumber" ]		= str_replace( " ", "", $_POST[ "faxnumber" ] );
		$_POST[ "gsm" ]				= str_replace( " ", "", $_POST[ "gsm" ] );
		$_POST[ "zipcode" ]			= str_replace( " ", "", $_POST[ "zipcode" ] );
		$_POST[ "phone_standard" ]	= str_replace( " ", "", $_POST[ "phone_standard" ] );
		$_POST[ "tva" ] 			= str_replace( " ", "", $_POST[ "tva" ] );
		$_POST[ "siret" ] 			= str_replace( " ", "", $_POST[ "siret" ] );
		$_POST[ "naf" ] 			= str_replace( " ", "", $_POST[ "naf" ] );
			
		//vérification des données
		$error_msg = "";
		
					
		$validatePostData = empty( $error_msg );
	
		if( $validatePostData ){
		$lastupdate = date( 'Y-m-d H:i:s' );
		
		//récupérer les données du client en cours

				$customer = Session::getInstance()->getCustomer();
				
				$UserInfos[ 'idbuyer' ] = $idbuyer;

	//recupérer le login du client en cours	

		$query = "
		SELECT login 
		FROM user_account 
		WHERE idbuyer = '$idbuyer' 
		AND idcontact = '0' 
		LIMIT 1";
	
		$rs = &DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		trigger_error( "Impossible de récupérer le nom d'utilisateur", E_USER_ERROR );
		
	$login = $rs->fields( "login" );
	
	
	//remplir les informations relative  à l'entreprise
	$Fields = array(
		
		'company'				=> $_POST[ 'company' ],
		'idactivity'			=> $_POST[ 'idactivity' ],
		'adress'				=> $_POST[ 'adress' ],
		'adress_2'				=> $_POST[ 'adress_2' ],
		'zipcode'				=> $_POST[ 'zipcode' ],
		'city'					=> strtoupper( $_POST[ 'city' ] ),
		'idstate'				=> $_POST[ 'idstate' ],
		"phone_standard"		=> $_POST[ 'phone_standard' ],
		"tva" 					=> $_POST[ 'tva' ],
		'siret'					=> $_POST[ 'siret' ],
		'naf'					=> $_POST[ 'naf' ],
		'idsource'				=> 1,
		'newsletter'			=> 0,
		// 'username'				=> $login,
		'username'				=> $_POST[ 'email' ],
		'buyer_info1'			=> '',
		'buyer_info2'			=> '',
		'buyer_info3'			=> '',
		'buyer_info4'			=> '',
		'contact'				=> 0,
		'lastupdate'			=> $lastupdate,
		'create_date'			=> date( 'Y-m-d H:i:s' ),
		'profile'				=> $_POST[ 'profile' ],
		
	);
	
	DBUtil::query("UPDATE `user_account` SET login='".$_POST[ 'email' ]."' WHERE idbuyer=$idbuyer");
	
	if( isset( $UserInfos['newsletter'] ) )
		$Fields[ 'newsletter' ] = $UserInfos[ 'newsletter' ];
			
	$customer = Session::getInstance()->getCustomer();
	
	foreach( $Fields as $fieldName => $fieldValue ){
		
		$customer->set( $fieldName , $fieldValue );
		
	}
	
		
	//remplir les informations relatives au client	
	$FieldsContact = array(
		
		'title'			=>  $_POST[ 'title' ],
		'firstname'		=> ucfirst(strtolower( $_POST[ 'firstname' ] ) ),
		'lastname'		=> ucfirst(strtolower( $_POST[ 'lastname' ] ) ),
		'adress'		=> $_POST[ 'adress' ],
		'mail'			=> $_POST[ 'email' ],
		'idfunction'	=>$_POST[ 'idfunction' ],
		'iddepartment'	=> $_POST[ 'iddepartment' ],
		'phonenumber'	=> $_POST[ "phonenumber" ],
		'gsm'			=> $_POST[ 'gsm' ],
		'faxnumber'		=> $_POST[ 'faxnumber' ],
		// 'username'		=> $login,
		'username'		=> $_POST[ 'email' ],
		
		'lastupdate'	=> $lastupdate
	);
	
		foreach( $FieldsContact as $contactField => $contactValue )
		
			{	
		
			$customer->getContact()->set( $contactField , $contactValue );
		
			}
	
		$customer->save();
			
			
			//Upload du RIB
			if( isset( $_FILES[ "RIBAttachment" ][ "tmp_name" ]) && !empty( $_FILES[ "RIBAttachment" ][ "tmp_name" ] ) ) {
				
				include_once( "$GLOBAL_START_PATH/objects/FileUploader.php" );
				$path = "$GLOBAL_START_PATH/data/rib/$buyerid";
				
				if( !file_exists( $path ) )
					@mkdir( $path );
					
				@chmod( $path, 0755 );
				
				$fileUploader = new FileUploader( "RIBAttachment", "$path", $_FILES[ "RIBAttachment" ][ "name" ] );
				$fileUploader->setOverrideExistingFile( true );
				$fileUploader->upload();

			}
			
			//Upload du logo
			if( isset( $_FILES[ "logo" ][ "tmp_name" ]) && !empty( $_FILES[ "logo" ][ "tmp_name" ] ) ) {
				
				include_once( "$GLOBAL_START_PATH/objects/ImageUploader.php" );
				
				$imageUploader = new ImageUploader( "logo", "$GLOBAL_START_PATH/www/logo_buyer/", "logo_$buyerid.jpg" );
				$imageUploader->setOverrideExistingFile();
				$imageUploader->upload();

				$query = "UPDATE buyer SET logo = '/www/logo_buyer/logo_$buyerid.jpg' WHERE idbuyer = '$buyerid' LIMIT 1";
				$db =& DBUtil::getConnection();
				$rs = $db->Execute( $query );
			}
			
			//$message .= Dictionnary::translate("infos_modified");
			$message .= "Informations modifi&eacute;es";
			//On redirige pour recharger l'objet  
			header('Location: /catalog/buyer_infos.php?confMaj');   
			exit();
			
		
		}else{
		
			$message = "<p class=\"msgError\">";
			$message .= $error_msg;
			$message .= "</p>";
			
		}
		
		
	}
	
}

if ( isset( $_GET["confMaj"] )) {
	//$message = Dictionnary::translate("infos_modified");
	$message .= "Informations modifi&eacute;es";
}

if( !isset( $message ) )
	$message = "";

$buyer_infos->setData('message', $message);
$buyer_infos->output();


//Retourne les RIBs d'un client donné $key
function showRIBAttachment( $key ){
	
	global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	$path = "$GLOBAL_START_PATH/data/rib/$key";
	
	if( !file_exists( $path ) || !is_dir( $path ) )
		return;
	
	$dir = opendir( $path );
	
	$files = array();
	while( $file = readdir( $dir ) ){
		
		if( $file != "." && $file != ".." )
			$files[] = $file;
			
	}
	
	if( !count( $files ) ){
	
		echo Dictionnary::translate( "gest_com_no_attachment_found" );
		return;
		
	}


	$return = array();
	$i = 0;
	foreach( $files as $file ){
		$return[$i]["name"] = htmlentities( $file );
		$return[$i]["link"] = $GLOBAL_START_URL."/data/rib/".$key."/".str_replace( "+", "%20", urlencode( $file ) );
		
		$i++;		
	}
	
	return $return;

}

function profileSelectField($idprofile)
{
	if (!$idprofile)
		$selected = (int)Session::getInstance()->getCustomer()->get( 'profile' );
	else
		$selected = (int)$idprofile;
	
	$selectField = '<select name="profile">';
	$selectField .= '<option value="0"'.($selected == 0 ? ' selected="selected"' : '').'></option>';
	$selectField .= '<option value="1"'.($selected == 1 ? ' selected="selected"' : '').'>Professionnel</option>';
	$selectField .= '<option value="2"'.($selected == 2 ? ' selected="selected"' : '').'>Association</option>';
	$selectField .= '<option value="3"'.($selected == 3 ? ' selected="selected"' : '').'>Administration</option>';
	$selectField .= '</select>';
	return $selectField;
}
?>
