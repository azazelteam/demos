<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des produits en gondole
 */

//gondole.php
$NbGondole=5;
$GondoleFile="../administration/gondole/gondole.dat";

function Gondole($Position,&$GondoleInfoArray,$Langue,$File="")
{
	global $NbGondole,$GondoleFile;
         
	$Result=0;

	if(!empty($File)) $GondoleFile=$File;

	$GondoleArray = &LoadGondole($NbGondole,$GondoleFile);

	if($Position-1 <= count($GondoleArray) && $Position>0)
	{
		$IdProduct=$GondoleArray[$Position-1];
	}

	if( !empty($IdProduct))
	{
		$Base = &DBUtil::getConnection();
		$rs=& DBUtil::query("select product.idproduct,idproduct_family,idsupplier,name$Langue as name,description$Langue as description,available,novelty, sellingcost from product, detail where idproduct='$IdProduct' AND product.idproduct=detail.idproduct");
		if($rs->RecordCount())
		{
			$GondoleInfoArray = $rs->fields ;
			
			//prix retourné pas internationalisé -> passer par un object PRODUCT

			$Result=1;
		}
	}

	return $Result;
}

function LoadGondole($Nb,$File)
{
	$GondoleArray = array();
	if(file_exists($File))
	{
		$fp=file($File);
		$n=min(count($fp),$Nb);
		for($i=0 ; $i < $n ; $i++)
		{
			$fp[$i]=str_replace(chr(13),"",$fp[$i]);
			$fp[$i]=str_replace(chr(10),"",$fp[$i]);
			$GondoleArray[]=$fp[$i];
		}
	}
	else
	{	echo "Fichier $File inexistant ! <br />";}
	return $GondoleArray;
}

?>