<?php
/**
 * Traitement + Affichage de la 1ère étape du devis
 * @version 	CVS: $Id: deprecated_estimate_basket.php,v 1.2 2010-11-25 09:01:30 foxdie Exp $
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once('../objects/Basket.php');
include_once('../script/global.fct.php');

include_once( dirname( __FILE__ ) . "/../objects/flexy/WebPageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );

/* ajout au panier */

if( isset( $_REQUEST[ "idarticle" ] ) && isset( $_REQUEST[ "quantity" ] ) ){
	
	if( is_array( $_REQUEST[ "idarticle" ] ) && is_array( $_REQUEST[ "quantity" ] ) ){
	
		$i = 0;
		while( $i < count( $_REQUEST[ "idarticle" ] ) ){
			
			Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "idarticle" ][ $i ] ), $_REQUEST[ "quantity" ][ $i ] );
			$i++;
			
		}
		
	}
	else Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "idarticle" ] ), $_REQUEST[ "quantity" ] );;
	
}

/* --------------------------------------------------------------------------------------------------- */
/* multisites : redirection pour les sites satellites */

if( file_exists( dirname( __FILE__ ) . "/../config/multisite.xml" ) ){

	header( "Location: /catalog/estimate_multisite.php" );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------- */

//important pour les popups
header('Content-Type: text/html; charset=utf-8'); 

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketItemProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CCategoryProxy.php" );

class EstimateBasketController extends WebPageController{};


$estimate_basket_page = new EstimateBasketController( "estimate_basket.tpl" , "Devis (panier)" );

$estimate_basket_page->setData( "category", new CCategoryProxy( new CCategory( 0 ) ) );

$items = array();
$it = Basket::getInstance()->getItems()->iterator();

while( $it->hasNext() )
	array_push( $items, new BasketItemProxy( $it->next() ) );
	
$estimate_basket_page->setData( "basketItems", $items );

$lang = "_1";

//-----------------------------------------------------------------------------------------------------------------

if( isset( $_GET[ "next" ] ) && ( isset( $_POST[ "GoToCata_x" ] ) || isset( $_POST[ "CatBack_x" ] ) ) ){
	
	Basket::getInstance()->save();
	header( "Location:$GLOBAL_START_URL" );
	
}

$BuyingScript = "estimate_complete.php";
$ScriptName = "estimate_basket.php";

$estimate_basket_page->setData( "buyingscript", "estimate_complete.php" );
$estimate_basket_page->setData( "scriptname", "estimate_basket.php" );
$estimate_basket_page->setData( "fromscript", basename( $_SERVER[ "PHP_SELF" ] ) );

//---------------------------------------------//
//--------------Google conversion--------------//
//---------------------------------------------//

$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_conversion_id = DBUtil::getParameterAdmin( "google_estimate_conversion_id" );


if( $google_conversion_id && $UseGoogleConversion ){
	$estimate_basket_page->setData('google',1);
	$estimate_basket_page->setData('google_conversion_id',$google_conversion_id);  
}else{
	$estimate_basket_page->setData('google',0);
}

//----------------------------------------------------------------------------------------------
//ajout d'articles

/**
 * @todo :
 * => plus de IdRealProduct, préférer idarticle
 * => les seules variables nécéssaires dans le formulaire de demande de devis/cde sont : idarticle, quantity, idcolor et idtissus
 */


if( isset( $_POST[ "EstimateSelection" ] ) || isset( $_POST[ "EstimateSelection_x" ] ) ){
	
	deprecated_createBasketFromSelection();
	
}
else if( isset( $_REQUEST[ "IdArticle" ] ) ){
	
	$qtt = $_POST[ "quantity" ];
   	
    if( is_array( $_REQUEST[ "IdArticle" ] ) ) {
		
    	if( !empty( $_REQUEST[ "estimate" ] ) ) {
    		
    		foreach( $_REQUEST[ "estimate" ] as $k => $order ) {
    			
				Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "IdRealProduct" ][ $k ] ), $_REQUEST[ "quantity" ][ $k ] );
				
    		}
    		
    	}
    	
    }else{
    	$compte = Basket::getInstance()->getItemCount();
    	$pos	 = 0 ;
    	
    	$idcloth = isset( $_REQUEST[ "idtissus" ] ) ? $_REQUEST[ "idtissus" ] : 0;
	    $idcolor = isset( $_REQUEST[ "idcolor" ] ) ? $_REQUEST[ "idcolor" ] : 0;
	    $modify = false;
	    
		for($i = 0 ; $i < $compte ; $i++){
    		if($_REQUEST[ "IdArticle" ] == Basket::getInstance()->getItemAt($i)->getArticle()->get("idarticle") ){
    			$modify = true;
    			$pos = $i;
    		}
    	}
    	if($modify===true){
    		$_POST[ "qtty_$pos" ] = $qtt + Basket::getInstance()->getItemAt($pos)->getQuantity();
    		Basket::getInstance()->save();
    	}else{
			Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "IdArticle" ], $idcolor, $idcloth ), $qtt );
    	}
    }
    
	if(isset($_REQUEST['IdArticle'])){
		if(isset($_REQUEST['fromScript']) && $_REQUEST['fromScript']!="" ){
			$url_redirect = $_REQUEST['fromScript'];
			$_SESSION["idproduit"] = $_REQUEST['IdProduct'];

		} else if(!isset($_REQUEST['fromAccess'])){
			$url_redirect = URLFactory::getProductURL(DBUtil::getDBValue( "idproduct", "detail", "idarticle", $_REQUEST[ "IdArticle" ] ) );

		}
	}else if(isset($_REQUEST['checkArticle'])){
		$url_redirect = $GLOBAL_START_URL."/catalog/composition.php";
	}
	
	if(isset($_POST["fromAccess"]) && $_POST["fromAccess"]==1 ){
		$url_redirect = $GLOBAL_START_URL."/catalog/estimate_basket.php";
	}
	
	$hide_template = DBUtil::getParameterAdmin( "hide_direct_basket" );
	
	if($hide_template){
		header( "Location:$url_redirect");
		exit;
	}
}

// Ajouts de plusieurs articles en meme temps (groupement)
if( isset($_REQUEST['IdArticles']) ){
	
	$IdArticles = $_REQUEST['IdArticles'];
    
    for($i=0;$i<count($IdArticles);$i++){
    	
    	$IdArt = $IdArticles[$i];
   		$qtt = $_POST["quantity_$IdArt"];
   
				
		Basket::getInstance()->addArticle( new CArticle( $IdArt ),$qtt);
				
    }

}

//----------------------------------------------------------------------------------------------
//modifications des quantités

if( isset( $_POST[ "Modify" ] ) || isset( $_POST[ "Modify_x" ] ) || isset( $_POST[ "Buy" ] ) || isset( $_POST[ "Buy_x" ] ) || ( isset( $deleteItem ) && $deleteItem ) )
	Basket::getInstance()->save();
	
//----------------------------------------------------------------------------------------------
//suppression d'article

$i = Basket::getInstance()->getItemCount() - 1;
while( $i >= 0 ){
	
	if( isset( $_POST[ "Delete_$i" ] ) || isset( $_POST[ "Delete_" . $i . "_x" ] ) )
		Basket::getInstance()->removeItemAt( $i );
	
	$i--;
	
}

//----------------------------------------------------------------------------------------------

$baseURL = $GLOBAL_START_URL;

//---------------------------------------------//
//-------------------Account-------------------//
//---------------------------------------------//
if( Session::getInstance()->getCustomer() ) {
	
	$estimate_basket_page->setData( "isLogin", "ok" );
	
	$estimate_basket_page->setData( "buyer", Session::getInstance()->getCustomer()->getId() );
	$estimate_basket_page->setData( "buyerLogin", Session::getInstance()->getCustomer()->getContact()->getEmail() );
	
} else {
	
	$estimate_basket_page->setData( "isLogin", "" );
	$com_step2 = true;
	
	if( empty( $com_step2 ) )
		$estimate_basket_page->setData( "com_step_empty", "ok" );
	else
		$estimate_basket_page->setData( "com_step_empty", null );
	
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		
		$estimate_basket_page->setData( "issetPostLoginAndNotLogged", "ok" );
		$estimate_basket_page->setData( "translateMSGErrorLoginPassword", Dictionnary::translate( "MSGErrorLoginPassword" ) );
		
	} else {
		
		$estimate_basket_page->setData( "issetPostLoginAndNotLogged", "" );
		
	}
	
	$estimate_basket_page->setData( "translateLogin", Dictionnary::translate( "login" ) );
	$estimate_basket_page->setData( "translatePassword", Dictionnary::translate( "password" ) );
	
}

//---------------------------------------------//
//----------Code spécifique à la page----------//
//---------------------------------------------//
$basket_artcount = Basket::getInstance()->getItemCount();
$estimate_basket_page->setData( "basket_artcount", $basket_artcount );

$message = Dictionnary::translate( "empty_basket_estimate" );
$estimate_basket_page->setData( "message", $message );

$basket_article = array();
$hasDiscount = "";

for( $i = 0 ; $i < Basket::getInstance()->getItemCount() ; $i++ ){

	$idarticle = Basket::getInstance()->getItemAt($i)->getArticle()->getId();
	$stock_level = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $idarticle );
	
	$basket_article[ $i ][ "productname" ]		= DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct );
	$basket_article[ $i ][ "productURL" ]		= URLFactory::getProductURL( $idproduct );
						
	$basket_article[ $i ][ "i" ]				= $i;
	$basket_article[ $i ][ "idarticle" ]		= Basket::getInstance()->getItemAt( $i)->getArticle()->getId();
	$basket_article[ $i ][ "icon" ]				= URLFactory::getReferenceImageURI( Basket::getInstance()->getItems()->get( $i )->getArticle()->get( "idarticle" ) );
	
	$basket_article[ $i ][ "reference" ]		= Basket::getInstance()->getItemAt( $i)->getArticle()->get( "reference" );
	$basket_article[ $i ][ "stock_level" ]		= $stock_level;
	$basket_article[ $i ][ "quantity" ]			= Basket::getInstance()->getItemAt( $i)->getQuantity();
	$basket_article[ $i ][ "designation" ]		= Basket::getInstance()->getItemAt( $i)->getArticle()->get( "designation_1" );
	$basket_article[ $i ][ "delay" ]			= Util::getDelay( Basket::getInstance()->getItemAt( $i)->getArticle()->get( "delivdelay" ) );
	
	$refdiscount = Basket::getInstance()->getItemAt( $i)->getDiscountRate();
	if($refdiscount > 0){
		$basket_article[ $i ][ "ref_discount" ]		= Util::rateFormat( $refdiscount );
	}else{
		$basket_article[ $i ][ "ref_discount" ]		= "";
	}
	
	$basket_article[ $i ][ "lot" ]				= Basket::getInstance()->getItemAt( $i)->getArticle()->get( "lot" );
	
	$basket_article[ $i ][ "unit" ]				= Basket::getInstance()->getItemAt( $i)->getArticle()->get( "unit" );

	$hidecost = DBUtil::getDBValue( "hidecost", "detail", "idarticle", Basket::getInstance()->getItemAt( $i)->getArticle()->getId() );
	
	if($hidecost){
		$basket_article[ $i ][ "unit_price" ]		= "";
		$basket_article[ $i ][ "discount_price" ]	= "";
		$basket_article[ $i ][ "total_price" ]		= "";
		$basket_article[ $i ][ "total_base_price" ]	= "";
	}else{
		$basket_article[ $i ][ "unit_price" ]		= Util::priceFormat( Basket::getInstance()->getItemAt( $i)->getPriceET() ); 
		$basket_article[ $i ][ "discount_price" ]	= Util::priceFormat( Basket::getInstance()->getItemAt( $i )->getDiscountPriceET() );
		$basket_article[ $i ][ "total_price" ]		= Util::priceFormat( Basket::getInstance()->getItemAt( $i )->getTotalET() );
		$basket_article[ $i ][ "total_base_price" ]	= Util::priceFormat( Basket::getInstance()->getItemAt( $i)->getQuantity() * Basket::getInstance()->getItemAt( $i)->getPriceET() );
	}
	
	$basket_article[ $i ][ "product_attribute" ] = Basket::getInstance()->getItemAt( $i)->getArticle()->get( "summary_1" );
	
	//On récupère l'id product et le name pour faire le lien vers la fiche produit
	$qlink = "SELECT p.idproduct, p.name_1 FROM product p, detail d WHERE d.idarticle='" . $basket_article[ $i ][ "idarticle" ] . "' AND d.idproduct = p.idproduct";
	$rlink = DBUtil::query( $qlink );
	
	if( $rlink === false )
		die( "Impossible de générer le lien vers la fiche produit" );
	
	if( $rlink->RecordCount() > 0 ){
		
		$idp = $rlink->fields( "idproduct" );
		$name = $rlink->fields( "name_1" );
		
		$basket_article[ $i ][ "link" ] =  URLFactory::getProductURL( $idp, $name );
		
	}
	
	if( Basket::getInstance()->getItemAt( $i)->getDiscountRate() > 0.0 )
		$hasDiscount = "ok";
	
}

$estimate_basket_page->setData( "basket_articles", $basket_article );
$estimate_basket_page->setData( "hasdiscount", $hasDiscount );

$commercial_set = isset( $_SESSION[ "commercial" ] ) ? "ok" : "";
$estimate_basket_page->setData( "commercial_set", $commercial_set );

$get_m = isset( $_GET[ "m" ] ) ? unserialize( base64_decode( $_GET[ "m" ] ) ) : "";
$estimate_basket_page->setData( "get_m", $get_m );

if( isset( $_GET[ "f" ] ) ){
	
	$form = array();
	$form = unserialize( base64_decode( $_GET[ "f" ] ) );
	
}

//infos visiteur

$buyer = array();

$buyer[ "title" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getGender() : 0;
$buyer[ "lastname" ]	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getLastName() : "";
$buyer[ "firstname" ]	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getFirstName() : "";
$buyer[ "email" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getEmail() : "";
$buyer[ "phonenumber" ]	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getPhonenumber() : "";
$buyer[ "faxnumber" ]	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getFaxnumber() : "";
$buyer[ "company" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getCompany() : "";
$buyer[ "zipcode" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getZipcode() : "";
$buyer[ "city" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getCity() : "";
$buyer[ "address" ]		= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getAddress() : "";
$buyer[ "address_comp" ]= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getAddress()->getAddress2() : "";

foreach( $buyer as $fieldname =>$value ){

	if( isset( $form[ $fieldname ] ) )
		$buyer[ $fieldname ] = stripslashes( $form[ $fieldname ] );
		
}

$estimate_basket_page->setData( "buyer", $buyer );

$translations = array();
$translations[ "title" ] = Dictionnary::translate( "title" );
$translations[ "lastname" ] = Dictionnary::translate( "lastname" );
$translations[ "email" ] = Dictionnary::translate( "email" );
$translations[ "company" ] = Dictionnary::translate( "company" );
$translations[ "phonenumber" ] = Util::doNothing(Dictionnary::translate( "phonenumber" ));
$translations[ "faxnumber" ] = Dictionnary::translate( "faxnumber" );
$translations[ "zipcode" ] = Dictionnary::translate( "zipcode" );
$translations[ "city" ] = Dictionnary::translate( "city" );
$estimate_basket_page->setData( "formtranslations", $translations );

//Récupération de la civilité
$selection = isset( $buyer[ "title" ] ) ? $buyer[ "title" ] : "";

$query = "SELECT idtitle, title$lang FROM title ORDER BY idtitle ASC";
$rs = DBUtil::query( $query );

if( $rs === false || !$rs->RecordCount() )
	die( "Impossible de récupérer la liste des civilités" );

$html = "\n						<select name=\"title\">\n";
while( !$rs->EOF() ){

	$title = $rs->fields( "title$lang" );
	$id = $rs->fields( "idtitle" ); 
	$selected = $id == $selection ? " selected=\"selected\"" : "";
	
	$html .= "							<option value=\"$id\"$selected>" . htmlentities( $title ) . "</option>\n";

	$rs->MoveNext();
	
}

$html .= " 					</select>\n";

$estimate_basket_page->setData( "titlelist", $html );

$votremessage =  isset( $_POST[ "message" ] ) ? stripslashes( $_POST[ "message" ] ) : "";

$estimate_basket_page->createElement( "message" , $votremessage);

$estimate_basket_page->GetCategoryLinks();
$estimate_basket_page->GetBrands();
$estimate_basket_page->GetTrades();
$estimate_basket_page->GetProductHistory();

$estimate_basket_page->output();




//----------------------------------------------------------------------------------------------

function deprecated_createBasketFromSelection(){

	$idArticles = $_POST[ "idArticleSelection" ];
	$quantities = $_POST[ "quantitySelection" ];
	
	$idArticleArray = array();	
	$quantityArray = array();
	
	$token = strtok( $idArticles, "," );
	while( $token !== false ){
			
		$idArticleArray[] .= $token;
		$token = strtok( "," );
			
	}
	
	$token = strtok( $quantities, "," );
	while( $token !== false ){
					
		$quantityArray[] .= $token;
		$token = strtok( "," );
			
	}
	
	if( isset( $_POST[ "EstimateSelection" ] ) || isset( $_POST[ "EstimateSelection_x" ] ) ){

		$i = 0;
		while( $i < count( $quantityArray ) ){
			
			
			Basket::getInstance()->addArticle( new CArticle( $idArticleArray[ $i ] ) , $quantityArray[ $i ] );
			
			$i++;
		}
		
	}

}

//----------------------------------------------------------------------------------------------

?>