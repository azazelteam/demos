<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi du compratif à un ami
 */

include( "./deprecations.php" );

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

class IndexController extends PageController{};
$send_compare_template = new IndexController( "send_compare.tpl" );

//----------------------------------------------------------------------------------------------

if( isset( $_GET[ "send" ] ) && $_GET[ "send" ] == 1 )
	$sent = 1;
else
	$sent = "";

$send_compare_template->setData( "sent", $sent );

if( isset( $_GET[ "send" ] ) && $_GET[ "send" ] == 0 )
	$notsent = 1;
else
	$notsent = "";

$send_compare_template->setData( "notSent", $notsent );

//----------------------------------------------------------------------------------------------

$send_compare_template->output();

//----------------------------------------------------------------------------------------------

function random( $car ){
	
	$string = "";
	$chaine = "abcdefghijklmnpqrstuvwxy";
	srand( (double)microtime() * 1000000 );
	
	for( $i = 0 ; $i < $car ; $i++ ){
		
		$string .= $chaine[ rand() % strlen( $chaine ) ];
		
	}
	
	return $string;
	
}

//----------------------------------------------------------------------------------------------

?>