<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi d'un mail au fournisseur ou au commercial s'il existe sinon à l'administrateur
 */
 
//---------------------------------------------------------------------------------------------

include_once( "../objects/classes.php" ); 
include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );
include_once( "drawlift.php" );  

//---------------------------------------------------------------------------------------------

// Pour n'afficher que le centre de la page dans une nouvelle fenêtre
$isPopup = true;

// Feuille de styles spécifique à la page
$HTML_link['stylesheet'][3] = "$GLOBAL_START_URL/css/catalog/mailto.css";

$HTML_title = 'Contact commercial';
$bodyAtt = ' onload="window.focus();"';

include( "$GLOBAL_START_PATH/www/head.htm.php" );

//---------------------------------------------------------------------------------------------

$lang 		= Session::getInstance()->getLang();

//infos du commercial

if( $idbuyer = Session::getInstance()->isLogin() )
		$iduser = DBUtil::getDBVBalue( "iduser", "buyer", "idbuyer", $idbuyer ); 	//commercial du client
else 	$iduser = DBUtil::getParameter( "contact_commercial" ); 						//commercial par défaut

$rs =& DBUtil::query( "SELECT * FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
$user =& $rs->fields;

//données client du formulaire avec les valeurs par défaut

$customerInfos = array(

	"title"			=> 1,
	"firstname" 	=> "",
	"lastname"		=> "",
	"phonenumber"	=> "",
	"email" 		=> "",
	"company" 		=> ""
	
);

//infos client
//récupérer les données ( priorité sur le formulaire, puis sur le compte utilisateur si le bonhomme est loggé, sinon valeurs par défaut )

foreach( $customerInfos as $key => $defaultValue ){
	
	if( isset( $_POST[ $key ] ) )
		$customerInfos[ $key ] = stripslashes( $_POST[ $key ] );
	else if( $idbuyer )
		$customerInfos[ $key ] = $buyer[ $key ];
		
}

//---------------------------------------------------------------------------------------------
//variables dispos pour les templates de mail 
//@todo:flexyfier ici ;o)

global $GLOBAL_START_URL;

$idproduct 				= intval( $_REQUEST[ "IdProduct" ] );
$productName 			= htmlentities( DBUtil::getDBValue( "name$lang", "product", "idproduct", $idproduct ) );
$logo 					= $GLOBAL_START_URL . "/" . DBUtil::getParameterAdmin( "ad_logo" ); //@todo : pourquoi stocker le nom de l'image du logo puisque c'est toujour 'logo.jpg' => faire le tri un de ces quatre matins dans la liste des paramètres
$currentDate 			= htmlentities( humanReadableDate( date( "Y-m-d" ) ) );

$commercialGender 		= htmlentities( DBUtil::getDBValue( "title$lang", "title", "idtitle", $user[ "gender" ] ) );
$commercialName			= htmlentities( $user[ "firstname" ] );
$commercialLastname 	= htmlentities( $user[ "lastname" ] );
$commercialPhonenumber 	= htmlentities( Util::phonenumberFormat( $user[ "phonenumber" ] ) );
$commercialEmail 		= htmlentities( $user[ "email" ] );

$customerGender 		= htmlentities( DBUtil::getDBValue( "title$lang", "title", "idtitle", $customerInfos[ "title" ] ) );
$customerName 			= htmlentities( $customerInfos[ "firstname" ] );
$customerLastname 		= htmlentities( $customerInfos[ "lastname" ] );
$customerPhonenumber 	= htmlentities( Util::phonenumberFormat( $customerInfos[ "phonenumber" ] ) );
$customerEmail 			= htmlentities( $customerInfos[ "email" ] );
$customerCompany 		= htmlentities( $customerInfos[ "company" ] );

$subject 				= isset( $_POST[ "subject" ] ) ? htmlentities( stripslashes( $_POST[ "subject" ] ) ) : "Demande d'informations concernant le produit $productName";
$body 					= isset( $_POST[ "body" ] ) ? htmlentities( stripslashes( $_POST[ "body" ] ) ) : "";

//template du formulaire

include( "$GLOBAL_START_PATH/catalog/include/mailto.htm.php" );

if( !isset( $_POST[ "send" ] ) ){

	include("$GLOBAL_START_PATH/www/foot.htm.php");
	
	?>
	</div><!-- centre -->
	<!--</div> MainDiv -->
	</body>
	</html>
	<?php
	
	exit();
		
}

//-------------------------------------------------------------------------------------------
//envois	
//template pour le client

ob_start();
include( "$GLOBAL_START_PATH/catalog/include/mail_customer.htm.php" );
$customerBody = ob_get_contents();
ob_end_clean();

//template pour le commercial

ob_start();
$body = stripslashes( $_POST[ "body" ] );
include("$GLOBAL_START_PATH/catalog/include/mail_commercial.htm.php");
$commercialBody = ob_get_contents();
ob_end_clean();

//---------------------------------------------------------------------------------------------
//envoyer le nimèle au commercial

$subject = stripslashes( $_POST[ "subject" ] );

$mailer = new EVM_Mailer();
$mailer->set_message_type( "html" );
$mailer->set_sender( $user[ "firstname" ] . " " . $user[ "lastname" ], $user[ "email" ] );
$mailer->add_recipient( $customer[ "firstname" ] . " " . $customer[ "lastname" ], $customer[ "email" ] );
$mailer->set_subject( $subject );
$mailer->set_message( $commercialBody );
$mailer->send();
$error = $mailer->is_errors();

//envoyer le nimèle au client

$subject = "Votre demande d'information sur le produit '$productName'";

$mailer = new EVM_Mailer();
$mailer->set_message_type( "html" );
$mailer->set_sender( $customer[ "firstname" ] . " " . $customer[ "lastname" ], $customer[ "email" ] );
$mailer->add_recipient( $user[ "firstname" ] . " " . $user[ "lastname" ], $user[ "email" ] );
$mailer->set_subject( $subject );
$mailer->set_message( $commercialBody );
$mailer->send();
$error &= $mailer->is_errors();

?>
<p class="msg">
<?php

	if( $error )	
		echo Dictionnary::translate("mail_error");
	else echo Dictionnary::translate("mail_send");

?>
</p>
<?php
	
//---------------------------------------------------------------------------------------------

?>
<script type="text/javascript" language="javascript">
<!--
	setTimeout("window.close()",2000);
// -->
</script>
<?php include("$GLOBAL_START_PATH/www/foot.htm.php"); ?>
</div><!-- centre -->
<!--</div> MainDiv -->
</body>
</html>