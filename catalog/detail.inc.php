<?php 

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des détails du produit
 * Création de la table des données à afficher
 * Appel du masque défini pour le produit
 * @todo : les fichiers templates/catalog/product_detail_*.htm.php sont utilisés à la fois
 * dans la fiche produit ( product.productmask ) et dans la page catégorie de dernier niveau ( category.idmaskproduct ).
 * L'objet PRODUCT ne doit être instancié que dans le premier cas ( sinon usine à gaz avec l'allocation
 * dynamique de mémoire ). Ce fichier ( detail.inc.php ) peut donc $etre appelé avec ou sans instance d'objet PRODUCT
 * Il serait judicieux de distinguer les 2 types de masques par la suite( catégorie et produit ) 
 */
 
 
 
if( !isset( $Product ) ) 
	$Product = new PRODUCT( $idproduct );

$ArticlesByTables = array();
$hasPromo = array();
$isDestocking = array();
$hasLot = array();
//showDebug($Product->article,'detail.inc.php => $Product->article',$DEBUG);

$articles =& $Product->getArticles(); //@todo quelque chose de plus propre, genre itérateur

foreach( $articles as $Article ) {
	//showDebug($Article->article,'detail.inc.php => $a['.$Article->GetId().']',$DEBUG);
	//$Article->article[ "delivdelay"] = getDelay( $Article->article[ "delivdelay"] );
	
	$tableId = $Article->GetTable();
	if( empty($tableId) ) $tableId = 0; // 0 au lieu de '' = clef du tableau $ArticlesByTables
	//showDebug($tableId,'detail.inc.php => $Article->GetTable('.$Article->GetId().')',$DEBUG);
	// Chargement d'une table contenant tous les articles ARTICLE->article du PRODUCT	
	
	$ArticlesByTables[$tableId][$Article->GetId()] = $Article;
	//$articles[] = &$Article->article;
	
	// Test s'il y a au moins 1 référence en promo
	if( !isset($hasPromo[$tableId]) ) 
		$hasPromo[$tableId] = 0;
	$hasPromo[$tableId] += (int)$Article->GetReduceRate();
	
	// Test s'il y a au moins 1 référence en destockage
	
	if( !isset($isDestocking[$tableId]) ) 
		$isDestocking[$tableId] = false;
	$isDestocking[$tableId] |= Product::isDestocking( $Article->GetVisualRef() );
	
	// Quantité par lot - test si lot
	if( !isset($hasLot[$tableId]) ) $hasLot[$tableId] = 0;
	if( $Article->get('lot','0') > 1 )  $hasLot[$tableId] = 1;;
}
//showDebug($articles,'detail.inc.php => $articles',$DEBUG);
//showDebug($hasPromo,'detail.inc.php => $hasPromo',$DEBUG);

if( isset($IdMaskDetailLight) ){
	
	include("$GLOBAL_START_PATH/catalog/include/detail$IdMaskDetailLight.htm.php");
}
else {
	//include("$GLOBAL_START_PATH/catalog/include/detail_8.htm.php");
	include("$GLOBAL_START_PATH/catalog/include/detail$IdMaskDetail.htm.php");
}


?>