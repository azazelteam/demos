<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de l'odf devis
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );

$odf = new ODFEstimate(  $_REQUEST[ "idestimate" ]  );
$odf->exportODT( "Devis n° " .  $_REQUEST[ "idestimate" ]  . ".odt" );

?>