<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Liste des produits par métier
 */

//include( "./deprecations.php" );
include_once( "../config/init.php" );


include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

if(isset($_REQUEST['IdTrade'])){
	$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
	//var_dump($_SERVER['REQUEST_URI']);exit();
	$idtrade1 = (int)$_REQUEST['IdTrade'];
	$rst = ( object )DBUtil::query( "SELECT * FROM trade WHERE idtrade = '$idtrade1' LIMIT 1" )->fields;
	$url_at = makeTradeUrl($rst->trade_1,$idtrade1);
	//var_dump($url_at);exit();
	if($url_at != $_SERVER['REQUEST_URI']){
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header('HTTP/1.0 404 Not Found');
		
		include_once( DOCUMENT_ROOT . "/objects/flexy/PageController.php" );
		
		$template = "404.tpl";
		$controller = new PageController( $template );
		
		$notfound = "Erreur 404: Cette page n'existe pas (ou plus) ...";
	
		$controller->setData( "notfound", $notfound );
		
		$controller->output();	
		
		exit();
	}
}

/* -------------------------------------------------------------------------------------------- */

//class TradeController extends PageController{};

//$pageTitle 			= isset( $_REQUEST[ "IdTrade" ] ) ? Dictionnary::translate( "trade" )." : " . DBUtil::getDBValue( "trade_1", "trade", "idtrade", intval( $_REQUEST[ "IdTrade" ] ) ) : "Catalogue des métiers";
$controller 	= new PageController( "trade.tpl" );

$controller->setData( "trade", 	isset( $_REQUEST[ "IdTrade" ] ) ? getTradeArray( intval( $_REQUEST[ "IdTrade"] ) ) 	: null ); /* métier */
//$controller->setData( "subtrades", isset( $_REQUEST[ "IdTrade" ] ) ? null : getTrades( 0 ) ); /* sous-métiers */
/*$controller->setData( "tradeSiblings", isset( $_REQUEST[ "IdTrade" ] ) ? getTradeSiblings( intval( $_REQUEST[ "IdTrade" ] ) ) : null );
$controller->setData( "tradeParents", isset( $_REQUEST[ "IdTrade" ] ) ? getTradeParents( intval( $_REQUEST[ "IdTrade" ] ) ) : getTradeList() );*/

$products = array();
$prdhtml = '';

if( isset( $_REQUEST[ "IdTrade" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CProductProxy.php" );
	
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
	
	$query = "
	SELECT p.idproduct
	FROM product p, trade_product pt
	WHERE pt.idproduct = p.idproduct
	AND pt.idtrade= '" . intval( $_REQUEST[ "IdTrade" ] ) . "'
	AND p.available IS TRUE
	AND p.catalog_right <= '$catalog_right'
	ORDER BY pt.display ASC";

	$rs = DBUtil::query( $query );
	if(DBUtil::query( "SELECT idproduct FROM trade_product WHERE idtrade = '".$_REQUEST[ "IdTrade" ]."' LIMIT 1" )->RecordCount() == 1)
$prdhtml .= '<ul class="list_product" >';
	while( !$rs->EOF() ){
		
		//array_push( $products, new CProductProxy( new CProduct( $rs->fields( "idproduct" ) ) ) );
		$product = new CProductProxy( new CProduct( $rs->fields( "idproduct" ) ) );
		
		/*$prdhtml .= '<li><a title="Ajouter au panier" class="btn-buy" href="'.URLFactory::getProductURL($rs->fields( "idproduct" )).'" style="left: -44px;"><img width="60" height="60" src="/templates/catalog/images/catalog/list-btn-buy.png"></a>
                            <a href="'.URLFactory::getProductURL($rs->fields( "idproduct" )).'">';
							if($product->getPromotionCount())
                            	$prdhtml .= '<div class="ruban promo"></div>';
								$prdhtml .= '
                                <img width="170" height="170" src="'.$product->getImageURI(170).'" alt="'.$product->get('name_1').'">
                                <h4 style="font-size:1em">'.$product->get('name_1').'</h4>
                                <small><strong>'.$product->getReferenceCount().'</strong> référence(s)</small>
                                <span class="prix">
                                    <small>à partir de</small><br>
                                    <strong>'.$product->getLowerPriceET().' <small>HT</small></strong>
                                </span>
                            </a></li>';*/
							
		$prdhtml .= '<li itemtype="http://schema.org/Product" itemscope="">
        
            				                            <a class="btn-buy" style="left: -44px;" href="'.URLFactory::getProductURL($rs->fields( "idproduct" )).'"><img width="60" height="60" src="/templates/catalog/images/catalog/list-btn-buy.png" alt="Ajouter au panier"></a>
                            <a itemprop="url" href="'.URLFactory::getProductURL($rs->fields( "idproduct" )).'">
                            	                                <img width="170" height="170" src="'.$product->getImageURI(170).'" alt="'.$product->get('name_1').'" class="product_image" itemprop="image">
                                <span class="small product_reference"><span class="strong">'.$product->getReferenceCount().'</span> r&eacute;f&eacute;rence(s)</span>
                                <span class="price" itemtype="http://schema.org/Offer" itemscope="" itemprop="offers">
                                    <small style="font-size:0.8em">&agrave; partir de</small><br>
                                    <span class="strong" itemprop="price">'.$product->getLowerPriceET().' <small>HT</small></span>
                                </span>
                            </a>
<h2 itemprop="name" style="font-size:0.85em"><a href="'.URLFactory::getProductURL($rs->fields( "idproduct" )).'" class="product-name">'.$product->get('name_1').'</a></h2>	
            </li>';
		$rs->MoveNext();
		
	}
	if(DBUtil::query( "SELECT idproduct FROM trade_product WHERE idtrade = '".$_REQUEST[ "IdTrade" ]."' LIMIT 1" )->RecordCount() == 1)
$prdhtml .= '</ul>';
}

$controller->setData( "prdhtml", $prdhtml );

$controller->output();

/* -------------------------------------------------------------------------------------------- */
function getTradeArray( $idtrade, $recursive = true ){

	global $GLOBAL_START_URL, $GLOBAL_START_PATH;

	$rs = ( object )DBUtil::query( "SELECT * FROM trade WHERE idtrade = '$idtrade' LIMIT 1" )->fields;
	if( strlen( $rs->image ) && file_exists( "$GLOBAL_START_PATH/www/" . $rs->image ) )
			$uri = "/www/" . $rs->image;
	else 	$uri = "/www/img_prod/photo_missing.jpg";
	
	switch((int)$idtrade){
		case 39:
			$name = 'Espaces verts : v&ecirc;tements pour jardinier, &eacute;lagueur, forestier';
		break;
		case 17:
			$name = 'Logistique - Transport';
		break;
		case 38:
			$name = 'Equipement de protection individuelle pour &eacute;lectricien';
		break;
		case 23:
			$name = 'EPI et mat&eacute;riel agroalimentaire';
		break;
		case 25:
			$name = 'Equipements de laboratoire';
		break;
		case 26:
			$name = 'B&acirc;timent et travaux publics : V&ecirc;tement de travail et chaussures';
		break;
		case 27:
			$name = 'V&ecirc;tements et chaussures de travail : couvreur, charpentier et menuisier';
		break;
		case 30:
			$name = 'Equipements, v&ecirc;tement et fournitures mairie';
		break;
		case 37:
			$name = 'V&ecirc;tements, chaussures de travail et EPI industriels';
		break;
		case 49:
			$name = 'Equipement soudeur';
		break;
		case 51:
			$name = 'Tenue de Peintre';
		break;
		case 50:
			$name = 'EPI et mat&eacute;riel pour travaux acrobatiques';
		break;
		default:
			$name = $rs->trade_1;
		break;
	}

	$trade = array(

		"idtrade"		=> $rs->idtrade,
		"name" 			=> $name,
		"image" 		=> "$GLOBAL_START_URL/$uri",
		"description" 	=> $rs->description_1,
		"description_plus_1" 	=> $rs->description_plus_1,
		"hasProducts" 		=> DBUtil::query( "SELECT idproduct FROM trade_product WHERE idtrade = '$idtrade' LIMIT 1" )->RecordCount() == 1,
		"trades" 		=> $recursive ? getTrades( $idtrade ) : null,
		"tradeurl"		=> makeTradeUrl($rs->trade_1, $idtrade),
		"url" 			=> "$GLOBAL_START_URL/produits-special-" . URLFactory::rewrite( $rs->trade_1 ) . "-t-" . $rs->idtrade
		
	);

	return $trade;

}
function makeTradeUrl($tradename,$idtrade){
	$tradename = strtr($tradename,'@ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ',
					   'aaaaaaaceeeeiiiiooooouuuuyaaaaaaceeeeiiiioooooouuuuyy');
	$tradename = str_replace(' ', '-', $tradename);
	$tradename = str_replace(',', '-', $tradename);
	$tradename = str_replace('---', '-', $tradename);
	$tradename = str_replace('--', '-', $tradename);
	$tradename = strtolower($tradename);
	return "/".$tradename.'-t-'.$idtrade;
}
/* -------------------------------------------------------------------------------------------- */

function getTrades( $idparent ){
	
	$rs =& DBUtil::query( "SELECT idtrade FROM trade WHERE idparent = '$idparent' ORDER BY display ASC" );
	
	if( !$rs->RecordCount() )
		return null;
	
	$trades = array();
	
	while( !$rs->EOF() ){
	
		$trades[] = getTradeArray( $rs->fields( "idtrade" ), false );
			
		$rs->MoveNext();
		
	}

	return $trades;
	
}

/* -------------------------------------------------------------------------------------------- */

function getTradeParents( $idtrade ){
	
	$parents = array();
	
	$done = false;
	while( !$done ){
		
		$rs =& DBUtil::query( "SELECT idparent FROM trade WHERE idtrade = '$idtrade' ORDER BY display ASC" );

		$done = $rs->RecordCount() == 0 || $rs->fields( "idparent" ) == 0;
		
		if( !$done ){
		
			$parents[] = getTradeArray( $rs->fields( "idparent" ), false );
			$idtrade = $rs->fields( "idparent" );
				
		}
		
	}
	
	return count( $parents ) ? array_reverse( $parents ) : null;

}

/* -------------------------------------------------------------------------------------------- */

function getTradeSiblings( $idtrade ){
	
	$lang = User::getInstance()->getLang();

	$rs =& DBUtil::query( "SELECT t2.idtrade, t2.trade$lang FROM trade t1, trade t2 WHERE t1.idtrade = '$idtrade' AND t1.idparent = t2.idparent AND t2.idtrade <> '$idtrade' ORDER BY t2.display ASC" );

	$siblings = array();
	
	while( !$rs->EOF() ){
		
		$siblings[] = getTradeArray( $rs->fields( "idtrade" ), false );
		
		$rs->MoveNext();
		
	}
	
	return count( $siblings ) ? $siblings : null;
	
}

/* -------------------------------------------------------------------------------------------- */

function getTradeList(){
	
	$lang = User::getInstance()->getLang();

	$rs =& DBUtil::query( "SELECT idtrade, trade$lang FROM trade WHERE idparent = 0 ORDER BY display ASC" );

	$trades = array();
	
	while( !$rs->EOF() ){
		
		$trades[] = getTradeArray( $rs->fields( "idtrade" ), false );
		
		$rs->MoveNext();
		
	}
	
	return count( $trades ) ? $trades : null;
	
}

/* -------------------------------------------------------------------------------------------- */

?>