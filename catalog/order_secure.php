<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Etapes lors d'une commande avec paiement sécurisé SSL
 */
 
include_once( dirname( __FILE__ ) . "/../objects/classes.php");
include_once( dirname( __FILE__ ) . "/../objects/DBUtil.php");
include_once( dirname( __FILE__ ) . "/../objects/User.php");
include_once( dirname( __FILE__ ) . "/../objects/Order.php");
include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php");

$CCM_tracker_mail = DBUtil::getParameterAdmin('CCM_tracker_mail');
$AddPdf = DBUtil::getParameter('PdfWithOrderFront');
$SiteName 	= DBUtil::getParameterAdmin('ad_name');

$tpeVer = DBUtil::getParameterAdmin( "CMCIC_version" );

if(  $_REQUEST["order_ref"] != "" ){
	
	
	
	User::fakeAuthentification();
	
	switch( $tpeVer ){
		case "1.2open":		
			@require_once("$GLOBAL_START_PATH/config/CMCIC_config.php");
			
			// --- PHP implementation of RFC2104 hmac sha1 ---
			// --- Implémentation PHP du RFC2104 hmac sha1 ---
			@require_once("$GLOBAL_START_PATH/script/CMCIC_HMAC.inc.php");
			if (!function_exists('CMCIC_hmac')) 
			{
			    die ('cant require hmac function.');
			}
					
			// Begin Main : Retrieve Variables posted by CMCIC Payment Server 
			//              Recevoir les variables postées par le serveur bancaire
			
			$CMCIC_reqMethod  = $HTTP_SERVER_VARS["REQUEST_METHOD"];
			if (($CMCIC_reqMethod == "GET") or ($CMCIC_reqMethod == "POST")) {
			    $wCMCIC_bruteVars = "HTTP_".$CMCIC_reqMethod."_VARS";
			    $CMCIC_bruteVars  = ${$wCMCIC_bruteVars};
			}
			else
			    die ('Invalid REQUEST_METHOD (not GET, not POST).');
			
			@$isVariableEmpty  = $CMCIC_bruteVars['TPE'];
						
			// empty variables ?
			if (!($isVariableEmpty > " "))
			{

			    echo "\r\nTrying PHP<=3 old style ! "."\r\n";
			
			    settype($CMCIC_bruteVars , "array"); 
			
			    @$CMCIC_bruteVars['MAC']         = $MAC;
			    @$CMCIC_bruteVars['TPE']         = $TPE;
			    @$CMCIC_bruteVars['date']        = $date;
			    @$CMCIC_bruteVars['montant']     = $montant;
			    @$CMCIC_bruteVars['reference']   = $reference;
			    $URL_texte_libre                 = "texte-libre";
			    @$CMCIC_bruteVars['texte-libre'] = $$URL_texte_libre;
			    $URL_code_retour                 = "code-retour";
			    @$CMCIC_bruteVars['code-retour'] = $$URL_code_retour;
			    @$CMCIC_bruteVars['retourPLUS']   = $retourPLUS;
			
			    // var_dump($CMCIC_bruteVars);
			    echo "\r\n Is it Better ? "."\r\n";
			}
		
			
			// Message Authentication
			// Test d'authentification
			@$CMCIC_authVars   = TesterHmac($CMCIC_CONFIG, $CMCIC_bruteVars );

			@$Verified_Result  = strtolower($CMCIC_authVars['resultatVerifie']);
			@$IdOrder = @$CMCIC_bruteVars['reference'];
			
			$Order = new Order(intval($IdOrder));
			
			$bdd = DBUtil::getConnection();
					
			// Copyright (c) 2003 Euro-Information ( mailto:centrecom@e-i.com )
			// All rights reserved. ---
			
			$CCM_return_code = DBUtil::getParameterAdmin('CCM_return_code');	
						
			//Mise à jour du paiement dans la base
			if( strtolower( $Verified_Result ) == strtolower( $CCM_return_code ) ){
				
				$Order->set("paid",1);
				$Order->set("cash_payment", 1);
				$Order->set("balance_date",date("Y-m-d"));
				$Order->set("balance_amount", $Order->getNetBill() );
				$Order->set("conf_order_date",date("Y-m-d H:i:s"));
				$Order->set("status",Order::$STATUS_ORDERED);
				$Order->set("idpayment_delay",1);
				$Order->set("payment_idpayment",1);
				$Order->Unstock();
		
				$Order->save();
				
				//maitenant on créé le règlement
				createMyRegulation(	$Order->get( "idorder" ),
									"order",
									$Order->get( "idbuyer" ),
									$Order->getNetBill() ,
									1,
									date("Y-m-d"),
									0,
									'Paiement Comptant en ligne CCM',
									'Paiement Comptant en ligne CCM',
									0);	

				$Order = new Order($Order->get( "idorder" ));
				//On génère les commandes fournisseur & les BLs
				TradeFactory::createSupplierOrders($Order);
				
				//On génère la facture et le règlement
				TradeFactory::createInvoiceFromOrder($Order);
				
				sendOrderMail( $Order );

			}else{
				
				if( strtolower( $Verified_Result ) == 'annulation' ){
					
					$Order->set("status",Order::$STATUS_TODO);
					$Order->save();
					
					sendCancelMail( $Order );		
					
				}
			}
			
			//-----------------------------------------------------------------------------
			// Send receipt to CMCIC server
			// Envoyer un A/R au serveur bancaire
			//-----------------------------------------------------------------------------
			@printf (CMCIC_PHP2_RECEIPT, $CMCIC_authVars['accuseReception']);
			
		break;
		
		case "3.0":
			// TPE Settings
			// Warning !! CMCIC_Config contains the key, you have to protect this file with all the mechanism available in your development environment.
			// You may for instance put this file in another directory and/or change its name. If so, don't forget to adapt the include path below.
			require_once("$GLOBAL_START_PATH/config/CMCIC_ConfigV3.php");
			
			// --- PHP implementation of RFC2104 hmac sha1 ---
			require_once("$GLOBAL_START_PATH/script/CMCIC_Tpe.inc.php");
			
			
			// Begin Main : Retrieve Variables posted by CMCIC Payment Server 
			$CMCIC_bruteVars = getMethode();
			
			// TPE init variables
			$oTpe = new CMCIC_Tpe();
			$oHmac = new CMCIC_Hmac($oTpe);
			
			// Message Authentication
			$cgi2_fields = sprintf(CMCIC_CGI2_FIELDS, $oTpe->sNumero,
								  $CMCIC_bruteVars["date"],
							          $CMCIC_bruteVars['montant'],
							          $CMCIC_bruteVars['reference'],
							          $CMCIC_bruteVars['texte-libre'],
							          $oTpe->sVersion,
							          $CMCIC_bruteVars['code-retour'],
								  $CMCIC_bruteVars['cvx'],
								  $CMCIC_bruteVars['vld'],
								  $CMCIC_bruteVars['brand'],
								  $CMCIC_bruteVars['status3ds'],
								  $CMCIC_bruteVars['numauto'],
								  $CMCIC_bruteVars['motifrefus'],
								  $CMCIC_bruteVars['originecb'],
								  $CMCIC_bruteVars['bincb'],
								  $CMCIC_bruteVars['hpancb'],
								  $CMCIC_bruteVars['ipclient'],
								  $CMCIC_bruteVars['originetr'],
								  $CMCIC_bruteVars['veres'],
								  $CMCIC_bruteVars['pares']
								);
							
			//var_dump($cgi2_fields);exit;
		//if ($oHmac->computeHmac($cgi2_fields) == strtolower($CMCIC_bruteVars['MAC'])){
								$CMCIC_bruteVars['code-retour']='paye';	
								
				switch($CMCIC_bruteVars['code-retour']) {
					case "Annulation":
						// Payment has been refused
						// put your code here (email sending / Database update)
						// Attention : an autorization may still be delivered for this payment
					$IdOrder = $_REQUEST["order_ref"];
						$Order = new Order(intval($IdOrder));
			
						$Order->set("status",Order::$STATUS_TODO);
						$Order->save();
						
						sendCancelMail( $Order );	
								
						break;
			
					case "paye":
									
						$IdOrder = $_REQUEST["order_ref"];
						$Order = new Order($IdOrder);

						/*modification statut panier abandonné*/
						$query_status = "UPDATE cart SET `status`=1 WHERE `orderid`=".$IdOrder;
						DBUtil::getConnection()->Execute($query_status);
						if(isset($_SESSION['id_cart']))
							unset($_SESSION['id_cart']);
						
						// Payment has been accepeted on the test server
						// put your code here (email sending / Database update)
						$Order->set("paid",1);
						$Order->set("cash_payment", 1);
						$Order->set("balance_date",date("Y-m-d"));
						$Order->set("balance_amount", $Order->getNetBill() );
						$Order->set("conf_order_date",date("Y-m-d H:i:s"));
						$Order->set("status",Order::$STATUS_ORDERED);
						$Order->set("idpayment_delay",1);
						$Order->set("payment_idpayment",1);
						$Order->Unstock();
									
						$Order->save();
						
						//maitenant on créé le règlement
						createMyRegulation($Order->get( "idorder" ),"order",$Order->get( "idbuyer" ),$Order->getNetBill() ,1,date("Y-m-d"),0,'Paiement Comptant en ligne CCM','Paiement Comptant en ligne CCM',0);	
						
						$Order = new Order($Order->get( "idorder" ));
						
						//On génère les commandes fournisseur & les BLs
						TradeFactory::createSupplierOrders($Order);
						
						//On génère la facture et le réglement
						TradeFactory::createInvoiceFromOrder($Order);
				
						//On envoi un mail au client
						sendOrderMail( $Order );
						header( "Location: /catalog/confirm_order.php" );
						break;
			
					case "paiement":
					
						$IdOrder = $_REQUEST["order_ref"];
						$Order = new Order(intval($IdOrder));

						/*modification statut panier abandonné*/
						$query_status = "UPDATE cart SET `status`=1 WHERE `orderid`=".$IdOrder;
						DBUtil::getConnection()->Execute($query_status);
						if(isset($_SESSION['id_cart']))
							unset($_SESSION['id_cart']);
					
						// Payment has been accepted on the productive server
						// put your code here (email sending / Database update)
						$Order->set("paid",1);
						$Order->set("cash_payment", 1);
						$Order->set("balance_date",date("Y-m-d"));
						$Order->set("balance_amount", $Order->getNetBill() );
						$Order->set("conf_order_date",date("Y-m-d H:i:s"));
						$Order->set("status",Order::$STATUS_ORDERED);
						$Order->set("idpayment_delay",1);
						$Order->set("payment_idpayment",1);
						$Order->Unstock();
									
						$Order->save();

						//maitenant on créé le règlement
						createMyRegulation($Order->get( "idorder" ),"order",$Order->get( "idbuyer" ),$Order->getNetBill() ,1,date("Y-m-d"),0,'Paiement Comptant en ligne CCM','Paiement Comptant en ligne CCM',0);	
						
						$Order = new Order($Order->get( "idorder" ));						
						
						//On génère les commandes fournisseur & les BLs
						TradeFactory::createSupplierOrders($Order);
						
						//On génère la facture et le réglement
						TradeFactory::createInvoiceFromOrder($Order);
				
						//On envoi un mail au client
						sendOrderMail( $Order );
						header( "Location: /catalog/confirm_order.php" );		
						break;
			
			
					/*** ONLY FOR MULTIPART PAYMENT ***/
					case "paiement_pf2":
					case "paiement_pf3":
					case "paiement_pf4":
						break;
			
					case "Annulation_pf2":
					case "Annulation_pf3":
					case "Annulation_pf4":
						break;
					
				}
			
				$receipt = CMCIC_CGI2_MACOK;
			
		//	}else{

		//		$receipt = CMCIC_CGI2_MACNOTOK.$cgi2_fields;
		//	}
			
			//printf (CMCIC_CGI2_RECEIPT, $receipt);
					
		break;
	}
}else{
	
	//Le client à cliqué sur le lien de retour
	$IdOrder = $_REQUEST["order_ref"];
	
	if(User::getInstance()->getId()>0){
		header( "Location: $GLOBAL_START_URL/sales_force/com_admin_order.php?IdOrder=$IdOrder" );	
	}else{
		header( "Location: /catalog/confirm_order.php" );	
	}	
		
}

//--------------------------------------------------------------------------------------------------------------------------------------

function createMyRegulation($iddocument,$typeDoc,$idbuyer,$amount,$idpayment,$payment_date,$idbank,$piece,$comment,$fromDP=0){

	$username = User::getInstance()->get('login');
	$iduser = User::getInstance()->getId();	
	
	 // ------- Mise à jour de l'id gestion des Index  #1161
	 	
		include_once( dirname( __FILE__ ) . "/TradeFactory.php" );

		//$maxNumRegul = DBUtil::query("SELECT ( IFNULL( MAX(idregulations_buyer) , 0 )+ 1) as newId FROM regulations_buyer")->fields('newId');
	 
		$table = 'regulations_buyer';
		$maxNumRegul = TradeFactory::Indexations($table);
	
	
	$insertReg=			
	"INSERT INTO regulations_buyer (
		idregulations_buyer,
		idbuyer,
		iduser,
		amount,
		idbank,
		piece_number,
		comment,
		idpayment,
		payment_date,
		accept,
		lastupdate,
		username,
		from_down_payment
	) VALUES (
		'$maxNumRegul',
		'$idbuyer',
		'$iduser',
		$amount,
		$idbank,
		'".addslashes($piece)."',
		'".addslashes($comment)."',
		$idpayment,
		'$payment_date',
		1,
		NOW(),
		'$username',
		$fromDP
	)";

	DButil::query($insertReg);
	
	$setline = "";
	
	//maj devis ou commande
	if($fromDP){
		$setline = "down_payment_idregulation = '$maxNumRegul', 
					down_payment_idbank = '$idbank', 
					down_payment_comment = '".addslashes($comment)."', 
					down_payment_piece = '".addslashes($piece)."', 
					down_payment_idpayment = '$idpayment',
					down_payment_date = '$payment_date'";
	}else{
		$setline = "cash_payment_idregulation='$maxNumRegul',
					cash_payment_idbank = '$idbank', 
					payment_comment = '".addslashes($comment)."', 
					cash_payment_piece = '".addslashes($piece)."', 
					payment_idpayment = '$idpayment',
					balance_date = '$payment_date'";
	}
	$query = "UPDATE `$typeDoc` SET $setline where id$typeDoc = '$iddocument'";	
	DButil::query($query);
	
	// si c'est une commande on doit aussi transferer au devis
	if($typeDoc=='order'){
		$rsidestimate = DBUtil::query("SELECT idestimate FROM `order` WHERE idorder='$iddocument' LIMIT 1");
		
		if($rsidestimate->RecordCount() && $rsidestimate->fields('idestimate')!=0 && $rsidestimate->fields('idestimate')!='' ){
			$idestimateassocie = $rsidestimate->fields('idestimate');
			$query = "UPDATE `estimate` SET $setline where idestimate = '$idestimateassocie'";
			DButil::query($query);
		}
	}
	
	// de même si le devis a une commande ce qui a mon avis est rare et stupide de permettre de passer en commande
	if($typeDoc=='estimate'){
		$rsido = DBUtil::query("SELECT idorder FROM `order` WHERE idestimate='$iddocument' LIMIT 1");
		
		if($rsido->RecordCount() && $rsido->fields('idorder')!='' && $rsido->fields('idorder')!=0 ){
			$idorderassocie = $rsido->fields('idorder');
			$query = "UPDATE `order` SET $setline where idorder = '$idorderassocie'";
			DButil::query($query);
		}
	}
}

//--------------------------------------------------------------------------------------------------------------------------------------

function sendCancelMail( Order &$Order ){
	global $GLOBAL_START_PATH;
	
	$CCM_tracker_mail = DBUtil::getParameterAdmin('CCM_tracker_mail');
	$AddPdf = DBUtil::getParameter('PdfWithOrderFront');
	$SiteName 	= DBUtil::getParameterAdmin('ad_name');
	
	//infos client, commercial et admin
		
	$AdminName 	= DBUtil::getParameterAdmin('ad_name');
	$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
	
	//FRONT OFFICE
	if( !isset( $_SESSION[ "commercial" ] ) ){
		$iduser = DBUtil::getParameter('contact_commercial');
	}else{
		//ADM_COMMERCIAL
		$iduser = $_SESSION[ "commercial" ];
	}	

	$rs =& DBUtil::query( "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
	
	$CommercialName = $rs->fields( "firstname" ).' '.$rs->fields( "lastname" );
	$CommercialEmail = $rs->fields( "email" );
	
	$BuyerName = $Order->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );
	
	//-----------------------------------------------------------------------------------------
			
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	
	$editor->setTemplate( "ORDER_NOTIFICATION_1_BAD" );
			

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseOrderTags( $Order->get( "idorder" ) );
	$editor->setUseBuyerTags( $Order->get( "idbuyer" ),$Order->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
				
	//-----------------------------------------------------------------------------------------
	//envoie du mail
	
	include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );

	$EVM_Mailer = new EVM_Mailer();
	$EVM_Mailer->set_message_type( "html" );
	$EVM_Mailer->set_sender( $AdminName, $AdminEmail );
	$EVM_Mailer->add_recipient( $BuyerName, $BuyerEmail );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$EVM_Mailer->add_CC( $CommercialName, $CommercialEmail );

	$EVM_Mailer->set_message($Message);
	$EVM_Mailer->set_subject( $Subject );
	
	$EVM_Mailer->send();
	
	if( $EVM_Mailer->is_errors() ) {
		$errorMsg[] = array('var'=>$EVM_Mailer->get_errors(),'user'=>Dictionnary::translate('error'),'log'=>$ScriptName.' (sendmail.inc.php) => $EVM_Mailer->get_errors()');
	}
	
	//Tracker
	$headers = 'From: AKILAE <webmaster@akilae-saas.fr>' . "\r\n";
     $headers .= 'Bcc: webmaster@akilae-saas.fr' . "\r\n";
	mail($CCM_tracker_mail,"$SiteName Tracker : Commande $IdOrder","Paiement Non Validé",$headers);  
	
}

//--------------------------------------------------------------------------------------------------------------------------------------

function sendOrderMail( Order &$Order ){
	global $GLOBAL_START_PATH;
	
	$CCM_tracker_mail = DBUtil::getParameterAdmin('CCM_tracker_mail');
	$AddPdf = DBUtil::getParameter('PdfWithOrderFront');
	$SiteName 	= DBUtil::getParameterAdmin('ad_name');
	$IdOrder = $Order->getId();
	
	$AdminName 	= DBUtil::getParameterAdmin('ad_name');
	$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
	
	//FRONT OFFICE
	if( !User::getInstance()->getId() ){
		$iduser = DBUtil::getParameter('contact_commercial');
	}else{
		//ADM_COMMERCIAL
		$iduser = User::getInstance()->getId(); //@todo : utiliser un fichier du front office dans le back-office = DANGER
	}	
	
	$rs =& DBUtil::query( "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
	
	$CommercialName = $rs->fields( "firstname" ).' '.$rs->fields( "lastname" );
	$CommercialEmail = $rs->fields( "email" );
	
	$BuyerName = $Order->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );
	
	//-----------------------------------------------------------------------------------------
			
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	
	$editor->setTemplate( "ORDER_NOTIFICATION_1" );
			

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseOrderTags( $Order->get( "idorder" ) );
	$editor->setUseBuyerTags( $Order->get( "idbuyer" ),$Order->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
				
	//-----------------------------------------------------------------------------------------
	//envoie du mail
	
	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");

	$mailer = new htmlMimeMail();
	$mailer->setFrom( '"'.$AdminName.'" <'.$AdminEmail.'>');
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setCc( $CommercialEmail );

	$mailer->setHtml($Message);
	$mailer->setSubject( $Subject );
	
	if($AddPdf){
		
		//ajout du PDF
			
		include_once( dirname( __FILE__ ) . "/../objects/odf/ODFOrder.php" );

		$odf = new ODFOrder( $IdOrder );
		$pdfData = $odf->convert( ODFDocument::$DOCUMENT_PDF );
		
		$mailer->addAttachment($pdfData, $pdf_filename,'application/pdf');

	}
	
	$mailer->send(array($BuyerEmail));
	
	//Tracker
	$headers = 'From: AKILAE <webmaster@akilae-saas.fr>' . "\r\n";
    $headers .= 'Bcc: webmaster@akilae-saas.fr' . "\r\n";
	mail($CCM_tracker_mail,"$SiteName Tracker : Commande $IdOrder","Paiement Validé",$headers); 
}

// ----------------------------------------------------------------------------
// function TesterHmac
// ----------------------------------------------------------------------------
function TesterHmac($CMCIC_Tpe, $CMCIC_bruteVars )
{
   @$php2_fields = sprintf(CMCIC_PHP2_FIELDS, $CMCIC_bruteVars['retourPLUS'], 
                                              $CMCIC_Tpe["tpe"], 
                                              $CMCIC_bruteVars["date"],
                                              $CMCIC_bruteVars['montant'],
                                              $CMCIC_bruteVars['reference'],
                                              $CMCIC_bruteVars['texte-libre'],
                                               CMCIC_VERSION,
                                              $CMCIC_bruteVars['code-retour']);


    if ( strtolower($CMCIC_bruteVars['MAC'] ) == CMCIC_hmac($CMCIC_Tpe, $php2_fields) ):
        $result  = $CMCIC_bruteVars['code-retour'];
        $receipt = CMCIC_PHP2_MACOK;
    else: 
        $result  = 'None';
        $receipt = CMCIC_PHP2_MACNOTOK.$php2_fields;
    endif;

    $mnt_lth = strlen($CMCIC_bruteVars['montant'] ) - 3;
    if ($mnt_lth > 0):
        $currency = substr($CMCIC_bruteVars['montant'], $mnt_lth, 3 );
        $amount   = substr($CMCIC_bruteVars['montant'], 0, $mnt_lth );
    else:
        $currency = "";
        $amount   = $CMCIC_bruteVars['montant'];
    endif;

    return array( "resultatVerifie" => $result ,
                  "accuseReception" => $receipt ,
                  "tpe"             => $CMCIC_bruteVars['TPE'],
                  "reference"       => $CMCIC_bruteVars['reference'],
                  "texteLibre"      => $CMCIC_bruteVars['texte-libre'],
                  "devise"          => $currency,
                  "montant"         => $amount);
}
?>