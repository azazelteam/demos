<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Tracking des produits visulaisés
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/URLFactory.php" );

$_REQUEST =& Util::arrayMapRecursive( "Util::doNothing", $_REQUEST );
$_REQUEST =& Util::arrayMapRecursive( "strip_tags", $_REQUEST );

/* champs de saisie obligatoires */
$postData = array(
	"idbuyer" 		=> Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0,
	"ipuser"		=> $_SERVER["REMOTE_ADDR"],
	"idarticle"		=> isset( $_REQUEST[ "idarticle" ] ) 	? intval( $_REQUEST[ "idarticle" ] )	: "",
	"quantity"		=> isset( $_REQUEST[ "quantity" ] ) 	? intval( $_REQUEST[ "quantity" ] )		: "",
	"clicModule"	=> isset( $_REQUEST[ "clicModule" ] ) 	? intval( $_REQUEST[ "clicModule" ] )	: "",
	"clicTable"		=> isset( $_REQUEST[ "clicTable" ] ) 	? intval( $_REQUEST[ "clicTable" ] )	: "",
	"isEstimate"	=> isset( $_REQUEST[ "isEstimate" ] ) 	? intval( $_REQUEST[ "isEstimate" ] )	: "",
	"isOrder"		=> isset( $_REQUEST[ "isOrder" ] ) 		? intval( $_REQUEST[ "isOrder" ] )		: ""	
);

/* Insertion dans la base de données */
if($_SERVER["REMOTE_ADDR"] != "80.14.149.101" && $_SERVER["REMOTE_ADDR"] != "213.56.146.34" ) {
	DBUtil::query( "INSERT INTO `trackers_product` ( `datetime`, `" . implode( "`,`", array_keys( $postData ) ) . "` ) VALUES( NOW(), '" . implode( "','", array_values( $postData ) ) . "' )" );
}

?>