<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonctions principales du panier
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Basket.php" );
include_once( dirname( __FILE__ ) . "/../objects/Util.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );
include_once( dirname( __FILE__ ) . "/product_gift.php" );
include_once( dirname( __FILE__ ) . "/product_basket.php" );

$_POST = Util::arrayMapRecursive( "strip_tags", 	$_POST );

//$_POST = Util::arrayMapRecursivegetTotalET( "stripslashes", 	$_POST );
//die("ok");
$_POST = Util::arrayMapRecursive( "Util::doNothing", 	$_POST );

unset($_SESSION["DISCOUNT"]);

if($_POST['functiontest'] == 1)
{
	$itemtest = intval($_POST['itemtest']);
	Basket::getInstance()->removeItemAt($itemtest);
}	

if(isset($_GET["utm_source"])){
 	$_SESSION["utm_source"] = $_GET["utm_source"];
 }
/* ----------------------------------------Î--------------------------------------------------------------------- */
/**
* création panier à partir id_cart
*/

if (isset($_GET['cart']))
{
	$id_cart = base64_decode($_GET['cart']);
	$cart_status = DBUtil::getDBValue('status', 'cart', 'id_cart', $id_cart);
	if ((int)$cart_status == 0)
	{
		$it = DBUtil::query("SELECT id_article, quantity FROM `cart_detail` WHERE `id_cart` = $id_cart");
		while (!$it->EOF())
		{
			$idarticle = $it->fields['id_article'];
			$quantity = $it->fields['quantity'];
			addBasketItem( intval( $idarticle ), intval($quantity) );
			$it->MoveNext();		
		}
		$_SESSION[ "id_cart" ] = $id_cart;
	}
}

/* ------------------------------------------------------------------------------------------------------------- */
/**
 * ajout d'une ou plusieurs références au panier : méthode GET, POST ou COOKIE avec les paramètres suivants :
 * order
 * 		$_REQUEST[ "idarticle" ] 	int ou array<int>, le/les identifiant(s) des articles à ajouter, 	requis
 * 		$_REQUEST[ "quantity" ] 	int ou array<int>, la/les quantité(s) à ajouter, 					optionnel
 * 		$_REQUEST[ "idcolor" ] 		int ou array<int>, la/les couleurs sélectionnée(s), 				optionnel
 * 		$_REQUEST[ "idcloth" ] 		int ou array<int>, le/les tissu(s) sélectionné(s), 					optionnel
 * 		$_REQUEST[ "exit" ] 		-					afficher le contenu du panier ou non,			optionnel
 */


if( isset( $_REQUEST[ "idarticle" ] )){
	if( !is_array( $_REQUEST[ "idarticle" ] ) ){ /* ajout d'une seule référence */
		addBasketItem( 
			
			intval( $_REQUEST[ "idarticle" ] ), 
			isset( $_REQUEST[ "quantity" ] ) ? intval( $_REQUEST[ "quantity" ] ) : 1,
			isset( $_REQUEST[ "idcolor" ] ) ? intval( $_REQUEST[ "idcolor" ] ) 	: 0,
			isset( $_REQUEST[ "idcloth" ] ) ? intval( $_REQUEST[ "idcloth" ] ) 	: 0
			
		);
	
	}
	else{  /* ajout de plusieurs références */

		$i = 0;
		while( $i < count( $_REQUEST[ "idarticle" ] ) ){
		
			addBasketItem(
			
				intval( $_REQUEST[ "idarticle" ][ $i ] ),
				isset( $_REQUEST[ "quantity" ] ) ? intval( $_REQUEST[ "quantity" ][ $i ] ) 	: 1,
				isset( $_REQUEST[ "idcolor" ] ) ? intval( $_REQUEST[ "idcolor" ][ $i ] ) 	: 0,
				isset( $_REQUEST[ "idcloth" ] ) ? intval( $_REQUEST[ "idcloth" ][ $i ] ) 	: 0
				
			);

			$i++;
		
		}
		
	
	}



	Basket::getInstance()->save();
	
	if( isset( $_REQUEST[ "exit" ] ) )
	{
		$response_array = array(
			"ItemCount" => strval(Basket::getInstance()->getItemCount()),
			"TotalET" => strval(Basket::getInstance()->getTotalET())
		);
		Basket::getInstance()->InitDiscountRate();
		// exit( strval( Basket::getInstance()->getItemCount() ) ); /* retourner le nombre d'articles dans le panier */
		exit(json_encode($response_array));
	}
		
	
}
/* ------------------------------------------------------------------------------------------------------------- */

function addBasketItem( $idarticle, $quantity = 1, $idcolor = 0, $idcloth = 0 ){
	
	if( !$quantity && $quantity < 1)
		return;

	$it = Basket::getInstance()->getItems()->iterator();

	while( $it->hasNext() ){
	
		$item =& $it->next();
		
		if( $item->getArticle()->getId() == $idarticle 
			&& ( ( !$idcolor && $item->getColor() ) == NULL || ( $idcolor && $item->getColor() != NULL && $item->getColor()->getId() == $idcolor ) )
			&& ( ( !$idcloth && $item->getCloth() ) == NULL || ( $idcloth && $item->getCloth() != NULL && $item->getCloth()->getId() == $idcloth ) ) ){
				
			$item->setQuantity( $item->getQuantity() + $quantity );

			return null;
				
		}
		
		
	}
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	
	Basket::getInstance()->addArticle( new CArticle( $idarticle, $idcolor, $idcloth ), $quantity );
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/**
 * suppression d'une référence du panier : méthode GET, POST ou COOKIE avec les paramètres suivants :
 * 		$_REQUEST[ "delete" ] 	requis
 * 		$_REQUEST[ "index" ] 	int ou array<int>, requis
 */

//  if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "index" ] ) ){
	
// 	Basket::getInstance()->removeItemAt( intval( $_REQUEST[ "index" ] ) );

// 	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );
// 	exit();
	
// }
/*

if( isset( $_REQUEST[ "delete" ] ) && isset( $_REQUEST[ "index" ] ) ){
	
	$_SESSION["RM_BASKET"] = isset($_SESSION["RM_BASKET"]) ? $_SESSION["RM_BASKET"] : [];
	$_SESSION["RM_BASKET"] [] = Basket::getInstance()->getItemAt( $_REQUEST[ "index" ] )->getArticle()->getId();
	Basket::getInstance()->removeItemAt( intval( $_REQUEST[ "index" ] ) );
	Basket::getInstance()->InitDiscountRate();
	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );
	exit();
	
}
*/


/* ------------------------------------------------------------------------------------------------------------- */
/**
 * Vider le panier : méthode GET, POST ou COOKIE avec les paramètres suivants :
 * 		$_REQUEST[ "reset" ]	vider le panier, requis
 * 		$_REQUEST[ "exit" ] 	afficher le contenu du panier ou non, optionnel
 */

if( isset( $_REQUEST[ "reset" ] ) ){
	
	Basket::getInstance()->removeItems();

	if( isset( $_REQUEST[ "exit" ] ) )
		exit();
	
}
if( isset( $_REQUEST[ "resetbon" ] ) ){
	
	Basket::getInstance()->removeBon();

}

/* ------------------------------------------------------------------------------------------------------------- */
/* traitement du formulaire du panier */
//var_dump($_REQUEST);die;
if( isset( $_POST[ "update" ] ) ){

	$index = 0;
	//var_dump($_POST[ "quantity" ]);die;

	foreach( $_POST[ "quantity" ] as $idarticle=>$quantity ){
		
		/*if($quantity == 0){
			
			$_SESSION["RM_BASKET"] = isset($_SESSION["RM_BASKET"]) ? $_SESSION["RM_BASKET"] : [];
			$_SESSION["RM_BASKET"] [] = Basket::getInstance()->getItemAt( $index )->getArticle()->getId();
			Basket::getInstance()->removeItemAt($index);
			continue;
		}*/

		//$quantity = max( 1, Basket::getInstance()->getItems()->get( $index )->getArticle()->get( "min_cde" ), intval( $quantity ) );
		

		//Basket::getInstance()->getItemAt( $index )->setQuantity( intval( $quantity ) );
		Basket::getInstance()->getItemFromIdArticle($idarticle )->setQuantity( intval( $quantity ) );

		//$index++;
		
	}

	//var_dump(Basket::getInstance()->getItems());die;
	
	Basket::getInstance()->save();
	Basket::getInstance()->InitDiscountRate();
	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );
	exit();
	
}

/* ------------------------------------------------------------------------------------------------------------- */

header( "Content-Type: text/html; charset=utf-8" );

include_once( "$GLOBAL_START_PATH/objects/GiftToken.php" );
include_once( "$GLOBAL_START_PATH/objects/Voucher.php" );

/*if( isset( $_REQUEST[ "gift_token_serial" ] ) ) //chèque-cadeau
	$BuyingScript .= "?gift_token_serial=" . urlencode( stripslashes( $_REQUEST[ "gift_token_serial" ] ) );*/
	
if( isset( $_REQUEST[ "voucher_code" ] ) ){ //bon de réduction
	
	$BuyingScript .= isset( $_REQUEST[ "gift_token_serial" ] ) ? "&" : "?";	
	$BuyingScript .= "voucher_code=" . urlencode( stripslashes( $_REQUEST[ "voucher_code" ] ) );

}

/* ------------------------------------------------------------------------------------------------------------- */
//Cas d'une composition ou de la venue d'une compo

include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );


if(isset($_REQUEST['checkArticle'])){

	foreach($_REQUEST['checkArticle'] as $idprod){
		
	    if( is_array($_REQUEST["selectRef_".$idprod]) ) {
	    	
	    	if( !empty($_REQUEST['commande_x']) && !empty($_REQUEST['order']) ) {
	    		
	    		foreach( $_REQUEST['order'] as $k=>$order ) {
	    
	    			if($qtt > 0 )
	    				Basket::getInstance()->addArticle( new CArticle( $_REQUEST["selectRef_".$idprod][$k] ), $_REQUEST['quantity']['order'][$k] );					
	    		}	
	    	}
	    	
	    }else{
		
	    	$idcloth = isset($_REQUEST['idtissus']) ? $_REQUEST['idtissus'] : 0;
		    $idcolor = isset($_REQUEST['idcolor']) ? $_REQUEST['idcolor'] : 0;
			
			Basket::getInstance()->addArticle( new CArticle( $_REQUEST["selectRef_".$idprod], $idcolor, $idcloth ), $_REQUEST['quantity'] );
	
	    }

	}
	
}

/* ------------------------------------------------------------------------------------------------------------- */
/* Controller */

include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketItemProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CCategoryProxy.php" );


class BasketController extends PageController{};

$basketController = new BasketController( "basket.tpl" );
Basket::getInstance()->InitDiscountRate();
$basketController->setTypeEncoding(2);

$_SESSION["menuTypeEncoding"] = 2;
$basketController->setData( "category", new CCategoryProxy( new CCategory( 0 ) ) );

$basketController->setData( "title", htmlentities(Dictionnary::translate("basket_title")));
$basketController->setData( "meta_description",  Dictionnary::translate("basket_description") );

$items = array();
$itemsg = array();
$itemsb = array();
$it = Basket::getInstance()->getItems()->iterator();

//print_r($it);exit;

while( $it->hasNext() ){
	$next = $it->next();

	array_push( $items, new BasketItemProxy( $next ) );
}

Basket::getInstance()->resetArticleGift();

$pdg = new ProductGift($items, Basket::getInstance());
$pdg->sortItem();
foreach($pdg->getArticleGiftInCart() as $idArticle){
	Basket::getInstance()->addArticleGift( new CArticle( $idArticle ) );
}

$itg = Basket::getInstance()->getItemsGift()->iterator();
while( $itg->hasNext() ){
	$next = $itg->next();
	array_push( $itemsg, new BasketItemProxy( $next, true ) );
}

$pdb = new product_basket();

foreach($pdb->getItemBasket() as $idArticle){
	Basket::getInstance()->AddArticleBasket( new CArticle( $idArticle ) );
	array_push( $itemsb, new BasketItemProxy( new BasketItem(new CArticle($idArticle))) ) ;
}
//print_r($items);exit;
// point 60


$basketController->setData( "auto_estimate", DBUtil::getParameter( "auto_estimate" ) );
$basketController->setData( "basket", new BasketProxy() );
$basketController->setData( "basketItems", $items );
$basketController->setData( "basketItemsGift", $itemsg );
$basketController->setData( "basketItemsBasket", $itemsb );
//----------------------------------------------------------------------------

$articlesET = 0.0;
$it = Basket::getInstance()->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();

$articlesATI = 0.0;
$it = Basket::getInstance()->getItems()->iterator();
while( $it->hasNext() )
	$articlesATI += $it->next()->getTotalATI();

/* ------------------------------------------------------------------------------------------------------------- */
/*
 * Réduction automatique à la première commande
 * Traité comme une remise commerciale
 */

$idbuyer 					= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
$firstOrder 				= $idbuyer && DBUtil::query( "SELECT COUNT(*) AS `count` FROM `order` WHERE idbuyer = '$idbuyer' AND `status` NOT LIKE 'Cancelled'" )->fields( "count" ) == 0;

$basketController->setData( "first_order", $firstOrder );

/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

 * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction * 
 
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

if( isset( $_REQUEST[ "voucher_code" ])||isset( $_SESSION[ "voucher" ])){

if( isset( $_REQUEST[ "voucher_code" ]))
$votchecode=$_REQUEST[ "voucher_code" ];
else
 $votchecode=$_SESSION[ "voucher" ];

 $vouchers = false;
//--------- vérifier si le bon de réduction exist --------------//
	if( DBUtil::query( "SELECT 1 FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) ) . " LIMIT 1" )->RecordCount() )
	 {
	 $basketController->setData( "voucher", true );
	 $vouchers = true;
	 $_SESSION[ "voucher" ] =  $votchecode;
	 }
	else
	$basketController->setData( "voucher", false );
	//-------------- vérifier les dates début et de fin -------------//
	$dates = date("Y-m-d");
	$voucherdates = false;
	if( DBUtil::query( 'SELECT 1 FROM discount_voucher WHERE  date_start < "'.$dates.'" AND date_end > "'.$dates.'" AND code = ' . DBUtil::quote( stripslashes( $votchecode ) ) . " LIMIT 1" )->RecordCount() )
	{
	 $basketController->setData( "voucherdate", true );
	 $voucherdates = true;
	 }
	else
	$basketController->setData( "voucherdate", false );
	//-------------------------vérifier si le bon de réduction n'est pas utiliser par le mme client -------------//
	
	if( isset( $_SESSION[ "idbuyer" ]))
$idbuyer = urldecode($_SESSION[ "idbuyer" ]);
	
	$req = "SELECT voucher_code FROM `order` WHERE voucher_code = ".DBUtil::quote( stripslashes( $votchecode ) )." AND idbuyer = " . DBUtil::quote( stripslashes( $idbuyer ) )." ";
	$res = DBUtil::query($req);

	$voucher_code = $res->fields("voucher_code");
	$basketController->setData( "idbuyer"  ,$idbuyer );
	
if ( $res->RecordCount() > 0 ) {
	$basketController->setData( "voucherexist", false );
	}
	else {
	$basketController->setData( "voucherexist", true );
	}
	
	
	//---------------- vérifier si montant de la commande > amount_min --------------//

	  $voucheramountmins = false;
	$req = "SELECT amount_min FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) )." ";
	$res = DBUtil::query($req);

	$amount_min = $res->fields("amount_min");
	$basketController->setData( "amount_min"  ,$amount_min );
	
	if( $amount_min > 0 ){
			$order_amount = 0.0;
			//$it = $this->items->iterator();
			$it = Basket::getInstance()->getItems()->iterator();
			while( $it->hasNext() ){
				
				if( B2B_STRATEGY )
						$order_amount += $it->next()->getTotalET();
				else 	$order_amount += $it->next()->getTotalATI();
				
			}
	$basketController->setData( "order_amount"  ,$order_amount );
	
			if( $amount_min < $order_amount ){
			 $basketController->setData( "voucheramountmin", true );
			 $voucheramountmins = true;
			}else {	$basketController->setData( "voucheramountmin", false ); }
		
		}
		else  { $basketController->setData( "voucheramountmin", true );
		$voucheramountmins = true;
		 }

//--------------------------------------------------------Cohérence avec produits et catégories---------------------//
		
		$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) )." ";
	$ress = DBUtil::query($reqs);
	$idcategory = $ress->fields("idcategory");
	$basketController->setData( "idcategory"  ,$idcategory );
	$idproduct = $ress->fields("idproduct");
	$basketController->setData( "idproduct"  ,$idproduct );
	$type = $ress->fields("type");
	$basketController->setData( "type"  ,$type );		
	$reference = $ress->fields("reference");
	$basketController->setData( "reference"  ,$reference );	
	
	$reqs = "SELECT reference,sellingcost FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$sellingcost = $ress->fields("sellingcost");
	$basketController->setData( "sellingcost"  ,$sellingcost );	
	$exist = 0; $exists = 0;
	$order_amount = 0.0;
	$basketController->setData( "voucherproduct", false );
	$basketController->setData( "voucherpromo", true );
	for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
	
	$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
	//$references = $it->next()->getReference();
	$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
	$rs = DBUtil::query($rq);
	$references = $rs->fields("reference");
	$dates = date("Y-m-d");
	$date_promo=date("Y-m-d H:i:s");
	$q = "SELECT reference FROM promo WHERE reference = '".$references."' and  begin_date < '".$date_promo."' AND end_date > '".$date_promo."'  ";
	$rs = DBUtil::query($q);
	$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	$tab1=array();
	$idcatparent =$idcategorys;
	while($idcatparent!=0){
	array_push($tab1,$idcatparent);
	$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
	$rs2 = DBUtil::query($q2);
	$idcatparent =$rs2->fields("idcategoryparent") ;
	}
	if ( $rs->RecordCount() > 0 ) {
	$ref = $rs->fields("reference");
	$basketController->setData( "references", $ref );
	
	$basketController->setData( "voucherpromo", false );
	}
	else {
	
		$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	
	
	$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
	$rs = DBUtil::query($q);

	$idcategoryparent = $rs->fields("idcategoryparent");
	
	if( B2B_STRATEGY )
			$amountht = Basket::getInstance()->getItemAt( $i)->getTotalET();
		else 	 
		$amountttc = Basket::getInstance()->getItemAt( $i)->getTotalATI(); 
if( $idproduct > 0  ){
	

	if($idproduct ==  $idprod)
	{
	if($type == "rate"){
			if( B2B_STRATEGY )
				 $order_amount =  $amountttc;		
				else	
			$order_amount = $amountht;
	
		$reduction = (($order_amount * $sellingcost)/100);
		$basketController->setData( "reduction"  , Util::priceFormat($reduction) );
	}
	if($type == "amount"){
			if( B2B_STRATEGY )
				$reduction =  $sellingcost;	
				else
			$reduction =  $sellingcost / 1.196;	
	
		
		$basketController->setData( "reduction"  , Util::priceFormat($reduction) );
	}
	 $basketController->setData( "voucherproduct", true );
	}	
 }
	else  { //$basketController->setData( "voucherproduct", true );
	
		if( $idcategory > 0 ){
		
		//if( $idcategory ==  $idcategoryparent ){
if(in_array($idcategory,$tab1)){
		if($type == "rate"){
			if( B2B_STRATEGY )
					 $order_amount +=$amountht;	
				else	
			$order_amount += $amountttc;
		$reduction = (($order_amount * $sellingcost)/100);
		
		$basketController->setData( "reduction"  , Util::priceFormat($reduction) );
		
	}
		if($type == "amount"){
			if( B2B_STRATEGY )
			$reduction =  $sellingcost;	
				else	
				$reduction =  $sellingcost / 1.196;	
		$basketController->setData( "reduction"  , Util::priceFormat($reduction) );
	}
	 $basketController->setData( "voucherproduct", true );
	 
	 }
	 
	 }
		else  { $basketController->setData( "voucherproduct", true ); }
		}
	
}	
}
	
$basketController->setData( "voucher_code", isset( $votchecode ) ? strip_tags( stripslashes(  $votchecode ) ) : false );

if(Basket::getInstance()->setUseVoucher( $votchecode )){
$basketController->setData( "vouchervalider", true );
}else  { $basketController->setData( "vouchervalider", false ); }
		
/* Afficher ligne bon de reduction */
if(Basket::getInstance()->setUseVoucher( $votchecode )){

	$req = "SELECT reference FROM discount_voucher WHERE code = '" .  stripslashes( $votchecode )."' ";
	$res = DBUtil::query($req);

	$reference = $res->fields("reference");
	
	$reqs = "SELECT * FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$idarticle = $ress->fields("idarticle");
	
	$basketController->setData( "reference", $reference );
	$summary = $ress->fields("summary_1");
	$basketController->setData( "summary", $summary );
$basketController->setData( "code", $votchecode );


/* ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- //

 * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction *  * Bon de réduction * 
 
// ------------------------------------------------------------------------------------------------------------------------------------------------------------------------- */

		$reduce = 0.0;
	
	
	$reqs = "SELECT idproduct,idcategory,type,reference FROM discount_voucher WHERE code = " . DBUtil::quote( stripslashes( $votchecode ) )." ";
	$ress = DBUtil::query($reqs);
$idcategory = $ress->fields("idcategory");
	
	$idproduct = $ress->fields("idproduct");

	$type = $ress->fields("type");
		
	$reference = $ress->fields("reference");
		
	
	$reqs = "SELECT reference,sellingcost,summary_1 FROM detail WHERE reference = '".$reference."' ";
	$ress = DBUtil::query($reqs);
	$sellingcost = $ress->fields("sellingcost");
	
	$exist = 0; $exists = 0;
	$order_amount = 0.0;
	
	for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
	
	$idprod =  Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" );
	
	$rq = "SELECT reference FROM detail WHERE idproduct = '".$idprod."' ";
	$rs = DBUtil::query($rq);
	$references = $rs->fields("reference");
	$dates = date("Y-m-d");
	$date_promo=date("Y-m-d H:i:s");
	$q = "SELECT reference FROM promo WHERE reference = '".$references."' and  begin_date < '".$date_promo."' AND end_date > '".$date_promo."' ";
	$rs = DBUtil::query($q);
	$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	$tab1=array();
	$idcatparent =$idcategorys;
	while($idcatparent!=0){
	array_push($tab1,$idcatparent);
	$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = '" . $idcatparent."' ";
	$rs2 = DBUtil::query($q2);
	$idcatparent =$rs2->fields("idcategoryparent") ;
	}
	if ( !$rs->RecordCount() ) {
	
	
		$qqs = "SELECT idcategory FROM product WHERE idproduct = '".$idprod."' ";
	$results = DBUtil::query($qqs);

	$idcategorys = $results->fields("idcategory");
	
	
	$q = "SELECT idcategoryparent FROM category_link WHERE idcategoryparent = '" . $idcategorys."'  OR idcategorychild = '" . $idcategorys."' ";
	$rs = DBUtil::query($q);

	$idcategoryparent = $rs->fields("idcategoryparent");
	
	if( B2B_STRATEGY )
			$amountht = Basket::getInstance()->getItemAt( $i)->getTotalET();
		else 	 
		$amountttc = Basket::getInstance()->getItemAt( $i)->getTotalATI(); 
if( $idproduct > 0  ){
	

	if($idproduct ==  $idprod)
	{
	if($type == "rate"){
			if( B2B_STRATEGY )
						$order_amount = $amountht;
				else	
			 $order_amount =  $amountttc;
	
		 $reduce = (($order_amount * $sellingcost)/100);
	
	}
	if($type == "amount"){
			if( B2B_STRATEGY )
				$reduce =  $sellingcost ;		
				else	
			$reduce =  $sellingcost / 1.196;
	
		
		
	}
	
	}	
 }
	else  { 
	
		if( $idcategory > 0 ){
		
		//if( $idcategory ==  $idcategoryparent ){
			if(in_array($idcategory,$tab1)){
		if($type == "rate"){
			if( B2B_STRATEGY )
						$order_amount += $amountht;
				else	
			 $order_amount +=  $amountttc;
		 $reduce = (($order_amount * $sellingcost)/100);
		
	}
		if($type == "amount"){
			if( B2B_STRATEGY )
				$reduce +=  $sellingcost ;		
				else	
			$reduce +=  $sellingcost / 1.196;
		
		
	}

	 
	 }
	 
	 }
	 else// modif ched 22-02
	 {
	 if($type == "rate"){
			if( B2B_STRATEGY )
						$order_amount += $amountht;
				else	
			 $order_amount +=  $amountttc;
		 $reduce = (($order_amount * $sellingcost)/100);
		
	}
		if($type == "amount"){
			if( B2B_STRATEGY )
				$reduce +=  $sellingcost ;		
				else	
			$reduce +=  $sellingcost / 1.196;
		
		
	}
	 }//fin de modif
		
		}
	
}	
}
	
	}
	}
/* ------------------------------------------------------------------------------------------------------------- */
/*
 * Chèque-cadeau
 * Considéré comme un mode de règlement donc l'utilisation du chèque-cadeau ne modifie pas les montants du panier
 */

$gift_token_amount 		= 0.0;
$gift_token_serial 		= isset( $_REQUEST[ "gift_token_serial" ] ) ?stripslashes( $_REQUEST[ "gift_token_serial" ] ) : "";
$isAvailableGiftToken 	= GiftToken::isAvailableGiftToken( $gift_token_serial );

$basketController->setData( "gift_token_serial"			,$gift_token_serial );
$basketController->setData( "gift_token_error"			,strlen( $gift_token_serial ) && !$isAvailableGiftToken ? Dictionnary::translate( "gift_token_unavailable" ) : ""  );
$basketController->setData( "display_gift_token_serial"	,isset( $_REQUEST[ "gift_token_serial" ] ) );

$basketController->setData( "gift_token_amount"			,0.0 );

//utilisation du chèque-cadeau

if( $gift_token_serial && $isAvailableGiftToken ){
		
	$query = "
	SELECT gtm.value 
	FROM gift_token_model gtm, gift_token gt 
	WHERE gt.serial = '" . Util::html_escape( $gift_token_serial ) . "'
	AND gtm.type LIKE '" . GiftToken::$TYPE_AMOUNT . "' 
	AND gt.idtoken_model = gtm.idmodel
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );

	$gift_token_amount = $rs->RecordCount() ? round( $rs->fields( "value" ), 2 ) : 0.0;
	
	$basketController->setData( "gift_token_amount"	, Util::numberFormat( $gift_token_amount ) );
	
}	

/* ------------------------------------------------------------------------------------------------------------- */
/* montants */

$articlesET = 0.0;
$it = Basket::getInstance()->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();
	
	$chargesET 	= Basket::getInstance()->getChargesET();
	$chargesATI = Basket::getInstance()->getChargesATI();

	if( B2B_STRATEGY ){
			$reduceht = 	$reduce;
			$reducettc =  $reduce +($reduce * Basket::getInstance()->getVATRate()) / 100;		
				}else	{
			$reducettc =  $reduce;
			$reduceht = 	$reduce /1.196;
	}

$netapayer =  max( Basket::getInstance()->getNetBill() - $gift_token_amount, 0.0 );

//$netapayers = $netapayer - $reduce;
//$netapayer_ht = $netapayers - ( ($netapayers * 16.39) / 100);

if( B2B_STRATEGY ){
			$netapayers = ($articlesATI + $chargesATI ) - $reducettc;
			$netapayer_ht = ($articlesET + $chargesET ) - $reduceht ;	
				}else	{
			$netapayers = ($articlesATI + $chargesATI )- $reducettc;
			$netapayer_ht = ($articlesATI + $chargesATI )- $reduceht ;
	}
//----------------------------------------------------------------------------
/* montants panier récupéré après abandon s'il y a réduction
*/

if(isset($_SESSION[ "id_cart" ]))
{
	
	$reduction_val = DBUtil::getDBValue('reduction', 'cart', 'id_cart', $_SESSION[ "id_cart" ]);
	if($reduction_val && (float)$reduction_val != (float)0)
	{
		$basket_items = array();
		for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++)
			$basket_items[] = Basket::getInstance()->getItemAt( $i)->getArticle()->getId();
			
		$ci = DBUtil::query("SELECT id_article FROM `cart_detail` WHERE `id_cart` = ".$_SESSION[ 'id_cart' ]);
		$cart_items = array();
		$cartInBasket = 0;
		while(!$ci->EOF)
		{
			if (in_array($ci->fields["id_article"], $basket_items))
				$cartInBasket += 1;
			
			$ci->MoveNext();
		}
		if($cartInBasket != $ci->RecordCount())
		{
			$reduction_type = DBUtil::getDBValue('reduction_type', 'cart', 'id_cart', $_SESSION[ "id_cart" ]);
			if ($reduction_type == 0)
				$reduction = $reduction_val;//Réduction calculée par montant
			elseif ($reduction_type == 1)
				$reduction = $articlesET * $reduction_val / 100;//Réduction calculée par pourcentage
			
			if( B2B_STRATEGY ){
				$netapayers = $articlesATI + $chargesATI - $reduction * (1 + Basket::getInstance()->getVATRate() / 100) ;
				$netapayer_ht = $articlesET + $chargesET - $reduction;
			}
			else
			{
				$netapayers = $articlesATI + $chargesATI - $reduction * (1 + Basket::getInstance()->getVATRate() / 100) ;
				$netapayer_ht = $articlesATI + $chargesATI - $reduction;
			}
			$basketController->setData( "reduction_ht"  , $reduction);
		}
		else
			DBUtil::getConnection()->Execute("UPDATE cart SET `reduction` = 0.0 WHERE `id_cart`=".$_SESSION[ "id_cart" ]);
		
	}
}

$basketController->setData( "remise_rate", 					Util::rateFormat( Basket::getInstance()->getDiscountRate() ) );
$basketController->setData( "total_discount", 				Util::rateFormat( Basket::getInstance()->getDiscountRate() ) );
$basketController->setData( "discount", 					Util::priceFormat( Basket::getInstance()->getDiscountAmount() ) );
$basketController->setData( "total_discount_amount",		Util::priceFormat( Basket::getInstance()->getDiscountAmount() ) );
$basketController->setData( "total_amount_ht", 				Util::numberFormat( $articlesET - Basket::getInstance()->getDiscountAmount() ) );
$basketController->setData( "total_amount_ht_wb", 			Util::priceFormat( Basket::getInstance()->getTotalET() ) );
$basketController->setData( "total_amount_ttc_wbi", 		Util::priceFormat( Basket::getInstance()->getNetBill() ) );
$basketController->setData( "total_amount_ht_avant_remise", Util::priceFormat( $articlesET ) );
$basketController->setData( "remise", 						Basket::getInstance()->getDiscountRate() > 0.0 );
$basketController->setData( "net_to_pay", 				    Util::numberFormat($netapayers)	 );
$basketController->setData( "net_to_pays", 				    Util::numberFormat(max( Basket::getInstance()->getNetBill() - $gift_token_amount, 0.0 )));
$basketController->setData( "net_to_pay_ht", 				Util::numberFormat($netapayer_ht));

if ($reduce > 0.0)
	$basketController->setData( "reduction", Util::priceFormat($reduce));

/* frais de port */

if( $articlesET == 0.0 ){ //ne pas afficher les frais de port si prix cachés

	$basketController->setData( "total_charge_ht", "-" );
	$basketController->setData( "total_charge_ttc", "-" );
	
}
else{
	
	$chargesET 	= Basket::getInstance()->getChargesET();
	$chargesATI = Basket::getInstance()->getChargesATI();
	
	$basketController->setData( "total_charge_ht", $chargesET > 0.0 ? Util::priceFormat( $chargesET ) : "FRANCO" ); 
	$basketController->setData( "total_charge_ttc", $chargesATI > 0.0 ? Util::priceFormat( $chargesATI ) : "FRANCO" ); 
	
	$basketController->setData( "franco", $chargesET == 0.0 );
		
}

//----------------------------------------------------------------------------

$hasLot = false;
$i = 0;
while( $i < Basket::getInstance()->getItemCount() && !$hasLot ){
	
	if( Basket::getInstance()->getItemAt( $i)->getArticle()->get( "lot" ) > 1 )
		$hasLot = true;
		
	$i++;
	
}

$basketController->setData( "hasLot", $hasLot );
$basketController->setData( "hasDiscount", "" );

$basket_article = array();
$j = 0;
//var_dump(Basket::getInstance()->getItemAt( 0)->getArticle());
//var_dump(Basket::getInstance()->getItemAt(0)->getDiscountRate() ); die;

for ($i=0 ; $i < Basket::getInstance()->getItemCount() ; $i++){
	$idCategory = Basket::getInstance()->getItemAt($i)->getArticle()->GetIdCategory();
	$idCategory1 = $i == 0 ? Basket::getInstance()->getItemAt($i)->getArticle()->GetIdCategory() : Basket::getInstance()->getItemAt($i-1)->getArticle()->GetIdCategory();
	$idarticle = Basket::getInstance()->getItemAt($i)->getArticle()->getId();
	$basket_article[$i]["i"] = $i;

	//Recherche des accessoires pour les produits du panier
	$basket_article[$i]["associates"]= $associates = getProductAssoc( Basket::getInstance()->getItemAt( $i)->getArticle()->get( "idproduct" ) );
	$basket_article[$i]["hasAssociates"] = count($associates) ? "ok" : "";

	//var_dump(Basket::getInstance()->getItemAt($i)->getDiscountRate()); die;
    //Basket::getInstance()->InitDiscountRate();
	
	$rate_i = Basket::getInstance()->getItemAt( $i)->getDiscountRate();
	if($idCategory==162)
		print_r($rate_i);echo "<br/>";
	if( Basket::getInstance()->getItemAt( $i)->getDiscountRate() > 0.0 ) {
        $basketController->setData("hasDiscount", "ok");
    }

}

$basketController->setData('basket_articles',$basket_article);

//----------------------------------------------------------------------------
//basket_buttons

/* produits complémentaires */
include_once( dirname( __FILE__ ) . "/../objects/catalog/CProduct.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CProductProxy.php" );
$complements = array();
foreach ($items as $item)
{
	$basket_product_id = $item->get('idproduct');
	$basket_product_proxy = new CProductProxy( new CProduct( intval( $basket_product_id ) ) );
	$complements_parts = $basket_product_proxy->getComplements();
	if (count($complements_parts))
		foreach ($complements_parts as $complements_part)
			array_push($complements, $complements_part);
}
if (count($complements))
	$basketController->setData('complements',$complements);
if (count($complements) > 2)
	$basketController->setData('slide_auto', true);
else
	$basketController->setData('slide_auto', false);
/* fin produits complémentaires */

allowOrder( $basketController );

$basketController->output( array() );

//---------------------------------------------------------------------------------------------
//option1: is gift, move on top

/**
 * Interdire la commande pour la Corse, les DOM-TOMs
 * car le port ne peut pas être calculé automatiquement
 */
function allowOrder( &$controller ){
	
	$controller->setData( "allowOrder", true );
	$controller->setData( "orderRestrictionPromo", false );
	$controller->setData( "orderRestrictionState", false );
				
	global $GLOBAL_START_PATH;
	
	//promo ou destockage
	
	$it = Basket::getInstance()->getItems()->iterator();
	
	while( $it->hasNext() ){
		
		$item =& $it->next();
		
		if(/* $item->get( "hasPromo" ) 
			|| $item->get( "isDestocking" ) 
			|| Product::getReferenceAttribute( $item->get( "reference" ), "type" ) == "destocking" 
			||*/ $item->getArticle()->get( "sellingcost" ) == 0.0 
			|| $item->getArticle()->get( "hidecost" ) == 1 ){
				
				$controller->setData( "allowOrder", false );
				$controller->setData( "orderRestrictionPromo", true );
				return $condition;
		
		}
			
	}
	
	if( !Session::getInstance()->getCustomer() )
		return;
		
	//strangers
	
	if( Session::getInstance()->getCustomer()->get( "idstate" ) != 1 ){
		
		$controller->setData( "allowOrder", false );
		$controller->setData( "orderRestrictionState", true );
		return $condition;
		
	}

	//corse
	
	if( strlen( Session::getInstance()->getCustomer()->get( "zipcode" ) ) > 2 && substr( Session::getInstance()->getCustomer()->get( "zipcode" ), 0, 2 ) == "20" ){
		
		$controller->setData( "allowOrder", false );
		$controller->setData( "orderRestrictionState", true );
		return $condition;
		
	}

}

//---------------------------------------------------------------------------------------------
/**
 * Genere la liste des produits associés (accessoires)
 */
function getProductAssoc( $idproduct ){

	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	$query = "
	SELECT d.idproduct, d.reference, d.summary_1, d.idarticle, d.sellingcost, d.designation_1, d.min_cde
	FROM associat_product ap, detail d
	WHERE ap.idproduct='$idproduct'
	AND d.idproduct = ap.associat_product
	";
	
	$rsAssociate =& DBUtil::query( $query );
	
	if( $rsAssociate === false )
		die( "Impossible de récupérer les produits associés" );
		
	$associates = array();
	$i = 0;
	
	while( !$rsAssociate->EOF() ){
			
		//Idproduit			
		$idProductAssociate=$rsAssociate->fields( "idproduct" );
		$associates[$i]["idproduct"]= $idProductAssociate;
		
		//Idarticle
		$idatricleAssociate=$rsAssociate->fields("idarticle");
		$associates[$i]["idarticle"]= $idatricleAssociate;
		
		//Nom du produit
		$associates[$i]["name"]= $rsAssociate->fields( "summary_1" );
		
		//reference
		$associates[$i]["ref"]= $rsAssociate->fields( "reference" );
		
		//Icones
		$associates[$i]["icone"]= URLFactory::getProductImageURI( $rsAssociate->fields( "idproduct" ), URLFactory::$IMAGE_ICON_SIZE );

		//Prix de l'article			
		$hidecost = $rsAssociate->fields( "sellingcost" );
		$associates[$i]["WotPrice"]= $hidecost;
		
		//designation
		$associates[$i]["designation"]= $rsAssociate->fields( "designation_1" );

		$i++;
		$rsAssociate->MoveNext();
		
	}
	return $associates;
	
}
//---------------------------------------------------------------------------------------------
?>