<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage informations groupements
 */


include_once('../objects/classes.php');  
require_once 'PEAR.php';

//include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );

// include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
// include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

//class GroupingController extends WebPageController{};
class GroupingController extends PageController{};
$grouping = new GroupingController( "/account/grouping_catalog.tpl" , "Votre referencement" );

$grouping->GetPromotions();
//$grouping->GetCategoryLinks();
//$grouping->GetBrands();
//$grouping->GetTrades();
$grouping->GetProductHistory();


include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");
include_once("$GLOBAL_START_PATH/objects/buyerobject.php");
//On vérifie qu'il y a un utilisateur de connecté et qu'il appartient bien à un groupe
if( Session::getInstance()->getCustomer() ) {
$idbuyer =  Session::getInstance()->getCustomer()->getId();

$idgrouping = GroupingCatalog::getBuyerGrouping($idbuyer);
}
if($idbuyer && isset($idgrouping) ){

	$GroupingCatalog = new GroupingCatalog();
	//$Buyer =  Session::getInstance()->getDeprecatedBuyer();
	//$Buyer =  DEPRECATEDBUYER::GetBuyerInfo($idbuyer);
//$Buyer = $GroupingCatalog->getGroupingInfos($idbuyer );
		$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
		$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$grouping ->setData( "contact_grouping", $contact_grouping );


$Buyer =   new DEPRECATEDBUYER();
	$references = $GroupingCatalog->setGroupingReferencesDatas();
	$i=0;
	$tab=array();
	$tab3=array();
	foreach ($references as $value) {
    $idproduct=$value["idproduct"];

    $query1 = "SELECT c.`idcategory`,c.`name_1` FROM product p inner join category c on p.`idcategory`=c.`idcategory` where p.`idproduct`=$idproduct";
		$rs1 = DBUtil::query( $query1 );
		if($rs1->RecordCount()){
			$idcategory = $rs1->fields("idcategory");

			$namecat = Util::doNothing($rs1->fields("name_1"));
			$references[$i]["idcat"]=$idcategory;
			$references[$i]["name_cat"]= Util::doNothing($namecat);
	
			$idcatparent =$idcategory; 
			$idcategorychild=$idcategory;
				while($idcatparent!=0){
				$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = $idcatparent ";

				$rs2 = DBUtil::query($q2);
				$idcatparent =$rs2->fields("idcategoryparent") ;
				$idcategorychild=$rs2->fields("idcategorychild") ;;
				}
			$i++;
			array_push($tab, $idcategory);
			array_push($tab3, $idcategorychild);
		
}}
$tab3=array_unique($tab3);
$tab3=array_values($tab3);
$tab=array_unique($tab);
$tab2=array_values($tab);
for($k=0;$k<count($tab2);$k++)
{

$idcat=$tab2[$k];
$result[$k]["idcat"]=$idcat;
$query2 = "SELECT name_1 FROM  category  where idcategory=$idcat";
		$rs2 = DBUtil::query( $query2 );

	
		if ($rs2->RecordCount()) {

			$namecat = $rs2->fields("name_1");
			$result[$k]["name_cat"]= Util::doNothing($namecat);
		}
			else $result[$k]["name_cat"]="";

$idcatparent =$idcat; 
$idcategorychild=$idcat;
	while($idcatparent!=0){
	$q2 = "SELECT idcategoryparent,idcategorychild FROM category_link WHERE idcategorychild = $idcatparent ";

	$rs2 = DBUtil::query($q2);
	$idcatparent =$rs2->fields("idcategoryparent") ;
	$idcategorychild=$rs2->fields("idcategorychild") ;;
	}
$query2 = "SELECT idcategory,name_1 from category  where idcategory=$idcategorychild";
		$rs11 = DBUtil::query( $query2 );
		if($rs11->RecordCount()){
			$namecat_pa = $rs11->fields("name_1");
			$url = URLFactory::getCategoryURL($idcategorychild);
			$result[$k]["id_cat_parent"]=$idcategorychild;
			$result[$k]["name_cat_parent"]=$namecat_pa;

			$result[$k]["url_cat_parent"]=$url;
		}
		else 
			{
			$result[$k]["name_cat_parent"]="";

			}

$j=0;
foreach ($references as $value) {
	
	if($idcat==$value["idcat"])
	{
		$result[$k]["article"][$j] = $value;
		$j++;
	}

}
}
$result1 = array();
for($k=0;$k<count($tab3);$k++)
{
$idcat=$tab3[$k];
$result1[$k]["idcat"][$j]=$idcat;
$url = URLFactory::getCategoryURL($idcat);
$result1[$k]["url_cat"]=$url;
$query22 = "SELECT name_1 FROM  category  where idcategory=$idcat";
		$rs22 = DBUtil::query( $query22 );

	
		if ($rs22->RecordCount()) {

			$namecat = $rs22->fields("name_1");
			$result1[$k]["nameCat"]= Util::doNothing($namecat);
		}
			else $result[$k]["nameCat"]="";

$j=0;
foreach ($result as $value) {

	
	if($idcat==$value["id_cat_parent"])
	{
		$result1[$k]["categorie"][$j]=$value;
		$j++;
	}

}

}

//$result=array_sort($result, 'name_cat', SORT_ASC);
$references=$result1;
//var_dump($references);exit();
  $categorys = $GroupingCatalog->setGroupingcategory();
  $grouping->setData('categorys', $categorys); 
	$grouping->setData('references', $references); 
	
	$logo = $GLOBAL_START_URL."/".$GroupingCatalog->getGrouping("grouping_logo");
	
	$grouping->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$grouping->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$grouping->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$grouping->setData('issetPostLoginAndNotLogged', "ok");
	$titre = $GroupingCatalog->getGrouping("grouping");
	$grouping->setData('logo', $logo);
	$grouping->setData('titre', $titre);
	$grouping->setData('buyerTitle', Util::getTitle($Buyer->getvalue( "title" )));
	$grouping->setData('firstname',$Buyer->getvalue("firstname"));
	$grouping->setData('lastname',$Buyer->getvalue("lastname"));
	$grouping->setData('address',$Buyer->getvalue("adress"));
	if($Buyer->getvalue("adress_2")==''){
		$grouping->setData('address_2','');
	}else{
		$grouping->setData('address_2',$Buyer->getvalue("adress_2"));
	}
	$grouping->setData('zipcode',$Buyer->getvalue("zipcode"));
	$grouping->setData('city',$Buyer->getvalue("city"));
	$grouping->setData('company',$Buyer->getvalue("company"));
	
	$grouping->setData('scriptName', 'grouping_catalog.php');
	// Initialiser showCanap
	$grouping->setData('showCanap','');
	//print "ZZZ";exit;
		$refs_not_found = false;
$list_articles_not_found = array ();
$list_refs_hide_prices = array ();

$hasrefwoprice = false;

$nb_lignes = 10;



$nb_ref_not_found = 0;
for ($i = 0; $i < $nb_lignes; $i++) {

	$articles[$i]["reference"] = "";
	$articles[$i]["quantity"] = "";
	$articles[$i]["i"] = $i;
}

$articles = array ();
	if (isset ($_POST['validated'])) {
//IdArticles[]


	for ($i = 0; $i < $nb_lignes; $i++) {
	//print $_POST['ref_'.$i];exit;
	//$nbr =  $_POST['check_'.$i];
	//print $nbr;
	
		$articles[$i]["reference"] = $_POST['ref_'.$i];
		$articles[$i]["quantity"] = $_POST['quantity_'.$i];
	}

	$notrefs = array (); //numéros qui ne sont pas des références
	$refwoprice = array (); //références dont le prix est caché

	$n = 0;

	for ($i = 0; $i < 10; $i++) {

		//si on a une quantite d'indiqué et une référence d'indiqué
		if ($articles[$i]['quantity'] > 0 && trim($articles[$i]['reference']) != '') {

			//vérifier si le prix est dispo (hidecost == 0 )
			$query = "SELECT hidecost FROM detail WHERE reference LIKE '" . $articles[$i]['reference'] . "' LIMIT 1";

			$base = & DBUtil::getConnection();
			$rs = $base->Execute($query);

			if ($rs->RecordCount()) {
				$hidecost = $rs->fields("hidecost");
				if ($hidecost != 0) {
					array_push($list_refs_hide_prices, $articles[$i]['reference']);
					$hasrefwoprice = true;
					
				}
			}
			else{
				$hidecost = 0;				
			}

			//si on n'a pas de prix caché
			if (!$hidecost > 0) {
			
			//print $articles[$i]['reference'];
				// enregistrement de la commande, les références non trouvées vont dans $notrefs
				//print VerifRef();exit;
			//	$qte = $articles[$i]['quantity'];
			//	$ref = $articles[$i]['reference'];
				$n += VerifRef($articles[$i]['quantity'], $articles[$i]['reference']);

				if ($n == 0) { //si on a pas réussi à ajouter au panier
					array_push($list_articles_not_found, $articles[$i]);
					$refs_not_found = true;

				} else { //on a réussit à l'ajouter
					$articles[$i]["reference"] = "";
					$articles[$i]["quantity"] = "";
				}

			}
		}
	}
}

$grouping->setData('hasrefwoprice', $hasrefwoprice);
$grouping->setData('list_refs_hide_prices', $list_refs_hide_prices);

$grouping->setData('refs_not_found', $refs_not_found);
$grouping->setData('list_articles_not_found', $list_articles_not_found);
$grouping->setData('nb_refs_not_found', $refs_not_found);

$grouping->setData('articles', $articles);

$ad_mail = DBUtil::getParameterAdmin("ad_mail");
$grouping->setData('ad_mail', $ad_mail);

$ad_tel = DBUtil::getParameterAdmin("ad_tel");
$grouping->setData('ad_tel', $ad_tel);
	$grouping->output();





}else{
	
	header("location:$GLOBAL_START_URL/index.php"); 
	
}
function dobasket($IdRealProduct, $Qtt) {
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	Basket::getInstance()->addArticle( new CArticle( $IdRealProduct ), $Qtt);

}

function VerifRef($qte, $ref) {
//print "zeezez";exit;
	
	$Base = & DBUtil::getConnection();
	$Query = "
												SELECT idarticle,stock_level
												FROM product p,detail d
												WHERE d.reference = '" . trim($ref) . "' 
												AND p.available = 1 
												AND p.idproduct = d.idproduct";

	$rs = $Base->Execute($Query);
	$tcount = $rs->RecordCount();

	if ($tcount == 1) {
		$stock = $rs->Fields("stock_level");
		$IdRealProduct = $rs->Fields("idarticle");

		if (dobasket($IdRealProduct, $qte) == 0)
			return $qte;
	}

	return 0;
}
/*function array_sort($array, $on, $order=SORT_ASC)
{
    $new_array = array();
    $sortable_array = array();

    if (count($array) > 0) {
        foreach ($array as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $k2 => $v2) {
                    if ($k2 == $on) {
                        $sortable_array[$k] = $v2;
                    }
                }
            } else {
                $sortable_array[$k] = $v;
            }
        }

        switch ($order) {
            case SORT_ASC:
                asort($sortable_array);
            break;
            case SORT_DESC:
                arsort($sortable_array);
            break;
        }

        foreach ($sortable_array as $k => $v) {
            $new_array[$k] = $array[$k];
        }
    }

    return $new_array;
}*/
?>