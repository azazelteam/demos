<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf factures 
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFInvoice.php" );
 
$odf = new ODFInvoice(  $_REQUEST[ "idinvoice" ]  );
$odf->exportPDF( "Facture n° " .  $_REQUEST[ "idinvoice" ]  . ".pdf" );

?>