<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf factures proforma
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFProFormaInvoice.php" );

if( isset( $_REQUEST[ "idestimate" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../objects/Estimate.php" );
	
	$odf = new ODFProFormaInvoice( new Estimate(  $_REQUEST[ "idestimate" ]  ) );
	$odf->exportPDF( "Facture Pro forma n° " .  $_REQUEST[ "idestimate" ]  . ".pdf" );
	exit();
	
}
else if( isset( $_REQUEST[ "idorder" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../objects/Order.php" );
	
	$odf = new ODFProFormaInvoice( new Order(  $_REQUEST[ "idorder" ] ) );
	$odf->exportPDF( "Facture Pro forma n° " .  $_REQUEST[ "idorder" ]  . ".pdf" );
	exit();
	
}
else if( isset( $_REQUEST[ "idbilling_buyer" ] ) ){
	
	include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
	
	$odf = new ODFProFormaInvoice( new Invoice(  $_REQUEST[ "idbilling_buyer" ]  ) );
	$odf->exportPDF( "Facture Pro forma n° " .  $_REQUEST[ "idbilling_buyer" ]  . ".pdf" );
	exit();
	
}

?>