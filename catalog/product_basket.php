<?php
	
	class product_basket{

		protected $_itemBaskets;
		protected $_b;

	 public function __construct(){
	 	$this->_itemBaskets = array();
	 	$this->_b = array();
	 	$this->initArray();
	 }

	 public function getItemBasket(){

	 	$query = "
		SELECT *
		FROM `product_basket`
		";
		$rs =& DBUtil::query( $query );
			
				while (!$rs->EOF())
				{
					$reference = $rs->fields['reference'];
					$idArticle = $this->getArticleFromRef($reference);
					if(isset($this->_b[$idArticle])){
						$rs->MoveNext();
						continue;
					}

					$this->_itemBaskets[] = $idArticle;
					$rs->MoveNext();		
				}

		return $this->_itemBaskets;
	 }

	 protected function getArticleFromRef($reference){
	 	$it = DBUtil::query("SELECT idarticle, idproduct FROM `detail` WHERE `reference` = '".trim($reference)."'");

	 	return $it->fields['idarticle'];
	 }

	 private function initArray(){
		$b=[];
		foreach($_SESSION[ "basket_items" ] as $s){
			$b[$s['idarticle']] = 0;
		}
		$this->_b=$b;
	}
	}

?>