<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Page d'affichage des promotions en cours
 */
 
	
include( "./deprecations.php" );

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Util.php" );
class PromotionController extends PageController{};
$controller = new PromotionController( 'promotions.tpl' );

$controller->setData( "categories", false );
$controller->setData( "category", 	false );
$controller->setData( "articles", 	false );

setCategories( $controller );

if( isset($_GET['idpromo_cat']) && intval( $_GET['idpromo_cat'] ) ){

	setCategory( $controller, intval( $_GET[ "idpromo_cat" ] ) );
	setArticles($controller, intval( $_GET[ "idpromo_cat" ] ) );
	
}
else
	setArticles($controller, 0 );	



$controller->output();

//-----------------------------------------------------------------------------------

function setCategories( PageController $controller ){
	
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
	
	$rs = DBUtil::query( 
	
		"SELECT DISTINCT pc.*
		FROM promo_cat pc, promo p, detail d, product prod
		WHERE pc.idpromo_cat = p.idpromo_cat
		AND p.begin_date<=NOW() 
		AND p.end_date>=NOW()
		AND p.reference = d.reference
		AND d.available = 1
		AND d.stock_level <> 0
		AND d.idproduct = prod.idproduct
		AND prod.available = 1
		AND prod.catalog_right <= '$catalog_right'
		ORDER BY pc.display ASC"
	
	);
		
	$categories = array();

	$i = 0;
	while(!$rs->EOF()){
		
		$category = ( object )$rs->fields;
		$category->uri = "/promotions-" . URLFactory::rewrite( $rs->fields('promo_cat_name') ) . "-" . $rs->fields('idpromo_cat');
		
		array_push( $categories, $category );
		
		$rs->MoveNext();
		
	}
	
	$controller->setData('categories', $categories);
			
}

//-----------------------------------------------------------------------------------

function setCategory( PageController $controller, $idpromo_cat ){
	
	$rs = DBUtil::query( "SELECT * FROM promo_cat WHERE idpromo_cat = '$idpromo_cat' LIMIT 1" );

	$category = ( object )$rs->fields;
	$category->uri = "/promotions-" . URLFactory::rewrite( $rs->fields('promo_cat_name') ) . "-" . $rs->fields('idpromo_cat');
		
	$controller->setData('category', $category);
			
}

//-----------------------------------------------------------------------------------

function setArticles($controller, $idpromo_cat = 0 ){
	include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CArticleProxy.php" );

	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CProduct.php" );

	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;

	$query = "
	SELECT d.idarticle, p.idproduct
	FROM promo pm, detail d, product p
	WHERE pm.begin_date <= NOW() AND pm.end_date >= NOW()
	AND pm.reference = d.reference
	AND p.idproduct = d.idproduct
	AND d.hidecost = 0
	AND p.catalog_right <= '$catalog_right'
	AND p.available = 1";
	
	if( $idpromo_cat )
		$query .= " AND pm.idpromo_cat = '$idpromo_cat'";

	$query .= "
	ORDER BY pm.display ASC";
	
	$rs = DBUtil::query( $query );
	
	$articles = array();
	gc_enable ();
	while( !$rs->EOF() ){
		$idproduct = $rs->fields('idproduct');
		$idarticle = $rs->fields('idarticle');
		$article = new CArticle($idarticle);
		$product = new CProduct($idproduct);
		$newpromo = new CArticleProxy(new CArticle($rs->fields['idarticle']));
		{					

					$articles[$rs->fields('idarticle')]["getURL"] 		= $product->getURL();
					$articles[$rs->fields('idarticle')]["imageSrc"] 	= $article->getImageURI("150");
					$articles[$rs->fields('idarticle')]["getReference"] = $article->getReference();
					$articles[$rs->fields('idarticle')]["getPriceET"] 	= $article->getPriceET();
					$articles[$rs->fields('idarticle')]["name"] 		= $article->GetProductName();
					$articles[$rs->fields('idarticle')]["getReference"] = $article->getReference();
					$articles[$rs->fields('idarticle')]["getDiscountPriceATI"] = $newpromo->getDiscountPriceATI();
					$articles[$rs->fields('idarticle')]["getDiscountRate"] = Util::priceFormat($newpromo->getDiscountRate(),0);
					$articles[$rs->fields('idarticle')]["getCeilingPriceATI"] = $newpromo->getCeilingPriceATI();
					$articles[$rs->fields('idarticle')]["getDiscountPriceATI"] = $newpromo->getDiscountPriceATI();
					$articles[$rs->fields('idarticle')]["getDiscountRate"] = $newpromo->getDiscountRate();
					$articles[$rs->fields('idarticle')]["idArticle"] = $article->getId();


				unset($product);

				unset($article);
			
		}
		//array_push( $articles, new CArticleProxy( new CArticle($rs->fields("idarticle")) ) );
		$rs->MoveNext();
		
	}


	$controller->setData( "articles", $articles );
//var_dump($articles);
}

//-----------------------------------------------------------------------------------

?>