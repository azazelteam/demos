<?php 
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonction générique pour generer un HTML Select avec des valeurs : 12 mois en avant de la date courante
 */

function FStatisticsGenerateDateHTMLSelect( $onchange = false, $style = "" )
{
	echo "
	<select style=\"border: 1px solid #AAAAAA;$style\" name=\"yearly_month_select\"" . ( $onchange ? " onchange=\"$onchange\"" : "" ) . " class=\"fullWidth\">";
	
	$dat = getdate();
	$mon = $dat['mon'] -1;
	$Str_Year = $dat['year'] ;
	$year = $Str_Year;
	
	
	$months = array (
	
		1 => 'Janvier',
		2 => "Février",
		3 => 'Mars',
		4 => 'Avril',
		5 => 'Mai',
		6 => 'Juin',
		7 => 'Juillet',
		8 => "Août",
		9 => 'Septembre',
		10 => 'Octobre',
		11 => 'Novembre',
		12 => "Décembre"
		
	);
	
	for ( $i = $mon + 12 ; $i > $mon ;  $i-- )  //parse one year
	{
		
		$j = ( ( ( $i - 1 ) % 12 + 1 )  ); // the month
			
		if ($j == 12 ) {

			$j = 0; 
			
		}
		
		if( $j > $mon )
			$Str_Year = $year-1; 
			
		$j++;
		
		echo "
		<option value=\"$j\"";
		
		if( isset( $_POST[ "yearly_month_select" ] ) && $_POST[ "yearly_month_select" ] == $j )
			echo " selected=\"selected\"";
			
		echo ">".$months[ $j ] . " " . $Str_Year. "</option>\n";
			
	}
	
	echo "
	</select>";
	
}// end Function a38:FStatisticsGenerateDateHTMLSelect()

function FStatisticsGetConnectionBottom(&$con, $Arr_Connection, $Int_ArrayIndex,  $Int_IdBuyer, &$Result)
{
	if ( isset($Result) && $Result !='')
		{
			return; // leave
		}
	// build SELECT statement
	$Str_SQLQuery = ' SELECT ';
	$Str_SQLQuery.= $Arr_Connection[$Int_ArrayIndex]['FinalFieldName'];
	$Str_SQLQuery.= ' FROM ';
	$Str_SQLQuery.= $Arr_Connection[$Int_ArrayIndex]['TableName'];
	$Str_SQLQuery.= ' WHERE ';
	$Str_SQLQuery.= $Arr_Connection[$Int_ArrayIndex]['OrigFieldName'];
	$Str_SQLQuery.= " = '";
	$Str_SQLQuery.= $Arr_Connection[$Int_ArrayIndex]['OrigFieldValue'];
	$Str_SQLQuery.= "'";
	// execute query
	$rs = $con->Execute("$Str_SQLQuery");
	if ($rs->RecordCount()>0)
		{
			$Int_ArrayIndex++; // pass through
			if ($Int_ArrayIndex < sizeof ($Arr_Connection) )
				{
					$Arr_Connection [$Int_ArrayIndex]['OrigFieldValue'] = $rs->Fields( $Arr_Connection [$Int_ArrayIndex - 1]['FinalFieldName'] );
					FStatisticsGetConnectionBottom ($con, $Arr_Connection, $Int_ArrayIndex,  $Int_IdBuyer, $Result );
				}
					else
				{
// build name, if necessary
					if ( strstr ( $Arr_Connection [$Int_ArrayIndex -1]['FinalFieldName'], ",") )
						{
							$Arr_Temp = split("," , $Arr_Connection [$Int_ArrayIndex -1]['FinalFieldName']);
							while ( list ($k, $v) = each ($Arr_Temp) )
								{
									$v = ltrim($v); // chop spaces
									$Result .= ' ' . $rs->Fields($v); //quit recursion, return final value
								}
							$Result = substr ($Result ,1);
							$Int_IdBuyer = $Arr_Connection[$Int_ArrayIndex - 1 ]['OrigFieldValue'];
						}
							else
						{
							$Result = $rs->Fields( $Arr_Connection [$Int_ArrayIndex - 1]['FinalFieldName'] ); //quit recursion, return final value
							$Int_IdBuyer = $Arr_Connection[$Int_ArrayIndex - 1 ]['OrigFieldValue'];
						}
				}
		}
}

function FProcessString($str)
{
	return "'". str_replace(",","','",$str) ."'";
}
?>