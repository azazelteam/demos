<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage facture
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class InvoiceController extends PageController {}
$controller = new InvoiceController( "/account/show_invoice.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
		$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
		$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
} else {
	$admin_order->setData('isLogin', "");
}

if( !isset($_REQUEST['IdInvoice']) ){
	exit( "Impossible de récupérer le numéro de facture" );	
}

/* On récupère la facture */
$invoice = new Invoice( intval($_REQUEST['IdInvoice']) );

/* On passe les infos facture */
$controller->setData('idinvoice', intval($_REQUEST['IdInvoice']) );
$invoice_date = humanReadableDatetime($invoice->get('DateHeure'));
$controller->setData('invoice_date',$invoice_date);

/* hasLot? */
$hasLot = false;
$count = $invoice->getItemCount();
$i = 0;
while( $i < $count && !$hasLot ){
	if( $invoice->getItemAt( $i)->get( "lot" ) > 1 ) { $hasLot = true; }
	$i++;
}
if($hasLot){ $controller->setData('hasLot','ok'); } else { $controller->setData('hasLot',''); }

/* On parcours les articles */
$nbReferences = $invoice->getItemCount();
$htbase = 0;
for( $i=0 ; $i<$nbReferences ; $i++ ) {
	
	$invoice_articles[$i]["Icon"] = URLFactory::getReferenceImageURI( $invoice->getItems()->get( $i )->get( "idarticle" ) );;
	$invoice_articles[$i]["reference"] = $invoice->getItemAt($i)->get('reference');
	$invoice_articles[$i]["summary"] = $invoice->getItemAt($i)->get('summary');
	$invoice_articles[$i]["quantity"] = $invoice->getItemAt($i)->get('quantity');	
	$invoice_articles[$i]["discount_price"] = Util::priceFormat( $invoice->getItemAt($i)->get( "discount_price" ) );
	$invoice_articles[$i]["lot"] = $invoice->getItemAt($i)->get( "lot" );
	$invoice_articles[$i]["unit"] = $invoice->getItemAt($i)->get( "unit" );
	$invoice_articles[$i]["total_price"] = Util::priceFormat($invoice->getItemAt($i)->getTotalET());
	$invoice_articles[$i]["unit_price"] = Util::priceFormat($invoice->getItemAt($i)->get( "unit_price" ));
	$invoice_articles[$i]["ref_discount"] = ($invoice->getItemAt($i)->get('ref_discount') > 0) ? Util::rateFormat($invoice->getItemAt($i)->get('ref_discount')) : "-" ;
	$invoice_articles[$i]["discount_price"] = Util::priceFormat($invoice->getItemAt($i)->get( "discount_price" ));
	$invoice_articles[$i]["RefTotalAmountHT"] = ($invoice->getItemAt($i)->get('discount_price')) ? Util::priceFormat($invoice->getItemAt($i)->get('discount_price') * $invoice->getItemAt($i)->get('quantity')) : "" ;	
	
	$htbase = $htbase + ($invoice->getItemAt($i)->get( "discount_price" ) * $invoice->getItemAt($i)->get('quantity') );	
}

/* On passe les articles au tpl */
$controller->setData('invoice_articles',$invoice_articles);

/* On affiche le total */
$articlesET = 0.0;
$it = $invoice->getItems()->iterator();

while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();

if( $articlesET == 0.0 ){ //ne pas afficher les frais de port si prix cachés
	$controller->setData( "total_charge_ht", "-" );
	$controller->setData( "total_charge_ttc", "-" );
}
else{
	$chargesET 	= $invoice->getChargesET();
	$chargesATI = $invoice->getChargesATI();
	$controller->setData( "total_charge_ht", $chargesET > 0.0 ? Util::priceFormat( $chargesET ) : "FRANCO" ); 
	$controller->setData( "total_charge_ttc", $chargesATI > 0.0 ? Util::priceFormat( $chargesATI ) : "FRANCO" ); 
	$controller->setData( "franco", $chargesET == 0.0 );
}

$basetva = $invoice->getTotalET();
$ttcbase = $invoice->getTotalATI();
$totvat = $ttcbase-$basetva;

$controller->setData( "remise_rate", 					Util::rateFormat( $invoice->get( "total_discount" ) ) );
$controller->setData( "total_discount", 				Util::rateFormat( $invoice->get( "total_discount" ) ) );
$controller->setData( "discount", 						Util::priceFormat( $invoice->get( "total_discount_amount" ) ) );
$controller->setData( "total_discount_amount",			Util::priceFormat( $invoice->get( "total_discount_amount" ) ) );
$controller->setData( "total_amount_ht", 				Util::priceFormat( $articlesET - $invoice->get( "total_discount_amount" ) ) );
$controller->setData( "total_amount_ht_wb", 			Util::priceFormat( $invoice->getTotalET() ) );
$controller->setData( "total_amount_ttc_wbi", 			Util::priceFormat( $invoice->getNetBill() ) );
$controller->setData( "total_amount_ht_avant_remise", 	Util::priceFormat( $articlesET ) );
$controller->setData( "remise", 						$invoice->get( "total_discount" ) > 0.0 );
$controller->setData( "getVATRate", 					Util::rateFormat( $invoice->getVATRate() ) );
$controller->setData( "getVATAmount", 					Util::priceFormat($totvat));


/* On passe les adresses de facturation & livraison */
$controller->setData('buyer_company', Util::checkEncoding($invoice->getInvoiceAddress()->getCompany()) );
$controller->setData('buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $invoice->getInvoiceAddress()->getGender() ) );
$controller->setData('buyer_firstname', Util::checkEncoding($invoice->getInvoiceAddress()->getFirstName() ));
$controller->setData('buyer_lastname', Util::checkEncoding($invoice->getInvoiceAddress()->getLastName()) );
$controller->setData('buyer_adress', Util::checkEncoding(($invoice->getInvoiceAddress()->getAddress2()) ? ($invoice->getInvoiceAddress()->getAddress()."<br/>".$invoice->getInvoiceAddress()->getAddress2()) : $invoice->getInvoiceAddress()->getAddress()) );
$controller->setData('buyer_zipcode', $invoice->getInvoiceAddress()->getZipcode());
$controller->setData('buyer_city', Util::checkEncoding($invoice->getInvoiceAddress()->getCity()) );

$controller->setData('d_buyer_company', Util::checkEncoding($invoice->getForwardingAddress()->getCompany()) );
$controller->setData('d_buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $invoice->getForwardingAddress()->getGender() ) );
$controller->setData('d_buyer_firstname', Util::checkEncoding($invoice->getForwardingAddress()->getFirstName()) );
$controller->setData('d_buyer_lastname', Util::checkEncoding($invoice->getForwardingAddress()->getLastName()) );
$controller->setData('d_buyer_adress', Util::checkEncoding(($invoice->getForwardingAddress()->getAddress2()) ? ($invoice->getForwardingAddress()->getAddress()."<br/>".$invoice->getForwardingAddress()->getAddress2()) : $invoice->getForwardingAddress()->getAddress()) );
$controller->setData('d_buyer_zipcode', $invoice->getForwardingAddress()->getZipcode());
$controller->setData('d_buyer_city', Util::checkEncoding($invoice->getForwardingAddress()->getCity()) );

/* On passe les conditions de réglements */
$controller->setData('PaymentDelay',Util::checkEncoding(DBUtil::getDBValue( "payment_delay_1", "payment_delay", "idpayment_delay", $invoice->get( "idpayment_delay" ) )));
$controller->setData('PayMod',Util::checkEncoding(DBUtil::getDBValue("name_1", "payment", "idpayment", $invoice->get( "idpayment" ) )));

/* On ferme l'envoi de données */
$controller->output();

?>