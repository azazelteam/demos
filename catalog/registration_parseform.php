<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Enregistrement des informations clients
 */




include_once( dirname( __FILE__ ) . '/../config/init.php' ); 
include_once( dirname( __FILE__ ) . '/../objects/Customer.php' );
include_once( dirname( __FILE__ ) . '/../objects/Dictionnary.php' );
include_once( dirname( __FILE__ ) . '/../script/global.fct.php' );
include_once( dirname( __FILE__ ) . '/../objects/Util.php' );
include_once( dirname( __FILE__ ) . '/../objects/User.php' );

//validation des données
/*
require_once( 'PEAR.php' );
include_once( 'Validate.php' ); //classe PEAR
include_once( 'Validate/FR.php' ); //classe PEAR
*/
User::fakeAuthentification();

/* protection XSS */
$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );
$_POST = Util::arrayMapRecursive( "Util::doNothing", $_POST );
// Champs sans espaces
$_POST[ 'phonenumber' ] = isset( $_POST[ 'phonenumber' ] ) ? str_replace( ' ' , '' , $_POST[ 'phonenumber' ] ) : '';
$_POST[ 'faxnumber' ] 	= isset( $_POST[ 'faxnumber' ] ) ? str_replace( ' ' , '' , $_POST[ 'faxnumber' ] ) : '';
$_POST[ 'gsm' ] 		= isset( $_POST[ 'gsm' ] ) ? str_replace( ' ' , '' , $_POST[ 'gsm' ] ) : '';
$_POST[ 'birthday' ] 		= isset( $_POST[ 'birthday' ] ) ? str_replace( ' ' , '' , $_POST[ 'birthday' ] ) : '';
$_POST[ 'optin' ] 		= isset( $_POST[ 'optin' ] ) ? str_replace( ' ' , '' , $_POST[ 'optin' ] ) : '';
$_POST[ 'zipcode' ] 	= str_replace( ' ' , '' , $_POST[ 'zipcode' ] );
$_POST[ 'siret' ] 		= isset( $_POST[ 'siret' ] ) ? str_replace( ' ' , '' , $_POST[ 'siret' ] ) : '';

// Initialisation des valeurs du formulaire à null au 1er appel de la page (formulaire non envoyé)
$formFields = array( 	'company',
						'idactivity',
						'adress',
						'adress_2',
						'zipcode',
						'city',
						'idstate',
						'idcurrency',
						'tva',
						'siret',
						'naf',
						'title',
						'logo',
						'firstname',
						'lastname',
						'email',
						'iddepartment',
						'idfunction',
						'phonenumber',
						'gsm',
						'birthday',
						'optin',
						'faxnumber',
						'newsletter',
						'mailtype',
						'idmanpower',
						'phone_standard',
						'idsource',
						'buyer_info1',
						'buyer_info2',
						'buyer_info3',
						'buyer_info4',
						'buyer_info5',
						'buyer_info6',
						'buyer_info7',
						'buyer_info8',
						'buyer_info9',
						'buyer_info10',
						'code_etabl',
						'n_compte',
						'pwd',
						'pwd_confirm',
						'catalog_right',
						'idcustomer_profile',
						'profile'
					);
					
foreach( $formFields as $fieldname) {
	$UserInfos[ $fieldname ] = isset( $_POST[ $fieldname ] ) ? $_POST[ $fieldname ] : '';
}

//vérification des données
$error_msg = '';
$mode = DBUtil::getParameterAdmin( 'b2c' );

if( !$mode ) {
	
	$lastnameIsRequired = DBUtil::getParameterAdmin( 'b_lastname_required' );
	$companyIsRequired 	= DBUtil::getParameterAdmin( 'b_company_required' );
	$adressIsRequired 	= DBUtil::getParameterAdmin( 'b_address_required' );
	$siretIsRequired 	= DBUtil::getParameterAdmin( 'b_siret_required' );
	$nafIsRequired 		= DBUtil::getParameterAdmin( 'b_naf_required' );
  	$zipcodeIsRequired 	= DBUtil::getParameterAdmin( 'b_zipcode_required' );
	
	$error_msg .= empty( $_POST[ 'lastname' ] ) && $lastnameIsRequired ? Dictionnary::translate( 'register_lastname_required' ) . '<br />' : '';
	$error_msg .= empty( $_POST[ 'company' ] ) && $companyIsRequired ? Dictionnary::translate( 'register_company_required' ) . '<br />' : '';
	$error_msg .= empty( $_POST[ 'adress' ] ) && $adressIsRequired ? Dictionnary::translate( 'register_address_required' ) . '<br />' : '';
		$error_msg .= (empty( $_POST[ 'siret' ] )  )  && $siretIsRequired ? Dictionnary::translate( 'pear_validator_siret_error' ) . '<br />' : '';

//	$error_msg .= (empty( $_POST[ 'siret' ] ) || !Validate_FR::SIRET( $_POST[ 'siret' ] ) )  && $siretIsRequired ? Dictionnary::translate( 'pear_validator_siret_error' ) . '<br />' : '';
	$error_msg .= empty( $_POST[ 'zipcode' ] ) && $zipcodeIsRequired ? "Merci de bien vouloir renseigner le champ 'code postal'" . '<br />' : '';
	$error_msg .= $nafIsRequired && ( empty( $_POST[ 'naf' ] ) || !DBUtil::query( 'SELECT idnaf FROM naf WHERE idnaf = ' . DBUtil::quote( stripslashes( $_POST[ 'naf' ] ) ) . ' LIMIT 1' )->RecordCount() ) ? "Merci de bien vouloir renseigner un code 'NAF' valide" . '<br />' : '';
	$error_msg .= empty( $_POST[ 'lastname' ] ) ? Dictionnary::translate( 'register_lastname_required' ) . '<br />' : '';
	$rslogins = DBUtil::query("SELECT * FROM user_account WHERE login LIKE '".$_POST[ 'email' ]."'");
	
	$error_msg .= $rslogins->RecordCount() > 0 ? "Le mail que vous avez saisi est d&eacute;ja utilis&eacute;!! Veuillez le changer." : "";
}

$validatePostData = empty( $error_msg );

if( $validatePostData ){

	$UserInfos[ 'login' ]	= isset( $_POST['login'] ) ? $_POST['login'] : $_POST[ 'email' ];
	$UserInfos[ 'password' ] = isset( $_POST[ 'pwd' ] ) ? $_POST[ 'pwd' ] : ( isset( $_POST[ 'password' ] ) ? $_POST[ 'password' ] : Util::getRandomPassword() );
	
	//Formulaire d'enregistrement rapide
	if( !isset( $_POST[ 'idstate' ] ) )
    	$UserInfos[ 'idstate' ] = DBUtil::getParameter("default_idstate");
    
    $UserInfos[ 'idcurrency' ] = DBUtil::getDBValue( "idcurrency", 'statecurrency' , "idstate", $UserInfos['idstate'] );

	// on définie les valeurs à insérer
	$lastupdate = date( 'Y-m-d H:i:s' );
	
	$UserInfos[ 'idcustomer_profile' ] 	= isset( $UserInfos[ 'idcustomer_profile' ] ) && $UserInfos[ 'idcustomer_profile' ]	!= "" ? $UserInfos[ 'idcustomer_profile' ] : 0;
	
	$delivpayment = 1;
	if( !$mode )
		$delivpayment = 16;
					
	$Fields = array(
		'iduser'				=> DBUtil::getParameter( 'contact_commercial' ),
		'company'				=> strtoupper( no_accent( $UserInfos[ 'company' ] ) ),
		'adress'				=> $UserInfos[ 'adress' ],
		'adress_2'				=> $UserInfos[ 'adress_2' ],
		'zipcode'				=> $UserInfos[ 'zipcode' ],
		'city'					=> strtoupper( $UserInfos[ 'city' ] ),
		'idstate'				=> $UserInfos[ 'idstate' ],
		'idcurrency'			=> $UserInfos[ 'idcurrency' ],
		'siret'					=> $UserInfos[ 'siret' ],
		'naf'					=> $UserInfos[ 'naf' ],
		'idsource'				=> 1,
		'newsletter'			=> 0,
		'username'				=> $UserInfos[ 'login' ],
		'buyer_info1'			=> $UserInfos[ 'buyer_info1' ],
		'buyer_info2'			=> $UserInfos[ 'buyer_info2' ],
		'buyer_info3'			=> $UserInfos[ 'buyer_info3' ],
		'buyer_info4'			=> $UserInfos[ 'buyer_info4' ],
		'buyer_info5'			=> $UserInfos[ 'buyer_info5' ],
		'buyer_info6'			=> $UserInfos[ 'buyer_info6' ],
		'buyer_info7'			=> $UserInfos[ 'buyer_info7' ],
		'buyer_info8'			=> $UserInfos[ 'buyer_info8' ],
		'buyer_info9'			=> $UserInfos[ 'buyer_info9' ],
		'buyer_info10'			=> $UserInfos[ 'buyer_info10' ],
		'code_etabl'			=> $UserInfos[ 'code_etabl' ],
		'n_compte'				=> $UserInfos[ 'n_compte' ],
		'contact'				=> 0,
		'lastupdate'			=> $lastupdate,
		'create_date'			=> date( 'Y-m-d H:i:s' ),
		'logo'					=> $UserInfos['logo'],
		'deliv_payment'			=> $delivpayment,
		'idcustomer_profile' 	=> $UserInfos[ 'idcustomer_profile' ],
		'catalog_right' 		=> isset( $_POST[ 'catalog_right' ] ) ? intval( $_POST[ 'catalog_right' ] ) : 0,
		'profile'				=> $UserInfos[ 'profile' ]
	);
						
	if( isset( $UserInfos['newsletter'] ) )
		$Fields[ 'newsletter' ] = $UserInfos[ 'newsletter' ];
			
	$customer = Customer::create();
	
	foreach( $Fields as $fieldName => $fieldValue ){
		
		$customer->set( $fieldName , $fieldValue );
		
	}
			
	$idbuyer = $customer->getId();

	$UserInfos[ 'idbuyer' ] = $idbuyer;
	
	$FieldsContact = array(
		'title'			=> $UserInfos[ 'title' ],
		'firstname'		=> ucfirst(strtolower( $UserInfos[ 'firstname' ] ) ),
		'lastname'		=> ucfirst(strtolower( $UserInfos[ 'lastname' ] ) ),
		'mail'			=> $UserInfos[ 'email' ],
		'iddepartment'	=> $UserInfos[ 'iddepartment' ],
		'idfunction'	=> $UserInfos[ 'idfunction' ],
		'phonenumber'	=> $UserInfos[ 'phonenumber' ],
		'gsm'			=> $UserInfos[ 'gsm' ],
		'optin' 		=> $UserInfos[ 'optin' ] == "on"? 1:0,
		'birthday' 		=> getDate2($UserInfos[ 'birthday' ]),
		'faxnumber'		=> $UserInfos[ 'faxnumber' ],
		'username'		=> $UserInfos[ 'login' ],
		'lastupdate'	=> $lastupdate
	);

	foreach( $FieldsContact as $contactField => $contactValue ){
		
		$customer->getContact()->set( $contactField , $contactValue );
		
	}


	
	$customer->save();
	
	$customer->createAccount( $UserInfos[ 'password' ] );
		
	/**
	 * Envoi d'un e-mail de confirmation de l'enregistrement
	 */
	$ret = Session::getInstance()->login( $UserInfos[ 'login' ] , md5( $UserInfos[ 'password' ] ) );

	if( $ret )
		sendRegistrationMail( Session::getInstance()->getCustomer()->getId(), $UserInfos[ 'email' ], $UserInfos[ 'password' ] );
	
		
}

//--------------------------------------------------------------------------------------------

function sendRegistrationMail( $idbuyer, $email, $password ){

	global $GLOBAL_START_PATH , $GLOBAL_START_URL;
		
	$iduser =	DBUtil::getParameter( "contact_commercial" );
	
	//-----------------------------------------------------------------------------------------

	$query = "
	SELECT login 
	FROM user_account 
	WHERE idbuyer = '$idbuyer' 
	AND idcontact = '0' 
	LIMIT 1";
	
	$rs = &DBUtil::query( $query );
	
	if( $rs === false || !$rs->RecordCount() )
		trigger_error( "Impossible de récupérer le nom d'utilisateur", E_USER_ERROR );
		
	$login = $rs->fields( "login" );
	
	//-----------------------------------------------------------------------------------------
				
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( "BUYER_REGISTRATION" );

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseBuyerTags( $idbuyer );
	$editor->setUseBuyerPassword( $password );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = html_entity_decode($editor->unTagSubject());
	  
	//-----------------------------------------------------------------------------------------
	
	//envoyer le mail
	
	include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );

	$mailer = new EVM_Mailer();

	$mailer->set_subject( $Subject );
	$mailer->set_sender( DBUtil::getParameterAdmin( "ad_sitemane" ), DBUtil::getParameterAdmin( "ad_mail" ) );
	$mailer->add_recipient( $login, $email );
	
	$user = DBUtil::stdclassFromDB( "user", "iduser", DBUtil::getParameter( "contact_commercial" ) );
	
	if( isset( $_POST[ "catalog_right" ] ) && intval( $_POST[ "catalog_right" ] ) == 6 )
		$mailer->add_recipient( $user->firstname . " " . $user->lastname, $user->email );
	
	$mailer->set_message( $Message );
	$mailer->set_message_type( "html" );
	  
	$ret = $mailer->send();

  	if( !$ret )
  		die( "Impossible d'envoyer l'email" );

}

function getDate2($value){
	if( preg_match( "/[0-9]{2}\/[0-9]{2}\/[0-9]{4}/", $value ) ){
			   		
			   			list( $day, $month, $year ) = explode( "/", $value );
			   			return "$year-$month-$day";
			   			
					}
	return "";
}

?>