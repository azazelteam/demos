<?php

include_once("../config/init.php" );
include_once("../objects/classes.php" );
include_once("$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");
include_once("$GLOBAL_START_PATH/objects/User.php");

//Création d'un compte
if(isset($_REQUEST["createLoginAjax"])) {

	//validation des données
	require_once( 'PEAR.php' );
	include_once( 'Validate.php' ); //classe PEAR
	include_once( 'Validate/FR.php' ); //classe PEAR	
	
	//$customer = new Customer();
	$createMail = $_POST["email"];
	$createPassword = $_POST["password"];
	$createConfPassword = $_POST["pwd_conf"];
	
	//Test de l'adresse mail
	$db =& DBUtil::getConnection();
	$SQL_Query = "SELECT COUNT(*) AS 'exists' FROM contact WHERE mail LIKE '" . $createMail . "'";
	$rs = $db->Execute($SQL_Query);
	$SQL_Query2 = "SELECT COUNT(*) AS 'exists_account' FROM user_account WHERE login LIKE '" . $createMail . "'";
	$rs2 = $db->Execute($SQL_Query2);
	
	//On teste si l'adresse mail est valide
	if( empty( $createMail ) || !Validate::email( $createMail )) {
		echo "1";
		exit();			
	}
	
	//On teste si l'adresse mail existe deja?
	else if ( $rs->Fields( "exists" ) != 0 || $rs2->Fields( "exists_account" ) != 0 ){
		echo "2";
		exit();	
	}
	
	//On teste si les mots de passe sont valides
	else if( empty( $createPassword ) || empty( $createConfPassword ) || !Validate::string( $createPassword, $options = array( "format" => VALIDATE_ALPHA .  VALIDATE_NUM, "min_length" => 6 ) ) ){ 
		echo "3";
		exit();	
	}
		
	//On teste si les mots de passe sont différents
	else if( $createPassword !== $createConfPassword ){
		echo "4";
		exit();	
	}
	
	//Sinon on valide le formulaire
	else {
		exit();	
	}
	

}
if( isset($_POST['redirectTo']) && $_POST['redirectTo'] == 'order.php')
	header( "Location:$GLOBAL_START_URL/catalog/" . $_POST['redirectTo']);

if( !Session::getInstance()->getCustomer() ){
	$_SESSION['FromAccountPage'] = true;
	
	if(isset($_POST["Login"]))
		$_SESSION['issetPostLoginAndNotLogged'] = true;
	
	header('Location: register.php?loginfail');
	exit();
}

//Affichage du template
class AccountController extends PageController{};

$account_page = new AccountController( "account.tpl" );
if( !Session::getInstance()->getCustomer() ) {
	if(isset($_POST["Login"])) {
		$account_page->setData('issetPostLoginAndNotLogged', "ok");
	}
} else {
	
	$account_page->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	
	$account_page->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$account_page->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	
	if ($GroupingCatalog->getGrouping( "grouping" )){
	$account_page->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));}
	
	$account_page->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();
	

	$account_page->setData( "contact_lastname", $salesman->get("lastname") );
	$account_page->setData( "contact_firstname", $salesman->get("firstname") );
	$account_page->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$account_page->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$account_page->setData( "contact_email", $salesman->get("email") );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	$contact_grouping = $rs->fields("contact_grouping");
	$account_page->setData( "contact_grouping", $contact_grouping );
	
}

$account_page->output();

?>
