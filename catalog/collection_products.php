<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */
 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CollectionProxy.php" );

$page = 0;
if( isset( $_REQUEST[ 'page' ] ) && $_REQUEST[ 'page' ] > 0 )
	$page = $_REQUEST[ 'page' ];

if( isset( $_GET[ 'IdCollection' ] ) && $_GET[ 'IdCollection' ] > 0 ){
	$idcollection = $_GET[ 'IdCollection' ];
}else{
	header( "Location: " . HTTP_HOST );
	exit();	
}

$collection = new CollectionProxy( $idcollection );

if( $collection === false ){
	header( "Location: " . HTTP_HOST );
	exit();
}

$brand		= $collection->getBrand();

if( $brand === false ){
	header( "Location: " . HTTP_HOST );
	exit();
}

$charte_collection = new PageController( $collection->getTemplate() );

$charte_collection->head->setTitle( $collection->getMetaTitle() );
$charte_collection->head->setMetaTag( "description" , $collection->getMetaDescription() );
$charte_collection->head->setMetaTag( "keywords" , $collection->getMetaKeywords() );

$charte_collection->setData( 'brand' , $brand );
$charte_collection->setData( 'collection' , $collection );
/*
$pagenum = 1;

$limit = "";
if($nbProds > 0){
	if($page > 0){
			
		$limitMax = $nbProds;
		$limitMin = ($limitMax * $page) - $nbProds;
		
		$pagenum = $page;
		
	}else{
		
		$limitMax = $nbProds;
		$limitMin = 0;
		
	}
	
	$limit = "LIMIT $limitMin,$limitMax";
}
*/


$charte_collection -> output();

function getParentCollections( $idbrand, $idcollection ){
	global $GLOBAL_START_URL;
	
	$collections = DBUtil::query("SELECT * FROM collection WHERE idbrand = $idbrand AND idcollection <> $idcollection");
	
	$collectArray = array();
	
	if( $collections->RecordCount() > 0 ){
	
		$i = 0;
		while( !$collections->EOF ){
			$collectArray[$i]["idcollection"] = $collections->fields( "idcollection" );           
			$collectArray[$i]["name"] = $collections->fields( "name" );
			$collectArray[$i]["image"] = $collections->fields( "image" );
			$collectArray[$i]["description"] = $collections->fields( "description" );
			$collectArray[$i]["meta_title"] = $collections->fields( "meta_title" );
			$collectArray[$i]["meta_description"] = $collections->fields( "meta_description" );
			$collectArray[$i]["url"] = $GLOBAL_START_URL.'/'.$collections->fields( "collection_url" );
			
			if( isset( $_POST['idcollection'] ) && $_POST['idcollection'] > 0 && $_POST['idcollection'] == $collections->fields( "idcollection" ) ){
				$collectArray[$i]['selected'] = 1;
			}else{
				$collectArray[$i]['selected'] = 0;			
			}

			$collections->MoveNext();
			$i++;
		}
	}
	
	return $collectArray;
}

?>