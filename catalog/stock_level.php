<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Répartition des produits / Catégories pour un groupement et client
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");

header( "Content-Type: text/html; charset=utf-8" );
class GroupingController extends PageController {}
$controller = new GroupingController( "/account/stock_level.tpl" );

$GroupingCatalog = new GroupingCatalog();
global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );

include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));  
	$stock =  "SELECT detail.reference AS id, detail.reference AS reference, detail.summary_1 AS nom, category.name_1 AS nom_categorie, detail.stock_level AS stock_level, detail.minstock AS seuil_min
					FROM detail
					JOIN product ON detail.idproduct = product.idproduct
					JOIN category ON category.idcategory= product.idcategory
					WHERE detail.idsupplier = 99
					";

	$result = & DBUtil::query( $stock ); 
		while( !$result->EOF )
			{
				$ref = ($result->fields("reference"));
				//$popup = RecordCount($pop);
				//$rsMvt = DButil::query->buidQueryString($result->fields("reference"));
				
				$html_popup = genereStock($ref);
				//print_r($html_popup);
				$ref_id = str_replace(' ', '', $ref);
				$arraydata = array( $result->fields("reference"),
				 					$result->fields("nom"),
				 					$result->fields("nom_categorie"),
				 					$result->fields("stock_level"),
				 					$result->fields("seuil_min"),
				 					$ref_id,
				 					$html_popup
									);
									
				 $export[] = $arraydata;
				 //print_r(genereStock($ref));
				//genereStock($ref);
				
				$result->MoveNext();
			}	
		//print_r($export);exit;
}

	$controller->setData('export', $export);
	global $GLOBAL_START_URL;
	
	$form = '';
	$form .= '<form  style="float: none!important;"action="/catalog/stock_level.php" method="post" id="SearchPop">
					<div  style="float: right;width:65%;">
						<div class="tableContainer">
							<table class="dataTable" width="100%">
								<tr>
									<td style="vertical-align: middle;"><span style="font-size: 12px;">Entre le</span></td>
									<td> <input type="text" name="date_order" id="start_date" class="calendarInput" value="" />
									</td>
									<td style="vertical-align: middle;"><span style="font-size: 12px;">Et le</span></td>
									<td> <input type="text" name="date_ordersup" id="end_date" class="calendarInput" value="" /></td>
							</table>
						</div>
						<div class="submitButtonContainer" style="">
							<input type="submit" class="blueButton" value="Rechercher" style="color:black;" />
						</div>
					</div>
				</form>';
				
	$controller->setData( "form",$form );

/* On ferme l'envoi de données */
$controller->output();

 function genereStock($ref){
	 
	if (isset ($_POST["date_order"])){ $start = $_POST["date_order"]; }
		
	//$start = explode("-", $start);
	//$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
	if(isset($_POST["date_ordersup"])){ $end = $_POST["date_ordersup"]; }
		
	//$end = explode("-", $end);
	//$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
	
	
				 $SQL_Query_input = "
					SELECT
							'input' as flag,
							BLR.quantity,
							BLR.delivered_quantity,
							OS.idorder_supplier,
							OS.idorder,
							BU.company,
							CO.lastname,
							CO.firstname,
							BL.date_delivery,
							BL.idbl_delivery
						FROM
						   bl_delivery_row BLR
						INNER JOIN bl_delivery BL ON
						   BL.idbl_delivery = BLR.idbl_delivery
						INNER JOIN order_supplier OS ON
						   OS.idorder_supplier= BL.idorder_supplier
						INNER JOIN `order` Ord ON
						   Ord.idorder= OS.idorder
						LEFT JOIN buyer BU ON
						   BU.idbuyer = BL.idbuyer
						LEFT JOIN contact CO ON
						   CO.idbuyer = BL.idbuyer
						WHERE
							BLR.reference = '$ref'
							AND BL.idsupplier = 99";
							
				if(isset($_POST['date_order']) && isset($_POST['date_ordersup'])){
					$SQL_Query_input .= " AND (OS.DateHeure BETWEEN '$start' and '$end')";		
					$SQL_Query_input .= " AND (Ord.DateHeure BETWEEN '$start' and '$end')";	
				}
				
				$SQL_Query_input .= " ORDER BY
							BL.date_delivery DESC
							LIMIT 30";
						
				$SQL_Query_output = "
					SELECT
						'output' as flag,
						BLR.quantity,
						BLR.delivered_quantity,
						BL.idorder_supplier,
						BL.idorder,
						BU.company,
						CO.lastname,
						CO.firstname,
						BL.date_delivery,
						BL.idbl_delivery
					FROM
					   bl_delivery_row BLR
					INNER JOIN bl_delivery BL ON
					   BL.idbl_delivery = BLR.idbl_delivery
					INNER JOIN order_supplier OS ON
					   OS.idorder_supplier= BL.idorder_supplier
					INNER JOIN `order` Ord ON
						   Ord.idorder= OS.idorder
					LEFT JOIN buyer BU ON
					   BU.idbuyer = BL.idbuyer
					LEFT JOIN contact CO ON
					   CO.idbuyer = BL.idbuyer
					WHERE
						BLR.reference = '$ref'
						AND OS.idorder = 0";
						
					if(isset($_POST['date_order']) && isset($_POST['date_ordersup'])){
						$SQL_Query_output .= " AND (OS.DateHeure BETWEEN '$start' and '$end')";		
						$SQL_Query_output .= " AND (Ord.DateHeure BETWEEN '$start' and '$end')";	
					}
					
					$SQL_Query_output .= " ORDER BY
						BL.`date_delivery` DESC
						LIMIT 30";
						
				$SQL_Query = '('. $SQL_Query_input .')  
					 UNION  
				   (' . $SQL_Query_output .')';
						
					
				$res = & DBUtil::query( $SQL_Query );
				$ref_id = str_replace(' ', '', $ref);
					$html = $htmls = '';
					$html .= "<span onmouseover=\"document.getElementById('pop$ref_id').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$ref_id').className='tooltip';\" style=\"position:relative; display:block;\">$ref<img src='/images/back_office/content/icons-warning.png '/>";
				
					$html .= "<span id='pop$ref_id' class='tooltip'>
							<div class=\"content\" style=\"padding:5px;\">
							<div class=\"subContent\">";
							
					$html .= "<div style=\"font-size: 12px;\">";
					$html.= "<table>
							<tr>
								<th><span style=\"font-size: 12px;\">Qté axpédiée</th>
								<th><span style=\"font-size: 12px;\">N° BL expédiée</th>
								<th><span style=\"font-size: 12px;\">Qté réceptionnée</th>
								<th><span style=\"font-size: 12px;\">N° BL réceptionné</th>
								<th><span style=\"font-size: 12px;\">Date</th>
								<th><span style=\"font-size: 12px;\">N° cde fournis.</th>	
								<th><span style=\"font-size: 12px;\">N° cde client</th>
								<th><span style=\"font-size: 12px;\">Nom</th>

							</tr>";
							
				$ii = 0;
				
				$array_stock = array();
				while (!$res->EOF()) {
					switch ($res->fields("flag")) {
						case 'output':
							$input = 0;
							$input_bl = 0;
							$output = $res->fields("quantity");
							$output_bl = $res->fields("idbl_delivery");
							break;
						case 'input':
							$input = $res->fields("quantity");
							$input_bl = $res->fields("idbl_delivery");
							$output = 0;
							$output_bl = 0;
							break;
					}
							
						$quantity_value = (($res->fields("flag") == 'output' ) && (intval($res->fields("delivered_quantity")) > 0) ) ? '0' : $res->fields("quantity");
						$html .= "<tr><td>". $input . "</td>" ;
						$html .= "<td>". $input_bl . "</td>" ;
						$html .= "<td>". $output . "</td>" ;
						$html .= "<td>". $output_bl . "</td>" ;
						$html .= "<td>". $res->fields("date_delivery") . "</td>" ;
						$html .= "<td>". $res->fields("idorder_supplier") . "</td>" ;
						$html .= "<td>". $res->fields("idorder") . "</td>" ;
						$html .= "<td>". $res->fields("lastname")." ". $res->fields("firstname")."<br />". $res->fields("company") . "</td>";
						$html .= "</tr>" ;
						
						//$array_stock[] = array($input,$input_bl,$output,$output_bl,$res->fields("date_delivery"),$res->fields("idorder_supplier"),$res->fields("idorder"),"". $res->fields("lastname")." ". $res->fields("firstname")."<br />". $res->fields("company") . ""		
						//);
						
						$res->MoveNext();	
						$ii ++;
					
				}
				
				$html .= '</table>';
				$html .= '</div>';
				$html .= '</div>';
				$html .= '</div>';
				$html .= '</span>';
				$html .= '</span>';
				$htmls .= $html;
				if($ii>0)
					return $htmls;
				else
					return $ref;
 }

?>

<script type="text/javascript">

/* <![CDATA[ */
	
		$(document).ready(function() { 
			$(function() {
				
				$('#start_date').datepicker({
					  showOn: "button",
					  buttonImage: "<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif",
					  buttonImageOnly: true,
					  buttonText: "Select date",
					changeMonth: true,
					changeYear: true,
					dateFormat: 'yy-mm-dd'
				});
			});	
			
			$(function() {
				
				$('#end_date').datepicker({
					  showOn: "button",
					  buttonImage: "<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif",
					  buttonImageOnly: true,
					  buttonText: "Select date",
					changeMonth: true,
					changeYear: true,
					dateFormat: 'yy-mm-dd'
				});
			});	
		});
		
/* ]]> */
</script>

