<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage du plan du site 
 */

include_once( "../objects/classes.php" );
include_once( "../objects/Dictionnary.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );

class IndexController extends PageController{};

$tpl = (isset($_REQUEST["account"])) ? "plan_account.tpl" : "plan.tpl" ;
$plan = new IndexController( $tpl , "Plan du site" );

if(isset($_REQUEST["account"])) {

	
	$plan->setData( "contact_lastname", Basket::getInstance()->getSalesman()->get("lastname") );
	$plan->setData( "contact_firstname", Basket::getInstance()->getSalesman()->get("firstname") );
	$plan->setData( "contact_phonenumber", Basket::getInstance()->getSalesman()->get("phonenumber") );
	$plan->setData( "contact_faxnumber", Basket::getInstance()->getSalesman()->get("faxnumber") );
	$plan->setData( "contact_email", Basket::getInstance()->getSalesman()->get("email") );
	
	$plan->setData( "isPlan", true );
}

$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;

$querycat = "
SELECT cl.idcategorychild
FROM category_link cl, category c
WHERE cl.idcategoryparent = 0
AND cl.idcategorychild = c.idcategory
AND c.available = 1
AND c.catalog_right <= '$catalog_right'
ORDER BY cl.displayorder ASC";

$parentcat = DBUtil::query($querycat);

$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get("catalog_right" ) : 0;

$colonnes = 2;

$map = array();

for( $i = 0 ; $i < $parentcat->RecordCount() ; $i++ ){
	
	if( $i % $colonnes == 0 )
		$category[ $i ][ "startLine" ] = 1;
	else
		$category[ $i ][ "startLine" ] = "";
	
	//On sélectionne le nom de la catégorie parente
	$idcat = $parentcat->Fields( "idcategorychild" );
	$queryp = "SELECT name_1 FROM category WHERE idcategory = '$idcat' AND catalog_right <= $catalog_right";
	$resp = DBUtil::query( $queryp );
	
	//On sélectionne les categories enfants
	$queryenf = "SELECT idcategorychild FROM category_link WHERE idcategoryparent = '$idcat' ORDER BY displayorder ASC";
	$resenf = DBUtil::query( $queryenf );
	
	$href = URLFactory::getCategoryURL( $idcat, $resp->Fields( "name_1" ) );
	
	$category[ $i ][ "href" ] = $href;
	$category[ $i ][ "name" ] = $resp->Fields("name_1");
		
	$category[ $i ][ "children" ] = array();

	for( $f = 0 ; $f < $resenf->RecordCount() ; $f++ ){
		
		//Pour chaque enfant on sélectionne le nom
		$idcat = $resenf->Fields( "idcategorychild" );
		$queryf = "SELECT name_1 FROM category WHERE idcategory = '$idcat' AND catalog_right <= $catalog_right";
		$resf = DBUtil::query( $queryf );
		
		if( $resf->Fields( "name_1" ) != "" ){
			
			$category[ $i ][ "children" ][ $f ][ "href" ] = URLFactory::getCategoryURL( $idcat, $resp->Fields( "name_1") );
			$category[ $i ][ "children" ][ $f ][ "name" ] = $resf->Fields( "name_1");
			
		}
		
		$resenf->MoveNext();
		
	}

	$parentcat->MoveNext();
	
	if( $i == $parentcat->RecordCount() - 1 && $i % $colonnes == 0 )
		$category[ $i ][ "emptyColumn" ] = 1;
	else
		$category[ $i ][ "emptyColumn" ] = "";
	
}

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CCategoryProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/catalog/CCategory.php" );

//---------------------------------------------------------------------------//
//------------------------Arborescence des catégories------------------------//
//---------------------------------------------------------------------------//

$list_plan = "";
$querycat = "
SELECT cl.idcategorychild,
		c.name_1
FROM category_link cl, category c
WHERE cl.idcategoryparent = 0
AND cl.idcategorychild = c.idcategory
AND c.available = 1
AND c.idcategory != 23
AND c.catalog_right <= '$catalog_right'
ORDER BY cl.displayorder ASC";


$rs = DBUtil::query($querycat);

	while( !$rs->EOF )
	{
		$idcat = $rs->Fields( "idcategorychild" );
		$name_cat = $rs->fields( "name_1" );
		
		$list_plan .= '<article class="plan">';
		$list_plan .= '<h3><a href="'.URLFactory::getCategoryURL( $idcat ).'" title="'.$name_cat.'"><strong>'.$name_cat.'</strong></a></h3>';
	
		//sous-catégorie
		$querycat1 = "
					SELECT cl.idcategorychild,
					c.name_1
					FROM category_link cl, category c
						WHERE cl.idcategoryparent = ".$idcat ."
						AND cl.idcategorychild = c.idcategory
						AND c.available = 1
						AND c.idcategory != 23
						AND c.catalog_right <= '$catalog_right'
						ORDER BY cl.displayorder ASC";
						
						
		$rs1 = DBUtil::query($querycat1);
					
		while( !$rs1->EOF )
		{
			$idcat1 = $rs1->Fields( "idcategorychild" );
			$name_cat1 = $rs1->fields( "name_1" );
			
			$list_plan .= '<ul>';
			$list_plan .= '<li>';
			$list_plan .= '<a href="'.URLFactory::getCategoryURL( $idcat1 ).'" title="'.$name_cat1.'">'.$name_cat1.'</a>';
						
						
			$querycat2 = "
						SELECT cl.idcategorychild,
						c.name_1
						FROM category_link cl, category c
						WHERE cl.idcategoryparent = ".$idcat1 ."
						AND cl.idcategorychild = c.idcategory
						AND c.available = 1
						AND c.idcategory != 23
						AND c.catalog_right <= '$catalog_right'
						ORDER BY cl.displayorder ASC";
				
			$rs2 = DBUtil::query($querycat2);
						
			while( !$rs2->EOF )
			{
				$idcat2 = $rs2->Fields( "idcategorychild" );
				$name_cat2 = $rs2->fields( "name_1" );
										
				$list_plan .= '<ul style="margin-left:35px;">';
				$list_plan .= '<li>';
				$list_plan .= '<a href="'.URLFactory::getCategoryURL( $idcat2 ).'" title="'.$name_cat2.'">'.$name_cat2.'</a>';
				$list_plan .= '</li>';
				$list_plan .= '</ul>';
											
				$rs2->MoveNext();
			}
						
		$list_plan .= '</li>';
		$list_plan .= '</ul>';
					
		$rs1->MoveNext();
		}
		
		
	$list_plan .= '</article>';	 
	$rs->MoveNext();
	}






$plan->setData( "category", new CCategoryProxy( new CCategory( 0 ) ) );
$plan->setData( "title", Dictionnary::translate("plan_title")." ");
$plan->setData( "meta_description",  Dictionnary::translate("plan_description") );
$plan->setData( "list_plan", $list_plan );

$plan->output();

//----------------------------------------------------------------------------------------------

?>