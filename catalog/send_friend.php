<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi du produit à un pote
*/


if ( !isset($_GET["IdProduct"]) && !isset($_POST['IdProduct'])){
	header("location: $GLOBAL_START_URL");
}

function VerifierAdresseMail($adresse)  
{  
   $Syntaxe='#^[\w.-]+@[\w.-]+\.[a-zA-Z]{2,6}$#';  
   if(preg_match($Syntaxe,$adresse))  
      return true;  
   else  
     return false;  
}
$mailRec = (isset($_POST['emailReciever'])) ? VerifierAdresseMail($_POST["emailReciever"]) : false;
$mailSend = (isset($_POST['emailReciever'])) ? VerifierAdresseMail($_POST["emailSender"]) : false;



//require_once( "./deprecations.php" );

require_once ( dirname( __FILE__ ) . "/../config/init.php");
require_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );

//include_once( "Validate.php" ); //classe PEAR
//include_once( "Validate/FR.php" ); //classe PEAR
//var_dump($_POST);exit();
/*if ( !isset($_REQUEST["IdProduct"]) ){
	header("location: $GLOBAL_START_URL");
}*/

class ContactController extends PageController{};
$controller = new ContactController( "send_friend.tpl" );

$ScriptName = "send_friend.php";
$IdProduct = intval( $_POST["IdProduct"] )?$_POST["IdProduct"]:$_GET["IdProduct"];
//var_dump($IdProduct);exit();
$controller->setData("IdProduct",$IdProduct);
	
//if ( (isset ($_POST["emailReciever"]) && $_POST["emailReciever"]!="" && Validate::email($_POST["emailReciever"])) && (Session::getInstance()->getCustomer() || (isset ($_POST["emailSender"]) && $_POST["emailSender"]!="" && Validate::email($_POST["emailSender"])) ) ){
if ( $mailRec &&  $mailSend){	
	$_POST = Util::arrayMapRecursive('Util::doNothing', $_POST);
	include_once (dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php");

	// infos de l'emetteur
	$senderFirstname = $_POST["firstnameSender"];
	$senderLastname = $_POST["lastnameSender"];
	$senderEmail = $_POST["emailSender"];
	
	// infos du receveur
	$recieverFirstname = $_POST["firstnameReciever"];
	$recieverLastname = $_POST["lastnameReciever"];
	$recieverEmail = $_POST["emailReciever"];

	// compilation du template du mail a envoyer au pote de l'émetteur
	$options['templateDir'][] = "$GLOBAL_START_PATH/templates/catalog/";
	//$options['compileDir'] = "$GLOBAL_START_PATH/catalog/templates/";
	$options['compileDir'] = "$GLOBAL_START_PATH/templates/catalog/";
	$mail_copy = new HTML_Template_Flexy($options);
	
	$mail_copy->setData("firstnameSender",$senderFirstname);
	$mail_copy->setData("lastnameSender",$senderLastname);
	$mail_copy->setData("emailSender",$senderEmail);
	
	$mail_copy->setData("firstnameReciever",$recieverFirstname);
	$mail_copy->setData("lastnameReciever",$recieverLastname);
	$mail_copy->setData("emailReciever",$recieverEmail);
	
	// $mail_copy->setData("commentSender",$_POST["commentSender"]);
		
	include_once( "$GLOBAL_START_PATH/objects/catalog/CProduct.php" );
	include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CProductProxy.php" );
	$cproduct = new CProduct( $IdProduct );
	$product = new CProductProxy( $cproduct );
//print "41".$product->getURL();exit;
$mail_copy->setData( "product", $product );

	$mail_copy->compile("send_friend_mail.tpl");
	$data = $mail_copy->toString();
	
	// on met le tout en boite et roule ma poule
	$email = new htmlMimeMail();
	$email->setFrom($senderEmail);
	$email->setSubject( iconv("UTF-8","ISO-8859-15", "Un ami vous invite à consulter un produit") );
	$email->setHtml(stripslashes($data));
	
	$sendResult = $email->send(array($recieverEmail));
	
	if($sendResult==false){
		$controller->setData("errorSend","Un problème est survenu. Votre mail n'a pas été envoyé, veuillez réessayer.");
	}else{
		$controller->setData("errorSend","Votre mail d'invitation a bien été envoyé. Merci de votre confiance.");
	}
	$controller->setData("errorEmailSender","");
	$controller->setData("errorEmailReciever","");
}else{
	$controller->setData("errorSend","");
	
	//if(!Session::getInstance()->getCustomer() && (!isset ($_POST["emailSender"]) || $_POST["emailSender"]=="" || !Validate::email($_POST["emailSender"]))){
		if(!$_POST['emailReciever']){
		$controller->setData("errorEmailSender","");
	}else{
		$controller->setData("errorEmailSender","Votre adresse e-mail n'est pas valide, veuillez en saisir une valide.");
	}
	
	if(!$_POST['emailReciever']){
		$controller->setData("errorEmailReciever","");
	}else{
		$controller->setData("errorEmailReciever","L'adresse e-mail de votre ami(e) n'est pas valide, veuillez en saisir une valide.");
	}
	
}

$controller->output();

?>