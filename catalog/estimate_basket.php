<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage de la 1ère étape du devis
 */
 
include( "./deprecations.php" );


include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Basket.php" );
include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );

/* ajout au panier */

if( isset( $_REQUEST[ "idarticle" ] ) && isset( $_REQUEST[ "quantity" ] ) ){
	
	if( is_array( $_REQUEST[ "idarticle" ] ) && is_array( $_REQUEST[ "quantity" ] ) ){
	
		$i = 0;
		while( $i < count( $_REQUEST[ "idarticle" ] ) ){
			
			Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "idarticle" ][ $i ] ), $_REQUEST[ "quantity" ][ $i ] );
			$i++;
			
		}
		
	}
	else Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "idarticle" ] ), $_REQUEST[ "quantity" ] );;
	
}

/* --------------------------------------------------------------------------------------------------- */
/* multisites : redirection pour les sites satellites */

if( file_exists( dirname( __FILE__ ) . "/../config/multisite.xml" ) ){

	header( "Location: /catalog/estimate_multisite.php" );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------- */

//important pour les popups
header('Content-Type: text/html; charset=utf-8'); 

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketItemProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CCategoryProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/WebPageController.php" );

class EstimateBasketController extends WebPageController{};


$estimate_basket_page = new EstimateBasketController( "estimate_basket.tpl" , "Devis (panier)" );
$estimate_basket_page->setTypeEncoding( 2);
$categ = new CCategory( 0 );
$estimate_basket_page->setData( "category", new CCategoryProxy( $categ ) );

$items = array();
$it = Basket::getInstance()->getItems()->iterator();

while( $it->hasNext() )
	array_push( $items, new BasketItemProxy( $it->next() ) );
	
$estimate_basket_page->setData( "basketItems", $items );

$lang = "_1";

//-----------------------------------------------------------------------------------------------------------------

if( isset( $_GET[ "next" ] ) && ( isset( $_POST[ "GoToCata_x" ] ) || isset( $_POST[ "CatBack_x" ] ) ) ){
	
	Basket::getInstance()->save();
	header( "Location:$GLOBAL_START_URL" );
	
}

$BuyingScript = "estimate_complete.php";
$ScriptName = "estimate_basket.php";

$estimate_basket_page->setData( "buyingscript", "estimate_complete.php" );
$estimate_basket_page->setData( "scriptname", "estimate_basket.php" );
$estimate_basket_page->setData( "fromscript", basename( $_SERVER[ "PHP_SELF" ] ) );

//---------------------------------------------//
//--------------Google conversion--------------//
//---------------------------------------------//

$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_conversion_id = DBUtil::getParameterAdmin( "google_estimate_conversion_id" );


if( $google_conversion_id && $UseGoogleConversion ){
	$estimate_basket_page->setData('google',1);
	$estimate_basket_page->setData('google_conversion_id',$google_conversion_id);  
}else{
	$estimate_basket_page->setData('google',0);
}

//----------------------------------------------------------------------------------------------
//ajout d'articles

/**
 * @todo :
 * => plus de IdRealProduct, préférer idarticle
 * => les seules variables nécéssaires dans le formulaire de demande de devis/cde sont : idarticle, quantity, idcolor et idtissus
 */


if( isset( $_POST[ "EstimateSelection" ] ) || isset( $_POST[ "EstimateSelection_x" ] ) ){
	
	createBasketFromSelection();
	
}
else if( isset( $_REQUEST[ "IdArticle" ] ) ){
	
	$qtt = $_POST[ "quantity" ];
   	
    if( is_array( $_REQUEST[ "IdArticle" ] ) ) {
		
    	if( !empty( $_REQUEST[ "estimate" ] ) ) {
    		
    		foreach( $_REQUEST[ "estimate" ] as $k => $order ) {
    			
				Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "IdRealProduct" ][ $k ] ), $_REQUEST[ "quantity" ][ $k ] );
				
    		}
    		
    	}
    	
    }else{
    	$compte = Basket::getInstance()->getItemCount();
    	$pos	 = 0 ;
    	
    	$idcloth = isset( $_REQUEST[ "idtissus" ] ) ? $_REQUEST[ "idtissus" ] : 0;
	    $idcolor = isset( $_REQUEST[ "idcolor" ] ) ? $_REQUEST[ "idcolor" ] : 0;
	    $modify = false;
	    
		for($i = 0 ; $i < $compte ; $i++){
    		if($_REQUEST[ "IdArticle" ] == Basket::getInstance()->getItemAt($i)->getArticle()->get("idarticle") ){
    			$modify = true;
    			$pos = $i;
    		}
    	}
    	if($modify===true){
    		$_POST[ "qtty_$pos" ] = $qtt + Basket::getInstance()->getItemAt($pos)->getQuantity();
    		Basket::getInstance()->save();
    	}else{
			Basket::getInstance()->addArticle( new CArticle( $_REQUEST[ "IdArticle" ], $idcolor, $idcloth ), $qtt );
    	}
    }
    
	if(isset($_REQUEST['IdArticle'])){
		if(isset($_REQUEST['fromScript']) && $_REQUEST['fromScript']!="" ){
			$url_redirect = $_REQUEST['fromScript'];
			$_SESSION["idproduit"] = $_REQUEST['IdProduct'];

		} else if(!isset($_REQUEST['fromAccess'])){
			$url_redirect = URLFactory::getProductURL(DBUtil::getDBValue( "idproduct", "detail", "idarticle", $_REQUEST[ "IdArticle" ] ) );

		}
	}else if(isset($_REQUEST['checkArticle'])){
		$url_redirect = $GLOBAL_START_URL."/catalog/composition.php";
	}
	
	if(isset($_POST["fromAccess"]) && $_POST["fromAccess"]==1 ){
		$url_redirect = $GLOBAL_START_URL."/catalog/estimate_basket.php";
	}
	
	$hide_template = DBUtil::getParameterAdmin( "hide_direct_basket" );
	
	if($hide_template){
		header( "Location:$url_redirect");
		exit;
	}
}

// Ajouts de plusieurs articles en meme temps (groupement)
if( isset($_REQUEST['IdArticles']) ){
	
	$IdArticles = $_REQUEST['IdArticles'];
    
    for($i=0;$i<count($IdArticles);$i++){
    	
    	$IdArt = $IdArticles[$i];
   		$qtt = $_POST["quantity_$IdArt"];
   
				
		Basket::getInstance()->addArticle( new CArticle( $IdArt ),$qtt);
				
    }

}

//----------------------------------------------------------------------------------------------
//modifications des quantités

if( isset( $_POST[ "Modify" ] ) || isset( $_POST[ "Modify_x" ] ) || isset( $_POST[ "Buy" ] ) || isset( $_POST[ "Buy_x" ] ) || ( isset( $deleteItem ) && $deleteItem ) )
	Basket::getInstance()->save();
	
//----------------------------------------------------------------------------------------------
//suppression d'article

$i = Basket::getInstance()->getItemCount() - 1;
while( $i >= 0 ){
	
	if( isset( $_POST[ "Delete_$i" ] ) || isset( $_POST[ "Delete_" . $i . "_x" ] ) )
		Basket::getInstance()->removeItemAt( $i );
	
	$i--;
	
}

//----------------------------------------------------------------------------------------------

$baseURL = $GLOBAL_START_URL;

//---------------------------------------------//
//-------------------Account-------------------//
//---------------------------------------------//
if( Session::getInstance()->getCustomer() ) {
	
	$estimate_basket_page->setData( "isLogin", "ok" );
	$BuyerLogin = Session::getInstance()->getCustomer()->getContact()->getEmail();
	
	$estimate_basket_page->setData( "buyerLogin", $BuyerLogin );
	
} else {
	
	$estimate_basket_page->setData( "isLogin", "" );
	$com_step2 = true;
	
	if( empty( $com_step2 ) )
		$estimate_basket_page->setData( "com_step_empty", "ok" );
	else
		$estimate_basket_page->setData( "com_step_empty", null );
	
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		
		$estimate_basket_page->setData( "issetPostLoginAndNotLogged", "ok" );
		$estimate_basket_page->setData( "translateMSGErrorLoginPassword", Dictionnary::translate( "MSGErrorLoginPassword" ) );
		
	} else {
		
		$estimate_basket_page->setData( "issetPostLoginAndNotLogged", "" );
		
	}
	
	$estimate_basket_page->setData( "translateLogin", Dictionnary::translate( "login" ) );
	$estimate_basket_page->setData( "translatePassword", Dictionnary::translate( "password" ) );
	
}

//---------------------------------------------//
//----------Code spécifique à la page----------//
//---------------------------------------------//
$basket_artcount = Basket::getInstance()->getItemCount();
$estimate_basket_page->setData( "basket_artcount", $basket_artcount );

$message = Dictionnary::translate( "empty_basket_estimate" );
$estimate_basket_page->setData( "message", $message );

$basket_article = array();
$hasDiscount = "";

for( $i = 0 ; $i < Basket::getInstance()->getItemCount() ; $i++ ){

	if( Basket::getInstance()->getItemAt( $i)->getDiscountRate() > 0.0 )
		$hasDiscount = "ok";
	
}

$estimate_basket_page->setData( "hasdiscount", $hasDiscount );

$commercial_set = isset( $_SESSION[ "commercial" ] ) ? "ok" : "";
$estimate_basket_page->setData( "commercial_set", $commercial_set );

$get_m = isset( $_GET[ "m" ] ) ? unserialize( base64_decode( $_GET[ "m" ] ) ) : "";
$estimate_basket_page->setData( "get_m", $get_m );

if( isset( $_GET[ "f" ] ) ){
	
	$form = array();
	$form = unserialize( base64_decode( $_GET[ "f" ] ) );
	
}

//Récupération de la civilité
$selection = isset( $buyer[ "title" ] ) ? $buyer[ "title" ] : "";

$query = "SELECT idtitle, title$lang FROM title ORDER BY idtitle ASC";
$rs = DBUtil::query( $query );

if( $rs === false || !$rs->RecordCount() )
	die( "Impossible de récupérer la liste des civilités" );

$html = "\n						<select name=\"title\" class=\"select_product\">\n";
while( !$rs->EOF() ){

	$title = $rs->fields( "title$lang" );
	$id = $rs->fields( "idtitle" ); 
	$selected = $id == $selection ? " selected=\"selected\"" : "";
	
	$html .= "							<option value=\"$id\"$selected>" . htmlentities( $title ) . "</option>\n";

	$rs->MoveNext();
	
}

$html .= " 					</select>\n";

$estimate_basket_page->setData( "titlelist", $html );

$votremessage =  isset( $_POST[ "message" ] ) ? stripslashes( $_POST[ "message" ] ) : "";

$estimate_basket_page->createElement( "message" , $votremessage);


$estimate_basket_page->output();


//----------------------------------------------------------------------------------------------

function createBasketFromSelection(){

	$idArticles = $_POST[ "idArticleSelection" ];
	$quantities = $_POST[ "quantitySelection" ];
	
	$idArticleArray = array();	
	$quantityArray = array();
	
	$token = strtok( $idArticles, "," );
	while( $token !== false ){
			
		$idArticleArray[] .= $token;
		$token = strtok( "," );
			
	}
	
	$token = strtok( $quantities, "," );
	while( $token !== false ){
					
		$quantityArray[] .= $token;
		$token = strtok( "," );
			
	}
	
	if( isset( $_POST[ "EstimateSelection" ] ) || isset( $_POST[ "EstimateSelection_x" ] ) ){

		$i = 0;
		while( $i < count( $quantityArray ) ){
			
			
			Basket::getInstance()->addArticle( new CArticle( $idArticleArray[ $i ] ) , $quantityArray[ $i ] );
			
			$i++;
		}
		
	}

}

//----------------------------------------------------------------------------------------------

?>