<?
include_once( "../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );

class StarsProductsController extends WebPageController{};
$stars_products = new StarsProductsController( "stars_products.tpl" , "Produits stars" );

$stars_products->GetPromotions();
$stars_products->GetCategoryLinks();
$stars_products->GetBrands();
$stars_products->GetTrades();
$stars_products->GetProductHistory();

$stars_products->GetStarProductsList();

$stars_products->output();

?>