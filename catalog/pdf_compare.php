<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf comparatif produits
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFCompareProduct.php" );

if( isset( $_REQUEST[ "articles" ] ) )
	$articles = explode( ",", $_REQUEST[ "articles" ] );
else{
	
	/* --------------------------------------------------------------------------------- */
	//@toto delete
	if( isset( $_POST[ "comparer" ] ) ){
		
		$tab_refs = $_POST[ "comparer" ];
		$tab_id = array();
		
		//recherche des identifiants associés
		foreach( $tab_refs as $ref )
			$tab_id[] = DBUtil::getDBValue( "idarticle", "detail", "reference", $ref );
		
		$articles = $tab_id;
		
	} else {
		$articles = $_SESSION[ "comproducts" ];
	}
	
	/* --------------------------------------------------------------------------------- */

}

$odf = new ODFCompareProduct( $articles );
$odf->exportPDF( "Comparatif de produits.pdf" );

?>