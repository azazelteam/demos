<?php

/**
 * @package FrontOffice
 * @author Nicolas Gerner
 * @param Integer n_largeur Nouvelle largeur de l'image
 * @param Integer n_hauteur Nouvelle hauteur de l'image
 * @param String monImage Chemin complet de l'image à redimenssioner
 * 
 * Cette fonction utilise la librairie GD afin de redimenssioner les images
 **/

$n_largeur = $_GET["n_largeur"];
$n_hauteur = $_GET["n_hauteur"];
$monImage = $_GET["monImage"];


	header('Content-type: image/jpeg');

	// Calcul des nouvelles dimensions
	list($largeur, $hauteur) = getimagesize($monImage); //list est un moyen plus pratique pour ne récupérer que ce qu'on veut
		
	//création de la destination
	$destination = imagecreatetruecolor($n_largeur, $n_hauteur);
	
	//on ouvre la source
	$source = imagecreatefromjpeg($monImage);
	
	// Redimensionnement
	imagecopyresampled($destination, $source, 0, 0, 0, 0, $n_largeur, $n_hauteur, $largeur, $hauteur);
	
	imagejpeg($destination);
	imagedestroy($destination);
	imagedestroy($source);
?>