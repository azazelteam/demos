<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */

include_once("../config/init.php" );
include_once("$GLOBAL_START_PATH/objects/CSSMenu.php" );

$depth = isset( $_REQUEST[ "depth" ] ) ? intval( $_REQUEST[ "depth" ] ) : -1;

$idcategory = isset( $_REQUEST[ "idcategory" ] ) ?  intval( $_REQUEST[ "idcategory" ] ) : 0;
$menu = new CSSMenu( $depth, $idcategory );

$menu->showHomeItem( true );
//$menu->addStaticItem( "Accès par métier","$GLOBAL_START_URL/catalogue-des-metiers", "", "metier" );
//$menu->addStaticItem( "Promos","$GLOBAL_START_URL/promotions", "", "promos" );
//$menu->addStaticItem( "Déstockage","$GLOBAL_START_URL/catalog/destock.php", "", "destock" );

header( "Content-Type: text/html; charset=utf-8" );

echo $menu->getHTML();
		
?>