<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement du devis en ligne + enregistrement du client
 * Affichage de la page de confirmation du traitement du devis
*/

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Estimate.php" );
include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../script/global.fct.php" );
include_once( dirname( __FILE__ ) . "/../objects/Customer.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );

//validation des données
require_once( 'PEAR.php' );
include_once( 'Validate.php' ); //classe PEAR
include_once( 'Validate/FR.php' ); //classe PEAR

/* --------------------------------------------------------------------------------------------------------------- */
//panier vide

if( !Basket::getInstance()->getItemCount() ){
	
	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------------------- */

User::fakeAuthentification();

/* protection XSS */
$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

$error_msg = "";
$isregister = false;
$already_register = false;

/* --------------------------------------------------------------------------------------------------------------- */
//création d'un nouveau compte

if( !Session::getInstance()->getCustomer() ){
	
//	if ( Validate::email( stripslashes( $_POST[ "email" ] ) ) ){
		if ( isset(  $_POST[ "email" ] ) ){
		$rs = DBUtil::query( "SELECT * FROM contact WHERE mail = " . DBUtil::quote( stripslashes( $_POST[ "email" ] ) ) );
		
		if( $rs->RecordCount() > 0 ){
			
			$already_register = true;
			$_SESSION[ "idcontact" ] = $rs->fields( "idcontact" );
		
			/* ici */
			$rs2 = DBUtil::query( "SELECT login, password FROM user_account WHERE idbuyer = '" . $rs->fields( "idbuyer" ) . "' AND idcontact = '" . $rs->fields( "idcontact" ) . "' LIMIT 1" );
			Session::getInstance()->login( $rs2->fields( "login" ), $rs2->fields( "password" ) );
			
		}else{

			if( empty( $_POST[ "lastname" ] ) )
				$error_msg = base64_encode( serialize( Dictionnary::translate( "register_lastname_required" ) ) );
			//else if( empty( $_POST[ "email" ] ) || !Validate::email( $_POST[ "email" ] ) ) 
				else if( empty( $_POST[ "email" ] )  ) 
				$error_msg = base64_encode( serialize( Dictionnary::translate( "pear_validator_email_error" ) ) );
			else if( empty( $_POST[ "phonenumber" ] ) ) 
				$error_msg = base64_encode( serialize( Dictionnary::translate( "pear_validator_phonenumber_error" ) ) );
			else if( empty( $_POST[ "zipcode" ] ) )
				$error_msg = base64_encode( serialize( Dictionnary::translate( "register_zipcode_required" ) ) );
			
			//Vérifier le format des données
			$_POST[ "phonenumber" ] = str_replace( " ", "", $_POST[ "phonenumber" ] );
			$_POST[ "faxnumber" ] = str_replace( " ", "", $_POST[ "faxnumber" ] );
			$_POST[ "zipcode" ] = str_replace( " ", "", $_POST[ "zipcode" ] );
			
		}
		
	}else{
		
		$error_msg = base64_encode(serialize(Dictionnary::translate( "pear_validator_email_error" )));
		
	}
	
	$validatePostData = strlen( $error_msg );
	
	if( $validatePostData > 0 ){ //on redirige avec le message d'erreur
		
		$f = base64_encode( serialize( $_POST ) );
		header( "Location: $GLOBAL_START_URL/catalog/estimate_basket.php?m=$error_msg&f=$f" );
		exit();
		
	}else{
		
		if( !$already_register )
			$isregister = registerUser();
		
	}
	
}

/* --------------------------------------------------------------------------------------------------------------- */

if( Session::getInstance()->getCustomer() || $isregister || $already_register ){
	
	//enregistrer le devis
	$Comment = $_POST["message"];	
	
	$estimate = TradeFactory::createEstimateFromBasket();
	
	$estimate->set("comment", stripslashes( strip_tags( $_POST["message"] ) ) );
	
	/* paiement à la commande si total HT < montant paramétré */
	
	$auto_estimate = DBUtil::getParameter( "auto_estimate" ); //Montant à partir duquel le devis n'est plus automatique et nécessite l'intervention d'un commercial
	
	if( $auto_estimate > 0.0 && $estimate->getTotalET() > 0.0 && $estimate->getTotalET() <= $auto_estimate )
		$estimate->set( "idpayment_delay", 1 );
		
	$estimate->save();

	$Msg_error = "Exit Basket2Estimate !";
	$Msg_already = "Devis déjà traité !";
	
	$msgfromcommande = "Devis enregistré";

	//-----------------------------------------------------------------------------------------
	/*
	 * devis auto si :
	 * 	- 1 seul produit
	 * 	- prix affiché sur le site
	 * 	- frais de port gérés
	 * 	- pas de commentaire
	 * 	- total HT <= paramètre catalogue "auto_estimate"
	 * 	- délais affiché 
	 */

	$customer = new Customer( $estimate->get( "idbuyer" ),  $estimate->get( "idcontact" ) );
	
	$auto_estimate = DBUtil::getParameter( "auto_estimate" ); //Montant à partir duquel le devis n'est plus automatique et nécessite l'intervention d'un commercial
	
	if( $auto_estimate > 0.0
		&& $estimate->getItemCount() == 1 
		//&& allowAutoEstimateForCategory( $estimate->getItemAt( 0 )->get( "idcategory" ) )
		&& !strlen( $estimate->get( "comment" ) )
		&& $estimate->get( "total_amount_ht" ) > 0.0
		&& $estimate->get( "total_amount_ht" ) <= floatval( $auto_estimate ) 
		&& !DBUtil::getDBValue( "hidecost", "detail", "idarticle", $estimate->getItemAt( 0 )->get( "idarticle" ) ) 
		&& DBUtil::getDBValue( "sellingcost", "detail", "idarticle", $estimate->getItemAt( 0 )->get( "idarticle" ) ) > 0.0 
		&& $estimate->getItemAt( 0 )->get( "delivdelay" )
		&& $customer->get( "idstate" ) == 1
		&& strlen( $customer->get( "zipcode" ) ) >= 4 && substr( $customer->get( "zipcode" ), 0, 2 ) != "20" ){

		include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
		
		$estimate->set( "status", Estimate::$STATUS_SENT );
		$estimate->set( "relaunch", date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) + 7, date( "Y" ) ) ) ); //relance sous 7 jours
		$estimate->set( "idpayment", Payment::$PAYMENT_MOD_UNDEFINED );
		$estimate->set( "payment_idpayment", Payment::$PAYMENT_MOD_UNDEFINED );
		$estimate->set( "auto", 1 );
		
		$userAuto = DBUtil::getParameter( "contact_commercial_auto_estima" );
		
		if(!$userAuto)
			$userAuto = DBUtil::getParameter( "contact_commercial" );
			
		$estimate->set( "iduser", $userAuto );
		
		$estimate->save();
		
	}
	
	//-----------------------------------------------------------------------------------------
	//infos client, commercial et admin
	
	$AdminName = DBUtil::getParameterAdmin( "ad_name" );
	$AdminEmail = DBUtil::getParameterAdmin( "ad_mail" );
	
	$iduser = DBUtil::getParameter( "contact_commercial" );

	$query = "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1";
	$rs =& DBUtil::query( $query );
	
	$CommercialName 	= $rs->fields( "firstname" ).' '. $rs->fields( "lastname" );
	$CommercialEmail 	= $rs->fields( "email" );
	
	$BuyerName = $estimate->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $estimate->getCustomer()->getContact()->get( "mail" );
	
	//-----------------------------------------------------------------------------------------
	
	//charger le modèle de mail
	
	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	
	if( $estimate->get( "status" ) == Estimate::$STATUS_TODO ) //devis en dessus d'un certains montant : à traiter par le commercial
			$editor->setTemplate( MailTemplateEditor::$ESTIMATE_NOTIFICATION );
	else	$editor->setTemplate( MailTemplateEditor::$FRONT_OFFICE_ESTIMATE ); //devis auto
	
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseEstimateTags( $estimate->getId() );
	$editor->setUseBuyerTags( $estimate->get( "idbuyer" ),$estimate->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
	
	//-----------------------------------------------------------------------------------------
	//envois du mail
	
	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$mailer = new htmlMimeMail();
	$mailer->setFrom( "\"$AdminName\" <$AdminEmail>" );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setCc( $CommercialEmail );
	
	$mailer->setHtml( $Message );
	$mailer->setSubject( $Subject );
	
	if( $estimate->get( "status" ) == Estimate::$STATUS_SENT ){ //devis auto
		
		//devis PDF
		
		include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );
		$odf = new ODFEstimate( $estimate->getId() );
	
		$mailer->addAttachment( $odf->convert( ODFDocument::$DOCUMENT_PDF ), "Devis_" . $estimate->getId() . ".pdf", "application/pdf" );
		
		//fiches produit

		$it = $estimate->getItems()->iterator();
		$products = array();
		
		while( $it->hasNext() )
			array_push( $products, DBUtil::getDBValue( "idproduct", "detail", "idarticle", $it->next()->get( "idarticle" ) ) );
	
		$products = array_unique( $products );
		foreach( $products as $idproduct )
			if( $idproduct )
				$mailer->addAttachment( file_get_contents( URLFactory::getPDFURL( $idproduct) ), "Notre fiche technique N° $idproduct.pdf", "application/pdf" );
	
	}
	
	$result = $mailer->send( array( $BuyerEmail ) );
	
	if( !$result )
		$errorMsg[] = array( "var"=>$EVM_Mailer->get_errors(), "user"=>Dictionnary::translate( "error" ), "log"=>$ScriptName." => $EVM_Mailer->get_errors()" );

	//-----------------------------------------------------------------------------------------

	//afficher le devis

	include( "$GLOBAL_START_PATH/catalog/confirm_estimate.php" );
	
}

//---------------------------------------------------------------------------------------------

function registerUser(){
	
	include_once( dirname( __FILE__ ) . "/registration_parseform.php" );
	
	if( $error_msg != "" )
		return false;
	
	/* ici */
	$rs2 = DBUtil::query( "SELECT login, password FROM user_account WHERE idbuyer = '" . $customer->get( "idbuyer" ) . "' AND idcontact = '" . $customer->get( "idcontact" ) . "' LIMIT 1" );
	Session::getInstance()->login( $rs2->fields( "login" ), $rs2->fields( "password" ) );
			
	return true;
	
}

//---------------------------------------------------------------------------------------------

?>
