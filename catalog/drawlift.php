<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Liste des différents select
 */

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift($Table,$Select='',$Condition='',$enabled=true, $isTemplate=false, $Name='', $onChange='')
{
	global $isTemplate;
	
	$Langue = "_1";
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	$onChangeTage = $onChange == '' ? '' : ' onchange="'.$onChange.'"';
	if( empty($Name) ) $Name = "id$Table";
	  $return = "<select id=\"$Name\" $onChangeTage name=\"$Name\"$disabled>\n";
        $Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select id$Table as id, name$Langue as name from $Table $Condition");
        if($rs->RecordCount()>0)
        {
                for($i=0 ; $i < $rs->RecordCount() ; $i++)
                {
                        if( $Select == $rs->Fields('id') )
                        {
                                $return .= "<option value='" . $rs->Fields('id') . "' selected=\"selected\">" . $rs->Fields('name') . "</option>";
                        }
                        else
                        {
                                $return .= "<option value='" . $rs->Fields('id') . "'>" . $rs->Fields('name') . "</option>";
                        }
                        $rs->MoveNext();
                }
        }
        else
        {       $return .= "<option selected value=0>-</option>";}
        $return .= "</select>\n";
        
        if($isTemplate) 
			return $return;
		else 
			echo $return;
}

/**
 * liste déroulante pour les paramèteres du type oui/non
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLiftParameters($param, $type1='Oui', $type2='Non'){
	$Base = DBUtil::getConnection();
	
	$selected = ' selected="selected"';
	
	$req = "SELECT idparameter,paramvalue FROM parameter_admin WHERE idparameter like '$param'";
	$res =& DBUtil::query($req);
	
	$HTMLselect= "<select name=\"$param\">\n";
	if($res->fields( "paramvalue" )==1){
		$HTMLselect.= "\t<option value='1' $selected>".$type1."</option>\n";		
		$HTMLselect.= "\t<option value='0' >".$type2."</option>\n";
	}else{
		$HTMLselect.= "\t<option value='1' >".$type1."</option>\n";
		$HTMLselect.= "\t<option value='0' $selected>".$type2."</option>\n";
	}

	$HTMLselect.= "</select>\n";
	
	return $HTMLselect;
}



/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_id ($Table, $Select='', $Condition='', $Name='',$enabled = true)
{
	$Langue = "_1";
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	if( empty($Name) ) $Name = "id$Table";
	$HTML_select = '<select name="'.$Name.'" '.$disabled.' id="'.$Name.'">'."\n";
	$Base = &DBUtil::getConnection();
	$SQL_select = "SELECT `id$Table` AS `id`, `name$Langue` AS `name` FROM `$Table` $Condition";
	$rs =& DBUtil::query($SQL_select);
	if( $rs === false )
		trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_id() => $SQL_select','sql'),E_USER_NOTICE);
	if( empty($rs) ) trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_id() => $SQL_select','log'),E_USER_ERROR);
	if( $rs->EOF ) {
		$HTML_select .= "\t".'<option value="0">-</option>'."\n";
	}
	else {
		while( !$rs->EOF ) {
			$id = $rs->Fields('id');
			$name = $rs->Fields('name');
			$selected = null;
			if( $Select == $id ) {
				$selected = ' selected="selected"';
			}
			$HTML_select .= "\t".'<option value="'.$id.'"'.$selected.'>'.$name."</option>\n";
			$rs->MoveNext();
        }
	}
    $HTML_select .= "</select>\n";
    return $HTML_select;
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_table ($Table, $Select='', $Condition='')
{

	$Langue = "_1";
	$HTML_select = '<select name="'.$Table.'">'."\n";
	$Base = &DBUtil::getConnection();
	$SQL_select = "SELECT `$Table$Langue` AS `$Table` FROM `$Table` $Condition";
	$rs =& DBUtil::query($SQL_select);
	
	//trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_table() => $SQL_select','sql'),E_USER_NOTICE);
	if( empty($rs) ) trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_table() => $SQL_select','log'),E_USER_ERROR);
	if( $rs->EOF ) {
		$HTML_select .= "\t".'<option value="">-</option>'."\n";
	}
	else {
		while( !$rs->EOF ) {
			$v = $rs->Fields($Table);
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			$HTML_select .= "\t".'<option value="'.$v.'"'.$selected.'>'.$v."</option>\n";
			$rs->MoveNext();
        }
	}
    $HTML_select .= "</select>\n";
    return $HTML_select;
}

/**
 * scoring Devis
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_scoring ($Select='X', $Frech='rech'){
	$Langue = "_1";
	$HTML_select = "<select name=\"scoring_tx\" >\n";
	$Base = DBUtil::getConnection();
	$SQL_select = "SELECT scoring_tx, scoring_name FROM scoring_estimate ORDER BY scoring_tx ASC";
	
	$rs =& DBUtil::query($SQL_select);

	//trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_table() => $SQL_select','sql'),E_USER_NOTICE);
	if( empty($rs) ) trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_scoring() => $SQL_select','log'),E_USER_ERROR);
	
	if( $rs->EOF ) {
		$HTML_select .= "\t".'<option value="">-</option>'."\n";
	}else{
		if($Frech=='rech'){
			$HTML_select .="<option value=\"X\">Tous</option>";
		}
		if($Select!='X'){
			while( !$rs->EOF ) {
				$id = $rs->Fields("scoring_tx");
				$txt= $rs->Fields("scoring_name");
				$selected = null;
				if( $Select == $id ) {
					$selected = ' selected="selected"';
				}
				$HTML_select .= "\t".'<option class="specialselect" value="'.$id.'"'.$selected.'>'.$id.'&nbsp;-&nbsp;'.$txt."</option>\n";
				$rs->MoveNext();
	        }
		}else{
			while( !$rs->EOF ) {
				$id = $rs->Fields("scoring_tx");
				$txt= $rs->Fields("scoring_name");
				$HTML_select .= "\t".'<option class="specialselect" value="'.$id.'">'.$id.'&nbsp;-&nbsp;'.$txt."</option>\n";
				$rs->MoveNext();
	        }
		}
	}
    $HTML_select .= "</select>\n";
    echo $HTML_select;
}

/**
 * scoring Clients
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_scoring_buyer ($Select=''){
	$Langue = "_1";
	$HTML_select = "<select name='idscoring_buyer' class='fullWidth'>\n";
	$Base = DBUtil::getConnection();
	$SQL_select = "SELECT idscoring_buyer, txtscoring_buyer FROM scoring_buyer";
	
	$rs =& DBUtil::query($SQL_select);
	$HTML_select .="<option value=\"0\">Sélectionner scoring</option>";
	//trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_table() => $SQL_select','sql'),E_USER_NOTICE);
	if( empty($rs) ) trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_scoring_buyer() => $SQL_select','log'),E_USER_ERROR);
	
	if( $rs->EOF ) {
		$HTML_select .= "\t".'<option value="">-</option>'."\n";
	}else{
		if($Select!=''){
			while( !$rs->EOF ) {
				$id = $rs->Fields("idscoring_buyer");
				$txt= Dictionnary::translate($rs->Fields("txtscoring_buyer"));
				$selected = null;
				if( $Select == $id ) {
					$selected = ' selected="selected"';
				}
				$HTML_select .= "\t".'<option value="'.$id.'"'.$selected.'>'.$id.'&nbsp;&nbsp;-&nbsp;&nbsp;'.$txt."</option>\n";
				$rs->MoveNext();
	        }
		}else{
			while( !$rs->EOF ) {
				$id = $rs->Fields("idscoring_buyer");
				$txt= Dictionnary::translate($rs->Fields("txtscoring_buyer"));
				$HTML_select .= "\t".'<option value="'.$id.'">'.$id.'&nbsp;&nbsp;-&nbsp;&nbsp;'.$txt."</option>\n";
				$rs->MoveNext();
	        }
		}
	}
    $HTML_select .= "</select>\n";
    echo $HTML_select;
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_catright($Table,$Select='',$Condition='', $javascript=0, $disabled='')
{
	$Langue = "_1";
	if ($javascript==0){
		$HTML_select = "<select name='catalog_right' $disabled class='fullWidth'>\n";
	}else{
		$HTML_select = "<select name='catalog_right' $disabled class='fullWidth' onchange='verifcatrightforrefus($Select);' onkeyup='verifcatrightforrefus($Select);'>\n";
	}
	$HTML_select .= "<option value='-'>Tous</option>";
	$Base = DBUtil::getConnection();
	$SQL_select="select idcatalog_right, catalog_right$Langue from $Table $Condition ORDER BY idcatalog_right ASC";
	$rs =& DBUtil::query($SQL_select);
	if( empty($rs) ) trigger_error(showDebug($SQL_select,'drawlift.php::DrawLift_catright() => $SQL_select','log'),E_USER_ERROR);
	
	if ($rs->EOF){
		$HTML_select .= "\t".'<option value="">-</option>'."\n";
	}else{
			if($Select!='-'){
				while( !$rs->EOF ) {
					$v = $rs->Fields('idcatalog_right');
					$selected = null;
					if( $Select == $v ) {
						$selected = ' selected="selected"';
					}
					$HTML_select .= "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('catalog_right'.$Langue)."</option>\n";
					$rs->MoveNext();
		        }
			}else{
				while( !$rs->EOF ) {
					$v = $rs->Fields('idcatalog_right');
					$selected = null;
					$HTML_select .= "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('catalog_right'.$Langue)."</option>\n";
					$rs->MoveNext();
		        }
			}
	}
	$HTML_select .= "</select>\n";
    echo $HTML_select;
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_referenced($Select='-'){
		
	$Langue = "_1";
	$HTML_select = "<select name='buyer_ref_us'>\n";
	$Base = DBUtil::getConnection();
	
	if($Select!='-'){
		$selected = null;
		if( $Select == 1 ) {
			$selected = ' selected="selected"';
			$HTML_select .= "<option value='-'>-</option>";
			$HTML_select .= "\t"."<option value=1".$selected.">".Dictionnary::translate( "yes" )."</option>\n";
			$HTML_select .= "\t"."<option value=0>".Dictionnary::translate( "no" )."</option>\n";	
		}else{
			$selected = ' selected="selected"';
			$HTML_select .= "<option value='-'>-</option>";
			$HTML_select .= "\t"."<option value=1>".Dictionnary::translate( "yes" )."</option>\n";
			$HTML_select .= "\t"."<option value=0".$selected.">".Dictionnary::translate( "no" )."</option>\n";
		}
	}else{
		$selected = ' selected="selected"';
		$HTML_select .= "<option value='-'".$selected.">-</option>";
		$HTML_select .= "\t"."<option value=1>".Dictionnary::translate( "yes" )."</option>\n";
		$HTML_select .= "\t"."<option value=0>".Dictionnary::translate( "no" )."</option>\n";	
	}	
	$HTML_select .= "</select>\n";
    echo $HTML_select;
}


/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_1($Table,$Select='',$Condition='', $enabled = true, $isTemplate = false )
{
	
	$Langue = "_1";
	
    $return = "<select name=\"idfunction\" id=\"idfunction\" ";
    
    if( !$enabled )
    	 $html .=  " disabled=\"disabled\"";
    	
    $return .=  ">\n";
    $Base = &DBUtil::getConnection();
    
	$rs =& DBUtil::query("select idfunction, function$Langue as function from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('idfunction');
			$w = $rs->Fields('function');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			$return .=  "\t".'<option value="'.$v.'"'.$selected.'>'.$w."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		$return .=  "\t".'<option value="0">-</option>'."\n";
	}
	
    $return .=  "</select>\n";
    
    if($isTemplate === true){
    	return $return;
    }else{
    	echo $return ; 
    }
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_2( $Table, $Select='', $Condition='', $enabled = true, $isTemplate = false )
{
	global $isTemplate;
	$Langue = "_1";
	$return = "<select name=\"iddepartment\"";
	
	if( !$enabled )
    	$return .= " disabled=\"disabled\"";
    	
	$return .= ">\n";
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select iddepartment, department$Langue as department from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('iddepartment');
			$w = $rs->Fields('department');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			$return .= "\t".'<option value="'.$v.'"'.$selected.'>'.$w."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		$return .="\t".'<option value="0">-</option>'."\n";
	}
    $return .= "</select>\n";
    
    if( $isTemplate === true ){
    	return $return;
    } 
	else{
		echo $return;
	} 
		
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_3($Table, $Select='', $Condition='', $attributes = "")
{
	global $isTemplate;
	$Langue = "_1";
    $return = "<select name=\"idactivity\"$attributes>\n";
    $Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idactivity, `activity$Langue` AS `activity` FROM `$Table` $Condition");
	if( $rs->RecordCount()>0 )
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('idactivity');
			$w = $rs->Fields('activity');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			$return .= "\t".'<option value="'.$v.'"'.$selected.'>'.$w."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		$return .= "\t".'<option value="0">-</option>'."\n";
	}
    $return .= "</select>\n";
    
    if($isTemplate === true) 
		return $return;
	else 
		echo $return;
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_4($Table,$Select='',$Condition='',$isTemplate=false)
{
	global $isTemplate;
	$Langue = "_1";
	$return = "<select name=\"$Table\">\n";
	if($Select==''){
		$return .= "<option value='' selected=\"selected\"></option>";
	}else{
		$return .= "<option value=''></option>";
	}
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select idtitle, title$Langue as title from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('title');
			$s = $rs->Fields('idtitle');
			$selected = null;
			if( $Select == $s ) {
				$selected = ' selected="selected"';
			}
			$return .= "\t".'<option value="'.$s.'"'.$selected.'>'.$v."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		$return .= "\t".'<option value="0">-</option>'."\n";
	}
	$return .= "</select>\n";
	
	if($isTemplate === true) 
		return $return;
	else 
		echo $return;
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_5( $selectedValue = 0, $enabled = true )
{
	global $isTemplate;
	
	$Langue = "_1";
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	$return = "
	<select id=\"idmanpower\" name=\"idmanpower\"$disabled>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idmanpower, qty FROM manpower ORDER BY idmanpower ASC");
	
	if($rs->RecordCount()>0){
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			if( $selectedValue == $rs->Fields( "idmanpower" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			$return .= "
			<option value=\"" . $rs->Fields( "idmanpower" ) . "\"$selected>" . $rs->Fields( "qty" ) . "</option>";
			
			$rs->MoveNext();
        }
	}

    $return .= "
	</select>";
    
    if($isTemplate === true) 
		return $return;
	else 
		echo $return;
	
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_6( $Table, $Select = "", $Condition = "" ){
	
	$Langue = "_1";
	
	$selected = null;
	
	if( $Select == "" )
		$selected = " selected=\"selected\"";
	
	echo "<select name=\"idsource\">\n";
	echo "<option value=\"\"$selected>Sélectionnez la provenance</option>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select idsource, source$Langue as source from $Table $Condition");
	
	if( $rs->RecordCount() > 0 ){
		
		for( $i = 0 ; $i < $rs->RecordCount() ; $i++ ){
			
			$v = $rs->Fields( "idsource" );
			$w =  $rs->Fields( "source" );
			$selected = null;
			
			if( $Select == $v )
				$selected = ' selected="selected"';
			
			echo "\t".'<option value="'.$v.'"'.$selected.'>'.$w."</option>\n";
			
			$rs->MoveNext();
			
        }
        
	}else{
		
		echo "\t".'<option value="0">-</option>'."\n";
		
	}
	
    echo "</select>\n";
    
}


/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_buyerfamily($Table,$Select='',$Condition='')
{

	$Langue = "_1";
	echo "<select name=\"idbuyer_family\">\n";
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select idbuyer_family, name from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('idbuyer_family');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			echo "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('name')."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		echo "\t".'<option value="0">-</option>'."\n";
	}
    echo "</select>\n";
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawDeliveryLift($Select='',$Condition='',$enabled = true, $isTemplate=false)
{
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	$return="<select name=\"iddelivery\"";
	$return.=$disabled;
	$return.=" id=\"iddelivery\" onchange=\"javascript:selectDeliveryAddress( this.options[ this.selectedIndex ].value );\">";
	$return.="<option value=\"0\">-</option>";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT `iddelivery`, `lastname` FROM `delivery` $Condition");
	
	if($rs->RecordCount()>0) {
		for($i=0 ; $i < $rs->RecordCount() ; $i++) {
			$value = $rs->Fields('iddelivery');
			$display = $rs->Fields('lastname');
			if( $Select == $value ) {
				$selected = ' selected="selected"';
			}
			else {
				$selected = '';
			}
			$return.="<option value=\"".$value."\"".$selected.">".$display."</option>";
			$rs->MoveNext();
		}
	}
	$return.="</select>";
	
	if($isTemplate)
		return $return;
	else
		echo $return;
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawBillingLift($Select='',$Condition='',$enabled = true, $isTemplate=false)
{
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	
	$return="<select name=\"idbilling\" ".$disabled." id=\"idbilling\" onchange=\"javascript:selectBillingAddress( this.options[ this.selectedIndex ].value );\">";
	$return.="<option value=\"0\">-</option>";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT `idbilling_adress`, `lastname` FROM `billing_adress` $Condition");
	
	if($rs->RecordCount()>0) {
		for($i=0 ; $i < $rs->RecordCount() ; $i++) {
			$value = $rs->Fields('idbilling_adress');
			$display = $rs->Fields('lastname');
			if( $Select == $value ) {
				$selected = ' selected="selected"';
			}
			else {
				$selected = '';
			}
			$return.="<option value=\"".$value."\"".$selected.">".$display."</option>";
			$rs->MoveNext();
		}
	}
	$return.="</select>";
	
	if($isTemplate)
		return $return;
	else
		echo $return;
		
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawPaymentLift()
{

	echo "<select name=\"Payment\">\n";
	$Base = &DBUtil::getConnection();
	$Langue = "_1";
    $rs =& DBUtil::query("select idpayment, name$Langue as name from payment");
    if($rs->RecordCount()>0)
    {
	$Langue = "_1";
            for($i=0 ; $i < $rs->RecordCount() ; $i++)
            {
                    echo "<option value='" . $rs->Fields('idpayment') . "'>" . $rs->Fields('name') . "</option>";
                    $rs->MoveNext();
            }
    }
    echo "</select>\n";
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function UnderCat($IdCategory,$CurrentPageNumberP=1,$CurrentPageNumberC=1, $IdMask=0)
{
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_Delay( $name, $selectedValue = 0, $enabled = true, $javascript )
{
	
	$lang = "_1";
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	echo "
	<select name=\"$name\" id=\"$name\"$disabled $javascript>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT iddelay, delay$lang FROM delay ORDER BY iddelay ASC");
	
	if($rs->RecordCount()>0){
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			if( $selectedValue == $rs->Fields( "iddelay" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "iddelay" ) . "\"$selected>" . $rs->Fields( "delay$lang" ) . "</option>";
			
			$rs->MoveNext();
        }
	}

    echo "
	</select>";
	
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawSupplierList( $selectedValue = 0, $enabled = true , $SQL_Condition = "" )
{
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	echo "
	<select name=\"idsupplier\" id=\"idsupplier\"$disabled>
		<option selected value=\"0\">Tous</option>";
		
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query( "SELECT idsupplier, name FROM supplier WHERE 1$SQL_Condition ORDER BY name ASC" );
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			$selected = $rs->fields( "idsupplier" ) == $selectedValue ? " selected=\"selected\"" : "";
			
			echo "<option{$selected} value=\"" . $rs->Fields( "idsupplier" ) . "\">" . $rs->Fields( "name" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawVATList( $selectedValue = 0, $enabled = true )
{
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	echo "
	<select name=\"idvat\" id=\"idvat\"$disabled>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idtva, name, name FROM tva ORDER BY idtva ASC");
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			if( $selectedValue == $rs->Fields( "idtva" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "idtva" ) . "\"$selected>" . $rs->Fields( "name" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawPaymentDelayList( $selectedValue = 0, $enabled = true , $special = false){
	
	
	$lang = "_1";
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	$specialchar=$special ? " onchange='document.getElementById(\"UpdateIdPayment\").value=1;'" : "";
	echo "
	<select name=\"idpayment_delay\" $disabled $specialchar>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idpayment_delay, payment_delay$lang FROM payment_delay ORDER BY idpayment_delay ASC");
	
	if($rs->RecordCount()>0){
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			if( $selectedValue == $rs->Fields( "idpayment_delay" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "idpayment_delay" ) . "\"$selected>" . $rs->Fields( "payment_delay$lang" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_buyercond($Table,$Select='',$Condition='')
{

	$Langue = "_1";
	echo "<select name=\"cond_payment\">\n";
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select idpayment, name_1 from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('idpayment');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			echo "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('name_1')."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		echo "\t".'<option value="0">-</option>'."\n";
	}
    echo "</select>\n";
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_buyerdelay($Table,$Select='',$Condition='')
{

	$Langue = "_1";
	echo "<select name=\"deliv_payment\" >\n";
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select idpayment_delay, payment_delay_1 from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('idpayment_delay');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			echo "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('payment_delay_1')."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		echo "\t".'<option value="0">-</option>'."\n";
	}
    echo "</select>\n";
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_modepayment( $selectedValue = 0, $enabled = true , $special = false){
	
	$lang = "_1";
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	$specialchar=$special ? " onchange='document.getElementById(\"UpdateIdPayment\").value=1;'" : "";
	echo "
	<select name=\"idpayment\" $disabled $specialchar>\n";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idpayment, name_1 FROM payment ORDER BY display ASC");
	
	if($rs->RecordCount()>0){
		
		for($i=0 ; $i < $rs->RecordCount() ; $i++){
			
			if( $selectedValue == $rs->Fields( "idpayment" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "idpayment" ) . "\"$selected>" . $rs->Fields( "name_1" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_commercial($Table,$Select='',$Condition='ORDER BY lastname ASC', $disabled = false, $attributes = "" )
{
	$Langue = "_1";
	
	if( strlen( $attributes ) )
		$attributes = " " . $attributes;
		
	$editable = $disabled ? " disabled=\"disabled\"" : "";

	echo "<select name=\"iduser\" class=\"fullWidth\"{$editable}{$attributes}>
		<option value=\"0\">Tous</option>";

	$Base = &DBUtil::getConnection();		
	$rs =& DBUtil::query("select iduser, firstname, lastname from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('iduser');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			echo "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('firstname')." ".$rs->Fields('lastname')."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		echo "\t".'<option value="0">-</option>'."\n";
	}
    echo "</select>\n";
}

/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function DrawLift_commercial_business($Table,$Select='',$Condition='', $disabled = false )
{

	$Langue = "_1";
	
	$editable = $disabled ? " disabled=\"disabled\"" : "";

	echo "<select name=\"iduser_business\" class=\"fullWidth\"$editable>
		<option value=\"0\">Tous</option>";

	$Base = &DBUtil::getConnection();		
	$rs =& DBUtil::query("select iduser, firstname, lastname from $Table $Condition");
	if($rs->RecordCount()>0)
	{
		for($i=0 ; $i < $rs->RecordCount() ; $i++)
		{
			$v = $rs->Fields('iduser');
			$selected = null;
			if( $Select == $v ) {
				$selected = ' selected="selected"';
			}
			echo "\t".'<option value="'.$v.'"'.$selected.'>'.$rs->Fields('firstname')." ".$rs->Fields('lastname')."</option>\n";
			$rs->MoveNext();
        }
	}
	else
	{
		echo "\t".'<option value="0">-</option>'."\n";
	}
    echo "</select>\n";
}
//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_unit( $selectedValue = 0, $enabled = true )
{
	
	$lang = "_1";
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	echo "
	<select name=\"unit\" id=\"unit\" $disabled>";
		
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT unit$lang AS unit FROM unit ORDER BY unit$lang ASC");
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			if( $selectedValue == $rs->Fields( "unit" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "unit" ) . "\"$selected>" . $rs->Fields( "unit" ) . "</option>";
			
			$rs->MoveNext();
        }
        
	}

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_order_supplier_status( $selectedValue = 0, $allowNullValue = true, $enabled = true )
{
	
	$lang = "_1";
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	echo "
	<select name=\"status\" id=\"orderSupplierStatus\" $disabled>";
	
	if( $allowNullValue )
		echo "
		<option value=\"\">-</option>";
		
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT order_supplier_status AS status, order_supplier_status$lang FROM order_supplier_status ORDER BY display_order ASC");
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			if( $selectedValue == $rs->Fields( "status" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "status" ) . "\"$selected>" . $rs->Fields( "order_supplier_status$lang" ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_factor_status_nlo( $selectedValue = 0, $allowNullValue = true, $enabled = true )
{
	
	$lang = "_1";
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	echo "
	<select name=\"factor_status\" class=\"fullWidth\" $disabled>";
	
	if( $allowNullValue )
		echo "
		<option value=\"\">-</option>";
		
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT factor_status AS status, factor_status$lang FROM factor_status ORDER BY factor_status ASC");
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			if( $selectedValue == $rs->Fields( "status" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "status" ) . "\"$selected>" . ucfirst( $rs->Fields( "factor_status$lang" ) ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_source( $selectedValue = 0, $enabled = true )
{
	
	$lang = "_1";
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	
	echo "
	<select name=\"idsource\" $disabled>
		<option value=\"\">Toutes</option>";
	
	$db = &DBUtil::getConnection();
	$rs = $db->Execute( "SELECT source_code, source_1 FROM source ORDER BY source$lang ASC" );
	
	while( !$rs->EOF() ){
		
		if( $selectedValue == $rs->fields( "source_code" ) )
			$selected = " selected=\"selected\"";
		else $selected = "";
		
		echo "
		<option value=\"" . $rs->fields( "source_code" ) . "\"$selected>" . $rs->fields( "source$lang" ) . "</option>";
		
		$rs->MoveNext();
		
    }

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_idsource( $selectedValue = 0, $enabled = true )
{
	
	$lang = "_1";
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	
	echo "
	<select name=\"idsource\" class=\"fullWidth\" $disabled>
		<option value=\"\">Toutes</option>";
	
	$db = &DBUtil::getConnection();
	$rs = $db->Execute( "SELECT idsource, source_code, source_1 FROM source ORDER BY source$lang ASC" );
	
	while( !$rs->EOF() ){
		
		if( $selectedValue == $rs->fields( "idsource" ) )
			$selected = " selected=\"selected\"";
		else $selected = "";
		
		echo "
		<option value=\"" . $rs->fields( "idsource" ) . "\"$selected>" . $rs->fields( "source$lang" ) . "</option>";
		
		$rs->MoveNext();
		
    }

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_state( $selectedValue = 0, $enabled = true )
{
	
	$lang = "_1";
	
	$disabled = $enabled ? "" : " disabled=\"disabled\"";
	
	echo "
	<select name=\"idstate\" $disabled>
		<option value=\"0\">-</option>";
	
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("SELECT idstate, name$lang AS name FROM state ORDER BY name$lang ASC");
	
	if($rs->RecordCount()>0){
		
		while( !$rs->EOF() ){
			
			if( $selectedValue == $rs->Fields( "idstate" ) )
				$selected = " selected=\"selected\"";
			else $selected = "";
			
			echo "
			<option value=\"" . $rs->Fields( "idstate" ) . "\"$selected>" . $rs->Fields( "name" ) . "</option>";
			
			$rs->MoveNext();
			
        }
        
	}

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------
/**
 * @deprecated préférer XHTMLFactory::createSelectElement()
 */
function Drawlift_factor_status($selectedValue='')
{
	
	echo "
	<select name=\"factor_status\" id=\"factor_status\" onchange=\"javascript:show_factor_infos()\">";
		
		if( $selectedValue == 'InProgress' OR empty($selectedValue)){
			echo "<option value=\"InProgress\" selected=\"selected\">En cours</option>";
		}else{
			echo "<option value=\"InProgress\">En cours</option>";
		}
        
        if( $selectedValue == 'Accepted' ){
			echo "<option value=\"Accepted\" selected=\"selected\">Accepté</option>";
		}else{
			echo "<option value=\"Accepted\">Accepté</option>";
		}
		
		if( $selectedValue == 'Refused' ){
			echo "<option value=\"Refused\" selected=\"selected\" style=\"color:#FF0000;\">Refusé</option>";
		}else{
			echo "<option value=\"Refused\" style=\"color:#FF0000;\">Refusé</option>";
		}

    echo "
	</select>";
	
}

//-------------------------------------------------------------------------------------

?>