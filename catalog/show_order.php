<?php 

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage commande
 */
 
include_once('../objects/classes.php');
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/Voucher.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");
include_once( dirname( __FILE__ ) . "/../lib/buyer.php" );

$lang = "_1";

class IndexController extends PageController{};
$show_order = new IndexController( "show_order.tpl" );

// on test si un nouvelle adresse à été postée
if( isset( $_REQUEST['idorder'] ) && ( isset( $_POST[ 'update_billing_adress' ] ) || isset( $_POST[ 'update_delivery_adress' ] ) || isset( $_POST[ 'update_billing_adress_x' ] ) || isset( $_POST[ 'update_delivery_adress_x' ] ) ) ){	

	// reste à enregister la nouvelle 
	$idorder 	 			 = $_REQUEST['idorder'];
	$adress[ "Company" ] 	 = $_POST[ 'company' ];
	$adress[ "Firstname" ] 	 = $_POST[ 'firstname' ];
	$adress[ "Lastname" ]	 = $_POST[ 'lastname' ];
	$adress[ "Title" ]	 	 = $_POST[ 'title' ];
	$adress[ "Phonenumber" ] = $_POST[ 'phonenumber' ];
	$adress[ "Adress" ]	 	 = $_POST[ 'adress' ];
	$adress[ "Adress_2" ]	 = $_POST[ 'adress_2' ];
	$adress[ "Zipcode" ]	 = $_POST[ 'zipcode' ];
	$adress[ "City" ]		 = $_POST[ 'city' ];
	$adress[ "idstate" ]	 = $_POST[ 'idstate' ];
	$adress[ "Faxnumber" ]	 = "";
	
	$Order = new Order($idorder);
	
	if( isset( $_POST[ 'update_billing_adress' ] ) || isset( $_POST[ 'update_billing_adress_x' ] ) ){
		$idbilling = buyer_AddBillingAddress( $adress );
		$Order->set( "idbilling_adress" , $idbilling );
	}else{
		$iddelivery = buyer_AddDeliveryAddress( $adress );
		$Order->set( "iddelivery" , $iddelivery ); 
	}
	
	// et mettre à jour la commande
	$Order->save();
	
}


/**************************************************
 *********************ACCOUNT**********************
 **************************************************/
 
if( Session::getInstance()->getCustomer() ) {
	$show_order->setData('isLogin', "ok");
	
	$GroupingCatalog = new GroupingCatalog();
	
	$show_order->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$show_order->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$show_order->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$show_order->setData( "contact_lastname", $salesman->get("lastname") );
	$show_order->setData( "contact_firstname", $salesman->get("firstname") );
	$show_order->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$show_order->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$show_order->setData( "contact_email", $salesman->get("email") );
	
	$show_order->setData( "isOrder", true );
	
		$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
		$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
		$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$show_order->setData( "contact_grouping", $contact_grouping );
} else {
	$show_order->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$show_order->setData('com_step_empty', "ok");
	} else {
		$show_order->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$show_order->setData('issetPostLoginAndNotLogged', "ok");
		$show_order->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$show_order->setData('issetPostLoginAndNotLogged', "");
	}
	$show_order->setData('scriptName', $ScriptName);
	$show_order->setData('translateLogin',Dictionnary::translate('login'));
	$show_order->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$show_order->setData('img', "acceder_compte.png");
		$show_order->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$show_order->setData('img', "poursuivre.png");
		$show_order->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

/**************************************************
 ****************SHOW_ORDER.TPL********************
 **************************************************/
 
if( isset( $_REQUEST['idorder']) )
	$idorder = $_REQUEST['idorder'];
else if( !isset( $idorder ) ){
		header("location: $GLOBAL_START_URL");
		exit();
}

$Order = new Order($idorder);

$show_order->setData('idorder',$idorder);

//----------------------------------------------------------------------------------------------
/* proxy commande */

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/OrderProxy.php" );
$show_order->setData( "order", new OrderProxy( $Order ) );
	
$order_date=html_entity_decode(humanReadableDatetime($Order->get('DateHeure')));
$show_order->setData('order_date',$order_date);

if( !Session::getInstance()->getCustomer() || Session::getInstance()->getCustomer()->getId() != $Order->getCustomer()->getId() ){


$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
		$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
		$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
if($contact_grouping!=1){

	header( "Location:$GLOBAL_START_URL" );
	exit();
}
		
}

//recherche de la commande 

$Str_Status = $Order->get('status');
switch ($Str_Status){
	case 'ToDo':
		{
			$show_order->setData('Str_Status_ToDo','ok');
			$show_order->setData('Str_Status_InProgress','');
			$show_order->setData('Str_Status_Send','');
			$show_order->setData('Str_Status_Ordered','');
			$show_order->setData('Str_Status_Invoiced','');
			$show_order->setData('Str_Status_Paid','');
			break;
		}
	case 'InProgress':
		{
			$show_order->setData('Str_Status_ToDo','');
			$show_order->setData('Str_Status_InProgress','ok');
			$show_order->setData('Str_Status_Send','');
			$show_order->setData('Str_Status_Ordered','');
			$show_order->setData('Str_Status_Invoiced','');
			$show_order->setData('Str_Status_Paid','');
			break;
		}
	case 'Send':
		{
			$show_order->setData('Str_Status_ToDo','');
			$show_order->setData('Str_Status_InProgress','');
			$show_order->setData('Str_Status_Send','ok');
			$show_order->setData('Str_Status_Ordered','');
			$show_order->setData('Str_Status_Invoiced','');
			$show_order->setData('Str_Status_Paid','');
			break;
		}
	case 'Ordered':
		{
			$show_order->setData('Str_Status_ToDo','');
			$show_order->setData('Str_Status_InProgress','');
			$show_order->setData('Str_Status_Send','');
			$show_order->setData('Str_Status_Ordered','ok');
			$show_order->setData('Str_Status_Invoiced','');
			$show_order->setData('Str_Status_Paid','');
			break;
		}
	case 'Invoiced':
		{
			$show_order->setData('Str_Status_ToDo','');
			$show_order->setData('Str_Status_InProgress','');
			$show_order->setData('Str_Status_Send','');
			$show_order->setData('Str_Status_Ordered','');
			$show_order->setData('Str_Status_Invoiced','ok');
			$show_order->setData('Str_Status_Paid','');
			break;
		}
	case 'Paid':
		{
			$show_order->setData('Str_Status_ToDo','');
			$show_order->setData('Str_Status_InProgress','');
			$show_order->setData('Str_Status_Send','');
			$show_order->setData('Str_Status_Ordered','');
			$show_order->setData('Str_Status_Invoiced','');
			$show_order->setData('Str_Status_Paid','ok');
			break;
		}
	default :
		{
			$show_order->setData('Str_Status_ToDo','');
			$show_order->setData('Str_Status_InProgress','');
			$show_order->setData('Str_Status_Send','');
			$show_order->setData('Str_Status_Ordered','');
			$show_order->setData('Str_Status_Invoiced','');
			$show_order->setData('Str_Status_Paid','');
			break;
		}
}
	
//----------------------------------------------------------------------------

$hasLot = false;
$count = $Order->getItemCount();
$i = 0;
while( $i < $count && !$hasLot ){
	
	if( $Order->getItemAt( $i)->get( "lot" ) > 1 )
		$hasLot = true;
		
	$i++;
	
}
if($hasLot){
	$show_order->setData('hasLot','ok');
}else{
	$show_order->setData('hasLot','');
}

$n = $Order->getItemCount();
$order_articles=array();
for ( $i=0 ; $i<$n ; $i++ ) {
	
	$idarticle = $Order->getItemAt($i)->get("idarticle");
	$stock_level = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $idarticle );
	$quantity = $Order->getItemAt( $i)->get( "quantity" );
	
	$order_articles[$i]["i"] = $i;
	$order_articles[$i]["Icon"] = URLFactory::getReferenceImageURI( $Order->getItems()->get( $i )->get( "idarticle" ) );;
	$order_articles[$i]["idarticle"] = $idarticle;
	$order_articles[$i]["reference"] = $Order->getItemAt( $i)->get( "reference" );
	$order_articles[$i]["summary"] = Util::checkEncoding(Util::doNothing($Order->getItemAt($i)->get('summary')));
	$order_articles[$i]["stock_level"] = $stock_level;
	$order_articles[$i]["quantity"] = $quantity;
	$order_articles[$i]["designation"] = $order_articles[$i]["productname"] = $Order->getItemAt( $i)->get( "designation" );
	$order_articles[$i]["delay"] = Util::getDelay($Order->getItemAt( $i)->get( "delivdelay" ));
	$order_articles[$i]["delivdelay"] = Util::doNothing(DBUtil::getDBValue( "delay_1", "delay", "iddelay", $Order->getItemAt($i)->get('delivdelay') ));
	$order_articles[$i]["discount_price"] = Util::priceFormat( $Order->getItemAt( $i)->get( "discount_price" ) );
	$order_articles[$i]["lot"] = $Order->getItemAt( $i)->get( "lot" );
	$order_articles[$i]["unit"] = $Order->getItemAt( $i)->get( "unit" );
	$order_articles[$i]["total_price"] = Util::priceFormat($Order->getItemAt( $i )->getTotalET());
	$order_articles[$i]["unit_price"] = Util::priceFormat($Order->getItemAt( $i)->get( "unit_price" ));
	$order_articles[$i]["ref_discount"] = ($Order->getItemAt($i)->get('ref_discount') > 0) ? Util::rateFormat($Order->getItemAt($i)->get('ref_discount')) : "-" ;
	$order_articles[$i]["discount_price"] = Util::priceFormat($Order->getItemAt($i)->get( "discount_price" )); 

	if( $idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $Order->getItemAt( $i)->get( "idarticle" ) ) )
			$order_articles[$i]["productURL"] = URLFactory::getProductURL( $idproduct );
	else 	$order_articles[$i]["productURL"] = "";
	
	$rk="SELECT iv.intitule_value_1 as value FROM intitule_value iv,intitule_link il WHERE il.idintitule_value=iv.idintitule_value AND il.idarticle=$idarticle LIMIT 1";
	$rc=DBUtil::query($rk);

	$valeurTaille=$rc->fields("value");
	$order_articles[$i]["taille"] = $valeurTaille;
	
	$order_articles[$i]["total_priceTTC"] = Util::priceFormat($Order->getItemAt( $i )->getTotalATI() );
	
	if($Order->getItemAt( $i)->get( "discount_price" ) != $Order->getItemAt( $i)->get( "unit_price" )){
		$HasDiscount='ok';
	}
	
	$order_articles[$i]["RefTotalAmountHT"] = Util::priceFormat(round( $Order->getItemAt( $i)->get( "discount_price" ) * $Order->getItemAt( $i)->get( "quantity" ), 2 ) );
	
}
$show_order->setData('order_articles',$order_articles);

$show_order->setData('PaymentDelay', Util::doNothing(DBUtil::getDBValue( "payment_delay_1", "payment_delay", "idpayment_delay",$Order->get( "idpayment_delay" ) )));
$show_order->setData('PayMod',Util::doNothing(DBUtil::getDBValue("name_1", "payment", "idpayment", $Order->get( "idpayment" ) )));

$show_order->setData('estimate', false );

//----------------------------------------------------------------------------
/* bon de réduction */

$rs =& DBUtil::query( "SELECT idvoucher FROM vouchers_order WHERE idorder = '" . $Order->getId() . "' LIMIT 1" );
$voucher = $rs->RecordCount() ? new Voucher( $rs->fields( "idvoucher" ) ) : null;

if( $voucher )
	$show_order->setData( "voucher", ( object )array(
	
		"code" 				=> $voucher->get( "code" ),
		"creation_date" 	=> Util::dateFormat( $voucher->get( "creation_date" ), "d/m/Y" ),
		"expiration_date" 	=> Util::dateFormat( $voucher->get( "expiration_date" ), "d/m/Y" ),
		"rate" 				=> Util::rateFormat( $voucher->get( "rate" ) ),
		"min_order_amount" 	=> Util::priceFormat( $voucher->get( "min_order_amount" ) )
	
	));
else $show_order->setData( "voucher", false );

$show_order->setData( "voucher_code", isset( $_REQUEST[ "voucher_code" ] ) ? strip_tags( stripslashes( $_REQUEST[ "voucher_code" ] ) ) : false );

//----------------------------------------------------------------------------
/* chèque cadeau */

$gift_token = $Order->getGiftToken();

$show_order->setData( "gift_token_serial", 			$gift_token == false ? "" : $gift_token->get( "serial" ) );
$show_order->setData( "gift_token_error", 			""  );
$show_order->setData( "display_gift_token_serial", 	false );

$gift_token_amount = $gift_token == false ? 0.0 : DBUtil::getDBValue( "value", "gift_token_model", "idmodel", $gift_token->get( "idtoken_model" ) );
$show_order->setData( "gift_token_amount", $gift_token_amount );

//----------------------------------------------------------------------------
/* montants */

$articlesET = 0.0;
$it = $Order->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();
	
$show_order->setData( "remise_rate", 					Util::rateFormat( $Order->get( "total_discount" ) ) );
$show_order->setData( "total_discount", 				Util::rateFormat( $Order->get( "total_discount" ) ) );
$show_order->setData( "discount", 						Util::priceFormat( $Order->get( "total_discount_amount" ) ) );
$show_order->setData( "total_discount_amount",			Util::priceFormat( $Order->get( "total_discount_amount" ) ) );
$show_order->setData( "total_amount_ht", 				Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) );
$show_order->setData( "total_amount_ht_wb", 			Util::priceFormat( $Order->getTotalET() ) );
$show_order->setData( "total_amount_ttc_wbi", 			Util::priceFormat( $Order->getNetBill() ) );
$show_order->setData( "total_amount_ht_avant_remise", 	Util::priceFormat( $articlesET ) );
$show_order->setData( "remise", 						$Order->get( "total_discount" ) > 0.0 );
$show_order->setData( "net_to_pay", 					Util::numberFormat( max( $Order->getNetBill() - $gift_token_amount, 0.0 ) ) );

$basetva = DBUtil::getDBValue('total_amount_ht', 'order', 'idorder', $Order->getId());
$ttcbase = DBUtil::getDBValue('total_amount', "order", 'idorder', $Order->getId() );
$totvat = $ttcbase-$basetva;
$vat = $Order->getVATRate();

$show_order->setData('getVATRate', Util::rateFormat($vat));
$show_order->setData('getVATAmount', Util::priceFormat($totvat));

//réduction depuis panier abandonné
$reduction_val = DBUtil::getDBValue('reduction', 'order', 'idorder', $Order->getId());
if($reduction_val && $reduction_val != '0.00')
{
	$reduction_type = DBUtil::getDBValue('reduction_type', 'cart', 'id_cart', $_SESSION[ "id_cart" ]);
	if ($reduction_type == 0)
		$reduction = $reduction_val;//Réduction calculée par montant
	elseif ($reduction_type == 1)
		$reduction = $articlesET * $reduction_val / 100;//Réduction calculée par pourcentage
	
	$show_order->setData( "total_amount_ht_wb",	Util::priceFormat( $Order->getTotalET() - $reduction ) );
	$show_order->setData( "total_amount_ttc_wbi", Util::priceFormat( $Order->get("amount_reduce") ) );
	$show_order->setData( "reduction", Util::numberFormat( $reduction ));
}

include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
$allow_debit_card_payment = false;
//if( in_array( $Order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD, Payment::$PAYMENT_MOD_FIANET_CB ) )
if( in_array( $Order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD ) )
	&& $Order->getBalanceOutstanding() > 0.0 
	&& $Order->get( "status" ) != Order::$STATUS_CANCELLED
	&& !$Order->get( "paid" ) )
		$allow_debit_card_payment = true;

$show_order->setData('allow_debit_card_payment', $allow_debit_card_payment);

/* frais de port */

if( $articlesET == 0.0 ){ //ne pas afficher les frais de port si prix cachés

	$show_order->setData( "total_charge_ht", "-" );
	$show_order->setData( "total_charge_ttc", "-" );
	
}
else{
	
	$chargesET 	= $Order->getChargesET();
	$chargesATI = $Order->getChargesATI();
	
	$show_order->setData( "total_charge_ht", $chargesET > 0.0 ? Util::priceFormat( $chargesET ) : "FRANCO" ); 
	$show_order->setData( "total_charge_ttc", $chargesATI > 0.0 ? Util::priceFormat( $chargesATI ) : "FRANCO" ); 
	
	$show_order->setData( "franco", $chargesET == 0.0 );
	
}
	
//----------------------------------------------------------------------------

//buyer_read.tpl

if( $ScriptName != "order_secure.php"){
	$show_order->setData('order_secure',0);
}else{
	$show_order->setData('order_secure',1);
}




$nb_lignes = 10;

$articles = array ();
	if (isset ($_POST['validated'])) {
//IdArticles[]


	for ($i = 0; $i < $nb_lignes; $i++) {
	//print $_POST['ref_'.$i];exit;
	//$nbr =  $_POST['check_'.$i];
	//print $nbr;
	
		$articles[$i]["reference"] = $_POST['ref_'.$i];
		$articles[$i]["quantity"] = $_POST['quantity_'.$i];
	}

	$notrefs = array (); //numéros qui ne sont pas des références
	$refwoprice = array (); //références dont le prix est caché

	$n = 0;

	for ($i = 0; $i < 10; $i++) {

		//si on a une quantite d'indiqué et une référence d'indiqué
		if ($articles[$i]['quantity'] > 0 && trim($articles[$i]['reference']) != '') {

			//vérifier si le prix est dispo (hidecost == 0 )
			$query = "SELECT hidecost FROM detail WHERE reference LIKE '" . $articles[$i]['reference'] . "' LIMIT 1";

			$base = & DBUtil::getConnection();
			$rs = $base->Execute($query);

			if ($rs->RecordCount()) {
				$hidecost = $rs->fields("hidecost");
				if ($hidecost != 0) {
					array_push($list_refs_hide_prices, $articles[$i]['reference']);
					$hasrefwoprice = true;
					
				}
			}
			else{
				$hidecost = 0;				
			}

			//si on n'a pas de prix caché
			if (!$hidecost > 0 && ( ($_POST['check_'.$i] == $i)) ) {
			
			//print $articles[$i]['reference'];
				// enregistrement de la commande, les références non trouvées vont dans $notrefs
				//print VerifRef();exit;
			//	$qte = $articles[$i]['quantity'];
			//	$ref = $articles[$i]['reference'];
				$n += VerifRef($articles[$i]['quantity'], $articles[$i]['reference']);

				if ($n == 0) { //si on a pas réussi à ajouter au panier
					array_push($list_articles_not_found, $articles[$i]);
					$refs_not_found = true;

				} else { //on a réussit à l'ajouter
					$articles[$i]["reference"] = "";
					$articles[$i]["quantity"] = "";
				}

			}
		}}}



$show_order->setData('buyer_company', Util::checkEncoding($Order->getCustomer()->getAddress()->getCompany()) );
$show_order->setData('buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getAddress()->getGender() ) );
$show_order->setData('buyer_firstname', Util::checkEncoding($Order->getCustomer()->getAddress()->getFirstName()) );
$show_order->setData('buyer_lastname', Util::checkEncoding($Order->getCustomer()->getAddress()->getLastName()) );
$show_order->setData('buyer_adress', Util::checkEncoding($Order->getCustomer()->getAddress()->getAddress()) );
$show_order->setData('buyer_adress_2', Util::checkEncoding($Order->getCustomer()->getAddress()->getAddress2()) );
$show_order->setData('buyer_zipcode', $Order->getCustomer()->getAddress()->getZipcode() );
$show_order->setData('buyer_city', Util::checkEncoding($Order->getCustomer()->getAddress()->getCity()) );

$show_order->setData('b_buyer_company', Util::checkEncoding($Order->getInvoiceAddress()->getCompany()));
$show_order->setData('b_buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getInvoiceAddress()->getGender() ) );
$show_order->setData('b_buyer_firstname', Util::checkEncoding($Order->getInvoiceAddress()->getFirstName()) );
$show_order->setData('b_buyer_lastname', Util::checkEncoding($Order->getInvoiceAddress()->getLastName()) );
$show_order->setData('b_buyer_adress', Util::checkEncoding($Order->getInvoiceAddress()->getAddress()) );
$show_order->setData('b_buyer_adress_2', Util::checkEncoding($Order->getInvoiceAddress()->getAddress2()) );
$show_order->setData('b_buyer_zipcode', $Order->getInvoiceAddress()->getZipcode());
$show_order->setData('b_buyer_city', Util::checkEncoding($Order->getInvoiceAddress()->getCity()) );
$show_order->setData('b_buyer_phonenumber', $Order->getInvoiceAddress()->getPhonenumber() );
$show_order->setData('b_buyer_idstate', $Order->getInvoiceAddress()->getIdState() );

$show_order->setData('d_buyer_company', Util::checkEncoding($Order->getForwardingAddress()->getCompany()));
$show_order->setData('d_buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getForwardingAddress()->getGender() ) );
$show_order->setData('d_buyer_firstname', Util::checkEncoding($Order->getForwardingAddress()->getFirstName() ));
$show_order->setData('d_buyer_lastname', Util::checkEncoding($Order->getForwardingAddress()->getLastName() ));
$show_order->setData('d_buyer_adress', Util::checkEncoding($Order->getForwardingAddress()->getAddress() ));
$show_order->setData('d_buyer_adress_2', Util::checkEncoding($Order->getForwardingAddress()->getAddress2() ));
$show_order->setData('d_buyer_zipcode', $Order->getForwardingAddress()->getZipcode());
$show_order->setData('d_buyer_city', Util::checkEncoding($Order->getForwardingAddress()->getCity() ));
$show_order->setData('d_buyer_phonenumber', $Order->getForwardingAddress()->getPhonenumber() );
$show_order->setData('d_buyer_idstate', $Order->getForwardingAddress()->getIdState() );

							
//----------------------------------------------------------------------------------------------
$show_order->output();


function dobasket($IdRealProduct, $Qtt) {
	
	include_once( dirname( __FILE__ ) . "/../objects/catalog/CArticle.php" );
	Basket::getInstance()->addArticle( new CArticle( $IdRealProduct ), $Qtt);

}

function VerifRef($qte, $ref) {
//print "zeezez";exit;
	
	$Base = & DBUtil::getConnection();
	$Query = "
												SELECT idarticle,stock_level
												FROM product p,detail d
												WHERE d.reference = '" . trim($ref) . "' 
												AND p.available = 1 
												AND p.idproduct = d.idproduct";

	$rs = $Base->Execute($Query);
	$tcount = $rs->RecordCount();

	if ($tcount == 1) {
		$stock = $rs->Fields("stock_level");
		$IdRealProduct = $rs->Fields("idarticle");

		if (dobasket($IdRealProduct, $qte) == 0)
			return $qte;
	}

	return 0;
}



?>