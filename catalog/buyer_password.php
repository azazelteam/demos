<?php 
 /**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement mot de passe du client
 */

include_once( dirname( __FILE__ ) . '/../objects/classes.php' );  
include_once( dirname( __FILE__ ) . '/drawlift.php' );
require_once( 'HTML/Template/Flexy.php' );
require_once( 'HTML/Template/Flexy/Element.php' );
require_once( 'PEAR.php' );
include_once( dirname( __FILE__ ) . '/../objects/flexy/PageController.php' );
include_once( dirname( __FILE__ ) . '/../objects/GroupingCatalog.php' );

$title = 'Changement de mot de passe';

class BuyerPasswordController extends PageController{};
$buyer_password = new BuyerPasswordController( "buyer_password.tpl" , $title);

if(isset($IdCategory) AND $IdCategory>0){		
		
	$query = "SELECT name_1 FROM category WHERE idcategory='$IdCategory'";
	$rs =& DBUtil::query( $query );
	
	$cat_name = $rs->fields("name_1");				
	$first = $cat_name{0};			
	$voyelles = "aeiouyAEIOUY";				
	$isvoy = strpos($voyelles,$first);
	
	if($isvoy===false){
		$appart_catalog = "Le catalogue de ".$cat_name;
		$buyer_password->setData('appart_catalog',$appart_catalog);
	}else{
		$appart_catalog = "Le catalogue d'".$cat_name;
		$buyer_password->setData('appart_catalog',$appart_catalog);
	}
}else{
		$buyer_password->setData('appart_catalog',"");
}


$msg='';
//Changement de mot de passe
if( isset($_POST['Modify']) or isset($_POST['Modify_x'])){
	
	//vérifier les données
	
	include_once( "Validate.php" ); //classes PEAR
	include_once( 'Validate/FR.php' ); //classe PEAR	
	
	/*$old_pass = Validate::string( $_POST[ "old_pwd" ], $options = array( "format" => VALIDATE_NUM . VALIDATE_ALPHA, "min_length" => 6 ) );
	$new_pass = Validate::string( $_POST[ "pwd" ], $options = array( "format" => VALIDATE_NUM . VALIDATE_ALPHA, "min_length" => 6 ) );
	$conf_new_pass = Validate::string( $_POST[ "pwd_confirm" ], $options = array( "format" => VALIDATE_NUM . VALIDATE_ALPHA, "min_length" => 6 ) );*/
	//var_dump(strlen($_POST['old_pwd']));exit();
	$long_old = strlen($_POST['old_pwd']);
	$long_pwd = strlen($_POST['pwd']);
	$long_confirm = strlen($_POST['pwd_confirm']);
	
	
	/*if( empty( $_POST[ "old_pwd" ] ) || !Validate::string( $_POST[ "old_pwd" ], $options = array( "format" => VALIDATE_NUM . VALIDATE_ALPHA, "min_length" => 6 ) ) )
		$msg = Dictionnary::translate( "pear_validator_password_error" );
	else if( empty( $_POST[ "pwd" ] ) || !Validate::string( $_POST[ "pwd" ], $options = array( "format" => VALIDATE_NUM . VALIDATE_ALPHA, "min_length" => 6 ) ) )
		$msg = Dictionnary::translate( "pear_validator_password_error" );
	else if( empty( $_POST[ "pwd_confirm" ] ) || !Validate::string( $_POST[ "pwd_confirm" ], $options = array( "format" => VALIDATE_NUM . VALIDATE_ALPHA, "min_length" => 6 ) ) )
		$msg = Dictionnary::translate( "pear_validator_password_error" );
	else if( $_POST[ "pwd" ] != $_POST[ "pwd_confirm" ] )
		$msg = Dictionnary::translate( "register_password_confirmation_error" );*/
		/* == Modification Onja */
		if( empty( $_POST[ "old_pwd" ] ) || $long_old < 6  )
		$msg = Dictionnary::translate( "pear_validator_password_error" );
	else if( empty( $_POST[ "pwd" ] ) || $long_pwd  < 6 )
		$msg = Dictionnary::translate( "pear_validator_password_error" );
	else if( empty( $_POST[ "pwd_confirm" ] ) || $long_confirm  < 6  )
		$msg = Dictionnary::translate( "pear_validator_password_error" );
	else if( $_POST[ "pwd" ] != $_POST[ "pwd_confirm" ] )
		$msg = Dictionnary::translate( "register_password_confirmation_error" );
	//Fin modification Onja
	$validatePostData = empty( $msg );
	
	if( $validatePostData ){
	
		$old_pwd 	= $_POST[ "old_pwd" ];
		$pwd 		= $_POST['pwd'];
		
		$idbuyer 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$idcontact 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getId() : 0;
				
		//vérifier l'ancien mot de passe
		$query = "SELECT idbuyer,idcontact FROM user_account WHERE idbuyer='$idbuyer' AND idcontact='$idcontact' AND password LIKE '" . md5( $old_pwd ) . "' LIMIT 1";
		
		$rs =& DBUtil::query( $query );
		
		if( $rs === false )
			die( "Impossible de vérifier l'ancien mot de passe" );
			
		if( !$rs->RecordCount() ) //ancien mot de passe incorrect
			$msg = Dictionnary::translate( "invalid_old_password" );
		else{ //ancien mot de passe correct
		
			//mettre à jour le nouveau mot de passe
				
			$Query = "UPDATE user_account SET password='".md5($pwd)."' WHERE idbuyer='$idbuyer' AND idcontact='$idcontact' AND password LIKE '" . md5( $old_pwd ) . "' LIMIT 1";
			DBUtil::query( $Query );
			
			//-----------------------------------------------------------------------------------------
						
			//charger le modèle de mail
		
			include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
			
			$editor = new MailTemplateEditor();
			//var_dump(substr( "_1", 1 ));exit();
			$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
			
			$editor->setTemplate( "BUYER_NEW_PASSWORD" );
			
			$iduser = DBUtil::getParameter('contact_commercial');
			
			$editor->setUseAdminTags();
			$editor->setUseUtilTags();
			$editor->setUseBuyerTags( $idbuyer );
			$editor->setUseBuyerPassword( $pwd );
			$editor->setUseCommercialTags( $iduser );
			
			$Message = $editor->unTagBody();
			$Subject = $editor->unTagSubject();

			//-----------------------------------------------------------------------------------------
			
			//envoyer le mail
			
			include_once ("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
		
			$mailer = new htmlMimeMail();
			
			$AdminName = DBUtil::getParameterAdmin('ad_name');
			$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
			
			$mailer->setFrom( '"'.$AdminName.'" <'.$AdminEmail.'>' );
			$mailer->setSubject( $Subject );
			$mailer->setHtml( stripslashes( $Message ) );

			$req = "SELECT * FROM contact WHERE idbuyer='".$idbuyer."' AND idcontact='".$idcontact."'";
			$rsb = DBUtil::query( $req );
			$BuyerTitle = $rsb->fields( "title" );
			$BuyerName = $rsb->fields( "firstname" ).' '.$rsb->fields( "lastname" );
			$BuyerEmail = $rsb->fields( "mail" );
			
			$ret = $mailer->send( array( $BuyerEmail ) );
		
			if(!$ret)
				die("Problème d'envoi");
					
			$msg = "Votre mot de passe a bien été modifié. Un e-mail récapitulant vos identifiants et votre mot de passe vous a été envoyé.";
	
		}
		
	}
	
}

$buyer_password->setData('message', $msg);

/* ACCOUNT */

if( Session::getInstance()->getCustomer() ) {
	$buyer_password->setData('isLogin', "ok");

	$BuyerLogin = Session::getInstance()->getCustomer()->get( "login" );
	
	$buyer_password->setData('buyer', Session::getInstance()->getCustomer()->getId());
	$buyer_password->setData('buyerLogin', $BuyerLogin);
	
	$GroupingCatalog = new GroupingCatalog();
	$buyer_password->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$buyer_password->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$buyer_password->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$buyer_password->setData( "contact_lastname", $salesman->get("lastname") );
	$buyer_password->setData( "contact_firstname", $salesman->get("firstname") );
	$buyer_password->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$buyer_password->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$buyer_password->setData( "contact_email", $salesman->get("email") );
	
	$buyer_password->setData( "isPassword", true );
	
} else {
	$buyer_password->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$buyer_password->setData('com_step_empty', "ok");
	} else {
		$buyer_password->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$buyer_password->setData('issetPostLoginAndNotLogged', "ok");
		$buyer_password->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$buyer_password->setData('issetPostLoginAndNotLogged', "");
	}
	$buyer_password->setData('scriptName', $ScriptName);
	$buyer_password->setData('translateLogin',Dictionnary::translate('login'));
	$buyer_password->setData('translatePassword',Dictionnary::translate('password'));

	$ScriptName = '$buyer_password.php';
	if($ScriptName == '$buyer_password.php') {
		$buyer_password->setData('img', "acceder_compte.png");
		$buyer_password->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$buyer_password->setData('img', "poursuivre.png");
		$buyer_password->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

$buyer_password->output();

function HumanReadablePhone($tel){
	//On regarde la longeueur de la cheine pour parcourir
	$lg=strlen($tel);

	$telwcs='';
	for ($i=0;$i<$lg;$i++){
		//On extrait caractère par caractère
		$tocheck=substr($tel,$i,1);
	
		//On regarde si le premier caractère est un + ou un chiffre et on garde
		if($i==0){
			if(($tocheck=='+')OR(is_numeric($tocheck))){
				$telwcs.=$tocheck;
			}
		}else{
			//On ne garde que les chiffres
			if(is_numeric($tocheck)){
				$telwcs.=$tocheck;
			}
		}
	}

	//On regarde la longueur de la chaine propre
	$lg=strlen($telwcs);

	$telok ='';

	
	//On regarde si le premier caractère est un +
	//On extrait le premier caractère
	$tocheck=substr($telwcs,0,1);

	if($tocheck=='+'){
		//On sort les 3 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,3);
		$telok.=$tocheck." ";
		$cpt=3;
	}else{
		//On sort les 2 premiers caractères et on ajoute un espace
		$tocheck=substr($telwcs,0,2);
		$telok.=$tocheck." ";
		$cpt=2;
	}

	//On traite le reste de la chaine
	for($i=$cpt;$i<$lg;$i+=2){
		if($i==$cpt){
			$tocheck=substr($telwcs,$i,2);
		}else{
			$tocheck=substr($telwcs,$i,2);
		}
		$telok.=$tocheck." ";
	}	

	//On enlève l'espace de fin
	$telok=trim($telok);

	return $telok;
}    
?>