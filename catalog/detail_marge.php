<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Répartition des achats produits pour un groupement
 */


include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class GroupController extends PageController {}
$controller = new GroupController( "/account/detail_marge.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
	
} else {
	$admin_order->setData('isLogin', "");
}

$detail = "";

$statStartDate = $_SESSION["start"];
$statEndDate = $_SESSION["end"];
$grouping = $_SESSION["grouping"];	
$idcategory = $_GET["idcategory"];
$namecategory = Util::doNothing($_GET["namecateg"]);
		
	$lastcateg = array();
	$where = "";
	$GroupingCatalog = new GroupingCatalog();
		
		if  ($GroupingCatalog->getGrouping( "idgrouping" ))
		{
		
		$where = " and gs.idgrouping = ". $GroupingCatalog->getGrouping( "idgrouping" );
		}

	
		$detail.= "<h2>Repartition des produits pour la categorie : ". $namecategory ."</h2>";
		$detail.= "<br>";
	
		
		$exportCSV = array();


$arraytitle = array("N. client","Ehpad","N. cde","Date cde","N. categorie","Categorie","Reference","Designation","Qte","Prix HT");

$exportCSV[] = $arraytitle;
		
		
		$detail .= "<div style=\"font-size: 12px;\">";
		
		$detail.= "<table class='resultTable' border='1' style='width: 100%; border-collapse: collapse; border-color: #BBBBBB;' cellspacing='1'>
				 <tr>
				 <th>
				 N. client
				 </th>
				 <th>
				 Structure
				 </th>
				 <th>
				 N. cde
				 </th>
				 <th>
				 Date cde
				 </th>
				<th>
				 Categorie
				 </th>
				 <th>
				 Reference
				 </th>
				 <th>
				 Designation
				 </th>
				 <th>
				 Qte
				 </th>
				 <th>
				 Prix HT
				 </th>
				 </tr>";
		
			
	$idcat = $idcategory;

	$q3 = "SELECT `idcategorychild`
			FROM `category_link`
			WHERE `idcategoryparent` =$idcat";

	$rs3 = & DBUtil::query( $q3 );
	
	if ($rs3->RecordCount()==0)
		{
		
		array_push($lastcateg, $idcat);
		}
	
	else
		{
		
		while( !$rs3->EOF )
			{
			$idcat = $rs3->fields("idcategorychild");
		
			$q4 = "SELECT `idcategorychild`
			FROM `category_link`
			WHERE `idcategoryparent` =$idcat";
			$rs4 = & DBUtil::query( $q4 );
		
		
		
			if ($rs4->RecordCount()==0)
			{
				array_push($lastcateg, $idcat);
			}
			else
			{
				$q5 = "SELECT `idcategorychild`
				FROM `category_link`
				WHERE `idcategoryparent` =$idcat";
				$rs5 = & DBUtil::query( $q5 );
				while( !$rs5->EOF )
					{
					$idcategg = $rs5->fields("idcategorychild");
					array_push($lastcateg, $idcategg);
					$rs5->MoveNext();	
					}
			}
		
							
			$rs3->MoveNext();
			}	 	
		}
		
		
	for($i=0;$i<count($lastcateg);$i++)
	{

	$idcat = $lastcateg[$i];	

	$q4= " SELECT gs.idbuyer,
				b.company,
				gs.idorder,
				gs.conf_order_date,	 
				gs.idcategory,
				gs.quantity,				
				c.name_1,
				gs.reference,	
				d.summary_1,	 
				gs.discount_price
		
		FROM    grouping_story gs,
				buyer b,
				category c,
				detail d
					
		WHERE 	gs.idcategory = $idcat
				AND	b.idbuyer = gs.idbuyer
				AND c.idcategory = gs.idcategory
				AND d.reference = gs.reference 
				AND (gs.conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' )
				".$where ."
				ORDER  BY gs.conf_order_date ASC
				
				
				";
	$rs4 = & DBUtil::query( $q4 );
				
	while( !$rs4->EOF )
		{
							
		$detail .="
			<tr>
			<td><span style='font-size:10px;'>".$rs4->fields("idbuyer")."</span></td>
			<td><span style='font-size:10px;'>".((mb_detect_encoding($rs4->fields("company"),mb_detect_order(),true) == 'UTF-8') ? Util::doNothing($rs4->fields("company")) : $rs4->fields("company"))."</span></td>
			<td><span style='font-size:10px;'>".$rs4->fields("idorder")."</span></td>
			<td><span style='font-size:10px;'>".$rs4->fields("conf_order_date")."</span></td>
			<td><span style='font-size:10px;'>".$rs4->fields("idcategory")."</span></td>
			<td><span style='font-size:10px;'>".$rs4->fields("reference")."</span></td>
			<td><span style='font-size:10px;'>".$rs4->fields("summary_1")."</span></td>
			<td><span style='font-size:10px;'>".$rs4->fields("quantity")."</span></td>			
			<td><span style='font-size:10px;'>".$rs4->fields("discount_price")*$rs4->fields("quantity")."</span></td>
			</tr>";
									
		$arraydata = array( $rs4->fields("idbuyer"),
				 			(mb_detect_encoding($rs4->fields("company"),mb_detect_order(),true) == 'UTF-8') ? Util::doNothing($rs4->fields("company")) : $rs4->fields("company"),
				 			$rs4->fields("idorder"),
				 			$rs4->fields("conf_order_date"),
				 			$rs4->fields("idcategory"),
				 			$rs4->fields("name_1"),
				 			$rs4->fields("reference"),
							$rs4->fields("summary_1"),
							$rs4->fields("quantity"),							
							$rs4->fields("discount_price")*$rs4->fields("quantity")
							);
									
		$exportCSV[] = $arraydata;
				
		$rs4->MoveNext();
		}	 

}	
		

				 
	$detail.="</table>";
	$detail .= "<br>";
	$detail .= "</div>";

include_once('../statistics/objects/tableStats.php');

$detail .=LinkExportHistofront($exportCSV,'',0,Date("YmdHms")."-HistogrammeCAL3",'turnover');

$controller->setData( "detail", Util::doNothing($detail) ); //Affichage	


/* On ferme l'envoi de données */
$controller->output();
?>
