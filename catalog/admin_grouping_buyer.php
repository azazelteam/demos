<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet client pour l'accès au groupement
 */


include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class InvoiceController extends PageController {}
$controller = new InvoiceController( "/account/admin_grouping_buyer.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
	$controller->setData( "isInvoice", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
} else {
	$admin_order->setData('isLogin', "");
}

//-----------------------------------------------------formulaire de recherche-------------------------------------//

global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
		
	$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime() - 30*3600*24),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( ) )
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
	{
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
	}
?>
		
			
			
			<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				.dataTable{
					width:75% !important
				}
				
			/* ]]> */
			</style>
			<?php
			$form = "";
			
			$form .= '<form action="/catalog/admin_grouping_buyer.php?all" method="post" id="SearchForm">
				
					<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />

					<div class="tableContainer">
					<table class="dataTable" width="100%">';
							
							
			$form .='	<tr>
					<th class="filledCell">Entre le</th>
					<td>
					<input type="text" name="start_date" id="start_date" class="calendarInput" value='. $postData[ "start_date" ] .' />';
			$form .= '	<a href="javascript:calendar(start_date)" id="test_a" ></a>';
					$displayIcon = true; $calledFunction = false;
	
			$form .= '	</td>';
							
			$form .='	<th class="filledCell">Et le</th>
					<td>
					<input type="text" name="end_date" id="end_date" class="calendarInput" value='. $postData[ "end_date" ] .' />';
			$form .= '	<a href="javascript:calendar(end_date)" id="test_b" ></a>';

			$form .= '
					</table>
					</div>
					<div class="submitButtonContainer" style="float:right;padding-right: 200px;">
					<input type="submit" class="blueButton" value="Rechercher" />
					</div>
					</div>
					<input type="hidden" name="detail" id="detail" value="1" />
					</form>';

			$controller->setData( "form",$form );

//-----------------------------------------------------formulaire de recherche-------------------------------------//

//-----------------------------------Recupération de la liste des clients--------------------//


//---------------------------------------Recherche sur la date-----------------------------------//
if (isset ($_POST["start_date"]))
$start = $_POST["start_date"];
else
$start =$postData[ "start_date" ];
		
$start = explode("-", $start);

$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
if(isset($_POST["end_date"]))
$end = $_POST["end_date"];
else
$end =$postData[ "end_date" ];
		
$end = explode("-", $end);
		
$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
$query1 = "";
//if(isset($_POST["start_date"]))
$query1 .= " AND (b.create_date BETWEEN '$start_date' AND '$end_date' ) ";


	$idgrouping = $GroupingCatalog->getGrouping( "idgrouping" );
	 
	$query = "SELECT b.idbuyer,
					 b.company,
					 c.firstname,
					 c.lastname
			  FROM buyer b,
			  	   contact c
			  WHERE b.idbuyer = c.idbuyer
			  AND c.idcontact = 0
			  AND b.idgrouping = $idgrouping 
			  ".$query1."";
	$rs = & DBUtil::query( $query );

	for($i=0;$i<$rs->RecordCount();$i++)
	{
		
		$q1 = "SELECT idestimate 
		FROM estimate 
		where idbuyer = '". $rs->fields("idbuyer") ."' 
		AND ( valid_until = '0000-00-00' OR valid_until >= NOW() )
		AND `status` IN ( 'ToDo', 'InProgress', 'Send', 'Ordered' ) ";
		$rs1 = & DBUtil::query( $q1 );
		$q2 = "SELECT idorder 
		FROM `order` 
		where idbuyer = '". $rs->fields("idbuyer") ."'
		AND ( `status` NOT IN( 'InProgress','ToDo' ) OR idpayment = 1 )";
		$rs2 = & DBUtil::query( $q2 );
	
		$q3 = "SELECT idbilling_buyer FROM billing_buyer where idbuyer = '". $rs->fields("idbuyer") ."' ";
		$rs3 = & DBUtil::query( $q3 );
		
		
		
		
		
		$num_client = $rs->fields("idbuyer");
		$firstname = $rs->fields("firstname");
		$lastname = $rs->fields("lastname");
		$company = $rs->fields("company") ? $rs->fields("company") : "-";/*var_dump($company.mb_detect_encoding($company,mb_detect_order(),true));*/
		$num_estimate = $rs1->RecordCount();
		$num_order = $rs2->RecordCount();
		$num_invoice =$rs3->RecordCount();
		
		$array_buyer[$i]["i"] = $i;
		$array_buyer[$i]["idbuyer"] = $num_client;
		$array_buyer[$i]["name"] = $firstname . " " . $lastname;
		$array_buyer[$i]["company"] = (mb_detect_encoding($company,mb_detect_order(),true) != 'UTF-8') ? Util::doNothing($company) : $company;
		$array_buyer[$i]["num_estimate"] = $num_estimate;
		$array_buyer[$i]["num_order"] = $num_order;
		$array_buyer[$i]["num_invoice"] = $num_invoice;
		
		
		$rs->MoveNext();
	}




	$controller->setData( "array_buyer", $array_buyer );








/* On ferme l'envoi de données */
$controller->output();
?>
<script type="text/javascript">
$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_a').click();
});

$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_b').click();
});

</script>
<script type="text/javascript">

/* <![CDATA[ */
function calendar(param){
$(document).ready(function() { 

	$(param).datepicker({
		buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		showAnim: 'fadeIn',
		showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
		onClose: function(){
			<?php echo $calledFunction ?>( this );
		}
		<?php } ?>
	});
	
	
		
});
}
/* ]]> */
</script>