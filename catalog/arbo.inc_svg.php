<?php

///////////////////////////////////////////////////
// INTERACT 4.0
////////////////////////////////////////////////////
// Propriété:
// MKT-NET
// 20, rue de Hoenheim
// 67207 Niederhausbergen
//
// Emplacement:
// catalog/arbo.inc.php
//
// Fonction:
// Bibliothèque de fonctions pour le catalogue
//
//
// * Historique *****************************
//
// * Modification ***************************
//

//affichae de l'arbre
function DisplayArboCat(&$Nodes,$o,$c)
{
global $ScriptName,$GLOBAL_START_URL;
$n=count($Nodes);
for($i=0;$i<$n;$i++)
	{
	if( isset($Nodes[$i]['name']) )
		{
		$mt = $Nodes[$i]['level'];
		$id = $Nodes[$i]['id'];
		$name = $Nodes[$i]['name'];
		$ref = $Nodes[$i]['ref'];
		//$p='&nbsp;&nbsp;';
		if($Nodes[$i]['p'])
			{
			if( $o == $id ) $Nodes[$i]['o'] = TRUE;
			if( $c == $id ) $Nodes[$i]['o'] = FALSE;
			if( $Nodes[$i]['o'] ) { $p='-'; $oc = 'c';} else { $p='+';  $oc = 'o'; }
			echo "<span style=\"margin-left:".$mt."px;\" > <a href=$GLOBAL_START_URL/catalog/$ScriptName?$oc=$id >$p<a> $ref <a href=\"$GLOBAL_START_URL/catalog/product.php?&IdCategory=$id\" >$name</a><br /></span>\n";
			}
		else echo "<span style=\"margin-left:".$mt."px;\" >&nbsp;&nbsp;&nbsp;$ref <a href=\"$GLOBAL_START_URL/catalog/product.php?&IdCategory=$id\" >$name</a><br /></span>\n";
		}
	if( isset($Nodes[$i]['childs']) && $Nodes[$i]['o'] ) DisplayArboCat($Nodes[$i]['childs'],$o,$c);
	}
}


//contruction de l'arbre
function ArboCatBuild($IdCategory=0,$First=0,&$parentnodes,&$AllNodes)
{
 
        $i=0;
	$Con = &DBUtil::getConnection();
	$Langue = Session::getInstance()->getLang();
        if($First == 0)
        {
                $rs = $Con->Execute("select code_cat,name$Langue from category where idcategory='$IdCategory='");// or $IdCategory
		$name = $rs->Fields("name$Langue");
		$id = $rs->Fields("code_cat");
		$node = array('id'=> $IdCategory,'p'=> TRUE,'o'=> TRUE, 'level'=>0,'ref'=>$id,'name'=>$name);
		$AllNodes[] = &$node;
                //echo "<span class=\"rootlevel\">", $rs->Fields("name$Langue") ,"</span><br />\n";
                $rs = $Con->Execute("select distinct idcategoryparent from category_link");
                $n = $rs->RecordCount(); $ii=0;
                while($ii < $n) { $parentnodes[]=$rs->Fields("idcategoryparent"); $rs->MoveNext(); $ii++; }
		$node['childs']= array();
		return ArboCatBuild($IdCategory,$First+1,$parentnodes,$node['childs']);
        }

        $rs = $Con->Execute("select idcategorychild, code_cat,name$Langue from category_link,category  where idcategoryparent='$IdCategory' and idcategorychild=idcategory ORDER BY code_cat");
        if($rs->RecordCount())
        {
		//$AllNodes['childs']= array('id'=> $IdCategory);
                while($i < $rs->RecordCount())
                {
                        $IdCategoryChild=$rs->Fields("idcategorychild");
                        if($IdCategory!=$IdCategoryChild)
                        {
                                $mt = $First * 10;
				$name = $rs->Fields("name$Langue");
				$id = $rs->Fields("code_cat"); $p = FALSE;	
				if( in_array($IdCategoryChild,$parentnodes, FALSE) ) 
				{
				$AllNodes[$i] = array('id'=> $IdCategoryChild,'p'=> TRUE,'o'=> FALSE,'level'=>$mt,'ref'=>$id,'name'=>$name,'childs'=>array());
				ArboCatBuild($IdCategoryChild,$First+1,$parentnodes,$AllNodes[$i]['childs']);
				}
				else
				$AllNodes[$i] = array('id'=> $IdCategoryChild,'p'=> FALSE, 'o'=> FALSE,'level'=>$mt,'ref'=>$id,'name'=>$name);
                        }
                        $rs->MoveNext();
                        $i++;
                }
        }
}
?>