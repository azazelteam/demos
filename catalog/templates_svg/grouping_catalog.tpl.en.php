	<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('head/header.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
<script type="text/javascript" language="javascript">
			<!--
			function CheckAll()
			{
				var checkboxes = document.forms.mainform.elements["IdArticles"];
				var atleastcheckbox = 0;
				
				for (i = 0; i < checkboxes.length; i++){
					
					if(checkboxes[i].checked){
						var valeur = checkboxes[i].value;
						var qtt = "quantity_"+valeur;
						if(document.forms.mainform.elements["quantity_"+valeur].value==""){
							alert ("Vous devez saisir une quantité pour chaque référence que vous voulez commander !");
							return false;
						} else {
							if(isNaN(document.forms.mainform.elements["quantity_"+valeur].value)){
								alert ("La quantité pour chaque référence doit être un nombre !");
								return false;
							} else {
								if(document.forms.mainform.elements["quantity_"+valeur].value <= 0){
									alert ("La quantité pour chaque référence doit être un nombre supérieur à 0 !");
									return false;
								}
							}
						}
						
						atleastcheckbox++;
					}
				}
				if(atleastcheckbox == 0){
					alert("Veuillez cocher au moins une case avant de commander !");
					return false;
				}else{
					return true;
				}	
			}
			// Auto check the box ! checkFlex Busta Flex
			function checktheBox(id){
				var myCheckboxes = document.forms.mainform.elements["IdArticles"];
				var myInputQuantity = document.forms.mainform.elements["quantity_"+id];
				
				for (i = 0; i < myCheckboxes.length; i++){
					if(myCheckboxes[i].value == id){
						if((myInputQuantity.value != "") && (myInputQuantity.value != 0) ){
							myCheckboxes[i].checked = true ;
						}
						else {
							myCheckboxes[i].checked = false ;
						}
					}
				}
			}
			
			// -->
		</script>

	<script type="text/javascript">
$( document ).ready(function() {

	// Find the toggles and hide their content
	$('.toggle').each(function(){
		$(this).find('.toggle-content').hide();
	});

	// When a toggle is clicked (activated) show their content
	$('.toggle a.toggle-trigger').click(function(){
		var el = $(this), parent = el.closest('.toggle');

		if( el.hasClass('active') )
		{
			parent.find('.toggle-content').slideToggle();
			el.removeClass('active');
		}
		else
		{
			parent.find('.toggle-content').slideToggle();
			el.addClass('active');
		}
		return false;
	});

});  //End
</script>
<!-- Header -->
<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('menu.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
<div id="content" class="account">
<?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'displayLink'))) if ($t->head->displayLink()) { ?><span style="color:red;font-weight: bold;">/account/grouping_catalog.tpl</span><?php }?>
		<!-- Menu Compte -->
		<div id="leftbloc">
				<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('menu/menu_compte.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
		</div>
  <div id="categorybloc" class="account">
		<div class="intro">
					<h2>Catégories avec remises spécifiques</h2>
			<div class="spacer"></div>
       </div>
 <?php if ($this->options['strict'] || (is_array($t->categorys)  || is_object($t->categorys))) foreach($t->categorys as $line) {?>
 <article>
<!-- Promo -->
<a href="<?php echo $line['url'];?>" style="min-height:90px!important;">
   <p style="color:#55C4C2;font-size:12px;"><b><?php echo $line['name_1'];?></b></p>
   <img src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/images/<?php echo $line['image'];?>" alt="<?php echo $line['name_1'];?>" /><br><br><br>
   <p style="color:#CC0000;font-size:12px;"><b>Remise  : <?php echo $line['rate'];?>% </b></p>
</a>
</article>
<?php }?>
<div class="spacer"></div>	
</div><br />
<br />
		<!-- Contenu -->
		<div id="categorybloc" class="account">
			<div class="intro">
					<h2>Produits négociés - <?php echo $t->buyerTitle;?> <?php echo $t->buyerLastname;?></h2>
			&nbsp;&nbsp;<span style="font-size:11px;display:block;margin: 0 0 10px 7px;">Veuillez trouvez ci-dessous votre selection de produits à prix négociés.</span>
			<?php if ($t->sucess_msg)  {?><br><br><center><span style="font-size:13px;font-family:Arial, Helvetica, sans-serif;"><b><?php echo htmlspecialchars($t->sucess_msg);?> <b></span><center><?php }?>
			<?php 
if (!isset($this->elements['mainform']->attributes['action'])) {
    $this->elements['mainform']->attributes['action'] = '';
    $this->elements['mainform']->attributes['action'] .=  htmlspecialchars($t->start_url);
    $this->elements['mainform']->attributes['action'] .= "/catalog/grouping_catalog.php";
}
$_attributes_used = array('action');
echo $this->elements['mainform']->toHtmlnoClose();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['mainform']->attributes[$_a]);
}}
?>
            <?php echo $this->elements['validated']->toHtml();?>
			
			<div class="spacer"></div>
			<?php if ($this->options['strict'] || (is_array($t->references)  || is_object($t->references))) foreach($t->references as $line) {?>
			<a style="text-decoration:none;font-size: 12px;color:#000;padding:0 0 0 8px" href="<?php echo $line['url_cat'];?>"><span><b> <?php echo $line['nameCat'];?></b></span>	</a>
			<?php if ($this->options['strict'] || (is_array($line['categorie'])  || is_object($line['categorie']))) foreach($line['categorie'] as $line1) {?>
<div class="toggle">
<!-- Toggle Link -->
	<a href="#" title="<?php echo $line1['name_cat'];?>" class="toggle-trigger"><span style="font-size: 12px;color: #666666;"> <?php echo $line1['name_cat'];?></span> </a>
<!--<span style="text-decoration:none;font-size: 11px;" href="{line[url_cat_parent]:h}">{line[name_cat_parent]:h}</span>-->

<div class="toggle-content">

			<table class="accountTable" cellspacing="0" cellpadding="0" border="0">		
				
				<tr>
					<th></th>
					<th>Photo</th>
					<th>Référence</th>
					<th>Désignation article</th>
					<th>Prix Unit.</th>
					<th>Remise</th>
					<th>Prix Net</th>
					<th>Quantité</th>
					<th>Cder</th>
				</tr>
				
				<?php if ($this->options['strict'] || (is_array($line1['article'])  || is_object($line1['article']))) foreach($line1['article'] as $value) {?>
				
				<tr><td class="inProgress"><span onmouseover="document.getElementById('pop<?php echo $value['idarticle'];?>').className='tooltiphover';" onmouseout="document.getElementById('pop<?php echo $value['idarticle'];?>').className='tooltip';" style="position:relative;"><img src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/images/iconPlus.gif" alt="voir le détail" class="tooltipa VCenteredWithText" /><span id='pop<?php echo $value['idarticle'];?>' class='tooltip'><?php echo $value['designation'];?></span></td>
					<td class="inProgress"><a href="<?php echo $value['url'];?>"><img src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/images/<?php echo $value['image'];?>" border="0" width="40" alt="Icône" /></a></td>
					<td class="inProgress"><a class="overHrefBold" href="<?php echo $value['url'];?>"><?php echo $value['reference'];?></a>
						</td>
					<td class="inProgress"><div style="margin-top:5px;"><a class="overHref" href="<?php echo $value['url'];?>"><p style="border-top: 1px dotted #E4E4E4;
    color: #565656;
  
    font-size: 12px;"><?php echo htmlentities($value['name_1']);?></p><b><?php echo $value['summary'];?></b></a></div></td>
					<td class="inProgress"><?php echo $value['basic_price'];?></td>
					<td class="inProgress"><?php echo $value['discount'];?></td>
					<td class="inProgress"><?php echo $value['discount_price'];?></td>
					<td class="inProgress"><input type="text" id="quantity_<?php echo htmlspecialchars($value['i']);?>" name="quantity_<?php echo htmlspecialchars($value['i']);?>" size="2" onChange="checktheBox(<?php echo htmlspecialchars($value['idarticle']);?>);" class="input_quantity" /></td>
					<td class="inProgress"><input type="checkbox" id="check_<?php echo htmlspecialchars($value['i']);?>" name="check_<?php echo htmlspecialchars($value['i']);?>" value="<?php echo htmlspecialchars($value['i']);?>" /></td>
				</tr>
                <input type="hidden" name="ref_<?php echo htmlspecialchars($value['i']);?>" value="<?php echo $value['reference'];?>">
			
				<?php }?>
				
				<tr>
					
			</table>
			<div style="clear:both"></div>
		</div>
<div style="clear:both"></div>
	</div>
				<?php }?>
				<?php }?>
			</form>	
            <?php if ($t->refs_not_found)  {?>
<br><br>
<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
<th>Qté</th>
 <th>Référence</td>
</tr>
<?php if ($this->options['strict'] || (is_array($t->list_articles_not_found)  || is_object($t->list_articles_not_found))) foreach($t->list_articles_not_found as $article) {?>
<tr>
<td><?php echo htmlspecialchars($article['quantity']);?></td>
<td><?php echo htmlspecialchars($article['reference']);?></td>
</tr>
<?php }?>
</table>

</br></br>

<font color=black size=-1 face=arial><b>
Les articles ci-dessus n'ont pas été trouvées ou la quantité est fausse!
</br>
Merci de vérifier la référence et la quantité.

</b></font>
<br><br>
<?php }?>

<?php if ($t->hasrefwoprice)  {?>

<table cellspacing="0" class="BorderedTable" width="200px">
<tr>
 <th>Référence</td>
</tr>
<?php if ($this->options['strict'] || (is_array($t->list_refs_hide_prices)  || is_object($t->list_refs_hide_prices))) foreach($t->list_refs_hide_prices as $ref) {?>
<tr>
<td><?php echo htmlspecialchars($ref);?></td>
</tr>
<?php }?>
</table>
		
<br><br>
		<p style="color:#000000; font-weight:bold">
		Le prix des références ci-dessus n'est pas disponible pour le moment
		</p>
		<p>
		Pour plus d'informations concernant ces références, <a href="mailto:<?php echo htmlspecialchars($t->ad_mail);?>" style="text-decoration:underline;">contactez-nous par email</a> ou au <font style="font-weight:bold; color:#FF0000;"><?php echo htmlspecialchars($t->ad_tel);?></font>
		</p>
<?php }?>
	<div class="spacer"></div>
</div>	
<div class="spacer"></div>	
</div>
</br></br></br><div class="spacer"></div>	
</div>
</div>

<!-- footer -->	
<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('foot.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
</div>
