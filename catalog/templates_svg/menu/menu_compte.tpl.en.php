 <nav id="left">
 <?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'displayLink'))) if ($t->head->displayLink()) { ?><span style="color:red;font-weight: bold;">/menu/menu_compte.tpl</span><?php }?>
 <ul class="menu">
        <li><span>Actions client</span>
<ul>
		<li>
		<?php if ($t->isEstimate)  {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_estimate.php?all" class="actif">
		<?php } else {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_estimate.php?all">
		<?php }?>
		Mes devis</a></li>
		<li>
		<?php if ($t->isOrder)  {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_order.php?all" class="actif">
		<?php } else {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_order.php?all">
		<?php }?>
		Mes commandes</a></li>
		<li>
		<?php if ($t->isInvoice)  {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_invoice.php?all" class="actif">
		<?php } else {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_invoice.php?all">
		<?php }?>
		Mes factures</a></li>
		<!-- @todo <li><a href="{baseURL}/"><span>Mon catalogue perso</span></a></li>  -->
       		<li>
		<?php if ($t->isOrder)  {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/product_term_buyer.php?all" class="actif">
		<?php } else {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/product_term_buyer.php?all">
		<?php }?>
		Mes échéanciers</a></li>
        
        <li>
        <?php if ($t->isOrder)  {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/marge_category_buyer.php?init=1" class="actif">
		<?php } else {?>
			<a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/marge_category_buyer.php?init=1">
		<?php }?>
		Décisionnel client</a></li>
        </li>	
		
        </ul>
	</li>
</ul>
	</nav>
	<?php if ($t->buyerGroup)  {?>
	<nav id="left">	
				<a href="/catalog/grouping_catalog.php"> <img src="<?php echo htmlspecialchars($t->logoGroup);?>" border="0" alt="Logo" width="180px" height="100px" /> </a>
               <ul class="menu">
	<li> <a href="/catalog/grouping_catalog.php"> <span>Catalogue</span></a>
				</ul>
	</nav>	
	<?php }?>	
	<?php if ($t->contact_grouping)  {?>	
	<nav id="left">	
        <ul class="menu">
        <li><span>Actions</span>
        	<ul>
            <li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_grouping_buyer.php?all" class="Account_liens">Liste des clients</a></li>
            <li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_grouping_estimate.php?all" class="Account_liens">Devis</a></li>
            <li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_grouping_order.php?all" class="Account_liens">Commandes</a></li>
            <li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/admin_grouping_invoice.php?all" class="Account_liens">Factures</a></li>
            </ul>
        </li>
        
        </ul>
        
        
        
        
<ul class="menu">
	<li><span>Reporting</span>
    	<ul>
		<li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/groupement_history.php" class="Account_liens">Historique groupement</a></li>
		<li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/marge_category.php" class="Account_liens">Produits / Catégories</a></li>
		</ul>
	</li>
</ul>
</nav>
<?php }?>		
<nav id="left">	
<ul class="menu">
	<li><span>Mon compte</span>
    	<ul>
			<li><a href="<?php echo htmlspecialchars($t->baseURL);?>/catalog/buyer_infos.php" class="Account_liens">Modifier mes coordonn&eacute;es</a></li>
            <li><a href="<?php echo htmlspecialchars($t->baseURL);?>/www/logoff.php" class="Account_liens">Déconnexion</a></li>
         </ul>
	</li>
</ul>
</nav>