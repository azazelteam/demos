	<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('head/header.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
	<script src="<?php echo htmlspecialchars($t->baseURL);?>/js/jquery.simpletooltip.js" type="text/javascript"></script>
	<script src="<?php echo htmlspecialchars($t->baseURL);?>/js/jquery/jquery.form.js" type="text/javascript"></script>
	
	<script type="text/javascript">
	<!--

	// Test des champs du formulaire de login
	function verifLogin (){
		var login = document.forms.RegisterForm.elements[ 'Login' ].value;	 
		var password = document.forms.RegisterForm.elements[ 'Password' ].value;
			
		if(login == ''){
			alert('Le champ E-mail est vide !');
			return false;
		}
		
		if(password == ''){
			alert('Le champ Mot de passe est vide !');
			return false;
		}
		return true ;
	}
		
	
	function verifCreateAccount() {
		
		var data = $('#CreateAccount').formSerialize(); 
		
		$.ajax({
			url: "/catalog/account.php?createLoginAjax",
			async: true,
			type: "POST",
			data: data,
			error : function( XMLHttpRequest, textStatus, errorThrown ){ },
		 	success: function( responseText ){
		 		
		 		if(responseText != '') {
					$("#returnCreateAccount").html(responseText); 
				} else {
					document.getElementById("CreateAccount").submit();
					
				}

			}
		});
	}
	
	
	// Test des champs du formulaire d'enregistrement
	function verifForm (){
		 
		var emailAddr = RegisterForm.elements[ 'email' ].value;
		
		if(emailAddr == ''){
			alert('Le champ E-mail est vide !');
			return false;
		}else{
			if(!isEmail(emailAddr)){
				alert('L\'identifiant saisie doit être une adresse e-mail  valide !');
				return false;
			}
		}		
			
	  
		if( RegisterForm.elements[ 'pwd' ].value == "") {
			
			alert('Le champ Mot de passe est vide !');
			return false;
			
		}else if( RegisterForm.elements[ 'pwd' ].value.length < 6) {
				
				alert('Le champ Mot de passe doit comporter au minimum 6 caractères !');
				return false;
	  
		}
	
		if( RegisterForm.elements[ 'pwd_confirm' ].value == "") {
		
			alert('Le champ confirmation mot de passe est vide !');
			return false;
			
		}else if( RegisterForm.elements[ 'pwd_confirm' ].value != RegisterForm.pwd.value) {
		
			alert('Le champ confirmation de mot de passe doit être identique au champ Mot de passe !');
			return false;
				
		}
		
		if(this.RegisterForm.elements['title'].options[this.RegisterForm.elements['title'].selectedIndex].value == ''){
			
			alert('Le champ Titre est vide !');
			return false;
		
		} 
		
		if( RegisterForm.elements[ 'lastname' ].value == '') {
		
			alert('Le champ Nom ?> est vide !');
			return false;
			
		}
			  
		if( RegisterForm.elements[ 'phonenumber' ].value == '') {
		
			alert('Le champ Téléphone est vide !');
			return false;
			
		}
		  
		if( RegisterForm.elements[ 'company' ].value == '') {
		  
			alert('Le champ Société est vide !');
			return false;
			
		}
		  
		if( RegisterForm.elements[ 'adress' ].value == '') {
		  
			alert('Le champ Adresse est vide !');
			return false;
			
		}
		
		if( RegisterForm.elements[ 'zipcode' ].value == "") {
		  
			alert('Le champ Code Postal est vide !');
			return false;
			
		}
		
		if( RegisterForm.elements[ 'city' ].value == "") {
		  
			alert('Le champ Ville est vide !');
			return false;
			
		}
		
		if( RegisterForm.elements[ 'siret' ].value == "") {
		  
			alert('Le champ N° Siret est vide !');
			return false;
			
		}
		  
		return true;
		 
		}
	
	function isEmail(strSaisie) {
		var verif = /^[^@]+@(([\w\-]+\.){1,4}[a-zA-Z]{2,4}|(([01]?\d?\d|2[0-4]\d|25[0-5])\.){3}([01]?\d?\d|2[0-4]\d|25[0-5]))$/
		return ( verif.test(strSaisie) );
	}
	
	jQuery(document).ready(function() {
		$("#newUser .blocTitle").click(function () {
			$("#newUser .toggle").toggle("slow");
		});
	});
	
	// -->
	</script>    
<!--</head>
<body>-->
<!-- Header -->
<!--<flexy:include src="head/banner.tpl"></flexy:include>
<flexy:include src="menu/navigation.tpl"></flexy:include>-->
<div id="content" class="account">
<?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'displayLink'))) if ($t->head->displayLink()) { ?><span style="color:red;font-weight: bold;">/account/account.tpl</span><?php }?>
	<?php if ($t->isLogin)  {?>
	
		<!-- Menu Compte -->
		<div id="leftbloc">
			<!--<nav id="left">-->
				<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('menu/menu_compte.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
             <!--   {if:logoGroup} 
				
               <ul class="menu">
	<li> <a href="/catalog/grouping_catalog.php"> <span>Votre catalogue</span></a>
    	
	</li>
</ul>
 {end:}
			
            {if:logoGroup}
		
             <a href="/catalog/grouping_catalog.php"> <img src="{logoGroup}" border="0" alt="Logo" width="180px" height="100px"/> </a>
             {end:}
			 -->
			</div>
		<!--</div>-->
		
		<!-- Contenu -->
		<div id="categorybloc" class="account">
			<div class="intro stretch-down">
				<h2>Bienvenue sur votre compte</h2>

				<div id="text">
                    <p>
                        La création de votre compte vous permet dès à présent :
                    </p>
                    <ul>
                        <li>de gérer votre compte.</li>
                        <li>de créer et mémoriser votre catalogue perso.</li>
                        <li>de consulter vos demandes de devis et de suivre l'état de vos commandes.</li>
                    </ul>
                    <p style="margin-bottom: 10px;">
                        Toute notre &eacute;quipe vous remercie de votre confiance.<br />
                        Pour toutes informations complémentaires, n'hésitez pas à nous contacter au 03.88.66.02.03<sup>*</sup>.
                    </p>
                    <p class="small">
                        * Appel gratuit depuis un poste fixe.<br />
                        <?php if ($t->buyerFirstname)  {?>
                            Si vous n'êtes pas <strong><?php echo htmlspecialchars($t->buyerFirstname);?> <?php echo htmlspecialchars($t->buyerLastname);?></strong>, merci de vous déconnecter <a href="<?php echo htmlspecialchars($t->baseURL);?>/www/logoff.php" class="Account_logoffA">en cliquant ici</a>.
                        <?php }?>
                        <?php if ($t->customer)  {?>
                            Si vous n'êtes pas <?php if ($this->options['strict'] || (isset($t->customer) && method_exists($t->customer, 'get'))) echo htmlspecialchars($t->customer->get("firstname"));?> <?php if ($this->options['strict'] || (isset($t->customer) && method_exists($t->customer, 'get'))) echo htmlspecialchars($t->customer->get("lastname"));?>, merci de vous déconnecter <a href="<?php echo htmlspecialchars($t->baseURL);?>/www/logoff.php" class="Account_logoffA">en cliquant ici</a>.
                        <?php }?></p>
				</div>
				
			</div>
		</div>
		<div class="spacer"></div>
		
	<?php } else {?>
		<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('page/left.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
		
		<!-- Contenu -->
		<div id="categorybloc">
			<div class="intro">
				<h2>Accéder à mon compte.</h2>
				<div id="text">
					<p>Liste personnelle, historique de devis et commandes...</p>
									
					<!-- Déja client -->
					<div id="registeredUser" class="blocRight">
						<p><strong>Déjà client ? Connectez-vous.</strong></p>
						
						<form action="<?php echo htmlspecialchars($t->baseURL);?>/catalog/account.php" method="post">
							<?php if ($t->issetPostLoginAndNotLogged)  {?>
								<!-- Erreur lors de l'identification -->
								<p class="msgError">Votre e-mail ou mot de passe est incorrect.</p>
							<?php }?>
							
							<p>	
								<?php echo $this->elements['Login']->toHtml();?>
								<?php echo $this->elements['Password']->toHtml();?>
								<input type="submit" class="submitField" value="Connexion" />
							</p>
							<div class="spacer"></div>
							
							<p>
								<a href="javascript:mail();" class="passwordForget">Mot de passe oublié ?</a>
							</p>
							<div class="spacer"></div>
						</form>				
						<div class="spacer"></div>
					</div>
                    
                    <div class="spacer"></div>
                    <hr />
                    
					<!-- Nouveau client -->
					<div id="newUser" class="bloc">
						<p class="blocTitle clickable"><strong>Nouveau client ? <a>Créer votre compte.</a></strong></p>
						
	                    <div class="toggle">
                            <p class="msgError" id="returnCreateAccount"></p>
                            
                            <!--
                            <form action="{baseURL}/catalog/register.php" method="post" id="CreateAccount">
                                <p>
                                    <input type="text" name="createLogin" class="loginField input_email" value="" />
                                    <input type="password" name="createPassword" class="loginFieldMedium input_password" value="" />
                                    <input type="password" name="createConfPassword" class="loginFieldMedium marginLeft input_conf_password" value="" />
                                    <input type="button" onClick="verifCreateAccount();" class="submitField" value="Etape suivante" />
                                </p>

                            </form>
                            <div class="spacer"></div>
                            -->

                            <form action="<?php echo htmlspecialchars($t->baseURL);?>/catalog/register.php" id="RegisterForm" method="post" onSubmit="return verifForm();"> 
                                <p>Tous les champs marqués d'un <span class="required">fond bleu</span> sont obligatoires.</p>
                                <div class="spacer"></div><br />
                                
                                <?php if ($t->errorMsgNotEmpty)  {?>					
                                    <p class="msgError"><?php echo $t->errorMsg;?></p><br />
                                <?php }?>
                                
                                <?php 
if (!isset($this->elements['FromScript']->attributes['value'])) {
    $this->elements['FromScript']->attributes['value'] = '';
    $this->elements['FromScript']->attributes['value'] .=  htmlspecialchars($t->scriptName);
}
$_attributes_used = array('value');
echo $this->elements['FromScript']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['FromScript']->attributes[$_a]);
}}
?>
                                <p><strong>Informations de connexion à mon compte</strong></p>
                                <div class="spacer"></div>
                                       
                                <p>
                                    <?php 
if (!isset($this->elements['email']->attributes['value'])) {
    $this->elements['email']->attributes['value'] = '';
    $this->elements['email']->attributes['value'] .=  $t->userInfoEmail;
}
$_attributes_used = array('value');
echo $this->elements['email']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['email']->attributes[$_a]);
}}
?>
                                    <?php 
if (!isset($this->elements['pwd']->attributes['value'])) {
    $this->elements['pwd']->attributes['value'] = '';
    $this->elements['pwd']->attributes['value'] .=  $t->userInfoPwd;
}
$_attributes_used = array('value');
echo $this->elements['pwd']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['pwd']->attributes[$_a]);
}}
?>
                                    <?php 
if (!isset($this->elements['pwd_confirm']->attributes['value'])) {
    $this->elements['pwd_confirm']->attributes['value'] = '';
    $this->elements['pwd_confirm']->attributes['value'] .=  $t->userInfoPwdConfirm;
}
$_attributes_used = array('value');
echo $this->elements['pwd_confirm']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['pwd_confirm']->attributes[$_a]);
}}
?>
                                </p>
                                <div class="spacer"></div><br />
                                
                                <p><strong>Mes coordonnées</strong></p>
                                <div class="spacer"></div>
                            
                                <div class="formleft">
									<div class="selectTitle required">
									<?php echo $this->elements['title']->toHtml();?>
									</div>

                                    <?php 
if (!isset($this->elements['firstname']->attributes['value'])) {
    $this->elements['firstname']->attributes['value'] = '';
    $this->elements['firstname']->attributes['value'] .=  $t->userInfoFirstname;
}
$_attributes_used = array('value');
echo $this->elements['firstname']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['firstname']->attributes[$_a]);
}}
?>
                                    <?php 
if (!isset($this->elements['lastname']->attributes['value'])) {
    $this->elements['lastname']->attributes['value'] = '';
    $this->elements['lastname']->attributes['value'] .=  $t->userInfoLastname;
}
$_attributes_used = array('value');
echo $this->elements['lastname']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['lastname']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>   
                                    <?php 
if (!isset($this->elements['phonenumber']->attributes['value'])) {
    $this->elements['phonenumber']->attributes['value'] = '';
    $this->elements['phonenumber']->attributes['value'] .=  $t->userInfoPhone;
}
$_attributes_used = array('value');
echo $this->elements['phonenumber']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['phonenumber']->attributes[$_a]);
}}
?>
                                    <?php 
if (!isset($this->elements['faxnumber']->attributes['value'])) {
    $this->elements['faxnumber']->attributes['value'] = '';
    $this->elements['faxnumber']->attributes['value'] .=  $t->userInfoFax;
}
$_attributes_used = array('value');
echo $this->elements['faxnumber']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['faxnumber']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>
                                    <?php 
if (!isset($this->elements['gsm']->attributes['value'])) {
    $this->elements['gsm']->attributes['value'] = '';
    $this->elements['gsm']->attributes['value'] .=  $t->userInfoGsm;
}
$_attributes_used = array('value');
echo $this->elements['gsm']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['gsm']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>             
									<div class="selectField required">
										<?php echo $this->elements['iddepartment']->toHtml();?>
									</div>
                                    <div class="spacer"></div>
                                    <?php 
if (!isset($this->elements['company']->attributes['value'])) {
    $this->elements['company']->attributes['value'] = '';
    $this->elements['company']->attributes['value'] .=  $t->userInfoCompany;
}
$_attributes_used = array('value');
echo $this->elements['company']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['company']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>
                                    <?php 
if (!isset($this->elements['adress']->attributes['value'])) {
    $this->elements['adress']->attributes['value'] = '';
    $this->elements['adress']->attributes['value'] .=  $t->userInfoAdress;
}
$_attributes_used = array('value');
echo $this->elements['adress']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['adress']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>
                                    <?php 
if (!isset($this->elements['adress_2']->attributes['value'])) {
    $this->elements['adress_2']->attributes['value'] = '';
    $this->elements['adress_2']->attributes['value'] .=  $t->userInfoAdress_2;
}
$_attributes_used = array('value');
echo $this->elements['adress_2']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['adress_2']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>
                                    <?php 
if (!isset($this->elements['zipcode']->attributes['value'])) {
    $this->elements['zipcode']->attributes['value'] = '';
    $this->elements['zipcode']->attributes['value'] .=  $t->userInfoZipcode;
}
$_attributes_used = array('value');
echo $this->elements['zipcode']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['zipcode']->attributes[$_a]);
}}
?>
                                    <?php 
if (!isset($this->elements['city']->attributes['value'])) {
    $this->elements['city']->attributes['value'] = '';
    $this->elements['city']->attributes['value'] .=  $t->userInfoCity;
}
$_attributes_used = array('value');
echo $this->elements['city']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['city']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div>
									<div class="selectField required">
										<?php echo $this->elements['idstate']->toHtml();?>
									</div>
                                    <div class="spacer"></div>
                                    <?php 
if (!isset($this->elements['siret']->attributes['value'])) {
    $this->elements['siret']->attributes['value'] = '';
    $this->elements['siret']->attributes['value'] .=  $t->userInfoSiret;
}
$_attributes_used = array('value');
echo $this->elements['siret']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['siret']->attributes[$_a]);
}}
?>
                                    <?php 
if (!isset($this->elements['naf']->attributes['value'])) {
    $this->elements['naf']->attributes['value'] = '';
    $this->elements['naf']->attributes['value'] .=  $t->userInfoNaf;
}
$_attributes_used = array('value');
echo $this->elements['naf']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['naf']->attributes[$_a]);
}}
?>
                                    <div class="spacer"></div><br />    
                                    <p><?php echo $this->elements['newsletter']->toHtml();?>&nbsp;<strong>Abonnement à la newsletter du site.</strong> Tenez-moi informé de l'actualité, des promotions et des opérations de déstockage qui vous concernent.</p>
                                    <p class="small">Selon la loi "Informatique et libertés" du 05/01/76, vous disposez d'un droit d'accès, de rectification et de suppression des données qui vous concernent.</p>
                                </div>
                                <p><?php echo $this->elements['Register']->toHtml();?></p>
                                
                                <?php 
if (!isset($this->elements['idsource']->attributes['value'])) {
    $this->elements['idsource']->attributes['value'] = '';
    $this->elements['idsource']->attributes['value'] .=  $t->param_admin_default_buyer_source;
}
$_attributes_used = array('value');
echo $this->elements['idsource']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['idsource']->attributes[$_a]);
}}
?>
                            </form>
                        </div>
                        
						<div class="spacer"></div>
					</div>
					
				</div>					
			</div>
			<div class="spacer"></div>
			
			
			
		</div>
	<?php }?>
	<div class="spacer"></div>
		
</div>
</div>


<!-- footer -->	
<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('foot.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
</div>