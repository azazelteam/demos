<!DOCTYPE HTML>
<html lang="fr">
<head>	
	<meta charset="utf-8" />
<title><?php if ($t->title)  {?><?php echo $t->title;?><?php } else {?><?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'getTitle'))) echo htmlentities($t->head->getTitle());?><?php }?></title>
<meta content="width=device-width, initial-scale=1, user-scalable=0, maximum-scale=1" name="viewport" />
	<?php if ($this->options['strict'] || (is_array($t->head->getMetaTags())  || is_object($t->head->getMetaTags()))) foreach($t->head->getMetaTags() as $name => $content) {?><meta name="<?php echo htmlspecialchars($name);?>" content="<?php echo $content;?>" /><?php }?>	
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/css/general.css" />
	<link rel="stylesheet" type="text/css" media="all" href="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/css/scroll.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/css/slider.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/css/carousel/carousel.css" />
	<!--<script src="{start_url}/js/tooltip.js" type="text/javascript" async></script>
	<script src="{start_url}/www/_js/bookmark.js" type="text/javascript" async></script>-->
    <script src="<?php echo htmlspecialchars($t->start_url);?>/js/prodoptions.js" type="text/javascript" defer></script>
	<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/jquery/jquery-1.6.2.min.js"></script>
	<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/jquery/jquery-ui-1.8.4.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/js_search_filter.js" defer></script>
	<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/front.droppy.js" defer></script>	
	<?php if ($t->head)  {?>
	<?php if ($this->options['strict'] || (is_array($t->head->getScripts())  || is_object($t->head->getScripts()))) foreach($t->head->getScripts() as $script) {?><script type="text/javascript" src="<?php echo htmlspecialchars($script);?>" defer></script><?php }?>
	<?php if ($this->options['strict'] || (is_array($t->head->getCSS())  || is_object($t->head->getCSS()))) foreach($t->head->getCSS() as $css) {?><link type="text/css" rel="stylesheet" href="<?php echo htmlspecialchars($css);?>" /><?php }?>
    <?php }?>	
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/css/superfish.css" />
<link rel="stylesheet" type="text/css" media="screen" href="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/css/superfish-navbar.css" />
	<!--[if lt IE 7.]>
	<script defer type="text/javascript" src="{start_url}/js/pngfix.js"></script>
	<link href="{start_url}/templates/catalog/css/ltie7.css" rel="stylesheet" type="text/css" />
	<![endif]--> 
    <?php require_once 'HTML/Javascript/Convert.php';?>
<script type='text/javascript'>
<?php $__tmp = HTML_Javascript_Convert::convertVar($t->baseURL,'baseURL',true);echo (is_object($__tmp) && is_a($__tmp,"PEAR_Error")) ? ("<pre>".print_r($__tmp,true)."</pre>") : $__tmp;?>
</script>	
	<script type="text/javascript">
		function redirectToBasket(){
			document.location.href=urldep+"/catalog/basket.php"; 
		}		
		function redirectToEstimateBasket(){
			document.location.href=urldep+"/catalog/estimate_basket.php"; 
		}		
	</script>
	<!-- ScrollTo -->
	<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/jquery/jquery.scrollTo-min.js" async></script>
	
	<?php require_once 'HTML/Javascript/Convert.php';?>
<script type='text/javascript'>
<?php $__tmp = HTML_Javascript_Convert::convertVar($t->start_url,'globalstarturl',true);echo (is_object($__tmp) && is_a($__tmp,"PEAR_Error")) ? ("<pre>".print_r($__tmp,true)."</pre>") : $__tmp;?>
</script>		
	<script type="text/javascript">
		$(document).ready(function() {
				var inprog1 = 0;
				$("#commandeI").hover(function() {
				 if (inprog1 == 0) {
					 inprog1 = 1;					
					filename = globalstarturl+"/catalog/basket_popup.php";				
					$.ajax(
								{
									type: "GET",
									url: filename,
									data: "",
									dataType: "html",
									contentType : "text/html; charset=UTF-8",
									success:function(response){document.getElementById('basketOrder').innerHTML = response;}
								}
							);					 
					 $("#divcommande").slideDown(100);					 
				 }
			}, function() {
				 $("#divcommande").slideUp(100, function() {inprog1 = 0;});
			});
		});	
		function forgetPassword(){$("#forgetPassword").dialog({title:"Mot de passe oublié ?",bgiframe:true,modal:true,width:550,minHeight:10,overlay:{backgroundColor:'#000000',opacity:0.5},close:function(event,ui){$("#forgetPassword").dialog("destroy");}});}
		function sendPassword(Email,divResponse){data="&Email="+Email;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(XMLHttpRequest,textStatus,errorThrown){alert("Impossible de récupérer l'email");},success:function(responseText){$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+responseText+"</p>");}});}
	</script>
    <script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/jquery-transit-modified.js"></script>
    <script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/jquery-ui-1.10.3.custom.pack.js"></script>
    <!--<script type="text/javascript" src="{start_url}/templates/catalog/js/jquery.selectbox-0.2.pack.js" ></script>-->
<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/jquery.textPlaceholder.js"></script>
<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/jquery.sky.carousel-1.0.2.pack.js"></script>   
    <script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/layerslider.transitions.js" defer></script>
	<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/layerslider.kreaturamedia.jquery.js" defer></script> 
    <script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/templates/catalog/js/menu.js"></script>    
    <script type="text/javascript">
		function forgetPassword(){
			//$('#bg-ui-clone').show();
			$('#forgetPassword').show();
		}
		function sendPassword(Email,divResponse){data="&Email="+Email;$.ajax({url:"/catalog/forgetmail.php",async:true,type:"POST",data:data,error:function(XMLHttpRequest,textStatus,errorThrown){alert("Impossible de récupérer l'email");},success:function(responseText){$(divResponse).html("<br /><p class='blocTextNormal' style='text-align:center;'>"+responseText+"</p>");}});}
		function closeBTN(){
			//$('#bg-ui-clone').hide();
			$('#forgetPassword').hide();
		}
	</script>
<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/superfish_menu/hoverIntent.js" defer></script>
<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/superfish_menu/superfish.js" defer></script>
<script type="text/javascript" src="<?php echo htmlspecialchars($t->start_url);?>/js/superfish_menu/supersubs.js" defer></script> 
<script type="text/javascript">
(function() 
	{
		var s = document.createElement('script');
		s.type = 'text/javascript'; s.async = true;
		s.src = ('https:' == document.location.protocol ? 'https://' : 'http://')+'bp-1c51.kxcdn.com/prj/AS-2314166.js';
		var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
	}
)();
</script> 
<script type="text/javascript">
var google_replace_number = "03.88.88.88.88";
(function(a,e,c,f,g,b,d){var h={ak:"1038552525",cl:"YM1jCOiS3WcQzZuc7wM"};a[c]=a[c]||function(){(a[c].q=a[c].q||[]).push(arguments)};a[f]||(a[f]=h.ak);b=e.createElement(g);b.async=1;b.src="//www.gstatic.com/wcm/loader.js";d=e.getElementsByTagName(g)[0];d.parentNode.insertBefore(b,d);a._googWcmGet=function(b,d,e){a[c](2,b,h,d,null,new Date,e)}})(window,document,"_googWcmImpl","_googWcmAk","script");
</script> 
</head>
<body>
<div class="ui-widget-overlay" id="bg-ui-clone" style="width: 100%; height: 100%;display: none; z-index: 1001;"></div>

<div id="mainContainer">

		<div id="contents">	
		<?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'displayLink'))) if ($t->head->displayLink()) { ?><span style="color:red;font-weight: bold;">/head/header.tpl</span><?php }?>
			<div id="header">
			

	        Logo
           
			<div id="menutop">
            	<div id="loginpanel">
                	<p>Mon <strong>compte</strong></p>
                    <?php if ($t->isLogin)  {?>
                    <style type="text/css">
						#header #loginpanel h3 {
							margin: 0;
							text-align: center;
							width: 100%;
						}
						#loginpanel > div {
							clear: both;
							float: left;
							margin: 10px 0;
							text-align: center;
							width: 100%;
						}
						#header #loginpanel ul {
							clear: both;
							float: left;
							text-align: center;
							width: 100%;
							margin: 0;
						}
					</style>   
                 <div>
                 	<span class="welcome" style="color: #fff; font-weight: bold;">Bienvenue</span>					
                	<span class="userlogin" style="color: #fff;"><?php if ($this->options['strict'] || (isset($t->customer) && method_exists($t->customer, 'get'))) echo htmlspecialchars($t->customer->get("firstname"));?> <?php if ($this->options['strict'] || (isset($t->customer) && method_exists($t->customer, 'get'))) echo htmlspecialchars($t->customer->get("lastname"));?></span>  
                 </div>
                <ul>					
                    <li class="left"><a href="<?php echo htmlspecialchars($t->start_url);?>/catalog/account.php" rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"> Mon compte</a></li>					
                    <li class="left"><a href="<?php echo htmlspecialchars($t->start_url);?>/www/logoff.php" rel="nofollow" class="lien" style="color:#F1A801; text-decoration:none"> [D&eacute;connexion]</a></li>
                </ul>    
            <?php } else {?>
                    <?php echo $this->elements['Form_logon']->toHtmlnoClose();?>                    
                        <label for="emaillogin">Adresse email</label>
                        <?php echo $this->elements['Login']->toHtml();?>
                        <label for="passwlogin">Mot de passe</label>
                        <?php echo $this->elements['Password']->toHtml();?>
                        <input type="submit" value="OK" />                    
                    </form>
                    <ul>
                    	<li><a href="#" onclick="forgetPassword(); return false;">Mot de passe oubli&eacute; ?</a></li>
                    	<li><a href="/catalog/register.php">Cr&eacute;er son compte client</a></li>
					</ul>
                  <?php }?>
                    <a onclick="hidelogin();" title="Fermer" class="close" href="javascript:;"></a>
                </div>
                <ul>
                    <li><a href="/catalog/contact.php">Contact</a></li>
                    <li><a id="mylogin" href="javascript:;">Mon compte</a></li>
                    <li><a href="<?php echo htmlspecialchars($t->start_url);?>/catalog/basket.php">Mon panier <small class="BasketItemCount">
                    <?php if ($this->options['strict'] || (isset($t->basket) && method_exists($t->basket, 'getItemCount'))) if ($t->basket->getItemCount()) { ?>
                                        (<?php if ($this->options['strict'] || (isset($t->basket) && method_exists($t->basket, 'getItemCount'))) echo htmlspecialchars($t->basket->getItemCount());?>)
                    <?php } else {?>
                    	0
                    <?php }?>                    
                                        </small></a></li>
                </ul>
               
			</div>
            <form id="SearchForm" action="/catalog/search.php" method="post">
                <label for="search-text">Rechercher...</label>
                <?php 
if (!isset($this->elements['search_product']->attributes['value'])) {
    $this->elements['search_product']->attributes['value'] = '';
    $this->elements['search_product']->attributes['value'] .=  htmlspecialchars($t->search_value);
}
$_attributes_used = array('value');
echo $this->elements['search_product']->toHtml();
if (isset($_attributes_used)) {  foreach($_attributes_used as $_a) {
    unset($this->elements['search_product']->attributes[$_a]);
}}
?>		
		<?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'GetBrand'))) echo $t->head->GetBrand();?>
		<?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'GetJobs'))) echo $t->head->GetJobs();?>
                <?php echo $this->elements['idcategoryname']->toHtml();?>
                <?php echo $this->elements['type']->toHtml();?>
                <?php echo $this->elements['page']->toHtml();?>
                <?php echo $this->elements['resultPerPage']->toHtml();?>
                <input type="submit" title="Rechercher" value="Rechercher" />
            </form>
            <span class="clear"></span>
		</div>		
			 <?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('menu_alt.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
			<div id="main">
                <div style="width: auto; min-height: 0px; height: auto; top: 30%; left: 35%; z-index: 1002; background: none repeat scroll 0px 0px rgb(255, 255, 255); display: none; padding: 5px; border-radius: 5px;" id="forgetPassword" class="">
                    <p class="txt-oubl" style="float:left">Mot de passe oubli&eacute;?</p>
                    <p class="txt-clos" style="float:right; cursor: pointer;"><a onclick="closeBTN();">close</a></p>
                    <div style="clear:both" id="EmailResponse">
                        <p class="blocTextNormal">Saisissez votre adresse e-mail pour r&eacute;cup&eacute;rer vos identifiants</p>
                        <?php echo $this->elements['Email']->toHtml();?>				<?php echo $this->elements['Envoyer']->toHtml();?>			</div>
                </div>