﻿<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('header.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
<!--====================================== Contenu ======================================-->
	<div id="center" class="page home">  
	
<b><b><?php if ($this->options['strict'] || (isset($t->head) && method_exists($t->head, 'displayLink'))) if ($t->head->displayLink()) { ?><span style="color:red;font-weight: bold;">/index.tpl</span><?php }?>
	
       
<script type="text/javascript">
            $('#slider').layerSlider({
				skin : 'lsskin',
				skinsPath : 'templates/catalog/images/home/',
				twoWaySlideshow : false,
				thumbnailNavigation : 'disabled',
                navPrevNext : true,
                navStartStop : false,
                navButtons : true,
				responsiveUnder : 960
            });
			
</script>
						<hr class="underblocks1-2">
         <div class="products">
		 Espace produits en promotions
                    	<ul class="list_product">
                         <?php if ($this->options['strict'] || (isset($t) && method_exists($t, 'getPromotions'))) if (!$t->getPromotions(1)) { ?>
                            <!--pas de promotion-->
                            <?php if ($this->options['strict'] || (is_array($t->getFocusProducts(6,1))  || is_object($t->getFocusProducts(6,1)))) foreach($t->getFocusProducts(6,1) as $focusProduct) {?><li itemtype="http://schema.org/Product" itemscope="" class="homelist">
                            <a itemprop="url" style="left: -29px;" href="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getURL'))) echo htmlspecialchars($focusProduct->getURL());?>" class="btn-det" title="Ajouter au panier"></a>
                                <a itemprop="url" href="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getURL'))) echo htmlspecialchars($focusProduct->getURL());?>">
                                        <span class="ruban ccoeur"></span>
                                        <img itemprop="image" width="150" height="150" alt="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'get'))) echo $focusProduct->get("name_1");?>" src="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getImageURI'))) echo htmlspecialchars($focusProduct->getImageURI(150));?>">
                                        <span itemprop="name" class="h2"><?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'get'))) echo $focusProduct->get("name_1");?></span>
                                        <span class="price">
                                            <small>&agrave; partir de</small><br>
                                            <?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getLowerPriceET'))) if ($focusProduct->getLowerPriceET()) { ?>
                                                <span class="strong"><?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getLowerPriceET'))) echo $focusProduct->getLowerPriceET();?> <small>HT</small></span>
                                            <?php }?>
                                        </span>
                                    </a>
                            </li><?php }?> 
                        <?php } else {?>
                            <?php if ($this->options['strict'] || (is_array($t->getPromotions(3))  || is_object($t->getPromotions(3)))) foreach($t->getPromotions(3) as $promoArticle) {?><li class="homelist" itemtype="http://schema.org/Product" itemscope="">
                            <a itemprop="url" class="btn-det" style="left: -29px;" href="<?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getURL'))) echo htmlspecialchars($promoArticle->getURL());?>" title="Ajouter au panier">
                            	Ajout au panier
                            </a>
                            <a itemprop="url" href="<?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getURL'))) echo htmlspecialchars($promoArticle->getURL());?>">
                            	<span class="ruban promo"><span>-<?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getDiscountRate'))) echo $promoArticle->getDiscountRate();?></span></span>
                                <img src="<?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getImageURI'))) echo htmlspecialchars($promoArticle->getImageURI(150));?>" width="150" height="150" alt="<?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'get'))) echo $promoArticle->get("summary_1");?>" title="<?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'get'))) echo htmlspecialchars($promoArticle->get("summary_1"));?>" itemprop="image">
                                <span itemprop="name" class="h2"><?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'get'))) echo $promoArticle->get("summary_1");?></span>
                                <span class="price">
                                            <small>&agrave; partir de</small><br>
                                            <?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getDiscountPriceET'))) if ($promoArticle->getDiscountPriceET()) { ?>
                                                <?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getDiscountRate'))) if ($promoArticle->getDiscountRate()) { ?>
                                                    <del><?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getCeilingPriceET'))) echo $promoArticle->getCeilingPriceET();?> <small>HT</small></del><br>
                                                 <?php }?>
                                                    <span class="strong"><?php if ($this->options['strict'] || (isset($promoArticle) && method_exists($promoArticle, 'getDiscountPriceET'))) echo $promoArticle->getDiscountPriceET();?><small> HT</small></span>
                                            <?php }?>
                                        </span>
                                
                                
                            </a>                               
                            </li><?php }?>
                            <?php if ($this->options['strict'] || (is_array($t->getFocusProducts(3,1))  || is_object($t->getFocusProducts(3,1)))) foreach($t->getFocusProducts(3,1) as $focusProduct) {?><li itemtype="http://schema.org/Product" itemscope="" class="homelist">
                            	<a itemprop="url" style="left: -29px;" href="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getURL'))) echo htmlspecialchars($focusProduct->getURL());?>" class="btn-det" title="Ajouter au panier"><img width="60" alt="Ajouter au panier" height="60" src="/templates/catalog/images/list-btn-detail.png"></a>
                                <a itemprop="url" href="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getURL'))) echo htmlspecialchars($focusProduct->getURL());?>">
	                                    <span class="ruban ccoeur"></span>
                                        <img itemprop="image" width="150" height="150" alt="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'get'))) echo $focusProduct->get("name_1");?>" src="<?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getImageURI'))) echo htmlspecialchars($focusProduct->getImageURI(150));?>">
                                        <span itemprop="name" class="h2"><?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'get'))) echo $focusProduct->get("name_1");?></span>
                                        <span class="price">
                                            <small>&agrave; partir de</small><br>
                                            <?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getLowerPriceET'))) if ($focusProduct->getLowerPriceET()) { ?>
                                                <span class="strong"><?php if ($this->options['strict'] || (isset($focusProduct) && method_exists($focusProduct, 'getLowerPriceET'))) echo $focusProduct->getLowerPriceET();?> <small>HT</small></span>
                                        	<?php }?>
                                        </span>
                                    </a>
                            </li><?php }?>
                        <?php }?> 
               			 </ul>
                <div class="clear"></div>
            <script type="text/javascript">
				$(document).ready(function(){
					$(".rate").each(function(index) {
						var discountFloat = $(this).text().trim();
						var discountInt = discountFloat.replace(/,[0-9]*\s%/g, "%");
						$(this).html(discountInt);
					});	
				});
							$('ul.listprods li').mouseover(function(e) {
								$(this).find('a.btn-det').stop().animate({left:43}, 200, "easeInOutCirc");
							})
							$('ul.listprods li').mouseout(function(e) {
								$(this).find('a.btn-det').stop().animate({left:-29}, 200, "easeInOutCirc");
							})
			</script>
        </div>
        

                       
        <div id="arbo">
                <h2 class="mob-title"><span><strong>...</strong>, spécialiste de la vente .....</span></h2>
				<h2 class="desk-title"><span>D&eacute;couvrez <em>l'ensemble de la gamme</em> <strong>.....</strong></span></h2>
                <?php if ($this->options['strict'] || (isset($t) && method_exists($t, 'getListCateg2'))) echo $t->getListCateg2();?>
      </div>
           </div>
        </div><!-- Fin main -->
        
		
	<!--====================================== Fin du contenu ======================================-->
<?php 
$x = new HTML_Template_Flexy($this->options);
$x->compile('foot.tpl');
$_t = function_exists('clone') ? clone($t) : $t;
foreach(array()  as $k) {
    if ($k != 't') { $_t->$k = $$k; }
}
$x->outputObject($_t, $this->elements);
?>
</div><!-- Fin content -->