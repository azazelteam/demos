<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf commandes
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFOrder.php" );

$type = "order_form";
$odf = new ODFOrder( $_REQUEST[ "idorder" ] , $type );

$odf->exportPDF( "Bon de commande n° " . $_REQUEST[ "idorder" ]  );

?>