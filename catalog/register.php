<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Liste des produits par métier
 * Enregistrement d'un nouvel acheteur
 */

include_once( dirname( __FILE__ ) . '/../config/init.php' ); 
include_once( dirname( __FILE__ ) . '/../objects/Customer.php' );
include_once( dirname( __FILE__ ) . '/../script/global.fct.php' );
include_once( dirname( __FILE__ ) . '/../objects/flexy/PageController.php' );
include_once( dirname( __FILE__ ) . '/../objects/Util.php' );

/* --------------------------------------------------------------------------------------------------- */
/* multisite : redirection pour les sites satellites */


if( file_exists( dirname( __FILE__ ) . "/../config/multisite.xml" ) ){

	header( "Location: /catalog/order_multisite.php" );
	exit();
	
}

/* --------------------------------------------------------------------------------------------------- */
//Traitement du formulaire
if( isset( $_POST[ 'Register' ] ) || isset( $_POST[ 'Register_x' ] ) ){
	
	include_once( dirname( __FILE__ ) . "/registration_parseform.php" );

	if (!empty($error_msg )){}
	elseif( isset($_POST['redirectTo']) ) {
		header( "Location: /catalog/" . $_POST['redirectTo'] );
		exit;
	}
	else{
		exit( $error_msg );	
	}
	
}

class IndexController extends PageController{};
$register_page = new IndexController( 'register.tpl' );
$register_page->setData( 'title' , 'Enregistrement' );

Basket::getInstance()->removeArticle0Quantity();
$orderInProgress = Basket::getInstance()->getItemCount() > 0;



if( !Session::getInstance()->getCustomer() ) {
	if(isset($_POST["Login"])) {
		$register_page->setData('issetPostLoginAndNotLogged', "ok");
	}
} else {
	
	$register_page->setData('isLogin', "ok");
	
	header( "Location:$GLOBAL_START_URL/catalog/account.php");
}


if( $orderInProgress && !isset( $_SESSION['FromAccountPage'] ) )
{
	$register_page->setData('orderInProgress', 'ok' );
	$register_page->setData('registerSubmitValue', 'Etape suivante' );
	$register_page->setData('hideBottomBar', 1 );
}
else{
	unset( $_SESSION['FromAccountPage'] );
	$register_page->setData('orderInProgress', '' );
	$register_page->setData('registerSubmitValue', 'S\'enregistrer' );
}
$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_estimate_conversion_id = DBUtil::getParameterAdmin( "google_estimate_conversion_id" );

$idUser = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;

if( $idUser == 0 ) {
	if(isset( $_SESSION['issetPostLoginAndNotLogged'] )) {
		$register_page->setData('issetPostLoginAndNotLogged', "ok");
		unset( $_SESSION['issetPostLoginAndNotLogged'] );
	}else{
		$register_page->setData('issetPostLoginAndNotLogged', "");	
	}
}

if( isset($error_msg) ){
	$register_page->setData('errorInscription', $error_msg );
}else{
	$register_page->setData('errorInscription', '' );
}

$register_page->setData( 'UseGoogleConversion' , $UseGoogleConversion );
$register_page->setData( 'google_estimate_conversion_id' , $google_estimate_conversion_id );

if( isset( $_POST[ 'email' ] ) )
	$register_page->setData( 'userInfoEmail' , $_POST[ 'email' ] );
	
if( isset( $_POST[ 'pwd' ] ) )
	$register_page->setData( 'userInfoPwd' , $_POST[ 'pwd' ] );

	//chèque-cadeau
$register_page->setData( "voucher_code", isset( $_REQUEST[ "voucher_code" ] ) ? urlencode( stripslashes( $_REQUEST[ "voucher_code" ] ) ) : "" );

//bon de réduction
$register_page->setData( "gift_token_serial", isset( $_REQUEST[ "gift_token_serial" ] ) ? urlencode( stripslashes( $_REQUEST[ "gift_token_serial" ] ) ) : "" );

//loginfail
if (isset($_GET["loginfail"]))
	$register_page->setData("loginfail", 1);

$register_page->output();

?>