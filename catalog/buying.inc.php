<?php
 /**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement de la 2ème étape de la commande
 * Page d'enregistrement de l'acheteur dans la commande
 */
 


	/* protection XSS */
	
	include_once( "$GLOBAL_START_PATH/objects/Util.php" );
	
	$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );
		
	$iddelivery = 0;
	if( isset($_POST['iddelivery']) ) $iddelivery = $_POST['iddelivery'];
	$idbilling = 0;
	if( isset($_POST['idbilling']) ) $idbilling = $_POST['idbilling'];
	$delivery_different = '';
	$billing_different = '';
	
	$Buyer = &Session::getInstance()->getDeprecatedBuyer();
	if( isset($_POST['Modify']) or isset($_POST['Modify_x']))
	{
	   Session::getInstance()->getDeprecatedBuyer()->UpdateBuyer();
	}
	if( !empty($_POST['Lastname']) && !empty($_POST['Adress'])
	 && !empty($_POST['Zipcode']) && !empty($_POST['City']) ) {
		$delivery = $_POST;
	}
	if( isset($_POST['Save']) or isset($_POST['Save_x']) )
	{
		if( !empty($_POST['delivery_checkbox']) && !empty($_POST['delivery']) ) {
			$delivery = $_POST['delivery'];
		}
		if( !empty($_POST['billing_checkbox']) && !empty($_POST['billing']) ) {
			$billing = $_POST['billing'];
		}
	}
	if( !empty($delivery) ) {
		if( empty($iddelivery) ) {
			$iddelivery = $Buyer->UseDelivery($delivery);
		}
		else {
			$iddelivery = $Buyer->UpdateDelivery($iddelivery,$delivery);
		}
	}
	if( !empty($billing) ) {
		if( empty($idbilling) ) {
			$idbilling = $Buyer->UseBilling($billing);
		}
		else {
			$idbilling = $Buyer->UpdateBilling($idbilling,$billing);
		}
	}
	//Basket::getInstance()->UpdateBasketBuyer(); mon dieu!
	if($iddelivery > 0) {
		$Buyer->GetDeliveryInfo($iddelivery);
		$delivery_different = ' checked="checked"';
	}
	if($idbilling > 0) {
		$Buyer->GetBillingInfo($idbilling);
		$billing_different = ' checked="checked"';
	}
	Basket::getInstance()->set( "iddelivery", $iddelivery );
	Basket::getInstance()->set( "idbilling_adress", $idbilling);

?>