<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */

header("HTTP/1.0 204 No Response");

include_once('../objects/classes.php');  

if( isset($_POST['IdProduct']) )
{
	$IdProduct = $_POST['IdProduct'];
	if( !isset($_SESSION['comproducts']) ) $_SESSION['comproducts']=array();
	if( isset($_POST['incompare']) )
	{
		$tmp = $_SESSION['comproducts'];
		$tmp[] = $IdProduct;
		$_SESSION['comproducts'] = array_unique($tmp);
	}
	else
	{ 
		$key = array_search( $IdProduct,$_SESSION['comproducts'] ) ;
		if($key === FALSE) ;
		else
		{
			unset($_SESSION['comproducts'][$key]);  
			$x= implode(';', $_SESSION['comproducts']); 
			if($x) $_SESSION['comproducts']= explode(';',$x); else $_SESSION['comproducts']=array();
		}
	}
}


?>