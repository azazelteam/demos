<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affiche le logo dans les mails envoyé au client pour les devis/commande et qui permet de savoir si le mail a été visualisé ou non
 */
 
 include_once( "../objects/classes.php" );
 
 if( isset( $_GET[ "idestimate" ] ) ){
 	
 	$table = "estimate";
 	$primaryKey = "idestimate";
 	$id = intval( $_GET[ "idestimate" ] );
 	
 }
 else if( isset( $_GET[ "idorder" ] ) ){
 	
 	$table = "order";
 	$primaryKey = "idorder";
 	$id = intval( $_GET[ "idorder" ] );
 	
 }
 
 /*
  * seen = champ binaire
  * 0 = non lu
  * 1 = devis/commande lu
  * 2 = confirmation devis/commande lu
  * 
  * ex : 3 = 1 OR 2 = devis/commande lu ET confirmation devis/commande lus
  */
  
 $seen = intval( $_GET[ "seen" ] );
 
 $db = &DBUtil::getConnection();
 $query = "UPDATE `$table` SET seen = seen | $seen WHERE $primaryKey = $id LIMIT 1";
 $rs = $db->Execute( $query );

 //affichage du logo
 
 $logo = DBUtil::getParameterAdmin( "ad_logo" );
 // header( "Content-Type: image/gif" );
 header( "Content-Type: image/jpeg" );
 readfile( $logo );
?>