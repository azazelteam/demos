<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf comparateur dans un mail
 */


include_once('../config/init.php');
include_once("../objects/mime_mail/htmlMimeMail.php");


$mailer = new htmlMimeMail();

$mailer->setFrom( '"'.$_POST["nom"].'" <'.$_POST["email"].'>');

if( isset( $_POST[ "bcc" ] ) )
	$mailer->setCc( $_POST[ "email" ] );
	
$mailer->setSubject("Comparatif produit"); 



$message = nl2br( stripslashes( $_POST[ "message" ] ) );
$mailer->setHtml($message);

$mailer->addAttachment( file_get_contents( URLFactory::getHostURL() . "/catalog/pdf_compare.php" ),'comparatif.pdf','application/pdf');

$result = $mailer->send(array($_POST["emaildest"])); 


if( !$result ) { 
	@header("Location: ./send_comparatif.php?send=0");
}else{
	@header("Location: ./send_comparatif.php?send=1");
}

?>