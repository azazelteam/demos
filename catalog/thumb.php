<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des photos catégories et produits
 */

include_once( "../config/init.php" );
include_once( "../objects/Thumbnail.php" );

/* -------------------------------------------------------------------------------------- */

$shortcuts = array(

	"category" 	=> "categories",
	"product" 	=> "products",
	"supplier" 	=> "suppliers",
	"provider" 	=> "providers"
	
);

foreach( $shortcuts as $type => $directory )
	if( isset( $_GET[ $type ] ) ){

		outputImage( $directory, $_GET[ $type ], isset( $_GET[ "w" ] ) ? intval( $_GET[ "w" ] ) : 0, isset( $_GET[ "h" ] ) ? intval( $_GET[ "h" ] ) : 0 );
		exit();
		
	}

/* -------------------------------------------------------------------------------------- */

Thumbnail::createThumb(

	$GLOBAL_START_PATH . $_GET[ "src" ], 
	isset( $_GET[ "w" ] ) ? intval( $_GET[ "w" ] ) : 0, 
	isset( $_GET[ "h" ] ) ? intval( $_GET[ "h" ] ) :0
	
);

/* -------------------------------------------------------------------------------------- */

function outputImage( $directory, $basename, $width = 0, $height = 0 ){
	
	if( !( $files = glob( dirname( __FILE__ ) . "/../images/$directory/$basename{.jpg,.jpeg,.png,.gif}", GLOB_BRACE ) ) ){
		
		 Thumbnail::createThumb( 

			dirname( __FILE__ ) . "/../images/{$directory}/default.jpg", 
			$width, 
			$height
			
		);
	
		exit();
		
	}
	
	$file 	= basename( $files[ 0 ] );
//die(dirname( __FILE__ ) . "/../images/{$directory}/{$file}");
	Thumbnail::createThumb( 

		dirname( __FILE__ ) . "/../images/{$directory}/{$file}", 
		$width, 
		$height
		
	);
	
}


?>