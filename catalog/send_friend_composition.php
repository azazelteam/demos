<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi d'une composition à un pote
*/

include_once ("../objects/classes.php");
include_once( "Validate.php" ); //classe PEAR
include_once( "Validate/FR.php" ); //classe PEAR

if ( !isset($_REQUEST["IdComposition"]) ){
	header("location: $GLOBAL_START_URL");
}

include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );

class ContactController extends WebPageController{};
$contact_template = new ContactController( "send_friend_composition.tpl" , "Envoi d'une composition à un ami" );

$ScriptName = "send_friend.php";
$IdComposition = $_REQUEST["IdComposition"];
$contact_template->setData("IdComposition",$IdComposition);

if (Session::getInstance()->islogin() > 0) {
	$contact_template->setData("isLogin", "ok");	
}else{
	$contact_template->setData("isLogin", "");
}

if ( (isset ($_POST["emailReciever"]) && $_POST["emailReciever"]!="" && Validate::email($_POST["emailReciever"])) && (Session::getInstance()->islogin() || (isset ($_POST["emailSender"]) && $_POST["emailSender"]!="" && Validate::email($_POST["emailSender"])) ) ){
	
	include_once ("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	require_once ("HTML/Template/Flexy.php");

	// infos de l'emetteur
	$senderFirstname = $_POST["firstnameSender"];
	$senderLastname = $_POST["lastnameSender"];
	$senderEmail = $_POST["emailSender"];
	
	// infos du receveur
	$recieverFirstname = $_POST["firstnameReciever"];
	$recieverLastname = $_POST["lastnameReciever"];
	$recieverEmail = $_POST["emailReciever"];

	// compilation du template du mail a envoyer au pote de l'émetteur
	$options['templateDir'][] = "$GLOBAL_START_PATH/templates/catalog/";
	$mail_copy = new HTML_Template_Flexy($options);
	
	$mail_copy->setData("firstnameSender",$senderFirstname);
	$mail_copy->setData("lastnameSender",$senderLastname);
	$mail_copy->setData("emailSender",$senderEmail);
	
	$mail_copy->setData("firstnameReciever",$recieverFirstname);
	$mail_copy->setData("lastnameReciever",$recieverLastname);
	$mail_copy->setData("emailReciever",$recieverEmail);
	
	$mail_copy->setData("commentSender",$_POST["commentSender"]);
		
	$rscompote = DBUtil::query("SELECT idnomenclature, name, photo, anim FROM nomenclature WHERE idnomenclature=$IdComposition");
	
	// photo de la composition
	if($rscompote->fields("photo")){
		$compoImageP = str_replace("../",$GLOBAL_START_PATH."/",$rscompote->fields("photo"));
	}else{
		$compoImageP = "default";
	}
	if(file_exists($compoImageP)){
		$compoImage = str_replace("../",$GLOBAL_START_URL."/",$rscompote->fields("photo"));
	}else{
		$compoImage = "";
	}
	
	$compositionName = $rscompote->fields("name");
	$compositionUrl = $GLOBAL_START_URL."/catalog/composition.php?composition=".$IdComposition;
	$compositionImage = $compoImage;
		
	$mail_copy->setData("compositionName",$compositionName);
	$mail_copy->setData("compositionUrl",$compositionUrl);
	$mail_copy->setData("compositionImage",$compositionImage);
	
	$mail_copy->compile("send_friend_composition_mail.tpl");
	$data = $mail_copy->toString();

	// on met le tout en boite et roule ma poule
	$email = new htmlMimeMail();
	$email->setFrom($senderEmail);
	$email->setSubject( "Un ami vous invite à consulter une composition" );
	$email->setHtml(stripslashes($data));
	
	$sendResult = $email->send(array($recieverEmail));
	
	if($sendResult==false){
		$contact_template->setData("errorSend","ok");
	}else{
		$contact_template->setData("errorSend","");
	}
	$contact_template->setData("errorEmailSender","");
	$contact_template->setData("errorEmailReciever","");
}else{
	$contact_template->setData("errorSend","");
	
	if(!Session::getInstance()->islogin() && (!isset ($_POST["emailSender"]) || $_POST["emailSender"]=="" || !Validate::email($_POST["emailSender"]))){
		$contact_template->setData("errorEmailSender","ok");
	}else{
		$contact_template->setData("errorEmailSender","");
	}
	
	if(isset ($_POST["emailReciever"]) && ( $_POST["emailReciever"]=="" || !Validate::email($_POST["emailReciever"]) ) ){
		$contact_template->setData("errorEmailReciever","ok");
	}else{
		$contact_template->setData("errorEmailReciever","");
	}
	
}

$contact_template->output();

?>