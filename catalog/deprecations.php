<?php

/**
 * ==================================================
 * /catalog/club.php
 * ==================================================
 * ajout de {grouping} stdclass groupement
 * ajout de {articles} array<CArticleProxy> références préférées
 * 
 * ==================================================
 * /catalog/confirm_order.php
 * ==================================================
 * {basket_articles} supprimé, préférer {order.getArticles()}
 * {buyer_company} supprimés, préférer {customer.get(#...#)}
 * {buyer_title} supprimés, préférer {customer.get(#...#)}
 * {buyer_firstname} supprimés, préférer {customer.get(#...#)}
 * {buyer_lastname} supprimés, préférer {customer.get(#...#)}
 * {buyer_adress} supprimés, préférer {customer.get(#...#)}
 * {buyer_adress_2} supprimés, préférer {customer.get(#...#)}
 * {d_buyer_company} supprimé, préférer {forwardingAddress}
 * {d_buyer_title} supprimé, préférer {forwardingAddress}
 * {d_buyer_firstname} supprimé, préférer {forwardingAddress}
 * {d_buyer_lastname} supprimé, préférer {forwardingAddress}
 * {d_buyer_adress} supprimé, préférer {forwardingAddress}
 * {d_buyer_adress_2} supprimé, préférer {forwardingAddress}
 * {d_buyer_zipcode} supprimé, préférer {forwardingAddress}
 * {d_buyer_city} supprimé, préférer {forwardingAddress}
 * {b_buyer_company} supprimé, préférer {invoiceAddress}
 * {b_buyer_title} supprimé, préférer {invoiceAddress}
 * {b_buyer_firstname} supprimé, préférer {invoiceAddress}
 * {b_buyer_lastname} supprimé, préférer {invoiceAddress}
 * {b_buyer_adress} supprimé, préférer {invoiceAddress}
 * {b_buyer_adress_2} supprimé, préférer {invoiceAddress}
 * {b_buyer_zipcode} supprimé, préférer {invoiceAddress}
 * {b_buyer_city} supprimé, préférer {invoiceAddress}
 * 
 * ==================================================
 * /catalog/estimate_basket.php
 * ==================================================
 * {buyer} supprimé, préferer {customer}
 * {basket_articles} supprimé, préférer {basket.getArticles()}
 * {formtranslations} supprimé, inutile
 * 
 * ==================================================
 * /catalog/finish.php
 * ==================================================
 * {idorder} supprimé, préférer {order.getId()}
 * {is_estimate} supprimé, inutile
 * {n_estimate} supprimé, inutile
 * {isLogin} supprimé, préférer {customer}
 * {buyer_company} supprimé, préférer {customer}
 * {buyer_title} supprimé, préférer {customer}
 * {buyer_firstname} supprimé, préférer {customer}
 * {buyer_lastname} supprimé, préférer {customer}
 * {buyer_adress} supprimé, préférer {customer}
 * {buyer_adress_2} supprimé, préférer {customer}
 * {buyer_zipcode} supprimé, préférer {customer}
 * {buyer_city} supprimé, préférer {customer}
 * {d_buyer_company} supprimé, préférer {forwardingAddress}
 * {d_buyer_title} supprimé, préférer {forwardingAddress}
 * {d_buyer_firstname} supprimé, préférer {forwardingAddress}
 * {d_buyer_lastname} supprimé, préférer {forwardingAddress}
 * {d_buyer_adress} supprimé, préférer {forwardingAddress}
 * {d_buyer_adress_2} supprimé, préférer {forwardingAddress}
 * {d_buyer_zipcode} supprimé, préférer {forwardingAddress}
 * {d_buyer_city} supprimé, préférer {forwardingAddress}
 * {b_buyer_company} supprimé, préférer {invoiceAddress}
 * {b_buyer_title} supprimé, préférer {invoiceAddress}
 * {b_buyer_firstname} supprimé, préférer {invoiceAddress}
 * {b_buyer_lastname} supprimé, préférer {invoiceAddress}
 * {b_buyer_adress} supprimé, préférer {invoiceAddress}
 * {b_buyer_adress_2} supprimé, préférer {invoiceAddress}
 * {b_buyer_zipcode} supprimé, préférer {invoiceAddress}
 * {b_buyer_city} supprimé, préférer {invoiceAddress}
 * {remise_rate} supprimé, préférer {order.get...()}
 * {total_discount} supprimé, préférer {order.get...()}
 * {discount} supprimé, préférer {order.get...()}
 * {total_discount_amount} supprimé, préférer {order.get...()}
 * {total_amount_ht} supprimé, préférer {order.get...()}
 * {total_amount_ht_wb} supprimé, préférer {order.get...()}
 * {total_amount_ttc_wbi} supprimé, préférer {order.get...()}
 * {total_amount_ht_avant_remise} supprimé, préférer {order.get...()}
 * {remise} supprimé, préférer {order.get...()}
 * {net_to_pay} supprimé, préférer {order.get...()}
 * {total_charge_ht} supprimé, préférer {order.get...()}
 * {total_charge_ttc} supprimé, préférer {order.get...()}
 * {basket_articles} supprimé, préférer {order.getArticles()}
 * {tracke} supprimé, inutile
 * {tracker_idorder} supprimé, inutile
 * {tracker_ht} supprimé, inutile
 * 
 * ==================================================
 * /catalog/grouping_catalog
 * ==================================================
 * fichier supprimé, préférer club.php
 * 
 * ==================================================
 * /catalog/last_consultation.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /catalog/list_buyergroup.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /catalog/mail_product.php
 * ==================================================
 * {thumbURL} supprimé, préférer {product.getImage()}
 * {productName} supprimé, préférer {product.get(#name_1#)}
 * {buyer} supprimé, préférer {customer}
 * {translations} supprimé, inutile
 * 
 * ==================================================
 * /catalog/order_direct.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /catalog/pay_secure.php
 * ==================================================
 * {isLogin} supprimé, préférer {customer}
 * 
 * ==================================================
 * /catalog/popup_login.php
 * ==================================================
 * {isLogin} supprimé, préférer {customer}
 * 
 * ==================================================
 * /catalog/product_brand.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /catalog/product_filtered_elements.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /catalog/product_novelty.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /catalog/product_trade.php
 * ==================================================
 * ajout de {products} array<CProductProxy>
 * 
 * ==================================================
 * /catalog/promo_cat.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /catalog/promotions.php
 * ==================================================
 * ajout de {categories} array<stdclass> ( cf. table promo_cat pour les propriétés )
 * ajout de {category} stdclass ( cf. table promo_cat pour les propriétés ) ou NULL
 * ajout de {articles} array<CArticleProxy>
 * {isProducts} supprimé, préférer {if:articles}...{else:}...{end:}
 * {promo_cat_name} supprimé, inutile
 * {error_msg} supprimé, inutile
 * {descriptions} supprimé, inutile
 * {promotions_menu} supprimé, préférer {categories}
 * {promo_cat_name} supprimé, préférer {category}
 * {idpromo_cat} supprimé, préférer {category}
 * 
 * ==================================================
 * /catalog/recap.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /catalog/send_comparatif.php
 * ==================================================
 * {buyerEmail} supprimé, préférer {customer}
 * {buyerName} supprimé, préférer {customer}
 * {buyerID} supprimé, préférer {customer}
 * 
 * ==================================================
 * /catalog/send_friend_composition.php
 * ==================================================
 * fichier supprimé
 * 
 * ==================================================
 * /catalog/send_friend.php
 * ==================================================
 * ( note : 2 templates utilisés : send_friend.tpl et send_friend_mail.tpl )
 * {isLogin} supprimé, préférer {customer}
 * ajout de {product} CProductProxy ( send_friend_mail.tpl uniquement )
 * {productName} supprimé, préférer {product}
 * {productUrl} supprimé, préférer {product}
 * {productImage} supprimé, préférer {product}
 * {productDescription} supprimé, préférer {product}
 * {productBrand} supprimé, préférer {product}
 * 
 * ==================================================
 * /catalog/send_to_friend.php
 * ==================================================
 * fichier supprimé
 * 
 * ==================================================
 * /catalog/sponsorship.php
 * ==================================================
 * fichier supprimé
 * 
 * ==================================================
 * /catalog/stars_products.php
 * ==================================================
 * fichier supprimé
 * 
 * ==================================================
 * /catalog/stat_buyergroup.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /catalog/stat_paiement_securise.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /www/stat_page_agent_usine.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /www/stat_page_fournisseur.php
 * ==================================================
 * rien à signaler
 *  
 * ==================================================
 * /www/stat_page_nos_differences.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /www/stat_page_agent_usine.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /www/stat_page_nos_refs.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /www/stat_page_sur_mesure.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /www/cabine.php
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ==================================================
 * /www/shopping-marques.php  supprimé, inutile
 * ==================================================
 * fichier supprimé, inutile
 * 
 * ===================
 * /catalog/stat_reference_list
 * ===================
 * rien à signaler
 * 
 * ==================================================
 * /catalog/stat_reference.php
 * ==================================================
 * rien à signaler
 * 
 * ==================================================
 * /catalog/survey.php
 * ==================================================
 * {idBuyer} supprimé, préférer {customer}
 * {type} supprimé, préférer {customer}
 * {title} supprimé, préférer {customer}
 * {lastname} supprimé, préférer {customer}
 * {firstname} supprimé, préférer {customer}
 * {company} supprimé, préférer {customer}
 * {phonenumber} supprimé, préférer {customer}
 * {faxnumber} supprimé, préférer {customer}
 * {email} supprimé, préférer {customer}
 * 
 * ===========================================================================
 * /product_management/catalog/custom_pages.php
 * ===========================================================================
 * ( Note : en plus de mettre à jour vos templates, vous devez également regénérer toutes vos pages 'statiques' depuis le back office )
 * rien à signaler
 * 
 */

/* tempo WebPageController -> PageController */

if( file_exists( dirname( __FILE__ ) . "/../deprecations.txt" ) ){
	
	$files = file( dirname( __FILE__ ) . "/../deprecations.txt", FILE_IGNORE_NEW_LINES );
	
	if( in_array( basename( $_SERVER[ "PHP_SELF" ] ), $files )
		&& file_exists( dirname( __FILE__ ) . "/deprecated_" . basename( $_SERVER[ "PHP_SELF" ] ) ) ){
		
		include( dirname( __FILE__ ) . "/deprecated_" . basename( $_SERVER[ "PHP_SELF" ] ) );
		exit();
			
	}
	
}

?>