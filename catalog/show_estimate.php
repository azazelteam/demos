<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Script d'affichage des devis par l'acheteur
*/

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Order.php"); 
include_once( dirname( __FILE__ ) . "/../objects/Payment.php");
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");
include_once( dirname( __FILE__ ) . "/../lib/buyer.php" );

// Traitement pour le paiement depuis le devis hop bisou1
if( isset( $_POST['IdEstimate'] ) && isset( $_POST['OKCB'] ) ){
	
	$idestimate = $_POST['IdEstimate'];
	
	$estimate = new Estimate( intval( $idestimate ) );
	
	$estimate->set( "status", "Ordered" );
	$estimate->save();

	User::fakeAuthentification();
	$order = TradeFactory::createOrderFromEstimate( $estimate, "ToDo" );
	
	$idorder = $order->getId();
	$idpayment = Payment::$PAYMENT_MOD_DEBIT_CARD;
	
	$order->set( "idpayment", intval( $idpayment ) );
	$order->save();	
	
	$_SESSION["idorder_secure"] = $order->getId();
	
	header("Location: $GLOBAL_START_URL/catalog/pay_secure.php");

	exit();
}


class IndexController extends PageController{};
$show_estimate = new IndexController( "show_estimate.tpl", "Devis n°".$_REQUEST['IdEstimate'] );

// on test si un nouvelle adresse à été postée
if( isset( $_REQUEST['IdEstimate'] ) && ( isset( $_POST[ 'update_billing_adress' ] ) || isset( $_POST[ 'update_delivery_adress' ] ) || isset( $_POST[ 'update_billing_adress_x' ] ) || isset( $_POST[ 'update_delivery_adress_x' ] ) ) ){	

	// reste à enregister la nouvelle 
	$idestimate 	 		 = $_REQUEST[ "IdEstimate" ];
	$adress[ "Company" ] 	 = $_POST[ 'company' ];
	$adress[ "Firstname" ] 	 = $_POST[ 'firstname' ];
	$adress[ "Lastname" ]	 = $_POST[ 'lastname' ];
	$adress[ "Title" ]	 	 = $_POST[ 'title' ];
	$adress[ "Phonenumber" ] = $_POST[ 'phonenumber' ];
	$adress[ "Adress" ]	 	 = $_POST[ 'adress' ];
	$adress[ "Adress_2" ]	 = $_POST[ 'adress_2' ];
	$adress[ "Zipcode" ]	 = $_POST[ 'zipcode' ];
	$adress[ "City" ]		 = $_POST[ 'city' ];
	$adress[ "idstate" ]	 = $_POST[ 'idstate' ];
	$adress[ "Faxnumber" ]	 = "";
	
	$estimate = new Estimate( intval( $idestimate ) );
	
	if( isset( $_POST[ 'update_billing_adress' ] ) || isset( $_POST[ 'update_billing_adress_x' ] ) ){
		$idbilling = buyer_AddBillingAddress( $adress );
		$estimate->set( "idbilling_adress" , $idbilling );
	}else{
		$iddelivery = buyer_AddDeliveryAddress( $adress );
		$estimate->set( "iddelivery" , $iddelivery ); 
	}
	
	// et mettre à jour la commande
	$estimate->save();
	
}

$Base= &DBUtil::getConnection();

$TableName="estimate_row";

if( !isset($_REQUEST['IdEstimate']) ){

	exit( "Impossible de récupérer le numéro de devis" );
		
}

$lang = "_1";

//-----------------------------------------------------------------------------------
/************************* Show Estimate c'est partiiii ****************************/

$estimate = new Estimate(intval( $_REQUEST[ "IdEstimate" ] ));

//----------------------------------------------------------------------------------------------
/* proxy devis */

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/EstimateProxy.php" );
$show_estimate->setData( "estimate", new EstimateProxy( $estimate ) );

//----------------------------------------------------------------------------------------------

if(Session::getInstance()->getCustomer() ){
	
	$GroupingCatalog = new GroupingCatalog();
	
	$show_estimate->setData('logoUser', $estimate->getCustomer()->get("logo"));
	$show_estimate->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$show_estimate->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$show_estimate->setData( "contact_lastname", $salesman->get("lastname") );
	$show_estimate->setData( "contact_firstname", $salesman->get("firstname") );
	$show_estimate->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$show_estimate->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$show_estimate->setData( "contact_email", $salesman->get("email") );
	
	$show_estimate->setData( "isEstimate", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
		$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
		$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$show_estimate->setData( "contact_grouping", $contact_grouping );
}else{


	header( "Location:$GLOBAL_START_URL" );
	exit();
}

if(Session::getInstance()->getCustomer() && Session::getInstance()->getCustomer()->getId() == $estimate->getCustomer()->getId() ){
$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
		$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
		$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	if($contact_grouping!=1)
	{
		header( "Location:$GLOBAL_START_URL" );
	exit();
	}

	}

$Str_Status = $estimate->get('status');
switch ($Str_Status){
	case 'ToDo':
		{
			$show_estimate->setData('Str_Status_ToDo','ok');
			$show_estimate->setData('Str_Status_InProgress','');
			$show_estimate->setData('Str_Status_Send','');
			$show_estimate->setData('Str_Status_Ordered','');
			break;
		}
	case 'InProgress':
		{
			$show_estimate->setData('Str_Status_ToDo','');
			$show_estimate->setData('Str_Status_InProgress','ok');
			$show_estimate->setData('Str_Status_Send','');
			$show_estimate->setData('Str_Status_Ordered','');
			break;
		}
	case 'Send':
		{
			$show_estimate->setData('Str_Status_ToDo','');
			$show_estimate->setData('Str_Status_InProgress','');
			$show_estimate->setData('Str_Status_Send','ok');
			$show_estimate->setData('Str_Status_Ordered','');
			break;
		}
	case 'Ordered':
		{
			$show_estimate->setData('Str_Status_ToDo','');
			$show_estimate->setData('Str_Status_InProgress','');
			$show_estimate->setData('Str_Status_Send','');
			$show_estimate->setData('Str_Status_Ordered','ok');
			break;
		}
	default :
		{
			$show_estimate->setData('Str_Status_ToDo','');
			$show_estimate->setData('Str_Status_InProgress','');
			$show_estimate->setData('Str_Status_Send','');
			$show_estimate->setData('Str_Status_Ordered','');
			break;
		}
}


$dateEstimate=Util::doNothing(HumanReadableDateTime($estimate->get('DateHeure')));

$show_estimate->setData('DateEstimate',$dateEstimate);

$count = $estimate->getItemCount();
$hasLot = false;
$i = 0;
while( $i < $count && !$hasLot ){

	if( $estimate->getItemAt( $i)->get( "lot" ) > 1 )
		$hasLot = true;
		
	$i++;
		
}

if($hasLot){
	$show_estimate->setData('hasLot','ok');
	$show_estimate->setData('haslot','ok');
}else{
	$show_estimate->setData('hasLot','');
	$show_estimate->setData('haslot','');
}

if($estimate->get("valid_until")!='0000-00-00'){
	$dateValid=usDate2eu($estimate->get("valid_until"));
}else{
	$dateValid="";
}
$show_estimate->setData('dateValid',$dateValid);

$iddelivery = $estimate->get('iddelivery');
$idbilling = $estimate->get('idbilling_adress');

$n = $estimate->getItemCount();
$estimate_articles=array();
for ( $i=0 ; $i<$n ; $i++ ) {

	$estimate_articles[$i]["Icon"] = URLFactory::getReferenceImageURI( $estimate->getItemAt($i)->get('idarticle'), URLFactory::$IMAGE_ICON_SIZE );
    $estimate_articles[$i]["reference"] = $estimate->getItemAt($i)->get('reference');
    $estimate_articles[$i]["summary"] = Util::doNothing($estimate->getItemAt($i)->get('summary'));
    $estimate_articles[$i]["designation"] = getValueOrDefaultIfEmpty($estimate->getItemAt($i)->get('designation'),'&nbsp;');
	$estimate_articles[$i]["delivdelay"] = DBUtil::getDBValue( "delay_1", "delay", "iddelay", $estimate->getItemAt($i)->get('delivdelay') );
	$estimate_articles[$i]["unit_price"] = Util::priceFormat($estimate->getItemAt($i)->get('unit_price'));
	$estimate_articles[$i]["ref_discount"] = ($estimate->getItemAt($i)->get('ref_discount') > 0) ? Util::rateFormat($estimate->getItemAt($i)->get('ref_discount')) : "-" ;
	$estimate_articles[$i]["discount_price"] = Util::priceFormat($estimate->getItemAt($i)->get('discount_price'));     
	$estimate_articles[$i]["quantity"] = $estimate->getItemAt($i)->get('quantity');
	    
	if( $hasLot ){
		$estimate_articles[$i]["lot"] = $estimate->getItemAt($i)->get('lot')." / ".$estimate->getItemAt($i)->get('unit');
	}
    
    $ref_total_price = 	$estimate->getItemAt( $i )->get( "discount_price" ) * $estimate->getItemAt( $i)->get( "quantity" );
    $estimate_articles[$i]["total_price"] = Util::priceFormat( $ref_total_price );

	if($Str_Status=='Send'){
		$estimate_articles[$i]["total_discount_price"] = Util::priceFormat( $estimate->getItemAt( $i )->get( "discount_price" ) );
	}
}
$show_estimate->setData('estimate_articles',$estimate_articles);

$show_estimate->setData('PaymentDelay', Util::checkEncoding(DBUtil::getDBValue( "payment_delay_1", "payment_delay", "idpayment_delay",$estimate->get( "idpayment_delay" ) ) ));
$paymentmode=$estimate->get( "idpayment" );

$show_estimate->setData('IsEstimate','ok');

$query = "SELECT idpayment, name$lang FROM payment WHERE payment_code NOT LIKE 'GIFT_TOKEN'";

$rs = DBUtil::query( $query );

$i = 0;

$payment_array=array();
while( !$rs->EOF() ){
	
	$idpayment = $rs->fields("idpayment");
	$paymentName = $rs->fields("name$lang");
	
	$payment_array[$i]["idpayment"]=strval( $idpayment );
	$payment_array[$i]["paymentName"]=$paymentName;

	if($i==0 && !isset($_POST["idpayment"])){
		$payment_array[$i]["first_line"]='ok';
	}else{
		$payment_array[$i]["first_line"]='';
	}
	
	$rs->MoveNext();
	$i++;
	
}
if( isset($_POST["idpayment"] ) && $_POST["idpayment"] != 0 ){
	$show_estimate->createElement( 'idpayment[]' , $_POST["idpayment"] );
}else if($paymentmode && $paymentmode != 0 ){
	$show_estimate->createElement( 'idpayment[]' , $paymentmode );
}else{
	$show_estimate->createElement( 'idpayment[]' , $payment_array[$i-1]["idpayment"] );
}

$show_estimate->setData('IdEstimate',intval( $_REQUEST[ "IdEstimate" ] ));

$show_estimate->setData('payments',$payment_array);

//commentaires devis
$comment = $estimate->get('comment');
$show_estimate->setData('comment',$comment);
//commercial du devis
include("$GLOBAL_START_PATH/catalog/commercial.inc.php");
commercial($show_estimate);

/**
 * Enregistrement des adresses de livraison et de facturation
 */

$message="";
include_once("$GLOBAL_START_PATH/catalog/addresses.inc.php");
if( isset($_REQUEST['iddelivery']) ) {
	$estimate->setForwardingAddress( new ForwardingAddress( $iddelivery ) );
	$estimate->save();
}
if( isset($_REQUEST['idbilling']) ) {
	$estimate->estInvoiceAddress( new InvoiceAddress( $idbilling ) );
}

if(isset($activate) or !empty($activate))
{
	
	$SQL_Query = "Update estimate set status='confirme' where idestimate='" . intval( $_REQUEST[ "IdEstimate" ] ) . "'";
	$rs=& DBUtil::query($SQL_Query);
	if(!$rs)
	{
		$message= "Erreur dans la confirmation du devis !<br />";
		exit();
	}
}

if( isset($_POST['OK']) && isset( $_REQUEST[ "IdEstimate" ] ) ){	
	
	$estimate->set( "status", "Ordered" );
	$estimate->save();

	User::fakeAuthentification();
	$order = TradeFactory::createOrderFromEstimate( $estimate, "ToDo" );
	
	$idorder = $order->getId();
	$idpayment = intval( Payment::$PAYMENT_MOD_DRAFT );
	
	$order->set( "idpayment", intval( Payment::$PAYMENT_MOD_DRAFT ) );
	$order->save();
	
	//-----------------------------------------------------------------------------------------
	//infos client, commercial et admin
	
	$AdminName 	= DBUtil::getParameterAdmin('ad_name');
	$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
	
	$iduser = DBUtil::getParameter('contact_commercial');
	$rs =& DBUtil::query( "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
			
	$CommercialName = $rs->fields( "firstname" ).' '.$rs->fields( "lastname" );
	$CommercialEmail = $rs->fields( "email" );
	
	$BuyerName = $order->getCustomer()->getContact()->getLastName();
	$BuyerEmail = $order->getCustomer()->getContact()->getEmail();
	
	//-----------------------------------------------------------------------------------------
			
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( 1 ); //@todo : imposer la langue du destinataire
	
	$idpaiement = isset( $_POST['idpayment[]']) && $_POST['idpayment[]'] != 0 ? intval( $_POST["idpayment[]"] ) : Payment::$PAYMENT_MOD_DRAFT;
	switch( $idpaiement ){
		
		case "1" :
			
			$editor->setTemplate( "ORDER_NOTIFICATION_1" );
			break;
		
		case "2" :
			
			$editor->setTemplate( "ORDER_NOTIFICATION_2" );
			break;
		
		case "3" :
			
			$editor->setTemplate( "ORDER_NOTIFICATION_3" );
			break;
		
		default : $editor->setTemplate( "ORDER_NOTIFICATION" );
			
	}

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseOrderTags( $order->get( "idorder" ) );
	$editor->setUseBuyerTags( $order->get( "idbuyer" ),$order->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
				
	//-----------------------------------------------------------------------------------------
	//envois du mail
	
	include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );

	$EVM_Mailer = new EVM_Mailer();
	$EVM_Mailer->set_message_type( "html" );
	$EVM_Mailer->set_sender( $AdminName, $AdminEmail );
	$EVM_Mailer->add_recipient( $BuyerName, $BuyerEmail );
	$EVM_Mailer->add_CC( $CommercialName, $CommercialEmail );

	$EVM_Mailer->set_message($Message);
	$EVM_Mailer->set_subject( $Subject );

	$EVM_Mailer->send();
	
	if( $EVM_Mailer->is_errors() ) {
		$errorMsg[] = array('var'=>$EVM_Mailer->get_errors(),'user'=>Dictionnary::translate('error'),'log'=>$ScriptName.' (sendmail.inc.php) => $EVM_Mailer->get_errors()');
	}

	//-----------------------------------------------------------------------------------------
			
	header("Location: $GLOBAL_START_URL/catalog/show_confirm.php?idorder=$idorder");
	
	exit();
	
}

if( isset($_POST['KO']) )
{
	Estimate::delete(intval( $_REQUEST[ "IdEstimate" ] ));

	header("Location: $GLOBAL_START_URL/catalog/admin_estimate.php");
	exit();
}


//basket_amounts
//amounts

$articlesET = 0.0;
$it = $estimate->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();
	

$show_estimate->setData('remise',0);
$show_estimate->setData('total_amount_ht_avant_remise',Util::priceFormat($articlesET));
$show_estimate->setData('remise_rate',"");
$show_estimate->setData('total_amount_ht',Util::priceFormat($articlesET - $estimate->get( "total_discount_amount" )));
$show_estimate->setData('net_to_pay',$estimate->getTotalATI());
$show_estimate->setData('discount',Util::priceFormat( -1.0 * $estimate->get( "total_discount_amount" )));

//Remise commercial
$total_discount = $estimate->get("total_discount");
$total_discount_amount = $estimate->get("total_discount_amount");

$show_estimate->setData('total_discount',Util::rateFormat("total_discount"));
$show_estimate->setData('total_discount_amount',Util::priceFormat($total_discount_amount));

$basetva = DBUtil::getDBValue('total_amount_ht', 'estimate', 'idestimate', $estimate->getId() );
$ttcbase = $estimate->getTotalATI() - $estimate->get( "billing_amount" );
$totvat = $ttcbase-$basetva;
$vat = $estimate->getVATRate();

$show_estimate->setData('getVATRate', Util::rateFormat($vat));
$show_estimate->setData('getVATAmount', Util::priceFormat($totvat));


if( $articlesET == 0.0 ) //ne pas afficher les frais de port si prix cachés
	$tcht = "-";
else{
	$total_charge_ht = $estimate->getChargesET();
	$total_charge_ttc = $estimate->getChargesATI();
	if( $total_charge_ht > 0.0 ){
		$tcht = Util::priceFormat( $total_charge_ht );
		$tchtTTC = Util::priceFormat( $total_charge_ttc );
	}else{
		$tcht = Dictionnary::translate( "FRANCO" );
	}
}

$show_estimate->setData('total_charge_ht',Util::priceFormat($tcht));
$show_estimate->setData('total_charge_ttc',Util::priceFormat($tchtTTC));
$show_estimate->setData('total_amount_ht_wb',Util::priceFormat( $estimate->getTotalET()));

$show_estimate->setData('estimate', 1 );

$show_estimate->setData('total_amount_ttc_wbi',Util::priceFormat( $estimate->getNetBill() ));

//----------------------------------------------------------------------------

//buyer_read.tpl

if( $ScriptName != "order_secure.php"){
	$show_estimate->setData('order_secure',0);
}else{
	$show_estimate->setData('order_secure',1);
}

$show_estimate->setData('buyer_company',  Util::checkEncoding($estimate->getCustomer()->getAddress()->getCompany()) );
$show_estimate->setData('buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $estimate->getCustomer()->getAddress()->getGender() ) );
$show_estimate->setData('buyer_firstname',  Util::checkEncoding($estimate->getCustomer()->getAddress()->getFirstName()) );
$show_estimate->setData('buyer_lastname', Util::checkEncoding($estimate->getCustomer()->getAddress()->getLastName()) );
$show_estimate->setData('buyer_adress', Util::checkEncoding($estimate->getCustomer()->getAddress()->getAddress()) );
$show_estimate->setData('buyer_adress_2', Util::checkEncoding($estimate->getCustomer()->getAddress()->getAddress2()) );
$show_estimate->setData('buyer_zipcode', $estimate->getCustomer()->getAddress()->getZipcode() );
$show_estimate->setData('buyer_city', Util::checkEncoding($estimate->getCustomer()->getAddress()->getCity()) );

$show_estimate->setData('b_buyer_company', Util::checkEncoding($estimate->getInvoiceAddress()->getCompany()) );
$show_estimate->setData('b_buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $estimate->getInvoiceAddress()->getGender() ) );
$show_estimate->setData('b_buyer_firstname', Util::checkEncoding($estimate->getInvoiceAddress()->getFirstName()) );
$show_estimate->setData('b_buyer_lastname', Util::checkEncoding($estimate->getInvoiceAddress()->getLastName()) );
$show_estimate->setData('b_buyer_adress', Util::checkEncoding($estimate->getInvoiceAddress()->getAddress()) );
$show_estimate->setData('b_buyer_adress_2', Util::checkEncoding($estimate->getInvoiceAddress()->getAddress2()) );
$show_estimate->setData('b_buyer_zipcode', $estimate->getInvoiceAddress()->getZipcode());
$show_estimate->setData('b_buyer_city', Util::checkEncoding($estimate->getInvoiceAddress()->getCity()) );
$show_estimate->setData('b_buyer_phonenumber', $estimate->getInvoiceAddress()->getPhonenumber() );
$show_estimate->setData('b_buyer_idstate', $estimate->getInvoiceAddress()->getIdState() );

$show_estimate->setData('d_buyer_company', Util::checkEncoding($estimate->getForwardingAddress()->getCompany()) );
$show_estimate->setData('d_buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $estimate->getForwardingAddress()->getGender() ) );
$show_estimate->setData('d_buyer_firstname', Util::checkEncoding($estimate->getForwardingAddress()->getFirstName()) );
$show_estimate->setData('d_buyer_lastname', Util::checkEncoding($estimate->getForwardingAddress()->getLastName()) );
$show_estimate->setData('d_buyer_adress', Util::checkEncoding($estimate->getForwardingAddress()->getAddress()) );
$show_estimate->setData('d_buyer_adress_2', Util::checkEncoding($estimate->getForwardingAddress()->getAddress2()) );
$show_estimate->setData('d_buyer_zipcode', $estimate->getForwardingAddress()->getZipcode());
$show_estimate->setData('d_buyer_city', Util::checkEncoding($estimate->getForwardingAddress()->getCity()) );
$show_estimate->setData('d_buyer_phonenumber', $estimate->getForwardingAddress()->getPhonenumber() );
$show_estimate->setData('d_buyer_idstate', $estimate->getForwardingAddress()->getIdState() );


//---------------------------------------------//
//-------------------account-------------------//
if( Session::getInstance()->getCustomer() ) {
	$show_estimate->setData('isLogin', "ok");
	
} else {
	$show_estimate->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$show_estimate->setData('com_step_empty', "ok");
	} else {
		$show_estimate->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$show_estimate->setData('issetPostLoginAndNotLogged', "ok");
		$show_estimate->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$show_estimate->setData('issetPostLoginAndNotLogged', "");
	}
	$show_estimate->setData('scriptName', $ScriptName);
	$show_estimate->setData('translateLogin',Dictionnary::translate('login'));
	$show_estimate->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$show_estimate->setData('img', "acceder_compte.png");
		$show_estimate->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$show_estimate->setData('img', "poursuivre.png");
		$show_estimate->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}


$show_estimate->output();
?>