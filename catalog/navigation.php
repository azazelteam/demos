<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonctions limite de produits + liens vers autres pages
 */

include_once("../objects/classes.php");

function CalculateLimiteToDisplay($NbByPages, $CurrentPageNumber, &$LimiteMin)
{	
	if( $CurrentPageNumber < 1 ) $CurrentPageNumber = 1;
        $LimiteMin = $NbByPages * ($CurrentPageNumber-1);
        $LimiteMax = $NbByPages;
	return ($LimiteMin + $NbByPages) ;
}

function CreateLinkToPage($ScriptName, $ParameterArray)
{
        $Link = "$ScriptName?";
        if ( is_array($ParameterArray) )
        {
         reset($ParameterArray);
          while (list($ParmName, $ParmValue) = each($ParameterArray))
          {
                $Link .= "$ParmName=$ParmValue&";
          }
        }
        return ($Link);
}

function CreateProductSeq(&$ScriptName,$CurPageNumber,&$Category )
{
   global $GLOBAL_START_URL;
   
   if( $Category->NBProduct > 1 ) 
		echo "Produits "; 
   else  return;
   
   for($i=1,$j=0;$i <= $Category->NBProduct; $i++,$j++ )
   {
   	
        if($i > 1 )
        	echo ' - ';
        	
        if($i ==  $CurPageNumber )
        	echo "<font class=number_page>$i</font>";
        	
        else {
        	
        	if( $ScriptName == 'product_one.php' ){

        		$href = URLFactory::getProductURL( $Category->products[ $j ], $Category->productnames[ $j ] );
        		echo "<a href=\"$href";;
        		
        	}
        	else echo '<a href="' , $ScriptName , '?IdProduct=',$Category->products[$j] ;
        	echo '" class="link_number_page" >',$i."</a></class>\n";
        }
        
    }
    
}

function CreateFormToPage(&$ScriptName, $Image, &$ParameterArray, $align="right")
{
		global $GLOBAL_START_URL;
        echo "<form action=\"$GLOBAL_START_URL/catalog/$ScriptName\" method=\"post\" style=\"text-align:$align; position:absolute\" >\n";

        if ( is_array($ParameterArray) )
        {
        reset($ParameterArray);
          while (list($ParmName, $ParmValue) = each($ParameterArray))
          {
                        echo "<input type=hidden name=$ParmName value=\"$ParmValue\">\n";
          }
        }
        //echo "<input class=\"PageNavigationButton\" type=\"image\" border=\"0\" src=\"$Image\" >\n";
        echo "</form>\n";
}

function CreatePageSeq(&$ScriptName,$deb,$fin,$CurPageNumber,&$ParameterArray )
{
	global $GLOBAL_START_PATH;
	
	$stringlist=array();
	//$paramstring="";
	
   for($i=$deb;$i <= $fin; $i++ ){
    	$stringlist[$i]["page"]=$i;
    	
        if($i ==  $CurPageNumber ){
        	$stringlist[$i]["current"]= "ok";
   		}else{
        	$stringlist[$i]["current"]= "";
   		}
          if ( is_array($ParameterArray) ){
                reset($ParameterArray);
                //$paramstring= '&';
                while (list($ParmName, $ParmValue) = each($ParameterArray)){
	                if( $ParmName == 'CurrentPageNumberP' ) $ParmValue =  $i ;
	                if( $ParmName == 'CurrentPageNumberC' ) $ParmValue =  $i ;
	                $paramstring.=  "&$ParmName=$ParmValue";
                }
          }
          
		$stringlist[$i]["paramstring"]=$paramstring;
		if($i+1 > $fin){
			$stringlist[$i]["last"]="ok";
		}else{
			$stringlist[$i]["last"]="";
		}
		
    }
    return $stringlist;
}


function Paginate(&$ScriptName,$NbAll,$NbByPages,$CurrentPageNumber,&$ParameterOutputArray)
{
	
	global $Category,$Dico;
	
	$pageliststring="";
	
	if( isset( $Category) && !isset( $_REQUEST["SSE"] ) && !isset( $_REQUEST["SSE2Form"] ) )
		$nmore = $Category->MoreProdPages();
	else $nmore = ceil($NbAll / $NbByPages ) - $CurrentPageNumber ;

	if( $NbAll > $NbByPages ) {
		$ParameterOutputArray["CurrentPageNumberP"] = $CurrentPageNumber;
		if ($CurrentPageNumber > 1) {	
			$ParameterOutputArray["CurrentPageNumberP"]	= $CurrentPageNumber-1;
			CreateFormToPage($ScriptName,Dictionnary::translate("PreviousButton"), $ParameterOutputArray,"right");
			$ParameterOutputArray["CurrentPageNumberP"]=$CurrentPageNumber;
		}

		$pageliststring=CreatePageSeq($ScriptName,1,ceil($NbAll / $NbByPages ),$CurrentPageNumber,$ParameterOutputArray );

		if ( $nmore ) {	
			$ParameterOutputArray["CurrentPageNumberP"]	= $CurrentPageNumber+1;
			CreateFormToPage($ScriptName,Dictionnary::translate("NextButton"), $ParameterOutputArray,"left");
			$ParameterOutputArray["CurrentPageNumberP"]=$CurrentPageNumber;
		}

	}
	
	return $pageliststring;
}


function DipslayQuantityPrices($Data) {
 if( $Data == FALSE ) return;
 global $Dico;
  $n = count($Data);
 if( $n > 0 ) {
 echo "<span onmouseover=\"drc('" , addcslashes(Dictionnary::translate('Prix par quantité'),"'") , "','";
  echo "<tr bgcolor=#DDDDDD ><td align=center ><font color=#cc3333 face=arial> " , addcslashes(Dictionnary::translate('Quantité'),"'") , " </td><td align=center ><font color=#cc3333 face=arial> " , addcslashes(Dictionnary::translate('Prix'),"'") , " </td></tr>";
 for($i=0 ; $i < $n ; $i++)
 {
 	if( $Data[$i][1] > 999998 )  echo "<tr><td align=center bgcolor=#ffffff ><font color=#444444 face=arial> ",$Data[$i][0]," ", addcslashes(Dictionnary::translate("et +"),"'") ," </td><td align=center bgcolor=#ffffff><font color=#444444 face=arial> ",$Data[$i][2],"&nbsp;</td></tr>";
 	else echo "<tr><td align=center bgcolor=#ffffff ><font color=#444444 face=arial> ", addcslashes(Dictionnary::translate('de'),"'") ," ",$Data[$i][0]," ", addcslashes(Dictionnary::translate("à"),"'") ," ",$Data[$i][1]," </td><td align=center bgcolor=#ffffff><font color=#444444 face=arial> ",$Data[$i][2],"&nbsp;</td></tr>";
 }
 echo  "')\" onmouseout='nd()'>(+)</span>\n";
 } 
}

function DisplayQuantityPrices($datas)
{
	global $Dico;

	showDebug($datas,'navigation.php::DisplayQuantityPrices($datas)','code');
	if( empty($datas) ) return '';
	$n = count($datas);
	if( $n > 0 ) {
		$title = addcslashes(Dictionnary::translate('Prix par quantité'),"'");
		$qty = addcslashes(Dictionnary::translate('Quantité'),"'");
		$price = addcslashes(Dictionnary::translate('Prix'),"'");
		$from = addcslashes(Dictionnary::translate('de'),"'");
		$to = addcslashes(Dictionnary::translate('à'),"'");
		$etplus = addcslashes(Dictionnary::translate('et +'),"'");
		$content = "<tr bgcolor=#DDDDDD><td align=center><font color=#cc3333 face=arial>$qty</font></td><td align=center><font color=#cc3333 face=arial>$price</font></td></tr>";
		foreach($datas as $data) {
			if( $data[1] > 999998 ) $content .= "<tr><td align=center bgcolor=#ffffff ><font color=#444444 face=arial>{$data[0]} $etplus</td><td align=center bgcolor=#ffffff ><font color=#444444 face=arial>{$data[2]}</td></tr>";
			else $content .= "<tr><td align=center bgcolor=#ffffff ><font color=#444444 face=arial>$from {$data[0]} $to {$data[1]}</td><td align=center bgcolor=#ffffff ><font color=#444444 face=arial>{$data[2]}</td></tr>";
		}
		return '<span onmouseover="drc(\''.$title.'\',\''.$content.'\')" onmouseout="nd()" class="aide">(+)</span>'."\n";
	} 
}
 
?>