<?php
/**
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Librairie de fonctions pour le moteur de recherche spécifique à une catégorie
 */

//////////////////////////////////////////////////////
// Création des champs pour le moteur de recherche
//////////////////////////////////////////////////////
function CreateSSEField($Table,$Field,$Index)
{
	
 	
	if($Field["lift"]==1)
	{
		
		$name = urlencode( $Field["name"] );
		
		$Where=''; if($Field["where"]!="")
			$Where=" where ".$Field["where"];
		$Base = &DBUtil::getConnection();
		
		$query = "select ".$Field["value"].','.$Field["name"]." from " . $Field["table"] . " $Where";

		$rs =& DBUtil::query( $query );
		
		if($rs->RecordCount())
		{
			
			echo "<select name=\"SSE[$Index]['".$name."']\">\n";
			echo "<option value=\"\">-</option>\n";
			for($i=0 ; $i < $rs->RecordCount() ; $i++)
			{
				
				$Value=$rs->Fields($Field["name"]);
				$Libelle=$rs->Fields($Field["value"]);
				
				$selected = isset( $_POST[ "SSE" ][ $Index ][ $name ] ) 
					&& $_POST[ "SSE" ][ $Index ][ $name ] == $Value ? " selected=\"selected\"" : "";
				
				echo "<option value=\"$Value\"$selected>$Libelle</option>\n";
				$rs->MoveNext();
			}
                  echo "</select>";
		}
	}
	else if($Field["lift"]==2)
	{
		
			$name = urlencode( $Field["name"] );
		
			$Where = $Field["where"];
			echo "<select name=\"SSE[$Index]['".$name."']\">\n";
			echo "<option value=\"\">-</option>\n";
			foreach($Field["where"] as $Value )
			{
				$selected = isset( $_POST[ "SSE" ][ $Index ][ $name ] ) && $_POST[ "SSE" ][ $Index ][ $name ] == $Value ? " selected=\"selected\"" : "";
				$Libelle= 'de ' . str_replace('/',' à ',$Value);
				echo "<option value=\"$Value\"$selected>$Libelle</option>\n";
			}
                  echo "</select>";
	}
	else
	{
		echo "<input size=25 type=text name=\"SSE[$Index][".$Field["name"]."]\" value=\"\">\n";
	}
}

//////////////////////////////////////////////////
// Requète de recherche
//////////////////////////////////////////////////


function CreateSSEQuery($Table,$Field,$SSE)
{
global $IdCategory;
	$Result="";
	$Cond = array();
	$n = count($SSE);
	//if($n > 0 ) $strcat=implode(',',catselect(array($IdCategory)));
	$cat_not_set=TRUE;
	for($i=0 ; $i < $n ; $i++)
	{
		$Name= $Field[$i]["name"];
		
		//echo "DV : count(SSE)".count($SSE)."<br />";
		if( !empty($SSE[$i][$Name]))
		{
			//echo "DV : ".$Field[$i]["type"][0]."<br />";
			switch($Field[$i]["type"][0])
			{
				case "string" : if($Field[$i]["type"][1]!="")
							$Cond[$i]=$Name." ".$Field[$i]["type"][1]." '%".$SSE[$i][$Name]."%'";
						else
							$Cond[$i]=$Name." like '%".$SSE[$i][$Name]."%'";
						break;
				case "number" : if($Field[$i]["type"][1] == "IN")
							$Cond[$i]= $Name . " IN(". implode(',',catselect(array($SSE[$i][$Name]))).")";
						else if($Field[$i]["type"][1]!="")
							$Cond[$i]=$Name.$Field[$i]["type"][1]."'".$SSE[$i][$Name]."'";
						else
							$Cond[$i]=$Name."='".$SSE[$i][$Name]."'";
						if( $Name == 'idcategory' ) $cat_not_set=FALSE;
						break;
				case "date" :   if($Field[$i]["type"][1]=="begin" or $Field[$i]["type"][1]=="")
							$Cond[$i]="UNIX_TIMESTAMP($Name)>=UNIX_TIMESTAMP('".$SSE[$i][$Name]."')";
					      	else
					      		$Cond[$i]="UNIX_TIMESTAMP($Name)<=UNIX_TIMESTAMP('".$SSE[$i][$Name]."')";
					      	break;
				case "interval" : $ia = explode('/',$SSE[$i][$Name]);
						$Cond[$i]="$Name between ".$ia[0]. " and ". $ia[1];
					      	break;
				case "intitule" : 
					$Cond[$i]="((parm_name_1 ='" .$Field[$i]["where"] . "' AND parm_value_1='". $SSE[$i][$Name]."')";
					for($j=2;$j<16;$j++)
					$Cond[$i].=" OR (parm_name_$j='" .$Field[$i]["where"] . "' AND parm_value_$j='".$SSE[$i][$Name]."')";
					$Cond[$i].=')';
				break;
			}
		}
	}
	if(count($Cond))
	{
	if($cat_not_set) { $strcat=implode(',',catselect(array($IdCategory))); $Cond[]= "idcategory IN($strcat)"; }
	$Where ='';
		while(list($k,$v)=each($Cond))
		{
			$Where.=" and $v";
		}
		
		$Result="WHERE".substr($Where,4).' ORDER BY idproduct, idcategory, reference';
		////echo $Result;
	}
	else
	{
		$Result=-1;
	}
	
	return $Result;
	
}

//////////////////////////////////////////////////////
// recherche des sous-catégories finales (sans enfants)
// param $cat_array : tableau avec la ou les id des catégories
// retourne un tableau avec les id des catégories
//////////////////////////////////////////////////////
function catselect($cat_array)
{

$Base = &DBUtil::getConnection();
$strcat=implode(',',$cat_array);
$query ="select idcategoryparent as pid ,idcategorychild as id  from category_link WHERE idcategoryparent IN ($strcat)";
//echo $query,"<br />\n";
$rs =& DBUtil::query($query);
$n = $rs->RecordCount() ;
//echo " n = $n<br />\n";
	if($n>0)
	{
	$sub_array = array(); $par_array = array();
	for($i=0 ; $i < $n ; $i++)
		{
		$cat_array[] = $rs->Fields("id");
		$par_array[] = $rs->Fields("pid");
		$rs->MoveNext();
		}
	}
if($n>0) { $result = array_diff($cat_array,$par_array);  $result = catselect($result); }
else return $cat_array;
return $result;
}

/////////////////////////////////////////
// Requète de création du résultat
/////////////////////////////////////////

function CreateSSECatResult($Table,$Field,$SSE)
{
	
global $Langue;
	
	$Base = &DBUtil::getConnection();
	$WhereCond= CreateSSEQuery($Table,$Field,$SSE);

	if( $WhereCond != -1 )
	{
	$pricectrl = -1 ; $j=0;
	$FieldList="idarticle, idproduct, name$Langue, reference, sellingcost as prix";
	
	for($i=0; $i < count($Field) ; $i++)
		{
			////////// $FieldList.=",".$Field[$i]["name"];
			if($Field[$i]["name"] == 'sellingcost') $pricectrl= $i; 
		} 
	$SSEResult = array();
	$Query = "SELECT $FieldList FROM $Table $WhereCond"; // echo $Query;
	/**/echo "<p>$Query</p>";
	showDebug($Query,'catalog/ssecat.inc.php => $Query','log');
;
	$rs =& DBUtil::query($Query);
	if($rs->RecordCount())
		{
			for($i=0 ; $i < $rs->RecordCount() ; $i++)
			{          
			$SSEResult[$j] =  $rs->fields;
			
			if( $pricectrl >= 0 && !empty($SSE[$pricectrl]['sellingcost']) ) //fourchette de prix
			{
			
				$art = array();
				$Article = new DEPRECATEDARTICLE($art);
	        		$Article->SetId( $SSEResult[$j]['idartcile'] ); $Article->Load();
				$SSEResult[$j]['prix'] = $Article->GetWoPrice();
				$fouchette = explode('/',$SSE[$pricectrl]['sellingcost']);
				if( !$Article->get( "hidecost", 0 ) && 0.0+$SSEResult[$j]['prix'] > 0.0+$fouchette[0] && 0.0+$SSEResult[$j]['prix'] < 0.0+$fouchette[1] )
					$j++;
				else unset($SSEResult[$j]);
				
			}
			else $j++;
			$rs->MoveNext();
			}
		}

	return $SSEResult;
	}
	return 0;
}


function intitule($names,$IdCategory)
{
	
	$strcat=implode(',',catselect( array($IdCategory) ));
	$res= array(); foreach($names as $name) $res[$name]=array();
	$Base = &DBUtil::getConnection();

	$buyer =& Session::getInstance()->getDeprecatedBuyer();
	$catalog_right = $buyer->getvalue( "catalog_right" );
	
	$query ="
	SELECT DISTINCT `intitule_title`.`intitule_title_1`,`intitule_value`.`intitule_value_1` 
	FROM `intitule_link`,`intitule_title`,`intitule_value`,
		category c, product p
	WHERE `intitule_link`.`idcategory` IN( $strcat )
	AND `intitule_link`.`idintitule_value` > 0
	AND `intitule_link`.`idintitule_title`=`intitule_title`.`idintitule_title`
	AND  `intitule_link`.`idintitule_value`=`intitule_value`.`idintitule_value`
	AND `intitule_link`.idcategory = c.idcategory
	AND `intitule_link`.idproduct = p.idproduct
	AND p.catalog_right <= '$catalog_right'
	AND c.catalog_right <= '$catalog_right'";
	
	
	$rs=DBUtil::query($query);
	
	$n = $rs->RecordCount() ;
	
	for($i=0 ; $i < $n ; $i++){
		
		$row = $rs->fields;
		foreach($names as $name)
			if( $row['intitule_title_1'] == $name ) { 
				
				$res[$name][] = $row['intitule_value_1'];
				
			}
			
		$rs->MoveNext();
		
	}

	foreach($names as $name) 
		natsort($res[$name]);

	return $res;
	
}


function intitule_lift($intitules,$Index,$name)
{
	
	echo "<select name=\"SSE[$Index][" .urlencode( $name ) . "]\" class=\"input\">\n";
	echo "<option value=\"\">-</option>\n";
	foreach($intitules as $intitule )
	{
			$selected = isset( $_POST[ "SSE" ][ $Index ][ urlencode( $name ) ] ) 
				&& $_POST[ "SSE" ][ $Index ][ urlencode( $name ) ] == $intitule ? " selected=\"selected\"" : "";
				
			echo "<option value=\"$intitule\"$selected>$intitule</option>\n";
	}
        echo "</select>";
}

function cat_lift($IdCategory,$Index)
{
global $Langue;
$Base = &DBUtil::getConnection();
	echo " <select name=\"SSE[$Index][idcategory]\">\n";
	echo "<option value=\"\">-</option>\n";
	$strcat=implode(',',catlist($IdCategory));
	$query ="select idcategory , name$Langue as name from category WHERE idcategory IN ($strcat)";
	$rs =& DBUtil::query($query);
	$n = $rs->RecordCount() ;
	for($i=0 ; $i < $n ; $i++)
		{
			$Value = $rs->Fields("idcategory");
			$Libelle = $rs->Fields("name");
			$selected = isset( $_POST[ "SSE" ][ $Index ][ "idcategory" ] ) && $_POST[ "SSE" ][ $Index ][ "idcategory" ] == $Value ? " selected=\"selected\"" : "";
			
			echo "<option value=\"$Value\"$selected>$Libelle</option>\n";
			$rs->MoveNext();
		}
        echo "</select>";
}


//////////////////////////////////////////////////////
// recherche des sous-catégories parents
// $category : tableau avec la ou les id des catégories
// retourne un tableau avec les id des catégories
//////////////////////////////////////////////////////
function catlist($category)
{

$Base = &DBUtil::getConnection();
$cat_array = array();
$query ="select idcategorychild as id  from category_link WHERE idcategoryparent = $category";
//echo $query,"<br />\n";
$rs=& DBUtil::query($query);
$n = $rs->RecordCount() ;
	for($i=0 ; $i < $n ; $i++)
		{
		$cat_array[] = $rs->Fields("id");
		$rs->MoveNext();
		}
return $cat_array;
}

 //---------------------------------------------------------------------------------------------
 
 function createSSEHeadingResults( $Table,$Field,$SSE ){

	 global  
	 		$GLOBAL_START_PATH, 
	 		$GLOBAL_START_URL, 
	 		$SSE, 
	 		$Field, 
	 		$SearchCategory, 
	 		$IdCategory;
	
	$lang = Session::getInstance()->getLang();
	$base = DBUtil::getConnection();
	
	$buyer =& Session::getInstance()->getDeprecatedBuyer();
	$catalog_right = $buyer->getvalue( "catalog_right" );
	
	$categories = array();
	listAllCategories( $IdCategory, $categories );
	
	$query = "
	SELECT d.idarticle";
	
	$query .= " 
	FROM 
		detail d,
		product p,
		intitule_link it_link,  
		intitule_title it_title,  
		intitule_value it_value
	WHERE 
		p.idcategory IN ( " . implode( ",", $categories ) . " )
		AND p.catalog_right <= '$catalog_right'
		AND d.idproduct = p.idproduct
		AND it_link.idarticle = d.idarticle 
		AND it_link.idintitule_title = it_title.idintitule_title
		AND it_link.idintitule_value = it_value.idintitule_value
		AND (";
	
	$results = array();
	
	$i = 0;
	$p = 0;
	while( $i < count( $Field ) ){
			
		if( isset( $Field[ $i ][ "type" ][ 0 ] ) && $Field[ $i ][ "type" ][ 0 ] == "intitule" ){
			
			$name = $Field[ $i ][ "name" ];
			$value = $SSE[$i][ urlencode( $name ) ];
			
			if( !empty( $value ) ){
				
				if( $p )
					$query .= " AND";
					
				$query .= "
				( it_title.intitule_title$lang LIKE '$name'
				AND it_value.intitule_value$lang LIKE '$value' )";
				
				$p++;
				
			}
				
		}

		$i++;
		
	}
	
	if( !$p )
		$query .= "1";
		
	$query .= " )";
	
	//showDebug( $query, "QUERY", "code" );
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible d'effectuer la recherche" );
		
	if( !$rs->RecordCount() )
		return array();
		
	$fields = "d.idarticle, d.idproduct, p.name$lang, d.reference, d.sellingcost";
	$where = "idarticle IN (";
	
	$k = 0;
	while( !$rs->EOF() ){
		
		if( $k )
			$where .= ", ";
			
		$where .= $rs->fields( "idarticle" );
		
		$rs->MoveNext();
		
		$k++;
		
	}
	
	$where .= ") AND p.idproduct = d.idproduct AND p.available = 1";
	
	$query = "SELECT $fields FROM product p, detail d WHERE $where";
	
	//showDebug( $query, "QUERY", "code" );
	
	$rs =& DBUtil::query( $query );
	
	if( $rs === false )
		die( "Impossible d'effectuer la recherche" );
	
	//résultats
	
	$p = 0;
	$SSEResult = array();
	while( !$rs->EOF() ){
		
		if( isset( $_POST[ "sellingcost" ] ) && !empty( $_POST[ "sellingcost" ] ) ){

			//usine à gaz pour calculer les prix :
			
			$art = array();
			$idarticle = $rs->fields( "idarticle" );
			$Article = new DEPRECATEDARTICLE( $art );
	        $Article->SetId( $idarticle ); 
	        $Article->Load();
			$discountPrice = $Article->GetWoPrice();
			$limits = explode( "/", $_POST[ "sellingcost" ] );
			
			if( !$Article->get( "hidecost", 0 ) 
				&& 0.0 + $discountPrice > 0.0 + $limits[ 0 ] 
				&& 0.0 + $discountPrice < 0.0 + $limits[ 1 ] ){
					
				$SSEResult[ $p ] = $rs->fields;
				$p++;
				
			}
				
		}
		else{

			$SSEResult[ $p ] = $rs->fields;
			$p++;
			
		}
		
		$rs->MoveNext();
		
	}
	
	return $SSEResult;
	
 }
 
//---------------------------------------------------------------------------------------------

function listAllCategories( $idcategory, &$categories ){
	
	
	
	$db =& DBUtil::getConnection();
	
	$categories[] = $idcategory;
	
	$query = "
	SELECT idcategorychild AS idchild
	FROM category_link
	WHERE idcategoryparent = '$idcategory'";
	
	$rs = $db->Execute( $query );
	
	if( $rs === false )
		die( "Impossible de récupérer les sous-catégories $query" );
		
	while( !$rs->EOF() ){
	
		$idchild = $rs->fields( "idchild" );

		listAllCategories( $idchild, &$categories );
		
		$rs->MoveNext();

	}
	
}


//---------------------------------------------------------------------------------------------

?>