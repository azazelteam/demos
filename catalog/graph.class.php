<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Classe de génération d'histogrammmes et courbes 3D
 */
 

class Graph {

	var $largeurImg=680;
	var $HauteurImg=465;
	var $largeurGraph=580;
	var $hauteurGraph=250;
	var $prodondeurGraph=2;
	var $paddingLeft=42;
	var $paddingRight=5;
	var $paddingTop=1;
	var $paddingBottom=40;
	var $largeurAretteExterne=10;
	var $nbrRepere=8;
	var $decimalX=0;
	var $modeZoom=0;
	var $effetCourbe=1;
	var $typeGraph='histo';
	var $img;
	var $gauche;
	var $bas;
	var $fond;
    var $colorGauche= 0;
    var $colorGaucheHex= 'CCCCCC'; 
    var $colorBas= 0;
    var $colorBasHex= 'FAEBDC';
	var $colorFond=0;
	var $colorFondHex='DDDDDD';
	var $colorAretteInterne=0;
	var $colorAretteInterneHex='000000';
	var $colorAretteExterne=0; 
	var $colorAretteExterneHex='B1C2D3'; 
	var $colorValue=0;
	var $colorValueHex='000000';
	var $colorPointGraph;
	var $colorPointGraphHex;
	var $colorShadeGraph;
	var $colorGraph=0;
	var $colorGraphHex='669999';
	var $colorContourGraphe=0;
	var $colorContourGrapheHex='000000';
	var $tabKey;
	var $tabValue;
	var $tabSerie;
	var $nbSerie;
	var $nbValSerie;
	var $title;
	var $maxValue;
	var $minValue='';
	var $nbrValue=1;
	var $value1px;
	var $bufferMAP='';
	var $nomImg;
	var $distanceElement;
	var $paddingBatton=20;
	var $margeLegend=0;
	var $decalSerie=4;
	var $idGraph;
	var $pieOptions;
	var $series;
	var $xTicks; 
	var $yTicks; 
	var $firstX;
	var $lastX;
	var $nbX;
	var $firstY;
	var $lastY;
	var $nbY;

/**
 * Constructeur
 * param _idGraph id du graphique
 * 
 * @return void
 */ 
 	function Graph($_idGraph){
 		$this->idGraph = $_idGraph;
 		$this->parametrage();
 		$this->series = '[';
 		$this->nbrSerie = 0;
 		$this->xTicks = array();
 		$this->yTicks = array();
 	}
 
	function parametrage(){

		//Calcul du nombre de series
		$this->nbSerie=count($this->title);
		
		//Calcul du nombre de valeur dans chaque série
		for($i=1;$i<=($this->nbSerie);$i++){ $this->nbValSerie[$i]=count($this->tabValue[$i]); }
		//for($i=1;$i<=count($this->tabValue)-1;$i++){ $this->nbValSerie[$this->tabSerie[$i]]++; }
		
		//Vérifie les paramètres
		if (($this->largeurGraph + $this->prodondeurGraph + $this->largeurAretteExterne + $this->paddingLeft + $this->paddingRight) >$this->largeurImg) {
			//echo 'Graphique trop large pour l\'image';
			$this->largeurGraph=$this->largeurImg - $this->prodondeurGraph - $this->largeurAretteExterne - ($this->paddingLeft + $this->paddingRight);
		}
		if (($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop + $this->paddingBottom) >$this->HauteurImg){
			//echo 'Graphique trop haut pour l\'image';
			$this->hauteurGraph=$this->HauteurImg - $this->prodondeurGraph - ($this->paddingTop + $this->paddingBottom);
		}

		// calcul des points l'image
		$this->gauche[]=$this->paddingLeft + $this->largeurAretteExterne;
		$this->gauche[]=$this->paddingTop + $this->prodondeurGraph;
		$this->gauche[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph;
		$this->gauche[]=$this->paddingTop;
		$this->gauche[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph;
		$this->gauche[]=$this->paddingTop + $this->hauteurGraph;
		$this->gauche[]=$this->paddingLeft + $this->largeurAretteExterne;
		$this->gauche[]=$this->paddingTop + $this->hauteurGraph + $this->prodondeurGraph;

		$this->fond[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph;
		$this->fond[]=$this->paddingTop;
		$this->fond[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph + $this->largeurGraph + 30 ;
		$this->fond[]=$this->paddingTop;
		$this->fond[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph + $this->largeurGraph + 30 ;
		$this->fond[]=$this->paddingTop + $this->hauteurGraph;
		$this->fond[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph;
		$this->fond[]=$this->paddingTop + $this->hauteurGraph;

		$this->bas[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph;
		$this->bas[]=$this->paddingTop + $this->hauteurGraph;
		$this->bas[]=$this->paddingLeft + $this->largeurAretteExterne + $this->prodondeurGraph + $this->largeurGraph + 30 ;
		$this->bas[]=$this->paddingTop + $this->hauteurGraph;
		$this->bas[]=$this->paddingLeft + $this->largeurAretteExterne + $this->largeurGraph + 30;
		$this->bas[]=$this->paddingTop + $this->hauteurGraph + $this->prodondeurGraph;
		$this->bas[]=$this->paddingLeft + $this->largeurAretteExterne;
		$this->bas[]=$this->paddingTop + $this->hauteurGraph + $this->prodondeurGraph;

		$this->xTicks = array();
		$this->yTicks = array();
		
	}


/**
 * Définir les valeurs des étiquettes des abscisses
 *
 * Param $tabTicks: Tableau associatif de type [value, text] avec value la valeur sur l'axe et text le label pour ce point
 *
 * @return string
 */
	function setXTicks($tabTicks)
	{
		$this->xTicks = 'xaxis: { ticks: [';
		$moreThanOne = false;
		foreach($tabTicks as $key=>$value) 
		{
			if($moreThanOne) $this->xTicks.=', ';
			$this->xTicks .= '['.$key.',"'.$value.'"]';
			$moreThanOne = true;
		}
		$this->xTicks .= ']';
	}

/**
 * Définir les valeurs des étiquettes des abscisses
 *
 * Param $firstValue: nombre de points sur l'axe
 * Param $lastValue: nombre de points sur l'axe 
 * 
 * @return string
 */
	function setXLandMarks($firstValue, $lastValue)
	{
		$this->firstX = $firstValue;
		$this->lastX = $lastValue; 
	}

/**
 * Définir le nombre de graduations des abscisses
 *
 * Param $nbTicks: nombre de graduations 
 * 
 * @return string
 */
	function setNbX($nbTicks)
	{
		$this->nbX = $nbTicks;
	}


/**
 * Définir les valeurs des étiquettes des ordonnées
 *
 * Param $tabTicks: Tableau associatif de type [value, text] avec value la valeur sur l'axe et text le label pour ce point
 * 
 * @return string
 */
	function setYTicks($tabTicks)
	{
		$this->yTicks = 'yaxis: { ticks: [';
		$moreThanOne = false;
		foreach($tabTicks as $key=>$value)
		{
			if($moreThanOne) $this->yTicks.=', ';
			$this->yTicks .= '['.$key.',"'.$value.'"]';
			$moreThanOne = true;
		}
		$this->yTicks .= ']';
	}


/**
 * Définir les valeurs des étiquettes des abscisses
 *
 * Param $firstValue: nombre de points sur l'axe
 * Param $lastValue: nombre de points sur l'axe 
 * 
 * @return string
 */
	function setYLandMarks($firstValue, $lastValue)
	{
		$this->firstY = $firstValue;
		$this->lastY = $lastValue; 
	}

/**
 * Définir le nombre de graduations des ordonnées
 *
 * Param $nbTicks: nombre de graduations 
 * 
 * @return string
 */
	function setNbY($nbTicks)
	{
		$this->nbY = $nbTicks;
	}

/**
 * Récupérer les couleurs du camembert dans le fichier
 * 
 * @return string
 */
 	function getColors()
	{
		global $camcolor;
			
		$colors = 'colors: [';
		$moreThanOne = false;
		
		foreach($camcolor as $color)
		{
			if($moreThanOne) $colors.=',';
			else $moreThanOne = true;
			$colors .= '"#'.$color.'"';
		}
		
		$colors .= '],'; 
		return $colors;
	}	
	
/**
 * Défini les options du camembert
 * 
 * @return void
 */
	function getOptions()
	{
		$result = '{'.$this->getColors().'
					bars: {show: true, autoScale: true, fillOpacity: 1},
					legend: {show: false}';
		if(count($this->xTicks))
		{
			$result .= ', xaxis : { ticks: [';
			$moreThanOne = false;

			if(count($this->xTicks)<=10)
			{
				foreach($this->xTicks as $key=>$val)
				{
					if($moreThanOne) $result .= ', ';
					else $moreThanOne = true;
					$result .= '['.$key.', "'.$val.'"]';					
				}
			}
			else
			{
				$cnt = count($this->xTicks) / 10;
				$cnt = number_format($cnt,0);
				$i = 0;
				
				foreach($this->xTicks as $key=>$val)
				{
					if(($i % $cnt) == 0) {
						if($moreThanOne) $result .= ', ';
						else $moreThanOne = true;
						$result .= '['.$key.', "'.$val.'"]';
					}
					$i++;					
				}
			}
			if(isset($this->firstX)) $result .= '] , min: '.$this->firstX.', max: '.$this->lastX.'}';
			else $result .= ']}'; 
		}			
		else if(isset($this->nbX))
		{
			$result .= ', xaxis : { ticks: '.$this->nbX;
			if(isset($this->firstX)) $result .= ', min: '.$this->firstX.', max: '.$this->lastX;
			$result .= '}'; 
		}
		else if(isset($this->firstX))
		{
			$result .= ', xaxis : { min: (new Date("'.str_replace("-","/",$this->firstX).'")).getTime(), max: (new Date("'.str_replace("-","/",$this->lastX).'")).getTime()}';
		}
					
		if(count($this->yTicks))
		{
			$result .= ', yaxis : { ticks: [';
			$moreThanOne = false;
			foreach($this->yTicks as $key->$val)
			{
				if($moreThanOne) $result .= ', ';
				else $moreThanOne = true;
				$result .= '['.$key.', "'.$val.'"]';
				
			}
			if(isset($this->firstY)) $result .= '] , min: '.$this->firstY.', max: '.$this->lastY;
			$result .= '}'; 
		}			
		else if(isset($this->nbY))
		{
			$result .= ', yaxis : { ticks: '.$this->nbY;
			if(isset($this->firstY)) $result .= ', min: '.$this->firstY.', max: '.$this->lastY;
			$result .= '}';
		}
		else if(isset($this->firstY))
		{
			$result .= ', yaxis : { min: '.$this->firstY.', max: '.$this->lastY.'}';
		}

		$result .= '}'; 

		return $result;
	}
	
/**
 * Création de l'image vide de base du graphique
 * 
 * @return void
 */
	function creatImg(){
		// creation de l'image
		$this->img = imagecreate($this->largeurImg,($this->HauteurImg));

		// couleur blanche du fond de l'image
		imagecolorallocate($this->img, 255,255,255);
	}


/**
 * Conversion d'une couleur Hexa en RVB
 * 
 * @param mixed $hexa Valeur de couleur hexadecimal à convertir
 * @return mixed $tab Tableau des valeurs RVB
 */
	function convertHexaEnRVB($hexa){
		for ($i=0; $i<3; $i++){
			$tab[$i]=hexdec(substr($hexa, (2 * $i)+1,2));
		}
		return $tab;
	}


/**
 * Gestion du format de date européen jj/mm/aaaa
 * 
 * @param string $date Date au format us
 * @return string $newdate Date au format europeen
 */	
	function Date2eu($date){
    		$euDateTab = explode("-",$date);
    		$newdate='';
    		if(isset($euDateTab[2]) ){ $newdate .= $euDateTab[2].'/'; }
    		if(isset($euDateTab[1]) ){ $newdate .= $euDateTab[1].'/'; }
    		$newdate .= $euDateTab[0];
    		return $newdate;
    }

/**
 * Initiale du jour de la semaine
 * 
 * @param string $usDate Date au format us
 * @return string l'initiale du jour de la semaine
 */	
 
function euDayFirstCHar( $usDate ){
	
	$week = array( "D", "L", "M", "M", "J", "V", "S" );
	
	$tmp = explode( "-", $usDate );
	
	if( !is_array( $tmp ) || count( $tmp ) != 3 )
		return "";
		
	list( $year, $month, $day ) = $tmp;
	
	$w = date( "w", mktime( 0, 0, 0, $month, $day, $year ) );
	
	return $week[ $w ];
	
}

/**
 * Gestion et allocation des couleurs du graphique
 * 
 * @return void
 */
	function color(){
			
		// couleur RVB
		$tab=$this->convertHexaEnRVB($this->colorFondHex);
		$this->colorFond = imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);
		
		$tab=$this->convertHexaEnRVB($this->colorGaucheHex);
		$this->colorGauche = imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);

		$tab=$this->convertHexaEnRVB($this->colorBasHex);
		$this->colorBas= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);

		$tab=$this->convertHexaEnRVB($this->colorAretteInterneHex);
		$this->colorAretteInterne= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);

		$tab=$this->convertHexaEnRVB($this->colorAretteExterneHex);
		$this->colorAretteExterne= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);

		$tab=$this->convertHexaEnRVB($this->colorValueHex);
		$this->colorValue= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);

		$tab=$this->convertHexaEnRVB($this->colorGraphHex); 
		$this->colorGraph= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);

		for($i=1;$i<=$this->nbSerie;$i++){
			$tab=$this->convertHexaEnRVB($this->colorPointGraphHex[$i]);
			$this->colorPointGraph[$i]= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);
			$this->colorShadeGraph[$i]= imagecolorallocate($this->img, max(0,$tab[0]-50),max(0,$tab[1]-50),max(0,$tab[2]-50));
		}

		$tab=$this->convertHexaEnRVB($this->colorContourGrapheHex);
		$this->colorContourGraphe= imagecolorallocate($this->img, $tab[0],$tab[1],$tab[2]);
		
	}


/**
 * Creation de la base (axes, quadrillage) du graphique
 * 
 * @return void
 */
	function creatGraph(){
		imagefilledpolygon($this->img, $this->gauche,4, $this->colorGauche); 
		//imagefilledpolygon($this->img, $this->fond,4, $this->colorFond);  
		imagefilledpolygon($this->img, $this->bas,4, $this->colorBas);  
		
		// dessine les arettes 
		imageline($this->img, $this->gauche[2], $this->gauche[3], $this->gauche[2], $this->gauche[5], $this->colorContourGraphe);
		imageline($this->img, $this->gauche[2], $this->fond[5], $this->fond[4], $this->fond[5], $this->colorContourGraphe);
		imageline($this->img, $this->gauche[2], $this->gauche[5], $this->gauche[6], $this->gauche[7], $this->colorContourGraphe);

		imageline($this->img, $this->gauche[0], $this->gauche[1], $this->gauche[2], $this->gauche[3], $this->colorContourGraphe);
		imageline($this->img, $this->gauche[2], $this->gauche[3], $this->fond[2], $this->fond[3], $this->colorContourGraphe);
		imageline($this->img, $this->fond[2], $this->fond[3], $this->fond[4], $this->fond[5], $this->colorContourGraphe);
		imageline($this->img, $this->fond[4], $this->fond[5], $this->bas[2], $this->bas[3], $this->colorContourGraphe);
		imageline($this->img, $this->bas[2], $this->bas[3], $this->bas[4], $this->bas[5], $this->colorContourGraphe);
		imageline($this->img, $this->bas[4], $this->bas[5], $this->bas[6], $this->bas[7], $this->colorContourGraphe);
		imageline($this->img, $this->gauche[6], $this->gauche[7], $this->gauche[0], $this->gauche[1], $this->colorContourGraphe);


	}


/**
 * Ajout d'une valeur dans une série
 * 
 * @param int $s Index de la serie
 * @param string $x 
 * @param float $y Valeur à ajouter
 * @return void
 */
	function valeur($s, $x, $y){
		$this->tabKey[]=$x;
		$this->tabValue[]=$y;
		$this->tabSerie[]=$s;
	}


/**
 * Ajout d'un titre/intitulé a une série
 * 
 * @param int $serie Index de la serie
 * @param string $x Titre/Intitulé
 * @return void
 */		
	function setTitle($serie,$title){
		$this->title[$serie]=$title;
	}


/**
 * Spécifie l'affichage de la légende (à droite)
 * 
 * @return void
 */		
	function showLegend(){
		$len = 20;
		foreach($this->tabtext as $content){
			if(strlen($content)>$len) $len = strlen($content);
		}
		$this->margeLegend=($len*8)+20;
	}



/**
 * Gestion des série de valeurs, des échelles abscisses et ordonnées
 * 
 * @return void
 */
	function statementValue(){
		// la plus grande valeur
		$tabValeur=$this->tabValue;
		
		rsort($tabValeur, SORT_NUMERIC);
		
		
		// marge en haut et en bas du graphique
		/*if ($this->modeZoom==1){
			$this->maxValue=(($tabValeur[0] * 101) / 100);
		}
		else{
			$this->maxValue=(($tabValeur[0] * 110) / 100);
		}
		
		// determine l'echelle de x
		$this->nbrValue=count($this->tabValue);
		$this->espX=round(($this->largeurGraph/$this->nbrValue));

		// calcul la valeur d'un pixel
		if ($this->modeZoom==1){
			// calcule la valeur mini
			sort($tabValeur, SORT_NUMERIC);
			
			if ($this->modeZoom==1){
				$this->minValue=(($tabValeur[0][0] * 98) / 100);
			}
			else{
				$this->minValue=(($tabValeur[0][0] * 90) / 100);
			}
			
			$this->value1px=(($this->maxValue - $this->minValue) / $this->hauteurGraph);
		}
		else{
			$this->value1px=($this->maxValue/$this->hauteurGraph);
		}

		
		
		$esp=floor($this->hauteurGraph/($this->nbrRepere+1));

		for ($i=1; $i<=$this->nbrRepere; $i++){
			if($this->value1px!=0){
			// marque la valeur
			$repereValue=round((($this->value1px * ($this->hauteurGraph - ($esp * $i))) + $this->minValue), $this->decimalX);
			imagestring($this->img, '2', ($this->gauche[0]-(strlen($repereValue)*6.5) - ($this->largeurAretteExterne/2)), round((($this->hauteurGraph - ($repereValue - $this->minValue) / $this->value1px) + $this->paddingTop), 1)-3, $repereValue, $this->colorValue);
			// dessine les reperes
			imageline($this->img, $this->gauche[0], round((($this->hauteurGraph - (($repereValue - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), $this->gauche[2], round((($this->hauteurGraph - ($repereValue - $this->minValue) / $this->value1px) + $this->paddingTop), 1), $this->colorAretteInterne); // GAUCHE
			imageline($this->img, $this->gauche[2], round((($this->hauteurGraph - ($repereValue - $this->minValue) / $this->value1px) + $this->paddingTop), 1), $this->fond[4], round((($this->hauteurGraph - ($repereValue - $this->minValue) / $this->value1px) + $this->paddingTop), 1), $this->colorAretteInterne); // FOND
			imageline($this->img, $this->gauche[0], round((($this->hauteurGraph - (($repereValue - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), $this->gauche[0] - $this->largeurAretteExterne, round((($this->hauteurGraph - (($repereValue - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), $this->colorAretteExterne); // EXTERIEUR
			}
		}

		$this->nbrX=count($this->tabKey);*/

		
	}


/**
 * Génération d'une courbe
 * 
 * @return void
 */		
	function traceCourbe(){
		
		// calcul la distance entre chaque mesure
		// 0.95 au lieu de 1 pour pas que barre de repere soit completement au bout du graph
		$this->distanceElement=round($this->largeurGraph /  (($this->nbrX/$this->nbSerie)-0.95), 1);
		
		for($t=1; $t<=$this->nbSerie; $t++){
			$position=0;
			
			for ($i=0; $i<$this->nbrX; $i++){
			
			if($this->tabSerie[$i]==$t and $position<$this->nbValSerie[$t]-1){
				
			
			// place les valeurs de mesure (en Abscisse)
			imageline($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)), ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop), ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)), ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop + 10), $this->colorAretteExterne);
			imagestringup($this->img, '2', ($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position) - 6), ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop + (strlen($this->tabKey[$i])*6) + $this->largeurAretteExterne), $this->tabKey[$i], $this->colorValue);

			imageline($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), (($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)) + $this->prodondeurGraph), round(($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop), 1), $this->colorPointGraph);
			imageline($this->img, ($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), (($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position)) + $this->prodondeurGraph), round(($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop), 1), $this->colorPointGraph);
			imageline($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop - 1), 1), (($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)) + $this->prodondeurGraph), round(($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop - 1), 1), $this->colorPointGraph);
			imageline($this->img, ($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop - 1), 1), (($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position)) + $this->prodondeurGraph), round(($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop - 1), 1), $this->colorPointGraph);
			
			imagefilledellipse($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop +1), 1), 5, 5, $this->colorPointGraph);

			// defini les zones MAP
			$this->bufferMAP.='<area shape="rect" coords="'.($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $i)-8).','.round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop -7), 1).','.($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $i)+8).','.round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop +9), 1).'" href="javascript:return false;" title="'.number_format($this->tabValue[$i],2,',',' ').' ('.$this->Date2eu($this->tabKey[$i]).')">';

			// attache les points pour creer une courbe
			if (isset($this->tabValue[$i+1])){

				switch ($this->effetCourbe) {
					case 0:
						$pointSuivant=round((($this->hauteurGraph - (($this->tabValue[$i+1] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1);
						$valeurEntrePoint=(round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1)-$pointSuivant);
						$pointParPixel=($valeurEntrePoint/$this->distanceElement);

						for ($v=0; $v<round($this->distanceElement); $v=$v+4){
							imageline($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position) + $v), round(((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop) - ($pointParPixel * $v)), 1), ((($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)) + $this->prodondeurGraph) + $v), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop) - ($pointParPixel * $v)), 1), $this->colorPointGraph[$t]);

						}
						break;
					case 1:
						$point[]=($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position));
						$point[]=round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop + 1), 1);
						$point[]=(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)) + $this->prodondeurGraph);
						$point[]=round(($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop ), 1);

						$point[]=(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))) + $this->prodondeurGraph);
						$point[]=round(($this->hauteurGraph - (($this->tabValue[($i+1)] - $this->minValue) / $this->value1px) + $this->paddingTop), 1);
						$point[]=($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1)));
						$point[]=round((($this->hauteurGraph - (($this->tabValue[($i+1)] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop + 1 ), 1);
						imagefilledpolygon($this->img, $point,4, $this->colorPointGraph[$t]);
						$point='';
						break;
				}
			}
			$position++;
							
			}

			}

		}

	}


/**
 * Génération du graphique
 * 
 * @return void
 */
	function traceHisto(){

		// calcul la distance entre chaque mesure
		// 0.25 au lieu de 1 pour pas que barre de repere soit completement au bout du graph
		if($this->nbSerie >1) $this->distanceElement=round($this->largeurGraph / (($this->nbrX/$this->nbSerie)-0.25), 1);
		else $this->distanceElement=$this->largeurGraph;
		$gray= imagecolorallocate($this->img, 150,150,150);
		
		for($t=1; $t<=$this->nbSerie; $t++){
			$position=0;
			for ($i=0; $i<$this->nbrX; $i++){
			
			if($this->tabSerie[$i]==$t){
							
			// place les valeurs de mesure (en Abscisse)
			imageline($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position) + ($this->distanceElement/6)), ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop), ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement *  $position) + ($this->distanceElement/6) +4 ), ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop + 10), $this->colorAretteExterne);
			imagestringup($this->img, '2', ($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position) + ($this->distanceElement/6)), ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop + (strlen($this->Date2eu($this->tabKey[$i]))*6) + $this->largeurAretteExterne),$this->Date2eu($this->tabKey[$i]), $this->colorValue);
			imagestring($this->img, '2',($this->paddingLeft + $this->largeurAretteExterne + ($this->distanceElement * $position) + ($this->distanceElement/6)) + 5, ($this->hauteurGraph + $this->prodondeurGraph + $this->paddingTop + (strlen($this->Date2eu($this->tabKey[$i]))*6) + 4 + $this->largeurAretteExterne), $this->euDayFirstCHar( $this->tabKey[$i] ), $this->colorValue );

			$point[]=($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie));
			$point[]=round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1);
			$point[]=(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie)) + $this->prodondeurGraph);
			$point[]=round(($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->paddingTop ), 1);

			$point[]=(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))+(($t-1)*$this->decalSerie)) + $this->prodondeurGraph - $this->paddingBatton);
			$point[]=round(($this->hauteurGraph - (($this->tabValue[($i)] - $this->minValue) / $this->value1px) + $this->paddingTop), 1);
			$point[]=($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))+(($t-1)*$this->decalSerie) - $this->paddingBatton);
			$point[]=round((($this->hauteurGraph - (($this->tabValue[($i)] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop + 1 ), 1);

			imagefilledpolygon($this->img, $point,4, $this->colorPointGraph[$t]);

			$point='';


			$point[]=(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))+(($t-1)*$this->decalSerie)) + $this->prodondeurGraph - $this->paddingBatton);
			$point[]=round(($this->hauteurGraph - (($this->tabValue[($i)] - $this->minValue) / $this->value1px) + $this->paddingTop), 1);
			$point[]=($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))+(($t-1)*$this->decalSerie) - $this->paddingBatton);
			$point[]=round((($this->hauteurGraph - (($this->tabValue[($i)] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop + 1 ), 1);

			$point[]=($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))+(($t-1)*$this->decalSerie) - $this->paddingBatton);
			$point[]=$this->bas[5];
			$point[]=(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * ($position+1))+(($t-1)*$this->decalSerie)) + $this->prodondeurGraph - $this->paddingBatton);
			$point[]=$this->bas[5] - $this->prodondeurGraph;

			imagefilledpolygon($this->img, $point,4, $this->colorPointGraph[$t]);

			$point='';

			imagerectangle($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie))+($this->distanceElement-$this->paddingBatton), $this->bas[5], $this->colorPointGraph[$t]);
			imagefilledrectangle($this->img, ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie)), round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1), ($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie))+($this->distanceElement-$this->paddingBatton), $this->bas[5], $this->colorPointGraph[$t]);
			
			// defini les zones MAP
			$this->bufferMAP.='<area style="cursor: help;" shape="rect" coords="'.($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie)).','.round((($this->hauteurGraph - (($this->tabValue[$i] - $this->minValue) / $this->value1px) + $this->prodondeurGraph) + $this->paddingTop), 1).','.(($this->paddingLeft + 1 + $this->largeurAretteExterne + ($this->distanceElement * $position)+(($t-1)*$this->decalSerie))+($this->distanceElement-$this->paddingBatton)).','.$this->bas[5].'" href="javascript:return false;" title="'.number_format($this->tabValue[$i],2,',',' ').' ('.$this->Date2eu($this->tabKey[$i]).')">'."\n";
			
			$position++;
			
			}
					
			}
		}

	}


/**
 * Affichage de la légende
 * 
 * @return void
 */
	function traceLegende(){
		
		/*$positionX1=$this->largeurGraph+$this->paddingLeft+70;
		$positionY1=$this->paddingTop;
		$positionX2=$this->largeurGraph+$this->paddingLeft+85;
		$positionY2=$this->paddingTop+10;
		for($i=1;$i<=$this->nbSerie;$i++){
			imagefilledrectangle($this->img, $positionX1,$positionY1, $positionX2, $positionY2, $this->colorPointGraph[$i]);
			ImageString ($this->img, 3, $positionX1+25, $positionY1-2, $this->title[$i], $this->colorValue);
			$positionY1 = $positionY1+ 15;
			$positionY2 = $positionY2+ 15;
		}*/
		
		$positionX1 = $this->paddingLeft + 15;
		$positionY1 = $this->paddingTop + $this->hauteurGraph + 115;
		$positionX2 = $this->paddingLeft + 30;
		$positionY2 = $this->paddingTop + $this->hauteurGraph + 125;
		
		for($i=1;$i<=$this->nbrSerie;$i++){
			
			imagefilledrectangle($this->img, $positionX1,$positionY1, $positionX2, $positionY2, $this->colorPointGraph[$i]);
			ImageString ($this->img, 3, $positionX1+25, $positionY1-2, $this->title[$i], $this->colorValue);
			$positionY1 += 15;
			$positionY2 += 15;
		
		}
		
	}



/**
 * Ajout d'une série au graphique
 * 
 * @param int $serie Index de la serie
 * @param mixed $color Couleur affectée aà la série 
 * @param string $title Titre de la série 
 * @param mixed $tabval Tableau de valeur de la série
 * @return void
 */			
	function setSerie($serie,$color,$title,$tabval){

    	 $this->colorPointGraphHex[$serie]=$color;
    /*	 $this->setTitle($serie,$title);
    	 $this->nbserie++;*/
    	
       	 $this->tabValue[$this->nbrSerie] = $tabval;
    	 $this->tabcolor[$this->nbrSerie] = $color;
     	 $this->tabtext[$this->nbrSerie] = $title;  
	 	 $this->nbrSerie ++; 
	}



/**
 * Export des données du graphique
 * 
 * @return mixed $arr Tableau représentant le graphique
 */		
	function getArray(){
		$arr=array();
		$temparr=array();
		$arr['date'][]=' ';
		foreach ($this->tabValue as $x => $y){
			$arr['date'][$this->tabKey[$x]]=$this->tabKey[$x];
		}
		foreach ($this->tabValue as $x => $y){
			$temparr[$this->title[$this->tabSerie[$x]]][0] = $this->title[$this->tabSerie[$x]];
			$temparr[$this->title[$this->tabSerie[$x]]][] = $this->tabValue[$x];
			$arr[$this->title[$this->tabSerie[$x]]]=$temparr[$this->title[$this->tabSerie[$x]]];
		}
		return $arr;
	}


/**
 * Affichage du graphique et des ses outils (légende, reperes, map, etc...)
 * 
 * @return void
 */	
	function Show(){

$graf ="";
$graf.= '<script type="text/javascript" src="<?php echo $GLOBAL_START_URL ?>/js/jquery/jquery-1.3.2.min.js"></script>';


$graf.= '<link type="text/css" rel="stylesheet" href="../css/statistiques.css" />';
$graf.= '<link type="text/css" rel="stylesheet" href="../css/newstats.css" />';

		
		$graf.= '<div class="histogramme" id="'.$this->idGraph.'"></div><br/><br/>'; 
  	 	 
  	 	$graf.= '<script language="Javascript">'."\n".'$(function (){'."\n";
  	 	
		$graf.= '$.plot($("#'.$this->idGraph.'"), [';
  	 	
  	 	for($i=0; $i<$this->nbrSerie; $i++)
 		{
 			if($i>0) $graf.= ', ';
 			$graf.= '{"label": "'.$this->tabtext[$i].'", "data": [';
		
			$number = 0;
		
 			$moreThanOne = false;
 			foreach($this->tabValue[$i] as $x => $y) 
 			{
 				if (preg_match ("/[0-9\.]+/", $y)) 
 				{
 					if(strlen($x) > 7)
 					{
	 					list($year, $month, $day) = preg_split('/[.-]/', $x);
	 					
	 					if($moreThanOne) $graf.= ', ';
	 					else $moreThanOne = true;
	 					
	  					$graf.= '['.$year.$month.$day.', '.$y.']';
	  					$this->xTicks[$year.$month.$day] = $day.'/'.$month.'/'.$year;
	  				}
	  				else
	  				{	
	 					list($year, $month) = preg_split('/[.-]/', $x);
	 					
	 					if($moreThanOne) $graf.= ', ';
	 					else $moreThanOne = true;
	 					
	  					$graf.= '['.$number.', '.$y.']';
	  					$this->xTicks[$number] = $month.'/'.$year;
	  				}
 				}
 				$number++;
 			}
 			$graf.= ']}';
 			
 		}
  	 	 
  	 	/* 
		 
		for($i=0; $i<$this->nbrSerie; $i++)
 		{ 
	        if($i > 0) echo ',';
	        
	        echo '{ "data": serie'.$i.', "label": "'.$this->tabtext[$i].'" }';
		}	*/        
			  
		$graf.= '],'.$this->getOptions().');';    
		
	 	$graf.='});</script>';
	  
		$this->creatImg();
		$this->color();
		$this->creatGraph();
		$this->statementValue();
		return $graf;
		if($this->value1px==0){ return("<br /><br />Aucune valeur disponible sur cette recherche"); }  // Si aucune valeur dispo pr cette periode 
		
		if($this->margeLegend>0)  $this->traceLegende();
		if ($this->typeGraph=='courbe'){
			$this->traceCourbe();
		}
		else{
			$this->traceHisto(); 
		}
		
   		imagepng($this->img,$this->nomImg.".png");
   		//echo '<img USEMAP="#map'.substr($this->nomImg,5,14).substr($this->nomImg,-1).'" src="'.$this->nomImg.'.png" border=0>'."\n\n";
   		//echo '<map name="map'.substr($this->nomImg,5,14).substr($this->nomImg,-1).'">'."\n".$this->bufferMAP.'</map>';
   		chmod($this->nomImg.'.png',0755);
		imagedestroy($this->img);
		
	}


/**
 * Enregistrement du graphique et des ses outils (légende, reperes, map, etc...)
 * 
 * @return void
 */	
	function Save(){
		$this->parametrage();
		$this->creatImg();
		$this->color();
		$this->creatGraph();
		$this->statementValue();
		if($this->value1px==0){ return(""); }  // Si aucune valeur dispo pr cette periode
		if($this->margeLegend>0)  $this->traceLegende();
		if ($this->typeGraph=='courbe'){
			$this->traceCourbe();
		}
		else{
			$this->traceHisto();
		}
		imagepng($this->img,$this->nomImg.".png");
   		chmod($this->nomImg.'.png',0755);
		imagedestroy($this->img);
	}
	
}

?>
