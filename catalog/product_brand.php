<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Liste des produits patr marque
 */
 
include_once('../objects/classes.php');
include_once('navigation.php'); 
require_once 'PEAR.php';
include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );


if(isset($_REQUEST['IdBrand'])){
	$IdBrand =$_REQUEST['IdBrand'];
}else{
	$IdBrand=1;
}

$rsb=DBUtil::query("SELECT brand FROM brand WHERE idbrand=$IdBrand LIMIT 1");

$title=$rsb->fields("brand");

class IndexController extends WebPageController{};
$charte_brands = new IndexController( "product/product_liste.tpl" , $title );

$lang = Session::getInstance()->getLang();

$NbBrandProductsByPages=DBUtil::getParameter("nb_brand_products_by_pages");
$NbBrandProductsByPages=20;

if( isset( $_SESSION[ "buyerinfo" ] ) && isset( $_SESSION[ "buyerinfo" ][ "catalog_right" ] ) )
	$catalog_right = $_SESSION[ "buyerinfo" ][ "catalog_right" ];
else $catalog_right = 0;

$query = "
SELECT p.idproduct, 
	p.name$lang AS name,
	p.description$lang AS description,
	p.desc_tech$lang AS desc_tech,
	p.desc_more$lang AS desc_more,
	p.text_commercial$lang AS text_commercial,
	p.space_msg_1,
	p.space_msg_2,
	p.space_msg_3,
	p.space_msg_4,
	p.space_msg_5,
	p.space_msg_6,
	p.logo_1,
	p.logo_2,
	p.logo_3,
	p.logo_4,
	p.logo_5,
	p.idsupplier
FROM product p, detail d
WHERE idbrand = $IdBrand
AND p.available = 1
AND p.catalog_right <= '$catalog_right'
AND p.idproduct=d.idproduct
AND p.idproduct>0
AND d.stock_level<>0
GROUP BY p.idproduct
ORDER BY p.display_product_order ASC";

if(isset($_REQUEST["CurrentPageNumberP"])){
	$newlimit=$NbBrandProductsByPages*$_REQUEST["CurrentPageNumberP"];
	$oldlimit=($newlimit-$NbBrandProductsByPages);
	$limit = " LIMIT $oldlimit, $NbBrandProductsByPages";

}else{
	$limit = " LIMIT $NbBrandProductsByPages";
	
}

$rsb=DBUtil::query("SELECT brand FROM brand WHERE idbrand=$IdBrand LIMIT 1");

$title=$rsb->fields("brand");
$charte_brands->setData('title',$title);


$query.=$limit;
$rs = DBUtil::query($query);
$i=0;
$NbAllProducts=$rs->RecordCount();
while( !$rs->EOF() ){

	$idproduct 				= $rs->fields( "idproduct" );
	$referenceCount = PRODUCT::getAvailableReferenceCount( $idproduct );
	
	if( $referenceCount ){
		$listFields[$i]["idproduct"]=$idproduct;
	
		$productName 			= $rs->fields( "name" );
		$listFields[$i]["name"]=$productName;

		$description 			= $rs->fields( "description" );
		$listFields[$i]["description"]=$description;
		
		$desc_tech				= $rs->fields( "desc_tech" );
		$listFields[$i]["desc_tech"]=$desc_tech;
		
		$desc_more				= $rs->fields( "desc_more" );
		$listFields[$i]["desc_more"]=$desc_more;
		
		$text_commercial		= $rs->fields( "text_commercial" );
		$listFields[$i]["text_commercial"]=$text_commercial;
		
		$space_msg_1			= $rs->fields( "space_msg_1" );
		$listFields[$i]["space_msg_1"]=$space_msg_1;
		
		$space_msg_2			= $rs->fields( "space_msg_2" );
		$listFields[$i]["space_msg_2"]=$space_msg_2;
		
		$space_msg_3			= $rs->fields( "space_msg_3" );
		$listFields[$i]["space_msg_3"]=$space_msg_3;
		
		$space_msg_4			= $rs->fields( "space_msg_4" );
		$listFields[$i]["space_msg_4"]=$space_msg_4;
		
		$space_msg_5			= $rs->fields( "space_msg_5" );
		$listFields[$i]["space_msg_5"]=$space_msg_5;
		
		$space_msg_6			= $rs->fields( "space_msg_6" );
		$listFields[$i]["space_msg_6"]=$space_msg_6;
		
		$logo_1					= $rs->fields( "logo_1" );
		$listFields[$i]["logo_1"]=$logo_1;
		
		$logo_2					= $rs->fields( "logo_2" );
		$listFields[$i]["logo_2"]=$logo_2;
		
		$logo_3					= $rs->fields( "logo_3" );
		$listFields[$i]["logo_3"]=$logo_3;
		
		$logo_4					= $rs->fields( "logo_4" );
		$listFields[$i]["logo_4"]=$logo_4;
		
		$logo_5					= $rs->fields( "logo_5" );
		$listFields[$i]["logo_5"]=$logo_5;
		
		$idsuplpier 			= $rs->fields( "idsupplier" );
		$listFields[$i]["idsupplier"]=$idsuplpier;
		
		//photos
		
		$listFields[$i]["imageURL"] = URLFactory::getHostURL() . URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_MAX_SIZE );
		$listFields[$i]["imagePath"] = URLFactory::getProductImagePath( $rs->fields( "idproduct" ) );
		$listFields[$i]["thumbURL"] = URLFactory::getHostURL() . URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_DEFAULT_SIZE );
		$listFields[$i]["thumbPath"] = URLFactory::getProductImagePath( $rs->fields( "idproduct" ) );
		$listFields[$i]["thumbURL_2"] = URLFactory::getHostURL() . URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_DEFAULT_SIZE, 1 );
		$listFields[$i]["thumbPath_2"] = URLFactory::getProductImagePath( $rs->fields( "idproduct" ), 1 );
		$listFields[$i]["imageDocURL"] = URLFactory::getHostURL() . URLFactory::getProductDataSheetURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_MAX_SIZE );
		$listFields[$i]["imageDocPath"] = URLFactory::getProductDataSheetPath( $rs->fields( "idproduct" ) );
		
		$visionImages = array();
		$p = 0;
		while( $image = URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_DEFAULT_SIZE, $p ) ){
			
			$visionImages[] = URLFactory::getHostURL() . $image;
			$p++;
			
		}
		
		$listFields[$i]["visionImages"]=$visionImages;
		
		//zooms

		$zooms = array();
		$p = 1;
		while( $p < 4 ){
			
			$k = $p - 1;
			
			if( $image = URLFactory::getProductImageURI( $rs->fields( "idproduct" ), URLFactory::$IMAGE_MAX_SIZE, $k ) )
				$zooms[ "Focus $p" ] = URLFactory::getHostURL() . $image;
				
			$p++;
			
		}
	
		$listFields[$i]["zooms"]=$zooms;

		//pictos
		$pictos = array();
		$a = 1;
		while( $a < 7 ){
		
			$varname = "logo_$i";
			
			if( !empty( $$varname ) && file_exists( "$GLOBAL_START_PATH/www/" . $$varname ) )
				$pictos[] = "$GLOBAL_START_URL/www/" . $$varname;
			
			$a++;
			
		}
		
		$listFields[$i]["pictos"]=$pictos;
		
		
		
		//produits associés
		$query = "
		SELECT associat_product 
		FROM associat_product 
		WHERE idproduct = '$idproduct'
		ORDER BY displayorder ASC";
		
		$rs2 = DBUtil::query( $query );
		
		if( $rs2 === false )
			die( "Impossible de récupérer les produits associés" );
	
		$associates = array();
		while( !$rs2->EOF() ){
	
			$associates[] = $rs2->fields( "associat_product" );
				
			$rs2->MoveNext();
			
		}

		$listFields[$i]["associates"]=$associates;


		
		//pubs
		$pubs = array();	
		for( $z = 1; $z < 7; $z++ ){
		
			$varname = "space_msg_$z";
			
			if( $$varname != 0 ){
			
				$query = "SELECT msg_commercial FROM msg_commercial WHERE idmsg_commercial = '" . $$varname . "' LIMIT 1";
				
				$rs2 = DBUtil::query( $query );
				
				if( $rs2 === false )
					die( "Impossible de récupérer les messages commerciaux" );
					
				if( $rs2->RecordCount() )
					$pubs[] = $rs2->fields( "msg_commercial" );
			
			}
	
		}
		
		$listFields[$i]["pubs"]=$pubs;
		
		//URLs
		$productURL = URLFactory::getProductURL( $idproduct, $productName );
		$listFields[$i]["productURL"]=$productURL;
		
		$pdfURL = URLFactory::getPDFURL( $idproduct, $productName );
		$listFields[$i]["pdfURL"]=$pdfURL;
	
		$lowerPrice = PRODUCT::getLowerPrice( $idproduct );
		$pxmin = $lowerPrice ? Util::priceFormat( $lowerPrice ) : false;
		$listFields[$i]["pxmin"]=$pxmin;
		
		//Puce orange
		$puceorange = "$GLOBAL_START_URL/www/img/puceorange.jpg";
		
		//Nombre de produits
		$nbref = $referenceCount = PRODUCT::getAvailableReferenceCount( $idproduct );
		$listFields[$i]["nbref"]=$nbref;
		
		//Texte commercial
		$lang = Session::getInstance()->getLang();
		$txtcom = PRODUCT::getProductAttribute( $idproduct, "text_commercial$lang" );
		$listFields[$i]["txtcom"]=$txtcom;
		
		//promotions
		if(PRODUCT::hasPromotionInStock( $idproduct )){
			$listFields[$i]["haspromos"]="yes";
		}else{
			$listFields[$i]["haspromos"]="";
		}
		
	}
	$i++;
	$rs->MoveNext();
}

$charte_brands->setData('listFields',$listFields);

$IdMaskProduct = "_13";
$charte_brands->setData('IdMaskProduct',$IdMaskProduct);

$Paginate=Paginate($ScriptName,$NbAllProducts,$NbBrandProductsByPages,$CurrentPageNumberP,$ParameterOutputArray);
$charte_brands->setData('Paginate',$Paginate);

$charte_brands->GetPromotions();
$charte_brands->GetCategoryLinks();
$charte_brands->GetBrands();
$charte_brands->GetTrades();
$charte_brands->GetProductHistory();

$charte_brands->output();

?>