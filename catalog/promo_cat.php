<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Page d'affichage des categories de promotions en cours
 */


include_once( dirname( __FILE__ ) . "/../objects/init.php" );

include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );

class IndexController extends WebPageController{};
$promotions_template = new IndexController( "promo_cat.tpl" , "Promotions par categories" );

$catalog_right = isset( $_SESSION['buyerinfo']['catalog_right'] ) ? $_SESSION['buyerinfo']['catalog_right'] : 0;
$lang = Session::getInstance()->getLang();

$limit = "";

$promoQuery = "
SELECT pr.reference,
	d.sellingcost,
	d.reference,
	d.idproduct
FROM promo pr, detail d, product p, category c
WHERE pr.begin_date<=NOW() 
AND pr.end_date>=NOW()
AND pr.reference = d.reference
AND d.idproduct = p.idproduct
AND p.idcategory = c.idcategory 
AND d.hidecost = 0
AND p.catalog_right <= '$catalog_right'
AND c.catalog_right <= '$catalog_right'
AND c.available = 1
AND p.available = 1
ORDER BY pr.display ASC";
			
$rs =& DBUtil::query( $promoQuery );

$results = array();
while( !$rs->EOF() ){
	
	$results[] = $rs->fields;
	
	$rs->MoveNext();
	
}

		
//completer les infos des resultats trouvés
$a = 0;
$ids = array();
while( $a < count( $results ) ){
	$ids[] = $results[ $a ][ "idproduct" ];

	$a++;
}

if(count($ids)>0){
	$promos = $promotions_template->GetProductList(0,0,$ids, 'array');
				
	//maintenant truk de porc, on trie par idcategories de promos
	if($categoryFiltered == true){
		$promocats = DBUtil::query("SELECT DISTINCT(pc.idpromo_cat), pc.display, promo_cat$lang as promo_cat_name, pc.image 
									FROM promo_cat pc, promo p
									WHERE pc.idpromo_cat = p.idpromo_cat
									ORDER BY pc.display ASC");
		
		//on le fait une foi pour les idcat_promo==0
		for($y=0;$y<count($promos);$y++){
			if($promos[$y]["idpromo_cat"] == 0){
				$promoCatTable[0]["products"][] = $promos[$y];
			}
		}
		$promoCatTable[0]["promo_cat_name"] = "";
		$promoCatTable[0]["promo_cat_image"] = "";
		
		if( $promocats->RecordCount() > 0 ){
			$i = 1;
			while(!$promocats->EOF()){
				
				for($x=0;$x<count($promos);$x++){
					if($promos[$x]["idpromo_cat"] == $promocats->fields("idpromo_cat")){
						$promoCatTable[$i]["products"][] = $promos[$x];
					}
				}
				$promoCatTable[$i]["promo_cat_name"] = $promocats->fields("promo_cat_name");
				$promoCatTable[$i]["promo_cat_image"] = $promocats->fields("image");
				
				$i++;
				$promocats->MoveNext();
			}
			
			
			
			//on jette les cat vides et bisou bisou bye bye la cigogne
			for($j=0;$j<count($promoCatTable)+1;$j++){
				if(!count($promoCatTable[$j]["products"])){
					unset($promoCatTable[$j]);
				}else{
					unset($promoCatTable[$j]["products"]);
				}
			}
			$promos = $promoCatTable;
		}
		//die(print_r($promos));
	}
	$this->setData("promo_cats",$promos);
}else{
	$this->setData("promo_cats", "" );
}


$promotions_template->GetCategoryLinks();
$promotions_template->GetBrands();
$promotions_template->GetTrades();
$promotions_template->GetProductHistory();

$promotions_template->output();

//-----------------------------------------------------------------------------------

?>