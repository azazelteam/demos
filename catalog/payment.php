<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */

function SecurePayment($Payment)
{

	$Result=0;
	$Base = &DBUtil::getConnection();
	$rs=& DBUtil::query("select securepay from payment where idpayment='$Payment'");
	if($rs->RecordCount())
	{
		$SecurePay=$rs->Fields("securepay");
		if($SecurePay) $Result=1;
	}
	return $Result;
}

function GetEstimatePaymentInfo($IdEstimate)
{

	$Base = &DBUtil::getConnection();
	$Estimate = array();
	$rs =& DBUtil::query("select idpayment,total_amount,comment from estimate where idestimate='$IdEstimate'");
	if($rs->RecordCount())
	{
		$Estimate["idpayment"]=$rs->Fields("idpayment");
		$Estimate["total_amount"]=$rs->Fields("idpayment");
		$Estimate["comment"]=$rs->Fields("idpayment");
	}
	return $Estimate;
}

function ConfirmPayment($Payment)
{

	$Result=0;
	$Base = &DBUtil::getConnection();
	$rs =& DBUtil::query("select status from payment where idpayment='$Payment'");
	if($rs->RecordCount())
	{
		$Status=$rs->Fields("status");
		if($Status=="confirme") $Result=1;
	}
	return $Result;
}

?>