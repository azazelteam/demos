<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet client pour afficher les factures du groupement
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class InvoiceController extends PageController {}
$controller = new InvoiceController( "/account/admin_grouping_invoice.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
	$controller->setData( "isInvoice", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
} else {
	$admin_order->setData('isLogin', "");
}

//-----------------------------------------------------formulaire de recherche-------------------------------------//

global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
		
	$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime() - 30*3600*24),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( ) )
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
	{
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
	}
?>
		
			
			
			<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				
				
			/* ]]> */
			</style>
			<?php
			$form = "";
			
			$form .= '<form action="/catalog/admin_grouping_invoice.php?all" method="post" id="SearchForm">
				
					<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />

					<div class="tableContainer">
					<table class="dataTable" width="100%">';
							
							
			$form .='	<tr>
					<th><span style="font-size: 12px;">Entre le</th>
					<td>
					<input type="text" name="start_date" id="start_date" class="calendarInput" value='. $postData[ "start_date" ] .' />';
			$form .= '	<a href="javascript:calendar(start_date)" id="test_a" ></a>';
					$displayIcon = true; $calledFunction = false;
	
			$form .= '	</td>';
							
			$form .='	<th><span style="font-size: 12px;">Et le</th>
					<td>
					<input type="text" name="end_date" id="end_date" class="calendarInput" value='. $postData[ "end_date" ] .' />';
			$form .= '	<a href="javascript:calendar(end_date)" id="test_b" ></a>';

			$form .= '
					</table>
					</div>
					<div class="submitButtonContainer" style="float:right;padding-right: 200px;">
					<input type="submit" class="blueButton" value="Rechercher" />
					</div>
					</div>
					<input type="hidden" name="detail" id="detail" value="1" />
					</form>';

			$controller->setData( "form",$form );

//-----------------------------------------------------formulaire de recherche-------------------------------------//

$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
/* On recherche les factures */

$idgrouping = $GroupingCatalog->getGrouping( "idgrouping" )? $GroupingCatalog->getGrouping( "idgrouping" ) : 0;


$query ="";
		if(isset($_POST["list_buyer"]) && !empty($_POST["list_buyer"]))
		{
		
		$nb_buyer = count($_POST["list_buyer"]);
		
			if($nb_buyer == 1)
			{
				$query.= "AND b.idbuyer = '". $_POST["list_buyer"][0] ."'";
			
			}
			else
			{
			
				$query.= "AND ( b.idbuyer = '". $_POST["list_buyer"][0] ."'";
			
					for ($i=1;$i<count($_POST["list_buyer"])-1;$i++)
					{
					$query.= " OR b.idbuyer = '". $_POST["list_buyer"][$i] ."'";
				
					}
			
			$query.= " OR b.idbuyer = '". $_POST["list_buyer"][count($_POST["list_buyer"])-1] ."')";
				
			}
		
		
		}

//---------------------------------------Recherche sur la date-----------------------------------//
if (isset ($_POST["start_date"]))
$start = $_POST["start_date"];
else
$start =$postData[ "start_date" ];
		
$start = explode("-", $start);
	
$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
if(isset($_POST["end_date"]))
$end = $_POST["end_date"];
else
$end =$postData[ "end_date" ];
		
$end = explode("-", $end);
		
$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
$where ="";
$where .= " AND (bb.DateHeure BETWEEN '$start_date' AND '$end_date' ) ";

$query = "SELECT bb.idbilling_buyer , b.company,
		b.idbuyer FROM `billing_buyer` bb, buyer b
WHERE bb.idbuyer = b.idbuyer
AND b.idgrouping = $idgrouping
".$where."
ORDER BY bb.DateHeure DESC";

$rs =& DBUtil::query( $query );

$array_invoice = array();
$mois = array("","janvier","f&eacute;vrier","mars","avril","mai","juin","juillet","ao&ucirc;t","septembre","octobre","novembre","d&eacute;cembre");


/* On vérifie qu'il y existe au moins une facture... */
if( $rs->RecordCount() ){
	$controller->setData('NoResults',0);
} else {
	$controller->setData('NoResults',1);
}

/* On parcours les factures... */
for($i=0; $i<$rs->RecordCount(); $i++){
			
	$invoice = new Invoice($rs->fields( "idbilling_buyer" ));
	
	$array_invoice[$i]["idinvoice"] = ( $rs->fields( "idbilling_buyer" ) );
	
	$bl = array();
	$query = "SELECT idbl_delivery FROM bl_delivery WHERE idbilling_buyer=".intval( $rs->fields( "idbilling_buyer" ) );
	$rsbl=& DBUtil::query($query);
	
	for($j=0; $j<$rsbl->RecordCount(); $j++){
		$bl[$j] = intval( $rsbl->fields("idbl_delivery") );
		$rsbl->MoveNext();
	}	
	
	$array_invoice[$i]["bls"] = $bl;	
	
	list($datedev,$heure)=explode(" ", $invoice->get("DateHeure") );
	list($y,$m,$d)=explode("-",$datedev);
	$dateinvoice = $d." ".$mois[intval($m)]." ".$y;
	$array_invoice[$i]["idbuyer"] =$rs->fields("idbuyer") ;
	$array_invoice[$i]["company"] = Util::checkEncoding($rs->fields("company"));
	$array_invoice[$i]["DateHeure"] =GenerateHTMLForToolTipBox(intval($rs->fields( "idbilling_buyer" )),$dateinvoice) ;
	$array_invoice[$i]["status"] = Util::doNothing(GenerateHTMLForToolTipBox(intval($rs->fields( "idbilling_buyer" )),Dictionnary::translate( $invoice->get( "status" ) )));
	
	$array_invoice[$i]["total_amount_ht"] =GenerateHTMLForToolTipBox(intval($rs->fields( "idbilling_buyer" )), Util::priceFormat($invoice->getNetBill()));
	
	list($y,$m,$d)=explode("-",$invoice->get("deliv_payment"));
	$deliv = $d." ".$mois[intval($m)]." ".$y;
	
	$array_invoice[$i]["echeance"] = $deliv;
	$rs->MoveNext();
	
}

/* On passe le tableau des factures au tpl */
$controller->setData('array_invoice',$array_invoice);

/* On ferme l'envoi de données */
$controller->output();


function GenerateHTMLForToolTipBox($idbillling_buyer, $value=0){
	
	global $GLOBAL_START_URL;
	

	$query = " SELECT idarticle,
				reference,
				summary,
				quantity,
				unit_price,
				ref_discount,
				discount_price
				FROM  `billing_buyer_row` 
				WHERE
				idbilling_buyer = '". $idbillling_buyer ."'
				
	
			";	
	
	$rs = & DBUtil::query( $query );
	
		
	$html = ' <link type="text/css" rel="stylesheet" href="'.$GLOBAL_START_URL.'/css/catalog/contents.css" />';
	$html .= "<span onmouseover=\"document.getElementById('pop$idbillling_buyer').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idbillling_buyer').className='tooltip';\" style=\"position:relative; display:block;\">" .$value." ";
	
		
	$html .= "<span id='pop$idbillling_buyer' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">";

		
		$html .= "<div style=\"font-size: 12px;\">";
		$html.= "<table>
				<tr>
					<th><span style=\"font-size: 12px;\">Icone</th>
					<th><span style=\"font-size: 12px;\">R&eacute;f&eacute;rence</th>
					<th><span style=\"font-size: 12px;\">D&eacute;signation</th>
					<th><span style=\"font-size: 12px;\">D&eacute;lai de livraison</th>
					<th><span style=\"font-size: 12px;\">Prix TTC</th>
					<th><span style=\"font-size: 12px;\">Remise</th>	
					<th><span style=\"font-size: 12px;\">Prix remis&eacute; TTC</th>
					<th><span style=\"font-size: 12px;\">Quantit&eacute;</th>
					<th><span style=\"font-size: 12px;\">Prix total TTC</th>

				</tr>";
				
				
				 while( !$rs->EOF ){
				$Icon = URLFactory::getReferenceImageURI( $rs->fields("idarticle") );;
				$reference = $rs->fields("reference");
				$summary = Util::doNothing($rs->fields("summary"));
				$delivdelay = DBUtil::getDBValue( "delay_1", "delay", "iddelay", $rs->fields("delivdelay") );
				$unit_price = Util::priceFormat($rs->fields("unit_price"));
				$quantity = $rs->fields("quantity");
				$ref_discount = ($rs->fields("ref_discount") > 0) ? Util::rateFormat($rs->fields("ref_discount")) : "-" ;
				$discount_price = Util::priceFormat($rs->fields("discount_price"));
				$total = Util::priceFormat($discount_price * $quantity);

				 
				 $html .="
				 <tr>
				 <td>
				 <img src='". $Icon ."' width='40' alt='Icone'>
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $reference ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$summary."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $delivdelay ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $unit_price ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $ref_discount ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$discount_price."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$quantity."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $total ."
				 </td>
				 </tr>";
				 
				 $rs->MoveNext();
	}
				 
		 
				 
	$html.="			 </table>
				
				";
		
		
		
		$html .= "<br>";
		
	
	$html .= "
</div>
</div>
</div>
</span>
</span>";
	
	return $html;
}

?>
?>
<script type="text/javascript">
$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_a').click();
});

$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_b').click();
});

</script>
<script type="text/javascript">

/* <![CDATA[ */
function calendar(param){
$(document).ready(function() { 

	$(param).datepicker({
		buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		showAnim: 'fadeIn',
		showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
		onClose: function(){
			<?php echo $calledFunction ?>( this );
		}
		<?php } ?>
	});
	
	
		
});
}
/* ]]> */
</script>