<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de l'odf fiche produit
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFProduct.php" );

$odf = new ODFProduct( intval( $_REQUEST[ "idestimate" ] ) );
$odf->exportODT( " N° " .intval( $_REQUEST[ "idestimate" ] ) . ".odt" );
?>