<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage de la 4ème étape de la commande OU paiement d'une commande par CB depuis "Mon compte"
 * Affichage de la dernière page de la commande pour la confirmation et sélection du mode de paiement
 */

define( "DELAYED_PAYMENT", isset( $_GET[ "idorder" ] ) );

include_once ('../objects/classes.php');
include_once ('payment.php');
include_once ('order.inc.php');

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/GiftToken.php" );
include_once( dirname( __FILE__ ) . "/../objects/Voucher.php" );
include_once( dirname( __FILE__ ) . "/../objects/RegulationsBuyer.php" );
include_once( dirname( __FILE__ ) . "/../objects/Sponsorship.php" );
include_once( dirname( __FILE__ ) . "/../objects/Encryptor.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/WebPageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );

User::fakeAuthentification();

//----------------------------------------------------------------------------

$_POST = Util::arrayMapRecursive( "strip_tags", $_POST );

//----------------------------------------------------------------------------
/* perte de login */

if( !Session::getInstance()->getCustomer() ){

	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );
	exit();

}

//----------------------------------------------------------------------------
/* créer la commande */

if( !DELAYED_PAYMENT && !Basket::getInstance()->getItemCount() ){
	
	header( "Location: $GLOBAL_START_URL/catalog/basket.php" );	
	exit();
	
}

//----------------------------------------------------------------------------
/*creation panier abandonné dans la table cart*/
if (isset($_POST["idpayment"]) && $_POST["idpayment"] == "1" && !isset($_SESSION[ "id_cart" ]))
{
	$current_session_id = session_id();
	$id_buyer = Session::getInstance()->getCustomer()->getId();
	$current_basket = Basket::getInstance();
	$id_cart = $current_basket->addCart($current_session_id, $id_buyer);
}
	
//----------------------------------------------------------------------------
/*Récupération commande panier abandonné*/
if(!empty($_POST["orderid"]))
{
	$Order = new Order( intval( $_POST[ "orderid" ] ));
	if ($Order->get("idpayment") != $_POST["idpayment"])
		$Order->set("idpayment", $_POST["idpayment"]);
	$OrderItems = $Order->getItems();
	$OrderItemsIdArticles = array();
	$OrderItemsArray = $OrderItems->toArray();
	foreach ($OrderItemsArray as $OrderItem)
	{
		$OrderItemsIdArticles[] = $OrderItem->get('idarticle');
	}
	if ( Basket::getInstance()->getItemCount() > $OrderItems->size() )
	{
		$basket_it = Basket::getInstance()->getItems()->iterator();
		while ($basket_it->hasNext())
		{
			$bitem = &$basket_it->next();
			$bitemArticle = $bitem->getArticle();
			if (!in_array($bitem->getArticle()->get( "idarticle" ), $OrderItemsIdArticles))
			{
				$Order->addArticle($bitemArticle, $bitem->getQuantity());
			}
		}
	}
}else
$Order = DELAYED_PAYMENT ? new Order( intval( $_GET[ "idorder" ] ) ) : TradeFactory::createOrderFromBasket();

//if( DELAYED_PAYMENT && ! in_array( $Order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD, Payment::$PAYMENT_MOD_FIANET_CB ) ) ){
	if( DELAYED_PAYMENT && ! in_array( $Order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD ) ) ){
	trigger_error( "Impossible de règler la commande par CB", E_USER_ERROR );
	exit();
	
}

if (isset($_POST["idpayment"]) && $_POST["idpayment"] == "1" && !isset($_SESSION[ "id_cart" ]) && $id_cart)
{
	$current_basket->insertOrderID($id_cart, $Order->getId());
}

//----------------------------------------------------------------------------
/* contrôleur */

class FinishController extends WebPageController{};

$order_finish = new FinishController( "finish.tpl" , 'Confirmation de votre commande' );

//----------------------------------------------------------------------------------------------
/* proxy commande */

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/OrderProxy.php" );
$order_finish->setData( "order", new OrderProxy( $Order ) );

//----------------------------------------------------------------------------------------------

$lang = "_1";

$order_finish->setData('idorder',$Order->getId());
$order_finish->setData('isLogin', "ok");
$order_finish->setData('n_order', isset( $_POST[ "n_order" ] ) ? strip_tags( stripslashes( $_POST[ "n_order" ] ) ) : "" );
$order_finish->setData('n_estimate', "" );
$order_finish->setData('is_estimate', false );

$articlesET = 0.0;
$it = $Order->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();

$articlesATI = 0.0;
$it = $Order->getItems()->iterator();
while( $it->hasNext() )
	$articlesATI += $it->next()->getTotalATI();
	
/* ------------------------------------------------------------------------------------------------------------- */
/*
 * Bon de réduction
 */

include_once( dirname( __FILE__ ) . "/../objects/Voucher.php" );

if( isset( $_REQUEST[ "voucher_code" ] ) )
	if( DBUtil::query( "SELECT 1 FROM vouchers WHERE code = " . DBUtil::quote( stripslashes( $_REQUEST[ "voucher_code" ] ) ) . " LIMIT 1" )->RecordCount() )
		Basket::getInstance()->setUseVoucher( new Voucher( DBUtil::getDBValue( "idvoucher", "vouchers", "code", stripslashes( $_REQUEST[ "voucher_code" ] ) ) ) );
	
if( $voucher = Basket::getInstance()->getVoucher() ){
	
	$order_finish->setData( "voucher", ( object )array(
	
		"code" 				=> $voucher->get( "code" ),
		"creation_date" 	=> Util::dateFormat( $voucher->get( "creation_date" ), "d/m/Y" ),
		"expiration_date" 	=> Util::dateFormat( $voucher->get( "expiration_date" ), "d/m/Y" ),
		"rate" 				=> Util::rateFormat( $voucher->get( "rate" ) ),
		"min_order_amount" 	=> Util::priceFormat( $voucher->get( "min_order_amount" ) )
	
	));

	DBUtil::query( "INSERT IGNORE INTO `vouchers_order` ( idvoucher, idorder ) VALUES ( '" . $voucher->get( "idvoucher" ) . "', '" . $Order->get( "idorder" ) . "' )" );

	$Order->save();
	
}
else $order_finish->setData( "voucher", false );

$order_finish->setData( "voucher_code", isset( $_REQUEST[ "voucher_code" ] ) ? strip_tags( stripslashes( $_REQUEST[ "voucher_code" ] ) ) : false );

//----------------------------------------------------------------------------
/**
 * Réduction automatique à la première commande
 * Traité comme une remise commerciale ( Basket::fields[ "total_discount" ] et Basket::fields[ "total_discount_amount" ] )
 */

$idbuyer 					= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
$firstOrder 				= $idbuyer && DBUtil::query( "SELECT COUNT(*) AS `count` FROM `order` WHERE idbuyer = '$idbuyer' AND `status` NOT LIKE 'Cancelled'" )->fields( "count" ) == 1;
$firstOrderDiscountValue 	= round( floatval( DBUtil::getParameterAdmin( "first_discount_value" ) ), 2 );
$firstOrderDiscountType 	= DBUtil::getParameterAdmin( "first_discount_type" );

$order_finish->setData( "first_order", $firstOrder );

if( $firstOrder && $firstOrderDiscountValue > 0.0 ){
		
		switch( $firstOrderDiscountType ){
		
			case "amount" : 	
			
				$discount_amount 	= $firstOrderDiscountValue;
				$discount_rate 		= !B2B_STRATEGY ? $firstOrderDiscountValue * 100.0 / $articlesATI : $firstOrderDiscountValue * 100.0 / $articlesET;
				break;
								
			case "rate" : 	
			
				$discount_amount 	= !B2B_STRATEGY ? $articlesATI * $firstOrderDiscountValue / 100.0 : $articlesET * $firstOrderDiscountValue / 100.0;
				$discount_rate 		= $firstOrderDiscountValue;
				break;
								
			default : trigger_error( "Type de remise inconnu", E_USER_ERROR );
				
		}
	
	
	$Order->set( "total_discount_amount", 	max( $discount_amount, 	$Order->get( "total_discount_amount" ) ) );
	$Order->set( "total_discount", 		max( $discount_rate, 	$Order->get( "total_discount" ) ) );

	$Order->save();
	
}

//----------------------------------------------------------------------------
/**
 * Chèque-cadeau
 * Considéré comme un mode de règlement donc l'utilisation du chèque-cadeau ne modifie pas les montants du panier
 */

$gift_token_amount 		= 0.0;
$gift_token_serial 		= isset( $_REQUEST[ "gift_token_serial" ] ) ? stripslashes( $_REQUEST[ "gift_token_serial" ] ) : "";
$isAvailableGiftToken 	= GiftToken::isAvailableGiftToken( $gift_token_serial );

$order_finish->setData( "gift_token_serial", 			$gift_token_serial );
$order_finish->setData( "gift_token_error", 			strlen( $gift_token_serial ) && !$isAvailableGiftToken ? Dictionnary::translate( "gift_token_unavailable" ) : ""  );
$order_finish->setData( "display_gift_token_serial", 	isset( $_REQUEST[ "gift_token_serial" ] ) );

$order_finish->setData( "gift_token_amount", 			0.0 );

/* utilisation d'un chèque-cadeau */

if( $gift_token_serial && $isAvailableGiftToken ){
		
	$query = "
	SELECT gtm.value 
	FROM gift_token_model gtm, gift_token gt 
	WHERE gt.serial = '" . Util::html_escape( $gift_token_serial ) . "'
	AND gtm.type LIKE '" . GiftToken::$TYPE_AMOUNT . "' 
	AND gt.idtoken_model = gtm.idmodel
	LIMIT 1";
	
	$rs =& DBUtil::query( $query );

	$gift_token_amount = $rs->RecordCount() ? round( $rs->fields( "value" ), 2 ) : 0.0;
	
	/* consommer le chèque cadeau */

	if( $gift_token_amount > 0.0 )
		DBUtil::query( "UPDATE gift_token SET idorder = '" . $Order->getId() . "', use_date = NOW() WHERE serial LIKE '" . Util::html_escape( $gift_token_serial ) . "' LIMIT 1" );
		
	$order_finish->setData( "gift_token_amount", Util::numberFormat( $gift_token_amount ) );
	
}

//----------------------------------------------------------------------------
/* remise auto */

$Order->set( "total_discount", $Order->get( "total_discount" ) );
$Order->set( "total_discount_amount", round( $articlesET * $Order->get( "total_discount" ) / 100.0, 2 ) );

//----------------------------------------------------------------------------
/* mode de paiement @todo : pouvoir en gérer plusieurs */

if( DELAYED_PAYMENT )
	$idpayment = $Order->get( "idpayment" );
else{
	
	if( isset( $_POST["idpayment"] ) && intval( $_POST["idpayment"] ) )
		$idpayment = intval( $_POST["idpayment"] );
	else if( $gift_token_amount > 0.0 )
		$idpayment = Invoice::$PAYMENT_MOD_GIFT_TOKEN;
	else $idpayment = DBUtil::getParameter( "default_idpayment" );

	$Order->set( "idpayment", $idpayment );
	
}

//----------------------------------------------------------------------------
/* statut commande @todo : DROP */

$net_to_pay = max( $Order->getNetBill() - $gift_token_amount, 0.0 );

//----------------------------------------------------------------------------
//adresses de livraison et de facturation

// $idbilling 	= isset( $_POST[ "idbilling" ] ) ? intval( $_POST["idbilling"] ) : $Order->get( "idbilling_adress" );
// $iddelivery = isset( $_POST[ "iddelivery" ] ) ? intval( $_POST["iddelivery"] ) : $Order->get( "iddelivery" );
include_once('addresses.inc.php');
$idbuyer 	= $Order->get( "idbuyer" );

if( !DELAYED_PAYMENT ){
	
	$forwardingAddress 	= $iddelivery ? new ForwardingAddress( $iddelivery ) : 	$Order->getCustomer()->getAddress();
	$invoiceAddress 	= $idbilling  ? new InvoiceAddress( $idbilling ) 	 : 	$Order->getCustomer()->getAddress();
	
	$Order->setForwardingAddress( $forwardingAddress );
	$Order->setInvoiceAddress($invoiceAddress);
	
	$Order->factorize();
	$Order->save();

}
	
if (ConfirmPayment( $Order->get( "idpayment" ) ) ){
	
	ConfirmOrder( $Order->getId() );
	$msgfromcommande = "";
	
}
else $msgfromcommande = VerifyOrder($Order->getId());

$msgfromcommande .= "Commande enregistrée";

//----------------------------------------------------------------------------	
//mail envoyé dans order.inc.php => ConfirmOrder()

include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );

if( $Order->get( "idpayment" ) == Payment::$PAYMENT_MOD_DEBIT_CARD ){
	$order_finish->setData('confirm_order',0);
	$_SESSION["idorder_secure"] = $Order->getId();
	
	$location = "$GLOBAL_START_URL/catalog/pay_secure.php";
	
	if( isset( $_REQUEST[ "gift_token_serial" ] ) || isset( $_REQUEST[ "voucher_code" ] ) ){
		
		$location .= "?";
		
		if( isset( $_REQUEST[ "gift_token_serial" ] ) )
			$location .= "gift_token_serial=" . stripslashes( $_REQUEST[ "gift_token_serial" ] );
			
		if( isset( $_REQUEST[ "voucher_code" ] ) ){
			
			if( isset( $_REQUEST[ "gift_token_serial" ] ) )
				$location .= "&";
				
			$location .= "voucher_code=" . stripslashes( $_REQUEST[ "voucher_code" ] );
			
		}
		
	}
	
}
else $order_finish->setData('confirm_order',1);

//paiement fianet cb ou fianet CB
/*
if($Order->get( "idpayment" )==Payment::$PAYMENT_MOD_FIANET_CB
	|| $Order->get( "idpayment" ) == Payment::$PAYMENT_MOD_FIANET_CREDIT ){
	
	include("$GLOBAL_START_PATH/fianet/fianet.php");

	//Creation d'une commande (voir commande.php);
	include("$GLOBAL_START_PATH/fianet/exemple_rnp/commande.php");
	
	$fianet_mode = DBUtil::getParameter( "fianet_mode" );
	
	if($fianet_mode=='test'){
		$enprod=false;
	}else{
		$enprod=true;
	}
	
	$typeIHM = $Order->get( "idpayment" ) == Payment::$PAYMENT_MOD_FIANET_CREDIT ? 2 : 1;
	
	$formulaire = $order->get_formular(null,null, array(), $typeIHM, $enprod, true);
	$formulaire = $order->get_formular(null,null, array(), 2, $enprod, true);
	
	 echo "<html><head></head><body>";                                         
    echo $formulaire;
    
     echo "</body></html>";   
		
	
}
*/
$order_finish->setData('displayhead','');
$order_finish->setData('displayfoot','');

//----------------------------------------------------------------------------

//buyer_read.tpl

if( $ScriptName != "order_secure.php"){
	$order_finish->setData('order_secure',0);
}else{
	$order_finish->setData('order_secure',1);
}

$order_finish->setData('buyer_company',$Order->getCustomer()->getAddress()->getCompany());
$order_finish->setData('buyer_title', DBUtil::getDBValue( "title_1", "title", "idtitle", $Order->getCustomer()->getContact()->getGender() ) );
$order_finish->setData('buyer_firstname',$Order->getCustomer()->getContact()->getFirstName());
$order_finish->setData('buyer_lastname',$Order->getCustomer()->getContact()->getLastName());
$order_finish->setData('buyer_adress',stripslashes( $Order->getCustomer()->getAddress()->getAddress() ) );

$adr2=stripslashes($Order->getCustomer()->getAddress()->getAddress2());
if(!empty($adr2)){
	$order_finish->setData('buyer_adress_2',$adr2."<br />");
}else{
	$order_finish->setData('buyer_adress_2','');
}

$order_finish->setData('buyer_zipcode',$Order->getCustomer()->getAddress()->getZipcode());
$order_finish->setData('buyer_city',$Order->getCustomer()->getAddress()->getCity());

//addresses_read.tpl

$order_finish->setData('d_buyer_company',$Order->getForwardingAddress()->getCompany());
$order_finish->setData('d_buyer_title',Util::getTitle( $Order->getForwardingAddress()->getGender()));
$order_finish->setData('d_buyer_firstname',$Order->getForwardingAddress()->getFirstName());
$order_finish->setData('d_buyer_lastname',$Order->getForwardingAddress()->getLastName());
$order_finish->setData('d_buyer_adress',stripslashes($Order->getForwardingAddress()->getAddress()));

$adr2=stripslashes($Order->getForwardingAddress()->getAddress2());
if(!empty($adr2)){
	$order_finish->setData('d_buyer_adress_2',$adr2."<br />");
}else{
	$order_finish->setData('d_buyer_adress_2','');
}

$order_finish->setData('d_buyer_zipcode',$Order->getForwardingAddress()->getZipcode());
$order_finish->setData('d_buyer_city',$Order->getForwardingAddress()->getCity());

$order_finish->setData('b_buyer_company',$Order->getInvoiceAddress()->getCompany());
$order_finish->setData('b_buyer_title',Util::getTitle( $Order->getInvoiceAddress()->getGender()));
$order_finish->setData('b_buyer_firstname',$Order->getInvoiceAddress()->getFirstName());
$order_finish->setData('b_buyer_lastname',$Order->getInvoiceAddress()->getLastName());
$order_finish->setData('b_buyer_adress',stripslashes($Order->getInvoiceAddress()->getAddress()));

$adr2=stripslashes($Order->getInvoiceAddress()->getAddress2());
if(!empty($adr2)){
	$order_finish->setData('b_buyer_adress_2',$adr2."<br />");
}else{
	$order_finish->setData('b_buyer_adress_2','');
}

$order_finish->setData('b_buyer_zipcode',$Order->getInvoiceAddress()->getZipcode());
$order_finish->setData('b_buyer_city',$Order->getInvoiceAddress()->getCity());

$order_finish->setData('iddelivery',$iddelivery);
$order_finish->setData('idbilling',$idbilling);

// include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/AddressProxy.php" );

// $confirm_order->setData('forwardingAddress', new AddressProxy( $Order->getForwardingAddress() ) );
// $confirm_order->setData('invoiceAddress', new AddressProxy( $Order->getInvoiceAddress() ) );

/* règlement total par chèque cadeau ou avoir */
	
if( $Order->getNetBill() - $gift_token_amount <= 0.0 ){
	
	//@todo : `order`.`paid` deprecated si on gère des paiements partiels car possibilité de n'enregistrer qu'une seule date de paiement

	$Order->set( "paid", 1 );
	$Order->set( "idpayment", $gift_token_amount > 0.0 ? Payment::$PAYMENT_MOD_GIFT_TOKEN : Invoice::$PAYMENT_MOD_CREDIT );
	$Order->set( "idpayment_delay", 1 );
	$Order->set( "cash_payment", 0 ); //réservé pour la CB, ...
	$Order->set( "balance_date", date( "Y-m-d" ) );
	$Order->set( "balance_amount", $Order->getNetBill() );
	$Order->set( "status", Order::$STATUS_ORDERED );

	$Order->save();
	$Order->Unstock();
	
	TradeFactory::createSupplierOrders( $Order );
	//$invoice =TradeFactory::createInvoiceFromOrder( $Order ); //Note : les règlements sont automatiquement créés lors de la génération de la facture

	//mail de notification
	
	mailOrder( $Order );
	
}

//paiement cb
if( $Order->get( "idpayment" ) == 1 ){

	$Montant = DELAYED_PAYMENT ? $Order->getBalanceOutstanding() : round($Order->getNetBill() - $gift_token_amount,2);
	$Reference_Cde = urlencode($Order->getId());
	$sEmail = $Order->getCustomer()->getContact()->getEmail();
	$sDate = date("d/m/Y:H:i:s");
	
	$tpeVer = DBUtil::getParameterAdmin( "CMCIC_version" );
	
	switch ( $tpeVer ){
		case "3.0":
			include_once( dirname( __FILE__ ) . "/../objects/PaySecureV3.php" );			
			$pay_secure = new PaySecureV3();			
			$pay_secure->CreerFormulaireHmac( $sEmail , $Montant , $Reference_Cde , $sDate );			
			break;
			
		case "1.2open":
			include_once( dirname( __FILE__ ) . "/../objects/PaySecure.php" );			
			$pay_secure = new PaySecure();				
			$pay_secure->CreerFormulaireHmac( $Montant , $Reference_Cde );  			
			break;
		
		default :
			die( " TPE introuvable : La version n'est pas renseignée dans le paramètrage " );
			break;
	
	}
		
    exit();
	
}

//paiement paybox
if( (int)$Order->get( "idpayment" ) == 21 ){

	$Montant = DELAYED_PAYMENT ? $Order->getBalanceOutstanding() : round($Order->getNetBill() - $gift_token_amount,2);
	//var_dump($Montant);exit();
	$Montant = (float)$Montant * 100;
	$Reference_Cde = urlencode($Order->getId());
	$sEmail = $Order->getCustomer()->getContact()->getEmail();
	$sDate = date("d/m/Y:H:i:s");
	include_once( dirname( __FILE__ ) . "/../objects/Paybox.php" );			
	$paybox = new Paybox();			
	$paybox->checkForm($Montant, $Reference_Cde, $sEmail);
		
    exit();
	
}

//----------------------------------------------------------------------------
/* montants */

$articlesET = 0.0;
$it = $Order->getItems()->iterator();
while( $it->hasNext() )
	$articlesET += $it->next()->getTotalET();

$order_finish->setData( "remise_rate", 					Util::rateFormat( $Order->get( "total_discount" ) ) );
$order_finish->setData( "total_discount", 				Util::rateFormat( $Order->get( "total_discount" ) ) );
$order_finish->setData( "discount", 					Util::priceFormat( $Order->get( "total_discount_amount" ) ) );
$order_finish->setData( "total_discount_amount",		Util::priceFormat( $Order->get( "total_discount_amount" ) ) );
$order_finish->setData( "total_amount_ht", 				Util::priceFormat( $articlesET - $Order->get( "total_discount_amount" ) ) );
$order_finish->setData( "total_amount_ht_wb", 			Util::priceFormat( $Order->getTotalET() ) );
$order_finish->setData( "total_amount_ttc_wbi", 		Util::priceFormat( $Order->getNetBill() ) );
$order_finish->setData( "total_amount_ht_avant_remise", Util::priceFormat( $articlesET ) );
$order_finish->setData( "remise", 						$Order->get( "total_discount" ) > 0.0 );
$order_finish->setData( "net_to_pay", 					Util::numberFormat( max( $Order->getNetBill() - $gift_token_amount, 0.0 ) ) );

$order_finish->setData( "net_to_pay_ht", Util::priceFormat( $Order->get( "total_amount_ht" ) ) );//total ht total_amount
$order_finish->setData( "net_to_pays", Util::priceFormat( $Order->get( "total_amount" ) ) );//total ttc

/* frais de port */

if( $articlesET == 0.0 ){ //ne pas afficher les frais de port si prix cachés
	
	$order_finish->setData( "total_charge_ht", "-" );
	$order_finish->setData( "total_charge_ttc", "-" );
	
}
else{
	
	$chargesET 	= $Order->getChargesET();
	$chargesATI = $Order->getChargesATI();
	
	$order_finish->setData( "total_charge_ht", $chargesET > 0.0 ? Util::priceFormat( $chargesET ) : "FRANCO" ); 
	$order_finish->setData( "total_charge_ttc", $chargesATI > 0.0 ? Util::priceFormat( $chargesATI ) : "FRANCO" ); 
	
	$order_finish->setData( "franco", $chargesET == 0.0 );
}

//----------------------------------------------------------------------------
//articles_read

$hasLot = false;
$i = 0;
$count = $Order->getItemCount();
while( $i < $count && !$hasLot ){
	
	$hasLot = $Order->getItemAt( $i)->get( "lot" ) > 1;
	$i++;
	
}

if($hasLot)
	$colspan=10;
else
	$colspan=9;

$order_finish->setData('colspan',$colspan); //lol
$order_finish->setData('haslot',$hasLot);

$basket_article = array();
$hasDiscount = false;

for ($i=0 ; $i < $Order->getItemCount() ; $i++ ){
	
	$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $Order->getItemAt($i)->get("idarticle") );
	
	$basket_article[$i]["i"] = $i;
	$basket_article[$i]["icon"] = URLFactory::getReferenceImageURI($Order->getItemAt($i)->get("idarticle" ));
	$basket_article[$i]["productname"] = html_entity_decode(html_entity_decode(DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct )));
	$basket_article[$i]["idarticle"] = $Order->getItemAt($i)->get("idarticle");
	$basket_article[$i]["reference"] = $Order->getItemAt( $i)->get( "reference" );
	$basket_article[$i]["stock_level"] = DBUtil::getDBValue( "stock_level", "detail", "idarticle", $Order->getItemAt( $i)->get( "idarticle" ) );
	$basket_article[$i]["quantity"] = $Order->getItemAt( $i)->get( "quantity" );
	$basket_article[$i]["designation"] = Util::doNothing(html_entity_decode(html_entity_decode($Order->getItemAt( $i)->get( "designation" ))));
	$basket_article[$i]["delay"] = DBUtil::getDBValue( "delay" . "_1" , "delay", "iddelay", $Order->getItemAt( $i)->get( "delivdelay" ));
	$basket_article[$i]["discount_price"] = Util::priceFormat( $Order->getItemAt( $i)->get( "discount_price" ) );
	$basket_article[$i]["lot"] = $Order->getItemAt( $i)->get( "lot" );
	$basket_article[$i]["unit"] = $Order->getItemAt( $i)->get( "unit" );
	$basket_article[$i]["puht"] = Util::priceFormat($Order->getItemAt( $i)->get( "unit_price" ));
	$basket_article[$i]["total_price"] = Util::priceFormat( $Order->getItemAt( $i)->get( "quantity" ) * $Order->getItemAt( $i)->get( "discount_price" ) );
	$basket_article[$i]["unit_price"] = Util::priceFormat($Order->getItemAt( $i)->get( "unit_price" ) );
	$basket_article[$i]["ref_discount"] = Util::rateFormat($Order->getItemAt( $i)->get( "ref_discount" ) );
	$basket_article[$i]["unit_price_ati"] = Util::priceFormat( $Order->getItemAt( $i )->get( "unit_price" ) * ( 1.0 + $Order->getItemAt( $i )->get( "vat_rate" ) / 100.0 ) ); 
	$basket_article[$i]["discount_price_ati"] = Util::priceFormat( $Order->getItemAt( $i )->get( "unit_price" ) * ( 1.0 + $Order->getItemAt( $i )->get( "vat_rate" ) / 100.0 ) * ( 1.0 - $Order->getItemAt( $i )->get( "ref_discount" ) / 100.0 ) ); 
	$basket_article[$i]["total_discount_price_ati"] = Util::priceFormat( $Order->getItemAt( $i )->get( "quantity" ) * $Order->getItemAt( $i )->get( "unit_price" ) * ( 1.0 + $Order->getItemAt( $i )->get( "vat_rate" ) / 100.0 ) * ( 1.0 - $Order->getItemAt( $i )->get( "ref_discount" ) / 100.0 ) );
		
	$hasDiscount |= $Order->getItemAt( $i)->getDiscountRate() > 0.0;

}

$order_finish->setData('basket_articles',$basket_article);

$order_finish->setData('hasdiscount',$hasDiscount);

//----------------------------------------------------------------------------
//modes de paiement

$mode_payment_used ="";
if( $Order->getNetBill() - $gift_token_amount <= 0.0 ){ 

	//net à payer = 0.0
	
	/**
	 * @todo le chèque-cadeau ne peut pas être utilisé comme mode de paiement ( pas d'entrée dans la table payment ) 
	 * car si le montant du chèque ne couvre pas le net à payer il doit pouvoir être combiné avec un autre mode 
	 * de paiement complémentaire et on ne gère pas plusieurs mode de paiement différents pour une même commande 
	 * dans le front office.
	 */
	 
	$radio = array();

}
else{
	
	$query = "SELECT idpayment, name$lang FROM payment WHERE use_web=1 AND payment_code NOT LIKE 'GIFT_TOKEN'";
	
	$rs = DBUtil::query( $query );
	
	$i = 0;
	
	$radio = array();
	
	if( DELAYED_PAYMENT )
		$payment = $Order->get( "idpayment" );
	else $payment = isset( $_POST[ "idpayment" ] ) ? intval( $_POST[ "idpayment" ] ) : "";
	
	while( !$rs->EOF() ){
	
		$idpayment = $rs->fields["idpayment"];
		$payment_name = htmlentities( $rs->fields["name$lang"] );
		
		if( $payment == $idpayment){
			$mode_payment_used = $payment_name;	
		}
		
		$radio[$i]["idpayment"] = $idpayment;
		$radio[$i]["payment_name"] = $payment_name;
			
		$rs->MoveNext();
		$i++;
			
	}
	
	$order_finish->createElement( 'idpayment' , intval( $payment ) );
	
}
$order_finish->setData('payment',"");
$order_finish->setData('annul',"");
$order_finish->setData('franco',"");

$order_finish->setData('radio',$radio);

$order_finish->setData('mode_payment_used',$mode_payment_used);

$order_finish->setData('basic_payment','ok');

//tracker
$effiliation = DBUtil::getParameter('use_effiliation_tag');

if($effiliation){
	
	$articlesET = 0.0;
	$it = $Order->getItems()->iterator();
	while( $it->hasNext() )
		$articlesET += $it->next()->getTotalET();
	
	$order_finish->setData('tracker','ok');
	$order_finish->setData('tracker_idorder',$Order->getId());
	$order_finish->setData('tracker_ht',round($articlesET - $Order->get( "total_discount_amount" ),2));
}


$order_finish->GetPromotions();
$order_finish->GetCategoryLinks();
$order_finish->GetBrands();
$order_finish->GetTrades();
$order_finish->GetProductHistory();

//------------------------------------------------------------------------------------------

include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/OrderProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CustomerProxy.php" );

$order_finish->setData( "order", new OrderProxy( $Order ) );
$order_finish->setData( "customer", new CustomerProxy( $Order->getCustomer() ) );

//------------------------------------------------------------------------------------------

if(isset($_SESSION['id_cart']))
{
	$query_status = "UPDATE cart SET `status`=1 WHERE `id_cart`=".$_SESSION['id_cart'];
	DBUtil::getConnection()->Execute($query_status);
	unset($_SESSION['id_cart']);
}
	

$order_finish->output();

//------------------------------------------------------------------------------------------

function mailOrder( &$Order ){
	
	global $GLOBAL_START_URL, $GLOBAL_START_PATH;
	
	//infos client, commercial et admin
		
	$AdminName 	= DBUtil::getParameterAdmin( "ad_name" );
	$AdminEmail = DBUtil::getParameterAdmin( "ad_mail" );
	$iduser 	= DBUtil::getParameter( "contact_commercial" );

	$rs =& DBUtil::query( "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
	
	$CommercialName 	= $rs->fields( "firstname" ) . " " . $rs->fields( "lastname" );
	$CommercialEmail 	= $rs->fields( "email" );
	
	$BuyerName 	= $Order->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );
			
	//charger le modèle de mail

	include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	$editor->setTemplate( "ORDER_NOTIFICATION_1" );

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseOrderTags( $Order->getId() );
	$editor->setUseBuyerTags( $Order->get( "idbuyer" ), $Order->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
				
	//envoi du mail
	
	include_once( "$GLOBAL_START_PATH/objects/mailerobject.php" );

	$EVM_Mailer = new EVM_Mailer();
	$EVM_Mailer->set_message_type( "html" );
	$EVM_Mailer->set_sender( $AdminName, $AdminEmail );
	$EVM_Mailer->add_recipient( $BuyerName, $BuyerEmail );
	$EVM_Mailer->add_CC( $CommercialName, $CommercialEmail );

	$EVM_Mailer->set_message( $Message );
	$EVM_Mailer->set_subject( $Subject );
	
	$EVM_Mailer->send();
	
	if( $EVM_Mailer->is_errors() )
		trigger_error( "Mail notification failure", E_USER_ERROR );
						
}

?>