<?php 
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet suivi des échéances clients
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../objects/Order.php" );  
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" ); 
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");

class AdminOrderController extends PageController{};
$admin_order = new AdminOrderController( "product_term_buyer.tpl" );

$lang = "_1";

/**************************************************
 *********************ACCOUNT**********************
 **************************************************/
if( Session::getInstance()->getCustomer() ) {
	$admin_order->setData('isLogin', "ok");
	
	$GroupingCatalog = new GroupingCatalog();
	$admin_order->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$admin_order->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$admin_order->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$admin_order->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$admin_order->setData( "contact_lastname", $salesman->get("lastname") );
	$admin_order->setData( "contact_firstname", $salesman->get("firstname") );
	$admin_order->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$admin_order->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$admin_order->setData( "contact_email", $salesman->get("email") );
	
	$admin_order->setData( "isOrder", true );
	
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$admin_order->setData( "contact_grouping", $contact_grouping );
	
} else {
	$admin_order->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$admin_order->setData('com_step_empty', "ok");
	} else {
		$admin_order->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$admin_order->setData('issetPostLoginAndNotLogged', "ok");
		$admin_order->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$admin_order->setData('issetPostLoginAndNotLogged', "");
	}
	$admin_order->setData('scriptName', $ScriptName);
	$admin_order->setData('translateLogin',Dictionnary::translate('login'));
	$admin_order->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$admin_order->setData('img', "acceder_compte.png");
		$admin_order->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$admin_order->setData('img', "poursuivre.png");
		$admin_order->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
}

//-------------------------------------------upload fichier --------------------------------------------------------------------------
//-----------------------------------------------------------------------------------------------------------------------------------------------
if( isset( $_POST[ "uploader" ] ) && $_POST[ "uploader" ] == 1 ){
	global $GLOBAL_START_PATH;
	
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			$idorder =  $_POST[ "idorder" ] ;
			$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
		//error_reporting( 0 );
   			//die( "<br /><br /><span style=\"color:#FF0000;size:16px;font-family: Arial, Helvetica, sans-serif;\">Le format de la documentation doit être .pdf, .PDF</span><br /><br />" );
			
	$admin_order->setData('erreurfichier', "Le format de la documentation doit être .pdf, .PDF");
	
	
	//exit();
			
		}
		else{
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term.$extension;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term.$extension;
	//$name= $idorder."_".$idproduct_term.$extension;
		include_once( "../config/init.php" );
		$id =1;
	$name = $idorder."_".$idproduct_term.$extension;
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name . "' 
	WHERE idproduct_term = '" . $idproduct_term . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
		//rep annêe	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	//echo "<br /><br /><span style=\"color:#FF0000;size:16px;font-family: Arial, Helvetica, sans-serif;\">Le document a été enregistré avec succée...</span><br /><br />";
				$admin_order->setData('fichieruploader', "Le document a été enregistré avec succée...");
	}
	//exit( $ret === false ? "" : "1" );	
	}
		
}






//-----------------------------------------------------------------------------------------------------------------------------------------------



if( isset( $_GET[ "update" ] ) && $_GET[ "update" ] == "n_service" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_name = '" . Util::html_escape( stripslashes( $_GET[ "n_service" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_GET[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_GET[ "update" ] ) && $_GET[ "update" ] == "n_serial" ){
	
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET serial_number = '" . Util::html_escape( stripslashes( $_GET[ "n_serial" ] ) ) . "' 
	WHERE idproduct_term = '" . intval( $_GET[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}
if( isset( $_GET[ "update" ] ) && $_GET[ "update" ] == "service_extern" ){

	if ($_GET[ "service_extern" ] == 0)$service_ext = 1;
	else $service_ext = 0;
	include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET service_extern = '" . Util::html_escape( stripslashes( $service_ext )) . "' 
	WHERE idproduct_term = '" . intval( $_GET[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}

//------------------------------------------------------------------------------------------------

if( isset( $_GET[ "update" ] ) && $_GET[ "update" ] == "availability" ){
include_once( "../config/init.php" );
	global $GLOBAL_START_PATH;
	
	
	$idorders = $_GET[ "idproduct_term" ];
	$availability_date = $_GET[ "availability" ];
	$availability_date = date("Y-m-d", strtotime($availability_date));
	$query = "
		UPDATE product_term 
		SET term_date = '$availability_date' 
		
		WHERE idproduct_term = '$idorders'
		LIMIT 1"
	;
	
		$ret = DBUtil::query( $query );
	
	exit( $ret === false ? "" : "1" );
}


$FormName = 'adm_order';



if( !empty( $_REQUEST["search"])||!empty( $_REQUEST["search_x"]) || isset( $_REQUEST[ "all" ] ) ){
	
	/**
	 * Traitement PHP du formulaire de recherche
	 */
	
	if( isset( $_REQUEST[ "all" ] ) ){
		//tri
		if(isset($_REQUEST[ "sort" ])){
			if($_REQUEST[ "sort" ] == "conf"){
				$join = " LEFT JOIN `order` o ON pt.idorder = o.idorder ";
				$orderby = " ORDER BY o.conf_order_date";
				$conf_link = '<a href="/catalog/product_term_buyer.php?all&sort=conf';
				$term_link = '<a href="/catalog/product_term_buyer.php?all&sort=term&dir=desc">Date &eacute;ch&eacute;ance<span class="sort_desc"></span></a>';
				if($_REQUEST[ "dir" ] == 'asc')
				{
					$orderby .= ' ASC';$conf_link .= '&dir=desc">Date de confirmation<span class="sort_asc"></span></a>';
				}
				else
				{
					$orderby .= ' DESC';$conf_link .= '&dir=asc">Date de confirmation<span class="sort_desc"></span></a>';
				}
			}
			elseif($_REQUEST[ "sort" ] == "term"){
				$join = ' ';
				$orderby = " ORDER BY pt.term_date";
				$term_link = '<a href="/catalog/product_term_buyer.php?all&sort=term';
				$conf_link = '<a href="/catalog/product_term_buyer.php?all&sort=conf&dir=asc">Date de confirmation<span class="sort_asc"></span></a>';
				if($_REQUEST[ "dir" ] == 'asc'){$orderby .= ' ASC';$term_link .= '&dir=desc">Date &eacute;ch&eacute;ance<span class="sort_asc"></span></a>';}
				else{$orderby .= ' DESC';$term_link .= '&dir=asc">Date &eacute;ch&eacute;ance<span class="sort_desc"></span></a>';}
			}
			else
			{
				$join = ' ';
				$orderby = ' ORDER BY pt.term_date DESC';
				$conf_link = '<a href="/catalog/product_term_buyer.php?all&sort=conf&dir=asc">Date de confirmation<span class="sort_desc"></span></a>';
				$term_link = '<a href="/catalog/product_term_buyer.php?all&sort=term&dir=asc">Date &eacute;ch&eacute;ance<span class="sort_desc"></span></a>';
			}
		}
		else
		{
			$join = ' ';
			$orderby = ' ORDER BY pt.term_date DESC';
			$conf_link = '<a href="/catalog/product_term_buyer.php?all&sort=conf&dir=asc">Date de confirmation<span class="sort_desc"></span></a>';
			$term_link = '<a href="/catalog/product_term_buyer.php?all&sort=term&dir=asc">Date &eacute;ch&eacute;ance<span class="sort_desc"></span></a>';
		}
		$admin_order->setData('conf_link', $conf_link);
		$admin_order->setData('term_link', $term_link);
		
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$query = "
		SELECT pt.* FROM `product_term` pt
		$join
		WHERE pt.idbuyer = '$idbuyer'
		$orderby";
		
		$rs =& DBUtil::query( $query );
		
	}
	else include($GLOBAL_START_PATH.'/catalog/admin_search.inc.php');
	
	/*if( $rs->RecordCount() ){
		$admin_order->setData('NoResults',0);
	}*/
	
	$array_order = array();
	$isResults = true;
	$mois = array("","janvier","fevrier","mars","avril","mai","juin","juillet","aout","septembre","octobre","novembre","decembre");
	
	for($i=0;$i<$rs->RecordCount();$i++){
	$term_date			= $rs->fields( "term_date" );
	$service_extern	= $rs->Fields( "service_extern" );
	$idproduct_term			= $rs->Fields( "idproduct_term" );
	
	//date de condirmation de la commande
	$idord = $rs->fields( "idorder" );
	
	$qr = "
		SELECT * FROM `order`
		WHERE idorder = '$idord'";
		
	$rsq =& DBUtil::query( $qr );
	$conf_date_tmp =  $rsq->fields( "conf_order_date" );
	$conf_date_tmp1 = explode(" ", $conf_date_tmp );
	$conf_date = date("m/d/Y", strtotime($conf_date_tmp1[0]));
	//Fin date de confirmation de la commande
					
	$dates =  date("Y-m-d");
	$Nombres_jours =  NbJours($dates,$term_date);
	$gris = false;
	$jaune = false;
	$vert = false;
	$rouge = false;

   if ($service_extern == 1 ) {$gris = true;
   }else{  if (($Nombres_jours < 30) && ($Nombres_jours > 2) ){ $jaune = true;
    } 
														 if ($Nombres_jours == 1 ){ $vert = true;
                                                       }
														   if ($Nombres_jours < 0 ){  $rouge = true;
														
														   }
														   
														   } 
	/*	include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
		
		$order = new Order( $rs->fields( "idorder" ) );
		$invoices = Order::getInvoices($order->get('idorder') ); 
		$bls = Order::getBLs( $order->get('idorder') );
		
		list($datedev,$heure)=explode(" ", $order->get('DateHeure') );
		list($y,$m,$d)=explode("-",$datedev);
		$dateorder = $d." ".$mois[intval($m)]." ".$y;
				
		$Str_Status = $order->get('status');
		if($Str_Status=="ToDo")
			$Str_Status ="InProgress";
			
		if( in_array( $order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD, Payment::$PAYMENT_MOD_FIANET_CB ) )
			&& $order->getBalanceOutstanding() > 0.0 
			&& $order->get( "status" ) != Order::$STATUS_CANCELLED
			&& !$order->get( "paid" ) )
				$allow_debit_card_payment = true;
		else 	$allow_debit_card_payment = false;
		*/
		
		//$packages = Order::getOrderPackages( $order->get('idorder') );
			$array_order[$i]["idproduct_term"] = $idproduct_term;
		$array_order[$i]["gris"] = $gris;
		$array_order[$i]["jaune"] = $jaune;
		$array_order[$i]["vert"] = $vert;
		$array_order[$i]["rouge"] = $rouge;
		$array_order[$i]["i"] = $i;
		$array_order[$i]["idorder"] = $rs->fields( "idorder" );
		$array_order[$i]["reference"] = $rs->fields( "reference" );
		$array_order[$i]["service_extern"] = $rs->fields( "service_extern" );
		$array_order[$i]["designation"] = $rs->fields( "designation" );
		$array_order[$i]["conf_date"] = $conf_date;
		$array_order[$i]["service_name"] = $rs->fields( "service_name" );
		$array_order[$i]["serial_number"] =$rs->fields( "serial_number" );
		$array_order[$i]["doc_certificat"] =$rs->fields( "doc_certificat" );
		//$array_order[$i]["term_date"] =$term_date;
		$array_order[$i]["term_date"] = date("d/m/Y", strtotime($term_date));
		//$array_order[$i]["term_date"] = date("Y-m-d", strtotime($availability_date));
		
		$isResults = false;
	
		//summary
		$srq = DBUtil::query( "SELECT `summary` FROM `order_row` WHERE `idorder` = ".$rs->fields( 'idorder' )." AND `reference` = '".$rs->fields( "reference" )."'" );
		$array_order[$i]["summary"] = Util::doNothing(html_entity_decode(html_entity_decode( $srq->fields( "summary" ) )));
		
		$rs->MoveNext();
	}
	
	$admin_order->setData('NoResults',$isResults);
	$admin_order->setData('array_order', $array_order);
	$admin_order->setData('datepickerjs', createDatepickerJS($array_order));
}	

$admin_order->output();




function ModifyDate( ){
	global $GLOBAL_START_PATH;
	
	
$idorders = $_GET[ "idos" ];
	$availability_date = $_GET[ "av" ];
		
	//$dispatch_date = $_GET[ "disp" ];
	
	//$Order = new SupplierOrder( $idorders );
	
	$regs = array();
	$pattern = "/^([0-9]{2}).{1}([0-9]{2}).{1}([0-9]{4})\$/";
	
	if( preg_match( $pattern, $availability_date, $regs ) !== false ){
		
		$day = $regs[ 1 ];
		$month = $regs[ 2 ];
		$year = $regs[ 3 ];
		
		$availability_date = $year."-".$month."-".$day;
		
	}elseif( !empty( $availability_date ) ){
		
		echo "wrong av date format";
		return;
		
	}
	
	
	

			
	$qSuivi = "
		UPDATE product_term 
		SET term_date = '$availability_date' 
		
		WHERE idproduct_term = '$idorders'
		LIMIT 1"
	;
	
		$rsSuivi =& DBUtil::query( $qSuivi );
	
	
		if($rsSuivi === false){
			trigger_error( "Impossible de mettre à jour la date!", E_USER_ERROR );
			echo "wrong sql";
		return;
		}
	
	
 	echo "Confirmée";
	
}
 	if( isset( $_REQUEST[ "update" ] ) && $_REQUEST[ "update" ] == "documents" ){
	$name_file = $_POST[ "documents" ];
		include_once( "../config/init.php" );
	
	$query = "
	UPDATE product_term 
	SET doc_certificat = '" . $name_file . "' 
	WHERE idproduct_term = '" . intval( $_POST[ "idproduct_term" ] ) . "' 
	LIMIT 1";

	$ret = DBUtil::query( $query );
	
	if( is_uploaded_file( $_FILES[ "documents" ][ "tmp_name" ] ) ){

		$YearFile = date("Y");
		$DateFile = date("d-m");
			$idorder =  $_POST[ "idorder" ] ;
			$idproduct_term =  $_POST[ "idproduct_term" ] ;
		// on a qu'un fournisseur donc un seul répertoire le doc sera le meme pour toutes les commandes
		// pas besoin d'uploader 15 fois
	
		$fieldname = "documents";
	
		$target_dir = "$GLOBAL_START_PATH/data/certificat/$idproduct_term/";
		//$prefixe = "Fact";
		
		$name_file = $_FILES[ $fieldname ][ 'name' ];
		$tmp_file = $_FILES[ $fieldname ][ 'tmp_name' ];
		
		$extension = substr( $name_file , strrpos( $name_file , "." ) );
		
		if(($extension!='.pdf')&&($extension!='.PDF')){
   			die( "<span style=\"color:#FF0000;size:12px;font-family: Arial, Helvetica, sans-serif;\">".Dictionnary::translate("gest_com_format_doc")."</span><br /><br /><a href=\"./product_term_list.php\" style=\"color:#000000;size:12px;font-family: Arial, Helvetica, sans-serif;text-decoration:none;font-weight:bold;\">Retour</a>" );
			return false;
		}
		
		// on copie le fichier dans le dossier de destination
    	$dest = $target_dir.$idorder."_".$idproduct_term;
		$newdoc = $idproduct_term."/".$idorder."_".$idproduct_term;
	
		//rep année	
		
		if( !file_exists( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) )
			if( mkdir( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term ) === false )
				die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_create_repertory")."</span>" );
			
		//if( chmod( $GLOBAL_START_PATH.'/data/certificat/'.$idproduct_term, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $target_dir </span>" );	
			
		
    	if( !move_uploaded_file($tmp_file, $dest) )
    	{
        	exit("<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_copy_link")." $target_dir </span>");
    	}
	
		//if( chmod( $dest, 0755 ) === false )
			//die( "<span class=\"msg\">".Dictionnary::translate("gest_com_impossible_modify_right")." $dest </span>" );	
				
	
	//exit( $ret === false ? "" : "1" );	
	}
		
}

function NbJours($debut, $fin) {

  $tDeb = explode("-", $debut);
  $tFin = explode("-", $fin);

  $diff = mktime(0, 0, 0, $tFin[1], $tFin[2], $tFin[0]) - 
          mktime(0, 0, 0, $tDeb[1], $tDeb[2], $tDeb[0]);
  
  return(($diff / 86400)+1);

}

function createDatepickerJS($array_order)
{
	$js = "<script>
		$(function() {
		  var dateParams = $.extend(
			{},
			{	
				dateFormat: 'dd/mm/yy',
				showAnim: 'fadeIn',
				changeMonth: true,
				changeYear: true
			}
		);
	";
	foreach ($array_order as $k => $value)
	{
		$js .= '$( "#datepicker'.$k.'" ).datepicker(dateParams);';
	}
	$js .= "});</script>";
	return $js;
}
?>
