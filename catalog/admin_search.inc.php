<?php 
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement de la recherche de devis ou de commande
 */
 

$now = getdate();
$IdBuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;

if ($FormName == 'adm_estim') {
	$Now_Time = date('Y-m-d') ;
	$SQL_Condition = " WHERE `es`.`idbuyer` = '$IdBuyer'";
	$SQL_Condition .= " AND (`es`.`valid_until`='0000-00-00' or `es`.`valid_until` >= '$Now_Time' AND es.`status` NOT IN( 'ToDo', 'InProgress' )";
	$TableNameParent = "estimate";
	$TableNameChild  = "estimate_row";
	
	$interval_select = $_POST['interval_select'];
	switch ($interval_select)
	{
		case 'since':
			$minus_date_select = $_POST['minus_date_select'];
			//min : today - back in time interval {-2; -7; -14; -30; -365} days
			$Min_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']+$minus_date_select,$now['year'])) ;
			//max : today
			$Max_Time = date('Y-m-d 23:59:59', mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday'],$now['year'])) ;
			break;
		case 'month':
			$yearly_month_select = $_POST['yearly_month_select'];
			if ($yearly_month_select > $now['mon'])
			{
				$now['year'] --;
			}
			//min : month selected, the 1st day
			$Min_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$yearly_month_select,1,$now['year'])) ;
			//max : month selected + 1 month, the 1st day
			$Max_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$yearly_month_select+1,1,$now['year'])) ;
			break;
		case 'between':
			$sp_bt_start_day   = $_POST['sp_bt_start_day'];
			$sp_bt_start_month = $_POST['sp_bt_start_month'];
			$sp_bt_start_year  = $_POST['sp_bt_start_year'];
			$sp_bt_stop_day    = $_POST['sp_bt_stop_day'];
			$sp_bt_stop_month  = $_POST['sp_bt_stop_month'];
			$sp_bt_stop_year   = $_POST['sp_bt_stop_year'];
			//min : day entered by user
			$Min_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$sp_bt_start_month,$sp_bt_start_day,$sp_bt_start_year)) ;
			//max : day entered by user
			$Max_Time = date('Y-m-d 23:59:59', mktime($now['hours'],$now['minutes'],$now['seconds'],$sp_bt_stop_month,$sp_bt_stop_day,$sp_bt_stop_year)) ;
			break;
	}
	if ($Min_Time <= $Max_Time) {
	}
	
	if( isset($_POST['sp_reference']) ) $sp_reference = $_POST['sp_reference'];
	if( isset($_POST['sp_designation']) ) $sp_designation = $_POST['sp_designation'];
	if( isset($_POST['sp_idbuyer']) ) $sp_idbuyer = $_POST['sp_idbuyer'];
	if( isset($_POST['sp_idorder']) ) $sp_idorder = $_POST['sp_idorder'];
	if( isset($_POST['sp_lastname']) ) $sp_lastname = $_POST['sp_lastname'];
	if( isset($_POST['sp_idcommercant']) ) $sp_idcommercant = $_POST['sp_idcommercant'];
	
	function FProcessString($str)
	{
		return "'". str_replace(",","','",$str) ."'";
	}
	
	
	if( isset($_POST['k_1']) &&  !empty($sp_reference) )
	{
		// Ajout de '' autour des ,
		$sp_reference = FProcessString($sp_reference);
		$SQL_Condition .= " AND `esr`.`reference` IN( $sp_reference )";
	}
	
	if ( isset($_POST['k_2']) && !empty($sp_designation) )
	{
		$sp_designation = FProcessString($sp_designation);
		$SQL_Condition .= " AND `esr`.`designation` IN( $sp_designation )";
	}
	
	if ( isset($_POST['k_3']) && !empty($sp_idbuyer) )
	{
		$sp_idbuyer = FProcessString($sp_idbuyer);
		$SQL_Condition .= " AND `es`.`idbuyer` IN( $sp_idbuyer )";
	}
	
	if ( isset($_POST['k_4']) && !empty($sp_idorder) )
	{
		$sp_idorder = FProcessString($sp_idorder);
		$SQL_Condition .= " AND `es`.`$FieldId` IN( $sp_idorder )";
	}
	
	if ( isset($_POST['k_6']) && !empty($sp_idcommercant) )
	{
		$sp_idcommercant = FProcessString($sp_idcommercant);
		$SQL_Condition .= " AND `es`.`idcommercant` IN( $sp_idcommercant )";
	}
	
	//$SQL_Condition .= " AND `es`.`status`<>'" . Order::$STATUS_TODO . "' AND `es`.`status`<>'" . Order::$STATUS_IN_PROGRESS . "'";
	
	$FieldId = "idestimate";
	
	$TableName = " `$TableNameParent` `es`, `$TableNameChild` `esr` ";
	$FieldList = "`es`.`$FieldId`," .
				" `es`.`idbuyer`," .
				" `es`.`DateHeure`," .
				" `es`.`iddelivery`," .
				" `es`.`total_amount_ht`," .
				" `es`.`status`,";
	
	$FieldList.=" `es`.`idpayment`";
	if ($FormName == 'adm_estim') {
		$FieldList .= ", `es`.`valid_until`";
	}
	$SQL_Condition	.= " AND `es`.`$FieldId` = `esr`.`$FieldId` GROUP BY $FieldList";
	$SQL_Query = "SELECT $FieldList FROM $TableName $SQL_Condition";
	
	//die($SQL_Query);
}else{
	$SQL_Condition = " WHERE `op`.`idbuyer` = '$IdBuyer'";
	$TableNameParent = "order";
	$TableNameChild  = "order_row";

	$interval_select = $_POST['interval_select'];
	switch ($interval_select)
	{
		case 'since':
			$minus_date_select = $_POST['minus_date_select'];
			//min : today - back in time interval {-2; -7; -14; -30; -365} days
			$Min_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday']+$minus_date_select,$now['year'])) ;
			//max : today
			$Max_Time = date('Y-m-d 23:59:59', mktime($now['hours'],$now['minutes'],$now['seconds'],$now['mon'],$now['mday'],$now['year'])) ;
			break;
		case 'month':
			$yearly_month_select = $_POST['yearly_month_select'];
			if ($yearly_month_select > $now['mon'])
			{
				$now['year'] --;
			}
			//min : month selected, the 1st day
			$Min_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$yearly_month_select,1,$now['year'])) ;
			//max : month selected + 1 month, the 1st day
			$Max_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$yearly_month_select+1,1,$now['year'])) ;
			break;
		case 'between':
			$sp_bt_start_day   = $_POST['sp_bt_start_day'];
			$sp_bt_start_month = $_POST['sp_bt_start_month'];
			$sp_bt_start_year  = $_POST['sp_bt_start_year'];
			$sp_bt_stop_day    = $_POST['sp_bt_stop_day'];
			$sp_bt_stop_month  = $_POST['sp_bt_stop_month'];
			$sp_bt_stop_year   = $_POST['sp_bt_stop_year'];
			//min : day entered by user
			$Min_Time = date('Y-m-d 00:00:00', mktime($now['hours'],$now['minutes'],$now['seconds'],$sp_bt_start_month,$sp_bt_start_day,$sp_bt_start_year)) ;
			//max : day entered by user
			$Max_Time = date('Y-m-d 23:59:59', mktime($now['hours'],$now['minutes'],$now['seconds'],$sp_bt_stop_month,$sp_bt_stop_day,$sp_bt_stop_year)) ;
			break;
	}
	if ($Min_Time <= $Max_Time) {
	}
	
	if( isset($_POST['sp_reference']) ) $sp_reference = $_POST['sp_reference'];
	if( isset($_POST['sp_designation']) ) $sp_designation = $_POST['sp_designation'];
	if( isset($_POST['sp_idbuyer']) ) $sp_idbuyer = $_POST['sp_idbuyer'];
	if( isset($_POST['sp_idorder']) ) $sp_idorder = $_POST['sp_idorder'];
	if( isset($_POST['sp_lastname']) ) $sp_lastname = $_POST['sp_lastname'];
	if( isset($_POST['sp_idcommercant']) ) $sp_idcommercant = $_POST['sp_idcommercant'];
	
	function FProcessString($str)
	{
		return "'". str_replace(",","','",$str) ."'";
	}
	
	
	if( isset($_POST['k_1']) &&  !empty($sp_reference) )
	{
		// Ajout de '' autour des ,
		$sp_reference = FProcessString($sp_reference);
		$SQL_Condition .= " AND `oc`.`reference` IN( $sp_reference )";
	}
	
	if ( isset($_POST['k_2']) && !empty($sp_designation) )
	{
		$sp_designation = FProcessString($sp_designation);
		$SQL_Condition .= " AND `oc`.`designation` IN( $sp_designation )";
	}
	
	if ( isset($_POST['k_3']) && !empty($sp_idbuyer) )
	{
		$sp_idbuyer = FProcessString($sp_idbuyer);
		$SQL_Condition .= " AND `op`.`idbuyer` IN( $sp_idbuyer )";
	}
	
	if ( isset($_POST['k_4']) && !empty($sp_idorder) )
	{
		$sp_idorder = FProcessString($sp_idorder);
		$SQL_Condition .= " AND `op`.`$FieldId` IN( $sp_idorder )";
	}
	
	if ( isset($_POST['k_6']) && !empty($sp_idcommercant) )
	{
		$sp_idcommercant = FProcessString($sp_idcommercant);
		$SQL_Condition .= " AND `op`.`idcommercant` IN( $sp_idcommercant )";
	}
	
	$SQL_Condition .= " AND `op`.`status`<>'" . Order::$STATUS_TODO . "' AND `op`.`status`<>'" . Order::$STATUS_IN_PROGRESS . "'";
	
	$TableName = " `$TableNameParent` `op`, `$TableNameChild` `oc` ";
	$FieldList = "`op`.`$FieldId`," .
				" `op`.`idbuyer`," .
				" `op`.`DateHeure`," .
				" `op`.`iddelivery`," .
				" `op`.`total_amount_ht`," .
				" `op`.`status`,";
	
	$FieldList.=" `op`.`idpayment`";
	if ($FormName == 'adm_estim') {
		$FieldList .= ", `op`.`valid_until`";
	}
	$SQL_Condition	.= " AND `op`.`$FieldId` = `oc`.`$FieldId` GROUP BY $FieldList";
	$SQL_Query = "SELECT $FieldList FROM $TableName $SQL_Condition";
}
error_reporting(0);

$DB = &DBUtil::getConnection();
$rs = $DB->Execute($SQL_Query);


if( empty($rs) ) {
	trigger_error(showDebug($SQL_Query,'admin_search.inc.php => $SQL_Query','log'),E_USER_ERROR);
}
?>