<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de l'odf commande fournisseur
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFSupplierOrder.php" );


$odf = new ODFSupplierOrder(  $_REQUEST[ "idorder" ]);

$odf->exportODT( "Commande fournisseur n° " .  $_REQUEST[ "idorder" ]  . ".odt" );

?>