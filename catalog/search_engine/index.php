<?php

include_once( dirname( __FILE__ ) . "/../../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../../objects/catalog/CCategoryProxy.php" );

/* ------------------------------------------------------------------------------------------ */

function createSearchEngine( PageController &$controller, CCategoryProxy &$category ){

	$idctest = $category->getId();

	$children = $category->getChildren();
	
	$lang = "_1";
	$tablesscats = array();
	
	foreach( $children as $child )
		$tablesscats[ $child->getId() ] = $child->get( "name$lang" );

	// 2ème trissée - la liste de tous les intitulés L et l
	// pourquoi? bé pour avoir le choix de choisir soit d'abord par la longueur
	// soit par la largeur, kom une bitch koi...
	$idctest2 = $idctest;
	if (isset($_COOKIE["searchParams"]["IdCateg"]) && $_COOKIE["searchParams"]["IdCateg"]!="" && $_COOKIE["searchParams"]["IdCateg"]!= $category->getId() )
		$idctest2 = $_COOKIE["searchParams"]["IdCateg"];
	
	$recupLong = array();
	$recupLong = GetIntitulesSEFromCategory($idctest2,'Long. (mm)');
	
	$recupLarg = array();
	$recupLarg = GetIntitulesSEFromCategory($idctest2,'Larg. (mm)');
	
	$recupHaut = array();
	$recupHaut = GetIntitulesSEFromCategory($idctest2,'Haut. (mm)');
	
	
	// Bon ça suffit maintenant, on créé les objets flexy n the mix
	$element =& $controller->createElement( "IdCateg" ) ;
	$element->setOptions(array("$idctest" => "-- indifférent --"));
	$element->setOptions($tablesscats);
	$element->attributes["onChange"] = "changeChildDatas('IdCateg','Long','','Long. (mm)');changeChildDatas('IdCateg','Larg','','Larg. (mm)');changeChildDatas('IdCateg','Haut','','Haut. (mm)');";
	if (isset($_COOKIE["searchParams"]) && $_COOKIE["searchParams"]["IdCateg"] != "")
		$element->setValue($_COOKIE["searchParams"]["IdCateg"]);
		
	$element =& $controller->createElement( "Long" ) ;
	$element->setOptions(array("-1" => "-- indifférent --"));
	$element->setOptions( $recupLong );
	$element->attributes["onChange"] = "changeChildDatas('Long','Larg','Long. (mm)','Larg. (mm)');changeChildDatas('Long','Haut','Long. (mm)','Haut. (mm)');";
	if (isset($_COOKIE["searchParams"]) && $_COOKIE["searchParams"]["Long"]!="" )
		$element->setValue($_COOKIE["searchParams"]["Long"]);
	
	$controller->createElement( "Larg" ) ;
	$element->setOptions(array("-1" => "-- indifférent --"));
	$element->setOptions( $recupLarg );
	$element->attributes["onChange"] = "changeChildDatas('Larg','Haut','Larg. (mm)','Haut. (mm)');";
	if (isset($_COOKIE["searchParams"]) && $_COOKIE["searchParams"]["Larg"]!="")
		$element->setValue($_COOKIE["searchParams"]["Larg"]);
	
	$element =& $controller->createElement( "Haut" ) ;
	$element->setOptions(array("-1" => "-- indifférent --"));
	$element->setOptions( $recupHaut );
	$element->attributes["onChange"] = "";
	if (isset($_COOKIE["searchParams"]) && $_COOKIE["searchParams"]["Haut"]!="")
		$element->setValue($_COOKIE["searchParams"]["Haut"]);
	
	$element =& $controller->createElement( "Alim" ) ;
	$element->setOptions(array("-1" => "-- indifférent --"));
	$element->setOptions(array("Oui" => "Oui"));
	$element->setOptions(array("Non" => "Non"));
	$element->attributes["onChange"] = "";
	if (isset($_COOKIE["searchParams"]) && $_COOKIE["searchParams"]["Alim"]!="")
		$element->setValue($_COOKIE["searchParams"]["Alim"]);

}

/* ------------------------------------------------------------------------------------------ */

function GetIntitulesSEFromCategory($idcategory,$intitule){
	
	$lang = "_1";
	
	$rqTaille="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE '$intitule' LIMIT 1";
	$rsTaile=DBUtil::query($rqTaille);
	
	$idTitleTaille=$rsTaile->fields("idintitule_title");
	$arrayCountByTaille=array();
	if($rsTaile->RecordCount()){
		$rqToutesLesTailles="SELECT iv.intitule_value_1, COUNT( p.idproduct ) as count
							 FROM product p, intitule_link il, intitule_value iv, detail d
							 WHERE p.idproduct = il.idproduct
							 AND il.idintitule_value=iv.idintitule_value
							 AND d.idarticle = il.idarticle
							 AND d.available = 1
							 AND d.stock_level <> 0
							 AND il.idintitule_title=$idTitleTaille
							 AND p.idcategory LIKE '$idcategory%' 
							 GROUP BY il.idintitule_value
							 ORDER BY CAST(iv.intitule_value_1 AS UNSIGNED) ASC";
	
		$rsToutesLesTailles=DBUtil::query($rqToutesLesTailles);
		
		$i=0;
		while(!$rsToutesLesTailles->EOF()){
			$arrayCountByTaille[$rsToutesLesTailles->fields("intitule_value_1")]=$rsToutesLesTailles->fields("intitule_value_1");
		
			$i++;
			$rsToutesLesTailles->MoveNext();
		}
	}
	
	return $arrayCountByTaille;
}

/* ------------------------------------------------------------------------------------------ */

?>