<?php
//important pour les popups
header('Content-Type: text/html; charset=UTF-8'); 

include_once( "../../config/init.php" );

$lang = "_1";

// test des POST emis
if($_POST["source"]=="IdCateg"){
	$intutulations = GetIntitulesFromCategory($_POST['category'],$_POST['intituleSource'],$_POST['intituleCible']);
	$countMoi = count($intutulations);

	echo 'var o = null; ';   
	echo 'var s = document.forms["'.$_POST["form"].'"].elements["'.$_POST["select"].'"]; ';   
	echo 's.options.length = 1; ';

	if($countMoi>0)  {
		foreach($intutulations as $intitulmoi){  
			echo 's.options[s.options.length] = new Option("'.$intitulmoi.'");';   
		}
	}

}else{
	$intitulesource = $_POST['intituleSource'];
	$intitulecible  = $_POST['intituleCible'];
	
	$sourceValue = $_POST['sourceValue'];
		
	$idcat = $_POST['category'];
	
	$intutulations = GetIntitulesFromCategory($idcat,$intitulesource,$intitulecible,$sourceValue);
	$countMoi = count($intutulations);
	
	echo 'var o = null; ';   
	echo 'var s = document.forms["'.$_POST["form"].'"].elements["'.$_POST["select"].'"]; ';   
	echo 's.options.length = 1; '; 

	if($countMoi>0)  {
		foreach($intutulations as $intitulmoi){  
			echo 's.options[s.options.length] = new Option("'.$intitulmoi.'");';   
		}
	}
	
	
}
	exit;

function GetIntitulesFromCategory($idcategory,$intituleSource,$intituleCible,$sourceValue=-1){

	$lang = "_1";	
	
	$rqTaille="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE '$intituleCible' LIMIT 1";
	$rsTaile=DBUtil::query($rqTaille);
	
	$idTitleTaille=$rsTaile->fields("idintitule_title");
	$arrayCountByTaille=array();
	
	$inLine = createInCategory($idcategory, array());
	$inLineTextual = implode("','" , $inLine);
	
	if($rsTaile->RecordCount()){
		$rqToutesLesTailles="SELECT iv.intitule_value_1
							 FROM product p, intitule_link il, intitule_value iv, detail d
							 WHERE p.idproduct = il.idproduct
							 AND il.idintitule_value=iv.idintitule_value
							 AND d.idarticle = il.idarticle
							 AND d.available = 1
							 AND d.stock_level <> 0
							 AND il.idintitule_title=$idTitleTaille
							 AND p.idcategory IN ('$inLineTextual')
							 GROUP BY il.idintitule_value
							 ORDER BY iv.intitule_value_1 ASC";
	
		$rsToutesLesTailles=DBUtil::query($rqToutesLesTailles);
		
		$i=0;
		while(!$rsToutesLesTailles->EOF()){
			$arrayCountByTaille[$rsToutesLesTailles->fields("intitule_value_1")]=$rsToutesLesTailles->fields("intitule_value_1");
		
			$i++;
			$rsToutesLesTailles->MoveNext();
		}
		
		if($intituleSource != ""){
			$arrayIntituleCible = array();
			
			$andSV="";
			if(intval($sourceValue)){
				$andSV = " AND iv.intitule_value_1 = $sourceValue ";
			}
			
			$rqidintituleSource="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE '$intituleSource' LIMIT 1";
			$rsidintituleSource=DBUtil::query($rqidintituleSource);
			$idintituleSource = $rsidintituleSource->fields("idintitule_title");
			
			$rqidprods="SELECT DISTINCT il.idproduct
					 	FROM product p, intitule_link il, intitule_value iv, detail d
					 	WHERE p.idproduct = il.idproduct
					 	AND il.idintitule_value=iv.idintitule_value
					 	AND d.idarticle = il.idarticle
					 	AND d.available = 1
					 	AND d.stock_level <> 0
					 	AND il.idintitule_title=$idintituleSource
						$andSV
					 	AND p.idcategory IN ('$inLineTextual')";

			$rsidprods=DBUtil::query($rqidprods);
				
			if($rsidprods->RecordCount()>0){
				$arrayaexploder = array();
				while(!$rsidprods->EOF()){
					array_push($arrayaexploder,$rsidprods->fields("idproduct"));
					
					$rsidprods->MoveNext();
				}
				$products = implode(",",$arrayaexploder);
				
				$rqidintituleCible="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE '$intituleCible' LIMIT 1";
				$rsidintituleCible=DBUtil::query($rqidintituleCible);
				
				$idintituleCible=$rsidintituleCible->fields("idintitule_title");
				
				$rqToutes="SELECT iv.intitule_value_1
						   FROM product p, intitule_link il, intitule_value iv, detail d
						 WHERE p.idproduct = il.idproduct
						 AND il.idintitule_value=iv.idintitule_value
						 AND d.idarticle = il.idarticle
						 AND d.available = 1
						 AND d.stock_level <> 0
						 AND il.idintitule_title=$idintituleCible
						 AND p.idcategory IN ('$inLineTextual')
						 AND il.idproduct IN ($products)
						 GROUP BY il.idintitule_value
						 ORDER BY iv.intitule_value_1 ASC";
			
				$rsToutes=DBUtil::query($rqToutes);
				
				while(!$rsToutes->EOF()){
					$arrayIntituleCible[$rsToutes->fields("intitule_value_1")]=$rsToutes->fields("intitule_value_1");
				
					$rsToutes->MoveNext();
				}
				
				return $arrayIntituleCible;
			}else{
				return $arrayIntituleCible;
			}
			
		}
	}
	
	
	return $arrayCountByTaille;
}

function createInCategory($idCatParent, $arrayIns, $passage = 0){
	// alors pour créer le IN il faut récuperer toutes les filles de la
	// catégorie en cours, pour cela une tit fonction récursive ça va chier
	// et le tour est joué. Attention Catia, démonstration !!!
	
	// 1er la cat en cours ça évitera les erreur de tableau vide...
	array_push($arrayIns, $idCatParent);
	
	// ensuite ben c'est partu mon kiki on cherche les filles hummm les filles
	$rsSScat1 = DBUtil::query("SELECT idcategorychild FROM category_link WHERE idcategoryparent=$idCatParent");
	if($rsSScat1 !== false && $rsSScat1->RecordCount() > 0){
		
		while(!$rsSScat1->EOF()){

			$idCatParent2 = $rsSScat1->fields("idcategorychild");
			
			$seconaryArray = createInCategory($idCatParent2, $arrayIns, 1);
			
			for($i=0 ; $i<count($seconaryArray) ; $i++){
				if(!in_array($seconaryArray[$i],$arrayIns)){
					array_push($arrayIns, $seconaryArray[$i]);
				}
			}
			
			$rsSScat1->MoveNext();
		}
	}
	
	return $arrayIns;
}
?>
