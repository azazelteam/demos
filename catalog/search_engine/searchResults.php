<?
//important pour les popups
header('Content-Type: text/html; charset=UTF-8'); 

include_once('../../config/init.php');
include_once('../../objects/categoryobject.php');
include_once('../../objects/productobject.php');
include_once('../../objects/articleobject.php');
include_once('../../script/global.fct.php');

$lang = "_1";

$idcat 	= $_POST["IdCateg"];
$long 	= $_POST["Long"];
$larg 	= $_POST["Larg"];
$haut	= $_POST["Haut"];
$alim	= $_POST["Alim"];

$sname	= $_POST["scriptName"];

//Application d'une tolérence de 10% sur long larg haut
$txtol = 10;

if($idcat > 0 && $idcat!=""){
	
	$inLine = createInCategory($idcat, array());
	$inLineTextual = implode("','" , $inLine);
	$bonusLine ="";
	
	if($haut > 0){
		$betweenHautMax = $haut + ( $haut * ( $txtol / 100 ) );
		$betweenHautMin = $haut - ( $haut * ( $txtol / 100 ) );
		
		$rqTaille="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Haut. (mm)' LIMIT 1";
		$rsTaile=DBUtil::query($rqTaille);
		$idTitleTaille=$rsTaile->fields("idintitule_title");
		
		$bonusLine .= "	AND p.idproduct IN (SELECT p.idproduct 
						FROM product p, intitule_link il, intitule_value iv, detail d
						WHERE p.idproduct = il.idproduct 
						AND il.idintitule_value=iv.idintitule_value 
						AND d.idarticle = il.idarticle 
						AND d.available = 1 
						AND d.stock_level <> 0 
						AND iv.intitule_value_1 BETWEEN $betweenHautMin AND $betweenHautMax
						AND il.idintitule_title=$idTitleTaille 
						AND p.idcategory IN ('$inLineTextual')
						AND p.available = 1
						GROUP BY p.idproduct)";
	}
	
	if($alim != -1){
		$rqTaille="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Qualité alimentaire' LIMIT 1";
		$rsTaile=DBUtil::query($rqTaille);
		$idTitleAlim=$rsTaile->fields("idintitule_title");
		if($idTitleAlim){
			$bonusLine .= "	AND p.idproduct IN (SELECT p.idproduct 
							FROM product p, intitule_link il, intitule_value iv, detail d
							WHERE p.idproduct = il.idproduct 
							AND il.idintitule_value=iv.idintitule_value 
							AND d.idarticle = il.idarticle 
							AND d.available = 1 
							AND d.stock_level <> 0 
							AND iv.intitule_value_1 = '$alim'
							AND il.idintitule_title=$idTitleAlim 
							AND p.idcategory IN ('$inLineTextual')
							AND p.available = 1
							GROUP BY p.idproduct)";
		}
	}
	
	if($long > 0 && $larg > 0){
		
		$betweenLongMax = $long + ( $long * ( $txtol / 100 ) );
		$betweenLargMax = $larg + ( $larg * ( $txtol / 100 ) );
		
		
		$betweenLongMin = $long - ( $long * ( $txtol / 100 ) );
		$betweenLargMin = $larg - ( $larg * ( $txtol / 100 ) );

		
		$rqTaille1="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Long. (mm)' LIMIT 1";
		$rsTaile1=DBUtil::query($rqTaille1);
		$idTitleTaille1=$rsTaile1->fields("idintitule_title");
		
		$rqTaille2="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Larg. (mm)' LIMIT 1";
		$rsTaile2=DBUtil::query($rqTaille2);
		$idTitleTaille2=$rsTaile2->fields("idintitule_title");
		
		$rq="	SELECT p.idproduct
				FROM product p, intitule_link il, intitule_value iv, detail d
				WHERE p.idproduct = il.idproduct
				AND il.idintitule_value=iv.idintitule_value
				AND d.idarticle = il.idarticle
				AND d.available = 1
				AND d.stock_level <> 0
				AND iv.intitule_value_1 BETWEEN $betweenLongMin AND $betweenLongMax
				AND il.idintitule_title=$idTitleTaille1
				AND p.idcategory IN ('$inLineTextual')
				AND p.available = 1
				AND p.idproduct IN (SELECT p.idproduct 
									FROM product p, intitule_link il, intitule_value iv, detail d
									WHERE p.idproduct = il.idproduct 
									AND il.idintitule_value=iv.idintitule_value 
									AND d.idarticle = il.idarticle 
									AND d.available = 1 
									AND d.stock_level <> 0 
									AND iv.intitule_value_1 BETWEEN $betweenLargMin AND $betweenLargMax
									AND il.idintitule_title=$idTitleTaille2 
									AND p.idcategory IN ('$inLineTextual') 
									AND p.available = 1
									GROUP BY p.idproduct)
				$bonusLine
				GROUP BY p.idproduct
				ORDER BY iv.intitule_value_1 ASC";
			//die($rq);
	}else if($long > 0){
		
		$betweenLongMax = $long + ( $long * ( $txtol / 100 ) );
		$betweenLongMin = $long - ( $long * ( $txtol / 100 ) );
		
		$rqTaille1="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Long. (mm)' LIMIT 1";
		$rsTaile1=DBUtil::query($rqTaille1);
		$idTitleTaille1=$rsTaile1->fields("idintitule_title");
			
		$rq="	SELECT p.idproduct
				FROM product p, intitule_link il, intitule_value iv, detail d
				WHERE p.idproduct = il.idproduct
				AND il.idintitule_value=iv.idintitule_value
				AND d.idarticle = il.idarticle
				AND d.available = 1
				AND d.stock_level <> 0
				AND iv.intitule_value_1 BETWEEN $betweenLongMin AND $betweenLongMax
				AND il.idintitule_title=$idTitleTaille1
				AND p.idcategory IN ('$inLineTextual')
				AND p.available = 1
				$bonusLine
				GROUP BY p.idproduct
				ORDER BY iv.intitule_value_1 ASC";
	}else if($larg > 0){

		$betweenLargMax = $larg + ( $larg * ( $txtol / 100 ) );
		$betweenLargMin = $larg - ( $larg * ( $txtol / 100 ) );
	
		$rqTaille2="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Larg. (mm)' LIMIT 1";
		$rsTaile2=DBUtil::query($rqTaille2);
		$idTitleTaille2=$rsTaile2->fields("idintitule_title");
			
		$rq="	SELECT p.idproduct
				FROM product p, intitule_link il, intitule_value iv, detail d
				WHERE p.idproduct = il.idproduct
				AND il.idintitule_value=iv.idintitule_value
				AND d.idarticle = il.idarticle
				AND d.available = 1
				AND d.stock_level <> 0
				AND iv.intitule_value_1 BETWEEN $betweenLargMin AND $betweenLargMax
				AND il.idintitule_title=$idTitleTaille2
				AND p.idcategory IN ('$inLineTextual')
				AND p.available = 1
				$bonusLine
				GROUP BY p.idproduct
				ORDER BY iv.intitule_value_1 ASC";
	}else if($haut > 0){
		
		$betweenHautMax = $haut + ( $haut * ( $txtol / 100 ) );
		$betweenHautMin = $haut - ( $haut * ( $txtol / 100 ) );
		
		$rqTaille="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Haut. (mm)' LIMIT 1";
		$rsTaile=DBUtil::query($rqTaille);
		$idTitleTaille=$rsTaile->fields("idintitule_title");	
		
		$rq="	SELECT p.idproduct
				FROM product p, intitule_link il, intitule_value iv, detail d
				WHERE p.idproduct = il.idproduct
				AND il.idintitule_value=iv.idintitule_value
				AND d.idarticle = il.idarticle
				AND d.available = 1
				AND d.stock_level <> 0
				AND iv.intitule_value_1 BETWEEN $betweenHautMin AND $betweenHautMax
				AND il.idintitule_title=$idTitleTaille
				AND p.idcategory IN ('$inLineTextual')
				AND p.available = 1
				$bonusLine
				GROUP BY p.idproduct
				ORDER BY iv.intitule_value_1 ASC";
		
	}else if($alim != -1){
		$rqTaille="SELECT idintitule_title FROM intitule_title WHERE intitule_title$lang LIKE 'Qualité alimentaire' LIMIT 1";
		$rsTaile=DBUtil::query($rqTaille);
		$idTitleAlim=$rsTaile->fields("idintitule_title");
		if($idTitleAlim){
			$rq = "	SELECT p.idproduct 
					FROM product p, intitule_link il, intitule_value iv, detail d
					WHERE p.idproduct = il.idproduct 
					AND il.idintitule_value=iv.idintitule_value 
					AND d.idarticle = il.idarticle 
					AND d.available = 1 
					AND d.stock_level <> 0 
					AND iv.intitule_value_1 = '$alim'
					AND il.idintitule_title=$idTitleAlim 
					AND p.idcategory IN ('$inLineTextual')
					AND p.available = 1
					GROUP BY p.idproduct";
		}
		
	}else{
		$rq="	SELECT p.idproduct
				FROM product p, detail d
				WHERE d.idproduct = p.idproduct
				AND d.available = 1
				AND d.stock_level <> 0
				AND p.available = 1
				AND p.idcategory IN ('$inLineTextual')
				GROUP BY p.idproduct
				";
	}
	
	$rs = DBUtil::query($rq);
	$count = $rs->RecordCount();
	
	include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
	class CharteSEController extends PageController{};
	$charte_se = new CharteSEController( "/product/search_engine_results.tpl");
	
	$charte_se->setData("resultCount",$count);
	$charte_se->setData("scriptName",$sname);
	
	if($count > 0){
		
		$prods = array();

		while(!$rs->EOF()){
			array_push($prods, $rs->fields("idproduct"));
	
			$rs->MoveNext();
		}
		
		//$charte_se->GetProductListOrderedByCat(0,0,$prods);
	}	
	
	$charte_se->output();
	
}else{
	$charte_se->setData("resultCount",0);
}

exit;


function createInCategory($idCatParent, $arrayIns, $passage = 0){
	// alors pour créer le IN il faut récuperer toutes les filles de la
	// catégorie en cours, pour cela une tit fonction récursive ça va chier
	// et le tour est joué. Attention Catia, démonstration !!!
	
	// 1er la cat en cours ça évitera les erreur de tableau vide...
	array_push($arrayIns, $idCatParent);
	
	// ensuite ben c'est partu mon kiki on cherche les filles hummm les filles
	$rsSScat1 = DBUtil::query("SELECT idcategorychild FROM category_link WHERE idcategoryparent=$idCatParent");
	if($rsSScat1 !== false && $rsSScat1->RecordCount() > 0){
		
		while(!$rsSScat1->EOF()){

			$idCatParent2 = $rsSScat1->fields("idcategorychild");
			
			$seconaryArray = createInCategory($idCatParent2, $arrayIns, 1);
			
			for($i=0 ; $i<count($seconaryArray) ; $i++){
				if(!in_array($seconaryArray[$i],$arrayIns)){
					array_push($arrayIns, $seconaryArray[$i]);
				}
			}
			
			$rsSScat1->MoveNext();
		}
	}
	
	return $arrayIns;
}
?>