function changeChildDatas(elemSource,elemToChange,intituleSource,intituleCible){

	var f 	  = document.forms["search_engine"];  
	var l1    = f.elements[elemSource];   
	var l2    = f.elements[elemToChange];
	
	var index = l1.selectedIndex;   
	
	if(index < 0)   
	   l2.options.length = 0;   
	else {   
	   var xhr_object = null;   
	       
	   if(window.XMLHttpRequest) // Firefox   
	      xhr_object = new XMLHttpRequest();   
	   else if(window.ActiveXObject) // Internet Explorer   
	      xhr_object = new ActiveXObject("Microsoft.XMLHTTP");   
	   else { // XMLHttpRequest non supporté par le navigateur   
	      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");   
	      return;   
	   }   
	  
	   xhr_object.open("POST", "/catalog/search_engine/changeIntituleValue.php", true);   
	       
	   xhr_object.onreadystatechange = function() {   
	      if(xhr_object.readyState == 4){  
	         eval(xhr_object.responseText);
	         //document.getElementById("resultCount").innerHTML = "";
	      }   
	   }   
	   //document.getElementById("resultCount").innerHTML = "<img src='/catalog/search_engine/ajax-loader.gif' class='loader'>"

	   xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   	   
   	   var headingValues = f.elements["IdCateg"];
   	   var catvalue = headingValues.options[ headingValues.selectedIndex ].value;
   	   var sourcevalue = l1.options[ l1.selectedIndex ].value;
   	   
	   var data = "category="+catvalue+"&form="+f.name+"&source="+elemSource+"&sourceValue="+sourcevalue+"&select="+elemToChange+"&intituleSource="+intituleSource+"&intituleCible="+intituleCible+"";   
	  // alert(data);
	   xhr_object.send(data);   
	}

}
function searchResults(divToChange,cookingTime){

		var f = document.forms["search_engine"];
		var elemts = new Array();
		var stringDeFou = "";
		
		for (i=0 ; i< f.length ; i++){
			elemts[f[i].id] = f[i].value;
			
			cre_cook('searchParams['+f[i].id+']' , f[i].value,cookingTime);
			
			if(stringDeFou.length > 0){
				stringDeFou += "&"+f[i].id+"="+f[i].value;
			}else{
				stringDeFou = f[i].id+"="+f[i].value;
			}
		}
		
		stringDeFou += "&scriptName="+document.location;
	   var xhr_object = null;   
	       
	   if(window.XMLHttpRequest) // Firefox   
	      xhr_object = new XMLHttpRequest();   
	   else if(window.ActiveXObject) // Internet Explorer   
	      xhr_object = new ActiveXObject("Microsoft.XMLHTTP");   
	   else { // XMLHttpRequest non supporté par le navigateur   
	      alert("Votre navigateur ne supporte pas les objets XMLHTTPRequest...");   
	      return;   
	   }   
	  
	   xhr_object.open("POST", "/catalog/search_engine/searchResults.php", true);   
	       
	   xhr_object.onreadystatechange = function() {   
	      if(xhr_object.readyState == 4){
	         document.getElementById(divToChange).innerHTML = xhr_object.responseText;
	      }
	   }   
	   
	   document.getElementById(divToChange).innerHTML = "<img src='/catalog/search_engine/ajax-loader.gif' class='loader'>"
	   
	   xhr_object.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
   	     	   
	   var data = stringDeFou;   
	   xhr_object.send(data);   
}

function cre_cook0(nom,contenu) {
	document.cookie = nom + "=" + escape(contenu)
}
     
function cre_cook(nom,contenu,secondes) {
	var expireDate = new Date();
	expireDate.setTime(expireDate.getTime() + secondes*1000);
    document.cookie = nom + "=" + escape(contenu) + ";expires=" + expireDate.toGMTString();
}
     
function lit_cook(nom) {
	var deb,fin
    deb = document.cookie.indexOf(nom + "=")
    if (deb >= 0) {
    	deb += nom.length + 1
        fin = document.cookie.indexOf(";",deb)
        if (fin < 0) fin = document.cookie.length
        	return unescape(document.cookie.substring(deb,fin))
    }
    return ""
}
     
function tue_cook(nom) { 
	cre_cook(nom,"",-1);

}

function askCook(nom) {
	c=prompt("Mettre dans le cookie :","")
  	cre_cook0(nom,c)
}

function litCook(nom) {
	c=lit_cook(nom)
    if(c=="") alert("Le cookie est vide !")
    else alert("Le cookie vaut : "+c)
}

function killMyCookie(){
		
	var f = document.forms["search_engine"];
	
	for (i=0 ; i< f.length ; i++){
		tue_cook('searchParams['+f[i].id+']');
	}
	
	window.location.reload();
}