<?php 
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Extranet client pour afficher les commandes
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../objects/Order.php" );  
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" ); 
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once("$GLOBAL_START_PATH/objects/GroupingCatalog.php");
/*include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/OrderProxy.php" );*/

class AdminOrderController extends PageController{};
$admin_order = new AdminOrderController( "admin_order.tpl" );

$lang = "_1";

/**************************************************
 *********************ACCOUNT**********************
 **************************************************/
if( Session::getInstance()->getCustomer() ) {
	$admin_order->setData('isLogin', "ok");
	
	$GroupingCatalog = new GroupingCatalog();
	$admin_order->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$admin_order->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$admin_order->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$admin_order->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$admin_order->setData( "contact_lastname", $salesman->get("lastname") );
	$admin_order->setData( "contact_firstname", $salesman->get("firstname") );
	$admin_order->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$admin_order->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$admin_order->setData( "contact_email", $salesman->get("email") );
	
	$admin_order->setData( "isOrder", true );
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	/*$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$admin_order->setData( "contact_grouping", $contact_grouping );*/
} else {
	$admin_order->setData('isLogin', "");
	$com_step2 = true;
	if(empty($com_step2)) {
		$admin_order->setData('com_step_empty', "ok");
	} else {
		$admin_order->setData('com_step_empty', null);
	}
	if( isset($_POST['Login']) && !Session::getInstance()->getCustomer()) {
		$admin_order->setData('issetPostLoginAndNotLogged', "ok");
		$admin_order->setData('translateMSGErrorLoginPassword', Dictionnary::translate('MSGErrorLoginPassword'));
	} else {
		$admin_order->setData('issetPostLoginAndNotLogged', "");
	}
	$admin_order->setData('scriptName', $ScriptName);
	$admin_order->setData('translateLogin',Dictionnary::translate('login'));
	$admin_order->setData('translatePassword',Dictionnary::translate('password'));
	$ScriptName = 'estimate_basket.php';
	if($ScriptName == 'estimate_basket.php') {
		$admin_order->setData('img', "acceder_compte.png");
		$admin_order->setData('style', "margin-top:11px;margin-right:13px;border:none;");
	} else {
		$admin_order->setData('img', "poursuivre.png");
		$admin_order->setData('style', "margin-top:7px;margin-right:13px;border:none;");
	}
	
	
	
}

/**************************************************
 ****************ADMIN_ORDER.TPL*******************
 **************************************************/

$FormName = 'adm_order';


/**************************************************
 ***************ADMIN_SEARCH.TPL*******************
 **************************************************/

$Arr_LastYear = getdate(mktime(0,0,0,date('m'),date('d'),date('Y'))); // back in time one year
$Arr_Today = getdate(); // today

$admin_order->setData('ScriptName',$ScriptName); 
$admin_order->setData('FormName',$FormName);
 
// Si l'utilisateur est identifié
if( Session::getInstance()->getCustomer() ) 
{
	if( isset($_POST['search']) ) $search = isset($_POST['search']) ? $_POST['search'] : '';
	
	$TableNameParent = 'order';
	$TableNameChild = 'order_row';
	$FieldId = 'idorder';
	$admin_order->setData('IdBuyer',Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0);
	
}

//Liste déroulante depuis
$select_from ='<select name="minus_date_select" class="AuthInput">
			<option value="-2">'.Dictionnary::translate("order_2days").'
					<option value="-7">'.Dictionnary::translate("order_1week").'
					<option value="-14">'.Dictionnary::translate("order_2weeks").'
					<option value="-30">'.Dictionnary::translate("order_1month").'
					<option value="-365">'.Dictionnary::translate("order_1year").'
				</select>';

$admin_order->setData('select_from',$select_from);

// Liste déroulante affichant les 12 derniers mois par ordre décroissant
$dat = getdate();
$mon = $dat['mon'] - 1;
$year = $dat['year'] ;
$months = array (
				1 => 'Janvier',
				2 => 'Février',
				3 => 'Mars',
				4 => 'Avril',
				5 => 'Mai',
				6 => 'Juin',
				7 => 'Juillet',
				8 => 'Août',
				9 => 'Septembre',
				10 => 'Octobre',
				11 => 'Novembre',
				12 => 'Décembre'
				);
				
$select_month='<select name="yearly_month_select" class="AuthInput">';

for ( $i = $mon + 12 ; $i > $mon ;  $i--){ //parse one year
	$j = ( ( ( $i-1) % 12 + 1 )  ); // the month
	if ($j == 12) {
		$year--;
		$j = 0;
	}
	
	$select_month.='<option value="'.++$j.'">'.$months[$j].' '.$year.'</option>';
	
	$j++;
}
$select_month.='</select>';

$admin_order->setData('select_month',$select_month);

$admin_order->setData('LastYearmday',$Arr_LastYear['mday']);
$admin_order->setData('LastYearmon',$Arr_LastYear['mon']);
$admin_order->setData('LastYearyear',$Arr_LastYear['year']);

$admin_order->setData('Todaymday',$Arr_Today['mday']);
$admin_order->setData('Todaymon',$Arr_Today['mon']);
$admin_order->setData('Todayyear',$Arr_Today['year']);

if( $FormName == 'adm_estim' ) {
	$admin_order->setData('adm_estim',1);
}else{
	$admin_order->setData('adm_estim',0);
}



if( !empty( $_REQUEST["search"])||!empty( $_REQUEST["search_x"]) || isset( $_REQUEST[ "all" ] ) ){
	
	/**
	 * Traitement PHP du formulaire de recherche
	 */
	
	if( isset( $_REQUEST[ "all" ] ) ){
		
		$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
		$query = "
		SELECT idorder FROM `order`
		WHERE idbuyer = '$idbuyer'
		AND ( `status` NOT IN( 'InProgress','ToDo' ) OR idpayment = 1 )
		ORDER BY DateHeure DESC";
		
		$rs =& DBUtil::query( $query );
		
	}
	else include($GLOBAL_START_PATH.'/catalog/admin_search.inc.php');
	
	if( $rs->RecordCount() ){
		$admin_order->setData('NoResults',0);
	}
	
	$array_order = array();
	$isResults = true;
	$mois = array("","janvier","f&eacute;vrier","mars","avril","mai","juin","juillet","ao&ucirc;t","septembre","octobre","novembre","d&eacute;cembre");
	
	for($i=0;$i<$rs->RecordCount();$i++){
		
		include_once( dirname( __FILE__ ) . "/../objects/Payment.php" );
		
		$order = new Order( $rs->fields( "idorder" ) );
		$invoices = Order::getInvoices($order->get('idorder') ); 
		$bls = Order::getBLs( $order->get('idorder') );
		
		list($datedev,$heure)=explode(" ", $order->get('DateHeure') );
		list($y,$m,$d)=explode("-",$datedev);
		$dateorder = $d." ".$mois[intval($m)]." ".$y;
				
		$Str_Status = $order->get('status');
		if($Str_Status=="ToDo")
			$Str_Status ="InProgress";
			
		//if( in_array( $order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD, Payment::$PAYMENT_MOD_FIANET_CB ) )
		if( in_array( $order->get( "idpayment" ), array( Payment::$PAYMENT_MOD_DEBIT_CARD ) )		
			&& $order->getBalanceOutstanding() > 0.0 
			&& $order->get( "status" ) != Order::$STATUS_CANCELLED
			&& !$order->get( "paid" ) )
				$allow_debit_card_payment = true;
		else 	$allow_debit_card_payment = false;
		
		
		$packages = Order::getOrderPackages( $order->get('idorder') );
		$array_order[$i]["status"] = GenerateHTMLForToolTipBox($order->getId(), Util::doNothing(Dictionnary::translate($Str_Status)));
		$array_order[$i]["i"] = $i;
		$array_order[$i]["idorder"] = $order->getId();
		$array_order[$i]["orderid"] = GenerateHTMLForToolTipBox($order->getId(), $order->getId() );
		$array_order[$i]["DateHeure"] = GenerateHTMLForToolTipBox($order->getId(), $dateorder );
		$array_order[$i]["total_amount_ht"] = GenerateHTMLForToolTipBox($order->getId(), Util::priceFormat($order->get('total_amount_ht')));
		$array_order[$i]["invoices"] = $invoices;
		$array_order[$i]["bls"] = $bls;
		$array_order[$i]["allow_debit_card_payment"] = $allow_debit_card_payment;
		$array_order[$i]["total_amount_ttc_wbi"] = Util::priceFormat($order->get('total_amount'));
		
		/*order items*/
		/*$order_proxy = new OrderProxy($order);
		$article_items = array();
		for ($j=0 ; $j < $order_proxy->getItemCount() ; $j++ )
		{
			$article_items[$j]["icon"] = URLFactory::getReferenceImageURI($order_proxy->getItemAt($j)->get("idarticle" ));
			$article_items[$j]["reference"] = $order_proxy->getItemAt( $j)->get( "reference" );
			$article_items[$j]["designation"] = html_entity_decode(html_entity_decode($order_proxy->getItemAt( $j)->get( "designation" )));
			$article_items[$j]["delivery"] = $order_proxy->getItemAt( $i)->getDeliveryTime();
			$article_items[$j]["priceATI"] = $order_proxy->getItemAt($j)->getPriceATI();
			$article_items[$j]["remise"] = $order_proxy->getItemAt($j)->getDiscountRate() ? $order_proxy->getItemAt($j)->getDiscountRate() : ' - ';
			$article_items[$j]["priceATIRemise"] = $order_proxy->getItemAt($j)->getDiscountPriceATI();
			$article_items[$j]["quantity"] = $order_proxy->getItemAt($j)->getQuantity();
			$article_items[$j]["totalATI"] = $order_proxy->getItemAt($j)->getTotalATI();
		}
		$array_order[$i]["article_items"] = $article_items;*/
		
		if($packages==0){ $array_order[$i]["packages"]=''; } else {
			foreach($packages as $package){
				if($package==0){ $array_order[$i]["packages"]=''; } else { $array_order[$i]["packages"]=$packages; }
			}
		}
		
		$isResults = false;
	
		
		$rs->MoveNext();
	}
	
	$admin_order->setData('NoResults',$isResults);
	$admin_order->setData('array_order', $array_order);
	//groupement
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	$contact_grouping = $rs->fields("contact_grouping");
	$admin_order->setData( "contact_grouping", $contact_grouping );
}	

$admin_order->output();

function GenerateHTMLForToolTipBox($idorder, $value=0){
	
	global $GLOBAL_START_URL;
	
	$query = " SELECT idarticle,
				reference,
				summary,
				delivdelay,
				quantity,
				unit_price,
				ref_discount,
				discount_price
				FROM  `order_row` 
				WHERE
				idorder = '". $idorder ."'
				
	
			";	
	
	$rs = & DBUtil::query( $query );
	
		
	$html = ' <link type="text/css" rel="stylesheet" href="'.$GLOBAL_START_URL.'/css/catalog/contents.css" />';
	$html .= "<span onmouseover=\"document.getElementById('pop$idorder').className='tooltiphover';\" onmouseout=\"document.getElementById('pop$idorder').className='tooltip';\" style=\"position:relative; display:block;\">" .$value." ";
	
	
	$html .= "<span id='pop$idorder' class='tooltip'>
<div class=\"content\" style=\"padding:5px;\">
<div class=\"subContent\">";

		
		$html .= "<div style=\"font-size: 12px;\">";
		$html.= "<table>
				<tr>
					<th><span style=\"font-size: 12px;\">Icone</th>
					<th><span style=\"font-size: 12px;\">R&eacute;f&eacute;rence</th>
					<th><span style=\"font-size: 12px;\">D&eacute;signation</th>
					<th><span style=\"font-size: 12px;\">D&eacute;lai de livraison</th>
					<th><span style=\"font-size: 12px;\">Prix TTC</th>
					<th><span style=\"font-size: 12px;\">Remise</th>	
					<th><span style=\"font-size: 12px;\">Prix remis&eacute; TTC</th>
					<th><span style=\"font-size: 12px;\">Quantit&eacute;</th>
					<th><span style=\"font-size: 12px;\">Prix total TTC</th>

				</tr>";
				
				
				 while( !$rs->EOF ){
				$Icon = URLFactory::getReferenceImageURI( $rs->fields("idarticle") );;
				$reference = $rs->fields("reference");
				$summary = $rs->fields("summary");
				$delivdelay = DBUtil::getDBValue( "delay_1", "delay", "iddelay", $rs->fields("delivdelay") );
				$unit_price = Util::priceFormat($rs->fields("unit_price"));
				$quantity = $rs->fields("quantity");
				$ref_discount = ($rs->fields("ref_discount") > 0) ? Util::rateFormat($rs->fields("ref_discount")) : "-" ;
				$discount_price = Util::priceFormat($rs->fields("discount_price"));
				$total = Util::priceFormat($discount_price * $quantity);

				 
				 $html .="
				 <tr>
				 <td>
				 <img src='". $Icon ."' width='40' alt='Icone'>
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $reference ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".Util::doNothing($summary)."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". Util::doNothing($delivdelay) ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $unit_price ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $ref_discount ."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$discount_price."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ".$quantity."
				 </td>
				 <td><span style=\"font-size: 12px;\">
				 ". $total ."
				 </td>
				 </tr>";
				 
				 $rs->MoveNext();
	}
				 
		 
				 
	$html.="			 </table>
				
				";
		
		
		
		$html .= "<br>";
		
	
	$html .= "
</div>
</div>
</div>
</span>
</span>";
	
	return $html;
}
?>
