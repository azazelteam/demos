<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage erreur 404
 */

if( !preg_match( "/^.*\.php\$/", $_SERVER[ "PHP_SELF" ] ) )
	exit();

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CCategoryProxy.php" );

class ErrorController extends PageController{};
$erreur_404_template = new ErrorController( "error_404.tpl" );


$erreur_404_template->setData( "category", new CCategoryProxy( new CCategory( 0 ) ) );

$erreur_404_template->setData('appart_catalog',"Equipement de manutention et matériel de stockage");

$erreur_404_template->output();

?>