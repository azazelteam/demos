<?php

 /**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage d'une marque
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BrandProxy.php" );

if( isset( $_GET[ 'IdBrand' ] ) && !empty( $_GET[ 'IdBrand' ] ) ){
	$idbrand = $_GET[ 'IdBrand' ];
}else{
	header( "Location: " . HTTP_HOST );
	exit();
}

$brand = new BrandProxy( $idbrand );

if( $brand === false ){
	header( "Location: " . HTTP_HOST );
	exit();
}
/*
$page = 0;
if( isset( $_REQUEST[ 'page' ] ) && $_REQUEST[ 'page' ] > 0 ){
	$page = $_REQUEST[ 'page' ];
}
*/


//class BrandController extends PageController{};

//$charte_brand = new BrandController( "brand.tpl" );

//print $idbrand;exit;
$charteComptete = $brand->getTemplate();

$charte_brand = new PageController( $charteComptete );

//$charte_brand->setData( "brand", $brand );

$charte_brand->setData( "brand", $brand->getName() );
$charte_brand->setData( "description", $brand->getDescription() );
$charte_brand->setData( "keywords", $brand->getMetaKeywords() );
$charte_brand->setData( "logo", $brand->getLogo() );



	$charte_brand -> setData( "product" , $brand->getProducts() );

$charte_brand -> output();

//----------------------------------------------------------------------------------------------------------------------//

function getCollections( $idbrand ){
	global $GLOBAL_START_URL;
	
	$collections = DBUtil::query( "SELECT * FROM collection WHERE idbrand = $idbrand");
	
	$collectArray = array();
	
	if( $collections->RecordCount() > 0 ){
	
		$i = 0;
		while( !$collections->EOF ){
			$collectArray[$i]["idcollection"] = $collections->fields( "idcollection" );           
			$collectArray[$i]["name"] = $collections->fields( "name" );
			$collectArray[$i]["image"] = $collections->fields( "image" );
			$collectArray[$i]["description"] = $collections->fields( "description" );
			$collectArray[$i]["meta_title"] = $collections->fields( "meta_title" );
			$collectArray[$i]["meta_description"] = $collections->fields( "meta_description" );
			$collectArray[$i]["url"] = $GLOBAL_START_URL.'/'.$collections->fields( "collection_url" );
			
			if( isset( $_POST['idcollection'] ) && $_POST['idcollection'] > 0 && $_POST['idcollection'] == $collections->fields( "idcollection" ) ){
				$collectArray[$i]['selected'] = 1;
			}else{
				$collectArray[$i]['selected'] = 0;			
			}

			$collections->MoveNext();
			$i++;
		}
	}
	
	return $collectArray;
}

//----------------------------------------------------------------------------------------------------------------------//

function sortByCollection( $arrayToSort ){
	global $GLOBAL_START_URL , $idbrand;
	
	$collections = DBUtil::query( "SELECT * FROM collection WHERE idbrand = $idbrand " );
	$collectionArray = array();
	$i = 0;
	$currentCol = "";
	
	while( !$collections->EOF ){
		$currentCol = $collections->fields( "idcollection" );
		
		$collectionArray[$i]['url'] = $GLOBAL_START_URL.'/'.$collections->fields( "collection_url" );
		$collectionArray[$i]['name'] = $collections->fields( "name" );
		foreach( $arrayToSort as $product){
			if( $product["idcollection"] == $currentCol ){
				$collectionArray[$i]['products'][] = $product;
				
			}
		}
		
		$collections->MoveNext();
		$i++;
	}
	
	return $collectionArray;
}

//----------------------------------------------------------------------------------------------------------------------//

?>