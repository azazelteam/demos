<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des méta, title
 */
 
 
if( file_exists( "../objects/classes.php" ) )
	include_once( "../objects/classes.php" );

include_once( "$GLOBAL_START_PATH/script/global.fct.php" );

$conmeta = DBUtil::getConnection();

$DEBUG = 'code';

// Liste des <meta ...> à afficher si définis

//si présence d'une catégorie

if(isset( $IdCategory ) && $IdCategory>0){
	$reqcat="SELECT title_1, referencing_1, referencing_text_1, title_keyword_1 FROM category WHERE idcategory=$IdCategory";
	$rscat=DBUtil::query($reqcat);	
	
	if($rscat->fields("title_1")!=""){
		$html_title	= $rscat->fields("title_1");
	}else{
		$ttkw1=$rscat->fields("title_keyword_1");
		if(isset($HTML_title)){
			$html_title =$HTML_title." ".$ttkw1;
		}else{
			$html_title.=" ".$ttkw1;
		}
	}
	
	$meta_keywords 		= $rscat->fields("referencing_1");
	$html_description 	= $rscat->fields("referencing_text_1");
	
	
}else{
	
	$meta_keywords 		= getMetaDCTitle( "soc" );
	$html_description 	= "";
	
}

if(isset($IdProduct) && $IdProduct>0){

	$reqprod="SELECT title_1, title_keyword_1 FROM product WHERE idproduct=$IdProduct";
	$rsprod=DBUtil::query($reqprod);	
	
	if($rsprod->fields("title_1")!=""){
		$html_title			= $rsprod->fields("title_1");
	}else{
		$ttkw1=$rsprod->fields("title_keyword_1");
		if(isset($HTML_title)){
			$html_title =$HTML_title." ".$ttkw1;
		}else{
			$html_title.=" ".$ttkw1;
		}
	}
	
}

if($IdProduct || $IdCategory || isset( $_GET[ "IdTrade" ] ) ){
	$meta_desc = getMetaDescription( Dictionnary::Translate("MetaDescription") );
}else{
	$meta_desc = Dictionnary::Translate("MetaDescription");
}
$meta_keywords .= 		getMetaKeywords( Dictionnary::Translate("MetaKeywords") );
$meta_author =			Dictionnary::Translate("MetaAuthor");

if($_SERVER["PHP_SELF"]=="/index.php"){
	$html_title = getHTMLTitle( Dictionnary::Translate("MetaTitle") ) ;
}

if( isset( $_GET[ "IdTrade" ] ) )
	$html_title = DBUtil::getDBValue( "title", "trade", "idtrade", intval( $_GET[ "IdTrade" ] ) );
if(isset( $html_title ) && $html_title!=""){
	$html_title	= $html_title;
}else{
	$html_title = getHTMLTitle( Dictionnary::Translate("MetaTitle") );
}

$HTML_title=$html_title;
// Ajout d'attributs HTML à la balise <body$bodyAtt>
// Ex: OnLoad="javascript: test()"
$bodyAtt = empty($bodyAtt) ? null : $bodyAtt;

//Inscription à la newsletter
if( isset( $_POST[ "SubscribeNewsletter" ] ) || isset( $_POST[ "SubscribeNewsletter_x" ] ) ){
		
	$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;	
	
	if($idbuyer){	
		$error_login="";
		
		$idnewsletter = 0;
		$query = "INSERT INTO newsletter_subscription ( idbuyer, idnewsletter ) VALUES ( $idbuyer, $idnewsletter )";
	
		$rs = DBUtil::query( $query );
		
		if( $rs === false ){
			$error_msg=1;
		}else{
			$error_msg="";
		}
	}else{
		$error_login=1;
	}
}

?>