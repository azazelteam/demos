<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fichier principal des statistiques Gestion commerciale
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");


class GroupController extends PageController {}
$controller = new GroupController( "/account/groupement_history.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	
	//Gestion des droits revendeurs
	$GroupingCatalog = new GroupingCatalog();
	$controller->setData('buyerGroup', $GroupingCatalog->getGrouping( "grouping" ));
	$controller->setData('logoUser', Session::getInstance()->getCustomer()->get("logo"));
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));
	$controller->setData('issetPostLoginAndNotLogged', "ok");
	
	include_once( dirname( __FILE__ ) . "/../objects/Salesman.php" );
	
	$salesman = Basket::getInstance()->getSalesman();

	$controller->setData( "contact_lastname", $salesman->get("lastname") );
	$controller->setData( "contact_firstname", $salesman->get("firstname") );
	$controller->setData( "contact_phonenumber", Util::phonenumberFormat( $salesman->get("phonenumber") ) );
	$controller->setData( "contact_faxnumber", Util::phonenumberFormat( $salesman->get("faxnumber") ) );
	$controller->setData( "contact_email", $salesman->get("email") );
	
	$idbuyer = Session::getInstance()->getCustomer()->get("idbuyer");
	
	$query = "SELECT contact_grouping FROM contact where idbuyer = '$idbuyer' ";
	$rs = & DBUtil::query( $query );
	
	$contact_grouping = $rs->fields("contact_grouping");
	$controller->setData( "contact_grouping", $contact_grouping );
	
} else {
	$admin_order->setData('isLogin', "");
}










//-----------------------------------------------------------------------------


include_once('../objects/classes.php'); 
include_once('../catalog/drawlift.php');
include_once('../statistics/objects/drawlift_stats.php'); 
include_once('../objects/Menu.php');
include_once('../statistics/objects/calendar.php');
include_once('../statistics/objects/getfunctions.inc.php');
include_once('../statistics/objects/statsFunctions.php');
include_once('../statistics/objects/convertPeriod.php');

// initialisation des critères de tri ----------------------------------------


include_once('../statistics/objects/status.inc.php');

// Include des pages de stats ------------------------------------------------
include_once('../statistics/objects/tableStats.php');
include_once('../statistics/objects/init_arr_stat.php');
include_once('../statistics/objects/camembert.php');
include_once('graph.class.php');
include_once('../statistics/objects/marge_sort.php');

/*if( isset( $_GET[ "cat" ] ) )
	$_SESSION[ "statcat" ] = $_GET[ "cat" ];*/

// initialisation message d'erreur -------------------------------------------

$ERROR_MSG = '<span class="errormsg">- Aucune donnée ne correspond à votre recherche -</span>';

// mise en place du menu haut ------------------------------------------------
if(isset($_SESSION["statcat"])){
	$cat = $_SESSION["statcat"];
}else{
	$statPageTitle='Statistiques';
}



 
// Paramétrage couleurs --------------------------------------------------------

	$filename = "../statistics/colors.init.htm";
	$handle = fopen ($filename, "r"); 
	$filecolors = fread ($handle, filesize ($filename));
	fclose ($handle); 
    $camcolor   = $histocolor = preg_split("/,?#/",$filecolors,-1,PREG_SPLIT_NO_EMPTY);
 
// Initialisation ---------------------------------------------------------------

  	$arr=array();	
  	$db = &DBUtil::getConnection();
  
	// Initialisation des abscisses histogramme et filtre de recherche
 		$datesearch		= Date("Y"); //Date de filtre	
 		
 		$datesearchsize	= strlen($datesearch);		//Taille du filtre de date
		$datesize 		= $datesearchsize+3;		//Taille en caracteres de la date en abscisse
		
		
		$searchDateParam = "period";


//Géneration automatiques des données a partir des commandes confirmées

	
$username = DBUtil::getDBValue( "login", "user", "iduser", 15 );

		$q1 = "SELECT 
				 o.idbuyer,
				 b.idgrouping,
				 o.conf_order_date,
				 o.idorder,
				 orow.reference,
				 orow.quantity,
				 orow.discount_price,
				 orow.idcategory
			
		
			FROM `order` o
 				 LEFT JOIN `order_row` orow ON o.idorder=orow.idorder
 				 LEFT JOIN buyer b ON b.idbuyer=o.idbuyer 
   			WHERE o.status = 'Ordered' and b.idgrouping != 0";
   			
   		$rs1 = $db->Execute( $q1 );
   		
   		
					 
		while( !$rs1->EOF() )
		{
			
			$idbuyer = $rs1->fields('idbuyer');
			$idgrouping = $rs1->fields('idgrouping');
			
			$date = explode(" ", $rs1->fields('conf_order_date'));
			$conf_order_date = $date[0];
			
			$idorder = $rs1->fields('idorder');
			$reference = $rs1->fields('reference');	
			$quantity = $rs1->fields('quantity');
			$discount_price = $rs1->fields('discount_price');
			$idcategory = $rs1->fields('idcategory');
			
			
			//Vérifications si l'element existe déja dans la table
			$q2 = "SELECT *			
		
			FROM grouping_story

   			WHERE   idbuyer = '$idbuyer'
				and idgrouping = $idgrouping
				and conf_order_date = '$conf_order_date'
				and idorder = '$idorder'
				and reference = '$reference'
				and quantity = $quantity
				and discount_price = $discount_price
				and idcategory = $idcategory   			
   			";
   			
   		$rs2 = $db->Execute( $q2 );
			
			if (($idcategory > 0) and ($rs2->RecordCount() == 0 ) ) {
			$qinsert = "INSERT IGNORE INTO grouping_story (
				idbuyer,
				idgrouping,
				conf_order_date,
				idorder,
				reference,
				quantity,
				discount_price,
				idcategory,
				lastupdate,
				username
				 )
				VALUES(
				'$idbuyer',
				$idgrouping,
				'$conf_order_date',
				'$idorder',
				'$reference',
				$quantity,
				$discount_price,
				$idcategory,
				now(),
				'$username'
				)";
			
		$rsinsert =& DBUtil::query( $qinsert );
			}	
			

			
			
			$rs1->MoveNext();
			
	        }




//Fin géneration automatiques des données




//formulaire de recherche

global	$GLOBAL_START_PATH,
			$GLOBAL_START_URL;
	
	include_once( "$GLOBAL_START_PATH/objects/DHTMLCalendar.php" );
	
	include_once( "$GLOBAL_START_PATH/catalog/admin_func.inc.php" );
	include_once( "$GLOBAL_START_PATH/catalog/drawlift.php" );
	
		
	$formFields = array(
	
		$fieldName = 
		"start_date"		=> date( "d-m-Y", mktime( 0, 0, 0, 1, 1, date( "Y" ) ) ),//usDate2eu( $maxDate ),
		"end_date"			=> date( "d-m-Y", mktime( 0, 0, 0, 12, 31, date( "Y" ) ) ),
		"idbuyer" 			=> "",
		"grouping" 			=> ""
	);
	
	$postData = array();
	
	foreach( $formFields as $fieldName => $defaultValue )
	{
		$postData[ $fieldName ] = isset( $_POST[ $fieldName ] ) ? stripslashes( $_POST[ $fieldName ] ) : $defaultValue;
	
	}
?>
		
			
			
			<style type="text/css">
			/* <![CDATA[ */
				
				.mainAccount:hover{
					background-color:#F5F5F5;
				}
				
				.openedAccount{
					background-color:#F5F5F5;
				}
				
				.openedAccount:hover{
					background-color:#F5F5F5;
				}
				
			/* ]]> */
			</style>
			<?php
			$form = "";
			
			$form .= '<form action="/catalog/groupement_history.php" method="post" id="SearchForm">
				
					<div class="subContent" id="export">
					<input type="hidden" name="export" id="hiddenExport" value="0" />
					<input type="hidden" name="search" id="search" value="1" />

					<div class="tableContainer">
					<table class="dataTable" width="100%">';
							
							
			$form .='	<tr>
					<th><span style="font-size: 12px;">Entre le</th>
					<td>
					<input type="text" name="start_date" id="start_date" class="calendarInput" value='. $postData[ "start_date" ] .' />';
			$form .= '	<a href="javascript:calendar(start_date)" id="test_a" ></a>';
					$displayIcon = true; $calledFunction = false;
	
			$form .= '	</td>';
							
			$form .='	<th><span style="font-size: 12px;">Et le</th>
					<td>
					<input type="text" name="end_date" id="end_date" class="calendarInput" value='. $postData[ "end_date" ] .' />';
			$form .= '	<a href="javascript:calendar(end_date)" id="test_b" ></a>';

			$form .= '
					</table>
					</div>
					<div class="submitButtonContainer" style="float:right;padding-right: 200px;">
					<input type="submit" class="blueButton" value="Rechercher" />
					</div>
					</div>
					<input type="hidden" name="detail" id="detail" value="1" />
					</form>';




			$controller->setData( "form",$form );


  		// Nombre de commerciaux afficher par défaut
 		$nbrComInital = 5;
 		$arrayresult = "";
 		
 		if(isset($_POST["start_date"]))
		$start = $_POST["start_date"];
		else
		$start =$postData[ "start_date" ];
		
		$start = explode("-", $start);
		
		$start_date = $start[2] . "-" . $start[1] . "-" . $start[0];
		
		
		if(isset($_POST["end_date"]))
		$end = $_POST["end_date"];
		else
		$end =$postData[ "end_date" ];
		
		$end = explode("-", $end);
		
		$end_date = $end[2] . "-" . $end[1] . "-" . $end[0];
		
		$statStartDate = $start_date;
		$statEndDate = $end_date;


		$listSupplier = 0;
 		
 		// Initialisation
 		$arrCA=array();
		$stat_filter=" (conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' ) ";
		$GroupingCatalog = new GroupingCatalog();
		
		if  ($GroupingCatalog->getGrouping( "idgrouping" ))
		{
		$grouping = $GroupingCatalog->getGrouping( "idgrouping" );
		$where = " and idgrouping = ". $GroupingCatalog->getGrouping( "idgrouping" );
		}

		
		$graphCA = new Graph('graphCA');
		$graphCA->nomImg = "temp/".Date("YmdHms")."-HistogrammeCA3"; // Nom du fichier temp image  
		$total = array();
		$total = initHisto($searchDateParam,$datesearch,$statStartDate,$statEndDate);

		
		//Traitement des données
		$query = "SELECT conf_order_date AS DateH, idbuyer, quantity, discount_price 
				FROM grouping_story
				WHERE $stat_filter $where";
									 
		$rs = & DBUtil::query( $query );
		$n= $rs->RecordCount();
		for($i=0;$i<$n;$i++){
		
			if(!isset($arrCA[$rs->Fields('idbuyer')])){ $arrCA[$rs->Fields('idbuyer')]=initHisto($searchDateParam,$datesearch,$statStartDate,$statEndDate); }
			if(isset($arrCA[$rs->Fields('idbuyer')][$rs->Fields('DateH')])){
				$arrCA[$rs->Fields('idbuyer')][$rs->Fields('DateH')] += $rs->Fields('discount_price')*$rs->Fields('quantity');
			}
			else
			{
				$arrCA[$rs->Fields('idbuyer')][$rs->Fields('DateH')] = $rs->Fields('discount_price')*$rs->Fields('quantity');
			}
			$rs->Movenext();
		}

		// Tri des données
		$numCom = 1; $nbcom = 1;
		foreach($arrCA as $key=>$val){
			$sort1[$key]=array_sum($arrCA[$key]);
		}
		arsort($sort1);
		if(!isset($listSupplier) or count($listSupplier)==0 ){
			foreach($sort1 as $key=>$val){
				if($nbcom<=$nbrComInital){ 
					$sort[$key]=$sort1[$key];
					$nbcom ++;
				}
			}
		}
		else
		{ 	$sort=$sort1;	}
		

		
		// Affichage de l'histogramme
		
		$arrayresult .= '<div align="center"><table style="border: 1px solid #cccccc;" cellspacing="10">'."\n";
		foreach($sort as $key=>$val){
		
		$q1 = "SELECT company
				FROM buyer
				WHERE idbuyer = '$key'";
									 
		$rs1 = & DBUtil::query( $q1 );
		if ($rs1->fields("company") !="")
		$company = $rs1->fields("company");
		else
		$company = "-";
		
				
		$arrCA[$key] = convertPeriod($statStartDate,$statEndDate,31,$arrCA[$key],'histo');
      		$graphCA->setSerie($numCom, $histocolor[$numCom], Dictionnary::translate('stat_turnover').' HT', $arrCA[$key]);
      		
      		if (array_sum($arrCA[$key]) > 0)
      		{
      		
      		$arrayresult .= '<tr><td><div style="width: 15px; height: 15px; background-color: #'.$histocolor[$numCom-1].'; float: left;">&nbsp;</div></td>';
		$arrayresult .= '<td style="text-align: left;"><span style="font-size: 12px;"><b>'.$key.' / '.((mb_detect_encoding($company,mb_detect_order(),true) != 'UTF-8') ? Util::doNothing($company) : $company).' </span></b></td><td align="right"><span style="font-size: 12px;">'.number_format(array_sum($arrCA[$key]),2,',',' ').' &euro;</span></td></tr>' ;
		
		}
			$numCom++;
			$svgdkey = $key;
		}
		$arrayresult .= '</tr></table><br><br>';
		
		
		if(!isset($_GET['day'])) { 
			$arra=$graphCA->getArray();
			$graphCAL = new Graph('graphCAL');
			$graphCAL = $graphCA;
			$graphCAL->nomImg = "temp/".Date("YmdHms")."-HistogrammeCAL3"; // Nom du fichier temp image
			$graphname = Date("YmdHms")."-HistogrammeCAL3";
			$graphCAL->ShowLegend();
			$graphCAL->Save();
			/**
			 * EXPORT CSV
			 */
			$exportCSV = array();
			
			$total = convertPeriod($statStartDate,$statEndDate,31,$total,'histo');
			
			//ligne des années
	
			$dates = array_keys( $arrCA[ $svgdkey ] );
	
			$years = array();
			foreach( $dates as $date ){
	
				$year = substr( $date, 0, 4 );
				$years[ $year ] = isset( $years[ $year ] ) ? $years[ $year ] + 1 : 1;
		
			}
			
			ksort( $years );
			
			if( count( $years ) ){
				$arrayYear = array();
				$arrayYear[] = Dictionnary::translate( "stat_year" );
				
				foreach( $years as $year => $columnCount )
					$arrayYear[] = $year;
				
				$exportCSV[] = $arrayYear;				
			}
			
			//ligne des mois
			
			setlocale( LC_TIME, "fr_FR.utf8" );
	
			$dates = array_keys( $arrCA[ $svgdkey ] );
	
			$months = array();
			$lastmonth = 0;
			foreach( $dates as $date ){
			
				$month = substr( $date, 5, 2 );
				
				if( $month == $lastmonth ){
					
					$months[ count( $months ) - 1 ][ "count" ]++;
					
				}
				else $months[] = array( "number" => substr( $date, 5, 2 ), "count" => 1 );
				
				$lastmonth = $month;
				
			}
	
			if( count( $months ) ){
				$arrayMonth = array();
				
				$arrayMonth[] = Dictionnary::translate( "stat_month" );
				
				foreach( $months as $monthInfos  ){
	
					$columnCount = $monthInfos[ "count" ];
					$monthNumber =  $monthInfos[ "number" ];
					
					$timestamp = mktime( 0, 0, 0, $monthNumber, 1, date( "Y" ) );
					$str_month = strftime("%B", $timestamp );
				
					$arrayMonth[] = $str_month;
				
				}
				
				$exportCSV[] = $arrayMonth;
			}
			
			//ligne des jours
			
			$datePattern = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}\$/";
			
			$dates = array_keys( $arrCA[ $svgdkey ] );
			$daysAvailable = count( $dates ) > 0;
			
			$i = 0;
			while( $i < count( $dates ) ){
			
				$daysAvailable &= preg_match( $datePattern, $dates[ $i ] );
				$i++;
				
			}
			
			if( $daysAvailable ){
				$arrayDay = array();
				
				$arrayDay[] = Dictionnary::translate( "stat_day" );
		
				foreach($arrCA[$svgdkey] as $key=>$val){	
					
					$key = substr( $key, 8 );
						
					$arrayDay[] = $key;
				
				}
				$exportCSV[] = $arrayDay;
			
			}
			
			//Valeurs et fournisseurs
			foreach($sort as $key=>$val){
				$arrayLigne = array();
				$arrayLigne[] = $key;
				
				foreach($arrCA[$key] as $key2=>$val2){
				
					$str_value = $val2 > 0.0 ? strval( number_format($val2,2,'.',' ') ) : "-";
					$arrayLigne[] = $str_value;
					$total[$key2] += $val2;
					
				}
			$exportCSV[] = $arrayLigne;
			
			}
			
			$arrayLigne =array();
			$arrayLigne[] = "Total";
			
			foreach($total as $key=>$valtot){ 
			
				$str_value = $valtot > 0.0 ? strval( number_format($valtot,2,'.',' ') ) : "-";
	
				$arrayLigne[] = $str_value; 
			
			}
			$exportCSV[] = $arrayLigne;
			 
			 /*
			  * FIN EXPORT CSV
			  */
			//nouveau export
			$export = array();


$arraytitle = array("N. client","Ehpad","N. cde","Date cde","N. categorie","Categorie","Reference","Designation","Prix HT");

$export[] = $arraytitle;


foreach($sort as $key=>$val)
{
	
$where = "";	
$GroupingCatalog = new GroupingCatalog();
		
		if  ($GroupingCatalog->getGrouping( "idgrouping" ))
		{
		$grouping = $GroupingCatalog->getGrouping( "idgrouping" );
		$where = " and gs.idgrouping = ". $GroupingCatalog->getGrouping( "idgrouping" );
		}

	
	
	$qr2 = " SELECT  b.company,
				gs.idorder,
					gs.conf_order_date,
					gs.idcategory,
					c.name_1,
					gs.reference,
					d.summary_1,
					gs.quantity,
					gs.discount_price
			FROM 	grouping_story gs,
					category c,
					detail d,
					buyer b
			WHERE	gs.idbuyer = '$key'
			AND		gs.idcategory = c.idcategory
			AND		gs.reference = d.reference
			AND		gs.idbuyer = b.idbuyer
			AND 	(gs.conf_order_date BETWEEN '$statStartDate' AND '$statEndDate' )
			".$where."
			ORDER  BY gs.conf_order_date ASC
		";
	
	$res2 = & DBUtil::query( $qr2 );

	 while( !$res2->EOF ){


				 
				
				$arraydata = array($key,
									$res2->fields("company"),
									$res2->fields("idorder"),
									$res2->fields("conf_order_date"),
									$res2->fields("idcategory"),
				 					$res2->fields("name_1"),
									$res2->fields("reference"),
									$res2->fields("summary_1"),
									$res2->fields("discount_price")*$res2->fields("quantity"));
				 $export[] = $arraydata;
				 
				 $res2->MoveNext();
	}
				
}

			
			
			
			$arrayresult .=LinkExportHistofront($export,'',0,Date("YmdHms")."-HistogrammeCAL3",'turnover');
			
			
			//$graphCA->Show(); //Affichage 
		

			
			
			$arrayresult .= $graphCA->Show();
			$arrayresult .= "</div>";
			
		}
		
		// Affichage tableau
		
		?>
		</div>
		<?php
		$arrayresult .= '<div style="margin:5px;">';
		$arrayresult .= '<br />';
		$arrayresult .= '<div class="subTitleContainer" style="margin-bottom:6px;"><p class="subTitle">Tableau r&eacute;capitulatif : </p></div>';
		$arrayresult .= '<table class="resultTable" border="1" style="width: 100%; border-collapse: collapse; border-color: #BBBBBB;" cellspacing="10">';
		
				
		$total = convertPeriod($statStartDate,$statEndDate,31,$total,'histo');
		
		//ligne des années

		$dates = array_keys( $arrCA[ $svgdkey ] );

		$years = array();
		foreach( $dates as $date ){

			$year = substr( $date, 0, 4 );
			$years[ $year ] = isset( $years[ $year ] ) ? $years[ $year ] + 1 : 1;
	
		}
		
		ksort( $years );
		
		if( count( $years ) ){
		
			$arrayresult .= "
			<tr>
				<th style=\"text-align:center; background-color: rgb(238, 238, 238);\">" . Util::doNothing(Dictionnary::translate( "stat_year" )) . "</th>";
			
			foreach( $years as $year => $columnCount )
				$arrayresult .= "<td colspan=\"$columnCount\" style=\" text-align:center; background-color: rgb(238, 238, 238);\">$year</td>";
			
			$arrayresult .= "
			</tr>";
			
		}
		
		//ligne des mois
		
		setlocale( LC_TIME, "fr_FR.utf8" );

		$dates = array_keys( $arrCA[ $svgdkey ] );

		$months = array();
		$lastmonth = 0;
		foreach( $dates as $date ){
		
			$month = substr( $date, 5, 2 );
			
			if( $month == $lastmonth ){
				
				$months[ count( $months ) - 1 ][ "count" ]++;
				
			}
			else $months[] = array( "number" => substr( $date, 5, 2 ), "count" => 1 );
			
			$lastmonth = $month;
			
		}

		if( count( $months ) ){
	
			$arrayresult .= "
			<tr>
				<th style=\"font-weight:bold; text-align:center; background-color: rgb(238, 238, 238);\">" . Dictionnary::translate( "stat_month" ) . "</th>";
			
			foreach( $months as $monthInfos  ){

				$columnCount = $monthInfos[ "count" ];
				$monthNumber =  $monthInfos[ "number" ];
				
				$timestamp = mktime( 0, 0, 0, $monthNumber, 1, date( "Y" ) );
				$str_month = Util::doNothing(strftime("%B", $timestamp ));
			
				$arrayresult .= "<td colspan=\"$columnCount\" style=\"font-weight:bold; text-align:center; background-color: rgb(238, 238, 238);\">$str_month</td>";
			
			}
			
			$arrayresult .= "
			</tr>";
			
		}
		
		//ligne des jours
		
		$datePattern = "/^[0-9]{4}-[0-9]{2}-[0-9]{2}\$/";
		
		$dates = array_keys( $arrCA[ $svgdkey ] );
		$daysAvailable = count( $dates ) > 0;
		
		$i = 0;
		while( $i < count( $dates ) ){
		
			$daysAvailable &= preg_match( $datePattern, $dates[ $i ] );
			$i++;
			
		}
		
		if( $daysAvailable ){
		
			$arrayresult .= "
			<tr>
				<th style=\"font-weight:bold; text-align:center; background-color: rgb(238, 238, 238);\">" . Dictionnary::translate( "stat_day" ) . "</th>";
	
			foreach($arrCA[$svgdkey] as $key=>$val){	
				
				$key = substr( $key, 8 );
					
				$arrayresult .= '<td style=\"font-weight:bold; text-align:center; background-color: rgb(238, 238, 238);\">'.($key).'</td>';
			
			}
			
			$arrayresult .= '</tr>'."\n";
		
		}
		
		//Valeurs et fournisseurs
	
		
		foreach($sort as $key=>$val){
			
			$q1 = "SELECT company
				FROM buyer
				WHERE idbuyer = '$key'";
									 
			$rs1 = & DBUtil::query( $q1 );
			if ($rs1->fields("company") !="")
			$company = $rs1->fields("company");
			else
			$company = "-";
			
			$_SESSION["start"] = $statStartDate;
			$_SESSION["end"] = $statEndDate;
			//$_SESSION["buyer"] = $idbuyer;
			$_SESSION["grouping"] = $grouping;
			
			$arrayresult .= '<tr><th>'.$key."/".((mb_detect_encoding($company,mb_detect_order(),true) != 'UTF-8') ? Util::doNothing($company) : $company).'
			<a href="'.$GLOBAL_START_URL.'/catalog/detail_historique.php?buyer='.$key.'">
			<img src="'.$GLOBAL_START_URL.'/images/show.gif"></a></th>';
			
			foreach($arrCA[$key] as $key2=>$val2){
			
				$str_value = $val2 > 0.0 ? strval( number_format($val2,2,',',' ') ) : "-";
				
				$arrayresult .= "<td style=\"padding:2px; text-align:center; white-space:nowrap;\">$str_value</td>";
				
				//$total[$key2] += $val2;
				
			}
			$arrayresult .= '</tr>'."\n";
			
		
		}
		$arrayresult .= "<tr><th style=\"font-weight:bold; text-align:center; background-color: rgb(238, 238, 238);\">Total :</th>";
		
		foreach($total as $key=>$valtot){ 
		
			$str_value = $valtot > 0.0 ? strval( number_format($valtot,2,',',' ') ) : "-";

			$arrayresult .= "<td style=\"padding:2px; font-weight:bold; text-align:center; background-color: rgb(238, 238, 238); white-space:nowrap;\">$str_value</td>"; 
		
		}
		$arrayresult .= '</tr>'."\n";
		$arrayresult .= '</table>';
		


$controller->setData( "arrayrecap",$arrayresult ); //Affichage



/* On ferme l'envoi de données */
$controller->output();
?>

<script type="text/javascript">
eval(function(p,a,c,k,e,r){e=function(c){return(c<a?'':e(parseInt(c/a)))+((c=c%a)>35?String.fromCharCode(c+29):c.toString(36))};if(!''.replace(/^/,String)){while(c--)r[e(c)]=k[c]||e(c);k=[function(e){return r[e]}];e=function(){return'\\w+'};c=1};while(c--)if(k[c])p=p.replace(new RegExp('\\b'+e(c)+'\\b','g'),k[c]);return p}('(18($){18 3X(z,A,B){Z C=[];Z D={2J:["#60","#5x","#56","#4Y","#4R"],1D:{1z:1B,4p:1,3k:14,3Y:"#5w",3q:14,1S:"51",3K:5,2a:14,34:0.2S},2y:{1q:14,3x:5,2T:3g,2I:14,12:14,13:14,2Q:14},2P:{3x:5,1q:14,2T:3g,12:14,13:14,2Q:0.5Z},1u:{1z:1t,23:3,1j:2,1E:1B,1R:"#52"},1U:{1z:1t,1j:2,1E:1t,1R:14},1r:{1z:1t,1j:2,2b:1,1E:1B,1R:14,2F:0.4,2E:1t},1Z:{1z:1t,23:50,4Q:1,1E:1B,1R:14,2F:0.2S,3E:30,3A:10,2E:1t},1G:{1n:"#6L",2a:14,4E:"#6E",2X:3,4v:14},22:{2i:14,1n:"#6x"},2h:4};Z E=14,1X=14;Z F=14,1I=14;Z G=z;Z H={};Z I={};Z J={1o:0,1x:0,1f:0,1w:0};Z K=0;Z L=0;Z M=0;Z N=0;Z O=0;Z P=0;Z Q=0;Z R=0;C=3V(A);4A(B);3T();4g();3R();4H();3P();3e(H,D.2y);3d(H,D.2y);3c(H,D.2y);3b();3e(I,D.2P);3d(I,D.2P);3c(I,D.2P);3a();3M();3L();1e.4X=18(){1d E};1e.4W=18(){1d J};1e.2f=2f;1e.37=37;18 3V(d){Z a=[];1m(Z i=0;i<d.1c;++i){Z s;Y(d[i].1v){s={};1m(Z v 4P d[i])s[v]=d[i][v]}1a{s={1v:d[i]}}a.20(s)}1d a}18 4A(o){$.33(1B,D,o)}18 3T(){Y(D.1r.1z&&D.1r.2E)D.1r.2b=1/C.1c/2}18 3R(){M=G.1L();N=G.28();G.6O("");G.3y("1S","6N");Y(M<=0||N<=0)6M"6K 6J 1m 4D, 1L = "+M+", 28 = "+N;E=2r(\'<2V 1L="\'+M+\'" 28="\'+N+\'"></2V>\').2q(G).2U(0);Y(2r.4x.4K)E=3s.4t.4s(E);F=E.4r("2d");1X=2r(\'<2V 1J="1S:2e;1o:4l;1f:4l;" 1L="\'+M+\'" 28="\'+N+\'"></2V>\').2q(G).2U(0);Y(2r.4x.4K)1X=3s.4t.4s(1X);1I=1X.4r("2d")}18 4H(){Y(D.22.2i!=14){$(1X).6u(4j);G.2U(0).6s=4f}Y(D.1G.4v&&D.1Z.1z)$(1X).6n(4a)}18 3P(){I.1P=H.1P=0;H.1H=I.1H=1;Y(C.1c==0)1d;Z i,3h=1t;1m(i=0;i<C.1c;++i){Y(C[i].1v.1c>0){H.1P=H.1H=C[i].1v[0][0];I.1P=I.1H=C[i].1v[0][1];3h=1B;46}}Y(!3h)1d;1m(i=0;i<C.1c;++i){Z a=C[i].1v;1m(Z j=0;j<a.1c;++j){Z x=a[j][0];Z y=a[j][1];Y(x<H.1P)H.1P=x;1a Y(x>H.1H)H.1H=x;Y(y<I.1P)I.1P=y;1a Y(y>I.1H)I.1H=y}}}18 3e(a,b){Z c=b.12!=14?b.12:a.1P;Z d=b.13!=14?b.13:a.1H;Y(d-c==0.0){Z e;Y(d==0.0)e=1.0;1a e=0.6c;c-=e;d+=e}1a{Z f=b.2Q;Y(f!=14){Y(b.12==14){c-=(d-c)*f;Y(c<0&&a.1P>=0)c=0}Y(b.13==14){d+=(d-c)*f;Y(d>0&&a.1H<=0)d=0}}}a.12=c;a.13=d}18 3d(a,b){Z c=(a.13-a.12)/b.3x;Z d=b.2I;Z e=-1b.2n(1b.65(c)/1b.5X);Y(d!=14&&e>d)e=d;Z f=1b.5V(10,-e);Z g=c/f;Z h=1;Y(g<1.5)h=1;1a Y(g<3){h=2;Y(g>2.25&&(d==14||e+1<=d)){h=2.5;++e}}1a Y(g<7.5)h=5;1a h=10;a.2N=h*f;a.2I=1b.13(0,(d!=14)?d:e)}18 3b(){Y(D.2y.13==14){Z a=H.13;1m(Z i=0;i<C.1c;++i)Y(C[i].1r.1z&&C[i].1r.2b+H.1H>a)a=H.1H+C[i].1r.2b;H.13=a}}18 3g(a,b){1d a.5K(b.2I)}18 3c(a,b){Z i,v;a.1q=[];Y(b.1q){Z c=b.1q;Y($.5J(c))c=c({12:a.12,13:a.13});1m(i=0;i<c.1c;++i){Z d=14;Z t=c[i];Y(3j(t)=="5D"){v=t[0];Y(t.1c>1)d=t[1]}1a v=t;Y(d==14)d=""+b.2T(v,a);a.1q[i]={v:v,1Q:d}}}1a{Z e=a.2N*1b.2n(a.12/a.2N);i=0;45{v=e+i*a.2N;a.1q.20({v:v,1Q:""+b.2T(v,a)});++i}2l(v<a.13)}Y(b.2Q!=14){Y(b.12==14)a.12=a.1q[0].v;Y(b.13==14&&a.1q.1c>1)a.13=a.1q[a.1q.1c-1].v}}18 3a(){Z i,2O="";1m(i=0;i<I.1q.1c;++i){Z l=I.1q[i].1Q.1c;Y(l>2O.1c)2O=I.1q[i].1Q}Z a=$(\'<1y 1J="1S:2e;1f:-5s;3B-3o:3m" 2t="3r">\'+2O+\'</1y>\').2q(G);K=a.1L();L=a.28();a.5j();Z b=2;Y(D.1u.1z)b=1b.13(b,D.1u.23+D.1u.1j/2);1m(i=0;i<C.1c;++i){Y(C[i].1u.1z)b=1b.13(b,C[i].1u.23+C[i].1u.1j/2)}J.1o=J.1x=J.1f=J.1w=b;J.1o+=K+D.1G.2X;J.1w+=L+D.1G.2X;O=M-J.1o-J.1x;P=N-J.1w-J.1f;Q=O/(H.13-H.12);R=P/(I.13-I.12)}18 4e(a){1m(i=0;i<a.1c;i++)3U(a[i],i,a.1c)}18 4q(a){1m(i=0;i<a.1c;i++){Z b=0;1m(j=0;j<a[i].1v.1c;j++)b+=1A(a[i].1v[j][1]);a[i].1v=b}}18 4z(a){Z b=0;Z c=0;Z d=O/2;Z e=P/2;Z f=0;Z g=0;Z h=D.1Z.3A;Z k=D.1Z.3E;Y(!D.1Z.2E)c=D.1Z.23;1a c=(P*0.2S)/2;Z l=c*1.57;1m(i=0;i<a.1c;i++)b+=a[i].1v;1m(j=0;j<a.1c;j++){Z m=a[j].1v/b;f=g;g+=m*(2*1b.21);Z n=(g-f)/2+f;Z o=d+1b.3O(n)*l;Z p=e+1b.3N(n)*l;Z q=d+1b.3O(n)*2;Z r=e+1b.3N(n)*2;Z s=14;Z t=14;Z u=0;Z v=0;F.27();F.1M(q,r);F.39(q,r,c,f,g,1t);F.4Z();F.2j=1W(a[j].1n).29(14,14,14,D.1Z.2F).1V();Y(D.1Z.1E)F.1E();Y(n<=0.25*(2*1b.21)){s="1o";t="1f";u=o;v=p+h}1a Y(n>0.25*(2*1b.21)&&n<=0.5*(2*1b.21)){s="1o";t="1w";u=o-k;v=p}1a Y(n>0.5*(2*1b.21)&&n<=0.3J*(2*1b.21)){s="1x";t="1w";u=o-k;v=p-h}1a{s="1x";t="1w";u=o;v=p-h}u=u+"1C";v=v+"1C";Z w=1b.2g(m*2v);Z x="<1y 1J=\\"1S: 2e;z-3I:11; 1L:"+k+"1C;3A:"+h+"1C;4V:4U;1f:"+v+";1o:"+u+";4T:"+s+";4S:"+t+"\\">"+w+"%</1y>";$(x).2q(G)}}18 3M(){Y(D.1r.1z){H.13+=(C.1c+1)*D.1r.2b;H.12-=(C.1c+1)*D.1r.2b;3b();3a();36();35();4e(C)}1a Y(D.1Z.1z){4q(C);4z(C)}1a{36();35();1m(Z i=0;i<C.1c;i++){3H(C[i])}}}18 1i(x){1d(x-H.12)*Q}18 1g(y){1d P-(y-I.12)*R}18 36(){F.2D();F.2C(J.1o,J.1f);Y(D.1G.2a!=14){F.2j=D.1G.2a;F.3G(0,0,O,P)}F.1j=1;F.1O=D.1G.4E;F.27();Z i,v;1m(i=0;i<H.1q.1c;++i){v=H.1q[i].v;Y(v<=H.12||v>=H.13)1p;F.1M(1b.2n(1i(v))+F.1j/2,0);F.1s(1b.2n(1i(v))+F.1j/2,P)}1m(i=0;i<I.1q.1c;++i){v=I.1q[i].v;Y(v<=I.12||v>=I.13)1p;F.1M(0,1b.2n(1g(v))+F.1j/2);F.1s(O,1b.2n(1g(v))+F.1j/2)}F.2u();F.1j=2;F.1O=D.1G.1n;F.2B="2g";F.3F(0,0,O,P);F.2A()}18 35(){Z i,1F;Z a=\'<1y 1J="3B-3o:3m;1n:\'+D.1G.1n+\'">\';Z b=O/6;1m(i=0;i<H.1q.1c;++i){1F=H.1q[i];Y(!1F.1Q||1F.v<H.12||1F.v>H.13)1p;a+=\'<1y 1J="1S:2e;1f:\'+(J.1f+P+D.1G.2X)+\'1C;1o:\'+(J.1o+1i(1F.v)-b/2)+\'1C;1L:\'+b+\'1C;3D-3C:4O" 2t="3r">\'+1F.1Q+"</1y>"}1m(i=0;i<I.1q.1c;++i){1F=I.1q[i];Y(!1F.1Q||1F.v<I.12||1F.v>I.13)1p;a+=\'<1y 1J="1S:2e;1f:\'+(J.1f+1g(1F.v)-L/2)+\'1C;1o:0;1L:\'+K+\'1C;3D-3C:1x" 2t="3r">\'+1F.1Q+"</1y>"}a+=\'</1y>\';G.4N(a)}18 4g(){Z i;Z a=C.1c;Z b=[];Z d=[];1m(i=0;i<C.1c;++i){Z e=C[i].1n;Y(e!=14){--a;Y(3j(e)=="4M")d.20(e);1a b.20(1W(C[i].1n))}}1m(i=0;i<d.1c;++i){a=1b.13(a,d[i]+1)}Z f=[];Z g=0;i=0;2l(f.1c<a){Z c;Y(D.2J.1c==i)c=1N 1K(2v,2v,2v);1a c=1W(D.2J[i]);Z h=g%2==1?-1:1;Z j=1+h*1b.6P(g/2)*0.2;c.29(j,j,j);f.20(c);++i;Y(i>=D.2J.1c){i=0;++g}}Z k=0;1m(i=0;i<C.1c;++i){Z s=C[i];Y(s.1n==14){s.1n=f[k].1V();++k}1a Y(3j(s.1n)=="4M")s.1n=f[s.1n].1V();s.1U=$.33(1B,{},D.1U,s.1U);s.1u=$.33(1B,{},D.1u,s.1u);s.1r=$.33(1B,{},D.1r,s.1r);Y(s.2h==14)s.2h=D.2h}}18 3H(a){Y(a.1U.1z||(!a.1r.1z&&!a.1u.1z))4L(a);Y(a.1u.1z)4J(a)}18 4L(g){18 32(a,b){Y(a.1c<2)1d;Z c=1i(a[0][0]),2z=1g(a[0][1])+b;F.27();F.1M(c,2z);1m(Z i=0;i<a.1c-1;++i){Z d=a[i][0],15=a[i][1],17=a[i+1][0],19=a[i+1][1];Y(15<=19&&15<I.12){Y(19<I.12)1p;d=(I.12-15)/(19-15)*(17-d)+d;15=I.12}1a Y(19<=15&&19<I.12){Y(15<I.12)1p;17=(I.12-15)/(19-15)*(17-d)+d;19=I.12}Y(15>=19&&15>I.13){Y(19>I.13)1p;d=(I.13-15)/(19-15)*(17-d)+d;15=I.13}1a Y(19>=15&&19>I.13){Y(15>I.13)1p;17=(I.13-15)/(19-15)*(17-d)+d;19=I.13}Y(d<=17&&d<H.12){Y(17<H.12)1p;15=(H.12-d)/(17-d)*(19-15)+15;d=H.12}1a Y(17<=d&&17<H.12){Y(d<H.12)1p;19=(H.12-d)/(17-d)*(19-15)+15;17=H.12}Y(d>=17&&d>H.13){Y(17>H.13)1p;15=(H.13-d)/(17-d)*(19-15)+15;d=H.13}1a Y(17>=d&&17>H.13){Y(d>H.13)1p;19=(H.13-d)/(17-d)*(19-15)+15;17=H.13}Y(c!=1i(d)||2z!=1g(15)+b)F.1M(1i(d),1g(15)+b);c=1i(17);2z=1g(19)+b;F.1s(c,2z)}F.2u()}18 4I(a){Y(a.1c<2)1d;Z b=1b.12(1b.13(0,I.12),I.13);Z c,3z=0;Z d=1B;F.27();1m(Z i=0;i<a.1c-1;++i){Z e=a[i][0],15=a[i][1],17=a[i+1][0],19=a[i+1][1];Y(e<=17&&e<H.12){Y(17<H.12)1p;15=(H.12-e)/(17-e)*(19-15)+15;e=H.12}1a Y(17<=e&&17<H.12){Y(e<H.12)1p;19=(H.12-e)/(17-e)*(19-15)+15;17=H.12}Y(e>=17&&e>H.13){Y(17>H.13)1p;15=(H.13-e)/(17-e)*(19-15)+15;e=H.13}1a Y(17>=e&&17>H.13){Y(e>H.13)1p;19=(H.13-e)/(17-e)*(19-15)+15;17=H.13}Y(d){F.1M(1i(e),1g(b));d=1t}Y(15>=I.13&&19>=I.13){F.1s(1i(e),1g(I.13));F.1s(1i(17),1g(I.13));1p}1a Y(15<=I.12&&19<=I.12){F.1s(1i(e),1g(I.12));F.1s(1i(17),1g(I.12));1p}Z f=e,2R=17;Y(15<=19&&15<I.12&&19>=I.12){e=(I.12-15)/(19-15)*(17-e)+e;15=I.12}1a Y(19<=15&&19<I.12&&15>=I.12){17=(I.12-15)/(19-15)*(17-e)+e;19=I.12}Y(15>=19&&15>I.13&&19<=I.13){e=(I.13-15)/(19-15)*(17-e)+e;15=I.13}1a Y(19>=15&&19>I.13&&15<=I.13){17=(I.13-15)/(19-15)*(17-e)+e;19=I.13}Y(e!=f){Y(15<=I.12)c=I.12;1a c=I.13;F.1s(1i(f),1g(c));F.1s(1i(e),1g(c))}F.1s(1i(e),1g(15));F.1s(1i(17),1g(19));Y(17!=2R){Y(19<=I.12)c=I.12;1a c=I.13;F.1s(1i(2R),1g(c));F.1s(1i(17),1g(c))}3z=1b.13(17,2R)}F.1s(1i(3z),1g(b));F.1E()}F.2D();F.2C(J.1o,J.1f);F.2B="2g";Z h=g.1U.1j;Z j=g.2h;Y(j>0){F.1j=j/2;F.1O="24(0,0,0,0.1)";32(g.1v,h/2+j/2+F.1j/2);F.1j=j/2;F.1O="24(0,0,0,0.2)";32(g.1v,h/2+F.1j/2)}F.1j=h;F.1O=g.1n;Y(g.1U.1E){F.2j=g.1U.1R!=14?g.1U.1R:1W(g.1n).29(14,14,14,0.4).1V();4I(g.1v,0)}32(g.1v,0);F.2A()}18 4J(d){18 4G(a,b,c){1m(Z i=0;i<a.1c;++i){Z x=a[i][0],y=a[i][1];Y(x<H.12||x>H.13||y<I.12||y>I.13)1p;F.27();F.39(1i(x),1g(y),b,0,2*1b.21,1B);Y(c)F.1E();F.2u()}}18 3w(a,b,c){1m(Z i=0;i<a.1c;++i){Z x=a[i][0],y=a[i][1];Y(x<H.12||x>H.13||y<I.12||y>I.13)1p;F.27();F.39(1i(x),1g(y)+b,c,0,1b.21,1t);F.2u()}}F.2D();F.2C(J.1o,J.1f);Z e=d.1U.1j;Z f=d.2h;Y(f>0){F.1j=f/2;F.1O="24(0,0,0,0.1)";3w(d.1v,f/2+F.1j/2,d.1u.23);F.1j=f/2;F.1O="24(0,0,0,0.2)";3w(d.1v,F.1j/2,d.1u.23)}F.1j=d.1u.1j;F.1O=d.1n;F.2j=d.1u.1R!=14?d.1u.1R:d.1n;4G(d.1v,d.1u.23,d.1u.1E);F.2A()}18 3U(l,m,n){18 4F(a,b,c,d,e,f){Y(a.1c<2)1d;Z g=0;Y(f%2==0)g=(1+(e-f/2-1))*b;1a{Z h=0.5;Y(e==(f/2-h))g=-b*h;1a g=(h+(e-1b.2g(f/2)))*b}1m(Z i=0;i<a.1c;i++){Z x=a[i][0],y=a[i][1];Z j=1B,31=1B,2Z=1B;Z k=x+g,1x=x+g+b,1w=0,1f=y;Y(1x<H.12||k>H.13||1f<I.12||1w>I.13)1p;Y(k<H.12){k=H.12;j=1t}Y(1x>H.13){1x=H.13;2Z=1t}Y(1w<I.12)1w=I.12;Y(1f>I.13){1f=I.13;31=1t}Y(d){F.27();F.1M(1i(k),1g(1w)+c);F.1s(1i(k),1g(1f)+c);F.1s(1i(1x),1g(1f)+c);F.1s(1i(1x),1g(1w)+c);F.1E()}Y(j||2Z||31){F.27();F.1M(1i(k),1g(1w)+c);Y(j)F.1s(1i(k),1g(1f)+c);1a F.1M(1i(k),1g(1f)+c);Y(31)F.1s(1i(1x),1g(1f)+c);1a F.1M(1i(1x),1g(1f)+c);Y(2Z)F.1s(1i(1x),1g(1w)+c);1a F.1M(1i(1x),1g(1w)+c);F.2u()}}}F.2D();F.2C(J.1o,J.1f);F.2B="2g";Z o=l.1r.2b;Z p=1b.12(l.1r.1j,o);F.1j=p;F.1O=l.1n;Y(l.1r.1E){F.2j=l.1r.1R!=14?l.1r.1R:1W(l.1n).29(14,14,14,D.1r.2F).1V()}4F(l.1v,o,0,l.1r.1E,m,n);F.2A()}18 3L(){Y(!D.1D.1z)1d;Z a=[];Z b=1t;1m(i=0;i<C.1c;++i){Y(!C[i].1Q)1p;Y(i%D.1D.4p==0){Y(b)a.20(\'</3v>\');a.20(\'<3v>\');b=1B}Z d=C[i].1Q;Y(D.1D.3k!=14)d=D.1D.3k(d);a.20(\'<2Y 2t="6I"><1y 1J="6H:4C 6G \'+D.1D.3Y+\';6F:4C"><1y 1J="1L:6D;28:6C;3u-1n:\'+C[i].1n+\'"></1y></1y></2Y>\'+\'<2Y 2t="6A">\'+d+\'</2Y>\')}Y(b)a.20(\'</3v>\');Y(a.1c>0){Z e=\'<4y 1J="3B-3o:3m;1n:\'+D.1G.1n+\'">\'+a.3t("")+\'</4y>\';Y(D.1D.3q!=14)D.1D.3q.4N(e);1a{Z f="";Z p=D.1D.1S,m=D.1D.3K;Y(p.2W(0)=="n")f+=\'1f:\'+(m+J.1f)+\'1C;\';1a Y(p.2W(0)=="s")f+=\'1w:\'+(m+J.1w)+\'1C;\';Y(p.2W(1)=="e")f+=\'1x:\'+(m+J.1x)+\'1C;\';1a Y(p.2W(1)=="w")f+=\'1o:\'+(m+J.1w)+\'1C;\';Z g=$(\'<1y 2t="1D" 1J="1S:2e;z-3I:2;\'+f+\'">\'+e+\'</1y>\').2q(G);Y(D.1D.34!=0.0){Z c=D.1D.2a;Y(c==14){Z h;Y(D.1G.2a!=14)h=D.1G.2a;1a h=4w(g);c=1W(h).4u(14,14,14,1).1V()}$(\'<1y 1J="1S:2e;1L:\'+g.1L()+\'1C;28:\'+g.28()+\'1C;\'+f+\'3u-1n:\'+c+\';"> </1y>\').2q(G).3y(\'6z\',D.1D.34)}}}}Z S={1T:14,2c:14};Z T={1k:{x:-1,y:-1},1l:{x:-1,y:-1}};Z U=14;Z V=14;Z W=1t;18 4f(a){Z e=a||3s.6y;Y(e.1T==14&&e.4o!=14){Z c=38.6w,b=38.4n;S.1T=e.4o+(c&&c.4m||b.4m||0);S.2c=e.6v+(c&&c.4k||b.4k||0)}1a{S.1T=e.1T;S.2c=e.2c}}18 4j(e){Y(e.3Q!=1)1d;2G(T.1k,e);Y(V!=14)4i(V);S.1T=14;V=6r(4h,6q);$(38).6o("6m",3S)}18 4a(e){Y(W){W=1t;1d}Z a=$(1X).4b();Z b={};b.x=e.1T-a.1o-J.1o;b.x=H.12+b.x/Q;b.y=e.2c-a.1f-J.1f;b.y=I.13-b.y/R;b.6l=e.1T;b.6k=e.2c;G.49("6j",[b])}18 3p(){Z a,17,15,19;Y(T.1k.x<=T.1l.x){a=T.1k.x;17=T.1l.x}1a{a=T.1l.x;17=T.1k.x}Y(T.1k.y>=T.1l.y){15=T.1k.y;19=T.1l.y}1a{15=T.1l.y;19=T.1k.y}a=H.12+a/Q;17=H.12+17/Q;15=I.13-15/R;19=I.13-19/R;G.49("6i",[{48:a,15:15,17:17,19:19}])}18 3S(e){Y(V!=14){4i(V);V=14}2G(T.1l,e);2f();Y(!3n()||e.3Q!=1)1d 1t;2K();3p();W=1B;1d 1t}18 2G(a,e){Z b=$(1X).4b();Y(D.22.2i=="y"){Y(a==T.1k)a.x=0;1a a.x=O}1a{a.x=e.1T-b.1o-J.1o;a.x=1b.12(1b.13(0,a.x),O)}Y(D.22.2i=="x"){Y(a==T.1k)a.y=0;1a a.y=P}1a{a.y=e.2c-b.1f-J.1f;a.y=1b.12(1b.13(0,a.y),P)}}18 4h(){Y(S.1T==14)1d;2G(T.1l,S);2f();Y(3n())2K()}18 2f(){Y(U==14)1d;Z x=1b.12(U.1k.x,U.1l.x),y=1b.12(U.1k.y,U.1l.y),w=1b.2o(U.1l.x-U.1k.x),h=1b.2o(U.1l.y-U.1k.y);1I.6h(x+J.1o-1I.1j,y+J.1f-1I.1j,w+1I.1j*2,h+1I.1j*2);U=14}18 37(a){2f();Y(D.22.2i=="x"){T.1k.y=0;T.1l.y=P}1a{T.1k.y=(I.13-a.15)*R;T.1l.y=(I.13-a.19)*R}Y(D.22.2i=="y"){T.1k.x=0;T.1l.x=O}1a{T.1k.x=(a.48-H.12)*Q;T.1l.x=(a.17-H.12)*Q}2K();3p()}18 2K(){Y(U!=14&&T.1k.x==U.1k.x&&T.1k.y==U.1k.y&&T.1l.x==U.1l.x&&T.1l.y==U.1l.y)1d;1I.1O=1W(D.22.1n).29(14,14,14,0.8).1V();1I.1j=1;F.2B="2g";1I.2j=1W(D.22.1n).29(14,14,14,0.4).1V();U={1k:{x:T.1k.x,y:T.1k.y},1l:{x:T.1l.x,y:T.1l.y}};Z x=1b.12(T.1k.x,T.1l.x),y=1b.12(T.1k.y,T.1l.y),w=1b.2o(T.1l.x-T.1k.x),h=1b.2o(T.1l.y-T.1k.y);1I.3G(x+J.1o,y+J.1f,w,h);1I.3F(x+J.1o,y+J.1f,w,h)}18 3n(){Z a=5;1d 1b.2o(T.1l.x-T.1k.x)>=a&&1b.2o(T.1l.y-T.1k.y)>=a}}$.4D=18(a,b,c){Z d=1N 3X(a,b,c);1d d};18 1K(r,g,b,a){Z e=[\'r\',\'g\',\'b\',\'a\'];Z x=4;2l(-1<--x){1e[e[x]]=2w[x]||((x==3)?1.0:0)}1e.1V=18(){Y(1e.a>=1.0){1d"3l("+[1e.r,1e.g,1e.b].3t(",")+")"}1a{1d"24("+[1e.r,1e.g,1e.b,1e.a].3t(",")+")"}};1e.29=18(a,b,c,d){x=4;2l(-1<--x){Y(2w[x]!=14)1e[e[x]]*=2w[x]}1d 1e.2L()};1e.4u=18(a,b,c,d){x=4;2l(-1<--x){Y(2w[x]!=14)1e[e[x]]+=2w[x]}1d 1e.2L()};1e.6f=18(){1d 1N 1K(1e.r,1e.b,1e.g,1e.a)};Z f=18(a,b,c){1d 1b.13(1b.12(a,c),b)};1e.2L=18(){1e.r=f(1A(1e.r),0,1h);1e.g=f(1A(1e.g),0,1h);1e.b=f(1A(1e.b),0,1h);1e.a=f(1e.a,0,1);1d 1e};1e.2L()}Z X={6e:[0,1h,1h],6d:[44,1h,1h],6b:[4d,4d,69],68:[0,0,0],67:[0,0,1h],66:[41,42,42],64:[0,1h,1h],63:[0,0,2m],5W:[0,2m,2m],5S:[3i,3i,3i],5R:[0,2v,0],5Q:[5P,5O,40],5N:[2m,0,2m],5M:[2S,40,47],5L:[1h,3Z,0],5T:[5U,50,5I],5H:[2m,0,0],5G:[5Y,5F,5E],61:[62,0,2M],5C:[1h,0,1h],5B:[1h,5A,0],5z:[0,1Y,0],5y:[3J,0,6a],5v:[44,3W,3Z],5u:[5t,5r,3W],6g:[43,1h,1h],5q:[4c,5p,4c],5o:[2M,2M,2M],5n:[1h,5m,5l],5k:[1h,1h,43],6p:[0,1h,0],5i:[1h,0,1h],5h:[1Y,0,0],5g:[0,0,1Y],6t:[1Y,1Y,0],5f:[1h,41,0],5e:[1h,2H,5d],5c:[1Y,0,1Y],5b:[1Y,0,1Y],5a:[1h,0,0],6B:[2H,2H,2H],59:[1h,1h,1h],58:[1h,1h,0]};18 4w(a){Z b,2x=a;45{b=2x.3y("3u-1n").4B();Y(b!=\'\'&&b!=\'3f\')46;2x=2x.54()}2l(!$.53(2x.2U(0),"4n"));Y(b=="24(0, 0, 0, 0)")1d"3f";1d b}18 1W(a){Z b;Y(b=/3l\\(\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*\\)/.2k(a))1d 1N 1K(1A(b[1]),1A(b[2]),1A(b[3]));Y(b=/24\\(\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9]{1,3})\\s*,\\s*([0-9]+(?:\\.[0-9]+)?)\\s*\\)/.2k(a))1d 1N 1K(1A(b[1]),1A(b[2]),1A(b[3]),26(b[4]));Y(b=/3l\\(\\s*([0-9]+(?:\\.[0-9]+)?)\\%\\s*,\\s*([0-9]+(?:\\.[0-9]+)?)\\%\\s*,\\s*([0-9]+(?:\\.[0-9]+)?)\\%\\s*\\)/.2k(a))1d 1N 1K(26(b[1])*2.55,26(b[2])*2.55,26(b[3])*2.55);Y(b=/24\\(\\s*([0-9]+(?:\\.[0-9]+)?)\\%\\s*,\\s*([0-9]+(?:\\.[0-9]+)?)\\%\\s*,\\s*([0-9]+(?:\\.[0-9]+)?)\\%\\s*,\\s*([0-9]+(?:\\.[0-9]+)?)\\s*\\)/.2k(a))1d 1N 1K(26(b[1])*2.55,26(b[2])*2.55,26(b[3])*2.55,26(b[4]));Y(b=/#([a-2p-2s-9]{2})([a-2p-2s-9]{2})([a-2p-2s-9]{2})/.2k(a))1d 1N 1K(1A(b[1],16),1A(b[2],16),1A(b[3],16));Y(b=/#([a-2p-2s-9])([a-2p-2s-9])([a-2p-2s-9])/.2k(a))1d 1N 1K(1A(b[1]+b[1],16),1A(b[2]+b[2],16),1A(b[3]+b[3],16));Z c=2r.6Q(a).4B();Y(c=="3f")1d 1N 1K(1h,1h,1h,0);1a{b=X[c];1d 1N 1K(b[0],b[1],b[2])}}})(2r);',62,425,'||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||if|var|||min|max|null|y1||x2|function|y2|else|Math|length|return|this|top|tVert|255|tHoz|lineWidth|first|second|for|color|left|continue|ticks|bars|lineTo|false|points|data|bottom|right|div|show|parseInt|true|px|legend|fill|tick|grid|datamax|octx|style|Color|width|moveTo|new|strokeStyle|datamin|label|fillColor|position|pageX|lines|toString|parseColor|overlay|128|pies|push|PI|selection|radius|rgba||parseFloat|beginPath|height|scale|backgroundColor|barWidth|pageY||absolute|clearSelection|round|shadowSize|mode|fillStyle|exec|while|139|floor|abs|fA|appendTo|jQuery|F0|class|stroke|100|arguments|elem|xaxis|prevy|restore|lineJoin|translate|save|autoScale|fillOpacity|setSelectionPos|192|tickDecimals|colors|drawSelection|normalize|211|tickSize|max_label|yaxis|autoscaleMargin|x2old|85|tickFormatter|get|canvas|charAt|labelMargin|td|drawRight||drawTop|plotLine|extend|backgroundOpacity|drawLabels|drawGrid|setSelection|document|arc|setSpacing|extendXRangeIfNeededByBar|setTicks|setTickSize|setRange|transparent|defaultTickFormatter|found|169|typeof|labelFormatter|rgb|smaller|selectionIsSane|size|triggerSelectedEvent|container|gridLabel|window|join|background|tr|plotPointShadows|noTicks|css|lastX|fontSize|font|align|text|labelWidth|strokeRect|fillRect|drawSeries|index|75|margin|insertLegend|draw|sin|cos|findDataRanges|which|constructCanvas|onSelectionMouseUp|setBarWidth|drawSeriesBars|parseData|230|Plot|labelBoxBorderColor|140|107|165||224|240|do|break||x1|trigger|onClick|offset|144|245|drawMyBarSeries|onMouseMove|fillInSeriesOptions|updateSelectionOnMouseMove|clearInterval|onMouseDown|scrollTop|0px|scrollLeft|body|clientX|noColumns|preparePieData|getContext|initElement|G_vmlCanvasManager|adjust|clickable|extractColor|browser|table|drawMyPieSeries|parseOptions|toLowerCase|1px|plot|tickColor|plotBars|plotPoints|bindEvents|plotLineArea|drawSeriesPoints|msie|drawSeriesLines|number|append|center|in|borderWidth|9440ed|verticalAlign|textAlign|hidden|overflow|getPlotOffset|getCanvas|4da74d|closePath||ne|ffffff|nodeName|parent||cb4b4b|05|yellow|white|red|violet|purple|203|pink|orange|navy|maroon|magenta|remove|lightyellow|193|182|lightpink|lightgrey|238|lightgreen|216|10000px|173|lightblue|khaki|ccc|afd8f8|indigo|green|215|gold|fuchsia|object|122|150|darksalmon|darkred|204|isFunction|toFixed|darkorange|darkolivegreen|darkmagenta|183|189|darkkhaki|darkgreen|darkgrey|darkorchid|153|pow|darkcyan|LN10|233|02|edc240|darkviolet|148|darkblue|cyan|log|brown|blue|black|220|130|beige|01|azure|aqua|clone|lightcyan|clearRect|selected|plotclick|orgY|orgX|mouseup|click|one|lime|200|setInterval|onmousemove|olive|mousedown|clientY|documentElement|e8cfac|event|opacity|legendLabel|silver|10px|14px|dddddd|padding|solid|border|legendColorBox|dimensions|Invalid|545454|throw|relative|html|ceil|trim'.split('|'),0,{}))
</script>
<script type="text/javascript">
$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_a').click();
});

$(document).ready(function() {
      //alert("document ready occurred!");
     document.getElementById('test_b').click();
});

</script>
<script type="text/javascript">

/* <![CDATA[ */
function calendar(param){
$(document).ready(function() { 

	$(param).datepicker({
		buttonImage: '<?php echo $GLOBAL_START_URL ?>/images/back_office/content/calendar.gif',
		buttonImageOnly: true,
		changeMonth: true,
		changeYear: true,
		dateFormat: 'dd-mm-yy',
		showAnim: 'fadeIn',
		showOn: '<?php echo $displayIcon ? "both" : "focus" ?>'<?php if( $calledFunction ){ ?>,
		onClose: function(){
			<?php echo $calledFunction ?>( this );
		}
		<?php } ?>
	});
	
	
		
});
}
/* ]]> */
</script>