<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Vérification et Confirmation d'une commande
 */
 
include_once('sendmail.inc.php');

/**
 * Confirmation d'une commande
 * utilisé a la suite d'un paiement securisé en general.
 * @param integer $IdOrder Identifiant de la commande
 * @todo à modifier!!! : ne pas passer un numéro de commande en paramètre si $Order est déclarée en global
 */
function ConfirmOrder($IdOrder) 
{
	global 
			$Order,
			$Buyer,
			$GLOBAL_START_PATH;
			$GLOBAL_START_URL;
	
	//verification stock et montant acheteur
	if( VerifyStock( $Order ) )
	{	
			
		//destockage
		/*if(!$Order->Unstock())
		{
			
             echo Dictionnary::translate("stock_not_enough") ; 
			echo "!<br />";
			exit();
		}*/

		$attachments = array( 
			
			"name" => array(), 
			"path" => array(), 
			"mimetype" => array() 
		
		);
		
		if( file_exists( "$GLOBAL_START_PATH/www/cgv.pdf" ) ){
			
			$attachments[ "name" ][] = "cgv.pdf";
			$attachments[ "path" ][] = "$GLOBAL_START_PATH/www/cgv.pdf";
			$attachments[ "mimetype" ][] = "application/pdf";
			
		}
		
		$IdOrder = $Order->get( "idorder" );
		
		$pdfData = array (
		
			"name" => array (),
			"data" => array ()
			
		);
			
			
		if( $Order->get( "status" ) != "ToDo" 
			&& $Order->get( "status" ) != "InProgress" 
			&& $Order->get( "idestimate") != 0 ){
			
			include_once( dirname( __FILE__ ) . "/../objects/odf/ODFOrder.php" );

			$odf = new ODFOrder( $Order->getId() );
			$data = $odf->convert( ODFDocument::$DOCUMENT_PDF );
		
			$pdf_filename = "Commande_" . $Order->getId() . ".pdf";
		
			$pdfData[ "name" ][] = $pdf_filename;
			$pdfData[ "data" ][] = $data;
			
		}

		//-----------------------------------------------------------------------------------------
		//infos client, commercial et admin
		
		$AdminName 	= DBUtil::getParameterAdmin('ad_name');
		$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
		
		$iduser = DBUtil::getParameter('contact_commercial');
	
		$rs =& DBUtil::query( "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
		
		$CommercialName = $rs->fields( "firstname" ).' '.$rs->fields( "lastname" );
		$CommercialEmail = $rs->fields( "email" );
		
		$BuyerName = $Order->getCustomer()->getContact()->get( "lastname" );
		$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );
		$idbuyer=$Order->get( "idbuyer" );
		$rs2 =& DBUtil::query( "SELECT company FROM `buyer` WHERE idbuyer = '$idbuyer' " );
		$BuyerCompany = $rs2->fields["company"];
		//-----------------------------------------------------------------------------------------
				
		//charger le modèle de mail
	
		include_once( "$GLOBAL_START_PATH/objects/MailTemplateEditor.php" );
		
		$editor = new MailTemplateEditor();
		
		$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
		
		$idpaiement = $Order->get('idpayment');	
		switch( $idpaiement ){
			
			case "1" :
				
				$editor->setTemplate( "ORDER_NOTIFICATION_1" );
				break;
			
			case "2" :
				
				$editor->setTemplate( "ORDER_NOTIFICATION_2" );
				break;
			
			case "3" :
				
				$editor->setTemplate( "ORDER_NOTIFICATION_3" );
				break;
			
			default : $editor->setTemplate( "ORDER_NOTIFICATION" );
				
		}
	
		$editor->setUseAdminTags();
		$editor->setUseUtilTags();
		$editor->setUseOrderTags( $Order->get( "idorder" ) );
		$editor->setUseBuyerTags( $Order->get( "idbuyer" ),$Order->get( "idcontact" ) );
		$editor->setUseCommercialTags( $iduser );
		
		$Message = $editor->unTagBody();
		$Subject = Util::doNothing(html_entity_decode(Util::doNothing( $editor->unTagSubject() )));
					
		//-----------------------------------------------------------------------------------------
		//envois du mail
		
		include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
		
		$mailer = new htmlMimeMail();
		
		$mailer->setFrom( '"'.$AdminName.'" <'.$AdminEmail.'>' );
		$mailer->setHtml( $Message );
		$mailer->setSubject( $Subject );
		
		if( DBUtil::getParameterAdmin( "cc_salesman" ) )
			$mailer->setCc( $CommercialEmail );
		
		//$recipients = array( '"'.$BuyerName.'" <'.$BuyerEmail.'>' );
		$recipients = array( $BuyerEmail );
		
		//Attachments
		$i = 0;
		
		while( $i < count( $attachments[ "name" ] ) ){
			
			if( file_exists( $attachments[ "path" ][ $i ] ) ){
				
				ob_start();
				readfile( $attachments[ "path" ][ $i ] );
				$attachment = ob_get_contents();
				ob_end_clean();
				
				$mailer->addAttachment( $attachment, $attachments[ "name" ][ $i ], $attachments[ "mimetype" ][ $i ] );
				
			}
			
			$i++;
			
		}
		
		$i = 0;
		
		while( $i < count( $pdfData[ "name" ] ) ){
			
			$mailer->addAttachment( $pdfData[ "data" ][ $i ], $pdfData[ "name" ][ $i ], "application/pdf" );
			
			$i++;
			
		}
		
		if( $Order->get( "idpayment" ) !=1 ){	
		$result = $mailer->send( $recipients );
	
			if( !$result )
				trigger_error( "Un problème est survenu lors de l'envoi du mail", E_USER_ERROR );

		}

	}
	else
	{	
                echo Dictionnary::translate("stock_not_enough") ; 
				echo "!<br />";
				exit();
				
	}

}

/**
 * Vérification d'une commande
 * si commande non payée
 * @todo à modifier!!! : ne pas passer un numéro de commande en paramètre si $Order est déclarée en global
 * @param integer $IdOrder Identifiant de la commande
 */
function VerifyOrder($IdOrder)
{

	
	
	$msg = ''; 
	global $Order,$GLOBAL_START_PATH;
	
	//verification stock et montant acheteur
	if( !VerifyStock( $Order ) )$msg .= Dictionnary::translate("stock_not_enough") . '<br />' ; 

	//destockage
	/*if(!$Order->Unstock()) $msg = Dictionnary::translate("stock_not_enough") . '<br />' ;*/

	$attachments = array( "name" => array(), "path" => array(), "mimetype" => array() );
	if( file_exists( "$GLOBAL_START_PATH/www/cgv.pdf" ) ){
		
		$attachments[ "name" ][] = "cgv.pdf";
		$attachments[ "path" ][] = "$GLOBAL_START_PATH/www/cgv.pdf";
		$attachments[ "mimetype" ][] = "application/pdf";
		
	}
	
	$pdfData = array (
		
			"name" => array (),
			"data" => array ()
			
		);
	//-----------------------------------------------------------------------------------------
	//infos client, commercial et admin
	
	$AdminName 	= DBUtil::getParameterAdmin('ad_name');
	$AdminEmail = DBUtil::getParameterAdmin('ad_mail');
	
	$iduser = DBUtil::getParameter('contact_commercial');

	$rs =& DBUtil::query( "SELECT firstname, lastname, email FROM `user` WHERE iduser = '$iduser' LIMIT 1" );
		
	$CommercialName = $rs->fields( "firstname" ).' '.$rs->fields( "lastname" );
	$CommercialEmail = $rs->fields( "email" );
		
	$BuyerName = $Order->getCustomer()->getContact()->get( "lastname" );
	$BuyerEmail = $Order->getCustomer()->getContact()->get( "mail" );
	
	//-----------------------------------------------------------------------------------------
			
	//charger le modèle de mail
	
	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( substr( "_1", 1 ) ); //@todo : imposer la langue du destinataire
	
	$idpaiement = $Order->get('idpayment');	
	switch( $idpaiement ){
		
		case "1" :
			
			$editor->setTemplate( "ORDER_NOTIFICATION_1" );
			break;
		
		case "2" :
			
			$editor->setTemplate( "ORDER_NOTIFICATION_2" );
			break;
		
		case "3" :
			
			$editor->setTemplate( "ORDER_NOTIFICATION_3" );
			break;
		
		default : $editor->setTemplate( "ORDER_NOTIFICATION" );
			
	}

	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseOrderTags( $Order->get( "idorder" ) );
	$editor->setUseBuyerTags( $Order->get( "idbuyer" ),$Order->get( "idcontact" ) );
	$editor->setUseCommercialTags( $iduser );
	
	$Message = $editor->unTagBody();
	$Subject = $editor->unTagSubject();
				
	//-----------------------------------------------------------------------------------------
	//envois du mail

	include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
	
	$mailer = new htmlMimeMail();
	
	$mailer->setFrom( '"'.$AdminName.'" <'.$AdminEmail.'>' );
	$mailer->setHtml( $Message );
	$mailer->setSubject( $Subject );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setCc( $CommercialEmail );
	
	//$recipients = array( '"'.$BuyerName.'" <'.$BuyerEmail.'>' );
	$recipients = array( $BuyerEmail );
	
	//Attachments
	$i = 0;
	
	while( $i < count( $attachments[ "name" ] ) ){
		
		if( file_exists( $attachments[ "path" ][ $i ] ) ){
			
			ob_start();
			readfile( $attachments[ "path" ][ $i ] );
			$attachment = ob_get_contents();
			ob_end_clean();
			
			$mailer->addAttachment( $attachment, $attachments[ "name" ][ $i ], $attachments[ "mimetype" ][ $i ] );
			
		}
		
		$i++;
		
	}
	
	$i = 0;
	
	while( $i < count( $pdfData[ "name" ] ) ){
		
		$mailer->addAttachment( $pdfData[ "data" ][ $i ], $pdfData[ "name" ][ $i ], "application/pdf" );
		
		$i++;
		
	}
	
	if( $Order->get( "idpayment" ) !=1 ){
	$result = $mailer->send( $recipients );

	if( !$result )
			trigger_error( "Un problème est survenu lors de l'envoi du mail", E_USER_ERROR );
	}
		
	//-----------------------------------------------------------------------------------------

	return $msg;
}

/**
 * Vérifie le stock pour la commande
 * @todo access private
 */
function VerifyStock( Order $order ){
	
	$Result=1;
	
	$it = $order->getItems()->iterator();
	while( $it->hasNext() && $Result ){
		
		$item =& $it->next();	
		
		$reference = $item->get( "reference" );
		$Qtt = $item->get( "quantity" );
		
		$rs =& DBUtil::query( "select stock_level from detail where reference='$reference'");
	
		if($rs && !$rs->EOF){
			
			$Stock=$rs->Fields('stock_level');
			
			if($Stock >= 0 && ($Stock-$Qtt) < 0 ){
				
				echo "<br />STOCK POUR LA REFERENCE : $reference : $Stock , Qté : $Qtt<br />";
				trigger_error("VerifyStock",E_USER_ERROR);
				
				$Result= 0;

			}
			
		}
		
	}
	
	return $Result;
	
}
	
?>