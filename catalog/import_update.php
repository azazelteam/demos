<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Répartition des produits / Catégories pour un groupement et client
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php"); 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Invoice.php" );
include_once( dirname( __FILE__ ) . "/../objects/GroupingCatalog.php");

header( "Content-Type: text/html; charset=utf-8" );
class GroupingController extends PageController {}
$controller = new GroupingController( "/account/update_import.tpl" );

$GroupingCatalog = new GroupingCatalog();

/* On contrôle si l'utilisateur est connecté.. */
if( Session::getInstance()->getCustomer() ) {
	$controller->setData('isLogin', "ok");
	$controller->setData('logoGroup', $GLOBAL_START_URL."/www/".$GroupingCatalog->getGrouping("grouping_logo"));                                                           
	$idgrouping = $GroupingCatalog->getGrouping( "idgrouping" )? $GroupingCatalog->getGrouping( "idgrouping" ) : 0;
	$idbuyer = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;

 //Liste des import update
	$import = "SELECT update_import.* FROM update_import Join buyer On buyer.idgrouping = update_import.idgrouping WHERE update_import.idgrouping = $idgrouping AND buyer.idbuyer = '$idbuyer' " ; 

	$result = & DBUtil::query( $import );
				
		while( !$result->EOF )
			{
			//print $result->fields("idcategory");	
			$nom_fichier_array = explode("/",$result->fields("file"));
			$nom_fichier = $nom_fichier_array[(count($nom_fichier_array)-1)];
							
				$arraydata = array( $result->fields("idupdate_import"),
				 					$result->fields("date"),
				 					$result->fields("file"),
				 					$result->fields("quantity"),
									$nom_fichier
									);
									
				 $export[] = $arraydata;
				
				$result->MoveNext();
			}	

//print_r($export);exit;	

}



$controller->setData('export', $export);



/* On ferme l'envoi de données */
$controller->output();

?>
