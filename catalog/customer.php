<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Mise à jour coordonnées clients
 */


include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/Customer.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CustomerProxy.php" );


class CustomerController extends PageController{};
$controller = new CustomerController( "customer.tpl" );

/* ------------------------------------------------------------------------------------------------- */

if( !Session::getInstance()->getCustomer() ){

	$controller->output();
	exit();
	
}

/* ------------------------------------------------------------------------------------------------- */

$_POST = Util::arrayMapRecursive( "strip_tags", 	$_POST );
$_POST = Util::arrayMapRecursive( "stripslahes", 	$_POST );

updateCustomer();

$controller->output();

/* ------------------------------------------------------------------------------------------------- */

function updateCustomer(){
	
	$customer =& Session::getInstance()->getCustomer();
	
	if( isset( $_POST[ "company" ] ) ) 				$customer->set( "company", $_POST[ "company" ] );
	if( isset( $_POST[ "nomCommercial" ] ) ) 		$customer->set( "nomCommercial", $_POST[ "nomCommercial" ] );
	if( isset( $_POST[ "kbis" ] ) ) 				$customer->set( "kbis", $_POST[ "kbis" ] );
	if( isset( $_POST[ "naf" ] ) ) 					$customer->set( "naf", $_POST[ "naf" ] );
	if( isset( $_POST[ "siren" ] ) ) 				$customer->set( "siren", $_POST[ "siren" ] );
	if( isset( $_POST[ "siret" ] ) ) 				$customer->set( "siret", $_POST[ "siret" ] );
	if( isset( $_POST[ "phone_standard" ] ) ) 		$customer->set( "phone_standard", Util::phonenumberFormat( $_POST[ "phone_standard" ] ) );
	if( isset( $_POST[ "fax_standard" ] ) ) 		$customer->set( "fax_standard", Util::phonenumberFormat( $_POST[ "fax_standard" ] ) );
	if( isset( $_POST[ "www" ] ) ) 					$customer->set( "www", $_POST[ "www" ] );
	if( isset( $_POST[ "company_mail" ] ) ) 		$customer->set( "company_mail", $_POST[ "company_mail" ] );
	if( isset( $_POST[ "domiciliation" ] ) ) 		$customer->set( "domiciliation", $_POST[ "domiciliation" ] );
	if( isset( $_POST[ "code_etabl" ] ) ) 			$customer->set( "code_etabl", $_POST[ "code_etabl" ] );
	if( isset( $_POST[ "code_guichet" ] ) ) 		$customer->set( "code_guichet", $_POST[ "code_guichet" ] );
	if( isset( $_POST[ "n_compte" ] ) ) 			$customer->set( "n_compte", $_POST[ "n_compte" ] );
	if( isset( $_POST[ "cle_rib" ] ) ) 				$customer->set( "cle_rib", $_POST[ "cle_rib" ] );

	if( isset( $_POST[ "idmanpower" ] ) ) 			$customer->set( "idmanpower", intval( $_POST[ "idmanpower" ] ) );
	if( isset( $_POST[ "idactivity" ] ) ) 			$customer->set( "idactivity", intval( $_POST[ "idactivity" ] ) );
	if( isset( $_POST[ "idstate" ] ) ) 				$customer->set( "idstate", intval( $_POST[ "idstate" ] ) );
	if( isset( $_POST[ "deliv_payment" ] ) ) 		$customer->set( "deliv_payment", intval( $_POST[ "deliv_payment" ] ) );
	if( isset( $_POST[ "cond_payment" ] ) ) 		$customer->set( "cond_payment", intval( $_POST[ "cond_payment" ] ) );
	if( isset( $_POST[ "idcustomer_profile" ] ) ) 	$customer->set( "idcustomer_profile", intval( $_POST[ "idcustomer_profile" ] ) );
	
	if( isset( $_POST[ "buyer_info1" ] ) ) 			$customer->set( "buyer_info1", intval( $_POST[ "buyer_info1" ] ) );
	if( isset( $_POST[ "buyer_info2" ] ) ) 			$customer->set( "buyer_info2", intval( $_POST[ "buyer_info2" ] ) );
	if( isset( $_POST[ "buyer_info3" ] ) ) 			$customer->set( "buyer_info3", intval( $_POST[ "buyer_info3" ] ) );
	if( isset( $_POST[ "buyer_info4" ] ) ) 			$customer->set( "buyer_info4", intval( $_POST[ "buyer_info4" ] ) );
	if( isset( $_POST[ "buyer_info5" ] ) ) 			$customer->set( "buyer_info5", intval( $_POST[ "buyer_info5" ] ) );
	if( isset( $_POST[ "buyer_info6" ] ) ) 			$customer->set( "buyer_info6", intval( $_POST[ "buyer_info6" ] ) );
	if( isset( $_POST[ "buyer_info7" ] ) ) 			$customer->set( "buyer_info7", intval( $_POST[ "buyer_info7" ] ) );
	if( isset( $_POST[ "buyer_info8" ] ) ) 			$customer->set( "buyer_info8", intval( $_POST[ "buyer_info8" ] ) );
	if( isset( $_POST[ "buyer_info9" ] ) ) 			$customer->set( "buyer_info9", intval( $_POST[ "buyer_info9" ] ) );
	if( isset( $_POST[ "buyer_info10" ] ) ) 		$customer->set( "buyer_info10", intval( $_POST[ "buyer_info10" ] ) );
	
	if( isset( $_POST[ "title" ] ) ) 				$customer->getContact()->set( "title", intval( $_POST[ "title" ] ) );
	if( isset( $_POST[ "lastname" ] ) ) 			$customer->getContact()->set( "lastname", $_POST[ "lastname" ] );
	if( isset( $_POST[ "firstname" ] ) ) 			$customer->getContact()->set( "firstname", $_POST[ "firstname" ] );
	if( isset( $_POST[ "iddepartment" ] ) ) 		$customer->getContact()->set( "iddepartment", intval( $_POST[ "iddepartment" ] ) );
	if( isset( $_POST[ "idfunction" ] ) ) 			$customer->getContact()->set( "idfunction", intval( $_POST[ "idfunction" ] ) );
	if( isset( $_POST[ "faxnumber" ] ) ) 			$customer->getContact()->set( "faxnumber", Util::phonenumberFormat( $_POST[ "faxnumber" ] ) );
	if( isset( $_POST[ "phonenumber" ] ) ) 			$customer->getContact()->set( "phonenumber", Util::phonenumberFormat( $_POST[ "phonenumber" ] ) );
	if( isset( $_POST[ "mail" ] ) ) 				$customer->getContact()->set( "mail", $_POST[ "mail" ] );
	if( isset( $_POST[ "comment" ] ) ) 				$customer->getContact()->set( "comment", $_POST[ "comment" ] );
	if( isset( $_POST[ "gsm" ] ) ) 					$customer->getContact()->set( "gsm", Util::phonenumberFormat( $_POST[ "gsm" ] ) );
	
	$customer->save();

}

/* ------------------------------------------------------------------------------------------------- */

?>