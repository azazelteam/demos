<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage des nouveaux produits
 */

include_once( dirname( __FILE__ ) ."/../objects/classes.php" );
include_once( "$GLOBAL_START_PATH/objects/catalnoveltyobject.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );

//$lang = Session::getInstance()->getLang();

class IndexController extends PageController{};
$novelty_template = new IndexController( "product_novelty.tpl" , "Nouveautés" );

$ScriptName = "product_novelty.php";

$IdCategory = 0;

if( isset( $_GET[ "IdCategory" ] ) )
	$IdCategory = $_GET[ "IdCategory" ];
elseif( isset( $_POST[ "IdCategory" ] ) )
	$IdCategory = $_POST[ "IdCategory" ];

$Category = new CATALNOVELTY( $IdCategory );
//Session::getInstance()->statistique( "view", $Category );

$NbAllProducts = $Category->GetNBProduct();

if( $NbAllProducts > 0 ){
	
	$NbProductsByPages = 5;
	$NbColumnProducts = 1;
	
	$LimiteMaxC = $Category->CalculateLimiteProdToDisplay( $NbProductsByPages, $CurrentPageNumberP );
	
	// Définition des paramètres à passer dans les liens vers les pages suivantes
	$ParameterOutputArray[ "FromScript" ]			= $ScriptName;
	$ParameterOutputArray[ "CurrentPageNumberP" ]	= $CurrentPageNumberP;
	$ParameterOutputArray[ "CurrentPageNumberC" ]	= $CurrentPageNumberC;
	$ParameterOutputArray[ "LimiteMaxP" ]			= $LimiteMaxC;
	$ParameterOutputArray[ "nb_products_by_pages" ]	= $NbProductsByPages;
	$ParameterOutputArray[ "nb_column_products" ]	= $NbColumnProducts;
		
	if( isset( $Category ) && !isset( $_REQUEST[ "SSE" ] ) && !isset( $_REQUEST[ "SSE2Form" ] ) )
		$nmore = $Category->MoreProdPages();
	else
		$nmore = ceil( $NbAllProducts / $NbProductsByPages ) - $CurrentPageNumberP ;
	
	//Plus d'une page à afficher
	if( $NbAllProducts > $NbProductsByPages ){
		
		$novelty_template->setData( "moreThanOnePage", 1 );
		
		$ParameterOutputArray[ "CurrentPageNumberP" ] = $CurrentPageNumberP;
		
		//Page autre que la première
		if( $CurrentPageNumberP > 1 ){
			
			$novelty_template->setData( "notFirstPage", 1 );
			
			$ParameterOutputArray[ "CurrentPageNumberP" ] = $CurrentPageNumberP - 1;
			
			$novelty_template->setData( "parameterArray1", $ParameterOutputArray );

			$ParameterOutputArray[ "CurrentPageNumberP" ] = $CurrentPageNumberP;
			
		}else{
			
			$novelty_template->setData( "notFirstPage", "" );
			
		}
		
		$pagination = array();
		
		for( $i = 1 ; $i <= ceil( $NbAllProducts / $NbProductsByPages ) ; $i++ ){
			
			$pagination[ $i ][ "offset" ] = $i;
			
			if( $i > 1 )
				$pagination[ $i ][ "notFirstOffset" ] = 1;
			else
				$pagination[ $i ][ "notFirstOffset" ] = "";
	
			if( $i ==  $CurrentPageNumberP ){
				
				$pagination[ $i ][ "currentPage" ] = 1;
				
			}else{
				
				$pagination[ $i ][ "currentPage" ] = "";
				
				$pagination[ $i ][ "urlArgs" ] = "";
				
				foreach( $ParameterOutputArray as $ParamName => $ParamValue ){
				
					if( $ParamName == "CurrentPageNumberP" || $ParamName == "CurrentPageNumberC" )
						$ParamValue =  $i ;
					
					$pagination[ $i ][ "urlArgs" ] .=  "$ParamName=$ParamValue&";
					
				}
				
				$pagination[ $i ][ "urlArgs" ] = substr( $pagination[ $i ][ "urlArgs" ], 0, strlen( $pagination[ $i ][ "urlArgs" ] ) - 1 );
				
			}
			
		}
		
		$novelty_template->setData( "pagination", $pagination );
		
		if( $nmore ) {
			
			$novelty_template->setData( "nmore", 1 );
			
			$ParameterOutputArray[ "CurrentPageNumberP" ] = $CurrentPageNumberP + 1;
			
			$novelty_template->setData( "parameterArray2", $ParameterOutputArray );
			
			$ParameterOutputArray[ "CurrentPageNumberP" ] = $CurrentPageNumberP;
			
		}else{
			
			$novelty_template->setData( "nmore", "" );
			
		}
		
	}else{
		
		$novelty_template->setData( "moreThanOnePage", "" );
		
	}
	
	$IdMaskProduct = "_11";
	$IdMaskDetail = "_1";
	$ParameterArray = $ParameterOutputArray;
	
}

$novelty_template->setData( "parameterArray", $ParameterOutputArray );
$novelty_template->setData( "nbAllProducts", $NbAllProducts );
$novelty_template->setData( "scriptName", $ScriptName );


$articles = [];
include_once( dirname( __FILE__ ) . "/../objects/DBUtil.php" );
gc_enable ();
foreach ($Category->GetAllProduct() as $key => $value) {
	if(intval($value['idproduct']) > 0){
		$product = $Category->GetProduct2(intval($value['idproduct']));
		$article = $product->getArticles()->iterator();
		if ($article->hasNext()) {
			$a = $article->next();
			

			$articles[$key]["getURL"] 				= $product->getURL();
			$articles[$key]["imageSrc"] 			= $a->getImageURI("150");
			$articles[$key]["getReference"] 		= $a->getReference();
			$articles[$key]["getPriceET"] 			= Util::priceFormat($a->getPriceET());
			$articles[$key]["name"] 				= $a->GetProductName();
			$articles[$key]["getReference"] 		= $a->getReference();
			$articles[$key]["getPriceATI"] 			= Util::priceFormat($a->getPriceATI());
			$articles[$key]["getPromotionCount"] 	= $a->getDiscountRate() > 0;
			$articles[$key]["getPromotionRate"] 	= $a->getDiscountRate();
			$articles[$key]["idArticle"] 				= $a->getId();
		}



		unset($product);

		unset($article);
	}
}
$novelty_template->setData("Articles", $articles);
$novelty_template->setData("Category",$Category);

$novelty_template->GetPromotions();
$novelty_template->GetCategoryLinks();
$novelty_template->GetBrands();
$novelty_template->GetTrades();
$novelty_template->GetProductHistory();

$novelty_template->output();
?>