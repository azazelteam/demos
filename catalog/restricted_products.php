<?php

/**
 * Récupère les produits à accès limités sous la forme suivante :
 * 
 * 		array(
 * 
 * 			array(
 * 
 * 				"idrestricted_product" 	=> 1,
 * 				"name" 					=> "name_1",
 * 				"image" 				=> "image_1.jpg",
 * 				"plan" 					=> "plan_1.jpg",
 * 				"sheet" 				=> "sheet_1.jpg"
 * 
 * 			),
 * 
 * 			array(
 * 
 * 				"idrestricted_product" 	=> 2,
 * 				"name" 					=> "name_2",
 * 				"image" 				=> "image_2.jpg",
 * 				"plan" 					=> "plan_3.jpg",
 * 				"sheet" 				=> "sheet_4.jpg"
 * 
 * 			), ...
 * 
 * 		)
 * 
 * puis inclut le template $GLBOAL_START_PATH/templates/catalog/restricted_products.htm.php
 */
 
include_once( "../objects/classes.php" );

//------------------------------------------------------------------------

$idbuyer = Session::getInstance()->getIdBuyer();
$useRestrictions = isset( $_GET[ "rexel" ] ) && $_GET[ "rexel" ] == md5( "rexel" ) ? false : true;

if( $idbuyer || !$useRestrictions ){
	
	//@todo revoir la structure des pages, c'est le boxon
	
	$restrictedProducts =& getRestrictedProducts( $idbuyer, $useRestrictions );
	
	include( "$GLOBAL_START_PATH/www/head.htm.php" );
	include( "$GLOBAL_START_PATH/www/barre_g.php" );
	
	?>
	<div id="centre">
	<?php
	
	include_once( "$GLOBAL_START_PATH/catalog/include/restricted_products.htm.php" );
	include("$GLOBAL_START_PATH/www/foot.htm.php");

	?>
	</div><!-- centre -->
	</div><!-- MainDiv -->
	<?php include( "$GLOBAL_START_PATH/js/xiti.php" ); ?>
	<?php include( "$GLOBAL_START_PATH/js/google_analytics.php" ); ?>
	</body>
	</html>
	<?php
	
}
else header( "Location:$GLOBAL_START_URL" );

//------------------------------------------------------------------------

function &getRestrictedProducts( $idbuyer, $useRestrictions = true ){
	
	$rs =& DBUtil::query( "SELECT DISTINCT( restriction_fieldname ) FROM restricted_products" );
	
	$restrictedProducts = array();
	
	while( !$rs->EOF() ){
		
		$restriction_fieldname = $rs->fields( "restriction_fieldname" );
		$value = DBUtil::getDBValue( $restriction_fieldname, "buyer", "idbuyer", $idbuyer );
		
		if( !$useRestrictions )
			$query = "SELECT * FROM restricted_products WHERE 1 ORDER BY name ASC";
		else {
			
			$query = "
			SELECT *
			FROM restricted_products 
			WHERE restriction_fieldname LIKE '" . Util::html_escape( $restriction_fieldname ) . "' 
			AND restriction_value = '" . Util::html_escape( $value ) . "' 
			ORDER BY name ASC";
		
		}
		
		$rs2 =& DBUtil::query( $query );
		
		while( !$rs2->EOF() ){
		
			$restrictedProducts[] = array(
			
				"idrestricted_product" 	=> $rs2->fields( "idrestricted_product" ),
				"name" 					=> $rs2->fields( "name" ),
				"image" 				=> $rs2->fields( "image" ),
				"plan" 					=> $rs2->fields( "plan" ),
				"sheet" 				=> $rs2->fields( "sheet" )
				
			);
				
			$rs2->MoveNext();
			
		}
		
		$rs->MoveNext();
		
	}
	
	return $restrictedProducts;
	
}

//------------------------------------------------------------------------

?>