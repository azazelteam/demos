<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Moteur de recherche
 */

/* ------------------------------------------------------------------------------------------- */

define( "USE_SOUNDEX", 						false );

define( "PRODUCT_NAME_RELEVANCE", 			15.0 );
define( "REFERENCE_DESIGNATION_RELEVANCE", 	5.0 );
define( "PRODUCT_DESCRIPTION_RELEVANCE", 	0.1 );

define( "CATEGORY_NAME_RELEVANCE", 			15.0 );
define( "CATEGORY_DESCRIPTION_RELEVANCE", 	5.0 );

/* ------------------------------------------------------------------------------------------- */
/* n° de page */

/*if( isset( $_REQUEST[ "search_product" ] ) && preg_match( "/^[0-9]+\$/", trim( $_REQUEST[ "search_product" ] ) ) ){
	
	header( "Location: /?pageNumber=" . intval( trim( $_REQUEST[ "search_product" ] ) ) );
	exit();
	
}*/

/* ------------------------------------------------------------------------------------------- */

//require_once "PEAR.php";
include_once( dirname( __FILE__ ) . "/../config/init.php" );

include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CCategoryProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/CProductProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/Dictionnary.php" );
/* ------------------------------------------------------------------------------------------- */

class SearchController extends PageController{

	/* ------------------------------------------------------------------------------------------- */
	
	public function searchProducts( $string, $resultPerPage = 20, $page = 1 ){
		
		$results = searchProducts( $string, $resultPerPage, $page );
		

		$this->setData( "productCount", $results[ 0 ] );
		$this->setData( "products", 	count( $results[ 1 ] ) ? $results[ 1 ] : NULL );
		$this->setData( "page", 		$page );
		$this->setData( "pageCount", 	ceil( $results[ 0 ] / $resultPerPage ) );

	}
	public function searchProducts2( $resultPerPage = 20, $page = 1 ){
		
		$results = searchProducts2(  $resultPerPage, $page );

		$this->setData( "productCount", $results[ 0 ] );
		$this->setData( "products", 	count( $results[ 1 ] ) ? $results[ 1 ] : NULL );
		$this->setData( "page", 		$page );
		$this->setData( "pageCount", 	ceil( $results[ 0 ] / $resultPerPage ) );

	}
	
	/* ------------------------------------------------------------------------------------------- */
	
	public function searchCategories( $string, $resultPerPage = 20, $page = 1 ){
		
		$categories = searchCategories( $string, $resultPerPage, $page );

		$this->setData( "categories", 		count( $categories ) ? $categories : NULL );
		$this->setData( "categoryCount", 	count( $categories ) );
		$this->setData( "page", 		$page );
		$this->setData( "search_value", 		$_REQUEST[ "search_product" ] );
	}
	
	/* ------------------------------------------------------------------------------------------- */
	
};

/* ------------------------------------------------------------------------------------------- */
 
$searchController = new SearchController( "search_page.tpl" , "Moteur de recherche" );

$searchController->createElement( "search", isset( $_REQUEST[ "search_product" ] ) ? stripslashes( $_REQUEST[ "search_product" ] ) : false );

$searchController->setData( "title", Dictionnary::translate("search_title"));
$searchController->setData( "meta_description",  Dictionnary::translate("search_description") );
//recherche

if( isset( $_REQUEST[ "search_product" ] ) ){
	
	//recherche produits
	if(!empty($_POST['search-marque'])||(!empty($_POST['search-metier'])))
	{
	$searchController->searchProducts2(  
		isset( $_REQUEST[ "resultPerPage" ] ) ? intval( $_REQUEST[ "resultPerPage" ] ) : 20, 
		isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 
	);
}
else 
{
$searchController->searchProducts( 
		trim( strip_tags( stripslashes( $_REQUEST[ "search_product" ] ) ) ), 
		isset( $_REQUEST[ "resultPerPage" ] ) ? intval( $_REQUEST[ "resultPerPage" ] ) : 20, 
		isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 
	);


}
	//recherche catégories
	
	$searchController->searchCategories( trim( strip_tags( stripslashes( $_REQUEST[ "search_product" ] ) ) ), 
		isset( $_REQUEST[ "resultPerPage" ] ) ? intval( $_REQUEST[ "resultPerPage" ] ) : 20, 
		isset( $_REQUEST[ "page" ] ) ? intval( $_REQUEST[ "page" ] ) : 1 

	 );
	
	//stats
		
	updateSearchStats( trim( strip_tags( stripslashes( $_REQUEST[ "search_product" ] ) ) ) );
	
}

$searchController->output();

/* ------------------------------------------------------------------------------------------- */
/**
 * @param string $string
 * @param int $resultPerPage
 * @param int $page
 * @param string $type
 * @return array<CProductProxy>
 */
function searchProducts( $string, $resultPerPage, $page, $type = "AND" ){
$lang=User::getInstance()->getActiveLang();
	/* type de recherche */

	switch( $type ){
		
		case "EXACT":
		
			$logicOperator = "AND";
			$exactSearch = true;
			break;
			
		case "OR" :
		
			$logicOperator = "OR";
			$exactSearch = false;
			break;

		case "AND" :
		default :
			$logicOperator = "AND";
			$exactSearch = false;
			break;
			
	}

	/* mots clés */
	
	$string = trim( $string );
	
	if( !strlen( $string ) )
		return array();
		
	$token = strtok( $string, " " );
	$keywords = array();
	
	while( $token !== false ){
		
		$len = strlen( $token );
		
		//recherche des singuliers à partir des pluriels :o/
		
		if( $len > 2 && substr( $token, $len - 1, 1 ) == "s" )
			$keywords[] = substr( $token, 0, $len - 1 );
		else $keywords[] = $token;
		
		$token = strtok(  " " );
		
	}

	/* pertinence */
	
	$relevancePattern = "";
	$relevances = array( 
	
		"p.name$lang" 			=> PRODUCT_NAME_RELEVANCE,
		"d.designation$lang" 	=> REFERENCE_DESIGNATION_RELEVANCE,
		//"p.description_1" 	=> PRODUCT_DESCRIPTION_RELEVANCE
		
	);

	$j = 0;
	foreach( $relevances as $fieldname => $relevance ){
		
		if( $j )
			$relevancePattern .= " + ";
		
		$p = 0;
		foreach( $keywords as $keyword ){
			
			if( $p )
				$relevancePattern .= " + ";
			
			$relevancePattern .= "$relevance * ( MATCH( $fieldname ) AGAINST ( '*" . addslashes( $keyword ) . "*' IN BOOLEAN MODE ) ) * LEAST( 1 / ( LOCATE( " . DBUtil::quote( $keyword ) . ", $fieldname ) - 0.9999999999 ), 1.0 * $relevance )";

			$p++;
			
		}
	
		$j++;
		
	}

	$searchStr = "";

	//expression exacte
	
	if( $exactSearch )
		$where = "( p.name$lang LIKE '%" . addslashes( $string ) . "%' OR d.designation$lang LIKE '%" . addslashes( $string ) . "%' )";
	else{
	
		//expression complète dans le nom ou la désignation en premier pour la pertinence
		
		$where = "( ( p.name$lang LIKE '%" . addslashes( $string ) . "%' OR d.designation$lang LIKE '%" . addslashes( $string ) . "%'";
		
		if( USE_SOUNDEX )
			$where .= " OR SOUNDEX( p.name$lang ) LIKE SOUNDEX( '%" . addslashes( $string ) . "%' ) OR SOUNDEX( d.designation$lang ) LIKE SOUNDEX( '%" . addslashes( $string ) . "%' )";
		
		$where .= " )";

			
		$where .= " OR (";
		
		$p = 0;
		foreach( $keywords as $keyword ){
		
			if( USE_SOUNDEX ){
				
				$query 		= "SELECT SOUNDEX( " . DBUtil::quote( $keyword ) . " ) AS soundex";
				$soundex 	= DBUtil::query( $query )->fields( "soundex" );
				
			}
			
			if( $p )
				$where .= " $logicOperator";
				
			$where .= " ( p.name$lang LIKE '%" . addslashes( $keyword ) . "%' OR d.designation$lang LIKE '%" . addslashes( $keyword ) . "%'";
			
			if( USE_SOUNDEX && !empty( $soundex ) )
				$where .= " OR SOUNDEX( p.name$lang ) LIKE '%" . addslashes( $soundex ) . "%' OR SOUNDEX( d.designation$lang ) LIKE '%" . addslashes( $soundex ) . "%'";

			$where .= " )";
	
			$p++;

		}

		$where .= ")";
		$where .= ")";
	
	}
	
	/* recherche par référence */

	$where .= " OR d.reference LIKE " . DBUtil::quote( $string . "%" );

	//recherche
	
	$start 			= ( $page - 1 ) * $resultPerPage;
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;


	$query = "
	SELECT SQL_CALC_FOUND_ROWS
		p.idproduct,
		p.idcategory,
		p.name$lang AS name,
		d.designation$lang AS designation,
		COUNT( d.idarticle ) AS referenceCount,
		( $relevancePattern ) AS relevance 
	FROM detail d, product p, category c
	LEFT JOIN category_link cl ON cl.idcategorychild = c.idcategory
	WHERE ( $where ) 
	AND d.stock_level <> 0
	AND d.type = 'catalog'
	AND p.idproduct = d.idproduct 
	AND p.idproduct <> 0
	AND p.catalog_right <= '$catalog_right' 
	AND p.available = 1
	AND c.idcategory = p.idcategory
	AND c.catalog_right <= '$catalog_right'
	AND c.available = 1
	GROUP BY d.idproduct
	ORDER BY relevance DESC, 
		c.star_order DESC, 
		cl.displayorder ASC, 
		p.display_product_order ASC,
		MIN( d.sellingcost ) ASC
	LIMIT $start, $resultPerPage";

	$rs = DBUtil::query( $query );

	if( $rs === false )
		 trigger_error( "Impossible d'éffectuer la recherche", E_USER_ERROR );

	$resultCount = DBUtil::query( "SELECT FOUND_ROWS() AS `count`" )->fields( "count" );
	
	$results = array();
	
	while( !$rs->EOF() ){

		array_push( $results, new CProductProxy( new CProduct( $rs->fields( "idproduct" ) ) ) );
		
		$rs->MoveNext();
		
	}

	return array( $resultCount, $results );

}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function searchProducts2(  $resultPerPage, $page){

	/* type de recherche */

	

	/* mots clés */
$lang=User::getInstance()->getActiveLang();
$start 			= ( $page - 1 ) * $resultPerPage;
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
	
	
	 if (!empty($_POST['search-marque']) && !empty($_POST['search-metier']))
{
$idtrade = $_POST['search-metier'];
$idbrand = $_POST['search-marque'];

$query = "
	SELECT SQL_CALC_FOUND_ROWS
		p.idproduct,
		p.idcategory,
		p.name$lang AS name,
		d.designation$lang AS designation,
		COUNT( d.idarticle ) AS referenceCount

	FROM trade_product t, detail d, product p, category c
	LEFT JOIN category_link cl ON cl.idcategorychild = c.idcategory
	WHERE 
	d.stock_level <> 0
	AND d.type = 'catalog'
	AND p.idproduct = d.idproduct 
	AND p.idproduct <> 0
	AND p.catalog_right <= '$catalog_right' 
	AND p.available = 1
	AND c.idcategory = p.idcategory
	AND c.catalog_right <= '$catalog_right'
	AND c.available = 1
	AND t.idtrade = '$idtrade'
	AND t.idproduct = p.idproduct
	AND p.idbrand = '$idbrand'
	GROUP BY d.idproduct
	ORDER BY 
		c.star_order DESC, 
		cl.displayorder ASC, 
		p.display_product_order ASC,
		MIN( d.sellingcost ) ASC
	LIMIT $start, $resultPerPage";
	$rs = DBUtil::query( $query );
if( $rs === false )
		 trigger_error( "Impossible d'éffectuer la recherche", E_USER_ERROR );

	$resultCount = DBUtil::query( "SELECT FOUND_ROWS() AS `count`" )->fields( "count" );
	
	$results = array();
	
	while( !$rs->EOF() ){

		array_push( $results, new CProductProxy( new CProduct( $rs->fields( "idproduct" ) ) ) );
		
		$rs->MoveNext();
		
	}
}


	
	
	
	
else if (!empty($_POST['search-marque']))
{
$idbrand = $_POST['search-marque'];

$query = "
	SELECT SQL_CALC_FOUND_ROWS
		p.idproduct,
		p.idcategory,
		p.name$lang AS name,
		d.designation$lang AS designation,
		COUNT( d.idarticle ) AS referenceCount
		
	FROM detail d, product p, category c
	LEFT JOIN category_link cl ON cl.idcategorychild = c.idcategory
	WHERE  d.stock_level <> 0
	AND d.type = 'catalog'
	AND p.idproduct = d.idproduct 
	AND p.idproduct <> 0
	AND p.catalog_right <= '$catalog_right' 
	AND p.available = 1
	AND p.idbrand = '$idbrand'
	AND c.idcategory = p.idcategory
	AND c.catalog_right <= '$catalog_right'
	AND c.available = 1
	GROUP BY d.idproduct
	ORDER BY 
		c.star_order DESC, 
		cl.displayorder ASC, 
		p.display_product_order ASC,
		MIN( d.sellingcost ) ASC
	LIMIT $start, $resultPerPage";
	$rs = DBUtil::query( $query );
if( $rs === false )
		 trigger_error( "Impossible d'éffectuer la recherche", E_USER_ERROR );

	$resultCount = DBUtil::query( "SELECT FOUND_ROWS() AS `count`" )->fields( "count" );
	
	$results = array();
	
	while( !$rs->EOF() ){

		array_push( $results, new CProductProxy( new CProduct( $rs->fields( "idproduct" ) ) ) );
		
		$rs->MoveNext();
		
	}
}

else if (!empty($_POST['search-metier']))
{
$idtrade = $_POST['search-metier'];

$query = "
	SELECT SQL_CALC_FOUND_ROWS
		p.idproduct,
		p.idcategory,
		p.name$lang AS name,
		d.designation$lang AS designation,
		COUNT( d.idarticle ) AS referenceCount

	FROM trade_product t, detail d, product p, category c
	LEFT JOIN category_link cl ON cl.idcategorychild = c.idcategory
	WHERE  d.stock_level <> 0
	AND d.type = 'catalog'
	AND p.idproduct = d.idproduct 
	AND p.idproduct <> 0
	AND p.catalog_right <= '$catalog_right' 
	AND p.available = 1
	AND c.idcategory = p.idcategory
	AND c.catalog_right <= '$catalog_right'
	AND c.available = 1
	AND t.idtrade = '$idtrade'
	AND t.idproduct = p.idproduct
	
	GROUP BY d.idproduct
	ORDER BY 
		c.star_order DESC, 
		cl.displayorder ASC, 
		p.display_product_order ASC,
		MIN( d.sellingcost ) ASC
	LIMIT $start, $resultPerPage";
	$rs = DBUtil::query( $query );
	if( $rs === false )
		 trigger_error( "Impossible d'éffectuer la recherche", E_USER_ERROR );

	$resultCount = DBUtil::query( "SELECT FOUND_ROWS() AS `count`" )->fields( "count" );
	
	$results = array();
	
	while( !$rs->EOF() ){

		array_push( $results, new CProductProxy( new CProduct( $rs->fields( "idproduct" ) ) ) );
		
		$rs->MoveNext();
		
	}

}




	return array( $resultCount, $results );

}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




/* ------------------------------------------------------------------------------------------- */
/**
 * @param string $string
 * @param int $resultPerPage
 * @return array<CCategoryProxy>
 */
function searchCategories( $string, $resultPerPage, $page ){

	/* mots clés */
	$lang=User::getInstance()->getActiveLang();
	$string = trim( $string );
	
	if( !strlen( $string ) )
		return array();
		
	$token = strtok( $string, " " );
	$keywords = array();
	
	while( $token !== false ){
		
		$len = strlen( $token );
		
		//recherche des singuliers à partir des pluriels :o/
		
		if( $len > 2 && substr( $token, $len - 1, 1 ) == "s" )
			$keywords[] = substr( $token, 0, $len - 1 );
		else $keywords[] = $token;
		
		$token = strtok(  " " );
		
	}

	/* pertinence */
	
	$relevancePattern = "";
	$relevances = array( 
	
		"c.name$lang" 			=> CATEGORY_NAME_RELEVANCE,
		//"c.description_1" 	=> CATEGORY_DESCRIPTION_RELEVANCE,
		
	);

	$j = 0;
	foreach( $relevances as $fieldname => $relevance ){
		
		if( $j )
			$relevancePattern .= " + ";
		
		$p = 0;
		foreach( $keywords as $keyword ){
			
			if( $p )
				$relevancePattern .= " + ";
			
			$relevancePattern .= "$relevance * ( MATCH( $fieldname ) AGAINST ( '*" . addslashes( $keyword ) . "*' IN BOOLEAN MODE ) ) * LEAST( 1 / ( LOCATE( " . DBUtil::quote( $keyword ) . ", $fieldname ) - 0.9999999999 ), 1.0 * $relevance )";

			$p++;
			
		}
	
		$j++;
		
	}

	$searchStr = "";

	//expression complète dans le nom ou la désignation en premier pour la pertinence
	
	$where = "( ( c.name$lang LIKE '%" . addslashes( $string ) . "%'";
	
	if( USE_SOUNDEX )
		$where .= " OR ( SOUNDEX( c.name$lang ) LIKE SOUNDEX( '%" . addslashes( $string ) . "%' ) )";
	
	$where .= " )";

	$where .= " OR (";
	
	$p = 0;
	foreach( $keywords as $keyword ){
	
		if( USE_SOUNDEX ){
			
			$query 		= "SELECT SOUNDEX( " . DBUtil::quote( $keyword ) . " ) AS soundex";
			$soundex 	= DBUtil::query( $query )->fields( "soundex" );
			
		}
		
		if( $p )
			$where .= " AND";
			
		$where .= " ( c.name$lang LIKE '%" . addslashes( $keyword ) . "%'";
		
		if( USE_SOUNDEX && !empty( $soundex ) )
			$where .= " OR SOUNDEX( c.name$lang ) LIKE '%" . addslashes( $soundex ) . "%'";

		$where .= " )";

		$p++;

	}

	$where .= ")";
	$where .= ")";

	//recherche
	
	$start 			= ( $page - 1 ) * $resultPerPage;
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
	
	$query = "
	SELECT c.idcategory,
		( $relevancePattern ) AS relevance 
	FROM category c
	LEFT JOIN category_link cl ON cl.idcategorychild = c.idcategory
	WHERE ( $where ) 
	AND c.idcategory <> 0
	AND c.catalog_right <= '$catalog_right' 
	AND c.available = 1
	AND ( $relevancePattern ) > 0
	ORDER BY relevance DESC, 
		c.star_order DESC, 
		cl.displayorder ASC	
	LIMIT $start, $resultPerPage";

	$rs = DBUtil::query( $query );

	if( $rs === false )
		 trigger_error( "Impossible d'éffectuer la recherche", E_USER_ERROR );

	$results = array();
	
	while( !$rs->EOF() ){

		array_push( $results, new CCategoryProxy( new CCategory( $rs->fields( "idcategory" ) ) ) );
		
		$rs->MoveNext();
		
	}

	return  $results;

}

/* ------------------------------------------------------------------------------------------- */

function updateSearchStats( $string ){

	$addresses = explode( ",", DBUtil::query( "SELECT GROUP_CONCAT( remote_addr ) AS addresses FROM white_list" )->fields( "addresses" ) );
	
	if( in_array( $_SERVER["REMOTE_ADDR"] , $addresses ) )
		return;
		
	$login = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getEmail() : "";
	
	DBUtil::query( "INSERT INTO stat_search VALUES( 
		" . DBUtil::quote( trim( strip_tags( $string ) ) ) . " , 
		NOW() , " . DBUtil::quote( $login ) . ",
		'" . $_SERVER["REMOTE_ADDR"] . "' )" );
	
}

/* ------------------------------------------------------------------------------------------- */

?>
