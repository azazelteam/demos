<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Appel de la fonction affichage pdf avoirs clients
 */
 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/odf/ODFCredit.php" );


$odf = new ODFCredit(  $_REQUEST[ "idcredit" ] );

$odf->exportPDF( "Avoir n° " .  $_REQUEST[ "idcredit" ]  . ".pdf" );

?>