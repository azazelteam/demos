<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Envoi d'un mail au commercial
 */

function commercial($template){
	global  $GLOBAL_START_URL;
	
	$idcom = Basket::getInstance()->getSalesman()->getId();
	
	if( $idcom ) {
	
		$Email = User::getInstance()->get('email');
		$Name = User::getInstance()->get('gender').' '.User::getInstance()->get('firstname').' '.User::getInstance()->get('lastname');
		if( !empty($nomailto) ) {
			$mailto = $Email;
		}else{
			$href = "$GLOBAL_START_URL/catalog/mailto.php?iduser=$idcom";
			if( isset($idorder) ) 			$href .= "&IdOrder=$idorder";
			else if( isset($IdEstimate) ) 	$href .= "&IdEstimate=$IdEstimate";
			else if( isset($Product) ) 		$href .= "&IdProduct=".$Product->GetId();
			$open = "'$href','mailto','width=620,height=750,toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1'";
			$mailto = '<a href="#" onclick="window.open('.$open.');">'.$Email.'</a>';
		}
		$fields = array('gender','lastname','firstname','address','address_2','city','zipcode','idstate','phonenumber','faxnumber');
		$default = '&nbsp;';
		foreach($fields as $fieldname) {
			$$fieldname = User::getInstance()->get($fieldname);
			$$fieldname = getValueOrDefaultIfEmpty($$fieldname,$default);
			
			$template->setData('commercial_'.$fieldname,$$fieldname);	
		}
			$template->setData('commercial_mailto',$mailto);	
		
		$lang = "_1";
		$state = DBUtil::getDBValue( "name$lang", "state", "idstate", $idstate );
		$title = DBUtil::getDBValue( "title$lang", "title", "idtitle", $gender );
		$template->setData('commercial_title',$title);	
		$template->setData('commercial_state',$state);	
			
		$template->setData('hasCommercial',"ok");	
		
	}else{
		$template->setData('hasCommercial',"");	
	}
}
?>