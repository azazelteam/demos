<?php

/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Script d'appel et de vérification avant paiement
 */

// * Historique *****************************

// * Modification ***************************


// include necessary files
include_once('../objects/classes.php');
include_once('../objects/Order.php');
include_once('ssl.inc.php');
include_once('order.inc.php');
include_once('payment.php');

$Order = new Order();

$Confirm ='';
if( isset($_POST["Confirm"]) ) $Confirm = $_POST["Confirm"];
else if( isset($_GET["Confirm"]) ) $Confirm = $_GET["Confirm"];

$SSL = array('cardnumber'=>'','exp_date_card'=>'','name_card'=>'');
if( isset($_POST["SSL"]) ) $SSL = $_POST["SSL"];

if( isset($_POST["Payment"]) ) $Payment = $_POST["Payment"];
if( isset($_POST["Comment"]) ) $Comment = $_POST["Comment"];
if( isset($_POST["DocType"]) ) $DocType = $_POST["DocType"];


$ScriptName="ssl.php";


if( $Confirm != "")

{

	if(VerifySSL($SSL))
	{
		$IdDoc= $Order->Basket2Order($Payment,$Comment);
		if($IdDoc>0)
		{	
			UpdateSSL($IdDoc,$SSL);
			if(ConfirmPayment($Payment))
				ConfirmOrder($IdDoc);
			//affichage
			$idorder=$IdDoc;
			include "show_order.php";
		}
		else
		{
			echo "Commande deja traitée !<br />";
		}
	}
}

else
	DisplaySSLForm($ScriptName,$SSL);

?>