<?php
/** 
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Traitement + Affichage de la 3ème étape du devis
 * Parrainage
 */

include_once('../objects/classes.php');  

require_once 'PEAR.php';

include_once( "$GLOBAL_START_PATH/objects/flexy/WebPageController.php" );

$Lang=Session::getInstance()->getLang();

$baseURL=$GLOBAL_START_URL;
$start_path=$GLOBAL_START_PATH;

class IndexController extends WebPageController{};
$sponsorship = new IndexController( "sponsorship.tpl" );

$DEBUG = 'none';

/**
 * Traitement du formulaire
 */
if(Session::getInstance()->islogin()){
	$sponsorship->setData('logged','true');
	if(isset($_POST["email"])){
	
		$arrayMails=$_POST["email"];
		$arrayFirstName=$_POST["firstname"];
		$arrayLastName=$_POST["lastname"];
		$compteMails=count($arrayMails);
		
		for($i=0 ; $i<$compteMails ; $i++){
			if($arrayMails[$i]){
				
				include_once("$GLOBAL_START_PATH/objects/mime_mail/htmlMimeMail.php");
				
				$mailer = new htmlMimeMail();
				$lastNameParrain=$_SESSION['buyerinfo']['lastname'];
				$firsNameParrain=$_SESSION['buyerinfo']['firstname'];
				$mailParrain=$_SESSION['buyerinfo']['email'];
		
				$mailer->setFrom( "\"$firsNameParrain $lastNameParrain \" <$mailParrain>" );

				include_once("$GLOBAL_START_PATH/objects/Sponsorship.php");
				
				Sponsorship::create(Session::getInstance()->islogin(),$arrayMails[$i],$arrayFirstName[$i],$arrayLastName[$i]);
			
				$sponsorship->setData('sponsorshipSent','ok');
				$sponsorship->setData('sponsorshipError',"");
			}else{
				$sponsorship->setData('sponsorshipError',"Aucun filleul n'a été entré");
			}		
		}
	}
	$sponsorship->setData('sponsorshipSent','');
}else{
	$sponsorship->setData('sponsorshipSent','ok');
	$sponsorship->setData('sponsorshipError','');
	$sponsorship->setData('logged','');
}

$sponsorship->setData('ScriptName', "sponsorship.php" );

if(isset($IdCategory) AND $IdCategory>0){		
	$bdd = DBUtil::getConnection();			
	$query = "SELECT name_1 FROM category WHERE idcategory='$IdCategory'";
	$rs = $bdd->Execute($query);
	
	$cat_name = $rs->fields("name_1");				
	$first = $cat_name{0};			
	$voyelles = "aeiouyAEIOUY";				
	$isvoy = strpos($voyelles,$first);
	
	if($isvoy===false){
		$appart_catalog = "Le catalogue de ".$cat_name;
		$sponsorship->setData('appart_catalog',$appart_catalog);
	}else{
		$appart_catalog = "Le catalogue d'".$cat_name;
		$sponsorship->setData('appart_catalog',$appart_catalog);
	}
}else{
		$sponsorship->setData('appart_catalog',"");
}

$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_estimate_conversion_id = DBUtil::getParameterAdmin( "google_estimate_conversion_id" );

$sponsorship->GetPromotions();
$sponsorship->GetCategoryLinks();
$sponsorship->GetBrands();
$sponsorship->GetTrades();
$sponsorship->GetProductHistory();

$sponsorship->output();

?>