<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Confirmation devis
 */

include_once( dirname( __FILE__ ) . "/../objects/classes.php" );
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( "HTML/Template/Flexy.php" );
include_once( "HTML/Template/Flexy/Element.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/EstimateProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/Estimate.php" );
$lang = "_1";

include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );

class EstimateController extends PageController{};
$confirm_estimate = new EstimateController( "confirm_estimate.tpl" , 'Confirmation de devis' );


//---------------------------------------------//
//--------------Google conversion--------------//
//---------------------------------------------//

$UseGoogleConversion = DBUtil::getParameterAdmin( "use_google_conversion" );
$google_conversion_id = DBUtil::getParameterAdmin( "google_estimate_conversion_id" );
$google_conversion_label = DBUtil::getParameterAdmin( "google_conversion_label_estima" );
$ScriptNem = "estimate_complete.php";


if( $ScriptName == $ScriptNem && $google_conversion_id && $google_conversion_label && $UseGoogleConversion ){
	$confirm_estimate->setData('google',1);
	$confirm_estimate->setData('google_conversion_id',$google_conversion_id); 
	$confirm_estimate->setData('google_conversion_label',$google_conversion_label); 
}else{
	$confirm_estimate->setData('google',0);
}

//----------------------------------------------------------------------------------------------

$ScriptName = "confirm_estimate.php";

//---------------------------------------------//
//-------------------Account-------------------//
//---------------------------------------------//

if( Session::getInstance()->getCustomer() ) {

	$confirm_estimate->setData( "isLogin", 1 );

} else {

	$confirm_estimate->setData( "isLogin", "" );
	$FromScript = $ScriptName;

}

$confirm_estimate->setData( "idEstimate", $estimate->getId() );
$confirm_estimate->setData( "net_to_pay", $estimate->get("total_amount") );
$confirm_estimate->setData( "total_amount_ht", $estimate->get("total_amount_ht") );

$basket_article1 = array();
$it1 = $estimate->getItems()->iterator();
$i=0;
while( $it1->hasNext() )
{
$idarticle=$it1->next()->get("idarticle");
$idproduct = DBUtil::getDBValue( "idproduct", "detail", "idarticle", $idarticle  );
$productname=DBUtil::getDBValue( "name" . "_1", "product", "idproduct", $idproduct );
$quantity= $estimate->getItemAt($i)->get( "quantity" );
$unit_price = Util::priceFormat($estimate->getItemAt($i)->get( "unit_price" )) ;
$reference = $estimate->getItemAt($i)->get( "reference" );

	$basket_article1[ $i ][ "productname" ]		= $productname;
	$basket_article1[ $i ][ "idarticle" ]		= $idarticle;
	$basket_article1[ $i ][ "reference" ]		=$reference;
	$basket_article1[ $i ][ "quantity" ]			= $quantity;
	$basket_article1[ $i ][ "unit_price" ]		= $unit_price;

$i++;

}

$confirm_estimate->setData('basket_articles1',$basket_article1);
//var_dump($basket_article);
$confirm_estimate->output();

//----------------------------------------------------------------------------------------------

?>