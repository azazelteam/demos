<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Affichage fiche produit (redirections + commentaires clients)
 */
 

/* ------------------------------------------------------------------------------------------ */ 
include_once( "../config/init.php" );

if( !isset( $_GET[ "IdProduct" ] )
	|| !intval( $_GET[ "IdProduct" ] )
	|| !DBUtil::query( "SELECT idproduct FROM product WHERE idproduct = '" . intval( $_GET[ "IdProduct" ] ) . "' LIMIT 1" )->RecordCount() ){
	
	header( "HTTP/1.1 301 Moved Permanently" );
	header( "Status: 301 Moved Permanently" );
	header( "Location: " . URLFactory::getHostURL() );
	exit();

}


if(isset($_REQUEST['check_user_connected'])){

include_once( dirname( __FILE__ ) . "/../objects/Session.php" );
echo json_encode((array)Session::getInstance()->getCustomer()->getUser());
exit();
}

if(isset($_REQUEST['addCommentaire'])){
	echo saveComment();
	exit();
}

include_once( "$GLOBAL_START_PATH/objects/catalog/CProduct.php" );

$product = new CProduct( intval( $_GET[ "IdProduct" ] ) );

/*-----------------------------------------------------------------------------------------------------*/
$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');
if ( ($occ == 0) && ($occ1 == 0) )
{
	
	$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
	
	$urll_p = strstr($_url_request, '&perfid=');
	
	$urr = substr($urll_p, 0, 8);
	
	if ($urr == "&perfid=")
	{
		$url_perfid = str_replace("&","?",$urll_p);
		
		$url_dis = URLFactory::getProductURL( $_GET[ "IdProduct" ] ).$url_perfid;
		
		
		
	
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header( "Location: " . $url_dis );
		exit();	
		
	}
}

/*------------------------------------------------------------------------------------------*/

$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');
if ( ($occ == 0) && ($occ1 == 0) )
{
	$urll = strstr($_url_request, '?perfid=');
	
	$urr = substr($urll, 0, 8);
	$urll1 = strstr($_url_request, '?gclid=');
	$urr1 = substr($urll1, 0, 7);
	if ($urr != "?perfid=" && $urr1 !="?gclid=")
	{
		$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
		$urll = strstr($_url_request, '?', true);
		if( $urll)
		{
			header( "HTTP/1.1 301 Moved Permanently" );
			header( "Status: 301 Moved Permanently" );
			header( "Location: " . $urll );
			exit();	
		}
	}
}
/*----------------------------Redirection si nom produit erroné----------------------------*/

$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');


if ( ($occ == 0) && ($occ1 == 0) )
{
	$urll = strstr($_url_request, '?perfid=');
	
	$urr = substr($urll, 0, 8);
	$urll1 = strstr($_url_request, '?gclid=');
	$urr1 = substr($urll1, 0, 7);

	if ($urr != "?perfid=" && $urr1 !="?gclid=")
	{

		$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
		if ($_url_request != URLFactory::getProductURL( $_GET[ "IdProduct" ] ) && !isset($_GET["utm_source"]) )
		{
			header( "HTTP/1.1 301 Moved Permanently" );
			header( "Status: 301 Moved Permanently" );
			header('HTTP/1.0 404 Not Found');
			
			include_once( DOCUMENT_ROOT . "/objects/flexy/PageController.php" );
			
			$template = "404.tpl";
			$controller = new PageController( $template );
			
			$notfound = "Erreur 404: Cette page n'existe pas (ou plus) ...";
		
			$controller->setData( "notfound", $notfound );
			
			$controller->output();	
			
			exit();
	
		}

	}
}
/*----------------------------------------------------------------------------------------*/


/* redirections permanentes */

if( $idproduct = DBUtil::getDBValue( "idproduct", "product_redirections", "idreferrer", $product->getId() ) ){

	header( "HTTP/1.1 301 Moved Permanently" );
	header( "Status: 301 Moved Permanently" );
	header( "Location: " . URLFactory::getProductURL( $idproduct ) );

	exit();

}

/* redirections permanentes vers catégorie */

if( $idcateg = DBUtil::getDBValue( "redirection", "product", "idproduct", $product->getId() ) ){
	
	if ($idcateg == 404)
	{
	
	header( "HTTP/1.1 301 Moved Permanently" );
	header( "Status: 301 Moved Permanently" );
	header( "Location: ". $GLOBAL_START_URL."/404.php" );
		
	}
	
		
	else if ($idcateg != 0 )
	{
	header( "HTTP/1.1 301 Moved Permanently" );
	header( "Status: 301 Moved Permanently" );
	header( "Location: " . URLFactory::getCategoryURL( $idcateg ) );

	
	}
	exit();
}




/* ------------------------------------------------------------------------------------------ */
/* produit non disponible */

$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;

if( !User::getInstance()->getId() && (
		!$product->get( "available" )
		|| $product->get( "catalog_right" ) > $catalog_right
		|| !isCategoryAvailable( $product->get( "idcategory" ) )
	) ){
	
	header( "HTTP/1.1 301 Moved Permanently" );
	header( "Status: 301 Moved Permanently" );
	header( "Location: $GLOBAL_START_URL" );
	exit();

}

/* ------------------------------------------------------------------------------------------ */
/* commentaires produit */


if(  isset( $_REQUEST[ "comment" ] ) && isset( $_REQUEST[ "IdProduct" ] ) )
	saveComment();

/* ------------------------------------------------------------------------------------------ */

include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CProductProxy.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CCategoryProxy.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CArticleProxy.php" );

/* ------------------------------------------------------------------------------------------ */

class ProductController extends PageController{

	public function __construct( $template ){

		parent::__construct( $template );

		if( Session::getInstance()->getCustomer() ){ /* @todo : CustomerProxy ? à justifier */

			$this->createElement( "gender", 	$this->customer->get( "title" ) );
			$this->createElement( "lastname", 	$this->customer->get( "lastname" ) );
			$this->createElement( "firstname", 	$this->customer->get( "firstname" ) );
			$this->createElement( "company", 	$this->customer->get( "company" ) );
			$this->createElement( "zipcode", 	$this->customer->get( "zipcode" ) );
			$this->createElement( "email", 		$this->customer->get( "mail" ) );

		}

	}

}

/* ------------------------------------------------------------------------------------------ */
/* nombre de visites */

if( !User::getInstance()->getId() )
	DBUtil::query( "UPDATE product SET hits = hits + 1 WHERE idproduct = '" . $product->getId() . "' LIMIT 1" );

/* ------------------------------------------------------------------------------------------ */
/* Enregistrer les derniers articles consultés */

if(isset ($_COOKIE["FrontOfficeNetlogix"])){
	$myCookie = $_COOKIE["FrontOfficeNetlogix"];
}else{
	$myCookie="";
}

if( !User::getInstance()->getId() )
{
	$query = "
		SELECT idproduct
		FROM stat
		";
		
		$rs =& DBUtil::query( $query );
		$nb_product = $rs->RecordCount();
		
		if ( $nb_product > 5000 )
		{
	$query1 = "
	DELETE FROM stat WHERE DateHeure < NOW()
		";
		
		$rs1 =& DBUtil::query( $query1 );
}
DBUtil::query( "INSERT INTO stat (idproduct, idsession, DateHeure ) VALUES ( " . $product->getId() .", '" . $myCookie . "', NOW() )" );


}

/* ------------------------------------------------------------------------------------------ */

$controller = new ProductController( "/product/chartep_one.tpl" );
$controller->setTypeEncoding(2);
$controller->head->_setTitle();
$_SESSION["menuTypeEncoding"] = 2;
$controller->setData( "category", new CCategoryProxy( new CCategory( $product->get( "idcategory" ) ) ) );

$productProxy = new CProductProxy( $product );
$controller->setData( "product", $productProxy );
$controller->setData( "idProduct", $productProxy->getId() );

//article le moins cher
//@todo : flexy patch = complex arguments

$cheapest = NULL;

$articles = $product->getArticles()->iterator();

//var_dump($productProxy->getLowerPriceRef());exit();

while( $articles->hasNext() ){

	$article =& $articles->next();

	if( !$article->get( "hidecost" ) && $article->getPriceET() > 0.0 && ( $cheapest === NULL || $cheapest->getPriceET() > $article->getPriceET() ) )
		$cheapest = $article;

}

$controller->setData( "cheapestArticle", $cheapest ? new CArticleProxy( $cheapest ) : NULL );

//@todo : flexy patch = complex arguments
$controller->setData( "IsRayonnage", in_array( $product->get( "idcategory" ), array( 1303, 1313, 1305, 1315, 1310, 1316, 131401 ) ) );

//@todo : flexy patch = dynamic includes
$controller->setData( "productTemplate", $product->get( "productmask" ) );
$controller->setData( "articlesTemplate", $product->get( "detailmask" ) );

$controller->setData( "allowOrder", allowOrder( $product ) );
$actionCommentsForm 	= URLFactory::getProductURL( intval( $_REQUEST[ "IdProduct" ] ) );
$controller->setData( "actionCommentsForm", $actionCommentsForm);

$hasComment = false;
$comments = getComment();
if(count($comments) > 0) {
	$hasComment = true;
}
$controller->setData("hasComment", $hasComment);
$controller->setData("comments", $comments);

/* sortie */

$controller->output();

//-----------------------------------------------------------------------

function allowOrder( CProduct &$product ){

	$it = $product->getArticles()->iterator();

	$hasPrice = false;
	while( !$hasPrice && $it->hasNext() )
		$hasPrice |= $it->next()->get( "hidecost" ) == 0;

	if( !$hasPrice )
		return false;

	if( !Session::getInstance()->getCustomer() )
		return true;

	$customer = Session::getInstance()->getCustomer();

	if( $customer->get( "idstate" ) != 1 )
		return false;

	if( strlen( $customer->get( "zipcode" ) ) > 2 && substr( $customer->get( "zipcode" ), 0, 2 ) == "20" )
		return false;

	if( $customer->get( "idstate" ) == 44 )
		return false;

	return true;

}

//-----------------------------------------------------------------------
/**
 * Commentaires produit visiteur
 * @return void
 */
function saveComment(){
	if( !strlen( $_REQUEST[ "comment" ] ) )
		return;
	$rating = isset( $_REQUEST[ "rating" ] ) ? intval( $_REQUEST[ "rating" ] ) : 0;
	$comment = Util::doNothing($_REQUEST[ "comment" ]);
$today=
	$idbuyer 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$idcontact 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getContact()->getId() : 0;
	$nom= isset( $_REQUEST[ "name" ] ) ?  $_REQUEST[ "name" ]  : '';
	$prenom=isset( $_REQUEST[ "sur_name" ] ) ?  $_REQUEST[ "sur_name" ]  : '';
	$email=isset( $_REQUEST[ "user_mail" ] ) ?  $_REQUEST[ "user_mail" ]  : '';
	$city=isset( $_REQUEST[ "user_city" ] ) ?  $_REQUEST[ "user_city" ]  : '';
	$query = "
	INSERT INTO product_comment( idbuyer, idcontact,user_name,user_surname,user_mail,user_city, idproduct, product_comment, date,date_affich, note_buyer )
	VALUES(
		'$idbuyer',
		'$idcontact',
		'$nom',
		'$prenom',
		'$email',
		'$city',
		'" . intval( $_REQUEST[ "IdProduct" ] ) . "',
		" . DBUtil::quote( stripslashes( strip_tags( $comment) ) ) . ",
		NOW(),
		NOW(),
		'$rating'
	)";

	$ret = DBUtil::query( $query );

	if( $ret === false )
		return;

	$productURL 	= URLFactory::getProductURL( intval( $_REQUEST[ "IdProduct" ] ) );
	$productName 	= htmlentities( DBUtil::getDBValue( "name_1", "product", "idproduct", intval( $_REQUEST[ "IdProduct" ] ) ) );
	$idproduct 		= intval( $_REQUEST[ "IdProduct" ] );
	$idcomment 		= DBUtil::query( "SELECT MAX( idproduct_comment ) AS idcomment FROM product_comment" )->fields( "idcomment" );
	$comment 		= $_REQUEST[ "comment" ];
	$hostURL 		= URLFactory::getHostURL();

	/*$html = "
	<p>
		<b>Nom du produit :</b>
		<a href=".$productURL.">$productName</a>
	</p>
	<p>
		<b>Commentaire :</b>
		<span style='border:1px solid #E7E7E7; padding:10px;'>".$comment."</span>
	</p>
	<p>
		<a href='".$hostURL."/templates/product_management/product_comments.php?idproduct=".$idproduct."&amp;idproduct_comment=".$idcomment."&amp;validate=1&amp;referer=mail'>Cliquez ici pour valider le commentaire</a>
	</p>
";*/

$html = "
<p>		<strong> $prenom $nom, le " . date("Y-m-d") . " </strong>
						<span id='commentNote_$rating' class='noters'>
						<select name='buyerNote_$rating' disabled='disabled'>
								<option value='0'>0 étoile</option>
								<option value='1'>1 étoile</option>
								<option value='2'>2 étoiles</option>
								<option value='3'>3 étoiles</option>
								<option value='4'>4 étoiles</option>
							</select>

						</span>
						<br />
						" . DBUtil::quote( stripslashes( strip_tags( $comment) ) ) . "
					</p>
	<p>
		
";

	include_once ( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );

	$mailer = new htmlMimeMail();
	$email = DBUtil::getParameterAdmin( "comment_products_dest_mail" );

	$From = DBUtil::getParameterAdmin( "admin_email" );

	$mailer->setFrom( $From );
	$mailer->setHtmlCharset('UTF-8');
	$mailer->setSubject( "Nouveau commentaire produit : $productName" );
	$mailer->setHtml( $html );
	//if( !$mailer->send( array( $email ) ) )
	if( !$mailer->send( array( $email ) ) )
	
		trigger_error( "Impossible d'envoyer le courriel de commentaire produit" );
	return $html;

}

function getComment(){
	$idbuyer 	= Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->getId() : 0;
	$query = "SELECT * FROM product_comment WHERE idbuyer = " . $idbuyer ." ORDER BY date DESC";
	$ret = DBUtil::query( $query );
	$comment = array();
	while (!$ret->EOF())
		{
			$c['name'] 				= $ret->fields('user_name') .' '. $ret->fields('user_surname') ;
			$c['date'] 				= $ret->fields('date_affich');
			$c['product_comment'] 	= $ret->fields('product_comment');
			$c['note_buyer'] 		= $ret->fields('note_buyer');
			$comment[$ret->fields('idproduct_comment')] = $c;
						$ret->MoveNext();		
		}

	return $comment;

}

/* ------------------------------------------------------------------------------------------ */

function isCategoryAvailable( $idcategory ){
	
	if( !intval( $idcategory ) )
		return false;

	if( User::getInstance()->getId() )
		return true;
	
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		
	$query = "
	SELECT 1
	FROM category
	WHERE idcategory = '$idcategory'
	AND available = 1
	AND catalog_right <= '$catalog_right'
	LIMIT 1";
	
	if( !DBUtil::query( $query )->RecordCount() )
		return false;
		
	$idparent = DBUtil::getDBValue( "idcategoryparent", "category_link", "idcategorychild", $idcategory );
	
	return $idparent ? isCategoryAvailable( $idparent ) : true;
	
}

/* ------------------------------------------------------------------------------------------ */

?>
