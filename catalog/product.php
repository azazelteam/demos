<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Fonctions plus utilisées à priori ....?
 */

include_once('../objects/classes.php');

include_once( "../config/init.php" );
include_once("$GLOBAL_START_PATH/objects/catalog/CCategory.php");
/* ------------------------------------------------------------------------------------------ */
/* redirections SEO permanentes : Luc Bizet */
	/*----------------------------Redirection si nom catégorie erroné----------------------------*/


$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');
if ( ($occ == 0) && ($occ1 == 0) )
{	
	$urll = strstr($_url_request, '?perfid=');
	$urll1 = strstr($_url_request, '?gclid=');

	$urr = substr($urll, 0, 8);
	$urr1 = substr($urll1, 0, 7);

	if (($urr != "?perfid=") && ($urr1 != "?gclid=") )
	{
	
		if ($_url_request != URLFactory::getCategoryURL( $_GET[ "IdCategory" ]) )
		{
			header( "HTTP/1.1 301 Moved Permanently" );
			header( "Status: 301 Moved Permanently" );
			header('HTTP/1.0 404 Not Found');
			
			include_once( DOCUMENT_ROOT . "/objects/flexy/PageController.php" );
			
			$template = "404.tpl";
			$controller = new PageController( $template,2 );
			$controller->setTypeEncoding(2);
            $_SESSION["menuTypeEncoding"] = 2;
			$notfound = "Erreur 404: Cette page n'existe pas (ou plus) ...";
		
			$controller->setData( "notfound", $notfound );
			
			$controller->output();	
			
			exit();
	
		}
	}
}
/*----------------------------------------------------------------------------------------*/
	$gclid = (isset($_GET['gclid'])) ? $_GET['gclid'] : '';
	//var_dump($_GET);exit();
	$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
	$_url_page = URLFactory::getCategoryURL( $_GET[ "IdCategory" ], $gclid);
	
	
	if($_url_request != $_url_page)
	{
	
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header( "Location: " . URLFactory::getCategoryURL( $_GET[ "IdCategory" ],$gclid) );
		exit();	
	}
$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');

if ( ($occ == 0) && ($occ1 == 0) )
{

	$urll_p = strstr($_url_request, '&perfid=');
	$urll1_p = strstr($_url_request, '&gclid=');
	
	$urr = substr($urll_p, 0, 8);
	$urr1 = substr($urll1_p, 0, 7);
	
	if ( ($urr == "&perfid=") && ($urr1!= "&gclid=") )
	{
		$url_perfid = str_replace("&","?",$urll_p);
		
		$url_dis = URLFactory::getCategoryURL( $_GET[ "IdCategory" ]).$url_perfid;
		
		//print $url_perfid;
		
	
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header( "Location: " . $url_dis );
		exit();	
		
	}
}
/*------------------------------------------------------------------------------------------*/

$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');
if ( ($occ == 0) && ($occ1 == 0) )
{

	$urll = strstr($_url_request, '?perfid=');
	$urll1 = strstr($_url_request, '?gclid=');
	
	$urr = substr($urll, 0, 8);
	$urr1 = substr($urll1, 0, 7);
	
	if (($urr != "?perfid=") && ($urr1 != "?gclid=") )
	{
		$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
		$urll = strstr($_url_request, '?', true);
		if( $urll)
		{
	
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header( "Location: " . $urll );
		exit();	
		}
	}
}

$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];

$occ = substr_count($_url_request, '*utm_source*=');
$occ1 = substr_count($_url_request, 'utm_source=');
/*if ( ($occ == 0) && ($occ1 == 0) )
{
	
	$urll = strstr($_url_request, '?perfid=');

	$urr = substr($urll, 0, 8);

	if ($urr != "?perfid=")
	{
		$gclid = (isset($_GET['gclid'])) ? $_GET['gclid'] : '';
		//var_dump($_GET);exit();
		$_url_request = URLFactory::getHostURL().$_SERVER['REQUEST_URI'];
		$_url_page = URLFactory::getCategoryURL( $_GET[ "IdCategory" ], $gclid);
		
		
		
		if($_url_request != $_url_page)
		{
	
			header( "HTTP/1.1 301 Moved Permanently" );
			header( "Status: 301 Moved Permanently" );
			header( "Location: " . URLFactory::getCategoryURL( $_GET[ "IdCategory" ],$gclid) );
			exit();	
		}
	}
}*/

/* ------------------------------------------------------------------------------------------ */

if( isset( $_POST['intitules'] ) && isset( $_GET[ "IdCategory" ] ) || !intval( $_GET[ "IdCategory" ] ) ){
	
	header('Content-Type: text/html; charset=utf-8'); 	
	
	include_once( DOCUMENT_ROOT . "/objects/flexy/PageController.php" );
	$template = "category/search_engine_results.tpl";
	$controller = new PageController( $template,2 );
    $controller->setTypeEncoding(2);
    $_SESSION["menuTypeEncoding"] = 2;
	include_once( DOCUMENT_ROOT . "/objects/catalog/CCategory.php" );
		
	$idcat = isset( $_POST[ 'intitules' ][ 'sscategory' ] ) && $_POST[ 'intitules' ][ 'sscategory' ] > 0 ? $_POST[ 'intitules' ][ 'sscategory' ] : $_GET[ "IdCategory" ];
	
	$category = new CCategory( $idcat );
	
	include_once( DOCUMENT_ROOT . "/objects/CategoryFilter.php" );
	$filter = new CategoryFilter( $category  );
		
	foreach( $_POST['intitules'] as $key => $value ){
		$filter->selectNode( $key , $value );
	}
		
	$controller->setData( "products", $filter->getProducts() );
	
	if( !$filter->getProductCount() ){
		exit( 'noproducts' );
	}
	
	$controller->output();
	
	
	
	exit();
}

/* ------------------------------------------------------------------------------------------ */
/* catégorie non disponible */

if( !isset( $_GET[ "IdCategory" ] ) || !intval( $_GET[ "IdCategory" ] )
	|| !DBUtil::query( 'SELECT 1 FROM `category` WHERE idcategory = "'. intval( $_GET[ "IdCategory" ] ) . '"  LIMIT 1' )->RecordCount() ){

	header( "Location: " . URLFactory::getHostURL() );
	exit();
	
}

/* ------------------------------------------------------------------------------------------ */

if( !isCategoryAvailable( intval( $_GET[ "IdCategory" ] ) ) ){
	// on redirige vers la cat mère sauf si c'est root
	$parent = DBUtil::query( 'SELECT idcategoryparent FROM category_link WHERE idcategorychild = ' . intval( $_GET[ "IdCategory" ] ) )->fields( 'idcategoryparent' );	
	
	if( $parent > 0 )
	{
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header( "Location: " . URLFactory::getCategoryURL( $parent ) );
	}
	else
	{
		header( "HTTP/1.1 301 Moved Permanently" );
		header( "Status: 301 Moved Permanently" );
		header( "Location: " . URLFactory::getHostURL() );
	}
	exit();
	
}

/* ------------------------------------------------------------------------------------------ */

include_once( "$GLOBAL_START_PATH/objects/catalog/CCategory.php" );

$category = new CCategory( intval( $_GET[ "IdCategory" ] ) );
//var_dump($category->getOthercategProd());exit();
if( strlen( $category->get( "html" ) ) ){
	
	header( "Location: " . $category->get( "html" ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------ */
$is_static_page = DBUtil::getDBValue('idpage', 'category_custom_pages', 'idcategory', $_GET[ "IdCategory" ]);
if ($is_static_page) {
	$cp_filename = DBUtil::getDBValue('filename', 'custom_pages', 'idpage', $is_static_page);
	//header( "Location: " . $cp_filename.".php");
    header( "Location: " . $cp_filename);
	exit();
}

/* ------------------------------------------------------------------------------------------ */

include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CCategoryProxy.php" );


//Fin//
/* Fin intervention  */

if( $category->getChildren()->get( 0 ) instanceof CCategory )
		$template = strlen( $category->get( "idcharte" ) ) ? "category/chartec_" . $category->get( "idcharte" ) . ".tpl" : "category/chartec_default.tpl";
else 	$template = strlen( $category->get( "idcharte" ) ) ? "product/chartep_" . $category->get( "idcharte" ) . ".tpl" : "product/chartep_default.tpl";


$filename = "$GLOBAL_START_PATH/catalog/product_aux.php";

$cat_page_title ="";
if (file_exists($filename)) 
include("$GLOBAL_START_PATH/catalog/product_aux.php");

$controller = new PageController( $template,2 );
$controller->head->setCategoryMetaTags($IdCategory);
$controller->head->_setTitle();
//var_dump($controller->head->getMetaTag("description"));die;
$controller->setTypeEncoding(2);
$_SESSION["menuTypeEncoding"] = 2;
$proxy = new CCategoryProxy( $category );

//var_dump($proxy->getOthercategProd()[0]->getArticleAt(0)->getId());die;

$controller->setData( "category", $proxy );

//var_dump($proxy->getChildren()[0]->getArticleAt(0)->getId());die;
$controller -> setData("cat_page_title", $cat_page_title);

/****** ticket 25 ******/

$Category = new CCATEGORY($IdCategory);

if( $Category->getParent() ){

    $categoryPath = $Category->GetPathCat();

    $controller->setData('categoryPath',$categoryPath);
}else{
    $controller->setData('categoryPath','');
}
/***********************/
/* pages statiques */

$rs =& DBUtil::query(

	"SELECT cp.* FROM custom_pages cp, category_custom_pages ccp
	WHERE ccp.idcategory = '" . $category->getId() . "'
	AND ccp.idpage = cp.idpage
	ORDER BY ccp.`index` ASC"
	
);

$static_web_pages = array();
while( !$rs->EOF() ){

	array_push( $static_web_pages, "/" . $rs->fields( "filename" ) . ".php" );
	$rs->MoveNext();
	
}

$controller->setData( "static_web_pages", count( $static_web_pages ) ? $static_web_pages : NULL );
	
/* moteur de recherche bacs, @todo : dynamique */

if( $category->get( "search" ) ){ 

	include_once( dirname( __FILE__ ) . "/search_engine/index.php" );	
	createSearchEngine( $controller, $proxy );
	
}

include_once( "$GLOBAL_START_PATH/objects/CategoryFilter.php" );
$filter = new CategoryFilter( $category );
$controller->setData( 'search_filter' , $filter );

if( $filter ){
	$filter->createNodes();
}
//Onja
if (file_exists($filename))
{
$breadcrumbs = makeHTMLBreadCrumbs($_GET["IdCategory"],2);
$controller->setData('breadcrumbs', $breadcrumbs);
}
//
/* sortie */



$controller->output();

/* ------------------------------------------------------------------------------------------ */

function isCategoryAvailable( $idcategory ){
	
	if( !intval( $idcategory ) )
		return false;

	if( User::getInstance()->getId() )
		return true;
	
	$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;
		
	$query = "
	SELECT 1
	FROM category
	WHERE idcategory = '$idcategory'
	AND available = 1
	AND catalog_right <= '$catalog_right'
	LIMIT 1";
	
	if( !DBUtil::query( $query )->RecordCount() )
		return false;
		
	$idparent = DBUtil::getDBValue( "idcategoryparent", "category_link", "idcategorychild", $idcategory );
	
	return $idparent ? isCategoryAvailable( $idparent ) : true;
	
}

?>