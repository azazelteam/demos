<?php

/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Permet de consulter ou de créer un devis.
 * =============================================================================================================================
 * Consultation d'un devis ( paramètre d'URL 'idestimate' fourni )
 * 
 * IMPORTANT : la personne autorisée à consulter est le devis est :
 *    - soit un utilisateur du logiciel identifié
 *    - soit le propriétaire du devis, identifié sur le site
 *    
 * =============================================================================================================================
 * Création d'un devis à partir du panier ( données postées )
 * 
 * IMPORTANT : le visiteur doit être identifié sur le site
 * IMPORTANT : le panier ne peut être vide
 * =============================================================================================================================
 * Les données suivantes seront traitées si elles sont postées par formulaire ( toutes sont facultatives ) :
 * 
 * 		comment : 				commentaire du client
 * 		idforwarding_address : 	identifiant de l'adresse de livraison sélectionnée par le client, 0 par défaut
 * 		idinvoice_address : 	identifiant de l'adresse de facturation sélectionnée par le client, 0 par défaut
 * 		idpayment : 			identifiant du mode de paiement sélectionné par le client, paramètre 'default_idpayment' par défaut
 * 		n_order : 				n° de devis correspondant côté client
 * 		idsource : 				identifiant de la provenance du client, 1 par défaut
 * 		auto :					si vaut 1, le devis sera traité automatiquement ( envoi immédiat par courriel ou fax ), 0 par défaut
 * 
 * -----------------------------------------------------------------------------------------------------------------------------
 * Délai de paiement : 	- si le paramètre 'auto_estimate' > 0.0 et que le total HT < paramètre 'auto_estimate' alors le mode de paiement 'à la commande' sera forcé
 * 						- sinon c'est le paramètre 'default_idpayment_delay' qui sera sélectionné
 * -----------------------------------------------------------------------------------------------------------------------------
 * Devis traités automatiquement :
 * 
 * Sous certaines conditions, le devis pouvait être traité automatiquement ( cad envoyé tout de suite au visiteur )
 * Désormais, les conditions nécessaires au traitement automatique du devis devront être placées dans vos templates.
 * 
 * Les prérequis étaient :
 *  - le paramètre 'auto_estimate' est renseigné ( valeur supérieure à 0.0 )
 * 	- le devis ne contient qu'un seul produit
 *  - le prix du produit est resneigné et est affiché sur le site
 *  - le délai de livraison du produit est renseigné
 *  - le visiteur n'a saisi aucun commentaire
 *  - le total HT du devis est supérieur à 0.0 
 *  - le total HT du devis est inférieur ou égal à la valeur du paramètre 'auto_estimate'
 * 	- le visiteur est français mais n'est pas corse
 * 
 * Si ces conditions sont remplies, rajouter un champ 'auto' dans votre formulaire avec pour valeur '1'
 * 
 * Dans le cas d'un devis traité automatiquement :
 * 	- la date de la première relance sera programmée pour dans 7 jours
 * 	- le devis est  tout de suite envoyé par courriel
 * 
 * =============================================================================================================================
 * Dépréciations :
 * 
 * Les fichiers aux adresses suivantes ainsi que les templates qu'ils utilisent deviennent inutiles :
 * 	- /catalog/estimate_basket.php
 *  - /catalog/estimate_buying.php
 *  - /catalog/estimate_complete.php
 *  - /catalog/estimate_order.php
 *  - /catalog/confirm_estimate.php
 *  - /catalog/show_estimate.php
 *  
 * =============================================================================================================================
 * Variables disponibles ( dans estimate.tpl )
 * 
 * 	{estimate} 	EstimateProxy en cas de succès ou false en cas d'échec
 * 
 * =============================================================================================================================
 */

include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/PageController.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/EstimateProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects//Estimate.php" );
include_once( dirname( __FILE__ ) . "/../objects/TradeFactory.php" );
include_once( dirname( __FILE__ ) . "/../objects/MailTemplateEditor.php" );
include_once( dirname( __FILE__ ) . "/../objects/mime_mail/htmlMimeMail.php" );


$_POST = Util::arrayMapRecursive( "strip_tags", 	$_POST );
$_POST = Util::arrayMapRecursive( "stripslahes", 	$_POST );

class EstimateController extends PageController{};
$controller = new EstimateController( "estimate.tpl" );

/* ------------------------------------------------------------------------------------- */
/* consultation d'un devis */

if( isset( $_GET[ "idestimate" ] ) && intval( $_GET[ "idestimate" ] ) ){
	
	if( User::getInstance()->getId() || (
		Session::getInstance()->getCustomer()
		&& Session::getInstance()->getCustomer()->getId() == DBUtil::getDBValue( "idbuyer", "estimate", "idestimate", intval( $_GET[ "idestimate" ] ) )
			
	) )
			$controller->setData( "estimate", new EstimateProxy( new Estimate( intval( $_GET[ "idestimate" ] ) ) ) );
	else 	$controller->setData( "estimate", false );
	
	$controller->output();
	
	exit();
	
}

/* ------------------------------------------------------------------------------------- */
/* visiteur non identifié ou panier vide */

if( !Basket::getInstance()->getItems()->size() || !Session::getInstance()->getCustomer() ){
	
	$controller->setData( "estimate", false );
	$controller->output();
	
	exit();
		
}

/* ------------------------------------------------------------------------------------- */
/* création du devis */

$estimate = TradeFactory::createEstimateFromBasket();

/* échec */

if( !( $estimate instanceof Estimate ) ){
	
	trigger_error( "Impossible de créer le devis", E_USER_ERROR );
	
	$controller->setData( "estimate", false );
	$controller->output();
	
	exit();
	
}

/* ------------------------------------------------------------------------------------- */
/* traitement formulaire */

if( isset( $_POST[ "comment" ] ) )
	$estimate->set( "comment", $_POST[ "comment" ] );

if( isset( $_POST[ "idforwarding_address" ] ) )
	$estimate->setForwardingAddress( intval( $_POST[ "idforwarding_address" ] ) ? new ForwardingAddress( intval( $_POST[ "idforwarding_address" ] ) ) : $estimate->getCustomer()->getAddress() );

if( isset( $_POST[ "idinvoice_address" ] ) )
	$estimate->setInvoiceAddress( intval( $_POST[ "idinvoice_address" ] ) ? new InvoiceAddress( intval( $_POST[ "idinvoice_address" ] ) ) : $estimate->getCustomer()->getAddress() );
	
if( isset( $_POST[ "idpayment" ] ) )
	$estimate->set( "idpayment", intval( $_POST[ "idpayment" ] ) );
	
if( isset( $_POST[ "n_order" ] ) )
	$estimate->set( "n_order", $_POST[ "n_order" ] );
	
if( isset( $_POST[ "idsource" ] ) )
	$estimate->set( "idsource", intval( $_POST[ "idsource" ] ) );

/* devis traités automatiquement */
	
if( isset( $_POST[ "auto" ] ) && intval( $_POST[ "auto" ] ) == 1 ){
	
	$estimate->set( "auto", 	1 );
	$estimate->set( "relaunch", date( "Y-m-d", mktime( 0, 0, 0, date( "m" ), date( "d" ) + 7, date( "Y" ) ) ) ); //relance sous 7 jours
		
}

$estimate->save();

/* ------------------------------------------------------------------------------------- */
/* envoyé le devis ou un accusé de réception par courriel */

mailEstimate( $estimate );

/* ------------------------------------------------------------------------------------- */
/*sortie */

$controller->setData( "estimate", $estimate );

$controller->output();

/* ------------------------------------------------------------------------------------- */
/* envoi du devis par mail ou par fax */

function mailEstimate( Estimate $estimate ){

	$editor = new MailTemplateEditor();
	
	$editor->setLanguage( 1 );
	$editor->setTemplate( $estimate->get( "auto" ) ? MailTemplateEditor::$FRONT_OFFICE_ESTIMATE : MailTemplateEditor::$ESTIMATE_NOTIFICATION );
	
	$editor->setUseAdminTags();
	$editor->setUseUtilTags();
	$editor->setUseEstimateTags( $estimate->getId() );
	$editor->setUseBuyerTags( $estimate->getCustomer()->getId(), $estimate->getCustomer()->getContact()->getId() );
	$editor->setUseCommercialTags( $estimate->getSalesman()->getId() );


	$mailer = new htmlMimeMail();
	$mailer->setFrom( "\"" . DBUtil::getParameterAdmin( "ad_name" ) . "\" <" . DBUtil::getParameterAdmin( "ad_mail" ) . ">" );
	
	if( DBUtil::getParameterAdmin( "cc_salesman" ) )
		$mailer->setCc( $estimate->getSalesman()->get( "email" ) );
	
	$mailer->setHtml( $editor->unTagBody() );
	$mailer->setSubject( $editor->unTagSubject() );
	
	/* ajout des pièces jointes pour les devis traités automatiquement */
	
	if( $estimate->get( "auto" ) ){
		
		/* PDF du devis */
		
		include_once( dirname( __FILE__ ) . "/../objects/odf/ODFEstimate.php" );
		
		$odf = new ODFEstimate( $estimate->getId() );
	
		$mailer->addAttachment( $odf->convert( ODFDocument::$DOCUMENT_PDF ), "Devis n° " . $estimate->getId() . ".pdf", "application/pdf" );
		
		/* PDF des fiches produit */

		$it = $estimate->getItems()->iterator();
		$products = array();
		
		while( $it->hasNext() )
			array_push( $products, DBUtil::getDBValue( "idproduct", "detail", "idarticle", $it->next()->get( "idarticle" ) ) );
	
		$products = array_unique( $products );
		
		foreach( $products as $idproduct )
			if( $idproduct )
				$mailer->addAttachment( file_get_contents( URLFactory::getPDFURL( $idproduct) ), "Notre fiche technique n° $idproduct.pdf", "application/pdf" );
	
	}
	
	if( !$mailer->send( array( $estimate->getCustomer()->getContact()->get( "mail" ) ) ) )
		trigger_error( "Impossible d'envoyer le devis par courriel", E_USER_ERROR );

}

/* ------------------------------------------------------------------------------------- */

?>