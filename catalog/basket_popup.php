<?php
/**
 * Package front-office
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Popup ajout référence au panier
 */
 
include_once( dirname( __FILE__ ) . "/../config/init.php" );
include_once( dirname( __FILE__ ) . "/../objects/Basket.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/proxies/BasketItemProxy.php" );
include_once( dirname( __FILE__ ) . "/../objects/flexy/FlexyController.php" );

class BasketPopupController extends FlexyController{};

$basketPopupController 	= new BasketPopupController( "basket_popup.tpl" );

$basketPopupController->setData( "basket", new BasketProxy() );

$items = array();
$it = Basket::getInstance()->getItems()->iterator();

while( $it->hasNext() )
	array_push( $items, new BasketItemProxy( $it->next() ) );
	
$basketPopupController->setData( "basketItems", $items );

header( "Cache-Control: no-cache, must-revalidate" );
header( "Pragma: no-cache" );
header( "Content-Type: text/html; charset=utf-8" );
	
$basketPopupController->output( array() );

?>