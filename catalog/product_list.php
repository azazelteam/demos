<?php
/**
 * Package administration
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * ....?
 */

/* ------------------------------------------------------------------------------------------ */
 
include_once( "../config/init.php" );

/* catégorie inexistante */

if( !isset( $_GET[ "IdCategory" ] ) || !intval( $_GET[ "IdCategory" ] )
	|| !DBUtil::query( "SELECT idcategory FROM category WHERE idcategory = '" . intval( $_GET[ "IdCategory" ] ) . "' LIMIT 1" )->RecordCount() ){
		
	header( "Location: " . URLFactory::getHostURL() );
	exit();
			
}

/* ------------------------------------------------------------------------------------------ */
 
include_once( "$GLOBAL_START_PATH/objects/catalog/CCategory.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CProductProxy.php" );

$category = new CCategory( intval( $_GET[ "IdCategory" ] ) );

/* ------------------------------------------------------------------------------------------ */
/* catégorie non disponible */

$catalog_right = Session::getInstance()->getCustomer() ? Session::getInstance()->getCustomer()->get( "catalog_right" ) : 0;

if( !$category->get( "available" ) 
	|| !$category->getChildren()->size() 
	|| $category->get( "catalog_right" ) > $catalog_right ){
	
	header( "Location: $GLOBAL_START_URL" );
	exit();
		
}

if( strlen( $category->get( "html" ) ) ){
	
	header( "Location: " . $category->get( "html" ) );
	exit();
	
}

/* ------------------------------------------------------------------------------------------ */

include_once( "$GLOBAL_START_PATH/objects/flexy/PageController.php" );
include_once( "$GLOBAL_START_PATH/objects/flexy/proxies/CCategoryProxy.php" );


$controller = new PageController( "product/product_list.tpl" );

$controller->setData( "category", new CCategoryProxy( $category ) );

$controller->output();

/* ------------------------------------------------------------------------------------------ */

?>