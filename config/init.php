<?php

/**
 * FrontOffice - prérequis
 * Created on 01 Juillet 2016
 * @author jfCorbé
 * Recherche factures fournisseurs
 */
 
//--------------------------------------------------------------------------------------------------------------
	
$sessid = session_id();

if( empty( $sessid ) ){

	$previous_name = session_name( "FrontOfficeNetlogix" );
	
	if( session_start() === false )
		trigger_error( "session_start() FAILED", E_USER_ERROR );
}	
	setlocale( LC_TIME, "fr_FR" );
	
	include( dirname( __FILE__ ) . "/config.php" );
	
	//--------------------------------------------------------------------------------------------------------------
	/* ADODB */
	
	$ADODB_DIR = dirname( __FILE__ ) . "/../objects/adodb";
    
	
	require_once( "$ADODB_DIR/adodb-errorhandler.inc.php" );
	require_once( "$ADODB_DIR/adodb.inc.php" );
	
	$ADODB_FETCH_MODE = ADODB_FETCH_ASSOC;
	
	include_once( dirname( __FILE__ ) . "/../objects/DBUtil.php" );
    
	
	//--------------------------------------------------------------------------------------------------------------
	/* gestionnaire d'erreurs */
	
	include_once( dirname( __FILE__ ) . "/../objects/ErrorHandler.php" );
    
	
	switch( $GLOBAL_DEBUG_LEVEL ){
		
		case ErrorHandler::$ERROR_HANDLER_PROD :
		case ErrorHandler::$ERROR_HANDLER_SQL :
		case ErrorHandler::$ERROR_HANDLER_DEV :
		case ErrorHandler::$ERROR_HANDLER_INSANE :
			
			ErrorHandler::setErrorHandler( $GLOBAL_DEBUG_LEVEL );
			break;
			
		default :
		
			$GLOBAL_LOG_SQL = null;
			ini_set( "display_errors","Off" );
			error_reporting( 0 );
			DBUtil::getConnection()->debug = false;
			
	}

	//----------------------------------------------------------------------------------

	$previous_name = session_name( "FrontOfficeNetlogix" );

	//----------------------------------------------------------------------------------
	
	include_once( dirname( __FILE__ ) . "/../objects/Util.php" );
	include_once( dirname( __FILE__ ) . "/../objects/URLFactory.php" );
	include_once( dirname( __FILE__ ) . "/../objects/Firewall.php" );
    
    
	new Firewall();
	
	include_once( dirname( __FILE__ ) . "/../objects/Cookie.php" );
    

	Cookie::getInstance();
	
	if( !Session::getInstance()->getCustomer() && isset( $_POST['Login'] ) && isset( $_POST['Password'] ) )
		Session::getInstance()->login( stripslashes( $_POST['Login'] ), md5( stripslashes( $_POST['Password'] ) ) );

	//----------------------------------------------------------------------------------
	


?>
